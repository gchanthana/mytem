package gestioncommande.popup
{
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.SaisieTechniqueEquipement;
	import gestioncommande.entity.TypesCommandesMobile;
	
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.EquipementService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	
	[Bindable]
	public class PopUpSaisieTechniqueImpl extends TitleWindow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListCommande			:DataGrid;

		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//

		private var _equService				:EquipementService 	= new EquipementService();
		private var _ligneUtils				:LignesUtils 		= new LignesUtils();
		private var _cmdService				:CommandeService	= new CommandeService();

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		public var commande					:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//

		public var listCommande				:ArrayCollection = new ArrayCollection();
		
		private var listArticles			:ArrayCollection = new ArrayCollection();
		private var _articlesXML			:XML 			 = new XML();
		private var _articlesToSendXML		:XML 			 = new XML();
		private var _closeValidate			:Boolean 		 = false;

		public var isMobile					:Boolean 		 = true;
		public var text_libelle_Colonne		:String 		 = "";
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
	
		public function PopUpSaisieTechniqueImpl()
		{			
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			
			_cmdService.addEventListener(CommandeEvent.COMMANDE_ARRIVED, articlesArrivedHandler);
			_equService.addEventListener(CommandeEvent.COMMANDE_MANAGED, livraisonManagedHandler);
			_equService.addEventListener(CommandeEvent.LISTED_SIMASSOCIATE, simAssociateHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					LISTENER DE L'IHM
	//--------------------------------------------------------------------------------------------//
	
		protected function creationCompleteHandler(fe:FlexEvent):void
		{
			_cmdService.fournirArticlesCommande(commande.IDCOMMANDE);
			
			isMobile = whatIsTypeCommande();
			
			switch(isMobile)
			{
				case true	: text_libelle_Colonne = ResourceManager.getInstance().getString('M16', 'IMEI_terminal__'); break;
				case false 	: text_libelle_Colonne = ResourceManager.getInstance().getString('M16', 'Num_ro_de_s_rie'); break;
			}
		}

//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A LA POPOUP
//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function btnAnnulClickHandler(e:Event):void
		{
			_closeValidate = false;
			closeHandler();
		}
		
		protected function btnValidClickHandler(e:Event):void
		{
			if(checkChamps())
				articleUpdate();
			else
				Alert.show(ResourceManager.getInstance().getString('M16', 'Veuillez_remplir_les_champs_obligatoires__'),'Consoview');
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER OBJECT
		//--------------------------------------------------------------------------------------------//		
		
		private function articlesArrivedHandler(cmde:CommandeEvent):void
		{
			_articlesXML = _cmdService.listeArticles;
			formatXMLToArrayCollection();
		}

		private function livraisonManagedHandler(cmde:CommandeEvent):void
		{
			_closeValidate = true;
			closeHandler();
		}

		private function simAssociateHandler(cmde:CommandeEvent):void
		{
			var sims	:ArrayCollection = _equService.cartesimAssociate;
			var lenSim	:int			 = sims.length;
			var lenCmd	:int			 = listCommande.length;
			
			for(var i:int = 0;i < lenCmd;i++)
			{
				for(var j:int = 0;j < lenSim;j++)
				{
					if(listCommande[i].IDSOUSTETE == sims[j].IDSOUSTETE)
					{
						if(sims[j].IDEQUIPEMENT_SIM != 0)
						{
							listCommande[i].IDEQUIPEMENT_SIM 	= sims[j].IDCARTESIM;
							listCommande[i].NUM_CARTESIM 		= sims[j].NUMEROSIM;
							listCommande[i].PUK 				= sims[j].PUK;
							listCommande[i].PIN 				= sims[j].PIN;
							listCommande[i].BOOL_CARTESIM 	 	= true;
						}
					}
				}
			}
			listCommande.refresh();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//	
		
		private function articleUpdate():void
		{
			_equService.manageLivraisonEquipements(formatArrayCollectionToXML());
		}
		
		//--- true-> MOBILE, false-> FIXE/DATA
		private function whatIsTypeCommande():Boolean
		{
			var OK:Boolean = true;
			
			switch(commande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		OK = false; break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		OK = true;  break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		OK = false; break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: OK = false; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	OK = false; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			OK = false; break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	OK = false; break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		OK = true;  break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	OK = false; break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			OK = false; break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		OK = false; break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		OK = false; break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	OK = false; break;//--- REACT
			}
			
			return OK;
		}
		
		private function closeHandler():void
		{
			var STRGEVENT:String = "LIVRAISONCLOSED";
			
			if(_closeValidate)
				STRGEVENT = "LIVRAISONCLOSEDVALIDATE"; 
			
			dispatchEvent(new Event(STRGEVENT));
		}

		private function addArticleByArticle(values:XML):Array
		{
			var articlesArray	:Array 	 = new Array();
			var children		:XMLList = values.children();
			var len				:int	 = children.length();
			var i				:int	 = -1;
			var allArticles		:XML 	 = <articles></articles>;
			
			for(i = 0;i < len;i++)
			{
				allArticles = <articles></articles>;
				allArticles.appendChild(children[i]);
				articlesArray[i] = allArticles;
			}
			
			return articlesArray;
		}
		
		//BOOL_CARTESIM, IDEQUIPEMENT_SIM, NUM_CARTESIM, PUK, BOOL_TERMINAL, IDEQUIPEMENT_MOB, NAME_EQUIPEMENT, IMEI
		private function formatArrayCollectionToXML():XML
		{
			var row		 	:XML = <row></row>;
			var equipement 	:XML;
			var iduser		:Number = CvAccessManager.getUserObject().CLIENTACCESSID;
			var idracine	:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
			
			for(var i:int = 0;i < listCommande.length;i++)
			{
				if(listCommande[i].BOOL_TERMINAL)
				{
					equipement 	= <equipement></equipement>;
					equipement.appendChild(<iduser>{iduser}</iduser>);
					equipement.appendChild(<idracine>{iduser}</idracine>);
					equipement.appendChild(<idequipement>{searchError(listCommande[i].IDEQUIPEMENT_MOB)}</idequipement>);			 
					equipement.appendChild(<no_serie>{searchError(listCommande[i].IMEI)}</no_serie>);
					equipement.appendChild(<no_sim></no_sim>);
					equipement.appendChild(<codepin></codepin>);
					equipement.appendChild(<codepuk></codepuk>);
					equipement.appendChild(<idsous_tete>{searchError(listCommande[i].IDSOUSTETE)}</idsous_tete>);
					equipement.appendChild(<ligne>{searchError(listCommande[i].LIGNE)}</ligne>);
					
					row.appendChild(equipement);
				}
				
				if(listCommande[i].BOOL_CARTESIM)
				{
					equipement 	= <equipement></equipement>;
					equipement.appendChild(<iduser>{iduser}</iduser>);
					equipement.appendChild(<idracine>{iduser}</idracine>);
					equipement.appendChild(<idequipement>{searchError(listCommande[i].IDEQUIPEMENT_SIM)}</idequipement>);
					equipement.appendChild(<no_serie></no_serie>);
					equipement.appendChild(<no_sim>{searchError(listCommande[i].NUM_CARTESIM)}</no_sim>);
					equipement.appendChild(<codepin>{searchError(listCommande[i].PIN)}</codepin>);
					equipement.appendChild(<codepuk>{searchError(listCommande[i].PUK)}</codepuk>);
					equipement.appendChild(<idsous_tete>{searchError(listCommande[i].IDSOUSTETE)}</idsous_tete>);
					equipement.appendChild(<ligne>{searchError(listCommande[i].LIGNE)}</ligne>);
					
					row.appendChild(equipement);
				}
			}
			
			return row;
		}
		
		private function searchError(value:String):String
		{
			var newValue:String = "";
			
			if(value != null && value != "null" && value!= "NaN")
				newValue = value;
			
			return newValue;
		}
		
		private function formatXMLToArrayCollection():void
		{
			listArticles = new ArrayCollection();
			var xmlList:XMLList = _articlesXML.children();
			var nbre:int = xmlList.length();
			
			if(nbre > 1)
				formatXMLList(xmlList);
			else
				formatXML(xmlList[0]);

			checkTerminauxOnlyOrNot();
		}
		
		private function formatXMLList(xmlList:XMLList):void
		{	
			for(var i:int = 0;i < xmlList.length();i++)
			{
				formatXML(xmlList[i]);
			}
		}
		
		private function formatXML(xml:XML):void
		{
			if(!xml) return;
			var object			:Object = new Object();
			var xmlListEquip	:XMLList = 	xml.equipements.children();
			var xmlListRess		:XMLList =	xml.ressources.children();
			 	
			if(xml.fpc[0] != "")
			{
				object.ENGAGEMENT_RESSOURECES 	= giveMeDate(String(xml.fpc[0]));
				object.ENGAGEMENT_RESSOURECES   = 24;
				object.ENGAGEMENT_EQUIPEMENTS 	= 24;
			}
			else
			{
				object.ENGAGEMENT_RESSOURECES   = Number(xml.engagement_res[0]);
				object.ENGAGEMENT_EQUIPEMENTS 	= Number(xml.engagement_eq[0]);
			}
			
			if(lookForCollaborateur(String(xml.nomemploye[0])) == " - " && lookForCollaborateur(String(xml.code_interne[0])) != " - ")
				object.NOMEMPLOYE 			= lookForCollaborateur(String(xml.code_interne[0]));
			else
				object.NOMEMPLOYE 			= lookForCollaborateur(String(xml.nomemploye[0]));
				
			object.SOUSTETE 				= String(xml.soustete[0]);
			object.IDSOUSTETE 				= Number(xml.idsoustete[0]);
	
			listArticles.addItem(addToObject(addEquipements(xmlListEquip), object));
		}
		
		private function giveMeDate(strg:String):String
		{
			var day:String 		= strg.substr(8,2);
			var month:String	= strg.substr(5,2);
			var year:String		= strg.substr(0,4);
			
			return day + "/" + month + "/" + year;
		}
		
		private function addEquipements(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0; i < xmlList.length();i++)
			{
				object = new Object();
				object.TYPE_EQUIPEMENT 	 = String(xmlList[i].type_equipement);
				object.LIBELLE_PRODUIT 	 = String(xmlList[i].libelle);
				object.ID_PRODUIT 		 = String(xmlList[i].idequipement);
				object.PRIX_UNIT 		 = Number(xmlList[i].prix);
				object.TOTAL			 = Number(xmlList[i].prix);
				object.IDTYPE_EQUIPEMENT = Number(xmlList[i].idtype_equipement);

				if(object.IDTYPE_EQUIPEMENT != 2191)
				{
					object.IMEI 			 = String(xmlList[i].numeroserie);
					
					if(object.IDTYPE_EQUIPEMENT == 71)
					{
						object.NUM_CARTESIM = String(xmlList[i].numeroserie);
						object.PUK 			= String(xmlList[i].codepuk);
						object.PIN			= String(xmlList[i].codepin);
					}
					
					if(object.IDTYPE_EQUIPEMENT == 2191)
						object.BOOL_ACCES 				= 0;//---ACCESSOIRES
					else
						object.BOOL_ACCES 				= 1;//---PAS ACCESSOIRES
				
					arrayCollection.addItem(object);
				}
			}
			
			return arrayCollection;
		}
		
		private function addToArrayCollection(equipements:ArrayCollection):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0; i < equipements.length;i++)
			{
				arrayCollection.addItem(equipements[i]);
			}
			
			return arrayCollection;
		}
		
		private function addToObject(allItems:Object,info:Object):Object
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			var object:Object = new Object();
			
			object.ITEMS 			= allItems;
			object.COLLABORATEUR 	= info.NOMEMPLOYE;
			object.ENGAGEMENT 		= info.ENGAGEMENT_RESSOURECES;
			object.SOUSTETE 		= info.SOUSTETE;
			object.IDSOUSTETE		= info.IDSOUSTETE;
			object.PRIX 			= calculPrice(allItems as ArrayCollection);
			
			return object;
		}
		
		private function calculPrice(allItems:ArrayCollection):String
		{
			var total:Number = 0;
			
			for(var i:int = 0; i < allItems.length;i++)
			{
				if(allItems[i].PRIX_UNIT != " - ")
					total = total + Number(allItems[i].PRIX_UNIT);
			}
			
			return total.toString();
		}
		
		private function lookForCollaborateur(name:String):String
		{
			var nameCollaborateur:String = "";
			
			if(name == "")
				nameCollaborateur = " - " 
			else
				nameCollaborateur = name;

			return nameCollaborateur;
		}

		private function checkTerminauxOnlyOrNot():void
		{
			var eq:ArrayCollection = new ArrayCollection();
			if(listArticles != null && listArticles.length > 0)
			{
				var  object:Object = new Object();
				for(var i:int = 0;i < listArticles.length;i++)
				{
					object = new Object();
					object.LIGNE 		= listArticles[i].SOUSTETE;
					object.IDSOUSTETE 	= listArticles[i].IDSOUSTETE;
					object.IMEI 		= listArticles[i].IMEI;
					object.NUM_CARTESIM	= listArticles[i].NUM_CARTESIM;
					object.PUK 			= listArticles[i].PUK;
					object.PIN 			= listArticles[i].PIN;
									
					for(var j:int = 0;j < listArticles[i].ITEMS.length;j++)
					{
						if(!listArticles[i].ITEMS[j].hasOwnProperty("ISNOTEQUIMENT"))
						{
							if(listArticles[i].ITEMS[j].IDTYPE_EQUIPEMENT == 71)
							{
								object.BOOL_CARTESIM 	= true;
								object.IDEQUIPEMENT_SIM = listArticles[i].ITEMS[j].ID_PRODUIT;
								object.NUM_CARTESIM		= listArticles[i].ITEMS[j].NUM_CARTESIM;
								object.PUK				= listArticles[i].ITEMS[j].PUK;
								object.PIN				= listArticles[i].ITEMS[j].PIN;
							}
							else if(listArticles[i].ITEMS[j].IDTYPE_EQUIPEMENT != 2191)
								{
									object.BOOL_TERMINAL 	= true;
									object.BOOL_CARTESIM 	= isSimpleEquipement();
									object.IDEQUIPEMENT_MOB = listArticles[i].ITEMS[j].ID_PRODUIT;
									object.NAME_EQUIPEMENT 	= listArticles[i].ITEMS[j].LIBELLE_PRODUIT;
									object.IMEI				= listArticles[i].ITEMS[j].IMEI;
								}
							
							object.COLLABORATEUR = listArticles[i].COLLABORATEUR;
						}
					}

					eq.addItem(object);
				}
				mapDataSaisieTechniqueEquipement(eq);
			}
		}

		private function isSimpleEquipement():Boolean
		{
			var OK:Boolean = true;
			
			switch(commande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	OK = true; 	break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		OK = true; 	break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		OK = false; break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		OK = false; break;//--- NVL COMMANDE
			}
			
			return OK;
		}

		private function mapDataSaisieTechniqueEquipement(eq:ArrayCollection):void
		{
			var saisie		:SaisieTechniqueEquipement;
			var idsSousTete :Array = new Array();
			
			
			for(var i:int = 0;i < eq.length;i++)
			{
				saisie = new SaisieTechniqueEquipement();
				saisie.BOOL_CARTESIM 	= eq[i].BOOL_CARTESIM;
				saisie.BOOL_TERMINAL 	= eq[i].BOOL_TERMINAL;
				saisie.IDEQUIPEMENT_MOB = eq[i].IDEQUIPEMENT_MOB;
				saisie.IDEQUIPEMENT_SIM = eq[i].IDEQUIPEMENT_SIM;
				
				if(!saisie.BOOL_TERMINAL)
					saisie.NAME_EQUIPEMENT	= ResourceManager.getInstance().getString('M16','Aucun_equipement');
				else
					saisie.NAME_EQUIPEMENT 	= eq[i].NAME_EQUIPEMENT;
					
				saisie.COLLABORATEUR	= eq[i].COLLABORATEUR;
				saisie.LIGNE			= eq[i].LIGNE;
				saisie.SOUSTETE			= eq[i].LIGNE;
				saisie.IDSOUSTETE		= eq[i].IDSOUSTETE;
				saisie.IMEI				= eq[i].IMEI;
				saisie.NUM_CARTESIM		= eq[i].NUM_CARTESIM;
				saisie.PUK				= eq[i].PUK;
				saisie.PIN				= eq[i].PIN;
				
				idsSousTete[i] = saisie.IDSOUSTETE;
				listCommande.addItem(saisie);
			}
			
			_equService.fournirAssociatesSim(idsSousTete, commande.IDPOOL_GESTIONNAIRE);
		}

		private function checkChamps():Boolean
		{
			var bool:Boolean = true;
			
			for(var i:int = 0;i < listCommande.length;i++)
			{
				if(listCommande[i].BOOL_TERMINAL as Boolean)
				{
					if(listCommande[i].IMEI == "")
						bool = false;
				}
				
				if(listCommande[i].BOOL_CARTESIM as Boolean)
				{
					if(listCommande[i].LIGNE == "" || listCommande[i].NUM_CARTESIM == "")
						bool = false;
				}
			}
			
			return bool;
		}

	}
}