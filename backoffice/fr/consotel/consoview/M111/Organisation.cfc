﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	</cffunction>

	<!--- 
		Fournis la liste des feuilles et la structure de l'organisation.
	 --->
	<cffunction name="getNodesOrga" access="remote" returntype="Array" hint="Fournis la liste des feuilles et la structure de l'organisation.">
		<cfargument name="idOrga" required="true" type="numeric" />
		
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m11.get_feuilles_orga">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrga#" >
				
				<cfprocresult name="listeFeuilles"	resultset="1">
	    		<cfprocresult name="structureOrga" 	resultset="2">
			</cfstoredproc>
		 
			<cfset p_retour = ArrayNew(1)>
			<cfset p_retour[1] = listeFeuilles >
			<cfset p_retour[2] = structureOrga >
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Fournis la liste des organisations.
	 --->
	<cffunction name="getListOrgaCliente" output="false" access="remote" returntype="Query" hint="Fournis la liste des organisations.">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_RAGO.GET_ORGA_CUS"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idRacine#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

</cfcomponent>