<cfcomponent name="gestionDestinataire">
		
		<cffunction name="getDestinataireList" access="remote" returntype="any">
			<cfargument name="idGroupe" type="numeric" required="true">	       
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M22.getDestinataireList">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="removeDestinataire" access="remote" returntype="any">
			<cfargument name="idDestinataire" type="numeric" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.delDestinataire">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDestinataire#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
			<cfreturn true>
		</cffunction>
		
		<cffunction name="addDestinataire" access="remote" returntype="any">
			<cfargument name="idSource" type="string" required="true">
			<cfargument name="idDest" type="string" required="true">
			<cfargument name="date" type="string" required="false">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.addDestinataire">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSource#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDest#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="addProfilToDest" access="remote" returntype="any">
			<cfargument name="IDDest" type="string" required="true">
			<cfargument name="idProfil" type="string" required="true">			
			<cfargument name="date" type="string" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.addProfilToDest">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDDest#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="removeProfilToDest" access="remote" returntype="any">
			<cfargument name="IDDest" type="string" required="true">
			<cfargument name="idProfil" type="string" required="true">			
			<cfargument name="date" type="string" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.delProfilToDest">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDDest#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
			<cfreturn true>
		</cffunction>
		
		<cffunction name="addProfil" access="remote" returntype="any">
			<cfargument name="idracine" type="string" required="true">
			<cfargument name="libelle" type="string" required="true">
			<cfargument name="codeInterne" type="string" required="true">
			<cfargument name="commentaire" type="string" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.addProfil">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="updateProfil" access="remote" returntype="any">
			<cfargument name="idProfil" type="string" required="true">
			<cfargument name="libelle" type="string" required="true">
			<cfargument name="codeInterne" type="string" required="true">
			<cfargument name="commentaire" type="string" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.updateProfil">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="removeProfil" access="remote" returntype="any">
			<cfargument name="idProfil" type="string" required="true">	 
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.delProfil">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="getProfils" access="remote" returntype="any">
			<cfargument name="idGroupe" type="string" required="false">
			<cfargument name="idDest" type="string" required="false">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.getProfils">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDest#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	       <!--- <cfset tab=arrayNew(1)>
			<cfset s=structNew()>
			<cfset s.IDPROFIL=1>
			<cfset s.LIBELLE="profil01">
			<cfset s.ASSIGN=1>
			<cfset arrayAppend(tab, s)>
			<cfset s=structNew()>
			<cfset s.IDPROFIL=2>
			<cfset s.LIBELLE="profilInfo">
			<cfset s.ASSIGN=1>
			<cfset arrayAppend(tab, s)>
			<cfset s=structNew()>
			<cfset s.IDPROFIL=3>
			<cfset s.LIBELLE="profilMarketing">
			<cfset s.ASSIGN=1>
			<cfset arrayAppend(tab, s)>
			<cfset s=structNew()>
			<cfset s.IDPROFIL=4>
			<cfset s.LIBELLE="profilCommercial">
			<cfset s.ASSIGN=0>
			<cfset arrayAppend(tab, s)>
			<cfset s=structNew()>
			<cfset s.IDPROFIL=5>
			<cfset s.LIBELLE="profil05">
			<cfset s.ASSIGN=1>
			<cfset arrayAppend(tab, s)>
			<cfset s=structNew()>
			<cfset s.IDPROFIL=6>
			<cfset s.LIBELLE="profilTest">
			<cfset s.ASSIGN=0>
			<cfset arrayAppend(tab, s)>
			<cfreturn tab> --->
		</cffunction>
		
		<cffunction name="getAllProfils" access="remote" returntype="any">
			<cfargument name="idRacine" type="string" required="false">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.getAllProfils">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="isEmploye" access="remote" returntype="any">
			<cfargument name="idGroupe" type="string" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.isEmploye">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">	
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		
		<cffunction name="createEmploye" access="remote" returntype="any">
			<cfargument name="idGroupe" type="string" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.Createemploye">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
		<cffunction name="getOrgaGeo" access="remote" returntype="any">
			<cfargument name="idGroupe" type="string" required="true">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.get_orga_geo">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_VaRCHAR" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
</cfcomponent>
