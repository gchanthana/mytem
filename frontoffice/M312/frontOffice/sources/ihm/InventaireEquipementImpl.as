//***************************************************	
//	author : samuel.divioka								*
//	date   : Mar 31, 2011								*
//	owner  : Consotel								*
//***************************************************
package ihm
{

	import mx.collections.ArrayCollection;
	import mx.containers.Panel;
	import mx.containers.ViewStack;
	import mx.controls.DataGrid;
	import mx.controls.TabBar;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Container;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.events.ListEvent;
	import mx.utils.ObjectUtil;
	
	import composants.tb.utils.preloader.Spinner;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import event.InventaireEquipementIhmEvent;
	
	[Event(name="marqueChangeEvent",type="event.InventaireEquipementIhmEvent")]
	[Event(name="modeleChangeEvent",type="event.InventaireEquipementIhmEvent")]
	[Event(name="navChangeEvent",type="event.InventaireEquipementIhmEvent")]
	
	[Bindable]
	public class InventaireEquipementImpl extends Panel
	{	
		
		/// Providers
		private var _listeMarque:ArrayCollection;// la liste des marque par équipement
		private var _listeModele:ArrayCollection;// la liste des modèles pour une marque
		private var _listeEquipement:ArrayCollection;// la liste des equipement d'un modèle 
		
		/// composant
		public var dgMarque:DataGrid;
		public var dgModele:DataGrid;
		public var dgList:DataGrid;
		
		private var _spinner:Spinner;
		private var _spinnerVisible:Boolean = true;
		private var _creationCompelted:Boolean = false;
		
		public var navInventaire:ViewStack;
		public var lb:TabBar;		
		///////////////////
		
		
		//// Date Période Complete
		public var dateLastCompletePeriodString:String = "";
		private var _dateLastCompletePeriod:Date;
		
		public function get dateLastCompletePeriod():Date
		{
			return 	_dateLastCompletePeriod;
		}
		
		public function set dateLastCompletePeriod(d:Date):void
		{
			if(!d)
			{
				dateLastCompletePeriodString = "n.c.";				
				
			}else if(d != _dateLastCompletePeriod)
			{
				_dateLastCompletePeriod = d;
				dateLastCompletePeriodString = DateFunction.formatDateAsShortString(_dateLastCompletePeriod)
			}
		}
		//////////////////////////
		
		///// Selection marque modèle 
		public var selectedMarque:String = "";
		public var selectedModele:String = "";
		public var selectedNbEquipement:String = "";
		public var selectedNbModele:String = "";
		///////////////
		
		///// private
		private var _currentNavIndex:Number = 0;// index de la navigation courrante
		
		//// FRAMEWORK
		override protected function createChildren():void			
		{
			super.createChildren();
			_spinner = new Spinner();
			
			_spinner.includeInLayout = false;
			_spinner.x = width / 2 - _spinner.width /2;
			_spinner.y = height / 2 - _spinner.height /2;
			_spinner.visible = true;
			addChild(_spinner);
			
		}
		
		override protected function commitProperties():void
		{
			if(_spinner)
			{
				_spinner.visible = spinnerVisible;
				if(spinnerVisible)
				{
					_spinner.play();
				}
				else
				{
					_spinner.stop();
				}
			}
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			_spinner.x = width / 2 - _spinner.width /2;
			_spinner.y = height / 2 - _spinner.height /2;
		}		
		///////////////////
		
		public function InventaireEquipementImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);
		}
		
		
		//Formattage de la colonne du grid de l'historique des dépenses
		protected function dgNumLignelabelFunction(item:Object,column:DataGridColumn):String
		{			
			var numero:String = ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
			return numero;
		}
		
		
		///Initialisation du composant
		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{	
			initTabInventaire();
			_creationCompelted = true;
		}
		
		public function initTabs():void
		{
			if(_creationCompelted == true)
			{
				initTabInventaire();
				navInventaire.selectedIndex =0;
			}
		}
		
		
		//on initialire le tableau d'inventaire
		private function initTabInventaire():void
		{				
			var len:Number = navInventaire.numChildren;				
			for (var n:Number = 1; n < len; n++)
			{
				(navInventaire.getChildAt(n) as Container).label = "";
				lb.getChildAt(n).visible = false;
			}
		}
		
		
		
		
		
		//Handler du click sur le grid des marques
		protected function dgMarque_changeHandler(event:ListEvent):void
		{	
			
			listeModele.removeAll();
			listeEquipement.removeAll();
		
			
			if(dgMarque.selectedItem)
			{					
				
				selectedMarque = ObjectUtil.copy(dgMarque.selectedItem.MARQUE).toString();
				selectedNbModele = " ("+ ObjectUtil.copy(dgMarque.selectedItem.QTE).toString() +")";
				navInventaire.selectedIndex ++;
				var n:Number = navInventaire.selectedIndex;
				lb.getChildAt(n).visible = true;
				dispatchEvent(new InventaireEquipementIhmEvent(InventaireEquipementIhmEvent.MARQUE_CHANGE_EVENT));
			}
			else
			{
				
			}
			
		}
		
		//Handler du click sur le grid des modeles
		protected function dgModele_changeHandler(event:ListEvent):void
		{
			listeEquipement.removeAll();	
			
			if(dgModele.selectedItem)
			{
				selectedModele = ObjectUtil.copy(dgModele.selectedItem.MODELE).toString() ;
				selectedNbEquipement = " ("+ ObjectUtil.copy(dgModele.selectedItem.QTE).toString() +")";
				navInventaire.selectedIndex ++;
				var n:Number = navInventaire.selectedIndex;				
				lb.getChildAt(n).visible = true;
				dispatchEvent(new InventaireEquipementIhmEvent(InventaireEquipementIhmEvent.MODELE_CHANGE_EVENT));
			}
			else
			{
				
			}
			
		}
		
		//Handler du click sur le navigateur d'inventaire
		protected function lb_itemClickHandler(event:ItemClickEvent):void
		{				
			var i:Number = navInventaire.selectedIndex +1;
			var len:Number = navInventaire.numChildren;
			var cont:Container;
			
			
			
			for (var n:Number = i; n < len; n++)
			{
				cont=(navInventaire.getChildAt(n) as Container)
				cont.label = "";					 
				lb.getChildAt(n).visible = false;
			}
			
			
			
				dispatchEvent(new InventaireEquipementIhmEvent(InventaireEquipementIhmEvent.NAV_CHANGE_EVENT));	
		
			
		}

	
		

		public function set listeMarque(value:ArrayCollection):void
		{
			
			
			if(value != _listeMarque)
			{
				_listeMarque = value;					
			}
			
		}

		public function get listeMarque():ArrayCollection
		{
			return _listeMarque;
		}
		public function set listeModele(value:ArrayCollection):void
		{
			
			
			if(value != _listeModele)
			{
				_listeModele = value;				
			}
			
		}

		public function get listeModele():ArrayCollection
		{
			return _listeModele;
		}
		
		public function set listeEquipement(value:ArrayCollection):void
		{	
			if(value != _listeEquipement)
			{
				_listeEquipement = value;							
			}
		}

		public function get listeEquipement():ArrayCollection
		{
			return _listeEquipement;
		}

		public function set spinnerVisible(value:Boolean):void
		{
			if(value != _spinnerVisible)
			{
				_spinnerVisible = value;
				invalidateProperties();
				invalidateDisplayList();
			}
		}

		public function get spinnerVisible():Boolean
		{
			return _spinnerVisible;
		}
	}
}