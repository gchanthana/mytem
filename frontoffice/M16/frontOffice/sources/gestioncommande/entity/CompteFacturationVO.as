package gestioncommande.entity
{
	[Bindable]
	public class CompteFacturationVO
	{
		private var _IDCOMPTE_FACTURATION	:Number;
		private var _LABEL					:String;
		private var _COMPTE_FACTURATION		:String;
		private var _SOUS_COMPTE			:String;
		private var _IDSOUS_COMPTE			:Number;
		private var _SELECTED				:Boolean;
		
		public function CompteFacturationVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty('IDCOMPTE_FACTURATION'))
					this.IDCOMPTE_FACTURATION = obj.IDCOMPTE_FACTURATION;
				if(obj.hasOwnProperty('LABEL'))
					this.LABEL = obj.LABEL;

				if(obj.hasOwnProperty('COMPTE_FACTURATION'))
					this.COMPTE_FACTURATION = obj.COMPTE_FACTURATION;
				
				if(obj.hasOwnProperty('SOUS_COMPTE'))
					this.SOUS_COMPTE = obj.SOUS_COMPTE;
				
				if(obj.hasOwnProperty('IDSOUS_COMPTE'))
					this.IDSOUS_COMPTE = obj.IDSOUS_COMPTE;
			} 
			catch(err:Error) 
			{
				
			}
		}
		
		public function get IDSOUS_COMPTE():Number { return _IDSOUS_COMPTE; }
		
		public function set IDSOUS_COMPTE(value:Number):void
		{
			if (_IDSOUS_COMPTE == value)
				return;
			_IDSOUS_COMPTE = value;
		}
		
		public function get SOUS_COMPTE():String { return _SOUS_COMPTE; }
		
		public function set SOUS_COMPTE(value:String):void
		{
			if (_SOUS_COMPTE == value)
				return;
			_SOUS_COMPTE = value;
		}
		
		public function get COMPTE_FACTURATION():String { return _COMPTE_FACTURATION; }
		
		public function set COMPTE_FACTURATION(value:String):void
		{
			if (_COMPTE_FACTURATION == value)
				return;
			_COMPTE_FACTURATION = value;
		}
		
		public function get IDCOMPTE_FACTURATION():Number { return _IDCOMPTE_FACTURATION; }
		
		public function set IDCOMPTE_FACTURATION(value:Number):void
		{
			if (_IDCOMPTE_FACTURATION == value)
				return;
			_IDCOMPTE_FACTURATION = value;
		}
		
		public function get LABEL():String { return _LABEL; }
		
		public function set LABEL(value:String):void
		{
			if (_LABEL == value)
				return;
			_LABEL = value;
		}
		
		public function get SELECTED():Boolean { return _SELECTED; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}
	}
}