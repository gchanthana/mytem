package gestioncommande.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.dataGridClasses.DataGridItemRenderer;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import composants.pagindatagrid.PaginateDatagrid;
	import composants.pagindatagrid.pagination.event.PaginationEvent;
	import composants.searchpagindatagrid.SearchPaginateDatagrid;
	import composants.searchpagindatagrid.event.SearchPaginateDatagridEvent;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.LignesByOperateur;
	import gestioncommande.entity.MultiCompte;
	import gestioncommande.entity.MultiSousCompte;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.TypeOperationSurLigneIHMEvent;
	import gestioncommande.services.Formator;
	import gestioncommande.services.LigneService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class OperationSurLigneImpl extends TitleWindow
	{
		//--------------------------------------------------------------------------------------------//
		//					CONSTANTES
		//--------------------------------------------------------------------------------------------//
		private const ETAT_LIGNE_RESILIEE:Number = 3;
		private const ETAT_LIGNE_SUSPENDUE:Number = 2;
		private const ETAT_LIGNE_ACTIVE:Number = 1;
		private const ETAT_LIGNE_INCONNUE:Number = -1;
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMs
			//--------------------------------------------------------------------------------------------//
		
		public var myPaginatedatagrid	:PaginateDatagrid;
		
		public var dgLines				:DataGrid;
		
		public var spdClef 				:SearchPaginateDatagrid;
		
		public var cbxOperateurs		:ComboBox;
		
		public var rdAct				:RadioButton;
		public var rdSus				:RadioButton;
		
		public var chxStructFacturation	:CheckBox;
		public var rbgEnFacture			:RadioButtonGroup;
		public var comboEligible		:ComboBox;
			
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpOperation		:TypeOperationSurLigneIHM;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _lignesSrv			:LigneService = new LigneService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJECTS TYPE
			//--------------------------------------------------------------------------------------------//
		
		
			//--------------------------------------------------------------------------------------------//
			//					AURTES
			//--------------------------------------------------------------------------------------------//
		
		public var operateurs			:ArrayCollection = new ArrayCollection();
		public var searchIn				:ArrayCollection = new ArrayCollection();
		public var lignes				:ArrayCollection = new ArrayCollection();
		public var lignesSelected		:ArrayCollection = new ArrayCollection();
		public var arrayComptes			:ArrayCollection = new ArrayCollection();
		public var arraySousComptes		:ArrayCollection = new ArrayCollection();
		public var comptesSousComptes	:ArrayCollection = new ArrayCollection();
		
		public var operateurSelected	:Object = null;
		
		public var isMultiComptes		:Boolean = false;
		public var isMultiSousComptes	:Boolean = false;
		public var isNoCompte			:Boolean = false;
		
		public static var NBITEMPARPAGE	:int = 6;
		
		private var _listeMultiCompte	:ArrayCollection = new ArrayCollection();

		private var _multiCompte		:Boolean = false;
		private var _multiSousCompte	:Boolean = false;
		
		private var _lastEtatLigne		:int = ETAT_LIGNE_INCONNUE;
		private var _lastCompte			:int = -1;
		private var _lastSousCompte		:int = -1;
		
		private var cpSelectedlignes:ArrayCollection = new ArrayCollection();

		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function OperationSurLigneImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function btnNextClickHandler(me:MouseEvent):void
		{
			if(chekEtatProcedure())
			{
				if(checkLignesSelected())
					selectTypeOperation();
			}
			else
				ConsoviewAlert.afficherAlertInfo("Veuillez patienter.\nDes lignes sont en cours de vérification.", ResourceManager.getInstance().getString('M16', 'Alerte'), null);
		}

		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function closeThisClickHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function cbxOperateursChangeHandler(e:Event):void
		{
			if(cbxOperateurs.selectedItem != null)
				operateurSelected = cbxOperateurs.selectedItem;
		}
		
		protected function spdClefSearchHandler(spde:SearchPaginateDatagridEvent):void
		{
			var len:int = myPaginatedatagrid.copyTempListeSelecd.length
			if(len > 0)
			{
				// si il ya des lignes sélectionnées alors avant de faire la recherche, attendre à vérifier les ligne déjà séléctionnées
				if(chekEtatProcedureList(myPaginatedatagrid.copyTempListeSelecd))
				{
					getLignesOperators();
				}
				else
					ConsoviewAlert.afficherAlertInfo("Veuillez patienter.\nDes lignes sont en cours de vérification.", ResourceManager.getInstance().getString('M16', 'Alerte'), null);
			}
			else
				getLignesOperators();
			
		}
		
		protected function myPaginatedatagridClickHandler(me:MouseEvent):void
		{
			if(me.currentTarget as PaginateDatagrid)
			{
				if(myPaginatedatagrid.selectedItem != null)
				{					
					getLignesOperateur(myPaginatedatagrid.selectedItem as LignesByOperateur, myPaginatedatagrid.selectedItem.SELECTED)
					
					myPaginatedatagrid.dataprovider.itemUpdated(myPaginatedatagrid.selectedItem);
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATES
		//--------------------------------------------------------------------------------------------//

			//--------------------------------------------------------------------------------------------//
			//					LISTENERS EVENT IHMS
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			lignesSelected = myPaginatedatagrid.tempListeSelected;
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			
			getOperators();
			
			myPaginatedatagrid.paginationBox.initPagination(0, 0);
			
			addEventListener('REFRESH_PAGINATE_DATAGRID', refreshPaginateDatagridHandler);
			addEventListener('ERASE_LIGNE_SELECTED', refreshLigneSelectedHandler);
			
			myPaginatedatagrid.addEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE, retrieveProcedure);
			
			lignesSelected.addEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChangeHandler);
			spdClef.addEventListener(SearchPaginateDatagridEvent.SEARCH, spdClefSearchHandler);
		}

		private function collectionChangeHandler(ce:CollectionEvent):void
		{
			if(ce.kind == 'add')
			{
				if(ce.items.length > 0)
				{
					var lbo:LignesByOperateur = ce.items[0];
						lbo.classProcEligibilite.checkEligibiliteLignesByLignesAndGetCompteSousCompte(lbo.IDSOUS_TETE, lbo.ETAT_ELIGIBILITE, cbxOperateurs.selectedItem.OPERATEURID,lbo.IDOPERATEUR);
						lbo.addEventListener('INFOS_LIGNE_REFRESH', refreshInfosLigneHandler);
				}
			}
			else if((ce.kind == 'remove')||(ce.kind == 'reset'))
			{
				setEnabledDisabledCheckBoxEtat();
			}
		}
		
		private function refreshInfosLigneHandler(e:Event):void
		{
			if(e.currentTarget as LignesByOperateur)
			{	
				if(checkCompte((e.currentTarget as LignesByOperateur), true))
				{
					lignesSelected.itemUpdated(e.currentTarget);
					
					lignes.itemUpdated(e.currentTarget);
				}
				else
				{
					getLignesOperateur((e.currentTarget as LignesByOperateur), false);
					
					 lignes.itemUpdated(e.currentTarget);
				}
			}
			
			setEnabledDisabledCheckBoxEtat();
		}
		
		private function refreshPaginateDatagridHandler(e:Event):void
		{			
			if(e.type == 'REFRESH_PAGINATE_DATAGRID')
			{
				if(e.target.hasOwnProperty('data'))
					(myPaginatedatagrid.dgPaginate.dataProvider as ArrayCollection).itemUpdated(e.target.data);
			}
		}
		
		private function refreshLigneSelectedHandler(e:Event):void
		{			
			if(e.currentTarget as OperationSurLigneIHM)
			{
				if(e.target.data != null)
				{
					var myLigneErased	:Object = e.target.data as Object;
					var idsoustete		:int = myLigneErased.IDSOUS_TETE;
					
					var len:int = lignesSelected.length;
					
					for(var i:int = 0;i < len;i++)
					{
						if(lignesSelected[i].IDSOUS_TETE == idsoustete)
						{
							lignesSelected.removeItemAt(i);
							
							findSousTeteAndDeselect(idsoustete);
							
							(myPaginatedatagrid.dgPaginate.dataProvider as ArrayCollection).refresh();
							
							checkEtatLigne(myLigneErased, false);
							
							checkCompte(myLigneErased, false);
							
							return;
						}
					}
				}
			}
		}
		
		private function retrieveProcedure(pe:PaginationEvent):void
		{
			//getLignesOperators();
			
			var len:int = myPaginatedatagrid.copyTempListeSelecd.length
			if(len > 0)
			{
				// si il ya des lignes sélectionnées alors avant de faire la recherche, attendre à vérifier les ligne déjà séléctionnées
				if(chekEtatProcedureList(myPaginatedatagrid.copyTempListeSelecd))
				{
					getLignesOperators();
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_patienter_Des_lignes_sont_en_cours'), ResourceManager.getInstance().getString('M16', 'Alerte'), null);
			}
			else
				getLignesOperators();
		}

			//--------------------------------------------------------------------------------------------//
			//					LISTENERS EVENT PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function operatorsHandler(cmde:CommandeEvent):void
		{
			cbxOperateurs.dataProvider 	= _lignesSrv.listeOperateurs;
			
			if(_lignesSrv.listeOperateurs.length == 1)
				cbxOperateurs.selectedIndex = 0;
			else
			{
				if(_lignesSrv.listeOperateurs.length != 0)
					cbxOperateurs.prompt 		= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				else
					cbxOperateurs.prompt 		= ResourceManager.getInstance().getString('M16', 'Aucun_op_rateur');
				
				cbxOperateurs.selectedIndex = -1;
			}
		}
		
		private function lignesOperatorsHandler(cmde:CommandeEvent):void
		{
			if(_lignesSrv.listeLignesByOperateur!= null && _lignesSrv.listeLignesByOperateur.length != 0)
			{
				lignes = _lignesSrv.listeLignesByOperateur;
				
				findSousTeteAndSelected();
			}
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		private function getOperators():void
		{
			_lignesSrv.fournirListeOperateurs();
			_lignesSrv.addEventListener(CommandeEvent.LISTED_OPERATEURS, operatorsHandler);
		}		
		
		private function getLignesOperators():void
		{
			if(cbxOperateurs.selectedItem != null)
			{
				lignes.removeAll();

				var order	:String = spdClef.comboOrderBy.selectedItem.value;										//--> 'Ordre'
				var search	:int = idFiltreFromComboboxFilter(spdClef.comboRecherche.selectedItem.headerText);		//--> 'Dans' 
				var column	:int = idFiltreFromComboboxFilter(spdClef.comboTrieColonne.selectedItem.headerText);	//--> 'Trier par'
				var etat	:int = idEtatSelected();		
								
				var depart	:int = 1;
				var fin		:int = NBITEMPARPAGE;
				var eligible:String = '';
				var valWithCompte:Number = (rbgEnFacture.selectedValue) as Number;
				if(valWithCompte == 2){
					eligible = (comboEligible.selectedItem as Object).code;
				}
				
				if(myPaginatedatagrid.currentIntervalle != null)
				{
					depart 	= myPaginatedatagrid.currentIntervalle.indexDepart;
				}

				_lignesSrv.fournirListeLignesByOperateurV3(SessionUserObject.singletonSession.POOL.IDPOOL,
															cbxOperateurs.selectedItem.OPERATEURID,
															search,
															etat,
															depart,
															fin,
															column,
															order,
															spdClef.txtCle.text,
															valWithCompte,
															eligible);
				_lignesSrv.addEventListener(CommandeEvent.LISTED_LIGNESBY_OPE, lignesOperatorsHandler);
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_op_rateur_'), ResourceManager.getInstance().getString('M16', 'Alerte'), null);
		}

			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS - GESTIONS POPUPs
			//--------------------------------------------------------------------------------------------//
		
		private function selectTypeOperation():void
		{
			_popUpOperation = new TypeOperationSurLigneIHM();
			
			_popUpOperation.addEventListener(TypeOperationSurLigneIHMEvent.BT_VALIDER_CLICK_EVENT, selectTypeOperationHandler);
			_popUpOperation.addEventListener(TypeOperationSurLigneIHMEvent.BT_ANNULER_CLICK_EVENT, TypeOperationSurLigneIHM_BtAnnulerHandler);
			
			PopUpManager.addPopUp(_popUpOperation, this.parent, true);
			PopUpManager.centerPopUp(_popUpOperation);
			
			_popUpOperation.setEtatLigneOperations(_lastEtatLigne);
		}
		
		private function TypeOperationSurLigneIHM_BtAnnulerHandler(event:TypeOperationSurLigneIHMEvent):void
		{
			if(_popUpOperation!= null && _popUpOperation.hasEventListener(TypeOperationSurLigneIHMEvent.BT_ANNULER_CLICK_EVENT))
			{
				_popUpOperation.removeEventListener(TypeOperationSurLigneIHMEvent.BT_ANNULER_CLICK_EVENT, TypeOperationSurLigneIHM_BtAnnulerHandler);
			}
			if(_listeMultiCompte)	_listeMultiCompte.removeAll();
			
		}
		
		private function selectTypeOperationHandler(e:TypeOperationSurLigneIHMEvent):void
		{
			
			if(_popUpOperation!= null && _popUpOperation.hasEventListener(TypeOperationSurLigneIHMEvent.BT_VALIDER_CLICK_EVENT))
			{
				_popUpOperation.removeEventListener(TypeOperationSurLigneIHMEvent.BT_VALIDER_CLICK_EVENT, selectTypeOperationHandler);
			}
			
			dispatchEvent(new TypeOperationSurLigneIHMEvent(TypeOperationSurLigneIHMEvent.OPERATION_LIGNE_EVENT));
			
			PopUpManager.removePopUp(this);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function setEnabledDisabledCheckBoxEtat():void
		{
			if(lignesSelected == null || lignesSelected.length == 0)
			{
				attributEtatCheckBox(true, true);
			}
			else if(lignesSelected.length > 0)
			{
				switch(lignesSelected[0].IDETAT_LIGNE)
				{
					case 1 : attributEtatCheckBox(true, false);  break;
					case 2 : attributEtatCheckBox(false, true);  break;
					case 3 : attributEtatCheckBox(false, false); break;
				}
			}
		}
		
		private function attributEtatCheckBox(rdb1:Boolean, rdb2:Boolean):void
		{
			rdAct.enabled = rdb1;
			rdSus.enabled = rdb2

		}
		
		private function idFiltreFromComboboxFilter(libelle:String):int
		{
			var id:int = 0;
			
			switch(libelle)
			{
				case ResourceManager.getInstance().getString('M16','Collaborateur')	: id = 1; break;
				case ResourceManager.getInstance().getString('M16','Ligne')			: id = 2; break;
				case ResourceManager.getInstance().getString('M16','Compte')		: id = 4; break;
				case ResourceManager.getInstance().getString('M16','Sous_compte')	: id = 3; break;
				case ResourceManager.getInstance().getString('M16','Code_interne')	: id = 5; break;
			}

			return id;
		}
		
		private function idEtatSelected():int
		{
			var etat:int = 1;
			
			if(rdAct.selected)
			{
				etat = 1;
			}
			else if(rdSus.selected)
				{
					etat = 2;
				}
			
			return etat;
		}
		
		private function findSousTeteAndDeselect(idsoustete:int):void
		{
			var len:int = lignes.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(lignes[i].IDSOUS_TETE == idsoustete)
				{
					lignes[i].SELECTED = false;
					
					lignes.refresh();
					
					return;
				}
			}
		}
		
		private function findSousTeteAndSelected():void
		{
			lignesSelected.source = ObjectUtil.copy(myPaginatedatagrid.copyTempListeSelecd.source) as Array;
			var lenLignes	:int = lignes.length;
			var lenSelect	:int = lignesSelected.length;
			
				for(var j:int = 0;j < lenSelect;j++)
				{
					for(var i:int = 0;i < lenLignes;i++)
					{
						if(lignesSelected[j].IDSOUS_TETE == lignes[i].IDSOUS_TETE)
						{
							lignes[i].SELECTED = true;
							break;
						}
					}
				}
			
			lignes.refresh();
		}
		
		private function getLignesOperateur(mySelectedLigne:LignesByOperateur, isAddItem:Boolean):void
		{
			if(mySelectedLigne != null)
			{
				if(isAddItem)
				{
					if(checkEtatLigne(mySelectedLigne, isAddItem))
					{
						if( ! elementExists(mySelectedLigne, lignesSelected))
							lignesSelected.addItem(mySelectedLigne);
					}
					else
					{
						mySelectedLigne.SELECTED = false;
						
						lignes.itemUpdated(mySelectedLigne);
						
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention__nVous_ne_pouvez_pas_s_lection'), '', null);
					}
				}
				else
				{
					var len:int = lignesSelected.length;
					
					for(var i:int = 0;i < len;i++)
					{
						if(mySelectedLigne.IDSOUS_TETE == lignesSelected[i].IDSOUS_TETE)
						{
							lignesSelected.removeItemAt(i);
							
							checkEtatLigne(mySelectedLigne, isAddItem);
							
							checkCompte(mySelectedLigne, isAddItem);
							
							lignesSelected.refresh();
							
							return;
						}
					}
				}
			}
		}
		
		private function elementExists(element:LignesByOperateur, liste:ArrayCollection):Boolean
		{
			var trouv:Boolean = false
			var len:int = liste.length;
			for(var i:int = 0;i < len;i++)
			{
				if(element.IDSOUS_TETE == liste[i].IDSOUS_TETE)
				{
					trouv = true
					break;
				}
			}
			return trouv;
		}

		private function checkEtatLigne(mySelectedLigne:Object, isAddItem:Boolean):Boolean
		{
			var isSame:Boolean = true;
			
			if(isAddItem)
			{
				if(_lastEtatLigne == ETAT_LIGNE_INCONNUE)
				{
					_lastEtatLigne = mySelectedLigne.IDETAT_LIGNE;
				}
				else if(_lastEtatLigne != mySelectedLigne.IDETAT_LIGNE)
					{
						isSame = false;
					}
			}
			else
			{
				if(lignesSelected.length == 0)
					_lastEtatLigne = ETAT_LIGNE_INCONNUE;
			}
			
			return isSame;
		}
		
		private function checkCompte(mySelectedLigne:Object, isAddItem:Boolean):Boolean
		{
			var isOk:Boolean = true;
			
			if(isAddItem)
			{
				if(_lastCompte == -1)
				{
					_lastCompte = mySelectedLigne.IDCOMPTE_FACTURATION;
				}
				else if((_lastCompte == 0 && mySelectedLigne.IDCOMPTE_FACTURATION > 0) || (_lastCompte > 0 && mySelectedLigne.IDCOMPTE_FACTURATION == 0))
					{
						isOk = mySelectedLigne.SELECTED = false;
						
						lignes.refresh();
						
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention_nous_vous_invitons_a_ne_choisi'), ResourceManager.getInstance().getString('M16', 'Alerte'), null);
					}
			}
			else
			{
				if(lignesSelected.length == 0)
					_lastCompte = -1;
			}
			
			if(isOk)
				isOk = checkSousCompte(mySelectedLigne, isAddItem);
			
			return isOk;
		}
		
		private function checkSousCompte(mySelectedLigne:Object, isAddItem:Boolean):Boolean
		{
			var isOk:Boolean = true;
			
			if(isAddItem)
			{
				if(_lastSousCompte == -1)
				{
					_lastSousCompte = mySelectedLigne.IDSOUS_COMPTE;
				}
				else if((_lastSousCompte == 0 && mySelectedLigne.IDSOUS_COMPTE > 0) || (_lastSousCompte > 0 && mySelectedLigne.IDSOUS_COMPTE == 0))
					{
						isOk = mySelectedLigne.SELECTED = false;
						
						lignes.refresh();
						
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention_nous_vous_invitons_a_ne_choisi'), ResourceManager.getInstance().getString('M16', 'Alerte'), null);
					}
			}
			else
			{
				if(lignesSelected.length == 0)
					_lastSousCompte = -1;
			}
			
			return isOk;
		}

		private function chekEtatProcedure():Boolean
		{
			var isOk	:Boolean = true;
			var len		:int = lignesSelected.length;
			for(var i:int = 0; i < len;i++)
			{
				if((lignesSelected[i]).classProcEligibilite.STATUS_PROC < 2)
					isOk = false;
			}
			
			return isOk;
		}
		
		private function chekEtatProcedureList(arrList:ArrayCollection):Boolean
		{
			var isOk	:Boolean = true;
			var len		:int = arrList.length;
			for(var i:int = 0; i < len;i++)
			{
				if((arrList[i]).classProcEligibilite.STATUS_PROC < 2)
					isOk = false;
			}
			
			return isOk;
		}
		
		private function checkLignesSelected():Boolean
		{
			var isOk:Boolean = true;
			
			if(cbxOperateurs.selectedItem == null)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_op_rateur_'), ResourceManager.getInstance().getString('M16', 'Alerte'), null);
				return false;
			}
			else
				operateurSelected = cbxOperateurs.selectedItem;
			
			if(lignesSelected.length == 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_au_moins_une_ligne'), ResourceManager.getInstance().getString('M16', 'Alerte'), null);
				return false;
			}
			else
			{
				if(_lastEtatLigne == ETAT_LIGNE_RESILIEE)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Attention___Il_est_impossible_d_effectuer_une_'), ResourceManager.getInstance().getString('M16', 'Alerte'), null);
					return false;
				}
				else
				{
					SessionUserObject.singletonSession.ETATLIGNERESSUSMOD = _lastEtatLigne;
					
					if(_lastCompte == 0 || _lastSousCompte == 0)
						isNoCompte = true;
					else
						isNoCompte = false;

					//---ICI
					var myComptesObject:Object = getCompteSousCompte(lignesSelected, 'IDCOMPTE_FACTURATION');
					
					arrayComptes   = myComptesObject.LISTE;
					isMultiComptes = myComptesObject.MULTI;
					
					var mySousComptesObject:Object = getCompteSousCompte(lignesSelected, 'IDSOUS_COMPTE');
					
					arraySousComptes   = mySousComptesObject.LISTE;
					isMultiSousComptes = mySousComptesObject.MULTI;
					
					var lenMulti:int = _listeMultiCompte.length;
					
					comptesSousComptes = new ArrayCollection();
					
					
					SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS = new ArrayCollection();
					SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS = ObjectUtil.copy(lignesSelected) as ArrayCollection;
					
					for(var i:int = 0;i < lenMulti;i++)
					{
						comptesSousComptes.addItem(getMultiSousCompte(_listeMultiCompte[i], _listeMultiCompte[i].LIGNES));
					}
				}				
			}
			
			return isOk;
		}
		
		private function getCompteSousCompte(values:ICollectionView, labelField:String):Object
		{			
			var liste	:ArrayCollection = new ArrayCollection();
			var facture	:ICollectionView = ObjectUtil.copy(lignesSelected) as ICollectionView;
			var object	:Object = new Object();
			var multi	:MultiCompte;
			
			facture = Formator.sortFunction(facture as ArrayCollection, labelField) as ICollectionView;
			
			object.MULTI = false;;
			
			if(facture != null)
			{
				var cursor:IViewCursor = facture.createCursor();
				
				while(!cursor.afterLast)
				{
					var integer:int = ConsoviewUtil.getIndexById(liste, labelField, cursor.current[labelField])
					
					if(integer < 0)
					{
						liste.addItem(cursor.current);
						
						if(labelField == 'IDCOMPTE_FACTURATION')
						{
							multi = new MultiCompte();
							multi.COMPTE_FACTURATION 	= cursor.current.COMPTE_FACTURATION;
							multi.IDCOMPTE_FACTURATION	= cursor.current.IDCOMPTE_FACTURATION;
							multi.NUMBER_OF_LINES 		= multi.LIGNES.length;
							multi.LIGNES.addItem(cursor.current);
							
							_listeMultiCompte.addItem(multi);							
						}
					}
					else
					{
						if(labelField == 'IDCOMPTE_FACTURATION')
							multi.LIGNES.addItem(cursor.current);
					}
					
					cursor.moveNext();
				}
				
				if(liste.length > 1)
					object.MULTI = true;
			}
			
			object.LISTE = liste;
			
			return object;
		}
		
		
		private function getMultiSousCompte(object:Object, compte:ICollectionView):Object
		{
			var liste		:ArrayCollection = new ArrayCollection();
			var multi		:MultiSousCompte;
			var lblField	:String = 'IDSOUS_COMPTE';
			
			compte = Formator.sortFunction(compte as ArrayCollection, lblField) as ICollectionView;
			
			if(compte != null)
			{
				var cursor:IViewCursor = compte.createCursor();
				
				while(!cursor.afterLast)
				{
					var integer:int = ConsoviewUtil.getIndexById(liste, lblField, cursor.current[lblField])
					
					if(integer < 0)
					{
						if(cursor.current.IDSOUS_COMPTE > -1)
						{
							multi = new MultiSousCompte();
							
							multi.SOUS_COMPTE 			= cursor.current.SOUS_COMPTE;
							multi.IDSOUS_COMPTE			= cursor.current.IDSOUS_COMPTE;
							multi.LIGNES.addItem(cursor.current);
							
							object.MULTI_SOUS_COMPTE.addItem(multi);
							
							liste.addItem(cursor.current);
						}
					}
					else
						multi.LIGNES.addItem(cursor.current);					
					
					cursor.moveNext();
				}
			}
			
			return object;
		}
		
		protected function fraisConvertor(item:Object, column:DataGridColumn):String  
		{  
			var frais:String = '';

			if(item != null && item.FPC != null ){
				var diffMonth:int = monthDifference(new Date(), item.FPC);
				if(diffMonth > 0)
				{
					frais = Formator.formatTotalWithSymbole(diffMonth * item.MONTANT_MENSUEL_PEN + item.FRAIS_FIXE_RESILIATION);
				}
			}
			return frais;
		}
		
		private function monthDifference(start:Date, end:Date):int {
			return (end.getUTCFullYear() - start.getUTCFullYear()) * 12 + (end.getUTCMonth() - start.getUTCMonth());
		} 

	}
}