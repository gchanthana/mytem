package service.destinataire.ajoutermodifierdestinataire
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import vo.destinataire.Destinataire;
	

	public class AjouterModifierDestinataireService 
	{
		private var _model:AjouterModifierDestinataireModel;
		public var handler:AjouterModifierDestinataireHandler;	
		
		public function AjouterModifierDestinataireService()
		{
			this._model  = new AjouterModifierDestinataireModel();
			this.handler = new AjouterModifierDestinataireHandler(model);
		}		
		
		public function get model():AjouterModifierDestinataireModel
		{
			return this._model;
		}
		
		public function ajouterDestinataire(destinataire :Destinataire):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.IUDDestinataireService","ajouterModifierDestinataire",handler.ajouterModifierResultHandler); 
			
			RemoteObjectUtil.callService(op,destinataire);
		}
		
		public function supprimerDestinataire(id:int):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.IUDDestinataireService","supprimerDestinataire",handler.supprimerResultHandler); 
			
			RemoteObjectUtil.callService(op,id); 
		}
		
		public function supprimerPlusieursDestinataire(idDest:String):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.IUDDestinataireService","supprimerPlusieursDestinataire",handler.supprimerPlusieursResultHandler); 
			
			RemoteObjectUtil.callService(op,idDest); 
		}
		
	}
}