<!--- =========================================================================
Classe: Facture
Auteur: Cedric RAPIERA
Last Modification : Intégration des factures générées par BI Publisher
========================================================================== --->

<cfcomponent name="Facture" alias="fr.consotel.consoview.Facture.Facture" extends="fr.consotel.consoview.Facture.AbstractFacture">
	<!--- 
	<cffunction name="setTypeFactureStrategy" access="public" returntype="void" output="false">
		<cfargument name="type_facture" required="true" type="string"/>
		<cfargument name="type_perimetre" required="true" type="string"/>
		<cfset variables.instance.typeFactureStrategy=createObject("component","TypeFacture#type_facture##type_perimetre#Strategy")>
	</cffunction>
	
	<cffunction name="setIDINVENTAIRE_PERIODE" access="public" output="false">
		<cfargument name="IDINVENTAIRE_PERIODE" type="numeric" required="true"/>
		<cfset variables.instance.IDINVENTAIRE_PERIODE = arguments.IDINVENTAIRE_PERIODE/>
	</cffunction>
	 --->
	
	<cffunction name="getFactureTva" access="public" returntype="query" output="true">
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">
        <cfset q = variables.instance.typeFactureStrategy.getFactureTva(IDINVENTAIRE_PERIODE,IDPerimetre)>
		<cfreturn q>
	</cffunction>
	
	<cffunction name="afficherFacture" access="public" returntype="void" output="true" hint="Permet d'afficher la facture">
		<cfargument name="ID" required="false" type="string" hint="ID Site ou ID sous tete (Ligne) pas nécessaire pour Complète">
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfset ConstruireGabarit(getFactDataset(variables.instance.IDINVENTAIRE_PERIODE,ID,IDPerimetre))>
	</cffunction>
	
	<cffunction name="afficherFactureCat" access="public" returntype="void" output="true" hint="Permet d'afficher la facture">
		<cfargument name="ID" required="false" type="string" hint="ID Site ou ID sous tete (Ligne) pas nécessaire pour Complète">
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfargument name="factureParams" required="true" type="struct">
		<cfset ConstruireGabaritCat(getFactDatasetCat(variables.instance.IDINVENTAIRE_PERIODE,ID,IDPerimetre),
				factureParams)>
	</cffunction>

	<cffunction name="getFactureInfos" access="public" returntype="query" output="true">
		<cfargument name="idInvPeriod" required="true" type="numeric">
        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLOBAL.fa_info_operateur">
	        <cfprocparam  value="#idInvPeriod#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qFactureInfos">
        </cfstoredproc>
		<cfreturn qFactureInfos>
	</cffunction>
	
	<cffunction name="ConstruireGabaritCat" access="public" returntype="void" output="true">
		<cfargument name="dataset" required="true" type="query">
		<cfargument name="factureParams" required="true" type="struct">
		<cfabort showerror="Erreur: Classe Abstraite">
	</cffunction>

	<cffunction name="ConstruireGabarit" access="public" returntype="void" output="false"
				hint="Cette classe construit le tableau (gabarit de la facture) à partir des données passées en paramètres (query)">
		<cfargument name="dataset" required="true" type="query">
		<cfabort showerror="Erreur: Classe Abstraite">
	</cffunction>

	<cffunction name="getFactDataset" access="private" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="" />
		<cfargument name="ID" required="false" type="string" hint="ID Site ou ID sous tete pas nécessaire pour complète">
		<cfargument name="IDPerimetre" required="false" type="numeric">		
		<cfreturn variables.instance.typeFactureStrategy.getData(IDINVENTAIRE_PERIODE,ID,IDPerimetre)>
	</cffunction>
	
	<cffunction name="getFactDatasetCat" access="private" returntype="Query" output="true">
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="" />
		<cfargument name="ID" required="false" type="string" hint="ID Site ou ID sous tete pas nécessaire pour complète">
		<cfargument name="IDPerimetre" required="false" type="numeric">
		<cfreturn variables.instance.typeFactureStrategy.getDataCat(IDINVENTAIRE_PERIODE,IDPerimetre)>
	</cffunction>
	
	<cffunction name="setData" access="remote" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="q" type="query">
		<cfif IsDefined('session.dataFacture')>
			<cfset structDelete(session,"dataFacture")>
		</cfif>
		<cfset session.dataFacture=q>
	</cffunction>
	
	<cffunction name="setSigneDevise" access="public" description="Va chercher la devise de la facture" returntype="void">
		<cfargument name="p_idinventaire_periode" required="true" type="numeric">
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_M00.getFactureDevise">
			<cfprocparam value="#p_idinventaire_periode#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfquery name="q_retour" datasource="ROCOFFRE">
				SELECT * 
				FROM devise d 
				WHERE d.iddevise = #p_retour.DEVISE#
		</cfquery>
			
		<cfset variables.instance.signeDevise = q_retour.SYMBOL>
		<cfset variables.instance.shortDevise = q_retour.SHORT_DESC>
	</cffunction>
	
	<cffunction name="getSigneDevise" access="public" description="Retourne la devise de la facture enregistrée" returntype="any">
		<cfreturn variables.instance.signeDevise>
	</cffunction>
	
	<cffunction name="getShortDevise" access="public" description="Retourne la devise de la facture enregistrée" returntype="any">
		<cfreturn variables.instance.shortDevise>
	</cffunction>
	
</cfcomponent>
