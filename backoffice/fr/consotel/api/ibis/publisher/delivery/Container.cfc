<!--- TODO : Modifier les paramètres FTP par défaut : Serveur, Login, Pwd, Répertoire, etc... (Container) --->
<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.Container" extends="fr.consotel.api.ibis.publisher.delivery.ContainerHandler"
hint="Stratégie identifiée par le type Container permettant de générer un reporting dans le container d'un utilisateur ConsoView
Les procédures stockées container nécessitent l'existence d'un datasource ROCOFFRE configuré sur le BackOffice
Les fichiers diffusés dans le container ConsoView sont générés sur le FTP pelican.consotel.fr dans le répertoire de l'utilisateur container
Le fichier qui est généré en FTP porte un nom unique qui est un UUID. Il est différent du nom sous lequel le fichier sera visible dans le container
Chaque exécution d'un reporting utilisant cette stratégie peut etre identifiée de manière unique par la valeur de la propriétés FILE_NAME
Cette stratégie ne peut pas etre utilisée pour effectuer le traitement d'un reporting généré par une autre
Si l'évènement passé à handleReportingEvent() est de type fr.consotel.api.ibis.publisher.handler.event.DelegateEvent : Une exception est levée 
Les propriétés suivantes doivent etre retournée par la méthode getDeliveryProperties() de la stratégie utilisée pour l'exécution du reporting :
- FICHIER : Nom sous lequel le fichier sera visible dans le container ConsoView
- FILE_EXT : Extension du fichier à générer (sans le séparateur point)
- USERID : Identifiant de l'utilisateur ConsoView
- IDRACINE : Identifiant du groupe utilisé pour les notifications container
Les propriétés suivantes sont facultatives :
- CODE_APP : Code App utilisé pour les notifications container (Par défaut : 1)
Lorsque les propriétés USERID et IDRACINE ne sont pas fournies ou non renseignée : Leur valeurs sont récupérées dans le scope SESSION ConsoView
Les propriétés suivantes sont ignorées : FTP_SERVER, FTP_USER, FTP_PWD, SFTP, FTP_DIR, FILE_NAME (ContainerHandler)
L'évènement DelegateEvent dispatché par cette stratégie est de type fr.consotel.api.ibis.publisher.handler.event.ContainerEvent
L'implémentation IDeliveryEvent utilisée est de type fr.consotel.api.ibis.publisher.handler.event.FtpEvent">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IDelivery">
		<cfset SUPER.createInstance()>
		<!--- Propriétés FTP spécifiques à cette implémentation (Propriétés par défaut de la classe parent) --->
		<cfset structAppend(VARIABLES.FTP,{SERVER="pelican.consotel.fr", USER="container", PWD="container", SFTP=FALSE, DIR=""},TRUE)>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getDeliveryProperties" returntype="Struct"
	hint="Récupère la structure retournée par la méthode de la classe parent et remplace les valeur des propriétés suivantes avant de la retourner :
	FTP_SERVER, FTP_USER, FTP_PWD, SFTP, FTP_DIR">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var deliveryProperties=SUPER.getDeliveryProperties(reportingImpl)>
		<!--- Remplace la valeur des propriétés suivantes : FTP_SERVER, FTP_USER, FTP_PWD, SFTP, FTP_DIR --->
		<cfset structAppend(deliveryProperties,{
			FTP_SERVER=VARIABLES.FTP.SERVER, FTP_USER=VARIABLES.FTP.USER, FTP_PWD=VARIABLES.FTP.PWD,
			SFTP=VARIABLES.FTP.SFTP, FTP_DIR=VARIABLES.FTP.DIR
		},TRUE)>
		<cfreturn deliveryProperties>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion --->
	<cffunction access="public" name="dispatchEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.ContainerEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.ContainerEvent">
	</cffunction>
	
	<cffunction access="private" name="deliveryEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.ContainerEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.ContainerEvent">
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.ContainerHandler --->
	<cffunction access="public" name="getContainerClass" returntype="String" hint="Retourne le type : fr.consotel.api.ibis.ext.ExtensionApiContainerV1">
		<cfreturn "fr.consotel.api.ibis.ext.ExtensionApiContainerV1">
	</cffunction>
	
	<cffunction access="public" name="getCodeApp" returntype="Numeric" hint="Retourne la valeur 1">
		<cfreturn 1>
	</cffunction>

	<cffunction access="public" name="getProcessId" returntype="Numeric" hint="Retourne la valeur 21">
		<cfreturn 21>
	</cffunction>

	<cffunction access="private" name="storeContainerProperties" returntype="void"
	hint="Stocke les paramètres du reporting retournés par getReportingParameters() et ayant la propriété VISIBLE à TRUE
	Un paramètre n'est pas stocké si une des propriétés suivantes n'est pas fournie :  VISIBLE, LABEL
	La méthode utilisée est container.ADDparamClient() et le type utilisé par défaut pour chaque paramètre est : string
	Une exception est levée si l'une des conditions suivantes est vérifiée : 
	- Un paramètre n'a pas pu etre stocké avec container.ADDparamClient()
	- Une exception a été capturée. (Elle est redéclenchée)">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var apiContainer=ARGUMENTS["container"]>
		<cfset var UUID=apiContainer.GETUuid()>
		<!--- Paramètres du reporting spécifiques au rapport BIP correspondant --->
		<cfset var nbVisibleParam=0>
		<cfset var nbAddedParam=0>
		<cfset var reportingParams=getReportingParameters(reportingImpl)>
		<cfloop item="param" collection="#reportingParams#">
			<cfif structKeyExists(reportingParams[param],"LABEL") AND structKeyExists(reportingParams[param],"VISIBLE") AND reportingParams[param]["VISIBLE"]>
				<cfset var addParamStatus = -1>
				<cfset var paramValues=arrayToList(reportingParams[param]["VALUES"],"/")>
				<cfset nbVisibleParam=nbVisibleParam + 1>
				<cftry>
					<cfset addParamStatus=apiContainer.ADDparamClient(UUID,"string",TRIM(reportingParams[param]["LABEL"]),paramValues)>
					<cfcatch type="any">
						<cfrethrow>
					</cfcatch>
				</cftry>
				<!--- Si le paramètre n'a pas pu etre stocké --->
				<cfif addParamStatus LT 0>
					<cfthrow attributecollection="#getErrorProperties()#"
						message="Le paramètre #param# n'a pas pu etre stocké avec la méthode ADDparamClient() de container"
						detail="UUID correspondant : #UUID#. Valeur de ADDparamClient() est : #addParamStatus#">
				</cfif>
				<cfset nbAddedParam=nbAddedParam+addParamStatus>
			</cfif>
		</cfloop>
		<!--- Si un des paramètres n'a pas été ajouté : La notification correspondante est envoyée et une exception est levée --->
		<cfif nbAddedParam NEQ nbVisibleParam>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="Tous les paramètres spécifiée comme visibles dans le module container n'ont pas été stocké avec la méthode ADDparamClient() de container"
				detail="UUID correspondant : #UUID#. Nbre de paramètre stockés : #nbAddedParam#">
		</cfif>
	</cffunction>

	<cffunction access="private" name="containerAvantExecutionError" returntype="void" hint="Cette méthode n'est pas implémentée
	Appelée par containerAvantExecution() lorsque la valeur retournée par CONTAINER.ADDDocumentTableContainer() est invalide">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Provient de containerAvantExecution()">
		<cfset var apiContainer=ARGUMENTS["container"]>
		<cfset var deliveryProperties=ARGUMENTS["deliveryProps"]>
		<cfset var UUID=apiContainer.GETUuid()>
		<cfset var MAIL_API=getMailProperties()>
		
		
		
		<cfsavecontent variable="body">
		   <cfoutput>
				<b>Le traitement container requis avant l'exécution du reporting a été invalidé en raison d'une valeur invalide retournée par une procédure stockée<br>
				Une exception a été levée et aboutira à l'envoi d'une notifiaction mail supplémentaire contenant l'exception correspondante (avec le meme UUID)</b>
				<br><br>
				UUID : #UUID#<br>
				Composant CONTAINER : #getContainerClass()#<br><br>
				<b>Propriétés concernant l'exécution correspondante :</b><br>
				Nom sous lequel le fichier est visible dans le module container : #deliveryProperties["FICHIER"]#<br>
				APP_LOGINID : #deliveryProperties["USERID"]#<br>
				IDRACINE : #deliveryProperties["IDRACINE"]#<br>
				CODE_APP : #deliveryProperties["CODE_APP"]#<br>
				JOB_ID (Valeur à ce stade de l'exécution) : -1<br>
				SHORT_NAME : #REPLACE(deliveryProperties["FICHIER"]," ","_","all")#<br>
				FORMAT : #deliveryProperties["FILE_EXT"]#<br>
				BI_SERVER : #apiContainer.getBipServerParamValue()#<br>
				CODE_RAPPORT : C<br>
				NOM_MODULE : M<br>
				APPLI : A<br> 
				IS_PUBLIC : 0<br> 
				STATUS  : 1<br> 
				TAILLE : -1<br> 
				TAILLEZIP : -1<br>
			</cfoutput>
		</cfsavecontent>
		
		<cftry>
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(13)>
			<cfset mLog.setIDMNT_ID(41)>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		</cftry>
		
		
		
		
		<!--- <cfmail attributecollection="#MAIL_API.ERROR#" priority="highest" subject="Diffusion container non effectuée (Avant Exécution) : #UUID#">
			<cfoutput>
				<b>Le traitement container requis avant l'exécution du reporting a été invalidé en raison d'une valeur invalide retournée par une procédure stockée<br>
				Une exception a été levée et aboutira à l'envoi d'une notifiaction mail supplémentaire contenant l'exception correspondante (avec le meme UUID)</b>
				<br><br>
				UUID : #UUID#<br>
				Composant CONTAINER : #getContainerClass()#<br><br>
				<b>Propriétés concernant l'exécution correspondante :</b><br>
				Nom sous lequel le fichier est visible dans le module container : #deliveryProperties["FICHIER"]#<br>
				APP_LOGINID : #deliveryProperties["USERID"]#<br>
				IDRACINE : #deliveryProperties["IDRACINE"]#<br>
				CODE_APP : #deliveryProperties["CODE_APP"]#<br>
				JOB_ID (Valeur à ce stade de l'exécution) : -1<br>
				SHORT_NAME : #REPLACE(deliveryProperties["FICHIER"]," ","_","all")#<br>
				FORMAT : #deliveryProperties["FILE_EXT"]#<br>
				BI_SERVER : #apiContainer.getBipServerParamValue()#<br>
				CODE_RAPPORT : C<br>
				NOM_MODULE : M<br>
				APPLI : A<br> 
				IS_PUBLIC : 0<br> 
				STATUS  : 1<br> 
				TAILLE : -1<br> 
				TAILLEZIP : -1<br>
			</cfoutput>
		</cfmail>
		 --->
		
	</cffunction>

	<cffunction access="private" name="containerApresExecutionError" returntype="void" hint="Cette méthode n'est pas implémentée
	Appelée par containerApresExecution() lorsque la valeur retournée par une des méthodes suivantes est invalide :
	CONTAINER.ADDJobIdToDocument() ou CONTAINER.setCodeProcessForJOBID()">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Provient de containerApresExecution()">
		<cfargument name="jobId" type="Numeric"  required="true" hint="Provient de containerApresExecution()">
		<cfset var apiContainer=ARGUMENTS["container"]>
		<cfset var deliveryProperties=ARGUMENTS["deliveryProps"]>
		<cfset var JOB_ID=ARGUMENTS["jobId"]>
		<cfset var UUID=apiContainer.GETUuid()>
		<cfset var MAIL_API=getMailProperties()>
		
		
		
		<cfsavecontent variable="body">
			<cfoutput>
				<b>Le traitement container requis après l'exécution du reporting a été invalidé en raison d'une valeur invalide retournée par une procédure stockée<br>
				Une exception a été levée et aboutira à l'envoi d'une notifiaction mail supplémentaire contenant l'exception correspondante (avec le meme UUID)</b>
				<br><br>
				UUID : #UUID#<br>
				Composant CONTAINER : #getContainerClass()#<br><br>
				<b>Propriétés concernant l'exécution correspondante :</b><br>
				JOBID : #JOB_ID#<br>
				PROCESSID : #getProcessId()#
			</cfoutput>
		</cfsavecontent>
		
		<cftry>
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(13)>
			<cfset mLog.setIDMNT_ID(42)>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		</cftry>
		
		
		<!---
		<cfmail attributecollection="#MAIL_API.ERROR#" priority="highest" subject="Diffusion container non effectuée (Après Exécution) : #UUID#">
			<cfoutput>
				<b>Le traitement container requis après l'exécution du reporting a été invalidé en raison d'une valeur invalide retournée par une procédure stockée<br>
				Une exception a été levée et aboutira à l'envoi d'une notifiaction mail supplémentaire contenant l'exception correspondante (avec le meme UUID)</b>
				<br><br>
				UUID : #UUID#<br>
				Composant CONTAINER : #getContainerClass()#<br><br>
				<b>Propriétés concernant l'exécution correspondante :</b><br>
				JOBID : #JOB_ID#<br>
				PROCESSID : #getProcessId()#
			</cfoutput>
		</cfmail>
		--->
		
	</cffunction>
	
	<cffunction access="private" name="containerFinExecutionError" returntype="void"
	hint="Diffuse une notification mail concernant le traitement correspondant puis appelle la méthode de la classe parent">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.ContainerEvent" required="true" hint="Provient de containerFinExecution()">
		<cfset var apiContainer=ARGUMENTS["container"]>
		<cfset var containerEvent=ARGUMENTS["event"]>
		<cfset var UUID=containerEvent.getFileUUID()>
		<cfset var JOB_ID=containerEvent.getJobId()>
		<cfset var MAIL_API=getMailProperties()>
		
		
		
		
		
		<cfsavecontent variable="body">
		   <cfoutput>
				<b>Le traitement container requis à la fin de l'exécution du reporting a été invalidé en raison d'une valeur invalide retournée SOIT par une procédure stockée
				SOIT par un composant de type fr.consotel.process.processXXX<br>
				Une exception a été levée et aboutira à l'envoi d'une notifiaction mail supplémentaire contenant l'exception correspondante (avec le meme UUID)</b>
				<br><br>
				UUID : #UUID#<br>
				Composant CONTAINER : #getContainerClass()#<br><br>
				<b>Propriétés concernant l'exécution correspondante : (Permettant entre autres de retrouver la valeur XXX)</b><br>
				JOBID : #JOB_ID#<br>
				Statut container : #containerEvent.getContainerStatus()#<br>
				BI_SERVER : #apiContainer.getBipServerParamValue()#<br><br>
				#containerEvent.getDeliveryInfos()#
			</cfoutput>
		</cfsavecontent>
		
		<cftry>
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(13)>
			<cfset mLog.setIDMNT_ID(43)>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		</cftry>
		
		
		<!--- 

		<cfmail attributecollection="#MAIL_API.ERROR#" priority="highest" subject="Diffusion container non effectuée (Fin Exécution) : #UUID#">
			<cfoutput>
				<b>Le traitement container requis à la fin de l'exécution du reporting a été invalidé en raison d'une valeur invalide retournée SOIT par une procédure stockée
				SOIT par un composant de type fr.consotel.process.processXXX<br>
				Une exception a été levée et aboutira à l'envoi d'une notifiaction mail supplémentaire contenant l'exception correspondante (avec le meme UUID)</b>
				<br><br>
				UUID : #UUID#<br>
				Composant CONTAINER : #getContainerClass()#<br><br>
				<b>Propriétés concernant l'exécution correspondante : (Permettant entre autres de retrouver la valeur XXX)</b><br>
				JOBID : #JOB_ID#<br>
				Statut container : #containerEvent.getContainerStatus()#<br>
				BI_SERVER : #apiContainer.getBipServerParamValue()#<br><br>
				#containerEvent.getDeliveryInfos()#
			</cfoutput>
		</cfmail>
		 --->
		 
		<!--- Traitement de la méthode parent --->
		<cfset SUPER.containerFinExecutionError(apiContainer,containerEvent)>
	</cffunction>
</cfcomponent>