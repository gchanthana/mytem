package vo.destinataire
{
	// pour faire le mapping entre cette class et le composant coldfuison (entre deux objets)
	[RemoteClass(alias="fr.consotel.consoview.M331.service.destinataires.AjouterModifierProfilService")]
	
	[Bindable]
	public class Profil
	{
		private var _idProfil:int;
		private var _nomProfil:String;
		private var _dateCreation:Date;
		private var _commentaire:String;
		private var _dateModification:Date;
		private var _codeInterne:String;
		private var _assign:int;
		
		public function Profil()
		{
		}

		public function fill(unProfil:Object):void
		{
			this._idProfil=unProfil.IDPROFIL_ANNUAIRE  ;
			
			if(unProfil.IDPROFIL_ANNUAIRE==null)
			{
				this._idProfil=0 ;
			}
			
			this._nomProfil=unProfil.NOM_PROFIL;
			this._dateCreation=unProfil.DATE_CREATION;
			this._dateModification=unProfil.DATE_MODIF;
			this._commentaire=unProfil.COMMENTAIRE_PROFIL ;
			this._codeInterne=unProfil.CODE_INTERNE;
			this._assign=unProfil.ASSIGN;
		}
		
		public function get idProfil():int
		{
			return _idProfil;
		}
		
		public function set idProfil(value:int):void
		{
			_idProfil = value;
		}
		
		public function get nomProfil():String
		{
			return _nomProfil;
		}
		
		public function set nomProfil(value:String):void
		{
			_nomProfil = value;
		}
		
		public function get dateCreation():Date
		{
			return _dateCreation;
		}
		
		public function set dateCreation(value:Date):void
		{
			_dateCreation = value;
		}
		
		
		public function get codeInterne():String
		{
			return _codeInterne;
		}
		
		public function set codeInterne(value:String):void
		{
			_codeInterne = value;
		}
		
		public function get dateModification():Date
		{
			return _dateModification;
		}
		
		public function set dateModification(value:Date):void
		{
			_dateModification = value;
		}
		
		public function get commentaire():String
		{
			return _commentaire;
		}
		
		public function set commentaire(value:String):void
		{
			_commentaire = value;
		}
		public function get assign():int
		{
			return _assign;
		}
		
		public function set assign(value:int):void
		{
			_assign = value;
		}

	}
}