﻿<cfcomponent displayname="proxyConsoView" hint="Proxy pour Application Mobile" output="true" namespace="http://cv.consotel.fr">
	
	<cfheader name="Access-Control-Allow-Origin" value="*">
		
	<cffunction access="remote" name="validateLogin" returntype="Struct" output="true">
		<cfargument name="username" type="string"  required="true">
		<cfargument name="password" type="string"  required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfreturn session.user>
		<cfelse>
			<cfreturn structNew()>
		</cfif>
	</cffunction>
	
	<cffunction access="remote" name="getSession" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfset w=obj.getGroupList(v.clientaccessid)>
		<cfset idperimetre=w.idgroupe_client[1]>
		<cfset iduser=v.clientaccessid>
		<cfset t=obj.connectOnGroup(iduser,idperimetre)>
		<cfreturn session>
	</cffunction>
	
	<cffunction access="remote" name="getAccueilData1" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfset w=obj.getGroupList(v.clientaccessid)>
		<cfset idperimetre=w.idgroupe_client[1]>
		<cfset iduser=v.clientaccessid>
		<cfset t=obj.connectOnGroup(iduser,idperimetre)>
		<cfobject component="fr.consotel.consoview.M511.PerimetreE0" type="component" name="ob1">
		<cfset a=ob1.setUpPerimetre(idperimetre)>
		<cfobject component="fr.consotel.consoview.M511.AccueilService" type="component" name="d1">
		<cfset ds1=d1.getData1()>
		<cfreturn ds1>
	</cffunction>
	
	<cffunction access="remote" name="getTBpage1" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfset w=obj.getGroupList(v.clientaccessid)>
		<cfset idperimetre=w.idgroupe_client[1]>
		<cfset iduser=v.clientaccessid>
		<cfset t=obj.connectOnGroup(iduser,idperimetre)>
		<cfset s=t.perimetre_data.perimetre_infos.periods>
		<cfset datefin=LsDateFormat(dateadd("d",-LsDateFormat(s.facture_fin,"dd"),s.facture_fin),"dd/mm/yyyy")>
		<cfset datedeb=LsDateFormat(dateadd("m",-6,s.facture_fin),"01/mm/yyyy")>
		<cfset type_perimetre=t.perimetre_data.perimetre_infos.type_perimetre>
		<cfobject component="fr.consotel.consoview.M311.accueil.facade" type="component" name="obj2">
		<cfset u=obj2.init(type_perimetre,"Complet",idperimetre,datedeb,datefin,"Complet")>
		<cfset v=obj2.getSurThemeAbos()>
		<cfreturn v>
	</cffunction>
	
	<cffunction access="remote" name="searchEmploye" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="chaine" type="string" required="true">
		<cfset chaine = replace(chaine,'%','')>
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfset idracine=v.clientid>
			<cfquery name="qSearch" datasource="ROCOFFRE">
				select e.idemploye, st.idsous_tete, e.prenom || ' ' || e.nom as patronyme, e.email, 
						gc.libelle_groupe_client, st.sous_tete
				from employe e, groupe_client gc, groupe_client_ref_client gcrc, sous_tete st
				where e.idracine=#idracine#
				and (lower(e.nom) like lower('%#chaine#%') 
				    or lower(e.prenom) like lower('%#chaine#%')
				    or lower(e.email) like lower('%#chaine#%')
				    or lower(st.sous_tete) = lower('#chaine#'))
				and gc.idemploye=e.idemploye
				and gc.idgroupe_client=gcrc.idgroupe_client
				and gcrc.idref_client=st.idsous_tete
			</cfquery>
			<cfreturn qSearch>
		<cfelse>
			<cfreturn queryNew("error")>
		</cfif>
	</cffunction>
	
	<cffunction access="remote" name="getFicheEmploye" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="idemploye" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfset idracine=v.clientid>
			<cfquery name="qSearch" datasource="ROCOFFRE">
				select e.prenom || ' ' || e.nom as patronyme, e.*, gc.libelle_groupe_client, st.sous_tete
				from employe e, groupe_client gc, groupe_client_ref_client gcrc, sous_tete st
				where e.idracine=#idracine#
				and e.idemploye=#idemploye#
				and gc.idemploye=e.idemploye
				and gc.idgroupe_client=gcrc.idgroupe_client
				and gcrc.idref_client=st.idsous_tete
			</cfquery>
			<cfreturn qSearch>
		<cfelse>
			<cfreturn queryNew("error")>
		</cfif>
	</cffunction>
	
	<cffunction access="remote" name="getFicheEmployeByLogin" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfset idracine=v.clientid>
			<cfquery name="qPeriode" datasource="ROCOFFRE">
				select max(ip.idperiode_mois) idperiode
				            from employe e, 
				                 groupe_client gc, 
				                 groupe_client_ref_client gcrc, 
				                 sous_tete st, 
				                 st_op so, 
				                 sous_compte sco, compte_facturation cf, inventaire_periode ip
				            where e.idracine=#idracine#
			            	and lower(e.email)=lower('#username#')
				            and e.idemploye=gc.idemploye
				            and gc.idgroupe_client=gcrc.idgroupe_client
				            and gcrc.idref_client=st.idsous_tete
				            and st.idsous_tete = so.idsous_tete
				            and so.idsous_compte = sco.idsous_compte
				            and sco.idcompte_facturation = cf.idcompte_facturation
				            and cf.idcompte_facturation = ip.idcompte_facturation
				            and ip.idracine_master = #idracine#
			</cfquery>
			<cfset vPeriode = qPeriode.idperiode>
			<cfquery name="qSearch" datasource="ROCOFFRE">
				select * 
				from
				(
				select decode(substr(st.sous_tete,0,1),0,substr(st.sous_tete,1,2)||' '||substr(st.sous_tete,3,2)
				       ||' '||substr(st.sous_tete,5,2)||' '||substr(st.sous_tete,7,2),st.sous_tete)||' '||substr(st.sous_tete,9,2) ligne,
				       st.sous_tete, 
				       e.prenom || ' ' || e.nom as patronyme, tp.type_theme, sum(fa.montant) montant
				        from employe e, groupe_client gc, groupe_client_ref_client gcrc, sous_tete st, 
				            e0.facturation fa, produit_catalogue pca, theme_produit_catalogue tpc, theme_produit tp
				        where e.idracine=#idracine#
				        and fa.idracine_master = #idracine#
				        and fa.idperiode_mois = #vPeriode#
				        and lower(e.email)=lower('#username#')
				        and gc.idemploye=e.idemploye
				        and gc.idgroupe_client=gcrc.idgroupe_client
				        and gcrc.idref_client=st.idsous_tete
				        and fa.idsous_tete = st.idsous_tete
				        and fa.idproduit_catalogue = pca.idproduit_catalogue
				        and pca.idproduit_catalogue = tpc.idproduit_catalogue
				        and tpc.idtheme_produit = tp.idtheme_produit
				group by decode(substr(st.sous_tete,0,1),0,substr(st.sous_tete,1,2)||' '||substr(st.sous_tete,3,2)
				       ||' '||substr(st.sous_tete,5,2)||' '||substr(st.sous_tete,7,2),st.sous_tete)||' '||substr(st.sous_tete,9,2) ,
				       st.sous_tete, 
				       e.prenom || ' ' || e.nom, tp.type_theme)
				PIVOT (sum(montant) for type_theme in ('Abonnements' Abonnements , 'Consommations' Consommations))
			</cfquery>
			<cfreturn qSearch>
		<cfelse>
			<cfreturn queryNew("error")>
		</cfif>
	</cffunction>
	
	<cffunction access="remote" name="getRevendeurs" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="ligne" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfset idracine=v.clientid>
			<cfquery name="qSearch" datasource="ROCOFFRE">
				SELECT login_email,al.login_nom,al.login_prenom
				FROM sous_tete st,pool_lignes PL,pool_flotte pf,pool_gestionnaire pg,app_login al
				WHERE st.sous_tete='#ligne#'
				       AND pl.idsous_tete=st.idsous_tete
				       AND pl.idpool=pf.idpool
				   AND pf.idracine=#idracine#
				   AND pf.idrevendeur IS NULL
				   AND pf.idpool=pg.idpool
				   AND pg.app_loginid=al.app_loginid
			</cfquery>
			<cfreturn qSearch>
		<cfelse>
			<cfreturn queryNew("error")>
		</cfif>
	</cffunction>
	
	<cffunction name="listeCommande" access="remote" returntype="any" output="true" hint="Fournit l'historique des etats d'une commande." >
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfquery name="qListe" datasource="ROCOFFRE" maxrows="50">
				select c.idcommande, c.numero_operation, i.libelle_etat,nvl(c.opeurateurid,0) operateurid, c.idinv_etat, c.montant, c.date_operation, 
				          to_char(date_operation,'dd/mm/yyyy HH24:MI') ladate,'Commandes du jour' header,c.date_create, c.libelle 
				        from commande c, inv_etat i
				        where c.idinv_etat=i.idinv_etat
				        and trunc(date_create)=trunc(sysdate)
				        and c.user_create = 645
				        union
				        select c.idcommande, c.numero_operation,i.libelle_etat,nvl(c.opeurateurid,0) operateurid, c.idinv_etat, c.montant, c.date_operation, 
				          to_char(date_operation,'dd/mm/yyyy HH24:MI') ladate,'Commandes du ' || to_char(date_create,'dd/mm/yyyy') header,c.date_create, c.libelle 
				        from commande c, inv_etat i
				        where c.idinv_etat=i.idinv_etat
				        and trunc(date_create) <= trunc(sysdate-1) 
				        and trunc(date_create) >=trunc(sysdate-7)
				        and c.user_create = 645
				        order by date_create desc
			</cfquery>
		</cfif>			
		<cfreturn qListe>
	</cffunction>
	
	<cffunction name="suiviCommande" access="remote" returntype="any" output="true" hint="Fournit l'historique des etats d'une commande." >
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="idCommande" required="true" type="numeric" displayname="ID DE LA COMMANDE"/>
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfset idracine=v.clientid>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_WORKFLOW.HISTORIQUEETATSOPERATION" blockfactor="100" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idCommande" value="#idCommande#">			
				<cfprocresult name="historique">
			</cfstoredproc>
		</cfif>			
		<cfreturn historique>
	</cffunction>
	
	<cffunction name="updateJS" access="remote" returntype="any" output="true" hint="" >
		<cfargument name="idCommande" required="true" type="numeric" displayname="ID DE LA COMMANDE"/>
		<cfset SESSION.OffreDSN="ROCOFFRE">
	
			<cfset idracine=2458788>
				<cfquery name="qGetCmd" datasource="ROCOFFRE">
					SELECT '' libelle_action,c.libelle,
					                al.login_nom||' '||al.login_prenom AS nom,
					                  ie.idinv_etat,ie.libelle_etat,
					                  al.login_email AS email,al.app_loginid AS iduser_create
					            FROM commande c,inv_etat ie,app_login al
					            WHERE c.idcommande=#idCommande#
					                AND c.idinv_etat=ie.idinv_etat
					                AND c.user_modif=al.app_loginid
					                AND c.idracine=#idracine#
					
				</cfquery>
				
				<cfset myUrl = "http://saaswedo.fr.jamespot.pro/api/api.php?v=2.0">

				<cfset dateFin = DateFormat(Now(),"yyyy-mm-ddT") & TimeFormat(Now(),"HH:mm:ss")>
				<cfset o = "article">
				<cfset f = "create">
				<cfset m = "EXT-SWD" >
				<cfset var = m & "-" & dateFin & "-CleSaaaswedo">
				<cfset secret = LCase(hash(var))>

					<cfset mail = "brice.miramont@saaswedo.com">
					<cfset idspot = 16>
					<cfset type = "commande">
					<cfset title = URLEncodedFormat(qGetCmd.libelle)>
					<cfset text = "">
					<cfset etat = URLEncodedFormat(qGetCmd.libelle_etat)>

					<cfset data = "&mail=" & mail & "&idspot=" & idspot & "&type=" & type & "&title=" & title & "&text=" & text & "&etat=" & etat>

				<cfset myUrl = myUrl & "&o=" & o & "&f=" & f & "&m=" & m & "&d=" & dateFin & "&k=" & secret & data>
				
				<cfhttp url="#myUrl#">
		<cfreturn qGetCmd>
	</cffunction>
	
	<cffunction access="remote" name="getProduits" returntype="any" output="true">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="ligne" type="string" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfobject component="fr.consotel.consoview.access.AccessManager" type="component" name="obj">
		<cfset v=obj.validateLogin(username,password)>
		<cfif v.auth_result eq 1>
			<cfset idracine=v.clientid>
			<cfquery name="qSearch" datasource="ROCOFFRE">
				SELECT DISTINCT e.libelle_eq,te.type_equipement,ep.libelle_eq AS equipement_parent,tep.type_equipement AS type_equipement_parent,
				             pca.libelle_produit,decode(pca.bool_acces,1,'Abo','Option') type_produit,cp.typchg
				FROM  port_ligne PL,port_equipement pe,equipement e,inventaire_produit ip,produit_catalogue pca,groupe_client_ref_client gcrc,type_equipement te,sous_tete st,
						equipement ep, type_equipement tep,type_produit tp,categorie_produit cp
				WHERE st.sous_tete='#ligne#'
				       AND pl.idsous_tete(+)=st.idsous_tete
				       AND pl.idport_equipement=pe.idport_equipement(+)
				   AND pe.idequipement=e.idequipement(+)
				   AND st.idsous_tete=ip.idsous_tete(+)
				   AND ip.idproduit_catalogue=pca.idproduit_catalogue(+)
				   AND gcrc.idref_client(+)=ip.idref_client
				   AND gcrc.idgroupe_client(+)=#idracine#
				   AND te.idtype_equipement(+)=e.idtype_equipement
				   AND e.idequipement_parent=ep.idequipement(+)
				   AND ep.idtype_equipement=tep.idtype_equipement(+)
				   AND pca.idtype_produit=tp.idtype_produit(+)
				   AND tp.idcategorie_produit=cp.idcategorie_produit(+)
				   AND cp.typchg(+)  <>'REM'

			</cfquery>
			<cfreturn qSearch>
		<cfelse>
			<cfreturn queryNew("error")>
		</cfif>
	</cffunction>
	
	<cffunction access="remote" name="getDetailEquipement" returntype="any" output="true">
		<cfargument name="idequipement_fournis" type="numeric" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
			<cfquery name="qSearch" datasource="ROCOFFRE">
				SELECT ef.idequipement_fournis,ef.libelle_eq,cpf.value AS valeur, nvl(cvfg.value,cvf.value) AS groupe,
				nvl(cvf.value,cvfg.value) AS nom,cvme.value AS unite,cdp.path||'/'||cdp.file_name thumb_pic,
				cdl.path||'/'||cdl.file_name low_pic,cdh.path||'/'||cdh.file_name high_pic,
				/*GET_IMAGE_WIDTH(cdh.path||'/'||cdh.file_name)*/cp.high_pic_width high_width,/*GET_IMAGE_WIDTH(cdl.path||'/'||cdl.file_name)*/cp.low_pic_width low_width,
				/*GET_IMAGE_Height(cdh.path||'/'||cdh.file_name)*/cp.high_pic_height high_height,/*GET_IMAGE_Height(cdl.path||'/'||cdl.file_name)*/cp.low_pic_height
				FROM equipement_fournis ef,ci_product cp, offrE.ci_product_feature cpf,offrE.ci_feature cf,offrE.ci_measure cm,offrE.ci_category_feature_group ccfg, 
				offrE.ci_feature_group cfg,offrE.ci_vocabulary cvfg,offrE.ci_vocabulary cvme,offrE.ci_vocabulary cvf,ci_down_pic cdp,
				ci_down_pic cdl,ci_down_pic cdh
				WHERE ef.product_id=cpf.product_id AND cpf.feature_id=cf.feature_id AND cf.measure_id=cm.measure_id(+) 
				AND cpf.categoryfeaturegroup_id=ccfg.category_feature_group_id(+) AND cfg.feature_group_id(+)=ccfg.feature_group_id 
				AND cvfg.sid(+)=cfg.sid AND cvfg.langid(+)=1 AND cvme.sid(+)=cm.sid AND cvme.langid(+)=1  AND cvf.sid(+)=cf.sid 
				AND cvf.langid(+)=1 
				AND ef.idequipement_fournis=#idequipement_fournis#
				AND ef.product_id=cp.product_id 
				and lower(cp.thumb_pic)=lower(cdp.url(+)) 
				and lower(cp.low_pic)=lower(cdl.url(+))
				and lower(cp.high_pic)=lower(cdh.url(+))
				AND nvl(cvfg.value,cvf.value) IS NOT NULL 
				ORDER BY groupe,nom
			</cfquery>
			<cfreturn qSearch>
	</cffunction>
	
	<cffunction access="remote" name="searchEquipement" returntype="any" output="true">
	    <cfargument name="chaine" type="string" required="true">
	    <cfargument name="idtype_equipement" type="string" required="false" default="0">
	    <cfset SESSION.OffreDSN="ROCOFFRE">
	    <cfquery name="qSearch" datasource="ROCOFFRE" maxrows="500">
	       select max(t.idequipement_fournis) idequipement_fournis,t.libelle_eq, 
	        max(t.reference_eq) ref_equipement,  max(t.ref_constructeur) ref_constructeur, f.nom_fournisseur,
	          te.type_equipement
	        from equipement_fournis t, fournisseur f, type_equipement te, ci_product cp
	        where t.idtype_equipement in (2091,70,9)
	        and t.idtype_equipement=decode(#idtype_equipement#,0,t.idtype_equipement,#idtype_equipement#)
	        and t.idtype_equipement=te.idtype_equipement
	        and t.visible=1
	        and t.product_id=cp.product_id
	        and t.ref_constructeur is not null
	        and cp.family_id = 1
	        and t.idfabriquant= f.idfournisseur
	        and (lower(t.libelle_eq) like '%' || replace('#lcase(chaine)#',' ','%') || '%'
	        or lower(f.nom_fournisseur) like '%' || replace('#lcase(chaine)#',' ','%') || '%')
	        group by t.libelle_eq,f.nom_fournisseur,te.type_equipement
	        order by lower(nom_fournisseur), lower(libelle_eq)
	    </cfquery>
	    <cfreturn qSearch>
 	 </cffunction>

	
	<cffunction access="remote" name="getAcc" returntype="any" output="true">
		<cfargument name="idequipement_fournis" type="numeric" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">
		<cfquery name="qSearch" datasource="ROCOFFRE" maxrows="500">
			SELECT DISTINCT ef2.idequipement_fournis idequipement_fournisseur,ef2.libelle_eq,ef2.reference_eq,
			ef2.ref_constructeur,f.nom_fournisseur,cdp.path||'/'||cdp.file_name thumb_pic,cdl.path||'/'||cdl.file_name low_pic,
			cdh.path||'/'||cdh.file_name high_pic,cv.value categorie,
			GET_IMAGE_WIDTH(cdh.path||'/'||cdh.file_name) high_width,GET_IMAGE_WIDTH(cdl.path||'/'||cdl.file_name) low_width,
			GET_IMAGE_Height(cdh.path||'/'||cdh.file_name) high_height,GET_IMAGE_Height(cdl.path||'/'||cdl.file_name) low_heigh
			FROM ci_product_related cpr,equipement_fournis ef,equipement_fournis ef2 ,fournisseur f,
			ci_product cp,ci_down_pic cdp,ci_down_pic cdl,ci_down_pic cdh,ci_category cc,ci_vocabulary CV
			WHERE ef.product_id=cpr.product_id
			AND ef.idequipement_fournis=#idequipement_fournis#
			AND cpr.rel_product_id=ef2.product_id
			AND ef2.idfabriquant=f.idfournisseur
			AND ef2.idfabriquant=ef2.idrevendeur
			AND cp.product_id=ef2.product_id
			and lower(cp.thumb_pic)=lower(cdp.url(+)) 
			and lower(cp.low_pic)=lower(cdl.url(+)) 
			and lower(cp.high_pic)=lower(cdh.url(+))
			AND cp.catid=cc.catid(+)
			AND cc.sid=cv.sid(+)
			AND cv.langid(+)=1
			ORDER BY ef2.libelle_eq
		</cfquery>
		<cfreturn qSearch>
	</cffunction>
	
	<cffunction name="enregistrerCommandeV2" displayname="enregistrerCommande" access="remote" output="false" returntype="string" hint="enregistrement des informations d'une commande">
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		/>
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	/>
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		/>
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	/>

		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		/>
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refClient2" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		/>

		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		/>
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		/>

		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		/>

		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		/>
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="segmentData" 			required="true" type="numeric" 	default="" 		/>

		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		/>

		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	/>

		<cfargument name="idProfileEquipement"	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idSiteFacturation"	required="true" type="numeric" 	default="NULL"	/>

			
			<cfset idracine = 2458788>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M16.ENREGISTREROPERATION_V6">
				
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="0" null="yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="0" null="yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="Test Jamespot">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="0">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="0" null="yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="P_idsite_facturation"  value="#idSiteFacturation#">
	 			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v1" 					value="">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v2"				 	value="">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v3"				 	value="">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v4"				 	value="">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_v5"				 	value="">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="M. Le Gestionnaire">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client_2"   		value="#refClient2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_data"   		value="#segmentData#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_liv_disrtib"   		value="0">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
			</cfstoredproc>
		<cfset v = faireActionWorkFlow(2066,"#dateOperation#","",p_idoperation)>
		<cfset v2 = updateJS(p_idoperation)>
		<cfreturn p_idoperation>		
	</cffunction>
	
	<cffunction name="faireActionWorkFlow" access="remote" returntype="numeric" output="false" hint="FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE">
		<cfargument name="idAction" 		 	required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="dateAction" 		 	required="true" type="string" 	default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="commentaireAction" 	required="true" type="string"  default="" displayname="struct commande" 	hint="Initial value for the commandeproperty." />
		<cfargument name="idOperation" 		 	required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		
	
		 
			<cfset _idgestionnaire= 645>
			<cfset _idracine = 2458788>

		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.faireactiondeworkflowmobile_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idracine" 		value="#_idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_date_action" 	value="#dateAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_commentaire" 	value="#commentaireAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idgestionnaire" 	value="#_idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idoperation" 	value="#idOperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idaction" 		value="#idAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_bool_mail" 		value="1">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idcv_log_mail" 	value="645">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
</cfcomponent>