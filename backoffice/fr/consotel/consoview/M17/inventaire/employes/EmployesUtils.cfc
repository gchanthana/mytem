<!--- =========================================================================
Classe: EmployesUtils
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="EmployesUtils" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="EmployesUtils" hint="Remplace le constructeur de EmployesUtils.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->

	<!--- 
	fournit la liste des employés des pools de gestion d'un gestionnaire suivant une clef de rechercher, 
	si id du gestionnaire = 0, on prend les employes de la racine.
	clef = | NOM | PRENOM | CODE_INTERNE | FONCTION_EMPLOYE | REFERENCE_EMPLOYE
	 --->
	<cffunction name="fournirListeEmployes" access="public" returntype="query" output="false" hint="" >
		<!---  
			TEST 
			<cfargument name="gestionnaire" required="false" type="struct" default="" displayname="struct gestionnaire" hint="Initial value for the gestionnaireproperty." />
			<cfargument name="clef" required="false" type="string" default="" displayname="string clef" hint="Initial value for the clefproperty." />
			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		 --->
	 
		<cfargument name="idPool" type="numeric" required="true" default="-2">
		<cfargument name="cle" type="string" required="true" default="">
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset search = "">
		 
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M17.FIND_EMPLOYE">			
				<cfprocparam  value="#idPool#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#cle#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result/>
	</cffunction>
	
	<!--- Retrouve l'id de l'employe grace à  son idgroupeclient  --->
	<cffunction name="fournirRealIDEmploye" access="remote" returntype="any" output="false" hint="" >
		<cfargument name="idGroupeClient" required="true" type="struct"/>				
		
		<cfset idemploye = -1/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.GETFICHEEMPLOYE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idGroupeClient#"/>		
			<cfprocresult name="p_result"/>        
		</cfstoredproc>
		 
		<cfif p_result.recordCount gt 0>
			<cfset idemploye = val(p_result["IDEMPLOYE"][1])/>
		</cfif>
				
		<cfreturn idemploye/>
	</cffunction>
		
		
	<cffunction name="fournirDetailEmploye" access="remote" returntype="any" output="false">
		<cfargument name="employeInfo" required="true" type="struct"/>				
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.GETFICHEEMPLOYE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#employeInfo.IDEMPLOYE#"/>		
			<cfprocresult name="p_result"/>        
		</cfstoredproc>
		 
		<cfif p_result.recordCount gt 0>		
			 
			<cfset employeInfo.IDRACINE_NUMBER = (p_result["IDRACINE"][1])/>
			<cfset employeInfo.IDSITE_PHYSIQUE = (p_result["IDSITE_PHYSIQUE"][1])/>
			<cfset employeInfo.IDEMPLACEMENT= (p_result["IDEMPLACEMENT"][1])/>
			<cfset employeInfo.APP_LOGINID = (p_result["APP_LOGINID"][1])/>
			<cfset employeInfo.CLE_IDENTIFIANT = (p_result["CLE_IDENTIFIANT"][1])/>
			<cfset employeInfo.CIVILITE = (p_result["CIVILITE"][1])/>
			<cfset employeInfo.NOMPRENOM = (p_result["NOM"][1] & ' ' & p_result["PRENOM"][1])/>
			<cfset employeInfo.NOM = (p_result["NOM"][1])/>
			<cfset employeInfo.PRENOM = (p_result["PRENOM"][1])/>
			<cfset employeInfo.EMAIL = (p_result["EMAIL"][1])/>
			<cfset employeInfo.FONCTION_EMPLOYE = (p_result["FONCTION_EMPLOYE"][1])/>
			<cfset employeInfo.STATUS_EMPLOYE = (p_result["STATUS_EMPLOYE"][1])/>
			<cfset employeInfo.COMMENTAIRE = (p_result["COMMENTAIRE"][1])/>
			<cfset employeInfo.BOOL_PUBLICATION = (p_result["BOOL_PUBLICATION"][1])/>
			<cfset employeInfo.CODE_INTERNE = (p_result["CODE_INTERNE"][1])/>
			<cfset employeInfo.REFERENCE_EMPLOYE = (p_result["REFERENCE_EMPLOYE"][1])/>
			<cfset employeInfo.MATRICULE = (p_result["MATRICULE"][1])/>
			<cfset employeInfo.DATE_CREATION = (p_result["DATE_CREATION"][1])/>
			<cfset employeInfo.DATE_MODIFICATION = (p_result["DATE_MODIFICATION"][1])/>
			<cfset employeInfo.INOUT = (p_result["INOUT"][1])/>
			<cfset employeInfo.DATE_ENTREE = (p_result["DATE_ENTREE"][1])/>
			<cfset employeInfo.DATE_SORTIE = (p_result["DATE_SORTIE"][1])/>
			<cfset employeInfo.SEUIL_COMMANDE = (p_result["SEUIL_COMMANDE"][1])/>
			<cfset employeInfo.NIVEAU = (p_result["NIVEAU"][1])/>
		<!--- 	<cfset employeInfo.CODE_ANALYTIQUE = (p_result["CODE_ANALYTIQUE"][1])/> --->
			<cfset employeInfo.C1 = (p_result["C1"][1])/>
			<cfset employeInfo.C2 = (p_result["C2"][1])/>
			<cfset employeInfo.C3 = (p_result["C3"][1])/>
			<cfset employeInfo.C4 = (p_result["C4"][1])/>
			<cfset employeInfo.TELEPHONE_FIXE = (p_result["TELEPHONE_FIXE"][1])/>
			<cfset employeInfo.FAX = (p_result["FAX"][1])/>
		</cfif>  
				
		<cfreturn employeInfo/>
	</cffunction>
	
	<cffunction name="fournirDetailCollaborateur" access="remote" returntype="query" output="false" hint="" >
		<cfargument name="idCollaborateur" required="true" type="numeric"/>				
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.GETFICHEEMPLOYE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#idCollaborateur#"/>		
				<cfprocresult name="p_result"/>        
			</cfstoredproc>

		<cfreturn p_result/>
	</cffunction>
		
	<cffunction name="updateInfosEmploye" access="remote" returntype="numeric" output="false" hint="">
		<cfargument name="employeInfo" required="true" type="struct" displayname="un employe (collaborateur)"/>		
			
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_M17.UPDATEEMPLOYE_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#employeInfo.IDEMPLOYE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#employeInfo.IDSITE_PHYSIQUE#" null="#iif((employeInfo.IDSITE_PHYSIQUE gt 0), de("no"), de("yes"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#employeInfo.IDEMPLACEMENT#" null="#iif((employeInfo.IDEMPLACEMENT gt 0), de("no"), de("yes"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#employeInfo.APP_LOGINID#" null="#iif((employeInfo.APP_LOGINID gt 0), de("no"), de("yes"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.CLE_IDENTIFIANT#" null="#iif((employeInfo.CLE_IDENTIFIANT gt 0), de("no"), de("yes"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.CIVILITE#" null="#iif((employeInfo.CIVILITE gt 0), de("no"), de("yes"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.NOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.PRENOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.EMAIL#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.FONCTION_EMPLOYE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.STATUS_EMPLOYE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.COMMENTAIRE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#employeInfo.BOOL_PUBLICATION#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.CODE_INTERNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.REFERENCE_EMPLOYE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.MATRICULE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#employeInfo.INOUT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsdateformat(employeInfo.DATE_ENTREE,'yyyy/mm/dd')#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsdateformat(employeInfo.DATE_SORTIE,'yyyy/mm/dd')#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.C1#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.C2#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.C3#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.C4#" null="false">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.NIVEAU#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.SEUIL_COMMANDE#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.TELEPHONE_FIXE#" null="false"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.FAX#" null="false"/>
			<!--- <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#employeInfo.CODE_ANALYTIQUE#" null="false"/>		 ---> 
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retourUdpateEmploye">
		</cfstoredproc>
		 
		<cfreturn p_retourUdpateEmploye>
	</cffunction>
	
	<cffunction name="getMatriculeEmploye" access="remote" returntype="query">
		<cfargument name="NBCLE" required="true" type="Numeric" />
			<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.getMatricule">
					<cfprocparam  value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#NBCLE#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="qGetMatriculeEmploye">
			</cfstoredproc>
		<cfreturn qGetMatriculeEmploye>
	</cffunction>
	
	<cffunction name="getSiteEmploye" access="remote" returntype="query" output="false" hint="">
		<cfargument name="idemploye" required="true" type="numeric" />
		<!---   <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>  --->
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.get_site_emp">
				<cfprocparam  value="#idemploye#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocresult name="p_result"/>
			</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="updateSiteEmploye" access="remote" returntype="numeric" output="false" hint="">
		<cfargument name="idemploye" required="true" type="numeric" />
		<cfargument name="idSite_physique" required="true" type="numeric" />
			<!--- <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE> --->
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.updateSiteEmploye">
					<cfprocparam  value="#idemploye#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam  value="#idSite_physique#" cfsqltype="CF_SQL_NUMERIC" type="in">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retourUdpateSiteEmploye">
					<!--- <cfprocresult name="p_result"/> --->
				</cfstoredproc>
				<!--- <cfif p_result.recordCount gt 0>
					<cfset employeInfo.IDSITE_PHYSIQUE(p_result["IDSITE_PHYSIQUE"][1])/>
				</cfif> --->
		<cfreturn p_retourUdpateSiteEmploye/>
	</cffunction>
	
	<cffunction name="getTerminauxEmploye" access="remote" returntype="query" output="false" hint="">
		<cfargument name="idEmploye" required="true" type="numeric" />
		<cfargument name="idPoolGestion" required="true" type="numeric" />
			<!--- <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE> --->
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.getCollabTerminaux">
					<cfprocparam  value="#idEmploye#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam  value="#idPoolGestion#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocresult name="qGetTerminauxEmploye"/>
				</cfstoredproc>
		<cfreturn qGetTerminauxEmploye>
	</cffunction>
	
	<cffunction name="getLignesEmploye" access="remote" returntype="query" output="false" hint="">
		<cfargument name="idEmploye" required="true" type="numeric" />
		<cfargument name="idPoolGestion" required="true" type="numeric" />
			<!--- <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE> --->
			<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.getCollabLignes">
					<cfprocparam  value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam  value="#idEmploye#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam  value="#idPoolGestion#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocresult name="qGetLignesEmploye"/>
				</cfstoredproc>
		<cfreturn qGetLignesEmploye>
	</cffunction>
	
	<cffunction name="getSimEmploye" access="remote" returntype="query" output="false" hint="">
		<cfargument name="idEmploye" required="true" type="numeric" />
		<cfargument name="idPoolGestion" required="true" type="numeric" />
			<!--- <cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE> --->
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.getCollabSim">
					<cfprocparam  value="#idEmploye#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam  value="#idPoolGestion#" cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocresult name="qGetSimEmploye"/>
				</cfstoredproc>
		<cfreturn qGetSimEmploye>
	</cffunction>
	
	    

	<cffunction name="savecollab" access="remote" returntype="numeric" >
		<cfargument name="IDPOOL" required="true" type="Numeric"/>
		<cfargument name="LISTECOLLAB" required="true" type="String"/>
		 <cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M17.SAVECOLLAB">
			<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#">
			<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER"  value="#IDPOOL#" >
			<cfprocparam  type="in" cfsqltype="CF_SQL_CLOB" value="#tostring(LISTECOLLAB)#">
			<cfprocparam  type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retourUpdateEmploye">
		</cfstoredproc>
		<cfreturn p_retourUpdateEmploye>
	</cffunction>
	
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>
