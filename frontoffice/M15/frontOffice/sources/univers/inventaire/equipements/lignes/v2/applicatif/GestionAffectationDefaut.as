package univers.inventaire.equipements.lignes.v2.applicatif
{
	import univers.inventaire.equipements.lignes.v2.vo.Ligne;
	import univers.inventaire.equipements.lignes.v2.vo.OngletAffectationVo;

	public class GestionAffectationDefaut extends GestionAffectationStrategy
	{
		public function GestionAffectationDefaut(ligne : Ligne)
		{	
			super(ligne);
		}
				
		override public function getInfosAffectation():void{
			trace("GestionAffectationDefaut.getInfosAffectation");
		}		
		
		override public function updateInfosAffectation(data : OngletAffectationVo):void{
			trace("GestionAffectationDefaut.updateInfosAffectation");
		}
		
	}
}