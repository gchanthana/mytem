<cfcomponent displayname="fr.consotel.consoview.M16.v2.ModeleService" output="false">

	<!--- SAUVEGARDE UN MODELE --->

	<cffunction name="sauvegardeModele" access="remote" output="false" returntype="numeric" hint="SAUVEGARDE UN MODELE">
		<cfargument name="idRacine" 				required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idPoolgestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idOperateur" 				required="true" type="numeric"/>
		<cfargument name="idrevendeur" 				required="true" type="numeric"/>
		<cfargument name="idContactRevendeur" 		required="true" type="numeric" default="NULL"/>
		<cfargument name="idSiteLivraison" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="libelleOperation" 		required="true" type="string"/>
		<cfargument name="referenceClient" 			required="true" type="string"/>
		<cfargument name="referenceClient2"			required="true" type="string"  default="NULL"/>
		<cfargument name="referenceOperateur" 		required="true" type="string"/>
		<cfargument name="commentaires" 			required="true" type="string" default="NULL"/>
		<cfargument name="montant" 					required="true" type="numeric"/>
		<cfargument name="segmentFixe" 				required="true" type="numeric"/>
		<cfargument name="segmentMobile" 			required="true" type="numeric"/>
		<cfargument name="segmentData" 				required="true" type="numeric"/>
		<cfargument name="inventaireTypeOpe" 		required="true" type="string"/>
		<cfargument name="idGroupePrefere" 			required="true" type="numeric" default="NULL"/>
		<cfargument name="articles" 				required="true" type="string"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SAVEMODELECOMMANDE_V2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpoolgestionnaire" 		value="#idPoolgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 				value="#idOperateur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 				value="#idrevendeur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 		value="#idContactRevendeur#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 		value="#idSiteLivraison#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_operation" 		value="#libelleOperation#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client" 				value="#referenceClient#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_revendeur" 			value="#referenceOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="in" cfsqltype="CF_SQL_NUMERIC" variable="p_montant" 					value="#montant#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" 			value="#segmentFixe#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" 			value="#segmentMobile#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_type_ope" 			value="#inventaireTypeOpe#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 			value="#idGroupePrefere#"><!--- DEFAUT VALUE null --->
				<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" 	  variable="p_articles" 				value="#tostring(articles)#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 				value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_client_2" 			value="#referenceClient2#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment_data" 			value="#segmentData#">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- MISE À JOUR D'UN MODÈLE DE COMMANDE --->
	
	<cffunction name="majModele" access="remote" returntype="numeric" output="false" hint="MISE À JOUR D'UN MODÈLE DE COMMANDE">
		<cfargument name="idmodele" 				required="true" type="numeric"/>
		<cfargument name="libelleoperation" 		required="true" type="string"/>
		<cfargument name="idPoolgestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idRacine" 				required="true" type="numeric"/>
		<cfargument name="idGestionnaire" 			required="true" type="numeric"/>
		<cfargument name="articles" 				required="true" type="string"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.MAJMODELE">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"  variable="p_idmodele" 				value="#idmodele#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR"  variable="p_libelle_operation" 		value="#libelleoperation#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"  variable="p_idpoolgestionnaire" 		value="#idPoolgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	   variable="p_article" 				value="#tostring(articles)#" null="false">
				<cfprocparam type="OUT" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>	
	
	<!--- FOUNI LA LISTE DES MODÈLE DE COMMANDE APPARTENANT À UN POOL --->
	
	<cffunction name="fournirListeModele" access="remote" returntype="Query" output="false" hint="FOUNI LA LISTE DES MODÈLE DE COMMANDE APPARTENANT À UN POOL">
		<cfargument name="idPool" 				required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 	required="true" type="numeric"/>
		<cfargument name="segment" 				required="true" type="numeric"/><!--- 0=tout,1=fixe,2=mobile,3=data --->
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.GETLISTEMODELECOMMANDE_V2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 	value="#segment#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>	

	<!--- FOURNI LES ARTICLES D'UN MODÈLE DE COMMANDE APPARTENANT À UN POOL --->
	
	<cffunction name="getModeleCommande" access="remote" returntype="query" output="false" hint="FOURNI LES ARTICLES D'UN MODÈLE DE COMMANDE APPARTENANT À UN POOL">
		<cfargument name="idModele" required="true" type="numeric" default=""/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.GETMODELECOMMANDE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idmodele" value="#idModele#">			
			<cfprocresult name="p_retour">			
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
	<!--- SUPPRIME UN MODÈLE ENREGISSTRÉ --->
	
	<cffunction name="deleteModeleCommande" access="remote" returntype="numeric" output="false" hint="SUPPRIME UN MODÈLE ENREGISSTRÉ">
		<cfargument name="idModele" required="true" type="numeric" default=""/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.deletemodelecommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idmodele" value="#idModele#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour" >
		</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
</cfcomponent>