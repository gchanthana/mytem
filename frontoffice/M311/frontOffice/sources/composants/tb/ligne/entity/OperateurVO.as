package composants.tb.ligne.entity
{
	[Bindable]
	public class OperateurVO
	{
		public var VALEURSC:String = "";
		public var IDCOMPTE_FACTURATION:Number = 0;
		public var IDSOUS_TETE:Number = 0;
		public var VALEURCF:String = "";
		public var IDSOUS_COMPTE:Number = 0;
		public var OPERATEURID:Number = 0;
		public var LIBELLE_CF:String = "";
		public var LIBELLESC:String = "";
		public var OPERATEURNAME:String = "";
		
		public function OperateurVO()
		{
		}
		
		public function fill(value:Object):void
		{
			this.VALEURSC = value.VALEURSC;
			this.IDCOMPTE_FACTURATION = value.IDCOMPTE_FACTURATION;
			this.IDSOUS_TETE = value.IDSOUS_TETE;
			this.VALEURCF = value.VALEURCF;
			this.IDSOUS_COMPTE = value.IDSOUS_COMPTE;
			this.OPERATEURID = value.OPERATEURID;
			this.LIBELLE_CF = value.LIBELLE_CF;
			this.LIBELLESC = value.LIBELLESC;
			this.OPERATEURNAME = value.OPERATEURNAME;
		}
		
	}
}