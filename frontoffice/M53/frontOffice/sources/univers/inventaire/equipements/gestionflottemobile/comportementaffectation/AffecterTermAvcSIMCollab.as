package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
		
	public class AffecterTermAvcSIMCollab implements IComportementAffectation 
	{
		public function AffecterTermAvcSIMCollab()
		{
		}
		public function affecter(idTerm : int, idCollab : int,libelle_term : String, libelle_Collab : String,date_effet : String, pret : int):void
		{
			//Alert.show("AffecterTermCollab : "+idTerm+"--"+idCollab+"--"+libelle_term+"--"+libelle_Collab+"--"+date_effet);
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"affectTermToEmp_V3",
																		affecter_handler);
			RemoteObjectUtil.callService(op,idTerm,idCollab,libelle_term,libelle_Collab,date_effet,pret);
		}
		private function affecter_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Affectations du terminal et de la SIM effectuées");
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
		public function annuler():void
		{
			Alert.show("fermer");
		}
	}
}