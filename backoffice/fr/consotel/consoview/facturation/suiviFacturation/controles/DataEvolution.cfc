<cfcomponent output="false">

	<cfproperty name="MOIS" type="date" />
	<cfproperty name="COUTABO" type="numeric" />
	<cfproperty name="COUTCONSO" type="numeric" />
	<cfproperty name="COUTMOYENABO" type="numeric" />
	<cfproperty name="COUTMOYENCONSO" type="numeric" />	
	<cfproperty name="RATIOCOUTABO" type="numeric" />
	<cfproperty name="RATIOCOUTCONSO" type="numeric" />
	<cfproperty name="RATIOMOYENNE" type="numeric" />
	
	<cfscript>
		variables.MOIS = "";
		variables.COUTABO = 0;
		variables.COUTCONSO = 0;
		variables.COUTMOYENABO = 0;
		variables.COUTMOYENCONSO = 0;
		variables.RATIOCOUTABO = 0;
		variables.RATIOCOUTCONSO = 0;
		variables.RATIOMOYENNE = 0;
	</cfscript>
	
	<cffunction name="getMOIS" access="public" output="false" returntype="date">
		<cfreturn variables.MOIS />
	</cffunction>

	<cffunction name="setMOIS" access="public" output="false" returntype="void">
		<cfargument name="MOIS" type="any" required="true" />
		<cfset variables.MOIS = arguments.MOIS />
	</cffunction>

	<cffunction name="getCOUTABO" access="public" output="false" returntype="numeric">
		<cfreturn variables.COUTABO />
	</cffunction>

	<cffunction name="setCOUTABO" access="public" output="false" returntype="void">
		<cfargument name="COUTABO" type="numeric" required="true" />
		<cfset variables.COUTABO = arguments.COUTABO />
		 
	</cffunction>

	<cffunction name="getCOUTCONSO" access="public" output="false" returntype="numeric">
		<cfreturn variables.COUTCONSO />
	</cffunction>

	<cffunction name="setCOUTCONSO" access="public" output="false" returntype="void">
		<cfargument name="COUTCONSO" type="numeric" required="true" />
		<cfset variables.COUTCONSO = arguments.COUTCONSO />
		 
	</cffunction>
	
	<cffunction name="getCOUTMOYENABO" access="public" output="false" returntype="numeric">
		<cfreturn variables.COUTMOYENABO />
	</cffunction>

	<cffunction name="setCOUTMOYENABO" access="public" output="false" returntype="void">
		<cfargument name="COUTMOYENABO" type="numeric" required="true" />
		<cfset variables.COUTMOYENABO = arguments.COUTMOYENABO />
		 
	</cffunction>

	<cffunction name="getCOUTMOYENCONSO" access="public" output="false" returntype="numeric">
		<cfreturn variables.COUTMOYENCONSO />
	</cffunction>

	<cffunction name="setCOUTMOYENCONSO" access="public" output="false" returntype="void">
		<cfargument name="COUTMOYENCONSO" type="numeric" required="true" />
		<cfset variables.COUTMOYENCONSO = arguments.COUTMOYENCONSO />
	</cffunction>
	
	<cffunction name="getRATIOCOUTABO" access="public" output="false" returntype="numeric">
		<cfreturn variables.RATIOCOUTABO />
	</cffunction>

	<cffunction name="setRATIOCOUTABO" access="public" output="false" returntype="void">
		<cfargument name="RATIOCOUTABO" type="numeric" required="true" />
		<cfset variables.RATIOCOUTABO = arguments.RATIOCOUTABO />
	</cffunction>

	<cffunction name="getRATIOMOYENNE" access="public" output="false" returntype="numeric">
		<cfreturn variables.RATIOMOYENNE />
	</cffunction>

	<cffunction name="setRATIOMOYENNE" access="public" output="false" returntype="void">
		<cfargument name="RATIOMOYENNE" type="numeric" required="true" />
		<cfset variables.RATIOMOYENNE = arguments.RATIOMOYENNE />
		 
	</cffunction>
	
	<cffunction name="getRATIOCOUTCONSO" access="public" output="false" returntype="numeric">
		<cfreturn variables.RATIOCOUTCONSO />
	</cffunction>

	<cffunction name="setRATIOCOUTCONSO" access="public" output="false" returntype="void">
		<cfargument name="RATIOCOUTCONSO" type="numeric" required="true" />
		<cfset variables.RATIOCOUTCONSO = arguments.RATIOCOUTCONSO />
		 
	</cffunction>
</cfcomponent>