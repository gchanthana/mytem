<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.zendesk.Search" extends="fr.saaswedo.api.support.zendesk.RestService" hint="Fonctionnalités de recherche">
	<cffunction access="public" name="search" returntype="Struct" hint="Effectue une recherche et retourne les résultats dans une structure avec une clé RESULTS">
		<cfargument name="searchQuery" type="String" required="true" hint="Requete de recherche (Voir https://support.zendesk.com/entries/20239737-zendesk-search-reference)">
		<cfargument name="sortBy" type="String" required="false" default="" hint="Critère de tri">
		<cfargument name="sortOrder" type="String" required="false" default="" hint="Critère d'ordre">
		<cfargument name="page" type="Numeric" required="false" default="1" hint="Numéro de la page de résultat">
		<cfargument name="per_page" type="String" required="false" default="15" hint="Nombre de résultats max par page">
		<cfargument name="allPages" type="boolean" required="false" default="false" hint="TRUE pour ramener les résultats de toutes les pages et FALSE sinon">
			<cfset var per_page_param=ARGUMENTS["per_page"]>
			<cfif ARGUMENTS["allPages"] OR (per_page_param GT MAX_RESULT_PER_PAGE())>
				<cfset per_page_param=MAX_RESULT_PER_PAGE()>
			</cfif>
			<cfset var resultInfos=performSearch(ARGUMENTS["searchQuery"],ARGUMENTS["sortBy"],ARGUMENTS["sortOrder"],ARGUMENTS["page"],per_page_param)>
			<cfif resultInfos["STATUS"]["CODE"] EQ SUCCESS()>
				<cfset var searchResult=resultInfos["RESULT"]>
				<cfset var resultCount=VAL(searchResult["count"])>
				<cfset var pageCount=ceiling(resultCount / per_page_param)>
				<!--- Effectue X requetes pour récupérer les résultats de toutes les pages --->
				<cfif ARGUMENTS["allPages"] AND (pageCount GT 1)>
					<cfloop index="i" from="2" to="#pageCount#">
						<cfset resultInfos=performSearch(ARGUMENTS["searchQuery"],ARGUMENTS["sortBy"],ARGUMENTS["sortOrder"],i,per_page_param)>
						<cfif resultInfos["STATUS"]["CODE"] EQ SUCCESS()>
							<!--- Concaténation des résultats page par page --->
							<cfloop index="j" from="1" to="#arrayLen(resultInfos.RESULT.RESULTS)#">
								<cfset arrayAppend(searchResult["RESULTS"],resultInfos["RESULT"]["RESULTS"][j])>
							</cfloop>
						<cfelse>
							<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#resultInfos.FAULT.MESSAGE#" detail="HTTP CODE : #resultInfos.STATUS.HTTP_STATUS#">
						</cfif>
					</cfloop>
				</cfif>
				<cfreturn searchResult>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#resultInfos.FAULT.MESSAGE#" detail="HTTP CODE : #resultInfos.STATUS.HTTP_STATUS#">
			</cfif>
	</cffunction>
	
	<cffunction access="public" name="MAX_RESULT_PER_PAGE" returntype="Numeric" hint="Retourne le nombre max de résultat par page">
		<cfreturn 50>
	</cffunction>
	
	<cffunction access="public" name="performSearch" returntype="Struct" hint="Effectue une recherche et retourne une structure avec la clé RESULT pour le résultat de la requete<br>
	Le résultat retourné comprend entre autres le nombre de pages de résultats. Cette méthode permet la pagination des résultats sinon il est plus pratique d'utiliser search()">
		<cfargument name="searchQuery" type="String" required="true" hint="Requete de recherche (Voir https://support.zendesk.com/entries/20239737-zendesk-search-reference)">
		<cfargument name="sortBy" type="String" required="false" default="" hint="Critère de tri">
		<cfargument name="sortOrder" type="String" required="false" default="" hint="Critère d'ordre">
		<cfargument name="page" type="Numeric" required="false" default="1" hint="Numéro de la page de résultat">
		<cfargument name="per_page" type="String" required="false" default="15" hint="Nombre de résultats max par page">
		<cfif TRIM(ARGUMENTS["searchQuery"]) NEQ "">
			<cfset var requestContent={}>
			<cfset var xmlTarget="api/v2/search">
			<cfset requestContent={query=ARGUMENTS["searchQuery"]}>
			<cfif TRIM(ARGUMENTS["sortBy"]) NEQ "">
				<cfset requestContent["sort_by"]=ARGUMENTS["sortBy"]>
			</cfif>
			<cfif TRIM(ARGUMENTS["sortOrder"]) NEQ "">
				<cfset requestContent["sort_order"]=ARGUMENTS["sortOrder"]>
			</cfif>
			<cfset requestContent["page"]=ARGUMENTS["page"]>
			<cfset requestContent["per_page"]=ARGUMENTS["per_page"]>
			<cfreturn sendJSONRequest(xmlTarget,GET_METHOD(),requestContent,"")>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le paramètre searchQuery doit etre renseigné">
		</cfif>
	</cffunction>
</cfcomponent>