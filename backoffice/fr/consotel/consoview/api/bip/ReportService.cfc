<cfcomponent name="ReportService" alias="fr.consotel.consoview.api.bip.ReportService">
	<cffunction name="scheduleReport" access="remote" returntype="any">
		<cfargument name="reportParameters" required="true" type="struct">
		<cfargument name="userProfile" required="true" type="string">
		<cfset bipService=createObject("component","fr.consotel.consoview.api.bip.BIPublisherService")>
		<cfset userId="consoview">
		<cfset credentials="public">
		<cfset authStatus=bipService.validateLogin(userId,credentials)>
		<cfset resultStruct=structNew()>
		<!---
		<cfset notificationEmail="frederic.le-blan@consotel.fr,cedric.rapiera@consotel.fr,production@consotel.fr,elena.kron@consotel.fr">
		--->
		
		<cfset notificationEmail="#arguments.reportParameters['USER_EMAIL']#">
		
		<cfset remoteFileUUID=createUUID()>
		<cfset arrayOfParamNameValue=processReportParameters(arguments.reportParameters)>
		<cfset reportName=arguments.reportParameters["RAPPORT"]>
		<cfset bipReportPath=arguments.reportParameters["BIP_REPORT_PATH"]>
		<cfset format=arguments.reportParameters["FORMAT_KEY"]>
		<cfset scheduleDirectory=arguments.reportParameters["REPORT_DIR"]>
		<cfif authStatus EQ 1>
			<cfset job_id=bipService.scheduleReport(bipReportPath,"v1",format,scheduleDirectory,arrayOfParamNameValue,userId,credentials,"FTP",remoteFileUUID,notificationEmail)>
			<cfset resultStruct["PARAMETERS"]=arrayOfParamNameValue>
			<cfset resultStruct["JOB_ID"]=job_id>
			<cfset resultStruct["JOBNAME"]=remoteFileUUID>
		<cfelse>
			<cfset resultStruct["JOB_ID"]=0>
			<cfset resultStruct["JOBNAME"]=remoteFileUUID>
		</cfif>
		<cfmail to="#notificationEmail#" server="mail-cv.consotel.fr"  from="no-reply@consotel.fr" subject="Demande du rapport #reportName#" type="html">
			Une demande du rapport <b>#reportName#</b> a &eacute;t&eacute; lanc&eacute;e depuis ConsoView.
			(BackOffice : #CGI.SERVER_NAME#)
			<br><br>
			<b><u>Param&egrave;tres de la demande :</u></b><br><br>
			<li><b>Identification de la demande :</b> #remoteFileUUID#</li><br>
			<li><b>Utilisateur :</b> Mr/Mme/Mlle #arguments.reportParameters["USER_PRENOM"]# #arguments.reportParameters["USER_NOM"]# 
				(#arguments.reportParameters["USER_EMAIL"]#)</li><br>
			<cfset dateNow=NOW()>
			<li><b>Date et heure de traitement  :</b> #lsDateFormat(dateNow,"dddd mmmm yyyy")# &agrave; #lsTimeFormat(dateNow,"H:mm")#</li><br>
			<li><b>R&eacute;pertoire de r&eacute;cup&eacute;ration du fichier :</b>
					#"\\pelican\pelican\services\OBIEE\PRODUCTION\FINANCIER\" #</li><br>
			<li><b>User : services mdp : services</b></li><br>
			<li><b>Format du fichier :</b> #format#</li><br><br><br>
			Le fichier g&eacute;n&eacute;r&eacute; portera le nom de l'identification de la demande et l'extension correspondant au format.<br><br>
			<cfloop index="i" from="1" to="#arrayLen(arrayOfParamNameValue)#">
				<b>#arrayOfParamNameValue[i]["name"]# :</b> #arrayToList(arrayOfParamNameValue[i]["values"])#<br>
			</cfloop>
			<br>
			<b>Un email de notification sera envoy&eacute; lorsque le processus de g&eacute;n&eacute;ration du fichier du rapport sera termin&eacute;.<br>
			Le sujet du mail pr&eacute;cisera le statut du processus : <i>SUCCES, AVERTISSEMENT, ECHEC</i>.
			</b><br><br>
			Cordialement,
		</cfmail>
		<cfreturn resultStruct>
	</cffunction>
	
	<cffunction name="runReport" access="remote" returntype="any">
		<cfargument name="reportParameters" required="true" type="struct">
		<cfargument name="userProfile" required="true" type="string">
		<cfset arrayOfParamNameValue=processReportParameters(arguments.reportParameters)>
		<cfreturn arrayOfParamNameValue>
	</cffunction>
	
	<cffunction name="processReportParameters" access="private" returntype="array">
		<cfargument name="reportParameters" required="true" type="struct">
		<cfset parameterKeys=structKeyArray(arguments.reportParameters)>
		<cfset arrayOfParamNameValue=arrayNew(1)>
		<cfset j=1>
		<cfloop index="i" from="1" to="#arrayLen(parameterKeys)#">
			<cfif FIND("P_",parameterKeys[i],1) EQ 1>
				<cfset value=structNew()>
				<cfset value.name=parameterKeys[i]>
				<cfset values=arrayNew(1)>
				<cfset values[1]=arguments.reportParameters[parameterKeys[i]]>
				<cfset value.values=values>
				<cfset arrayOfParamNameValue[j]=value>
				<cfset j=j+1>
			</cfif>
		</cfloop>
		<cfreturn arrayOfParamNameValue>
	</cffunction>
</cfcomponent>
