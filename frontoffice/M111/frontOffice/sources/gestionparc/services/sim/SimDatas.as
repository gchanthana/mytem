package gestionparc.services.sim
{
	import flash.events.EventDispatcher;

	import gestionparc.entity.SIMVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;

	public class SimDatas extends EventDispatcher
	{

		private var _sim:SIMVO=new SIMVO();

		/**
		 * Traite les données reçues au retour de la procedure <code>getInfosFicheSim</code>.
		 * Appel la fonction <code>fillSimVO()</code> pour affecter a la data "sim" toutes les données concernant :
		 * - Ses informations.
		 * - Ses abonnements et options.
		 * - Ses incidents.
		 * - Ses organisations.
		 */
		public function treatDataGetInfosFicheSim(obj:Object):void
		{
			sim=new SIMVO();
			sim=sim.fillSimVO(obj);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SIM_LOADED));
		}

		/**
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant la fiche Sim s'est bien déroulée.
		 */
		public function treatSetInfosFicheSim():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SIM_UPLOADED));
		}

		public function treatechangerSim():void
		{
			refreshData();
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.ASSOCIATION_REALIZED));
		}

		public function treatrebusTerminal():void
		{
			refreshData();
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.MISE_AU_REBUT_VALIDATE));
		}

		private function refreshData():void
		{
			SpecificationVO.getInstance().boolHasChanged=true;
			SpecificationVO.getInstance().refreshData();
		}

		public function set sim(value:SIMVO):void
		{
			_sim=value;
		}

		public function get sim():SIMVO
		{
			return _sim;
		}
	}
}
