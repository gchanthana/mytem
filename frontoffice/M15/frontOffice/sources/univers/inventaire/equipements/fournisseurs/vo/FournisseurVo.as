package univers.inventaire.equipements.fournisseurs.vo
{
	public class FournisseurVo
	{
		public var IDFOURNISSEUR:Number=0;
		public var IDGROUPE_CLIENT:Number=0;
		public var NOM_FOURNISSEUR:String=""; 	//	VARCHAR2(100)
		public var ADRESSE1_HQ:String=""; 		//	VARCHAR2(200)
		public var ADRESSE2_HQ:String=""; 		//	VARCHAR2(200)
		public var CODE_POSTAL_HQ:String=""; 	//	VARCHAR2(5)
		public var COMMUNE_HQ:String=""; 		//	VARCHAR2(100)
		
		public var PAYSID_HQ:Number=0;			//	PAYS_CONSOTELID
		public var PAYS_HQ:String="";
					
		public var PAYSORIGINEID_HQ:Number=0;	//	PAYS_CONSOTELID
		public var PAYSORIGINE_HQ:String="";	//	PAYS_CONSOTELID
		
		public var TYPE_FOURNISSEUR:Number=0;	//	0 pour fabriquant 1 pour revendeur 
		public var TYPE_FOURNISSEUR_LIBELLE:Number=0;
		
		public function FournisseurVo()
		{
		}

	}
}