
<cfset format="excel">
<cfset locale="fr-FR">
<cfset etat="/consoview/gestion/lignesetservices/searchLignes/searchLignes.xdo">

<cfif StructKeyExists(session,"perimetre")>
	<cfset o=CreateObject("component","fr.consotel.consoview.util.PublicReportService")>
	<cfset d=o.createReportRequest(etat,form.template,format,locale)>
	<cfset d=o.AddParameter("P_IDGROUPE_CLIENT",session["perimetre"]["id_groupe"])>
	<cfset d=o.AddParameter("P_CHAINE",form.chaine)>
	<cfset contenu=o.runReport()>
	
	<cfheader name="Content-Disposition" value="inline;filename=#form.libelle#_#LsDateFormat(Now(),"yyyymmdd")#.xls">
	<cfcontent type="#contenu.getReportContentType()#" variable="#contenu.getReportBytes()#">
<cfelse>
Session expirée.
</cfif>