<cfcomponent output="false" displayname="Liste des destinataires">
	
	<cffunction access="remote" output="false" name="getListeDest1" returntype="query"> 		
		<cfset racineIduser   = SESSION.PERIMETRE.ID_GROUPE>
		<!--- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
		<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.--->		 
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.getDestRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#racineIduser#" variable="p_idracine">
	
			<cfprocresult name="qListeDestinataires">
		</cfstoredproc>		
        <cfreturn qListeDestinataires><!--- le resultat de la procedure --->
     </cffunction>
	
	<cffunction access="remote" output="false" name="getListeDest" returntype="query"> 	
		
		<cfargument name="idNode" type="numeric" required="true">	
		<cfset racineIduser   = SESSION.PERIMETRE.ID_GROUPE>
		
	 <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.getDestRacine_v2">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#racineIduser#" variable="p_idracine">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idNode#" variable="p_idnoeud">
			<cfprocresult name="qListeDestinataires">
		</cfstoredproc>		
        <cfreturn qListeDestinataires><!--- le resultat de la procedure --->
     </cffunction>
	
</cfcomponent>