package entity
{
	[Bindable]
	public class ModuleFormationVO
	{
		private var _id:int;
		private var _nomModule:String;
		private var _code:String;
		private var _commentaires:String;
		private var _univers:String;
		
		public function ModuleFormationVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_SUJET_FORMATION"))
					this.id = obj.IDFC_SUJET_FORMATION;
				if(obj.hasOwnProperty("SUJET_FORMATION"))
					this.nomModule = obj.SUJET_FORMATION;
				if(obj.hasOwnProperty("CODE_MODULE"))
					this.code = obj.CODE_MODULE;
				if(obj.hasOwnProperty("COMMENTAIRES"))
					this.commentaires = obj.COMMENTAIRES;
				if(obj.hasOwnProperty("UNIVERS"))
					this.univers = obj.UNIVERS;
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet ModuleFormationVO erroné ");
			}
		}
		
		public function get univers():String { return _univers; }
		
		public function set univers(value:String):void
		{
			if (_univers == value)
				return;
			_univers = value;
		}
		
		public function get commentaires():String { return _commentaires; }
		
		public function set commentaires(value:String):void
		{
			if (_commentaires == value)
				return;
			_commentaires = value;
		}
		
		public function get code():String { return _code; }
		
		public function set code(value:String):void
		{
			if (_code == value)
				return;
			_code = value;
		}
		
		public function get nomModule():String { return _nomModule; }
		
		public function set nomModule(value:String):void
		{
			if (_nomModule == value)
				return;
			_nomModule = value;
		}
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
	}
}