package univers.facturation.suivi.controles.vo
{
	[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Operateur")]

	[Bindable]
	public class Operateur
	{

		public var nom:String = "";
		public var id:Number = 0;
		public var idGroupeClient:Number = 0;


		public function Operateur()
		{
		}

	}
}