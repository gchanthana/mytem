package univers.inventaire.lignesetservices.applicatif
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.utils.describeType;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.lignesetservices.util.viewsHelpers;
	import univers.inventaire.lignesetservices.vo.Ligne;
	
	 
//----- Events -----------------------------------
	
	
	[Event(name="searchLignesComplete")]
	
	
	
	
	[Event(name="ListeTetesLigneUpdated")]
	[Event(name="getInfosLigneComplete")]
	[Event(name="UpdateInfosLigneComplete")]
	
	
	
	
	[Event(name="getEtatLigneComplete")]
	[Event(name="updateEtatLigneComplete")]
	
	
	
	
	
	
//----------------------------------------------	
	
	
	
	[Bindable]
	public class GestionTechniqueLignesApp extends EventDispatcher
	{
		//const 
		public static const LIGNE_MOBILE : Number = 707;
		
		public static const TYPE_FICHE_MOBILE : String = "MOB";
		
		
		
		//---------- Eevent const type ------------------------
		public static const SEARCHLIGNES_COMPLETE : String = "searchLignesComplete";
		
		
		
		public static const LISTETETESLIGNE_COMPLETE : String =  "ListeTetesLigneUpdated";
		public static const GETINFOSLIGNE_COMPLETE : String = "getInfosLigneComplete";
		public static const UPDATEINFOSLIGNE_COMPLETE : String = "UpdateInfosLigneComplete";
		
		
		
		
		public static const GETETATLIGNE_COMPLETE : String = "getEtatLigneComplete";
		public static const UPDATEETATLIGNE_COMPLETE : String = "updateEtatLigneComplete";
		//----------Fin Eevent const type -----------------------		
		
		//Données pour les combos et autres
		private var _viewHelper : viewsHelpers;
		public function get viewHelper():viewsHelpers{
			return _viewHelper;
		}
		
		
		
		public function set viewHelper(vh : viewsHelpers):void{
			_viewHelper = vh;
			_viewHelper.getListeTypesLigne();
			_viewHelper.getListeTypeRaccordement();
			_viewHelper.getListeUsages();
			_libellesPersos.getLibellesPersos();
		}
			
		
			
		
		
		//Gestion des libellés persos
		private var _libellesPersos : LibellesPersos = new LibellesPersos();
		public function get libellesPersos():LibellesPersos{
			return _libellesPersos;
		}
		public function set libellesPersos(lp : LibellesPersos):void{
			_libellesPersos = lp;
		}
		
		
		
				
		
		
		
		
		
		
		
		
		
		//Le mode ecriture ou consultation
		public function get modeEcriture():Boolean{
			return true;
		}
		
		
		
		
		
			
		
		
		
		
		
		
		
		
		
		
		
		
		///Constructeur	
		public function GestionTechniqueLignesApp(target:IEventDispatcher=null)
		{
			//TODO: implement function
			super(target);
			viewHelper = new viewsHelpers();
		}
		
		
		
		
		
		
		//*===================== LISTE DES LIGNES =============================*//
		private var _listeLignes : ArrayCollection;
		public function get listeLignes():ArrayCollection{
			return _listeLignes;
		}		
		public function set listeLignes(liste : ArrayCollection):void{
			if (_listeLignes != null) _listeLignes = null;
			_listeLignes = new ArrayCollection();
			
			for (var item : Object in liste){
				var ligne : Ligne = new Ligne();
				doMapping(liste[item],ligne);
				_listeLignes.addItem(ligne); 
					
			}
		 
		}
		
		//Pour l'export 
		private var _tmpIdGroupeClient : Number = 0;
		private var _tmpChaine : String = "";
				
		public function getListeLignes(idGroupe_client : Number,chaine : String):void{
			_tmpIdGroupeClient = idGroupe_client;
			_tmpChaine = chaine;			
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"searchLignes",
																				getListeLignesResultHandler);
			RemoteObjectUtil.callService(op,idGroupe_client,chaine)
		}		
		
		
		private function getListeLignesResultHandler(re :ResultEvent):void{
			if(re.result){
				dispatchEvent(new Event(SEARCHLIGNES_COMPLETE));
				listeLignes = re.result as ArrayCollection;
				
			}else{
				listeLignes = null;
			}
		}		
		
		
		public function exporterListeLignes():void {
			 var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																"exporterListeLignes",
																exporterListeLignesResultHandler);
			RemoteObjectUtil.callService(opData,_tmpIdGroupeClient,_tmpChaine);
		}
		
		private function exporterListeLignesResultHandler(re : ResultEvent):void{
			if (String(re.result) != "error"){
				displayExport(String(re.result));
			}else{
				Alert.show("Erreur export");
			}
		}
		
		private function displayExport(name : String):void {			 
			var url:String = moduleLignesEtServicesIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/inventaire/equipement/lignes/csv/exportCSV.cfm";
            var request:URLRequest = new URLRequest(url);         
           	var variables:URLVariables = new URLVariables();
            variables.FILE_NAME = name;
            request.data = variables;
            request.method = URLRequestMethod.POST;
  			navigateToURL(request,"_blank");
         } 
		//*===================== FIN LISTE LIGNES =============================*//
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//*==================== ONGLET ========================================*//
		private var _ligne : Ligne;
		public function get ligne():Ligne{
			return _ligne;
		}		
		public function set ligne(lg : Ligne):void{			
			_ligne = lg;
			
			
			//Estancie la gestion de l'onglet générale---------------------------------
			if (gestionOngletGenerale != null)gestionOngletGenerale = null;
			gestionOngletGenerale = new GestionOngletGenerale(_ligne);
			gestionOngletGenerale.listeLignes = listeLignes;
			gestionOngletGenerale.helpers = this.viewHelper;
			
			//-------------------------------------------------------------------------			
			
			
			//Configuration de la fiche affectation ----------------------------------			
			if (gestionAffectationStrategy != null) gestionAffectationStrategy = null;
			switch(_ligne.TYPE_FICHELIGNE){
				case TYPE_FICHE_MOBILE : {
					gestionAffectationStrategy = new GestionAffectationMobile(_ligne);
					break;
				}
				default:{
					gestionAffectationStrategy = new GestionAffectationDefaut(_ligne);
					break;
				}
			}			
			//-----------------------------------------------------------------------
			
			
			
			
			//Estancie la gestion de l'onglet Etat------------------------------------ 
			if (gestionEtat != null)gestionEtat  = null;
			gestionEtat = new GestionEtatRaccordement();
			gestionEtat.helpers = viewHelper;
			gestionEtat.ligne = _ligne;
			//-----------------------------------------------------------------------
			
			
			//Estancie la gestion de l'onglet Cablage------------------------------------ 
			if (gestionCablage!= null)gestionCablage = null;
			gestionCablage = new GestionCablage();
			gestionCablage.selectedLigne = _ligne;
			//-----------------------------------------------------------------------
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//*===================== ONGLET GENRALE ==================================*//
		private var _gestionOngletGenerale : GestionOngletGenerale;
		public function get gestionOngletGenerale():GestionOngletGenerale{
			return _gestionOngletGenerale;
		}
		public function set gestionOngletGenerale(gestionOnglet : GestionOngletGenerale):void{
			
			_gestionOngletGenerale = gestionOnglet;
		}
		//*===================== FIN ONGLET LIGNE ==============================*//
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				
		
		
		
		
		
		
		//*==================== ONGLET ETAT ====================================*//
		
		private var _gestionEtat : GestionEtatRaccordement;
		public function get gestionEtat():GestionEtatRaccordement{
			return _gestionEtat;
		}
		public function set gestionEtat(donnee : GestionEtatRaccordement):void{
			_gestionEtat = donnee;
		}
		
		//*==================== FIN ONGLET ETAT ================================*//
		
		
		
		
		
		
		
		
		
		
		
		//*==================== ONGLET AFFECTATION ===================================*//		
		private var _gestionAffectationStrategy : GestionAffectationStrategy;
		public function get gestionAffectationStrategy():GestionAffectationStrategy{
			return _gestionAffectationStrategy;
		}
		public function set gestionAffectationStrategy(gestionAffectation : GestionAffectationStrategy):void{
			_gestionAffectationStrategy = gestionAffectation;
		}		
		//*==================== FIN ONGLET AFFECTATION ===============================*//
		
		
		
		
		//================ ONGLET CABLAGE =============================================//
		private var _gestionCablage : GestionCablage;
		public function get gestionCablage():GestionCablage{
			return _gestionCablage;
		}
		public function set gestionCablage(gestion : GestionCablage):void{
			_gestionCablage = gestion;
		}
		//============ FIN ONGLET CABLAGE =============================================//
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		private function doMapping(src : Object, dest : Object):void{
			var classInfo : XML = describeType(dest);
			
			
            for each (var v:XML in classInfo..accessor) {
               if (src.hasOwnProperty(v.@name) && src[v.@name] != null){
               		dest[v.@name] = src[v.@name];
               }
            }            				 		
		}
	}
}