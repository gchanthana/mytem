<cfcomponent name="equipement">
	<cffunction name="setData" access="remote" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="q" type="query">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.parametres.perimetres')>
			<cfif IsDefined('session.parametres.perimetres.#cle#')>
				<cfset structDelete(session.parametres.perimetres,"#cle#")>
				<cfset StructInsert(session.parametres.perimetres,cle,q)>
		
			<cfelse>
				<cfset structInsert(session.parametres.perimetres,"#cle#",q)>
			</cfif>
		<cfelse>
			<cfset st=structNew()>
			<cfset structInsert(st,"#cle#",q)>
			<cfset session.parametres.perimetres=st>
		</cfif>
	</cffunction>
	
	<cffunction name="setAnyData" access="remote" returntype="void">
		<cfargument name="s" type="any">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.parametres.perimetres')>
			<cfif IsDefined('session.parametres.perimetres.#cle#')>
				<cfset structDelete(session.parametres.perimetres,"#cle#")>
				<cfset StructInsert(session.parametres.perimetres,cle,s)>
			<cfelse>
				<cfset structInsert(session.parametres.perimetres,"#cle#",s)>
			</cfif>
		<cfelse>
			<cfset st=structNew()>
			<cfset structInsert(st,"#cle#",s)>
			<cfset session.parametres.perimetres=st>
		</cfif>
	</cffunction>
		
	<cffunction name="clearData" access="remote" returntype="void" >
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.parametres.perimetres')>
			<cfif IsDefined('session.parametres.perimetres.#cle#')>
				<cfset structDelete(session.parametres.perimetres,"#cle#")>
			</cfif>
		</cfif>
	</cffunction>
</cfcomponent>