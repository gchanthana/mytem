<cfcomponent author="Cedric" displayname="fr.consotel.consoview.M00.SessionModel" hint="Implémentation permettant de récupérer des données de session utilisateur (MyTEM)">
	<cffunction access="public" name="getUser" returntype="Struct" hint="Retourne une structure contenant les clés suivantes :<br>
	- USERID : Identifiant utilisateur<br>- USER_EMAIL : Email et login utilisateur<br>
	- USER : Prénom et Nom de l'utilisateur<br>-CODEAPP : Code application<br>
	- IDGLOBALIZATION : ID Langue<br>- GLOBALIZATION : Code Langue">
		<cfreturn {
			USERID=VAL(SESSION.USER.CLIENTACCESSID),USER_EMAIL=SESSION.USER.EMAIL,
			USER=SESSION.USER.NOM & " " & SESSION.USER.PRENOM, CODEAPP=VAL(SESSION.CODEAPPLICATION),
			IDGLOBALIZATION=VAL(SESSION.USER.IDGLOBALIZATION), GLOBALIZATION=SESSION.USER.GLOBALIZATION
		}>
	</cffunction>
	
	<cffunction access="public" name="getIdGroupe" returntype="Numeric" hint="Retourne l'identifiant de la racine de la session">
		<cfreturn VAL(SESSION.PERIMETRE.ID_GROUPE)>
	</cffunction>
	
	<cffunction access="public" name="getGroupe" returntype="String" hint="Retourne le libellé du groupe">
		<cfreturn SESSION.PERIMETRE.GROUPE>
	</cffunction>
	
	<cffunction access="public" name="setRemoteInfos" returntype="void" hint="Défini les infos du client distant">
		<cfargument name="remoteInfos" type="String" required="false" default="" hint="Infos du client distant">
		<cfif TRIM(ARGUMENTS.remoteInfos) NEQ "">
			<cfset var remoteClientInfos={}>
			<cfset var appName="N.D">
			<cfset var remoteHost=CGI.REMOTE_HOST>
			<cfif isDefined("COOKIE") AND structKeyExists(COOKIE,"IP_ORIGINE")>
				<cfset remoteHost=urlDecode(COOKIE.IP_ORIGINE)>
			</cfif>
			<cfset remoteClientInfos.remoteHost=remoteHost>
			<cfset remoteClientInfos.infos=URLDecode(ARGUMENTS.remoteInfos)>
			<cfif isDefined("APPLICATION")>
				<cfset appName=structKeyExists(APPLICATION,"applicationName") ? APPLICATION.applicationName:appName>
				<cfset remoteClientInfos.appName=appName>
				<cfif isDefined("SESSION")>
					<cfset remoteClientInfos.ID=getIdGroupe() & ":" & getUser().USERID>
					<cflock scope="SESSION" type="exclusive" timeout="120" throwontimeout="true">
						<cfif structKeyExists(SESSION,"USER")>
							<cfset SESSION.USER.CAPABILITIES=remoteClientInfos>
						<cfelse>
							<cfset SESSION.USER_CAPABILITIES=remoteClientInfos>
						</cfif>
					</cflock>
				</cfif>
			</cfif>
			<cflog type="information" file="eventgateway" text="///#serializeJSON(remoteClientInfos)#///">
		</cfif>
	</cffunction>
</cfcomponent>