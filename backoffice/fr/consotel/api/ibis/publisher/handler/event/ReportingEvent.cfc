<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.ReportingEvent" extends="fr.consotel.api.ibis.publisher.handler.event.AbstractReportingEvent"
hint="Implémentation des méthodes IReportingEvent qui ne sont pas implémentées dans fr.consotel.api.ibis.publisher.handler.event.AbstractReportingEvent
Les méthodes getReporting(), getDelivery(), getXDO() retournent une structure dont chaque clé est associée à tableau contenant la ou les valeurs correspondantes">
	<!--- fr.consotel.api.ibis.publisher.handler.event.IReportingEvent --->
	<cffunction access="public" name="getStatus" returntype="String" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getJob(),"STATUS")>
			<cfreturn getJob().STATUS>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le statut du reporting n'est pas correctement défini">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getReportingName" returntype="String" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfset var deliveryProps=getDelivery()>
		<cfif structKeyExists(deliveryProps,"REPORTING") AND isValid("Array",deliveryProps.REPORTING) AND (arrayLen(deliveryProps.REPORTING) EQ 1)>
			<cfreturn deliveryProps.REPORTING[1]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le nom du reporting n'est pas correctement défini">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getDeliveryType" returntype="String"
	hint="Retourne une chaine vide si le type n'est pas défini et une exception est levée s'il n'est pas correctement défini">
		<cfif structKeyExists(getDelivery(),"TYPE")>
			<cfif isValid("Array",getDelivery().TYPE) AND (arraylen(getDelivery().TYPE) EQ 1)>
				<cfreturn getDelivery().TYPE[1]>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le type de la stratégie n'est pas correctement défini">
			</cfif>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
		
	<cffunction access="public" name="getBipDNS" returntype="String" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getJob(),"BIP")>
			<cfreturn getJob().BIP>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le DNS du serveur BIP n'est pas correctement défini">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getBipSessionInterval" returntype="Numeric" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getJob(),"SESSION_INTERVAL")>
			<cfreturn getJob().SESSION_INTERVAL>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La durée de session du serveur BIP n'est pas correctement définie">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getBackOffice" returntype="String" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getJob(),"BACKOFFICE")>
			<cfreturn getJob().BACKOFFICE>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le nom du BackOffice n'est pas correctement défini">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getJob" returntype="Struct" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getEventData(),"JOB")>
			<cfreturn getEventData().JOB>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La clé JOB n'est pas correctement définie">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getReporting" returntype="Struct" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getEventData(),"REPORTING")>
			<cfreturn getEventData().REPORTING>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La clé REPORTING n'est pas correctement définie">
		</cfif>
	</cffunction>

	<cffunction access="public" name="getDelivery" returntype="Struct" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getEventData(),"DELIVERY")>
			<cfreturn getEventData().DELIVERY>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La clé DELIVERY n'est pas correctement définie">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getXDO" returntype="Struct" hint="Lève une exception si l'info correspondante n'est pas correctement définie">
		<cfif structKeyExists(getEventData(),"XDO")>
			<cfreturn getEventData().XDO>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La clé XDO n'est pas correctement définie">
		</cfif>
	</cffunction>
</cfcomponent>