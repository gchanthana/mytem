package gestionparc.ihm.action.extension.actionextechanger
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.extension.FicheExtEchangerIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionExtEchangerReceiver
	{
		private var fiche:FicheExtEchangerIHM;

		public function ActionExtEchangerReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheExtEchangerIHM();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
