package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Societe")]
	
	 
	[Bindable]
	public class Societe
	{
		public var groupeId:Number = 0;
		public var id:Number = 0;
		public var raisonSociale:String = "";
		
		public function Societe(){
			
		}
	}
}