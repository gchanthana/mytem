﻿<cfcomponent hint="Gestion du backoffice pour la fonctionnalité ListeClient"
			 extends="fr.consotel.consoview.M28.AbstractClassProc">

	<cffunction name="getListeClientContractuel" 
				displayname="getListeClientContractuel" 
				description="retourne la liste des clients facturés par SWD : client ou partenaire" 
				access="remote" 
				output="false" 
				returntype="query">
		<cfargument name="codeApp" default="0">
		<cfargument name="partenaireid" default="0">	
		<cfargument name="offreid" default="0">	
		<cfargument name="strRecherche" default="">			
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.listeClientContractuelV2">
			<cfprocparam value="#codeApp#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#partenaireid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#offreid#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#strRecherche#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult/>
	</cffunction>
	<cffunction name="getListeApplication" 
				displayname="getListeApplication" 
				description="retourne la liste des applications" 
				access="private" 
				output="false" 
				returntype="query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_M28.listeApplicationV2">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult/>
	</cffunction>
	<cffunction name="getListeOffre" 
				displayname="getListeOffre" 
				description="retourne la liste des offres liés à une application, ou toutes les offres si codeApp = 0" 
				access="private" 
				output="false" 
				returntype="query">
		<cfargument name="codeApp" default="0">				
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_M28.listeOffreV2">
			<cfprocparam value="#codeApp#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult/>
	</cffunction>
	<cffunction name="getListePartenaire" 
				displayname="getListePartenaire" 
				description="retourne la liste des partenaires ayant au moins une offre de l'application, ou tous les partenaires si codeApp = 0" 
				access="private" 
				output="false" 
				returntype="query">
		<cfargument name="codeApp" default="0">				
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_M28.listePartenaireV2">
			<cfprocparam value="#codeApp#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult/>
	</cffunction>

	<cffunction name="getData" 
				displayname="getData" 
				description="retourne les listes des partenaires, offres et application" 
				access="remote" 
				output="false" 
				returntype="Struct" >
		
		<cfset resultStruct = structNew()>		
					
		<cfset resultStruct.LISTEAPPLICATION = getListeApplication()>
		<cfset resultStruct.LISTEOFFRE = getListeOffre()>
		<cfset resultStruct.LISTEPARTENAIRE = getListePartenaire()>
		
		<cfreturn resultStruct/>
	</cffunction>
	
	<cffunction name="getDataFromApplication" 
				displayname="getDataFromApplication" 
				description="retourne les listes des partenaires et des offres liée à une application" 
				access="remote" 
				output="false" 
				returntype="Struct" >
		<cfargument name="codeApp" type="numeric" default="0">
			
		<cfset resultStruct = structNew()>		
					
		<cfset resultStruct.LISTEOFFRE = getListeOffre(codeApp)>
		<cfset resultStruct.LISTEPARTENAIRE = getListePartenaire(codeApp)>
		
		<cfreturn resultStruct/>
	</cffunction>
	

</cfcomponent>