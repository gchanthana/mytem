package commandemobile.popup
{
	import commandemobile.entity.Commande;
	import commandemobile.system.CommandeEvent;
	import commandemobile.system.GestionCommandeMobile;
	import commandemobile.system.GestionTransporteur;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	public class PopUpLivraisonImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------
		
		//COMPOSANTS
		public var txtNumeroTracking	:TextInput
		public var cbxTransporteurs		:ComboBox;
		
		//OBJECT
		private var _gestionTrans		:GestionTransporteur 	= new GestionTransporteur();
		private var _gestionCmd			:GestionCommandeMobile 	= new GestionCommandeMobile();
		public var cmd					:Commande;
		
		//VARIABLES GLOBALES
		public var ignored				:Boolean = false;
		
		//VARIABLES LOCALES
		public var listeTransporteurs	:ArrayCollection 		= new ArrayCollection();

//VARIABLES------------------------------------------------------------------------------

//METHODES PUBLICS-----------------------------------------------------------------------
		
		//CONSTRUCTEURS (>LISTNER)
		public function PopUpLivraisonImpl()
		{
			_gestionTrans.fournirListeTransporteurs();
			_gestionTrans.addEventListener("transporteursArrived",			transporteursArrivedHandler);
			_gestionCmd.addEventListener(CommandeEvent.COMMANDE_UPDATED,	closePopUpHandler);
		}

//METHODES PUBLICS-----------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function btnValiderClickHandler():void
		{
			if(checkIfComposantsIsNull())
			{
				ignored = true;
				_gestionCmd.majInfosLivraisonCommande(cmd);
			}
		}
		
		protected function btnIgnorerClickHandler():void
		{
			ignored = true;
			dispatchEvent(new Event("transporteurIsSelected"));
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}

//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------
		
		private function transporteursArrivedHandler(e:Event):void
		{
			if(_gestionTrans.listeTransporteurs.length > 1)
			{
				cbxTransporteurs.prompt = "Sélectionnez";
			}
			listeTransporteurs = _gestionTrans.listeTransporteurs;
			cbxTransporteurs.dataProvider = listeTransporteurs;
		}
		
		private function checkIfComposantsIsNull():Boolean
		{
			var bool:Boolean = true;
			if(txtNumeroTracking.text == null || txtNumeroTracking.text == "")
			{
				Alert.show("Veuillez saisir un numéro de tracking!","Consoview");
				bool = false;
			}
			else
			{
				if(cbxTransporteurs.selectedItem == null || cbxTransporteurs.selectedItem == "")
				{
					Alert.show("Veuillez saisir un transporteur!","Consoview");
					bool = false;
				}
				else
				{
					cmd.IDTRANSPORTEUR 			= cbxTransporteurs.selectedItem.IDTRANSPORTEUR;
					cmd.LIBELLE_TRANSPORTEUR 	= cbxTransporteurs.selectedItem.LIBELLE_TRANSPORTEUR;
					cmd.NUMERO_TRACKING			= txtNumeroTracking.text;
				}
			}
			return bool;
		}
		
		private function closePopUpHandler(e:Event):void
		{
			dispatchEvent(new Event("transporteurIsSelected"));
			PopUpManager.removePopUp(this);
		}

//METHODES PRIVATE-----------------------------------------------------------------------

	}
}