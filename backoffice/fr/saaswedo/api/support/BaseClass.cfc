<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.BaseClass" hint="Classe parent pour fr.saaswedo.api.support.*">
	<cffunction access="public" name="getInstance" returntype="fr.saaswedo.api.support.BaseClass"
	hint="Retourne une instance de cette classe. Permet de spécifier le serveur Zendesk et les credentiels qui seront utilisées par cette instance<br>
	Les autres méthodes ne fonctionnent que sur une instance retournée par cette méthode<br>
	Chaque appel à cette méthode met à jour la valeur des paramètres correspondant au login, token et url du serveur Zendesk">
		<cfargument name="userLogin" type="String" required="false" default="" hint="Login utilisateur pour Zendesk">
		<cfargument name="userToken" type="String" required="false" default="" hint="Token d'authentification à Zendesk">
		<cfargument name="serverUrl" type="String" required="false" default="" hint="URL du serveur zendesk <b>incluant le protocole et avec un / à la fin</b> e.g https://societe.zendesk.com/">
		<cfset VARIABLES["USER_LOGIN"]=ARGUMENTS["userLogin"]>
		<cfset VARIABLES["USER_TOKEN"]=ARGUMENTS["userToken"]>
		<cfset VARIABLES["SERVER_URL"]=ARGUMENTS["serverUrl"]>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="package" name="getAttachments" returntype="fr.saaswedo.api.support.zendesk.Attachments" hint="Retourne l'instance d'accès au service des Attachments">
		<cfset apiKey="ATTACHMENT_API">
		<cfif NOT structKeyExists(VARIABLES,apiKey)>
			<cfset VARIABLES[apiKey]=createObject("component","fr.saaswedo.api.support.zendesk.Attachments")>
			<cfset VARIABLES[apiKey].setRequestAuthorization(getUserLogin(),getUserToken(),getServerUrl())>
		</cfif>
		<cfreturn VARIABLES[apiKey]>
	</cffunction>
	
	<cffunction access="package" name="getOrganizations" returntype="fr.saaswedo.api.support.zendesk.Organizations" hint="Retourne l'instance d'accès au service des Organizations">
		<cfset apiKey="ORGANIZATION_API">
		<cfif NOT structKeyExists(VARIABLES,apiKey)>
			<cfset VARIABLES[apiKey]=createObject("component","fr.saaswedo.api.support.zendesk.Organizations")>
			<cfset VARIABLES[apiKey].setRequestAuthorization(getUserLogin(),getUserToken(),getServerUrl())>
		</cfif>
		<cfreturn VARIABLES[apiKey]>
	</cffunction>
	
	<cffunction access="package" name="getUsers" returntype="fr.saaswedo.api.support.zendesk.Users" hint="Retourne l'instance d'accès au service des Tickets">
		<cfset apiKey="USER_API">
		<cfif NOT structKeyExists(VARIABLES,apiKey)>
			<cfset VARIABLES[apiKey]=createObject("component","fr.saaswedo.api.support.zendesk.Users")>
			<cfset VARIABLES[apiKey].setRequestAuthorization(getUserLogin(),getUserToken(),getServerUrl())>
		</cfif>
		<cfreturn VARIABLES[apiKey]>
	</cffunction>
	
	<cffunction access="package" name="getSearch" returntype="fr.saaswedo.api.support.zendesk.Search" hint="Retourne l'instance d'accès au service des Search">
		<cfset apiKey="SEARCH_API">
		<cfif NOT structKeyExists(VARIABLES,apiKey)>
			<cfset VARIABLES[apiKey]=createObject("component","fr.saaswedo.api.support.zendesk.Search")>
			<cfset VARIABLES[apiKey].setRequestAuthorization(getUserLogin(),getUserToken(),getServerUrl())>
		</cfif>
		<cfreturn VARIABLES[apiKey]>
	</cffunction>
	
	<cffunction access="package" name="getTickets" returntype="fr.saaswedo.api.support.zendesk.Tickets" hint="Retourne l'instance d'accès au service des Tickets">
		<cfset var apiKey="TICKET_API">
		<cfif NOT structKeyExists(VARIABLES,apiKey)>
			<cfset VARIABLES[apiKey]=createObject("component","fr.saaswedo.api.support.zendesk.Tickets")>
			<cfset VARIABLES[apiKey].setRequestAuthorization(getUserLogin(),getUserToken(),getServerUrl())>
		</cfif>
		<cfreturn VARIABLES[apiKey]>
	</cffunction>
	
	<cffunction access="package" name="getUserLogin" returntype="String" hint="Retourne le login utilisateur">
		<cfset var userLoginKey="USER_LOGIN">
		<cfif structKeyExists(VARIABLES,userLoginKey)>
			<cfreturn VARIABLES[userLoginKey]>
		<cfelse>
			<cfthrow type="Custom" message="Login utilisateur Zendesk non défini">
		</cfif>
	</cffunction>
	
	<cffunction access="package" name="getUserToken" returntype="String" hint="Retourne le token utilisateur">
		<cfset var propertyKey="USER_TOKEN">
		<cfif structKeyExists(VARIABLES,propertyKey)>
			<cfreturn VARIABLES[propertyKey]>
		<cfelse>
			<cfthrow type="Custom" message="Token utilisateur Zendesk non défini">
		</cfif>
	</cffunction>
	
	<cffunction access="package" name="getServerUrl" returntype="String" hint="Retourne l'url du serveur Zendesk incluant le protocole et se terminant par /">
		<cfset var propertyKey="SERVER_URL">
		<cfif structKeyExists(VARIABLES,propertyKey)>
			<cfreturn VARIABLES[propertyKey]>
		<cfelse>
			<cfthrow type="Custom" message="URL du serveur Zendesk non défini">
		</cfif>
	</cffunction>
</cfcomponent>