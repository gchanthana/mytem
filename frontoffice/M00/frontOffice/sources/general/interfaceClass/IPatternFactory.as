package general.interfaceClass
{
	import flash.display.DisplayObject;

	public interface IPatternFactory
	{
		function execute():DisplayObject;
	}
}