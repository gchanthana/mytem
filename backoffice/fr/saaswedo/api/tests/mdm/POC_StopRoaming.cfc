<cfcomponent author="Cedric" hint="POC d'activation/désactivation du roaming">
	<cffunction access="public" name="updateRoaming" returntype="void" hint="">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="phoneNumber" type="String" required="true" hint="Identifiant du device">
		<cfargument name="enableRoaming" type="Boolean" required="true" hint="TRUE pour activer le roaming et FALSE sinon">
		<cfset var idRacine=ARGUMENTS.idGroupe>
		<cfset var phone=ARGUMENTS.phoneNumber>
		<cfset var mdmInfos=getMdmServerInfos(idRacine)>
		<cfset var mdmVendor=mdmInfos.MDM_TYPE>
		<cfset var phoneMdmInfos=getPhoneMdmInfos(idRacine,phone)>
		<!--- MobileIron --->
		<cfif compareNoCase(mdmVendor,"MobileIron") EQ 0>
			<cfset var mdm=createObject("component","fr.saaswedo.api.tests.mdm.mobileiron.POC_StopRoaming")>
			<cfset mdm.updateRoaming(idRacine,phoneMdmInfos.ID_MDM,phoneMdmInfos.platform,ARGUMENTS.enableRoaming)>
		<!--- Citrix (Valeur: Zenprise) --->
		<cfelseif compareNoCase(mdmVendor,"Zenprise") EQ 0>
			<cfset var mdm=createObject("component","fr.saaswedo.api.tests.mdm.citrix.POC_StopRoaming")>
			<cfset mdm.updateRoaming(idRacine,phone,phoneMdmInfos.platform,ARGUMENTS.enableRoaming)>
		<cfelse>
			<cfthrow type="Custom" message="Invalid MDM Vendor #mdmVendor#">
		</cfif>
	</cffunction>
	
	<!--- Utilitaires parc MDM --->
	<cffunction access="private" name="getPhoneMdmInfos" returntype="Struct">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="phoneNumber" type="String" required="true" hint="Identifiant du device">
		<cfset var idRacine=ARGUMENTS.idGroupe>
		<cfset var phone=ARGUMENTS.phoneNumber>
		<cfset procedureName="PKG_MDM.getLineInfo">
		<cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#phone#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocresult name="_qMdmInfos">
		</cfstoredproc>
		
		<cfif _qMdmInfos.recordcount GT 0>
			<cfset _procedureName="PKG_MDM.getDeviceMdmInfos">
			<cfstoredproc datasource="ROCOFFRE" procedure="#_procedureName#">				
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#_qMdmInfos['NUM_SERIE'][1]#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#_qMdmInfos['IMEI'][1]#">
				<cfprocresult name="qMdmInfos">
			</cfstoredproc>
		<cfelse>
			<cfthrow type="Custom" errorCode="DEVICE_NOT_FOUND" message="serial #_qMdmInfos['NUM_SERIE'][1]#  imei #_qMdmInfos['IMEI'][1]# not found in MDM Pool (idGroupe: #idRacine#)">
		</cfif>
		
		<cfif qMdmInfos.recordcount GT 0>		
			<cfset var os=qMdmInfos["PLATFORM"][1]>
			<cfif findNoCase("iOS",os) GT 0>
				<cfset os="iOS">
			<cfelseif findNoCase("Android",os) GT 0>
				<cfset os="Android">
			<cfelse>
				<cfthrow type="Custom" errorCode="UNSUPPORTED_OS" message="Phone number '#phone#' os #os# is not supported">
			</cfif>
			<cfreturn {
				marque=qMdmInfos["MARQUE"][1],modele=qMdmInfos["MODELE"][1],platform=os,imei=qMdmInfos["IMEI"][1],
				num_serie=qMdmInfos["NUM_SERIE"][1],collaborateur=qMdmInfos["COLLABORATEUR"][1],matricule=qMdmInfos["MATRICULE"][1],
				username_mdm=qMdmInfos["USERNAME_MDM"][1],id_mdm=qMdmInfos["ID_MDM"][1],udid=qMdmInfos["UDID"][1],num_sim=qMdmInfos["NUM_SIM"][1],
				num_tel=qMdmInfos["NUM_TEL"][1],operateur=qMdmInfos["OPERATEUR"][1],
				mcc=qMdmInfos["MCC"][1],mnc=qMdmInfos["MNC"][1],imsi=qMdmInfos["IMSI"][1]
			}>
		<cfelse>
			<cfthrow type="Custom" errorCode="PHONE_NUMBER_NOT_FOUND" message="Phone number '#phone#' not found in MDM Pool (idGroupe: #idRacine#)">
		</cfif>
	</cffunction>
	
	<!--- Utilitaires : Infos serveur MDM --->
	
	<cffunction access="private" name="getMdmServerInfos" returntype="Struct" hint="Retourne les infos du serveur MDM de la racine">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfset var idRacine=ARGUMENTS.idGroupe>
		<cfset var dataSource="ROCOFFRE">
		<!--- Récupération des infos MDM de la racine --->
		<cfset var procedureName="PKG_MDM.get_server_racine_v2">
		<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
			<cfprocresult name="qMdmServerInfos">
		</cfstoredproc>
		<cfif (qMdmServerInfos.recordcount GT 0) AND (procedure_status GT 0)>
			<!--- Code d'erreur --->
			<cfset var code="MDM.ERROR">
			<!--- Structure des infos MDM : La valeur ramenée de la propriété MDM doit etre de la forme "PROVIDER://URI" --->
			<cfset var mdmServerInfos={
				MDM_TYPE=qMdmServerInfos["MDM_TYPE"][1],MDM=qMdmServerInfos["MDM"][1],USE_SSL=yesNoFormat(VAL(qMdmServerInfos["USE_SSL"][1])),
				USERNAME=qMdmServerInfos["USERNAME"][1],PASSWORD=qMdmServerInfos["PASSWORD"][1],USERID=VAL(qMdmServerInfos["USERID"][1]),
				ENROLL_USERNAME=qMdmServerInfos["ENROLL_USERNAME"][1],ENROLL_PASSWORD=qMdmServerInfos["ENROLL_PASSWORD"][1],
				IS_DEFAULT=yesNoFormat(VAL(qMdmServerInfos["IS_DEFAULT"][1])),ADMIN_EMAIL=qMdmServerInfos["ADMIN_EMAIL"][1],
				TEST_MSG=qMdmServerInfos["TEST_MSG"][1],IS_TEST_OK=yesNoFormat(VAL(qMdmServerInfos["IS_TEST_OK"][1]))
			}>
			<!--- Vérification de la valeur de la propriété MDM --->
			<cfset var mdmValue=mdmServerInfos.MDM>
			<cfif (TRIM(mdmValue) EQ "") OR (TRIM(mdmValue) NEQ mdmValue)>
				<cfset var extendedInfos=isDefined("qMdmServerInfos") ? serializeJSON(qMdmServerInfos):"">
				<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns invalid MDM '#mdmValue#' for idGroupe #idRacine#">
			<cfelse>
				<!--- Propriétés MDM et MDM_TYPE --->
				<cfset mdmServerInfos.MDM_TYPE=extractMdmProvider(mdmValue)>
				<cfset mdmServerInfos.MDM=extractMdm(mdmValue)>
				<!--- Traitement et conversion des valeurs (Le type Booléen de Java est utilisé pour compatibilité avec ActionScript) --->
				<cfset mdmServerInfos.IS_DEFAULT=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_DEFAULT ? "true":"false")>
				<cfset mdmServerInfos.USE_SSL=createObject("java","java.lang.Boolean").init(mdmServerInfos.USE_SSL ? "true":"false")>
				<cfset mdmServerInfos.IS_TEST_OK=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_TEST_OK ? "true":"false")>
				<cfreturn mdmServerInfos>
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns status #procedure_status# for idGroupe #idRacine#">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="extractMdmProvider" returntype="String" hint="Retourne la valeur du Provider MDM correspondant">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmProvider=mdmTokens[1]>
		<cfif TRIM(mdmProvider) NEQ mdmProvider>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmProvider#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid Provider #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmProvider>
	</cffunction>
	
	<cffunction access="private" name="extractMdm" returntype="String" hint="Retourne l'URI d'accès au MDM sans le protocole (ou le Scheme)">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmValue=mdmTokens[2]>
		<cfif TRIM(mdmValue) NEQ mdmValue>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmValue#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid URI #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmValue>
	</cffunction>
	
	<cffunction access="private" name="extractMdmTokens" returntype="Array" hint="Retourne les éléments du découpage d'une la valeur MDM d'origine">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=listToArray(ARGUMENTS.mdm,"://",FALSE,TRUE)>
		<cfif arrayLen(mdmTokens) EQ 2>
			<cfreturn mdmTokens>
		<cfelse>
			<cfset var code="MDM.ERROR">
			<cfthrow type="Custom" errorCode="#code#" message="Cannot extract Provider and URI tokens from invalid MDM value '#ARGUMENTS.mdm#'">
		</cfif>
	</cffunction>
</cfcomponent>