package carol.popUp
{
	import carol.event.CarolEvent;
	
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.managers.PopUpManager;

	public class PopupChargementPerimetreImpl extends VBox
	{
		public var ckx1:CheckBox
		public var ckx2:CheckBox
		
		public function PopupChargementPerimetreImpl()
		{
			super();
		}
		protected function valider():void
		{
			if(ckx1.selected)
				dispatchEvent(new CarolEvent(CarolEvent.OPEN_ANALYSE_DATENOTCHANGED));
			else if(ckx2.selected)
				dispatchEvent(new CarolEvent(CarolEvent.OPEN_ANALYSE_DATECHANGED));
			PopUpManager.removePopUp(this)
		}
	}
}