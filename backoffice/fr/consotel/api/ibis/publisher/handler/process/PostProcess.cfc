<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.process.PostProcess"
implements="fr.consotel.api.ibis.publisher.handler.IServiceHandler"
hint="Implementation de base pour IServiceHandler qui affiche seulement des messages de log. Fichier de Log : [CFUSION]/logs/POST_PROCESS.log<br>
L'évènement eventObject est supposé etre une implémentation de fr.consotel.api.ibis.publisher.handler.event.IReportingEvent<br>
La méthode createInstance() doit toujours être appelée en 1er en tant que constructeur (N'est pas spécifiée dans IServiceHandler)<br>
Les données retournée par getTargetInfos() ont été stockés par l'implémentation de IDelivery qui a été utilisée à l'exécution du reporting">
<!--- *********************************************************************************************************************** --->
	<!--- fr.consotel.api.ibis.publisher.handler.IServiceHandler --->
	<cffunction access="public" name="run" returntype="void"
	hint="Affiche en log EVENT_TARGET et l'ID de MAJ. Pour eventObject de type fr.consotel.api.ibis.publisher.handler.event.IReportingEvent<br>
	Un warning est affiché lorsque eventObject n'implémente pas fr.consotel.api.ibis.publisher.handler.event.IReportingEvent">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfset var eventTarget=ARGUMENTS.eventObject.getEventTarget()>
		<cfif isInstanceOf(ARGUMENTS.eventObject,getEventInterface())>
			<cfif UCase(ARGUMENTS.eventObject.getReportStatus()) NEQ "F">
				<cfset var idMaj=ARGUMENTS.eventObject.getId()><!--- ID de MAJ Evènement --->
				<cfset updateId(idMaj)><!--- MAJ ID --->
				<cflog text="PostProcess.run() - (ID : #idMaj#) : #eventTarget#" file="#getLog()#" type="information">
			</cfif>
		<cfelse>
			<cflog text="PostProcess.run() - Event is not IReportingEvent : #eventTarget#" file="#getLog()#" type="warning">
		</cfif>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.process.mail.MailHandler --->
	<cffunction access="private" name="updateId" returntype="void" hint="Appelle la procédure de MAJ de l'ID Evènement (ORIGINALE)">
		<cfargument name="eventId" type="numeric" required="true" hint="ID de MAJ Evènement">
		<cflog text="PostProcess.updateId() : #ARGUMENTS.eventId#" file="#getLog()#" type="information">
		<cfstoredproc datasource="#getDataSource()#" procedure="pkg_pfgp_suivi_chargement.valide_suivi_chargement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  value="#ARGUMENTS.eventId#"><!--- P_PARAM_TECH (TECH_PARAM) --->
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="codeRetour">
		</cfstoredproc>
		<cflog text="PostProcess.updateId() : #codeRetour#" file="#getLog()#" type="information">
		<cfif codeRetour LT 0>
			<cflog text="PostProcess.updateId() : #codeRetour#" file="#getLog()#" type="warning">
			<cfmail attributecollection="#getMailAttributes()#" subject="[*Warning*] PostProcess.updateId() : #ARGUMENTS.eventId#">
				Statut retourné par <b>pkg_pfgp_suivi_chargement.valide_suivi_chargement</b> pour l'ID : #ARGUMENTS.eventId#
			</cfmail>
		</cfif>
	</cffunction>
	
	
	<cffunction access="public" name="onError" returntype="void"
	hint="Affiche un log d'erreur. Pour eventObject de type fr.consotel.api.ibis.publisher.handler.event.IReportingEvent<br>
	Un warning est affiché lorsque eventObject n'implémente pas fr.consotel.api.ibis.publisher.handler.event.IReportingEvent">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfargument name="eventHandler" type="string" required="true">
		<cfargument name="exceptionObject" type="any" required="false" default="">
		<cfset var eventTarget=ARGUMENTS.eventObject.getEventTarget()>
		<cfif isInstanceOf(ARGUMENTS.eventObject,getEventInterface())>
			<cfset var idMaj=ARGUMENTS.eventObject.getId()><!--- ID de MAJ Evènement --->
			<cfset var exceptionMsg="Method run() FAILED">
			<cfif isDefined("ARGUMENTS.exceptionObject") AND isDefined("ARGUMENTS.exceptionObject.MESSAGE")>
				<cfset exceptionMsg=ARGUMENTS.exceptionObject.MESSAGE>
			</cfif>
			<cflog text="PostProcess.onError - (TARGET:#eventTarget#) : #exceptionMsg#" file="#getLog()#" type="error">
		<cfelse>
			<cflog text="PostProcess.onError() - Event is not IReportingEvent : #eventTarget#" file="#getLog()#" type="warning">
		</cfif>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.process.PostProcess --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Constructeur de cette classe">
		<cfset VARIABLES.NOTIFICATION_LOG="POST_PROCESS"><!--- Valeur file pour cflog --->
		<cfif NOT isDefined("VARIABLES.INITIALIZED")><!--- Flag d'initialisation --->
			<cfset VARIABLES.DATASOURCE="ROCOFFRE"><!--- Datasource --->
			<!--- Interface requise pour l'implémentation de IServiceEvent --->
			<cfset VARIABLES.EVENT_INTERFACE="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent">
			<!--- Mail --->
			<cfset VARIABLES.DOMAIN="consotel.fr">
			<cfset VARIABLES.MAIL={from="api@" & VARIABLES.DOMAIN, to="monitoring@saaswedo.com", type="html"}>
			<cfset VARIABLES.PERSISTENCE="pkg_m00.getmail"><!--- Persistance : Procédure Stockée (ROCOFFRE) --->
			<cfset VARIABLES.INITIALIZED=TRUE>
			<cflog text="PostProcess.createInstance() - COMPLETED : #getMetaData(THIS).NAME#" file="#getLog()#" type="information">
		</cfif>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="private" name="getMailAttributes" returntype="Struct" hint="Retourne la collection d'attribut pour CFMAIL (Pour les logs)">
		<cfreturn VARIABLES.MAIL>
	</cffunction>

	<cffunction access="private" name="getEventInterface" returntype="String" hint="Retourne le nom de l'interface attendue pour IServiceEvent">
		<cfreturn VARIABLES.EVENT_INTERFACE>
	</cffunction>

	<cffunction access="private" name="getDataSource" returntype="String" hint="Retourne le nom du data source pour la valeur de getPersistence()">
		<cfreturn VARIABLES.DATASOURCE>
	</cffunction>

	<cffunction access="private" name="getPersistence" returntype="String" hint="Retourne le nom de la procédure stockée qui est utilisée pour la persistance">
		<cfreturn VARIABLES.PERSISTENCE>
	</cffunction>
	
	<cffunction access="private" name="getLog" returntype="string" hint="Valeur de l'attribut file pour cflog">
		<cfreturn VARIABLES.NOTIFICATION_LOG>
	</cffunction>
	
	<cffunction access="private" name="getTargetInfos" returntype="query"
	hint="Retourne une requête contenant les infos concernant l'entité EVENT_TARGET (Redondant avec les propriétés de IServiceEvent). Entres autres :<br>
	- SYS_PARAM : XML (CLOB) contenant les paramètres du reporting (Paramètres métiers : DOSSIER, XDO, UserId, CodeApp, etc...)<br>
	- TECH_PARAM : XML (CLOB) contenant les paramètres du rapport BIP">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cftry>
			<cfstoredproc datasource="#getDataSource()#" procedure="#getPersistence()#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#ARGUMENTS.eventObject.getEventTarget()#">
				<cfprocresult name="eventTargetInfos">
			</cfstoredproc>
			<cfreturn eventTargetInfos><!--- pkg_m00.getmail --->
			<cfcatch type="any">
				<cflog text="PostProcess.getTargetInfos() : #CFCATCH.message#" file="#getLog()#" type="error">
				<cfmail attributecollection="#getMailAttributes()#" subject="[*Error*] PostProcess.getTargetInfos() : #CFCATCH.MESSAGE#">
					Implémentation IServiceHandler : #getMetaData(serviceHandler).NAME#<br>
					Exception : #CFCATCH.message#<br>
					Details : #CFCATCH.Detail#<br><br>
					<cfdump var="#CFCATCH.TagContext#" label="Tag Context">
				</cfmail>
				<cfreturn queryNew("ERROR","VARCHAR")><!--- Requête retournée en cas d'erreur --->
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>