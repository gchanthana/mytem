package appli.control {
	import appli.events.ConsoViewEvent;
	
	import composants.access.ListePerimetresWindow;
	import composants.access.perimetre.IPerimetreControl;
	import composants.access.perimetre.PerimetreEvent;
	import composants.util.ConsoviewUtil;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.Menu;
	import mx.controls.Tree;
	import mx.controls.menuClasses.IMenuDataDescriptor;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	
	public class CtrlBar extends CtrlBarIHM {
		private var displayFactureBox:DisplayFactureBoxImpl;
		private var componentReady:Boolean;
		private var universMenu:Menu;
		private var universMenuFunction:Menu;
		private var lastUniversKey:String;
		private var universFunctionData:Object;
		
		private var treeUniversMenu:Tree; // Arbre Menu des univers avec leurs fonctions
		private var treeUniversMenuRenderer:IFactory; // Renderer de l'arbre des menus
		
		private var menuUniversDataDesc:IMenuDataDescriptor; // Pour le menu des univers
		private var menuUniversFunctionDataDesc:IMenuDataDescriptor; // Pour le menu des fonctions des univers
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		private var positionPoint:Point;
		private var listePerimetreWindow:ListePerimetresWindow;
		
		public function getPerimetreControl():IPerimetreControl {
			return listePerimetreWindow;
		}
		
		public function getSelectedGroupIndex():int {
			return listePerimetreWindow.getSelectedGroupIndex();
		}

		public function getSelectedNode():XML {
			return listePerimetreWindow.getSelectedNode();
		}
		
		public function CtrlBar() {
			trace("(CtrlBar) Instance Creation");
			/*================= CODE ORIGINAL =================
			menuUniversFunctionDataDesc = new SimpleMenuFunctionDataDescriptor();
			componentReady = false;
			================= FIN CODE ORIGINAL =================*/
			positionPoint = new Point();
			universMenu = null;
			listePerimetreWindow = new ListePerimetresWindow();
			menuUniversDataDesc = new SimpleMenuDataDescriptor();
			treeUniversMenuRenderer = new ClassFactory(SimpleTreeMenuItemRenderer);
			
			treeUniversMenu = new Tree();
			treeUniversMenu.labelField = "@LBL";
			treeUniversMenu.doubleClickEnabled = true;
			treeUniversMenu.setStyle("folderOpenIcon",null);
			treeUniversMenu.setStyle("folderClosedIcon",null);
			treeUniversMenu.setStyle("defaultLeafIcon",null);
			treeUniversMenu.setStyle("indentation",5);
			treeUniversMenu.setStyle("openDuration",150);
			treeUniversMenu.setStyle("rollOverColor","#DDEEDD");
			treeUniversMenu.setStyle("selectionColor","#C1C1C1");
			treeUniversMenu.itemRenderer = treeUniversMenuRenderer;
			treeUniversMenu.dataDescriptor = new SimpleTreeMenuDataDescriptor();
			
			this.addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}

		private function afterCreationComplete(event:Event):void {
			trace("(CtrlBar) Performing IHM Initialization");
			listePerimetresBtn.addEventListener(MouseEvent.CLICK,displayListePerimetres);
			btnUnivers.addEventListener(MouseEvent.CLICK,displayMenuUnivers);
			listePerimetreWindow.addEventListener(ConsoViewEvent.GROUP_CHANGED,handleGroupChanged);
			listePerimetreWindow.addEventListener(PerimetreEvent.PERIMETRE_NODE_CHANGED,handleChangeNodeEvent);
			
			//initPerimetre();
			/*================= CODE ORIGINAL =================
			treeUniversMenu.visible = false;
			treeUniversMenu.addEventListener(MouseEvent.DOUBLE_CLICK,onTreeMenuAction);
			
			listePerimetreWindow = new ListePerimetresWindow();
			listePerimetreWindow.setStyle("borderStyle","solid");
			listePerimetreWindow.width = ((ConsoViewModuleObject.ConsoViewModuleObjectWidth * 35) / 45); // Coeff : 0.77
			listePerimetreWindow.height = ConsoViewModuleObject.ConsoViewModuleObjectHeight / 2;
			
			displayFactureBox = new DisplayFactureBoxImpl();

			listePerimetreWindow.addEventListener(PerimetreEvent.PERIMETRE_MODEL_INITIALIZED,onPerimetreInitialized);
			listePerimetresBtn.label = "En cours...";
			listePerimetresBtn.addEventListener(MouseEvent.CLICK,displayListePerimetres);
			btnUniversMenu.addEventListener(MouseEvent.CLICK,displayUniversMenu);
			btnUnivers.addEventListener(MouseEvent.CLICK,displayMenuUnivers);

			listePerimetreWindow.addEventListener(PerimetreEvent.PERIMETRE_CHANGING_GROUP,checkIhmAndData);
			listePerimetreWindow.addEventListener(PerimetreEvent.PERIMETRE_CHANGING_PERIMETRE,checkIhmAndData);
			listePerimetreWindow.addEventListener(PerimetreEvent.PERIMETRE_GROUP_CHANGED,checkIhmAndData);
			
			displayFactureBox.addEventListener(ConsoViewDataEvent.FACTURE_DEB_CHANGED,facturePeriodChange);
			displayFactureBox.addEventListener(ConsoViewDataEvent.FACTURE_FIN_CHANGED,facturePeriodChange);
			
			checkIhmAndData(null);
			================= FIN CODE ORIGINAL =================*/
		}

		public function updatePerimetre():void {
			trace("(CtrlBar) Updating Perimetre");
			listePerimetreWindow.updatePerimetre();
			listePerimetresBtn.label = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			
			// Update Univers menu : Remove event univers menu listeners, Create Menu with data provider, Update univers menu
			var menuDataProvider:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			if(universMenu != null) { // Remove Previous Menu if necessary !!!
				universMenu.removeEventListener(MenuEvent.CHANGE,changeUniversFunction);
				universMenu = null;
			}
			universMenu = Menu.createMenu(btnUnivers,menuDataProvider,false);
			universMenu.addEventListener(MenuEvent.CHANGE,changeUniversFunction);
			universMenu.dataDescriptor = menuUniversDataDesc;
			universMenu.labelField = "@LBL";
			/*================== A ENLEVER ==================
			btnUnivers.label = CvAccessManager.CURRENT_UNIVERS_FUNCTION_LABEL;
			listePerimetresBtn.enabled = btnUnivers.enabled = true;
			*/
			
			trace("(CtrlBar) Perimetre Updated");
		}
		
		private function displayMenuUnivers(event:Event):void {
			var tmpPoint:Point = btnUnivers.localToGlobal(positionPoint);
			universMenu.show(tmpPoint.x,(tmpPoint.y + btnUnivers.height));
		}

		private function displayListePerimetres(event:MouseEvent):void {
			trace("(CtrlBar) Display Liste Perimetres Window");
			var tmpPoint:Point = listePerimetresBtn.localToGlobal(positionPoint);
			ConsoviewUtil.scale((listePerimetreWindow as DisplayObject),ConsoViewModuleObject.W_ratio,ConsoViewModuleObject.H_ratio);
			PopUpManager.addPopUp(listePerimetreWindow,this,false);
			PopUpManager.bringToFront(listePerimetreWindow);
			listePerimetreWindow.x = tmpPoint.x;
			listePerimetreWindow.y = tmpPoint.y;
		}
		
		private function changeUniversFunction(event:MenuEvent):void {
			var i:int;
			var eventToDispatchType:String = null;
			var tmpCurrentUniversKey:String = CvAccessManager.CURRENT_UNIVERS;
			var node:XML = event.item as XML;
			var menuDataProvider:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var toggledNodes:XMLList = menuDataProvider.descendants("*").(@toggled == 'true');
			
			if(node.@toggled == "false") { // Univers or Function CHANGE
				btnUnivers.enabled = false;
				for(i=0; i < toggledNodes.length(); i++)
					toggledNodes[i].@toggled = false;
				if((node as XML).parent().@KEY != "ROOT") { // Node is a FUNCTION
					(node as XML).parent().@toggled = true;
					if((node as XML).parent().@KEY == tmpCurrentUniversKey) { // Univers is the same - Function changes
						eventToDispatchType = ConsoViewEvent.FUNCTION_CHANGED;
					} else { // Univers changes (Thus function changes
						eventToDispatchType = ConsoViewEvent.UNIVERS_FUNCTION_CHANGED;
					}
				} else { // Node is DEFAULT Univers
					eventToDispatchType = ConsoViewEvent.DEFAULT_UNIVERS_CHANGED;
				}
				node.@toggled = true;
				dispatchEvent(new ConsoViewEvent(eventToDispatchType));
			}
		}
		
		public function afterUniversFunctionUpdated(actionType:String):void {
			trace("(CtrlBar) Performing After Univers Function Updated Tasks - " + actionType);
			btnUnivers.label = CvAccessManager.CURRENT_UNIVERS_FUNCTION_LABEL;
			listePerimetresBtn.enabled = btnUnivers.enabled = true;
			trace("(CtrlBar) Univers and Function Status : " +
						CvAccessManager.CURRENT_UNIVERS + "|" + CvAccessManager.CURRENT_FUNCTION +
						"|" + CvAccessManager.getSession().CURRENT_ACCESS + "|" + actionType);
		}
		
		private function handleGroupChanged(event:Event):void {
			trace("(CtrlBar) Handling Group Changed Event - Re-Dispatch Handled Event");
			dispatchEvent(event);
		}
		
		private function handleChangeNodeEvent(event:Event):void {
			trace("(CtrlBar) Handling Change Node Event (From Perimetre Window)");
			var selectedNode:XML = listePerimetreWindow.getSelectedNode();
			/*
			trace("(CtrlBar) Selected Node Infos :\n" +
						"LBL : " + selectedNode.@LBL + "\n" +
						"NID : " + selectedNode.@NID);
			*/
			dispatchEvent(event);
		}
		
		//===============================================================================================================================
		
		private function checkIhmAndData(event:PerimetreEvent):void {
			/*
			if(event != null) {
				if(event.type == PerimetreEvent.PERIMETRE_GROUP_CHANGED)
					componentReady = true;
				else if((event.type == PerimetreEvent.PERIMETRE_CHANGING_PERIMETRE) ||
							(event.type == PerimetreEvent.PERIMETRE_CHANGING_GROUP)) {
					componentReady = false;
					listePerimetresBtn.label = "En cours...";
					listePerimetresBtn.enabled = false;
					btnUnivers.visible = btnUniversMenu.visible = false;
				}
			}
			if(this.initialized && (componentReady == true))
				dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_GROUP_CHANGED));
			*/
			dispatchChangeGroupEvent();
		}
		
		private function dispatchChangeGroupEvent():void {
			/*
			if(event != null) {
				if(event.type == PerimetreEvent.PERIMETRE_GROUP_CHANGED)
					componentReady = true;
				else if((event.type == PerimetreEvent.PERIMETRE_CHANGING_PERIMETRE) ||
							(event.type == PerimetreEvent.PERIMETRE_CHANGING_GROUP)) {
					componentReady = false;
					listePerimetresBtn.label = "En cours...";
					listePerimetresBtn.enabled = false;
					btnUnivers.visible = btnUniversMenu.visible = false;
				}
			}
			if(this.initialized && (componentReady == true))
				dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_GROUP_CHANGED));
			*/
			dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_GROUP_CHANGED));
		}		
		
		/*
		public function getSelectedGroupIndex():int {
			return listePerimetreWindow.getSelectedGroupIndex();
		}
		*/
		
		public function perimetreUpdated():void {
			//trace("(CtrlBar) Perimetre Update Success");
			//listePerimetreWindow.perimetreUpdated();
			/*
			recherchGlobale.onPerimetreChange();
			listePerimetreWindow.perimetreUpdated();
			displayFactureBox.perimetreUpdated();
			
			treeUniversMenu.dataProvider = CvAccessManager.getMainMenuDataProvider2();
			treeUniversMenu.callLater(onMenuTreeDataSet);
			*/
			
			/*================= CODE ORIGINAL =================
			treeUniversMenu.visible = false;
			
			listePerimetresBtn.label = "Périmètre: " + CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			listePerimetresBtn.enabled = true;
			// Menu des univers
            universMenu = Menu.createMenu(btnUnivers,CvAccessManager.getMainMenuDataProvider2(),false);
            universMenu.dataDescriptor = menuUniversDataDesc;
            universMenu.labelField = "@label";
            universMenu.addEventListener(MenuEvent.ITEM_CLICK,changeUnivers);
			var resultList:XMLList = (CvAccessManager.getMainMenuDataProvider2() as XML).children().(@toggled == true);
			if(resultList.length() == 0)
				universMenu.selectedItem = (CvAccessManager.getMainMenuDataProvider2() as XML).children()[0]; // Retour sur Accueil
			else
				universMenu.selectedItem = resultList[0];
			universMenu.selectedItem.@toggled = true;
			// Menu des fonctions de l'univers actuel
			setUniversMenuFunctionData(universMenu.selectedItem.@id);
			btnUnivers.visible = true;
			btnUnivers.label = universMenu.selectedItem.@label;
			recherchGlobale.onPerimetreChange();
			listePerimetreWindow.perimetreUpdated();
			displayFactureBox.perimetreUpdated();
			
			treeUniversMenu.dataProvider = CvAccessManager.getMainMenuDataProvider2();
			treeUniversMenu.callLater(onMenuTreeDataSet);
			================= FIN CODE ORIGINAL =================*/
		}
		
		public function logoff():void {
			/*
			if(this.initialized == true) {
				listePerimetresBtn.label = "En cours...";
				listePerimetresBtn.enabled = false;
				lastUniversKey = null;
				btnUniversMenu.label = "";
				btnUnivers.visible = btnUniversMenu.visible = false;
				
				if(displayFactureBox.initialized == true)
					displayFactureBox.logoff();
			}
			
			//allMenuTree.visible = false;
			this.recherchGlobale.logOff();
			universFunctionData = null;
			listePerimetreWindow.logoff();
			trace("CtrlBar logoff() : Déconnexion effectuée");
			*/
			listePerimetreWindow.logoff();
			recherchGlobale.logOff();
		}
		
// ====================================================================================================================









		/*

		public function getMenuTree():Tree {
			return treeUniversMenu;
		}
		
		private function onPerimetreInitialized(event:PerimetreEvent):void {
			dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_MODEL_INITIALIZED));
		}

		private function onMenuTreeDataSet():void {
			treeUniversMenu.visible = true;
			treeUniversMenu.expandItem(treeUniversMenu.dataProvider[0],true);
			treeUniversMenu.selectedItem = (treeUniversMenu.dataProvider[0] as XML).children()[0];
		}

		private function onTreeMenuAction(event:MouseEvent):void {
			// Univers en cours :
			var currentUniversNode:XML;
			// Nouvel Univers :
			var universNode:XML;
			// Fonction actuelle :
			var currentFunctionNode:XML;
			// Nouvelle fonction :
			var functionNode:XML;
			//var sourceXmlData:XML = CvAccessManager.getMainMenuDataProvider2() as XML;
			var sourceXmlData:XML = null;
			if(treeUniversMenu.selectedItem.@enabled == true) {
				if(treeUniversMenu.selectedItem.@groupName == "menuUniversGroup") { // Choix d'un univers
					if((treeUniversMenu.selectedItem.@id.toString() != "ROOT") &&
						(treeUniversMenu.selectedItem.@id.toString() != UniversFactory.UNIVERS_ACCUEIL)) {
						currentUniversNode = sourceXmlData.children().(@toggled == true)[0] as XML;
						universNode = sourceXmlData.children().(@id == treeUniversMenu.selectedItem.@id)[0] as XML;
						currentUniversNode.@toggled = false;
						universNode.@toggled = true;
						universMenu.selectedItem = universNode;
						changeUnivers(null); // Changement d'univers
					}
				} else { // Choix d'une fonction d'univers
					if((treeUniversMenu.selectedItem as XML).parent().@enabled == true) {
						currentUniversNode = sourceXmlData.children().(@toggled == true)[0] as XML;
						universNode = (treeUniversMenu.selectedItem as XML).parent() as XML;
						// Séléction de l'univers :
						currentUniversNode.@toggled = false;
						universNode.@toggled = true;
						universMenu.selectedItem = universNode;
						// Séléction de la fonction :
						currentFunctionNode = universNode.children().(@toggled == true)[0] as XML;
						functionNode = universNode.children().(@id == treeUniversMenu.selectedItem.@id)[0] as XML;
						currentFunctionNode.@toggled = false;
						functionNode.@toggled = true;
						changeUnivers(null); // Changement d'univers
						trace("CtrlBar onTreeMenuAction() : Passer de " +
								currentUniversNode.@id.toString() + " vers " + universNode.@id + " - " +
								treeUniversMenu.selectedItem.@label + " - " + treeUniversMenu.selectedItem.@id.toString());
					}
				}
				treeUniversMenu.selectedItem = (treeUniversMenu.dataProvider[0] as XML).children()[0];
			}
		}

		public final function getDisplayFactureBox():DisplayFactureBoxImpl {
			return displayFactureBox;
		}

		private function setUniversMenuFunctionData(universKey:String):void {
			//var mainMenuData:XML = CvAccessManager.getMainMenuDataProvider2() as XML;
			var mainMenuData:XML = null;
			var universData:XMLList = mainMenuData.children().(@id == universKey);
			var menuFunctionData:XML = universData[0] as XML; // Noeud Univers
			var resultList:XMLList;

			//var menuFunctionData:XML = getUniversMenuFunctionData(universKey) as XML;
			if(menuFunctionData.children().length() > 0) {
	            universMenuFunction = Menu.createMenu(btnUniversMenu,menuFunctionData,false);
	            universMenuFunction.dataDescriptor = menuUniversFunctionDataDesc;
	            universMenuFunction.labelField = "@label";
	            universMenuFunction.addEventListener(MenuEvent.ITEM_CLICK,changeUniversFunction);
				resultList = menuFunctionData.children().(@toggled == true);
				if(resultList[0].@enabled == false) { // Si on n'a pas accès à cette fonction
					resultList[0].@toggled = false;
					resultList = menuFunctionData.children().(@enabled == true);
					resultList[0].@toggled = true;
				}
				if(ConsoViewModuleObject.AUTO_SELECT_FIRST_FUNCTION == true) { // Au changement d'univers toujours la 1ère fonction
					if(lastUniversKey == universKey)
						universMenuFunction.selectedItem = resultList[0];
					else {
						resultList[0].@toggled = false;
						universMenuFunction.selectedItem = menuFunctionData.children()[0];
						universMenuFunction.selectedItem.@toggled = true;
					}
				} else { // Au changement d'univers toujours la dernière fonction
					universMenuFunction.selectedItem = resultList[0];
					universFunctionData.universFunctionId = universMenuFunction.selectedItem.@id;
					universFunctionData.universFunctionLabel = universMenuFunction.selectedItem.@label;
				}
				btnUniversMenu.visible = true;
				btnUniversMenu.label = universMenuFunction.selectedItem.@label;
			} else
				btnUniversMenu.visible = false;
			// Evènement du changement de fonction :
			if(lastUniversKey != universKey) { // Changement d'univers donc aussi de fonction
				universFunctionData.universKey = universKey;
				if(menuFunctionData.children().length() <= 0) { // Univers n'ayant pas de fonction (ex: Accueil)
					universFunctionData.universFunctionId = 0;
					universFunctionData.universFunctionLabel = "";
				}
			}
			lastUniversKey = universKey;
		}
		
		private function facturePeriodChange(event:ConsoViewDataEvent):void {
			var tmpEvent:ConsoViewDataEvent;
			if(event.type == ConsoViewDataEvent.FACTURE_DEB_CHANGED) {
				tmpEvent = new ConsoViewDataEvent(ConsoViewDataEvent.FACTURE_DEB_CHANGED,event.data);
				dispatchEvent(tmpEvent);
			} else if(event.type == ConsoViewDataEvent.FACTURE_FIN_CHANGED) {
				tmpEvent = new ConsoViewDataEvent(ConsoViewDataEvent.FACTURE_FIN_CHANGED,event.data);
				dispatchEvent(tmpEvent);
			}
		}
		
		private function displayUniversMenu(event:MouseEvent):void {
			var tmpPoint:Point = btnUniversMenu.localToGlobal(positionPoint);
			universMenuFunction.show(tmpPoint.x,(tmpPoint.y + btnUniversMenu.height));
		}

		private function changeUniversFunction(event:MenuEvent):void {
			universFunctionData.universKey = CvAccessManager.CURRENT_UNIVERS;
			universFunctionData.universFunctionId = universMenuFunction.selectedItem.@id;
			universFunctionData.universFunctionLabel = universMenuFunction.selectedItem.@label;
			btnUniversMenu.label = universMenuFunction.selectedItem.@label;
			if(universMenuFunction.selectedItem.@toggled == false) { // On a cliqué sur la même fonction
				universMenuFunction.selectedItem.@toggled = true;
			} else {
				universMenuFunction.selectedItem.@toggled = true;
				dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.UNIVERS_FUNCTION_CHANGED,universFunctionData));
			}
		}

		public function getUniversFunctionData():Object {
			return universFunctionData;
		}
		*/
	}
}
