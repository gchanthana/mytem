package gestionparc.ihm.action.equipement.actionEqpSav
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.IncidentVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.gestion.GestionCommandeMobile;
	import gestionparc.ihm.fiches.ficheEntity.FicheIncidentIHM;
	import gestionparc.ihm.fiches.popup.PopUpSelectCarrierIHM;
	import gestionparc.ihm.fiches.popup.PopUpSelectCarrierImpl;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.equipement.EquipementServices;

	public class ActionEqpSavReceiver
	{
		private const ID_MESPOOL:Number = -10;
		private const NO_PROFILE:Number = 0;
		private const GFC_MOVE_IDRACINE:Number = 7348803;

		
		private var ficheAddIncident:FicheIncidentIHM;
		
		private var srcObject:Object;
		private var gestionCommmande:GestionCommandeMobile;
		private var _equipServ:EquipementServices;
		private var _selectCarrier:PopUpSelectCarrierIHM;

		public function ActionEqpSavReceiver()
		{
			srcObject=new Object();
		}

		public function action():void
		{
			var currentIdRacine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("MASTER_PARC_GFC")
				&& (moduleGestionParcIHM.globalParameter.userAccess.MASTER_PARC_GFC == 1) 
				&& (currentIdRacine != GFC_MOVE_IDRACINE))		

			{
				if(SpecificationVO.getInstance().elementDataGridSelected.T_ID_STATUT==4)
				{
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etes_vous_s_r_de_vouloir_fermer_l_inc'), ResourceManager.getInstance().getString('M111', 'Fermer_l_incident'), alertClickHandler);					
				}	
				else
				{
					setSources(); 
				}
			}
			else
			{
				displayIHM();
			}			
		}
		
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				_equipServ= new EquipementServices();
				_equipServ.mettreEquipactive(SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL)
			}
		}

		private function displayIHM():void
		{
			if (SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL)
			{
				var ficheAddIncident:FicheIncidentIHM=new FicheIncidentIHM();
				ficheAddIncident.isAddIncident=true;
				ficheAddIncident.isSIM=false;
				ficheAddIncident.idEqpt=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
				ficheAddIncident.libelleEqpt=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
				ficheAddIncident.revendeurEqpt=SpecificationVO.getInstance().elementDataGridSelected.DISTRIBUTEUR;
				ficheAddIncident.idRevendeurEqpt=SpecificationVO.getInstance().elementDataGridSelected.T_IDREVENDEUR;
				ficheAddIncident.listIncidentEqpt=new ArrayCollection();
				ficheAddIncident.myIncident=new IncidentVO();
				PopUpManager.addPopUp(ficheAddIncident, Application.application as DisplayObject, true);
				PopUpManager.centerPopUp(ficheAddIncident);
			}
		}
		
		private function displayIHMCmd():void
		{
			gestionCommmande=new GestionCommandeMobile(srcObject);
			gestionCommmande.goSAV();
		}
		
		private function setSources(isPoolFromSpec:Boolean=true):void
		{
			
			
			if(isPoolFromSpec){
				srcObject.IDPOOL=SpecificationVO.getInstance().idPool;
				srcObject.IDPROFIL=SpecificationVO.getInstance().idProfil;
				srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeur;
				srcObject.LIBELLE_CONCAT=SpecificationVO.getInstance().labelPool + " - " + SpecificationVO.getInstance().labelProfil;
				srcObject.LIBELLE_POOL=SpecificationVO.getInstance().labelPool;
				srcObject.LIBELLE_PROFIL=SpecificationVO.getInstance().labelProfil;
			}
			
			srcObject.ligne=SpecificationVO.getInstance().elementDataGridSelected;
			srcObject.IDTYPECOMMANDE=13;	
			srcObject.BOOLNEWCOMMANDE=true;
			srcObject.IDEQUIPEMENT=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;			
			srcObject.IDSEGMENT=(SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE) ? 1 : 2;
			
			if(srcObject.ligne.OPERATEURID > 0 && srcObject.IDPROFIL != NO_PROFILE){//il y a un opérateur sur la ligne et on *n'est pas* dans le pool "Mes Polls" 				
				displayIHMCmd();
				return;
			}else if(srcObject.ligne.OPERATEURID > 0 && srcObject.IDPROFIL == NO_PROFILE){//il y a un opérateur sur la ligne et on *est* dans le pool "Mes Polls" 
				loadInfoPool(srcObject);
			}else{
				loadInfoPool(srcObject);
			}
			
		}
		
		private function selectCarrier(row:Object):void{
			
			var carrierList:ArrayCollection = CacheDatas.listTousOperateur;
			var message:String = "";
		 
			if(carrierList.length == 1)
			{
				row.ligne.OPERATEURID = carrierList[0].OPERATEURID;
				row.ligne.OPERATEUR = carrierList[0].NOM;
				displayIHMCmd();
			}else if(row.ligne.IDSOUS_TETE > 0){ //si une ligne est associé vérifié l'operateur dans la facture
				row.message =ResourceManager.getInstance().getString("M111","SAV");
				showSelectCarrierDBox(row);
			}else{
				//si un seul operateur le choisir et contiuner					
				//sinon ask for selection
				row.message = ResourceManager.getInstance().getString("M111","SAV");
				showSelectCarrierDBox(row);
			}
			//}
		}
		
		private function showSelectCarrierDBox(row:Object):void{
			
			if(_selectCarrier != null){
				if(_selectCarrier.isPopUp){
					PopUpManager.removePopUp(_selectCarrier);
				}
				_selectCarrier = null;
			}
			_selectCarrier = new PopUpSelectCarrierIHM();
			_selectCarrier.context = row;
			_selectCarrier.subtitle = row.message;
			_selectCarrier.carrierList = CacheDatas.listTousOperateur;
			_selectCarrier.addEventListener(CloseEvent.CLOSE,selectCarrierCloseHandler);
			_selectCarrier.addEventListener(PopUpSelectCarrierImpl.POPUPCARRIER_SUBMIT_EVENT,selectCarrierSubmitHandler);
			
			PopUpManager.addPopUp(_selectCarrier, Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(_selectCarrier);

		}
		
		private function selectCarrierCloseHandler(evt:CloseEvent):void{
			PopUpManager.removePopUp(_selectCarrier);
		}
		
		private function selectCarrierSubmitHandler(evt:Event):void{
			srcObject.ligne.OPERATEURID = _selectCarrier.selectedItem.idOperateur;
			srcObject.ligne.OPERATEUR = _selectCarrier.selectedItem.nom;
			PopUpManager.removePopUp(_selectCarrier);
			displayIHMCmd();
		}
		
		
		private function loadInfoPool(row:Object):void
		{
			var matrice:ArrayCollection = CacheDatas.matricePoolProfil;
			var idPool:Number = row.IDPOOL;
			
			//Si idpool = -10  ou si pas de profil alors on est dans le pool "Mes Pools" et l'idpool est celui de ligne du GRID
			//On cherche dans la matrice en cache le profil correspondant au pool  
			if(row.IDPOOL == ID_MESPOOL || row.IDPROFIL == NO_PROFILE){
				idPool = row.ligne.IDPOOL;
			}
			
			for(var i:int = 0; i < matrice.length; i ++)
			{
				if (idPool == matrice[i].IDPOOL){
					var message:String = "IDPOOL :" + idPool + "  " + "IDPROFIL :" + matrice[i].IDPROFIL;
					trace(message);
					localPoolChanged(matrice[i]);					
					break;
				}
			}
		}
		
		private var _myServicesCache:CacheServices;
		
		/** selectedPool
		 * 
		 *  ACTION = "Preparer la commande"
		 *	IDACTION = "2066"
		 *	IDPOOL = "51038"
		 *	IDPROFIL = "28215"
		 *	POOL = "35680 FERROV. INFRAPOLE BRETAGNE"
		 *	PROFIL = "Default Profile"
		 */
		private function localPoolChanged(selectedPool:Object):void
		{
			srcObject.IDPOOL = selectedPool.IDPOOL;
			srcObject.IDPROFIL = selectedPool.IDPROFIL;
			srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeur;
			srcObject.LIBELLE_CONCAT=selectedPool.POOL + " - " + selectedPool.PROFIL;
			srcObject.LIBELLE_POOL=selectedPool.POOL ;
			srcObject.LIBELLE_PROFIL=selectedPool.PROFIL;
			
			this._myServicesCache = CacheServices.getInstance();
			this._myServicesCache.myDatas.addEventListener(gestionparcEvent.RECUPERER_DROITUSER_FINISHED, localRecupDroitUserHandler);			
			this._myServicesCache.initDataInCache(selectedPool.IDPOOL,selectedPool.IDPROFIL);
		}
		
		private function localRecupDroitUserHandler(e:gestionparcEvent):void
		{
			CvAccessManager.getSession().INFOS_DIVERS.M111_CACHE=CacheDatas;
			//MYT-1606 >> pas besoin de faire ça >>> SpecificationVO.getInstance().initDroitCommandes(CacheDatas.listeDroitCommande);
			this._myServicesCache.myDatas.removeEventListener(gestionparcEvent.RECUPERER_DROITUSER_FINISHED, localRecupDroitUserHandler);
			//setSources(false);
			selectCarrier(srcObject);
		}
		
	}
}
