<cfcomponent displayname="FctGEST_COMM" output="false">
	
	<cffunction name="executeFct" access="public" returntype="struct" >
		<cfargument name="code" type="String" required="true" >
		<cfargument name="data" type="Struct" required="true" >
		
		<cftry>

			<cfset  obj=CreateObject("component","fr.consotel.consoview.M28.action.Act#code#").executeAct(data)>
				
			<cfreturn obj/>
			
			<!---<cfset  obj=CreateObject("component","fr.consotel.consoview.M28.action.Act#code#")>
        	
        	<cfif obj.haspermission() eq 1> 
            	<cfset  obj=CreateObject("component","fr.consotel.consoview.M28.action.Act#code#").execute(data)>
        	<cfelse>
            	<cfreturn ThrowCfCatchError("F000")>
        	</cfif>
        	
        	<cfreturn obj/>--->
			
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-1,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
				
			</cfcatch>
		</cftry>
	</cffunction>
	
</cfcomponent>