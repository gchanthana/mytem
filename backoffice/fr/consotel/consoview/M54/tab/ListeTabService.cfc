<cfcomponent displayname="Recuperer la liste des onglets selon le user" hint="ListeTabService" output="false">
	
	<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
	<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
	<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
	<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	
	<!--- 
		recuperer la liste de tous les onglets configurés par le utilisateur 
	--->
	<cffunction name="getListeTabByUserByRacine" access="remote" output="false" returntype="Query"
				hint="recuperer la liste de tous les onglets configurés par le utilisateur selon la racine">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m54.getOnglets">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_app_loginid" value="#idUser#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idracine" value="#idRacine#"><!--- p_idgroupe_client --->
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
		
	</cffunction>

</cfcomponent>