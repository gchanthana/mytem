<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" name="fr.consotel.consoview.M28.action.ActAL015" hint="retourne la liste des sites" author="Florian">

	<cffunction name="executeAct" access="remote" returntype="Struct" output="false" hint="retourne la liste des sites">
		<cfargument name="data" type="Struct" required="true" hint="contient IDPARTENAIRE (int)">
	
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.List_Site">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDPARTENAIRE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
				
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL015", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL015", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL015", 0, ThrowCfCatchError("AL015",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
	
	</cffunction>
	
</cfcomponent>