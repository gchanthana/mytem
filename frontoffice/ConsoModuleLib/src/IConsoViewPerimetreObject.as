package {
	public interface IConsoViewPerimetreObject {
		/*=============== GROUP INFOS ===============*/
//		function get GROUPE_IDX():int;
		
		function get GROUPE_INDEX():int;
		
		function get RACINE_LIBELLE():String;
		
//		function get IDCLIENTPV():int;
		
		function get BOOLINVENTAIRE():Boolean;
		
		function get CODE_STYLE():String;

		/*=============== PERIMETRE INFOS ===============*/
		function get DROIT_FOURNIS():Boolean;
		
		function get PERIMETRE_INDEX():int;
		
		function get TYPE_LOGIQUE():String;
		
//		function get TYPE_LOGIN():int;

		function get PERIMETRE_LIBELLE():String;
		
		function get TYPE_PERIMETRE():String;
		
		function get OPERATEURID():int;
		
		function get nodeList():XML;
		
		function get accessListData():XML;
		
		function get dateFirstFacture():Date;

		function get dateLastFacture():Date;

		function get displayDateDeb():Date;

		function get displayDateFin():Date;
		
		function get maxMonthToDisplay():Number;
	}
}
