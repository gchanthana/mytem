<cfcomponent output="false">
	
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	<cfproperty name="xmlParamUtil" type="any">
	<cfproperty name="biServiceUrl" type="numeric" >
	
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idRacineMaster = session.perimetre.IDRACINE_MASTER;
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
		variables.idUser = session.user.CLIENTACCESSID;
		variables.profileOperateur = 'Info Opérateur';
		variables.biServiceUrl = APPLICATION.BI_SERVER;
		variables.xmlParamUtil = createObject("component","fr.consotel.util.consoview.xml.XmlParamUtil");
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	
	<cffunction name="getidRacineMaster" output="false" access="public" returntype="numeric">
		<cfreturn variables.idRacineMaster>
	</cffunction>
	
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	<cffunction name="getidUser" output="false" access="public" returntype="numeric">
		<cfreturn variables.idUser>
	</cffunction>
	
	<cffunction name="getprofileOperateur" output="false" access="public" returntype="string">
		<cfreturn variables.profileOperateur>
	</cffunction>
	
	<cffunction name="logXmlParamIfSpecified" access="remote" returntype="void">
		<cfargument name="xmlParamObject" required="true" type="XML">
	</cffunction>
	
	<cffunction name="getXmlParamUtil" output="false" access="public" returntype="any">
		<cfreturn variables.xmlParamUtil>
	</cffunction>
	
	<cffunction name="getBiServiceUrl" output="false" access="public" returntype="string">
		<cfreturn variables.biServiceUrl>
	</cffunction>	
	
	<cffunction name="toInverseDateString" output="false" access="public" returntype="string">
		<cfargument name="laDate" type="any" required="true">
		<cfif isDate(laDate)>
			<cfreturn lsDateFormat(laDate,"YYYY/MM/DD")>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
</cfcomponent>