package univers.inventaire.equipements.lignes.fiche.ana.menu {
	import flash.events.Event;
	import mx.events.FlexEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.FaultEvent;
	import mx.collections.ArrayCollection;
	import flash.events.MouseEvent;
	
	
	public class Localisation extends LocalisationIHM {
		private var sousteteObject:Object;
		private var _data:Object;
		
		public function Localisation(sousteteObject:Object) {
			this.sousteteObject = sousteteObject
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:Event):void {
			nomsiteText.addEventListener(Event.CHANGE, txtChanged);
			adresse1Text.addEventListener(Event.CHANGE, txtChanged);
			zipcodeText.addEventListener(Event.CHANGE, txtChanged);
			communeText.addEventListener(Event.CHANGE, txtChanged);
			ficheSiteUpdateButton.addEventListener(MouseEvent.CLICK, btUpdateClicked);
			getFicheSite();
		}
		
		protected function txtChanged(event:Event):void
		{
			ficheSiteUpdateButton.enabled = true;
		}
		
		protected function btUpdateClicked(event:MouseEvent):void
		{
			var sitePhysique:String = _data.IDSITE_PHYSIQUE;
			var reference:String = _data.SP_REFERENCE;
			var code:String = _data.SP_CODE_INTERNE;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"updateFicheSite",
																				updateSiteResult, processError);
			RemoteObjectUtil.callService(op, sitePhysique, nomsiteText.text, reference, code,
									adresse1Text.text, "", zipcodeText.text, communeText.text);		
		}
		
		private function updateSiteResult(event:ResultEvent):void
		{
			ficheSiteUpdateButton.enabled = false;
		}
		
		private function getFicheSite():void
		{
			var idsoutete:String = sousteteObject.IDSOUS_TETE;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"getFicheSiteByLine",
																					getFicheSiteResult, processError);
			RemoteObjectUtil.callService(op, idsoutete);
		}
		
		private function getFicheSiteResult(event:ResultEvent):void
		{
			try
			{
				_data = ArrayCollection(event.result)[0];
				nomsiteText.text = _data.NOM_SITE;
				adresse1Text.text = _data.SP_ADRESSE1;
				zipcodeText.text = _data.SP_CODE_POSTAL;
				communeText.text = _data.SP_COMMUNE;
			}
			catch (error:Error) {}
			
		}
		
		private function processError(error:FaultEvent):void
		{
			
		}
	}
}
