package appli.Pilotage.CreateMenuPFGP
{
	import appli.Pilotage.CreateMenuPFGP.composant.MenuPFGPIHM;
	import appli.Pilotage.service.AppPFGPService;
	
	import composants.gradientcomponents.GradientBox;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import general.appliComposants.ClickableCheminPerimetre.ClickableCheminPerimetrePFGPIHM;
	import general.appliComposants.CvPerimetreNodeVO;
	import general.appliComposants.ListePerimetreWindow.ListePerimetresWindow;
	import general.appliComposants.ListePerimetreWindow.SimplePerimetreTree.SimplePerimetreTree;
	import general.event.PerimetreEvent;
	import general.interfaceClass.IMenu;
	import general.interfaceClass.IPerimetreControl;
	import general.service.InfosChargementService;
	
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.DateField;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;

	public class CreateMenuPFGPImpl extends GradientBox implements IMenu
	{
		[Bindable] protected var chemin:String
		
		[Bindable]public var infosChargementService:InfosChargementService;
		public var btnModifierPerimetre:Button
		public var ccp:ClickableCheminPerimetrePFGPIHM
		public var menu:MenuPFGPIHM;
		public var lblDate:Label;
		
		private var _boolAfficherDates:Boolean;
		private var positionPoint:Point;
		private var listePerimetreWindow:ListePerimetresWindow;
		
		public function CreateMenuPFGPImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		public function init(e:FlexEvent):void
		{
			infosChargementService = new InfosChargementService();
			infosChargementService.addEventListener("DATE_LOADED",setDateLabel);
			infosChargementService.getInfosDates();
			listePerimetreWindow = new ListePerimetresWindow();
//			listePerimetreWindow.addEventListener(PerimetreEvent.PERIMETRE_NODE_CHANGED,handleChangeNodeEvent);
//			listePerimetreWindow.addEventListener(ConsoViewEvent.GROUP_CHANGED,handleGroupChanged);
			positionPoint = new Point();
		}
	// Quand le perimetre change, on réinitialise le composant
		public function updatePerimetre():void
		{
			menu.updatePerimetre();
			listePerimetreWindow.updatePerimetre();
			initAffichagePerimetre()
		}
		public function updateMenu():void
		{
			menu.updatePerimetre();
		}
		private function setDateLabel(e:Event):void
		{
			lblDate.text = 'données du '+ DateField.dateToString(infosChargementService.dateInfos.LAST_LOAD_FACTURATION,"MM/YYYY");
		}
		public function afterUniversFunctionUpdated(actionType:String):void
		{
			if( CvAccessManager.CURRENT_UNIVERS == "HOME" || CvAccessManager.CURRENT_UNIVERS == "HOME_E0") 
			{				
				boolAfficherDates = true;
			}
			if( CvAccessManager.CURRENT_FUNCTION == "FACT_DASHBOARD_E0" || CvAccessManager.CURRENT_FUNCTION == "FACT_DASHBOARD_E0")
			{	
				boolAfficherDates = true;
			}
		} 
		public function checkIhmAndData(event:PerimetreEvent):void
		{
		} 
		public function dispatchChangeGroupEvent():void
		{
		} 
		public function perimetreUpdated():void
		{
		} 
		public function logoff():void
		{
		} 
		protected function btnTEMClickHandler():void
		{
			
		}
/*	------------------------------------------------------------------------------------------
								LISTEPERIMETREWINDOW
------------------------------------------------------------------------------------------  */		
		public function displayListePerimetres(event:MouseEvent):void 
		{
			PopUpManager.addPopUp(listePerimetreWindow,this.parentApplication as DisplayObject);
			PopUpManager.bringToFront(listePerimetreWindow);
			PopUpManager.centerPopUp(listePerimetreWindow)
		}
//		public function handleChangeNodeEvent(event:Event):void 
//		{
//			var selectedNode:XML = listePerimetreWindow.getSelectedNode();
//			ccp.updateAffichage((listePerimetreWindow.perimetreTree as SimplePerimetreTree).listeNode);
//			dispatchEvent(event);
//		}
//		public function handleGroupChanged(event:Event):void 
//		{
//			dispatchEvent(event);
//		}
		private function initAffichagePerimetre():void
		{
			var arr:Array = []
			var sess:ConsoViewSessionObject = CvAccessManager.getSession() as ConsoViewSessionObject
			var idperimetre:int = sess.CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var nodeListXml:XML = sess.CURRENT_PERIMETRE.nodeList;
			var xmlSelected:XMLList = nodeListXml.descendants("*").(@NID== idperimetre);
			var parentXml:Object = xmlSelected.parent();
		
		// Si nous sommes à la racine, on ajoute qu'un élément à l'affichage
			if(nodeListXml.@NID == idperimetre)
			{
				var x:CvPerimetreNodeVO = new CvPerimetreNodeVO()
				x.LBL = nodeListXml.@LBL
				x.NID = nodeListXml.@NID
				if(nodeListXml.@STC == 0)
					x.HASRIGHT = false
				else
					x.HASRIGHT = true
				arr.push(x)
			}	
		// Si nous sommes en sous périmètre, on récupère le parent
			else
			{
			// on ajoute le premier élément
				var x1:CvPerimetreNodeVO = new CvPerimetreNodeVO()
				x1.LBL = xmlSelected.@LBL
				x1.NID = xmlSelected.@NID
				if(xmlSelected.@STC == 0)
					x1.HASRIGHT = false
				else
					x1.HASRIGHT = true
				arr.push(x1)
			// tant qu'un parent existe, on ajoute un élément 	
				while(parentXml != null)
				{
					if(parentXml.@NID > 0)
					{
						var x2:CvPerimetreNodeVO = new CvPerimetreNodeVO()
						x2.LBL = parentXml.@LBL
						x2.NID = parentXml.@NID
						if(parentXml.@STC == 0)
							x2.HASRIGHT = false
						else
							x2.HASRIGHT = true
						arr.push(x2)
					}
					parentXml = parentXml.parent()	
				}
			}
			ccp.updateAffichage(arr)
		}
/*	------------------------------------------------------------------------------------------
									GETTER SETTER 
------------------------------------------------------------------------------------------  */	
		[Bindable]
		public function set boolAfficherDates(value:Boolean):void
		{	
			_boolAfficherDates = value;
		}

		public function get boolAfficherDates():Boolean
		{
			return _boolAfficherDates;
		}	
//		public function getSelectedGroupIndex():int {
//			return listePerimetreWindow.getSelectedGroupIndex();
//		}
		public function getSelectedNode():XML {
			return listePerimetreWindow.getSelectedNode();
		}
	}
}