<cfcomponent name="CvAccplicationTracker">
	<cfset appTracker = createObject("java","coldfusion.runtime.ApplicationScopeTracker")>
	<cfset sessionTracker = createObject("java","coldfusion.runtime.SessionTracker")>
	
	<cffunction name="getApplications" access="remote" returntype="any" output="true">
		<cfset appKeyList = appTracker.getApplicationKeys()>
		<cfset appArray = arrayNew(1)>
		<cfset i = 1>
		<cfset hasNext = appKeyList.hasMoreElements()>
		<cfloop condition="#hasNext#">
			<cfset tmpStruct = structNew()>
			<cfset tmpStruct.APPLICATION_KEY = appKeyList.nextElement()>
			<cfset tmpStruct.SERVER_NAME = #CGI.SERVER_NAME#>
			<cfset tmpStruct.SERVER_PORT = #CGI.SERVER_PORT#>
			<cfset tmpStruct.SERVER_SOFTWARE = #CGI.SERVER_SOFTWARE#>
			<cfset tmpApp = appTracker.getApplicationScope(tmpStruct.APPLICATION_KEY)>
			<cfset tmpStruct.ELAPSED_TIME_SECONDS = (tmpApp.getElapsedTime() / 1000)>
			<cfset appArray[i] = tmpStruct>
			<cfset hasNext = appKeyList.hasMoreElements()>
			<cfset i = i + 1>
		</cfloop>
		<cfwddx action="cfml2wddx" input="#appArray#" output="tmp_wddx_array"/>
		<cfwddx action="wddx2cfml" input="#tmp_wddx_array#" output="appArray"/>
		<cfreturn appArray>
	</cffunction>
	
	<cffunction name="getSessions" access="remote" returntype="any" output="true">
 		<cfargument name="APPLICATION_KEY" required="true" type="string"/>
		<cfset sessionCollection = sessionTracker.getSessionCollection(APPLICATION_KEY)>
		<cfset sessionKeys = sessionCollection.getNames()>
		<cfset sessionArray = arrayNew(1)>
		<cfset i = 1>
		<cfset hasNext = sessionKeys.hasMoreElements()>
		<cfloop condition="#hasNext#">
			<cfset currentSessionAppLabel = appDirectory[APPLICATION_KEY]>
			
			<cfset tmpSessionKey = sessionKeys.nextElement()>
			<cfset tmpSessionScope = sessionTracker.getSession(tmpSessionKey)>
  			<cfset tmpSessionObject = sessionCollection['#tmpSessionKey#']>
			<cfset tmpStruct = structNew()>
			<cfset tmpStruct.SESSION_KEY = tmpSessionKey>
			<cfset tmpStruct.APPLICATION_KEY = tmpSessionScope.getAppName()>
			<cfset tmpStruct.IS_ID_FROM_URL = tmpSessionScope.isIdFromURL()>
			<cfset tmpStruct.ELAPSED_TIME_SECONDS = (tmpSessionScope.getElapsedTime() / 1000)>
			<cfset tmpStruct.LAST_ACCESS_TIME_SECONDS = (tmpSessionScope.getTimeSinceLastAccess() / 1000)>
			<cfset tmpStruct.IS_EXPIRED = tmpSessionScope.expired()>
			<cfset tmpStruct.IS_NEW_SESSION = tmpSessionScope.isNew()>
			<cfset tmpStruct.IS_J2EE_SESSION = tmpSessionScope.isJ2eeSession()>
			<cfset sessionArray[i] = tmpStruct>
			<cfset hasNext = sessionKeys.hasMoreElements()>
			<cfset i = i + 1>
		</cfloop>
		<cfwddx action="cfml2wddx" input="#sessionArray#" output="tmp_wddx_array"/>
		<cfwddx action="wddx2cfml" input="#tmp_wddx_array#" output="sessionArray"/>
		<cfreturn sessionArray>
	</cffunction>
	
 	<cffunction name="getSessionsFromAppType" access="remote" returntype="any" output="true">
 		<cfargument name="APPLICATION_KEY" required="true" type="string"/>
 		<cfargument name="APPLICATION_STRATEGY_TYPE" required="true" type="string"/>
		<cfset sessionCollection = sessionTracker.getSessionCollection(APPLICATION_KEY)>
		<cfset sessionKeys = sessionCollection.getNames()>
		<cfset sessionArray = arrayNew(1)>
		<cfset i = 1>
		<cfset hasNext = sessionKeys.hasMoreElements()>
		<cfloop condition="#hasNext#">			
			<cfset tmpSessionKey = sessionKeys.nextElement()>
			<cfset tmpSessionScope = sessionTracker.getSession(tmpSessionKey)>
  			<cfset tmpSessionObject = sessionCollection['#tmpSessionKey#']>
			<cfset tmpStruct = structNew()>
			<cfset tmpStruct.SESSION_KEY = tmpSessionKey>
			<cfset tmpStruct.APPLICATION_KEY = tmpSessionScope.getAppName()>
			<cfset tmpStruct.IS_ID_FROM_URL = tmpSessionScope.isIdFromURL()>
			<cfset tmpStruct.ELAPSED_TIME_SECONDS = (tmpSessionScope.getElapsedTime() / 1000)>
			<cfset tmpStruct.LAST_ACCESS_TIME_SECONDS = (tmpSessionScope.getTimeSinceLastAccess() / 1000)>
			<cfset tmpStruct.IS_EXPIRED = tmpSessionScope.expired()>
			<cfset tmpStruct.IS_NEW_SESSION = tmpSessionScope.isNew()>
			<cfset tmpStruct.IS_J2EE_SESSION = tmpSessionScope.isJ2eeSession()>
			<!--- SpÃ©cifique ï¿½ ConsoView --->
			<cfif structKeyExists(tmpSessionObject,"REMOTE_ADDR")>
				<cfset tmpStruct.REMOTE_ADDR = tmpSessionObject.REMOTE_ADDR>
			<cfelse>
				<cfset tmpStruct.REMOTE_ADDR = "N.D">
			</cfif>
			<cfif structKeyExists(tmpSessionObject,"REMOTE_HOST")>
				<cfset tmpStruct.REMOTE_HOST = tmpSessionObject.REMOTE_HOST>
			<cfelse>
				<cfset tmpStruct.REMOTE_HOST = "N.D">
			</cfif>


			<cfif structKeyExists(tmpSessionObject,"STATUS")>
				<cfset tmpStruct.STATUS = tmpSessionObject.STATUS>
			<cfelse>
				<cfset tmpStruct.STATUS = 0>
			</cfif>
			
			<cfif structKeyExists(tmpSessionObject,"USER")>
				<cfif structKeyExists(tmpSessionObject.USER,"CLIENTACCESSID")>
					<cfset tmpStruct.CLIENTACCESSID = tmpSessionObject.USER.CLIENTACCESSID>
				<cfelse>
					<cfset tmpStruct.CLIENTACCESSID = 0>
				</cfif>
			<cfelse>
				<cfset tmpStruct.CLIENTACCESSID = 0>
			</cfif>
			
			<cfif structKeyExists(tmpSessionObject,"USER")>
				<cfif structKeyExists(tmpSessionObject.USER,"EMAIL")>
					<cfset tmpStruct.EMAIL = tmpSessionObject.USER.EMAIL>
				<cfelse>
					<cfset tmpStruct.EMAIL = "N.D">
				</cfif>
			<cfelse>
				<cfset tmpStruct.EMAIL = "N.D">
			</cfif>
			
			<cfif structKeyExists(tmpSessionObject,"USER")>
				<cfif structKeyExists(tmpSessionObject.USER,"NOM")>
					<cfset tmpStruct.NOM = tmpSessionObject.USER.NOM>
				<cfelse>
					<cfset tmpStruct.NOM = "N.D">
				</cfif>
			<cfelse>
				<cfset tmpStruct.NOM = "N.D">
			</cfif>
			
			<cfif structKeyExists(tmpSessionObject,"USER")>
				<cfif structKeyExists(tmpSessionObject.USER,"PRENOM")>
					<cfset tmpStruct.PRENOM = tmpSessionObject.USER.PRENOM>
				<cfelse>
					<cfset tmpStruct.PRENOM = "N.D">
				</cfif>
			<cfelse>
				<cfset tmpStruct.PRENOM = "N.D">
			</cfif>
			<!------------------------------>
			<cfset sessionArray[i] = tmpStruct>
			<cfset hasNext = sessionKeys.hasMoreElements()>
			<cfset i = i + 1>
		</cfloop>
		<cfwddx action="cfml2wddx" input="#sessionArray#" output="tmp_wddx_array"/>
		<cfwddx action="wddx2cfml" input="#tmp_wddx_array#" output="sessionArray"/>
		<cfreturn sessionArray>
	</cffunction>
	
	<cffunction name="getSessionsByKey" access="public" returntype="any" output="true">
 		<cfargument name="APPLICATION_KEY" required="true" type="string"/>
 		<cfargument name="SESSION_KEY" required="true" type="string"/>
		<cfset sessionCollection = sessionTracker.getSessionCollection(APPLICATION_KEY)>
		<cfset tmpSessionObject = sessionCollection['#SESSION_KEY#']>
		<cfreturn tmpSessionObject>
	</cffunction>
</cfcomponent>
