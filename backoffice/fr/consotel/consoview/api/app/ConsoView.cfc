<cfcomponent displayname="fr.consotel.consoview.api.app.ConsoView" extends="fr.consotel.consoview.api.app.AbstractApplication" hint="Application ConsoView">
	<cffunction access="public" name="invokeService" returntype="any" hint="Invokes service corresponding to serviceID with providen parameters">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
		<cfreturn getModuleInstance(arguments["moduleId"]).invokeService(arguments["serviceId"],arguments["parameters"])>
	</cffunction>
	
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.app.IApplication" hint="Instance Initializer : Explicit Call Only">
		<cflog type="information" text=">>> ConsoView Initialization USING AbstractApplication">
		<cfset SUPER.initInstance()>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="private" name="getModuleInstance" returntype="fr.consotel.consoview.api.app.IModule"
			hint="Returns module instance by passing its moduleId">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID">
		<cfif arguments["moduleId"] EQ "IBIS">
			<cfreturn createObject("component","fr.consotel.consoview.api.ibis.Ibis").initInstance()>
		<cfelse>
			<cflog type="error" text="UNSUPPORTED MODULE : #arguments.moduleId#">
			<cfabort showerror="ERROR IN fr.consotel.consoview.api.app.ConsoView.getServiceInstance() : SEE LOG FILES FOR DETAILS">
			<cfreturn "NULL">
		</cfif>
	</cffunction>

	<!--- 	
	<cffunction access="private" name="initContext" returntype="void" hint="Initialize Context Values">
		<cftry>
			<cfset VARIABLES["MAIL-SERVER"]="mail.consotel.fr:26">
			<cfset VARIABLES["MODULE-DIRECTORY"]="NULL">
			<cflog text="ConsoView Context Initialization : OK">
			<cfcatch type="any">
				<cflog type="error" text="AN ERROR DURING CONTEXT INITIALIZATION : SEE LOG FILES FOR DETAILS">
				<cfabort showerror="ERROR IN fr.consotel.consoview.api.app.ConsoView.initContext() : SEE LOG FILES FOR DETAILS">
			</cfcatch>
		</cftry>
	</cffunction>
	 --->
	
	<!--- 
	<cffunction access="public" name="initContext" returntype="void" hint="Initialize Context Values">
		<cfif structKeyExists(VARIABLES,"CLIENT") EQ FALSE>
			<cfset VARIABLES["CLIENT"]=createObject("component","fr.consotel.consoview.api.ibis.client.ClientCV")>
		</cfif>
		<cfif structKeyExists(VARIABLES,"REPORTING") EQ FALSE>
			<cfset VARIABLES["REPORTING"]=createObject("component","fr.consotel.consoview.api.ibis.ReportingService")>
		</cfif>
		<cfif structKeyExists(VARIABLES,"BACKOFFICE") EQ FALSE>
			<cfset VARIABLES["BACKOFFICE"]=createObject("component","fr.consotel.api.BackOffice")>
		</cfif>
		<cfif isDefined("SESSION")>
			<cfif structKeyExists(SESSION,"CONSOVIEW") EQ FALSE>
				<cfset putContextInSession()>
				<cflog text="|| Instance ConsoView - put context in session : OK">
			</cfif>
		</cfif>
		<cflog text="|| Instance ConsoView initialization : OK">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="putContextInSession" access="private" returntype="void">
		<cfset var context=structNew()>
		<cfset var keys=structNew()>
		<cfset keys["APPLICATION"]="APPLICATIONNAME,MAIL_SERVER,BI_SERVER,BI_SERVICE_URL">
		<cfset keys["CGI"]="SERVER_NAME,HTTP_HOST,REMOTE_HOST,SCRIPT_NAME">
		<cfset keys["SESSION"]="APPLICATION,SESSIONID,CREATED,REQUEST_DATE,URLTOKEN,IP_ADDR">
		<cfset keys["COOKIE"]="JSESSIONID">
		<cfset context["SESSION"]=structNew()>
		<cfset context["COOKIE"]=structNew()>
		<cfloop item="key" collection="#keys#">
			<cfset context["#key#"]=structNew()>
			<cfloop index="infos" list="#keys[key]#">
				<cfset value="-1">
				<cfif structKeyExists(evaluate("#key#"),infos)>
					<cfset value=evaluate("#key#.#infos#")>
				</cfif>
				<cfset structInsert(context[key],infos,value)>
			</cfloop>
		</cfloop>
		<cfset context["DEFAULT"]=getService("BACKOFFICE").getDefaultContext()>
		<cfset SESSION["CONSOVIEW"]=context>
	</cffunction>
	 --->
</cfcomponent>