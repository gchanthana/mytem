package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;

	public class RuleDissocierCollabForLigne implements IRuleDisplayItem
	{
		public function RuleDissocierCollabForLigne()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new HasCollaborateurAssociated();
			
			if(r1.isDisplay())
				return true;
			else
				return false;
		}
	}
}