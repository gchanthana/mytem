<cfcomponent output="true">
	
	<cffunction access="remote" output="true" name="listeRapportsEnDiffusion" returntype="query">
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset idLang  = SESSION.USER.IDGLOBALIZATION>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.listeRapportsEnDiffusion">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idRacine">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#" variable="p_code_langue">
			<cfprocresult name="qListeDiffusion">
		</cfstoredproc>				
		<cfreturn qListeDiffusion><!--- le resultat de la procedure --->
    </cffunction>
	
</cfcomponent>