package gestioncommande.events
{
	import flash.events.Event;
	
	public class EtapeEvent extends Event
	{
		public static const ETAPE_LISTE_COMMANDE		:String = "ETAPE_LISTE_COMMANDE";
		public static const ETAPE_NOUVL_COMMANDE		:String = "ETAPE_NOUVL_COMMANDE";
		public static const ETAPE_HISTO_COMMANDE		:String = "ETAPE_HISTO_COMMANDE";
		public static const ETAPE_MODIF_COMMANDE		:String = "ETAPE_MODIF_COMMANDE";
		public static const ETAPE_LIGNE_COMMANDE		:String = "ETAPE_LIGNE_COMMANDE";

		
		public function EtapeEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new EtapeEvent(type, bubbles, cancelable);
		}
	}
}