package event.destinataire
{
	import flash.events.Event;

	public class CreateProfilEvent extends Event
	{
		public var libelle:String;
		public var codeInterne:String;
		public var commentaire:String;
		
		public function CreateProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}