package utils
{
	import flash.filters.GlowFilter;

	public class CreateShadow
	{

		private var _glow:GlowFilter;

		private var _theColor:uint;
		private var _theAlpha:Number;
		private var _theBlurX:Number;
		private var _theBlurY:Number;
		private var _theInner:Boolean;
		private var _theQuality:int;
		private var _theStrength:Number;

		/**
		 * ci-dessous le lien pour plus de details
		 * http://livedocs.adobe.com/flash/9.0_fr/ActionScriptLangRefV3/flash/filters/GlowFilter.html
		 */
		public function CreateShadow(theColor:uint, theAlpha:Number, theBlurX:Number, theBlurY:Number, theInner:Boolean, theStrength:Number, theQuality:int)
		{
			this._theColor=theColor;
			this._theAlpha=theAlpha;
			this._theBlurX=theBlurX;
			this._theBlurY=theBlurY;
			this._theInner=theInner;
			this._theStrength=theStrength;
			this._theQuality=theQuality;
		}

		/**
		 * ajouter un shadow autour de l'objet passé en param
		 * @param obj
		 */
		public function addShadow(obj:Object):void
		{
			_glow=new GlowFilter();
			_glow.color=this._theColor;
			_glow.alpha=this._theAlpha;
			_glow.blurX=this._theBlurX;
			_glow.blurY=this._theBlurY;
			_glow.inner=this._theInner;
			_glow.strength=this._theStrength;
			_glow.quality=this._theQuality;

			obj.filters=[_glow];
		}

		/**
		 * enlever le shadow
		 * @param obj
		 */
		public function removeShadow(obj:Object):void
		{
			_glow=null;
			obj.filters=[];
		}
		public function get theStrength():Number
		{
			return _theStrength;
		}

		public function set theStrength(value:Number):void
		{
			_theStrength=value;
		}

		public function get theQuality():int
		{
			return _theQuality;
		}

		public function set theQuality(value:int):void
		{
			_theQuality=value;
		}

		public function get theInner():Boolean
		{
			return _theInner;
		}

		public function set theInner(value:Boolean):void
		{
			_theInner=value;
		}

		public function get theBlurY():Number
		{
			return _theBlurY;
		}

		public function set theBlurY(value:Number):void
		{
			_theBlurY=value;
		}

		public function get theBlurX():Number
		{
			return _theBlurX;
		}

		public function set theBlurX(value:Number):void
		{
			_theBlurX=value;
		}

		public function get theAlpha():Number
		{
			return _theAlpha;
		}

		public function set theAlpha(value:Number):void
		{
			_theAlpha=value;
		}

		public function get theColor():uint
		{
			return _theColor;
		}

		public function set theColor(value:uint):void
		{
			_theColor=value;
		}
	}
}
