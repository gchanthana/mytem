﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	
	<!--- Recuperer la liste des fonctions --->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true">
	
		<cftry>
				<cfstoredproc procedure="pkg_M28.getListFonction3" datasource="ROCOFFRE">
					<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#getCodeLangue()#">
					<cfprocresult name="p_retour" >
				</cfstoredproc>
			
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.LISTFCTS = p_retour>
				<cfreturn CreateReponse("AL003",1,data)/>
			 <cfelse>
                <cfset err = ThrowCfCatchError("PFCT0020","AL003","Erreur de procédure","","")>
                <cfreturn CreateReponse("AL003",0,err)/>
			</cfif>
			<cfcatch type="any" >
				<cfset err = ThrowCfCatchError("AL003",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
                <cfreturn CreateReponse("AL003",0,err)/>
			</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>