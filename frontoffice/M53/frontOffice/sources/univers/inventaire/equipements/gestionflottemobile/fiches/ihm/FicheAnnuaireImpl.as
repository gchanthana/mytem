package univers.inventaire.equipements.gestionflottemobile.fiches.ihm
{
	import composants.util.employes.Collaborateur;
	import composants.util.employes.EmployesUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;

	[Bindable]
	public class FicheAnnuaireImpl extends TitleWindow
	{		
		
		public var employeUtils:EmployesUtils = new EmployesUtils();
		
		
		public function FicheAnnuaireImpl()
		{
			super();	
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);		
		}
		
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btValiderClickHandler(event:MouseEvent):void		
		{ 
			updateData();
			
			
		}
		
		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{
			getData();
		}
		
		
		private function getData():void
		{
			 
			employeUtils.fournirDetailEmploye();
			
		}
		private function updateData():void
		{	
			employeUtils.addEventListener(EmployesUtils.EMPLOYE_UPDATED,employeUpdatedHandler);
			employeUtils.updateInfosEmploye();
			
		
		}
		
		private function employeUpdatedHandler(event:Event):void
		{
			dispatchEvent(new FicheEvent(FicheEvent.FICHE_UPDATED,true));
			PopUpManager.removePopUp(this);
		}
			

		public function set collaborateur(value:Collaborateur):void
		{
			employeUtils.selectedEmploye = value;
		}

		public function get collaborateur():Collaborateur
		{
			return employeUtils.selectedEmploye;
		}
		
		
	}
}