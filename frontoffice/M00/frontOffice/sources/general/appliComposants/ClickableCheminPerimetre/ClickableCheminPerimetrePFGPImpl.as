package general.appliComposants.ClickableCheminPerimetre
{
	import mx.containers.HBox;
	
	import general.appliComposants.CvPerimetreNodeVO;
	import general.appliComposants.NodeRenderer.NodeRendererPFGPIHM;

	public class ClickableCheminPerimetrePFGPImpl extends HBox
	{
		private static var LARGEURMAX:int = 700;
		
		[Bindable]public var lblChemin:String = "";
		
		public function ClickableCheminPerimetrePFGPImpl()
		{
			super();
		}
/*	------------------------------------------------------------------------------------------
									FONCTION
------------------------------------------------------------------------------------------  */		
		public function updateAffichage(listeNode:Array):void
		{
			this.removeAllChildren()
			lblChemin = ""
			if (listeNode != null && listeNode.length > 0)
			{
				var larg:int = 0
					
				for each(var node:CvPerimetreNodeVO in listeNode)
				{
					var nd:NodeRendererPFGPIHM = new NodeRendererPFGPIHM()
					nd.TITRE = node.LBL
					nd.NID = node.NID
					nd.ENABLED = node.HASRIGHT
					this.addChildAt(nd,0);
					lblChemin = node.LBL+"/"+lblChemin
					invalidateDisplayList()
				}
				var nd1:NodeRendererPFGPIHM = this.getChildAt(this.getChildren().length-1) as NodeRendererPFGPIHM
				nd1.ISLAST = true
			}
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight)
			if(width > LARGEURMAX)
			{
				this.removeChildAt(0)
				invalidateDisplayList()
			}
		}
/*	------------------------------------------------------------------------------------------
									GETTER SETTER 
------------------------------------------------------------------------------------------  */	
	}
}