package gestioncommande.services.organisation
{
	import flash.events.EventDispatcher;
	
	import gestioncommande.entity.OrgaVO;
	import gestioncommande.events.GestionParcMobileEvent;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * <b>Class <code>OrganisationDatas</code></b>
	 * <p><pre>
	 * <u>Description :</u>
	 * Traite les données reçues aux retours des procedures appelées dans <code>OrganisationServices</code>.
	 * 
	 * Auteur	: RENEL Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * 
	 * </pre></p>
	 *
	 * @see gestionparcmobile.services.organisation.OrganisationServices
	 * 
	 */
	public class OrganisationDatas extends EventDispatcher
	{
		// VARIABLES------------------------------------------------------------------------------
		
		private var _listeFeuilles			: ArrayCollection = null;
		private var _listeOrga				: ArrayCollection;
		private var _structureOrga			: String = "";
		
		private var _cheminSelected			: Object = null;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLICS FONCTIONS-------------------------------------------------------------------------

		/**
		 * <b>Fonction <code>treatDatasGetNodesOrga(obj:Object)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Traite les données reçues au retour de la procedure <code>getNodesOrga()</code>.
		 * </pre></p>
		 * 
		 * @param obj:<code>Object</code>.
		 * @return void.
		 * 
		 * @see gestionparcmobile.services.organisation.OrganisationServices#getNodesOrga()
		 * 
		 */	
		public function treatDatasGetNodesOrga(obj:Object):void
		{
			treatListeFeuille(obj[0]);
			treatStructureOrga(obj[1]);
			
			dispatchEvent(new GestionParcMobileEvent(GestionParcMobileEvent.LISTED_NODES_ORGA_LOADED));
		}
		
		/**
		 * <b>Fonction <code>treatGetListOrgaClienteResultHandler(myArray:ArrayCollection)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Créé la liste des organisations.
		 * </pre></p>
		 * 
		 * @param myArray:<code>ArrayCollection</code>.
		 * @return void.
		 */	
		public function treatGetListOrgaClienteResultHandler(obj:Object):void
		{
			var longueur : int = (obj as ArrayCollection).length;
			
			listeOrga = new ArrayCollection();
			
			for(var i :int = 0; i < longueur; i++)
			{
				var myOrga : OrgaVO = new OrgaVO();
				myOrga.libelleOrga = obj[i].LIBELLE_GROUPE_CLIENT;
				myOrga.idOrga = obj[i].IDGROUPE_CLIENT;
				
				listeOrga.addItem(myOrga);
			}

			dispatchEvent(new GestionParcMobileEvent(GestionParcMobileEvent.LISTE_ORGA_LOADED));
		}
	
		// PUBLICS FONCTIONS-------------------------------------------------------------------------

		// PRIVATES FONCTIONS-------------------------------------------------------------------------

		/**
		 * <b>Fonction <code>treatListeFeuille(myArray:ArrayCollection)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Affecte à la data "listeFeuilles" toutes les feuilles de l'organisation et leurs valeurs.
		 * </pre></p>
		 * 
		 * @param myArray:<code>ArrayCollection</code>.
		 * @return void.
		 */	
		private function treatListeFeuille(myArray:ArrayCollection):void
		{
			var objFeuille : Object;
			var longeur : int = myArray.length;
			
			listeFeuilles = new ArrayCollection();
			
			for(var i :int = 0 ; i < longeur ; i++)
			{
				objFeuille = new Object();
				objFeuille.chemin = myArray[i].CHEMIN;
				objFeuille.idFeuille = myArray[i].IDFEUILLE;
				
				listeFeuilles.addItem(objFeuille);
			}
		}
		
		/**
		 * <b>Fonction <code>treatStructureOrga(myArray:ArrayCollection)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Créé la structure de l'organisation et l'affecte à la data "structureOrga"
		 * </pre></p>
		 * 
		 * @param myArray:<code>ArrayCollection</code>.
		 * @return void.
		 */	
		private function treatStructureOrga(myArray:ArrayCollection):void
		{
			var longeur : int = myArray.length;
			
			for(var i :int = 0 ; i < longeur ; i++)
			{
				structureOrga += myArray[i].TYPE_NIVEAU + ";";
			}
		}
		
		// PRIVATES FONCTIONS-------------------------------------------------------------------------

		// PUBLICS PROPERTIES (GETTER/SETTER)-----------------------------------------------------

		public function set listeFeuilles(value:ArrayCollection):void
		{
			_listeFeuilles = value;
		}

		public function get listeFeuilles():ArrayCollection
		{
			return _listeFeuilles;
		}

		public function set structureOrga(value:String):void
		{
			_structureOrga = value;
		}

		public function get structureOrga():String
		{
			return _structureOrga;
		}

		public function set cheminSelected(value:Object):void
		{
			_cheminSelected = value;
		}

		public function get cheminSelected():Object
		{
			return _cheminSelected;
		}

		public function set listeOrga(value:ArrayCollection):void
		{
			_listeOrga = value;
		}

		public function get listeOrga():ArrayCollection
		{
			return _listeOrga;
		}
		
		// PUBLICS PROPERTIES (GETTER/SETTER)-----------------------------------------------------

	}
}