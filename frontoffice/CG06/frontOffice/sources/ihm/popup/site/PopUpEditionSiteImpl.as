package ihm.popup.site
{
	import composants.ui.TitleWindowBounds;
	import composants.util.CvDateChooser;
	
	import entity.CodeAction;
	import entity.SiteVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.controls.DateField;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.DateValidator;
	import mx.validators.StringValidator;
	import mx.validators.Validator;
	
	import service.FctGestionPartenaireService;
	
	public class PopUpEditionSiteImpl extends TitleWindowBounds
	{
		
		private var _iServiceGestPartenaire	:FctGestionPartenaireService;
		private var _idSelectdPartn:int;
		private var _selectedSite:SiteVO;
		
		public var sv_site			:StringValidator;
		public var dv_dateDebut		:DateValidator;
		[Bindable]
		public var ti_site			:TextInput;
		[Bindable]
		public var ti_adress1		:TextInput;
		public var ti_adress2		:TextInput;
		[Bindable]
		public var ti_codePostale	:TextInput;
		[Bindable]
		public var ti_ville			:TextInput;
		[Bindable]
		public var ti_pays			:TextInput;
		public var cbx_facturation	:CheckBox;
		[Bindable]
		public var df_dateDebut		:CvDateChooser;
		[Bindable]
		public var df_dateFin		:CvDateChooser;
		
		public function PopUpEditionSiteImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		private function init(event:FlexEvent):void
		{
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValider_clickHandler(evt:MouseEvent):void
		{
			if(validateEnteredSite() == true){	
				var modifiedSite:SiteVO = new SiteVO();
				modifiedSite.id = selectedSite.id;
				modifiedSite.libelle = ti_site.text;
				modifiedSite.adresse1 = ti_adress1.text;
				modifiedSite.adresse2 = ti_adress2.text;
				modifiedSite.codePostale = ti_codePostale.text;
				modifiedSite.ville = ti_ville.text;
				modifiedSite.pays = ti_pays.text;
				modifiedSite.adresseFacturation = (cbx_facturation.selected == true)? 1:0;
				modifiedSite.dateDebut = DateField.stringToDate(df_dateDebut.text, ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_'));
				modifiedSite.dateFin = DateField.stringToDate(df_dateFin.text, ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_'));

				iServiceGestPartenaire.ajouterEditerSite(CvAccessManager.CURRENT_FUNCTION, CodeAction.CREATE_UPD_SITE,idSelectdPartn, modifiedSite);	
				this.close_clickHandler(evt);
			} 
		}
		
		private function validateEnteredSite():Boolean
		{
			var result:Boolean = false;
			var validationResult:Array = Validator.validateAll([sv_site, dv_dateDebut]);
			
			if (validationResult.length == 0)
			{
				result = true
			}
			else
			{
				result = false
			}
			
			return result;
		}
		
		protected function dateField_labelFormat(item:Date):String
		{
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_');
			return dateFormatter.format(item);
			
		}
		
		public function get iServiceGestPartenaire():FctGestionPartenaireService
		{
			return _iServiceGestPartenaire;
		}
		
		public function set iServiceGestPartenaire(value:FctGestionPartenaireService):void
		{
			_iServiceGestPartenaire = value;
		}
		
		public function get idSelectdPartn():int { return _idSelectdPartn; }
		
		public function set idSelectdPartn(value:int):void
		{
			if (_idSelectdPartn == value)
				return;
			_idSelectdPartn = value;
		}
		
		[Bindable]
		public function get selectedSite():SiteVO { return _selectedSite; }
		
		public function set selectedSite(value:SiteVO):void
		{
			if (_selectedSite == value)
				return;
			_selectedSite = value;
		}
	}
}