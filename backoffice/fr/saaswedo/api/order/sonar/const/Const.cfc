component  displayname="Const"
{
	property name="PREFIX" type="sting" default="::str::"
	getter=true setter=false displayname="jprefix" hint="Prefix to fix json issue with numbers";

	property name="SONAR_MODIFY_OPTIONS" type="numeric" default="1"
	getter=true setter=false displayname="ModifyOptions" hint="Sonar CareTransactionPkey Modify Options";

	property name="SONAR_TERMINATE" type="numeric" default="5"
	getter=true setter=false displayname="Terminate" hint="Sonar CareTransactionPkey Terminate";

	property name="SONAR_SUSPEND" type="numeric" default="3"
	getter=true setter=false displayname="Suspend" hint="Sonar CareTransactionPkey Suspend";

	property name="SONAR_REACTIVATE" type="numeric" default="4"
	getter=true setter=false displayname="Reactivate" hint="Sonar CareTransactionPkey Reactivate";

	property name="MYTEM_MODIFY_OPTIONS" type="numeric" default="1282"
	getter=true setter=false displayname="ModifyOptions" hint="Mytem Order Type Modify Options";

	property name="MYTEM_TERMINATE" type="numeric" default="1283"
	getter=true setter=false displayname="Terminate" hint="Mytem Order Type Terminate";

	property name="MYTEM_SUSPEND" type="numeric" default="1383"
	getter=true setter=false displayname="Suspend" hint="Mytem Order Type Suspend";

	property name="MYTEM_REACTIVATE" type="numeric" default="1483"
	getter=true setter=false displayname="Reactivate" hint="Mytem Order Type Reactivate";

	property name="MYTEM_REACTIVATE" type="numeric" default="1382"
	getter=true setter=false displayname="Reactivate" hint="Mytem Order Type Renew";



	public Product function init()
	{
		return this;
	}
}
