package gestionparc.ihm.action.dissocier.actionDissocierEqpFromLigne
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierEqpFromLigneCommand implements ICommand
	{
		private var receiver:ActionDissocierEqpFromLigneReceiver;

		public function ActionDissocierEqpFromLigneCommand(rec:ActionDissocierEqpFromLigneReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
