package univers.facturation.suivi.controles.event
{
	import flash.events.Event;

	public class HistoriqueEvent extends Event
	{
		public static var HISTORIQUE_COMPELTE:String = "HitoriqueComplete"; 
		
		public function HistoriqueEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new HistoriqueEvent(type,bubbles,cancelable);
		}
	}
}