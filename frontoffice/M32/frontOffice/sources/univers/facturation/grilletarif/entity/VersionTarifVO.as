package univers.facturation.grilletarif.entity
{
	public class VersionTarifVO
	{
		private var _idVersionTarif: int;
		private var _libelle :String="";
		private var _prixUnitaire :Number;
		private var _remisePercent:Number;
		private var _prixUnitaireRemise:Number;
		private var _dateDebut : Date;
		private var _dateFin : Date;
		private var _isValide : Boolean;
		private var _idCompte : int;
		private var _idSousCompte : int;
		private var _idGroupeProduit : int;
		private var _cleType : String;
		private var _libelleCompteSousCompte : String;
		
		public function VersionTarifVO()
		{
		}
		public function get cleType():String
		{
			return _cleType;
		}
		
		public function set cleType(value:String):void
		{
			_cleType = value;
		}
		public function get idVersionTarif():int
		{
			return _idVersionTarif;
		}

		public function set idVersionTarif(value:int):void
		{
			_idVersionTarif = value;
		}
		
		public function get isValide():Boolean
		{
			return _isValide;
		}

		public function set isValide(value:Boolean):void
		{
			_isValide = value;
		}
		public function get libelleCompteSousCompte():String
		{
			return _libelleCompteSousCompte;
		}

		public function set libelleCompteSousCompte(value:String):void
		{
			_libelleCompteSousCompte = value;
		}
		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle = value;
		}
		public function get idGroupeProduit():Number
		{
			return _idGroupeProduit;
		}

		public function set idGroupeProduit(value:Number):void
		{
			_idGroupeProduit = value;
		}
		public function get idCompte():Number
		{
			return _idCompte;
		}

		public function set idCompte(value:Number):void
		{
			_idCompte = value;
		}
		public function get idSousCompte():Number
		{
			return _idSousCompte;
		}

		public function set idSousCompte(value:Number):void
		{
			_idSousCompte = value;
		}
		public function get prixUnitaire():Number
		{
			return _prixUnitaire;
		}

		public function set prixUnitaire(value:Number):void
		{
			_prixUnitaire = value;
		
		}

		public function get remisePercent():Number
		{
			return _remisePercent;
		}

		public function set remisePercent(value:Number):void
		{
			_remisePercent = value;
		
		}

		public function get prixUnitaireRemise():Number
		{
			return _prixUnitaireRemise;
			
		}

		public function set prixUnitaireRemise(value:Number):void
		{
			_prixUnitaireRemise = value;
			
		}

		public function get dateDebut():Date
		{
			return _dateDebut;
		}

		public function set dateDebut(value:Date):void
		{
			_dateDebut = value;
		}

		public function get dateFin():Date
		{
			return _dateFin;
		}

		public function set dateFin(value:Date):void
		{
			_dateFin = value;
		}
		public function prixChangeHandler():Number
		{						
			var prixRemise : Number = Number(prixUnitaire) - (Number(prixUnitaire) * Number(remisePercent) / 100);
			prixUnitaireRemise = Number(prixRemise.toFixed(4));
			return prixUnitaireRemise;
			
		}
		public function txtPrixURemiseChangeHandler():void{
			var prixRemiseOld : Number = (prixUnitaire) - (prixUnitaire * remisePercent / 100) // SI le prix remisé à changer
			
			if(prixRemiseOld!=prixUnitaireRemise)
			{
				//prixUnitaire = 0;
				//remisePercent = 0;
			}
			
		}



	}
}