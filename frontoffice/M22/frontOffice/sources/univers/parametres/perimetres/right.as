package univers.parametres.perimetres
{
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	import mx.events.IndexChangedEvent;
	
	public class right extends rightIHM
	{
		private var _mainRef:main;
		
		
		public function right()
		{		
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function setMainRef(ref:main):void
		{
			_mainRef = ref;
		}
		
		protected function initIHM(event:Event):void
		{  
			canvAssignment.setMainRef(_mainRef);
					
			if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE == "GroupeLigne" && _mainRef.getSelectedTypeOrga() != "OPE"){				
				canvAssignment.modeRestreint = true;
			}
			this.viewstack1.addEventListener(IndexChangedEvent.CHANGE, onItemChange);
		}
		
		
		
		
		protected function onItemChange(event:IndexChangedEvent):void
		{	
			refreshContent(event.relatedObject);
		}
		
		private function refreshContent(value:Object):void
		{	
			switch(value.id)
			{
				case "cvAffecter" :
				{
					canvAssignment.setMainRef(_mainRef);
					
					if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE == "GroupeLigne" && _mainRef.getSelectedTypeOrga() != "OPE"){				
						canvAssignment.modeRestreint = true;
					}
					
					break;
				}
				case "cvAffectAuto" :
				{
					dispatchEvent(new Event("viewRAGOIHM",true));
					canvAssignmentAuto.setMainRef(_mainRef);
					break;
				}
				case "cvDestinataireArea":
				{
					canvDestinataire.setMainRef(_mainRef);
					break;
				}
				case "cvBudget":
				{
					canvBudget.setMainRef(_mainRef);
					break;	
				}
				case "cvParameters":
				{
					canvParameters.setMainRef(_mainRef);
					break;	
				}
			}
			
		}
		
		public function refreshMenu():void
		{
			if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE == "GroupeLigne" && _mainRef.getSelectedTypeOrga() != "OPE")
			{	
				viewstack1.removeAllChildren();
				viewstack1.addChild(cvAffecter);
				viewstack1.addChild(cvAffectAuto);
				viewstack1.addChild(cvDestinataireArea);
				viewstack1.addChild(cvParameters);
				
				callLater(selecteFirstMenuItem);
				
				try
				{
					canvAssignment.modeRestreint = true; 
					canvAssignment.setMainRef(_mainRef);
					canvAssignment.fillOrganisation();	
						
				}
				catch(e:Error)
				{
					trace("0");
				}
				
				return;
			}
			else if (_mainRef.getSelectedTypeOrga() == "OPE")
			{
				
				viewstack1.removeAllChildren();
				viewstack1.addChild(cvDestinataireArea)
				viewstack1.addChild(cvBudget);
				viewstack1.addChild(cvParameters);
				
				callLater(selecteFirstMenuItem); 
				
				try
				{	
					callLater(setcanvBudgetsetMainRef);
					canvDestinataire.setMainRef(_mainRef);					
				}
				catch(e:Error)
				{
					trace("1");
				}
				
				return;
			}else{
				
				viewstack1.removeAllChildren();
				viewstack1.addChild(cvAffecter);
				viewstack1.addChild(cvAffectAuto);
				viewstack1.addChild(cvDestinataireArea);
				viewstack1.addChild(cvBudget);
				viewstack1.addChild(cvParameters);
				
			 	
				callLater(selecteFirstMenuItem);
				
				try
				{
					canvAssignment.modeRestreint = false; 
					canvAssignment.setMainRef(_mainRef);
					canvAssignment.fillOrganisation();	
				}
				catch(e:Error)
				{
					trace("2");
				}
			}
			
			
		}
		public function initRefreshMenu():void
		{
			if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE == "GroupeLigne" && _mainRef.getSelectedTypeOrga() != "OPE")
			{	
				viewstack1.removeAllChildren();
				viewstack1.addChild(cvAffecter);
				viewstack1.addChild(cvAffectAuto);
				viewstack1.addChild(cvDestinataireArea);
				viewstack1.addChild(cvParameters);
				
				callLater(selecteFirstMenuItem);
				
				try
				{
					canvAssignment.modeRestreint = true; 
					canvAssignment.setMainRef(_mainRef);
					canvAssignment.fillOrganisation();	
						
				}
				catch(e:Error)
				{
					trace("0");
				}
				
				return;
			}
			else if (_mainRef.getSelectedTypeOrga() == "OPE")
			{
				
				viewstack1.removeAllChildren();
				viewstack1.addChild(cvDestinataireArea);
				viewstack1.addChild(cvBudget);
				viewstack1.addChild(cvParameters);
				
				callLater(selecteFirstMenuItem); 
				
				try
				{	
					callLater(setcanvBudgetsetMainRef);
					canvDestinataire.setMainRef(_mainRef);	
				}
				catch(e:Error)
				{
					trace("1");
				}
				
				return;
			}else{
				
				viewstack1.removeAllChildren();
				viewstack1.addChild(cvAffecter);
				viewstack1.addChild(cvAffectAuto);
				viewstack1.addChild(cvDestinataireArea);
				viewstack1.addChild(cvBudget);
				viewstack1.addChild(cvParameters);
				
			 	
				callLater(selecteFirstMenuItem);
				
				try
				{
					canvAssignment.modeRestreint = false; 
					canvAssignment.setMainRef(_mainRef);
					canvAssignment.fillOrganisation();	
				}
				catch(e:Error)
				{
					trace("2");
				}
			}
			
			
		}
		private function selecteFirstMenuItem():void
		{
			lbView.selectedIndex = 0;
		}
		
		private function setcanvBudgetsetMainRef():void
		{
			canvBudget.setMainRef(_mainRef);
		}

		public function newEvent(eventName:String):void
		{
			/* var e:Event = new Event(eventName);
			dispatchEvent(e); */
			try
			{
				switch (eventName)
				{
					case "organisationChanged":{
						getSelectedRef().mainOrganisationChanged();
						refreshMenu();
						break;
					}	
					case "treeSelectionChanged":
						getSelectedRef().mainTreeSelectionChanged();
						break;
				}
			}
			catch (e:Error) 
			{
				trace(e.getStackTrace());
			}
		}
		
		public function getSelectedRef():Object
		{
			return viewstack1.selectedChild.getChildAt(0);
			
			
		}
		
		public function onPerimetreChange():void {
			refreshContent(viewstack1.selectedChild);
		}
	
	}
	
}