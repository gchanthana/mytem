package entity
{
	[Bindable]
	public class ClientPartenaireVO
	{
		private var _CLIENT:ClientVO;
		private var _PARTENAIRE:PartenaireVO;
		private var _OFFRE:OffreVO;
		private var _NIVEAU_FACTURATION:int;
		private var _DATE_DESACTIVATION:Date;
		private var _IS_ACTIF:Boolean;
		
		public function ClientPartenaireVO()
		{
		}
		
		public function fill(obj:Object,fillClient:Boolean=true):Boolean
		{
			try
			{
				if(obj.hasOwnProperty("IS_ACTIF"))
					this._IS_ACTIF = obj.IS_ACTIF;
				if(obj.hasOwnProperty("DATE_DESACTIVATION"))
					this._DATE_DESACTIVATION = obj.DATE_DESACTIVATION;
				if(obj.hasOwnProperty("NIVEAU_FACTURATION"))
					this._NIVEAU_FACTURATION = obj.NIVEAU_FACTURATION;
				
				this._CLIENT= new ClientVO()
				if(fillClient)
					this._CLIENT.fill(obj);
				
				this._PARTENAIRE = new PartenaireVO()
				this._PARTENAIRE.fill(obj);
				this._OFFRE = new OffreVO()
				this._OFFRE.fill(obj);
				return true;
			}
			catch(e:Error)
			{
				return false
			}
			return false
		}
		
		public function get IS_ACTIF():Boolean { return _IS_ACTIF; }
		public function set IS_ACTIF(value:Boolean):void
		{
			if (_IS_ACTIF == value)
				return;
			_IS_ACTIF = value;
		}
		
		public function get DATE_DESACTIVATION():Date { return _DATE_DESACTIVATION; }
		public function set DATE_DESACTIVATION(value:Date):void
		{
			if (_DATE_DESACTIVATION == value)
				return;
			_DATE_DESACTIVATION = value;
		}
		
		public function get NIVEAU_FACTURATION():int { return _NIVEAU_FACTURATION; }
		public function set NIVEAU_FACTURATION(value:int):void
		{
			if (_NIVEAU_FACTURATION == value)
				return;
			_NIVEAU_FACTURATION = value;
		}
		
		public function get OFFRE():OffreVO { return _OFFRE; }
		public function set OFFRE(value:OffreVO):void
		{
			if (_OFFRE == value)
				return;
			_OFFRE = value;
		}
		
		public function get PARTENAIRE():PartenaireVO { return _PARTENAIRE; }
		public function set PARTENAIRE(value:PartenaireVO):void
		{
			if (_PARTENAIRE == value)
				return;
			_PARTENAIRE = value;
		}
		
		public function get CLIENT():ClientVO { return _CLIENT; }
		public function set CLIENT(value:ClientVO):void
		{
			if (_CLIENT == value)
				return;
			_CLIENT = value;
		}
	}
}