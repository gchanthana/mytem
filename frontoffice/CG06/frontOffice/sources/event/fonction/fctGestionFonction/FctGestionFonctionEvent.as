package event.fonction.fctGestionFonction
{
	import flash.events.Event;
	
	public class FctGestionFonctionEvent extends Event
	{
		// Event action GET
		public static const LISTE_NIVEAUX 		:String = "LISTE_NIVEAUX";
		public static const LISTE_FONCTIONS 	:String = "LISTE_FONCTIONS";
		public static const LISTE_DROITS_SPEC 	:String = "LISTE_DROITS_SPEC";
		public static const LISTE_UNIVERS		:String = "LISTE_UNIVERS";
		
		//Event action Create, update and delete.
		public static const FONCTION_SAVED			:String = "FONCTION_SAVED";
		public static const FONCTION_DELETED		:String = "FONCTION_DELETED";;
		public static const FONCTION_UPDATED		:String = "FONCTION_UPDATED";
		
		public static const DROIT_FCT_CREATED		:String = "DROIT_FCT_CREATED";
		public static const DROIT_FCT_UPDATED		:String = "DROIT_FCT_UPDATED";
		public static const DROIT_FCT_DELETED		:String = "DROIT_FCT_DELETED";
		
		//others
		public static const SELECT_FONCTION			:String = "SELECT_FONCTION";
		public static const CLICK_DETAIL_FCT		:String = "CLICK_DETAIL_FCT";
		public static const CLICK_SUPPRESSION_FCT	:String = "CLICK_SUPPRESSION_FCT";
		public static const CLICK_SUPP_DROIT_FCT	:String = "CLICK_SUPP_DROIT_FCT";
		public static const CLICK_DETAIL_DROIT_FCT	:String = "CLICK_DETAIL_DROIT_FCT";
		
		public function FctGestionFonctionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}