package gestionparc.ihm.action.dissocier.actionDissocierLigneExtFromExt
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierLigneExtFromExtCommand implements ICommand
	{
		private var receiver:ActionDissocierLigneExtFromExtReceiver=null;

		public function ActionDissocierLigneExtFromExtCommand(rec:ActionDissocierLigneExtFromExtReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
