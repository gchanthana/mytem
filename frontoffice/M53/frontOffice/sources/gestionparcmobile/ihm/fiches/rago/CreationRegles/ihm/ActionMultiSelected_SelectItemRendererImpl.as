package univers.parametres.rago.CreationRegles.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.controls.CheckBox;
	
	[Bindable]
	public class ActionMultiSelected_SelectItemRendererImpl extends HBox
	{
		public var ckbSelected:CheckBox;
		
		
		public function ActionMultiSelected_SelectItemRendererImpl()
		{
			super();
			addEventListener(MouseEvent.CLICK, _localeClickHandler);
		}
		
		protected function _localeClickHandler(event:MouseEvent):void
		{
			if(event == null) return;			
		
			switch(ckbSelected.selected)
			{
				case true: 	{
								dispatchEvent(new Event("thisNodeIsAnExceptionSelected",true)); break;
							}
				case false: {
								dispatchEvent(new Event("thisNodeIsNotAnExceptionSelected",true)); break;
							}
				default:
				{  }
			}
		}

	}
}