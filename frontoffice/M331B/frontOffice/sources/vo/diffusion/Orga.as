package vo.diffusion
{

	public class Orga
	{
		private var _idOrga:int;
		private var _nomOrga:String;
		private var _typeOrga: String;

		public function Orga()
		{
		}

		public function fill(uneOrga:Object):void
		{
			this._idOrga=uneOrga.IDORGA;
			this._nomOrga=uneOrga.NOMORGA;
			this._typeOrga=uneOrga.TYPE_ORGA;
		}

		public function get idOrga():int
		{
			return _idOrga;
		}

		public function set idOrga(value:int):void
		{
			_idOrga=value;
		}

		public function get nomOrga():String
		{
			return _nomOrga;
		}

		public function set nomOrga(value:String):void
		{
			_nomOrga=value;
		}
		
		public function get typeOrga():String
		{
			return _typeOrga;
		}
		
		public function set typeOrga(value:String):void
		{
			_typeOrga = value;
		}

	}
}
