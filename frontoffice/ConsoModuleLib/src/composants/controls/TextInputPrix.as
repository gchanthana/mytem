package composants.controls
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	
	import mx.controls.TextInput;
	import mx.validators.NumberValidator;
	import mx.resources.ResourceManager;
	import mx.resources.ResourceBundle;
		
	[ResourceBundle("ConsoModuleLib")]
	
	public class TextInputPrix extends TextInput
	{
		private var _validatorPrice : NumberValidator ;
		private var _prix : Number = 0;
		
		public function TextInputPrix()
		{
			super();
			
			initValidator();
			
			this.addEventListener(FocusEvent.FOCUS_IN , majPrixEventFocusIn);
			this.addEventListener(FocusEvent.FOCUS_OUT , majPrixEventFocusOut);
		}
		
		// ------ getter and setter
		
		public function get validatorPrice():NumberValidator
		{
			return _validatorPrice;
		}
		
		public function set validatorPrice(value:NumberValidator):void
		{
			if(value!=null)
			{
				_validatorPrice = value;
			}
		}
		
		public function get prix():Number
		{
			return makeNumber(this.text);
		}
		
		// -----------
		
		private function initValidator():void
		{
			validatorPrice = new NumberValidator();
			
			validatorPrice.source = this;
			validatorPrice.property = "text";
			validatorPrice.required = false;
			validatorPrice.decimalPointCountError = ResourceManager.getInstance().getString('ConsoModuleLib','Le_format_est_invalide__');
			validatorPrice.decimalSeparator = 		ResourceManager.getInstance().getString('ConsoModuleLib','DECIMAL_SEPARATOR');
			validatorPrice.thousandsSeparator = 	ResourceManager.getInstance().getString('ConsoModuleLib','THOUSANDS_SEPARATO');
			validatorPrice.invalidCharError = 		ResourceManager.getInstance().getString('ConsoModuleLib','Le_format_est_invalide__');
		}

		private function majPrixEventFocusOut(evt:Event):void
		{
			validatorPrice.enabled=true; // on reactive le validateur
			
			if(evt.currentTarget.text!="")
			{
				var myTarifTmp : Number = new Number(evt.currentTarget.text);
				if(!isNaN(myTarifTmp))
					if(myTarifTmp > -1)
					{
						evt.currentTarget.text=ConsoviewFormatter.formatNumber(Number(myTarifTmp),2);
						if(evt.currentTarget.text.charAt(evt.currentTarget.text.length-3)!=",") // ajout des decimales si elle n'y sont pas
						{
							evt.currentTarget.text+=",00";
						} 
					}
			}
		}
		
		private function majPrixEventFocusIn(evt:Event):void
		{
			if(evt.currentTarget.errorString == "") // si le validateur n'est pas en train d'annoncer une erreur
			{
				validatorPrice.enabled=false; // on deseactive le validateur
					
				if(evt.currentTarget.text!="")
				{
					var retoutdeMakeNumber:Number = makeNumber(evt.currentTarget.text);
					if(!isNaN(retoutdeMakeNumber))
						evt.currentTarget.text=retoutdeMakeNumber.toString();
				}
				else
					evt.currentTarget.text="";
			}
			else
				evt.currentTarget.text="";
		}
		
		// enleve les espaces et remplace la virgule par un point (exple 25 356,99 -->> 25356.99)
		private function makeNumber(textNumber : String):Number
		{
			var result : String;
			var paternEspace : RegExp = /([ ])/g;
			var paternVirgule : RegExp = /([,])/;
			
			result = textNumber.replace(paternEspace, "");
			result = result.replace(paternVirgule, ".");
			
			var resultatFinal : Number = new Number(result);
			
			return resultatFinal;
		}
		
	}
}