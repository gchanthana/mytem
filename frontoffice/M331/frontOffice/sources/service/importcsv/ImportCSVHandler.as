package service.importcsv
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ImportCSVHandler
	{
		private var _model:ImportCSVModel;
		
		public function ImportCSVHandler(_model:ImportCSVModel):void
		{
			this._model = _model;
		}
		
		internal function getInfosCVSResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.getInfosCSVResult(evt.result as ArrayCollection);
			}
		}
		internal function getUUIDResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.getUUIDResult(evt);
			}
		}	
		
		
		
	}
}