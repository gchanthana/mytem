package univers.inventaire.equipements.gestionflottemobile.paneldesaffectationequipofcollab
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.IComportementDesaffectation;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.IListeDispo;
	
	public class PanelDesaffectationOfCollab extends PanelDesaffectationOfCollabIHM
	{
		private var obj_cellule:Cellule;
		private var obj_comportement : IComportementDesaffectation;
		private var obj_listeDispo : IListeDispo;
		public function PanelDesaffectationOfCollab(obj_cellule:Cellule, obj_comportement : IComportementDesaffectation,obj_listeDispo : IListeDispo)
		{
			this.obj_cellule = obj_cellule;//La cellule séléctionnée dans l'écran principal
			this.obj_comportement = obj_comportement;//Les précédure à appeler
			this.obj_listeDispo = obj_listeDispo;//La liste à afficher (Ou sim, ou collab, ou terminal)
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM)
		}
		private function initIHM(evt : FlexEvent):void
		{
			img_cellule.source = obj_cellule.image_cellule; //Afficher l'icone de la cellule séléctionné
			img_liste.source=obj_listeDispo.getSourceIcone();//Afficher l'icone de la liste
			img_liste_big.source=obj_listeDispo.getSourceIcone();//Afficher l'icone de la liste
			lab_cellule_name.text = obj_cellule.libelle_cellule;//Affichage du nom de la cellule séléctionné
			lab_titre.text=""+obj_listeDispo.getLibelleType()+"de "+obj_cellule.libelle_cellule;//Affichage du type d'objet de la liste
			box_listeDispo.addChild(obj_listeDispo as VBox);//Ajout du composant contenant la dg + le filtre
					
			//dgListeDispo.addEventListener(ListEvent.CHANGE,dgListeDispo_handler);//On ajoute un écouteur d'évènement au dg
			calendar.selectedDate = new Date(); // Selection de la date actuel dans la date du jour
			
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
			addEventListener(CloseEvent.CLOSE,fermer);
			addEventListener(MouseEvent.CLICK,dgListeDispo_handler);
		}
		private function dgListeDispo_handler(evt : Event):void
		{
			labDgSelection.text = obj_listeDispo.getLibelleSelection();
		}
		private function valider(evt : MouseEvent):void
		{
			if(obj_listeDispo.getIDSelection()!=-1)
			{
				obj_comportement.desaffecter(obj_cellule.id_cellule,obj_listeDispo.getIDSelection(),obj_cellule.libelle_cellule,obj_listeDispo.getLibelleSelection(),DateFunction.formatDateAsInverseString(calendar.selectedDate));
				dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));
			}
			else
			{
				Alert.show("Sélectionnez un élément dans la liste");
			}
		}
		private function fermer(evt : Event):void 
		{
			dispatchEvent(new Event(PanelGestionFoltteMobile.ANNULER_EVENT));
		}
		
		
		

	}
}