<!---
<cfset applicationstop()>
--->
<cfif isDefined("APPLICATION")>
	<cfdump var="#APPLICATION#" label="Application (fr.consotel.consoview)" expand="false"><br>
	<cfif isDefined("SESSION")>
		<cfif structKeyExists(SESSION,"PERIMETRE")>
			<cfdump var="#SESSION.PERIMETRE#" label="Session PERIMETRE (fr.consotel.consoview)"><br>
		</cfif>
		<cfif structKeyExists(SESSION,"USER")>
			<cfdump var="#SESSION.USER#" label="Session USER (fr.consotel.consoview)"><br>
		</cfif>
		<cfdump var="#SESSION#" label="Session (fr.consotel.consoview)" expand="false"><br>
	</cfif>
</cfif>