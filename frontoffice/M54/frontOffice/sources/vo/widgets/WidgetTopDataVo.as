package vo.widgets
{
	import mx.utils.StringUtil;

	public dynamic class WidgetTopDataVo
	{
		public function WidgetTopDataVo()
		{
		}
		
		/**
		 * permet de creer les propriétés dynamiquement 
		 * @param obj
		 */		
		public function fill(obj:Object):void
		{
			for (var elem:String in obj)
			{ 
				this[elem.toLowerCase()]=StringUtil.trim(obj[elem]);
			}
		}
	}
}