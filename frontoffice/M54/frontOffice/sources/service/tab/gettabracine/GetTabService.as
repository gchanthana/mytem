package service.tab.gettabracine
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class GetTabService 
	{

		private var _model:GetTabModel;
		public var handler:GetTabHandler;
		private var remote:RemoteObject;
		
		public function GetTabService()
		{
			this._model=new GetTabModel();
			this.handler=new GetTabHandler(_model);
		}

		public function get model():GetTabModel
		{
			return this._model;
		}

		public function getListTabByRacine():void
		{
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.tab.ListeTabService";
			remote.addEventListener(ResultEvent.RESULT,handler.getListKpiResultHandler,false,0,true);
			remote.getListeTabByUserByRacine();
		}
	}
}
