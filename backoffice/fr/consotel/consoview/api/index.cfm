<cflog type="information" text="*** START - TEST API REPORTING ***">
<cfif structKeyExists(URL,"ACTION") AND (UCASE(URL["ACTION"]) EQ "RUN")>
	<!--- SIMULATES USER LOGON : CLEAR SESSION THUS RUNTIME DATA --->
	<cfif structKeyExists(SESSION,"CV")>
		<cfset structDelete(SESSION,"CV")>
	</cfif>
	
	<cfset report1="/CONSOTEL/TB-REPORT/TB-REPORT.xdo">
	<cfset report2="/consoview/M311/TB-REPORT/TB-REPORT.xdo">
	
	<!--- DEFINE PROPERTIES AND PARAMETERS VALUES --->
	Service Invocation Test :<br />
	<cfset SESSION["USER"]=structNew()>
	<cfset SESSION["USER"]["GLOBALIZATION"]="fr_FR">
	<cfset idRacine=477>
	<cfset optionnel="Non Renseigné">
	<cfset parameters = {
		"idRacine"=idRacine,
		"userIdCv"=88096,
		"idGestionnaire"=0,
		"xdoAbsolutePath"=report2,
		"xdoTemplateId"="COMPLET",
		"outputFormat"="html",
		"outputExtension"="html",
		"outputName"=optionnel,
		"jobName"="Test API Rapports [#idRacine#]: #NOW()#",
		"appName"=optionnel,
		"moduleName"=optionnel,
		"codeRapport"=optionnel,
		"localization"=SESSION["USER"]["GLOBALIZATION"],
		"enableBurst"=FALSE,
		"enableInternalNotifications"=TRUE,
		"internalNotificationsTo"="cedric.rapiera@saaswedo.com",
		"deliveryMode"="EMAIL",
		"emailFrom"="api@consotel.fr",
		"emailTo"="monitoring@saaswedo.com",
		"emailSubject"="Test API Rapports [#idRacine#]: #NOW()#",
		"emailBody"=" ",
		"reportParameters" = {
			"G_IDMASTER" = {"parameterValues"=[idRacine], "parameterType"="CV", "parameterLabel"="Racine Master"},
			"G_IDRACINE" = {"parameterValues"=[idRacine], "parameterType"="CV", "parameterLabel"="Groupe"},
			"G_LANGUE_PAYS" = {"parameterValues"=[REPLACE(SESSION["USER"]["GLOBALIZATION"],"-","_")], "parameterType"="CV", "parameterLabel"="Langue"},
			"G_IDPERIMETRE" = {"parameterValues"=[idRacine],"parameterType"="CV", "parameterLabel"="Périmètre"},
			"G_IDORGA" = {"parameterValues"=[idRacine],"parameterType"="CV", "parameterLabel"="Organisation"},
			"G_IDCLICHE" = {"parameterValues"=[0],"parameterType"="CV", "parameterLabel"="Cliché"},
			"U_IDPERIOD_DEB" = {"parameterValues"=[125],"parameterType"="CV", "parameterLabel"="Début"},
			"U_IDPERIOD_FIN" = {"parameterValues"=[126],"parameterType"="CV", "parameterLabel"="Fin"},
			"G_SEGMENT" = {"parameterValues"=["Data"],"parameterType"="CV", "parameterLabel"="Segment"},
			"G_NIVEAU" = {"parameterValues"=["A"],"parameterType"="CV", "parameterLabel"="Niveau"},
			"X_INCLUDE_CSV_HEADER" = {"parameterValues"=[0],"parameterType"="CV", "parameterLabel"="Niveau"},
			"ParametreInexistant" = {"parameterValues"=["Parametre inexistant - Erreur ou Pas ? NON"], "parameterType"="CV", "parameterLabel"="Inconnu"}
		}
	}>
	<!--- INSTANCIATE API --->
	<cfset api=createObject("component","fr.consotel.consoview.api.CV")>
	<cfset result=api.invokeService("IBIS","scheduleReport",parameters)>
	<cfif isValid("numeric",result)>
		<cfoutput>
			JOBID URL : <a href="/fr/consotel/api/auth.cfm?JOBID=#result#"><strong>#result#</strong></a><br />
		</cfoutput>
	<cfelse>
		RESULT IS NOT A NUMERIC :<br />
		<cfdump var="#result#" label="Result">
	</cfif>
<cfelse>
	<cflog type="information" text="NO TEST TO RUN : DUMPING [URL] SCOPE">
	<cfdump var="#URL#" label="URL">
</cfif>
<cflog type="information" text="*** END - TEST API REPORTING ***">