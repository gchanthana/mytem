<cfcomponent author="Cedric" extends="fr.saaswedo.api.mdm.impl.airwatch.MdmService" implements="fr.saaswedo.api.mdm.core.services.IDeviceInfo">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.airwatch.DeviceInfo">
		<cfreturn "fr.saaswedo.api.mdm.impl.airwatch.DeviceInfo">
	</cffunction>
	
	<!--- TODO: A implémenter pour Datalert --->
	<cffunction access="public" name="getAllDevices" returntype="Array"
	hint="Ramène la liste des devices présents sur le MDM. Certains devices ne sont pas forcément présents dans le parc MDM local">
		<cfset var deviceList=[]>
		<cftry>
			<cfset var httpResponse={}>
			<cfset var jsonDeviceList=[]>
			<!--- Info API MDM --->
			<cfset var params=[]>
			<cfset var result={}>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var service=getDeviceService()>
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
			<cfset service.endpoint=service.endpoint & "/search">
			<!--- MAJ du endpoint: Recherche de devices (Par défaut dans Airwatch: 500 devices max par page) --->
			<cfset var pageNum=0>
			<cfset var devicesPerPage=500>
			<cfset var devicesTotal=0>
			<!--- Nombre de device de la liste qui est retournée par cette fonction --->
			<cfset var deviceProcessedCount=0>
			<!--- TRUE s'il y a plus d'une page de devices (500 max par page) --->
			<cfset var hasModeDevices=TRUE>
			<cfloop condition="hasModeDevices EQ TRUE">
				<cfset params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()},
					{type="url",name="pagesize",value=devicesPerPage},
					{type="url",name="page",value=pageNum}
				]>
				<cfset result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset var exception=structKeyExists(result,remoting.RESULT()) ? result[remoting.RESULT()]:{}>
					<cfset var errorMsg=structKeyExists(exception,"message") ? exception.message:"Error occured during remoting (HTTP)">
					<cfset var errorDetail=structKeyExists(exception,"detail") ? exception.detail:"Remoting status is #result[remoting.STATUS()]#">
					<cfset hasModeDevices=FALSE>
					<cfthrow type="Custom" message="#errorMsg#" detail="#errorDetail#">
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset hasModeDevices=FALSE>
						<cfthrow type="Custom" message="HTTP response code: #httpCode#">
					<!--- Traitement de la liste de device de la page de résultat (Search) --->
					<cfelse>
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<!--- Type du contenu pour une réponse sans erreur (HTTP 200) --->
							<cfif isInstanceOf(result.result.fileContent,"java.io.ByteArrayOutputStream")>
								<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
								<cfset var jsonResponse=dataUtils.byteArrayToString(responseContent)>
								<!--- Désérialisation JSON --->
								<cfif isJSON(jsonResponse)>
									<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
									<cfset jsonDeviceList=jsonResponse.devices>
									<cfset devicesTotal=structKeyExists(jsonResponse,"total") ? VAL(jsonResponse.total):0>
									<cfif isArray(jsonDeviceList)>
										<cfset var currentPageSize=arrayLen(jsonDeviceList)>
										<!--- Ajout des devices dans le résultat retourné --->
										<cfloop index="i" from="1" to="#currentPageSize#">
											<!--- On ne prend que les devices enrollés --->
											<cfset var device=jsonDeviceList[i]>
											<cfset var mdmDevice={
												Serial_Number=structKeyExists(device,"serialNumber") ? device.serialNumber:"",
												IMEI=structKeyExists(device,"IMEI") ? device.IMEI:"",
												UUID=structKeyExists(device,"UUID") ? device.UUID:"",
												STATUS=structKeyExists(device,"enrollmentStatus") ? getStatusValue(device.enrollmentStatus):UNMANAGED()
											}>
											<cfset arrayAppend(deviceList,mdmDevice)>
											<cfset deviceProcessedCount=deviceProcessedCount + 1>
										</cfloop>
										<!--- Condition d'arret: Indique s'il reste encore des pages de devices non traitées --->
										<cfset hasModeDevices=(devicesTotal GT deviceProcessedCount)>
										<cfif hasModeDevices EQ TRUE>
											<!--- Incrémentation du numéro de page --->
											<cfset pageNum=pageNum + 1>
										</cfif>
									<cfelseif structKeyExists(jsonResponse.devices.device,"uuid")>
										<cfset hasModeDevices=FALSE>
										<cfthrow type="Custom" message="Invalid devices list type from HTTP reponse: Should be an Array">
									</cfif>
								<cfelse>
									<cfset hasModeDevices=FALSE>
									<cfthrow type="Custom" message="Invalid HTTP reponse format: Should be java.io.ByteArrayOutputStream">
								</cfif>
							<cfelse>
								<cfset hasModeDevices=FALSE>
								<cfthrow type="Custom" message="Invalid HTTP reponse type: Should be JSON">
							</cfif>
						</cfif>
					</cfif>
				<cfelse>
					<cfset hasModeDevices=FALSE>
					<cfthrow type="Custom" message="Unknwon remoting status value: #result[remoting.STATUS()]#">
				</cfif>
			</cfloop>
			<cfreturn deviceList>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn deviceList>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="deviceExists" returntype="Boolean" hint="Retourne TRUE le device existe et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<!--- C'est cette fonction qui est utilisée car elle ne lève pas d'exception si le device n'existe pas dans le MDM --->
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfreturn NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- Erreur non critique --->
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getManagedStatus" returntype="String" hint="Retourne un des status : MANAGED(),UNMANAGED()">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<!--- Le device est présent dans le parc MDM local et son statut a été vérifié sur le MDM  --->
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<!--- La valeur du statut provenant de findDeviceFromCache() est déjà formaté --->
				<cfreturn structKeyExists(deviceFromCache,"status") ? deviceFromCache.status:UNMANAGED()>
			<cfelse>
				<cfreturn UNMANAGED()>
			</cfif>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- Erreur non critique --->
				<cfreturn UNMANAGED()>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getLastUser" returntype="String" hint="Retourne le login d'enrollment ou une chaine vide s'il n'est pas enrollé">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var lastUser="">
		<cftry>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfset lastUser=device.username>
			</cfif>
			<cfreturn lastUser>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- Erreur non critique --->
				<cfreturn lastUser>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getDeviceInfo" returntype="Struct"
	hint="La structure retournée contient les clés supplémentaires suivantes :<br>- GENERAL (Struct): UUID (UUID MobileIron)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var mdmDevice={}>
		<cftry>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<!--- Utilisé pour parser les dates au format ISO8601 provenant de MobileIron --->
				<cfset var javaDateParser=createObject("java","javax.xml.bind.DatatypeConverter")>
				<!--- Extraction des propriétés : GENERAL, PROPERTIES --->
				<cfset mdmDevice=extractMdmDevice(device)>
				<!--- Autres propriété de la clé GENERAL : firstConnectionDate,lastAuthDate --->
				<cfset structAppend(mdmDevice.GENERAL,{firstConnectionDate=0,lastAuthDate=0},TRUE)>
				<cfif structKeyExists(device,"lastEnrolledOn")>
					<cfset mdmDevice.GENERAL.firstConnectionDate=device.lastEnrolledOn>
				</cfif>
				<cfif structKeyExists(device,"lastSeen")>
					<cfset mdmDevice.GENERAL.lastAuthDate=device.lastSeen>
				</cfif>
				<!--- Inventaire logiciel: L'UUID est récupéré depuis le cache des devices --->
				<cfset mdmDevice.SOFTWARE=getDeviceSoftwares(deviceFromCache.id_mdm)>
				<!--- Propriétés du device provenant de extractMdmDevice() s'ils sont définis --->
				<cfset var mdmDeviceProps=structKeyExists(mdmDevice,"PROPERTIES") ? mdmDevice.PROPERTIES:[]>
				<!--- Propriétés réseaux (e.g Roaming) - IPAddress --->
				<cfset var networkInfo=getNetworkInfo(deviceFromCache.id_mdm)>
				<cfloop item="infoKey" collection="#networkInfo#">
					<cfset var infoValue=networkInfo[infoKey]>
					<!--- Les adresses IP sont regroupées dans une structure --->
					<cfif compareNoCase(infoKey,"IPAddress") EQ 0>
						<cfset var ipAddress=extractMdmProperty(CELLULAR_IP_ADDRESS(),infoValue)>
						<cfif NOT structIsEmpty(ipAddress)>
							<cfset arrayAppend(mdmDeviceProps,ipAddress)>
						</cfif>
						<cfset ipAddress=extractMdmProperty(ETHERNET_IP_ADDRESS(),infoValue)>
						<cfif NOT structIsEmpty(ipAddress)>
							<cfset arrayAppend(mdmDeviceProps,ipAddress)>
						</cfif>
						<cfset ipAddress=extractMdmProperty(WIFI_IP_ADDRESS(),infoValue)>
						<cfif NOT structIsEmpty(ipAddress)>
							<cfset arrayAppend(mdmDeviceProps,ipAddress)>
						</cfif>
					<!--- Autres propriétés réseaux --->
					<cfelse>
						<cfset var property=extractMdmProperty(infoKey,infoValue)>
						<cfif NOT structIsEmpty(property)>
							<cfset arrayAppend(mdmDeviceProps,property)>
						</cfif>
					</cfif>
				</cfloop>
				<cfset mdmDevice.PROPERTIES=mdmDeviceProps>
				<!--- Propriétés du device provenant de extractMdmDevice() s'ils sont définis --->
				<cfset mdmDeviceProps=structKeyExists(mdmDevice,"PROPERTIES") ? mdmDevice.PROPERTIES:[]>
				<!--- Infos de localisation du device --->
				<cfset var gpsLocation=getLocation(deviceFromCache.id_mdm)>
				<cfloop item="locationKey" collection="#gpsLocation#">
					<cfset var locationValue=gpsLocation[locationKey]>
					<cfset var property=extractMdmProperty(locationKey,locationValue)>
					<cfif NOT structIsEmpty(property)>
						<cfset arrayAppend(mdmDeviceProps,property)>
					</cfif>
				</cfloop>
				<cfset mdmDevice.PROPERTIES=mdmDeviceProps>
			</cfif>
			<cfreturn mdmDevice>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei})>
				<!--- Erreur non critique --->
				<cfreturn mdmDevice>
			</cfcatch>
		</cftry>
		<cfreturn {}>
	</cffunction>
	
	<!--- =========== Fonctions d'extraction des infos des devices =========== --->
	
	<cffunction access="private" name="extractMdmDevice" returntype="Struct" hint="Retourne une structure contenant :
	<br>- GENERAL (Struct): OS,UUID<br>- PROPERTIES (Array): Chaque élément est une structure contenant les clés NAME,VALUE.
	Celle implémentation est une redéfinition de la fonction parent et est utilisée par la fonction getDeviceInfo() de ce composant">
		<cfargument name="jsonDevice" type="Struct" required="true" hint="Device de l'API MDM">
		<!--- TODO: Peut etre utiliser l'adresse MAC Wifi comme valeur du SERIAL et IMEI quand ces derniers ne sont pas renseignés --->
		<cfset var jsonStruct=ARGUMENTS.jsonDevice>
		<cfset var properties=[]>
		<cfset var deviceStatus=structKeyExists(jsonStruct,"enrollmentStatus") ? getStatusValue(jsonStruct.enrollmentStatus):UNMANAGED()>
		<!--- Structure du device qui est retourné --->
		<cfset var mdmDevice={}>
		<cfset mdmDevice.GENERAL={
			Serial_Number=structKeyExists(jsonStruct,"serialNumber") ? jsonStruct.serialNumber:"",
			IMEI=structKeyExists(jsonStruct,"IMEI") ? jsonStruct.IMEI:"",
			OS=getPlatformValue(structKeyExists(jsonStruct,"platform") ? jsonStruct.platform:""),
			WIFI_MAC=structKeyExists(jsonStruct,"macAddress") ? jsonStruct.macAddress:"",
			STATUS=deviceStatus,id_mdm=""
		}>
		<!--- Identifiant MDM provenant du Parc local --->
		<cfif structKeyExists(jsonStruct,"id") AND structKeyExists(jsonStruct.id,"value")>
			<cfset mdmDevice.GENERAL.id_mdm=jsonStruct.id.value>
		</cfif>
		<!--- Parcours des propriétés du device --->
		<cfloop item="key" collection="#jsonStruct#">
			<!--- Extraction spécifique aux autres propriétés --->
			<cfset var mdmProperty=extractMdmProperty(key,jsonStruct[key])>
			<cfif NOT structIsEmpty(mdmProperty)>
				<!--- Ajout de la propriété dans le device retourné --->
				<cfset arrayAppend(properties,mdmProperty)>
			</cfif>
		</cfloop>
		<!--- Ajout du provider MDM dans les propriétés du device --->
		<cfset arrayAppend(properties,{NAME="MDM_PROVIDER",VALUE=getMdmServer().getMdmServerInfo().getProvider()})>
		<cfset mdmDevice.PROPERTIES=properties>
		<cfreturn mdmDevice>
	</cffunction>
	
	<cffunction access="private" name="getNetworkInfo" returntype="Struct" hint="Retourne la structure des propriétés Network du device (e.g Roaming)">
		<cfargument name="id_mdm" type="String" required="true" hint="Identifiant MDM du device dans le parc MDM local">
		<cfset var deviceUUID=ARGUMENTS.id_mdm>
		<cfset var networkInfo={}>
		<cfset var httpResponse={}>
		<cftry>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset var service=getDeviceService()>
			<cfset var params=[
				{type="header",name="accept",value="application/json"},
				{type="header",name="User-Agent",value="SaaSwedo"},
				{type="header",name="aw-tenant-code",value=infos.getToken()}
			]>
			<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/network">
			<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
			<cfset var result=remoting.invoke(service,"",params)>
			<!--- Erreur durant le reporting --->
			<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
				<cfset storeError({
					UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],CLIENT_REQUEST={service=service,params=isDefined("params") ? params:[]}
				})>
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset var httpCode=remoting.getResponseHttpCode(result)>
				<cfset httpResponse=result[remoting.RESULT()]>
				<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
				<cfset networkInfo.RESULT=result[remoting.RESULT()]>
				<!--- Code HTTP d'erreur --->
				<cfif httpCode NEQ 200>
					<cfset storeError({
						UUID=createUUID(),SERVER_RESPONSE=httpResponse,CLIENT_REQUEST={service=service,params=isDefined("params") ? params:[]}
					})>
				<cfelse>
					<!--- Désérialisation de la réponse HTTP --->
					<cfif structKeyExists(httpResponse,"FileContent")>
						<cfset var responseContent=httpResponse.FileContent>
						<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
						<cfset networkInfo=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
					</cfif>
				</cfif>
			</cfif>
			<!--- Supression de la clé RESULT du résultat retourné --->
			<cfset structDelete(networkInfo,"RESULT",FALSE)>
			<cfreturn networkInfo>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn networkInfo>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getDeviceSoftwares" returntype="Array"
	hint="Retourne l'inventaire logiciel du device. Chaque logiciel est une structure contenant :<br>NAME,AUTHOR,VERSION,SIZE (0),InstallTimeStamp (0)">
		<cfargument name="id_mdm" type="String" required="true" hint="Identifiant MDM du device dans le parc MDM local">
		<cfset var deviceUUID=ARGUMENTS.id_mdm>
		<cfset var softwareList=[]>
		<cfset var httpResponse={}>
		<cfset var jsonAppList=[]>
		<cftry>
			<!--- Info API MDM --->
			<cfset var params=[]>
			<cfset var result={}>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var service=getSoftwareService()>
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
			<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/apps">
			<!--- MAJ du endpoint: Récupération de la liste des applications (Par défaut: 500 apps max par page) --->
			<cfset var pageNum=0>
			<cfset var appsPerPage=500>
			<cfset var appsTotal=0>
			<!--- Nombre de device de la liste qui est retournée par cette fonction --->
			<cfset var appProcessedCount=0>
			<!--- TRUE s'il y a plus d'une page d'applications (500 max par page) --->
			<cfset var hasMoreApps=TRUE>
			<cfloop condition="hasMoreApps EQ TRUE">
				<cfset params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()},
					{type="url",name="pagesize",value=appsPerPage},
					{type="url",name="page",value=pageNum}
				]>
				<cfset result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset var exception=structKeyExists(result,remoting.RESULT()) ? result[remoting.RESULT()]:{}>
					<cfset var errorMsg=structKeyExists(exception,"message") ? exception.message:"Error occured during remoting (HTTP)">
					<cfset var errorDetail=structKeyExists(exception,"detail") ? exception.detail:"Remoting status is #result[remoting.STATUS()]#">
					<cfset hasMoreApps=FALSE>
					<cfthrow type="Custom" message="#errorMsg#" detail="#errorDetail#">
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset hasMoreApps=FALSE>
						<cfthrow type="Custom" message="HTTP response code: #httpCode#">
					<!--- Traitement de la liste de device de la page de résultat (Search) --->
					<cfelse>
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<!--- Type du contenu pour une réponse sans erreur (HTTP 200) --->
							<cfif isInstanceOf(result.result.fileContent,"java.io.ByteArrayOutputStream")>
								<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
								<cfset var jsonResponse=dataUtils.byteArrayToString(responseContent)>
								<!--- Désérialisation JSON --->
								<cfif isJSON(jsonResponse)>
									<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
									<cfset jsonAppList=jsonResponse.deviceApps>
									<cfset appsTotal=structKeyExists(jsonResponse,"total") ? VAL(jsonResponse.total):0>
									<cfif isArray(jsonAppList)>
										<cfset var currentPageSize=arrayLen(jsonAppList)>
										<!--- Ajout des devices dans le résultat retourné --->
										<cfloop index="i" from="1" to="#currentPageSize#">
											<!--- On ne prend que les devices enrollés --->
											<cfset var app=jsonAppList[i]>
											<cfset var mdmApp={
												ID=structKeyExists(app,"ID") ? app.ID:"",
												applicationIdentifier=structKeyExists(app,"applicationIdentifier") ? app.applicationIdentifier:"",
												NAME=structKeyExists(app,"applicationName") ? app.applicationName:"",
												VERSION=structKeyExists(app,"VERSION") ? app.VERSION:"",
												SIZE=structKeyExists(app,"SIZE") ? VAL(app.SIZE):0,
												AUTHOR="",InstallTimeStamp=0
											}>
											<cfset arrayAppend(softwareList,mdmApp)>
											<cfset appProcessedCount=appProcessedCount + 1>
										</cfloop>
										<!--- Condition d'arret: Indique s'il reste encore des pages de devices non traitées --->
										<cfset hasMoreApps=(appsTotal GT appProcessedCount)>
										<cfif hasMoreApps EQ TRUE>
											<!--- Incrémentation du numéro de page --->
											<cfset pageNum=pageNum + 1>
										</cfif>
									<cfelseif structKeyExists(jsonResponse.devices.device,"uuid")>
										<cfset hasMoreApps=FALSE>
										<cfthrow type="Custom" message="Invalid devices list type from HTTP reponse: Should be an Array">
									</cfif>
								<cfelse>
									<cfset hasMoreApps=FALSE>
									<cfthrow type="Custom" message="Invalid HTTP reponse format: Should be java.io.ByteArrayOutputStream">
								</cfif>
							<cfelse>
								<cfset hasMoreApps=FALSE>
								<cfthrow type="Custom" message="Invalid HTTP reponse type: Should be JSON">
							</cfif>
						</cfif>
					</cfif>
				<cfelse>
					<cfset hasMoreApps=FALSE>
					<cfthrow type="Custom" message="Unknwon remoting status value: #result[remoting.STATUS()]#">
				</cfif>
			</cfloop>
			<cfreturn softwareList>			
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn softwareList>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getLocation" returntype="Struct" hint="Localise un device et retourne sa dernière position connue">
		<cfargument name="id_mdm" type="String" required="true" hint="Identifiant MDM du device dans le parc MDM local">
		<cfset var httpResponse={}>
		<cfset var location={}>
		<cftry>
			<cfset var deviceUUID=arguments.id_mdm>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset var service=getDeviceService()>
			<cfset service.endpoint=service.endpoint & "/" & deviceUUID & "/gps">
			<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
			<cfset var params=[
				{type="header",name="accept",value="application/json"},
				{type="header",name="User-Agent",value="SaaSwedo"},
				{type="header",name="aw-tenant-code",value=infos.getToken()}
			]>
			<cfset var result=remoting.invoke(service,"",params)>
			<!--- Erreur durant le reporting --->
			<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
				<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],CLIENT_REQUEST={service=service,params=params}})>
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset var httpCode=remoting.getResponseHttpCode(result)>
				<cfset httpResponse=result[remoting.RESULT()]>
				<!--- Code HTTP d'erreur --->
				<cfif httpCode NEQ 200>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],CLIENT_REQUEST={service=service,params=params}})>
				<cfelse>
					<!--- Désérialisation de la réponse HTTP --->
					<cfif structKeyExists(httpResponse,"FileContent")>
						<cfset var responseContent=httpResponse.FileContent>
						<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
						<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
						<cfif isArray(jsonResponse) AND (arrayLen(jsonResponse) GT 0)>
							<cfset location=jsonResponse[1]>
						</cfif>
					</cfif>
				</cfif>
				</cfif>
			<cfreturn location>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn location>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- =========== Fonctions de gestion des services =========== --->
	
	<cffunction access="private" name="getSoftwareService" returntype="Struct" hint="Retourne une copie des propriétés du Software qui seront
	fournies à la librairie de Remoting (REST). La structure retournée est une copie car son contenu peut etre modifié par les fonctions appelantes">
		<!--- Meme URI que getDeviceService() mais avec un préfixe et des paramètres différents --->
		<cfreturn getDeviceService()>
	</cffunction>
</cfcomponent>