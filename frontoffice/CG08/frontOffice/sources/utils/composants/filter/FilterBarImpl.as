package utils.composants.filter
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class FilterBarImpl extends HBox
	{
		private var _searchText 		: TextInput;
		private var _fieldSelector	: ComboBox;
		
		//the collection on which the search is to be applied
		private var _searchListCollection:ArrayCollection;
		
		//use the datagrid for getting the filter field labels
		private var _dataGrid:DataGrid;
		private var _columns:Array;
		
		
		// composants
		[Bindable]public var _showFieldSelector:Boolean;
		
		public var lbl_filtre:Label;
		[Bindable]public var _showLabelFilter:Boolean = true;
		
		public function FilterBarImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		protected function clearHandler(event:MouseEvent):void
		{
			searchText.text='';
			fieldSelector.selectedIndex=-1;
			if(_searchListCollection!=null){
				_searchListCollection.filterFunction=null;
				_searchListCollection.refresh();
			}
		} 
		
		protected function searchTextChangeHandler(event:Event):void
		{
			doSearch();
		}
		
		//exposed public method to be called to force search from other pages
		public function doSearch():void
		{
			if(dataGrid != null){
				searchListCollection = dataGrid.dataProvider as ArrayCollection;
				
				if(fieldSelector.selectedIndex!=-1 && _searchListCollection!=null){
					_searchListCollection.filterFunction=searchFilterFunction;
					_searchListCollection.refresh();
				}
			}
		}
		
		private function searchFilterFunction(item:Object):Boolean
		{
			if(fieldSelector.selectedItem.dataField != 'any'){
				
				return searchOperation(item, (fieldSelector.selectedItem) as DataGridColumn, searchText.text);
				
			}
			else{
				
				//1. starting iteration from index 1 since index 0 is the 'any' column for search
				for(var i:Number=1;i<_columns.length;i++){
					//2. call the search operation for each row
					//3. even if one column matches return the row
					if(searchOperation(item, _columns[i] as DataGridColumn,searchText.text)==true){
						return true;
					}
				}
			}
			//if none of the columns satisfied the filter then you can return false
			return false;        
		}
		
		private function searchOperation(item:Object, gridColumn:DataGridColumn, searchWord:String):Boolean
		{
			//when the search text is empty show all text
			if(searchWord==''){
				return true;
			}
			
			//for complex object hierarchies walk through the object 
			//get the last string object in the chain you are filtering
			var object:Object=item;
			
			var selectedField:String = gridColumn.dataField;
			var tokens:Array = selectedField.split(".");
			
			for(var i:int=0;i<tokens.length;i++){
				//if the object is null at any instance return false
				//since we will not be able to walk down a null object tree
				if(object==null){
					return false;
				}
				object=object[tokens[i]];
			}
			
			//check again if the object is not null
			if(object==null){
				return false;
			}
			
			//do the actual search
			return object.toString().search(new RegExp(searchWord,"i")) > -1;
		}
		
		//we know for sure it is a grid column
		//we can get the drop down label
		protected function getGridColumnLabel(item:DataGridColumn):String
		{
			return item.headerText;
		}
		
		protected function init(event:FlexEvent):void
		{
			if(_dataGrid != null)
			{
				// Insert the datagrid columns that will be used in the filter
				if(_columns == null)
				{
					_columns = new Array(); 
					var longCol:uint = (_dataGrid.columns as Array).length;
					for(var j:uint = 0; j<longCol; j++)
					{
						if((_dataGrid.columns[j] as DataGridColumn).dataField != null)
						{
							_columns.unshift(_dataGrid.columns[j] as DataGridColumn);
						}
					}
				}
				
				//insert the any column and make it invisible
				var anyColumn:DataGridColumn = new DataGridColumn();
				anyColumn.headerText=ResourceManager.getInstance().getString('CG08','_tout___');
				anyColumn.dataField='any';
				anyColumn.visible=false;
				
				_columns.unshift(anyColumn);
				
				fieldSelector.dataProvider = _columns;
				fieldSelector.selectedItem = anyColumn;
				
				if(_showFieldSelector == false)
				{
					fieldSelector.width = 0;
					fieldSelector.visible = false;
				}
				else
				{
					//fieldSelector.width = 0;
					fieldSelector.visible = true;
				}
				
				if(_showLabelFilter == false)
				{
					lbl_filtre.width = 0;
					lbl_filtre.visible = false;
				}
			}
			
		}
		
		
		[Bindable]
		public function get searchListCollection():ArrayCollection
		{
			return _searchListCollection;
		}
		
		public function set searchListCollection(value:ArrayCollection):void
		{
			_searchListCollection = value;
		}
		
		[Bindable]
		public function get dataGrid():DataGrid
		{
			return _dataGrid;
		}
		
		public function set dataGrid(value:DataGrid):void
		{
			_dataGrid = value;
		}
		
		[Bindable]
		public function get columns():Array
		{
			return _columns;
		}
		
		public function set columns(value:Array):void
		{
			_columns = value;
		}
		
		public function get fieldSelector():ComboBox
		{
			return _fieldSelector;
		}
		
		public function set fieldSelector(value:ComboBox):void
		{
			_fieldSelector = value;
		}
		
		public function get searchText():TextInput
		{
			return _searchText;
		}
		
		public function set searchText(value:TextInput):void
		{
			_searchText = value;
		}
		
	}
}