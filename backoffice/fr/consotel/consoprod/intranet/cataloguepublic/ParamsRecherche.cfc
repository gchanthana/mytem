<cfcomponent output="false" alias="fr.consotel.consoprod.intranet.cataloguepublic.ParamsRecherche">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">
	<cfproperty name="chaine" type="string" default="">
	<cfproperty name="mode" type="numeric" default="0">
	<cfproperty name="operateurId" type="numeric" default="0">
	<cfproperty name="themeId" type="numeric" default="0">
	<cfproperty name="etatVise" type="numeric" default="1">
	<cfproperty name="etatViseAna" type="numeric" default="1">
	<cfproperty name="etatControle" type="numeric" default="1">
	<cfproperty name="etatExporte" type="numeric" default="1">
	<cfproperty name="societeId" type="numeric" default="0">
	<cfproperty name="catalogueClient" type="numeric" default="0">
 	<cfproperty name="operateurNom" type="string" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.dateDebut = "";
		variables.dateFin = "";
		variables.chaine = "";
		variables.mode = 0;
		variables.operateurId = 0;
		variables.themeId = 0;
		variables.etatVise = 1;
		variables.etatViseAna = 1;
		variables.etatControle = 1;
		variables.etatExporte = 1;
		variables.societeId = 0;
		variables.catalogueClient = 1;
		variables.operateurNom = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="ParamsRecherche">
		<cfreturn this>
	</cffunction>
	<cffunction name="getDateDebut" output="false" access="public" returntype="any">
		<cfreturn variables.dateDebut>
	</cffunction>

	<cffunction name="setDateDebut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateDebut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFin" output="false" access="public" returntype="any">
		<cfreturn variables.dateFin>
	</cffunction>

	<cffunction name="setDateFin" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.dateFin = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getChaine" output="false" access="public" returntype="any">
		<cfreturn variables.chaine>
	</cffunction>

	<cffunction name="setChaine" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.chaine = arguments.val>
	</cffunction>

	<cffunction name="getMode" output="false" access="public" returntype="any">
		<cfreturn variables.mode>
	</cffunction>

	<cffunction name="setMode" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.mode = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getOperateurId" output="false" access="public" returntype="any">
		<cfreturn variables.operateurId>
	</cffunction>

	<cffunction name="setOperateurId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.operateurId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getThemeId" output="false" access="public" returntype="any">
		<cfreturn variables.themeId>
	</cffunction>

	<cffunction name="setThemeId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.themeId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getEtatVise" output="false" access="public" returntype="any">
		<cfreturn variables.etatVise>
	</cffunction>

	<cffunction name="setEtatVise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.etatVise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getEtatViseAna" output="false" access="public" returntype="any">
		<cfreturn variables.etatViseAna>
	</cffunction>

	<cffunction name="setEtatViseAna" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.etatViseAna = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getEtatControle" output="false" access="public" returntype="any">
		<cfreturn variables.etatControle>
	</cffunction>

	<cffunction name="setEtatControle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.etatControle = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getEtatExporte" output="false" access="public" returntype="any">
		<cfreturn variables.etatExporte>
	</cffunction>

	<cffunction name="setEtatExporte" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.etatExporte = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getSocieteId" output="false" access="public" returntype="any">
		<cfreturn variables.societeId>
	</cffunction>

	<cffunction name="setSocieteId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.societeId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getCatalogueClient" output="false" access="public" returntype="any">
		<cfreturn variables.catalogueClient>
	</cffunction>

	<cffunction name="setCatalogueClient" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.catalogueClient = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getOperateurNom" output="false" access="public" returntype="any">
		<cfreturn variables.operateurNom>
	</cffunction>

	<cffunction name="setOperateurNom" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.operateurNom = arguments.val>
	</cffunction>
</cfcomponent>