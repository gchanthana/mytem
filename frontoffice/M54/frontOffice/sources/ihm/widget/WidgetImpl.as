package ihm.widget
{
	import event.widget.WidgetEvent;
	
	import manager.widgets.WidgetManager;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.events.FlexEvent;
	
	import service.widget.getwidgetracine.GetWidgetService;
	
	import vo.widgets.WidgetUserVo;
	import vo.widgets.WidgetVo;

	public class WidgetImpl extends Canvas
	{

		[Bindable]
		public var widgetVoDataProvider:ArrayCollection;
		[Bindable]
		public var widgetUserVoDataProvider:ArrayCollection;

		private var _serviceGetWidget:GetWidgetService;

		private var _widgetManager:WidgetManager;
		private var _emptyVoWidget:ArrayCollection=new ArrayCollection();
		private var _numTab:Number=1;
		private var _nbShowedItem:int=4; /** nb des widgets à afficher */

		public function WidgetImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteWidget);
		}

		/**
		 * permet de creer le manager des widgets et le service pour chercher la liste des widgets
		 * @param evt
		 */
		private function onCreationCompleteWidget(e:FlexEvent):void
		{
			_serviceGetWidget=new GetWidgetService();
			_serviceGetWidget.getListWidget();
			_serviceGetWidget.model.addEventListener(WidgetEvent.UPDATE_DATA_WIDGET_EVENT,setWidgetManager);
			
			_widgetManager=new WidgetManager;
			_widgetManager.container=this;
			_widgetManager.nbShowedItem=_nbShowedItem;
		}

		/**
		 * permet de creer un tableau de 4 element correspondent aux 4 widgets qui seront affichés dans l'IHM
		 * @param widgetManager
		 */
		private function setWidgetManager(e:WidgetEvent):void
		{
			widgetVoDataProvider=_serviceGetWidget.model.widgets; /** liste des widgets récupéré par le service */
			widgetUserVoDataProvider=_serviceGetWidget.model.widgetsUser;

			var newWidgetVo:WidgetUserVo;

			for (var i:int=0; i < _nbShowedItem; i++)
			{
				newWidgetVo=new WidgetUserVo();
				newWidgetVo.positionIHM=i; /** préciser le position de chaque widget  */
				newWidgetVo.codeWidget="";
				_emptyVoWidget.addItem(newWidgetVo);
			}
			_widgetManager.setWidgetVoInArray(widgetVoDataProvider,widgetUserVoDataProvider,_emptyVoWidget, numTab);
		}
		
		public function get numTab():Number
		{
			return _numTab;
		}
		
		public function set numTab(value:Number):void
		{
			_numTab=value;
		}
	}
}
