package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.ContrainteSuppressionEvent;
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.service.GestionPoolService;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	
	[Bindable]
	public class ListePool extends VBox
	{
		public var gestionPoolService : GestionPoolService = new GestionPoolService();
		
		
		//COMPONANT
		public var dg_pool_gestionnaire: DataGrid;
		private var popContrainteSuppressionIHM : ContrainteSuppressionIHM;
		private var popUpdatePoolIHM : ParametrePoolIHM;
		
		public function ListePool()
		{
			
		}
		public function init(evt : FlexEvent):void
		{
			gestionPoolService.listePool(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		public function modifier(pool : PoolGestionnaire):void
		{
			popUpdatePoolIHM = new ParametrePoolIHM();
			popUpdatePoolIHM.poolGestionnaire = pool;
			addPop();
		}
		public function addPool(event:MouseEvent):void
		{
			popUpdatePoolIHM = new ParametrePoolIHM();
			addPop();
			
		}
		private function parametrePoolCOmpleteHandler(evt : GestionPoolEvent):void
		{
			//PopUpManager.removePopUp(popUpdatePoolIHM);
			//gestionPoolService.listePool(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			initData();
		}
		public function supprimer(data : PoolGestionnaire):void
		{
			gestionPoolService.addEventListener(GestionPoolEvent.LISTE_CONTRAINTE_COMPLETE,listeContrainteOfPoolHandler);
			gestionPoolService.listeContrainteOfPool((dg_pool_gestionnaire.selectedItem as PoolGestionnaire).IDPool);
		}

	
		private function listeContrainteOfPoolHandler(evt : GestionPoolEvent):void
		{
			if(gestionPoolService.col_contrainte_of_pool.length>0) // il existe des contraintes qui empêche la suppression
			{
				popContrainteSuppressionIHM = new ContrainteSuppressionIHM();
				popContrainteSuppressionIHM.addEventListener(ContrainteSuppressionEvent.VALIDATE_SUPPRESSION_POOL,supprimerPoolSelected);
				popContrainteSuppressionIHM.listeContraintes = gestionPoolService.col_contrainte_of_pool;
				
				//Test singulier ou pluriel
				if(gestionPoolService.col_contrainte_of_pool.length>1)
				{
					popContrainteSuppressionIHM.messageDecription = 'Des éléments sont rattachés au pool '+(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).libelle_pool+'. La suppression de ce pool entrainera la suppression de ces liens.';
				}
				else
				{
					popContrainteSuppressionIHM.messageDecription = 'Un élément est rattaché au pool '+(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).libelle_pool+'. La suppression de ce pool entrainera la suppression de ce lien.';
				}
				popContrainteSuppressionIHM.messageConfirmation = 'Êtes vous sûr de vouloir supprimer le pool ?';
				PopUpManager.addPopUp(popContrainteSuppressionIHM,this,true);
				PopUpManager.centerPopUp(popContrainteSuppressionIHM);
			}
			else // pas de contrainte on peut supprimer
			{
				supprimerPoolSelected();
			}
		}
		private function supprimerPoolSelected(evt : ContrainteSuppressionEvent=null):void
		{
			//TODO REQUETE DE SUPPRESSION
			gestionPoolService.addEventListener(GestionPoolEvent.REMOVE_POOL_COMPLETE,remove_complete_handler);
			gestionPoolService.remove_pool_gestionnaire((dg_pool_gestionnaire.selectedItem as PoolGestionnaire).IDPool);
		}
		private function remove_complete_handler(evt : GestionPoolEvent):void
		{
			ConsoviewAlert.afficherOKImage("le pool "+dg_pool_gestionnaire.selectedItem.libelle_pool+" a bien été supprimé.");
			initData();
		}
		public function initData():void
		{
			gestionPoolService.listePool(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		
		private function addPop():void
		{
			popUpdatePoolIHM.addEventListener(GestionPoolEvent.PARAMETRE_POOL_COMPLETE,parametrePoolCOmpleteHandler);
			PopUpManager.addPopUp(popUpdatePoolIHM,this,true);
			PopUpManager.centerPopUp(popUpdatePoolIHM);
		}
		
	}
}