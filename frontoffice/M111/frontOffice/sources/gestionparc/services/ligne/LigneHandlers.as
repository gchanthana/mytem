package gestionparc.services.ligne
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.utils.gestionparcMessages;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Envoie les données reçues à la classe <code>LigneDatas</code>,
	 * au retour des procédures appelées dans <code>LigneServices</code>.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 31.08.2010
	 * 
	 * </pre></p>
	 * 
	 * @see gestionparc.services.ligne.LigneDatas
	 * @see gestionparc.services.ligne.LigneServices
	 * 
	 */
	public class LigneHandlers
	{
		// VARIABLES------------------------------------------------------------------------------
		
		private var myDatas : LigneDatas;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Instancie les datas.
		 * </pre></p>
		 * 
		 * @param instanceOfDatas
		 * 
		 */
		public function LigneHandlers(instanceOfDatas:LigneDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		// INTERNALS FUNCTIONS------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getInfosFicheLigne()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetInfosFicheLigne()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#getInfosFicheLigne()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataGetInfosFicheLigne()
		 * 
		 */	
		internal function getInfosFicheLigneResultHandler(re:ResultEvent):void
		{
			if((re.result is Array) && (re.result as Array)[0] == 1)
			{
				myDatas.treatDataGetInfosFicheLigne(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_FICHE_LIGNE_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>setInfosFicheLigne()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosFicheLigne()</code>.
		 * Si le retour est KO, un message d'erreur est affiché selon le retour erreur de la base.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#setInfosFicheLigne()
		 * @see gestionparc.services.ligne.LigneDatas#treatSetInfosFicheLigne()
		 * 
		 */	
		internal function setInfosFicheLigneResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				myDatas.treatSetInfosFicheLigne();
			}
			else if(re.result == -1)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.UPDATE_FICHE_LIGNE_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
			else if(re.result == -2)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.NUMERO_LIGNE_EXISTANT,
					gestionparcMessages.ERREUR);
			}
			else if(re.result == -3)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.NUMERO_SIM_EXISTANT,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>fournirListeAbonnementsOptions()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataFournirListeAbonnementsOptions()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#fournirListeAbonnementsOptions()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataFournirListeAbonnementsOptions()
		 * 
		 */	
		internal function fournirListeAbonnementsOptionsResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataFournirListeAbonnementsOptions(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_ABO_OPT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>ajouterProduitAMesFavoris()</code> (appel de procédure).
		 * Si le retour est OK, alors rien.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#ajouterProduitAMesFavoris()
		 */	
		internal function ajouterProduitAMesFavorisResultHandler(re:ResultEvent):void
		{
			if(re.result <= 0)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.PB_GESTION_FAVORIS,
					gestionparcMessages.ERREUR);
			} 
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>supprimerProduitDeMesFavoris()</code> (appel de procédure).
		 * Si le retour est OK, alors rien.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#supprimerProduitDeMesFavoris()
		 */
		internal function supprimerProduitDeMesFavorisResultHandler(re:ResultEvent):void
		{
			if(re.result <= 0)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.PB_GESTION_FAVORIS,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>listeLigneSimSansTerm()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataListeLigneSimSansTerm()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#listeLigneSimSansTerm()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataListeLigneSimSansTerm()
		 * 
		 */	
		internal function listeLigneSimSansTermResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataListeLigneSimSansTerm(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LIGNE_SIM_SS_TERM_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		} 
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>listeLigneFixeSansEqp()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataListeLigneFixeSansEqp()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#listeLigneFixeSansEqp()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataListeLigneFixeSansEqp()
		 * 
		 */	
		internal function listeLigneFixeSansEqpResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataListeLigneFixeSansEqp(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LIGNE_SIM_SS_TERM_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		} 
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>listeLigneSansTerm()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataListeLigneSansTerm()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#listeLigneSansTerm()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataListeLigneSansTerm()
		 * 
		 */	
		internal function listeLigneSansTermResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataListeLigneSansTerm(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LIGNE_SIM_SS_TERM_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		} 
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>majColFacture()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataMajColFacture()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#majColFacture()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataMajColFacture()
		 * 
		 */	
		internal function majColFactureResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataMajColFacture(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_INFO_COL_FACURE_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getActionsPossiblesByEtat()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetActionsPossiblesByEtat()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#getActionsPossiblesByEtat()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataGetActionsPossiblesByEtat()
		 *  
		 */	
		internal function getActionsPossiblesByEtatResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetActionsPossiblesByEtat(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LISTE_ACT_RACC_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>doActionRaccordement()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataDoActionRaccordement()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#doActionRaccordement()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataDoActionRaccordement()
		 *  
		 */	
		internal function doActionRaccordementResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataDoActionRaccordement();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.UPDATE_RACCOR_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getTeteLigne()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetTeteLigne()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#getTeteLigne()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataGetTeteLigne()
		 *  
		 */	
		internal function getTeteLigneResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetTeteLigne(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LINE_DISPO_GPEMENT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getLignesGroupement()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetLignesGroupement()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re
		 * @return void
		 * 
		 * @see gestionparc.services.ligne.LigneServices#getLignesGroupement()
		 * @see gestionparc.services.ligne.LigneDatas#treatDataGetLignesGroupement()
		 *  
		 */	
		internal function getLignesGroupementResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetLignesGroupement(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LINE_GPEMENT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function resilierLigneResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetLignesGroupement(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LINE_GPEMENT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function renumeroterLigneResultHandler(re:ResultEvent):void
		{
			if(re.result
				&& re.result > -1 )
			{
				myDatas.treatDatarenumeroterLigne(re.result);
			}
			else
			{
				if(re.result == -2)
				{
					ConsoviewAlert.afficherError(
						gestionparcMessages.RENUM_LIGNE_IMPOSSIBLE_CAUSE_LIGNE_EXISTE,
						gestionparcMessages.ERREUR);
				}
				else
				{
					ConsoviewAlert.afficherError(
						gestionparcMessages.RENUM_LIGNE_IMPOSSIBLE,
						gestionparcMessages.ERREUR);
				}
			}
		}
		
		internal function updateEtatLigneResultHandler(re:ResultEvent):void
		{ 
			if(re.result
				&& re.result > -1 )
			{
				myDatas.updateEtatLigne(re);
			}
			else
			{
				
				ConsoviewAlert.afficherError(
					gestionparcMessages.ERREUR,
					gestionparcMessages.ERREUR);
				
			}
		}
		
		internal function getListeDestinatairesEchangeSIM(re:ResultEvent):void
		{ 
			if(re.result)
			{
				myDatas.treatGetListeDestinatairesEchangeSIM(re.result as ArrayCollection);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ERREUR,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function sendEmailEchangeSimStock(re:ResultEvent):void
		{ 
			if(re.result > -1)
			{
				myDatas.treatMessageEchangeSim();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ERREUR,
					gestionparcMessages.ERREUR);
				
			}
		}
	}
}