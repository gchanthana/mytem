<!--- =========================================================================
Classe: ListeFactureGroupeCompteStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent name="ListeFactureGroupeCompteStrategy" extends="ListeFactureStrategy" hint="">
<!--- METHODS --->
	<cffunction name="rechercheFactureByNumber" access="public" returntype="Query" output="false" >
		<cfargument name="NUMERO_CLE" required="false" type="string" default="" displayname="string NUMERO_CLE" hint=%qt%%paramNotes%%qt% />
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfquery name="listeFactures" datasource="#session.OffreDSN#">
			
		</cfquery>
		<cfreturn listeFactures>
	</cffunction>
</cfcomponent>