<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.security.SecurityManagement"
implements="fr.consotel.api.mdm.partner.private.ws.ISecurityManagement" hint="Fournisseur des services de gestion de la sécurité des équipements mobiles
Les méthodes de cette implémentation ne sont pas soumises à la vérification de l'authentification">
	<cffunction access="public" name="getService" returntype="fr.consotel.api.mdm.partner.private.ws.ISecurityManagement" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.security.FSecure" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getCorporationService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSCorporationService" hint="Retourne le service de gestion des Corporations">
		<cfset var fsecureRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"CORP_SERVICE")>
			<cfset var corpService=createObject("component","fr.consotel.api.mdm.partner.private.ws.security.WSCorporationService").getWSService(fsecureRepository)>
			<cfset VARIABLES["CORP_SERVICE"]=corpService>
		</cfif>
		<cfreturn VARIABLES["CORP_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getAntitheftService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSAntitheftService" hint="Retourne le service : Anti-Theft">
		<cfset var fsecureRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"ANTITHEFT_SERVICE")>
			<cfset var serviceInstance=createObject("component","fr.consotel.api.mdm.partner.private.ws.security.WSAntitheftService").getWSService(fsecureRepository)>
			<cfset VARIABLES["ANTITHEFT_SERVICE"]=serviceInstance>
		</cfif>
		<cfreturn VARIABLES["ANTITHEFT_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSMultiLicenseService" hint="Retourne le service : Multi Licence Services">
		<cfset var fsecureRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"MULTI_LICENCE_SERVICE")>
			<cfset var serviceInstance=createObject("component","fr.consotel.api.mdm.partner.private.ws.security.WSMultiLicenseService").getWSService(fsecureRepository)>
			<cfset VARIABLES["MULTI_LICENCE_SERVICE"]=serviceInstance>
		</cfif>
		<cfreturn VARIABLES["MULTI_LICENCE_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSSingleLicenseService" hint="Retourne le service : Single Licence Services">
		<cfset var fsecureRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"SINGLE_LICENCE_SERVICE")>
			<cfset var serviceInstance=createObject("component","fr.consotel.api.mdm.partner.private.ws.security.WSSingleLicenseService").getWSService(fsecureRepository)>
			<cfset VARIABLES["SINGLE_LICENCE_SERVICE"]=serviceInstance>
		</cfif>
		<cfreturn VARIABLES["SINGLE_LICENCE_SERVICE"]>
	</cffunction>
	
	<cffunction access="public" name="getOtherService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSOtherService" hint="Retourne le service : Other Services">
		<cfset var fsecureRepository=getWSRepository()>
		<!--- Instanciation ou Récupération du service --->
		<cfif NOT structKeyExists(VARIABLES,"OTHER_SERVICE")>
			<cfset var serviceInstance=createObject("component","fr.consotel.api.mdm.partner.private.ws.security.WSOtherService").getWSService(fsecureRepository)>
			<cfset VARIABLES["OTHER_SERVICE"]=serviceInstance>
		</cfif>
		<cfreturn VARIABLES["OTHER_SERVICE"]>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.security.FSecure" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
</cfcomponent>