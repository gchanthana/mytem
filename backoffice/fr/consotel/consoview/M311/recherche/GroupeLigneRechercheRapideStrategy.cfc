<cfcomponent name="GroupeLigneRechercheRapideStrategy" displayname="GroupeLigneRechercheRapide" hint="Recherche rapide pour un groupe de ligne">
	<cffunction name="init" access="public" output="false" returntype="GroupeCompletStrategy" displayname="GroupeCompletStrategy init()" hint="Initialize the GroupeCompletStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "GroupeLigne">
	</cffunction>	 
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric, string)">		
		<cfargument name="idGroupe_client" required="true" type="numeric" default="" />
		<cfargument name="idCliche" required="true" type="numeric" default="" />
		<cfargument name="chaine"   required="true" type="string"  default="" />
		<cfargument name="niveau"   required="true" type="string"  default=""/>
		<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
			
			<!--- 
						search_nodes_hierarchie_v2 ( p_chaine IN VARCHAR2,
													p_idgroupe IN INTEGER, 
													p_niveau IN VARCHAR2,
													p_idorga IN INTEGER, 
													p_idperiode_deb IN INTEGER,
													p_idperiode_fin IN INTEGER,
													p_retour OUT SYS_REFCURSOR
			 --->
		
			
			<cfset idorga = "#SESSION.PERIMETRE.SELECTED_ID_ORGA#">
			<cfstoredproc datasource="E0" procedure="PKG_M311.SEARCH_NODES_HIERARCHIE_V2">		
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_chaine" 			value="#chaine#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupe_client#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_niveau"	 		value="#niveau#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorga" 			value="#idorga#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idperiode_deb" 	value="#iddatedeb#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idperiode_fin" 	value="#iddatefin#"/>
				<cfprocresult name="p_result"/>        
			</cfstoredproc>

		<cfreturn p_result/>
	</cffunction>	
	
	<cffunction name="getLignes" access="public" returntype="query" output="false" displayname="query getData(numeric, numeric)">
		<cfargument name="idGroupe" required="true" type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>
		<cfargument name="idCliche" required="true" type="numeric" default="" displayname="numeric ID" 	hint="Initial value for the IDproperty."/>
		<cfargument name="niveau"   required="true" type="string"  default="" displayname="niveau orga" hint="Initial value for the chaine property."/>
		<cfargument name="iddatedeb" 	type="numeric" 	required="true">
		<cfargument name="iddatefin"   type="numeric" 	required="true">
			
 			<!--- 
				search_nodes_sous_tetes_v2 ( 
				p_idgroupe IN INTEGER, 
				p_niveau IN VARCHAR2, 
				p_idorga IN INTEGER,
				p_idperiode_deb IN INTEGER,
				p_idperiode_fin IN INTEGER,
				p_chaine IN VARCHAR2,
				p_retour OUT SYS_REFCURSOR)
			 --->
			
			
		 	<cfset idorga = "#SESSION.PERIMETRE.SELECTED_ID_ORGA#">
			<cfstoredproc datasource="E0" procedure="PKG_M311.SEARCH_NODES_SOUS_TETES_V2">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupe#"/>				
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_niveau" 		value="#niveau#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idorganisation" 	value="#idorga#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idperiode_deb" 	value="#iddatedeb#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idperiode_fin" 	value="#iddatefin#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_chaine" 		value=""/>
				<cfprocresult name="p_result"/>
			</cfstoredproc>
						
		<cfreturn p_result/>
	</cffunction>
		
</cfcomponent> 