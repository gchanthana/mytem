package univers.inventaire.lignesetservices.views
{
	import univers.inventaire.lignesetservices.applicatif.GestionTechniqueLignesApp;
	
	public interface IGestionTechniqueLignesApp
	{
		function get gestionTechnique():GestionTechniqueLignesApp;
		function set gestionTechnique(gestion : GestionTechniqueLignesApp):void;
		
	}
}