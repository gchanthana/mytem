package rapport.service.datafacturation
{
		
	import flash.events.EventDispatcher;
	import mx.collections.ICollectionView;
	import mx.rpc.events.ResultEvent;
	import rapport.event.AccueilEvent;
	
	[Bindable]
	public class DataHandlers extends EventDispatcher
	{
		private var _theModel:DataModel;
				
		public function DataHandlers(value:DataModel)
		{
			_theModel = value;	
		}
		
		internal function getData1ResultHandler(event:ResultEvent):void
		{
			
			if(event.result)
			{
				_theModel.data1 = event.result as ICollectionView; 
				
			}
			
			dispatchEvent(new AccueilEvent(AccueilEvent.ACCUEIL_EVENT));
		}
		
		internal function getData2ResultHandler(event:ResultEvent):void
		{
			
			if(event.result)
			{
				var result : Array = event.result as Array;
				if(result.length > 1)
				{
					_theModel.data2  = event.result[0] as ICollectionView;
					_theModel.data3 = event.result[1] as ICollectionView;
				}
			}
			
			dispatchEvent(new AccueilEvent(AccueilEvent.ACCUEIL_EVENT));
		}
		
		internal function getData3ResultHandler(event:ResultEvent):void
		{
			
			if(event.result)
			{
				_theModel.data3 = event.result as ICollectionView; 
				
			}
			
			dispatchEvent(new AccueilEvent(AccueilEvent.ACCUEIL_EVENT));
		} 
	

	}
}