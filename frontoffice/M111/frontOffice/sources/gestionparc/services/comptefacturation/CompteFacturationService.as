package gestionparc.services.comptefacturation
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class CompteFacturationService
	{
		[Bindable]
		public var model:CompteFacturationData;
		public var myHandler:CompteFacturationHandler;

		public function CompteFacturationService() 
		{
			model=new CompteFacturationData();
			myHandler=new CompteFacturationHandler(model);
		}

		public function getCompteFacturation(listeIdSousTete: String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.CompteFacturationOperateurServices", "getCompteLstLignes", myHandler.ViewResultHandler);
			RemoteObjectUtil.callService(op,listeIdSousTete);
		}
	}
}