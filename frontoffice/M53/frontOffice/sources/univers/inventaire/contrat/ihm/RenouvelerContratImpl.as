package univers.inventaire.contrat.ihm
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.commande.ihm.SelectionBaseViewImpl;
	import univers.inventaire.commande.ihm.SelectionEquipementsIHM;
	import univers.inventaire.commande.ihm.ValiderCommandeIHM;
	import univers.inventaire.commande.system.ArticleEvent;
	import univers.inventaire.commande.system.CommandeEvent;
	import univers.inventaire.commande.system.GestionOptionsMobile;
	import univers.inventaire.commande.system.GestionPanierMobile;
	import univers.inventaire.commande.system.GestionRenouvellementMobile;
	
	
	 
	public class RenouvelerContratImpl extends GererOptionsAbonnementImpl
	{
		public function RenouvelerContratImpl()
		{
			super();
		}
		
		
		override protected function commitProperties():void
		{	
			super.commitProperties();
					
			if(article != null)
			{
				cboEngagementEq.selectedIndex = ConsoviewUtil.getIndexByLabel(cboEngagementEq.dataProvider as ArrayCollection,"value",article.dureeEngagementEquipement.toString()); 
				cboEngagementRes.selectedIndex = ConsoviewUtil.getIndexByLabel(cboEngagementRes.dataProvider as ArrayCollection,"value",article.dureeEngagementRessources.toString());
				lignesUtils.listeNumerosValides.push(article.sousTete.toString());	
			}			
		}
		
	///////////////////////////////////  HANDLERS  /////////////////////////////////////////////////////////////////////////
		
		override protected function txtFiltreChangeHandler(event:Event): void
	    {		
	    	if(dgListe.dataProvider)
	    	{	
	    		(dgListe.dataProvider as XMLListCollection).filterFunction = listeEquipementsFilterFunction;
	    		(dgListe.dataProvider as XMLListCollection).refresh();
	    	}
	    }
	    
	  
	    override protected function btAjouterRessourcesClickHandler(event:MouseEvent):void
	  	{	
	  		//MUTED
	  	}
	  	
	  	override protected function btRetirerRessourcesClickHandler(event:MouseEvent):void
		{
			//MUTED
		}
	    		
		override protected function btValiderClickHandler(event:MouseEvent):void
		{
			var boolRessources:Boolean = false;
			var boolEquipement:Boolean = false;
			var boolEngagement:Boolean = false;
			
			var message:String="\n";
			var nbEquipement:Number = new XMLList(article.article.equipements.equipement).length();
			
			if(Number(cboEngagementEq.text.split(" ")[0]) >0 
					&& nbEquipement > 0)			
			{
				boolEquipement = true;
				article.dureeEngagementEquipement = Number(cboEngagementEq.text.split(" ")[0]);
				article.libelleContratEq = txtDesignationContratEq.text;
				
				if(article.libelleContratEq.length == 0)
				{
					article.libelleContratEq = "Garantie";
				}
			}
			else
			{
				boolEquipement = false;
			}
			
			if(Number(cboEngagementRes.text.split(" ")[0])>0)
			{
				boolEngagement = true;	
			}
			else
			{
				boolEngagement = false
			}
			
			if(boolEngagement)
			{
				if(boolEquipement && nbEquipement > 0)
				{
					article.dureeEngagementEquipement = Number(cboEngagementEq.text.split(" ")[0]);	
				}
				else
				{
					Alert.show("Vous devez indiquer une durée de garantie pour l'équipement");
					return;
				}
				article.sousTete = txtNumLigne.text;
				article.codeInterne = txtCodeInterne.text;	
				article.dureeEngagementRessources = Number(cboEngagementRes.text.split(" ")[0]);
				if(boolCreate)
				{
					var gestionArticles:GestionPanierMobile = new GestionPanierMobile();
					gestionArticles.addItem( article );
					
					removePopUp();
					_popUp = new ValiderCommandeIHM();
					
					ValiderCommandeIHM(_popUp).commande = commande;
					ValiderCommandeIHM(_popUp).gestionArticles = gestionArticles;
					ValiderCommandeIHM(_popUp).addEventListener(CommandeEvent.COMMANDE_CREATED,commandeMobileSAvedHandler);
					PopUpManager.addPopUp(_popUp,this,true);
					PopUpManager.centerPopUp(_popUp);	
				}
				else
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_UPDATED));
				} 
				PopUpManager.removePopUp(this);	
			}
			else
			{
				Alert.show("Vous devez indiquer une durée d'engagement","Erreur");
			}
			
		}
		
		private function commandeMobileSAvedHandler(event:CommandeEvent):void
		{
			removePopUp();
			PopUpManager.removePopUp(this);
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////		
		protected function listeEquipementsFilterFunction(item:Object):Boolean
	    {	
	    	
		    var rfilter:Boolean = true;
		    var node:XML = XML(item);
		    
		    // Filtre sur le libelle des équipements
		    if (node.libelle != undefined)
		    {  
		        rfilter = rfilter && (String(node.libelle ).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le libelle du type d équipements
		    if (node.type != undefined)
		    {  
		        rfilter = rfilter && (String(node.type).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le prix
		    if (node.prix != undefined)
		    {  
		        rfilter = rfilter && (String(node.prix).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }		    
		    return rfilter;
	    }
	    
	   //////////////////////////// ACCESSORS ////////////////////////////////////////////////////////////////////////
	   override public function get gestionAbosOptions():GestionOptionsMobile
		{
			if(_gestionAbosOptions == null) _gestionAbosOptions = new GestionRenouvellementMobile();
			
			return _gestionAbosOptions;
		}		
	}
}
