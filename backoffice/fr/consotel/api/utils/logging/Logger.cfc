<cfcomponent displayname="fr.consotel.api.utils.logging.Logger" implements="fr.consotel.api.utils.logging.ILogger" hint="ILogger implementation using CFLOG">
	<!--- LOGGING UTILITY IMPLEMENTATION (Author : Cedric)
		- Log filename and location :
			- Default is : [CF_ROOT]/logs/application.log
			- If APPLICATION scope is defined with a non empty name : [CF_ROOT]/logs/application.log
	 	- TODO :
	 		- ConsoView LOG_REPORT data integration (See fr.consotel.util.consoview.tracking.SessionTracker)
	 		- API java.util.logging Integration : Logger, LoggerManager
	 --->
	
	<cffunction access="public" name="getLogger" returntype="fr.consotel.api.utils.logging.ILogger"
			hint="Find or create a logger for a named subsystem. This method should be called befor any other">
		<cfargument name="name" type="string" required="true" hint="A name for the logger">
		<cfset THIS.ALL="ALL">
		<cfset THIS.OFF="OFF">
		<cfset THIS.SEVERE="SEVERE">
		<cfset THIS.WARNING="WARNING">
		<cfset THIS.INFO="INFO">
		<cfset THIS.CONFIG="CONFIG">
		<cfset THIS.FINE="FINE">
		<cfset THIS.FINER="FINER">
		<cfset THIS.FINEST="FINEST">
		<cfset VARIABLES.TYPES=structNew()>
		<cfset VARIABLES.TYPES.INFO=THIS.INFO & "," & THIS.CONFIG & "," & THIS.FINE & "," & THIS.FINER & "," & THIS.FINEST>
		<cfset VARIABLES.TYPES.WARNING=THIS.WARNING & "," & THIS.CONFIG & "," & THIS.FINE & "," & THIS.FINER & "," & THIS.FINEST>
		<cfset VARIABLES.TYPES.STD_ERROR=THIS.FINER & "," & THIS.FINE & "," & THIS.CONFIG>
		<cfset VARIABLES.TYPES.FATAL_ERROR=THIS.SEVERE & "," & THIS.FINER & "," & THIS.FINE & "," & THIS.CONFIG>
		<cfset VARIABLES.DEFAULT_LOG_FILENAME="application">
		<cfset VARIABLES.NAME="fr.consotel.api.utils.logging.Logger">
		<cfif LEN(TRIM(ARGUMENTS.name)) GT 0>
			<cfset VARIABLES.NAME=ARGUMENTS.name>
		</cfif>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getName" returntype="string" hint="Get the name for this logger">
		<cfreturn VARIABLES.NAME>
	</cffunction>
 
	<cffunction access="public" name="logInfo" returntype="void" hint="Log an INFORMATION type message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog)">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object. Not supported for current version">
		<cfset logPrefix="[" & VARIABLES.NAME & ":" & ARGUMENTS.level & "]">
		<cflog application="true" type="information" file="#getLogFileName()#" text="#logPrefix# #ARGUMENTS.msg#">
	</cffunction>
	
	<cffunction access="public" name="logWarning" returntype="void" hint="Log a WARNING message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog)">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object. Not supported for current version">
		<cfset logPrefix="[" & VARIABLES.NAME & ":" & ARGUMENTS.level & "]">
		<cflog application="true" type="warning" file="#getLogFileName()#" text="#logPrefix# #ARGUMENTS.msg#">
	</cffunction>
	
	<cffunction access="public" name="logError" returntype="void" hint="Log an ERROR message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog)">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object. Not supported for current version">
		<cfset logPrefix="[" & VARIABLES.NAME & ":" & ARGUMENTS.level & "]">
		<cflog application="true" type="error" file="#getLogFileName()#" text="#logPrefix# #ARGUMENTS.msg#">
		<cflog application="true" type="error" file="exception" text="#logPrefix# #ARGUMENTS.msg#">
	</cffunction>
	
	<cffunction access="public" name="logFatalError" returntype="void" hint="Log a FATAL type message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog). Not supported for current version">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object. Not supported for current version">
		<cfset logPrefix="[" & VARIABLES.NAME & ":" & ARGUMENTS.level & "]">
		<cflog application="true" type="fatal" file="#getLogFileName()#" text="#logPrefix# #ARGUMENTS.msg#">
		<cflog application="true" type="fatal" file="exception" text="#logPrefix# #ARGUMENTS.msg#">
	</cffunction>
	
	<cffunction access="private" name="getLogFileName" returntype="string" hint="Returns log filename OR an empty string if this logger does not write in a file">
		<cfreturn VARIABLES.DEFAULT_LOG_FILENAME>
	</cffunction>
	
	<!--- 
	<cffunction access="public" name="logException" returntype="void">
		<cfargument name="level" type="string" required="true">
		<cfargument name="desc" type="string" required="true">
		<cfargument name="exception" type="any" required="false" hint="Exception Object">
		<cfset levelList="information,warning,error,fatal information">
		<cfset thisName="UNKNOWN">
		<cfset nameKey="NAME">
		<cfset logLevel="ERROR">
		<cfset msgKey="MESSAGE">
		<cfset detailKey="DETAIL">
		<cfset stackTraceKey="STACKTRACE">
		<cfset tagContextKey="TAGCONTEXT">
		<cfset templateKey="TEMPLATE">
		<cfset lineKey="LINE">
		<cfset columnKey="COLUMN">
		<cfset TAG_CONTEXT_LENGTH=0>
		<cfset currentColumn=0>
		<cfset currentLine=0>
		<cfset exceptionObject="">
		<cfif structKeyExists(VARIABLES,nameKey)>
			<cfset thisName=VARIABLES[nameKey]>
		</cfif>
		<cfif listFindNoCase(levelList,ARGUMENTS["level"],",") LTE 0>
			<cftrace type="#logLevel#" text="#thisName# Logger : Invalid level value #arguments.level#">
		<cfelse>
			<cfset logLevel=ARGUMENTS["level"]>
		</cfif>
		<cftrace type="#logLevel#" text="[DESC] #thisName# : #arguments.desc#">
		<cfif structKeyExists(ARGUMENTS,"exception")>
			<cfset exceptionObject=ARGUMENTS["exception"]>
			<cfset TAG_CONTEXT_LENGTH=arrayLen(exceptionObject[tagContextKey])>
			<cftrace type="#logLevel#" text="[MESSAGE/DETAIL] #thisName# : #exceptionObject[msgKey]# #exceptionObject[detailKey]#">
			<cfloop index="i" from="1" to="#TAG_CONTEXT_LENGTH#">
				<cfset currentLine=exceptionObject[tagContextKey][i][lineKey]>
				<cfset currentColumn=exceptionObject[tagContextKey][i][columnKey]>
				<cftrace type="#logLevel#"
					text="[TAGCONTEXT (#i#)] #thisName# : #exceptionObject[tagContextKey][i][templateKey]# at L#currentLine#:C#currentColumn#">
			</cfloop>
			<cftrace type="#logLevel#" text="[STACKTRACE] #thisName# : #exceptionObject[stackTraceKey]#">
		</cfif>
	</cffunction>
	 --->
</cfcomponent>