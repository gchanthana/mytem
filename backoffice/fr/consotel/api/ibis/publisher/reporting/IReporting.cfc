<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.IReporting" hint="Représentation une demande d'exécution de rapport (BIP)">
	<cffunction name="getReportingName" returntype="String" hint="Retourne le nom du reporting">
	</cffunction>

	<cffunction name="getDeliveryType" returntype="String" hint="Retourne le type de la stratégie de diffusion">
	</cffunction>

	<cffunction name="getProperties" returntype="Struct" hint="Retourne la structure contenant les valeurs des propriétés et paramètres (Eventuellement modifiées)">
	</cffunction>
</cfinterface>