<cfscript>
// System
call = {};
call['url'] = "https://sonar.managemobility.com/api/orders/";
call['url'] = "http://backoffice-dev.local/fr/saaswedo/api/order/bridge/FakeServer.cfm";
call['method'] = "Post";
call['multipart'] = "yes";

boundary = "00ab558a-4ba9-46c6-9c5a-73e8419241bf";
apiKey = "AF6EB676-D0B7-4E48-9F51-6A157570005E";

ContentType = "Content-Type: multipart/form-data; boundary=" & boundary;
prefix = "::str::";
//Order

OrderCarrier = {};
OrderCarrier['Name'] = prefix & "Sprint";
OrderCarrier['AccountId'] = prefix & "000000002";

OrderRequestor = {};
OrderRequestor['FullName'] = prefix & "Brice Miramont";
OrderRequestor['Email'] = prefix & "monitoring@saaswedo.com";

ShippingAddress = {};
ShippingAddress['Recipient'] = "saaswedo";
ShippingAddress['Company'] = "saaswedo";
ShippingAddress['AddressLine1'] = "saaswedo";
ShippingAddress['AddressLine2'] = "";
ShippingAddress['City'] = "Atlanta";
ShippingAddress['State'] = "GA";
ShippingAddress['PostalCode'] = prefix & "30005";

Products = [];

product1 = {};
product1['Type'] = prefix & "1";
product1['SKU'] = prefix & "885909971640";
product1['Price'] = prefix & "849.99";
product1['PriceType'] = prefix & "2";
product1['Qty'] = prefix & "2";

Plans = [];

plan1 = {};
plan1['Code'] = prefix & "PDSETV108";
plan1['Price'] = prefix & "7.00";
plan1['Type'] = prefix & "2";

Plans[1] = plan1;

plan2 = {};
plan2['Code'] = prefix & "U2748";
plan2['Price'] = prefix & "39.99";
plan2['Type'] = prefix & "1";

Plans[2] = plan2;

plan3 = {};
plan3['Code'] = prefix & "SMSUNL20";
plan3['Price'] = prefix & "20.00";
plan3['Type'] = prefix & "3";

Plans[3] = plan3;

product1['Plans'] = Plans;
Products[1] = product1;

product2 = {};
product2['Type'] = prefix & "2";
product2['SKU'] = prefix & "660543353614";
product2['Price'] = prefix & "59.99";
product2['PriceType'] = prefix & "5";
product2['Qty'] = prefix & "2";

Products[2] = product2;

order = {};
order['OrderCarrier'] = OrderCarrier;
order['OrderRequestor'] = OrderRequestor;
order['ShippingAddress'] = ShippingAddress;
order['Products'] = Products;

jsOrderBase = SerializeJSON(order);

jsOrder = Replace(jsOrderBase,prefix,'','all');
WriteDump(jsOrder);

</cfscript>

<cfset strFilePath = ExpandPath( "./Foo.txt" ) />

<cfhttp attributecollection="#call#" >

     <cfhttpparam
        type="header"
        name="api-key"
        value="#apiKey#"
        />
    <cfhttpparam
        type="formfield"
        name="data"
        value="#jsOrder#"
        />
     
</cfhttp>

<cfdump var="#cfhttp#"> <br></br>
<cfoutput>#cfhttp.fileContent#</cfoutput>
