package gestionparc.ihm.fiches.importmasse
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	[Bindable]
	public class ImportConfirmation extends TitleWindow
	{
		public var nbIMEI:int;
		public var nbLignes:int;
		public var nbSIM:int;
		public var nbCollabo:int;
		public var xmlErreur:String;

		public var lbdebut:Label;
		public var lbfin:Label;
		public var taInfo:TextArea;

		public var btValider:Button;
		public var btAnnuler:Button;

		public var checkConfirmation:CheckBox;

		public function ImportConfirmation()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			// initilaisation des ecouteurs
			initListener();

			// initialisation de l'afficge par defaut (bad or good selon le retour)
			if (xmlErreur)
			{
				initBadDefaultDisplay();
			}
			else
			{
				initGoodDefaultDisplay();
			}
		}

		private function initGoodDefaultDisplay():void
		{
			lbdebut.text=ResourceManager.getInstance().getString('M111', 'Nous_avons_d_tect__des__l_ments_d_j__exi');
			taInfo.htmlText=displayGoodInfos();
			lbfin.text=ResourceManager.getInstance().getString('M111', 'Si_vous_confirmez_l_import__de_nouvelles');

			taInfo.setStyle("borderStyle", "none");
			checkConfirmation.selected=false;
			btValider.enabled=false;
		}

		private function initBadDefaultDisplay():void
		{
			lbdebut.text=ResourceManager.getInstance().getString('M111', 'Nous_avons_d_tect__des__l_ments_d_j__exi');
			taInfo.htmlText+=displayBadInfos();
			lbfin.text=ResourceManager.getInstance().getString('M111', 'Vous_devez_modifier_ces__l_ments_pour_po');

			checkConfirmation.visible=false;
			btValider.visible=false;
			btValider.includeInLayout=false;
			btAnnuler.label=ResourceManager.getInstance().getString('M111', 'OK');
		}

		private function displayBadInfos():String
		{
			var myXML:XML=new XML(xmlErreur);
			var myChildren:XMLList=myXML.children();
			var result:String=ResourceManager.getInstance().getString('M111', 'Les_couples_suivant_posent_probl_me___br');
			var index:int=0;
			var longueur:int=myChildren.length();

			for (index=0; index < longueur; index++)
			{
				result+="   •	  " + myChildren[index] + "<br>"
			}

			return result;
		}

		private function displayGoodInfos():String
		{
			var result:String="";

			if (nbIMEI > 0)
			{
				result+="   •	  " + nbIMEI + ResourceManager.getInstance().getString('M111', '_IMEI__l_ancien_mod_le_sera_conserv___br');
			}
			else
			{
				result+="   •	  " + nbIMEI + ResourceManager.getInstance().getString('M111', '_IMEI_br_');
			}
			if (nbSIM > 0)
			{
				result+="   •	  " + nbSIM + ResourceManager.getInstance().getString('M111', '_SIM__les_anciens_distributeur_et_op_rat');
			}
			else
			{
				result+="   •	  " + nbSIM + " SIM<br>";
			}
			if (nbLignes > 0)
			{
				result+="   •	  " + nbLignes + ResourceManager.getInstance().getString('M111', '_Lignes__les_anciens_distributeur_et_op_');
			}
			else
			{
				result+="   •	  " + nbLignes + ResourceManager.getInstance().getString('M111', '_Lignes_br_');
			}
			result+="   •	  " + nbCollabo + ResourceManager.getInstance().getString('M111', '_Collaborateurs__matricules_');

			return result;
		}

		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			checkConfirmation.addEventListener(Event.CHANGE, changeConfirmationCheck);
			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validerHandler);
		}

		private function changeConfirmationCheck(evt:Event):void
		{
			if (checkConfirmation.selected)
			{
				btValider.enabled=true;
			}
			else
			{
				btValider.enabled=false;
			}
		}

		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validerHandler(me:MouseEvent):void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_CONFIRMATION_IMPORT));
			closePopup(null);
		}
	}
}
