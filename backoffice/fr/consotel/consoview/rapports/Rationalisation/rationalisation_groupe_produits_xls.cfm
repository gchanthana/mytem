<cfheader name="Content-Disposition" value="inline;filename=rapport_rationalisation_#replace(RAISON_SOCIALE,' ','_','all')#.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="ISO-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
 xmlns:s="uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882"
 xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Version>11.6360</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10005</WindowHeight>
  <WindowWidth>10005</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>135</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
	    <ss:Styles>
	        <ss:Style ss:ID="1">
	            <ss:Font ss:Bold="1"/>
				<ss:Borders>
				    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
			   </ss:Borders>
	        </ss:Style>
	  <ss:Style ss:ID="s21">
	   <ss:Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	  </ss:Style>
	   
	  <ss:Style ss:ID="s30">
	   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Center" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Font ss:Bold="1"/>
	  </ss:Style>
		<ss:Style ss:ID="s16" ss:Name="Euro">
		   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s17" ss:Name="Total">
		   	<ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s18" ss:Name="Total1">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3" ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s19" ss:Name="Total2">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
	</ss:Styles>
	    <Worksheet ss:Name="Liste des Lignes par Site">
	      <ss:Table>
	            <ss:Column ss:Width="150"/>
	            <ss:Column ss:Width="200"/>
	            <ss:Column ss:Width="350"/>
	            <ss:Column ss:Width="120"/>
	            <ss:Column ss:Width="70"/>
	            <ss:Column ss:Width="90"/>
	            <ss:Column ss:Width="170"/>
	            <ss:Column ss:Width="120"/>
	            <ss:Column ss:Width="350"/>
				<ss:Column ss:Width="60"/>
	            <ss:Column ss:Width="60"/>
	            <ss:Column ss:Width="60"/>
				<ss:Column ss:Width="80"/>
	            <cfoutput>
				<ss:Row>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Société</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Site</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Adresse 1</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Adresse 2</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Code Postal</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Ligne</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Fonction</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Opérateur</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Nom du produit</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Nombre appels</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Durée (mn)</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Quantité</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Montant</ss:Data>
	                </ss:Cell>
	            </ss:Row>
				</cfoutput>
 	           	<cfoutput query="qGetData">
					<ss:Row>
			            <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#societe#</ss:Data>
		                </ss:Cell>
			            <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#site#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#adresse1#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#adresse2#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#zipcode#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#ligne#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#commentaires#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#nom#</ss:Data>
		                </ss:Cell>
		                 <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#produit#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="Number">#val(nombre_appel)#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="Number">#val(duree_appel)#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="Number">#val(qte)#</ss:Data>
		                </ss:Cell>
		                <ss:Cell ss:StyleID="s16">
		                    <ss:Data ss:Type="Number">#val(montant)#</ss:Data>
		                </ss:Cell>
		            </ss:Row>
				</cfoutput>
				<cfoutput>
				<ss:Row>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s18">
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s17">
	                    <ss:Data ss:Type="String">TOTAL</ss:Data>
	                </ss:Cell>
	                <ss:Cell ss:StyleID="s19" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
					<ss:Cell ss:StyleID="s19" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
					<ss:Cell ss:StyleID="s19" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
					<ss:Cell ss:StyleID="s17" ss:Formula="=SUM(R[-#Evaluate(qGetData.recordcount)#]C:R[-1]C)">
						<ss:Data ss:Type="Number"></ss:Data>
					</ss:Cell>
	                </cfoutput>
	            </ss:Row>
	        </ss:Table>
	    </Worksheet>
 	</Workbook>