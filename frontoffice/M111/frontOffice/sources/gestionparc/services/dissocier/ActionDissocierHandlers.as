package gestionparc.services.dissocier
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.rpc.events.ResultEvent;

	public class ActionDissocierHandlers
	{
		private var myDatas : ActionDissocierDatas;
		
		public function ActionDissocierHandlers(instanceOfDatas:ActionDissocierDatas)
		{
			myDatas = instanceOfDatas; 
		}
	/* ******************************** COLLABORATEUR ******************************** */
		/* REPRENDRE EQP */
		internal function reprendreEqpFromCollab1ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function reprendreEqpFromCollab2ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function reprendreEqpFromCollab3ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		/* LIGNE SIM */
		internal function reprendreLigneSimFromCollab1ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function reprendreLigneSimFromCollab2ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function reprendreLigneSimFromCollab3ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function reprendreLigneExtFromCollabResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DIASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		
		/* LIGNE */
		internal function reprendreLigneFromCollab1ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function reprendreLigneFromCollab2ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function reprendreLigneFromCollab3ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function reprendreLigneFromCollab4ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
	/* ******************************** EQUIPEMENT ******************************** */
		/* COLLABORATEUR */
		internal function dissocierCollabFromEqp1ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function dissocierCollabFromEqp2ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function dissocierCollabFromEqp3ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		internal function dissocierCollabFromEqp4ResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		/* LIGNE SIM */
		internal function dissocierLigneSimFromEqpResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/** extension */
		internal function dissocierLigneExtFromEqpResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DIASSOC_EQP_LIGNE_EXT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function dissocierExtFromEqpResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DIASSOC_EQP_LIGNE_EXT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/* LIGNE */
		internal function dissocierLigneFromEqpResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		/* SOUS EQP */
		internal function dissocierSousEqpFromEqpResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
	/* ******************************** LIGNE ******************************** */
		/* COLLABORATEUR */
		internal function dissocierCollabFromLigneResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		/* EQP */
		internal function dissocierEqpFromLigneResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		/* SIM */
		internal function dissocierSimFromLigneResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_LIGNE_EXT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function dissocierExtFromLigneResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/* COLLABORATEUR */
		internal function dissocierCollabFromSimResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		
		internal function dissocierCollabFromExtResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DIASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		
		/* EQP */
		internal function dissocierEqpFromSimResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function dissocierEqpFromExtResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DIASSOC_EQP_LIGNE_EXT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/* LIGNE */
		internal function dissocierLigneFromSimResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ASSOC_COLLAB_LIGNE_SIM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function dissocierLigneFromExtResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDissociationRealized();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DIASSOC_LIGNE_EXT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
	}
}