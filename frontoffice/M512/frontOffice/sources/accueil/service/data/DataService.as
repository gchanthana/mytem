package accueil.service.data
{
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	[Bindable]
	public class DataService
	{
		public var theModele:DataModel;
		public var theHandler:DataHandlers;
		
		private var _remo1:RemoteObject;
		private var _remo2:RemoteObject;
		
		
		public function DataService()
		{
			theModele = new DataModel();
			theHandler = new DataHandlers(theModele);
			
			_remo1 = new RemoteObject("ColdFusion");
			_remo1.source="fr.consotel.consoview.M512.AccueilService";
			_remo1.addEventListener(ResultEvent.RESULT,theHandler.getDateDataCompletedHandler,false,0,false);
			_remo1.addEventListener(FaultEvent.FAULT,theHandler.faultHandler,false,0,false);
			
			_remo2 = new RemoteObject("ColdFusion");
			_remo2.source="fr.consotel.consoview.M512.AccueilService";
			_remo2.addEventListener(ResultEvent.RESULT,theHandler.getIndicateurCleResultHandler,false,0,false);
			_remo2.addEventListener(FaultEvent.FAULT,theHandler.faultHandler,false,0,false);
		}
		
		public function reset():void
		{
		}
		
		public function getPFGPInfosDernierMoisComplet():void
		{	
			_remo1.getDateDataCompleted();
		}
		
		public function getIndicateurCle(idperiode:int = 0):void
		{	
			if(theModele.pfgpDateLastLoadComplete.IDPERIODE > 0)
			{
				_remo2.getIndicateurCle(theModele.pfgpDateLastLoadComplete.IDPERIODE );	
			}
		}
	}
}