package gestionparc.services.importmasse
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	
	import mx.rpc.AbstractOperation;

	public class ImportMasseServices
	{
		
		//--------------- VARIABLES ----------------//
		
		public var myDatas		:ImportMasseDatas;
		public var myHandlers	:ImportMasseHandlers;
		
		private var _pathBackoffice:String = "fr.consotel.consoview.M111.GestionImportMasse";
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function ImportMasseServices()
		{
			myDatas		= new ImportMasseDatas();
			myHandlers 	= new ImportMasseHandlers(myDatas);
		}
		
		/* */
		private function setSegment():int
		{
			var p_segment:int = -1;
			return p_segment;
		}
		
		/* */
		public function initComboRevendeur(idRacine:int, idLogin:int, chaine:String):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice, 
				"get_catalogue_revendeur", 
				myHandlers.initComboRevendeurResultHandler);
			RemoteObjectUtil.callService(opData, idRacine, chaine, idLogin, setSegment());
		}
		
		/* */
		public function initComboOperateur():void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice, 
				"fournirListeOperateursSegment", 
				myHandlers.initComboOperateurResultHandler);
			RemoteObjectUtil.callService(opData);
		}
		
		/* */
		public function initComboModele(selectedRevendeur:Object):void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice, 
				"fournirListeMobileCatalogueClient", 
				myHandlers.initComboModeleResultHandler);

			RemoteObjectUtil.callService(opData, selectedRevendeur.IDREVENDEUR, "", setSegment())
		}
		
		/* */
		public function getOperatorFPCU(selectedOperateur:Object):void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice, 
				"getInfosClientOperateurActuel", 
				myHandlers.getOperatorFPCUResultHandler);
			RemoteObjectUtil.callService(opData, selectedOperateur.OPERATEURID);
		}
		
		/* */
		public function saveMultipleEquip(revendeur:Object, modele:Object, operateur:Object, mydoc:String, dateGarantie:String, duree:Object, dateLigne:String, engagement:Object, is_pool_rev:int, segment:int=1):void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice, 
				"saveMultipleEquip_v4", 
				myHandlers.saveMultipleEquipHandlerResultEvent);

			var idrev:int = -1;
			var idmod:int = -1;
			var idope:int = -1;

			if (revendeur)
				idrev = revendeur.hasOwnProperty("IDREVENDEUR") ? Number(revendeur.IDREVENDEUR) : -1;
			if (modele)
				idmod = modele.hasOwnProperty("IDEQUIPEMENT_FOURNIS") ? Number(modele.IDEQUIPEMENT_FOURNIS) : -1;
			if (operateur)
				idope = operateur.hasOwnProperty("OPERATEURID") ? Number(operateur.OPERATEURID) : -1;

			RemoteObjectUtil.callService(opData, idrev, idmod, idope, mydoc, Number((SpecificationVO.getInstance().idPool == 0) ? SpecificationVO.getInstance().idPoolNotValid : SpecificationVO.getInstance().idPool), dateGarantie, Number(duree.value), dateLigne, Number((engagement != null) ? engagement.value : -1), is_pool_rev, segment);
		}
		
		/* */
		public function validerHandler(myXML:String):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice, 
				"ValidatesaveMultipleEquip", 
				myHandlers.ValidatesaveMultipleEquipHandlerResultEvent);

			RemoteObjectUtil.callService(opData, myXML, SpecificationVO.getInstance().idPoolNotValid, CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		
		/* */
//		public function readCSVandCSVToArray(pathFile:String):void
//		{
//			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
//				"fr.consotel.consoview.inventaire.equipement.utils.ParsingCSV", 
//				"ReadCSVandCSVToArray", 
//				myHandlers.readCSVandCSVToArrayResultHandler);
//			RemoteObjectUtil.callService(opData, pathFile);
//		}
		
		/* */
//		public function readXLSandXLSToArray(pathFile:String, nbColumn:int, nbSheet:int):void
//		{
//			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
//				"fr.consotel.consoview.inventaire.equipement.utils.ParsingXLS", 
//				"ReadExcel", 
//				myHandlers.readXLSandXLSToArrayResultHandler);
//			RemoteObjectUtil.callService(opData, pathFile, nbColumn, true, nbSheet);
//		}
		
		/* */
//		public function renameFile(nameFileRef:String, pathFile:String):void
//		{
//			var longueur:int=nameFileRef.length;
//			var newName:String=nameFileRef.substr(0, longueur - 4)
//			newName+="_" + UIDUtil.createUID();
//			newName+=nameFileRef.substr(longueur - 4, longueur);
//
//			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
//				"fr.consotel.consoview.inventaire.equipement.utils.RenameFileUploaded", 
//				"RenameFile", 
//				myHandlers.renameFileResultHandler);
//			RemoteObjectUtil.callService(opData, nameFileRef, newName, nameFileRef);
//		}		
		
		/* */
		public function checkCollab(nom:String,prenom:String,matricule:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice,
				"iMPCheckCollab",
				myHandlers.checkCollabHandler);
			
			RemoteObjectUtil.callSilentService(op,nom,prenom,matricule);
		}
		
		/* */
		public function checkIMEI(IMEI:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice,
				"iMPCheckIMEI",
				myHandlers.checkIMEIHandler);
			
			RemoteObjectUtil.callSilentService(op,IMEI);
		}
		
		/* */
		public function checkNSERIE(num_serie:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice,
				"iMPCheckNSERIE",
				myHandlers.checkNSERIEHandler);
			
			RemoteObjectUtil.callSilentService(op,num_serie);
		}
		
		/* */
		public function checkNEXTENSION(num_ext:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice,
				"iMPCheckNEXTENSION",
				myHandlers.checkNEXTENSIONHandler);
			
			RemoteObjectUtil.callSilentService(op,num_ext);
		}
		
		/* */
		public function checkNSIM(num_sim:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice,
				"iMPCheckNSIM",
				myHandlers.checkNSIMHandler);
			
			RemoteObjectUtil.callSilentService(op,num_sim);
		}
		
		/* */
		public function checkNLIGNE(num_ligne:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice,
				"iMPCheckNLIGNE",
				myHandlers.checkNLIGNEHandler);
			
			RemoteObjectUtil.callSilentService(op,num_ligne);
		}
		
		/* */
		public function importFichier(listEqp:Object,idpool:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_pathBackoffice,
				"iMPImporterFichier",
				myHandlers.importerFichierHandler);
			
			RemoteObjectUtil.callService(op,listEqp,idpool);
		}
	}
}
