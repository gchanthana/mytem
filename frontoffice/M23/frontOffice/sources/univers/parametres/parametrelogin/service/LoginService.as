package univers.parametres.parametrelogin.service
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.vo.AccesCompte;

	[Bindable]
	public class LoginService extends EventDispatcher
	{
		public var col_login : ArrayCollection = new ArrayCollection();
		public var col_acces : ArrayCollection = new ArrayCollection();
		public var col_adresseIP : ArrayCollection = new ArrayCollection();
		public var col_acces_not_formated : ArrayCollection = new ArrayCollection();
		public var objGroupeClient : Object = new Object();
		
		
		public function LoginService() 
		{
		}
		public function listeLogin(idGroupe : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.login.GestionClient",
							"getListeLogin",
							listeLogin_handler,listeLogin_fault_handler);
			
			RemoteObjectUtil.callService(opData,idGroupe);
		}
		public function getAllLogin(idGroupe : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.login.GestionClient",
							"getLogin",
							listeLogin_handler,listeLogin_fault_handler);
			
			RemoteObjectUtil.callService(opData,idGroupe);
		}
		public function createUser(valNom:String,valPrenom:String,valAdresse:String,valCodePostal:String,valVille:String,valTelephone:String,valDirection:String,valEmail:String,valPwd:String,valRestriction : int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																		"createUtilisateur",
																		createUser_handler,createUser_fault_handler);
					RemoteObjectUtil.callService(op,valNom,valPrenom,valAdresse,valCodePostal,valVille,valTelephone,valDirection,valEmail,valPwd,valRestriction);
					
		}
		public function updateUser(idLogin: int,valNom:String,valPrenom:String,valAdresse:String,valCodePostal:String,valVille:String,valTelephone:String,valDirection:String,valEmail:String,valPwd:String,valRestriction : int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																		"updateUtilisateur",
																		updateUser_handler,updateUser_fault_handler);
					RemoteObjectUtil.callService(op,idLogin,valNom,valPrenom,valAdresse,valCodePostal,valVille,valTelephone,valDirection,valEmail,valPwd,valRestriction);
					
		}
		public function listeIPOfLogin(idLogin:int):void
		{
			var opGetAdresseIP:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"getAdresseIp",
																	listeIPOfLogin_handler,listeIPOfLogin_fault_handler);
			RemoteObjectUtil.callService(opGetAdresseIP,idLogin);
		}
		public function removeAdresseIP(idLogin:int,adrIP : String, masque : String):void{
		
			var oPsupIp:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"deleteAdresseIp",
																	removeAdresseIPn_handler,removeAdresseIP_fault_handler);
																	
				
			RemoteObjectUtil.callService(oPsupIp,idLogin,adrIP,masque);
		}
		public function envoyerLoginParMail(nom:String,prenom : String,email : String):void{
			
			var op1:AbstractOperation =
							RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M23.GestionClient",
																		//"fr.consotel.consoview.parametres.login.GestionClient",
																		"send_Login",
																		envoyerLoginParMail_handler,envoyerLoginParMail_fault_handler);
			
			RemoteObjectUtil.callService(op1,[nom,prenom,email]);
		}
		public function envoyerMDPParMail(nom:String,prenom : String,email : String, passe : String):void
		{
				
				var op1:AbstractOperation =
							RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M23.GestionClient",
																		//"fr.consotel.consoview.parametres.login.GestionClient",
																		"send_Pass",
																		envoyerMDPParMail_handler,envoyerMDPParMail_fault_handler);
			RemoteObjectUtil.callService(op1,[nom,prenom,email,passe]);
			
		}
		public function deleteLogin(idLogin : int):void{
					
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.login.GestionClient",
																		"deleteLogin",
																		deleteLogin_handler,deleteLogin_fault_handler);
			RemoteObjectUtil.callService(op,[idLogin]);
			
		}
		public function getAccesOfLogin(idLogin : int, idGroupe : int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M23.GestionClient",
																		"getAccess",
																		getAccesOfLogin_handler,getAccesOfLogin_fault_handler);
			RemoteObjectUtil.callService(op1,idLogin,idGroupe);	
		}
		public function updateParametreAbonnement(module:String,value : int,idLogin : int,idAcces:int):void
		{
						var op1:AbstractOperation =
							RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"changeDroit",
																	updateParametreAbonnement_handler,updateParametreAbonnement_fault_handler);
																
			RemoteObjectUtil.callService(op1,[module,value,idLogin,idAcces]);
		}
		
		public function deleteAcces(idLogin : int, idAcces : int):void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"deleteAcces",
																	deleteAcces_handler,deleteAcces_fault_handler);
			RemoteObjectUtil.callService(op1,[idLogin,idAcces]);
		}
		public function getRacineInfos(idGroupe : int):void {
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.login.GestionClient",
																		"getGroupesMaitres",
																		getRacineInfos_handler,getRacineInfos_fault_handler);
			
			RemoteObjectUtil.callService(opData,idGroupe);
		}
		public function getRacineInfos_handler(evt : ResultEvent):void
		{
			var tempCol : ArrayCollection = evt.result as ArrayCollection ;
			objGroupeClient = tempCol.getItemAt(0);
			
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.INFO_ACCES_COMPLETE));
		}
		public function deleteAcces_handler(re : ResultEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.DELETE_ACCES_COMPLETE));
		}
		public function updateParametreAbonnement_handler(re : ResultEvent):void
		{
			
		}
		public function getAccesOfLogin_handler(re : ResultEvent):void
		{
			// pour l'arbre
			col_acces_not_formated = new ArrayCollection();
			col_acces = new ArrayCollection();
			col_acces_not_formated = re.result as ArrayCollection;
			
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
		
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : AccesCompte = new AccesCompte();
								
				item.libelle=tmpArr[i].LIBELLE ;
			
				item.idLogin=tmpArr[i].APP_LOGINID;
				item.idAcces=tmpArr[i].ID;
				item.idGroupeMaitre =tmpArr[i].idGroupeMaitre;
				
				item.module_usage=tmpArr[i].MODULE_USAGE;
				item.module_fixe_data =tmpArr[i].MODULE_FIXE_DATA ;
				item.module_gestion_login=tmpArr[i].MODULE_GESTION_LOGIN;
				item.module_mobile=tmpArr[i].MODULE_MOBILE;
				item.module_fournisseur=tmpArr[i].DROIT_GESTION_FOURNIS ;
				item.module_facturation=tmpArr[i].MODULE_FACTURATION;
				item.module_gestion_orga=tmpArr[i].MODULE_GESTION_ORG;
				item.module_workflow=tmpArr[i].MODULE_WORKFLOW ;
				item.DATE_CREATE = tmpArr[i].DATE_CREATE;
						
				col_acces.addItem(item);
			}
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LISTE_ACCES_COMPLETE));
		}
		private function deleteLogin_handler(re : ResultEvent):void
		{
			//if(re.result){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.DELETE_LOGIN));
			//}
		}

		private function envoyerLoginParMail_handler(re: ResultEvent):void
		{
			//if(re.result>1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SEND_MAIL_COMPLETE));
			//}
		}
		private function envoyerMDPParMail_handler(re: ResultEvent):void
		{
			//if(re.result>1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.SEND_PASS_COMPLETE));
			//}
		}
		private function removeAdresseIPn_handler (re : ResultEvent):void
		{
			if(re.result){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.DELETE_IP_COMPLETE));
			}
		}
		private function listeIPOfLogin_handler(re : ResultEvent):void
		{
			if(re.result)
			{
				col_adresseIP = re.result as ArrayCollection;
			}
		}
		private function createUser_handler(evt :ResultEvent):void
		{
			if(evt.result>1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LOGIN_INFO_CHANGE_COMPLETE,false,false,evt.result as int));
			}
		}
		private function updateUser_handler(evt :ResultEvent):void
		{
			//if(evt.result>1){
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LOGIN_INFO_CHANGE_COMPLETE,false,false,evt.result as int));
			//}
		}
	
		private function listeLogin_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_login = new ArrayCollection();
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Gestionnaire = new Gestionnaire();
								
				item.nom=tmpArr[i].LOGIN_NOM;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
				item.adresse=tmpArr[i].ADRESSE_POSTAL;
				item.cp=tmpArr[i].CODE_POSTAL;
				item.ville=tmpArr[i].VILLE_POSTAL;
				item.tel=tmpArr[i].TELEPHONE;
				item.direction=tmpArr[i].DIRECTION;
				item.mail=tmpArr[i].LOGIN_EMAIL;
				item.password=tmpArr[i].LOGIN_PWD;
				item.id	= tmpArr[i].APP_LOGINID;
				
				if(tmpArr[i].RESTRICTION_IP==1)
				{
					item.restrictIP=true;
				}
				else
				{
					item.restrictIP=false;
				}
						
				col_login.addItem(item);
			}
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.LISTE_LOGIN_COMPLETE));
		}
		private function getRacineInfos_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateParametreAbonnement_fault_handler'+evt.toString());
		}
		private function updateParametreAbonnement_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateParametreAbonnement_fault_handler'+evt.toString());
		}
		private function getAccesOfLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getAccesOfLogin_fault_handler'+evt.toString());
		}
		private function envoyerLoginParMail_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.envoyerLoginParMail_fault_handler'+evt.toString());
		}
		private function envoyerMDPParMail_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.envoyerMDPParMail_fault_handler'+evt.toString());
		}
		private function removeAdresseIP_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeLogin_fault_handler'+evt.toString());
		}
		private function listeIPOfLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeLogin_fault_handler'+evt.toString());
		}
		private function listeLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeLogin_fault_handler'+evt.toString());
		}
		private function createUser_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.createUser_fault_handler'+evt.toString());
		}
		private function deleteAcces_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.deleteAcces_fault_handler'+evt.toString());
		}
		private function updateUser_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.createUser_fault_handler'+evt.toString());
		}
		private function deleteLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.deleteLogin_fault_handler'+evt.toString());
		}
	}
}