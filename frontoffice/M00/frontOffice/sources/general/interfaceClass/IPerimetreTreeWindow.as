package general.interfaceClass {
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;

	public interface IPerimetreTreeWindow extends IEventDispatcher {
		/**
		 * Si true alors la racine est toujours le noeud sur lequel
		 * on est connecté dans l'application
		 * Si false alors la racine est toujours la racine de l'arbre des périmètres
		 * */
		function get connectedNodeIsRoot():Boolean;
		function set connectedNodeIsRoot(value:Boolean):void;
		
		/**
		 * Si true alors à la séléction d'un noeud l'arbre récupère les infos supplémentaires
		 * concernant le noeud en question. Lorsque ces infos sont à jour alors l'instance
		 * lance un évènement de type PerimetreTreeEvent.NODE_INFOS_RESULT.
		 * Les infos du noeud sont alors obtenues par appel à get nodeInfos()
		 * Si false alors la séléction ne fait rien et la get nodeInfos() renvoie null
		 * */
		function get loadDataOnSelection():Boolean;
		function set loadDataOnSelection(value:Boolean):void;
		
		/**
		 * Donne les infos supplémentatires concernant le noeud séléctionné
		 * Cette fonction renvoie toujours null si loadDataOnSelection = false
		 * */
		function get nodeInfos():INodeInfos;
		
		/**
		 * Si la racine est visible ou pas
		 * */
		function get showGroupeRacine():Boolean;
		function set showGroupeRacine(value:Boolean):void;
		
		/**
		 * Retourne le noeud séléctionné
		 * Quelque soit la valeur de loadDataOnSelection
		 * */
		function getSelectedItem():Object;
		
		function onPerimetreChange():void;
		
		function onChangePerimetre(evt:Event):void
	}
}
