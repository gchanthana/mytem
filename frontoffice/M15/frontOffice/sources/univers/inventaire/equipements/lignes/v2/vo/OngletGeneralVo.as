package univers.inventaire.equipements.lignes.v2.vo
{
	import mx.collections.ArrayCollection;
	
	[RemoteClass(alias="fr.consotel.consoview.inventaire.equipement.lignes.OngletGeneralVo")]

	[Bindable]
	public class OngletGeneralVo
	{

		public var IDTYPE_LIGNE:Number = 0;
		public var LIBELLE_TYPE_LIGNE:String = "";
		public var IDSOUS_TETE:Number = 0;
		public var SOUS_TETE:String = "";
		public var IDTETE_LIGNE:Number = 0;
		public var TETE_LIGNE:String = "";
		public var IDTYPE_RACCORDEMENT:Number = 0;
		public var LIBELLE_RACCORDEMENT:String = "DIRECT";
		public var NBR_LIGNE:Number = 0;
		public var IDENTIFIANT_COMPLEMENTAIRE:String = "";
		public var CODE_INTERNE:String = "";
		public var BOOL_PRINCIPAL:Number = 1;
		public var BOOL_BACKUP:Number = 0;
		public var COMMENTAIRES_GENERAL:String = "";
		public var USAGE:String = "";
		public var POPULATION:String = "";
		public var V1:String = "";
		public var V2:String = "";		
		public var ListeGroupementLignes : ArrayCollection;


		public function OngletGeneralVo()
		{
		}

	}
}