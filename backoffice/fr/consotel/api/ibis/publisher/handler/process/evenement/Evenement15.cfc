<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement15"
extends="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement"
hint="Implémentation IReporting pour Evènement. Fichier de Log : [CFUSION]/logs/EVENEMENT.log">
<!--- *********************************************************************************************************************** --->
	<cffunction access="private" name="getReportList" returntype="query" hint="Retourne la liste des rapports à exécuter et détermine les paramètres du rapport BIP">
		<cfset var startTick=getTickCount()><!--- Tracking de la durée --->
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.type_15_liste">
			<cfprocresult name="qEvenement">
		</cfstoredproc>
		<cflog text="Evenement.getReportList() : COMPLETED in #(getTickCount() - startTick)# ms" file="#getLog()#" type="information">
		<cfreturn qEvenement>
	</cffunction>
</cfcomponent>