package univers.inventaire.lignesetservices.views
{
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.containers.ViewStack;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	[Bindable]
	public class GestionTechniqueImpl extends BaseView
	{
		
		
		
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public var vsMain : ViewStack;
		public var cmpDetailTechniqueLigne : BaseView;
		public var cmpLigneSelector : LigneSeledctorView;
		
		
		
		
		
		
		
		
		public function GestionTechniqueImpl()
		{
			super();
		}
		
		
		override public function onPerimetreChange():void{
			vsMain.selectedChild = cmpLigneSelector;
			cmpLigneSelector.onPerimetreChange();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties(); 			
			cmpLigneSelector.addEventListener("LigneSelected",cmpLigneSelectorLigneSelectedHandler);
			
		}
		
		
				
		protected function cmpLigneSelectorLigneSelectedHandler(ev : Event):void{
			
			configOngletDetailTechnique(gestionTechnique.ligne.TYPE_FICHELIGNE);
			vsMain.selectedChild = cmpDetailTechniqueLigne;
		}
		
		
		
		
		protected function cmpDetailTechniqueLigneDeatilRetourClickedHandler(ev : Event):void{
			vsMain.selectedChild = cmpLigneSelector;
			cmpLigneSelector.dgLignes.selectedIndex = -1;
			 
		}
		
		protected function cmpDetailTechniqueLigneExportFicheClickedHandler(ev : Event):void{
			var url:String = moduleLignesEtServicesIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/inventaire/equipement/lignes/csv/exportFicheLigne.cfm";
			var request:URLRequest = new URLRequest(url);         
           	var variables:URLVariables = new URLVariables();
            variables.IDSOUS_TETE = gestionTechnique.ligne.IDSOUS_TETE;
            request.data = variables;
            request.method = URLRequestMethod.POST;
  			navigateToURL(request,"_blank");
		}
		
		private function configOngletDetailTechnique(typeLigne : String ):void{
			if(cmpDetailTechniqueLigne != null){
				try{
					removeChild(cmpDetailTechniqueLigne);
				}catch(e : Error){
					trace("removeChild(cmpDetailTechniqueLigne) [error]");
				}
				cmpDetailTechniqueLigne = null;
			}
			
			switch(typeLigne){
				case "AUT":{
					cmpDetailTechniqueLigne = new DetailTechniqueLigneViewLight();
					cmpDetailTechniqueLigne.gestionTechnique = gestionTechnique;
					
					vsMain.addChild(cmpDetailTechniqueLigne);
					callLater(laterConfigOngletDetailTechniqueLight,[typeLigne]);
				}break;				 
				case "MOB": {
					cmpDetailTechniqueLigne = new DetailTechniqueLigneMOBView();
					cmpDetailTechniqueLigne.gestionTechnique = gestionTechnique;
					
					vsMain.addChild(cmpDetailTechniqueLigne);
					callLater(laterConfigOngletDetailTechniqueMOB,[typeLigne]);
				}break;
				default:{
					cmpDetailTechniqueLigne = new DetailTechniqueLigneView();
					cmpDetailTechniqueLigne.gestionTechnique = gestionTechnique;
					vsMain.addChild(cmpDetailTechniqueLigne);
					callLater(laterConfigOngletDetailTechnique,[typeLigne]);
				}break;	
			}
			
			
		}
		
		private function laterConfigOngletDetailTechniqueMOB(typeLigne : String):void{
			cmpDetailTechniqueLigne.percentHeight = 100;
			cmpDetailTechniqueLigne.percentWidth = 100;
			cmpDetailTechniqueLigne.gestionTechnique = gestionTechnique;
			cmpDetailTechniqueLigne.modeEcriture = modeEcriture;
			cmpDetailTechniqueLigne.addEventListener("DeatilRetourClicked",cmpDetailTechniqueLigneDeatilRetourClickedHandler);
			cmpDetailTechniqueLigne.addEventListener("ExportFicheClicked",cmpDetailTechniqueLigneExportFicheClickedHandler);
			DetailTechniqueLigneMOBView(cmpDetailTechniqueLigne).vsOnglets.selectedIndex = 0;
			DetailTechniqueLigneMOBView(cmpDetailTechniqueLigne).cmpOngletAffectationView.configurerOnglet();
			BaseView(DetailTechniqueLigneMOBView(cmpDetailTechniqueLigne).vsOnglets.selectedChild).getData(typeLigne);
		}
		
		private function laterConfigOngletDetailTechniqueLight(typeLigne : String):void{
			cmpDetailTechniqueLigne.percentHeight = 100;
			cmpDetailTechniqueLigne.percentWidth = 100;
			cmpDetailTechniqueLigne.gestionTechnique = gestionTechnique;
			cmpDetailTechniqueLigne.modeEcriture = modeEcriture;
			cmpDetailTechniqueLigne.addEventListener("DeatilRetourClicked",cmpDetailTechniqueLigneDeatilRetourClickedHandler);
			cmpDetailTechniqueLigne.addEventListener("ExportFicheClicked",cmpDetailTechniqueLigneExportFicheClickedHandler);
			DetailTechniqueLigneViewLight(cmpDetailTechniqueLigne).vsOnglets.selectedIndex = 0;
			/* DetailTechniqueLigneViewLight(cmpDetailTechniqueLigne).cmpOngletAffectationView.configurerOnglet(); */
			BaseView(DetailTechniqueLigneViewLight(cmpDetailTechniqueLigne).vsOnglets.selectedChild).getData(typeLigne);
		}
		
		
		private function laterConfigOngletDetailTechnique(typeLigne : String):void{
			cmpDetailTechniqueLigne.percentHeight = 100;
			cmpDetailTechniqueLigne.percentWidth = 100;
			
			cmpDetailTechniqueLigne.modeEcriture = modeEcriture;
			
			
			cmpDetailTechniqueLigne.addEventListener("DeatilRetourClicked",cmpDetailTechniqueLigneDeatilRetourClickedHandler);
			cmpDetailTechniqueLigne.addEventListener("ExportFicheClicked",cmpDetailTechniqueLigneExportFicheClickedHandler);
			/* DetailTechniqueLigneView(cmpDetailTechniqueLigne).cmpOngletAffectationView.configurerOnglet(); */
			DetailTechniqueLigneView(cmpDetailTechniqueLigne).cmpOngletCablageView.configurerOnglet();
			DetailTechniqueLigneView(cmpDetailTechniqueLigne).vsOnglets.selectedIndex = 0;
			BaseView(DetailTechniqueLigneView(cmpDetailTechniqueLigne).vsOnglets.selectedChild).getData(typeLigne);	
		}
			
	}
}