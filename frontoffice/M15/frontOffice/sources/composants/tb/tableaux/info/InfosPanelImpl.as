package composants.tb.tableaux.info
{
	import composants.util.DateFunction;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.events.CloseEvent;
	
	
	
	
	
	
	
	
	[Event(name="infosWindowClosed")]
	
	
	
	
	
	
	
	
	[Bindable]
	public class InfosPanelImpl extends TitleWindow
	{
		public var btFermer : Button;
				
		private var _ligne : Object;
		public function get ligne():Object{
			return _ligne;
		}
		public function set ligne(idst : Object):void{
			_ligne = idst;
			if (_ligne.hasOwnProperty("IDSOUS_TETE")){
				_infos.getInfosLigne(_ligne.IDSOUS_TETE);			
			}else{
				throw new IllegalOperationError("L'objet passé en parametre n'a pas d'IDSOUS_TETE");
			}
		}
		
		
		private var _infos : InfosPanel = new InfosPanel();
		public function get infos():InfosPanel{
			return _infos;
		}
		public function set infos(inf : InfosPanel):void{
			_infos = inf;			
		}
		
		
		public function InfosPanelImpl()
		{
			super();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			addEventListener(CloseEvent.CLOSE,closeEventHandler);
			btFermer.addEventListener(MouseEvent.CLICK,btFermerClickHandler);
		}
		
		protected function formateDateAsString(value : Date):String{
			
			return value != null ? DateFunction.formatDateAsString(value) : ""; 
		}
		
		//=================== HANDLERS ========================================
		protected function closeEventHandler(ce :CloseEvent):void{
			dispatchEvent(new Event("infosWindowClosed"));
		}
		protected function btFermerClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event("infosWindowClosed"));
		}		
		//=================== FIN HANDLERS ====================================
		

		
	}
}