package intranet.GestionProduit.renderers
{
	import flash.events.Event;
	import flash.events.FocusEvent;
	
	import mx.containers.Canvas;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	
	public class UIInputCapIR extends Canvas
	{
		
		protected var _textInput:TextInput;
		protected var _boolCreationComplete:Boolean = false;
		protected var _text:String = "";
		protected var _editable:Boolean = false;
		protected var _enabled:Boolean = false;
		protected var _stylename:String = "";
		private var _dataChanged:Boolean = false;
		
		
		public function UIInputCapIR()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			
		}
		
		override protected function createChildren():void
		{
			super.createChildren()
				
			if(!_textInput)
			{
				_textInput = new TextInput();
				_textInput.width = 100;
				_textInput.restrict = '0-9';
				_textInput.enabled = false;
				addChild(_textInput);
			}
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			
			
		}
		
		public override function set data(value:Object):void
		{
			_text = "";
			_enabled = _editable = false;			 
			_stylename = "";
			
			if(value == null) return;
			
			super.data = value;
			
			if(data.BOOL_DRT==0)
			{
				//data.PLAN_MCAP=0;
			}
			else
			{				
				if(data.PLAN_MCAP==-1)
				{
					_text = "Illimité"; 
					_stylename = "tiNoBorder";
				}
				else
				{	
					_editable = _enabled = true;
					_text = parseInt((value.PLAN_MCAP / 1024).toFixed(0)).toString();
				}
			}
			_dataChanged = true;
			invalidateDisplayList();
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			
			_textInput.x = unscaledWidth - _textInput.width - 5;
			
			if(_dataChanged == false)
			{
				return 
			}
			
			if(_boolCreationComplete)
			{
				_textInput.text = _text;
				_textInput.enabled = _enabled;
				_textInput.editable = _editable;
				_textInput.styleName = _stylename;
			}
			_dataChanged = false;
			
		}
		
		private function creationCompleteHandler(ev:FlexEvent):void
		{	
			_boolCreationComplete = true;
			_textInput.addEventListener(Event.CHANGE,textInputChangeHandler);
			_textInput.addEventListener(FocusEvent.FOCUS_IN,textInputFocusInHandler);			
			horizontalScrollPolicy = "off";
			
		}	
		
		protected function textInputFocusInHandler(event:FocusEvent):void
		{			
			_textInput.setSelection(0,_text.length);
		}
		
		protected function textInputChangeHandler(event:Event):void
		{	
			data.PLAN_MCAP = (_textInput.text=="")?0:parseInt(_textInput.text)*1024;
			if(parseFloat(_textInput.text) <= 0)
			{
				_textInput.text.charAt(0);
				_textInput.text = "0";
			}
		}
	}
}