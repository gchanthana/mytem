package composants.searchpagindatagrid
{
import composants.pagindatagrid.pagination.vo.ItemIntervalleVO;

import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.NumericStepper;
	import mx.controls.TextInput;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;
	
	import composants.pagindatagrid.PaginateDatagrid;
	import composants.pagindatagrid.columns.SortableColumn;
	import composants.searchpagindatagrid.event.SearchPaginateDatagridEvent;
	import composants.util.preloader.Spinner;
	
	import gestioncommande.entity.Critere;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.ListeCommandesService;
	
	import session.SessionUserObject;
	
	[Bindable]
	[Event(name="onSearch", type="composants.searchpagindatagrid.event.SearchPaginateDatagridEvent")]
	public class SearchPaginateDatagridControleur extends HBox
	{
		private var _paginateDatagridInstance : PaginateDatagrid;
		private var _colonnesRecherchables : ArrayCollection;
		private var _typeRecherche : String = 'defaut'; //par défaut la recherche ne gère pas de période
		private var _parametresRechercheVO:ParametresRechercheVO = new ParametresRechercheVO();
		public var paddingLeftForm : Number = 90;
		public var widthFormItem : Number =200;
		
		//PUBLIC UI COMPONANT
		public var txtCle : TextInput;
		public var comboRecherche :ComboBox;
		public var comboTrieColonne:ComboBox;
		public var comboOrderBy:ComboBox;
		public var dfDateDeb : DateField;
		public var dfDateFin: DateField;
		public var dfPeriode: DateField;
		public var listeMesCommandes:ArrayCollection = new ArrayCollection([
																				{label:ResourceManager.getInstance().getString('M16','Mes_Commandes'), data:1},
																				{label:ResourceManager.getInstance().getString('M16','Toutes_les_commandes'), data:0} 
																			]);
		
		public var listeStatuts:ArrayCollection = new ArrayCollection([
																			{label:ResourceManager.getInstance().getString('M16','Toutes'), data:-1},
																			{label:ResourceManager.getInstance().getString('M16','En_cours'), data:1},
																			{label:ResourceManager.getInstance().getString('M16','Closes'), data:0}
																		]);
		
		public var isListeCommande:Boolean = false;
					
		public function SearchPaginateDatagridControleur() 
		{
			this.setCurrentState(_typeRecherche);
		}
		public function initData():void
		{
			txtCle.addEventListener(KeyboardEvent.KEY_DOWN,reportKeyDown);
		}
		private function reportKeyDown(event : KeyboardEvent):void
		{
			if((event as KeyboardEvent).keyCode == 13){
				newSearch();
			}
		}
		public function newSearch(evt : MouseEvent=null):void
        {
        	parametresRechercheVO = new ParametresRechercheVO();
        	
        	//Recherche du texte
        	parametresRechercheVO.setSearchTexte((comboRecherche.selectedItem as SortableColumn).columDBName,txtCle.text);
        	//Recherche du orderBY
        	parametresRechercheVO.setOrderBY((comboTrieColonne.selectedItem as SortableColumn).columDBName,comboOrderBy.selectedItem.value);
        	//Recherche sur les dates
        	if(typeRecherche == 'periode')
        	{
        		parametresRechercheVO.setDateDebut(dfDateDeb.selectedDate);
        		parametresRechercheVO.setDateFin(dfDateFin.selectedDate);
        	}
        	if(typeRecherche == 'mois')
        	{
        		parametresRechercheVO.setDateDebut(dfPeriode.selectedDate);
        		parametresRechercheVO.setDateFin(dfPeriode.selectedDate);
        	}
            var intervalle:ItemIntervalleVO = new ItemIntervalleVO();
            intervalle.indexDepart = 1;
            paginateDatagridInstance.currentIntervalle = intervalle;
            paginateDatagridInstance.refreshPaginateDatagrid(true);
			paginateDatagridInstance.refreshTempListeSelected();
			
            dispatchEvent(new SearchPaginateDatagridEvent(SearchPaginateDatagridEvent.SEARCH,_parametresRechercheVO,true));
        }

		public function imgResetFilterClickHandler(me:MouseEvent):void
		{
			SessionUserObject.singletonSession.CURRENTCRITERE = new Critere();
			
			dispatchEvent(new Event('CRITERE_REMOVED', false));
		}
		
       	public function get paginateDatagridInstance():PaginateDatagrid
		{
			return this._paginateDatagridInstance;
		}
		public function set paginateDatagridInstance(paginateDatagridInstance : PaginateDatagrid):void
		{
			this._paginateDatagridInstance = paginateDatagridInstance;
			initColonnesRecherchables();
		}
		public function get colonnesRecherchables():ArrayCollection
		{
			return this._colonnesRecherchables;
		}
		public function set colonnesRecherchables(colonnesRecherchables : ArrayCollection):void
		{
			this._colonnesRecherchables = colonnesRecherchables;
			
		}
		private function initColonnesRecherchables():void
		{
			colonnesRecherchables = new ArrayCollection();
			var arrColonne : Array = paginateDatagridInstance.columns;
			for(var i:int=0;i<arrColonne.length;i++)
			{
				if(paginateDatagridInstance.columns[i] is SortableColumn)
				{
					colonnesRecherchables.addItem(paginateDatagridInstance.columns[i]);
				}
			}
			//Initialisation du composant recherche :	
			parametresRechercheVO = new ParametresRechercheVO();
        	//Recherche du texte
        	parametresRechercheVO.setSearchTexte(colonnesRecherchables[0].columDBName,null);
        	//Recherche du orderBY
        	parametresRechercheVO.setOrderBY(colonnesRecherchables[0].columDBName,comboOrderBy.selectedItem.toString());
		}
		
		[Inspectable(type="String",enumeration="defaut,periode,mois",defaultValue="defaut")]
        public function set typeRecherche(value:String):void {
            if (value!=null)
            _typeRecherche=value;
            this.setCurrentState(_typeRecherche);
        }
      
        public function get typeRecherche():String {
            return _typeRecherche;
        }

		public function txtCleHandler():void
		{
			parametresRechercheVO.setSearchTexte((comboRecherche.selectedItem as SortableColumn).columDBName,txtCle.text);
		}
		public function comboRechercheHandler():void
		{
			if(comboRecherche.selectedIndex == -1)
			{
				comboRecherche.selectedIndex = 0;
				
			}
			else
			{
				comboTrieColonne.selectedIndex = comboRecherche.selectedIndex;
			}
		}
		public function comboTrieColonneHandler():void
		{
			if(comboTrieColonne.selectedIndex == -1)
			{
				comboTrieColonne.selectedIndex = 0;
			}
		}
		public function comboOrderByHandler():void
		{
			if(comboOrderBy.selectedIndex == -1)
			{
				comboOrderBy.selectedIndex = 0;
			}
		}
		public function dfDateDebHandler():void
		{
			
		}
		public function dfDateFinHandler():void
		{
			
		} 
		
		public function dfDatePeriodeHandler():void
		{
			
		}
		public function set parametresRechercheVO(value:ParametresRechercheVO):void
		{
			_parametresRechercheVO = value;
		}

		public function get parametresRechercheVO():ParametresRechercheVO
		{
			return _parametresRechercheVO;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					RAJOUT
		//--------------------------------------------------------------------------------------------//
		
		
		public var cbxStatut			:ComboBox;
		public var cbxEtat				:ComboBox;
		public var cbxGestionnaire		:ComboBox;
		public var cbxOperateur			:ComboBox;
		public var cbxCompte			:ComboBox;
		public var cbxSousCompte		:ComboBox;
		
		private var _listeCmdeSrv		:ListeCommandesService = new ListeCommandesService();
		
		public var listeActions			:ArrayCollection = new ArrayCollection();
		public var listeActionsProfil	:ArrayCollection = new ArrayCollection();
		
		public var isAutorised			:Boolean = false;
		
		public function razAndGetFiltres(idpool:int, idprofil:int):void
		{
			try
			{
				(cbxEtat.dataProvider as ArrayCollection).removeAll();
				(cbxGestionnaire.dataProvider as ArrayCollection).removeAll();
				(cbxOperateur.dataProvider as ArrayCollection).removeAll();
				(cbxCompte.dataProvider as ArrayCollection).removeAll();
				(cbxSousCompte.dataProvider as ArrayCollection).removeAll();
			}
			catch (error:Error) {}
			
			getFiltres(idpool, idprofil);
		}
		
		private function getFiltres(idpool:int, idprofil:int):void
		{
			_listeCmdeSrv.getFiltersCommandeAndActions(idpool, idprofil);
			_listeCmdeSrv.addEventListener(CommandeEvent.COMMANDE_FILTRES, filtresHandler);
		}
		
		public function setFilters(etats:ArrayCollection, gestionnaires:ArrayCollection, operateurs:ArrayCollection,
								   comptes:ArrayCollection, souscomptes:ArrayCollection, actions:ArrayCollection,
								   actionsProfil:ArrayCollection):void
		{
			(cbxEtat.dataProvider as ArrayCollection).removeAll();
			(cbxGestionnaire.dataProvider as ArrayCollection).removeAll();
			(cbxOperateur.dataProvider as ArrayCollection).removeAll();
			(cbxCompte.dataProvider as ArrayCollection).removeAll();
			(cbxSousCompte.dataProvider as ArrayCollection).removeAll();
			
			cbxEtat.dataProvider 			= etats;
			if(cbxEtat.dropdown)
				cbxEtat.dropdown.dataProvider	= etats;
			
			if(etats.length > 8)
				cbxEtat.rowCount = 8;
			else
				cbxEtat.rowCount = etats.length;
			
			cbxGestionnaire.dataProvider 			= gestionnaires;
			
			if(cbxGestionnaire.dropdown)
				cbxGestionnaire.dropdown.dataProvider	= gestionnaires;
			
			if(gestionnaires.length > 8)
				cbxGestionnaire.rowCount = 8;
			else
				cbxGestionnaire.rowCount = gestionnaires.length;
			
			cbxOperateur.dataProvider 			= operateurs;
			if(cbxOperateur.dropdown)
				cbxOperateur.dropdown.dataProvider	= operateurs;
			
			if(operateurs.length > 8)
				cbxOperateur.rowCount = 8;
			else
				cbxOperateur.rowCount = operateurs.length;
			
			cbxCompte.dataProvider 			= comptes;
			
			if(cbxCompte.dropdown)
				cbxCompte.dropdown.dataProvider	= comptes;
			
			if(comptes.length > 8)
				cbxCompte.rowCount = 8;
			else
				cbxCompte.rowCount = comptes.length;
			
			cbxSousCompte.dataProvider 			= souscomptes;
			if(cbxSousCompte.dropdown)
				cbxSousCompte.dropdown.dataProvider	= souscomptes;
			
			if(souscomptes.length > 8)
				cbxSousCompte.rowCount = 8;
			else
				cbxSousCompte.rowCount = souscomptes.length;
			
			listeActions		= actions;
			listeActionsProfil	= actionsProfil;
			
			dispatchEvent(new Event('SET_ACTIONS_WORKFLOW', true));
		}

		private function filtresHandler(cmde:CommandeEvent):void
		{
			cbxEtat.dataProvider 			= _listeCmdeSrv.listeEtats;
			cbxEtat.dropdown.dataProvider	= _listeCmdeSrv.listeEtats;
			
			if(_listeCmdeSrv.listeEtats.length > 8)
				cbxEtat.rowCount = 8;
			else
				cbxEtat.rowCount = _listeCmdeSrv.listeEtats.length;
			
			cbxGestionnaire.dataProvider 			= _listeCmdeSrv.listeGestionnaires;
			cbxGestionnaire.dropdown.dataProvider	= _listeCmdeSrv.listeGestionnaires;
			
			if(_listeCmdeSrv.listeGestionnaires.length > 8)
				cbxGestionnaire.rowCount = 8;
			else
				cbxGestionnaire.rowCount = _listeCmdeSrv.listeGestionnaires.length;
			
			cbxOperateur.dataProvider 			= _listeCmdeSrv.listeOperateurs;
			cbxOperateur.dropdown.dataProvider	= _listeCmdeSrv.listeOperateurs;
			
			if(_listeCmdeSrv.listeOperateurs.length > 8)
				cbxOperateur.rowCount = 8;
			else
				cbxOperateur.rowCount = _listeCmdeSrv.listeOperateurs.length;
			
			cbxCompte.dataProvider 			= _listeCmdeSrv.listeComptes;
			cbxCompte.dropdown.dataProvider	= _listeCmdeSrv.listeComptes;
			
			if(_listeCmdeSrv.listeComptes.length > 8)
				cbxCompte.rowCount = 8;
			else
				cbxCompte.rowCount = _listeCmdeSrv.listeComptes.length;
			
			cbxSousCompte.dataProvider 			= _listeCmdeSrv.listeSousComptes;
			cbxSousCompte.dropdown.dataProvider	= _listeCmdeSrv.listeSousComptes;
			
			if(_listeCmdeSrv.listeSousComptes.length > 8)
				cbxSousCompte.rowCount = 8;
			else
				cbxSousCompte.rowCount = _listeCmdeSrv.listeSousComptes.length;
			
			listeActions		= _listeCmdeSrv.listeActions
			listeActionsProfil	= _listeCmdeSrv.listeActionsProfil;
			
			
			dispatchEvent(new Event('SET_ACTIONS_WORKFLOW', true));
		}
		
		protected function cbxChangeHandler(le:ListEvent):void
		{
			if(le.currentTarget as ComboBox)
			{
				if((le.currentTarget as ComboBox).selectedItem == null)
					(le.currentTarget as ComboBox).selectedIndex = 0;
			}
		}
		
		public function lblCritereClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('MORE_CRITERES', false));
		}
		
		public function cbxRefreshDataProvider(me:MouseEvent):void
		{
			if(me.currentTarget as ComboBox)
				(me.currentTarget as ComboBox).validateProperties();
		}
		
		public var isSpinnerVisible		:Boolean = false;
		
		public var spin					:Spinner;
		
		public var imgBackground		:Image;
		
		[Embed(source="/assets/background/green_background_70x54.png",mimeType='image/png')]
		public var imgSearch :Class;
		
		[Embed(source="/assets/background/blue_background_70x54.png",mimeType='image/png')]
		public var imgFinded :Class;
		
		public function setSpinnerEtat(isVisible:Boolean):void
		{
			isSpinnerVisible = isVisible;
			
			if(isVisible)
			{
				spin.play();
				imgBackground.source = imgSearch;
			}
			else
			{
				spin.stop();
				imgBackground.source = imgFinded;
			}
		}
		
	}
}