package vo.widgets
{

	public class WidgetDataVo
	{

		private var _mois:String="";
		private var _montant:Number;
		private var _idPeriode:Number;
		private var _operateur:String="";
		
		private var _mnt_mobile :Number=0;
		private var _mnt_data  :Number=0;
		private var _mnt_fixe  :Number=0;
		
		private var _volume:int; 
		private var _code:String; 
		private var _trigramme:String;
		private var _total :Number=0;
		private var _montantRoaming:Number;

		public function WidgetDataVo()
		{
		}

		public function fill(widgetData:Object):void
		{
			this._mois=widgetData.MOIS;
			this._montant=widgetData.MNT;
			this._idPeriode=widgetData.IDP;

			this._operateur=widgetData.OPERATEUR;
			
			this._volume=widgetData.VOLUME;
			this._code=widgetData.PAYSAPL;
			this._trigramme=widgetData.TRIGRAMME;
			this._montantRoaming=widgetData.MONTANT;
			
			this._mnt_data=widgetData.MNT_DATA;
			this._mnt_fixe=widgetData.MNT_FIXE;
			this._mnt_mobile=widgetData.MNT_MOBILE;
			
			this._total=this._mnt_mobile+this._mnt_fixe+this.mnt_data;
		}

		public function get idPeriode():Number
		{
			return _idPeriode;
		}

		public function set idPeriode(value:Number):void
		{
			_idPeriode=value;
		}

		public function get montant():Number
		{
			return _montant;
		}

		public function set montant(value:Number):void
		{
			_montant=value;
		}

		public function get mois():String
		{
			return _mois;
		}

		public function set mois(value:String):void
		{
			_mois=value;
		}

		public function get operateur():String
		{
			return _operateur;
		}

		public function set operateur(value:String):void
		{
			_operateur=value;
		}
		public function get trigramme():String
		{
			return _trigramme;
		}
		
		public function set trigramme(value:String):void
		{
			_trigramme = value;
		}
		
		public function get code():String
		{
			return _code;
		}
		
		public function set code(value:String):void
		{
			_code = value;
		}
		
		public function get volume():int
		{
			return _volume;
		}
		
		public function set volume(value:int):void
		{
			_volume = value;
		}
		
		public function get mnt_fixe():Number
		{
			return _mnt_fixe;
		}
		
		public function set mnt_fixe(value:Number):void
		{
			_mnt_fixe = value;
		}
		
		public function get mnt_data():Number
		{
			return _mnt_data;
		}
		
		public function set mnt_data(value:Number):void
		{
			_mnt_data = value;
		}
		
		public function get mnt_mobile():Number
		{
			return _mnt_mobile;
		}
		
		public function set mnt_mobile(value:Number):void
		{
			_mnt_mobile = value;
		}
		
		public function get total():Number
		{
			return _total;
		}
		
		public function set total(value:Number):void
		{
			_total = value;
		}
		public function get montantRoaming():Number
		{
			return _montantRoaming;
		}
		
		public function set montantRoaming(value:Number):void
		{
			_montantRoaming = value;
		}
	}
}
