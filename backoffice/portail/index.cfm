<cftry>	
	<!--- NE PAS FLUSHER AVANT LE DUMP ci dessus ! --->
	<!--- ENVOI LES DONNÉES DISPONIBLE AU CLIENT AU FUR ET À MESURE --->
	<cfflush interval="100">
	
	
	<!--- REDIRECTION EVENTUELLE SI SESSION A EXPIRÉ --->
	<cfset createObject("component","portail.class.classData.SessionService").redirectIfSessionInvalid(URL,SESSION) >
	
	
	<!--- SI CLIC SUR BOUTON DE DECONNEXION GESTION DANS METHODE --->
	<cfset logOut = createObject("component","portail.class.classData.SessionService").logOutSession(URL) >
	
	
	<!--- si BTN DECONNEXION n'a pas été cliqué --->
	<cfif logOut eq false>
		<!--- on verifie si LE LIEN DE L'EMAIL(premiere connexion) a été cliqué --->
		<!--- METHODE QUI RECUPERE LE NUMERO DE LIGNE DANS LA BDD SELON UUID SPECIFIE DANS LE LIEN --->
		<!--- retourne une structure vide ou non vide dans laquelle se trouve le numero de ligne --->
		<cfset URL = createObject("component","portail.class.classData.AccessManagerService").getLineFromUuid(#CGI.QUERY_STRING#) >
	</cfif>
	
	
	<!--- DETECTION BROWSER + DEVICE --->
	<cfif NOT isdefined("SESSION.browser") || NOT isdefined("SESSION.device") >
		<cfset SESSION.browser = createObject("component","portail.class.classMeta.DetectBrowser").detectBrowser() >
		<cfset SESSION.device  = createObject("component","portail.class.classMeta.DetectDevice").detectDevice() >
	</cfif>
	
	
	<!--- INITIALISATION OU RECUPERATION DU CONTEXTE (REF) PASSÉ DANS L'URL --->
	<cfset context = structNew() >
	<cfif isDefined("URL") >
		<cfset context = #URL# >
	</cfif>

	
	<!--- STRATEGY / INSTANCIATION DE LA CLASSE DU TEMPLATE UTILISÉ --->
	<cfset template = createObject("component","portail.#SESSION.device#.Template#SESSION.device#") >
	
	
	<!--- BALISE HEAD ET CSS --->
	<cfset SESSION.css = template.getCss("#SESSION.device#") > <!--- dans cette methode, verif si SESSION.USER.CLIENTID existe mais pas dans arborescence --->
	<cfset head = template.getHead("#SESSION.css#","#SESSION.device#") >
	
	
	<!--- STRATEGY (suite) / CRÉATION DE LA PAGE APPELLÉ ---> <!--- le contexte = ref --->
	<cfset page = template.getPage(#context#)>
	<cfset page.afficher("#head#","#context#")>
	
	
	<!--- SI ERREUR --->
	<cfcatch type="any">
	
		<!--- ENVOI DE MAIL D'ERREUR --->
		<cfmail from="ReportError@portail.fr" to="joan.cirencien@saaswedo.com" subject="ERREUR AFFICHAGE PAGE" type="text/html" server="mail.consotel.fr" port="26" charset="utf-8" > 
			<cfdump var="#SESSION#" label="SESSION ERROR (CFCATCH)">
			<cfdump var="#CFCATCH#">
		</cfmail>
		
	</cfcatch>
</cftry>