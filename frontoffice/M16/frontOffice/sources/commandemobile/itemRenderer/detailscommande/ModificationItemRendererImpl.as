package commandemobile.itemRenderer.detailscommande
{
	
	import flash.events.Event;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.RessourcesElements;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.resources.ResourceManager;
	
	
	[Bindable]
	public class ModificationItemRendererImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listeOptions			:ArrayCollection = new ArrayCollection();
		
		public var nombreLignes			:int = 0;
		public var nombreOptions		:int = 0;
		public var optResiliees			:int = 0;
		public var optAjoutees			:int = 0;
		
		private var _myArticles			:XML = new XML();
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function ModificationItemRendererImpl()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function setValueConfiguration(articles:XML, myCommande:Commande):void
		{
			_myArticles = articles;
			formatToConfigurationSelected();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//	
	
		private function formatToConfigurationSelected():void
		{
			var listObject	:ArrayCollection = new ArrayCollection();
			var xmlarticles	:XMLList = _myArticles.children();
			var bool		:Boolean = false;
			var len			:int = xmlarticles.length();
			var id			:int = 0;
			var i			:int = 0;
			
			for(i = 0;i < len;i++)
			{
				var articles	:XML = xmlarticles[i];
				var myElements:ElementsCommande2 = new ElementsCommande2();
				
				getRessources(articles.ressources[0], myElements);
				getConfiguration(articles, myElements);					
				
				listObject.addItem(myElements);	
			}
			
			len = listObject.length;
			
			for(i = 0;i < len;i++)
			{
				getItems(listObject[i] as ElementsCommande2);
			}
			
			nombreLignes  = len;
			nombreOptions = listeOptions.length;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - FORMAT XML
		//--------------------------------------------------------------------------------------------//

		private function getRessources(articlesRe:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesRe[0].ressource.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML:XML = articlesRe[0].ressource[i];
				
				var reElts:RessourcesElements	= new RessourcesElements();	
					reElts.LIBELLETHEME 		= String(myXML[0].theme);
					reElts.LIBELLE 				= String(myXML[0].libelle);
					reElts.IDCATALOGUE 			= int(myXML[0].idproduit_catalogue);
					reElts.IDPRODUIT 			= int(myXML[0].idThemeProduit);
					reElts.BOOLACCES 			= int(myXML[0].bool_acces);
					reElts.ACTION				= String(myXML[0].action);
				
				//---> 'BOOLACCES' = 0 -> OPTION, 'BOOLACCES' = 1 -> ABONNEMENT
				if(reElts.BOOLACCES > 0)
					valuesElements.ABONNEMENTS.addItem(reElts);
				else
					valuesElements.OPTIONS.addItem(reElts);
			}
		}

		private function getConfiguration(configuration:XML, valuesElements:ElementsCommande2):void
		{
			var conElts:ConfigurationElements 		= new ConfigurationElements();
				conElts.LIBELLE 					= String(configuration.code_interne);
				conElts.PORTABILITE.NUMERO 			= String(configuration.soustete[0]);
				conElts.COLLABORATEUR 				= String(configuration.nomemploye[0]);
			
			valuesElements.CONFIGURATIONS.addItem(conElts);
		}
		
		private function getItems(elts:ElementsCommande2):void
		{
			var obj		:Object = new Object();
			var len		:int = elts.OPTIONS.length;
			var ligne	:String = '';
			
			if(elts.CONFIGURATIONS.length > 0)
				ligne = (elts.CONFIGURATIONS[0] as ConfigurationElements).PORTABILITE.NUMERO;
			
			for(var i:int = 0;i < len;i++)
			{
				obj 		 = new Object();
				obj.LIGNE	 = ligne;
				obj.LIBELLE	 = (elts.OPTIONS[i] as RessourcesElements).LIBELLE;
				
				var type:String = '';
				
				switch((elts.OPTIONS[i] as RessourcesElements).ACTION)//---> R:RESILIER, A:AJOUTER, S:SUSPENDRE, V:REACTIVATION 
				{
					case 'R': type = ResourceManager.getInstance().getString('M16', 'R_siliation'); optResiliees ++; break;
					case 'A': type = ResourceManager.getInstance().getString('M16', 'Ajout'); 		optAjoutees++; 	 break;
				}

				obj.TYPE = type;
				
				listeOptions.addItem(obj);
			}
		}

	}
}