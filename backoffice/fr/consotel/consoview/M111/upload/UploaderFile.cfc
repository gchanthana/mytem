<cfcomponent output="false" displayname="fr.consotel.consoview.M111.upload.UploaderFile" hint="Gestionnaire d'upload de fichiers">
	
	<!--- 
		uploader un fichier
	 --->
	<cffunction name="uploadFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="currentFile" 	type="fr.consotel.consoview.M111.vo.FileUploadVo" required="true"/>
		<cfargument name="idTarget"	 	type="Numeric" required="false"/>						  
		
		<cfset var webPath 	= GetDirectoryFromPath( GetCurrentTemplatePath() )>
		<cfset var pathDirTMP 	= webPath & currentFile.fileUUID>
<!---cfmail from="soufiane.ghenimi@saaswedo.com" subject="Test Email" to="soufiane.ghenimi@saaswedo.com" type="html" >
		webPath : <cfdump var="#webPath#" ><br/>
		pathDirTMP : <cfdump var="#webPath#" ><br/>
</cfmail--->
		<cfset status = 1>
		<cfif !DirectoryExists('#pathDirTMP#')>
			<cfdirectory action="create" directory="#pathDirTMP#" mode="777">
		</cfif>
		
		<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M111.upload.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
		<cfset var pathFileTmpRenamed= pathDirTMP & "/" & currentFile.fileName>
		
		<cfset currentFile.localDirPath = pathDirTMP>
		<cfset currentFile.localFilePath = pathFileTmpRenamed>
		
		<cffile action="write" output="#currentFile.fileData#" file="#pathFileTmpRenamed#" mode="777">
		<cfset status = 1>
		<cfreturn status>
	</cffunction>
	
	<!--- 
		uploader plusieurs fichiers
	 --->
	<cffunction name="uploadFiles" access="remote" output="false" returntype="Array" hint="Upload plusieurs fichiers">
		<cfargument name="filesSelected" type="Array" required="true"/>
		<cfargument name="idTarget"	 type="Numeric" required="false"/>
				
		<cfset var resultUpload	= ArrayNew(1)>
		<cfset var lenFiles 	= arrayLen(filesSelected)>
		<cfset var statusReturn	= 0>
		
		<cfloop index="idx" from="1" to="#lenFiles#">
			
			 <cfset statusReturn = uploadFile(filesSelected[idx],idTarget)>

			<cfset ArrayAppend(resultUpload, statusReturn)> 
		
		</cfloop>
			
		<cfreturn resultUpload>
	</cffunction>
	
	<!--- 
	 	Ajouter fichier dans ftp
	--->
	<cffunction name="handleFTP" access="private" output="false" returntype="numeric">
		<cfargument name="pathDirFileTmpRenamed" 	type="string" required="true">
		<cfargument name="currentFile" type="struct" required="true">
		
		<cfset var nameFileFtp		= currentFile.fileUUID>
		<cftry>	
		
			<!--- OUVRIR LA CONNEXION --->
			<cfftp connection="ftp" username="container" password="container" server="Pelican.consotel.fr" action="open" stopOnError="Yes">
			<cf
			<!--- UPLOAD DU FICHIER --->
			<cfftp action="putfile" connection="ftp" localFile="#pathDirFileTmpRenamed#" remoteFile="#nameFileFtp#" timeout="300">
			
			<!--- VERIF EXISTENCE FICHIER --->
			<cfftp action="existsfile" connection="ftp" remoteFile="#nameFileFtp#"><!--- directory="#dirFtp#" --->
			<cfif cfftp.returnValue eq "yes">
				 <cfset var retour = 1>
			<cfelse>
				<cfset var retour = 0>
			</cfif>
			
			<!--- FERMER LA CONNEXION --->
			<cfftp connection="ftp" action="close" stopOnError="Yes">
			<cfreturn retour>
		<cfcatch>
			<cflog type="error" text="#CFCATCH.Message#">													
			<cfmail server="mail.consotel.fr" port="26" from="container@saaswedo.com" to="container@saaswedo.com.com" 
					failto="monitoring@saaswedo.com" subject="[M61]Erreur lors de l'upload d'un fichier" type="html">
					#CFCATCH.Message#<br/><br/><br/><br/>
			</cfmail>
		</cfcatch>					
		</cftry>
		
	</cffunction>
	
	
	<!--- 
	 	supprimer le dossier TMP et le fichier tmp
	--->
	<cffunction name="deleteDirectory" access="remote" output="false" returntype="void">
		<cfargument name="pathDirTMP" type="string" required="true">
		
		<cfif DirectoryExists('#pathDirTMP#')>
			<cfdirectory action="delete" directory="#pathDirTMP#" type="dir" mode="777" recurse="true">
		</cfif>
		
	</cffunction>
	
	<!--- 
	 	créer dossier TMP + fichier TMP
	--->
	<cffunction name="createDirTMP" access="private" output="false" returntype="numeric">
		
		<cfargument name="pathDirTMP" type="string" required="true">
		<cfargument name="currentFile" type="struct" required="true">
		
		<cftry>
			
			<cfif NOT DirectoryExists('#pathDirTMP#')>
				<cfdirectory action="Create" directory="#pathDirTMP#" type="dir" mode="777">
			</cfif>
			
			<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M111.upload.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
			
			<cfset pathFileTMP = pathDirTMP & "/" & currentFile.fileName>
			<cffile action="write" output="#currentFile.fileData#" file="#pathFileTMP#" mode="777">
			<cfset statusReturn = 1>
			
			<cfreturn statusReturn>
			
		<cfcatch>
			<!--- log --->
			<cflog type="error" text="#CFCATCH.Message#">
			<!--- mail d'erreur --->
			<cfmail server="mail-cv.consotel.fr" port="25" from="container@saaswedo.com" to="monitoring@saaswedo.com" 
					failto="monitoring@saaswedo.com" subject="[M61] Erreur lors de l'upload de fichiers" type="html">
					
					#CFCATCH.Message#<br/><br/><br/><br/>
					
			</cfmail>
			
		</cfcatch>					
		</cftry>
		
	</cffunction>
	
</cfcomponent>