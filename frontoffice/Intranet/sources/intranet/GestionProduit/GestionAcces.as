package intranet.GestionProduit
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import intranet.GestionProduit.event.ChangeData_EVENT;

	public class GestionAcces extends VBox
	{
		public var CBtri:CBTriOpeIHM;
		public var adg:AdvancedDataGrid;
		public var btnu:Button;
		public var txtFiltre:TextInput;	
		public var btnRefresh:Button;
		public var cboAcces:ComboBox;
		public var cbDebugMode:CheckBox;
		public var adgcDebug:AdvancedDataGridColumn;
		
		
		private var tmpListe:Array=new Array();
		
		public function GestionAcces()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}

		public function init(e:FlexEvent):void{
			if(DataSample.TabOperateur != null && DataSample.TabOperateur.length>0)
				DataSample.TabOperateur.removeItemAt(0);
			
			CBtri.addEventListener(ListEvent.CHANGE,tri);
			btnu.addEventListener(MouseEvent.CLICK,btnClick);
			btnRefresh.addEventListener(MouseEvent.CLICK,refresh);
			adg.addEventListener(ChangeData_EVENT.ChangeData_EVENT,modifData);
			txtFiltre.addEventListener(Event.CHANGE,filtrer);
			txtFiltre.addEventListener(MouseEvent.ROLL_OVER,filtreReset);
			cboAcces.addEventListener(ListEvent.CHANGE,filtreAcces);
		}
		
		protected function cbDebugModeClickHandler():void
		{
			if(cbDebugMode.selected)
			{
				 
				adgcDebug.visible = true;				
			}
			else
			{
				 
				adgcDebug.visible = false;
			}
			 
			 
		}

// GESTION DU FILTRE ACCES
		public function filtreAcces(e:ListEvent):void{
			var acces:int=0;
			var opeid:int=0;
			
			if(CBtri.selectedIndex > -1)
				opeid=CBtri.selectedItem.OPERATEURID;
			
			switch(cboAcces.selectedIndex){
					case 1:  // remise
						acces=3;
						break;
						
					case 2: // prod. acces
						acces=2;
						break;
						
					case 3: // aucun
						acces=1
						break;	
						
					default:
						break;	
			}
 
 // Appel du backoffice
	 		if(opeid==0)
	 			Alert.show("Veuillez choisir un opérateur","Erreur",Alert.OK,this);
	 		else{ 		
	  		var ope:AbstractOperation=RemoteObjectUtil.getOperationFrom(
	 					RemoteObjectUtil.DEFAULT_DESTINATION,
	 					"fr.consotel.consoprod.gestionproduits.GestionProduits",
	 					"getAccesProduit",
	 					getResult, getFault);
	 		RemoteObjectUtil.callService(ope,opeid,acces); 
			}
		} 


// DATATIP DU PRODUIT
		public function ProdDataTip(item:Object):String{
			var s:String="";
			
			if(item.hasOwnProperty("PRODUIT")){
				 s=item.CATEGORIE+"\n -> "+item.PRODUIT;
			}
			return s;
		}	
		
// REFRESH
		public function refresh(e:MouseEvent):void{
			if(CBtri.selectedIndex != -1){
				var tmpOperation:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoprod.gestionproduits.GestionProduits",
							"getAccesProduit",
							getResult,getFault);
				RemoteObjectUtil.callService(tmpOperation,CBtri.selectedItem.OPERATEURID);	
			}else{
				var tmpOpe:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoprod.gestionproduits.GestionProduits",
							"getAccesProduit",
							getResult,getFault);
				RemoteObjectUtil.callService(tmpOpe,0);
			}
			txtFiltre.text="";
			DataSample.TabGestionProduit.refresh();
		}

// FILTRE
		public function filtrer(e:Event):void{
			DataSample.TabGestionAcces.filterFunction= filtreProduit;
			DataSample.TabGestionAcces.refresh();
		}
		
		public function filtreProduit(item:Object):Boolean{
			if(String(item.PRODUIT).match(new RegExp(txtFiltre.text,'i'))) return true;
			else return false;
		}
		
		public function filtreReset(e:MouseEvent):void{
			txtFiltre.text="";
		}


// GESTION DE LA MODIFICATION DES DONNEES
		public function modifData(e:ChangeData_EVENT):void{
			var existe:Boolean=false;
				
			if(tmpListe == null)
				tmpListe=new Array();

			if(tmpListe.length>0){
				for(var i:int=0;i<tmpListe.length;i++){
					if(tmpListe[i].IDPRODUIT_CATALOGUE == e.item.IDPRODUIT_CATALOGUE){
						tmpListe[i].BOOL_ACCES= e.item.BOOL_ACCES;
						tmpListe[i].TOUT_ACCES= e.item.TOUT_ACCES;
						existe=true;
						break;
					}
				}
				
				if(!existe)
					tmpListe.push(e.item);	
			}else{
				tmpListe.push(e.item);
			}
		}

// GESTION DE L'EVENT CLICK SUR LE BOUTON - Met à jour
		public function btnClick(e:MouseEvent):void{
			
			if(tmpListe.length>0){
				var tmpOpe:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"updAcces",
						updOk,updNo);
				RemoteObjectUtil.callService(tmpOpe,tmpListe);
			}
			txtFiltre.text="";		 		
		}
		
		public function updOk(e:ResultEvent):void{
			this.tmpListe=new Array();
			DataSample.TabGestionAcces.refresh();
		}
		
		public function updNo(e:FaultEvent):void{
			trace("GestionAcces - btnClick :"+e.message);
		}

// GESTION DE L'EVENT TRI_EVENT EMIS PAR LE COMPOSANT CBTriOpeIHM		
		public function tri(e:ListEvent):void{
			var acces:int=0;
			switch(cboAcces.selectedIndex){
					case 1:	acces=3;
						break;
					case 2: acces=2;
						break;
					case 3: acces=1
						break;	
					default:break;	
			}
				var tmpOperation:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"getAccesProduit",
						getResult,getFault);
				RemoteObjectUtil.callService(tmpOperation,CBtri.selectedItem.OPERATEURID,acces);
				txtFiltre.text="";	
		}
		
		public function getResult(e:ResultEvent):void{
			DataSample.TabGestionAcces=e.result as ArrayCollection;
			DataSample.TabGestionAcces.refresh();
			tmpListe=new Array();
		}
		
		public function getFault(e:FaultEvent):void{
			trace("GestionAcces - tri() :"+e.message);
		}
	}
}