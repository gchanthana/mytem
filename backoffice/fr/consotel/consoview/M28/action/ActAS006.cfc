<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Desactiver un contrat" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient p_idFc_contrat, p_Fc_Partenaireid, p_date_deb_contrat et p_date_fin_contrat">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.DContrat">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idFc_contrat#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_Fc_Partenaireid#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_deb_contrat, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_fin_contrat, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
			
			<cfif p_retour EQ 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AS006", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AS006", 0, err)/>
			<cfelseif p_retour EQ -10>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de chevauchement date", "", "")>
				<cfreturn CreateReponse("AS006", 0, err)/>
			<cfelseif p_retour EQ -100>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de paramètres d'entrées obligatoire", "", "")>
				<cfreturn CreateReponse("AS006", 0, err)/>

			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AS006", 0, ThrowCfCatchError("AS006",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
			
		</cftry>
		
	</cffunction>
	
</cfcomponent>