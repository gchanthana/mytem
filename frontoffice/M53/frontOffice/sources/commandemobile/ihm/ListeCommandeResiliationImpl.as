package commandemobile.ihm
{
	import commandemobile.entity.Commande;
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	import commandemobile.itemRenderer.PanelAffectationEvent;
	import commandemobile.popup.PopUpAnnulerCommandeIHM;
	import commandemobile.popup.PopUpDetailsCommandeIHM;
	import commandemobile.popup.PopUpLivraisonIHM;
	import commandemobile.popup.PopUpSaisieTechniqueIHM;
	import commandemobile.system.AbstractGestionPanier;
	import commandemobile.system.Action;
	import commandemobile.system.ActionEvent;
	import commandemobile.system.CommandeEvent;
	import commandemobile.system.Export;
	import commandemobile.system.GestionCommandeMobile;
	import commandemobile.system.GestionTransporteur;
	import commandemobile.system.GestionWorkFlow;
	import commandemobile.system.Transporteur;
	import commandemobile.temp.temp;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.URLUtilities;
	
	import exportData.DataGridDataExporter;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class ListeCommandeResiliationImpl extends Box
	{
		
		//IHM
		private var _pUpMailBox			:ActionMailBoxIHM;
		private var _popUp				:PopUpSaisieTechniqueIHM;
		private var _pUpTransporteurs	:PopUpLivraisonIHM;
		private var _popUpAnnulerCmd	:PopUpAnnulerCommandeIHM;
		private var _popUpHistorique	:HistoriqueIHM;
		
		//COMPOSANTS
		public var txtptSearch			:TextInput;
		public var dgListCommande		:DataGrid;
		public var cboFiltreEtat		:ComboBox;
		public var cbxPools				:ComboBox;
		public var txtFiltre			:TextInput;
		public var rdTous				:RadioButton;
		public var rdCours				:RadioButton;
		public var rdCloses				:RadioButton;
		public var rdAnnul				:RadioButton;
		public var rdbtnMesCommandes	:RadioButton;
		public var rdbtnToutesCommandes	:RadioButton;
		
		//OBJECTS
		private var gestionTrans		:GestionTransporteur 	= new GestionTransporteur();
		public var _temp				:temp 					= new temp();
		private var _selectedAction		:Action 				= new Action();
		private var _gworkFlow			:GestionWorkFlow 		= new GestionWorkFlow();
		private var _gestionCmd			:GestionCommandeMobile 	= new GestionCommandeMobile();
		private var _gestionPanier		:AbstractGestionPanier 	= new AbstractGestionPanier();
		private var _cmd				:Commande 				= new Commande();
		private var _infosMail			:InfosObject 			= new InfosObject();
		
		//VARIABLES GLOBALES
		public var ignored				:Boolean = false;
		
		//VARIABLES LOCALES
		public var listeTransporteurs	:ArrayCollection = new ArrayCollection();
		private var _listHistorique		:ArrayCollection = new ArrayCollection();
		public var listCommande			:ArrayCollection = new ArrayCollection();
		public var listCommandeEnCours	:ArrayCollection = new ArrayCollection();
		public var listCommandeCloses	:ArrayCollection = new ArrayCollection();
		public var listEtat				:ArrayCollection = new ArrayCollection();
		private var _livraisonView 		:Boolean = false;
		private var thisLogin			:String = "";
		private var userSession			:Object = new Object();
		private var rdbtMesCommandes	:int = 0;
		public var creationCompl		:Boolean = false;
		public var btnModeHandMode		:Boolean = true;
		public var img					:Image;
		[Embed(source="/assets/images/Help3.png",mimeType='image/png')]
		public var imgTrue :Class;
		
		
		public function set infosMail(value:InfosObject):void
		{
			_infosMail  = value
		}
		public function get infosMail():InfosObject
		{
			return _infosMail;
		}
		
		public function ListeCommandeResiliationImpl()
		{
			_temp.addEventListener("listPool",									listPoolHandler);
			_temp.addEventListener("listCommande", 								listCommandeHandler);
			_temp.addEventListener(CommandeEvent.COMMANDE_DELETED, 				listHandler);
			_temp.addEventListener("thisUtilisateur", 							thisUtilisateurHandler);
			_gworkFlow.addEventListener("historiqueFinished",					getHistoriqueResultHandler);
			_gestionPanier.addEventListener("fournirArticlesCommandeFinished",	openLivraisosnIHM);
			_gestionCmd.addEventListener("detailsFinished",						manageLivraison);
			gestionTrans.addEventListener("transporteursArrived",				transporteursArrivedHandler);
			addEventListener("mailSent",										sendActions);
			addEventListener("etapeListCommande",								listHandler);
			addEventListener(PanelAffectationEvent.DISPLAY_PANEL,				itemClickHandler);
			userSession = CvAccessManager.getUserObject();
		}

		protected function creationCompleteHandler():void
		{
			_temp.fournirPoolsDuGestionnaire();
		}
		
		protected function imgCreationCompletehandler():void
		{
			img.visible = true;
			img.source 	= imgTrue;
		}
				
		protected function imgClickHandler():void
		{
			var u:URLRequest = new URLRequest("http://www.consotel.fr/gestiondecommandesncf/gestioncommandesncf.htm");
			URLUtilities.openWindow(u.url);
		}
		
		private function listPoolHandler(e:Event):void
		{
			cbxPools.prompt			= "";
			if(_temp.listePools.length == 1)
			{
				cbxPools.dataProvider 	= _temp.listeConcatPools[0];
				cbxPools.errorString 	= "";
				recupererListeCommande();
			}
			else
			{
				if(_temp.listePools.length == 0)
				{
					cbxPools.prompt = "Aucun pool";
				}
				else
				{
					cbxPools.prompt = "Sélectionnez";
					cbxPools.dataProvider = _temp.listeConcatPools;
				}
			}
		}
		
		protected function cbxPoolsChnageHandler():void
		{
			if(cbxPools.selectedItem != null)
			{
				recupererListeCommande();
			}
		}
		
		private function thisUtilisateurHandler(e:Event):void//A METTRE EN FORME
		{

		}
		
		protected function rdBtnGrpCommandeChangeHandler(event:Event,rdbtnSelected:int):void//A METTRE EN FORME
		{
			rdbtMesCommandes = rdbtnSelected;
			if(rdTous.selected)
			{
				rdBtnGrpChangeHandler(event,0);
			}
			if(rdCours.selected)
			{
				rdBtnGrpChangeHandler(event,1);
			}
			if(rdCloses.selected)
			{
				rdBtnGrpChangeHandler(event,2);
			}
//			if(rdAnnul.selected)
//			{
//				rdBtnGrpChangeHandler(event,3);
//			}
		}
		
		protected function btnHistoriqueClickhandler():void
		{
			dispatchEvent(new Event("etapeHistorique",true));
		}
		
		protected function btnExporterClickHandler():void
		{
			exportCSV();
		}
		
		private function exportCSV():void 
		{
			if((dgListCommande.dataProvider as ArrayCollection).length > 0) 
			{	
				var dataToExport	:String = DataGridDataExporter.getCSVString(dgListCommande,";","\n",false);
				var url				:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/gridexports/exportCSVString.cfm";
				DataGridDataExporter.exportCSVString(url,dataToExport,"export");
			}
			else
			{
				Alert.show("Pas de données à exporter !","Consoview");
			}
		}
		
		private function popUpHistoriqueClosedHandler(e:Event):void
		{
		
		}
		
		private function getHistoriqueResultHandler(e:Event):void
		{
			if(_selectedAction != null)
			{
				ignored = false;
				_listHistorique = new ArrayCollection();
				_listHistorique = _gworkFlow.historiqueWorkFlow;
				
				if(_selectedAction.CODE_ACTION != null)
				{
					if(_selectedAction.CODE_ACTION == "EXPED")
					{
						afficherTransporteurs();
					}
					else
					{	
						if(_selectedAction.CODE_ACTION == "ANNUL")
						{
							_popUpAnnulerCmd 				= new PopUpAnnulerCommandeIHM();
							_popUpAnnulerCmd.selectedAction = _selectedAction;
							_popUpAnnulerCmd.libelleAction 	= _selectedAction.LIBELLE_ACTION;
							_popUpAnnulerCmd.addEventListener("messageSaisie",messageSaisieHandler);
							PopUpManager.addPopUp(_popUpAnnulerCmd,this);
							PopUpManager.centerPopUp(_popUpAnnulerCmd);
						}
						else
						{
							afficherMailBox();
						}
					}
				}
				else
				{
					_popUpAnnulerCmd 				= new PopUpAnnulerCommandeIHM();
					_popUpAnnulerCmd.selectedAction = _selectedAction;
					_popUpAnnulerCmd.libelleAction 	= _selectedAction.LIBELLE_ACTION;
					_popUpAnnulerCmd.addEventListener("messageSaisie",messageSaisieHandler);
					PopUpManager.addPopUp(_popUpAnnulerCmd,this);
					PopUpManager.centerPopUp(_popUpAnnulerCmd);
				}
			}
		}
		
		private function messageSaisieHandler(e:Event):void
		{
			_selectedAction = _popUpAnnulerCmd.selectedAction;
			_popUpAnnulerCmd.dispatchEvent(new Event("closeThis"));
			dispatchEvent(new Event("mailSent"));
		}
		
		private function sendActions(e:Event):void
		{
			_gworkFlow.faireActionWorkFlow(_selectedAction,dgListCommande.selectedItem as Commande,true);
			if(_selectedAction.CODE_ACTION == "LIVRE")
			{
				_livraisonView = true;
				addDetailCommande();
			}
			else
			{
				dispatchEvent(new Event("etapeListCommande"));
			}
		}

        private function addDetailCommande():void
        {
        	if(_livraisonView)
        	{
       			_gestionCmd.fournirDetailOperation(dgListCommande.selectedItem as Commande, dgListCommande.selectedItem.IDCOMMANDE);
       			_livraisonView = false;
       		}
       		else
       		{
       			if(ignored)
       			{
	       			_gestionCmd.fournirDetailOperation(dgListCommande.selectedItem as Commande, dgListCommande.selectedItem.IDCOMMANDE);
	       			ignored = false;
       			}
       		}
        }
        
        private function manageLivraison(e:Event):void
        {	
        	_cmd = _gestionCmd.selectedCommande;
//			_gestionPanier.fournirArticlesCommande(_cmd);
			openLivraisosnIHM(e);
        }
        
        private function openLivraisosnIHM(e:Event):void
        {
	        _popUp = new PopUpSaisieTechniqueIHM();
			PopUpSaisieTechniqueIHM(_popUp).commande = _cmd;
			_popUp.addEventListener("livraisonClosed",livraisonsIHMClosed);
			PopUpManager.addPopUp(_popUp,this,true);
			PopUpManager.centerPopUp(_popUp);
        }
        private function livraisonsIHMClosed(e:Event):void
        {
//        	dispatchEvent(new Event("mailSent"));
        	dispatchEvent(new Event("etapeListCommande"));
        }
		
		private function addHistorique():void
		{
			_gworkFlow.fournirHistoriqueEtatsCommande(dgListCommande.selectedItem as Commande);
		}
		
		private function itemClickHandler(e:PanelAffectationEvent):void
		{
			if(e.codeItem == "VIEW_CMD" || e.codeItem == "ERASE_CMD" || e.codeItem == "VIEW_CMD_PDF" || e.codeItem == "VIEW_CMD_TRACKING" ||  e.codeItem == "VIEW_SAISETECHNIQUE")
			{
				switch(e.codeItem)
				{
					case "VIEW_CMD"						: viewCommande();				break;
					case "ERASE_CMD"					: eraseCommande();				break;
					case "VIEW_CMD_PDF"					: viewPDF();					break;
					case "VIEW_CMD_TRACKING"			: viewTracking();				break;
					case "VIEW_SAISETECHNIQUE"			: viewSaisieTechnique();		break;
				}
			}
			else
			{
				_selectedAction = e.actionItem;
				addHistorique();
			}
		}
		
		private function viewSaisieTechnique():void
		{
			if(dgListCommande.selectedItem != null)
			{
				_livraisonView = false;
				ignored = true;
				addDetailCommande();
			}
		}
		
		protected function viewPDF():void
		{
			if(dgListCommande.selectedItem.IDCOMMANDE > 0)
	    	{
				var export:Export = new Export();
	    		export.exporterCommandeEnPDF(dgListCommande.selectedItem as Commande);
	    	}	
		}
		
		private function transporteursArrivedHandler(e:Event):void
		{
			var bool:Boolean = false;
			var listTrans:ArrayCollection = new ArrayCollection();
			if(dgListCommande.selectedItem != null)
			{
				listeTransporteurs = gestionTrans.listeTransporteurs;
				for(var i:int = 0;i < listeTransporteurs.length;i++)
				{
					if(listeTransporteurs[i].IDTRANSPORTEUR == dgListCommande.selectedItem.IDTRANSPORTEUR)
					{
						if(listeTransporteurs[i].IDTRANSPORTEUR != 0)
						{
							listTrans.addItem(listeTransporteurs[i]);
							bool = true;
						}
					}
				}
				if(!bool)
				{
					Alert.show("Aucun transporteur!","Erreur");
				}
				else
				{
					if(listTrans.length > 0)
					{
						navigateToURL(new URLRequest(Transporteur(listTrans.getItemAt(0)).URL_TRACKING+_cmd.NUMERO_TRACKING)/* */,"_blank");
					}
				}
			}
		}
		
		protected function viewTracking():void
		{
			if(dgListCommande.selectedItem == null)
			{ return; }
			var boolNumTracking:Boolean = false;
	    	var boolTransporteur:Boolean = false;
	    	var message:String = "";
	    	if(dgListCommande.selectedItem.NUMERO_TRACKING != "")
	    	{
	    		boolNumTracking = true	    		
	    	}
	    	else
	    	{
	    		message += "Pas de numéro de suivi spécifié\n"
	    	}
	    	if(dgListCommande.selectedItem.IDTRANSPORTEUR)
	    	{
	    		boolTransporteur = true	    		
	    	}
	    	else
	    	{
	    		message += "Pas de transporteur spécifié"
	    	}
	    	
	    	if(boolTransporteur && boolNumTracking)
	    	{
	    		gestionTrans.fournirListeTransporteurs();
	    	}
	    	else
	    	{
	    		Alert.show(message,"Erreur");
	    	}
		}
		
		
		protected function afficherTransporteurs():void
		{	
			_pUpTransporteurs = new PopUpLivraisonIHM();
			_pUpTransporteurs.cmd = dgListCommande.selectedItem as Commande;		
			_pUpTransporteurs.addEventListener("transporteurIsSelected",transporteurIsSelectedCloseHandler);
			PopUpManager.addPopUp(_pUpTransporteurs,this,true);
			PopUpManager.centerPopUp(_pUpTransporteurs);
		}
		
		private function transporteurIsSelectedCloseHandler(e:Event):void
		{
			ignored = _pUpTransporteurs.ignored;
			afficherMailBox();
		}
		
		protected function afficherMailBox():void
		{	
			_pUpMailBox = new ActionMailBoxIHM();
			infosMail.commande = dgListCommande.selectedItem as Commande;
			infosMail.historiqueCommande = _listHistorique;
			infosMail.corpMessage = _selectedAction.MESSAGE;
			var objet:String = "Réf. : "  + infosMail.commande.REF_CLIENT;
			_pUpMailBox.initMail("Commande Mobile",objet,"");
			_pUpMailBox.configRteMessage(infosMail,GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			_pUpMailBox.selectedAction = _selectedAction;
			PopUpManager.addPopUp(_pUpMailBox,this,true);
			PopUpManager.centerPopUp(_pUpMailBox);
		}
		
		protected function _pUpMailBoxMailEnvoyeHandler(ev : Event):void
		{	
			_selectedAction.COMMENTAIRE_ACTION = _pUpMailBox.txtCommentaire.text; //txtCommentaire.text;
	    	_selectedAction.DATE_ACTION = new Date();//dcDateAction.selectedDate;
	    		    	
	    	var actionEvent:ActionEvent = new ActionEvent(ActionEvent.ACTION_CLICKED);
	    	actionEvent.action = _selectedAction;
	    	dispatchEvent(actionEvent);	    	
	    	
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
			dispatchEvent(new Event("mailSent"));
			if(ignored)
			{
				addDetailCommande();
			}
		}
		
		protected function searchClickHandler():void
		{
			rdbtMesCommandes = 0;
			txtFiltre.text = "";
			rdCours.selected = true;
			rdbtnMesCommandes.selected = true;
			var event:ListEvent;
			rdBtnGrpChangeHandler(event,1);
			dispatchEvent(new Event("etapeListCommande"));
		}

		protected function listHandler(e:Event):void
		{
			if(cbxPools.selectedItem != null)
			{
				recupererListeCommande();
			}
		}
		
		protected function btnNewCommandeClickHandler():void
		{
			dispatchEvent(new Event("etapeNewCommande",true));
		}
		
		protected function rdBtnGrpChangeHandler(e:Event,rdBtnSelected:int):void
		{
			listCommande = new ArrayCollection();
			if(cboFiltreEtat.selectedItem != null && cboFiltreEtat.selectedItem.IDETAT > 0)
			{
				switch(rdBtnSelected)
				{
					case 0:	if(rdbtMesCommandes == 0)
							{
								searchForFilters(_temp.inventaireCommandes,cboFiltreEtat.selectedItem,userSession.CLIENTACCESSID);
							}
							else
							{
								searchForFilters(_temp.inventaireCommandes,cboFiltreEtat.selectedItem);
							}
							break;
					case 1:	if(rdbtMesCommandes == 0)
							{
								searchForFilters(listCommandeEnCours,cboFiltreEtat.selectedItem,userSession.CLIENTACCESSID);
							}
							else
							{
								searchForFilters(listCommandeEnCours,cboFiltreEtat.selectedItem);
							}
							break;
					case 2:	if(rdbtMesCommandes == 0)
							{
								searchForFilters(listCommandeCloses,cboFiltreEtat.selectedItem,userSession.CLIENTACCESSID);
							}
							else
							{
								searchForFilters(listCommandeCloses,cboFiltreEtat.selectedItem);
							}
							break;
				}
			}
			else
			{	
				if(rdbtMesCommandes == 0)
				{
					switch(rdBtnSelected)
					{
						case 0:listCommande = mesCommandesOrNot(_temp.inventaireCommandes,userSession.CLIENTACCESSID);
								break;
						case 1:listCommande = mesCommandesOrNot(listCommandeEnCours,userSession.CLIENTACCESSID);
								break;
						case 2:listCommande = mesCommandesOrNot(listCommandeCloses,userSession.CLIENTACCESSID);
								break;
					}
				}
				else
				{
					switch(rdBtnSelected)
					{
						case 0:listCommande = _temp.inventaireCommandes;break;
						case 1:listCommande = listCommandeEnCours;break;
						case 2:listCommande = listCommandeCloses;break;
					}
				}
			}
		}
		
		private function mesCommandesOrNot(dataArrayC:ArrayCollection,userId:int = -1):ArrayCollection
		{
			var tempArrayC:ArrayCollection = new ArrayCollection();
			if(dataArrayC != null)
			{
				for(var i:int = 0;i < dataArrayC.length;i++)
				{
					if(dataArrayC[i].USERID == userId)
					{
						tempArrayC.addItem(dataArrayC[i]);
					}
				}
			}
			return tempArrayC;
		}
		
		private function searchForFilters(dataArrayC:ArrayCollection,obj:Object,userId:int = -1):void
		{
			var tempArrayC:ArrayCollection = new ArrayCollection();
			for(var i:int = 0;i < dataArrayC.length;i++)
			{
				if(userId != -1)
				{
					if(dataArrayC[i].LIBELLE_LASTETAT == obj.LIBELLE_ETAT && dataArrayC[i].USERID == userId)
					{
						tempArrayC.addItem(dataArrayC[i]);
					}
				
				}
				else
				{
					if(dataArrayC[i].LIBELLE_LASTETAT == obj.LIBELLE_ETAT)
					{
						tempArrayC.addItem(dataArrayC[i]);
					}
				}
			}
			listCommande = tempArrayC;
		}
		
		protected function cboFiltreEtatChangeHandler(event:ListEvent): void
	    {
			if(rdTous.selected)
			{
				rdBtnGrpChangeHandler(event,0)
			}
			if(rdCours.selected)
			{
				rdBtnGrpChangeHandler(event,1)
			}
			if(rdCloses.selected)
			{
				rdBtnGrpChangeHandler(event,2)
			}
	    }
	    
	    protected function dgListeDoubleClickHandler():void
	    {
	    	afficherDetailCommande();
	    }
	    
	    private function viewCommande():void
		{
			afficherDetailCommande();
		}
	    
	    private function afficherDetailCommande():void
	    {
	    	if(dgListCommande.selectedItem != null)
	    	{
				var popUpDetails:PopUpDetailsCommandeIHM = new PopUpDetailsCommandeIHM();
				PopUpManager.addPopUp(popUpDetails,this,true);
				PopUpManager.centerPopUp(popUpDetails);
//				popUpDetails.idCommande = (dgListCommande.selectedItem as Commande).IDCOMMANDE;
//				popUpDetails._cmd = dgListCommande.selectedItem as Commande;
				popUpDetails.idCommande = dgListCommande.selectedItem.IDCOMMANDE;
				popUpDetails._cmd = copyCommande(dgListCommande.selectedItem)[0] as Commande;
				
//				popUpDetails._cmd = ObjectUtil.copy(dgListCommande.selectedItem) as Commande;
				popUpDetails.dispatchEvent(new Event("commandeArrived"));
				popUpDetails.addEventListener("popUpDetailsClosed", popUpDetailsClosedHandler);
	    	}
	    }
	    
	    private function copyCommande(objectCommande:Object):ArrayCollection
	    {
	    	var arrayCTemp:ArrayCollection = new ArrayCollection();
	    	arrayCTemp.addItem(objectCommande);
	    	return arrayCTemp;
	    }
		
		private function popUpDetailsClosedHandler(e:Event):void
		{
			dispatchEvent(new Event("etapeListCommande"));
		}

		private function listCommandeHandler(e:Event):void
		{
			if(cbxPools.selectedItem != null)
			{
				var arrayC:ArrayCollection = new ArrayCollection();
				
				for(var j:int ; j < _temp.inventaireCommandes.length ; j++)
				{
					if(_temp.inventaireCommandes[j].IDPOOL_GESTIONNAIRE == cbxPools.selectedItem.IDPOOL)
					{
						arrayC.addItem(_temp.inventaireCommandes[j]);
					}
				}

				listCommande = new ArrayCollection();
				listCommandeEnCours = new ArrayCollection();
				listCommandeCloses = new ArrayCollection();
				
				for(var i:int = 0;i < arrayC.length;i++)
				{
					if(arrayC[i].ENCOURS  == "1")
					{
						listCommandeEnCours.addItem(arrayC[i]);
					}
					
					if(arrayC[i].ENCOURS  == "0")
					{
						listCommandeCloses.addItem(arrayC[i]);
					}
				}
				rdbtnMesCommandes.selected 	= true;
				rdCours.selected 			= true;
				if(arrayC.length > 0)
				{
					rdBtnGrpChangeHandler(e,1);
				}
				else
				{
					_temp.inventaireCommandes = new ArrayCollection();
					listCommande = new ArrayCollection();
					listCommandeEnCours = new ArrayCollection();
					listCommandeCloses = new ArrayCollection();
				}
			}
		}
		
		private function checkEtat(item:ArrayCollection):void
		{
			listEtat = new ArrayCollection();
			var object:Object = new Object();
			
			for(var i:int = 0;i < listCommande.length;i++)
			{
				if(listCommande[i].TYPE_OPERATION  == "1")
				{
					listCommandeEnCours.addItem(listCommande[i]);
				}
				
				if(listCommande[i].ENCOURS  == "0")
				{
					listCommandeCloses.addItem(listCommande[i]);
				}
			}
		}
		
		protected function txtptSearchHandler():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
	
			if(txtptSearch.text == "")
			{
				listCommande = _temp.inventaireCommandes;
			}
			else
			{
				for(var i:int = 0;i < listCommande.length;i++)
				{
					if(listCommande[i].LIBELLE_COMMANDE.toString().toLowerCase().indexOf(txtptSearch.text.toLowerCase()) != -1)
					{ 
						arrayC.addItem(listCommande[i]); 
					}
				}
				listCommande = new ArrayCollection();
				listCommande = arrayC;
			}
		}
		
		private function closePopUpNewCommandeHandler(e:Event):void
		{
		
		}
		
		private function sendCommande():void
		{
		
		}
		private function askValidationDistributeur():void
		{
		
		}
		private function askValidationInterne():void
		{
		
		}
		private function eraseCommande():void
		{
			if(dgListCommande.selectedItem != null)
			{
				_temp.supprimerCommande(dgListCommande.selectedItem as Commande);
			}
		}

		private function recupererListeCommande():void
		{
			_temp.fournirInventaireCommandes(new Date(2008,0),new Date(2020,11),"");
		}
		
		protected function filterFunction(item:Object):Boolean
	    {    
		    var idEtat:int = ((cboFiltreEtat.selectedItem != null) ? cboFiltreEtat.selectedItem.IDETAT:-1);
//		    var idRevendeur:int = ((cboFiltreRevendeur.selectedItem != null) ? cboFiltreRevendeur.selectedItem.IDREVENDEUR:-1);
		    
		    var rfilter:Boolean = true;		    
		    // Filtre sur les etats
		   	if(idEtat > 0) 
		    {		     
		        rfilter = rfilter && (Commande(item).IDLAST_ETAT == idEtat); 
		    }
		    else
		    {
		    	rfilter = rfilter && true;
		    }
		    
		    //filtre sur les revendeurs
//		    if(idRevendeur > 0) 
//		    {		     
//		        rfilter = rfilter && (Commande(item).IDREVENDEUR == idRevendeur); 
//		    }
//		    else
//		    {
//		    	rfilter = rfilter && true;
//		    }
		    
		    // Filtre sur la ref client
		    // Filtre sur le libelle de la commande
		    // Filtre sur le numéro de commande
		     
		    rfilter = rfilter && ( 
		    						(Commande(item).REF_CLIENT != null && Commande(item).REF_CLIENT.toLocaleLowerCase().search(txtFiltre.text) != -1)
		    						||
		    						(Commande(item).LIBELLE_COMMANDE != null && Commande(item).LIBELLE_COMMANDE.toLocaleLowerCase().search(txtFiltre.text) != -1)
		    						||
		    						(Commande(item).NUMERO_COMMANDE != null && Commande(item).NUMERO_COMMANDE.toLocaleLowerCase().search(txtFiltre.text) != -1)
		    					)
		    return rfilter;
		}
		
		protected function txtFiltreChangeHandler(event:Event): void
	    {	
	    	if(dgListCommande.dataProvider != null)
	    	{
	    		(dgListCommande.dataProvider as ArrayCollection).filterFunction = filterFunction;
	    		(dgListCommande.dataProvider as ArrayCollection).refresh();
	    	}
	    }
	    
	    private function refreshGrid():void
	    {
	    	dgListCommande.dataProvider = listCommande;
	    }
	    
	    
	    
	    /* =================================================================================================*/
	    /*																									*/			
	    /*					PARAMS OPERATEURS																*/
	    /*																									*/
	    /* =================================================================================================*/
	    protected function btParametresClickHandler(event:MouseEvent = null):void
	    {
	    	//parametresOperateurDistributeurIHM
	    	//HistoriqueInfosClientOperateurIHM
	    	//////////////////////--------------------------------------------------------------------------------------------CA VA CHANGER ICI!!!!!!!!!
	    	var paramWindow:parametresOperateurDistributeurIHM = new parametresOperateurDistributeurIHM();	    		    	
	    	PopUpManager.addPopUp(paramWindow,this,true);
	    	PopUpManager.centerPopUp(paramWindow);
	    	
	    }

	}
}