<cfcomponent output="false">
	<cfproperty name="idGroupeMaitre" type="numeric" default="0">
	<cfproperty name="idGroupeClient" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	<cfproperty name="xmlParamUtil" type="any">
	
	
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupeMaitre = 0;
		variables.idGroupeClient = 0;
		variables.typePerimetre = "Groupe";
		variables.idUser = 0;
		variables.profileOperateur = 'Info OpÃ©rateur';
		variables.xmlParamUtil = createObject("component","fr.consotel.util.consoview.xml.XmlParamUtil"); 
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getIdGroupeMaitre" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeMaitre>
	</cffunction>
	
	<cffunction name="getIdGroupeClient" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeClient>
	</cffunction>
	 
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	<cffunction name="getIdUser" output="false" access="public" returntype="numeric">
		<cfreturn variables.idUser>
	</cffunction>
	
	<cffunction name="getProfileOperateur" output="false" access="public" returntype="string">
		<cfreturn variables.profileOperateur>
	</cffunction>
	
	<cffunction name="logXmlParamIfSpecified" access="remote" returntype="void">
		<cfargument name="xmlParamObject" required="true" type="XML">
	</cffunction>
	
	<cffunction name="getXmlParamUtil" output="false" access="public" returntype="any">
		<cfreturn variables.xmlParamUtil>
	</cffunction>
	
	<cffunction name="toInverseDateString" output="false" access="public" returntype="string">
		<cfargument name="laDate" type="any" required="true">
		<cfif isDate(laDate)>
			<cfreturn lsDateFormat(laDate,"YYYY/MM/DD")>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
</cfcomponent>