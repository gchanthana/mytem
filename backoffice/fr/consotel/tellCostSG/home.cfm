	<cfif IsDefined ('SESSION')>
		<cfif IsDefined ('SESSION.IS_CONNECTED')>
			<cfif SESSION.IS_CONNECTED EQ 0> 		
				<cflocation url="index_mobile.cfm">
			</cfif>
		<cfelse>	 
			<cflocation url="index_mobile.cfm">
		</cfif>	
	<cfelse>	 
		<cflocation url="index_mobile.cfm">
	</cfif>
	
	<?xml version="1.0" encoding="iso-8859-1" ?>
	<!DOCTYPE html PUBLIC
	"-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
	"http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>
			Bienvenue sur TellCost
		</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<meta name="robots" content="all"/>
		<meta name="classification" content="Services"/>
		<meta name="copyright" content="ConsoTel SAS"/>
		<meta name="author" content="Guillaume RAMOS"/>
		<meta name="language" content="fr"/>
		<meta name="description" content="TELLCOST"/>
		<meta name="keywords" content="TELLCOST"/>
		<link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
	
    <body text="#FFFFFF">
		
		<FONT face="Arial">
	        <div class="header-img">
	        	<br/>
	          	<strong>TELLCOST</strong>
	        	<br/><br/>
	        </div>
	
			<div class="BodyText">       
				<div class="homeng">
				 	<a href="<cfoutput>details_pda.cfm?#SESSION.URLTOKEN#</cfoutput>" accesskey="1"><strong>1 - VOTRE FACTURE</strong></a> 
		        </div> 
			
				<table border="0" height="100%">
					<tr height="100%">
						<td valign="middle" height="100%"> 
							<img alt="list" src="tellCostImg/note_view.png" height="30" width="30" style="float:left;margin: 6px 6px 6px 6px;" />
						</td>
						<td valign="middle" height="100%"> 
			        		<p><a href="<cfoutput>details_pda.cfm?#SESSION.URLTOKEN#</cfoutput>" accesskey="1">Consultez le relev&eacute; des services et consommation de t&eacute;l&eacute;phonie mobile du mois.</a></p>
						</td>
					</tr>
				</table>
				
		        <div class="homeng">
			          <a href="<cfoutput>conseils.cfm?#SESSION.URLTOKEN#</cfoutput>" accesskey="2"><strong>2 - REDUIRE MA FACTURE</strong></a>
		        </div>
			
				<table border="0" height="100%">
					 <tr height="100%">
						<td valign="middle" height="100%"> 
							<img alt="list" src="tellCostImg/picto_info.gif" height="30" width="30" style="float:left;margin: 6px 6px 6px 6px;" />
						</td>
						<td valign="middle" height="100%"> 
		        			<p><a href="<cfoutput>conseils.cfm?#SESSION.URLTOKEN#</cfoutput>" accesskey="2">Réduisez vos consommations.</a></p>
						</td>
					</tr>
				</table>	
		
				<div class="homeng">
			    	<a href="<cfoutput>index_mobile.cfm</cfoutput>" accesskey="3"><strong>3 - DECONNEXION</strong></a>
		        </div>
			
				<table border="0" height="100%">
					 <tr height="100%">
						<td valign="middle" height="100%"> 
							<img alt="list" src="tellCostImg/ico157.gif" height="30" width="30" style="float:left;margin: 6px 6px 6px 6px;" />
						</td>
						<td valign="middle" height="100%"> 
		       				<p><a href="index_mobile.cfm" accesskey="3">Déconnectez vous.</a></p>
						</td>
					</tr>
				</table>
			</div>
		</FONT>
    </body>
</html>