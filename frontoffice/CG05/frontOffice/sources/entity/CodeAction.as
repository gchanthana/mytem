package entity
{
	public class CodeAction
	{
		public function CodeAction()
		{
		}
		
/*********************************************************/
/******************* AL - ACTION LISTE *******************/
/*********************************************************/
		public static var LISTE_CLIENTS				:String = "AL001";// liste des clients par utilisateur
		public static var LISTE_PROFILS				:String = "AL002";// liste des profils

		public static var LISTE_FONCTIONS			:String = "AL003";// liste des fonctions
		public static var LISTE_NIVEAUX				:String = "AL004";// liste des Niveaux
		public static var LISTE_DROIT_SPEC_FCT		:String = "AL005";// droit spécifique d'une fonction
		
		public static var LISTE_APPLICATIONS		:String = "AL006";// liste des applications
		public static var LISTE_LANGUES				:String = "AL007";// liste des langues
		public static var LISTE_ACTUALITES			:String = "AL008";// liste des actualités
		public static var PJ_ACTUALITES				:String = "AL010";// liste des PJ affiliés à une actualité
		
		public static var LISTE_PARAM_FCT			:String = "AL009";//liste des paramètres d'une fonction et pour quel niveau chaque parametre est visible
		
		public static var LISTE_FCT_USER			:String = "AL011";// liste des fonction par utilisateur
		public static var LISTE_FCT_PROFIL			:String = "AL012";// liste des fonctions par profil
		
		public static var LISTE_UNIVERS				:String = "AL013";// Liste des univers (univers = catégories) : indispensable pour créer une nouvelle fonction
		
		// Gestion des partenaires
		public static var LISTE_PARTENAIRES			:String = "AL014"; // Liste des partenaires
		public static var LISTE_SITES				:String = "AL015"; // liste des sites d'un partenaire
		public static var LISTE_CONTRATS			:String = "AL016"; // listes des contrats d'un partenaire
		public static var LISTE_FORMATIONS			:String = "AL017"; // liste des formations d'un partenaire
		public static var LISTE_INTERLOCUTEURS		:String = "AL018"; // liste des interlocuteurs d'un partenaire
		public static var LISTE_SUJETS_FORMATION	:String = "AL019"; // liste des modules disponibles pour formation
		public static var LISTE_LIEUX_FORMATION		:String = "AL020"; // Les locaux où se tiennent les formations
		public static var LISTE_LANGUES_PARTN		:String = "AL021"; // Liste des langues utilisées par les les utilisateurs
		public static var LISTE_CERTIFICATIONS		:String = "AL022"; // Liste des certifications (ou types de contrats)
		public static var LISTE_PARTICIPANTS		:String = "AL023"; // Liste des participants à une formation
		public static var LISTE_CIVILITES			:String = "AL024"; // Liste des civilités Mr, Mme ...
		public static var LISTE_TYPES_PLAN_TARIF	:String = "AL025"; // Liste des types de plan tarifaires.
		
		// Gestion des collectes
		public static var LISTE_COLLECTETYPE		:String = "AL026"; // Liste des templates de collectes en fonction de l'opérateur, du type de données et du mode de récupération
		public static var LISTE_PUBLISHEDTEMPLATE	:String = "AL027"; // Liste des templates publiés en fonction de l'opérateur, du type de données et du mode de récupération
		public static var LISTE_COLLECTECLIENT		:String = "AL028"; // Liste des collectes pour une racine donnée
		public static var LISTE_RACINE				:String = "AL029"; // Liste des racines
		public static var LISTE_OPERATEURS			:String = "AL030"; // Liste des opérateurs
		public static var LISTE_OPERATEURS_TEMPLATE	:String = "AL031"; // Liste des opérateurs avec template  [id, libelle, libellé rocf associé]
		public static var LISTE_DATATYPE			:String = "AL032"; // Liste des types de données [id,libelle] (ex : facturation, usages ...)
		public static var LISTE_RECUPMODE			:String = "AL033"; // Liste des modes de récupération [id,libelle] (ex : PULL-FTP, PUSH-MAIL ...)
		
		// Gestion des Clients						
		public static var GCLI_LISTE_CLIENTS		:String = "AL034"; // Liste des clients.

/*********************************************************/
/***************** AI - ACTION INFOS *********************/
/*********************************************************/
		// Gestion des Profils
		
		//Gestion des fonctions
		
		//Gestions des Actualités(communications)
		public static var INFOS_ACTUALITES			:String = "AI001";// infos des actualités
		
/*********************************************************/	
/****************** AJ - ACTION AJOUT ********************/
/*********************************************************/
		// Gestion des Profils
		public static var CREATE_PROFIL				:String = "AJ001";// creation profil
		public static var UPDATE_PROFIL 			:String = "AJ002";// modification profil	

		//Gestion des Fonctions
		public static var UPDATE_FUNCTION			:String = "AJ003";// Changement d'acces pour une fonction
		public static var CREATE_FUNCTION			:String = "AJ004";// Enregistrer une nouvelle fonction
		public static var CREATE_DROIT_FCT			:String = "AJ005";// Ajouter un droit à une fonction
		public static var UPDATE_DROIT_FCT			:String = "AJ006";// mise à jour des droits d'une fonctions
		
		//Gestions des Actualités(communications)
		public static var CREATE_ACTUALITE			:String = "AJ007";// Création/Edition d'une actualité
		public static var ADD_FILE_TO_ACTUALITE		:String = "AJ008";// Ajouter une pièce jointe à une actualité
		public static var UPDATE_SLIDER_ACTUALITE	:String = "AJ009";// modifier la position d'une actualité
		
		// Gestion des partenaires
		public static var CREATE_UPD_SITE			:String = "AJ010";
		public static var UPDATE_CONTRAT			:String = "AJ011";
		public static var CREATE_UPD_FORMATION		:String = "AJ012";
		public static var UPD_PARTICIPANTS_FORMA	:String = "AJ013";
		public static var CREATE_UPD_INTERLOC		:String = "AJ014";
		public static var CREATE_CONTRAT			:String = "AJ015";
		public static var CREATE_UPD_PARTENAIRE		:String = "AJ016";
		public static var CREATE_ALL_INFOS_PARTN	:String = "AJ017";
		
		// Gestion des collectes
		public static var CREATE_COLLECTECLIENTVO	:String = "AJ018";//Crée ou édite une nouvelle collecte
		public static var CREATE_COLLECTETYPEVO		:String = "AJ019";//Crée ou édite un template de collecte
		public static var CREATE_UIDSTORAGE			:String = "AJ020";//Crée un identifiant unique associé à un template et nécessaire pour stocker les documents sur le serveur

/*********************************************************/
/***************** AS - ACTION SUPPRIMER *****************/
/*********************************************************/
		// Gestion des Profils
		public static var DELETE_PROFIL			:String = "AS001";// supprimer un profil		
		
		//Gestion des Fonctions
		public static var DELETE_FUNCTION		:String = "AS002";// supprimer une fonction
		

		//Gestions des Actualités(communications)
		public static var DELETE_ACTUALITE		:String = "AS003";// supprimer une actualité
		public static var DELETE_PJ_ACTUALITE	:String = "AS005";// supprimer une PJ actualité
		
		//Gestion des Fonctions
		public static var DELETE_PARAM_FCT		:String = "AS004";// supprimer une fonction
		
		//Gestion des Partenaires
		public static var DESACTIVE_CONTRAT		:String = "AS006";// désactiver un contrat
		
		//Gestion des Collectes
		public static var DESTROY_LASTUID		:String = "AS007";// supprime un dossier UID spécifique suite à l'annulation de création d'un template
		
/*********************************************************/
/******************** AF - ACTION METIER *****************/
/*********************************************************/
		
		//Gestions des Actualités(communications)
		public static var COPY_ACTUALITE_TO_OTHER_APPLICATION	:String = "AF001";// copier une actualité vers autre application
		public static var RENAME_FILE							:String = "AF002";// renommer un fichier que le nom contient des caractères spéciaux.
		
/*********************************************************/
/********************* AM - ACTION MENU ******************/
/*********************************************************/
		public static var MENU_RACINE : String = "AM001";

		
	}
}