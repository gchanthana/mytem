package service.tab.gettabracine
{
	import event.tab.TabEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.tab.TabVo;

	internal class GetTabModel extends EventDispatcher
	{
		private var _tabs:ArrayCollection;

		public function GetTabModel()
		{
			_tabs=new ArrayCollection();
		}

		public function get tabs():ArrayCollection
		{
			return _tabs;
		}

		internal function updateListTab(value:ArrayCollection):void
		{
			_tabs.removeAll();
			var tabVo:TabVo;

			for (var i:int=0; i < value.length; i++)
			{
				tabVo=new TabVo();
				tabVo.fill(value[i]);
				_tabs.addItem(tabVo);
			}
			dispatchEvent(new TabEvent(TabEvent.CHANGER_DATA_TAB_EVENT));/** capture dans SUperTabNavigateurImpl */
		}
	}
}
