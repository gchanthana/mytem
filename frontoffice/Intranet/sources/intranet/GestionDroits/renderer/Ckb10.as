package intranet.GestionDroits.renderer
{
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.CheckBox;

	public class Ckb10 extends VBox
	{
		public var ckb:CheckBox=new CheckBox();
		
		public function Ckb10()
		{
			super();
			setStyle("horizontalAlign","center");
			this.addChild(ckb);
			this.addEventListener(MouseEvent.CLICK,ckbChangeDataHandler);
		}
		
		public function ckbChangeDataHandler(e:MouseEvent):void{
			if(ckb.selected){
				data.obj10 = 1;
			}else{
				data.obj10 = 0;
			}
		}
					
		override public function set data(value:Object):void{
			if(value != null && value != ""){
				super.data=value;
				if(data.obj10 == 1){
					ckb.selected=true;
				}else{
					ckb.selected=false;
				}
				if(data.EDITABLE == false){
					ckb.enabled = false
				}else{
					ckb.enabled = true;
				}
			}
		}
	}
}