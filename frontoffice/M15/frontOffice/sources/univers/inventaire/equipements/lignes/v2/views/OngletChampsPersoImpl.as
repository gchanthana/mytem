package univers.inventaire.equipements.lignes.v2.views
{
	import composants.util.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.describeType;
	
	import mx.containers.Form;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import univers.inventaire.equipements.lignes.v2.applicatif.LibellesPersos;
	
	[Bindable]
	public class OngletChampsPersoImpl extends BaseView
	{
		
		public var frmLeft : Form;
		public var frmRight : Form;
		
		
		public var libelle1:TextInputLabeled;
		public var libelle2:TextInputLabeled; 
		public var libelle3:TextInputLabeled;
		public var libelle4:TextInputLabeled;
		public var libelle5:TextInputLabeled;
		public var libelle6:TextInputLabeled;
		public var libelle7:TextInputLabeled;
		public var libelle8:TextInputLabeled;
		public var libelle9:TextInputLabeled;
		public var libelle10:TextInputLabeled;
		public var libelle11:TextInputLabeled;
		public var libelle12:TextInputLabeled;
		public var libelle13:TextInputLabeled;
		public var libelle14:TextInputLabeled;
		public var libelle15:TextInputLabeled;
		public var libelle16:TextInputLabeled;
		public var libelle17:TextInputLabeled;
		public var libelle18:TextInputLabeled;
		public var libelle19:TextInputLabeled;
		public var libelle20:TextInputLabeled;
		public var libelle21:TextInputLabeled;
		public var libelle22:TextInputLabeled;
		public var libelle23:TextInputLabeled;
		public var libelle24:TextInputLabeled;
		public var libelle25:TextInputLabeled;
		public var libelle26:TextInputLabeled;
		public var btUpdate : Button
		
			
		public function OngletChampsPersoImpl()
		{
			super();
		}
		
		override protected function commitProperties():void{
			super.commitProperties();			 
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);			
		}
		
		override protected function enregistrer():void{
			var libelles : LibellesPersos = gestionTechnique.libellesPersos;
			var classInfo : XML = describeType(libelles);
			
			var c1 :  String = (libelle1 != null)?libelle1.text:"Sans titre";
			var c2 :  String = (libelle2 != null)?libelle2.text:"Sans titre";
			var c3 :  String = (libelle3 != null)?libelle3.text:"Sans titre";
			var c4 :  String = (libelle4 != null)?libelle4.text:"Sans titre";
			var c5 :  String = (libelle5 != null)?libelle5.text:"Sans titre";
			var c6 :  String = (libelle6 != null)?libelle6.text:"Sans titre";
			var c7 :  String = (libelle7 != null)?libelle7.text:"Sans titre";
			var c8 :  String = (libelle8 != null)?libelle8.text:"Sans titre";
			var c9 :  String = (libelle9 != null)?libelle9.text:"Sans titre";
			var c10 :  String = (libelle10 != null)?libelle10.text:"Sans titre";
			
			gestionTechnique.libellesPersos.updateAllLibelles(	c1,
																c2,
																c3,
																c4,
																c5,
																c6,
																c7,
																c8,
																c9,
																c10);	
		}
		
		//================== HANDLERS ===============================================
		private function btUpdateClickHandler(me : MouseEvent):void{			
            enregistrer();
 		}
		
		//================== FIN HANDLERS ===========================================
	}
}