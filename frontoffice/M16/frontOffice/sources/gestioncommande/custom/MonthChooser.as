package gestioncommande.custom
{
	import mx.controls.DateChooser;
	import mx.core.mx_internal;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.DateChooserEvent;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	use namespace mx_internal;
	public class MonthChooser extends DateChooser
	{
		// le mois courant
		private var currentMonth:Date = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
		
		public function MonthChooser()
		{
			monthNames=[ResourceManager.getInstance().getString('M16', 'Janvier'),ResourceManager.getInstance().getString('M16', 'F_vrier'),ResourceManager.getInstance().getString('M16', 'Mars'),ResourceManager.getInstance().getString('M16', 'Avril'),ResourceManager.getInstance().getString('M16', 'Mai'),ResourceManager.getInstance().getString('M16', 'Juin'),ResourceManager.getInstance().getString('M16', 'Juillet'),ResourceManager.getInstance().getString('M16', 'Ao_t'),ResourceManager.getInstance().getString('M16', 'Septembre'),ResourceManager.getInstance().getString('M16', 'Octobre'),ResourceManager.getInstance().getString('M16', 'Novembre'),ResourceManager.getInstance().getString('M16', 'D_cembre')];
			selectedDate = currentMonth;
		}

		public function getCurrentMonth():Date
		{
			return this.currentMonth;
		}

		override protected function createChildren():void
		{
			super.createChildren();
			dateGrid.addEventListener(DateChooserEvent.SCROLL,dateGrid_scrollHandler);
		}
		
		override protected function measure():void
		{
			super.measure();
			dateGrid.visible = false;
			measuredHeight = measuredHeight - dateGrid.getExplicitOrMeasuredHeight();
		}
		
		private function dateGrid_scrollHandler(event:DateChooserEvent):void
		{
			var month:int = event.currentTarget.displayedMonth;
			var year:int = event.currentTarget.displayedYear;
			
			selectedDate = new Date(year, month, 1);
		
			if(ObjectUtil.dateCompare(currentMonth, selectedDate) == -1)
				selectedDate = currentMonth;
			
			var e:CalendarLayoutChangeEvent = new CalendarLayoutChangeEvent(CalendarLayoutChangeEvent.CHANGE);
			e.newDate = selectedDate;
			dispatchEvent(e);
			
		}
	}
}