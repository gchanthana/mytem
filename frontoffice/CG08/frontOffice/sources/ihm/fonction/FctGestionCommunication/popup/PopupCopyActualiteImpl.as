package ihm.fonction.FctGestionCommunication.popup
{
	import composants.ui.TitleWindowBounds;
	
	import entity.ApplicationVO;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	public class PopupCopyActualiteImpl extends TitleWindowBounds
	{
		//----------- VARIABLES -----------//
		
		private var _listeApp				:ArrayCollection;
		private var _codeapp				:int;
		private var _idlangue				:int;
		
		public var dgCopyActu				:DataGrid;
		public var btnValider				:Button;
		public var btnAnnuler				:Button;
		[Bindable]
		public var proposedApp				:ArrayCollection = new ArrayCollection();
		
		
		//----------- METHODES -----------//
		
		/* */
		public function PopupCopyActualiteImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/* */
		private function init(fe:FlexEvent):void
		{
			initlisteners();
			initDataProvider();
		}
		
		/* */
		private function initlisteners():void
		{
			btnAnnuler.addEventListener(MouseEvent.CLICK,closePopup);
			dgCopyActu.addEventListener(ListEvent.ITEM_CLICK,dgAppli);
		}
		
		/* */
		private function initDataProvider():void
		{
			for each (var item:ApplicationVO in listeApp)
			{
				if(codeapp!=item.CODEAPP)
				{
					proposedApp.addItem(item);
					item.SELECTED = false; // initialiser propriété SELECTED=false(non coché)
				}
			}
		}
		
		/* */
		private function dgAppli(le:ListEvent):void
		{
			if(dgCopyActu.selectedItem.SELECTED)
				dgCopyActu.selectedItem.SELECTED=false;
			else
				dgCopyActu.selectedItem.SELECTED=true;
			
			listeApp.refresh();
		}
		
		/* */
		private function closePopup(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		
		//----------- SETTERS - GETTERS -----------//
		
		public function get listeApp():ArrayCollection
		{
			return _listeApp;
		}

		public function set listeApp(value:ArrayCollection):void
		{
			_listeApp = value;
		}

		public function get codeapp():int
		{
			return _codeapp;
		}

		public function set codeapp(value:int):void
		{
			_codeapp = value;
		}

		public function get idlangue():int
		{
			return _idlangue;
		}

		public function set idlangue(value:int):void
		{
			_idlangue = value;
		}
	}
}