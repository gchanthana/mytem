package intranet.gestionLoginClasse.entity
{

    public class UserVO
    {
        private var _access:AccessVO;
        private var _style:StyleVO;
        private var _app_login:String;
        private var _login_nom:String;
        private var _login_prenom:String;
        private var _login_email:String;
        private var _login_pwd:String;
        private var _libelle_groupe_client:String;
        private var _restriction_ip:Boolean;

        public function UserVO()
        {
            access = new AccessVO();
        }

        public function deserialize(userObj:Object):UserVO
        {
            app_login = userObj.APP_LOGINID;
            login_email = userObj.LOGIN_EMAIL;
            login_pwd = userObj.LOGIN_PWD;
            login_nom = userObj.LOGIN_NOM;
            login_prenom = userObj.LOGIN_PRENOM;
            return this;
        }

        public function serialize():Object
        {
            var userObj:Object = new Object();
            userObj.APP_LOGINID = app_login;
            userObj.LOGIN_EMAIL = login_email;
            userObj.LOGIN_PWD = login_pwd;
            userObj.LOGIN_NOM = login_nom;
            userObj.LOGIN_PRENOM = login_prenom;
            return userObj;
        }

        public function setFeatures(p_nom:String, p_prenom:String, p_email:String, p_pwd:String):void
        {
            login_nom = p_nom;
            login_prenom = p_prenom;
            login_email = p_email;
            login_pwd = p_pwd;
        }

        public function toString():String
        {
            return ("Utilisateur : " + login_prenom + " " + login_nom + "  mail : " + login_email + " password : " + login_pwd);
        }

        public function get login_nom():String
        {
            return _login_nom;
        }

        public function set login_nom(value:String):void
        {
            _login_nom = value;
        }

        public function get login_prenom():String
        {
            return _login_prenom;
        }

        public function set login_prenom(value:String):void
        {
            _login_prenom = value;
        }

        public function get login_email():String
        {
            return _login_email;
        }

        public function set login_email(value:String):void
        {
            _login_email = value;
        }

        public function get login_pwd():String
        {
            return _login_pwd;
        }

        public function set login_pwd(value:String):void
        {
            _login_pwd = value;
        }

        public function get libelle_groupe_client():String
        {
            return _libelle_groupe_client;
        }

        public function set libelle_groupe_client(value:String):void
        {
            _libelle_groupe_client = value;
        }

        public function get restriction_ip():Boolean
        {
            return _restriction_ip;
        }

        public function set restriction_ip(value:Boolean):void
        {
            _restriction_ip = value;
        }

        public function get app_login():String
        {
            return _app_login;
        }

        public function set app_login(value:String):void
        {
            _app_login = value;
        }

        public function get access():AccessVO
        {
            return _access;
        }

        public function set access(value:AccessVO):void
        {
            _access = value;
        }

        public function get style():StyleVO
        {
            return _style;
        }

        public function set style(value:StyleVO):void
        {
            _style = value;
        }
    }
}