﻿<cfcomponent>
	<!--- Supprime un dossier UID spécifique suite à l'annulation
	 de création d'un template	
	   --->
	<cffunction name="executeAct" access="public" returntype="Numeric" output="false">		
		<cfargument name="p_uid"  required="true" type="String" />
				
			<!--- TODO suppimer le dossier et contenu de stockage
			  nommé comme l'uid passé en param  --->		
			
		<cfreturn 1 >
	</cffunction>
</cfcomponent>