package ihm.component.upload
{
    import mx.resources.ResourceManager;
	import entity.enum.ETypePJ;
    import entity.vo.CollecteTypeVO;
    import entity.vo.UploadVO;
    
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.MouseEvent;
    import flash.events.ProgressEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.FileFilter;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    import flash.ui.Mouse;
    import flash.utils.setTimeout;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import ihm.TemplateCollecteFicheIHM;
    import ihm.component.upload.preloader.Spinner;
    
    import mx.containers.FormItem;
    import mx.containers.HBox;
    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.Image;
    import mx.controls.Label;
    import mx.controls.ProgressBar;
    import mx.controls.ProgressBarDirection;
    import mx.controls.TextInput;
    import mx.events.FlexEvent;

    [Bindable]
    /**
     * Cette classe étend la classe <code>FormItem</code> et permet d'insérer
     * au sein d'un formulaire un <b>composant d'upload </b>de fichier autonome
     * Elle assure le <b>Pilotage</b> de l'IHM  associé <code>UploadCompIHM</code>
     */
    public class UploadCompImpl extends FormItem
    {
        // Variables publiques paramétrables à l'extérieur du composant
        // IHM
        public var i_message:Label;
        // Ressource static
        [Embed(source="/assets/icones/publish.png")]
        public static const imgOk:Class;
        [Embed(source="/assets/icones/IMAGE9.gif")]
        public static const imgKo:Class;
        [Embed(source="/assets/icones/question.gif")]
        public static const imgQuestion:Class;
        public var message:String = "";
        public var percStr:String = "";
        // public var i_progressBar:ProgressBar;
        public var img_visible:Boolean = false;
        public var i_progressBar:ProgressBar;
        public var i_inputFileName:Label;
        public var i_btnBrowse:Button;
        public var i_btnCancel:Button;
        public var i_img_valid:Image;
        public var i_hb:HBox;
        // Paramètres du composant
        private var _libelle:String;
        private var _uploadTooltipInfo:String;
        private var _isLoading:Boolean;
        private var _defaultInputText:String;
        private var _uploadFileType:String;
        private var _uploadUID:String;
        private var _widthField:Number = 200;
        private var fileRef:FileReference;
        private var uploadVo:UploadVO;
        // CV_NON_SECURE_URL
        private const URL_BACKOFFICE:String = RevisionM25.urlBackoffice;
        private const URL_CACHE:String = "https://cache.consotel.fr/documents/M25/";
        private const FILE_UPLOAD_URL:String = "fr/consotel/consoview/M25/upload/uploader.cfm";
        private var _downloadUrl:String;
        // Filtre sur le chargement des fichiers
        private var imageFiltre:FileFilter = new FileFilter("Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg; *.jpeg; *.gif; *.png");
        private var textFiltre:FileFilter = new FileFilter("Text Files (*.txt, *.rtf ,*.pdf ,*.xls ,*.doc ,*.csv)", "*.txt; *.rtf; *.pdf; *.xls; *.doc; *.csv");
        private var allTypes:Array = new Array(imageFiltre, textFiltre);
        private var textTypes:Array = new Array(textFiltre);

        public function UploadCompImpl()
        {
            super();
        }

        /**
         * Initialisation du composant upload
         *
         */
        public function init():void
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreation_completeHandler);
        }

        public function onCreation_completeHandler(event:FlexEvent):void
        {
            this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreation_completeHandler);
            i_btnBrowse.addEventListener(MouseEvent.CLICK, i_btnBrowse_clickHandler);
            if(downloadUrl == "" || !downloadUrl)
            {
                setDisplayTo("INIT");
            }
            else
            {
                setDisplayTo("COMPLETE");
            }
            fileRef = new FileReference();
        }

        public function i_btnBrowse_clickHandler(event:MouseEvent):void
        {
            if(_isLoading)
            {
                onUpload_cancelHandler(event);
            }
            else
            {
                fileRef = new FileReference();
                fileRef.addEventListener(Event.SELECT, onFileRef_selectHandler);
                fileRef.browse(textTypes);
            }
        }

        /**
         *  A la selection d'un fichier
         *
         */
        public function onFileRef_selectHandler(event:Event):void
        {
            setDisplayTo("SELECT");
            uploadVo = new UploadVO();
            uploadVo.uploadFileRef = fileRef;
            uploadVo.docType = uploadFileType;
            uploadVo.uid = uploadUID;
            uploadFile(uploadVo);
        }

        /**
         *  Upload un fichier sur le serveur
         *  pour construire le répertoire de stockage du fichier
         *  les éléments suivants sont nécéssaires : UID du template,
         *  le type de document
         *  chemin du fichier ( STORE_PLACE/UID_template/type_DOC/nom_doc
         *
         */
        public function uploadFile(p_uploadVO:UploadVO):void
        {
            var ref:FileReference = p_uploadVO.uploadFileRef
            var params:URLVariables = new URLVariables();
            params.date = new Date();
            params.uid = uploadUID;
            params.storeUrl = getUploadUrl();
            params.doctype = p_uploadVO.docType;
            params.action = "upload";
            var request:URLRequest = new URLRequest();
            request.data = params;
            request.url = URL_BACKOFFICE + FILE_UPLOAD_URL;
            request.method = URLRequestMethod.POST;
            ref.addEventListener(ProgressEvent.PROGRESS, onUpload_progressHandler);
            ref.addEventListener(Event.COMPLETE, onUpload_completeHandler);
            ref.addEventListener(IOErrorEvent.IO_ERROR, onUpload_ioErrorHandler);
            ref.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onUpload_securityErrorHandler);
            ref.upload(request, "file", false);
            downloadUrl = getUploadUrl() + ref.name;
        }

        /**
         * Construit au préalable le chemin du document à stocker pour l'affecter
         * à la propriété path correspondantes dans le VO du template
         *
         */
        private function getUploadUrl():String
        {
            var url:String = URL_CACHE + uploadUID + "/" + uploadFileType + "/";
            return url;
        }

        /**
         * Progression du chargement du fichier
         *
         */
        public function onUpload_progressHandler(event:ProgressEvent):void
        {
            var numPerc:Number = Math.round((event.bytesLoaded / event.bytesTotal) * 100);
            percStr = numPerc.toString() + "%";
            i_progressBar.label = percStr;
            i_progressBar.setProgress(event.bytesLoaded, event.bytesTotal);
            setDisplayTo("PROGRESS");
        }

        /**
         * Demande d'annulation du chargement en cours
         *
         */
        public function onUpload_cancelHandler(event:Event):void
        {
            setDisplayTo("CANCEL");
        }

        /**
         * Chargement du fichier terminé
         *
         */
        public function onUpload_completeHandler(event:Event):void
        {
            setDisplayTo("COMPLETE");
            clearUploadListeners(event);
        }

        /**
         * Erreur d'Entrée/sorties survenu lors du chargement du fichier
         *
         */
        public function onUpload_ioErrorHandler(event:IOErrorEvent):void
        {
            clearUploadListeners(event);
            dispatchEvent(event);
        }

        /**
         * Erreur d'Entrée/sorties survenu lors du chargement du fichier
         *
         */
        public function onUpload_securityErrorHandler(event:SecurityErrorEvent):void
        {
            clearUploadListeners(event);
            dispatchEvent(event);
        }

        /**
         * Au click sur l'icone pj , chargement du fichier uploadé
         * via le chemin stocké dans <code>downloadUrl</code>
         *
         */
        public function onDownloadBtn_clickHandler(event:MouseEvent):void
        {
            if(downloadUrl != "")
            {
                navigateToURL(new URLRequest(downloadUrl));
            }
        }

        /**
         *
         * désinscrit les écouteurs de chargement du fichier
         *
         */
        private function clearUploadListeners(event:Event):void
        {
            this.removeEventListener(ProgressEvent.PROGRESS, onUpload_progressHandler);
            this.removeEventListener(Event.COMPLETE, onUpload_completeHandler);
            this.removeEventListener(IOErrorEvent.IO_ERROR, onUpload_ioErrorHandler);
            this.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onUpload_securityErrorHandler);
        }

        /**
         * Configure les élémentes IHM du composant
         * en fonction de sont état (Initialisation, chgt complet ect)
         * @param p_state
         *
         */
        private function setDisplayTo(p_state:String):void
        {
            switch(p_state)
            {
                case "INIT":
                    _isLoading = false;
                    i_progressBar.indeterminate = true;
                    i_progressBar.visible = false;
                    i_progressBar.labelPlacement = ProgressBarDirection.LEFT;
                    i_img_valid.toolTip = uploadTooltipInfo;
                    i_img_valid.visible = img_visible;
                    if(downloadUrl && downloadUrl != '')
                    {
                        i_img_valid.visible = img_visible;
                        i_img_valid.addEventListener(MouseEvent.CLICK, onDownloadBtn_clickHandler);
                        i_img_valid.toolTip = ResourceManager.getInstance().getString('M25', 'T_l_charger_le_document___l_adresse_suiv') + downloadUrl;
                    }
                    i_btnBrowse.label = ResourceManager.getInstance().getString('M25', 'Parcourir');
                    break;
                case "CANCEL":
                    _isLoading = false;
                    i_progressBar.visible = false;
                    i_inputFileName.text = "";
                    i_img_valid.toolTip = uploadTooltipInfo;
                    i_img_valid.visible = false;
                    i_btnBrowse.label = ResourceManager.getInstance().getString('M25', 'Parcourir');
                    break;
                case "COMPLETE":
                    _isLoading = false;
                    i_progressBar.visible = false;
                    i_img_valid.source = imgOk;
                    i_img_valid.visible = img_visible;
                    i_img_valid.addEventListener(MouseEvent.CLICK, onDownloadBtn_clickHandler);
                    i_img_valid.toolTip = ResourceManager.getInstance().getString('M25', 'T_l_charger_le_document___l_adresse_suiv') + downloadUrl;
                    i_btnBrowse.label = ResourceManager.getInstance().getString('M25', 'Parcourir');
                    break;
                case "SELECT":
                    _isLoading = true;
                    i_inputFileName.text = fileRef.name;
                    i_img_valid.visible = false;
                    i_progressBar.visible = true;
                    i_btnBrowse.label = ResourceManager.getInstance().getString('M25', 'Annuler');
                    break;
                case "PROGRESS":
                    i_progressBar.visible = true;
                    i_img_valid.visible = false;
                    break;
                default:
                    setDisplayTo("INIT");
                    break;
            }
        }

        public function get libelle():String
        {
            return _libelle;
        }

        /**
         * l'intitulé de l'item du formulaire ( ex Pièce jointe : )
         */
        public function set libelle(value:String):void
        {
            _libelle = value;
        }

        public function get defaultInputText():String
        {
            return _defaultInputText;
        }

        /**
         * le texte par défaut contenu dans le la label
         * nom de fichier
         */
        public function set defaultInputText(value:String):void
        {
            i_inputFileName.text = value;
            _defaultInputText = value;
        }

        public function get uploadTooltipInfo():String
        {
            return _uploadTooltipInfo;
        }

        /**
         * le contenu de l'info bulle
         */
        public function set uploadTooltipInfo(value:String):void
        {
            _uploadTooltipInfo = value;
        }

        public function get uploadUID():String
        {
            return _uploadUID;
        }

        /**
         * l'identifiant unique UID du template correspondant
         * au répertoire racine de stockage des docs associé
         * à ce template
         */
        public function set uploadUID(value:String):void
        {
            _uploadUID = value;
        }

        public function get uploadFileType():String
        {
            return _uploadFileType;
        }

        /**
         * le type de doc (ex:PRESTA_PERIFAC,LOCALISATION_ROIC ...)
         * correspondant au sous répertoire du document
         */
        public function set uploadFileType(value:String):void
        {
            _uploadFileType = value;
        }

        public function get downloadUrl():String
        {
            return _downloadUrl;
        }

        /**
         * l'url où est/sera stockée le document
         */
        public function set downloadUrl(value:String):void
        {
            _downloadUrl = value;
        }

        public function get widthField():Number
        {
            return _widthField;
        }

        /**
         * la largeur des champs de l'item
         */
        public function set widthField(value:Number):void
        {
            _widthField = value;
        }
    }
}