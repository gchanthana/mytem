<!--- Traitement des notifications de rapports (Diffusés par mail avec BIP au format HTML et en disposition INLINE) --->
<cfsetting enablecfoutputonly="TRUE" requesttimeout="3600">
<!--- UUID utilisé pour le stockage des logs si aucun UUID n'est défini dans la notification HTTP BIP --->
<cfset logUUID=createUUID()>
<cfset execute(logUUID)>

<!--- Fonctions --->
<cffunction access="private" name="execute" returntype="Numeric" hint="Traitement des notifications HTTP BIP">
	<cfargument name="logUUID" type="String" required="true" hint="UUID des logs si aucun UUID n'est défini dans la notification HTTP BIP">
	<cfset var httpCode=200>
	<cfset var responseMsg="">
	<cfset var returnCode=0>
	<cfset var logType="information">
	<!--- UUID commun pour les logs concernant les notifications de la meme diffusion --->
	<cfset var caseId=-2>
	<cfset var logStorageUUID=ARGUMENTS.logUUID>
	<cfset var logStorage=new fr.saaswedo.utils.log.LogStorage()>
	<!--- Paramètres HTTP --->
	<cfset var requestParams=getRequestParams()>
	<!--- Infos contenues dans le JOBNAME (Ne devrait pas lever d'exception) --->
	<cfset var jobNameInfos={}>
	<cfset var contextParameters={}>
	<cftry>
		<cfif compareNoCase(CGI.REQUEST_METHOD,"POST") EQ 0>
			<!--- Vérification des paramètres HTTP --->
			<cfif (NOT structKeyExists(requestParams,"JOBID")) OR
				(NOT structKeyExists(requestParams,"JOBNAME")) OR (NOT structKeyExists(requestParams,"STATUS"))>
				<cfset httpCode=400>
				<cfset responseMsg="One of the following parameter is missing: JOBID,JOBNAME,STATUS">
			<cfelse>
				<!--- Vérification des infos contenues dans le JOBNAME --->
				<cfset jobNameInfos=getJobNameInfos(requestParams.JOBNAME)>
				<cfif structKeyExists(jobNameInfos,"UUID") AND structKeyExists(jobNameInfos,"MODE") AND
					structKeyExists(jobNameInfos,"HTTP_DEST") AND structKeyExists(jobNameInfos,"LIST") AND structKeyExists(jobNameInfos,"ID")>
					<cfif isValid("UUID",jobNameInfos.UUID)>
						<!--- UUID des logs utilisé lors de la diffusion des rapports --->
						<cfset logStorageUUID=jobNameInfos.UUID>
						<!--- Infos concernant la notification BIP --->
						<cfset var jobStatus=requestParams.STATUS>
						<cfset var jobId=requestParams.JOBID>
						<cfset var jobName=requestParams.JOBNAME>
						<cfset var mode=jobNameInfos.MODE>
						<cfset var httpDest=jobNameInfos.HTTP_DEST>
						<cfset var reportListId=VAL(jobNameInfos.LIST)>
						<cfset var diffusionId=VAL(jobNameInfos.ID)>
						<cfset var endpoint=getModeEndpoint(mode)>
						<cfif diffusionId GT 0>
							<!--- L'historique du JOB est récupéré uniquement si la diffusion du rapport n'est pas en succès --->
							<cfif compareNoCase(jobStatus,"S") NEQ 0>
								<!--- Récupération de l'historique du JOB BIP (Librairie de remoting) --->
								<cfset var remoting=new fr.saaswedo.utils.remoting.SoapClient()>
								<cfset var jobHistoryInfosResult=getJobHistoryInfos(remoting,jobId,mode)>
								<!--- Librairie de remoting et résultat de la récupération de l'historique du JOB BIP --->
								<cfset contextParameters.REMOTING=remoting>
								<cfset contextParameters.REMOTING_RESULT=jobHistoryInfosResult>
								<!--- Traitement des cas d'erreurs lors de la récupération de l'historique d'un JOB BIP --->
								<cfif jobHistoryInfosResult[remoting.STATUS()] NEQ remoting.DEFINED()>
									<cfset caseId=-3>
									<cfset httpCode=400>
									<cfif jobHistoryInfosResult[remoting.STATUS()] EQ remoting.UNDEFINED()>
										<cfset httpCode=404>
										<cfset responseMsg="Job History of JOBID #jobId# is undefined (e.g DELETED)">
									<cfelseif jobHistoryInfosResult[remoting.STATUS()] EQ remoting.ERROR()>
										<cfset httpCode=500>
										<cfset responseMsg="Erreur dans le résultat de la récupération de l'historique du JOBID #jobId#">
									</cfif>
								<cfelse>
									<cfset caseId=-4>
									<cfset httpCode=502>
									<cfset responseMsg="JOBID #jobId# has error/warning status. JOBNAME: #jobName#">
								</cfif>
							</cfif>
							<!--- Procédure de mise à jour des ID de diffusion --->
							<cfset var updateIdProcName="pkg_pfgp_suivi_chargement.valide_suivi_chargement_v2">
							<cfstoredproc datasource="ROCOFFRE" procedure="#updateIdProcName#">
								<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#diffusionId#">
								<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#jobStatus#">
								<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="codeRetour">
							</cfstoredproc>
							<!--- Code d'erreur de la procédure de mise à jour des ID de diffusion --->
							<cfif codeRetour LT 0>
								<cfthrow type="Custom" message="#updateIdProcName# returned error status #codeRetour#">
							<cfelse>
								<cfif compareNoCase(jobStatus,"S") EQ 0>
									<cfset httpCode=200>
									<cfset responseMsg="[UPDATE_ID_STATUS:#codeRetour#] - " & requestParams.JOBNAME>
								<cfelse>
									<cfset responseMsg="[UPDATE_ID_STATUS:#codeRetour#] - " & responseMsg>
								</cfif>
							</cfif>
						<!--- Un ID de diffusion invalide est considéré comme une erreur (et non une exception levée) --->
						<cfelse>
							<cfset httpCode=400>
							<cfset responseMsg="La valeur de l'ID de diffusion (#diffusionId#) pour JOBID #jobId# est invalide." &
								" Elle n'a pas d'incidence sur la diffusion du rapport. Mais la MAJ de cet ID de diffusion en base ne sera pas fait">
						</cfif>
					<cfelse>
						<cfset httpCode=400>
						<cfset responseMsg="The log UUID in the JOBNAME is not a valid ColdFusion UUID">
					</cfif>
				<cfelse>
					<cfset httpCode=400>
					<cfset responseMsg="One of the following parameter is missing: UUID,MODE,HTTP_DEST,ID">
				</cfif>
			</cfif>
		<!--- Affiche le nom du backoffice suivi de son port d'écoute (MNT_DETAIL.MESSAGEID) --->
		<cfelseif (compareNoCase(CGI.REQUEST_METHOD,"GET") EQ 0) AND structIsEmpty(requestParams)>
			<cfset responseMsg="BACKOFFICE " & CGI.SERVER_NAME & ":" & CGI.SERVER_PORT>
		<cfelse>
			<cfset httpCode=400>
			<cfset responseMsg="Invalid HTTP METHOD " & CGI.REQUEST_METHOD>
		</cfif>
		<!--- Le code HTTP est différent de 200 si la liste de rapports n'est pas traitée (e.g Paramètres invalides) --->
		<cfif httpCode NEQ 200>
			<cfset logType="error">
			<cfset returnCode=-1>
			<cfset contextParameters.REL_MSG="[CaseId: #caseId#] " & responseMsg>
			<cfset storeReportListNotification(caseId,logStorage,logStorageUUID,requestParams,contextParameters)>
		</cfif>
		<!--- Réponse HTTP --->
		<cflog type="#logType#" text="[NOTIFICATION >> HTTP #httpCode#][#logStorageUUID#] #responseMsg#">
		<cfset sendHttpResponse(httpCode,serializeJSON({message=responseMsg,logUUID=logStorageUUID}))>
		<cfreturn returnCode>
		<cfcatch type="Any">
			<cfset caseId=-1>
			<!--- Notification des exceptions capturées durant l'exécution --->
			<cfset contextParameters={ERROR=CFCATCH,REL_MSG="[CaseId: #caseId#] Exception durant le traitement d'une notification BIP"}>
			<cfset storeReportListNotification(caseId,logStorage,logStorageUUID,requestParams,contextParameters)>
			<!--- Réponse HTTP en cas d'exception durant le processus --->
			<cfset var httpCode=500>
			<cfset var responseMsg="[CaseId: #caseId#] - " & CFCATCH.message>
			<cflog type="error" text="[NOTIFICATION >> HTTP #httpCode#][#logStorageUUID#] #responseMsg#">
			<cfset sendHttpResponse(httpCode,serializeJSON({message=responseMsg,logUUID=logStorageUUID}))>
			<cfreturn -1>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction access="private" name="storeReportListNotification" returntype="void" output="false" hint="Stockage des notifications durant l'exécution">
	<cfargument name="notificationCase" type="Numeric" required="true" hint="Valeur indiquant le type métier de la notification">
	<cfargument name="logging" type="fr.saaswedo.utils.log.LogStorage" required="true" hint="Librairie de stockage des logs">
	<cfargument name="logUUID" type="String" required="true" hint="UUID à utiliser pour le stockage de la notification">
	<cfargument name="requestParameters" type="Struct" required="true" hint="Paramètres HTTP">
	<cfargument name="contextParameters" type="Struct" required="true" hint="Paramètres contextuels (SOAP Request,Exception,etc...)">
	<!--- Notification --->
	<cfset var body="">
	<cfset var logStorage=ARGUMENTS.logging>
	<cfset var logStorageUUID=ARGUMENTS.logUUID>
	<cfset var caseId=ARGUMENTS.notificationCase>
	<!--- Paramètres et contexte d'execution --->
	<cfset var requestParams=ARGUMENTS.requestParameters>
	<cfset var contextParams=ARGUMENTS.contextParameters>
	<cftry>
		<!--- Liste des cas de notifications
		Cas -1 (Error) : Exception capturée durant le traitement de la liste des rapports
		Cas -2 (Error) : Erreur durant le traitement de la liste de rapports
		Cas -3 (Error) : Erreur dans le résultat de la récupération des infos du JOB BIP
		Cas -4 (Error) : Notification de rapport en erreur dans BIP
		--->
		<cfset var logType=logStorage.ERROR_LOG()>
		<cfif (caseId EQ -1) OR (caseId EQ -2) OR (caseId EQ -3) OR (caseId EQ -4)>
			<!--- BackOffice --->
			<cfset var serverName=CGI.SERVER_NAME & ":" & CGI.SERVER_PORT>
			<cfset var serverVersion=
				(structKeyExists(SERVER,"coldfusion") AND structKeyExists(SERVER.coldfusion,"productversion")) ? SERVER.coldfusion.productversion:"N.D">
			<!--- Contexte d'execution --->
			<cfset var jobStatus=structKeyExists(requestParams,"STATUS") ? requestParams.STATUS:"">
			<cfset var jobId=structKeyExists(requestParams,"JOBID") ? requestParams.JOBID:-1>
			<cfset var jobName=structKeyExists(requestParams,"JOBNAME") ? requestParams.JOBNAME:"">
			<cfset var jobNameInfos=getJobNameInfos(jobName)>
			<cfset var mode=structKeyExists(jobNameInfos,"MODE") ? jobNameInfos.MODE:"">
			<cfset var httpDest=structKeyExists(jobNameInfos,"HTTP_DEST") ? jobNameInfos.HTTP_DEST:"N.D">
			<cfset var reportListId=structKeyExists(jobNameInfos,"LIST") ? VAL(jobNameInfos.LIST):-1>
			<cfset var diffusionId=structKeyExists(jobNameInfos,"ID") ? VAL(jobNameInfos.ID):-1>
			<cfset var relatedMessage=structKeyExists(contextParams,"REL_MSG") ? contextParams.REL_MSG:"Non défini">
			<cfset var endpoint=getModeEndpoint(mode)>
			<!--- Contenu de la notification --->
			<cfset var subjectId=jobName>
			<cfsavecontent variable="body">
				<cfoutput>
					<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
					<body>
						<center><h2>Notification concernant le traitement d'une notification HTTP BI Publisher</h2></center><hr>
						<center><h3>Infos générales</h3></center>
						<b>Identifiant du cas (-1,-2,-3,-4):</b> #caseId#<br>
						<b>UUID des logs de la diffusion :</b> #logStorageUUID#<br>
						<b>Identifiant de la liste (TYPE_MAIL) :</b> #reportListId#<br>
						<b>Identifiant de la diffusion (ID) :</b> #diffusionId#<br>
						<b>MODE (API) :</b> #mode#<br>
						<b>URI API (HTTP Method:#CGI.REQUEST_METHOD#):</b> #CGI.SCRIPT_NAME#<br>
						<b>WSDL BIP :</b> #endpoint#<br>
						<b>BackOffice :</b> #serverName# (version: #serverVersion#)<hr>
						<center><h3>Infos concernant la planification du rapport</h3></center>
						<b>JOBID :</b> #jobId#<br>
						<b>JOBNAME :</b> #jobName#<br>
						<b>STATUT du rapport (F:Erreur,I:Warning,S:Succès,C:En cours d'exécution) :</b> #jobStatus#<br>
						Voir plus bas dans <i>Infos concernant la récupération de l'historique du JOB BIP</i> pour le détail<br>
						<b>Destination HTTP (BI Publisher) :</b> #httpDest#<br>
						<cfif caseId LT 0>
							<center><h3>Infos concernant l'erreur si c'est le cas</h3></center>
							<b>Remarque :</b> <i>Toute erreur concernant la procédure stockée de MAJ des ID de diffusion
							n'impacte pas la diffusion du rapport. Elle impacte uniquement les process de MAJ des diffusion en base</i><br>
							<b>Message associée à l'erreur :</b> #relatedMessage#<br>
							<cfif structKeyExists(contextParams,"ERROR")>
								<b>Exception :</b><br>
								<cfdump var="#contextParams.ERROR#" format="text"><hr>
							<cfelse>
								Non défini<hr>
							</cfif>
						</cfif>
						<b>Paramètres HTTP :</b><br>
						<cfdump var="#requestParams#">
						<cfif structKeyExists(contextParams,"REMOTING_RESULT")>
							<hr><center><h3>Infos concernant la récupération de l'historique du JOB BIP</h3></center>
							<cfset var remoting=contextParams.REMOTING>
							<cfset var remotingResult=contextParams.REMOTING_RESULT>
							<!--- La clé REMOTING est supprimée pour ne pas surcharger les dumps --->
							<cfset structDelete(contextParams,"REMOTING",FALSE)>
							<cfif remotingResult[remoting.STATUS()] EQ remoting.ERROR()>
								<b>Erreur rencontrée durant la récupération de l'historique :</b><br>
								<cfdump var="#remotingResult[remoting.RESULT()]#" format="text"><br>
							<cfelseif remotingResult[remoting.STATUS()] EQ remoting.DEFINED()>
								<cfif isArray(remotingResult[remoting.RESULT()]) AND (arrayLen(remotingResult[remoting.RESULT()]) GT 0)>
									<cfset var jobHistory=remotingResult[remoting.RESULT()][1]>
									<b>Message concernant le statut du JOB BIP :</b><br>
									#toString(jobHistory.getJobMessage())#<hr>
								<cfelse>
									L'historique du JOB est invalide (NOT a valid array OR empty array)<hr>
								</cfif>
							<cfelseif remotingResult[remoting.STATUS()] EQ remoting.UNDEFINED()>
								L'historique du JOB n'est pas présent dans le serveur BIP concerné (e.g Supprimé de l'historique)<hr>
							</cfif>
							<cfif structKeyExists(remotingResult,"S1_DURATION_MS")>
								<b>Durée du remoting (Etape S1) :</b> #(remotingResult["S1_DURATION_MS"] / 1000)# secs<br>
							</cfif>
							<cfif structKeyExists(remotingResult,"S2_DURATION_MS")>
								<b>Durée du remoting (Etape S2) :</b> #(remotingResult["S2_DURATION_MS"] / 1000)# secs<hr>
							</cfif>
							<cfif structKeyExists(remotingResult,remoting.CLIENT_REQUEST())>
								<b>Requete SOAP :</b><br>
								<cfdump var="#remotingResult[remoting.CLIENT_REQUEST()]#"><hr>
							</cfif>
							<cfif structKeyExists(remotingResult,remoting.SERVER_RESPONSE())>
								<b>Réponse SOAP :</b><br>
								<cfdump var="#remotingResult[remoting.SERVER_RESPONSE()]#"><hr>
							</cfif>
						</cfif>
					</body>
				</cfoutput>
			</cfsavecontent>
			<cfset logStorage.logIt({
				IDMNT_CAT=IDMNT_CAT(),IDMNT_ID=IDMNT_ID(),LOG_TYPE=logType,UUID=logStorageUUID,SUBJECTID=subjectId,BODY=body
			})> 
		<!--- Sinon aucune notification n'est stockée et une exception sera lancée (et stockée) --->
		<cfelse>
			<cfset subjectId="Failed to store notification. Unknown type (caseId) '#caseId#'">
			<cfthrow type="Custom" message="#subjectId#" detail="logUUID: #logStorageUUID#">
		</cfif>
		<cfcatch type="Any">
			<cfset var subjectId="Failed to store notification " & logStorageUUID>
			<cfset var relatedMessage=structKeyExists(contextParams,"REL_MSG") ? contextParams.REL_MSG:"Non défini">
			<cfsavecontent variable="body">
				<cfoutput>
					<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
					<center><h2>Echec du stockage d'une notification concernant une diffusion de rapports</h2></center>
					<b>Identifiant du cas (0,-1,-2,-3,-4) :</b> #caseId#<br>
					<b>UUID des logs de la diffusion :</b> #logStorageUUID#<br>
					<b>URI API (HTTP Method:#CGI.REQUEST_METHOD#):</b> #CGI.SCRIPT_NAME#<hr>
					<center><h3>Infos concernant l'erreur</h3></center>
					<b>Message associée à l'erreur :</b> #relatedMessage#<hr>
					<b>Exception :</b><br>
					<cfdump var="#CFCATCH#" format="text"><hr>
					<center><h3>Autres infos liées au contexte d'exécution :</h3></center>
					<b>Scope SERVER (#CGI.SERVER_NAME#) :</b><br>
					<cfdump var="#SERVER#" format="text"><hr>
					<b>Paramètres HTTP:</b><br>
					<cfdump var="#requestParams#"><hr>
					<b>Contexte fourni lors du stockage de la notification</b><br>
					<!--- La clé REMOTING est supprimée pour ne pas surcharger les dumps --->
					<cfif structKeyExists(contextParams,"REMOTING")>
						<cfset structDelete(contextParams,"REMOTING",FALSE)>
					</cfif>
					<cfdump var="#contextParams#">
				</cfoutput>
			</cfsavecontent>
			<cfset logStorage.logIt({
				IDMNT_CAT=IDMNT_CAT(),IDMNT_ID=IDMNT_ID(),LOG_TYPE=logStorage.ERROR_LOG(),UUID=logStorageUUID,SUBJECTID=subjectId,BODY=body
			})>
		</cfcatch>
	</cftry>
</cffunction>

<cffunction access="private" name="getJobHistoryInfos" returntype="Struct" hint="Planifie le rapport dans BIP pour un envoi par mail">
	<cfargument name="remotingClient" type="fr.saaswedo.utils.remoting.SoapClient" required="true" hint="Librairie de remoting">
	<cfargument name="jobId" type="Numeric" required="true" hint="JOBID">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var remoting=ARGUMENTS.remotingClient>
	<cfset var userJobId=ARGUMENTS.jobId>
	<cfset var currentMode=isValidMode(ARGUMENTS.mode) ? ARGUMENTS.mode:DEFAULT_MODE()>
	<!--- API BIP via Librairie Remoting --->
	<cfset var service={endpoint=getModeEndpoint(currentMode)}>
	<cfset var procedure="getScheduledReportHistoryInfo">
	<cfset var parameters=[
		{name="scheduledJobID",value=userJobId},
		{name="userID",value="scheduler"},{name="password",value="public"},
		{name="viewByFilter",value="All"},{name="bDownloadReport",value=javaCast("boolean",FALSE)}
	]>
	<!--- Historique du JOB BIP --->
	<cfset var startTick=getTickCount()>
	<cfset var invokeResult=remoting.invoke(service,procedure,parameters)>
	<cfset var durationS1=getTickCount() - startTick>
	<cfset invokeResult.S1_DURATION_MS=durationS1>
	<!--- [Overhead négligeable] Les données XML sont converties au format texte car elles ne s'affichent pas correctement dans certains outils --->
	<cfset startTick=getTickCount()>
	<cfif structKeyExists(invokeResult,remoting.CLIENT_REQUEST()) AND isXML(invokeResult[remoting.CLIENT_REQUEST()])>
		<cfset invokeResult[remoting.CLIENT_REQUEST()]=toString(invokeResult[remoting.CLIENT_REQUEST()])>
	</cfif>
	<cfif structKeyExists(invokeResult,remoting.SERVER_RESPONSE()) AND isXML(invokeResult[remoting.SERVER_RESPONSE()])>
		<cfset invokeResult[remoting.SERVER_RESPONSE()]=toString(invokeResult[remoting.SERVER_RESPONSE()])>
	</cfif>
	<cfset var durationS2=getTickCount() - startTick>
	<cfset invokeResult.S2_DURATION_MS=durationS2>
	<cflog type="information"
	text="[JOBID:#userJobId# / S1: #(durationS1 / 1000)# secs / S2: #(durationS2 / 1000)# secs] BI Publisher WSDL : #service.endpoint#">
	<cfreturn invokeResult>
</cffunction>

<cffunction access="private" name="getJobNameInfos" returntype="Struct" hint="Retourne une stucture contenant les infos encapsulées dans le JOBNAME">
	<cfargument name="jobName" type="String" required="true" hint="JOBNAME provenant d'une notification HTTP BIP">
	<cfset var jobNameInfos={}>
	<cfset var userJobName=ARGUMENTS.jobName>
	<cfset var items=listToArray(userJobName,"/",FALSE,FALSE)>
	<cfset var itemCount=arrayLen(items)>
	<cfset var itemInfos="">
	<cfloop index="i" from="1" to="#itemCount#">
		<cfset itemInfos=listToArray(items[i],":",FALSE,FALSE)>
		<!--- 1ère valeur = Clé, 2ème valeur = Valeur --->
		<cfif arrayLen(itemInfos) EQ 2>
			<cfset jobNameInfos[itemInfos[1]]=itemInfos[2]>
		</cfif>
	</cfloop>
	<cfreturn jobNameInfos>
</cffunction>

<cffunction access="private" name="getRequestParams" returntype="Struct" hint="Retourne une copie des paramètres HTTP">
	<cfif (compareNoCase(CGI.REQUEST_METHOD,"GET") EQ 0) OR (compareNoCase(CGI.REQUEST_METHOD,"DELETE") EQ 0)>
		<cfreturn isDefined("URL") ? DUPLICATE(URL):{}>
	<cfelseif (compareNoCase(CGI.REQUEST_METHOD,"POST") EQ 0) OR (compareNoCase(CGI.REQUEST_METHOD,"PUT") EQ 0)>
		<cfreturn isDefined("FORM") ? DUPLICATE(FORM):{}>
	<cfelse>
		<cfreturn {}>
	</cfif>
</cffunction>

<cffunction access="private" name="getHttpDestination" returntype="String" hint="Retourne la destination HTTP utilisée pour le mode">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var modeValue=isValidMode(ARGUMENTS.mode) ? ARGUMENTS.mode:DEFAULT_MODE()>
	<cfreturn UCASE(modeValue) & ".DEFAULT">
</cffunction>

<cffunction access="private" name="getModeEndpoint" returntype="String" hint="Retourne l'URL du WSDL de l'API BI Publisher pour le mode spécifié">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var modeValue=isValidMode(ARGUMENTS.mode) ? ARGUMENTS.mode:DEFAULT_MODE()>
	<!--- Liste des endpoints BIP --->
	<cfset var bipServers={
		PROD="http://bip-prod.consotel.fr/xmlpserver/services/PublicReportService_v11?wsdl",
		TEST="http://bip-int.consotel.fr/xmlpserver/services/PublicReportService_v11?wsdl",
		DEV="http://developpement.consotel.fr/xmlpserver/services/PublicReportService_v11?wsdl"
	}>
	<cfreturn bipServers[modeValue]>
</cffunction>

<cffunction access="private" name="isValidMode" returntype="Boolean" hint="Retourne TRUE si la valeur du mode est valide">
	<cfargument name="mode" type="String" required="false" default="" hint="Mode (e.g PROD)">
	<cfset var modeValue=ARGUMENTS.mode>
	<cfreturn (compareNoCase(modeValue,DEFAULT_MODE()) EQ 0) OR (compareNoCase(modeValue,"PROD") EQ 0) OR (compareNoCase(modeValue,"DEV") EQ 0)>
</cffunction>

<cffunction access="private" name="IDMNT_CAT" returntype="Numeric" hint="IDMNT_CAT">
	<cfreturn 15>
</cffunction>

<cffunction access="private" name="IDMNT_ID" returntype="Numeric" hint="IDMNT_ID">
	<cfreturn 28>
</cffunction>

<cffunction access="private" name="DEFAULT_MODE" returntype="String" hint="Mode par défaut">
	<cfreturn "TEST">
</cffunction>

<cffunction access="private" name="sendHttpResponse" returntype="void" output="true" hint="Envoi au client une réponse HTTP avec le contenu jsonContent">
	<cfargument name="httpCode" type="Numeric" required="true" hint="Code HTTP envoyé au client">
	<cfargument name="jsonContent" type="String" required="true" hint="Contenu de la réponse HTTP au format JSON">
	<cfheader statuscode="#ARGUMENTS.httpCode#">
	<cfheader name="Content-Type" value="application/json">
	<cfoutput>#TRIM(ARGUMENTS.jsonContent)#</cfoutput>
</cffunction>