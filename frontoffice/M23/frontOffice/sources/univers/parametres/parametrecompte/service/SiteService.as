package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionSiteEvent;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	import univers.parametres.parametrecompte.vo.Site;
	[Bindable]
	public class SiteService extends EventDispatcher
	{
		public var idPoolSelected : int = -1;
		public var col_site_of_pool : ArrayCollection = new ArrayCollection();
		public var col_site : ArrayCollection = new ArrayCollection();
		public var col_pool_of_site : ArrayCollection = new ArrayCollection();
		public var tab_idSite_of_pool: ArrayCollection = new ArrayCollection(); //Liste d'id site affectées au pool (ihm univers.parametres.parametrecompte.ihm.parametrePoolIHM)
		public var tab_idPool_of_site : ArrayCollection = new ArrayCollection();
		public var col_liste_pays : ArrayCollection = new ArrayCollection();
		public var col_contrainte_of_site : ArrayCollection = new ArrayCollection();
		
		public function SiteService()
		{
		}
		public function getSitePool(groupeIndex:int,idPool:int):void
		{
			
			idPoolSelected = idPool;
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getSitePool",
																		liste_site_handler,liste_site_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex,idPool);
			//RemoteObjectUtil.callService(op,groupeIndex,idPool);
		}
		public function listeContrainteOfSite(idSite:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getContrainteOfSite",
																		listeContrainteOfSite_handler,listeContrainteOfSite_fault_handler);
																						
				RemoteObjectUtil.callService(op,idSite);
				
		}
		
		private function listeContrainteOfSite_handler(evt : ResultEvent):void
		{
			col_contrainte_of_site.removeAll();
			
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
						
			while(!cursor.afterLast)
			{
				col_contrainte_of_site.addItem(cursor.current);	
				cursor.moveNext();
			}
			dispatchEvent(new GestionSiteEvent(GestionSiteEvent.LISTE_CONTRAINTE_SITE_COMPLETE));	
		}
		public function updateSiteOfPool(idPool:int,tab_idSite : Array,value : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateXSite",
																		updateSiteOfPool_handler,updateSiteOfPool_fault_handler);
			
			RemoteObjectUtil.callService(op,idPool,tab_idSite, value);
		}
		public function addSite(idGroupe : int ,libelle_site : String,ref_site : String,code_interne_site : String,adr_site:String,cp:String,commune_site : String,commentaire_site : String,idPays:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"addSite", 
																		addSite_handler,addSite_fault_handler);
			RemoteObjectUtil.callService(op,idGroupe,libelle_site,ref_site,code_interne_site,adr_site,cp,commune_site,commentaire_site,idPays);
		}
		private function addSite_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new GestionSiteEvent(GestionSiteEvent.UPDATE_INFO_SITE_COMPLETE,false,false,Number(re.result)));
			}
		}
		public function updateInfoSite(idSite : int ,libelle_site : String,ref_site : String,code_interne_site : String,adr_site : String,cp:String,commune_site : String,commentaire_site : String,idPays : int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.sites.SitesUtils",
																		"updateInfosSite", 
																		updateInfoSite_handler,updateInfoSite_fault_handler);
			RemoteObjectUtil.callService(op,idSite,libelle_site,ref_site,code_interne_site,adr_site,cp,commune_site,commentaire_site,idPays);
		}
		private function updateInfoSite_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new GestionSiteEvent(GestionSiteEvent.UPDATE_INFO_SITE_COMPLETE,false,false,Number(re.result)));
			}
		}
		public function removeSite(idSite : int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"removeSite", 
																		removeSite_handler,removeSite_fault_handler);
			RemoteObjectUtil.callService(op,idSite);
		}
		public function listeSite(idGroupe : int, search : String = null,allResult:int = 1):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getSite", 
																		listeSite_handler,listeSite_fault_handler);
			RemoteObjectUtil.callService(op,idGroupe,search,allResult);
		}
		public function getPoolSite(idGroupe : int, idSite : int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getPoolSite", 
																		getPoolSite_handler,getPoolSite_fault_handler);
			RemoteObjectUtil.callService(op,idGroupe,idSite);
			
		}
		public function updateXpoolSite(idSite:int,tab_idPool : Array,value : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateXPoolSite",
																		updateXpoolSite_handler,updateXpoolSite_fault_handler);
			
			RemoteObjectUtil.callService(op,idSite,tab_idPool, value);
		}
		public function listePays():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.revendeurs.GestionPays",
																		"get_liste_pays",
																		listePays_handler,listePays_fault_handler);
			
			RemoteObjectUtil.callService(op);
		}
		private function listePays_handler(re : ResultEvent):void
		{
			col_liste_pays = re.result as ArrayCollection;
			dispatchEvent(new GestionSiteEvent(GestionSiteEvent.LISTE_PAYS_COMPLETE));
		}
		private function updateXpoolSite_handler(re : ResultEvent):void
		{
			
		}
		private function getPoolSite_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_pool_of_site.removeAll();//Vider la collection
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : PoolGestionnaire = new PoolGestionnaire();
				item.IDPool=tmpArr[i].IDPOOL;
				item.libelle_pool=tmpArr[i].LIBELLE;
				item.isRevendeurString=tmpArr[i].CODE_INTERNE;	
				
				//item.id_profil=tmpArr[i].IDPROFIL;
				
				if(tmpArr[i].SELECTED =='1')
				{
					tab_idPool_of_site.addItem(tmpArr[i].ID);
					item.boolSelected = true;
				}							
				col_pool_of_site.addItem(item);
			}	
		}
		private function listeSite_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			
			col_site.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Site = new Site();
				item.id_site=tmpArr[i].IDSITE_PHYSIQUE;
				item.idpays_site = tmpArr[i].PAYSCONSOTELID;
				item.pays_site = tmpArr[i].PAYS
				item.adr_site=tmpArr[i].ADRESSE;
				item.code_interne_site=tmpArr[i].SP_CODE_INTERNE;
				item.commentaire_site=tmpArr[i].SP_COMMENTAIRE;
				item.commune_site=tmpArr[i].SP_COMMUNE;
				item.cp_site=tmpArr[i].SP_CODE_POSTAL;
				item.libelle_site=tmpArr[i].NOM_SITE;
				item.ref_site = tmpArr[i].SP_REFERENCE;
				
				
				col_site.addItem(item);
			}
		}
		private function removeSite_handler(re : ResultEvent):void
		{
			if(re)
			{
				dispatchEvent(new GestionSiteEvent(GestionSiteEvent.REMOVE_SITE_COMPLETE));		
			}
		}
		private function updateSiteOfPool_handler(evt : ResultEvent):void
		{
			
		}
		private function liste_site_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			
			col_site_of_pool.removeAll();//vider la collection
			tab_idSite_of_pool.removeAll();
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Site = new Site();
				item.id_site=tmpArr[i].IDSITE_PHYSIQUE;
				item.adr_site=tmpArr[i].ADRESSE;
				item.code_interne_site=tmpArr[i].SP_CODE_INTERNE;
				item.commentaire_site=tmpArr[i].SP_COMMENTAIRE;
				item.commune_site=tmpArr[i].SP_COMMUNE;
				item.cp_site=tmpArr[i].SP_CODE_POSTAL;
				item.libelle_site=tmpArr[i].NOM_SITE;
				item.ref_site = tmpArr[i].SP_REFERENCE;
				item.idpays_site = tmpArr[i].IDPAYS ;
				item.pays_site = tmpArr[i].LIBELLEPAYS ;
				if(tmpArr[i].SELECTED == '1')
				{
					item.isInPool = true;
					tab_idSite_of_pool.addItem(tmpArr[i].IDSITE_PHYSIQUE);
				}
				
				col_site_of_pool.addItem(item);
			}
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_SITE_COMPLETE));
		}
		private function liste_site_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.liste_site_fault_handler'+evt.toString());
		}
		private function updateSiteOfPool_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateSiteOfPool_fault_handler'+evt.toString());
		}
		private function addSite_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.addSite_fault_handler'+evt.toString());
		}
		private function updateInfoSite_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateInfoSite_fault_handler'+evt.toString());
		}
		private function removeSite_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.removeSite_fault_handler'+evt.toString());
		}
		private function listeSite_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeSite_fault_handler'+evt.toString());
		}
		private function getPoolSite_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeSite_fault_handler'+evt.toString());
		}
		private function updateXpoolSite_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeSite_fault_handler'+evt.toString());
		}
		private function listePays_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listePays_fault_handler'+evt.toString());
		}
		private function listeContrainteOfSite_fault_handler(evt : ResultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeContrainteOfSite_fault_handler'+evt.toString());
		}
		

	}
}