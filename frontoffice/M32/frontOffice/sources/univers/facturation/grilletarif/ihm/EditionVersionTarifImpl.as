package univers.facturation.grilletarif.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewFormatter;
	
	import flash.display.DisplayObject;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.DataGridEvent;
	import mx.managers.PopUpManager;
	
	import univers.facturation.grilletarif.entity.GroupeProduitVO;
	import univers.facturation.grilletarif.entity.OperateurVO;
	import univers.facturation.grilletarif.entity.VersionTarifVO;
	import univers.facturation.grilletarif.event.GrilleTarifEvent;
	import univers.facturation.grilletarif.services.GrilleTarifService;
	import univers.facturation.grilletarif.utils.GestionSuppressionTarif;
	
	[Bindable]
	public class EditionVersionTarifImpl extends TitleWindow
	{
		public var col_version_general : ArrayCollection = new ArrayCollection();
		public var serviceGrilleTarif : GrilleTarifService;
		public var groupeProduits : GroupeProduitVO;
		public var idRacine : int;
		public var listeVersionComposant : VersionTarifGeneralIHM;
		public var operateur_current : OperateurVO;
		public var cbPerime : CheckBox;
		public var doRefreshGridTarif: Boolean = false;
		public var modeLecture : Boolean = false;
		public var reselectExceptionIndex : int = -1;
		
		
		private var typeCreationVersion : String; // Soit exception soit versionException
		
		//UI COMPONANT
		public var boxException : VBox;
		public var popAjouterTarif : AjoutVersionTarifIHM;
		public var popEditDateFinTarif : EditionDateFinTarifIHM;
		public var dgException : DataGrid;
		public var dgVersionException : DataGrid;
		
		public var popRechercheCompte : RechercheCompteIHM;
		public var filtreTxt : TextInput;
		public var popRemove : RemoveVersionTarifIHM;
		
		public var col_version_exception_updated : ArrayCollection = new ArrayCollection();
	
		public function EditionVersionTarifImpl()
		{
		
		}
		
		/**
		 * Mise en forme des colonnes affichant des tarifs. La précision est de 4 chiffres après la virgule 
		 * pour les consos sinon elle est de 2 chiffres
		 * Le test est fait sur des libellés 'Consommations' (pour l'international il faudrat ajouter des id ex: 1 pour Abo et 2 pour Conso) 
		 * Le formatage est sensible à la localisation
		 * ex : 14 122,45 € ou $ 14,122.45 pour abo et 14 122,4545 € ou $ 14,122.4545 pour conso  
		 * @param item une réference vers la ligne du DataGrid.
		 * @param column une référence vers la colonne a mettre en forme.
		 * @return la donnée formatée.
		 * */
		public function eurosFormat(item : Object, column : DataGridColumn):String
		{	
			if(item[column.dataField])
			{
				var digit:Number = 2;
				
				//TO DO changer le test et prendre des id
				//groupeProdui est une variables global  
				if(groupeProduits && groupeProduits.typeLibelle && 
				groupeProduits.typeLibelle.toLowerCase().search('consommation') != -1)
				{
					digit = 4;	
				}
				
				return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],digit);
				
			}
			else
			{
				return "-";
			}	
		}
		
		public function creationCompleteHandler():void
		{
			
			addEventListener(GrilleTarifEvent.REMOVE_VERSION_TARIF,remove_version_tarif_handler);
			addEventListener(GrilleTarifEvent.OPEN_POP_EDITE_DATE_FIN,open_edition_tarif_handler);
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.GET_EXCEPTION_COMPLETE,getExceptionCompleteHandler);
			
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.REMOVE_VERSION_TARIF_COMPLETE,removeCompleteHandler);
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.ADD_VERSION_TARIF_COMPLETE,addVersionTarifHandler);
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.EDITE_DATE_FIN_COMPLETE,editDateFinCompleteHandler);
			
			
			serviceGrilleTarif.get_tarif_exception(idRacine,groupeProduits.id_groupe_produit);
		}
		private function open_edition_tarif_handler(vt : GrilleTarifEvent):void
		{
			if(!modeLecture)
			{
				popEditDateFinTarif = new EditionDateFinTarifIHM();
				popEditDateFinTarif.title = ResourceManager.getInstance().getString('M32', 'Edition_de_la_fin_de_validit__d_une_vers');
				popEditDateFinTarif.serviceGrilleTarif = serviceGrilleTarif;
				popEditDateFinTarif.version = dgVersionException.selectedItem as VersionTarifVO;
				PopUpManager.addPopUp(popEditDateFinTarif,this,true);
				PopUpManager.centerPopUp(popEditDateFinTarif);
			}
		}
		private function editDateFinCompleteHandler(evt: GrilleTarifEvent):void
		{
			doRefreshGridTarif = true;
			reselectExceptionIndex = dgException.selectedIndex;
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.GET_EXCEPTION_COMPLETE,getExceptionCompleteHandler);
			serviceGrilleTarif.get_tarif_exception(idRacine,groupeProduits.id_groupe_produit);
		}
		
		private function removeCompleteHandler(evt : GrilleTarifEvent):void
		{
			var obj : Object = evt.objectReturn;
			
			//Lors de la suppression d'une version, il faut mettre à jour le grid des exception tout en gardant l'exception selectionné
			if(obj.typeSuppression==GestionSuppressionTarif.VERSION_TARIF_EXCEPTION)
			{
				if(obj.removeUniqueVersion) // Si on a supprimé l'unique version, on ne reslectionne pas d'exception --> elle n'existe plus
				{
					reselectExceptionIndex =-1;	
				}
				else
				{
					reselectExceptionIndex = dgException.selectedIndex;
				}
				
			}
			else
			{
				if(obj.typeSuppression==GestionSuppressionTarif.TARIF_EXCEPTION)
				{
					//ras	
				}
			}
			//Dans tous les cas il faut actualiser les exceptions
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.GET_EXCEPTION_COMPLETE,getExceptionCompleteHandler);
			serviceGrilleTarif.get_tarif_exception(idRacine,groupeProduits.id_groupe_produit);
			
		}
		private function remove_version_tarif_handler(evt :GrilleTarifEvent):void
		{
			//CS
			removeVersionTarifGestion(GestionSuppressionTarif.VERSION_TARIF_EXCEPTION,serviceGrilleTarif.col_version_exception,dgVersionException);
		}
		public function removeException(data : VersionTarifVO):void
		{
			// Alert.show('removeException');
			removeVersionTarifGestion(GestionSuppressionTarif.TARIF_EXCEPTION,serviceGrilleTarif.col_exception_valide,dgException);
		}
		public function cbOnlyPermime():void
		{
			filtreHandler();
		}
		protected function filtreHandler(e:Event=null):void{
			serviceGrilleTarif.col_exception_valide.filterFunction= filtreGrid;
			serviceGrilleTarif.col_exception_valide.refresh();
		}
		private function filtreGrid(item:Object):Boolean{
			if (((item.libelleCompteSousCompte as String).toLowerCase()).search(filtreTxt.text.toLowerCase())!=-1)
			{
				if(cbPerime.selected)
				{
					var dtCurrent:Date = new Date();
					
					if(item.dateFin) // si elle a une date de fin
					{
						if(item.dateFin<dtCurrent) // et qu'elle est inferieur à toda
						{
							return true;
						}
						else
						{
							return false;
						}
					}
					else // si il a pas de date fin elle n'est pas périmée
					{
						return false
					}
					
				}
				else
				{
					return true;
				}
				
			}
			else
			{
				return false;	
			}
		}
		
	
		public function btExceptionHandler():void
		{
			boxException.visible =true;
			boxException.includeInLayout =true;
		}
		
		public function addVersionException():void
		{
			
			if(dgException.selectedItem)
			{
				typeCreationVersion = 'versionException';
				
				popAjouterTarif = new AjoutVersionTarifIHM();
				if((dgException.selectedItem as VersionTarifVO).cleType == 'C') // Si c'est un compte
				{
					popAjouterTarif.idCompte = (dgException.selectedItem as VersionTarifVO).idCompte;
				}
				else if((dgException.selectedItem as VersionTarifVO).cleType == 'S') // Si c'est un sous compte
				{
					popAjouterTarif.idSousCompte = (dgException.selectedItem as VersionTarifVO).idSousCompte;
				}
				popAjouterTarif.title = ResourceManager.getInstance().getString('M32', 'Ajouter_une_version_de_tarif');
				popAjouterTarif.serviceGrilleTarif = serviceGrilleTarif;
				popAjouterTarif.idRacine = idRacine;
				popAjouterTarif.groupeProduit_current = groupeProduits;
				PopUpManager.addPopUp(popAjouterTarif,this,true);
				PopUpManager.centerPopUp(popAjouterTarif);	
				doRefreshGridTarif = true;
			}
		}
		public function addException():void
		{
			typeCreationVersion = 'exception';
			popRechercheCompte = new RechercheCompteIHM();
			popRechercheCompte.groupeProduits = groupeProduits;
			serviceGrilleTarif.col_compte_sousCompte.removeAll()//vider la data
			popRechercheCompte.serviceGrilleTarif =serviceGrilleTarif;
			popRechercheCompte.idRacine = idRacine;
			popRechercheCompte.operateur_current = operateur_current;
			PopUpManager.addPopUp(popRechercheCompte,this,true);
			PopUpManager.centerPopUp(popRechercheCompte);
			doRefreshGridTarif = true;
		}

		private function removeVersionTarifGestion(typeSuppression : String,collectionTarif : ArrayCollection,datagridTarif : DataGrid):void
		{
			
				var pop : RemoveVersionTarifIHM;
				var helpRemove : GestionSuppressionTarif = new GestionSuppressionTarif();
				//retourne si il faut ouvrir un POP_CONFIRMATION ou un POP_REMOVE
				var action_requise : String = helpRemove.getMethodeSuppression(typeSuppression,collectionTarif,datagridTarif.selectedIndex,groupeProduits);
								
				if(action_requise==GestionSuppressionTarif.POP_CONFIRMATION)
				{
					pop  = helpRemove.createPopConfirmation(							
														groupeProduits,
														serviceGrilleTarif,
														idRacine,
														datagridTarif.selectedItem as VersionTarifVO,
														helpRemove.message,
														helpRemove.removeUniqueVersion,
														typeSuppression
														);
				}
				else if(action_requise==GestionSuppressionTarif.POP_REMOVE)
				{
					pop  = helpRemove.createPopRemove(							
														groupeProduits,
														serviceGrilleTarif,
														idRacine,
														datagridTarif.selectedItem as VersionTarifVO,
														collectionTarif.getItemAt(datagridTarif.selectedIndex+1) as VersionTarifVO,
														collectionTarif.getItemAt(datagridTarif.selectedIndex-1) as VersionTarifVO,
														typeSuppression
														);
				}
				doRefreshGridTarif = true;
				PopUpManager.addPopUp(pop,Application.application as DisplayObject,true);
				
			
		}
		public function fermerHandler():void
		{
			if(!listeVersionComposant.col_version_general_updated.length>0 && !col_version_exception_updated.length>0)
			{
				removeMe();
			}
			else
			{
				Alert.yesLabel = ResourceManager.getInstance().getString('M32', 'Fermer');
				Alert.cancelLabel = ResourceManager.getInstance().getString('M32', 'Annuler');
				Alert.show(ResourceManager.getInstance().getString('M32', 'Attention_aucune_modification_ne_sera_en'),"Confirmation",Alert.YES | Alert.CANCEL,this,fermerHandlerReponse);	
			}
			
		}
		private function fermerHandlerReponse(cle : CloseEvent):void{
			
			if (cle.detail == Alert.YES)
			{
				serviceGrilleTarif.get_version_general(idRacine,groupeProduits.id_groupe_produit); // remettre tt à zero;
				removeMe();
			}
			
		}
		public function valider():void
		{
			if(!listeVersionComposant.col_version_general_updated.length>0 && !col_version_exception_updated.length>0)
			{
				removeMe();
			}
			else
			{
				Alert.yesLabel = ResourceManager.getInstance().getString('M32', 'Oui');
				Alert.cancelLabel = ResourceManager.getInstance().getString('M32', 'Non');
				Alert.show(ResourceManager.getInstance().getString('M32', 'Voulez_vous_enregistrer_les_modification'),"Confirmation",Alert.YES | Alert.CANCEL,this,confirmerAjout);	
			}
		
		}
		private function confirmerAjout(cle : CloseEvent):void{
			
			if (cle.detail == Alert.YES)
			{
				serviceGrilleTarif.save_X_version_tarif_general(listeVersionComposant.col_version_general_updated);
				serviceGrilleTarif.save_X_version_tarif_general(col_version_exception_updated);
				doRefreshGridTarif = true;
				removeMe();
				
			}
			else
			{
				serviceGrilleTarif.get_version_general(idRacine,groupeProduits.id_groupe_produit); // remettre tt à zero;
				removeMe();
			}
			
	}
	private function removeMe():void
	{
		if(doRefreshGridTarif)
		{
			dispatchEvent(new GrilleTarifEvent(GrilleTarifEvent.REFRESHGRIDTARIF));
		}
		PopUpManager.removePopUp(this);
	}
	public function itemVexceptionEditEndHandler(event : DataGridEvent):void
	{
			var objV : VersionTarifVO = serviceGrilleTarif.col_version_exception.getItemAt(event.rowIndex) as VersionTarifVO;
			var size : int = col_version_exception_updated.length;
			
			var boolDejaModifie : Boolean = false;
			for(var i:int = 0;i<size;i++)
			{
				if((col_version_exception_updated.getItemAt(i) as VersionTarifVO).idVersionTarif==objV.idVersionTarif)
				{
					col_version_exception_updated.removeItemAt(i);
					col_version_exception_updated.addItemAt(objV,i);
					
					boolDejaModifie = true;
				}
			}
			if(!boolDejaModifie)
			{
				col_version_exception_updated.addItem(objV);
			}
			(dgVersionException.dataProvider as ArrayCollection).itemUpdated((dgVersionException.selectedItem as VersionTarifVO));
		}
	
	
	/***************
	 * 
	 * gestion des mises à jours suite update
	 * 
	 ************** */
		public function getVersionException():void
		{
			if(dgException.selectedItem)
			{
				var versionSelected : VersionTarifVO = dgException.selectedItem as VersionTarifVO;
				
				var typeVersion : String = "C"
				if(versionSelected.idSousCompte>0)
				{
					typeVersion = "S"
				}
				dgVersionException.selectedIndex = -1;
				serviceGrilleTarif.get_version_tarif_exception(idRacine,versionSelected.idGroupeProduit,typeVersion,versionSelected.idCompte,versionSelected.idSousCompte);
			}
		}
	
		private function getExceptionCompleteHandler(evt : GrilleTarifEvent):void
		{
			serviceGrilleTarif.removeEventListener(GrilleTarifEvent.GET_EXCEPTION_COMPLETE,getExceptionCompleteHandler);
			
			boxException.visible = serviceGrilleTarif.col_exception_valide.length>0;
			boxException.includeInLayout = serviceGrilleTarif.col_exception_valide.length>0;
			
			
			if(serviceGrilleTarif.col_exception_valide.length>0)
			{
				if(reselectExceptionIndex!=-1)
				{
					dgException.selectedIndex = reselectExceptionIndex;
					reselectExceptionIndex = -1;
				}
				else
				{
					dgException.selectedIndex=0;
				}
				getVersionException();
			}
			
		}
		private function addVersionTarifHandler(evt : GrilleTarifEvent):void
		{
			//
			doRefreshGridTarif = true;
			if(typeCreationVersion=='exception')
			{
				serviceGrilleTarif.get_tarif_exception(idRacine,groupeProduits.id_groupe_produit);
				typeCreationVersion="";
			}
			if(typeCreationVersion=='versionException')
			{
				
				reselectExceptionIndex = dgException.selectedIndex;
				serviceGrilleTarif.get_tarif_exception(idRacine,groupeProduits.id_groupe_produit);
				typeCreationVersion="";
			}
		}
	
	}	
	
}