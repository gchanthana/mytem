package univers.inventaire.inventaire.menu
{
	import composants.util.ConsoviewUtil;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.GestionDroitCycleDeVie;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.DemandeFactory;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements.AlgoSelectedEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements.RapWindowIHM;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.menu.filtre.FiltreCommande;
	import univers.inventaire.inventaire.menu.filtre.FiltreOperation;
	
	
	/**
	 * Classe Onglet 'Gestion des Operations'
	 * 
	 * */ 
	public class OperationMenu extends OperationMenuIHM 
	{
		//Reference vers le type de commande selectionne (nouvelles lignes -> 1 ou nouveaux produits -> 2)
		private var selectedDemandeType : int;
		
		//Reference vers le type de périmtre courant
		private var typePerimetre : String;		
		
		//Reference vers la methode distante ramenant une liste d'operations
		private var opListeOpe : AbstractOperation;	
		
		//Reference vers la methode distante ramenant une liste d'operations et de commandes
		private var opListeDemande : AbstractOperation;
		
		//Reference vers la methode distante rapprochant une liste de commandes
		private var opRapp : AbstractOperation;		
		
		
		//ArrayCollection contenant la liste des operations de resiliation
		[Bindable]
		private var listeOpeResi : ArrayCollection = new ArrayCollection();
		
		//ArrayCollection contenant la liste des operations de verifiaction	
		[Bindable]
		private var listeOpResiFact : ArrayCollection = new ArrayCollection();
		
		//ArrayCollection contenant la liste des commande en cours et des operations issues d'une commande
		[Bindable]
		private var listeDemandes : ArrayCollection = new ArrayCollection();
		
		[Bindable]
		private var listeOpLiees : ArrayCollection = new ArrayCollection();
		
		//Reference vers le menu du PopUpMenuButton (Type de commande)
		private var myMenu : Menu;
		
		//Reference vers l'id de l'operation ou de la commande sélectionnée dans un des tableau
		private var selectedOpId : int;
		
		//Conteur d'etape pour le remplissage des tableau
		//1 -> on rempli le tableau des commandes
		//2 -> on rempli le tableau des operations de resiliation
		//3 -> on rempli le tableau des operations de verification et de reclamation
		private var step : int = 1;
		
		[Bindable]
		private var _modeEcriture : Boolean = (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
		
		private var _boolCreerCommande:Boolean = false;
		private var _boolCreerResi:Boolean = false;
				
		/**
		 * Constructeur 
		 * */
		public function OperationMenu()
		{
			super();		
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			typePerimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;	
					
		}
		
		//Initialisation de l'ihm
		//Affectation des ecouteurs d'évenements
		private function initIHM(fe:FlexEvent):void {
			
			//filtre resi
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			
			//filtre demandes
			txtFiltreDemandes.addEventListener(Event.CHANGE,filtrerGridDemandes);	
			
			//filtre ope ctrl facture
			txtFiltreCtrlFact.addEventListener(Event.CHANGE,filtrerGridCtrlFact);
			cbFiltreLiee.addEventListener(Event.CHANGE,cbFiltreLieesClickHandler);
			cbFiltreLiee.addEventListener(Event.CHANGE,filtrerGridCtrlFact);
			
			//le chx filtre 
			cbResi.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cbResiCtrl.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cbResiEnv.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cbResiCotrlGestAvec.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cbResiCotrlGestSans.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			
			
			cbComRapp.addEventListener(Event.CHANGE,filtrerGridDemandes);
			cbComCtrl.addEventListener(Event.CHANGE,filtrerGridDemandes);
			cbComEnv.addEventListener(Event.CHANGE,filtrerGridDemandes);
			cbComLiv.addEventListener(Event.CHANGE,filtrerGridDemandes);
			cbCotrlGestAvec.addEventListener(Event.CHANGE,filtrerGridDemandes);
			cbCotrlGestSans.addEventListener(Event.CHANGE,filtrerGridDemandes);
			
			
			cbEtatEncours.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cbEtatTermine.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cbEtatAnnule.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			
			cbEtatEncours.addEventListener(Event.CHANGE,filtrerGridDemandes);
			cbEtatTermine.addEventListener(Event.CHANGE,filtrerGridDemandes);
			cbEtatAnnule.addEventListener(Event.CHANGE,filtrerGridDemandes);
						
			cbEtatEncours.addEventListener(Event.CHANGE,filtrerGridCtrlFact);
			cbEtatTermine.addEventListener(Event.CHANGE,filtrerGridCtrlFact);
			
			
			
			
			//btNouvelle ligne
			initMenu();
			popB.addEventListener(MouseEvent.CLICK,displayPanelNouvellesDemandes);
			
						
			myGridCommande.addEventListener(Event.CHANGE,displayPanelDemande);
			myGridCommande.dataProvider = listeDemandes;	
			
			btSuppDem.addEventListener(MouseEvent.CLICK,supprimerDemande);
			
			//bouton pour montrer le panel creation operation
			btnAddOpe.addEventListener(MouseEvent.CLICK,displayCreatePanel);
			
			
			myGridListeOperations.addEventListener(Event.CHANGE,displayOpePanel);
			
			DataGridColumn(myGridListeOperations.columns[0]).dataTipFunction = showInfoBulle; 
			
			//operations liéés
			myGridOpLiees.addEventListener(Event.CHANGE,displayOpePanelOpLiee);
			myGridOpLiees.addEventListener(DataGridEvent.HEADER_RELEASE,headerRelEventHandler);
			myGridOpLiees.dataProvider = listeOpResiFact;
			
			//rapprochements	
				
			imgRappAuto.addEventListener(MouseEvent.CLICK,afficherChoixRapp);
			
			
			
			imgRappAuto.enabled = _modeEcriture;
			imgRappAuto.mouseChildren = _modeEcriture;
			imgRappAuto.visible = _modeEcriture;
			btnAddOpe.enabled = _modeEcriture && gestiondroit.boolPrepareResiliation;;
			/* popB.mouseChildren = _modeEcriture && boolCreerCommande;
			popB.enabled = _modeEcriture && boolCreerCommande; */
			
			
			
			mettreAJourGridOperation();
			
		} 
		
		
		///-------------- RAPPROCHEMENT ------------------------------////
		
		//Affiche la fenetre permettant de choisir la methode de rapprochement
		private function afficherChoixRapp(me : MouseEvent):void{
			var rappWindow : RapWindowIHM = new RapWindowIHM();
			rappWindow.addEventListener(AlgoSelectedEvent.ALGO_SELECTED,lancerLeRapprochement);
			
			PopUpManager.addPopUp(rappWindow,this,true);
			PopUpManager.centerPopUp(rappWindow);
		}
		
		//Lancer le rapprochement semi-automatique des commandes selon la méthode choisi 
		private function lancerLeRapprochement(ase : AlgoSelectedEvent):void{
			rapprocher(ase.selectedAlgo);
			trace(ase.selectedAlgo);
		}
		
		///-----------FIN RAPPROCHEMENT ----------------------------------////
		
		
		//Montre le tooltip du grid des opérations de résiliation
		private function showInfoBulle(value : Object):String{
			var s : String ="Numéro opération : "+value.REFERENCE_INTERNE +
							"\nType opération : "+value.TYPE_OPERATION+
							"\nLibellé opération : "+value.LIBELLE_OPERATIONS;
			return s;
		}
				
		
		//montre le panel de création d'operation de résiliation
		private function displayCreatePanel(e:Event):void {
			parentDocument.displayCreatePanel();			
		}
		
		//------ FORMATTER-----------------------------------------------////
		
		 
		///---------FIN FORMATER--------------------------------------------///
		
		//--------------------------FILTRES ---------------------------------------------///
		
		//gere le fitre du tabelau des opérations de résiliation
		private function txtFiltreChangeHandler(ev : Event):void{
			if (myGridListeOperations.dataProvider != null){
				listeOpeResi.filterFunction = null;
				listeOpeResi.refresh();
				
				if (cbEtatTermine.selected||cbEtatAnnule.selected){						
					listeOpeResi.filterFunction = filterFuncGobla;	
				}else if (cbEtatEncours.selected){					
					listeOpeResi.filterFunction = filterFuncSFiltre;
				}else{
					listeOpeResi.filterFunction = filterFuncTxtFiltre;
				}
				listeOpeResi.refresh();										
				
			}
		}
		
		
		//change le dataprovider si on coche la checkBox operation liées
		private function cbFiltreLieesClickHandler(ev : Event):void{
			if (cbFiltreLiee.selected){
				myGridOpLiees.dataProvider = listeOpLiees;
				listeOpLiees.refresh();
			} else{
				myGridOpLiees.dataProvider = listeOpResiFact;
				listeOpResiFact.refresh();
			}
		}
		
	 	//gere le filtre du tableau des operation de verification et de reclamation liées
	 	private function filtrerGridCtrlFact(ev : Event):void{	
	 		listeOpResiFact.filterFunction = null;		
	 		listeOpResiFact.refresh();
			
 			listeOpLiees.filterFunction = null;
 			listeOpLiees.refresh();
	 		
	 		if (myGridOpLiees.dataProvider != null){	
	 			
	 			listeOpLiees.filterFunction = doFiltrerLeGridVerifFacr;
	 			listeOpLiees.refresh();
	 			 			
	 			listeOpResiFact.filterFunction = doFiltrerLeGridVerifFacr;
				listeOpResiFact.refresh();							
	 		}	
		}
		
		//gere le filtre des commandes
		private function filtrerGridDemandes(ev : Event):void{			
			if (myGridCommande.dataProvider != null){
				listeDemandes.filterFunction = null;				
				listeDemandes.refresh();
					
				if (cbEtatTermine.selected || cbEtatAnnule.selected){						
					listeDemandes.filterFunction = dofilrerLeGridDemandeGlobal;	
				}else if (cbEtatEncours.selected){					
					listeDemandes.filterFunction = dofilrerLeGridDemandeSFiltre;
				}else{
					listeDemandes.filterFunction = dofilrerLeGridDemandeTxtFiltre;
				}
				filtrerGridCtrlFact(null);
				listeDemandes.refresh();
			}
		}
		
		//Filtre les tableau des opérations de résiliation 
		//Filtre sur les états "en cours"/"terminée"
		private function filterFuncGobla(item:Object):Boolean {	
			var bool : Boolean = FiltreOperation.lettersMatch(item,txtFiltre);
			
			if (cbEtatTermine.selected && bool && FiltreOperation.isTermine(item)) return true;	
			else if (cbEtatEncours.selected && bool && FiltreOperation.isEnCours(item)) return true;	
			else if	(cbEtatAnnule.selected && bool && FiltreOperation.isAnnulee(item)) return true;	
			//else if (!cbEtatAnnule.selected && bool && !cbEtatEncours.selected && !cbEtatTermine.selected) return true;
			else return false;				 
			
			return true;
		}
		
		//Filtre le tabelau des operations de résiliation 
		//Filtre sur les etats en "En attente d'envoi"/"En attente de résiliation"/"En attente de décision de verification la facturation"  
		private function filterFuncSFiltre(item:Object):Boolean {	
			var bool : Boolean = FiltreOperation.lettersMatch(item,txtFiltre);
			
			if ((cbResiEnv.selected && bool && FiltreOperation.isEnAttenteEnvoi(item))
				&&((cbResiCotrlGestAvec.selected && cbResiCotrlGestSans.selected)	
				|| (cbResiCotrlGestAvec.selected && FiltreOperation.isCtrlBuget(item))
				|| (cbResiCotrlGestSans.selected && !FiltreOperation.isCtrlBuget(item))
				|| (!cbResiCotrlGestAvec.selected && !cbResiCotrlGestSans.selected))) 
			{
				return true;
			}
			else if (cbResi.selected && bool && FiltreOperation.isEnAttenteResi(item)) return true
			else if (cbResiCtrl.selected && bool && FiltreOperation.isEnAttenteCtrl(item)) return true
			else return false
			return true
		}
		
		//Filtre le tableau des operations de resiliation
		//Filtre sur les champs  'libelle', 'type' et 'reference interne' du tableau des operations de résiliation
		private function filterFuncTxtFiltre(item:Object):Boolean {	
			var bool : Boolean = FiltreOperation.lettersMatch(item,txtFiltre);			
			if (bool) return true;
			else return false;				 
			
			return true;
		} 
		
		//Filtre les tableaux des commandes 
		//Filtre sur les états "en cours"/"terminée"/"annulée"
		private function dofilrerLeGridDemandeGlobal(item : Object):Boolean{
			var bool : Boolean = FiltreCommande.lettersMatch(item,txtFiltreDemandes);			
			if (cbEtatTermine.selected && bool && FiltreCommande.isTermine(item)) return true;
			else if (cbEtatTermine.selected && bool && FiltreCommande.isTermine(item)) return true;	
			else if (cbEtatEncours.selected && bool && FiltreCommande.isEnCours(item)) return true;	
			else if (cbEtatEncours.selected && bool && FiltreCommande.isEnAttenteCtrl(item)) return true;	
			else if (cbEtatAnnule.selected && bool && FiltreCommande.isAnnulee(item)) return true;	
			else return false;				 
			
			return true;
		}
		
		//Filtre le tableau des commandes
		//Filtre sur les champs  'libelle', 'type' et 'reference interne' du tableau des commandes
		private function dofilrerLeGridDemandeTxtFiltre(item : Object):Boolean{
			var bool : Boolean = FiltreCommande.lettersMatch(item,txtFiltreDemandes);
			if (bool) return true;
			else return false;
			return true;
		}
		
		//Filtre les tableau des commandes 
		//Filtre sur les etats en "En attente d'envoi"/"En attente de livraison"/"En attente de décision de verification la facturation"  
		private function dofilrerLeGridDemandeSFiltre(item : Object):Boolean{
			var bool : Boolean = FiltreCommande.lettersMatch(item,txtFiltreDemandes);
			if ((cbComEnv.selected && bool && FiltreCommande.isEnAttenteEnvoi(item))
				&&((cbCotrlGestAvec.selected && cbCotrlGestSans.selected)				
				|| (cbCotrlGestAvec.selected && FiltreCommande.isCtrlBuget(item))
				|| (cbCotrlGestSans.selected && !FiltreCommande.isCtrlBuget(item))
				|| (!cbCotrlGestAvec.selected && !cbCotrlGestSans.selected)))
			{
				return true
			}
			else if (cbComLiv.selected && bool && FiltreCommande.isEnAttenteLivraison(item))
			{
				return true;
			}			
			else if (cbComCtrl.selected && bool && FiltreCommande.isEnAttenteCtrl(item)) return true;
			else if (cbComRapp.selected && bool && FiltreCommande.isEnAttenteRapprochement(item)) return true;
			else return false;
			
			return true;
			
		}
		
		//Filtre le tableau des operations de verifiaction et de reclamation
		private function doFiltrerLeGridVerifFacr(item : Object):Boolean{					
			if 	((cbEtatEncours.selected) && 				
				 (parseInt(item.EN_COURS) == 1) && 
				 (cbFiltreLiee.selected) && 				
				/*  (item.IDOPERATIONS_MAITRE == selectedOpId) &&  */
					   (/*(String(item.TYPE_OPERATION).toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase()) != -1)  ||*/
				 	   (String(item.LIBELLE_OPERATIONS).toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase()) != -1)/*||					 	   				 	   
				 	   (String(item.REFERENCE_INTERNE).toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase()) != -1)*/)) 
			{
				return (true);
			}else if ((cbEtatTermine.selected) && 
					 (parseInt(item.EN_COURS) == 0) &&
					 (cbFiltreLiee.selected) && 				
					/*  (item.IDOPERATIONS_MAITRE == selectedOpId) &&  */
					   		(/*(String(item.IDINV_TYPE_OPE).toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase()) != -1) ||*/
				 	   		(item.LIBELLE_OPERATIONS.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase())!= -1) /* ||					 	   		
				 	   		(item.REFERENCE_INTERNE.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase())!= -1)*/))
			{
				return (true);
			} else  if ((cbEtatEncours.selected) && (!cbFiltreLiee.selected) &&
					 (parseInt(item.EN_COURS) == 1) &&					
					   		(/* (item.IDINV_TYPE_OPE.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase()) != -1) || */
				 	   		(item.LIBELLE_OPERATIONS.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase())!= -1) /*||					 	   		
				 	   		(item.REFERENCE_INTERNE.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase())!= -1)*/))
			{
				return (true);
			}else if ((cbEtatTermine.selected) && (!cbFiltreLiee.selected) &&
					 (parseInt(item.EN_COURS) == 0) &&					 
					   		(/* (item.IDINV_TYPE_OPE.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase()) != -1) || */
				 	   		(item.LIBELLE_OPERATIONS.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase())!= -1)/* ||					 	   		
				 	   		(item.REFERENCE_INTERNE.toLowerCase().search(txtFiltreCtrlFact.text.toLowerCase())!= -1)*/))
			{
				return (true);
			} else return false;	   		
				 	
				
			return true; 
		}
		
		//------------------- FIN FILTRES ---------------------------------------------//
		
		//regarde si la valeur passer en param est presente dans le tableau passer en param
		//si oui retourne true si non retourne false
		private function isPresent(s:String, a : Array):Boolean{			
			var OK:Boolean = false;
			
			for (var i:int = 0; i < a.length; i++){				 
					 if ( s == a[i].toString() ) OK = true;				 
			}
			return OK;
		}
		
		
		//charge, ou bien la liste de toutes les operations de verification et de reclamation, 
		//ou bien la liste des operations liées, selon l'etat la checkbox 'filtre op liee'
		private function cbOPLieeChangeHandler(ev : Event):void{
			if (!cbFiltreLiee.selected){
				chargerOperationsVerifFact();
			}else if (selectedOpId > 0){
				chargerOperationsLiees(selectedOpId);
			}
		}
		
		//--------------------------------------
		
		//charge la liste des operation de resiliation			
		private function chargerListeOperation():void{
		
 				opListeOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListeOperationsResi",
																				listeOperationResultHandler);				
				RemoteObjectUtil.callService(opListeOpe,
											 CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);	
		}
		
		//Affecte le resultat de la methode 'chargerListeOperation' à l 'ArrayCollection 'listeOpeResi' et au tableau 'Liste des Operation de résiliation'		
		private function listeOperationResultHandler(re : ResultEvent):void{			
			listeOpeResi = re.result as ArrayCollection;				
			listeOpeResi.refresh();
			myGridListeOperations.dataProvider = listeOpeResi;					 
			txtFiltreChangeHandler(null);
		
		
			mettreAJourGridOperation();			 
		}
		
		//charge la liste des commande et des operation issues d'une commande
		private function chargerListeDemandes():void{
		
 				opListeDemande = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
																				"getList",
																				listeDemandeResultHandler);				
				RemoteObjectUtil.callService(opListeDemande,
											 CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);	
		}
		
		//Affecte le resultat de la methode 'chargerListeDemandes' à l 'ArrayCollection 'listeDemandes' et au tableau 'Liste des Commande'		
		private function listeDemandeResultHandler(re : ResultEvent):void{			
			listeDemandes = re.result as ArrayCollection;						
			listeDemandes.refresh();
			myGridCommande.dataProvider = listeDemandes;	
		 	filtrerGridDemandes(null);
			mettreAJourGridOperation();			 		 		
		}
		
		
		//charge la liste des opérations de vérification et de réclamation
		private function chargerOperationsVerifFact():void{
			opListeOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListeOperationsVerif",
																				chargerOperationsVerifFactResultHandler);				
			RemoteObjectUtil.callService(opListeOpe,
											 CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);	
			
		}
		
		//Affecte le resultat de la methode 'chargerOperationsVerifFact' à l 'ArrayCollection 'listeOpResiFact' et au tableau 'Tableau des opérations liées'		
		private function chargerOperationsVerifFactResultHandler(re : ResultEvent):void{
		
				listeOpResiFact = re.result as ArrayCollection;
				initGridVerif();								
				filtrerGridCtrlFact(null);			
				mettreAJourGridOperation();			 		
				step = 1;
		}
		
		//charge la liste des opérations de vérification et de réclamation liées à l'opération 
		//référencé par l'identifiant passé en parametre
		//param in idopMaitre l'identifiant d'une opération
		private function chargerOperationsLiees(idopMaitre : int):void{
			opListeOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListeOperationsLiees",
																				chargerOperationsLieesResultHandler);				
			RemoteObjectUtil.callService(opListeOpe,idopMaitre);	
		}
		
		//Affecte le resultat de la methode 'chargerOperationsLiees' à l 'ArrayCollection 'listeOpResiFact' et au tableau 'Tableau des opérations liées'		
		private function chargerOperationsLieesResultHandler(re : ResultEvent):void{
		
				listeOpLiees = re.result as ArrayCollection;
				listeOpLiees.refresh();
				
				initGridVerif();				
		}
		
		
		//Rapproche la liste de commande en utilisant la méthode référencée par l'identifiant passé en paramètre
		//param in typeAlgo l'identifiant de la methode de rapprochement
		private function rapprocher(typeAlgo : int):void{
		
 				opRapp = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.GestionRapprochement",
																				"rapprocherCommandes",
																				rapprocherResultHandler);				
				
				var algo : String = "libelle";
				switch(typeAlgo){
					case 1:algo = "libelle";break;
					case 2:algo = "cible";break;
					case 3:algo = "op";break;
				}
				RemoteObjectUtil.callService(opRapp,
											 CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,algo);	
		}
		
		//Une fois le rapprochement des commande du tableau  terminé , met à jour ce drenier
		private function rapprocherResultHandler(re : ResultEvent):void{
			chargerListeDemandes();
		}
		
				
		/**
		 * Rend l'onglet inactif
		 * -ne fait rien pour le moment
		 **/
		public function disablePanel():void{
			
		}
		
		/**
		 * Rend l'onglet actif 
		 * -ne fait rien pour le moment
		 **/
		public function enabledPanel():void{
			
		}
		
		/**
		 * Met à jour les différents tableaux l'un apres l'autre.
		 **/
		public function mettreAJourGridOperation():void{
			cbFiltreLiee.selected = false;
			switch (step){
				case  1 : {
					step++;
					chargerListeDemandes();												
					break;
				}
				case  2 : {
					step++;
					chargerListeOperation();										
					btnAddOpe.enabled = gestiondroit.boolPrepareResiliation && _modeEcriture;			
					
					break;
				}
				case  3 : {
					step++;					
					chargerOperationsVerifFact();		 												
					break;
				}
				case  4 : {					
					dispatchEvent(new Event("GridsOK"));
					break;
				}
			}			
		}
				
		/**
		 *  selectionne l'element référencé par l'identifiant passé en paramètre dans les différent tableau
		 *  On cherche successivement dans les tableaux 'Commandes', 'Operation de résiliation' et 'Opération de vérification'
		 * 	@param idop L'identifiant de l'operation
		 * */
		public function selectionnerGridLigne(idOp : int):void{			
			//-- TO DO ---//
			//Optimiser la recherche en selectionnant directement l'operation dans le bon tableau grace à son type
			cbEtatEncours.selected = true;			
			cbEtatTermine.selected = true;						
			cbFiltreLiee.selected = false;
			
			txtFiltreChangeHandler(null);		
		    filtrerGridCtrlFact(null);
		    filtrerGridDemandes(null);
		    
		    listeOpeResi.filterFunction = null;
		    listeOpeResi.refresh();
		    
		    listeOpResiFact.filterFunction = null;
		    listeOpResiFact.refresh(); 
		    
		    listeDemandes.filterFunction = null;
		    listeDemandes.refresh(); 
		    
		   
		    
			//1 pour resi 2 pour creation
			myGridCommande.selectedIndex = -1;
			myGridListeOperations.selectedIndex = -1;
			myGridOpLiees.selectedIndex = -1;
			
			var index : int = ConsoviewUtil.getIndexById(listeDemandes,"IDINV_OPERATIONS",idOp);			
			if (index != -1){					
				myGridCommande.selectedIndex = index;
				myGridCommande.scrollToIndex(index);								
			}else {
				
				index = ConsoviewUtil.getIndexById(listeOpeResi,"IDINV_OPERATIONS",idOp);		
				if (index != -1){				
					myGridListeOperations.selectedIndex = index;
					myGridListeOperations.scrollToIndex(index);							
					
				}else{					
					index = ConsoviewUtil.getIndexById(listeOpResiFact,"IDINV_OPERATIONS",idOp);		
					myGridOpLiees.selectedIndex = index;
					myGridOpLiees.scrollToIndex(index);
				}
			}
			
		}
		
		
		/**
		 *  Fait les actrions nécessaire au changement de périmetre
		 * */
		public function onPerimetreChange():void{
			_modeEcriture = (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
			imgRappAuto.enabled = _modeEcriture;
			imgRappAuto.mouseChildren = _modeEcriture;
			imgRappAuto.visible = _modeEcriture;
			btnAddOpe.enabled = gestiondroit.boolPrepareResiliation && _modeEcriture;
			popB.mouseChildren = gestiondroit.boolPrepareCreation && _modeEcriture;
			popB.enabled = gestiondroit.boolPrepareCreation && _modeEcriture;;
			
			
			mettreAJourGridOperation();
		}
		
		/**
		 * Montre l'Operation de résiliatin, la Commande ou l'operation de verification suivant la variable globale selectedOpId
		 * deselectionne les lignes des autres tableaux
		 * */
		public function displayOpePanel(e:Event):Boolean {
			
			if (myGridListeOperations.selectedIndex != -1 ){
				//montre les operations liées
				if (myGridCommande.selectedIndex != -1)myGridCommande.selectedIndex =-1;
				if (myGridOpLiees.selectedIndex != -1)myGridOpLiees.selectedIndex =-1;
				
				selectedOpId = myGridListeOperations.selectedItem.IDINV_OPERATIONS;
								
				/// Montrer le Panel Operation
				var eventObj : OperationMenuEvent = new OperationMenuEvent();				
				eventObj.type_Operation = myGridListeOperations.selectedItem.IDINV_TYPE_OPE;
				eventObj.numero_Operation = myGridListeOperations.selectedItem.REFERENCE_INTERNE;
				eventObj.id_Operation = myGridListeOperations.selectedItem.IDINV_OPERATIONS;
				eventObj.op_En_Cours = myGridListeOperations.selectedItem.EN_COURS;
				dispatchEvent(eventObj);	
				lblError.text = "";
				filtrerGridCtrlFact(null);
				chargerOperationsLiees(myGridListeOperations.selectedItem.IDINV_OPERATIONS);
				return true;
			}else if (myGridCommande.selectedIndex != -1){				
					displayPanelDemande(null);
					return true;
			}else if (myGridOpLiees.selectedIndex != -1){				
					displayOpePanelOpLiee(null);
					return true;
				
			}else{
				return false;	
			}
						
			
		}
		
		
		//Montre l'operation de vérification ou de réclamation apres un click sur le tableau des operations liées
		private function displayOpePanelOpLiee(e:Event):void {
			
			if (myGridOpLiees.selectedIndex != -1 ){
				//montre les operations liées
			
								
				/// Montrer le Panel Operation
				var eventObj : OperationMenuEvent = new OperationMenuEvent();				
				eventObj.type_Operation = myGridOpLiees.selectedItem.IDINV_TYPE_OPE;
				eventObj.numero_Operation = myGridOpLiees.selectedItem.REFERENCE_INTERNE;
				eventObj.id_Operation = myGridOpLiees.selectedItem.IDINV_OPERATIONS;
				eventObj.op_En_Cours = myGridOpLiees.selectedItem.EN_COURS;
				dispatchEvent(eventObj);	
				lblError.text = "";
			}else{				
				lblError.text = "Selectionnez une operation";
			}
		}
		
		
		//Initialise le menu du PopUpMenuButton 'Type de Commande'
		private function initMenu():void {
			myMenu = new Menu();
			var dp:Array = DemandeFactory.getTypesDemandes();
			myMenu.dataProvider = dp;
			myMenu.selectedIndex = 0;			
			  
			myMenu.addEventListener("itemClick", itemClickHandler);
			
			popB.popUp = myMenu;
			popB.label = myMenu.dataProvider[myMenu.selectedIndex].label;
			selectedDemandeType =  myMenu.dataProvider[myMenu.selectedIndex].code;     
		}
		

		//Handler du clique sur le menu du PopUpMenuButton
		private function itemClickHandler(event:MenuEvent):void {
			var label:String = event.item.label;        			
			popB.label = label;
			popB.close();
			myMenu.selectedIndex = event.index;
			selectedDemandeType = event.item.code;
		}
		
		//Dispatche un evenement OperationMenuEvent de type SHOW_NOUVELLES_DEMANDE
		//Afin d'afficher le bon formualire de saisie de commande
		private function displayPanelNouvellesDemandes(ev :Event):void{						
			var evtObj : OperationMenuEvent = new OperationMenuEvent(OperationMenuEvent.SHOW_NOUVELLES_DEMANDE);
			evtObj.id_Operation = 0;
			evtObj.type_Operation =  selectedDemandeType;
			dispatchEvent(evtObj);	
		}
		
		
		//Dispatche un evenement OperationMenuEvent afin d'afficher l'operation ou la commande sélectionnée				
		private function displayPanelDemande(ev : Event):void{			
			if (myGridCommande.selectedIndex != -1){
				
				if (myGridListeOperations.selectedIndex != -1)myGridListeOperations.selectedIndex =-1;
				if (myGridOpLiees.selectedIndex != -1) myGridOpLiees.selectedIndex =-1;
				
				selectedOpId = myGridCommande.selectedItem.IDINV_OPERATIONS;								
				/// Montrer le Panel Operation
				
				var eventObj : OperationMenuEvent = new OperationMenuEvent();				
				eventObj.type_Operation = myGridCommande.selectedItem.IDINV_TYPE_OPE;
				eventObj.numero_Operation = myGridCommande.selectedItem.REFERENCE_INTERNE;
				eventObj.id_Operation = myGridCommande.selectedItem.IDINV_OPERATIONS;
				eventObj.op_En_Cours = myGridCommande.selectedItem.EN_COURS;
				eventObj.infosDemende = myGridCommande.selectedItem;
				dispatchEvent(eventObj);	
				lblError.text = "";
					
				filtrerGridCtrlFact(null);
				chargerOperationsLiees(myGridCommande.selectedItem.IDINV_OPERATIONS);
			}
			
		}
		
		//---- NON UTILISE POUR LE MOMENT --------------------------//
		
		
		//Met à jour le tableau des commandes
		private function rafraichirListeDemandes(ev : Event):void{
			chargerListeDemandes();
			var evtObj : OperationMenuEvent = new OperationMenuEvent(OperationMenuEvent.DEMANDE_DELETED);	
			evtObj.isDemande = true;		
			dispatchEvent(evtObj);
		}
		
		//Supprime la commande sélectionnée
		private function supprimerDemande(me : MouseEvent):void{
			if (myGridCommande.selectedIndex != -1){
				var commande : Commande = new Commande();
				commande.addEventListener(Commande.DELETE_COMPLETE,rafraichirListeDemandes);
				commande.addEventListener(Commande.DELETE_ERROR,supprimerCommandeErreur);				
				commande.deleteCommande(myGridCommande.selectedItem.IDCDE_COMMANDE);
			}
		}
		
		//Affiche un message d'erreur si la suppression d'une commande ne s'est pas bien passée
		private function supprimerCommandeErreur(ev : Event):void{
			Alert.show("Une erreur s'est produite lors de la suppression");
		}
		
		///// ------------------------- SORTING THE GRID OPERATION_MENU BY LIBELLE --------------------------------//// 
		
		
		//Référence sur un style de trie
		[Bindable]
		private var sortA : Sort;
		
		//Référence sur la colonne à filtrer
		private var sortByLibelle : SortField;
		
		//Initialisation du tableau des opérations de vérification de facture et de réclamation pour le trie
		private function initGridVerif():void{
			sortA = new Sort();
			sortByLibelle = new SortField("LIBELLE_OPERATIONS");
			
			sortByLibelle.descending = true;
			
			sortA.fields = [sortByLibelle];
			
			
			
			
			if (cbFiltreLiee.selected){
				listeOpLiees.sort = sortA;
				listeOpLiees.refresh();	
									
				myGridOpLiees.dataProvider = listeOpLiees;
				myGridOpLiees.rowCount = listeOpLiees.length + 1;
				
			}else{
				listeOpResiFact.sort = sortA;
				listeOpResiFact.refresh();

				myGridOpLiees.dataProvider = listeOpResiFact;
				myGridOpLiees.rowCount = listeOpResiFact.length + 1;
				
			}
			
			 
			
		}
		
		//Handler du click sur l'en-tête du tableau des opération de vérification
		//Pour inverser le trie
		private function headerRelEventHandler(de : DataGridEvent):void{			
			
			if(de.columnIndex == 0){
				sortByLibelle.reverse();
				sortA.fields[0] = sortByLibelle;
				
			}
			
			if (cbFiltreLiee.selected){
				listeOpLiees.sort = sortA;
				listeOpLiees.refresh();	
			}else{
				listeOpResiFact.sort = sortA;
				listeOpResiFact.refresh();	
			}
			
			de.preventDefault();
		}
		
		[Bindable]
		public function set boolCreerCommande(value:Boolean):void
		{
			if(value != _boolCreerCommande)
			{
				_boolCreerCommande = value;
				invalidateProperties();
				invalidateSize();
				invalidateDisplayList();	
			}
		}

		public function get boolCreerCommande():Boolean
		{
			return _boolCreerCommande;
		}
		
		[Bindable]
		public function set boolCreerResi(value:Boolean):void
		{
			if(value != _boolCreerResi)
			{
				_boolCreerResi = value;
				invalidateProperties();
				invalidateSize();
				invalidateDisplayList();	
			}
		}

		public function get boolCreerResi():Boolean
		{
			return _boolCreerResi;
		}
		
		
		
		
		override protected function commitProperties():void
		{
			if(btnAddOpe && gestiondroit)
			{
				btnAddOpe.enabled = gestiondroit.boolPrepareResiliation;
			}
			
			if(popB && gestiondroit)
			{
				popB.mouseChildren = gestiondroit.boolPrepareCreation;
				popB.enabled = gestiondroit.boolPrepareCreation;
			}
		} 
		
		
		private var _gestiondroit:GestionDroitCycleDeVie;
		[Bindable]
		public function set gestiondroit(value:GestionDroitCycleDeVie):void
		{
			if(value && value != _gestiondroit)
			{
				_gestiondroit = value;
				_gestiondroit.addEventListener("initActionComplete",function (event:Event):void
																	{	
																		invalidateProperties();
																		invalidateSize();
																		invalidateDisplayList();
																	})
					
			}	
		}

		public function get gestiondroit():GestionDroitCycleDeVie
		{
			return _gestiondroit;
		}
	}
}