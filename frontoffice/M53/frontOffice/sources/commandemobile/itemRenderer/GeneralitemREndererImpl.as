package commandemobile.itemRenderer
{
	import commandemobile.entity.Commande;
	import commandemobile.system.GestionTransporteur;
	
	import composants.util.DateFunction;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.ComboBox;
	
	[Bindable]
	public class GeneralitemREndererImpl extends Box
	{
		//COMPOSANTS
		public var cbxTransporteur				:ComboBox;
		
		
		//OBJETS
		public var _cmd							:Commande;
		private var gestionTrans				:GestionTransporteur 	= new GestionTransporteur();
		
		//VARIABLES LOCALES
		public var periodeObject				:Object 				= {rangeStart:new Date(new Date().getTime() - (1 * DateFunction.millisecondsPerDay) )};
		public var listeTransporteurs			:ArrayCollection 		= new ArrayCollection();
		
		//CONSTRUCTEUR (>LISTNER)
		public function GeneralitemREndererImpl()
		{
			gestionTrans.fournirListeTransporteurs();
			gestionTrans.addEventListener("transporteursArrived",	transporteursArrivedHandler);
			addEventListener("getProcedure",						transporteursArrivedHandler);
		}
		
		private function transporteursArrivedHandler(e:Event):void
		{
			var bool:Boolean = false;
			if(_cmd != null)
			{
				listeTransporteurs = gestionTrans.listeTransporteurs;
				for(var i:int = 0;i < listeTransporteurs.length;i++)
				{
					if(listeTransporteurs[i].IDTRANSPORTEUR == _cmd.IDTRANSPORTEUR)
					{
						if(listeTransporteurs[i].IDTRANSPORTEUR != 0)
						{
							listeTransporteurs.addItemAt(listeTransporteurs[i],0);
							listeTransporteurs.removeItemAt(i+1);
							bool = true;
						}
					}
				}
				if(!bool)
				{
					cbxTransporteur.prompt = "Sélectionnez";
				}
				cbxTransporteur.dataProvider = listeTransporteurs;
			}
		}
		
		protected function cboxCloseHandler():void
		{
			if(cbxTransporteur.selectedItem != null)
			{
				_cmd.IDTRANSPORTEUR			= cbxTransporteur.selectedItem.IDTRANSPORTEUR;
				_cmd.LIBELLE_TRANSPORTEUR	= cbxTransporteur.selectedItem.LIBELLE_TRANSPORTEUR;
			}
		}

	}
}