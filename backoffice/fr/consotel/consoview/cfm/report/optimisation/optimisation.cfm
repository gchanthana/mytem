<cfif isDefined("FORM")>
	<!--- =================== PARAMS ============================== --->
	<!---
	<cfset SUPPORT_EMAIL="production@consotel.fr">
	<cfset SERVICE_IT_EMAIL="it@consotel.fr">
	 --->
	
	<cfset SUPPORT_EMAIL="production@consotel.fr">
	<cfset SERVICE_IT_EMAIL="dev@consotel.fr">
	
	<cfset UTILISATEUR = SESSION.USER.EMAIL>
	
	<!--- LE PERIMETRE --->
	<cfset TYPE_DE_RAPPORT = ucase(FORM.REPORT_TYPE)>
	<cfset TYPE_DE_PERIMETRE = ucase(form.TYPE_PERIMETRE)>
	<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
	
	<!--- LES RAPPORTS --->
	<cfset structRapport=StructNew()>
	<cfset structRapport.ETATCONSOGSM = "Etat Des Consommations Par Familles">
	<cfset structRapport.INVENTAIRELIGNESGLOBAL = "Inventaire des lignes">
	<cfset structRapport.INVENTAIREPRODUITSGLOBAL = "Inventaire des produits">
	
	<!--- LES DOSSIER --->
	<cfset TEMP_DIR = "/opt/webroot/intranetTmp">
	<cfset FTP_SERVER = "cluster-web-2">
	<cfset FTP_LOGIN = "cv">
	<cfset FTP_PORT = "21">
	<cfset FTP_MDP = "cv9*">
	<cfset MAIL_SERVER = "192.168.3.119:26">
</cfif>

<!---
	Codification des noms des rapports dans Optimisation (Nom du rapport - REPORT_TYPE) :
	
	Optimisation :
	Rapport de Déploiement - Deploiement - Groupe, GroupeLigne
	Rapport de Rationalisation - Rationalisation - Groupe, GroupeLigne
	Lignes sans consommations - NdiSansConso - Groupe, GroupeLigne
	
	Rapports :
	Etat Des Consommations Par Familles - EtatConsoGsm (Seulement pour noeud Organisation)
	Consos Des Lignes Par Organisations (Thèmes) - ConsoNdiByNode (Seulement pour noeud Organisation)
	Consos Des Lignes Par Organisations (SurThèmes) - ConsoNdiByNodeSurTheme (Seulement pour noeud Organisation)
	Consos Par Organisations (Thèmes) - ConsoByNode (Seulement pour noeud Organisation)
	Consos Par Organisations (SurThèmes) - ConsoByNodeSurTheme (Seulement pour noeud Organisation)
	Répartition et évolution des communications - RepartEvoGlobal - Groupe, GroupeLigne
	Détail des appels - DetailsConsoGlobal - Groupe, GroupeLigne
	Inventaire des lignes - InventaireLignesGlobal - Groupe, GroupeLigne
	Inventaire des produits - InventaireProduitsGlobal - Groupe, GroupeLigne
	Rapport Téléphonie Fixe Global - DetailFilaireSynthese - Groupe, GroupeLigne
	Rapport Téléphonie Fixe Par Sociétés - DetailFilaire - Groupe, GroupeLigne
	Spécifique GSM - DetailGsm - Groupe, GroupeLigne
	Extract GSM par Thème - CoutGsmThemeParLigne - Groupe, GroupeLigne
	Extract GSM par Catégorie - CoutGsmParLigne - Groupe, GroupeLigne
	Spécifique Data - DetailData - Groupe, GroupeLigne
--->

<cfif UCase(FORM.REPORT_TYPE) EQ "DEBUG">
	<!--- 
	<cfinclude template="a-supprimer.cfm">
	 --->
<cfelseif UCase(FORM.REPORT_TYPE) EQ "ETATCONSOGSM">
<!--- Instanciation et Initialisation du Service --->
<cfset developerReportService=createObject("component","fr.consotel.api.ibis.publisher.service.developer.DeveloperReportService")>
<!--- Initialisation du Service avec les paramètres obligatoires : IDRACINE, XDO BI Publisher --->
<cfset initStatus=developerReportService.init(SESSION.PERIMETRE.ID_GROUPE,
			"/consoview/facturation/rapport/EtatConsoGsm/EtatConsoGsm.xdo","ConsoView DEV","CONSOTEL","MODULE RAPPORT-CONTAINER")>
<!--- Passages des paramètres avec leur(s) valeur(s) --->
<cfset valeursPourIdRacine=arrayNew(1)>
<cfset valeursPourIdRacine[1]=SESSION.PERIMETRE.ID_GROUPE>
<cfset setIdRacine=developerReportService.setParameter("P_IDRACINE",valeursPourIdRacine)>

<cfset valeursPourIdPerimetre=arrayNew(1)>
<cfset valeursPourIdPerimetre[1]=form.perimetre_index>
<cfset setIdPerimetre=developerReportService.setParameter("S_IDPERIMETRE",valeursPourIdPerimetre,"CLIENT")>

<cfset valeursPourDatedeb=arrayNew(1)>
<cfset valeursPourDatedeb[1]=form.datedeb & "/01">
<cfset setDatedeb=developerReportService.setParameter("Q_SHORT_DEB",valeursPourDatedeb,"CLIENT")>

<cfset valeursPourDatefin=arrayNew(1)>
<cfquery name="getDate" datasource="#SESSION.OFFREDSN#">
	SELECT to_char(last_day(to_date('#form.datefin#','yyyy/mm')),'yyyy/mm/dd') ladate FROM dual
</cfquery>
<cfset valeursPourDatefin[1]=getDate.ladate>
<cfset setDatefin=developerReportService.setParameter("Q_SHORT_FIN",valeursPourDatefin,"CLIENT")>

<cfset valeursPourUserId=arrayNew(1)>
<cfset valeursPourUserId[1]=SESSION.USER.CLIENTACCESSID>
<cfset setUserId=developerReportService.setParameter("Q_USERID",valeursPourUserId)>
<!--- Mise à jour des paramètres de restitution --->
<cfset filename="Etat Des Consommations Par Familles - " & lsDateFormat(now(),"MM-YYYY")>
<cfset extension="NULL">
<cfif UCase(FORM.FORMAT) EQ "EXCEL">
	<cfset extension="xls">
<cfelseif UCase(FORM.FORMAT) EQ "PDF">
	<cfset extension="pdf">
<cfelseif UCase(FORM.FORMAT) EQ "CSV">
	<cfset extension="csv">
<cfelse>
	<cfset extension="data">
</cfif>
<cfset setOutputStatus=developerReportService.setOutput("cv",TRIM(LCASE(FORM.FORMAT)),extension,"Etat Consos Familles","JOB - REPORT TEST")>
<cftry>
	<cfset JOB_ID=developerReportService.runTask(SESSION.USER.CLIENTACCESSID)>
	<cfmail from="consoview@consotel.fr"
				to="#SUPPORT_EMAIL#"
				bcc="#SERVICE_IT_EMAIL#"
				subject="Demande de rapport : Etat des consommations par familles par #SESSION.USER.EMAIL# (BackOffice : #CGI.SERVER_NAME#)"
				type="html"
				charset="utf-8"
				wraptext="72"
				failto="#SUPPORT_EMAIL#"
				server="#MAIL_SERVER#">
		Une demande du rapport Etat des consommations par familles a été effectuée par #SESSION.USER.EMAIL# (Connecté depuis #SESSION.PERIMETRE.RAISON_SOCIALE#).<br>
		Le rapport généré sera accessible à l'emplacement : <strong><u>ftp://pelican.consotel.fr/</u></strong><br />
		Le nom de la tâche : #filename#<br />
		JOB ID : #JOB_ID#<br />
		Le nom du fichier : #filename#.#extension#<br />
		<a href="http://xmlpserver-1.consotel.fr/CONSOTEL/Rapports+en+planification/Rapports+en+planification.xdo?Q_JOB_NAME=#filename#&_xf=html">Cliquer pour suivre la génération du rapport</a><br /><br />
		<strong><u>Paramètres :</u></strong><br />
		<strong>S_IDPERIMETRE :</strong> #form.perimetre_index#<br />
		<strong>Q_SHORT_DEB :</strong> #form.datedeb & "/01"#<br />
		<strong>Q_SHORT_FIN :</strong> #getDate.ladate#<br />
		<strong>Q_USERID :</strong> #SESSION.USER.CLIENTACCESSID#<br /><br />
		Cordialement,<br />
		Le support BI.
	</cfmail>
	<cfoutput>
		<img  src="Consoview.GIF">
		<div id="contenu" width="100%" align="left">
			<br/><br/><br/><br/><br/>
			<font style="font-family: 'Arial Black', Arial, Verdana, serif;">
			<p>Votre rapport va être généré. Il vous sera envoyé par mail.
			Si la taille de votre rapport est trop importante, l'adresse de récupération ainsi que la procédure 
			de récupération du fichier sur l'extranet CONSOTEL vous sera communiquée.<br/>
			</p><br/>Merci,<br/><br/>
			L'équipe support ConsoView.
			<br/><br/>
		<div>
		<div width="100%" align="center">
			<button type="button" onclick="JavaScript:window.close()">Fermer</button>
		</div>
	 </cfoutput>
	<cfcatch type="any">
		<cfmail from="consoview@consotel.fr"
					to="#SERVICE_IT_EMAIL#"
					subject="EXCEPTION: Etat des consommations par familles par #SESSION.USER.EMAIL# (BackOffice : #CGI.SERVER_NAME#)"
					type="html"
					charset="utf-8"
					wraptext="72"
					server="#MAIL_SERVER#">
			<cfdump var="#developerReportService.getInitInfos()#" label="DUMP INIT INFOS"><br />
			<cfdump var="#developerReportService.getOutputInfos()#" label="DUMP OUTPUT INFOS"><br />
			<cfdump var="#developerReportService.getParametersInfos()#" label="DUMP PARAMETERS INFOS"><br />
			DUMP XML_TASK :<br />
			<cfdump var="#developerReportService.getDebug('XML_TASK').TASK#"><br />
			<cfdump var="#CFCATCH#" label="DUMP CFCATCH">
		</cfmail>
		<cfoutput>
			<img  src="Consoview.GIF">
			<div id="contenu" width="100%" align="left">
				<br/><br/><br/><br/><br/>
				<font style="font-family: 'Arial Black', Arial, Verdana, serif;">
				<p>Une erreur s'est produite durant la génération du rapport. Un ticket d'incident a été envoyé au support technique.<br />
				L'équipe support ConsoView.
				<br/><br/>
			<div>
			<div width="100%" align="center">
				<button type="button" onclick="JavaScript:window.close()">Fermer</button>
			</div>
		</cfoutput>
	</cfcatch>
</cftry>
<!--- ************************************ 
	<cfset obj=createObject("component","fr.consotel.consoview.rapports.EtatConsoGsm.RapportEtatConsoGsmStrategy")>
	<cfset xmlTask=obj.getDefaultXmlReportTask()>
	<cfset P_IDRACINE_XPATH="/TASK/PARAMETERS/PARAMETER[child::NAME='P_IDRACINE']">
	<cfset P_IDRACINE_ARR=xmlSearch(xmlTask,P_IDRACINE_XPATH)>
	<cfset P_IDRACINE_ARR[1]["VALUE"].xmlText=SESSION.PERIMETRE.ID_GROUPE>
	
	<cfset S_IDPERIMETRE_XPATH="/TASK/PARAMETERS/PARAMETER[child::NAME='S_IDPERIMETRE']">
	<cfset S_IDPERIMETRE_ARR=xmlSearch(xmlTask,S_IDPERIMETRE_XPATH)>
	<cfset S_IDPERIMETRE_ARR[1]["VALUE"].xmlText=form.perimetre_index>
	
	<cfset Q_SHORT_DEB_XPATH="/TASK/PARAMETERS/PARAMETER[child::NAME='Q_SHORT_DEB']">
	<cfset Q_SHORT_DEB_ARR=xmlSearch(xmlTask,Q_SHORT_DEB_XPATH)>
	<cfset Q_SHORT_DEB_ARR[1]["VALUE"].xmlText=form.datedeb & "/01">
	
	<cfset Q_SHORT_FIN_XPATH="/TASK/PARAMETERS/PARAMETER[child::NAME='Q_SHORT_FIN']">
	<cfset Q_SHORT_FIN_ARR=xmlSearch(xmlTask,Q_SHORT_FIN_XPATH)>
	<cfset Q_SHORT_FIN_ARR[1]["VALUE"].xmlText=getDate.ladate>
	
	<cfset Q_USERID_XPATH="/TASK/PARAMETERS/PARAMETER[child::NAME='Q_USERID']">
	<cfset Q_USERID_ARR=xmlSearch(xmlTask,Q_USERID_XPATH)>
	<cfset Q_USERID_ARR[1]["VALUE"].xmlText=SESSION.USER.CLIENTACCESSID>
	<cfset extension="NULL">
	<cfif UCase(FORM.FORMAT) EQ "EXCEL">
		<cfset extension="xls">
	<cfelseif UCase(FORM.FORMAT) EQ "PDF">
		<cfset extension="pdf">
	<cfelseif UCase(FORM.FORMAT) EQ "CSV">
		<cfset extension="csv">
	<cfelse>
		<cfset extension="data">
	</cfif>
	<cfset filename="Etat des consommations par familles - " & lsDateFormat(now(),"MM-YYYY")>
	<cfset xmlTask["TASK"]["OUTPUT_NAME"].xmlText=filename>
	<cfset xmlTask["TASK"]["OUTPUT_FORMAT"].xmlText=UCase(FORM.FORMAT)>
	<cfset xmlTask["TASK"]["OUTPUT_EXT"].xmlText=extension>

	<cfset JOB_ID=obj.run(SESSION.USER.CLIENTACCESSID,0,xmlTask)>
	

	<cfmail from="consoview@consotel.fr"
				to="#SUPPORT_EMAIL#"
				bcc="#SERVICE_IT_EMAIL#"
				subject="Demande de rapport : Etat des consommations par familles par #SESSION.USER.EMAIL# (BackOffice : #CGI.SERVER_NAME#)"
				type="html"
				charset="utf-8"
				wraptext="72"
				failto="#SUPPORT_EMAIL#"
				server="#MAIL_SERVER#">
		Une demande du rapport Etat des consommations par familles a été effectuée par #SESSION.USER.EMAIL# (Connecté depuis #SESSION.PERIMETRE.RAISON_SOCIALE#).<br>
		Le rapport généré sera accessible à l'emplacement : <strong><u>ftp://pelican.consotel.fr/</u></strong><br />
		Le nom de la tâche : #filename#<br />
		JOB ID : #JOB_ID#<br />
		Le nom du fichier : #filename#.#extension#<br />
		<a href="http://xmlpserver-1.consotel.fr/CONSOTEL/Rapports+en+planification/Rapports+en+planification.xdo?Q_JOB_NAME=#filename#&_xf=html">Cliquer pour suivre la génération du rapport</a><br /><br />
		<strong><u>Paramètres :</u></strong><br />
		<strong>S_IDPERIMETRE :</strong> #form.perimetre_index#<br />
		<strong>Q_SHORT_DEB :</strong> #form.datedeb & "/01"#<br />
		<strong>Q_SHORT_FIN :</strong> #getDate.ladate#<br />
		<strong>Q_USERID :</strong> #SESSION.USER.CLIENTACCESSID#<br /><br />
		Cordialement,<br />
		Le support BI.
	</cfmail>
	<cfoutput>
		<img  src="Consoview.GIF">
		<div id="contenu" width="100%" align="left">
			<br/><br/><br/><br/><br/>
			<font style="font-family: 'Arial Black', Arial, Verdana, serif;">
			<p>Votre rapport va être généré. Il vous sera envoyé par mail.
			Si la taille de votre rapport est trop importante, l'adresse de récupération ainsi que la procédure 
			de récupération du fichier sur l'extranet CONSOTEL vous sera communiquée.<br/>
			</p><br/>Merci,<br/><br/>
			L'équipe support ConsoView.
			<br/><br/>
		<div>
		<div width="100%" align="center">
			<button type="button" onclick="JavaScript:window.close()">Fermer</button>
		</div>
	 </cfoutput>
************************************** --->
<!--- 
	<cfset idMessage = createUUID()>
	<cfquery name="getDate" datasource="#SESSION.OFFREDSN#">
		SELECT to_char(last_day(to_date('#form.datefin#','yyyy/mm')),'yyyy/mm/dd') ladate FROM dual
	</cfquery>
	<cfset var="#form.perimetre_index#_#idMessage#">
	<cfobject name="sr" type="component" component="fr.consotel.consoview.util.publicreportservice.ScheduleRequest">
	<cfset sr.setReportRequest("/consoview/facturation/rapport/EtatConsoGsm/EtatConsoGsm.xdo","cv","#form.format#")>
	<cfset sr.AddParameter("S_IDPERIMETRE","#form.perimetre_index#")>
	<cfset sr.AddParameter("Q_SHORT_DEB","#form.datedeb#/01")>
	<cfset sr.AddParameter("Q_SHORT_FIN","#getDate.ladate#")>
	<cfset sr.AddParameter("Q_USERID","#SESSION.USER.CLIENTACCESSID#")>
	<cfset sr.setScheduleRequest("production","prod","#var#")>
	<cfset sr.setNotificationScheduleRequest("import@consotel.fr",true,true,true)>
	<cfset extension="xls">
	<cfif UCase(FORM.FORMAT) EQ "EXCEL">
		<cfset extension="xls">
	<cfelseif UCase(FORM.FORMAT) EQ "PDF">
		<cfset extension="pdf">
	<cfelseif UCase(FORM.FORMAT) EQ "CSV">
		<cfset extension="csv">
	<cfelse>
		<cfset extension="data">
	</cfif>
	<cfset sr.setFtpDeliveryRequest("pelican.consotel.fr","services","services","/OBIEE/PRODUCTION/CVRP1/EtatConsoGsm_v41_#var#.#extension#",false)>
	<cfset sr.setBurstScheduleRequest(false)>
	<cfset sr.scheduleReport()>


			 <cfmail from="#UTILISATEUR#"
							to="#SUPPORT_EMAIL#"
							bcc="#SERVICE_IT_EMAIL#"
							subject="Demande de rapport : #structRapport[TYPE_DE_RAPPORT]# par #UTILISATEUR# (BackOffice : #CGI.SERVER_NAME#)"
							type="html"
							charset="utf-8"
							wraptext="72"
							failto="#SUPPORT_EMAIL#"
							server="#MAIL_SERVER#">
				Le rapport Etat des consos par famille a &eacute;t&eacute; g&eacute;n&eacute;r&eacute; ici :<br>
				<cfset extension="xls">
				<cfif UCase(FORM.FORMAT) EQ "EXCEL">
					<cfset extension="xls">
				<cfelseif UCase(FORM.FORMAT) EQ "PDF">
					<cfset extension="pdf">
				<cfelseif UCase(FORM.FORMAT) EQ "CSV">
					<cfset extension="csv">
				<cfelse>
					<cfset extension="data">
				</cfif>
				\\db-5\pelican\services\OBIEE\PRODUCTION\CVRP1\EtatConsoGsm_#var#.#extension#
				<p><br></p>
				<br>les infos utilisateurs sont:<br>
				<cfdump var="#SESSION.USER#" expand="true" label="Infos Utilisateur">
				<br><br>
				<cfdump var="#FORM#" expand="true" label="Paramètres du rapport">
			 </cfmail>	 --->
<cfelse>

<!--- ==================== PARAMS ============================== --->
<!--- Creation du dossier temporaire --->
<cffunction name="createFolder" access="private" returntype="numeric" output="true">
	<cfargument name="folderName" type="string" required="true">
	<cfset racine = "listemailsclient">
	<cfset qDir = 1>
	<cfdirectory action="List" directory="#TEMP_DIR#/#folderName#" name="qDir">
	<cftry>
		<cfdirectory action="create" directory="#TEMP_DIR#/#folderName#">	
		<cfcatch type="any">
			<!--- 
			<cfoutput>Le dossier existe</cfoutput>
			 --->
		</cfcatch>
	</cftry>
	<cfreturn qDir.recordcount>
</cffunction>

<cfquery name="qBadRapport" datasource="#SESSION.OFFREDSN#">
	SELECT decode('#TYPE_DE_RAPPORT#','INVENTAIREPRODUITSGLOBAL',1,'INVENTAIRELIGNESGLOBAL',1,0) as BOOL_BAD_REPORT
	FROM DUAL
</cfquery>

<cfset isReportBlackListed = qBadRapport["BOOL_BAD_REPORT"]>

<cfquery name="gCheckID" datasource="#SESSION.OFFREDSN#">
	SELECT count(*) as nb
		FROM groupe_client
		where idgroupe_client in (106539,436,197,228,279,282,375,277,903,278,280,233,500,503,3122988,3073734,2322220,349817,3097911,2458807,415,18242,311,27217,2458788,106543,477)
   AND idgroupe_client=#idracine#
</cfquery>

<cfif (isReportBlackListed eq 1 and TYPE_DE_PERIMETRE eq "GROUPE" AND gCheckID.nb eq 1) OR (TYPE_DE_RAPPORT EQ "ETATCONSOGSM" AND gCheckID.nb eq 1)>
	 <!--- Sauvegarde du message --->
		<cftry>	
			<cfset idMessage = createUUID()>
			<cfset folder = "#TEMP_DIR#/" & SESSION.PERIMETRE.RAISON_SOCIALE & "/" & SESSION.USER.NOM & "_" & SESSION.USER.PRENOM & '/' & LSDATEFORMAT(now(),"dd_mm_yyyy") >
			<cfset resFolder = createFolder( SESSION.PERIMETRE.RAISON_SOCIALE & "/" & SESSION.USER.NOM & "_" & SESSION.USER.PRENOM & '/' & LSDATEFORMAT(now(),"dd_mm_yyyy"))>
			<cfset FILE_NAME="#folder#/" & idMessage & "_" & TYPE_DE_RAPPORT & resFolder & ".html">
			<!--- Gestion du rapport Etat par famille --->
			 <cfif TYPE_DE_RAPPORT eq "ETATCONSOGSM">
				<cfquery name="getDate" datasource="#SESSION.OFFREDSN#">
					SELECT to_char(last_day(to_date('#form.datefin#','yyyy/mm')),'yyyy/mm/dd') ladate FROM dual
				</cfquery>
				<cfset var="#form.perimetre_index#_#idMessage#">
				<cfthread action="run" name="myThread">
					<cfobject name="sr" type="component" component="fr.consotel.consoview.util.publicreportservice.ScheduleRequest">
					<cfset sr.setReportRequest("/consoview/facturation/rapport/EtatConsoGsm/EtatConsoGsm.xdo","cv","#form.format#")>
					<cfset sr.AddParameter("P_IDGROUPE_CLIENT","#form.perimetre_index#")>
					<cfset sr.AddParameter("P_DATEDEB","#form.datedeb#/01")>
					<cfset sr.AddParameter("P_DATEFIN","#getDate.ladate#")>
					<cfset sr.setScheduleRequest("production","prod","#var#")>
					<cfset sr.setNotificationScheduleRequest("import@consotel.fr",true,true,true)>
					<cfset extension="xls">
					<cfif UCase(FORM.FORMAT) EQ "EXCEL">
						<cfset extension="xls">
					<cfelseif UCase(FORM.FORMAT) EQ "PDF">
						<cfset extension="pdf">
					<cfelseif UCase(FORM.FORMAT) EQ "CSV">
						<cfset extension="csv">
					<cfelse>
						<cfset extension="data">
					</cfif>
					<!--- 
					<cfset sr.setFtpDeliveryRequest("SUN-BI","oracle","tred78","/opt/oracle/home/OBIEE/PRODUCTION/CVRP1/EtatConsoGsm_#var#.#extension#",false)>
					 --->
					<cfset sr.setFtpDeliveryRequest("DB-5","services","services","/OBIEE/PRODUCTION/CVRP1/EtatConsoGsm_#var#.#extension#",false)>
					<cfset sr.setBurstScheduleRequest(false)>
					<cfset sr.scheduleReport()>
				</cfthread>
	 		<cfelse>
				<cfsavecontent variable="contentObj">
					<cfheader name="Content-Disposition" value="inline;filename=#FILE_NAME#" charset="utf-8">
					<cfcontent type="text/html">
					<cfoutput>
						Demande du rapport #structRapport[TYPE_DE_RAPPORT]# par #UTILISATEUR# (BackOffice : #CGI.SERVER_NAME#)
						<br>les params sont :<br>
							<cfdump var="#FORM#" expand="true">						
						<br><br><br>les infos de perimetre sont: <br>
						<cfdump var="#SESSION.PERIMETRE#" expand="true"><br>
						les infos utilisateurs sont:<br>
						<cfdump var="#SESSION.USER#" expand="true">
					</cfoutput>
				</cfsavecontent>
				<!--- Création du fichier text--->
				<cffile action="write" file="#FILE_NAME#" charset="utf-8" mode="777" addnewline="true" output="#contentObj#">
				 <!--- 	Begin copying the file on Final Directory	--->			
				<cfftp action="open" server="#FTP_SERVER#" port="#FTP_PORT#" username="#FTP_LOGIN#" password="#FTP_MDP#" connection="ftp">
				<cfset str1 = ' '>
				<cfset str2 = '_'>		
				<cfset SOCIETE = #Replace(SESSION.PERIMETRE.RAISON_SOCIALE,str1,str2,"all")#>
				<cfset logFilename=societe & "_" & SESSION.USER.NOM & "_" & SESSION.USER.PRENOM 
									& '_' & LSDATEFORMAT(now(),"dd_mm_yy") & "_" & idMessage & "_" & TYPE_DE_RAPPORT & resFolder & ".html">
				<cfftp action="putfile" localfile="#FILE_NAME#" remotefile="#logFilename#" connection="ftp">
				<cfftp action="close" connection="ftp">
			</cfif>	
		<cfcatch type="any">
			 <cfmail from="#UTILISATEUR#"
				to="#SUPPORT_EMAIL#"
				bcc="#SERVICE_IT_EMAIL#"
				subject="Demande de rapport : #structRapport[TYPE_DE_RAPPORT]# par #UTILISATEUR# (BackOffice : #CGI.SERVER_NAME#)"
				type="html"
				charset="utf-8"
				wraptext="72"
				failto="#SUPPORT_EMAIL#"
				server="#MAIL_SERVER#">
				Erreur : 	
				Demande du rapport #structRapport[TYPE_DE_RAPPORT]# par #UTILISATEUR#					
				<br>les params sont :<br>
				<cfdump var="#FORM#" expand="true">
				<br><br><br>les infos de perimetre sont:<br>
				<cfdump var="#SESSION.PERIMETRE#" expand="true">
				<br>les infos utilisateurs sont:<br>
				<cfdump var="#SESSION.USER#" expand="true">
			 </cfmail>		
		</cfcatch>
		</cftry>
	<!--- Fin sauvegarde du message --->
	<cfif TYPE_DE_RAPPORT eq "ETATCONSOGSM">
		<!--- Envoi du message ---> 
			 <cfmail from="#UTILISATEUR#"
							to="#SUPPORT_EMAIL#"
							bcc="#SERVICE_IT_EMAIL#"
							subject="Demande de rapport : #structRapport[TYPE_DE_RAPPORT]# par #UTILISATEUR# (BackOffice : #CGI.SERVER_NAME#)"
							type="html"
							charset="utf-8"
							wraptext="72"
							failto="#SUPPORT_EMAIL#"
							server="#MAIL_SERVER#">
				Le rapport Etat des consos par famille a &eacute;t&eacute; g&eacute;n&eacute;r&eacute; ici :<br>
				<cfset extension="xls">
				<cfif UCase(FORM.FORMAT) EQ "EXCEL">
					<cfset extension="xls">
				<cfelseif UCase(FORM.FORMAT) EQ "PDF">
					<cfset extension="pdf">
				<cfelseif UCase(FORM.FORMAT) EQ "CSV">
					<cfset extension="csv">
				<cfelse>
					<cfset extension="data">
				</cfif>
				\\db-5\pelican\services\OBIEE\PRODUCTION\CVRP1\EtatConsoGsm_#var#.#extension#
				<p><br></p>
				<br>les infos utilisateurs sont:<br>
				<cfdump var="#SESSION.USER#" expand="true" label="Infos Utilisateur">
				<br><br>
				<cfdump var="#FORM#" expand="true" label="Paramètres du rapport">
			 </cfmail>	
	 <!--- Fin Envoi du message --->	
	<cfelse>
		<!--- Envoi du message ---> 
		 <cfmail from="#UTILISATEUR#"
						to="#SUPPORT_EMAIL#"
						bcc="#SERVICE_IT_EMAIL#"
						subject="Demande de rapport : #structRapport[TYPE_DE_RAPPORT]# par #UTILISATEUR# (BackOffice : #CGI.SERVER_NAME#)"
						type="html"
						charset="utf-8"
						wraptext="72"
						failto="#SUPPORT_EMAIL#"
						server="#MAIL_SERVER#">
			Demande du rapport #structRapport[TYPE_DE_RAPPORT]# par #UTILISATEUR#				
			<br>les params sont :<br>
			<cfdump var="#FORM#" expand="true">
			<br><br><br>les infos de perimetre sont:<br>
			<cfdump var="#SESSION.PERIMETRE#" expand="true">
			<br>les infos utilisateurs sont:<br>
			<cfdump var="#SESSION.USER#" expand="true">
		 </cfmail>
	</cfif>
	<cfoutput>
		<img  src="Consoview.GIF">
		<div id="contenu" width="100%" align="left">
			<br/><br/><br/><br/><br/>
			<font style="font-family: 'Arial Black', Arial, Verdana, serif;">
			<p>Votre rapport va &egrave;tre g&eacute;n&eacute;r&eacute;. Il vous sera envoy&eacute; par mail.
			Si la taille de votre rapport est trop importante, l'adresse de r&eacute;cup&eacute;ration ainsi que la proc&eacute;dure 
			de r&eacute;cup&eacute;ration du fichier sur l'extranet CONSOTEL vous sera communiqu&eacute;e.<br/>
			</p><br/>Merci,<br/><br/>
			L'&eacute;quipe support ConsoView.
			<br/><br/>
		<div>
		<div width="100%" align="center">
			<button type="button" onclick="JavaScript:window.close()">Fermer la fenêtre</button>
		</div>
	 </cfoutput>
<cfelse>
	<cfset reportContext=createObject("component","fr.consotel.consoview.rapports.RapportContext")>
	<cfset reportContext.displayRapport(FORM.PERIMETRE_INDEX,FORM.TYPE_PERIMETRE,FORM.REPORT_TYPE,FORM.RAISON_SOCIALE,FORM)>
</cfif> 
</cfif>
