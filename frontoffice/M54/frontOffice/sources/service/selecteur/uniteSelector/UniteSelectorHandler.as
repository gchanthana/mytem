package service.selecteur.uniteSelector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class UniteSelectorHandler
	{
		private var _model: UniteSelectorModel;
		
		public function UniteSelectorHandler(_model:UniteSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValueUniteHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValues(evt.result as ArrayCollection);
			}
		}	
	}
}