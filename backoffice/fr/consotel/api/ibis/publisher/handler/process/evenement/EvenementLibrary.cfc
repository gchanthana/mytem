<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.evenement.EvenementLibrary"
extends="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement"
hint="Extension de Evenement caractérisée par les règles suivantes :
- Les rapports exécutés sont référencés à partir de leur identifiant dans la bibliothèque
- La définition du rapport de chaque exécution est récupérée dans la bibliothèque à partir d'une requête
- La liste des paramètres dépend de l'identifiant du rapport et est récupérée pour chaque exécution à effectuer
- La visibilité de chaque paramètre dépend de l'identifiant du rapport, la liste des paramètres est donc récupérée pour chacune des exécution à effectuer">
	<!--- fr.consotel.api.ibis.publisher.handler.process.evenement.EvenementLibrary --->
	<cffunction access="private" name="getCurrentReportDefinition" returntype="Query"
	hint="Retourne la requete contenant la définition du rapport pour l'exécution dont l'index est currentIndex()">
		<cfset var RLIST=getExecutionList()>
		<cfset var reportId=VAL(RLIST["IDRAPPORT"][currentIndex()])>
		<cfif VARIABLES.CURRENT_REPORT_DEV.RECORDCOUNT NEQ 1>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
				message="La définition du rapport correspond à l'identifiant #reportId# est introuvable ou est invalide"
				detail="La définition du rapport est introuvable ou invalide (e.g : Plusieurs enregistrements trouvés)">
		</cfif>
		<cfreturn VARIABLES.CURRENT_REPORT_DEV>
	</cffunction>
	
	<cffunction access="private" name="currentReportDefinition" returntype="Query"
	hint="Execute et retourne une requete contenant la définition du rapport pour l'exécution dont l'index est currentIndex(). Les colonnes sont les suivantes :
	DOSSIER (Chemin absolu du dossiser du XDO dans BIP), XDO (Nom du XDO sans l'extension .xdo),
	FILE_NAME (Nom du fichier dans le container et pour le mail), LIBELLE_IHM (Libellé du fichier dans le container)">
		<cfset var periode="">
		<cfset var perimetre="">
		<cfset var RLIST=getExecutionList()>
		<cfset var reportId=VAL(RLIST["IDRAPPORT"][currentIndex()])>
		<cfset var code_langue=RLIST["LOCALISATION"][currentIndex()]>
		<!--- Colonnes : DOSSIER, XDO, FILE_NAME (Ticket 5698), LIBELLE_IHM (Ticket 5698) --->
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.INFO_RAPPORT">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#reportId#" 	null="false" variable="p_idrapport">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#periode#" 	null="false" variable="p_L_PERIODE">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#perimetre#" 	null="false" variable="p_L_PERIMETRE">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_langue#" 	null="false" variable="p_code_langue">
			<cfprocresult name="qReportingDefinition">
		</cfstoredproc>
		<cfreturn qReportingDefinition>
	</cffunction>

	<cffunction access="private" name="getCurrentParamsDefinition" returntype="Query"
	hint="Retourne la requete contenant la définition des paramètres du rapport pour l'exécution dont l'index est currentIndex()">
		<cfset VARIABLES.CURRENT_PARAMS_DEV=currentParamsDefinition()>
		<cfreturn VARIABLES.CURRENT_PARAMS_DEV>
	</cffunction>
	
	<cffunction access="private" name="currentParamsDefinition" returntype="Query"
	hint="Execute et retourne une requete contenant la définition des paramètres du rapport pour l'exécution dont l'index est currentIndex(). Les colonnes sont les suivantes :
	NOM (Nom du paramètre), LIBELLE (Libellé du paramètre), IS_SYSTEM (0 si le paramètre est visible dans le container et 1 sinon)">
		<cfset var RLIST=getExecutionList()>
		<cfset var reportId=VAL(RLIST["ID"][currentIndex()])>
		<cfquery name="qParamsDef" datasource="ROCOFFRE">
			select	'P_STATUS' as NOM, 'Statut' as LIBELLE, 0 as IS_SYSTEM
			from		DUAL
			where	1=0
		</cfquery>
		<cfreturn qParamsDef>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement --->
	<cffunction access="public" name="getName" returntype="String" hint="Retourne le nom de l'évènement">
		<cfreturn "Bibliothèque Evènement " & listLast(getMetadata(THIS).NAME,".")>
	</cffunction>

	<cffunction access="private" name="currentReporting" returntype="Struct"
	hint="Appelle getReportDefinition() pour construire la structure retournée par currentReportDefinition() puis appelle la méthode parent">
		<!--- Référence vers la définition du rapport de l'exécution dont l'index est currentIndex() --->
		<cfset VARIABLES.CURRENT_REPORT_DEV=currentReportDefinition()>
		<cfreturn SUPER.currentReporting()>
	</cffunction>

	<cffunction access="private" name="reportLongName" returntype="String" hint="Déterminée à partir de la définition du rapport dans la bibliothèque">
		<cfset var reportDefQuery=getCurrentReportDefinition()>
		<cfreturn reportDefQuery["LIBELLE_IHM"][1]>
	</cffunction>

	<cffunction access="private" name="reportFileName" returntype="String" hint="Déterminée à partir de la définition du rapport dans la bibliothèque">
		<cfset var reportDefQuery=getCurrentReportDefinition()>
		<cfreturn reportDefQuery["FILE_NAME"][1]>
	</cffunction>

	<cffunction access="private" name="xdoAbsolutePath" returntype="String" hint="Déterminée à partir de la définition du rapport dans la bibliothèque">
		<cfset var reportDefQuery=getCurrentReportDefinition()>
		<cfreturn reportDefQuery["DOSSIER"][1] & "/" & reportDefQuery["XDO"][1] & "/" & reportDefQuery["XDO"][1] & ".xdo">
	</cffunction>

	<cffunction access="private" name="parameterColumns" returntype="struct"
	hint="La valeur retournée est construite à partir de la définition des paramètres du rapport dans la bibliothèque">
		<cfset var paramColumns={}>
		<cfset var paramsDef=getCurrentParamsDefinition()>
		<cfloop index="i" from="1" to="#paramsDef.recordcount#">
			<cfset var paramName=paramsDef["NOM"][i]>
			<!--- Un paramètre ne peut etre ajouté qu'une et une seule fois (La colonne NOM pouvant avoir plusieurs fois la meme valeur) --->
			<cfif NOT structKeyExists(paramColumns,paramName)>
				<cfset var isVisible=NOT (yesNoFormat(VAL(paramsDef["IS_SYSTEM"][i])))>
				<cfset structInsert(paramColumns,paramName,{LABEL=paramsDef["LIBELLE"][i], VISIBLE=isVisible},FALSE)>
			</cfif>
		</cfloop>
		<cfreturn paramColumns>
	</cffunction>

	<cffunction access="private" name="requiredColumns" returntype="struct" hint="Retourne la liste des colonnes qui sera définie comme paramètres métiers">
		<cfreturn {
			ID="ID de MAJ", DELIVERY="Stratégie de diffusion", POST_PROCESS="Post Process Evènement", USERID="UserId container", CODE_APPLI="CodeApp container",
			IDRACINE="IdRacine container", EMAIL_FROM="Expéditeur mail", EMAIL_TO="Destinataire mail", EMAIL_CC="Copie mail", EMAIL_SUBJECT="Sujet mail",
			IDRAPPORT="Identifiant du rapport dans la bibliothèque", FILE_EXT="Extension du fichier container et mail", HAVE_ATTACHEMENT="1 Si attachment mail et 0 sinon",
			TEMPLATE="Template du XDO", FORMAT="Format de sortie", LOCALISATION="Localisation",
			L_PERIODE="Libellé de période requis par les rapports de la bibliothèque", L_PERIMETRE="Libellé de périmètre requis par les rapports de la bibliothèque"
		}>
	</cffunction>
	
	<cffunction access="private" name="requiredProperties" returntype="Struct" hint="Retourne la liste retournée par la méthode parent">
		<cfreturn SUPER.requiredProperties()>
	</cffunction>

	<cffunction access="private" name="getReportList" returntype="query" hint="Exécute et retourne la requete ramenant la liste des rapports à exécuter">
		<cfquery name="qEvenement" datasource="ROCOFFRE">
			select	/* Champs métiers */
						null as ID, null as DELIVERY, null as POST_PROCESS, null as USERID, null as CODE_APPLI, null as IDRACINE,
						null as EMAIL_FROM, null as EMAIL_TO, null as EMAIL_CC, null as EMAIL_SUBJECT, null as FILE_EXT, null as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						null as IDRAPPORT, null as TEMPLATE, null as FORMAT, null as LOCALISATION,
						/* Colonnes requises par les bibliothèque des rapports */
						null as L_PERIMETRE, null as L_PERIODE
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
			from		DUAL
			where	1=0
		</cfquery>
		<cfreturn qEvenement>
	</cffunction>
</cfcomponent>