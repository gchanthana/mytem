package univers.facturation.suivi.controles.vo
{
	[Bindable]
	public class InventaireVO
	{
		
		public var idInventaire:Number = 0;
		public var filtre:String = "";
		public var abonnement:Number = 1;
		public var option:Number = 1;
		public var nonTraite:Number = 1;
		public var reclamation:Number = 1;
		public var cloture:Number = 0;
		public var gestion:Number = -1;
		public var effectif:Number = 10;
		public var solution:Number = 1;
		
		
		public function InventaireVO()
		{
		}
		
		
		
	}
}