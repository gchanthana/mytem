package univers.facturation.optimisation {
	import composants.tb.rapports.RapportEvent;
	
	import mx.events.FlexEvent;
	
	import univers.UniversFunctionItem;
	
	public class Optimisation extends OptimisationIHM implements UniversFunctionItem {
		
		public function Optimisation() {
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}

		private function initIHM(event:FlexEvent):void {
			trace("Optimisation initIHM()");
			mainIhm.addEventListener(RapportEvent.REPORT_SELECTION_CHANGED,onReportChange);
			mainIhm.setReportList(paramIhm.getReportList());
		}
		
		private function onReportChange(event:RapportEvent):void {
			paramIhm.selectReport(mainIhm.getCurrentReportIndex());
		}
		
		public function onPerimetreChange():void {
			paramIhm.onPerimetreChange();
			histoIhm.onPerimetreChange();
			trace("Optimisation onPerimetreChange()");
		}
	}
}
