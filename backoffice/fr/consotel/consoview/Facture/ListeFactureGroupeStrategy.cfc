 =========================================================================
Classe: ListeFactureGroupeStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="ListeFactureGroupeStrategy" extends="ListeFactureStrategy" hint="This class implements the algorithm using the Strategy interface.">
 
	<cffunction name="rechercheFactureByNumber" access="public" returntype="Query" output="false" >
		<cfargument name="NUMERO_CLE" required="false" type="string" default="" displayname="string NUMERO_CLE" hint="" />
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="ID du groupe (ID_PERIMETRE)" />
		<cfquery name="listeFactures" datasource="#session.OffreDSN#">
			select numero_facture, COMPTE_FACTURATION, DATE_EMISSION, DATEDEB, DATEFIN, libelle, rc.idref_client, ip.IDINVENTAIRE_PERIODE, '1' as a, '1' as b, '1' as c
			from inventaire_periode ip, compte_facturation cp, ref_client rc, groupe_client_ref_client gcrc
			where lower(ip.numero_facture) like lower('%#NUMERO_CLE#%')
			and ip.idcompte_facturation=cp.idcompte_facturation
			and cp.idref_client=rc.idref_client
			and rc.idref_client=gcrc.idref_client
			and gcrc.idgroupe_client='#ID#'
			order by numero_facture asc
		</cfquery>
		<cfloop from="1" to="#listeFactures.recordcount#" index="i">
			<cfset listeFactures.a[i]=LsDateFormat(listeFactures.DATE_EMISSION[i],"dd mmmm yyyy")>
			<cfset listeFactures.b[i]=LsDateFormat(listeFactures.DATEDEB[i],"dd mmmm yy")>
			<cfset listeFactures.c[i]=LsDateFormat(DateAdd("d",-1,listeFactures.DATEFIN[i]),"dd mmmm yy")>
		</cfloop>
		<cfset obj=createObject("component","fr.consotel.consoview.Facture.Facture")>
		<cfset obj.setData(listeFactures)>
		<cfreturn listeFactures>
	</cffunction>
	
	<cffunction name="rechercheFactureByNumberEtOpe" access="remote" returntype="Query" output="false" >
		<cfargument name="arr" required="false" type="array" default="" displayname="string NUMERO_CLE" hint="" />
		
		 
		<cfset d1=getToken(arr[4],3,"/") & "/" & getToken(arr[4],2,"/") & "/" & getToken(arr[4],1,"/")>
		<cfset d2=getToken(arr[5],3,"/") & "/" & getToken(arr[5],2,"/") & "/" & getToken(arr[5],1,"/")> 
		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_V3.SF_LISTFACTUREOP_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#arr[2]#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#d1#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#d2#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#arr[3]#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#arr[1]#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_code_langue" value="#SESSION.USER.GLOBALIZATION#"/>
	        <cfprocresult name="listeFactures"/>        
		</cfstoredproc>
		<cfloop query="listeFactures">
			<cfset listeFactures.a  = LsDateFormat(listeFactures.DATE_EMISSION,"dd mmmm yyyy")>
			<cfset listeFactures.b  = LsDateFormat(listeFactures.DATEDEB,"dd mmmm yy")>
			<cfset listeFactures.c  = LsDateFormat(listeFactures.DATEFIN,"dd mmmm yy")>
		</cfloop> 
		<cfset obj=createObject("component","fr.consotel.consoview.Facture.Facture")>
		<cfset obj.setData(listeFactures)>
		<cfreturn listeFactures> 
	</cffunction>
	
	<cffunction name="getListeOperateur" access="remote" returntype="Query" output="false" >
		<cfargument name="arr" required="false" type="array" default="" displayname="string NUMERO_CLE" hint="" />
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_v3.sf_listeop_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#arr[1]#"/>
	        <cfprocresult name="listeFactures"/>        
		</cfstoredproc>
		<cfquery name="getListe" dbtype="query">
			select opnom as label, operateurID as data
			from listeFactures
		</cfquery>
		<cfreturn getListe>
	</cffunction>
	
	<cffunction name="ValiderFacture" access="remote" returntype="void" output="false" >
		<cfargument name="arr" required="true" type="array" default="" />
		<cfquery name="updateEtat" datasource="#SESSION.OFFREDSN#">
			update inventaire_periode ip
			set ip.idetat_facture=2
			where ip.idinventaire_periode=#arr[1]#
		</cfquery>

	</cffunction>
	<cffunction name="displayListeFacture" access="public" returntype="void" output="true">
		<cfargument name="listeFactures" required="true" type="Query">
		<cfabort showerror="Erreur: Classe abstraite">
	</cffunction>
	
</cfcomponent>
