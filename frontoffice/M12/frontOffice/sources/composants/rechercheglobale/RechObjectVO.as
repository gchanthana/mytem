package composants.rechercheglobale
{											  
	[RemoteClass(alias="fr.consotel.consoview.rechercheglobale.RechObject")]
	[Bindable]
	public class RechObjectVO
	{
		public var ID : Number = 0;
		public var APP_LOGINID : Number = 0;
		public var IDRACINE : Number = 0;
		public var LIBELLE_GROUPE : String = "";
		public var LIGNES : String = "";
		
	}
}