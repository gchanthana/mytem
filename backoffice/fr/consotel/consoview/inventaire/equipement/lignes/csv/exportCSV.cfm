<cfsetting enablecfoutputonly="true">
<cfset charset="ISO-8859-1">
<cfset rootPath=expandPath("/")>
<cfset ddlFilename="Export-Lignes-Et-Services_" & replace(session.perimetre.raison_sociale,' ','_',"all") & ".csv">
<cfheader name="Content-Disposition" value="attachment;filename=#ddlFilename#" charset="#charset#">
<cfcontent type="text/csv" file="#rootPath#/fr/consotel/consoview/inventaire/equipement/lignes/csv/#FILE_NAME#" deletefile="true">
