<cfcomponent output="false" alias="fr.consotel.consoview.M16.vo.FileUploadVo">

	<cfproperty name="fileData" 		type="Any" 		default="">
	
	<cfproperty name="fileDate" 		type="Date" 	default="">
	
	<cfproperty name="fileName" 		type="String" 	default="">
	<cfproperty name="fileExt" 			type="String" 	default="">
	<cfproperty name="fileUUID" 		type="String" 	default="">
	
	<cfproperty name="fileBy" 			type="String" 	default="">
	<cfproperty name="fileDateStrg" 	type="String" 	default="">
	
	<cfproperty name="fileSelected" 	type="Boolean" 	default="false">
	
	<cfproperty name="fileSize" 		type="Numeric" 	default="0">
	<cfproperty name="fileJoin" 		type="Numeric" 	default="1">
	<cfproperty name="fileRename" 		type="Numeric" 	default="0">
	<cfproperty name="fileRemove" 		type="Numeric" 	default="0">
	<cfproperty name="fileId" 			type="Numeric" 	default="0">
	<cfproperty name="fileIdRacine" 	type="Numeric" 	default="0">
	<cfproperty name="fileIdCommande" 	type="Numeric" 	default="0">
	
	<!--- Initialize the CFC with the default properties values --->
	<cfscript>
		
		this.fileData 		= "";
		
		this.fileDate		= "";
		
		this.fileName 		= "";
		this.fileExt 		= "";
		this.fileUUID 		= "";
		this.fileBy			= "";
		this.fileDateStrg	= "";
		
		this.fileSelected 	= false;
		
		this.fileSize 		= 0;
		this.fileJoin 		= 1;
		this.fileRename 	= 0;
		this.fileRemove 	= 0;
		this.fileId			= 0;
		this.fileIdRacine	= 0;
		this.fileIdCommande	= 0;
		
	</cfscript>
	
	<!---
		
		Auteur : nicolas.renel
		
		Date : 26/03/2012
		
		Description : GETTER et SETTER
	
	--->
	
	<cffunction name="init" output="false" returntype="FileUploadVo">
		<cfreturn this>
	</cffunction>
	
</cfcomponent>