package appli.mytem
{
	import appli.impersonnification.refreshManager;
	
	import general.abstract.AbstractCreateApplication;
	import general.abstract.AbstractModuleLoader;
	import general.interfaceClass.ICreateApplication;
	
	public class CreateMytemImpl extends  AbstractCreateApplication implements ICreateApplication
	{
		
		public function CreateMytemImpl()
		{
			super();
		}
		
		override public function updatePerimetre():void
		{
			menu.updatePerimetre();
			initDisplay();
		}
		
		override public function displayModule(key:String=null):void
		{
			var menuXML:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var selectedMenu:XMLList = menuXML.descendants("*").(@SYS == 1).(@toggled == 'true'); 
			if (refreshManager.isReloading == true)
			{//Exception dans le cas où il s'agit d'une connexion sur un autre compte, le point de départ doit devenir le HOME
				refreshManager.isReloading = false;
				/*moduleLoader.loadModule("HOME");
				trace("isReloading avait pour valeur true, on le met sur false");
				super.displayModule();*/
				CvAccessManager.changeModule("HOME");
			}
			else
			{
				moduleLoader.loadModule(selectedMenu.@KEY);
			}
		}
		
		override protected function initDisplay():void
		{
			displayModule();
		}
		
	}
}