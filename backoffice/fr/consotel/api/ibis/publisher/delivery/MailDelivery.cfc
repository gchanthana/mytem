<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.MailDelivery" extends="fr.consotel.api.ibis.publisher.delivery.Email"
hint="Extension de la stratégie Email qui est utilisée par défaut par la stratégie Mail pour déléguer la diffusion mail d'un reporting
La méthode effectuant la diffusion mail du reporting a été redéfinie pour effectuer les actions suivantes dans l'ordre :
- Effectue la diffusion mail du reporting (implémentation héritée de la classe parent)
- Dispatche un évènement de type fr.consotel.api.ibis.publisher.handler.event.UpdateEvent une fois la diffusion effectuée
L'évènement est dispatché en utilisant l'implémentation IReportingService provenant de l'évènement source IReportingEvent
Ce dispatch a pour effet d'executer la stratégie fr.consotel.api.ibis.publisher.delivery.Evenement">
	<cffunction access="private" name="getDeliveryEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"
	hint="Retourne un évènement de type fr.consotel.api.ibis.publisher.handler.event.EmailEvent non initialisé (i.e : Constructeur non appelé)">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent" required="true">
		<cfreturn createObject("component","fr.consotel.api.ibis.publisher.handler.event.MailDeliveryEvent")>
	</cffunction>

	<!---
	<cffunction access="private" name="diffuseEmailEvent" returntype="void"
	hint="Appelle la méthode de la classe parent puis dispatche un évènement de type fr.consotel.api.ibis.publisher.handler.event.UpdateEvent
	L'évènement est dispatché en utilisant l'implémentation IReportingService provenant de l'évènement source IReportingEvent">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.MailEvent"  required="true" hint="Provient de handleReportingEvent()">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<cfset var mailProperties=getEmailProperties(emailEvent)>
		<!--- Nom de l'attachement (Chaine vide lorsque le contenu est à diffuser dans le corps du mail) --->
		<cfset var localAttachment=emailEvent.getAttachment()>
		<!--- Si le contenu est à diffuser dans le corps du mail : Vérifie qu'il est de type texte --->
		<cfif TRIM(localAttachment) EQ "">
			<!--- Si le contenu à diffuser dans le corps du mail est de type texte : Le mail est diffusé avec le contenu --->
			<cfif emailEvent.isTextContent()>
				<cfset var reportingContent=emailEvent.getContent("","")>
				<cfmail attributecollection="#mailProperties#">
					<html>
						<head>
							<style type="text/css">
							body {background-color: ##FFFFFF;}
							</style>
						</head>
						<cfoutput>
							#reportingContent#<br>
						</cfoutput>
					</html>
				</cfmail>
			<!--- Sinon il n'est pas de type texte : Il est diffusé en attachement à onFailureTo() et le contenu du mail est event.getDeliveryInfos() --->
			<cfelse>
				<cfset handleInvalidContentType(emailEvent)>
			</cfif>
		<!--- Si le contenu est à diffuser en tant qu'attachement mail : Le nom de la pièce jointe est retournée par getAttachment() --->
		<cfelse>
			<cfset var localFileAttachment=emailEvent.getContentAsLocalFile(emailEvent.getAttachment(),"")>
			<cfmail attributecollection="#mailProperties#">
				<!--- Si event..getContentAsLocalFile() est une chaine vide alors la pièce jointe correspond au contenu retourné par event.getContent() --->
				<cfif TRIM(localFileAttachment) EQ "">
					<cfset var reportingContent=emailEvent.getContent("","")>
					<cfmailparam file="#localAttachment#" type="#emailEvent.getContentType()#" content="#reportingContent#">
				<cfelse>
					<cfmailparam file="#localFileAttachment#" type="#emailEvent.getContentType()#" remove="true">
				</cfif>
			</cfmail>
			<!--- Si le chemin du fichier local correspond à l'attachement n'est pas une chaine vide : Le répertoire du fichier correspondant est supprimé --->
			<cfif TRIM(localFileAttachment) NEQ "">
				<cfset deleteAttachmentDir(emailEvent,localFileAttachment)>
			</cfif>
		</cfif>
	</cffunction>
	--->
</cfcomponent>