package univers.parametres.rago.CreationRegles.event
{
	import flash.events.Event;
	
	import univers.parametres.rago.entity.Regle;

	public class WizardEvent extends Event
	{
		public static const GO_NEXT_ETAPE:String = "goNext";
		public static const GO_PREVIOUS_ETAPE:String = "goPrevious";
		public static const VALIDER_CREATION_REGLE:String = "validerCreationRegle";
		public static const VALIDER_CREATION_ET_EXECUTER_REGLE:String = "validerCreationEtExecuterRegle";
		public static const ANNULER_CREATION_REGLE:String = "annulerCreationRegle";
		
		private var _regle:Regle;
		
		public function WizardEvent(__regle:Regle,type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_regle = __regle;
		}
				
		override public function clone():Event
		{
			return new WizardEvent(_regle,type,bubbles,cancelable);
		}
		
		public function get regle():Regle
		{
			return _regle;
		}
	}
}