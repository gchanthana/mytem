package commandemobile.ihm.mail
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewUtil;
	import composants.util.PopUpImpl;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class SelectDestinataireImpl extends PopUpImpl
	{
		//Ui controls
		public var btOk:Button;
		public var btAnnuler:Button;
		public var dgListe:DataGrid;
		public var txtFiltre:TextInputLabeled;
		public var lblTitre:String;
		//fin ui controls
		
			
		//liste des contacts selectionnés
		private var _contactsSelectionnes:Array = [];
		private var _dataProvider:ICollectionView;
		private var _listeMails:String = "";
				
		
		public function SelectDestinataireImpl()
		{
			super();
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
		 	dgListe.selectedItems = contactsSelectionnes;
		}
		
		public function set contactsSelectionnes(value:Array):void
		{
			_contactsSelectionnes = value;
		}

		public function get contactsSelectionnes():Array
		{
			return _contactsSelectionnes;
		}

		public function set dataProvider(value:ICollectionView):void
		{
			_dataProvider = value;
		}
		

		public function get dataProvider():ICollectionView
		{
			return _dataProvider;
		}
		
		public function set listeMails(value:String):void
		{
			_listeMails = value;
		}

		public function get listeMails():String
		{
			return _listeMails;
		}
		
		//Handlers
		protected function dgListeChangeHandler(event:ListEvent):void
		{
			contactsSelectionnes = dgListe.selectedItems
		}
		
		protected function _dgListeDoubleClickHandler(event:MouseEvent):void
		{
			
		}
		
		protected function _btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function _btOkClickHandler(event:MouseEvent):void
		{
			var eventObj:ContactSelectedEvent = new ContactSelectedEvent(ContactSelectedEvent.CONTACT_SELECTED,contactsSelectionnes);
			dispatchEvent(eventObj);  
			PopUpManager.removePopUp(this);
			listeMails = ConsoviewUtil.arrayToList(new ArrayCollection(contactsSelectionnes),"EMAIL",",");
		}
		
		protected function txtFiltreChangeHandler(event:Event):void
		{
			if(dataProvider)
			{
				dataProvider.filterFunction = filterFunc;
				dataProvider.refresh();
			}	
		}		
		//Fin Handlers
		
		protected function filterFunc(item:Object):Boolean
		{
			var fResult:Boolean = true;
			
			if(String(item.NOM).toLocaleLowerCase().search(txtFiltre.text) != -1 
			|| String(item.EMAIL).toLocaleLowerCase().search(txtFiltre.text) != -1)
			{
				fResult = true;
			}
			else
			{
				fResult = false;
			}
			
			return fResult
		}

		
	}
}