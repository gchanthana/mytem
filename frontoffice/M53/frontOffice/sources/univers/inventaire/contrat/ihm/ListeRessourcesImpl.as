package univers.inventaire.contrat.ihm
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class ListeRessourcesImpl extends TitleWindow
	{
		private var _selectedContrat:Object;
		
		public function ListeRessourcesImpl()
		{
			super();
		}
		
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this)
		}
		
		
		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			
		}
		
		protected function btValiderClickHander(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this)
		}
		
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this)	
		}
	

		public function set selectedContrat(value:Object):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():Object
		{
			return _selectedContrat;
		}
	}
}