<!--- =========================================================================
Classe: ConcreteFactureCreator
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->

<cfcomponent name="GenericFacture" displayname="GenericFacture" hint="Facture Générique (Uniquement pour les opérateurs autres que FT - 63)" extends="Facture">

	<!--- UNNIQUEMENT POUR LA FACTURE GENERIQUE --->
	<cffunction name="setOPERATEURID" access="public" returntype="void">
		<cfargument name="OPERATEURID" required="true" type="numeric">
		<cfset variables.instance.OPERATEURID=#OPERATEURID#>
	</cffunction>
	
	<cffunction name="getOPERATEURID" access="public" returntype="numeric">
		<cfreturn variables.instance.OPERATEURID>
	</cffunction>
	<!---------------------------------------------->

	<!--- METHODS --->
	<cffunction name="ConstruireGabarit" access="public" returntype="void" output="true" >
		<cfargument name="dataset" required="true" type="query">

		<!--- MONTANT TOTAL --->
		<cfquery name="total" dbtype="query">
			SELECT SUM(montant) AS total
			FROM dataset
		</cfquery>

		<!--- MONTANT TOTAL DES ABONNEMENTS --->
		<cfquery name="getTotalAbonnements" dbtype="query">
			SELECT SUM(montant) AS total
			FROM dataset
			WHERE gamme = 'Abonnements'
		</cfquery>
		
		<!--- MONTANT TOTAL DES COMMUNNICATIONS --->
		<cfquery name="getTotalConsommations" dbtype="query">
			SELECT SUM(montant) AS total
			FROM dataset
			WHERE gamme = 'Communications'
		</cfquery>
		
		<!--- MONTANT TOTAL DES RESTANTS --->
		<cfquery name="getTotalRestants" dbtype="query">
			SELECT SUM(montant) as total
			FROM dataset
			WHERE gamme <> 'Abonnements' AND
			gamme <> 'Communications'
		</cfquery>
		
		<!--- MONTANT TOTAL PAR LIBELLE PRODUIT --->
		<cfquery name="getTotalByLibelle_Produit" dbtype="query">
			SELECT libelle_produit, SUM(qte) AS total_qte, SUM(montant) AS total_montant
			FROM dataset
			WHERE gamme = 'Abonnements'
			GROUP BY libelle_produit
		</cfquery>
		
		<!--- MONTANT TOTAL PAR DESTINATIONS --->
		<cfquery name="getTotalByDestinations" dbtype="query">
			SELECT libelle_produit, SUM(duree_appel) AS total_duree, SUM(nombre_appel) AS total_appel, SUM(montant) AS total_montant
			FROM dataset
			WHERE gamme = 'Communications'
			GROUP BY libelle_produit
		</cfquery>

		<!--- MONTANT TOTAL PAR LIBELLE_PRODUIT POUR LES RESTANTS --->
		<cfquery name="getTotalByOthers" dbtype="query">
			SELECT libelle_produit, SUM(qte) AS total_qte, SUM(montant) as total_montant
			FROM dataset
			WHERE gamme <> 'Abonnements'
			AND gamme <> 'Communications'
			GROUP BY libelle_produit
		</cfquery>

		<p><br></p>
		<cfoutput>
			<center><strong align=CENTER>#variables.instance.typeFactureStrategy.displayType()#</strong></center>
		</cfoutput>
		<p><br></p>
		<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="white" class="cadre" width="90%">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="white" class="FT">
				<tr>
					<td align="LEFT" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="left" bgcolor="white">
						<tr>
							<th colspan="2" class="huge" valign="bottom">
								<cfset displayer=createObject("component","fr.consotel.consoview.images.LogoDisplayer")>
								<cfset displayer.DisplayLogoByOperateurID(getOPERATEURID(),70)>
							</th>
						</tr>
						<TR>
							<td colspan="2">&nbsp;</td>
						</TR>
						<tr>
							<td class="normal">
								<strong>
									Né Compte Facturation : #dataset.compte_facturation#
								</strong>
							</th>
						</tr>
						</table>	
					</td>
					<td align="RIGHT" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="right" bgcolor="white">
						<tr>
							<th colspan="2" class="huge" valign="bottom"><strong>FACTURE</strong></th>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td  class="normal">Date de la facture: </td>
							<td  class="normal">#LsDateFormat(dataset.date_emission,"dd/mm/yyyy")#</td>
						</tr>
						<tr>
							<td class="normal">Numéro de la facture: </td>
							<td class="normal">#dataset.numero_facture#</td>
						</tr>
						</table>
					</td>
				</tr>
				<!--- FIN DE L'EN-TETE --->
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<!--- MONTANT TOTAL DES ABONNEMENTS --->
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<strong>Abonnements</strong>
								</td>
								<td align="right" class="big">
									<cfset total=#getTotalAbonnements.total#>
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<!--- DESCRIPTIF DES MONTANTS --->
				<cfif getTotalByLibelle_Produit.recordcount neq 0>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="small_header">&nbsp;</td>
								<td width="20" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">Qté</td>
								<td width="50" class="small_header"><nobr>Total en #getShortDevise()# HT</nobr></td>
							</tr>
							<cfoutput query="getTotalByLibelle_Produit">
							<tr>
								<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
								<td class="normal" align="right">&nbsp;</td>
								<td class="normal" align="right">&nbsp;</td>
								<td class="normal" align="center">#total_qte#</td>
								<td class="normal" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
							</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<!--- MONTANT TOTAL DES COMMUNICATIONS --->
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<strong>Commnunications</strong>
								</td>
								<td align="right" class="big">
									<cfset total=#getTotalConsommations.total#>
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<!--- DESCRIPTIF DES MONTANTS --->
				<cfif getTotalByDestinations.recordcount neq 0>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="small_header">&nbsp;</td>
								<td width="20" class="small_header">&nbsp;</td>
								<td width="20" class="small_header"><nobr>durée HH:MM:SS</nobr></td>
								<td width="50" class="small_header">Qté</td>
								<td width="50" class="small_header"><nobr>Total en #getShortDevise()# HT</nobr></td>
							</tr>
							<cfoutput query="getTotalByDestinations">
							<tr>
								<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
								<td class="normal" align="right">&nbsp;</td>
								<cfset heure=int(#total_duree#/60)>
								<cfset minute=int((#total_duree#-heure*60))>
								<cfset seconde=(#total_duree#*60-heure*3600-minute*60)>
								<td class="normal" align="right">
									<cfif heure lt 100>
										#LsNumberFormat(heure, '00')#
									<cfelse>
										#heure#
									</cfif>
								:#LsNumberFormat(minute,'00')#:#LsNumberFormat(seconde,'00')#
								</td>
								<td class="normal" align="center">#total_appel#</td>
								<td class="normal" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
							</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<!--- MONTANT TOTAL DES RESTANTS --->
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<strong>Autres</strong>
								</td>
								<td align="right" class="big">
									<cfset total=#getTotalRestants.total#>
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<!--- DESCRIPTIF DES MONTANTS --->
				<cfif getTotalByOthers.recordcount neq 0>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="small_header">&nbsp;</td>
								<td width="20" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">Qté</td>
								<td width="50" class="small_header"><nobr>Total en #getShortDevise()# HT</nobr></td>
							</tr>
							<cfoutput query="getTotalByOthers">
							<tr>
								<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
								<td class="normal" align="right">&nbsp;</td>
								<td class="normal" align="right">&nbsp;</td>
								<td class="normal" align="center">#total_qte#</td>
								<td class="normal" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
							</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
				</cfif>
			</table>
		</td>
	</tr>
</table>
</td></tr>
</table>
</cffunction>

</cfcomponent>