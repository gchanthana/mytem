package univers.parametres.perimetres.rightSide
{
    import mx.resources.ResourceManager;
    import composants.util.ConsoviewAlert;
    import flash.events.MouseEvent;
    import mx.containers.TitleWindow;
    import mx.controls.Button;
    import mx.controls.DateField;
    import mx.controls.TextArea;
    import mx.controls.TextInput;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.validators.NumberValidator;
    import mx.validators.Validator;

    [Bindable]
    public class BudgetParentImpl_V2 extends TitleWindow
    {
        /**************************************
         *		VARIABLES PUBLIQUES
         **************************************/ // TextInput
        public var tiName:TextInput;
        public var tiMin:TextInput;
        public var tiBudget:TextInput;
        public var tiMax:TextInput;
        // TextArea	
        public var taComment:TextArea;
        // DateField		
        public var dtStart:DateField;
        public var dtEnd:DateField;
        // Button
        public var btValider:Button;
        public var btAnnuler:Button;
        // Validator
        public var budgetValidator:NumberValidator;
        public var nameValidator:Validator;
        public var dateDebutValidator:Validator;
        public var dateFinValidator:Validator;
        public var enfant:BudgetEnfantImpl_V2;
        public var newExercice:Boolean = false;;

        /**************************************
         *			VARIABLES PRIVEES
         **************************************/
        public function BudgetParentImpl_V2()
        {
            addEventListener(FlexEvent.CREATION_COMPLETE, init);
        }

        private function init(evt:FlexEvent):void
        {
            initListener();
            initValue();
        }

        private function initValue():void
        {
            if(!newExercice)
            {
                tiName.text = enfant.resultData[enfant.currentExercice].LIBELLE_EXERCICE;
                tiBudget.text = enfant.resultData[enfant.currentExercice].MONTANTBUDGET;
                taComment.text = enfant.resultData[enfant.currentExercice].COMMENTAIRES;
                dtStart.selectedDate = enfant.resultData[enfant.currentExercice].DATEDEBUT;
                dtEnd.selectedDate = enfant.resultData[enfant.currentExercice].DATEFIN;
            }
        }

        private function initListener():void
        {
            this.addEventListener(CloseEvent.CLOSE, closePopup);
            btValider.addEventListener(MouseEvent.CLICK, onClickbtValider);
            btAnnuler.addEventListener(MouseEvent.CLICK, onClickbtAnnuler);
        }

        private function runValidators():Boolean
        {
            var validators:Array = [ budgetValidator, nameValidator, dateDebutValidator, dateFinValidator ];
            var results:Array = Validator.validateAll(validators);
            if(results.length > 0)
            {
                ConsoviewAlert.afficherSimpleAlert(ResourceManager.getInstance().getString('M22', 'Veuillez_renseigner_les_champs_obligatoi'), ResourceManager.
                                                   getInstance().getString('M22', 'Attention__'));
                return false;
            }
            else
            {
                return true;
            }
        }

        private function onClickbtValider(event:MouseEvent):void
        {
            if(runValidators())
            {
                if(newExercice)
                {
                    enfant.myBudgetService.addExercice(this);
                }
                else
                {
                    enfant.myBudgetService.updateExerciceData(this);
                }
            }
        }

        private function onClickbtAnnuler(evt:MouseEvent):void
        {
            closePopup(null);
        }

        public function closePopup(evt:CloseEvent):void
        {
            PopUpManager.removePopUp(this);
        }
    }
}