<cfcomponent output="false">
	
	<cfset init()>
	<!---
		CONSTRUCTEUR
	--->
	<cffunction name="init" access="public" output="false" returntype="void">
		
		<!--- <cfset test = createObject('component','fr.consotel.consoview.M111.MDMService')> --->
		
		<cfreturn>
	</cffunction>
	
	
	<!---
		CONSTRUCTEUR
	--->
	<cffunction name="invoke" access="remote" output="false" returntype="any">
		<cfargument name="component" type="string" required="true">
		<cfargument name="message" type="string" required="true">
		<cfargument name="jsargument" type="string" required="false">
			
			<cfset _argumentcollection = deserializeJSON(jsargument)>
			
			<!--- <cfmail from="joan.cirencien@saaswedo.com" to="joan.cirencien@saaswedo.com" 
					subject="[ PORTAIL UTILISATEUR - invoke Remotegateway ]" priority="urgent" type="text/html" 
					server="mail-cv.consotel.fr" port="25" failto="joan.cirencien@saaswedo.com" charset="utf-8">
					
					<cfoutput>#component#</cfoutput><br><br>
					<cfoutput>#message#</cfoutput><br><br>
					<cfdump var="#_argumentcollection#"><br><br>
					
			</cfmail> --->
			
			<cfinvoke argumentcollection="#_argumentcollection#" component="#component#" method="#message#" returnvariable="p_retour"/>
			
			<cfif IsDefined("p_retour")>
				<cfreturn p_retour>
			</cfif>
			
		<cfreturn>
	</cffunction>
	
	
	<!---
		
	--->
	<cffunction name="getStructGatewayMDM" access="remote" output="true" returntype="void">
		
		<cfargument name="userStructJSON" 		type="string" required="true">
		<cfargument name="perimetreStructJSON" 	type="string" required="true">
		<cfargument name="diversStructJSON" 			type="string" required="true">
		
		<cftry>
			
			<cfset userStruct = deserializeJSON(userStructJSON)>
			<cfset perimetreStruct = deserializeJSON(perimetreStructJSON)>
			<cfset diversStruct = deserializeJSON(diversStructJSON)>
			
			<cfset SESSION.USER = structNew()>
			<cfset SESSION.USER.CLIENTACCESSID = userStruct.clientaccessid>
			<cfset SESSION.USER.CLIENTID = userStruct.clientid>
			<cfset SESSION.USER.EMAIL = userStruct.email>
			<cfset SESSION.USER.GLOBALIZATION = userStruct.globalization>
			<cfset SESSION.USER.IDGLOBALIZATION = userStruct.idglobalization>
			<cfset SESSION.USER.IDREVENDEUR = userStruct.idrevendeur>
			<cfset SESSION.USER.IDTYPEPROFIL = userStruct.idtypeprofil>
			<cfset SESSION.USER.LIBELLE = userStruct.libelle>
			<cfset SESSION.USER.NOM = userStruct.nom>
			<cfset SESSION.USER.PRENOM = userStruct.prenom>
			
			<cfset SESSION.PERIMETRE = structNew()>
			<cfset SESSION.PERIMETRE.ID_GROUPE = perimetreStruct.id_groupe>
			<cfset SESSION.PERIMETRE.GROUPE = perimetreStruct.groupe>
			<cfset SESSION.PERIMETRE.IDRACINE_MASTER = perimetreStruct.idracine_master>
			<cfset SESSION.PERIMETRE.ID_PERIMETRE = perimetreStruct.id_perimetre>
			
			<cfset SESSION.CODEAPPLICATION = diversStruct.codeapplication>
			<cfset SESSION.OFFREDSN = diversStruct.offreDSN>
			
			<!--- <cfmail from="joan.cirencien@saaswedo.com" to="joan.cirencien@saaswedo.com" 
				subject="[ PORTAIL UTILISATEUR - session créee ]" priority="urgent" type="text/html" 
				server="mail-cv.consotel.fr" port="25" failto="joan.cirencien@saaswedo.com" charset="utf-8">
				
				<cfdump var="#SESSION#">
				
			</cfmail> --->
			
		<cfcatch type="any">
			
			<cfmail from="monitoring@saaswedo.com" to="joan.cirencien@saaswedo.com" cc="samuel.divioka@saaswedo.com"
				subject="[ PORTAIL UTILISATEUR - ERROR CFCATCH remotegateway.cfc ]" priority="urgent" type="text/html" 
				server="mail-cv.consotel.fr" port="25" failto="joan.cirencien@saaswedo.com" charset="utf-8">
				
				<cfdump var="#CFCATCH#">
				
			</cfmail>
			
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>