<cfcomponent output="false" extends="fr.consotel.consoview.api.container.mail.AbstractMail">
	
	<cffunction name="sendMail" returntype="numeric">
		<cfargument name="infosDoc" type="struct" required="true">
			
			<cfset pretour = -1>
			
			<cfif not isDefined("infosDoc.CODEAPPLICATION")>
				<cfset infosDoc['CODEAPPLICATION'] = 1>
			</cfif>	
				
			<cfset compName = "fr.consotel.consoview.api.container.mail.strategy.SENDMailPublicationParProductionClient#infosDoc.CODEAPPLICATION#">	
			<cfset pretour = createobject("component",compName).sendMail(infosDoc)>
			
		<cfreturn pretour>
	</cffunction>
	
</cfcomponent>