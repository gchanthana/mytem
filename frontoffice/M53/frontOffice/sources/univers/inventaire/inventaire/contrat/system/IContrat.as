package univers.inventaire.contrat.system
{
	import mx.collections.ICollectionView;
	
	public interface IContrat
	{
		function createContrat(items:ICollectionView):void;
		
		function getDetailContrat():void;
		
		function udpateInfosContrat():void;
		
		function deleteContrat():void;
		
		function findItems():void;
		
		function get items():ICollectionView;
		
	}
}