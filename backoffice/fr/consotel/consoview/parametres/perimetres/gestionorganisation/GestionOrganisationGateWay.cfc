<cfcomponent extends="fr.consotel.consoview.access.AccessObject" output="false">
	<!--- La liste des organisations d'un groupe de ligne --->
	<cffunction name="getOrganisationsAsQuery" returntype="query" access="remote">
		<cfreturn createObject("component","GestionOrganisation"& getTypePerimetre()).getOrganisationsAsQuery()>		
	</cffunction>
	
	
	<cffunction name="getOrganisationsVo" returntype="OrganisationVo[]" access="remote">			 		
		<cfreturn createObject("component","GestionOrganisation"& getTypePerimetre()).getOrganisationsVo()>		
	</cffunction>
	
	<cffunction name="getOrganisationsPlusAsQuery" returntype="query" access="remote">
		<cfreturn createObject("component","GestionOrganisation"& getTypePerimetre()).getOrganisationsPlusAsQuery()>		
	</cffunction>
	
	
	<cffunction name="getOrganisationsPlusVo" returntype="OrganisationVo[]" access="remote">			 		
		<cfreturn createObject("component","GestionOrganisation"& getTypePerimetre()).getOrganisationsPlusVo()>		
	</cffunction>
	
	
	
	
	<cffunction name="rechercherNoeudASquery" returntype="query" access="remote">			 				
		<cfargument name="chaine" required="true" type="string" />
		<cfargument name="orga" required="true" type="OrganisationVo"/>
					
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_SEARCH_V3.SEARCH_NODES">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#orga.getIDGROUPE_CLIENT()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="" null="true">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="rechercherNoeud" returntype="NoeudVo[]" access="remote">			 				
		<cfargument name="libelle" required="true" type="string" />
		<cfargument name="orga" required="true" type="OrganisationVo"/>			
		
		<cfset tabNoeuds = arrayNew(1)>
		<cfset qListeNoeuds = rechercherNoeudASquery(libelle,orga)>
		<cfLoop query="qListeNoeuds">
			<cfset myNoeud = createObject("component","NoeudVo")>
			<cfscript>				
				myNoeud.setIDGROUPE_CLIENT(qListeNoeuds.IDGROUPE_CLIENT);
				/*myNoeud.setLIBELLE_GROUPE_CLIENT(qListeNoeuds.LIBELLE_GROUPE_CLIENT);
				myNoeud.setCOMMENTAIRES(qListeNoeuds.COMMENTAIRES);
				myNoeud.setISPRIVATE(qListeNoeuds.ISPRIVATE);
				myNoeud.setID_GROUPE_MAITRE(qListeNoeuds.ID_GROUPE_MAITRE);
				myNoeud.setIDDECOUPAGE(qListeNoeuds.IDDECOUPAGE);
				myNoeud.setTYPE_ORGA(qListeNoeuds.TYPE_ORGA);
				myNoeud.setTYPE_DECOUPAGE(qListeNoeuds.TYPE_DECOUPAGE);
				myNoeud.setOPERATEURID(qListeNoeuds.OPERATEURID);
				myNoeud.setDATE_CREATION(qListeNoeuds.DATE_CREATION);
				myNoeud.setNB_ELEM(qListeNoeuds.NB_ELEM);
				myNoeud.setBOOL_ORGA(qListeNoeuds.BOOL_ORGA);
				myNoeud.setMARGE_REFACTURATION(qListeNoeuds.MARGE_REFACTURATION);
				myNoeud.setMODE_AFFECTATION(qListeNoeuds.MODE_AFFECTATION);
				myNoeud.setIDCLIENTS_PV(qListeNoeuds.IDCLIENTS_PV);
				myNoeud.setDATE_MODIF(qListeNoeuds.DATE_MODIF);
				myNoeud.setHISTO(qListeNoeuds.HISTO);
				myNoeud.setIDEMPLOYE(qListeNoeuds.IDEMPLOYE);
				myNoeud.setIDLEVEL_MODELE(qListeNoeuds.IDLEVEL_MODELE);
				myNoeud.setIDORGA_NIVEAU(qListeNoeuds.IDORGA_NIVEAU);
				myNoeud.setIDATE_CREATION_STRING(qListeOrgas.DATE_CREATION_STRING);*/
			</cfscript>
			<cfset arrayAppend(tabNoeuds,myNoeud)>
		</cfLoop>
		<cfreturn tabNoeuds />
	</cffunction>
	
	<cffunction name="getCheminSourceCibleAsQuery" returntype="query" access="remote">			
		<cfargument name="idSource" required="true" type="numeric" />
		<cfargument name="idCible" required="true" type="numeric" />			
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.chemin_source_cible_v2">
			<cfprocparam  value="#idSource#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#idCible#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qChemin">
		</cfstoredproc>
		<cfreturn qChemin>
	</cffunction>
	
	
	<cffunction name="geListeLignesDuNoeudAsQuery" returntype="query" access="remote">			
		<cfargument name="noeud" required="true" type="NoeudVo" />	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_lines">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#noeud.getIDGROUPE_CLIENT()#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="getOrganisationPlusPerimetre" returntype="query" access="remote">
		<cfreturn createObject("component","GestionOrganisation"& getTypePerimetre()).getOrganisationPlus()>		
	</cffunction>

	<cffunction name="getOrganisationByAcces" returntype="query" access="remote">
		<cfreturn createObject("component","GestionOrganisation"& getTypePerimetre()).getOrganisationByAcces()>		
	</cffunction>	
	
	
</cfcomponent>