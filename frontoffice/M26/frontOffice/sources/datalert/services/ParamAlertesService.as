package datalert.services
{
	import datalert.vo.LigneVO;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class ParamAlertesService extends EventDispatcher
	{
		public var jsonRecieved:String = '';
		public var ligne:LigneVO;
		public function ParamAlertesService()
		{
		}
		
		public function registerParams(jsonSent:String, listIdSoustete:ArrayCollection):void
		{
			if (jsonSent != "")
			{
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M26.AlertsManager",
					"registerParams",
					registerParams_handler,null);
				
				RemoteObjectUtil.callService(op, jsonSent, listIdSoustete);
			}
			else
				trace("Le JSON à envoyer est vide. Appel à la base de données annulé.");
		}
		
		public function registerParams_handler(re:ResultEvent):void
		{
			trace("Requete effectuee : " + re.result);
			if (re.result.toString() == 1)
			{
				Alert.show(ResourceManager.getInstance().getString('M26', 'Param_tres_enregistr_s_'));
				dispatchEvent(new Event("ALERTSREGISTERED"));
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M26', 'Une_erreur_est_survenue_durant_l_enregistrement_'));
		}
		
		public function retrieveParams():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M26.AlertsManager",
				"retrieveParams",
				retrieveParams_handler,null);
			
			RemoteObjectUtil.callService(op, ligne.IDSOUSTETE);
		}
		public function retrieveParams_handler(re:ResultEvent):void
		{
			if (re.result is Object)
			{
				this.jsonRecieved = re.result.toString();
				trace("Conversion en String : " + this.jsonRecieved);
				dispatchEvent(new Event("JSONRECIEVED"));
			}
			else
				trace("Erreur lors de la réception des paramètres d'alerte depuis la base.");
		}
	}
}