package univers.inventaire.commande.ihm
{
	
	import composants.util.article.Article;
	import composants.util.produits.ProduitsUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	 
	[Bindable]
	public class SelectionAbosOptionsImpl extends SelectionBaseViewImpl
	{		    
	    public var btFermer:Button;
	    public var cboThemes:ComboBox;
	    
	    public var cbkAbonnements:CheckBox;
	    public var cbkAbosVoix:CheckBox;
	    public var cbkAbosData:CheckBox;
	    public var cbkAbosPushMail:CheckBox;
	    public var cbkOptions:CheckBox;
	    public var cbkOptsVoix:CheckBox;
	    public var cbkOptsData:CheckBox;
	    public var cbkOptsAutre:CheckBox;
	    public var cbkAutres:CheckBox;
	    
	    public var rbTous:RadioButton;
	    public var rbFavouris:RadioButton;
	    public var rbgFavoris:RadioButtonGroup;
	    
	    
	    public var lblOperateur : Label;
	    
	    private var _produitsUtils : ProduitsUtils = new ProduitsUtils();
	    private var _isInit:Boolean = true;
	    
	    public function get produitsUtils():ProduitsUtils
	    {
	    	return _produitsUtils;
	    }
	    
	    public function set produitsUtils(value:ProduitsUtils):void
	    {
	    	_produitsUtils = value
	    }
	    
	    
	    private var _article : Article;    
	   	public function set article(value : Article):void{	   		
	   		_article = value 
	   	}
	   	public function get article():Article{
	   		return _article
	   	}
	    
	    public function SelectionAbosOptionsImpl()
		{
			Alert.buttonWidth = 100;
	    	Alert.okLabel = "Fermer";
	    	_isInit = true;
		}
		
		//---- protcted --------------------------------------------
		
		
		
		//-- Handlers -----------------------------------------------
		protected function cbkAbonnementsClickHandler(event:MouseEvent):void
		{	
			cbkAbosVoix.selected = cbkAbosData.selected = cbkAbosPushMail.selected = cbkAbonnements.selected ;
			filtrerDgListe()
		}
		
		protected function cbkOptionsClickHandler(event:MouseEvent):void
		{	
			cbkOptsVoix.selected = cbkOptsData.selected = cbkOptsAutre.selected = cbkOptions.selected;
			filtrerDgListe()
		}
		
		protected function cbkClickHandler(event:MouseEvent):void
		{	
			filtrerDgListe();
		}
		/**
		 * protected
		 * */
		public function rbgFavorisChangeHandler(event:Event):void
	    {
	    	filtrerDgListe();
	    }
		
		protected function filtrerDgListe():void
		{
			if(dgListe.dataProvider != null)
			{
				(dgListe.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListe.dataProvider as ArrayCollection).refresh();
			}	
		}
		
		protected function filterFunction(item:Object):Boolean
		{
			
			var rfiltre:Boolean = true;
			var rfiltreAbos:Boolean = true;
			var rfiltreConsos:Boolean = true;
			var rfiltreAutre:Boolean = true;
			
			if(cbkAbonnements.selected)
			{
				//filtre sur les abos AbosPushMail			
				rfiltreAbos = rfiltreAbos && (	(cbkAbosPushMail.selected && (item.IDTHEME_PRODUIT == ProduitsUtils.THEME_ABOSPUSHMAIL))
										||//filtre sur les abos AbosVoix
									 	(cbkAbosVoix.selected && (item.IDTHEME_PRODUIT == ProduitsUtils.THEME_ABOSVOIX))
									 	||//filtre sur les abos AbosData
									 	(cbkAbosData.selected && (item.IDTHEME_PRODUIT == ProduitsUtils.THEME_ABOSDATA)))		
				 	
			}
			else
			{
				rfiltreAbos = false;
			}
			
						
			if(cbkOptions.selected)
			{
									//filtre sur les abos OptsAutre			
				rfiltreConsos  = rfiltreConsos && (( cbkOptsAutre.selected && (item.IDTHEME_PRODUIT == ProduitsUtils.THEME_OPTSAUTRE))
									 ||//filtre sur les Opts OptsVoix
				 					(cbkOptsVoix.selected && (item.IDTHEME_PRODUIT == ProduitsUtils.THEME_OPTSVOIX))
									 ||//filtre sur les Opts OptsData
				 					(cbkOptsData.selected && (item.IDTHEME_PRODUIT == ProduitsUtils.THEME_OPTSDATA)))	
			}
			else
			{
				rfiltreConsos = false;
			}
			
			if(cbkAutres.selected){
				//filtre sur les autres
				rfiltreAutre = rfiltreAutre && cbkAutres.selected && ((item.IDTHEME_PRODUIT == ProduitsUtils.THEME_ACCESSOIRES)||(item.IDTHEME_PRODUIT == ProduitsUtils.THEME_LOCATIONSTERMINAUX));	
			}
			else
			{
				rfiltreAutre = false;
			}
			
			
			//filtre sur le libelle du prosduit
			rfiltre = (rfiltreAbos||rfiltreConsos||rfiltreAutre) && (String(item.LIBELLE_PRODUIT).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
							&&((rbgFavoris.selectedValue == "favoris")?(item.FAVORI == 1):(true)) 
			
			
			return rfiltre
		}
		
	    protected function creationCompleteHandler(event : FlexEvent):void
	    {
	    	if(commande != null)
	    	{
	    		if(commande.IDOPERATEUR > 0)
	    		{
	    			produitsUtils.fournirListeProduitCatalogueOperateur(commande.IDOPERATEUR);
	    		}
	    		else
	    		{
	    			Alert.show("Vous devez selectionner un opérateur!","Erreur");
	    		}
	    	}	
	    }
	    
	    
	    /**
	     * 
	     * @param event
	     */
	    override protected function dgListeUpdateCompleteHandler(event:FlexEvent=null): void
	    {
	    	if(dgListe.initialized && _isInit && produitsUtils.listeProduitsCatalogue)
	    	{
	    		filtrerDgListe()
	    		
	    		_isInit = false;
	    	}
	    }
	    
	    
	    
	    
	   
	    
	    override protected function txtFiltreChangeHandler(event:Event):void
	    {
	    	filtrerDgListe()
	    }
	    
	    protected function dgListeDoubleClickHandler(event:MouseEvent):void
	    {
	    	ajouterLesRessources();
	    } 
	    
	    protected function btAjouterClickhandler(event:MouseEvent):void
	    {
	    	/* if(dgListe.selectedItem != null)
	    	{
	    		//TEST
		    	if(article != null)
		    	{	    		
		    		article.addRessource(dgListe.selectedItem);
		    		dispatchEvent(new Event(AJOUTER_CLICKED))
		    	}
		    }else{
		    	Alert.show("Vous devez sélectionner un abonnement ou une option");
		    } */
		    ajouterLesRessources()
	    }
	    
	   
	    
	    
	    protected function btFermerClickHandler(event:MouseEvent):void
	    {
	    	PopUpManager.removePopUp(this);
	    	dispatchEvent(new Event(FERMER_CLICKED))
	    }
	    
	    public function favorisClickHandler(event:MouseEvent):void
	    {
	    	if(event.currentTarget.selected)
	    	{
	    		produitsUtils.ajouterProduitAMesFavoris(dgListe.selectedItem)	
	    	}
	    	else
	    	{
	    		produitsUtils.supprimerProduitDeMesFavoris(dgListe.selectedItem)
	    	}
	    }
	    
	    
	   
	    
		//-----
		protected function ajouterLesRessources():void
	    {
	    	var tabRessources:Array = dgListe.selectedItems;
	    	var len:int = tabRessources.length;
	    		    	  
	    	if(len > 0)
	    	{	
	    		if(article != null)
	    		{
	    			for(var i:int = 0 ; i < len; i++)
		    		{
		    			article.addRessource(tabRessources[i]);	
		    		}
		    		 PopUpManager.removePopUp(this);
		    		dispatchEvent(new Event(AJOUTER_CLICKED))	
	    		}
		    }else{
		    	Alert.show("Vous devez sélectionner un abonnement ou une option");
		    }
		    
		   
	    }
	    
	    
	    protected function numberCompareFunction(itemA:Object,itemB:Object):int
	    {
	    	return ObjectUtil.compare(itemA.PRIX_UNIT,itemB.PRIX_UNIT);
	    }
			
	}
}