package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Theme")]
	
	
	 
	[Bindable]
	public class Theme
	{

		public var id:Number = 0;
		public var libelle:String = "";
		public var typeTheme:String ="";
		public var idCategorie:Number = 0;

		public function Theme()
		{
		}

	}
}