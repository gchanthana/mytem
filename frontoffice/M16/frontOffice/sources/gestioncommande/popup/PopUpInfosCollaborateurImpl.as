package gestioncommande.popup
{	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ValidationResultEvent;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import mx.resources.ResourceManager;
	import mx.validators.EmailValidator;
	import mx.validators.StringValidator;
	
	import composants.com.hillelcoren.components.AutoComplete;
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import gestioncommande.entity.Collaborateur;
	import gestioncommande.entity.OrgaVO;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.GestionParcMobileEvent;
	import gestioncommande.services.EmployeService;
	import gestioncommande.services.SiteService;

	[Bindable]
	public class PopUpInfosCollaborateurImpl extends TitleWindow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cbxCivilite			:ComboBox;
		public var cbxNiveau			:ComboBox;
		public var cbxStatut			:ComboBox;
		public var cbxPubliAnu			:ComboBox;
		public var cbxOrganisations		:ComboBox;
		
		public var cbxSites				:AutoComplete;

		public var tbxPrenom			:TextInput;
		public var tbxNom				:TextInput;
		public var tbxFonction			:TextInput;
		public var tbxEmail				:TextInput;
		public var tbxIdentifiant		:TextInput;
		public var tbxCodeInterne		:TextInput;
		public var tbxMatricule			:TextInput;
		public var tbxPosition			:TextInput;
		public var tbxC1				:TextInput;
		public var tbxC2				:TextInput;
		public var tbxC3				:TextInput;
		public var tbxC4				:TextInput;

		public var cvdEntree			:CvDateChooser;
		public var cvdSortie			:CvDateChooser;

		public var tbaCommentaires		:TextArea;
		
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		
		public var validation_email		:EmailValidator;
		
		public var validation_matricule	:StringValidator;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUP
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpOrgaCible		:FicheOrgaIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _employeSrv			:EmployeService = new EmployeService();
		
		private var _siteService		:SiteService = new SiteService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		public var infosCollab			:Collaborateur = new Collaborateur();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//	
		
		public var listeOrganisations	:ArrayCollection = new ArrayCollection();
		public var listCivilite			:ArrayCollection = new ArrayCollection([
																					{label:ResourceManager.getInstance().getString('M16', 'Mr_'), data:"0"},
																					{label:ResourceManager.getInstance().getString('M16', 'Mme_'), data:"1"},
																					{label:ResourceManager.getInstance().getString('M16', 'Mlle_'), data:"2"}
																				]);
		public var listStatut			:ArrayCollection = new ArrayCollection([
																					{label:'In', data:"1"},
																					{label:'Out', data:"0"}
																				]);
		public var listPublication		:ArrayCollection = new ArrayCollection([
																					{label:ResourceManager.getInstance().getString('M16', 'Oui'), data:"1"},
																					{label:ResourceManager.getInstance().getString('M16', 'Non'), data:"0"}
																				]);
		public var listNiveau			:ArrayCollection = new ArrayCollection([
																					{label:"1", data:"1"},
																					{label:"2", data:"2"},
																					{label:"3", data:"3"},
																					{label:"4", data:"4"},
																					{label:"5", data:"5"}
																				]);
		public var listeSites			:ArrayCollection = new ArrayCollection();
		
		public var popUpTitle			:String = '';
		public var C1 					:String = ResourceManager.getInstance().getString('M16', '_Libell__1_');
		public var C2  					:String = ResourceManager.getInstance().getString('M16', '_Libell__2_');
		public var C3 					:String = ResourceManager.getInstance().getString('M16', '_Libell__3_');
		public var C4 					:String = ResourceManager.getInstance().getString('M16', '_Libell__4_');
		
		public var idgroupeClient		:int = -1;
		public var idCollaborateur		:int = -1;
		public var idPool				:int = -1;
		
		private var _idcible			:int = -1;
		private var _idCivilite			:int = 1;
		private var _idNiveau			:int = 1;
		private var _idStatut			:int = 1;
		private var _idPubli			:int = 1;
		private var _idSite				:int = 0;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PopUpInfosCollaborateurImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					LISTENER IHM
	//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			if(idCollaborateur > 0 && idgroupeClient > 0)
			{
				popUpTitle = ResourceManager.getInstance().getString('M16', 'Mise___jour_fiche_collaborateur');
				btnValider.enabled = true;
				tbxPosition.width = 228;
				getInfosCollaborateur();
			}
			else
			{
				popUpTitle = ResourceManager.getInstance().getString('M16','Fiche_nouveau_collaborateur');
				btnValider.enabled = false;
				getSiteLivraison();
			}
			
			getMatriculeAuto();
			getEmployeChampsPerso();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function changeInputHandler(e:Event):void
		{
			if(cbxCivilite.selectedIndex != -1 && tbxNom.text != "" && tbxPrenom.text != ""  && tbxMatricule.text != "")
				btnValider.enabled = true;
			else
				btnValider.enabled = false;

			if(btnValider.enabled)
			{
				var matriculeValidation	:ValidationResultEvent = validation_matricule.validate();
				
				if(matriculeValidation.type == ValidationResultEvent.INVALID)
					btnValider.enabled = false;
				else
					btnValider.enabled = true;
			}
		}
		
		protected function btnModifierPositionClickHandler(me:MouseEvent):void
		{
			if(cbxOrganisations.selectedItem == null)
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_une_organisation__'), 'Consoview', null);
			else
			{
				_popUpOrgaCible 			= new FicheOrgaIHM();
				_popUpOrgaCible.libelleOrga = (cbxOrganisations.selectedItem as OrgaVO).libelleOrga;
				_popUpOrgaCible.idOrga 		= (cbxOrganisations.selectedItem as OrgaVO).idOrga;
				_popUpOrgaCible.myOrga		= cbxOrganisations.selectedItem as OrgaVO;
				
				_popUpOrgaCible.addEventListener(GestionParcMobileEvent.VALIDE_FICHE_ORGA, updateOrgaHandler);
				
				PopUpManager.addPopUp(_popUpOrgaCible, this.parent, true);
				PopUpManager.centerPopUp(_popUpOrgaCible);
			}
		}

		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(idCollaborateur > 0)
				updateInfosCollaborateur();
			else
				setInfosCollaborateur();
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			closeHandler(me);
		}

		protected function cbxCloseHandler(e:Event):void
		{
			if(e.target as ComboBox)
			{
				var myCbx	:ComboBox = e.target as ComboBox;
				var myId	:int = 1;
				
				if(myCbx.selectedItem != null)
					myId = myCbx.selectedItem[getLabelFieldId(myCbx.id)];
				else
				{
					if(myCbx.id == 'cbxSites')
						myId = 0;
				}
				
				switch(myCbx.id)
				{
					case 'cbxCivilite'	: _idCivilite = myId; break;
					case 'cbxNiveau'	: _idNiveau   = myId; break;
					case 'cbxStatut'	: _idStatut   = myId; break;
					case 'cbxPubliAnu'	: _idPubli 	  = myId; break;
					case 'cbxSites'		: _idSite 	  = myId; break;
				}
			}
		}
		
		protected function cbxOrganisationsCloseHandler(e:Event):void
		{
			
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		private function updateOrgaHandler(gpme:GestionParcMobileEvent):void 
		{
			tbxPosition.text = _popUpOrgaCible.myService.myDatas.cheminSelected.chemin;
			_idcible 		 = _popUpOrgaCible.myService.myDatas.cheminSelected.idFeuille;
			
			PopUpManager.removePopUp(_popUpOrgaCible);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function employeChampsPersoHandler(cmde:CommandeEvent):void
		{
			C1 = _employeSrv.champsPersoEmploye.C1;
			C2 = _employeSrv.champsPersoEmploye.C2;
			C3 = _employeSrv.champsPersoEmploye.C3;
			C4 = _employeSrv.champsPersoEmploye.C4;
		}
		
		private function getInfosCollaborateurHandler(cmde:CommandeEvent):void
		{
			infosCollab = _employeSrv.employe;
			attributInfosCollaborateur();
			getSiteLivraison();
		}
		
		private function setInfosCollaborateurHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Collaborateur_cr__'), SystemManager.getSWFRoot(this));
			
			dispatchEvent(new Event("REFRESH_EMPLOYES", true));
			
			closeHandler(cmde);
		}
		
		private function updateInfosHandler(cmde:CommandeEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Collaborateur_mis___jour'), SystemManager.getSWFRoot(this));
			
			dispatchEvent(new Event("REFRESH_EMPLOYES", true));
			
			closeHandler(cmde);
		}
		
		private function siteLivraisonFacturationHandler(cmde:CommandeEvent):void
		{
			listeSites = _siteService.listeSitesLivraison;
			
			var len:int = listeSites.length;
			
			if(len > 0)
			{
				if(len == 1)
					cbxSites.selectedItemId = listeSites[0].IDSITE_PHYSIQUE;
				else
				{
					cbxSites.prompt 		= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
					cbxSites.selectedItemId = -1;
				}
			}
			else
			{
				cbxSites.prompt 		= ResourceManager.getInstance().getString('M16', 'Aucun_site');
				cbxSites.selectedItemId = -1;
			}
			
			if(_idSite > 0)
			{
				for(var i:int = 0;i < len;i++)
				{
					//					if(listeSites[i].IDSITE_PHYSIQUE == _idSite)
					cbxSites.selectedItemId = _idSite;
				}
			}
		}
		
		private function matriculeAutoHandler(cmde:CommandeEvent):void
		{
			if(_employeSrv.matricule.IS_ACTIF > 0)
			{
				if(idCollaborateur < 1)
					tbxMatricule.text = _employeSrv.matricule.CURRENT_VALUE;
				
				tbxMatricule.enabled = false;
			}
			else
				tbxMatricule.enabled = true;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getEmployeChampsPerso():void
		{
			_employeSrv.fournirChampsPersoEmploye();
			_employeSrv.addEventListener(CommandeEvent.EMPLOYE_CHAMPS, employeChampsPersoHandler);
		}
		
		private function getInfosCollaborateur():void
		{
			_employeSrv.fournirDetailEmploye(idCollaborateur, idgroupeClient, idPool);
			_employeSrv.addEventListener(CommandeEvent.EMPLOYE_LISTED, getInfosCollaborateurHandler);
		}
		
		private function setInfosCollaborateur():void
		{
			if(checkDataIsValidate())
			{
				infosCollab = formatToCollaborateur();
				_employeSrv.createEmploye(infosCollab, idPool, createOrganisationsClob());
				_employeSrv.addEventListener(CommandeEvent.EMPLOYE_CREATED, setInfosCollaborateurHandler);
			}				
		}
		
		private function updateInfosCollaborateur():void
		{
			if(checkDataIsValidate())
			{
				infosCollab = formatToCollaborateur();
				_employeSrv.updateEmploye(infosCollab, idCollaborateur, idgroupeClient, createOrganisationsClob());
				_employeSrv.addEventListener(CommandeEvent.EMPLOYE_UPDATED, updateInfosHandler);
			}				
		}
		
		private function getSiteLivraison():void
		{
			_siteService.fournirListeSiteLivraisonsFacturation(idPool);
			_siteService.addEventListener(CommandeEvent.LISTED_SITES, siteLivraisonFacturationHandler);
		}
		
		private function getMatriculeAuto():void
		{
			_employeSrv.getMatriculeAuto();
			_employeSrv.addEventListener(CommandeEvent.MATRICULE_AUTO, matriculeAutoHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function attributInfosCollaborateur():void
		{
			//---REMPLISAGE TEXINPUT
			tbxCodeInterne.text = infosCollab.CODE_INTERNE;
			tbxEmail.text 		= infosCollab.EMAIL
			tbxFonction.text	= infosCollab.FONCTION_EMPLOYE
			tbxIdentifiant.text = infosCollab.CLE_IDENTIFIANT;
			tbxMatricule.text 	= infosCollab.MATRICULE;
			tbxNom.text 		= infosCollab.NOM;
			tbxPosition.text 	= '';
			tbxPrenom.text 		= infosCollab.PRENOM;
			
			tbaCommentaires.text = infosCollab.COMMENTAIRE;
			
			//---REMPLISAGE COMBOBOX
			cbxNiveau.selectedIndex = infosCollab.NIVEAU-1;
			
			_idNiveau = infosCollab.NIVEAU;
			
			if(infosCollab.INOUT == 0)
				cbxStatut.selectedIndex = 1;
			
			_idStatut = infosCollab.INOUT;
			
			cbxCivilite.selectedIndex = whatIsCivilite(int(infosCollab.CIVILITE));
			
			_idCivilite = Number(infosCollab.CIVILITE);
			
			cbxPubliAnu.selectedIndex = whatIsPublication(infosCollab.BOOL_PUBLICATION);
			
			_idPubli = infosCollab.BOOL_PUBLICATION;
			
			_idSite = infosCollab.IDSITE_PHYSIQUE;
			
			//---REMPLISAGE CVDATECHOOSER
			cvdEntree.selectedDate	= infosCollab.DATE_ENTREE;
			cvdSortie.selectedDate 	= infosCollab.DATE_SORTIE;
			
			tbxC1.text 				= infosCollab.C1;
			tbxC2.text 				= infosCollab.C2;
			tbxC3.text 				= infosCollab.C3;
			tbxC4.text 				= infosCollab.C4;
			
			listeOrganisations 		= infosCollab.LISTEORGANISATIONS;
		}
		
		private function whatIsCivilite(civilite:int):int
		{
			var idx:int = 0;
			
			if(!isNaN(civilite))
				idx = civilite;

			return idx;
		}
		
		private function whatIsPublication(publication:int):int
		{
			var idx:int = 0;
			
			if(publication == 0)
				idx = 0;
			
			return idx;
		}

		private function getLabelFieldId(idCbx:String):String
		{
			var label:String = '';
			
			switch(idCbx)
			{
				case 'cbxCivilite'	: label = 'data'; break;
				case 'cbxNiveau'	: label = 'data'; break;
				case 'cbxStatut'	: label = 'data'; break;
				case 'cbxPubliAnu'	: label = 'data'; break;
				case 'cbxSites'		: label = 'IDSITE_PHYSIQUE'; break;
			}
			
			return label;
		}
		
		private function checkDataIsValidate():Boolean
		{
			var isOK:Boolean = false;
			
			if(cbxCivilite.selectedIndex != -1 && tbxNom.text != "" && tbxPrenom.text != ""  && tbxMatricule.text != "")
				isOK = true;
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_les_champs_obligatoires'), 'Consoview', null);
				
				return false;
			}
			
			var matriculeValidation	:ValidationResultEvent = validation_matricule.validate();
			var emailOk				:Boolean = false;
			
			if(tbxEmail.text != "") 
			{
				var emailValidation		:ValidationResultEvent = validation_email.validate();
				
				if(emailValidation.type == ValidationResultEvent.INVALID) 
				{
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur_dans_la_saisie_du_champ_email'), 'Consoview');
					return false;
				}
				else
					emailOk = true;
			}
			else if(matriculeValidation.type == ValidationResultEvent.INVALID) 
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Vous_devez_saisir_un_num_ro_de_matricule'), 'Consoview');
				
				return false;
			}
			else
				emailOk = true;
			
			return isOK && emailOk;
		}

		private function createOrganisationsClob():String
		{
			var myXML	:String = '';
			var len		:int = listeOrganisations.length;

			myXML += "<orgas>";
			
			for(var i : int = 0;i < len;i++)
			{
				if((listeOrganisations[i] as OrgaVO).chemin != null)
				{
					var path:String = earseCaracteresSpeciaux((listeOrganisations[i] as OrgaVO).chemin);
					
					myXML += "<orga>";
					myXML += "<idregleorga>" + (listeOrganisations[i] as OrgaVO).idRegleOrga + "</idregleorga>"
					myXML += "<chemin>" + path + "</chemin>";
					myXML += "<idcible>" + (listeOrganisations[i] as OrgaVO).idCible + "</idcible>";
					myXML += "</orga>";
				}
			}
			
			myXML += "</orgas>";
			
			return myXML;
		}
		
		private function earseCaracteresSpeciaux(mypath:String):String
		{
			var pattern	:RegExp = /&/g;
			
			mypath = mypath.replace(pattern, '');
			
			return mypath;
		}
		
		private function formatToCollaborateur():Collaborateur
		{
			var employeObj:Collaborateur 	= new Collaborateur();
				employeObj.CIVILITE 		= _idCivilite.toString();
				employeObj.NOMPRENOM 		= tbxPrenom.text + ' ' + tbxNom.text;
				employeObj.NOM				= tbxNom.text;
				employeObj.PRENOM			= tbxPrenom.text;
				employeObj.EMAIL 			= tbxEmail.text;
				employeObj.FONCTION_EMPLOYE = tbxFonction.text;
				employeObj.MATRICULE 		= tbxMatricule.text;
				employeObj.CODE_INTERNE 	= tbxCodeInterne.text;
				employeObj.CLE_IDENTIFIANT 	= tbxIdentifiant.text;
				employeObj.INOUT 			= _idStatut;
				employeObj.STATUS_EMPLOYE	= _idStatut.toString();
				employeObj.NIVEAU 			= _idNiveau;
				employeObj.BOOL_PUBLICATION = _idPubli;
				employeObj.COMMENTAIRE  	= tbaCommentaires.text;
				employeObj.DATE_CREATION	= cvdEntree.selectedDate;
				employeObj.DATE_ENTREE		= cvdEntree.selectedDate;
				employeObj.DATE_SORTIE		= cvdSortie.selectedDate;
				employeObj.C1				= tbxC1.text;
				employeObj.C2				= tbxC2.text;
				employeObj.C3				= tbxC3.text;
				employeObj.C4				= tbxC4.text;
				
			if(cbxSites.selectedItem != null)
				employeObj.IDSITE_PHYSIQUE	= cbxSites.selectedItem.IDSITE_PHYSIQUE;
			else
				employeObj.IDSITE_PHYSIQUE	= 0;
			
			return employeObj;
		}
		
	}
}
