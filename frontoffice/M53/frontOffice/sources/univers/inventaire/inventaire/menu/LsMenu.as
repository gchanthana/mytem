package univers.inventaire.inventaire.menu
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	
	/**
	 * Classe Onglet Journaux
	 * 
	 * */
	public class LsMenu extends LsMenuIHM
	{
		private var myDp:ArrayCollection=new ArrayCollection([
						{label:"Inventaire des lignes",template:"liste",libelle:"Liste_Des_Lignes"}
						
					]);
		
		/**
		 * Constructeur 
		 * */
		public function LsMenu()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		
		//Initialisation de l'IHM 
		//Affectation des ecouteurs d'évènements
		private function initIHM(event:Event):void {
			btLsGenerate.addEventListener(MouseEvent.CLICK,exporter);
			myLsGrid.dataProvider=myDp;
		}
		
        //Export ----------------------------------------------------------------------------------
        
        //Handler du clique sur le bouton exporter
        private function exporter(me : MouseEvent):void{
        	if (myLsGrid.selectedIndex == -1/*  && treeOrga.nodeInfo == null */){
        		lblErrorWf.text = "Sélectionnez un rapport";
        	}else{
        		displayExport();
        	}        	       
        }
        
         //Exporte le journal affiché suivant le format passé en paramètre        
         //param in format le format sous lequel on souhaite exporter
        private function displayExport():void {
			var url:String = cv.NonSecureUrlBackoffice+"/fr/consotel/consoview/inventaire/rapport/ls/listeLignes.cfm";
            var variables:URLVariables = new URLVariables();
            variables.template = myLsGrid.selectedItem.template;
            variables.libelle = myLsGrid.selectedItem.libelle;
            variables.chaine =  txtChaine.text; 
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            navigateToURL(request,"_blank");
        } 			    
        
        //Fin Export ------------------------------------------------------------------------------
        
		/**
		*  Fait les actrions nécessaire au changement de périmetre
		* */
		public function onPerimetreChange():void{
		
			//dispatchEvent(new Event("PerimetreChange"));
			//synthese.removeChild(treeOrga);
			//treeOrga = null;
			//treeOrga = new InvSearchTree();			
			//synthese.addChildAt(treeOrga,0);
			//callLater(initTree);
			
		}
	}
}