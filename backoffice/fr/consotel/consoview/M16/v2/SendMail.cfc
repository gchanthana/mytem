<cfcomponent displayname="fr.consotel.consoview.M16.v2.SendMail" hint="Gestion de l'envoi de mails" output="false">
	<cfsetting enablecfoutputonly="true"/>

	<cfset properties 	 = structNew()>
	<cfset destMailError = 'monitoring@saaswedo.com'>
	<cfset fromMailError = 'monitoring@saaswedo.com'>
	<cfset toMail 		 = 'monitoring@saaswedo.com'>

	<!--- RECUPERER LES INFORMATIONS POUR LE SERVEUR DE MAILS --->
	<cffunction access="public" name="getProperties" returntype="Struct">
		<cfargument name="codeapp" 	type="Numeric" 	required="true">
 		<cfargument name="key" 		type="String" 	required="true"/>
			
			<cfset properties 	 	= structNew()>
			
			<cfset mailProperties  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
			<cfset rsltProperties  	= mailProperties.get_appli_properties(codeapp, key)>
			
			<cfloop query="rsltProperties">
				<cfset properties[rsltProperties.KEY] = rsltProperties.VALUE>
			</cfloop>
			
			<cfif NOT isDefined('properties.MAIL_SERVER')>
				<cfset properties.MAIL_SERVER = 'mail-cv.consotel.fr'>
			</cfif>
			
			<cfif properties.MAIL_SERVER EQ ''>
				<cfset properties.MAIL_SERVER = 'mail-cv.consotel.fr'>
			</cfif>
			
			<cfif NOT isDefined('properties.MAIL_PORT')>
				<cfset properties.MAIL_PORT = 25>
			</cfif>
			
			<cfif properties.MAIL_PORT EQ ''>
				<cfset properties.MAIL_PORT = 25>
			</cfif>
			
			<cfif NOT isDefined('properties.MAIL_FROM_COMMANDE')>
				<cfset properties.MAIL_FROM_COMMANDE = 'no-reply@consotel.fr'>
			</cfif>
			
			<cfif properties.MAIL_FROM_COMMANDE EQ ''>
				<cfset properties.MAIL_FROM_COMMANDE = 'no-reply@consotel.fr'>
			</cfif>
			
		<cfreturn properties>
	</cffunction>


	<!--- ENVOI DE MAIL DE MODIFICATION DE COMMANDE --->
	<cffunction access="public" name="sendMailMOD" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" 	required="true">

			<cfset mailing  = createObject("component","fr.consotel.consoview.M01.Mailing")>

			<cfset myCurrentDir = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset ImportTmp	= "/container/M16/">

			<cfset eventStatut 	= ARGUMENTS.eventObject.getReportStatus()>
			<cfset eventType	= ARGUMENTS.eventObject.getEventType()>
			<cfset eventUuid	= ARGUMENTS.eventObject.getEventTarget()>
			<cfset eventJobId	= ARGUMENTS.eventObject.getJobId()>
			
			<cfset infosMail = mailing.getMailToDatabase(#eventUuid#)>
			<cfif infosMail.RecordCount GT 0>
							
				<cfset thisCodeApp 	= infosMail['CODE_APPLI'][infosMail.RecordCount]>
				<cfset mailSubject 	= infosMail['MAIL_SUBJECT'][infosMail.RecordCount]>
				<cfset mailTo 		= infosMail['MAIL_TO'][infosMail.RecordCount]>
				<cfset myFile 		= infosMail['MAIL_BODY'][infosMail.RecordCount]>
				<cfset mailBcc 		= infosMail['MAIL_BCC'][infosMail.RecordCount]>
				
				<cfset getProperties(thisCodeApp, 'NULL')>
				
				<cfset root = "#myCurrentDir##eventUuid#">
			
				<cfif DirectoryExists('#root#')>
				
					<cfset eventUuid = createUUID()>
					
					<cfset root = "#myCurrentDir##eventUuid#">
					
				</cfif>
				
				<cftry>
			
					<cfdirectory action="Create" directory="#root#" type="dir" mode="777">
					
					<cffile action="copy" source="#ImportTmp##eventUuid#/#myFile#" destination="#root#/#myFile#">
					
					<cfdirectory name="dGetDir" directory="#root#" action="List">
					
					<cfmail from="#properties.MAIL_FROM_COMMANDE#" to="#mailTo#" bcc="#mailBcc#,monitoring@saaswedo.com" subject="#mailSubject#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
									
						<cfinclude template="/fr/consotel/consoview/M16/v2/#eventUuid#/#myFile#">
	 				
					</cfmail>
					
					<cffile action="READ" file="#ImportTmp##eventUuid#/#myFile#" variable="fichier">
					<!--- DEBUT LOG --->
					<cfsavecontent variable="body">	
						<cfoutput>#fichier#</cfoutput>
					</cfsavecontent>
					
					<cfmail from="monitoring@saaswedo.com" to="monitoring@saaswedo.com" subject="#mailSubject#" type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
						<cfdump var="#infosMail#">
					</cfmail>
					
					<cftry>
					
						<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
						<cfset mLog.setIDMNT_CAT(16)>
						<cfset mLog.setIDMNT_ID(101)>
						<cfset mLog.setSUBJECTID("JOB_ID : " & eventJobId & " | " & mailSubject)>
						<cfset mLog.setBODY(body)>
						<cfset mLog.setIDRACINE(0)>
						
						<cfinvoke 
							component="fr.consotel.api.monitoring.MLogger"
							method="logIt"  returnVariable = "result">
							<cfinvokeargument name="mLog" value="#mLog#">  
						</cfinvoke>	
						
					<cfcatch type="any">
						<cfmail from="MLog <monitoring@saaswedo.com>" 
								to="monitoring@saaswedo.com" 
								server="mail-cv.consotel.fr" 
								port="25" type="text/html"
								subject="[EVT-9999] Errereur dans l'enregistrement d'un log de monitoring">
									<cfdump var="#cfcatch#">						
						</cfmail>
					</cfcatch>
					
				</cftry>
				<!--- FIN LOG --->
					
					
					
					
					
					
					
					
					
					
					<cfloop query="dGetDir">
						
						<cfif type NEQ "DIR">
							
							<cffile action="delete" file="#root#/#dGetDir.name#">
							
						</cfif> 
						
					</cfloop>
				
					<cfdirectory action="delete" directory="#root#">

				<cfcatch type="any">
				
					<cfmail from="#fromMailError#" to="monitoring@saaswedo.com" failto="monitoring@saaswedo.com" subject="[MOD - CMD]Echec envoi de mail de modification de commande" type="text/html" server="mail.consotel.fr" port="26">
						
						<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br/></cfoutput>
						<cfoutput>Echec envoi de mail de modification de commande.<br /><br/></cfoutput>
						
						<cfoutput>
						
							POST PROCESS RESULT :<br /><br />
							
							RESPOSITORY_PATH	: #ImportTmp##eventUuid#/#myFile#<br/><br/>
							
							SERVER				: #CGI.SERVER_NAME#:#CGI.SERVER_PORT#<br/>
							REMOTE				: HOST #CGI.REMOTE_HOST# | ADDR #CGI.REMOTE_ADDR# | USER #CGI.REMOTE_USER#<br/>
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br/>
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br/>
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br/>
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br/>
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br/>
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br/>
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br/><br/><br/>
								
						
						</cfoutput>
						
						<cfoutput>
						
							ERREUR (CATCH DUMP - Message) :<br/><br/>
							
							<cfdump var="#CFCATCH.Message#"><br/><br/><br/><br/>
						
						</cfoutput>
						
						<cfoutput>
						
							ERREUR (CATCH DUMP - TOTAL) :<br/><br/>
							
							<cfdump var="#CFCATCH#"><br/><br/>
						
						</cfoutput>

					</cfmail>
				
				</cfcatch>
				</cftry>


			<cfelse>
			
				
			
			</cfif>

	</cffunction>

	<!--- ENVOI DU MAIL --->
	<cffunction access="public" name="sendMailDBC" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfargument name="logsObject" 	type="Struct"  required="true">
		<cfargument name="attachObject" type="Query"   required="true">
		<cfargument name="codeapp" 		type="Numeric" required="true">
			
			<cfset logObject    = createObject('component',"fr.consotel.consoview.M16.LogsCommande")>

			<cfset uuidlog		= #ARGUMENTS.eventObject.getEventTarget()#>
			<cfset jobid		= #ARGUMENTS.eventObject.getJobId()#>
			<cfset ImportTmp	= "/container/M16/">

			<cfset getProperties(codeapp, 'NULL')>
			

			<cftry>
			
				<cfsavecontent variable="body">
				   <cfoutput>#logsObject.MESSAGE#</cfoutput>
				</cfsavecontent>
				
				<cftry>
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(16)>
					<cfset mLog.setIDMNT_ID(101)>
					<cfset mLog.setSUBJECTID("ID_CMD : " & logsObject.IDCOMMANDE & " | #logsObject.SUBJECT#")>
					<cfset mLog.setBODY(body)>
					<cfset mLog.setIDRACINE(logsObject.IDRACINE)>
					
					
					<!--- Preparation du tableau de fichier --->
					
					<cfset aOfMFile = []>					
					
					<cfloop query="attachObject">
							<cfset mFile = createobject('component','fr.consotel.api.monitoring.MFile')>
							<cfset _fileName = attachObject.FILE_NAME>
							<cfset _path = ImportTmp & attachObject.PATH>							
							<cfset mFile.setFileName(_fileName)>
							<cfset mFile.setPath(_path)>
							
							<cfset arrayAppend(aOfMFile,mfile)>
							
					</cfloop>
					
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MContentLogger"
						method="logItWithContent"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						<cfinvokeargument name="arrayOfMFile" value="#aOfMFile#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="[EVT-9999] Errereur dans l'enregistrement d'un log de monitoring">
								<cfdump var="#cfcatch#">						
					</cfmail>
				</cfcatch>
				</cftry>	
				

				
				<cfmail from="#properties.MAIL_FROM_COMMANDE#" to="#logsObject.MAIL_TO#" cc="#logsObject.CC#" bcc="#logsObject.BCC#,monitoring@saaswedo.com" replyto="#properties.MAIL_FROM_COMMANDE#"
				 		subject="#logsObject.SUBJECT#" type="text/plain"  server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					 				#logsObject.MESSAGE#
									

					<cfsilent>
					
						<cfloop query="attachObject">
							
							<cfset currentAttachementFileName = #attachObject.FILE_NAME#>
							
							<cfif attachObject.JOIN_MAIL EQ 1>
	
								<cfset myCurrentPath = #ImportTmp# & #attachObject.PATH# & '/' & #currentAttachementFileName#>
							
								<cfif FileExists(#ImportTmp# & #attachObject.PATH# & '/' & #currentAttachementFileName#)>
									
									<cfmailparam file="#ImportTmp & attachObject.PATH & '/' & currentAttachementFileName#">
								
								</cfif>
								
							</cfif>
							
						</cfloop>
					
					</cfsilent>
				</cfmail>
				
				
								
				<!--- MAJ LE LOG DE COMMANDE AVEC LE JOBID --->
				<cfset rsltUpdateJobId = logObject.updateJobId(uuidlog, jobid)>
				
				<cfif rsltUpdateJobId GT 0>
				
					<!--- ON NE FAIT RIEN TOUT c'EST BIEN PASSÉ --->
					<!---
					<cfmail from="#fromMailError#" to="monitoring@saaswedo.com" subject="Mise à jour jobid OK" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
						
						<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>
						<cfoutput>Mise à jour jobid OK après envoi de mail IDCOMMANDE = #logsObject.IDCOMMANDE#.<br /><br/></cfoutput>
						
						<cfoutput>
							MISE A JOUR <br />
							PARAMETRES :<br />
							UUID : #uuidlog#<br />
							JOBID : #jobid#<br /><br />
							RESULT : #rsltUpdateJobId#<br /><br />
						</cfoutput>

						<cfoutput>
								POST PROCESS RESULT :<br /><br />
								IDCOMMANDE 			: #logsObject.IDCOMMANDE#<br />
								JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
								REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
								REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
								BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
								EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
								EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
								EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
						</cfoutput>
	
					</cfmail>
					--->
				
				<cfelse>
				
					<cfmail from="#fromMailError#" to="monitoring@saaswedo.com" subject="Mise à jour jobid en echec" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
						
						<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>
						<cfoutput>Mise à jour jobid en echec après envoi de mail IDCOMMANDE = #logsObject.IDCOMMANDE#.<br /><br/></cfoutput>
						
						<cfoutput>
							MISE A JOUR EN ERREUR <br />
							PARAMETRES :<br />
							UUID : #uuidlog#<br />
							JOBID : #jobid#<br /><br />
							RESULT : #rsltUpdateJobId#<br /><br />
						</cfoutput>

						<cfoutput>
								POST PROCESS RESULT :<br /><br />
								IDCOMMANDE 			: #logsObject.IDCOMMANDE#<br />
								JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
								REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
								REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
								BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
								EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
								EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
								EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
						</cfoutput>
	
					</cfmail>
				
				</cfif>
				
			<cfcatch type="any">
				<cfmail from="#fromMailError#" to="monitoring@saaswedo.com" subject="Envoi de mails en erreur" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>
					<cfoutput>L'envoi de mail avec pièces jointes est en erreur IDCOMMANDE = #logsObject.IDCOMMANDE#.<br /><br/></cfoutput>
					
					<cfoutput>
							IDCOMMANDE 			: #logsObject.IDCOMMANDE#<br />
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>
					
					
					<cfoutput><p>Message: #CFCATCH.Message#</p><br /></cfoutput>
					<cfoutput><p>Detail : #CFCATCH.Detail#</p><br /></cfoutput>
					<cfoutput><p>Pile<br><cfdump var="#CFCATCH#"></p></cfoutput>

				</cfmail>
			</cfcatch>
			</cftry>

	</cffunction>
	
	<!--- ENVOI LES MAILS D'ERREURS --->
	<cffunction access="public" name="sendMailDBCError" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfargument name="logsObject" 	type="Struct" required="true">
		<cfargument name="attachObject" type="Query"  required="true">
		<cfargument name="codeapp" 		type="Numeric" required="true">

			<cfset uuidlog	= #ARGUMENTS.eventObject.getEventTarget()#>
			<cfset jobid	= #ARGUMENTS.eventObject.getJobId()#>
			
			<cfset getProperties(codeapp, 'NULL')>
			
			<cftry>
				<cfmail from="#fromMailError#" to="#destMailError#" subject="Report on error" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br/><br/></cfoutput>
					
					<cfoutput>Le rapport JOBID = #ARGUMENTS.eventObject.getJobId()# est en erreur.<br /><br/></cfoutput>

					<cfoutput>
							IDCOMMANDE 			: #logsObject.IDCOMMANDE#<br />
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>

				</cfmail>
			<cfcatch type="any">
				<cfmail from="#fromMailError#" to="#destMailError#" subject="Envoi de mails en erreur (Report on error)" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br/><br/></cfoutput>
					
					<cfoutput>Le rapport JOBID = #ARGUMENTS.eventObject.getJobId()# est en erreur et le mail est en erreur.<br /><br/></cfoutput>
					
					<cfoutput>
							IDCOMMANDE 			: #logsObject.IDCOMMANDE#<br />
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>
					<cfoutput><p>Message: #CFCATCH.Message#</p><br /></cfoutput>
					<cfoutput><p>Detail : #CFCATCH.Detail#</p><br /></cfoutput>
					<cfoutput><p>Pile<br><cfdump var="#CFCATCH#"></p></cfoutput>
					
				</cfmail>
			</cfcatch>
			</cftry>

	</cffunction>
	
	<!--- ENVOI UN MAIL D'ERREUR SI LE REPERTOIRE N'EXISTE PAS --->
	<cffunction access="public" name="sendMailRespositoryError" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfargument name="logsObject" 	type="Struct" required="true">
		<cfargument name="attachObject" type="Query"  required="true">
		<cfargument name="codeapp" 		type="Numeric" required="true">
			
			<cfset uuidlog	= #ARGUMENTS.eventObject.getEventTarget()#>
			<cfset jobid	= #ARGUMENTS.eventObject.getJobId()#>
			
			<cfset getProperties(codeapp, 'NULL')>
			
			<cftry>
				<cfmail from="#fromMailError#" to="#destMailError#" subject="Error no respository" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>

					<cfoutput>Le répertoire n'existe pas pour la commande ID = #logsObject.IDCOMMANDE#.<br /><br/></cfoutput>

					<cfoutput>
							IDCOMMANDE 			: #logsObject.IDCOMMANDE#<br />
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>

				</cfmail>
			<cfcatch type="any">
				<cfmail from="#fromMailError#" to="#destMailError#" subject="Envoi de mails en erreur (Error no respository)" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br/></cfoutput>

					<cfoutput>Le répertoire n'existe pas pour la commande ID = #logsObject.IDCOMMANDE# et le mail est en erreur.<br/><br/></cfoutput>
					
					<cfoutput>
							IDCOMMANDE 			: #logsObject.IDCOMMANDE#<br />
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>
					<cfoutput><p>Message: #CFCATCH.Message#</p><br /></cfoutput>
					<cfoutput><p>Detail : #CFCATCH.Detail#</p><br /></cfoutput>
					<cfoutput><p>Pile<br><cfdump var="#CFCATCH#"></p></cfoutput>

				</cfmail>
			</cfcatch>
			</cftry>

	</cffunction>

	<!--- ENVOI LES MAILS D'ERREURS --->
	<cffunction access="public" name="sendMailMODError" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		
			<cfset getProperties(codeapp, 'NULL')>
			
			<cftry>
				
				<cfmail from="#fromMailError#" to="#destMailError#" subject="[MOD]Report on error" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>

					<cfoutput>Mail de modification de commande, le rapport est en erreur.<br /><br/></cfoutput>

					<cfoutput>
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>

				</cfmail>
				
			<cfcatch type="any">
				
				<cfmail from="#fromMailError#" to="#destMailError#" subject="[MOD]Envoi de mails en erreur (Report on error)" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br/></cfoutput>

					<cfoutput>Mail de modification de commande, le rapport et le mail est en erreur.<br/><br/></cfoutput>
					
					<cfoutput>
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>
					<cfoutput><p>Message: #CFCATCH.Message#</p><br /></cfoutput>
					<cfoutput><p>Detail : #CFCATCH.Detail#</p><br /></cfoutput>
					<cfoutput><p>Pile<br><cfdump var="#CFCATCH#"></p></cfoutput>

				</cfmail>
				
			</cfcatch>
			</cftry>
		
	</cffunction>
	
		<!--- ENVOI LES MAILS D'ERREURS --->
	<cffunction access="public" name="sendMailMODRespositoryError" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">

			<cfset getProperties(codeapp, 'NULL')>
			
			<cftry>
				
				<cfmail from="#fromMailError#" to="#destMailError#" subject="[MOD]Error no respository" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>

					<cfoutput>Mail de modification de commande, le répertoire n'existe pas.<br /><br/></cfoutput>

					<cfoutput>
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>

				</cfmail>
				
			<cfcatch type="any">
				
				<cfmail from="#fromMailError#" to="#destMailError#" subject="[MOD]Envoi de mails en erreur (Error no respository)" type="html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br/></cfoutput>

					<cfoutput>Mail de modification de commande, le répertoire n'existe pas et le mail est en erreur.<br/><br/></cfoutput>
					
					<cfoutput>
							JOB_ID 				: #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS 		: #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL 			: #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER 			: #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET 		: #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE 			: #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER 	: #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					</cfoutput>
					
					<cfoutput><p>Quand : #LsdateFormat(now(),'dd/mm/yyyy:hh:mm:ss')#</p><br /></cfoutput>
					<cfoutput><p>Message: #CFCATCH.Message#</p><br /></cfoutput>
					<cfoutput><p>Detail : #CFCATCH.Detail#</p><br /></cfoutput>
					<cfoutput><p>Pile<br><cfdump var="#CFCATCH#"></p></cfoutput>

				</cfmail>
				
			</cfcatch>
			</cftry>

	</cffunction>

</cfcomponent>