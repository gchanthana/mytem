package commandemobile.ihm
{
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.MailVO;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.ConsoviewAlert;
	import composants.util.URLUtilities;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.registerClassAlias;
	import flash.xml.XMLDocument;
	
	import gestioncommande.composants.PanierIHM;
	import gestioncommande.composants.PiecesJointesMinIHM;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.Panier;
	import gestioncommande.entity.Pool;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.EventData;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.popup.PopUpConfirmationCommandeIHM;
	import gestioncommande.services.CommandeArticles;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.MailService;
	import gestioncommande.services.RevendeurAutoriseService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.TextArea;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class MainNewCommandeImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var vs						:ViewStack;
		
		public var hbxButtonWizzard			:HBox;

		public var txtAreaCommentaires		:TextArea;
		
		public var btPre					:Button;
		public var btNext					:Button;
		public var btValid					:Button;
		public var btCancel					:Button;
		public var btCancelConfig			:Button;
		
		//--------------------------------------------------------------------------------------------//
		//					ELEMENTS VS
		//--------------------------------------------------------------------------------------------//
		
		public var etapeParametres			:ParametresIHM;
		
		public var etapeValidation			:ValidationIHM;	
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _pUpMailBox				:ActionMailBoxIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					COMPONENTS
		//--------------------------------------------------------------------------------------------//
		
		public var composantPanier			:PanierIHM;
		
		public var piecesJointes			:PiecesJointesMinIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var objectPool				:Pool;

		private var _cmd					:Commande = new Commande(true);

		private var _selectedAction			:Action = new Action();
		
		private var _infosMail				:InfosObject = new InfosObject();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmdSrv					:CommandeService = new CommandeService();
		
		private var _revAutorises				:RevendeurAutoriseService = new RevendeurAutoriseService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		//---> INDEX VIEWSTACK
		public var selectedIndex			:int = 0;
		
		public var actePour					:Boolean = false;
		public var isNewCommande			:Boolean = false;
		
		public var multiConfig:ArrayCollection=new ArrayCollection();// liste permettant de stocker toutes les commandes d'une config
		
		private var _isRecorded				:Boolean = false;
		
		public var img:Image;
		[Embed(source="/assets/images/Help3.png",mimeType='image/png')]
		public var imgTrue :Class;
		
		private var _articles				:Array;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function MainNewCommandeImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			
			_cmd.addEventListener(CommandeEvent.INFOS_COMMANDE, infosCommandeHandler);
					
			addEventListener("thisMailSent", close);
			
			//---> PAS D'ACCESSOIRES ALORS ON SAUTE CETTE ETAPE
			addEventListener('NO_ACCESSORIES_NEXT', noAccessoriesHandler);
			addEventListener('NO_ACCESSORIES_PREV', noAccessoriesHandler);
			
			addEventListener('ONE_ENGAGEMENT_EQUIPEMENT', onlyHandler);
			addEventListener('ONE_ENGAGEMENT_RESSOURCES', onlyHandler);
			
			//---> LORSQUE L'ON CHANGE  UN DE CES 5 ELEMENTS ALORS ON BLOQUE LE WIZZARD
			addEventListener('MOBILE_CHANGED', dataCommandeChanged);
			addEventListener('PROFILE_CHANGED', dataCommandeChanged);
			addEventListener('REVENDEUR_CHANGED', dataCommandeChanged);
			addEventListener('OPERATEUR_CHANGED', dataCommandeChanged);
			addEventListener('TYPE_CMD_CHANGED', dataCommandeChanged);
			
			addEventListener('VALIDER_ENABLED', enableBtnValiderHandler);
			addEventListener('VALID_CONTINU', btValidAndPurchaseClickHandler);
			addEventListener('PRICE_CHANGE', calculPriceHandler);
			
			SessionUserObject.singletonSession.COMMANDEARTICLE = new ArrayCollection();
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			
			addEventListener('STEP_VALIDATION_SHOWED', endShowStepValidationHandler);
		}
		
		protected function endShowStepValidationHandler(event:Event):void
		{
			refreshPanier();
		}
		
		private function infosCommandeHandler(cmde:CommandeEvent):void
		{
			etapeParametres.setCommande();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PUBLIC
	//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		// ALLER SUR LA PAGE PRECEDENTE ACCESSIBLE SELON LE TYPE DE COMMANDE(ACCESS)
		protected function btPreClickHandler(me:MouseEvent):void   
		{
			decompteVs();
			//refreshPanier();
		}        
		
		
		// ALLER SUR LA PAGE SUIVANTE ACCESSIBLE SELON LE TYPE DE COMMANDE(ACCESS)
		protected function btNextClickHandler(me:MouseEvent):void
		{
			var isOk:Boolean = (vs.getChildAt(selectedIndex) as Object).validateCurrentPage();
			
			if(selectedIndex == 0)
				whatIsTypeCommande();
			
			if(isOk)
				compteVs();
		}
		
		protected function btValidClickHandler(me:MouseEvent):void
		{
			multiConfig.addItem(SessionUserObject.singletonSession.IDTYPEDECOMMANDE);// ajout du type de commande de la currentConfig
			
			if (multiConfig.contains(1) || multiConfig.contains(2))// test si la config contient une commande de nouvelle ligne 
			{
				_cmd.IDTYPE_COMMANDE=TypesCommandesMobile.NOUVELLES_LIGNES;
			}
			
			if(!_isRecorded)
			{
				if(selectedIndex == 6)
				{
					if(SessionUserObject.singletonSession.COMMANDEARTICLE.length > 0)
					{
						if(etapeValidation.mailCommande.myAction != null &&  etapeValidation.mailCommande.myAction.IDACTION > 0)
						{
							if(etapeValidation.mailCommande.checkMail())
							{
								if(etapeValidation.mailCommande.myAction.CODE_ACTION == "EMA3T")
								{ 
									var poUpConfir:PopUpConfirmationCommandeIHM = new PopUpConfirmationCommandeIHM();
										poUpConfir.addEventListener("sendCommande", sendCommandeHandler);
									
									PopUpManager.addPopUp(poUpConfir,this,true);
									PopUpManager.centerPopUp(poUpConfir);
									
								}
								else
									sendCommandeHandler();
							}
						}
						else
							sendCommandeHandler();
					}
					else
						ConsoviewAlert.afficherAlertInfo(resourceManager.getString('M16','Veuillez_cr_er_au_moins_une_commande_'), 'Consoview', null);
				}
			}
			else
				ConsoviewAlert.afficherAlertConfirmation(resourceManager.getString('M16','Commande_d_j__enregistr_e___Voulez_vous_aller_sur_la'), 'Consoview', commandeIsRecorded);
		}

		// A NE PAS TESTER le calcul de panier
		protected function btCancelConfigClickHandler(me:MouseEvent):void
		{
			multiConfig.removeItemAt(multiConfig.length-1);//suppresion type de commande de la derniere config
			
			btCancelConfig.visible = false;
			selectedIndex = 6;
			
			refreshPanier();
			
			var elements:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var len:int = elements.length;
			
			var elts		:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			var idrand		:int = elts.RANDOM;
			
			if(elements.contains(elts))//si l'objet currentconfig existe dans l'objet de la commande
			{
				for(var h:int = 0;h < len;h++)
				{	//si le random de la currentconfig = le random de l'un des objet commande
					if(SessionUserObject.singletonSession.COMMANDEARTICLE[h].RANDOM == idrand)
					{	//-> suppression de cet objet
						SessionUserObject.singletonSession.COMMANDEARTICLE.removeItemAt(h);
						break;
					}
				}
			}

			len = SessionUserObject.singletonSession.COMMANDEARTICLE.length;
			
			if(len > 0)
				SessionUserObject.singletonSession.CURRENTCONFIGURATION = elements[len-1];
			
			if(len == 1)
				etapeParametres.enabledDisableComponents(true);
			
			
			_cmd.CONFIG_NUMBER = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			
			
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TYPE;
			//donne au radioBouton la valeur de la config précédante
			switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)
			{
				case 1 : etapeParametres.rdbtnNewTerminal.selected = true; break;
				case 2 : etapeParametres.rdbtnNoTerminal.selected = true; break;
				case 3 : etapeParametres.rdbtnterminalOnly.selected = true; break;
				case 4 : etapeParametres.rdbtnChargerConfiguration.selected = true; break;
			}
			//initialise le TYPE de la commande globale avec l'IDTYPE_COMMANDE de la premiere config
			_cmd.IDTYPE_COMMANDE = giveMeTypeCommande(elements[0]["TYPE"]);
			
			whatIsTypeCommande();
			
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).setConfiguration();
			}
			
			enabledDisableButtonWizzard();
		}
		
		//ATTRIBUE LE BON IDTYPECOMMANDE
		private function giveMeTypeCommande(idTypeCmde:int):Number
		{
			var typedecommande:Number = 0;
			
			switch(idTypeCmde)
			{
				case 1: typedecommande = TypesCommandesMobile.NOUVELLES_LIGNES; break;//--- NVL COMMANDE
				case 2: typedecommande = TypesCommandesMobile.NOUVELLES_LIGNES; break;//--- NVL COMMANDE
				case 3: typedecommande = TypesCommandesMobile.EQUIPEMENTS_NUS;  break;//--- NVL COMMANDE
				case 4: typedecommande = TypesCommandesMobile.NOUVELLES_LIGNES; break;//--- NVL COMMANDE
			}
			return typedecommande;
		}
		
		// RETOUR A LA LISTE DE COMMANDE(ECRAN INITIAL)
		protected function btCancelClickHandler(me:MouseEvent):void
		{
			selectedIndex = 0;
			dispatchEvent(new ViewStackEvent(ViewStackEvent.ANNUL_COMMANDE, true));
		}
		
		// AU CLIC SELECTION DU VIEW STACK
		protected function btn1ClickHandler(me:MouseEvent):void
		{
			selectedIndex = 0;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		protected function btn2ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(1);
		}
		
		protected function btn3ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(3);
		}
		
		protected function btn4ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(4);
		}
		
		protected function btn5ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(6);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		private function noAccessoriesHandler(e:Event):void
		{
			if(e.type == 'NO_ACCESSORIES_NEXT')
			{
				compteVs();
			}
			else if(e.type == 'NO_ACCESSORIES_PREV')
				{
					decompteVs();
				}
		}
		
		private function onlyHandler(e:Event):void
		{
			btNextClickHandler(new MouseEvent(MouseEvent.CLICK));
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			SessionUserObject.singletonSession.COMMANDE = _cmd;
			
			SessionUserObject.singletonSession.COMMANDE.IDROLE = 101;

			enabledDisableButtonWizzard();
			buttonNavigationEnableDisable();
		}
		
		private function closeAllPopups(e:Event):void
		{
			if(Application.application.hasOwnProperty('consoviewIhm'))
				Application.application.consoviewIhm.hideProgressBar();
		}
		// A NE PAS TESTER le calcul de panier
		private function btValidAndPurchaseClickHandler(e:Event):void
		{
			
			multiConfig.addItem(SessionUserObject.singletonSession.IDTYPEDECOMMANDE);// ajout du type de commande de la currentConfig
			var isOk:Boolean = (vs.getChildAt(selectedIndex) as Object).validateCurrentPage();
			
			if(isOk)
			{
				var children	:Array = vs.getChildren();
				var lenChild	:int = children.length;
				
				for(var i:int = 0;i < lenChild;i++)
				{
					(children[i] as Object).reset();
				}
				
				etapeParametres.enabledDisableComponents(false);
				
				btCancelConfig.visible = true;
				selectedIndex = 0;
				
				buttonNavigationEnableDisable();
				enabledDisableButtonWizzard();
				refreshPanier();
			}
		}
		
		private function enableBtnValiderHandler(e:Event):void
		{
			btValid.enabled = true;
		}

		private function dataCommandeChanged(e:Event):void
		{			
			if(e.type == 'TYPE_CMD_CHANGED' || e.type == 'PROFILE_CHANGED')
			{
				var children	:Array = vs.getChildren(); // récuperation des onglets
				var lenChild	:int = children.length; 
				
				for(var i:int = 0;i < lenChild;i++)
				{
					(children[i] as Object).reset(); //appel des fonctions reset() de chaque objet onglet
				}
				
				//recupere les infos de commande et de config si elles existent
				var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
				var elts		:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
				var lenElts		:int = elements.length;
				var idrand		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM;
				
				
				//si l'objet de la commande contient l'objet de la currentconfig
				if(elements.contains(elts))
				{
					for(var j:int = 0;j < lenElts;j++)
					{
						if(SessionUserObject.singletonSession.COMMANDEARTICLE[j].RANDOM == idrand)
						{	//-> suppression de l'item objet dans la commande qui a le meme random que l'objet currentconfig
							SessionUserObject.singletonSession.COMMANDEARTICLE.removeItemAt(j);
							break;
						}
					}
				}
				//nouvelle config(new objet configuration)
				SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			}
			
			enabledDisableButtonWizzard();
		}
		
		private function calculPriceHandler(e:Event):void
		{
			refreshPanier();
		}
		
		private function close(e:Event):void
		{
			selectedIndex = 0;
			
			dispatchEvent(new ViewStackEvent(ViewStackEvent.COMMANDE_BUILT, true));
		}
		
		private function commandeIsRecorded(ce:CloseEvent):void
		{
			if(ce.detail == 4)
				close(ce);
		}

		private function _pUpMailBoxMailEnvoyeHandler(e:Event):void
		{	
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			
			if(!_pUpMailBox.cbMail.selected)
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Validation_enregistr__'), this);
			else
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_mail_a__t__envoy__'), this);
			
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
			
			_cmd.IDLAST_ETAT = _selectedAction.IDETAT;
			
			dispatchEvent(new Event("thisMailSent"));
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
				
		private function sendMailHandler(cmde:CommandeEvent):void
		{
			_isRecorded = true;
			close(cmde);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function sendCommandeHandler(e:Event = null):void
		{
			this._articles = addArticles();
			
			_cmd.COMMENTAIRES 	= txtAreaCommentaires.text;
			
			
			SessionUserObject.singletonSession.COMMANDE = _cmd;
			
			if(_articles.length > 0)
			{
				if(etapeValidation.mailCommande.myAction != null && etapeValidation.mailCommande.myAction.IDACTION > 0)
				{
					etapeValidation.mailCommande.myAction.COMMENTAIRE_ACTION = etapeValidation.txaCommentaires.text;
					etapeValidation.mailCommande.myAction.DATE_ACTION 		 = new Date();
					etapeValidation.mailCommande.myAction.DATE_HOURS		 = 	Formator.formatReverseDate(etapeValidation.mailCommande.myAction.DATE_ACTION) 
																				+ " " 
																				+ Formator.formatHourConcat(etapeValidation.mailCommande.myAction.DATE_ACTION);
					
					etapeValidation.mailCommande.createMailInfos();
				}
				
				verifierAvantEnvoyer();
			}
		}
		
		private function verifierAvantEnvoyer():void
		{
			_cmdSrv.addEventListener(CommandeEvent.CMD_INEXISTANT_V, traitCmdInexistant);
			
			_cmdSrv.verifierCommande(_cmd.NUMERO_COMMANDE);
		}
		
		protected function traitCmdInexistant(event:Event):void
		{
			_cmdSrv.recordCommandeV2(	_cmd, 
										this._articles, 
										piecesJointes.piecesjointes.source, 
										etapeValidation.mailCommande.mailCommande, 
										Formator.formatBoolean(etapeValidation.mailCommande.chxMailCommande.selected),
										etapeValidation.mailCommande.myAction,
										piecesJointes.UUID
									);
			
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_CREATED, sendMailHandler);
			_cmdSrv.addEventListener('COMMANDE_ERROR_VERIF', traitErreurCommande);
		}
		
		protected function traitErreurCommande(event:Event):void
		{
			this.verifierAvantEnvoyer();
			
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//

		private function refreshPanier():void
		{
			if(vs.selectedIndex == 6)
			{
				composantPanier.attributMontantConfirme();
				composantPanier.attributMontantEnPreparation(true);
				composantPanier.attributMontantTotal(true);
			}
			else
			{
				composantPanier.attributMontantEnPreparation();	
				composantPanier.attributMontantTotal();
			}
		}
		
		// AU CLIC SUR UN BOUTON "ETAPE", VALIDATION DE LA PAGE PRECEDANTE
		private function validatePreviousPage(pageMax:int):void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			var childError	:int = -1;
			
			for(var i:int = 0; i < pageMax; i++)
			{
				if((children[i] as Object).ACCESS)
				{
					if(!(children[i] as Object).validateCurrentPage())
					{
						childError = i;
						break;
					}
				}
			}
			
			if(childError > 0)
				selectedIndex = childError;
			else
				selectedIndex = pageMax;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		private function addArticles():Array
		{
			var cart		:CommandeArticles = new CommandeArticles();
			var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var xmlArray	:ArrayCollection = new ArrayCollection();
			var len			:int = elements.length;

			for(var i:int = 0;i < len;i++)
			{
				var xmlEq		:XML = cart.addEquipements(elements[i] as ElementsCommande2);
				var xmlRs		:XML = cart.addRessources(elements[i] as ElementsCommande2);
				var config		:ArrayCollection = elements[i].CONFIGURATIONS;
				var engagement	:Engagement = elements[i].ENGAGEMENT;
				var idConfig	:int = elements[i].ID;
				var lenConfig	:int = config.length;
				
				for(var j:int = 0;j < lenConfig;j++)
				{
					var articles	:XML = <articles></articles>;
					var article		:XML = <article></article>;
					
					var equXML:XML = new XML();
					
					if((config[j] as ConfigurationElements).IDSIM > 0)
						equXML = attributNumeroSim(xmlEq.copy(), config[j] as ConfigurationElements);
					else
						equXML = xmlEq.copy();
					
					article.appendChild(equXML);
					article.appendChild(xmlRs);
					
					article = cart.addConfiguration(article, config[j] as ConfigurationElements, engagement, idConfig);

					articles.appendChild(article.copy());
					
					xmlArray.addItem(articles);
				}
			}

			return xmlArray.source;
		}
		
		private function attributNumeroSim(article:XML, conElts:ConfigurationElements):XML
		{
			var eqListe	:XMLList = article.children();
			var eqLen	:int = eqListe.length();
			var parent	:int = 0;
			
			for(var i:int = 0;i < eqLen;i++)
			{
				var equipement:XML = eqListe[i] as XML;

				if(int(equipement[0].idtype_equipement) == 71)
				{
					equipement[0].idequipementfournis = conElts.IDEQUFOURNI;
					equipement[0].idequipementclient  = conElts.IDEQUCLIENT;
					equipement[0].idequipement 		  = conElts.IDSIM;
					equipement[0].numeroserie  		  = conElts.IMEISIM;
				}
			}
			
			return article;
		}

		private function afficherMailBox():void
		{	
			_pUpMailBox 				= new ActionMailBoxIHM();
			
			_pUpMailBox.idsCommande 	= addIDCOMMANDEInArray();
			_pUpMailBox.numCommande 	= _cmd.NUMERO_COMMANDE;
			_pUpMailBox.idtypecmd 		= _cmd.IDTYPE_COMMANDE;
			
			_infosMail.commande 		= _cmd;
			_infosMail.corpMessage 		= _selectedAction.MESSAGE;
			_infosMail.UUID				= piecesJointes.UUID;
			
			var moduleText	:String = giveMeTheSegementTheme();
			var sujet		:String = giveMeSubject(_cmd);
			var libelle		:String	= _cmd.LIBELLE_COMMANDE;
			var numero		:String	= _cmd.NUMERO_COMMANDE;
			var objet		:String = sujet + " / " + libelle + " / " + numero;

			_pUpMailBox.initMail(moduleText, objet, "");
			_pUpMailBox.configRteMessage(_infosMail, GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE, _pUpMailBoxMailEnvoyeHandler);
			_pUpMailBox.selectedAction = _selectedAction;
			
			PopUpManager.addPopUp(_pUpMailBox, this, true);
			PopUpManager.centerPopUp(_pUpMailBox);
		}
		
		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function giveMeSubject(objectCommande:Commande):String
		{
			var TC:String = "";
			
			switch(objectCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	TC = ResourceManager.getInstance().getString('M16', 'Commande_Mobile');				break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_Fixe_R_seau');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_'); 		break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.SAV: 					TC = ResourceManager.getInstance().getString('M16', 'SAV_'); 						break;//--- SAV
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM: 	TC = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_');			break;//--- REACT
			}
			
			return TC;
		}
		
		private function giveMeTheSegementTheme():String
		{
			var moduleText	:String = "";
			var idseg		:int = 1;
			
			switch(idseg)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Mobile'); break;
				case 2: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Fixe'); 	break;
				case 3: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Data'); 	break;
			}
			
			return moduleText;
		}
		
		private function addIDCOMMANDEInArray():Array
		{
			var value:ArrayCollection = new ArrayCollection();
				value.addItem(_cmd.IDCOMMANDE);
			
			return value.source;
		}
		
		private function addNUMEROCOMMANDEInArray():Array
		{
			var value:ArrayCollection = new ArrayCollection();
				value.addItem(_cmd.NUMERO_COMMANDE);
			
			return value.source;
		}

		//--------------------------------------------------------------------------------------------//
		//					GESTION DU WIZZARD
		//--------------------------------------------------------------------------------------------//
		
		// AFFICHE LA PAGE SUIVANTE DISPONIBLE(SELON COMMANDE PRÉSELECTIONNÉ[ACCESS]) 
		private function compteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = selectedIndex+1; i < lenChild; i++)
			{
				if((children[i] as Object).ACCESS)
				{
					selectedIndex = i;
					break;
				}
			}
			enabledDisableButtonWizzard();
		}
		
		// AFFICHE LA PAGE PRECEDENTE DISPONIBLE(SELON COMMANDE PRÉSELECTIONNÉ[ACCESS])
		private function decompteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = selectedIndex-1;i > -1;i--)
			{
				if((children[i] as Object).ACCESS)
				{
					selectedIndex = i;
					break;
				}
			}
			enabledDisableButtonWizzard();
		}
		
		// GRISE OU DEGRISE LES BOUTONS D'ACCES AUX ONGLETS
		private function enabledDisableButtonWizzard():void
		{
			var children	:Array = hbxButtonWizzard.getChildren(); //tout les enfants du wizzard (bannière[HBox] des onglets)
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(selectedIndex) as Object).IDPAGE;
			
			for(var i:int = lenChild;i > -1;i--)
			{
				if(children[i] as Button)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					if(idButton <= idPage)
					{
						var childrenVs	:Array = vs.getChildren();
						
						for(var j:int = selectedIndex;j > -1;j--)
						{
							if((childrenVs[j] as Object).IDPAGE == idButton)
								(children[i] as Button).enabled = (childrenVs[j] as Object).ACCESS;
						}
					}
					else
					{
						myButton.enabled = false;
					}
				}
			}
			
			buttonWizzardColor();
			buttonNavigationEnableDisable();
		}
		
		// COLORISE LE BOUTON ACTIF OU DECOLORISE LE BOUTON INACTIF 
		private function buttonWizzardColor():void
		{
			var children	:Array = hbxButtonWizzard.getChildren();
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(selectedIndex) as Object).IDPAGE;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				if(children[i] as Button)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					var childrenVs	:Array = vs.getChildren();
					var lenVs		:int = childrenVs.length;
						
					for(var j:int = 0;j < lenVs;j++)
					{
						if((childrenVs[j] as Object).IDPAGE == idButton)
						{
							if(idButton == idPage)
								myButton.styleName = "btnWizzardActif";
							else
							{
								if(!(childrenVs[j] as Object).ACCESS)
								{
									myButton.styleName = "btnWizzardDesactivate";
								}
								else
								{
									if(myButton.enabled)
										myButton.styleName = "btnWizzardInactif";
									else
										myButton.styleName = "btnWizzardDesactivate";
								}
							}
						}
					}
				}
			}
		}
		
		// REND VISIBLE OU NON LES BOUTONS DE NAVIGATION(PRECEDANT / SUIVANT)
		private function buttonNavigationEnableDisable():void
		{
			refreshPanier();
			
			if(selectedIndex == 0)
			{
				btPre.visible = btPre.includeInLayout = false;
				btNext.visible = btNext.includeInLayout = true;
				btValid.visible = btValid.includeInLayout = false;
			}
			else if(selectedIndex > 0)
			{
				if(selectedIndex == 6)
				{
					btPre.visible = btPre.includeInLayout = true;
					btNext.visible = btNext.includeInLayout = false;
					btValid.visible = btValid.includeInLayout = true;
					btCancelConfig.visible = btCancelConfig.includeInLayout = false;
				}
				else
				{
					btPre.visible = btPre.includeInLayout = true;
					btNext.visible = btNext.includeInLayout = true;
					btValid.visible = btValid.includeInLayout = false;
				}
			}
		}
		
		// TYPE DE COMMANDE SELECTIONNÉ
		private function whatIsTypeCommande():void
		{
			var accessed	:Array = new Array(false,false,false,false,false,false,false,false);
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1)
			{
				accessed = new Array(true,true,true,true,true,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				accessed = new Array(true,false,false,true,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 3)
			{
				accessed = new Array(true,true,true,false,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 4)
			{
				accessed = new Array(true,false,false,false,true,true,true);	
			}
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).ACCESS = accessed[i];
			}
		}
		
		public function get revAutorises():RevendeurAutoriseService { return _revAutorises; }
		
		public function set revAutorises(value:RevendeurAutoriseService):void
		{
			if (_revAutorises == value)
				return;
			_revAutorises = value;
		}
			
	}
}
