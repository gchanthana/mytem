<!--- =========================================================================
Name: Produit
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="Produit" hint="This class is configured with a ConcreteStrategy object, maintains a reference to a Strategy object and may define an interface that lets Strategy access its data.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" returntype="AbstractStrategy" output="false" displayname="void init(string, string)" >
		<cfargument name="perimetre" required="false" type="string" default="" displayname="string perimetre" hint="Initial value for the perimetreproperty." />
		<cfargument name="ModeCalcul" required="false" type="string" default="" displayname="string ModeCalcul" hint="Initial value for the ModeCalculproperty." />
		 
		<cfset objStrategy=createObject("component","#perimetre##ModeCalcul#Strategy")>
		<cfset Variables.Strategy=objStrategy>
		<cfreturn Variables.Strategy>
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	
	<cffunction name="getProduit" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfreturn Strategy.getProduit(IDproduit)>
	</cffunction>
	
	<cffunction name="getProduit_cat" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfreturn Strategy.getProduit_cat(IDproduit)>
	</cffunction>
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfreturn Strategy.getTypeProduit(IDproduit)>
	</cffunction>
	
	<cffunction name="getTypeProduit_cat" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfreturn Strategy.getTypeProduit_cat(IDproduit)>
	</cffunction>
	
	<cffunction name="getOperateur" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfreturn Strategy.getOperateur(IDproduit)>
	</cffunction>
	
	<cffunction name="getOperateur_cat" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfreturn Strategy.getOperateur_cat(IDproduit)>
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" displayname="string getCompte(numeric)" >
		<cfargument name="IDcompte" required="true" type="numeric" default="" displayname="numeric IDcompte" hint="Initial value for the IDcompteproperty." />
		<cfreturn Strategy.getCompte(IDcompte)>
	</cffunction>
	
	<cffunction name="queryData" access="public" returntype="query" output="false" displayname="query queryData(numeric, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfreturn Strategy.getData(ID,IDproduit,DateDebut,DateFin)>
	</cffunction>

	<cffunction name="queryData_cat" access="public" returntype="query" output="false" displayname="query queryData(numeric, date, date)" >
		
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		
		 
		
		<cfreturn Strategy.getData_cat(ID,IDproduit,DateDebut,DateFin)>
	</cffunction>
	
	<cffunction name="queryData_mobile" access="public" returntype="query" output="false" displayname="query queryData(numeric, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfreturn Strategy.getData_mobile(ID,IDproduit,DateDebut,DateFin)>
	</cffunction>

	<cffunction name="queryData_cat_mobile" access="public" returntype="query" output="false" displayname="query queryData(numeric, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfreturn Strategy.getData_cat_mobile(ID,IDproduit,DateDebut,DateFin)>
	</cffunction>

	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn strategy.getStrategy()>
	</cffunction>
</cfcomponent>