<cfinterface author="Cedric" displayname="fr.saaswedo.api.mdm.core.services.IMDM" hint="Interface qui décrit un serveur MDM :
<br>- Informations concernant le serveur : Implémentation IMdmServerInfo qui fourni toutes les propriétés d'identification du serveur
<br>- Services du serveur : Implémentations permettant d'accéder aux services du serveur (IDeviceInfo,IDeviceManagement,etc...)
<br>- API de remoting : Implémentation IRemoteClient utilisée par défaut par les services pour accéder au serveur.
Des implémentations IRemoteClient supplémentaires peuvent etre spécifiées par le serveur pour accéder à des emplacements spécifiques">
	<cffunction name="init" returntype="fr.saaswedo.api.mdm.core.services.IMDM" hint="Le constructeur doit avoir cette signature">
		<cfargument name="mdmServerInfo" type="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" required="true" hint="Infos du serveur MDM">
	</cffunction>

	<cffunction name="checkConnection" returntype="Struct" hint="Vérifie la connexion avec le serveur MDM et retourne une structure
	contenant :<br>- IS_TEST_OK: TRUE si la connexion en succès<br>- TEST_MSG: Message explicatif lié à la vérification de la connexion">
	</cffunction>

	<!--- =========== Informations concernant le serveur MDM =========== --->
		
	<cffunction name="getMdmServerInfo" returntype="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" hint="Retourne les infos du serveur MDM">
	</cffunction>
	
	<cffunction name="getEnrollmentURL" returntype="String" hint="Retourne l'URL du lien d'enrollment">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<cfargument name="requireAgent" type="Boolean" required="false" default="true" hint="TRUE pour un enrollment qui requiert un agent MDM">
	</cffunction>
	
	<cffunction name="getEnrollmentServer" returntype="String" hint="Retourne le DNS ou l'IP du serveur d'enrollment">
	</cffunction>
	
	<cffunction name="getEnrollmentServerPort" returntype="Numeric" hint="Retourne le numéro de port du serveur d'enrollment">
	</cffunction>
	
	<!--- =========== Services du serveur MDM =========== --->

	<cffunction name="getIDeviceInfo" returntype="fr.saaswedo.api.mdm.core.services.IDeviceInfo" hint="API d'accès aux infos des devices">
	</cffunction>
	
	<cffunction name="getIDeviceManagement" returntype="fr.saaswedo.api.mdm.core.services.IDeviceManagement" hint="API de gestion des devices">
	</cffunction>
	
	<!--- Obsolete
	<cffunction name="getRemoteClient" returntype="fr.saaswedo.api.remoting.IRemoteClient" hint="Utilisée par défaut par l'API pour l'accès au serveur">
	</cffunction>
	--->
</cfinterface>