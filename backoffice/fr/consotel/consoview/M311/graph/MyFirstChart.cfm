<HTML>
<HEAD>
<TITLE>FusionCharts Sample Single Page Chart</TITLE>
</HEAD>
<BODY bgcolor="#FFFFFF">
<!--- profondeur de la ligne 0 --->
<!--- couleur fond du popup on hover --->
<!--- couleur bord du popup on hover --->
<cfset v_data="
	<graph caption='Decline in Net Interest Margins of Asian Banks (1995-2001)' 
		bgcolor='FFFFFF'
		canvasbgcolor='000000'
		formatNumber='0' 
		subCaption='(in Percentage)' 
		xaxisname='Country' 
		numdivlines='6' 
		zeroPlaneColor='333333' 
		zeroPlaneAlpha='40' 
		hovercapbg='FFECAA' 
		hovercapborder='F47E00' 
		numberSuffix=' &euro;'
		animation='1'
		animspeed='10'
		basefont='Verdana'
		basefontsize='13'
		showvalues='0'
		showCanvas='0'
		spacingArea='20'
		>
		
		
		<set name='Taiwan' value='10000.50' color='FF6600' link='javascript:alert(1234)' showName='1' /> 
		<set name='Malaysia' value='20014.50' color='FF6600'/> 
		<set name='Hong Kong' value='30000.89' color='FF6622' /> 
		<set name='Philippines' value='50000.1' color='FF6600'/> 
		<set name='Singapore' value='80448.45' color='FF6600' /> 
		<set name='Thailand' value='50015.54' color='FF6600'/> 
		<set name='India' value='10001.15' color='FF6600'/> 
		<trendlines>
			<line value='49025.598' displayValue='Moyenne' color='000000' thickness='1'/>
		</trendlines>
	</graph>">
<CENTER>

<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" 
codebase="http://download.macromedia.com/
pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
WIDTH="400" HEIGHT="300" id="FC2Column" ALIGN="">
<PARAM NAME=movie VALUE="FC2Column.swf?dataXML=<cfoutput>#v_data#</cfoutput>">
<PARAM NAME=quality VALUE=high>
<PARAM NAME=bgcolor VALUE=#FFFFFF>
<!--- <EMBED src="FC2Column.swf?dataXML=<cfoutput>#v_data#</cfoutput>" quality=high bgcolor=#FFFFFF WIDTH="565" HEIGHT="420" NAME="FC2Column" 
ALIGN=""
TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED> --->
</OBJECT>
</CENTER>
</BODY>
</HTML>