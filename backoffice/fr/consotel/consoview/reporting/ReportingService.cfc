<cfcomponent name="ReportingService" alias="fr.consotel.consoview.reporting.ReportingService">
	<cffunction name="getReportList" access="remote" returntype="query">
		<cfset pathPrefix="/~consoview/">
		<cfquery name="qGetReportList" datasource="#SESSION.OFFREDSN#"> 
			select 'Financier Mensuel Consolide Tous Segments' as RAPPORT,
				   'FinancierMensuelConsolideClass' as RAPPORTCLASS,
				   '#pathPrefix#' || 'financier/Financier Mensuel Consolide/Financier Mensuel Consolide.xdo' as BIP_REPORT_PATH,
				   'financier.FinancierConsolide' as RAPPORTCFC, 'OBIEE/PRODUCTION/FINANCIER/' as REPORT_DIR,
				   1 as ordre
			from   DUAL
			UNION
			select 'Suivi Budget Global' as RAPPORT,
				   'ClicheExerciceBudgetReportViewClass' as RAPPORTCLASS,
				   '#pathPrefix#' || 'budgetaire/Suivi Budget Global/Suivi Budget Global.xdo' as BIP_REPORT_PATH,
				   'budgetaire.SuiviBudgetGlobal' as RAPPORTCFC, 'OBIEE/PRODUCTION/BUDGET/' as REPORT_DIR,
				   2 as ordre
			from   DUAL
			UNION
			select 'Suivi Budgetaire' as RAPPORT,
				   'ClicheExerciceBudgetReportViewClass' as RAPPORTCLASS,
				   '#pathPrefix#' || 'budgetaire/Suivi Budgetaire/Suivi Budgetaire.xdo' as BIP_REPORT_PATH,
				   'budgetaire.SuiviBudgetaire' as RAPPORTCFC, 'OBIEE/PRODUCTION/BUDGET/' as REPORT_DIR,
				   3 as ordre
			from   DUAL
		</cfquery>
		<cfreturn qGetReportList>
	</cffunction>
</cfcomponent>
