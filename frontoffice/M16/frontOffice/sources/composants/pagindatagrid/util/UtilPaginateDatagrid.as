package composants.pagindatagrid.util
{
	import mx.collections.ArrayCollection;
	
	public class UtilPaginateDatagrid
	{
		public function UtilPaginateDatagrid()
		{
		}
		/**
		 * Handler : clic sur une case à cocher
		 * 
		 */
		/*public function onItemChanged(item : Object,listeItemSelected : ArrayCollection):void{
			//Si l'item n'est pas déja dans la liste
			if(listeItemSelected.getItemIndex(item)==-1) {
				//on l'ajoute
				//listeItemSelected.addItem(item);
			}else{
				listeItemSelected.removeItemAt(listeItemSelected.getItemIndex(item)); //sinon on le supprime
			}
		}*/
		/**
		 * 
		 * Handler : clic sur tout cocher
		 */
		public function onCBAllChanged(dataProvider : ArrayCollection, listeItemSelected : ArrayCollection,boolSelected : Boolean):void{
			
			listeItemSelected.removeAll();
			for each (var item:Object in dataProvider){
				item.SELECTED = boolSelected;
				dataProvider.itemUpdated(item);
			}
		}
		
		public function formatdata(dataProvider : ArrayCollection,listeItemSelected : ArrayCollection):void
		{
			listeItemSelected.removeAll();
			for(var i : int = 0;i<dataProvider.length;i++)
			{
				if(dataProvider.getItemAt(i).SELECTED)
				{
					listeItemSelected.addItem(dataProvider.getItemAt(i));
					//dataProvider.itemUpdated(dataProvider.getItemAt(i));
				}
			}
			//dataProvider.refresh();
		}
	}
}