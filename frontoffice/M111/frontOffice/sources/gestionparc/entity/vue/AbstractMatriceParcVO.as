package gestionparc.entity.vue
{
	import mx.utils.StringUtil;

	[Bindable]
	public dynamic class AbstractMatriceParcVO
	{
		/** t : pour terminal , s : pour SIM , ex :extention */

		private var _ID:Number=0;
		private var _ROWN:Number; /** nb total des lignes */
		private var _NB_ROWN:Number; /** index de la ligne de resultat */

		private var _IDEMPLOYE:Number;
		private var _COLLABORATEUR:String;

		private var _E_MATRICULE:String;
		private var _IDTERMINAL:Number;

		private var _T_IDTYPE_EQUIP:Number;
		private var _T_IMEI:String;
		private var _T_MODELE:String
		private var _T_NO_SERIE:String;
		private var _T_REVENDEUR:String;
		private var _T_IDREVENDEUR :Number;
		private var _T_BYOD:Number;

		private var _IDSIM:Number;
		private var _S_IMEI:String; /** numero de la carte SIM */
		private var _S_IDTYPE_EQUIP:Number; /** pour equipement avec sim = islignemobile*/
		private var _S_REVENDEUR:String;
		private var _S_IDREVENDEUR :Number; 
		private var _S_BYOD:Number;
		
		private var _IDEXTENSION:Number;
		private var _EX_NUMERO:String;
		private var _EX_IDTYPE_EQUIP:Number;

		private var _IDTYPE_LIGNE:Number;
		private var _IDSOUS_TETE:Number;
		private var _SOUS_TETE:String; /** le numero de tel */

		private var _OPERATEUR:String;
		private var _OPERATEURID:Number;
		private var _DISTRIBUTEUR:String;
		
		private var _SELECTED:Boolean=false;

		private var _ISLIGNEMOBILE:Boolean=false;
		private var _ISLIGNEFIXE:Boolean=false;

		private var _BOOL_ISMANAGED:Boolean;
		private var _IS_LAST_OP_FAULT:Boolean;
		private var _ZDM_IMEI:String;
		private var _ZDM_SERIAL_NUMBER:String;

		private var _COMPTE_FACTURATION:String="";
		private var _SOUS_COMPTE:String="";
		
		private var _E_DATE_ENTREE:Date=null;
		private var _E_DATE_SORTIE:Date=null;
		private var _E_EMAIL:String="";
		
		private var _INOUT:Number; 
		private var _T_ID_STATUT :Number=0; /** etat terminal */
		private var _S_ID_STATUT :Number=0; /** etat de SIM   */
		private var _ST_IDETAT :Number=0;   /** etat de la ligne */
		
		private var _PIN1:String;
		private var _PIN2:String;
		
		private var _T_FABRIQUANT :String; 		
		private var _TETE_LIGNE:String;
		private var _TYPE_RACCORDEMENT:String;
		private var _TYPE_LIGNE :String;
		
		private var _PUK1:String;
		private var _PUK2:String;
		
		private var _E_FULLNAME_MANAGER:String = '';
		
		private var _E_C1:String; // Champ perso 1
		private var _E_C2:String; // Champ perso 2
		private var _E_C3:String; // Champ perso 3
		private var _E_C4:String; // Champ perso 4
		
		private var _OPERATEUR_TELECOM:String;
		private var _COMPTE_FACTURATION_TELECOM:String;
		
		public function get OPERATEUR_TELECOM():String { return _OPERATEUR_TELECOM; }
		
		public function set OPERATEUR_TELECOM(value:String):void
		{
			if (_OPERATEUR_TELECOM == value)
				return;
			_OPERATEUR_TELECOM = value;
		}
		
		
		public function AbstractMatriceParcVO()
		{
		}
		
		public function createObject(obj:Object):void
		{
			this._T_IDTYPE_EQUIP=obj.T_IDTYPE_EQUIP;
			this._T_IMEI=obj.T_IMEI;
			this._T_MODELE=obj.T_MODELE;
			this._T_BYOD=obj.T_BYOD;
			this._T_NO_SERIE = (obj.T_NO_SERIE != null)?obj.T_NO_SERIE:'';
			this._IDSIM=obj.IDSIM;
			this._S_IDTYPE_EQUIP=obj.S_IDTYPE_EQUIP;
			this._S_IMEI=obj.S_IMEI;
			this._S_BYOD=obj.S_BYOD;
			this._IDEXTENSION=obj.IDEXTENSION;
			this._EX_NUMERO=obj.EX_NUMERO;
			this._EX_IDTYPE_EQUIP=obj.EX_IDTYPE_EQUIP;
			this._IDEMPLOYE=obj.IDEMPLOYE;
			this._NB_ROWN=obj.NB_ROWN;
			this._ROWN=obj.ROWN; /** le numero de ligne dans le resultat */

			this._IDTERMINAL=obj.IDTERMINAL;
			this._IDTYPE_LIGNE=obj.IDTYPE_LIGNE;

			if (obj.ISLIGNEMOBILE == 1)
				_ISLIGNEMOBILE=true;
			if (obj.ISLIGNEFIXE == 1)
				_ISLIGNEFIXE=true;

			this._IDSOUS_TETE=obj.IDSOUS_TETE;
			this._ID=obj.ID;
			this._COLLABORATEUR=obj.COLLABORATEUR;
			this._E_MATRICULE=obj.E_MATRICULE;
			this._SOUS_TETE=obj.SOUS_TETE;
			this._OPERATEUR=obj.OPERATEUR;
			this._OPERATEURID=obj.OPERATEURID;
			this._S_REVENDEUR=obj.S_REVENDEUR;
			this._T_REVENDEUR=obj.T_REVENDEUR;
			this._T_IDREVENDEUR=obj.T_IDREVENDEUR;

			if (obj.hasOwnProperty("T_ZDM_IMEI"))
				this.ZDM_IMEI=obj.T_ZDM_IMEI;

			if (obj.hasOwnProperty("T_ZDM_SERIAL_NUMBER"))
				this.ZDM_SERIAL_NUMBER=obj.T_ZDM_SERIAL_NUMBER;

			if (obj.hasOwnProperty("T_ZDM_DEVICE_ID"))
				this.ZDM_DEVICEID=obj.T_ZDM_DEVICE_ID;

			if (obj.hasOwnProperty("BOOL_ISMANAGED"))
			{
				if (obj.BOOL_ISMANAGED == 1)
					this.BOOL_ISMANAGED=true;
				else
					this.BOOL_ISMANAGED=false;
			}

			_IS_LAST_OP_FAULT=false;
			
			if (obj.hasOwnProperty("IDCOMPTE_FACTURATION"))
				this.IDCOMPTE_FACTURATION=obj.IDCOMPTE_FACTURATION;
			
			if (obj.hasOwnProperty("IDSOUS_COMPTE"))
				this.IDSOUS_COMPTE=obj.IDSOUS_COMPTE;

			if (obj.hasOwnProperty("IDDISTRIBUTEUR"))
				this.IDDISTRIBUTEUR=obj.IDDISTRIBUTEUR;
			
			if (obj.hasOwnProperty("S_IDREVENDEUR"))
				this._S_IDREVENDEUR =obj.S_IDREVENDEUR;
			
			this._E_DATE_ENTREE=obj.E_DATE_ENTREE;
			this._E_DATE_SORTIE=obj.E_DATE_SORTIE ;
			this._E_EMAIL=obj.E_EMAIL;
			
			this._INOUT=obj.INOUT;
			this._S_ID_STATUT=obj.S_ID_STATUT;
			this._T_ID_STATUT=obj.T_ID_STATUT;
			this._ST_IDETAT=obj.ST_IDETAT;
			
			this._TYPE_RACCORDEMENT=obj.TYPE_RACCORDEMENT;
			this._TETE_LIGNE=obj.TETE_LIGNE;
			this._TYPE_LIGNE=obj.TYPE_LIGNE;
			
			this._PIN1=obj.PIN1;
			this._PIN2=obj.PIN2;
			this._T_FABRIQUANT=obj.T_FABRIQUANT;
			
			this.E_FULLNAME_MANAGER = ((obj.E_FULLNAME_MANAGER != null)?obj.E_FULLNAME_MANAGER : '' );
			
			this.E_C1 = (obj.E_C1 != null) ? obj.E_C1 : '';
			this.E_C2 = (obj.E_C2 != null) ? obj.E_C2 : '';
			this.E_C3 = (obj.E_C3 != null) ? obj.E_C3 : '';
			this.E_C4 = (obj.E_C4 != null) ? obj.E_C4 : '';
			
			if (obj.hasOwnProperty("OPERATEUR_TELECOM"))
				this.OPERATEUR_TELECOM = (obj.OPERATEUR_TELECOM != null)?obj.OPERATEUR_TELECOM : '';
			if (obj.hasOwnProperty("COMPTE_FACTURATION_TELECOM"))
				this.COMPTE_FACTURATION_TELECOM = (obj.COMPTE_FACTURATION_TELECOM != null)? obj.COMPTE_FACTURATION_TELECOM : '' ;
		}

		public function get S_BYOD():Number
		{
			return _S_BYOD;
		}
		
		public function set S_BYOD(value:Number):void
		{
			this._S_BYOD = value;
		}
		
		public function get T_BYOD():Number
		{
			return _T_BYOD;
		}
		
		public function set T_BYOD(value:Number):void
		{
			this._T_BYOD = value;
		}
		
		public function get IDEXTENSION():Number
		{
			return _IDEXTENSION;
		}

		public function set IDEXTENSION(value:Number):void
		{
			_IDEXTENSION=value;
		}

		public function get EX_NUMERO():String
		{
			return _EX_NUMERO;
		}

		public function set EX_NUMERO(value:String):void
		{
			_EX_NUMERO=value;
		}

		public function get EX_IDTYPE_EQUIP():Number
		{
			return _EX_IDTYPE_EQUIP;
		}

		public function set EX_IDTYPE_EQUIP(value:Number):void
		{
			_EX_IDTYPE_EQUIP=value;
		}

		public function get IDTYPE_LIGNE():Number
		{
			return _IDTYPE_LIGNE;
		}

		public function set IDTYPE_LIGNE(value:Number):void
		{
			_IDTYPE_LIGNE=value;
		}

		public function get IDSOUS_TETE():Number
		{
			return _IDSOUS_TETE;
		}

		public function set IDSOUS_TETE(value:Number):void
		{
			_IDSOUS_TETE=value;
		}

		public function get SOUS_TETE():String
		{
			return _SOUS_TETE;
		}

		public function set SOUS_TETE(value:String):void
		{
			_SOUS_TETE=value;
		}

		public function get IDTERMINAL():Number
		{
			return _IDTERMINAL;
		}

		public function set IDTERMINAL(value:Number):void
		{
			_IDTERMINAL=value;
		}

		public function get T_IDTYPE_EQUIP():Number
		{
			return _T_IDTYPE_EQUIP;
		}

		public function set T_IDTYPE_EQUIP(value:Number):void
		{
			_T_IDTYPE_EQUIP=value;
		}

		public function get T_IMEI():String
		{
			var tmpImei:String=((_T_IMEI != null) && (StringUtil.trim(_T_IMEI).length > 0)) ? _T_IMEI : "";
			var imeiResult:String=tmpImei;

			if (tmpImei.length > 0)
			{ // IMEI renseigné
				if (T_MODELE != null)
				{
					var tmpTerminal:String=T_MODELE.toUpperCase();
					var isApple:int=tmpTerminal.indexOf("APPLE");
					var isIPhone:int=tmpTerminal.indexOf("IPHONE");
					// Pour les terminaux APPLE : Un formatage de la valeur de l'IMEI doit etre effectué
					if ((isIPhone >= 0) && (isApple >= 0) && (tmpImei.length >= 15) && (tmpImei.charAt(2) != " ") && (tmpImei.charAt(9) != " "))
					{
						imeiResult=tmpImei.substr(0, 2) + " " + tmpImei.substr(2, 6) + " " + tmpImei.substr(8, 6) + " " + tmpImei.substr(14, tmpImei.length - 14);
					}
				}
			}
			return imeiResult;
		}

		public function set T_IMEI(value:String):void
		{
			_T_IMEI=value;
		}

		public function get T_MODELE():String
		{
			return _T_MODELE;
		}

		public function set T_MODELE(value:String):void
		{
			_T_MODELE=value;
		}

		public function get T_NO_SERIE():String
		{
			return _T_NO_SERIE == null ? "" : _T_NO_SERIE;
		}

		public function set T_NO_SERIE(value:String):void
		{
			_T_NO_SERIE=value;
		}

		public function get ID():Number
		{
			return _ID;
		}

		public function set ID(value:Number):void
		{
			_ID=value;
		}

		public function get NB_ROWN():Number
		{
			return _NB_ROWN;
		}

		public function set NB_ROWN(value:Number):void
		{
			_NB_ROWN=value;
		}


		public function get IDEMPLOYE():Number
		{
			return _IDEMPLOYE;
		}

		public function set IDEMPLOYE(value:Number):void
		{
			_IDEMPLOYE=value;
		}

		public function get COLLABORATEUR():String
		{
			return _COLLABORATEUR;
		}

		public function set COLLABORATEUR(value:String):void
		{
			_COLLABORATEUR=value;
		}

		public function get E_MATRICULE():String
		{
			return _E_MATRICULE;
		}

		public function set E_MATRICULE(value:String):void
		{
			_E_MATRICULE=value;
		}

		public function get IDSIM():Number
		{
			return _IDSIM;
		}

		public function set IDSIM(value:Number):void
		{
			_IDSIM=value;
		}

		public function get S_IMEI():String
		{
			return _S_IMEI;
		}

		public function set S_IMEI(value:String):void
		{
			_S_IMEI=value;
		}

		public function get OPERATEUR():String
		{
			return _OPERATEUR;
		}

		public function set OPERATEUR(value:String):void
		{
			_OPERATEUR=value;
		}

		public function get OPERATEURID():Number
		{
			return _OPERATEURID;
		}

		public function set OPERATEURID(value:Number):void
		{
			_OPERATEURID=value;
		}

		public function get T_REVENDEUR():String
		{
			return _T_REVENDEUR;
		}

		public function set T_REVENDEUR(value:String):void
		{
			_T_REVENDEUR=value;
		}

		public function get S_REVENDEUR():String
		{
			return _S_REVENDEUR;
		}

		public function set S_REVENDEUR(value:String):void
		{
			_S_REVENDEUR=value;
		}

		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}

		public function set SELECTED(value:Boolean):void
		{
			_SELECTED=value;
		}

		public function set ISLIGNEMOBILE(value:Boolean):void
		{
			_ISLIGNEMOBILE=value;
		}

		public function get ISLIGNEMOBILE():Boolean
		{
			return _ISLIGNEMOBILE;
		}

		public function set ISLIGNEFIXE(value:Boolean):void
		{
			_ISLIGNEFIXE=value;
		}

		public function get ISLIGNEFIXE():Boolean
		{
			return _ISLIGNEFIXE;
		}

		public function get S_IDTYPE_EQUIP():Number
		{
			return _S_IDTYPE_EQUIP;
		}

		public function set S_IDTYPE_EQUIP(value:Number):void
		{
			_S_IDTYPE_EQUIP=value;
		}

		public function set ZDM_SERIAL_NUMBER(value:String):void
		{
			_ZDM_SERIAL_NUMBER=value;
		}

		public function get ZDM_SERIAL_NUMBER():String
		{
			return T_NO_SERIE;
		}

		public function set ZDM_IMEI(value:String):void
		{
			_ZDM_IMEI=value;
		}

		public function get ZDM_IMEI():String
		{
			return T_IMEI;
		}

		public function get IS_LAST_OP_FAULT():Boolean
		{
			return _IS_LAST_OP_FAULT;
		}

		public function set IS_LAST_OP_FAULT(value:Boolean):void
		{
			_IS_LAST_OP_FAULT=value;
		}

		public function get BOOL_ISMANAGED():Boolean
		{
			return _BOOL_ISMANAGED;
		}

		public function set BOOL_ISMANAGED(value:Boolean):void
		{
			_BOOL_ISMANAGED=value;
		}

		public function set COMPTE_FACTURATION(value:String):void
		{
			_COMPTE_FACTURATION=value;
		}
		
		public function get COMPTE_FACTURATION():String
		{
			return _COMPTE_FACTURATION;
		}
		
		public function set SOUS_COMPTE(value:String):void
		{
			_SOUS_COMPTE=value;
		}
		
		public function get SOUS_COMPTE():String
		{
			return _SOUS_COMPTE;
		}
		
		public function get DISTRIBUTEUR():String
		{
			return _DISTRIBUTEUR;
		}
		
		public function set DISTRIBUTEUR(value:String):void
		{
			_DISTRIBUTEUR = value;
		}
		
		public function get TYPE_RACCORDEMENT():String
		{
			return _TYPE_RACCORDEMENT;
		}
		
		public function set TYPE_RACCORDEMENT(value:String):void
		{
			_TYPE_RACCORDEMENT = value;
		}
		
		public function get INOUT():Number
		{
			return _INOUT;
		}
		
		public function set INOUT(value:Number):void
		{
			_INOUT = value;
		}
		public function get S_ID_STATUT():Number
		{
			return _S_ID_STATUT;
		}
		
		public function set S_ID_STATUT(value:Number):void
		{
			_S_ID_STATUT = value;
		}
		
		public function get T_ID_STATUT():Number
		{
			return _T_ID_STATUT;
		}
		
		public function set T_ID_STATUT(value:Number):void
		{
			_T_ID_STATUT = value;
		}
		
		public function get T_FABRIQUANT():String
		{
			return _T_FABRIQUANT;
		}
		
		public function set T_FABRIQUANT(value:String):void
		{
			_T_FABRIQUANT = value;
		}
		
		public function get ST_IDETAT():Number
		{
			return _ST_IDETAT;
		}
		
		public function set ST_IDETAT(value:Number):void
		{
			_ST_IDETAT = value;
		}
		
		public function get E_EMAIL():String
		{
			return _E_EMAIL;
		}
		
		public function set E_EMAIL(value:String):void
		{
			_E_EMAIL = value;
		}
		
		public function get E_DATE_SORTIE():Date
		{
			return _E_DATE_SORTIE;
		}
		
		public function set E_DATE_SORTIE(value:Date):void
		{
			_E_DATE_SORTIE = value;
		}
		
		public function get E_DATE_ENTREE():Date
		{
			return _E_DATE_ENTREE;
		}
		
		public function set E_DATE_ENTREE(value:Date):void
		{
			_E_DATE_ENTREE = value;
		}
		
		public function get TYPE_LIGNE():String
		{
			return _TYPE_LIGNE;
		}
		
		public function set TYPE_LIGNE(value:String):void
		{
			_TYPE_LIGNE = value;
		}
		
		public function get TETE_LIGNE():String
		{
			return _TETE_LIGNE;
		}
		
		public function set TETE_LIGNE(value:String):void
		{
			_TETE_LIGNE = value;
		}
		
		public function get S_IDREVENDEUR() :Number
		{
			return _S_IDREVENDEUR;
		}
		
		public function set S_IDREVENDEUR(value: Number):void
		{
			_S_IDREVENDEUR = value;
		}
		
		public function get T_IDREVENDEUR():Number
		{
			return _T_IDREVENDEUR;
		}
		
		public function set T_IDREVENDEUR(value:Number):void
		{
			_T_IDREVENDEUR = value;
		}
		
		public function get E_FULLNAME_MANAGER():String { return _E_FULLNAME_MANAGER; }
		
		public function set E_FULLNAME_MANAGER(value:String):void
		{
			if (_E_FULLNAME_MANAGER == value)
				return;
			_E_FULLNAME_MANAGER = value;
		}
		
		public function get E_C4():String { return _E_C4; }
		
		public function set E_C4(value:String):void
		{
			if (_E_C4 == value)
				return;
			_E_C4 = value;
		}
		
		public function get E_C3():String { return _E_C3; }
		
		public function set E_C3(value:String):void
		{
			if (_E_C3 == value)
				return;
			_E_C3 = value;
		}
		
		public function get E_C2():String { return _E_C2; }
		
		public function set E_C2(value:String):void
		{
			if (_E_C2 == value)
				return;
			_E_C2 = value;
		}
		
		public function get E_C1():String { return _E_C1; }
		
		public function set E_C1(value:String):void
		{
			if (_E_C1 == value)
				return;
			_E_C1 = value;
		}
		
		public function get COMPTE_FACTURATION_TELECOM():String { return _COMPTE_FACTURATION_TELECOM; }
		
		public function set COMPTE_FACTURATION_TELECOM(value:String):void
		{
			if (_COMPTE_FACTURATION_TELECOM == value)
				return;
			_COMPTE_FACTURATION_TELECOM = value;
		}
	}
}
