package univers.inventaire.equipements.lignes
{
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.rpc.events.ResultEvent;
	import flash.ui.Mouse;
	import flash.events.MouseEvent;
	import mx.containers.Panel;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.FaultEvent;
	import mx.collections.XMLListCollection;
	
	import flash.display.DisplayObject;
	import flash.xml.XMLNode;
	import mx.controls.Text;
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.messaging.errors.NoChannelAvailableError;
	import mx.events.TreeEvent;
	import mx.utils.ArrayUtil;
	import mx.controls.Alert;
	import univers.facturation.optimisation.economies.Eco;
	import flash.ui.ContextMenuItem;
	import flash.ui.ContextMenu;
	import mx.collections.IViewCursor;
	import flash.events.ContextMenuEvent;
	import mx.events.ListEvent;
	import mx.containers.TitleWindow;
	import mx.containers.Tile;
	import univers.inventaire.equipements.lignes.mainComponent.NodeProperties;
	import mx.managers.PopUpManager;
	import mx.core.IFlexDisplayObject;
	import mx.utils.ObjectUtil;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.core.ClassFactory;
	import univers.inventaire.equipements.lignes.mainComponent.griditemRender;
	import mx.controls.Tree;
	import univers.inventaire.equipements.lignes.mainComponent.griditemRender;
	
	public class main extends mainIHM
	{
		
		private var _contextList:ArrayCollection; 
		private var _contextGrid:ArrayCollection;  
		private var _propertiesData:ArrayCollection;
		private var _dataLines:ArrayCollection;
		private var _organisationData:ArrayCollection;
		private var _showLines:Boolean = false;
		
		public function main()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			/** Tableau qui permet de remplir le menu contextuel du grid */
			 _contextGrid= new ArrayCollection(
						  [
					  	   {label:"Affecter en manuel", callback:onApplyManuelMode},
					  	   {label:"Affecter en auto", callback:onApplyAutoMode}
						  ]);
						  
			/** Tableau qui permet de remplir le menu contextuel du tree */
			_contextList= new ArrayCollection(
						  [
						   {label:"Afficher les lignes", callback:onbtAfficherLignesClicked, type:1, separate:true}
						  ]);
		}
		
		
		protected function initIHM(event:Event):void
		{
			
			fillOrganisation();
								  
			orgaStructure.setContextMenu(_contextList);						

			initGridContextMenu();

			// cbShowDate.addEventListener(MouseEvent.CLICK, onCheckcbShowDate);
			 gdrLignes.addEventListener(ListEvent.ITEM_CLICK, onGridDbClicked);
			 orgaStructure.setParentRef(this);
			 orgaStructure.setXMLFilter(menuFilterData);
			 refreshButtons();
			 cdcDate.data = new Date();
			
		}
		
		/** ####################### EVENTS ###############################**/
		
		
		
		protected function onGridDbClicked(event:ListEvent):void
		{
			parentDocument.showFiche(gdrLignes.selectedItem);
		}
		
		
			
		public function onbtAfficherLignesClicked(event: Event):void
		{
			var id:Number = orgaStructure.getSelectedTreeItemValue();
			_showLines = true;
			displayLines(id);
		}
		
		/** En analytique : evenement pour afficher la date */
		protected function onCheckcbShowDate(event:MouseEvent):void
		{
			refresh();
		}
		
		/** En analytique : Si la date change = rafraichit l'arbre */
		protected function onDateChange(event:Event):void
		{
			refresh();
		}
		
		protected function onbtManageClicked(event:Event):void
		{
			parentDocument.displayRightSide();
		}
		
		protected function onApplyManuelMode(event:Event):void
		{
			updateLinesAssignType(0);
		}
		
		protected function onApplyAutoMode(event:Event):void
		{
			updateLinesAssignType(1);
		}
				
		
		/** Perimetre change */
		public function onPerimetreChange():void {
			fillOrganisation();
		}
		
		/** Methode appelée quand l'organisation change */
		public function organisationChanged():void
		{
			orgaStructure.clearSearch();
			clearGridLines();
			refreshButtons();
			refreshTreeInterface();
		}
		
		/** Methode appelée quand on change de noeud */
		public function treeSelectionChanged():void
		{
			//clearGridLines();
			refreshButtons();
			displayLinesSelectedNode();
		}
		
		public function displayLinesSelectedNode():void
		{
			var id:Number = orgaStructure.getSelectedTreeItemValue();
			_showLines = true;
			displayLines(id);	
		}
		
		/** Lorsque l'arbre a été modifié (noeud supprimé ou renommé)
		 * on Rafraichit l'arbre de droite si on est dans la même organisation 
		 * */
		private function treeModified():void
		{
			
		}
		
		/** ####################### END EVENT ###############################*/
		
		/** Envoie requête pour récupérer les organisation de la racine */
		private function fillOrganisation():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getOrganisation",
																				fillOrganisationResult);				
			RemoteObjectUtil.callService(op, idGroupeMaitre);		
		}
		
		private function fillOrganisationResult(result:ResultEvent):void
		{
			_organisationData = ArrayCollection(result.result);
			orgaStructure.setCmbData(_organisationData, "LIBELLE_GROUPE_CLIENT");
			refreshTreeInterface();
		}

		/** Get the lines which belong to a node and display them on the datagrid
		 *  The second parameter of getLines is not used.
		 *  */
		private function displayLines(IDnode:Number):void
		{
		 	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"getAllLines",
																				getLinesResult);
			RemoteObjectUtil.callService(op, IDnode, 0, getSelectedDate());		
		}
		
		private function getLinesResult(event: ResultEvent):void
		{
			_dataLines = ArrayCollection(event.result);
			gdrLignes.dataProvider = _dataLines;
			//gdrLignes.itemRenderer = new ClassFactory(griditemRender);
		}
				
		/**  Tree Context Menu   */
		private function initGridContextMenu():void
		{
			var cm:ContextMenu = new ContextMenu();
			cm.hideBuiltInItems();
			
			for (var i:int = 0; i < _contextGrid.length; i++)
			{
				var item:ContextMenuItem = new ContextMenuItem(_contextGrid.getItemAt(i).label);
				if (_contextGrid.getItemAt(i).separate == true)
					item.separatorBefore = true;
				item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, _contextGrid.getItemAt(i).callback);
				cm.customItems.push(item);
			}
			gdrLignes.contextMenu = cm;	
			
			cm.addEventListener(ContextMenuEvent.MENU_SELECT, displayContextOptions);
		}
		
		private function displayContextOptions(event:Event):void
		{
			for (var i:int = 0; i < _contextGrid.length; i++)
			{
				event.currentTarget.customItems[i].enabled=true;
				if (_contextGrid[i].forceEnabled == true)
					event.currentTarget.customItems[i].enabled= false;
			}			
		}

		
		private function refreshTreeInterface():void
		{
			if (orgaStructure.getSelectedTypeOrga() == "ANA")
				orgaStructure.getMover().yTo = 30;
			else
			{
				orgaStructure.getMover().yTo = 0;
				//cbShowDate.visible = false;
				//cdcDate.visible = false;
			}
			orgaStructure.getMover().play();
		}
		
		/** Met à jour le type d'affectation des lignes selectionnées
		 * assignType : 0 - Manuel
		 * 				1 - Auto
		 *  */
		private function updateLinesAssignType(assignType:Number):void
		{
			var lines:String = getSelectedLinesList();
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"updateAssignMode",
																				updateAssignModeResult, throwError);
			RemoteObjectUtil.callService(op, lines, assignType);
			
		}
		
		private function updateAssignModeResult(event:ResultEvent):void
		{
			onbtAfficherLignesClicked(new Event(""));	
		}
		
				
		/** ######################## PUBLIC METHODS ##################### **/
		
		/** Return the selected Organisation */
		public function getSelectedOrganisationItem():Object
		{
			return orgaStructure.getSelectedOrganisation();
		}
		
		/** Return the ID (IDGROUPE_CLIENT) of the selected Organisation */
		public function getSelectedOrganisation():Number
		{
			return  orgaStructure.getSelectedOrganisationValue();
		}
		
		/** Return the selected node from the tree */
		public function getSelectedNode():Number
		{
			return  orgaStructure.getSelectedTreeItemValue();
		}
		
		/** Retourne un XML qui correspond au noeud selectionné */
		public function getSelectedItemNode():Object
		{
			return orgaStructure.getSelectedItem();
		}
		
		/** Return node's data */
		public function getNodeProperties():ArrayCollection
		{
			return _propertiesData;
		}
		
		/** Return an list id corresponding to the selected lines */
		public function getSelectedLinesList():String
		{
			var items:String = "";
			var bFlag:Boolean = true;
			for each (var obj:Object in gdrLignes.selectedItems)
			{
				if (!bFlag)
					items += ",";
				items += obj.IDSOUS_TETE;
				bFlag = false;
			}
			return items;
		}
		
		public function getSelectedLinesLabel():String
		{
			if (gdrLignes.selectedIndex < 0)
				return null;
			return gdrLignes.selectedItem.SOUS_TETE;
		}
		
		/** Rafraichit l'arbre */
		public function refresh():void
		{
			orgaStructure.refreshTree();
			orgaStructure.clearSearch();
		}
		
		/** Vide le grid des lignes */
		public  function clearGridLines():void
		{
			gdrLignes.dataProvider = null;
			_dataLines = null
			gdrLignes.selectedItem = null;
			_showLines = false;
		}
		
		/** 
		 * Retourne True si l'element selectionné n'a pas de fils
		 * sinon false
		 * */
		public function isLastSelectedChild():Boolean
		{
			var treeRef:Tree =	orgaStructure.getTreeRef();
			if (treeRef.selectedIndex < 0)
				return false;
			var tmp:XML = XML(treeRef.selectedItem);
			
			if (tmp.children()[0] == null)
				return true;
			return false;
		}
		
		/** Renvoie toutes les organisations */
		public function getOrganisationData():ArrayCollection
		{
			return _organisationData;
		}
		
		/** Renvoie le type de l'orga selectionné */
		public function getSelectedTypeOrga():String
		{
			return orgaStructure.getSelectedTypeOrga();
		}
		
		/** initialise la recherche */
		public function clearSearch():void
		{
			orgaStructure.clearSearch();
		}
		
		/** En analytique : renvoie la date selectionné */
		public function getSelectedDate():String 
		{
			if (cdcDate.visible == true)
				return df.format(cdcDate.data);
			return null;
		}
		
		/** Rafraichit le grid lignes */
		public function refreshLineGrid():void
		{
			if (_showLines == false)
				return ;
			var id:Number = orgaStructure.getSelectedTreeItemValue();
			displayLines(id);
		}
		
		/** Ajoute un élement dans les organisations */
		public function refreshOrganisation(item:Object):void
		{
			orgaStructure.addOrganisationInArray(item);
		}
		
		/** Rafraichit l'état de tous les boutons de la partie gauche */
		public function refreshButtons():void
		{
		
		}

		
		/** ######################## END PUBLIC METHODS ###################### */


		private function throwResult(result:ResultEvent):void
		{
			
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}	
	}
}
