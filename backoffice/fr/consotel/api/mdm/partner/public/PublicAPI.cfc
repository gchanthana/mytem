<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.PublicAPI" hint="Implémentation commune des API Partenaires Publiques MDM<br>
<b>Le constructeur getPublicZenprise() doit etre appelé coté BackOffice pour obtenir une instance de cette classe sur laquelle les autres méthodes peuvent etre appelées</b>">
	<cffunction access="package" name="getServiceProvider" returntype="fr.consotel.api.mdm.partner.private.MdmServiceProvider">
		<cfif NOT structKeyExists(VARIABLES,"SERVICE_PROVIDER")>
			<cfset var authDomain={DOMAIN="public.consotel.fr", PUBLIC_CLIENT=THIS}>
			<cfif structKeyExists(VARIABLES,"PUBLIC_CREDENTIALS")>
				<cfset authDomain=VARIABLES["PUBLIC_CREDENTIALS"]>
			</cfif>
			<cfset VARIABLES["SERVICE_PROVIDER"]=createObject("component","fr.consotel.api.mdm.partner.private.MdmServiceProvider").getServiceProvider(authDomain)>
		</cfif>
		<cfreturn VARIABLES["SERVICE_PROVIDER"]>
	</cffunction>
</cfcomponent>