package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class HasEqpAssociated implements IRuleDisplayItem
	{
		public function HasEqpAssociated()
		{
		}

		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			if ((ligne.T_IMEI != null && ligne.T_IMEI != "") || (ligne.T_NO_SERIE != null && ligne.T_NO_SERIE != ""))
				return true
			else
				return false
		}
	}
}
