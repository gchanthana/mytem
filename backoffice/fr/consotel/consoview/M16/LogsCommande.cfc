<cfcomponent output="false">
	
	<!--- INSERTION DU LOG D'UNE COMMANDE --->
	
	<cffunction name="insertlogCommande" returntype="numeric" access="remote" hint="INSERTION DU LOG D'UNE COMMANDE">
		<cfargument name="idRacine" 		type="numeric"	 						required="true">
		<cfargument name="libelleRacine" 	type="string"	 						required="true">
		<cfargument name="idGestionnaire" 	type="numeric"	 						required="true">
		<cfargument name="gestionnaire" 	type="string"	 						required="true">
		<cfargument name="mailGestionnaire"	type="string"	 						required="true">
		<cfargument name="mail" 			type="fr.consotel.consoview.util.Mail" 	required="true">
		<cfargument name="idCommande" 		type="numeric" 							required="true">
		<cfargument name="message"	 		type="string" 							required="true">
		<cfargument name="jobId" 			type="numeric"							required="true">
		<cfargument name="status" 			type="string" 							required="true"  default="n.c.">
		<cfargument name="theUuid" 			type="string" 							required="true"  default="n.c.">
		<cfargument name="codeApp"		 	type="Numeric" 							required="false" default="1"/>
		
			<cfset cc = mail.getCc()>
					
			<cfif mail.getCopiePourExpediteur() eq "YES">
				<cfif len(mail.getCc()) eq 0>
					<cfset cc = mail.getExpediteur()>
				</cfif>
			</cfif>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.insert_log_commande">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDCOMMANDE" 	type="in"   value="#idCommande#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDGESTIONNAIRE" type="in"   value="#idGestionnaire#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_RACINE" 		type="in"   value="#libelleRacine#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_GESTIONNAIRE" 	type="in"   value="#gestionnaire#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MAIL" 			type="in"   value="#mailGestionnaire#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_FROM" 			type="in"   value="#mail.getExpediteur()#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_TO" 			type="in"   value="#mail.getDestinataire()#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_CC" 			type="in"   value="#cc#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_BCC" 			type="in"   value="#mail.getBcc()#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MODULE" 		type="in"   value="#mail.getModule()#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_SUBJECT" 		type="in"   value="#mail.getSujet()#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MESSAGE" 		type="in"   value="#message#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_JOB_ID" 		type="in"   value="#jobId#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_STATUS" 		type="in"   value="#status#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" 		type="in"   value="#idRacine#"/>
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_UUID" 			type="in"   value="#theUuid#"/>
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_RETOUR" 		type="out"  />
			</cfstoredproc>
		
		<cfreturn P_RETOUR>
	</cffunction>
	
	<!--- INSERTION DU LOG D'UNE COMMANDE AVEC UN UUID --->
	
	<cffunction name="insertlogBDCMobile" returntype="numeric" access="remote" hint="INSERTION DU LOG D'UNE COMMANDE AVEC UN UUID">
		<cfargument name="idRacine" 		type="numeric"	 						required="true">
		<cfargument name="libelleRacine" 	type="string"	 						required="true">
		<cfargument name="idGestionnaire" 	type="numeric"	 						required="true">
		<cfargument name="gestionnaire" 	type="string"	 						required="true">
		<cfargument name="mailGestionnaire"	type="string"	 						required="true">
		<cfargument name="mail" 			type="fr.consotel.consoview.util.Mail" 	required="true">
		<cfargument name="idCommande" 		type="numeric" 							required="true">
		<cfargument name="message"	 		type="string" 							required="true">
		<cfargument name="bdcUuid"	 		type="string" 							required="true">
		<cfargument name="jobId" 			type="numeric"							required="true">
		<cfargument name="status" 			type="string" 							required="true" default="n.c.">
		
		<cfset cc = mail.getCc()>
				
		<cfif mail.getCopiePourExpediteur() eq "YES">
			<cfif len(mail.getCc()) eq 0>
				<cfset cc = mail.getExpediteur()>
			</cfif>
		</cfif>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.insert_log_commande_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDCOMMANDE" type="in"   value="#idCommande#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDGESTIONNAIRE" type="in"   value="#idGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_RACINE" type="in"   value="#libelleRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_GESTIONNAIRE" type="in"   value="#gestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MAIL" type="in"   value="#mailGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_FROM" type="in"   value="#mail.getExpediteur()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_TO" type="in"   value="#mail.getDestinataire()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_CC" type="in"   value="#cc#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_BCC" type="in"   value="#mail.getBcc()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MODULE" type="in"   value="#mail.getModule()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_SUBJECT" type="in"   value="#mail.getSujet()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MESSAGE" type="in"   value="#message#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_UUID" type="in"   value="#bdcUuid#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_JOB_ID" type="in"   value="#jobId#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_STATUS" type="in"   value="#status#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" type="in"   value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_RESULT" type="out"  />
		</cfstoredproc>		
		<cfreturn P_RESULT>
	</cffunction>
	
	<!--- AFFICHAGE DES LOGS DES COMMANDES POUR UNE RACINE OU POUR TOUTES LES RACINES SI IDRACINE = 0 ENTRE DEUX DATES --->
	
	<cffunction name="updateJobIDBDCMobile" returntype="numeric" access="remote" hint="AFFICHAGE DES LOGS DES COMMANDES POUR UNE RACINE OU POUR TOUTES LES RACINES SI IDRACINE = 0 ENTRE DEUX DATES">
		<cfargument name="theUuid" type="string" 	required="true">
		<cfargument name="jobId" type="numeric" 		required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.updateJobId">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_UUID" type="in"   value="#theUuid#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_jobId" type="in"   value="#jobId#"/>
			 <cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out"/>
		</cfstoredproc>
		
		<cfreturn p_retour>	
	</cffunction>
	
	<!--- AFFICHAGE DES LOGS DES COMMANDES POUR UNE RACINE OU POUR TOUTES LES RACINES SI IDRACINE = 0 ENTRE DEUX DATES --->
	
	<cffunction name="updateJobId" returntype="numeric" access="remote" hint="AFFICHAGE DES LOGS DES COMMANDES POUR UNE RACINE OU POUR TOUTES LES RACINES SI IDRACINE = 0 ENTRE DEUX DATES">
		<cfargument name="theUuid" type="string" 	required="true">
		<cfargument name="jobId" type="numeric" 		required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.updateJobId">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_UUID" type="in"   value="#theUuid#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_jobId" type="in"   value="#jobId#"/>
			 <cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out"/>
		</cfstoredproc>
		
		<cfreturn p_retour>	
	</cffunction>
	
	<!--- AFFICHAGE DES LOGS DES COMMANDES POUR UNE RACINE OU POUR TOUTES LES RACINES SI IDRACINE = 0 ENTRE DEUX DATES --->
	
	<cffunction name="getlogsCommande" returntype="query" access="remote" hint="AFFICHAGE DES LOGS DES COMMANDES POUR UNE RACINE OU POUR TOUTES LES RACINES SI IDRACINE = 0 ENTRE DEUX DATES">
		<cfargument name="idRacine" type="Numeric" 	required="true">
		<cfargument name="dateDebut" type="Date" 	required="true">
		<cfargument name="dateFin" type="Date" 		required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.get_logs_commande">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="IDRACINE" type="in" value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut" type="in"   value="#lsDateFormat(dateDebut,'YYYY/MM/DD')#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_date_fin" type="in"   value="#lsDateFormat(dateDebut,'YYYY/MM/DD')#"/>
			<cfprocresult name="P_RESULT">
		</cfstoredproc>
		<cfreturn P_RESULT>	
	</cffunction>

	<!--- AFFICHAGE DES LOGS DES COMMANDES SUIVANT L'UUID --->

	<cffunction name="getlogsCommandeuuid" returntype="Any" access="remote" hint="AFFICHAGE DES LOGS DES COMMANDES SUIVANT L'UUID">
		<cfargument name="uuidlog" 		type="String" 	required="true">
<!--- 		<cfargument name="idcommande" 	type="Numeric" 	required="false">
		<cfargument name="dirsource" 	type="String" 	required="false"> --->
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.get_logs_commande_uuid">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_uuidlog" type="IN"   value="#uuidlog#"/>
			<cfprocresult name="p_result">
		</cfstoredproc>
		
<!--- -------------------------------------------- --->			
		
		<!--- VARIABLES TEMPORAIRES --->
		
			<!--- <cfset expediteur 	= "mailcommandenicolasrenel@consotel.fr">
			<cfset destinataires= "dev@consotel.fr"><!--- SEPARATEUR VIRGULE --->
			<cfset subject 		= "test Commande nicolas renel">
			<cfset corpsmail	= "Bonjour,<br><br> Ceci est un test pour la commande mobile/fixe/data !
									<br><br>Cordialement,<br>Nicolas RENEL<br><br> IDCOMMANDE="&#idcommande#&
									"<br><br> DIRSOURCE="&#dirsource#> --->
		
		<!--- CREATION D'UNE STRUCTURE S'APPRENTANT A LA STRUCTURE FINAL --->
		
			<!--- <cfset p_result 			= structnew()>
			<cfset p_result.MAIL_FROM 	= expediteur>
			<cfset p_result.MAIL_TO 	= destinataires>
			<cfset p_result.SUBJECT 	= subject>
			<cfset p_result.MESSAGE 	= corpsmail>
			<cfset p_result.UUIDLOG 	= uuidlog> --->
			
<!--- -------------------------------------------- --->	

		<cfreturn p_result>	
	</cffunction>

</cfcomponent>