package rapport.service.perimetre
{
	import flash.events.EventDispatcher;
	import mx.rpc.events.ResultEvent;
	import rapport.event.AccueilEvent;
	
	public class PerimetreHandlers extends EventDispatcher
	{
		private var _model : PerimetreModel;
		
		public function PerimetreHandlers(modele:PerimetreModel)
		{	
			_model = modele;
		}
		
		internal function setUpPerimetreResultHandler(event:ResultEvent):void
		{	
			if(event.result > 0)
			{
				dispatchEvent(new rapport.event.AccueilEvent(AccueilEvent.PERIMETRE_CHANGE));
			}
			else if(event.result == 0)
			{
				dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_NOT_CHANGE));
			}
			else 
			{
				dispatchEvent(new AccueilEvent(AccueilEvent.PERIMETRE_ERROR));
			}
		}

	}
}