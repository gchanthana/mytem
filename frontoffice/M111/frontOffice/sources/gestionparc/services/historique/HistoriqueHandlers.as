package gestionparc.services.historique
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.rpc.events.ResultEvent;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Envoie les données reçues à la classe <code>HistoriqueDatas</code>,
	 * au retour des procédures appelées dans <code>HistoriqueServices</code>.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 20.08.2010
	 * 
	 * </pre></p>
	 * 
	 * @see gestionparc.services.historique.HistoriqueDatas
	 * @see gestionparc.services.historique.HistoriqueServices
	 * 
	 */
	public class HistoriqueHandlers
	{
		// VARIABLES------------------------------------------------------------------------------
		
		private var myDatas : HistoriqueDatas;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Instancie les datas.
		 * </pre></p>
		 * 
		 * @param instanceOfDatas:<code>HistoriqueDatas</code>.
		 */	
		public function HistoriqueHandlers(instanceOfDatas:HistoriqueDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		// INTERNALS FUNCTIONS------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getHistorique()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetHistorique()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.sim.SimServices#getHistoriqueAction()
		 * @see gestionparc.services.sim.SimDatas#treatDataGetHistorique()
		 * 
		 */	
		internal function getHistoriqueHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetHistorique(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_FICHE_HISTO_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getHistoriqueActionRessource()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetHistoriqueActionRessource()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.sim.SimServices#getHistoriqueActionRessource()
		 * @see gestionparc.services.sim.SimDatas#treatDataGetHistoriqueActionRessource()
		 * 
		 */	
		internal function getHistoriqueActionRessourceResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetHistoriqueActionRessource(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_FICHE_HISTO_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		
		// INTERNALS FUNCTIONS------------------------------------------------------------------------

	}
}