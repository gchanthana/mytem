package general.appliComposants
{
	public class CvPerimetreNodeVO
	{
		private var _LBL:String;
		private var _NID:int;
		private var _HASRIGHT:Boolean = true
		
		public function CvPerimetreNodeVO()
		{
		}


		public function set NID(value:int):void
		{
			_NID = value;
		}

		public function get NID():int
		{
			return _NID;
		}

		public function set LBL(value:String):void
		{
			_LBL = value;
		}

		public function get LBL():String
		{
			return _LBL;
		}

		public function set HASRIGHT(value:Boolean):void
		{
			_HASRIGHT = value;
		}

		public function get HASRIGHT():Boolean
		{
			return _HASRIGHT;
		}
	}
}