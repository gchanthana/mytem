package univers.inventaire.contrat.ihm
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;

	public class CreerContratEquipementImpl extends TitleWindow
	{
		public var btContratCadre:Button;
		public var ppContratsCadre:ListeContratsIHM;
		
		public function CreerContratEquipementImpl()
		{
			super();
		}		
		
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this)
		}
		
		
		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			
		}
		
		protected function btValiderClickHander(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this)
		}
		
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this)	
		}

		protected function _btContratCadreClickHandler(event:MouseEvent):void
		{
			ppContratsCadre = new ListeContratsIHM();
			ppContratsCadre.titre = "Sélectionnez le contrat cadre";
			PopUpManager.addPopUp(ppContratsCadre,this,true);
			PopUpManager.centerPopUp(ppContratsCadre);		
		}
	}
}