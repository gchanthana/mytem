package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleDissocierEqpForLigne implements IRuleDisplayItem
	{
		public function RuleDissocierEqpForLigne()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneMobile();
			var r2:IRuleDisplayItem=new HasSimAssociated();
			var r3:IRuleDisplayItem=new HasEqpAssociated();

			if (r1.isDisplay() && r2.isDisplay() && r3.isDisplay())
				return true;
			else
				return false;
		}
	}
}
