package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	/**
	 * Classe, definissant une erreur dans l'instanciation des ecrans des Commandes
	 * */
	public class DemandeError extends Error
	{
		/**
		 * Constructeur
		 * @param message le message d'erreur à afficher 
		 * @id id l'identifiant unique pour l'erreur
		 * */
		public function DemandeError(message:String="", id:int=0)
		{
			//TODO: implement function
			super(message, id);
		}
		
	}
}