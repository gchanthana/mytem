package service.fiche.FicheDetailClient
{
	import mx.rpc.AbstractOperation;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class FicheDetailClientServices
	{
		[Bindable] public var myDatas 		: FicheDetailClientDatas;
		[Bindable] public var myHandlers 	: FicheDetailClientHandlers;
		
		public function FicheDetailClientServices()
		{
			myDatas 	= new FicheDetailClientDatas();
			myHandlers 	= new FicheDetailClientHandlers(myDatas);
		}
		public function getDetailClientContractuel(clientId:int):void
		{
			 var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				 "fr.consotel.consoview.M28.fiche.FctListeClient", 
				 "getDetailClientContractuel",
				 myHandlers.getDetailClientHandler);
			 
			 RemoteObjectUtil.callService(op,clientId);
		}
	}
}