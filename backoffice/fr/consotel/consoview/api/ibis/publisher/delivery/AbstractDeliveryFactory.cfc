<cfcomponent displayname="fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryFactory"
									hint="Creates instances of BIP Webservice DeliveryRequest Objects">
	<!--- STATIC PROPERTIES
		VARIABLES["DEFAULT-MAIL-ADMIN"] : Default ADMIN email (cedric.rapiera@consotel.fr)
		
		Roadmap :
		- Add support for contentType element in DeliveryRequest definition (PublicReportService_v11). Available values in BIP SOAP API.
	 --->
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryFactory"
			hint="Instance Initialized : Explicit Call Only">
		<cfargument name="initParams" type="struct" required="true" hint="Instance Initialization Parameters">
		<cflog type="information" text="AbstractDeliveryFactory Initialization : OK">
		<cfset VARIABLES["DEFAULT-MAIL-ADMIN"]=arguments["initParams"]["DEFAULT-MAIL-ADMIN"]>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="createDeliveryRequest" returntype="struct">
		<cfargument name="parameters" type="struct" required="true">
		<cfset var deliveryRequest=structNew()>
		<cfset var deliveryMode=arguments["parameters"]["deliveryMode"]>
		<cfswitch expression="#UCASE(deliveryMode)#">
			<cfcase value="LOCAL">
				<!--- 
				<cfset deliveryRequest = {"localOption" = {"destination"=arguments["parameters"]["destination"]}}>
				 --->
				<cfset deliveryRequest=structNew()>
				<cfset deliveryRequest["localOption"]=structNew()>
				<cfset deliveryRequest["localOption"]["destination"]=arguments["parameters"]["destination"]>
			</cfcase>
			<cfcase value="BURST">
				<cflog type="information" text="Using Empty Delivery for BURST delivery Mode">
			</cfcase>
			<cfcase value="FTP">
				<!--- 
				<cfset deliveryRequest = {"ftpOption" = {"remoteFile"=arguments["parameters"]["remoteFile"],
					"ftpServerName"="PELICAN","ftpUserName"="services","ftpUserPassword"="services","sftpOption"=FALSE}}>
				 --->
				<cfset deliveryRequest = structNew()>
				<cfset deliveryRequest["ftpOption"]=sttructNew()>
				<cfset deliveryRequest["ftpOption"]["remoteFile"]=arguments["parameters"]["remoteFile"]>
				<cfset deliveryRequest["ftpOption"]["ftpServerName"]="PELICAN">
				<cfset deliveryRequest["ftpOption"]["ftpUserName"]="container">
				<cfset deliveryRequest["ftpOption"]["ftpUserPassword"]="container">
				<cfset deliveryRequest["ftpOption"]["sftpOption"]=FALSE>
				
			</cfcase>
			<cfcase value="CONTAINER">
				<!--- 
				<cfset deliveryRequest={"ftpOption"=createContainerOption(arguments["parameters"])}>
				 --->
				<cfset deliveryRequest=structNew()>
				<cfset deliveryRequest["ftpOption"]=createContainerOption(arguments["parameters"])>
			</cfcase>
			<cfcase value="EMAIL">
				<!--- 
				<cfset deliveryRequest={"emailOption"=createEmailOption(arguments["parameters"])}>
				 --->
				<cfset deliveryRequest=structNew()>
				<cfset deliveryRequest["emailOption"]=createEmailOption(arguments["parameters"])>
			</cfcase>
			<cfdefaultcase>
				<cflog type="error" text="INVALID DELIVERY MODE : #UCASE(deliveryMode)#">
				<cfabort showerror="INVALID DELIVERY MODE : SEE LOG FILES FOR DETAILS">
			</cfdefaultcase>
		</cfswitch>
		<cfreturn deliveryRequest>
	</cffunction>
	
	<cffunction access="private" name="createContainerOption" returntype="struct" hint="Creates DeliveryRequest for CONTAINER delivery mode">
		<cfargument name="parameters" type="struct" required="true">
		<cfset outputFilename="/app/pelican/container/" & arguments["parameters"]["jobName"] & ".consotel">
		<cfif LEN(TRIM(arguments["parameters"]["jobName"])) EQ 0>
			<cfset outputFilename="/app/pelican/container/OUTPUT." & arguments["parameters"]["outputExtension"]>
		</cfif>
		<!--- 
		<cfreturn {"remoteFile"=outputFilename,"ftpServerName"="CONTAINER","ftpUserName"="container","ftpUserPassword"="container","sftpOption"=FALSE}>
		 --->
		<cfset returnRES=structNew()>
		<cfset returnRES["remoteFile"]=outputFilename>
		<cfset returnRES["ftpServerName"]="CONTAINER">
		<cfset returnRES["ftpUserName"]="container">
		<cfset returnRES["ftpUserPassword"]="container">
		<cfset returnRES["sftpOption"]=FALSE>
		<cfreturn returnRES>
	</cffunction>
	
	<cffunction access="private" name="createEmailOption" returntype="struct" hint="Creates DeliveryRequest for EMAIL delivery mode">
		<cfargument name="parameters" type="struct" required="true">
		<!--- 
		<cfset var emailOption={"emailFrom"=VARIABLES["DEFAULT-MAIL-ADMIN"],"emailReplyTo"=VARIABLES["DEFAULT-MAIL-ADMIN"],
												"emailTo"=VARIABLES["DEFAULT-MAIL-ADMIN"],"emailSubject"="","emailBody"=""}>
		<cfset emailOption["emailFrom"]=VARIABLES["DEFAULT-MAIL-ADMIN"]>
		<cfset emailOption["emailReplyTo"]=VARIABLES["DEFAULT-MAIL-ADMIN"]>
		<cfset emailOption["emailTo"]=VARIABLES["DEFAULT-MAIL-ADMIN"]>
		 --->
		<cfset var emailOption=structNew()>
		<cfset emailOption["emailSubject"]="">
		<cfset emailOption["emailBody"]="">
		
		<cfif structKeyExists(arguments["parameters"],"emailFrom")>
			<cfset emailOption["emailFrom"]=arguments["parameters"]["emailFrom"]>
			<cfset emailOption["emailReplyTo"]=emailOption["emailFrom"]>
		<cfelse>
			<cflog type="error" text="USING EMAIL Delivery Mode without [emailFrom]">
			<cfabort showerror="USING EMAIL Delivery Mode without [emailFrom]">
		</cfif>
		<cfif structKeyExists(arguments["parameters"],"emailTo")>
			<cfset emailOption["emailTo"]=arguments["parameters"]["emailTo"]>
			<cflog type="information" text=">>> Delivery EMAIL TO : #emailOption["emailTo"]#">
		<cfelse>
			<cflog type="warning" text="USING EMAIL Delivery Mode without [emailTo]">
		</cfif>
		<cfif structKeyExists(arguments["parameters"],"emailReplyTo")>
			<cfset emailOption["emailReplyTo"]=arguments["parameters"]["emailReplyTo"]>
			<cflog type="information" text=">>> Delivery EMAIL REPLYTO : #emailOption["emailReplyTo"]#">
		</cfif>
		<cfif structKeyExists(arguments["parameters"],"emailCC")>
			<cfset emailOption["emailCC"]=arguments["parameters"]["emailCC"]>
		</cfif>
		<cfif structKeyExists(arguments["parameters"],"emailBCC")>
			<cfset emailOption["emailBCC"]=arguments["parameters"]["emailBCC"]>
		</cfif>
		<cfif structKeyExists(arguments["parameters"],"emailSubject")>
			<cfset emailOption["emailSubject"]=arguments["parameters"]["emailSubject"]>
		<cfelse>
			<cflog type="error" text="USING EMAIL Delivery Mode without [emailSubject]">
			<cfabort showerror="USING EMAIL Delivery Mode without [emailSubject]">
		</cfif>
		<cfif structKeyExists(arguments["parameters"],"emailBody")>
			<cfset emailOption["emailBody"]=arguments["parameters"]["emailBody"]>
		<cfelse>
			<cfset emailOption["emailBody"]=" ">
		</cfif>
		<cfreturn emailOption>
	</cffunction>
</cfcomponent>