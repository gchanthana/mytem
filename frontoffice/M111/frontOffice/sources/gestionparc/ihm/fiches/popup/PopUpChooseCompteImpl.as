package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import composants.ui.TitleWindowBounds;
	
	import gestionparc.event.gestionparcEvent;

	public class PopUpChooseCompteImpl extends TitleWindowBounds
	{
		
		//--------------- VARIABLES ----------------//
		[Bindable] 
		public var rbgItem				:RadioButtonGroup;
		[Bindable] 
		public var itemCurrentLabel		:Label;
		public var dgChooseItem			:DataGrid;
		public var tiFiltre				:TextInput;
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		[Bindable]
		public var btnValidEnabled		:Boolean = false;
		
		private var _itemCurrent		:Object = null;
		private var _selectedIndex		:Number;
		private var _listeItem 			:ArrayCollection;
		private var _itemSelectedFirst 	: Object = null;
		
		//--------------- METHODES ----------------//
		
		public function PopUpChooseCompteImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		private function initData():void
		{
			listeItem.filterFunction = filtrerItem;
			listeItem.refresh();
			
			// initialisation de l'affichage du label
			afficherInitialLabel();
			initCurseur();
		}
		
		private function initCurseur():void
		{
			callLater(tiFiltre.setFocus);
		}
		
		// initialisation de l'affichage du label
		private function afficherInitialLabel():void
		{
			if(listeItem != null)
			{
				var long:int = listeItem.length;
				for (var i:int = 0; i < long; i++) 
				{
					if(listeItem[i].SELECTED)
					{
						itemCurrent = listeItem[i];
						_itemSelectedFirst = listeItem[i]; 
						break;
					}
				}
			}
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		private function initListeners():void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseRacineHandler);
			addEventListener(KeyboardEvent.KEY_DOWN,validerChooseRacineHandler2);
			btnAnnuler.addEventListener(MouseEvent.CLICK,onCloseHandler);
			dgChooseItem.addEventListener(ListEvent.CHANGE,currentItemHandler);
			
		}
		
		/* */
		protected function currentItemHandler(le:ListEvent):void
		{
			itemCurrent = le.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listeItem.length)
			{
				if(itemCurrent!=listeItem[i])
					listeItem[i].SELECTED = false;
				else
				{
					listeItem[i].SELECTED = true;
					//					_selectedIndex = i;
				}
				listeItem.itemUpdated(listeItem[i]);
				i++;
			}
			
			itemCurrentLabel.text = getItemCurrentLabel();
			btnValidEnabled = true;
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeItem.filterFunction = filtrerItem;
			listeItem.refresh();
		}
		
		/* systeme de filtre */
		private function filtrerItem(item:Object):Boolean
		{
			if(dgChooseItem.selectedIndex)
				rbgItem.selection = rbgItem.getRadioButtonAt(dgChooseItem.selectedIndex + 1);
			
			if (item.LABEL != null && (item.LABEL as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		/**
		 * Retourne le libelle racine approprié peuplant la combobox :
		 */
		public function labelItemFunction(item:Object, column:DataGridColumn):String
		{
			return item.LABEL;
		}
		
		protected function onCloseHandler(event:Event):void
		{
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			this.initPopUp();
			PopUpManager.removePopUp(this);
		}
		
		// initialise la popup vers le choix initial
		private function initPopUp():void
		{
			for (var i:int = 0; i < listeItem.length; i++) 
			{
				listeItem[i].SELECTED = false;
				if((_itemSelectedFirst != null)&& (listeItem[i].IDCOMPTE == _itemSelectedFirst.IDCOMPTE))
				{
					listeItem[i].SELECTED = true;
					listeItem.itemUpdated(listeItem[i]);
					break;
				}
				
			}
			
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		/* au click sur valider */
		private function validerChooseRacineHandler(evt:MouseEvent):void
		{
			validerChoix();
		}
		
		private function validerChoix():void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.VALID_CHOIX_COMPTE,true));
			onCloseHandler(null);
			
		}
		
		private function validerChooseRacineHandler2(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
			{
				validerChoix();
			}
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		[Bindable]
		public function get listeItem():ArrayCollection
		{
			return _listeItem;
		}
		public function set listeItem(value:ArrayCollection):void
		{
			_listeItem = value;
		}
		
		public function get itemCurrent():Object
		{
			return _itemCurrent;
		}
		
		public function set itemCurrent(value:Object):void
		{
			_itemCurrent = value;
		}
		
		public function getItemCurrentLabel():String
		{
			if (_itemCurrent != null)
				return _itemCurrent.LABEL;
			else
				return '';
		}
	}
}