package  rapport.utils
{
	import flash.display.DisplayObject;
	import mx.containers.Canvas;
	import mx.controls.Label;
	
	[DefaultProperty("children")]
	
	public class FieldSetContainer extends Canvas
	{
		protected var titleLabel:Label;
		private var dataChanged:Boolean = false;
		private var _title:String = "";
		private var _childrenChanged:Boolean = false;
		private var _children:Array = [];
		
		
		public function FieldSetContainer()
		{
			super();
		}
		
		public function set title(value:String):void
		{
			_title = value;
			dataChanged = true;
			invalidateProperties();
		}
		public function set children(value:*):void
		{
			if( value is DisplayObject )
				_children = [ value ];
			else
				_children = value;
			
			_childrenChanged = true;
			invalidateDisplayList();
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if(dataChanged)
			{
				dataChanged = false;
				titleLabel.text = _title;
				invalidateDisplayList();
			}
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			titleLabel = new Label();
			titleLabel.x = 10;
			titleLabel.y = 0;
			titleLabel.setStyle("color", 0x333333);
			titleLabel.setStyle("paddingRight", 7);
			titleLabel.text = _title;
			
			addChild(titleLabel);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if(_childrenChanged)
			{
				_childrenChanged = false;
				
				var index:uint = 1;
				for each(var child:DisplayObject in _children)
				{
					child.y += titleLabel.measuredHeight;
					child.x += 1;
					this.addChildAt(child, index);
					index++;
				}
			}
			
			createBorders(unscaledWidth, unscaledHeight);
		}
		
		protected function createBorders(width:Number, height:Number):void
		{
			var borderTopY:uint = titleLabel.height/2;
			
			this.graphics.clear();
			this.graphics.lineStyle(1, 0x858585);
			this.graphics.moveTo(7, borderTopY);
			this.graphics.lineTo(0, borderTopY);
			this.graphics.lineTo(0, height);
			this.graphics.lineTo(width, height);
			this.graphics.lineTo(width, borderTopY);
			this.graphics.lineTo(titleLabel.x + titleLabel.width - 2, borderTopY);
		}    
	}
}