package catalogue.ihm.parametre
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.util.TarifDatagrid;
	import catalogue.vo.AbonnementVO;
	
	import composants.util.ConsoviewAlert;
	
	[Bindable]
	public class UpdatePlanImpl extends TitleWindow
	{
		private var globalService 	:GlobalCatalogueService = new GlobalCatalogueService();

		 
		private var validatorArr:Array;
		 
		private var erroMess:String=ResourceManager.getInstance().getString('M24', 'Ce_champs_est_obligatoire');
		private var _plan:AbonnementVO;
		private var _idracine:int = 0;
		
		 
		public var dgTarif:TarifDatagrid;
		public var tiNameCommande:TextInput;
		public var tiRefence:TextInput;
		public var ckboxPriceIncmd:CheckBox;
		
		public function UpdatePlanImpl()
		{
			
			super();			
			initListeners();
			
		}
		
		private function initListeners():void
		{
			globalService.addEventListener(CatalogueEvent.UPDATE_ABO_COMPLETE, updateAboCompleteHandler);
			globalService.addEventListener(CatalogueEvent.UPDATE_FAILURE, updateAboFailureHandler);
		}
		
		
		
		public function get plan():AbonnementVO
		{
			return _plan;
		}

		public function set plan(value:AbonnementVO):void
		{
			_plan = value;
		}

		public function fermer(event : Event=null):void
		{
			PopUpManager.removePopUp(this);
		}
		
		
		protected function rdbtnClickHandler(me:MouseEvent):void
		{
			var idStrg:String = (me.target as RadioButton).id;
			
			if(idStrg == 'rbAffiche')
			{
				plan.ISVISIBLETOCOMMANDE = true;
			}
			else if(idStrg == 'rbNoAffiche')
			{	
				plan.ISVISIBLETOCOMMANDE = false;					
			}
		}
		
		public function btValiderFermerHandler():void
		{
			plan.LIBELLE = tiNameCommande.text;
			plan.REFERENCE = tiRefence.text;
			plan.ISVISIBLETOCOMMANDE = ckboxPriceIncmd.selected;
			plan.toXMl();
			var boolAllTarifValide : Boolean = true;
			var rowRenderers:Array = dgTarif.listRendererArray;
			for(var i:int = 0; i < rowRenderers[0].length; i++)
			{
				if(rowRenderers[0][i].tarifIsValide==false)
				{
					boolAllTarifValide = false;
				}
			}
			
			if(boolAllTarifValide)
			{
				if(_plan.IDABO > 0)
				{
					var plans:XML = <plans></plans>;
					plans.appendChild(plan.toXMl());
					
					globalService.updatePlanCatalog(plan.IDCLIENT, plans);
					trace(ObjectUtil.toString(plans));
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Tous_les_tarifs_doivent__tre_valides'), ResourceManager.getInstance().getString('M24', 'Erreur'));
			}
		}
		
		protected function updateAboCompleteHandler(ce:CatalogueEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Ok'));
			
			dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			
			fermer();
		}
		
		protected function updateAboFailureHandler(ce:CatalogueEvent):void
		{
			//ConsoviewAlert.a (ResourceManager.getInstance().getString('M24', 'Abonnement_modifi__'));
			trace("---> updateAboFailureHandler <-----");
			//dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			
			//fermer();
		}
	}
}