package univers.facturation.suivi.controles.vues
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparcmobile.GestionParcMobileImpl;
	import gestionparcmobile.event.CheckBoxChangeEvent;
	import gestionparcmobile.event.ContentFicheEvent;
	import gestionparcmobile.renderer.EditerItemRenderer;
	import gestionparcmobile.renderer.SupprimerItemRenderer;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.Spacer;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.ClassFactory;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class TabModel extends VBox
	{
		private var hb_top:HBox = new HBox();
		private var hb_bottom:HBox = new HBox();
		private var bt_action:Button = new Button();
		private var bt_enregistrer:Button = new Button();
		private var lb_libelle:Label = new Label();
		private var dg_datagrid:DataGrid = new DataGrid();
		private var sp_top:Spacer = new Spacer();
		private var sp_bottom:Spacer = new Spacer();
		
		private var _dataDg:ArrayCollection = new ArrayCollection();
		private var _colonnes:ArrayCollection = new ArrayCollection();
		private var _colSupVisible:Boolean = false;
		
		private var nbElt:int;
		private var colEditer:DataGridColumn = new DataGridColumn();
		private var colDesaffecter:DataGridColumn = new DataGridColumn();
		
		public var colSortirInventaire:DataGridColumn = new DataGridColumn(); // public pour la gestion des abo et opt dans la fiche ligne
		public var colEntrerInventaire:DataGridColumn = new DataGridColumn(); // public pour la gestion des abo et opt dans la fiche ligne
		private var colInventaire:DataGridColumn = new DataGridColumn();
		private var colHistorique:DataGridColumn = new DataGridColumn();
		
		private var _tabLength:int;
	
		private var _typeAjoutColonne:String = "none";
		private var _selectedItem:Object;
	
	
		public function TabModel() {
			super();
			addTopContainer();
			addMiddleContainer();
			addBottomContainer();
			this.addEventListener(CheckBoxChangeEvent.CHBX_SELECTION_CHANGE, activeButtonSave);
		}
		
		public function set buttonEnabled(value:Boolean):void {
			bt_enregistrer.enabled = value;
		}
		
		public function get buttonEnabled():Boolean {
			return bt_enregistrer.enabled;
		}
		
		
		private function activeButtonSave(evt:*):void {
			buttonEnabled = true;
		}
		
		private function addTopContainer():void {
			sp_top.percentWidth = 100;
			hb_top.addChild(lb_libelle);
			hb_top.percentWidth = 100;
			hb_top.addChild(sp_top);
			hb_top.addChild(bt_action);
			bt_action.addEventListener(MouseEvent.MOUSE_DOWN, clickBtAction);
			this.addChild(hb_top);
		}
		
		private function addMiddleContainer():void {
			dg_datagrid.percentWidth = 100;
			dg_datagrid.addEventListener(ListEvent.CHANGE, _dg_datagridChangeHandler);
			this.addChild(dg_datagrid);
		}
		
		private function addBottomContainer():void {
			sp_bottom.percentWidth = 100;
			bt_enregistrer.addEventListener(MouseEvent.MOUSE_DOWN, clickBtEnregistrer);
			hb_bottom.addChild(sp_bottom);
			//hb_bottom.addChild(bt_enregistrer);
			bt_enregistrer.enabled = false;
			hb_bottom.percentWidth = 100;
			this.addChild(hb_bottom);
		}
		
		/**
		 *	(String) Fonction d'ajout des colonnes du datagrid avec personnalisation des dernières colonnes
		 **/ 
		public function addColonnes(value:String):void {
			var listColonnes:Array = new Array();
			
			for(var i:int = 0; i < _colonnes.length; i++) 
			{
				if(_colonnes[i] is DataGridColumn)
				{
					listColonnes.push(_colonnes[i]);
				}
				
			}

			switch(value) {
				case "collab":
					//ajouter 2 colonnes
					colEditer.itemRenderer = new ClassFactory(gestionparcmobile.renderer.EditerItemRenderer);
					colEditer.width = 50;
					colEditer.sortable = false;
					listColonnes.push(colEditer);
					
					colDesaffecter.itemRenderer = new ClassFactory(gestionparcmobile.renderer.SupprimerItemRenderer);
					colDesaffecter.width = 75;
					colDesaffecter.sortable = false;
					
					if(colSupVisible)
					{
						listColonnes.push(colDesaffecter);
					}
					break;
				case "ligne":
					//ajouter 3 colonnes
					colEntrerInventaire.itemRenderer = new ClassFactory(gestionparcmobile.renderer.CheckBoxItemRendererIHM_V2);
					colEntrerInventaire.width = 30;
					colDesaffecter.sortable = false;
					colSortirInventaire.itemRenderer = new ClassFactory(gestionparcmobile.renderer.CheckBoxItemRendererIHM_V2);
					colSortirInventaire.width = 30;
					colDesaffecter.sortable = false;
					colInventaire.itemRenderer = new ClassFactory(gestionparcmobile.renderer.CheckBoxItemRendererIHM_V2);
					colInventaire.width = 30;
					colDesaffecter.sortable = false;
					colHistorique.itemRenderer = new ClassFactory(gestionparcmobile.renderer.HistoriqueItemRenderer);
					colHistorique.width = 30;
					colDesaffecter.sortable = false;
					
					colEntrerInventaire.headerText = ResourceManager.getInstance().getString('M11', '__entrer');
					colSortirInventaire.headerText = ResourceManager.getInstance().getString('M11', '__sortir');
					colEntrerInventaire.dataField = "aEntrer";
					colSortirInventaire.dataField = "aSortir";
					colInventaire.dataField = "dansInventaire";
					
					listColonnes.push(colInventaire);
					listColonnes.push(colEntrerInventaire);
					listColonnes.push(colSortirInventaire);
					listColonnes.push(colHistorique);
					break;
				case "sim":
					//ajouter 1 colonnes
					colInventaire.itemRenderer = new ClassFactory(gestionparcmobile.renderer.CheckBoxItemRendererIHM_V2);
					colHistorique.itemRenderer = new ClassFactory(gestionparcmobile.renderer.HistoriqueItemRenderer);
					
					colInventaire.dataField = "dansInventaire";
					
					listColonnes.push(colInventaire);
					listColonnes.push(colHistorique);
					break;
				case "none":
					//ne pas ajouter de colonnes
					break;
			}
		
			dg_datagrid.columns = listColonnes;
		}
		
		private function clickBtEnregistrer(evt:MouseEvent):void {
			switch(GestionParcMobileImpl.ClassFiche.NAV_SELECTED) {
				case "aboOption":
					this.dispatchEvent(new ContentFicheEvent(ContentFicheEvent.ENREGISTRER_ABO_OPTION, true, GestionParcMobileImpl.ClassFiche.NAV_SELECTED));
					break;
				case "contratOperateur":
					this.dispatchEvent(new ContentFicheEvent(ContentFicheEvent.ENREGISTRER_CONTRAT_OPERATEUR, true, GestionParcMobileImpl.ClassFiche.NAV_SELECTED));
					break;
			}
		}
		
		private function clickBtAction(evt:MouseEvent):void {
			//this.dispatchEvent(new ContentFicheEvent(ContentFicheEvent.AJOUTER_ELT_NAV, true, FicheDetailleeEquipementImpl.NAV_SELECTED));
			try
			{
				this.dispatchEvent(new ContentFicheEvent(ContentFicheEvent.AJOUTER_ELT_NAV, true, GestionParcMobileImpl.ClassFiche.NAV_SELECTED));
			}
			catch(error:Error)
			{
				this.dispatchEvent(new Event(ContentFicheEvent.AJOUTER_ELT_NAV));
			}
		}
		
		protected function _dg_datagridChangeHandler(event:ListEvent):void
		{
			selectedItem = dg_datagrid.selectedItem;	
		}
		
		public function actualizeDatagrid():void {
			_dataDg.refresh();
		}
		
		/////////////////////////////////////////////////
		//
		//		GETTERS / SETTERS
		//
		/////////////////////////////////////////////////	
		
		public function get tabLength():int {
			if(dg_datagrid.dataProvider != null)
				_tabLength = (dg_datagrid.dataProvider as ArrayCollection).length;
			return _tabLength; 
		}
		
		public function get dataDatagrid():ArrayCollection {
			return _dataDg;
		}
		
		public function get colonnes():ArrayCollection {
			return _colonnes;
		}
		
		public function set headerLabel(value:String):void {
			lb_libelle.text = value;
		}
		
		public function set labelTypeOfEdit(value:String):void {
			colEditer.headerText = value;
		}
		
		public function set labelTypeOfDelete(value:String):void {
			colDesaffecter.headerText = value;
		}
	

		public function set labelInventaire(value:String):void {
			colInventaire.headerText = value;
		}
		
		public function set labelHistorique(value:String):void {
			colHistorique.headerText = value;
		}
	
		public function set buttonActionLabel(value:String):void {
			bt_action.label = value;
		}
		
		public function set buttonEnregistrerLabel(value:String):void {
			bt_enregistrer.label = value;
		}
		
		[Inspectable(defaultValue="true",enumeration="true,false",type="Boolean")]
		public function set buttonActionVisible(value:Boolean):void {
			bt_action.visible = value;
		}
		
		public function get buttonAction():Button
		{
			return bt_action;
		}
		
		[Inspectable(defaultValue="true",enumeration="true,false",type="Boolean")]
		public function set buttonEnregistrerVisible(value:Boolean):void {
			bt_enregistrer.visible = value;
		}
		
		public function set datagridHeight(value:Number):void {
			dg_datagrid.height = value;
		}
		
		public function set dataDatagrid(dg:ArrayCollection):void {
			dg_datagrid.dataProvider = dg;
			_dataDg = dg;
		}
		
		public function set colonnes(col:ArrayCollection):void {
			this._colonnes = col;
			addColonnes(_typeAjoutColonne);
		}
		
		[Inspectable(defaultValue="none",enumeration="none,collab,ligne,sim",type="String")]
		public function set typeAjoutColonnes(value:String):void {
			_typeAjoutColonne = value;			
		}
		
		public function set visibleTab(value:Boolean):void {
			this.visible = value;
		}

		public function set selectedItem(value:Object):void
		{
			_selectedItem = value;
		}

		public function get selectedItem():Object
		{
			return _selectedItem;
		}
		
		public function get ligneDgSelected():Object {
			return dg_datagrid.selectedItem as Object;
		}
		public function set colSupVisible(value:Boolean):void
		{
			_colSupVisible = value;
		}

		public function get colSupVisible():Boolean
		{
			return _colSupVisible;
		}
	}
}