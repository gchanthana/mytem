package vo.diffusion
{
	import mx.formatters.DateFormatter;

	public class HistoriqueEnvoi
	{
		private var _dateEnvoi:String;
		private var _periode:String;
		private var _id: int;
		private var _perimetre:String;
		private var _mailDestinataire:String;
		private var _dateCreation:String;
		private var _statut:int;
		private var _nomRapport:String;

		public function HistoriqueEnvoi()
		{
		}
		
		public function fill(historique:Object):void
		{
			this._periode=historique.PERIODE;
			this._id=historique.ID;
			this._perimetre=historique.PERIMETRE;
			this._mailDestinataire=historique.MAILDESTINATAIRE;	
			if(historique.NOMRAPPORT ==null)
			{
				this._nomRapport="";
			}
			else
			{
				this._nomRapport=historique.NOMRAPPORT;
			}
			this._dateEnvoi=dateFormat(historique.DATE_ENVOI);
			this._dateCreation=dateFormat(historique.DATE_CREATION);	
			this._statut=historique.STATUT;
		}
		private function dateFormat(pDate:Date):String
		{
			var lDf:DateFormatter=new DateFormatter();
			lDf.formatString="DD/MM/YYYY"; 				
			var lDateFormatee:String=lDf.format(pDate);
			return lDateFormatee;
		}
		
		public function get dateEnvoi():String
		{
			return _dateEnvoi;
		}
		
		public function set dateEnvoi(value:String):void
		{
			_dateEnvoi = value;
		}
		
		public function get periode():String
		{
			return _periode;
		}
		
		public function set periode(value:String):void
		{
			_periode = value;
		}
		
		public function get id():int
		{
			return _id;
		}
		
		public function set id(value:int):void
		{
			_id = value;
		}
		
		public function get perimetre():String
		{
			return _perimetre;
		}
		
		public function set perimetre(value:String):void
		{
			_perimetre = value;
		}
		
		public function get mailDestinataire():String
		{
			return _mailDestinataire;
		}
		
		public function set mailDestinataire(value:String):void
		{
			_mailDestinataire = value;
		}
		
		public function get dateCreation():String
		{
			return _dateCreation;
		}
		
		public function set dateCreation(value:String):void
		{
			_dateCreation = value;
		}
		
		public function get statut():int
		{
			return _statut;
		}
		
		public function set statut(value:int):void
		{
			_statut = value;
		}
		
		public function get nomRapport():String
		{
			return _nomRapport;
		}
		
		public function set nomRapport(value:String):void
		{
			_nomRapport = value;
		}
	}
}