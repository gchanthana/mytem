package intranet.gestionLoginClasse
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.core.Application;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.formatters.SwitchSymbolFormatter;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class PopUpFicheCompteClient extends PopUpFicheCompteClientIHM
	{
		public var objGroupeClient:Object;
		private var actualiseFunction:Function;
		private var arrayTypeClient:ArrayCollection = new ArrayCollection();
		
		public function PopUpFicheCompteClient(objGroupeClient:Object, actualise:Function)
		{
			this.objGroupeClient = objGroupeClient;
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			this.actualiseFunction = actualise;
		}
		
		private function initIHM(evt:FlexEvent):void
		{
			recupListeCodeApplication();
			cdm.idRacine = objGroupeClient.IDGROUPE_CLIENT
			if(objGroupeClient.hasOwnProperty("CODE_APPLI"))
				cdm.code_app = objGroupeClient.CODE_APPLI
			else
				cdm.code_app = 1;
			cdm.refresh()
			addEventListener(CloseEvent.CLOSE, fermer);
			cboCodeApp.addEventListener(ListEvent.CHANGE,saveCodeApp);
			if(objGroupeClient.TYPE_LOGIN == 1)
				rbg.selectedValue = rbCV3.value;
			else if(objGroupeClient.TYPE_LOGIN == 2)
				rbg.selectedValue = rbCV4.value;
			inputValiditeDeb.selectedDate = objGroupeClient.VALIDITE_DEBUT;
			inputValiditeFin.selectedDate = objGroupeClient.VALIDITE_FIN;
			intputMargeContractuel.text = (objGroupeClient.MARGE_CONTRACTUELLE != null) ? objGroupeClient.MARGE_CONTRACTUELLE : "0";
			intputNbLoginContractuel.text = (objGroupeClient.NB_LOGIN != null) ? objGroupeClient.NB_LOGIN : "0";
			intputBudgetData.text = (objGroupeClient.BUDGET_DATA != null) ? objGroupeClient.BUDGET_DATA : "0";
			intputBudgetFixe.text = (objGroupeClient.BUDGET_FIXE != null) ? objGroupeClient.BUDGET_FIXE : "0";
			intputBudgetMobile.text = (objGroupeClient.BUDGET_MOBILE != null) ? objGroupeClient.BUDGET_MOBILE : "0";
			intputNbLignesDatas.text = (objGroupeClient.NBLIGNE_DATA != null) ? objGroupeClient.NBLIGNE_DATA : "0";
			intputNbLignesFixes.text = (objGroupeClient.NBLIGNE_FIXE != null) ? objGroupeClient.NBLIGNE_FIXE : "0";
			intputNbLignesMobiles.text = (objGroupeClient.NBLIGNE_MOBILE != null) ? objGroupeClient.NBLIGNE_MOBILE : "0";
			intputNombreSiteFixe.text = (objGroupeClient.NBSITE_FIXE != null) ? objGroupeClient.NBSITE_FIXE : "0";
			intputNombreSiteMobiles.text = (objGroupeClient.NBSITE_MOBILE != null) ? objGroupeClient.NBSITE_MOBILE : "0";
			intputNombreSiteDatas.text = (objGroupeClient.NBSITE_DATA != null) ? objGroupeClient.NBSITE_DATA : "0";
			rbgData.selectedValue = (objGroupeClient.MODE_TARIF_DATA != null) ? objGroupeClient.MODE_TARIF_DATA : "L";
			rbgFixe.selectedValue = (objGroupeClient.MODE_TARIF_FIXE != null) ? objGroupeClient.MODE_TARIF_FIXE : "L";
			rbgMobile.selectedValue = (objGroupeClient.MODE_TARIF_MOBILE != null) ? objGroupeClient.MODE_TARIF_MOBILE : "L";
			if (inputValiditeDeb.selectedDate != null)
			{
				inputValiditeDebChangeHandler(null);
			}
			inputValiditeDeb.addEventListener(CalendarLayoutChangeEvent.CHANGE, inputValiditeDebChangeHandler);
			radioButtonsModuleBase.selectedValue = objGroupeClient.MODULE_FACTURATION;
			radioButtonsCycleDeVie.selectedValue = objGroupeClient.MODULE_WORKFLOW;
			radioButtonsUsages.selectedValue = objGroupeClient.MODULE_USAGE;
			radioButtonsGestionOrgas.selectedValue = objGroupeClient.MODULE_GESTION_ORG;
			radioButtonFixData.selectedValue = objGroupeClient.MODULE_FIXE_DATA;
			radioButtonsMobile.selectedValue = objGroupeClient.MODULE_MOBILE;
			radioButtonsLogin.selectedValue = objGroupeClient.MODULE_GESTION_LOGIN;
			if (objGroupeClient.DROIT_GESTION_FOURNIS == "1")
			{
				checkAdmin.selected = true;
			}
			else
			{
				checkAdmin.selected = false;
			}
			addEventListener(CloseEvent.CLOSE, fermer);
			btEnregistrerFicheClient.addEventListener(MouseEvent.CLICK, enregistrer);
		}
		
		private function recupTypeClient():void
		{
			var opUpdateDgLogin:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getTypeClient", processGetTypeClient, null);
			RemoteObjectUtil.callService(opUpdateDgLogin, objGroupeClient.IDGROUPE_CLIENT)
		}
		private function saveCodeApp(e:ListEvent):void
		{
			//			var opSaveCodeApp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "setCodeApplication", processSetCodeAppHandler, null);
			//			RemoteObjectUtil.callService(opSaveCodeApp, cboCodeApp.selectedItem.CODE_APP, objGroupeClient.IDGROUPE_CLIENT)
			cdm.code_app = (cboCodeApp.selectedItem)?cboCodeApp.selectedItem.CODE_APP : -1;
			cdm.refresh();
		}
		private function enregistrerCodeApp(e:ListEvent):void
		{
			var opSaveCodeApp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "setCodeApplication", processSetCodeAppHandler, null);
			RemoteObjectUtil.callService(opSaveCodeApp, cboCodeApp.selectedItem.CODE_APP, objGroupeClient.IDGROUPE_CLIENT)
		}
		private function processSetCodeAppHandler(e:ResultEvent):void
		{
			objGroupeClient.CODE_APPLI = cboCodeApp.selectedItem.CODE_APP;
		}
		private function processGetTypeClient(evt:ResultEvent):void
		{
			var checkbox:CheckBox;
			if (evt.result != -1)
			{
				trace("---------------->" + ObjectUtil.toString(evt.result));
				arrayTypeClient = (evt.result as ArrayCollection);
				trace("tab + " + arrayTypeClient.toString());
				for (var i:int = 0; i < arrayTypeClient.length; i++)
				{
					switch (arrayTypeClient.getItemAt(i).IDTYPE_CLIENT)
					{
						case "1":
							checkbox = checkConseil;
							break;
						case "2":
							checkbox = checkDemoEDI;
							break;
						case "3":
							checkbox = checkDemoCV;
							break;
						case "4":
							checkbox = checkCV;
							break;
						case "5":
							checkbox = checkTellcost;
							break;
						default:
							break;
					}
					if (arrayTypeClient.getItemAt(i).IS_SELECTED == 1)
					{
						checkbox.selected = true;
					}
					else
					{
						checkbox.selected = false;
					}
				}
			}
			else
			{
				Alert.show("Erreur getTypeClient : PopUpFicheCompteClients.as");
			}
		}
		private function recupListeCodeApplication():void
		{
			var op1:AbstractOperation =	RemoteObjectUtil.getOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.GestionClient",
				"getListeCodeApp",
				getListeCodeAppHandler,error);
			RemoteObjectUtil.callService(op1);
		}
		private function error(e:FaultEvent):void
		{
			Alert.show("Aucune application n'a été chargé","Erreur");
		}
		private function getListeCodeAppHandler(e:ResultEvent):void
		{
			if(e.result != null && e.result.length > 0)
				cboCodeApp.dataProvider = e.result as ArrayCollection
			for each(var obj:Object in cboCodeApp.dataProvider as ArrayCollection)
			{
				if(obj.CODE_APP == objGroupeClient.CODE_APPLI)
				{
					cboCodeApp.selectedItem = obj;
					break;
				}
			}
			recupTypeClient();
		}
		private function fermer(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function inputValiditeDebChangeHandler(clce:CalendarLayoutChangeEvent):void
		{
			var selectableRange:Object = new Object();
			selectableRange.rangeStart = inputValiditeDeb.selectedDate;
			inputValiditeFin.selectableRange = selectableRange;
		}
		
		private function enregistrer(evt:MouseEvent):void
		{
			var boolOk:Boolean = true;
			if(cboCodeApp.selectedIndex > -1)				
			{
				boolOk = true
				cboCodeApp.errorString = "";
			}	
			else
			{
				boolOk = false;
				cboCodeApp.errorString = "Veuillez choisir une application";
			}	
			if (inputValiditeDeb.selectedDate == null)
			{
				boolOk = false;
				inputValiditeDeb.setStyle("borderColor", "red");
			}
			else
			{
				inputValiditeDeb.clearStyle("borderColor");
			}
			/*
			if (inputValiditeFin.selectedDate == null){
			boolOk = false;
			inputValiditeFin.setStyle("borderColor","red");
			}else{
			inputValiditeFin.clearStyle("borderColor");
			} */
			if (isNaN(Number(intputNbLoginContractuel.text)) || intputNbLoginContractuel.text == "")
			{
				boolOk = false;
				intputNbLoginContractuel.setStyle("borderColor", "red");
			}
			else
			{
				intputNbLoginContractuel.clearStyle("borderColor");
			}
			if (isNaN(Number(intputMargeContractuel.text)) || intputMargeContractuel.text == "")
			{
				boolOk = false;
				intputMargeContractuel.setStyle("borderColor", "red");
			}
			else
			{
				intputMargeContractuel.clearStyle("borderColor");
			}
			//--Fixe
			if (rbgFixe.selectedValue == "L")
			{
				if (isNaN(Number(intputNbLignesFixes.text)))
				{
					boolOk = false;
					intputNbLignesFixes.setStyle("borderColor", "red");
				}
				else
				{
					intputNbLignesFixes.clearStyle("borderColor");
					intputNombreSiteFixe.clearStyle("borderColor");
				}
			}
			else
			{
				if (isNaN(Number(intputNombreSiteFixe.text)))
				{
					boolOk = false;
					intputNombreSiteFixe.setStyle("borderColor", "red");
				}
				else
				{
					intputNbLignesFixes.clearStyle("borderColor");
					intputNombreSiteFixe.clearStyle("borderColor");
				}
			}
			if (isNaN(Number(intputBudgetFixe.text)))
			{
				boolOk = false;
				intputBudgetFixe.setStyle("borderColor", "red");
			}
			else
			{
				intputBudgetFixe.clearStyle("borderColor");
			}
			//--
			//--Data
			if (rbgData.selectedValue == "L")
			{
				if (isNaN(Number(intputNbLignesDatas.text)))
				{
					boolOk = false;
					intputNbLignesDatas.setStyle("borderColor", "red");
				}
				else
				{
					intputNbLignesDatas.clearStyle("borderColor");
					intputNombreSiteDatas.clearStyle("borderColor");
				}
			}
			else
			{
				if (isNaN(Number(intputNombreSiteDatas.text)))
				{
					boolOk = false;
					intputNombreSiteDatas.setStyle("borderColor", "red");
				}
				else
				{
					intputNbLignesDatas.clearStyle("borderColor");
					intputNombreSiteDatas.clearStyle("borderColor");
				}
			}
			if (isNaN(Number(intputBudgetData.text)))
			{
				boolOk = false;
				intputBudgetData.setStyle("borderColor", "red");
			}
			else
			{
				intputBudgetData.clearStyle("borderColor");
			}
			//--
			//--Mobile
			if (rbgData.selectedValue == "L")
			{
				if (isNaN(Number(intputNbLignesMobiles.text)))
				{
					boolOk = false;
					intputNbLignesMobiles.setStyle("borderColor", "red");
				}
				else
				{
					intputNbLignesMobiles.clearStyle("borderColor");
					intputNombreSiteMobiles.clearStyle("borderColor");
				}
			}
			else
			{
				if (isNaN(Number(intputNombreSiteMobiles.text)))
				{
					boolOk = false;
					intputNombreSiteMobiles.setStyle("borderColor", "red");
				}
				else
				{
					intputNbLignesMobiles.clearStyle("borderColor");
					intputNombreSiteMobiles.clearStyle("borderColor");
				}
			}
			if (isNaN(Number(intputBudgetMobile.text)))
			{
				boolOk = false;
				intputBudgetMobile.setStyle("borderColor", "red");
			}
			else
			{
				intputBudgetMobile.clearStyle("borderColor");
			}
			//---
			if (boolOk)
			{
				trace("Execution de requête");
				Alert.show("Etes vous sur(s) de vouloir enregistrer ?","validation",Alert.YES | Alert.NO,Application.application as Sprite,closeValidation);
			}
			else
			{
				Alert.show("Les champs en rouge sont erronés");
			}
		}
		private function closeValidation(evt:CloseEvent):void
		{
			if(evt.detail == Alert.YES)
			{
				var valeurDroitFournisseur:int;
				if (checkAdmin.selected == true)
				{
					valeurDroitFournisseur = 1;
				}
				else
				{
					valeurDroitFournisseur = 0;
				}
				var typelogin:int = 0;
				if(rbg.selectedValue == rbCV4.value)
					typelogin=2;
				else if(rbg.selectedValue == rbCV3.value)
					typelogin=1;
				enregistrerCodeApp(null);
				cdm.sauvegarderDroit();
				var opUpdateDgLogin:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "UPDATE_RACINE_INFO", processTypeClient, null);
				RemoteObjectUtil.callService(opUpdateDgLogin, objGroupeClient.IDGROUPE_CLIENT, inputValiditeDeb.selectedDate, (inputValiditeFin.selectedDate != null) ? inputValiditeFin.selectedDate : new Date(1970, 0, 1), (intputNbLoginContractuel.text != "") ? Number(intputNbLoginContractuel.text) : 0, (intputMargeContractuel.text != "") ? Number(intputMargeContractuel.text) : 0, (rbgFixe.selectedValue != null) ? rbgFixe.selectedValue : "L", (rbgMobile.selectedValue != null) ? rbgMobile.selectedValue : "L", (rbgData.selectedValue != null) ? rbgData.selectedValue : "L", (intputNbLignesFixes.text != "" && rbgFixe.selectedValue == "L") ? Number(intputNbLignesFixes.text) : 0, (intputNbLignesMobiles.text != "" && rbgMobile.selectedValue == "L") ? Number(intputNbLignesMobiles.text) : 0, (intputNbLignesDatas.text != "" && rbgData.selectedValue == "L") ? Number(intputNbLignesDatas.text) : 0, (intputNombreSiteFixe.text != "" && rbgFixe.selectedValue == "S") ? Number(intputNombreSiteFixe.text) : 0, (intputNombreSiteMobiles.text != "" && rbgMobile.selectedValue == "S") ? Number(intputNombreSiteMobiles.text) : 0, (intputNombreSiteDatas.text != "" && rbgData.selectedValue == "S") ? Number(intputNombreSiteDatas.text) : 0, (intputBudgetFixe.text != "") ? Number(intputBudgetFixe.text) : 0, (intputBudgetMobile.text != "") ? Number(intputBudgetMobile.text) : 0, (intputBudgetData.text != "") ? Number(intputBudgetData.text) : 0, radioButtonsModuleBase.selectedValue as int, radioButtonsUsages.selectedValue as int, radioButtonsCycleDeVie.selectedValue as int, radioButtonFixData.selectedValue as int, radioButtonsMobile.selectedValue as int, radioButtonsGestionOrgas.selectedValue as int, radioButtonsLogin.selectedValue as int, valeurDroitFournisseur,typelogin);
			}
		}
		private function processTypeClient(evt:ResultEvent):void
		{
			var arrayNewType:ArrayCollection = new ArrayCollection();
			var checkbox:CheckBox;
			if (evt.result != -1)
			{
				for (var i:int = 0; i < arrayTypeClient.length; i++)
				{
					switch (arrayTypeClient.getItemAt(i).IDTYPE_CLIENT)
					{
						case "1":
							checkbox = checkConseil;
							break;
						case "2":
							checkbox = checkDemoEDI;
							break;
						case "3":
							checkbox = checkDemoCV;
							break;
						case "4":
							checkbox = checkCV;
							break;
						case "5":
							checkbox = checkTellcost;
							break;
						default:
							break;
					}
					if (checkbox.selected == true)
					{
						arrayNewType.addItem(arrayTypeClient.getItemAt(i).IDTYPE_CLIENT);
					}
				}
				var opUpdateDgLogin:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "update_typeClient", processSaveInfos, null);                
				RemoteObjectUtil.callService(opUpdateDgLogin, objGroupeClient.IDGROUPE_CLIENT, arrayNewType.source);
			}
			else
			{
				Alert.show("Erreur UPDATE_RACINE_INFO");
			}
		}
		
		private function processSaveInfos(evt:ResultEvent):void
		{
			trace(ObjectUtil.toString(evt.result));
			if (evt.result != -1)
			{
				actualiseFunction();
				Alert.show("Modifications enregistrées");
				PopUpManager.removePopUp(this);
			}
			else
			{
				Alert.show("Erreur UPDATE_TYPE_CLIENT_V1 : " + evt.toString());
			}
		}
	}
}