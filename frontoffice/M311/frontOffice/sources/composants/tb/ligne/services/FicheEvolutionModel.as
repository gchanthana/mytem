package composants.tb.ligne.services
{
	import composants.tb.ligne.entity.FicheEvolutionVO;
	import composants.tb.ligne.event.FicheEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class FicheEvolutionModel extends EventDispatcher
	{
		
		private var _ficheEvolution:ArrayCollection;
		
		public function FicheEvolutionModel()
		{
			_ficheEvolution = new ArrayCollection();
		}

		public function get ficheEvolution():ArrayCollection
		{
			return _ficheEvolution;
		}

		public function set ficheEvolution(value:ArrayCollection):void
		{
			_ficheEvolution = value;
		}
		
		internal function updateFicheEvolution(value:ArrayCollection):void
		{		
			
			var temp:FicheEvolutionVO;
			for (var i:int=0; i < value.length; i++)
			{
				temp = new FicheEvolutionVO();
				temp.fill(value[i]);
				_ficheEvolution.addItem(temp);
			}
			
			dispatchEvent(new FicheEvent(FicheEvent.FICHEEVOLUTION));
		}

	}
}