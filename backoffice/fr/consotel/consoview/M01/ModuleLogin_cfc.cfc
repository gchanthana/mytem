﻿<cfcomponent>

	<!--- 
		Procédure d'envoi du mot de passe utilisateur via email.
		Vérifie l'existence de l'utilisateur dans la base
	--->
	
	<cffunction name="getUserPassword" access="remote" returntype="Any"  >		
		<cfargument name="email" 			required="true" type="string" />
		<cfargument name="codeLang" 		required="true" type="string" />
		<cfargument name="codeApplication" 	required="true" type="Numeric" />
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.getuserpasword">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#email#" >			
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		


			<!--- Récupération de l'objet utilisateur --->
			<cfset objUser = [p_retour.LOGIN_NOM,p_retour.LOGIN_PRENOM,p_retour.LOGIN_EMAIL,p_retour.PSW]>
		
			<cfif  #objUser[4]# eq -2 >
			<!---- aucun utilisateur trouvé pour cet email   ---->
			 	<cfset sendMailToConsotel(email,codeApplication)>
			 	<cfreturn -1 />	 
			  <cfelse> 
			<!---- l'utilisateur est référencé : envoi des identifiants en 2 mails ( login puis password )
			       récupération d'un tableau des paramètres utilisateur
			        p_retour = [ nom ,prénom ,mail, password ]		
			  ---->	
			  	<cfset getInfosUser(p_retour.LOGIN_EMAIL, p_retour.PSW, objUser, codeLang, codeApplication)> 	   
			 	 	
			 	<cfreturn p_retour/>			 	
			</cfif>

			<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getInfosUser" access="public" returntype="Any">		
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="pwd" 				required="true" type="string"/>
		<cfargument name="objUser"  		required="true" type="array"/>
		<cfargument name="codeLang" 		required="true" type="string" />
		<cfargument name="codeApplication" 	required="true" type="Numeric" />
		
			<cfset rsltID = -4>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_GLOBAL.CONNECTCLIENT">
				<cfprocparam value="#email#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam value="#pwd#" 	 cfsqltype="CF_SQL_VARCHAR">
				<cfprocresult name="p_retour">
			</cfstoredproc>

			<cfif p_retour.recordcount gt 0>
				<cfset auhentification = 1>
			<cfelse>
				<cfset auhentification = 0>
			</cfif>
			
			<cfif auhentification GT 0>
				<cfset iduser 	= p_retour['CLIENTACCESSID'][1]>
				<cfset lang 	= codeLang>
				<cfset ID 		= p_retour['ID'][1]>
				<cfset CODEAPP	= codeApplication>
				<cfset rsltID 	= sendIdentifier(objUser, iduser, lang, ID, CODEAPP)>
			<cfelse>
				<cfset rsltID = -4> 
			</cfif>
		<cfreturn rsltID>	
	</cffunction>

	<!--- 
		Envoie de mail au support consotel pour indiquer qu'une demande d'envoi de
		mot de passe a été effectué pour une adresse mail non référencée dans la base client'
	 --->
	 
	<cffunction name="sendMailToConsotel" access="public" returntype="numeric" output="true">

	    <cfargument name="email" required="true" type="String"  />
		<cfargument name="codeApplication" 	required="true" type="Numeric" />
		<cfargument name="destinataire" required="false" type="String" default="Support@consotel.fr" />				
		<cfset var p_retour = -1>
		<cfset currentDate=now()>	
		<cfset body="Bonjour,<br/><br/>L'utilisateur #email# a fait une demande de mot de passe
		 à #TimeFormat(now(), "hh:mm")# le  #DateFormat(now(), "dd/mm/yyyy")# .<br/>
		 Cependant celui-ci nest pas présent dans notre base d'utilisateurs actifs référencés.
		 Merci de prendre contact avec cette personne si nécessaire.<br/> Cordialement le Service IT." >	
		<cfset str1 = 'SIZE="10"'>
		<cfset str2 = 'SIZE="2"'>		
		<cfset message1 = Replace(body,str1,str2,"all")>
			
		<cfset var properties = createObject("component","fr.consotel.consoview.M01.SendMail").getProperties(codeApplication,'NULL')>
			
		<cfset destinataire = properties['MAIL_DEST_N1']>
		<cfset expediteur = properties['MAIL_EXP_N1']>

		<cfset str1 = "&apos;">
		<cfset str2 = "'">
		<cfset message = Replace(message1,str1,str2,"all")>
		<cftry>		
			<!--- envoie du mail --->
			
			<cfif NOT isDefined('properties.MAIL_SERVER')>
				<cfset properties.MAIL_SERVER = 'mail-cv.consotel.fr'>
			</cfif>
			
			<cfif NOT isDefined('properties.MAIL_PORT')>
				<cfset properties.MAIL_PORT = 25>
			</cfif>
			
			
			<cfmail from="#expediteur#" to="#destinataire#" subject="Echec de demande de récupération de mot de passe" 
					type="text/html" server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#" charset="utf-8">				
				<html lang='fr'>
				<head>
				<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
				<body>									
					#message#
				</body>
				</html>
			</cfmail>					
			<cfset p_retour = 1>
		<cfcatch>
			<cfset p_retour = -1>
		</cfcatch>
		</cftry>
		<cfreturn p_retour>
	</cffunction>

	<!--- 
		Envoie par mail des identifiants de connexion d'un utilisateur en 2 étapes'
	 --->
	 
	<cffunction  name="sendIdentifier" output="false" description="Envoie des identifiants utilisateur par mail" access="remote" returntype="numeric">
		<cfargument name="tableau" 			required="true" type="array"/>
		<cfargument name="iduser"  			required="true" type="numeric"/>
		<cfargument name="lang"    			required="true" type="string"/>
		<cfargument name="idEntr" 			required="true" type="numeric"/>
		<cfargument name="codeApplication"  required="true" type="numeric"/>
			

			<cfset var properties = createObject("component","fr.consotel.consoview.M01.SendMail").getProperties(codeApplication,'NULL')>
			
			<cfset var mailing  = createObject("component","fr.consotel.consoview.M01.Mailing")>				
			
			<cfset var M01_UUID = ''>
			<cfset var userTab  = tableau[3]>
			
			<cfset var myVar = false>
			
			<cfset var ImportTmp="/container/M01/">
			
			<cfset var rsltApi  = -4>
			<cfset var resltRecordMail = 0>
			
			<cfset var fromMailing = properties['MAIL_EXP_N1']>

			<cfloop condition="myVar eq false">
			  <cfset M01_UUID = createUUID()>
			  <cfif NOT DirectoryExists('#ImportTmp##M01_UUID#')>
				<cfdirectory action="Create" directory="#ImportTmp##M01_UUID#" type="dir" mode="777">
				<cfset myVar="true">
				<cfdump var="#myVar#">
			  </cfif>
			</cfloop>


		<!--- COMMUN A TOUS --->







			<!--- BIP REPORT --->
			<cfset parameters["bipReport"] = structNew()>
			<cfset parameters["bipReport"]["xdoAbsolutePath"]	= "/consoview/LOGIN/LOGIN.xdo">
			<cfset parameters["bipReport"]["outputFormat"] 		= "html">
			<cfset parameters["bipReport"]["localization"]		= lang>
			
			<!--- DONNEES ENVOYEES A BIP --->
			<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"]["parameterValues"] = [tableau[3]]>
			
			<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
			<cfset parameters.bipReport.delivery.parameters.ftpUser="container">
			<cfset parameters.bipReport.delivery.parameters.ftpPassword="container">
								
			<cfset parameters.EVENT_TARGET = M01_UUID>
			<cfset parameters.EVENT_HANDLER = "M01">
	
		<!--- COMMUN A TOUS --->	
		
		
		<!--- PASSWORD --->
		
		<!--- BIP REPORT --->
		<cfset parameters["bipReport"]["xdoTemplateId"]		= "SEND-LOGIN-1">
		<cfset parameters.bipReport.delivery.parameters.fileRelativePath="M01/#M01_UUID#/password_#M01_UUID#.html">
		
		<cfset parameters.EVENT_TYPE = "P">
		
		<!--- ENREGISTREMENT DU REPERTOIRE DE DESTINATION --->
		<cfset resltRecordMail = mailing.setMailToDatabase(	#iduser#,
															#userTab#,
															#M01_UUID#,
															"M01",
															#parameters.EVENT_TYPE#,
															#fromMailing#,
															#userTab#,
															"Codes d'accès à la solution TEM Consoview - 1/2",

															'',
															'',
															#codeApplication#)>
		
		<cfif resltRecordMail GT 0>
			<!--- EXÉCUTION DU SERVICE POUR LE LOGIN --->
			<cfset api= createObject("component","fr.consotel.consoview.api.CV")>
			<cfset rsltApi = api.invokeService("IbIs","scheduleReport",parameters)>
		<cfelse>
			<cfreturn -10>
		</cfif>
			
			
		<!--- LOGIN --->
		<cfset parameters["bipReport"]["xdoTemplateId"]		= "SEND-PWD-1">
		<cfset parameters.bipReport.delivery.parameters.fileRelativePath="M01/#M01_UUID#/login_#M01_UUID#.html">
		
		<cfset parameters.EVENT_TYPE = "L">
		
		<!--- ENREGISTREMENT DU REPERTOIRE DE DESTINATION --->
		<cfset resltRecordMail = mailing.setMailToDatabase(	#iduser#,
															#userTab#,
															#M01_UUID#,
															"M01",
															#parameters.EVENT_TYPE#,
															#fromMailing#,
															#userTab#,
															"Codes d'accès à la solution TEM Consoview - 2/2",

															'',
															'',
															#codeApplication#)>
		
		<cfif resltRecordMail GT 0>
			<!--- EXÉCUTION DU SERVICE POUR LE LOGIN --->
			<cfset api= createObject("component","fr.consotel.consoview.api.CV")>
			<cfset rsltApi = api.invokeService("IbIs","scheduleReport",parameters)>
		<cfelse>
			<cfreturn -10>
		</cfif>
		<!--- LOGIN --->
		<cfreturn rsltApi>
	</cffunction>		
								
</cfcomponent>

