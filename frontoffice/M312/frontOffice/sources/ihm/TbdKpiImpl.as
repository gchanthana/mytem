package ihm
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.containers.HDividedBox;
	
	import event.HistoriqueServiceEvent;
	import event.InventaireEquipementIhmEvent;
	import event.InventaireServiceEvent;
	
	import service.HistoriqueService;
	import service.InventaireService;
	
	[Bindable]
	public class TbdKpiImpl extends HDividedBox
	{
		private var _selectedModele:String;
		private var _selectedMarque:String;		
		
		//Composant
		public var kpiInventaire:InventaireEquipementIHM;
		public var kpiHistoriqueDepense:HistoriqueDepenseIHM;
		
		private var t:Timer;
		private var _typeperimetre:String = "";
	
		
		
		public var isrv:InventaireService = new InventaireService();
		public var hsrv:HistoriqueService = new HistoriqueService();
	
		
		public function TbdKpiImpl()
		{
			super();
		}
		
		
		/**
		 * Initialisation des KPI
		 * */
		public function initInventaire(typeperimetre:String):void
		{
			_typeperimetre = typeperimetre;		
			initRequest(_typeperimetre);
		}
		
		
		//On va chercher les données
		private function initRequest(typeperimetre:String):void
		{	
			//On lance les requêtes avec un interval de 300ms pour éviter le 'Bundling' des requêtes
			t=new Timer(300);
			t.addEventListener(TimerEvent.TIMER,function handler(event:TimerEvent):void 
			{					
				if(t.currentCount == 2)
				{
					if(kpiInventaire != null)
					{
						kpiInventaire.initTabs();
					}
					isrv.getMarqueEquipement(typeperimetre);
					boolShowInventaireSpinner = true;
					isrv.model.addEventListener(InventaireServiceEvent.INVENTAIRE_SERVICE_MODEL_LMARQUE_UPDATED, _modelInventaireServiceModelLmarqueUpdatedHandler);
					t.stop();
					t.removeEventListener(TimerEvent.TIMER,handler);	
				}
				else
				{
					hsrv.getEvolutionSurThemeConso(typeperimetre);
					boolShowHistoriqueSpinner = true;
					hsrv.model.addEventListener(HistoriqueServiceEvent.HISTORIQUE_SERVICE_MODEL_HISTORIQUE_UPDATED, _modelHistoriqueServiceModelHistoriqueUpdatedHandler);
				}
				
			});
			t.start();
		}
		
		
		
		//Création des series du graph des évolutions
		
		
		
		//// GESTION KPI Inventaire PARC
		
		/**
		 * quand on sélectionne une marque dans le tableau 
		**/		
		protected function marqueChangeHandler(event:InventaireEquipementIhmEvent):void
		{
			isrv.getModeleEquepement(kpiInventaire.selectedMarque,_typeperimetre);
			boolShowInventaireSpinner = true;
			isrv.model.addEventListener(InventaireServiceEvent.INVENTAIRE_SERVICE_MODEL_LMODELE_UPDATED, _modelInventaireServiceModelLmodeleUpdatedHandler);
		}
		
		/**
		 * quand on sélectionne un modèle 
		 * */
		protected function modeleChangeHandler(event:InventaireEquipementIhmEvent):void
		{
			isrv.getListEquepement(kpiInventaire.selectedMarque,kpiInventaire.selectedModele,_typeperimetre);
			boolShowInventaireSpinner = true;
			isrv.model.addEventListener(InventaireServiceEvent.INVENTAIRE_SERVICE_MODEL_LEQPT_UPDATED, _modelInventaireServiceModelLeqptUpdatedHandler);
		}
		
		public function set selectedModele(value:String):void
		{
			_selectedModele = value;
		}

		public function get selectedModele():String
		{
			return _selectedModele;
		}
		public function set selectedMarque(value:String):void
		{
			_selectedMarque = value;
		}

		public function get selectedMarque():String
		{
			return _selectedMarque;
		}
		
		public var boolShowInventaireSpinner:Boolean = false;
		public var boolShowHistoriqueSpinner:Boolean = false;

		protected function _modelInventaireServiceModelLmodeleUpdatedHandler(event:InventaireServiceEvent):void
		{
			boolShowInventaireSpinner = false	
		}		

		protected function _modelInventaireServiceModelLeqptUpdatedHandler(event:InventaireServiceEvent):void
		{
			boolShowInventaireSpinner = false
		}

		protected function _modelInventaireServiceModelLmarqueUpdatedHandler(event:InventaireServiceEvent):void
		{
			boolShowInventaireSpinner = false;
		}
		
		protected function kpiInventaire_navChangeEventHandler(event:InventaireEquipementIhmEvent):void
		{
			boolShowInventaireSpinner = false;
			isrv.cancelOperation();
		}
				

		protected function _modelHistoriqueServiceModelHistoriqueUpdatedHandler(event:HistoriqueServiceEvent):void
		{
			boolShowHistoriqueSpinner = false;
		}
	}
}