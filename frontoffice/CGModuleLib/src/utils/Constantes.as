package utils
{
	import mx.collections.ArrayCollection;

	public class Constantes
	{
		[Bindable] public static var listeApplication		:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeNiveau			:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeAllNiveau			:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeUnivers			:ArrayCollection = new ArrayCollection();
		
		[Bindable] public static var listeLangues			:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeCertifications	:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeCivilites			:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeTypesPlanTarifaire:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeSujetsFormation	:ArrayCollection = new ArrayCollection();
		[Bindable] public static var listeLieuxFormation	:ArrayCollection = new ArrayCollection();
		
		
		public function Constantes()
		{
		}
	}
}