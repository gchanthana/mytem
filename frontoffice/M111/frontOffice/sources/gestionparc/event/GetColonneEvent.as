package gestionparc.event
{
	import flash.events.Event;

	public class GetColonneEvent extends Event
	{
		public static const GET_ALL_COLONNES:String="getAllColonnes";

		public function GetColonneEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new GetColonneEvent(type, bubbles, cancelable);
		}
	}
}
