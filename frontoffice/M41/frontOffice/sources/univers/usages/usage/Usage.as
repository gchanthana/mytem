package univers.usages.usage
{
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.UniversFunctionItem;
	
	
	public class Usage extends UsageIHM implements UniversFunctionItem {
		private var opData:AbstractOperation = new AbstractOperation();
		private var arrCol1:ArrayCollection  = new ArrayCollection();
		private var arrCol2:ArrayCollection  = new ArrayCollection();
		private var arrCol3:ArrayCollection  = new ArrayCollection();
		
		[Bindable]
		internal var _moisDeb : String ;//le debut de la periode 
		[Bindable]
		internal var _moisDebDate : Date ;//le debut de la periode 
		[Bindable]
		internal var _moisFin : String ; //la fin de la periode
		[Bindable]
		internal var _moisFinDate : Date ; //la fin de la periode

		public function Usage()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		public function initIHM(e:Event):void {			
			myListe.nodeSearchItem.addEventListener(MouseEvent.CLICK,getLignes);
			myListe.searchInput.addEventListener(KeyboardEvent.KEY_DOWN,processKey);
			myListe.btnDetails.addEventListener(MouseEvent.CLICK,getTypeAppel);
			myListe.btnTickets.addEventListener(MouseEvent.CLICK,getCDR);
			myListe.txtLigneFilter.addEventListener(KeyboardEvent.KEY_UP,filtergrdLignes);
			myListe.grdLigne.addEventListener(MouseEvent.CLICK,clearGrid);
			myListe.grdDetails.addEventListener(MouseEvent.CLICK,cleargrdTicket);
			myListe.imgCSV.addEventListener(MouseEvent.CLICK,imgCSVClickHandler);
			myListe.listNdiCsv.addEventListener(MouseEvent.CLICK,exportContent);
			myListe.destCsv.addEventListener(MouseEvent.CLICK,exportContent);
			// gestion des dates
			var today:Date=new Date();
			myListe.mySelector.addEventListener("periodeChange",updatePeriode);
			initDate();
			setSelectedPeriode(_moisDeb,_moisFin);
		}

		private function processKey(event:KeyboardEvent):void {
	      	if((event.keyCode == 13)) { // 38 et 40
		    	getLignes(new MouseEvent(""));
		    }
		}

		private function updatePeriode(pe : PeriodeEvent):void{
			_moisDeb = pe.moisDeb;
			_moisFin = pe.moisFin;
			
			_moisDebDate = new Date(_moisDeb.substr(6,4),parseInt(_moisDeb.substr(3,2))-1,_moisDeb.substr(0,2));
			_moisFinDate = new Date(_moisDeb.substr(6,4),_moisDeb.substr(3,2),parseInt(_moisDeb.substr(0,2))-1);
					
			setSelectedPeriode(_moisDeb,_moisFin);
			///	getLignes(new MouseEvent(""));
		}

		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
															
			var formMoisD : String = DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
									+ moisDebut.substr(6,4);
									
			var formMoisF : String = DateFunction.moisEnLettre(parseInt(moisFin.substr(3,2),10)-1) 
								   	+ " " 
								   	+ moisFin.substr(6,4);
								   	
			///////////////// faire la suite 
			myListe.myPeriodeLbl.text = formMoisD;
		}

		private function initDate():void{
			
			var periodeArray : Array = myListe.mySelector.getTabPeriode();
		    var len : int = periodeArray.length;
		    var month : AMonth;
		    
			month = periodeArray[len-1];
			
			_moisDeb = month.getDateDebut();	
			_moisFin = month.getDateFin();
						
			_moisDebDate = month.dateDebut;
			_moisFinDate = month.dateFin;
			
			//_moisDebDate = new Date(_moisDeb.substr(6,4),_moisDeb.substr(3,2),_moisDeb.substr(0,2));
			//_moisFinDate = new Date(_moisFin.substr(6,4),_moisFin.substr(3,2),_moisFin.substr(0,2));			
		}

		public function getLignes(e:MouseEvent):void {
			getSynthese();
			clearGrid(e);		
		}	

		public function getTypeAppel(e:Event):void {
			var idracine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var datedeb:String= DateFunction.formatDateAsInverseString(_moisDebDate);
			var datefin:String= DateFunction.formatDateAsInverseString(_moisFinDate);
			if (myListe.grdLigne.selectedIndices.length > 0) {
				opData = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.usages.CallUsage",
						"getTypeAppel", processgetTypeAppel);
				RemoteObjectUtil.callService(opData,idracine,
											datedeb,
											myListe.grdLigne.selectedItems[0].APPELANT,
											myListe.grdLigne.selectedItems[0].SDA);
			}
		}
		
		public function processgetTypeAppel(r:ResultEvent):void {
			if (r.result.length==0) {
				Alert.show("Pas de données");
			}
			arrCol2 = ArrayCollection(r.result);          
            arrCol2.refresh();
            myListe.grdDetails.dataProvider = arrCol2;
		}				

		public function getCDR(e:Event):void {
			var idracine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var datedeb:String= DateFunction.formatDateAsInverseString(_moisDebDate);
			var datefin:String= DateFunction.formatDateAsInverseString(_moisFinDate);
			if (myListe.grdLigne.selectedIndices.length > 0 && myListe.grdDetails.selectedIndices.length > 0) {
				opData = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.usages.CallUsage",
						"getCDRbyType", processgetCDR);
						
				RemoteObjectUtil.callService(opData,idracine,datedeb,myListe.grdLigne.selectedItems[0].APPELANT,myListe.grdLigne.selectedItems[0].SDA,myListe.grdDetails.selectedItems[0].TYPE_APPEL);
			}
		}

		public function processgetCDR(r:ResultEvent):void {
			if (r.result.length==0) {
				Alert.show("Pas de données");
			}
			arrCol3 = ArrayCollection(r.result);          
            arrCol3.refresh();
            myListe.grdTicket.dataProvider = arrCol3;
		}	     
        
        private function filtergrdLignes(e:Event):void{
                clearGrid(e);               
                if (myListe.txtLigneFilter.text != "") {
                    arrCol1.filterFunction = filterLignesFunc;
                } else {
                    arrCol1.filterFunction = null;
                }                
                arrCol1.refresh();	
                myListe.grdLigne.dataProvider = arrCol1;        	
        }        
        
        private function filterLignesFunc(item:Object):Boolean {
        	var rfilter:Boolean = true;
        	var valueAppelant : String = (item.APPELANT != null)?item.APPELANT:"";
    		var valueSDA : String = (item.SDA != null)?item.SDA:"";
    		
    		rfilter = rfilter && ((valueAppelant.toLowerCase().search(myListe.txtLigneFilter.text.toLowerCase()) != -1)
    						  		||
    						  	  (valueSDA.toLowerCase().search(myListe.txtLigneFilter.text.toLowerCase())!= -1)) ;
    		 				  	   
        	
       	
        	return rfilter;
        	
        }      

		public function getSynthese():void {			
			var idracine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			trace(_moisDebDate.toDateString());
			var datedeb:String= DateFunction.formatDateAsInverseString(_moisDebDate);
			var datefin:String= DateFunction.formatDateAsInverseString(_moisFinDate);
			
			opData = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.usages.CallUsage",
					"getLignesByFiltre", processgetLignes);
			RemoteObjectUtil.callService(opData,idracine,datedeb,
											myListe.searchInput.text);
		}	

		public function processgetLignes(r:ResultEvent):void {
			if (r.result.length==0) {
				Alert.show("Pas de données");
			}
			arrCol1 = ArrayCollection(r.result);
			arrCol1.filterFunction = filterLignesFunc;            
            arrCol1.refresh();
            myListe.grdLigne.dataProvider = arrCol1;	
			//myParams.updateGridLignes(r.result);
		}
		
      
		public function clearGrid(e:Event):void {
			cleargrdDetails(e); 
			cleargrdTicket(e);     
		}
		
		public function cleargrdDetails(e:Event):void {
            myListe.grdDetails.dataProvider = null;
		}
		
		public function cleargrdTicket(e:Event):void {
           	myListe.grdTicket.dataProvider = null;
		}			
		
		public function FormatteHeure(heure:String,minute:String):String {
			var temp:String;
			if (heure.length==1) {
				temp="0" + heure +":";
			} else {
				temp=heure.substring(0,2) +":";
			}
			if (minute.length==1) {
				temp=temp + "0" + minute +":00";
			} else {
				temp=temp + minute.substring(0,2) +":00";
			}
			return temp;
		}

		public function onPerimetreChange():void {
			myListe.onPerimetreChange();
			myListe.grdDetails.dataProvider = null;
			myListe.grdTicket.dataProvider = null;
			myListe.grdLigne.dataProvider = null;
			myListe.txtLigneFilter.text="";
			myListe.searchInput.text="";
			myListe.mySelector.onPerimetreChange();
			initDate();
			setSelectedPeriode(_moisDeb,_moisFin);
		}
		
		
		////---------------------EXPORT 
		
		public function exportContent(event:Event):void{
			if(myListe.grdLigne.dataProvider != null) {
				var targetObject:Object=event.target;
			 	var urlback : String = moduleListeAppelsIHM.NonSecureUrlBackoffice+"/fr/consotel/consoview/usages/export/export.cfm";	
	            var variables : URLVariables = new URLVariables();
				variables.IDRACINE=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				variables.DATEDEB=DateFunction.formatDateAsInverseString(_moisDebDate);
				variables.DATEFIN=DateFunction.formatDateAsInverseString(_moisFinDate);
				variables.TEXT_INPUT=myListe.searchInput.text;
				variables.COMPONENT="usages.CallUsage";
				var canBeRun:Boolean=false;
	            switch(targetObject) {
	            	case myListe.listNdiCsv :
	            		variables.CONTENT_DESC="LIST_APPELS";
	            		variables.PROCESS="getLignesByFiltre";
	            		canBeRun=(myListe.grdLigne.dataProvider != null);
	            		break;
	            	case myListe.destCsv :
	            		variables.CONTENT_DESC="LIST_DEST";
	            		variables.PROCESS="getTypeAppel";
	            		canBeRun=((myListe.grdDetails.dataProvider != null) && (myListe.grdLigne.selectedItem != null));
	            		if(canBeRun == true) {
							variables.APPELANT=myListe.grdLigne.selectedItems[0].APPELANT;
							variables.SDA=myListe.grdLigne.selectedItems[0].SDA;
	            		}
	            		break;
	            	default :
	            		variables.CONTENT_DESC="UNKNOWN";
	            		break;
	            }
	            var request:URLRequest = new URLRequest(urlback);
	            request.data = variables;
	            request.method=URLRequestMethod.POST;
	            if(canBeRun==true)
	            	navigateToURL(request,"_blank");
			}
		}
		
		protected function imgCSVClickHandler(event:MouseEvent):void {
			if(myListe.grdTicket.dataProvider != null) {
				if((myListe.grdTicket.dataProvider as ArrayCollection).length > 0)
					exporterListe();
			}
		}
		
		public function exporterListe():void{
			if (myListe.grdDetails.selectedIndex != -1 && myListe.grdLigne.selectedIndex != -1){
				var idracine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				var datedeb:String= DateFunction.formatDateAsInverseString(_moisDebDate);
				var datefin:String= DateFunction.formatDateAsInverseString(_moisFinDate);
				var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																"fr.consotel.consoview.usages.CallUsage",
																"exporterListeTicketsDestinationLigne",
																exporterListeResultHandler);
				RemoteObjectUtil.callService(opData,idracine,datedeb,myListe.grdLigne.selectedItems[0].APPELANT,
							myListe.grdLigne.selectedItems[0].SDA,myListe.grdDetails.selectedItems[0].TYPE_APPEL);	
			}
		}
		
		private function exporterListeResultHandler(re : ResultEvent):void{
			if (String(re.result) != "error"){
				displayExport(String(re.result));				
			}else{
				Alert.show("Erreur export");				
			}
		}
		
		private function displayExport(name : String):void {			 
			var url:String = moduleListeAppelsIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/usages/csv/exportCSV.cfm";
            var request:URLRequest = new URLRequest(url);         
           	var variables:URLVariables = new URLVariables();
            variables.FILE_NAME = name;
            request.data = variables;
            request.method = URLRequestMethod.POST;
  			navigateToURL(request,"_blank");
         }

 	}
}
