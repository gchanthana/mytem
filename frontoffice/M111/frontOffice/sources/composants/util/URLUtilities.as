package composants.util
{
	import flash.external.ExternalInterface;

	public class URLUtilities
	{
		public function URLUtilities()
		{
		}
		protected static const WINDOW_OPEN_FUNCTION:String="window.open";

		public static function openWindow(url:String, window:String="_blank", features:String=""):void
		{
			ExternalInterface.call(WINDOW_OPEN_FUNCTION, url, window, features);
		}
	}
}
