package composants.util
{
	import composants.util.popupok.PopUpOkIHM;
	
	import flash.display.DisplayObject;
	
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import mx.resources.ResourceManager;

	[ResourceBundle("ConsoModuleLib")]
	
	public class ConsoviewAlert
	{
		
		
		/**		  
		 * Affiche une alertbox de confirmation 
		 * @param text  Le texte
		 * @param titre Le titre
		 * @param traitement La fonction qui gere le click sur OK ou CANCEL 
		 **/	
		public static function afficherAlertConfirmation(text : String , titre : String,traitement :  Function):void{
			
			[Embed(source='/assets/images/warning.png')]
	 		var myAlertImg0 : Class;
			var myAlertBox0 : Alert;								
			Alert.okLabel = ResourceManager.getInstance().getString('ConsoModuleLib','Oui');
			Alert.cancelLabel = ResourceManager.getInstance().getString('ConsoModuleLib','Non');			
			Alert.show(text,titre,Alert.OK | Alert.CANCEL,
			null,traitement,myAlertImg0);			
		}
		
		/**		  
		 * Affiche une alertbox 
		 * @param text  Le texte
		 * @param titre Le titre
		 **/	
		public static function afficherSimpleAlert(texte : String , titre : String, libelleBouton : String = "OK", couleurTexte : uint = 0x000000):void
		{	
			Alert.okLabel = libelleBouton;
			var a:Alert;
			a = Alert.show(texte,titre,Alert.OK);
			a.setStyle("color", couleurTexte);
		}
		
		/**		  
		 * Affiche une alertbox d'information 
		 * @param text  Le texte
		 * @param titre Le titre
		 * @param traitement La fonction qui gere le click sur OK ou CANCEL 
		 **/	
		public static function afficherAlertInfo(text : String , titre : String,traitement :  Function):void{
			
			[Embed(source='/assets/images/warning.png',mimeType='image/png')]
	 		var myAlertImg1 : Class;
			var myAlertBox1 : Alert;
			Alert.okLabel = "OK";					
			myAlertBox1 = Alert.show(text,titre,Alert.OK,
			null,traitement,myAlertImg1);			
		}
		
		
		public static function afficherError(text : String , titre : String,traitement :  Function = null):void
		{
			[Embed(source='/assets/images/error_big.png',mimeType='image/png')]
	 		var myAlertImg2 : Class;
			var myAlertBox2 : Alert;
			Alert.okLabel = "OK";
			myAlertBox2 = Alert.show(text,titre,Alert.OK,
			null,traitement,myAlertImg2); 				
			myAlertBox2.width = 500;
			
		}
		
		
		
		public static function afficherOKImage(text : String,ref:DisplayObject = null,duree:int=1000):void
		{
			var popup:PopUpOkIHM = new PopUpOkIHM();
			popup.duration = duree
			popup.message = text;
			if(ref)
			{
				PopUpManager.addPopUp(popup,ref,false);		
			}
			else
			{
				PopUpManager.addPopUp(popup,SystemManager.getSWFRoot(popup),false);
			}
			
			PopUpManager.centerPopUp(popup);
			
		}
	}
}