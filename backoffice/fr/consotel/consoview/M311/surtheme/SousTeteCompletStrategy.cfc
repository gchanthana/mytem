<!--- =========================================================================
Name: SousTeteFacturationStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords$
========================================================================== --->
<cfcomponent extends="Strategy" displayname="SousTeteFacturationStrategy" hint="">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="SousTeteFacturationStrategy" displayname="SousTeteFacturationStrategy init()" hint="Initialize the SousTeteFacturationStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS 
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		
		<cfargument name="sur_theme" required="true" type="string" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDCompte" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_LIGNE_V3.TB_SUR_THEME">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcompte" value="#IDCompte#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_debut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_date_fin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_surtheme" value="#sur_theme#"/>
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
		
		<cfreturn p_result/>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "Ligne">
	</cffunction>
	
	<cffunction name="getLibelle" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfquery name="qGetLibelle" datasource="#session.OffreDSN#">
			select theme_libelle
			from theme_produit
			where idtheme_produit=#ID#
		</cfquery>
		<cfreturn qGetLibelle.theme_libelle>
	</cffunction>
	
	<cffunction name="getNumero" access="public" returntype="string" output="false" displayname="string getNumero(numeric)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
			<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
				select sous_tete
				from sous_tete st
				where st.idsous_tete=#IDcompte#
			</cfquery>
			<cfreturn qGetCompte.sous_tete>
	</cffunction>
	
	
</cfcomponent>