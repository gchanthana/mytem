component implements="fr.saaswedo.api.order.bridge.IOperation"   {

	property
		name = "context"
		type = "struct"
		hint = "Call object";

	property
		name = "result"
		type = "struct"
		hint = "result object";

	property
		name = "tmpSonarDir"
		type = "string"
		hint = "temp directory for the order";

	public CreateOrder function init(struct context)
	output="false"
	{
		var UUIDdirectory = createUUID();

		this.context = arguments.context;
		this.result = {};

		//create a directory in ram
		this.tmpSonarDir = "ram:///sonar/#UUIDdirectory#";
		DirectoryCreate(this.tmpSonarDir);

		updateContext();
		return( this );
	}

	public string function getDefinition()
	{
		return 'CreateOrder';
	}

	public boolean function manageResult(required struct result)
	{

		var status = (arguments.result.statuscode contains "201");

		if(status eq true)
		{
			var sonarOrderId = 0;

			this.result.data = parseResult(arguments.result);
			this.result.sonarOrderId = trim(Listlast(ReReplaceNoCase(this.result.data,'/','|','ALL'),'|','yes'));
			sonarOrderId = this.result.sonarOrderId;
			linkOrder(this.context.OrderId,sonarOrderId);
		}
		else
		{
			this.result.data = parseError(arguments.result);
		}


		this.result.status = arguments.result.Statuscode;

		return status;
	}

	public struct function getResult()
	{
		return this.result;
	}

	public void function purgeTemporyItems()
	{
		DirectoryDelete(this.tmpSonarDir,true);
	}



	//--- Private Methods -----------------------------------------------------------------------------

	private string function  parseResult(required struct result)
	{
		var token = tostring(arguments.result.Responseheader.Location);

		return token;

	}

	private string function  parseError(required struct result)
	{
		return tostring(arguments.result.fileContent);
	}


	private void function updateContext()
	output="false"
	{
		context = this.context;
		HttpCall = createHttpCall();
		context.HttpCall = HttpCall;
	}

	private struct function	createHttpCall()
	output="false"
	{
		HttpCall = {};
		HttpCall['url'] = this.context.endpoint & "orders/";
		HttpCall['method'] = "POST";
		HttpCall['multipart'] = "yes";
		HttpCall['HttpParams'] = createHttpParams();

		return HttpCall;
	}

	private array function	createHttpParams()
	output="false"
	{

		var HttpParams = [];


		var param1 = {};
		param1['type'] = "header";
		param1['name'] = "api-key";
		param1['value'] = this.context.apiKey;

		HttpParams[1] = param1;



		var param2 = {};
		param2['type'] = "formfield";
		param2['name'] = "data";
		param2['value'] = createSonarOrder();
		HttpParams[2] = param2;

		HttpParams = addFileFormattedOrder(HttpParams,param2['value']);
		HttpParams = addMytemOrderFiles(HttpParams);

		return HttpParams;

	}

	private string function	createSonarOrder()
	output="true"
	{
		var sonarorder = new fr.saaswedo.api.order.sonar.database.GetSonarOrder();
		var cmd = sonarorder.execute(this.context.OrderId);

		var allRows = cmd['ARTICLE'].recordcount;
		var Order = {};
		var prefix = "::str::";
		var Products = [];


		//Order
		var OrderCarrier = {};
		var OrderRequestor = {};
		var ShippingAddress = {};

		OrderCarrier['Name'] = 	getCarrierName(cmd['COMMANDE']['NOMOP'][1]);
		OrderCarrier['AccountId'] =  prefix &  cmd['COMMANDE'][getCarrierAccount(OrderCarrier['Name'])][1];

		WriteLog(type="INFO", file="sonar", text="[ACCOUNT INFO] Carrier : #OrderCarrier['Name']#, AccountId : #OrderCarrier['AccountId']#");


		OrderRequestor['FullName'] = prefix &  		cmd['COMMANDE']['PRENOM'][1] & ' ' & cmd['COMMANDE']['NOM'][1];
		OrderRequestor['Email'] = prefix & 			cmd['COMMANDE']['EMAIL'][1];


		ShippingAddress['Recipient'] = 				cmd['COMMANDE']['A_L_ATTENTION'][1];
		ShippingAddress['Company'] = 				cmd['COMMANDE']['LIBELLE_RACINE'][1];
		ShippingAddress['AddressLine1'] = 			cmd['COMMANDE']['SP_ADRESSE1'][1];
		ShippingAddress['AddressLine2'] = 			cmd['COMMANDE']['SP_ADRESSE2'][1];
		ShippingAddress['City'] = 					cmd['COMMANDE']['SP_COMMUNE'][1];
		ShippingAddress['State'] = 					cmd['COMMANDE']['SP_CODE_INTERNE'][1];
		ShippingAddress['PostalCode'] = prefix & 	cmd['COMMANDE']['SP_CODE_POSTAL'][1];


		for (var intRow = 1;
			intRow <= allRows;
			intRow++)
		{
				var SProduct = new fr.saaswedo.api.order.sonar.entity.Product(cmd['ARTICLE']['IDARTICLE'][intRow],cmd);
				Products.addAll(SProduct.getSonarProduct());
		}


		order['OrderCarrier'] = OrderCarrier;
		order['OrderRequestor'] = OrderRequestor;
		order['ShippingAddress'] = ShippingAddress;
		order['Products'] = Products;

		jsOrderBase = SerializeJSON(order);
		jsOrder = Replace(jsOrderBase,prefix,'','all');

	 	writedump(jsOrder);
	 	//jsOrder = '{"OrderCarrier":{"AccountId":"000000002","Name":"Sprint"},"Products":[{"Plans":[{"Code":"PDSETV108","Price":"7.00","Type":"2"},{"Code":"U2748","Price":"39.99","Type":"1"},{"Code":"SMSUNL20","Price":"20.00","Type":"3"}],"PriceType":"2","Price":"849.99","Qty":"2","SKU":"885909971640","Type":"1"},{"PriceType":"5","Price":"59.99","Qty":"2","SKU":"660543353614","Type":"2"}],"ShippingAddress":{"Recipient":"saaswedo","PostalCode":"30005","City":"Atlanta","AddressLine1":"saaswedo","State":"GA","AddressLine2":"","Company":"saaswedo"},"OrderRequestor":{"Email":"monitoring@saaswedo.com","FullName":"Brice Miramont"}}';
		//writedump(jsOrder);
		WriteLog(type="INFO", file="sonar", text="[CREATE_SONAR_ORDER]  SONAR JSON : #jsOrder#");

		return  jsOrder;
	}



	private array function addMytemOrderFiles(required array httpParamList)
	output="false"
	{
		var filesService = new fr.saaswedo.api.order.sonar.database.GetOrderFile();
		var FSresult = filesService.execute(this.context.OrderId);
		var qFileList = FSresult['FILELIST'];
		var allRows = qFileList.recordcount;
		var _timeout = 180; //timeout in sec

	    for (	var intRow = 1;
				intRow <= allRows;
				intRow ++)
		{
			var param = {};
			var path = qFileList['PATH'][intRow];
			var filename = qFileList['FILE_NAME'][intRow];
			var file =  '/container/M16/#path#/#filename#';
			var timer = 10;

			WriteLog(type="INFO", file="sonar", text="[SONAR ADD FILE] FILE PATH : /container/M16/#path#/#filename#");
			while(!FileExists(file) && timer lte _timeout)
    	{
				WriteLog(type="INFO", file="sonar", text="[SONAR ADD FILE] waiting for file creation  ...  duration : #timer# sec limit = #_timeout# sec");
				WriteLog(type="INFO", file="sonar", text="[SONAR ADD FILE] file exists : #FileExists(file)#");
				sleep(10000);//wait 10 sec
				timer = timer + 10;
			}

			if(FileExists(ExpandPath(file)))
			{
				var mimeType =  getPageContext().getServletContext().getMimeType(file);
				param['type'] = "file";
				param['name'] = filename;
				param['file'] = file;
				param['mimeType'] = mimeType;

				arrayAppend(arguments.httpParamList,param);
			}

		}

		return arguments.httpParamList;
	}

	private array function addFileFormattedOrder(required array httpParamList,required string jsOrder)
	{

		var filename = "OwnerList.txt";
		var file = "#this.tmpSonarDir#/EndUserList.txt";
		//Write the file
		FileWrite(file, arguments.jsOrder);
		var mimeType =  getPageContext().getServletContext().getMimeType(file);
		var param = {};
		param['type'] = "file";
		param['name'] = filename;
		param['file'] = file;
		param['mimeType'] = mimeType;

		arrayAppend(arguments.httpParamList,param);

		//debug
		//var owners = FileRead(file);
		//FileWrite("/Users/samuel.divioka/Documents/projects/owner_list2.txt", "#owners#",'utf-8');
		//fin debug

		return arguments.httpParamList;
	}

	private string function getCarrierName(required string MytemCarrierName)
	{
	  var SonarCarrierList = 'Sprint,AT&T,Verizon';
	  for(var carrier in listToArray(SonarCarrierList))
	  {

	    var result = FindNoCase(carrier,arguments.MytemCarrierName);

	    if(result gt 0)
	    {
	      return carrier;
	    }


	  }

	  return  "#arguments.MytemCarrierName# not found";

	}

	private string function getCarrierAccount(required string SonarCarrierName)
	{
		var SonarCarrierList = 'Sprint|COMPTE_FACTURATION,AT&T|SOUS_COMPTE,Verizon|COMPTE_FACTURATION';
		var index = listContainsNoCase(SonarCarrierList,arguments.SonarCarrierName,',');

		if(index > 0)
		{
			return listToArray(toSTring(ListGetAt(SonarCarrierList,index,',')),'|')[2];
		}
		else
		{
			return "COMPTE_FACTURATION";
		}

	}

	private void function linkOrder(required numeric MytemOrderId, required string SonarOrderId)
	{
		var OrderLinker = new fr.saaswedo.api.order.sonar.database.LinkOrder();
		var result = OrderLinker.execute(arguments.MytemOrderId, arguments.SonarOrderId);
		WriteLog(type="INFO", file="sonar", text="[LINKORDER] MYTEMID : #arguments.MytemOrderId#, SONARID : #arguments.SonarOrderId#");


	}


}
