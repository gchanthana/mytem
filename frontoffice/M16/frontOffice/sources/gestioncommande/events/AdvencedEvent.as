package gestioncommande.events
{
	import gestioncommande.entity.Commande;
	
	import flash.events.Event;
	
	public class AdvencedEvent extends Event
	{
		
		public static const COMMANDEANNULER_CLICKED		:String = "ANNULER_CLICKED";
		public static const COMMANDEVALIDER_CLICKED		:String = "VALIDER_CLICKED";
		public static const COMMANDEPRECEDENT_CLICKED	:String = "PRECEDENT_CLICKED";
		public static const COMMANDENEXT_CLICKED		:String = "NEXT_CLICKED";
		public static const BUTTON_WIZZARD_CLICKED		:String = "BUTTON_WIZZARD_CLICKED";
		public static const FIELDSOK					:String = "FIELDSOK";
		public static const FIELDSKO					:String = "FIELDSKO";
		public static const NONE						:String = "NONE";
		
		public var commande								:Commande 	= null;
		public var viewStack							:Boolean 	= false;
		public var isValidate							:Boolean 	= false;

		
		public function AdvencedEvent(type:String, bubbles:Boolean = false, cmd:Commande = null, isValide:Boolean = false, vs:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.commande	= cmd;
			this.viewStack 	= vs;
			this.isValidate = isValide;
		}

		override public function clone():Event
		{
			return new AdvencedEvent(type, bubbles, commande , isValidate, viewStack, cancelable);
		}

	}
}