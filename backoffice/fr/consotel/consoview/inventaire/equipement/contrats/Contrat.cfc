<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.equipement.contrats.Contrat">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDCONTRAT" type="numeric" default="0">
	<cfproperty name="IDEQUIPEMENT" type="numeric" default="0">
	<cfproperty name="IDTYPE_CONTRAT" type="numeric" default="0">
	<cfproperty name="IDFOURNISSEUR" type="numeric" default="0">
	<cfproperty name="REFERENCE_CONTRAT" type="string" default="">
	<cfproperty name="DATE_SIGNATURE" type="date" default="">
	<cfproperty name="DATE_ECHEANCE" type="date" default="">
	<cfproperty name="COMMENTAIRE_CONTRAT" type="string" default="">
	<cfproperty name="MONTANT_CONTRAT" type="numeric" default="0">
	<cfproperty name="DUREE_CONTRAT" type="numeric" default="0">
	<cfproperty name="TACITE_RECONDUCTION" type="numeric" default="0">

	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.IDCONTRAT = 0;
		variables.IDEQUIPEMENT = 0;
		variables.IDTYPE_CONTRAT = 0;
		variables.IDFOURNISSEUR = 0;
		variables.REFERENCE_CONTRAT = "";
		variables.DATE_SIGNATURE = "";
		variables.DATE_ECHEANCE = "";
		variables.COMMENTAIRE_CONTRAT = "";
		variables.MONTANT_CONTRAT = 0;
		variables.DUREE_CONTRAT = 0;
		variables.TACITE_RECONDUCTION = 0;
	</cfscript>
	
	
	
	<cffunction name="init" output="false" returntype="Contrat">
		<cfargument name="id" required="false">
		<cfscript>
			if( structKeyExists(arguments, "id") )
			{
				load(arguments.id);
			}
			return this;
		</cfscript>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : GETTER et SETTER
	
	--->
	<cffunction name="getIDCONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.IDCONTRAT>
	</cffunction>

	<cffunction name="setIDCONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDCONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getIDEQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn variables.IDEQUIPEMENT>
	</cffunction>

	<cffunction name="setIDEQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDEQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>


	<cffunction name="getIDTYPE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.IDTYPE_CONTRAT>
	</cffunction>

	<cffunction name="setIDTYPE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDTYPE_CONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDFOURNISSEUR" output="false" access="public" returntype="any">
		<cfreturn variables.IDFOURNISSEUR>
	</cffunction>

	<cffunction name="setIDFOURNISSEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.IDFOURNISSEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getREFERENCE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.REFERENCE_CONTRAT>
	</cffunction>

	<cffunction name="setREFERENCE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.REFERENCE_CONTRAT = arguments.val>
	</cffunction>

	<cffunction name="getDATE_SIGNATURE" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_SIGNATURE>
	</cffunction>

	<cffunction name="setDATE_SIGNATURE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.DATE_SIGNATURE = arguments.val>
		<!--- <cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DATE_SIGNATURE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif> --->
	</cffunction>

	<cffunction name="getDATE_ECHEANCE" output="false" access="public" returntype="any">
		<cfreturn variables.DATE_ECHEANCE>
	</cffunction>

	<cffunction name="setDATE_ECHEANCE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.DATE_ECHEANCE = arguments.val>
		<!--- <cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.DATE_ECHEANCE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif> --->
	</cffunction>

	<cffunction name="getCOMMENTAIRE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.COMMENTAIRE_CONTRAT>
	</cffunction>

	<cffunction name="setCOMMENTAIRE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.COMMENTAIRE_CONTRAT = arguments.val>
	</cffunction>

	<cffunction name="getMONTANT_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.MONTANT_CONTRAT>
	</cffunction>

	<cffunction name="setMONTANT_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.MONTANT_CONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDUREE_CONTRAT" output="false" access="public" returntype="any">
		<cfreturn variables.DUREE_CONTRAT>
	</cffunction>

	<cffunction name="setDUREE_CONTRAT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DUREE_CONTRAT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getTACITE_RECONDUCTION" output="false" access="public" returntype="any">
		<cfreturn variables.TACITE_RECONDUCTION>
	</cffunction>

	<cffunction name="setTACITE_RECONDUCTION" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.TACITE_RECONDUCTION = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : Enregistre le contrat dans la base,
					 si l'identifiant du contrat est égal à 0 on crée un nouvelle enregistrement
					 sinon on met à jour l'enregistremnt référencé par l'identifiant.
		
		param out:
			- l'identifiant de l'enregistrement créé ou mis à jour
	--->
	<cffunction name="save" output="false" access="public" returntype="numeric">
	 
		<cfscript>
		
		if(getIDCONTRAT() eq 0)
		{	
				return create();
		}else{
				return update();
		}
		</cfscript>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : charge les données d'un contrat et affecte les propriétés de l'objet.		
		
		param in :		
			- l'identifiant du contrat
			
	
	--->
	<cffunction name="load" output="false" access="public" returntype="void">
		<cfargument name="id" required="true" >
		<cfset var qRead="">
		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select 	C.IDCONTRAT,IDEQUIPEMENT, IDTYPE_CONTRAT, IDFOURNISSEUR, REFERENCE_CONTRAT, DATE_SIGNATURE, DATE_ECHEANCE, 
					COMMENTAIRE_CONTRAT, MONTANT_CONTRAT, DUREE_CONTRAT, TACITE_RECONDUCTION
			from OFFRE.CONTRAT C,OFFRE.CONTRAT_EQUIPEMENT CE
			where C.IDCONTRAT = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.id#" />
			AND C.IDCONTRAT = CE.IDCONTRAT
		</cfquery>

		<cfscript>
			setIDCONTRAT(qRead.IDCONTRAT);
			setIDEQUIPEMENT(qRead.IDEQUIPEMENT);
			setIDTYPE_CONTRAT(qRead.IDTYPE_CONTRAT);
			setIDFOURNISSEUR(qRead.IDFOURNISSEUR);
			setREFERENCE_CONTRAT(qRead.REFERENCE_CONTRAT);
			setDATE_SIGNATURE(qRead.DATE_SIGNATURE);
			setDATE_ECHEANCE(qRead.DATE_ECHEANCE);
			setCOMMENTAIRE_CONTRAT(qRead.COMMENTAIRE_CONTRAT);
			setMONTANT_CONTRAT(qRead.MONTANT_CONTRAT);
			setDUREE_CONTRAT(qRead.DUREE_CONTRAT);
			setTACITE_RECONDUCTION(qRead.TACITE_RECONDUCTION);
		</cfscript>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : Crée un enregistrement dans la table offre.contrat avec les propriétés de l'objet.
		
		param out:
			- l'identifiant qui référence l'enregistrement venant d'être créé
	--->
	<cffunction name="create" output="false" access="private" returntype="numeric">
		<cfset var qCreate="">
		<cfset var qGetID="">
		
		<cfset var local1=this.getIDTYPE_CONTRAT()>
		<cfset var local2=this.getIDFOURNISSEUR()>
		<cfset var local3=this.getREFERENCE_CONTRAT()>
		<cfset var local4=getDATE_SIGNATURE()>
		<cfset var local5=getDATE_ECHEANCE()>
		<cfset var local6=this.getCOMMENTAIRE_CONTRAT()>
		<cfset var local7=this.getMONTANT_CONTRAT()>
		<cfset var local8=this.getDUREE_CONTRAT()>
		<cfset var local9=this.getTACITE_RECONDUCTION()>
		<cfset var local10=this.getIDEQUIPEMENT()>

		<cftransaction isolation="read_committed" >
			<cfquery name="qCreate" datasource="#SESSION.OFFREDSN#">
				insert into OFFRE.CONTRAT(IDRACINE,IDTYPE_CONTRAT, IDFOURNISSEUR, REFERENCE_CONTRAT, DATE_SIGNATURE, DATE_ECHEANCE, COMMENTAIRE_CONTRAT, MONTANT_CONTRAT, DUREE_CONTRAT, TACITE_RECONDUCTION)
				values (
					<cfqueryparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER"/>,
					<cfqueryparam value="#local1#" cfsqltype="CF_SQL_INTEGER" null="#iif((local1 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local2#" cfsqltype="CF_SQL_INTEGER" null="no" />,
					<cfqueryparam value="#local3#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local4#" cfsqltype="CF_SQL_DATE" null="#iif((local4 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local5#" cfsqltype="CF_SQL_DATE" null="#iif((local5 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local6#" cfsqltype="CF_SQL_VARCHAR" />,
					<cfqueryparam value="#local7#" cfsqltype="CF_SQL_FLOAT" null="#iif((local7 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local8#" cfsqltype="CF_SQL_INTEGER" null="#iif((local8 eq ""), de("yes"), de("no"))#" />,
					<cfqueryparam value="#local9#" cfsqltype="CF_SQL_INTEGER" null="#iif((local9 eq ""), de("yes"), de("no"))#" />
				)
			</cfquery>

			<!--- If your server has a better way to get the ID that is more reliable, use that instead --->
			<cfquery name="qGetID" datasource="#SESSION.OFFREDSN#">
				select IDCONTRAT 
				from OFFRE.CONTRAT
        		order by IDCONTRAT desc
			</cfquery>
			
			 
			<!--- <cfset setIDCONTRAT(qGetID.IDCONTRAT[1])>  --->
			<cfset setIDCONTRAT(qGetID.IDCONTRAT[1])>	
			
			<cfquery name="qLink" datasource="#SESSION.OFFREDSN#">
				insert into OFFRE.CONTRAT_EQUIPEMENT(IDCONTRAT,IDEQUIPEMENT)
				values (
				#getIDCONTRAT()#,
					<cfqueryparam value="#local10#" cfsqltype="CF_SQL_INTEGER" null="no" />					
				)
			</cfquery> 
			
		</cftransaction>
		
		
		
		
				
		<cfreturn 1>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : Met à jour un enregistrement dans la table offre.contrat avec les propriétés de l'objet.
		
		param out:
			- l'identifiant qui référence l'enregistrement qui vient d'être mis à jour
	--->
	<cffunction name="update" output="false" access="private" returntype="numeric">
		<cfset qUpdate="">
		 
		<cfquery name="qUpdate" datasource="#SESSION.OFFREDSN#" result="status">
			update OFFRE.CONTRAT
			set IDTYPE_CONTRAT = <cfqueryparam value="#this.getIDTYPE_CONTRAT()#" cfsqltype="CF_SQL_INTEGER" null="#iif((this.getIDTYPE_CONTRAT() eq ""), de("yes"), de("no"))#" />,
				IDFOURNISSEUR = <cfqueryparam value="#this.getIDFOURNISSEUR()#" cfsqltype="CF_SQL_INTEGER" null="#iif((this.getIDFOURNISSEUR() eq ""), de("yes"), de("no"))#" />,
				REFERENCE_CONTRAT = <cfqueryparam value="#this.getREFERENCE_CONTRAT()#" cfsqltype="CF_SQL_VARCHAR" />,
				DATE_SIGNATURE = <cfqueryparam value="#this.getDATE_SIGNATURE()#" cfsqltype="CF_SQL_DATE" null="#iif((this.getDATE_SIGNATURE() eq ""), de("yes"), de("no"))#" />,
				DATE_ECHEANCE = <cfqueryparam value="#this.getDATE_ECHEANCE()#" cfsqltype="CF_SQL_DATE" null="#iif((this.getDATE_ECHEANCE() eq ""), de("yes"), de("no"))#" />,
				COMMENTAIRE_CONTRAT = <cfqueryparam value="#this.getCOMMENTAIRE_CONTRAT()#" cfsqltype="CF_SQL_VARCHAR" />,
				MONTANT_CONTRAT = <cfqueryparam value="#this.getMONTANT_CONTRAT()#" cfsqltype="CF_SQL_FLOAT" null="#iif((this.getMONTANT_CONTRAT() eq ""), de("yes"), de("no"))#" />,
				DUREE_CONTRAT = <cfqueryparam value="#this.getDUREE_CONTRAT()#" cfsqltype="CF_SQL_INTEGER" null="#iif((this.getDUREE_CONTRAT() eq ""), de("yes"), de("no"))#" />,
				TACITE_RECONDUCTION = <cfqueryparam value="#this.getTACITE_RECONDUCTION()#" cfsqltype="CF_SQL_INTEGER" null="#iif((this.getTACITE_RECONDUCTION() eq ""), de("yes"), de("no"))#" />,
				IDRACINE = #SESSION.PERIMETRE.ID_GROUPE#
			where IDCONTRAT = <cfqueryparam value="#this.getIDCONTRAT()#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		<cfreturn 1> 
	</cffunction>

	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : Efface un enregistrement dans la table offre.contrat suivant les parametre de l'objet
	
	--->
	<cffunction name="delete" output="false" access="public" returntype="void">
		<cfset var qDelete="">
		
		<cfquery name="qDeleteCE" datasource="#SESSION.OFFREDSN#" result="status">
			delete
			from OFFRE.CONTRAT_EQUIPEMENT
			where IDCONTRAT = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#this.getIDCONTRAT()#" />
		</cfquery>
		
		<cfquery name="qDeleteC" datasource="#SESSION.OFFREDSN#" result="status">
			delete
			from OFFRE.CONTRAT
			where IDCONTRAT = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#this.getIDCONTRAT()#" />
		</cfquery>
		
	</cffunction>

	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="">		
		<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>		
	</cffunction>

</cfcomponent>