﻿<!-- displayname : le nom de composant -->

<cfcomponent displayname="Parametres" output="false">
	
	<!-- déclarer une fonction  name : le nom de la fonction ;returntype : le type du resultat retourné -->
	<!-- access : visibilité -->
	
	<cffunction access="remote" output="false" name="getSelector" returntype="query">
			
			<!--- les arguements de la fonction ; name: le nom de l'arguement; required="true": obligatoire -->
			<!-- plusieurs arguements = plusieurs balise -->
			<!-- on peut donner des noms differents des noms utilisé dans la fonction flex,  mais il faut garder l'order' --->
			
			<cfargument name="idRapport" type="numeric" required="true">
			
			<cfset idLang  = SESSION.USER.IDGLOBALIZATION>
							 
            <cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M331.get_param_rapport">			
				
				 <!-- paramtères : Si la procédure stockée utilise des paramètres d'entrée ou de sortie -->	
		
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRapport#" variable="idRapport">	
	                <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idLang#" variable="p_langid">
	                
                  <cfprocresult name="qParametres"><!-- Si la procédure stockée retourne un résultat -->
            </cfstoredproc>        					
            <cfreturn qParametres><!-- le resultat de la procedure-->
      </cffunction>  
</cfcomponent>