package catalogue.vo
{
	[Bindable]
	public class OperateurVO
	{
		public var ID_OP : int;
		public var LIBELLE_OP : String;
		public var SELECTED:Boolean = false;
		public var DEVISE : String = "";

		public function OperateurVO()
		{
			
		}
	}
}
		