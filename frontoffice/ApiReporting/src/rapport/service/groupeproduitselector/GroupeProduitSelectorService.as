package rapport.service.groupeproduitselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import rapport.service.module.*;

	public class GroupeProduitSelectorService
	{
		
		private var _model:GroupeProduitSelectorModel;
		public var handler:GroupeProduitSelectorHandler;
		
		public function GroupeProduitSelectorService():void
		{
			this._model = new GroupeProduitSelectorModel();
			this.handler = new GroupeProduitSelectorHandler(model);
		}		
		public function get model():GroupeProduitSelectorModel
		{
			return this._model;
		}
		
		public function getValuesGroupeProduit():void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.GroupeProduitSelectorService","getValuesGroupeProduit",handler.getValuesGroupeProduitHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
	}
}