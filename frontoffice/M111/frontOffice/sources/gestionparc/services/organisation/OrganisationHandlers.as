package gestionparc.services.organisation
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.rpc.events.ResultEvent;
	
	public class OrganisationHandlers
	{
		private var myDatas : OrganisationDatas;
		
		public function OrganisationHandlers(instanceOfDatas:OrganisationDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getNodesOrga()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDatasGetNodesOrga()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getNodesOrgaResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDatasGetNodesOrga(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.LOAD_NODE_ORGA_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getListOrgaCliente()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetListOrgaClienteResultHandler()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getListOrgaClienteResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatGetListOrgaClienteResultHandler(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.GET_ORGA_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
	}
}