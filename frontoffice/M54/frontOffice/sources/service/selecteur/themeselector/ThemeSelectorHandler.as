package service.selecteur.themeselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ThemeSelectorHandler
	{
		private var _model: ThemeSelectorModel;
		
		public function ThemeSelectorHandler(_model:ThemeSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValueThemeHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValues(evt.result as ArrayCollection);
			}
		}	
	}
}