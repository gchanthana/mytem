<cfcomponent output="false">
		
	<cffunction access="remote" output="true" name="getHistoriqueEnvoi" returntype="query">

		<cfargument name="idEvent" type="numeric" required="true">
		<cfargument name="noContrat" type="numeric" required="true">	 			
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.getHistoriqueEnvoi">
			
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEvent#" variable="p_idevent">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#noContrat#" variable="p_no_contrat">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
			
			<cfprocresult name="qHistorique">
		</cfstoredproc>	
					
		<cfreturn qHistorique>
		
	</cffunction>

</cfcomponent>