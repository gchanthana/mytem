<cfset tempUuid=createUUID()>
<cfset OFFREDSN = "ROCOFFRE">
<cfset boolOk = 1>

<cftry>

	<cfsavecontent variable="bodyMail">
		<cfdump var="#URL#">
		<cfoutput><hr></cfoutput>
		<cfdump var="#FORM#" format="html">
	</cfsavecontent>
<!---
	<cfset logInfoMntDetail(107,681,bodyMail,"[CONTAINER]
	 Demande de téléchargement", 0)>
--->
	<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.UUIDARRAYTOQUERY">
		<cfprocparam value="#URL.ARUUID#" cfsqltype="CF_SQL_CLOB" type="in">
		<cfprocresult name="arrDoc">
	</cfstoredproc>

	<cfsavecontent variable="arrDocBody">
		<cfdump var="#arrDoc#" format="html">
	</cfsavecontent>
<!---
	<cfset logInfoMntDetail(106, 680,arrDocBody, "[CONTAINER] 1ere proc - liste des fichiers à télécharger", 0)>
--->	
	


	<!--- Création du répertoire temporaire --->
	<cfset folder = expandPath('./tmp') >
	<cfset newFolder = folder & "/" & year(now()) & "/" & month(now()) & "/" & day(now()) & "/" & tempUuid>
	<cfset newFolder = folder & "/" & year(now()) & "/" & tempUuid>

	<cfif not directoryExists(newFolder)>

		<cflog text="[INFO]----------- [DEBUT] 	CREATE DIRECTORY  		#newFolder#" type="information">
		<cfdirectory action="create" directory="#newFolder#" recurse="true">
		<cflog text="[INFO]----------- [FIN] 	CREATE DIRECTORY  		#newFolder#" type="information">

	</cfif>

	<cfset filepath=newFolder>
	


	<cfset racineDir="/container">
	
	<!--- boucle sur les fichiers et les copier en locale--->
	<cfset index=1>
	<cfloop query="arrDoc">
	       <cfset fileUuid=arrDoc.uuid>
	       <cfset filenameTmp="#arrDoc.NOM_DETAILLE_RAPPORT#_#index#">
		   <cfset filename=cleanString(filenameTmp)>
	       <cfset filesuffix=arrDoc.format_fichier>
	       <cflog text="[DEBUT]  	COPIE source : #racineDir#/#fileUuid# - destination : #filepath# - filename : #filename#.#filesuffix#" type="information">
	       <cffile action="copy" source="#racineDir#/#fileUuid#" destination="#filepath#/#filename#.#filesuffix#">	      
	       <cflog text="[FIN] 		COPIE source : #racineDir#/#fileUuid# - destination : #filepath# - filename : #filename#.#filesuffix#" type="information">
	       <cfset index=index+1>
	 </cfloop>


	 <!--- Zipper le dossier --->
	<cflog text="INFO] ----------- [DEBUT] 		ZIP FOLDER 		#filepath#" 		type="information">
	<cfzip action="zip" source="#filepath#/" file="#filepath#/#archiveName#.zip" />	 
	<cflog text="[INFO] ----------- [FIN] 		ZIP FOLDER  	#filepath#" 		type="information">



	<cffile action="readbinary" variable="fileContent" file="#filepath#/#archiveName#.zip">
	<cfset deleteDir(filepath)>

	 
	<cfif boolOk eq 1>
		<cfheader name="Content-Disposition" value="attachment;filename=#archiveName#.zip">
		<cfcontent type="multipart/x-zip" variable="#fileContent#">	 	
	</cfif>


<cfcatch type="any">
	<cflog text="Catch : #cfcatch.message#">
	<cflog text="Catch : #cfcatch.detail#">
	<cfset  boolOk = 0>
	
	<!--- Test l'existance du dossier tmp  si l'existe on delete tout --->
	

	<cfmail to="monitoring@saaswedo.com" from="container@saaswedo.com" subject="Rapport impossible à télécharger : FTP erreur"
			type="html" server="mail-cv.consotel.fr" port="25" charset="utf-8" wraptext="74">
			Une erreur est survenue et empêche l'utilisateur #URL.USERNAME# (app_loginid : #URL.APPLOGINID#) de télécharger son(ses) rapport(s) : <br>
			type : #cfcatch.type# <br>
			message : #cfcatch.message# <br>
			detail : #cfcatch.detail# <br><br>

			Cordialement.
	</cfmail><!--- production@consotel.fr / bcc="dev@consotel.fr" --->
	<cfset deleteDir(filepath)>
</cfcatch>
</cftry>


<cfset deleteDir(filepath)>

<cffunction name="deleteDir" access="private" returntype="void" output="false">
	<cfargument name="dir" 		required="true" type="String" hint="absolute dir path"/>
	 
	<cfif DirectoryExists(dir)>
	 
    	<!--- If yes, check whether there are files in the directory before deleting. ---> 
		<cflog text="[INFO] ********** DIRECTORY EXISTS : #dir#" type="information">
   		<cfdirectory action="list" directory="#dir#" name="qDirectory"> 
	    <cfif qDirectory.recordcount gt 0> 
	    <!--- If yes, delete the files from the directory. ---> 
	        <cflog text="[INFO] ********** DIRECTORY NOT EMPTY  : #dir#" type="information">
			<cfloop query="qDirectory">
				<cflog text="[INFO] ********** [START] SO DELETE FILES : #dir#/#qDirectory.name#" type="information">
			    <cffile action="delete" file="#dir#/#qDirectory.name#">
			    <cflog text="[INFO] ********** [END] SO DELETE FILES : #dir#/#qDirectory.name#" type="information">
			</cfloop>
	    </cfif>

    	<!--- Directory is empty - just delete the directory. ---> 
    	<cflog text="[INFO] ********** [START] DIRECTORY EMPTY  : #dir# *** SO DELETE IT" type="information">
        <cfdirectory action = "delete" directory = "#dir#"> 
        <cflog text="[INFO] ********** [END] DIRECTORY EMPTY  : #dir# *** SO DELETE IT" type="information">

    </cfif>  
		 
</cffunction>



<!--- Cherche les caractères spéciaux les suppriment puis retourne la nouvelle chaine --->
<cffunction name="cleanString" access="private" returntype="String" output="false" hint="Cherche les caractères spéciaux les suppriment puis retourne la nouvelle chaine">
		<cfargument name="currentName" 		required="true" type="String"/>

			<cfset var specialChars = arrayNew(1)>
			<cfset var chars 		= structNew()>

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("À,Á,Â,Ã,Ä,Å,à,á,â,ã,ä,å")>
			<cfset chars.CHAR  = "a">

			<cfset arrayAppend(specialChars, chars)>

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ç,ç")>
			<cfset chars.CHAR  = "c">

			<cfset arrayAppend(specialChars, chars)>

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("È,É,Ê,Ë,è,é,ê,ë")>
			<cfset chars.CHAR  = "e">

			<cfset arrayAppend(specialChars, chars)>

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ì,Í,Î,Ï,ì,í,î,ï")>
			<cfset chars.CHAR  = "i">

			<cfset arrayAppend(specialChars, chars)>

			<cfset arrayAppend(specialChars, chars)>

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ñ,ñ")>
			<cfset chars.CHAR  = "n">

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ò,Ó,Ô,Õ,Ö,ð,ò,ó,ô,õ,ö")>
			<cfset chars.CHAR  = "o">

			<cfset arrayAppend(specialChars, chars)>

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ù,Ú,Û,Ü,ù,ú,û,ü")>
			<cfset chars.CHAR  = "u">

			<cfset arrayAppend(specialChars, chars)>

			<cfset chars 	   = structNew()>
			<cfset chars.CHARS = ListToArray("Ý,Ÿ,ý,ÿ")>
			<cfset chars.CHAR  = "y">

			<cfset arrayAppend(specialChars, chars)>

			<cfset lenSpecialChar = arrayLen(specialChars)>

			<!--- ICI ON REMPLACE TOUS LES ACCENTS ET AUTRES --->
			<cfloop index="idx1" from="1" to="#lenSpecialChar#">

				<cfset arraySpecialChars = specialChars[idx1]['CHARS']>
				<cfset lenSpecialChars = arrayLen(arraySpecialChars)>

				<cfloop index="idx2" from="1" to="#lenSpecialChars#">

					<cfset currentName = REReplace(currentName, arraySpecialChars[idx2], specialChars[idx1]['CHAR'], "All")>

				</cfloop>

			</cfloop>

			<!--- ICI ON REMPLACE TOUS LES CARACTERES SPECIAUX PAR '_' --->
			<!--- <cfset currentName = REReplace(currentName, "[&!?/\\-]", "_", "All")> --->
			<cfset currentName = REReplace(currentName, "[&!?/\\]", "_", "All")>
			<cfset currentName = REReplace(currentName, "<", "_", "All")>
			<cfset currentName = REReplace(currentName, ">", "_", "All")>
			<cfset currentName = REReplace(currentName, "}", "_", "All")>
			<cfset currentName = REReplace(currentName, "{", "_", "All")>
			<cfset currentName = REReplace(currentName, "\(", "_", "All")>
			<cfset currentName = REReplace(currentName, "\)", "_", "All")>
			<cfset currentName = REReplace(currentName, "\+", "_", "All")>
			<cfset currentName = REReplace(currentName, "\*", "_", "All")>
			<cfset currentName = REReplace(currentName, "\[", "_", "All")>
			<cfset currentName = REReplace(currentName, "\]", "_", "All")>
			<cfset currentName = REReplace(currentName, "\]", "_", "All")>
			<cfset currentName = REReplace(currentName, "°", "_", "All")>
			<cfset currentName = REReplace(currentName, "~", "_", "All")>
			<cfset currentName = REReplace(currentName, "'", "_", "All")>
			<cfset currentName = REReplace(currentName, "\|", "_", "All")>
			<cfset currentName = REReplace(currentName, "`", "_", "All")>
			<cfset currentName = REReplace(currentName, "\^", "_", "All")>
			<cfset currentName = REReplace(currentName, "@", "_", "All")>
			<cfset currentName = REReplace(currentName, "=", "_", "All")>
			<cfset currentName = REReplace(currentName, ",", "_", "All")>
			<cfset currentName = REReplace(currentName, "\,", "_", "All")>
			<cfset currentName = REReplace(currentName, ";", "_", "All")>
			<cfset currentName = REReplace(currentName, "\s", "_", "All")>

		<cfreturn currentName>
</cffunction>

<cffunction name="logInfoMntDetail" access="private" returntype="void">
		<cfargument name="p_idmnt_process" 	type="numeric"	required="true">
		<cfargument name="p_idmnt_event" 	type="numeric" 	required="true">
		<cfargument name="p_body" 			type="any" 		required="true">
		<cfargument name="p_subject" 		type="string" 	required="true">
		<cfargument name="p_bool_error" 	type="numeric" 	required="true">

		<cfset p_idracine = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="mnt.pkg_mnt_log.logit">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idmnt_process"	value="#ARGUMENTS.p_idmnt_process#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idmnt_event" 	value="#ARGUMENTS.p_idmnt_event#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_body"		value="#ARGUMENTS.p_body#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_subject" 		value="#ARGUMENTS.p_subject#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 		value="#p_idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_uuid" 			null="true">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_bool_error" 	value="#ARGUMENTS.p_bool_error#">
			<cfprocparam type="In" cfsqltype="CF_SQL_TIMESTAMP" variable="p_DATE" 			null="true">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  	variable="p_file_name" 		value="-">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  	variable="p_file_path" 		value="-">

			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
</cffunction>