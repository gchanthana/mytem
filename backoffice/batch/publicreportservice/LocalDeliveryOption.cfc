<cfcomponent name="LocalDeliveryOption" displayname="LocalDeliveryOption">
	<cfset variables.LocalDeliveryOption=structNew()>
	
	<cffunction name="setLocalDeliveryOption" access="package" returntype="void">
		<cfargument name="destination" type="String" required="true">

		<cfset variables.LocalDeliveryOption.destination=destination>
	</cffunction>
	
	<cffunction name="getLocalDeliveryOption" access="package" returntype="Struct">
		<cfreturn variables.LocalDeliveryOption>
	</cffunction>
</cfcomponent>