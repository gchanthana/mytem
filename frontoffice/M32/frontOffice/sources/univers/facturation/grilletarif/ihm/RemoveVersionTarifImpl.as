package univers.facturation.grilletarif.ihm
{
	import mx.containers.TitleWindow;
	import mx.controls.RadioButton;
	import mx.managers.PopUpManager;
	
	import univers.facturation.grilletarif.entity.GroupeProduitVO;
	import univers.facturation.grilletarif.entity.VersionTarifVO;
	import univers.facturation.grilletarif.services.GrilleTarifService;
	import univers.facturation.grilletarif.utils.GestionSuppressionTarif;

	[Bindable]
	public class RemoveVersionTarifImpl extends TitleWindow
	{
		
		public var groupeProduits : GroupeProduitVO;
		public var serviceGrilleTarif : GrilleTarifService;
		public var idRacine : int;
		public  var version_current : VersionTarifVO;
		public var version_anterieur : VersionTarifVO;
		public  var version_posterieur : VersionTarifVO;
				
		public var boolEneableCreation : Boolean = false;
		public var rbAnt : RadioButton;
		public var rbPost : RadioButton;
		public var message : String;
		
		public var removeUniqueVersion : Boolean;
		public var typeSuppression : String;
		
		
		public function RemoveVersionTarifImpl()
		{
			super();
		}
		public function init():void
		{
			if(!message)
			{
				setCurrentState('');
			}
			else
			{
				boolEneableCreation = true;
			}
			PopUpManager.centerPopUp(this);
		}
		public function fermer():void
		{
			PopUpManager.removePopUp(this);
		}
		public function radioHandler():void
		{
			if(rbAnt.selected || rbPost.selected)
			{
				boolEneableCreation = true
			}
		}
		public function valider():void
		{
			var methode : String='';
			
			if(rbAnt.selected )
			{
				methode = "Ant";
			}
			else if(rbPost.selected)
			{
				methode = "Post";
			}
			
			if(typeSuppression==GestionSuppressionTarif.TARIF_EXCEPTION)
			{
				
				serviceGrilleTarif.removeAllVersionTarif(groupeProduits.id_groupe_produit,idRacine,version_current.idCompte,version_current.idSousCompte,version_current.cleType);
			}
			else
			{
				serviceGrilleTarif.removeVersionTarif(version_current.idVersionTarif,methode,removeUniqueVersion,typeSuppression);
			}
			
			
			PopUpManager.removePopUp(this);
		}
	}
}