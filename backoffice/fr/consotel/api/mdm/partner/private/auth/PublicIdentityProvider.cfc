<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.auth.PublicIdentityProvider" extends="fr.consotel.api.mdm.partner.private.auth.ClientIdentityProvider"
hint="Stratégie d'authentification utilisée par les API Partenaires Publiques MDM (Aucune authentification utilisateur n'est requise)
Les credentials fournis à getAuthIdentity() doivent avoir le contenu suivant :
- DOMAIN : Valeur public.consotel.fr
- PUBLIC_CLIENT : Instance d'une implémentation de type PublicAPI du package fr.consotel.api.mdm.partner.public. Seules les classes suivantes sont autorisées : Zenprise, FSecure">
	<cffunction access="package" name="assertAuthIdentity" returntype="boolean">
		<cfargument name="authIdentity" type="Struct" required="true">
		<cfset var userIdentity=ARGUMENTS["authIdentity"]>
		<cfset var authAssertion=FALSE>
		<cfif structKeyExists(userIdentity,"DOMAIN") AND (NOT compareNoCase(userIdentity["DOMAIN"],"public.consotel.fr"))>
			<cfif structKeyExists(userIdentity,"PUBLIC_CLIENT")>
				<cfset authAssertion=isClientPackageValid(userIdentity["PUBLIC_CLIENT"])>
			</cfif>
		</cfif>
		<cfreturn authAssertion>
	</cffunction>

	<cffunction access="package" name="getAuthIdentity" returntype="Struct" hint="La structure retournée est authCredentials">
		<cfargument name="authCredentials" type="Struct" required="true">
		<cfreturn ARGUMENTS["authCredentials"]>
	</cffunction>
	
	<cffunction access="package" name="isClientPackageValid" returntype="boolean" hint="Retourne TRUE si publicClient se trouve dans le même package que PublicAPI">
		<cfargument name="publicClient" type="fr.consotel.api.mdm.partner.public.PublicAPI" required="true">
		<cfset var publicClientName=getMetadata(ARGUMENTS["publicClient"]).NAME>
		<cfset var publicClientNameIndex=listLen(publicClientName,".")>
		<cfset var publicClientPackage=listDeleteAt(publicClientName,publicClientNameIndex,".")>
		<cfreturn (NOT compareNoCase(publicClientPackage,"fr.consotel.api.mdm.partner.public"))>
	</cffunction>
</cfcomponent>