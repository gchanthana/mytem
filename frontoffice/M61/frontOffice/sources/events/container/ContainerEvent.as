package events.container
{
	import flash.events.Event;

	public class ContainerEvent extends Event
	{
		
		//----------------- VARIABLES -----------------//
		
		public static const CLOSE_EVENT				:String = "CLOSE_EVENT";
		public static const VALIDATE_EVENT			:String = "VALIDATE_EVENT";
		public static const VALIDATEDEPOT_EVENT		:String = "VALIDATEDEPOT_EVENT";
		public static const SUPPRESION_RESULTEVENT	:String = "SUPPRESION_RESULTEVENT";
		public static const DOWNLOAD_RESULTEVENT	:String = "DOWNLOAD_RESULTEVENT";
		public static const UPLOAD_COMPLETE			:String = "UPLOAD_COMPLETE";
		public static const UPLOAD_ERROR			:String = "UPLOAD_ERROR";
		public static const ZIP_READY				:String = "ZIP_READY";
		public static const ZIP_SELECT				:String = "ZIP_SELECT";
		public static const ZIP_ERROR				:String = "ZIP_ERROR";
		public static const LISTEDOC_RESULTEVENT	:String = "LISTEDOC_RESULTEVENT";
		public static const DOCUMENT_ADDED			:String = "DOCUMENT_ADDED";
		public static const DOCUMENT_RENAMED		:String = "DOCUMENT_RENAMED";
		public static const LISTEPARAMS_RESULTEVENT	:String = "LISTEPARAMS_RESULTEVENT"
		
		private var _obj:Object;
		
		
		//----------------- METHODES -----------------//
		
		/* */
		public function ContainerEvent(type:String, obj:Object= null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._obj = obj
		}
		
		/* */
		public function set obj(value:Object):void
		{
			_obj = value;
		}

		/* */
		public function get obj():Object
		{
			return _obj;
		}
	}
}