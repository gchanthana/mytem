package intranet.cataloguepublic.controles
{
	import intranet.cataloguepublic.vo.GroupeProduitControle;
	import intranet.cataloguepublic.vo.ParamsRecherche;
	import intranet.cataloguepublic.vo.Societe;
	import intranet.cataloguepublic.vo.VersionDeTarif;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	
	[Bindable]
	public class AbstractGestionTarifs 
	{
		//la liste des opérateurs du groupe
		protected var _listeOperateurs : ArrayCollection = new ArrayCollection();		
		public function get listeOperateurs():ArrayCollection{
			return _listeOperateurs;
		};
		public function set listeOperateurs(lo :ArrayCollection):void{
			_listeOperateurs = lo;
		};
				
		
		//La liste des sociétés du groupe		
		protected var _listeSocietes : ArrayCollection = new ArrayCollection();		
		public function get listeSocietes():ArrayCollection{
			return _listeSocietes;
		};
		public function set listeSocietes(ls :ArrayCollection):void{
			_listeSocietes = ls;
		};
		
		
		//La liste des version de tarifs du produits sélectionné		
		protected var _listeVesrions : ArrayCollection = new ArrayCollection();		
		public function get listeVesrions():ArrayCollection{
			return _listeVesrions;
		};
		public function set listeVesrions(lv :ArrayCollection):void{
			_listeVesrions = lv;
		};
		
		//la liste des goupes de produits abos
		protected var _listeProduitsAbos : ArrayCollection = new ArrayCollection();				
		public function get listeProduitsAbos():ArrayCollection{
			return _listeProduitsAbos;
		};
		public function set listeProduitsAbos(lp :ArrayCollection):void{
			_listeProduitsAbos = lp;
		};
		
		//la liste des groupes de produits consos
		protected var _listeProduitsConsos : ArrayCollection = new ArrayCollection();				
		public function get listeProduitsConsos():ArrayCollection{
			return _listeProduitsConsos;
		};
		
		//la liste des goupes de produits abos
		protected var _listeProduitsPublicAbos : ArrayCollection = new ArrayCollection();				
		public function get listeProduitsPublicAbos():ArrayCollection{
			return _listeProduitsPublicAbos;
		};
		public function set listeProduitsPublicAbos(lp :ArrayCollection):void{
			_listeProduitsPublicAbos = lp;
		};
		
		//la liste des groupes de produits consos
		protected var _listeProduitsPublicConsos : ArrayCollection = new ArrayCollection();				
		public function get listeProduitsPublicConsos():ArrayCollection{
			return _listeProduitsPublicConsos;
		};
		
		public function set listeProduitsConsos(lp :ArrayCollection):void{
			_listeProduitsConsos = lp;
		};
		
		//le poids des produits tagué 
		protected var _poidsTotal : String = "0";		
		public function get poidsTotal():String
		{
			return _poidsTotal;
		}		
		public function set poidsTotal(pt:String):void
		{
			_poidsTotal = pt
		}
		
		
		//montant total 
		protected var _montantTotal : String = "0";
		public function get montantTotal():String
		{
			return _montantTotal;
		}		
		public function set montantTotal(mtt : String):void
		{
			_montantTotal = mtt
		}
		
		//montant total selectionné 
		protected var _montantTotalPrdtSelectionnes : String = "0";
		public function get montantTotalPrdtSelectionnes():String
		{
			return _montantTotalPrdtSelectionnes;
		}		
		public function set montantTotalPrdtSelectionnes(mttps : String):void
		{
			_montantTotalPrdtSelectionnes = mttps
		}
		
		
		//La societe selectionné
		protected var _selectedSociete : Societe;
		public function get selectedSociete():Societe
		{
			return _selectedSociete;
		}
		public function set selectedSociete(s:Societe):void
		{
			_selectedSociete = s;
		}
			
		
		//Le produits sélectionné
		protected var _selectedProduit : GroupeProduitControle;
		public function get selectedProduit():GroupeProduitControle
		{
			return _selectedProduit;
		}		
		public function set selectedProduit(p:GroupeProduitControle):void
		{
			_selectedProduit = p;
		}
		
			
		//La nouvelle version de tarifs
		protected var _newVersion : VersionDeTarif;
		public function get newVersion():VersionDeTarif
		{	
			return _newVersion;
		}		
		public function set newVersion(v:VersionDeTarif):void
		{
			
			_newVersion= v;
		}
		
		protected var _selectedVersion : VersionDeTarif;
		public function get selectedVersion():VersionDeTarif
		{
			return _selectedVersion;
		}		
		public function set selectedVersion(v:VersionDeTarif):void
		{
			_selectedVersion= v;
		}
		
		//Les params de recherche		
		protected var _paramsRecherche : ParamsRecherche = new ParamsRecherche();
		
		
		
		
		public function taguerProduit(): void
	    {
	    	 
	    }
	    
	    public function detaguerProduit(): void	    
	    {	    	
	    	
	    }
		
		public function setParamsRecherches(operateurId:Number, chaine:String, dateDebut:Date, dateFin:Date, catalogueClient:Number = 1):void
		{
			//TODO: implement function
		}
		
		public function getListeProduits():void
		{
			//TODO: implement function
		}
		
		public function creerVesrionTarif():void
		{
			//TODO: implement function
		}
		
		public function enregistrerVersionTarif():void{
	    	//TODO: implement function
	    }
	    
	    public function supprimerVersion():void{
	    	//TODO: implement function	
	    }
		
		public function getListeSocietes():void
		{
			//TODO: implement function
		}		
		
		public function getListeOperateurs():void
		{
			//TODO: implement function
		}
		
		public function calculerPartProduitDansFacturation():void{
	    	//TODO: implement function
	    }
	    
	    protected function updateProduit(ptoduit : GroupeProduitControle):void{
			
		}
		
		
	    protected function remoteErrorHandler(faultEvent:FaultEvent):void {
			//TODO: implement function
		}
		
		
	}
}