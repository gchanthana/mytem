package univers.inventaire.equipements.gestionflottemobile.importmasse
{
	
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	import composants.util.employes.Collaborateur;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.ByteArray;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	
	public class ImportMasse extends ImportMasseIHM
	{
		public static var _array_data_equipement :ArrayCollection = new ArrayCollection();
		
		[Bindable]
       	public static var _dataListeCollab:ArrayCollection = new ArrayCollection();
       
       	public static var _datagrid_import :DataGrid;
		
		public static var bool_check_term : Boolean = true;
		public static var bool_check_sim:Boolean = true;
		public static var bool_check_collab : Boolean=true;
		
		private var bool_check_garantie : Boolean=true;
		
		private var _bool_new_libelle_modele : Boolean=false;
		
		protected var _lastItemEditor:IListItemRenderer;
    	protected var _lastItemEditorPosition:Object;
    	
    	//-1 signifit par defaut on ne selectionne aucun modèle
    	private var _selected_new_modele : int = -1;
    	
    	private var _type_initialisation : String ;
    	
    	private var _data_constructeur : ArrayCollection = new ArrayCollection();
		
		public function ImportMasse(type_initialisation : String = null) 
		{
			_type_initialisation = type_initialisation;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			//_dataListeCollab = new ArrayCollection();
			_array_data_equipement.removeAll();
			//Fermeture du pop-up
			addEventListener(CloseEvent.CLOSE,fermer);
			//Ecouteur sur la liste des revendeurs de terminaux
			combo_revendeur.addEventListener(Event.CHANGE,combo_revendeur_handler);
			//Ajouter une ligne
			bt_add_row.addEventListener(MouseEvent.CLICK,addObjToDg);
			//Liste des revendeur						
			init_data_combo_revendeur();
			//Liste déroulante des opérateurs
			init_liste_ope();
			//Init le datagrid
			init_dg_equipement();
			//init liste collab
			init_liste_collab();
			//init la liste des constructeur --> Utilisé pour créer un modèle
			init_data_combo_constructeur();
			//Ecouteur sur les cases à cocher
			check_collab.addEventListener(Event.CHANGE,check_collab_handler);
			check_sim.addEventListener(Event.CHANGE,check_sim_handler);			
			check_term.addEventListener(Event.CHANGE,check_term_handler);
			check_garantie.addEventListener(Event.CHANGE,check_garantie_handler);
			
			//Ecouteur sur les colonnes:
			bt_ok.addEventListener(MouseEvent.CLICK,valider_handler);
			bt_annuler.addEventListener(MouseEvent.CLICK,fermer);
			//
			bt_remove_row.addEventListener(MouseEvent.CLICK,remove_row);
			
			column_collab.visible=false;
			bool_check_collab=false;
			check_collab.selected=false;
			
			if(_type_initialisation == "TERMINAL")
			{

				column_sim.visible=false;
				bool_check_sim=false;
				check_sim.selected=false;
				box_param_sim.enabled = false;
				
			}
			if(_type_initialisation == "SIM")
			{
				column_term.visible=false;
				bool_check_term=false;
				check_term.selected=false;
				hbox2.enabled = false;
			}
			
						
			calendar.selectedDate = new Date(); // Selection de la date actuel dans la date du jour
		}
		
		private function remove_row(evt : MouseEvent):void
		{
			if(dg_equipement.selectedIndex!=-1){
				(dg_equipement.dataProvider as ArrayCollection).removeItemAt(dg_equipement.selectedIndex);
			}
			else
			{
				Alert.show("Sélectionnez le ligne à supprimer");
			}
		} 
		private function init_liste_ope():void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
					"getFabOp", process_init_liste_ope);
			RemoteObjectUtil.callService(opData);
			
		}
		private function process_init_liste_ope(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				combo_operateur.dataProvider = evt.result;
			}
		}		
   
		private function valider_handler(evt : Event):void
		{	
			var mydoc:XML = <equipements></equipements>;
			
			var col_dataImport : ArrayCollection = dg_equipement.dataProvider as ArrayCollection;
			for(var i:int=0;i<col_dataImport.length;i++)
			{
				var tmp_imei:String;
				var tmp_numSim:String;
				var tmp_idEmp:String;
				
				var boolAddChild : Boolean=false;
				if(col_dataImport.getItemAt(i).IMEI=="Aucune entrée" || col_dataImport.getItemAt(i).IMEI=="Entrez un IMEI" || col_dataImport.getItemAt(i).IMEI==null)
				{
					tmp_imei = "";
				}
				else
				{
					tmp_imei = col_dataImport.getItemAt(i).IMEI;
					boolAddChild = true;
				}
				if(col_dataImport.getItemAt(i).NUMSIM=="Aucune entrée" || col_dataImport.getItemAt(i).NUMSIM=="Entrez un numéro de SIM" || col_dataImport.getItemAt(i).NUMSIM==null)
				{
					tmp_numSim= "";
					
				}
				else
				{
					tmp_numSim = col_dataImport.getItemAt(i).NUMSIM;
					boolAddChild = true;
				}				
				if(col_dataImport.getItemAt(i).IDEMPLOYE=="-1" || col_dataImport.getItemAt(i).IDEMPLOYE=="undefined" || col_dataImport.getItemAt(i).IDEMPLOYE==-1 || col_dataImport.getItemAt(i).IDEMPLOYE==null)
				{
					tmp_idEmp= "";
					
				}
				else
				{
					tmp_idEmp = col_dataImport.getItemAt(i).IDEMPLOYE;
					boolAddChild = true;
				}	
				//Si les 3 variables ne sont pas vides :
				if(boolAddChild == true)
				{
					var myChild : Object = "<equipement><num_sim>"+tmp_numSim+"</num_sim><imei>"+tmp_imei+"</imei><idemploye>"+tmp_idEmp+"</idemploye></equipement>";
					mydoc.appendChild(myChild);
					//Re-init :
					boolAddChild=false;
				}
			}
			if(mydoc != <equipements></equipements>){
			
			
			var dateDebGarantie : Date = calendar.selectedDate;
			var dureeGrantie : Number = Number(combo_listeMois.selectedItem.data);
	    	
	    	if(check_garantie.selected==false)
	    	{
	    		dateDebGarantie = null;
				dureeGrantie = 0;
	    	}
	    	  	
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
								"saveMultipleEquip", process_init_save_all_equip);
		 
		
			RemoteObjectUtil.callService(opData,
						Number(combo_revendeur.selectedItem.IDREVENDEUR),
						Number(combo_modele.selectedItem.IDEQUIPEMENT_FOURNIS),
						Number(combo_operateur.selectedItem.IDFOURNISSEUR),
						mydoc.toString(),
						Number(PanelGestionFoltteMobile.idPoolGestionnaire),
						DateFunction.formatDateAsInverseString(dateDebGarantie),
						dureeGrantie
						);
			}
			else
			{
				Alert.show("Aucun équipement à importer");
			}
		}
		private function process_init_save_all_equip(evt : ResultEvent):void
		{
			if(evt.result>0)
			{
				ConsoviewAlert.afficherOKImage("Equipement importé");
				PopUpManager.removePopUp(this);
			}
			else
			{
				trace("erreur procédure fr.consotel.consoview.inventaire.employes.EmployesUtils : saveMultipleEquip");
			}
		}
		private function onCollectionChange(e:CollectionEvent):void
        {
           //Alert.show(ObjectUtil.toString(_array_data_equipement));
        }

		private function check_collab_handler(evt:Event):void
		{
			bool_check_collab = check_collab.selected;
			column_collab.visible = check_collab.selected;
		}
		private function check_sim_handler(evt:Event):void
		{
			bool_check_sim = check_sim.selected;
			box_param_sim.enabled=bool_check_sim;
			column_sim.visible = bool_check_sim;
		}
		private function check_term_handler(evt:Event):void
		{
			bool_check_term = check_term.selected;
			//box_param_term.enabled=bool_check_term;
			hbox2.enabled=bool_check_term;
			column_term.visible = bool_check_term;
		}
		
		private function check_garantie_handler(evt:Event):void
		{
			bool_check_garantie = check_garantie.selected;
			box_garantie.enabled = bool_check_garantie;
		}
		private function init_dg_equipement():void
		{
			dg_equipement.dataProvider=null;
			_datagrid_import = this.dg_equipement;
			
			var obj:Object = new Object();
           	obj.IMEI = "Entrez un IMEI";
           	obj.NUMSIM="Entrez un numéro de SIM";
           	obj.IDEMPLOYE="-1";
          	 _array_data_equipement.addItem(obj);
           	dg_equipement.dataProvider=_array_data_equipement;           	
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function combo_revendeur_handler(evt : Event):void
		{
			if(combo_revendeur.selectedIndex!=-1)
			{
				init_state_modele();
			}
			else
			{
				this.setCurrentState('');
			}
		}
		private function combo_modele_handler(evt : Event):void
		{
			
		}
		private function combo_operateur_handler(evt : Event):void
		{
			
		}
		private function init_data_combo_revendeur():void
		{
			
			var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
	    	var chaine:String = chaine;
	    	var APP_LOGINID:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
	    	var p_segment:int = 1;//Segment mobile
	    	
	    	
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
					"get_catalogue_revendeur", process_init_data_combo_revendeur);
			RemoteObjectUtil.callService(opData, Idracine, chaine, APP_LOGINID, p_segment);
			
		}
		private function process_init_data_combo_revendeur(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				combo_revendeur.dataProvider = evt.result;
				//Si 1 seul résultat le Event.change de combo_revendeur n'est jamais déclenché
				if((combo_revendeur.dataProvider as ArrayCollection).length==1)
				{
					init_state_modele();
				}
			}
		}
		private function init_data_combo_modele():void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils",
							"fournirListeMobileCatalogueClient", 
							process_init_data_combo_modele);
			RemoteObjectUtil.callService(opData,combo_revendeur.selectedItem.IDREVENDEUR,"","MOBILE")
		}
		private function process_init_data_combo_modele(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				combo_modele.dataProvider = evt.result;
				if(_selected_new_modele !=-1)
				{
					for(var i : int = 0;i<(combo_modele.dataProvider as ArrayCollection).length;i++)
					{
						
						if((combo_modele.dataProvider as ArrayCollection).getItemAt(i).IDEQUIPEMENT_FOURNIS  == _selected_new_modele)
						{
							combo_modele.selectedIndex = i;
							_selected_new_modele = -1;
							
						}
					}
				}
			}
			
			
		}

		private function process_init_data_combo_operateur(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				combo_operateur.dataProvider = evt.result;
			}
		}
		private function clone(source:Object):*
        {
            var myBA:ByteArray = new ByteArray();
            myBA.writeObject(source);
            myBA.position = 0;
            return(myBA.readObject());
        }
        private function addObjToDg(evt:Event=null):void
        {
          	var temp:Array = clone(dg_equipement.dataProvider.toArray());          	
            _array_data_equipement=new ArrayCollection(temp);
            dg_equipement.dataProvider=_array_data_equipement;
            var obj:Object = new Object();
          
           _array_data_equipement.addItemAt(obj,0);
           setDataGridFocus();
        }
   
        
        private function setDataGridFocus():void
        {	
        	dg_equipement.editedItemPosition = {rowIndex: 0, columnIndex: 0};
        }
        private function init_liste_collab():void
			{ 
				
				
				 var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.inventaire.employes.EmployesUtils",
								"fournirListeEmployes",
								initResultHandler);
								
				RemoteObjectUtil.callService(opData,PanelGestionFoltteMobile.idPoolGestionnaire,"");
			}
			private function initResultHandler(re : ResultEvent):void
			{
				if (re.result)
				{
					   
		    		var cursor : IViewCursor = (re.result as ArrayCollection).createCursor();
		    		while(!cursor.afterLast)
		    		{	
		    			var employeObj:Collaborateur = new Collaborateur();	    			
		    			employeObj.NOMPRENOM = (cursor.current.NOM != null)?cursor.current.NOM:'';
		    			employeObj.NOM = employeObj.NOMPRENOM;
		    			employeObj.IDEMPLOYE = cursor.current.IDEMPLOYE;
		    			employeObj.IDGROUPE_CLIENT = cursor.current.IDGROUPE_CLIENT;
		    			employeObj.MATRICULE = cursor.current.MATRICULE;
		    			employeObj.CLE_IDENTIFIANT = cursor.current.CLE_IDENTIFIANT;
		    			employeObj.INOUT = 	cursor.current.INOUT; 
		    			   			
		    			if(employeObj.INOUT > 0)
		    			{
		    				_dataListeCollab.addItem(employeObj);	
		    			}
		    			cursor.moveNext();
		    			_dataListeCollab.refresh();
		    			
		    			
		    		}	
		    	}
			}
		private function init_data_combo_constructeur():void
		{
			var idGroupe : int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.inventaire.equipement.equipement",
					"getConstructeur", process_init_data_combo_constructeur);
					RemoteObjectUtil.callService(opData,idGroupe);
			
		}
		private function process_init_data_combo_constructeur(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				_data_constructeur = evt.result as ArrayCollection;
			}
			
			
		}
		private function init_data_combo_modele_public(evt : Event):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils",
							"fournirListeMobileCatalogueClient", 
							process_init_data_combo_modele_public);
			RemoteObjectUtil.callService(opData,combo_constructeur.selectedItem.IDFOURNISSEUR,"","MOBILE")
		}
		private function process_init_data_combo_modele_public(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				combo_modele_public.dataProvider = evt.result as ArrayCollection;
			}
			
			
		}	
		
			
		/*--------------Gestion des states----------------------*/
		
		private function init_state_modele(evt : MouseEvent=null):void
		{
			this.setCurrentState("states_modele");
			init_data_combo_modele();
			bt_add_modele.removeEventListener(MouseEvent.CLICK,init_state_modele);
			bt_add_modele.addEventListener(MouseEvent.CLICK,init_state_creerModele);
		}
		private function init_state_creerModele(evt : MouseEvent):void
		{
			this.setCurrentState("states_new_modele");
			//Si on annule la création d'un nouveau libellé modèle
			if(_bool_new_libelle_modele == true)
			{
				_bool_new_libelle_modele = false;
				bt_add_new_libelle_modele.removeEventListener(MouseEvent.CLICK,init_state_creerModele);
			}
			else
			{
				combo_constructeur.dataProvider = _data_constructeur;
			}
			//Le bouton pour créer un modèle change de rôle et permet de revenir à l'état initial
			bt_add_modele.removeEventListener(MouseEvent.CLICK,init_state_creerModele);
			bt_add_modele.addEventListener(MouseEvent.CLICK,init_state_modele);
			bt_save_new_modele.addEventListener(MouseEvent.CLICK,saveNewModele);
			bt_add_new_libelle_modele.addEventListener(MouseEvent.CLICK,init_state_add_libelle_modele);
			
			combo_constructeur.addEventListener(Event.CHANGE,init_data_combo_modele_public);
			init_data_combo_modele_public(null);
			
			
		}
		private function init_state_add_libelle_modele(evt : MouseEvent=null):void
		{
			this.setCurrentState("states_add_libelle_modele");
			input_libelle_modele.text="";
			bt_add_new_libelle_modele.removeEventListener(MouseEvent.CLICK,init_state_modele);
			bt_add_new_libelle_modele.addEventListener(MouseEvent.CLICK,init_state_creerModele);
			_bool_new_libelle_modele = true;
			
		}
		
		private function saveNewModele(evt : MouseEvent):void
		{
			//Si ça n'est pas un nouveau modèle :
			if(_bool_new_libelle_modele == false)
			{
				if(combo_constructeur.selectedIndex == -1 || combo_revendeur.selectedIndex==-1 || combo_modele_public.selectedIndex==-1)
				{
					Alert.show("Sélectionnez un revendeur et un modèle");
				}
				else
				{
					var opData: AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
											"fr.consotel.consoview.inventaire.equipement.equipement",
							"insertModeleRevendeur", processInsertEquip);
							RemoteObjectUtil.callService(opData,combo_modele_public.selectedItem.IDEQUIPEMENT_FOURNIS,combo_revendeur.selectedItem.IDREVENDEUR); 
				}
			}
			else // Si c'est un nouveau modèle : 
			{
				if(combo_constructeur.selectedIndex == -1 || combo_revendeur.selectedIndex==-1 || input_libelle_modele.text.length ==0)
				{
					Alert.show("Sélectionnez un revendeur et un constructeur puis entrez le libellé du nouveau modèle");
				}
				else
				{
					var opData_new_modele: AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
											"fr.consotel.consoview.inventaire.equipement.equipement",
							"insertEquipementAllCatalogue", processInsertEquip);
							
							RemoteObjectUtil.callService(opData_new_modele,combo_constructeur.selectedItem.IDFOURNISSEUR,input_libelle_modele.text,combo_revendeur.selectedItem.IDREVENDEUR); 
							/* 
							Dans le backOffice : Le nouveau modèle est enregistré avec les parametres suivant :
							
							70,//Type = mobile 
							0, //p_idgamme_fab
							input_libelle_modele,//Reférence
							"",//Reference 
							"", //CODE INTERNE
							"" //Commentaire
							*/
				}
			}
		}
		private function processInsertEquip(evt : ResultEvent):void
		{
			ConsoviewAlert.afficherOKImage("Nouveau modèle ajouté");
					
			//Permet de selectionnez le nouveau modèle : 
			_selected_new_modele = evt.result as Number;
			init_state_modele();
		}
		
		
	}
}