/**
 *Cette classe est utilisée pour supprimer des pools, des sites, et des profils. 
 * */
package univers.parametres.poolgestionnaire.ihm
{
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.managers.PopUpManager;
	
	import univers.parametres.poolgestionnaire.event.ContrainteSuppressionEvent;
	import univers.parametres.poolgestionnaire.event.GestionPoolEvent;
	[Bindable]
	public class ContrainteSuppression extends TitleWindow
	{
		public var listeContraintes : ArrayCollection ;
			
		public var messageDecription : String;
		public var messageConfirmation : String;
		
		public function ContrainteSuppression()
		{
			
		}
		public function valider():void
		{
			dispatchEvent(new ContrainteSuppressionEvent(ContrainteSuppressionEvent.VALIDATE_SUPPRESSION_POOL));
			fermer();
	
		}
		public function fermer():void
		{
			PopUpManager.removePopUp(this);
		}
	}
}