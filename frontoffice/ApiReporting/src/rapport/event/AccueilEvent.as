package rapport.event
{
	import flash.events.Event;

	public class AccueilEvent extends Event
	{
		public static const ACCUEIL_EVENT:String = 'accueilEvent';
		public static const PERIMETRE_CHANGE:String = 'perimetreChange';
		public static const PERIMETRE_NOT_CHANGE:String = 'perimetreNotChange';				
		public static const PERIMETRE_ERROR:String = 'perimetreError';
		
		public function AccueilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		override public function clone():Event
		{
			return new AccueilEvent(type,bubbles,cancelable);
		}
	}
}