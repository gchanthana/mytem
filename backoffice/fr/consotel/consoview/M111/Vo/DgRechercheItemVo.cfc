<cfcomponent output="false" alias="fr.consotel.consoview.M111.Vo.DgRechercheItemVo">
	
	<cfproperty name="DATE_RENOUVELLEMENT"			type="Date" default="">
	<cfproperty name="DATE_FPC"						type="Date" default="">
	<cfproperty name="DATE_LAST_FACTURE"			type="Date" default="">
	<cfproperty name="DATE_ELLIGIBILITE"			type="Date" default="">
	<cfproperty name="DATE_FIN_GARANTIE"			type="Date" default="">
	<cfproperty name="DATE_COMMANDE"				type="Date" default="">
	<cfproperty name="DATE_RESILIATION"				type="Date" default="">
	<cfproperty name="DATE_DEBUT"		 			type="Date" default="">
	<cfproperty name="DATE_DERNIER_RACCORD"			type="Date" default="">
	<cfproperty name="DATE_OUVERTURE"		 		type="Date" default="">
	<cfproperty name="DATE_SORTIE"					type="Date" default="">
	<cfproperty name="DATE_ENTREE"		 			type="Date" default="">
	<cfproperty name="DATE_CREATION_RACCORDEMENT"	type="Date" default="">
	
	<cfproperty name="MARQUE" 						type="String" default=""> 
	<cfproperty name="SITE_EQP" 					type="String" default=""> 
	<cfproperty name="VALEUR" 						type="String" default=""> 
	<cfproperty name="CODE_INTERNE" 				type="String" default=""> 
	<cfproperty name="NOM_SITE" 					type="String" default=""> 
	<cfproperty name="COLLABORATEUR" 				type="String" default="">
	<cfproperty name="AMORCE"						type="String" default="">
	<cfproperty name="COMPTE_FACTURATION"			type="String" default="">
	<cfproperty name="NIVEAU_COLLABORATEUR"			type="String" default="">
	<cfproperty name="TYPE"		 					type="String" default="">
	<cfproperty name="REFERENCE_EMPLOYE"			type="String" default="">
	<cfproperty name="CATEGORIE"		 			type="String" default="">
	<cfproperty name="AFFECT_LIGNE"		 			type="String" default="">
	<cfproperty name="SEUIL"		 				type="String" default="">
	<cfproperty name="USAGE_LIGNE"		 			type="String" default="">
	<cfproperty name="MODELE"		 				type="String" default="">
	<cfproperty name="LIBELLE_TYPE_LIGNE"			type="String" default=""> 
	<cfproperty name="MATRICULE"					type="String" default="">
	<cfproperty name="CONTRAT"		 				type="String" default="">
	<cfproperty name="SERIAL_NUMBER"		 		type="String" default="">
	<cfproperty name="ETAT_LIGNE"					type="String" default="">
	<cfproperty name="ETAGE"		 				type="String" default="">
	<cfproperty name="LIBELLE_GROUPE_CLIENT"		type="String" default="">
	<cfproperty name="TYPE_FICHELIGNE"				type="String" default="">
	<cfproperty name="IDENTIFIANT_EMPLOYE"			type="String" default="">
	<cfproperty name="ETAT_SIM"		 				type="String" default="">
	<cfproperty name="TICKET"						type="String" default="">
	<cfproperty name="CONTENU"		 				type="String" default="">
	<cfproperty name="EMAIL"		 				type="String" default="">
	<cfproperty name="ETAT"							type="String" default="">
	<cfproperty name="SALLE"		 				type="String" default="">
	<cfproperty name="CONSOS_NETTES"		 		type="String" default="">
	<cfproperty name="ORGA_CLIENT"		 			type="String" default="">
	<cfproperty name="TYPE_ACCES"					type="String" default="">
	<cfproperty name="VOL_MN"		 				type="String" default="">
	<cfproperty name="P1"		 					type="String" default="">
	<cfproperty name="ZDM_IMEI"		 				type="String" default="">
	<cfproperty name="P2"		 					type="String" default="">
	<cfproperty name="ABO_PRINCIPAL"		 		type="String" default="">
	<cfproperty name="OPE_ALTERNATIF"		 		type="String" default="">
	<cfproperty name="OPERATEUR"					type="String" default="">
	<cfproperty name="ETAT_IMEI"		 			type="String" default="">
	<cfproperty name="BATIMENT"		 				type="String" default="">
	<cfproperty name="LIGNE"		 				type="String" default="">
	<cfproperty name="NIVEAU"		 				type="String" default="">
	<cfproperty name="TOTAL"		 				type="String" default="">
	<cfproperty name="COMMENTAIRES"		 			type="String" default="">
	<cfproperty name="DISTRIBUTEUR"		 			type="String" default="">
	<cfproperty name="USAGE"						type="String" default="">
	<cfproperty name="NIVEAU_TERMINAL"				type="String" default="">
	<cfproperty name="NUM_SIM"		 				type="String" default="">
	<cfproperty name="VAR_M1"		 				type="String" default="">
	<cfproperty name="ETAT_COLLABORATEUR"			type="String" default="">
	<cfproperty name="TYPE_RACCORDEMENT"			type="String" default="">
	<cfproperty name="ALERTE_SUR"		 			type="String" default="">
	<cfproperty name="VOL_KO"		 				type="String" default="">
	<cfproperty name="ELIGIBILITE"					type="String" default="">
	<cfproperty name="OPE_ACCES"		 			type="String" default="">
	<cfproperty name="LIGNE_TETE"		 			type="String" default="">
	<cfproperty name="TERMINAL"		 				type="String" default="">
	<cfproperty name="NOEUD"		 				type="String" default="">
	<cfproperty name="ZDM_SERIAL_NUMBER"			type="String" default="">
	<cfproperty name="TYPE_SEUIL"		 			type="String" default="">
	<cfproperty name="SOUS_COMPTE"		 			type="String" default="">
	<cfproperty name="NUM_CONTRAT_ABO"				type="String" default="">
	<cfproperty name="RESTE"		 				type="String" default="">
	<cfproperty name="MONTANT"						type="String" default="">
	<cfproperty name="IMEI"		 					type="String" default="">
	<cfproperty name="REGLETTE1"		 			type="String" default="">
	<cfproperty name="ABONNEMENT_NET"				type="String" default="">
	<cfproperty name="REGLETTE2"		 			type="String" default="">
	<cfproperty name="LIBELLE_GARANTIE"				type="String" default="">
		
	<cfproperty name="IDREVENDEUR_TERMINAL" 		type="Numeric" default="0">
	<cfproperty name="IDDISTRIBUTEUR" 				type="Numeric" default="0">
	<cfproperty name="NBLIGNESIM" 					type="Numeric" default="0">
	<cfproperty name="PRIX_TERMINAL" 				type="Numeric" default="0">
	<cfproperty name="IDSOUS_COMPTE"				type="Numeric" default="0">
	<cfproperty name="IDOPERATEUR"					type="Numeric" default="0">
	<cfproperty name="IDCONTRAT_ABO"		 		type="Numeric" default="0">
	<cfproperty name="IDTYPE_RACCORDEMENT"			type="Numeric" default="0">
	<cfproperty name="IDTERMINAL"		 			type="Numeric" default="0">
	<cfproperty name="REAL_IDOPERATEUR"				type="Numeric" default="0">
	<cfproperty name="NBSOUSEQP"		 			type="Numeric" default="0">
	<cfproperty name="NBJR_FIN"		 				type="Numeric" default="0">
	<cfproperty name="IDTETE_LIGNE"		 			type="Numeric" default="0">
	<cfproperty name="PRIX"							type="Numeric" default="0">
	<cfproperty name="IDGC_EMPLOYE"		 			type="Numeric" default="0">
	<cfproperty name="IDSOUS_TETE"		 			type="Numeric" default="0">
	<cfproperty name="IDSITE_PHYSIQUE"				type="Numeric" default="0">
	<cfproperty name="OPERATEURID_ACCES"			type="Numeric" default="0">
	<cfproperty name="IDSIM"		 				type="Numeric" default="0">
	<cfproperty name="IDEMPLOYE"		 			type="Numeric" default="0">
	<cfproperty name="IDNOEUD"		 				type="Numeric" default="0">
	<cfproperty name="OPERATEURID_INDIRECT_CIBLE1"	type="Numeric" default="0">
	<cfproperty name="IDCOMPTE_FACTURATION"			type="Numeric" default="0">
	<cfproperty name="ID"		 					type="Numeric" default="0">
	<cfproperty name="IDPRODUIT_CATALOGUE"			type="Numeric" default="0">
	<cfproperty name="INOUT"		 				type="Numeric" default="0">
	<cfproperty name="NB_JRS_FIN_LIGNE"				type="Numeric" default="0">
	<cfproperty name="NBLIGNE"		 				type="Numeric" default="0">
	<cfproperty name="REF_COMMANDE_TERMINAL"		type="Numeric" default="0">
	<cfproperty name="IDCOMMANDE"					type="Numeric" default="0">
	<cfproperty name="NBEQP"		 				type="Numeric" default="0"> 
	<cfproperty name="NBSIM"		 				type="Numeric" default="0">
	
	<cfproperty name="SIM_REBUT" 					type="Boolean" default="false">
	<cfproperty name="TERM_SAV"		 				type="Boolean" default="false">
	<cfproperty name="LIGNE_REBUT"		 			type="Boolean" default="false">
	<cfproperty name="ISLIGNEFIXE"		 			type="Boolean" default="false">
	<cfproperty name="LIGNE_SAV"		 			type="Boolean" default="false">
	<cfproperty name="BOOL_ISMANAGED"		 		type="Boolean" default="false">
	<cfproperty name="BOOL_MDM"		 				type="Boolean" default="false">
	<cfproperty name="ISLIGNEMOBILE"		 		type="Boolean" default="false">
	<cfproperty name="TERM_REBUT"		 			type="Boolean" default="false">
	<cfproperty name="SIM_SAV"		 				type="Boolean" default="false">
	<cfproperty name="SELECTED"		 				type="Boolean" default="false">
	
	<!---
		
		Auteur : nicolas.renel
		
		Date : 26/03/2012
		
		Description : GETTER et SETTER
	
	--->
	
	<cffunction name="init" output="false" returntype="DgRechercheItemVo">
		<cfreturn this>
	</cffunction>
	
	
</cfcomponent>