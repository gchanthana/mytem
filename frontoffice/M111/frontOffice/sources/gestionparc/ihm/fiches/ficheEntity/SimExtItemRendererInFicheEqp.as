package gestionparc.ihm.fiches.ficheEntity
{
	import gestionparc.entity.SpecificationVO;
	
	import mx.containers.HBox;
	import mx.controls.Label;
	import mx.utils.ObjectUtil;

	//https://books.google.fr/books?id=yFNZGjqJe9IC&pg=PA262&lpg=PA262&dq=when+addchild+actionscript&source=bl&ots=oSx8LVAcy1&sig=83RbZidaFtE6p1yI-Qw7C4GMWjc&hl=en&sa=X&ved=0ahUKEwj78ZfZ7sPXAhXMh7QKHQlJDssQ6AEIWzAH#v=onepage&q=when%20addchild%20actionscript&f=false

	public class SimExtItemRendererInFicheEqp extends HBox
	{
		private var lbLibelle:Label;
		private var message:String = "";

		public function SimExtItemRendererInFicheEqp():void
		{
			super();
		}

		override protected function createChildren():void{
			super.createChildren();
			lbLibelle=new Label();
			addChild(lbLibelle);
		}

		override public function set data(value:Object):void
		{
			//lbLibelle.text="";
			message="";
			trace("-------------> SimExtItemRendererInFicheEqp");
			trace(ObjectUtil.toString(value));
			trace("SimExtItemRendererInFicheEqp <-------------");
			
			if (value != null)
			{
				trace("");
				super.data=value;

				if (SpecificationVO.getInstance().elementDataGridSelected.S_IMEI !=null)
				{
					//lbLibelle.text="Sim : " + value.numSerieEquipement;
					//lbLibelle.toolTip=lbLibelle.text;
					message = "Sim : " + value.numSerieEquipement;
				}
				else if (SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO !=null)
				{
					//lbLibelle.text="Ext : " + value.numSerieEquipement;
					//lbLibelle.toolTip=lbLibelle.text;
					message = "Ext : " + value.numSerieEquipement;
				}
			}
			invalidateDisplayList();
		}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if ((lbLibelle != null) && lbLibelle.text)
			{
				lbLibelle.text = message;				
			}
			if ((lbLibelle != null) && lbLibelle.toolTip)
			{
				lbLibelle.toolTip = message;				
			}
		}
	}
}
