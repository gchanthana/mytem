package univers.parametres.parametrelogin.service.user
{
    public class UserDatas
    {
        private var _userObj:Object;

        public function UserDatas()
        {
        }


        public function get userObj():Object
        {
            return _userObj;
        }

        public function set userObj(value:Object):void
        {
            _userObj = value;
        }
    }
}