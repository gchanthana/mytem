package univers.inventaire.equipements.gestionflottemobile.fiches.ihm
{
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	import composants.util.equipements.EquipementsUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.commande.ihm.AbstractBaseViewImpl;
	
	[Bindable]
	public class FicheTerminaleImpl extends AbstractBaseViewImpl
	{
	
		private var _editabel:Boolean;
		
		public var equipementUtils:EquipementsUtils = new EquipementsUtils();
		
		public var cmbDureeGarantie:ComboBox;
		public var dcDateAchat:CvDateChooser;
		public var dcDateFinGarantie:CvDateChooser;
		public var txtJoursRestants:TextInput;
		
		
		public function FicheTerminaleImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, _localeCreationCompleteHandler);
		}		

		public function set selectedTerminal(value:Object):void
		{
			equipementUtils.selectedEquipement = value;
		}

		public function get selectedTerminal():Object
		{
			return equipementUtils.selectedEquipement;
		}
		
		public function set editabel(value:Boolean):void
		{
			_editabel = value;
		}

		public function get editabel():Boolean
		{
			return _editabel;
		}

		private function getData():void
		{	 
			equipementUtils.fournirInfosTerminal(equipementUtils.selectedEquipement.IDTERMINAL);
		}
				
		private function updateData():void
		{	
			if(equipementUtils.tmpValues.IMEI)
			{
				equipementUtils.addEventListener(EquipementsUtils.MAJINFOSTERMINALSUCCED,updateDataHandler);
				equipementUtils.majInfosTerminal(equipementUtils.tmpValues);
			}
			else
			{
				Alert.show("Le numéro d'IMEI est obligatoire","erreur");
			}
		}
		
		private function updateDateFingarantie():void
		{
			try
			{ 
				dcDateFinGarantie.selectedDate = DateFunction.dateAdd("m",dcDateAchat.selectedDate,Number(equipementUtils.tmpValues.DUREE_GARANTIE));
				txtJoursRestants.text = DateFunction.dateDiff("d",dcDateFinGarantie.selectedDate,new Date()).toString();
			}
			catch(e:Error)
			{
					
			}
		}		
//------------- HANDLERS --------------------------------------------------------------------------------------------------------------------
		protected function _localeCreationCompleteHandler(event:FlexEvent):void
		{
			getData();
			cmbDureeGarantie.addEventListener(ListEvent.CHANGE, _cmbDureeGarantieChangeHandler);
			dcDateAchat.addEventListener(CalendarLayoutChangeEvent.CHANGE, _dcDateAchatChangeHandler);
		}
		
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
				
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
	 		
		protected function btValiderClickHandler(event:MouseEvent):void
		{
			updateData();
		}
		
		private function updateDataHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
			dispatchEvent(new FicheEvent(FicheEvent.FICHE_UPDATED,true));
		}

		protected function _cmbDureeGarantieChangeHandler(event:ListEvent):void
		{
			if(cmbDureeGarantie.selectedIndex > -1)
			{
				equipementUtils.tmpValues.DUREE_GARANTIE = cmbDureeGarantie.selectedItem.duree; 	
			}
			else
			{
				cmbDureeGarantie.selectedIndex = 0;
				
				equipementUtils.tmpValues.DUREE_GARANTIE = cmbDureeGarantie.selectedItem.duree;
			}
			
			updateDateFingarantie();
		}

		protected function _dcDateAchatChangeHandler(event:CalendarLayoutChangeEvent):void
		{
			updateDateFingarantie()
		}
	}
}