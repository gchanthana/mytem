package univers.facturation.tb.service
{
	import flash.events.EventDispatcher;
	
	import univers.facturation.tb.event.CheminOrgaEvent;

	public class CheminOrgaModel extends EventDispatcher
	{
		private var _chemin:String;
		
		private var _sous_tete:String;
		
		private var _perimetreRecherche:String;
		
		public function CheminOrgaModel()
		{
			_chemin = '';
		}
		
		public function get sous_tete():String
		{
			return _sous_tete;
		}

		public function set sous_tete(value:String):void
		{
			_sous_tete = value;
		}

		public function get perimetreRecherche():String
		{
			return _perimetreRecherche;
		}

		public function set perimetreRecherche(value:String):void
		{
			_perimetreRecherche = value;
		}

		public function get chemin():String
		{
			return _chemin;
		}
		
		public function set chemin(value:String):void
		{
			_chemin = value;
		}
		
		internal function updateCheminOrga(value:String):void
		{		
			chemin=value;
			
			dispatchEvent(new CheminOrgaEvent(CheminOrgaEvent.ORGA));
		}
	}
}