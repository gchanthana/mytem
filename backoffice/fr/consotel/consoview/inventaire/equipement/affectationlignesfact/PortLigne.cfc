<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.equipement.affectationlignesfact.PortLigne">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	
	<cfproperty name="IDPORT_EQUIPEMENT" type="numeric" default="0">
	<cfproperty name="IDSOUS_TETE" type="numeric" default="0">
	<cfproperty name="IDEQUIPEMENT" type="numeric" default="0">
	<cfproperty name="TYPE_CONNECTEUR" type="string" default="">
	<cfproperty name="SOUS_TETE" type="string" default="">
	<cfproperty name="LIBELLE_EQ" type="string" default="">
	<cfproperty name="TYPE_EQUIPEMENT" type="string" default="">
	<cfproperty name="NUMERO_PORT" type="numeric" default="0">
	<cfproperty name="LIBELLE_PORT" type="string" default="">	
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		this.IDPORT_EQUIPEMENT = 0;		
		this.IDSOUS_TETE = 0;
		this.IDEQUIPEMENT = 0;
		this.TYPE_CONNECTEUR = "";
		this.SOUS_TETE = "";
		this.LIBELLE_EQ = "";		
		this.TYPE_EQUIPEMENT = "";
		this.LIBELLE_PORT = "";
		this.NUMERO_PORT = 0;
	</cfscript>
	
	<cffunction name="getIDPORT_EQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.IDPORT_EQUIPEMENT>
	</cffunction>

	<cffunction name="setIDPORT_EQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDPORT_EQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getIDEQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.IDEQUIPEMENT>
	</cffunction>

	<cffunction name="setIDEQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDEQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDSOUS_TETE" output="false" access="public" returntype="any">
		<cfreturn this.IDSOUS_TETE>
	</cffunction>

	<cffunction name="setIDSOUS_TETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDSOUS_TETE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getSOUS_TETE" output="false" access="public" returntype="any">
		<cfreturn this.SOUS_TETE>
	</cffunction>

	<cffunction name="setSOUS_TETE" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.SOUS_TETE = arguments.val>		
	</cffunction>
	
	<cffunction name="getTYPE_CONNECTEUR" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_CONNECTEUR>
	</cffunction>

	<cffunction name="setTYPE_CONNECTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_CONNECTEUR = arguments.val>		
	</cffunction>

	<cffunction name="getTYPE_EQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_EQUIPEMENT>
	</cffunction>

	<cffunction name="setTYPE_EQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_EQUIPEMENT = arguments.val>		
	</cffunction>
	
		<cffunction name="getLIBELLE_EQ" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_EQ>
	</cffunction>

	<cffunction name="setLIBELLE_EQ" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_EQ = arguments.val>		
	</cffunction>
	
	
	<cffunction name="getNUMERO_PORT" output="false" access="public" returntype="any">
		<cfreturn this.NUMERO_PORT>
	</cffunction>

	<cffunction name="setNUMERO_PORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NUMERO_PORT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getLIBELLE_PORT" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_PORT>
	</cffunction>

	<cffunction name="setLIBELLE_PORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_PORT = arguments.val>		
	</cffunction>


	<cffunction name="save" output="false" access="public" returntype="numeric">		
		<cfscript>
			if((getIDPORT_EQUIPEMENT() gt 0 ) AND (getIDSOUS_TETE() gt 0 )) 
			{	
				return create();
			}else{
				return -15;
			}
		</cfscript>
	</cffunction>

	<cffunction name="create" output="false" access="private" returntype="numeric">
		<cfset var qCreate="">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.add_lign_port">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getIDPORT_EQUIPEMENT()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getIDSOUS_TETE()#">			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="qCcreate">
		</cfstoredproc>
		
		<cfreturn qCcreate>
	</cffunction>

	<cffunction name="delete" output="false" access="public" returntype="numeric">
		<cfset var qDelete="">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.delete_lign_port">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getIDPORT_EQUIPEMENT()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getIDSOUS_TETE()#">			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="qDelete">
		</cfstoredproc>
		<cfreturn qDelete>
	</cffunction>

</cfcomponent>