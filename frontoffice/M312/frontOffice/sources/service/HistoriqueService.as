//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 1, 2011								*
//	owner  : Consotel								*
//***************************************************
package service
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class HistoriqueService
	{
		private const _REMOTE_SERVICE_NAME:String = "fr.consotel.consoview.M312.HistoriqueService";
		
		public static const OP_HISTORIQUE:String = "opHistorique";
		
		//les données
		[Bindable]public var model:HistoriqueModel;
		
		//le handler charger de récupérer la requête
		private var handler:HistoriqueHandler;	
		
		//
		private var _opHistorique:AbstractOperation;
		
		
		public function HistoriqueService()
		{
			model = new HistoriqueModel();
			handler = new HistoriqueHandler(model);
		}
		
		public function getEvolutionSurThemeConso(typeperimetre:String):void
		{
			_opHistorique = RemoteObjectUtil.getSilentOperation(_REMOTE_SERVICE_NAME,"getEvolutionSurThemeConso",handler.getEvolutionSurThemeConsoResultHandler);
			RemoteObjectUtil.callSilentService(_opHistorique,typeperimetre);
		}
		
		public function cancelOperation(idOperation:String = 'all'):void
		{
			var _idOperation:String = '_'+idOperation;
			
			if(_idOperation == '_all')
			{
				if(_opHistorique) _opHistorique.cancel();
			}
			else
			{	
				if(this.hasOwnProperty(_idOperation) && this[_idOperation] is AbstractOperation && this[_idOperation])
				{
					( this[_idOperation] as AbstractOperation).cancel();
				}	
			}	
			
			
		}
		
	}
}