<cfcomponent name="CheminLigne">

	<cffunction name="getCheminLigne" access="remote" returntype="string">
		
		<cfargument name="idSous_tete" type="numeric" required="true">
		
		<cfset idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset idperimetre = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset idDebut = SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB>
		<cfset idFin = SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN >
		
		<cfif idracine neq idperimetre>
			<cfset id_orga=getIdOrga (idperimetre) >
		<cfelse>
			<cfset id_orga=idracine>
		</cfif>
		
		<cfquery datasource="#SESSION.OFFREDSN#" name="chemin">
		
			SELECT e0.pkg_m311.chemin_ligne_v2(#idracine#,#id_orga#,#idDebut#,#idFin#,#idSous_tete#) as chemin FROM DUAL
		
		</cfquery>
		
		<cfset p_retour=chemin['chemin'][1]>
		
		<cfreturn p_retour>
		
	</cffunction>
	
	
	<cffunction name="getIdOrga" returntype="numeric" access="public" output="false">

		<cfargument name="idPerimetre" type="numeric" required="true" >
		
		<cfset valeurIdPerimetre=ARGUMENTS.idPerimetre>     
		
		<cfquery datasource="#SESSION.OFFREDSN#" name="qOrga">
		
			SELECT PKG_CV_RAGO.GET_ORGA(#valeurIdPerimetre#) as IDORGA FROM DUAL
		
		</cfquery>
		
		<cfset id_orag=qOrga['IDORGA'][1]>
		
		<cfreturn id_orag>
	
	</cffunction>

</cfcomponent>