package ihm.destinataire.perimetre
{
    import composants.access.perimetre.PerimetreTreeDataDescriptor;
    import flash.external.ExternalInterface;
    import mx.collections.ICollectionView;
    import mx.collections.XMLListCollection;
    import mx.controls.treeClasses.ITreeDataDescriptor;

    /**
     * Implements a very simple filterable TreeDataDescriptor
     */
    public class FilteredTreeDataDescriptor extends PerimetreTreeDataDescriptor
    {
        private var _filter:Function;

        /**
         * Creates a new FilteredTreeDataDescriptor with the specified filter
         *
         * @param filterFunction    Function that complies with the signature:
         *                             function(node:Object):ICollectionView.
         *                             The function is expected to return the set
         *                             of children to be displayed
         */
        public function FilteredTreeDataDescriptor(filterFunction:Function)
        {
            super();
            _filter = filterFunction;
        }

        /**
         * Applies the filter to the node&apos;s children and returns the result
         */
        public override function getChildren(node:Object, model:Object = null):ICollectionView
        {
            return _filter(node);
        }

        /**
         * True if the the filtered set of children >= 1
         */
        public override function hasChildren(node:Object, model:Object = null):Boolean
        {
            return getChildren(node, model).length > 0;
        }

        /**
         * This method determines whether a folder or file icon is displayed for
         * this node. Here it is a branch if the node has children
         */
        public override function isBranch(node:Object, model:Object = null):Boolean
        {
            var hasChild:int = parseInt(node.@NTY, 10);
            return (hasChild > 0);
        }

        public override function getData(node:Object, model:Object = null):Object
        {
            //Not implemented
            return null;
        }

        public override function addChildAt(parent:Object, newChild:Object, index:int, model:Object = null):Boolean
        {
            //Not implemented
            // return false;
            return true;
        }

        public override function removeChildAt(parent:Object, child:Object, index:int, model:Object = null):Boolean
        {
            //Not implemented
            //return false;
            return true;
        }
    }
}