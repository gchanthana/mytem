<cfcomponent name="savEquipement">
	<cffunction name="createSAV" returntype="numeric" access="remote" output="true">
		
		<cfargument name="idequipement" required="true" type="numeric" />
		<cfargument name="idcontrat" required="true" type="numeric" />
		<cfargument name="date_deb" required="true" type="string" />
		<cfargument name="description" required="true" type="string" />
		<cfargument name="garantie_applicable" required="true" type="numeric" />
		<cfargument name="idfournisseur" required="true" type="numeric" />
		<cfargument name="idsite" required="true" type="numeric" />
		
		<cfargument name="idemploye" required="true" type="numeric" />
		<cfargument name="commentaires" required="true" type="string" />
		<cfargument name="numero_sav" required="true" type="string" />
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset USER = SESSION.USER.CLIENTACCESSID>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.createSAV">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGROUPE_CLIENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idequipement#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcontrat#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_deb#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#description#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#garantie_applicable#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idfournisseur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#USER#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idemploye#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numero_sav#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
	<cffunction name="updateSAV" returntype="numeric" access="remote" output="true">
		<cfargument name="idsav" required="true" type="numeric" />
		<cfargument name="idequipement" required="true" type="numeric" />
		<cfargument name="idcontrat" required="true" type="numeric" />
		<cfargument name="date_deb" required="true" type="string" />
		<cfargument name="description" required="true" type="string" />
		<cfargument name="garantie_applicable" required="true" type="numeric" />
		<cfargument name="idfournisseur" required="true" type="numeric" />
		<cfargument name="idsite" required="true" type="numeric" />
		<cfargument name="commentaires" required="true" type="string" />
		<cfargument name="numero_sav" required="true" type="string" />
		<cfargument name="idemploye" required="true" type="numeric" />
		<cfargument name="fermer" required="true" type="numeric" />
		<cfset USER = SESSION.USER.CLIENTACCESSID>
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.updateSAV">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGROUPE_CLIENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsav#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idequipement#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcontrat#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_deb#" null="false">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#description#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#garantie_applicable#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idfournisseur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsite#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#USER#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmploye#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numero_sav#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#fermer#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="addIntervention" returntype="numeric" access="remote" output="true">
		<cfargument name="idsav" required="true" type="numeric" />
		<cfargument name="idmotif_intervention" required="true" type="numeric" />
		<cfargument name="idtype_intervention" required="true" type="numeric" />
		<cfargument name="libelle_intervention" required="true" type="string" />
		<cfargument name="reference_intervention" required="true" type="string" />
		<cfargument name="code_interne" required="true" type="string" />
		<cfargument name="date_deb" required="true" type="string" />
		<cfargument name="date_fin" required="true" type="string" />
		<cfargument name="duree_inter" required="true" type="numeric" />
		<cfargument name="cout_intervention" required="true" type="numeric" />
		<cfargument name="commentaire" required="true" type="string" />
		<cfargument name="periode" required="true" type="string" />
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.addIntervention">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsav#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idmotif_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idtype_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_deb#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_fin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#duree_inter#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#cout_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#val(periode)#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>  
	
	<cffunction name="updateIntervention" returntype="numeric" access="remote" output="true">
		<cfargument name="idIntervention" required="true" type="numeric" />	
		<cfargument name="idsav" required="true" type="numeric" />
		<cfargument name="idmotif_intervention" required="true" type="numeric" />
		<cfargument name="idtype_intervention" required="true" type="numeric" />
		<cfargument name="libelle_intervention" required="true" type="string" />
		<cfargument name="reference_intervention" required="true" type="string" />
		<cfargument name="code_interne" required="true" type="string" />
		<cfargument name="date_deb" required="true" type="string" />
		<cfargument name="date_fin" required="true" type="string" />
		<cfargument name="duree_inter" required="true" type="numeric" />
		<cfargument name="cout_intervention" required="true" type="numeric" />
		<cfargument name="commentaire" required="true" type="string" />
		<cfargument name="periode" required="true" type="string" />
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.updateIntervention">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIntervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsav#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idmotif_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idtype_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_deb#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_fin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#duree_inter#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#cout_intervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#val(periode)#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction> 
	
	<cffunction name="getTicketNumber" returntype="String" access="remote">
			<cfargument name="idfournisseur" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTicketNumber">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idfournisseur#" null="false">
				<cfprocparam  variable="return_code" cfsqltype="CF_SQL_vARCHAR" type="out">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn return_code>
	</cffunction>
	<cffunction name="getlistTicket" returntype="query" access="remote">
			<cfargument name="idracine" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getlistTicket">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	<cffunction name="getTicketOfEquip" returntype="query" access="remote">
			<cfargument name="idEquip" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTicketOfEquip">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquip#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	<cffunction name="getMotifIntervention" returntype="query" access="remote">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getMotifIntervention">
				
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	<cffunction name="getTypeIntervention" returntype="query" access="remote">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTypeIntervention">
				
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
	<cffunction name="getTicketInfo" returntype="query" access="remote">
			<cfargument name="idTicketSAV" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTicketInfo">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTicketSAV#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
		
</cfcomponent>