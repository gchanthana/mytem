package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.valueobjects
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import flash.events.Event;
	
	import mx.controls.Alert;
	
	public dynamic class Commande extends EventDispatcher
	{
		public static const CREATION_COMPLETE : String = "commandeOk";
		public static const UPDATE_COMPLETE : String = "commandeUpdated";
		public static const SAVE_COMPLETE : String = "commandeSaved";
		public static const LISTE_COMPLETE : String = "commandeListOk";
		public static const DELETE_COMPLETE : String = "commandeDeleted";
		
		public static const CREATION_ERROR : String = "commandeCreateError";
		public static const UPDATE_ERROR : String ="commandeUpdateError";  
		public static const SAVE_ERROR : String = "commandeSaveError";
		public static const LISTE_ERROR : String = "commandeListError";
		public static const DELETE_ERROR : String = "commandeDeleteError";
				
		public static const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande";
  		public static const COLDFUSION_GET : String = "get";
  		public static const COLDFUSION_UPDATE : String = "update";
  		public static const COLDFUSION_CREATE : String = "create";
		public static const COLDFUSION_GETLISTE : String = "getList";
		public static const COLDFUSION_GETUSERLISTE : String = "getUserList";		
		public static const COLDFUSION_DELETE : String = "delete";
		public static const COLDFUSION_UPDATE_STATUT : String ="updateStatut";
								
		private var opGet : AbstractOperation;
		private var opCreate : AbstractOperation;
		private var opUpdate : AbstractOperation;		
		private var opDelete : AbstractOperation;
		private var opList : AbstractOperation;
		
		
		
		public function Commande(commande_ID : Number = 0){
			if (commande_ID != 0){				
				commandeID = commande_ID;
				getCommande(commandeID);
			}
		}
		
		//
		public function clean():void{			
			if (opGet != null) opGet.cancel();
			if (opCreate != null) opCreate.cancel();							
			if (opUpdate != null) opUpdate.cancel();
			if (opDelete != null) opDelete.cancel();
			if (opList != null) opList.cancel();
		}
		
		private var  _elementsCommande : ArrayCollection;
		
		public function get elementsCommande():ArrayCollection{
			return _elementsCommande;
		}		
		public function set elementsCommande(elements : ArrayCollection):void{
			_elementsCommande = elements;
		}		
						
		//identifiant de la commande
		private var _commandeID : Number;
		public function get commandeID():Number{
			return _commandeID;
		}
		public function set commandeID(id : Number):void{
			_commandeID = id;
		}
		
		private var _flagCible : Number = 0;
		public function get flagCible():Number{
			return _flagCible;
		}
		public function set flagCible(id : Number):void{
			_flagCible = id;
		}
		
		private var _typeCommande : Number = -1;
		public function get typeCommande():Number{
			return _typeCommande;
		}
		public function set typeCommande(id : Number):void{
			_typeCommande = id;
		}
				
		//libellé de la commande
		private var _libelle : String ;
		public function get libelle():String{
			return _libelle;
		}
		public function set libelle(l : String):void{
			_libelle = l;
		}
		
		//Référence client de la commande
		private var _refClient : String;
		public function get refClient():String{
			return _refClient;
		}
		public function set refClient(ref : String):void{
			_refClient = ref;
		}
		
		//Référence operateur de la commande
		private var _refOperateur : String;
		public function get refOperateur():String{
			return _refOperateur;
		}
		public function set refOperateur(ref : String):void{
			_refOperateur = ref;
		}
		
		private var _operateurID : Number = -1;
		public function get operateurID ():Number{
			return _operateurID ;
		}
		public function set operateurID(id : Number):void{
			_operateurID = id;
		}
		
		private var _operateurNom : String;
		public function get operateurNom():String{
			return _operateurNom;
		}
		public function set operateurNom(opnom : String):void{
			_operateurNom = opnom;
		}
		
		private var _compteID: Number = -1;
		public function get compteID():Number{
			return _compteID;
		}
		public function set compteID(id : Number):void{
			_compteID = id;
		}
		
		private var _sousCompteID : Number = -1;
		public function get sousCompteID():Number{
			return _sousCompteID;
		}
		public function set sousCompteID(id : Number):void{
			_sousCompteID = id;
		}

				
		//Commentaire de la commande
		private var _commentaire : String;
		public function get commentaire():String{
			return _commentaire;
		}
		public function set commentaire(com : String):void{
			_commentaire = com;
		}
		
		//Date effective de la commande
		private var _dateEffective : String;
		public function get dateEffective():String{
			return _dateEffective;
		}
		public function set dateEffective(da : String):void{
			_dateEffective = da;
		}
		
		//Date de livraison de la commande
		private var _dateLivraison : String;
		public function get dateLivraison():String{
			return _dateLivraison;
		}
		public function set dateLivraison(da : String):void{
			_dateLivraison = da;
		}
		
		//login id du createur de la commande
		private var _idUserCreate : Number = -1;
		public function get idUserCreate ():Number{
			return _idUserCreate ;
		}
		public function set idUserCreate(id : Number):void{
			_idUserCreate = id;
		}
		
		//login id dernier utilisateur ayant modifier la commande
		private var _idUserModif : Number = -1;
		public function get idUserModif ():Number{
			return _idUserModif ;
		}
		public function set idUserModif (id : Number):void{
			_idUserModif  = id;
		}
		
		//date de creation
		private var _dateCreation : String;
		public function get dateCreation ():String{
			return _dateCreation  ;
		}
		
		//date modification
		private var _dateModif : String
		public function get dateModif ():String{
			return _dateModif;
		}
		
		//id du groupe client auxquel appartient la commande
		private var _idGroupeClient : Number = -1;
		public function get idGroupeClient ():Number{
			return _idGroupeClient ;
		}
		public function set idGroupeClient (id : Number):void{
			_idGroupeClient = id;
		}
		
		private var _idGroupeCible : Number = -1;
		public function get idGroupeCible():Number{
			return idGroupeCible;
		}		
		public function set idGroupeCible(id : Number):void{
			_idGroupeCible = id;
		}
		
		private var _idGroupeCibleLibelle : String;
		public function get idGroupeCibleLibelle():String{
			return idGroupeCibleLibelle;
		}
		public function set idGroupeCibleLibelle(libelle : String):void{
			_idGroupeCibleLibelle = libelle;
		}
		
		private var _statutID : Number = -1;
		public function get statutID ():Number{
			return _statutID;
		}		
		public function set statutID (st : Number):void{			
			_statutID  = st;
			switch(_statutID){
				case 1 :  statut = "Annulée";break;
				case 2 : statut = "En cours";break;
				case 3 : statut = "Livrée";break;
				case 4 : statut = "Rapprochée";break;
			}
		}
		
		private var _contactID : Number;
		public function get contactID():Number{
			return _contactID;
		}
		public function set contactID(c : Number):void{
			_contactID = c;
		}
		
		private var _societeID : Number;
		public function get societeID():Number{
			return _societeID;
		}		
		public function set societeID(id : Number):void{			
			_societeID = id;
	
		}
		
		private var _statut : String;
		public function get statut():String{
			return _statut;
		}
		public function set statut( s : String ):void{
			_statut = s;
		}
		
		//------------------ REMOTING -------------
						
		//---------------------------------------------------------/
		
		private function fill(value : Object):void{				
				contactID = value.IDCDE_CONTACT;			
				dateEffective = value.DATE_COMMANDE; 
				dateLivraison = value.DATE_LIVRAISON;															
				idGroupeCible = value.IDGROUPE_REPERE;
				idGroupeCibleLibelle = value.GROUPE_LIBELLE;			
				idGroupeClient = value.IDGROUPE_CLIENT;
				idUserCreate = value.USER_CREATE;
				idUserModif = value.USER_MODIF;
				libelle = value.LIBELLE_COMMANDE;
				refClient = value.REF_CLIENT;			
				refOperateur = value.REF_OPERATEUR;						
				contactID = value.IDCDE_CONTACT;
				societeID = value.IDCDE_CDE_DETAIL;			
				statutID = value.IDCDE_STATUS_CDE;			
		}
		
		///---------------- REMOTING ------------------------------/
		private function getCommande(id : Number):void{
			opGet = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_GET,
													getCommandeResultHandler,null);
			
			trace(Commande.COLDFUSION_COMPONENT,Commande.COLDFUSION_GET,id);
			RemoteObjectUtil.callService(opGet,id);
		}
		
		private function getCommandeResultHandler(re : ResultEvent):void{
			
			if (re.result){							
				fill(re.result[0]);				
				dispatchEvent(new Event(Commande.CREATION_COMPLETE));				
			}else{
				dispatchEvent(new Event(Commande.CREATION_ERROR));
			}
		}
		
		//--------	
		public function deleteCommande(id : Number):void{
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_DELETE,
													deleteCommandeResultHandler,null);

			RemoteObjectUtil.callService(opDelete,id);
		}
		
		private function deleteCommandeResultHandler(re : ResultEvent):void{
			if (re.result > 0){							
				dispatchEvent(new Event(Commande.DELETE_COMPLETE));				
			}else{
				dispatchEvent(new Event(Commande.DELETE_ERROR));
			}			
		}
		
		//--------					
		public function updateCommande():void{
			//if (opUpdate != null) opUpdate.cancel(); 
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_UPDATE,
													updateCommandeResultHandler,null);

			RemoteObjectUtil.callService(opUpdate,this);						
		}	
		
		private function updateCommandeResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				dispatchEvent(new Event(Commande.UPDATE_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.UPDATE_ERROR));
			}
		}
		
		public function updateStatut():void{
			//if (opUpdate != null) opUpdate.cancel(); 
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_UPDATE_STATUT,
													updateStatutResultHandler,null);

			RemoteObjectUtil.callService(opUpdate,this);						
		}	
		
		private function updateStatutResultHandler(re : ResultEvent):void{
			if (re.result > 0){				
				dispatchEvent(new Event(Commande.UPDATE_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.UPDATE_ERROR));
			}
		}
		
		//----------
		public function sauvegarder(idgroupe : Number):void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_CREATE,
													sauvegarderResultHandler,null);

			RemoteObjectUtil.callService(opCreate,idgroupe,this);					 											
		}		
		
		private function sauvegarderResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				commandeID = Number(re.result);				
				dispatchEvent(new Event(Commande.SAVE_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.SAVE_ERROR));
			}			
		}
						
//---------LISTE---------------------------------------------------------------------------		
		private var _liste : ArrayCollection;
		private function setliste(values : ArrayCollection):void{
			var ligne : Object;
			_liste = new ArrayCollection();
			for (ligne in values){
				var cmde : Commande = new Commande();
				cmde.idCommande = values[ligne].IDCDE_COMMANDE;
				cmde.commentaire = values[ligne].COMMENTAIRES;				
				cmde.statut = values[ligne].STATUS;					
				_liste.addItem(cmde);			
			}			
		}
		
		public function get liste():ArrayCollection{
			return _liste;
		} 
		
		public function prepareList(idGpe : Number):void{
			if (opList != null) opList.cancel();
			
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_GETLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		public function  prepareUserList(idGpe : Number):void{			
			if (opList != null) opList.cancel();			
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_GETUSERLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		private function getListResultHandler(re : ResultEvent):void{
			try{
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(Commande.LISTE_COMPLETE));
			}catch(e : Error){
				dispatchEvent(new Event(Commande.LISTE_ERROR));
			}			
		}
	}
}