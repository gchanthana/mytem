package rapport.vo
{
	
	public class ModuleCV
	{
	
		private var _code_module:String;
		private var _nom_module:String;
		private var _module_consotelid:int;

		
		public function ModuleCV(){}
		
		/*
		** permet de construire le Module CV
		** param : moduleCV un objet de type ModuleCV
		** retour : non 
		*/
		public function fill(moduleCV:Object):void
		{
			this._code_module=moduleCV.CODE_MODULE;
			this._nom_module=moduleCV.NOM_MODULE;
			this._module_consotelid=moduleCV.MODULE_CONSOTELID;
		}
		
		public function get module_consotelid():int
		{
			return _module_consotelid;
		}
		
		public function set module_consotelid(value:int):void
		{
			_module_consotelid = value;
		}
		
		public function get nom_module():String
		{
			return _nom_module;
		}

		public function set nom_module(value:String):void
		{
			_nom_module = value;
		}
		
		public function get code_module():String
		{
			return _code_module;
		}
		
		public function set code_module(value:String):void
		{
			_code_module = value;
		}
		
	}
}// fin class 