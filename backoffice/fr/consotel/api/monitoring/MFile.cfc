<cfcomponent output="false">

	<cfproperty name="fileName" displayname="fileName" hint="Nom du fichier à insérer" type="string" />
	<cfproperty name="path" displayname="path" hint="emplacement du fichier" type="string" />

	<cffunction name="getFileName" access="public" output="false" returntype="string">
		<cfreturn this.fileName />
	</cffunction>

	<cffunction name="setFileName" access="public" output="false" returntype="void">
		<cfargument name="fileName" type="string" required="true" />
		<cfset this.fileName = arguments.fileName />
		<cfreturn />
	</cffunction>

	<cffunction name="getPath" access="public" output="false" returntype="string">
		<cfreturn this.path />
	</cffunction>

	<cffunction name="setPath" access="public" output="false" returntype="void">
		<cfargument name="path" type="string" required="true" />
		<cfset this.path = arguments.path />
		<cfreturn />
	</cffunction>
</cfcomponent>