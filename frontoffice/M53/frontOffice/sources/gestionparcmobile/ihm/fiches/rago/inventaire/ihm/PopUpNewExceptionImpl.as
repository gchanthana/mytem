package univers.parametres.rago.inventaire.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.rago.CreationRegles.ihm.destinataireOrga;
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.entity.Rules;
	
	[Bindable]
	public class PopUpNewExceptionImpl  extends HBox
	{
		
		private var _selectedRegle:Regle;
		private var _rules:Rules = new Rules();
		
		public var btAdd:Button;
		public var btSupp:Button;
		
		public var searchabletreeihm2:destinataireOrga = new destinataireOrga();
		public var listeExceptions:List;
		public var txtFiltre:TextInput;
		public var txtFiltreSelected:TextInput;
		public var dgListSelectExceptions:DataGrid;
		public var dgListSelectedExceptions:DataGrid;
		public var nbrExceptionSelect:Label;
		public var nbrExceptionSelected:Label;
		public var checkBoxSelectAll:CheckBox;
		public var checkBoxSelectedAll:CheckBox;
		
		private var noeudException:Object;
		private var _libelleException:ArrayCollection;
		private var _listChildException:ArrayCollection;
		private var _arrayRemoveDatagrid:ArrayCollection = new ArrayCollection();
		private var _listException:ArrayCollection = new ArrayCollection();
		public var _arrayCollectionSelected:ArrayCollection = new ArrayCollection();
		
		public var referenceException:ArrayCollection = new ArrayCollection();
		public var eraseReferenceExceptionInDatabase:ArrayCollection = new ArrayCollection();
		
		private var _arrayRemoveIndexDatagrid:Array = new Array();
		private var _libelleSourceExcptionSearch:ArrayCollection;
		public var _nbrException:int;
		
		private var tempArrayForNodeOrNot:ArrayCollection = new ArrayCollection();
		private var idOldOrga:int;
		private var _arrayOrga:ArrayCollection = new ArrayCollection();
		private var _executeTreeForArray:ArrayCollection = new ArrayCollection();
		private var _dataToAdd:ArrayCollection = new ArrayCollection();
		public var thisIsAFeuille:Boolean = false;
		
		
		public function set dataToAdd(value:ArrayCollection):void
		{ _dataToAdd = value; }

		public function get dataToAdd():ArrayCollection
		{ return _dataToAdd; }
		
		
		public function PopUpNewExceptionImpl()
		{ 
			searchabletreeihm2.addEventListener("finishToRefresh",search);
			addEventListener("finishToRefresh",search);
			addEventListener("thisNodeIsAnExceptionSelect", thisNodeIsAnExceptionSelect);
			addEventListener("thisNodeIsNotAnExceptionSelect", thisNodeIsNotAnExceptionSelect);
			addEventListener("thisNodeIsAnExceptionSelected", thisNodeIsAnExceptionSelected);
			addEventListener("thisNodeIsNotAnExceptionSelected", thisNodeIsNotAnExceptionSelected);
			chargeDataToDatagrid();			
		}

		private function chargeDataToDatagrid():void
		{
			_rules.fillOrganisation();
			_rules.addEventListener("OrgaFind",executeListOrga);
		}
		
		public function checkIfAnodeOrNot(thisRegle:Regle):void
		{
			SelectedRegle = thisRegle;
//			_rules.fillOrganisation();
//			_rules.addEventListener("OrgaFind",executeListNewOrga);
		}
		
		private function executeListNewOrga(event:Event):void
		{
//			arrayOrga = _rules.ListOrgaOpe;
//			findOldValue();
//			tempArrayForNodeOrNot.addItem(searchabletreeihm2.treeDataArray);
//			search(event);
		}
		
		private function executeListOrga(event:Event):void
		{
			if(SelectedRegle!=null)
			{
				if(SelectedRegle.TYPE_REGLE == "ANU")
				{ arrayOrga = _rules.ListOrgaAnu;}
				else
				{ arrayOrga = _rules.ListOrgaOpe;}
				findOldValue();
			}
		}
		
		private function findOldValue():void
		{
			for(var i:int = 0;i < arrayOrga.length;i++)
			{
				if(arrayOrga[i].LIBELLE_GROUPE_CLIENT == SelectedRegle.ORGA_SOURCE)
				{ idOldOrga = arrayOrga[i].IDGROUPE_CLIENT }
			}
			if(idOldOrga!=0)
			{
				callLater(clearSearchabletreeihm2,[idOldOrga]);
			}
		}
		
		private function clearSearchabletreeihm2(ido:Number):void
		{
			 
			searchabletreeihm2.clearSearch();
			searchabletreeihm2._selectedOrga = ido;
			searchabletreeihm2.refreshTree();
		 
		}
		
		public function set libelleSourceExcptionSearch(value:ArrayCollection):void
		{ _libelleSourceExcptionSearch= value; }

		public function get libelleSourceExcptionSearch():ArrayCollection
		{ return _libelleSourceExcptionSearch; }

		public function set arrayOrga(value:ArrayCollection):void
		{ _arrayOrga = value; }

		public function get arrayOrga():ArrayCollection
		{ return _arrayOrga; }

		public function set libelleException(value:ArrayCollection):void
		{ _libelleException = value; }

		public function get libelleException():ArrayCollection
		{ return _libelleException; }
		
		public function set listChildException(value:ArrayCollection):void
		{ _listChildException = value; }

		public function get listChildException():ArrayCollection
		{ return _listChildException; }
		
		public function set listException(value:ArrayCollection):void
		{ _listException = value; }

		public function get listException():ArrayCollection
		{ return _listException; }
		
		private function set arrayRemoveDatagrid(value:ArrayCollection):void
		{ _arrayRemoveDatagrid = value; }

		private function get arrayRemoveDatagrid():ArrayCollection
		{ return _arrayRemoveDatagrid; }
		
		private function set arrayRemoveIndexDatagrid(value:Array):void
		{ _arrayRemoveIndexDatagrid = value; }

		private function get arrayRemoveIndexDatagrid():Array
		{ return _arrayRemoveIndexDatagrid; }

		//Recupère les propriétées de la règle en cours
		public function get SelectedRegle(): Regle
		{ return _selectedRegle; }
		
		public function set SelectedRegle(value : Regle):void
		{ _selectedRegle = value; }
		
		protected function btValiderClickHandler(event:Event):void
		{ 
			if(dgListSelectedExceptions.dataProvider != null && dgListSelectedExceptions.dataProvider.length !=0)
			{
				try{ SelectedRegle.LIST_EXCEPTION = dgListSelectedExceptions.dataProvider as ArrayCollection; }
				catch(error:Error){}

				if(SelectedRegle.LIST_EXCEPTION != null)
				{
					if(SelectedRegle.LIST_EXCEPTION.length !=0)
					{
						var ListExcep:Array = new Array();
						var obj:Object = SelectedRegle.LIST_EXCEPTION;
						
						for(var i:int = 0;i<dgListSelectedExceptions.dataProvider.length;i++)
						{ ListExcep[i] = dgListSelectedExceptions.dataProvider[i].VALUE.toString(); 
							
						}
						for(i = 0; i < SelectedRegle.LIST_EXCEPTION.length; i++)
						{
							SelectedRegle.LIST_EXCEPTION[i].LIBELLE_GROUPE_CLIENT = dgListSelectedExceptions.dataProvider[i].LABEL;
						}
				
						_rules.IURulesOrganisationException(SelectedRegle.IDREGLE_ORGA,ListExcep.length,1,ListExcep);
					}
				}
				_rules.addEventListener("IUOrgaException",executeSendNewExceptions);
			}
			else
			{ PopUpManager.removePopUp(this); }
		}
		
		private function executeSendNewExceptions(event:Event):void
		{
//			Alert.show("Vos nouvelles exceptions ont été envoyées.","CONSOVIEW");
			
			ConsoviewAlert.afficherOKImage("Vos nouvelles exceptions ont été envoyées.",this);
			dispatchEvent(new Event("newExceptionIsCreated",true));
			PopUpManager.removePopUp(this); 
		}
		
		protected function Close(event:CloseEvent):void
		{ PopUpManager.removePopUp(this); }
		
		protected function _btAddClickHandler(event:MouseEvent):void
		{ executeBtnAdd(); }

		protected function _btSuppClickHandler(event:MouseEvent):void
		{ executeBtnSupp(); }
		
		protected function btAnnulerClickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function searchAll():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			
			
			//recupere le dossier maitre
			var parents:XML = _executeTreeForArray[0] as XML;
			//Recupere le dossier parents
			var mere:XMLList = parents.children();
			var obj:Object = new Object();
			
			
//				dataFound.addItem(parents);
//				
//				obj.VALUE = parents.@VALUE;
//				obj.NIV = parents.@NIV;
//				obj.LABEL = parents.@LABEL;
//				dataFoundConcat.addItem(obj);
				
//				_nbrOfStage = 1;
			
			for(var i:int = 0;i < mere.length();i++)
			{	
				obj = new Object();
				dataFound.addItem(mere[i]);
				
				obj.VALUE = mere[i].@VALUE;
				obj.NIV = mere[i].@NIV;
				obj.LABEL = mere[i].@LABEL;
				dataFoundConcat.addItem(obj);
				
//				_nbrOfStage = 2;
				
				var enfants0:XMLList = mere[i].children();
				for(var j:int = 0;j < enfants0.length();j++)
				{	
					obj = new Object();
					dataFound.addItem(enfants0[j]);
					
					obj.VALUE = enfants0[j].@VALUE;
					obj.NIV = enfants0[j].@NIV;
					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
					dataFoundConcat.addItem(obj);
//					_nbrOfStage =  3;
					var enfants1:XMLList = enfants0[j].children();
					for(var k:int = 0;k < enfants1.length();k++)
					{
						obj = new Object();
						dataFound.addItem(enfants1[k]);
						
						obj.VALUE = enfants1[k].@VALUE;
						obj.NIV = enfants1[k].@NIV;
						obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
						dataFoundConcat.addItem(obj);
//						_nbrOfStage =  4;
						var enfants2:XMLList = enfants1[k].children();
						for(var l:int = 0;l < enfants2.length();l++)
						{
							obj = new Object();
							dataFound.addItem(enfants2[l]);
							
							obj.VALUE = enfants2[l].@VALUE;
							obj.NIV = enfants2[l].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
							dataFoundConcat.addItem(obj);
//							_nbrOfStage =  5;
							var enfants3:XMLList = enfants2.children();
							for(var m:int = 0;m < enfants3.length();m++)
							{
								obj = new Object();
								dataFound.addItem(enfants3[m]);
								
								obj.VALUE = enfants3[m].@VALUE;
								obj.NIV = enfants3[m].@NIV;
								obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
								dataFoundConcat.addItem(obj);
//								_nbrOfStage = 6;
								var enfants4:XMLList = enfants3.children();
								for(var n:int = 0;n < enfants4.length();n++)
								{
									obj = new Object();
									dataFound.addItem(enfants4[n]);
									
									obj.VALUE = enfants4[n].@VALUE;
									obj.NIV = enfants4[n].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
									dataFoundConcat.addItem(obj);
//									_nbrOfStage = 7;
									var enfants5:XMLList = enfants4.children();
									for(var o:int = 0;o < enfants5.length();o++)
									{
										obj = new Object();
										dataFound.addItem(enfants5[o]);
										
										obj.VALUE = enfants5[o].@VALUE;
										obj.NIV = enfants5[o].@NIV;
										obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
										dataFoundConcat.addItem(obj);
//										_nbrOfStage = 8;
										var enfants6:XMLList = enfants5.children();
										for(var p:int = 0;p < enfants6.length();p++)
										{
											obj = new Object();
											dataFound.addItem(enfants6[o]);
										
											obj.VALUE = enfants6[p].@VALUE;
											obj.NIV = enfants6[p].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
											dataFoundConcat.addItem(obj);
//											_nbrOfStage = 9;	
										}
									}
								}
							}
						}
					}
				
				}
			}
//////////////////////////////////////**************************************\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\CHANGEMENT!!!!!!!
			if(dataFound.length != 0)
			{
				var cptrSelect:int = 0;
				var cptrSelected:int = 0;
				var cptrArraySelect:Array = new Array();
				var cptrArraySelected:Array = new Array();
				
				if(_selectedRegle.LIST_EXCEPTION.length != 0)
				{
					for(var z:int = 0;z < dataFoundConcat.length;z++)
					{
						for(var w:int = 0;w < _selectedRegle.LIST_EXCEPTION.length;w++)
						{
							if(dataFoundConcat[z].LABEL.toString().toLowerCase().indexOf(_selectedRegle.LIST_EXCEPTION[w].LIBELLE_GROUPE_CLIENT.toString().toLowerCase()) != -1)
							{ 
								dataToAdd.addItem(dataFoundConcat[z]);
								referenceException.addItem(dataFoundConcat[z]);
								cptrArraySelected[cptrSelected] = z;
								cptrSelected++;
							}
						}
					}
					cptrSelected = 0;
					for(var y:int = 0; y < dataFoundConcat.length;y++)
					{
						if(y == cptrArraySelected[cptrSelected])
						{
							cptrSelected++;
						}
						else
						{
							_arrayCollectionSelected.addItem(dataFoundConcat[y]);
						}
					}
				}
				else
				{
						_arrayCollectionSelected.addItem(dataFoundConcat[z]);
				}

				dgListSelectExceptions.dataProvider = _arrayCollectionSelected;
				dgListSelectedExceptions.dataProvider = dataToAdd;
//				for(var tempInt:int = 0;tempInt < dataFoundConcat.length;tempInt++)
//				{
//					_arrayCollectionSelected.addItem(dataFoundConcat[tempInt]);////////////////////////////////////////////////////////////////////////////////
//				}
			}
//			libelleException = dataFoundConcat;
			
			libelleException = _arrayCollectionSelected;
			
            nbrExceptionSelect.text = _arrayCollectionSelected.length.toString();
			nbrExceptionSelected.text = dataToAdd.length.toString();
		}
		
		private function search(event:Event):void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();


			var grandParents:XMLListCollection = searchabletreeihm2.treeDataArray[0] as XMLListCollection;
			arrayC.addItem(searchabletreeihm2.treeDataArray[0]);
			//recupere le dossier maitre
			var parents:XML =  searchabletreeihm2.treeDataArray[0] as XML;
			//Recupere le dossier parents

			var obj:Object = new Object();
		try
		{

				var mere:XMLList = parents.children();

			for(var i:int = 0;i < mere.length();i++)
			{	
				obj = new Object();
				dataFound.addItem(mere[i]);
				
				obj.VALUE = mere[i].@VALUE;
				obj.NIV = mere[i].@NIV;
				obj.LABEL = parents.@LABEL + "/" + mere[i].@LABEL;
				dataFoundConcat.addItem(obj);
				

				
				var enfants0:XMLList = mere[i].children();
				for(var j:int = 0;j < enfants0.length();j++)
				{	
					obj = new Object();
					dataFound.addItem(enfants0[j]);
					
					obj.VALUE = enfants0[j].@VALUE;
					obj.NIV = enfants0[j].@NIV;
					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
					dataFoundConcat.addItem(obj);

					var enfants1:XMLList = enfants0[j].children();
					for(var k:int = 0;k < enfants1.length();k++)
					{
						obj = new Object();
						dataFound.addItem(enfants1[k]);
						
						obj.VALUE = enfants1[k].@VALUE;
						obj.NIV = enfants1[k].@NIV;
						obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
						dataFoundConcat.addItem(obj);
			
						var enfants2:XMLList = enfants1[k].children();
						for(var l:int = 0;l < enfants2.length();l++)
						{
							obj = new Object();
							dataFound.addItem(enfants2[l]);
							
							obj.VALUE = enfants2[l].@VALUE;
							obj.NIV = enfants2[l].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
							dataFoundConcat.addItem(obj);
			
							var enfants3:XMLList = enfants2.children();
							for(var m:int = 0;m < enfants3.length();m++)
							{
								obj = new Object();
								dataFound.addItem(enfants3[m]);
								
								obj.VALUE = enfants3[m].@VALUE;
								obj.NIV = enfants3[m].@NIV;
								obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
								dataFoundConcat.addItem(obj);
					
								var enfants4:XMLList = enfants3.children();
								for(var n:int = 0;n < enfants4.length();n++)
								{
									obj = new Object();
									dataFound.addItem(enfants4[n]);
									
									obj.VALUE = enfants4[n].@VALUE;
									obj.NIV = enfants4[n].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
									dataFoundConcat.addItem(obj);
							
									var enfants5:XMLList = enfants4.children();
									for(var o:int = 0;o < enfants5.length();o++)
									{
										obj = new Object();
										dataFound.addItem(enfants5[o]);
										
										obj.VALUE = enfants5[o].@VALUE;
										obj.NIV = enfants5[o].@NIV;
										obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
										dataFoundConcat.addItem(obj);
									
										var enfants6:XMLList = enfants5.children();
										for(var p:int = 0;p < enfants6.length();p++)
										{
											obj = new Object();
											dataFound.addItem(enfants6[o]);
										
											obj.VALUE = enfants6[p].@VALUE;
											obj.NIV = enfants6[p].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
											dataFoundConcat.addItem(obj);
											
										}
									}
								}
							}
						}
					}
				
//				}
			}
			}
			
			if(dataFound.length != 0)
			{
				
				if(searchIfHasSimpleContent())
				{
					thisIsAFeuille = true;
					executeClosePopUp();
				}
				else
				{
//					listChildException = dataFoundConcat;
//					dgListSelectExceptions.dataProvider = dataFoundConcat;
					if(_executeTreeForArray.length != 0)
					{ searchAll(); }
					_nbrException = dataFoundConcat.length;
				}
				
			}
			else
			{
				var newObject:Object = new Object();
				newObject.LABEL = "Non répertoriée";
				dgListSelectExceptions.dataProvider = newObject;
				_nbrException = dataFound.length;
			}
		}
		catch (error:Error){
				// code dans le cas d'une erreur non-spécifique
			}
		}
		
		private function executeClosePopUp():void
		{
			if(thisIsAFeuille)
			{ dispatchEvent(new Event("thisIsAFeuille",true)); }
			else
			{ 
				Alert.show("Le noeud sélectionné est déja une feuille.","CONSOVIEW");
				PopUpManager.removePopUp(this); 
			}
		}
		
		private function searchIfHasSimpleContent():Boolean
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var dataSource:ArrayCollection = new ArrayCollection();
			var dataFound:ArrayCollection = new ArrayCollection();
			var dataFoundConcat:ArrayCollection = new ArrayCollection();
			
			
			libelleSourceExcptionSearch = new ArrayCollection();
			
//			searchabletreeihm2.txtRechercheOrga.text = _strgToSearch.text;
			libelleSourceExcptionSearch.addItem(searchabletreeihm2.treeDataArray[0]);
			
			//recupere le dossier maitre
			var parents:XML = libelleSourceExcptionSearch[0] as XML;
			//Recupere le dossier parents
			var mere:XMLList = parents.children();
			var obj:Object = new Object();
			
			
				dataFound.addItem(parents);
				
				obj.VALUE = parents.@VALUE;
				obj.NIV = parents.@NIV;
				obj.LABEL = parents.@LABEL;
				dataFoundConcat.addItem(obj);
				
			
			for(var i:int = 0;i < mere.length();i++)
			{	
				obj = new Object();
				dataFound.addItem(mere[i]);
				
				obj.VALUE = mere[i].@VALUE;
				obj.NIV = mere[i].@NIV;
				obj.LABEL = mere[i].@LABEL;
				dataFoundConcat.addItem(obj);		
				
				var enfants0:XMLList = mere[i].children();
				for(var j:int = 0;j < enfants0.length();j++)
				{	
					obj = new Object();
					dataFound.addItem(enfants0[j]);
					
					obj.VALUE = enfants0[j].@VALUE;
					obj.NIV = enfants0[j].@NIV;
					obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL;
					dataFoundConcat.addItem(obj);

					var enfants1:XMLList = enfants0[j].children();
					for(var k:int = 0;k < enfants1.length();k++)
					{
						obj = new Object();
						dataFound.addItem(enfants1[k]);
						
						obj.VALUE = enfants1[k].@VALUE;
						obj.NIV = enfants1[k].@NIV;
						obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL;
						dataFoundConcat.addItem(obj);

						var enfants2:XMLList = enfants1[k].children();
						for(var l:int = 0;l < enfants2.length();l++)
						{
							obj = new Object();
							dataFound.addItem(enfants2[l]);
							
							obj.VALUE = enfants2[l].@VALUE;
							obj.NIV = enfants2[l].@NIV;
							obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL;
							dataFoundConcat.addItem(obj);

							var enfants3:XMLList = enfants2.children();
							for(var m:int = 0;m < enfants3.length();m++)
							{
								obj = new Object();
								dataFound.addItem(enfants3[m]);
								
								obj.VALUE = enfants3[m].@VALUE;
								obj.NIV = enfants3[m].@NIV;
								obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL;
								dataFoundConcat.addItem(obj);

								var enfants4:XMLList = enfants3.children();
								for(var n:int = 0;n < enfants4.length();n++)
								{
									obj = new Object();
									dataFound.addItem(enfants4[n]);
									
									obj.VALUE = enfants4[n].@VALUE;
									obj.NIV = enfants4[n].@NIV;
									obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL;
									dataFoundConcat.addItem(obj);

									var enfants5:XMLList = enfants4.children();
									for(var o:int = 0;o < enfants5.length();o++)
									{
										obj = new Object();
										dataFound.addItem(enfants5[o]);
										
										obj.VALUE = enfants5[o].@VALUE;
										obj.NIV = enfants5[o].@NIV;
										obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL;
										dataFoundConcat.addItem(obj);

										var enfants6:XMLList = enfants5.children();
										for(var p:int = 0;p < enfants6.length();p++)
										{
											obj = new Object();
											dataFound.addItem(enfants6[o]);
										
											obj.VALUE = enfants6[p].@VALUE;
											obj.NIV = enfants6[p].@NIV;
											obj.LABEL = mere[i].@LABEL + "/" + enfants0[j].@LABEL + "/" + enfants1[k].@LABEL + "/" + enfants2[l].@LABEL + "/" + enfants3[m].@LABEL + "/" + enfants4[n].@LABEL + "/" + enfants5[o].@LABEL + "/" + enfants6[p].@LABEL;
											dataFoundConcat.addItem(obj);	
										}
									}
								}
							}
						}
					}
				
				}
			}
			var numberOfResultFound:int = 0;
//			var strg:String = SelectedRegle.IDSOURCE.LABEL;
			for(var tp:int;tp < dataFoundConcat.length;tp++)
			{
//				if(dataFoundConcat[tp].LABEL.toString().indexOf(SelectedRegle.LIBELLE_SOURCE) != -1)
//				{
				var strg:String = dataFoundConcat[tp].VALUE.toString();
				if(dataFoundConcat[tp].VALUE == SelectedRegle.IDSOURCE)
				{
					dataFound.addItem(dataFound[tp]);
					numberOfResultFound = tp;
				}
			}
			var bool:Boolean = false;
			try
			{
				_executeTreeForArray.addItem(dataFound[numberOfResultFound]);
//				_executeTreeForArray
				bool = (dataFound[numberOfResultFound] as XML).hasSimpleContent();
			}
			catch(error:Error)
			{  }
			return bool;
		}
		
		//Affiche l'arbre selon la source selectionnée
		public function zListNoeud():void
		{
//			 if(_exceptionWizzard.noeudSource!=null) { executePrintTree(_exceptionWizzard.noeudSource); } 
		}
		
		protected function thisNodeIsAnExceptionSelect(event:Event):void
		{  }
				
		protected function thisNodeIsNotAnExceptionSelect(event:Event):void
		{  }
		
		protected function thisNodeIsAnExceptionSelected(event:Event):void
		{  }
		
		protected function thisNodeIsNotAnExceptionSelected(event:Event):void
		{  }
		
		//Affiche l'arbre selon la source selectionnée
		private function executePrintTree(objectSelected:Object):void
		{ 
			listChildException = new ArrayCollection();
			searchabletreeihm2.fillTree(objectSelected);
			listChildException.addItem(searchabletreeihm2.treeDataArray);
		}

		private function executeBtnAdd():void
		{ removeDatagrid(); }
		
		private function executeBtnSupp():void
		{ addDatagrid(); }
		
		private function removeDatagrid():void
		{
			var _arrayCollectTemp:ArrayCollection = new ArrayCollection();
			var arrayPlace:Array = new Array();
			var cptr:int = 0;
		
			for(var i:int = 0;i < _arrayCollectionSelected.length;i++)
			{ 
				if(_arrayCollectionSelected[i].SELECTED)
				{
					_arrayCollectionSelected[i].SELECTED = false;
					dataToAdd.addItem(_arrayCollectionSelected[i]);
					arrayPlace[arrayPlace.length] = i;
//					_arrayCollectionSelected.removeItemAt(i);
					cptr++;
				}
			}
			
			var cptrRemove:int = 0;
			for(var j:int = 0;j < arrayPlace.length;j++)
			{ 
				var tempCpt:int = arrayPlace[j]-cptrRemove;
				_arrayCollectionSelected.removeItemAt(tempCpt);
				cptrRemove++;
			}
			
			if(cptr > 0)
			{
				dgListSelectExceptions.dataProvider = treatementArrayCollection(_arrayCollectionSelected,_arrayCollectTemp);
				dgListSelectedExceptions.dataProvider = treatementArrayCollection(dataToAdd,_arrayCollectTemp);
			
				_selectedRegle.LIST_EXCEPTION = dataToAdd;
			
				nbrExceptionSelect.text = _arrayCollectionSelected.length.toString();
				nbrExceptionSelected.text = dataToAdd.length.toString();
				checkBoxSelectAll.selected = false;
			}
		}
		
		private function addDatagrid():void
		{	
			var _arrayCollectTemp:ArrayCollection = new ArrayCollection();
			var arrayPlace:Array = new Array();
			var cptr:int = 0;
			
			for(var i:int = 0;i < dataToAdd.length;i++)
			{ 
				if(dataToAdd[i].SELECTED)
				{
					dataToAdd[i].SELECTED = false;
					_arrayCollectionSelected.addItem(dataToAdd[i]);
					arrayPlace[arrayPlace.length] = i;
//					dataToAdd.removeItemAt(i);
					cptr++;
				}
			}
			
			var cptrRemove:int = 0;
			for(var j:int = 0;j < arrayPlace.length;j++)
			{ 
				var tempCpt:int = arrayPlace[j]-cptrRemove;
				dataToAdd.removeItemAt(tempCpt);
				cptrRemove++;
			}
			
			if(cptr > 0)
			{
				dgListSelectExceptions.dataProvider = treatementArrayCollection(_arrayCollectionSelected,_arrayCollectTemp);
				dgListSelectedExceptions.dataProvider = treatementArrayCollection(dataToAdd,_arrayCollectTemp);
			
				_selectedRegle.LIST_EXCEPTION = dataToAdd;
			
				nbrExceptionSelect.text = _arrayCollectionSelected.length.toString();
				nbrExceptionSelected.text = dataToAdd.length.toString();
				checkBoxSelectedAll.selected = false;
			}
		}
		
		private function treatementArrayCollection(array0:ArrayCollection,array1:ArrayCollection):ArrayCollection
		{
			array1 = new ArrayCollection();
			for(var i:int = 0;i < array0.length;i++)
			{
				array0[i].SELECTED = false;
				array1.addItem(array0[i]);
			}
			return array1;
		}
		
		protected function selectAll(event:Event):void
		{
			var _arrayTempCollectionSelect:ArrayCollection = new ArrayCollection();

			dgListSelectExceptions.dataProvider = treatementInverseArrayCollection(_arrayTempCollectionSelect,_arrayCollectionSelected,checkBoxSelectAll.selected);
		}
		
		protected function selectedAll(event:Event):void
		{
			var _arrayTempCollectionSelect:ArrayCollection = new ArrayCollection();

			dgListSelectedExceptions.dataProvider = treatementInverseArrayCollection(_arrayTempCollectionSelect,dataToAdd,checkBoxSelectedAll.selected);
		}
		
		private function treatementInverseArrayCollection(array0:ArrayCollection,array1:ArrayCollection,bool:Boolean):ArrayCollection
		{
			for(var i:int = 0;i < array1.length;i++)
			{
				array1[i].SELECTED = bool;
				array0.addItem(array1[i]);
			}
			return array0;
		}
			
		protected function searchClickHandler(event:Event):void
		{
			if(libelleException != null)
			{
				txtFiltre.text = "";
				dgListSelectExceptions.dataProvider = _arrayCollectionSelected;
				nbrExceptionSelect.text = _arrayCollectionSelected.length.toString();
			}
		}
		
		protected function filtreChangeHandler(event:Event):void
		{
			var arrayCollectionSearch:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0;i<libelleException.length;i++)
			{
				if(_arrayCollectionSelected[i].LABEL.toString().indexOf(txtFiltre.text) != -1)
				{ arrayCollectionSearch.addItem(libelleException[i]); }
			}
			
			if(arrayCollectionSearch.length == 0)
			{  }
			else
			{ dgListSelectExceptions.dataProvider = arrayCollectionSearch; }
			nbrExceptionSelect.text = arrayCollectionSearch.length.toString();
		}
		
		private function listnbrExceptions(arrayC:ArrayCollection):void
		{ _nbrException = arrayC.length; }
	
	
		protected function searchSelectedClickHandler(event:Event):void
		{
			if(dataToAdd != null)
			{
				txtFiltreSelected.text = "";
				dgListSelectedExceptions.dataProvider = dataToAdd;
				nbrExceptionSelected.text = dataToAdd.length.toString();
			}
		}
		
		protected function filtreSelectedChangeHandler(event:Event):void
		{
			var arrayCollectionSearch:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0;i < dataToAdd.length;i++)
			{
				if(dataToAdd[i].LABEL.toString().toLowerCase().indexOf(txtFiltreSelected.text.toLowerCase()) != -1)
				{ arrayCollectionSearch.addItem(dataToAdd[i]); }
			}
			
			if(arrayCollectionSearch.length == 0)
			{  }
			else
			{ dgListSelectedExceptions.dataProvider = arrayCollectionSearch; }
							
			nbrExceptionSelected.text = arrayCollectionSearch.length.toString();
		}

	}

}