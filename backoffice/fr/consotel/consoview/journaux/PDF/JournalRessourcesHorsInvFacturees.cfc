<cfcomponent name="JournalRessourcesHorsInvFacturees">
	<cffunction name="afficher">
		<cfset label="Journal">	
		<cfset sessionObject = createObject("component","fr.consotel.consoview.util.utils")>
		<cfset qGetGrid=sessionObject.getQuery("session.dataInventaire.journal")>	
		<cfset raison_sociale = sessionObject.getString("session.perimetre.RAISON_SOCIALE")>	
		<cfset nom = sessionObject.getString("session.user.nom")>	
		<cfset prenom = sessionObject.getString("session.user.prenom")>	
		<!--- Rapport Flash et pdf --->
		<cfcontent type="application/pdf"> 
		<cfheader name="Content-Disposition" value="inline;filename=ressources_non_facturees.pdf">
		<cfdocument format="pdf" backgroundvisible="yes" fontembed = "yes" unit="CM" margintop="5">							
			<cfoutput> 
				<html>
					<head>									
					</head>									
					<body> 
					<style>
						@page { 
							size:portrait; margin-top:2cm; margin-bottom:2cm; margin-left:1cm; margin-right:1cm;
						 }
						 
						html body {
						  margin		: 0;
						  padding		: 0;
						  background	: ##FFFFFF;
						  font			: x-small Verdana,Georgia, Sans-serif;
						  voice-family	: "\"}\""; voice-family:inherit;
						  font-size:x-small;
						  } html>body {
							font-size	: x-small;
						}
						
						.cadre {
							border-color:##000000;
							border-style:solid;
							border-collapse:collapse;
							
							border-top-width:1px;
							border-left-width:1px;
							border-right-width:1px;
							border-bottom-width:1px;
						}
						
						.cadre1{
							background		: rgb(255,255,255);
							border-bottom	:1px solid rgb(0,0,0);
							border-left		:1px solid rgb(0,0,0);
							border-right	:1px solid rgb(0,0,0);
							border-top		:1px solid rgb(0,0,0);
							border-collapse : collapse;
						}
						
						table .menuconsoview
						{
							border 		: none;
							background	: transparent;
							font		: normal 8px Tahoma,Arial, sans-serif;
							color		: ##005EBB;
						}
						
						.menuconsoview caption{
							background		:##036 ;
							color			: white;
							font			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							padding			: 4px;
							letter-spacing	: 1px;
						
						}
						
						.menuconsoview th{
							background   	:##99CCFF ;
							color        	: ##036;
							font        	: normal 9px EurostileBold,verdana,arial,helvetica,serif;
							padding      	: 3px;
							border-collapse	: collapse;
						}
						
						.menuconsoview .th_bottom{
							vertical-align	: middle;
							padding			: 3px;
							border-bottom	: 1px solid ##808080;
							font			: normal 9px Tahoma,Arial, sans-serif;
							color        	: ##036;
							background		: ##99CCFF;
						}
						
						.menuconsoview .th_top{
							vertical-align	: middle;
							padding			:3px;
							border-top		: 1px solid ##808080;
							font			: normal 9px Tahoma,Arial, sans-serif;
							color       	: ##036;
							background		: ##99CCFF;
						}
						
						.menuconsoview .grid_normal{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 8px Tahoma,Arial, sans-serif;
							padding		: 2px;
						}
						
						.menuconsoview .grid_normal_alt{
							background	: ##eceae6;
							color		: rgb(0 , 0, 0);
							font		: normal 8px Tahoma,Arial, sans-serif;
							padding		: 2px;
						}
						
						.menuconsoview .grid_small{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 8px Tahoma,Arial, sans-serif;
							padding		: 0px;
						}
						
						.menuconsoview .grid_medium {
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 8px Tahoma,Arial, sans-serif;
							padding-top		: 1px;
						}
						
						.menuconsoview .grid_total{
							background	: ##008ACC;
							color		: rgb(0 , 0, 0);
							font		: normal 8px Tahoma,Arial, sans-serif;
							padding-top	: 2px;
						}
						
						.menuconsoview .grid_bold{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: bold 8px Tahoma,Arial, sans-serif;
							padding		: 0px;
						}
						
						/* caption impossible pour liste des produits */
						.td5{
							padding		:5px;
							background	:##036;
							color		:rgb(255,255,255);
							font		:normal 9px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
						}
						
						/*facture*/
						table .FT{
							border 		: none;
							background	: transparent;
							font		: normal 7px Tahoma,Arial, sans-serif;
							color		: ##000000;
						}
						
						.FT .small_header{
							margin-top	: 0px;
							color		: rgb(0 , 0, 0);
							font		: italic 6px Arial, sans-serif;
							padding		: 2px;
							font-style	: italic;
							vertical-align : top;
							text-align	: center;
						}
						
						.FT .big{
							border-top		: 2px solid ##C0C0C0;
							border-bottom	: 1px solid ##C0C0C0;
							margin-top		: 0px;
							color			: rgb(0 , 0, 0);
							font			: 9px Arial, sans-serif;
							padding			: 5px;
						}
						
						.FT .very_big{
							border-top	: 1px solid ##C0C0C0;
							margin-top	: 0px;
							color		: rgb(0 , 0, 0);
							font		: 12px Arial, sans-serif;
							padding		: 6px;
						}
						
						.FT .huge{
							margin-top	: 0px;
							color		: rgb(0 , 0, 0);
							font		: 18px Arial, sans-serif;
							padding		: 2px;
						}
						
						.FT .normal{
							margin-top	: 0px;
							color		: rgb(0 , 0, 0);
							font		: 8px Arial, sans-serif;
							padding		: 2px;
						}
						/*fin facture*/
						
						
						/* resultat (consofacture, redzone)*/
						table .resultat 
						{
							font-size		: 100%;
							border			: 1px solid ##CCCC99;
							background 		: ##F4F2EF;
							border-collapse	: collapse;
							padding-right	: 2;
							padding-left	: 2;
						}
						
						.resultat th{
							background		: ##EDCEA5;
							color			: ##7F2114;
							font			: normal 7px EurostileBold,Verdana,Georgia, Arial,Helvetica,Serif;
							font-weight		: normal; 
							border-collapse	: collapse;
							padding			: 2px;
						}
						
						.resultat td {
							font 			: normal 7px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							color			: ##292820;
							border-collapse	: collapse;
							padding			: 2px;
						}
						
						.resultat .td_clair {
							background		: rgb(241,241,241);
							font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							color			: ##292820;
							border-collapse	: collapse;
							padding			: 1px;
						}
						
						.resultat .td_fonce {
							background		: ##E9E9E9;
							font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							color			: ##292820;
							border-collapse	: collapse;
							padding			: 1px;
						}
						
						.resultat .td_rouge {
							background		: rgb(242,169,41);
							font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							color			: ##292820;
							border-collapse	: collapse;
							padding			: 1px;
						}
						
						.resultat .td_out {
							layout-grid		: both loose 2px 2px;
							background		: ##FFE1E1;
							border			: none;
							color			: rgb(0 , 0, 0);
							font			: normal 6px Tahoma,Arial, sans-serif;
							padding			: 2px;
							border-collapse	: collapse;
							padding			: 1px;
						}
						
						.resultat .grid_small{
							background	: ##FDFDFB;
							color		: rgb(0 , 0, 0);
							font		: normal 8px Tahoma,Arial, sans-serif;
							padding		: 0px;
						}
						/*fin resultat*/
						
						/*ConsoFacture*/
						.titres_gros {
							background		: transparent;
							color			: ##292820;
							font 			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							letter-spacing	: 2px;
							border-collapse	: collapse;
							text-align		: left;
						}
						
						.sous_titres_gros {
							background		: transparent;
							color			: ##292820;
							font 			: normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							border-collapse	: collapse;
							text-align		: left;
						}
						/*fin ConsoFacture*/
						
						/* Logique budgetaire */
						table .logique_budget {
							font-size		:100%;
							padding			: 1px;
							border			: 1px solid black;
							border-collapse	: collapse; 
						}
						
						.logique_budget th {
							font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							padding			: 6px;
							color			: ##000000;
							text-align		: left;
							background		: Silver;
							border			: 1px solid black;
							border-collapse	: collapse;
						}
						
						.logique_budget .Titre {
							font			: normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							padding			: 2px;
							color			: white;
							background		: black;
							border			: 1px solid black;
							border-collapse	: collapse;
						}
						
						.logique_budget td {
							font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							color			: Black;
							border			: 1px solid black;
							background		: white;
							border-collapse	: collapse;
						}
						
						.logique_budget .td_total {
							font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							padding			: 2px;
							color			: black;
							background		: ##E9E9E9;
							border			: 1px solid black;
							border-collapse	: collapse;
						}
						
						.logique_budget .td_alerte {
							font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
							color			: red;
							border			: 1px solid black;
							background		: white;
							border-collapse	: collapse;
						}
					</style>
					<cfdocumentitem type="header">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
							<tr>
								<td align="left" width="40%" height="50"><img src="/fr/consotel/consoview/images/consoview_new.gif" height="40"></td>
								<td width="17%">&nbsp;</td>
								<td width="3%" bgcolor="F4F188">&nbsp;</td>
								<td align="right" bgcolor="FDFCE1" width="40%"><font face="Arial" size="2">Facturation hors inventaire</font></td>
							</tr>
							<tr><td height="40">&nbsp;</td></tr>
							<tr>
								<td colspan="4">
									<font face="Tahoma" size="2"><strong>#raison_sociale#</strong></font><br>
									<font face="Tahoma" size="1">Edité par&nbsp<strong>#prenom# #nom#</strong></font><br>
									<font face="Tahoma" size="1">Le&nbsp;#Lsdateformat(now(), 'dd mmmm yyyy')#</font>
								</td>								
							</tr>
						</table>
					</cfdocumentitem>	
					<cfinclude template="ressourcesHorsInvFacturees.cfm">		 
					<cfdocumentitem type="footer">
						<style>
						.tfooter {
							color: rgb(0 , 0, 0);
							font: normal 11px Tahoma,Arial, sans-serif;
						}
						</style>
						<center>
						<table border="0" cellpadding="0" cellspacing="0" width="560" align="center">
							<tr>
								<td align="center" class="tfooter" width="560">copyright CONSOTEL  2000 / 2007 - page #cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</td>
							</tr>
						</table>
						</center>
					</cfdocumentitem>			
					</body>
				</html> 
			</cfoutput>			
		</cfdocument>		
	</cffunction>
</cfcomponent>