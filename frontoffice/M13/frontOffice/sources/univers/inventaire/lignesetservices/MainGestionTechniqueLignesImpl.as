package univers.inventaire.lignesetservices
{
	import mx.containers.Canvas;
	import mx.containers.ViewStack; 
	import univers.inventaire.lignesetservices.applicatif.GestionTechniqueLignesApp;
	import univers.inventaire.lignesetservices.views.GestionTechniqueView;
	import univers.inventaire.lignesetservices.views.OngletChampsPersoView;
	
	public class MainGestionTechniqueLignesImpl extends Canvas
	{
		[Bindable]
		protected var gestionTechnique : GestionTechniqueLignesApp;			
		
		[Bindable]
		protected var mode : Boolean = false;
		
		public var cmpGestionLignes : GestionTechniqueView;
		public var cmpOngletChampsPersoView : OngletChampsPersoView;
		public var tbnMain : ViewStack;
	
		public function init():void{
			gestionTechnique = new GestionTechniqueLignesApp();
			mode = (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
			tbnMain.selectedChild = cmpGestionLignes;
			cmpGestionLignes.onPerimetreChange();
		}			
		
		public function onPerimetreChange():void{
			gestionTechnique = new GestionTechniqueLignesApp();				
			mode = (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
			tbnMain.selectedChild = cmpGestionLignes;
			cmpGestionLignes.onPerimetreChange();
		}
	}
}