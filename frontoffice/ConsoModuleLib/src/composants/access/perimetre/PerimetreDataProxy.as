package composants.access.perimetre {
	import appli.events.PerimetreDataProxyEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.XMLListCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

	public class PerimetreDataProxy extends EventDispatcher implements IPerimetreDataProxy {
		private var getChildOp:AbstractOperation;
		private var nodeToLoadChildren:XML;
		
		public function PerimetreDataProxy() {
			super();
			nodeToLoadChildren = null;
			getChildOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.M00.ConnectionManager",
																			"getNodeChild",addNodeChildren);
		}
		
		public function loadNodeChildren(node:XML):void {
			nodeToLoadChildren = node;
			RemoteObjectUtil.invokeService(getChildOp,parseInt(nodeToLoadChildren.@NID,10));
		}
		
		private function addNodeChildren(event:Event):void {
			var i:int;
			var childCollection:XMLListCollection =
					new XMLListCollection(((event as ResultEvent).result as XML).children());
			for(i = 0; i < childCollection.length; i++)
				CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList.descendants("*").(@NID == nodeToLoadChildren.@NID).appendChild(childCollection.getItemAt(i));
			nodeToLoadChildren = null;
			dispatchEvent(new PerimetreDataProxyEvent(PerimetreDataProxyEvent.NODE_CHILD_LOADED));
		}
	}
}