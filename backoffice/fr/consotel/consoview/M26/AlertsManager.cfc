<cfcomponent name="AlertsManager" author="Florian">

	<cfset racineId=SESSION.PERIMETRE.ID_GROUPE>
	
	<cffunction name="registerParams" access="remote" returntype="numeric">
		<cfargument name="jsonSent" type="string" required="true">
		<cfargument name="IDSOUSTETE" type="any" required="true">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.IUAlerte_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(IDSOUSTETE,',')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#jsonSent#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="retrieveParams" access="remote" returntype="string">
		<cfargument name="IDSOUSTETE" type="numeric" required="true"> 
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.getAlerte">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#IDSOUSTETE#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#racineId#">
			<cfprocparam type="out" cfsqltype="CF_SQL_CLOB" variable="strJson">
		</cfstoredproc>
		
		<cfreturn ArrayToList(strJson, "")>
	</cffunction>
	
</cfcomponent>