package ihm {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.system.Capabilities;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	import mx.validators.EmailValidator;
	
	/**
	 * Model : Entité du modèle MVC qui émet des évènements liés aux données.
	 * Ces évènements sont écoutés par l'entité View et correspondent à des modifications concernant les données.
	 * */
	public class MainModel extends EventDispatcher {
		public static const FAULT:String="FAULT"; // Event : Lorsqu'une erreur s'est produite (e.g FaultEvent)
		// Event : Les valeurs des infos ont été réinitialisées par défaut et sont considérées comme modifiées (i.e changées)
		public static const MDM_SERVER_INFOS_RESETED:String="MDM_SERVER_INFOS_RESETED";
		// Event : Les valeurs des infos ont été réinitialisées par défaut et sont considérées comme non modifiées (i.e inchangées)
		public static const MDM_SERVER_INFOS_RESETED_NOT_CHANGED:String="MDM_SERVER_INFOS_RESETED_NOT_CHANGED";
		// Event : La valeur d'un mot de passe a changée (PASSOWRD, ENROLL_PASSWORD)
		public static const MDM_SERVER_PASSWORD_CHANGED:String="MDM_SERVER_PASSWORD_CHANGED";
		public static const MDM_SERVER_INFOS_CHANGED:String="MDM_SERVER_INFOS_CHANGED"; // Event : Les infos du serveur MDM de la racine ont été modifiées (Sauf mots de passes)
		// Event : Les infos du serveur MDM de la racine ont été modifiées mais que les valeurs restent inchangées
		public static const MDM_SERVER_INFOS_NOT_CHANGED:String="MDM_SERVER_INFOS_NOT_CHANGED";
		public static const MDM_SERVER_INFOS_UPDATED:String="MDM_SERVER_INFOS_UPDATED"; // Event : Les infos du serveur MDM de la racine ont été mises à jour
		public static const MDM_SERVER_INFOS:String="MDM_SERVER_INFOS"; // Event : Les infos du serveur MDM de la racine ont été récupérées
		
		// Option d'enregistrement : Test de connexion avec le serveur MDM
		public static const CHECK_SERVER_CONNECTION:String="CHECK_SERVER_CONNECTION";
		
		/* Tableau dont chaque élément vaut TRUE si la valeur de la propriété correspondant a changée et FALSE sinon
		Les propriétés correspondantes sont listées dans l'ordre suivant :
		USE_SSL, MDM, ADMIN_EMAIL, USERNAME, PASSWORD, ENROLL_USERNAME, ENROLL_PASSWORD, IS_DEFAULT
		
		*/
		private var _changedList:Array=[false,false,false,false,false,false,false,false,false];
		
		/* Tableau dont chaque élément vaut TRUE si le mot de passe correspondant a été confirmé et FALSE sinon
		Les mot de passes correspondants sont listés dans l'ordre suivant : PASSWORD, ENROLL_PASSWORD
		*/
		private var _passwordConfirmedList:Array=[true,true];
		
		/* Tableau dont chaque élément vaut TRUE si l'email correspondant est valide et contient une seule adresse mail et FALSE sinon
		Les emails correspondants sont listés dans l'ordre suivant : ADMIN_EMAIL
		*/
		private var _emailValidList:Array=[true];
		private var mdmAdminEmailValidator:EmailValidator;
		
		private var _apiKeyValidList:Array=[true];

		// Liste des providers MDM
		private var mdmProviders:ArrayCollection=null
		
		// Chaque clé de cet objet est une propriété MDM connue
		private const _mdmInfosPropertyMap:Object={
			USERID:0,ADMIN_EMAIL:"",MDM:"",USE_SSL:true,USERNAME:"",PASSWORD:"",ENROLL_USERNAME:"",ENROLL_PASSWORD:"",
			IS_DEFAULT:true,IS_TEST_OK:false,TEST_MSG:"",MDM_TYPE:"",TOKEN:""
		};
		
		// Valeurs actuelles des infos du serveur MDM
		private var _currentMdmServerInfos:Object={
			USERID:0,ADMIN_EMAIL:"",MDM:"",USE_SSL:true,USERNAME:"",PASSWORD:"",ENROLL_USERNAME:"",ENROLL_PASSWORD:"",
			IS_DEFAULT:true,IS_TEST_OK:false,TEST_MSG:"",MDM_TYPE:"",TOKEN:""
		};
		private var _mdmServerInfos:Object; // Infos du serveur MDM de la racine
		
		private var _mdmServerInfosBeforeReset:Object; // Infos du serveur MDM de la racine avant reset
		
		public function MainModel(target:IEventDispatcher=null) {
			super(target);
			mdmAdminEmailValidator=new EmailValidator(); // Validateur de l'email administrateur MDM
			groupeMdmServerInfos=_currentMdmServerInfos; // Valeurs initiales des infos du serveur MDM de la racine
		}

		/** Retourne TRUE si les valeurs des mots de passes sont confirmés et FALSE sinon */
		public function get wsPasswordConfirmed():Boolean {
			return _passwordConfirmedList[0];
		}
		
		/** Retourne TRUE si les valeurs des mots de passes sont confirmés et FALSE sinon */
		public function get enrollPasswordConfirmed():Boolean {
			return _passwordConfirmedList[1];
		}
		
		/** Retourne TRUE si l'adresse mail de l'administrateur MDM est valide et FALSE sinon */
		public function isAdminEmailValid():Boolean {
			return _emailValidList[0];
		}
		
		
		/** Retourne TRUE si l'adresse mail de l'administrateur MDM est valide et FALSE sinon */
		public function isApiKeyValid():Boolean {
			return _apiKeyValidList[0] || !isApiKeyCompulsory(getCurrentMdmServerProperty("MDM") as String);
		}
		
		/**
		 * Effectue l'enregistrement des modifications des infos actuelles. Le paramètre updateOptions contient les options d'enregistrement.
		 * Ces options correspondent à des actions qui seront effectuées avant l'enregistrement des infos.
		 * Les options possibles sont les clés suivantes :
		 * 		- MainModel.CHECK_SERVER_CONNECTION : TRUE pour effectuer un test de connexion avec le serveur MDM et FALSE sinon
		 * Les résultats des actions sont restitués dans celui de l'enregistrement et sont les suivants :
		 * 		- Résultats restitués dans les infos MDM :
		 * 			- Propriété IS_TEST_OK : TRUE si le test de connexion a été effectué avec succès et FALSE sinon
		 * 			- Propriété TEST_MSG : Message associé au test de connexion ou la valeur nulle si l'option n'a pas été choisie
		 * Dispatche un évènement MainModel.MDM_SERVER_INFOS_UPDATED : Les modifications ont été appliquées (i.e enregistrées)
		 * Règles métiers appliquées :
		 * 		- Si la valeur actuelle de la propriété IS_DEFAULT est TRUE :
		 * 			- Une réinitialisation par défaut des valeurs sera effectuée
		 * 			- Le paramètre updateOptions est ignoré
		 * 		- Si la valeur actuelle de la propriété IS_DEFAULT est FALSE : Une mise à jour des valeurs sera effectuée
		 * 		- L'évènement dispatché est toujours MainModel.MDM_SERVER_INFOS_UPDATED
		 * 		- Aucune action n'est effectuée si updateOptions est nul
		 * 		- Les modifications seront appliquées (i.e enregistrées) quelque soit le résulat des actions correspondant aux options
		 * */
		public function updateMdmServerInfos(updateOptions:Object):void {
			var isCurrentMdmDefaultValues:Boolean=Boolean(getCurrentMdmServerProperty("IS_DEFAULT"));
			if(isCurrentMdmDefaultValues) // Récupération avec réinitialisation des valeurs par défaut des infos
				resetToDefaultMdmInfos(true);
			else { // Mise à jour des valeurs des infos
				if(updateOptions != null) {
					var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M27.MDM",'updateMdmServerInfos',mdmServerInfosUpdated
					);
					op.addEventListener(FaultEvent.FAULT,onFault);
					RemoteObjectUtil.callService(op,_currentMdmServerInfos,Boolean(updateOptions[MainModel.CHECK_SERVER_CONNECTION]));
				}
			}
		}
		
		/**
		 * Annule les modifications des valeurs des infos.
		 * Les valeurs sont remplacées par celles d'origine et seront sont considérées comme inchangées.
		 * Réinitialise les statuts des valeurs des infos MDM
		 * Valide les valeurs des infos MDM
		 * Dispatche l'évènement MainModel.MDM_SERVER_INFOS
		 * */
		public function cancelModifications():void {
			copyMdmInfos(groupeMdmServerInfos,_currentMdmServerInfos); // Copie les valeurs d'origine dans celles des infos actuelles
			resetDataStatus(); // Réinitialisation des status des infos MDM
			validateMdmInfos(); // Validation des valeurs
			dispatchEvent(new Event(MainModel.MDM_SERVER_INFOS));
		}
		
		/**
		 * Retourne la valeur actuelle de la propriété propertyKey qui fait partie des infos MDM. La valeur nulle est retournée si la propriété n'existe pas. 
		 * */
		public function getCurrentMdmServerProperty(propertyKey:String):Object {
			return getMdmPropertyFromInfos(_currentMdmServerInfos,propertyKey);
		}
		
		
		/**
		 * Modifie la valeur actuelle de la propriété propertyKey qui fait partie des infos MDM. Aucune action n'est effectuée si la propriété n'existe pas.
		 * Les statut de modification et de validité sont mis à jour après la modification.
		 * Cette méthode dispatche un des types d'évènement suivants :
		 * 		- MainModel.MDM_SERVER_INFOS_RESETED : Les valeurs des infos ont été réinitialisées et sont considérées comme modifiées
		 * 		- MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED : Les valeurs des infos ont été réinitialisées et sont considérées comme non modifiées
		 * 		- MainModel.MDM_SERVER_PASSWORD_CHANGED : La valeur d'un mot de passe a changée (PASSWORD,ENROLL_PASSWORD)
		 * 		- MainModel.MDM_SERVER_INFOS_CHANGED : La valeur de la propriété propertyKey a changée par rapport à celle d'origine (i.e De la racine)
		 * 		- MainModel.MDM_SERVER_INFOS_NOT_CHANGED : La valeur de la propriété propertyKey est inchangée par rapport à celle d'origine (i.e De la racine)
		 * Règles métiers appliquées pour les mots de passes (PASSWORD,ENROLL_PASSWORD) et quelque soit la valeur propertyValue fournie :
		 * 		- Le statut de modificiation du mot de passe est toujours TRUE dès qu'il est modifié
		 * 		- La valeur du mot de passe doit etre renseignée. Voir confirmMdmPassword()
		 * 		- Le statut de validité du mot de passe devient FALSE quand il est modifié. Voir confirmMdmPassword()
		 * Règles métiers appliquées pour la propriété IS_DEFAULT et quelque soit la valeur propertyValue fournie :
		 * 		- La modification est ignorée si la valeur d'origine est TRUE sinon elle est effectuée.
		 * 		- La modification est ignorée si la valeur propertyValue vaut FALSE.
		 * */
		public function setCurrentMdmServerProperty(propertyKey:String,propertyValue:Object):void {
			var eventType:String=null;
			var mdmPropertyValue:Object=getGroupeMdmServerProperty(propertyKey); // Valeur d'origine de la propriété
			if(mdmPropertyValue != null) {
				switch(propertyKey) {
					case "MDM_TYPE":
						if(propertyValue is String) {
							_changedList[9]=(StringUtil.trim((propertyValue as String)).length) && ((propertyValue as String) != mdmPropertyValue);
							_currentMdmServerInfos[propertyKey]=propertyValue;// Modification de la valeur actuelle de la propriété
							performApiKeyValidation()
						}
						break;
					case "USE_SSL":
						if(propertyValue is Boolean) {
							_changedList[0]=Boolean(mdmPropertyValue) ? (! Boolean(propertyValue)):Boolean(propertyValue);
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
						}
						break;
					case "MDM":
						if(propertyValue is String) {
							_changedList[1]=(StringUtil.trim((propertyValue as String)).length) && ((propertyValue as String) != mdmPropertyValue);
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
						}
						break;
					case "TOKEN":
						if(propertyValue is String) {
							_changedList[1]=(StringUtil.trim((propertyValue as String)).length) && ((propertyValue as String) != mdmPropertyValue);
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
							performApiKeyValidation();
						}
						break;
					case "ADMIN_EMAIL":
						if(propertyValue is String) {
							_changedList[2]=(StringUtil.trim((propertyValue as String)).length) && ((propertyValue as String) != mdmPropertyValue);
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
							performAdminEmailValidation(); // Validation de la valeur de l'email administrateur MDM
						}
						break;
					case "USERNAME":
						if(propertyValue is String) {
							_changedList[3]=(StringUtil.trim((propertyValue as String)).length) && ((propertyValue as String) != mdmPropertyValue);
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
						}
						break;
					case "PASSWORD": // Le statut de modificiation du mot de passe est toujours TRUE dès qu'il est modifié
						if(propertyValue is String) {
							_changedList[4]=true;
							_passwordConfirmedList[0]=false;
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
							eventType=MainModel.MDM_SERVER_PASSWORD_CHANGED; // Evènement correspondant
						}
						break;
					case "ENROLL_USERNAME":
						if(propertyValue is String) {
							_changedList[5]=(StringUtil.trim((propertyValue as String)).length) && ((propertyValue as String) != mdmPropertyValue);
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
						}
						break;
					case "ENROLL_PASSWORD": // Le statut de modificiation du mot de passe est toujours TRUE dès qu'il est modifié
						if(propertyValue is String) {
							_changedList[6]=true;
							_passwordConfirmedList[1]=false;
							_currentMdmServerInfos[propertyKey]=propertyValue; // Modification de la valeur actuelle de la propriété
							eventType=MainModel.MDM_SERVER_PASSWORD_CHANGED; // Evènement correspondant
						}
						break;
					case "IS_DEFAULT":
						var isCurrentMdmDefaultValues:Boolean=Boolean(getCurrentMdmServerProperty("IS_DEFAULT"));
						if(! isCurrentMdmDefaultValues) { // Les valeurs actuelles ne sont pas celles du serveur MDM par défaut
							// Si la modification consiste à réinitialiser les valeurs i.e les valeurs vont changées
							if((propertyValue is Boolean) && Boolean(propertyValue)) {
								_changedList[7]=true;
								_currentMdmServerInfos[propertyKey]=Boolean(propertyValue); // Modification de la valeur actuelle de la propriété
								resetToDefaultMdmInfos(false); // Récupération sans réinitialisation des valeurs par défaut des infos
							}
						}
						break;
					
				}
				// Traitement commun pour tous les cas sauf celui de la réinitialisation qui est géré dans la méthode resetToDefaultMdmInfos()
				if(propertyKey != "IS_DEFAULT") {
					// La valeur d'origine de la propriété IS_DEFAULT est restituée car elle sera modifiée par l'éventuelle modification d'une autre propriété
					_currentMdmServerInfos["IS_DEFAULT"]=_mdmServerInfos["IS_DEFAULT"];
					_changedList[7]=false; // La valeur de IS_DEFAULT est donc inchangée
					if(dataChanged()) { // Si la valeur d'au moins une propriété a changée
						// Modification : Il s'agit soit d'une propriété soit d'un mot de passe
						eventType=(eventType == null ? MainModel.MDM_SERVER_INFOS_CHANGED:eventType);
						// Les valeurs des infos deviennent différentes de celles par défaut
						if(Boolean(getCurrentMdmServerProperty("IS_DEFAULT"))) {
							_currentMdmServerInfos["IS_DEFAULT"]=false;
							_changedList[7]=true; // La valeur de IS_DEFAULT a changée
						}
					} else // Si les valeurs sont restées inchangées : La valeur de IS_DEFAULT reste inchangée
						eventType=MainModel.MDM_SERVER_INFOS_NOT_CHANGED;
					// Dispatch de l'évènement correspondant à la modification effectuée
					dispatchEvent(new Event(eventType));
				}
			}
		}
		
		/**
		 * Effectue la confirmation de la valeur d'un mot de passe MDM. Cette méthode est utilisée pour valider une double saisie.
		 * Le paramètre passwordKey indique le mot de passe à confirmer : PASSWORD (Mot de passe du WebService), ENROLL_PASSWORD (Mot de passe d'enrollment)
		 * Le paramètre passwordValue indique la valeur à confirmer avec celle du mot de passe.
		 * Dispatche un évènement MainModel.MDM_SERVER_PASSWORD_CHANGED lorsque la confirmation a été effectuée et quelque soit son résultat.
		 * Règles métiers appliquées quelque soit le mot de passe :
		 * 		- Le statut de validité devient TRUE la valeur passwordValue est identique à celle du mot de passe. Voir setCurrentMdmServerProperty()
		 * 		- Le statut de validité (confirmation) du mot de passe reste inchangé si passwordValue est une chaine vide (ou est non renseignée)
		 * */
		public function confirmMdmPassword(passwordKey:String,passwordValue:Object):void {
			var mdmPasswordValue:Object=getCurrentMdmServerProperty(passwordKey);
			if(mdmPasswordValue != null) {
				switch(passwordKey) {
					case "PASSWORD":
						if((passwordValue is String) && (StringUtil.trim((passwordValue as String)).length)) {
							_passwordConfirmedList[0]=(passwordValue == mdmPasswordValue);
							_changedList[4]=true;
						}
						break;
					case "ENROLL_PASSWORD":
						if((passwordValue is String) && (StringUtil.trim((passwordValue as String)).length)) {
							_passwordConfirmedList[1]=(passwordValue == mdmPasswordValue);
							_changedList[6]=true;
						}
						break;
				}
				dispatchEvent(new Event(MainModel.MDM_SERVER_PASSWORD_CHANGED)); // Evènement correspondant
			}
		}
		
		/**
		 * Retourne la liste des providers MDM
		 * */
		public function getMdmProviders():ArrayCollection {
			return mdmProviders;
		}
		
		
		/**
		 * Retourne true si une clé API est nécessaire
		 * */
		public function isApiKeyCompulsory(mdmProvideer:String):Boolean
		{
			if(ObjectUtil.stringCompare("airwatch",mdmProvideer,true) == 0)
			{
				return true;
			}
			return false;
		}
		
		/**
		 * Récupère la liste des providers MDM.
		 * Une fois la liste des providers obtenue : Récupère les infos du serveur MDM de la racine.
		 * Une validation des valeurs des infos est effectuée lorsque les données seront été récupérées.
		 * */
		public function loadMdmServerInfos():void {
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M27.MDM",'getProviders',mdmProvidersResult
			);
			op.addEventListener(FaultEvent.FAULT,onFault);
			RemoteObjectUtil.callService(op,null);
			apiIsAvailable(null);
		}
		
		private function mdmProvidersResult(eventObject:ResultEvent):void {
			mdmProviders=new ArrayCollection(eventObject.result as Array);
			
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(
				RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M27.MDM",'getMdmServerInfos',mdmServerInfosResult
			);
			op.addEventListener(FaultEvent.FAULT,onFault);
			RemoteObjectUtil.callService(op,null);
		}
		
		/** Listener exécuté lorsque les valeurs des infos ont été récupérées
		 * Remplace les valeurs des infos actuelles et d'origine par celles qui ont été récupérées
		 * Réinitialise les statuts des valeurs des infos MDM
		 * Valide les valeurs des infos MDM
		 * Dispatche un évènement MainModel.MDM_SERVER_INFOS
		 * Règles métiers : Aucune action n'est effectuée si eventObject est nul
		 * */
		private function mdmServerInfosResult(eventObject:ResultEvent):void {
			if(eventObject) {
				_currentMdmServerInfos=eventObject.result; // Copie des infos du serveur MDM de la racine
				groupeMdmServerInfos=_currentMdmServerInfos; // Infos du serveur MDM de la racine
				resetDataStatus(); // Réinitialisation des status des infos MDM
				validateMdmInfos(); // Validation des valeurs des infos
				dispatchEvent(new Event(MainModel.MDM_SERVER_INFOS));
			}
		}
		
		/**
		 * Listener : Exécuté lorsque la transaction correspondant à l'enregistrement des modifications a été effectuée
		 * Les infos MDM de la racine sont remplacées par les valeurs actuelles qui ont été enregistrées.
		 * Réinitialise les statuts des valeurs des infos MDM
		 * Les valeurs sont valides car elles ont été enregistrées
		 * Dispatche un évènement MainModel.MDM_SERVER_INFOS_UPDATED
		 * */
		private function mdmServerInfosUpdated(eventObject:ResultEvent):void {
			if(eventObject) {
				_currentMdmServerInfos["IS_TEST_OK"]=eventObject["result"]["IS_TEST_OK"];
				_currentMdmServerInfos["TEST_MSG"]=StringUtil.trim(eventObject["result"]["TEST_MSG"]) == "NONE" ? null:eventObject["result"]["TEST_MSG"];
				groupeMdmServerInfos=_currentMdmServerInfos; // Remplacement des infos MDM de la racine après mise à jour
				resetDataStatus(); // Réinitialisation des statuts des infos MDM
				dispatchEvent(new Event(MainModel.MDM_SERVER_INFOS_UPDATED));
			}
		}
		
		/** Réinitialise et par défaut récupére les valeurs par défaut des infos.
		 * var isCurrentMdmDefaultValues:Boolean=Boolean(getCurrentMdmServerProperty("IS_DEFAULT"));
		 * Si updateValues vaut TRUE : La réinitialisation est appliquée (i.e Enregistrement des valeurs pour la racine)
		 * Sinon les valeurs des infos sont réinitialisées mais ne sont pas appliquées (i.e enregistrées)
		 * Le listener defaultMdmServerInfosResult() est exécuté lorsque les données ont été récupérées.
		 * La valeur de updateValues est stockée dans les "properties" de l'instance AbstractOperation pour etre retrouvée dans le listener.
		 * Règles métiers appliquées :
		 * 		- Dispatche un des évènements suivants :
		 * 			- MainModel.MDM_SERVER_INFOS_RESETED : Si updateValues vaut FALSE et si la valeur d'origine de la propriété IS_DEFAULT est FALSE
		 * 			- MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED : Si updateValues vaut FALSE et si la valeur d'origine de la propriété IS_DEFAULT est TRUE
		 * 			- MainModel.MDM_SERVER_INFOS_UPDATED : Si updateValues vaut TRUE
		 * 		- Aucune action n'est effectuée si le statut de modification de la propriété IS_DEFAULT vaut FALSE
		 * */
		private function resetToDefaultMdmInfos(updateValues:Boolean=true):void {
			var isDefaultChanged:Boolean=_changedList[7]; // TRUE si la propriété IS_DEFAULT a changée
			if(isDefaultChanged) {
				var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M27.MDM",'resetMdmServerInfos',resetToDefaultMdmInfosResult
				);
				op.properties={updateValues:updateValues}; // Stockage de la valeur de updateValues pour etre retrouvée dans le listener
				op.addEventListener(FaultEvent.FAULT,onFault);
				RemoteObjectUtil.callService(op,updateValues);
			}
		}
		
		/** Listener exécuté lorsque les valeurs par défaut des infos ont été récupérées
		 * Si le statut de modification de la propriété IS_DEFAULT vaut FALSE :
		 * 		- Récupère la valeur du paramètre updateValues stockés dans l'instance AbstractOperation lors de l'appel au service
		 * 		- Remplace les valeurs des infos actuelles par les valeurs par défaut qui ont été récupérées
		 * 		- Réinitialise les statuts des valeurs des infos MDM (Le statut de modification de IS_DEFAULT passe à FALSE)
		 * 		- Valide les valeurs des infos MDM
		 * Règles métiers appliquées :
		 * 		- Aucune action n'est effectuée si :
		 * 			- Si le paramètre eventObject et eventObject.target sont nuls et eventObject.target n'est pas de type AbstractOperation
		 * 			- Si le statut de modification de la propriété IS_DEFAULT vaut FALSE
		 * 		- La valeur FALSE est utilisée par défaut pour updateValues si cette propriété n'est pas présente dans l'instance AbstractOperation
		 * 		- Si la valeur d'origine de la propriété IS_DEFAULT est FALSE :
		 * 			- Si updateValues vaut TRUE :
		 * 				- Les infos MDM de la racine sont remplacées par les valeurs actuelles qui ont été réinitialisées.
		 * 				- La valeur de la propriété IS_TEST_OK est remplacée par TRUE et celle de TEST_MSG par null
		 * 				- L'évènement MainModel.MDM_SERVER_INFOS_UPDATED est dispatché
		 * 			- Sinon :
		 * 				- La valeur TRUE est affectée au statut de modification de la propriété IS_DEFAULT (Car sa valeur a été modifiée)
		 * 				- L'évènement MainModel.MDM_SERVER_INFOS_RESETED est dispatché
		 * 		- Sinon : Aucune action n'est effectuée et l'évènement MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED est dispatché
		 * */
		private function resetToDefaultMdmInfosResult(eventObject:ResultEvent):void {
			if(eventObject && (eventObject.target != null) && (AbstractOperation(eventObject.target) != null )) {
				var isDefaultChanged:Boolean=_changedList[7]; // TRUE si la propriété IS_DEFAULT a changée
				// Si la valeur actuelle de la propriété IS_DEFAULT a changée : Les valeurs des infos seront réinitialisées
				if(isDefaultChanged) {
					_currentMdmServerInfos=eventObject.result; // Remplacement des valeurs par celles par défaut
					resetDataStatus(); // Réinitialisation des statuts des infos MDM (Le statut de modification de IS_DEFAULT passe à FALSE)
					validateMdmInfos(); // Validation des valeurs des infos
					if(Boolean(getGroupeMdmServerProperty("IS_DEFAULT"))) // La valeur d'origine de IS_DEFAULT est TRUE
						dispatchEvent(new Event(MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED));
					else { // La valeur d'origine de IS_DEFAULT est FALSE : Les valeurs des infos sont modifiées et différentes de celles d'origine
						var op:AbstractOperation=AbstractOperation(eventObject.target);
						var updateValues:Boolean=false;
						// Récupération de la valeur de updateValues lors de l'appel au service
						if((op.properties != null) && op.properties.hasOwnProperty("updateValues") && (op.properties.updateValues != null))
							updateValues=Boolean(op.properties.updateValues);
						if(updateValues) { // Les valeurs sont réinitialisées et ont été appliquées (i.e enregistrées)
							_currentMdmServerInfos[""]=true; // Pas de test de connexion pour la réinitialisation
							_currentMdmServerInfos["TEST_MSG"]=null; // Pas de test de connexion pour la réinitialisation
							groupeMdmServerInfos=_currentMdmServerInfos; // Remplacement des infos MDM de la racine après mise à jour
							dispatchEvent(new Event(MainModel.MDM_SERVER_INFOS_UPDATED));
						} else { // Les valeurs sont réinitialisées sans etre appliquées (i.e enregistrées)
							_changedList[7]=true; // La propriété IS_DEFAULT reste marquée comme modifiée
							dispatchEvent(new Event(MainModel.MDM_SERVER_INFOS_RESETED));
						}
					}
				}
			}
		}
		
		/** Listener exécuté lorsque une erreur a eu lieu durant une des actions suivantes :
		 * 		- Récupération des données
		 * 		- Enregistrement des modifications
		 * Dispatche un évènement MainModel.FAULT
		 * */
		private function onFault(eventObject:FaultEvent):void {
			var eventTarget:EventDispatcher=eventObject.target as EventDispatcher;
			if(eventTarget && eventTarget.hasEventListener(FaultEvent.FAULT))
				eventTarget.removeEventListener(FaultEvent.FAULT,onFault);
			dispatchEvent(new Event(MainModel.FAULT));
		}
		
		/** Réinitialise les statuts de modification et de validité des valeurs des infos MDM
		 * Après un appel à cette méthode les conditions suivantes seront vérifiées :
		 * 		- La valeur retournée par dataChanged() est FALSE
		 * 		- La valeur retournée par les méthode suivantes est TRUE :
		 * 		isAdminEmailValid(), wsPasswordConfirmed(), enrollPasswordConfirmed()
		 * */
		private function resetDataStatus():void {
			// Réinitialisation des statuts de modification
			for(var i:int=0; i < _changedList.length; i++)
				_changedList[i]=false;
			// Réinitialisation des statuts de validité
			_apiKeyValidList[0]=_emailValidList[0]=_passwordConfirmedList[0]=_passwordConfirmedList[1]=true;
		}
		
		/**
		 * Effectue la validation des valeurs actuelles des infos MDM et met à jour leur statut de validité.
		 * Les infos concernées par cette validation sont : Email administrateur MDM
		 * La confirmation de la valeur des mots de passes ne fait pas partie des validations effectuée par cette méthode
		 * */
		private function validateMdmInfos():void {
			performAdminEmailValidation(); // Validation Email Admin MDM
			performApiKeyValidation();//Validation de la présence de la clé api si necessaire
		}
		
		/**
		 * Effectue la validation de la valeur actuelle de l'email administrateur MDM et met à jour son statut de validité.
		 * */
		private function performAdminEmailValidation():void {
			var mdmAdminEmailValue:Object=getCurrentMdmServerProperty("ADMIN_EMAIL");
			if((mdmAdminEmailValue != null) && (mdmAdminEmailValue is String))
				_emailValidList[0]=(EmailValidator.validateEmail(mdmAdminEmailValidator,mdmAdminEmailValue,"mdmAdminEmailValue").length == 0);
		}
		
		private function performApiKeyValidation():void
		{
			var mdmApiKeyValue:String=getCurrentMdmServerProperty("TOKEN") as String;
			_apiKeyValidList[0] = ((mdmApiKeyValue != null) && (mdmApiKeyValue.length > 0)) ? true:false;
			
		}
		
		
		/** Retourne TRUE si les valeurs des infos sont différentes de celles d'origine et FALSE sinon */
		private function dataChanged():Boolean {
			var dataHaveChanged:Boolean=false;
			for(var i:int=0; i < _changedList.length; i++)
				dataHaveChanged=(dataHaveChanged || _changedList[i]);
			return dataHaveChanged;
		}
		
		/**
		 * Retourne la valeur d'origine de la propriété propertyKey qui fait partie des infos MDM. La valeur nulle est retournée si la propriété n'existe pas
		 * */
		private function getGroupeMdmServerProperty(propertyKey:String):Object {
			return getMdmPropertyFromInfos(groupeMdmServerInfos,propertyKey);
		}
		
		/**
		 * Retourne la valeur de la propriété propertyKey contenue dans les infos MDM mdmInfos.
		 * La valeur nulle est retournée si mdmInfos est nul ou si la propriété propertyKey n'existe pas
		 * */
		private function getMdmPropertyFromInfos(mdmInfos:Object,propertyKey:String):Object {
			if(mdmInfos != null)
				if(isMdmProperty(propertyKey) && mdmInfos.hasOwnProperty(propertyKey))
					return mdmInfos[propertyKey];
			trace("MDM property "+propertyKey+" is not defined");
			return null; // La valeur nulle est retournée si mdmInfos est nul ou si la propriété propertyKey n'existe pas
		}
		
		/**
		 * Retourne TRUE si la propriété propertyKey fait partie des infos MDM connues et FALSE sinon.
		 * */
		private function isMdmProperty(propertyKey:String):Boolean {
			return _mdmInfosPropertyMap.hasOwnProperty(propertyKey);
		}
		
		// Retourne les valeurs d'origine des infos du serveur MDM
		private function get groupeMdmServerInfos():Object {
			return _mdmServerInfos;
		}
		
		// Définit les infos du serveur MDM de la racine
		private function set groupeMdmServerInfos(mdmInfos:Object):void {
			if(_mdmServerInfos == null)
				_mdmServerInfos=new Object();
			copyMdmInfos(mdmInfos,_mdmServerInfos);
		}
		
		public function saveMdmServerInfosBeforeReset():void
		{
			_mdmServerInfosBeforeReset = _mdmServerInfos;
		}
		
		public function get mdmServerInfosBeforeReset():Object
		{
			return _mdmServerInfosBeforeReset;
		}
		
		public function mdmServerInfosJustGotReseted():Boolean
		{
			var b:Boolean = (_mdmServerInfosBeforeReset == _mdmServerInfos);
			_mdmServerInfosBeforeReset = new Object();
			return b;
		}
		/**
		 * Copie les infos MDM contenues dans source dans dest.
		 * Aucune action n'est effectuée si source ou dest sont des références nulles
		 * */
		private function copyMdmInfos(source:Object,dest:Object):void {
			if(source != null && dest != null) {
				dest["USERID"]=source["USERID"];
				dest["ADMIN_EMAIL"]=source["ADMIN_EMAIL"];
				dest["MDM"]=source["MDM"];
				dest["USE_SSL"]=source["USE_SSL"];
				dest["USERNAME"]=source["USERNAME"];
				dest["PASSWORD"]=source["PASSWORD"];
				dest["ENROLL_USERNAME"]=source["ENROLL_USERNAME"];
				dest["ENROLL_PASSWORD"]=source["ENROLL_PASSWORD"];
				dest["IS_DEFAULT"]=source["IS_DEFAULT"];
				dest["IS_TEST_OK"]=source["IS_TEST_OK"];
				dest["TEST_MSG"]=source["TEST_MSG"];
				dest["MDM_TYPE"]=source["MDM_TYPE"];
				dest["TOKEN"]=source["TOKEN"];
			}
		}
		
		private function apiIsAvailable(eventObject:ResultEvent):void {
			if(!eventObject) {
				RemoteObjectUtil.callService(RemoteObjectUtil.getHandledOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M27.MDM",'isApiAvailable',apiIsAvailable
				),Capabilities.serverString);
			}
		}
	}
}