package rapport.service.templateselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import rapport.service.formattemplatetelector.*;

	public class TemplateSelectorService
	{
		
		private var _model:TemplateSelectorModel;
		public var handler:TemplateSelectorHandler;
		
		public function TemplateSelectorService():void
		{
			this._model = new TemplateSelectorModel();
			this.handler = new TemplateSelectorHandler(model);
		}		
		
		
		public function get model():TemplateSelectorModel
		{
			return this._model;
		}
		
		public function getValuesTemplate(idRapportRacine:int):void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.TemplateSelectorService","getValuesTemplate",handler.getValuesTemplateHandler); 
			
			RemoteObjectUtil.callService(op,idRapportRacine);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
		
		
	}
}