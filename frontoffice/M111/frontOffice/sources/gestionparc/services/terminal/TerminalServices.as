package gestionparc.services.terminal
{
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.ContratVO;
	
	import mx.rpc.AbstractOperation;

	/**
	 * Appelle les procédures liées aux équipements.
	 */
	public class TerminalServices
	{
		
		public var myDatas 		: TerminalDatas;
		public var myHandlers 	: TerminalHandlers;
		
		public function TerminalServices()
		{
			myDatas 	= new TerminalDatas();
			myHandlers 	= new TerminalHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 */	
		public function getInfosFicheTerminal(createObjToServiceGet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M11.FicheTerminal",
				"getInfosFicheTerminal",
				myHandlers.getInfosFicheTerminalResultHandler);
			
			RemoteObjectUtil.callService(op, createObjToServiceGet.idTerminal);
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant l'équipement.
		 */	
		public function setInfosFicheTerminal(createObjToServiceSet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M11.FicheTerminal",
				"setInfosFicheTerminal",
				myHandlers.setInfosFicheTerminalResultHandler);
			
			RemoteObjectUtil.callService(op, 
				createObjToServiceSet.myTerminal.idEquipement,
				createObjToServiceSet.myTerminal.idEqptFournis,
				createObjToServiceSet.myTerminal.idRevendeur,
				createObjToServiceSet.myTerminal.idFabricant,
				createObjToServiceSet.myTerminal.idTypeEquipement,
				createObjToServiceSet.myTerminal.commande.idCommande,
				createObjToServiceSet.isNotSameImei,
				createObjToServiceSet.myTerminal.libelleEquipement,
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myTerminal.commande.dateAchat),
				createObjToServiceSet.myTerminal.commande.prix,
				createObjToServiceSet.myTerminal.commande.numCommande,
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myTerminal.commande.dateLivraison),
				createObjToServiceSet.myTerminal.commande.refLivraison,
				createObjToServiceSet.myTerminal.commentaires,
				createObjToServiceSet.hasSite,
				createObjToServiceSet.myTerminal.idSite,
				createObjToServiceSet.myTerminal.libEqptFournis,
				createObjToServiceSet.myTerminal.refConst,
				createObjToServiceSet.myTerminal.refRevendeur);
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant un contrat d'équipement.
		 * N.B. : Si l'idContrat = -1 alors l'action correspond à un ajout de contrat,
		 * 		  sinon cela correspond à une mise à jour des infos du contrat.
		 */	
		public function setInfosContrat(myContrat:ContratVO, idTerminal:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.FicheTerminal",
				"setInfosContrat",
				myHandlers.setInfosContratResultHandler);
			
			RemoteObjectUtil.callService(op, 
				myContrat.idContrat,
				idTerminal,
				myContrat.idTypeContrat,
				myContrat.idRevendeur,
				myContrat.refContrat,
				DateFunction.formatDateAsInverseString(myContrat.dateSignature),
				DateFunction.formatDateAsInverseString(myContrat.dateEcheance),
				myContrat.commentaires,
				myContrat.tarif,
				myContrat.dureeContrat,
				myContrat.taciteReconduction,
				DateFunction.formatDateAsInverseString(myContrat.dateDebut),
				myContrat.preavis,
				myContrat.tarifMensuel,
				DateFunction.formatDateAsInverseString(myContrat.dateResiliation),
				DateFunction.formatDateAsInverseString(myContrat.dateRenouvellement),
				DateFunction.formatDateAsInverseString(myContrat.dateEligibilite),
				myContrat.dureeEligibilite);
		}
		
		/**
		 * Appelle la procédure permettant de supprimer le contrat.
		 */	
		public function deleteContrat(myContrat:ContratVO):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.contrats.GestionContratsService",
				"deleteContrat",
				myHandlers.deleteContratResultHandler);
			
			RemoteObjectUtil.callService(op,myContrat.idContrat);
		}
		
		public function addNewModele(objResult:Object):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview..M111.fiches.FicheTerminal",
				"addNewModele", 
				myHandlers.AddNewModeleResultHandler);
			RemoteObjectUtil.callService(opData,objResult)
		}
	}
}