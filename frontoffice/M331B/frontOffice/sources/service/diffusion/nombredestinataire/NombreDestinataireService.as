package service.diffusion.nombredestinataire
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class NombreDestinataireService
	{
		public var _model:NombreDestinataireModel;
		public var handler:NombreDestinataireHandler;

		public function NombreDestinataireService()
		{
			this._model=new NombreDestinataireModel();
			this.handler=new NombreDestinataireHandler(model);
		}

		public function get model():NombreDestinataireModel
		{
			return this._model;
		}

		public function getNbProfil(idOrga : int ,idProfil: int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M331.service.diffusion.GetNbProfilService", "getNbDestinataire", handler.nbProfilHandler);

			RemoteObjectUtil.callService(callOp,idOrga,idProfil);
		}
	}
}
