package gestionparc.ihm.fiches.ligne
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import gestionparc.entity.FileUpload;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.listedispo.DispoSimMobile;
	import gestionparc.ihm.question.ligne.QuestionFournirSimStock;
	import gestionparc.services.ligne.LigneServices;
	import gestionparc.services.sim.SimServices;
	import gestionparc.utils.gestionparcConstantes;

	public class FicheFournirSimStockImpl extends TitleWindow
	{	
		public var img_cellule:Image;
		public var img_liste:Image;
		public var img_liste_big:Image;
		public var lab_cellule_name:Label;
		public var lab_titre:Label;
		public var labDgSelection:Label;
		public var box_listeDispo:VBox;
		public var calendar:CvDateChooser;
		public var btAnnuler:Button;
		public var btValider:Button;
		public var linkBtnRetour:Button;
		[Bindable]
		public var obj_liste:DispoSimMobile;
		private var simService:SimServices;
		private var qn:QuestionFournirSimStock;
		private var ligneService:LigneServices;
		private var _currentObjectParc:Object;
		
		
		public function FicheFournirSimStockImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM)
		}
		
		private function initIHM(evt : FlexEvent):void
		{
			simService= new SimServices();
			ligneService = new LigneServices();
			obj_liste = new DispoSimMobile(0,0,0, 1);
			
			img_cellule.source = gestionparcConstantes.adrImgTerm; //Afficher l'icone de la cellule séléctionné
			img_liste.source=obj_liste.getSourceIcone();//Afficher l'icone de la liste
			img_liste_big.source=obj_liste.getSourceIcone();//Afficher l'icone de la liste
			lab_cellule_name.text = SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;//Affichage du nom de la cellule séléctionné
			lab_titre.text=""+obj_liste.getLibelleType()+"";//Affichage du type d'objet de la liste
			box_listeDispo.addChild(obj_liste as VBox);//Ajout du composant contenant la dg + le filtre
			calendar.selectedDate = new Date(); // Selection de la date actuel dans la date du jour
			labDgSelection.text = obj_liste.getLibelleSelection();
			
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
			addEventListener(CloseEvent.CLOSE,fermer);
			linkBtnRetour.addEventListener(MouseEvent.CLICK, retourHandler);
			
			obj_liste.addEventListener(MouseEvent.CLICK,dgListeDispo_handler);
			addEventListener(MouseEvent.CLICK,dgListeDispo_handler);

			ligneService.myDatas.addEventListener(gestionparcEvent.LISTE_DESTINA_SIM_STOCK, getListeDestinataireHandler);
			ligneService.getListeDestinatairesEchangeSIM(SpecificationVO.getInstance().elementDataGridSelected.S_IDREVENDEUR);
			//trace("###########  Object info ligne : " + ObjectUtil.toString(currentObjectParc));
		}
		
		private function retourHandler(me:MouseEvent):void
		{
			obj_liste.ajoutSimHorsParc = false;
			obj_liste.containerInitial.visible = obj_liste.containerInitial.includeInLayout = true;
			obj_liste.containerAddElement.visible = obj_liste.containerAddElement.includeInLayout = false;
			
		}
		
		private function getListeDestinataireHandler(ev:gestionparcEvent):void
		{
			//trace("################### LISTEEMAILS : " + ligneService.myDatas.listeEmails);
		}
		
		private function dgListeDispo_handler(evt : Event):void
		{
			labDgSelection.text = obj_liste.getLibelleSelection();
		}
		private function valider(evt : MouseEvent):void
		{
			if(obj_liste.ajoutSimHorsParc == true)
			{
				if ((obj_liste.isValidSIM == true) && (obj_liste.cbOperateur.selectedIndex > -1))
				{
					echangerSIM();
				}
				else
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Veuillez_renseigner_les_champs_obligatoires'), 'Alerte', null);
				}
			}
			else if((obj_liste.ajoutSimHorsParc == false) && (obj_liste.getIDSelection() != -1))
			{
				echangerEqp();
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
		}
		
		
		private function createMailInfos():Object
		{
			var emailnfo:Object = new Object;
			emailnfo.destinataire 		 = ligneService.myDatas.listeEmails;	
			
			emailnfo.SOUS_TETE			 = SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
			emailnfo.NEW_SIM			 = obj_liste.getLibelleSelection();
			emailnfo.OLD_SIM			 = SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;
			emailnfo.GFL 		 		 = CvAccessManager.getSession().USER.NOM + ' ' + CvAccessManager.getSession().USER.PRENOM;
			emailnfo.POOL       		 = currentObjectParc.LABEL_POOL;
			emailnfo.withAttachement 	 = (obj_liste.isFileUploadPresent == true)? 1 : 0;
			emailnfo.attachement	 	 = (obj_liste.isFileUploadPresent == true)?obj_liste.enteredFileUpload : null;
			
			return emailnfo; 
		}

		/**
		 * Cette échange de SIM se fait selon les étapes suivantes:
		 * 1 - Saisie de l'utilisateur d'un numéro de SIM, une fois l'utilisateur valide le formulaire
		 * 2 - une fois l'utilisateur valide le formulaire, l'ID SIM est généré
		 * 3 - Echange de SIM
		 * 
		 * 4 - Envoie d'Email
		 * 
		 */
		private function echangerSIM():void
		{
			var objectCreateSim:Object = new Object;
			
			//cbModele.selectedItem.IDEQUIPEMENT_FOURNISSEUR
			objectCreateSim.IDEQUIPEMENT_FOURNISSEUR = obj_liste.myServiceTerminal.myDatas.listeModele[0].IDEQUIPEMENT_FOURNISSEUR; 
			
			//cbModele.selectedItem.IDREVENDEUR
			objectCreateSim.IDREVENDEUR = obj_liste.myServiceTerminal.myDatas.listeModele[0].IDREVENDEUR;
			
			//cbMarque.selectedItem.IDFOURNISSEUR
			objectCreateSim.IDFOURNISSEUR = obj_liste.cbOperateur.selectedItem.IDFOURNISSEUR
			
			// le Numéro de SIM SAISIE
			objectCreateSim.NEW_SIM = obj_liste.enteredSIM;
				
			//cbModele.selectedItem.LIBELLE_EQ
			objectCreateSim.LIBELLE_EQ = obj_liste.myServiceTerminal.myDatas.listeModele[0].LIBELLE_EQ;
			
			//IdPool
			objectCreateSim.IDPOOL = SpecificationVO.getInstance().idPool;
			
			qn = new QuestionFournirSimStock(obj_liste, calendar, objectCreateSim);
			qn.serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED,associationHandler);
			
		}
		
		private function echangerEqp():void
		{
			qn = new QuestionFournirSimStock(obj_liste,calendar);
			qn.serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED,associationHandler);
		}
		
		private function associationHandler(evt:gestionparcEvent):void
		{
			fermer(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Echange_de_la_SIM_effectu_'),Application.application as DisplayObject);
			if(ligneService.myDatas.listeEmails != '')
			{
				ligneService.myDatas.addEventListener(gestionparcEvent.MESSAGE_ECHANGE_SIM_SENDED, confirmationHandler);
				ligneService.sendEmailEchangeSimStock(createMailInfos());	
			}
		}
		
		private function confirmationHandler(gpevt: gestionparcEvent):void
		{
			// message envoyé
		}
		
		private function fermer(evt : Event):void 
		{
			PopUpManager.removePopUp(this);
		}
		
		public function get currentObjectParc():Object { return _currentObjectParc; }
		
		public function set currentObjectParc(value:Object):void
		{
			if (_currentObjectParc == value)
				return;
			_currentObjectParc = value;
		}
	}
}