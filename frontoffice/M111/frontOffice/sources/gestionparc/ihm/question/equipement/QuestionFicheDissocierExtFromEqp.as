package gestionparc.ihm.question.equipement
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheDissocierExtFromEqp
	{
		private var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheDissocierExtFromEqp()
		{
			var ruleExt:IRuleDisplayItem=new HasExtAssociated();
			var ruleTypeMobile :IRuleDisplayItem= new IsTypeLigneMobile();
			var ligne : AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			var ruleCollab:IRuleDisplayItem=new HasCollaborateurAssociated();
			

			if (ruleExt.isDisplay() && ! ruleTypeMobile.isDisplay() && ligne.IDTYPE_LIGNE > 0) // il y a une ligne fixe associée à l'équipement 
			{
				if (ruleCollab.isDisplay())
					Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERLIGNEEXTFROMEQP"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseLigneExt)
				else
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "Etre_vous_s_r_de_vouloir_dissocier_cette_extention_de_l__quipement"), ResourceManager.getInstance().getString("M111", "Dissocier_extention_de_l__quipement"), alertClickHandler);
			}
			else if (ruleExt.isDisplay() && ! ligne.IDTYPE_LIGNE > 0) // pas de ligne associée à l'équipement
			{
				if (ruleCollab.isDisplay())
					Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIEREXTFROMEQP"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseLigneExt);
				else
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "Etre_vous_s_r_de_vouloir_dissocier_cette_extention_de_l__quipement"), ResourceManager.getInstance().getString("M111", "Dissocier_extention_de_l__quipement"), alertClickHandler);			
			}
		}
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				dissocierLigneExtFromEqp3();
			}
		}

		private function qnCloseLigneExt(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				dissocierLigneExtFromEqp1();
				
			else if (evt.detail == Alert.CANCEL)
			{
				
			}
			else
				dissocierLigneExtFromEqp2()
		}
		
		private function dissocierLigneExtFromEqp1():void
		{
			serv.dissocierLigneExtFromEqp1(setObj());
		}
		
		private function dissocierLigneExtFromEqp2():void
		{
			serv.dissocierLigneExtFromEqp2(setObj());
		}
		
		private function dissocierLigneExtFromEqp3():void
		{
			serv.dissocierLigneExtFromEqp3(setObj());
		}
		
	
		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.T_MODELE;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_EXT=SpecificationVO.getInstance().elementDataGridSelected.IDEXTENSION;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}
	}
}
