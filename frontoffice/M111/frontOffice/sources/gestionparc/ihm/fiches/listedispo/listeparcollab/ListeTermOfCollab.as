package gestionparc.ihm.fiches.listedispo.listeparcollab
{
	import flash.events.Event;
	import flash.events.MouseEvent;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.liste.ListeService;

	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class ListeTermOfCollab extends ListeTermOfCollabIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")]
		private var adrImg:Class;
		public var serv:ListeService=new ListeService();

		public function ListeTermOfCollab()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			dgListeTerm.addEventListener(MouseEvent.CLICK, dgVisibleChanged);
			//txtFiltre.addEventListener(Event.CHANGE, filtrer);
			txtFiltre.setFocus();
			radioMobileSansSIM.addEventListener(Event.CHANGE, filtrerMobileSans);
			radioMobileAvecSIM.addEventListener(Event.CHANGE, filtrerMobileAvec);
			radioFixeAvecSIM.addEventListener(Event.CHANGE, filtrerFixeAvec);
			radioFixeSansSIM.addEventListener(Event.CHANGE, filtrerFixeSans);

			initDataMobile();
			initDataFixe();

			radioFixeSansSIM.selected=false;
			radioFixeAvecSIM.selected=false;

			radioMobileAvecSIM.selected=false;
			radioMobileSansSIM.selected=true;
		}

		private function dgVisibleChanged(ev:Event):void
		{
			if (dgListeTerm && dgListeTerm.selectedIndex > -1)
				dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		public function getSelection():Object
		{
			if (dgListeTerm.selectedIndex != -1)
			{
				return dgListeTerm.selectedItem;
			}
			else
			{
				return null;
			}
		}

		public function getIDSelection():int
		{
			if (dgListeTerm.selectedItem != null)
			{
				return dgListeTerm.selectedItem.IDTERMINAL
			}
			else
			{
				return -1;
			}
		}

		public function getDataGrid():DataGrid
		{
			return dgListeTerm;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_eqp');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		public function getLibelleSelection():String
		{
			if (dgListeTerm.selectedIndex != -1)
			{
				if (dgListeTerm.selectedItem.hasOwnProperty("NO_SERIE") && dgListeTerm.selectedItem.NO_SERIE != null && dgListeTerm.selectedItem.NO_SERIE != "")
					return dgListeTerm.selectedItem.NO_SERIE;
				else
					return dgListeTerm.selectedItem.IMEI;
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		private function initDataMobile():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_TERMMOBILEOFCOLLAB, updateDGMobile);
			serv.listeTermMobileOfCollab();
		}

		private function updateDGMobile(re:gestionparcEvent):void
		{
			var longueur:int=serv.myDatas.dataListeTermMobileOfCollab.length;
			dgListeTerm.dataProvider=serv.myDatas.dataListeTermMobileOfCollab;

			for (var index:int=0; index < longueur; index++)
			{
				if (serv.myDatas.dataListeTermMobileOfCollab[index].IMEI == SpecificationVO.getInstance().elementDataGridSelected.T_IMEI)
				{
					dgListeTerm.selectedIndex=index;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
					break;
				}
			}
			filtrerMobileSans();
		}

		private function initDataFixe():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_TERMFIXEOFCOLLAB, updateDGFixe);
			serv.listeTermFixeOfCollab();
		}

		private function updateDGFixe(re:gestionparcEvent):void
		{
			var longueur:int=serv.myDatas.dataListeTermFixeOfCollab.length;
			dgListeTerm.dataProvider=serv.myDatas.dataListeTermFixeOfCollab;

			for (var index:int=0; index < longueur; index++)
			{
				if (serv.myDatas.dataListeTermFixeOfCollab[index].IMEI == SpecificationVO.getInstance().elementDataGridSelected.T_IMEI)
				{
					dgListeTerm.selectedIndex=index
					dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
					break;
				}
			}
			filtrerFixeSans();
		}

		/* FILTRE */
		private function filtrerFixeAvec(ev:Event=null):void
		{
			dgListeTerm.dataProvider=serv.myDatas.dataListeTermFixeOfCollab;
			serv.myDatas.dataListeTermFixeOfCollab.filterFunction=filtrerFixeAvecSim;
			serv.myDatas.dataListeTermFixeOfCollab.refresh();
			dgListeTerm.selectedIndex=-1;
			refreshLibelle()
		}

		private function filtrerFixeSans(ev:Event=null):void
		{
			dgListeTerm.dataProvider=serv.myDatas.dataListeTermFixeOfCollab;
			serv.myDatas.dataListeTermFixeOfCollab.filterFunction=filtrerFixeSansSim;
			serv.myDatas.dataListeTermFixeOfCollab.refresh();
			dgListeTerm.selectedIndex=-1;
			refreshLibelle()
		}

		private function filtrerMobileAvec(ev:Event=null):void
		{
			dgListeTerm.dataProvider=serv.myDatas.dataListeTermMobileOfCollab;
			serv.myDatas.dataListeTermMobileOfCollab.filterFunction=filtrerMobileAvecSim;
			serv.myDatas.dataListeTermMobileOfCollab.refresh();
			dgListeTerm.selectedIndex=-1;
			refreshLibelle()
		}

		private function filtrerMobileSans(ev:Event=null):void
		{
			dgListeTerm.dataProvider=serv.myDatas.dataListeTermMobileOfCollab;
			serv.myDatas.dataListeTermMobileOfCollab.filterFunction=filtrerMobileSansSim;
			serv.myDatas.dataListeTermMobileOfCollab.refresh();
			dgListeTerm.selectedIndex=-1;
			refreshLibelle()
		}

		private function refreshLibelle():void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION, false));
		}

		private function filtrerMobileAvecSim(item:Object):Boolean
		{
			if (item.NUM_SIM != null && item.NUM_SIM != "" && Number(item.NUM_SIM) > 0)
			{
				if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.NUM_SIM).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
					return true
				else
					return false
			}
			else
				return false
		}

		private function filtrerMobileSansSim(item:Object):Boolean
		{
			if (item.NUM_SIM == null || item.NUM_SIM == "")
			{
				if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
					return true
				else
					return false
			}
			else
				return false
		}

		private function filtrerFixeAvecSim(item:Object):Boolean
		{
			if (item.LIGNE != null && item.LIGNE != "" && Number(item.LIGNE) > 0)
			{
				if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.SERIAL_NUMBER).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
					return true
				else
					return false
			}
			else
				return false
		}

		private function filtrerFixeSansSim(item:Object):Boolean
		{
			if (item.LIGNE == null || item.LIGNE == "")
			{
				if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
					return true
				else
					return false
			}
			else
				return false
		}
	}
}
