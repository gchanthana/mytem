<cfcomponent output="false" name="Champs">


<!--- NB: 'enregistrerChamps' ET 'updateChamps' SONT LA MEME PROCEDURE  --->

	<cffunction name="enregistrerChamps" returntype="Numeric">	
		<cfargument name="idRacine" type="Numeric" 	required="true">
		<cfargument name="champs" 	type="String" 	required="true">
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m21.enregistrerChamps">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  value="#idRacine#"/>
				<cfprocparam type="in" 	cfsqltype="CF_SQL_VARCHAR" variable="p_champs"   value="#tostring(champs)#"/>
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="updateChamps" returntype="Numeric">	
		<cfargument name="idRacine" type="Numeric" 	required="true">
		<cfargument name="champs" 	type="String" 	required="true">
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m21.enregistrerChamps">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" variable="p_idracine"  value="#idRacine#"/>
				<cfprocparam type="in" 	cfsqltype="CF_SQL_VARCHAR" variable="p_champs"   value="#tostring(champs)#"/>
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getChamps" returntype="Any">	
		<cfargument name="idRacine" type="Numeric" 	required="true">
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m21.getChamps">
				<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idRacine#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

</cfcomponent>