package catalogue.event
{
	import flash.events.Event;

	public class CatalogueEvent extends Event
	{
		public static const LISTE_EQUIPEMENT_COMPLETE:String= "listeEquipementComplete";
		public static const REMOVE_EQUIPEMENT_COMPLETE:String= "removeEquipementComplete";
		public static const REMOVE_ABO_COMPLETE:String= "removeAboComplete";
		public static const ADD_EQUIPEMENT_COMPLETE: String="addEquipementComplete";
		public static const ADD_ABO_COMPLETE: String="addAboComplete";
		public static const UPDATE_EQUIPEMENT_COMPLETE: String="updateEquipementComplete";
		public static const UPDATE_ABO_COMPLETE: String="updateAboComplete";
		public static const LISTE_TYPE_EQUIPEMENT_COMPLETE: String="listeTypeEquipementComplete";
		public static const LISTE_CLASSE_EQUIPEMENT_COMPLETE: String="listeClasseEquipementComplete";
		public static const LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE: String="listeConstructeurEquipementComplete";
		public static const LISTE_REVENDEUR_EQUIPEMENT_COMPLETE: String="listeRevendeurEquipementComplete";
		public static const LISTE_OPERATEUR_COMPLETE: String="listeOPerateurComplete";
		public static const SUBMIT_FORM: String="submitForm";
		public static const IMPORT_EQUIPEMENT_COMPLETE: String="importEquipementComplete";
		public static const SELECTE_MODELE_COMPLETE: String="selectMOdeleComplete";
		public static const PROFIL_CHANGE: String="profilChange";
		public static const LISTE_CLIENT_DISTRIBUTEUR: String="listeClientDistributeur";
		public static const CREATE_MODELE_COMPLETE: String="createModeleComplete";
		public static const EDIT_MODELE_COMPLETE: String="editModeleComplete";
		public static const REMOVE_MODELE_COMPLETE: String="removeModeleComplete";
		public static const CREATE_CATALOGUE_CLIENT_COMPLETE: String="createCatalogueClientComplete";
		public static const GET_ID_TYPEPROFIL_COMPLETE : String="getIDtypeProfilComplete";
		public static const SEGMENT_REQUEST_COMPLETE:String = "segmentRequestComplete";
		
		public static const ADD_SPECIFIC_NAME		:String = "ADD_SPECIFIC_NAME";
		public static const UPDATE_SPECIFIC_NAME	:String = "UPDATE_SPECIFIC_NAME";
		public static const UPDATE_FAILURE			:String = "UPDATE_FAILURE";
	
		private var _objectReturn:Object;
		
		public function CatalogueEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,objectReturn : Object = null)
		{
			this.objectReturn = objectReturn;
			super(type, bubbles, cancelable);
		}
		

		public function set objectReturn(value:Object):void
		{
			_objectReturn = value;
		}

		public function get objectReturn():Object
		{
			return _objectReturn;
		}
		
		override public function clone():Event
		{
			return new CatalogueEvent(type,bubbles,cancelable,objectReturn)
		}
	}
}