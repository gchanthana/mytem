package univers.facturation.grilletarif.renderer

{

    import flash.display.Graphics;
    
    import mx.controls.DataGrid;
    import mx.controls.Label;
    import mx.controls.dataGridClasses.*;

    [Style(name="backgroundColor", type="uint", format="Color", inherit="no")]
    
    public class BackgroundDgRenderer extends Label {

       override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{ 
		     super.updateDisplayList(unscaledWidth, unscaledHeight);
		 		
		     var g:Graphics = graphics;
		     g.clear();
		     var grid1:DataGrid = DataGrid(DataGridListData(listData).owner);
		     if (grid1.isItemSelected(data) || grid1.isItemHighlighted(data))
		         return;
		   
			 if (data.isValide == true) 
	             {
		         g.beginFill(0xd7e4be);
		         g.drawRect(0, 0, unscaledWidth, unscaledHeight);
		         g.endFill();
		     }
		}
	 }
}