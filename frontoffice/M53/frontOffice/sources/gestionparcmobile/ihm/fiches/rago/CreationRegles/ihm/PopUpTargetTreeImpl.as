package gestionparcmobile.ihm.fiches.rago.CreationRegles.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.managers.PopUpManager;
	
	import gestionparcmobile.ihm.fiches.rago.CreationRegles.system.Wizzard;
	import gestionparcmobile.ihm.fiches.rago.entity.Rules;
	
	[Bindable]	
	public class PopUpTargetTreeImpl extends HBox
	{
		
//VARIABLES GLOBALES----
		
		public var searchabletreeihm1:destinataireOrga;
		private var _rules:Rules;
		public var _wizzard:Wizzard;
		
		public var btChoisir:Button;
		public var btCancel:Button;
		
		public var _resultClient:Object;
		public var _resultOpe:Object;
		public var objectSelectedForTarget:Object = new Object();
		
//FIN VARIABLES GLOBALES----		
		
//PROPRIETEES PUBLICS----		

		public function set resultClient(value:Object):void
		{ _resultClient = value; }

		public function get resultClient():Object
		{ return _resultClient; }

//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----

		//Constructeur
		public function PopUpTargetTreeImpl()
		{ addEventListener("closePopUpTree",targetTreeAnnul); }

//METHODES PUBLICS----

//METHODES PROTECTED----

		//Récupération de l'XML pour l'afficher dans l'arbre
		protected function creationPopUpSourceTreeIHM(event:Event):void
		{
			searchabletreeihm1.clearSearch();			
			searchabletreeihm1._selectedOrga = resultClient.IDGROUPE_CLIENT;
			searchabletreeihm1.refreshTree();
		}
		
		//Lorsque l'on click sur le bouton valider on regarde si ce qui a été sélectionné est bien une feuille
		protected function btChoisirClickHandler(event:Event):void
		{ checkOnlyOrNot(); }
		
		//Dispatch un event si l'on click sur le bouton annuler
		protected function btCancelClickHandler(event:Event):void
		{ dispatchEvent(new Event("closePopUpTree")); }
		
		//Récupère ce qui a été sélectionné dans l'arbre
		protected function searchabletreeihm1_TargetClickHandler(event:Event):void
		{ 
			if (searchabletreeihm1.getSelectedItem() != null)
			{ 	
				objectSelectedForTarget = searchabletreeihm1.getSelectedItem();
				_wizzard.myRegle.LIBELLE_CIBLE = searchabletreeihm1.getSelectedItem().@LABEL;
				_wizzard.myRegle.IDCIBLE = searchabletreeihm1.getSelectedItem().@VALUE;
			}
		}

//FIN METHODES PROTECTED----

//METHODES PRIVATE----

		//Dispatch un event si l'on click sur le bouton annuler
		private function targetTreeAnnul(event:Event):void
		{ 
			dispatchEvent(new Event("AnnulChoiceTarget",true));
			PopUpManager.removePopUp(this); 
		}
		
		//Check si ce qui à été sélectionné est bien une feuille
		private function checkOnlyOrNot():void
		{
			if (!(objectSelectedForTarget as XML).hasSimpleContent())///////TEST Pour verifier si c'est bien l'enfant (bool)
				{ 
					//Alert.show("Veuillez séléctionner une feuille","CONSOVIEW"); 
					ConsoviewAlert.afficherOKImage("Veuillez séléctionner une feuille.",this);
				}
				else
				{
					if(searchabletreeihm1.getSelectedItem()!=null)	
					{
						btChoisir.enabled = true;
						_wizzard.myRegle.LIBELLE_CIBLE = searchabletreeihm1.getSelectedItem().@LABEL;
						_wizzard.myRegle.IDCIBLE = searchabletreeihm1.getSelectedItem().@VALUE;
					
						dispatchEvent(new Event("getChoiceTarget",true));
						PopUpManager.removePopUp(this);
					}
				}
		}

//FIN METHODES PRIVATE----

	}
}