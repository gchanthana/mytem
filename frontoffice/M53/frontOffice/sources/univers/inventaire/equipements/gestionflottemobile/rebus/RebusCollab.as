package univers.inventaire.equipements.gestionflottemobile.rebus
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	
	public class RebusCollab extends RebusCollabIHM
	{
		[Embed(source="/assets/equipement/SIM50x50.png")] 
		private var adrImgSIM : Class;
		private var dataListeSIM : ArrayCollection = new ArrayCollection();
		
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
		private var adrImgTerm : Class;
		private var dataListeTerm : ArrayCollection = new ArrayCollection();
		
		
		private var obj_cellule : Cellule;

		public function RebusCollab(cellule : Cellule)
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			this.obj_cellule = cellule;
			
		}
		private function initIHM(evt : FlexEvent):void
		{
			initDataSIM();
			initDataTerm();					
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandlerTerm);
			txtFiltreSIM.addEventListener(Event.CHANGE,txtFiltreChangeHandlerSIM);
			addEventListener(CloseEvent.CLOSE,fermer);
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			lab_emp.text = obj_cellule.libelle_cellule;
			btValider.addEventListener(MouseEvent.CLICK,valider);
			calendar.selectedDate = new Date();
			
		}
		private function initDataSIM():void
		{
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
						var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getAffectedSimToEmp",
							initResultHandler);
			
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,obj_cellule.id_cellule,PanelGestionFoltteMobile.idPoolGestionnaire);							
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeSIM= re.result as ArrayCollection;
				dgListeSIM.dataProvider = dataListeSIM;
				dataListeSIM.refresh();
				trace("SIM disponible :"+ObjectUtil.toString(re.result));
			}		
		}
		private function initDataTerm():void
		{
			
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
		 			 	var xmlParam :XML = XmlParamUtil.createXmlParamObject();
		 	XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","Groupe racine",GROUPE_INDEX);
		 	XmlParamUtil.addParam(xmlParam,"ID_EMPLOYE ","ID du collaborateur",obj_cellule.id_cellule);
		   		 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getAffectedTermToEmp",
							initResultHandlerTerm);
						
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,obj_cellule.id_cellule);		
		}
		private function initResultHandlerTerm(re : ResultEvent):void{
			if (re.result){
				dataListeTerm = re.result as ArrayCollection;
				dgListeTerm.dataProvider = dataListeTerm;
				dataListeTerm.refresh();
				trace("Terminal disponibles :"+ObjectUtil.toString(re.result));
			}		
		}
		private function txtFiltreChangeHandlerTerm(ev : Event):void
		{
			try{
				dataListeTerm.filterFunction = filtrerLeGridTerm;
				dataListeTerm.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
		private function filtrerLeGridTerm(item : Object):Boolean{
			if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(String(item.MAARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
		private function txtFiltreChangeHandlerSIM(ev : Event):void
		{
			try{
				dataListeSIM.filterFunction = filtrerLeGridSIM;
				dataListeSIM.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
		private function filtrerLeGridSIM(item : Object):Boolean{
			if ((String(item.NUM_SIM).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1)
				||
				(String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1)
				||				
				(String(item.LIGNE).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1)
				||
				(String(item.REVENDEUR).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
		private function fermer(evt : Event):void 
		{
			dispatchEvent(new Event(PanelGestionFoltteMobile.ANNULER_EVENT));
		}
		private function valider(evt : MouseEvent):void
		{
			
	
			
			var vbox_rendu : VBox = new VBox();
			var vbox_garde : VBox = new VBox();
			
			
			var listeIDEquip : String = "";
			for(var i : int = 0;i<(dgListeTerm.dataProvider as ArrayCollection).length;i++)
			{
				var unLabel : Label = new Label();
				unLabel.text = "Terminal : "+(dgListeTerm.dataProvider as ArrayCollection).getItemAt(i).IMEI;
				
				if ((dgListeTerm.dataProvider as ArrayCollection).getItemAt(i).SELECTED==1){
					listeIDEquip = (dgListeTerm.dataProvider as ArrayCollection).getItemAt(i).IDTERMINAL+","+listeIDEquip;
					
					vbox_rendu.addChild(unLabel);
				}
				else
				{
					vbox_garde.addChild(unLabel);
				}
			}
			//var listeIDSIM : String = "";
			var listeIDLigne : String = "";
			for(var a : int = 0;a<(dgListeSIM.dataProvider as ArrayCollection).length;a++)
			{
				var unLabelSIM : Label = new Label();
				unLabelSIM.text = "Carte SIM : "+(dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).NUM_SIM;
				
				if ((dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).SELECTED==1){
					if((dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).IDSOUS_TETE  != ""){
						listeIDLigne =(dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).IDSOUS_TETE+","+listeIDLigne;
						listeIDEquip = (dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).IDSIM+","+listeIDEquip;
					}
					else
					{
					listeIDEquip = (dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).IDSIM+","+listeIDEquip;
					}
					
					vbox_rendu.addChild(unLabelSIM);
				}
				else
				{
					vbox_garde.addChild(unLabelSIM);
				}
			}
			//var listeEquip : String = listeIDTerm+","+listeIDSIM;
			var PanelConf : PanelConfirmationRebus = new PanelConfirmationRebus(vbox_rendu,vbox_garde,DateFunction.formatDateAsInverseString(calendar.selectedDate),listeIDEquip,listeIDLigne,obj_cellule);
			PanelConf.addEventListener(PanelConfirmationRebus.VALIDER_EVENT,PanelGestionFoltteMobileValiderEventHandler);
			PopUpManager.addPopUp(PanelConf,this,true);
			PopUpManager.centerPopUp(PanelConf);
		
			
		}
		private function PanelGestionFoltteMobileValiderEventHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
			dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));	
		}
		

	}
}