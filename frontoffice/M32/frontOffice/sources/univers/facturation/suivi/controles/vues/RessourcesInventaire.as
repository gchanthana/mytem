package univers.facturation.suivi.controles.vues
{
	import mx.resources.ResourceManager;
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;
	import univers.facturation.suivi.controles.vo.Ressource;
	
	[Bindable]
	public class RessourcesInventaire extends HBox
	{
		public var lblQuantiteAbos : Label;
		public var lblMontantAbos : Label;	
		public var txtFiltre : TextInputLabeled; 
		public var imgCSV : Image;
		
		private var _boolInit : Boolean;
		public function get boolInit():Boolean{
			return _boolInit;
		}
		public function set boolInit(bool : Boolean):void{
			_boolInit = bool;
		}
		
		private var _helpMessage : String = ResourceManager.getInstance().getString('M32', 'Il_sagit_des_produits_des_lignes_du_comp');
		public function get helpMessage():String{
			return _helpMessage;
		}
		public function set helpMessage(message : String):void{
			_helpMessage = message;
		}
		
		private var _ctrlFactureApp : AbstractControleFacture;		 
		public function set ctrlFactureApp(ctrl : AbstractControleFacture):void{
			_ctrlFactureApp = ctrl;		
		}
		
		public function get ctrlFactureApp():AbstractControleFacture{
			return _ctrlFactureApp;
		}		
		
		public function RessourcesInventaire()
		{
			super();
			
			_boolInit = true;
		}
						
		protected function formateDates(item : Object, column : DataGridColumn):String{			
			if (item[column.dataField] != null){
				var ladate:Date = new Date(item[column.dataField]);
				return DateFunction.formatDateAsShortString(ladate);				
			}else return "-";
		}
		
		protected function formateEuros(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);	
		}
		
		protected function formateType(item : Object, column : DataGridColumn):String{
			if(String(item[column.dataField]).toLowerCase().search("abo") != -1) return "Abo"
			else return "Conso"
		}
		
		protected function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		protected function formateNumber(item : Object, column : DataGridColumn):String{
			return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
		}
		
		protected function formateEurosP4(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],4);	
		}
		
		protected function dateEntreeSortCompareFunction(itemA:Object, itemB:Object):int{
			return ObjectUtil.dateCompare(itemA.dateEntree,itemB.dateEntree);
		}
		
		protected function dateSortieSortCompareFunction(itemA:Object, itemB:Object):int{
			return ObjectUtil.dateCompare(itemA.dateSortie,itemB.dateSortie);
		}
		
		override protected function commitProperties():void{
			addEventListener(FlexEvent.SHOW,showHandler);
		} 
		
		
		//============HADNLERS =======================================================
		protected function showHandler(fe : FlexEvent):void{
			if(_boolInit){
				ctrlFactureApp.getRessourcesInventaire();
				ctrlFactureApp.getDetailRessourcesInventaire();
				_boolInit = false
			}
		}
		
		protected function imgCSVClickHandler(me : MouseEvent):void{
			if(ctrlFactureApp.listeRessourcesInv.length > 0){
				ctrlFactureApp.exportContent("DANS_INVENTAIRE");
				//ctrlFactureApp.exporterDetailRessourcesInventaire();
			}
		}
			
		
		protected function txtFiltreChangeHandler(ev : Event):void{
			if (ctrlFactureApp.listeRessourcesInv != null){
				ctrlFactureApp.listeRessourcesInv.filterFunction = filtrerLeGrid;
				ctrlFactureApp.listeRessourcesInv.refresh();
			}
		}
		//=========FIN HADNLERS =======================================================
		
		
		//Verifie si le text entree dans le textInput correspond
		private function isTxtMatching(item : Ressource):Boolean{
			if ((item.typeTheme.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||
			   (item.sousTete.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||
			   (item.libelleProduit.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||
			   (item.sousCompte.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
			   	||
			   (item.prixUnitaire.toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)){
			   	return true;			   	
		   	}else{
		   		return false;
		   	}
		}
		
		private function filtrerLeGrid(item : Ressource):Boolean{
		 	if (isTxtMatching(item)) return true;
		   	return false;
		}
		

		
	}
}