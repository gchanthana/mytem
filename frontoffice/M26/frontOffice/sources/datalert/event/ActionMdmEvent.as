package datalert.event
{
	import flash.events.Event;
	
	public class ActionMdmEvent extends Event
	{
		public static var SYNC_OK:String = "AME_SYN_OK";
		public function ActionMdmEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}