<cfcomponent author="Cedric" displayname="fr.saaswedo.api.patterns.const.Globals"
extends="fr.saaswedo.api.patterns.const.Const" hint="Définit les constantes suivantes :
<br><b>Logs :</b>
<br>- LOG.LOG: Désignation générique<br>- Patterns.LOG: API Patterns<br>- ErrorHandling.LOG: API de gestion d'erreur
<br>- Remoting.LOG: API Remoting<br>- MDM.LOG: API MDM<br>- MobileIron.LOG: Implémentation MobileIron (MDM)
<br>- Zenprise.LOG: Implémentation Zenprise (MDM)<br>- Datalert.LOG : API Datalert
<br><br>
<b>Codes d'erreurs :</b>
<br>- ERROR.ERROR: Code générique<br>- ERROR.ALL_ERRORS: Désigne tous les codes d'erreurs possibles
<br>- ErrorHandling.NO_HANDLER: Aucun handler pour l'erreur<br>- ErrorHandling.ERROR: Code générique de l'API de gestion d'erreur
<br>- Remoting.ERROR: Code générique de l'API Remoting<br>- HTTP.ERROR: Erreur dans la réponse d'un appel HTTP
<br>- HTTP.REQUEST_ERROR: Erreur durant un appel HTTP
<br>- WS.ERROR: Erreur API Remoting (Axis)<br>- WS.REQUEST_ERROR: Erreur durant un appel SOAP dans l'API Remoting (Axis)
<br>-MDM.ERROR: Générique pour API MDM<br>- MobileIron.ERROR: Code générique pour l'implémentation MobileIron (MDM)
<br>- MobileIron.SERVICE_ERROR: Erreur du serveur MobileIron
<br>- MobileIron.RESULT_ERROR: Erreur liée au résultat retourné par MobileIron (e.g Plusieurs devices pour un meme SERIAL et IMEI)
<br>- Zenprise.ERROR: Code générique pour l'implémentation Zenprise (MDM)
<br>- Zenprise.SERVICE_ERROR: Erreur du serveur Zenprise
<br>- Zenprise.RESULT_ERROR: Erreur liée au résultat retourné par Zenprise (e.g Plusieurs devices pour un meme SERIAL et IMEI)
<br><br>
<b>Constantes génériques globales :</b>
<br>- GLOBAL.TIMEOUT: Timeout générique<br>- MobileIron.CACHE_TIMEOUT: Timeout utilisé pour le cache de devices dans l'API MobileIron">
	<!--- =========== Fin des fonction publiques =========== --->
	<cffunction access="private" name="initConst" returntype="void" hint="Appelle la fonction parent puis rajoute d'autres constantes">
		<!--- Fonction d'initialisation parent --->
		<cfset SUPER.initConst()>
		<!--- Constantes génériques globales --->
		<cfset generics()>
		<!--- Constantes liées aux logs --->
		<cfset logs()>
		<!--- Codes d'erreurs --->
		<cfset errors()>
	</cffunction>

	<!--- Constantes génériques --->
	<cffunction access="private" name="logs" returntype="void" hint="Constantes utilisées comme désignation dans les logs">
		<!--- Génériques --->
		<cfset defineConst("LOG.LOG","Generic Log")>
		<!--- fr.saaswedo.api.patterns.error --->
		<cfset defineConst("ErrorHandling.LOG","API:ErrorHandling")>
		<!--- fr.saaswedo.api.patterns.error --->
		<cfset defineConst("Patterns.LOG","API:Patterns")>
		<!--- fr.saaswedo.api.remoting --->
		<cfset defineConst("Remoting.LOG","API:Remoting")>
		<!--- API MDM : fr.saaswedo.api.mdm --->
		<cfset defineConst("MDM.LOG","API:MDM")>
		<!--- API MobileIron --->
		<cfset defineConst("MobileIron.LOG","MDM:MobileIron")>
		<!--- API Zenprise --->
		<cfset defineConst("Zenprise.LOG","MDM:Zenprise")>
		<!--- API Datalert --->
		<cfset defineConst("Datalert.LOG","API:Datalert")>
	</cffunction>
	
	<!--- Codes d'erreurs --->
	<cffunction access="private" name="errors" returntype="void" hint="Constantes codes d'erreurs">
		<!--- Génériques (Le nom de la constante ERROR.ALL_ERRORS dans le code existant aurait dû etre ErrorHandling.ALL_ERRORS) --->
		<cfset defineConst("ERROR.ERROR","Generic Error")>
		<cfset defineConst("ERROR.ALL_ERRORS","ALL_ERRORS")><!--- Désigne tous les codes d'erreurs possibles --->
		<!--- fr.saaswedo.api.patterns.error --->
		<cfset defineConst("ErrorHandling.ERROR","ErrorHandling.ERROR")><!--- Code d'erreur de l'API --->
		<cfset defineConst("ErrorHandling.NO_HANDLER","ErrorHandling.NO_HANDLER")><!--- Aucun handler trouvé pour un code d'erreur --->
		<!--- fr.saaswedo.api.remoting --->
		<cfset defineConst("Remoting.ERROR","Remoting.ERROR")><!--- Code d'erreur de l'API --->
		<!--- fr.saaswedo.api.remoting.http --->
		<cfset defineConst("HTTP.ERROR","HTTP.ERROR")><!--- Erreur HTTP générique --->
		<cfset defineConst("HTTP.REQUEST_ERROR","HTTP.REQUEST_ERROR")><!--- Erreur durant un appel HTTP --->
		<!--- fr.saaswedo.api.remoting.soap : Implémentations utilisant les fonctions SOAP de ColdFusion --->
		<cfset defineConst("WS.ERROR","WS.ERROR")><!--- Erreur générique --->
		<cfset defineConst("WS.REQUEST_ERROR","WS.REQUEST_ERROR")><!--- Erreur durant un appel SOAP --->
		<!--- API MDM --->
		<cfset defineConst("MDM.ERROR","MDM.ERROR")>
		<!--- Implémentation MobileIron (MDM) --->
		<cfset defineConst("MobileIron.ERROR","MobileIron.ERROR")><!--- Code générique pour l'implémentation MobileIron --->
		<cfset defineConst("MobileIron.SERVICE_ERROR","MobileIron.SERVICE_ERROR")><!--- Erreur du serveur MobileIron --->
		<cfset defineConst("MobileIron.RESULT_ERROR","MobileIron.RESULT_ERROR")><!--- Erreur liées au résultat MobileIron --->
		<!--- Implémentation Zenprise (MDM) --->
		<cfset defineConst("Zenprise.ERROR","Zenprise.ERROR")><!--- Code générique pour l'implémentation Zenprise --->
		<cfset defineConst("Zenprise.SERVICE_ERROR","Zenprise.SERVICE_ERROR")><!--- Erreur du serveur Zenprise --->
		<cfset defineConst("Zenprise.RESULT_ERROR","Zenprise.RESULT_ERROR")><!--- Erreur liées au résultat Zenprise --->
	</cffunction>
	
	<!--- Constantes génériques globales --->
	<cffunction access="private" name="generics" returntype="void" hint="Constantes génériques">
		<cfset defineConst("GLOBAL.TIMEOUT",120)><!--- Timeout générique --->
		<!--- Timeout pour le cache de devices dans l'API MobileIron : Doit etre inférieur ou égal à GLOBAL.TIMEOUT --->
		<cfset defineConst("MobileIron.CACHE_TIMEOUT",120)>
	</cffunction>
</cfcomponent>