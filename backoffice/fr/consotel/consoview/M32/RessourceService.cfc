﻿<cfcomponent displayname="fr.consotel.consoview.M32.RessourceService" hint="Gestion des Ressource">

	<cffunction name="updateStatus" displayname="updateEtatRessource()" description="Mis à jour du statut d'un ensemble de ressources" hint="Mis à jour du satut d'un ensemble de ressources 0=cloturé, 1= non traité, 2= en cours de reclamation" access="remote" output="false" returntype="numeric">
		<cfargument name="tabIdIPR" displayName="tableau des identifiants des ressource à mettre à jour" type="array" required="true" />
		<cfargument name="statut_ipr" displayName="statut_ipr" type="numeric" hint="L'identifaint de l'état dans lequel passe les ressources" required="true" />
		
		<cfset result = 0>
		
		<!--- 
			PROCEDURE majStatut_IPR(p_idinventaire_produit IN INTEGER,
			p_statut_ipr IN INTEGER, -- 0=cloturé, 1= non traité, 2= en cours de reclamation-
			p_retour OUT INTEGER)
		 --->
		<cfloop array="#tabIdIPR#" index="idipr">
			<cfstoredproc datasource="#session.offreDSN#" procedure="Pkg_m32.majStatut_IPR">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#idipr#" null="false">
			    <cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_statut_ipr" value="#statut_ipr#" null="false">
			    <cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		    </cfstoredproc>
			<cfset result = result + p_retour>
		</cfloop>
		
		<cfreturn result>		
	</cffunction>
	
	
	<cffunction name="updateInventoryState" displayname="upadteInventoryState()" description="Mis à jour de l'état d'un ensemble de ressources" hint="Mis à jour de l'état d'un ensemble de ressources 1 = dans l'inventaire,2 = hors inventaire" access="remote" output="false" returntype="numeric">
		<cfargument name="tabIdIPR" displayName="tableau des identifiants des ressource à mettre à jour" type="array" required="true" />
		<cfargument name="inventoryState" displayName="state" type="numeric" hint="L'identifaint de l'état dans lequel passe les ressources" required="true" />
		<cfargument name="targetDate" displayName="date" type="date" hint="la date à laquelle les ressources changent d'état " required="true" />
		<cfargument name="commentaire_action" displayName="commentaire" type="string" hint="un commentaire pour l'action" required="false" />
				
		<cfset _commentaire_action = "">
		<cfif isdefined("commentaire_action")>
			<cfset _commentaire_action = commentaire_action>	
		</cfif>
		
		<cfset idinv_actions = -1>
		<cfset date_action = lsDateFormat(targetDate,'YYYY/MM/DD')>
		<cfset result = 0>
		<cfset lisIpr = arrayToList(tabIdIPR,",")>
		<cfset userId = SESSION.USER.CLIENTACCESSID>
		
		<cfif inventoryState eq 1>
			<!--- Entrer dans l'inventaire --->
			<cfset idinv_actions = 62>
			<cfelse>
			<!--- Sortir de l'inventaire --->
			<cfset idinv_actions = 61>
		</cfif>
		
		<!---<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_cyclevie_v3.createaction_horsope">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_list_idinventaire_produit" value="#lisIpr#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_actions" value="#idinv_actions#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_op_action_prec" null="yes">	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_userid" value="#userId#"/>	
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire_action" value="#_commentaire_action#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_etat_actuel" value="0"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_dans_inventaire" value="#iif(idinv_actions eq 61 , de("0"), de("1"))#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_qte" value="0"/>	
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#date_action#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>	
		</cfstoredproc>	--->	
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_cyclevie_v3.Createaction_horsope_v2">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_listeIPR" value="#lisIpr#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_IDINV_ACTIONS" value="#idinv_actions#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_IDINV_ACTIONS_PREC" null="yes">	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_USERID" value="#userId#"/>	
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_COMMENTAIRE_ACTION" value="#_commentaire_action#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_ETAT_ACTUEL" value="0"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_DANS_INVENTAIRE" value="#iif(idinv_actions eq 61 , de("0"), de("1"))#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_qte" value="0"/>	
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_action" value="#date_action#"/>	
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>	
		</cfstoredproc>
		
		<cfreturn p_retour>		
	</cffunction>
	
	
	<cffunction name="mergeProduct" displayname="updateEtatRessource()" description="Rapprochement de deux ressources" access="remote" output="false" returntype="numeric">
		<cfargument name="iprNonFacture" displayName="Ressource non facturée" type="numeric" required="true" hint="L'identifaint de la ressource dans inventaire non facturé"/>
		<cfargument name="iprFacture" displayName="Ressource facturée" type="numeric" hint="L'identifaint de la ressource hors inventaire facturé" required="true" />
		 
		
			<cfstoredproc datasource="#session.offreDSN#" procedure="Pkg_m32.R_produit_v1">
				<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_produit_old" value="#iprNonFacture#" null="false">
			    <cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_produit_new" value="#iprFacture#" null="false">
			    <cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="result">
		    </cfstoredproc>
	
		<cfreturn result>
	</cffunction>
	
	
</cfcomponent>