<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSServerGroupService" hint="Interface décrivant les méthodes du Service ServerGroup Zenprise">
	<cffunction access="public" name="addServerToServerGroup" returntype="Numeric" hint="Ajoute une serveur à un groupe de serveur et retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
	</cffunction>
	
	<cffunction access="public" name="changeServerPriority" returntype="Numeric" hint="Change la priorité d'un serveur et retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfargument name="isUpPriority" type="boolean" required="true" hint="TRUE pour une priorité élévée et FALSE sinon">
	</cffunction>
	
	<cffunction access="public" name="createServer" returntype="Numeric" hint="Crée un nouveau serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverName" type="String" required="true" hint="Nom du serveur">
		<cfargument name="serverHost" type="String" required="true" hint="Nom de machine du serveur">
		<cfargument name="serverPort" type="numeric" required="true" hint="Numéro de port du serveur">
		<cfargument name="serverIsSSL" type="boolean" required="true" hint="TRUE si le serveur utilise SSL et FALSE sinon">
		<cfargument name="serverUseProxy" type="boolean" required="true" hint="TRUE si le serveur utilise un proxy et FALSE sinon">
		<cfargument name="serverIsDefault" type="boolean" required="true" hint="TRUE si le serveur est à utiliser par défaut et FALSE sinon">
		<cfargument name="ipBegin" type="Array" required="true" hint="Tableau contenant la plage de début d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="ipEnd" type="Array" required="true" hint="Tableau contenant la plage de fin d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
	</cffunction>
	
	<cffunction access="public" name="createServerGroup" returntype="Numeric" hint="Crée un groupe de serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupName" type="String" required="true" hint="Nom du groupe de serveur">
	</cffunction>
	
	<cffunction access="public" name="deleteServer" returntype="Numeric" hint="Supprime un ou plusieurs serveurs. Retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverIds" type="Array" required="true" hint="Tableau contenant les ID des serveurs à supprimer">
	</cffunction>
	
	<cffunction access="public" name="deleteServerGroup" returntype="Numeric" hint="Supprime un groupe de serveur. Retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
	</cffunction>

	<cffunction access="public" name="getDefaultServerInfos" returntype="Array" hint="Retourne un tableau contenant les infos du serveur par défaut<br>
	Le 1er élément du tableau contient le nom d'une propriété et le 2ème élément contient la valeur correspondante et ainsi de suite">
	</cffunction>

	<cffunction access="public" name="getServerFromServerGroup" returntype="Array" hint="Retourne un tableau contenant les infos d'un serveur d'un groupe de serveur<br>
	Le 1er élément du tableau contient le nom d'une propriété et le 2ème élément contient la valeur correspondante et ainsi de suite">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
	</cffunction>

	<cffunction access="public" name="getServerGroupManager" returntype="Any" hint="N.D">
	</cffunction>

	<cffunction access="public" name="getServerListfromServerGroup" returntype="Array" hint="Retourne la liste des serveur d'un groupe de serveur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur ou -1 pour retourner tous les serveurs">
	</cffunction>
	
	<cffunction access="public" name="getServerManager" returntype="Any" hint="N.D">
	</cffunction>
	
	<cffunction access="public" name="removeServerFromServerGroup" returntype="numeric"
	hint="Supprime un ou plusieurs serveurs d'un groupe de serveur. Retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverIds" type="Array" required="true" hint="Tableau contenant les ID des serveurs à supprimer">
	</cffunction>
	
	<cffunction access="public" name="setServerGroupManager" returntype="void" hint="N.D">
		<cfargument name="serverGroupManager" type="Struct" required="true" hint="Structure représentant un groupe de serveur">
	</cffunction>
	
	<cffunction access="public" name="setServerManager" returntype="void" hint="N.D">
		<cfargument name="serverManager" type="Struct" required="true" hint="Structure représentant un serveur">
	</cffunction>
	
	<cffunction access="public" name="updateServer" returntype="Numeric" hint="Met à jour les infos d'un serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverName" type="String" required="true" hint="Nom du serveur">
		<cfargument name="serverHost" type="String" required="true" hint="Nom de machine du serveur">
		<cfargument name="serverPort" type="numeric" required="true" hint="Numéro de port du serveur">
		<cfargument name="serverIsSSL" type="boolean" required="true" hint="TRUE si le serveur utilise SSL et FALSE sinon">
		<cfargument name="serverUseProxy" type="boolean" required="true" hint="TRUE si le serveur utilise un proxy et FALSE sinon">
		<cfargument name="serverIsDefault" type="boolean" required="true" hint="TRUE si le serveur est à utiliser par défaut et FALSE sinon">
		<cfargument name="ipBegin" type="Array" required="true" hint="Tableau contenant la plage de début d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="ipEnd" type="Array" required="true" hint="Tableau contenant la plage de fin d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
	</cffunction>
	
	<cffunction access="public" name="updateServerGroup" returntype="Numeric" hint="Met à jour les infos d'un groupe de serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupName" type="String" required="true" hint="Nom du groupe de serveur">
		<cfargument name="id" type="numeric" required="true" hint="N.D">
	</cffunction>
</cfinterface>