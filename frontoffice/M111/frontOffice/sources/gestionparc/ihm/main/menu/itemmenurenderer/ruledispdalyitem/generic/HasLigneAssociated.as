package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	
	public class HasLigneAssociated implements IRuleDisplayItem
	{
		public function HasLigneAssociated()
		{
		}
		
		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO = SpecificationVO.getInstance().elementDataGridSelected;
			
			if(ligne.SOUS_TETE && ligne.SOUS_TETE != "" && ligne.SOUS_TETE.length > 0)
				return true
			else
				return false
		}
	}
}