package intranet.event
{
	import flash.events.Event;
	/**
	 * @author samuel.divioka
	 */
	public class PropagationEvent extends Event
	{
		public static const PROPAGATION_FAILED : String = "propagationFailed";
		public static var PROPAGATION_SUCCED : String = "propagationSucced";
		
		public function PropagationEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new PropagationEvent(type, bubbles, cancelable);
		}
		
		override public function toString():String
		{
			return formatToString("PropagationEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}

	
}