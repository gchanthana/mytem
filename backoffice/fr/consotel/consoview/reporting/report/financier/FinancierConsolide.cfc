<cfcomponent name="FinancierConsolide" alias="fr.consotel.consoview.reporting.report.financier.FinancierConsolide">
	<cffunction name="runReport" access="remote" output="true" returntype="numeric">
		<cfargument name="parameters" required="true" type="struct">
		<cfset biPub=createObject("component","fr.consotel.consoview.api.bi.BIPublisher")>
		<cfset biPub.validateLogin("consoview","public")>
		<cfset arrayOfParamNameValue=arrayNew(1)>
		 
		<cfset value=structNew()>
		<cfset value.name="P_IDCLICHE">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDCLICHE"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[1]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDNOEUD">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDGROUPE_CLIENT"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[2]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_TYPE_NOEUD">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_TYPE_CONSOLIDATION"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[3]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDPERIOD_DEB">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDPERIOD_DEB"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[4]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDRACINE_MASTER">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDRACINE_MASTER"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[5]=value>
		<cfset job_id=biPub.scheduleReport("/~consoview/budgetaire/Financier Mensuel Consolide/Financier Mensuel Consolide.xdo","v1",FORMAT,"OBIEE/SCHEDULED/",arrayOfParamNameValue,"consoview","public","FTP")>
		<cfreturn job_id>
	</cffunction>
</cfcomponent>
