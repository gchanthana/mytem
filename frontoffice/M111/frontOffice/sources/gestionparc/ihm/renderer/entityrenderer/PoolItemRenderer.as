package gestionparc.ihm.renderer.entityrenderer
{
	public class PoolItemRenderer extends EntityItemRenderer
	{
		public function PoolItemRenderer():void
		{
			super();
		}

		override public function set data(value:Object):void
		{
			lbLibelle.text="";

			if (value != null)
			{
				super.data=value;

				lbLibelle.text=value.POOL;
				this.img.visible=false;
				lbLibelle.useHandCursor=false;
				lbLibelle.buttonMode=false;
				lbLibelle.setStyle("textDecoration", "none");
			}
		}
	}
}