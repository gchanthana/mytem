package composants.tb.tableaux
{
	import flash.events.Event;

	public class DisplayFacureEvent extends Event
	{
		private var _numeroFacture : String;
		
		private var _indexPeriode : String;
		
		private var _affichable : Boolean = false;
		
		public function DisplayFacureEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		};
		
		
		public function set numeroFacture ( n : String):void{
			_numeroFacture = n;
		}
		
		public function set indexPeriode (i : String):void{
			_indexPeriode = i
		}
		
		public function set affichable (a : Boolean):void{
			_affichable = a;
		}
		
		public function get numeroFacture ():String{
			return	_numeroFacture;
		}
		
		public function get indexPeriode ():String{
			return _indexPeriode;
		}
		
		public function get affichable ():Boolean{
			return _affichable;
		}
	}
}