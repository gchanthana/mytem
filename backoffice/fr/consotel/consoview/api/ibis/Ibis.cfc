<cfcomponent displayname="fr.consotel.consoview.api.ibis.Ibis" hint="Module IBIS. Adapter for PublisherRequest parameters keys">
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.app.IModule" hint="Implicit Initializer : Explicit Invocation Only">
		<!--- INITIALIZATION FLAG --->
		<cfset VARIABLES["IS_INITIALIZED"]=FALSE>
		<!--- FACTORIES --->
		<cfset VARIABLES["REQUEST_FACTORY"]=createObject("component","fr.consotel.api.ibis.publisher.PublisherRequestFactory").getFactory()>
		<cfset VARIABLES["BIP_WS_FACTORY"]=createObject("component","fr.consotel.api.ibis.publisher.BipWebServiceFactory").getFactory()>
		<!--- LIST OF SERVICES --->
		<cfset VARIABLES["SERVICES"]= structNew()>
		<cfset VARIABLES["SERVICES"]["envoyer1RapportParMail"]="envoyer1RapportParMail">
		<cfset VARIABLES["SERVICES"]["scheduleReport"]="scheduleReport">
		<cfset VARIABLES["SERVICES"]["demanderRapport"]="demanderRapport">
		<!--- REQUEST PROPERTIES (DEFAULT VALUES OF REQUIRED KEY) --->
		<cfset VARIABLES["REQUEST_PROPERTIES"]=structNew()>
		<!--- REPORT PROPERTIES --->
		<cfset VARIABLES["REQUEST_PROPERTIES"]["REPORT_PROPERTIES"]=structNew()>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["REPORT_PROPERTIES"]["reportAbsolutePath"]="">
		<cfset VARIABLES["REQUEST_PROPERTIES"]["REPORT_PROPERTIES"]["attributeTemplate"]="">
		<!--- EXECUTION PROPERTIES --->
		<cfset VARIABLES["REQUEST_PROPERTIES"]["EXECUTION_PROPERTIES"]=structNew()>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["EXECUTION_PROPERTIES"]["useUTF8Option"]=TRUE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["EXECUTION_PROPERTIES"]["byPassCache"]=FALSE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["EXECUTION_PROPERTIES"]["attributeLocale"]="fr_FR">
		<cfset VARIABLES["REQUEST_PROPERTIES"]["EXECUTION_PROPERTIES"]["jobLocale"]=VARIABLES["REQUEST_PROPERTIES"]["EXECUTION_PROPERTIES"]["attributeLocale"]>
		<!--- OUTPUT PROPERTIES : ftpUserName,ftpUserPassword --->
		<cfset VARIABLES["REQUEST_PROPERTIES"]["OUTPUT_PROPERTIES"]=structNew()>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["OUTPUT_PROPERTIES"]["FTP"]=structNew()>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["OUTPUT_PROPERTIES"]["FTP"]["remoteFile"]="">
		<cfset VARIABLES["REQUEST_PROPERTIES"]["OUTPUT_PROPERTIES"]["FTP"]["attributeFormat"]="xml">
		<cfset VARIABLES["REQUEST_PROPERTIES"]["OUTPUT_PROPERTIES"]["FTP"]["sftpOption"]=FALSE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["OUTPUT_PROPERTIES"]["FTP"]["scheduleBurstringOption"]=FALSE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["OUTPUT_PROPERTIES"]["FTP"]["ftpServerName"]="">
		<!--- HISTORY PROPERTIES --->
		<cfset VARIABLES["REQUEST_PROPERTIES"]["HISTORY_PROPERTIES"]=structNew()>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["HISTORY_PROPERTIES"]["userJobName"]="IBIS">
		<cfset VARIABLES["REQUEST_PROPERTIES"]["HISTORY_PROPERTIES"]["saveDataOption"]=FALSE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["HISTORY_PROPERTIES"]["saveOutputOption"]=FALSE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["HISTORY_PROPERTIES"]["schedulePublicOption"]=TRUE>
		<!--- NOTIFICATION PROPERTIES : notificationTo --->
		<cfset VARIABLES["REQUEST_PROPERTIES"]["NOTIFICATION_PROPERTIES"]=structNew()>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["NOTIFICATION_PROPERTIES"]["notifyWhenSuccess"]=TRUE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["NOTIFICATION_PROPERTIES"]["notifyWhenWarning"]=TRUE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["NOTIFICATION_PROPERTIES"]["notifyWhenFailed"]=TRUE>
		<cfset VARIABLES["REQUEST_PROPERTIES"]["NOTIFICATION_PROPERTIES"]["httpNotificationServer"]="IBIS">
		<!--- BIP CREDENTIALS --->
		<cfset VARIABLES["BIP_USER"]="consoview">
		<cfset VARIABLES["BIP_USER_KEY"]="public">
		<cfset VARIABLES["IS_INITIALIZED"]=TRUE>
		<cfreturn THIS>
		
		<!--- 
		<!--- Default parameter values --->
		<cfset VARIABLES["DEFAULT_LOCALE"]="fr_FR">
		<cfset VARIABLES["USE_UTF8"]=TRUE>
		<cfset VARIABLES["BYPASS_DOC_CACHE"]=FALSE>
		<cfset VARIABLES["SAVE_DATA"]=FALSE>
		<cfset VARIABLES["SAVE_OUTPUT"]=FALSE>
		<cfset VARIABLES["PUBLIC_JOB"]=TRUE>
		<cfset VARIABLES["NOTIFY_FAILED"]=TRUE>
		<cfset VARIABLES["NOTIFY_WARNING"]=TRUE>
		<cfset VARIABLES["NOTIFY_JOB"]=TRUE>
		<cfset VARIABLES["NOTIFY_EMAILTO"]="">
		<cfset VARIABLES["OUTPUT_FORMAT"]="xml">
		<!--- Initialization flag --->
		<cfset VARIABLES["IS_INITIALIZED"]=TRUE>
		<!--- FTP Server Infos --->
		<cfset VARIABLES["FTP_SERVER_NAME"]="PELICAN">
		<cfset VARIABLES["SFTP_ENABLED"]=FALSE>
		<cfset VARIABLES["FTP_USERS"]=structNew()>
		<cfset VARIABLES["FTP_USERS"]["services"]=structNew()>
		<cfset VARIABLES["FTP_USERS"]["services"]["USER_LOGIN"]="services">
		<cfset VARIABLES["FTP_USERS"]["services"]["USER_KEY"]="services">
		<cfset VARIABLES["FTP_USERS"]["services"]["HOME_DIR"]="/app/pelican/services/">
		<cfset VARIABLES["FTP_USERS"]["container"]=structNew()>
		<cfset VARIABLES["FTP_USERS"]["container"]["USER_LOGIN"]="container">
		<cfset VARIABLES["FTP_USERS"]["container"]["USER_KEY"]="container">
		<cfset VARIABLES["FTP_USERS"]["container"]["HOME_DIR"]="/app/pelican/container/">
		<cftrace type="information" text="initInstance() : Initialization SUCESS">
		 --->
	</cffunction>
	
	<cffunction access="public" name="invokeService" returntype="any" hint="Invokes service corresponding to serviceID with providen parameters">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
		<cfset var udfName="">
		<cfif isInitialized()>
			<cfset udfName=VARIABLES["SERVICES"][ARGUMENTS["serviceId"]]>
			<cftry>
				<cfset EVALUATE("#udfName#(ARGUMENTS.parameters)")>
				<cfreturn TRUE>
				<cfcatch type="any">
					<cfthrow type="Custom" message="invokeService() : Unable to execute operation on BIP webservice">
				</cfcatch>
			</cftry>
		<cfelse>
			<cfthrow type="Custom" message="invokeService() : This instance is not initialized">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="isInitialized" returntype="boolean" hint="Returns TRUE if this instance is initialized. FALSE otherwise">
		<cfif structKeyExists(VARIABLES,"IS_INITIALIZED")>
			<cfreturn VARIABLES["IS_INITIALIZED"]>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>

	<cffunction access="private" name="scheduleReport" returntype="any" hint="Executes scheduleReport Service. Returns a strict positive numeric value if SUCCESS (JOB_ID). FAILURE otherwise">
		<cfargument name="parameters" type="struct" required="true" hint="Struct which contains parameters keys values">
		<cfset var bipRequest=VARIABLES["REQUEST_FACTORY"].createRequest()>
		<cfset var requestProperties=adaptParameters(ARGUMENTS["parameters"])>
		<cfset var fillStatus=bipRequest.setReportProperties(requestProperties["REPORT_PROPERTIES"]) AND
								bipRequest.setExecutionProperties(requestProperties["EXECUTION_PROPERTIES"]) AND
								bipRequest.setOutputProperties(requestProperties["OUTPUT_PROPERTIES"]) AND
								bipRequest.setHistoryProperties(requestProperties["HISTORY_PROPERTIES"]) AND
								bipRequest.setNotificationProperties(requestProperties["NOTIFICATION_PROPERTIES"])>
		<cfset var bipWS=VARIABLES["BIP_WS_FACTORY"].getInstance()>
		<cftrace type="information" text="scheduleReport() - Called with BIP USER : #VARIABLES.BIP_USER#">
		<cfset var jobId=bipWS.scheduleReport(bipRequest.getScheduleRequest(),VARIABLES["BIP_USER"],VARIABLES["BIP_USER_KEY"])>
		<cftrace type="information" text="scheduleReport() - Returned JOB_ID : #jobId#">
		<cfif fillStatus EQ FALSE>
			<cfthrow type="Custom" message="scheduleReport() : Unable to update PublisherRequest">
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>

	<cffunction access="private" name="adaptParameters" returntype="struct" hint="Adapts parameters keys for PublisherRequest. Returns an updated DEEP COPY of REQUEST_PROPERTIES prototype">
		<cfargument name="parameters" type="struct" required="true" hint="Parameters structure according to fr.consotel.consoview.api.CV">
		<cfset var adaptedParams=DUPLICATE(VARIABLES["REQUEST_PROPERTIES"])>
		<cfset var sourceParams=ARGUMENTS["parameters"]>
		<cfset var deliveryProperties="">
		<cftry>
			<!--- REPORT_PROPERTIES --->
			<cfset adaptedParams["REPORT_PROPERTIES"]["reportAbsolutePath"]=sourceParams["bipReport"]["xdoAbsolutePath"]>
			<cfset currentKey="xdoTemplateId">
			<cfif structKeyExists(sourceParams["bipReport"],currentKey)>
				<cfset adaptedParams["REPORT_PROPERTIES"]["attributeTemplate"]=sourceParams["bipReport"][currentKey]>
			<cfelse>
				<cfset structDelete(adaptedParams["REPORT_PROPERTIES"],"attributeTemplate",TRUE)>
			</cfif>
			<!--- EXECUTION_PROPERTIES --->
			<cfset currentKey="localization">
			<cfif structKeyExists(sourceParams["bipReport"],currentKey)>
				<cfset adaptedParams["EXECUTION_PROPERTIES"]["attributeLocale"]=sourceParams["bipReport"][currentKey]>
			</cfif>
			<!--- OUTPUT_PROPERTIES --->
			<cfset currentKey="outputFormat">
			<cfset deliveryProperties=sourceParams["bipReport"]["delivery"]>
			<cfset outputFormat=UCASE(deliveryProperties["channel"])>
			<cfset adaptedParams["OUTPUT_PROPERTIES"]=DUPLICATE(adaptedParams["OUTPUT_PROPERTIES"][outputFormat])>
			<!--- TODO : PREFIX WITH FTP USER HOME DIRECTORY WITH ABSOLUTE PATH --->
			<cfset adaptedParams["OUTPUT_PROPERTIES"]["ftpServerName"]=UCASE(deliveryProperties["parameters"]["ftpUser"])>
			<cfset adaptedParams["OUTPUT_PROPERTIES"]["remoteFile"]=deliveryProperties["parameters"]["fileRelativePath"]>
			<cfif structKeyExists(sourceParams["bipReport"],currentKey)>
				<cfset adaptedParams["OUTPUT_PROPERTIES"]["attributeFormat"]=sourceParams["bipReport"][currentKey]>
			</cfif>
			<!--- HISTORY_PROPERTIES --->
			<!--- XDO REPORT PARAMETERS --->
			<cfcatch type="any">
				<cfthrow type="Custom" message="adaptParameters() : Unable to adapt parameters">
			</cfcatch>
		</cftry>
	<!--- PARAMETERS KEY LIST REFERENCE : (http://nathalie.consotel.fr:3000/issues/418)
		================= Parameters structure =================
		{
			"EVENT_TARGET" = "(STRING) EVENT_TARGET element. DEFAULT : EMPTY STRING. MAX LENGTH : 35",
			"EVENT_HANDLER" = "(STRING) EVENT_HANDLER element. DEFAULT : EMPTY STRING. MAX_LENGTH : 3",
			"EVENT_TYPE" = "(STRING) EVENT_TYPE element. DEFAULT : EMPTY STRING. MAX_LENGTH : 3",
			"bipReport" = { BIP Report properties :
				"reportParameters" = { List of XDO report parameters :
					"PARAMETER_NAME" = { XDO parameter name
						[Array of PARAMETER_NAME values]
					}
				},
				"delivery" = { Delivery properties
					"channel" = "Channel. Available values : FTP",
					"parameters" = { Channel parameters
						Depends on channel value
					}
				}
			}
		}
		================= FTP channel parameters =================
		{
			"ftpUser" = "FTP USERNAME (Case Insensitive). Available values : SERVICES, CONTAINER",
			"fileRelativePath" = "Report output file FTP USER relative path"
		}
	 --->
		<cfreturn adaptedParams>
	</cffunction>

	<cffunction access="private" name="getDeliveryChannel" returntype="string" hint="Returns the value of CHANNEL key in parameters according to fr.consotel.consoview.api.CV">
		<cfargument name="parameters" type="struct" required="true" hint="Source parameters according to fr.consotel.consoview.api.CV">
		<cfif structKeyExists(ARGUMENTS["parameters"],"bipReport")>
		</cfif>
		<cfset var adaptedParams=DUPLICATE(VARIABLES["REQUEST_PROPERTIES"])>
		<cfreturn adaptedParams>
	</cffunction>
	
	
	
	
	<!--- *** LIST OF SERVICE FUNCTIONS *** 
	<cffunction access="private" name="scheduleReport" returntype="any" hint="Schedule Report">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
		<cfset var structParam=ARGUMENTS["parameters"]>
		<cfset var scheduleRequest=VARIABLES["REQUEST_FACTORY"].createRequest()>
		<cfset var currentStruct="">
		<cfset var reportProps="">
		<cfset var reportParameters="">
		<cfset var executionProps="">
		<cfset var outputProps="">
		<cftry>
			<!--- KEY : bipReport --->
			<cfif structKeyExists(structParam,"bipReport")>
				<cfset currentStruct=structParam["bipReport"]>
				<cfif structKeyExists(currentStruct,"xdoAbsolutePath")>
					<cfset reportProps=structNew()>
					<cfset reportProps["reportAbsolutePath"]=currentStruct["xdoAbsolutePath"]>
					<cfif structKeyExists(currentStruct,"xdoTemplateId")>
						<cfset reportProps["attributeTemplate"]=currentStruct["xdoTemplateId"]>
					</cfif>
					<cfif structKeyExists(currentStruct,"reportParameters")>
						<cfset reportParameters=currentStruct["reportParameters"]>
						<cfloop index="paramName" list="#structKeyList(reportParameters)#">
							<cfif structKeyExists(reportParameters[paramName],"parameterValues") AND isValid("Array",reportParameters[paramName]["parameterValues"])>
								<cfset scheduleRequest.addParameter(paramName,reportParameters[paramName]["parameterValues"])>
							</cfif>
						</cfloop>
					</cfif>
					<cfset executionProps=structNew()>
					<cfset executionProps["useUTF8Option"]=VARIABLES["USE_UTF8"]>
					<cfset executionProps["byPassCache"]=VARIABLES["BYPASS_DOC_CACHE"]>
					<cfset executionProps["attributeLocale"]=VARIABLES["DEFAULT_LOCALE"]>
					<cfif structKeyExists(currentStruct,"localization")>
						<cfset executionProps["attributeLocale"]=currentStruct["localization"]>
					</cfif>
					<cfset outputProps=structNew()>
					<cfset outputProps["attributeFormat"]=VARIABLES["OUTPUT_FORMAT"]>
					<cfif structKeyExists(currentStruct,"outputFormat")>
						<cfset outputProps["attributeFormat"]=currentStruct["outputFormat"]>
					</cfif>
					<cfif structKeyExists(currentStruct,"delivery")>
						<cfif structKeyExists(currentStruct["delivery"],"channel") AND structKeyExists(currentStruct["delivery"],"parameters")>
							<cfif UCASE(currentStruct["delivery"]["channel"]) EQ "FTP">
								<cfif structKeyExists(currentStruct["delivery"]["parameters"],"ftpUser") AND structKeyExists(currentStruct["delivery"]["parameters"],"fileRelativePath")>
									<cfset outputProps["ftpServerName"]=VARIABLES["FTP_SERVER_NAME"]>
									<cfset outputProps["ftpUserName"]=VARIABLES["FTP_USERS"]["services"]["USER_LOGIN"]>
									<cfset outputProps["ftpUserPassword"]=VARIABLES["FTP_USERS"]["services"]["USER_KEY"]>
									<cfset outputProps["sftpOption"]=VARIABLES["SFTP_ENABLED"]>
									<cfset outputProps["remoteFile"]=VARIABLES["FTP_USERS"]["services"]["HOME_DIR"] & currentStruct["delivery"]["parameters"]["fileRelativePath"]>
								<cfelse>
									<cfthrow type="Custom" message="scheduleReport() - Missing KEY : bipReport.delivery.parameters.ftpUser OR bipReport.delivery.parameters.fileRelativePath">
								</cfif>
							<cfelse>
								<cfthrow type="Custom" message="scheduleReport() - Invalid channel value : #currentStruct.delivery.channel#">
							</cfif>
							<cfset outputProps[""]=currentStruct["delivery"]>
						<cfelse>
							<cfthrow type="Custom" message="scheduleReport() - Missing KEY : bipReport.delivery.channel OR bipReport.delivery.parameters">
						</cfif>

						<!---
		<cfset VARIABLES["SAVE_DATA"]=FALSE>
		<cfset VARIABLES["SAVE_OUTPUT"]=FALSE>
		<cfset VARIABLES["PUBLIC_JOB"]=TRUE>
		<cfset VARIABLES["NOTIFY_FAILED"]=TRUE>
		<cfset VARIABLES["NOTIFY_WARNING"]=TRUE>
		<cfset VARIABLES["NOTIFY_JOB"]=TRUE>
		<cfset VARIABLES["NOTIFY_EMAILTO"]="">
						
<cfset outputProps=structNew()>
<cfset outputProps["emailFrom"]="api">
<cfset outputProps["emailTo"]="cedric.rapiera">
<cfset outputProps["emailSubject"]="Test">
<cfset outputProps["attributeFormat"]="excel">
<cfset outputProps["contentType"]="application/vnd.ms-excel">
<cfset outputProps["scheduleBurstringOption"]=TRUE>
<cfset req.setOutputProperties(outputProps)>
						 --->
					</cfif>
				<cfelse>
					<cfthrow type="Custom" message="scheduleReport() - Missing KEY : bipReport.xdoAbsolutePath">
				</cfif>
			<cfelse>
				<cfthrow type="Custom" message="scheduleReport() - Missing KEY : bipReport">
			</cfif>
			<!--- Fill ScheduleRequest --->
			<cfset scheduleRequest.setReportProperties(reportProps)>
			<cfset scheduleRequest.setExecutionProperties(executionProps)>
			<cfset scheduleRequest.setOutputProperties(outputProps)>
			<cfcatch type="any">
				<cfset VARIABLES["LOGGER"].logException("ERROR","scheduleReport() : An error has occurred",CFCATCH)>
				<cfthrow type="Custom" message="scheduleReport() : ERROR">
			</cfcatch>
		</cftry>
		<cfdump var="#scheduleRequest.getScheduleRequest()#">
		<cfreturn scheduleRequest>
	</cffunction>
		
	<cffunction access="private" name="envoyer1RapportParMailParams" returntype="any" hint="Deprecated Service">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
		<cfthrow type="Custom" message="DEPRECATED SERVICE : envoyer1RapportParMail">
	</cffunction>
	
	<cffunction access="private" name="demanderRapportParams" returntype="any" hint="Deprecated Service">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
		<cfthrow type="Custom" message="DEPRECATED SERVICE : demanderRapport">
	</cffunction>
	*** END OF SERVICE FUNCTIONS *** --->
	
	<!---
	<cffunction access="public" name="invokeService" returntype="any" hint="Invokes service corresponding to serviceID with providen parameters">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID">
		<cfargument name="parameters" type="struct" required="true" hint="List of parameters wrapper. An empty structure means no parameters to pass">
		<cfset var serviceManager="NULL">
		<cflog type="information" text=">>> IBIS SERVICE INVOCATION [#arguments.serviceId#](#structKeyList(arguments.parameters)#)">
		<cfif structKeyExists(VARIABLES["SERVICES"],arguments["serviceId"]) AND (VARIABLES["SERVICES"][arguments["serviceId"]] NEQ "NULL")>
			<cfset serviceManager=createObject("component",VARIABLES["SERVICES"][arguments["serviceId"]]).initInstance()>
			<cfreturn serviceManager.invokeMethod(arguments["serviceId"],arguments["parameters"])>
		<cfelse>
			<cflog type="error" text="Undefined Service in IBIS module : #arguments.serviceId#">
			<cfabort showerror="UNDEFINED serviceId [#arguments.serviceId#] : SEE LOG FILES FOR DETAILS">
		</cfif>
	</cffunction> 
	 --->
</cfcomponent>
