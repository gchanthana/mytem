package composants.tb.ligne.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class FicheTicketHandler
	{
		
		private var _model:FicheTicketModel;
		
		public function FicheTicketHandler(model:FicheTicketModel)
		{
			_model = model;
		}

		public function get model():FicheTicketModel
		{
			return _model;
		}

		public function set model(value:FicheTicketModel):void
		{
			_model = value;
		}
		
		internal function getFicheTicketResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateFicheTicket(event.result as ArrayCollection);
			}
		}

	}
}