package gestioncommande.ihm.commandemobilefixereseau
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.CommandeFormatter;
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.composants.PanierIHM;
	import gestioncommande.composants.PiecesJointesMinIHM;
	import gestioncommande.custom.CustomButtonWizzard;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.ihm.commandemobilefixereseau.button.Etape11IHM;
	import gestioncommande.ihm.commandemobilefixereseau.button.Etape21IHM;
	import gestioncommande.ihm.commandemobilefixereseau.button.Etape31IHM;
	import gestioncommande.ihm.commandemobilefixereseau.button.Etape41IHM;
	import gestioncommande.ihm.commandemobilefixereseau.button.Etape51IHM;
	import gestioncommande.ihm.commandemobilefixereseau.button.Etape61IHM;
	import gestioncommande.ihm.commandemobilefixereseau.button.Etape71IHM;
	import gestioncommande.iinterface.IWizzard;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpConfirmationCommandeIHM;
	import gestioncommande.services.CommandeArticles;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class MainNouvelleCommandeImpl extends Box implements IWizzard
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var vs							:ViewStack;
		
		public var txtAreaCommentaires			:TextArea;
		
		public var hbxButtonWizzard				:HBox;
		
		public var cbxRevendeur					:ComboBox;
		
		//---> IHM - BOUTON VIEWSTACK
		public var btPre						:Button;
		public var btNext						:Button;
		public var btValid						:Button;
		public var btCancel						:Button;
		public var btCancelConfig				:Button;
		
		//---> IHM - BOUTON WIZZARD
		public var btn1							:CustomButtonWizzard;
		public var btn2							:CustomButtonWizzard;
		public var btn3							:CustomButtonWizzard;
		public var btn4							:CustomButtonWizzard;
		public var btn5							:CustomButtonWizzard;
		
		public var  etape1						:Etape11IHM;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _pUpMailBox					:ActionMailBoxIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					COMPONENTS
		//--------------------------------------------------------------------------------------------//
		
		public var composantPanier				:PanierIHM;
		public var piecesJointes				:PiecesJointesMinIHM;
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		private var _infosMail					:InfosObject 			= new InfosObject();
		
		private var _selectedAction				:Action 				= new Action();		
		
		private var _cmd						:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _commandeServ					:CommandeService		= new CommandeService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		//---> CHILDREN VIEWSTACK
		public var  etape2						:Etape21IHM = new Etape21IHM();
		public var  etape3						:Etape31IHM = new Etape31IHM();
		public var  etape4						:Etape41IHM = new Etape41IHM();
		public var  etape5						:Etape51IHM = new Etape51IHM();
		public var  etape6						:Etape61IHM = new Etape61IHM();
		public var  etape7						:Etape71IHM = new Etape71IHM();
		public var  boolFaireNouvelleCommande	:Boolean = false;
		private var _isRecorded					:Boolean = false;
		
		private var _articles					:Array;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
	
		public function MainNouvelleCommandeImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A CETTE CLASSE
//--------------------------------------------------------------------------------------------//

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					LISTENER DE L'IHM
		//--------------------------------------------------------------------------------------------//

		public function clickButtonPreviousHandler(me:MouseEvent):void
		{
			decompteVs();
			refreshPanier();
		}
					
		public function clickButtonNextHandler(me:MouseEvent):void
		{
			var isOk:Boolean = (vs.getChildAt(vs.selectedIndex) as IWorflow).checkData();
			
			if(vs.selectedIndex == 0)
				whatIsTypeCommande();
			
			if(isOk)
				compteVs();
		}

		public function clickButtonValidHandler(me:MouseEvent):void
		{
			if(!_isRecorded)
			{
				if(etape7.mailCommande.myAction != null &&  etape7.mailCommande.myAction.IDACTION > 0)
				{
					if(etape7.mailCommande.checkMail())
					{
						if(etape7.mailCommande.myAction.CODE_ACTION == "EMA3T")
						{ 
							var poUpConfir:PopUpConfirmationCommandeIHM = new PopUpConfirmationCommandeIHM();
								poUpConfir.addEventListener("sendCommande", registerCommande);
							
							PopUpManager.addPopUp(poUpConfir,this,true);
							PopUpManager.centerPopUp(poUpConfir);
							
						}
						else
							registerCommande();
					}
				}
				else
					registerCommande();
			}
			else
				ConsoviewAlert.afficherAlertConfirmation(resourceManager.getString('M16','Commande_d_j__enregistr_e___Voulez_vous_aller_sur_la'), 'Consoview', commandeIsRecorded);
		}

		private function commandeIsRecorded(ce:CloseEvent):void
		{
			if(ce.detail == 4)
				dispatchEvent(new ViewStackEvent(ViewStackEvent.COMMANDE_BUILT, true));
		}
		
		public function clickButtonCancelHandler(me:MouseEvent):void
		{
			dispatchEvent(new ViewStackEvent(ViewStackEvent.ANNUL_COMMANDE, true));
		}
		
		protected function composantPanierCreationCompleteHandler(e:Event):void
		{
			refreshPanier();
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					LISTENER
		//--------------------------------------------------------------------------------------------//

		public function creationCompleteHandler(fe:FlexEvent):void
		{
			SessionUserObject.singletonSession.COMMANDE 			= new Commande(true);
			SessionUserObject.singletonSession.COMMANDEARTICLE		= new ArrayCollection();
			SessionUserObject.singletonSession.ACTION				= null;
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			
			SessionUserObject.singletonSession.COMMANDE.IDROLE = 101;
			
			SessionUserObject.singletonSession.COMMANDE.addEventListener(CommandeEvent.INFOS_COMMANDE, infosCommandeHandler);
			
			addChildrenElement();
			enabledDisableButtonWizzard();
			buttonNavigationEnableDisable();
			
			//---> PAS D'ACCESSOIRES ALORS ON SAUTE CETTE ETAPE
			addEventListener('NO_ACCESSORIES_NEXT', noAccessoriesHandler);
			addEventListener('NO_ACCESSORIES_PREV', noAccessoriesHandler);
			
			//---> LORSQUE L'ON CHANGE  UN DE CES 5 ELEMENTS ALORS ON BLOQUE LE WIZZARD
			addEventListener('MOBILE_CHANGED', dataCommandeChanged);
			addEventListener('PROFILE_CHANGED', dataCommandeChanged);
			addEventListener('REVENDEUR_CHANGED', dataCommandeChanged);
			addEventListener('OPERATEUR_CHANGED', dataCommandeChanged);
			addEventListener('TYPE_CMD_CHANGED', dataCommandeChanged);
			
			addEventListener('VALIDER_ENABLED', enableBtnValiderHandler);
			addEventListener('VALID_CONTINU', btValidAndPurchaseClickHandler);
			addEventListener('PRICE_CHANGE', calculPriceHandler);
		}
		
		private function infosCommandeHandler(cmde:CommandeEvent):void
		{
			etape1.cmd = SessionUserObject.singletonSession.COMMANDE;
			etape1.autoCompleteRefClient();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function btCancelConfigClickHandler(me:MouseEvent):void
		{
			btCancelConfig.visible = false;
			vs.selectedIndex = 6;
			
			refreshPanier();
			
			var elements	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var elts		:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			var len			:int = elements.length;
			var idrand		:int = elts.RANDOM;
			
			if(elements.contains(elts))
			{
				for(var h:int = 0;h < len;h++)
				{
					if(SessionUserObject.singletonSession.COMMANDEARTICLE[h].RANDOM == idrand)
					{
						SessionUserObject.singletonSession.COMMANDEARTICLE.removeItemAt(h);
						break;
					}
				}
			}
			
			len = SessionUserObject.singletonSession.COMMANDEARTICLE.length;
			
			if(len > 0)
				SessionUserObject.singletonSession.CURRENTCONFIGURATION = elements[len-1];
			
			if(len == 1)
				etape1.disableAll(true);
			
			SessionUserObject.singletonSession.COMMANDE.CONFIG_NUMBER = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TYPE;
			
			whatIsTypeCommande();
			
			var idint:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).setConfiguration();
			}
			
			enabledDisableButtonWizzard();
		}
		
		protected function btn1ClickHandler(me:MouseEvent):void
		{
			vs.selectedIndex = 0;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		protected function btn2ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(1);
		}
		
		protected function btn3ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(3);
		}
		
		protected function btn4ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(4);
		}
		
		protected function btn5ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(6);
		}
	
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//
		
		private function validatePreviousPage(pageMax:int):void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			var childError	:int = -1;
			
			for(var i:int = 0;i < pageMax;i++)
			{
				if((children[i] as Object).ACCESS)
				{
					if(!(children[i] as Object).checkData())
					{
						childError = i;
						break;
					}
				}
			}
			
			if(childError > 0)
				vs.selectedIndex = childError;
			else
				vs.selectedIndex = pageMax;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					GESTION WIZZARD
		//--------------------------------------------------------------------------------------------//
		
		private function addChildrenElement():void
		{
			vs.addChild(etape2);
			vs.addChild(etape3);
			vs.addChild(etape4);
			vs.addChild(etape5);
			vs.addChild(etape6);
			vs.addChild(etape7);
		}
		
		private function whatIsTypeCommande():void
		{
			var accessed	:Array = new Array(false,false,false,false,false,false,false,false);
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			var idty:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1)
			{
				accessed = new Array(true,true,true,true,true,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				accessed = new Array(true,false,false,true,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 3)
			{
				accessed = new Array(true,true,true,false,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 4)
			{
				accessed = new Array(true,false,false,false,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 10)
			{
				accessed = new Array(true,true,true,false,true,true,true);
			}
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).ACCESS = accessed[i];
			}
		}
		
		private function compteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = vs.selectedIndex+1;i < lenChild;i++)
			{
				if((children[i] as Object).ACCESS)
				{
					vs.selectedIndex = i;
					break;
				}
			}
			
			(vs.getChildAt(vs.selectedIndex) as IWorflow).viewStackChange();
			
			enabledDisableButtonWizzard();
		}
		
		private function decompteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = vs.selectedIndex-1;i > -1;i--)
			{
				if((children[i] as Object).ACCESS)
				{
					vs.selectedIndex = i;
					break;
				}
			}
			
			(vs.getChildAt(vs.selectedIndex) as IWorflow).viewStackChange();
			
			enabledDisableButtonWizzard();
		}
		
		private function enabledDisableButtonWizzard():void
		{
			var children	:Array = hbxButtonWizzard.getChildren();
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(vs.selectedIndex) as Object).IDPAGE;
			var isCurrent	:Boolean = false;
			
			for(var i:int = lenChild;i > -1;i--)
			{
				if(children[i] as CustomButtonWizzard)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					if(idButton <= idPage)
					{
						var childrenVs	:Array = vs.getChildren();
						
						for(var j:int = vs.selectedIndex;j > -1;j--)
						{
							if((childrenVs[j] as Object).IDPAGE == idButton)
								(children[i] as Button).enabled = (childrenVs[j] as Object).ACCESS;	
						}
					}
					else
						myButton.enabled = false;
				}
			}
			
			buttonWizzardColor();
			buttonNavigationEnableDisable();
		}
		
		private function buttonNavigationEnableDisable():void
		{
			refreshPanier();
			
			if(vs.selectedIndex == 0)
			{
				btPre.visible 	= btPre.includeInLayout = false;
				btNext.visible 	= btNext.includeInLayout = true;
				btValid.visible = btValid.includeInLayout = false;
			}
			else if(vs.selectedIndex > 0)
			{
				if(vs.selectedIndex == 6)
				{
					btPre.visible 	= btPre.includeInLayout = true;
					btNext.visible 	= btNext.includeInLayout = false;
					btValid.visible = btValid.includeInLayout = true;
				}
				else
				{
					btPre.visible 	= btPre.includeInLayout = true;
					btNext.visible 	= btNext.includeInLayout = true;
					btValid.visible = btValid.includeInLayout = false;
				}
			}
		}
		
		private function buttonWizzardColor():void
		{
			var children	:Array = hbxButtonWizzard.getChildren();
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(vs.selectedIndex) as Object).IDPAGE;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				if(children[i] as Button)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					var childrenVs	:Array = vs.getChildren();
					var lenVs		:int = childrenVs.length;
					
					for(var j:int = 0;j < lenVs;j++)
					{
						if((childrenVs[j] as Object).IDPAGE == idButton)
						{
							if(idButton == idPage)
								myButton.styleName = "btnWizzardActif";
							else
							{
								if(!(childrenVs[j] as Object).ACCESS)
								{
									myButton.styleName = "btnWizzardDesactivate";
								}
								else
								{
									if(myButton.enabled)
										myButton.styleName = "btnWizzardInactif";
									else
										myButton.styleName = "btnWizzardDesactivate";
								}
							}
						}
					}
				}
			}
		}
		
		private function refreshPanier():void
		{
			if(vs.selectedIndex == 6)
			{
				composantPanier.attributMontantConfirme();
				composantPanier.attributMontantEnPreparation(true);
				composantPanier.attributMontantTotal(true);
			}
			else
			{
				composantPanier.attributMontantEnPreparation();	
				composantPanier.attributMontantTotal();
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - ENREGISTREMENT DE LA COMMANDE
		//--------------------------------------------------------------------------------------------//
		
		public function registerCommande(e:Event = null):void
		{
			this._articles 	= addArticles();
			
			_cmd = SessionUserObject.singletonSession.COMMANDE;
			
			_cmd.COMMENTAIRES = txtAreaCommentaires.text;

			if(_articles.length > 0)
			{
				if(etape7.mailCommande.myAction != null && etape7.mailCommande.myAction.IDACTION > 0)
				{
					etape7.mailCommande.myAction.COMMENTAIRE_ACTION = etape7.txaCommentaires.text;
					etape7.mailCommande.myAction.DATE_ACTION 		= new Date();
					etape7.mailCommande.myAction.DATE_HOURS		 	= 	Formator.formatReverseDate(etape7.mailCommande.myAction.DATE_ACTION) 
																		+ ' ' 
																		+ Formator.formatHourConcat(etape7.mailCommande.myAction.DATE_ACTION);
					
					etape7.mailCommande.createMailInfos();
				}
				
				this.verifierAvantEnvoyer();
			}
		}
		
		private function verifierAvantEnvoyer():void
		{
			
			_commandeServ.addEventListener(CommandeEvent.CMD_INEXISTANT_V, traitCmdInexistant);
			_commandeServ.verifierCommande(_cmd.NUMERO_COMMANDE);
		}
		
		protected function traitCmdInexistant(event:Event):void
		{
			_commandeServ.recordCommandeV2(	_cmd, 
											this._articles, 
											piecesJointes.piecesjointes.source, 
											etape7.mailCommande.mailCommande, 
											Formator.formatBoolean(etape7.mailCommande.chxMailCommande.selected),
											etape7.mailCommande.myAction,
											piecesJointes.UUID
										);
			
			_commandeServ.addEventListener(CommandeEvent.COMMANDE_CREATED, viewMail);
			_commandeServ.addEventListener('COMMANDE_ERROR_VERIF', traitErreurCommande);
		
		}
		
		protected function traitErreurCommande(event:Event):void
		{
			this.verifierAvantEnvoyer();
			
		}
		
		private function addArticles():Array
		{
			var cart		:CommandeArticles = new CommandeArticles();
			var elements	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var xmlArray	:ArrayCollection = new ArrayCollection();
			var len			:int = elements.length;
			
			for(var i:int = 0;i < len;i++)
			{
				var xmlEq		:XML = cart.addEquipements(elements[i] as ElementsCommande2);
				var xmlRs		:XML = cart.addRessources(elements[i] as ElementsCommande2);
				var config		:ArrayCollection = elements[i].CONFIGURATIONS;
				var engagement	:Engagement = elements[i].ENGAGEMENT;
				var idConfig	:int = elements[i].ID;
				var lenConfig	:int = config.length;
				
				for(var j:int = 0;j < lenConfig;j++)
				{
					var articles	:XML = <articles></articles>;
					var article		:XML = <article></article>;
					
					article.appendChild(xmlEq);
					article.appendChild(xmlRs);
					
					article = cart.addConfiguration(article, config[j] as ConfigurationElements, engagement, idConfig);
					
					articles.appendChild(article);
					
					xmlArray.addItem(articles)
				}
			}
			
			return xmlArray.source;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					MAILS
		//--------------------------------------------------------------------------------------------//

		private function viewMail(cmde:CommandeEvent):void
		{
			_isRecorded = true;
			
			dispatchEvent(new ViewStackEvent(ViewStackEvent.COMMANDE_BUILT, true));
		}
		
		private function afficherMailBox():void
		{	
			_pUpMailBox 				= new ActionMailBoxIHM();
			
			_pUpMailBox.idsCommande 	= [_commandeServ.idCommande];
			_pUpMailBox.numCommande 	= _cmd.NUMERO_COMMANDE;
			_pUpMailBox.idtypecmd 		= _cmd.IDTYPE_COMMANDE;
			
			_infosMail.commande 	= _cmd;
			_infosMail.corpMessage 	= _selectedAction.MESSAGE;
			_infosMail.UUID			= piecesJointes.UUID;			//---->>>>>>>>>>>>>>>> UUID
			
			var moduleText	:String = giveMeTheSegementTheme();
			var sujet		:String = giveMeSubject(_cmd);
			var libelle		:String	= _cmd.LIBELLE_COMMANDE;
			var numero		:String	= _cmd.NUMERO_COMMANDE;
			var objet		:String = sujet + " / " + libelle + " / " + numero;
			
			_pUpMailBox.initMail(moduleText, objet, "");
			_pUpMailBox.configRteMessage(_infosMail, GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE, pUpMailBoxMailEnvoyeHandler);
			_pUpMailBox.selectedAction = _selectedAction;
			
			PopUpManager.addPopUp(_pUpMailBox, this, true);
			PopUpManager.centerPopUp(_pUpMailBox);
		}
		
		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function giveMeSubject(objectCommande:Commande):String
		{
			var TC:String = "";
			
			switch(objectCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	TC = ResourceManager.getInstance().getString('M16', 'Commande_Mobile');				break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_Fixe_R_seau');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_'); 		break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.SAV: 					TC = ResourceManager.getInstance().getString('M16', 'SAV_'); 						break;//--- SAV
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM: 	TC = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_');			break;//--- REACT
			}
			
			return TC;
		}
		
		private function giveMeTheSegementTheme():String
		{
			var moduleText:String = "";
			var idseg:int = SessionUserObject.singletonSession.IDSEGMENT;
			
			switch(idseg)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Mobile'); break;
				case 2: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Fixe'); 	break;
				case 3: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Data'); 	break;
			}
			
			return moduleText;
		}
		
		private function pUpMailBoxMailEnvoyeHandler(e:Event):void
		{
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE, pUpMailBoxMailEnvoyeHandler);
			
			if(!_pUpMailBox.cbMail.selected)
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Validation_enregistr__'),this);
			else
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_mail_a__t__envoy__'),this);
			
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
			
			_cmd.IDLAST_ETAT = _selectedAction.IDETAT;
			
			dispatchEvent(new ViewStackEvent(ViewStackEvent.COMMANDE_BUILT, true));
		}

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - LISTENER
		//--------------------------------------------------------------------------------------------//	
		
		private function enableBtnValiderHandler(e:Event):void
		{
			btValid.enabled = true;
		}
		
		private function noAccessoriesHandler(e:Event):void
		{
			if(e.type == 'NO_ACCESSORIES_NEXT')
			{
				compteVs();
			}
			else if(e.type == 'NO_ACCESSORIES_PREV')
				{
					decompteVs();
				}
		}
		
		private function dataCommandeChanged(e:Event):void
		{
			if(e.type == 'TYPE_CMD_CHANGED')
			{
				var children	:Array = vs.getChildren();
				var lenChild	:int = children.length;
				
				for(var i:int = 0;i < lenChild;i++)
				{
					(children[i] as Object).reset();
				}
				
				var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
				var elts		:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
				var lenElts		:int = elements.length;
				var idrand		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM;
				
				if(elements.contains(elts))
				{
					for(var j:int = 0;j < lenElts;j++)
					{
						if(SessionUserObject.singletonSession.COMMANDEARTICLE[j].RANDOM == idrand)
						{
							SessionUserObject.singletonSession.COMMANDEARTICLE.removeItemAt(j);
							break;
						}
					}
				}
				
				SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			}
			
			if (e.type == 'REVENDEUR_CHANGED')
			{
				CommandeFormatter.getInstance().setCommandeCurrency(cbxRevendeur.selectedItem.DEVISE)
			}
			
			enabledDisableButtonWizzard();
		}
		
		private function btValidAndPurchaseClickHandler(e:Event):void
		{
			var isOk:Boolean = (vs.getChildAt(vs.selectedIndex) as Object).checkData();
			
			if(isOk)
			{
				var children	:Array = vs.getChildren();
				var lenChild	:int = children.length;
				
				for(var i:int = 0;i < lenChild;i++)
				{
					(children[i] as Object).reset();
				}
				
				etape1.disableAll(false);
				
				btCancelConfig.visible = true;
				vs.selectedIndex = 0;
				
				buttonNavigationEnableDisable();
				enabledDisableButtonWizzard();
				refreshPanier();
			}
		}
		
		private function calculPriceHandler(e:Event):void
		{
			refreshPanier();
		}
		
	}
}