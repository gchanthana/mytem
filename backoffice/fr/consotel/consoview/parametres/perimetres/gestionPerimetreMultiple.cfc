<cfcomponent name="gestionPerimetreMultiple">

		<cffunction name="getListeAssignNodes" access="remote" returntype="any">
			<cfargument name="idRacine" type="numeric" required="true">
			<cfargument name="idOrgaRef" type="numeric" required="true">
			<cfargument name="idOrga" type="numeric" required="true">
	        <cfargument name="date" type="string" required="true">
	        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLOBAL.Arbre_Assig_orgaref_orga">
		        <cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrgaRef#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#idOrga#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#date#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocresult name="qGetListePerimetresDataset">
		    </cfstoredproc>
			<cfreturn qGetListePerimetresDataset>
		</cffunction>
		
		<cffunction name="formatXMLData" returntype="xml" access="remote">
			<cfargument name="sourceQuery" type="any" required="true">
			<cfargument name="idGroupe" type="numeric" required="true">
			
			
	 
			
			<cfset dataStruct = structNew()>
			<cfset perimetreXmlDoc = XmlNew()>
			<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode = perimetreXmlDoc.node>
			<cfset dataStruct.ROOT_NODE = rootNode>
			<cfset rootNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][1]>

			<cfset rootNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][1]>
		
			<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
			

			<cfloop index="i" from="1" to="#sourceQuery.recordcount#">
				<cfset currentKey = sourceQuery['IDGROUPE_CLIENT'][i]>
				<cfset currentParentKey = sourceQuery['ID_GROUPE_MAITRE'][i]>
				<cfif sourceQuery['IDGROUPE_CLIENT'][i] EQ idGroupe>
					<cfset currentParentKey = "ROOT_NODE">
				</cfif>
				<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>
				<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
				<cfset tmpChildNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][i]>

				<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][i]>

				<cfset tmpChildNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][i]>

				<cfset structInsert(dataStruct,currentKey,tmpChildNode)>
			</cfloop>
			<cfreturn perimetreXmlDoc>
		</cffunction>
		
		<cffunction name="getMultipleAssignNodes" returntype="xml" access="remote">
			<cfargument name="id_racine" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="id_orga" required="true" type="numeric" />
			<cfargument name="date" required="true" type="string" />
			<cfset realid=session.perimetre.ID_GROUPE/>
			
			<cfset sourceQuery = getListeAssignNodes(realid, id_orgaRef, id_orga, date)>
			<cfset result = formatXMLData(sourceQuery, id_orga)>
			
			<cfreturn result>
		</cffunction>
		
		<cffunction name="getMultipleAssignedLines" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="string" />
			<cfargument name="id_orgaRef" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.Get_mult_Affect_orgaref_lignes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
		
		<cffunction name="searchMultipleAssignNodes" returntype="query" access="remote">
			<cfargument name="id_racine" required="true" type="numeric" />
			<cfargument name="id_orgaRef" required="true" type="numeric" />
			<cfargument name="id_orga" required="true" type="numeric" />
			<cfargument name="libelle_groupe" required="true" type="string" />
			<cfargument name="date" required="true" type="string" />
			<cfset realid=session.perimetre.ID_GROUPE/>
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_Global.Search_Assig_orgaref_orga">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orgaRef#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
		</cffunction>
	
</cfcomponent>
