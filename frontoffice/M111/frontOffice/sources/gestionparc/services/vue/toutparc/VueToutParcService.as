package gestionparc.services.vue.toutparc
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.services.servicelocator.IServiceView;

	
	public class VueToutParcService implements IServiceView
	{
		[Bindable]
		public var model:VueToutParcData;
		public var myHandler:VueToutParcHandler;
		public var myData:ArrayCollection;
		
		public function VueToutParcService()
		{
			model = new VueToutParcData();
			myHandler = new VueToutParcHandler(model);
			myData = new ArrayCollection();
		}
		
		public function getData(indexDebut:int, nbItem:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.vue.VueToutParc", "getVueToutParc", myHandler.ViewResultHandler);
			
			RemoteObjectUtil.callService(op,  
											SpecificationVO.POOL_TOUT_PARC_ID,
											SpecificationVO.getInstance().keyWord,
											SpecificationVO.getInstance().idKeyWordScope,
											SpecificationVO.getInstance().keyWord2,
											SpecificationVO.getInstance().idKeyWordScope2,
											SpecificationVO.getInstance().labelSort, 
											indexDebut, 
											nbItem,  
											SpecificationVO.getInstance().idRestriction,
											SpecificationVO.getInstance().orderByReq);
		}
		
		public function getModel():EventDispatcher
		{
			return model;
		}
		
		public function restoreData():ArrayCollection
		{
			myData=model.listeResultat;
			return myData;
		}
		
		public  function getAllnbRow(): Number
		{
			return model.NB_ROWS; 
		}
	}
}