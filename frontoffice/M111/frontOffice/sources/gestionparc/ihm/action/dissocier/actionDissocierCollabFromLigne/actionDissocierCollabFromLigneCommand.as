package gestionparc.ihm.action.dissocier.actionDissocierCollabFromLigne
{
	import gestionparc.ihm.action.ICommand;

	public class actionDissocierCollabFromLigneCommand implements ICommand
	{
		private var receiver:ActionDissocierCollabFromLigneReceiver;

		public function actionDissocierCollabFromLigneCommand(rec:ActionDissocierCollabFromLigneReceiver):void
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
