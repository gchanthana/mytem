<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.Listing" extends="fr.saaswedo.api.support.BaseClass" hint="Fonctionnalités de listing des entités (e.g tickets)">
	<cffunction access="public" name="getUserOrganizationTickets" returntype="Array" hint="Retourne les tickets de l'organisation d'un utilisateur">
		<cfargument name="email" type="String" required="true" hint="Email de l'utilisateur">
			<!--- Search user --->
			<cfset var queryString="type:user email:" & ARGUMENTS["email"]>
			<cfset var resultInfos=search(queryString,"","",1,1)>
			<cfif arrayLen(resultInfos["RESULTS"]) EQ 1>
				<!--- Get organization ID --->
				<cfset var userOrganizationId=VAL(resultInfos["RESULTS"][1]["organization_id"])>
				<cfset var organizationApi=getOrganizations()>
				<cfset var userOrganization=organizationApi.show(userOrganizationId)>
				<cfset var userOrganizationName=userOrganization["RESULT"]["ORGANIZATION"]["NAME"]>
				<!--- Search tickets --->
				<cfset queryString="type:ticket organization:" & userOrganizationName>
				<cfset resultInfos=search(queryString,"status","asc",1,1,TRUE)>
				<cfreturn resultInfos["RESULTS"]>
			<cfelse>
				<cfthrow type="Custom" message="L'utilisateur #ARGUMENTS.email# n'existe pas"/>
			</cfif>
	</cffunction>

	<cffunction access="public" name="getTicketsWhereUserIsCC" returntype="Array" hint="Retourne les tickets pour lesquels un utilisateur est en copie">
		<cfargument name="email" type="String" required="true" hint="Email de l'utilisateur">
			<cfset var queryString="type:ticket cc:" & ARGUMENTS["email"]>
			<cfset var resultInfos=search(queryString,"created_at","asc",1,1,TRUE)>
			<cfreturn resultInfos["RESULTS"]>
	</cffunction>

	<cffunction access="public" name="getUserTickets" returntype="Array" hint="Retourne les tickets d'un utilisateur">
		<cfargument name="email" type="String" required="true" hint="Email de l'utilisateur">
			<cfset var queryString="type:ticket requester:" & ARGUMENTS["email"]>
			<cfset var resultInfos=search(queryString,"created_at","asc",1,1,TRUE)>
			<cfreturn resultInfos["RESULTS"]>
	</cffunction>
	
	<cffunction access="public" name="search" returntype="Struct" hint="Effectue une recherche">
		<cfargument name="searchQuery" type="String" required="true" hint="Requete de recherche (Voir https://support.zendesk.com/entries/20239737-zendesk-search-reference)">
		<cfargument name="sortBy" type="String" required="false" default="" hint="Critère de tri">
		<cfargument name="sortOrder" type="String" required="false" default="" hint="Critère d'ordre">
		<cfargument name="page" type="Numeric" required="false" default="1" hint="Numéro de la page de résultat">
		<cfargument name="per_page" type="String" required="false" default="15" hint="Nombre de résultats max par page">
		<cfargument name="allPages" type="boolean" required="false" default="false" hint="TRUE pour ramener les résultats de toutes les pages et FALSE sinon">
			<cfset var searchApi=getSearch()>
			<cfreturn searchApi.search(ARGUMENTS["searchQuery"],ARGUMENTS["sortBy"],ARGUMENTS["sortOrder"],ARGUMENTS["page"],ARGUMENTS["per_page"],ARGUMENTS["allPages"])>
	</cffunction>
</cfcomponent>