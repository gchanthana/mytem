package composants.util.sites
{
	[Bindable]
	public class Site
	{
		public function Site()
		{	
		}
		
		public var IDSITE_PHYSIQUE:Number=0;
		public var IDGROUPE_CLIENT:Number=0;
		public var IDPOOL_GESTION:Number=0;
		public var NOM_SITE:String="";
		public var SP_REFERENCE:String="";
		public var SP_CODE_INTERNE:String="";
		public var SP_ADRESSE1:String="";
		public var SP_ADRESSE2:String="";
		public var SP_CODE_POSTAL:String="";
		public var SP_COMMUNE:String="";
		public var SP_COMMENTAIRE:String="";
		public var SP_BOOLLIVRAISON:Number=0;

	}
}