package univers.parametres.poolgestionnaire.ihm
{
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.service.TypeCommandeService;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	
	public class PopTypeCommandePool extends TitleWindow
	{	
		[Bindable] public var _typeCommandeService:TypeCommandeService  = new TypeCommandeService();
	 
		[Bindable]
		protected var _pool:PoolGestionnaire;
		public function set pool(value:PoolGestionnaire):void
		{
			_pool = value;
			if(_pool != null)
			{	
				_typeCommandeService.getTypeCommandeByPool(_pool.IDPool);	
				
			}
		}

		public function PopTypeCommandePool()
		{
			super();
		}
		
		
		protected function btFermerClickHandler(event : MouseEvent):void
		{
			fermer();
		}
		
		protected function popCloseHandler(event:CloseEvent):void
		{
			fermer();
		}
		
		//========================================================
		private function fermer():void
		{
			PopUpManager.removePopUp(this);
		}
		
		
	

		
	}
}