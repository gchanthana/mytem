package gestionparc.ihm.action.sim.actionsimvoirfiche
{
	import flash.display.DisplayObject;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.ficheEntity.FicheSimIHM;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionSimVoirFicheReceiver
	{
		private var fiche:FicheSimIHM;

		public function ActionSimVoirFicheReceiver()
		{
		}

		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 */
		public function action():void
		{
			displayIHM();
		}

		/**
		 * Cette fonction permet d'afficher l'IHM permettant de faire l'action.
		 */
		private function displayIHM():void
		{
			fiche=new FicheSimIHM();
			fiche.nomPrenomCollab=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR
			fiche.matriculeCollab=SpecificationVO.getInstance().elementDataGridSelected.E_MATRICULE;
			fiche.idGpeClient=CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			fiche.idSousTete=SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			fiche.idSim=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			fiche.ID=SpecificationVO.getInstance().elementDataGridSelected.ID;
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}