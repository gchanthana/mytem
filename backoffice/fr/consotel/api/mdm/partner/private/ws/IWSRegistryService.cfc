<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSRegistryService" hint="Interface décrivant les méthodes du Service Registry Zenprise">
	<cffunction access="public" name="addRegistryKey" returntype="Void" hint="Ajoute une nouvelle clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
	</cffunction>
	
	<cffunction access="public" name="addRegistryValue" returntype="Void" hint="Ajoute une nouvelle propriété dans une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfargument name="value" type="String" required="true" hint="Valeur de la propriété">
		<cfargument name="valType" type="Numeric" required="true" hint="Type de la valeur">
	</cffunction>
	
	<cffunction access="public" name="updateRegistryValue" returntype="Void" hint="Met à jour la valeur d'une propriété dans une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfargument name="value" type="String" required="true" hint="Valeur de la propriété">
		<cfargument name="valType" type="Numeric" required="true" hint="Type de la valeur">
	</cffunction>
	
	<cffunction access="public" name="removeRegistryKey" returntype="Void" hint="Supprime une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
	</cffunction>
	
	<cffunction access="public" name="getRegistryValue" returntype="Any" hint="Retourne la valeur d'une propriété d'une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
	</cffunction>
	
	<cffunction access="public" name="removeRegistryValue" returntype="Void" hint="Supprime des propriétés d'une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="Array" required="true" hint="Liste des propriétés à supprimer">
	</cffunction>
	
	<cffunction access="public" name="createTypedConfiguration" returntype="Void" hint="Crée une configuration registre typé">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration registre typé">
		<cfargument name="typeName" type="String" required="true" hint="Type de la configuration. Valeurs possibles : Chaine vide, ZDM, SCHEDULING, UNINSTALL">
	</cffunction>
	
	<cffunction access="public" name="removeConfiguration" returntype="Void" hint="Supprime une configuration de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration registre typé">
	</cffunction>
	
	<cffunction access="public" name="getConfigurationList" returntype="Array" hint="Retourne un tableau contenant la liste de toutes les configurations">
	</cffunction>
	
	<cffunction access="public" name="getOsRegistryKeysByFullPath" returntype="Array" hint="Retourne un tableau contenant la liste des chemins des clés de registre pour un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre ou une chaine vide pour la racine">
	</cffunction>
	
	<cffunction access="public" name="getRegistryKeysByFullPath" returntype="Array" hint="Retourne un tableau contenant la liste des chemins des clés de registre">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre ou une chaine vide pour la racine">
	</cffunction>
	
	<cffunction access="public" name="getRegistryValueNamesByFullPath" returntype="Array" hint="Retourne un tableau contenant la liste des noms de valeurs incluant par chemins">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
	</cffunction>
</cfinterface>