package composants.tb.tableaux
{
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	
	public class TableauAboFactoryStratey implements ITableauFactoryStrategy
	{
		public function createTableau(ev : TableauChangeEvent):ITableauModel
		{
			var tp : String = ev.TYPE;
			var seg : String = ev.SEGMENT;
			var idthemeproduit : String = ev.ID;
			var surtheme : String = ev.SUR_THEME;
			var idproduit : String = ev.IDPRODUIT;
			
			try{
				switch (tp.toUpperCase()){
					case "ACCUEIL" : return new TableauAbo();break;					
					case "SEGMENT" : return new TableauAboSegment(ev);break;
					case "SURTHEME" : return new TableauSurThemeAbo(ev);break;
					case "THEME" : return new TableauThemeAbo(ev);break;
					case "PRODUIT" : return new TableauProduitAbo(ev);break; 
					default : throw new TableauError();break;
				}
			}catch(e:Error){				
				throw new TableauError();
			}
			 return null;		
		}
		
	}
}