<!--- =========================================================================
Classe: Facture105
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="Facture105" extends="Facture" hint="nom de la classe Facture<OPERATEURID>">
 
<!--- METHODS --->
	<cffunction name="ConstruireGabarit" access="public" returntype="void" output="true" >
		<cfargument name="dataset" required="true" type="query">
		<!--- <cfdump var= "#dataset#"> --->
		<!--- MONTANT TOTAL --->
		<cfquery name="total" dbtype="query">
			SELECT SUM(montant) AS total
			FROM dataset
		</cfquery>

		<!--- MONTANT TOTAL DES ABONNEMENTS --->
		<cfquery name="getTotalAbonnements" dbtype="query">
			SELECT SUM(montant) AS total, datedeb_fact, datefin_fact, gamme
			FROM dataset
			WHERE gamme = 'abonnements et options'
			GROUP BY datedeb_fact, datefin_fact, gamme
		</cfquery>
		
		<!--- MONTANT TOTAL DES COMMUNNICATIONS --->
		<cfquery name="getTotalConsommations" dbtype="query">
			SELECT SUM(montant) AS total, datedeb_fact, datefin_fact, gamme
			FROM dataset
			WHERE gamme = 'consommations (hors forfaits)'
			GROUP BY datedeb_fact, datefin_fact, gamme
		</cfquery>
		
		<!--- MONTANT TOTAL DES RESTANTS --->
		<cfquery name="getTotalRestants" dbtype="query">
			SELECT SUM(montant) as total, datedeb_fact, datefin_fact, gamme
			FROM dataset
			WHERE gamme <> 'abonnements et options' AND
			gamme <> 'consommations (hors forfaits)'
			GROUP BY datedeb_fact, datefin_fact, gamme
		</cfquery>
		
		<!--- MONTANT TOTAL PAR LIBELLE PRODUIT --->
		<cfquery name="getTotalByLibelle_Produit" dbtype="query">
			SELECT libelle_produit, SUM(qte) AS total_qte, SUM(montant) AS total_montant, idproduit_catalogue
			FROM dataset
			WHERE gamme = 'abonnements et options'
			GROUP BY libelle_produit,idproduit_catalogue
		</cfquery>
		
		<!--- MONTANT TOTAL PAR DESTINATIONS --->
		<cfquery name="getTotalByDestinations" dbtype="query">
			SELECT libelle_produit, SUM(duree_appel) AS total_duree, SUM(nombre_appel) AS total_appel, SUM(montant) AS total_montant, idproduit_catalogue
			FROM dataset
			WHERE gamme = 'consommations (hors forfaits)'
			GROUP BY libelle_produit, idproduit_catalogue
		</cfquery>

		<!--- MONTANT TOTAL PAR LIBELLE_PRODUIT POUR LES RESTANTS --->
		<cfquery name="getTotalByOthers" dbtype="query">
			SELECT libelle_produit, SUM(qte) AS total_qte, SUM(montant) as total_montant, idproduit_catalogue
			FROM dataset
			WHERE gamme <> 'abonnements et options'
			AND gamme <> 'consommations (hors forfaits)'
			GROUP BY libelle_produit, idproduit_catalogue
		</cfquery>

		<p><br></p>
		<cfoutput>
			<center><strong align=CENTER>#variables.instance.typeFactureStrategy.displayType()#</strong></center>
		</cfoutput>
		<p><br></p>
		<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="white" class="cadre" width="90%">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="white" class="FT">
				<tr>
					<td align="LEFT" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="left" bgcolor="white">
						<tr>
							<th align="LEFT" colspan="2" class="huge" valign="bottom">
								<cfset displayer=createObject("component","fr.consotel.consoview.images.LogoDisplayer")>
								<cfset displayer.DisplayLogoByOperateurID(190,180)>
							</th>
						</tr>
						<TR>
							<td colspan="2">&nbsp;</td>
						</TR>
						<tr>
							<td class="normal">
								<strong>
									N° Compte Facturation &nbsp;: #dataset.compte_facturation#
								</strong>
							</th>
						</tr>
						</table>	
					</td>
					<td align="RIGHT" valign="top">
						<table cellpadding="0" cellspacing="2" border="0" align="right" bgcolor="white">
						<tr>
							<th colspan="2" class="huge" valign="bottom"><strong>FACTURE</strong></th>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td  class="normal">Date de la facture: </td>
							<td  class="normal">#LsDateFormat(dataset.date_emission,"dd/mm/yyyy")#</td>
						</tr>
						<tr>
							<td class="normal">Numéro de la facture: </td>
							<td class="normal">#dataset.numero_facture#</td>
						</tr>
						</table>
					</td>
				</tr>
				<!--- FIN DE L'EN-TETE --->
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<!--- MONTANT TOTAL DES ABONNEMENTS --->
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<cfif getTotalAbonnements.recordCount neq 0>
										<cfoutput query="getTotalAbonnements">
											<strong>Abonnements pour la période du #LsDateFormat(CreateDate(Left(datedeb_fact,4), 
											Mid(datedeb_fact,6,2), right(datedeb_fact,2)),
											"dd/mm/yyyy")# au #LsDateFormat(CreateDate(Left(datefin_fact,4), 
											Mid(datefin_fact,6,2), right(datefin_fact,2)),
											"dd/mm/yyyy")#</strong>
										</cfoutput>
									<cfelse>
										<strong>Abonnements</strong>
									</cfif>
								</td>
								<td align="right" class="big">
									<cfset total=#getTotalAbonnements.total#>
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<!--- DESCRIPTIF DES MONTANTS --->
				<cfif getTotalByLibelle_Produit.recordcount neq 0>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="small_header">&nbsp;</td>
								<td width="20" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">Qté</td>
								<td width="50" class="small_header"><nobr>Total en #getShortDevise()# HT</nobr></td>
							</tr>
							<cfoutput query="getTotalByLibelle_Produit">
							<cfset bool = false>
							
							<cfloop index = "ListElement" list = "#PRODUIT_RECHERCHE#">
								<cfif #idproduit_catalogue# eq #ListElement#>
									<cfset bool = true>
								</cfif>
							</cfloop>
							
							<cfif bool eq true>
									<tr>
										<td class="normalColored">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
										<td class="normalColored" align="right">&nbsp;</td>
										<td class="normalColored" align="right">&nbsp;</td>
										<td class="normalColored" align="center">#total_qte#</td>
										<td class="normalColored" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
									</tr>
								<cfelse>
									<tr>
										<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
										<td class="normal" align="right">&nbsp;</td>
										<td class="normal" align="right">&nbsp;</td>
										<td class="normal" align="center">#total_qte#</td>
										<td class="normal" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
									</tr>	
								</cfif>
							</cfoutput>
						</table>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<!--- MONTANT TOTAL DES COMMUNICATIONS --->
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<cfoutput query="getTotalConsommations">
									<strong>#gamme# pour la période du #LsDateFormat(CreateDate(Left(datedeb_fact,4), 
										Mid(datedeb_fact,6,2), right(datedeb_fact,2)),
										"dd/mm/yyyy")# au #LsDateFormat(CreateDate(Left(datefin_fact,4), 
										Mid(datefin_fact,6,2), right(datefin_fact,2)),
										"dd/mm/yyyy")#</strong>
									</cfoutput>
								</td>
								<td align="right" class="big">
									<cfset total=#getTotalConsommations.total#>
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<!--- DESCRIPTIF DES MONTANTS --->
				<cfif getTotalByDestinations.recordcount neq 0>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="small_header">&nbsp;</td>
								<td width="20" class="small_header">&nbsp;</td>
								<td width="20" class="small_header"><nobr>durée HH:MM:SS</nobr></td>
								<td width="50" class="small_header">Qté</td>
								<td width="50" class="small_header"><nobr>Total en #getShortDevise()# HT</nobr></td>
							</tr>
							<cfoutput query="getTotalByDestinations">
								<cfset bool = false>
							<cfloop index = "ListElement" list = "#PRODUIT_RECHERCHE#">
								<cfif #idproduit_catalogue# eq #ListElement#>
									<cfset bool = true>
								</cfif>
							</cfloop>
							
							<cfif bool eq true>
									<tr>
										<td class="normalColored">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
										<td class="normalColored" align="right">&nbsp;</td>
										<cfif total_duree neq "">
											<cfset heure=int(#total_duree#/60)>
											<cfset minute=int((#total_duree#-heure*60))>
											<cfset seconde=(#total_duree#*60-heure*3600-minute*60)>
										<cfelse>
											<cfset heure=int(0)>
											<cfset minute=int(0)>
											<cfset seconde=(0)>
										</cfif>										
										<td class="normalColored" align="right">
											<cfif heure lt 100>
												#LsNumberFormat(heure, '00')#
											<cfelse>
												#heure#
											</cfif>
										:#LsNumberFormat(minute,'00')#:#LsNumberFormat(seconde,'00')#
										</td>
										<td class="normalColored" align="center">#total_appel#</td>
										<td class="normalColored" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
									</tr>
								<cfelse>
									<tr>
										<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
										<td class="normal" align="right">&nbsp;</td>
										<cfif total_duree neq "">
											<cfset heure=int(#total_duree#/60)>
											<cfset minute=int((#total_duree#-heure*60))>
											<cfset seconde=(#total_duree#*60-heure*3600-minute*60)>
										<cfelse>
											<cfset heure=int(0)>
											<cfset minute=int(0)>
											<cfset seconde=(0)>
										</cfif>
										<td class="normal" align="right">
											<cfif heure lt 100>
												#LsNumberFormat(heure, '00')#
											<cfelse>
												#heure#
											</cfif>
										:#LsNumberFormat(minute,'00')#:#LsNumberFormat(seconde,'00')#
										</td>
										<td class="normal" align="center">#total_appel#</td>
										<td class="normal" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
									</tr>
								</cfif>
							
							</cfoutput>
						</table>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<!--- MONTANT TOTAL DES RESTANTS --->
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<cfif getTotalRestants.recordCount neq 0>
										<cfoutput query="getTotalRestants">
										<strong>Autres pour la période du #LsDateFormat(CreateDate(Left(datedeb_fact,4), 
											Mid(datedeb_fact,6,2), right(datedeb_fact,2)),
											"dd/mm/yyyy")# au #LsDateFormat(CreateDate(Left(datefin_fact,4), 
											Mid(datefin_fact,6,2), right(datefin_fact,2)),
											"dd/mm/yyyy")#</strong>
										</cfoutput>
									<cfelse>
										<strong>Autres</strong>
									</cfif>
								</td>
								<td align="right" class="big">
									<cfset total=#getTotalRestants.total#>
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<!--- DESCRIPTIF DES MONTANTS --->
				<cfif getTotalByOthers.recordcount neq 0>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="small_header">&nbsp;</td>
								<td width="20" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">&nbsp;</td>
								<td width="50" class="small_header">Qté</td>
								<td width="50" class="small_header"><nobr>Total en #getShortDevise()# HT</nobr></td>
							</tr>
							<cfoutput query="getTotalByOthers">
								<cfset bool = false>
							<cfloop index = "ListElement" list = "#PRODUIT_RECHERCHE#">
								<cfif #idproduit_catalogue# eq #ListElement#>
									<cfset bool = true>
								</cfif>
							</cfloop>
							
							<cfif bool eq true>
									<tr>
										<td class="normalColored">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
										<td class="normalColored" align="right">&nbsp;</td>
										<td class="normalColored" align="right">&nbsp;</td>
										<td class="normalColored" align="center">#total_qte#</td>
										<td class="normalColored" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
									</tr>
								<cfelse>
									<tr>
										<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<STRONG>#Libelle_produit#</STRONG></td>
										<td class="normal" align="right">&nbsp;</td>
										<td class="normal" align="right">&nbsp;</td>
										<td class="normal" align="center">#total_qte#</td>
										<td class="normal" align="right">#LsEuroCurrencyFormat(total_montant, 'none')# #getSigneDevise()#&nbsp;</td>
									</tr>
								</cfif>										
							</cfoutput>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<hr size="1" noshade>
					</td>
				</tr>
				</cfif>
				<!--- SEPARATION AVEC LE DETAIL PAR SITE --->
				<tr>
					<td colspan="2" height="60">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<hr size="5" noshade>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="60">
						&nbsp;
					</td>
				</tr>
				<!------------------------------------------>
				<!--- DETAILS PAR SOUS COMPTE(S) --->
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<strong>D&eacute;tails par Site(s)</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<!--- BOUCLE PAR SOUS COMPTE --->
				<cfoutput query="dataset" group="sous_compte">
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>
					<!--- IDENTIFICATION SOUS COMPTE ET SITE --->
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="big">
									<strong>N° Sous Compte :&nbsp;#sous_compte#</strong>
								</td>
								<td align="right" class="big">
									<strong>
										<NOBR>#nom_site#</NORB><br>
										<cfif len(trim(adresse1)) neq 0>
											<NOBR>#adresse1#</NOBR><br>
										</cfif>
										<cfif len(trim(adresse2)) neq 0>
											<NOBR>#adresse2#</NOBR><br>
										</cfif>
										<NOBR>#zipcode# #commune#</NOBR>
									</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<!--- DETAILS DU SOUS COMPTE (PAR GAMME) --->
					
				<cfoutput group="gamme">
					<cfset total=0>
						<cfoutput group="libelle">
							<cfoutput group="sous_tete">
								<cfoutput>
									<cfset total=total+montant>
								</cfoutput>
							</cfoutput>
						</cfoutput>
				<tr>
					<td colspan="2"><hr size="2" noshade></td>
				</tr>

				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="big">
									<strong>
										#gamme# pour la période du #LsDateFormat(CreateDate(Left(datedeb_fact,4), 
										Mid(datedeb_fact,6,2), right(datedeb_fact,2)),
										"dd/mm/yyyy")# au #LsDateFormat(CreateDate(Left(datefin_fact,4), 
										Mid(datefin_fact,6,2), right(datefin_fact,2)),
										"dd/mm/yyyy")#
									</strong>
								</td>
								<td align="right" class="big">
									<strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr size="1" noshade></td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellpadding="0" cellspacing="2" border="0" width="100%" bgcolor="white" class="FT">
							<tr>
								<td class="small_header">&nbsp;</td>
								<td width="50" class="small_header">&nbsp;</td>
									<cfif gamme neq "communications">
										<td width="20" class="small_header"><nobr>prix unit. #getShortDevise()# HT</nobr></td>
									<cfelse>
										<td width="20" class="small_header"><nobr>durée HH:MM:SS</nobr></td>
									</cfif>
								<td width="50" class="small_header">qté</td>
								<!---

								--->
								<td width="50" class="small_header"><nobr>total en #getShortDevise()# HT</nobr></td>
							</tr>
							<cfoutput group="libelle">
							<cfset total=0>
								<cfoutput>
									<cfset total=total+montant>
								</cfoutput>
							<tr>
								<td class="normal"><strong>#libelle#</strong></td>
								<td></td>
								<td></td>
								<td></td>
								<td align="right" class="normal"><strong>#LsEuroCurrencyFormat(total, 'none')# #getSigneDevise()#</strong></td>
							</tr>
								<cfoutput group="sous_tete">
							<tr>
								<cfif sous_tete neq sous_compte>
									<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<strong>N° #sous_tete# #libelle_type_ligne#</strong></td>
								<cfelse>
									<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;<strong>non ventilable par ligne</strong></td>
								</cfif>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
								<cfoutput>
									<cfset bool = false>
							<cfloop index = "ListElement" list = "#PRODUIT_RECHERCHE#">
								<cfif #idproduit_catalogue# eq #ListElement#>
									<cfset bool = true>
								</cfif>
							</cfloop>
							
							<cfif bool eq true>
										<tr>
											<td class="normalColored">&nbsp;&nbsp;&nbsp;&nbsp;#Libelle_produit#</td>
											<td class="normalColored" align="center">&nbsp;</td>
											<cfif prix_unit neq ".01">
												<td class="normalColored" align="right">#LsEuroCurrencyFormat(prix_unit,'none')#</td>
											<cfelse>
												<cfif duree_appel neq "">												
													<cfset heure=int(duree_appel/60)>
													<cfset minute=int((duree_appel-heure*60))>
													<cfset seconde=(duree_appel*60-heure*3600-minute*60)>
												<cfelse>
													<cfset heure=int(0)>
													<cfset minute=int(0)>
													<cfset seconde=(0)>
												</cfif>
												<td class="normalColored" align="right">
												<cfif heure lt 100>
												#LsNumberFormat(heure, '00')#
												<cfelse>
												#heure#
												</cfif>
											:#LsNumberFormat(minute,'00')#:#LsNumberFormat(seconde,'00')#</td>
											</cfif>
											<cfif nombre_appel eq 0>
												<td class="normalColored" align="right">#qte#&nbsp;</td>
											<cfelse>
												<td class="normalColored" align="right">#nombre_appel#&nbsp;</td>
											</cfif>
											<td class="normalColored" align="right">#LsEuroCurrencyFormat(montant, 'none')# #getSigneDevise()#&nbsp;</td>
										</tr>
									<cfelse>
										<tr>
											<td class="normal">&nbsp;&nbsp;&nbsp;&nbsp;#Libelle_produit#</td>
											<td class="normal" align="center">&nbsp;</td>
											<cfif prix_unit neq ".01">
												<td class="normal" align="right">#LsEuroCurrencyFormat(prix_unit,'none')#</td>
											<cfelse>																								
												<cfif duree_appel neq "">
													<cfset heure=int(duree_appel/60)>
													<cfset minute=int((duree_appel-heure*60))>
													<cfset seconde=(duree_appel*60-heure*3600-minute*60)>
												<cfelse>
													<cfset heure=int(0)>
													<cfset minute=int(0)>
													<cfset seconde=(0)>
												</cfif>																								
												<td class="normal" align="right">
												<cfif heure lt 100>
												#LsNumberFormat(heure, '00')#
												<cfelse>
												#heure#
												</cfif>
											:#LsNumberFormat(minute,'00')#:#LsNumberFormat(seconde,'00')#</td>
											</cfif>
											<cfif nombre_appel eq 0>
												<td class="normal" align="right">#qte#&nbsp;</td>
											<cfelse>
												<td class="normal" align="right">#nombre_appel#&nbsp;</td>
											</cfif>
											<td class="normal" align="right">#LsEuroCurrencyFormat(montant, 'none')# #getSigneDevise()#&nbsp;</td>
										</tr>
									</cfif>
										
						</cfoutput>
					</cfoutput> <!--- SOUS TETE --->
				</cfoutput> <!--- LIBELLE --->
						</table>
						</td>
					</tr>
			</cfoutput> <!--- GAMME --->
					<!------------------------------------------>
				</cfoutput> <!--- SOUS COMPTE --->
				</table>
			</td>
		</tr>
	</table>
</cffunction>

	<cffunction name="ConstruireGabaritCat" access="public" returntype="void" output="true">
		<cfargument name="dataset" required="true" type="query">
		<cfargument name="factureParams" required="true" type="struct">
		<cfset factureInfos = getFactureInfos(factureParams.ID_FACTURE)>
		<cfset factureTva = getFactureTva(factureParams.ID_FACTURE,idx_perimetre)>
		<p><br></p>
		<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="white" class="cadre" width="900">
			<tr height="150">
				<td>
					<table cellpadding="4" cellspacing="4" border="0" width="100%" bgcolor="white" class="FT">
						<!--- Ligne 1 : Logo + Numero et date de facture --->
						<tr>
							<td align="left" valign="top" width="200">
								<table cellpadding="0" cellspacing="2" border="0" align="left" bgcolor="white">
									<tr>
										<td>
											<cfset displayer=createObject("component","fr.consotel.consoview.images.LogoDisplayer")>
											<cfset displayer.DisplayLogoByOperateurID(190,180)>
										</td>
									</tr>
								</table>
							</td>
							<td valign="middle" align="center" width="350">
								<strong><font size="5">facture</font></strong>
							</td>
							<td align="center" valign="middle">
								<table cellpadding="0" cellspacing="1" border="0" bgcolor="white">
									<tr>
										<td class="normal" align="right">n° de la facture : <strong>#factureParams.NUMERO_FACTURE#</strong></td>
									</tr>
									<tr>
										<td  class="normal" align="right">date de la facture :
											<strong>
												#lsDateFormat(parseDateTime(factureParams.DATE_EMISSION,true),"DD/mm/YYYY")#
											</strong>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<!--- Ligne 2 :  --->
						<tr>
							<td class="normal" rowspan="2" valign="top">
								<strong>Pour nous contacter :</strong>
								<cfset contactOp = "">
								<cfset contactOpBool = 0>
								<cfif TRIM(factureInfos['adresse1_operateur'][1]) NEQ "">
									<br>#factureInfos['ADRESSE1_OPERATEUR'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['adresse2_operateur'][1]) NEQ "">
									<br>#factureInfos['ADRESSE2_OPERATEUR'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['code_postal_operateur'][1]) NEQ "">
									<br>#factureInfos['code_postal_operateur'][1]# #factureInfos['ville_operateur'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<br>
								<cfif contactOpBool EQ 0>
									<br>Données non disponibles
								</cfif>
							</td>
							<td class="normal" valign="middle">
								numéro de compte : <br><strong>#factureInfos['COMPTE_FACTURATION'][1]#</strong>
							</td>
							<td><!--- coordonnées de FT ---></td>
						</tr>
						<!--- Ligne 3 :  --->
						<tr>
							<td class="normal">
								<strong>
								#factureParams.LIBELLE# (#factureParams.PERIMETRE_LIBELLE#)<br>
								#factureInfos['by_adresse1'][1]#<br>
								#factureInfos['by_adresse2'][1]#<br>
								#factureInfos['by_zipcode'][1]# #factureInfos['by_commune'][1]#<br>
								</strong>
							</td>
							<td class="normal" rowspan="2">
								<strong>
								#factureParams.LIBELLE# (#factureParams.PERIMETRE_LIBELLE#)<br>
								#factureInfos['by_adresse1'][1]#<br>
								#factureInfos['by_adresse2'][1]#<br>
								#factureInfos['by_zipcode'][1]# #factureInfos['by_commune'][1]#<br>
								</strong>
							</td>
						</tr>
						<!--- Ligne 4 :  --->
						<tr>
							<td colspan="2" valign="top">
								<cfif TRIM(factureInfos['TEL_SERVICE_CLIENT'][1]) NEQ "">
									Service Client : #factureInfos['TEL_SERVICE_CLIENT'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['HORAIRE_SERVICE_CLIENT'][1]) NEQ "">
									<br>Horaire : #factureInfos['HORAIRE_SERVICE_CLIENT'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['FAX_SERVICE_CLIENT'][1]) NEQ "">
									<br>Fax : #factureInfos['FAX_SERVICE_CLIENT'][1]#
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['MAIL_SERVICE_CLIENT'][1]) NEQ "">
									<br><a href="mailto://#factureInfos['MAIL_SERVICE_CLIENT'][1]#">
										#factureInfos['MAIL_SERVICE_CLIENT'][1]#
									</a>
									<cfset contactOpBool = 1>
								</cfif>
								<cfif TRIM(factureInfos['URL_OPERATEUR'][1]) NEQ "">
									<br><a href="#factureInfos['URL_OPERATEUR'][1]#" target="_blank">#factureInfos['URL_OPERATEUR'][1]#</a>
									<cfset contactOpBool = 1>
								</cfif>
								<p><br></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td valign="top" width="200px">
								<table cellpadding="0" cellspacing="5" border="0" align="right" bgcolor="FFFFFF" class="FT" width="100%">
									<tr>
										<td class="normal">
											
										</td>
									</tr>
								</table>
							</td>
							<td align="left" valign="top">
								<table cellpadding="0" cellspacing="5" border="0" bgcolor="white" class="FT" width="100%">
									<tr>
										<td align="left" valign="top" colspan="2">
											<table cellpadding="0" cellspacing="5" border="0" bgcolor="white" class="FT" width="100%">
												<cfset totalMontantHt = 0>
												<cfset totalMontantTtc = 0>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<cfloop index="i" from="1" to="#dataset.recordcount#">
													<tr>
														<td class="normal" align="left">
															<strong>#dataset['LIBELLE'][i]#</strong>
														</td>
														<td class="normal" align="right">
															<strong>#LsEuroCurrencyFormat(dataset['MONTANT'][i],'none')# #getSigneDevise()#</strong>
															<cfset totalMontantHt = totalMontantHt + dataset['MONTANT'][i]>
														</td>
													</tr>
												</cfloop>
												<tr>
													<td class="normal" align="left">
														<font size="1"><i>Les prestations facturées ci-dessus sont détaillées en annexe</i></font>
													</td>
													<td class="normal" align="right">
													</td>
												</tr>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr>
													<td class="very_big" align="left">
														<strong>total facture <font size="2">(montant #getShortDevise()# HT)</font></strong>
													</td>
													<td class="very_big" align="right">
														<strong>#LsEuroCurrencyFormat(totalMontantHt,'none')#</strong>
													</td>
												</tr>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr>
													<td class="normal" align="left">
														<font size="2">T.V.A. payée sur les débits</font><br>
													</td>
													<td class="normal" align="right">
													</td>
												</tr>
												<cfset totalTTC=0>
												<cfoutput query="factureTva">
													<tr>
													<td class="normal" align="left">
														Montant TVA à #LsEuroCurrencyFormat(Taux,'none')# % sur #LsEuroCurrencyFormat(montant,'none')# #getShortDevise()#
													</td>
													<cfset montantTVA = Evaluate((taux * montant) / 100)>
													<td class="normal" align="right">
														#LsEuroCurrencyFormat(montantTVA,'none')#
													</td>
												</tr>
												<cfset totalTTC=totalTTC+montant+montantTVA>
												</cfoutput>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr>
													<td class="very_big" align="left">
														<strong>somme à payer <font size="2">avant le <cfoutput>#LsDateFormat(factureTva.echeance_paiement,"dd/mm/yyyy")#</cfoutput> (#getShortDevise()# TTC)</font></strong>
													</td>
													<td class="very_big" align="right">
														<strong>#LsEuroCurrencyFormat(totalTTC,'none')#</strong>
													</td>
												</tr>
												<tr><td colspan="2"><hr size="3" noshade></td></tr>
												<tr height="120"><td colspan="2"></td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</cffunction>
</cfcomponent>
