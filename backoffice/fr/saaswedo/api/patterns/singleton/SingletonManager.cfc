<!--- =========== Use this comment as a rule to get a 100 characters line MAX width =========== --->
<cfcomponent author="Cedric"
displayname="fr.saaswedo.api.patterns.singleton.SingletonManager"
hint="Gestion des références (Singleton) qui sont placées dans les scopes Server, Application">
	<!--- Roles :
	- Enregistrement des références : Pour les stocker dans une liste (e.g Ajout,Suppression)
	- Suppression des références : Evite un redémarrage pour les scopes Server, Application
	--->
</cfcomponent>