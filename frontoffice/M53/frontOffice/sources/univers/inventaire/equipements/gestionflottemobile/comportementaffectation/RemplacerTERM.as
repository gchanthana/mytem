package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class RemplacerTERM implements IComportementAffectation
	{
		public function RemplacerTERM()
		{
			
		}
		
		public function affecter(idTerm : int, idTerm_New : int, libelle_term : String, libelle_New : String, date : String, pret :int):void
		{
			//Alert.show("AffecterSimTerm : "+idSIM+"--"+idTerm);
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"replaceTERM",
																		affecter_handler);
			RemoteObjectUtil.callService(op,PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE,PanelGestionFoltteMobile.idPoolGestionnaire,idTerm,idTerm_New,libelle_term,libelle_New,date);
		}
		private function affecter_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Echange du terminal effectué");
			}
			else
			{
				trace("Erreur replace_term");
			}
		}
		public function annuler():void
		{
			Alert.show("fermer");
		}
		

	}
}