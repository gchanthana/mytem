package univers.inventaire.equipements.emplacement.customitemrenderer
{

    import mx.resources.ResourceManager;
	import mx.controls.treeClasses.*;
    import mx.collections.*;

    public class TreeEmplacementItemRenderer extends TreeItemRenderer
    {
    	

        // Define the constructor.      
        public function TreeEmplacementItemRenderer() {
            super();
        }
        
        // Override the set method for the data property
        // to set the font color and style of each node.        
        override public function set data(value:Object):void {
            super.data = value;
            /* if(TreeListData(super.listData).hasChildren)
            {
                setStyle("color", "blue");
                setStyle("fontWeight", 'bold');
            }
            else
            {
                setStyle("color", "green");
                setStyle("fontWeight", 'normal');
            } */
            
        }
     
        // Override the updateDisplayList() method 
        // to set the text for each tree node.      
        override protected function updateDisplayList(unscaledWidth:Number, 
            unscaledHeight:Number):void {
       
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if(super.data)
            {
            	
                if(TreeListData(super.listData).hasChildren)
                {
                    var tmp:XMLList = 
                        new XMLList(TreeListData(super.listData).item);
                    var myStr:int = tmp[0].children().length();
                   	var str : String = "";
                   	if (Number(TreeListData(super.listData).item.@IDSITE_PHYSIQUE) == 0){
                   		str = (myStr > 1)? ResourceManager.getInstance().getString('M22', '_Sites__') : ResourceManager.getInstance().getString('M22', '_Site__')
                   		super.label.text =  TreeListData(super.listData).label + 
                        " (" + myStr + str;	
                   	}else /* if (Number(TreeListData(super.listData).item.@IDTYPE_EMPLACEMENT) > 0) */{
                   		str = (myStr > 1)? ResourceManager.getInstance().getString('M22', '_Emplacements__') : ResourceManager.getInstance().getString('M22', '_Emplacement__');
                    	super.label.text =  TreeListData(super.listData).label + 
                        " (" + myStr + str;
                    		
                   	}
                }
            }
        }
    }
}