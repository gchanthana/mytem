package gestionparc.ihm.fiches.collaborateur
{
	import flash.events.Event;
	import flash.events.MouseEvent;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.FicheAffectationIHM;
	import gestionparc.ihm.fiches.listedispo.DispoEqp;
	import gestionparc.ihm.question.collaborateur.QuestionFicheFournirEqp;
	import gestionparc.utils.gestionparcConstantes;

	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheFournirEqp extends FicheAffectationIHM
	{
		private var obj_listeDispo:DispoEqp;
		private var qn:QuestionFicheFournirEqp;

		public function FicheFournirEqp()
		{
			super();
			obj_listeDispo=new DispoEqp(true,'collab');
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(ev:FlexEvent):void
		{
			initListeners();
			initIHM();
		}

		private function initListeners():void
		{
			obj_listeDispo.addEventListener(MouseEvent.CLICK, dgListeDispo_handler);
			obj_listeDispo.addEventListener(gestionparcEvent.REFRESH_LIBELLESELECTION, dgListeDispo_handler);
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);
			this.addEventListener(CloseEvent.CLOSE, fermer);
		}

		private function initIHM():void
		{
			this.title=ResourceManager.getInstance().getString("M111", "FOURNIR_EQP");

			this.img_cellule.source=gestionparcConstantes.adrImgEmp;
			this.lab_cellule_name.text=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR

			this.img_liste.source=gestionparcConstantes.adrImgTerm;
			this.img_liste_big.source=gestionparcConstantes.adrImgTerm;
			this.lab_titre.text=obj_listeDispo.getLibelleType()

			this.box_listeDispo.addChild(obj_listeDispo as VBox);
		}

		private function dgListeDispo_handler(evt:Event):void
		{
			labDgSelection.text=obj_listeDispo.getLibelleSelection();
		}

		private function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function valider(e:Event):void
		{
			if (obj_listeDispo.getIDSelection() > -1)
			{
				qn=new QuestionFicheFournirEqp(obj_listeDispo, check_pret.selected, (calendar.selectedDate != null) ? calendar.selectedDate : new Date(),obj_listeDispo.typeEqp);
				qn.serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, fermer);
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
		}
	}
}
