<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<cfreturn>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getAccueilDivLastActions" access="public" output="true"  returnType="string"
				displayname="" 
				hint="Génère la div contenant les dernieres actions sur les lignes et terminal d'un collaborateur / template D1" >
		
		<cfargument name="idSousTete" required="false" type="numeric" default=0 >
		
		<cfset idTerminal = 0>
		<cfset idSIM = 0>
		<cfset sous_tete = 0>
			
		<cfif idSousTete eq 0><!--- PREMIERE FOIS SUR LA PAGE D'ACCUEIL (AUCUN IDSOUSTETE N'A ETE SPECIFIE) --->
			
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
							
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfbreak>
					
				</cfif>
			</cfloop>
		
		<!--- SI CE N'EST PAS LE PREMIER ACCES À LA PAGE D'ACCUEIL / VARIABLES DEJA INITIALISÉES / RECUPERATION DES AUTRE VARIABLES DONT ON A BESOIN --->
		<cfelse>

			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] eq idSousTete>
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfbreak>
				</cfif>
			</cfloop>
		
		</cfif>
		
		<!--- historique des logg --->
		<cfset queryHistorik = createObject("component","fr.consotel.consoview.M111.fiches.FicheCommun").getHistorique(SESSION.IDEMPLOYE,idTerminal,idSIM,val(idSousTete)) >
		
		<cfset divLastAction = '<div id="tooltip"></div>'>		
		
		<cfset divLastAction &= '
				
				<div class="divLastAction" style="">
					<div class="divRightLasAction" style="">
						<div style="">
							<div class="pictoLastAction" style=""><img src="img/mytem/mes_dernieresActions.png"></div>
							<div class="tdLastActionIntitule" style="">Les dernières actions sur mes lignes et terminaux</div>
							<div class="clear" style=""></div>
						</div>
						<div style="">
							<div class="pictoLastAction" style=""></div>
							<div class="divTableLastAction" style="">
								<table class="tableLastAction" style="" width="100%">
									<colgroup>
										<col width="10%"></col>
										<col width="90%"></col>
									</colgroup>
									<tbody>
		'>
		
		<cfset cpt = 0>
		<cfif queryHistorik.recordcount gt 0> <!--- si table historique est non vide --->
		
		<cfloop query="queryHistorik">
			<cfset currentRow = queryHistorik.currentrow >
			
			<cfset date_action1 = queryHistorik["DATE_ACTION"][currentrow] >
			<cfset date_action2 = LSDateFormat(date_action1, "dd/mm/yyyy") >
			
			<cfif queryHistorik["WORD1"][currentrow] neq " ">
				<cfset cpt = cpt + 1>
				<cfset tooltipLog = "" >
				
				<cfif queryHistorik["WORD2"][currentrow] neq " ">
					<cfset tooltipLog = '&nbsp;<a style="cursor:auto;" onmouseover="tooltip.show(this)" onmouseout="tooltip.hide(this)" title="#queryHistorik.WORD2#&nbsp;#queryHistorik.ITEM2#"><img style="vertical-align:middle" border="0" src="img/mytem/plus_bleu.png" width="14" height="14" alt="voir l''objet du mail"></a>'>
				</cfif>
				
				<cfset divLastAction &= '
											<tr style="">
												<td style="">Le&nbsp;#date_action2#</td>
												<td style="">:&nbsp;#queryHistorik.WORD1##queryHistorik.ITEM1##tooltipLog#</td>
											</tr>
				'>
			</cfif>
			
			<cfif cpt eq 3><!--- affiche les 3 dernieres actions maximum --->
				<cfbreak>
			</cfif>
		</cfloop>
		
		<cfelse><!--- si table historique est vide --->
		
			<cfset divLastAction &= '
										<tr style="">
											<td colspan="2" style="">Aucune action n''est répertoriée dans l''historique</td>
										</tr>
			'>
		</cfif>
		
		<cfset divLastAction &= '
									</tbody>
								</table>
							</div>
							<div class="clear" style=""></div>
						</div>
					</div>
					<div class="divContactGest" style="">
						<input align="center" type="button" onclick="popupEmail(''ombreee2'')" class="btnContacterGestionnaireAccueil" name="contactGest" value="contacter mon gestionnaire">
					</div>
					<div class="clear" style=""></div>
				</div>
		'>
		
		<cfreturn divLastAction >
	</cffunction>
	
</cfcomponent>