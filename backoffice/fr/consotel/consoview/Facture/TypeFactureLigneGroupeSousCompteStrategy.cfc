<!--- =========================================================================
Classe: TypeFactureLigneGroupeSousCompteStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="TypeFactureLigneGroupeSousCompteStrategy" extends="TypeFactureStrategy" hint="">
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="Initial value for the IDINVENTAIRE_PERIODEproperty." />
		<cfargument name="IDsous_tete" required="false" type="string">
	</cffunction>
	<cffunction name="displayType" access="public" returntype="string" output="false" >
		<cfreturn "Facture de la ligne">
	</cffunction>
</cfcomponent>