<cfcomponent output="false" displayname="fr.consotel.consoview.M24.upload.UploaderFile" hint="Gestionnaire d'upload de fichiers">
	<cfset PATH_LOW = "product/low">
	<cfset PATH_THUMB = "product/thumb">
	<cfset URL_IMAGES = "http://images.consotel.fr/">
	<!--- 
		uploader un fichier
	 --->
	<cffunction name="uploadFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="currentFile" 	type="fr.consotel.consoview.M24.vo.FileUploadVo" required="true"/>
		<cfargument name="idTarget"	 	type="Numeric" required="false"/>						  

		<cfset currentFile.fileUUID = createUUID()>
		
		<cfset var webPath 	= GetDirectoryFromPath( GetCurrentTemplatePath() )>
		<cfset var pathDirTMP 	= webPath & "tmp/"&currentFile.fileUUID>
<!---<cfmail from="soufiane.ghenimi@saaswedo.com" subject="Test Email" to="soufiane.ghenimi@saaswedo.com" type="html" >
		webPath : <cfdump var="#webPath#" ><br/>
		dirTmp : <cfdump var="#dirTmp#" ><br/>
		pathDirTMP : <cfdump var="#webPath#" ><br/>
</cfmail>--->
		<cfif !DirectoryExists('#pathDirTMP#')>
			<cfdirectory action="create" directory="#pathDirTMP#" mode="777">
		</cfif>
		
		<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M24.upload.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
		<cfset var pathDirFileTmpRenamed= pathDirTMP & "/" & currentFile.fileName>
		
		<!--- Chargement fichier en local sur le serveur --->
		<cffile action="write" output="#currentFile.fileData#" file="#pathDirFileTmpRenamed#" mode="777">

		<!--- GESTION FTP --->
		<cfif currentFile.imageType EQ "LOW">
			<cfset  pathRemoteFile= PATH_LOW & "/" & currentFile.fileName>
			<cfset  urlpicture= URL_IMAGES & PATH_LOW & "/" & currentFile.fileName>
			<cfset pathFile = "/" & PATH_LOW>
		<cfelse>
			<cfset pathRemoteFile = PATH_THUMB & "/" & currentFile.fileName >
			<cfset urlpicture= URL_IMAGES & PATH_THUMB & "/" & currentFile.fileName>
			<cfset pathFile = "/" & PATH_THUMB>
		</cfif>
		
		<cfset var retour = handleFTP(pathDirFileTmpRenamed,pathRemoteFile)>
		<cfset var retourAdd = addInternalPicture(urlpicture, currentFile.fileName, pathFile)>
		
		
		<cfreturn retour>
	</cffunction>
	
	<!--- 
		uploader plusieurs fichiers
	 --->
	<cffunction name="uploadFiles" access="remote" output="false" returntype="Array" hint="Upload plusieurs fichiers">
		<cfargument name="filesSelected" type="Array" required="true"/>
		<cfargument name="idTarget"	 type="Numeric" required="false"/>
				
		<cfset var resultUpload	= ArrayNew(1)>
		<cfset var lenFiles 	= arrayLen(filesSelected)>
		<cfset var statusReturn	= 0>
		
		<cfloop index="idx" from="1" to="#lenFiles#">
			
			 <cfset statusReturn = uploadFile(filesSelected[idx],idTarget)>

			<cfset ArrayAppend(resultUpload, statusReturn)> 
		
		</cfloop>

		<cfset var webPath 	= GetDirectoryFromPath( GetCurrentTemplatePath() )>
		<cfset var pathDirTMP 	= webPath & "tmp/">

		<cfif DirectoryExists('#pathDirTMP#')>
			<!--- SUPPRESSION Dossier et FICHIER TEMPORAIRE --->
			<cfset deleteDirectory(pathDirTMP)>
		</cfif>
			
		<cfreturn resultUpload>
	</cffunction>
	
	<!--- 
	 	Ajouter fichier dans ftp
	--->
	<cffunction name="handleFTP" access="private" output="false" returntype="numeric">
		<cfargument name="pathDirFileTmpRenamed" 	type="string" required="true">
		<cfargument name="pathRemoteFile" 			type="string" required="true">
		<cftry>	
			<cfset var retour = 0>
			<!--- OUVRIR LA CONNEXION --->
			<cfftp connection="ftp" username="image" password="cockney-octet-claimant" server="Pelican.consotel.fr" action="open" stopOnError="Yes">
			
			<!--- UPLOAD DU FICHIER --->
			<cfftp action="putFile" connection="ftp" localFile="#pathDirFileTmpRenamed#" remoteFile="#pathRemoteFile#" timeout="3000">
			
			<!--- VERIF EXISTENCE FICHIER --->
			<cfftp action="existsfile" connection="ftp" remoteFile="#pathRemoteFile#"><!--- directory="#dirFtp#" --->
			<cfif cfftp.returnValue eq "yes">
				 <cfset retour = 1>
			<cfelse>
				<cfset retour = 0>
			</cfif>
			
			<!--- FERMER LA CONNEXION --->
			<cfftp connection="ftp" action="close" stopOnError="Yes">
		<cfcatch>
			<cflog type="error" text="#CFCATCH.Message#">													
		</cfcatch>					
		</cftry>
		<cfreturn retour>
	</cffunction>
	
	
	<!--- 
	 	supprimer le dossier TMP et le fichier tmp
	--->
	<cffunction name="deleteDirectory" access="private" output="false" returntype="void">
		<cfargument name="pathDirTMP" type="string" required="true">
		
		<cfif DirectoryExists('#pathDirTMP#')>
			<cfdirectory action="delete" directory="#pathDirTMP#" type="dir" mode="777" recurse="true">
		</cfif>
		
	</cffunction>
	
	<!--- 
	 	créer dossier TMP + fichier TMP
	--->
	<cffunction name="createDirTMP" access="private" output="false" returntype="numeric">
		
		<cfargument name="pathDirTMP" type="string" required="true">
		<cfargument name="currentFile" type="struct" required="true">
		
		<cftry>
			
			<cfif NOT DirectoryExists('#pathDirTMP#')>
				<cfdirectory action="Create" directory="#pathDirTMP#" type="dir" mode="777">
			</cfif>
			
			<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M24.upload.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
			
			<cfset pathFileTMP = pathDirTMP & "/" & currentFile.fileName>
			<cffile action="write" output="#currentFile.fileData#" file="#pathFileTMP#" mode="777">
			<cfset statusReturn = 1>
			
			<cfreturn statusReturn>
			
		<cfcatch>
			<!--- log --->
			<cflog type="error" text="#CFCATCH.Message#">
			<!--- mail d'erreur --->
			<cfmail server="mail-cv.consotel.fr" port="25" from="container@saaswedo.com" to="monitoring@saaswedo.com" 
					failto="monitoring@saaswedo.com" subject="[M61] Erreur lors de l'upload de fichiers" type="html">
					
					#CFCATCH.Message#<br/><br/><br/><br/>
					
			</cfmail>
			
		</cfcatch>					
		</cftry>
		
	</cffunction>

	<cffunction name = "addInternalPicture" returnType = "numeric" access = "private">
		<cfargument name="myUrl" type="String" required="true"/>
		<cfargument name="myFileName" type="string" required="true"/>
		<cfargument name="myPathFile" type="string" required="true"/>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_import_icecat.addInternalPic">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#myUrl#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#myFileName#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#myPathFile#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<!--- CREATION UUID (ID UNIQUE SUR 35 CAR) --->
	
	<cffunction name="initUUID" access="remote" output="false" returntype="String" hint="CREATION UUID (ID UNIQUE SUR 35 CAR)">
		
			<cfset uuid = createUUID()>
					
		<cfreturn uuid> 
	</cffunction>
	
</cfcomponent>