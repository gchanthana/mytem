<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.mobileIron.DeviceManagement"
extends="fr.saaswedo.api.mdm.impl.mobileIron.MdmService" implements="fr.saaswedo.api.mdm.core.services.IDeviceManagement"
hint="Implémentation MobileIron. Les fonctionnalités sont implémentées de manière à éviter la présence de doublons (SERIAL,IMEI) dans le MDM">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.mobileIron.DeviceManagement">
		<cfreturn "fr.saaswedo.api.mdm.impl.mobileIron.DeviceManagement">
	</cffunction>
	
	<cffunction access="public" name="lock" returntype="Boolean" hint="Verrouille un device">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="newPinCode" type="String" required="false" default="0000" hint="Ignoré">
		<cftry>
			<cfset var httpResponse={}>
			<cfset var actionStatus=FALSE>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfset var deviceUUID=device.uuid>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{TYPE="formfield",NAME="Reason",VALUE="LOCK"}
				]>
				<cfset service.endpoint=service.endpoint & "/lock/" & deviceUUID>
				<cfset service.HTTP_METHOD=remoting.HTTP_PUT()>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
					<cfset device.RESULT=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Statut de l'opération --->
							<cfif structKeyExists(jsonResponse,"messages") AND structKeyExists(jsonResponse.messages,"message")>
								<cfset msg=jsonResponse.messages.message>
								<cfset actionStatus=(REFindNoCase("^Device is locked successfully",msg) GT 0)>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<!--- Si actionStatus vaut FALSE --->
			<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
			<cfif actionStatus EQ TRUE>
				<cflog type="information" text="[#log#] MobileIron LOCK [#serial#,#imeiValue#]: #msg#">
			<cfelse>
				<cflog type="warning" text="[#log#] MobileIron LOCK failed [#serial#,#imeiValue#]: #msg#">
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="unlock" returntype="Boolean"
	hint="Déverrouille un device et supprime le passcode (PIN) existant">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<cfset var httpResponse={}>
			<cfset var actionStatus=FALSE>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfset var deviceUUID=device.uuid>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{TYPE="formfield",NAME="Reason",VALUE="UNLOCK"}
				]>
				<cfset service.endpoint=service.endpoint & "/unlock/" & deviceUUID>
				<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
					<cfset device.RESULT=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Statut de l'opération --->
							<cfif structKeyExists(jsonResponse,"messages") AND structKeyExists(jsonResponse.messages,"message")>
								<cfset msg=jsonResponse.messages.message>
								<cfset actionStatus=(REFindNoCase("^[1-9]+[0-9]* passcode\(s\)",msg) GT 0)>
								<cfif structKeyExists(jsonResponse,"passcodes") AND
									structKeyExists(jsonResponse.passcodes,"passcode") AND structKeyExists(jsonResponse.passcodes.passcode,"value")>
									<cfset var passcode=jsonResponse.passcodes.passcode.value>
									<cfset actionStatus=actionStatus AND (compareNoCase("NA",passcode) NEQ 0)>
									<cfset msg=msg & " (passcode : #passcode#)">
								</cfif>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<!--- Si actionStatus vaut FALSE --->
			<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
			<cfif actionStatus EQ TRUE>
				<cflog type="information" text="[#log#] MobileIron LOCK [#serial#,#imeiValue#]: #msg#">
			<cfelse>
				<cflog type="warning" text="[#log#] MobileIron UNLOCK failed [#serial#,#imeiValue#]: #msg#">
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="locate" returntype="Boolean" hint="Localise un device. L'opération est synchrone (Bloquante)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<cfset var httpResponse={}>
			<cfset var actionStatus=FALSE>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfset var deviceUUID=device.uuid>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{TYPE="formfield",NAME="locatenow",VALUE=TRUE},
					{TYPE="formfield",NAME="Reason",VALUE="LOCATE"}
				]>
				<cfset service.endpoint=service.endpoint & "/locate/" & deviceUUID>
				<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
					<cfset device.RESULT=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Statut de l'opération --->
							<cfif structKeyExists(jsonResponse,"messages") AND structKeyExists(jsonResponse.messages,"message")>
								<cfset msg=jsonResponse.messages.message>
								<cfset actionStatus=(REFindNoCase("^Sent wake up request to client",msg) GT 0)>
								<cfif structKeyExists(jsonResponse,"locations") AND structKeyExists(jsonResponse.locations,"location")
									AND structKeyExists(jsonResponse.locations.location,"lookupResult")>
									<!--- Condition de validité des infos --->
									<cfset var locationNode=jsonResponse.locations.location>
									<cfset actionStatus=actionStatus AND (compareNoCase("LookupFailure",locationNode.lookupResult) NEQ 0)>
									<cfset msg=msg & " (lookupResult : #locationNode.lookupResult#)">
								</cfif>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
			<cfif actionStatus EQ TRUE>
				<cflog type="information" text="[#log#] MobileIron LOCATE [#serial#,#imeiValue#]: #msg#">
			<cfelse>
				<cflog type="warning" text="[#log#] MobileIron LOCATE failed [#serial#,#imeiValue#]: #msg#">
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn actionStatus>
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- Réinitialise le device puis effectue l'action REVOKE pour le retirer du MDM. Retourne le statut des 2 actions --->
	<cffunction access="public" name="wipe" returntype="Boolean" hint="Réinitialise un device puis le retire du MDM avec revoke()
	pour éviter la présence de doublons (SERIAL,IMEI). Mais il n'est pas requis car les devices concernés ne sont pas ramenés par cette API">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="erasedMemoryCard" type="Boolean" required="false" default="false" hint="Ignoré">
		<cfset var deviceUUID="">
		<cftry>
			<cfset var httpResponse={}>
			<cfset var actionStatus=FALSE>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfset deviceUUID=device.uuid>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{TYPE="formfield",NAME="Reason",VALUE="WIPE"}
				]>
				<cfset service.endpoint=service.endpoint & "/wipe/" & deviceUUID>
				<cfset service.HTTP_METHOD=remoting.HTTP_PUT()>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
					<cfset device.RESULT=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Statut de l'opération --->
							<cfif structKeyExists(jsonResponse,"messages") AND structKeyExists(jsonResponse.messages,"message")>
								<cfset msg=jsonResponse.messages.message>
								<cfset actionStatus=(REFindNoCase("success",msg) GT 0)>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<!--- Si la réinitialisation a été effectuée alors le device est retiré --->
			<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
			<cfif actionStatus EQ TRUE>
				<cfset actionStatus=actionStatus AND revoke(deviceUUID)>
				<cfif actionStatus EQ TRUE>
					<cflog type="information" text="[#log#] MobileIron WIPE and RETIRE [#serial#,#imeiValue#]: #msg#">
				<cfelse>
					<cflog type="warning" text="[#log#] MobileIron WIPE success but RETIRE failed for [#serial#,#imeiValue#]">
				</cfif>
			<cfelse>
				<cflog type="warning" text="[#log#] MobileIron WIPE failed [#serial#,#imeiValue#]: #msg#">
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="revoke" returntype="Boolean" hint="La révocation correspond au retrait un device dans MobileIron">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cftry>
			<cfset var httpResponse={}>
			<cfset var actionStatus=FALSE>
			<cfset var msg="NO MESSAGE">
			<cfset var serial=ARGUMENTS.serialNumber>
			<cfset var imeiValue=ARGUMENTS.imei>
			<cfset var deviceFromCache=findDeviceFromCache(ARGUMENTS.serialNumber,ARGUMENTS.imei)>
			<cfif NOT structIsEmpty(deviceFromCache[xmlDeviceKey()])>
				<cfset var device=deviceFromCache[xmlDeviceKey()]>
				<cfset deviceUUID=device.uuid>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{TYPE="formfield",NAME="Reason",VALUE="RETIRE"}
				]>
				<cfset service.endpoint=service.endpoint & "/retire/" & deviceUUID>
				<cfset service.HTTP_METHOD=remoting.HTTP_PUT()>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset httpResponse=result[remoting.RESULT()]>
					<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
					<cfset device.RESULT=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Statut de l'opération --->
							<cfif structKeyExists(jsonResponse,"messages") AND structKeyExists(jsonResponse.messages,"message")>
								<cfset msg=jsonResponse.messages.message>
								<cfset actionStatus=(REFindNoCase("success",msg) GT 0)>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			<cfset var log=GLOBALS().CONST("MobileIron.LOG")>
			<cfif actionStatus EQ TRUE>
				<cflog type="information" text="[#log#] MobileIron RETIRE [#serial#,#imeiValue#]: #msg#">
			<cfelse>
				<cflog type="warning" text="[#log#] MobileIron RETIRE failed [#serial#,#imeiValue#]: #msg#">
			</cfif>
			<cfreturn actionStatus>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH,SERIAL=ARGUMENTS.serialNumber,IMEI=ARGUMENTS.imei}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<!--- Erreur non critique --->
				<cfreturn actionStatus>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="register" returntype="Boolean" hint="Retourne toujours TRUE car n'existe pas dans MobileIron">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<!--- Cette action n'a pas besoin d'etre effectuée dans MobileIron --->
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction access="public" name="remove" returntype="void" hint="Retire un device du MDM en utilisant revoke()">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfthrow type="Custom" errorCode="#code#" message="Function 'Remove device' is not implemented">
	</cffunction>
	
	<!--- =========== Action MDM non supportées ou non implémentées =========== --->
	
	<cffunction access="public" name="corporateWipe" returntype="Boolean" hint="Non supporté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var code=GLOBALS().CONST("MobileIron.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="Function 'Corporate wipe device' is not supported">
	</cffunction>
	
	<cffunction access="public" name="putDeviceProperties" returntype="void" hint="Non supporté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="properties" type="Array" required="true" hint="Propriétés à ajouter">
		<cfset var code=GLOBALS().CONST("MobileIron.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="Function 'Put device properties' is not supported">
	</cffunction>
	
	<cffunction access="public" name="add" returntype="String" hint="Non supporté">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<cfset var code=GLOBALS().CONST("MobileIron.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="Function 'Add device' is not implemented">
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
</cfcomponent>