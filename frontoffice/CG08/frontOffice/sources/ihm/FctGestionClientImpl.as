package ihm
{
	import composants.util.ConsoviewAlert;
	
	import entity.ClientVO;
	
	import event.FctGestionClientEvent;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.utils.ByteArray;
	
	import ihm.popup.PopUpCreateClientIHM;
	import ihm.popup.PopUpEditClientIHM;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.DateField;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	
	import service.FctGestionClientService;
	
	import utils.abstract.AbstractFonction;
	import utils.composants.export.FlexToExcel;
	
	public class FctGestionClientImpl extends AbstractFonction
	{
		private var _niveauUtilisateur		:Number;
		private var _iServiceGestionClient	:FctGestionClientService;
		private var _dpListeClients			:ArrayCollection;
		private var _isSWD					:Boolean;
		private var _startindex 				:int=1;
		private var _nbreSelectedClient		:int;
		private var _nbreTotalResult		:Number;
		private var _dpListeMasters			:ArrayCollection;
		
		[Bindable]
		public static var NB_PAR_PAGE		:Number = 33;
		public static const MAS				:String = 'MAS';
		public static const CLI				:String = 'CLI';
		public static const DIS				:String = 'DIS';
		public static const APP				:String = 'APP';
		
		[Bindable]public var pdg_clients	:PaginateDatagrid;
		[Bindable]public var btn_nouveau	:Button;
		[Bindable]public var btn_exporter	:Button;
		[Bindable]public var cbx_allClient	:CheckBox;
		[Bindable]public var ti_filtre		:TextInput;
		[Bindable]public var ti_motClef 	:TextInput;
		[Bindable]public var btn_rechercher	:Button;
		[Bindable]public var combo_filtre	:ComboBox;
		[Bindable]public var combo_trie		:ComboBox;
		
		[Bindable]public var dgc_master 			:DataGridColumn;
		[Bindable]public var dgc_idClient 			:DataGridColumn;
		[Bindable]public var dgc_client 			:DataGridColumn;
		[Bindable]public var dgc_distributeur 		:DataGridColumn;
		[Bindable]public var dgc_application 		:DataGridColumn;
		[Bindable]public var dgc_nbrCollectes 		:DataGridColumn;
		[Bindable]public var dgc_login 				:DataGridColumn;
		[Bindable]public var dgc_creation 			:DataGridColumn;
		[Bindable]public var dgc_modification 		:DataGridColumn;
		
		public function FctGestionClientImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		protected function init(event:FlexEvent):void
		{
			iServiceGestionClient = new FctGestionClientService();
			this.initDispaly();
			this.initAcces();
			this.initListeners();
			this.initData();
		}
		
		private function initDispaly():void
		{
			(pdg_clients.getChildAt(0) as UIComponent).includeInLayout = false;
			pdg_clients.getChildAt(0).visible = false;
			(pdg_clients.getChildAt(1) as UIComponent).includeInLayout = false;
			pdg_clients.getChildAt(1).visible = false;
			
		}
		
		private function initData():void
		{
			this.getListeMasters();
			this.getListeClients();
		}
		
		private function getListeMasters():void
		{
			this.iServiceGestionClient.getListeMasters();
			
		}
		
		private function getListeClients():void
		{
			var textFiltre:String = ''
			var textTrie:String = '';
			if(combo_filtre.selectedIndex > -1)
			{
				textFiltre = combo_filtre.selectedItem.idCode; 
			}
			
			if(combo_trie.selectedIndex > -1)
			{
				textTrie = combo_trie.selectedItem.idCode;
			}
			
			this.iServiceGestionClient.getListeClients(ti_motClef.text, textFiltre, textTrie, startindex, NB_PAR_PAGE);
		}
		
		private function initListeners():void
		{
			this.iServiceGestionClient.myDatas.addEventListener(FctGestionClientEvent.LISTE_CLIENTS, 	getListeClientsHandler);
			this.iServiceGestionClient.myDatas.addEventListener(FctGestionClientEvent.EDITION_CLIENT, 	editClientHandler);
			this.iServiceGestionClient.myDatas.addEventListener(FctGestionClientEvent.LISTE_MASTRERS, 	getListeMastersHandler);
			
			this.btn_nouveau.addEventListener(MouseEvent.CLICK, 			clickNouveauClientHandler);
			this.btn_exporter.addEventListener(MouseEvent.CLICK, 			clickExportClientsHandler);
			this.cbx_allClient.addEventListener(MouseEvent.CLICK, 			clickAllClientsHandler);
			this.btn_rechercher.addEventListener(MouseEvent.CLICK, 			clickRechercheHandler);
			this.btn_rechercher.addEventListener(KeyboardEvent.KEY_DOWN, 	clickRechercheHandler);
			
			// Listeners pour les clicks à partir d'ItemRenderer
			this.addEventListener(FctGestionClientEvent.CLICK_EDITER_CLIENT, 	 	onClickEditerHandler)
			this.addEventListener(FctGestionClientEvent.CLICK_VERROUILLER_CLIENT, 	onClickVerrouillerHandler)
			this.addEventListener(FctGestionClientEvent.CLICK_DESACTIVER_CLIENT, 	onClickDesactiverHandler)
			
		}
		
		protected function getListeMastersHandler(event:Event):void
		{
			this.dpListeMasters = iServiceGestionClient.myDatas.listeMasters;
		}
		
		protected function onClickDesactiverHandler(event:Event):void
		{
			// TODO Auto-generated method stub
			
		}
		
		protected function onClickVerrouillerHandler(event:Event):void
		{
			// TODO Auto-generated method stub
			
		}
		
		protected function onClickEditerHandler(event:Event):void
		{
			var popup:PopUpEditClientIHM 	= new PopUpEditClientIHM();
			popup.currentClient 			= pdg_clients.selectedItem as ClientVO;
			popup.iServiceGestClient 		= this.iServiceGestionClient;
			PopUpManager.addPopUp(popup, this, true);
			PopUpManager.centerPopUp(popup);
			PopUpManager.bringToFront(popup);
		}
		
		protected function editClientHandler(event:Event):void
		{
			ti_motClef.text = '';
			this.getListeClients();
		}
		
		
		protected function clickExportClientsHandler(event:MouseEvent):void
		{
			if(pdg_clients != null && dpListeClients.length > 0 )
			{
				var dg_clients:DataGrid = new DataGrid();
				dg_clients.dataProvider = dpListeClients;
				var arrayExport:Array = new Array;
				
				if(isSWD == true){
					arrayExport = [dgc_master, dgc_idClient, dgc_client, dgc_distributeur,  dgc_application, dgc_nbrCollectes, dgc_login, dgc_creation, dgc_modification];
				}
				else
				{
					arrayExport = [dgc_master, dgc_client,  dgc_application, dgc_creation, dgc_modification];
				}
				
				dg_clients.columns = arrayExport;
				dpListeClients.refresh();
				
				try
				{
					var dateFormatter:DateFormatter = new DateFormatter();
					dateFormatter.formatString = 'DDMMMJJNNSS';
					
					var defaultName:String = 'exportedList_' + dateFormatter.format(new Date()) + '.xls';
					FlexToExcel.exportDataGrid(dg_clients, defaultName);
					
				} 
				catch(error:Error) 
				{
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('CG08', 'Probl_me_d_export_des_donn_es__'), ResourceManager.getInstance().getString('CG08', 'Erreur'));
				}
			}
			else
			{
				ConsoviewAlert.afficherError("La liste est vide", "Erreur");
			}
		}
		
		protected function clickAllClientsHandler(event:MouseEvent):void
		{
			var ns:int = 0;
			var taille:int = dpListeClients.length
			for(var i:int =0; i < taille; i++ )
			{
				(dpListeClients[i] as ClientVO).SELECTED = cbx_allClient.selected;
				if((dpListeClients[i] as ClientVO).SELECTED)
					ns ++;
			}
			
			this.nbreSelectedClient = ns;
			this.dpListeClients.refresh();
		}
		
		protected function clickNouveauClientHandler(event:MouseEvent):void
		{
			var popup:PopUpCreateClientIHM 	= new PopUpCreateClientIHM();
			popup.iServiceGestClient 		= this.iServiceGestionClient;
			popup.dpListeMasters			= this.dpListeMasters;
			PopUpManager.addPopUp(popup, this, true);
			PopUpManager.centerPopUp(popup);
			PopUpManager.bringToFront(popup);
		}
		
		protected function getListeClientsHandler(event:Event):void
		{
			this.dpListeClients = iServiceGestionClient.myDatas.listeClients;
			pdg_clients.nbItemParPage = NB_PAR_PAGE;
			pdg_clients.nbTotalItem = dpListeClients.length;
			if(dpListeClients.length > 1){
				nbreTotalResult = (dpListeClients[1] as ClientVO).NBROWS;
				pdg_clients.paginationBox.initPagination(nbreTotalResult, NB_PAR_PAGE);
			}
		}
		
		private function initAcces():void
		{
			_niveauUtilisateur = Number(CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDNIVEAU);
//			_niveauUtilisateur = 2;
			switch(_niveauUtilisateur)
			{
				case 1:
				{
					// Vue Saaswedo (SWD)
					isSWD = true;
					break;
				}
				case 2:
				{
					// Vue Application Master ou Partenaire (AMP)
					isSWD = false;
					break;
				}
				default :
				{
					// Comportement à préciser : Quel message ?
					// un email ? deconnexion ?
					break;
				}
			}
		}
		
		protected function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			if(pdg_clients.currentIntervalle!=null)
			{
				if (pdg_clients.currentIntervalle.indexDepart == 0)
				{
					startindex = 1;
				}
				else
				{		
					startindex = pdg_clients.currentIntervalle.indexDepart + 1;
				}
				
				this.getListeClients();
			}
		}
		
		protected function onChangeFiltreHandler(event:Event):void
		{
			dpListeClients.filterFunction = onFilterInformation;
			dpListeClients.refresh();
		}
		
		private function onFilterInformation(item:Object):Boolean
		{
			if((String((item as ClientVO).NOM).toLowerCase().search(ti_filtre.text.toLowerCase()) != -1) || 
			   (String((item as ClientVO).master).toLowerCase().search(ti_filtre.text.toLowerCase()) != -1))
				return true;
			else
				return false;
		}
		
		protected function clickRechercheHandler(evt:Event):void
		{
			// si texte vide ou length >= 3
			if((ti_motClef.text == '') || ((ti_motClef.text).length >= 3)){
				this.getListeClients();
			}
			else
			{
				Alert.show('Veuillez entrer au moins 3 lettres pour la recherche', 'Alerte');
			}
		}
		
		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
			{
				var dt:Date=item[column.dataField] as Date;
				return DateField.dateToString(dt, ResourceManager.getInstance().getString('CG08', '_DD_MM_YYYY_'));
			}
			else
			{
				return '';
			}
		}
		
		protected function onSortDateFunc(obj1:Object, obj2:Object, fieldName:String):int
		{
			if (obj1.hasOwnProperty(fieldName) && obj2.hasOwnProperty(fieldName))
			{
				return ObjectUtil.dateCompare(obj1[fieldName], obj2[fieldName]);
			}
			else
				return 1; //si la date est indéfinie alors elle est toujours supérieur à celle definie
		}
		
		
		public function get iServiceGestionClient():FctGestionClientService { return _iServiceGestionClient; }
		
		public function set iServiceGestionClient(value:FctGestionClientService):void
		{
			if (_iServiceGestionClient == value)
				return;
			_iServiceGestionClient = value;
		}
		
		[Bindable]
		public function get dpListeClients():ArrayCollection { return _dpListeClients; }
		
		public function set dpListeClients(value:ArrayCollection):void
		{
			if (_dpListeClients == value)
				return;
			_dpListeClients = value;
		}
		
		[Bindable]
		public function get isSWD():Boolean { return _isSWD; }
		
		public function set isSWD(value:Boolean):void
		{
			if (_isSWD == value)
				return;
			_isSWD = value;
		}

		public function get startindex():int
		{
			return _startindex;
		}

		public function set startindex(value:int):void
		{
			_startindex = value;
		}
		
		[Bindable]
		public function get nbreSelectedClient():int { return _nbreSelectedClient; }
		
		public function set nbreSelectedClient(value:int):void
		{
			if (_nbreSelectedClient == value)
				return;
			_nbreSelectedClient = value;
		}
		
		public function get dpListeMasters():ArrayCollection { return _dpListeMasters; }
		
		public function set dpListeMasters(value:ArrayCollection):void
		{
			if (_dpListeMasters == value)
				return;
			_dpListeMasters = value;
		}
		[Bindable]
		public function get nbreTotalResult():Number { return _nbreTotalResult; }
		
		public function set nbreTotalResult(value:Number):void
		{
			if (_nbreTotalResult == value)
				return;
			_nbreTotalResult = value;
		}
	}
}