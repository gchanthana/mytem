<cfcomponent author="Cedric" displayname="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP" extends="com.adobe.coldfusion.http"
implements="fr.saaswedo.api.patterns.error.IErrorTarget" hint="Extension de CFHTTP qui permet d'accéder aux paramètres d'une requete HTTP">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.remoting.ext.ExtendedCFHTTP">
		<cfreturn "fr.saaswedo.api.remoting.ext.ExtendedCFHTTP">
	</cffunction>
	
	<cffunction access="public" name="getParameters" returntype="Array" hint="Retourne les paramètres fournis à la fonction parent addParam()">
		<!--- Les paramètres fournis à SUPER.addParam() sont référencés par variables.parameters --->
		<cfif structKeyExists(VARIABLES,"parameters")>
			<cfreturn VARIABLES.parameters>
		<cfelse>
			<cfreturn []>
		</cfif>
	</cffunction>
	
	<!--- fr.saaswedo.api.patterns.error.IErrorTarget --->
	<cffunction name="hasErrorTypeHandler" returntype="Boolean" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="errorCode" type="String" required="true">
		<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.hasErrorTypeHandler() is not implemented">
	</cffunction>
	
	<cffunction name="addErrorHandler" returntype="void" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="errorHandler" type="fr.saaswedo.api.patterns.error.IErrorHandler" required="true">
		<cfargument name="errorCode" type="String" required="true">
		<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.addErrorHandler() is not implemented">
	</cffunction>
	
	<cffunction name="dispatchError" returntype="void" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="cfCatchObject" type="Any" required="true">
		<cfargument name="errorInfos" type="Struct" required="false">
		<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.dispatchError() is not implemented">
	</cffunction>
	
	<cffunction access="public" name="componentName" returntype="String"
	hint="Retourne le nom complet du composant. Cette fonction peut etre appelée avant init()">
		<cfif NOT structKeyExists(VARIABLES,"cfcName")>
			<!--- Stocke le nom complet du CFC pour ne pas appeler getMetadata() à chaque fois. Voir componentName() --->
			<cfset VARIABLES.cfcName=getMetadata(THIS).NAME>
		</cfif>
		<cfreturn VARIABLES.cfcName>
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP" hint="Appele le constructeur parent et écrit un log">
		<!--- TODO: Cette librairie est obsolète --->
		<cfthrow type="Custom" message="Obsolete implementation" detail="This implementation will be removed and should not be used">
		
		<cfset var log=GLOBALS().CONST('Remoting.LOG')>
		<cfif isDefined("ARGUMENTS") AND (NOT structIsEmpty(ARGUMENTS))>
			<cfset SUPER.init(ARGUMENTS)>
		<cfelse>
			<cfset SUPER.init()>
		</cfif>
		<cflog type="information" text="[#log#] #componentName()# initialized">
		<cfreturn THIS>
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
		
	<cffunction access="private" name="GLOBALS" returntype="fr.saaswedo.api.patterns.const.Globals"
	hint="Retourne le conteneur des constantes globales. Cette fonction peut etre appelée avant init()">
		<cfset var globalsKey="fr.saaswedo.api.patterns.const.Globals">
		<cfif NOT structKeyExists(VARIABLES,globalsKey)>
			<cfset VARIABLES[globalsKey]=new fr.saaswedo.api.patterns.const.Globals()>
		</cfif>
		<cfreturn VARIABLES[globalsKey]>
	</cffunction>
</cfcomponent>