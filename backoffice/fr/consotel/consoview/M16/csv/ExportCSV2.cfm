﻿<cfsetting enablecfoutputonly="true"/>
<cfset codelangue 	= SESSION.USER.GLOBALIZATION>
<cfset setlocale('French (Standard)')>
<cfset NewLine = Chr(10) & Chr(13) >
<cfset NewLine10 = Chr(10)><!--- linefeed character : saut de ligne --->
<cfset NewLine13 = Chr(13)><!---carriage return character : Retour chariot --->
<cfset COL_POOL = "">
<cfset DATA_POOL ="">
<cfset POOL_SEPARATOR ="">
<cfset label="Commandes">
<cfset qObject = createObject("component","fr.consotel.consoview.M16.ExportCommande")>
<cfset qGetGrid="">

	<cfif isDefined("FORM.ID_COMMANDES") AND compareNoCase(FORM.METHODE,"fournirDataSetToExport") eq 0>
		<cfset listeIDs = replace(FORM.ID_COMMANDES,'%2C',',','all')/>
		
		<cfset qGetGrid=qObject.fournirDataSetToExport(listeIDs)>
		<cfset COL_POOL = "">
		<cfset DATA_POOL ="">
		<cfset POOL_SEPARATOR ="">

		<cftry>
			<cfset runExportDataSet()>
		<cfcatch type = "Any">
			<cflog text="#cfcatch.message#">
		</cfcatch>
		</cftry>
		
	<cfelse>
		<!--- AJOUT IDPOOL en paramètre : MYT-1265 --->
		<!--- CODE DEPRECATED  voir  fr.consotel.consoview.M16.export.ExportAllOrders --->
		<cfif isDefined("FORM.IDPOOL") AND compareNoCase(FORM.METHODE,"fournirAllDataSetToExport") eq 0>
			
			<!---
			<cfset qGetGrid=qObject.fournirAllDataSetToExport(val(0))>
			<cfif codelangue EQ "fr_FR">
				<cfset COL_POOL = "LIBELLE_POOL">
			<cfelse>
				<cfset COL_POOL = "POOL_NAME">
			</cfif>
			<cfset DATA_POOL ="LABEL_POOL">
			<cfset POOL_SEPARATOR =";">
			--->
		</cfif>
		
	</cfif>

<cffunction name = "runExportDataSet" returnType = "any" access = "private" description = "export data set" displayName = "runExportDataSet" hint = "export data set">
<cfsavecontent variable="report">
<cfprocessingdirective suppressWhitespace="true">
<cfif codelangue EQ "fr_FR">
<cfoutput>NOM_RACINE;#COL_POOL##POOL_SEPARATOR#TYPE_COMMANDE;LIBELLE_TYPE_COMMANDE;LIBELLE_ETAT;NOMBRE_DEJOUR_SLA;HEURE_LIMITE_SLA;LIBELLE;NUMERO_OPERATION;NUMERO_ORDRE;NUMERO_MARCHE;REF_CLIENT_2;PRIX;MONTANT_TOTAL;OPERATEUR;DISTRIBUTEUR;AGENCE;COMPTE_FACTURATION;SOUS_COMPTE;CODE_LISTE;DATE_DE_PREPARATION;DATE_DEMANDE_VALID_INT;DATE_VALID_INT;DATE_DMDE_VALID_INTERNE2;DATE_VALID_INTERNE2;DATE_DMDE_VALID_INTERNE3;DATE_VALID_INTERNE3;DATE_DMDE_VALID_INTERNE4;DATE_VALID_INTERNE4;DATE_DMDE_VALID_INTERNE5;DATE_VALID_INTERNE5;DATE_DE_COMMANDE;DATE_DEM_VALID_DIST;DATE_VALID_ENV_CMD;DATE_ACC_REC;DATE_EXP_CMD;DATE_ACC_REC_LIV;DT_DEM_ACTIV;DATE_SUPP;DATE_CLOTURE;CONTACT_PREPARATION;CONTACT_DEMANDE_VALID_INT;CONTACT_VALID_INT;CONT_DMDE_VALID_INTERNE2;CONTACTE_VALID_INTERNE2;CONT_DMDE_VALID_INTERNE3;CONTACTE_VALID_INTERNE3;CONT_DMDE_VALID_INTERNE4;CONTACTE_VALID_INTERNE4;CONT_DMDE_VALID_INTERNE5;CONTACTE_VALID_INTERNE5;CONTACT_VALID_ENV_CMD;CONTACT_ACC_REC;CONTACT_EXP_CMD;CONTACT_ACC_REC_LIV;CONTACT_DEM_ACTIV;CONTACT_SUPP;CONTACT_CLOTURE;A_L_ATTENTION;NOM_SITE;REFERENCE_SITE_LIV;CODE_INTERNE_SITE_LIV;ADRESSE_SITE_LIV;CODE_POSTAL_SITE_LIV;VILLE_SITE_LIVRAISON;SITE_FACT;REFERENCE_SITE_FACT;CODE_INTERNE_SITE_FACT;ADRESSE_SITE_FACT;CODE_POSTAL_SITE_FACT;VILLE_SITE_FACT;REFERENCE_EQ;TYPE_EQUIPEMENT;IMEI;MODELE;NIVEAU;PUK1;LIBELE_COLLABORATEUR;MATRICULE;CIVILITE;PRENOM;NOM;STATUS_EMPLOYE;BOOL_PUBLICATION;CODE_INTERNE_COLLABORATEUR;EMAIL;COMMENTAIRE_COMMANDE;COMMENTAIRE_EMPLOYE;COMMENTAIRE_SITE_FACT;COMMENTAIRE_SITE_LIVRAISON;NUM_SIM;IDSOUS_TETE;SOUS_TETE;CODE_RIO;DATE_PORTAGE_SOUHAITE;TYPE_ABONNEMENT;LIBELLE_ABONNEMENT;CONTACT;DATE_EFFET_PREVUE;DATE_ENTREE;DATE_SORTIE;IDENTIFIANT;NO_CDE_CLIENT;TRANSPORTEUR;NUMERO_TRACKING#NewLine13#</cfoutput>
<cfelse>
<cfoutput>CLIENT_NAME;#COL_POOL##POOL_SEPARATOR#ORDER_TYPE;ORDER_TYPE_NAME;STATUS;SLA_NUMBER_OF_DAYS;SLA_TIME_LIMIT;ORDER_NAME;OPERATION_NUMBER;ORDER_NUMBER;CONTRACT_NUMBER;CLIENT_REF2;RATE;TOTAL_AMOUNT;CARRIER;RESELLER;AGENCY;BAN;SUB_BAN;SUB_SUB_BAN;PREPARATION_DATE;INTERNAL_REQUEST_APPROV1_DATE;INTERNAL_APPROV1_DATE;INTERNAL_REQUEST_APPROV2_DATE;INTERNAL_APPROV2_DATE;INTERNAL_REQUEST_APPROV3_DATE;INTERNAL_APPROV3_DATE;INTERNAL_REQUEST_APPROV4_DATE;INTERNAL_APPROV4_DATE;INTERNAL_REQUEST_APPROV5_DATE;INTERNAL_APPROV5_DATE;ORDERING_DATE;RESELLER_APPROV_RQST_DATE;SHIPPING_APPROVAL_DATE;ACK_O_RECEPTION_DATE;SHIPPING_DATE;ACK_O_DELIVERY_DATE;DEM_ACTIV_DATE;DELETE_DATE;CLOSE_DATE;PREPARATION_CONTACT;INTERNAL_REQUEST_APPROV1_CONTACT;INTERNAL_APPROV1_CONTACT;INTERNAL_REQUEST_APPROV2_CONTACT;INTERNAL_APPROV2_CONTACT;INTERNAL_REQUEST_APPROV3_CONTACT;INTERNAL_APPROV3_CONTACT;INTERNAL_REQUEST_APPROV4_CONTACT;INTERNAL_APPROV4_CONTACT;INTERNAL_REQUEST_APPROV5_CONTACT;INTERNAL_APPROV5_CONTACT;SHIPPING_APPROVAL_CONTACT;ACK_O_RECEPTION_CONTACT;SHIPPING_CONTACT;ACK_O_DELIVERY_CONTACT;DEM_ACTIV_CONTACT;DELETE_CONTACT;CLOSE_CONTACT;TO;LOCATION_NAME;DELIVERY_LOCATION_ID;DELIVERY_LOCATION_CODE;DELIVERY_LOCATION_ADDRESS;DELIVERY_LOCATION_ZIP;DELIVERY_LOCATION_CITY;BILLING_LOCATION;BILLING_LOCATION_ID;BILLING_LOCATION_CODE;BILLING_LOCATION_ADDRESS;BILLING_LOCATION_ZIP;BILLING_LOCATION_CITY;DEVICE_ID;DEVICE_TYPE;IMEI;MODELE;LEVEL;PUK1;EMPLOYEE;EID;CIVILITY;FIRST_NAME;LAST_NAME;EMPLOYEE_STATUS;PUBLIC_DIRECTORY;INTERNAL_EMPLOYEE_CODE;EMAIL;ORDER COMMENTS;EMPLOYEE_COMMENTS;BILLING_SITE_COMMENTS;DELIVERY_SITE_COMMENTS;SIM_NUMBER;LINE_NUMBER_ID;LINE_NUMBER;TRANSFER_ID;REQUESTED_TRANSFER_DATE;SUBSCRIPTION_TYPE;SUBSCRIPTION_NAME;CONTACT;EFFECT_DATE;IN_DATE;OUT_DATE;ID;CLIENT_CODE_NUMBER;DELIVERY_COMPANY;TRACKING_NUMBER_#NewLine13#			</cfoutput>
</cfif>
<cfoutput query="qGetGrid">#REReplace('#NOM_RACINE#;#evaluate("#DATA_POOL#")##POOL_SEPARATOR##TYPE_COMMANDE#;#LIBELLE_PROFIL#;#LIBELLE_ETAT#;#NOMBRE_DEJOUR_SLA#;#HEURE_LIMITE_SLA#;#LIBELLE#;#NUMERO_OPERATION#;#NUMERO_ORDRE#;#NUMERO_MARCHE#;#REF_CLIENT_2#;#LSNumberFormat(val(TRIM(PRIX)), "-_________.___")#;#LSNumberFormat(val(TRIM(MONTANT_TOTAL)), "-_________.___")#;#OPERATEUR#;#DISTRIBUTEUR#;#AGENCE#;#COMPTE_FACTURATION#;#SOUS_COMPTE#;#CODE_LISTE#;#DATE_DE_PREPARATION#;#DATE_DEMANDE_VALID_INT#;#DATE_VALID_INT#;#DATE_DMDE_VALID_INTERNE2#;#DATE_VALID_INTERNE2#;#DATE_DMDE_VALID_INTERNE3#;#DATE_VALID_INTERNE3#;#DATE_DMDE_VALID_INTERNE4#;#DATE_VALID_INTERNE4#;#DATE_DMDE_VALID_INTERNE5#;#DATE_VALID_INTERNE5#;#DATE_DE_COMMANDE#;#DATE_DEM_VALID_DIST#;#DATE_VALID_ENV_CMD#;#DATE_ACC_REC#;#DATE_EXP_CMD#;#DATE_ACC_REC_LIV#;#DT_DEM_ACTIV#;#DATE_SUPP#;#DATE_CLOTURE#;#CONTACT_PREPARATION#;#CONTACT_DEMANDE_VALID_INT#;#CONTACT_VALID_INT#;#CONT_DMDE_VALID_INTERNE2#;#CONTACTE_VALID_INTERNE2#;#CONT_DMDE_VALID_INTERNE3#;#CONTACTE_VALID_INTERNE3#;#CONT_DMDE_VALID_INTERNE4#;#CONTACTE_VALID_INTERNE4#;#CONT_DMDE_VALID_INTERNE5#;#CONTACTE_VALID_INTERNE5#;#CONTACT_VALID_ENV_CMD#;#CONTACT_ACC_REC#;#CONTACT_EXP_CMD#;#CONTACT_ACC_REC_LIV#;#CONTACT_DEM_ACTIV#;#CONTACT_SUPP#;#CONTACT_CLOTURE#;#A_L_ATTENTION#;#NOM_SITE#;#REFERENCE_SITE_LIV#;#CODE_INTERNE_SITE_LIV#;#replace(TRIM(ADRESSE_SITE_LIV),chr(13),'  ','all')#;#CODE_POSTAL_SITE_LIV#;#VILLE_SITE_LIVRAISON#;#SITE_FACT#;#REFERENCE_SITE_FACT#;#CODE_INTERNE_SITE_FACT#;#ADRESSE_SITE_FACT#;#CODE_POSTAL_SITE_FACT#;#VILLE_SITE_FACT#;#REFERENCE_EQ#;#TYPE_EQUIPEMENT#;#IMEI#;#MODELE#;#NIVEAU#;#PUK1#;#LIBELE_COLLABORATEUR#;#MATRICULE#;#CIVILITE#;#PRENOM#;#NOM#;#STATUS_EMPLOYE#;#BOOL_PUBLICATION#;#CODE_INTERNE_COLLABORATEUR#;#EMAIL#;#COMMENTAIRE_COMMANDE#;#COMMENTAIRE_EMPLOYE#;#COMMENTAIRE_SITE_FACT#;#COMMENTAIRE_SITE_LIVRAISON#;#NUM_SIM#;#IDSOUS_TETE#;#SOUS_TETE#;#CODE_RIO#;#DATE_PORTAGE_SOUHAITE#;#TYPE_ABONNEMENT#;#LIBELLE_ABONNEMENT#;#CONTACT#;#DATE_EFFET_PREVUE#;#DATE_ENTREE#;#DATE_SORTIE#;#IDENTIFIANT#;#NO_CDE_CLIENT#;#Replace(TRANSPORTEUR,newLine,"","all")#;#NUMERO_TRACKING#;',"[#NewLine10##newLine13#]","", "all")##NewLine13#</cfoutput>
</cfprocessingdirective>
</cfsavecontent>
<cfheader name="Content-Disposition" value="attachement;filename=report.csv" >
<cfcontent type="text/csv" >
<cfoutput>#report#</cfoutput>
</cffunction>



 
 
 





