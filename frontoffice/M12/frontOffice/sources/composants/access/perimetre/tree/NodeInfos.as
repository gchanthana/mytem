package composants.access.perimetre.tree {
	[Bindable]
	public class NodeInfos implements INodeInfos {
		private var nodeInfos:Object;
		
		public function get NID():int {
			return nodeInfos.NID;
		}
		
		public function get LBL():String {
			return nodeInfos.LBL;
		}
		
		public function get STC():int {
			return nodeInfos.STC;
		}
		
		public function NodeInfos(nodeInfosData:Object) {
			nodeInfos = nodeInfosData;
		}
		
		public function get TYPE_PERIMETRE():String {
			return nodeInfos.TYPE_PERIMETRE;
		}

		public function get TYPE_LOGIQUE():String {
			return nodeInfos.TYPE_LOGIQUE;
		}
		
		public function get MODULE_FIXE_DATA():String{
			return nodeInfos.MODULE_FIXE_DATA;
		}
		
		public function get MODULE_GESTION_LOGIN():String{
			return nodeInfos.MODULE_GESTION_LOGIN;
		}
		
		public function get MODULE_MOBILE():String{
			return nodeInfos.MODULE_MOBILE;
		}
		
		public function get DROIT_GESTION_FOURNIS():String{
			return nodeInfos.DROIT_GESTION_FOURNIS;
		}
		
		public function get INFOS_STATUS():Number{
			return nodeInfos.INFOS_STATUS;
		}
		
		public function get CODE_STYLE():String{
			return nodeInfos.CODE_STYLE;
		}
		
		public function get MODULE_FACTURATION():String{
			return nodeInfos.MODULE_FACTURATION;
		}
		
		public function get MODULE_GESTION_ORG():String{
			return nodeInfos.MODULE_GESTION_ORG;
		}
		
		public function get MODULE_WORKFLOW():String{
			return nodeInfos.MODULE_WORKFLOW;
		}
		
		public function get MODULE_USAGE():String{
			return nodeInfos.MODULE_USAGE;
		}
		
		public function get GEST():Number{
			return nodeInfos.GEST;
		}
		
		public function get STRUCT():Number{
			return nodeInfos.STRUCT;
		} 
		
		public function get FACT():String{
			return nodeInfos.FACT;
		}
		
		public function get OPERATEURID():String{
			return nodeInfos.OPERATEURID;
		}
		

		public function getUniversAccess(universKey:String):int {
			//return nodeInfos[universKey];
			return 0;
		}
	}
}