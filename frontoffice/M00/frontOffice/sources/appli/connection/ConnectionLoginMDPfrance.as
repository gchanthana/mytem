package appli.connection
{
	import appli.events.LoginEvent;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import entity.AppParams;
	import entity.CodeLangue;
	import entity.ObjectError;
	
	import general.abstract.AbstractConnection;
	
	import interfaceClass.IConnection;
	
	import mx.core.Container;
	
	public class ConnectionLoginMDPfrance extends AbstractConnection implements IConnection
	{
		public function ConnectionLoginMDPfrance(progress:ConsoProgressBar)
		{
			super(progress);
		}
	// PUBLIC
		override public function goLogin(param:AppParams, container:Container = null):void
		{
			trace("ConnectionCVclassique.login()"); 
			
			super.goLogin(param,container);
			
			loadModule("login");
		}
		override public function validateLogin(objLogin:Object):void
		{
			super.validateLogin(objLogin);
			loginService.login(params.login,params.mdp,params.codeApp,params.codeConnection,params.codeLangue,params.seSouvenir);
		}
	}
}	