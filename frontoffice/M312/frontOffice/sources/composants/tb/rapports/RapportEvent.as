package composants.tb.rapports
{
	import flash.events.Event;

	public class RapportEvent extends Event
	{
		private var FORMAT : String;
		
		public function RapportEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		
		public function set format(f : String):void{
			FORMAT = f;
		}
		
		public function get format():String{
			return FORMAT;
		}
		
		
		
	}
}