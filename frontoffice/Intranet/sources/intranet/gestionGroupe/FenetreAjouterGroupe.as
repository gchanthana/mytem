package intranet.gestionGroupe
{
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	public class FenetreAjouterGroupe extends FenetreAjouterGroupeIHM
	{
  		private var objGridGroupeRacine : GridGroupeRacine ;
  		private var modif : Boolean;
  		private var libelle : String;
  		private var commentaire : String;
  		private var reselectionneGroupeRacine : Boolean=false;
  		private var idMaster : String;
		public var enableLib : Boolean = true;
        
       
        public function FenetreAjouterGroupe(objGridGroupeRacine : GridGroupeRacine,modif : Boolean = false,libelle : String="",commentaire : String = "",idMaster : String = "")
        {
        	this.objGridGroupeRacine = objGridGroupeRacine;
        	super();
         	this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
         	this.modif = modif;
         	reselectionneGroupeRacine = modif;
         	this.libelle=libelle;
         	this.commentaire=commentaire;
         	this.idMaster=idMaster;
        }
        public function initIHM(event:Event) : void
        {
        	btCancel.addEventListener(MouseEvent.CLICK,removeMe);    
            btValide.addEventListener(MouseEvent.CLICK, submitData);
          	inputLibelleGroupe.text = libelle;
			inputLibelleGroupe.enabled = enableLib;
           	inputCommentaire.text  = commentaire; 
           	initMaster();
        }
        private function submitData(event:MouseEvent):void {
        	
        	if(inputLibelleGroupe.text == "")
			{
				Alert.show("Le libellé de société ne doit pas être vide")
			}
			else
			{	
				//Alert.show("-->"+comboMaster.selectedItem.IDRACINE_MASTER);
				if (modif==false)
				{
	           		objGridGroupeRacine.addGroupeMaitre(StringUtil.trim(inputLibelleGroupe.text),StringUtil.trim(inputCommentaire.text),false,comboMaster.selectedItem.IDRACINE_MASTER);
	           		
	   			}
	   			else
	   			{
	   				objGridGroupeRacine.addGroupeMaitre(StringUtil.trim(inputLibelleGroupe.text),StringUtil.trim(inputCommentaire.text),true,comboMaster.selectedItem.IDRACINE_MASTER);
	   				reselectionneGroupeRacine = true;
	   			}
	   			PopUpManager.removePopUp(this);
	   
   			}
        }
        private function initMaster():void
        {
        	var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.intranet.procedureGestionGroupeGroupeRacine",
																	"getGroupeMaster",
																	processInitGroupes,error);
				RemoteObjectUtil.callService(op);
        }
        private function error(evt : FaultEvent):void
        {
        	Alert.show("Erreur "+evt.toString());
        }
        private function processInitGroupes(event:ResultEvent):void {
			//myFilterArrayGroupe=event.result as ArrayCollection;
			comboMaster.dataProvider=event.result as ArrayCollection;
			if (reselectionneGroupeRacine == true){
				reselectionneGroupe();
				reselectionneGroupeRacine=false;
			}
		}
		private function reselectionneGroupe():void{
			
			var index : int = 0;
		
			for (var i : int =0; i < ((comboMaster.dataProvider) as ArrayCollection ).length;i++)
			{
				trace("---->"+ ((comboMaster.dataProvider) as ArrayCollection ).getItemAt(i).IDRACINE_MASTER+" =="+ idMaster);
				if (((comboMaster.dataProvider) as ArrayCollection ).getItemAt(i).IDRACINE_MASTER == idMaster)
				{
					index = i;
					//Alert.show("trouvé");
				}
				
			}
			//index=ConsoviewUtil.getIndexById((comboMaster.dataProvider) as ArrayCollection,"IDRACINE_MASTER",idMaster as Number);
			comboMaster.selectedIndex=(index);
			//callLater(scrollToModif);
			
	}
		

        // Cancel button click event listener.
        private function removeMe(event:MouseEvent):void {
            PopUpManager.removePopUp(this);
        }
        	
	}
}