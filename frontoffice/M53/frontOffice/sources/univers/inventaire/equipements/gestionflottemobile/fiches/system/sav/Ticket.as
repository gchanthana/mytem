package univers.inventaire.equipements.gestionflottemobile.fiches.system.sav
{
	import composants.util.DateFunction;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class Ticket extends EventDispatcher
	{
			public var IDSAV: Number = 0;
		    public var IDEQUIPEMENT: Number = 0;
		    public var IDCONTRAT: Number = 0;
		    public var DATE_DEB: Date;
		    public var DATE_FIN: Date;
		    public var DESCRIPTION: String ="";
		    public var GARANTIE_APPLICABLE: Number = 0;
		    public var IDFOURNISSEUR: Number = 0;
		    public var IDSITE: Number = 0;
		    public var IDUSER: Number = 0;
		    public var COMMENTAIRES: String = "";
		    public var NUMERO_SAV: String = "";
		    public var LIBELLE_EQUIP: String = "";
		    public var IDEMPLOYE: Number = 0;
		    public var OUVERT:Number = 1;
		    
		    public var INTERVENTIONS : ArrayCollection = new ArrayCollection();
		    
		   // private var DATE_DEB_STRING: String =""; // pour afficher une date dans un dg
		    
		    public function Ticket(idFournisseur:Number=0)
		    {
		    	if(idFournisseur)
		    	{
		    		getNewSavUniqueNumero(idFournisseur);
		    	}
		    }
		    
		    public static const SAVUNIQUENUMERO:String = "getNewSavUniqueNumero";
		    public function getNewSavUniqueNumero(idFournisseur : Number):void
			{
			
				var op :AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoview.inventaire.equipement.savEquipement",
						"getTicketNumber",getTicketNumberHandler);
						
				RemoteObjectUtil.callService(op,idFournisseur);
			
		}
		private function getTicketNumberHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				this.NUMERO_SAV = event.result as String;
				dispatchEvent(new Event(SAVUNIQUENUMERO));
			}
			else
			{
				Alert.show("Erreur lors de la récupération des sites","Erreur");									
			}
		}
		public function get DATE_DEB_STRING():String
		{
			return DateFunction.formatDateAsShortString(DATE_DEB);
		}
						
						

	}
}