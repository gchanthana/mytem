package commandemobile.itemRenderer
{
	import commandemobile.entity.Commande;
	import commandemobile.ihm.QuantityChoice_2IHM;
	import commandemobile.popup.PopUpCollaborateurIHM;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class CollaborateurItemRendererImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------

		//IHM
		public var ihm:QuantityChoice_2IHM;

		//IMAGES
		[Embed(source="/assets/images/collaborateur.png",mimeType='image/png')]
		public var imgTrue :Class;
		[Embed(source="/assets/images/collaborateurEnabled.png",mimeType='image/png')]
		public var imgFalse:Class;
		
		//COMPOSANTS
		public var rdbtnLibelle					:RadioButton;
		public var rdbtnCollaborateur			:RadioButton;
		public var txtLibelle					:TextInput;
		public var lblCollaborateurName			:Label;
		public var img							:Image;
		
		//IHM
		private var popUpCollaborateurs		:PopUpCollaborateurIHM;
		
		//CONSTANTES
		private const widthMax		:int = 160;
		private const widthMin		:int = 0;

//VARIABLES------------------------------------------------------------------------------

//METHODES PUBLICS-----------------------------------------------------------------------
		
		//CONSTRUCTEUR
		public function CollaborateurItemRendererImpl()
		{
			super.addEventListener("pompom",tempFunction);
			
		}

//METHODES PUBLICS-----------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		override public function set data(value:Object):void
		{
			txtLibelle.text 					= "";
			lblCollaborateurName.text 			= "";
			txtLibelle.errorString 				= "";
			lblCollaborateurName.errorString	= "";
			
			if(value != null)
			{
				super.data = value;
				
				if(data.COLLABORATEUR != "Non affecté" || data.LIBELLE_CONFIGURATION != "")
				{	
					if(data.LIBELLE_CONFIGURATION != "Non affecté")
					{
						this.txtLibelle.text 			= data.LIBELLE_CONFIGURATION;
				 		this.txtLibelle.errorString 	= "";
					}
					else
					{
						this.txtLibelle.text 			= "";
					 	this.txtLibelle.errorString		="Veuillez saisir un libellé";
					}
				 	this.lblCollaborateurName.text 	= data.COLLABORATEUR;
				 	this.lblCollaborateurName.errorString = "";
				 	if(data.LIBELLESELECTED)
				 	{
				 		refreshComposantsWithResultRdbtn(true,false,widthMax,widthMin,imgFalse);
				 	}
				 	else
				 	{
				 		refreshComposantsWithResultRdbtn(false,true,widthMin,widthMax,imgTrue);
				 	}
				}
				else
				{
					creationCompleteHandler();
					rdbtnLibelleClickHandler();
				}
				imgCreationCompleteHandler();	
			}
		}
		
		private function tempFunction(e:Event):void
		{
			var b:Boolean = true;
		}
		
		protected function creationCompleteHandler():void
		{
			txtLibelle.text = "";
			txtLibelle.errorString="Veuillez saisir un libellé";
			lblCollaborateurName.errorString="Veuillez choisir un collaborateur";
			rdbtnLibelle.selected = true;
		}
		
		//AFFICHE L'IMAGE 'CollaborateurDesactivé' LORS DE L'INITIALISATION
		protected function imgCreationCompleteHandler():void
		{
			img.visible = true;
			if(data.LIBELLESELECTED)
			{
				img.source 	= imgFalse;
			}
			else
			{
				img.source 	= imgTrue;
			}
		}
		
		//RAFRAICHI L'ETAT DES COMPOSANTS POUR CACHER LA SELECTION DU COLLABORATEUR ET AFFICHER LE LIBELLE
		protected function rdbtnLibelleClickHandler():void
		{
			if(rdbtnLibelle.selected)
			{
				refreshComposantsWithResultRdbtn(true,false,widthMax,widthMin,imgFalse);
				if(txtLibelle.text != "Non affecté")
				{
					data.LIBELLE_CONFIGURATION = txtLibelle.text;
				}
				data.LIBELLESELECTED = true;
			}
		}
		
		//RAFRAICHI L'ETAT DES COMPOSANTS POUR AFFICHER LA SELECTION DU COLLABORATEUR ET CACHER LE LIBELLE
		protected function rdbtnCollaborateurClickHandler():void
		{
			if(rdbtnCollaborateur.selected)
			{
				refreshComposantsWithResultRdbtn(false,true,widthMin,widthMax,imgTrue);
				data.LIBELLE_CONFIGURATION = lblCollaborateurName.text;
				data.LIBELLESELECTED = false;
			}
		}
		
		//APPEL LA FONCTION PERMETTANT D'OUVRIR LA POPUP DE SELECTION DE COLLABORATEUR SI 'Collaborateur' EST SELECTIONNE
		protected function imgClickHandler():void
		{
			if(rdbtnCollaborateur.selected)
			{
				popUpCollaborateurOpenHandler();
			}
		}
		
		protected function txtLibelleCreationCompleteHandler():void
		{
			txtLibelle.text = "";
			txtLibelle.errorString="Veuillez saisir un libellé";
			rdbtnLibelle.selected = true;
		}
		
		protected function lblCollaborateurNameCreationCompleteHandler():void
		{
			lblCollaborateurName.text = data.COLLABORATEUR;
			lblCollaborateurName.errorString="Veuillez choisir un collaborateur";
		}
		
		protected function rdbtnLibelleCreationComplete():void
		{
			rdbtnLibelle.selected = true;
		}
		
		

//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------
		
		//REND VISIBLE OU CACHE DES COMPOSANTS SUIVANT L'ETAT DES RADIOBUTTON
		private function refreshComposantsWithResultRdbtn(txtVisible:Boolean,lblVisible:Boolean,txtWidth:int,lblWidth:int,imgVisible:Class):void
		{
			txtLibelle.enabled 				= txtVisible;
			txtLibelle.visible 				= txtVisible;
			txtLibelle.width				= txtWidth;
			lblCollaborateurName.enabled 	= lblVisible;
			lblCollaborateurName.visible 	= lblVisible;
			lblCollaborateurName.width 		= lblWidth;
			img.source 						= imgVisible;
		}
		
		//OUVRE UNE POPUP POUR POUVOIR SELECTIONNER LE COLLABORATEUR AUQUEL ON VEUT ASSOCIER LA CONFIGURATION
		private function popUpCollaborateurOpenHandler():void
		{
			popUpCollaborateurs = new PopUpCollaborateurIHM();
			PopUpManager.addPopUp(popUpCollaborateurs,this.parentApplication as DisplayObject,true);
			PopUpManager.centerPopUp(popUpCollaborateurs);
			
			popUpCollaborateurs.IDPOOL_GESTIONNAIRE = data.IDPOOL_GESTIONNAIRE;
			popUpCollaborateurs._itemsSelected = data;

			popUpCollaborateurs.dispatchEvent(new Event("listerCollaborateurs"));
			popUpCollaborateurs.addEventListener("POPUP_COLLABORATEURS_CLOSED_AND_VALIDATE",closedPopUpCollaborateurs);
		}

		protected function txtInputHandler():void
		{
			data.LIBELLE_CONFIGURATION 	= txtLibelle.text;
			txtLibelle.errorString = "";
		}
		
		//ATTRIBUT LES CARACTERISTIQUES DU COLLABORATEUR SELECTIONNE A LA CONFIGURATION SELECTIONNE DANS LE TABLEAU
		private function closedPopUpCollaborateurs(e:Event):void
		{
			data.COLLABORATEUR 			= popUpCollaborateurs._itemsSelected.COLLABORATEUR;
			data.EMAIL 					= popUpCollaborateurs._itemsSelected.EMAIL;
			data.NOM 					= popUpCollaborateurs._itemsSelected.NOM;
			data.IDCOLLABORATEUR		= popUpCollaborateurs._itemsSelected.IDCOLLABORATEUR;
			data.IDEMPLOYE 				= popUpCollaborateurs._itemsSelected.IDEMPLOYE;
			data.IDGROUPE_CLIENT 		= popUpCollaborateurs._itemsSelected.IDGROUPE_CLIENT;
			data.CLE_IDENTIFIANT 		= popUpCollaborateurs._itemsSelected.CLE_IDENTIFIANT;
			data.MATRICULE				= popUpCollaborateurs._itemsSelected.MATRICULE;
			data.INOUT					= popUpCollaborateurs._itemsSelected.INOUT;
			data.LIBELLE_CONFIGURATION	= popUpCollaborateurs._itemsSelected.NOM;
			if(data.COLLABORATEUR != "Non affecté")
			{
				lblCollaborateurName.text = data.COLLABORATEUR;
				lblCollaborateurName.errorString = "";
			}
			dispatchEvent(new Event("refreshGrid",true));
		}
		


//METHODES PRIVATE-----------------------------------------------------------------------

	}
}