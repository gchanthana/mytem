package gestionparc.ihm.fiches.changementcomptefacturation
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class ChangementCompteFacturationServices extends EventDispatcher
	{

		public static const LISTE_COMPTES_SOUSCOMPTES_LOADED:String="listeComptesSouscomptesLoaded";
		public static const LISTE_DESTINATAIRE_LOADED:String="listeDestinataireLoaded";
		public static const LISTE_COMPTES_SOUSCOMPTES_LISTES_SELECTED_LOADED:String="listeComptesSouscomptesListesSelectedLoaded";
		public static const END_OF_OPERATION:String="endOfOperation";

		private var _listeComptesAndSousCompte:ArrayCollection;
		private var _listeDestinataire:ArrayCollection;

		private var _myChangementCompteFacturation:ChangementCompteFacturationImpl;

		public function ChangementCompteFacturationServices(instanceChangementCompteFacturation:ChangementCompteFacturationImpl)
		{
			myChangementCompteFacturation=instanceChangementCompteFacturation;
		}

		public function getCompteOpeCompte():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.CompteFacturationOperateurServices", "getCompteOpeCompte", getCompteOpeCompteResultHandler);
			RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX, myChangementCompteFacturation.idOperateur);
		}


		/**
		 *Fournit la liste des compte de facturation paramétrés pour un gestionnaire
		 * remote Object fr.consotel.consoview.M11.CompteFacturationOperateurServices
		 * remote methode getListeComptesOperateurByLoginAndPool
		 * remote params idOperateur, idPool
		 */
		public function getListeComptesOperateurByLoginAndPool():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.CompteFacturationOperateurServices", "getListeComptesOperateurByLoginAndPool", getListeComptesOperateurByLoginAndPoolResultHandler);

			RemoteObjectUtil.callService(op, myChangementCompteFacturation.idOperateur, myChangementCompteFacturation.idPool);
		}

		public function getListeDestinataires():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.CompteFacturationOperateurServices", "getListeDestinataires", getListeDestinatairesResultHandler);
			RemoteObjectUtil.callService(op, myChangementCompteFacturation.idRevendeur);
		}

		public function sendChangementCompteFacture(destinataire:String, pieceJointe:String, copie:Boolean, xmlHistoric:String, destinataireCopie:String="", destinaire2:String=""):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.CompteFacturationOperateurServices", "sendChangementCompteFacture", sendChangementCompteFactureResultHandler);

			RemoteObjectUtil.callService(op, myChangementCompteFacturation.taTextMail.htmlText, destinataire, pieceJointe, copie, xmlHistoric, destinaire2, destinataireCopie);
		}

		public function getListeCompte(listeIdSousTete:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.CompteFacturationOperateurServices", "getCompteLstLignes", getListeCompteResultHandler);

			RemoteObjectUtil.callService(op, listeIdSousTete);
		}

		private function getCompteOpeCompteResultHandler(re:ResultEvent):void
		{
			listeComptesAndSousCompte=new ArrayCollection();

			var objComtpe:Object;
			var objSousComtpe:Object;

			if (re.result)
			{
				for each (var myObj:* in re.result)
				{
					if (Number(myObj.SELECTED) > 0)
					{
						var len:int=listeComptesAndSousCompte.length;
						var j:int=0;

						for (; j < len; j++)
						{
							if (listeComptesAndSousCompte[j].IDCOMPTE == myObj.IDCOMPTE_FACTURATION)
							{
								break;
							}
						}

						if (j >= len) // si le compte n'existe pas
						{
							/** creation d'un objet compte */
							objComtpe=new Object();
							objComtpe.COMPTE=myObj.COMPTE_FACTURATION;
							objComtpe.LABEL=myObj.LABEL2;
							objComtpe.IDCOMPTE=myObj.IDCOMPTE_FACTURATION;
							objComtpe.TAB_SOUS_COMPTE=new ArrayCollection();

							/** creation d'un objet sous compte */
							objSousComtpe=new Object();
							objSousComtpe.SOUS_COMPTE=myObj.SOUS_COMPTE;
							objSousComtpe.IDSOUS_COMPTE=myObj.IDSOUS_COMPTE;

							objComtpe.TAB_SOUS_COMPTE.addItem(objSousComtpe); // ajout de l'objet sous compte au tableau sous compte

							if (!listeComptesAndSousCompte.contains(objComtpe.COMPTE))
								listeComptesAndSousCompte.addItem(objComtpe); // ajout de l'objet compte au tableau des comptes
						}
						else //  le compte existe
						{
							var longueur:int=(listeComptesAndSousCompte[j].TAB_SOUS_COMPTE as ArrayCollection).length;
							var i:int=0;
							for (; i < longueur; i++)
							{
								if (listeComptesAndSousCompte[j].TAB_SOUS_COMPTE[i].IDSOUS_COMPTE == myObj.IDSOUS_COMPTE)
									break;
							}

							if (i >= longueur) // le sous compte n'existe pas
							{
								// creation d'un objet sous compte
								objSousComtpe=new Object();
								objSousComtpe.SOUS_COMPTE=myObj.SOUS_COMPTE;
								objSousComtpe.IDSOUS_COMPTE=myObj.IDSOUS_COMPTE;

								(listeComptesAndSousCompte[j].TAB_SOUS_COMPTE as ArrayCollection).addItem(objSousComtpe); // ajout de l'objet sous compte au tableau sous compte
							}
						}
					}
				}
				dispatchEvent(new Event(LISTE_COMPTES_SOUSCOMPTES_LOADED));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Recup_ration_des_comptes_et_des_sous_com'), ResourceManager.getInstance().getString('M111', 'Consoview'));
			}
		}

		private function getListeComptesOperateurByLoginAndPoolResultHandler(re:ResultEvent):void
		{

			listeComptesAndSousCompte=new ArrayCollection();

			var objComtpe:Object;
			var objSousComtpe:Object;

			if (re.result)
			{
				for each (var myObj:* in re.result)
				{

					var len:int=listeComptesAndSousCompte.length;
					var j:int=0;

					for (; j < len; j++)
					{
						if (listeComptesAndSousCompte[j].IDCOMPTE == myObj.IDCOMPTE_FACTURATION)
						{
							break;
						}
					}

					if (j >= len) // si le compte n'existe pas
					{
						/** creation d'un objet compte */
						objComtpe=new Object();
						objComtpe.COMPTE=myObj.COMPTE_FACTURATION;
						objComtpe.LABEL=myObj.LABEL2;
						objComtpe.IDCOMPTE=myObj.IDCOMPTE_FACTURATION;
						objComtpe.TAB_SOUS_COMPTE=new ArrayCollection();

						/** creation d'un objet sous compte */
						objSousComtpe=new Object();
						objSousComtpe.SOUS_COMPTE=myObj.SOUS_COMPTE;
						objSousComtpe.IDSOUS_COMPTE=myObj.IDSOUS_COMPTE;

						objComtpe.TAB_SOUS_COMPTE.addItem(objSousComtpe); /** ajout de l'objet sous compte au tableau sous compte */
						listeComptesAndSousCompte.addItem(objComtpe); /** ajout de l'objet compte au tableau des comptes */
					}
					else //  le compte existe
					{
						var longueur:int=(listeComptesAndSousCompte[j].TAB_SOUS_COMPTE as ArrayCollection).length;
						var i:int=0;
						for (; i < longueur; i++)
						{
							if (listeComptesAndSousCompte[j].TAB_SOUS_COMPTE[i].IDSOUS_COMPTE == myObj.IDSOUS_COMPTE)
							{
								break;
							}
						}

						if (i >= longueur) // le sous compte n'existe pas
						{
							/** creation d'un objet sous compte */
							objSousComtpe=new Object();
							objSousComtpe.SOUS_COMPTE=myObj.SOUS_COMPTE;
							objSousComtpe.IDSOUS_COMPTE=myObj.IDSOUS_COMPTE;

							(listeComptesAndSousCompte[j].TAB_SOUS_COMPTE as ArrayCollection).addItem(objSousComtpe); // ajout de l'objet sous compte au tableau sous compte
						}
					}
				}
				dispatchEvent(new Event(LISTE_COMPTES_SOUSCOMPTES_LOADED));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Recup_ration_des_comptes_et_des_sous_com'), ResourceManager.getInstance().getString('M111', 'Consoview'));
			}

		}

		private function getListeDestinatairesResultHandler(re:ResultEvent):void
		{
			listeDestinataire=new ArrayCollection();
			if (re.result)
			{
				for each (var obj:* in re.result)
				{
					/** traitement des comptes facturations */
					if (!listeDestinataire.contains(obj))
					{
						listeDestinataire.addItem(obj)
					}
				}
				dispatchEvent(new Event(LISTE_DESTINATAIRE_LOADED));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Recup_ration_des_destinataires_impossibl'), ResourceManager.getInstance().getString('M111', 'Consoview'));
			}
		}

		private function getListeCompteResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				for each (var tmp:* in(re.result as ArrayCollection))
				{
					for each (var tmp2:* in myChangementCompteFacturation.listeElementsSelectiones)
					{
						if (tmp2.IDSOUS_TETE == tmp.IDSOUS_TETE)
						{
							tmp2.COMPTE_FACTURATION=tmp.COMPTE_FACTURATION;
							tmp2.SOUS_COMPTE=tmp.SOUS_COMPTE;
							break;
						}
					}
				}
				dispatchEvent(new Event(LISTE_COMPTES_SOUSCOMPTES_LISTES_SELECTED_LOADED));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Recup_ration_des_comptes_et_sous_comptes'), ResourceManager.getInstance().getString('M111', 'Consoview'));
			}
		}

		private function sendChangementCompteFactureResultHandler(re:ResultEvent):void
		{
			if (re.result == 1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'La_demande_de_changement_de_compte_factu'), Application.application as DisplayObject)
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'La_demande_de_changement_de_compte_factu'), ResourceManager.getInstance().getString('M111', 'Consoview'));
			}
			dispatchEvent(new Event(END_OF_OPERATION));
		}

		public function set listeComptesAndSousCompte(value:ArrayCollection):void
		{
			_listeComptesAndSousCompte=value;
		}

		public function get listeComptesAndSousCompte():ArrayCollection
		{
			return _listeComptesAndSousCompte;
		}

		public function set listeDestinataire(value:ArrayCollection):void
		{
			_listeDestinataire=value;
		}

		public function get listeDestinataire():ArrayCollection
		{
			return _listeDestinataire;
		}

		public function set myChangementCompteFacturation(value:ChangementCompteFacturationImpl):void
		{
			_myChangementCompteFacturation=value;
		}

		public function get myChangementCompteFacturation():ChangementCompteFacturationImpl
		{
			return _myChangementCompteFacturation;
		}
	}
}
