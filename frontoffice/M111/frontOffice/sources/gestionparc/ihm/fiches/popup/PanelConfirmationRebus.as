package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.collaborateur.CollaborateurServices;
	
	import mx.containers.VBox;
	import mx.controls.DateField;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	
	
	public class PanelConfirmationRebus extends PanelConfirmationRebusIHM
	{
		public static const VALIDER_EVENT:String="validerEvent";
		private var colonneGauche : VBox;
		private var colonneDroite : VBox;
		private var dateRebus : Date;
		private var xml : String;
		private var ligne : AbstractMatriceParcVO;
		
		public var serv:CollaborateurServices = new CollaborateurServices();;
		
		public function PanelConfirmationRebus(colonneGauche : VBox,colonneDroite : VBox,dateRebus : Date,xml : String, ligne : AbstractMatriceParcVO, boolVide:Boolean = false)
		{
			this.dateRebus=dateRebus;
			this.xml=xml;
			this.ligne = ligne;
			this.colonneDroite = colonneDroite;
			this.colonneGauche = colonneGauche;
			
			if(boolVide)
				valider(null);
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{	
			panel_gauche.addChild(colonneGauche);
			panel_droite.addChild(colonneDroite);
			addEventListener(CloseEvent.CLOSE,fermer);
			serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED,rebus_handler);
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
		}
		private function fermer(evt : Event):void 
		{
			PopUpManager.removePopUp(this);
		}
		private function valider(evt : MouseEvent):void
		{
			serv.validerDepartEntreprise(setObj());
		}
		private function rebus_handler (evt : gestionparcEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		private function setObj():Object
		{
			var obj:Object = new Object();
			obj.idorigine = SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1 = SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ITEM2 = "";
			obj.XML = xml;
			obj.IDPOOL = SpecificationVO.getInstance().idPool;
			obj.ID_TERMINAL = 0;
			obj.ID_SIM = 0;
			obj.ID_EMPLOYE = SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE = 0;
			obj.DATE_EFFET = DateField.dateToString(dateRebus,'YYYY/MM/DD');
			obj.IS_PRETER = 0;
			return obj;
		}
	}
}