package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionProfilEvent;
	import univers.parametres.parametrecompte.service.GestionPoolService;
	import univers.parametres.parametrecompte.service.ProfilService;
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrecompte.vo.Profil;
	
	[Bindable]
	public class ListeUtilisateurProfil extends TitleWindow
	{
		public var profilService : ProfilService = new ProfilService();
		public var poolService : GestionPoolService = new GestionPoolService();
		private var groupeIndex : int;
		public var profil : Profil;	
		//COMPONANT
		public var dgUser : DataGrid;
		
		public function ListeUtilisateurProfil()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			profilService.listeUserOfProfil(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,profil.idProfil);
		}
				
		
		public function supprimer(data : Gestionnaire):void
		{
			ConsoviewAlert.afficherAlertConfirmation("Êtes vous sûr de vouloir supprimer le profil "+profil.libelle+" de "+data.nom_prenom+" sur le pool "+data.libellePool+" ?","Confirmation",supprimerBtnValiderCloseEvent);
		}

		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				poolService.addEventListener(GestionPoolEvent.UPDATE_PROFIL_ON_POOL,remove_complete_handler);
				poolService.updatePoolOfLogin(groupeIndex,(dgUser.selectedItem as Gestionnaire).id,[(dgUser.selectedItem as Gestionnaire).idPool],1); // id profil = -1 par default --> affecte le profil par défault
			}
		}
		private function remove_complete_handler(evt : GestionPoolEvent=null):void
		{
			ConsoviewAlert.afficherOKImage("le profil a bien été désaffecté");
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_PROFIL_OFUSER_COMPLETE));
			PopUpManager.removePopUp(this);
		}
	
		public function fermer(evt :Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
	}
}