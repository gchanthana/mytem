package univers.inventaire.equipements.lignes.fiche.mob.menu {
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import mx.collections.ArrayCollection;
	
	import composants.util.ConsoviewFormatter;
	import flash.events.MouseEvent;
	
	public class Infos extends InfosIHM {
		private var sousteteObject:Object;
		
		public function Infos(sousteteObject:Object) {
			this.sousteteObject = sousteteObject
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:Event):void {
			infosUpdateButton.addEventListener(MouseEvent.CLICK, updateBtClicked);
			
			codeInterneText.addEventListener(Event.CHANGE, dataChanged);
			usageText.addEventListener(Event.CHANGE, dataChanged);
			backupFields.addEventListener(Event.CHANGE, dataChanged);
			commentText.addEventListener(Event.CHANGE, dataChanged);
			prestataireText.addEventListener(Event.CHANGE, dataChanged);
			
			codeInterneText.addEventListener(FlexEvent.ENTER, updateBtClicked);
			usageText.addEventListener(FlexEvent.ENTER, updateBtClicked);
			backupFields.addEventListener(FlexEvent.ENTER, updateBtClicked);
			commentText.addEventListener(FlexEvent.ENTER, updateBtClicked);
			prestataireText.addEventListener(FlexEvent.ENTER, updateBtClicked);
			
			usageFields.addEventListener(Event.CHANGE, cmbusageChanged);		
			sousteteTitle.text = ConsoviewFormatter.formatPhoneNumber(sousteteObject.SOUS_TETE);
			sousteteType.text = sousteteObject.LIBELLE_TYPE_LIGNE;
			getFicheData();
		}
		
		protected function dataChanged(event:Event):void
		{
			infosUpdateButton.enabled = true;
		}
		
		protected function cmbusageChanged(event:Event):void
		{
			usageText.text = usageFields.selectedItem.COMMENTAIRES;
			infosUpdateButton.enabled = true;
		}
		
		protected function updateBtClicked(event:Event):void
		{
			var idsoutete:String = sousteteObject.IDSOUS_TETE;
			var codeinterne:String = codeInterneText.text;
			var usage:String = usageText.text;
			var principal:Number = backupFields.selectedItem.data;
			var commentaire:String = commentText.text;
			var prestataire:String = prestataireText.text;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"updateInfoLine",
																					updateInfoLineResult, processError);
			RemoteObjectUtil.callService(op, idsoutete, codeinterne, usage, principal.toString(), commentaire, prestataire);
		}
		
		private function updateInfoLineResult(event:ResultEvent):void
		{
			infosUpdateButton.enabled = false;
		}
		
		private function getFicheData():void {
			var idsoutete:String = sousteteObject.IDSOUS_TETE;
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var dateDeb:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateDeb;
			var dateFin:Date = CvAccessManager.getSession().CURRENT_PERIMETRE.displayDateFin;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.equipement",
																					"getFicheData",
																					getFicheDataResult, processError);
			RemoteObjectUtil.callService(op, idsoutete, idGroupeMaitre, df.format(dateDeb), df.format(dateFin));
		}
		
		private function getFicheDataResult(event:ResultEvent):void
		{
			var res:Array = event.result as Array;
			fillLineInfo(res[0][0]);
			fillAllUse(res[1]);
			fillOpeTrafic(res[2]);
		}
		
		private function fillLineInfo(rData:Object):void
		{
			codeInterneText.text = rData.CODE_INTERNE;
			usageText.text = rData.USAGE_LIGNE;
			commentText.text = rData.COMMENTAIRES;	
			if (rData.BOOL_PRINCIPAL == "0")
				backupFields.selectedIndex = 1;
			prestataireText.text = rData.PRESTATAIRE;
		}
		
		private function fillAllUse(rData:ArrayCollection):void
		{
			usageFields.dataProvider = rData;
		}
		
		private function fillOpeTrafic(rData:ArrayCollection):void
		{
			operatorTrafficList.dataProvider = rData;
		}
		
		private function processError(error:FaultEvent):void
		{
			
		}
	}
}
