<cfcomponent output="false" extends="PerimetreE0">
	
	<cffunction name="executQueryByID" output="true" access="public" returntype="Query">
		<cfargument name="idTabBord" 				required="true"	type="Numeric" default="0">
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idsurtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="dateDebutProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="dateFinProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="perimetreParm" 			required="true" type="String"  default="0">		
			
			<cfset idtdb 	 = idTabBord>
			<cfset queryrslt = "NULL">
			<cfset query 	 = "NULL">
			<!--- 
			le mot clé var devant après cfset signifie que la variable est locale à la fonction.
			Donc elle disparait lorsque l'on sort de la fonction
			 --->
			<cfset idDebut 	= zgetIdPeriodeMois(dateDebutProvenantDuTDB)>	
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB = idDebut>
					
			<cfset idFin 	= zgetIdPeriodeMois(dateFinProvenantDuTDB)>
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN = idFin>
						
			<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga = Val(rsltIdOrga)>
		    </cfif>
		    <cfset SESSION.PERIMETRE.SELECTED_ID_ORGA = idorga>
		    
			<cfset idCliche = zGetCliche(idorga)>
			<cfset SESSION.PERIMETRE.SELECTED_ID_CLICHE = idCliche>
			
			<cfif idCliche EQ "">
				<cfabort showerror="Aucun cliché">
			</cfif>
			
			<cfif perimetreParm EQ "SousTete">
				<cfset niveau 	= 'IDSOUS_TETE'>
			<cfelse>
				<cfset niveau 	= zgetNiveauPerimetre(p_idperimetre)>
			</cfif>

			<cfset idniveau 	= getIdNiveau(niveau)>
			

			<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = niveau>
			
			<cfset rsltHandlerquery = getQueryByID(p_idproduit_catalogue, p_idtheme, p_idsurtheme, p_idracine_master, p_idracine, p_idperimetre, idDebut, idFin, idCliche, idorga)>

		<cfreturn rsltHandlerquery>
	</cffunction>
	
	<cffunction name="getQueryByID" output="true" access="private" returntype="Query">
		
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idsurtheme" 			required="true" type="Numeric"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="idDebut" 					required="true" type="Numeric" default="0">
		<cfargument name="idFin" 					required="true" type="Numeric" default="0">
		<cfargument name="idCliche" 				required="true" type="Numeric" default="0">
		<cfargument name="idorga" 					required="true" type="Numeric" default="0">
		
			<cfset dataTable = "E0_AGGREGS">
			<cfswitch expression="#niveau#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS_DEFRAG">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
						
			<cfswitch expression="#idtdb#">				
				
				<cfcase value="2">
					<!--- SURTHEME --->
					 
					<cfquery datasource="#dataSourceName#" name="queryrslt2">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
					 	 SELECT PRODUIT1GROUPE.THEME as THEME_LIBELLE, 
									PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
								 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
									PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
									PRODUIT1GROUPE.IDTHEME_PRODUIT as IDTHEME_PRODUIT, 
									FACTURATION.QTE as QTE, 
									DEVISE.TAUX_CONV as TAUX_CONV,
									FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
									FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
									ROUND(FACTURATION.VOLUME_MN,0) as DUREE_APPEL,
									UNITE_VOLUME.UNITE_SOMMEE as unite
						FROM #dataTable#
						WHERE (PERIODE.IDPERIODE_MOIS BETWEEN #idDebut# AND #idFin#)
								AND (PRODUIT1GROUPE.IDSUR_THEME =  #arguments.p_idsurtheme#) 
						ORDER BY MONTANT_FINAL DESC
					</cfquery>					
					<cfset query = queryrslt2>
				</cfcase>
				
			</cfswitch>	
		<cfreturn query>
	</cffunction>
	
</cfcomponent>
