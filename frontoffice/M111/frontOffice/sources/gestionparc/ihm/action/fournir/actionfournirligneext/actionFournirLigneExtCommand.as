package gestionparc.ihm.action.fournir.actionfournirligneext
{
	import gestionparc.ihm.action.ICommand;

	public class actionFournirLigneExtCommand implements ICommand
	{

		private var receiver:actionFournirLigneExtReceiver=null;

		public function actionFournirLigneExtCommand(rec:actionFournirLigneExtReceiver)
		{
			this.receiver=rec;
		}

		/**
		 * Cette fonction représente le "execute()" de la pattern command.
		 * Elle delegue les actions a realiser au receiver.
		 */
		public function execute():void
		{
			receiver.action();
		}
	}
}
