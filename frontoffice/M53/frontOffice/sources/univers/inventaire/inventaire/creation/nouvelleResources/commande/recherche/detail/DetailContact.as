package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail
{
	import mx.events.FlexEvent;
	import mx.events.CloseEvent;
	import flash.events.MouseEvent;
	import mx.managers.PopUpManager;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	
	import flash.events.Event;
	import mx.controls.Image;
	import mx.collections.ArrayCollection;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import composants.util.ConsoviewUtil;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheSociete;
	import mx.controls.Alert;
	import flash.display.DisplayObject;
	
	/**
	 * Classe permettant de gerer l'écran 'Déatil d'un contact'
	 * */
	public class DetailContact extends DetailContactIHM
	{
		/**
		 * Constante definissant le type d'evenement dispatché lors de la validation du formulaire
		 * */
		public static const VALIDER : String = "contactCreated";
		
		//Reference vers le contact
		private var contact : Contact;
		
		//Reference vers l'identifiant du groupe sur lequel on est logué
		private var idGroupe : Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
		
		//Tableau contenant la liste des salutations
		[Bindable]
		private var listeCivilite : Array = [{civilite : "Mr"},{civilite : "Mme"},{civilite : "Mlle"}];
		
		//Reference vers l'objet gérant la liste des société
		private var societes : Societe;
		
		//Reference vers l'ecran de recherche d'une societe
		private var rechercheSociete : PanelRechercheSociete;
		
		//Référence locale vers l'identifiant du contact nouvellement créé 
		private var _newContactID : int;

		/**
		 * Retourne l'identifiant du contact créée, 
		 * Il faut attendre que l'evenement DetailContact.VALIDER ait été dispatché
		 * @return _newContactID
		 * */
		public function get newContactID():int{
			return _newContactID;
		} 
		
		/**
		 * Constructeur
		 * @param c le contact que l'on veut afficher
		 * */
		public function DetailContact(c : Contact = null)
		{
			super();
			if (c != null) {
				contact = c;									
			}else{
				contact = new Contact();
			}		
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
				
		}
		
		/**
		 * Ne fait rien pour le moment
		 * */
		public function clean():void{
			if (rechercheSociete != null) rechercheSociete.clean();
			rechercheSociete = null;
			contact.clean();
		}
		
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{				
			addEventListener(CloseEvent.CLOSE,fermerFenetre);						
			
			cmbCivilite.dataProvider = listeCivilite;
			cmbCivilite.labelField = "civilite";		
			
			btSortir.addEventListener(MouseEvent.CLICK,sortir);								
			btValider.addEventListener(MouseEvent.CLICK,sauverContact);	
			
			imgSearchSociete.addEventListener(MouseEvent.CLICK,afficherRechercheSociete);			
			
			societes = new Societe();
			societes.prepareList(idGroupe);
			societes.addEventListener(Societe.LISTE_COMPLETE,afficherSocietes);					
		}
		
		//Affiche la fenêtre de recherche d'une societe
		private function afficherRechercheSociete(me : MouseEvent):void{
			rechercheSociete = new PanelRechercheSociete(false);						
			rechercheSociete.addEventListener(PanelRechercheSociete.VALIDER,selectionnerSociete);
			ConsoviewUtil.scale(DisplayObject(rechercheSociete),moduleCommandeMobileIHM.W_ratio,moduleCommandeMobileIHM.H_ratio);				
			PopUpManager.addPopUp(rechercheSociete,this,true);	
			PopUpManager.centerPopUp(rechercheSociete);		
		}
		
		//selectionne la societe valier dans la fenêtre 'Recherche Société'
		private function selectionnerSociete(ev : Event):void{
			cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,"societeID",rechercheSociete.societeSelectionneeID);			
		}
		
		//Initialisation du composant suivant le parametre passé
		//si le param est à 'true' initialise le composant pour une creation, si non pour une mise à jour
		//param in isNew un Boléen
		private function initContact(isNew : Boolean):void{
			(contact.contactID > 0)
			if (isNew){
				initNewContact();
			}else{
				initContactForUpdate();
			}			
		}
		
		//Initialise le composant pour une mise à jour d'un contact
		private function initContactForUpdate():void{
			contact.addEventListener(Contact.UPDATE_COMPLETE,contactCree);	
			afficherContact();
			btValider.label = "Mettre à jour";
		}
		
		//Initialise le composant pour une création de contact
		private function initNewContact():void{
			contact.addEventListener(Contact.SAVE_COMPLETE,contactCree);	
			btValider.label = "Valider";
		}
		
		
		//fermer fentre apres un click sur la croix de fremeture
		private function fermerFenetre(ce : CloseEvent):void{
			clean();
			PopUpManager.removePopUp(this);			
		}
		
		//fermer fentre apres un click sur le bouton 'Sortir'
		private function sortir(me : MouseEvent):void{
			clean();
			PopUpManager.removePopUp(this);
		}
		
		//Affiche le detail du contact dans le formulaire
		private function afficherContact():void{			
			txtEmail.text = contact.email;
			txtNom.text = contact.nom;
			txtPrenom.text = contact.prenom;
			txtTelephone.text = contact.telephone;
			txtFax.text = contact.fax;
			cmbCivilite.selectedIndex = ConsoviewUtil.getIndexByLabel(cmbCivilite.dataProvider as ArrayCollection,
													"civilite",
													contact.civilite);
			cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection
													,"societeID",contact.societeID);			
		}
		
		
		//Affiche la liste des société dans la combo 'Société'
		private function afficherSocietes(ev : Event):void{
			cmbSociete.labelField = "raisonSociale";
			cmbSociete.dataProvider = societes.liste;
			initContact(!(contact.contactID > 0));
		}
		
		//Enregistre le contact (mise a jour ou creation)
		private function sauverContact(ev : Event):void{			
			contact.email = txtEmail.text;
			contact.nom = txtNom.text;
			contact.prenom = txtPrenom.text;
			contact.telephone = txtTelephone.text;
			contact.fax = txtFax.text;
			contact.civilite = String(cmbCivilite.selectedItem.civilite);
			
			if (cmbSociete.selectedItem != -1){
				contact.societeID = Societe(cmbSociete.selectedItem).societeID;	
			}
			 
			
			
			if (contact.contactID){
				contact.updateContact();
			}else{
				if (checkContact()){
					contact.sauvegarder(idGroupe);	
				}				
			} 
		}
		
		//Verifie la validité du formulaire
		private function checkContact():Boolean{
			var nomOk : Boolean = (txtNom.length > 0);
			var societeOk : Boolean = (cmbSociete.selectedItem != null);
			var message : String = "";
			
			if (!nomOk){
				message = message + "- Le nom est obligatoire\n";
			}			
			if (!societeOk){
				message = message + "- La société est obligatoire\n";
			}
			
			if (nomOk && societeOk){
				return true;
			}else{
				Alert.show(message,"Attention !!");				
				return false;
			}
			
		}
		
		//Dispatche un évenement signifiant qu'un contact vient d'être créer
		//et ferme la fenêtre
		private function contactCree(ev : Event):void{
			_newContactID = contact.contactID;
			dispatchEvent(new Event(DetailContact.VALIDER));
			PopUpManager.removePopUp(this);
		}
		
	}
}