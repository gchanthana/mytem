<!--- Instanciation de la repartition selon operateur --->
<cfset obj = CreateObject("component","fr.consotel.consoview.tb.accueil.facade")>

<cfset repartitionOperateur=obj.getRepartOperateurBySegment(tb)>
<cfset repartitionDuree=obj.getRepartDureeBySegment(tb)>

<!--- <cfset repartitionOperateur=Evaluate("session.tableauBord_#tb#.tableau.getRepartOperateur()")>
<cfset repartitionDuree=Evaluate("session.tableauBord_#tb#.tableau.getRepartDuree()")> --->

<cfobject component="fr.consotel.consoview.cfm.graph.graph" name="mygraph">

<!--- <cfinclude template="pdf/report_creation.cfm"> --->
<cfset format_graph="png">

<cfset resultset=obj.getAboBySegment(tb)>
<cfset conso=obj.getConsoBySegment(tb)>

<!--- <cfset resultset=Evaluate("session.tableauBord_"&tb&".tableau.getAbo()")>
<cfset conso=Evaluate("session.tableauBord_" & tb & ".tableau.getConso()")> --->

<table cellpadding="2" cellspacing="0" border="0" align="center" width="100%">
	<tr>
		<!--- Tableau des abonnements par type --->
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<caption align="left"><cfoutput>Abonnements</cfoutput></caption>
							<tr align="center">
								<th>Type de Produits</th>
								<td width="1" bgcolor="808080"></td>
								<th>Qt&eacute; factur&eacute;e</th>
								<td width="1" bgcolor="808080"></td>
								<th>Montant (&euro; H.T)</th>
							</tr>
							<tr><td colspan="5" height="1" bgcolor="808080"></td></tr>
							<cfset montant_abo_total=0>
							<!--- va chercher les donn&eacute;es dans la session --->
							<cfoutput query="resultset">
									<tr>
										<td class="grid_normal">
												#theme_Libelle#
										</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right">#LsNumberFormat(val(qte), '9,999,999,999')#</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right">#LsEuroCurrencyFormat(val(montant_final), 'none')#</td>
									</tr>
									<cfset montant_abo_total=montant_abo_total+montant_final>
							</cfoutput>
							<tr><td colspan="5" height="1" bgcolor="808080"></td></tr>
							<cfoutput>
								<tr>
									<th align="left">TOTAL</th>
									<td width="1" bgcolor="99CCFF"></td>
									<th></th>
									<td width="1" bgcolor="808080"></td>
									<th align="right">#LsEuroCurrencyFormat(montant_abo_total, 'none')# &euro;</th>
								</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<!--- Tableau des consommations par type --->
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<caption align="left"><cfoutput>Consommations</cfoutput></caption>
							<tr align="center">
								<th><nobr>Type de Produits</nobr></th>
								<td width="1" bgcolor="808080"></td>
								<th><nobr>Nb. Appels</nobr></th>
								<td width="1" bgcolor="808080"></td>
								<th><nobr>Volume (min ou kO)</nobr></th>
								<td width="1" bgcolor="808080"></td>
								<th><nobr>Montant (&euro; H.T)</nobr></th>
							</tr>
							<tr><td colspan="7" height="1" bgcolor="808080"></td></tr>
							<cfset montant_conso_total=0>
							<cfset nbr_conso_total=0>
							<cfset duree_conso_total=0>
							<!--- <cfoutput query="qGetDetailFactureAbo"> --->
							<cfoutput query="conso">
							 
									<tr>
									<td class="grid_normal">
											<nobr>#theme_Libelle#</nobr>
									</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right">#LsNumberFormat(val(nombre_appel), '9,999,999,999')#</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right">#LsNumberFormat(int(val(duree_appel)), '9,999,999,999')#</td>
										<td width="1" bgcolor="808080"></td>
										<td class="grid_normal" align="right">#LsEuroCurrencyFormat(val(montant_final), 'none')#</td>
									</tr>
									<cfset montant_conso_total=montant_conso_total+val(montant_final)>
									<cfset nbr_conso_total=nbr_conso_total+nombre_appel>
									<cfset duree_conso_total=duree_conso_total+int(val(duree_appel))>
								 
							</cfoutput>
							<tr><td colspan="7" height="1" bgcolor="808080"></td></tr>
							<cfoutput>
								<tr>
								<th align="left">TOTAL</th>
									<td width="1" bgcolor="808080"></td>
									<th align="right">#LsNumberFormat(val(nbr_conso_total), '9,999,999,999')#</th>
									<td width="1" bgcolor="808080"></td>
									<th align="right">#LsNumberFormat(val(duree_conso_total), '9,999,999,999')#</th>
									<td width="1" bgcolor="808080"></td>
									<th align="right">#LsEuroCurrencyFormat(val(montant_conso_total), 'none')# &euro;</th>
								</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top">
			<cfif repartitionOperateur.recordcount gt 0>
			<table cellspacing="0" align="center"  border="0" cellpadding="0" width="100%"> 
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="350">
							<caption align="left">R&eacute;partition des co&ucirc;ts</caption>
							<tr align="center">
							<td valign="middle" width="350" height="140" bgcolor="ffffff">
							<cfsavecontent variable="chartData">
								<cfchart  
									style="/fr/consotel/consoview/cfm/graph/graph_pie_no_insets_sm_euro.xml"
									chartHeight = "140"
									chartWidth = "350"
									format="#format_graph#"
									showLegend="yes"
									font="Verdana"
									fontsize="9"
									>
									<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#">
										<cfloop query="repartitionOperateur">
											<cfset v=nom & " - " & replace(left(type_theme,3),"Con","Com") & ".">
											<cfchartdata item="#v#" value="#trim(montant_final)#">
										</cfloop>
									</cfchartseries>
								</cfchart>
								</cfsavecontent>
								<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")> 
								<cfoutput>#chartData#</cfoutput>																
							</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</cfif>
		</td>
		<!--- Camembert de repartition cout des conso --->
		<td valign="top">
			<cfif ((montant_abo_total NEQ 0) AND (montant_conso_total NEQ 0))>
				<table cellspacing="0" align="center" border="0" cellpadding="0" width="100%">
					<tr>
						<td valign="top">
							<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="350">
								<caption align="left">R&eacute;partition des co&ucirc;ts des consommations</caption>
								<tr align="center">
									<td valign="middle" width="350" height="140" bgcolor="ffffff">
										<cfsavecontent variable="chartData">
								<cfchart  
									style="/fr/consotel/consoview/cfm/graph/graph_pie_no_insets_euro.xml"
									chartHeight = "140"
									chartWidth = "350"
									format="#format_graph#"
									showLegend="yes"
									font="Verdana"
									fontsize="9"
									>
									<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',2)#">
										<cfloop query="repartitionOperateur">
											<cfif type_theme eq "Consommations">
												<cfchartdata item="#nom#" value="#trim(montant_final)#">
											</cfif>
										</cfloop>
									</cfchartseries>
								</cfchart>
								</cfsavecontent>
								<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
								<cfoutput>#chartData#</cfoutput>
									</td>								
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</cfif>
		</td>
	</tr>
	<tr>
		<td valign="top">
		<!--- on fabrique une liste avec les valeurs cumul&eacute;es --->
		<!--- <cfset conso_abo = "">
		<cfset maxi=0>
		<cfloop collection='#Evaluate("session.tableauBord_"&tb&".evoCout")#' item="a">
			<cfset conso_abo = ListAppend(conso_abo, Evaluate("session.tableauBord_"&tb&".evoCout[a].coutConso")+Evaluate("session.tableauBord_"&tb&".evoCout[a].coutAbo"))>
			<cfif Evaluate("session.tableauBord_"&tb&".evoCout[a].coutConso")+Evaluate("session.tableauBord_"&tb&".evoCout[a].coutAbo") gt maxi>
				<cfset maxi=Evaluate("session.tableauBord_"&tb&".evoCout[a].coutConso")+Evaluate("session.tableauBord_"&tb&".evoCout[a].coutAbo")>
			</cfif>
		</cfloop>
		<cfset maxi=ceiling(maxi)>
		<cfif maxi neq 0>
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td valign="middle" bgcolor="ffffff">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<caption align="left">Evolution des co&eacute;ts sur les 6 derniers mois</caption>
							<tr align="center">
								<td valign="middle" width="350" height="140" bgcolor="ffffff">
									<cfset structTriee=ListSort(StructKeyList(Evaluate("session.tableauBord_"&tb&".evoCout")),"numeric","asc")>
									<cfsavecontent variable="chartData">
									<cfchart
									 	style="/fr/consotel/consoview/cfm/graph/graph_bar.xml"
										chartHeight = "140"
										chartWidth = "350"
										format="#format_graph#"
										showLegend="yes"
										font="Verdana"
										fontsize="9"
										yaxistitle="Euros"
										>
										<cfchartseries  type="bar" seriesColor="#mygraph.GetColor('bar',1,2)#" serieslabel="Conso.">
											<cfloop list="#structTriee#" index="i">
												<cfchartdata item='#Evaluate("session.tableauBord_"&tb&".evoCout[i].mois")#' value='#Evaluate("session.tableauBord_"&tb&".evoCout[i].coutConso")#'>
											</cfloop>
										</cfchartseries>
										<cfchartseries type="bar" seriesColor="#mygraph.GetColor('bar',1,1)#" serieslabel="Abo.">
											<cfloop list="#structTriee#" index="i">
												<cfchartdata item='#Evaluate("session.tableauBord_"&tb&".evoCout[i].mois")#' value='#Evaluate("session.tableauBord_"&tb&".evoCout[i].coutAbo")#'>
											</cfloop>
										</cfchartseries>
									</cfchart>
									</cfsavecontent>
										<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
										<cfoutput>#chartData#</cfoutput>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</cfif>--->
		</td> 		
		<td valign="top">
			<cfif repartitionDuree.recordcount gt 0>
			<table cellspacing="0" align="center"  border="0" cellpadding="0" width="100%"> 
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="350">
							<caption align="left">R&eacute;partition des dur&eacute;es des consommations</caption>
							<tr align="center">
							<td valign="middle" width="350" height="140" bgcolor="ffffff">
								<cfsavecontent variable="chartData">
								<cfchart  
									style="/fr/consotel/consoview/cfm/graph/graph_pie_no_insets_min.xml"
									chartHeight = "140"
									chartWidth = "350"
									format="#format_graph#"
									showLegend="yes"
									font="Verdana"
									fontsize="9"
									>
									<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',2)#">
										<cfloop query="repartitionDuree">
											<cfchartdata item="#nom#" value="#Int(duree_appel)#">
										</cfloop>
									</cfchartseries>
								</cfchart>
								</cfsavecontent>
								<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
								<cfoutput>#chartData#</cfoutput>
							</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</cfif>
		</td>
	</tr>
</table>
