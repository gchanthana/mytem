<cfif rapportParams.periodicite EQ 0>
	<cfset periodiciteString = "mensuel">
<cfelse>
	<cfset periodiciteString = "bimestriel">
</cfif>
<cfheader name="Content-Disposition" value="inline;filename=detailfilaire_#RAISON_SOCIALE#.xls" charset="iso-8859-1">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Brice Miramont</Author>
  <LastAuthor>cedric.rapiera</LastAuthor>
  <Created>2006-04-27T06:43:07Z</Created>
  <LastSaved>2006-05-04T15:26:14Z</LastSaved>
  <Company>CONSOTEL</Company>
  <Version>11.6408</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8190</WindowHeight>
  <WindowWidth>13500</WindowWidth>
  <WindowTopX>600</WindowTopX>
  <WindowTopY>495</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s211" ss:Name="Euro">
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s16" ss:Name="Milliers">
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s60">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>
  <Style ss:ID="s91">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s102">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat ss:Format="#,##0"/>
  </Style>
  <Style ss:ID="s103">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s104">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="2"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>

  <Style ss:ID="s125">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7" ss:Bold="1"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>

  <Style ss:ID="s126">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FF9900" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s129">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7" ss:Bold="1"/>
   <Interior ss:Color="#FF9900" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s131">
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>
  <Style ss:ID="s144">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s145">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s147">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s149">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>
  <Style ss:ID="s182">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="2"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>
  <Style ss:ID="s185">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s193">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="2"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s195">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s196">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>
  <Style ss:ID="s199">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s201">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s202">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:Rotate="-90"
    ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s203">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s204">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s207">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s208">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s213" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>
  <Style ss:ID="s215" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Dot" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
  </Style>
  <Style ss:ID="s217" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Dot" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s227" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat ss:Format="0_ ;\-0\ "/>
  </Style>
  <Style ss:ID="s228">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s230">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s231">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s232">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s233">
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s234">
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s235">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s236">
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s237">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"
    ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s238">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s239">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"
    ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s240">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s241">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s242">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s243">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Right" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s244">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:Rotate="-90"
    ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8" ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s245">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:Indent="1"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat/>
  </Style>
  <Style ss:ID="s279" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat ss:Format="_-* #,##0\ _?_-;\-* #,##0\ _?_-;_-* &quot;-&quot;??\ _?_-;_-@_-"/>
  </Style>
  <Style ss:ID="s280" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s281">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s282" ss:Parent="s16">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s283" ss:Parent="s16">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s284">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s285">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s286">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FF9900" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_-* #,##0\ _?_-;\-* #,##0\ _?_-;_-* &quot;-&quot;??\ _?_-;_-@_-"/>
  </Style>
  <Style ss:ID="s287">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s288" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s289" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s290" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FF9900" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s292">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous"/>
    <Border ss:Position="Left" ss:LineStyle="Dot" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="7" ss:Bold="1"/>
   <Interior/>
   <NumberFormat ss:Format="0.0%"/>
  </Style>
  <Style ss:ID="s294" ss:Parent="s211">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_-* #,##0\ _?_-;\-* #,##0\ _?_-;_-* &quot;-&quot;??\ _?_-;_-@_-"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Segment Fixe">
  <Names>
	<cfset rangeVar="R1C1:R#NB_ROWS_TOTAL#C#NB_COLS_TOTAL#">
	<cfoutput>
	   <NamedRange ss:Name="Print_Area" ss:RefersTo="='Segment Fixe'!#rangeVar#"/>
	</cfoutput>
  </Names>

<cfoutput>
	<cfset expandedRow=NB_ROWS_TOTAL+1>
	<cfset nbSocietes=listeSocietes.recordcount>
	<Table ss:ExpandedColumnCount="#NB_COLS_TOTAL#" ss:ExpandedRowCount="#expandedRow#" x:FullColumns="1"
	 x:FullRows="1" ss:StyleID="s131" ss:DefaultColumnWidth="60">
</cfoutput>
				<!---======= DEBUT 2 LIGNES FIXES =======--->
   <!---Column ss:StyleID="s131" ss:AutoFitWidth="0" ss:Width="234.75"/--->
   <Column ss:StyleID="s131" ss:Width="214.5"/>
	<cfoutput query="listeSocietes">
	   <Column ss:StyleID="s131" ss:Width="90"/>
	   <Column ss:StyleID="s131" ss:AutoFitWidth="0" ss:Width="92.25"/>
	   <!---Column ss:StyleID="s131" ss:Width="58.5"/--->
	   <Column ss:StyleID="s131" ss:Width="20"/>
	</cfoutput>
				<!---======= DEBUT 3 LIGNES FIXES =======--->
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:StyleID="s233"><Data ss:Type="String">Rapport Spécifique Téléphonie Fixe</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
	<cfoutput query="listeSocietes">
	    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	</cfoutput>
   </Row>

   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:StyleID="s235"><Data ss:Type="String">Lignes <cfoutput>#RAISON_SOCIALE#</cfoutput></Data><NamedCell
      ss:Name="Print_Area"/></Cell>
	<cfoutput query="listeSocietes">
	    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s236"><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	</cfoutput>
   </Row>

   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:StyleID="s235"><Data ss:Type="String">Mois de <cfoutput>#RapportParams.chaine_date#</cfoutput></Data><NamedCell
      ss:Name="Print_Area"/></Cell>
	<cfoutput query="listeSocietes">
	    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s236"><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	</cfoutput>
   </Row>

   <Row ss:AutoFitHeight="0" ss:Height="13.5">
    <Cell ss:StyleID="s234"><NamedCell ss:Name="Print_Area"/></Cell>
	<Cell ss:StyleID="s237"><ss:Data ss:Type="String">
		Cumul <cfoutput>#periodiciteString# #RapportParams.date_m# au
#LsDateFormat(parseDateTime(RapportParams.date_m2),"dd/mm/yyyy")#
			<!---
#LsDateFormat(dateadd("d",-1,dateadd("m",1,CreateDate(right(RapportParams.date_m2,4),mid(RapportParams.date_m2,4,2),"01"))),"dd/mm/yyyy")#
			--->
		</cfoutput>
		</ss:Data>
	</Cell>
	
    <Cell ss:StyleID="s238"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s238"><NamedCell ss:Name="Print_Area"/></Cell>
	<cfset fois1=(nbSocietes - 1)>
	<cfloop from="1" to="#fois1#" index="i">
		<cfoutput>
		    <Cell ss:StyleID="s239"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s239"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s239"><NamedCell ss:Name="Print_Area"/></Cell>
		</cfoutput>
	</cfloop>
   </Row>
				<!---======= FIN 3 LIGNES FIXES =======--->
				<!---======= DEBUT DES SOCIETES =======--->
   <Row ss:Height="26.25" ss:StyleID="s62">
    <Cell ss:StyleID="s240"><NamedCell ss:Name="Print_Area"/></Cell>
	<cfoutput query="listeSocietes">
	    <Cell ss:StyleID="s237"><Data ss:Type="String">#libelle#</Data><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s239"><NamedCell ss:Name="Print_Area"/></Cell>
	    <Cell ss:StyleID="s239"><NamedCell ss:Name="Print_Area"/></Cell>
	</cfoutput>
   </Row>
				<!---======= FIN DES SOCIETES =======--->



				<!---======= DEBUT EN-TETE COLONNES TYPE-THEME-1 =======--->
<cfoutput query="listeThemesHierarchy" group="type_theme">
	<cfset currentTypeTheme=type_theme>
   <Row ss:Height="25.5">
	    <Cell ss:StyleID="s241"><Data ss:Type="String">#currentTypeTheme#</Data><NamedCell
	      ss:Name="Print_Area"/></Cell>
	<cfloop from="1" to="#nbSocietes#" index="i">
			<cfif UCase(#currentTypeTheme#) eq "ABONNEMENTS">
			    <Cell ss:StyleID="s242"><Data ss:Type="String">Quantité</Data><NamedCell
			      ss:Name="Print_Area"/></Cell>
			<cfelse>      
			    <Cell ss:StyleID="s242"><Data ss:Type="String">Durée en minutes</Data><NamedCell
			      ss:Name="Print_Area"/></Cell>
			</cfif>
		    <Cell ss:StyleID="s243"><Data ss:Type="String">Montant</Data><NamedCell
		      ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s244"><Data ss:Type="String">%P-1</Data><NamedCell
		      ss:Name="Print_Area"/></Cell>
	</cfloop>
   </Row>

    <Row>
	    <Cell ss:StyleID="s104"><NamedCell ss:Name="Print_Area"/></Cell>
		<cfloop from="1" to="#nbSocietes#" index="i">
		    <Cell ss:StyleID="s279"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s213"><NamedCell ss:Name="Print_Area"/></Cell>
			<Cell ss:StyleID="s91"><NamedCell ss:Name="Print_Area"/></Cell>
		</cfloop>
    </Row>

	<cfoutput group="sur_theme">
		<cfset currentSurTheme=sur_theme>
	   <Row>
		<Cell ss:StyleID="s245"><Data ss:Type="String">#currentSurTheme#</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<cfloop from="1" to="#nbSocietes#" index="i">
		    <Cell ss:StyleID="s230"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s231"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s232"><NamedCell ss:Name="Print_Area"/></Cell>
		</cfloop>
	   </Row>
	   
		<cfoutput>
			<cfset currentTheme=theme_libelle>
		    <Row>
			    <Cell ss:StyleID="s104"><Data ss:Type="String">#currentTheme#</Data><NamedCell
			      ss:Name="Print_Area"/></Cell>
				<cfloop from="1" to="#listeSocietes.recordcount#" index="k">
					<cfset idref_client=listeSocietes['idref_client'][k]>
					<cfset qte =
						detailsStruct[currentTypeTheme][currentSurTheme][idtheme_produit][idref_client].qte>
					<cfset duree_appel =
						detailsStruct[currentTypeTheme][currentSurTheme][idtheme_produit][idref_client].duree>
					<cfset montant_final =
						detailsStruct[currentTypeTheme][currentSurTheme][idtheme_produit][idref_client].montant>
					<cfset pourc_evo =
						detailsStruct[currentTypeTheme][currentSurTheme][idtheme_produit][idref_client].evo_average>
					<cfset pourc_evo_msg =
						detailsStruct[currentTypeTheme][currentSurTheme][idtheme_produit][idref_client].pourc_evo_msg>
					
					<cfif UCase(#currentTypeTheme#) eq "ABONNEMENTS">
					    <Cell ss:StyleID="s279"><Data ss:Type="Number">#qte#</Data>
					    	<NamedCell ss:Name="Print_Area"/></Cell>
					<cfelse>
					    <Cell ss:StyleID="s279"><Data ss:Type="Number">#duree_appel#</Data><NamedCell
					      ss:Name="Print_Area"/></Cell>
					</cfif>
				    <Cell ss:StyleID="s213"><Data ss:Type="Number">#montant_final#</Data><NamedCell
				      ss:Name="Print_Area"/></Cell>

					<Cell ss:StyleID="s91">
				    <cfif pourc_evo_msg eq "Number">
					    <Data ss:Type="Number">#pourc_evo#</Data>
					<cfelse>
						<Data ss:Type="String">N.D</Data>
				    </cfif>
				    	<NamedCell ss:Name="Print_Area"/>
				    </Cell>
				</cfloop>
		   </Row>
			
		</cfoutput>
		    <Row>
			    <Cell ss:StyleID="s104"><Data ss:Type="String">Total #currentSurTheme#</Data><NamedCell
			      ss:Name="Print_Area"/></Cell>
				<cfloop from="1" to="#nbSocietes#" index="l">
					<cfset idref_client1=listeSocietes['idref_client'][l]>
					<cfset totalSurThemeMontantSociete=totalSurThemes[currentSurTheme][idref_client1].totalMontantSurTheme>
					<cfset totalSurThemeDureeSociete=totalSurThemes[currentSurTheme][idref_client1].totalDureeSurTheme>
					<cfset totalSurThemeQteSociete=totalSurThemes[currentSurTheme][idref_client1].totalQteSurTheme>
					
					<cfif UCase(#currentTypeTheme#) eq "ABONNEMENTS">
					    <Cell ss:StyleID="s279"><Data ss:Type="Number">#totalSurThemeQteSociete#</Data>
					    	<NamedCell ss:Name="Print_Area"/></Cell>
				    <cfelse>
					    <Cell ss:StyleID="s279"><Data ss:Type="Number">#totalSurThemeDureeSociete#</Data>
					    	<NamedCell ss:Name="Print_Area"/></Cell>
				    </cfif>
				    <Cell ss:StyleID="s213"><Data ss:Type="Number">#totalSurThemeMontantSociete#</Data><NamedCell
				      ss:Name="Print_Area"/></Cell>
					<Cell ss:StyleID="s91">
				    	<NamedCell ss:Name="Print_Area"/>
				    </Cell>
				</cfloop>
		    </Row>
		    
		    <Row>
			    <Cell ss:StyleID="s104"><NamedCell ss:Name="Print_Area"/></Cell>
				<cfloop from="1" to="#nbSocietes#" index="i">
				    <Cell ss:StyleID="s279"><NamedCell ss:Name="Print_Area"/></Cell>
				    <Cell ss:StyleID="s213"><NamedCell ss:Name="Print_Area"/></Cell>
					<Cell ss:StyleID="s91"><NamedCell ss:Name="Print_Area"/></Cell>
				</cfloop>
		    </Row>
	</cfoutput>
	
   <Row>
		<Cell ss:StyleID="s147"><Data ss:Type="String">Total #currentTypeTheme#</Data><NamedCell
		  	ss:Name="Print_Area"/></Cell>
		<cfloop from="1" to="#nbSocietes#" index="m">
			<cfset idref_client2=listeSocietes['idref_client'][m]>
			<cfset totalTypeThemeMontantSociete=totalTypesThemes[currentTypeTheme][idref_client2].totalMontantTypeTheme>
			<cfset totalTypeThemeDureeSociete=totalTypesThemes[currentTypeTheme][idref_client2].totalDureeTypeTheme>
			<cfset totalTypeThemeQteSociete=totalTypesThemes[currentTypeTheme][idref_client2].totalQteTypeTheme>
			<cfif UCase(#currentTypeTheme#) eq "ABONNEMENTS">
			    <Cell ss:StyleID="s294"><Data ss:Type="Number">#totalTypeThemeQteSociete#</Data><NamedCell
			      ss:Name="Print_Area"/></Cell>
			<cfelse>
			    <Cell ss:StyleID="s294"><Data ss:Type="Number">#totalTypeThemeDureeSociete#</Data><NamedCell
			      ss:Name="Print_Area"/></Cell>
			</cfif>
		      
		    <Cell ss:StyleID="s288"><Data ss:Type="Number">#totalTypeThemeMontantSociete#</Data><NamedCell
		      ss:Name="Print_Area"/></Cell>
		      
		    <Cell ss:StyleID="s195"><NamedCell
		      ss:Name="Print_Area"/></Cell>
		</cfloop>
   </Row>

   <Row ss:Height="13.5">
    <Cell ss:StyleID="s196"><Data ss:Type="String">dont part France Telecom</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
	<cfloop from="1" to="#nbSocietes#" index="n">
		<cfset idref_client3=listeSocietes['idref_client'][n]>
		<cfset totalPartMontantFt=detailsFtTableauStruct[currentTypeTheme][idref_client3].montant>
		<cfset pourc_ft_msg=detailsFtTableauStruct[currentTypeTheme][idref_client3].pourc_evo_msg>
		<cfset pourc_ft=detailsFtTableauStruct[currentTypeTheme][idref_client3].evo_average>
		
	    <Cell ss:StyleID="s281"><NamedCell ss:Name="Print_Area"/></Cell>
	    
	    <Cell ss:StyleID="s289">
		<Data ss:Type="Number">#totalPartMontantFt#</Data>
	    <NamedCell ss:Name="Print_Area"/></Cell>
	    
	    <Cell ss:StyleID="s208">
		    <cfif pourc_ft_msg eq "Number">
		    	<Data ss:Type="Number">#pourc_ft#</Data>
		    <cfelse>
				<Data ss:Type="String">N.D</Data>
			</cfif>
	    	<NamedCell ss:Name="Print_Area"/>
	    </Cell>
	</cfloop>
   </Row>

	<Row ss:AutoFitHeight="0" ss:Height="13.5">
	    <Cell ss:StyleID="s125"><NamedCell ss:Name="Print_Area"/></Cell>
		<cfloop from="1" to="#nbSocietes#" index="i">
		    <Cell ss:StyleID="s125"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s125"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s125"><NamedCell ss:Name="Print_Area"/></Cell>
		</cfloop>
	</Row>

</cfoutput>

<Row ss:AutoFitHeight="0" ss:Height="13.5">
    <Cell ss:StyleID="s126"><Data ss:Type="String">Total Général</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
	<cfloop from="1" to="#nbSocietes#" index="o">
		<cfset idref_client4=listeSocietes['idref_client'][o]>
		<cfset totalMontantFinal=totalFinal[idref_client4].totalMontantFinal>
		<cfoutput>
		    <Cell ss:StyleID="s286"><NamedCell ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s290"><Data ss:Type="Number">#totalMontantFinal#</Data><NamedCell
		      ss:Name="Print_Area"/></Cell>
		    <Cell ss:StyleID="s129"><NamedCell
		      ss:Name="Print_Area"/></Cell>
		</cfoutput>
	</cfloop>
</Row>

</Table>

  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Layout x:Orientation="Landscape" x:CenterHorizontal="1" x:CenterVertical="1"/>
    <Header x:Margin="0.19685039370078741"/>
    <Footer x:Margin="0.23622047244094491"/>
    <PageMargins x:Bottom="0.35433070866141736" x:Left="0.23622047244094491"
     x:Right="0.19685039370078741" x:Top="0.35433070866141736"/>
   </PageSetup>
   <FitToPage/>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>69</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <DoNotDisplayGridlines/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>25</ActiveRow>
     <ActiveCol>2</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
