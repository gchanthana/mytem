package univers.inventaire.equipements.lignes.mainComponent
{
	import composants.parametres.perimetres.OrgaStructure;
	import flash.events.Event;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	import mx.events.ListEvent;
	import mx.controls.Tree;
	
	import mx.collections.XMLListCollection;
	import univers.inventaire.equipements.lignes.main;
	import mx.controls.Alert;

	public class MainOrgaCmp extends OrgaStructure
	{
		
		public function MainOrgaCmp()
		{
			
		}
		
		override public function initIHM(event:Event):void
		{
			super.initIHM(event);
			cmbOrganisation.styleName = "comboInventaire";
		}
		
		override public function searchNode(nodeName:String):void
		{
			/** Todo
			 * Change the request 
			 * */
			 parentDocument.clearGridLines();
			 var selectedOrgaID:Number = cmbOrganisation.selectedItem.IDGROUPE_CLIENT;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"searchNode",
																				searchNodeResult, throwError);				
			RemoteObjectUtil.callService(op, selectedOrgaID, nodeName, 0, (parentDocument as main).getSelectedDate());
		}
		
		private function searchNodeResult(event : ResultEvent):void
		{
			/** ToDo
			 * Get the result of the search : ok
			 * */
			setDataResult(ArrayCollection(event.result).source);
		}
		
		override public function refreshTree():void
		{
			//var selectedOrgaID:Number = cmbOrganisation.selectedItem.IDGROUPE_CLIENT;
			_treeNodes = null;
			var selectedOrgaID:Number = getSelectedOrganisationValue();
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getNode",
																				fillTreeResult, throwError);
			RemoteObjectUtil.callService(op, selectedOrgaID, 0, _parentRef.getSelectedDate());	
		}

		
		private function fillTreeResult(event: ResultEvent):void
		{
			_treeNodes = XML(event.result).children();
			setTreeData(XML(event.result).children(), "@LABEL");
			//treePerimetre.selectedIndex = 0;
			//treePerimetre.validateNow();
			//treePerimetre.expandItem(treePerimetre.selectedItem, true);
			callLater(expandFirstElement);
		}
		
		private function expandFirstElement():void
		{
			treePerimetre.expandItem(_treeNodes[0], true);
		}
		
		/** Set the tree selected item editable */
		public function setSelectedTreeItemEditable(status:Boolean):void
		{
			treePerimetre.editable = status;
			if (status)
				treePerimetre.editedItemPosition = {columnIndex:0,rowIndex:treePerimetre.selectedIndex };
		}
		
		/** Add node to nodeParent in the tree */
		public function addItemToTree(IDparent:Number, nodeName:String):void
		{
			var newNode:XML = <node VALUE='-1' LABEL="untitled" TYPE_PERIMETRE="GroupeLigne"/>;			
			
			var dept:XMLList;
			if (_treeNodes.@VALUE != IDparent)
				dept = _treeNodes.descendants().(@VALUE == IDparent);
			else
				dept = _treeNodes.(@VALUE == IDparent);

			dept[0].appendChild(newNode);

		}
		
		/* Update the value of the last created node */
		public function updateNewItemValue(value:Number):void
		{
			var dept:XMLList = _treeNodes.descendants().(@VALUE == "-1");
			dept[0].@VALUE = value;
		}
		
		public function deleteTreeItem(id:Number):void
		{
			var dept:XMLList = _treeNodes.descendants().(@VALUE == id);
			delete dept[0];
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}
		
	}
}