package univers.parametres.rago.inventaire.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	
	import univers.parametres.rago.CreationRegles.ihm.PopUpTargetDatagridIHM;
	import univers.parametres.rago.CreationRegles.ihm.PopUpTargetTreeIHM;
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.entity.Rules;
	import univers.parametres.rago.entity.TypeRegle;
	
	[Bindable]
	public class PopUpNewOrigineOrCibleImpl extends HBox
	{
		
//FIN VARIABLES GLOBALES----

		private static const type_Origine:String = "Noeuds source";
		private static const type_Cible:String = "Noeuds cible";
		
		public var _selectedRegle:Regle;
		public var _rules:Rules = new Rules();
		public var _wizzard:Wizzard = new Wizzard();
		
		public var lbl_Noeuds:Label;
		public var lbl_typeRegle:Label;
		public var lbl_OpeOrClient:Label;
		public var lbl_SourceOrCible:Label;
		public var lblSourceSelected:Label;
		public var lastCible:Label;
		public var btnFindSourceInDatagrid:Button;
		public var btnFindSourceInTree:Button;
		public var findItSource:TextInput;
		public var cboxOpeAnuClient:ComboBox;
		public var rbTypaAnnuaire:RadioButton;
		public var rbOperateur:RadioButton;
		public var hbox_RadioBtnOpe:HBox;
		public var hbox_RadioBtnAnu:HBox;
		
		public var typeModif:String;
		
		private var _clientResult:Object = new Object();
		private var _operateurResult:Object = new Object();
		private var origineOrNot:Boolean = false;
		
//FIN VARIABLES GLOBALES----

//PROPRIETEES PUBLICS----
		
		//Récupère le type d'opérateur sélectionné lors de la création/modification de la règle
		public function set operateurResult(value:Object):void
		{ _operateurResult = value; }

		public function get operateurResult():Object
		{ return _operateurResult; }

//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur
		public function PopUpNewOrigineOrCibleImpl()
		{ super(); }

//FIN METHODES PUBLICS----

//METHODES PROTECTED----
		
		//Initialise la poUp avec les données qu'il faut
		protected function init(event:Event):void
		{
			if(typeModif == type_Origine)
			{
				origineOrNot = true;
				hbox_RadioBtnOpe.visible = true;
				hbox_RadioBtnOpe.height = 30;
				hbox_RadioBtnAnu.visible = true;
				hbox_RadioBtnAnu.height = 30;
				lbl_Noeuds.text = "origine";
				lbl_typeRegle.text = "Choix du type de règle";
				lbl_OpeOrClient.text = "Opérateur :";
				lbl_SourceOrCible.text = "Origine :";
				_selectedRegle.TYPE_REGLE = TypeRegle.REGLE_OPERATEUR;
			}
			else
			{
				origineOrNot = false;
				hbox_RadioBtnOpe.visible = false;
				hbox_RadioBtnAnu.visible = false; 
			}
			attributData();
		}
		
		//Lors de la validation de la modififcation de la règle en renvoie 
		//la règle à la base pour mettre a jour la règle
		protected function btValiderClickHandler(event:Event):void
		{ 
			_selectedRegle.REGLE_NOM = _selectedRegle.LIBELLE_SOURCE + "->" + _selectedRegle.LIBELLE_CIBLE;
			
			_rules.IURulesOrganisation(		_selectedRegle.IDREGLE_ORGA,
											_selectedRegle.REGLE_NOM,
											_selectedRegle.TYPE_REGLE,
											_selectedRegle.REGLE_COMMENT,
											_selectedRegle.IDSOURCE,
											_selectedRegle.IDCIBLE,
											zActiveOrNot(_selectedRegle.BOOL_ACTIVE),
											zFormatBoolean(_selectedRegle.BOOL_SUIVRE_AUTO));
			
			_rules.addEventListener("envoiRegle",executeSendException);
		}
		
		//Format un int en bool 0->false, 1->true
		private function zActiveOrNot(bool:Boolean):int
		{
			if(bool)return 1;
			else return 0;
		}

		//Fermer le poUp 'PopUPNewOrigineOrCibleIHM'
		protected function btAnnulerClickHandler(event:Event):void
		{ PopUpManager.removePopUp(this); }
		
		//Affecte la valeur du cbox à la propriétée 'operateurResult' <<<<INUTILISEE>>>>
		protected function cboxOpeCloseHandler(event:Event):void
		{ operateurResult = cboxOpeAnuClient.selectedItem; }

		//
		protected function choixSource(event:Event):void
		{
			lblSourceSelected.text = _wizzard.myRegle.LIBELLE_SOURCE;
			_selectedRegle.LIBELLE_SOURCE = _wizzard.myRegle.LIBELLE_SOURCE;
			_selectedRegle.IDSOURCE = _wizzard.myRegle.IDSOURCE;
			_rules.getNameAndSurname();
			_rules.addEventListener("NameAndSurname",recupName);
		}

		//Ne fais rien s'il on appuis sur le bouton 'annuler' dans la popUp 'SelectSourceIHM'
		protected function pasChoixSource(event:Event):void
		{ }
		
		//Récupère les données sélectionnées dans la popUp 'SelectTargetIHM'
		protected function choixTarget(event:Event):void
		{
			lblSourceSelected.text = _wizzard.myRegle.LIBELLE_CIBLE;
			_selectedRegle.LIBELLE_CIBLE = _wizzard.myRegle.LIBELLE_CIBLE;
			_selectedRegle.IDCIBLE = _wizzard.myRegle.IDCIBLE;
		}
		
		//Ne fais rien s'il on appuis sur le bouton 'annuler' dans la popUp 'SelectTargetIHM'
		protected function pasChoixTarget(event:Event):void
		{  }
		
		//Regarde s'il l'on veut modifier la source ou la cible et affiche la popUp en conséquence (datagrid)
		protected function findSourceInDatagridClickHandler(event:Event):void
		{
			if(origineOrNot) {  }
			else
			{ findTargetDatagridPopOp(); }
		}
		
		//Regarde s'il l'on veut modifier la source ou la cible et affiche la popUp en conséquence (arbre)
		protected function findSourceInTreeClickHandler(event:Event):void
		{
			if(origineOrNot) {  }
			else
			{ findTargetTreePopOp(); }
		}
		
		//Change le type de règle (annuaire/opérateur) et affiche dans le cbox les données correspondante (annuaire)
		protected function rdgAnnuaireChangeHandler(event:Event):void
		{
			cboxOpeAnuClient.enabled = false;
			zAttributTypeRegleInRegle(TypeRegle.REGLE_ANNUAIRE);
			
			for(var i:int = 0;i < _rules.ListOrgaAnu.length;i++)
			{
				if(_rules.ListOrgaAnu[i].LABEL.toString().indexOf(_selectedRegle.ORGA_CIBLE) != -1)
				{ operateurResult.addItem(_rules.ListOrgaAnu[i]); }
			}
		}
		
		//Change le type de règle (annuaire/opérateur) et affiche dans le cbox les données correspondante (opérateur)
		protected function rdgOperateurChangeHandler(event:Event):void
		{
			cboxOpeAnuClient.enabled = true;
			zAttributTypeRegleInRegle(TypeRegle.REGLE_OPERATEUR);
			operateurResult = _rules.ListOrgaOpe;
		}
		
		//Enabled le bouton annuaire et disabled le 'textInput' et le bouton 'ok'
		protected function rdgTreeChangeHandler(event:Event):void
		{ zAttributEnabledOrNot(false,false,true); }
		
		//Disabled le bouton annuaire et enabled le 'textInput' et le bouton 'ok'
		protected function rdgNoTreeChangeHandler(event:Event):void
		{ zAttributEnabledOrNot(true,true,false); }

//FIN METHODES PROTECTED----

//METHODES PRIVATE----
		
		//Liste toutes les orga
		private function attributData():void
		{
			_rules.fillOrganisation();
			_rules.addEventListener("OrgaFind",executeResultListOrgaOpeIntoCbox);
		}
		
		//
		private function executeResultListOrgaOpeIntoCbox(event:Event):void
		{ 
			for(var i:int = 0;i < _rules.ListOrgaCliente.length;i++)
			{
				if(_rules.ListOrgaCliente[i].LIBELLE_GROUPE_CLIENT.toString().indexOf(_selectedRegle.ORGA_CIBLE) != -1)
				{ operateurResult = _rules.ListOrgaCliente[i]; }
			}
			
			lastCible.text = recupLastCible();
		}
		
		//
		private function recupLastCible():String
		{ return _selectedRegle.LIBELLE_CIBLE;}
		
		//
		private function zFormatBoolean(bool:Boolean):int
		{
			if(bool)return 1;
			else return 0;
		}
		
		//
		private function executeSendException(event:Event):void
		{
			//Alert.show("Votre modification a été prise en compte.","CONSOVIEW");
			ConsoviewAlert.afficherOKImage("Votre modification a été prise en compte.",this);
			dispatchEvent(new Event("newOrigineOrCibleCreated",true));
			PopUpManager.removePopUp(this);
		}
		
		//
		private function recupName(event:Event):void
		{ _selectedRegle.REGLE_NOM =  _selectedRegle.LIBELLE_SOURCE + "->" + _selectedRegle.LIBELLE_CIBLE; }
		
		//
		private function findTargetDatagridPopOp():void
		{
			var p:PopUpTargetDatagridIHM = new PopUpTargetDatagridIHM();
			
			p._wizzard = _wizzard;
			p.resultClient = operateurResult;
			p._strgToSearch = findItSource;
			p.addEventListener("getChoiceTarget", choixTarget);
			p.addEventListener("AnnulChoiceTarget", pasChoixTarget);
			PopUpManager.addPopUp(p,this,true);
			PopUpManager.centerPopUp(p);
		}
		
		//
		private function findTargetTreePopOp():void
		{
			var p:PopUpTargetTreeIHM = new PopUpTargetTreeIHM();
			
			p._wizzard = _wizzard;
			p.resultClient = operateurResult;
			p._resultOpe = _selectedRegle;
			p.addEventListener("getChoiceTarget", choixTarget);
			p.addEventListener("AnnulChoiceTarget", pasChoixTarget);
			PopUpManager.addPopUp(p,this,true);
			PopUpManager.centerPopUp(p);
		}

		//
		private function zAttributTypeRegleInRegle(typeRegle:String):void
		{	
			if(_selectedRegle != null)
			{_selectedRegle.TYPE_REGLE = typeRegle; }
		}

		//
		private function zAttributEnabledOrNot(bool1:Boolean,bool2:Boolean,bool3:Boolean):void
		{
			btnFindSourceInDatagrid.enabled = bool1;
			findItSource.enabled = bool2;
			btnFindSourceInTree.enabled = bool3;
		}

//FIN METHODES PRIVATE----

	}
}