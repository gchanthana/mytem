package appli.Pilotage.CreateMenuPFGP.composant
{
	import composants.gradientcomponents.GradientBox;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import general.appliComposants.customClassButton.CustomClassSeparatorMenu1IHM;
	import general.appliComposants.customClassButton.CustomPFGPClassButtonMenu1;
	import general.appliComposants.customClassButton.CustomPFGPClassButtonMenu2;
	
	public class MenuPFGPImpl extends VBox
	{
		[Bindable] public var menu1:HBox;
		[Bindable] public var menu2:GradientBox;
		
		public var mainMenu:GradientBox;
		private var _menuDataprovider:XML;
		private var boolRollOverAvailable:Boolean = true;
		
		protected function init():void
		{
						
		}
/* ------------------------------------------------------------------------
		FONCTION PUBLIC
-------------------------------------------------------------------------*/	
	// rafraichit les status des modules
		public function refresh():void
		{
			for each(var x:XML in _menuDataprovider.descendants('UNIVERS'))
			{
				for each(var displ:DisplayObject in menu1.getChildren())
				{
					if(displ is CustomPFGPClassButtonMenu1)
					{
						if((displ as CustomPFGPClassButtonMenu1).DATAPROVIDER.@KEY == x.@KEY)
						{
							(displ as CustomPFGPClassButtonMenu1).DATAPROVIDER = x;
							break;
						}
					}
				}
			}
		}
	// construit le menu
		public function buildMenu():void
		{
			MENU_DATAPROVIDER = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
		}
	// gére quand le perimetre change	
		public function updatePerimetre():void
		{
			buildMenu();
		}
/* ------------------------------------------------------------------------
		FONCTION PRIVATE
-------------------------------------------------------------------------*/	
	// initialise le menu 1 
		private function initMenu1():void
		{
			resetMenu1();
			menu1.addChild(new CustomClassSeparatorMenu1IHM);
			if(_menuDataprovider != null)
			{
				for each(var x:XML in _menuDataprovider.descendants('UNIVERS'))
				{
					var btn:CustomPFGPClassButtonMenu1 = new CustomPFGPClassButtonMenu1();
					btn.styleName = "boutonmenu1Default";
					btn.DATAPROVIDER = x;
					btn.addEventListener(MouseEvent.CLICK,btn_clickHandler);
					btn.addEventListener(MouseEvent.ROLL_OVER,btn_rollOverHandler);
					menu1.addChild(btn);
					menu1.addChild(new CustomClassSeparatorMenu1IHM);
					if(!btn.boolUniversOnly && btn.selected)
					{
						initMenu2(x);
						afficherMenu2();
					}
				}
			}
		}
	// initialise le menu 2
		private function initMenu2(dataprovider:XML):void
		{
			resetMenu2();
			cacherMenu2();
			for each(var x:XML in dataprovider.descendants('FONCTION'))
			{
				var btn:CustomPFGPClassButtonMenu2 = new CustomPFGPClassButtonMenu2();
				btn.styleName = "boutonmenu2Default";
				btn.DATAPROVIDER = x;
				btn.addEventListener(MouseEvent.CLICK, btn2_clickHandler);
				//btn.addEventListener(MouseEvent.ROLL_OVER, btn2_rollOverHandler);
				menu2.addChild(btn);
			}
		}
	// rafraichit le status des composants du niveau 1
		private function refreshMenu1():void
		{
			var i:int = 0;
			for each(var xUnivers:XML in MENU_DATAPROVIDER.descendants('UNIVERS'))
			{
				(menu1.getChildAt(i*2+1) as CustomPFGPClassButtonMenu1).DATAPROVIDER = xUnivers;
				i++;
			}
		}
	// rafraichit le status des composants du niveau 1
		private function refreshMenu2():void
		{
			var i:int = 0;
			for each(var x:XML in MENU_DATAPROVIDER.descendants('UNIVERS').(@toggled == 'true').descendants('FONCTION'))
			{
				(menu2.getChildAt(i) as CustomPFGPClassButtonMenu2).DATAPROVIDER = x;
				i++;
			}
		}
	// détruis le menu 1
		private function resetMenu1():void
		{
			menu1.removeAllChildren();
		}
	// détruis le menu 2
		private function resetMenu2():void
		{
			if(menu2.getChildren().length > 0)
			{
				menu2.removeAllChildren();
				cacherMenu2();
			}
		}
	// gestion des effets d'affichage du menu 2	
		private function afficherMenu2():void
		{
			menu2.height= menu1.height;
			boolRollOverAvailable = false;
		}
	// gestion des effets de destruction du menu 2
		private function cacherMenu2():void
		{
			menu2.height= 3;
			boolRollOverAvailable = true;
		}
		
		
	/* ---------------------------------------------------------------------------------
		gestion du changement d'univers
	--------------------------------------------------------------------------------- */		
		public function changeUnivers(_dataprovider:XML):void 
		{
			CvAccessManager.changeModule(_dataprovider.@KEY);
		}
	/* ---------------------------------------------------------------------------------
		charge un module aprés : 
		- un clic sur un bouton de niveau 2
		- un clic sur un bouton de niveau 1 qui possède des enfants ( booluniversonly = false).
	--------------------------------------------------------------------------------- */		
		public function changeUniversFunction(_dataprovider:XML):void 
		{
			CvAccessManager.changeModule(_dataprovider.@KEY);
		}
/* ------------------------------------------------------------------------
		HANDLER
-------------------------------------------------------------------------*/
		protected function btnTEMClickHandler():void
		{
			var popup:PopupAideIHM = new PopupAideIHM();
			PopUpManager.addPopUp(popup,SystemManager.getSWFRoot(this),true);
			PopUpManager.centerPopUp(popup);
		}
		/* --------------------------------------------------------------------------
		Quand on clique sur le bouton du menu 1:
		- si le bouton est positionné sur un univers uniquement (boolUniversOnly = true), on affiche le module de l'univers concerné.
		- si le bouton est positionné sur un univers composé de fonction (boolUniversOnly = false), on affiche la seconde barre du menu et le module correspondant au premier élément enfant. 
		-------------------------------------------------------------------------- */		
		private function btn_clickHandler(event:MouseEvent):void
		{
			var btn:CustomPFGPClassButtonMenu1 = event.currentTarget as CustomPFGPClassButtonMenu1;
			setMenu1StyleToClick(btn);
			if(btn.boolUniversOnly)
			{
				changeUnivers(btn.DATAPROVIDER);
				resetMenu2();
				cacherMenu2();
			}
			else
			{
				initMenu2(btn.DATAPROVIDER);
				afficherMenu2();
				var dtprvdr:XML = (menu2.getChildAt(0) as CustomPFGPClassButtonMenu2).DATAPROVIDER;
				changeUniversFunction(dtprvdr);
			}
			refreshMenu1();
		}
		
		/* --------------------------------------------------------------------------
		Quand la souris survole le bouton du menu 1:
		- si le bouton est positionné sur un univers uniquement (boolUniversOnly = true), on en fait rien. 
		- si le bouton est positionné sur un univers composé de fonction (boolUniversOnly = false), on affiche la seconde barre du menu. 
		-------------------------------------------------------------------------- */		
		private function btn_rollOverHandler(event:MouseEvent):void
		{
			if(boolRollOverAvailable)
			{
				var btn:CustomPFGPClassButtonMenu1 = event.currentTarget as CustomPFGPClassButtonMenu1;
				setMenu1StyleToClick(btn);
			}
		}
		
		/* --------------------------------------------------------------------------
		Quand l'utilisateur clique le bouton du menu 2:
		- on affiche le module 
		-------------------------------------------------------------------------- */		
		private function btn2_clickHandler(event:MouseEvent):void
		{
			var btn:CustomPFGPClassButtonMenu2 = event.currentTarget as CustomPFGPClassButtonMenu2;
			setMenu2StyleToClick(btn);
			changeUniversFunction(btn.DATAPROVIDER);
		}
		/* --------------------------------------------------------------------------
		Quand l'utilisateur survole le bouton du menu 2:
		- on change le style du bouton
		-------------------------------------------------------------------------- */		
		private function btn2_rollOverHandler(event:MouseEvent):void
		{
			var btn:CustomPFGPClassButtonMenu2 = event.currentTarget as CustomPFGPClassButtonMenu2;
			setMenu2StyleToClick(btn);
		}
/* ------------------------------------------------------------------------
		STYLE
-------------------------------------------------------------------------*/			
		/* --------------------------------------------------------------------------
		l'utilisateur clique ou survole un bouton de menu1
		-------------------------------------------------------------------------- */	
		private function setMenu1StyleToClick(btn:CustomPFGPClassButtonMenu1):void
		{
			for each(var displ:DisplayObject in menu1.getChildren())
			{
				if(displ is CustomPFGPClassButtonMenu1)
					(displ as CustomPFGPClassButtonMenu1).styleName = "boutonmenu1Default";
			}	
			btn.styleName = "boutonmenu1click";
		}
		/* --------------------------------------------------------------------------
		l'utilisateur clique ou survole un bouton de menu2
		-------------------------------------------------------------------------- */	
		private function setMenu2StyleToClick(btn:CustomPFGPClassButtonMenu2):void
		{
			for each(var bt:CustomPFGPClassButtonMenu2 in menu2.getChildren())
				bt.styleName = "boutonmenu2Default";
			btn.styleName = "boutonmenu2click";
		}
/* ------------------------------------------------------------------------
		CONSTRUCTEUR	
-------------------------------------------------------------------------*/			
		public function MenuPFGPImpl()
		{
			super();
		}
/* ------------------------------------------------------------------------
		GETTER / SETTER	
-------------------------------------------------------------------------*/		
		public function set MENU_DATAPROVIDER(value:XML):void
		{
			_menuDataprovider = value;
			resetMenu2();
			initMenu1();
		}
		public function get MENU_DATAPROVIDER():XML
		{
			return _menuDataprovider;
			
		}
	}
}
