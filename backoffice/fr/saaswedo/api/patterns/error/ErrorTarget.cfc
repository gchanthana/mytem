<!--- Structures utilisée pour implémenter le fonctionnement :
Structure utilisée pour lister les instances IErrorHandler par code d'erreur : VARIABLES.ERROR_CODES {
	<errorCode>:[Liste des IErrorHandler pour errorCode]
}
Structure utilisée pour lister les codes d'erreur par instance IErrorHandler ie IErrorHandler.getId() : VARIABLES.ERROR_HANDLERS {
	<IErrorHandler.getId()>:{
		<errorCode>:TRUE;
	}
}
Le support de plusieurs traitements par code d'erreur implique la gestion de leur ordre d'exécution : Voir MAX_COUNT()
Si Séquentiel: Il faut décider pour le traitement suivant en cas d'exception durant le traitement qui le précède. Etc...
Si Parallèle: Il faut gérer la fin des threads et les variables partagées. Etc...
Dans tous les cas il faut aussi décider de la priorité entre le code d'erreur ERROR.ALL_ERRORS et les autres. Etc... --->
<cfcomponent author="Cedric" displayname="fr.saaswedo.api.patterns.error.ErrorTarget" extends="fr.saaswedo.api.patterns.cfc.Component"
implements="fr.saaswedo.api.patterns.error.IErrorTarget" hint="Implémentation IErrorTarget qui permet d'enregistrer une seule instance IErrorHandler
par code d'erreur (pour l'instant). <b>Si un traitement est enregistré pour le code d'erreur ERROR.ALL_ERRORS (Voir fr.saaswedo.api.patterns.const.Globals)
alors aucun autre ne sera enregistré par la suite.</b> La valeur de getId() n'a pas d'importance ici car les instances n'ont pas besoin d'etre
différenciées. Cela n'empeche pas à une instance IErrorHandler de sous-traiter séquentiellement plusieurs autres sous-traitements<br>
Une fonction interne effectue le traitement de toute exception capturée lors de l'exécution des traitements (Elle n'est pas à nouveau levée)">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.patterns.error.ErrorTarget">
		<cfreturn "fr.saaswedo.api.patterns.error.ErrorTarget">
	</cffunction>
	
	<cffunction access="public" name="hasErrorTypeHandler" returntype="Boolean" description="LOCK:ReadOnly">
		<cfargument name="errorCode" type="String" required="true" hint="Par défaut ERROR.ALL_ERRORS si non renseigné">
		<cfreturn arrayLen(getErrorHandlers(ARGUMENTS.errorCode)) GT 0>
	</cffunction>
	
	<!--- Le nombre max de traitements par code d'erreur est limité par MAX_COUNT(). Les structures suivantes sont créées par initInstance() --->
	<cffunction access="public" name="addErrorHandler" returntype="void" description="LOCK:Exclusive"
	hint="L'instance errorHandler n'est enregistrée que si la valeur de errorHandler.getId() n'est pas déjà enregistrée pour errorCode">
		<cfargument name="errorHandler" type="fr.saaswedo.api.patterns.error.IErrorHandler" required="true">
		<cfargument name="errorCode" type="String" required="true" hint="Par défaut ERROR.ALL_ERRORS si non renseigné">
		<cfset var handler=ARGUMENTS.errorHandler>
		<cfset var handlerId=handler.getId()>
		<cfset var code=getErrorCodeValue(ARGUMENTS.errorCode)>
		<!--- Condition de validité des paramètres --->
		<cfif (TRIM(code) NEQ "") AND (TRIM(handlerId) NEQ "")>
			<cftry>
				<cfset var log=GLOBALS().CONST("ErrorHandling.LOG")>
				<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
					<cfset var errorHandlers=VARIABLES.ERROR_HANDLERS>
					<!--- Ajout de l'identifiant de l'instance IErrorHandler --->
					<cfif NOT structKeyExists(errorHandlers,handlerId)>
						<cfset errorHandlers[handlerId]={}>
					</cfif>
					<!--- (Pour l'instant) L'enregistrement est fait si le traitement fourni n'est pas déjà enregistré pour ERROR.ALL_ERRORS
					C'est pour cette raison que l'implémentation hasErrorTypeHandler() de ce composant n'est pas utilisé dans cette fonction --->
					<cfif NOT structKeyExists(errorHandlers[handlerId],GLOBALS().CONST('ERROR.ALL_ERRORS'))>
						<!--- Vérification de l'existence de (IErrorHandler,Code d'erreur) --->
						<cfif NOT structKeyExists(errorHandlers[handlerId],code)>
							<cfset errorHandlers[handlerId][code]=TRUE>
							<!--- Ajout l'instance IErrorHandler dans la liste des traitements du code d'erreur --->
							<cfif NOT structKeyExists(VARIABLES.ERROR_CODES,code)>
								<cfset VARIABLES.ERROR_CODES[code]=[]>
							</cfif>
							<!--- Limitation : Au maximum MAX_COUNT() traitements IErrorHandler par code d'erreur --->
							<cfset var handleCount=arrayLen(VARIABLES.ERROR_CODES[code])>
							<cfif handleCount LT MAX_COUNT()>
								<cfset arrayAppend(VARIABLES.ERROR_CODES[code],handler)>
								<!--- Log content le nom complet de l'implémentation IErrorHandler --->
								<cfset var handlerClass="fr.saaswedo.api.patterns.error.IErrorHandler">
								<cfif isInstanceOf(handler,"fr.saaswedo.api.patterns.cfc.Component")>
									<cfset handlerClass=handler.componentName()>
								</cfif>
								<cflog type="information" text="[#log#] #componentName()#.addHandler(#code#,#handlerClass#) [ID:#handlerId#]">
							<cfelse>
								<cflog type="warning" text="[#log#] #componentName()# MAX add count reached (#handleCount#)">
							</cfif>
						</cfif>
					</cfif>
				</cflock>
				<cfcatch type="Any">
					<cfrethrow>
				</cfcatch>
			</cftry>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="dispatchError" returntype="void" hint="Appele la fonction handleError() de chaque traitement IErrorHandler.
	Le paramètre errorTarget fourni à handleError() correspond par défaut à la référence THIS sinon il peut etre précisé par l'implémentation
	qui appelle dispatchError(). L'exécution de chaque traitement est bloquante i.e cette fonction rend la main à la fin de l'exécution des traitements
	<br>Une fonction interne effectue le traitement de toute exception capturée lors de l'exécution des traitements (Elle n'est pas à nouveau levée).
	<br><b>Un log d'erreur est écrit si aucun traitement n'est enregistré pour traiter l'erreur. Mais aucune exception n'est levée</b>">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Exception (CFCATCH) contenant au moins les clés: message,detail,errorCode">
		<cfargument name="errorInfos" type="Struct" required="false" hint="Infos liées à l'exception">
		<cfset var infos=isDefined("ARGUMENTS.errorInfos") ? ARGUMENTS.errorInfos:{}>
		<!--- L'implémentation de dispatchError() est dispatchErrorAs() car elle permet de spécifier le paramètre errorTarget --->
		<cfset dispatchErrorAs(THIS,ARGUMENTS.cfCatchObject,infos)>
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->
	<cffunction access="private" name="dispatchErrorAs" returntype="void" hint="Implémentation utilisée pour le fonctionnement de dispatchError()
	<br>Le paramètre errorTarget sera fourni à la fonction handeError() du traitement qui est enregistré pour cfCatchObject.errorCode.
	<br>Toute exception capturée durant l'exécution du traitement est passée à onHandlerError() puis levée à nouveau">
		<cfargument name="errorTarget" type="fr.saaswedo.api.patterns.error.IErrorTarget" required="true"
		hint="Paramètre errorTarget qui sera fourni à IErrorhandler.handeError()">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Exception (CFCATCH) contenant au moins les clés: message,detail,errorCode">
		<cfargument name="errorInfos" type="Struct" required="false" hint="Infos liées à l'exception">
		<cfset var target=ARGUMENTS.errorTarget>
		<cfset var error=ARGUMENTS.cfCatchObject>
		<!--- Détermine la valeur du code d'erreur --->
		<cfset var errorCode=structKeyExists(error,"errorCode") ? error.errorCode:"">
		<cfset errorCode=getErrorCodeValue(errorCode)>
		<!--- Dispatch de l'erreur vers les instances IErrorHandler --->
		<cfif hasErrorTypeHandler(errorCode)>
			<cfset var infos=isDefined("ARGUMENTS.errorInfos") ? ARGUMENTS.errorInfos:{}>
			<cfset var errorHandlers=getErrorHandlers(errorCode)>
			<cfset var handlerCount=arrayLen(errorHandlers)>
			<!--- Le nombre max de traitements exécuté par code d'erreur est limité par MAX_COUNT() --->
			<cfset handlerCount=handlerCount LTE MAX_COUNT() ? handlerCount:MAX_COUNT()>
			<cfloop index="i" from="1" to="#handlerCount#">
				<cfset var errorHandler=errorHandlers[i]>
				<!--- Exécution d'un traitement --->
				<cftry>
					<cfset errorHandler.handleError(target,error,infos)>
					<!--- Toute exception capturée durant l'exécution du traitement est passée à onHandlerError() --->
					<cfcatch type="Any">
						<cfset onHandlerError(target,errorHandler,error,CFCATCH)>
						<!--- L'exception est ensuite à nouveau levée --->
						<cfrethrow>
					</cfcatch>
				</cftry>
			</cfloop>
		<!--- Traitement interne de l'erreur si aucune instance IErrorHandler --->
		<cfelse>
			<cfset ifNoHandlerFound(argumentCollection=ARGUMENTS)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="ifNoHandlerFound" returntype="void" hint="Appelée par dispatchErrorAs() si aucun traitement n'est trouvé.
	Ecrit un log avec le détail de l'erreur dans les logs puis lève une erreur ErrorHandling.NO_HANDLER correspondant à l'erreur cfCatchObject.<br>
	Il faut donc SOIT implémenter cette fonction SOIT la gérer par un TRY/CATCH. Attention un nouveau dispatch pourrait faire un boucle sans fin">
		<cfargument name="errorTarget" type="fr.saaswedo.api.patterns.error.IErrorTarget" required="true" hint="Paramètre fourni à dispatchErrorAs()">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Paramètre fourni à dispatchErrorAs()">
		<cfargument name="errorInfos" type="Struct" required="false" hint="Paramètre fourni à dispatchErrorAs()">
		<cfset var target=ARGUMENTS.errorTarget>
		<cfset var error=ARGUMENTS.cfCatchObject>
		<cfset var log=GLOBALS().CONST("ErrorHandling.LOG")>
		<cfset var noHandlerError=GLOBALS().CONST("ErrorHandling.NO_HANDLER")>
		<!--- Détermine la valeur du code d'erreur et le message qui sera écrit dans les logs --->
		<cfset var errorCode=structKeyExists(error,"errorCode") ? error.errorCode:"">
		<cfset errorCode=getErrorCodeValue(errorCode)>
		<cfset var msg=error.message & (error.detail NEQ "" ? " [#error.detail#]":"")>
		<!--- Extraction du fichier et la ligne concernée qui sont mentionés dans le contexte d'exécution --->
		<cfset var tagContext=[]>
		<cfif structKeyExists(error,"tagContext") AND isValid("Array",error.tagContext) AND (arrayLen(error.tagContext) GT 0)>
			<cfset tagContext=error.tagContext>
			<cfset msg=msg & " (#tagContext[1].TEMPLATE# line #tagContext[1].LINE#)">
		</cfif>
		<!--- Log d'avertissement contenant le message d'erreur --->
		<cflog type="warning" text="[#log#] #componentName()# has no handler for error #errorCode# : '#msg#'">
		<!--- Exception correspondant à cfCatchObject car aucun traitement trouvé --->
		<cfset var contextMsg=(arrayLen(tagContext) GT 0) ? " [Found in " & tagContext[1].TEMPLATE & " line " & tagContext[1].LINE & "]":"">
		<cfthrow type="Custom" errorCode="#noHandlerError#" message="Unhandled error '#errorCode#' : #error.message##contextMsg#" detail="#error.detail#">
	</cffunction>
	
	<!--- Toute exception qui est levée par cette fonction n'est pas forcément gérée (CFTRY/CFCATCH) par celui qui a appelé dispatchError() --->
	<cffunction access="private" name="onHandlerError" returntype="void" hint="Appelée par dispatchError() quand une exception
	est capturée lors de l'exécution d'un traitement. Ecrit un log avec le détail de l'erreur rencontrée durant l'exécution du traitement.<br>
	Si handledError.errorCode vaut ErrorHandling.NO_HANDLER: L'exception correspond à celle levée par l'implémentation ifNoHandlerFound() par défaut">
		<cfargument name="errorTarget" type="fr.saaswedo.api.patterns.error.IErrorTarget" required="true"
		hint="Paramètre errorTarget qui a été fourni à IErrorhandler.handeError(). Cette implémentation n'utilise pas ce paramètre">
		<cfargument name="errorHandler" type="fr.saaswedo.api.patterns.error.IErrorHandler" hint="Traitement dont l'exécution a levée une exception">
		<cfargument name="handledError" type="Any" required="true" hint="Exception qui a été l'objet du traitement (CFCATCH)">
		<cfargument name="cfCatchObject" type="Any" required="true" hint="Exception capturée lors de l'exécution du traitement (CFCATCH)">
		<cfset var log=GLOBALS().CONST("ErrorHandling.LOG")>
		<!--- Erreur qui a été l'objet du traitement --->
		<cfset var theHandledError=ARGUMENTS.handledError>
		<cfset var handledErrorCode=structKeyExists(theHandledError,"errorCode") ? theHandledError.errorCode:"">
		<cfset handledErrorCode=getErrorCodeValue(handledErrorCode)>
		<!--- Erreur rencontrée durant l'exécution du traitement --->
		<cfset var catchedError=ARGUMENTS.cfCatchObject>
		<cfset var catchedCode=structKeyExists(catchedError,"errorCode") ? catchedError.errorCode:"">
		<cfset catchedCode=getErrorCodeValue(catchedCode)>
		<!--- SOIT l'exécution du traitement est en erreur car il utilise l'implémentation par défaut ifNoHandlerFound() --->
		<cfif catchedCode EQ GLOBALS().CONST("ErrorHandling.NO_HANDLER")>
			<cfset var msg="catched default ifNoHandlerFound() error when handling '#handledErrorCode#'">
			<cfif isInstanceOf(ARGUMENTS.errorHandler,"fr.saaswedo.api.patterns.cfc.Component")>
				<cfset msg=msg & " with handler " & ARGUMENTS.errorHandler.componentName()>
			</cfif>
			<cflog type="error" text="[#log#] #componentName()# #msg#">
		<!---  SOIT il est en erreur en raison d'une autre une cause --->
		<cfelse>
			<cfset var msg="Cause : " & catchedError.message & (catchedError.detail NEQ "" ? " [#catchedError.detail#]":"")>
			<cfif structKeyExists(catchedError,"tagContext") AND (arrayLen(catchedError.tagContext) GT 0)>
				<cfset var catchedTemplate=catchedError["tagContext"][i]["TEMPLATE"] & " at " & catchedError["tagContext"][i]["LINE"]>
				<cfset msg=msg & " - Found in " & catchedTemplate>
			</cfif>
			<cflog type="error" text="[#log#] #componentName()# catched error #catchedCode# when handling '#handledErrorCode#' - #msg#">
		</cfif>
	</cffunction>
	
	<!--- Cette fonction est synchronisée (Threads) : ErrorDispatcher --->
	<cffunction access="private" name="getErrorHandlers" returntype="Array" description="LOCK:ReadOnly"
	hint="Retourne la liste des traitements IErrorHandler enregistrés pour errorCode">
		<cfargument name="errorCode" type="String" required="true" hint="Par défaut ERROR.ALL_ERRORS si non renseigné">
		<cfset var error=getErrorCodeValue(ARGUMENTS.errorCode)>
		<cfset var defaultErrorCode=GLOBALS().CONST('ERROR.ALL_ERRORS')>
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfset var errorCodes=VARIABLES.ERROR_CODES>
			<!--- Les traitements pour ERROR.ALL_ERRORS sont retournés en priorité quelque soit le code d'erreur fourni --->
			<cfif structKeyExists(errorCodes,defaultErrorCode)>
				<cfset error=defaultErrorCode>
			<cfelse>
				<!--- Si le code d'erreur fourni n'est pas traité : Aucun traitement --->
				<cfif NOT structKeyExists(errorCodes,error)>
					<cfreturn []>
				</cfif>
			</cfif>
			<cfreturn errorCodes[error]>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="getErrorCodeValue" returntype="String"
	hint="Retourne la valeur errorCode fournie sinon ERROR.ALL_ERRORS (Voir fr.saaswedo.api.patterns.const.Globals) si elle n'est pas renseignée">
		<cfargument name="errorCode" type="String" required="true" hint="Code d'erreur">
		<cfif TRIM(ARGUMENTS.errorCode) EQ "">
			<cfreturn GLOBALS().CONST('ERROR.ALL_ERRORS')>
		<cfelse>
			<cfreturn ARGUMENTS.errorCode>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="MAX_COUNT" returntype="Numeric" hint="Nombre maximum de traitements pouvant etre ajoutés avec addErrorHandler()">
		<cfreturn 1>
	</cffunction>
	
	<!--- Héritée de Component --->
	<cffunction access="private" name="initInstance" returntype="void"
	hint="Appele la fonction parent puis crée les structures utilisées pour gérer les instances IErrorHandler et les codes d'erreurs">
		<!--- Initialisation héritée de Component --->
		<cfset SUPER.initInstance()>
		<!--- Structure : Liste des instances IErrorHandler par code d'erreur --->
		<cfif NOT structKeyExists(VARIABLES,"ERROR_CODES")>
			<cfset VARIABLES.ERROR_CODES={}>
		</cfif>
		<!--- Structure : Liste des codes d'erreur par instance IErrorHandler ie IErrorHandler.getId() --->
		<cfif NOT structKeyExists(VARIABLES,"ERROR_HANDLERS")>
			<cfset VARIABLES.ERROR_HANDLERS={}>
		</cfif>
	</cffunction>
</cfcomponent>