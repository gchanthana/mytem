package univers.inventaire.commande.system
{
	public class TypesCommandesMobile
	{
		public static const NOUVELLES_LIGNES:Number = 1083;
		public static const MODIFICATION_OPTIONS:Number = 1282;
		public static const RENOUVELLEMENT:Number = 1382;
		public static const RESILIATION:Number = 1283;
		public static const EQUIPEMENTS_NUS:Number = 1182;
		
		public function TypesCommandesMobile()
		{
		}
	}
}