package rapportPilotage.service.poolsselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.vo.ValeurSelector;

	internal class PoolSelectorModel  extends EventDispatcher
	{
		private var _pools :ArrayCollection;
		
		public function PoolSelectorModel()
		{
			_pools = new ArrayCollection();	
		}
		public function get pools():ArrayCollection
		{
			return _pools;
		}
		internal function updateValues(value:ArrayCollection):void
		{	
			_pools.removeAll();// vider l'ArrayCollection
			var valeurSelector:ValeurSelector;
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_pools.addItem(valeurSelector); 
			}
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_POOL_SELECTOR_EVENT));
		}
	}
}