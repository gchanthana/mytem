<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.impl.HandlingService"
extends="fr.consotel.api.ibis.publisher.reporting.impl.RequestService" hint="Extension de RequestService contenant les méthodes permettant de :
- Instancier et accéder aux stratégies de diffusion et de traitement de fin
- Définition et récupération des credentials utilisés par le service en tant que propriétés du reporting (Sans limitation de taille pour la valeur du login/pwd)
- Récupérer les paramètres et propriétés concernant une exécution ou une planification
- Instancier et traiter les évènement correspondants aux étapes d'exécution ou de planification
- Traiter et dispatcher les évènements vers les stratégies de diffusion et de traitement de fin">
	<cffunction access="public" name="handleNotification" returntype="void"
	hint="Implémentation commune du traitement des notifications qui crée une notification à partir de celle reçue en paramètre avant de créer l'évènement
	Une clé SERVICE est ajoutée dans la notification qui sera utilisée pour créer l'évènement : Implémentation IReportingService qui va créer et dispatcher l'évènement 
	Si la notification est invalide par rapport à cette implémentation : Un log d'erreur est affiché puis une exception est levée et traitée (Non redéclenchée)">
		<cfargument name="notification" type="Struct"  required="true" hint="Structure contenant au moins les clés : JOBID (JOBID du JOB), JOBNAME (Nom du JOB)">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var getParamTick=getTickCount()>
		<cftry>
			<cfset logInfo(">> Starting Notification Handling on script : " & CGI.SCRIPT_NAME)>
			<cfif isValidNotification(notificationInfos)>
				<cfset var jobId=notificationInfos["JOBID"]>
				<cfset logInfo("Job [#jobId#] " & notificationInfos.JOBNAME & " notification handling from : " & CGI.REMOTE_HOST & "[" & CGI.HTTP_USER_AGENT & "]")>
				<!--- Création de la notification permettant de créer l'évènement IReportingEvent à partir du paramètre notification --->
				<cfset var builtNotificationTick=getTickCount()>
				<cfset var jobNotification=buildReportingNotification(notificationInfos)>
				<cfset logInfo("Job [#jobId#] " & notificationInfos.JOBNAME & " notification BUILT after " & ((getTickCount() - builtNotificationTick) / 1000) & "s")>
				<!--- Ajoute la clé SERVICE (Event Target) dans la notification : Implémentation IReportingService qui a créé et qui va dispatcher l'évènement --->
				<cfset jobNotification=addNotificationTarget(jobNotification)>
				<!--- Création de l'évènement IReportingEvent à partir de la notification obtenue --->
				<cfset var reportingEvent=createReportingEvent(jobNotification)>
				<cfset logInfo("Job [#jobId#] " & notificationInfos.JOBNAME & " notification HANDLED after " & ((getTickCount() - getParamTick) / 1000) & "s")>
				<!--- Dispatch de l'évènement vers la stratégie de diffusion correspondante (Event Listener) --->
				<cfset getParamTick=getTickCount()> 
				<cfset dispatchReportingEvent(reportingEvent)>
				<cfset logInfo("<< Job [#jobId#] " & notificationInfos.JOBNAME & " DELIVERY COMPLETED in " & ((getTickCount() - getParamTick) / 1000) & "s")>
			<!--- Traitement de toutes autres type de notifications : Aucun traitement et levée d'une exception --->
			<cfelse>
				<cfset logError("Invalid notification. Keys : " & structKeyList(notificationInfos,","))>
				<cfthrow attributecollection="#getErrorProperties()#" message="Traitement d'une notification invalide">
			</cfif>
			<!--- Traitement des exceptions capturées --->
			<cfcatch type="any">
				<cfset onNotificationHandlingException(notificationInfos,CFCATCH,getParamTick)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="dispatchReportingEvent" returntype="void"
	hint="La stratégie de diffusion indiquée par event.getDeliveryType() est instanciée et l'évènement event est passé à sa méthode handleReportingEvent()
	Les autres méthodes de l'interface IReportingEvent ne sont pas utilisées par cette implémentation. Toute exception capturée est redéclenchée">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true">
		<cfset var reportingEvent=ARGUMENTS["event"]>
		<cftry>
			<!--- Création de la stratégie de diffusion à partir de son type --->
			<cfset var deliveryType=reportingEvent.getDeliveryType()>
			<cfset var deliveryStrategy=createDelivery(deliveryType)>
			<!--- Exécution du handler de la stratégie de diffusion --->
			<cfset logInfo("Service dispatch : " & listLast(getMetadata(reportingEvent).NAME,".") & " => " & deliveryType)> 
			<cfset deliveryStrategy.handleReportingEvent(reportingEvent)>
			<!--- Traitement des exceptions capturées --->
			<cfcatch type="any">
				<cfset logError("Dispatch Reporting Event FAILED : " & CFCATCH.message)>
				<!--- Redéclenche l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="buildScheduleRequest" returntype="Struct"
	hint="Ajoute des propriétés supplémentaires avec la méthode addProperties() dans la structure retournée par l'implémentation de la classe parent.
	Ces propriétés sont celles qui sont retournées par getAuthProperties() et permettent de retrouver la valeur des credentials utilisés pour l'exécution du reporting
	Le préfixe fourni à la méthode addProperties() pour l'ajout de ces propriétés est la valeur retournée par RPREFIX()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true" hint="Reporting à exécuter">
		<cfargument name="deliveryStrategy" type="fr.consotel.api.ibis.publisher.handler.IDelivery" required="true" hint="Instance de la stratégie de diffusion">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var delivery=ARGUMENTS["deliveryStrategy"]>
		<cfset var xdoRequest=SUPER.buildScheduleRequest(reportingImpl,delivery)>
		<!--- Ajout des propriétés attributeTemplate,attributeFormat,attributeLocale,attributeTimeZone avec le préfixe RPREFIX() --->
		<cfset xdoRequest=addProperties(xdoRequest,getAuthProperties(reportingImpl,delivery),RPREFIX())>
		<cfreturn xdoRequest>
	</cffunction>

	<cffunction access="private" name="getAuthProperties" returntype="Struct"
	hint="Appelée par buildScheduleRequest() pour retourner des propriétés permettant à addNotificationTarget() de retrouver les credentials utilisés pour le reporting
	Cette implémentation retourne une structure contenant une clé AUTH_TOKEN associée à la valeur retournée par getServiceProperties().PWD au format héxadécimal
	La valeur héxadécimal est obtenue par le résultat final des opérations suivantes :
	- La valeur de getServiceProperties().PWD est encodée au format base64 avec l'encodage UTF-8 en utilisant toBase64()
	- Cette valeur au format base64 est encodée au format binaire avec toBinary()
	- Cette valeur au format binaire est encodée au format héxadécimal avec binaryEncode()
	- Cette valeur au format héxadécimal est associée à la clé AUTH_TOKEN
	Les propriétés retournées par cette méthode peuvent dépendre de l'implémentation concernée et du mode d'exécution utilisé pour générer le reporting">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true" hint="Reporting à exécuter">
		<cfargument name="deliveryStrategy" type="fr.consotel.api.ibis.publisher.handler.IDelivery" required="true" hint="Instance de la stratégie de diffusion">
		<cfset var pwdBase64=toBase64(getServiceProperties().PWD,"utf-8")>
		<cfset var pwdBinary=toBinary(pwdBase64)>
		<cfset var pwdHex=binaryEncode(pwdBinary,"HEX")>
		<cfreturn {AUTH_TOKEN=pwdHex}>
	</cffunction>

	<cffunction access="private" name="addNotificationTarget" returntype="Struct"
	hint="Appelée par handleNotification() juste avant d'instancier l'évènement IReportingEvent et lui permet de :
	- Retrouver la valeur des credentials utilisés pour générer le reporting à partir des propriétés qui ont été fournies par getAuthProperties() pour son exécution
	- Fournir ces credentials à une implémentation IReportingService similaire à celle utilisée pour exécuter le reporting
	- Ajouter une clé SERVICE correspondant à l'implémentation IReportingService dans la notification passée en paramètre
	Cette méthode est appelée avec en paramètre la notification utilisée pour l'instancier l'évènement IReportingEvent. Elle retourne cette notification avec la clé SERVICE
	Elle lève une exception et doit etre redéfinie par les classes dérivées pour adapter son implémentation en fonction du type d'exécution utilisé pour le reporting">
		<cfargument name="notification" type="Struct" required="true" hint="Notification utilisée par handleNotification() pour créer l'évènement de type IReportingEvent">
		<cfset logError("ABSTRACT IMPLEMENTATION ERROR from : " & getMetadata(THIS).NAME)>
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode abstraite qui doit etre implémentée par les classes dérivées">
	</cffunction>

	<cffunction access="private" name="buildReportingNotification" returntype="Struct"
	hint="Appelée par handleNotification() pour retourner la notification qu'il utilise pour créer l'évènement IReportingEvent.
	Appelle getJob() avec le paramètre notification pour obtenir les infos du JOB puis le passe à getJobParameters() pour obtenir les paramètres du JOB
	Les paramètres obtenus sont passés à splitParameters() pour retourner les paramètres identifiés selon leur type métier (JOB, REPORTING, XDO, DELIVERY) 
	Cette structure est modifiée pour y ajouter une clé JOB associée à la valeur initiale retournée par getJob() puis retournée comme résultat">
		<cfargument name="notification" type="Struct" required="true" hint="Provient de handleNotification() et est utilisée pour créer la notification qui est retournée">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var jobInfos=getJob(notificationInfos)>
		<!--- Construction de la notification à partir des paramètres provenant de getJobParameters --->
		<cfset var xdoParameters=getJobParameters(jobInfos)>
		<cfset var reportingNotification=splitParameters(xdoParameters)>
		<!--- Ajout de infos concernant le JOB provenant de getJob() --->
		<cfset structAppend(reportingNotification,{JOB=jobInfos},TRUE)>
		<cfreturn reportingNotification>
	</cffunction>

	<cffunction access="private" name="getJob" returntype="Struct"
	hint="Lève une exception. Appelée par buildReportingNotification() pour retourner une structure représentant les infos du JOB
	En principe cette méthode doit etre redéfinie par les classes dérivées pour adapter l'implémentation en fonction du type d'exécution utilisé pour le reporting">
		<cfargument name="notification" type="Struct" required="true" hint="Provient de buildReportingNotification() et validée avec isValidNotification()">
		<cfset logError("ABSTRACT IMPLEMENTATION ERROR from : " & getMetadata(THIS).NAME)>
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode abstraite qui doit etre implémentée par les classes dérivées">
	</cffunction>
	
	<cffunction access="private" name="getJobNotification" returntype="Struct"
	hint="Méthode spécifique à cette classe qui retourne une structure représentant les infos du JOB et contenant les clés :
	- JOBID : Valeur de la clé contenue dans notification
	- JOBNAME : Valeur de la clé contenue dans notification
	- STATUS : Valeur de getEvent().WARNING()
	- REPORT_URL : Chaine vide
	- MESSAGE : Chaine vide
	- USER : Valeur de getServiceProperties().USER (Login utilisateur BIP utilisé pour l'exécution ou la planification du rapport)
	- BIP : Valeur de getBipProperties().DNS (Par défaut : BIP ou DNS du serveur BIP)
	- SESSION_INTERVAL : Valeur de getBipProperties().SESSION_INTERVAL
	- BACKOFFICE : Valeur de la clé SERVER_NAME dans CGI
	- SCRIPT : Valeur de la clé SCRIPT_NAME dans CGI
	Cette méthode permet aux classes dérivées d'appeler directement cette implémentation et non la redéfinition de getJob()
	En principe elle ne doit pas etre redéfinie et sert de résultat de base pour une implémentation de getJob()">
		<cfargument name="notification" type="Struct" required="true" hint="Provient de buildReportingNotification() et validée avec isValidNotification()">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfreturn {
			JOBID=notificationInfos["JOBID"], JOBNAME=notificationInfos["JOBNAME"], STATUS=getEvent().WARNING(), REPORT_URL="", MESSAGE="",
			USER=getServiceProperties().USER, BIP=getBipProperties().DNS, BACKOFFICE=CGI.SERVER_NAME, SCRIPT=CGI.SCRIPT_NAME,
			SESSION_INTERVAL=getBipProperties().SESSION_INTERVAL
		}>
	</cffunction>

	<cffunction access="private" name="getJobParameters" returntype="Array" hint="Cette méthode n'est pas implémentée et lève une exception.
	Elle doit retourner les paramètres qui seront passés à splitParameters() par buildReportingNotification()">
		<cfargument name="notification" type="Struct" required="true" hint="Résultat de getJob() obtenu par buildReportingNotification()">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode abstraite qui doit etre implémentée par les classes dérivées">
	</cffunction>

	<cffunction access="private" name="isValidNotification" returntype="boolean" hint="Cette méthode n'est pas implémentée et lève une exception. 
	Elle est appelée par handleNotification() pour retourner TRUE si notification est une notification valide par rapport à cette implémentation">
		<cfargument name="notification" type="Struct"  required="true" hint="Notification à valider">
		<cfset logError("ABSTRACT IMPLEMENTATION ERROR from : " & getMetadata(THIS).NAME)>
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode abstraite qui doit etre implémentée par les classes dérivées">
	</cffunction>
	
	<cffunction access="private" name="isValidJobNotification" returntype="boolean"
	hint="Méthode spécifique à cette classe qui retourne TRUE si notification est une notification valide par rapport à cette implémentation et FALSE sinon
	La notification est valide lorsqu'elle contient au moins les clés JOBID (JOBID du JOB), JOBNAME (Nom du JOB)
	Cette méthode permet aux classes dérivées d'appeler directement cette implémentation et non la redéfinition de isValidNotification()
	En principe elle ne doit pas etre redéfinie et sert de résultat de base pour une implémentation de isValidNotification()">
		<cfargument name="notification" type="Struct"  required="true" hint="Notification à valider">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<!--- Vérification de la validité et du type de la notification --->
		<cfreturn structKeyExists(notificationInfos,"JOBID") AND structKeyExists(notificationInfos,"JOBNAME")>
	</cffunction>

	<cffunction access="private" name="createDelivery" returntype="fr.consotel.api.ibis.publisher.handler.IDelivery"
	hint="Instancie et retourne la stratégie de diffusion représentée par la concaténation getDeliveryApi().deliveryType
	Le type de stratégie de diffusion retourné par getDefaultDeliveryType() est instancié si le paramètre deliveryType est une chaine vide">
		<cfargument name="deliveryType" type="String" required="true" hint="Type de la stratégie de diffusion (Case Sensitive)">
		<!--- Type par défaut de la stratégie de diffusion --->
		<cfset var deliverySuffix=getDefaultDeliveryType()>
		<cfif LEN(TRIM(ARGUMENTS.deliveryType)) GT 0>
			<cfset var deliverySuffix=ARGUMENTS.deliveryType>
		</cfif>
		<cfreturn createObject("component",getDeliveryApi() & "." & deliverySuffix).createInstance()>
	</cffunction>

	<cffunction access="private" name="getDeliveryApi" returntype="String" hint="Retourne le package parent des stratégies de diffusion">
		<cfreturn "fr.consotel.api.ibis.publisher.delivery">
	</cffunction>
	
	<cffunction access="private" name="getDefaultDeliveryType" returntype="String" hint="Retourne le type par défaut de la stratégie de diffusion">
		<cfreturn "Delivery">
	</cffunction>
	
	<cffunction access="private" name="createReportingEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"
	hint="Appelée par handleNotification() pour créer et retourner l'évènement IReportingEvent à partir de la notification obtenue de buildReportingNotification()">
		<cfargument name="notification" type="Struct" required="true" hint="Structure obtenue avec buildReportingNotification()">
		<cfreturn createObject("component",getEventApi() & "." & getEventType()).createReportingEvent(ARGUMENTS["notification"])>
	</cffunction>

	<cffunction access="private" name="getEventApi" returntype="String" hint="Retourne le package des évènements (INotificationEvent, IReportingEvent)">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event">
	</cffunction>
	
	<cffunction access="private" name="getEventType" returntype="String" hint="Retourne le type par défaut de l'implémentation IReportingEvent">
		<cfreturn "ReportingEvent">
	</cffunction>
	
	<cffunction access="private" name="getEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"
	hint="Retourne l'implémentation IReportingEvent utilisée par ce service. Utilisé pour les valeurs du statut : SUCCESS(), FAILURE(), etc...">
		<cfreturn getServiceProperties().REPORTING_EVENT>
	</cffunction>
	
	<cffunction access="private" name="setServiceProperties" returntype="void"
	hint="Appelle la méthode de la classe parent et instancie les stratégies de diffusion et de traitement de fin utilisées par défaut
	Définit une propriété accessible par getServiceProperties().NOTIFICATION_EVENT : Instance générique de l'évènement de notification">
		<cfset SUPER.setServiceProperties()>
		<!--- Evènement utilisé par le service --->
		<cfset VARIABLES.SERVICE.REPORTING_EVENT=createReportingEvent({})>
	</cffunction>
	
	<!--- TODO : Prochaine version
		- Supprimer la création d'une notification à partir de l'exception exceptionObj
		- Remplacer cette méthode par un ensemble de handlers en fonction du type de notification identifiés par CFCATCH.errorCode
	--->
	<cffunction access="private" name="onNotificationHandlingException" returntype="void" hint="Appelée par handleNotification() pour traiter toute exception capturée">
		<cfargument name="notification" type="Struct"  required="true" hint="Notification passée à handleNotification()">
		<cfargument name="exceptionObj" type="any"  required="true" hint="Exception CFCATCH capturée par handleNotification()">
		<cfargument name="duration" type="numeric"  required="true" hint="Permet de calculer et afficher la durée écoulée par soustraction avec getTickCount()">
		<cfset var notificationInfos=ARGUMENTS["notification"]>
		<cfset var exceptionInfos=ARGUMENTS["exceptionObj"]>
		<cfset var notifType=ARGUMENTS["notificationType"]>
		<cfset var getParamTick=ARGUMENTS["duration"]>
		<cftry>
			<!--- Récupération du JOBID et JOBNAME s'ils sont définis. La méthode isValidNotification() pouvant etre la cause de l'exception --->
			<cfif structKeyExists(notificationInfos,"JOBID") AND structKeyExists(notificationInfos,"JOBNAME")>
				<cfset var jobId=notificationInfos["JOBID"]>
				<cfset var jobName=notificationInfos["JOBNAME"]>
				<cfset logError("<< Job [#jobId#] " & jobName & " handling FAILED after " & ((getTickCount() - getParamTick) / 1000) & "s")>
			<cfelse>
				<cfset logError("<< Notification handling FAILED after " & ((getTickCount() - getParamTick) / 1000) & "s")>
			</cfif>
			<cfset logError("Handling exception message : " & exceptionInfos.message)>
			<cfset handleException(exceptionInfos,"Echec du traitement de la notification du reporting")>
			<!--- La notification reçue en paramètre est passée au traitement de l'exception --->
			<cfcatch type="any">
				<cfset logError("Handling exception in onNotificationHandlingException() FAILED : " & CFCATCH.message)>
				<cfset handleException(CFCATCH,"Echec du traitement interne onNotificationHandlingException()")>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>