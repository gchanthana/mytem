<cfcomponent extends="portail.class.specialClass.AbstractComposant"
			 implements="portail.class.specialClass.InterfaceComposant"
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>
	
	
	<!--- AFFICHE LE COMPOSANT DE LOGIN --->
	<cffunction name="getIndexLogin" access="public" output="false" returntype="string"
				displayname="" 
				hint="Génère la div de Login / template D1" >
		<cfargument name="btnCliked" required="true" type="numeric" >
		<cfargument name="varCnx" required="true" type="numeric" >
		<cfargument name="sous_tete" required="false" type="string" >
		<cfargument name="type" required="false" type="string" >
		

		<cfsilent>
			<!--- Param FORM values. --->
		    <cfparam name="FORM.captchaCodeForget" type="string" default="" />
		    <cfparam name="FORM.captchaFirstCnx" type="string" default="" />
		    <cfparam name="FORM.captcha_checkSMS" type="string" default="" />
		    <cfparam name="FORM.captcha_checkFirstCnx" type="string" default="" />
		    
		    <cftry>
		    	<cfparam name="FORM.submitted" type="numeric" default="0" />
		    <cfcatch>
		    	<cfset FORM.submitted = 0 />
		    </cfcatch>
		    </cftry>
		</cfsilent>
		
		
		<cfset confirm = "">
		<cfset error = "" >
		<cfset errorLigneRetour = "">
		<cfset errorCaptchaCodeForget = "" >
		<cfset errorCaptchaFirstCnx = "" >
		<cfset errorValidTel = "">
		<cfset errorValidCode = "">
		
		<cfset spanCnx = "visibility:visible;">
		<cfset codeForgetVisibleStyle = "visibility:hidden;">
		<cfset SMSSendVisibleStyle = "visibility:hidden;">
		<cfset firstCnxVisibleStyle = "visibility:hidden;">
		<cfset errorValidCodeVisibleStyle = "visibility:hidden;">
		<cfset errorValidTelVisibleStyle = "visibility:hidden;">
		
		<cfset numTel = "">
		<cfset email = "">
		<cfset telFirstCnx = "">
		<cfset nom = "">
		<cfset prenom = "">
		<cfset civilite = "">
		<cfset selected1 = "">
		<cfset selected2 = "">
		<cfset selected3 = "">
		<cfset SESSION.strEmail = "">
		<cfset hideBtn = false >
		
		
		<cfif btnCliked eq 1><!--- BOUTON DE VALIDATION DE FIRST CONNEXION --->
			<cftry >
				<!--- decriptage du captcha_check --->
				<cfset strCaptchaFirstCnx = Decrypt(FORM.captcha_checkFirstCnx,"bots-aint-sexy", "CFMX_COMPAT", "HEX") />
									
				<cfset firstCnxVisibleStyle = "visibility:visible;">
				<cfset email = FORM.email >
				<cfset telFirstCnx = ReReplace(FORM.telFirstCnx, "[-. ]","","ALL")>
				<cfset nom = FORM.nom >
				<cfset prenom = FORM.prenom >
				<cfset civilite = FORM.civilite >
				
				<cfset SESSION.FIRSTCO.strMail = FORM.email ><!--- variable string email qui peut etre renvoyé par mail --->
				<cfset SESSION.FIRSTCO.strTelFirstCnx = ReReplace(FORM.telFirstCnx, "[-. ]","","ALL") ><!--- variable string telFirstCnx qui peut etre renvoyé par mail --->
				<cfset SESSION.FIRSTCO.strNom = FORM.nom ><!--- variable string nom qui peut etre renvoyé par mail --->
				<cfset SESSION.FIRSTCO.strPrenom = FORM.prenom ><!--- variable string prenom qui peut etre renvoyé par mail --->
				<cfset SESSION.FIRSTCO.strCivilite = FORM.civilite ><!--- variable string civilite qui peut etre renvoyé par mail --->
				
				<cfif FORM.civilite eq "1">
					<cfset selected1 = "selected=""selected""">
				<cfelseif FORM.civilite eq "2">
					<cfset selected2 = "selected=""selected""">
				<cfelseif FORM.civilite eq "3">
					<cfset selected3 = "selected=""selected""">
				</cfif>
				
				
				<cfif (strCaptchaFirstCnx EQ FORM.captchaFirstCnx)><!--- popup premiere connexion, SMS envoyé pour validation --->
				
					<!--- envoi email pour validation --->
					<cfset valRetour = createObject("component","portail.class.classData.MailService").emailFirstCnxValidation(civilite,nom,prenom,telFirstCnx,email,btnCliked) >
					
					<cfif valRetour eq "1"><!--- si la fiche annuaire vient d'être créee --->
					
						<cfset errorCaptchaFirstCnx = ""><!--- suppression de l'erreur captcha --->
					    <!--- display de ce qui doit etre affiché et fermé --->
						<cfset firstCnxVisibleStyle = "visibility:hidden;">
						<cfset spanCnx = "visibility:hidden;">
						<cfset SMSSendVisibleStyle = "visibility:visible;">
						
						<!--- affichage message de confimation d'envoi de mail --->
						<cfset confirm = "Merci. Vos informations sont bien enregistrées. Un mail vous a été envoyé. 
								Vous devez le valider afin de recevoir votre code personnel. 
								Il vous sera envoyé par SMS sur votre téléphone dès validation du mail.<br />
								Vérifiez que le mail envoyé n'a pas été classé automatiquement par votre boite mail dans les spams.">
								
					<cfelse> <!--- si erreur dans enregistrement de la fiche annuaire --->
					
						<cfset codeForgetVisibleStyle = "visibility:hidden;">
						<cfset spanCnx = "visibility:hidden;">
						<cfif valRetour eq -1 >
							<cfset errorLigneRetour = "Autre erreur">
						<cfelseif valRetour eq -2>
							<cfset errorLigneRetour = "La ligne n'existe pas">
						<cfelseif valRetour eq -3>
							<cfset errorLigneRetour = "Le client associé à la ligne n'a pas accès au Portail">
						<cfelseif valRetour eq -4>
							<cfset errorLigneRetour = "La ligne existe déjà">
						</cfif>
					</cfif>
				    
							
				<cfelse><!--- ressaisir le code de securité --->
					
					<cfset codeForgetVisibleStyle = "visibility:hidden;">
					<cfset spanCnx = "visibility:hidden;">
					<cfset errorCaptchaFirstCnx = "Resaisir le code de sécurité">
					
				</cfif>
			
			<cfcatch>
			    <!--- ENVOI DE MAIL D'ERREUR --->
				<cfmail from="ReportError@portail.fr" to="joan.cirencien@saaswedo.com" subject="ERREUR first cnx" type="text/html" server="mail.consotel.fr" port="26" charset="utf-8" > 
					<cfdump var="#SESSION#" label="SESSION ERROR (CFCATCH)">
					<cfdump var="#cfcatch#">
				</cfmail>
		    </cfcatch>
		    </cftry>
		</cfif>
		
		
		<cfif btnCliked eq 2><!--- BOUTON DE VALIDATION DE CODE PERSO OUBLIÉ --->

			<cftry>
				<!--- Decrypt the check value --->
				<cfset strCaptchaSMS = Decrypt(FORM.captcha_checkSMS,"bots-aint-sexy", "CFMX_COMPAT", "HEX") />
				<!--- besoin du numero de telephone pour envoyer le SMS --->
				<cfset numTel =  FORM.cloneTel>
				
				<cfif (strCaptchaSMS EQ FORM.captchaCodeForget)><!--- popup code perso oublié, code envoyé par SMS --->
					<!--- The user entered the correct text. Set the flag for future use. --->
				    <cfset errorCaptchaCodeForget = "">
					
					<!--- supprime tout les carcatères non désirés de la string --->
					<cfset numeroTel = ReReplace(FORM.cloneTel, "[-. ]","","ALL") >
					
					<cfset hideBtn = true > <!---  cache le bouton renvoyer le mail qui est inutile dans la popup de confirmation de code envoyer --->
				    <cfinclude template="/portail/SendSMS.cfm"> <!--- API SMS POUR ENVOI SMS --->
				    <cfset returnSend = true>
				    
				    <!--- si le SMS a bien été envoyé --->
				    <cfif returnSend eq true>
						<cfset codeForgetVisibleStyle = "visibility:hidden;">
						<cfset spanCnx = "visibility:hidden;">
						<cfset SMSSendVisibleStyle = "visibility:visible;">
						<cfset confirm = "Merci. Votre code personnel vous a été adressé par SMS">
					</cfif>
				
				<cfelse><!--- RESSAISIR SON CAPTCHA --->
				
						<cfset codeForgetVisibleStyle = "visibility:visible;">
						<cfset spanCnx = "visibility:hidden;">
						<cfset errorCaptchaCodeForget = "Resaisissez le code de sécurité">
				</cfif>
			
			<cfcatch>
			    <!--- ENVOI DE MAIL D'ERREUR --->
				<cfmail from="ReportError@portail.fr" to="joan.cirencien@saaswedo.com" subject="ERREUR code perso oublié" type="text/html" server="mail.consotel.fr" port="26" charset="utf-8" > 
					<cfdump var="#SESSION#" label="SESSION ERROR (CFCATCH)">
					<cfdump var="#cfcatch#">
				</cfmail>
		    </cfcatch>
		    </cftry>
		</cfif>
		
		
		<!--- Create the array of valid characters. Leave out the numbers 0 (zero) and 1 (one) as they can be easily confused with the characters o and l (respectively). --->
	    <cfset arrValidCharsFirstCnx = ListToArray(
	    "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z," &
	    "2,3,4,5,6,7,8,9"
	    ) />
	    <cfset arrValidCharsSMS = ListToArray(
	    "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z," &
	    "2,3,4,5,6,7,8,9"
	    ) />
		
	    <!--- melange du tableau --->
	    <cfset CreateObject("java", "java.util.Collections" ).Shuffle(arrValidCharsFirstCnx) />
		<cfset CreateObject("java", "java.util.Collections" ).Shuffle(arrValidCharsSMS) />
	     
	    <!--- Now that we have a shuffled array, let's grab the first 8 characters as our CAPTCHA text string. --->
	    <cfset strCaptchaFirstCnx = (
	    arrValidCharsFirstCnx[ 1 ] & arrValidCharsFirstCnx[ 2 ] & arrValidCharsFirstCnx[ 3 ] & arrValidCharsFirstCnx[ 4 ]
	    ) />
	    
	    <cfset strCaptchaSMS = (
	    arrValidCharsSMS[ 1 ] & arrValidCharsSMS[ 2 ] & arrValidCharsSMS[ 3 ] & arrValidCharsSMS[ 4 ]
	    ) />
		
		
	    <!--- At this point, we have picked out the CAPTCHA string that we want the users to ender. However, we don't want to pass that text anywhere in the form otherwise
	    a spider might be able to scrape it. Thefefore, we now want to encrypt this value into our check field. --->
	    <cfset FORM.captcha_checkSMS = Encrypt(strCaptchaSMS, "bots-aint-sexy","CFMX_COMPAT", "HEX") />
		<cfset FORM.captcha_checkFirstCnx = Encrypt(strCaptchaFirstCnx, "bots-aint-sexy","CFMX_COMPAT", "HEX") />
		
		
		<cfif btnCliked eq 3><!--- BOUTON DE VALIDATION CONNEXION AU PORTAIL --->
		
			<cftry>
				<cfif varCnx eq -1 >
					<!--- display de ce qui doit etre affiché et fermé --->
					<cfset errorValidTelVisibleStyle = "visibility:visible;">
					<cfset errorValidCodeVisibleStyle = "visibility:hidden;">
					<!--- affichage message de confimation d'envoi de mail --->
					<cfset errorValidTel = "Votre numéro / votre email n'est pas reconnu dans l'annuaire de votre entreprise, veuillez prendre contact avec votre gestionnaire télécom.">
				</cfif>
			    <cfif varCnx eq -2 >
					<!--- display de ce qui doit etre affiché et fermé --->
					<cfset errorValidCodeVisibleStyle = "visibility:visible;">
					<cfset errorValidTelVisibleStyle = "visibility:hidden;">
					<!--- affichage message de confimation d'envoi de mail --->
					<cfset errorValidCode = "Le mot de passe / le code saisi n'est pas reconnu.">
				</cfif>
			
			<cfcatch>
			    <!--- ENVOI DE MAIL D'ERREUR --->
				<cfmail from="ReportError@portail.fr" to="joan.cirencien@saaswedo.com" subject="ERREUR code perso oublié" type="text/html" server="mail.consotel.fr" port="26" charset="utf-8" > 
					<cfdump var="#SESSION#" label="SESSION ERROR (CFCATCH)">
					<cfdump var="#cfcatch#">
				</cfmail>
		    </cfcatch>
		    </cftry>
		</cfif>
		
		
		<cfif btnCliked eq 4><!--- BOUTON RENVOI DE MAIL --->
		
			<cftry>
				<!--- envoi email pour validation --->
				<cfset indRetour = createObject("component","portail.class.classData.MailService").emailFirstCnxValidation(SESSION.FIRSTCO.strCivilite,SESSION.FIRSTCO.strNom,SESSION.FIRSTCO.strPrenom,SESSION.FIRSTCO.strTelFirstCnx,SESSION.FIRSTCO.strMail,btnCliked) >
				
				<cfset errorCaptchaFirstCnx = ""><!--- suppression de l'erreur captcha --->
			    <!--- display de ce qui doit etre affiché et fermé --->
				<!--- <cfset firstCnxVisibleStyle = "visibility:hidden;"> --->
				<cfset spanCnx = "visibility:hidden;">
				<cfset SMSSendVisibleStyle = "visibility:visible;">
				
				<!--- affichage message de confimation d'envoi de mail --->
				<cfset confirm = "Un mail vous a été envoyé.
						Vous devez le valider afin de recevoir votre code personnel. 
						Il vous sera envoyé par SMS sur votre téléphone dès validation du mail.<br />
						Vérifiez que le mail envoyé n'a pas été classé automatiquement par votre boite mail dans les spams.">
				
			<cfcatch>
			    <!--- ENVOI DE MAIL D'ERREUR --->
				<cfmail from="ReportError@portail.fr" to="joan.cirencien@saaswedo.com" subject="ERREUR renvoi de mail" type="text/html" server="mail.consotel.fr" port="26" charset="utf-8" > 
					<cfdump var="#SESSION#" label="SESSION ERROR (CFCATCH)">
					<cfdump var="#cfcatch#">
				</cfmail>
		    </cfcatch>
		    </cftry>
		</cfif>
		
		
		<cfif btnCliked eq 5 ><!--- CLIC SUR LIEN DU MAIL DE CONFIRMATION --->

			<cftry>
				<cfset numeroTel = sous_tete> <!--- pour page SMSSend.cfm --->
				<cfset numTel = sous_tete> <!--- pour affichage dans input "telimail" --->
				
				<cfinclude template="/portail/SendSMS.cfm"> <!--- page d'envoi de SMS --->
			<cfcatch>
			    <!--- ENVOI DE MAIL D'ERREUR --->
				<cfmail from="ReportError@portail.fr" to="joan.cirencien@saaswedo.com" subject="ERREUR clic lien du mail de confirmation" type="text/html" server="mail.consotel.fr" port="26" charset="utf-8" > 
					<cfdump var="#SESSION#" label="SESSION ERROR (CFCATCH)">
					<cfdump var="#cfcatch#">
				</cfmail>
		    </cfcatch>
		    </cftry>
		</cfif>
		
		
		<!--- CAPTCHA PREMIER CONNEXION --->
		<cfsavecontent variable="imgCaptcha1"> 
			<cfimage action="captcha" height="43" width="93" text="#strCaptchaFirstCnx#" difficulty="low" fonts="verdana,arial,times new roman,courier" fontsize="18"/>
		</cfsavecontent>
		<cfsavecontent variable="imgCaptcha2"> 
			<cfimage action="captcha" height="43" width="93" text="#strCaptchaSMS#" difficulty="low" fonts="verdana,arial,times new roman,courier" fontsize="18"/>
		</cfsavecontent>
		
		
		<!--- COMPOSANT GRAPHIQUE --->
		<cfset tableLogin =	'
				<div id="divLogin" style="">
					<div id="headder" style="">
						<img align="right" src="img/mytem/saaswedo_papieraentete.jpg" alt="Logo SaasWeDo" /> <!--- width="360px" height="70px" --->
						<!--- <div class="headderDrapeau" align="right"></div>
						<div class="headderEmpty"></div>
						<div class="spacer"></div>
						<div class="headderLogo"></div> --->
					</div>
					<div id="bbody" style="">
						<div class="bbodyImgNomProduit">
							<div class="bbodyLogo" >&nbsp;</div>
							<div class="bbodyNoLogo">&nbsp;</div>
							<div class="spacer"></div>
						</div>
						
						
						<div class="bbodyDivTableAndImgLogin">
							<div class="bbodyDivLoginAndTextCNIL">
								<div class="bbodyLogin" >
									<form method="post" action="index.cfm" id="connexion">
										<input type="hidden" name="idPage" value="0">
										<table class="tableLogintest" cellpadding="0" cellspacing="0" border="0">
											<colgroup>
												<col width="225px"></col>
												<col width="170px"></col>
												<col width="140px"></col>
											</colgroup>
											<tbody>
												<tr style="height:10px;">
													<td align="center" colspan="3">&nbsp;<span id="errorTel" class="tdIndentErrorLogin">Numéro manquant ou invalide</span>#error#</td>
												</tr>
												<tr>
													<td></td>
													<td colspan="2" style="text-indent:10px;font-size:9px;font-style:italic;"><span class="red">*</span>&nbsp;champs obligatoire</td>
												</tr>
												<tr>
													<td align="right" class="inputLongueur" style="">Email ou n° de mobile <span class="asteriskSmall">**</span> : <span class="asteriskSmallObligatoire">*</span></td>
													<td align="right" class="inputPadding" style=""><input type="text" onfocus="cloneTel();" onkeydown="cloneTel();" onkeyup="cloneTel();"  class="inputTel" name="telimail" id="telimail" value="#numTel#"></td>
													<td >&nbsp;</td>
												</tr>
												<tr>
													<td align="right" class="inputLongueur" style="border:0px solid red;">Mot de passe ou code personnel : <span class="asteriskSmallObligatoire">*</span></td>
													<td align="right" class="inputPadding" style="border:0px solid red;" ><input type="password" class="inputPwd" name="pwd" id="pwd" value="" ></td>
													<td class="inputPadding" style="" align="center" >&nbsp;<span id="spanCnx" style="#spanCnx#"><input type="submit" name="cnx" onclick="if(verificationLog()){loadsablier();}else{return false;}" value="Connexion" style="width:100px;"></span></td>
												</tr>
												<tr style="height:10px;">
									              <td align="" style="height:10px;border:0px solid red;">&nbsp;</td>
									              <td align="" style="border:0px solid red;">&nbsp;</td>
									              <td align="" style="border:0px solid red;">&nbsp;</td>
									            </tr>
									            <tr style="">
									              <td colspan="2" align="right" style="border:0px solid red;">
										              <input type="button" onclick="openPopFirstCnx();" name="btnFirstCnx" id="btnFirstCnx" value="Première connexion" style="width:160px;">
										              &nbsp;&nbsp;
										              <input type="button" onclick="OpenPopCodeForget();" name="btnCodeForget" id="btnCodeForget" value="Code personnel oublié" style="width:160px;">
												 </td>
									              <td style="">&nbsp;</td>
									            </tr>
												<tr>
													<td colspan="3" style="font-size:9px;font-style:italic;padding:10px;">**&nbsp;La connexion par l''email permet de visualiser toutes les lignes rattachées / La connexion par le n° de
														<span style="display:inline-block;text-indent:14px;">mobile ne ramène que les infos de la ligne</span></td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
								
								<!--- div conteneur des DIVS à superposer --->
								<div class="divSuperpose" id="superpose" style="">
									
									<!--- div first connexion --->
									<div class="popupIndex" id="firstCoPop" style="#firstCnxVisibleStyle#">
										<form action="index.cfm" method="post" id="firstCo">
											<input type="hidden" name="submitted" id="submitted" value="1" />
											<input type="hidden" name="captcha_checkFirstCnx" id="captcha_checkFirstCnx" value="#FORM.captcha_checkFirstCnx#" />
											<table class="tableFirstCnx" cellspading="0" cellspacing="0" width="100%">
												<colgroup>
													<col width="235px"></col>
													<col width="170px"></col>
													<col width="140px"></col>
												</colgroup>
												<tr>
													<td align="center"></td>
													<td align="center" style="font-size:13px;text-indent:0px;" colspan="1">Première connexion ?</td>
													<td align="right" ><a style="cursor:pointer" onclick="closePopFirstCo();"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td align="right" class="inputLongueur" style="">Adresse email : <span class="asteriskSmallObligatoire">*</span></td>
													<td class="inputPadding" style=""><input type="text" class="inputTel" name="email" id="email" value="#email#"></td>
													<td >&nbsp;</td>
												</tr>
												<tr style="" align="center">
													<td colspan="3"  style=""><span id="errorEmail" class="tdIndentErrorMail" style="display:none;color:red;">champs vide ou syntaxe invalide</span></td>
												</tr>
												<tr>
													<td align="right" class="inputLongueur" style="border:0px solid red;">N° de mobile : <span class="asteriskSmallObligatoire">*</span></td>
													<td class="inputPadding" style="border:0px solid red;"><input type="text" class="inputTel" name="telFirstCnx" id="telFirstCnx" value="#telFirstCnx#"></td>
													<td >&nbsp;</td>
												</tr>
												<tr style="" align="center">
													<td colspan="3"><span id="errorLigneRetour" class="tdIndentErrorLigneRetour">#errorLigneRetour#</span><span id="errorTelFirstCnx" class="tdIndentErrorNumMobile" style="display:none;color:red;">Numéro manquant ou invalide</span></td>
												</tr>
												<tr>
													<td align="right" class="inputLongueur" style="">Civilité : <span class="asteriskSmallObligatoire">*</span></td>
													<td class="inputPadding" style="">
														<select name="civilite" id="civilite" style="width:170">
															<option value="0">- choix -</option>
															<option #selected1# value="1">Monsieur</option>
															<option #selected2# value="2">Madame</option>
															<option #selected3# value="3">Mademoiselle</option>
														</select>
													</td>
													<td >&nbsp;</td>
												</tr>
												<tr style="" align="center">
													<td colspan="3" style=""><span id="errorCivilite"  class="tdIndentErrorCivilite red">champs à saisir</span></td>
												</tr>
												<tr>
													<td align="right" class="inputLongueur" style="">Nom : <span class="asteriskSmallObligatoire">*</span></td>
													<td class="inputPadding" style=""><input type="text" onfocus="" class="inputTel" name="nom" id="nom" value="#nom#"></td>
													<td >&nbsp;</td>
												</tr>
												<tr style="" align="center">
													<td colspan="3" style=""><span id="errorNom" class="tdIndentErrorNom">champs à saisir</span></td>
												</tr>
												<tr>
													<td align="right" class="inputLongueur" style="">Prénom : <span class="asteriskSmallObligatoire">*</span></td>
													<td class="inputPadding" style=""><input type="text" onfocus="" class="inputTel" name="prenom" id="prenom" value="#prenom#"></td>
													<td >&nbsp;</td>
												</tr>
												<tr style="" align="center">
													<td colspan="3" style=""><span id="errorPrenom" class="tdIndentErrorPrenom">champs à saisir</span></td>
												</tr>
												<tr>
													<td align="right" style="">#imgCaptcha1#<span class="asteriskSmallObligatoire">*</span></td>
													<td class="tdCaptcha" style="">&nbsp;<input class="inputTel" onfocus="" type="text" name="captchaFirstCnx" id="captchaFirstCnx" value="" /><br>
													<span id="errorCaptchaFirstCnxBis" class="tdIndentErrorCaptcha" style="">#errorCaptchaFirstCnx#</span>&nbsp;<span align="center" id="errorCaptchaFirstCnx" style="text-indent:40px;display:none;">champs à saisir</span></td>
													<td align="" style="">&nbsp;</td>
												</tr>
												<tr style="height:0px;">
									              <td style="text-indent:10px;font-size:9px;font-style:italic;"><span class="red">*</span>&nbsp;champs obligatoire</td>
									              <td align="" style="">&nbsp;</td>
									              <td align="" style="text-indent:20px;"><input type="submit" name="firstCnx" id="firstCnx" onclick="javascript:if(verificationFirstCo()){ document.forms[''firstCo''].submit();loadsablier(); }else{ return false;}" value="Enregistrer" style=""></td>
									            </tr>
											</table>
										</form>
									</div>
									
									<!--- popup code oublie --->
									<div class="popupIndex2" id="codeOubliePop" style="#codeForgetVisibleStyle#">
										<form action="index.cfm" method="post" id="codeOublie">
										
											<!--- This is the hidden field that will flag form submission for data validation. --->
										    <input type="hidden" name="submitted" id="submitted" value="1" />
										    <!--- This is an encrypted field so that spiders / bots cannot use it to their advantage. --->
										    <input type="hidden" name="captcha_checkSMS" id="captcha_checkSMS" value="#FORM.captcha_checkSMS#" />
										    <input type="hidden" name="cloneTel" id="cloneTel" value="">
											<table class="tableIndexForgetCode" class="tableIndexForgetCode" cellspading="0" cellspacing="0" width="100%">
												<colgroup>
													<col width="255px"></col>
													<col width="170px"></col>
													<col width="125px"></col>
												</colgroup>
												<tr>
													<td colspan="3" align="center" style="font-size:13px;">Veuillez saisir le code de sécurité ci dessous : </td>
													<td align="right"><a style="cursor:pointer" onclick="document.forms[''codeOublie''].reset();closeCodeForget();"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
												</tr>
												<tr style="height:27px;">
													<td colspan="3" align="center" style="color:red;" >&nbsp;
														<span id="errorCaptchaCodeForget" class="spanErrorCaptcha1" style="display:none;">champs à saisir</span>
														<span id="errorCaptchaCodeForgetBis" class="spanErrorCaptcha2">#errorCaptchaCodeForget#</span>
													</td>
												</tr>
												<tr>
													<td align="right" style="">#imgCaptcha2#<span class="asteriskSmallObligatoire">*</span></td>
													<td style="padding-left:15px;"><input class="inputTel" onfocus="supErrorCaptchaCodeForgetBis()" type="text" name="captchaCodeForget" id="captchaCodeForget" value="" /></td>
													<td >&nbsp;</td>
												</tr>
												<tr style="height:0px;">
									              <td style="text-indent:10px;font-size:9px;font-style:italic;"><span class="red">*</span>&nbsp;champs obligatoire</td>
									              <td align="" style="">&nbsp;</td>
									              <td align="" style="text-indent:20px;">
										              <input type="submit" id="EnvoiSMS" name="EnvoiSMS" value="Envoyer" onclick="if(verificationCodeForget()){document.forms[''codeOublie''].submit();loadsablier();}else{return false;}" style="">
										              <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
												  </td>
									            </tr>
											</table>
										</form>
									</div>
									
									<!--- popup confirmation envoi Mail / envoi SMS --->
									<div class="popupIndex3" id="SMSSendPop" style="#SMSSendVisibleStyle#">
										<form action="index.cfm" method="post">
											<table class="tableCodePersoSend" cellspading="0" cellspacing="0" width="100%">
												<colgroup>
													<col width="140px"></col>
													<col width="195px"></col>
													<col width="245px"></col>
												</colgroup>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr style="height:27px;">
													<td colspan="3" align="center">&nbsp;<span id="errorCaptchaCodeForget" style="color:green;font-size:11px;">#confirm#</span></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
												<tr style="height:0px;">
									              <td colspan="3" align="center" style="">
								'>
								
								<cfif hideBtn eq false >
									<cfset tableLogin &= '
											        <input type="submit" value="Renvoyer le mail" name="renvoiEmail" id="renvoiEmail" onclick="" style="">
									'>
								</cfif>
								
								<cfset tableLogin &= '
										              &nbsp;&nbsp;
										              <input type="button" value="Fermer" onclick="closeSMSSend();" style="">
										              <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
												  </td>
									            </tr>
											</table>
										</form>
									</div>
									
									
									<!--- popup erreur mauvais telephone --->
									<div class="popupIndex4" id="errorValidTel" style="#errorValidTelVisibleStyle#">
										<table class="tableIndexErrorValidTel" cellspading="0" cellspacing="0" width="100%">
											<colgroup>
												<col width="140px"></col>
												<col width="195px"></col>
												<col width="245px"></col>
											</colgroup>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr style="height:27px;">
												<td colspan="3" align="center">&nbsp;<span id="errorCaptchaCodeForget" style="color:red">#errorValidTel#</span></td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr style="height:0px;">
								              <td colspan="3" align="center" style="">
									              <input type="button" value="Fermer" onclick="closePopup(''errorValidTel'');" style="">
									              <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
											  </td>
								            </tr>
										</table>
									</div>
									
									<!--- popup erreur mauvais code --->
									<div class="popupIndex5" id="errorValidCodePerso" style="#errorValidCodeVisibleStyle#">
										<table class="tableIndexErrorValidCodePerso" cellspading="0" cellspacing="0" width="100%">
											<colgroup>
												<col width="140px"></col>
												<col width="195px"></col>
												<col width="245px"></col>
											</colgroup>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr style="height:27px;">
												<td colspan="3" align="center">&nbsp;<span id="errorCaptchaCodeForget" style="color:red">#errorValidCode#</span></td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr style="height:0px;">
								              <td colspan="3" align="center" style="">
									              <input type="button" value="Fermer" onclick="closePopup(''errorValidCodePerso'');" style="">
									              <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
											  </td>
								            </tr>
										</table>
									</div>
									
									<div class="bbodyTextCNIL">
										Tout utilisateur est informé que l''acces de ce logiciel doit se faire dans le respect des dispositions
										prévues par la CNIL en matière de protection de données personnelles et nominatives. Nous ne pourrons pas être
										tenu pour responsable d''une utilisation non conforme.
									</div>
								</div>
							</div>
							<div class="bbodyImgLogin"><img width="273px" height="189px" src="img/main_v1.jpg" title="image_login" alt=""></div><!--- width="273px" height="179px" --->
							<div class="spacer"></div>
						</div>
					</div><!--- bbody --->
					<div id="ffooter" style="">
						<!--- <span style="valign:top;">Pour utiliser ce service vous avez besoin des modules suivants : --->
					</div><!--- ffooter --->
					
				</div><!--- divLogin --->
			'
		/>
		
		<cfreturn tableLogin >
	</cffunction>
	
	
</cfcomponent><!--- fin class --->
