package univers.parametres.rago.inventaire.ihm
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.entity.Rules;
	
	[Bindable]
	public class DetailRegleImpl extends HBox
	{
		public var _selectedRegle:Regle = new  Regle();
		public var _rules:Rules = new Rules();
		
		public var dgListExceptions:DataGrid;
		public var dgListNoeuds:DataGrid;
		public var btnNewExecption:Button;
		public var rbNon:RadioButton;
		public var rbOui:RadioButton;
		public var txtCommentaire:TextArea;
		
		public var popUpErasrConfir:PopUpConfirmationIHM;
		public var popUp:PopUpNewExceptionIHM = new PopUpNewExceptionIHM();
		public var _listExceptions:ArrayCollection = new ArrayCollection();
		
		private var _selectedLine:Object;
			
		//Recupère les propriétées de la règle en cours
		public function get SelectedRegle(): Regle
		{ return _selectedRegle; }
		
		public function set SelectedRegle(value : Regle):void
		{ _selectedRegle = value; }
		
		public function get selectedLine(): Object
		{ return _selectedLine; }
		
		public function set selectedLine(value : Object):void
		{ _selectedLine = value; }
		
		
		
		public function DetailRegleImpl()
		{ 
			addEventListener("eraseException", _eraseLineInDatagrid);
			addEventListener("refreshDategridForNewExceptions",executeInitDetail);
			addEventListener("exceptionWasRefresh",executeInitDetail);
			addEventListener("newOrigineOrSource",newOrigineOrCible);
			super(); 
		}
		
		protected function initDatagridHandler(event:Event):void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			var obj:Object = new Object();
							
			obj.TYPE_NOEUDS = "Noeuds source";							
			obj.LIBELLE_SOURCE = SelectedRegle.LIBELLE_SOURCE;
			arrayC.addItem(obj);
				
			obj = new Object();	
				
			obj.TYPE_NOEUDS = "Noeuds cible";
			obj.LIBELLE_SOURCE = SelectedRegle.LIBELLE_CIBLE
			arrayC.addItem(obj);
			
			dgListNoeuds.dataProvider = arrayC;

		}
		
		protected function Close(event:CloseEvent):void
		{ PopUpManager.removePopUp(this); }
		
		protected function dgListExceptionsClickHandler(event:Event):void
		{
			if(dgListExceptions!=null && dgListExceptions.selectedIndex!=-1)
			{
				selectedLine = dgListExceptions.selectedItem;
			}
		}
		
		private function _eraseLineInDatagrid(event:Event):void
		{
//			if(dglistExceptions!=null && dglistExceptions.selectedIndex!=-1)
//			{
			if(SelectedRegle.LIST_EXCEPTION !=null && SelectedRegle.LIST_EXCEPTION.length !=0)
			{
//				_rules.listRegleExceptions.removeItemAt(dgListExceptions.selectedIndex);
				popUpErasrConfir = new PopUpConfirmationIHM();
				
				popUpErasrConfir.addEventListener("eraseExceptionNow",eraseNowException);
				popUpErasrConfir.addEventListener("noEraseExceptionNow",noEraseNowException);
				PopUpManager.addPopUp(popUpErasrConfir,this,true);
				PopUpManager.centerPopUp(popUpErasrConfir);
			}
		}
		
//		private function _executLineExceptionInDatagrid():void
//		{
//			_rules.IURulesOrganisationException(SelectedRegle.IDREGLE_ORGA,tempArray.length,0,tempArray);
//			_rules.addEventListener("IUOrgaException",executeEraseException);
//		}

		private function eraseNowException(event:Event):void
		{
			var tempArray:Array = new Array();

			tempArray[0] = SelectedRegle.LIST_EXCEPTION[dgListExceptions.selectedIndex].IDSOURCE.toString();
//			SelectedRegle.LIST_EXCEPTION = _rules.listRegleExceptions;
				
			_rules.IURulesOrganisationException(SelectedRegle.IDREGLE_ORGA,tempArray.length,0,tempArray);
			_rules.addEventListener("IUOrgaException",executeEraseException);
		}
		
		private function noEraseNowException(event:Event):void
		{ }
		
		private function executeEraseException(event:Event):void
		{
			dispatchEvent(new Event("findNewExceptionCreatedNow",true));
		}
		
		protected function initDetailRegles(event:Event):void
		{
			if(SelectedRegle.OPERATEUR == "Annuaire")
			{ btnNewExecption.enabled=false; }
			 initRadiobtn();
			 popUp.checkIfAnodeOrNot(SelectedRegle);
			 popUp.addEventListener("thisIsAFeuille",executethisIsAFeuille);
		}
		
		private function executethisIsAFeuille(event:Event):void
		{
			btnNewExecption.enabled = false;
		}
		
		
		private function initRadiobtn():void
		{
			if(SelectedRegle.BOOL_SUIVRE_AUTO)
			{ rbOui.selected = true; }
			else
			{ rbNon.selected = true; }
		}
		
		protected function rdgFollowChangeHandler(event:Event):void
		{ zAttributFollowInRegle(true); }
		
		protected function rdgNoFollowChangeHandler(event:Event):void
		{ zAttributFollowInRegle(false); }
		
		private function zAttributFollowInRegle(followValue:Boolean):void
		{	
			if(SelectedRegle!= null)
			{ SelectedRegle.BOOL_SUIVRE_AUTO = followValue; }
		}
		
		private function refreshDatagridWithNewException():void
		{
			dgListExceptions.dataProvider = SelectedRegle.LIST_EXCEPTION;
		}
		
		private function refreshDatagrid(event:Event):void
		{
			var objtemp:Object = new Object();
			var arraytemp:ArrayCollection = new ArrayCollection();
			for(var i:int = 0;i < SelectedRegle.LIST_EXCEPTION.length;i++)
			{
				objtemp.LIBELLE_GROUPE_CLIENT =  SelectedRegle.LIST_EXCEPTION[i].LABEL;
				arraytemp.addItem(objtemp);
			}
			dgListExceptions.dataProvider = arraytemp;

		}
		
		protected function btnNewExecptionClickHandler(event:Event):void
		{
			popUp = new PopUpNewExceptionIHM();
			popUp.SelectedRegle = SelectedRegle;
			popUp.addEventListener("newExceptionIsCreated",whenThisWindowIsVisible);
			PopUpManager.addPopUp(popUp,this,true);
			PopUpManager.centerPopUp(popUp);
		}
		
		public function newOrigineOrCible(event:Event):void
		{
			var p:PopUpNewOrigineOrCibleIHM = new PopUpNewOrigineOrCibleIHM();
			p.typeModif = "Noeuds cible";
			p._selectedRegle = SelectedRegle;
			p.addEventListener("newOrigineOrCibleCreated",refreshDatagridNoeuds);
			PopUpManager.addPopUp(p,this,true);
			PopUpManager.centerPopUp(p);
			
		}
		
		private function refreshDatagridNoeuds(event:Event):void
		{
			dispatchEvent(new Event("regleIsModified",true)); 
		}

		protected function btValiderClickHandler(event:Event):void
		{
			dispatchEvent(new Event("regleIsModifiedAndSendNewRegle",true));			
			dispatchEvent(new Event("regleIsModified",true));
			dispatchEvent(new Event("exceptionsIsClosed",true));
			PopUpManager.removePopUp(this); 
		}
		
		protected function changeTxt(event:Event):void
		{
			_selectedRegle.REGLE_COMMENT = txtCommentaire.text;
		}
		
		protected function btAnnulerClickHandler(event:Event):void
		{ 
			dispatchEvent(new Event("regleIsModified",true));
			dispatchEvent(new Event("exceptionsIsClosed",true));
			PopUpManager.removePopUp(this); 
		}
		
		
		protected function whenThisWindowIsVisible(event:Event):void
		{
//			_rules.GetRegleException(SelectedRegle.IDREGLE_ORGA);
//			_rules.addEventListener("receivedExceptions",executeInitDetail);
			dispatchEvent(new Event("findNewExceptionCreatedNow",true));
		}
		
		protected function initDetailHandler(event:Event):void
		{
//			_rules.GetRegleException(SelectedRegle.IDREGLE_ORGA);
//			_rules.addEventListener("receivedExceptions",executeInitDetail);
		}
		
		private function executeInitDetail(event:Event):void
		{
			tempFucntionForrefresh();
		}
		
		private function tempFucntionForrefresh():void
		{
			dgListExceptions.dataProvider = SelectedRegle.LIST_EXCEPTION;
		}
		
		
	}
}