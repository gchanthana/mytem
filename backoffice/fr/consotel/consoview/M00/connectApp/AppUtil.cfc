﻿<cfcomponent>
			 	 
	<cffunction name="getLastCodePays" access="remote" output="false" returntype="string">
		<!----<cfif isDefined("SESSION")>
			<cfif structKeyExists(SESSION,"USER")>
				<cfif structKeyExists(SESSION.USER,"GLOBALIZATION")>
					<cfreturn SESSION.USER.GLOBALIZATION>
				</cfif>
			</cfif>
		</cfif>---->
		<cfif structKeyExists(COOKIE,"CODE_PAYS")>
			<cfreturn COOKIE.CODE_PAYS>
		<cfelse>
			<cfreturn "NO_CODE_PAYS">
		</cfif>
	</cffunction> 
	<cffunction name="getLastLogin" access="remote" output="false" returntype="string">
		<cfset var defaultLogin="">
		<cftry>
			<cfif structKeyExists(COOKIE,"LAST_LOGIN")>
				<cfreturn COOKIE.LAST_LOGIN>
			<cfelse>
				<cfreturn defaultLogin>
			</cfif>
			<cfcatch type="any">
				<cftry>
					<cfset var mailSubject="Exception rencontrée durant la récupération du dernier login connecté à ConsoView">
					<cfif isDefined("CFCATCH.message") AND (lCase(CFCATCH.message) EQ lCase("Session is invalid"))>
						<cfset mailSubject="Session ConsoView invalidée">
					</cfif>
					<cfmail from="consoview@consotel.fr" to="dev@consotel.fr" subject="#mailSubject# sur le backoffice : #CGI.SERVER_NAME#" type="html">
						Une exception a été rencontrée durant la récupération du dernier login utilisé par un utilisateur de ConsoView (Cookie)<br>
						Une des cause connue pour une telle exception est décrite dans le ticket : http://nathalie.consotel.fr/issues/5832<br>
						<cfoutput>
							Cible de la requete HTTP : #CGI.SCRIPT_NAME#<br>
							Adresse cliente : #CGI.REMOTE_HOST#<br>
							<cfif isDefined("APPLICATION") AND isDefined("APPLICATION.applicationName")>
								Application correspondante : #APPLICATION.applicationName#<br>
							</cfif>
						</cfoutput>
						<cfif isDefined("SESSION")>
							<cfdump var="#CFCATCH#" label="Session correspondante"><br>
						</cfif>
						<cfdump var="#CFCATCH#" label="Exception capturée"> 
					</cfmail>
					<cfset structClear(COOKIE)>
					<cfcatch type="any">
						<cfmail from="consoview@consotel.fr" to="dev@consotel.fr" subject="Erreur lors de l'invalidation d'un cookie sur le backoffice : #CGI.SERVER_NAME#" type="html">
							Une exception a été rencontrée durant l'invalidation d'un cookie ConsoView<br>
							L'invalidation du cookie a été spécifiée par le ticket : http://nathalie.consotel.fr/issues/5832<br>
							<cfoutput>
								Cible de la requete HTTP : #CGI.SCRIPT_NAME#<br>
								Adresse cliente : #CGI.REMOTE_HOST#<br>
								<cfif isDefined("APPLICATION") AND isDefined("APPLICATION.applicationName")>
									Application correspondante : #APPLICATION.applicationName#<br>
								</cfif>
							</cfoutput>
							<cfif isDefined("SESSION")>
								<cfdump var="#SESSION#" label="Session correspondante"><br>
							</cfif>
							<cfdump var="#CFCATCH#" label="Exception capturée"> 
						</cfmail>
						<cfreturn defaultLogin>
					</cfcatch>
				</cftry>
				<cfreturn defaultLogin>
			</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="getLastCodeStyle" access="remote" output="false" returntype="string">
		<cfif structKeyExists(COOKIE,"CODE_STYLE")>
			<cfreturn COOKIE.CODE_STYLE>
		<cfelse>
			<cfreturn "NO_STYLE">
		</cfif>
	</cffunction> 
	<cffunction name="searchNodes" access="remote" returntype="query">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfargument name="searchKeyword" required="true" type="string">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.search_noeud">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#searchKeyword#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam  value="50" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeList">
		</cfstoredproc>
		<cfreturn qGetNodeList>
	</cffunction>
	<cffunction name="getNodeXmlPath" access="remote" returntype="xml">
		<cfargument name="idGroupeRacine" required="true" type="numeric">
		<cfargument name="idSource" required="true" type="numeric">
		<cfargument name="idCible" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_GLOBAL.chemin_source_cible_v2">
			<cfprocparam  value="#idSource#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#idCible#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qChemin">
		</cfstoredproc>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"NODE")>
		<cfset rootNode = perimetreXmlDoc.NODE>
		<cfif qChemin.recordcount GT 0>
			<cfset typeOrga = qChemin['TYPE_ORGA'][qChemin.recordcount]>
			<cfloop index="i" from="1" to="#qChemin.recordcount#">
				<cfif i EQ 1>
					<cfset rootNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset rootNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset rootNode.XmlAttributes.STC = qChemin['STC'][i]>
					<cfif idSource EQ idGroupeRacine>
						<cfif qChemin.recordcount GT 1>
							<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"NODE")>
							<cfswitch expression="#UCASE(typeOrga)#">
								<cfcase value="OPE">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Opérateurs">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -2>
								</cfcase>
								<cfcase value="CUS">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfcase>
								<cfcase value="SAV">
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Recherches Sauvegardées">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -4>
								</cfcase>
								<cfdefaultcase>
									<cfset rootNode.XmlChildren[1].XmlAttributes.LBL = "Liste Des Organisations Clientes">
									<cfset rootNode.XmlChildren[1].XmlAttributes.NID = -3>
								</cfdefaultcase>
							</cfswitch>

							<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 0>
							<cfset rootNode.XmlChildren[1].XmlAttributes.STC = 0>
						</cfif>
					</cfif>
				<cfelseif i EQ 2>
					<cfset parentNode = ''>
					<cfif idSource EQ idGroupeRacine>
						<cfset rootNode.XmlChildren[1].XmlAttributes.NTY = 1>
						<cfset parentNode = rootNode.XmlChildren[1]>
					<cfelse>
						<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
						<cfoutput>
							<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
							<cfif arrayLen(nodeArray) NEQ 1>
								<cfthrow type="INVALID_GROUP_NODES_QUERY"
										message="Requï¿½te Arbre Pï¿½rimï¿½tres Invalide"
										detail="1006">
							</cfif>
							<cfset parentNode = nodeArray[1]>
						</cfoutput>
					</cfif>
					<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
					<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
					<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
				<cfelse>
					<cfset tmpNID = qChemin['IDGROUPE_CLIENT'][i]>
					<cfset tmpParentNID = qChemin['IDGROUPE_CLIENT'][i - 1]>
					<cfoutput>
						<cfset nodeArray = XMLSearch(perimetreXmlDoc,"//NODE[@NID=#tmpParentNID#]")>
						<cfif arrayLen(nodeArray) NEQ 1>
							<cfthrow type="INVALID_GROUP_NODES_QUERY"
									message="Requï¿½te Arbre Pï¿½rimï¿½tres Invalide"
									detail="1006">
						</cfif>
						<cfset parentNode = nodeArray[1]>
						<cfset parentChildNb = arrayLen(parentNode.XmlChildren)>
						<cfset parentNode.XmlChildren[parentChildNb + 1] = XmlElemNew(perimetreXmlDoc,"NODE")>
						<cfset tmpNode = parentNode.XmlChildren[parentChildNb + 1]>
						<cfset tmpNode.XmlAttributes.LBL = qChemin['LIBELLE_GROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NID = qChemin['IDGROUPE_CLIENT'][i]>
						<cfset tmpNode.XmlAttributes.NTY = qChemin['TYPE_NOEUD'][i]>
						<cfset tmpNode.XmlAttributes.STC = qChemin['STC'][i]>
					</cfoutput>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset rootNode.XmlAttributes.LBL = "Pas de chemin trouvï¿½">
			<cfset rootNode.XmlAttributes.NID = -1>
			<cfset rootNode.XmlAttributes.NTY = 0>
			<cfset rootNode.XmlAttributes.STC = 0>
		</cfif>
		<cfset qChemin = ''>
		<cfreturn perimetreXmlDoc>
	</cffunction>
	<cffunction name="getParamFromCookie" access="remote" >
		<cfargument name="listeValeur" type="array" >
		<cftry>
			<cfif isDefined("COOKIE")>
				<cfif arraylen(listeValeur) > 0>
					<cfloop array="#listeValeur#" index="idx">
						<cfif structKeyExists(COOKIE,idx)>
							<cflog text="#COOKIE['#idx#']#" />
							<cfreturn COOKIE[#idx#]/>
							<cfbreak>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
			<cfreturn COOKIE>
		<cfcatch type="any" >
			<cfreturn cfcatch>
		</cfcatch>
		</cftry>
	</Cffunction>
	<cffunction name="deleteTokenFromCookie" access="remote" >
		<cfargument name="codeApp" type="numeric" >
		<cftry>
			<cfif isDefined("COOKIE")>
				<cfif structKeyExists(COOKIE,"cleSSO#codeApp#")>
					<cfcookie name="cleSSO#codeApp#" expires="now" >
				</cfif>
			</cfif>
			<cfreturn 1>
		<cfcatch type="any" >
			<cfset obj=structNew()>
			<cfset obj.cfcatch = cfcatch>
			<cfset obj.cookie = COOKIE>
			<cfreturn obj>
		</cfcatch>
		</cftry>
	</Cffunction>
	
	
	<cffunction name="createSessionXmlPFGPAccueil" access="package" output="false" returntype="void" hint="Créer le xml qui sert au menu de l'accueil">
		<cfargument name="qResult" type="query" required="true" >
		<cfargument name="qResult2" type="query" required="true" >
		
		<cflog text="connection2.createSessionXmlPFGPAccueil">
		<!---PHASE 0 : on créé le xml racine--->
		<cfset xmlAcces = xmlNew(true)/>
		<cfset xmlAcces.xmlRoot = xmlElemNew(xmlAcces,"ROOT")/>
		<!--- PHASE 1 : on créé le xml des univers--->
		<cfset idx=1/>
		<cfloop query="qResult">
		<!---Si le type est celui désiré. 
			 Type = 1 -> Menu M00. Type = 2 -> Accueil M521--->
			<cfif qResult.TYPE eq 2>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx] = XmlElemNew(xmlAcces,"UNIVERS")>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM#>
			<!---Si l'univers est SYS = 1, cad qu'un module charge quand on clique dessus--->
				<cfif qResult.SYS eq 1>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.MODULE_KEY#>
				<cfelse>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
				</cfif>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.SYS = #qResult.SYS#>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.USR = "2">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.type = "radio">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.toggled = "false">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ENABLED = "true">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ICON = #qResult.ICONE#>
				<cfset idx = idx +1>
			</cfif>
		</cfloop>
		<!--- PHASE 2: on ajoute les fonctions--->
		<!---1) on boucle sur chaque univers--->
		<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
			<cfset keyUniv = xmlAcces.xmlRoot.XmlChildren[i].xmlAttributes.KEY>
		<!---2) on boucle sur les fonctions pour trouver les fonctions enfants de l'univers parent--->	
			<cfloop query="qResult2">
				<!---Si l'univers est différent, on passe à la fonction suivante--->
				<cfif qResult2.UNIVERS_BLOC eq keyUniv>
					<!---Si la clé de la fonction est égale à la clé de l'univers parent, on ignore. Sinon on continue le traitement--->
					<cfif qResult2.KEY neq qResult2.UNIVERS_BLOC>
						<cfset idx = arraylen(xmlAcces.xmlRoot.xmlChildren[i].xmlChildren)+1>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx] = XmlElemNew(xmlAcces,"FONCTION")>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.LBL = #qResult2.NOM_BLOC#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.DSC = #qResult2.DESCRIPTION_BLOC#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.KEY = #qResult2.KEY#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ENABLED = "true">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ICON = #qResult2.ICONE_BLOC#>
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>
		<cflog text="connection2.xmlAcces = #xmlAcces#" >
		<cfset SESSION.XML_ACCESS_PFGP_ACCUEIL = xmlAcces>
	</cffunction>
	<cffunction name="createSessionXmlPFGP" access="package" returntype="void" >
		<cfargument name="qResult" type="query" required="true" >
		<cfargument name="qResult2" type="query" required="true" >
		<cfargument name="qResult3" type="query" required="true" >
		
		<cflog text="connection2.createSessionXmlPFGP">
		<!---PHASE 0 : on créé le xml racine--->
		<cfset xmlAcces = xmlNew(true)/>
		<cfset xmlAcces.xmlRoot = xmlElemNew(xmlAcces,"ROOT")/>
		<!--- PHASE 1 : on créé le xml des univers--->
		<cfset idx=1/>
		<cfloop query="qResult">
		<!---Si le type est celui désiré. 
			 Type = 1 -> Menu M00. Type = 2 -> Accueil M521--->
			<cfif qResult.TYPE eq 1>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx] = XmlElemNew(xmlAcces,"UNIVERS")>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM#>
			<!---Si l'univers est SYS = 1, cad qu'un module charge quand on clique dessus--->
				<cfif qResult.SYS eq 1>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.MODULE_KEY#>
				<cfelse>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
				</cfif>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.SYS = #qResult.SYS#>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.USR = "2">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.type = "radio">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.toggled = "false">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ENABLED = "true">
				<cfset idx = idx +1>
			</cfif>
		</cfloop>
		<!--- PHASE 2: on ajoute les fonctions--->
		<!---1) on boucle sur chaque univers--->
		<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
			<cfset keyUniv = xmlAcces.xmlRoot.XmlChildren[i].xmlAttributes.KEY>
		<!---2) on boucle sur les fonctions pour trouver les fonctions enfants de l'univers parent--->	
			<cfloop query="qResult2">
				<!---Si l'univers est différent, on passe à la fonction suivante--->
				<cfif qResult2.UNIVERS_MENU eq keyUniv>
					<!---Si la clé de la fonction est égale à la clé de l'univers parent, on ignore. Sinon on continue le traitement--->
					<cfif qResult2.KEY neq qResult2.UNIVERS_MENU>
						<cfset idx = arraylen(xmlAcces.xmlRoot.xmlChildren[i].xmlChildren)+1>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx] = XmlElemNew(xmlAcces,"FONCTION")>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.LBL = #qResult2.NOM_MENU#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.KEY = #qResult2.KEY#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ENABLED = "true">
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>
		<!--- PHASE 3: on definit la fonction affichée par défaut--->
		<cfset boolDefault = 0>
		<cfloop query="qResult3">
			<cfset keyDefault = qResult3.KEY>
			<!--- On boucle sur les univers --->
			<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
				<cfset keyUniv= xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.KEY/>
				<!--- Si l'univers correspond à l'univers par défaut, on modifie la propriété toggled à true et on sort de la boucle --->
				<cfif keyDefault eq keyUniv>
					<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.toggled = 'true'>
					<cfset boolDefault = 1>
					<cfbreak>
				<cfelse>
					<!--- Sinon on vérifie que l'univers a des enfants --->
					<cfif arraylen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren) gt 0>
						<!--- On boucle sur les fonctions de l'univers --->	
						<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren)#" index="j">
							<cfset keyFonction= xmlAcces.xmlRoot.XmlChildren[i].XmlChildren[j].XmlAttributes.KEY>
							<!--- Si la fonction correspond à la fonction par défaut, on modifie la propriété toggled à true et on sort de la boucle --->
							<cfif keyFonction eq keyDefault>
								<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlChildren[j].XmlAttributes.toggled = 'true'>
								<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.toggled = 'true'>
								<cfset boolDefault = 1>	
								<cfbreak>
							</cfif> 
						</cfloop>
					</cfif>
				</cfif>
			</cfloop>
			<cfif boolDefault eq 0>
				<cfif arraylen(xmlAcces.xmlRoot.XmlChildren) gt 0>
					<cfset xmlAcces.xmlRoot.XmlChildren[1].XmlAttributes.toggled = 'true'>
					<cfif arraylen(xmlAcces.xmlRoot.XmlChildren[1].XmlChildren) gt 0>
						<cfset xmlAcces.xmlRoot.XmlChildren[1].XmlChildren[1].XmlAttributes.toggled = 'true'>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<cflog text="connection2.xmlAcces = #xmlAcces#">
		<cfset SESSION.XML_ACCESS = xmlAcces>
	</cffunction>
</cfcomponent>