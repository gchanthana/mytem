package service.diffusion.supprimerdiffusion
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class SupprimerDiffusionService
	{
		public var _model:SupprimerDiffusionModel;
		public var handler:SupprimerDiffusionHandler;

		public function SupprimerDiffusionService()
		{
			this._model=new SupprimerDiffusionModel();
			this.handler=new SupprimerDiffusionHandler(model);
		}

		public function get model():SupprimerDiffusionModel
		{
			return this._model;
		}

		public function supprimerDiffusion(idEvent:int,noContrat:int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ActionsDiffusionService", "supprimerDiffusion", handler.supprimerDiffusionHandler);

			RemoteObjectUtil.callService(callOp,idEvent,noContrat);
		}
	}
}
