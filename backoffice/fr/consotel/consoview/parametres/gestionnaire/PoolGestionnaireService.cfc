<cfcomponent name="gestionPool">
	
<cffunction name="getPoolGestionnaire" access="remote" returntype="any" description="Retourne la liste des pools de gestionnaires dun groupe Client">
	<cfargument name="idGroupe" type="numeric" required="true">
	<cfargument name="Search_texte" type="String" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPoolGestionnaire_v2">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Search_texte#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>

<cffunction name="getListeGestByPool" access="remote" returntype="any" description="Liste des gestionnaires du pool">
	<cfargument name="idPool" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M21.ListeGestionnairePool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">		
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>

<cffunction name="getTypeCommandeByPool" access="remote" returntype="any" description="Liste des type de commande du pool">
	<cfargument name="idPool" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M21.ListeTypeCommandeByPool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">		
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>


	<cffunction name="getAllRevendeur" access="remote" returntype="any" description="Retourne les revendeurs du client">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfargument name="search" type="string" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.Get_all_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#search#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#segment1#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
<cffunction name="getRevendeur" access="remote" returntype="query" description="Retourne les revendeurs du client qui ne sont pas rattaaché a un pool">		
	<cfargument name="idPool" type="numeric" required="true">
	<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getRevNotInPool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="#iif((idPool eq -1), de("yes"), de("no"))#">		
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="listOpDistrib" access="remote" returntype="query" description="Retourne les revendeurs d un operateur">		
	<cfargument name="idDistrib" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M24.listOpDistrib">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDistrib#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="updateOpDistrib" access="remote" returntype="any" description="maj les op d un dsitrib">
	<cfargument name="idDistrib" type="numeric" required="true">
	<cfargument name="idOperateur" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M24.updateOpDistrib">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDistrib#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>

<cffunction name="getSitePool" access="remote" returntype="any" description="Retourne la liste des sites d un POOL + tous les sites">
	<cfargument name="IDGroupe" type="numeric" required="true">
	<cfargument name="IDPool" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getSitePool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDPool#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getActionProfil" access="remote" returntype="any" description="Retourne la des actions pour un profil">
	<cfargument name="IDProfil" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getActionProfil">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDProfil#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getTypeCommandePool" access="remote" returntype="any" description="Retourne la liste des types de commande d un pool + tous les type de commande">
	<cfargument name="IDGroupe" type="numeric" required="true">
	<cfargument name="IDRevendeur" type="numeric" required="true">
	<cfargument name="IDPool" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getTypeCommandePool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRevendeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDPool#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getTypeCommandeLogin" access="remote" returntype="any" description="Retourne la liste des types de commande d'un login">
	<cfargument name="IDGroupe" type="numeric" required="true">
	<cfargument name="IDLogin" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getTypeCommandeLogin">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDLogin#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getPoolLogin" access="remote" returntype="any" description="Retourne les pools d un login">
	<cfargument name="IDGroupe" type="numeric" required="true">
	<cfargument name="IDLogin" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m21.getPoolOfLogin">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDLogin#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getListeTypeCommande" access="remote" returntype="any" description="Retourne les pools d un login">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m21.ListerTypeCommande">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_GROUPE#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="addPoolGestionnaire" access="remote" returntype="any" description="Creer un nouveau Pool de gestionnaire">
	<cfargument name="Libelle_pool" type="string" required="true">
	<cfargument name="codeInterne_pool" type="string" required="true">
	<cfargument name="commentaire_pool" type="string" required="true">
	<cfargument name="idUser" type="numeric" required="true">
	<cfargument name="idRacine" type="numeric" required="true">
	<cfargument name="idRevendeur" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addPoolGestionnaire">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_pool#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne_pool#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_pool#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#SESSION.USER.CLIENTACCESSID#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRacine#" null="false">
		<cfif idRevendeur eq -1>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRevendeur#" null="true">
		<cfelse>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRevendeur#">
			
		</cfif>
		
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="updatePoolGestionnaire" access="remote" returntype="any" description="Permet de modifier les infos Dun Pool de gestionnaire">
		<cfargument name="IDPool" type="Numeric" required="true">
		<cfargument name="Libelle_pool" type="string" required="true">
		<cfargument name="codeInterne_pool" type="string" required="true">
		<cfargument name="commentaire_pool" type="string" required="true">
		<cfargument name="user" type="Numeric" required="true">
		<cfargument name="idRevendeur" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updatePoolGestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDPool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_pool#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#user#" null="false">
			<cfif idRevendeur eq -1>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRevendeur#" null="true">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idRevendeur#">
			</cfif>
			<!---<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="#iif((idRevendeur eq 0), de("yes"), de("no"))#">	--->	
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
	
<cffunction name="getPoolRevendeur" access="remote" returntype="any" description="Retourne les revendeurs du client">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfargument name="search" type="string" required="true">
		<cfargument name="segment1" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.Get_all_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#search#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#segment1#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
<cffunction name="remove_pool_gestionnaire_contrainte" access="remote" returntype="any" description="Supprime un pool + toutes les contraintes associées">
	<cfargument name="idPool" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.deletepoolgestionnaire">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="updateXPoolSite" access="remote" returntype="numeric">
		<cfargument name="idSite" type="numeric" required="true">
		<cfargument name="tab_pool" type="array" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfset len = arrayLen(tab_pool)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updateSiteOfPool(idSite,val(tab_pool[i]),value)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateXSite" access="remote" returntype="numeric">
		<cfargument name="idPool" type="numeric" required="true">
		<cfargument name="tab_site" type="array" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfset len = arrayLen(tab_site)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updateSiteOfPool(val(tab_site[i]),idPool,value)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateXActionProfil" access="remote" returntype="numeric">
	<cfargument name="idProfil" type="numeric" required="true">
	<cfargument name="tab_action" type="array" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfset len = arrayLen(tab_action)>
	<cfset r = -1>
	<cfloop index="i" from="1" to="#len#">
		<cfset r = updateActionProfil(idProfil,val(tab_action[i]),value)>
	</cfloop>
	<cfreturn r>
</cffunction>
<cffunction name="updateXTypeCMD" access="remote" returntype="numeric">
		<cfargument name="idPool" type="numeric" required="true">
		<cfargument name="tab_typeCMD" type="array" required="true">
		<cfargument name="idRevendeur" type="numeric" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfset len = arrayLen(tab_typeCMD)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updateTypeCommandeOfPool(idPool,tab_typeCMD[i],idRevendeur,value)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateXPoolLogin" access="remote" returntype="numeric">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfargument name="idLogin" type="numeric" required="true">
		<cfargument name="tab_pool" type="array" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="idProfil" type="numeric" required="true">
		<cfset len = arrayLen(tab_pool)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updatePoolLogin(IDGroupe_client,tab_pool[i],idLogin,value,idProfil)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateXTypeCommandeOfLogin" access="remote" returntype="numeric">
		<cfargument name="idLogin" type="numeric" required="true">
		<cfargument name="tab_typecmd" type="array" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfset len = arrayLen(tab_typecmd)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updateTypeCommandeOfLogin(idLogin,tab_typecmd[i],value)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateTypeCommandeOfLogin" access="remote" returntype="any" description="maj les site du pool">
	<cfargument name="idLogin" type="numeric" required="true">
	<cfargument name="idTypecmd" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateTypeCommandeOfLogin">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLogin#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypecmd#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="updateActionProfil" access="remote" returntype="any" description="maj les site du pool">
	<cfargument name="idProfil" type="numeric" required="true">
	<cfargument name="idAction" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateActionProfil">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idAction#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="updateSiteOfPool" access="remote" returntype="any" description="maj les site du pool">
	<cfargument name="idSite" type="numeric" required="true">
	<cfargument name="idPool" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateSitePool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="updateTypeCommandeOfPool" access="remote" returntype="any" description=" maj les type de commande du pool">
	<cfargument name="idPool" type="numeric" required="true">
	<cfargument name="idCommande" type="numeric" required="true">
	<cfargument name="idRevendeur" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateTypeCommandeOfPool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCommande#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	
</cffunction>
<cffunction name="fournirListeActProfil" access="remote" returntype="any" description="liste actions par profile">
	<cfargument name="idProfil" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.fournirListeActProfil">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="fournirListeProfiles" access="remote" returntype="any" description="liste profile">
	<cfargument name="idRacine" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.fournirListeProfiles">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getAllAction" access="remote" returntype="any" description="Retourne tous les gestionnaires dun groupe Client">
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getAllAction">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<!--- On ne retiens que les actions de la flotte mobile  where idinv_actions > 2000--->
		<cfquery name="qActionGestionMobile" dbtype="query">
			select * from p_result 
		</cfquery>
		<cfreturn qActionGestionMobile>
</cffunction>
<cffunction name="createProfile" access="remote" returntype="any" description="crÃ©er un profil">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfargument name="libelle" type="string" required="true">
		<cfargument name="codeInterne" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.createProfile">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#codeInterne#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

<cffunction name="createTypeCommande" access="remote" returntype="any" description="crÃ©er un profil">
		<cfargument name="IDGroupe_client" type="numeric" required="true">
		<cfargument name="libelle" type="string" required="true">
		<cfargument name="codeInterne" type="string" required="true">
		<cfargument name="commentaire" type="string" required="true">
		<cfargument name="fixe" type="Numeric" required="true">
		<cfargument name="mobile" type="Numeric" required="true">
		<cfargument name="data" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M21.createTypeCommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#codeInterne#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#fixe#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#mobile#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="updateInfoTypeCommande" access="remote" returntype="any" description="crÃ©er un profil">
		<cfargument name="idTypeCmd" type="numeric" required="true">
		<cfargument name="libelle" type="string" required="true">
		<cfargument name="codeInterne" type="string" required="true">
		<cfargument name="commentaire" type="string" required="true">
		<cfargument name="fixe" type="Numeric" required="true">
		<cfargument name="mobile" type="Numeric" required="true">
		<cfargument name="data" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M21.MAJTypeCommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeCmd#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#codeInterne#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#commentaire#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#fixe#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#mobile#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="getPoolOfTypeCommande" access="remote" returntype="any" description="Retourne la liste des types de commande d un pool + tous les type de commande">
	<cfargument name="IDGroupe" type="numeric" required="true">
	<cfargument name="IDTypeCmd" type="numeric" required="true">
	<cfargument name="IDPool" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPoolOfTypeCommande">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTypeCmd#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="updateProfile" access="remote" returntype="any" description="update un profil">
	<cfargument name="idProfil" type="Numeric" required="true">
	<cfargument name="libelle" type="string" required="true">
	<cfargument name="codeInterne" type="string" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateProfile">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#codeInterne#" null="false">
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="updatePoolLogin" access="remote" returntype="any" description="update pool d un login">
	<cfargument name="IDGroupe_client" type="numeric" required="true">
	<cfargument name="idPool" type="Numeric" required="true">
	<cfargument name="idLogin" type="Numeric" required="true">
	<cfargument name="value" type="Numeric" required="true">
	<cfargument name="idProfil" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updatePoolLogin">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLogin#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfif idProfil eq -1>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idProfil#" null="true">
		<cfelse>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#idProfil#">
		</cfif>
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="addSite" access="remote" returntype="any" description="Permet de creer un site">
		<cfargument name="IDgroupe_client" type="Numeric" required="true">
		<cfargument name="Libelle_site" type="string" required="true">
		<cfargument name="ref_site" type="string" required="true">
		<cfargument name="code_interne_site" type="string" required="true">
		<cfargument name="adr_site" type="string" required="true">
		<cfargument name="cp_site" type="string" required="true">
		<cfargument name="commune_site" type="string" required="true">
		<cfargument name="commentaire_site" type="string" required="true">
		<cfargument name="idpays" type="string" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.addSite_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDgroupe_client#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#cp_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idpays#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<cffunction name="updateInfoSite" access="remote" returntype="any" description="Permet de modifier les infos d un site">
<cfargument name="IDsite" type="Numeric" required="true">
<cfargument name="Libelle_site" type="string" required="true">
<cfargument name="ref_site" type="string" required="true">
<cfargument name="code_interne_site" type="string" required="true">
<cfargument name="adr_site" type="string" required="true">
<cfargument name="cp_site" type="string" required="true">
<cfargument name="commune_site" type="string" required="true">
<cfargument name="commentaire_site" type="string" required="true">
<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.updateSite">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Libelle_site#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_site#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#code_interne_site#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr_site#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#cp_site#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commune_site#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire_site#" null="false">
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
</cfstoredproc>
<cfreturn p_retour>
</cffunction>
<cffunction name="getSite" access="remote" returntype="any" description="Retourne la liste des sites d un groupe client">
<cfargument name="IDGroupe_client" type="numeric" required="true">
<cfargument name="Search_texte" type="string" required="true">
<cfargument name="maxRow" type="string" required="true">
<cfset maxRow = 100>
<cfif maxRow neq 1 >
<cfset maxRow = -1>
</cfif>
<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getSite">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Search_texte#" null="false">
	<cfprocresult name="p_result" maxrows="#maxRow#">
</cfstoredproc>
<cfreturn p_result>
</cffunction>
<cffunction name="getContrainteOfSite" access="remote" returntype="any" description="iste des contrainte du pool">
	<cfargument name="idSite" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getContrainteOfSite">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getContrainteOfTypeCommande" access="remote" returntype="any" description="iste des contrainte du type de commande">
	<cfargument name="idTypeCOmmande" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getContrainteOfTypeCommande">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeCOmmande#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="removeTypeCommande" access="remote" returntype="any" description="Permet de supprimer un type de commande">
		<cfargument name="idTypeCOmmande" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.removeTypeCommande">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeCOmmande#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="removeSite" access="remote" returntype="any" description="Permet de supprimer lun site">
		<cfargument name="IDsite" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.removeSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDsite#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="removeProfil" access="remote" returntype="any" description="Permet de supprimer lun site">
<cfargument name="IDprofil" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.removeProfil">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDprofil#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="getContraintesOfPool" access="remote" returntype="any" description="iste des contrainte du pool">
	<cfargument name="idPool" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getContrainteOfPool">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="listeContrainteOfSite" access="remote" returntype="any" description="liste des contrainte du site">
	<cfargument name="idSite" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.listeContrainteOfSite">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="listeContrainteOfProfil" access="remote" returntype="any" description="liste des contrainte du site">
	<cfargument name="idProfil" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getContrainteOfProfil">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="listeUserOfProfil" access="remote" returntype="any" description="liste des contrainte du site">
	<cfargument name="idProfil" type="Numeric" required="true">
	<cfset IDRACINE = SESSION.PERIMETRE.ID_GROUPE>
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.listeUserProfilRac">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getPoolOfLogin" access="remote" returntype="any" description="update un profil">
	<cfargument name="idGroupe" type="Numeric" required="true">
	<cfargument name="idPool" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPoolOfLogin">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getPoolSite" access="remote" returntype="any" description="update un profil">
	<cfargument name="idGroupe" type="Numeric" required="true">
	<cfargument name="idSite" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GESTIONNAIRE.getPoolSite">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<!---
compte opérateur et revendeurs
--->
<cffunction name="get_catalogue_revendeur_actif" access="public" returntype="query" output="false" hint="Lister revendeurs, génériques et privés pour une racine donnée. (Catalogue fournisseur)." >
	<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
	<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
	<cfargument name="app_loginID" required="false" type="numeric" default="" displayname="numeric app_loginID" hint="paramNotes" />
	<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_catalogue_revendeur">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#app_loginID#">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>		
</cffunction>				
<cffunction name="getOperateursActif" access="remote" returntype="any" >
	<cfargument name="idRacine" type="Numeric" required="true">
	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.GETOPERATEURSACTIF">
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#idRacine#"/>
		<cfprocresult name="qResult">
	</cfstoredproc>
	<cfreturn qResult>
</cffunction>
<cffunction name="getCompteOpeCompte" access="remote" returntype="any" >
	<cfargument name="idGroupe" type="Numeric" required="true">
	<cfargument name="idoperateur" type="Numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getCompteOpeCompte">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idoperateur#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getCompteOpeLogin" access="remote" returntype="any" >
	<cfargument name="idLogin" type="Numeric" required="true">
	<cfargument name="idoperateur" type="Numeric" required="true">
	<cfset idracine = SESSION.PERIMETRE.ID_GROUPE>
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getCompteOpeLogin">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLogin#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idoperateur#" null="false">
	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="getCompteOpePool" access="remote" returntype="any" >
      <cfargument name="idPool" type="Numeric" required="true">
      <cfargument name="idoperateur" type="Numeric" required="true">
      <cfset idracine = SESSION.PERIMETRE.ID_GROUPE>
      <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getCompteOpePool">
      <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" null="false">
      <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idoperateur#" null="false">
      <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#" null="false">
      <cfprocresult name="p_result">
      </cfstoredproc>
      <cfreturn p_result>
</cffunction>


<cffunction name="updateXCompteOpeCompte" access="remote" returntype="numeric">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="tab_souscompte" type="array" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfset len = arrayLen(tab_souscompte)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updateCompteOpeCompte(idRacine,tab_souscompte[i],value)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateCompteOpeCompte" access="remote" returntype="any">
	<cfargument name="idRacine" type="numeric" required="true">
	<cfargument name="idsouscompte" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.updateCompteOpeCompte">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsouscompte#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="updateXCompteOpeLogin" access="remote" returntype="numeric">
		<cfargument name="idlogin" type="numeric" required="true">
		<cfargument name="tab_souscompte" type="array" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfset len = arrayLen(tab_souscompte)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updateCompteOpeLogin(idlogin,tab_souscompte[i],value)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateCompteOpeLogin" access="remote" returntype="any">
	<cfargument name="idlogin" type="numeric" required="true">
	<cfargument name="idsouscompte" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.updateCompteOpeLogin">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idlogin#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsouscompte#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>
<cffunction name="updateXCompteOpePool" access="remote" returntype="numeric">
		<cfargument name="idpool" type="numeric" required="true">
		<cfargument name="tab_souscompte" type="array" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfset len = arrayLen(tab_souscompte)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = updateCompteOpePool(idpool,tab_souscompte[i],value)>
		</cfloop>
		<cfreturn r>
</cffunction>
<cffunction name="updateCompteOpePool" access="remote" returntype="any">
	<cfargument name="idpool" type="numeric" required="true">
	<cfargument name="idsouscompte" type="numeric" required="true">
	<cfargument name="value" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.updateCompteOpePool">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpool#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsouscompte#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#value#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
</cffunction>

<!---
Procédures de démo
--->
<cffunction name="getTypeCommandeDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("ID,LIBELLE,CODE,NBPOOL,NBGESTIONNAIRE","Integer,VarChar,VarChar,Integer,Integer")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"LIBELLE","Bureautique");
		QuerySetCell(myQuery,"CODE","BU"); 
		QuerySetCell(myQuery,"NBPOOL",22);
		QuerySetCell(myQuery,"NBGESTIONNAIRE",15);  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","PM3");
		QuerySetCell(myQuery,"CODE","PM"); 
		QuerySetCell(myQuery,"NBPOOL",3);
		QuerySetCell(myQuery,"NBGESTIONNAIRE",5);  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"LIBELLE","MMP");
		QuerySetCell(myQuery,"CODE","MM"); 
		QuerySetCell(myQuery,"NBPOOL",2);
		QuerySetCell(myQuery,"NBGESTIONNAIRE",15);   
	</cfscript>
<cfreturn myQuery>
</cffunction>
<cffunction name="getPoolTypeCommandeDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("ID,LIBELLE,CODE,SELECTED","Integer,VarChar,VarChar,Integer")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"LIBELLE","REGION OUEST");
		QuerySetCell(myQuery,"CODE","Oui");
		
		QuerySetCell(myQuery,"SELECTED","0");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","RESGION SUD");
		QuerySetCell(myQuery,"CODE","Non");
		
			QuerySetCell(myQuery,"SELECTED","1");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"LIBELLE","POOL TEST");
		QuerySetCell(myQuery,"CODE","Oui");
		
		QuerySetCell(myQuery,"SELECTED","0");  
	</cfscript>
<cfreturn myQuery>
</cffunction>
<cffunction name="getContrainteSiteDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("ID,LIBELLE,TYPE","Integer,VarChar,VarChar")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"LIBELLE","SIM 0154841");
		QuerySetCell(myQuery,"TYPE","Equipement");
		
		
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","SIM 2");
		QuerySetCell(myQuery,"TYPE","Equipement");
		
			
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"LIBELLE","SIM 3");
		QuerySetCell(myQuery,"TYPE","Equipement");
		
		
	</cfscript>
<cfreturn myQuery>
</cffunction>
<cffunction name="getCompteOPDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("ID,LIBELLE,SELECTED","Integer,VarChar,Integer")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"LIBELLE","4616518676165");
		QuerySetCell(myQuery,"SELECTED","0");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","798981897");
		QuerySetCell(myQuery,"SELECTED","1");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"LIBELLE","156798619878691987");
		QuerySetCell(myQuery,"SELECTED","0");  
	</cfscript>
<cfreturn myQuery>
</cffunction>
<cffunction name="getActionProfilDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("ID,LIBELLE,WORKFLOW,SELECTED","Integer,VarChar,VarChar,VarChar")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"LIBELLE","ecriture");
		QuerySetCell(myQuery,"WORKFLOW","mobile"); 
			QuerySetCell(myQuery,"SELECTED","false");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"LIBELLE","lecture");
		QuerySetCell(myQuery,"WORKFLOW","mobile"); 
			QuerySetCell(myQuery,"SELECTED","true");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"LIBELLE","valider la comande");
		QuerySetCell(myQuery,"WORKFLOW","mobile"); 
			QuerySetCell(myQuery,"SELECTED","false");  
	</cfscript>
<cfreturn myQuery>
</cffunction>
<cffunction name="listeUserOfProfilDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("ID,NOM,PRENOM,IDPOOL,LIBELLEPOOL","Integer,VarChar,VarChar,Integer,VarChar")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",1);            
		QuerySetCell(myQuery,"NOM","vincent");
		QuerySetCell(myQuery,"PRENOM","le gallic");
		QuerySetCell(myQuery,"IDPOOL",1);
			QuerySetCell(myQuery,"LIBELLEPOOL","pool de démo");  
	</cfscript>
		<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",2);            
		QuerySetCell(myQuery,"NOM","test1");
		QuerySetCell(myQuery,"PRENOM","nom1");
		QuerySetCell(myQuery,"IDPOOL",1);
			QuerySetCell(myQuery,"LIBELLEPOOL","pool de démo 2 ");  
	</cfscript>
		<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"ID",3);            
		QuerySetCell(myQuery,"NOM","test2");
		QuerySetCell(myQuery,"PRENOM","nom2");
		QuerySetCell(myQuery,"IDPOOL",1);
			QuerySetCell(myQuery,"LIBELLEPOOL","pool de démo 3");  
	</cfscript>
	
<cfreturn myQuery>
</cffunction>
<cffunction name="getSitePoolDemo" access="remote" returntype="query">
	<cfargument name="idLogin" type="numeric" required="true">
<cfset var myQuery = QueryNew("IDSITE_PHYSIQUE,ADRESSE,SP_CODE_INTERNE,SP_COMMENTAIRE,SP_COMMUNE,SP_CODE_POSTAL,NOM_SITE,SP_REFERENCE,SELECTED","Integer,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"IDSITE_PHYSIQUE",1);            
		QuerySetCell(myQuery,"ADRESSE","13 AV...");
		QuerySetCell(myQuery,"SP_CODE_INTERNE","code demo");
		QuerySetCell(myQuery,"SP_COMMENTAIRE","com"); 
		QuerySetCell(myQuery,"SP_COMMUNE","PARIS"); 
		QuerySetCell(myQuery,"SP_CODE_POSTAL","75000"); 
		QuerySetCell(myQuery,"NOM_SITE","site de paris"); 
		QuerySetCell(myQuery,"SP_REFERENCE","ref"); 
		QuerySetCell(myQuery,"SELECTED","true");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"IDSITE_PHYSIQUE",2);            
		QuerySetCell(myQuery,"ADRESSE","13 AV...");
		QuerySetCell(myQuery,"SP_CODE_INTERNE","code demo");
		QuerySetCell(myQuery,"SP_COMMENTAIRE","com"); 
		QuerySetCell(myQuery,"SP_COMMUNE","PARIS"); 
		QuerySetCell(myQuery,"SP_CODE_POSTAL","75000"); 
		QuerySetCell(myQuery,"NOM_SITE","site de paris"); 
		QuerySetCell(myQuery,"SP_REFERENCE","ref"); 
		QuerySetCell(myQuery,"SELECTED","false");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"IDSITE_PHYSIQUE",3);            
		QuerySetCell(myQuery,"ADRESSE","13 AV...");
		QuerySetCell(myQuery,"SP_CODE_INTERNE","code demo");
		QuerySetCell(myQuery,"SP_COMMENTAIRE","com"); 
		QuerySetCell(myQuery,"SP_COMMUNE","PARIS"); 
		QuerySetCell(myQuery,"SP_CODE_POSTAL","75000"); 
		QuerySetCell(myQuery,"NOM_SITE","site de paris"); 
		QuerySetCell(myQuery,"SP_REFERENCE","ref"); 
		QuerySetCell(myQuery,"SELECTED","false");  
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"IDSITE_PHYSIQUE",4);            
		QuerySetCell(myQuery,"ADRESSE","13 AV...");
		QuerySetCell(myQuery,"SP_CODE_INTERNE","code demo");
		QuerySetCell(myQuery,"SP_COMMENTAIRE","com"); 
		QuerySetCell(myQuery,"SP_COMMUNE","PARIS"); 
		QuerySetCell(myQuery,"SP_CODE_POSTAL","75000"); 
		QuerySetCell(myQuery,"NOM_SITE","site de paris"); 
		QuerySetCell(myQuery,"SP_REFERENCE","ref"); 
		QuerySetCell(myQuery,"SELECTED","true");  
	</cfscript>
<cfreturn myQuery>
</cffunction>


</cfcomponent>
