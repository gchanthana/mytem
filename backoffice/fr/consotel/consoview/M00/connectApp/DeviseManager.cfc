﻿<cfcomponent displayname="fr.consotel.consoview.M00.connectApp.DeviseManager">
	<cffunction name="getDevise" access="remote" hint="sert à renvoyer la devise vers le front" returntype="String" >
		<cfreturn SESSION.USER.DEVISEINFOS.SYMBOL>
	</cffunction>
</cfcomponent>