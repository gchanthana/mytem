<cfdocument format="pdf" backgroundvisible="yes" fontembed = "yes" unit="CM" margintop="4">
<html>
	<head>
		<style type="text/css">
.titres_gros {
	background		: transparent;
	color			: #292820;
	font 			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	letter-spacing	: 2px;
	border-collapse	: collapse;
	text-align		: left;
}

.sous_titres_gros {
	background		: transparent;
	color			: #292820;
	font 			: normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	border-collapse	: collapse;
	text-align		: left;
}

table .resultat 
{
	font-size		: 100%;
	border			: 1px solid #CCCC99;
	background 		: #F4F2EF;
	border-collapse	: collapse;
	padding-right	: 2;
	padding-left	: 2;
}

.resultat th{
	background		: #EDCEA5;
	color			: #7F2114;
	font			: normal 7px EurostileBold,Verdana,Georgia, Arial,Helvetica,Serif;
	font-weight		: normal; 
	border-collapse	: collapse;
	padding			: 2px;
}

.resultat td {
	font 			: normal 7px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 2px;
}

.resultat .td_clair {
	background		: rgb(241,241,241);
	font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .td_fonce {
	background		: #E9E9E9;
	font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .td_rouge {
	background		: rgb(242,169,41);
	font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .td_out {
	layout-grid		: both loose 2px 2px;
	background		: #FFE1E1;
	border			: none;
	color			: rgb(0 , 0, 0);
	font			: normal 6px Tahoma,Arial, sans-serif;
	padding			: 2px;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .grid_small{
	background	: #FDFDFB;
	color		: rgb(0 , 0, 0);
	font		: normal 8px Tahoma,Arial, sans-serif;
	padding		: 0px;
}
		</style>
	</head>
	<body>
	<cfdocumentitem type="header">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
			<tr>
				<td width="13%">&nbsp;</td>
				<td width="2%" bgcolor="F4F188">&nbsp;</td>
				<td align="right" bgcolor="FDFCE1" width="85%" height="50" ><font face="Arial" size="2">CONTR&Ocirc;LE DE FACTURATION DES CONSOMMATIONS<br>HORS N° SP&Eacute;CIAUX&nbsp;&nbsp;</font></td>
			</tr>
		</table>
	</cfdocumentitem>
	<cfset ii = 0>
<cfoutput query="qGetData" group="REF_CLIENT">
	<cfoutput group="OFFRECLIENTID">
		<cfset nbAppelsTotal = 0>
		<cfset dureeTotal = 0>
		<cfset calculTotal = 0>
		<cfset remiseTotal = 0>
		<cfset opTotal = 0>
		<cfset diffTotal = 0>
		<cfset pourcTotal = 0>
		<table border="0" cellpadding="0" cellspacing="0" class="resultat" align="center" width="100%">

			<caption align="left">
					<font class="titres_gros">#RapportParams.raison_sociale# - #REF_CLIENT# : #session.user.prenom# #session.user.nom#</font><br>
					<font class="sous_titres_gros">Contr&ocirc;le de facturation des consommations hors N° spéciaux<br>
					Période analysée : <strong>#lsdateformat(parseDateTime(rapportParams.datedeb),"MMMM YYYY")#</strong> -
										<strong>#lsdateformat(parseDateTime(rapportParams.datefin),"MMMM YYYY")#</strong>.<br>
					<u>Offre : #OFFRENOM# (#OPNOM#)</u></font>
			</caption>
			<tr><td height="15px">&nbsp;</td></tr>
			<thead>
			<tr>
				<th align="center" nowrap="nowrap" valign="top" rowspan="2"><strong>Destination</strong></th>
				<th align="center" nowrap="nowrap" valign="top" rowspan="2"><strong>Nb appels</strong></th>
				<th align="center" nowrap="nowrap" valign="top" rowspan="2"><strong>Dur&eacute;e<br>(hh:mm:ss)</strong></th>
				<th colspan="3" align="center"><strong>Co&ucirc;t (&euro;)</strong></th>
				<th align="center" colspan="2"><strong>Ecart</strong></th>
				<th align="center" rowspan="2"><strong>Prix minute<br>moyen (cts &euro;)</strong></th>
			</tr>
			<tr>
				<th align="center">Calcul&eacute;e</th>
				<th align="center">Remis&eacute;e</th>
				<th align="center">Factur&eacute;e</th>
				<th align="center">(montant &euro;)</th>
				<th align="center">(pourcentage %)</th>
			</tr>
			</thead>
			<cfset tempclass="td_clair">
			<cfoutput>
				<cfset hh = int(#DUREE# / 60)>
				<cfset mm = int(#DUREE#) - (hh * 60)>
				<cfset ss = ((#DUREE#  * 60) - (mm * 60) - (hh * 3600))>
			<tr>
				<td class="#tempclass#">#PAYS#</td>
				<td class="#tempclass#" align="right">#LSNumberFormat(NBAPPEL)#</td>
				<td class="#tempclass#" align="right">#LSNumberFormat(hh)#:#mm#:#ss#</td>
				<td class="#tempclass#" align="right">#LSCurrencyFormat(CMPCOUT, "none")#</td>
				<td class="#tempclass#" align="right">#LSCurrencyFormat(COUTREDUIT, "none")#</td>
				<td class="#tempclass#" align="right">#LSCurrencyFormat(CMPCOUT2, "none")#</td>
				<td class="#tempclass#" align="right">#LSCurrencyFormat(DIFF_COUT, "none")#</td>
				<td class="#tempclass#" align="right">#LSCurrencyFormat(POURC_ERREUR, "none")# %</td>
				<cfset minuteMoyen = (CMPCOUT2 / (DUREE))>
				<td class="#tempclass#" align="right">#LSCurrencyFormat(minuteMoyen, "none")#</td>
			</tr>
				<cfset nbAppelsTotal = NBAPPEL + nbAppelsTotal>
				<cfset dureeTotal = DUREE + dureeTotal>
				<cfset calculTotal = CMPCOUT + calculTotal>
				<cfset remiseTotal = COUTREDUIT + remiseTotal>
				<cfset opTotal = CMPCOUT2 + opTotal>
				<cfset diffTotal = DIFF_COUT + diffTotal>
				<cfset ii = ii + 1>
			</cfoutput>
			<tr>
				<cfif opTotal NEQ 0>
					<cfset pourcTotal = ((diffTotal * 100) / opTotal)>
				<cfelse>
					<cfset pourcTotal = 0>
				</cfif>
				
				<cfset hhTotal = int(dureeTotal / 60)>
				<cfset mmTotal = int(dureeTotal) - (hhTotal * 60)>
				<cfset ssTotal = (dureeTotal * 60 - (mmTotal * 60) - (hhTotal * 3600))>
				
				
				<td align="left" style="border:0F339D 1px solid;"><b>TOTAL</b></td>
				<td align="right" style="border:0F339D 1px solid;"><b>#LSNumberFormat(nbAppelsTotal)#</b></td>
				<td align="right" style="border:0F339D 1px solid;"><b>#LSNumberFormat(hhTotal)#:#mmTotal#:#ssTotal#</b></td>
				<td align="right" style="border:0F339D 1px solid;"><b>#lscurrencyformat(calculTotal,"none")#</b></td>
				<td align="right" style="border:0F339D 1px solid;"><b>#lscurrencyformat(remiseTotal,"none")#</b></td>
				<td align="right" style="border:0F339D 1px solid;"><b>#lscurrencyformat(opTotal,"none")#</b></td>
				<td align="right" style="border:0F339D 1px solid;"><b>#LSCurrencyFormat(diffTotal,"none")#</b></td>
				<td align="right" style="border:0F339D 1px solid;"><b>#LSCurrencyFormat(pourcTotal,"none")# %</b></td>
				<td align="right" style="border:0F339D 1px solid;">&nbsp;</td>	
			</tr>
		</table>
		<cfif ii LT qGetData.recordcount>
			<cfdocumentitem type="pagebreak"/>
		</cfif>
		</cfoutput>
		</cfoutput>
		<cfdocumentitem type="footer">
		<style>
		.tfooter {
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
		}
		</style>
		<center>
		<table border="0" cellpadding="0" cellspacing="0" width="560" align="center">
			<tr>
				<cfoutput>
					<td align="center" class="tfooter" width="560">Copyright CONSOTEL © 2000 / 2007 - page #cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</td>
				</cfoutput>
			</tr>
		</table>
		</center>
	</cfdocumentitem>
	</body>
</html> 
</cfdocument>
