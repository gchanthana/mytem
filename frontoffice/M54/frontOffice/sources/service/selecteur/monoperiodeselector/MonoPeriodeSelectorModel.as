package service.selecteur.monoperiodeselector
{
	import event.selector.MonoPeriodeEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import service.selecteur.monoperiodeselector.cache.CacheServiceSingleton;
	
	import vo.selector.ValeurSelectorVo;

	internal class MonoPeriodeSelectorModel  extends EventDispatcher
	{
		private var _valuesMonoPeriode :ArrayCollection;
	
		
		public function MonoPeriodeSelectorModel():void
		{
		}
		public function get getValuesMonoPeriode():ArrayCollection
		{
			return _valuesMonoPeriode;
		}
		internal function updateValues(value:ArrayCollection):void
		{
			if(CacheServiceSingleton.getInstance().getCache(MonoPeriodeSelectorService.CACHE_KEY)  == null )
			{
				_valuesMonoPeriode=new ArrayCollection();
				
				var valeurSelector:ValeurSelectorVo;
				
				for(var i:int=0;i<value.length;i++)
				{
					valeurSelector = new ValeurSelectorVo();
					valeurSelector.fill(value[i],i);
					_valuesMonoPeriode.addItem(valeurSelector); 
				}
				
				CacheServiceSingleton.getInstance().setCache(MonoPeriodeSelectorService.CACHE_KEY,_valuesMonoPeriode)
			}
			else
			{
				_valuesMonoPeriode = CacheServiceSingleton.getInstance().getCache(MonoPeriodeSelectorService.CACHE_KEY);
			}
			
			this.dispatchEvent(new MonoPeriodeEvent(MonoPeriodeEvent.USER_MONO_PERIODE_SELECTOR_EVENT));/** capturé dans MonoPeriodeSelectorImpl */
		}
	}
}