<cfcomponent author="Cedric" displayname="fr.saaswedo.api.remoting.soap.WSClient" extends="fr.saaswedo.api.remoting.http.HttpClient"
hint="Implémentation pour WebService (SOAP): Le endpoint est soit l'URL du WSDL soit le nom correspondant enregistré dans l'administration ColdFusion.
Cette implémentation est basée sur HttpClient uniquement pour des raisons techniques mais elle utilise les fonctions SOAP de ColdFusion (Apache Axis).
<br>L'accès à un WebService peut se faire avec une des méthodes suivantes :
<br>- Utiliser invoke() : C'est la méthode recommandée car elle dispatche les exceptions avec l'implémentation IErrorTarget qui convient.
<br>- Utiliser directement l'instance Stub (Axis) retournée par getServiceCall().getStub() : Les exceptions ne seront pas dispatchées">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.remoting.soap.WSClient">
		<cfreturn "fr.saaswedo.api.remoting.soap.WSClient">
	</cffunction>
	
	<cffunction access="public" name="RPC_PARAM" returntype="String" hint="Type correspondant à un paramètre d'une méthode d'un WebService">
		<cfreturn "RPC_PARAM">
	</cffunction>
	
	<cffunction access="public" name="getServiceCall" returntype="Any" description="LOCK:ReadOnly" hint="fr.saaswedo.api.remoting.ext.StubWrapper">
		<cfargument name="serviceId" type="String" required="true" hint="Identifiant du service obtenu avec register()">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<!--- L'implémentation appel de service est paramétré à partir de createService() --->
			<cfset var sourceService=getService(ARGUMENTS.serviceId)>
			<cfset var wsdlReference=sourceService.getUrl()>
			<cfset var serviceUser=sourceService.getUsername()>
			<cfset var servicePwd=sourceService.getPassword()>
			<cfset serviceUser=isDefined("serviceUser") ? serviceUser:"">
			<cfset servicePwd=isDefined("servicePwd") ? servicePwd:"">
			<!--- Instanciation et paramétrage du WebService (Stub) --->
			<cfset var stub=createObject("webservice",wsdlReference)>
			<cfset var userAgentKey=createObject("java","org.apache.axis.transport.http.HTTPConstants")>
			<cfset stub._setProperty(userAgentKey.HEADER_USER_AGENT,"SAASWEDO")>
			<!--- Wrapper du Stub --->
			<cfset var stubWrapper=new fr.saaswedo.api.remoting.ext.StubWrapper(stub)>
			<!--- Crédentiels de l'appel de service --->
			<cfset setCallCrendentials(stubWrapper,serviceUser,servicePwd)>
			<cfreturn stubWrapper>
		</cflock>
	</cffunction>

	<cffunction access="public" name="invoke" returntype="Any"
	hint="Envoie une requete SOAP en utilisant serviceCall et retourne le résultat. Celui ci peut etre indéfini si la méthode ne retourne rien (void). 
	Lève une erreur AXIS.REQUEST_ERROR si une exception est capturée durant l'appel. Dans ce cas le paramètre errorTarget de
	IErrorHandler.handleError() est l'instance de l'appel. Toute exception capturée est dispatchée">
		<cfargument name="serviceCall" type="Any" required="true" hint="fr.saaswedo.api.remoting.ext.StubWrapper">
		<cfargument name="method" type="String" required="true" hint="Méthode du WebService">
		<cfargument name="parameters" type="Array" required="true" hint="Paramètres pour la méthode et qui seront ajoutés par ordre d'apparition.
		Chaque élément doit etre une structure contenant les clés : TYPE e.g RPC_PARAM(),NAME,VALUE. <b>Il est recommandé de grouper par TYPE</b>">
		<cfset var errorTarget=THIS>
		<cftry>
			<cfset var stubWrapper=ARGUMENTS.serviceCall>
			<!--- Vérification de la validité de la méthode HTTP --->
			<cfset checkMethod(ARGUMENTS.method)>
			<!--- Vérification de la validité des paramètres de l'appel au service --->
			<cfset setCallParameters(stubWrapper,ARGUMENTS.parameters)>		
			<!--- IErrorTarget devient l'appel de service et reste inchangé pour la suite --->
			<cfset var errorCode=GLOBALS().CONST("WS.REQUEST_ERROR")>
			<cfset errorTarget=stubWrapper>
			<!--- Envoi de la requete HTTP avec l'appel --->
			<cfset var parameterCount=arrayLen(ARGUMENTS.parameters)>
			<cftry>
				<cfset var params=ARGUMENTS.parameters>
				<cfinvoke webservice="#stubWrapper.getStub()#" method="#ARGUMENTS.method#" returnvariable="wsResult">
					<cfloop index="i" from="1" to="#parameterCount#">
						<cfinvokeargument name="#params[i].NAME#" value="#params[i].VALUE#">
					</cfloop>
				</cfinvoke>
				<cfcatch type="Any">
					<cfthrow type="Custom" errorCode="#errorCode#" message="#CFCATCH.message#" detail="#CFCATCH.detail#">
				</cfcatch>
			</cftry>
			<!--- Si la méthode du WebService ne retourne rien (void) alors le résultat de l'appel doit etre indéfini --->
			<cfreturn isDefined("wsResult") ? wsResult:javaCast("null",0)>
			<cfcatch type="Any">
				<cfset dispatchErrorAs(errorTarget,CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- =========== Fonctions concernant l'implémentation : Appel de service =========== --->

	<cffunction access="private" name="setCallParameter" returntype="void" hint="Appele la fonction de vérification de la validité du paramètre">
		<cfargument name="serviceCall" type="fr.saaswedo.api.remoting.ext.StubWrapper" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre à fournir à serviceCall. Structure avec les clés NAME,VALUE,TYPE">
		<cfset var httpCall=ARGUMENTS.serviceCall>
		<cfset var httpParam=ARGUMENTS.parameter>
		<!--- Vérification de la validité du paramètre --->
		<cfset checkParameter(httpParam)>
	</cffunction>

	<cffunction access="private" name="setCallCrendentials" returntype="void" hint="Défini les crédentiels HTTP en utilisant le header
	BASIC_AUTH_HEADER(). Aucune action n'est effectuée si username n'est pas renseigné. Le username fourni est vérifiée avec checkUsername()">
		<cfargument name="serviceCall" type="fr.saaswedo.api.remoting.ext.StubWrapper" required="true" hint="Retourné par getServiceCall()">
		<cfargument name="username" type="String" required="false" default="" hint="Login (HTTP Basic)">
		<cfargument name="password" type="String" required="false" default="" hint="Password (HTTP Basic)">
		<!--- Vérification de la validité du username --->
		<cfset checkUsername(ARGUMENTS.username)>
		<!--- Les crédentiels sont définis si le username fourni est renseigné --->
		<cfif ARGUMENTS.username NEQ "">
			<cfset var stub=ARGUMENTS.serviceCall.getStub()>
			<cfset stub.setUsername(ARGUMENTS.username)>
			<cfset stub.setPassword(ARGUMENTS.password)>
		</cfif>
	</cffunction>

	<cffunction access="private" name="checkMethod" returntype="void"
	hint="Vérifie la validité de la méthode method et lève une exception si elle est invalide ou non supportée">
		<cfargument name="method" type="String" required="true" hint="Méthode HTTP">
		<cfset var rpcMethod=ARGUMENTS.method>
		<cfset var rpcMethodValue=TRIM(rpcMethod)>
		<cfset var code=GLOBALS().CONST("WS.ERROR")>
		<cfif rpcMethodValue EQ "">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid method '#rpcMethod#'" detail="Value is empty">
		<cfelseif rpcMethodValue NEQ rpcMethod>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid method '#rpcMethod#'" detail="Contains extra spaces">
		<!--- La méthode ne doit pas contenir d'espaces --->
		<cfelseif REFindNoCase(" ",rpcMethod) GT 1>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid method '#rpcMethod#'" detail="Cannot include spaces">
		</cfif>
	</cffunction>

	<cffunction access="private" name="isParameterTypeSupported" returntype="Boolean" hint="Retourne TRUE si le paramètre est supporté">
		<cfargument name="parameterType" type="String" required="true" hint="Type de paramètre">
		<!--- Liste des types de paramètres CFHTTP supportés --->
		<cfreturn (ARGUMENTS.parameterType EQ RPC_PARAM())>
	</cffunction>

	<cffunction access="private" name="checkParameter" returntype="void"
	hint="Appele la fonction parent puis vérifie si parameter est valide et lève une exception s'il est invalide ou non supporté">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre HTTP. Structure contenant les clés : TYPE,NAME,VALUE">
		<cfset var httpParam=ARGUMENTS.parameter>
		<cfset SUPER.checkParameter(httpParam)>
		<!--- Le nom d'un paramètre RPC ne doit pas contenir d'espace --->
		<cfif httpParam.TYPE EQ RPC_PARAM()>
			<cfif REFindNoCase(" ",httpParam.NAME) GT 1>
				<cfset var code=GLOBALS().CONST("WS.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="Invalid parameter name '#httpParam.NAME#'" detail="Cannot include spaces">
			</cfif>
		</cfif>
	</cffunction>
	
	<!--- =========== Fonctions de gestion du endpoint =========== --->
	
	<cffunction access="private" name="checkEndpoint" returntype="void" hint="Vérifie endpoint et lève une exception si elle est invalide">
		<cfargument name="endpoint" type="String" required="true" hint="Endpoint d'un service">
		<cfset var endpointValue=TRIM(ARGUMENTS.endpoint)>
		<cfset var code=GLOBALS().CONST("WS.ERROR")>
		<cfif endpointValue EQ "">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid endpoint '#ARGUMENTS.endpoint#'" detail="Value is empty">
		<cfelseif endpointValue NEQ ARGUMENTS.endpoint>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid endpoint '#ARGUMENTS.endpoint#'" detail="Contains extra spaces">
		</cfif>
	</cffunction>
</cfcomponent>