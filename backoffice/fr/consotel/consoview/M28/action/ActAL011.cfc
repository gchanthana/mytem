﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">

	<cffunction name="executeAct" access="remote" returntype="Struct" output="false"
				hint="retourne la liste des fonctions par utilisateur">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getListeFonctionClient">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" type="in" value="#data.idRacine#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_lang" type="in" value="#getCodeLangue()#">
				<cfprocresult name="p_retour">
			</cfstoredproc> <!--- appel à la procédure stockée décommenté par Florian--->
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AL011", 1, qResult)>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL011", 0, ThrowCfCatchError("AL011",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>