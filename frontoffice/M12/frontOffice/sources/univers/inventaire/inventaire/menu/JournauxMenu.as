package univers.inventaire.inventaire.menu
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.controls.Menu;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.FlexEvent;
	
	import univers.inventaire.inventaire.journaux.AfficherJournalEvent;
	import univers.inventaire.inventaire.menu.event.JournauxEvent;
	
	[Event (name="dateChange",type="univers.inventaire.inventaire.menu.event.JournauxEvent")]
	
	/**
	 * Classe Onglet Journaux
	 * 
	 * */
	public class JournauxMenu extends JournauxMenuIHM
	{
		
		//Référence vres le menu du PopUpMenuButton 'Format' 
		private var myMenu:Menu;
		
		//Constante definissant le format CSV
		private const CSV : String = "CSV";

		//Constante definissant le format PDF		
		private const PDF : String = "PDF";;

		//Constante definissant le format XLS			
		private const XLS : String = "XLS";
		
		//Journal à exporter
		private var journal : String;
		
		//Libelle du journal à exporter
		private var libelleJournal : String;
		
		   
		
		/**
		 * Constructeur 
		 * */
		public function JournauxMenu()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		
		//Initialisation de l'IHM 
		//Affectation des ecouteurs d'évènements
		private function initIHM(event:Event):void 
		{
			//initMenu();
			myGrid.addEventListener(Event.CHANGE,afficherJournal);
			btExport.addEventListener(MouseEvent.CLICK,exporter);			 
			dcDateRef.addEventListener(CalendarLayoutChangeEvent.CHANGE, _dcDateRefChangeHandler);
			
		}
		
		//Formate l'arbre
		private function initTree():void
		{
			
		}
        
        //Affiche le journal selectionné
        private function afficherJournal( ev : Event):void{
        	if (myGrid.selectedIndex != -1){
        		try{
	        		lblError.text = "";
	        		var evtObj : AfficherJournalEvent = new AfficherJournalEvent();
	        		evtObj.typeJournal = String(myGrid.selectedItem.type);
	        		journal = String(myGrid.selectedItem.type);
	        		libelleJournal = String(myGrid.selectedItem.rapport);
	        		dispatchEvent(evtObj);
	        		txtDescription.text = String(myGrid.selectedItem.rapport);
	        	}catch(er : Error){
	        		trace(er.message,er.getStackTrace());
	        	}	
        	}        	
        }
        
        //Export ----------------------------------------------------------------------------------
        
        //Handler du clique sur le bouton exporter
        private function exporter(me : MouseEvent):void
        {	
        	if (myGrid.selectedIndex == -1)
        	{	
        		lblError.text = "Sélectionnez un journal";
        	}
        	else if(myGrid.selectedItem.showDate && dcDateRef.selectedDate == null)
        	{
        		lblError.text = "Sélectionnez une date de référence";
        	}
        	else if(cboFormat.selectedItem == null)
        	{
        		lblError.text = "Sélectionnez un format de sortie";
        	} 
        	else
        	{
        		this["exporter"+cboFormat.selectedItem.value]();
        	}        	       
        }
        
        //Exporte le journal affiché au format CSV
        private function exporterCSV():void{        
        	displayExport(CSV);
        }
       
        //Exporte le journal affiché au format PDF 
        private function exporterPDF():void{        
        	displayExport(PDF);
        }
		
		 //Exporte le journal affiché au format XLS        
        private function exporterXLS():void{
        	displayExport(XLS);
        }
        
         //Exporte le journal affiché suivant le format passé en paramètre        
         //param in format le format sous lequel on souhaite exporter
        private function displayExport(format : String):void {
			 
				var url:String = moduleJournauxIHM.NonSecureUrlBackoffice+"/fr/consotel/consoview/cfm/cycledevie/journaux/Display_Journal.cfm";
	            var variables:URLVariables = new URLVariables();
	            variables.FORMAT = format;	  
	            variables.JOURNAL =  journal; 
				variables.JOURNAL_LIBELLE = libelleJournal;	 
				variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
				variables.PERIMETRE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				
				if(dcDateRef.selectedDate)
				{
					variables.DATE_REFERENCE =  DateFunction.formatDateAsString(dcDateRef.selectedDate);	
				}
				else
				{
					variables.DATE_REFERENCE = "";
				}
				 
	            
	            var request:URLRequest = new URLRequest(url);
	            request.data = variables;
	            request.method=URLRequestMethod.POST;
	            navigateToURL(request,"_blank");
        } 			    
        
        //Fin Export ------------------------------------------------------------------------------
        
        
        //Affiche le journal selectionné        
        private function afficherRapport( ev : Event):void
        {	 
        }
        
        //Affiche la synthese pour le noeud selectionné
        private function afficherJournalDuNoeud():void
        {
        }
        
		/**
		*  Fait les actrions nécessaire au changement de périmetre
		* */
		public function onPerimetreChange():void
		{	
		}
		
		public function updateDate(value:Date):void
		{	
			dcDateRef.selectedDate = value;	
		}

		protected function _dcDateRefChangeHandler(event:CalendarLayoutChangeEvent):void
		{
			var evt:JournauxEvent = new JournauxEvent(JournauxEvent.DATE_CHANGE,true);
			evt.date_reference = event.newDate
			dispatchEvent(evt);
		}
	}
}