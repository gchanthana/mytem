package rapportPilotage.vo
{

	import rapportPilotage.ihm.ModuleRapportPilotageIHM;
	import rapportPilotage.ihm.ModuleRapportPilotageImpl;

	public class ApiReportingSingleton
	{
		private static var _instance : ApiReportingSingleton = null;// contient l'instance unique de la classe (la référence vers l'objet)
		// empêche l'instanciation de la classe à partir du constructeur car en AS3 les constructeur sont tourjours public et 
		// on peux faire new config de par tout (donc plusieurs instance)
		
		private static var _allowInstance:Boolean = false; 
		private static var _composantRapport:ModuleRapportPilotageIHM
		private static var _isPopUP :Boolean=false;
		private static var _nbInstanciation :int=0; // le nb instanciation de ModuleRapportIHM comme composant 
		private static var _nbInstanciationPopUP :int=0; // le nb instanciation de ModuleRapportIHM comme  pop up 
		
		
		public function ApiReportingSingleton()
		{
			if (!_allowInstance)// test s'il est possible d'instancier la classe
			{
				trace('This class cannot be instantiated from the constructor' ); 
			}
			else // initialisation des paramètres de l'objet
			{
				_composantRapport =new ModuleRapportPilotageIHM();
			}
		}	

		public static function getInstance():ApiReportingSingleton
		{	
			if (ApiReportingSingleton._instance==null) // si l'instance n'existe pas, on la crée
			{
				_allowInstance = true;// autorise la création d'une instance
				ApiReportingSingleton._instance = new ApiReportingSingleton();
				_allowInstance = false;// désactive la création d'autre instance
			}
			return ApiReportingSingleton._instance;// renvoie toujours la même instance de la classe
		}
		public function get moduleRapport() : ModuleRapportPilotageImpl
		{
			return _composantRapport;	
		}
		
		public function afficherPOpup():void
		{
			ApiReportingSingleton.isPopUP=true;
			_composantRapport.showPopUp();
		}
		
		public static function get nbInstanciation():int
		{
			return _nbInstanciation;
		}
		
		public static function set nbInstanciation(value:int):void
		{
			_nbInstanciation = value;
		}
		
		public static function get isPopUP():Boolean
		{
			return _isPopUP;
		}
		
		public static function set isPopUP(value:Boolean):void
		{
			_isPopUP = value;
		}
		
		public static function get nbInstanciationPopUP():int
		{
			return _nbInstanciationPopUP;
		}
		
		public static function set nbInstanciationPopUP(value:int):void
		{
			_nbInstanciationPopUP = value;
		}
		public function initialiserSingleton():void
		{	
			ApiReportingSingleton._instance=null;
		}
	}
}