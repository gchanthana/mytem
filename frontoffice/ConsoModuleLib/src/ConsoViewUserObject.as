package {
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class ConsoViewUserObject implements IConsoViewUserObject {
		private var nomField:String;
		private var prenomField:String;
		private var emailField:String;
		private var accessIdField:int;
		private var _CODEAPPLICATION:int;
		private var _GLOBALIZATION:String = "fr_FR";
		private var _CLIENTID:Number = 0;  
		private var _LIBELLE_ACCES:String;
		private var _TYPE_LOGIN:int;
		
		public function ConsoViewUserObject():void {
			nomField = prenomField = emailField = null;
			accessIdField = 0;
		}
		
		internal function updateUserInfos(userInfos:Object):Boolean {
			nomField = new String(userInfos.NOM);
			prenomField = new String(userInfos.PRENOM);
			emailField = new String(userInfos.EMAIL);
			accessIdField = new int(userInfos.CLIENTACCESSID);
			_CODEAPPLICATION = userInfos.CODEAPPLICATION;
			_GLOBALIZATION = userInfos.GLOBALIZATION;
			_CLIENTID = userInfos.CLIENTID;
			_LIBELLE_ACCES = ""
			_TYPE_LOGIN = parseInt(userInfos.TYPE_LOGIN)	
			return true;
		}

		internal function updateLibelleAcces():void
		{
			var universSelected:XMLList = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData.descendants("UNIVERS").(@toggled == true)
			var fonctionSelected:XMLList;
			var chemin:String = ""
			
			if(universSelected.length() == 1)
				chemin = universSelected.@LBL
			
			fonctionSelected = universSelected.descendants("FONCTION").(@toggled == true)
			if(fonctionSelected.length() == 1)
				chemin += ' : '+fonctionSelected.@LBL
			
			CvAccessManager.getUserObject().LIBELLE_ACCES = chemin
		}

		public function get NOM():String {
			return nomField;
		}
		
		public function get PRENOM():String {
			return prenomField;
		}
		
		public function get EMAIL():String {
			return emailField;
		}
		
		public function get CLIENTACCESSID():int {
			return accessIdField;
		}
				
		public function get CODEAPPLICATION():int
		{ 
			return _CODEAPPLICATION;
		}
		
		public function get TYPE_LOGIN():int
		{ 
			return _TYPE_LOGIN;
		}
		
		public function get CLIENTID():Number
		{ 
			return _CLIENTID;
		}
		
		public function get GLOBALIZATION():String
		{
			return _GLOBALIZATION;
		}
		
		internal function clearUser():void {
			nomField = prenomField = emailField = null;
			accessIdField = 0;
			_CODEAPPLICATION = 0;
		 	_GLOBALIZATION = "fr_FR";
			_CLIENTID = 0;
		}

		public function set LIBELLE_ACCES(value:String):void
		{
			_LIBELLE_ACCES = value;
		}

		public function get LIBELLE_ACCES():String
		{
			return _LIBELLE_ACCES;
		}
	}
}
