package catalogue.strategie.strategieconcrete
{
import mx.collections.ArrayCollection;
import mx.resources.ResourceManager;
import mx.rpc.AbstractOperation;
import mx.rpc.events.ResultEvent;

import catalogue.event.CatalogueEvent;
import catalogue.strategie.ICatalogue;
import catalogue.vo.AbonnementVO;
import catalogue.vo.ConstructeurVO;
import catalogue.vo.EquipementCatalogueVO;
import catalogue.vo.ParametreSearchCatalogueVO;
import catalogue.vo.RevendeurVO;

import composants.util.DateFunction;

import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

import paginatedatagrid.pagination.vo.ItemIntervalleVO;



	[Bindable]
	public class ClientCS extends AbstractCatalogueCS implements ICatalogue
	{ 
		
		private var _listeEquipementCatalogue:ArrayCollection = new ArrayCollection;
		
		
		public function ClientCS()
		{
			
		}
		public function rechercheListeDataCatalogue(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"rechercheEquipementClient",
							rechercheEquipementConstructeur_handler,null);
			
			RemoteObjectUtil.callService(opData,
												parametresRechercheVO.SEARCH_TEXT,
												parametresRechercheVO.ORDER_BY,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,
												parametresRechercheVO.IDCONSTRUCTEUR,
												parametresRechercheVO.IDTYPEEQUIPEMENT,
												parametresRechercheVO.IDCLASSE,
												parametresRechercheVO.IDCLIENT,
												parametresRechercheVO.IDREVENDEUR,
												parametresRechercheVO.IDTYPECMD,
												parametresRechercheVO.IDSEGMENT,
												parametresRechercheVO.IDETAT
												);
		}
		override public function rechercheAbonnementCatalogue(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Abonnement",
							"searchAboOperateurClient",
							function rechercheAbonnementCatalogue_handler(re : ResultEvent):void
							{
								var tmpArr:Array = (re.result as ArrayCollection).source;
								listeAbonnementCatalogue = new ArrayCollection();
								
								for(var i : int=0; i < tmpArr.length;i++)
								{	
									var item:AbonnementVO  			= new AbonnementVO();
									
									item.fill(tmpArr[i]);
									
									listeAbonnementCatalogue.addItem(item);
								}
							},null);
			
			RemoteObjectUtil.callService(opData,
												parametresRechercheVO.SEARCH_TEXT,
												parametresRechercheVO.ORDER_BY,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,
												parametresRechercheVO.IDCLIENT,
												parametresRechercheVO.IDTYPECMD,
												parametresRechercheVO.IDOP,
												parametresRechercheVO.DEFINIDEFAUT
												);
		}
		
		
		private function formatIntToBoolean(intBool:int):Boolean
		{
			var bool:Boolean = false;
			
			if(intBool > 0)
				bool = true;
			
			return bool;
		}
		
		private function addTypeThemeID(acces:int):int
		{
			var idthemetype:int = 1;//1->ABO, 2->OPT
			
			if(acces == 0)
				idthemetype = 2;
			
			return idthemetype;
		}
		
	 	public function addEquipement(equip : EquipementCatalogueVO):void
		{
			/*
			impl
			*/			
		} 
		public function updateEquipement(equip : EquipementCatalogueVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"updateEquipementClientV2",
							updateEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData, equip.IDEQUIPEMENT,
												equip.MODELE,
												equip.CODE_INTERNE,
												Number(equip.PRIX_1), 
												Number(equip.PRIX_2), 
												Number(equip.PRIX_3), 
												Number(equip.PRIX_4), 
												Number(equip.PRIX_5), 
												Number(equip.PRIX_6), 
												Number(equip.PRIX_7), 
												Number(equip.PRIX_8),
												Number(equip.PRIX_CATALOGUE),
												DateFunction.formatDateAsInverseString(equip.DATEVALIDITE_CLT),
												Number(equip.SUSPENDU_CLT))
				
			/*RemoteObjectUtil.callService(opData, equip.IDEQUIPEMENT,
				equip.MODELE,
				equip.CODE_INTERNE,
				equip.DATEVALIDITE_CLT,
				equip.SUSPENDU_CLT,
				Number(equip.PRIX_1), 
				Number(equip.PRIX_2), 
				Number(equip.PRIX_3), 
				Number(equip.PRIX_4), 
				Number(equip.PRIX_5), 
				Number(equip.PRIX_6), 
				Number(equip.PRIX_7), 
				Number(equip.PRIX_8),
				Number(equip.PRIX_CATALOGUE))	*/
		}
		
		public function removeEquipement(equip : EquipementCatalogueVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"removeEquClient",
							removeEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData,equip.IDEQUIPEMENT);
		}
		override public function removeAbonnement(idClient : int,abo : AbonnementVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Abonnement",
							"RemoveAboClient",
							removeAbo_handler,null);
			
			RemoteObjectUtil.callService(opData,idClient,abo.IDABO);
		}
		private function removeAbo_handler(re : ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.REMOVE_ABO_COMPLETE,true));
		}
		private function removeEquipement_handler(re : ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,true));
		}
		private function addEquipement_handler(evt : ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.ADD_EQUIPEMENT_COMPLETE,true,false,evt.result));
		}
		private function updateEquipement_handler(evt : ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_EQUIPEMENT_COMPLETE,true,false,evt.result));
		}
	
		private function rechercheEquipementConstructeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeDataCatalogue = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : EquipementCatalogueVO = new EquipementCatalogueVO();
						
				item.IDEQUIPEMENT=tmpArr[i].IDEQUIPEMENT;
				item.IDEQUIPFOURNISSEUR=tmpArr[i].IDEQUIPEMENT_FOURNIS;
				item.REFERENCE=tmpArr[i].REFERENCE;
				item.REFCONSTRUCTEUR=tmpArr[i].REFERENCECONSTRUCTEUR;
				item.REFDISTRIBUTEUR=tmpArr[i].REFERENCEDISTRIBUTEUR; 
				item.TYPE=tmpArr[i].TYPE;
				item.CLASSE=tmpArr[i].CLASSE;
				item.TYPE_CREATION = tmpArr[i].TYPE_CREATION;
				item.ORIGINE=(item.TYPE_CREATION == 1)?ResourceManager.getInstance().getString('M24','Manuel'):tmpArr[i].ORIGINE;
				//item.ORIGINE=tmpArr[i].ORIGINE;
				
				
				item.DATEVALIDITE=tmpArr[i].DATEVALIDITE;
				item.SUSPENDU=tmpArr[i].SUSPENDU;
				item.DATEVALIDITE_CLT=tmpArr[i].DATEVALIDITE_CLT;
				item.SUSPENDU_CLT=tmpArr[i].SUSPENDU_CLT;
				item.DELAIALERT=tmpArr[i].DELAIALERT;
				
				item.MODELE=tmpArr[i].MODELE;
				item.NBRECORD=tmpArr[i].NBRECORD;
				item.IDCONSTRUCTEUR = tmpArr[i].IDCONSTRUCTEUR;
				item.IDCLASSE = tmpArr[i].IDCATEGORIE;
				item.IDTYPE = tmpArr[i].IDTYPE;
				item.CODE_INTERNE = tmpArr[i].CODE_INTERNE;
				item.PRIX_CATALOGUE=Number(tmpArr[i].PRIX_CATALOGUE).toFixed(2);
				item.PRIX_1=Number(tmpArr[i].TARIF12MOIS).toFixed(2);
				item.PRIX_2=Number(tmpArr[i].TARIF24MOIS).toFixed(2);
				item.PRIX_3=Number(tmpArr[i].TARIF36MOIS).toFixed(2);
				item.PRIX_4=Number(tmpArr[i].TARIF48MOIS).toFixed(2);
				item.PRIX_5=Number(tmpArr[i].TARIF12MOISR).toFixed(2);
				item.PRIX_6=Number(tmpArr[i].TARIF24MOISR).toFixed(2);
				item.PRIX_7=Number(tmpArr[i].TARIF36MOISR).toFixed(2);
				item.PRIX_8=Number(tmpArr[i].TARIF48MOISR).toFixed(2);
				
				item.NIVEAU = tmpArr[i].NIVEAU;
				listeDataCatalogue.addItem(item);
			}
		}
		override public function rechercheRevendeur(idClient : Number = -1):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"getDistribOfRacine",
							rechercheRevendeur_handler,null);
			
			RemoteObjectUtil.callService(opData,idClient);
		}
		
		override public function updateNiveau(idEquip : int,idClient : int, niveau : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"UpdateNiveauxEquip",
							UpdateNiveauxEquip_handler,null);
			
			RemoteObjectUtil.callService(opData,idEquip,niveau);
		}
		override public function updateNiveauAbo(idAbo : int,idClient : int, niveau : int):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Abonnement",
							"UpdateNiveauxAbo",
							UpdateNiveauxEquip_handler,null);
			
			RemoteObjectUtil.callService(opData,idAbo,idClient,niveau);
		}
		private function UpdateNiveauxEquip_handler(re : ResultEvent):void
		{
			
		}
		override protected function rechercheRevendeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeRevendeur = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : RevendeurVO = new RevendeurVO();
				item.ID_REVENDEUR=tmpArr[i].IDREVENDEUR;
				item.LIBELLE_REVENDEUR=tmpArr[i].LIBELLE;
				item.SYMBOL_DEVISE = tmpArr[i].DEVISE;
				listeRevendeur.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_REVENDEUR_EQUIPEMENT_COMPLETE,true));
		}
		
		override public function listeConstructeurRacine_handler(re : ResultEvent):void
		{ 
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeConstructeur = new ArrayCollection();
			
			var  specialitem : ConstructeurVO = new ConstructeurVO();
			specialitem.ID_CONSTRUCTEUR=-1;
			specialitem.LIBELLE_CONSTRUCTEUR=ResourceManager.getInstance().getString('M24','Tous');
			listeConstructeur.addItem(specialitem);
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : ConstructeurVO = new ConstructeurVO();
				item.ID_CONSTRUCTEUR=tmpArr[i].IDCONSTRUCTEUR;
				item.LIBELLE_CONSTRUCTEUR=tmpArr[i].LIBELLECONSTRUCTEUR;
				listeConstructeur.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,true));
		}
		
		public function get listeDataCatalogue():ArrayCollection
		{
			return _listeEquipementCatalogue;
		}
		public function set listeDataCatalogue(value:ArrayCollection):void
		{
			_listeEquipementCatalogue = value;
		}
	
	
	}
}