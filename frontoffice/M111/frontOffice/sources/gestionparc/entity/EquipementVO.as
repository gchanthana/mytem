package gestionparc.entity
{
	/**
	 * Value Object représentant les propriétés pour un équipement.
	 */
	[Bindable]
	public class EquipementVO
	{
		public static const CATEGORIE_TERMINAUX:int = 4;
		public static const TYPE_MOBILE:int = 70;
		
		public var idEquipement:int=-1;
		public var numSerieEquipement:String="";
		public var numIemiEquipement:String="";

		public var libelleEquipement:String=""; // equivalent a un modele pour un term, ou "CARTE SIM" pour une sim 

		public var idCategorie:int=-1; // la catégorie implique un ensemble de type
		public var categorie:String="";

		public var idTypeEquipement:int=-1; // les types sont des sous ensembles des catégories
		public var typeEquipement:String="";

		public var idSite:int=-1;
		public var idEmplacement:int=-1;

		public var revendeur:RevendeurVO=null; // revendeur = fournisseur
		public var nomRevendeur:String=""; // pour le dataField

		public var idFabricant:int=-1;
		public var nomFabricant:String=""; // fabricant = constructeur = marque

		public var collabRattache:CollaborateurVO=null;
		public var idRow:Number=-1;

	}
}
