﻿<cfcomponent output="false">
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/M332/CTLF/CTLF.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="CTL_Fixe">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="CTLF">
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="Couts par Theme par Ligne - Fixe">
<!--- ************************************************************ --->


	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>		
	<cfset reportService=createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>

	<cffunction name="scheduleReport" access="remote" returnType="numeric">
		<cfargument name="PARAM_ARRAY" type="array" required="true">

		<cflog text="************ RAPPORT : #VARIABLES['OUTPUT']#">
	
<!--- ***************************** CODE A MODIFIER ******************************* --->		
<!--- LES PARAMETRES FOURNIT PAR L'IHM --->
	<!--- OBLIGATOIRE OBLIGATOIRE OBLIGATOIRE POUR TOUS LES RAPPORTS --->
		<cfset MODULE=PARAM_ARRAY[1]>
		<cfset FORMAT=PARAM_ARRAY[2]>
		<cfset EXT=PARAM_ARRAY[3]>
	<!--- DEPEND DU RAPPORT (CF DOC) --->
		<!--- a multiplier par le nombre de paramètre nécessaire --->
		<cfset G_IDPERIMETRE = PARAM_ARRAY[4]>
		<cfset DATE = PARAM_ARRAY[5]>
		<cfset LBLNOEUD = PARAM_ARRAY[6]>
		<cfset LBLPERIMETRE = ApiE0.getLibellePerimetre(G_IDPERIMETRE)>
<!--- ************************************************************ --->		

		
		<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],"ConsoView",MODULE,VARIABLES["CODE_RAPPORT"])>
		<cfif initStatus EQ TRUE>
			<cfset setStatus=TRUE>
			
<!--- ***************************** CODE A MODIFIER ******************************* --->					
<!--- DEFINITION DES PARAMETRES DU RAPPORT --->	

	
		<!--- PARAMETRES RECUPERES EN SESSION --->
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDMASTER",tmpParamValues)>
			
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["ID_GROUPE"]>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDRACINE",tmpParamValues)>
	
	
	<!--- PARAMETRES RECUPERES VIA L'IHM--->		
		<!--- IDPERIMETRE --->			
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=G_IDPERIMETRE>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDPERIMETRE",tmpParamValues)>
	<!--- G_LANGUE_PAYS --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]= session.user.globalization>
			<cfset setStatus=setStatus AND reportService.setParameter("G_LANGUE_PAYS",tmpParamValues)>
	
	<!--- PARAMETRES RECUPERES VIA L'APIEO A PARTIR DE L'IDPERIMETRE --->
		<!--- ID DE L'ORGA --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDORGA=ApiE0.getOrga(G_IDPERIMETRE)>
			<cfset tmpParamValues[1]=G_IDORGA>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDORGA",tmpParamValues)>
		<!--- IDCLICHE : Celui du dernier mois du client --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDCLICHE=ApiE0.getCliche(G_IDORGA)>
			<cfset tmpParamValues[1]=G_IDCLICHE>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDCLICHE",tmpParamValues)>
		<!--- IDPERIODEMOIS DEB --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset U_IDPERIODE_MOIS = ApiE0.getIdPeriodeMois(DATE)>
			<cfset tmpParamValues[1]=U_IDPERIODE_MOIS>
			<cfset setStatus=setStatus AND reportService.setParameter("U_IDPERIOD_DEB",tmpParamValues)>
			<cfset setStatus=setStatus AND reportService.setParameter("U_IDPERIOD_FIN",tmpParamValues)>
		<!--- G_NIVEAU --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_NIVEAU = ApiE0.getNiveauOrga_V2(G_IDPERIMETRE)>
			<cfset tmpParamValues[1]=G_NIVEAU>
			<cfset setStatus=setStatus AND reportService.setParameter("G_NIVEAU",tmpParamValues)>
		<!--- X_INCLUDE_CSV_HEADER  --->	
			<cfset tmpParamValues=arrayNew(1)>
			<cfif ucase(FORMAT) eq "CSV">
				<cfset tmpParamValues[1]=1>
				<cflog text="************ X_INCLUDE_CSV_HEADER = 1">
			<cfelse>
				<cfset tmpParamValues[1]=0>
				<cflog text="************ X_INCLUDE_CSV_HEADER = 0">
			</cfif>
			<cfset setStatus=setStatus AND reportService.setParameter("X_INCLUDE_CSV_HEADER",tmpParamValues)>	
<!--- ************************************************************ --->

			<cflog text="*************************** idracine_master : #SESSION.PERIMETRE['IDRACINE_MASTER']#">
			<cflog text="*************************** idracine : #SESSION.PERIMETRE['ID_GROUPE']#">
			<cflog text="*************************** idperimetre : #G_IDPERIMETRE#">
			<cflog text="*************************** idorga : #G_IDORGA#">
			<cflog text="*************************** idcliche : #G_IDCLICHE#">
			<cflog text="*************************** u_idperiod_deb : #U_IDPERIODE_MOIS#">
			<cflog text="*************************** u_idperiod_fin : #U_IDPERIODE_MOIS#">			
			<cflog text="*************************** g_langue_pays : #session.user.globalization#">
			<cflog text="*************************** g_niveau : #G_NIVEAU#">
			
			<cfif setStatus EQ TRUE>
				<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],FORMAT,EXT,"#lsDateFormat(DATE,'MM-YYYY')# #VARIABLES['OUTPUT']# - #LBLNOEUD#",VARIABLES["CODE_RAPPORT"])>
				<cfif outputStatus EQ TRUE>
					<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
					<cflog text="************ reportStatus.JOBID = #reportStatus['JOBID']#">
					<cflog text="************ reportStatus.UUID  = #reportStatus['UUID']#">
					<cfif reportStatus['JOBID'] neq -1>
						<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Période d'étude",lsDateFormat(DATE,'MM/YYYY'))>				
						<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périmètre d'étude",LBLPERIMETRE)>	
						<cfreturn reportStatus["JOBID"]>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn -1>
	</cffunction>
</cfcomponent>
