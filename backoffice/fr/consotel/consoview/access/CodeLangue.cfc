<cfcomponent output="false" alias="fr.consotel.consoview.access.CodeLangue">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	
	<cfproperty name="CODE" type="string" default="" hint="le code langue au format i18n">	
	<cfproperty name="DEVISE" type="string" default="" hint="le symbole de la devise">
	<cfproperty name="LANGUE" type="string" default="" hint="la langue">
	<cfproperty name="ID_LANGUE" type="numeric" default="" hint="l'id de la langue">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		this.ID_LANGUE = 0;		
		this.LANGUE = "french";
		this.DEVISE = "€";
		this.CODE = "fr_FR";		
	</cfscript>

	<cffunction name="init" output="false" returntype="Commande">
		<cfreturn this>
	</cffunction>
	
	
	<cffunction name="getID_LANGUE" output="false" access="public" returntype="any" hint="id langue">
		<cfreturn this.ID_LANGUE>
	</cffunction>

	<cffunction name="setID_LANGUE" output="false" access="public" returntype="void" hint="id langue">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.ID_LANGUE = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getLANGUE" output="false" access="public" returntype="any" hint="la langue">
		<cfreturn this.LANGUE>
	</cffunction>

	<cffunction name="setLANGUE" output="false" access="public" returntype="void" hint="la langue">
		<cfargument name="val" required="true">
		<cfset this.LANGUE = arguments.val>
	</cffunction>

	<cffunction name="getDEVISE" output="false" access="public" returntype="any" hint="le symbole de la devise">
		<cfreturn this.DEVISE>
	</cffunction>

	<cffunction name="setDEVISE" output="false" access="public" returntype="void" hint="le symbole de la devise">
		<cfargument name="val" required="true">
		<cfset this.DEVISE = arguments.val>
	</cffunction>

	<cffunction name="getCODE" output="false" access="public" returntype="any" hint="le code langue au format i18n">
		<cfreturn this.CODE>
	</cffunction>

	<cffunction name="setCODE" output="false" access="public" returntype="void" hint="le code langue au format i18n">
		<cfargument name="val" required="true">
		<cfset this.CODE = arguments.val>
	</cffunction>

	
</cfcomponent>