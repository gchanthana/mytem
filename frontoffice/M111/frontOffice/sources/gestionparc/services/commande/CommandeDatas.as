package gestionparc.services.commande
{
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.CommandeVO;
	import gestionparc.event.gestionparcEvent;

	public class CommandeDatas extends EventDispatcher
	{

		private var _commande:CommandeVO=new CommandeVO();


		/**
		 * Dispatch un événements pour signaler que la mise à jour des informations
		 * concernant la commande s'est bien déroulée.
		 */
		public function treatDataSetInfosCommande():void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_CMD_SAVED));
		}

		/**
		 * Dispatch un événements pour signaler qu'un numéro de comande a été généré.
		 */
		public function treatDataGenererNumeroDeCommande(obj:Object):void
		{
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.NUM_CMD_GENERATED, obj));
		}

		public function set commande(value:CommandeVO):void
		{
			_commande=value;
		}

		public function get commande():CommandeVO
		{
			return _commande;
		}
	}
}
