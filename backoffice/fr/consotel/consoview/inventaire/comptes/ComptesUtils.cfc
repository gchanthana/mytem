<!--- =========================================================================
Classe: ComptesUtils
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="ComptesUtils" hint="Fournit la liste des compte est s" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="ComptesUtils" hint="Remplace le constructeur de ComptesUtils.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	
	<!---
		 fournit la liste des comptes de fgacturation de la racine
		 pour un opérateur donnné. 
	 --->
	<cffunction name="fournirListeComptes" access="public" returntype="query" output="false">
		<cfargument name="idRacine" required="false" type="numeric" default="" displayname="numeric idRacine" hint="Initial value for the idRacineproperty." />
		<cfargument name="idOperateur" required="false" type="numeric" default="" displayname="numeric operateurId" hint="Initial value for the operateurIdproperty." />
		<!--- 
		pkg_cv_commande.l_cf_racine_op_v1(p_idracine => :p_idracine,
                                    p_operateurid => :p_operateurid,
                                    p_retour => :p_retour);
		 --->	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_commande_v3.l_cf_racine_op_v1">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#idOperateur#"/>
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfreturn result/>	
		
	</cffunction>
	
	<cffunction name="fournirIdRacine" access="public" returntype="numeric" output="false">

		<cfreturn #SESSION.PERIMETRE.ID_GROUPE#/>	
	
	</cffunction>
	
	<!---
		 fournit la liste des sous comptes de fgacturation pour un compte de facturation donnné. 
	 --->
	<cffunction name="fournirListeSousComptes" access="public" returntype="query" output="false" hint="fournit la liste des sous-comptes d'un compte" >
		<cfargument name="idCompte" required="false" type="numeric" default="" displayname="numeric compteId" hint="Initial value for the compteIdproperty." />
		<!--- 
		 pkg_cv_commande.l_sous_compte_cf_v1(p_idcompe => :p_idcompe,
                                      p_retour => :p_retour);
		 --->	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_commande_v3.l_sous_compte_cf_v1">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcompe" value="#idCompte#"/>
			<cfprocresult name="result">		  
		</cfstoredproc>								
		<cfreturn result/>
	</cffunction>

	<!--- 
		fournit la liste des comptes et sous comptes pour un gestionnaire, un pool et un opérateur donné.
	 --->
	<cffunction name="fournirCompteOperateurByLoginAndPool" access="public" returntype="Query" output="false">
		<cfargument name="idLogin" 		required="true" type="numeric"/>
		<cfargument name="idOperateur" 	required="true" type="numeric"/>
		<cfargument name="idPool" 		required="true" type="numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getcompteopeloginpool">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 				value="#Session.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 	value="#idPool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
<!---  --->	

<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>