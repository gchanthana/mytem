<cfcomponent name="Datalert">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services,
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfprocessingdirective pageEncoding="utf-8">
	<cffunction name="rechercheListeLigne" access="remote" returntype="query">

		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="WITHALERT" type="numeric" required="true">
		<cfargument name="WITHOUTALERT" type="numeric" required="true">
		<cfargument name="ONALERT" type="numeric" required="true">
		<cfargument name="ONROAMING" type="numeric" required="true">
		<cfargument name="ONROAMINGDATE" type="any" required="true">
		<cfargument name="WITHOUTCONNEXION" type="numeric" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">

		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]>

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfset _ONROAMINGDATE= "null">
		<cfif isDefined("ONROAMINGDATE")>
			<cfset _ONROAMINGDATE = ONROAMINGDATE>
		</cfif>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.searchListLigne_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#WITHALERT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#WITHOUTALERT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ONALERT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ONROAMING#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsDateFormat(_ONROAMINGDATE,'YYYY/MM/DD')#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#WITHOUTCONNEXION#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="-1" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="-1" null="false">

			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>

	</cffunction>

	<cffunction name="getParamNotification" access="remote" returntype="any" description="get les parametres de notification gestionnaire">
		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.getParamNotification">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocresult name="p_retour">

		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="rechercheSeuilLigne" access="remote" returntype="query">

		<cfargument name="IDLIGNE" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.getParamSeuilLigne">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDLIGNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>

	</cffunction>

	<cffunction name="refreshBillingDay" access="remote" returntype="query">

		<cfargument name="IDLIGNE" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.refreshbillingday">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDLIGNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>

	</cffunction>

	<cffunction name="refreshDomesticData" access="remote" returntype="query">

		<cfargument name="IDLIGNE" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.refreshDomesticData">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDLIGNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>

	</cffunction>

	<cffunction name="refreshRoamingData" access="remote" returntype="query">

		<cfargument name="IDLIGNE" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.refreshRoamingData">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDLIGNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>

	</cffunction>

	<cffunction name="rechercheDafaultsMessage" access="remote" returntype="query">

		<cfargument name="BOOLDOM" type="numeric" required="true">
		<cfargument name="BOOLROAM" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>


		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.GetDefaultsMessages">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLDOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLROAM#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>
	</cffunction>

	<cffunction name="rechercheHistMessage" access="remote" returntype="query">

		<cfargument name="IDLIGNE" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.GetHistoriqueMessage">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDLIGNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>

	</cffunction>


	<cffunction name="updateParamAllSearch" access="remote" returntype="any" description="Fusion des fonctions de param de masse des seuils et de param de masse des alertes">
		<cfargument name="listLigneString"  type="any" required="true">
		<cfargument name="JOURFACTURATION" type="numeric" required="true">
		<cfargument name="DOMESTIQUEPLAN" type="numeric" required="true">
		<cfargument name="ROAMINGPLAN" type="numeric" required="true">
		<cfargument name="BOOLFAIRUSEDOM" type="numeric" required="true">
		<cfargument name="BOOLDRTDOM" type="numeric" required="true">
		<cfargument name="BOOLDRTROAM" type="numeric" required="true">
		<cfargument name="BOOLAUTOREFRESH" type="numeric" required="true">
			<cfargument name="jsonSent" type="string" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.updateParamSeuilLigne_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#listLigneString#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#JOURFACTURATION#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#DOMESTIQUEPLAN#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ROAMINGPLAN#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLFAIRUSEDOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLDRTDOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLDRTROAM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLAUTOREFRESH#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			<cfprocresult name="p_retour">
		</cfstoredproc>

		<cfif p_retour GT 0>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.IUAlerte_V2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#listLigneString#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#jsonSent#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
		</cfif>
		<cfreturn p_retour>
	</cffunction>


	<cffunction name="updateParamSeuilLigne" access="remote" returntype="any" description="Mettre à jour les parametres des lignes ">
		<cfargument name="listLigneString"  type="any" required="true">
		<cfargument name="JOURFACTURATION" type="numeric" required="true">
		<cfargument name="DOMESTIQUEPLAN" type="numeric" required="true">
		<cfargument name="ROAMINGPLAN" type="numeric" required="true">
		<cfargument name="BOOLFAIRUSEDOM" type="numeric" required="true">
		<cfargument name="BOOLDRTDOM" type="numeric" required="true">
		<cfargument name="BOOLDRTROAM" type="numeric" required="true">
		<cfargument name="BOOLAUTOREFRESH" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.updateParamSeuilLigne_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#listLigneString#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#JOURFACTURATION#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#DOMESTIQUEPLAN#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ROAMINGPLAN#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLFAIRUSEDOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLDRTDOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLDRTROAM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLAUTOREFRESH#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<cffunction name="NotifyManagers" access="remote" returntype="any" description="Mettre à jour les parametres des lignes ">
		<cfargument name="listMail"  type="any" required="true">
		<cfargument name="BOOLACTIVENOTIF" type="numeric" required="true">
		<cfargument name="BOOLNOTIFDOM" type="numeric" required="true">
		<cfargument name="BOOLNOTIFROAM" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.NotifyManagers">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#listMail#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLACTIVENOTIF#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLNOTIFDOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLNOTIFROAM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">

			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="revoquerLigne" access="remote" returntype="any" description="révoquer une ligne ">
		<cfargument name="listLigne"  type="any" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.revokeLine">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(listLigne,',')#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">

			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="deployerService" access="remote" returntype="any" description="envoyer le message de deploiement du service ">
		<cfargument name="listLigne"  type="any" required="true">
		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>
		<cfset checkOK =0>
		<cfset newApiSmsPublic = createObject("component","fr.consotel.api.sms.public.ApiSmsPublic") >
		<cfset uuid=newApiSmsPublic.createCampagne(racineId) >
		<cfif SESSION.USER.GLOBALIZATION eq "fr_FR">
			<cfset contentSms="Bonjour, le service Datalert vient d'etre activé sur votre ligne. Merci de l'installer : " & "https://#CGI.SERVER_NAME#/fr/saaswedo/api/datalert/browser/redirectToStores.cfm">
			<cfelseif SESSION.USER.GLOBALIZATION eq "en_US" OR SESSION.USER.GLOBALIZATION eq "en_CH" OR SESSION.USER.GLOBALIZATION eq "en_GB" OR SESSION.USER.GLOBALIZATION eq "en_EU">
				<cfset contentSms="Hello, The Datalert service has just been activated for your number. Please install : " & "https://#CGI.SERVER_NAME#/fr/saaswedo/api/datalert/browser/redirectToStores.cfm">
			<cfelseif SESSION.USER.GLOBALIZATION eq "es_ES">
			<cfset contentSms="Buenos días, El servicio Datalert ! acaba de ser activado en su línea. Gracias a instalarlo : " & "https://#CGI.SERVER_NAME#/fr/saaswedo/api/datalert/browser/redirectToStores.cfm">
		</cfif>

		<cfset operator=newApiSmsPublic.TWILIO_OPERATOR()>
		<cfset delay="1000">
		<!---<cfset phoneNumber=arraynew(1)>--->

		<cfloop index="idx" from="1" to="#ArrayLen(listLigne)#">

			<cfset phoneNumber=listLigne[idx]>

			<cfif  IsDefined ("uuid") and uuid neq "" >

				<cfset returnSend=newApiSmsPublic.sendMessageToSingleNumber(phoneNumber,contentSms,operator,uuid)>
				<cfif returnSend eq true>
					<cfset checkOK =checkOK+1>
				</cfif>
			</cfif>
		</cfloop>
		<cfreturn checkOK>
	</cffunction>


	<cffunction name="refreshMultipleBillingDays" access="remote" returntype="any" description="révoquer des lignes ">
		<cfargument name="listLigne"  type="any" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.refreshMultipleBillingDays">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(listLigne,',')#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="refreshMultiplePlans" access="remote" returntype="any" description="révoquer des lignes ">
		<cfargument name="listLigne"  type="any" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.refreshMultiplePlans">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(listLigne,',')#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="refreshALLPlans" access="remote" returntype="any" description="révoquer des lignes ">
		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.refreshALLPlans">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="sendMessageLigne" access="remote" returntype="any" description="Envoyer Message">
		<cfargument name="listLigne"  type="any" required="true">
		<cfargument name="format" type="numeric" required="true">
		<cfargument name="idgestionnaire" type="numeric" required="true">
		<cfargument name="messageText" type="String" required="true">

		<cfset idsMessage = ArrayNew(1)>
		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfset newApiSmsPublic = createObject("component","fr.consotel.api.sms.public.ApiSmsPublic") >

		<cfset index = 1>
		<cfset checkOK = 1>

 		<cfloop index="idx" from="1" to="#ArrayLen(listLigne)#">
			<cfif checkOK GTE 1>
				<cfset numLigne = listLigne[idx]["numLigne"]>
				<cfset IDSOUSTETE = listLigne[idx]["IDSOUSTETE"]>
				<cfset DOMESTIQUEDATA = listLigne[idx]["DOMESTIQUEDATA"]>
				<cfset DOMESTIQUEPLAN = listLigne[idx]["DOMESTIQUEPLAN"]>
				<cfset DOMESTIQUEALERT = listLigne[idx]["DOMESTIQUEALERT"]>
				<cfset ROAMINGDATA = listLigne[idx]["ROAMINGDATA"]>
				<cfset ROAMINGPLAN = listLigne[idx]["ROAMINGPLAN"]>
				<cfset ROAMINGALERT = listLigne[idx]["ROAMINGALERT"]>

				<cfset uuid=newApiSmsPublic.createCampagne(racineId) >

				<cfset phoneNumber=listLigne[idx]["UUID"]>
				<cfset operator=newApiSmsPublic.TWILIO_OPERATOR()>
				<cfset delay="1000">

 				<cfif  IsDefined ("uuid") and uuid neq "" >
				 	<cfset returnSend=newApiSmsPublic.sendMessageToSingleNumber(phoneNumber,messageText,operator,uuid)>
					<cfif returnSend eq true>
					 	 <cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m26.SendMessage">
						 	<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#racineId#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	value="#IDSOUSTETE#" null="false" >
							<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  	value="#numLigne#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	value="#idgestionnaire#" null="false" >
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#format#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	value="#lsDateFormat(Now(),'YYYY/MM/DD')# #LSTimeFormat(Now(), 'HH:mm') #" null="false" >
							<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	value="#messageText#" null="false" >
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#DOMESTIQUEDATA#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#DOMESTIQUEPLAN#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#DOMESTIQUEALERT#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#ROAMINGDATA#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#ROAMINGPLAN#" null="false">
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  	value="#ROAMINGALERT#" null="false">
							<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
						</cfstoredproc>
						<cfset checkOK =  p_idoperation>
						<cfset ArrayAppend(idsMessage, checkOK)>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<cfreturn idsMessage>

	</cffunction>

	<cffunction name="sendAutomaticMessage" access="remote" returntype="any" description="envoyer le sms d'alert">
		<cfargument name="numLigne"  type="any" required="true">
		<cfargument name="contentSms"  type="any" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>
		<cfset checkOK =0>
		<cfset newApiSmsPublic = createObject("component","fr.consotel.api.sms.public.ApiSmsPublic") >
		<cfset uuid=newApiSmsPublic.createCampagne(racineId) >

		<cfset operator=newApiSmsPublic.TWILIO_OPERATOR()>
		<cfset delay="1000">
		<cfset phoneNumber=numLigne>

		<cfif  IsDefined ("uuid") and uuid neq "" >
			<cfset returnSend=newApiSmsPublic.sendMessageToSingleNumber(phoneNumber,contentSms,operator,uuid)>
			<cfif returnSend eq true>
				<cfset checkOK =1>
			</cfif>
		</cfif>

		<cfreturn checkOK>
	</cffunction>

	<cffunction name="saveDefaultMessage" access="remote" returntype="any" description="Enregistrer message" >
		<cfargument name="LIBMESSAGE"  type="string" required="true">
		<cfargument name="TXTMESSAGE" type="string" required="true">
		<cfargument name="BOOLACTIF" type="numeric" required="true">
		<cfargument name="BOOLDOM" type="numeric" required="true">
		<cfargument name="BOOLROAM" type="numeric" required="true">
		<cfargument name="IDMESSAGE" type="numeric" required="true">


		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.saveMessage">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#LIBMESSAGE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TXTMESSAGE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLACTIF#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLDOM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#BOOLROAM#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDMESSAGE#" null="false">

			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="deleteDefaultMessage" access="remote" returntype="any" description="supprimer message" >

		<cfargument name="IDMESSAGE" type="numeric" required="true">

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.deleteMessage">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDMESSAGE#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">

			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<!--- Export des flux bruts (bouchon, à modifier par la suite, voir ticket MYT-1042) --->

	<cffunction name="exportRawFlow" access="remote" returntype="numeric" description="fonction servant pour l'export des flux bruts de datalert">
		<cfargument name="dateDebut" type="date" required="true">
		<cfargument name="dateFin" type="date" required="true">
		<cfargument name="parametresRechercheVO" type="struct" required="true">

		<cfset flowReport = CreateObject("component","fr.consotel.consoview.M26.reporting.RapportFlux")>
		<cfset retour = flowReport.scheduleRepport(dateDebut,dateFin,parametresRechercheVO)>

		<cfreturn retour>
	</cffunction>

</cfcomponent>