package vo
{
	public class TypeSegment
	{
		public static const SEGMENT_FIXE:String = 'FIXE';
		public static const SEGMENT_MOBILE:String = 'MOBILE';
		public static const SEGMENT_DATA:String = 'DATA';
		
		public static const ID_SEGMENT_FIXE:Number = 1;
		public static const ID_SEGMENT_MOBILE:Number = 2;
		public static const ID_SEGMENT_DATA:Number = 3;
		
		public static function getIdSegment(value:String):Number
		{
			var _localValue:String = value.toUpperCase();
			
			switch(value)
			{
				case SEGMENT_FIXE : 
				{
					return ID_SEGMENT_FIXE;					
				}
				case SEGMENT_MOBILE :
				{
					return ID_SEGMENT_MOBILE;
				}
				case SEGMENT_DATA :
				{
					return ID_SEGMENT_DATA;
				}
					
			}
			return -1
		}
	}
}