package univers.parametres.gestionnaire
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class CreerProfile extends CreerProfileIHM
	{
		private var gestionAuto : GestionAutorisationAction;
		
		
		public function CreerProfile()
		{
			
			super();
			//this.gestionAuto = gestionAuto;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer);
			initData();
			
		}
		private function initData():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"getAllAction",
																	process_listeAction);
			RemoteObjectUtil.callService(op);
		}
		private function process_listeAction(re : ResultEvent):void
		{
			dg_action.dataProvider = re.result;
		}
		private function initIHM(evt : FlexEvent):void
		{
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			bt_ajouter.addEventListener(MouseEvent.CLICK,ajouter);
			
		}
		private function ajouter(evt : MouseEvent):void
		{
			var listeID : String = "";
			for(var i : int = 0;i<(dg_action.dataProvider as ArrayCollection).length;i++)
			{
				if ((dg_action.dataProvider as ArrayCollection).getItemAt(i).SELECTED==1){
					listeID = (dg_action.dataProvider as ArrayCollection).getItemAt(i).IDINV_ACTIONS+","+listeID;
				}
			}
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"saveProfile",
																	process_create_profile);
			// le -1 est pour signaler une création (id>0 = modif)
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,-1,in_libelle.text,in_code.text,listeID);
		}
		private function process_create_profile(evt : ResultEvent):void
		{
			dispatchEvent(new Event("CLOSE"));
			Alert.show("Ajouté");
			//this[initDataFunction];
			PopUpManager.removePopUp(this);
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		
	}
}