package intranet.gestionLoginClasse.entity
{
    import mx.collections.ArrayCollection;

    public class AccessVO
    {
        public static const NO_ACCESS:String = "Pas d'accès";
        public static const R_ACCESS:String = "Lecture";
        public static const RW_ACCESS:String = "Lecture/Ecriture";
        private var _module_usage:String;
        private var _module_fixe_data:String;
        private var _module_gestion_login:String;
        private var _idGroupeMaitre:String;
        private var _module_mobile:String;
        private var _droit_gestion_fournis:String;
        private var _libellegm:String;
        private var _idconso_style:String;
        private var _iduser_create:String;
        private var _type_access:String;
        private var _module_facturation:String;
        private var _datedeb:String;
        private var _module_gestion_org:String;
        private var _date_create:Date;
        private var _module_workflow:String;
        private var _user_create:String;
        private var _app_loginid:String;
        private var _id:String;
        private var _libelle:String;
        private var _type:String;

        public function AccessVO()
        {
        }

        public function deserialize(o:Object):AccessVO
        {
            module_usage = o.MODULE_USAGE;
            module_fixe_data = o.MODULE_FIXE_DATA;
            module_gestion_login = o.MODULE_GESTION_LOGIN;
            idGroupeMaitre = o.idGroupeMaitre;
            module_mobile = o.MODULE_MOBILE;
            droit_gestion_fournis = o.DROIT_GESTION_FOURNIS;
            libellegm = o.libellegm;
            idconso_style = o.IDCONSO_STYLE;
            iduser_create = o.IDUSER_CREATE;
            type_access = o.TYPE_ACCESS;
            module_facturation = o.MODULE_FACTURATION;
            datedeb = o.DATEDEB;
            module_gestion_org = o.MODULE_GESTION_ORG;
            date_create = o.DATE_CREATE;
            module_workflow = o.MODULE_WORKFLOW;
            user_create = o.USER_CREATE;
            app_loginid = o.APP_LOGINID;
            id = o.ID;
            libelle = o.LIBELLE;
            type = o.TYPE;
            return this;
        }

        public function serialize():Object
        {
            var o:Object = new Object();
            o.MODULE_USAGE = module_usage;
            o.MODULE_FIXE_DATA = module_fixe_data;
            o.MODULE_GESTION_LOGIN = module_gestion_login;
            o.idGroupeMaitre = idGroupeMaitre;
            o.MODULE_MOBILE = module_mobile;
            o.DROIT_GESTION_FOURNIS = droit_gestion_fournis;
            o.libellegm = libellegm;
            o.IDCONSO_STYLE = idconso_style;
            o.IDUSER_CREATE = iduser_create;
            o.TYPE_ACCESS = type_access;
            o.MODULE_FACTURATION = module_facturation;
            o.DATEDEB = datedeb;
            o.MODULE_GESTION_ORG = module_gestion_org;
            o.DATE_CREATE = date_create;
            o.MODULE_WORKFLOW = module_workflow;
            o.USER_CREATE = user_create;
            o.APP_LOGINID = app_loginid;
            o.ID = id;
            o.LIBELLE = libelle;
            o.TYPE = type;
            return this;
        }

        public static function getLibell(value:String):String
        {
            var libel:String = "";
            switch (value)
            {
                case "0":
                    libel = NO_ACCESS;
                    break;
                case "1":
                    libel = R_ACCESS;
                    break;
                case "2":
                    libel = RW_ACCESS;
                    break;
                default:
                    break;
            }
            return libel;
        }

        public function get module_usage():String
        {
            return _module_usage;
        }

        public function set module_usage(value:String):void
        {
            _module_usage = value;
        }

        public function get module_fixe_data():String
        {
            return _module_fixe_data;
        }

        public function set module_fixe_data(value:String):void
        {
            _module_fixe_data = value;
        }

        public function get module_gestion_login():String
        {
            return _module_gestion_login;
        }

        public function set module_gestion_login(value:String):void
        {
            _module_gestion_login = value;
        }

        public function get idGroupeMaitre():String
        {
            return _idGroupeMaitre;
        }

        public function set idGroupeMaitre(value:String):void
        {
            _idGroupeMaitre = value;
        }

        public function get module_mobile():String
        {
            return _module_mobile;
        }

        public function set module_mobile(value:String):void
        {
            _module_mobile = value;
        }

        public function get droit_gestion_fournis():String
        {
            return _droit_gestion_fournis;
        }

        public function set droit_gestion_fournis(value:String):void
        {
            _droit_gestion_fournis = value;
        }

        public function get libellegm():String
        {
            return _libellegm;
        }

        public function set libellegm(value:String):void
        {
            _libellegm = value;
        }

        public function get idconso_style():String
        {
            return _idconso_style;
        }

        public function set idconso_style(value:String):void
        {
            _idconso_style = value;
        }

        public function get iduser_create():String
        {
            return _iduser_create;
        }

        public function set iduser_create(value:String):void
        {
            _iduser_create = value;
        }

        public function get type_access():String
        {
            return _type_access;
        }

        public function set type_access(value:String):void
        {
            _type_access = value;
        }

        public function get module_facturation():String
        {
            return _module_facturation;
        }

        public function set module_facturation(value:String):void
        {
            _module_facturation = value;
        }

        public function get datedeb():String
        {
            return _datedeb;
        }

        public function set datedeb(value:String):void
        {
            _datedeb = value;
        }

        public function get module_gestion_org():String
        {
            return _module_gestion_org;
        }

        public function set module_gestion_org(value:String):void
        {
            _module_gestion_org = value;
        }

        public function get date_create():Date
        {
            return _date_create;
        }

        public function set date_create(value:Date):void
        {
            _date_create = value;
        }

        public function get module_workflow():String
        {
            return _module_workflow;
        }

        public function set module_workflow(value:String):void
        {
            _module_workflow = value;
        }

        public function get user_create():String
        {
            return _user_create;
        }

        public function set user_create(value:String):void
        {
            _user_create = value;
        }

        public function get app_loginid():String
        {
            return _app_loginid;
        }

        public function set app_loginid(value:String):void
        {
            _app_loginid = value;
        }

        public function get id():String
        {
            return _id;
        }

        public function set id(value:String):void
        {
            _id = value;
        }

        public function get libelle():String
        {
            return _libelle;
        }

        public function set libelle(value:String):void
        {
            _libelle = value;
        }

        public function get type():String
        {
            return _type;
        }

        public function set type(value:String):void
        {
            _type = value;
        }
    }
}