//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 26, 2011								*
//	owner  : Consotel								*
//***************************************************
package univers.facturation.suivi.controles.vo
{
	public class StatutVO
	{
		public function StatutVO(id:Number,libelle:String)
		{
			_ID = id;
			_LIBELLE = libelle;
		}
		
		private var _ID:Number = 0;
		private var _LIBELLE:String = "";
		
		public function get ID():Number
		{
			return _ID
		}
		
		public function get LIBELLE():String
		{
			return _LIBELLE
		}
	}
}