package gestioncommande.services.parameters
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;


	public class UserAccessService
	{	
		
		
		private var _model:UserAccessModel;
		public var handler:UserAccessHandler;
		
				
		public function UserAccessService()
		{
			_model = new UserAccessModel();
			handler = new UserAccessHandler(model);
		}		

		public function get model():UserAccessModel
		{
			return _model;
		}
		
		
		/**
		 * Récupère les paramètres de l'utilisateur connecté pour un module (au niveau de l'abonnement)
		 */		
		public function getUserAccess():void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M00.UserAccessService",
																				"getModuleUserAccess",
																				handler.getUserAccessResultHandler);
			
			RemoteObjectUtil.callService(op, 'M16');		
		}
		
		
		/**
		 * Verifie le mot de passe de l'utilisateur connecté
		 * @param password (obligatoire)
		 */		
		public function checkPassWord(value:String):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.UserAccessService",
				"checkPassWord",
				handler.checkPassWordResultHandler);
			
			RemoteObjectUtil.callService(op,value);		
		}
		
	}
}