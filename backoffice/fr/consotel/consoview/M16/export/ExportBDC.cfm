﻿<cfscript>

		

	 biServer="http://bip-commande.consotel.fr/xmlpserver/services/PublicReportService_v11?WSDL";	
	 _locale = SESSION.USER.GLOBALIZATION;
	 _racine = Session.perimetre.GROUPE;	
	 _idracine = Session.PERIMETRE.ID_GROUPE;
	 _idtype_commande = FORM.IDTYPE_COMMANDE;
	 _idCommande = FORM.IDCOMMANDE;
	 _numCommande = FORM.NUMERO_COMMANDE;
	 _idoperateur = FORM.IDOPERATEUR;
	 _idGestionnaire = Session.user.CLIENTACCESSID;
	 _gestionnaire = Session.user.PRENOM & ' ' & Session.user.NOM;
	 _mailGestionnaire = Session.user.EMAIL;
	
 
	/* RÉCUPÉRATION DU TEMPLATE */
	_bdcInfos = new fr.consotel.consoview.M16.v2.util.BDCInfos(_idracine,_idoperateur,_idtype_commande);				 
	_templateName = _bdcInfos.template;
	_reportAbsolutePath = _bdcInfos.absolutepath;
					
	
	if(compareNoCase(_locale,"fr_FR") neq 0)
	{	 
		_templateName = left(_templateName,3);
	}
	
	
	 
	 
	  
	 _format = "pdf";
	 
	
	 ArrayOfParamNameValues=ArrayNew(1);
	 ParamNameValue=structNew();
	 ParamNameValue.multiValuesAllowed=FALSE;
	 ParamNameValue.name="P_IDCOMMANDE";
	 t=ArrayNew(1);
	 t[1]=_idCommande;
	 ParamNameValue.values=t;
	 ArrayOfParamNameValues[1]=ParamNameValue;
	
	 ParamNameValue=structNew();
	 ParamNameValue.multiValuesAllowed=FALSE;
	 ParamNameValue.name="P_IDGESTIONNAIRE";
	 t=ArrayNew(1);
	 t[1]=_idGestionnaire;
	 ParamNameValue.values=t;
	 ArrayOfParamNameValues[2]=ParamNameValue;
	
	
	 ParamNameValue=structNew();
	 ParamNameValue.multiValuesAllowed=FALSE;
	 ParamNameValue.name="P_RACINE";
	 t=ArrayNew(1);
	 t[1]=_racine;
	 ParamNameValue.values=t;
	 ArrayOfParamNameValues[3]=ParamNameValue;
	
		
	 ParamNameValue=structNew();
	 ParamNameValue.multiValuesAllowed=FALSE;
	 ParamNameValue.name="P_GESTIONNAIRE";
	 t=ArrayNew(1);
	 t[1]=_gestionnaire;
	 ParamNameValue.values=t;
	 ArrayOfParamNameValues[4]=ParamNameValue;
	
	
	 ParamNameValue=structNew();
	 ParamNameValue.multiValuesAllowed=FALSE;
	 ParamNameValue.name="P_MAIL";
	 t=ArrayNew(1);   
	 t[1]=_mailGestionnaire;
	 ParamNameValue.values=t;
	 ArrayOfParamNameValues[5]=ParamNameValue;
	
	 ParamNameValue=structNew();
	 ParamNameValue.multiValuesAllowed=FALSE;
	 ParamNameValue.name="P_LANGUE_PAYS";
	 t=ArrayNew(1);
	 t[1]=_locale;
	 ParamNameValue.values=t;
	 ArrayOfParamNameValues[6]=ParamNameValue;
	
	
	 myParamReportRequest=structNew();	
	 myParamReportRequest.reportAbsolutePath=_reportAbsolutePath;
	 myParamReportRequest.attributeTemplate= _templateName;
	 myParamReportRequest.attributeLocale=_locale;
	 myParamReportRequest.attributeFormat=LCase(_format);
	 myParamReportRequest.sizeOfDataChunkDownload=-1;
	 myParamReportRequest.parameterNameValues=ArrayOfParamNameValues;
	 myParamReportParameters=structNew();
	 structInsert(myParamReportParameters,"reportRequest",myParamReportRequest);	
	 myParamReportParameters.userID="consoview";
	 myParamReportParameters.password="public";	
</cfscript>


<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
<cfscript>
	reporting=createObject("component","fr.consotel.consoview.api.Reporting");
	filename = "BDC" & _numCommande & reporting.getFormatFileExtension(_format);
</cfscript>
 
<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">
<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">
