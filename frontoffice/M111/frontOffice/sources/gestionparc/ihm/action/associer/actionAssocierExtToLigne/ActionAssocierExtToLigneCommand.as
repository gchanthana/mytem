package gestionparc.ihm.action.associer.actionAssocierExtToLigne
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierExtToLigneCommand implements ICommand
	{
		private var receiver:ActionAssocierExtToLigneReceiver=null;

		public function ActionAssocierExtToLigneCommand(rec:ActionAssocierExtToLigneReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
