<cfcomponent output="false">
	<cfset VARIABLES["XDO"]="/consoview/M332/REPART-TYPE-DE-CHARGE-PAR-LIGNE/REPART-TYPE-DE-CHARGE-PAR-LIGNE.xdo">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="RTCL_TousSegments">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="CV">
	<!--- OUTPUT ---> 
	<cfset VARIABLES["OUTPUT"]="Repartition des types de charge par ligne">
	
	<cffunction name="scheduleReport" returnType="any" access="public">
		<cfargument name="PARAM_ARRAY" type="array" required="true">
		
		<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
		
		<cflog text="************ RAPPORT : #VARIABLES['OUTPUT']#">
					
		<cfset SUPPORT_EMAIL="production@consotel.fr">
		<cfset SERVICE_IT_EMAIL="dev@consotel.fr">
		<cfset MAIL_SERVER="192.168.3.119:26">
		
		<cfset MODULE=PARAM_ARRAY[1]>
		<cfset FORMAT=PARAM_ARRAY[2]>
		<cfset EXT=PARAM_ARRAY[3]>
		<cfset G_IDPERIMETRE = PARAM_ARRAY[4]>
		<cfset DATE_DEB = PARAM_ARRAY[5]>
		<cfset DATE_FIN = PARAM_ARRAY[6]>
		<cfset LBLNOEUD = PARAM_ARRAY[7]>
		<cfset LBLPERIMETRE = ApiE0.getLibellePerimetre(G_IDPERIMETRE)>
		
		<cflog text="************************ RapportRTCL_TousSegments lancé">
		
	<!--- Instanciation et Initialisation du Service --->
		<cfset developerReportService=createObject("component","fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService")>
		<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>
		
	<!--- Initialisation du Service avec les paramètres obligatoires : IDRACINE, XDO BI Publisher --->
		<cfset initStatus=developerReportService.init(SESSION.PERIMETRE.ID_GROUPE,VARIABLES["XDO"],"M331 module rapport",MODULE,VARIABLES["CODE_RAPPORT"])>
	
	
	<!--- Passages des paramètres avec leur(s) valeur(s) --->
	
		<!--- PARAMETRES RECUPERES EN SESSION --->
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>
			<cfset setStatus = developerReportService.setParameter("G_IDMASTER",tmpParamValues)>
	
		<!--- IDRACINE --->
			<cfset valeursPourIdRacine=arrayNew(1)>
			<cfset valeursPourIdRacine[1]=SESSION.PERIMETRE.ID_GROUPE>
			<cfset setStatus=setStatus AND developerReportService.setParameter("G_IDRACINE",valeursPourIdRacine)>
		
		<!--- PARAMETRES RECUPERES VIA L'IHM--->		
		<!--- IDPERIMETRE --->				
			<cfset valeursPourIdPerimetre=arrayNew(1)>
			<cfset valeursPourIdPerimetre[1]=G_IDPERIMETRE>
			<cfset setStatus=setStatus AND developerReportService.setParameter("G_IDPERIMETRE",valeursPourIdPerimetre)>
		
		<!--- IDPERIODEMOIS DEB --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset U_IDPERIODE_MOIS_DEB = ApiE0.getIdPeriodeMois(DATE_DEB)>
			<cfset tmpParamValues[1]=U_IDPERIODE_MOIS_DEB>
			<cfset setStatus=setStatus AND developerReportService.setParameter("U_IDPERIOD_DEB",tmpParamValues)>
		
		<!--- IDPERIODEMOIS FIN --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset U_IDPERIODE_MOIS_FIN = ApiE0.getIdPeriodeMois(DATE_FIN)>
			<cfset tmpParamValues[1]=U_IDPERIODE_MOIS_FIN>
			<cfset setStatus=setStatus + developerReportService.setParameter("U_IDPERIOD_FIN",tmpParamValues)>
		
		<!--- ID DE L'ORGA --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDORGA=ApiE0.getOrga(G_IDPERIMETRE)>
			<cfset tmpParamValues[1]=G_IDORGA>
			<cfset setStatus=setStatus AND developerReportService.setParameter("G_IDORGA",tmpParamValues)>	
		
		<!--- APP_LOGINID --->
			<!--- <cfset valeursPourUserId=arrayNew(1)>
			<cfset valeursPourUserId[1]=SESSION.USER.CLIENTACCESSID>
			<cfset setStatus=setStatus AND developerReportService.setParameter("Q_USERID",valeursPourUserId)> --->

		<!--- G_NIVEAU --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_NIVEAU = ApiE0.getNiveauOrga_V2(G_IDPERIMETRE)>
			<cfset tmpParamValues[1]=G_NIVEAU>
			<cfset setStatus=setStatus AND developerReportService.setParameter("G_NIVEAU",tmpParamValues)>
		
		<!--- G_LANGUE_PAYS --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]= session.user.globalization>
			<cfset setStatus=setStatus AND developerReportService.setParameter("G_LANGUE_PAYS",tmpParamValues)>
	
		<!--- X_INCLUDE_CSV_HEADER  --->	
			<cfset tmpParamValues=arrayNew(1)>
			<cfif ucase(FORMAT) eq "CSV">
				<cfset tmpParamValues[1]=1>
				<cflog text="************ X_INCLUDE_CSV_HEADER = 1">
			<cfelse>
				<cfset tmpParamValues[1]=0>
				<cflog text="************ X_INCLUDE_CSV_HEADER = 0">
			</cfif>
			<cfset setStatus=setStatus AND developerReportService.setParameter("X_INCLUDE_CSV_HEADER",tmpParamValues)>	
	
			<cflog text="*************************** idracine_master : #SESSION.PERIMETRE['IDRACINE_MASTER']#">
			<cflog text="*************************** idracine : #SESSION.PERIMETRE['ID_GROUPE']#">
			<cflog text="*************************** idperimetre : #G_IDPERIMETRE#">
			<cflog text="*************************** idorga : #G_IDORGA#">
			<cflog text="*************************** u_idperiod_deb : #U_IDPERIODE_MOIS_DEB#">
			<cflog text="*************************** u_idperiod_fin : #U_IDPERIODE_MOIS_FIN#">	
			<cflog text="*************************** g_langue_pays : #session.user.globalization#">
			<cflog text="*************************** g_niveau : #G_NIVEAU#">
	
		<cfset setOutputStatus=developerReportService.setOutput(VARIABLES["TEMPLATE"],FORMAT,EXT,"#LsDateFormat(DATE_DEB,'MM-YYYY')# - #LsDateFormat(DATE_FIN,'MM-YYYY')# #VARIABLES['OUTPUT']# - #LBLNOEUD#",VARIABLES["CODE_RAPPORT"])>

		<cfif setOutputStatus EQ TRUE>
			<cfset reportStatus = developerReportService.runTask(SESSION.USER["CLIENTACCESSID"])>
			<cflog text="************ reportStatus.JOBID = #reportStatus['JOBID']#">
			<cflog text="************ reportStatus.UUID  = #reportStatus['UUID']#">
			<cfif reportStatus['JOBID'] neq -1>
				<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Période d'étude début",lsDateFormat(DATE_DEB,'MM/YYYY'))>				
				<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Période d'étude fin",lsDateFormat(DATE_FIN,'MM/YYYY'))>
				<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périmètre d'étude",LBLPERIMETRE)>					
				<cfreturn reportStatus["JOBID"]>
			</cfif>
		<clelse>
			<cfreturn -1>
		</cfif>
	</cffunction>

</cfcomponent>
