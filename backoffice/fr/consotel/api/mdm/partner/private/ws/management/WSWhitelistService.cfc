<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSWhitelistService" implements="fr.consotel.api.mdm.partner.private.ws.IWSWhitelistService"
hint="Implémentation fournissant les méthodes du Service Whitelist Zenprise. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification
<br>Une exception provenant du web service est systématiquement levée si une liste qui n'existe pas est utilisée dans ces méthodes">
	<!--- fr.consotel.api.mdm.partner.private.ws.management.WSWhitelistService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSWhitelistService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
	
	<!--- Whitelist Service --->
	<cffunction access="public" name="getAllApplicationsWhiteList" returntype="Array" hint="Retourne un tableau contenant la liste des noms des applications en whitelist définis sur le serveur d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfreturn whitelistService.getAllApplicationsWhiteList(ARGUMENTS["osFamily"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getApplicationsInWhitelist" returntype="Array" hint="Retourne un tableau contenant la liste des applications d'une whitelist d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="applicationsWhitelistName" type="String" required="true" hint="Nom d'une whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:getApplicationsInWhitelist>
							<zenprise:applicationsWhitelistName xsi:nil="false">#ARGUMENTS["applicationsWhitelistName"]#</zenprise:applicationsWhitelistName>
							<zenprise:osFamily xsi:nil="false">#ARGUMENTS["osFamily"]#</zenprise:osFamily>
						</zenprise:getApplicationsInWhitelist>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendWhitelistRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var wsResult=[]>
			<cfset var soapResultList=soapResponse.xmlRoot.Body.getApplicationsInWhitelistResponse.getApplicationsInWhitelistReturn.xmlChildren>
			<cfset var listCount=arrayLen(soapResultList)>
			<cfloop index="i" from="1" to="#listCount#">
				<cfset wsResult[i]={identifier=toString(soapResultList[i]["identifier"]["xmlText"]), name=toString(soapResultList[i]["name"]["xmlText"])}>
			</cfloop>
			<cfreturn wsResult>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service Whitelist mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getDefaultWhitelist" returntype="Array" hint="Retourne un tableau contenant la liste des application de la whitelist par défaut d'un OS<br>
	Chaque élément du tableau contient les propriétés suivantes : identifier, name">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:getDefaultWhitelist>
							<zenprise:osFamily xsi:nil="false">#ARGUMENTS["osFamily"]#</zenprise:osFamily>
						</zenprise:getDefaultWhitelist>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendWhitelistRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var wsResult=[]>
			<cfset var soapResultList=soapResponse.xmlRoot.Body.getDefaultWhitelistResponse.getDefaultWhitelistReturn.xmlChildren>
			<cfset var listCount=arrayLen(soapResultList)>
			<cfloop index="i" from="1" to="#listCount#">
				<cfset wsResult[i]={identifier=toString(soapResultList[i]["identifier"]["xmlText"]), name=toString(soapResultList[i]["name"]["xmlText"])}>
			</cfloop>
			<cfreturn wsResult>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service Whitelist mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="addWhiteList" returntype="Void" hint="Ajoute une whitelist d'un OS et lève une exception si <b>name</b> existe déjà<br>">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="description" type="String" required="true" hint="Description à associer à la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la whitelist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
		<cfset var soapRequest="">
		<cfset var app=ARGUMENTS["applications"]>
		<cfset var appCount=arrayLen(app)>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:addWhiteList>
							<zenprise:name xsi:nil="false">#ARGUMENTS["name"]#</zenprise:name>
							<zenprise:description xsi:nil="false">#ARGUMENTS["description"]#</zenprise:description>
							<zenprise:osFamily xsi:nil="false">#ARGUMENTS["osFamily"]#</zenprise:osFamily>
							<zenprise:applications xsi:nil="false">
							<cfloop index="i" from="1" to="#appCount#">
								<cfset var appId=app[i]["identifier"]>
								<cfif TRIM(appId) EQ "">
									<cfset appId=app[i]["name"]>
								</cfif>
								<ns2:BWApplication xmlns:ns2="urn:BeanService" xsi:type="ns2:BWApplication">
									<zenprise:identifier xsi:type="soapenc:string">#appId#</zenprise:identifier>
									<zenprise:name xsi:nil="false" xsi:type="soapenc:string">#app[i]["name"]#</zenprise:name>
								</ns2:BWApplication>
							</cfloop>
							</zenprise:applications>
						</zenprise:addWhiteList>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendWhitelistRequest(soapRequest)>
		<cfif yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service Whitelist mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="updateWhiteList" returntype="Void" hint="Met à jour une whitelist d'un OS<br>
	<b>Bug (Zenprise) :</b> Cette méthode lève toujours une exception car elle ne fonctionne pas (error code [1407]; Could not execute JDBC batch update. QueryTimeoutException)">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="newName" type="String" required="true" hint="Nouveau nom pour la whitelist">
		<cfargument name="newDescription" type="String" required="true" hint="Nouvelle description à associer à la whitelist">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la whitelist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée dans la version actuelle de Zenprise">
		<cfset var soapRequest="">
		<cfset var app=ARGUMENTS["applications"]>
		<cfset var appCount=arrayLen(app)>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:updateWhiteList>
							<zenprise:name xsi:nil="false">#ARGUMENTS["name"]#</zenprise:name>
							<zenprise:osFamily xsi:nil="false">#ARGUMENTS["osFamily"]#</zenprise:osFamily>
							<zenprise:newName xsi:nil="false">#ARGUMENTS["newName"]#</zenprise:newName>
							<zenprise:newDescription xsi:nil="false">#ARGUMENTS["newDescription"]#</zenprise:newDescription>
							<zenprise:applications xsi:nil="false">
							<cfloop index="i" from="1" to="#appCount#">
								<cfset var appId=app[i]["identifier"]>
								<cfif TRIM(appId) EQ "">
									<cfset appId=app[i]["name"]>
								</cfif>
								<ns2:BWApplication xmlns:ns2="urn:BeanService" xsi:type="ns2:BWApplication">
									<zenprise:identifier xsi:type="soapenc:string">#appId#</zenprise:identifier>
									<zenprise:name xsi:nil="false" xsi:type="soapenc:string">#app[i]["name"]#</zenprise:name>
								</ns2:BWApplication>
							</cfloop>
							</zenprise:applications>
						</zenprise:updateWhiteList>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendWhitelistRequest(soapRequest)>
		<cfif yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service Whitelist mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="updateDefaultWhiteList" returntype="Void" hint="Met à jour la whitelist par défaut d'un OS<br>
	<b>Bug (Zenprise) :</b> Cette méthode lève toujours une exception car Zenprise n'autorise pas cette opération une fois la liste par défaut existante">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applications" type="Array" required="true" hint="Applications de la whitelist. Chaque élément du tableau est une structure contenant les clé suivantes : identifier, name">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée dans la version actuelle de Zenprise">
		<cfset var soapRequest="">
		<cfset var app=ARGUMENTS["applications"]>
		<cfset var appCount=arrayLen(app)>
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:updateDefaultWhiteList>
							<zenprise:osFamily xsi:nil="false">#ARGUMENTS["osFamily"]#</zenprise:osFamily>
							<zenprise:applications xsi:nil="false">
							<cfloop index="i" from="1" to="#appCount#">
								<cfset var appId=app[i]["identifier"]>
								<cfif TRIM(appId) EQ "">
									<cfset appId=app[i]["name"]>
								</cfif>
								<ns2:BWApplication xmlns:ns2="urn:BeanService" xsi:type="ns2:BWApplication">
									<zenprise:identifier xsi:type="soapenc:string">#appId#</zenprise:identifier>
									<zenprise:name xsi:nil="false" xsi:type="soapenc:string">#app[i]["name"]#</zenprise:name>
								</ns2:BWApplication>
							</cfloop>
							</zenprise:applications>
						</zenprise:updateDefaultWhiteList>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendWhitelistRequest(soapRequest)>
		<cfdump var="#soapResponse#">
		<cfif yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service Whitelist mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="removeWhiteList" returntype="Void" hint="Supprime une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfset whitelistService.removeWhiteList(ARGUMENTS["name"],ARGUMENTS["osFamily"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addApplicationToWhiteList" returntype="Void" hint="Ajoute une application dans une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfset whitelistService.addApplicationToWhiteList(ARGUMENTS["name"],ARGUMENTS["osFamily"],ARGUMENTS["applicationName"],ARGUMENTS["applicationIdentifier"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addApplicationToDefaultWhiteList" returntype="Void" hint="Ajoute une application dans la whitelist par défaut de l'OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name pour l'application">
		<cfargument name="applicationIdentifier" type="String" required="true" hint="Valeur de la propriété identifier pour l'application">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfset whitelistService.addApplicationToDefaultWhiteList(ARGUMENTS["osFamily"],ARGUMENTS["applicationName"],ARGUMENTS["applicationIdentifier"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="updateApplicationInWhiteList" returntype="Void" hint="Met à jour une application dans une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfset whitelistService.updateApplicationInWhiteList(
				ARGUMENTS["name"],ARGUMENTS["osFamily"],ARGUMENTS["applicationOldName"],ARGUMENTS["applicationNewName"],ARGUMENTS["applicationNewIdentifier"]
			)>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="updateApplicationInDefaultWhiteList" returntype="Void" hint="Met à jour une application dans la whitelist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationOldName" type="String" required="true" hint="Valeur actuelle de la propriété name de l'application">
		<cfargument name="applicationNewName" type="String" required="true" hint="Nouvelle valeur pour de la propriété name de l'application">
		<cfargument name="applicationNewIdentifier" type="String" required="true" hint="Nouvelle valeur pour de la propriété identifier de l'application">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfset whitelistService.updateApplicationInDefaultWhiteList(
				ARGUMENTS["osFamily"],ARGUMENTS["applicationOldName"],ARGUMENTS["applicationNewName"],ARGUMENTS["applicationNewIdentifier"]
			)>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeApplicationFromWhiteList" returntype="Void" hint="Supprime une application d'une whitelist d'un OS">
		<cfargument name="name" type="String" required="true" hint="Nom pour la whitelist">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfset whitelistService.removeApplicationFromWhiteList(ARGUMENTS["name"],ARGUMENTS["osFamily"],ARGUMENTS["applicationName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeApplicationFromDefaultWhiteList" returntype="Void" hint="Supprime une application de la whitelist par défaut d'un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="applicationName" type="String" required="true" hint="Valeur de la propriété name de l'application">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var whitelistService=zdmRepository.getWhitelistService()>
		<cftry>
			<cfset whitelistService.removeApplicationFromWhiteList(ARGUMENTS["osFamily"],ARGUMENTS["applicationName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>