<cfcomponent name="utilisateurService">
<cffunction name="getInfosConnexionUser" access="remote" returntype="query" description="Retourne les informations du user connecte a l appli">
		<cfargument name="idActeur" type="numeric" required="true">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_mbp_login.getInfosConnectedActor">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idActeur#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
	</cffunction>
<cffunction name="addUtilisateur" access="remote" returntype="numeric" description="">
	<cfargument name="nom" type="string" required="true">
	<cfargument name="prenom" type="string" required="true">
	<cfargument name="fixe" type="numeric" required="true">
	<cfargument name="mobile" type="numeric" required="true">
	<cfargument name="email" type="string" required="true">
	<cfargument name="idSite" type="numeric" required="true">
	<cfargument name="idIhmUser" type="numeric" required="true">
	<cfreturn 1>
</cffunction>
<cffunction name="updateUtilisateur" access="remote" returntype="numeric" description="">
	<cfargument name="idUtilisateur" type="numeric" required="true">
	<cfargument name="nom" type="string" required="true">
	<cfargument name="prenom" type="string" required="true">
	<cfargument name="fixe" type="numeric" required="true">
	<cfargument name="mobile" type="numeric" required="true">
	<cfargument name="email" type="string" required="true">
	<cfargument name="idSite" type="numeric" required="true">
	<cfargument name="idIhmUser" type="numeric" required="true">
	<cfreturn 1>
</cffunction>
<cffunction name="getInfosBlocUtilisateur" access="remote" returntype="query" description="">
		<cfargument name="idActeur" type="numeric" required="true">
		<cfstoredproc datasource="MBPOFFRE" procedure="pkg_mbp_global.getInfosBlockUtilisateurs">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idActeur#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result> 
</cffunction>
<cffunction name="removeXUser" access="remote" returntype="Numeric">
		<cfargument name="arr" type="Array">
		<cfargument name="idihmUser" type="numeric">
		<cfset tmp = 0>
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>	
			<cfstoredproc datasource="MBPOFFRE" procedure="pkg_parc.removeCollaborateur">
				<cfprocparam value="#val(arr[i]['USER_ID'])#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam value="#idihmUser#" cfsqltype="CF_SQL_INTEGER" type="in"> 
				<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">		
			</cfstoredproc>
			<cfif p_result eq -1>
				<cfset tmp=0>
			<cfelse>
				<cfset tmp=1>	
			</cfif>
		</cfloop>
		<cfreturn tmp>
</cffunction>
<cffunction name="listeUtilisateur" access="remote" returntype="query" description="">utilisateurService
	<!---<cfargument name="idSociete" type="numeric" required="true">
	<cfargument name="typeUser" type="numeric" required="true">
	<cfargument name="avUtilisateur" type="numeric" required="true">
	<cfargument name="cle" type="String" required="true">
	<cfargument name="idSite" type="numeric" required="true">
	<cfstoredproc datasource="MBPOFFRE" procedure="pkg_mbp_global.listePack">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#typeUser#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#avUtilisateur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#cle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idSite#" null="false">
	<cfprocresult name="p_result">
	</cfstoredproc>--->
	<cfset p_result=listePackDemo()>
	<cfreturn p_result>
</cffunction>

<cffunction name="listeUtilisateur2" access="remote" returntype="query" description="">
	<cfargument name="listeidSite" type="String" required="true">
	<cfargument name="idSociete" type="numeric" required="true">
	<cfargument name="avPack" type="numeric" required="true">
	<cfargument name="listetypePack" type="String" required="true">
	<cfstoredproc datasource="MBPOFFRE" procedure="pkg_parc.SearchPack">
		<cfif listeidSite neq "-1">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeidSite#" null="false">
		<cfelse>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listeidSite#" null="true">
		</cfif>
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
		
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#avPack#" null="false">
		<cfif listetypePack neq "-1">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listetypePack#" null="false">
		<cfelse>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#listetypePack#" null="true">
		</cfif>
	
	<cfprocresult name="p_result">
	</cfstoredproc>
<cfreturn p_result>
</cffunction>

<cffunction name="listePackDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("PACK_ID,PACK_TYPE,PACK_LIBELLE,COMMANDE_ID,COMMANDE_REF,NOM,PRENOM,USERCIVILITE,USER_IHM_ID,SITE_LIBELLE,SITE_ID,LIBELLE_ENR,IMEI,FIXE,MOBILE,COMMANDEENCOURS","Integer,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,Integer,VarChar,VarChar,VarChar,VarChar,Integer")>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",154586715);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Bureau"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",154678515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Mobile"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","pierre"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",0);            
		QuerySetCell(myQuery,"PACK_LIBELLE","");
		QuerySetCell(myQuery,"PACK_TYPE",""); 
		QuerySetCell(myQuery,"COMMANDE_ID","");
		QuerySetCell(myQuery,"COMMANDE_REF","");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID",""); 
		QuerySetCell(myQuery,"SITE_LIBELLE",""); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",1567864515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Mobile"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",1548678515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Mobile"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","1");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",154678515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Contact"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",154578515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Mobile"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","fred"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",15487515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Contact"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",15488515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Integral"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",154515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Integral"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","jean"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",154515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Mobile"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","0");
	</cfscript>
	<cfscript> 
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"PACK_ID",154515);            
		QuerySetCell(myQuery,"PACK_LIBELLE","libellePack");
		QuerySetCell(myQuery,"PACK_TYPE","Mobile"); 
		QuerySetCell(myQuery,"COMMANDE_ID","15654");
		QuerySetCell(myQuery,"COMMANDE_REF","ref15645");
		QuerySetCell(myQuery,"NOM","le gallic");  
		QuerySetCell(myQuery,"PRENOM","vincent"); 
		QuerySetCell(myQuery,"USERCIVILITE","Mr"); 
		QuerySetCell(myQuery,"USER_IHM_ID","1"); 
		QuerySetCell(myQuery,"SITE_LIBELLE","Nantes"); 
		QuerySetCell(myQuery,"SITE_ID","1");
		QuerySetCell(myQuery,"LIBELLE_ENR","0015187");
		QuerySetCell(myQuery,"IMEI","0215415445644");
		QuerySetCell(myQuery,"FIXE","0134855585");
		QuerySetCell(myQuery,"MOBILE","0606060606");
		QuerySetCell(myQuery,"COMMANDEENCOURS","1");
	</cfscript>
<cfreturn myQuery>
</cffunction>
</cfcomponent>