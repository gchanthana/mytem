package univers.inventaire.equipements.lignes.v2.views
{
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.controls.Tree;
	import composants.util.TextInputLabeled;
	import mx.events.ListEvent;
	import mx.containers.HBox;
	import flash.events.Event;
	import mx.containers.ViewStack;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	[Bindable]
	public class GestionTechniqueImpl extends BaseView
	{
		
		
		
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public var vsMain : ViewStack;
		public var cmpDetailTechniqueLigne : DetailTechniqueLigneView;
		public var cmpLigneSelector : LigneSeledctorView;
		
		
		
		
		
		
		
		
		public function GestionTechniqueImpl()
		{
			super();
		}
		
		
		override public function onPerimetreChange():void{
			vsMain.selectedChild = cmpLigneSelector;
			cmpLigneSelector.onPerimetreChange();
			
			
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties(); 			
			cmpLigneSelector.addEventListener("LigneSelected",cmpLigneSelectorLigneSelectedHandler);
			cmpDetailTechniqueLigne.addEventListener("DeatilRetourClicked",cmpDetailTechniqueLigneDeatilRetourClickedHandler);
		}
		
		
		
				
		protected function cmpLigneSelectorLigneSelectedHandler(ev : Event):void{
			vsMain.selectedChild = cmpDetailTechniqueLigne;
			cmpDetailTechniqueLigne.vsOnglets.selectedIndex = 0;
			BaseView(cmpDetailTechniqueLigne.vsOnglets.selectedChild).getData();	
			cmpDetailTechniqueLigne.cmpOngletAffectationView.configurerOnglet();
			cmpDetailTechniqueLigne.cmpOngletCablageView.configurerOnglet();
		}
		
		
		
		
		protected function cmpDetailTechniqueLigneDeatilRetourClickedHandler(ev : Event):void{
			vsMain.selectedChild = cmpLigneSelector;
			cmpLigneSelector.dgLignes.selectedIndex = -1;
			 
		}		
	}
}