package entity
{
	public class PieceJointeActualiteVO
	{
		//----------- VARIABLES -----------//
		
		private var _IDACTUALITE:int;
		private var _IDFILE:int;
		private var _EXTENSION:String;
		private var _NOM:String;
		private var _UUID:String;
		private var _ENBASE:Boolean;
		
		//----------- METHODES -----------//
		
		/* */
		public function PieceJointeActualiteVO()
		{
		}
		
		/* remplir le VO */
		public function fill(obj:Object):Boolean
		{
			try
			{
				if(obj.hasOwnProperty("EXTENSION"))
					this._EXTENSION = obj.EXTENSION;
				if(obj.hasOwnProperty("IDARTICLE"))
					this._IDACTUALITE = obj.IDARTICLE;
				if(obj.hasOwnProperty("IDFILE"))
					this._IDFILE = obj.IDFILE;
				if(obj.hasOwnProperty("NOM"))
					this._NOM = obj.NOM;
				if(obj.hasOwnProperty("UUID"))
					this._UUID = obj.UUID;
				this._ENBASE = true;
				
				return true;
			}
			catch(e:Error)
			{
				return false;
			}
			return false;
		}
	
		/* transforme le type de l'objet en PieceJointeActualiteVO */
		public function parseToPJVO(obj:FileUpload,idActu:int):Boolean
		{
			try
			{
				if(obj.hasOwnProperty("fileExt"))
					this._EXTENSION = obj.fileExt;
				if(obj.hasOwnProperty("fileId"))
					this._IDFILE = obj.fileId;
				if(obj.hasOwnProperty("fileName"))
					this._NOM = obj.fileName;
				if(obj.hasOwnProperty("fileUUID"))
					this._UUID = obj.fileUUID;
				
				this._ENBASE = false;
				this._IDACTUALITE = idActu;
				
				return true;
			}
			catch(e:Error)
			{
				return false;
			}
			return false;
		}
		
		public function get IDACTUALITE():int
		{
			return _IDACTUALITE;
		}

		public function set IDACTUALITE(value:int):void
		{
			_IDACTUALITE = value;
		}

		public function get IDFILE():int
		{
			return _IDFILE;
		}

		public function set IDFILE(value:int):void
		{
			_IDFILE = value;
		}

		public function get EXTENSION():String
		{
			return _EXTENSION;
		}

		public function set EXTENSION(value:String):void
		{
			_EXTENSION = value;
		}

		public function get NOM():String
		{
			return _NOM;
		}

		public function set NOM(value:String):void
		{
			_NOM = value;
		}

		public function get UUID():String
		{
			return _UUID;
		}

		public function set UUID(value:String):void
		{
			_UUID = value;
		}

		public function get ENBASE():Boolean
		{
			return _ENBASE;
		}

		public function set ENBASE(value:Boolean):void
		{
			_ENBASE = value;
		}
	}
}