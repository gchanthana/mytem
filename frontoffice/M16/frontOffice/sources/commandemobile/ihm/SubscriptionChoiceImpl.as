package commandemobile.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.RessourcesNumber;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.popup.PopUpChoiceOptionsIHM;
	import gestioncommande.popup.PopUpChoiceSubscriptionIHM;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.RessourceService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class SubscriptionChoiceImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cboxEngagement				:ComboBox;
		
		public var dgListOptions				:DataGrid;
				
		public var boxVisible					:Box;
		
		public var btnChoisirAbo				:Button;
		public var btnChoisirOpt				:Button;

		//--------------------------------------------------------------------------------------------//
		//					POPUP
		//--------------------------------------------------------------------------------------------//
		
		private var popUpSubscrib				:PopUpChoiceSubscriptionIHM;
		
		private var popUpOptions				:PopUpChoiceOptionsIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		private var _cmd						:Commande;
		
		private var _myElements					:ElementsCommande2	= new ElementsCommande2();
		
		private var _cartesim					:EquipementsElements = null;
		
		private var _ressNumber					:RessourcesNumber;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipements				:EquipementService = new EquipementService();
		
		private var _ressources					:RessourceService = new RessourceService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var strgAbo						:String = "";
		public var strgPrixAbo					:String = "";
		
		public var montantAbonnement			:Number = 0;
		public var montantOptions				:Number = 0;

		public var isEngagementVisible			:Boolean = false;
		
		private var _isAboReturn				:Boolean = false;
		private var _isOptReturn				:Boolean = false;
		private var _isEngReturn				:Boolean = false;
		private var _isSimReturn				:Boolean = false;	
		private var _isNbRessAvailable			:Boolean = false;
		private var _razl						:Boolean = false;
		
		private var _idOpe						:int = -1;
		
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 3;
		public var ACCESS						:Boolean = false;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function SubscriptionChoiceImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function validateCurrentPage():Boolean
		{
			var isOK:Boolean = true;
			
			if(_myElements.ABONNEMENTS.length != 0)
			{
				if(isEngagementVisible)
				{
					if(cboxEngagement.selectedItem == null)
					{
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_engagement'), 'Consoview', null);
						isOK = false;
					}
					else
					{
						_myElements.ENGAGEMENT = cboxEngagement.selectedItem as Engagement;
						isOK = true;
					}
				}
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_abonnement'), 'Consoview', null);
				isOK = false;
			}
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				if(!_myElements.TERMINAUX.contains(_cartesim))
					_myElements.TERMINAUX.addItem(_cartesim);
			}
			
			return isOK;
		}
		
		//reinitialise l'onglet
		public function reset():void
		{
			strgAbo = "";
			strgPrixAbo = "";
			_idOpe = -1;
			isEngagementVisible = false;
			_cartesim = null;
		}		
		
		public function setConfiguration():void
		{
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				_razl = isEngagementVisible = true;
				
				getEngagements();
				getCarteSim();
			}
			else
				_razl = false;
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//

		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES ABONNEMENTS
		protected function btnChoisirClickHandler(me:MouseEvent = null):void
		{
			popUpSubscrib 			 = new PopUpChoiceSubscriptionIHM();
			popUpSubscrib.cmd 		 = _cmd;
			popUpSubscrib.myElements = _myElements;
			
			popUpSubscrib.addEventListener("POPUP_ABONNEMENTS_CLOSED_AND_VALIDATE", closedPopUpSubscribHandler);
			
			PopUpManager.addPopUp(popUpSubscrib, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(popUpSubscrib);
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES OPTIONS
		protected function btnOptionsClickHandler(me:MouseEvent):void
		{
			popUpOptions 			= new PopUpChoiceOptionsIHM();
			popUpOptions.cmd 		= _cmd;
			popUpOptions.myElements = _myElements;

			popUpOptions.addEventListener("POPUP_OPTIONS_CLOSED_AND_VALIDATE", closedPopUpOptionsHandler);
			
			PopUpManager.addPopUp(popUpOptions, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(popUpOptions);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener('REMOVE_OPTIONS', removeOptionsHandler);
		}
		
		private function showHandler(fe:FlexEvent):void 
		{
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			if(_idOpe != _cmd.IDOPERATEUR)
			{
				_isNbRessAvailable = false;
				_ressNumber = null;
				_ressNumber = new RessourcesNumber(_cmd.IDOPERATEUR, _cmd.IDPROFIL_EQUIPEMENT);
				_ressNumber.addEventListener('RESSOURCES_NUMBER', _ressNumberHandler);
			}
				
				
			if(_myElements.ABONNEMENTS.length > 0)
				boxVisible.visible = true;
			else
			{
				getDefaultAbonnement();
				getDefaultOptions();
				
				boxVisible.visible = false;
			}

			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				isEngagementVisible = true;
				
				if(_idOpe != _cmd.IDOPERATEUR)
				{
					getEngagements();
					getCarteSim();
					
					_idOpe = _cmd.IDOPERATEUR;
				}
			}
			else
			{
				isEngagementVisible = false;
				
				_isEngReturn = true;
				_isSimReturn = true;
			}
		}
		
		private function checkIfOnly():void
		{
			var isPass:Boolean = false;
			
			if(_isAboReturn && _isEngReturn && _isOptReturn && _isSimReturn && _isNbRessAvailable)
			{
				if(_ressNumber.NB_ABO == 1)
				{
					isPass = true;
				}
				else if(_ressNumber.NB_ABO > 1 && _ressNumber.NB_ABO_OBL > 0)
					{
						isPass = true;
					}
				
				btnChoisirAbo.enabled = !isPass;
				
				if(isPass)
				{
					if(_ressNumber.NB_OPT == 0 || _ressNumber.NB_OPT == _ressNumber.NB_OPT_OBL)
						isPass = true;
					else
						isPass = false;
				}
				
				btnChoisirOpt.enabled = !isPass;
				
				if(isEngagementVisible)
				{
					if(isPass && _equipements.listEngagement.length == 1)
						isPass = true;
					else
						isPass = false;
				}
			}
			_idOpe =_cmd.IDOPERATEUR;
			if(isPass)
				dispatchEvent(new Event('ONE_ENGAGEMENT_RESSOURCES', true));
		}
		
		private function removeOptionsHandler(e:Event):void
		{
			if(dgListOptions.selectedItem != null)
				(dgListOptions.dataProvider as ArrayCollection).removeItemAt(dgListOptions.selectedIndex);
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURE
		//--------------------------------------------------------------------------------------------//

		private function abonnementAssoHandler(ce:CommandeEvent):void
		{
			_isAboReturn = true;
			
			var value:ArrayCollection = _ressources.ABO_ASSOCIATED;
			
			if(value.length > 0)
			{
				addToItemSelected(value, true);
				
				btnChoisirAbo.enabled = !_myElements.ABONNEMENTS[0].OBLIGATORY;
			}
			else
				btnChoisirClickHandler();
			
			checkIfOnly();
		}
		
		private function _ressNumberHandler(evt:Event):void
		{
			_isNbRessAvailable = true;
			checkIfOnly();
		}
		
		private function optionsAssoHandler(ce:CommandeEvent):void
		{
			_isOptReturn = true;
			
			var value:ArrayCollection = _ressources.OPT_ASSOCIATED;
			
			if(value.length > 0)
				addToItemSelected(value, false);
			
			checkIfOnly();
		}
		
		private function listEngagementHandler(cmde:CommandeEvent):void
		{
			_isEngReturn = true;
			
			if(_equipements.listEngagement.length > 1)
				cboxEngagement.prompt = ResourceManager.getInstance().getString('M16','Choisir_la_dur_e_d_engagement');
			else
			{
				if(_equipements.listEngagement.length == 0)
					cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Aucun_engagement');
				else
					cboxEngagement.prompt = "";
			}
			
			cboxEngagement.dataProvider = _equipements.listEngagement;
			
			if(_razl)
				getEngegementSelected(cboxEngagement.dataProvider as ArrayCollection);
			
			checkIfOnly();
		}
		
		private function simHandler(cmde:CommandeEvent):void
		{
			_isSimReturn = true;
			
			_cartesim = _equipements.simcard;
			
			checkIfOnly();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM FERMETURE POPUPs
		//--------------------------------------------------------------------------------------------//
		
		//AFFECTE LES OPTIONS SELECTIONNEES A '_myElements.ABO' ET 
		//AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos abonnements'
		private function closedPopUpSubscribHandler(e:Event):void
		{
			if(_myElements.ABONNEMENTS.length > 0)
			{
				var ref:String = (_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT)?_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT + ' | ' :'';
				strgAbo 	=  ref + _myElements.ABONNEMENTS[0].LIBELLE;
				strgPrixAbo = _myElements.ABONNEMENTS[0].PRIX_STRG;
				
				boxVisible.visible = true;
			}
			else
				boxVisible.visible = false;
		}
		
		//AFFECTE LES OPTIONS SELECTIONNEES A '_myElements.OPTIONS' ET 
		//AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos options'
		private function closedPopUpOptionsHandler(e:Event):void
		{
			_cmd 		= popUpOptions.cmd;
			_myElements = popUpOptions.myElements;
			
			(dgListOptions.dataProvider as ArrayCollection).refresh();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS DE PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function getEngagements():void
		{
			_isEngReturn = false;
			
			_equipements.addEventListener(CommandeEvent.LISTED_ENGAGEMENTS, listEngagementHandler);
			_equipements.fournirNouvelleEngagement(_cmd.IDOPERATEUR);			
		}	
		
		private function getCarteSim():void
		{
			_isSimReturn = false;
			
			_equipements.addEventListener(CommandeEvent.LISTED_SIM, simHandler);
			_equipements.fournirSimCard(_cmd.IDREVENDEUR, _cmd.IDOPERATEUR, _cmd.IDPROFIL_EQUIPEMENT);
			
		}
		
		private function getDefaultAbonnement():void
		{
			_isAboReturn = false;
			
			_ressources.addEventListener(CommandeEvent.LISTED_ABO_ASS, abonnementAssoHandler);
			_ressources.getassociatedabo(_cmd.IDPROFIL_EQUIPEMENT, _cmd.IDOPERATEUR);
			
		}
		
		private function getDefaultOptions():void
		{
			_isOptReturn = false;
			
			_ressources.addEventListener(CommandeEvent.LISTED_OPT_ASS, optionsAssoHandler);
			_ressources.getassociatedoptions(_cmd.IDPROFIL_EQUIPEMENT, _cmd.IDOPERATEUR);
			
		}
		
		
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function addToItemSelected(value:ArrayCollection,isAbo:Boolean):void
		{
			if(isAbo)
			{
				_myElements.ABONNEMENTS = new ArrayCollection();
				_myElements.ABONNEMENTS.addItem(value[0]);
				var ref:String = (_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT)?_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT + ' | ' :'';
				strgAbo 	=  ref  + _myElements.ABONNEMENTS[0].LIBELLE;
				strgPrixAbo = _myElements.ABONNEMENTS[0].PRIX_STRG;

				boxVisible.visible = true;
			}
			else
			{	
				_myElements.OPTIONS = new ArrayCollection();
				
				var len:int = value.length;
				
				for(var i:int = 0;i < len;i++)
				{
					_myElements.OPTIONS.addItem(value[i]);
				}
			}
		}
		
		private function getEngegementSelected(values:ArrayCollection):void
		{
			var duree	:int = int(SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.VALUE);
			var len		:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].VALUE == duree)
				{
					cboxEngagement.selectedIndex = i;
					break;
				}
			}
			
			_razl = false;
		}
		
	}
}
