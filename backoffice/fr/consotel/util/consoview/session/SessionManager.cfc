<cfcomponent name="SessionManager" alias="fr.consotel.util.consoview.session.SessionManager">
	<cfset accessKey = "CONSOVIEW_3_SESSION_MANAGER">

	<cffunction name="initAppSession" access="public" returntype="boolean">
		<cfargument name="applicationKey" required="true" type="string">
		<cfargument name="sessionKey" required="true" type="string">
		<cfargument name="userInfos" required="true" type="struct">
		<cfargument name="secretKey" required="false" type="string">
		<cfif isDefined('arguments.secretKey')>
			<cfif arguments.secretKey EQ accessKey>
				<cfset sessionTracker = createObject("java","coldfusion.runtime.SessionTracker")>
				<cfset sessionList = sessionTracker.getSessionCollection("#applicationKey#")>
				<cfif structKeyExists(sessionList,"#sessionKey#")>
					<cfset currentSession = sessionList['#sessionKey#']>
					<cfset currentSession.CONNECTED = 1>
					<cfset currentSession.USER = structNew()>
					<cfset currentSession.USER.NOM = userInfos.NOM>
					<cfset currentSession.USER.PRENOM = userInfos.PRENOM>
					<cfset currentSession.USER.EMAIL = userInfos.EMAIL>
					<cfset currentSession.USER.CLIENTACCESSID = userInfos.CLIENTACCESSID>
					<cfset currentSession.OffreDSN = "ROCOFFRE">
					<cfset currentSession.CDRDSN = "ROCCDR">
					<cfset currentSession.PARCVIEWDSN = "CLPARCVIEW">
					<!--- Suppression des rÃ©fÃ©rences (Garbage Collector) --->
					<cfset sessionTracker = ''>
					<cfset sessionList = ''>
					<cfset currentSession = ''>
					<cfset userInfos = ''>
					<cfreturn TRUE>
				</cfif>
			</cfif>
		<cfelse>
		   	<cflog type="Information" text="SessionManager - initAppSession() - ACCESS KEY UNDEFINED">
		</cfif>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="updateTrackingInfos" access="public" returntype="boolean">
		<cfargument name="applicationKey" required="true" type="string">
		<cfargument name="sessionKey" required="true" type="string">
		<cfargument name="trackingInfos" required="true" type="struct">
		<cfargument name="secretKey" required="false" type="string">
		<cfif isDefined('arguments.secretKey')>
			<cfif arguments.secretKey EQ accessKey>
				<cfset sessionTracker = createObject("java","coldfusion.runtime.SessionTracker")>
				<cfset sessionList = sessionTracker.getSessionCollection("#applicationKey#")>
				<cfif structKeyExists(sessionList,"#sessionKey#")>
					<cfset currentSession = sessionList['#sessionKey#']>
					<cfif NOT structKeyExists(currentSession,"TRACKING")>
						<cfset currentSession.TRACKING = structNew()>
					</cfif>
					<cfset currentSession.TRACKING.IP_ADDR = trackingInfos.IP_ADDR>
					<cfif NOT structKeyExists(currentSession.TRACKING,"REQUEST_COUNT")>
						<!--- 2 : Car en comptant le cas du credentials (-1,-1) qui n'est pas trackÃ© --->
						<cfset currentSession.TRACKING.REQUEST_COUNT = 2>
					<cfelse>
						<cfset currentSession.TRACKING.REQUEST_COUNT = currentSession.TRACKING.REQUEST_COUNT + 1>
					</cfif>
					<cfif NOT structKeyExists(currentSession.TRACKING,"DATE_DEB")>
						<cfset currentSession.TRACKING.DATE_DEB = lsDateFormat(NOW(),"DD MMMM YYYY") & " - " &
																	lsTimeFormat(NOW(),"HH:mm:ss")>
					</cfif>
					<cfset currentSession.TRACKING.DATE_LAST = lsDateFormat(NOW(),"DD MMMM YYYY") & " - " &
																	lsTimeFormat(NOW(),"HH:mm:ss")>
					<cfset sessionTracker = ''>
					<cfset sessionList = ''>
					<cfset currentSession = ''>
					<cfset trackingInfos = ''>
					<cfreturn TRUE>
				</cfif>
			</cfif>
		<cfelse>
		   	<cflog type="Information" text="SessionManager - initAppSession() - ACCESS KEY UNDEFINED">
		</cfif>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="getSession" access="public" returntype="struct">
		<cfargument name="applicationKey" required="true" type="string">
		<cfargument name="sessionKey" required="true" type="string">
		<cfargument name="secretKey" required="false" type="string">
		<cfset dsnStruct = structNew()>
		<cfif isDefined('arguments.secretKey')>
			<cfif arguments.secretKey EQ accessKey>
				<cfset sessionTracker = createObject("java","coldfusion.runtime.SessionTracker")>
				<cfset sessionList = sessionTracker.getSessionCollection("#applicationKey#")>
				<cfif structKeyExists(sessionList,"#sessionKey#")>
					<cfset currentSession = sessionList['#sessionKey#']>
					<cfreturn currentSession>
				<cfelse>
				   	<cflog type="Information" text="SessionManager - getDsnStruct() - SESSION KEY UNDEFINED">
					<cfreturn ''>
				</cfif>
			<cfelse>
				<cfreturn ''>
			   	<cflog type="Information" text="SessionManager - getDsnStruct() - ACCESS KEY INVALID">
			</cfif>
		<cfelse>
			<cfreturn ''>
		   	<cflog type="Information" text="SessionManager - getDsnStruct() - ACCESS KEY UNDEFINED">
		</cfif>
		<cfreturn ''>
	</cffunction>
	
	<cffunction name="logoffAppSession" access="public" returntype="boolean">
		<cfargument name="applicationKey" required="true" type="string">
		<cfargument name="sessionKey" required="true" type="string">
		<cfargument name="secretKey" required="false" type="string">
		<cfif isDefined('arguments.secretKey')>
			<cfif arguments.secretKey EQ accessKey>
				<cfset sessionTracker = createObject("java","coldfusion.runtime.SessionTracker")>
				<cfset sessionList = sessionTracker.getSessionCollection("#applicationKey#")>
				<cfif structKeyExists(sessionList,"#sessionKey#")>
					<cfset currentSession = sessionList['#sessionKey#']>
					<cfset structDelete(currentSession,"CONNECTED")>
					<cfset structDelete(currentSession,"USER")>
					<cfset structDelete(currentSession,"PERIMETRE")>
					<!--- ******* Tableau de bord *******--->
					<cfset structDelete(currentSession,"PARAMETRE")>
					<cfset structDelete(currentSession,"PARAMETRES")>
					<cfset structDelete(currentSession,"RECORDSET")>
					<!--- ************************************** --->
					<cfset structDelete(currentSession,"OffreDSN")>
					<cfset structDelete(currentSession,"CDRDSN")>
					<cfset structDelete(currentSession,"PARCVIEWDSN")>
					<cfset structDelete(currentSession,"DATAUSAGE")>
					<cfset structDelete(currentSession,"DATAFACTURE")>
					<cfset structDelete(currentSession,"RECHERCHETB")>
					<cfset structDelete(currentSession,"TARIFS")>
					<!--- Suppression des rÃ©fÃ©rences (Garbage Collector) --->
					<cfset sessionTracker = ''>
					<cfset sessionList = ''>
					<cfset currentSession = ''>
					<cfreturn TRUE>
				</cfif>
			</cfif>
		<cfelse>
		   	<cflog type="Information" text="SessionManager - logoffAppSession() - ACCESS KEY UNDEFINED">
		</cfif>
		<cfreturn FALSE>
	</cffunction>
	
	<!--- 
	<cffunction name="getDsnStruct" access="public" returntype="struct">
		<cfargument name="applicationKey" required="true" type="string">
		<cfargument name="sessionKey" required="true" type="numeric">
		<cfargument name="secretKey" required="false" type="string">
		<cfset dsnStruct = structNew()>
		<cfif isDefined('arguments.secretKey')>
			<cfif arguments.secretKey EQ accessKey>
				<cfset sessionTracker = createObject("java","coldfusion.runtime.SessionTracker")>
				<cfset sessionList = sessionTracker.getSessionCollection("#applicationKey#")>
				<cfif structKeyExists(sessionList,"#sessionKey#")>
					<cfset dsnStruct.OffreDSN = "ROCOFFRE">
					<cfset dsnStruct.CDRDSN = "ROCCDR">
					<cfset dsnStruct.PARCVIEWDSN = "CLPARCVIEW">
				<cfelse>
					<cfset dsnStruct.OffreDSN = "">
					<cfset dsnStruct.CDRDSN = "">
					<cfset dsnStruct.PARCVIEWDSN = "">
				   	<cflog type="Information" text="SessionManager - getDsnStruct() - SESSION KEY UNDEFINED">
				</cfif>
			<cfelse>
				<cfset dsnStruct.OffreDSN = "">
				<cfset dsnStruct.CDRDSN = "">
				<cfset dsnStruct.PARCVIEWDSN = "">
			   	<cflog type="Information" text="SessionManager - getDsnStruct() - ACCESS KEY INVALID">
			</cfif>
		<cfelse>
			<cfset dsnStruct.OffreDSN = "">
			<cfset dsnStruct.CDRDSN = "">
			<cfset dsnStruct.PARCVIEWDSN = "">
		   	<cflog type="Information" text="SessionManager - getDsnStruct() - ACCESS KEY UNDEFINED">
		</cfif>
		<cfreturn dsnStruct>
	</cffunction>
	 --->
</cfcomponent>
