<!--- =========================================================================
Classe: RapportDeploiementSocieteStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent name="RapportDeploiementSocieteStrategy" extends="RapportStrategy" hint="">	
	<cffunction name="processQueryForm" access="private" returntype="query" output="false">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
			<cfquery name="qGetData" datasource="#session.offreDSN#">
					SELECT scl.idsous_tete,scl.sous_tete, scl.zipcode, scl.siteid, scl.commune, scl.code_site,
				    	scl.adresse1, scl.adresse2,
	                    nvl(a.duree, 0) as duree_ft, nvl(b.duree,0) AS duree_ft_avant, 
	                    nvl(c.duree,0) AS duree_autre, nvl(d.duree,0) as duree_autre_avant, 
	                    nvl(a.montant, 0) AS montant_ft, nvl(b.montant,0) AS montant_ft_avant,
	                    nvl(c.montant,0) AS montant_autre, nvl(d.montant,0) as montant_autre_avant
	               FROM
	               		-- requete pour le mois en cours et pour FT
	                    (SELECT idsous_tete, round(SUM(nvl(dfa.duree_appel,1)/60)) AS duree, SUM(dfa.montant) AS montant
	                     FROM	detail_facture_abo dfa, produit_client pcl, produit_catalogue pca, theme_produit_catalogue tpca, theme_produit tp,
				    					(	SELECT a.idinventaire_periode , a.numero_facture, a.date_emission
				    						FROM inventaire_periode a, compte_facturation b
				    						WHERE a.idcompte_facturation=b.idcompte_facturation
				    								AND b.idref_client=289
				    				   ) ip
	                     WHERE dfa.idproduit_client=pcl.Idproduit_Client
	                     AND pcl.Idproduit_Catalogue=pca.Idproduit_Catalogue
	                     AND pca.Idproduit_Catalogue=tpca.Idproduit_Catalogue
	                     AND tpca.Idtheme_Produit=tp.Idtheme_Produit
	                     AND dfa.idref_client=#ID_PERIMETRE# 
	                     AND pcl.Idref_Client=#ID_PERIMETRE#
	                     AND pca.operateurid=63
	                     AND tp.Idtheme_Produit<>28
	                     AND tp.Idtheme_Produit<>20
	                     AND nvl(dfa.typusg,'0') > '0'
	                     AND ip.idinventaire_periode=dfa.idinventaire_periode
	                     <!---
	                     AND trunc(ip.date_emission, 'MM') >= trunc(to_date('#RapportParams.date_m#','dd/mm/yyyy'), 'MM')
	                     AND trunc(ip.date_emission, 'MM') <= trunc(to_date('#RapportParams.date_m2#','dd/mm/yyyy'), 'MM')
	                     --->
	                     AND trunc(ip.date_emission, 'MM') >= trunc(to_date('01/05/06','dd/mm/yyyy'), 'MM')
	                     AND trunc(ip.date_emission, 'MM') <= trunc(to_date('01/07/06','dd/mm/yyyy'), 'MM')
	                     GROUP BY idsous_tete
	                     ) a,
	                     -- requete pour le mois precedent pour FT
	                    (SELECT idsous_tete, round(SUM(nvl(dfa.duree_appel,1)/60)) AS duree, SUM(dfa.montant) AS montant
	                     FROM 	detail_facture_abo dfa, produit_client pcl, produit_catalogue pca, theme_produit_catalogue tpca, theme_produit tp,
				   					(	SELECT a.idinventaire_periode , a.numero_facture, a.date_emission
				   						FROM inventaire_periode a, compte_facturation b
				   						WHERE a.idcompte_facturation=b.idcompte_facturation
				   								AND b.idref_client=#ID_PERIMETRE#
				   				   ) ip
	                     WHERE dfa.idproduit_client=pcl.Idproduit_Client
	                     AND pcl.Idproduit_Catalogue=pca.Idproduit_Catalogue
	                     AND pca.Idproduit_Catalogue=tpca.Idproduit_Catalogue
	                     AND tpca.Idtheme_Produit=tp.Idtheme_Produit
	                     AND dfa.idref_client=#ID_PERIMETRE#
	                     AND pcl.Idref_Client=#ID_PERIMETRE#
	                     AND pca.operateurid=63
	                     AND tp.Idtheme_Produit<>28
	                     AND tp.Idtheme_Produit<>20
	                     AND nvl(dfa.typusg,'0') > '0'
	                     AND ip.idinventaire_periode=dfa.idinventaire_periode
	                     <!---
	                     AND trunc(ip.date_emission, 'MM') >= trunc(to_date('#RapportParams.date_m_moins_deux#','dd/mm/yyyy'), 'MM')
	                     AND trunc(ip.date_emission, 'MM') <= trunc(to_date('#RapportParams.date_m_moins_un#','dd/mm/yyyy'), 'MM')
	                     --->
	                     AND trunc(ip.date_emission, 'MM') >= trunc(to_date('01/05/06','dd/mm/yyyy'), 'MM')
	                     AND trunc(ip.date_emission, 'MM') <= trunc(to_date('01/07/06','dd/mm/yyyy'), 'MM')
	                     GROUP BY idsous_tete
	                     ) b,
	                     -- requete pour le mois en cours pour OP alternatif
	                    (SELECT idsous_tete, round(SUM(nvl(dfa.duree_appel,1)/60)) AS duree, SUM(dfa.montant) AS montant
	                     FROM	detail_facture_abo dfa, produit_client pcl, produit_catalogue pca, theme_produit_catalogue tpca, theme_produit tp,
				    					(	SELECT a.idinventaire_periode , a.numero_facture, a.date_emission
				    						FROM inventaire_periode a, compte_facturation b
				    						WHERE a.idcompte_facturation=b.idcompte_facturation
				    								AND b.idref_client=289
				    				   ) ip
	                     WHERE dfa.idproduit_client=pcl.Idproduit_Client
	                     AND pcl.Idproduit_Catalogue=pca.Idproduit_Catalogue
	                     AND pca.Idproduit_Catalogue=tpca.Idproduit_Catalogue
	                     AND tpca.Idtheme_Produit=tp.Idtheme_Produit
	                     AND dfa.idref_client=#ID_PERIMETRE#
	                     AND pcl.Idref_Client=#ID_PERIMETRE#
	                     AND pca.operateurid<>63
	                     AND tp.Idtheme_Produit<>28
	                     AND tp.Idtheme_Produit<>20
	                     AND nvl(dfa.typusg,'0') > '0'
	                     and ip.idinventaire_periode=dfa.idinventaire_periode
	                     <!---
	                     AND trunc(ip.date_emission, 'MM') >= trunc(to_date('#RapportParams.date_m#','dd/mm/yyyy'), 'MM')
	                     AND trunc(ip.date_emission, 'MM') <= trunc(to_date('#RapportParams.date_m2#','dd/mm/yyyy'), 'MM')
	                     --->
	                     AND trunc(ip.date_emission, 'MM') >= trunc(to_date('01/05/06','dd/mm/yyyy'), 'MM')
	                     AND trunc(ip.date_emission, 'MM') <= trunc(to_date('01/07/06','dd/mm/yyyy'), 'MM')
	                     GROUP BY idsous_tete
	                     ) c,
	                     -- requete pour le mois precedent pour OP alternatif
	                    (SELECT idsous_tete, round(SUM(nvl(dfa.duree_appel,1)/60)) AS duree, SUM(dfa.montant) AS montant
	                     FROM   detail_facture_abo dfa, produit_client pcl, produit_catalogue pca, theme_produit_catalogue tpca, theme_produit tp,
				    					(	SELECT a.idinventaire_periode , a.numero_facture, a.date_emission
				    						FROM inventaire_periode a, compte_facturation b
				    						WHERE a.idcompte_facturation=b.idcompte_facturation
				    								AND b.idref_client=#ID_PERIMETRE#
				    				   ) ip
	                     WHERE dfa.idproduit_client=pcl.Idproduit_Client
			                     AND pcl.Idproduit_Catalogue=pca.Idproduit_Catalogue
			                     AND pca.Idproduit_Catalogue=tpca.Idproduit_Catalogue
			                     AND tpca.Idtheme_Produit=tp.Idtheme_Produit
			                     AND dfa.idref_client=289
			                     AND pcl.Idref_Client=289
			                     AND pca.operateurid<>63
			                     AND tp.Idtheme_Produit<>28
			                     AND tp.Idtheme_Produit<>20
			                     AND nvl(dfa.typusg,'0') > '0'
			                     AND ip.idinventaire_periode=dfa.idinventaire_periode
			                     AND trunc(ip.date_emission, 'MM') >= trunc(to_date('#RapportParams.date_m#','dd/mm/yyyy'), 'MM')
			                     AND trunc(ip.date_emission, 'MM') <= trunc(to_date('#RapportParams.date_m2#','dd/mm/yyyy'), 'MM')
	                     GROUP BY idsous_tete
	                     ) d,
	                    (SELECT st.idsous_tete, st.sous_tete, sc.*
	                     FROM   site_client sc , cli c, sous_tete st
	                     WHERE sc.siteid=c.siteid AND c.cliid=st.cliid
	                     AND clientid=#session.perimetre.clientID#
						) scl
	            WHERE scl.idsous_tete=a.idsous_tete (+)
	            AND scl.idsous_tete=b.idsous_tete (+)
	            AND scl.idsous_tete=c.idsous_tete (+)
	            AND scl.idsous_tete=d.idsous_tete (+)
	            AND ((nvl(a.duree,0)+nvl(c.duree,0)+nvl(a.montant,0)+nvl(c.montant,0))<>0)
       			AND	nvl(a.montant, 0) >= #RapportParams.f_montant_max#
	            ORDER BY commune, siteID
			</cfquery>
			<cfreturn qGetData>
		</cffunction>
		
		<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="RapportParams" required="true" type="struct" default="" />
		<cfargument name="ID_PERIMETRE" required="true" type="numeric" default="" />
		<cfargument name="RAISON_SOCIALE" required="true" type="string" default="" />
			<!--- Commun � tous --->
			<cfset cfr_type="">
			<cfset qGetData=processQueryForm(#RapportParams#,#ID_PERIMETRE#)>
		<cfif #qGetData.recordcount# gt 0>
			<!--- --->
			<cfset total_minutes=0>
			<cfset total_montant=0>
			<cfset total_minutes_autre_avant=0>
			<cfset total_montant_autre_avant=0>
			<cfset total_minutes_ft_avant=0>
			<cfset total_montant_ft_avant=0>
			<cfoutput query="qGetData">
				<cfset total_minutes=total_minutes+duree_ft>
				<cfset total_montant=total_montant+montant_ft>
				<cfset total_minutes_autre_avant=total_minutes_autre_avant+duree_autre_avant>
				<cfset total_montant_autre_avant=total_montant_autre_avant+montant_autre_avant>
				<cfset total_minutes_ft_avant=total_minutes_ft_avant+duree_ft_avant>
				<cfset total_montant_ft_avant=total_montant_ft_avant+montant_ft_avant>
			</cfoutput>			
				<cfif lcase(#RapportParams.format#) eq "excel">
					<cfinclude template="/report/deploiement_xls.cfm">
				<cfelseif lcase(#RapportParams.format#) eq "csv">
					<cfinclude template="/report/deploiement_csv.cfm">
				<cfelse>
					<cfreport format="#RapportParams.format#" template="#session.path#\report\rapport_suivi_de_deploiement#cfr_type#.cfr" query="qGetData">
						<cfreportparam name="total_minutes" value="#total_minutes#">
						<cfreportparam name="total_montant" value="#total_montant#">
						<cfreportparam name="client_raison_sociale" value="#raison_sociale#">
						<cfreportparam name="date_rapport" value="#RapportParams.chaine_date#">
						<cfreportparam name="idref_client" value="#ID_PERIMETRE#">
					</cfreport>
				</cfif>
				
		<cfelse>
			<cfoutput>
					<center><strong><h2>Aucunes donn�es r�pondant � votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
		</cffunction>
	</cfcomponent>
