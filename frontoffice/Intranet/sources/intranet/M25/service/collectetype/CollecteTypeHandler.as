package intranet.M25.service.collectetype
{
    import flash.events.EventDispatcher;
    import intranet.M25.entity.vo.CollecteClientVO;
    import intranet.M25.entity.vo.CollecteTypeVO;
    import intranet.M25.entity.vo.OperateurVO;
    import intranet.M25.entity.vo.SimpleEntityVO;
    import intranet.M25.event.CollecteEvent;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.events.ResultEvent;
    import mx.utils.StringUtil;

    [Bindable]
    public class CollecteTypeHandler extends EventDispatcher
    {
        private var myDatas:CollecteTypeData;

        public function CollecteTypeHandler(instanceOfDatas:CollecteTypeData)
        {
            myDatas = instanceOfDatas;
        }

        /******************************************************************************
         *                         Admin et Client : Procédures communes
         ******************************************************************************/ /**
         * Récupération de la liste des opérateurs
         *
         */
        internal function getOperateurListResultHandler(re:ResultEvent):void
        {
            myDatas.operateurList = new ArrayCollection();
            var resultCollec:ArrayCollection = ArrayCollection(re.result);
            var currentVo:OperateurVO = new OperateurVO();
            if(resultCollec.length > 0)
            {
                for(var i:int = 0; i < resultCollec.length; i++)
                {
                    currentVo = new OperateurVO();
                    currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as OperateurVO;
                    myDatas.operateurList.addItem(currentVo);
                }
                myDatas.operateurList.refresh();
                dispatchEvent(new CollecteEvent(CollecteEvent.OPERATEUR_LIST_LOADED_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.OPERATEUR_LIST_LOADED_ERROR));
                trace("Erreur dans la récupération de la liste des opérateurs de collecte");
            }
        }

        /**
         * Récupération de la liste des opérateurs possédant un template
         *
         */
        internal function getOperateurWithTemplateListResultHandler(re:ResultEvent):void
        {
            myDatas.operateurWithTemplateList = new ArrayCollection();
            var resultCollec:ArrayCollection = ArrayCollection(re.result);
            var currentVo:OperateurVO = new OperateurVO();
            if(resultCollec.length > 0)
            {
                for(var i:int = 0; i < resultCollec.length; i++)
                {
                    currentVo = new OperateurVO();
                    currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as OperateurVO;
                    myDatas.operateurWithTemplateList.addItem(currentVo);
                }
                myDatas.operateurWithTemplateList.refresh();
                dispatchEvent(new CollecteEvent(CollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.OPERATEUR_WITH_TEMPLATE_LIST_LOADED_ERROR));
                trace("Erreur dans la récupération de la liste des opérateurs avec template ");
            }
        }

        /**
         * Récupération de la liste des types de données
         *
         */
        internal function getDataTypeListResultHandler(re:ResultEvent):void
        {
            myDatas.dataTypeList = new ArrayCollection();
            var resultCollec:ArrayCollection = ArrayCollection(re.result);
            var currentVo:SimpleEntityVO = new SimpleEntityVO();
            if(resultCollec.length > 0)
            {
                for(var i:int = 0; i < resultCollec.length; i++)
                {
                    currentVo = new SimpleEntityVO();
                    currentVo.type = "DataType";
                    currentVo.label = resultCollec.getItemAt(i).LIBELLE_TYPE as String
                    currentVo.value = new Number(resultCollec.getItemAt(i).IDCOLLECT_TYPE)
                    myDatas.dataTypeList.addItem(currentVo);
                }
                dispatchEvent(new CollecteEvent(CollecteEvent.DATATYPE_LIST_LOADED_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.DATATYPE_LIST_LOADED_SUCCESS));
                trace("Erreur dans la récupération de la liste des types de données de collecte");
            }
        }

        /**
         * Récupération de la liste des modes de récupération
         *
         */
        internal function getRecupModeListResultHandler(re:ResultEvent):void
        {
            myDatas.recupModeList = new ArrayCollection();
            var resultCollec:ArrayCollection = ArrayCollection(re.result);
            var currentVo:SimpleEntityVO = new SimpleEntityVO();
            if(resultCollec.length > 0)
            {
                for(var i:int = 0; i < resultCollec.length; i++)
                {
                    currentVo = new SimpleEntityVO();
                    currentVo.type = "RecupMode";
                    currentVo.label = resultCollec.getItemAt(i).LIBELLE_MODE as String;
                    currentVo.value = new Number(resultCollec.getItemAt(i).IDCOLLECT_MODE);
                    myDatas.recupModeList.addItem(currentVo);
                }
                dispatchEvent(new CollecteEvent(CollecteEvent.RECUPMODE_LIST_LOADED_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.RECUPMODE_LIST_LOADED_ERROR));
                trace("Erreur dans la récupération de la liste des types de données de collecte");
            }
        }

        /******************************************************************************
         *                         Admin : Gestion des Templates
         ****************************************************************************/ /**
         * Retourne la liste des Template de collectes
         * @param re
         *
         */
        internal function getCollecteTypeListResultHandler(re:ResultEvent):void
        {
            if(re.result)
            {
                myDatas.collecteTypeVoList = new ArrayCollection();
                var resultCollec:ArrayCollection = ArrayCollection(re.result);
                var currentVo:CollecteTypeVO = new CollecteTypeVO();
                if(resultCollec.length > 0)
                {
                    for(var i:int = 0; i < resultCollec.length; i++)
                    {
                        currentVo = new CollecteTypeVO();
                        currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as CollecteTypeVO;
                        myDatas.collecteTypeVoList.addItem(currentVo);
                    }
                    dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS));
                }
                else
                {
                    dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_LIST_LOADED_ERROR));
                }
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_LIST_LOADED_ERROR));
                trace("Erreur dans la récupération des Templates");
            }
        }

        /**
         * Si l'édition/création s'est bien déroulée , retourne l'id du template
         * sinon code erreur (TODO)
         * @param re
         *
         */
        internal function createEditCollecteTypeVoResultHandler(re:ResultEvent):void
        {
            if(re.result)
            {
                var num:Number = re.result as Number;
                switch(num)
                {
                    case -10:
                        trace("Champs obligatoires vides");
                        dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
                        break;
                    case -11:
                        trace("Template existant");
                        dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
                        break;
                    case -12:
                        trace("Impossible de publier");
                        dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
                        break;
                    default:
                        dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS, num));
                        break;
                }
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR));
                trace("Erreur dans l'édition,création du template de collecte");
            }
        }

        /**
         * Retourne la liste des groupes maitres
         *
         */
        internal function getRacineListResultHandler(re:ResultEvent):void
        {
            myDatas.racineList = new ArrayCollection();
            var resultCollec:ArrayCollection = ArrayCollection(re.result);
            var item:Object = new Object();
            if(re.result)
            {
                for(var i:int = 0; i < resultCollec.length; i++)
                {
                    item = new Object();
                    item.RACINE = resultCollec.getItemAt(i).RACINE as String;
                    item.IDRACINE = new Number(resultCollec.getItemAt(i).IDRACINE);
                    item.NB_COLLECTES = new Number(resultCollec.getItemAt(i).NB_COLLECTES);
                    myDatas.racineList.addItem(item);
                }
                dispatchEvent(new CollecteEvent(CollecteEvent.RACINE_LIST_LOADED_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.RACINE_LIST_LOADED_ERROR));
                trace("Erreur dans la récupération de la liste des racine");
            }
        }

        /**
         * Retourne l'UID créee pour le stockage des docs associé à un
         * template
         * @param re
         *
         */
        internal function createUIDStorageResultHandler(re:ResultEvent):void
        {
            myDatas.uid = "";
            if(re.result.length > 0)
            {
                myDatas.uid = re.result as String;
                dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_UID_CREATED_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_UID_CREATED_ERROR));
                trace("Erreur dans la creation d'un uid");
            }
        }

        /**
         * Réponse serveur sur la destruction de l'uid
         *
         */
        internal function destroyLastUIDResultHandler(re:ResultEvent):void
        {
            if(re.result as Number == 1)
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_UID_DESTROY_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_UID_DESTROY_ERROR));
                trace("Erreur dans la destruction du dossier UID de la collecte");
            }
        }

        /******************************************************************************
         *                         Client : Gestion des Collectes
         ****************************************************************************/ /**
         * Retourne la liste des collectes clientes pour un client donné
         * @param re
         *
         */
        internal function getCollecteClientListResultHandler(re:ResultEvent):void
        {
            myDatas.collecteClientList = new ArrayCollection();
            if(re.result)
            {
                //myDatas.collecteClientList = new ArrayCollection();
                var resultCollec:ArrayCollection = ArrayCollection(re.result);
                var currentVo:CollecteClientVO = new CollecteClientVO();
                if(resultCollec.length > 0)
                {
                    for(var i:int = 0; i < resultCollec.length; i++)
                    {
                        currentVo = new CollecteClientVO();
                        currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as CollecteClientVO;
                        myDatas.collecteClientList.addItem(currentVo);
                    }
                    dispatchEvent(new CollecteEvent(CollecteEvent.COLLECTE_LIST_LOADED_SUCCESS));
                }
                else
                {
                    // cas : pas de collectes pour la racine
                    dispatchEvent(new CollecteEvent(CollecteEvent.COLLECTE_LIST_LOADED_SUCCESS));
                }
            }
            else
            {
                // cas : pas de collectes pour la racine
                dispatchEvent(new CollecteEvent(CollecteEvent.COLLECTE_LIST_LOADED_SUCCESS));
                trace("Pas de collecte pour cette racine");
            }
        }

        /**
         * Retourne la liste des Template à disposition du client
         * pour la création de sa collecte
         * @param re
         *
         */
        internal function getPublishedTemplateResultHandler(re:ResultEvent):void
        {
            if(re.result)
            {
                myDatas.publishedTemplateList = new ArrayCollection();
                var resultCollec:ArrayCollection = ArrayCollection(re.result);
                var currentVo:CollecteTypeVO = new CollecteTypeVO();
                if(resultCollec.length > 0)
                {
                    for(var i:int = 0; i < resultCollec.length; i++)
                    {
                        currentVo = new CollecteTypeVO();
                        currentVo = currentVo.deserialize(resultCollec.getItemAt(i)) as CollecteTypeVO;
                        myDatas.publishedTemplateList.addItem(currentVo);
                    }
                    dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_SUCCESS));
                }
                else
                {
                    dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR));
                }
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.TEMPLATE_FOR_CLIENT_LIST_LOADED_ERROR));
                trace("Erreur dans la récupération des Templates");
            }
        }

        /**
         * Si l'édition/création d'une collecte s'est bien déroulée , retourne l'id du template
         * sinon code erreur (TODO)
         * @param re
         *
         */
        internal function createEditCollecteClientVoResultHandler(re:ResultEvent):void
        {
            if(re.result > 0)
            {
                var idCollecte:Number = re.result as Number;
                dispatchEvent(new CollecteEvent(CollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, idCollecte));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.COLLECTE_CREATED_UPDATED_ERROR));
                trace("Erreur dans l'édition,création de la collecte");
            }
        }

        /**
         *  Résultat des test d'identification
         *
         */
        internal function testIdentificationResultHandler(re:ResultEvent):void
        {
            if(re.result == 1)
            {
                myDatas.identificationSuccess = true;
                dispatchEvent(new CollecteEvent(CollecteEvent.TEST_IDENTIFICATION_SUCCESS));
            }
            else
            {
                dispatchEvent(new CollecteEvent(CollecteEvent.RECUPMODE_LIST_LOADED_ERROR));
                myDatas.identificationSuccess = false;
                trace("Erreur dans le test d'identification");
            }
        }
    }
}