cv<!--- =========================================================================
Classe: te
Auteur: brice.miramont
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="Revendeur" hint="Pour la Gestion des revendeurs" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="remote" output="false" returntype="Revendeur" hint="Remplace le constructeur de Revendeur.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->

	<cffunction name="create_revendeur" access="remote" returntype="numeric" output="false" hint="Pour l'ajout d'un revendeur pour la gestion de parc Fixe / Data ou a partir du module Gestion Parc Mobile " >
		
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="libelle de la societe du revendeur" />
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric idracine" hint="idgroupe_client de la racine du perimetre sur laquelle on se trouve" />
		<cfargument name="segment_mobile" required="false" type="numeric" default="" displayname="numeric segment_mobile" hint="1 si couvre le segment mobile, 0 sinon" />
		<cfargument name="segment_fixe_data" required="false" type="numeric" default="" displayname="numeric segment_fixe_data" hint="1 séil couvre le segment fixe/data, 0 sinon" />
		<cfargument name="idDevise" required="false" type="numeric" default="" displayname="numeric idDevise" hint="l'id de la devise choisie par l'utilisateur, 1 > euro, 301 > dollar..."/>
		
		<cfif Idracine NEQ 0>
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.create_revendeur_v3">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle" value="#libelle#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#Idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" value="#segment_mobile#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" value="#segment_fixe_data#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_livraison" value="0">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_fpc" null="yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_iddevise" value="#idDevise#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
			<cfelse>
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.create_revendeur_v3">			
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle" value="#libelle#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" null="true">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" value="#segment_mobile#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" value="#segment_fixe_data#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_livraison" value="0">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_fpc" null="yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_iddevise" value="#idDevise#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>
		</cfif>
				
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="rename_revendeur" access="remote" returntype="numeric" output="false" hint="Renommer un revendeur." >
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="Libelle du revendeur" />
		<cfargument name="idrevendeur" required="false" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer" />
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.rename_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		<cfreturn p_result>		
	</cffunction>	
	
	<cffunction name="edit_revendeur" access="remote" returntype="numeric" output="false" hint="Renommer un revendeur." >
		<cfargument name="idrevendeur" required="true" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer" />
		<cfargument name="libelle" required="true" type="string" default="" displayname="string libelle" hint="Libelle du revendeur" />
		<cfargument name="Idracine" required="true" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="segment_fixe" required="true" type="numeric" default="0"/> 
		<cfargument name="segment_mobile" required="true" type="numeric" default="0"/>		
		<cfargument name="idcde_contact_societe" required="true" type="numeric" default="0"/>
		<cfargument name="idDevise" required="true" type="numeric" default="0"/>

		<cfif Idracine NEQ 0>
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.edit_revendeur_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" value="#idrevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle" value="#libelle#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#Idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" value="#segment_fixe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" value="#segment_mobile#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcde_contact_societe" value="#idcde_contact_societe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_livraison" value="0">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_fpc" null="yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_iddevise" value="#idDevise#">			
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>	
			<cfelse>
			<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.edit_revendeur_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" value="#idrevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle" value="#libelle#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" null="true">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" value="#segment_fixe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" value="#segment_mobile#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcde_contact_societe" value="#idcde_contact_societe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_livraison" value="0">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_fpc" null="yes">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_iddevise" value="#idDevise#">			
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
			</cfstoredproc>	
		</cfif>
		
		<cfreturn p_result>		
	</cffunction>
	
	
	
	
	<cffunction name="delete_revendeur" access="remote" returntype="numeric" output="false" hint="Supprimer un revendeur, ses contacts et sa fiche.Attention aux codes retour de la procedure stockée." >
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.delete_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="get_all_revendeurs" access="remote" returntype="query" output="false" hint="Lister lesconstructeurs, revendeurs, generiques et privés pour une racine donnée." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
		<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_all_revendeurs">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>
	
	<cffunction name="get_revendeurs_nonaffectes" access="remote" returntype="query" output="false" hint="Lister les constructeurs, revendeurs, génériques et privés non affectés au catalogue fournisseur d'une racine donnée." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
		<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_revendeurs_nonaffectes">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>	
	
	<cffunction name="get_catalogue_revendeur" access="remote" returntype="query" output="false" hint="Lister revendeurs, génériques et privés pour une racine donnée. (Catalogue fournisseur)." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
		<cfargument name="app_loginID" required="false" type="numeric" default="" displayname="numeric app_loginID" hint="paramNotes" />
		<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m16.get_catalogue_revendeur_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#app_loginID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>
	
	<cffunction name="getListeRevendeurGenerique" access="remote" returntype="query" output="false" hint="Lister revendeurs." >
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.Get_catalogue_revendeur_Gene">			
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>

	<cffunction name="affecter_revendeur" access="remote" returntype="numeric" output="false" hint="Ajouter un enregistrement au catalogue des revendeurs d'une racine (table CATALOGUE_REVENDEUR)." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.affecter_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="create_revendeurEtSociete" access="remote" returntype="numeric" output="false" hint="Ajouter un enregistrement au catalogue des revendeurs d'une racine (table CATALOGUE_REVENDEUR)." >
	
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="libelle de la societe du revendeur" />
		<cfargument name="idracine" required="false" type="numeric" default="" displayname="numeric idracine" hint="idgroupe_client de la racine du perimetre sur laquelle on se trouve" />
		<cfargument name="segment_mobile" required="false" type="numeric" default="" displayname="numeric segment_mobile" hint="1 si couvre le segment mobile, 0 sinon" />
		<cfargument name="segment_fixe_data" required="false" type="numeric" default="" displayname="numeric segment_fixe_data" hint="1 séil couvre le segment fixe/data, 0 sinon" />
		
		<cfargument name="Raison_sociale" required="false" type="string" default="" displayname="string Raison_sociale" hint="Initial value for the Raison_socialeproperty." />
		<cfargument name="SIREN" required="false" type="string" default="" displayname="string SIREN" hint="Initial value for the SIRENproperty." />
		<cfargument name="ADRESSE1" required="false" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="false" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="false" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="false" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="false" type="numeric" default="1577" displayname="numeric PAYSCONSOTELID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDDEVISE" required="false" type="numeric" default="0" displayname="numeric IDDEVISE" hint="Initial value for the IDDEVISEproperty"/>
		<cfargument name="TELEPHONE" required="false" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="false" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="APP_LOGINID" required="false" type="numeric" default="" displayname="numeric APP_LOGINID" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="CODE_INTERNE" required="false" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="ID_SOCIETE_MERE" required="false" type="numeric" null="true" default="" displayname="numeric ID_SOCIETE_MERE" hint="Initial value for the ID_SOCIETE_MEREproperty." />
		
		<cfset revendeurId = -1>
		<cfset societeId = -1>
		<cfset retourEditRevendeur = -1>
		
		<!--- creation du revendeur --->		
		<cfset revendeurId = create_revendeur(libelle, idracine, segment_mobile, segment_fixe_data, IDDEVISE)>		
		<cfif revendeurId gt 0>
			<!--- creation de l'agence --->	
			<cfset societeId = createObject("component","fr.consotel.consoview.inventaire.equipement.revendeurs.FicheSociete").create_cde_contact_societe(idracine, Raison_sociale, SIREN, ADRESSE1, ADRESSE2, COMMUNE, ZIPCODE, PAYSCONSOTELID, TELEPHONE, FAX, APP_LOGINID, CODE_INTERNE, ID_SOCIETE_MERE)>
			<cfif societeId gt 0>
					<!--- maj de revendeur--->	
					<cfset retourEditRevendeur = edit_revendeur(revendeurId, libelle, idracine, segment_mobile, segment_fixe_data, societeId, IDDEVISE)>
					<cfif retourEditRevendeur lt 1>
						<cfreturn -3>	
					</cfif>	
			<cfelse>
				<cfreturn -2> 
			</cfif>				
		<cfelse>
			<cfreturn -1>
		</cfif>
		
		
		<cfreturn revendeurId>			
	</cffunction>
	
	<cffunction name="edit_revendeurEtSociete" access="remote" returntype="numeric" output="false" hint="Ajouter un enregistrement au catalogue des revendeurs d'une racine (table CATALOGUE_REVENDEUR)." >
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="libelle de la societe du revendeur" />
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfargument name="idracine" required="false" type="numeric" default="" displayname="numeric idracine" hint="idgroupe_client de la racine du perimetre sur laquelle on se trouve" />		
		
		<cfargument name="Idcde_contact_societe" required="false" type="numeric" default="" displayname="numeric Idcde_contact_societe" hint="Initial value for the Idcde_contact_societeproperty." />
		<cfargument name="SIREN" required="false" type="string" default="" displayname="string SIREN" hint="Initial value for the SIRENproperty." />
		<cfargument name="ADRESSE1" required="false" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="false" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="false" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="false" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="false" type="numeric" default="1577" displayname="numeric PAYSCONSOTELID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="TELEPHONE" required="false" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="false" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="USER_CREATE" required="false" type="numeric" default="" displayname="numeric USER_CREATE" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="CODE_INTERNE" required="false" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="ID_SOCIETE_MERE" required="false" type="numeric" default="" displayname="numeric ID_SOCIETE_MERE" hint="Initial value for the ID_SOCIETE_MEREproperty." />
		<cfargument name="IDDEVISE" required="false" type="numeric" default="0" displayname="numeric IDDEVISE" hint="Initial value for the IDDEVISEproperty"/>
		
		<cfset revendeurId = -1>
		<cfset revendeurId = rename_revendeur(libelle, Idrevendeur, idracine)>
		
		<cfif revendeurId gt 0>
			<cfset societeId = createObject("component","fr.consotel.consoview.inventaire.equipement.revendeurs.FicheSociete").edit_cde_contact_societe(Idcde_contact_societe, libelle, SIREN, ADRESSE1, ADRESSE2, COMMUNE, ZIPCODE, PAYSCONSOTELID, TELEPHONE, FAX, USER_CREATE, CODE_INTERNE, ID_SOCIETE_MERE, IDDEVISE)>
			
			<cfif societeId lt 0>
				<cfreturn -2> 
			</cfif>				
			
		<cfelse>
			<cfreturn -1>
		</cfif>	

		<cfreturn revendeurId>
		
	</cffunction>
	
	<!--- Edite les différentes propriétés d'un revendeur concernant différents delais qui le ie avec un opérateur  --->
	<cffunction name="editRevendeurSLA" access="remote" returntype="numeric" output="false" hint="Editer le SLA d'un revendeur." >
		<cfargument name="idrevendeur" required="true" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer" />
		<cfargument name="idOperateur" required="true" type="numeric" default="" displayname="id de l'opérateur" hint="Id de l'opérateur'" />
		<cfargument name="idApplogin" required="true" type="numeric" default="" displayname="numeric identifiants de l'utilisateur éditant" hint="" />
		<cfargument name="delai_livraison" required="true" type="numeric" default="2"/> 
		<cfargument name="delai_hour_livraison" required="true" type="string" default="16.00"/>		
		<cfargument name="delai_option" required="true" type="numeric" default="2"/> 
		<cfargument name="delai_hour_option" required="true" type="string" default="16.00"/>		
		<cfargument name="delai_resiliation" required="true" type="numeric" default="2"/> 
		<cfargument name="delai_hour_resiliation" required="true" type="string" default="16.00"/>		
		<cfargument name="delai_suppression" required="true" type="numeric" default="2"/> 
		<cfargument name="delai_hour_suppression" required="true" type="string" default="16.00"/>		
		<cfargument name="delai_reactivation" required="true" type="numeric" default="2"/> 
		<cfargument name="delai_hour_reactivation" required="true" type="string" default="16.00"/>		
		
				
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.Edit_Revendeur_SLA_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 				value="#idrevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idOperateur" 				value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idApplogin" 				value="#idApplogin#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_livraison" 			value="#delai_livraison#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_option" 			value="#delai_option#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_resiliation" 		value="#delai_resiliation#">				
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_suppression" 		value="#delai_suppression#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_delai_reactivation" 		value="#delai_reactivation#">					
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_delai_hour_livraison" 	value="#delai_hour_livraison#">							
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_delai_hour_option" 		value="#delai_hour_option#">						
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_delai_hour_resiliation" 	value="#delai_hour_resiliation#">					
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_delai_hour_suppression" 	value="#delai_hour_suppression#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_delai_hour_reactivation" 	value="#delai_hour_reactivation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 				value="#Session.PERIMETRE.ID_GROUPE#">					
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>			
		<cfreturn p_result>		
	</cffunction>
	
	<!--- Récupère les différentes propriétés d'un revendeur concernant différents delais  --->
	<cffunction name="getRevendeurSLA" access="remote" returntype="query" output="false" hint="obtenir les sla d'un revendeur" >
		<cfargument name="idrevendeur" required="true" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer" />
		<cfargument name="idOperateur" required="true" type="numeric" default="" displayname="numeric idOperateur" hint="ID de l'operateur" />				
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m16.get_Revendeur_SLA_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" 	value="#idrevendeur#">						
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idOperateur" 	value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#Session.PERIMETRE.ID_GROUPE#">					
			<cfprocresult name="p_retour">
		</cfstoredproc>			
		<cfreturn p_retour>		
	</cffunction>
	
	<cffunction name="desaffecter_revendeur" access="remote" returntype="numeric" output="false" hint="Supprime un enregistrement de la table CATALOGUE_REVENDEUR." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.desaffecter_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>		
		<cfreturn p_result>			
	</cffunction>	
	
	
	<cffunction name="getContactsRevendeur" access="remote" returntype="query" output="false" hint="" >		
		<cfargument name="idrevendeur" required="true" type="numeric"/>
		<cfset user = Session.USER.CLIENTACCESSID>
		<cfset role = 101>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.GETCONTACTSROLESREVENDEUR_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idrevendeur#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#user#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#role#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#Session.PERIMETRE.ID_GROUPE#">					
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getContactsRevendeurWithRole" access="remote" returntype="query" output="false" hint="" >		
		<cfargument name="idrevendeur" required="true" type="numeric"/>
		<cfargument name="idRole" required="true" type="numeric"/><!--- Resiliation = 6 && Commande = 101 --->
		
			<cfset user = Session.USER.CLIENTACCESSID>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.GETCONTACTSROLESREVENDEUR_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idrevendeur#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#user#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idRole#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#Session.PERIMETRE.ID_GROUPE#">					
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="getDevises" access="remote" returntype="query" output="false" hint="Retourne la liste des devises possibles">
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getDevises">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="remote" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="remote" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>
