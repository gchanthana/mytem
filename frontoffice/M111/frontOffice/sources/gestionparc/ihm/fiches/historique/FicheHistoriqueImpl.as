package gestionparc.ihm.fiches.historique
{
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.historique.HistoriqueServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.DateField;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * Classe concernant toutes les informations de la fiche historique.
	 */
	[Bindable]
	public class FicheHistoriqueImpl extends TitleWindow
	{

		public var tiFiltre:TextInput;
		public var dgHisto:DataGrid;
		public var btFermer:Button;

		public var idRessource:int=0;
		public var idEmploye:int=0;
		public var idTerm:int=0;
		public var idSim:int=0;
		public var idSoustete:int=0;

		private var myServiceHistorique:HistoriqueServices=null;

		public function FicheHistoriqueImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Formate les données de la colonne avec le bon affichage des dates au format String.
		 */
		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
			{
				var dt:Date=item[column.dataField] as Date;
				return DateField.dateToString(dt, ResourceManager.getInstance().getString("M111", "DD_MM_YYYY") + ' ' + dt.hours + ':' + ((dt.minutes < 10) ? '0' : '') + dt.minutes + ':' + ((dt.seconds < 10) ? '0' : '') + dt.seconds);
			}
			else
			{
				return "";
			}
		}

		/**
		 * Initialisation générale de cette classe  :
		 * - Instanciation du service lié à l'historique.
		 */
		private function init(event:FlexEvent):void
		{
			myServiceHistorique=new HistoriqueServices();
			if (idRessource == 0)
			{
				myServiceHistorique.getHistorique(idEmploye, idTerm, idSim, idSoustete);
			}
			else
			{
				myServiceHistorique.getHistoriqueActionRessource(idRessource);
			}
			initListener();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			btFermer.addEventListener(MouseEvent.CLICK, cancelPopup);
			tiFiltre.addEventListener(Event.CHANGE, filtreHandler);
			myServiceHistorique.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_HISTO_LOADED, infoFicheHistoriqueLoadedHandler);
		}

		/**
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 */
		private function infoFicheHistoriqueLoadedHandler(event:gestionparcEvent):void
		{
			dgHisto.dataProvider=myServiceHistorique.myDatas.listHistorique;
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Appel la fonction "filtrer" si il y a des données dans le datagrid.
		 */
		private function filtreHandler(event:Event):void
		{
			if (dgHisto.dataProvider != null)
			{
				(dgHisto.dataProvider as ArrayCollection).filterFunction=filtrer;
				(dgHisto.dataProvider as ArrayCollection).refresh();
			}
		}

		/**
		 * Ffiltre les données dans le datagrid.
		 */
		private function filtrer(item:Object):Boolean
		{
			if ((String(DateFunction.formatDateAsString(item.dateEffet)).toLowerCase().search(tiFiltre.text) != -1) 
				|| (String(item.description).toLowerCase().search(tiFiltre.text) != -1) 
				|| (String(item.qui).toLowerCase().search(tiFiltre.text) != -1) 
				|| (String(DateFunction.formatDateAsString(item.quand)).toLowerCase().search(tiFiltre.text) != -1))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
