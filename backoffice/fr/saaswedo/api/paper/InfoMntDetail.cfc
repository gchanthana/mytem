<cfcomponent output="false">
	
	<cffunction access="public" name="getInfoMntDetail" returntype="query">
		
		<cfargument name="idmnt_detail" type="numeric" required="true">

		<cfstoredproc datasource="ROCOFFRE" procedure="mnt.PKG_MNT_LOG.infos_mnt_detail">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#idmnt_detail#" variable="p_idmnt_detail">
			<cfprocresult name="qinfo_dtail">
		</cfstoredproc>				
		
        <cfreturn qinfo_dtail>
		
	</cffunction>

</cfcomponent>