package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	
	public class HasEqpFixeAssociated implements IRuleDisplayItem
	{
		public function HasEqpFixeAssociated()
		{
		}
		
		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO = SpecificationVO.getInstance().elementDataGridSelected;
			if( ligne
				&& ligne.IDSIM != null
				&& ligne.IDSIM > 0)
				return true
			else
				return false
		}
	}
}