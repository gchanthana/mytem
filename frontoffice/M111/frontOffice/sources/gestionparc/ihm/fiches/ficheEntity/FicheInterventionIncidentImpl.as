package gestionparc.ihm.fiches.ficheEntity
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.NumberValidator;
	import mx.validators.Validator;
	
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import gestionparc.entity.IncidentVO;
	import gestionparc.entity.InterventionVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.incident.IncidentServices;
	import gestionparc.utils.gestionparcMessages;

	/**
	 * Classe concernant toutes les informations d'une intervention lié un incident.
	 */
	[Bindable]
	public class FicheInterventionIncidentImpl extends TitleWindow
	{

		public var lbNumIntervention:Label;
		public var lbEnregistrePar:Label;
		public var tiLibelle:TextInput;
		public var tiPrix:TextInput;
		public var cbTypeIntervention:ComboBox;
		public var cbHoraire:ComboBox;

		public var rbOui:RadioButton;
		public var rbNon:RadioButton;

		public var taDescription:TextArea;
		public var dcIntervention:CvDateChooser;

		public var btValider:Button;
		public var btAnnuler:Button;

		public var valDateIntervention:Validator;
		public var valTypeIntervention:Validator;
		public var valPrix:NumberValidator;

		public var isAddIntervention:Boolean=false;
		public var myIntervention:InterventionVO=null;
		public var myIncident:IncidentVO=null;
		public var myServiceIncident:IncidentServices=null;
		public var myServiceCache:CacheServices=null;

		public function FicheInterventionIncidentImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialisation générale de cette classe  :
		 * - Instanciation du service lié aux caches si c'est le premier appel, sinon pas d'appel de procédure.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 */
		private function init(event:FlexEvent):void
		{
			myServiceCache=CacheServices.getInstance();
			if (!CacheDatas.listeTypeIntervention)
			{
				myServiceCache.getListeTypeIntervention();
			}

			initListener();
			initDisplay();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validClickHandler);

			// listeners permettant l'activation du bouton "valider"
			dcIntervention.addEventListener(CalendarLayoutChangeEvent.CHANGE, activeValidButton);

			tiLibelle.addEventListener(Event.CHANGE, activeValidButton);
			tiPrix.addEventListener(Event.CHANGE, activeValidButton);

			taDescription.addEventListener(Event.CHANGE, activeValidButton);

			cbHoraire.addEventListener(Event.CHANGE, activeValidButton);
			cbTypeIntervention.addEventListener(Event.CHANGE, activeValidButton);

			rbNon.addEventListener(Event.CHANGE, activeValidButton);
			rbOui.addEventListener(Event.CHANGE, activeValidButton);

			// listeners liés aux services
			if (myServiceCache)
			{
				myServiceCache.myDatas.addEventListener(gestionparcEvent.LISTE_INTERVENTION_LOADED, interventionLoadedHandler);
			}
		}

		/**
		 * Initialise l'affichage de cette IHM.
		 */
		private function initDisplay():void
		{
			cbTypeIntervention.dataProvider=CacheDatas.listeTypeIntervention;
			rbNon.selected=true;

			if (isAddIntervention) // Creation
			{
				dcIntervention.selectedDate=new Date();
				btValider.enabled=true;
			}
			else // Edition
			{
				lbNumIntervention.text=myIntervention.numIntervention;
				dcIntervention.selectedDate=myIntervention.dateIntervention;
				tiLibelle.text=myIntervention.libelle;
				lbEnregistrePar.text=myIntervention.savedByUser;
				tiPrix.text=myIntervention.prix.toString();
				taDescription.text=myIntervention.description;
				rbNon.selected=(myIntervention.isClose == 1) ? false : true;
				rbOui.selected=(myIntervention.isClose == 1) ? true : false;
				cbHoraire.selectedIndex=myIntervention.horaire - 1;

				// type intervention
				if (CacheDatas.listeTypeIntervention)
				{
					var lenListeIntervention:int=CacheDatas.listeTypeIntervention.length;
					for (var i:int=0; i < lenListeIntervention; i++)
					{
						if (CacheDatas.listeTypeIntervention[i].IDTYPE_INTERVENTION == myIntervention.idTypeIntervention)
						{
							cbTypeIntervention.selectedIndex=i;
							break;
						}
					}
				}

				btValider.enabled=false;
			}
		}

		/**
		 * Active le bouton "Valider".
		 */
		private function activeValidButton(event:Event):void
		{
			btValider.enabled=true;
		}

		/**
		 * Met à jour les données d'une intervention concernant un incident sur un équipement.
		 */
		private function setInfoInterventionVO():void
		{
			myIntervention.dateIntervention=dcIntervention.selectedDate;
			myIntervention.typeIntervention=cbTypeIntervention.selectedItem.TYPE_INTERVENTION;
			myIntervention.idTypeIntervention=cbTypeIntervention.selectedItem.IDTYPE_INTERVENTION;
			myIntervention.libelle=tiLibelle.text;
			myIntervention.horaire=cbHoraire.selectedIndex + 1;
			myIntervention.prix=parseFloat(tiPrix.text);
			myIntervention.description=taDescription.text;
			myIntervention.isClose=(rbOui.selected) ? 1 : 0;
		}

		/**
		 * Valide la fiche intervention en mettant à jour les données de l'intervention ou en créeant une intervention.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			if (runValidators())
			{
				setInfoInterventionVO();
				myServiceIncident.setInfosIntervention(myIntervention, myIncident);
				myServiceIncident.myDatas.addEventListener(gestionparcEvent.INFOS_INTERVENTION_SAVED, infosInterventionSavedHandler);
			}
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant l'intervention d'un incident de l'équipement ont été mis à jour.
		 */
		private function infosInterventionSavedHandler(evt:gestionparcEvent):void
		{
			myIntervention.idIntervention=(evt.obj.idIntervention as int);
			myIntervention.numIntervention=(evt.obj.numIntervention as String);
			myIntervention.savedByUser=CvAccessManager.getSession().USER.NOM + " " + CvAccessManager.getSession().USER.PRENOM;

			myServiceIncident.myDatas.removeEventListener(gestionparcEvent.INFOS_INTERVENTION_SAVED, infosInterventionSavedHandler);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.UPDATE_LISTE_INTERVENTION, myIntervention));

			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			closePopup(null);
		}

		/**
		 * Charge les données revenues de la base par rapport à la liste des types d'interventions.
		 */
		private function interventionLoadedHandler(event:gestionparcEvent):void
		{
			cbTypeIntervention.dataProvider=CacheDatas.listeTypeIntervention;

			var lenListeIntervention:int=CacheDatas.listeTypeIntervention.length;
			for (var i:int=0; i < lenListeIntervention; i++)
			{
				if (myIntervention && CacheDatas.listeTypeIntervention[i].IDTYPE_INTERVENTION == myIntervention.idTypeIntervention)
				{
					cbTypeIntervention.selectedIndex=i;
					break;
				}
			}
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}


		/**
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 */
		private function runValidators():Boolean
		{
			var validators:Array=null;
			validators=[valDateIntervention, valTypeIntervention];
			var results:Array=Validator.validateAll(validators);

			var prixValidator:Array=[valPrix];
			var resultPrix:Array=Validator.validateAll(prixValidator);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
			{
				if (tiPrix.text != "")
				{
					if (resultPrix.length > 0)
					{
						ConsoviewAlert.afficherAlertInfo(gestionparcMessages.SYNTAXE_INVALIDE, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
						return false;
					}
				}
				return true;
			}
		}
	}
}