<cfcomponent name="PublisherService" alias="fr.consotel.consoview.bi.service.PublisherService">
	<!--- BI Server Host --->
	<cfset biServer="NONE">
	<!--- BI Publisher Web Service Object --->
	<cfset biService="">
	<!--- Login Profile --->
	<cfset loginProfile="">
	<cfset userId="">
	<cfset userAuth="">
	<!--- Base Config Parameter values for scheduling --->
	<cfset schedulerBaseConfig="NONE">
	<!--- Notify when Success, Warning, Error --->
	<cfset notifyWhen=TRUE>
	<!--- Save of not Data (XML) --->
	<cfset saveOption=TRUE>
	
	<cffunction name="initService" access="private" returntype="numeric">
		<cfargument name="initMode" type="string" required="true">
		<cfargument name="notifyFlag" type="numeric" required="false" default="0">
		<cfargument name="saveFlag" type="numeric" required="false" default="0">
		<cfif schedulerBaseConfig EQ "NONE">
			<cfset schedulerBaseConfig=structNew()>
			<cfset schedulerBaseConfig["jobLocale"]="fr-FR">
			<cfset schedulerBaseConfig["jobTZ"]="fr-FR">
			<cfif notifyFlag EQ 0>
				<cfset notifyWhen=FALSE>
			</cfif>
			<cfif saveFlag EQ 0>
				<cfset saveOption=FALSE>
			</cfif>
			<cfset schedulerBaseConfig["notifyWhenSuccess"]=notifyWhen>
			<cfset schedulerBaseConfig["notifyWhenWarning"]=notifyWhen>
			<cfset schedulerBaseConfig["notifyWhenFailed"]=notifyWhen>
			<cfset schedulerBaseConfig["saveDataOption"]=saveOption>
			<cfset schedulerBaseConfig["saveOutputOption"]=FALSE>
			<cfset schedulerBaseConfig["saveOutputOption"]=FALSE>
			<cfset schedulerBaseConfig["notificationTo"]="cedric.rapiera@consotel.fr">
		</cfif>
		<cfreturn 1>
	</cffunction>

	<cffunction name="authService" access="public" returntype="numeric">
		<cfargument name="loginKey" type="string" required="true">
		<cfif loginKey EQ "ConsoTel">
			<cfset userId="cvuser">
			<cfset userAuth="consotel09">
			<cfset biServer="sun-bi.consotel.fr">
			<cfset biService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
			<cfif biService.validateLogin(userId,userAuth) EQ TRUE>
				<cfset loginProfile=loginKey>
				<cfset initResult=initService("PROD")>
				<cfif initResult LT 0>
					<cfreturn -3>
				</cfif>
				<cfreturn 1>
			<cfelse>
				<cfreturn -1>
			</cfif>
		<cfelse>
			<cfreturn -2>
		</cfif>
	</cffunction>
	
	<cffunction name="scheduleService" access="public" returntype="numeric">
		<cfargument name="jobName" type="string" required="true">
		<cfargument name="reportParameters" type="struct" required="true">
		<cfargument name="burstOption" required="false" type="boolean" default="FALSE">
		<cfif loginProfile EQ "ConsoTel">
			<!--- First checking if user has access to related report --->
			<cfif loginProfile EQ "ConsoTel">
				<cfset reportAccess=biService.hasReportAccess(reportParameters["REPORT_PATH"],userId,userAuth)>
				<cfif reportAccess EQ TRUE>
					<!--- *************** Report Request Structure *************** --->
					<cfset relatedReportRequest=structNew()>
					<cfset relatedReportRequest["attributeLocale"]="fr-FR">
					<cfset relatedReportRequest["reportAbsolutePath"]=reportParameters["REPORT_PATH"]>
					<cfset relatedReportRequest["attributeFormat"]=reportParameters["FORMAT"]>
					<cfset relatedReportRequest["attributeTemplate"]=reportParameters["TEMPLATE"]>
					<cfset relatedReportRequest["parameterNameValues"]=reportParameters["PARAMETERS"]>
					<!--- ********************************************************* --->
					<!--- *************** Delivery Request Structure (v1 : Only Local Delivery Option) *************** --->
					<cfset relatedDeliveryRequest=structNew()>
					<cfset relatedDeliveryRequest["localOption"]=structNew()>
					<cfset relatedDeliveryRequest["localOption"]["destination"]=reportParameters["DELIVERY_FILE_PATH"]>
					<!--- ********************************************************* --->
					<cfset relatedScheduleRequest=DUPLICATE(schedulerBaseConfig)>
					<cfset relatedScheduleRequest["scheduleBurstringOption"]=burstOption>
					<cfset relatedScheduleRequest["reportRequest"]=relatedReportRequest>
					<cfset relatedScheduleRequest["deliveryRequest"]=relatedDeliveryRequest>
					<cfset relatedScheduleRequest["userJobName"]="CFMX-" & jobName>
					<cfdump var="#relatedScheduleRequest#" label="RELATED SCHEDULE REQUEST">
					<cfset schedulerResult=biService.scheduleReport(relatedScheduleRequest,userId,userAuth)>
					<cfreturn schedulerResult>
				<cfelse>
					<cfabort showerror="PublisherService.scheduleService() - REPORT ACCESS : #reportAccess#">
					<cfreturn -1>
				</cfif>
			</cfif>
		</cfif>
		<cfabort showerror="PublisherService.scheduleService() - LOGIN PROFILE">
		<cfreturn -2>
	</cffunction>
	
	<cffunction name="getScheduledReportInfos" access="public" returntype="struct">
		<cfargument name="jobId" type="numeric" required="true">
		<cfif loginProfile EQ "ConsoTel">
			<cfset tmpScheduleInfos=biService.getScheduledReportInfo(jobId,userId,userAuth,"My History")>
			<cfset tmpScheduleInfos=tmpScheduleInfos[1]>
			<cfset tmpHistoryInfos=biService.getScheduledReportHistoryInfo(jobId,userId,userAuth,"My History",FALSE)>
			<cfset tmpHistoryInfos=tmpHistoryInfos[1]>
			<cfset scheduleInfos=structNew()>
			<cfset scheduleInfos["JOB_ID"]=jobId>
			<!--- ***************** SCHEDULE INFOS ***************** --->
			<cfset scheduleInfos["SCHEDULE"]=structNew()>
			<cfset structInsert(scheduleInfos["SCHEDULE"],"JOB_NAME",tmpScheduleInfos.getJobName())>
			<cfset structInsert(scheduleInfos["SCHEDULE"],"JOB_GROUP",tmpScheduleInfos.getJobGroup())>
			<cfset structInsert(scheduleInfos["SCHEDULE"],"REPORT_NAME",tmpScheduleInfos.getReportName())>
			<!--- 
			<cfset structInsert(scheduleInfos["SCHEDULE"],"LOCAL_DESTINATION",
								tmpScheduleInfos.getDeliveryParameters().getLocalOption().getDestination())>
			 --->
			<cfset structInsert(scheduleInfos["SCHEDULE"],"CREATED_DATE",tmpScheduleInfos.getCreatedDate().getTime())>
			<cfset structInsert(scheduleInfos["SCHEDULE"],"START_DATE",tmpScheduleInfos.getStartDate().getTime())>
			<cfset structInsert(scheduleInfos["SCHEDULE"],"RUN_TYPE",tmpScheduleInfos.getRunType())>
			<cfset structInsert(scheduleInfos["SCHEDULE"],"USERNAME",tmpScheduleInfos.getUsername())>
			<cfset structInsert(scheduleInfos["SCHEDULE"],"IS_BURSTING",tmpScheduleInfos.isBurstingOption())>
			<!--- ************************************************** --->
			<!--- ***************** HISTORY INFOS ***************** --->
			<cfset scheduleInfos["HISTORY"]=structNew()>
			<cfset structInsert(scheduleInfos["HISTORY"],"JOB_STATUS",tmpHistoryInfos.getStatus())>
			<cfset structInsert(scheduleInfos["HISTORY"],"JOB_MESSAGE",tmpHistoryInfos.getJobMessage())>
			<cfset structInsert(scheduleInfos["HISTORY"],"CREATED_DATE",tmpHistoryInfos.getCreatedDate().getTime())>
			<cfset structInsert(scheduleInfos["HISTORY"],"OUTPUT_CONTENT_TYPE",tmpHistoryInfos.getDocumentDataContentType())>
			<!--- ************************************************** --->
			<cfset tmpScheduleInfos=''>
			<cfset tmpHistoryInfos=''>
			<cfreturn scheduleInfos>
		</cfif>
		<cfreturn structNew()>
	</cffunction>
</cfcomponent>
