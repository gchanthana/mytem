package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import univers.parametres.parametrecompte.event.GestionCollaborateursEvent;

	public class GestionCollaborateursModel extends EventDispatcher
	{
		
		private var _type:Number;
		
		public function GestionCollaborateursModel()
		{
		}
		
		public function get type():Number
		{
			return _type;
		}

		public function set type(value:Number):void
		{
			_type = value;
		}

		internal function updateRacineGestCollab(value:ArrayCollection):void
		{
			type = value.getItemAt(0).GESTION_COLLABORATEUR;
			dispatchEvent(new GestionCollaborateursEvent(GestionCollaborateursEvent.GESTIONCOLLABORATEURS_INIT));
		}
	}
}