<?xml version="1.0" encoding="iso-8859-1" ?><!DOCTYPE html PUBLIC
	"-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
	"http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head><title>TELLCOST</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="robots" content="all" />
<meta name="classification" content="Services" />
<meta name="copyright" content="ConsoTel SAS" />
<meta name="author" content="Guillaume RAMOS" />
<meta name="language" content="fr" />
<meta name="description" content="TELLCOST" />
<meta name="keywords" content="TELLCOST" />
<link rel="stylesheet" type="text/css" href="css/style.css" />

		<STYLE TYPE="TEXT/CSS">

			.mybouton { 
			width: 150px;} 

			.submit {
			border: solid 1px #000000;
			border-radius:5PX;
		 	}
		 	
		 	#CADRECENTRALE2 {
		 	width:80%;
			BORDER-LEFT:1PX SOLID #FFFFFF; 
			BORDER-RIGHT:1PX SOLID #FFFFFF;
			BORDER-BOTTOM:1PX SOLID #FFFFFF;
			BORDER-TOP:1PX SOLID #FFFFFF;
			PADDING:10PX;
			}

		</style>

</head>

<!--- Variables --->

	<cfset ValidateLog = -1>
	<cfset MessageLogError = "">
	
<!--- Fin variables --->

	<cfset obj = createobject('component','TellCostService')>
		
<!--- Session --->

	<cfset SESSION.IS_CONNECTED = 0>
	<cfset SESSION.SESSION_ID = -1>
	<cfset SESSION.PHONENUMBER = "">
	<cfset SESSION.IDSOUS_TETE = "">
	<cfset SESSION.IDRACINE_MASTER = "">
	<cfset SESSION.IDPERIODE = "">
	<cfset SESSION.IDTHEME = "">
	<cfset SESSION.LIBELLE_MOIS = "">
	<cfset SESSION.PATH_CONNECT = "home.cfm?"&"#SESSION.URLTOKEN#">

<!--- Fin session --->

<!--- Function --->

	<cffunction name="zCheckValidateLog" access="private" returntype="numeric">
			
			<cfset objectValidate = obj.zQueryCheckLog(URL._Login,URL._Password,SESSION)>
			<cfset ValidateLog = objectValidate.RETOUR>
			<cfset rslt = -1>
			
			<cfif ValidateLog GT 0>
				<cfset MessageLogError = "">
				<cfset SESSION = objectValidate>
				<cfset SESSION.IS_CONNECTED = 1>
				<cfset SESSION.SESSION_ID = ValidateLog>
				<cfset SESSION.PHONENUMBER = URL._Login>
				<cfset rslt = 1>
			<cfelseif ValidateLog EQ -4>
				<cfset MessageLogError = "Numéro ou mot de passe invalide">
				<cfset SESSION.IS_CONNECTED = 0>
				<cfset SESSION.SESSION_ID = -1>
				<cfset rslt = -1>
			<cfelseif ValidateLog EQ 0>
				<cfset MessageLogError = "Numéro ou mot de passe invalide">
				<cfset SESSION.IS_CONNECTED = 0>
				<cfset SESSION.SESSION_ID = -1>
				<cfset rslt = -1>
			</cfif> 

 		<cfreturn rslt> 
	</cffunction> 

<!--- Fin function --->

<!--- Procédure --->

	
<!--- Fin procédure --->

<!--- Tests --->

	<cfif structKeyExists(URL,'CONNEXION')>
		<cfif (URL._Login NEQ "" AND URL._Password NEQ "")>
			<cfif zCheckValidateLog() EQ 1>
				<cflocation url="#SESSION.PATH_CONNECT#">
			</cfif>
		</cfif>
	</cfif>

<!--- Fin tests --->

	<FONT face="Arial">
		<body text="#FFFFFF">
			<div align="center">
			<br/><br/>
				
				<DIV ID="CADRECENTRALE2">	    
					<img src="img/key.png" width="64" height="64" alt="tellcost" />	
			    
				<br/>
				
					<form name="myform" method="GET" style="TEXT-ALIGN:RIGHT">
				    	<input type="hidden" name="" value="" />
						<input type="hidden" name="mode" value="2" />
						<cfoutput>Mobile : </cfoutput>
						<input type="texte" name="_Login" class="mybouton" />
						<br />
						<cfoutput>Code : </cfoutput>
				        <input type="password" name="_Password" class="mybouton"/>
						<br />
						<input type="submit" name="CONNEXION" value="OK" class="submit"/>
					</form>
				</DIV>
			</div>
		</body>
	</FONT>
</html>