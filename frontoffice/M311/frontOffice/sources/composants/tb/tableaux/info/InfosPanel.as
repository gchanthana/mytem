package composants.tb.tableaux.info
{
	import composants.util.DateFunction;
	
	import flash.utils.describeType;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class InfosPanel
	{
		private var _infos : InfosPanelVo;
		public function set infos(infos : InfosPanelVo):void{
			_infos = infos;
		}
		public function get infos():InfosPanelVo{
			return _infos;
		}		
		
		
		
		public function getInfosLigne(idSous_tete : Number):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getInfosPanelLigne",
																				getInfosLigneResultHandler);
			RemoteObjectUtil.callService(op,idSous_tete);
			
		}
		
		private function getInfosLigneResultHandler(re : ResultEvent):void{
			if (re.result){				
				if ((re.result as ArrayCollection).length > 0){								
					doMapping(re.result[0],infos = new InfosPanelVo());
				}
			}	
		}
		
		protected function clearInfos():void{
			infos  =  null;
		}
				
		private function doMapping(src : Object, dest : Object):void{
			var classInfo : XML = describeType(dest);
			 
            for each (var v:XML in classInfo..accessor) {
               if (src.hasOwnProperty(v.@name) && src[v.@name] != null){               		
               		dest[v.@name] = src[v.@name];
               }else{
               	trace("-----> echec mapping ["+v.@name+"] ou valeur null" );
               }
            }            				 		
		}
	}
}