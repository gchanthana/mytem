package univers.inventaire.inventaire
{
	import mx.containers.Box;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	
	import univers.AbstractConsoViewFunction;
	import univers.inventaire.inventaire.journaux.AfficherJournalEvent;
	import univers.inventaire.inventaire.journaux.JournauxMain;
	import univers.inventaire.inventaire.menu.JournauxMenu;

	public class FunctionJournauxImpl extends AbstractConsoViewFunction
	{
		public var myJournauxMenu : JournauxMenu;
		public var myMainBox : Box;
		
		//Reference vers l ecran contenant les Journeaux
		protected var myMainJournaux:JournauxMain = new JournauxMain();
		
		public function FunctionJournauxImpl()
		{	
			super();
		}
		
		override protected function commitProperties():void{
			/*-- Journaux ---*/
			myJournauxMenu.addEventListener(AfficherJournalEvent.AFFICHER_JOURAUX,displayMenuPanel);			
		}
		
		override protected  function afterCreationComplete(event:FlexEvent):void {
			super.afterCreationComplete(event);
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
		}
		
		override public  function getUniversKey():String {
			return "GEST";
		}
		
		override public  function getFunctionKey():String {
			return "GEST_REPORT";
		}
		
		override public  function afterPerimetreUpdated():void {
			 myJournauxMenu.onPerimetreChange();
			 myMainJournaux.onPerimetreChange();
			 
			 	 
			trace("(FunctionListeAppels) Perform After Perimetre Updated Tasks");
			trace("(FunctionListeAppels) SESSION FLEX :\n" +
						ObjectUtil.toString(CvAccessManager.getSession()));
		}
		
		
		
		
		//Affiche le journal suivant le type et eventuellement l'id du noeud passés en parametre
		//via l'evenement AfficherJournalEvent
		private function displayMenuPanel(aje : AfficherJournalEvent):void{			
			if (myMainBox.getChildren().length > 0){
				myMainJournaux.afficherJournal(aje.typeJournal,aje.nodeInfos);				
			}else{
				myMainBox.addChild(myMainJournaux);
				myMainJournaux.afficherJournal(aje.typeJournal,aje.nodeInfos);	
			} 
		}
				
		
	}
}