﻿﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>

	
	<!--- ASSOCIATION COLLABORATEUR --->
		<!--- EQUIPEMENT --->
	<cffunction name="associerFournirEqp1" access="remote" returntype="any">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.asscoierTermCollab">
					<cfprocparam  value="#IDORIGINE#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#IDDESTINATION#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="associerFournirEqp2" access="remote" returntype="any">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.asscoierTermCollab">
					<cfprocparam  value="#IDORIGINE#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#IDDESTINATION#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="associerFournirEqp3" access="remote" returntype="any">
		
		<cfargument name="IDORIGINE" 		required="true" type="Numeric" />
		<cfargument name="IDDESTINATION"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierTermCollab">
					<cfprocparam  value="#IDORIGINE#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#IDDESTINATION#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerFournirEqp4" access="remote" returntype="any">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierTermCollab">
					<cfprocparam  value="#IDORIGINE#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#IDDESTINATION#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="3" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	
		<!--- LIGNE SIM --->
	<cffunction name="associerFournirLigneSim1" access="remote" returntype="Any" >
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierSimCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>

		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerFournirLigneExt1" access="remote" returntype="Any" >
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierSimCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>

		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerFournirLigneSim2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierSimCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
		<cffunction name="associerFournirLigneExt2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"	required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierSimCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerFournirLigneSim3" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierSimCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerFournirLigneExt3" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"	required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierSimCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
		<!--- LIGNE --->
	<cffunction name="associerFournirLigne1" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierLigneCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerFournirLigne2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierLigneCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="associerFournirLigne3" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
								
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierLigneCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerFournirLigne4" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierLigneCollab">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="3" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		<cfreturn result>
	</cffunction>
	
	
	
	<!--- ASSOCIATION EQUIPEMENT --->
		<!--- COLLABORATEUR	 --->
	<cffunction name="associerCollabToEqp" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerCollabTerm">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(1,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
		<!--- LIGNE SIM  --->
	<cffunction name="associerLigneSimToEqp" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerSimTerm">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerLigneExtToEqp" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerSimTerm">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerExtToEquipementWithSim" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerExtTerm">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
		<!--- LIGNE --->
	<cffunction name="associerLigneToEqp" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerLigneTerm">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(7,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(7,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		
		<cflog text="action = #result#">
		
		
		<cfreturn result>
	</cffunction>
	
		<!--- SOUS EQUIPEMENT --->
	<cffunction name="associerSousEqpToEqp" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerSsEqptTerm">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(28,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(28,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- ASSOCIATION SIM --->
		<!--- COLLABORATEUR	 --->
	<cffunction name="associerCollabToSim1" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerCollabToExt1" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerCollabToSim2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
		<cffunction name="associerCollabToExt2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(3,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
		
		<!--- LIGNE --->
	<cffunction name="associerLigneToSim" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerLigneSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
		<cffunction name="associerLigneToExt" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerLigneSim">
					<cfprocparam  value="#idorigine#"	 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
		<!--- EQUIPEMENT --->
	<cffunction name="associerEqpToSim" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerTermSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerEqpToExt" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerTermSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerEqpWithSimToExt" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerTermExt">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(11,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<!--- ASSOCIATION LIGNE --->
		<!--- COLLABORATEUR A UNE LIGNE MOBILE--->
	<cffunction name="associerCollabToLigneSim1" access="remote" returntype="numeric">
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		
		<cflog text="action = #result#">
		
		
		<cfreturn result>
	</cffunction>
	<cffunction name="associerCollabToLigneSim2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerCollabToLigneSim3" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
		<!--- COLLABORATEUR A UNE LIGNE FIXE--->
	<cffunction name="associerCollabToLigne1" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabLigne">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="associerCollabToLigne2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabLigne">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerCollabToLigne3" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabLigne">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="3" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerCollabToLigne4" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.asscoierCollabLigne">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(2,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
		<!--- EQUIPEMENT  --->
	<cffunction name="associerEqpToLigne1" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerTermLigne">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(7,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(7,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerEqpToLigne2" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerTermSim">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(7,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(7,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- SIM --->
	<cffunction name="associerSimToLigne" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"			required="true" type="string" />
		<cfargument name="ITEM2"			required="true" type="string" />
		<cfargument name="ID_TERMINAL"		required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerSimLigne">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="associerExtToLigne" access="remote" returntype="numeric">
		
		<cfargument name="idorigine" 		required="true" type="Numeric" />
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"			required="true" type="string" />
		<cfargument name="ITEM2"			required="true" type="string" />
		<cfargument name="ID_TERMINAL"		required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M111B.associerSimLigne">
					<cfprocparam  value="#idorigine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#iddestination#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(8,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
</cfcomponent>