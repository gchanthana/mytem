package rapportPilotage.utils.periodeselector
{
	import mx.containers.Box;
	import mx.controls.HSlider;
	import mx.events.SliderEvent;
	import rapportPilotage.event.RapportIHMEvent;

	public class PeriodeSelectorRoot extends Box
	{
		public var slider : HSlider;
		
		public function PeriodeSelectorRoot()
		{
			super();
		}
		protected function slider_changeHandler(event:SliderEvent):void
		{
			dispatchEvent(new RapportIHMEvent(RapportIHMEvent.CHANGER_VALUE_HSLIDER_BIS_EVENT));
		}
	}
}