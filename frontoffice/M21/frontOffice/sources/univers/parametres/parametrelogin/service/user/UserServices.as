package univers.parametres.parametrelogin.service.user
{
    import mx.rpc.AbstractOperation;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

    public class UserServices
    {
        public var myDatas:UserDatas;
        public var myHandlers:UserHandlers;

        public function UserServices()
        {
            myDatas = new UserDatas();
            myHandlers = new UserHandlers(myDatas);
        }

        /**
         *
         * Affecte tous accès  d'un utilisateur à un autre ( sur otutes les racines)
         * @param userRef_id: l'id de l'utilisateur de référence
         * @param userToAffectId :  l'id de l'utilisateur à affecter
         * @param id_racine : l'id du groupe racine
         */
        public function duplicateAllUserProfile(userRef_id:Number, userToAffectId:Array):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																					"fr.consotel.consoview.M21.LoginService",
																					"duplicateAllAccessR", 
																					myHandlers.duplicateUserProfileResultHandler, null);
            RemoteObjectUtil.callService(op1, userRef_id, userToAffectId);
        }
    }
}