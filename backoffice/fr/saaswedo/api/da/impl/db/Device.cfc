<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.db.RoamingDetection" extends="fr.saaswedo.api.da.impl.Device"
hint="Extension qui fournit les données à des procédures stockées">
	<cffunction access="private" name="saveRegisterDevice" returntype="void"
	hint="Stocke le flux Register Device au format JSON dans la base. En cas d'exception il sera stocké avec l'implémentation parent">
		<cfargument name="datalertPayload" type="Struct" required="true" hint="Données du flux">
		<cfset var targetURI=CGI.SCRIPT_NAME>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<cfset var dbJsonData=serializeJSON(ARGUMENTS.datalertPayload)>
		<cftry>
			<cfset storeUsageInDatabase("pkg_m26.registerDevice",dbJsonData)>
			<!--- En cas d'exception : Le stockage de l'implémentation parent sera effectué --->
			<cfcatch type="Any">
				<cflog type="error" text="[#log#][#targetURI#] #clientInfo# - #CFCATCH.message# [#CFCATCH.detail#]">
				<cfset var requestData=DUPLICATE(getHttpRequestData())>
				<cfset requestData[DB_JSON_DATA()]=dbJsonData>
				<!--- Traitement de l'exception : Sans envoi de réponse HTTP au client car les données seront stockées avec l'implémentation parent --->
				<cfset processServerError(targetURI,requestData,CFCATCH,"Register Device : Storage FAILED")>
				<!--- Stockage par l'implémentation parent --->
				<cfset SUPER.saveRegisterDevice(argumentCollection=ARGUMENTS)>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>