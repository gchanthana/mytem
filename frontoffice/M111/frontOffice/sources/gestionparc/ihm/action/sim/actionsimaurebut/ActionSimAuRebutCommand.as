package gestionparc.ihm.action.sim.actionsimaurebut
{
	import gestionparc.ihm.action.ICommand;

	public class ActionSimAuRebutCommand implements ICommand
	{
		private var receiver:ActionSimAuRebutReceiver=null;

		public function ActionSimAuRebutCommand(rec:ActionSimAuRebutReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
