<!--- =========================================================================
Classe: Contact
Auteur: brice.miramont
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="Contact" hint="Pour la gestion dse contacts" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="Contact" hint="Remplace le constructeur de Contact.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->

	<cffunction name="affecter_role_cde_contact" access="public" returntype="numeric" output="false" hint="Ajouter un enregistrement a la liste des roles d'un contact (table ROLE_GESTION_CONTACT)" >
		<cfargument name="IDCDE_CONTACT" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT" hint="Initial value for the IDCDE_CONTACTproperty." />
		<cfargument name="IDROLE_GESTION" required="true" type="numeric" default="" displayname="numeric IDROLE_GESTION" hint="Initial value for the IDROLE_GESTIONproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.affecter_role_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDROLE_GESTION#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="create_cde_contact" access="public" returntype="any" output="false" hint="Ajouter un contact a une société." >
		<cfargument name="IDCDE_CONTACT_SOCIETE" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT_SOCIETE" hint="Initial value for the IDCDE_CONTACT_SOCIETEproperty." />
		<cfargument name="IDRACINE" required="true" type="numeric" default="" displayname="numeric IDRACINE" hint="Initial value for the IDRACINEproperty." />
		<cfargument name="CIVILITE" required="true" type="string" default="" displayname="string CIVILITE" hint="Initial value for the CIVILITEproperty." />
		<cfargument name="NOM" required="true" type="string" default="" displayname="string NOM" hint="Initial value for the NOMproperty." />
		<cfargument name="PRENOM" required="true" type="string" default="" displayname="string PRENOM" hint="Initial value for the PRENOMproperty." />
		<cfargument name="EMAIL" required="true" type="string" default="" displayname="string EMAIL" hint="Initial value for the EMAILproperty." />
		<cfargument name="ADRESSE1" required="true" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="true" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="true" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="true" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="true" type="numeric" default="1577" displayname="numeric PAYSCONSOTELID" hint="Initial value for the PAYSCONSOTELIDproperty." />
		<cfargument name="TELEPHONE" required="true" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="true" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="APP_LOGINID" required="true" type="numeric" default="0" displayname="numeric APP_LOGINID" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="TEL_MOBILE" required="true" type="string" default="" displayname="string TEL_MOBILE" hint="Initial value for the TEL_MOBILEproperty." />
		<cfargument name="CODE_INTERNE" required="true" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="FONCTION" required="true" type="string" default="" displayname="string FONCTION" hint="Initial value for the FONCTIONproperty." />
		<cfargument name="ROLES" required="true" type="array" default="" displayname="array ROLES" hint="Initial value for the ROLESproperty." />
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.create_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT_SOCIETE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#CIVILITE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#NOM#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#PRENOM#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#EMAIL#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE1#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE2#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#COMMUNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ZIPCODE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#PAYSCONSOTELID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TELEPHONE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FAX#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Session.user.CLIENTACCESSID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#APP_LOGINID#" null="#iif((APP_LOGINID eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TEL_MOBILE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#CODE_INTERNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FONCTION#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		
		
		<cfif p_result gt 0>
			<cfset userId = p_result>
			<cfset rolesAffected = 0>
			<cfloop index="count" from="1" to="#arrayLen(ROLES)#">
				<cfset idRole = #ROLES[count]["IDROLE_GESTION"]#>
				<cfset affectresult = affecter_role_cde_contact(userId, idRole)>
				 
				<cfif affectresult lt 0>
					<cfset rolesAffected = -1>
				</cfif>
				
			</cfloop>
			
			<cfif rolesAffected lt 0>
				<cfreturn -1>
			</cfif>			
			
		<cfelse>
			<cfreturn p_result>
		</cfif>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="delete_cde_contact" access="public" returntype="numeric" output="false" hint="Supprimer un contact." >
		<cfargument name="IDCDE_CONTACT" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT" hint="Initial value for the IDCDE_CONTACTproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.delete_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="edit_cde_contact" access="public" returntype="any" output="false" hint="Editer un contact." >
		<cfargument name="IDCDE_CONTACT" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT" hint="Initial value for the IDCDE_CONTACTproperty." />
		<cfargument name="IDCDE_CONTACT_SOCIETE" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT_SOCIETE" hint="Initial value for the IDCDE_CONTACT_SOCIETEproperty." />
		<cfargument name="CIVILITE" required="true" type="string" default="" displayname="string CIVILITE" hint="Initial value for the CIVILITEproperty." />
		<cfargument name="NOM" required="true" type="string" default="" displayname="string NOM" hint="Initial value for the NOMproperty." />
		<cfargument name="PRENOM" required="true" type="string" default="" displayname="string PRENOM" hint="Initial value for the PRENOMproperty." />
		<cfargument name="EMAIL" required="true" type="string" default="" displayname="string EMAIL" hint="Initial value for the EMAILproperty." />
		<cfargument name="ADRESSE1" required="true" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="true" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="true" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="true" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="true" type="numeric" default="" displayname="numeric PAYSCONSOTELID" hint="Initial value for the PAYSCONSOTELIDproperty." />
		<cfargument name="TELEPHONE" required="true" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="true" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="APP_LOGINID" required="true" type="numeric" default="" displayname="numeric APP_LOGINID" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="TEL_MOBILE" required="true" type="string" default="" displayname="string TEL_MOBILE" hint="Initial value for the TEL_MOBILEproperty." />
		<cfargument name="CODE_INTERNE" required="true" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="FONCTION" required="true" type="string" default="" displayname="string FONCTION" hint="Initial value for the FONCTIONproperty." />
		<cfargument name="ROLES" required="true" type="array" default="" displayname="array ROLES" hint="Initial value for the ROLESproperty." />
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.edit_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT_SOCIETE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#CIVILITE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#NOM#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#PRENOM#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#EMAIL#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE1#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE2#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#COMMUNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ZIPCODE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#PAYSCONSOTELID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TELEPHONE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FAX#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#APP_LOGINID#" null="#iif((APP_LOGINID eq -1), de("yes"), de("no"))#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TEL_MOBILE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#CODE_INTERNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FONCTION#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		
		
		<cfif p_result gt 0>
			<cfset desaffectResult = supprimer_role_cde_contact(IDCDE_CONTACT)>
			<cfset userId = IDCDE_CONTACT>
			<cfset rolesAffected = 0>
			<cfloop index="count" from="1" to="#arrayLen(ROLES)#">
				<cfset idRole = #ROLES[count]["IDROLE_GESTION"]#>
				<cfset affectresult = affecter_role_cde_contact(userId, idRole)>
				 
				<cfif affectresult lt 0>
					<cfset rolesAffected = -1>
				</cfif>
				
			</cfloop>
			
			<cfif rolesAffected lt 0>
				<cfreturn -1>
			</cfif>			
			
		<cfelse>
			<cfreturn p_result>
		</cfif>		
		
		
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="get_cde_contact" access="public" returntype="query" output="false" hint="Raméne les informations d'un contact." >
		<cfargument name="IDCDE_CONTACT" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT" hint="Initial value for the IDCDE_CONTACTproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="lister_cde_contact" access="public" returntype="query" output="false" hint="Lister les contacts pour une société ou une agence." >
		<cfargument name="IDCDE_CONTACT_SOCIETE" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT_SOCIETE" hint="Initial value for the IDCDE_CONTACT_SOCIETEproperty." />
		<cfargument name="APP_LOGINID" required="true" type="numeric" default="" displayname="numeric APP_LOGINID" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="chaine" required="true" type="string" default="" displayname="string chaine" hint="Initial value for the chaineproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.lister_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT_SOCIETE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#APP_LOGINID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="lister_roles_cde_contact" access="public" returntype="query" output="false" hint="Raméne la liste des réles d'un contact." >
		<cfargument name="IDCDE_CONTACT" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT" hint="Initial value for the IDCDE_CONTACTproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.lister_roles_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>

	<cffunction name="Lister_roles" access="public" returntype="query" output="false" hint="Raméne la liste des réles possible spour un contact." >		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.Lister_roles">			
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="supprimer_role_cde_contact" access="public" returntype="numeric" output="false" hint="Supprimer tous les enregistrements de la liste des réles d'un contact (table ROLE_GESTION_CONTACT). On supprime tous les roles d'un coup. Ensuite l'insertion se fait dans une boucle dans Coldfusion." >
		<cfargument name="IDCDE_CONTACT" required="true" type="numeric" default="" displayname="numeric IDCDE_CONTACT" hint="Initial value for the IDCDE_CONTACTproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.supprimer_role_cde_contact">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>

	
	
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>