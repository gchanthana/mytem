package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	 

	[Bindable]
	public class Collaborateur
	{

		public var IDGROUPE_CLIENT:Number = 0;
		public var IDEMPLOYE:Number = 0;
		public var IDRACINE_NUMBER:Number = 0;
		public var IDSITE_PHYSIQUE:Number = 0;
		public var IDEMPLACEMENT:Number = 0;
		public var IDPOOL_GESTION:Number = 0;
		public var APP_LOGINID:Number = 0;
		public var CLE_IDENTIFIANT:String = "";
		public var CIVILITE:String = "";
		public var LCIVILITE:String = "";
		public var NOMPRENOM:String = "";
		public var NOM:String = "";
		public var PRENOM:String = "";
		public var EMAIL:String = "";
		public var FONCTION_EMPLOYE:String = "";
		public var STATUS_EMPLOYE:String = "";
		public var COMMENTAIRE:String = "";
		public var BOOL_PUBLICATION:Number = 0;
		public var CODE_INTERNE:String = "";
		public var REFERENCE_EMPLOYE:String = "";
		public var MATRICULE:String = "";
		public var DATE_CREATION:Date = null;
		public var DATE_MODIFICATION:Date = null;
		public var INOUT:Number = 0;
		public var DATE_ENTREE:Date = null;
		public var DATE_SORTIE:Date = null;
		public var SEUIL_COMMANDE:Number = 0;
		public var NIVEAU:Number = 0;
		public var C1:String = "";
		public var C2:String = "";
		public var C3:String = "";
		public var C4:String = "";
		public var TELEPHONE_FIXE:String = "";
		public var FAX:String = "";
		
		public var NBRECORD	:int = 30;
		public var NBROWS	:int = 0;
		
		public var SELECTED:Boolean=false;
		
		public var LISTEORGANISATIONS:ArrayCollection=new ArrayCollection();



		public function Collaborateur()
		{
		}

	}
}