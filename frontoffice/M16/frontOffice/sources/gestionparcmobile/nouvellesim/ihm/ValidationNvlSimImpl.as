package gestionparcmobile.nouvellesim.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.VBox;
	import mx.containers.ViewStack;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import commandemobile.ihm.mail.MailCOmmandeIHM;
	
	import gestioncommande.custom.CustomRadioButton;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.services.WorkflowService;
	
	import gestionparcmobile.resiliationLigne.validation.OptionsModifyIHM;
	import gestionparcmobile.resiliationLigne.validation.ReengagementIHM;
	import gestionparcmobile.resiliationLigne.validation.ResiliationLigneIHM;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ValidationNvlSimImpl extends Box
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var mailCommande				:MailCOmmandeIHM;
		
		public var vs						:ViewStack;
		
		public var vbxAction				:VBox;
		
		//		public var hbxRecapitulatif			:Box;
		
		public var txaCommentaires			:TextArea;
		
		//--------------------------------------------------------------------------------------------//
		//					VIEWSTACK
		//--------------------------------------------------------------------------------------------//
		
		public var reengagement				:ReengagementIHM 		= null;
		
		public var optionsModify			:OptionsModifyIHM 		= null;
		
		public var resiliationLigne			:ResiliationLigneIHM	= null;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var selectedAction			:Action;
		
		private var _cmd					:Commande;
		
		private var _myElements				:ElementsCommande2 = new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _wflSrv					:WorkflowService = new WorkflowService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var linesNotSelected			:ArrayCollection = null;
		
		public var libelleAction			:String = "";
		
		public var indexSelected			:int = -1;
		
		//--------------------------------------------------------------------------------------------//
		//					CONCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 5;
		
		public var ACCESS					:Boolean = false;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function ValidationNvlSimImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		public function validateCurrentPage():Boolean
		{			
			return (vs.getChildAt(vs.selectedIndex) as Object).validateCurrentPage();
		}
		
		public function reset():void
		{
			var children :Array = vs.getChildren();
			var lenChild :int = children.length;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).reset();
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function showViewStackHandler(slideViewStack:int):void
		{
			var index:int = slideViewStack;
		}
		
		public var lblRecapitulatif			:Label;
		public var lblEditerMail			:Label;
		
		public var isActionSelected			:Boolean = false;
		public var isViewMail				:Boolean = false;
		
		protected function lblEditerMailClickHandler(me:MouseEvent):void
		{
			if(!lblEditerMail.enabled) return;
			
			isViewMail = true;
			
			lblRecapitulatif.setStyle('textDecoration', 'underline');
			lblRecapitulatif.useHandCursor 	= true;
			lblRecapitulatif.buttonMode 	= true;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'none');
			lblEditerMail.useHandCursor = false;
			lblEditerMail.buttonMode 	= false;
			lblEditerMail.validateNow();
		}
		
		protected function lblRecapitulatifClickHandler(me:MouseEvent):void
		{
			isViewMail = false;
			
			lblRecapitulatif.setStyle('textDecoration', 'none');
			lblRecapitulatif.useHandCursor 	= false;
			lblRecapitulatif.buttonMode 	= false;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'underline');
			lblEditerMail.useHandCursor = true;
			lblEditerMail.buttonMode 	= true;
			lblEditerMail.validateNow();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODE PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//	
		
		private function creationCompleteHandler(e:Event):void
		{
		}
		
		private function showHandler(e:Event):void
		{
			_cmd		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			getTypeCommande();
			
			if(vbxAction.numChildren == 0)
				fournirListeActionsPossibles();
			
			setGoodEligibilite();
			
			switch(indexSelected)
			{
				case 0: libelleAction = ResourceManager.getInstance().getString('M16', 'commande_de_r_engagement'); break;
				case 1: libelleAction = ResourceManager.getInstance().getString('M16', 'modification_d_options'); 	break;
				case 2: resiliationLigne.libelleAction = libelleAction = attributLibelleAction();  					break;
			}
			var s_idx:int = vs.selectedIndex;
			(vs.getChildAt(s_idx) as Object).setMyDataToGrid();
		}
		
		private function setGoodEligibilite():void
		{
			var currentConfiguration	:ArrayCollection = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS;
			var lineOptions				:ArrayCollection = _myElements.LINEOPTIONS;
			var lenConf					:int = currentConfiguration.length;
			var lenOpts					:int = lineOptions.length;
			
			for(var i:int = 0; i < lenConf;i++)
			{
				var idsstt:int = currentConfiguration[i].IDSOUS_TETE;
				var iselig:int = int(currentConfiguration[i].ELIGIBILITE);
				
				for(var j:int = 0; j < lenOpts;j++)
				{
					if(lineOptions[j].IDSOUSTETE == idsstt)
					{
						lineOptions[j].ISELIGIBLE = iselig;
					}
				}
			}
		}
		
		private function cstmRdbtnClickHandler(e:Event):void
		{
			var myChidldren	:Array = vbxAction.getChildren();
			var lenChildren :int = vbxAction.numChildren;
			
			selectedAction = (e.target as CustomRadioButton).ACTION
			
			for(var i:int = 0; i < lenChildren;i++)
			{
				if(myChidldren[i] as CustomRadioButton)
				{
					if((myChidldren[i] as CustomRadioButton).ACTION.IDACTION != selectedAction.IDACTION)
						(myChidldren[i] as CustomRadioButton).selected = false;
				}
			}
			
			if(selectedAction.IDACTION > 0)
			{				
				isActionSelected = true;
				
				lblEditerMail.enabled = true;
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'underline');
				lblEditerMail.useHandCursor = true;
				lblEditerMail.buttonMode 	= true;
				lblEditerMail.validateNow();
			}
			else
			{
				isActionSelected = false;
				
				lblEditerMail.enabled = false;
				
				lblRecapitulatifClickHandler(new MouseEvent(MouseEvent.CLICK));
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'none');
				lblEditerMail.useHandCursor = false;
				lblEditerMail.buttonMode 	= false;
				lblEditerMail.validateNow();
			}
			
			this.validateNow();
			
			mailCommande.setAction(selectedAction);
			
			dispatchEvent(new Event('VALIDER_ENABLED', true));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function fournirListeActionsPossibles():void
		{
			_wflSrv.fournirActionsNoRemote(3066,_cmd, SessionUserObject.singletonSession.IDSEGMENT);
			
			addRecordOnly();
			
			if(_wflSrv.listeActions.length > 0)
			{
				for(var i:int = 0;i < _wflSrv.listeActions.length;i++)
				{
					if(_wflSrv.listeActions[i].CODE_ACTION != "ANNUL" && _wflSrv.listeActions[i].CODE_ACTION != null && _wflSrv.listeActions[i].CODE_ACTION != "")
					{
						var cstmRdbtn:CustomRadioButton = new CustomRadioButton();
						cstmRdbtn.ACTION			= _wflSrv.listeActions[i] as Action;
						cstmRdbtn.addEventListener("RADIOBUTTON_CLICKHANDLER", cstmRdbtnClickHandler);
						
						vbxAction.addChild(cstmRdbtn);
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		//---> 5-> REENGAGEMENT, 6-> MODIFICATION OPTION, 7-> SUSPENSION, 8-> REACTIVATION, 9-> RESILIATION
		private function getTypeCommande():void
		{
			var idtype	:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			switch(idtype)
			{
				case 5: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.RENOUVELLEMENT;
						else
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.RENOUVELLEMENT_FIXE; 
					break;

				case 6: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.MODIFICATION_OPTIONS;
						else
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.MODIFICATION_FIXE; 
					break;

				case 7: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.SUSPENSION;
						else
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.SUSPENSION_FIXE; 
					break;

				case 8: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.REACTIVATION;
						else
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.REACTIVATION_FIXE; 
					break;

				case 9: if(SessionUserObject.singletonSession.IDSEGMENT == 1)
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.RESILIATION;
						else
							SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.RESILIATION_FIXE; 
					break;
				
				case 11:
					SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE = TypesCommandesMobile.NOUVELLE_CARTE_SIM;
					break;
			}
		}
		
		private function showLibelle():String
		{
			var libelle:String = "";
			
			switch(_cmd.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.MODIFICATION_OPTIONS	: libelle = ResourceManager.getInstance().getString('M16', 'modifier__'); 	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE		: libelle = ResourceManager.getInstance().getString('M16', 'modifier__'); 	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION			: libelle = ResourceManager.getInstance().getString('M16', 'r_silier__'); 	break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE		: libelle = ResourceManager.getInstance().getString('M16', 'r_silier__'); 	break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT		: libelle = ResourceManager.getInstance().getString('M16', 'renouveler__'); 	break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE	: libelle = ResourceManager.getInstance().getString('M16', 'renouveler__'); 	break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION			: libelle = ResourceManager.getInstance().getString('M16', 'suspendre__'); 	break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE		: libelle = ResourceManager.getInstance().getString('M16', 'suspendre__'); 	break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION			: libelle = ResourceManager.getInstance().getString('M16', 'r_activer__');	break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE		: libelle = ResourceManager.getInstance().getString('M16', 'r_activer__');	break;//--- REACT
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM	: libelle = ResourceManager.getInstance().getString('M16', 'r_activer__');	break;//--- Nouvelle carte SIM
			}
			
			return libelle;
		}
		
		private function attributLibelleAction():String//---> 7-> SUSPENSION, 8-> REACTIVATION, 9-> RESILIATION
		{	
			var libelle:String = "";
			
			switch(SessionUserObject.singletonSession.IDTYPEDECOMMANDE)
			{
				case 7:libelle = ResourceManager.getInstance().getString('M16', 'Suspension'); break;
				case 8:libelle = ResourceManager.getInstance().getString('M16', 'R_activation'); break;
				case 9:libelle = ResourceManager.getInstance().getString('M16', 'R_siliation'); break;
				case 11:libelle = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_'); break;
			}
			
			return libelle;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - MISE EN FORME DES RADIOBUTTON
		//--------------------------------------------------------------------------------------------//
		
		private function addRecordOnly():void
		{
			var cstmRdbtn:CustomRadioButton 		= new CustomRadioButton();
			cstmRdbtn.ACTION					= new Action();
			cstmRdbtn.ACTION.LIBELLE_ACTION		= ResourceManager.getInstance().getString('M16','Enregistrer_la_commande_sans_l_envoyer');
			cstmRdbtn.ACTION.IDACTION			= -1;
			cstmRdbtn.addEventListener('RADIOBUTTON_CLICKHANDLER', cstmRdbtnClickHandler);
			
			vbxAction.addChild(cstmRdbtn);
		}
	}
}