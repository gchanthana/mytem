<cfcomponent output="false">
	
	<cffunction access="remote" name="ListeDestProfil"  returntype="query" output="true">
	
		<cfargument name="idDestinataire" type="numeric" required="true">
	
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.listProfilDest">		
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idDestinataire#" variable="p_iddestinataire">
			<cfprocresult name="qListeDestProfil">		
		</cfstoredproc>	
	
		<cfreturn qListeDestProfil>
			
	</cffunction>
</cfcomponent>