package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleReprendreLigneSim implements IRuleDisplayItem
	{
		public function RuleReprendreLigneSim()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneMobile();
			var r2:IRuleDisplayItem=new HasSimAssociated();
			if (r1.isDisplay() && r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
