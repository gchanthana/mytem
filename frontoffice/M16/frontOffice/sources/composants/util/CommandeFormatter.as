package composants.util
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.NumberBaseRoundType;
	import mx.resources.ResourceManager;

	public class CommandeFormatter extends EventDispatcher
	{
		public var commandeCurrencyPrecision:String = "2";
		public var commandeCurrencySymbol:String = "";
		public var commandeDecimalSeparator:String = ResourceManager.getInstance().getString('M16','formateur_decimalSeparatorTo');
		public var commandeThousandsSeparator:String = ResourceManager.getInstance().getString('M16','formateur_thousandsSeparatorTo');
		public var commandeAlignSymbol:String = ResourceManager.getInstance().getString('M16','formateur_align_symbol');
		
		private static var _commandeFormatter:CommandeFormatter;
		
		public static function getInstance():CommandeFormatter
		{
			if (!_commandeFormatter)
			{
				_commandeFormatter = new CommandeFormatter();
			}
			return _commandeFormatter;
		}
		
		public function formatCommandeCurrency(currency:String, num:Number):String
		{
			setCommandeCurrency(currency);
			setValuesByCurrency();
			return formatCurrency(num);
		}
		
		//formate la monnaie avec la précision passée en parametre	
		public function formatCurrency(num:Number):String
		{
			var cf:CurrencyFormatter = new CurrencyFormatter();
			cf.precision = commandeCurrencyPrecision;
			cf.rounding = NumberBaseRoundType.NONE;
			cf.decimalSeparatorTo = commandeDecimalSeparator;
			cf.thousandsSeparatorTo = commandeThousandsSeparator;
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.alignSymbol = commandeAlignSymbol;
			cf.currencySymbol = commandeCurrencySymbol;//Mettre ici la devise d'origine du revendeur
			return cf.format(num);
		}
		
		private function setValuesByCurrency():Boolean
		{
			switch (this.commandeCurrencySymbol)
			{
				case "€":
					this.commandeDecimalSeparator = ",";
					this.commandeThousandsSeparator = " ";
					this.commandeAlignSymbol = "right";
					return true;
				case "$":
					this.commandeDecimalSeparator = ".";
					this.commandeThousandsSeparator = ",";
					this.commandeAlignSymbol = "left";
					return true;
				case "£":
					this.commandeDecimalSeparator = ".";
					this.commandeThousandsSeparator = ",";
					this.commandeAlignSymbol = "left";
					return true;
				case "¥":
					this.commandeDecimalSeparator = ",";
					this.commandeThousandsSeparator = " ";
					this.commandeAlignSymbol = "right";
					return true;
				case "FR":
					this.commandeDecimalSeparator = ",";
					this.commandeThousandsSeparator = " ";
					this.commandeAlignSymbol = "right";
					return true;
				case "FCFA":
					this.commandeDecimalSeparator = ",";
					this.commandeThousandsSeparator = " ";
					this.commandeAlignSymbol = "right";
					return true;
				case "LEU":
					this.commandeDecimalSeparator = ",";
					this.commandeThousandsSeparator = " ";
					this.commandeAlignSymbol = "right";
					return true;
				default:
					this.commandeDecimalSeparator = ",";
					this.commandeThousandsSeparator = " ";
					this.commandeAlignSymbol = "right";
					return false;
			}
		}
		
		public function setCommandeCurrency(value:String):void
		{
			commandeCurrencySymbol = value;
			setValuesByCurrency();
		}
		
		public function setCommandeCurrencyEvent(value:String):void
		{
			commandeCurrencySymbol = value;
			setValuesByCurrency();
			dispatchEvent(new Event("CURRENCY_CHANGED", true));
		}
		
		public function addNewEventListener(s:String, func:Function):void
		{
			this.addEventListener(s, func);
		}
	}
}