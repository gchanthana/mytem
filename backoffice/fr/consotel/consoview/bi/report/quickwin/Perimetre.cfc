<cfcomponent name="Perimetre" alias="fr.consotel.consoview.bi.report.quickwin.Perimetre">
	<cffunction name="createReportInfos" access="public" returntype="struct">
		<cfargument name="qwInfos" required="true" type="struct">
		<cfset perimetreInfos=arguments.qwInfos["PERIMETRE"]>
		<cfset perimetreReportInfos=structNew()>
		<cfset perimetreReportInfos["ABSOLUTE_PATH"]="/LABS/QUICKWIN/QwPerimetre/QwPerimetre.xdo">
		<cfset perimetreReportInfos["TEMPLATE"]="default">
		<cfset perimetreReportInfos["OUTPUT_DIRECTORY"]="/opt/oracle/home/OBIEE/QUICKWIN/LABS/" & arguments.qwInfos["IDRACINE"] & "/">
		<!--- NE SONT PAS UTILISES 
		<cfset sansConsoDateDebString=parseDateTime(perimetreInfos["DATEDEB"],"yyyy/mm/dd")>
		<cfset sansConsoDateFinString=parseDateTime(perimetreInfos["DATEFIN"],"yyyy/mm/dd")>
		 --->
		<cfset perimetreReportInfos["OUTPUT_FILENAME"]="02-Perimetre">
		<cfset perimetreReportInfos["OUTPUT_FORMAT"]="EXCEL">
		<cfset perimetreReportInfos["BURST_OPTION"]="FALSE">
		<cfset perimetreReportInfos["JOB_NAME"]=arguments.qwInfos["IDRACINE"] & "-PERIMETRE-QWREPORT">
		<!--- REPORT PARAMETERS --->
		<cfset i=1>
		<cfset arrayOfParamNameValues=arrayNew(1)>
		<cfset P_IDRACINE_MASTER=structNew()>
		<cfset P_IDRACINE_MASTER["name"]="P_IDRACINE_MASTER">
		<cfset P_IDRACINE_MASTER["values"]=[arguments.qwInfos["IDRACINE_MASTER"]]>
		<cfset P_IDRACINE_MASTER["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE_MASTER>
		<cfset i=i+1>
		<cfset P_IDRACINE=structNew()>
		<cfset P_IDRACINE["name"]="P_IDRACINE">
		<cfset P_IDRACINE["values"]=[arguments.qwInfos["IDRACINE"]]>
		<cfset P_IDRACINE["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE>
		<cfset i=i+1>
		<cfset P_DATEDEB1=structNew()>
		<cfset P_DATEDEB1["name"]="P_DATEDEB1">
		<cfset P_DATEDEB1["values"]=[perimetreInfos["DATEDEB1"]]>
		<cfset P_DATEDEB1["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEDEB1>
		<cfset i=i+1>
		<cfset P_DATEFIN1=structNew()>
		<cfset P_DATEFIN1["name"]="P_DATEFIN1">
		<cfset P_DATEFIN1["values"]=[perimetreInfos["DATEFIN1"]]>
		<cfset P_DATEFIN1["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEFIN1>
		<cfset i=i+1>
		<cfset P_DATEDEB2=structNew()>
		<cfset P_DATEDEB2["name"]="P_DATEDEB2">
		<cfset P_DATEDEB2["values"]=[perimetreInfos["DATEDEB2"]]>
		<cfset P_DATEDEB2["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEDEB2>
		<cfset i=i+1>
		<cfset P_DATEFIN2=structNew()>
		<cfset P_DATEFIN2["name"]="P_DATEFIN2">
		<cfset P_DATEFIN2["values"]=[perimetreInfos["DATEFIN2"]]>
		<cfset P_DATEFIN2["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEFIN2>
		<cfset i=i+1>
		<cfset P_DATEDEB3=structNew()>
		<cfset P_DATEDEB3["name"]="P_DATEDEB3">
		<cfset P_DATEDEB3["values"]=[perimetreInfos["DATEDEB3"]]>
		<cfset P_DATEDEB3["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEDEB3>
		<cfset i=i+1>
		<cfset P_DATEFIN3=structNew()>
		<cfset P_DATEFIN3["name"]="P_DATEFIN3">
		<cfset P_DATEFIN3["values"]=[perimetreInfos["DATEFIN3"]]>
		<cfset P_DATEFIN3["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEFIN3>
		
		<cfset perimetreReportInfos["ARRAY_OF_PARAMETERS"]=arrayOfParamNameValues>
		<cfreturn perimetreReportInfos>
	</cffunction>
</cfcomponent>
