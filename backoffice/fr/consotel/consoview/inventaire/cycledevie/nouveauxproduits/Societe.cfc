<cfcomponent name="Societe" output="false">
			
	<cffunction name="create" access="public" returntype="numeric">		
		<cfargument name="idGroupeClient" type="numeric" required="true">			
		<cfargument name="ste" type="struct">				
		 
		<cfset userId = session.user.CLIENTACCESSID>				
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.I_SOCIETE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#idGroupeClient#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsociete_mere" value="#ste.societeParenteID#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_raison_sociale" value="#ste.raisonSociale#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_siren" value="#ste.siretSiren#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adresse1" value="#ste.adresse1#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adresse2" value="#ste.adresse2#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commune" value="#ste.commune#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_zipcode" value="#ste.codePsotal#"/>						
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_telephone" value="#ste.telephone#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_fax" value="#ste.fax#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_user_create" value="#userId#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     						     
		</cfstoredproc>		
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="testXmL" access="public" returntype="xml" output="true">		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="TEST_XML">
			<cfprocparam cfsqltype="CF_SQL_LONGVARCHAR" type="out" variable="resultat" />						     						     
		</cfstoredproc>
		
	 	<cfset result = resultat>
	 	
	 	 
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="saveListOperateurSociete" access="public" returntype="numeric">		
		<cfargument name="idsociete" type="numeric" required="true">					
		<cfargument name="listeOpe" type="array" required="true">								
		
		<cfset len = arraylen(listeOpe)>	
		<cfloop index="i" from="1" to="#len#">	
			<cfset result = doSaveOperateurSociete(idsociete,listeOpe[i])>
		</cfloop>					
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="doSaveOperateurSociete" access="private" returntype="numeric">		
		<cfargument name="idsociete" type="numeric" required="true">					
		<cfargument name="idope" type="numeric" required="true">								
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.IU_SOCIETE_OPERATEUR">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsociete" value="#idsociete#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idop" value="#idope#"/>						
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     						     
		</cfstoredproc>		
		
		
		
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="delete" access="public" returntype="numeric">			
		<cfargument name="idSociete" type="numeric">		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.D_SOCIETE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsociete" value="#idsociete#"/>						
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     						     
		</cfstoredproc>				
		<cfreturn result/>	
	</cffunction>	
		
	<cffunction name="get" access="public" returntype="query">			
		<cfargument name="idSociete" type="numeric">			
		<cfquery name="qresult" datasource="#SESSION.OFFREDSN#">
			select ccs.*
			from cde_contact_societe ccs
			where ccs.idcde_contact_societe = #idSociete#
		</cfquery>									
		<cfreturn qresult/>     
	</cffunction>	
	
	<cffunction name="getFormCommande" access="public" returntype="query">			
		<cfargument name="idCommande" type="numeric">			
		<cfquery name="qresult" datasource="#SESSION.OFFREDSN#">
			select ccs.*
			from cde_contact_societe ccs,cde_commande c
			where ccs.idcde_contact_societe = c.idcde_contact_societe
			and c.idcde_commande =  #idCommande#
		</cfquery>									
		<cfreturn qresult/>     
	</cffunction>
	 
	
	<cffunction name="update" access="public" returntype="numeric">			
		<cfargument name="fake" type="numeric" required="true">			
		<cfargument name="ste" type="struct" required="true">						
		<cfset userId = session.user.CLIENTACCESSID>		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.U_SOCIETE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" value="#ste.societeID#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#ste.groupeClientID#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsociete_mere" value="#ste.societeParenteID#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_raison_sociale" value="#ste.raisonSociale#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_siren" value="#ste.siretSiren#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adresse1" value="#ste.adresse1#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_adresse2" value="#ste.adresse2#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commune" value="#ste.commune#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_zipcode" value="#ste.codePsotal#"/>						
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_telephone" value="#ste.telephone#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_fax" value="#ste.fax#"/>			
			<!--- <cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurId" value="#ste.operateurID#"/> --->
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_user_modif" value="#userId#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>						     						     
		</cfstoredproc>		
		
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="getList" access="public" returntype="query">			
		<cfargument name="idGroupeClient" type="numeric" required="true">							
			
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_SOCIETES">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupeClient" value="#idGroupeClient#"/>							
			<cfprocresult name="result">		  
		</cfstoredproc>	
		
		
		 	
		<cfset qResult = result>
		<cfset i = 1>		
		<cfset tabOp = arraynew(1)>
		<cfloop query="qResult">		 	
			<cfset operateurs = getOperateurlistSociete(IDCDE_CONTACT_SOCIETE)>
			<cfset operateursStringList = "" >
			<cfloop query="operateurs">						
				<cfset operateursStringList = operateursStringList & NOM & ",">
			</cfloop>
			<cfif len(operateursStringList) gt 0 >
				<cfset tabOp[i] = left(operateursStringList,len(operateursStringList)-1)>
			</cfif>
			<cfset i=i+1>
		</cfloop>  		
		<cfset queryAddColumn(qResult,"LISTE_OPE","VarChar",tabOp)> 
		 
		
		
		<cfreturn qResult/>			
	</cffunction>
	
	<cffunction name="getContactList" access="public" returntype="query">					
		<cfargument name="contactId" type="numeric" required="true">			
		<cfset userId = session.user.CLIENTACCESSID>
		
		<!--- <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE.">
			
		</cfstoredproc>		 --->
		
		<cfreturn result/>		
		
	</cffunction>
	
	<cffunction name="getAgences" access="public" returntype="query">
		<cfargument name="idSociete" type="numeric" required="true">	
								
		<cfquery datasource="#SESSION.OFFREDSN#" name="result">
			select s.* from cde_contact_societe s
			where s.idsociete_mere = #idSociete# 
		</cfquery>
		<!--- <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE.L_SOCIETES">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGroupeClient" value="#idSociete#"/>							
			<cfprocresult name="result">		  
		</cfstoredproc>	 --->
		
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="getOperateurList" access="public" returntype="query">							
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_ALL_OPERATEURS">		
			<cfprocresult name="result">		  
		</cfstoredproc>			
		<cfreturn result/>
	</cffunction>
	
	<cffunction name="getOperateurlistSociete" access="public" returntype="query">	
		<cfargument name="idsociete" type="numeric" required="true">									
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_OPERATEURSSOCIETE">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsociete" value="#idsociete#"/>					
			<cfprocresult name="result">		  
		</cfstoredproc>			
		<cfreturn result/>
	</cffunction>
	
	<cffunction name="getlistePays" access="public" returntype="query">	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_COMMANDE_V3.L_ALL_PAYS">					
			<cfprocresult name="result">		  
		</cfstoredproc>					
		<cfreturn result/>
	</cffunction>
		
</cfcomponent>