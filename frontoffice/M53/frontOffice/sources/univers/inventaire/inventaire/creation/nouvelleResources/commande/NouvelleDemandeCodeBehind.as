package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.ComboBox;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits.SelectionDesProduitsV2IHM;
	
	[Bindable]
	public class NouvelleDemandeCodeBehind extends Canvas
	{
		//public var treeCible : SearchTree;
		public var cmbOperateur : ComboBox;
		public var pnlSelection : SelectionDesProduitsV2IHM;
		
		private var _oldOperateurId : Number;
		 
		public function NouvelleDemandeCodeBehind()
		{
			//TODO: implement function
			super();
		}
		
		public function get panierEmpty():Boolean{
			try{
				if((pnlSelection.myGridProduits.dataProvider as ArrayCollection).length > 0){
					return false;
				}else{
					return true;
				}	
			}catch(error : Error){
				return true	
			}
			return true	
		} 
		
		/* public function cmbOperateurChangeHandler(event : ListEvent):void{
		//	if (event.currentTarget.selectedItem ! = null)	_oldOperateurId = event.currentTarget.selectedItem.id;
		} */
		
		
		public function selectOldOperateur(event : Event):void{
			if (cmbOperateur.initialized){
				/* var index : Number = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"id",_oldOperateurId);
				if (index > -1){
					cmbOperateur.selectedIndex = index;
					cmbOperateur.dispatchEvent(new ListEvent(ListEvent.CHANGE)); 
				} */
				
			}
		} 
		
	}
}