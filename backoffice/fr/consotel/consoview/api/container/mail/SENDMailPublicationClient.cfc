<cfcomponent output="false" extends="fr.consotel.consoview.api.container.mail.AbstractMail">
	
	<cffunction name="sendMail" returntype="numeric">
		<cfargument name="infosDoc" type="struct" required="true">
			
			<cfset compName = "fr.consotel.consoview.api.container.mail.strategy.SENDMailPublicationClient1">
			<cfset pretour = -1>
			
			<cfif structkeyexists(infosDoc,"CODEAPPLICATION")>
				<cfset compName = "fr.consotel.consoview.api.container.mail.strategy.SENDMailPublicationClient#infosDoc.CODEAPPLICATION#">
			</cfif>	
					
			<cfset pretour = createobject("component",compName).sendMail(infosDoc)>		
				
		<cfreturn pretour>	
	</cffunction>
	
</cfcomponent>