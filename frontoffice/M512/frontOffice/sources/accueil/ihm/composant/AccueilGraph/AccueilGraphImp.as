package accueil.ihm.composant.AccueilGraph
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import accueil.event.AccueilEvent;
	import accueil.ihm.graph.EvolutionCoutBySurthemeIHM;
	import accueil.ihm.graph.IntegrationDesFacturesIHM;
	import accueil.ihm.graph.RepartitionCoutsSurthemeIHM;
	import accueil.ihm.kpi.KpiArpuIHM;
	import accueil.service.data.DataService;
	import accueil.service.perimetre.PerimetreService;
	
	import composants.gradientcomponents.GradientBox;
	
	public class AccueilGraphImp extends VBox
	{
		public var comp0:KpiArpuIHM;
		public var comp1:EvolutionCoutBySurthemeIHM;
		public var comp2:IntegrationDesFacturesIHM;
		public var comp3:RepartitionCoutsSurthemeIHM;	
		
		public var remo1:RemoteObject;
		public var remo2:RemoteObject;
		public var remo3:RemoteObject;
		
		public var vb:GradientBox;
		
		public var dateDebut	:String = "";
		public var dateFin		:String = "";
		
		private var _data1:ArrayCollection;
		private var _data2:ArrayCollection;
		private var _data3:ArrayCollection;	
		
		private var minuteur:Timer = new Timer(300,1);
		private var minuteur2:Timer = new Timer(300,1);
		private var minuteur3:Timer = new Timer(300,1);
		
		
		[Bindable] public var dtService:DataService = new DataService();
				
		public function set data1(value:ArrayCollection):void
		{
			_data1 = value;
		}
		[Bindable] public function get data1():ArrayCollection
		{
			return _data1;
		}
		public function set data2(value:ArrayCollection):void
		{
			_data2 = value;
		}
		[Bindable] public function get data2():ArrayCollection
		{
			return _data2;
		}
		public function set data3(value:ArrayCollection):void
		{
			_data3 = value;
		}
		[Bindable] public function get data3():ArrayCollection
		{
			return _data3;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		public function AccueilGraphImp()
		{
			super();
			
			
			if(!dtService.theModele.hasEventListener(AccueilEvent.LASTE_DATE_COMPLETE_UPDATED))
			{
				dtService.theModele.addEventListener(AccueilEvent.LASTE_DATE_COMPLETE_UPDATED,lastDateUpdatedHandler);	
			}
			
			minuteur.addEventListener(TimerEvent.TIMER_COMPLETE, getData1);
			minuteur2.addEventListener(TimerEvent.TIMER_COMPLETE, getData2);
			minuteur3.addEventListener(TimerEvent.TIMER_COMPLETE, getData3);
			
			remo1 = new RemoteObject("ColdFusion");
			remo1.source="fr.consotel.consoview.M512.AccueilService";
			remo1.addEventListener(ResultEvent.RESULT,getData1ResultHandler,false,0,true);
			remo1.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
			
			remo2 = new RemoteObject("ColdFusion");
			remo2.source="fr.consotel.consoview.M512.AccueilService";
			remo2.addEventListener(ResultEvent.RESULT,getData2ResultHandler,false,0,true);
			remo2.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
			
			remo3 = new RemoteObject("ColdFusion");
			remo3.source="fr.consotel.consoview.M512.AccueilService";
			remo3.addEventListener(ResultEvent.RESULT,getData3ResultHandler,false,0,true);
			remo3.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
			
			remo3 = new RemoteObject("ColdFusion");
			remo3.source="fr.consotel.consoview.M512.AccueilService";
			remo3.addEventListener(ResultEvent.RESULT,getData3ResultHandler,false,0,true);
			remo3.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
		}
		
		public function init():void
		{
			var perimetreE0:PerimetreService;
			if(perimetreE0)
			{				
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_CHANGE, _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE, _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler = null;
				perimetreE0.model = null;
				perimetreE0 = null;				
			}
			
			perimetreE0 = new PerimetreService();
			perimetreE0.handler.addEventListener(AccueilEvent.PERIMETRE_CHANGE
				, _perimetreE0handlerPerimetreChangeHandler,false,0,true);
			perimetreE0.handler.addEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE
				, _perimetreE0handlerPerimetreChangeHandler,false,0,true);
			
			perimetreE0.setUpPerimetre();
		}
		
		public function reset():void
		{
			data1 = null;
			data2 = null;
			data3 = null;
			dateDebut = "";
			dateFin	= "";
			dtService.theModele.reset();
		}
		
		private function getData1ResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				data1 = event.result as ArrayCollection;
				if(data1.length > 0)
				{
					dateDebut 	= data1[0].saw_10;
					dateFin 	= data1[data1.length-1].saw_10;
					comp1.stopLoader();
				}	
			}
		}
		private function getData2ResultHandler(event:ResultEvent):void
		{	
			if(event.result)
			{
				data2 = event.result as ArrayCollection;	
				comp2.stopLoader();
			}							
		}
		private function getData3ResultHandler(event:ResultEvent):void
		{	
			if(event.result)
			{
				data3 = event.result as ArrayCollection;
				comp3.stopLoader();
			}							
		}
		private function faultHandler(event:FaultEvent):void
		{
			trace(event.fault.getStackTrace());
		}
		
		private function getData1(event:TimerEvent):void
		{
			minuteur.stop();
			remo1.getData1();
			minuteur2.start();
		}
		private function getData2(event:TimerEvent):void
		{
			minuteur2.stop();
			remo2.getData2();
			minuteur3.start();
		}
		private function getData3(event:TimerEvent):void
		{
			minuteur3.stop();
			remo3.getData3();
		}
		private function _perimetreE0handlerPerimetreChangeHandler(event:AccueilEvent):void
		{
			minuteur.start();
			
			comp0.stopLoader();
			comp1.stopLoader();
			comp2.stopLoader();
			comp3.stopLoader();
			
			data1 = null;
			data2 = null;
			data3 = null;
			
			comp0.launchLoader();
			comp1.launchLoader();
			comp2.launchLoader();
			comp3.launchLoader();
			
		
			dtService.getPFGPInfosDernierMoisComplet();
			
		}
		
		protected function lastDateUpdatedHandler(event:Event):void
		{
			if(dtService.theModele.pfgpDateLastLoadComplete.IDPERIODE > 0)
			{
				dtService.getIndicateurCle(dtService.theModele.pfgpDateLastLoadComplete.IDPERIODE);
			}
			
		}		
		
	}
}