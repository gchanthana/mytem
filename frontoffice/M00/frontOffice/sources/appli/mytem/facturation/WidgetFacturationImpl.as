package appli.mytem.facturation
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	import mx.containers.Canvas;
	import mx.containers.VBox;
	import mx.controls.Image;
	import mx.effects.Move;
	import mx.events.EffectEvent;
	
	import appli.mytem.facturation.donnees.DonneesFacturationIHM;
	
	import general.event.FacturationEvent;
	import general.service.facturation.FacturationService;
	[Bindable]
	public class WidgetFacturationImpl extends Canvas
	{
		
		private var intervalId:uint;
		
		
		public var canvaFacturation:Canvas;
		public var vb_widget:VBox;
		public var img_bill:Image;
		public var canvasMvtIn:Move;
		public var canvasMvtOut:Move;
		
		 
		public var bool_processing:Boolean = false;
		public var boo_request_error:Boolean = false;
		
		private var serviceBill:FacturationService  = new FacturationService();
		public var odg_facturation:DonneesFacturationIHM;
		
		public function WidgetFacturationImpl()
		{
			super();
			initListeners();
			initServices();
		}
		
		private function initListeners():void
		{
			this.addEventListener(MouseEvent.ROLL_OUT, canvaFacturation_rollOutHandler);
			serviceBill.myDatas.addEventListener(FacturationEvent.FACTURATION_RESULT, getResultBill);
			serviceBill.myDatas.addEventListener(FacturationEvent.FACTURATION_ERROR, processResultBillError);

		}
		
		private function initServices():void
		{	
			serviceBill.getDatasFacturation();
			startSpinner(true);
		}
		
		protected function processResultBillError(event:Event):void
		{	
			startSpinner(false);
			boo_request_error = true;
		}
		
		protected function getResultBill(event:Event):void
		{
			odg_facturation.dataProvider = serviceBill.myDatas.datasBill;
			startSpinner(false);
		}
		
		protected function refresh():void
		{	
			serviceBill.getDatasFacturation();
			startSpinner(true);
		}
		
		private function startSpinner(value:Boolean):void
		{
			if(!odg_facturation) return
			boo_request_error = false;
			bool_processing = value;
			if(value == true)
			{
				odg_facturation.launchLoader();
			}
			else
			{
				odg_facturation.stopLoader();
			}
		}
		
		protected function toggleWidget(me:MouseEvent):void{
			
			
			if (this.x <0) // si this.x négatif alors Apparaitre le Widget
			{
					canvasMvtIn.play();
			}
			else //( this.x >= 0) si x null (ou sup. à zéro) alors Cacher le Widget
			{
					canvasMvtOut.play();
			}
		}
		
		protected function canvaFacturation_rollOutHandler(me:MouseEvent):void
		{
			if (this.x >=0)
			{
				canvasMvtOut.play();
			}
		}
		
		protected function canvasMvtOut_effectEndHandler(event:EffectEvent):void
		{
			vb_widget.visible = false;
		}
		
		protected function canvasMvtIn_effectStartHandler(event:EffectEvent):void
		{
			vb_widget.visible = true;
		}
		
	}
}