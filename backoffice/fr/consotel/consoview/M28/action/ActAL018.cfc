<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" output="false" hint="retourne la liste des intervenants du partenaire triée par intervenants principaux et date de validité du plus récent au plus ancien" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient IDPARTENAIRE">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.List_Interlocuteur">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDPARTENAIRE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
				
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL018", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL018", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL018", 0, ThrowCfCatchError("AL018",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>