package intranet.gestionLoginClasse.services.useraccountstate
{
	import mx.rpc.events.ResultEvent;

	internal class UserAccountStateHandler
	{
		private var _model:UserAccountStateModel;
		public function UserAccountStateHandler(model:UserAccountStateModel)
		{
			_model = model;
		}
		
		internal function updateUserAccountStateHandler(re:ResultEvent):void
		{
			var value:Number = (re.result>=0)?Number(re.result):-99;
			_model.updateUserAccountState(value)
		}
	}
}