package accueil.ihm.graph
{
    import accueil.utils.preloader.Spinner;
    
    import composants.util.ConsoviewFormatter;
    
    import mx.charts.series.ColumnSeries;
    import mx.charts.series.items.ColumnSeriesItem;
    import mx.collections.ArrayCollection;
    import mx.collections.ICollectionView;
    import mx.collections.IViewCursor;
    import mx.containers.Canvas;
    import mx.formatters.CurrencyFormatter;
    import mx.resources.ResourceManager;

    [Bindable]
    public class EvolutionCoutBySegmentImpl extends Canvas
    {
        private var _dataProvider:ICollectionView;
        private var _progressBar:Spinner;
        public var dataProviderFixe:ArrayCollection;
        public var dataProviderMobile:ArrayCollection;
        public var dataProviderData:ArrayCollection;
        
        
			
		public var total:Number = 0;
		public var cf		:CurrencyFormatter;
		
		
		

        public function EvolutionCoutBySegmentImpl()
        {
            super();
        }

        protected function creationCompleteHandler():void
        {
            // instanciation et ajout du loader à la liste d'affichage
            //launchLoader();
        }

        public function launchLoader():void
        {
            _progressBar = new Spinner();
            _progressBar.x = this.width / 2;
            _progressBar.y = this.height / 2;
            this.addChild(_progressBar);
            _progressBar.play();
        }
		
		protected function mydataTipFunction(obj:Object):String
		{
			if(obj)
			{
				try
				{
					var csi:ColumnSeriesItem = obj.chartItem as ColumnSeriesItem;
					if(csi)
					{
						var libelle :String = (csi.element  as ColumnSeries).displayName;
					    var montant	:Number = csi.yNumber;
						var mois 	:String = csi.xValue.toString(); 
					  	return ResourceManager.getInstance().getString('M511', 'P_riode____b_')+ mois + ResourceManager.getInstance().getString('M511', '__b__br_Libell_____b_')+ libelle + ResourceManager.getInstance().getString('M511', '__b__br_Montant____b__font_color____ff00')+ConsoviewFormatter.formatEuroCurrency(montant,2)+"</font>";	
					}
					else
					{
						return ""
					}
				}catch(e:Error)
				{
					trace(e.getStackTrace());
					return "";
				}
				return "";
					
			}
			else
			{
				return "";
			}
			
		}
		
        public function set dataProvider(value:ICollectionView):void
        {
            if (!value)
            {
            	_dataProvider = null;
                
            }
            else if (_dataProvider != value)
            {   
               
               
            	var cursor:IViewCursor = value.createCursor();
            	var currentIdPeriode:Number = 0;
            	
            	_dataProvider = new ArrayCollection();
            	           
                while(!cursor.afterLast)
                {	
                	
                	if(cursor.current["saw_12"] !=  currentIdPeriode)
                	{
                		var currentObj:Object = {};
                		currentObj["saw_10"]=cursor.current["saw_10"];
                		currentObj["saw_12"]=cursor.current["saw_12"];
                		currentObj[cursor.current["saw_13"]] = cursor.current["saw_11"];
                		
                		currentIdPeriode = currentObj["saw_12"];
                		
                		(_dataProvider as ArrayCollection).addItem(currentObj);
                		 
                	}
                	else
                	{
                		
                		currentObj[cursor.current["saw_13"]] = cursor.current["saw_11"];
                		(_dataProvider as ArrayCollection).itemUpdated(currentObj);
                	}   
                	cursor.moveNext();             	
                	
                }
            }
            
            if (_progressBar)
            {
                _progressBar.stop();
                this.removeChild(_progressBar);
                _progressBar = null;
            }
        }
		
		public function stopLoader():void
		{
			if (_progressBar)
			{
				_progressBar.stop();
				this.removeChild(_progressBar);
				_progressBar = null;
			}	
		}

        public function get dataProvider():ICollectionView
        {
            return _dataProvider;
        }
    }
}