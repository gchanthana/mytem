<cfcomponent name="LoginService">
<!--- création d'un nouvel utilisateur --->
	<cffunction name="createUtilisateur" access="remote" returntype="numeric" output="false">
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		<cfargument name="idType"			required="true" type="numeric"/>
		<cfargument name="idRevendeur"		required="true" type="numeric"/>
		<cfargument name="idRacineRat"	 	required="true" type="numeric"/>
		<cfargument name="attribut" 		required="false" type="string" >

			<cfif idRacineRat EQ -1>
			
				<cfset arguments.idRacineRat = SESSION.PERIMETRE.ID_GROUPE>
			
			</cfif>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.ADD_APP_LOGIN_V3">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_IDRACINE" 		value="#idRacineRat#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_TYPE" 			value="#idType#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_REVENDEUR" 		value="#idRevendeur#" null="#iif((idRevendeur eq -1), de("yes"), de("no"))#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_attribut_sso" 	value="#attribut#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

<!--- OK --->
	<cffunction name="updateUtilisateur" access="remote" returntype="numeric" output="false">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="nom" 				required="true" type="string"/>
		<cfargument name="prenom" 			required="true" type="string"/>
		<cfargument name="adresse" 			required="true" type="string"/>
		<cfargument name="codepostal" 		required="true" type="string"/>
		<cfargument name="ville" 			required="true" type="string"/>
		<cfargument name="telephone" 		required="true" type="string"/>
		<cfargument name="direction" 		required="true" type="string"/>
		<cfargument name="email" 			required="true" type="string"/>
		<cfargument name="password" 		required="true" type="string"/>
		<cfargument name="restrictionIP"	required="true" type="numeric"/>
		<cfargument name="idType"			required="true" type="numeric"/>
		<cfargument name="idRevendeur"		required="true" type="numeric"/>
		<cfargument name="idRacineRat"	 	required="true" type="numeric"/>
		<cfargument name="attribut" 		required="false" type="string" >

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m21.UPD_APP_LOGIN_v3">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_APP_LOGINID" 		value="#appLogin#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_NOM" 		value="#nom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_LOGIN_PRENOM" 	value="#prenom#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_adresse" 			value="#adresse#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_codepostal" 		value="#codepostal#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ville" 			value="#ville#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_telephone" 		value="#telephone#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_direction" 		value="#direction#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_EMAIL" 		value="#email#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="P_LOGIN_PWD" 		value="#password#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_RESTRICTION_IP" 	value="#restrictionIP#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_TYPE" 			value="#idType#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_REVENDEUR" 		value="#idRevendeur#" null="#iif((idRevendeur eq -1), de("yes"), de("no"))#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" 		value="#idRacineRat#" null="#iif((idRacineRat eq -1), de("yes"), de("no"))#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_attribut_sso" 	value="#attribut#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- Duplication des styles et accès d'un utlisateur vers un autre param : APP_LOGINID,ID_RACINE,CLIENTACCESSID,GROUP_CODE_STYLE --->
	<cffunction name="updateAffectation" output="false" description="Duplication des styles et accès" access="remote" returntype="NUMERIC">
		<cfargument name="tableau" required="true" type="array" />	
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_INTRANET.updateAffectation_V4">
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[2]#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#tableau[1]#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#SESSION.USER.CLIENTACCESSID#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.PERIMETRE.GROUP_CODE_STYLE#"/>
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
				</cfstoredproc>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getRacineRattachement" output="false" description="Donne la racine de rattachement de l'utilisateur logué et la racine du périmetre courant" access="remote" returntype="query">
			
			<cfset qlisteRacine=queryNew("RACINE,IDRACINE")>
			
			<!--- racine de l'utilisateur --->
			<cfset queryAddRow(qlisteRacine,1)>
			<cfset querySetCell(qlisteRacine,"RACINE",SESSION.USER.LIBELLE,1)>
			<cfset querySetCell(qlisteRacine,"IDRACINE",SESSION.USER.CLIENTID,1)>
			
			<cfif SESSION.USER.CLIENTID NEQ SESSION.PERIMETRE.ID_GROUPE>
			
				<!--- racine du périmètre --->
				<cfset queryAddRow(qlisteRacine,1)>
				<cfset querySetCell(qlisteRacine,"RACINE",SESSION.PERIMETRE.RAISON_SOCIALE,2)>
				<cfset querySetCell(qlisteRacine,"IDRACINE",SESSION.PERIMETRE.ID_GROUPE,2)>
			
			</cfif>
		
		<cfreturn qlisteRacine>
		
	</cffunction>

	<!---  Affecte tous les accès d'un utilisateur à un autre utilisateur  retour 1:Succes / -1:Echec                   --->
	<cffunction name="duplicateAllAccessR"	access="remote" description="Affecte les  styles et accès d'un utilisateur à un autre utilisateur " returntype="Numeric"  >
		<cfargument name="id_user_ref" type="numeric" required="true"  >
		<cfargument name="users_id_to_affect" type="Array" required="true" >
	
		<cfloop array="#users_id_to_affect#" index="id_to_affect">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.duplicateAllAccessR"  >
				<cfprocparam value="#id_user_ref#" type="in" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#id_to_affect#"  type="in" cfsqltype="CF_SQL_INTEGER">		
			<cfprocparam cfsqltype="cf_SQL_INTEGER"  type="out" variable="p_retour"/>		
			</cfstoredproc>
			
			<cfif p_retour NEQ 1>
				<cfbreak>	
			</cfif>
		</cfloop>
		
		<cfreturn p_retour >	
	</cffunction>

	
	
</cfcomponent>
