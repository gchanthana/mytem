package gestionparc.ihm.main.menu.itemmenurenderer
{
	import flash.events.EventDispatcher;
	
	import mx.utils.ObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.ihm.main.menu.ItemMenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_ENABLE;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.MDM_USER_RIGHT;
	import gestionparc.services.mdm.MDMService;

	public class AbstractItemMenuContextual extends EventDispatcher
	{

		private var _mdmSrv:MDMService=new MDMService();
		
		protected var selectedObject:AbstractMatriceParcVO;

		/**  Retourne TRUE si l'OS est ANDROID */ 
		
		public function get isDroid():Boolean
		{
			return ObjectUtil.stringCompare(SpecificationVO.getInstance().elementDataGridSelected.OS, SpecificationVO.ANDROID, true) == 0;
		}

		public function set isDroid(value:Boolean):void
		{
			// N'effectue aucune action
		}


		public function AbstractItemMenuContextual(value:AbstractMatriceParcVO=null)
		{
			selectedObject=value;
			super();
		}

		/**  Doit etre appelée explicitement après l'instanciation de( SerieItemRenderer et EquipementItemRenderer ) */
		
		public function initItemMenu():void
		{
			var r1:IRuleDisplayItem=new MDM_ENABLE();
			var r2:IRuleDisplayItem=new MDM_USER_RIGHT();

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			
			if (r1.isDisplay() && r2.isDisplay() && (ligne != null) && (!isNaN(ligne.T_IDTYPE_EQUIP)) && ((ligne.T_IDTYPE_EQUIP == 70) || (ligne.T_IDTYPE_EQUIP == 2192)))
			{
				phoneOS();
			}
			else
				dispatchEvent(new MDMDataEvent(MDMDataEvent.MDM_DATA_COMPLETE));
		}

		private function phoneOS():void
		{
			_mdmSrv.getDeviceInfo(SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI);
			_mdmSrv.myDatas.addEventListener(MDMDataEvent.MDM_DATA_COMPLETE, deviceInfoHandler);
		}

		/**
		 * cette methode permet d'ajouter dynamiquement OS et MDM_PROVIDER dans la ligne selectionnée
		 * @param mde
		 */		
		private function deviceInfoHandler(mde:MDMDataEvent):void
		{
			if (_mdmSrv.myDatas.arrListeDevice != null && _mdmSrv.myDatas.arrListeDevice.length > 0)
			{
				for each (var item:Object in _mdmSrv.myDatas.arrListeDevice)
				{
					/** Ajoute  dynamiquement l'OS dans la ligne seléctionnée du tableau*/
					
					if (String(item.NAME).toLowerCase() == "plateforme")
						SpecificationVO.getInstance().elementDataGridSelected.OS=item.VALUE;
					else if (String(item.NAME).toUpperCase() == "MDM_PROVIDER")/** Ajoute le PROVIDER dans la ligne seléctionnée du tableau */
						SpecificationVO.getInstance().elementDataGridSelected.MDM_PROVIDER=item.VALUE;
				}
			}
			dispatchEvent(mde); /** Instanciation et initialisation terminées */
		}

		/**
		 * Cette fonction créer la liste des items du menu contextuel du collaborateur
		 * selon les règles de gestion à appliquer pour chaque item.
		 */
		public function returnArrayItemMenuContextual():Array
		{
			var res:Array=new Array();
			var tmp:Array=new Array();

			tmp=fillArrayItemMenuContextual();

			var lenTmp:int=tmp.length;
			for (var i:int=0; i < lenTmp; i++) /** afficher ou ne pas afficher l'item dans le menu */
			{
				if ((tmp[i] as ItemMenuContextual).ruleDisplayItem.isDisplay())
				{
					var lg:int=res.length;
					if (i > 0 && !(res[lg - 1] as ItemMenuContextual).enabled && !(tmp[i] as ItemMenuContextual).enabled)
						res.pop();
					res.push(tmp[i]);
				}
			}
			return res
		}

		protected function fillArrayItemMenuContextual():Array
		{
			return [];
		}
	}
}
