<cfcomponent name="Inventaire">

	<!--- ::::::::::::::::::::::::::: la liste des type d'operations du cycle de vie :::::::::::::::::::::::::::: --->	
	<cffunction name="getListeTypeOperation" access="public" returntype="query" output="false">		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.CYV_LISTETYPEOPERATION">
			<cfprocresult name="p_result"/>        
		</cfstoredproc>
		<cfreturn p_result/> 		
	</cffunction>	
	
	<!--- ::::::::::::::::::::::::::: la liste des operations du cycle de vie pour un gp de lignes :::::::::::::::::::::::::::: --->	
	<cffunction name="getListeOperation" access="public" returntype="query" output="false">		
		<cfargument name="idgroupe_client" required="true" type="numeric"/>
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.CYV_LISTEOPERATION">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idgroupe_client#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>		
		<cfreturn p_result/> 		
	</cffunction>
	
	<cffunction name="getListeOperationsResi" access="public" returntype="query" output="false">		
		<cfargument name="idgroupe_client" required="true" type="numeric"/>		
		<cfset listeOpe = getListeOperation(idgroupe_client)>
		<cfquery name="qresult" dbtype="query">
			select * from listeOpe where idinv_type_ope = 1
		</cfquery>
		<cfreturn qresult/> 		
	</cffunction>
	
	<cffunction name="getListeOperationsVerif" access="public" returntype="query" output="false">		
		<cfargument name="idgroupe_client" required="true" type="numeric"/>
		<cfset listeOpe = getListeOperation(idgroupe_client)>		 
		<cfquery name="qresult" dbtype="query">
			SELECT * 
			FROM listeOpe 
			WHERE  idinv_type_ope = 41
				 OR idinv_type_ope = 61
				 OR idinv_type_ope = 62
				 OR idinv_type_ope = 82
				 OR idinv_type_ope = 83 
		</cfquery>		
		<cfreturn qresult/> 		
	</cffunction>
	
	<!--- ::::::::::::::::::::::::::: la liste des operations enfants d'une operation :::::::::::::::::::::::::::: --->	
	<cffunction name="getListeOperationsLiees" access="public" returntype="query" output="false">		
		<cfargument name="idoperation" required="true" type="numeric"/>		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.CYV_LISTEOPERATIONS_LIEES">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idoperation" value="#idoperation#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>		
		<cfreturn p_result/> 		
	</cffunction>
	
	<!--- ::::::::::::::::::::::: la demande rattachée é l'opération --->
	<cffunction name="getDemandeLiees" access="public" returntype="query" output="false">		
		<cfargument name="idoperation" required="true" type="numeric"/>		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.DEMANDE_RATACHE_OPE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idoperation" value="#idoperation#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>		
		<cfreturn p_result/> 		
	</cffunction>
	
	<!--- ::::::::::::::::::::::::::::: Le Detail d'un operation :::::::::::::::::::::::::::
	:: retourne le détail d'une operation (une ligne) 
	:: (il faudra generer un numero de ref_interne et un numero d'operation) 
	:: param in  : idinv_operations.
	:: param out : idinv_operations,
	::			idinv_type_op,libelle_operations,en_cours,commentaire,date_creation
	::			,date_modif,date_cloture,idoperations_maitre,idgroupe_client,userid,refClient,refOperateur
	:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 	
	<cffunction name="getDetailOperation" access="public" returntype="query" output="false">		
		<cfargument name="idoperation" required="true" type="numeric"/>		
				
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.DETAIL_OPE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idoperation#"/>
			<cfprocresult name="p_retour"/>     	       
		</cfstoredproc>		
		<cfreturn p_retour/>							
		 				
	</cffunction>	
		
	<!--- ::::::::::::::::::::::::::: Enregistrer les eléments de facture pour une operation :::::::::::::::::::::::::::: --->	
	<cffunction name="enregistrerFactureOperation" access="public" returntype="numeric" output="false">	
		<cfargument name="idOperation" required="true" type="numeric"/>		
		<cfargument name="listeDeProduits" required="true" type="array"/>		
		<cfset liste = arraytolist(listeDeProduits,",")>
	
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.INSERT_ILF">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_liste_produits" value="#liste#">	
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_operations" value="#idOperation#">								
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">        
		</cfstoredproc> 
		
		<cfreturn p_retour/> 
		 				
	</cffunction>
	
	<!--- ::::::::::::::::::::::::::: la liste des eléments de facture pour une operation :::::::::::::::::::::::::::: --->	
	<cffunction name="getListFactureLettreeOperation" access="public" returntype="query" output="false">	
		<cfargument name="idOperation" required="true" type="numeric"/>				
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.LIGNE_FACTURATION">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_operations" value="#idOperation#">										
		    <cfprocresult name="p_result"/>   
		</cfstoredproc>		
		<cfreturn p_result/> 				
	</cffunction>
	
	<!--- ::::::::::::::::::::::::::: supprime une liste d'eléments de facture pour une operation :::::::::::::::::::::::::::: --->	
	<cffunction name="supprimerEltFacturationOperation" access="public" returntype="numeric" output="false">	
		<cfargument name="idOperation" required="true" type="numeric"/>				
		<cfargument name="listeDetailFacture" required="true" type="array">
		
		<cfset liste = arraytolist(listeDetailFacture,",")>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.DELETE_ILF">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_liste_iddfa" value="#liste#">	
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_operations" value="#idOperation#">	
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">  
		</cfstoredproc>	
		
		<cfreturn p_retour/> 						
	</cffunction>
	
	<!--- ::::::::::::::::::::::::::::: cloturer une operation de rapprochement :::::::::::::::::::::::: --->
	<cffunction name="cloturerOperationRapp" access="public" returntype="numeric" output="false">	
		<cfargument name="idOperation" required="true" type="numeric"/>		
		<cfargument name="idAction" required="true" type="numeric"/>		
		<cfargument name="listeProduit" required="true" type="array">
		<cfargument name="listeDetailFacture" required="true" type="array">
		<cfargument name="idOpeMaitre" required="true" type="numeric">
				
		<cfset p_retour = 0>	
		<cfset opmaitre = getDetailOperation(idOpeMaitre)>
		<cfset typop = opmaitre.IDINV_TYPE_OPE[1]>	
		
		<!---typop = 1 pour operation de resiliation = dansInventaire = 0 --->
		<!---typop = 2 pour operation de commande = dansInventaire = 1 --->
		<!---typeop = ??? pour pseudo operation de gestion direct rentrer dans l'inventaire = dansInventaire = 1 --->
		<!---typeop = ??? pour pseudo operation de gestion direct sortir de l'inventaire = dansInventaire = 0 --->
		<!---typeop = ??? pour pseudo operation de commande de nouvelles lignes = dansInventaire = 1 --->
		<!---typeop = ??? pour pseudo operation de commande de nouveaux produits= dansInventaire = 1 --->		
		
		<cfset dansInventaire = "">
		<cfswitch expression="#typop#">
			<cfcase delimiters="," value="1">
				<cfset dansInventaire = 0>	
			</cfcase>
			<cfcase delimiters="," value="2">
				<cfset dansInventaire = 1>
			</cfcase>			
		</cfswitch>
		
					
		<cfset opAct = createobject("component","fr.consotel.consoview.inventaire.cycledevie.OperationAction")>		
		<cfset ar = listeProduit>
		<cfset lenght = arraylen(ar)>
		
		<cfif idAction eq 21>
			<cfset commentaire = "Validation sans lettrage">
		<cfelse>
			<cfset commentaire = "Validation avec lettrage">
		</cfif>
		
		<!--- on fait l'action pour chaque produit  --->
		<cfloop index="i" from="1" to="#lenght#">
			
			<cfset histoligne = opAct.getHistoriqueProduitAction(idOperation,ar[i])>			
			<cfset idActionPrec = histoligne.IDINV_ACTIONS[histoligne.recordcount]> 	
			<cfset tab = arraynew(1)>
			<cfset tab[1] = ar[i]>
			<cfset res = opAct.doAction(idOperation,
										tab,
										idAction,
										idActionPrec,
										commentaire,
										commentaire,
										1,
										dansInventaire,
										LSDateFormat(now(),"dd/mm/yyyy"))>					
			
		</cfloop>	
		
		
		<cfif res gt 0>
				<cfif (idAction eq 21) > <!--- VALIDER SANS LETTRER --->
					<!--- on supprime les élément qui on servie pour lettrer le rapprochement --->
					<cfset res2 = supprimerEltFacturationOperation(idOperation,listeDetailFacture)>	
				<cfelse>
					<cfset res2 = 1>			
				</cfif>
				<cfif res2 gt 0>
					<!--- si tous c'est bien passé on cloture l'opération --->
					<cfset res3 = cloturerOperation(idOperation)>
					<cfif res3 gt 0>
						<cfset p_retour = res3>;
					<cfelse>
						<cfset p_retour = p_retour -1>
					</cfif>
				<cfelse>
					<cfset p_retour = p_retour -1>
				</cfif>
		<cfelse>
			<cfset p_retour = p_retour -1>				
		</cfif>
				
		<cfreturn p_retour/> 			 	
	</cffunction>
	
	
	<cffunction name="enregistrerOperation" access="public" returntype="any" output="false">		
		
		<cfargument name="idOperation" required="true" type="numeric"/>
		<cfargument name="typeOperation" required="true" type="numeric"/>
		<cfargument name="libelleOperation" required="true" type="string"/>
		<cfargument name="flagEnCours" required="true" type="numeric"/>
		<cfargument name="commentaire" required="false" type="string"/>
		<cfargument name="date_cloture" required="true" type="string"/>
		<cfargument name="idOperationMaitre" required="true" type="numeric" default=""/>
		<cfargument name="idgroupe_client" required="true" type="numeric"/>		
		<cfargument name="listeDeProduits" required="true" type="array"/>		
		<cfargument name="idSociete" required="false" type="numeric"/>
		<cfargument name="idContact" required="false" type="numeric"/>
		<cfargument name="dateCreation" required="false" type="string"/>
		<cfargument name="reference_interne" required="false" type="string" default=""/>
		<cfargument name="ref_operateur" required="false" type="string" default=""/>
		<cfargument name="idInvActionOpPrec" required="false" type="numeric" />
		
		
		
		<cfif not isdefined("dateCreation")>
			<cfset dateCreation = lsDateFormat(now(),"yyyy/mm/dd")>		
		<cfelse>
			<cfset dateCreation = transformDate(dateCreation)>
		</cfif>
		
		<cfif not isdefined("idSociete")>
			<cfset idSociete = 0>	
		</cfif>
		
		<cfif not isdefined("idContact")>
			<cfset idContact = 0>		
		</cfif>
		
		<cfset userid = session.user.CLIENTACCESSID>	
		<cfset dateCloture = transformDate(date_cloture)>
		
		<cfset liste = arraytolist(listeDeProduits,",")>			
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.MANAGE_OPERATION">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_operations" value="#idOperation#">			
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_liste_produits" value="#liste#">						
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_type_ope" value="#typeOperation#" >
			
			<cfif idSociete gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" value="#idSociete#"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact_societe" null="true"/>
			</cfif>
			<cfif idContact gt 0>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" value="#idContact#"/>
			<cfelse>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_contact" null="true"/>
			</cfif>
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_operations" value="#libelleOperation#" >
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_en_cours" value="#flagEnCours#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_commentaire" value="#commentaire#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_date_cloture" value="#dateCloture#">			
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_date_creation" value="#dateCreation#">				
			<cfif idOperationMaitre neq 0>
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperations_maitre" value="#idOperationMaitre#">
			<cfelse>
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idoperations_maitre" value="" null="true">
			</cfif>	
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" value="#idgroupe_client#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_userid" value="#userid#">		
			
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_reference_interne" value="#reference_interne#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_ref_operateur" value="#ref_operateur#">
				
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idOpe" >        
		</cfstoredproc>
		
		<cfreturn p_idOpe/> 
	</cffunction>
	
	<!--- ::::::::::::::::::::::::::: Cloturer Operation :::::::::::::::::::::::::::: 
	:: met le flag en_cours de la table inv_operations é 0 pour l'idinv_operation passer en param 	  
	:: la date_cloture est generer automatiquement (date de l'execution de la procedure)
	:: 
	:: param in : idinv_operation
	::		 out: 1 pour Ok sinon 0
	:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 		
	<cffunction name="cloturerOperation" access="public" returntype="numeric" output="false">	
		<cfargument name="idOperation" required="true" type="numeric"/>
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.CLOTUREROPERATION">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idinv_operations" value="#idOperation#">
			<cfprocparam type="out" cfsqltype="CF_SQL_NUMERIC" variable="p_retour" >        
		</cfstoredproc>
		<cfreturn p_retour/>		
	</cffunction>		
		
	<!--- <!--- initialise la premiere action decoulant d'une creation d'operation' --->
	<cffunction name="doInitFirstAction" returntype="numeric" access="private">		
		<cfargument name="typeOperation" type="numeric" required="true">
		<cfargument name="listeProduits" type="array" required="true">		
				
		<cfswitch expression="#toString(typeOperation)#">			
			<cfcase value="1" >
				<cfset idinv_actions = 9>				
				<cfset dans_inventaire = 1>	
				<cfset etat_actuel = "1">										
				<cfset commentaire_action = "">
				<cfset commentaire_etat = "">
				<cfset idinv_op_action_prec = 0>		
			</cfcase>
			
			<cfcase value="2">
				<cfset idinv_actions = 0>				
			</cfcase>
			
			<cfcase value="3">
				<cfset idinv_actions = 0>				
			</cfcase> 
			
			<cfcase value="4">
				<cfset idinv_actions = 0>				
			</cfcase>			 
		</cfswitch>
		
		<cfset op = createObject("component","fr.consotel.consoview.inventaire.cycledevie.OperationAction")>
		<cfset res = op.doAction(listeProduits,
								idinv_actions,
								idinv_op_action_prec,								
								commentaire_action,
								commentaire_etat,
								etat_actuel,
								dans_inventaire)> 
											
		<cfreturn res>		
	</cffunction>
	 --->
	
	<cffunction name="transformeArrayCreation" access="public" returntype="array">
		<cfargument name="liste" required="true" type="array"/>
		<cfset lenght = arraylen(liste)>
		
		<cfset newlisteSoustTete = "">
		<cfset newlisteInvProduit = "">
		<cfset newlisteQte = "">
		
		<cfloop index="i" from="1" to="#lenght#">
			<cfset tmp = liste[i]>
			<cfset qte = 1>
			
			<cfif not isdefined("tmp.QUANTITE")>
				<cfset qte = 1>
			<cfelse>
				<cfset qte = tmp.QUANTITE>
			</cfif>
			
			<cfset newlisteSoustTete = newlisteSoustTete & liste[i].IDSOUS_TETES & ",">
			<cfset newlisteInvProduit = newlisteInvProduit & liste[i].IDPRODUIT_CLIENT & ",">
			<cfset newlisteQte = newlisteQte & qte & ",">
			
		</cfloop>
		
		<cfset finalListeSoustTete = left(newlisteSoustTete,len(newlisteSoustTete)-1)>
		<cfset finalListeInvProduit = left(newlisteInvProduit,len(newlisteInvProduit)-1)>
		<cfset finalListeQte = left(newlisteQte,len(newlisteQte)-1)>
			
		<cfset ar = arraynew(1)>
		<cfset ar[1] = finalListeSoustTete>
		<cfset ar[2] = finalListeInvProduit>
		<cfset ar[3] = finalListeQte>
				
		<cfreturn ar>
	</cffunction>
	
	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="">		
		<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>
		
	</cffunction>
		
	 
	<cffunction name="getListeOperateur" access="public" returntype="query" output="false">	
		<cfargument name="numero" type="numeric" required="false" default="1">	
		<cfargument name="typePerimetre" type="string" required="false" default="groupe">
		
		<cfset listeOperateur = createObject("component","fr.consotel.consoview.inventaire.cycledevie.operateur.ListeOperateur" & typePerimetre )>
		<cfset liste = listeOperateur.getListeOperateur(numero)>				
				
		<cfreturn liste>
	</cffunction>
 	

	<!--- 
		PKG_M17.rename_sous_tete(p_idsous_tete IN INTEGER,p_libelle IN VARCHAR2,p_retour OUT INTEGER)
		-1: erreur
		-2: la sous_tete existe
		1 uccès
	 --->
	<cffunction name="renommerSousTete" access="public" returntype="numeric">
		<cfargument name="pdt" type="struct" required="true">
		<cfargument name="newSousTete" type="string" required="true">
		<cfargument name="oldSousTete" type="string" required="true">
		
				
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.RENAME_SOUS_TETE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsous_tete" value="#pdt.ligneID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_libelle" value="#newSousTete#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result">		  
		</cfstoredproc>					
		<cfreturn result/>
		
		<!--- <cfargument name="idSous_tete" type="numeric" required="true">
		<cfargument name="new_sous_tete" type="string" required="true">
		<cfargument name="old_sous_tete" type="string" required="true">
		<cfargument name="prdt" type="struct" required="true">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.rename_sous_tete">										
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsous_tete" value="#idsous_tete#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_libelle" value="#new_sous_tete#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>										     
		</cfstoredproc>
 --->		
		<cfreturn result/>				
	</cffunction>
	
	<cffunction name="updateInfoCommandeCycleDeVie" access="public" returntype="numeric">			
		<cfargument name="idOperation" type="numeric" required="true">
		<cfargument name="refCommande" type="string" required="true">
		<cfargument name="refBudget" type="string" required="true">
		<cfargument name="refOperateur" type="string" required="true">
		<cfargument name="commantaire" type="string" required="true">
		
		
		<cfset var qUpdate="">
		
		<cftry>
			
			<cfquery name="qUpdate" datasource="#SESSION.OFFREDSN#" result="status">
				update
					OFFRE.INV_OPERATIONS
				set
					LIBELLE_OPERATIONS  = <cfqueryparam value="#refCommande#" cfsqltype="CF_SQL_VArchar"/>,
					REFERENCE_INTERNE = <cfqueryparam value="#refBudget#" cfsqltype="CF_SQL_VArchar"/>,
					REF_OPERATEUR = <cfqueryparam value="#refOperateur#" cfsqltype="CF_SQL_VArchar"/>,
					COMMENTAIRE = <cfqueryparam value="#commantaire#" cfsqltype="CF_SQL_VArchar"/>
				where
					IDINV_OPERATIONS = <cfqueryparam value="#idOperation#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
			
			<cfcatch type="database">
				<cfreturn -1>
			</cfcatch>
		</cftry>
		
		<cfreturn 1>
		
	</cffunction>
</cfcomponent>