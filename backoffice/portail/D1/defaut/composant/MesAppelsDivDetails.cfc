<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	<!---  --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- CONSTRUCTEUR --->
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getDivDetailAppel" access="public" output="true" returnType="string" 
				displayname="" 
				hint="Génère la div contenant le detail des appels d'une ligne / template D1" >
		
	    
	    <cfargument name="sous_tete" required="false" type="string" default="0">
		<cfargument name="idSousTete" required="false" type="numeric" default=0>
		<cfargument name="idInventaire_periode" required="false" type="numeric" default=0>

		<cfset date_deb = "">
		<cfset date_fin = "">		
		
		<cfif idSousTete eq 0>
			<cfloop query="SESSION.queryMesTerminaux">
			<cfset currentRow = SESSION.queryMesTerminaux.currentrow >
			
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" &&
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
					
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow]>
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow]>
					<cfbreak>
				</cfif>
				
			</cfloop>
		</cfif>
		
		
		
		
		<!--- COMBO CHOIX LIGNE --->
		<cfset tableAppel = '
				<!--- script initialisation DATATABLE --->
				
				
				<div style="margin-bottom:20px;">
					<div style="height:250px;">
		'>
		
		<!--- SI LE COLLABORATEUR A PLUSIEURS LIGNES (sim,sous_tete_idsous_tete renseignés)--->
		<cfif SESSION.terminauxAvailable gt 1>
		
			<cfset tableAppel &= '		
						<div class="divMaLigneComboMesaAppels" align="left" style=""> <!--- debut div select ligne --->
							<form action="##">
								<span style="font-size:14px;font-weight:600;">Ma ligne&nbsp;</span>&nbsp;
								<select id="selectLigne" class="selectMaLigneCombo" onchange="comboLigne();loadsablier();" style="">
			'>
		
		<cfloop query="SESSION.queryMesTerminaux"><!--- loop pour combo sous_tete --->
			<cfset currentRow = SESSION.queryMesTerminaux.currentrow >
		
			<cfset sousTete_ = "" >
			<cfset idSous_tete_ = "" >
			
			<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
					SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
					SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
							
				<cfset sousTete_ = SESSION.queryMesTerminaux["SOUS_TETE"][currentRow] >
				<cfset idSous_tete_ = SESSION.queryMesTerminaux["IDSOUS_TETE"][currentRow] >
			
			</cfif>
			
			
			<!--- CREATION D'UNE STRUCTURE POUR LA CONSTRUCTION DE L'URL --->
			<cfif sousTete_ neq "">
				<cfset struct = {idPage = "4",
								sous_tete = "#sousTete_#",
								idSous_tete = "#idSous_tete_#",
								idInventaire_periode = 0
				} >
				<cfset urlString = createRefURL(struct)>
				
				
				<!--- formattage numero de telephone / espace entre les chiffres --->
				<cfset numm = createObject("component","portail.class.classData.DataService").phoneFormat("#sousTete_#")>
				
				<cfif sousTete_ eq sous_tete>
					<cfset tableAppel &= '<option selected="selected" id="" value="#urlString#" >#numm#</option>'>
				<cfelse>
					<cfset tableAppel &= '<option id="" value="#urlString#" >#numm#</option>'>
				</cfif>
			</cfif>
			
		</cfloop>
		
		<cfset tableAppel &= '
							</select>
						</form>
					</div><!--- fin div select ligne --->
		'>
		
		<!--- ----------------------------------------- --->
		<!--- OU SI LE  COLLABORATEUR A UNE SEULE LIGNE --->
		<!--- ----------------------------------------- --->
		<cfelse>
			
			<cfloop query="SESSION.queryMesTerminaux">
			<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
						<cfset sous_tete = SESSION.queryMesTerminaux["SOUS_TETE"][currentRow] >
				
				</cfif>
			</cfloop>
			
			<!--- formatage numero de telephone / espace entre les chiffres --->
			<cfset numm = createObject("component","portail.class.classData.DataService").phoneFormat("#sous_tete#")>
			<cfif sous_tete eq "0"> <cfset numm = " -- -- -- -- -- "> </cfif>
			<cfset tableAppel &= '<div class="divMaLigneComboMesfactures" align="left" style="font-weight:bold;"> <!--- debut div select ligne --->
								  	Ma ligne&nbsp;&nbsp;<span style="color:##009fe3;">#numm#</span>
								  </div> <!--- fin div select ligne --->
			' >
		
		</cfif>
		
		
		
		<!--- SELECT FACTURE + TABLEAU FACTURE MENSUELLE --->
		
		<cfset SESSION.queryFact = createObject("component","portail.class.classData.DataService").getDateFactByLigne(idSousTete,SESSION.PERIMETRE.ID_GROUPE,6,SESSION.USER.GLOBALIZATION) >
		<cfset SESSION.clone_IdSous_tete = idSousTete>
		
		<cfset euro = "">
		<cfset ht = "">
		
		<cfset dateAppel = "Vos Appels du <span class='color_date_MesAppels'>-</span> au <span class='color_date_MesAppels'>-</span> " >
		
		<!--- SI AU MOINS UNE DATE DE FACTURATION EXISTTE DANS QUERY FACT --->
		<cfif SESSION.queryFact.recordcount gt 0>
		
			<cfif idInventaire_periode eq 0>
				<cfloop query = "SESSION.queryFact"><!--- loop sur query periode --->
					<cfset currentRow = SESSION.queryFact.currentrow >
					
					<cfset lastFact = SESSION.queryFact["DATE_EMISSION"][currentRow] >
					<cfset idPeriode = SESSION.queryFact["IDPERIODE_MOIS"][currentRow] >
					<cfset idInventaire_periode = SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] >
					<cfset date_deb = SESSION.queryFact["DATEDEB"][currentRow] >
					<cfset date_fin = SESSION.queryFact["DATEFIN"][currentRow] >
					<cfset num_facture = SESSION.queryFact["NUMERO_FACTURE"][currentRow] >
					<cfbreak>
				</cfloop>
				
			<cfelse>
			
				<cfloop query = "SESSION.queryFact"><!--- loop sur query periode --->
					<cfset currentRow = SESSION.queryFact.currentrow >
					
					<cfif SESSION.queryFact['IDINVENTAIRE_PERIODE'][currentRow] eq idInventaire_periode>
						
						<cfset lastFact = SESSION.queryFact["DATE_EMISSION"][currentRow] >
						<cfset idPeriode = SESSION.queryFact["IDPERIODE_MOIS"][currentRow] >
						<cfset idInventaire_periode = SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] >
						<cfset date_deb = SESSION.queryFact["DATEDEB"][currentRow] >
						<cfset date_fin = SESSION.queryFact["DATEFIN"][currentRow] >
						<cfset num_facture = SESSION.queryFact["NUMERO_FACTURE"][currentRow] >
						<cfbreak>
					</cfif>
				</cfloop>
			</cfif>
			
			
			<!--- formatage date pour le premier choix du select --->
			<cfif lastFact neq "">
				<cfset dateFact = "Votre facture du&nbsp;"&LSDateFormat(lastFact, "dd/mm/yyyy") >
				<cfset dateDeb_fact = LSDateFormat(date_deb, "dd/mm/yyyy") >
				<cfset dateFin_fact = LSDateFormat(date_fin, "dd/mm/yyyy") >
				
				<cfset dateAppel = "Vos Appels du <span class='color_date_MesAppels'>"&dateDeb_fact&"</span> au <span class='color_date_MesAppels'>"&dateFin_fact&"</span>" >
			</cfif>
			
						
			<!--- query montant factu --->
			<cfset queryMontant = createObject("component","portail.class.classData.DataService").getMontantFactureByLigne(SESSION.PERIMETRE.IDRACINE_MASTER,idSousTete,idInventaire_periode,SESSION.USER.GLOBALIZATION) >
			
			<cfset euro = "&nbsp;€">
			<cfset ht = "&nbsp;HT">
			
			<cfset tableAppel &= '
					
					<div class="divFactureMensuelle_Appel" style="">
						<div class="divGrise" style="">
						<div align="left" class="divSelectDateFacture" style=""><!--- margin-left:5px;text-indent:5px; --->
							<form action="##">
								<span style="font-size:12px;font-weight:600;">Votre facture du</span>&nbsp;
								<select id="selectFacture" class="selectMaFacture" onchange="comboFacture();loadsablier();" >
			'>
			
			<!--- loop sur query periode pour le select (sauf premier choix) --->
			<cfloop query="SESSION.queryFact">
				<cfset currentRow = SESSION.queryFact.currentrow >
				
				<cfif SESSION.queryFact["DATE_EMISSION"][currentRow] neq "" >
					
					<cfif SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] neq "" >
						<cfset idInventaire_periode_ = SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] >
					</cfif>
					<cfif SESSION.queryFact["NUMERO_FACTURE"][currentRow] neq "" >
						<cfset num_facture = SESSION.queryFact["NUMERO_FACTURE"][currentRow] >
					</cfif>
					
					<!--- formatage date pour le reste du select --->
					<cfset date_emission1 = SESSION.queryFact["DATE_EMISSION"][currentRow] ><!--- #queryFact.DATE_EMISSION# --->
					<cfset date_emission2 = LSDateFormat(date_emission1, "dd/mm/yyyy") >
					<cfset last_date = LSDateFormat(lastFact, "dd/mm/yyyy") >
					
					<!--- CREATION D'UNE STRUCTURE POUR LA CONSTRUCTION DE L'URL --->
					<cfset struct = {idPage = "4",
									sous_tete = "#sous_tete#",
									idSous_tete = "#idSousTete#",
									idInventaire_periode = "#idInventaire_periode_#"
					} >
					<cfset urlString = createRefURL(struct)>
					
					<cfif date_emission2 eq last_date >
						<cfset tableAppel &= '<option selected="selected" id="" value="#urlString#">#date_emission2# - n° de facture : #num_facture#</option>'>
					<cfelse>
						<cfset tableAppel &= '<option id="" value="#urlString#" >#date_emission2# - n° de facture : #num_facture#</option>'>
					</cfif>
				</cfif>
				
			</cfloop>
			
			
			<cfset tableAppel &= '
								</select>
							</form>
						</div>
			'>
		
		<!--- ------------------------------------------ --->
		<!--- OU SI AUCUNE DATE DE FACTU DANS QUERY FACT --->
		<!--- ------------------------------------------ --->
		<cfelse>
		
			<cfset tableAppel &= '
					
					<div class="divFactureMensuelle_Appel" style="">
						<div class="divGrise" style=""> <!--- divGrise --->
						<div align="left" class="divSelectDateFacture" style="">
							<span style="font-weight:600;">Aucune facture de moins de 6 mois disponible</span>&nbsp;
							<!--- <select id="selectFacture" class="selectMaFacture" onchange="comboFacture();loadsablier();" >
								<option id="" value="" > - </option>
							</select> --->
						</div>
			'>
		
		
		</cfif><!--- FIN IF --->
		
	    <cfset tableAppel &= '
	    		<div>
	    		<div align="left" class="divDateAppel" style="">#dateAppel#</div>
	    		<!--- form d''export csv --->
    			<div align="right" class="exportCsvAppel" style="float:left;width:10%;height:auto;">
	    			<form method="post" action="class/classData/ExportService.cfm" >
		    			<input type="hidden" name="sous_tete" value="#sous_tete#">
		    			<input type="hidden" name="idSousTete" value="#idSousTete#">
		    			<input type="hidden" name="date_deb" value="#date_deb#">
		    			<input type="hidden" name="date_fin" value="#date_fin#">
		    			<input type="hidden" name="idInventaire_periode" value="#idInventaire_periode#">
		    			<input type="submit" class="btnExportCsv" name="exportCSVAppel" value="" title="exporter en csv" >
		    		</form>
	    		</div>
	    		<div class="spacer"></div>
	    		</div>
	    		</div>
	    '>
		
		<!--- query des appels pour une ligne selon idInventaire_periode --->
		<cfset queryAppel = createObject("component","fr.consotel.consoview.M311.ficheligne.FicheLigne").getTicket(idSousTete,idInventaire_periode,"DATE_APPEL","ASC","DATE_APPEL","",1,-1) >
		
		<cfset tableAppel &= '
				
	    		<div align="center" class="" style="margin-top:20px;">
	    			
					<table id="tableSortable1" class="tableAppel" cellpadding="0" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th title="Trier par Pays appelé">Pays appelé</th>
								<th title="Trier par Numéro" class="sorting" align="center">Numéro</th>
								<th title="Trier par Date d''appel" align="center">Date</th>
								<th title="Trier par Heure d''appel" align="center">Heure</th>
								<th title="Trier par Type d''appel" align="left" style="text-indent:5px;">Type d''appel</th>
								<th title="Trier par Volume" align="right">Volume</th>
								<th title="Trier par Unité" align="right">Unité</th>
								<th title="Trier par Montant € H.T" align="right">Mnt € HT</th>
								<th title="Trier par Remise" align="right">Remise</th>
								<th title="Trier par Montant remisé" align="right">Mnt remisé</th>
								<!--- <th align="right" style="width:10px;">&nbsp;</th> --->
							</tr>
						</thead>
						<tbody>
		'>
		
		<cfloop query="queryAppel"> <!--- loop sur queryAppel --->
		
			<cfset date_call = LSDateFormat(queryAppel.DATE_APPEL, "dd/mm/yyyy") >
			<cfset montantHT = LSNumberFormat(queryAppel.COUT_OP, "______.___")>
			<cfset montantRemHT = LSNumberFormat(queryAppel.COUT_OP_REMISE, "______.___")>
			<cfset vol = LSNumberFormat(queryAppel.DUREE_FACTUREE, "_________.__")>
			
			<cfset tableAppel &= '
					<tr>
						<td>#queryAppel.PAYS_APPELE#&nbsp;</td>
						<td align="center">#queryAppel.APPELE#&nbsp;</td>
						<td align="center">#date_call#</td>
						<td align="center">#queryAppel.HEURE_APPEL#</td>
						<td align="left" style="text-indent:5px;">#queryAppel.TYPE_APPEL#&nbsp;</td>
						<td align="right">#vol#&nbsp;</td>
						<td align="right">#queryAppel.UNITE_VOLUME#</td>
						<td align="right">#montantHT##euro#</td>
						<td align="right">#queryAppel.MONTANT_REMISE##euro#</td>
						<td align="right">#montantRemHT##euro#</td>
					</tr>
					
			'>
			
		</cfloop><!--- fin loop --->
		
		<cfset tableAppel &= '
						</tbody>
						<tfoot>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tfoot>
					</table>
				</div>
	    '>
		
		
		<cfreturn tableAppel >
	</cffunction>
	
</cfcomponent>