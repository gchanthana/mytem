package gestionparc.services.dissocier
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import mx.rpc.AbstractOperation;

	public class ActionDissocierServices
	{
		public var myDatas:ActionDissocierDatas;
		public var myHandlers:ActionDissocierHandlers;

		private var urlBackoffice:String="fr.consotel.consoview.M111.actions.Dissocier";

		public function ActionDissocierServices()
		{
			myDatas=new ActionDissocierDatas();
			myHandlers=new ActionDissocierHandlers(myDatas);
		}

		/**----------------------------------------------------- COLLABORATEUR ------------------------------------------------- */

		/* ACTION : REPRENDRE EQUIPEMENT */
		public function reprendreEqpFromCollab1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreEqpFromCollab1", myHandlers.reprendreEqpFromCollab1ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function reprendreEqpFromCollab2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreEqpFromCollab2", myHandlers.reprendreEqpFromCollab2ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function reprendreEqpFromCollab3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreEqpFromCollab3", myHandlers.reprendreEqpFromCollab3ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : REPRENDRE LIGNE SIM */
		public function reprendreLigneSimFromCollab1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreLigneSimFromCollab1", myHandlers.reprendreLigneSimFromCollab1ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
		/** reprendre ligne ext du collaborateur **/
		
		public function reprendreLigneExtFromCollab(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreLigneExtFromCollab1", myHandlers.reprendreLigneExtFromCollabResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		
		public function reprendreLigneExtFromCollab2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreLigneExtFromCollab2", myHandlers.reprendreLigneExtFromCollabResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}

		public function reprendreLigneSimFromCollab2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreLigneSimFromCollab2", myHandlers.reprendreLigneSimFromCollab2ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function reprendreLigneSimFromCollab3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreLigneSimFromCollab3", myHandlers.reprendreLigneSimFromCollab3ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
		/** reprendre que l'extension */
		public function reprendreExtFromCollab(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreExtFromCollab", myHandlers.reprendreLigneExtFromCollabResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : REPRENDRE LIGNE */
		public function reprendreLigneFromCollab1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreLigneFromCollab1", myHandlers.reprendreLigneFromCollab1ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function reprendreLigneFromCollab2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"reprendreLigneFromCollab2", myHandlers.reprendreLigneFromCollab2ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function reprendreLigneFromCollab3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"reprendreLigneFromCollab3", myHandlers.reprendreLigneFromCollab3ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		/** ----------------------------------------------- EQUIPEMENT -------------------------------------------------- */

		/* ACTION : DISSOCIER COLLAB */
		public function dissocierCollabFromEqp1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierCollabFromEqp1", myHandlers.dissocierCollabFromEqp1ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierCollabFromEqp2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierCollabFromEqp2", myHandlers.dissocierCollabFromEqp2ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierCollabFromEqp3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierCollabFromEqp3", myHandlers.dissocierCollabFromEqp3ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierCollabFromEqp4(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierCollabFromEqp4", myHandlers.dissocierCollabFromEqp4ResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		/* DISSOCIER LIGNE SIM */
		public function dissocierLigneSimFromEqp1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneSimFromEqp1", myHandlers.dissocierLigneSimFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}	

		public function dissocierLigneSimFromEqp2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneSimFromEqp2", myHandlers.dissocierLigneSimFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierLigneSimFromEqp3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneSimFromEqp3", myHandlers.dissocierLigneSimFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
	
		/** dissocier ligne extension et equipement*/
		public function dissocierLigneExtFromEqp1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneExtFromEqp1", myHandlers.dissocierLigneExtFromEqpResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		
		public function dissocierLigneExtFromEqp2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneExtFromEqp2", myHandlers.dissocierLigneExtFromEqpResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		
		public function dissocierLigneExtFromEqp3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneExtFromEqp3", myHandlers.dissocierLigneExtFromEqpResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		
		/** dissocier extension et equipement*/
		public function dissocierExtFromEqp(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierExtFromEqp", myHandlers.dissocierLigneFromEqpResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		

		/* DISSOCIER LIGNE */
		public function dissocierLigneFromEqp1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneFromEqp1", myHandlers.dissocierLigneFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierLigneFromEqp2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneFromEqp2", myHandlers.dissocierLigneFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierLigneFromEqp3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierLigneFromEqp3", myHandlers.dissocierLigneFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		
		
		/* DISSOCIER SOUS EQP */
		public function dissocierSsEqpFromEqp1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierSousEqpFromEqp1", myHandlers.dissocierSousEqpFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierSsEqpFromEqp2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierSousEqpFromEqp2", myHandlers.dissocierSousEqpFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierSsEqpFromEqp3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierSousEqpFromEqp3", myHandlers.dissocierSousEqpFromEqpResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : DISSOCIER COLLAB */
		public function dissocierCollabFromLigne(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierCollabFromLigne", myHandlers.dissocierCollabFromLigneResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : DISSOCIER EQP */
		public function dissocierEqpFromLigne1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromLigne1", myHandlers.dissocierEqpFromLigneResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		// dans le cas d'une ligne mobile, on dissocie l'équipement de la sim
		public function dissocierEqpFromLigne2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromLigne2", myHandlers.dissocierEqpFromLigneResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : DISSOCIER EQP D'UNE LIGNE FIXE */
		public function dissocierSimFromLigne(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierSimFromLigne", myHandlers.dissocierSimFromLigneResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
		public function dissocierExtFromLigne(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierExtFromLigne", myHandlers.dissocierExtFromLigneResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : DISSOCIER EQP D'UNE LIGNE MOBILE */
		public function dissocierEqpFromLigneSim1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromLigneSim1", myHandlers.dissocierSimFromLigneResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierEqpFromLigneSim2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromLigneSim2", myHandlers.dissocierSimFromLigneResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierEqpFromLigneSim3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromLigneSim3", myHandlers.dissocierSimFromLigneResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : DISSOCIER COLLAB */
		public function dissocierCollabFromSim1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierCollabFromSim1", myHandlers.dissocierCollabFromSimResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierCollabFromExt1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierCollabFromExt1", myHandlers.dissocierCollabFromExtResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		
		public function dissocierCollabFromSim2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierCollabFromSim2", myHandlers.dissocierCollabFromSimResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
		public function dissocierCollabFromExt2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierCollabFromExt2", myHandlers.dissocierCollabFromExtResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		

		public function dissocierCollabFromSim3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierCollabFromSim3", myHandlers.dissocierCollabFromSimResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
		public function dissocierCollabFromExt3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierCollabFromExt3", myHandlers.dissocierCollabFromExtResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}

		/* ACTION : DISSOCIER EQP */
		public function dissocierEqpFromSim1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierEqpFromSim1", myHandlers.dissocierEqpFromSimResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierEqpFromExt1(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice, 
				"dissocierEqpFromExt1", myHandlers.dissocierEqpFromExtResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}

		
		public function dissocierEqpFromSim2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromSim2", myHandlers.dissocierEqpFromSimResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
		public function dissocierEqpFromExt2(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromExt2", myHandlers.dissocierEqpFromExtResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}


		public function dissocierEqpFromSim3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromSim3", myHandlers.dissocierEqpFromSimResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}

		public function dissocierEqpFromExt3(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierEqpFromExt3", myHandlers.dissocierEqpFromExtResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
		
		/* ACTION : DISSOCIER LIGNE */
		public function dissocierLigneFromSim(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierLigneFromSim", myHandlers.dissocierLigneFromSimResultHandler);

			RemoteObjectUtil.callService(op, obj);
		}
		
		/** dissocier une ligne d'une extension */
		public function dissocierLigneFromExt(obj:Object):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, urlBackoffice,
				"dissocierLigneFromExt", myHandlers.dissocierLigneFromSimResultHandler);
			
			RemoteObjectUtil.callService(op, obj);
		}
	}
}
