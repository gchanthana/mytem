package gestionparc.ihm.fiches.ficheEntity
{
	import composants.util.ConsoviewAlert;

	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import gestionparc.entity.EmplacementVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.site.SiteServices;
	import gestionparc.utils.gestionparcMessages;

	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.Validator;

	[Bindable]
	public class FicheSiteImpl extends TitleWindow
	{

		public var tiLibelleSite:TextInput;
		public var tiAdresse:TextInput;
		public var tiCodePostal:TextInput;
		public var tiVille:TextInput;
		public var tiReference:TextInput;
		public var tiCodeInterne:TextInput;
		public var tiLibelle:TextInput;
		public var tiBatiment:TextInput;
		public var tiEtage:TextInput;
		public var tiSalle:TextInput;
		public var tiCouloir:TextInput;
		public var tiArmoire:TextInput;

		public var taCommentaires:TextArea;

		public var cbPays:ComboBox;

		public var valLibelleSite:Validator;
		public var valAdresse:Validator;
		public var valCodePostale:Validator;
		public var valVille:Validator;
		public var valPays:Validator;

		public var btValid:Button;
		public var btCancel:Button;
		public var btAddEmplacement:Button;

		public var dgEmplacement:DataGrid;

		public var idSite:int=-1;
		public var isNewSite:Boolean=false;

		private var myServiceSite:SiteServices=null;

		public function FicheSiteImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialisation générale de cette classe  :
		 * - Instanciation du service lié aux sites.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 */
		private function init(event:FlexEvent):void
		{
			myServiceSite=new SiteServices();

			if (!isNewSite)
			{
				var objToServiceGet:Object=new Object();
				objToServiceGet.idSite=idSite;

				myServiceSite.getSiteEmplacement(objToServiceGet);
			}

			initListener();
			initDisplay();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			// listeners lié aux boutons de FicheSiteImpl
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);

			btAddEmplacement.addEventListener(MouseEvent.CLICK, onAddEmplacementClickHandler);
			this.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV, onSupprimerEltNavHandler)

			// listeners permettant l'activation du bouton "valider"
			tiAdresse.addEventListener(Event.CHANGE, activeValidButton);
			tiCodePostal.addEventListener(Event.CHANGE, activeValidButton);
			tiLibelleSite.addEventListener(Event.CHANGE, activeValidButton);
			tiVille.addEventListener(Event.CHANGE, activeValidButton);
			taCommentaires.addEventListener(Event.CHANGE, activeValidButton);
			cbPays.addEventListener(ListEvent.CHANGE, activeValidButton);

			// listeners lié aux services
			myServiceSite.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_LOADED, infoFicheSiteLoadedHandler);
		}

		/**
		 * Initialise l'affichage de cette IHM.
		 */
		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btValid.enabled=false;
		}

		/**
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 */
		private function infoFicheSiteLoadedHandler(event:gestionparcEvent):void
		{
			//trace("données fiche site chargées");

			// maj des infos concernant le site
			tiLibelleSite.text=myServiceSite.myDatas.site.libelleSite;
			tiAdresse.text=myServiceSite.myDatas.site.adresse1;
			tiCodePostal.text=myServiceSite.myDatas.site.codePostale;
			tiVille.text=myServiceSite.myDatas.site.ville;
			tiCodePostal.text=myServiceSite.myDatas.site.codePostale;
			taCommentaires.text=myServiceSite.myDatas.site.commentaires;
			tiCodeInterne.text=myServiceSite.myDatas.site.codeInterneSite;
			tiReference.text=myServiceSite.myDatas.site.refSite;

			if (myServiceSite.myDatas.site.idPays > 0)
			{
				var listePaysLen:int=CacheDatas.listePays.length;
				for (var i:int=0; i < listePaysLen; i++)
				{
					if (CacheDatas.listePays[i].PAYSCONSOTELID == myServiceSite.myDatas.site.idPays)
					{
						cbPays.selectedIndex=i;
						break;
					}
				}
			}
			else
			{
				cbPays.selectedIndex=(CacheDatas.listePays.length > 0) ? 0 : -1;
			}

			dgEmplacement.dataProvider=myServiceSite.myDatas.site.listeEmplacement;
		}

		/**
		 * Active le bouton "Valider".
		 */
		private function activeValidButton(event:Event):void
		{
			btValid.enabled=true;
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Valide la fiche ligne.
		 * Si la ligne existait auparavant, alors il en résulte de sa mise à jour.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			// tester les validators
			if (runValidators())
			{
				myServiceSite.setSiteEmplacement(createObjToServiceSet());
				myServiceSite.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, ficheSiteUploadHandler);
			}
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant le site ont été mise à jour.
		 */
		private function ficheSiteUploadHandler(event:gestionparcEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, true));
			closePopup(null);
		}

		/**
		 * Créé l'objet non typé que l'on va passer au service
		 */
		private function createObjToServiceSet():Object
		{
			var obj:Object=new Object();

			setInfoSiteVO();
			obj.mySite=myServiceSite.myDatas.site;
			obj.xmlEmplacement=createXmlEmplacement();
			obj.idPool=SpecificationVO.getInstance().idPool;

			return obj;
		}

		/**
		 * Met à jour les données du site.
		 */
		private function setInfoSiteVO():void
		{
			if (isNewSite)
			{
				myServiceSite.myDatas.site=new SiteVO();
				myServiceSite.myDatas.site.idSite=-1;
			}
			myServiceSite.myDatas.site.libelleSite=tiLibelleSite.text;
			myServiceSite.myDatas.site.adresse1=tiAdresse.text;
			myServiceSite.myDatas.site.adresse2="";
			myServiceSite.myDatas.site.refSite=tiReference.text;
			myServiceSite.myDatas.site.codeInterneSite=tiCodeInterne.text;
			myServiceSite.myDatas.site.codePostale=tiCodePostal.text;
			myServiceSite.myDatas.site.ville=tiVille.text;
			myServiceSite.myDatas.site.commentaires=taCommentaires.text;
			myServiceSite.myDatas.site.idPays=cbPays.selectedItem.PAYSCONSOTELID;
			myServiceSite.myDatas.site.pays=cbPays.selectedItem.PAYS;
		}

		/**
		 * Créé le xml correspondant à la liste des emplacements
		 */
		private function createXmlEmplacement():String
		{
			var myXML:String="";
			var lenDataGridEmplacement:int=0;

			if (dgEmplacement.dataProvider)
			{
				lenDataGridEmplacement=(dgEmplacement.dataProvider as ArrayCollection).length;
			}

			myXML+="<emplacements>";
			for (var j:int=0; j < lenDataGridEmplacement; j++)
			{
				myXML+="<emplacement>";
				myXML+="<libelle>" + (dgEmplacement.dataProvider[j] as EmplacementVO).libelleEmplacement + "</libelle>";
				myXML+="<batiment>" + (dgEmplacement.dataProvider[j] as EmplacementVO).batiment + "</batiment>";
				myXML+="<etage>" + (dgEmplacement.dataProvider[j] as EmplacementVO).etage + "</etage>";
				myXML+="<salle>" + (dgEmplacement.dataProvider[j] as EmplacementVO).salle + "</salle>";
				myXML+="<couloir>" + (dgEmplacement.dataProvider[j] as EmplacementVO).couloir + "</couloir>";
				myXML+="<armoire>" + (dgEmplacement.dataProvider[j] as EmplacementVO).armoire + "</armoire>";
				myXML+="</emplacement>";
			}
			myXML+="</emplacements>";

			return myXML;
		}

		/**
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 */
		private function runValidators():Boolean
		{
			var validators:Array=[valLibelleSite, valAdresse, valCodePostale, valPays, valVille];
			var results:Array=null;

			results=Validator.validateAll(validators);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'), null);
				return false;
			}
			else
			{
				return true;
			}
		}

		/**
		 * Ajoute un emplacement au datagrid
		 */
		private function onAddEmplacementClickHandler(event:MouseEvent):void
		{
			if (tiLibelle.text)
			{
				var obj:Object=new Object();
				obj.libelle=tiLibelle.text;
				obj.batiment=tiBatiment.text;
				obj.etage=tiEtage.text;
				obj.salle=tiSalle.text;
				obj.couloir=tiCouloir.text;
				obj.armoire=tiArmoire.text;

				var myEmplacementVO:EmplacementVO=new EmplacementVO();
				myEmplacementVO=myEmplacementVO.fillEmplacementVO(obj);

				if (dgEmplacement.dataProvider == null)
				{
					dgEmplacement.dataProvider=new ArrayCollection();
				}
				(dgEmplacement.dataProvider as ArrayCollection).addItem(myEmplacementVO);
				activeValidButton(null);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Veuillez_renseigner_les_champs_obligatoi'), ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
		}

		/**
		 * Suppression d'un emplacement du datagrid
		 */
		private function onSupprimerEltNavHandler(event:gestionparcEvent):void
		{
			var lenDataProvider:int=(dgEmplacement.dataProvider as ArrayCollection).length;

			for (var i:int=0; i < lenDataProvider; i++)
			{
				if ((dgEmplacement.dataProvider as ArrayCollection)[i] == dgEmplacement.selectedItem)
				{
					(dgEmplacement.dataProvider as ArrayCollection).removeItemAt(i);
					break;
				}
			}
			activeValidButton(null);
		}

		public function getObjSiteServices():SiteServices
		{
			return myServiceSite;
		}
	}
}
