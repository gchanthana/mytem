<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement16"
extends="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement" hint="Implémentation de l'évènement 16">
	<cffunction access="private" name="requiredProperties" returntype="Struct" hint="Retourne la liste des propriétés requises par l'évènement
	Reprend la liste de l'implémentation parent et ajoute les propriétés requises suivantes : IDRACINE, IDCONTRAT, BOOL_CREATION">
		<cfset var requiredProps=SUPER.requiredProperties()>
		<cfset structAppend(requiredProps,{IDRACINE="Identifiant de la racine", IDCONTRAT="N.D", BOOL_CREATION="N.D"},TRUE)>
		<cfreturn requiredProps>
	</cffunction>
	
	<cffunction access="private" name="getReportList" returntype="query" hint="Retourne la liste des rapports à exécuter et détermine les paramètres du rapport BIP">
		<!--- Structure contenant les paramètres qui ont été fournis SOIT dans l'URL SOIT via getInstance(Struct) --->
		<cfset var runProps=getRunProperties()>
		<!--- Liste de diffusion --->
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.type_16_liste">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#VAL(runProps.IDRACINE)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#VAL(runProps.IDCONTRAT)#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#VAL(runProps.BOOL_CREATION)#">
			<cfprocresult name="qEvenement">
		</cfstoredproc>
		<cfreturn qEvenement>
	</cffunction>
</cfcomponent>