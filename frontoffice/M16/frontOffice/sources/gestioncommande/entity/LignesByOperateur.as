package gestioncommande.entity
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.describeType;
	
	import mx.collections.ArrayCollection;
	
	import composants.util.DateFunction;
	
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.EligibiliteService;

	public class LignesByOperateur extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var ELIGIBILITE				:String = '';
		public var OPERATEUR				:String = '';
		public var SOUS_COMPTE				:String = '';
		public var COMPTE_FACTURATION 		:String = '';
		public var LIGNE					:String = '';
		public var IMEI 					:String = '';
		public var COLLABORATEUR			:String = '';
		public var CODE_INTERNE				:String = '';
		
		public var IDCOLLABORATEUR			:int = 0;
		public var IDPOOL					:int = 0;
		public var IDSOUS_TETE				:int = 0;
		public var IDCOMPTE_FACTURATION 	:int = 0;
		public var IDSOUS_COMPTE			:int = 0;
		public var RESTE					:int = 0;
		public var IDOPERATEUR				:int = 0;
		public var IDEMPL					:int = 0;
		public var IDETAT_LIGNE				:int = 0;
		
		public var ETAT_ELIGIBILITE			:int = 0;
		
		public var FIN_PERIODE_CONTRACTUELLE:Date = null;
		public var DATE_ELLIGIBILITE		:Date = null;
		public var FPC						:Date = null;		
		
		public var DATE_ELIGIBILITE_STRG	:String = '';
		
		public var MONTANT_MENSUEL_PEN		:Number	= 0;
		public var FRAIS_FIXE_RESILIATION	:Number	= 0;
		public var FRAIS_TOTAL_RESIL		:Number	= 0;
		
		public var NBRECORD					:int = 20;

		public var SELECTED					:Boolean = false;
		
		public var ADDOPTIONS				:ArrayCollection = new ArrayCollection();
		public var REMOPTIONS				:ArrayCollection = new ArrayCollection();
		
		public var classProcEligibilite		:EligibiliteService = new EligibiliteService();
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//	
		
		public function LignesByOperateur()
		{
			classProcEligibilite.addEventListener(CommandeEvent.ELIGIBLE_COMPTE_LIGNE, eligibiliteLignesByLignesAndGetCompteSousCompteHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					COPY DE L'OBJECT EN COURS
		//--------------------------------------------------------------------------------------------//	
		
		public function copyLignesByOperateur():LignesByOperateur
		{
			var copyLignes	:LignesByOperateur = new LignesByOperateur();
			var classInfo 	:XML = describeType(this);
			
			for each (var v:XML in classInfo..accessor)
			{
				if (this.hasOwnProperty(v.@name))
				{
					copyLignes[v.@name] = this[v.@name];
				}
			}   
			
			return copyLignes;         				 		
		}
		
		private function eligibiliteLignesByLignesAndGetCompteSousCompteHandler(cmde:CommandeEvent):void
		{
			if(this.ETAT_ELIGIBILITE == 0)
			{
				this.DATE_ELLIGIBILITE 			= classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE;
				this.DATE_ELIGIBILITE_STRG		= DateFunction.formatDateAsString(this.DATE_ELLIGIBILITE as Date);
				this.ELIGIBILITE 				= classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE;
				this.RESTE 						= classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.NB_JR_FIN_LG;
				this.FIN_PERIODE_CONTRACTUELLE 	= classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.FIN_PERIODE_CONTRACTUELLE;
				this.FPC 						= classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.FPC;
				this.ETAT_ELIGIBILITE			= 1;
				
				if(classProcEligibilite.isElegibilitePresent)
				{
					this.MONTANT_MENSUEL_PEN 	= classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.MONTANT_MENSUEL_PEN;
					this.FRAIS_FIXE_RESILIATION = classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.FRAIS_FIXE_RESILIATION;
					this.FRAIS_TOTAL_RESIL 		= classProcEligibilite.eligiblecomptesouscompte.INFOSELIGIBILITE.FRAIS_TOTAL_RESIL;
				}
				else
				{
					this.MONTANT_MENSUEL_PEN 	= 0;
					this.FRAIS_FIXE_RESILIATION = 0;
					this.FRAIS_TOTAL_RESIL 		= 0;
					this.ELIGIBILITE 			= '0';
				}
			}
			
			dispatchEvent(new Event('INFOS_LIGNE_REFRESH', true));
		}
		
		
		

	}
}