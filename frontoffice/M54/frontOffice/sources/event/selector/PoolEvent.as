package event.selector
{
	import flash.events.Event;

	public class PoolEvent extends Event
	{
		public static const GET_VALUE_POOL_EVENT :String ="getValuePoolEvent";
		public static const VALIDER_VALUE_POOL_EVENT :String ="validerValuePoolEvent";
		
		public function PoolEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new PoolEvent(type, bubbles, cancelable);
		}
	}
}