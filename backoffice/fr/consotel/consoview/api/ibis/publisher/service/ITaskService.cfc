<cfinterface displayName="fr.consotel.consoview.api.ibis.publisher.service.ITaskService">
	<cffunction name="getDefaultXmlReportTask" access="public" output="false" returntype="xml">
	</cffunction>
	
	<cffunction name="run" access="public" output="false" returntype="Struct">
		<cfargument name="userIdCv" type="numeric" required="true">
		<cfargument name="idgestionnaire" type="numeric">
		<cfargument name="xmlReportTaskParam" type="xml">
	</cffunction>
</cfinterface>