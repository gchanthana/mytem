<!--- TODO : Effectuer un test avec un nom de fichier FTP comportant des accents --->
<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.FTP" extends="fr.consotel.api.ibis.publisher.delivery.Sync"
hint="Stratégie identifiée par le type FTP permettant de générer un reporting sur un serveur FTP qui est prédéfini dans BIP
Le serveur par défaut est pelican.consotel.fr et le répertoire celui de l'utilisateur container. Le séparateur de chemin utilisé pour FTP est /
Si l'évènement passé à handleReportingEvent() est de type fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent : Une exception est levée
Les propriétés suivantes doivent etre retournée par la méthode getDeliveryProperties() de la stratégie utilisée pour l'exécution du reporting :
- FILE_NAME : Nom du fichier à générer sans l'extension
- FILE_EXT : Extension du fichier à générer (sans le séparateur point)
Les propriétés suivantes sont facultatives :
- FTP_SERVER : Nom d'un serveur FTP tel qu'il est enregistré dans la configuration du serveur BI Publisher (Par défaut : pelican.consotel.fr)
- FTP_USER : Login utilisateur FTP (Par défaut : services)
- FTP_PWD : Mot de passe utilisateur FTP (Par défaut : Celui de l'utilisateur services)
- SFTP : TRUE pour utiliser une connexion SFTP et FALSE sinon (Par défaut : FALSE)
- FTP_DIR : Chemin relatif du répertoire FTP par rapport au répertoire de FTP_USER et sans le séparateur de chemin en dernier caractère
Le répertoire par défaut est celui de l'utilisateur FTP_USER (Par défaut : HOME DIRECTORY de container)
Aucune action n'est effectuée lorsque le type de cette stratégie est utilisé pour la propriété DIFFUSION
L'implémentation IDeliveryEvent dispatché par cette stratégie est : fr.consotel.api.ibis.publisher.handler.event.FtpEvent">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IDelivery">
		<!--- Constructeur parent --->
		<cfset SUPER.createInstance()>
		<!--- Propriétés de diffusion FTP --->
		<cfset VARIABLES.FTP = {
			SERVER="pelican.consotel.fr", USER="services", PWD="services", SFTP=FALSE, PATH_SEP="/", EXT_SEP=".", DIR=""
		}>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getDeliveryProperties" returntype="Struct"
	hint="Retourne les propriétés retournées par la méthode de la classe parent en spécifiant des valeurs par défaut pour celles qui ne sont pas renseignées">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<!--- Structure contenant toutes les propriétés fournies par le reporting et celles ajoutée par la méthode de la classe parent --->
		<cfset var deliveryProperties=SUPER.getDeliveryProperties(reportingImpl)>
		<!--- MAJ des propriétés et paramètres FTP par rapport aux valeurs fournies par le reporting --->
		<cfif (NOT structKeyExists(deliveryProperties,"FTP_SERVER")) OR (LEN(TRIM(deliveryProperties.FTP_SERVER)) EQ 0)>
			<cfset deliveryProperties.FTP_SERVER=VARIABLES.FTP["SERVER"]>
		</cfif>
		<cfif (NOT structKeyExists(deliveryProperties,"FTP_USER")) OR (LEN(TRIM(deliveryProperties.FTP_USER)) EQ 0)>
			<cfset deliveryProperties.FTP_USER=VARIABLES.FTP["USER"]>
		</cfif>
		<cfif (NOT structKeyExists(deliveryProperties,"FTP_PWD")) OR (LEN(TRIM(deliveryProperties.FTP_PWD)) EQ 0)>
			<cfset deliveryProperties.FTP_PWD=VARIABLES.FTP["PWD"]>
		</cfif>
		<cfif (NOT structKeyExists(deliveryProperties,"SFTP")) OR (LEN(TRIM(deliveryProperties.SFTP)) EQ 0)>
			<cfset deliveryProperties.SFTP=VARIABLES.FTP["SFTP"]>
		</cfif>
		<cfif NOT structKeyExists(deliveryProperties,"FTP_DIR")>
			<cfset deliveryProperties.FTP_DIR=VARIABLES.FTP["DIR"]>
		</cfif>
		<cfreturn deliveryProperties>
	</cffunction>

	<cffunction access="public" name="getDeliveryRequest" returntype="Struct"
	hint="Retourne la structure retournée par la méthode de la classe parent en définissant les propriétés FTP à partir de celles de getDeliveryProperties()
	Une exception est levée si les propriétés retournées par getDeliveryProperties() ne sont pas validées par une méthode de validation interne
	Retourne la structure avec les valeurs des propriétés FTP définies ou mises à jour">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfset var reportingIml=ARGUMENTS["reporting"]>
		<cfset var scheduleRequest=SUPER.getDeliveryRequest(reportingIml)>
		<!--- Propriétés d'exécution de cette stratégie de diffusion --->
		<cfset var deliveryProperties=getDeliveryProperties(reportingIml)>
		<!--- Valide les propriétés retournées par getDeliveryProperties() --->
		<cfset validateDeliveryProperties(deliveryProperties)>
		<!--- Chemin relatif du fichier FTP correspondant au rapport généré --->
		<cfset var remoteFilePath=deliveryProperties["FILE_NAME"] & VARIABLES.FTP["EXT_SEP"] & deliveryProperties["FILE_EXT"]>
		<!--- Si le répertoire à utiliser est celui de l'utilisateur FTP_USER : MAJ du chemin relatif du fichier FTP --->
		<cfif LEN(TRIM(deliveryProperties["FTP_DIR"])) GT 0>
			<cfset remoteFilePath=deliveryProperties["FTP_DIR"] & VARIABLES.FTP["PATH_SEP"] & remoteFilePath>
		</cfif>
		<!--- Ajoute les propriétés DeliveryRequest spécifiques à la diffusion FTP --->
		<cfset scheduleRequest.deliveryRequest = {
			ftpOption = {
				ftpServerName=deliveryProperties["FTP_SERVER"], sftpOption=deliveryProperties["SFTP"], remoteFile=remoteFilePath,
				ftpUserName=deliveryProperties["FTP_USER"], ftpUserPassword=deliveryProperties["FTP_PWD"]
			}
		}>
		<cfreturn scheduleRequest>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion --->
	<cffunction access="public" name="dispatchEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.FtpEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.FtpEvent">
	</cffunction>
	
	<cffunction access="private" name="deliveryEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.FtpEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.FtpEvent">
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.FTP --->
	<cffunction access="private" name="validateDeliveryProperties" returntype="void"
	hint="Valide les propriétés retournée par getDeliveryProperties() et lève une exception si elles ne sont pas validées
	Cette méthode est appelée par getDeliveryRequest() pour valider les propriétés retournées par getDeliveryProperties()
	Une exception est levée si une des conditions suivantes est vérifiée :
	- Une des propriétés suivantes est absente ou non renseignée (i.e : Chaine vide) : FILE_NAME, FILE_EXT
	- Une des propriétés suivantes est un chemin de fichier : FILE_NAME, FILE_EXT">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Structure retournée par getDeliveryProperties()">
		<cfset var deliveryProperties=ARGUMENTS["deliveryProps"]>
		<!--- Vérifie que les propriétés FILE_NAME et FILE_EXT sont présentes --->
		<cfif (NOT structKeyExists(deliveryProperties,"FILE_NAME")) OR (NOT structKeyExists(deliveryProperties,"FILE_EXT"))>
			<cfthrow attributecollection="#getErrorProperties()#" message="La propriété FILE_NAME et/ou FILE_EXT n'est pas fournie pour la stratégie FTP"
				detail="Les proriétés FILE_NAME et FILE_EXT indiquent respectivement le nom du fichier FTP et son extension">
		</cfif>
		<!--- Vérifie que les propriétés FILE_NAME ou FILE_EXT sont renseignées --->
		<cfif (LEN(TRIM(deliveryProperties["FILE_NAME"])) EQ 0) OR (LEN(TRIM(deliveryProperties["FILE_EXT"])) EQ 0)>
			<cfthrow attributecollection="#getErrorProperties()#" message="La propriété FILE_NAME et/ou FILE_EXT n'est pas renseignée pour la stratégie FTP"
				detail="Les proriétés FILE_NAME et FILE_EXT indiquent respectivement le nom du fichier FTP et son extension et ne peuvent pas etre des chaines vides">
		</cfif>
		<!--- Vérifie que FILE_NAME ou FILE_EXT n'est pas un chemin de fichier --->
		<cfif (arrayLen(listToArray(deliveryProperties.FILE_NAME,"/",TRUE)) GT 1) OR (arrayLen(listToArray(deliveryProperties.FILE_EXT,"/",TRUE)) GT 1)>
			<cfthrow attributecollection="#getErrorProperties()#" message="La propriété FILE_NAME ou FILE_EXT ne doit pas etre un chemin de fichier">
		</cfif>
	</cffunction>
</cfcomponent>