package univers.parametres.perimetres.rightSide.assignmentComponent
{
    import composants.access.perimetre.PerimetreTreeItemRendererRightTree;
    import composants.parametres.perimetres.OrgaStructure;
    import composants.parametres.perimetres.specialTree;
    import composants.util.ConsoviewAlert;
    import flash.events.Event;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.core.ClassFactory;
    import mx.events.ListEvent;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    import univers.parametres.perimetres.main;

    public class AssignOrgaCmp extends OrgaStructure
    {
        private var _mainRef:main;
        public var _linesType:Number;
        public static const SELECTION_CHANGE:String = "selectionHasChanged";

        [Bindable] public function set linesType(lt:Number):void
        {
            _linesType = lt;
        }

        public function get linesType():Number
        {
            return _linesType;
        }
        private var _isMemeOrga:Boolean = false;

        [Bindable] public function get isMemeOrga():Boolean
        {
            return _isMemeOrga;
        }

        public function set isMemeOrga(b:Boolean):void
        {
            _isMemeOrga = b;
        }
        private var _modeRestreint:Boolean = false;

        public function get modeRestreint():Boolean
        {
            return _modeRestreint
        }

        public function set modeRestreint(b:Boolean):void
        {
            _modeRestreint = b;
        }

        public function AssignOrgaCmp()
        {
        }

        override public function initIHM(event:Event):void
        {
            super.initIHM(event);
            treePerimetre.y = 30;
        /*treePerimetre.height = 220;*/
        }

        override public function searchNode(nodeName:String):void
        {
            parentDocument.clearGridLines();
            switch(_linesType)
            {
                case 1:
                    searchNodesByFilter("searchNotAssignNodes", nodeName);
                    break;
                case 2:
                    searchNodesByFilter("searchAssignNodes", nodeName);
                    break;
                default:
                    var selectedOrgaID:Number = cmbOrganisation.selectedItem.IDGROUPE_CLIENT;
                    var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
                                                                                        "searchNode", searchNodeResult);
                    RemoteObjectUtil.callService(op, selectedOrgaID, nodeName, 0, _mainRef.getSelectedDate());
            }
        }

        private function searchNodesByFilter(method:String, txtSearch:String):void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var selectedOrgaRefId:Number = _mainRef.getSelectedOrganisation();
            var selectedOrgaID:Number = getSelectedOrganisationValue();
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
                                                                                method, searchNodeResult);
            RemoteObjectUtil.callService(op, idGroupeMaitre, selectedOrgaRefId, selectedOrgaID, txtSearch, _mainRef.getSelectedDate());
        }

        private function searchNodeResult(evt:ResultEvent):void
        {
            /** ToDo
             * Get the result of the search : ok
             * */
            setDataResult(ArrayCollection(evt.result).source);
        }

        override public function refreshTree():void
        {
            callLater(doLaterRefrehTree);
        }

        private function doLaterRefrehTree():void
        {
            _treeNodes = null;
            treePerimetre.selectedIndex = -1;
            var selectedOrgaID:Number = getSelectedOrganisationValue();
            if(!isInit)
            {
                switch(_linesType)
                {
                    case 1:
                    {
                        filterTreeByFilter("getNotAssignNode");
                        break;
                    }
                    case 2:
                    {
                        filterTreeByFilter("getAssignNode");
                        break;
                    }
                    default:
                    {
                        var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
                                                                                            "getNode", fillTreeResult);
                        RemoteObjectUtil.callService(op, selectedOrgaID, 0, _mainRef.getSelectedDate());
                        break;
                    }
                }
            }
        }

        private function filterTreeByFilter(method:String):void
        {
            if(_mainRef != null)
            {
                if(_mainRef.getSelectedOrganisation() == -1)
                {
                    cmbOrganisation.selectedIndex = -1;
                    ConsoviewAlert.afficherAlertInfo(resourceManager.getString('M22', 'Veuillez_choisir_une_organisation___modifier__partie_gauche_'),
                                                     resourceManager.getString('M22', 'Erreur'), null);
                }
                else
                {
                    var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
                    var selectedOrgaRefId:Number = _mainRef.getSelectedOrganisation();
                    var selectedOrgaID:Number = getSelectedOrganisationValue();
                    var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
                                                                                        method, fillTreeResult);
                    RemoteObjectUtil.callService(op, idGroupeMaitre, selectedOrgaRefId.toString(), selectedOrgaID.toString(), _mainRef.getSelectedDate());
                }
            }
        }

        private function fillTreeResult(evt:ResultEvent):void
        {
            _treeNodes = XML(evt.result).children();
            var listeNoeud:XMLList;
            if(modeRestreint && isMemeOrga)
            {
                listeNoeud = XML(evt.result).descendants().(@VALUE == CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
            }
            else
            {
                //listeNoeud = XML(event.result).children().children();
                listeNoeud = XML(evt.result).children();
            }
            setTreeData(listeNoeud, "@LABEL");
        }

        /** ################### PUBLIC METHODS ######################### */
        public function setMainRef(ref:main):void
        {
            _mainRef = ref;
        }

        public function setLineType(value:Number):void
        {
            _linesType = value;
            refreshTree();
            super.onRechercheOrgaChange(new Event("change"));
        }

        override public function setTreeData(data:XMLList, label:String):void
        {
            var _x:Number = treePerimetre.x;
            var _y:Number = treePerimetre.y;
            ConsoViewModuleObjectTreePerimetre.removeChild(treePerimetre);
            treePerimetre = null;
            treePerimetre = new specialTree();
            treePerimetre.itemRenderer = new ClassFactory(composants.access.perimetre.PerimetreTreeItemRendererRightTree);
            treePerimetre.x = _x;
            treePerimetre.y = _y;
            treePerimetre.percentWidth = 100;
            treePerimetre.percentHeight = 100;
            treePerimetre.showRoot = true;
            treePerimetre.dataProvider = data;
            treePerimetre.labelField = label;
            treePerimetre.labelFunction = treeLabelFunction;
            treePerimetre.addEventListener(ListEvent.CHANGE, treePerimetreChangeEvent);
            if(_contextMenu != null)
                treePerimetre.setContextMenu(_contextMenu);
            ConsoViewModuleObjectTreePerimetre.addChild(treePerimetre);
            dispatchEvent(new Event(AssignOrgaCmp.SELECTION_CHANGE));
        }

        private function treePerimetreChangeEvent(ev:Event):void
        {
            dispatchEvent(new Event(AssignOrgaCmp.SELECTION_CHANGE));
        }

        /** ################# END PUBLIC METHODS ######################### */
        private function treeLabelFunction(item:Object):String
        {
            if(XML(item).hasOwnProperty("@COMMENTAIRES") && String(item.@COMMENTAIRES).length > 0)
            {
                return item.@LABEL + " - " + item.@COMMENTAIRES;
            }
            else
            {
                return item.@LABEL;
            }
        }
    }
}