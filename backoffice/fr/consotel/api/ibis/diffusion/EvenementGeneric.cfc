<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.diffusion.EvenementGeneric"
hint="Service de permettant d'exécuter une liste de diffusion qui est identifiée à partir des paramètres fournie dans l'URL et passés à une procédure stockée au format JSON">
	<!--- Initialisation implicite --->
	<cfset initInstance()>

	<cffunction access="public" name="getInstance" returntype="fr.consotel.api.ibis.diffusion.EvenementGeneric" hint="Constructeur explicite qui fait appel à initInstance()">
		<cfset initInstance()>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getLog" returntype="String" hint="Retourne la valeur utilisée pour l'attribut file de CFLOG">
		<cfreturn "DIFFUSION">
	</cffunction>

	<cffunction access="public" name="getReqDiffusionColumns" returntype="Struct" hint="Retourne une structure contenant la liste des colonnes requises pour la liste de diffusion
	Chaque clé est le nom d'une colonne requise et la valeur associée correspond à sa description. Cette méthode ne devrait pas être redéfinie">
		<cfreturn VARIABLES.REQ_DIFFUSION_COLUMNS>
	</cffunction>

	<cffunction access="public" name="getOptDiffusionColumns" returntype="Struct" hint="Retourne une structure contenant la liste des colonnes facultatives pour la liste de diffusion
	Chaque clé est le nom d'une colonne facultative et la valeur associée correspond à sa description. Cette méthode ne devrait pas être redéfinie">
		<cfreturn VARIABLES.OPT_DIFFUSION_COLUMNS>
	</cffunction>

	<cffunction access="public" name="getListeDiffusionProc" returntype="String" hint="Retourne nom complet de la procédure stockée qui ramène la liste de diffusion">
		<cfreturn "PKG_PFGP_SUIVI_CHARGEMENT.TYPE_LISTE">
	</cffunction>
	
	<cffunction access="public" name="getDataSourceName" returntype="String" hint="Retourne le nom de la source de données utilisée pour exécuter les requêtes/procédures">
		<cfreturn "ROCOFFRE">
	</cffunction>

	<cffunction access="remote" name="execute" returntype="void" hint="Exécute une liste de diffusion" output="false">
		<cfargument name="runProperties" type="Struct" required="false" hint="Structure contenant les paramètres qui seront fournit à la procédure ramenant la liste de diffusion
		Lorsque runProperties est une structure vide ou n'est pas défini alors le contneu du scope URL est utilisé à sa place
		Aucune action n'est effectuée lorsque la propriété ID est absente de runProperties ou du scope URL">
		<cfset var runProps={}>
		<cfset var startTick=getTickCount()>
		<cfif isDefined("ARGUMENTS.runProperties")>
			<cfset runProps=ARGUMENTS.runProperties>
		</cfif>
		
		<cfset AnythingToXML = createObject('component', 'fr.consotel.util.parsing.AnythingToXML').init() />
		
		<cftry>
		
			<cfset var reportingService=getReportingService()>
			<cfset runProps=defineRunProperties(runProps)>
			<cfset var listeDiffusion=defineListeDiffusion(runProps)>
			<cfset var listeSize=listeDiffusion.DATA.recordcount>
			<cfset beforeExecution(runProps,listeDiffusion)>
			
			<cfif listeSize GT 0>
			
				<cflog file="#getLog()#" type="information" text="EvenementGeneric executing #listeSize# record count...">
				<cfloop index="currentIndex" from="1" to="#listeSize#">
					<!--- Toute exception capturée est traitée puis la diffusion continue jusqu'à la fin de la liste --->
					
					<cftry>
					
						<cfset var reportingProperties=getDiffusionReporting(listeDiffusion,currentIndex)>
						<cfset var reporting=reportingService.createReporting(reportingProperties)>
						
						<cfset reportingService.execute(reporting)>
						
				<!--- LOG --->
							 
							<cfset subject = listeDiffusion.DATA['EMAIL_SUBJECT'][currentIndex]>
							
							
							<cfsavecontent variable="body">
								<cfset myXML = AnythingToXML.toXML(reportingProperties,"DATA")>
								<cfoutput>#myXML#</cfoutput>
							</cfsavecontent>
							
							<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
							<cfset mLog.setIDMNT_CAT(15)>
							<cfset mLog.setIDMNT_ID(141)>
							<cfset mLog.setBODY(body)>
							<cfset mLog.setSUBJECTID(subject)>
							<cfset mLog.setIDRACINE(listeDiffusion.DATA['IDRACINE'][currentIndex])>
							
							<cfinvoke 
								component="fr.consotel.api.monitoring.MLogger"
								method="logIt"  returnVariable = "result">
								
								<cfinvokeargument name="mLog" value="#mLog#">  
								
							</cfinvoke>	
							
						 
					<!---FIN LOG --->	
						 
						
						
						<cfcatch type="any">
							<cflog file="#getLog()#" type="error" text="EvenementGeneric.execute() failed for index #currentIndex# : #CFCATCH.message#. #CFCATCH.detail#">
							<cfset var exceptionInfos=generateExceptionInfos(CFCATCH)>
							<cfset var listeDesc={}>
							<cfif isDefined("listeDiffusion")>
								<cfset listeDesc=listeDiffusion>
							</cfif>
							<cfset var listeInfos=generateListeDiffusionInfos(runProps,listeDesc)>
							<cfset notifyExecutionException(listeInfos,exceptionInfos)>
						</cfcatch>
					</cftry>
					
				</cfloop>
				<cflog file="#getLog()#" type="information" text="EvenementGeneric execution finished after : #((getTickCount() - startTick) / 1000)# secs">
			</cfif>
			
			
			<cfcatch type="any">
				<cflog file="#getLog()#" type="error" text="EvenementGeneric.execute() : #CFCATCH.message#. #CFCATCH.detail#">
				<cfset var exceptionInfos=generateExceptionInfos(CFCATCH)>
				<cfset var listeDesc={}>
				<cfif isDefined("listeDiffusion")>
					<cfset listeDesc=listeDiffusion>
				</cfif>
				<cfset var listeInfos=generateListeDiffusionInfos(runProps,listeDesc)>
				<cfset notifyExecutionException(listeInfos,exceptionInfos)>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
	<cffunction access="private" name="beforeExecution" returntype="void" hint="Handler exécuté avant l'exécution de la liste de diffusion
	Cette implémentation vérifie la validité du contenu de la liste de diffusion et lève une exception si elle n'est pas valide
	Si le contenu est valide alors une notification du lancement de l'exécution est envoyée">
		<cfargument name="runProperties" type="Struct" required="true" hint="Propriétés d'exécution de la liste de diffusion">
		<cfargument name="listeDiffusion" type="Struct" required="true" hint="Description de la liste de diffusion retournée par defineListeDiffusion()">
		<cfset var LISTE_DIFFUSION=ARGUMENTS["listeDiffusion"]>
		<!--- Vérification de la validité du contenu de la liste de diffusion --->
		<cfif NOT structIsEmpty(LISTE_DIFFUSION.MISSING_COLUMNS)>
			<!--- Exception : Colonnes manquantes --->
			<cfthrow type="Custom" errorcode="ILLEGAL_EXCEPTION" message="Une ou plusieurs colonnes requises sont manquantes dans la liste de diffusion"
				detail="Colonnes manquantes : #structKeyList(LISTE_DIFFUSION.MISSING_COLUMNS)#">
		</cfif>
		<!--- Envoi de la notification de lancement de l'exécution de la liste de diffusion --->
		<cfset notifyExecution(ARGUMENTS["runProperties"],LISTE_DIFFUSION)>
	</cffunction>
	
	<cffunction access="private" name="getDiffusionReporting" returntype="Struct"
	hint="Retourne la diffusion indéxée par indexDiffusion dans la liste sous forme de reporting exécutable par l'implémentation IReportingService retourné par getReportingService()">
		<cfargument name="listeDiffusion" type="Struct" required="true" hint="Description de la liste de diffusion retournée par defineListeDiffusion()">
		<cfargument name="indexDiffusion" type="Numeric" required="true" hint="Index de la diffusion">
		<cfset var LISTE_DESC=ARGUMENTS["listeDiffusion"]>
		<cfset var LISTE=LISTE_DESC["DATA"]>
		<cfset var OPT_COLUMNS=LISTE_DESC["OPTIONAL_COLUMNS"]>
		<cfset var IDX=ARGUMENTS["indexDiffusion"]>
		<cfif IDX LTE LISTE.recordcount>
			<cfset var reportingProperties = {
				DESTINATION="",
				bipReport = {
					reporting="Service EvenementGeneric"& " (ID : " & LISTE["ID"][IDX] & ")",
					xdoAbsolutePath=LISTE["DOSSIER"][IDX] & "/" & LISTE["XDO"][IDX] & "/" & LISTE["XDO"][IDX] & ".xdo",
					xdoTemplateId=LISTE["TEMPLATE"][IDX], outputFormat=LISTE["FORMAT"][IDX], localization=LISTE["LOCALISATION"][IDX], timeZone="Europe/Paris",
					reportParameters=extractParamValues(LISTE_DESC,IDX)
				},
				DELIVERY = {
					ID=VAL(LISTE["ID"][IDX]), DIFFUSION="Evenement",
					TYPE=LISTE["DELIVERY"][IDX], FILE_NAME=LISTE["FILE_NAME"][IDX], FILE_EXT=LISTE["FILE_EXT"][IDX], FICHIER=LISTE["FILE_NAME"][IDX],
					POST_PROCESS=LISTE["POST_PROCESS"][IDX], USERID=VAL(LISTE["USERID"][IDX]), IDRACINE=VAL(LISTE["IDRACINE"][IDX]), CODE_APP=VAL(LISTE["CODE_APPLI"][IDX]),
					FROM=LISTE["EMAIL_FROM"][IDX], TO=LISTE["EMAIL_TO"][IDX], CC=LISTE["EMAIL_CC"][IDX], SUBJECT=LISTE["EMAIL_SUBJECT"][IDX], REPLYTO="", ATTACHMENT=""
				}
			}>
			<!--- Ajout des colonnes facultatives en tant que propriétés fournies à la stratégie de diffusion qui est utilisée --->
			<cfloop item="optColumn" collection="#OPT_COLUMNS#">
				<cfif structKeyExists(LISTE,optColumn)>
					<cfset reportingProperties.DELIVERY[optColumn]=LISTE[optColumn][IDX]>
				</cfif>
			</cfloop>
			<!--- Mise à jour de la valeur de la propriété ATTACHMENT --->
			<cfif VAL(LISTE["HAVE_ATTACHEMENT"][IDX]) GT 0>
				<cfset reportingProperties.DELIVERY["ATTACHMENT"]=reportingProperties.DELIVERY["FILE_NAME"] & "." & reportingProperties.DELIVERY["FILE_EXT"]>
			</cfif>
			<cfreturn reportingProperties>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'index #IDX# est supérieur à la taille de la liste de diffusion">
		</cfif>
	</cffunction>

	<cffunction access="private" name="extractParamValues" returntype="Struct"
	hint="Retourne une structure contenant les paramètres du rapport de la la diffusion indéxée par indexDiffusion dans listeDiffusion
	Cette structure est utilisée en tant que paramètres du reporting dans la structure retournée par getDiffusionReporting()">
		<cfargument name="listeDiffusion" type="Struct" required="true" hint="Description de la liste de diffusion retournée par defineListeDiffusion()">
		<cfargument name="indexDiffusion" type="Numeric" required="true" hint="Index de la diffusion">
		<cfset var LISTE_DESC=ARGUMENTS["listeDiffusion"]>
		<cfset var IDX=ARGUMENTS["indexDiffusion"]>
		<cfset var LISTE=LISTE_DESC["DATA"]>
		<cfset var PARAM_COLUMNS=LISTE_DESC["PARAMS"]>
		<cfset var reportingParams={}>
		<cfloop item="paramName" collection="#PARAM_COLUMNS#">
			<cfset reportingParams[paramName]={LABEL=paramName, VALUES=listToArray(LISTE[paramName][IDX],"|"), VISIBLE=FALSE}>
		</cfloop>
		<cfreturn reportingParams>
	</cffunction>

	<cffunction access="private" name="defineListeDiffusion" returntype="Struct" hint="Retourne une structure contenant les propriétés suivantes :
	- DURATION : Durée d'exécution de la procédure ramenant la liste de diffusion i.e de executeListeDiffusionProc()
	- DATA : Contenu de la liste de diffusion retournée par executeListeDiffusionProc()
	- PARAMS : Structure dans laquelle chaque clé est le nom d'un paramètre à envoyer à chaque rapport BIP de la la liste de diffusion
	- MISSING_COLUMNS : Structure dans laquelle chaque clé est une colonne requise est qui est manquante dans la liste de diffusion
	- OPTIONAL_COLUMNS : Structure dans laquelle chaque clé est une colonnes facultative présente dans la liste de diffusion et qui n'est pas un paramètre pour chaque rapport
	Toute colonne de la liste de diffusion et qui ne fait pas contenue dans la structure retournée par getReqDiffusionColumns() est considérée comme un paramètre
	Une exception est levée si la liste de diffusion récupérée n'est pas validée par validateListeDiffusion()">
		<cfargument name="runProperties" type="Struct" required="true" hint="Propriétés d'exécution contenant au moins la propriété ID">
		<cfset var runProps=ARGUMENTS["runProperties"]>
		<cfset var startTick=getTickCount()>
		<cfset var LISTE=executeListeDiffusionProc(serializeJSON(runProps))>
		<cfset var COLUMN_LIST=extractColumnListDesc(LISTE)>
		<cfset var LISTE_DIFFUSION={
			DURATION=((getTickCount() - startTick) / 1000), DATA=LISTE, PARAMS=COLUMN_LIST.PARAM_COLUMNS,
			MISSING_COLUMNS=COLUMN_LIST.MISSING_COLUMNS, OPTIONAL_COLUMNS=COLUMN_LIST.OPTIONAL_COLUMNS
		}>
		<cfreturn LISTE_DIFFUSION>
	</cffunction>

	<cffunction access="private" name="extractColumnListDesc" returntype="Struct" hint="Retourne une structure contenant les propriétés suivantes :
	- PARAM_COLUMNS : Structure dans laquelle chaque clé est un paramètre à envoyer à chaque rapport BIP de la liste de diffusion 
	Toute colonne de la liste de diffusion et qui ne fait pas contenue dans la structure retournée par getReqDiffusionColumns() est considérée comme un paramètre
	- MISSING_COLUMNS : Structure dans laquelle chaque clé est une colonne requise est qui est manquante dans la liste de diffusion
	- OPTIONAL_COLUMNS : Structure dans laquelle chaque clé est une colonnes facultative présente dans la liste de diffusion et qui n'est pas un paramètre pour chaque rapport">
		<cfargument name="listeDiffusion" type="Query" required="true" hint="Liste de diffusion à valider">
		<cfset var LISTE=ARGUMENTS["listeDiffusion"]>
		<cfset var COLUMN_LIST=LISTE.columnList>
		<cfset var REQ_COLUMNS=DUPLICATE(getReqDiffusionColumns())>
		<cfset var OPT_COLUMNS=getOptDiffusionColumns()>
		<!--- Structures contenant la description des liste de colonnes : Respectivement colonnes paramètres et facultatives --->
		<cfset var PARAM_COLUMN_LIST={}>
		<cfset var OPTIONAL_COLUMNS={}>
		<!--- Définition de la liste des colonnes correspondants à des paramètres à envoyer à chaque rapport BIP de la liste de diffusion --->
		<cfloop index="columnName" list="#COLUMN_LIST#" delimiters=",">
			<!--- Colonne requises dans la liste de diffusion --->
			<cfif structKeyExists(REQ_COLUMNS,columnName)>
				<cfset structDelete(REQ_COLUMNS,columnName)>
			<cfelse>
				<!--- Colonne facultative dans la liste de diffusion --->
				<cfif structKeyExists(OPT_COLUMNS,columnName)>
					<cfset OPTIONAL_COLUMNS[columnName]=columnName>
				<!--- Colonne paramètres pour chaque rapport dans la liste de diffusion --->
				<cfelse>
					<cfset PARAM_COLUMN_LIST[columnName]=columnName>
				</cfif>
			</cfif>
		</cfloop>
		<cfreturn {PARAM_COLUMNS=PARAM_COLUMN_LIST, MISSING_COLUMNS=REQ_COLUMNS, OPTIONAL_COLUMNS=OPTIONAL_COLUMNS}>
	</cffunction>

	<cffunction access="private" name="defineReqColumns" returntype="Struct"
	hint="Définit et retourne une structure dans laquelle chaque clé est le nom d'une colonne requise pour la liste de diffusion associée à sa description">
		<cfset var reqColumns={
			ID="ID de suivi", DELIVERY="Stratégie de diffusion", POST_PROCESS="POST_PROCESS", USERID="USERID", CODE_APPLI="CODE_APPLI", IDRACINE="IDRACINE",
			EMAIL_FROM="EMAIL_FROM", EMAIL_TO="EMAIL_TO", EMAIL_CC="EMAIL_CC", EMAIL_SUBJECT="EMAIL_SUBJECT",
			FILE_NAME="FILE_NAME", FILE_EXT="FILE_EXT", HAVE_ATTACHEMENT="HAVE_ATTACHEMENT",
			DOSSIER="DOSSIER", XDO="XDO", TEMPLATE="TEMPLATE", FORMAT="FORMAT", LOCALISATION="LOCALISATION"
		}>
		<cfreturn reqColumns>
	</cffunction>
	
	<cffunction access="private" name="defineOptColumns" returntype="Struct"
	hint="Définit et retourne une structure dans laquelle chaque clé est le nom d'une colonne facultative pour la liste de diffusion associée à sa description
	Lorsqu'une de ces colonnes est présente dans la liste de diffusion alors elle est ajoutée avec sa valeur en tant que propriété fournie à la stratégie de diffusion">
		<cfset var optColumns={
			BCC="Destinataire en copie cachée (uniquement pour une diffusion mail)", SHORT_NAME="Nom du rapport lorsqu'il est téléchargé depuis le module container"
		}>
		<cfreturn optColumns>
	</cffunction>

	<cffunction access="private" name="executeListeDiffusionProc" returntype="Query"
	hint="Exécute la procédure dont le nom est retourné par getListeDiffusionProc() et retourne son résultat. Le paramètre jsonRunProperties est passé à la procédure au format JSON">
		<cfargument name="jsonRunProperties" type="String" required="true" hint="Propriétés d'exécution au format JSON provenant de defineListeDiffusion() et passés à la procédure">
		<cfset var jsonParam=ARGUMENTS["jsonRunProperties"]>
		<cfstoredproc datasource="#getDataSourceName()#" procedure="#getListeDiffusionProc()#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#jsonParam#">
			<cfprocresult name="qListeDiffusion">
		</cfstoredproc>
		<cfif structKeyExists(qListeDiffusion,"ERROR")>
			<cfset var errorMsg="">
			<cfif qListeDiffusion.recordcount GT 0>
				<cfset errorMsg=qListeDiffusion["ERROR"][1]>
			</cfif>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
				message="La procédure #getListeDiffusionProc()# a retourné un code d'erreur" detail="Contenu du code d'erreur (Colonne ERROR) : #errorMsg#">
		</cfif>
		<cfreturn qListeDiffusion>
	</cffunction>
	
	<cffunction access="private" name="defineRunProperties" returntype="Struct" hint="Définit les propriétés d'exécution à partir de initProperties
	Lorsque initProperties est une structure vide alors le contenu du scope URL est utilisé à sa place">
		<cfargument name="runProperties" type="Struct" required="true" hint="Propriétés d'exécution">
		<cfset var runProps=ARGUMENTS.runProperties>
		<cfif structIsEmpty(runProps)>
			<cfif isDefined("URL") AND (NOT structIsEmpty(URL))>
				<cfset runProps=DUPLICATE(URL)>
			</cfif>
		</cfif>
		<cfreturn runProps>
	</cffunction>
	
	<cffunction access="private" name="notifyExecution" returntype="void"
	hint="Cette implémentation envoie une notification contenant des informations concernant la liste de diffusion qui va être exécutée">
		<cfargument name="runProperties" type="Struct" required="true" hint="Propriétés d'exécution de la liste de diffusion">
		<cfargument name="listeDiffusion" type="Struct" required="true" hint="Description de la liste de diffusion retournée par defineListeDiffusion()">
		<cfset var RUN_PROPS=ARGUMENTS["runProperties"]>
		<cfset var LISTE_DIFFUSION=ARGUMENTS["listeDiffusion"]>
		<cfset var listeInfos=generateListeDiffusionInfos(RUN_PROPS,LISTE_DIFFUSION)>


		<cftry>
				
				<cfsavecontent variable="body">
					<cfoutput>
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 7bit

<head><META http-equiv="Content-Type" content="html; charset=UTF-8"></head>
Cette notification a été générée depuis #CGI.SERVER_NAME# le <b>#lsDateFormat(NOW(),"dd/mm/yyyy")# à #lsTimeFormat(NOW(),"HH:mm:ss")#</b><br>
TYPE MAIL : #RUN_PROPS.TYPE_MAIL#<br>
<cfif NOT structIsEmpty(LISTE_DIFFUSION)>
Nbre de diffusion(s) : #LISTE_DIFFUSION.DATA.recordcount# (Durée en secs : #LISTE_DIFFUSION.DURATION#)<br>
<cfif structKeyExists(LISTE_DIFFUSION,"OPTIONAL_COLUMNS") AND (NOT structIsEmpty(LISTE_DIFFUSION.OPTIONAL_COLUMNS))>
<cfdump var="#LISTE_DIFFUSION.OPTIONAL_COLUMNS#" label="Liste des colonnes optionnelles fournies"><br>
<cfelse>
Pas de colonnes optionnelles<br>
</cfif>
</cfif>
Implémentation de du service : #getMetadata(THIS).NAME#<br>
Liste de diffusion : #getListeDiffusionProc()#<br>
Page d'exécution : #CGI.SCRIPT_NAME#<br>
Machine cliente : #CGI.REMOTE_HOST# (#CGI.HTTP_USER_AGENT#)

					</cfoutput>
				</cfsavecontent>

				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(15)>
				<cfset mLog.setIDMNT_ID(25)>
				<cfset mLog.setBODY(body)>
				<cfset mLog.setSUBJECTID("TYPE MAIL : #RUN_PROPS.TYPE_MAIL#")>
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			
		
		</cftry>


<!---
		<cfmail attributecollection="#getMailProperties().NOTIFICATION#"
			subject="Lancement d'une exécution de liste de diffusion (TYPE_MAIL : #RUN_PROPS.TYPE_MAIL#) : #getListeDiffusionProc()#">
			<cfmailpart type="multipart/mixed; boundary=#chr(34)#P1#chr(34)#">
--P1
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 7bit

<head><META http-equiv="Content-Type" content="html; charset=UTF-8"></head>
Cette notification a été générée depuis #CGI.SERVER_NAME# le <b>#lsDateFormat(NOW(),"dd/mm/yyyy")# à #lsTimeFormat(NOW(),"HH:mm:ss")#</b><br>
TYPE MAIL : #RUN_PROPS.TYPE_MAIL#<br>
<cfif NOT structIsEmpty(LISTE_DIFFUSION)>
Nbre de diffusion(s) : #LISTE_DIFFUSION.DATA.recordcount# (Durée en secs : #LISTE_DIFFUSION.DURATION#)<br>
<cfif structKeyExists(LISTE_DIFFUSION,"OPTIONAL_COLUMNS") AND (NOT structIsEmpty(LISTE_DIFFUSION.OPTIONAL_COLUMNS))>
<cfdump var="#LISTE_DIFFUSION.OPTIONAL_COLUMNS#" label="Liste des colonnes optionnelles fournies"><br>
<cfelse>
Pas de colonnes optionnelles<br>
</cfif>
</cfif>
Implémentation de du service : #getMetadata(THIS).NAME#<br>
Liste de diffusion : #getListeDiffusionProc()#<br>
Page d'exécution : #CGI.SCRIPT_NAME#<br>
Machine cliente : #CGI.REMOTE_HOST# (#CGI.HTTP_USER_AGENT#)

--P1
Content-Type: text/html; charset=UTF-8
Content-Disposition: attachment; filename="Liste de diffusion.html"
Content-Transfer-Encoding: 7bit

#listeInfos.LISTE_INFOS#

--P1--
			</cfmailpart>
		</cfmail>
		
--->
	</cffunction>
	
	<cffunction access="private" name="notifyExecutionException" returntype="void" hint="Envoi une notification concernant une exception capturée durant l'exécution d'une liste de diffusion">
		<cfargument name="listeDiffusion" type="Struct" required="true" hint="Description de la liste de diffusion retournée par defineListeDiffusion()">
		<cfargument name="exceptionInfos" type="Struct" required="true" hint="Structure générée par generateExceptionInfos()">
		<cfset var LISTE_DIFFUSION_INFOS=ARGUMENTS["listeDiffusion"]>
		<cfset var ERROR_INFOS=ARGUMENTS["exceptionInfos"]>
		
		
		<cftry>
				
				<cfsavecontent variable="body">
					<cfoutput>
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 7bit

<head><META http-equiv="Content-Type" content="html; charset=UTF-8"></head>
Cette notification a été générée depuis #CGI.SERVER_NAME# le <b>#lsDateFormat(NOW(),"dd/mm/yyyy")# à #lsTimeFormat(NOW(),"HH:mm:ss")#</b><br>
Liste de diffusion : #getListeDiffusionProc()#<br>
Message : #ERROR_INFOS.CFCATCH_OBJECT.MESSAGE#<br>
Détail : #ERROR_INFOS.CFCATCH_OBJECT.DETAIL#


					</cfoutput>
				</cfsavecontent>

				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(15)>
				<cfset mLog.setIDMNT_ID(26)>
				<cfset mLog.setBODY(body)>
				<cfset mLog.setSUBJECTID("#CGI.SERVER_NAME#")>
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			
		
		</cftry>
		
		
<!---		
		<cfmail attributecollection="#getMailProperties().ERROR#" subject="Exception durant l'exécution d'une liste de diffusion : #ERROR_INFOS.CFCATCH_OBJECT.MESSAGE#">
			<cfmailpart type="multipart/mixed; boundary=#chr(34)#P1#chr(34)#">
--P1
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 7bit

<head><META http-equiv="Content-Type" content="html; charset=UTF-8"></head>
Cette notification a été générée depuis #CGI.SERVER_NAME# le <b>#lsDateFormat(NOW(),"dd/mm/yyyy")# à #lsTimeFormat(NOW(),"HH:mm:ss")#</b><br>
Liste de diffusion : #getListeDiffusionProc()#<br>
Message : #ERROR_INFOS.CFCATCH_OBJECT.MESSAGE#<br>
Détail : #ERROR_INFOS.CFCATCH_OBJECT.DETAIL#

--P1
Content-Type: text/html; charset=UTF-8
Content-Disposition: attachment; filename="Liste de diffusion.html"
Content-Transfer-Encoding: 7bit

#LISTE_DIFFUSION_INFOS.LISTE_INFOS#

--P1
Content-Type: text/html; charset=UTF-8
Content-Disposition: attachment; filename=Exception.html
Content-Transfer-Encoding: 7bit

#ERROR_INFOS.EXCEPTION_INFOS#

--P1--
			</cfmailpart>  
		</cfmail> --->
	</cffunction>
	
	<cffunction access="private" name="generateListeDiffusionInfos" returntype="Struct" hint="Retourne une structure contenant les propriétés suivantes :
	- LISTE_INFOS : Contenu HTML contenant les infos concernant une liste de diffusion">
		<cfargument name="runProperties" type="Struct" required="true" hint="Propriétés d'exécution de la liste de diffusion">
		<cfargument name="listeDiffusion" type="Struct" required="true" hint="Description de la liste de diffusion ou une structure vide si elle n'est pas définie">
		<cfset var RUN_PROPS=ARGUMENTS["runProperties"]>
		<cfset var LISTE_DIFFUSION=ARGUMENTS["listeDiffusion"]>
		<!--- Informations concernant la liste de diffusion --->
		<cfsavecontent variable="listeDiffusionInfos">
			<head><META http-equiv="Content-Type" content="html; charset=UTF-8"></head>
			<cfoutput>Liste de diffusion : #getListeDiffusionProc()#</cfoutput><br>
			<cfdump var="#RUN_PROPS#" label="Propriétés d'exécution fournies à l'exécution de la liste de diffusion"><br>
			<cfdump var="#getReqDiffusionColumns()#" label="Liste des colonnes requises pour la liste de diffusion">
			<cfif NOT structIsEmpty(LISTE_DIFFUSION)>
				<b>Description de la liste de diffusion :</b> (DATA:Contenu, DURATION:Durée en secs, PARAMS:Paramètres pour chaque rapport,
				MISSING_COLUMNS: Colonnes manquantes, OPTIONAL_COLUMNS: Colonnes facultatives fournies à la stratégie de diffusion)<br>
				<br><cfdump var="#LISTE_DIFFUSION#" label="Liste de diffusion">
			</cfif>
		</cfsavecontent>
		<cfreturn {LISTE_INFOS=listeDiffusionInfos}>
	</cffunction>
	
	<cffunction access="private" name="generateExceptionInfos" returntype="Struct" hint="Retourne une structure contenant les propriétés suivantes :
	- EXCEPTION_INFOS : Contenu HTML contenant les infos concernant l'exception capturée durant l'exécution d'une liste de diffusion
	- CFCATCH_OBJECT : Contenu de l'exception captuée par CFTRY/CFCATCH durant l'exécution d'une liste de diffusion">
		<cfargument name="cfcatchStruct" type="Any" required="true" hint="Exception capturée par CFTRY/CFCATCH">
		<cfset var EXCEPTION_OBJ=ARGUMENTS["cfcatchStruct"]>
		<!--- Informations concernant l'exception capturée --->
		<cfsavecontent variable="exceptionInfos">
			<head><META http-equiv="Content-Type" content="html; charset=UTF-8"></head>
			<cfoutput>
				Implémentation de du service : #getMetadata(THIS).NAME#<br>
				Page d'exécution : #CGI.SCRIPT_NAME#<br>
				Machine cliente : #CGI.REMOTE_HOST# (#CGI.HTTP_USER_AGENT#)<br>
			</cfoutput>
			<cfdump var="#EXCEPTION_OBJ.TAGCONTEXT#" label="Contexte d'exécution">
		</cfsavecontent>
		<cfreturn {CFCATCH_OBJECT=EXCEPTION_OBJ, EXCEPTION_INFOS=exceptionInfos}>
	</cffunction>

	<cffunction access="private" name="initInstance" returntype="void"
	hint="Handler exécuté à l'instanciation et qui initialise les propriétés utilisées par cette implémentation. Toute exception capturée est notifiée puis redéclenchée">
		<cftry>
			<cfif NOT structKeyExists(VARIABLES,"INITIALIZED")>
				<cfset var notificationTo="monitoring@saaswedo.com">
				<cfset VARIABLES.MAIL_PROPS={
					NOTIFICATION={from="api@consotel.fr", to=notificationTo, type="html"},
					WARNING={from="warning@consotel.fr", to=notificationTo, type="html"},
					ERROR={from="error@consotel.fr", to=notificationTo, type="html"}
				}>
				<!--- Définition des colonnes requises pour la liste de diffusion --->
				<cfset VARIABLES.REQ_DIFFUSION_COLUMNS=defineReqColumns()>
				<!--- Définition des colonnes facultatives pour la liste de diffusion --->
				<cfset VARIABLES.OPT_DIFFUSION_COLUMNS=defineOptColumns()>
				<!--- Instanciation de l'implémentation du service de reporting (IReportingService) --->
				<cfset VARIABLES.REPORTING_SERVICE=createObject("component","fr.consotel.api.ibis.publisher.reporting.impl.ReportingService").getService("evenement","public")>
				<cflog file="#getLog()#" type="information" text="EvenementGeneric INITIALIZED : Using #getListeDiffusionProc()# on #getDataSourceName()#">
			</cfif>
			<cfcatch type="any">
				<cflog file="#getLog()#" type="error" text="EvenementGeneric initialization error : #CFCATCH.message#. #CFCATCH.detail#">
				<cfset var exceptionInfos=generateExceptionInfos(CFCATCH)>
				<cfset var listeInfos=generateListeDiffusionInfos({},{})>
				<cfset notifyExecutionException(listeInfos,exceptionInfos)>
				<!--- Redéclenche l'exception capturée --->
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="private" name="getReportingService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService"
	hint="Retourne l'instance authentifiée de l'implémentation IReportingService utilisée par ce service">
		<cfreturn VARIABLES.REPORTING_SERVICE>
	</cffunction>
	
	<cffunction access="private" name="getMailProperties" returntype="Struct" hint="Retourne une structure contenant les propriétés utilisées pour envoyer les notifications de cette API">
		<cfreturn VARIABLES.MAIL_PROPS>
	</cffunction>
</cfcomponent>