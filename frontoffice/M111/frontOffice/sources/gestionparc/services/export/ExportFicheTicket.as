package gestionparc.services.export
{
	import composants.util.searchPaginateDatagrid.CriteresRechercheVO;
	
	import flash.net.URLVariables;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	[Bindable]
	public class ExportFicheTicket
	{
		public var handler:ExportHandler;
		
		public function ExportFicheTicket()
		{
			handler = new ExportHandler();
		}
		
		public function getExport(paramInventaire:URLVariables):void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.export.PrintListProduitContainer",
				"run",
				handler.getExportInventaireHandler);
			RemoteObjectUtil.callService(opData,
				paramInventaire.idproduit_client,
				paramInventaire.datefin,
				paramInventaire.tb,
				paramInventaire.mode,
				paramInventaire.raisonsociale,
				paramInventaire.modeCalcul,
				paramInventaire.numero,
				paramInventaire.PATH_PRDT,
				paramInventaire.format,
				paramInventaire.perimetre,
				paramInventaire.datedeb);
			
		}
		
		public function getExportDetailAppel(idSous_tete:Number,numero:String,idinventaire_periode:Number,params:CriteresRechercheVO,nbTotalItem:int):void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.export.Export",
				"getExportDetailAppel",
				handler.getExportDetailAppelHandler);
			RemoteObjectUtil.callService(opData,
				idSous_tete,
				numero,
				idinventaire_periode,
				params.orderColonne,
				params.orderBy,
				params.searchColonne,
				params.searchText,
				1,
				nbTotalItem);
		}
		
	}
}