<cfcomponent displayname="EquipementService" output="false">
	
	<!--- INFO SUR L'EQUIPEMENTS  --->
	
	<cffunction name="getProductFeature" access="remote" returntype="array" output="false" hint="INFO SUR L'EQUIPEMENTS">
		<cfargument name="idEquipement" required="true" type="numeric" 	default=""/>
		<cfargument name="idlang" 		required="true" type="numeric" 	default="3"/>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_equipement.get_product_feature_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_langid" 		value="#idlang#">
			<cfprocresult name="p_retour" resultset="1">
			<cfprocresult name="p_retour_photo" resultset="2">
		</cfstoredproc>
		
		<cfset result = arrayNew(1)>
		<cfset arrayAppend(result,p_retour)>
		<cfset arrayAppend(result,p_retour_photo)>
				
		<cfreturn result>
	</cffunction>
	
	<!--- FOURNIT LES ACCESSOIRES CLIENT COMPATIBLES AVEC L'APPAREIL SÉLECTIONNÉ --->
		
	<cffunction name="getProductRelated" access="remote" returntype="query" output="false" hint="FOURNIT LES ACCESSOIRES CLIENT COMPATIBLES AVEC L'APPAREIL SÉLECTIONNÉ">
		<cfargument name="idEquipement" 	required="true" type="numeric" 	default=""/>
		<cfargument name="sameRevendeur" 	required="true" type="numeric" 	default=""/>
		<cfargument name="chaine" 			required="true" type="string" 	default="null"/>
		<cfargument name="idlang" 			required="true" type="numeric" 	default="3"/>

			<cfif isDefined("code_langue")>
				<cfset _code_langue = code_langue>	
			<cfelse>
				<cfset _code_langue = SESSION.USER.GLOBALIZATION>	
			</cfif>
	
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.get_product_related_client ">				
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_chaine" 		value="#chaine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_same_rev" 		value="#sameRevendeur#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_code_langue" 	value="#_code_langue#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine " 		value="#Session.PERIMETRE.ID_GROUPE#">	
				<cfprocresult name="p_retour">			
			</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>

</cfcomponent>