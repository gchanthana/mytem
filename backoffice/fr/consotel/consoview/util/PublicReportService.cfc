<cfcomponent name="PublicReportService" displayname="PublicReportService">
	<cfset variables.RunReportRequest=structNew()>
	<cfset variables.RunReportRequest.userID="consoview">
	<cfset variables.RunReportRequest.password="public">
	<cfset variables.ArrayOfParamNameValues=ArrayNew(1)>
	
	<cffunction name="AddParameter" access="remote" returntype="void">
		<cfargument name="paramName" type="string" required="true">
		<cfargument name="paramValues" type="string" required="true">
		<cfset ParamNameValue=structNew()>
		<cfif ArrayLen(ListToArray(paramValues)) gt 1>
			<cfset ParamNameValue.multiValuesAllowed=true>
		<cfelse>
			<cfset ParamNameValue.multiValuesAllowed=false>
		</cfif>
		<cfset ParamNameValue.name=paramName>
		<cfset t=ArrayNew(1)>
		<cfset v=ArrayNew(1)>
		<cfset v=ListToArray(paramValues)>
		<cfloop from="1" to="#ArrayLen(v)#" index="i">
			<cfset t[i]=v[i]>
		</cfloop>
		<cfset ParamNameValue.values=t>		
		<cfset ArrayOfParamNameValues[ArrayLen(ArrayOfParamNameValues)+1]=ParamNameValue>
		<cfset variables.ReportRequest.parameterNameValues=variables.ArrayOfParamNameValues>
		<cfset structInsert(variables.RunReportRequest,"reportRequest",variables.ReportRequest,true)>
	</cffunction>
	
	<cffunction name="createReportRequest" access="remote" returntype="void">
		<cfargument name="reportAbsolutePath" type="string" required="true">
		<cfargument name="attributeTemplate" type="string" required="true">
		<cfargument name="attributeFormat" type="string" required="true">
		<cfargument name="attributeLocale" type="string" default="fr-FR" required="false">
		<cfset variables.ReportRequest=structNew()>
		<cfset variables.ReportRequest.reportAbsolutePath=reportAbsolutePath>
		<cfset variables.ReportRequest.attributeTemplate=attributeTemplate>
		<cfset variables.ReportRequest.attributeFormat=attributeFormat>
		<cfset variables.ReportRequest.attributeLocale=attributeLocale>
		<cfset variables.ReportRequest.sizeOfDataChunkDownload=-1>
		<cfset variables.ReportRequest.parameterNameValues=variables.ArrayOfParamNameValues>
		<cfset structInsert(variables.RunReportRequest,"reportRequest",variables.ReportRequest)>
	</cffunction>
	
	<cffunction name="getRunReportRequest" access="remote" returntype="Any">
		<cfreturn variables.RunReportRequest>
	</cffunction>
	
	<cffunction name="runReport" access="remote" returntype="Any">
		<cfinvoke webservice="#SESSION.BI_SERVICE_URL#" returnvariable="RunReportReturn" method="runReport"
				argumentCollection="#variables.RunReportRequest#">
		</cfinvoke>
		<cfreturn RunReportReturn>
	</cffunction>
	
</cfcomponent>