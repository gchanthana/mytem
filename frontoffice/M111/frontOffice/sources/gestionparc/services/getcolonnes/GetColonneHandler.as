package gestionparc.services.getcolonnes
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class GetColonneHandler
	{
		private var model:GetColonneData;

		public function GetColonneHandler(instanceOfDatas:GetColonneData)
		{
			model=instanceOfDatas;
		}

		internal function ViewResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				model.setColonne(re.result as ArrayCollection);
			}
		}
	}
}