package univers.inventaire.equipements.lignes.mainComponent
{
	import mx.controls.TextInput;
	import mx.core.IFactory;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.dataGridClasses.DataGridItemRenderer;
	import composants.util.ConsoviewFormatter;
	import mx.controls.Label;
	
	public class griditemRender  extends DataGridItemRenderer
	{
		public function griditemRender()
		{
			setStyle("borderStyle", "none"); 
		}
		
		override public function set data(value:Object):void {
            super.data = value;
         if (value != null && value.dataField == undefined)
         {
         	if (parseInt(value.NBNODE) > 1)
         		setStyle("color", 0xff9900); 
         	else
	         	setStyle("color", 0x000000);
         }
  		}
	}
	
}