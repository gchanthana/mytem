package service.upload
{
	import composants.util.ConsoviewAlert;
	
	import entity.FileUpload;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
		public class UploadFileService extends EventDispatcher	{				//----------- VARIABLES -----------//				private static var _path:String = "fr.consotel.consoview.M61.upload.UploaderFile";						//----------- METHODES -----------//		
		/* */
		public function UploadFileService()
		{
		}
		
		/* */		public function uploadFile(currentFile:FileUpload): void
		{			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,																				_path,																				"uploadFile",																				uploadFileResultHandler);
						RemoteObjectUtil.callService(op,currentFile);			}
		
		/* */
		public function uploadFiles(filesToUpload:Array, idactu:int): void
		{			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"uploadFiles",
																				uploadFilesResultHandler);
			
			RemoteObjectUtil.callService(op,filesToUpload,idactu);	
		}
		
		/* */
//		public function renameFile(currentFile:FileUpload, newFileName:String, isDataBaseRecord:Boolean): void
//		{
//			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//																				_path,
//																				"renameFile",
//																				renameFileResultHandler);
//			
//			RemoteObjectUtil.callService(op,currentFile,
//											newFileName,
//											Formator.formatBoolean(isDataBaseRecord));	
//		}
//		
		/* */
		public function removeFile(currentFile:FileUpload, isDataBaseRecord:Boolean): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"removeFile",
																				removeFileResultHandler);
			
			RemoteObjectUtil.callService(op,currentFile/*, Formator.formatBoolean(isDataBaseRecord)*/);	
		}		
		
		
		//----------- RETOURS - HANDLERS -----------//
		
		/* */		private function uploadFileResultHandler(re:ResultEvent):void		{			if(re.result > 0)				dispatchEvent(new Event('FILE_UPLOADED'));			else				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de l'upload du fichier", 'Attention', null);
		}
		
		/* */
		private function uploadFilesResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var bool	:Boolean = true;
				var len		:int = (re.result as Array).length;
				
				for(var i:int=0; i<len; i++)
				{
					if(re.result[i] == 0)
					{
						bool = false;
						break;
					}
				}
				
				if(bool)
					dispatchEvent(new Event('FILES_UPLOADED'));
				else
					ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de l'upload du fichier", 'Attention', null);
			}
			else
				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de l'upload du fichier", 'Attention', null);
		}
		
		/* */
		private function renameFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new Event('FILE_RENAMED'));
			else
				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors du renommage du fichier", 'Attention', null);
		}
		
		/* */
		private function removeFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new Event('FILE_REMOVED'));
			else
				ConsoviewAlert.afficherAlertInfo("Une erreur est survenue lors de la suppression du fichier", 'Attention', null);
		}	}}