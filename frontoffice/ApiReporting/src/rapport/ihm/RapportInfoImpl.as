package rapport.ihm
{
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import rapport.event.AccueilEvent;
	import rapport.event.RapportIHMEvent;
	import rapport.ihm.infofacture.IntegrationDesFacturesIHM;
	import rapport.service.perimetre.PerimetreService;
	import rapport.vo.Rapport;
	
	[Bindable]
	public class RapportInfoImpl extends VBox
	{
		public var btnExportOrga:Button;
		public var btnExportOrgaSt:Button;
		public var comp2:IntegrationDesFacturesIHM;
		public var perimetreE0:PerimetreService;		
		public var remo1:RemoteObject;
		public var remo2:RemoteObject;
		public var remo3:RemoteObject;
		public var dateDebut	:String = "";
		public var dateFin		:String = "";
		public var selectedRapportUser :Rapport;
		
		private var _data1:ArrayCollection;
		private var _data2:ArrayCollection;
		private var _data3:ArrayCollection;	
		private var _idCurrentPerimetre:Number=0;
		
		private  var libelle_racine:String="";
		
		public function set data1(value:ArrayCollection):void
		{
			_data1 = value;
		}
		
		public function get data1():ArrayCollection
		{
			return _data1;
		}
		public function set data2(value:ArrayCollection):void
		{
			_data2 = value;
		}
		
		public function get data2():ArrayCollection
		{
			return _data2;
		}
		public function set data3(value:ArrayCollection):void
		{
			_data3 = value;
		}
		
		public function get data3():ArrayCollection
		{
			return _data3;
		}
			
	    public function initDataIntegrationFacture():void
		{
			this.afterCreationComplete(null);
		}
		public function RapportInfoImpl() 
		{
			super();
			
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
			
			remo2 = new RemoteObject("ColdFusion");
			remo2.source="fr.consotel.consoview.M331.service.datafacture.AccueilService";
			remo2.addEventListener(ResultEvent.RESULT,getData2ResultHandler,false,0,true);
			remo2.addEventListener(FaultEvent.FAULT,faultHandler,false,0,true);
		}
		
		protected function afterCreationComplete(event:FlexEvent):void 
		{
			//menuTree.addEventListener(ConsoViewEvent.TREE_MENU_FUNCTION_CHANGED,handlePeriodControlEvents);
			if(_idCurrentPerimetre!=CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX)
			{
				afterPerimetreUpdated();
				libelle_racine=CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
				comp2.libelle_racine=libelle_racine;
				_idCurrentPerimetre=CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
					
			}
		}
		
		public function getUniversKey():String 
		{
			return "HOME_E0";
		}
		
		/**
		 * Univers Accueil do not have Function (Particular case).
		 * IMPORTANT :
		 * Other ConsoView Function classes SHOULD NOT return NULL for this method !!!!
		 * */
		public function getFunctionKey():String 
		{
			return null;
		}
		
		public function afterPerimetreUpdated():void 
		{
			if(perimetreE0)
			{				
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_CHANGE, _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE, _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler = null;
				perimetreE0.model = null;
				perimetreE0 = null;				
			}
			
			perimetreE0 = new PerimetreService();
			perimetreE0.handler.addEventListener(AccueilEvent.PERIMETRE_CHANGE
				, _perimetreE0handlerPerimetreChangeHandler,false,0,true);
			perimetreE0.handler.addEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE
				, _perimetreE0handlerPerimetreChangeHandler,false,0,true);
			
			perimetreE0.setUpPerimetre();
			
			//menuTree.afterPerimetreUpdated();
		}
		
		
		protected function _perimetreE0handlerPerimetreChangeHandler(event:rapport.event.AccueilEvent):void
		{
				
			if(perimetreE0)
			{
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_CHANGE,    _perimetreE0handlerPerimetreChangeHandler);
				perimetreE0.handler.removeEventListener(AccueilEvent.PERIMETRE_NOT_CHANGE,_perimetreE0handlerPerimetreChangeHandler);				
			}
			
			comp2.stopLoader();
			data2 = null;
				
			comp2.launchLoader();
			remo2.getData2();
				
		}			
		public function getData2ResultHandler(event:ResultEvent):void
		{	
			if(event.result)
			{
				data2 = event.result as ArrayCollection;	
			}							
		}
				
		public function faultHandler(event:FaultEvent):void
		{
			trace(event.fault.getStackTrace());
		}

	}
}