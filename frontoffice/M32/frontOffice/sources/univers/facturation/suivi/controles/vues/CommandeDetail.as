package univers.facturation.suivi.controles.vues
{
	import mx.containers.VBox;
	
	import univers.facturation.suivi.controles.controls.TypesCommandesMobile;
	import univers.facturation.suivi.controles.vo.CommandeVO;

	public class CommandeDetail extends VBox
	{
		
		[Bindable]
		public var commande:CommandeVO = new CommandeVO();
		
		[Bindable]
		public var indexView:Number=0;
		
		public function CommandeDetail()
		{
		}
		
		public function init():void{
			selectView(whatIsTypeCommande());
		}
		
		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function whatIsTypeCommande():int
		{
			var idtypecommande:int = -1;
			
			switch(commande.idTypeCommande)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	idtypecommande = 1; break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		idtypecommande = 1; break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		idtypecommande = 1; break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		idtypecommande = 1; break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: idtypecommande = 3; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	idtypecommande = 3; break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			idtypecommande = 4; break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	idtypecommande = 4; break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		idtypecommande = 2; break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	idtypecommande = 2; break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			idtypecommande = 4; break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		idtypecommande = 4; break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		idtypecommande = 4; break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	idtypecommande = 4; break;//--- REACT
			}
			
			return idtypecommande;
		}
		
		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function selectView(idTypeCommande:int):void
		{
			
			
			switch(idTypeCommande)
			{
				case 1: 	{	indexView = 0;
					//etapeDetailsCommande.listArticles 	= dataForList;
					break;
				}
				case 2: 	{	indexView = 0;
					//etapeDetailsCommande.listArticles 	= dataForList;
					break;
				}
				case 3: 	{	indexView = 1;
					//etapeModificationOptions.allOptions = dataForList;
					break;
				}
				case 4: 	{	indexView = 2;
					//etapeResiliationLigne.listAResilier = dataForList;
					break;
				}
				default:
				{ 
					indexView = 0;
					//etapeDetailsCommande.listArticles 	= dataForList;
					break;
					
				}
			}
		}
		
	}
}