package univers.inventaire.commande.system
{
	import flash.events.Event;

	public class CommandeEvent extends Event
	{
		public static const COMMANDE_CREATED:String="commandeCreated";
		public static const COMMANDE_UPDATED:String="commandeUpdated";
		public static const COMMANDE_DELETED:String="commandeDeleted";
		
		private var _type:String;
		private var _bubbles:Boolean;
		private var _cancelable:Boolean;
		
		public var commande:Commande;
				
		public function CommandeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_type = type;
			_bubbles = bubbles;
			_cancelable = cancelable;
			
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new CommandeEvent(_type,_bubbles,_cancelable);
		}
		
	}
}