<cfcomponent 
	displayname="CompteFacturationOperateurServices" 
	output="false">

	<!--- 
		Fournis la liste des destinataires (= liste des contacts distributeurs ayant un role facturation).
	 --->
	<cffunction name="getListeDestinataires" access="remote" returntype="query" output="false" hint="Fournis la liste des destinataires (= liste des contacts distributeurs ayant un role facturation).">		
		<cfargument name="idrevendeur" required="true" type="numeric"/>
		<cfset user = Session.USER.CLIENTACCESSID>
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset role = 1>   <!--- role facturation --->
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getcontactsrolesrevendeur_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#user#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#role#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Fournis la liste des comptes et sous comptes pour un gestionnaire, un pool et un opérateur donné.
	 --->
	<cffunction name="getListeComptesOperateurByLoginAndPool" access="remote" returntype="Query" output="false" hint="Fournis la liste des comptes et sous comptes pour un gestionnaire, un pool et un opérateur donné.">
		<cfargument name="idOperateur" 	required="true" type="numeric"/>
		<cfargument name="idPool" 		required="true" type="numeric"/>
		<cfset user = Session.USER.CLIENTACCESSID>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getcompteopeloginpool">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#user#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idPool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getCompteOpeCompte" access="remote" returntype="any" >
		<cfargument name="idGroupe" type="Numeric" required="true">
		<cfargument name="idoperateur" type="Numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.getCompteOpeCompte">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idoperateur#" null="false">
		<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>

	<!--- 
		Fournis la liste des comptes et sous comptes des idsousTete passés en parametre
	 --->	
	<cffunction name="getCompteLstLignes" access="remote" returntype="query" hint="Fournis la liste des comptes et sous comptes des idsousTete passés en parametre">
		<cfargument name="LIGNES" required="true" type="String" />
		<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.getCompteLstLignes_v2">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#LIGNES#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qGetListeLignes">
		</cfstoredproc>
		<cfreturn qGetListeLignes>
	</cffunction>	
	
	<!--- 
		Envoie de mail
	 --->	
	<cffunction name="sendChangementCompteFacture" access="remote" returntype="numeric" output="true" hint="Envoie de mail">
		<cfargument name="body" required="true" type="any" />
		<cfargument name="destinataire" required="true" type="String" />
		<cfargument name="pieceJointe" required="true" type="String" />
		<cfargument name="copie" required="true" type="boolean" />
		<cfargument name="xmlHistoric" required="true" type="String" />
		<cfargument name="destinataire2" required="false" type="String" />
		<cfargument name="destinataireCopie" required="true" type="String" />
		
		<cfset var p_retour = -1>

		<cflog text="#SESSION.CODEAPPLICATION#">

		<cfif SESSION.CODEAPPLICATION eq 101>
			<cfset mailSender= "no-reply@mytem360.com">
		<cfelseif SESSION.CODEAPPLICATION eq 51>	
			<cfset mailSender= "no-reply@sfrbusinessteam.fr'">
		<cfelseif SESSION.CODEAPPLICATION eq 1>
			<cfset mailSender= "no-reply@consotel.fr">
		<cfelse>
			<cfset mailSender= "#SESSION.USER.EMAIL#">
		</cfif>	
		
		<cfset str1 = 'SIZE="10"'>
		<cfset str2 = 'SIZE="2"'>		
		<cfset message1 = Replace(body,str1,str2,"all")>

		<cfset str1 = "&apos;">
		<cfset str2 = "'">
		<cfset message = Replace(message1,str1,str2,"all")>
		
		<cfset createExcelFile(pieceJointe)>

		<cfset pathRep="./Temp">
		<cfif  Not DirectoryExists("#expandPath(pathRep)#")>
            <cfdirectory action = "create" directory="#expandPath(pathRep)#" mode="777" />
        </cfif>
		
		<cfset fileExpandPath2 = expandPath("./Temp/Lignes_A_Changer.xls")>

		<cftry>
			
			<!--- création de la pièce jointe --->
			<!--- <cfset createCsvFile(pieceJointe)>  --->
				<!--- envoie du mail --->
				<cfif destinataireCopie neq "">	 
					<cflog text="Envoi de mail au destinataire et à la cc" >  	
                  <cfmail 	from="#mailSender#" 
				  			to="#destinataire#" cc="#destinataireCopie#"
				  			server="mail-cv.consotel.fr" port="25" 
						  	type="text/html" charset="utf-8" 
                            subject="Demande de changement de compte facturation Opérateur">
                        
                        <cfmailparam file="#fileExpandPath2#" type="text/plain">
                              
                        <html lang='fr'>
                        <head>
                        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                        <body>                                                     
                              #message#
                        </body>
                        </html>
                  </cfmail>
                  <cfelse>
				  	  <cfmail 	from="#mailSender#" 
				  			to="#destinataire#"
				  			server="mail-cv.consotel.fr" port="25"
						  	type="text/html" charset="utf-8" 
                            subject="Demande de changement de compte facturation Opérateur">
                        
                        <cfmailparam file="#fileExpandPath2#" type="text/plain">
                              
                        <html lang='fr'>
                        <head>
                        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                        <body>                                                     
                              #message#
                        </body>
                        </html>
                  </cfmail>
				</cfif>
                  <cfif copie IS TRUE>
                        <cfmail from="#mailSender#" to="#destinataire2#" server="mail-cv.consotel.fr" port="25" type="text/html" 
                              subject="Demande de changement de compte facturation Opérateur" charset="utf-8" >
                        
                              <cfmailparam file="#fileExpandPath2#" type="text/plain">
                                   
                              <html lang='fr'>

					<head>
					<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
					<body>									
						#message#
					</body>
					</html>
				</cfmail>
			</cfif>
					
			<!--- écriture de l''historique --->
 			<cfset myXML = Xmlparse(xmlHistoric)>
			<cfloop	
				index="intNodeIndex"
				from="1"
				to="#ArrayLen( myXML.lignes.ligneChangement )#"
				step="1">
					
				<cfset myIdLigne = myXML.lignes.ligneChangement[intNodeIndex].idligne.XmlText />
				<cfset myLigne = myXML.lignes.ligneChangement[intNodeIndex].ligne.XmlText />
				<cfset myItem1 = myXML.lignes.ligneChangement[intNodeIndex].item1.XmlText />
				<cfset myItem2 = myXML.lignes.ligneChangement[intNodeIndex].item2.XmlText />
				<cfset myDate = myXML.lignes.ligneChangement[intNodeIndex].date.XmlText />
				
				<cfset logAction(23,1, myLigne &' : '& myItem1,myItem2,-1,-1,-1,myIdLigne,SESSION.PERIMETRE.ID_GROUPE,-2,myDate)/>
			</cfloop>
			<cfset p_retour = 1>
			<cfcatch>
				<cfset p_retour = -1> 
			</cfcatch>
		</cftry>
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Creation du fichier CSV spécifique pour le changement de compte facturation
	 --->	
	<cffunction name="createCsvFile" access="remote" returntype="numeric" output="true">
		<cfargument name="csvString" required="true" type="String" />
		
		<cfset pathRep="./Temp">
		<cfif  Not DirectoryExists("#expandPath(pathRep)#")>
            <cfdirectory action = "create" directory="#expandPath(pathRep)#" mode="777" />
        </cfif>
		<cfset p_retour = -1>
		<cfset pathName = expandPath ("./Temp/Lignes_A_Changer.xls")>
		
		<cftry>
			<!--- Write the csv String to the file. --->
			<cffile
				action="WRITE"
				file="#pathName#"
				output="#csvString#" />
				
			<cfset p_retour = 1>
			<cfcatch>
				<cfset p_retour = -1>
			</cfcatch>
		</cftry>
		<cfreturn p_retour>
	</cffunction>		

	<!--- 
		Creation d'un fichier Excel spécifique pour le changement de compte facturation
	 --->
	 <cffunction name="createExcelFile" access="remote" returntype="void" hint="Creation d'un fichier Excel spécifique pour le changement de compte facturation">
		<cfargument name="pieceJointe" required="true" type="String" />
		
		<cfset nbColonne = 5 >
		<cfset theCSV = pieceJointe >
		<cfset theCSVTmp = "">
		<cfset arrayColonnes = ArrayNew(1) >
		<cfset index=1>
		<cfloop condition="#index# LTE #nbColonne#">
			<cfset placePointVirgule = Find(";",theCSV)>
			<cfset ArrayAppend(arrayColonnes , Mid(theCSV, 1, placePointVirgule-1))>
			<cfset theCSVTmp = Mid(theCSV, placePointVirgule+1, Len(theCSV))>
			<cfset theCSV = theCSVTmp>
			<cfset index=index+1>
		</cfloop>
			
		<cfset queryForPieceJointe = queryNew( "" ) />
		<cfset queryAddColumn(queryForPieceJointe,Replace(arrayColonnes[1]," ","_","all"),"VarChar",listToArray(""))/> <!--- colonne ligne --->
		<cfset queryAddColumn(queryForPieceJointe,Replace(arrayColonnes[2]," ","_","all"),"VarChar",listToArray(""))/> <!--- colonne compte actuel --->
		<cfset queryAddColumn(queryForPieceJointe,Replace(arrayColonnes[3]," ","_","all"),"VarChar",listToArray(""))/> <!--- colonne sous compte actuel --->
		<cfset queryAddColumn(queryForPieceJointe,Replace(arrayColonnes[4]," ","_","all"),"VarChar",listToArray(""))/> <!--- colonne compte cible --->
		<cfset queryAddColumn(queryForPieceJointe,Replace(arrayColonnes[5]," ","_","all"),"VarChar",listToArray(""))/> <!--- colonne sous compte cible --->
		
		<cfdump var="#arrayColonnes#">
		
		<cfset numeroColonne = 1>
		<cfloop condition="#Len(theCSV)# GT 1">
			<cfset index=1>
			<cfset QueryAddRow(queryForPieceJointe)>
			<cfloop condition="#index# LTE #nbColonne#">
				<cfset placePointVirgule = Find(";",theCSV)>
				<cfif placePointVirgule-1 GT 1>
					<cfset QuerySetCell(queryForPieceJointe,Replace(arrayColonnes[index]," ","_","all"),Mid(theCSV, 1, placePointVirgule-1))>
					<cfset theCSVTmp = Mid(theCSV, placePointVirgule+1, Len(theCSV))>
					<cfset theCSV = theCSVTmp>
				</cfif>
				<cfset index=index+1>
			</cfloop>
			<cfset numeroColonne = numeroColonne + 1>
		</cfloop>
		
		<cfset pathRep="./Temp">
		<cfif  Not DirectoryExists("#expandPath(pathRep)#")>
            <cfdirectory action = "create" directory="#expandPath(pathRep)#" mode="777" />
        </cfif>
		
		<cfset objPOI = CreateObject("component", "POIUtility").Init() />
		<cfset objPOI.WriteSingleExcel(expandPath ("./Temp/Lignes_A_Changer.xls"), queryForPieceJointe,
				"Ligne,Compte_actuel,Sous_compte_actuel,Compte_cible,Sous_compte_cible",
				"Ligne,Compte actuel,Sous compte actuel,Compte cible,Sous compte cible")>
			
	</cffunction>
	
	<!--- 
		Gestion de l'historique
	 --->	
	<cffunction name="logAction" access="remote" returntype="void" hint="Gestion de l'historique">
		
		<cfargument name="ID_WORDS" required="true" type="Numeric" />
		<cfargument name="ACTION_PRINCIPALE" required="true" type="String" />
		<cfargument name="ITEM1" required="true" type="String" />
		<cfargument name="ITEM2" required="true" type="String" />
		<cfargument name="ID_TERMINAL" required="true" type="Numeric" />
		<cfargument name="ID_SIM" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE" required="true" type="Numeric" />
		<cfargument name="IDGROUPE_RACINE" required="true" type="Numeric" />
		<cfargument name="UID_ACTION" required="true" type="String" />
		<cfargument name="DATE_EFFET" required="true" type="String" />
		<cfargument name="COMMENTAIRE" required="false" type="String" default=-1/>
		<cfargument name="ID_CAUSE" required="false" type="Numeric" default=-1/>
		<cfargument name="ID_STATUT" required="false" type="Numeric" default=-1 />
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset USER = SESSION.USER.CLIENTACCESSID>

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.log_action_flotte_v2">
			<cfprocparam value="#USER#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_WORDS#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ACTION_PRINCIPALE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ITEM1#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ITEM2#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#ID_TERMINAL#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_SIM#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDSOUS_TETE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDGROUPE_CLIENT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#UID_ACTION#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#DATE_EFFET#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfif COMMENTAIRE neq -1>
			<cfprocparam value="#COMMENTAIRE#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfelse>
				<cfprocparam  value="#COMMENTAIRE#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfif ID_CAUSE neq -1>
				<cfprocparam value="#ID_CAUSE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfelse>
				<cfprocparam  value="#ID_CAUSE#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			 <cfif ID_STATUT neq -1>
			<cfprocparam value="#ID_STATUT#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfelse>
			<cfprocparam  value="#ID_STATUT#" cfsqltype="CF_SQL_INTEGER" null="true">
			</cfif>
			<cfprocparam variable="logStatus" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>			
	</cffunction>
	
</cfcomponent>