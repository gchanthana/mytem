package catalogue.service
{
import mx.collections.ArrayCollection;
import mx.resources.ResourceManager;
import mx.rpc.AbstractOperation;
import mx.rpc.events.ResultEvent;

import catalogue.event.CatalogueEvent;
import catalogue.strategie.strategieconcrete.AbstractCatalogueCS;
import catalogue.vo.AbonnementVO;
import catalogue.vo.EquipementCatalogueVO;
import catalogue.vo.ParametreSearchCatalogueVO;

import composants.util.ConsoviewUtil;

import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

import paginatedatagrid.pagination.vo.ItemIntervalleVO;

	[Bindable]
	public class ImportService extends AbstractCatalogueCS
	{ 
		
		private var _listeEquipementCatalogue:ArrayCollection = new ArrayCollection;
		//private var _listeAbonnementCatalogue:ArrayCollection = new ArrayCollection;
		
		public function ImportService()
		{
		}
		public function rechercheListeEquipement(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Revendeur",
							"searchEquipConstrImport",
							rechercheEquipementConstructeur_handler,null);
			
			RemoteObjectUtil.callService(opData,
												parametresRechercheVO.SEARCH_TEXT,
												parametresRechercheVO.ORDER_BY,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,
												parametresRechercheVO.IDCONSTRUCTEUR,
												parametresRechercheVO.IDTYPEEQUIPEMENT,
												parametresRechercheVO.IDCLASSE,
												parametresRechercheVO.IDREVENDEUR,
												parametresRechercheVO.IDSEGMENT
												);
		}
		public function rechercheListeEquipementRevendeur(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"searchEquipRevendeurImport",
							rechercheEquipementConstructeur_handler,null);
			
			RemoteObjectUtil.callService(opData,
												parametresRechercheVO.SEARCH_TEXT,
												parametresRechercheVO.ORDER_BY,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,
												parametresRechercheVO.IDCONSTRUCTEUR,
												parametresRechercheVO.IDTYPEEQUIPEMENT,
												parametresRechercheVO.IDCLASSE,
												parametresRechercheVO.IDREVENDEUR,
												parametresRechercheVO.IDCLIENT,
												parametresRechercheVO.IDSEGMENT
												);
		}
		public function rechercheAbonnementCatalogueImport(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Abonnement",
							"searchAboOperateurImport",
							rechercheAbonnementCatalogueImport_handler,null);
			
			RemoteObjectUtil.callService(opData,
												parametresRechercheVO.SEARCH_TEXT,
												parametresRechercheVO.ORDER_BY,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,
												parametresRechercheVO.IDOP,
												parametresRechercheVO.IDCLIENT
												);
		}
		public function rechercheAbonnementCatalogueImport_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeAbonnementCatalogue = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : AbonnementVO = new AbonnementVO();
				
				item.IDABO= tmpArr[i].IDABONNEMENT;
				item.TYPE= tmpArr[i].TYPE;
				item.LIBELLE= tmpArr[i].LIBELLE;
				item.NBRECORD= tmpArr[i].NBRECORD;
				item.THEME= tmpArr[i].THEME;    
				       
				listeAbonnementCatalogue.addItem(item);
			}
		}
		public function importXEquipInCatRevendeur(idRevendeur:Number,listeEquip : ArrayCollection):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Revendeur",
							"addXEquiConsRevendeur",
							importXEquipInCatRevendeur_handler,null);
			
			//Fixer le bug des value object dans CF
			var obj : Object;
			var col : ArrayCollection = new ArrayCollection();
			for(var i :int;i<listeEquip.length;i++)
			{
				obj = new Object();
				obj.IDEQUIPEMENT = listeEquip.getItemAt(i).IDEQUIPEMENT;
				obj.MODELE = listeEquip.getItemAt(i).MODELE;
				col.addItem(obj);
			}
			
			
			
			RemoteObjectUtil.callService(opData,
												idRevendeur,
												col
												
												);
		}
		public function importXEquipInCatClient(idClient:Number,listeEquip : ArrayCollection):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"addXEquiClient",
							importXEquipInCatClient_handler,null);
			
			//Fixer le bug des value object dans CF
			var obj : Object;
			var col : ArrayCollection = new ArrayCollection();
			for(var i :int;i<listeEquip.length;i++)
			{
				obj = new Object();
				obj.IDEQUIPEMENT = listeEquip.getItemAt(i).IDEQUIPEMENT;
				col.addItem(obj);
			}
			
			RemoteObjectUtil.callService(opData,
												idClient,
												col
												);
		}
		public function importXAboInCatClient(idClient:Number,listeAbo : ArrayCollection):void
		{
			
			var listeIdAbos:String = ConsoviewUtil.arrayToList(listeAbo,"IDABO",",");
			
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Abonnement",
							"addXAboClient",
							importXAboInCatClient_handler,null);
			
			RemoteObjectUtil.callService(	opData,
											idClient,
											listeIdAbos
										);
		}
		public function importXAboInCatClient_handler(re : ResultEvent):void
		{
			
			dispatchEvent(new CatalogueEvent(CatalogueEvent.ADD_ABO_COMPLETE));
			
		}
		public function importXEquipInCatRevendeur_handler(re : ResultEvent):void
		{
			if(re.result)
			{				
				var temp : ArrayCollection = new ArrayCollection(re.result as Array);
				var evtObj : CatalogueEvent = new CatalogueEvent(CatalogueEvent.IMPORT_EQUIPEMENT_COMPLETE);
				evtObj.objectReturn = temp;
				dispatchEvent(evtObj);
			}
		}
		public function importXEquipInCatClient_handler(re : ResultEvent):void
		{
			if(re)
			{
				dispatchEvent(new CatalogueEvent(CatalogueEvent.IMPORT_EQUIPEMENT_COMPLETE));
			}
		}
		
		private function rechercheEquipementConstructeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeEquipementCatalogue = new ArrayCollection();
			var  item : EquipementCatalogueVO;
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				item = new EquipementCatalogueVO();
						
				item.IDEQUIPEMENT=tmpArr[i].IDEQUIPEMENT;
				item.REFERENCE=tmpArr[i].REFERENCE;
				item.REFCONSTRUCTEUR=tmpArr[i].REFERENCECONSTRUCTEUR;
				item.TYPE=tmpArr[i].TYPE;
				item.CLASSE=tmpArr[i].CLASSE;
				item.TYPE_CREATION = tmpArr[i].TYPE_CREATION;
				item.ORIGINE=(item.TYPE_CREATION == 1)?ResourceManager.getInstance().getString('M24','Manuel'):tmpArr[i].ORIGINE;				
				item.MODELE=tmpArr[i].MODELE;
				item.NBRECORD=tmpArr[i].NBRECORD;
				item.IDCONSTRUCTEUR = tmpArr[i].IDCONSTRUCTEUR;
				item.IDCLASSE = tmpArr[i].IDCATEGORIE;
				item.IDTYPE = tmpArr[i].IDTYPE;
				
				item.PATH_LOW = tmpArr[i].PATH_LOW;
				item.PATH_THUMB = tmpArr[i].PATH_THUMB;
				
		
				listeEquipementCatalogue.addItem(item);
			}
		}
	/* 	private function rechercheEquipementConstructeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeEquipementCatalogue = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : EquipementCatalogueVO = new EquipementCatalogueVO();
						
				item.IDEQUIPEMENT=tmpArr[i].IDEQUIPEMENT;
				item.REFERENCE=tmpArr[i].REFERENCE;
				item.REFCONSTRUCTEUR=tmpArr[i].REFERENCECONSTRUCTEUR;
				item.TYPE=tmpArr[i].TYPE;
				item.CLASSE=tmpArr[i].CLASSE;
				item.ORIGINE=tmpArr[i].ORIGINE;
				item.MODELE=tmpArr[i].MODELE;
				item.NBRECORD=tmpArr[i].NBRECORD;
				item.IDCONSTRUCTEUR = tmpArr[i].IDCONSTRUCTEUR;
				item.IDCLASSE = tmpArr[i].IDCATEGORIE;
				item.IDTYPE = tmpArr[i].IDTYPE;
		
				listeEquipementCatalogue.addItem(item);
			}
		} */
		
	
		public function set listeEquipementCatalogue(value:ArrayCollection):void
		{
			_listeEquipementCatalogue = value;
		}

		public function get listeEquipementCatalogue():ArrayCollection
		{
			return _listeEquipementCatalogue;
		}
	
	}
}