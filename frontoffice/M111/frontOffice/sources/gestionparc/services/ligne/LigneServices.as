package gestionparc.services.ligne
{
	import mx.rpc.AbstractOperation;
	
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.FileUpload;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;

	/**
	 * Appelle les procédures liées aux lignes.
	 */
	public class LigneServices
	{
		
		public var myDatas 		: LigneDatas;
		public var myHandlers 	: LigneHandlers;
		
		public function LigneServices()
		{
			myDatas 	= new LigneDatas();
			myHandlers 	= new LigneHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - Les informations de la ligne.
		 * - Ses abo et options.
		 * - Sa SIM.
		 * - Ses organisations.
		 */	
		public function getInfosFicheLigne(idsoustete:Number = -1, idrow:Number = -1):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"getInfosFicheLigne",
				myHandlers.getInfosFicheLigneResultHandler);
			
			RemoteObjectUtil.callService(op, (idsoustete == -1)?SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE:idsoustete, 
				CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,
				(idrow == -1)?SpecificationVO.getInstance().elementDataGridSelected.ID:idrow);
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Les informations de la ligne.
		 * - Ses abo et options.
		 * - Sa SIM.
		 * - Ses organisations.
		 */	
		public function setInfosFicheLigne(createObjToServiceSet : Object):void
		{
			var idPool : Number = 0; 
			
			if(SpecificationVO.getInstance().idPool > 0)
			{
				idPool = SpecificationVO.getInstance().idPool;	
			}
			else if(SpecificationVO.getInstance().idPoolNotValid > 0)
			{
				idPool = SpecificationVO.getInstance().idPoolNotValid;	
			}
			else
			{
				idPool = 0;
			}
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"setInfosFicheLigne",
				myHandlers.setInfosFicheLigneResultHandler);
			
			RemoteObjectUtil.callService(op, 
				createObjToServiceSet.myLigne.idSousTete,
				createObjToServiceSet.myLigne.sousTete,
				createObjToServiceSet.myLigne.operateur.idOperateur,-1,
				(createObjToServiceSet.myLigne.fournisseur)?createObjToServiceSet.myLigne.fournisseur.idRevendeur:-1,idPool,
				(createObjToServiceSet.idGpeClient)?createObjToServiceSet.idGpeClient:-1,
				createObjToServiceSet.myLigne.fonction,
				createObjToServiceSet.myLigne.idCompte,
				createObjToServiceSet.myLigne.idSousCompte,
				(createObjToServiceSet.myLigne.payeur)?1:0,
				(createObjToServiceSet.myLigne.titulaire)?1:0,
				createObjToServiceSet.myLigne.typeLigne,
				createObjToServiceSet.myLigne.idTypeLigne,
				createObjToServiceSet.myLigne.idSite,
				createObjToServiceSet.myLigne.idEmplacement,
				createObjToServiceSet.myLigne.simRattache.numSerieEquipement,
				createObjToServiceSet.myLigne.simRattache.PIN1,
				createObjToServiceSet.myLigne.simRattache.PIN2,
				createObjToServiceSet.myLigne.simRattache.PUK1,
				createObjToServiceSet.myLigne.simRattache.PUK2,
				createObjToServiceSet.myLigne.texteChampPerso1,
				createObjToServiceSet.myLigne.texteChampPerso2,
				createObjToServiceSet.myLigne.texteChampPerso3,
				createObjToServiceSet.myLigne.texteChampPerso4,
				createObjToServiceSet.myLigne.idContrat,
				createObjToServiceSet.myLigne.numContrat,
				createObjToServiceSet.myLigne.dureeContrat,
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myLigne.dateOuverture),
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myLigne.dateRenouvellement),
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myLigne.dateResiliation),
				createObjToServiceSet.myLigne.dureeEligibilite,
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myLigne.dateEligibilite),
				DateFunction.formatDateAsInverseString(createObjToServiceSet.myLigne.dateFPC),
				createObjToServiceSet.myLigne.commentaires,
				createObjToServiceSet.xmlListeAboOpt,
				createObjToServiceSet.myLigne.etat,
				createObjToServiceSet.xmlListeOrga,
				createObjToServiceSet.myLigne.isLignePrincipale,
				createObjToServiceSet.myLigne.isLigneBackUp,
				createObjToServiceSet.myLigne.operateurPreselect.idOperateur,
				createObjToServiceSet.xmlListeCablageCanaux,
				(createObjToServiceSet.insertPoolDistrib > 0)?1:0,
				(createObjToServiceSet.insertPoolSociete > 0)?1:0,
				0,//createObjToServiceSet.BOOLNUMBERHASCHANGED
				"06-test",//createObjToServiceSet.oldNumero
				createObjToServiceSet.myLigne.codeRio);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste des abonnements et options.
		 */	
		public function fournirListeAbonnementsOptions(idOperateur:int,idProfilEquipement:int,isMobile:int = 1, isFixe:int = 0): void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"fournirListeAbonnementsOptions",
				myHandlers.fournirListeAbonnementsOptionsResultHandler);
			
			RemoteObjectUtil.callService(op,idOperateur,idProfilEquipement,isMobile,isFixe);	
		}
		
		/**
		 * Appelle la procédure permettant d'ajouter un produit a ses favoris.
		 */	
		public function ajouterProduitAMesFavoris(value:Object):void
		{
			if (value.hasOwnProperty("IDPRODUIT_CATALOGUE") && value.IDPRODUIT_CATALOGUE > 0)
			{
				var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M111.fiches.FicheLigne",
					"ajouterProduitAMesFavoris",
					myHandlers.ajouterProduitAMesFavorisResultHandler);
				
				RemoteObjectUtil.callService(op,value.IDPRODUIT_CATALOGUE);
			}
		}
		
		/**
		 * Appelle la procédure permettant de supprimer un produit de ses favoris.
		 */	
		public function supprimerProduitDeMesFavoris(value:Object):void
		{
			if (value.hasOwnProperty("IDPRODUIT_CATALOGUE") && value.IDPRODUIT_CATALOGUE > 0)
			{
				var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M111.fiches.FicheLigne",
					"supprimerProduitDeMesFavoris",
					myHandlers.supprimerProduitDeMesFavorisResultHandler);
				
				RemoteObjectUtil.callService(op,value.IDPRODUIT_CATALOGUE);
			}
		}
		
		/**
		 * Appelle la procédure permettant de recupere toutes les lignes/sim sans terminaux
		 */	
		public function listeLigneSimSansTerm(createObjToServiceGet :Object):void
		{
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"listeLigneSimSansTerm",
				myHandlers.listeLigneSimSansTermResultHandler);
			
			RemoteObjectUtil.callService(op,createObjToServiceGet.idPool);
		}
		
		/**
		 * Appelle la procédure permettant de recupere toutes les lignes/fixes sans équipements
		 */	
		public function listeLigneFixeSansEqp(createObjToServiceGet :Object):void
		{
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"listeLigneFixeSansEqp",
				myHandlers.listeLigneFixeSansEqpResultHandler);
			
			RemoteObjectUtil.callService(op,SpecificationVO.getInstance().idPool);
		}
		
		/**
		 * Appelle la procédure permettant de recupere toutes les lignes sans équipement
		 */	
		public function listeLigneSansTerm(createObjToServiceGet :Object):void
		{
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"listeLigneSansTerm",
				myHandlers.listeLigneSansTermResultHandler);
			
			RemoteObjectUtil.callService(op,createObjToServiceGet.idPool);
		}
		
		/**
		 * Appelle la procédure permettant de recupere les informations sur la colonne "facturé" du datagrid abo et opt
		 */	
		public function majColFacture(objToGetService:Object):void
		{
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"majColFacture",
				myHandlers.majColFactureResultHandler);
			
			RemoteObjectUtil.callService(op,objToGetService.idSousTete);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste des actions de raccordement
		 */
		public function getActionsPossiblesByEtat(objToGetService:Object):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
				"getActionsPossiblesByEtat",
				myHandlers.getActionsPossiblesByEtatResultHandler);
			RemoteObjectUtil.callService(op,(objToGetService.idTypeRaccordement > 0)?objToGetService.idTypeRaccordement:4);	
		}
		
		/**
		 * Appelle la procédure permettant de faire le raccordement
		 */
		public function doActionRaccordement(objToGetService:Object):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
				"doActionRaccordement",
				myHandlers.doActionRaccordementResultHandler);

			RemoteObjectUtil.callService(op,
				objToGetService.idSousTete,
				objToGetService.idActionRaccordement,
				objToGetService.historiqueActions,
				objToGetService.idTypeRaccordement,
				objToGetService.dateRaccordement,"","",
				objToGetService.idOperateur,
				objToGetService.arrayASortir,
				objToGetService.arrayAEntrer);	
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste des lignes disponibles pour un groupement.
		 */
		public function getTeteLigne(objToGetService:Object):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
				"getTeteLigne",
				myHandlers.getTeteLigneResultHandler);
			RemoteObjectUtil.callService(op,objToGetService.idSousTete);	
		}
		public function getTeteLigneViaSouscompte(objToGetService:Object):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"getTeteLigneViaSouscompte",
				myHandlers.getTeteLigneResultHandler);
			RemoteObjectUtil.callService(op,objToGetService.idSousCompte);	
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste des lignes du groupement.
		 */
		public function getLignesGroupement(objToGetService:Object):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
				"getLignesGroupement",
				myHandlers.getLignesGroupementResultHandler);
			RemoteObjectUtil.callService(op,objToGetService.idSousTete);	
		}
		public function resilierLigne():void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"resilierLigne",
				myHandlers.resilierLigneResultHandler);
			RemoteObjectUtil.callService(op);
		}
		public function renumeroterLigne(objToGetService:Object):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"renumeroterLigne",
				myHandlers.renumeroterLigneResultHandler);
			RemoteObjectUtil.callService(op,objToGetService);
		}
		
		public function updateEtatLigne(row:AbstractMatriceParcVO, idetat:Number):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"updateEtatLigne",
				myHandlers.updateEtatLigneResultHandler);
				myDatas.row = row;
			RemoteObjectUtil.callService(op,row.SOUS_TETE,row.IDSOUS_TETE,idetat);
		}
		
		public function getListeDestinatairesEchangeSIM(idRevendeur:Number):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"getListeDestinataires",
				myHandlers.getListeDestinatairesEchangeSIM);
			RemoteObjectUtil.callService(op,idRevendeur);
		}
		
		public function sendEmailEchangeSimStock(infoMail:Object):void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"sendEmailEchangeSimStock",
				myHandlers.sendEmailEchangeSimStock);
			RemoteObjectUtil.callService(op,infoMail, 1);
		}
		
	}
}