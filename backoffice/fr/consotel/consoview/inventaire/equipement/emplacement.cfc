<cfcomponent name="equipement">

	<cffunction name="getFicheSite" returntype="query" access="remote" output="true">
		<cfargument name="idgroupe" type="string">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.FicheSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgroupe#">
			<cfprocresult name="results">
		</cfstoredproc>
		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.session")>
		<cfset obj.setData(results,"ficheSite")>
		<cfreturn results>
	</cffunction>
	
	<cffunction name="getFicheEmplacement" returntype="query" access="remote" output="true">
		
		<cfargument name="idEmplacement" type="string">
		<cfargument name="idgroupe" type="string">
		 
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.ficheemplacement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmplacement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgroupe#">
			<cfprocresult name="results">
		</cfstoredproc>
		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.session")>
		<cfset obj.setData(results,"ficheEmplacement")>
		<cfreturn results>
		
	</cffunction>
	
	<cffunction name="getTypeEmplacement" returntype="query" access="remote" output="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.TypeEmplacement">
			<cfprocresult name="results">
		</cfstoredproc>
		<cfquery name="dataset" dbtype="query">
			select idtype_emplacement as data, type_emplacement as label
			from results
			order by idtype_emplacement
		</cfquery>
		<cfreturn dataset>
	</cffunction>
	
	<!--- Ajouter Commentaire  --->
	<cffunction name="updateFicheSite" returntype="numeric" access="remote" output="true">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Maj_FicheSite">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.dataInventaire.ficheSite.idsite_physique#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].NOM_SITE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].SP_REFERENCE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].SP_CODE_INTERNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].SP_ADRESSE1#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].SP_ADRESSE2#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].SP_CODE_POSTAL#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].SP_COMMUNE#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].SP_COMMENTAIRE#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="updateFicheEmplacement" returntype="numeric" access="remote" output="true">
		<cfargument name="id_groupe" type="numeric">
		<cfargument name="arr" type="array">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.maj_ficheemplacement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.dataInventaire.ficheEmplacement.idemplacement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.dataInventaire.ficheEmplacement.idsite_physique#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1].IDTYPE_EMPLACEMENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.dataInventaire.ficheEmplacement.idemplacement_parent#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].LIBELLE_EMPLACEMENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].REFERENCE_EMPLACEMENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].CODE_INTERNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#arr[1].COMMENTAIRE_EMPLACEMENT#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getEmplacement" returntype="query" access="private" output="true">
		<cfargument name="ID_PERIMETRE" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.arbre_emplacement">
	        <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_PERIMETRE#">
			<cfprocresult name="results">
		</cfstoredproc>
<!---		<cfset obj=createObject("component","fr.consotel.consoview.inventaire.session")>
		<cfset obj.setData(results,"ListeEmplacements")>--->
		<cfreturn results>
	</cffunction>

	<cffunction name="getEmplacementXml" access="remote" output="true" returntype="xml">
		<cfargument name="ID_PERIMETRE" type="string">
		<cfset sourceQuery = getEmplacement(ID_PERIMETRE)>
		<cfquery name="dataset" dbtype="query">
			select IDGROUPE_CLIENT, IDSITE_PHYSIQUE, IDTYPE_EMPLACEMENT, NOM_SITE,
					IDEMPLACEMENT, LIBELLE_EMPLACEMENT, IDEMPLACEMENT_PARENT,
					NODE_ID, NODE_NIV
			from sourceQuery
			order by idgroupe_client, node_niv, libelle_emplacement
		</cfquery>
		<!--- Arbre XML des Emplacements --->
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset rootNode.XmlAttributes.LABEL = "Choisir un emplacement">
		<cfset rootNode.XmlAttributes.VALUE = "">
		<cfset rootNode.XmlAttributes.NODE_ID = "0">
		<cfset rootNode.XmlAttributes.IDSITE_PHYSIQUE = "0">
		<cfset rootNode.XmlAttributes.IDTYPE_EMPLACEMENT = "0">
		<cfset rootNode.XmlAttributes.ACTIF = "0">
		<!--- HashMap : IDEMPLACEMENT :: Noeud XML correspondant --->
 		<cfset emplacementMap = structNew()>
		<!--- Index sur les groupes --->
		<cfset groupeIndex = 1>
		<cfoutput query="dataset" group="IDGROUPE_CLIENT">
			<!--- Création des noeuds groupes (NIVEAU 0 : Juste sous la racine) --->
			<cfset rootNode.xmlChildren[groupeIndex] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.xmlChildren[groupeIndex].xmlAttributes.LABEL = "#NOM_SITE#">
			<cfset rootNode.xmlChildren[groupeIndex].xmlAttributes.IDSITE_PHYSIQUE = "#IDSITE_PHYSIQUE#">
			<cfset rootNode.xmlChildren[groupeIndex].xmlAttributes.IDTYPE_EMPLACEMENT = "0">
			<cfset rootNode.XmlAttributes.VALUE = "">
			<cfset rootNode.XmlAttributes.NODE_ID = "0">
			<cfset rootNode.XmlAttributes.IDSITE_PHYSIQUE = "0">
			<cfset rootNode.XmlAttributes.IDTYPE_EMPLACEMENT = "0">
			<cfset rootNode.XmlAttributes.ACTIF = "0">
			<!--- Référence sur le noeud groupe en cours de traitement --->
			<cfset currentGroupChildNode = rootNode.xmlChildren[groupeIndex]>
			<!--- Index sur les enfants des groupes (NIVEAU 1 : Juste en dessous des groupes) --->
			<cfset groupeChildIndex = 1>
			<cfoutput group="NODE_NIV">
				<cfoutput>
					<!--- Si Niv 1 ie Juste en dessous des groupes (Donc enfants des groupes) --->
					<cfif NODE_NIV EQ 1 and IDEMPLACEMENT neq 0>
						<!--- On crée le noeud enfant et on le rajoute parmi les enfant du noeud groupe en cours --->
						<cfset currentGroupChildNode.xmlChildren[groupeChildIndex] = XmlElemNew(perimetreXmlDoc,"node")>
						<cfset currentGroupChildNode.xmlChildren[groupeChildIndex].xmlAttributes.LABEL = LIBELLE_EMPLACEMENT>
						<cfset currentGroupChildNode.xmlChildren[groupeChildIndex].xmlAttributes.VALUE = IDEMPLACEMENT>
						<cfset currentGroupChildNode.xmlChildren[groupeChildIndex].xmlAttributes.IDTYPE_EMPLACEMENT = IDTYPE_EMPLACEMENT>
						<cfset currentGroupChildNode.xmlChildren[groupeChildIndex].xmlAttributes.IDSITE_PHYSIQUE = IDSITE_PHYSIQUE>
						<cfset currentGroupChildNode.xmlChildren[groupeChildIndex].xmlAttributes.NODE_ID = NODE_ID>
						<cfset currentGroupChildNode.xmlChildren[groupeChildIndex].xmlAttributes.ACTIF = "1">
						<cfset emplacementMap[IDEMPLACEMENT] = structNew()>
						<cfset emplacementMap[IDEMPLACEMENT].NODE = currentGroupChildNode.xmlChildren[groupeChildIndex]>
						<cfset emplacementMap[IDEMPLACEMENT].NB_CHILD = 0>
						<!--- Le nombre de noeuds enfants de ce groupe a augmenté (Equivalent é l'index du noeud enfant) --->
						<cfset groupeChildIndex = groupeChildIndex + 1>
					<!--- Pour tous les noeuds qui ont un niveau en desous du niveau 1 (ie plus grand que 1) --->
					<cfelse>
						<!--- Si ce noeud est enfant d'un noeud déjé construit auparavant --->
				 		<cfif structKeyExists(emplacementMap,"#IDEMPLACEMENT_PARENT#")>
					 		<!--- Mise é jour du nombre d'enfants du noeud parent et ajout du noeud enfant --->
							<cfset emplacementMap[IDEMPLACEMENT_PARENT].NB_CHILD = emplacementMap[IDEMPLACEMENT_PARENT].NB_CHILD + 1>
					 		<cfset emplacementMap[IDEMPLACEMENT_PARENT].NODE.xmlChildren[emplacementMap[IDEMPLACEMENT_PARENT].NB_CHILD] =
														 				XmlElemNew(perimetreXmlDoc,"node")>
							<!--- Référence vers le noeud enfant précédement créé --->
							<cfset tmpParentChild =
									emplacementMap[IDEMPLACEMENT_PARENT].NODE.xmlChildren[emplacementMap[IDEMPLACEMENT_PARENT].NB_CHILD]>
							<cfset tmpParentChild.xmlAttributes.LABEL = LIBELLE_EMPLACEMENT>
							<cfset tmpParentChild.xmlAttributes.VALUE = IDEMPLACEMENT>
							<cfset tmpParentChild.xmlAttributes.IDTYPE_EMPLACEMENT = IDTYPE_EMPLACEMENT>
							<cfset tmpParentChild.xmlAttributes.NODE_ID = NODE_ID>
							<cfset tmpParentChild.xmlAttributes.IDSITE_PHYSIQUE = IDSITE_PHYSIQUE>
							<cfset tmpParentChild.xmlAttributes.ACTIF = "1">
							<!--- Entrée du noeud enfant créé précédement dans la HashMap des noeuds XML --->
							<cfset emplacementMap[IDEMPLACEMENT] = structNew()>
							<cfset emplacementMap[IDEMPLACEMENT].NB_CHILD = 0>
							<cfset emplacementMap[IDEMPLACEMENT].NODE = tmpParentChild>
						</cfif>
					</cfif>
				</cfoutput>
			</cfoutput>
			<!--- Passage au groupe suivant --->
			<cfset groupeIndex = groupeIndex + 1>
		</cfoutput>
		<cfdump var="#dataset#" label="dataset">
	    <cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="getGamme" returntype="query" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfif index neq "">
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.listegammegroupe">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index#" null="false">
				<cfprocresult name="results">
			</cfstoredproc>
		<cfelse>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.listegammegroupe">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="" null="true">
				<cfprocresult name="results">
			</cfstoredproc>
		</cfif>
		<cfreturn results>
	</cffunction>
	
	<cffunction name="getTypeXML" returntype="xml" access="remote" output="true">
		<cfset qResult=getType()>

		<cfquery dbtype="query" name="qGetType">
			select *
			from qResult
			order by IDCATEGORIE_EQUIPEMENT, type_equipement
		</cfquery>
		
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = "Sélectionner un type d'equipement">
		<cfset rootNode.XmlAttributes.NODE_ID = "0">
		<cfset rootNode.XmlAttributes.LEVEL = "1">
		<cfset rootNode.XmlAttributes.ACTIF = "0">
		<cfset rootNode.XmlAttributes.IDCATEGORIE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.IDTYPE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.TYPE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.CATEGORIE_EQUIPEMENT = "">
		<cfset rootNode.XmlAttributes.TE_CODE_IHM = "">

		<!---**************************************************************************************--->
		<cfset sourceQuery=qGetType>
		<cfset i=1>
		<cfset j=1>
		<cfset k=1>
		<cfset st=structNew()>
		<cfoutput group="IDCATEGORIE_EQUIPEMENT" query="sourceQuery">
			<cfset k=k+1>
			<cfset stFab=structNew()>
			<cfset stFab.node=XmlElemNew(perimetreXmlDoc,"node")>
			<cfset stFab.node.XmlAttributes.LABEL = sourceQuery['CATEGORIE_EQUIPEMENT'][i]>
			<cfset stFab.node.XmlAttributes.NODE_ID = k>
			<cfset stFab.node.XmlAttributes.LEVEL = "2">
			<cfset stFab.node.XmlAttributes.ACTIF = "0">
			<cfset stFab.node.XmlAttributes.IDCATEGORIE_EQUIPEMENT = sourceQuery['IDCATEGORIE_EQUIPEMENT'][i]>
			<cfset stFab.node.XmlAttributes.IDTYPE_EQUIPEMENT = "">
			<cfset stFab.node.XmlAttributes.TYPE_EQUIPEMENT = "">
			<cfset stFab.node.XmlAttributes.CATEGORIE_EQUIPEMENT = sourceQuery['CATEGORIE_EQUIPEMENT'][i]>
			<cfset stFab.node.XmlAttributes.TE_CODE_IHM = "">
			
			<cfset structInsert(st,i,stFab)>
			<cfoutput>
				<cfset k=k+1>
				<cfset tmp = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmp.XmlAttributes.LABEL = sourceQuery['TYPE_EQUIPEMENT'][i]>
				<cfset tmp.XmlAttributes.NODE_ID = k>
				<cfset tmp.XmlAttributes.LEVEL = "2">
				<cfset tmp.XmlAttributes.ACTIF = "1">
				<cfset tmp.XmlAttributes.IDCATEGORIE_EQUIPEMENT = sourceQuery['IDCATEGORIE_EQUIPEMENT'][i]>
				<cfset tmp.XmlAttributes.IDTYPE_EQUIPEMENT = sourceQuery['IDTYPE_EQUIPEMENT'][i]>
				<cfset tmp.XmlAttributes.TYPE_EQUIPEMENT = sourceQuery['TYPE_EQUIPEMENT'][i]>
				<cfset tmp.XmlAttributes.CATEGORIE_EQUIPEMENT = sourceQuery['CATEGORIE_EQUIPEMENT'][i]>
				<cfset tmp.XmlAttributes.TE_CODE_IHM = sourceQuery['TE_CODE_IHM'][i]>

				<cfset stFab.node.XmlChildren[j]=tmp>

				<cfset i=i+1>
				<cfset j=j+1>
			</cfoutput>
				<cfset j=1>
		</cfoutput>
		<cfset k=1>
		<cfset li=structKeyList(st)>

		<cfset sortedList=listsort(li,"numeric")>

		<cfloop list="#sortedList#" index="i">
			<cfset rootNode.XmlChildren[k]=st[i].NODE>
			<cfset k=k+1>
		</cfloop>

		<cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="getGammeXML" returntype="xml" access="remote" output="true">
		<cfargument name="index" type="numeric">
		<cfset qResult=getGamme(index)>

		<cfquery dbtype="query" name="qGetFab">
			select *
			from qResult
			order by type_fournisseur, nom_fournisseur, libelle_gamme
		</cfquery>
		
		<cfquery dbtype="query" name="qGetFour">
			select *
			from qResult
			where type_fournisseur<>0
			order by type_fournisseur, nom_fournisseur, libelle_gamme
		</cfquery>
		
		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = "Sélectionner un Fabriquant ou une Gamme">
		<cfset rootNode.XmlAttributes.NODE_ID = "0">
		<cfset rootNode.XmlAttributes.LEVEL = "1">
		<cfset rootNode.XmlAttributes.ACTIF = "0">
		<cfset rootNode.XmlAttributes.IDFOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.IDGAMME_FOURNIS = "">
		<cfset rootNode.XmlAttributes.IDGROUPE_CLIENT = "">
		<cfset rootNode.XmlAttributes.LIBELLE_GAMME = "">
		<cfset rootNode.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
		<cfset rootNode.XmlAttributes.NOM_FOURNISSEUR = "">
		<cfset rootNode.XmlAttributes.TYPE_FOURNISSEUR = "">

		<!---************************ TOUJOURS 2 GROUPEMENTS ORGANISATIONS ************************--->
		<cfif qGetFab.recordcount GT -1>
			<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Fabriquant">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NODE_ID = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.LEVEL = "2">
			<cfset rootNode.XmlChildren[1].XmlAttributes.ACTIF = "0">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDFOURNISSEUR = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGAMME_FOURNIS = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.IDGROUPE_CLIENT = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.LIBELLE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.NOM_FOURNISSEUR = "">
			<cfset rootNode.XmlChildren[1].XmlAttributes.TYPE_FOURNISSEUR = "">
			<cfset dataStruct.OP_ORGANISATIONS = rootNode.XmlChildren[1]>
		</cfif>
		
		<cfif qGetFour.recordcount GT -1>
			<cfset rootNode.XmlChildren[2] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[2].XmlAttributes.LABEL = "Fournisseur">
			<cfset rootNode.XmlChildren[2].XmlAttributes.NODE_ID = "0">
			<cfset rootNode.XmlChildren[2].XmlAttributes.LEVEL = "2">
			<cfset rootNode.XmlChildren[2].XmlAttributes.ACTIF = "0">
			<cfset rootNode.XmlChildren[2].XmlAttributes.IDFOURNISSEUR = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.IDGAMME_FOURNIS = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.IDGROUPE_CLIENT = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.LIBELLE_GAMME = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.NOM_FOURNISSEUR = "">
			<cfset rootNode.XmlChildren[2].XmlAttributes.TYPE_FOURNISSEUR = "">
			<cfset dataStruct.CLIENT_ORGANISATIONS = rootNode.XmlChildren[2]>
		</cfif>
		<!---**************************************************************************************--->
		<cfset sourceQuery=qGetFab>

		<!--- <cfdump var="#datastruct#"> --->
		<cfset i=1>
		<cfset j=1>
		<cfset st=structNew()>
		<cfset st1=structNew()>
		<cfoutput group="idfournisseur" query="sourceQuery">
			<cfset stFour=structNew()>
			<cfset stFour.node=XmlElemNew(perimetreXmlDoc,"node")>
			<cfset stFour.node.XmlAttributes.LABEL = sourceQuery['nom_fournisseur'][i]>
			<cfset stFour.node.XmlAttributes.NODE_ID = "0">
			<cfset stFour.node.XmlAttributes.LEVEL = "2">
			<cfset stFour.node.XmlAttributes.ACTIF = "1">
			<cfset stFour.node.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
			<cfset stFour.node.XmlAttributes.IDGAMME_FOURNIS = "">
			<cfset stFour.node.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
			<cfset stFour.node.XmlAttributes.LIBELLE_GAMME = "">
			<cfset stFour.node.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset stFour.node.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
			<cfset stFour.node.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
			
			<cfset stFab=structNew()>
			<cfset stFab.node=XmlElemNew(perimetreXmlDoc,"node")>
			<cfset stFab.node.XmlAttributes.LABEL = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.NODE_ID = "0">
			<cfset stFab.node.XmlAttributes.LEVEL = "2">
			<cfset stFab.node.XmlAttributes.ACTIF = "1">
			<cfset stFab.node.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
			<cfset stFab.node.XmlAttributes.IDGAMME_FOURNIS = "">
			<cfset stFab.node.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
			<cfset stFab.node.XmlAttributes.LIBELLE_GAMME = "">
			<cfset stFab.node.XmlAttributes.MODELE_COMMENTAIRE_GAMME = "">
			<cfset stFab.node.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
			<cfset stFab.node.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
			
			<cfif sourceQuery['type_fournisseur'][i] eq 0>
				<cfset structInsert(st,i,stFab)>
			<cfelseif sourceQuery['type_fournisseur'][i] eq 1>
				<cfset structInsert(st1,i,stFour)>
			</cfif>
			<cfoutput>
				<cfset tmp = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmp.XmlAttributes.LABEL = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.NODE_ID = "0">
				<cfset tmp.XmlAttributes.LEVEL = "2">
				<cfset tmp.XmlAttributes.ACTIF = "1">
				<cfset tmp.XmlAttributes.IDFOURNISSEUR = sourceQuery['idfournisseur'][i]>
				<cfset tmp.XmlAttributes.IDGAMME_FOURNIS = sourceQuery['idgamme_fournis'][i]>
				<cfset tmp.XmlAttributes.IDGROUPE_CLIENT = sourceQuery['idgroupe_client'][i]>
				<cfset tmp.XmlAttributes.LIBELLE_GAMME = sourceQuery['libelle_gamme'][i]>
				<cfset tmp.XmlAttributes.MODELE_COMMENTAIRE_GAMME = sourceQuery['modele_commentaire_gamme'][i]>
				<cfset tmp.XmlAttributes.NOM_FOURNISSEUR = sourceQuery['nom_fournisseur'][i]>
				<cfset tmp.XmlAttributes.TYPE_FOURNISSEUR = sourceQuery['type_fournisseur'][i]>
				<cfif sourceQuery['type_fournisseur'][i] eq 0 and sourceQuery['libelle_gamme'][i] neq "">
					<cfset stFab.node.XmlChildren[j]=tmp>
				<cfelseif sourceQuery['type_fournisseur'][i] eq 1 and sourceQuery['libelle_gamme'][i] neq "">
					<cfset stFour.node.XmlChildren[j]=tmp>
				</cfif>
				<cfset i=i+1>
				<cfset j=j+1>
			</cfoutput>
				<cfset j=1>
		</cfoutput>
		<cfset k=1>
		<cfset li=structKeyList(st)>
		<cfset li1=structKeyList(st1)>
		<cfset sortedList=listsort(li,"numeric")>
		<cfset sortedList1=listsort(li1,"numeric")>
		<cfloop list="#sortedList#" index="i">
			<cfset rootNode.XmlChildren[1].XmlChildren[k]=st[i].NODE>
			<cfset k=k+1>
		</cfloop>
		<cfset k=1>		
		<cfloop list="#sortedList1#" index="i">
			<cfset rootNode.XmlChildren[2].XmlChildren[k]=st1[i].NODE>
			<cfset k=k+1>
		</cfloop>
		<cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="insertEquipementCatalogue" returntype="string" access="remote">
		<cfargument name="arr" type="array">
		<cfif arr[3] eq 0>
			<cfset flag="true">
		<cfelse>
			<cfset flag="false">
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.add_equipementsfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[3]#" null="#flag#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="idequip">
		</cfstoredproc>
		<cfreturn idequip>
	</cffunction>
	
	<cffunction name="searchCatalogue" returntype="query" access="remote">
		<cfargument name="arr" type="array">
<!---

	PROCEDURE
	 
	 pkg_cv_equipement.SearchCatalogue
		p_idcategorie_equipement	IN INTEGER
		p_idtype_equipement			IN INTEGER
		p_idfournisseur				IN INTEGER
		p_idgamme_fournis			IN INTEGER
		p_chaine					IN VARCHAR2
		p_idgroupe_client			IN INTEGER
		p_retour 					OUT SYS_REFCURSOR
--->
		<cfset p_idcategorie_equipement = arr[1]>
		<cfset p_idtype_equipement = arr[2]>
		<cfset p_idfournisseur = arr[3]>
		<cfset p_idgamme_fournis = arr[4]>
		<cfset p_chaine = arr[5]>
		<cfset p_idgroupe_client = arr[6]>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.searchcatalogue">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
		
	</cffunction>
	
	<cffunction name="removeEquipementFab" returntype="numeric" access="remote">
		<cfargument name="arr" type="array">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.del_equipementsfournis">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_FLOAT" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="addEmplacement" returntype="Numeric" access="remote">
		 	<cfargument name="idSite" required="true" type="string" />
		 	<cfargument name="typeParent" required="true" type="string" />
		 	<cfargument name="idParent" required="true" type="string" />
		 	<cfargument name="id_groupe" required="true" type="string" />
		 	 
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.maj_FicheEmplacement">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSite#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#typeParent#" null="false">
				<cfif IsNumeric(idParent) >
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idParent#" null="false">
				<cfelse>
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idParent#" null="true">
				</cfif>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="untitled" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="deleteEmplacement" returntype="Numeric" access="remote">
		 	<cfargument name="idEmplacement" required="true" type="string" />
		 	<cfargument name="id_groupe" required="true" type="string" />
		 	 
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.Del_Emplacement">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEmplacement#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>

	<cffunction name="getLibelleSiteById" returntype="query" access="remote">
		 	<cfargument name="idsite" required="true" type="string" />
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.get_LibelleSite_ById">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsite#" null="false">
				<cfprocresult name="results">
			</cfstoredproc>
		<cfreturn results>
	</cffunction>
	
</cfcomponent>