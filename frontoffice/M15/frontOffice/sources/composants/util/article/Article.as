package composants.util.article
{
	import composants.util.ConsoviewFormatter;
	
	
	[Bindable]
	public class Article
	{	
		protected static const MOBILE:Number = 70;
	    protected static const SIM:Number = 71;
	    
		public function Article(article:XML=null)
		{
			//TO DO verifier la DTD de l'article
			if(article != null)
			{
				this._article = article;	
				equipements = article.equipements[0];
				ressources = article.ressources[0];
				
				calculTotal();
			}
				
		}
		
		protected var _idRevendeur:Number=0;
		public function set idRevendeur(value:Number):void
		{
			_idRevendeur = value;
			
		}
		public function get idRevendeur():Number
		{
			return _idRevendeur
		}
		
/* 		public static const SIM : String  = "SIM";
		public static const TER : String  = "TER";
		public static const ACC : String  = "ACC"; */
		
		protected var _sousTete:String="";
		protected var _codeInterne:String="";
		protected var _codeRio:String="";
		
		protected var _total:Number = 0;
		public function set total(value:Number):void{
			_total = value;
			if (!article.hasOwnProperty("total"))
			{
				article.appendChild(<total></total>);
			}
			article.total[0] = _total
			
		}
		
		public function get total():Number{
			return _total
		}
		
		
		protected var _article : XML = <article>
											<engagement_eq></engagement_eq>
											<engagement_res></engagement_res>
											<libelle></libelle>
											<total></total>				
											<soustete></soustete>
											<idsoustete></idsoustete>										
											<nomemploye></nomemploye>
											<idemploye></idemploye>
											<code_rio></code_rio>															
											<code_interne></code_interne>
											<equipements></equipements>
											<ressources></ressources>																
									   	</article>;
		
		protected var equipements:XML = article.equipements[0];
		protected var ressources:XML = article.ressources[0];
		
		protected var _boolValide:Boolean = false;
		public function get boolValide():Boolean
		{
			return _boolValide
		}
		
		
		public function get article():XML
		{
			return _article
		}
		public function set article(value:XML):void
		{
			_article = value;
		}
		
		
		
		/**
		 * Ajoute un equipement pour l'article
		 * @param value les attributs suivant sont obligatoire 
		 * 			   TYPE_EQUIPEMENT   		MOBILE|CARTE SIM|...
		 * 			   IDEQUIPEMENT_CLIENT		NUMBER
		 * 			   IDEQUIPEMENT_PARENT		NUMBER
		 * 			   LIBELLE_EQUIPEMENT		STRING
		 * 			   MODELE_EQUIPEMENT		STRING
		 * 			   PRIX						NUMBER
		 * 			   COMMENTAIRES				NUMBER	
		 * 			   [BONUS]					NUMBER
		 * 			   	
		 * @return code (1 si ok sinon -1) 
		 * */
		public function addEquipement(value : Object):Number
		{    						
			var equipement : XML = <equipement></equipement>;
			var prixToString:String = ConsoviewFormatter.formatNumber(value.PRIX_CATALOGUE,2);
			
	       
	       	equipement.appendChild(<idarticle_equipement></idarticle_equipement>);
			equipement.appendChild(<idequipementfournisparent></idequipementfournisparent>);
			equipement.appendChild(<idequipementfournis>{value.IDEQUIPEMENT_FOURNISSEUR}</idequipementfournis>);
			equipement.appendChild(<code_ihm></code_ihm>);
			equipement.appendChild(<codepin></codepin>);
			equipement.appendChild(<codepuk></codepuk>);
			
			if(value.IDEQUIPEMENT_CLIENT)
			{
				equipement.appendChild(<idequipementclient>{value.IDEQUIPEMENT_CLIENT}</idequipementclient>);	
			}
			else
			{
				equipement.appendChild(<idequipementclient/>);
			}
			equipement.appendChild(<numeroserie></numeroserie>);			
			equipement.appendChild(<libelle>{value.LIBELLE_EQ}</libelle>);
			equipement.appendChild(<type_equipement>{value.TYPE_EQUIPEMENT}</type_equipement>);
			equipement.appendChild(<modele></modele>);
			equipement.appendChild(<prix>{(value.PRIX_CATALOGUE!=null)?prixToString:0}</prix>);
			equipement.appendChild(<prixAffiche>{(value.PRIX_CATALOGUE!=null)?value.PRIX_CATALOGUE:0}</prixAffiche>);
			equipement.appendChild(<bonus>{(value.BONUS>0)?value.BONUS:0}</bonus>);	
			equipement.appendChild(<commentaire>{value.COMMENTAIRES}</commentaire>);
			equipement.appendChild(<contrats></contrats>);
			
			var contrat:XML = <contrat></contrat>;						
			contrat.appendChild(<idcontrat></idcontrat>);
			contrat.appendChild(<referencecontrat></referencecontrat>);
			contrat.appendChild(<dureecontrat></dureecontrat>);
			
			 
			
			XML(equipement.contrats).appendChild(contrat);
			equipements.appendChild(equipement);
			
			calculTotal();
			trace(article.toXMLString());
			return 1;
		
		}
		
		public function removeEquipement(value : Object):void
		{
			var node:XML = XML(value);
		        if( node == null ) return;
		        if( node.localName() != "equipement" ) return;
		    
		        var children:XMLList = XMLList(node.parent()).children();
		        for(var i:Number=0; i < children.length(); i++) {
		            if( children[i] === node) {
		                delete children[i];
		            }
		        	
		        }
				calculTotal();
			
		}
		
		public function addRessource(value : Object):void{
			var ressource : XML = <ressource>
										<idarticle_ressource></idarticle_ressource>
										<idressource></idressource>
										<bool_rapproche></bool_rapproche>
								  </ressource>;
			
			ressource.appendChild(<idproduit_catalogue>{value.IDPRODUIT_CATALOGUE}</idproduit_catalogue>);
			ressource.appendChild(<prix>0</prix>);
			ressource.appendChild(<prixAffiche>0</prixAffiche>);
			ressource.appendChild(<bonus>{(value.BONUS>0)?value.BONUS:0}</bonus>);
			ressource.appendChild(<libelle>{value.LIBELLE_PRODUIT}</libelle>);
			ressource.appendChild(<theme>{value.THEME_LIBELLE}</theme>);
			ressource.appendChild(<idThemeProduit>{value.IDTHEME_PRODUIT}</idThemeProduit>);
			ressource.appendChild(<bool_acces>{value.BOOL_ACCES}</bool_acces>);
			ressource.appendChild(<commentaire></commentaire>);
			ressources.appendChild(ressource);
			
			calculTotal();
			
		}
		
		public function removeRessource(value : Object):void{
			
				var node:XML = XML(value);
		        if( node == null ) return;
		        if( node.localName() != "ressource" ) return;
		    
		        var children:XMLList = XMLList(node.parent()).children();
		        for(var i:Number=0; i < children.length(); i++) {
		            if( children[i] === node) {
		                delete children[i];
		            }
		        	
		        }
				calculTotal();
		}
	
		public function set employe(value : Object):void
		{	
			article.idemploye[0] = value.IDEMPLOYE;
			article.nomemploye[0] = value.NOMPRENOM;
		}
		
		public function get employe():Object
		{
			return {NOMPRENOM:article.nomemploye[0],
					IDEMPLOYE:article.idemploye[0]};
		}
		
		public function set codeInterne(value:String):void
		{	
			article.code_interne[0] = value;
		}
		
		public function get codeInterne():String
		{
			return String(article.code_interne[0])
		}
		
		
		public function set codeRio(value:String):void
		{	
			article.code_rio[0] = value;
		}
		
		public function get codeRio():String
		{
			return String(article.code_rio[0])
		}
		
		protected var _dureeEngagementRessources:Number=0;
		public function set dureeEngagementRessources(value:Number):void
		{
			if(value > 0)
			{
				_dureeEngagementRessources = value;			
				article.engagement_res[0] = _dureeEngagementRessources;
			}
			else
			{
				delete article.engagement_res[0];
				article.appendChild(<engagement_res/>)	
			}
		}
		public function get dureeEngagementRessources():Number
		{
			return Number(article.engagement_res)
		}
		
		protected var _dureeEngagementEquipement:Number=0;
		public function set dureeEngagementEquipement(value:Number):void
		{
			if (value > 0)
			{
				_dureeEngagementEquipement = value;
				article.engagement_eq[0] = _dureeEngagementEquipement;	
			}
			else
			{
				delete article.engagement_eq[0];
				article.appendChild(<engagement_eq/>)	
			}
			
		}
		public function get dureeEngagementEquipement():Number
		{
			return Number(article.engagement_eq[0])
		}
		
		public function set sousTete(value:String):void
		{	
			_sousTete = value;
			article.soustete[0] = _sousTete;
		}		
		public function get sousTete():String
		{
			return String(article.soustete[0])
		} 
		
		public function removeEmploye(value : Object):void
		{	
			 employe = null;
			 employe = {};
			 
			 article.nomempoye[0]="";
			 article.idempoye[0]="";
		}
		
		public function calculTotal():void{			
			total = 0;
			for (var pname:String in article.equipements.equipement)
			{
				var prixEq:Number = Number(article.equipements.equipement.prix[pname]);
				var plusEq:Number = (isNaN(prixEq))?0:prixEq;
				
				if (!article.equipements.equipement.hasOwnProperty("prixAffiche"))
				{
					article.equipements.equipement[pname].appendChild(<prixAffiche></prixAffiche>);
					article.equipements.equipement[pname].prixAffiche[0] = plusEq;
				}
				total = total + plusEq;
			}
			
			/* for (var pname2:String in article.ressources.ressource)
			{
				var prixRes:Number = Number(article.ressources.ressource.prix[pname2]);	
				var plusRes:Number = (isNaN(prixRes))?0:prixRes;
				
				if (!article.ressources.ressource.hasOwnProperty("prixAffiche"))
				{
					article.ressources.ressource[pname2].appendChild(<prixAffiche></prixAffiche>);
					article.ressources.ressource[pname2].prixAffiche[0] = plusRes;
				}
				
				
				 			
			    total = total + plusRes;enregistrerCommande
			    trace(pname2 + " " + total + "      " + article.ressources.ressource.libelle[pname2]);
			} */			
			
		}
		
		
		protected function updateIdEquipementsParent(idequipement:Number):void
		{
			var value:String="";
			
			if (idequipement > 0)
			{
				value = idequipement.toString();
			}
			
			for (var pname:String in article.equipements.equipement)
			{	
				article.equipements.equipement[pname].idequipementfournisparent[0] = value;
			}
			
		}
	}
}