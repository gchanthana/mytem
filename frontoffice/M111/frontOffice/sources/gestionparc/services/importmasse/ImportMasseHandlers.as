package gestionparc.services.importmasse
{
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import gestionparc.event.gestionparcEvent;
	
	[Bindable]
	public class ImportMasseHandlers extends EventDispatcher
	{
		//--------------- CONSTANTES ----------------//
		
		private const TYPE_ACCESSOIRE:int = 2191;
		private const TYPE_CARTE_SIM:int = 71;
		
		//--------------- VARIABLES ----------------//
		
		public var nouveauModele : String;
		public var segment : int = 1; // 1 = mobile / 2 = fixe 
		public var comboModele : ComboBox;
		public var comboEngagement : ComboBox;
		public var formItemEngagement : FormItem;
		public var checkLignes:CheckBox;
		public var checkFPC:CheckBox;
		
		public var dateFPC:Date;
		
		private var myDatas : ImportMasseDatas;
		
		
		//--------------- METHODES ----------------//
		
		/* */		
		public function ImportMasseHandlers(instanceOfDatas:ImportMasseDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		/* */
		
		internal function initComboModeleResultHandler(evt : ResultEvent):void
		{
			myDatas.listeModele = new ArrayCollection();
			if(evt.result!=-1)
			{
				var longueur : int = (evt.result as ArrayCollection).length;
				if(segment == 1 || segment == 2 || segment == 3)
				{
					for(var i:int = 0; i< longueur ; i++)
					{
						if ((evt.result as ArrayCollection)[i].IDTYPE_EQUIPEMENT != TYPE_CARTE_SIM && (evt.result as ArrayCollection)[i].IDTYPE_EQUIPEMENT != TYPE_ACCESSOIRE )
						{
							myDatas.listeModele.addItem((evt.result as ArrayCollection)[i]);
						}
						
						if(nouveauModele != "" && (evt.result as ArrayCollection)[i].LIBELLE_EQ==nouveauModele)
						{
							comboModele.selectedIndex=i;
							break;
						}
					}
					nouveauModele="";
				}
				dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTED_MODELE_LOADED));
			}
		}		 
		
		/* */		
		internal function initComboOperateurResultHandler(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				myDatas.listeOperateur = evt.result as ArrayCollection;
				dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTED_OPERATEUR_LOADED));
			}
		}		 
		
		/* */		
		internal function initComboRevendeurResultHandler(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				myDatas.listeRevendeur = evt.result as ArrayCollection;
				dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTED_REVENDEUR_LOADED));
			}  
		}
		
		/* */		
		internal function getOperatorFPCUResultHandler(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				if(evt.result.FPC_UNIQUE != null)
				{
					dateFPC = evt.result.FPC_UNIQUE;
					checkFPC.label=ResourceManager.getInstance().getString('M111', 'FPCU_au_') + DateFunction.formatDateAsString(evt.result.FPC_UNIQUE);
					checkFPC.selected=true;
					checkFPC.visible=true;
					checkFPCUClickHandler(null);
				}
				else
				{
					checkFPC.selected=false;
					checkFPC.visible=false;
					if(checkLignes.selected)
					{
						checkFPCUClickHandler(null);
					}
				}
			}
		}
		
		/* */
		public function checkFPCUClickHandler(me:MouseEvent):void
		{
			if (checkFPC.selected)
			{
				checkFPC.setStyle("color", 0x000000);
				comboEngagement.enabled=false;
			}
			else
			{
				checkFPC.setStyle("color", 0xCCCCCC);
				comboEngagement.enabled=true;
			}
		}
		
		/* */
		internal function ValidatesaveMultipleEquipHandlerResultEvent(evt:ResultEvent):void
		{
			if (evt.result)
			{
				if (evt.result.NBIMEI > 0 || evt.result.NBSIM > 0 || evt.result.NBLIGNE > 0 || evt.result.NBCOLLAB > 0)
				{
					var obj : Object = new Object();
					obj.NBIMEI = evt.result.NBIMEI;
					obj.NBSIM = evt.result.NBSIM;
					obj.NBLIGNE = evt.result.NBLIGNE;
					obj.NBCOLLAB = evt.result.NBCOLLAB;
					
					dispatchEvent(new gestionparcEvent(gestionparcEvent.OPEN_POPUP_CONFIRMATION,obj));
				}
				else
				{
					dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_CONFIRMATION_IMPORT));
				}
			}
		}
		
		/* */
		internal function saveMultipleEquipHandlerResultEvent(evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Equipement_import_'),Application.application as DisplayObject);
				dispatchEvent(new gestionparcEvent(gestionparcEvent.EQPT_IMPORTE));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue_lors_de_l_import'), ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
		}
		
		/* */
		public function checkCollabHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatCheckCollab(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError("erreur","consoview");
			}
		}
		
		/* */
		public function checkIMEIHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatCheckIMEI(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError("erreur","consoview");
			}
		}
		
		/* */
		public function checkNSERIEHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatCheckNSERIE(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError("erreur","consoview");
			}
		}
		
		/* */
		public function checkNEXTENSIONHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatCheckNEXTENSION(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError("erreur","consoview");
			}
		}
		
		/* */
		public function checkNSIMHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatCheckNSIM(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError("erreur","consoview");
			}
		}
		
		/* */
		public function checkNLIGNEHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatCheckNLIGNE(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError("erreur","consoview");
			}
		}
		
		/* */
		public function importerFichierHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatImporterFichier(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError("erreur","consoview");
			}
		}
		
		/* */
		//		internal function readCSVandCSVToArrayResultHandler(evt:ResultEvent):void
		//		{
		//			if(evt.result is Array)
		//			{
		// 				/*if(myImportMasse_V2.myUploadDownloadImportMasse.verifieHeaderCSV((evt.result as Array)))
		//				{
		//					myImportMasse_V2.myUploadDownloadImportMasse.remplirDGCSV((evt.result as Array));
		//				} */
		//				dispatchEvent(new gestionparcEvent(gestionparcEvent.READ_CSV_AND_CSV_TO_ARRAY));
		//			}
		//		}
		
		//		internal function readXLSandXLSToArrayResultHandler(evt:ResultEvent):void
		//		{
		//			if(evt.result is Object)
		//			{
		// 				/*if(myImportMasse_V2.myUploadDownloadImportMasse.verifieHeaderXLS((evt.result.SHEETDATA as Object)))
		//				{
		//					myImportMasse_V2.myUploadDownloadImportMasse.remplirDGXLS((evt.result.ARRCELL as Array));
		//				}*/
		//				dispatchEvent(new gestionparcEvent(gestionparcEvent.READ_XLS_AND_XLS_TO_ARRAY));
		//			}
		//		}
		
		//		internal function renameFileResultHandler(evt:ResultEvent):void
		//		{
		// 			var longueur : int = myImportMasse_V2.myUploadDownloadImportMasse.fileRef.name.length; 
		//			if(myImportMasse_V2.myUploadDownloadImportMasse.fileRef.name.substr(longueur-3,longueur) == "csv")
		//			{
		//				readCSVandCSVToArray();
		//			}
		//			else
		//			{
		//				readXLSandXLSToArray();
		//			}
		//		}
		
	}
}