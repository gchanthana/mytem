package service.destinataire.listedestinataires
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;


	public class ListeDestinatairesService
	{
		private var _model:ListeDestinatairesModel;
		public var handler:ListeDestinatairesHandler;	
		
		public function ListeDestinatairesService()
		{
			this._model  = new ListeDestinatairesModel();
			this.handler = new ListeDestinatairesHandler(model);
		}		
		
		public function get model():ListeDestinatairesModel
		{
			return this._model;
		}

		public function getListeDestinataires(idNode:Number):void
		{		
			var callService:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.ListeDestinataires","getListeDest",handler.getListeDestinatairesResultHandler); 
			
			RemoteObjectUtil.callService(callService,idNode);// appel au service
		}
	}
}