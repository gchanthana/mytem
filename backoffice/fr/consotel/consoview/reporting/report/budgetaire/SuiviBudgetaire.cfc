<cfcomponent name="SuiviBudgetaire" alias="fr.consotel.consoview.reporting.report.budgetaire.SuiviBudgetaire">
	<cffunction name="runReport" access="remote" output="true" returntype="numeric">
		<cfargument name="parameters" required="true" type="struct">
		<cfset biPub=createObject("component","fr.consotel.consoview.api.bi.BIPublisher")>
		<cfset biPub.validateLogin("consoview","public")>
		<cfset arrayOfParamNameValue=arrayNew(1)>
		 
		<cfset value=structNew()>
		<cfset value.name="P_IDCLICHE">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDCLICHE"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[1]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDNOEUD">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDGROUPE_CLIENT"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[2]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDEXERCICE">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDEXERCICE"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[3]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDRACINE_MASTER">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDRACINE_MASTER"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[4]=value>
		<cfset job_id=biPub.scheduleReport("/~consoview/budgetaire/Suivi Budgetaire/Suivi Budgetaire.xdo","v1",FORMAT,"OBIEE/SCHEDULED/",arrayOfParamNameValue,"consoview","public","FTP")>
		<cfreturn job_id>
	</cffunction>
	
	<cffunction name="getListExercice" access="remote" output="true" returntype="query">
		<cfargument name="idnoeud" required="true" type="numeric">
		<cfquery name="qGetListExercice" datasource="#SESSION.OFFREDSN#">
			SELECT be.idbudget_exercice, be.libelle_exercice
			FROM budget_exercice be,
			  (
			   SELECT idgroupe_client
			   FROM groupe_client g
			   WHERE id_groupe_maitre IS NULL
			   START WITH idgroupe_client=#arguments.idnoeud#
			   CONNECT BY PRIOR id_groupe_maitre=idgroupe_client
			  ) ba
			WHERE be.idgroupe_client=ba.idgroupe_client
			order by be.libelle_exercice
		</cfquery>
		<cfreturn qGetListExercice>
	</cffunction>
</cfcomponent>
