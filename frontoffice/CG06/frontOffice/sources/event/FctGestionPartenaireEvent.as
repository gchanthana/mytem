package event
{
	import flash.events.Event;
	
	public class FctGestionPartenaireEvent extends Event
	{
		// les types Event liés au service remote
		public static const LISTE_PARTENAIRE				:String = "LISTE_PARTENAIRE";
		public static const LISTE_SITES						:String = "LISTE_SITES";
		public static const LISTE_CONTRATS					:String = "LISTE_CONTRATS";
		public static const LISTE_INTERLOCUTEURS			:String = "LISTE_INTERLOCUTEURS";
		public static const LISTE_FORMATIONS				:String = "LISTE_FORMATIONS";
		public static const LISTE_LANGUES					:String = "LISTE_LANGUES";
		
		public static const NIVEAUX_CERTIFICATION			:String = "NIVEAUX_CERTIFICATION";
		public static const TYPES_PLAN_TARIFAIRE			:String = "TYPES_PLAN_TARIFAIRE";
		
		public static const LISTE_SUJETS_FORMATION			:String = "LISTE_MODULES_FORMATION";
		public static const LISTE_LIEUX_FORMATION			:String = "LISTE_LIEUX_FORMATION";
		public static const LISTE_PARTICIPANTS_FORMATION	:String = "LISTE_PARTICIPANTS_FORMATION";
		public static const LISTE_CIVILITES					:String = "LISTE_CIVILITES";
		
		
		public static const EDITION_SITE					:String = "EDITION_SITE";
		public static const EDITION_INTERLOC				:String = "EDITION_INTERLOC";
		public static const EDITION_FORMATION				:String = "EDITION_FORMATION";
		
		public static const MODIF_CONTRAT_PARTN				:String = "MODIF_CONTRAT";
		public static const DESACTIVE_CONTRAT_PARTN			:String = "DESACTIVE_CONTRAT";
		public static const EDITION_PARTICIPANTS			:String = "MODIF_PARTICIPANTS";
		public static const AJOUT_CONTRAT_PARTN				:String = "AJOUT_CONTRAT";
		public static const AJOUT_PARTENAIRE				:String = "AJOUT_PARTENAIRE";
		
		public static const MAJ_CONTRAT						:String = "MAJ_CONTRAT"; //mise à jour du contrat
		
		// les types Event liés aux clicks et actions utilisateur
		public static const CLICK_DETAIL_PARTENAIRE			:String = "CLICK_DETAIL_PARTENAIRE";
		public static const CLICK_EDITION_CONTRAT			:String = "CLICK_EDITION_CONTRAT";
		public static const CLICK_EDITION_INTERLOC			:String = "CLICK_EDITION_INTERLOC";
		public static const CLICK_EDITION_FORMATION			:String = "CLICK_EDITION_FORMATION";
		public static const CLICK_EDITION_SITE				:String = "CLICK_EDITION_SITE";
		
		public static const CLICK_SUPPRIMER_PARTENAIRE		:String = "CLICK_SUPPRIMER_PARTENAIRE";
		public static const CLICK_SUPPRIMER_SITE			:String = "CLICK_SUPPRIMER_SITE";
		public static const CLICK_SUPPRIMER_FORMATION		:String = "CLICK_SUPPRIMER_FORMATION";
		public static const CLICK_PARTICIPANTS_FORMATION	:String = "CLICK_PARTICIPANTS_FORMATION";
		
		public static const CLICK_NOUVEAU_PARTN				:String = "CLICK_NOUVEAU_PARTN";
		public static const CLICK_EXPORT_LIST_PARTN			:String = "CLICK_EXPORT_LIST_PARTN";
		
		public static const CLICK_PARTENAIRE_PROSPECT		:String = "CLICK_PARTENAIRE_PROSPECT";
		public static const CLICK_PARTENAIRE_NON_PROSPECT	:String = "CLICK_PARTENAIRE_NON_PROSPECT";
		
		
		private var _idPtn : int;
		private var _nomPtn:String;
		
		public function FctGestionPartenaireEvent(type:String, idPartenaire:int=-1, nomPartenaire:String="", bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this._idPtn = idPartenaire;
			this._nomPtn = nomPartenaire;
		}

		public function get idPtn():int
		{
			return _idPtn;
		}
		
		public function get nomPtn():String 
		{ 
			return _nomPtn; 
		}

	}
}