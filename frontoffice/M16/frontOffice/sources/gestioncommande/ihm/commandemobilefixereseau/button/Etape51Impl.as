package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpLibelleModeleIHM;
	
	import session.SessionUserObject;
	
	
	[Bindable]
	public class Etape51Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListEquipements	:DataGrid;
		public var dgListAboOptions		:DataGrid;
		
		public var txtptNumberConfig	:TextInput;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUplibelle		:PopUpLibelleModeleIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		private var  _cmd				:Commande;
		private var _myElements			:ElementsCommande2	= new ElementsCommande2();

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listEquipements		:ArrayCollection = new ArrayCollection();
		public var listAboOptions		:ArrayCollection = new ArrayCollection();
				
		public var prixEquipements		:Number = 0;
		public var prixRessources		:Number = 0;
		
		private var _addAllPool			:Boolean = false;
		private var _convertedToInt		:Boolean = true;
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES BUTTON CUSSTOM
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 4;
		
		public var ACCESS						:Boolean = false;
		
		private var _razl						:Boolean = false;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function Etape51Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}

		protected function btnRecordModeleClickHandler(me:MouseEvent):void
		{
			_popUplibelle 			 = new PopUpLibelleModeleIHM();
			_popUplibelle.myCommande = _cmd;
			
			PopUpManager.addPopUp(_popUplibelle, this);
			PopUpManager.centerPopUp(_popUplibelle);
		}
		
		//---> VERIFIE QUE LE TEXTE SAISIE EST BIEN UN ENTIER ET VERIFIE QU'IL EST >= 1 
		protected function txtptNumberConfigHandler(e:Event):void
		{
			_convertedToInt = true;
			
			try
			{
				if(Number(txtptNumberConfig.text) < 1)
				{
					txtptNumberConfig.text = "1";
					_convertedToInt = false;
				}
			}
			catch(erreur:Error)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_nombre_'), 'Consoview', null);
				_convertedToInt = false;
			}
		}

	//--------------------------------------------------------------------------------------------//
	//					LISTENER DE L'IHM
	//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			dgListEquipements.addEventListener('TERMINAL_PRICE_CHANGED' , terminalPriceChangedHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function viewStackChange():void
		{
			_myElements 	= SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			_cmd 			= SessionUserObject.singletonSession.COMMANDE;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			attributToEquipments();
			attributToAboOptions();
			compteTotal();
			
			callLater(refreshDatagrid);
			
			txtptNumberConfig.text = _cmd.CONFIG_NUMBER.toString();
		}

		public function reset():void
		{
			if(_cmd != null)
			_cmd.CONFIG_NUMBER = 1;
			
			listEquipements.removeAll();
			listAboOptions.removeAll();
		}
		
		public function setConfiguration():void
		{
			_razl = false;
			
			attributToEquipments();
			attributToAboOptions();
			compteTotal();
			
			callLater(refreshDatagrid);
			
			_cmd.CONFIG_NUMBER = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
		}

	//--------------------------------------------------------------------------------------------//
	//					VERIFICATION ET CONTRUCTION DES DONNÉES CHOISIES
	//--------------------------------------------------------------------------------------------//
		
		public function checkData():Boolean
		{
			var bool:Boolean = false;
			
			if(!isNaN(Number(txtptNumberConfig.text)))
			{
				_cmd.CONFIG_NUMBER = Number(txtptNumberConfig.text);
				bool = true;
			}
			
			return bool;
		}
		
		public function buildParameters():void
		{
			if(!checkData())
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_nombre_de_configurart'), 'Consoview', null);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function terminalPriceChangedHandler(e:Event):void
		{
			compteTotal();
			
			dispatchEvent(new Event('PRICE_CHANGE', true));
		}
		
		private function attributToEquipments():void
		{			
			var lenTer	:int = _myElements.TERMINAUX.length;
			var lenAcc	:int = _myElements.ACCESSOIRES.length;
			var i		:int = 0;
			
			listEquipements = new ArrayCollection();
			
			for(i = 0;i < lenTer;i++)
			{
				if(_myElements.TERMINAUX[i].IDEQUIPEMENT > 0)
					listEquipements.addItem(_myElements.TERMINAUX[i]);
			}
			
			for(i = 0;i < lenAcc;i++)
			{
				listEquipements.addItem(_myElements.ACCESSOIRES[i]);
			}
			
			(dgListEquipements.dataProvider as ArrayCollection).refresh();
		}
		
		private function attributToAboOptions():void
		{	
			var lenAbo	:int = _myElements.ABONNEMENTS.length;
			var lenOpt	:int = _myElements.OPTIONS.length;
			var i		:int = 0;
			
			listAboOptions = new ArrayCollection();
			
			for(i= 0;i < lenAbo;i++)
			{
				listAboOptions.addItem(_myElements.ABONNEMENTS[i]);
			}
			
			for(i = 0;i < lenOpt;i++)
			{
				listAboOptions.addItem(_myElements.OPTIONS[i]);
			}
			
			(dgListAboOptions.dataProvider as ArrayCollection).refresh();
		}
		
		private function refreshDatagrid():void
		{
			dgListEquipements.dataProvider 	= listEquipements;
			dgListAboOptions.dataProvider 	= listAboOptions;
		}
		
		//---> COMPTE LE TOTAL DES CONFIGURATIONS QUE L'ON VIENT DE FAIRE
		private function compteTotal():void
		{	
			prixEquipements = 0;
			prixRessources  = 0;
			
			for(var i:int = 0;i < listEquipements.length;i++)
			{
				if(listEquipements[i].PRIX.toString() != "NaN" && listEquipements[i].PRIX != null)
					prixEquipements = prixEquipements + Number(listEquipements[i].PRIX);
			}
			
			for(var j:int = 0;j < listAboOptions.length;j++)
			{
				if(listAboOptions[j].PRIX_UNIT.toString() != "NaN" && listAboOptions[j].PRIX_UNIT != null)
					prixRessources = prixRessources + Number(listAboOptions[j].PRIX_UNIT);
			}
		}


	}
}