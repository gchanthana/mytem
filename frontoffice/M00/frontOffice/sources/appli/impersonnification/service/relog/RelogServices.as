package appli.impersonnification.service.relog
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import general.service.LoginServices;
	
	import mx.rpc.AbstractOperation;

	public class RelogServices
	{
		public var myHandlers:RelogHandlers;
		public var myDatas:RelogDatas = new RelogDatas();
		private var loginServices:LoginServices = new LoginServices();
		
		public function RelogServices()
		{
			myHandlers = new RelogHandlers(myDatas);
		}
		
		//Afficher les logins		
		public function setImpersonnification():void//Set l'impersonnification dans la var SESSION sur true
		{
			var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M00.connectApp.AbstractConnectToApp",
				"fromAdminToClient", myHandlers.setImpersonnificationHandler);
			RemoteObjectUtil.callService(op1);
		}
		
		public function stopImpersonnification():void//Set l'impersonnification dans la var SESSION sur false
		{
			var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M00.connectApp.AbstractConnectToApp",
				"fromClientToAdmin", myHandlers.stopImpersonnificationHandler);
			RemoteObjectUtil.callService(op1);
		}
		
		public function getImpersonnification():void//retourne l'impersonnification dans le re.result du handler
		{
			var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M00.connectApp.AbstractConnectToApp",
				"getImpersonnification", myHandlers.getImpersonnificationHandler);
			RemoteObjectUtil.callService(op1);
		}
		
		public function updateCv():void//On récupère la SESSION, puis on update CvAccessManager dans le handler
		{
			var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M00.connectApp.AbstractConnectToApp",
				"getSession", myHandlers.updateCvHandler);
			RemoteObjectUtil.callService(op1);
		}
		
		public function getCodeApplication():void//On récupère le code de l'appli depuis le back
		{
			var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M00.connectApp.AbstractConnectToApp",
				"getCodeApplication", myHandlers.getCodeApplicationHandler);
			RemoteObjectUtil.callService(op1);	
		}
	}
}