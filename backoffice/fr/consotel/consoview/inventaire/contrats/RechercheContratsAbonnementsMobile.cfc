<cfcomponent output="false">
	
	<cffunction name="findContrats" access="remote" output="false" returntype="query">
		<cfargument name="idpool" type="Numeric" required="true" />
		<cfargument name="clef" type="string" required="true" />
				
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_CONTRATSABO">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idpool" value="#idpool#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_clef" value="#clef#"/>
			<cfprocresult name="listeContrats">		  
		</cfstoredproc>
		
		<cfreturn listeContrats/>
	</cffunction>
	
</cfcomponent>