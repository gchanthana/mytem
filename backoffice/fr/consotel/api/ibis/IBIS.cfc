<cfcomponent displayname="fr.consotel.api.ibis.IBIS"
extends="fr.consotel.consoview.api.IBIS"
hint="Le paramètre parameters de invokeService() doit contenir une clé EVENT_ID qui est l'ID de mise à jour Evènement
Serveur de notification HTTP : REPORTING (Non modifiable)
Fichier de Log : [CFUSION]/logs/IBIS.log
fr.consotel.consoview.api.CV, fr.consotel.consoview.api.ApiConsoView :
invokeService,registerEventListener,initInstance,cloneParameters,envoyer1RapportParMailAdapter
fr.consotel.consoview.api.IBIS :
fr.consotel.api.ibis.publisher.BipWebServiceFactory,fr.consotel.api.ibis.publisher.prototype.FtpSchedulingPrototype">
<!--- ************************************************************************************************************************* --->
	<!--- fr.consotel.api.ibis.IBIS --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.IBIS">
		<cfset var startTick=getTickCount()><!--- Tracking de la durée d'exécution (en ms) --->
		<cfset VARIABLES.TRACING=isDebugMode()><!--- TODO : Inutilisé --->
		<cfset VARIABLES.IBIS_LOG="IBIS"><!--- Valeur file pour cflog --->
		<cfif NOT isDefined("VARIABLES.INITIALIZED")><!--- Flag d'initialisation --->
			<!--- fr.consotel.consoview.api.IBIS (Champs privés hérités) --->
			<cfset SUPER.initInstance()><!--- (*) Méthode d'initialisation de la classe parent --->
			<cfset VARIABLES.BIP_USER="scheduler">
			<cfset VARIABLES.BIP_USER_KEY="public">
			<cfset VARIABLES.WS_FACTORY="fr.consotel.api.ibis.publisher.BipWebServiceFactory">
			<!--- Spécifique à Evènement --->
			<cfset VARIABLES.EVENT_ID_KEY="EVENT_ID"><!--- Clé dans le paramètre parameters de invokeService() dont la valeur est l'ID de MAJ --->
			<!--- fr.consotel.api.ibis.IBIS --->
			<cfset VARIABLES.USER_ID=0>
			<cfset VARIABLES.DOMAIN="consotel.fr">
			<cfset VARIABLES.MAIL_FROM=CGI.SERVER_NAME & "@" & VARIABLES.DOMAIN>
			<cfset VARIABLES.MAIL_TO="">
			<cfset VARIABLES.MAIL_SUBJECT=LEFT(CGI.SCRIPT_NAME,100)>
			<cfset VARIABLES.TARGET_SEP="_"><!--- Séparateur TARGET,NOTIFICATION --->
			<cfset VARIABLES.NOTIFICATION="REPORTING"><!--- Paramètre httpTarget de CV : /fr/consotel/api/ibis/notification/notification.cfm --->
			<cfset VARIABLES.WS_FACTORY_OBJ=createObject("component",VARIABLES.WS_FACTORY)><!--- Instance de WS_FACTORY --->
			<!--- (*) Endpoint du WebService BIP : VARIABLES.BIP_ENDPOINT (Appel dynamique en Java) --->
			<cftry>
				<cfset var bipService=VARIABLES.WS_FACTORY_OBJ.createWebService()>
				<cfset var serviceClass=bipService._getService().getClass()>
				<cfset var getEndpointMethod=serviceClass.getMethod("get" & bipService.getPortName() & "Address",[])>
				<cfset VARIABLES.BIP_ENDPOINT=getEndpointMethod.invoke(bipService._getService(),[])><!--- URL du WSDL (BIP) --->
				<cflog text="IBIS Creation Complete (#(getTickCount() - startTick)# ms) : #VARIABLES.BIP_ENDPOINT#" file="#getLog()#" type="information">
				<cfcatch type="any">
					<cflog text="IBIS.createInstance() : #CFCATCH.message#" file="#getLog()#" type="error">
					<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="WEBSERVICE : #CFCATCH.message#">
				</cfcatch>
			</cftry>
			<cfset VARIABLES.INITIALIZED=TRUE>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getLog" returntype="string" hint="Valeur de l'attribut file pour cflog">
		<cfreturn VARIABLES.IBIS_LOG>
	</cffunction>

	<!--- fr.consotel.consoview.api.CV, fr.consotel.consoview.api.ApiConsoView --->
	<cffunction access="public" name="invokeService" returntype="any" hint="Retourne le JOB_ID (Résultat de scheduleReport)">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID (e.g IBIS)">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID (e.g scheduleReport)">
		<cfargument name="parameters" type="struct" required="true" hint="Structure of parameter (KEYS,VALUES)">
		<cfargument name="httpTarget" type="string" required="false" hint="Paramètre ignoré pour cette implémentation">
		<cfreturn SUPER.invokeService(ARGUMENTS.moduleId,ARGUMENTS.serviceId,ARGUMENTS.parameters,VARIABLES.NOTIFICATION)>
	</cffunction>
	
	<cffunction access="private" name="registerEventListener" returntype="string" hint="Adaptation de la classe parent : Retourne le JOBNAME (EVENT_TARGET)">
		<cfargument name="parameters" type="struct" required="true" hint="Structure of parameter (KEYS,VALUES)">
		<cflog text="IBIS.invokeService() - TARGET : #ARGUMENTS.parameters.EVENT_TARGET#" file="#getLog()#" type="information">
		<cfreturn ARGUMENTS.parameters.EVENT_TARGET><!--- JOBNAME (EVENT_TARGET) --->
	</cffunction>

	<!--- fr.consotel.consoview.api.IBIS --->
	<cffunction access="private" name="scheduleReport" returntype="any" hint="Schedule a BIP Report and return JOB_ID">
		<cfargument name="scheduleRequest" type="struct" required="true" hint="ScheduleRequest">
		<!--- Identique à l'implémentation de la classe parent (fr.consotel.consoview.api.IBIS) --->
		<cftry>
			<cfset var startTick=getTickCount()>
			<cfset var bipService=VARIABLES.WS_FACTORY_OBJ.createWebService()>
			<!--- Planification du rapport --->
			<cfset var jobId=bipService.scheduleReport(ARGUMENTS.scheduleRequest,VARIABLES.BIP_USER,VARIABLES.BIP_USER_KEY)>
			<cflog text="IBIS  - HTTP Notification Server : #scheduleRequest.httpNotificationServer#" file="#getLog()#" type="information">
			<cflog text="IBIS  - BIP Endpoint (#(getTickCount() - startTick)# ms) : #VARIABLES.BIP_ENDPOINT#" file="#getLog()#" type="information">
			<cfreturn jobId>
			
			<cfcatch type="any">
				<cflog type="error" text="IBIS.scheduleReport() : #CFCATCH.MESSAGE#">
				<cfsavecontent variable="body">
						[*Error*] IBIS.scheduleReport()</br> 
						<cfdump var="#CFCATCH#">
				</cfsavecontent>	
				<cftry>
						
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(13)>
					<cfset mLog.setIDMNT_ID(24)>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfoutput>server #CGI.SERVER_NAME#</br></cfoutput>
								<cfdump var="#cfcatch#">
					</cfmail>
				</cfcatch>
				
				</cftry>
				
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#CFCATCH.message#">
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>