package univers.inventaire.inventaire.export
{
	[RemoteClass(alias="fr.consotel.consoview.inventaire.cycledevie.Reclamation")]
	[Bindable]
	public class ReclamationVO
	{
		public var IDINV_OPERATIONS : Number = 0;
		public var IDINV_TYPEOP : Number = 0;
		public var LIBELLE_OPERATIONS : String = "";
		public var ID_SOCIETE : Number = 0;
		public var ID_CONTACT : Number = 0;
		public var DATE_REF : String = "";
		public function ReclamationVO(){
			
		}
	}
}