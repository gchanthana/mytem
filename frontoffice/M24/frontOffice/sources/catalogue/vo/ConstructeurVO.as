package catalogue.vo
{
	public class ConstructeurVO
	{
		private var _ID_CONSTRUCTEUR:Number;
		private var _LIBELLE_CONSTRUCTEUR:String;
		public var SELECTED			:Boolean = false;
		
		public function ConstructeurVO()
		{
		}


		public function set ID_CONSTRUCTEUR(value:Number):void
		{
			_ID_CONSTRUCTEUR = value;
		}

		public function get ID_CONSTRUCTEUR():Number
		{
			return _ID_CONSTRUCTEUR;
		}
		public function set LIBELLE_CONSTRUCTEUR(value:String):void
		{
			_LIBELLE_CONSTRUCTEUR = value;
		}

		public function get LIBELLE_CONSTRUCTEUR():String
		{
			return _LIBELLE_CONSTRUCTEUR;
		}
		
		
	}
}