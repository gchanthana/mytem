package interfaceClass
{
	import flash.events.Event;
	
	import appli.control.AppControlEvent;
	
	import entity.AppParams;
	
	public interface IApplication
	{
		function executeLogin():void
		function displayIHM(evt:Event = null):void
		function init(param:AppParams):void
		function validateLogin(objLogin:Object):void
		function changeModule(evt:AppControlEvent):void
		function deconnect(evt:AppControlEvent=null):void
		function askNewPerimetre(evt:AppControlEvent = null,idperimetre:int = -1):void
		function processAppStyle():void
	}
}