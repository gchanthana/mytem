package gestionparc.services.liste
{
	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;	public class ListeHandlers	{		private var myDatas : ListeDatas;				public function ListeHandlers(instanceOfDatas:ListeDatas)		{			myDatas = instanceOfDatas;		}		public function listeCollaborateurHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeCollaborateur(re.result as ArrayCollection);			else				showError()		}		public function listeLigneHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeLigne(re.result as ArrayCollection);			else				showError()		}		public function ListeSimSsTermHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeSimSsTerm(re.result as ArrayCollection);			else				showError()		}
		
		public function ListeExtAvecLigneHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result is ArrayCollection)
				myDatas.treatListeExtLigne(re.result as ArrayCollection);
			else
				showError()
		}
		
				public function ListeSimSsLigneHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeSimSsLigne(re.result as ArrayCollection);			else				showError()		}
		
		public function ListeExtSansLigneHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result is ArrayCollection)
				myDatas.treatListeExtSansLigne(re.result as ArrayCollection);
			else
				showError()
		}
				public function ListeSimFixeSsTermHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeSimFixeSsTerm(re.result as ArrayCollection);			else				showError()		}		public function ListeSimFixeSsLigneHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeSimFixeSsLigne(re.result as ArrayCollection);			else				showError()		}		public function listeTermDispoHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteTermDispo(re.result as ArrayCollection);			else				showError()		}		public function listeTermAvecSimDispoHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteTermAvecSimDispo(re.result as ArrayCollection);			else				showError()		}		public function listeTermFixeDispoHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteTermFixeSansEqpFixe(re.result as ArrayCollection);			else				showError()		}		public function listeTermFixeAvecEqpFixeDispoHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteTermFixeAvecEqpFixe(re.result as ArrayCollection);			else				showError()		}		public function listeEqpOfCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteEqpOfCollab(re.result as ArrayCollection);			else				showError()		}		public function listeTermMobileOfCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteTermMobileOfCollab(re.result as ArrayCollection);			else				showError()		}		public function ListeSsEqpHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeSsEqp(re.result as ArrayCollection);			else				showError()		}		public function ListeSsEqpOfEqpHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatListeSsEqpOfEqp(re.result as ArrayCollection);			else				showError()		}		public function listeTermFixeOfCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteTermFixeOfCollab(re.result as ArrayCollection);			else				showError()		}		public function listeSimOfCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteSimOfCollab(re.result as ArrayCollection);			else				showError()		}		public function listeSimWithLigneOfCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteSimWithLigneOfCollab(re.result as ArrayCollection);			else				showError()		}		public function listeEqFixeWithLigneOfCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteEqFixeWithLigneOfCollab(re.result as ArrayCollection);			else				showError()		}		public function listeEqFixeWithoutLigneOfCollabCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteEqFixeWithoutLigneOfCollab(re.result as ArrayCollection);			else				showError()		}		public function listeLigneOfCollabHandler(re:ResultEvent):void		{			if(re.result != null && re.result is ArrayCollection)				myDatas.treatlisteLigneOfCollab(re.result as ArrayCollection);			else				showError()		}
		
		internal function listeEqptFixeDispoHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result is ArrayCollection)
				myDatas.updateListeEqptFixeDispo(re.result as ArrayCollection);
			else
				showError()
			
		}
		
		
		
		internal function listeLigneFixeDispoHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result is ArrayCollection)
				myDatas.updateListeLigneFixeDispo(re.result as ArrayCollection);
			else
				showError()
		}						private function showError():void		{			ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Une_erreur_est_survenue__Veuillez_r_it_r'),ResourceManager.getInstance().getString('M111', 'Erreur'));		}	}}