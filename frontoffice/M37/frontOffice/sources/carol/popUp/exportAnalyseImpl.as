package carol.popUp
{
	import mx.resources.ResourceManager;
	import carol.event.CarolEvent;
	
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.RadioButton;
	import mx.managers.PopUpManager;
	
	public class exportAnalyseImpl extends Box
	{

//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - COMPOSANTS FLEX
		//--------------------------------------------------------------------------------------------//		
		
		public var rdbtnExportPDF	:RadioButton;
		public var rdbtnExportXLS	:RadioButton;
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - GLOBAL
		//--------------------------------------------------------------------------------------------//
		
		public var goExportPDF		:Boolean = false;

//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//
		
		public function exportAnalyseImpl()
		{
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED - EVENT COMPOSANTS
		//--------------------------------------------------------------------------------------------//
		//SI C'EST PAS EN PDF C'EST FORCEMENT EN XLS (CAR  POUR LE MOMENT UNIQUEMENT 2 FORMATS)
		protected function btnValidClickHandler(e:Event):void
		{
			var ce	:CarolEvent; 
			var obj	:Object = new Object();
			if(rdbtnExportPDF.selected)
			{
				if(goExportPDF)
				{
					Alert.show(ResourceManager.getInstance().getString('M37', 'Veuillez_effectuer_une_analyse_pour_expo'), ResourceManager.getInstance().getString('M37', 'Erreur'));
					return;
				}
				obj.TYPE = "PDF";
			}
			else
			{
			 	obj.TYPE = "CSV";
			}
			ce = new CarolEvent(CarolEvent.POPUP__VALIDATE,false,obj);
			dispatchEvent(ce);
			PopUpManager.removePopUp(this);
		}
		
		protected function btnAnnulClickHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

	}
}
