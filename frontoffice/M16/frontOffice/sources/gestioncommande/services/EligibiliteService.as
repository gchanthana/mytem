package gestioncommande.services
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.LignesByOperateur;
	import gestioncommande.events.CommandeEvent;
	
	public class EligibiliteService extends EventDispatcher
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		public var infosEligibilite				:Object = new Object();
		public var eligiblecomptesouscompte		:Object = new Object()
		
		
		//---> STATUS DE LA PROCEDURE 0-> PAS D'APPEL DE PROCEDURE, 1-> VERIFICATION EN COURS
		public var STATUS						:int = 0;
		public var PROC							:int = -1;
		
		//---> STATUS DE LA PROCEDURE 0-> PAS D'APPEL DE PROCEDURE, 1-> VERIFICATION EN COURS, 2-> VERIFIER
		public var STATUS_PROC					:int = 0;
		
		//--> ETAT DE LA LIGNE -1-> ON SAIS PAS, 0-> NON ELIGIBLE, 1-> ELIGIBLE
		public var ETAT							:int = -1;
		
		public var isElegibilitePresent			:Boolean = false;
		
		private var _DATE						:String = '';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function EligibiliteService()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function getDateEligibilite():String
		{
			return _DATE;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function checkEligibiliteLignesByLignes(idOperateur:int, idsoustete:int): void
		{			
			STATUS = 1;
			
			_DATE = '';
			
			infosEligibilite = new Object();
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation('fr.consotel.consoview.M16.v2.LigneService',
																			'checkEligibilite',
																			checkEligibiliteLignesByLignesResultHandler);
			
			RemoteObjectUtil.callSilentService(op, idsoustete.toString(), idOperateur.toString());
		}
		
		public function checkEligibiliteLignesByLignesAndGetCompteSousCompte(idsoustete:int, etatEligibilite:int, idoperateur:int, idoperevendeur:Number = 0): void
		{			
			PROC = 0;
			STATUS_PROC = 1;
			_DATE = '';
			eligiblecomptesouscompte = new Object();
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation('fr.consotel.consoview.M16.v2.LigneService',
																			'checkEligibiliteAndCompteSousCompte',
																			checkEligibiliteLignesByLignesAndGetCompteSousCompteResultHandler);
			
			RemoteObjectUtil.callSilentService(op,	idsoustete.toString(),
													etatEligibilite,
													idoperateur,
													idoperevendeur);
		}
		
		public var myLigneOperateur:LignesByOperateur;
		
		public var currentOperateur		:Object;
		
		public function getOperateurLastFactureLigneM111(idsoustete:int): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation('fr.consotel.consoview.M16.v2.LigneService',
																			'getOperateurLastFactureLigneM111',
																			getOperateurLastFactureLigneM111ResultHandler);
			
			RemoteObjectUtil.callSilentService(op, idsoustete);
		}
		
		
		public function checkEligibiliteLignesByLignesAndGetCompteSousCompteM111(idsoustete:int, myLigneOpe:LignesByOperateur, idoperateur:int, idoperevendeur:Number = 0): void
		{	
			_DATE = '';
			
			myLigneOperateur = myLigneOpe;
			
			eligiblecomptesouscompte = new Object();
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation('fr.consotel.consoview.M16.v2.LigneService',
																			'checkEligibiliteAndCompteSousCompte',
																			checkEligibiliteLignesByLignesAndGetCompteSousCompteM111ResultHandler);
			
			RemoteObjectUtil.callSilentService(op,	idsoustete.toString(),
													0,
													idoperateur,
													idoperevendeur);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					RETOURS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function checkEligibiliteLignesByLignesResultHandler(re:ResultEvent):void
		{
			if(re.result)//---> OK
			{
				STATUS = 0;
								
				if(re.result != null && re.result.length > 0)
				{
					isElegibilitePresent = true;
					infosEligibilite 	 = re.result[0];
				}
				else
				{
					isElegibilitePresent = true;
					
					infosEligibilite 	 						= new Object();
					infosEligibilite.DATE_ELLIGIBILITE 			= null;
					infosEligibilite.DATE_ELIGIBILITE_STRG		= DateFunction.formatDateAsString(infosEligibilite.DATE_ELLIGIBILITE as Date);
					infosEligibilite.ELLIGIBILITE 				= '1';
					infosEligibilite.RESTE 						= 0;
					infosEligibilite.FIN_PERIODE_CONTRACTUELLE 	= null;
					infosEligibilite.FPC 						= null;
					infosEligibilite.MONTANT_MENSUEL_PEN 		= 0;
					infosEligibilite.FRAIS_FIXE_RESILIATION 	= 0;
					infosEligibilite.FRAIS_TOTAL_RESIL 			= 0;
				}
				
				_DATE = DateFunction.formatDateAsString(infosEligibilite.DATE_ELLIGIBILITE as Date);
				
				eligiblecomptesouscompte.INFOSELIGIBILITE = infosEligibilite;
				
				if(!eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE || eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE == null)
					eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE = '1';
				
				if(Number(eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE) > 0)
					ETAT = 1;
				else
					ETAT = 0;
				
				dispatchEvent(new CommandeEvent(CommandeEvent.ELIGIBILITE_LIGNE));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur'), 'Consoview');
		}

		private function checkEligibiliteLignesByLignesAndGetCompteSousCompteResultHandler(re:ResultEvent):void
		{
			if(re.result)//---> OK
			{	
				eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE 	= re.result.INFOSCOMPTESOUSCOMPTE[0];
				
				if(re.result.INFOSELIGIBILITE != null && re.result.INFOSELIGIBILITE.length > 0)
				{
					isElegibilitePresent = true;
					
					eligiblecomptesouscompte.INFOSELIGIBILITE 	= re.result.INFOSELIGIBILITE[0];
				}
				else
				{
					isElegibilitePresent = true;
					
					eligiblecomptesouscompte.INFOSELIGIBILITE 							= new Object();
					eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE 		= null;
					eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELIGIBILITE_STRG		= DateFunction.formatDateAsString(infosEligibilite.DATE_ELLIGIBILITE as Date);
					eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE 				= '1';
					eligiblecomptesouscompte.INFOSELIGIBILITE.RESTE 					= 0;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FIN_PERIODE_CONTRACTUELLE = null;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FPC 						= null;
					eligiblecomptesouscompte.INFOSELIGIBILITE.MONTANT_MENSUEL_PEN 		= 0;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FRAIS_FIXE_RESILIATION 	= 0;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FRAIS_TOTAL_RESIL 		= 0;
				}
				
				PROC = 1;
				STATUS_PROC = 2;
				
				
				if(eligiblecomptesouscompte.INFOSELIGIBILITE)
				{
					_DATE = DateFunction.formatDateAsString(eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE as Date);
					
					if(!eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE || eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE == null)
						eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE = '1';
					
					if(Number(eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE) > 0)
						ETAT = 1;
					else
						ETAT = 0;
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.ELIGIBLE_COMPTE_LIGNE, true));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur'), 'Consoview');
		}
		
		private function getOperateurLastFactureLigneM111ResultHandler(re:ResultEvent):void
		{
			if(re.result)//---> OK
			{	
				currentOperateur = new Object();
				if((re.result as ArrayCollection).length > 0){
					currentOperateur = re.result[0];				
					dispatchEvent(new CommandeEvent(CommandeEvent.SOUSTETE_OPERATEUR, true));
				}else
				{					
					dispatchEvent(new CommandeEvent(CommandeEvent.SOUSTETE_OPERATEUR, true));
				}
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur'), 'Consoview');
		}
		
		private function checkEligibiliteLignesByLignesAndGetCompteSousCompteM111ResultHandler(re:ResultEvent):void
		{
			if(re.result)//---> OK
			{	
				eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE 	= re.result.INFOSCOMPTESOUSCOMPTE[0];
				
				if(re.result.INFOSELIGIBILITE != null && re.result.INFOSELIGIBILITE.length > 0)
				{
					isElegibilitePresent = true;
					
					eligiblecomptesouscompte.INFOSELIGIBILITE 	= re.result.INFOSELIGIBILITE[0];
				}
				else
				{
					isElegibilitePresent = true;
					
					eligiblecomptesouscompte.INFOSELIGIBILITE 							= new Object();
					eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE 		= null;
					eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELIGIBILITE_STRG		= DateFunction.formatDateAsString(infosEligibilite.DATE_ELLIGIBILITE as Date);
					eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE 				= '1';
					eligiblecomptesouscompte.INFOSELIGIBILITE.RESTE 					= 0;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FIN_PERIODE_CONTRACTUELLE = null;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FPC 						= null;
					eligiblecomptesouscompte.INFOSELIGIBILITE.MONTANT_MENSUEL_PEN 		= 0;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FRAIS_FIXE_RESILIATION 	= 0;
					eligiblecomptesouscompte.INFOSELIGIBILITE.FRAIS_TOTAL_RESIL 		= 0;
				}

				if(eligiblecomptesouscompte.INFOSELIGIBILITE)
				{
					_DATE = DateFunction.formatDateAsString(eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE as Date);
					
					if(!eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE || eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE == null)
						eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE = '1';
					
					if(Number(eligiblecomptesouscompte.INFOSELIGIBILITE.ELLIGIBILITE) > 0)
						ETAT = 1;
					else
						ETAT = 0;
				}
				
				myLigneOperateur.SELECTED 					= true;
				myLigneOperateur.RESTE						= eligiblecomptesouscompte.INFOSELIGIBILITE.NB_JR_FIN_LG;
				myLigneOperateur.FPC						= eligiblecomptesouscompte.INFOSELIGIBILITE.FPC;
				myLigneOperateur.MONTANT_MENSUEL_PEN 		= eligiblecomptesouscompte.INFOSELIGIBILITE.MONTANT_MENSUEL_PEN;
				myLigneOperateur.FRAIS_FIXE_RESILIATION 	= eligiblecomptesouscompte.INFOSELIGIBILITE.FRAIS_FIXE_RESILIATION;
				myLigneOperateur.FIN_PERIODE_CONTRACTUELLE 	= eligiblecomptesouscompte.INFOSELIGIBILITE.FIN_PERIODE_CONTRACTUELLE;
				
				myLigneOperateur.ELIGIBILITE  				= ETAT.toString();
				myLigneOperateur.ETAT_ELIGIBILITE  			= ETAT;
				myLigneOperateur.DATE_ELLIGIBILITE 			= eligiblecomptesouscompte.INFOSELIGIBILITE.DATE_ELLIGIBILITE as Date;
				myLigneOperateur.DATE_ELIGIBILITE_STRG 		= _DATE;

				myLigneOperateur.IDCOMPTE_FACTURATION 		= eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE.IDCOMPTE_FACTURATION;
				myLigneOperateur.IDSOUS_TETE 				= eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE.IDSOUS_TETE;
				myLigneOperateur.SOUS_COMPTE 				= eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE.SOUS_COMPTE;
				myLigneOperateur.COMPTE_FACTURATION 		= eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE.COMPTE_FACTURATION;
				myLigneOperateur.IDSOUS_COMPTE 				= eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE.IDSOUS_COMPTE;
				myLigneOperateur.LIGNE 						= eligiblecomptesouscompte.INFOSCOMPTESOUSCOMPTE.SOUS_TETE;
				
				dispatchEvent(new CommandeEvent(CommandeEvent.ELIGIBLE_COMPTE_LIGNE, true));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Erreur'), 'Consoview');
		}
		
	}
}