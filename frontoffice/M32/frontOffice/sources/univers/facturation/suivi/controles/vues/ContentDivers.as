package univers.facturation.suivi.controles.vues
{
	import mx.resources.ResourceManager;
	import composants.util.LibellesPersos;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparcmobile.GestionParcMobileImpl;
	import gestionparcmobile.event.ContentFicheEvent;
	import gestionparcmobile.event.EventData;
	
	import mx.containers.FormItem;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;

	public class ContentDivers extends VBox
	{

		public var btn_save:Button;

		public var ti_chp1:TextInput;
		public var ti_chp2:TextInput;
		public var ti_chp3:TextInput;
		public var ti_chp4:TextInput;
		public var fi1:FormItem;
		public var fi2:FormItem;
		public var fi3:FormItem;
		public var fi4:FormItem;

		private var libellesPersos:LibellesPersos;

		public static const MODIFICATION_INFOS_COLLAB:String="modificationInfosCollab";

		public function ContentDivers()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			libellesPersos = new LibellesPersos();
			/* optimi
			libellesPersos.getLibellesPersosCollab();
			*/
			libellesPersos.addEventListener(LibellesPersos.LIBELLES_COLLAB_LOADED,treatLibellesCollabLoaded);
			
			ti_chp1.addEventListener(Event.CHANGE, textInputChangeHandler);
			ti_chp2.addEventListener(Event.CHANGE, textInputChangeHandler);
			ti_chp3.addEventListener(Event.CHANGE, textInputChangeHandler);
			ti_chp4.addEventListener(Event.CHANGE, textInputChangeHandler);
		}

		private function treatLibellesCollabLoaded(evtData : EventData):void
		{
			if(evtData.objData)
			{
				fi1.label = (evtData.objData[0] != null && evtData.objData[0] != " :")?evtData.objData[0]:ResourceManager.getInstance().getString('M11', 'Libell__1__');
				fi2.label = (evtData.objData[1] != null && evtData.objData[1] != " :")?evtData.objData[1]:ResourceManager.getInstance().getString('M11', 'Libell__2__');
				fi3.label = (evtData.objData[2] != null && evtData.objData[2] != " :")?evtData.objData[2]:ResourceManager.getInstance().getString('M11', 'Libell__3__');
				fi4.label = (evtData.objData[3] != null && evtData.objData[3] != " :")?evtData.objData[3]:ResourceManager.getInstance().getString('M11', 'Libell__4__');
			}
		}

		private function textInputChangeHandler(evt:Event):void
		{
			btn_save.enabled=true;
			this.dispatchEvent(new Event(MODIFICATION_INFOS_COLLAB));
		}

		public function clickButtonHandler(evt:MouseEvent):void
		{
			this.dispatchEvent(new ContentFicheEvent(ContentFicheEvent.ENREGISTRER_DIVERS, true, GestionParcMobileImpl.ClassFiche.NAV_SELECTED));
		}

		public function set champ1(value:String):void
		{
			ti_chp1.text=value;
		}

		public function set champ2(value:String):void
		{
			ti_chp2.text=value;
		}

		public function set champ3(value:String):void
		{
			ti_chp3.text=value;
		}

		public function set champ4(value:String):void
		{
			ti_chp4.text=value;
		}

		public function get champ1():String
		{
			return ti_chp1.text;
		}

		public function get champ2():String
		{
			return ti_chp2.text;
		}

		public function get champ3():String
		{
			return ti_chp3.text;
		}

		public function get champ4():String
		{
			return ti_chp4.text;
		}
	}
}