package gestionparc.event
{
	import flash.events.Event;

	public class gestionparcEvent extends Event
	{

		public static const POPUP_TRANCHE_VALIDATED:String="popupTrancheValidated";
		public static const POPUP_ABO_OPT_VALIDATED:String="popupAboOptValidated";
		public static const POPUP_SIM_VALIDATED:String="popupSimValidated";

		public static const POPUP_CONFIRMER_ACTION_CONFIRMER:String="popupConfirmerActionConfirmer";
		public static const POPUP_CONFIRMER_ACTION_ANNULER:String="popupConfirmerActionAnnuler";

		public static const NEW_MODELE_ADDED:String="newModeleAdded";
		public static const NEW_LINE_MANUAL_ADDED_OK:String="NewLineManualAddedOk";
		public static const NEW_LINE_MANUAL_ADDED_KO:String="NewLineManualAddedKo";

		public static const EQPT_IMPORTE:String="eqptImporte";

		public static const OPEN_POPUP_CONFIRMATION:String="openPopupConfirmation";

		public static const POPUP_CONFIRMATION_IMPORT:String="popupConfirmationImport";

		public static const POPUP_USAGE_VALIDED:String="popUpUsageValided";
		public static const POPUP_RESILIATION_VALIDATED:String="popup_resiliation_validated";

		public static const INFOS_TYPE_PROFILE:String="INFOS_TYPE_PROFILE";
		public static const LISTE_MAIL_REVENDEUR:String="LISTE_MAIL_REVENDEUR";

		public static const REFRESH_DATALISTE_TERMAVECSIM:String="REFRESH_DATALISTE_TERMAVECSIM";
		public static const REFRESH_DATALISTE_TERMSANSSIM:String="REFRESH_DATALISTE_TERMSANSSIM";
		public static const REFRESH_DATALISTE_TERMFIXESANSEQPFIXE:String="REFRESH_DATALISTE_TERMFIXESANSEQPFIXE";
		public static const REFRESH_DATALISTE_TERMFIXEAVECEQPFIXE:String="REFRESH_DATALISTE_TERMFIXEAVECEQPFIXE";
		public static const REFRESH_DATALISTE_COLLAB:String="REFRESH_DATALISTE_COLLAB";
		public static const REFRESH_DATALISTE_LIGNE:String="REFRESH_DATALISTE_LIGNE";
		public static const REFRESH_DATALISTE_SIMSSTERM:String="REFRESH_DATALISTE_SIMSSTERM";
		public static const REFRESH_DATALISTE_SIMSSLIGNE:String="REFRESH_DATALISTE_SIMSSLIGNE";
		public static const REFRESH_DATALISTE_SIMFIXESSTERM:String="REFRESH_DATALISTE_SIMFIXESSTERM";
		public static const REFRESH_DATALISTE_SIMFIXESSLIGNE:String="REFRESH_DATALISTE_SIMFIXESSLIGNE";

		public static const REFRESH_DATALISTE_EQPOFCOLLAB:String="REFRESH_DATALISTE_EQPOFCOLLAB";
		public static const REFRESH_DATALISTE_LIGNEOFCOLLAB:String="REFRESH_DATALISTE_LIGNEOFCOLLAB";
		public static const REFRESH_DATALISTE_SIMOFCOLLAB:String="REFRESH_DATALISTE_SIMOFCOLLAB";
		public static const REFRESH_DATALISTE_TERMMOBILEOFCOLLAB:String="REFRESH_DATALISTE_TERMMOBILEOFCOLLAB";
		public static const REFRESH_DATALISTE_TERMFIXEOFCOLLAB:String="REFRESH_DATALISTE_TERMFIXEOFCOLLAB";
		public static const REFRESH_DATALISTE_SOUSEQPOFEQP:String="REFRESH_DATALISTE_SOUSEQPOFEQP";
		public static const REFRESH_DATALISTE_SIMFIXEWITHLIGNE_OFCOLLAB:String="REFRESH_DATALISTE_SIMFIXEWITHLIGNE_OFCOLLAB";
		public static const REFRESH_DATALISTE_SIMFIXEWITHOUTLIGNE_COLLAB:String="REFRESH_DATALISTE_SIMFIXEWITHOUTLIGNE_COLLAB";
		public static const REFRESH_LISTE_POOL:String="REFRESH_LISTE_POOL";

		public static const INFOS_FICHE_HISTO_LOADED:String="infosFicheHistoLoaded";
		public static const AFFICHER_HISTORIQUE:String="afficherHistorique";

		public static const INFOS_FICHE_COLLAB_LOADED:String="infosFicheCollabLoaded";
		public static const INFOS_FICHE_COLLAB_UPLOADED:String="infosFicheCollabUploaded";
		public static const INFOS_FICHE_COLLAB_MANAGER_UPLOADED:String="INFOS_FICHE_COLLAB_MANAGER_UPLOADED";
		public static const NEW_COLLAB_ADDED:String="newCollabAdded";
		public static const LIST_LINK_LINE_SIM_TERM_LOADED:String="listLinkLineSimTermLoaded";
		public static const LIST_TERM_COLLAB_LOADED:String="listTermCollabLoaded";
		public static const COLLAB_UPDATE_LIST_LINE_SIM_TERM:String="collabUpdateListLineSimTerm";
		public static const COLLAB_UPDATE_LIST_TERM:String="collabUpdateListTerm";
		public static const MATRICULE_AUTO:String="collabMatriculeAuto";

		public static const INFOS_FICHE_TERM_LOADED:String="infosFicheTermLoaded";
		public static const INFOS_FICHE_TERM_UPLOADED:String="infosFicheTermUploaded";
		public static const INFOS_CMD_SAVED:String="infosCmdSaved";
		public static const UPLOAD_INFOS_CMD:String="uploadInfosCmd";
		public static const NUM_CMD_GENERATED:String="numCmdGenerated";
		public static const LISTE_CONSTRU_LOADED:String="listeConstruLoaded";
		public static const LISTE_MODELES_LOADED:String="listeModelesLoaded";
		public static const ADDNEW_MODELE_LOADED:String="addNewModelesLoaded";
		public static const LISTE_MARQUES_LOADED:String="listeMarquesLoaded";
		public static const LISTE_TYPE_LOADED:String="listeTypeLoaded";
		public static const CHOICE_MODELE_DID:String="choiceModeleDid";
		public static const TERM_UPDATE_LIST_LINE_SIM:String="termUpdateListLineSim";
		public static const LIST_LINK_LINE_SIM_LOADED:String="listLinkLineSimLoaded";
		public static const LIST_SS_EQPT_DISPO_LOADED:String="listSsEqptDispoLoaded";

		public static const INFOS_CONTRAT_SAVED:String="infosContratSaved";
		public static const UPDATE_LISTE_CONTRAT:String="updateListeContrat";
		public static const LISTE_INTERVENTION_LOADED:String="listeInterventionLoaded";

		public static const INFOS_INCIDENT_SAVED:String="infosIncidentSaved";
		public static const UPDATE_LISTE_INCIDENT:String="updateListeIncident";
		public static const INFOS_INTERVENTION_SAVED:String="infosInterventionSaved";
		public static const UPDATE_LISTE_INTERVENTION:String="updateListeIntervention";
		public static const LISTE_INTER_LOADED:String="listeInterLoaded";

		public static const LISTE_REVENDEUR_LOADED:String="listeRevendeurLoaded";
		public static const LISTE_CONTRAT_LOADED:String="listeContratLoaded";

		public static const INFOS_FICHE_SIM_LOADED:String="infosFicheSimLoaded";
		public static const INFOS_FICHE_SIM_UPLOADED:String="infosFicheSimUploaded";

		public static const INFOS_FICHE_LIGNE_LOADED:String="infosFicheLigneLoaded";
		public static const INFOS_FICHE_LIGNE_UPLOADED:String="infosFicheLigneUploaded";
		public static const LISTE_USAGES_LOADED:String="listeUsagesLoaded";
		public static const LISTE_PROFIL_EQPT_LOADED:String="listeProfilEqptLoaded";
		public static const LISTE_REVENDEUR_PROF_EQPT_LOADED:String="listeRevendeurProfEqptLoaded";
		public static const LISTE_OPERATEUR_LOADED:String="listeOperateurLoaded";
		public static const LISTE_TYPES_LIGNE_LOADED:String="listeTypesLigneLoaded";
		public static const LISTE_COMPTES_SOUSCOMPTES_LOADED:String="listeComptesSouscomptesLoaded";
		public static const INFO_OP_LOADED:String="infoOpLoaded";
		public static const INFOS_COL_FACTURE_LOADED:String="infosColFactureLoaded";
		public static const LISTE_HISTO_RACC_LOADED:String="listeHistoRaccLoaded";
		public static const RACCOR_LIGNE_UPLOADED:String="raccorLigneUploaded";
		public static const RACCOR_LIGNE_DID:String="raccorLigneDid";
		public static const LISTE_LINE_DISPO_GRPMENT:String="listeLineDispoGrpment";
		public static const LINE_GRPMENT_ADDED:String="lineGrpmentAdded";
		public static const OPEN_POPUP_ACTION:String="openPopupAction";
		public static const SELECTED_TYPE_LIGNE_CHANGED:String="selected_type_ligne_changed";

		public static const CHBX_SELECTED:String="chbxSelected";
		public static const CHBX_NO_SELECTED:String="chbxNoSelected";
		public static const STATUT_SELECTED_CHECKBOX:String="statutSelectedCheckbox";
		public static const CHBX_SELECTION_CHANGE:String="chbxSelectionChange";
		
		public static const INFOS_FICHE_SITE_LOADED:String="infosFicheSiteLoaded";
		public static const INFOS_EMPLA_SITE_LOADED:String="infosEmplaSiteLoaded";
		public static const INFOS_FICHE_SITE_UPLOADED:String="infosFicheSiteUploaded";

		public static const EDITER_ELT_NAV:String="editerEltNav";
		public static const AJOUTER_ELT_NAV:String="ajouterEltNav";
		public static const SUPPRIMER_ELT_NAV:String="supprimerEltNav";

		public static const LISTE_PAYS_LOADED:String="listePaysLoaded";
		public static const LIST_CATEGORIES_EQPT_LOADED:String="listCategoriesEqptLoaded";
		public static const LIST_TYPES_EQPT_LOADED:String="listTypesEqptLoaded";
		public static const LIST_TYPES_EQPT_WITH_CAT_LOADED:String="listTypesEqptWithCatLoaded";
		public static const LISTE_SITE_LOADED:String="listeSiteLoaded";
		public static const LISTE_MODE_RACCOR_LOADED:String="listeModeRaccorLoaded";
		public static const LISTE_TOUS_OP_LOADED:String="listeTousOpLoaded";
		//public static const MATRICE_POOL_PROFIL_LOADED:String="matricePoolProfilLoaded";
		public static const LISTE_ACT_RACCOR_LOADED:String="listeActRaccorLoaded";

		public static const MISE_AU_REBUT_VALIDATE:String="MISE_AU_REBUT_VALIDATE";

		public static const LISTED_PROFILES_LOADED:String="listedProfilesLoaded";
		public static const LISTED_ABO_OPT_LOADED:String="listedAboOptLoaded";

		public static const LISTED_OPERATEUR_LOADED:String="listedOperateurLoaded";
		public static const LISTED_REVENDEUR_LOADED:String="listedRevendeurLoaded";
		public static const LISTED_MODELE_LOADED:String="listedModeleLoaded";

		public static const TYPE_LIGNE_LOADED:String="typeLigneLoaded";

		public static const READ_CSV_AND_CSV_TO_ARRAY:String="readCsvAndCsvToArray";
		public static const READ_XLS_AND_XLS_TO_ARRAY:String="readXlsAndXlsToArray";

		public static const THIS_IS_FAVORITE:String="thisIsFavorite";
		public static const THIS_IS_NOT_FAVORITE:String="thisIsNotFavorite";

		public static const INIT_COMBO_CONSTRUCTEUR_FINISH:String="initComboConstructeurFinish";

		public static const FICHE_NEW_LINE_ORGA_SELECTED:String="ficheNewLineOrgaSelected";

		public static const OBJECT_DESAFFECTED:String="objectDesaffected";
		public static const OBJECT_AFFECTED:String="objectAffected";

		public static const RECHERCHER_FINISHED:String="rechercherFinished";
		public static const RECUPERER_DROITUSER_FINISHED:String="recupererDroitUserFinished";

		public static const LISTED_NODES_ORGA_LOADED:String="listedNodesOrgaLoaded ";
		public static const LISTE_ORGA_LOADED:String="listeOrgaLoaded ";

		public static const LISTE_LIGNE_SIM_SS_TERM_LOADED:String="listeLigneSimSsTermLoaded";
		public static const LISTE_LIGNE_FIXE_SS_EQP_LOADED:String="listeLigneFixeSsEqpLoaded";
		public static const LISTE_LIGNE_SS_TERM_LOADED:String="listeLigneSsTermLoaded";

		public static const REFRESH_MAIN_GRID:String="refreshMainGrid";
		public static const RECEIVER_IHM_VALIDATE:String="receiverIhmValidate";
		public static const RECEIVER_IHM_CANCEL:String="receiverIhmCancel";
		public static const ASSOCIATION_REALIZED:String="associationRealized";
		public static const DISSOCIATION_REALIZED:String="dissociationRealized";

		public static const ASSOCIER_COLLAB_LIGNE_SIM_DONE:String="associerCollabToLigneSimDone";
		public static const ASSOCIER_COLLAB_LIGNE_DONE:String="associerCollabToLigneDone";
		public static const ASSOCIER_EQPT_SOUS_EQPT_DONE:String="associerEqptSousEqptDone";

		public static const MENU_ROLL_OVER:String="menuRollOver";
		public static const MENU_ROLL_OUT:String="menuRollOut";
		public static const MENU_ITEM_CLICK:String="menuItemClick";

		public static const VALIDE_FICHE_ORGA:String="valideFicheOrga ";
		public static const VALIDE_POPUP:String="validePopup ";
		public static const SEGMENT_FIXE_SELECTED:String="SEGMENT_FIXE_SELECTED";
		public static const SEGMENT_MOBILE_SELECTED:String="SEGMENT_MOBILE_SELECTED";

		public static const RESTRICTION_SELECTED:String="RESTRICTION_SELECTED";

		public static const DELETE_INCIDENT:String="deleteIncident";
		public static const DELETE_INTERVENTION:String="deleteIntervention";
		public static const LISTE_CAUSE_LOADED:String="listeCauseLoaded";
		public static const DELETE_CONTRAT:String="deleteContrat";
		public static const INIT_DEPART_ENTREPRISE_LOADED:String="initDepartEntrepriseLoaded";

		public static const REFRESH_LIBELLESELECTION:String="REFRESH_LIBELLESELECTION";
		
		
		public static const REFRESH_DATALISTE_EXTAVECLIGNE:String="REFRESH_DATALISTE_EXTAVECLIGNE";
		public static const REFRESH_DATALISTE_EXTSANSLIGNE:String="REFRESH_DATALISTE_EXTSANSLIGNE";
		public static const GET_COMPTE_FACTURATION :String="GET_COMPTE_FACTURATION";
		
		public static const LISTE_MANAGERS :String = 'LISTE_MANAGERS';
		public static const MANAGER_SELECTED :String = 'MANAGER_SELECTED';
		
		public static const LISTE_COLONNE_PERSO:String = 'LISTE_LIBELLE_PERSO';
		
		public static const VALID_CHOIX_POOL	:String = 'VALID_CHOIX_POOL';
		public static const VALID_CHOIX_FILTRE	:String = 'VALID_CHOIX_FILTRE';
		
		public static const LISTE_VALEURS_CHAMPS_PERSO :String = 'LISTE_VALEURS_CHAMPS_PERSO';
		public static const LISTE_DESTINA_SIM_STOCK :String = 'LISTE_DESTINA_SIM_STOCK';
		
		public static const VALID_CHOIX_COMPTE		:String = 'VALID_CHOIX_COMPTE';
		public static const VALID_CHOIX_SOUSCOMPTE		:String = 'VALID_CHOIX_SOUSCOMPTE';
		public static const MESSAGE_ECHANGE_SIM_SENDED		:String = 'MESSAGE_ECHANGE_SIM_SENDED';
		
		public var obj:Object=null;

		public function gestionparcEvent(type:String, objet:Object=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.obj=objet;
		}

		override public function clone():Event
		{
			return new gestionparcEvent(type, obj, bubbles, cancelable);
		}

	}
}
