package event.kpi
{
	import flash.events.Event;

	public class KpiChangeEvent extends Event
	{
		public static const CLOSE_KPI_EVENT:String="closeKpiEvent";
		public static const GOTO_MODULE_EVENT:String="gotoModuleEvent";
		public static const LISTER_KPI_EVENT:String="listerKpiEvent";
		public static const LISTER_ALL_KPI_EVENT:String="listerAllKpiEvent";
		public static const ADD_KPI_EVENT:String="addKpiEvent";
		
		public function KpiChangeEvent(type:String)
		{
			super(type, true, true);
		}
	}
}
