package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleAssocierSousEqpForEqp implements IRuleDisplayItem
	{
		public function RuleAssocierSousEqpForEqp()
		{
		}

		public function isDisplay():Boolean
		{
			var r2:IRuleDisplayItem=new IsLigneFixe();
			return r2.isDisplay();
		}
	}
}
