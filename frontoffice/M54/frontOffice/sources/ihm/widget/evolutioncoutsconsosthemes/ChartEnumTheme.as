package ihm.widget.evolutioncoutsconsosthemes
{
	import mx.collections.ArrayCollection;
	
	public class ChartEnumTheme
	{
		public static const CATEGORY_ONE:String = "mois";
		public static const CATEGORY_TWO:String = "lib_theme";/** valeur définie dans le VO*/
		
		public static const SERIES_ONE:String = "31";
		public static const SERIES_TWO:String = "m";
		public static const SERIES_THREE:String = "mnt_data";
		
		public static const SERIES:ArrayCollection = new ArrayCollection([
			SERIES_ONE,
			SERIES_TWO,
			SERIES_THREE
		]);
		
		public function ChartEnumTheme()
		{
		}
	}
}