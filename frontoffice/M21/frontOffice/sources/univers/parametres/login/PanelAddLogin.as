package univers.parametres.login
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	
	public class PanelAddLogin extends PanelAddLoginIHM
	{
		private var modification 	: Boolean = false;
		private var objGestionLogin : GestionLogin;
		
		private var idGestionnaire 	: int = 0;
		private var profileSelected	: Array = new Array();
		
		
		public function PanelAddLogin(modification : Boolean,objGestionLogin : GestionLogin) 
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			this.modification = modification;
			this.objGestionLogin=objGestionLogin;
//			addEventListener("poolChange",poolChangeHandler);
		}
		private function initIHM(evt : FlexEvent):void{
			addEventListener(CloseEvent.CLOSE,fermer);
			btFermer.addEventListener(MouseEvent.CLICK,fermer);
			btEnresgistrer_Nouveau.addEventListener(MouseEvent.CLICK,enregistrer);
			
			inputPwd.text=generateMotDePasse(7);
//			initPools();
			idGestionnaire = 0;
			if(modification == true)
			{
				inputNom.text			= objGestionLogin.dgTousLesLogins.selectedItem.LOGIN_NOM;
				inputPrenom.text		= objGestionLogin.dgTousLesLogins.selectedItem.LOGIN_PRENOM;
				inputAdresse.text		= objGestionLogin.dgTousLesLogins.selectedItem.ADRESSE_POSTAL;
				inputCodePostal.text	= objGestionLogin.dgTousLesLogins.selectedItem.CODE_POSTAL;
				inputVille.text			= objGestionLogin.dgTousLesLogins.selectedItem.VILLE_POSTAL;
				inputTelephone.text		= objGestionLogin.dgTousLesLogins.selectedItem.TELEPHONE;
				inputDirection.text		= objGestionLogin.dgTousLesLogins.selectedItem.DIRECTION;
				inputEmail.text			= objGestionLogin.dgTousLesLogins.selectedItem.LOGIN_EMAIL;
				inputPwd.text			= objGestionLogin.dgTousLesLogins.selectedItem.LOGIN_PWD;
				idGestionnaire			= objGestionLogin.dgTousLesLogins.selectedItem.APP_LOGINID;
				
				if(objGestionLogin.dgTousLesLogins.selectedItem.RESTRICTION_IP==1)
				{
					checkRestrictionIP.selected=true;
				}
				else
				{
					checkRestrictionIP.selected=false;
				}
			}
			fournirListeProfiles();
			checkRestrictionIP.addEventListener(Event.CHANGE,updateRestrictionIP);
		}
		private function updateRestrictionIP(evt : Event):void
		{
			if(checkRestrictionIP.selected == true)
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'Attention__si_vous_activez_la_restrictio'));
			}
		}
		private function generateMotDePasse(longueur :int):String
		{
		   var chars :String  = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789"; 
		   var pass : String = "";
		   
		    for(var i :int=0;i<longueur;i++)
		    {
		    	var x : int = Math.floor((Math.random() * 62)); // 62 nbChoix
		       	pass += chars.charAt(x);
		    }
		   	return pass;
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function enregistrer(evt : MouseEvent):void
		{
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var objGROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
			var PERIMETRE_INDEX : int = perimetreObj.PERIMETRE_INDEX; // idPerimetre ( groupe client) 
			var restriction :int;
			if(checkRestrictionIP.selected==true)
			{
				restriction  = 1;
			}
			else
			{
				restriction  = 0;
			}
			if (inputNom.length==0 || inputPrenom.length==0 || inputEmail.length==0 || inputPwd.length==0){
				Alert.show(ResourceManager.getInstance().getString('M21', 'Veuillez_remplir_tous_les_champs'));
			}
			else
			{
				if(modification == false)
				{
					var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																		"createUtilisateur",
																		processCreaUser,erreur);
					RemoteObjectUtil.callService(op,inputNom.text,
													inputPrenom.text,
													inputAdresse.text,
													inputCodePostal.text,
													inputVille.text,
													inputTelephone.text,
													inputDirection.text,
													inputEmail.text,
													inputPwd.text,
													restriction);
					
				}
				else
				{
					var op2:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																		"updateUtilisateur",
																		processModifUser,erreur);
							
					RemoteObjectUtil.callService(op2,objGestionLogin.dgTousLesLogins.selectedItem.APP_LOGINID,
														inputNom.text,
														inputPrenom.text,
														inputAdresse.text,
														inputCodePostal.text,
														inputVille.text,
														inputTelephone.text,
														inputDirection.text,
														inputEmail.text,
														inputPwd.text,
														restriction);
				}
			}
		}
		private function processCreaUser(evt : ResultEvent):void
		{
			if(evt.result != -1)
			{
				idGestionnaire = evt.result as int;
				objGestionLogin.actualise_dgLogin();
				objGestionLogin.setLoginForSelection(inputEmail.text);
				addProfilInGestionnaire(true);
			}
		}
		private function processModifUser(evt : ResultEvent):void
		{
			if(evt.result != -1)
			{
				objGestionLogin.actualise_dgLogin();
				objGestionLogin.setLoginForSelection(inputEmail.text);
				addProfilInGestionnaire(false);
			}
		}
		
//		private function affecter():void
//		{
//			if(modification == false)
//			{
//				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
//																		"add_gestionnaire_in_pool",
//																		process_affecter,erreur);
//				RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,gestionnaire.id,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,combo_collab.selectedIndex,dg_profile.selectedItem.IDPROFIL_COMMANDE);
//			}
//			else
//			{
////				var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
////																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
////																		"updatePoolGestionnaire", 
////																		process_affecter,erreur);
////			
////				RemoteObjectUtil.callService(op,_IDPool,_libelle_pool,_codeInterne_pool,_commentaire_pool,CvAccessManager.getSession().USER.CLIENTACCESSID,_IDPoolRevendeur);
//			} 
//			
//		}
//		private function process_affecter(evt : ResultEvent):void
//		{
//			if(evt.result != -1)
//			{
//				 addProfilInGestionnaire(modification);
//			}
//		}

		private function attributProfilSelected(selectedItems:ArrayCollection):ArrayCollection
		{
			var profilSelected:ArrayCollection = new ArrayCollection();
			for(var i:int = 0;i < selectedItems.length;i++)
			{
				if(selectedItems[i].SELECTED)
				{
					profilSelected.addItem(selectedItems[i]);
				}
			}
			return profilSelected;
		}		
		
		private function addProfilInGestionnaire(bool:Boolean):void
		{
			if(bool)
			{
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																	"addProfilInGestionnaire",
																	addProfilInGestionnaireHandler,erreur);
				RemoteObjectUtil.callService(op,idGestionnaire,attributProfilSelected(dgProfil.dataProvider as ArrayCollection).source);
			}
			else
			{
				var op2:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																	"eraseProfilInGestionnaire",
																	eraseProfilInGestionnaireHandler,erreur);
				RemoteObjectUtil.callService(op2,idGestionnaire);
			}
		}

		private function eraseProfilInGestionnaireHandler(evt:ResultEvent):void
		{
			if(evt.result != -1)
			{
				addProfilInGestionnaire(true);
			}
		}
		
		private function addProfilInGestionnaireHandler(evt:ResultEvent):void
		{
			if(evt.result != -1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Les_information_ont_bien__t__enregistr_e'));
				PopUpManager.removePopUp(this);
			}
		}
		
		private function erreur(evt : FaultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Erreur___addApp_Login'));
		}
		
//		public function initPools(evt : Event = null):void
//		{
//			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//																	"fr.consotel.consoview.inventaire.sncf.commande_sncf",
//																	"fournirListePools",
//																	process_getProfile);
//			RemoteObjectUtil.callService(op);	
//		}
//		
//		private function process_getProfile(re : ResultEvent):void
//		{
//			if(re.result != null)
//			{
//				if(re.result.length > 1)
//				{
//					cbxPool.prompt = ResourceManager.getInstance().getString('M21', 'S_lectionnez');
//					cbxPool.dataProvider = re.result;
//				}
//				else
//				{
//					cbxPool.prompt = "";
//					cbxPool.dataProvider = re.result;
//					poolChangeHandler(re);//APPEL DE PROCEDURE PERMETTANT DE LISTER LES PROFIL DE COMMANDE ACCESSIBLE DU POOL
//				}
//			}
//			else
//			{
//				Alert.show(ResourceManager.getInstance().getString('M21', 'La_liste_des_pools_est_indisponible_'));
//			}
//		}
//
//		public function poolChangeHandler(e:Event):void
//		{
//			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//																	"fr.consotel.consoview.inventaire.sncf.commande_sncf",
//																	"fournirListeProfiles",
//																	fournirListeProfilesResultHandler);
//			RemoteObjectUtil.callService(op,idGestion);	
//		}
		
		
		public function fournirListeProfiles():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																	"fournirListeProfilesGeneric",
																	fournirListeProfilesResultHandler);
			RemoteObjectUtil.callService(op,idGestionnaire);	
		}
		
		private function fournirListeProfilesResultHandler(re : ResultEvent):void
		{
			if(re.result != null)
			{
				dgProfil.dataProvider = null;
				dgProfil.dataProvider = attributNewObject(re.result as ArrayCollection);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'La_liste_des_profiles_est_indisponible_'));
			}
		}
		
		private function attributNewObject(value:ArrayCollection):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			var i:int = 0;
			var object:Object = new Object();
			if(!modification)
			{
				for(i = 0; i < value.length;i++)
				{
					object = new Object();
					object.LIBELLE_PROFIL  		= value[i].LIBELLE_PROFIL;
					object.IDRACINE				= value[i].IDRACINE;
					object.IDPROFIL_EQUIPEMENT	= value[i].IDPROFIL_EQUIPEMENT;
					object.SELECTED				= false;
					arrayCollection.addItem(object);
				}
			}
			else
			{
				for(i = 0; i < value.length;i++)
				{
					object = new Object();
					object.LIBELLE_PROFIL  		= value[i].LIBELLE_PROFIL;
					object.IDRACINE				= value[i].IDRACINE;
					object.IDPROFIL_EQUIPEMENT	= value[i].IDPROFIL_EQUIPEMENT;
					object.SELECTED				= false;
					if(value[i].SELECTED == 1)
					{
						object.SELECTED			= true;
					}
					arrayCollection.addItem(object);
				}
			}
			return arrayCollection;
		}
		
//		private function affecter():void
//		{
//			if(dgProfil.selectedIndex != -1 && cbxPool.selectedItem != null)
//			{
//				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//																		"fr.consotel.consoview.inventaire.sncf.commande_sncf",
//																		"addGestionnaireInPool",
//																		process_affecter);
//				RemoteObjectUtil.callService(op,cbxPool.selectedItem.IDPOOL,
//												gestionnaire.id,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,
//												combo_collab.selectedIndex,
//												dg_profile.selectedItem.IDPROFIL_COMMANDE);
//			}
//			else
//			{
//				
//				if(dgProfil.selectedIndex == -1 && cbxPool.selectedItem == null)
//				{
//					Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_pool_et_un_profil_'));
//				}
//				else
//				{
//					if(cbxPool.selectedItem == null)
//					{
//						Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_pool_'));
//					}
//					else
//					{
//						if(dgProfil.selectedIndex == -1)
//						{
//							Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionnez_un_profil_'));
//						}
//					}
//				}
//			} 
//		}
//		
//		private function process_affecter(evt : ResultEvent):void
//		{
//			PopUpManager.removePopUp(this);
//		}
		
	}
}