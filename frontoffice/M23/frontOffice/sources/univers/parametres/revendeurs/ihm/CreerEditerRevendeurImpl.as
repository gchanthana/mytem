package univers.parametres.revendeurs.ihm
{
	///////////////////////////////////////////////////////////
	//  CreerEditerRevendeurImpl.as
	//  Macromedia ActionScript Implementation of the Class CreerEditerRevendeurImpl
	//  Generated by Enterprise Architect
	//  Created on:      20-ao�t-2008 16:26:32
	//  Original author: samuel.divioka
	///////////////////////////////////////////////////////////

	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.pays.GestionPays;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.revendeurs.system.AbstractGestionAgences;
	import univers.parametres.revendeurs.system.AbstractGestionContacts;
	import univers.parametres.revendeurs.system.AbstractGestionRevendeurs;
	import univers.parametres.revendeurs.system.Agence;
	import univers.parametres.revendeurs.system.GestionAgencesFixeData;
	import univers.parametres.revendeurs.system.GestionContactsFixeData;
	import univers.parametres.revendeurs.system.GestionRevendeursFixeData;
	import univers.parametres.revendeurs.system.Revendeur;

	/**
	 * [Bindable]
	 * 
	 * Formulaire de cr�ation et d'�dition d'un Revendeur.
	 * 
	 * - Afficher le d�tail d'un revendeur.
	 * - Afficher l'�cran liste des agences d'un revendeur(ListeAgencesRevendeurIHM).
	 * - Afficher l'�cran liste des contacts d'un revendeur et de ses
	 * agences(ListeContactsRevendeurIHM)
	 * - Cr�er un revendeur.
	 * - Mettre � jour les infos d'un revendeur.
	 * - Fermer le fen�tre.
	 * @author samuel.divioka
	 * @version 1.0
	 * @created 20-ao�t-2008 16:26:32
	 */
	 
	[Bindable] 
	public class CreerEditerRevendeurImpl extends PopUpImpl
	{
	    /**
	     * Enabled = false tant que IDCDE_CONTACT_SOCIETE &lt;= 0  
	     */
	    public var btAgences: Button;
	    /**
	     * Enabled = false tant que IDCDE_CONTACT&lt;= 0  
	     */
	    public var btContacts: Button;
	    /**
	     * Enabled = false si les champs obligatoire ne sont pas saisies et si
	     * _modeEcriture = false.
	     */
	    public var btSave: Button;
	    public var _initRaisonSociale:String  = "";
	    public var txtRaisonSociale: TextInputLabeled;
	    public var txtSIREN: TextInputLabeled;
	    public var txtAdresse1: TextInputLabeled;
	    public var txtAdresse2: TextInputLabeled;
	    public var txtCommune: TextInputLabeled;
	    public var txtCP: TextInputLabeled;
	    public var cboPays: ComboBox;
	    public var txtTelephone: TextInputLabeled;
	    public var txtFax: TextInputLabeled;
	    public var txtCodeInterne: TextInputLabeled;
	    public var ckbxActiveRevendeur:CheckBox;
	    
	    
	    /**
	     * Configure la pop-up suivant le mode Edition/Creation.
	     * Si modeCreation = true alors la popUp s'affiche avec un formulaire vide sinon
	     * on affiche le d�tail de l'agence principale du revendeur s�lectionn�
	     */
	    public var modeCreation: Boolean = true;
	    public var m_AbstractGestionRevendeurs: AbstractGestionRevendeurs = new GestionRevendeursFixeData();
	    public var m_AbstractGestionAgences: AbstractGestionAgences = new GestionAgencesFixeData();
	    public var m_AbstractGestionContacts: AbstractGestionContacts = new GestionContactsFixeData();
	    
	    private var MesRevendeurs : MesRevendeursImpl;
	    
		private var _paysManager:GestionPays = new GestionPays();
		public var modeAffectation: Boolean = false;
		public var ckbxSelectedOrNot: Boolean = false;
		
		public var idRevendeur:int = 0;
	    		
		public function get paysManager():GestionPays
		{
			return _paysManager;
		}	    
	
		
	    /**
	    * Initialisation
	    * 
	    */
	    protected function creationCompletHandler(fe : FlexEvent):void
	    {
	    	if(modeCreation == false)
	    	{
	    		paysManager.getListePays(m_AbstractGestionRevendeurs.revendeur.PAYSCONSOTELID);
	    	}
	    	else
	    	{
	    		paysManager.getListePays();
	    	}
	    }		

	    /**
	     * affiche le revendeur
	     */
	    public function afficherRevendeur(): void
	    {  }

	    /**
	     * Affiche la fen�tre 'Liste des Agences de revendeur(ListeAgencesRevendeurIHM)'.
	     * 
	     * @param me    MouseEvent.CLIK
	     */
	    public function btAgencesClickHandler(me:MouseEvent): void
	    {
	    	if(_popUp != null) _popUp = null;
			_popUp = new ListeAgencesRevendeurIHM;
			_popUp.modeEcriture = modeEcriture;
			
			var revendeur:Revendeur = new Revendeur();
			
			revendeur.IDCDE_CONTACT_SOCIETE = m_AbstractGestionRevendeurs.revendeur.IDCDE_CONTACT_SOCIETE;
			revendeur.LIBELLE = m_AbstractGestionRevendeurs.revendeur.LIBELLE;
			
			m_AbstractGestionAgences.revendeur = revendeur;
			
			ListeAgencesRevendeurIHM(_popUp).m_AbstractGestionAgences = m_AbstractGestionAgences;
			PopUpManager.addPopUp(_popUp,this,true);
			PopUpManager.centerPopUp(_popUp);	    	
	    }

	    /**
	     * Affiche la fen�tre 'Liste des Contacts de l'agence (ListeContactsRevendeurIHM)'
	     * 
	     * @param me    MouseEvent.CLIK
	     */
	    public function btContactsClickHandler(me:MouseEvent): void
	    {
	    	if(_popUp != null) _popUp = null;
			_popUp = new ListeContactsRevendeurIHM;
			_popUp.modeEcriture = modeEcriture;
			
			var agence:Agence = new Agence();	
			agence.IDCDE_CONTACT_SOCIETE = m_AbstractGestionRevendeurs.revendeur.IDCDE_CONTACT_SOCIETE;

			
			ListeContactsRevendeurIHM(_popUp).m_AbstractGestionContacts = m_AbstractGestionContacts;
			ListeContactsRevendeurIHM(_popUp).m_AbstractGestionContacts.agence = agence;
			PopUpManager.addPopUp(_popUp,this,true);
			PopUpManager.centerPopUp(_popUp);	    	
	    }

	    /**
	     * Sauvegarde les modifications en mode Edition (_revendeur.ID.&gt; 0)
	     * Cr�ation d'un revendeur en mode Cr�ation.
	     * 
	     * @param me    MouseEvent.CLIK
	     */
	    public function btSaveClickHandler(me:MouseEvent): void
	    {
	    	var currentRevendeur : Revendeur = new Revendeur();
	    	currentRevendeur.IDCDE_CONTACT_SOCIETE  = m_AbstractGestionRevendeurs.revendeur.IDCDE_CONTACT_SOCIETE;
			currentRevendeur.Raison_sociale = txtRaisonSociale.text;
			currentRevendeur.SIREN = txtSIREN.text;
			currentRevendeur.ADRESSE1 = txtAdresse1.text;
			currentRevendeur.ADRESSE2 = txtAdresse2.text;
			currentRevendeur.COMMUNE = txtCommune.text;
			currentRevendeur.ZIPCODE = txtCP.text;
			currentRevendeur.PAYSCONSOTELID = cboPays.selectedItem.ID;
			currentRevendeur.TELEPHONE = txtTelephone.text;
			currentRevendeur.FAX = txtFax.text;
			currentRevendeur.CODE_INTERNE = txtCodeInterne.text;
			
			var selectedAgence : Agence = new Agence();
			selectedAgence.IDCDE_CONTACT_SOCIETE = currentRevendeur.IDCDE_CONTACT_SOCIETE;
			selectedAgence.RAISON_SOCIALE = currentRevendeur.Raison_sociale ;
			selectedAgence.SIREN = currentRevendeur.SIREN;
			selectedAgence.ADRESSE1 = currentRevendeur.ADRESSE1;
			selectedAgence.ADRESSE2 = currentRevendeur.ADRESSE2;
			selectedAgence.COMMUNE = currentRevendeur.COMMUNE;
			selectedAgence.ZIPCODE = currentRevendeur.ZIPCODE;
			selectedAgence.PAYSCONSOTELID = currentRevendeur.PAYSCONSOTELID;
			selectedAgence.TELEPHONE = currentRevendeur.TELEPHONE;
			selectedAgence.FAX = currentRevendeur.FAX;
			selectedAgence.CODE_INTERNE = currentRevendeur.CODE_INTERNE;			
			
	    	if(modeCreation)
	    	{
		    	m_AbstractGestionRevendeurs.revendeur = currentRevendeur;	  
		    	m_AbstractGestionRevendeurs.addEventListener(AbstractGestionRevendeurs.REVENDEUR_ENREGISTRE,revendeurEnregistrerHandler);  		
				m_AbstractGestionRevendeurs.enregistrerRevendeur(m_AbstractGestionRevendeurs.revendeur);
//				m_AbstractGestionRevendeurs.addEventListener("revendeurEnregistre",revendeurEnregistreHandler);
	    	}
	    	else
	    	{
		    	currentRevendeur.ID = idRevendeur;
		    	m_AbstractGestionRevendeurs.revendeur = currentRevendeur;	    		
	    		if(initRaisonSociale != txtRaisonSociale.text)
	    		{
	    			m_AbstractGestionRevendeurs.addEventListener(AbstractGestionRevendeurs.REVENDEUR_ENREGISTRE,revendeurEnregistrerHandler);
	    			m_AbstractGestionRevendeurs.modifierRevendeur(m_AbstractGestionRevendeurs.revendeur);
	    		}
	    		else
	    		{
	    			m_AbstractGestionAgences.addEventListener(AbstractGestionAgences.AGENCE_MAJ,agenceEnregistrerHandler);
	    			m_AbstractGestionAgences.modifierAgence(selectedAgence);//m_AbstractGestionRevendeurs.modifierRevendeur(m_AbstractGestionRevendeurs.revendeur);
	    		}
	    	}
	    	
	    }
	    
	    private function revendeurEnregistreHandler(e:Event):void
	    {
	    	affectationRevendeur(modeCreation);
	    }
	    
	    private function affectationRevendeur(creationModeOrNot:Boolean):void//IL FAUDRA FAIRE DIFFERENT CAS//**************************************---------------------------
	    {
	    	if(creationModeOrNot)
	    	{
	    		if(ckbxActiveRevendeur.selected)
	    		{
	    			m_AbstractGestionRevendeurs.revendeur.ID = m_AbstractGestionRevendeurs.idNewRevendeur;
	    			m_AbstractGestionRevendeurs.affecterRevendeur(m_AbstractGestionRevendeurs.revendeur);
	    			ConsoviewAlert.afficherOKImage("Revendeur créé et activer !");
	    		}
	    		else
	    		{
	    			ConsoviewAlert.afficherOKImage("Revendeur créé !");
	    		}
	    	}
	    	else
	    	{
		    	if(ckbxActiveRevendeur.selected)
	    		{
	    			if(!modeAffectation)
	    			{
	    				m_AbstractGestionRevendeurs.affecterRevendeur(m_AbstractGestionRevendeurs.revendeur);
	    				ConsoviewAlert.afficherOKImage("Revendeur mis à jour et affecté !");
	    			}
	    			else
	    			{
	    				ConsoviewAlert.afficherOKImage("Revendeur mis à jour !");
	    			}
	    		}
	    		else
	    		{
	    			if(modeAffectation)
	    			{
	    				m_AbstractGestionRevendeurs.desaffecterRevendeur(m_AbstractGestionRevendeurs.revendeur);
	    				ConsoviewAlert.afficherOKImage("Revendeur mis à jour et désaffecté !");
	    			}
	    			else
	    			{
	    				ConsoviewAlert.afficherOKImage("Revendeur mis à jour !");
	    			}
	    		}
	    	}
	    }
	    
	    private function revendeurEnregistrerHandler(event:Event):void
	    {
	    	m_AbstractGestionRevendeurs.removeEventListener(AbstractGestionRevendeurs.REVENDEUR_ENREGISTRE,revendeurEnregistrerHandler);
	    	affectationRevendeur(modeCreation);
	    	this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
	    }
	    
	    private function agenceEnregistrerHandler(event:Event):void
	    {
	    	m_AbstractGestionAgences.removeEventListener(AbstractGestionAgences.AGENCE_MAJ,agenceEnregistrerHandler);
	    	affectationRevendeur(modeCreation);
	    	this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
	    }	 
	       
		public function get initRaisonSociale():String{
			return _initRaisonSociale;
		}
		
		public function set initRaisonSociale(rs:String):void{
			_initRaisonSociale = rs;
		}
		
	    protected function btCancelClickHandler(me:MouseEvent): void
	    {
	    	this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
	    }  			    

	}//end CreerEditerRevendeurImpl
}	