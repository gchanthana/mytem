package univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterSimCollab;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterSimLigne;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterSimTerm;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.RemplacerSIM;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterSIMOfLigne;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterSIMOfTerm;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterSIMofCollab;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoCollab;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoLigne;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoSim;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoTerm;
	import univers.inventaire.equipements.gestionflottemobile.panelEnconstruction;
	import univers.inventaire.equipements.gestionflottemobile.panelaffectation.PanelAffectation;
	import univers.inventaire.equipements.gestionflottemobile.paneldesaffectation.PanelDesaffectation;
	import univers.inventaire.equipements.gestionflottemobile.panelechange.PanelEchange;
	import univers.inventaire.equipements.gestionflottemobile.rebus.RebusSIM;
	
	[Event(name="btHistoriqueCliked",type="flash.events.MouseEvent")]
	
	public class MenuFlotteMobSIM extends MenuGestionFlotteMobileIHM
	{
		public static const BT_HISTORIQUE_CLIKED:String = "btHistoriqueCliked";
		
		[Embed(source="/assets/equipement/SIM50x50.png")] 
		private var adrImg : Class;
		private var panelGestionFlotteMobile  : PanelGestionFoltteMobile;
		private var popup : TitleWindow;
		private var stateToolTip : Boolean = stateToolTip;
		public function MenuFlotteMobSIM(panelGestionFlotteMobile  : PanelGestionFoltteMobile,stateToolTip : Boolean=false)
		{
			this.panelGestionFlotteMobile=panelGestionFlotteMobile;
			super();
			this.stateToolTip = stateToolTip;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			
		}
				
		private function initIHM(fe : FlexEvent):void
		{
			if(stateToolTip==true)
			{
				bt_sup_sim.visible = false;
				bt_sup_sim.width=0;
				bt_sup_sim.height=0;
				
				bt_new_sim.visible = false;
				bt_new_sim.width=0;
				bt_new_sim.height=0;
				
				bt_mod_sav.visible = false;
				bt_mod_sav.width=0;
				bt_mod_sav.height=0;
			
				bt_mod_renouv.visible = false;
				bt_mod_renouv.width=0;
				bt_mod_renouv.height=0;
			
					
				this.setCurrentState("toolTipState");
				labToolTip = panelGestionFlotteMobile.objMenuFlotteMobSIM_fixe.labToolTip;
			}
			
			var imgNew : Image = new Image();
			imgNew.source=adrImg;
			canvas_icone.addChild(imgNew);
			
			bt_new_user.initParametre(true,"Associer à un utilisateur la carte SIM: ");
			bt_new_tel.initParametre(true,"Associer à un terminal la carte SIM: ");	
			bt_new_num.initParametre(true,"Associer à une ligne la carte SIM: ");
			bt_new_sim.initParametre(false);
			
			bt_sup_user.initParametre(true,"Reprendre à l'utilisateur la carte SIM: ");	
			bt_sup_tel.initParametre(true,"Dissocier du terminal la carte SIM: ");	
			bt_sup_num.initParametre(true,"Dissocier de la ligne la carte SIM: ");
			bt_sup_sim.initParametre(false);
			
			bt_mod_sav.initParametre(false);	
			bt_mod_renouv.initParametre(false,"Renouveler la ligne/SIM: ");	
			bt_mod_preter.initParametre(true,"Remplacer la carte SIM:");
			bt_mod_rebus.initParametre(true,"Mettre au rebut la carte SIM: ");
			btGetHistorique.initParametre(true,"Historique de la carte SIM:");
			
			bt_new_tel.addEventListener(MouseEvent.CLICK,newPanelListeTerminal);
			bt_new_num.addEventListener(MouseEvent.CLICK,newPanelListeLigne);
			bt_new_user.addEventListener(MouseEvent.CLICK,newPanelListeUser);
			/*
			bt_new_tel.addEventListener(MouseEvent.CLICK,newPanelListeTerminal);
			bt_new_user.addEventListener(MouseEvent.CLICK,newPanelListeTerminal);
			bt_new_num.addEventListener(MouseEvent.CLICK,newPanelListeNum);
			*/	
			
			bt_sup_user.addEventListener(MouseEvent.CLICK,newPanelUserOfSIM);
			bt_sup_num.addEventListener(MouseEvent.CLICK,newPanelLigneOfSIM);
			bt_sup_tel.addEventListener(MouseEvent.CLICK,newPanelTermOfSIM);
			
			bt_mod_renouv.addEventListener(MouseEvent.CLICK,newPanelEnConstruction);
			bt_mod_preter.addEventListener(MouseEvent.CLICK,newPanelPreterSIM);
			bt_mod_rebus.addEventListener(MouseEvent.CLICK,newPanelRebus);
			bt_mod_sav.addEventListener(MouseEvent.CLICK,newPanelEnConstruction);
			
			btGetHistorique.addEventListener(MouseEvent.CLICK,_btGetHistoriqueClickHandler);
			
		}
		private function newPanelEnConstruction(event : MouseEvent):void
		{
			addPopup(new panelEnconstruction());
		}
		
		private function newPanelRebus(event : MouseEvent):void
		{
			addPopup(new RebusSIM(PanelGestionFoltteMobile.objLigneSelected));
			//addPopup(new panelEnconstruction());
		}	
		
		private function newPanelListeUser(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE==null)
			{
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"carte SIM"),
									new AffecterSimCollab(),
									new DispoCollab()
									));
			}
			else
			{
				Alert.show("La SIM sélectionnée est déja liée à un collaborateur","Impossible de fournir cette carte SIM à un collaborateur");
			}		
			
		}
		private function newPanelPreterSIM(event : MouseEvent):void
		{
			/*if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE==null)
			{
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"carte SIM"),
									new AffecterSimCollab(),
									new DispoCollab(),
									true,//autorise les prêts
									true//cocher la case préter
									));
			}
			else
			{
				Alert.show("La SIM sélectionnée est déja liée à un collaborateur","Impossible de fournir cette carte SIM à un collaborateur");
			}*/
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE ==null && PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL ==null)
			{
				Alert.show("La carte sim ne peut pas être échangée car elle n'est pas dans une relation (pas de collaborateur et pas de terminal)");
			}
			else
			{
				addPopup(new PanelEchange(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"carte SIM"),
									new RemplacerSIM(),
									new DispoSim()
									
									));
			}		
			
		}
		private function newPanelRebut(event : MouseEvent):void
		{
			//addPopup(new Rebus(PanelGestionFoltteMobile.objLigneSelected,4));
		}
		private function newPanelListeLigne(event : MouseEvent):void
		{
			var obj:Object = PanelGestionFoltteMobile.objLigneSelected;
			if(PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE>0)
			{
				Alert.show("La SIM sélectionnée est déja liée à une ligne","Impossible de lier une ligne à cette carte SIM");
			}
			else
			{
				
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"carte SIM"),
									new AffecterSimLigne(),
									new DispoLigne(),
									false
									));
			}
		}
		private function newPanelListeTerminal(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL==null)
			{
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"carte SIM"),
									new AffecterSimTerm(),
									new DispoTerm(),
									false
									));
			}
			else
			{
				Alert.show("La SIM sélectionnée est déja dans un terminal","Impossible de lier un terminal à cette carte SIM");
			}		
		}
		private function newPanelUserOfSIM(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE!=null)
			{
				[Embed(source="/assets/equipement/User.gif")] 
				var adrImgUser : Class;
				//Pour ce cas, il n'y a pas besoin de liste dispo : Un term ne peut avoir que une SIM.
				addPopup(new PanelDesaffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE),PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR,adrImgUser,"le collaborateur"),
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"de la carte SIM"),
									new DesaffecterSIMofCollab()
									));
			}
			else
			{
				Alert.show("La SIM sélectionnée n'est pas affectée à un collaborateur","Impossible de rendre la SIM");
			}		
		}
		private function newPanelLigneOfSIM(event : MouseEvent):void
		{
			//var idLigne  : int= PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE;
			//if(idLigne > 0)
			if(PanelGestionFoltteMobile.objLigneSelected.LIGNE!=null)
			{
				[Embed(source="/assets/equipement/06-50x50BIG.png")] 
				var adrImgLigne : Class;
				addPopup(new PanelDesaffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE),PanelGestionFoltteMobile.objLigneSelected.LIGNE,adrImgLigne,"la ligne"),
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"de la carte SIM"),
									new DesaffecterSIMOfLigne()
									));
			}
			else
			{
				Alert.show("La SIM sélectionnée n'a pas de ligne","Impossible de disscocier la ligne de la SIM");
			}		
		}
		private function newPanelTermOfSIM(event : MouseEvent):void
		{
			
			if(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL!=null)
			{
				[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
				var adrImgterm : Class;
				//Pour ce cas, il n'y a pas besoin de liste dispo : Un term ne peut avoir que une SIM.
				addPopup(new PanelDesaffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL),PanelGestionFoltteMobile.objLigneSelected.IMEI,adrImgterm,"terminal"),
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"de la carte SIM"),
									new DesaffecterSIMOfTerm()
									
									));
			}
			else
			{
				Alert.show("La SIM sélectionnée n'est pas dans un terminal","Impossible de rendre la SIM");
			}		
		}
		private function addPopup(popup : TitleWindow):void {
			this.popup=popup;
			popup.addEventListener(PanelGestionFoltteMobile.VALIDER_EVENT,closePopUp); // Si il y a une validation
			popup.addEventListener(PanelGestionFoltteMobile.ANNULER_EVENT,closePopUp); // Si il y a une annulation
									
			PopUpManager.addPopUp(popup,panelGestionFlotteMobile,true);
			PopUpManager.centerPopUp(popup);
		}
		private function closePopUp(evt : Event):void {
			if(evt.type == PanelGestionFoltteMobile.ANNULER_EVENT)
			{
				PopUpManager.removePopUp(popup); 
			}
			else if(evt.type == PanelGestionFoltteMobile.VALIDER_EVENT) 
			{
				panelGestionFlotteMobile.actualise(); 
				PopUpManager.removePopUp(popup); 
			}
		}
		
		protected function _btGetHistoriqueClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new MouseEvent(BT_HISTORIQUE_CLIKED));
		}
	}
}
