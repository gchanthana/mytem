<cfcomponent name="session">
	<cffunction name="setData" access="remote" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="q" type="query">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.dataUsage')>
			<cfif IsDefined('session.dataUsage.#cle#')>
				<cfset structDelete(session.dataUsage,"#cle#")>
				<cfset StructInsert(session.dataUsage,cle,q)>
			<cfelse>
				<cfset structInsert(session.dataUsage,"#cle#",q)>
			</cfif>
		<cfelse>
			<cfset st=structNew()>
			<cfset structInsert(st,"#cle#",q)>
			<cfset session.dataUsage=st>
		</cfif>
	</cffunction>
	
	<cffunction name="clearData" access="remote" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.dataUsage')>
			<cfif IsDefined('session.dataUsage.#cle#')>
				<cfset structDelete(session.dataUsage,"#cle#")>
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction name="getData" access="remote" output="true" returntype="query" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="cle" type="String">
		<cfif IsDefined('session.dataUsage')>
			<cfif IsDefined('session.dataUsage.#cle#')>
				<cfreturn session.dataUsage['#cle#']>
			<cfelse>
				<cfset q=queryNew("B")>
				<cfreturn q>
			</cfif>
		<cfelse>
			<cfset q=queryNew("C")>
			<cfreturn q>
		</cfif>
	</cffunction>
</cfcomponent>