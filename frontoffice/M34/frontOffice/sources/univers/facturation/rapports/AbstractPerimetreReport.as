package univers.facturation.rapports {
	
	import univers.facturation.optimisation.AbstractReport;
	import composants.access.perimetre.tree.PerimetreTreeImpl;
	import mx.core.IFactory;
	import composants.access.perimetre.PerimetreTreeItemRenderer;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import composants.tb.periode.AMonth;
	import mx.controls.Menu;
	import flash.net.URLVariables;
	import composants.util.DateFunction;
	import flash.events.MouseEvent;
	import mx.events.MenuEvent;
	import composants.tb.periode.PeriodeEvent;
	import flash.errors.IllegalOperationError;
	import flash.net.URLRequest;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.events.ListEvent;
	import mx.rpc.events.ResultEvent;
	import composants.access.PerimetreTreeEvent;
	import composants.access.perimetre.tree.CvPerimetreWindowIHM;

	/**
	 * Même composition et comportement que AbstractReport mais avec un arbre
	 * des périmètres pour pouvoir choisir un noeud et l'envoyer en paramètre
	 * Ce type de rapport comprend :
	 * Un composant arbre des périmètres : perimetreTree
	 * Le libéllé du périmètre séléctionné : perimetreLabel
	 * Infos concernant le noeud séléctionné : nodeInfos
	 * */
	public class AbstractPerimetreReport extends AbstractReport {
		public var perimetreTree:CvPerimetreWindowIHM; // Arbre des périmètres (IHM)
		
		/**
		 * Chaque rapport doit paramétrer son arbre des périmètres
		 * (Affichage de la racine, chargement des infos du noeud séléctionné, etc...)
		 * dans son constructeur. Par défaut la racine est toujours affichée, le groupe racine est la racine
		 * et les infos du noeud sont chargés
		 * */
		public function AbstractPerimetreReport() {
			super();
			reportType = "AbstractPerimetreReport";
		}
		
		public override function getReportName():String {
			return "AbstractPerimetreReport - Modèle Abstrait de rapport avec arbre des périmètres";
		}
		
		public override function onPerimetreChange():void {
			super.onPerimetreChange();
			if(this.initialized){
				perimetreTree.onPerimetreChange();
			}
		}
		
		/**
		 * Chaque rapport doit enregistrer cette méthode comme listener pour FlexEvent.CREATION_COMPLETE
		 * */
		protected override function initIHM(event:FlexEvent):void {
			super.initIHM(event);
			
			perimetreTree.addEventListener(ConsoViewDataEvent.PERFORM_NODE_SEARCH,onSearchEvent);
			perimetreTree.addEventListener(ConsoViewDataEvent.BACK_NODE_SEARCH,onSearchEvent);
			
			perimetreTree.addEventListener(PerimetreTreeEvent.SELECTED_ITEM_CHANGE,selectedNodeChange);
			perimetreTree.addEventListener(PerimetreTreeEvent.NODE_INFOS_RESULT,selectedNodeChange);
		}
		
		/**
		 * Handler appelé lorsqu'on lance une recherche dans l'arbre des périmètres
		 * ou lorsqu'on revient sur l'arbre des périmètres depuis l'arbre des résultats de recherche de noeuds
		 * */
		protected function onSearchEvent(event:ConsoViewDataEvent):void {
			if(event.type == ConsoViewDataEvent.BACK_NODE_SEARCH)
				actionItem.enabled = true;
			else if(event.type == ConsoViewDataEvent.PERFORM_NODE_SEARCH)
				actionItem.enabled = false;
		}
		
		/**
		 * Handler appelé lorsqu'un noeud de l'arbre est séléctionné
		 * Cette méthode appelle checkAndUpdateActionItemStatus() pour mettre à jour
		 * l'objet qui permet de lancer l'action actuelle
		 * Par défaut cette méthode ne fait rien lorsque les infos du noeud séléctionné
		 * sont chargées. Elle effectue les actions que lorsqu'on séléctionné un noeud.
		 * Mais elle est quand même enregistrée comme listener lorsque les infos concernant le
		 * noeud séléctionné sont chargées
		 * */
		protected function selectedNodeChange(event:PerimetreTreeEvent):void {
			if(event.type == PerimetreTreeEvent.SELECTED_ITEM_CHANGE) {
				checkAndUpdateActionItemStatus();
			}
		}

		/**
		 * L'objet qui lance l'action actuelle n'est activé que si l'on a séléctionné
		 * un noeud est un noeud connectable
		 * */
		protected override function checkAndUpdateActionItemStatus():Boolean {
	/*		if(perimetreTree.getPerimetreTreeSelectedItem() != null)
	//			actionItem.enabled = (perimetreTree.getPerimetreTreeSelectedItem().@STC > 0);
			else
				actionItem.enabled = false;
			return actionItem.enabled;*/
			return false
		}

		/**
		 * Pour ce type de rapport :
		 * Le type périmètre est remplacé par celui du noeud séléctionné si le paramètre
		 * loadDataOnSelection du perimetreTree vaut true sinon il a comme valeur la chaîne vide
		 * Si un rapport particulier a besoin de charger les infos du noeud séléctionné mais qu'il
		 * veut que le type périmètre soit vide alors il suffit de redéfinir cette méthode et de
		 * modifier la valeur du type périmètre
		 * */
		protected override function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			super.getReportUrlRequest(secureBool);
			urlRequestPath.data.PERIMETRE_INDEX = perimetreTree.getPerimetreTreeSelectedItem().@NID;
			urlRequestPath.data.RAISON_SOCIALE = perimetreTree.getPerimetreTreeSelectedItem().@LBL;
			if(perimetreTree.loadDataOnSelection != true) {
				urlRequestPath.data.TYPE_PERIMETRE = "";
			} else {
				urlRequestPath.data.TYPE_PERIMETRE = perimetreTree.nodeInfos.TYPE_PERIMETRE;
			}
			return urlRequestPath;
		}
	}
}
