<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryDefinition" implements="fr.consotel.consoview.api.ibis.publisher.delivery.IDeliveryDefinition">
	<cfset init()>
	<cffunction name="init" returntype="fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryDefinition" access="private" description="Constructor">
		<cfset VARIABLES["DELIVERY_OPTIONS"]=structNew()>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="getType" returntype="string" access="public"
				description="Returns delivery type localOption (LOCAL DELIVERY OPTION).">
		<cfreturn "NULL">
	</cffunction>
	
	<cffunction name="setParameter" returntype="boolean" access="public" description="Set value for a delivery parameter.">
		<cfargument name="parameterName" type="string" required="true" hint="Value is destination for this component">
		<cfargument name="parameterValue" type="string" required="true" hint="Value is remote file absolute path">
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="getParameterInfos" returntype="struct" access="public"
				description="Returns Delivery Parameters Infos as a struture. This can be provided as delivery option when executing report.">
		<cfreturn VARIABLES["DELIVERY_OPTIONS"]>
	</cffunction>
</cfcomponent>