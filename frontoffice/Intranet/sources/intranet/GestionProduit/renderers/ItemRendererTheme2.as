package intranet.GestionProduit.renderers
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import intranet.GestionProduit.DataSample;
	import intranet.GestionProduit.event.ChangeData_EVENT;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class ItemRendererTheme2 extends HBox
	{
		public var editor:ComboBox;
		public var olddata:String;
		
		public function ItemRendererTheme2()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function init(e:FlexEvent):void{
			editor.addEventListener(ListEvent.CHANGE,changeTheme);
		}

		public function changeTheme(e:ListEvent):void{
			var item:Object=new Object();
			
			data.THEME=editor.selectedItem.THEME;
			data.IDTHEME_PRODUIT=editor.selectedItem.IDTHEME_PRODUIT;
			
			item.IDPRODUIT_CATALOGUE=data.IDPRODUIT_CATALOGUE;
			item.IDTHEME_PRODUIT=data.IDTHEME_PRODUIT;
			item.THEME= data.THEME;
			item.BOOL_DRT = data.BOOL_DRT;
			item.PLAN_CAP = data.PLAN_MCAP;
			item.PLAN_TYPE = data.PLAN_TYPE;
			
			dispatchEvent(new ChangeData_EVENT(ChangeData_EVENT.ChangeData_EVENT,item,true,false));
		}

		override public function set data(value:Object):void{
			super.data=value;
			
			if(!DataSample.TabTheOk){
				var tmp:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"getListeTheme",
					ThemeOk,ThemeNo);
				RemoteObjectUtil.callService(tmp);
			}

  			if(value!=null && DataSample.TabTheme != null){
				for(var i:int=0;i<editor.dataProvider.length;i++){
					if(editor.dataProvider[i].THEME == data.THEME){
						editor.selectedIndex=i;
						break;
					}
				}
			}
  		}

		public function ThemeOk(e:ResultEvent):void{
			var item:Object=new Object();
			item.IDTHEME_PRODUIT=0;
			item.THEME="** Tous **";
			
			DataSample.TabTheme=e.result as ArrayCollection;
			DataSample.TabTheme.addItemAt(item,0);
			DataSample.TabTheOk=true;
		}

		public function ThemeNo(e:FaultEvent):void{
			trace("ItemRendererTheme2 - set data() : "+e.message);
			DataSample.TabTheOk=false;
		}
		
	}
}