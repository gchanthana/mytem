interface 
{
	public boolean function manageResult(required struct result);	 
	public string function getDefinition();	
	public struct function getResult();
	public void function purgeTemporyItems();
	 
}