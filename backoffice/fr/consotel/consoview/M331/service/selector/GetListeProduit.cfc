<cfcomponent output="false">
	
	<cffunction access="remote" output="true" name="getListeProduits" returntype="query">
			
             <cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
			 <cfset idOperator = 0>
			 <cfset idTheme = ''>
			 <cfset typeTheme =''>
			 <cfset idLang  = SESSION.USER.IDGLOBALIZATION>
	
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_liste_produit_theme">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOperator#" variable="p_operateurid">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idTheme#" variable="p_idtheme_produit" null="true">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#typeTheme#" variable="p_type_theme">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#" variable="p_langid">
				<cfprocresult name="qListeProduitOpertaorSFR">
			</cfstoredproc>				
            <cfreturn qListeProduitOpertaorSFR><!--- le resultat--->
      </cffunction> 
</cfcomponent>