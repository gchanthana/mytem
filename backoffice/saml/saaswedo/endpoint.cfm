<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">  
  <meta name="description" content="Mytem360">
  <meta name="author" content="saaswedo">
  <title>Mytem360</title>
</head>
  <body>
<cftry>

<cfparam name="cryptedQS" type="string" >
<cfparam name="_transactionid" type="string" >
<cfparam name="_sp" type="string" >


<cfset dtGMT = GetHTTPTimeString( DateAdd("n", 2, Now()) ) />
<cfset short_dtGMT = GetHTTPTimeString( DateAdd("s", 15, Now()) ) />

<cfheader 	name="Set-Cookie"
          value="cryptedQS=#cryptedQS#;  Expires=#dtGMT#; Path=/; domain=mytem360.com; HttpOnly">
<cfheader 	name="Set-Cookie"
          value="_transactionid=#_transactionid#;  Expires=#dtGMT#; Path=/; domain=mytem360.com; HttpOnly">
<cfheader   name="Set-Cookie"
          value="_sp=#_sp#;  Expires=#short_dtGMT#; Path=/; domain=mytem360.com;">


<cfset algo = 'AES'>
<cfset encoding = 'HEX'>
<cflog text="SSO IDP : What #cryptedQS#">
<cflog text="SSO IDP : What #_transactionid#">

<cfset response = DeserializeJSON(decrypt(cryptedQS,urldecode(_transactionid),algo, encoding))>

<cfif !StructKeyExists(response,"valid") or !(CompareNoCase(trim(response['valid']),'YES') eq 0) >
  <cfheader name="Set-Cookie"
            value="cryptedQS=#cryptedQS#;  Expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=/; domain=mytem360.com; HttpOnly">
  <cfheader name="Set-Cookie"
            value="_transactionid=#_transactionid#;  Expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=/; domain=mytem360.com; HttpOnly">
  <cfheader name="Set-Cookie"
            value="_sp=#_sp#;  Expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=/; domain=mytem360.com;">

  <cfset showError("Vous n'êtes pas autorisé à accéder à cette page ! ","")>  
  <cflog text = "Echec de connexion reponse non valide (#response#)"  file = "saml-saaswedo" type = "INFO">
</cfif>

<cfset spUser = response.user>
<cfset spUser.racineID = response.RacineID>
<!--- SAASWEDO  --->

<!--- CHECK IF USER EXISTS --->
<cfset mytemUser = {}>
<cfset mytemUser = CheckMytemUser(trim(spUser.uid))>

<cfset goToMytem360()>


<cfcatch TYPE="ANY">
    <cfmail from="samuel@divioka.fr" subject="[SAML2 Debug]"  type="text/html"
          to="samuel.divioka@saaswedo.com"
          server="mail-cv.consotel.fr">
    <cfdump var="#cfcatch#">
  </cfmail>

    <cfset showError("Vous n'êtes pas autorisé à accéder à cette page ! ","")>
  
  </cfcatch>
  </cftry>
  </body>
</html>

<cffunction name = "goToMytem360" returnType = "void"  access = "private" description = "redirection" output="true">
  <cfheader
        statuscode="302"
        />
  <cfheader
    name="location"
    value="https://dev.mytem360.com/?cc=6"
  />
  <cfabort>
</cffunction>

<cffunction name = "showError" returnType = "void"  access = "private" description = "show error page" output="true">
  <cfargument name="message" type="string" required="false" default="">
  <cfargument name="redMessage" type="string" required="false" default="">
  <cfoutput>
    <p style="color:##000000;"><strong>#message#</strong></p>   
    <p style="color:##FF0000;"><strong>#redMessage#</strong></p>
  </cfoutput>
  <cfabort>
</cffunction>


<cffunction name = "CheckMytemUser" returnType = "any"  access = "public" >
  <cfargument name="uid" type="string" required="true">

    <cfset LOCAL = {}>
    <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.getuserpasword">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#arguments.uid#" >
		<cfprocresult name="mytemUser">
	</cfstoredproc>

	<cfif mytemUser.recordcount eq 0>
		<cflog text="SSO IDP : invalid user (#arguments.uid#) satus redirect to idp login (exit 001)">
		<cfset showError("Vous n'êtes pas autorisé à accéder à cette page ! ","")>  
	</cfif>

    <cfset LOCAL.mail = mytemUser['LOGIN_EMAIL'][1] >

    <cfreturn LOCAL.mail>
</cffunction>



