package univers.parametres.gestiondesdroitsdefacturation.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.Panel;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestiondesdroitsdefacturation.event.profilEvent;
	import univers.parametres.gestiondesdroitsdefacturation.system.GestionDroitClass;
	
	public class GestionProfil extends Panel
	{
		public var cboProfil:ComboBox;
		public var cboRacine:ComboBox;
		public var btnValider:Button;
		public var lblProfil:Label;
		public var fiProfil:FormItem;
		public var tiProfil:TextInput;
		public var adgAfficheProfil:AdvancedDataGrid;
		
		[Bindable]
		public var origine:String;
		[Bindable]
		public var tabProfil:ArrayCollection=new ArrayCollection();
		[Bindable]
		public var tabRacine:ArrayCollection=new ArrayCollection();
		[Bindable]
		public var tabProf:ArrayCollection=new ArrayCollection();
		
		private var gestionDroit:GestionDroitClass= new GestionDroitClass();
		private var o:Object= new Object();
		private var edit:Boolean=false;
		private var str:String="";
		public var titre:String= ResourceManager.getInstance().getString('M21', 'Profil_');
		
		public function GestionProfil()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,create);
		}

/** --------------------------------------     gestion des listeners     -------------------------------------------**/
	
		protected function create(e:FlexEvent):void{
					
			cboProfil.addEventListener(ListEvent.CHANGE,cboProfilDataChangeHandler);
			
			initObject();
		}

		protected function dispCreerClickHandler(e:MouseEvent):void{
			initObject();
			o.EDITABLE = true;
			
			compoVisible(true,false,true,true,true,true);
			compoEnable(true,true);

			tabProf = new ArrayCollection();			
			tabProf.addItem(o);
			tabProf.refresh();
			
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			
			tiProfil.text="";
			str="CREER";
			this.title = ResourceManager.getInstance().getString('M21', 'Profil___')+str
		}
		
		protected function dispSupprimerClickHandler(e:MouseEvent):void{
			
			if(origine=="intranet"){
				compoVisible(true,false,false,false,false,false);
			}else{
				compoVisible(true,true,false,false,false,false);
				listeProfil();
			}
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			
			tiProfil.text="";
			str="SUPPRIMER";
			this.title = ResourceManager.getInstance().getString('M21', 'Profil___')+str
		}
		
		protected function dispMajClickHandler(e:MouseEvent):void{
			if(origine == "intranet"){
				compoVisible(true,false,false,false,false,false);
			}else{
				compoVisible(true,true,false,false,false,false);
				listeProfil();
			}
			
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			
			tiProfil.text="";
			str="MAJ";
			this.title = ResourceManager.getInstance().getString('M21', 'Profil___')+str
		}
		
		protected function dispAfficherClickHandler(e:MouseEvent):void{
			if(origine=="intranet"){
			compoVisible(true,false,false,false,false,false);
			}else{
				compoVisible(true,true,false,false,false,false);
				listeProfil();
			}
			
			resetErrorString();
			
			cboProfil.selectedIndex=-1;
			
			tiProfil.text="";
			str="AFFICHER";
			this.title = ResourceManager.getInstance().getString('M21', 'Profil___')+str
		}
		
		protected function btnValiderClickHandler(e:MouseEvent):void{
			switch(str){			
				case "CREER" : 
					creerBtnValider();
					break;
				
				case "SUPPRIMER" : 
					supprimerBtnValider();							
					break;
					
				case "MAJ" :
					majBtnValider();
					break;
					
				case "AFFICHER" :
					
					break;
			}
		}
		
		protected function cboProfilDataChangeHandler(e:ListEvent):void{
			if(cboProfil.selectedIndex != -1){
				gestionDroit.getProfil(opeGetProfilOk,parseInt(cboProfil.selectedItem.IDPROFIL));
			}			
			
			switch(str){
				case "SUPPRIMER":
					compoVisible(true,true,true,true,true,true);
					compoEnable(false,false);
					break;
				
				case "MAJ":
					compoVisible(true,true,true,true,true,true);
					compoEnable(true,true);
					break;
					
				case "AFFICHER":
					compoVisible(true,true,true,true,false,true);
					compoEnable(false,false);
					break;
			}
			
			tiProfil.text=cboProfil.selectedItem.NAME;
		}
		
		private function opeGetProfilOk(e:ResultEvent):void{
			tabProf = new ArrayCollection();
			initObject();
			var t:ArrayCollection= e.result as ArrayCollection;
			for(var i:int=0;i<t.length;i++){
				switch(t[i].BOUTON_CODE.toString()){
					case "B1" :
						o.obj1 = 1;
						o.obj1BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
						
					case "B2" :
						o.obj2 = 1;
						o.obj2BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
						
					case "B3" :
						o.obj3 = 1;
						o.obj3BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B4" :
						o.obj4 = 1;
						o.obj4BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B5" :
						o.obj5 = 1;
						o.obj5BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
						
					case "B6" :
						o.obj6 = 1;
						o.obj6BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B7" :
						o.obj7 = 1;
						o.obj7BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B8" :
						o.obj8 = 1;
						o.obj8BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B9" :
						o.obj9 = 1;
						o.obj9BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B10" :
						o.obj10 = 1;
						o.obj10BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					case "B11" :
						o.obj11 = 1;
						o.obj11BOUTON_CODE=t[i].BOUTON_CODE.toString();
						break;
					
					default :
						break;
				}
			}
			o.EDITABLE = edit;
			tabProf.addItem(o);
			tabProf.refresh();
		}

/** --------------------------------------     fonction     -------------------------------------------**/

// CREATION 
		private function creerBtnValider():void{
			var boolExiste:Boolean = false;
			if(tiProfil.text != null && tiProfil.text != ""){
				for(var i:int=0;i<tabProfil.length;i++){
					if(tiProfil.text == tabProfil[i].NAME){
						boolExiste=true;
						break;
					}
				}
				
				if(boolExiste){
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Le_profil_que_vous_souhaitez_cr_er_exist'),ResourceManager.getInstance().getString('M21', 'Profil_existant'),null);
					tiProfil.errorString=ResourceManager.getInstance().getString('M21', 'Le_profil_existe_');
				}else{
					var s:String= setTab();
					if(origine== "intranet"){
						gestionDroit.creerProfil(creerProfilYes,tiProfil.text,s,cboRacine.selectedItem.IDGROUPE_CLIENT)
					}else{
						gestionDroit.creerProfil(creerProfilYes,tiProfil.text,s,-1)
					}

				}
			}else{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Veuillez_saisir_un_nom_de_profil'),ResourceManager.getInstance().getString('M21', 'Erreur_de_saisie'),null);	
			}
		}

		private function creerProfilYes(e:ResultEvent):void{
			resetErrorString();
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Profil_cr___avec_r_ussite'),this);
			dispatchEvent(new profilEvent(profilEvent.PROFIL_EVENT,true,false));
			trace('Création du profil réussie');
		}

// SUPPRESSION
		private function supprimerBtnValider():void{
			if(cboProfil.selectedIndex > -1){
/* 				for(var i:int=0;i<tabProfil.length */
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M21', 'Etes_vous_sur_e__de_vouloir_supprimer_ce'),ResourceManager.getInstance().getString('M21', 'Confirmation_de_suppression'),supprimerBtnValiderCloseEvent);
			}else{
				cboProfil.errorString=ResourceManager.getInstance().getString('M21', 'Veuillez_s_lectionner_le_profil___suppri');
			}
		}

		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				gestionDroit.supprimerProfil(supprimerBtnValiderYes,parseInt(cboProfil.selectedItem.IDPROFIL));
			}
		}
		
		private function supprimerBtnValiderYes(e:ResultEvent):void{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Le_profil__')+cboProfil.selectedItem.NAME+ResourceManager.getInstance().getString('M21', '__a__t__supprim_'),this);
			listeProfil();
			resetErrorString();
			compoVisible(true,true,false,false,true,false);
			dispatchEvent(new profilEvent(profilEvent.PROFIL_EVENT,true,false));
		}

// MISE A JOUR 
		private function majBtnValider():void{
			if(cboProfil.selectedIndex > -1){
				if(tiProfil.text != ""){
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M21', 'Etes_vous_sur_e__de_vouloir_modifier_ce_'),ResourceManager.getInstance().getString('M21', 'Confirmation_de_modification'),majBtnValiderCloseEvent);
				}else{
					tiProfil.errorString=ResourceManager.getInstance().getString('M21', 'Veuillez_saisir_un_nom_de_profil');
				}
			}else{
				cboProfil.errorString=ResourceManager.getInstance().getString('M21', 'Veuillez_s_lectionner_le_profil___modifi');
			}
		}
		
		private function majBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.YES || e.detail == Alert.OK){
				var s:String = setTab();;
				if(origine=="intranet"){
					gestionDroit.majProfil(majBtnValiderYes,parseInt(cboProfil.selectedItem.IDPROFIL),tiProfil.text,s,parseInt(cboRacine.selectedItem.IDGROUPE_CLIENT));
				}else{
					gestionDroit.majProfil(majBtnValiderYes,parseInt(cboProfil.selectedItem.IDPROFIL),tiProfil.text,s,-1);
				}
			}
		}			
		
		private function majBtnValiderYes(e:ResultEvent):void{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Le_profil__')+cboProfil.selectedItem.NAME+ResourceManager.getInstance().getString('M21', '__a__t__modifi_'),this);
			listeProfil();
			resetErrorString();
			compoVisible(true,true,true,true,true,true);
			dispatchEvent(new profilEvent(profilEvent.PROFIL_EVENT,true,false));
		}

// FONCTION : liste les profils
		private function listeProfil():void{
			if(origine=="intranet"){
				gestionDroit.getListeProfil(opeProfilOk,parseInt(cboRacine.selectedItem.IDGROUPE_CLIENT));
			}else{
				gestionDroit.getListeProfil(opeProfilOk,-1);
			}
		}
		
		private function opeProfilOk(e:ResultEvent):void{
			tabProfil = e.result as ArrayCollection;
			tabProfil.refresh();
			cboProfil.selectedIndex=-1;
		}
		
		private function initObject():void{
			o.obj1 = 0;
			o.obj2 = 0;
			o.obj3 = 0;
			o.obj4 = 0;
			o.obj5 = 0;
			o.obj6 = 0;
			o.obj7 = 0;
			o.obj8 = 0;
			o.obj9 = 0;
			o.obj10 = 0;
			o.obj11 = 0;
			o.obj1BOUTON_CODE="B1";
			o.obj2BOUTON_CODE="B2";
			o.obj3BOUTON_CODE="B3";
			o.obj4BOUTON_CODE="B4";
			o.obj5BOUTON_CODE="B5";
			o.obj6BOUTON_CODE="B6";
			o.obj7BOUTON_CODE="B7";
			o.obj8BOUTON_CODE="B8";
			o.obj9BOUTON_CODE="B9";
			o.obj10BOUTON_CODE="B10";
			o.obj11BOUTON_CODE="B11";
			o.EDITABLE = edit;
		}

		public function setTab():String{
			var s:String="";
			var o:Object=adgAfficheProfil.selectedItem as Object;
			
			if(o != null){
				if(o.obj1 == 1)
					s+=","+o.obj1BOUTON_CODE;
				if(o.obj2 == 1)
					s+=","+o.obj2BOUTON_CODE;
				if(o.obj3 == 1)
					s+=","+o.obj3BOUTON_CODE;
				if(o.obj4 == 1)
					s+=","+o.obj4BOUTON_CODE;
				if(o.obj5 == 1)
					s+=","+o.obj5BOUTON_CODE;
				if(o.obj6 == 1)
					s+=","+o.obj6BOUTON_CODE;
				if(o.obj7 == 1)
					s+=","+o.obj7BOUTON_CODE;
				if(o.obj8 == 1)
					s+=","+o.obj8BOUTON_CODE;
				if(o.obj9 == 1)
					s+=","+o.obj9BOUTON_CODE;
				if(o.obj10 == 1)
					s+=","+o.obj10BOUTON_CODE;
				if(o.obj11 == 1)
					s+=","+o.obj11BOUTON_CODE;
				s=s.substr(1,s.length-1);
			}
			return s;
		}
	
		private function compoEnable(ti:Boolean,adg:Boolean):void{
			fiProfil.enabled=ti;
			adgAfficheProfil.enabled=adg;
			adgAfficheProfil.editable=adg.toString();
			edit= adg;
		}
		
		private function compoVisible(cbolisteracine:Boolean,cbolisteprofil:Boolean,lbl:Boolean,ti:Boolean,btnvalider:Boolean,adg:Boolean):void{
			cboProfil.visible=cbolisteprofil;
			lblProfil.visible=lbl;
			fiProfil.visible=ti;
			fiProfil.required=ti;
			btnValider.visible=btnvalider;
			adgAfficheProfil.visible=adg;
		}	
		
		private function resetErrorString():void{
			cboProfil.errorString="";
			tiProfil.errorString="";
			adgAfficheProfil.errorString="";
		}	
	}
}