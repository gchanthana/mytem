package gestionparc.ihm.fiches.ficheEntity
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TabBar;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.Validator;
	
	import composants.popuputils.ConfirmerOrAnnulerMsgIHM;
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import gestionparc.entity.LigneVO;
	import gestionparc.entity.OperateurVO;
	import gestionparc.entity.OrgaVO;
	import gestionparc.entity.RevendeurVO;
	import gestionparc.entity.SIMVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCablageSdaTypePointaPointIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCablageSdaTypeT2T0AnaIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCablageSdaTypeXdslIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCanauxGroupmentAjoutLigneIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCaracteristiqueFixeIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentContratOperateurLigneIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentDiversIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentTabNavSiteIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.TabModelIHM;
	import gestionparc.ihm.fiches.popup.PopUpAboOptIHM;
	import gestionparc.ihm.fiches.popup.PopUpUsageIHM;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.ligne.LigneServices;
	import gestionparc.services.organisation.OrganisationServices;
	import gestionparc.services.site.SiteServices;
	import gestionparc.utils.gestionparcConstantes;
	import gestionparc.utils.gestionparcMessages;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de la fiche ligne.
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 1.09.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class FicheAjoutLigneImpl extends TitleWindowBounds
	{
		// VARIABLES------------------------------------------------------------------------------

		// IHM components
		public var tiSousTete:TextInput;
		public var tiNumContrat:TextInput;

		public var lbPosition:Label;
		public var lbAboPrincipal:Label;
		public var lbOperateur:Label;
		public var lbOperateurPreselection:Label;
		public var lbCompte:Label;
		public var lbSousCompte:Label;

		public var ckxPoolDistributeur:CheckBox;
		public var ckxPoolSociete:CheckBox;

		public var taCommentaires:TextArea;

		public var valSousTete:Validator;
		public var valNumContrat:Validator;
		public var valOperateur:Validator;
		public var valCompte:Validator;
		public var valSousCompte:Validator;
		public var valTypeCmd:Validator;
		public var valDistributeur:Validator;
		public var valAcces:Validator;

		public var cbFonction:ComboBox;
		public var cbTitulaire:ComboBox;
		public var cbPayeur:ComboBox;
		public var cbOrganisation:ComboBox;
		public var cbOperateur:ComboBox;
		public var cbOperateurPreselection:ComboBox;
		public var cbCompte:ComboBox;
		public var cbSousCompte:ComboBox;
		public var cbTypeCmd:ComboBox;
		public var cbDistributeur:ComboBox;
		public var cbTypeLigne:ComboBox;

		public var tnLigne:ViewStack;
		public var tnLigneTabBar:TabBar;
		public var tncSite:ContentTabNavSiteIHM;
		public var tncDivers:ContentDiversIHM;
		public var tncRaccordement:TabModelIHM;
		public var tncAboOption:TabModelIHM;
		public var tncContratOperateur:ContentContratOperateurLigneIHM;
		public var tncCablageSdaPointaPoint:ContentCablageSdaTypePointaPointIHM;
		public var tncCablageSdaT2T0Ana:ContentCablageSdaTypeT2T0AnaIHM;
		public var tncCablageSdaXdsl:ContentCablageSdaTypeXdslIHM;
		public var tncCanauxGroupment:ContentCanauxGroupmentAjoutLigneIHM;
		public var tncCaracteristique:ContentCaracteristiqueFixeIHM;

		public var fiTypeLigne:FormItem;
		public var fiOperateur:FormItem;
		public var fiTypeCmd:FormItem;
		public var fiDistributeur:FormItem;
		public var fiOperateurPreselection:FormItem;

		public var btValid:Button;
		public var btCancel:Button;
		public var btModifierOrga:Button;

		public var imgEditFonction:Image;

		/*		public var rbgLigne					:RadioButtonGroup;
				public var rbLigneMobile			:RadioButton;
				public var rbLigneFixe				:RadioButton;*/

		public var colonnesAboOptions:ArrayCollection;
		public var colonnesRaccordement:ArrayCollection;
		public var listeOuiNon:ArrayCollection;
		public var listeEtat:ArrayCollection;

		// publics variables 
		public var idsoustete:Number=-1;

		// privates variables
		private var myServiceLigne:LigneServices=null;
		private var myServiceOrga:OrganisationServices=null;
		private var myServicesSite:SiteServices=null;

		private var ficheOrga:FicheOrgaIHM=null;
		private var myDataProviderAboOpt:ArrayCollection=null;

		private var indexTypeCmd:int=0;
		private var indexOperateur:int=0;

		private var strOldUsage:String="";

		private var pin1:String="";
		private var pin2:String="";
		private var puk1:String="";
		private var puk2:String="";

		private var boolSite:Boolean=false;
		private var boolContrat:Boolean=false;
		private var boolAbo:Boolean=false;
		private var boolDivers:Boolean=false;
		private var boolT2:Boolean=false;
		private var boolXdsl:Boolean=false;
		private var boolPoint:Boolean=false;
		private var boolRacco:Boolean=false;
		private var boolCanaux:Boolean=false;
		private var boolCaracteristique:Boolean=false;

		private var popupAboOpt:PopUpAboOptIHM;

		[Embed(source='/assets/images/arrow_right_green.png')]
		private var myIconProduitAcces:Class;

		public function FicheAjoutLigneImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
			{
				return "";
			}
		}

		private function init(event:FlexEvent):void
		{
			myServiceLigne=new LigneServices();
			myServicesSite=new SiteServices();
			myServiceOrga=new OrganisationServices();

			cbTypeLigne.selectedIndex=0;
			cbTypeLigne_changeHandler(null);
			initListener();
			initDisplay();
			initData();
		}

		private function initData():void
		{
			if (CacheDatas.listeTypesLigne != null)
			{
				CacheDatas.listeTypesLigne.refresh();
				cbTypeLigne.dataProvider=CacheDatas.listeTypesLigne;
				cbTypeLigne.dropdown.dataProvider=CacheDatas.listeTypesLigne;
				cbTypeLigne.selectedItem=selectedItemTypeLigne;
			}
		}
		private var selectedItemTypeLigne:Object;


		private function tncSiteComplete(evt:FlexEvent):void
		{
			boolSite=true;
			initIHM()
		}

		private function tncCaracteristiqueComplete(evt:FlexEvent):void
		{
			boolCaracteristique=true;
			initIHM()
		}

		private function tncAboComplete(evt:FlexEvent):void
		{
			boolAbo=true;
			initIHM()
		}

		private function tncContratComplete(evt:FlexEvent):void
		{
			boolContrat=true;
			initIHM()
		}

		private function tncDiverComplete(evt:FlexEvent):void
		{
			boolDivers=true;
			initIHM()
		}

		private function tncRaccoComplete(evt:FlexEvent):void
		{
			boolRacco=true;
			initIHM()
		}

		private function tncT2Complete(evt:FlexEvent):void
		{
			boolT2=true;
			initIHM()
		}

		private function tncXdslComplete(evt:FlexEvent):void
		{
			boolXdsl=true;
			initIHM();
		}

		private function tncPointComplete(evt:FlexEvent):void
		{
			boolPoint=true;
			initIHM()
		}

		private function tncCanauxComplete(evt:FlexEvent):void
		{
			boolCanaux=true;
			initIHM()
		}

		private function initIHM():void
		{
			if (boolAbo && boolCanaux && boolContrat && boolDivers && (boolPoint || boolT2) && boolRacco /*&& boolSite*/ && boolCaracteristique)
			{
				boolSite=false;
				boolAbo=false;
				boolCanaux=false;
				boolContrat=false;
				boolDivers=false;
				boolPoint=false;
				boolRacco=false;
				boolT2=false;
				boolCaracteristique=false;

				initListenerOnglet();
				initDisplayOnglet();
			}
		}

		private function createOngletSite():ContentTabNavSiteIHM
		{
			if (tncSite == null)
			{
				tncSite=new ContentTabNavSiteIHM();
				tncSite.addEventListener(FlexEvent.CREATION_COMPLETE, tncSiteComplete);
				tncSite.label=ResourceManager.getInstance().getString('M111', 'Site');
				tncSite.height=250;
				tncSite.percentWidth=100;
				tncSite.toolTip=ResourceManager.getInstance().getString('M111', 'Une_carte_sim_est_necessaire_pour_pouvoir_saisir_un_site')
			}
			return tncSite;
		}

		private function createOngletAbo():TabModelIHM
		{
			if (tncAboOption == null)
			{
				tncAboOption=new TabModelIHM();
				tncAboOption.addEventListener(FlexEvent.CREATION_COMPLETE, tncAboComplete);
				tncAboOption.label=resourceManager.getString('M111', 'Abos_et_options');
				tncAboOption.labelInventaire=resourceManager.getString('M111', 'Dans_l_inventaire')
				tncAboOption.labelHistorique=resourceManager.getString('M111', 'Historique')
				tncAboOption.typeAjoutColonnes="none"
				tncAboOption.colonnes=colonnesAboOptions
				tncAboOption.datagridHeight=180;
				tncAboOption.height=250;
				tncAboOption.percentWidth=100;
				tncAboOption.buttonActionVisible=false
			}
			return tncAboOption;
		}

		private function createOngletContrat():ContentContratOperateurLigneIHM
		{
			if (tncContratOperateur == null)
			{
				tncContratOperateur=new ContentContratOperateurLigneIHM();
				tncContratOperateur.addEventListener(FlexEvent.CREATION_COMPLETE, tncContratComplete);
				tncContratOperateur.height=250;
				tncContratOperateur.label=resourceManager.getString('M111', 'Contrat_op_rateur')
			}
			return tncContratOperateur;
		}

		private function createOngletDivers():ContentDiversIHM
		{
			if (tncRaccordement == null)
			{
				tncDivers=new ContentDiversIHM();
				tncDivers.addEventListener(FlexEvent.CREATION_COMPLETE, tncDiverComplete);
				tncDivers.height=250;
				tncDivers.label=resourceManager.getString('M111', 'Divers')
			}
			return tncDivers;
		}

		private function createOngletRaccordement():TabModelIHM
		{
			if (tncRaccordement == null)
			{
				tncRaccordement=new TabModelIHM();
				tncRaccordement.addEventListener(FlexEvent.CREATION_COMPLETE, tncRaccoComplete);
				tncRaccordement.label=resourceManager.getString('M111', 'Raccordement')
				tncRaccordement.typeAjoutColonnes="none"
				tncRaccordement.colonnes=colonnesRaccordement
				tncRaccordement.datagridHeight=180
				tncRaccordement..height=250;
				tncRaccordement.percentWidth=100
				tncRaccordement.buttonActionVisible=true
				tncRaccordement.buttonActionLabel=resourceManager.getString('M111', 'Changer_le_raccordement')
			}
			return tncRaccordement;
		}

		private function createOngletCablageSdaT2():ContentCablageSdaTypeT2T0AnaIHM
		{
			if (tncCablageSdaT2T0Ana == null)
			{
				tncCablageSdaT2T0Ana=new ContentCablageSdaTypeT2T0AnaIHM();
				tncCablageSdaT2T0Ana.addEventListener(FlexEvent.CREATION_COMPLETE, tncT2Complete);
				tncCablageSdaT2T0Ana.label=resourceManager.getString('M111', 'C_blage_SDA')
				tncCablageSdaT2T0Ana..height=250;
				tncCablageSdaT2T0Ana.percentWidth=100;
				tncCablageSdaT2T0Ana.myServiceLigne=this.myServiceLigne;
			}
			return tncCablageSdaT2T0Ana;
		}

		private function createOngletCablageSdaXdsl():ContentCablageSdaTypeXdslIHM
		{
			if (tncCablageSdaXdsl == null)
			{
				tncCablageSdaXdsl=new ContentCablageSdaTypeXdslIHM();
				tncCablageSdaXdsl.addEventListener(FlexEvent.CREATION_COMPLETE, tncXdslComplete);
				tncCablageSdaXdsl.label=resourceManager.getString('M111', 'C_blage_SDA');
				tncCablageSdaXdsl.height=250;
				tncCablageSdaXdsl.percentWidth=100;
				tncCablageSdaXdsl.myServiceLigne=this.myServiceLigne;
			}
			return tncCablageSdaXdsl;
		}

		private function createOngletCablageSdaPointAPoint():ContentCablageSdaTypePointaPointIHM
		{
			if (tncCablageSdaPointaPoint == null)
			{
				tncCablageSdaPointaPoint=new ContentCablageSdaTypePointaPointIHM();
				tncCablageSdaPointaPoint.addEventListener(FlexEvent.CREATION_COMPLETE, tncPointComplete);
				tncCablageSdaPointaPoint.label=resourceManager.getString('M111', 'C_blage_SDA')
				tncCablageSdaPointaPoint.height=250;
				tncCablageSdaPointaPoint.percentWidth=100;
				tncCablageSdaPointaPoint.myServiceLigne=this.myServiceLigne;
			}
			return tncCablageSdaPointaPoint;
		}

		private function createOngletCaracteristique():ContentCaracteristiqueFixeIHM
		{
			if (tncCaracteristique == null)
			{
				tncCaracteristique=new ContentCaracteristiqueFixeIHM();
				tncCaracteristique.addEventListener(FlexEvent.CREATION_COMPLETE, tncCaracteristiqueComplete);
				tncCaracteristique.label=resourceManager.getString('M111', 'onglet_Caract_ristique')
				tncCaracteristique.height=250;
				tncCaracteristique.percentWidth=100;
				tncCaracteristique.myLigneService=this.myServiceLigne;
			}
			return tncCaracteristique;
		}

		private function createOngletGroupement():ContentCanauxGroupmentAjoutLigneIHM
		{
			if (tncCanauxGroupment == null)
			{
				tncCanauxGroupment=new ContentCanauxGroupmentAjoutLigneIHM();
				tncCanauxGroupment.addEventListener(FlexEvent.CREATION_COMPLETE, tncCanauxComplete);
				tncCanauxGroupment.myServiceLigne=this.myServiceLigne;
				tncCanauxGroupment.label=resourceManager.getString('M111', 'Canaux_Groupement')
				tncCanauxGroupment.height=250;
				tncCanauxGroupment.percentWidth=100;
				tncCanauxGroupment.myServiceLigne=this.myServiceLigne;
			}
			return tncCanauxGroupment;
		}

		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);
			imgEditFonction.addEventListener(MouseEvent.CLICK, imgEditFonctionClickHandler);
//			tiNumContrat.addEventListener(Event.CHANGE,activeValidButton);
			tiSousTete.addEventListener(Event.CHANGE, tiSousTeteChangeHandler);
//			taCommentaires.addEventListener(Event.CHANGE,activeValidButton);
//			cbFonction.addEventListener(ListEvent.CHANGE,activeValidButton);
//			cbOrganisation.addEventListener(ListEvent.CHANGE,activeValidButton);
//			cbPayeur.addEventListener(ListEvent.CHANGE,activeValidButton);
//			cbTitulaire.addEventListener(ListEvent.CHANGE,activeValidButton);
			cbOrganisation.addEventListener(ListEvent.CHANGE, changeOrgaHandler);
			btModifierOrga.addEventListener(MouseEvent.CLICK, clickModifierOrgaHandler);
			cbTypeCmd.addEventListener(ListEvent.CHANGE, cbTypeCmdListEventChange);
			cbOperateur.addEventListener(ListEvent.CHANGE, cbOperateurListEventChange);
			cbCompte.addEventListener(Event.CHANGE, cbCompteChangeHandler);
		}

		private function tiSousTeteChangeHandler(evt:Event):void
		{
			if (tncCanauxGroupment && boolCanaux)
			{
				refreshInfoLigne(null);
				tncCanauxGroupment.changeSousTete();
			}
		}

		private function initListenerOnglet():void
		{
//			tncDivers.ti_chp1.addEventListener(Event.CHANGE,activeValidButton);
//			tncDivers.ti_chp2.addEventListener(Event.CHANGE,activeValidButton);
//			tncDivers.ti_chp3.addEventListener(Event.CHANGE,activeValidButton);
//			tncDivers.ti_chp4.addEventListener(Event.CHANGE,activeValidButton);
//			tncAboOption.dataDatagrid.addEventListener(CollectionEvent.COLLECTION_CHANGE,activeValidButton);
//			tncContratOperateur.cbDureeContrat.addEventListener(ListEvent.CHANGE,activeValidButton);
//			tncContratOperateur.cbDureeEligibilite.addEventListener(ListEvent.CHANGE,activeValidButton);
//			tncContratOperateur.dcDateOuverture.addEventListener(CalendarLayoutChangeEvent.CHANGE,activeValidButton);
//			tncContratOperateur.dcDateRenouvellement.addEventListener(CalendarLayoutChangeEvent.CHANGE,activeValidButton);
//			tncContratOperateur.dcDateResiliation.addEventListener(CalendarLayoutChangeEvent.CHANGE,activeValidButton);
			tncRaccordement.buttonAction.addEventListener(MouseEvent.CLICK, onTncRaccordementClickHandler);
			tncAboOption.addEventListener(gestionparcEvent.AJOUTER_ELT_NAV, tncAboOptiongestionparcEventAjouterEltNavHandler);
			CacheServices.getInstance().myDatas.addEventListener(gestionparcEvent.INFO_OP_LOADED, cacheInfoOpLoadedHandler);
		}

		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btValid.enabled=true;
			cbFonction.dataProvider=CacheDatas.listeUsages;
			this.height+=75;
			lbOperateur.visible=false;
			lbOperateur.includeInLayout=false;
			lbOperateurPreselection.visible=false;
			lbOperateurPreselection.includeInLayout=false;
			lbCompte.visible=false;
			lbCompte.includeInLayout=false;
			lbSousCompte.visible=false;
			lbSousCompte.includeInLayout=false;
			fiOperateur.required=true;
			fiTypeLigne.required=true;
			cbTitulaire.selectedIndex=1;
			cbPayeur.selectedIndex=1;
		}

		private function initDisplayOnglet():void
		{
			tncAboOption.buttonActionVisible=true;
			tncAboOption.buttonActionLabel=ResourceManager.getInstance().getString('M111', 'Abos_et_options');
			tiNumContrat.text=ResourceManager.getInstance().getString('M111', 'n_c_');
			tncDivers.fi1.label=CacheDatas.libellesPerso1;
			tncDivers.fi2.label=CacheDatas.libellesPerso2;
			tncDivers.fi3.label=CacheDatas.libellesPerso3;
			tncDivers.fi4.label=CacheDatas.libellesPerso4;
			listeOragLoadedHandler(null);
			cacheListeProfilEqptLoadedHandler(null);
			cacheListeOperateurLoadedHandler(null);
		}

		public function refreshInfoLigne(evt:FlexEvent):void
		{
			setInfoLigneVO()
		}

		private function tncAboOptiongestionparcEventAjouterEltNavHandler(event:gestionparcEvent):void
		{
			if (cbOperateur.selectedItem && cbTypeLigne.selectedItem && cbOperateur.selectedItem.OPERATEURID && cbTypeCmd.selectedIndex > -1 && cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT)
			{
				popupAboOpt=new PopUpAboOptIHM();
				popupAboOpt.addEventListener(gestionparcEvent.POPUP_ABO_OPT_VALIDATED, updateGridAboOpt);
				popupAboOpt.myServices=myServiceLigne;
				popupAboOpt.isFixe=cbTypeLigne.selectedItem.ISFIXE;
				popupAboOpt.isMobile=cbTypeLigne.selectedItem.ISMOBILE;
				popupAboOpt.idOperateur=cbOperateur.selectedItem.OPERATEURID;
				popupAboOpt.idProfilEquipement=cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT;

				if (tncAboOption.dataDatagrid.length > 0)
				{
					popupAboOpt.isAbonnement=true;
				}

				PopUpManager.addPopUp(popupAboOpt, this, true);
				PopUpManager.centerPopUp(popupAboOpt);
			}
		}

		private function updateGridAboOpt(event:gestionparcEvent):void
		{
			myDataProviderAboOpt=new ArrayCollection();
			if (popupAboOpt)
			{
				for each (var obj:* in popupAboOpt.getSelectedAboOpt())
				{
					// creation d'un attribut "libelleProduit" pour l'affichage dans le tableau
					// creation d'un attribut "operateur" pour l'affichage dans le tableau
					obj.libelleProduit=obj.LIBELLE_PRODUIT;
					obj.operateur=cbOperateur.selectedItem.NOM;
					myDataProviderAboOpt.addItem(obj);
				}
			}
			tncAboOption.dataDatagrid=myDataProviderAboOpt;
			tncAboOption.dataDatagrid.refresh();
		}

		private function listeOragLoadedHandler(event:gestionparcEvent):void
		{
			myServiceLigne.myDatas.ligne=new LigneVO();
			myServiceLigne.myDatas.ligne.listeOrganisation=CacheDatas.listeOrga;
			myServiceOrga.myDatas.listeOrga=CacheDatas.listeOrga;
			cbOrganisation.dataProvider=myServiceLigne.myDatas.ligne.listeOrganisation;
		}

		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validClickHandler(event:MouseEvent):void
		{
			// test les validators
			if (runValidators())
			{
				if(ckxPoolSociete.selected){
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'L_operation_s_effectuera_sur_tous_les_pools__'),
						ResourceManager.getInstance().getString('M111', 'Confirmation'),
						confirmationAddNewLigneHandler);
				}
				else
				{
					myServiceLigne.setInfosFicheLigne(createObjToServiceSet());
					myServiceLigne.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED, ficheLigneUploadHandler);
				}
			}
		}
		
		protected function confirmationAddNewLigneHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				myServiceLigne.setInfosFicheLigne(createObjToServiceSet());
				myServiceLigne.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED, ficheLigneUploadHandler);
			}
		}

		private function createObjToServiceSet():Object
		{
			var obj:Object=new Object();

			obj.idTypeCmd=cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT;
			setInfoLigneVO();
			obj.myLigne=myServiceLigne.myDatas.ligne;
			obj.xmlListeAboOpt=createXmlListeAboOpt();
			obj.xmlListeOrga=createXmlListeOrga();
			obj.xmlListeCablageCanaux=createXmlCablageCanaux();

			obj.insertPoolDistrib=ckxPoolDistributeur.selected ? 1 : 0;
			obj.insertPoolSociete=ckxPoolSociete.selected ? 1 : 0;

			return obj;
		}

		private function ficheLigneUploadHandler(event:gestionparcEvent):void
		{
			SpecificationVO.getInstance().refreshData();
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
//			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED));
		}

		private function setInfoLigneVO():void
		{
			myServiceLigne.myDatas.ligne.idSousTete=-1;
			myServiceLigne.myDatas.ligne.operateur=new OperateurVO();
			myServiceLigne.myDatas.ligne.operateur.idOperateur=cbOperateur.selectedItem.OPERATEURID;
			myServiceLigne.myDatas.ligne.operateurPreselect=new OperateurVO();
			myServiceLigne.myDatas.ligne.operateurPreselect.idOperateur=cbOperateurPreselection.selectedItem.OPERATEURID;
			myServiceLigne.myDatas.ligne.idCompte=(cbCompte.selectedItem) ? cbCompte.selectedItem.IDCOMPTE : -1;
			myServiceLigne.myDatas.ligne.idSousCompte=(cbSousCompte.selectedItem) ? cbSousCompte.selectedItem.IDSOUS_COMPTE : -1;
			myServiceLigne.myDatas.ligne.typeLigne=cbTypeLigne.selectedItem ? cbTypeLigne.selectedItem.LIBELLE_TYPE_LIGNE : ResourceManager.getInstance().getString('M111', 'encours_de_creation___');
			myServiceLigne.myDatas.ligne.idTypeLigne=cbTypeLigne.selectedItem ? cbTypeLigne.selectedItem.IDTYPE_LIGNE : 0;
			myServiceLigne.myDatas.ligne.fournisseur=new RevendeurVO();
			myServiceLigne.myDatas.ligne.fournisseur.idRevendeur=(cbDistributeur.selectedItem) ? cbDistributeur.selectedItem.IDREVENDEUR : -1;
			myServiceLigne.myDatas.ligne.simRattache=new SIMVO();
			myServiceLigne.myDatas.ligne.simRattache.idEquipement=-1;
			myServiceLigne.myDatas.ligne.isLignePrincipale=1;
			myServiceLigne.myDatas.ligne.isLigneBackUp=1;
			if (cbFonction.selectedItem)
			{
				if (cbFonction.selectedItem.hasOwnProperty("USAGE")) // c'est une ancienne fonction
				{
					myServiceLigne.myDatas.ligne.fonction=cbFonction.selectedItem.USAGE;
				}
				else // c'est une nouvelle fonction
				{
					myServiceLigne.myDatas.ligne.fonction=(cbFonction.selectedItem as String);
				}
			}
			myServiceLigne.myDatas.ligne.sousTete=tiSousTete.text;
			myServiceLigne.myDatas.ligne.payeur=(cbPayeur.selectedItem) ? cbPayeur.selectedItem.data : 0;
			myServiceLigne.myDatas.ligne.titulaire=(cbTitulaire.selectedItem) ? cbTitulaire.selectedItem.data : 0;
			myServiceLigne.myDatas.ligne.idSite= /*(tncSite.cbSite.selectedItem)?tncSite.cbSite.selectedItem.idSite:*/-1;
			myServiceLigne.myDatas.ligne.idEmplacement= /*(tncSite.cbEmplacement.selectedItem)?tncSite.cbEmplacement.selectedItem.idEmplcament:*/-1;
			;
			myServiceLigne.myDatas.ligne.texteChampPerso1=tncDivers.ti_chp1.text;
			myServiceLigne.myDatas.ligne.texteChampPerso2=tncDivers.ti_chp2.text;
			myServiceLigne.myDatas.ligne.texteChampPerso3=tncDivers.ti_chp3.text;
			myServiceLigne.myDatas.ligne.texteChampPerso4=tncDivers.ti_chp4.text;
			myServiceLigne.myDatas.ligne.numContrat=tiNumContrat.text;
			myServiceLigne.myDatas.ligne.commentaires=taCommentaires.text;
			myServiceLigne.myDatas.ligne.etat=1;
			myServiceLigne.myDatas.ligne.dureeContrat=(tncContratOperateur.cbDureeContrat.selectedItem) ? tncContratOperateur.cbDureeContrat.selectedItem.value : 0;
			myServiceLigne.myDatas.ligne.dateOuverture=(tncContratOperateur.dcDateOuverture.selectedDate) ? tncContratOperateur.dcDateOuverture.selectedDate : null;
			myServiceLigne.myDatas.ligne.dateRenouvellement=(tncContratOperateur.dcDateRenouvellement.selectedDate) ? tncContratOperateur.dcDateRenouvellement.selectedDate : null;
			myServiceLigne.myDatas.ligne.dateResiliation=(tncContratOperateur.dcDateResiliation.selectedDate) ? tncContratOperateur.dcDateResiliation.selectedDate : null;
			myServiceLigne.myDatas.ligne.dureeEligibilite=(tncContratOperateur.cbDureeEligibilite.selectedItem) ? tncContratOperateur.cbDureeEligibilite.selectedItem.value : 0;
			myServiceLigne.myDatas.ligne.dateFPC=(tncContratOperateur.lbDateFpc.text) ? DateField.stringToDate(tncContratOperateur.lbDateFpc.text, 'DD/MM/YYYY') : null;
		}

		private function createXmlListeOrga():String
		{
			var tmpListeOrga:ArrayCollection=new ArrayCollection();
			var lenListeOrga:int=myServiceLigne.myDatas.ligne.listeOrganisation.length;
			var myXML:String="";

			// trier la liste des orga par date de modification (pour pas changer procedure dba)
			for (var j:int=0; j < lenListeOrga; j++)
			{
				if (tmpListeOrga.length == 0)
				{
					tmpListeOrga.addItem((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO));
				}
				else
				{
					var index:int=0;
					var longueur:int=tmpListeOrga.length;

					if ((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO).dateModifPosition)
					{
						// tant la date de l'item de tmpListeOrga est plus recente que la date de l'item de listeOrganisation
						while ((index < longueur) && ((tmpListeOrga[index] as OrgaVO).dateModifPosition) && ((tmpListeOrga[index] as OrgaVO).dateModifPosition.getTime() > (myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO).dateModifPosition.getTime()))
						{
							index++;
						}
						tmpListeOrga.addItemAt((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO), index);
					}
					else
					{
						index++;
						tmpListeOrga.addItemAt((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO), index);
					}
				}
			}

			// création de l'XML
			myXML+="<Orgas>";
			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((tmpListeOrga[i] as OrgaVO).chemin != null && (tmpListeOrga[i] as OrgaVO).idCible != 0)
				{
					myXML+="<Orga>";
					myXML+="<idOrga>" + (tmpListeOrga[i] as OrgaVO).idOrga + "</idOrga>"
					myXML+="<chemin>" + (tmpListeOrga[i] as OrgaVO).chemin + "</chemin>";
					myXML+="<idCible>" + (tmpListeOrga[i] as OrgaVO).idCible + "</idCible>";
					myXML+="</Orga>";
				}
			}
			myXML+="</Orgas>";

			if (myXML != "<Orgas></Orgas>")
			{
				return myXML;
			}
			else
			{
				return "";
			}
		}

		private function createXmlListeAboOpt():String
		{
			var myXML:String="";

			var lenDataGridAboOpt:int=tncAboOption.dataDatagrid.length;

			myXML+="<aboOption>";
			// boucle pour l'abonnement. Une ligne ne peut avoir qu'un abonnement.
			for (var i:int=0; i < lenDataGridAboOpt; i++)
			{
				if (tncAboOption.dataDatagrid[i].hasOwnProperty("ISABO") && tncAboOption.dataDatagrid[i].ISABO)
				{
					myXML+="<abo><idProduitCatalogue>" + tncAboOption.dataDatagrid[i].IDPRODUIT_CATALOGUE + "</idProduitCatalogue></abo>";
					break;
				}
			}
			// boucle pour les options
			myXML+="<options>";
			for (var j:int=0; j < lenDataGridAboOpt; j++)
			{
				if (!tncAboOption.dataDatagrid[j].hasOwnProperty("ISABO") || !tncAboOption.dataDatagrid[j].ISABO)
				{
					myXML+="<option><idProduitCatalogue>" + tncAboOption.dataDatagrid[j].IDPRODUIT_CATALOGUE + "</idProduitCatalogue></option>";
				}
			}
			myXML+="</options></aboOption>";

			return myXML;
		}

		private function createXmlCablageCanaux():String
		{
			var myXML:String="<element>";
			if (tncCaracteristique && tncCaracteristique.tiDescendantContrat != null)
			{
				myXML+="<caracteristique>";
				myXML+="<nom_reseau>" + tncCaracteristique.tiNomReseau.text + "</nom_reseau>";
				myXML+="<usage>" + tncCaracteristique.getUsageValue() + "</usage>";
				myXML+="<debitMontant_Unite>" + tncCaracteristique.cbMontantUnite.selectedItem.VALUE + "</debitMontant_Unite>";
				myXML+="<debitMontant_Contrat>" + tncCaracteristique.tiMontantContrat.text + "</debitMontant_Contrat>";
				myXML+="<debitMontant_Max>" + tncCaracteristique.tiMontantDebitMax.text + "</debitMontant_Max>";
				myXML+="<debitDescendant_Unite>" + tncCaracteristique.cbDescendantUnite.selectedItem.VALUE + "</debitDescendant_Unite>";
				myXML+="<debitDescendant_Contrat>" + tncCaracteristique.tiDescendantContrat.text + "</debitDescendant_Contrat>";
				myXML+="<debitDescendant_Max>" + tncCaracteristique.tiDescendantDebitMax.text + "</debitDescendant_Max>";
				myXML+="<GTR>" + tncCaracteristique.tiGTRContractuel.text + "</GTR>";
				myXML+="<presentation>" + tncCaracteristique.tiPresentation.text + "</presentation>";
				myXML+="<protocole_physique>" + tncCaracteristique.tiProtocolePhysique.text + "</protocole_physique>";
				myXML+="<protocole_liaison>" + tncCaracteristique.tiProtocoleLiaison.text + "</protocole_liaison>";
				myXML+="<protocole_reseau>" + tncCaracteristique.tiProtocoleReseau.text + "</protocole_reseau>";
				myXML+="</caracteristique>";
			}
			if (tncCablageSdaPointaPoint && tncCablageSdaPointaPoint.tiElementActifA != null)
			{
				myXML+="<cablage_point_a_point>";
				myXML+="<element_actif_PA>" + tncCablageSdaPointaPoint.tiElementActifA.text + "</element_actif_PA>";
				myXML+="<element_actif_amorce_emission_PA>" + tncCablageSdaPointaPoint.tiElementActifEmissionA.text + "</element_actif_amorce_emission_PA>";
				myXML+="<element_actif_amorce_reception_PA>" + tncCablageSdaPointaPoint.tiElementActifReceptionA.text + "</element_actif_amorce_reception_PA>";
				myXML+="<amorce_tete_emission_PA>" + tncCablageSdaPointaPoint.tiAmorceTeteEmissionA.text + "</amorce_tete_emission_PA>";
				myXML+="<amorce_tete_reception_PA>" + tncCablageSdaPointaPoint.tiAmorceTeteReceptionA.text + "</amorce_tete_reception_PA>";
				myXML+="<adresse_PA>" + tncCablageSdaPointaPoint.tiAdresseA.text + "</adresse_PA>";
				myXML+="<code_postal_PA>" + tncCablageSdaPointaPoint.tiCodePostalA.text + "</code_postal_PA>";
				myXML+="<commune_PA>" + tncCablageSdaPointaPoint.tiCommuneA.text + "</commune_PA>";
				myXML+="<element_actif_PB>" + tncCablageSdaPointaPoint.tiElementActifB.text + "</element_actif_PB>";
				myXML+="<element_actif_amorce_emission_PB>" + tncCablageSdaPointaPoint.tiElementActifEmissionB.text + "</element_actif_amorce_emission_PB>";
				myXML+="<element_actif_amorce_reception_PB>" + tncCablageSdaPointaPoint.tiElementActifReceptionB.text + "</element_actif_amorce_reception_PB>";
				myXML+="<amorce_tete_emission_PB>" + tncCablageSdaPointaPoint.tiAmorceTeteEmissionB.text + "</amorce_tete_emission_PB>";
				myXML+="<amorce_tete_reception_PB>" + tncCablageSdaPointaPoint.tiAmorceTeteReceptionB.text + "</amorce_tete_reception_PB>";
				myXML+="<adresse_PB>" + tncCablageSdaPointaPoint.tiAdresseB.text + "</adresse_PB>";
				myXML+="<code_postal_PB>" + tncCablageSdaPointaPoint.tiCodePostalB.text + "</code_postal_PB>";
				myXML+="<commune_PB>" + tncCablageSdaPointaPoint.tiCommuneB.text + "</commune_PB>";
				myXML+="<identifiant_LL_PA>" + tncCablageSdaPointaPoint.tiIdentifiantLLA.text + "</identifiant_LL_PA>";
				myXML+="<identifiant_LL_PB>" + tncCablageSdaPointaPoint.tiIdentifiantLLB.text + "</identifiant_LL_PB>";
				myXML+="<nature>" + tncCablageSdaPointaPoint.tiNature.text + "</nature>";
				myXML+="<debit>" + tncCablageSdaPointaPoint.tiDebit.text + "</debit>";
				myXML+="<distance>" + tncCablageSdaPointaPoint.tiDistance.text + "</distance>";
				myXML+="</cablage_point_a_point>";
			}
			if (tncCablageSdaXdsl && tncCablageSdaXdsl.tiElementActifA != null)
			{
				myXML+="<cablage_point_a_point>";
				myXML+="<element_actif_PA>" + tncCablageSdaXdsl.tiElementActifA.text + "</element_actif_PA>";
				myXML+="<element_actif_amorce_emission_PA>" + tncCablageSdaXdsl.tiElementActifEmissionA.text + "</element_actif_amorce_emission_PA>";
				myXML+="<element_actif_amorce_reception_PA>" + tncCablageSdaXdsl.tiElementActifReceptionA.text + "</element_actif_amorce_reception_PA>";
				myXML+="<amorce_tete_emission_PA>" + tncCablageSdaXdsl.tiAmorceTeteEmissionA.text + "</amorce_tete_emission_PA>";
				myXML+="<amorce_tete_reception_PA>" + tncCablageSdaXdsl.tiAmorceTeteReceptionA.text + "</amorce_tete_reception_PA>";
				myXML+="<adresse_PA>" + tncCablageSdaXdsl.tiAdresseA.text + "</adresse_PA>";
				myXML+="<code_postal_PA>" + tncCablageSdaXdsl.tiCodePostalA.text + "</code_postal_PA>";
				myXML+="<commune_PA>" + tncCablageSdaXdsl.tiCommuneA.text + "</commune_PA>";
				myXML+="<identifiant_LL_PA>" + tncCablageSdaXdsl.tiIdentifiantLLA.text + "</identifiant_LL_PA>";
				myXML+="<nature>" + tncCablageSdaXdsl.tiNature.text + "</nature>";
				myXML+="<debit>" + tncCablageSdaXdsl.tiDebit.text + "</debit>";
				myXML+="<distance>" + tncCablageSdaXdsl.tiDistance.text + "</distance>";
				myXML+="</cablage_point_a_point>";
			}
			if (tncCablageSdaT2T0Ana && tncCablageSdaT2T0Ana.tiAmorce != null)
			{
				myXML+="<cablage_T2_T0_ANA>";
				myXML+="<cablage>";
				myXML+="<amorce>" + tncCablageSdaT2T0Ana.tiAmorce.text + "</amorce>";
				myXML+="<reglette1>" + tncCablageSdaT2T0Ana.tiReglette1.text + "</reglette1>";
				myXML+="<paire1>" + tncCablageSdaT2T0Ana.tiPaire1.text + "</paire1>";
				myXML+="<reglette2>" + tncCablageSdaT2T0Ana.tiReglette2.text + "</reglette2>";
				myXML+="<paire2>" + tncCablageSdaT2T0Ana.tiPaire2.text + "</paire2>";
				myXML+="</cablage>";
				myXML+="<tranches>";

				var lenDgSda:int=(tncCablageSdaT2T0Ana.dgSDA.dataProvider) ? (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection).length : 0;

				for (var i:int=0; i < lenDgSda; i++)
				{
					myXML+="<tranche>";
					myXML+="<libelle>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].libelle + "</libelle>";
					myXML+="<type_tranche>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].typeTranche + "</type_tranche>";
					myXML+="<nombre_de_num>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].nbNum + "</nombre_de_num>";
					myXML+="<num_de_depart>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].numDepart + "</num_de_depart>";
					myXML+="<num_de_fin>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].numFin + "</num_de_fin>";
					myXML+="</tranche>";
				}
				myXML+="</tranches>";
				myXML+="</cablage_T2_T0_ANA>";
			}
			if (tncCanauxGroupment && tncCanauxGroupment.tiCanauxMixteDebut != null)
			{
				myXML+="<canaux_groupement>";
				myXML+="<specialisation_canaux>";
				myXML+="<canaux_mix_debut>" + tncCanauxGroupment.tiCanauxMixteDebut.text + "</canaux_mix_debut>";
				myXML+="<canaux_mix_fin>" + tncCanauxGroupment.tiCanauxMixteFin.text + "</canaux_mix_fin>";
				myXML+="<canaux_entrants_debut>" + tncCanauxGroupment.tiCanauxEntrantsDebut.text + "</canaux_entrants_debut>";
				myXML+="<canaux_entrants_fin>" + tncCanauxGroupment.tiCanauxEntrantsFin.text + "</canaux_entrants_fin>";
				myXML+="<canaux_sortants_debut>" + tncCanauxGroupment.tiCanauxSortantsDebut.text + "</canaux_sortants_debut>";
				myXML+="<canaux_sortants_fin>" + tncCanauxGroupment.tiCanauxSortantsFin.text + "</canaux_sortants_fin>";
				myXML+="<nbr_canaux>" + tncCanauxGroupment.tiNbCanaux.text + "</nbr_canaux>";
				myXML+="</specialisation_canaux>";
				myXML+="<groupement>";
				if (tncCanauxGroupment.rbLigneGrpmentLigneOK.selected)
					myXML+="<ligne_appartient_a_groupement>1</ligne_appartient_a_groupement>";
				else
					myXML+="<ligne_appartient_a_groupement>0</ligne_appartient_a_groupement>";
				if (tncCanauxGroupment.cbxTeteLigne.selected)
					myXML+="<is_tete_de_ligne>1</is_tete_de_ligne>";
				else
					myXML+="<is_tete_de_ligne>0</is_tete_de_ligne>";
				myXML+="<id_sous_tete_tete_de_ligne>" + (tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection)[0].idTeteLigne + "</id_sous_tete_tete_de_ligne>";
				myXML+="<lignes_du_groupement>";

				var lenDgGrpment:int=(tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection).length;

				for (var j:int=1; j < lenDgGrpment; j++)
				{
					myXML+="<ligne>";
					myXML+="<id_sous_tete>" + (tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne + "</id_sous_tete>";
					myXML+="</ligne>";
				}

				myXML+="</lignes_du_groupement>";
				myXML+="</groupement>";
				myXML+="</canaux_groupement>";
			}
			myXML+="</element>";

			return myXML;
		}

//		private function activeValidButton(event:Event):void
//		{
//			btValid.enabled = true;
//		}
		public function clickModifierOrgaHandler(event:MouseEvent):void
		{
			ficheOrga=new FicheOrgaIHM();
			ficheOrga.libelleOrga=(cbOrganisation.selectedItem as OrgaVO).libelleOrga;
			ficheOrga.idOrga=(cbOrganisation.selectedItem as OrgaVO).idOrga;
			ficheOrga.addEventListener(gestionparcEvent.VALIDE_FICHE_ORGA, updateOrga);
			PopUpManager.addPopUp(ficheOrga, this, true);
			PopUpManager.centerPopUp(ficheOrga);
		}

		private function updateOrga(event:gestionparcEvent):void
		{
			var lenListeOrga:int=myServiceLigne.myDatas.ligne.listeOrganisation.length;

			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).idOrga == ficheOrga.myService.myDatas.cheminSelected.idOrga)
				{
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).idCible=ficheOrga.myService.myDatas.cheminSelected.idFeuille;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).chemin=ficheOrga.myService.myDatas.cheminSelected.chemin;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).position=1;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition=new Date();
				}
			}
			lbPosition.text=ficheOrga.myService.myDatas.cheminSelected.chemin;
			(cbOrganisation.selectedItem as OrgaVO).chemin=ficheOrga.myService.myDatas.cheminSelected.chemin;
//			activeValidButton(null);
		}

		private function changeOrgaHandler(event:ListEvent):void
		{
			if ((cbOrganisation.selectedItem as OrgaVO).position == 1)
				lbPosition.text=(cbOrganisation.selectedItem as OrgaVO).chemin;
			else
				lbPosition.text="";
//			activeValidButton(null);
		}

		private function imgEditFonctionClickHandler(event:MouseEvent):void
		{
			var popupUsage:PopUpUsageIHM=new PopUpUsageIHM();
			if (cbFonction.selectedItem)
			{
				if (cbFonction.selectedItem.hasOwnProperty("USAGE")) // c'est une ancienne fonction
				{
					popupUsage.oldUsage=cbFonction.selectedItem.USAGE;
				}
				else // c'est une nouvelle fonction
				{
					popupUsage.oldUsage=(cbFonction.selectedItem as String);
					strOldUsage=(cbFonction.selectedItem as String);
				}
			}
			popupUsage.addEventListener(gestionparcEvent.POPUP_USAGE_VALIDED, addUsage);
			PopUpManager.addPopUp(popupUsage, this, true);
			PopUpManager.centerPopUp(popupUsage);
		}

		private function addUsage(evt:gestionparcEvent):void
		{
			if (strOldUsage) // si ce n'est pas le premiere edition de fonction
			{
				var lenListFonction:int=(cbFonction.dataProvider as ArrayCollection).length;

				for (var i:int=0; i < lenListFonction; i++)
				{
					if (cbFonction.dataProvider[i] == strOldUsage)
					{
						(cbFonction.dataProvider as ArrayCollection).removeItemAt(i);
						break;
					}
				}
			}

			(cbFonction.dataProvider as ArrayCollection).addItem(evt.obj);
			cbFonction.selectedIndex=(cbFonction.dataProvider as ArrayCollection).length - 1;
//			activeValidButton(null);
		}

		private function cacheInfoOpLoadedHandler(event:gestionparcEvent):void
		{
			if (CacheDatas.listeInfosOperateur && CacheDatas.listeInfosOperateur.length > 0)
			{
				var dureeEligibilite:int=CacheDatas.listeInfosOperateur[0].DUREE_ELLIGIBILITE;
				reinitiliseContratOperateur(dureeEligibilite - 1); // -1 car l'index commence de 0
			}
		}

		private function cacheListeProfilEqptLoadedHandler(event:gestionparcEvent):void
		{
			cbTypeCmd.dataProvider=CacheDatas.listeProfilEqpt;
			cbTypeCmd.selectedIndex=0;
			if (cbTypeCmd.selectedItem)
			{
				var objToServiceGet:Object=new Object();
				objToServiceGet.idPool=SpecificationVO.getInstance().idPoolNotValid;
				objToServiceGet.idProfilEquipement=cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT;
				objToServiceGet.clefRecherche="";

				CacheServices.getInstance().getListRevendeurForProfEqpt(objToServiceGet);
			}
		}

		private function cbTypeCmdListEventChange(event:ListEvent):void
		{
			var popUpChangeType:ConfirmerOrAnnulerMsgIHM=new ConfirmerOrAnnulerMsgIHM();
			popUpChangeType.msg=ResourceManager.getInstance().getString('M111', 'Si_vous_modifiez_le_type_de_commande__tous_vos_c');
			popUpChangeType.btConfirmerLabel=ResourceManager.getInstance().getString('M111', 'Confirmer');
			popUpChangeType.btAnnulerLabel=ResourceManager.getInstance().getString('M111', 'Annuler');
			popUpChangeType.addEventListener(gestionparcEvent.POPUP_CONFIRMER_ACTION_CONFIRMER, changeTypeCmdConfirmer);
			popUpChangeType.addEventListener(gestionparcEvent.POPUP_CONFIRMER_ACTION_ANNULER, changeTypeCmdAnnuler);
			PopUpManager.addPopUp(popUpChangeType, this, true);
			PopUpManager.centerPopUp(popUpChangeType);
		}

		private function reinitiliseAboOpt():void
		{
			myServiceLigne.myDatas.listeAbo=new ArrayCollection();
			myServiceLigne.myDatas.listeAboFavoris=new ArrayCollection();
			myServiceLigne.myDatas.listeOpt=new ArrayCollection();
			myServiceLigne.myDatas.listeOptFavoris=new ArrayCollection();

			tncAboOption.dataDatagrid.removeAll();
		}

		private function reinitiliseContratOperateur(dureeEligibilite:int=3):void
		{
			tncContratOperateur.lbOperateur.text=cbOperateur.selectedItem.NOM;
			tncContratOperateur.dcDateOuverture.selectedDate=new Date();
			tncContratOperateur.cbDureeContrat.selectedIndex=2; // 24 mois
			tncContratOperateur.cbDureeEligibilite.selectedIndex=dureeEligibilite;
			if (tncContratOperateur.cbDureeEligibilite.selectedIndex != -1)
			{
				tncContratOperateur.lbDateEligibilite.text=DateFunction.formatDateAsString(DateFunction.dateAdd("m", new Date(), tncContratOperateur.cbDureeEligibilite.selectedIndex));
			}
			if (tncContratOperateur.cbDureeContrat.selectedIndex != -1)
			{
				tncContratOperateur.lbDateFpc.text=DateFunction.formatDateAsString(DateFunction.dateAdd("m", new Date(), tncContratOperateur.cbDureeContrat.selectedIndex));
			}
		}

		private function changeTypeCmdConfirmer(event:Event):void
		{
			reinitiliseAboOpt();

			indexTypeCmd=cbTypeCmd.selectedIndex;

			var objToServiceGet:Object=new Object();
			objToServiceGet.idPool=SpecificationVO.getInstance().idPoolNotValid;
			objToServiceGet.idProfilEquipement=cbTypeCmd.selectedItem.IDPROFIL_EQUIPEMENT;
			objToServiceGet.clefRecherche="";

			CacheServices.getInstance().getListRevendeurForProfEqpt(objToServiceGet);
		}

		private function changeTypeCmdAnnuler(event:Event):void
		{
			cbTypeCmd.selectedIndex=indexTypeCmd;
		}

		private function cacheListeOperateurLoadedHandler(event:gestionparcEvent):void
		{
			cbOperateur.dataProvider=CacheDatas.listTousOperateur;
			cbOperateur.selectedIndex=0;
			if (cbOperateur.selectedItem)
			{
				var objToServiceGet:Object=new Object();
				objToServiceGet.idPool=SpecificationVO.getInstance().idPoolNotValid;
				objToServiceGet.idOperateur=cbOperateur.selectedItem.OPERATEURID;

				CacheServices.getInstance().getListeComptesOperateurByLoginAndPool(objToServiceGet);
				CacheServices.getInstance().getInfoClientOp(objToServiceGet);
			}
			cbOperateurPreselection.dataProvider=CacheDatas.listTousOperateur;
		}

		private function cbOperateurListEventChange(event:ListEvent):void
		{
			var popUpChangeType:ConfirmerOrAnnulerMsgIHM=new ConfirmerOrAnnulerMsgIHM();
			popUpChangeType.msg=ResourceManager.getInstance().getString('M111', 'Si_vous_modifiez_l_op_rateur__tous_vos_c');
			popUpChangeType.btConfirmerLabel=ResourceManager.getInstance().getString('M111', 'Confirmer');
			popUpChangeType.btAnnulerLabel=ResourceManager.getInstance().getString('M111', 'Annuler');
			popUpChangeType.addEventListener(gestionparcEvent.POPUP_CONFIRMER_ACTION_CONFIRMER, changeOperateurConfirmer);
			popUpChangeType.addEventListener(gestionparcEvent.POPUP_CONFIRMER_ACTION_ANNULER, changeOperateurAnnuler);
			PopUpManager.addPopUp(popUpChangeType, this, true);
			PopUpManager.centerPopUp(popUpChangeType);
		}

		private function changeOperateurConfirmer(event:gestionparcEvent):void
		{
			reinitiliseAboOpt();

			indexOperateur=cbOperateur.selectedIndex;

			var objToServiceGet:Object=new Object();
			objToServiceGet.idPool=SpecificationVO.getInstance().idPoolNotValid;
			objToServiceGet.idOperateur=cbOperateur.selectedItem.OPERATEURID;

			CacheServices.getInstance().getListeComptesOperateurByLoginAndPool(objToServiceGet);
			CacheServices.getInstance().getInfoClientOp(objToServiceGet);
		}

		private function changeOperateurAnnuler(event:gestionparcEvent):void
		{
			cbOperateur.selectedIndex=indexOperateur;
		}

		private function cbCompteChangeHandler(event:Event):void
		{
			cbSousCompte.dataProvider=cbCompte.dataProvider[cbCompte.selectedIndex].TAB_SOUS_COMPTE;
			cbSousCompte.selectedIndex=0;
		}

		private function onTncRaccordementClickHandler(event:MouseEvent):void
		{
			var popUpChangementRaccordement:FicheChangementRaccordementIHM=new FicheChangementRaccordementIHM();
			popUpChangementRaccordement.myServiceLigne=myServiceLigne;
			popUpChangementRaccordement.addEventListener(gestionparcEvent.RACCOR_LIGNE_DID, onRaccorLigneDidHandler);
			PopUpManager.addPopUp(popUpChangementRaccordement, this, true);
			PopUpManager.centerPopUp(popUpChangementRaccordement);
		}

		private function onRaccorLigneDidHandler(evt:gestionparcEvent):void
		{
			tncRaccordement.dataDatagrid.addItem(evt.obj);
			tncRaccordement.buttonAction.enabled=false;
//			activeValidButton(null);
		}

		private function runValidators():Boolean
		{
			var validators:Array=null;
			var results:Array=null;

			validators=[valSousTete, valNumContrat, valOperateur, valCompte, valSousCompte, valTypeCmd, valDistributeur, valAcces];
			results=Validator.validateAll(validators);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
			{
				return true;
			}
		}

		public function btnRecopier_clickHandler():void
		{
			tiNumContrat.text=tiSousTete.text;

		}

		private function changeOngletDefault():void
		{
			tnLigne.removeAllChildren();

			tnLigne.addChild(createOngletAbo());
			tnLigne.addChild(createOngletContrat());
			tnLigne.addChild(createOngletDivers());
			tnLigne.addChild(createOngletRaccordement());

			boolSite=true;
			boolPoint=true;
			boolT2=true;
			boolXdsl=true;
			boolCanaux=true;
			boolCaracteristique=true;
		}

		private function changeOngletLS():void
		{
			tnLigne.removeAllChildren();

			tnLigne.addChild(createOngletSite());
			tnLigne.addChild(createOngletAbo());
			tnLigne.addChild(createOngletDivers());
			tnLigne.addChild(createOngletContrat());
			tnLigne.addChild(createOngletRaccordement());
			tnLigne.addChild(createOngletCaracteristique());
			tnLigne.addChild(createOngletCablageSdaPointAPoint());

			boolT2=true;
			boolXdsl=true;
			boolCanaux=true;
		}

		private function changeOngletXDSL():void
		{
			tnLigne.removeAllChildren();

			tnLigne.addChild(createOngletSite());
			tnLigne.addChild(createOngletAbo());
			tnLigne.addChild(createOngletDivers());
			tnLigne.addChild(createOngletContrat());
			tnLigne.addChild(createOngletRaccordement());
			tnLigne.addChild(createOngletCaracteristique());
			tnLigne.addChild(createOngletCablageSdaXdsl());

			boolT2=true;
			boolPoint=true;
			boolCanaux=true;
		}

		private function changeOngletT2():void
		{
			tnLigne.removeAllChildren();

			tnLigne.addChild(createOngletSite());
			tnLigne.addChild(createOngletAbo());
			tnLigne.addChild(createOngletDivers());
			tnLigne.addChild(createOngletContrat());
			tnLigne.addChild(createOngletRaccordement());
			tnLigne.addChild(createOngletCaracteristique());
			tnLigne.addChild(createOngletCablageSdaT2());
			tnLigne.addChild(createOngletGroupement());

			boolPoint=true;
			boolXdsl=true;
		}

		/*
		 *
			En fonction du type de fiche ligne, on 	affiche certains onglets ou pas.
			TO,T2, LA => Site, AboOption, ContratOpérateur, Raccordement, Cablage SDA T2T0ANA, Canaux groupement
			LS, LL => Site, AboOption, ContratOpérateur, Raccordement, Cablage SDA Point à Point
			Autres => AboOption, ContratOpérateur, Divers, Raccordement
		*
		*/
		protected function cbTypeLigne_changeHandler(event:ListEvent):void
		{
			if (cbTypeLigne.selectedItem.ISMOBILE == 1 && cbTypeLigne.selectedItem.ISFIXE == 0)
				tiSousTete.maxChars=15;
			else
				tiSousTete.maxChars=50;

			if (cbTypeLigne.selectedIndex > -1)
			{
				switch (cbTypeLigne.selectedItem.TYPE_FICHELIGNE)
				{
					case gestionparcConstantes.LS:
						changeOngletLS();
						break;
					case gestionparcConstantes.LL:
						changeOngletLS();
						break;
					case gestionparcConstantes.XDSL:
						changeOngletXDSL();
						break;
					case gestionparcConstantes.T2:
						changeOngletT2();
						break;
					case gestionparcConstantes.T0:
						changeOngletT2();
						break;
					case gestionparcConstantes.LA:
						changeOngletT2();
						break;
					default:
						changeOngletDefault();
						break;
				}
			}
			tnLigneTabBar.selectedIndex=0;
		}
	}
}
