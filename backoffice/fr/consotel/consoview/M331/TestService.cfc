<cfcomponent output="false">
	
	
	
	<cffunction name='getListeRacines' returntype="query">
		<cfquery datasource="ROCOFFRE" name="qLRacines">
			SELECT * FROM groupe_client gc WHERE gc.id_groupe_maitre IS NULL ORDER BY gc.libelle_groupe_client ASC
		</cfquery>
		<cfreturn qLRacines>
	</cffunction>
	
	<cffunction name='getListeOrganisations' returntype="query">
		<cfargument name="idgRacine" type="numeric" required="true">
		<cfquery datasource="ROCOFFRE" name="qLOrgas">
			SELECT * FROM groupe_client gc, orga_niveau og WHERE og.idorga_niveau = gc.idorga_niveau AND gc.id_groupe_maitre = #idgRacine# AND (gc.type_orga = 'CUS' OR gc.type_orga = 'OPE') ORDER BY gc.type_orga ,gc.libelle_groupe_client ASC
		</cfquery>
		<cfreturn qLOrgas>
	</cffunction>
	
	<cffunction name='getListePeriodesAndCliche' returntype="query">
		<cfargument name="idOrganisation" type="numeric" required="true">
		<cfquery datasource="ROCOFFRE" name="qLPeriodes">
			SELECT * FROM cliche cl, periode p WHERE 
	         cl.idperiode_mois between 121 AND 132 
	         AND cl.idorganisation = #idOrganisation#
	         AND p.idperiode_mois = cl.idperiode_mois
	         ORDER BY p.idperiode_mois ASC
		</cfquery>P
		<cfreturn qLPeriodes>
	</cffunction>
		
	<cffunction name='getListeRapports' returntype="query">
		<cfquery datasource="ROCOFFRE" name="qLRapports">
			select 'inventaire des lignes' as label, 'INV_LG' as code, 1 as idrapport from dual
		</cfquery>
		<cfreturn qLRapports>
	</cffunction>
	
	<cffunction name='getListeFormats' returntype="query">
		<cfquery datasource="ROCOFFRE" name="qLRapports">
			select 'PDF' as label,'PDF' as code  from dual
			union
			select 'XLS' as label,'XLS' as code from dual
			union
			select 'CSV' as label,'CSV' as code from dual
		</cfquery>
		<cfreturn qLRapports>
	</cffunction>	
	
	<cffunction name='getListeSegments' returntype="query">
		<cfquery datasource="ROCOFFRE" name="qLSegmenst">
			select 'Fixe' as label,'FIXE' as code  from dual
			union
			select 'Mobile' as label,'MOBILE' as code from dual
			union
			select 'Réseau' as label,'RESEAU' as code from dual
		</cfquery>
		<cfreturn qLSegmenst>
	</cffunction>	
	
	<cffunction name='genererRapport' returntype="string">
		
		<cfargument name="rapport" type="string" required="true" default="INV_LG">
		<cfargument name="segment" type="string" required="true" default="FIXE">
		<cfargument name="format" type="string" required="true" default="XLS">
		<cfargument name="niveau" type="string" required="true" default="B">
		<cfargument name="idRacineMaster" type="numeric" required="true">
		<cfargument name="idRacine" type="numeric" required="true">
		<cfargument name="idOrganisation" type="numeric" required="true">
		<cfargument name="idPerimetre" type="numeric" required="true">
		<cfargument name="idCliche" type="numeric" required="true">
		<cfargument name="idPeriodeMoisDebut" type="numeric" required="true">
		<cfargument name="idPeriodeMoisFin" type="numeric" required="true">
		
		
		
		
		<cfreturn 'Rapport planifié'>
	</cffunction>
	
	
</cfcomponent>