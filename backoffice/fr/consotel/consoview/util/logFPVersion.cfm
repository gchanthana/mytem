<cfprocessingdirective pageencoding = "utf-8" suppressWhiteSpace = "Yes">
<cfif isDefined("fpversion") and isDefined("SESSION.USER.EMAIL") and fpversion NEQ "">
	<cfset message = SESSION.USER.EMAIL & ';'& fpversion>
	<cfmail from="no-reply@saaswedo.com"
		to="samuel.divioka@saaswedo.com"
		subject="[Sondage 1] - FP Version"
		type="text/html"
		charset="utf-8"
		server="mail.consotel.fr"
		port="25">	
		<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01//EN' 'http://www.w3.org/TR/html4/strict.dtd'>
		<html lang='fr'>
		<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<body>			
			#message#
		</body>
		</html>
	</cfmail>				
</cfif>
</cfprocessingdirective>