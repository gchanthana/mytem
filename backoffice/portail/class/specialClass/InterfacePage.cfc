<cfinterface displayname="Interface implémenté par les classes Page" >
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="afficher" access="public" output="true"  
				displayname="" 
				hint="Méthode abstraite">
		<cfargument name="head" required="true" type="string" >
		<cfargument name="context" required="true" type="struct" >
		<!--- methode abstraite - no implementation --->
	</cffunction>
	
</cfinterface>