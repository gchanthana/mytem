package univers.inventaire.inventaire.journaux
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	
	/**
	 * Classe coneteur pour les journaux
	 * */
	public class JournauxMain extends JournauxMainIHM
	{
		
		//Référence vers le journal qui est affiché		
		private var currentJournal : IJournal;
		
		//Booleen permettant de savoir si un journal est affiché
		private var isAffiche : Boolean = false;
		
		   
		 /**
		 * Constructeur 
		 * */
		public function JournauxMain()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		
		/**
		 * Gere le changement de périmètre
		 * */
		public function onPerimetreChange():void{		
			if (currentJournal != null)	currentJournal.onPerimetreChange();									
		}
		
		
		/**
		 * Affiche le journal suivant les parametres
		 * 
		 * @param type le type de journal a afficher
		 * @param nodeInfos un objet dont les attributs sont les information sur un noeud (ou feuille)
		 * 	-> NID l'identifiant du noeud
		 *  -> LBL le libelle du noeud
		 * */
		public function afficherJournal(type : String, nodeInfos : Object, dateRef:Date = null):void{
			 
			conteneur.removeAllChildren();
			currentJournal = JournalFactory.createJournal(type);	
			
			if (nodeInfos != null) currentJournal.nodeInfos = nodeInfos;
								
			conteneur.addChildAt(DisplayObject(currentJournal),0);
			isAffiche = true;
			
			DisplayObject(currentJournal).addEventListener(SelectionOperationEvent.OPERATION_SELECTED,operationSelectionneeHandler);
			if(currentJournal is InventaireComplet)
			{
				(currentJournal as InventaireComplet).dateRef = dateRef;
			}
			//currentJournal.refresh();
		}
		
		
		
		//Initialisation de l'IHM affectation des écouteurs d'évenements
		private function initIHM(fe :FlexEvent):void{
			btRafraichir.addEventListener(MouseEvent.CLICK,rafraichir);
		}
		
		
			
		
		private function operationSelectionneeHandler(soe : SelectionOperationEvent):void{
			var evtObj : SelectionOperationEvent = new SelectionOperationEvent(SelectionOperationEvent.OPERATION_SELECTED);
			evtObj.idOperation = soe.idOperation;
			dispatchEvent(evtObj);
		}	
		
		
		//gere le click sur le bouton rafraichir
		private function rafraichir(me : MouseEvent):void{
			if (currentJournal != null) currentJournal.refresh();
		}
		
		public function updateDate(value:Date):void
		{	
			if(currentJournal is InventaireComplet)
			{
				(currentJournal as InventaireComplet).dateRef = value;
			}
		}
		
	}
}