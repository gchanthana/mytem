<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.FSecureAntiTheft" extends="fr.consotel.api.mdm.partner.public.FSecureBase" hint="Implémentation AntiTheft FSecure">
	<!--- Anti-Theft Services --->
	<cffunction access="remote" name="getLockSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour vérrouiller un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.getLockSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getWipeSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour effectuer l'action WIPE sur un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.getWipeSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getResetAntitheftSettingsSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour réinitialiser la configuration Antitheft d'un équipement">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.getResetAntitheftSettingsSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="lockDevice" returntype="String" hint="Vérrouille un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.lockDevice(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="wipeDevice" returntype="String" hint="Effectue l'action WIPE sur un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.wipeDevice(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="resetAntitheftSettings" returntype="String" hint="Réinitialise la configuration Antitheft d'un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.resetAntitheftSettings(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="locateDevice" returntype="String" hint="Envoi un SMS de requête de localisation à un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.locateDevice(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getAntitheftCommandLog" returntype="String" hint="Retourne les logs des commandes Anti-Theft pour une Single License<br>
	Le résultat est un tableau vide si aucune commande n'est trouvée et sinon chaque élément est une entité commande contenant les éléments suivants :<br>
	- commandType : Type de la commande (Lock, Wipe, Reset, Locate, Alarm activation)<br>
	- phone : Numéro du mobile (e.g : +000000000000)<br>
	- sendTime : Timestamp de l'envoi de la requête de la commande<br>
	- responseTime : Timestamp de la réponse du client">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var commandLog=[]>
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfset var wsLogRto=wsService.getAntitheftCommandLog(ARGUMENTS["singleLicenseKey"])>
		<cfset var wsLogArray=wsLogRto.getSingleLicenceATLog()>
		<cfif isDefined("wsLogArray")>
			<cfset var logCount=arrayLen(wsLogArray)>
			<cfloop index="i" from="1" to="#logCount#">
				<cfset commandLog[i]={
					commandType=toString(wsLogArray[i].getCommandType()), phone=toString(wsLogArray[i].getPhone()),
					sendTime=toString(wsLogArray[i].getSendTime()), responseTime=toString(wsLogArray[i].getResponseTime())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(commandLog)>
	</cffunction>
	
	<cffunction access="remote" name="getLocationResponseLog" returntype="String" hint="Retourne les logs des commandes Anti-Theft de localisation pour une Single License<br>
	Le résultat est un tableau vide si aucune commande n'est trouvée et sinon chaque élément est une entité commande contenant les éléments suivants :<br>
	- phone : Numéro du mobile (e.g : +000000000000)<br>
	- sendTime : Timestamp de l'envoi de la requête de la commande<br>
	- responseTime : Timestamp de la réponse du client<br>
	- latitude : Coordonnée de latitude<br>
	- longitude : Coordonnée de longitude<br>
	- elevation : Elevation<br>
	- locationSource : Source de la localisation. Valeurs possibles : 0 (Inconnu), 1 (GPS), 2 (Réseau mobile)">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var commandLog=[]>
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfset var wsLogRto=wsService.getLocationResponseLog(ARGUMENTS["singleLicenseKey"])>
		<cfset var wsLogArray=wsLogRto.getSingleLicenceRTO()>
		<cfif isDefined("wsLogArray")>
			<cfset var logCount=arrayLen(wsLogArray)>
			<cfloop index="i" from="1" to="#logCount#">
				<cfset commandLog[i]={
					phone=toString(wsLogArray[i].getPhone()), sendTime=toString(wsLogArray[i].getSendTime()), responseTime=toString(wsLogArray[i].getResponseTime()),
					latitude=VAL(toString(wsLogArray[i].getLatitude())), longitude=VAL(toString(wsLogArray[i].getLongitude())), elevation=VAL(toString(wsLogArray[i].getElevation())),
					locationSource=VAL(toString(wsLogArray[i].getLocationSource()))
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(commandLog)>
	</cffunction>
	
	<cffunction access="remote" name="getLocateSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour retourner la réponse de localisation du client AntiTheft">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.getLocateSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="activateAlarm" returntype="String" hint="Envoi un SMS d'activation d'alarme à un équipement et retourne un identifiant de la commande effectuée">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.activateAlarm(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getAlarmSMS" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour activer l'alarme du client">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfreturn serializeJSON(wsService.getAlarmSMS(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getATMessageResponseLog" returntype="String" hint="Retourne les logs pour une commande spécifique<br>
	Le résultat est un tableau vide si aucune commande n'est trouvée et sinon chaque élément est une entité commande contenant les éléments suivants :<br>
	- commandType : Type de la commande (Lock, Wipe, Reset, Locate, Alarm activation)<br>
	- phone : Numéro du mobile (e.g : +000000000000)<br>
	- sendTime : Timestamp de l'envoi de la requête de la commande<br>
	- responseTime : Timestamp de la réponse du client">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfargument name="commandId" type="String" required="true" hint="Identifiant de la commande Anti-Theft">
		<cfset var commandLog=[]>
		<cfset var wsService=getSecurityService().getAntitheftService()>
		<cfset var wsLogRto=wsService.getATMessageResponseLog(ARGUMENTS["singleLicenseKey"],ARGUMENTS["commandId"])>
		<cfset var wsLogArray=wsLogRto.getSingleLicenceATLog()>
		<cfif isDefined("wsLogArray")>
			<cfset var logCount=arrayLen(wsLogArray)>
			<cfloop index="i" from="1" to="#logCount#">
				<cfset commandLog[i]={
					commandType=toString(wsLogArray[i].getCommandType()), phone=toString(wsLogArray[i].getPhone()),
					sendTime=toString(wsLogArray[i].getSendTime()), responseTime=toString(wsLogArray[i].getResponseTime())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(commandLog)>
	</cffunction>
</cfcomponent>