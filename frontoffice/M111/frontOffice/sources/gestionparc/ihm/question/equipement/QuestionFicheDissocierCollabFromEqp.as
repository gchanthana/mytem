package gestionparc.ihm.question.equipement
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheDissocierCollabFromEqp
	{
		private var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheDissocierCollabFromEqp()
		{
			var ruleMobile:IRuleDisplayItem=new IsLigneMobile();
			var ruleFixe:IRuleDisplayItem=new IsLigneFixe();
			var ruleSIM:IRuleDisplayItem=new HasSimAssociated();
			var ruleLIGNE:IRuleDisplayItem=new HasLigneAssociated();

			if (ruleSIM.isDisplay() && ruleMobile.isDisplay())
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERCOLLABMOBILE"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnMobile)
			}
			else if (ruleLIGNE.isDisplay() && ruleFixe.isDisplay())
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERCOLLABFIXE"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnFixe)
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERCOLLABEQP"), ResourceManager.getInstance().getString('M111', 'Dissocier_le_collaborateur_de_l__quipement'), alertClickHandler);	
			}
		}

		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				dissocierCollabFromEqp4();
			}
		}
		
		private function qnMobile(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				dissocierCollabFromEqp1()

			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
				dissocierCollabFromEqp2()
		}

		private function qnFixe(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				dissocierCollabFromEqp3()
			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
				dissocierCollabFromEqp1()

		}

		private function dissocierCollabFromEqp1():void
		{
			serv.dissocierCollabFromEqp1(setObj());
		}

		private function dissocierCollabFromEqp2():void
		{
			serv.dissocierCollabFromEqp2(setObj());
		}

		private function dissocierCollabFromEqp3():void
		{
			serv.dissocierCollabFromEqp3(setObj());
		}

		private function dissocierCollabFromEqp4():void
		{
			serv.dissocierCollabFromEqp4(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}
	}
}
