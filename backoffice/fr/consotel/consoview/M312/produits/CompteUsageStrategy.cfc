<!--- =========================================================================
Name: CompteUsageStrategy
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent extends="AbstractStrategy" displayname="CompteUsageStrategy" hint="This class implements the algorithm using the Strategy interface.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="CompteUsageStrategy" displayname="CompteUsageStrategy init()" hint="Initialize the CompteUsageStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getProduit" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#application.OFFREDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_client=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.libelle_produit>
	</cffunction>
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#application.OFFREDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_client=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.theme_libelle>
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" displayname="string getCompte(numeric)" >
		<cfargument name="IDcompte" required="true" type="numeric" default="" displayname="numeric IDcompte" hint="Initial value for the IDcompteproperty." />
		<cfquery name="qGetCompte" datasource="#application.OFFREDSN#">
			select compte_facturation
			from compte_facturation cf
			where cf.idcompte_facturation=#IDcompte#
		</cfquery>
		<cfreturn qGetCompte.compte_facturation>
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, date, date)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailProduit" datasource="#application.OFFREDSN#">
			SELECT SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
	           SUM(df.duree_appel) AS duree_appel, df.idproduit_client,
	           pc.idproduit_catalogue, pca.prorata, st.sous_tete, sc.siteID,
	           sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune, ip.numero_facture, th.type_theme,
				  ip.idinventaire_periode , ip.numero_facture, cf.compte_facturation
	           FROM detail_facture_abo df, produit_client pc, produit_catalogue pca, sous_tete st
	                	,site_client sc, sous_compte sco,inventaire_periode ip, compte_facturation cf
							,theme_produit th, theme_produit_catalogue tpc
	           WHERE df.idinventaire_periode=ip.idinventaire_periode
				  		AND ip.idcompte_facturation=cf.idcompte_facturation
		             AND cf.idcompte_facturation=#ID#
	           		AND df.idproduit_client=pc.idproduit_client 
	           		AND df.idsous_tete=st.idsous_tete
	           		AND st.idsous_compte=sco.idsous_compte
	           		AND sco.siteid=sc.siteid
	           		AND th.idtheme_produit=tpc.idtheme_produit
					AND tpc.idproduit_catalogue=pca.idproduit_catalogue
	           		AND df.idproduit_client=#IDproduit#
	           		AND pca.idproduit_catalogue=pc.idproduit_catalogue
	           		AND trunc(df.date_affectation,'MM')>=trunc(#DateDebut#,'MM')
			   		AND trunc(df.date_affectation,'MM')<=trunc(last_day(#DateFin#),'MM')
			   GROUP BY df.idproduit_client, ip.idinventaire_periode, pc.idproduit_catalogue, 
	           pca.prorata, st.sous_tete, sc.nom_site, sc.adresse1, sc.adresse2,sc.zipcode, sc.commune,
			   sc.siteID, numero_facture, th.type_theme, cf.compte_facturation
	           ORDER BY siteID,nom_site, adresse1, sous_tete
		</cfquery>
		<cfreturn qGetDetailProduit>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "Compte">
	</cffunction>
</cfcomponent>