package univers.parametres.poolgestionnaire.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.poolgestionnaire.event.GestionPoolEvent;
	import univers.parametres.poolgestionnaire.event.GestionTypeCommandeEvent;
	import univers.parametres.poolgestionnaire.service.GestionPoolService;
	import univers.parametres.poolgestionnaire.service.TypeCommandeService;
	import univers.parametres.poolgestionnaire.vo.PoolGestionnaire;
	import univers.parametres.poolgestionnaire.vo.TypeCommande;
	[Bindable]
	public class ParametreTypeCommande extends TitleWindow
	{
		private var groupeIndex : int;
		public var typeCommande : TypeCommande = new TypeCommande();
		public var typeCommandeService : TypeCommandeService= new TypeCommandeService();
		
		
		
		//UI COMPONANT
		public var cbAll : CheckBox;
		public var comboPays : ComboBox;
		public var combo_revendeur_commande : ComboBox;
		
		public function ParametreTypeCommande()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			if(typeCommande.idTypeCmd>0)
			{
				initMode_updateSite();
			}
			else
			{
				initMode_createSite();
			}
		}
		private function initMode_createSite():void
		{
			//Aucune modif à faire en mode création
		}
		private function initMode_updateSite():void
		{
			registerClassAlias("univers.parametres.poolgestionnaire.vo.TypeCommande",TypeCommande);  
			typeCommande= TypeCommande(ObjectUtil.copy(typeCommande))  // delete les ref sur le grid
			initAttribut();
		}
		private function initAttribut():void
		{
			//typeCommandeService.getPoolOfTypeCommande(groupeIndex,typeCommande.idTypeCmd);
		}
		public function valider():void
		{
			typeCommandeService.addEventListener(GestionTypeCommandeEvent.UPDATE_INFO_TYPE_CMD_COMPLETE,update_type_cmd_complete);
			
			if(typeCommande.idTypeCmd>0)
			{
				typeCommandeService.updateInfoTypeCommande(typeCommande.idTypeCmd,typeCommande.libelleTypeCmd,typeCommande.codeTypeCmd,typeCommande.commentaire);
			}
			else
			{
				typeCommandeService.addTypeCommande(groupeIndex,typeCommande.libelleTypeCmd,typeCommande.codeTypeCmd, typeCommande.commentaire);
			}
		}
		private function update_type_cmd_complete(evt : GestionTypeCommandeEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.PARAMETRE_TYPE_CMD_COMPLETE));
			var msg : String;
			if(typeCommande.idTypeCmd>0)
			{
				msg = ResourceManager.getInstance().getString('M21', 'Les_modifications_ont_bien__t__enregistr');
			}
			else
			{
				msg = ResourceManager.getInstance().getString('M21', 'La_cr_ation_du_type_de_commande_a_bien__');
			}
			ConsoviewAlert.afficherOKImage(msg);
			PopUpManager.removePopUp(this);
		}
		private function parametrePoolCOmpleteHandler(evt : GestionPoolEvent):void
		{
			initAttribut();
		}
		public function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
	
	}
}