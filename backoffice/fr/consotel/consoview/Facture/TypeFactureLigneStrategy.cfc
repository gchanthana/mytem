<!--- =========================================================================
Classe: TypeFactureLigneStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="TypeFactureLigneStrategy" extends="TypeFactureStrategy" hint="">

<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="Initial value for the IDINVENTAIRE_PERIODEproperty." />
		<cfargument name="IDsous_tete" required="false" type="string">
		<!--- Requêtte pour la facture de la ligne --->
			<cfquery name="qGetFactureAbo" datasource="#session.offreDSN#">
		SELECT cf.compte_facturation, sc.sous_compte, pca.libelle_produit, st.sous_tete, sum(dfa.montant) as montant, sum(nvl(dfa.nombre_appel,0)) as nombre_appel, sum(dfa.duree_appel) as duree_appel,
		sum(dfa.qte) as qte , tp.libelle, cp.libelle as gamme, dfa.prix_unit, to_char(dfa.datedeb,'yyyy/mm/dd') as datedeb, to_char(dfa.datefin,'yyyy/mm/dd') as datefin,
		ip.numero_facture, ip.date_emission, scl.nom_site, scl.adresse1, scl.adresse2, 
		scl.zipcode, scl.commune, tpl.libelle_type_ligne, pcv.code_tva, scl.siteID
		FROM inventaire_periode ip, DETAIL_FACTURE_ABO dfa, produit_client pcl, produit_catalogue pca, 
		sous_tete st, type_produit tp, categorie_produit cp, sous_compte sc, compte_facturation cf,
		site_client scl, type_ligne tpl, prod_cat_version pcv
		where ip.IDINVENTAIRE_PERIODE=#IDINVENTAIRE_PERIODE#
		AND dfa.idproduit_client=pcl.idproduit_client 
		AND sc.siteID=scl.siteID
		AND pca.idproduit_catalogue=pcv.idproduit_catalogue
		AND st.idtype_ligne=tpl.idtype_ligne
		AND pca.idproduit_catalogue=pcl.idproduit_catalogue 
		AND st.IDsous_tete=#IDsous_tete#
		AND ip.idinventaire_periode=dfa.idinventaire_periode 
		AND st.idsous_tete=dfa.idsous_tete
		AND cf.idcompte_facturation=sc.idcompte_facturation
		AND pca.idtype_produit=tp.idtype_produit 
		AND tp.idcategorie_produit=cp.idcategorie_produit
		AND st.idsous_compte=sc.idsous_compte 
		GROUP BY cf.compte_facturation, sc.sous_compte, tp.libelle, 
		cp.libelle,pca.libelle_produit,st.sous_tete, dfa.prix_unit, to_char(dfa.datedeb,'yyyy/mm/dd'), to_char(dfa.datefin,'yyyy/mm/dd'), ip.numero_facture, ip.date_emission, scl.nom_site, scl.adresse1, scl.adresse2, scl.zipcode, scl.commune, tpl.libelle_type_ligne,pcv.code_tva, scl.siteID
		ORDER BY sc.sous_compte, gamme, tp.libelle, sous_tete
	</cfquery>
	<cfreturn qGetFactureAbo>
	</cffunction>
	<cffunction name="displayType" access="public" returntype="string" output="false" >
		<cfreturn "Facture de la ligne">
	</cffunction>

</cfcomponent>