<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"
hint="Interface qui décrit un évènement permettant d'accéder aux infos et contenu correspondants à l'exécution d'un reporting.
Ce type d'évènement permet d'accéder à la référence IReportingEvent qui correspond à l'exécution du reporting
L'évènement IReportingEvent correspond à celui qui dispatché par l'implémentation IReportingService ayant exécuté le reporting">
	<cffunction name="getTemplateId" returntype="String" hint="Retourne le nom du template utilisé">
	</cffunction>
	
	<cffunction name="getFormat" returntype="String" hint="Retourne le format">
	</cffunction>
	
	<cffunction name="getLocalization" returntype="String" hint="Retourne la localisation utilisée">
	</cffunction>
	
	<cffunction name="getTimeZone" returntype="String" hint="Retourne le TimeZone utilisé">
	</cffunction>
	
	<cffunction name="getReportParameters" returntype="Struct" hint="Retourne les paramètres qui sont spécifiques au rapport BIP (XDO)
	Chaque paramètre est identifié par une clé ayant la meme valeur que son nom et associée à une structure.
	Cette structure contient les clés : NAME (Nom du paramètre avec la casse), VALUES (Tableau contenant les valeurs du paramètres)">
	</cffunction>

	<cffunction name="getContent" returntype="any" hint="Retourne le contenu correspondant au reporting">
		<cfargument name="localFileName" type="String"  required="true" hint="Nom à donner au fichier local correspondant au contenu récupéré localement">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="Chemin absolu du répertoire local dans lequel stocker le fichier localFileName">
	</cffunction>

	<cffunction name="getContentAsLocalFile" returntype="String"
	hint="Stocke le contenu du reporting dans le fichier local localFileName qui sera créé dans le répertoire localDirAbsPath puis retourne le chemin absolu du fichier
	Une chaine vide est à retourner si le contenu ne peut pas etre obtenu localement. Dans ce cas il est obtenu directement avec getContent()">
		<cfargument name="localFileName" type="String"  required="true" hint="Nom à donner au fichier local correspondant au contenu récupéré localement">
		<cfargument name="localDirAbsPath" type="String"  required="true"
		hint="Chemin absolu du répertoire local dans lequel stocker le fichier localFileName et qui se termine par le séparateur de chemin utilisé">
	</cffunction>
	
	<cffunction name="getDeliveryInfos" returntype="any"
	hint="Retourne un contenu générique de type texte (ou html) qui peut etre utilisé par exemple lorsque getStatus() est égal à FAILURE() ou EXCEPTION()">
	</cffunction>

	<cffunction name="getContentType" returntype="String"
	hint="Retourne le type MIME du contenu retourné par getContent() et référencé par getContentAsLocalFile">
	</cffunction>
	
	<cffunction name="getReportingEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"
	hint="Retourne l'évènement IReportingEvent initialement dispatché par une implémentation IReportingService et correspondant à l'exécution du reporting">
	</cffunction>
	
	<cffunction name="getEventSource" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"
	hint="Retourne l'évènement à partir duquel celui ci a été créé puis dispatché avec la méthode dispatchReportingEvent()
	Les infos qui sont retourné par cette implémentation sont aussi récupérés à partir de l'évènement retourné par cette méthode">
	</cffunction>
	
	<cffunction name="isTextContent" returntype="boolean" hint="Retourne TRUE si le contenu correspondant au reporting est de type Texte">
	</cffunction>
	
	<cffunction name="isBinaryContent" returntype="boolean" hint="Retourne TRUE si le contenu correspondant au reporting est de type Binaire">
	</cffunction>
	
	<cffunction name="isUnknownContent" returntype="boolean" hint="Retourne TRUE si le contenu correspondant au reporting est de type Inconnu">
	</cffunction>
	
	<cffunction name="getLocalPathSep" returntype="String" hint="Retourne le séparateur de chemin qui doit etre utilisé pour les chemins locaux">
	</cffunction>
</cfinterface>