package gestionparc.services.historique
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	public class HistoriqueServices
	{
		public var myDatas 		: HistoriqueDatas;
		public var myHandlers 	: HistoriqueHandlers;

		public function HistoriqueServices()
		{
			myDatas 	= new HistoriqueDatas();
			myHandlers 	= new HistoriqueHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer l'historique d'une ligne, d'un collab, d'unee sim ou d'un equipement
		 */	
		public function getHistorique(idemploye:int,idterm:int,idsim:int, idsoustete:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getHistorique",
				myHandlers.getHistoriqueHandler);
			
			RemoteObjectUtil.callService(op, idemploye,idterm,idsim,idsoustete);
		}
		/**
		 * Appelle la procédure permettant de récupérer l'historique d'un produit, 
		 * à savoir si le produit est entré ou sorti de l'inventaire.
		 */	
		public function getHistoriqueActionRessource(idressource:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheLigne",
				"getHistoriqueActionRessource",
				myHandlers.getHistoriqueActionRessourceResultHandler);
			
			RemoteObjectUtil.callService(op, idressource);
		}
	}
}