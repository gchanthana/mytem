package ihm.fonction.FctGestionCommunication.popup
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import ihm.fonction.FctGestionCommunication.composant.AttachementIHM;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.core.Application;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import utils.formator.Formator;

	public class AddPiecesJointesImpl extends TitleWindowBounds
	{
		//----------- VARIABLES -----------//
		
		private var _attachements				:AttachementIHM;
		
		public var vbxParcourir					:VBox;
		public var btnValid						:Button;
		public var btnClose						:Button;
		
		public var currentFilesToSave			:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public var sizeTotal					:String = '0.00';
		
		public static const sizeMaxPJ			:Number = 20;
		
		public static const ADD_ATTACHEMENT		:String = 'ADD_ATTACHEMENT';
		
		
		//----------- METHODES -----------//
		
		/* */
		public function AddPiecesJointesImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initListeners);
		}
		
		/* init listeners */
		private function initListeners(fe:FlexEvent):void
		{
			btnValid.addEventListener(MouseEvent.CLICK, btnValidClickHandler);
			btnClose.addEventListener(MouseEvent.CLICK, closeHandler);
			
			addChildParcourir();
		}
		
		/* au click sur btn Valider - Handler */
		protected function btnValidClickHandler(me:MouseEvent):void
		{
			var lenChild	:int 	= vbxParcourir.numChildren;
			var children 	:Array	= vbxParcourir.getChildren();
			var totalNbr	:Number = 0;
			var totalstr	:String = '0.00';
			
			for(var i:int=0; i<lenChild-1; i++)
			{
				if(children[i] as AttachementIHM)
				{
					if((children[i] as AttachementIHM).isStatus)
					{
						currentFilesToSave.addItem((children[i] as AttachementIHM).currentFile);
					}
					else
					{
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('CG05', 'Les_fichiers_sont_en_cours_de_v_rificati'), 'Consoview', null);
						return;
					}
					totalNbr = totalNbr + (children[i] as AttachementIHM).currentFile.fileSize;
				}
			}
			
			totalstr = Formator.formatOctetsToMegaOctets(totalNbr);
			
			if(currentFilesToSave.length == 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('CG05', 'Veuillez_s_lectionner_au_moins_un_fichier'), 'Consoview', null);
			}
			else
			{
				if(Number(totalstr) > sizeMaxPJ)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('CG05', 'Votre_capacit__total_de_pi_ces_jointes_e') + sizeTotal + ResourceManager.getInstance().getString('M28', '_20_Mo_'), 'Consoview', null);
				}
				else
				{
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('CG05', 'La_sauvegarde_de_fichier_se_fera_lors_de_la_validation_de_la_fiche_actualit_'),Application.application as DisplayObject);
					closeHandler(null);
				}
			}
		}
		
		/* fermeture de popup - Handler */
		protected function closeHandler(e:Event):void 
		{
			PopUpManager.removePopUp(this);
		}

		/*  ajout de l'enfant Attachement piece jointe */
		private function addChildParcourir():void
		{
			var idChildren:int = Math.random() * 1000000;
			
			_attachements 		= new AttachementIHM();
			_attachements.ID	= idChildren;
			
			vbxParcourir.addChild(_attachements); //ajouter l'enfant au VBox
			
			_attachements.addEventListener(AttachementIHM.ADDED_ATTACHEMENT, addedAttachementHandler);
			_attachements.addEventListener(AttachementIHM.REMOVE_ATTACHEMENT, removeAttachementHandler);
		}
		
		/* ajout de piece jointe - Handler */
		private function addedAttachementHandler(e:Event):void
		{
			var lenChild		:int 	= vbxParcourir.numChildren;
			var children 		:Array	= vbxParcourir.getChildren();
			var totalNbr		:Number = 0;
			var totalCount		:String = "";
			
			for(var i:int=0; i<lenChild; i++)
			{
				totalNbr = totalNbr + (children[i] as AttachementIHM).currentFile.fileSize;
			}
			totalCount = Formator.formatOctetsToMegaOctets(totalNbr);
			
			if(Number(totalCount) > sizeMaxPJ)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('CG05', 'Votre_capacit__total_de_pi_ces_jointes_e') + totalCount + ResourceManager.getInstance().getString('M28', '_20_Mo_'), 'Consoview', null);
			}
			else
			{
				_attachements.dispatchEvent(new Event(AttachementIHM.TOTALSIZEPJ_INFERIOR_MAXSIZE));
				sizeTotal = Formator.formatOctetsToMegaOctets(totalNbr);
				addChildParcourir(); //ajout d'un enfant Attachement PJ
			}
		}
		
		/* supression de piece jointe - Handler */
		private function removeAttachementHandler(e:Event):void
		{
			var idTarget	:int 	= e.currentTarget.ID;
			removeAttachement(idTarget);
			
			// recuperer les valeurs mises à jour apres suppression de l'enfant
			var lenChild	:int 	= vbxParcourir.numChildren;
			var children 	:Array	= vbxParcourir.getChildren();
			var totalNbr	:Number = 0;
			
			for(var i:int=0; i<lenChild-1; i++)
			{
				totalNbr = totalNbr + (children[i] as AttachementIHM).currentFile.fileSize;
			}
			
			sizeTotal = Formator.formatOctetsToMegaOctets(totalNbr);
			
			if(lenChild == 0)
				addChildParcourir();
			
			if(Number(sizeTotal) > sizeMaxPJ)
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('CG05', 'Votre_capacit__total_de_pi_ces_jointes_e') + sizeTotal + ResourceManager.getInstance().getString('M28', '_20_Mo_'), 'Consoview', null);
		}
		
		/* suppression d'un enfant [attachement piece jointe] dans l'IHM */
		private function removeAttachement(idTarget:int):void
		{
			var lenChild	:int 	= vbxParcourir.numChildren;
			var children 	:Array	= vbxParcourir.getChildren();
			
			for(var i:int = 0;i < lenChild;i++)
			{
				if((children[i] as AttachementIHM).ID == idTarget)
				{
					vbxParcourir.removeChildAt(i);
					return;
				}
			}
		}
	}
}