package univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile
{
	import composants.util.article.Article;
	import composants.util.article.ArticleNouvelleLigneMobile;
	import composants.util.contrats.Contrat;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.commande.system.Commande;
	import univers.inventaire.commande.system.TypesCommandesMobile;
	import univers.inventaire.contrat.ihm.GererOptionsAbonnementIHM;
	import univers.inventaire.contrat.ihm.RenouvelerContratIHM;
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.cellule.Cellule;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterSimCollab;
	import univers.inventaire.equipements.gestionflottemobile.comportementaffectation.AffecterSimLigne;
	import univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation.DesaffecterSIMOfLigne;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoCollab;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoSim;
	import univers.inventaire.equipements.gestionflottemobile.panelEnconstruction;
	import univers.inventaire.equipements.gestionflottemobile.panelaffectation.PanelAffectation;
	import univers.inventaire.equipements.gestionflottemobile.paneldesaffectation.PanelDesaffectation;
	import univers.inventaire.equipements.gestionflottemobile.rebus.RebusLigne;
	
	[Event(name="btHistoriqueCliked",type="flash.events.MouseEvent")]
	
	public class MenuFlotteMobNDI extends MenuGestionFlotteMobileIHM
	{
		public static const BT_HISTORIQUE_CLIKED:String = "btHistoriqueCliked";
		
		[Embed(source="/assets/equipement/06-50x50BIG.png")] 
		private var adrImg : Class;
		
		[Embed(source='/assets/flotteMobile/Color/Transforme.jpg')] 
		private var adrOptionLigne: Class;
		
		[Embed(source='/assets/equipement/minilogo_mobiles/icTransforme.jpg')] 
		private var mini_adrOptionLigne: Class;
		
		private var panelGestionFlotteMobile  : PanelGestionFoltteMobile;
		private var popup : TitleWindow;		
		private var stateToolTip : Boolean = stateToolTip;
		
		public function MenuFlotteMobNDI(panelGestionFlotteMobile  : PanelGestionFoltteMobile,stateToolTip : Boolean = false)
		{
			this.panelGestionFlotteMobile=panelGestionFlotteMobile;
			super();
			this.stateToolTip = stateToolTip;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(fe : FlexEvent):void
		{
			if(stateToolTip==true)
			{
				bt_mod_sav.visible = false;
				bt_mod_sav.width=0;
				bt_mod_sav.height=0;
					
				this.setCurrentState("toolTipState");
				labToolTip = panelGestionFlotteMobile.objMenuFlotteMobNDI_fixe.labToolTip;
			}
			var imgNew : Image = new Image();
			imgNew.source=adrImg;
			canvas_icone.addChild(imgNew);
			
			bt_new_user.initParametre(true,"Associer à un utilisateur la ligne: ");
			bt_new_tel.initParametre(true,"Associer à un terminal la ligne:");	
			bt_new_num.initParametre(true,"Réactiver à une ligne la ligne:");
			bt_new_sim.initParametre(true,"Associer à une carte SIM la ligne:");
			
			bt_sup_user.initParametre(true,"Dissocier l'utilisateur de la ligne:");	
			bt_sup_tel.initParametre(true,"Dissocier le terminal de la ligne:");	
			bt_sup_num.initParametre(true,"Suspendre la ligne : ");
			bt_sup_sim.initParametre(true,"Dissocier de la carte SIM la ligne: ");
			
			bt_mod_sav.initParametre(false);	
			bt_mod_renouv.initParametre(true,"Renouveler la ligne: ");	
			bt_mod_preter.initParametre(true,"Ajouter une option à la ligne: ");
			bt_mod_rebus.initParametre(true,"Résillier la ligne: ");
			btGetHistorique.initParametre(true,"Historique de la ligne ");
			
			bt_sup_user.addEventListener(MouseEvent.CLICK,newPanelUserOfSIM);
			bt_sup_sim.addEventListener(MouseEvent.CLICK,newPanelSIMOfLigne);
			bt_sup_tel.addEventListener(MouseEvent.CLICK,newPanelTermOfSIM);
			bt_sup_num.addEventListener(MouseEvent.CLICK,newPanelActivation);
			
			bt_new_user.addEventListener(MouseEvent.CLICK,newPanelListeUser);
			bt_new_sim.addEventListener(MouseEvent.CLICK,newPanelListeSIM);
			bt_new_tel.addEventListener(MouseEvent.CLICK,newPanelListeTerm);
			bt_new_num.addEventListener(MouseEvent.CLICK,newPanelActivation);
			
			bt_mod_renouv.addEventListener(MouseEvent.CLICK,newPanelRenouvellement);
			bt_mod_preter.addEventListener(MouseEvent.CLICK,newPanelGestionOptions);
			bt_mod_rebus.addEventListener(MouseEvent.CLICK,newPanelRebus);
			bt_mod_sav.addEventListener(MouseEvent.CLICK,newPanelEnConstruction);
			if(stateToolTip==true)
			{
				bt_mod_preter.setStyle("icon",mini_adrOptionLigne); // Changement exeption
			}
			else
			{
				bt_mod_preter.setStyle("icon",adrOptionLigne); // Changement exeption
			}
			
			btGetHistorique.addEventListener(MouseEvent.CLICK,_btGetHistoriqueClickHandler);
			
		}
		private function newPanelRenouvellement(event : MouseEvent):void
		{
			 
			var contrat:Contrat = new Contrat();
			
			contrat.IDCONTRAT = PanelGestionFoltteMobile.objLigneSelected.IDCONTRAT_ABO;
			contrat.IDSOUS_TETE = PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE;
			contrat.NUMERO_CONTRAT = PanelGestionFoltteMobile.objLigneSelected.NUM_CONTRAT_ABO;
			contrat.SOUS_TETE = PanelGestionFoltteMobile.objLigneSelected.LIGNE;
					
			var commande:Commande = new Commande(true);
			commande.TYPE_OPERATION = "Renouvellement";
			commande.IDTYPE_COMMANDE = TypesCommandesMobile.RENOUVELLEMENT;  
			commande.IDPOOL_GESTIONNAIRE = PanelGestionFoltteMobile.idPoolGestionnaire;		
			commande.LIBELLE_POOL = panelGestionFlotteMobile.comboPoolGestionnaire.selectedLabel;		
			commande.IDREVENDEUR = PanelGestionFoltteMobile.objLigneSelected.IDDISTRIBUTEUR;
			commande.LIBELLE_REVENDEUR = PanelGestionFoltteMobile.objLigneSelected.DISTRIBUTEUR;
			
			
			
			var article:Article = new ArticleNouvelleLigneMobile();
			article.sousTete = PanelGestionFoltteMobile.objLigneSelected.LIGNE;
			article.employe = {	IDEMPLOYE:	PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE
								,NOMPRENOM: PanelGestionFoltteMobile.objLigneSelected.COLLABORATEUR};
			 var _ppRenouvellement:RenouvelerContratIHM = new RenouvelerContratIHM();
			_ppRenouvellement.boolCreate = true;
			_ppRenouvellement.commande = commande; //la nouvelle commande 
			_ppRenouvellement.article = article; // les articles de la commande
			_ppRenouvellement.contrat = contrat; // le contrat a renouveler
			addPopup(_ppRenouvellement);  
			
		}
		
		private function newPanelGestionOptions(event : MouseEvent):void
		{
			var contrat:Contrat = new Contrat();
			
			contrat.IDCONTRAT = PanelGestionFoltteMobile.objLigneSelected.IDCONTRAT_ABO;
			contrat.NUMERO_CONTRAT = PanelGestionFoltteMobile.objLigneSelected.NUM_CONTRAT_ABO;
			contrat.IDSOUS_TETE = PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE;
			contrat.SOUS_TETE = PanelGestionFoltteMobile.objLigneSelected.LIGNE;
					
			var commande:Commande = new Commande(true);
			commande.TYPE_OPERATION = "Modification d'options";
			commande.IDTYPE_COMMANDE = TypesCommandesMobile.MODIFICATION_OPTIONS;  
			commande.IDPOOL_GESTIONNAIRE = PanelGestionFoltteMobile.idPoolGestionnaire;		
			commande.LIBELLE_POOL = panelGestionFlotteMobile.comboPoolGestionnaire.selectedLabel;		
			commande.IDREVENDEUR = PanelGestionFoltteMobile.objLigneSelected.IDDISTRIBUTEUR;
			commande.LIBELLE_REVENDEUR = PanelGestionFoltteMobile.objLigneSelected.DISTRIBUTEUR;
			
			
			var article:Article = new Article();
			article.sousTete = PanelGestionFoltteMobile.objLigneSelected.LIGNE;
			
			var _ppGererOptions:GererOptionsAbonnementIHM = new GererOptionsAbonnementIHM();
			_ppGererOptions.boolCreate = true;
			_ppGererOptions.commande = commande;
			_ppGererOptions.article = article;
			_ppGererOptions.contrat = contrat;
			addPopup(_ppGererOptions);
			
		}
		
		private function newPanelEnConstruction(event : MouseEvent):void
		{
			addPopup(new panelEnconstruction());
		}
		private function newPanelRebus(event : MouseEvent):void
		{
			addPopup(new RebusLigne(PanelGestionFoltteMobile.objLigneSelected));
		}
		private function newPanelListeUser(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE==null){
				if(PanelGestionFoltteMobile.objLigneSelected.IDSIM > 0){
					addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImg,"carte SIM"),
									new AffecterSimCollab(),
									new DispoCollab()
									));
				}
				else
				{
					Alert.show("Une ligne sans carte SIM ne peut pas être associée à un collaborateur","Impossible d'affecter la ligne"); 
				}
			}
			else
			{
				Alert.show("La ligne séléctionnée est déja affectée à un collaborateur","Impossible d'associer cette ligne à un collaborateur");
			}
		}
		private function newPanelListeSIM(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDSIM > 0){
				Alert.show("La ligne sélectionnée est déja affectée à une carte SIM","Impossible d'associer cette ligne à une carte SIM");	
			}
			else
			{
				
				addPopup(new PanelAffectation(
									new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE),PanelGestionFoltteMobile.objLigneSelected.LIGNE,adrImg,"ligne"),
									new AffecterSimLigne(),
									new DispoSim(false),//false pour ne pas afficher les lignes
									false,
									false,
									false
									
									));
			}
			
		}
		private function newPanelListeTerm(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL==null){
				if(PanelGestionFoltteMobile.objLigneSelected.IDSIM>0){
					//addPopup(new NDIAffectation (PanelGestionFoltteMobile.objLigneSelected,2));
				}
				else
				{
					Alert.show("Une ligne sans carte SIM ne peut pas être associée à un terminal","Impossible d'affecter la ligne"); 
				}
			}
			else
			{
				Alert.show("La ligne sélectionnée est déja affectée à un terminal","Impossible d'associer cette ligne à un terminal");
			}
		
		}
		private function newPanelUserOfSIM(event : MouseEvent):void
		{
			if(PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE!=null)
			{
				//addPopup(new NDIDesaffectation(PanelGestionFoltteMobile.objLigneSelected,1));
			}
			else
			{
				Alert.show("La ligne sélectionnée n'est pas affectée à un collaborateur","Impossible de rendre la SIM");
			}		
		}
		private function newPanelSIMOfLigne(event : MouseEvent):void
		{
			var idLigne  : int= PanelGestionFoltteMobile.objLigneSelected.IDSIM;
			if(idLigne > 0)
			{
				[Embed(source="/assets/equipement/SIM50x50.png")] 
				var adrImgSim : Class;
				addPopup(new PanelDesaffectation(
					new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSOUS_TETE),PanelGestionFoltteMobile.objLigneSelected.LIGNE,adrImg,"la ligne"),
					new Cellule(Number(PanelGestionFoltteMobile.objLigneSelected.IDSIM),PanelGestionFoltteMobile.objLigneSelected.NUM_SIM,adrImgSim,"la carte SIM"),
					new DesaffecterSIMOfLigne()
					));
			}
			else
			{
				Alert.show("La ligne sélectionné n'est pas liée à une SIM","Impossible de disscocier la ligne de la SIM");
			}		
		}
		private function newPanelTermOfSIM(event : MouseEvent):void
		{
			
			if(PanelGestionFoltteMobile.objLigneSelected.IDTERMINAL!=null)
			{
				//addPopup(new NDIDesaffectation(PanelGestionFoltteMobile.objLigneSelected,2));
			}
			else
			{
				Alert.show("La ligne séléctionnée n'est pas liée à un terminal","Impossible de rendre la SIM");
			}		
		}
		private function newPanelActivation(evt : MouseEvent):void
		{
			//addPopup(new PanelActivationLigne());
		}
		private function addPopup(popup : TitleWindow):void {
			this.popup=popup;
			popup.addEventListener(PanelGestionFoltteMobile.VALIDER_EVENT,closePopUp); // Si il y a une validation
			popup.addEventListener(PanelGestionFoltteMobile.ANNULER_EVENT,closePopUp); // Si il y a une annulation
									
			PopUpManager.addPopUp(popup,panelGestionFlotteMobile,true);
			PopUpManager.centerPopUp(popup);
		}
		private function closePopUp(evt : Event):void {
			if(evt.type == PanelGestionFoltteMobile.ANNULER_EVENT)
			{
				PopUpManager.removePopUp(popup); 
			}
			else if(evt.type == PanelGestionFoltteMobile.VALIDER_EVENT) 
			{
				panelGestionFlotteMobile.actualise(); 
				PopUpManager.removePopUp(popup); 
			}
		}
		
		protected function _btGetHistoriqueClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new MouseEvent(BT_HISTORIQUE_CLIKED));
		}
	  
		
	}
}
