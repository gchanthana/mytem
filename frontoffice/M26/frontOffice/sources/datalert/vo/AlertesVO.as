package datalert.vo
{
	import mx.collections.ArrayCollection;

	public class AlertesVO
	{
		public var seuils:Array = new Array();//contient 1 à 6 AlertesSeuilVO
		
		/*public var seuils:Object = {
			"type_periode":"2",
			"is_roaming":1,
			"limit":1024,
			"type_alerte":"U",
			"idmessage_user":1456,
			"send_message_gest":0,
			"disable_data":1
		};*/

		public var controle_acces:AlertesControleAccesVO = new AlertesControleAccesVO();
		public var controle_data:AlertesControleDataVO = new AlertesControleDataVO();
		
		public function AlertesVO()
		{
		}	
		public function addToSeuils(type_periode:Number, is_roaming:Number, limit:Number, type_alerte:String,
									idmessage_user:Number, send_message_gest:Number, disable_data:Number):void
		{
			this.seuils[seuils.length] = {
				"type_periode":type_periode,
				"is_roaming":is_roaming,
				"limit":limit,
				"type_alerte":type_alerte,
				"idmessage_user":idmessage_user,
				"send_message_gest":send_message_gest,
				"disable_data":disable_data
			};
		}
		public function fillControleAcces(is_actif:Number, delai:Number, idmessage_user:Number, send_message_gest:Number,
										   repeat:Number):void
		{//controleAcces = reactivation
			this.controle_acces.is_actif = is_actif;
			this.controle_acces.delai = delai;
			this.controle_acces.idmessage_user = idmessage_user;
			this.controle_acces.send_message_gest = send_message_gest;
			this.controle_acces.repeat = repeat;
		}
		public function fillControleData(is_actif:Number, idmessage_user:Number, send_message_gest:Number, disable_data:Number, repeat:Number):void
		{//controleData = sans info
			this.controle_data.is_actif = is_actif;
			this.controle_data.idmessage_user = idmessage_user;
			this.controle_data.send_message_gest = send_message_gest;
			this.controle_data.disable_data = disable_data;
			this.controle_data.repeat = repeat;
		}
	}
}