package rapport.vo
{
	public class Parametre
	{
		private var _cle:String; // répresente le nom du paramètre
		private var _typeValue:String;
		private var _value:Array;
		private var _is_system:Boolean;
		private var _libelle:String; // c'est le libelle de la clé 
		
		public function Parametre(){}

		public function fill(parametre:Object):void{}
		
		
		public function get is_system():Boolean
		{
			return _is_system;
		}
		
		public function set is_system(value:Boolean):void
		{
			_is_system = value;
		}
		
		public function get value():Array
		{
			return _value;
		}
		
		public function set value(value:Array):void
		{
			_value = value;
		}
		
		public function get typeValue():String
		{
			return _typeValue;
		}
		
		public function set typeValue(value:String):void
		{
			_typeValue = value;
		}
		
		public function get cle():String
		{
			return _cle;
		}
		
		public function set cle(value:String):void
		{
			_cle = value;
		}
		
		public function get libelle():String
		{
			return _libelle;
		}
		
		public function set libelle(value:String):void
		{
			_libelle = value;
		}
	}
}