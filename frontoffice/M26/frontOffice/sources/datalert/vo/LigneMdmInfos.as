package datalert.vo
{
	import flash.utils.describeType;
	
	import mx.core.UIComponent;

	[Bindable]
	public dynamic class LigneMdmInfos
	{
		internal var bool_updated:Boolean= false;
		private var _parent:LigneVO;
		
		private var _OPERATEUR:String = "";
		private var _MARQUE:String = "";
		private var _MODELE:String = "";
		private var _NUM_SERIE:String = "";
		private var _NUM_SIM:String = "";
		private var _IMEI:String = ""; 
		private var _PLATFORM:String = "";
		private var _MDM_USER:String = "";

		
		public function get IMEI():String
		{
			return _IMEI;
		}

		private function set IMEI(value:String):void
		{
			_IMEI = value;
		}

		public function get NUM_SERIE():String
		{
			return _NUM_SERIE;
		}

		private function set NUM_SERIE(value:String):void
		{
			_NUM_SERIE = value;
		}
		
		public function get NUM_SIM():String
		{
			return _NUM_SIM;
		}
		
		private function set NUM_SIM(value:String):void
		{
			_NUM_SIM = value;
		}

		public function get MODELE():String
		{
			return _MODELE;
		}

		private function set MODELE(value:String):void
		{
			_MODELE = value;
		}

		public function get MARQUE():String
		{
			return _MARQUE;
		}

		public function set MARQUE(value:String):void
		{
			_MARQUE = value;
		}

		public function get OPERATEUR():String
		{
			return _OPERATEUR;
		}

		private function set OPERATEUR(value:String):void
		{
			_OPERATEUR = value;
		}
		
		public function get PLATFORM():String
		{
			return _PLATFORM;
		}
		
		private function set PLATFORM(value:String):void
		{
			_PLATFORM = value;
		}
		
		

		public function get parent():LigneVO
		{
			return _parent;
		}
		 
		
		
				 
		
		
		public function LigneMdmInfos(parent:LigneVO)
		{
			if(parent != null)
			{
				_parent = parent;	
			}
			else
			{
				throw Error("orphan : parent is null");
			}
			
		}
		
		public function displayInfos(IdisplayerSource:UIComponent):void
		{  
			var txt:String = 
				 "<strong><font color='#FFFFFF' face='Arial'><p>" + this.MARQUE + " " + this.MODELE + "</p></font></strong>|"
				+"<textformat indent='4' tabstops='[30,40]'><font color='#FFFFFF' face='Arial' size='14'>"
				+"<p>OS\t:\t" + this.PLATFORM + "</p>"
				+"<p>SN\t:\t" + this.NUM_SERIE + "</p>"
				+"<p>SIM\t:\t" + this.NUM_SIM + "</p>"
				+"<p>OP.\t:\t" + this.OPERATEUR + "</p>"
				+"</font></textformat>";
					
			 
				
			IdisplayerSource.toolTip = txt;
			IdisplayerSource.invalidateDisplayList();
				
		}
		
		public function fill(values:Object):Boolean
		{	
			for (var propertie:* in values) 
			{	
				this[propertie]=(values[propertie] == null)?' ':values[propertie];	
				trace(propertie + " "+ this[propertie]);				 
			}
			
			var description:XML = describeType(values);
			for each (var a:XML in description.accessor) 
			{	
				this[a.@name]=(values[a.@name] == null)?' ':values[a.@name];	
				trace(a.@name  + " "+ this[	a.@name]);				 
			}
			bool_updated =true;
			return true;
		}
		
		 
	}
}