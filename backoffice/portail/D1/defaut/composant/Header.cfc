<cfcomponent extends="portail.class.specialClass.AbstractComposant"
			 implements="portail.class.specialClass.InterfaceComposant"
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>
	
	<!---  DIV HEADER ALIAS MENU --->
  	<cffunction name="getHeader" access="public" output="false" returntype="string"
				displayname="" 
				hint="Génère la div de header contenant le menu / composant récurrent / template D1" >
		
		<cfargument name="idPage" required="false" type="numeric" default=2><!--- default=2 est par defaut page accueil activé --->
		<cfargument name="idSous_tete" required="false" type="numeric" default=0>
		<cfargument name="sous_tete" required="false" type="string" default="0">
		
		<!--- INITIALISATION VARIABLE ONGLET --->
		<cfset ongletUn = "">
		<cfset ongletDeux = "">
		<cfset ongletTrois = "">
		<cfset ongletQuatre = "">
		
		<!--- POUR SPÉCIFIER L'ONGLET ACTIF --->
		<cfset currentAcc = "">
		<cfset currentFac = "">
		<cfset currentApp = "">
		<cfset currentTerm = "">
		
		<cfif idPage eq 2>
			<cfset currentAcc = "current">
		<cfelseif idPage eq 3>
			<cfset currentFac = "current">
		<cfelseif idPage eq 4>
			<cfset currentApp = "current">
		<cfelseif idPage eq 5>
			<cfset currentTerm = "current">
		</cfif>
		
		
		<!--- GET ID_EMPLOYE --->
		<cfif NOT isDefined("SESSION.IDEMPLOYE")>
			<cfset tab = createObject("component","portail.class.classData.DataService").getIdEmploye(SESSION.USER.EMAIL,SESSION.USER.CLIENTID)>
			<cfset x = tab["IDEMPLOYE"]>
			<cfset SESSION.IDEMPLOYE = val(x)>
		</cfif>

		
		<!--- STRUCT POUR CREER UNE URL --->
		<cfset struct0 = {idPage = "2"} >
		<cfset urlString0 = createRefURL(struct0)>
		<cfset struct1 = {logOut ="0"} >
		<cfset urlString1 = createRefURL(struct1)>
		
		<!--- page accueil Portail complète --->
		<cfset struct2 = {idPage = "2", idSous_tete = "#idSous_tete#", sous_tete = "#sous_tete#"} >
		<cfset urlString2 = createRefURL(struct2)>
		
		<!--- page accueil MDM --->
		<cfset struct2MDM = {idPage = "2MDM", idSous_tete = "#idSous_tete#", sous_tete = "#sous_tete#"} >
		<cfset urlString2MDM = createRefURL(struct2MDM)>
		
		<!--- page mes factures --->
		<cfset struct3 = {idPage = "3", idSous_tete = "#idSous_tete#", sous_tete = "#sous_tete#"} >
		<cfset urlString3 = createRefURL(struct3)>
		
		<!--- page mes appels --->
		<cfset struct4 = {idPage = "4", idSous_tete = "#idSous_tete#", sous_tete = "#sous_tete#"} >
		<cfset urlString4 = createRefURL(struct4)>
		
		<!--- page mon terminal --->
		<cfset struct5 = {idPage = "5", idSous_tete = "#idSous_tete#", sous_tete = "#sous_tete#"} >
		<cfset urlString5 = createRefURL(struct5)>
		
		
		<!---  FORMAT DATE FRANCAIS --->
		<cfset oldlocale = SetLocale("French (Standard)")>
		<!---  MAJUSCULES SUR LA PREMIERE LETTRE DU JOUR DE LA DATE --->
		<cfset date = LSDateFormat(Now(), "dddd dd mmmm yyyy") >
		<cfset dateFL = UCase(Left(date, 1)) >
		<cfset SDdate = Right(date,len(date)-1)>
		
		
		<cfif SESSION.GSTDROITS.pilotageFinancier eq true && SESSION.GSTDROITS.droitsMDM eq false >
			<cfset ongletUn = '<li><a class="#currentAcc#" title="Accueil" onclick="loadsablier();" href="'&urlString2&'">Accueil</a></li>' >
			<cfset ongletDeux = '<li><a class="#currentFac#" title="Mes factures" onclick="loadsablier();" href="'&urlString3&'">Mes factures</a></li>' >
			<cfset ongletTrois = '<li><a class="#currentApp#" title="Mes appels" onclick="loadsablier();" href="'&urlString4&'">Mes appels</a></li>' >
		</cfif>
		
		<cfif SESSION.GSTDROITS.droitsMDM eq true && SESSION.GSTDROITS.pilotageFinancier eq false >
			<cfset ongletUn = '<li><a class="#currentAcc#" title="Accueil" onclick="loadsablier();" href="'&urlString2&'">Accueil</a></li>' >
			<cfset ongletQuatre = '<li><a class="#currentTerm#" title="Mon terminal" onclick="loadsablier();" href="'&urlString5&'">Mon terminal</a></li>' >
		</cfif>
		
		<cfif SESSION.GSTDROITS.droitsMDM eq true && SESSION.GSTDROITS.pilotageFinancier eq true >

			<cfset ongletUn = '<li><a class="#currentAcc#" title="Accueil" onclick="loadsablier();" href="'&urlString2&'">Accueil</a></li>' >
			<cfset ongletDeux = '<li><a class="#currentFac#" title="Mes factures" onclick="loadsablier();" href="'&urlString3&'">Mes factures</a></li>' >
			<cfset ongletTrois = '<li><a class="#currentApp#" title="Mes appels" onclick="loadsablier();" href="'&urlString4&'">Mes appels</a></li>' >
			<cfset ongletQuatre = '<li><a class="#currentTerm#" title="Mon terminal" onclick="loadsablier();" href="'&urlString5&'">Mon terminal</a></li>' >
		</cfif>
		
		
		<!--- MENU HTML --->
		<cfset header = '
				<div class="entete">
					<table width="100%" cellspacing="0" cellpadding="0" border="0" >
						<colgroup>
							<col width="400px">
							<col width="*">
							<col width="250px">
						</colgroup>
						<tbody>
							<tr >
								<td width="400px" style="vertical-align:middle;padding-top:10px;padding-left:10px;"><img style="display:block" border="0" src="img/mytem/bandeau_mytem360_transparent.png" alt="Logo MyTem360"></td>
								<td width="">&nbsp;</td>
								<td align="right" valign="top" class="">
									<table width="250px" height="80" cellspacing="0" cellpadding="0" border="0" class="infoUser">
										<colgroup>
											<col width="250px">
										</colgroup>
										<tbody>
											<tr class="">
												<td align="right" valign="top" class="login_ste">
													#dateFL##SDdate#
												</td>
											</tr>
											<tr class="">
												<td align="right" valign="top" class="login_ste">
													<nobr><div title="Bonjour&nbsp;#SESSION.USER.PRENOM#&nbsp;#SESSION.USER.NOM#" class="">Bonjour&nbsp;'&SESSION.USER.PRENOM&'&nbsp;'&SESSION.USER.NOM&'</div></nobr>
												</td>
											</tr>
											<tr class="">
												<td align="right" valign="top" class="login_ste">
													#SESSION.USER.LIBELLE#
												</td>
											</tr>
											<tr class="">
												<td align="right" valign="top" class="login_ste">
													<nobr>
														<a href="'&urlString1&'" style=""><span style="padding-right:5px;">Déconnexion</span><img width="14px" height="14px" border="0" alt="Deconnexion" style="vertical-align: middle;" src="img/deconnexion.png"></a>
													</nobr>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			<!--- SOUS MENU --->
			
				<div class="red">
					<div id="slatenav">
						<ul>
							#ongletUn#
							#ongletDeux#
							#ongletTrois#
							#ongletQuatre#
						</ul>
					</div>
				</div>

			' >
		<cfreturn header>
		
	</cffunction>
	
</cfcomponent>