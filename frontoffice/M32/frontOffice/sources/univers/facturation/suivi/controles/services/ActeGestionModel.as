package univers.facturation.suivi.controles.services
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import univers.facturation.suivi.controles.event.ActeGestionEvent;
	import univers.facturation.suivi.controles.vo.ActeGestionVO;
	
	[Bindable]
	public class ActeGestionModel extends EventDispatcher
	{
		public var acteGestion:ArrayCollection;
		
		public function ActeGestionModel()
		{
		}
		
		public function updateActeGestion(values:ArrayCollection):void
		{
			
			acteGestion = new ArrayCollection();
			
			var acteGest:ActeGestionVO;
			for(var i:int=0;i<values.length;i++){
				acteGest = new ActeGestionVO();
				acteGest.addActeGestion(values[i]);
				acteGestion.addItem(acteGest);
			}
			
			dispatchEvent(new ActeGestionEvent(ActeGestionEvent.ACTEGESTION_COMPELTE));
			
		}
	}
	
}