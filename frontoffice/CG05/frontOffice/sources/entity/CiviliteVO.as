package entity
{
	public class CiviliteVO
	{
		private var _id					:int;
		private var _libelleCivilite	:String;
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDCIVILITE"))
					this.id = obj.IDCIVILITE;	
				
				if(obj.hasOwnProperty("CIVILITE"))
					this.libelleCivilite = (obj.CIVILITE != null)?obj.CIVILITE:'';
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet CiviliteVO erroné ");
			}
			
		}
		public function CiviliteVO()
		{
			
		}
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
		public function get libelleCivilite():String { return _libelleCivilite; }
		
		public function set libelleCivilite(value:String):void
		{
			if (_libelleCivilite == value)
				return;
			_libelleCivilite = value;
		}
	}
}