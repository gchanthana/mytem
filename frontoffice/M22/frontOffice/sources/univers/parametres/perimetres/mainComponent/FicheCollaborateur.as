package univers.parametres.perimetres.mainComponent
{
    import composants.util.*;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;

    public class FicheCollaborateur extends FicheCollaborateurIHM
    {
        //private var _mainRef:main;
        private var _currentId:Number;
//		private var mySearch:GeoSearchTree;
        private var _ficheSiteWin:ArbreficheSite;
        private var _currentIdPhysique:Number;
        private var _currentIdEmp:Number;
        private var _empWin:ArbreEmplacement;

        public function FicheCollaborateur()
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        public function setId(id:Number):void
        {
            _currentId = id;
        }

        protected function initIHM(event:Event):void
        {
            this.visible = false;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "isEmploye", isEmployeResult);
            RemoteObjectUtil.callService(op, _currentId.toString());
            this.x = 100;
            this.y = 150;
            //dcDateEnter.data = new Date();
            //dcDateExit.data = new Date();
        }

        private function isEmployeResult(e:ResultEvent):void
        {
            if (Number(e.result) == 0)
            {
                Alert.show("Voulez vous transformer ce noeud en collaborateur ?", "Message", Alert.YES | Alert.NO, null, handleAlert, null, Alert.YES);
            }
            else
                showFicheCollaborateur();
        }

        private function handleAlert(e:Object):void
        {
            if (e.detail == Alert.YES)
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "createEmploye", createEmployeResult);
                RemoteObjectUtil.callService(op, _currentId.toString());
            }
            else
            {
                /*closeWin(new CloseEvent(""));*/
                dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
            }
        }

        private function createEmployeResult(e:ResultEvent):void
        {
            showFicheCollaborateur();
        }

        protected function showFicheCollaborateur():void
        {
            this.visible = true;
            fillFiche();
            btUpdate.addEventListener(MouseEvent.CLICK, updateFiche);
            btModifyFicheSite.addEventListener(MouseEvent.CLICK, onBtModifyFicheSite);
            btModifyFicheEmp.addEventListener(MouseEvent.CLICK, onBtModifyEmplacement);
            btAnnuler.addEventListener(MouseEvent.CLICK, closeWin);
            this.addEventListener(CloseEvent.CLOSE, closeWin);
        }

        protected function onBtModifyEmplacement(e:MouseEvent):void
        {
            _empWin = PopUpManager.createPopUp(this, ArbreEmplacement) as ArbreEmplacement;
            _empWin._idsitephysique = _currentIdPhysique.toString();
            //_empWin.myArbreEmplacement.getEmplacement(_currentIdPhysique.toString());
            _empWin.addEventListener(Event.CHANGE, empChanged);
            //_empWin.myArbreEmplacement.myEmp.myTree.addEventListener(Event.CHANGE, empChanged);	
//			_empWin.arbreSite.addEventListener("nodeChanged", empChanged);	 
            _empWin.x = this.x + this.width + 20;
            _empWin.y = this.y + 20;
        }

        protected function empChanged(e:Event):void
        {
            var item:Object = _empWin.myArbreEmplacement.myEmp.myTree.selectedItem;
            lbFicheEmp.text = item.@LABEL;
            _currentIdEmp = item.@VALUE;
        }

        protected function onBtModifyFicheSite(e:MouseEvent):void
        {
            /**/
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "getOrgaGeo", showFicheSiteWin);
            RemoteObjectUtil.callService(op, idGroupeMaitre.toString());
        }

        protected function showFicheSiteWin(e:ResultEvent):void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var orgageo:int = int(e.result);
            _ficheSiteWin = PopUpManager.createPopUp(this, ArbreficheSite) as ArbreficheSite;
            _ficheSiteWin._idgeo = orgageo.toString();
            //_ficheSiteWin.arbreSite.initDP(orgageo);
            _ficheSiteWin.arbreSite.addEventListener("treeSelectionChanged", ficheSiteChanged);
            _ficheSiteWin.x = this.x + this.width + 20;
            _ficheSiteWin.y = this.y;
        }

        protected function ficheSiteChanged(e:Event):void
        {
            _currentIdPhysique = _ficheSiteWin.arbreSite.getSelectedItem().@VALUE;
            lbFicheSite.text = _ficheSiteWin.arbreSite.getSelectedItem().@LABEL;
        }

        protected function closeWin(event:Event):void
        {
            PopUpManager.removePopUp(this);
            try
            {
                PopUpManager.removePopUp(_ficheSiteWin);
            }
            catch (e:Error)
            {
            }
        }

        protected function updateFiche(event:MouseEvent):void
        {
            var dateEnter:String = df.format(dcDateEnter.selectedDate);
            var dateExit:String = df.format(dcDateExit.selectedDate);
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "updateFicheCollaborateur_v3", updateFicheResult);
            RemoteObjectUtil.callService(op, _currentId.toString(), _currentIdPhysique.toString(), _currentIdEmp.toString(), 0, txtIdentifiant.text, cmbCivility.selectedIndex, txtNom.text, txtPrenom.text, txtEmail.text, txtFonction.text, txtStatut.text, txtCommentaire.text, Number(rbPublication.selectedValue), txtCodeInterne.text, txtReference.text, txtMatricule.text, rbSociete.selectedValue, dateEnter, dateExit, txtC1.text, txtC2.text, txtC3.text, txtC4.text);
        /*			RemoteObjectUtil.callService(op, _currentId.toString(), _currentIdPhysique.toString(), _currentIdEmp.toString(),
           0, txtIdentifiant.text,	cmbCivility.selectedIndex, txtNom.text,	txtPrenom.text,
         txtEmail.text, txtFonction.text,	txtStatut.text,	txtCommentaire.text);	*/
        }

        private function updateFicheResult(event:ResultEvent):void
        {
            var e:FicheCollaborateurEvent = new FicheCollaborateurEvent("updateLabel");
            e._label = /* cmbCivility.text + " " +  */ txtNom.text + " " + txtPrenom.text;
            dispatchEvent(e);
            dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
        }

        private function fillFiche():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "getFicheCollaborateur", getFicheResult);
            RemoteObjectUtil.callService(op, _currentId.toString());
        }

        private function getFicheResult(event:ResultEvent):void
        {
            //try
            //{
            var data:Object = ArrayCollection(event.result)[0];
            rbPublication.selectedValue = data.BOOL_PUBLICATION;
            txtPrenom.text = data.PRENOM;
            txtCommentaire.text = data.COMMENTAIRE;
            txtNom.text = data.NOM;
            _currentIdEmp = data.IDEMPLACEMENT;
            lbFicheEmp.text = data.LIBELLE_EMPLACEMENT;
            if (int(data.IDEMPLACEMENT) == 0)
                lbFicheEmp.text = "non défini";
            _currentIdPhysique = data.IDSITE_PHYSIQUE;
            lbFicheSite.text = data.NOM_SITE;
            if (int(data.IDSITE_PHYSIQUE) == 0)
                lbFicheSite.text = "non défini";
            if (data.DATE_ENTREE != null)
                dcDateEnter.data = data.DATE_ENTREE as Date;
            if (data.DATE_SORTIE != null)
                dcDateExit.data = data.DATE_SORTIE as Date;
            cmbCivility.selectedIndex = data.CIVILITE;
            txtFonction.text = data.FONCTION_EMPLOYE;
            txtCodeInterne.text = data.CODE_INTERNE;
            txtStatut.text = data.STATUS_EMPLOYE;
            txtEmail.text = data.EMAIL;
            rbSociete.selectedValue = data.INOUT;
            txtIdentifiant.text = data.CLE_IDENTIFIANT;
            txtReference.text = data.REFERENCE_EMPLOYE;
            txtMatricule.text = data.MATRICULE;
            txtC1.text = data.C1;
            txtC2.text = data.C2;
            txtC3.text = data.C3;
            txtC4.text = data.C4;
        /*lbAcessConsoViewModuleObject.text = "non";
           lbFicheSite.text = data.LIBELLE_PHYSIQUE;

           if (data.ACCES_ConsoViewModuleObject == 1)
         lbAcessConsoViewModuleObject.text = "oui";					*/ /*}
         catch (e:Error) {}*/
        }

        private function toBoolean(val:String):Boolean
        {
            if (val == "true")
                return true;
            return false;
        }
    }
}