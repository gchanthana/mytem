package univers.parametres.poolgestionnaire.ihm
{
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	
	import univers.parametres.poolgestionnaire.event.GestionProfilEvent;
	import univers.parametres.poolgestionnaire.event.GestionTypeCommandeEvent;
	import univers.parametres.poolgestionnaire.service.TypeCommandeService;
	
	[Bindable]
	public class ListeTypeCommandeLogin extends VBox
	{
		public var typeCommandeService : TypeCommandeService= new TypeCommandeService();
		
		
		public var idLogin : int;
		public var groupeIndex : int;
		
		//COMPONANT
		public var dg_pool_gestionnaire: DataGrid;
		public var cbAll : CheckBox;		
		
		public function ListeTypeCommandeLogin()
		{
			
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			typeCommandeService.addEventListener(GestionTypeCommandeEvent.UPDATE_TYPE_COMMANDE_LOGIN,update_type_commande_handler);
		}
		public function initData(id : int):void
		{
			idLogin = id;
			
			typeCommandeService.getTypeCommandeLogin(groupeIndex,idLogin);
		}
		public function onItemChanged(item : Object):void
		{
			var boolExiste : Boolean = false;
			for(var i:int=0;i<typeCommandeService.tab_typeCMD_of_login.length;i++)
			{
				if(typeCommandeService.tab_typeCMD_of_login[i]==item.idTypeCmd)
				{
					typeCommandeService.tab_typeCMD_of_login.removeItemAt(i);
					
					typeCommandeService.updateTypeCommandeOfLogin(idLogin,[item.idTypeCmd],0);
					
					boolExiste = true;
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				typeCommandeService.tab_typeCMD_of_login.addItem(item.idTypeCmd);
				typeCommandeService.updateTypeCommandeOfLogin(idLogin,[item.idTypeCmd],1);
			}
			typeCommandeService.tab_typeCMD_of_login.refresh();
		}
		private function update_type_commande_handler(evt : GestionTypeCommandeEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.UPDATE_TYPE_COMMANDE_LOGIN));	
		}
		
	}
}