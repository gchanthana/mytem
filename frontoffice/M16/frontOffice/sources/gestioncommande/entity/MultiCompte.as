package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	
	public class MultiCompte
	{
		public var SELECTED					:Boolean 			= false;					//compte selectionne ou non
		public var NUMBER_OF_LINES			:int 				= 0;						//nombre de ligne comprenant le meme compte
		public var IDCOMPTE_FACTURATION		:int 				= 0;						//id du compte
		public var COMPTE_FACTURATION		:String 			= "";						//nom du compte	
		public var LIGNES					:ArrayCollection 	= new ArrayCollection();	//Contenant le meme idcompte	
		public var MULTI_SOUS_COMPTE		:ArrayCollection 	= new ArrayCollection();	//Contenant le meme idcompte	

		public function MultiCompte()
		{
		}

	}
}