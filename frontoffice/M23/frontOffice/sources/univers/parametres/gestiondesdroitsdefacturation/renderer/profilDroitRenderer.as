package univers.parametres.gestiondesdroitsdefacturation.renderer
{
	import mx.containers.Box;
	import mx.controls.Image;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.listClasses.IDropInListItemRenderer;
	import mx.states.AddChild;
	import mx.states.SetStyle;

	public class profilDroitRenderer extends Box
		implements IDropInListItemRenderer
	
	{
		private var _listData:BaseListData;
		[Bindable("datachange")]
		public function get listData():BaseListData{
			return _listData;}
		public function set listData(value:BaseListData):void{
			_listData= value;}
		
		override protected function createChildren():void{
			super.createChildren();
			this.addChild(image);
		}
		
		
		private var image:Image;
		
		[Embed(source="/assets/gestiondroit/IMAGE8.gif")]
		public var ValidSymbol:Class;
		
		[Embed(source="/assets/gestiondroit/IMAGE9.gif")]
		public var InvalidSymbol:Class;

		public function profilDroitRenderer()
		{
			super();
			setStyle("horizontalAlign","center");
			setStyle("verticalAlign","middle");
			image= new Image()
		}
		
		
		
		
		override public function set data(value:Object):void{
			if(value != null && value != ""){
				super.data=value;
				
				var currentValue:String=listData.label;
				
				switch(currentValue){
					case "1":
						image.source= ValidSymbol;
						break;
					case "0":
						image.source= InvalidSymbol;
					default:
						image.source= InvalidSymbol;
				}
			}
		}
	}
}