package intranet.GestionProduit
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class CBTriOpe extends ComboBox
	{
		public var tmpOperateur:ArrayCollection=new ArrayCollection(['Tous']);
		
		public function CBTriOpe()
		{
			super();
			this.addEventListener(FlexEvent.INITIALIZE,init)
		}

// REMPLIR LA COMBOBOX PAR LA LISTE DES OPERATEURS		
		public function init(e:FlexEvent):void{
			if(!DataSample.TabOpeOk){				
				var tmp:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestionproduits.GestionProduits",
					"getListeOperateur",
					OpeOk,OpeNo);
				RemoteObjectUtil.callService(tmp);
				DataSample.TabOpeOk=true;
			}else{
				var dataSortField:SortField = new SortField();
            	dataSortField.name = "OPERATEUR";
            	var DataSort:Sort = new Sort();
            	DataSort.fields = [dataSortField];
				
				this.dataProvider=DataSample.TabOperateur;
				this.labelField="OPERATEUR";
			}
		}
		
		public function OpeOk(e:ResultEvent):void{
			var item:Object=new Object();
			item.OPERATEURID=0;
			item.OPERATEUR="** Tous **";
			
			var dataSortField:SortField = new SortField();
            dataSortField.name = "OPERATEUR";
            var DataSort:Sort = new Sort();
            DataSort.fields = [dataSortField];            
		
			DataSample.TabOperateur=e.result as ArrayCollection;
			DataSample.TabOperateur.addItemAt(item,0);
			DataSample.TabOperateur.sort=DataSort;
			DataSample.TabOperateur.refresh();

			this.dataProvider=DataSample.TabOperateur;
			this.labelField="OPERATEUR";			
		}
		
		public function OpeNo(e:FaultEvent):void{
			trace("Erreur CBTriOpe: "+e.message);
		}
	}
}