package composants.tb.ligne
{
	import composants.tb.ligne.entity.CriteresRechercheVO;
	import composants.tb.ligne.event.FicheEvent;
	import composants.tb.ligne.renderer.PaginateDatagridExtend;
	import composants.tb.ligne.services.FicheTicketService;
	import composants.tb.service.ExportService;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.Tile;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.utils.StringUtil;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	
	import searchpaginatedatagrid.SearchPaginateDatagrid;
	import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;

	[Bindable]
	public class FicheTicketImpl extends VBox
	{
		
		private var _ligne:Number;
		private var _facture:Number;
		private var _numero:String;
		
		private var _ficheTicket:ArrayCollection;
		private var _nbTotalItem:Number;
		
		private var serviceFicheTicket:FicheTicketService = new FicheTicketService();
		private var serviceExport:ExportService = new ExportService();
		
		public var params:CriteresRechercheVO=new CriteresRechercheVO();
		
		public var arrayUnite:ArrayCollection=new ArrayCollection();
		
		public var mySearchPaginateDatagrid:SearchPaginateDatagrid;
		public var myPaginatedatagrid:PaginateDatagridExtend;
		public var checkFiltre:HBox;
		public var myFooterGrid:DataGrid;
		
		
		public function FicheTicketImpl()
		{
		}

		public function get numero():String
		{
			return _numero;
		}

		public function set numero(value:String):void
		{
			_numero = value;
		}

		public function get nbTotalItem():Number
		{
			return _nbTotalItem;
		}

		public function set nbTotalItem(value:Number):void
		{
			_nbTotalItem = value;
		}

		public function get ficheTicket():ArrayCollection
		{
			return _ficheTicket;
		}

		public function set ficheTicket(value:ArrayCollection):void
		{
			_ficheTicket = value;
		}

		public function get facture():Number
		{
			return _facture;
		}

		public function set facture(value:Number):void
		{
			_facture = value;
		}

		public function get ligne():Number
		{
			return _ligne;
		}

		public function set ligne(value:Number):void
		{
			_ligne = value;
		}
		
		public function init():void
		{
			serviceFicheTicket.getFicheTicket(ligne,facture,params);
			serviceFicheTicket.model.addEventListener(FicheEvent.FICHETICKET,ficheTicketHandler);
		}
		
		private function ficheTicketHandler(ev:FicheEvent):void
		{
			ficheTicket = serviceFicheTicket.model.ficheTicket;
			nbTotalItem = Number(serviceFicheTicket.model.nbTotalItem);
			initPaginateDatagrid();
			getTotal();
			getUnite();
			myPaginatedatagrid.dataprovider.filterFunction = filtreGrid;
			myPaginatedatagrid.dataprovider.refresh();
		}
		
		protected function initPaginateDatagrid():void
		{
			if(myPaginatedatagrid!=null){
				myPaginatedatagrid.paginationBox.initPagination(nbTotalItem,params.limit);
				myPaginatedatagrid.nbTotalItem = nbTotalItem;
				myPaginatedatagrid.dgPaginate.headerHeight=40;
				myPaginatedatagrid.dgPaginate.wordWrap=true;
				myPaginatedatagrid.filtre = filtreGrid;
			}
		}
		
		private function getTotal():void
		{
			if(ficheTicket.length>0){
				myFooterGrid.columns[1].headerText = ConsoviewFormatter.formatNumber(ficheTicket.getItemAt(0).DUREE_FACTURE_TOTAL, 0);
				myFooterGrid.columns[3].headerText = moduleTableauDeBordE0IHM.formatNumberCurrency(ficheTicket.getItemAt(0).COUT_OP_TOTAL, 2);
				myFooterGrid.columns[4].headerText = moduleTableauDeBordE0IHM.formatNumberCurrency(ficheTicket.getItemAt(0).MONTANT_REMISE_TOTAL, 2);
				myFooterGrid.columns[5].headerText = moduleTableauDeBordE0IHM.formatNumberCurrency(ficheTicket.getItemAt(0).COUT_OP_REMISE_TOTAL, 2);
			}
		}
		
		private function getUnite():void
		{
			
			var obj:Object;
			var chek:CheckBox;
			
			if(ficheTicket.length>0){
				for(var i:int=0;i<ficheTicket.length;i++)
				{
					if(!isInArray(ficheTicket.getItemAt(i).UNITE_VOLUME,arrayUnite))
					{
						obj = new Object();
						obj.unite = ficheTicket.getItemAt(i).UNITE_VOLUME;
						obj.selected = true;
						arrayUnite.addItem(obj);
						
						chek = new CheckBox();
						chek.label = arrayUnite.getItemAt(arrayUnite.length-1).unite;
						chek.selected = true;
						chek.addEventListener(MouseEvent.CLICK,filtreChekHandler);
						checkFiltre.addChild(chek);
					}
				}
			}
			
		}
		
		private function filtreChekHandler(e:Event):void
		{
			actualiseUnite(e);
			myPaginatedatagrid.dataprovider.filterFunction = filtreGrid;
			myPaginatedatagrid.dataprovider.refresh();
		}
		
		protected function filtreGrid(ticket:Object):Boolean
		{
			var result:Boolean = filtreUnite(ticket) && filtreText(ticket);
			
			return result;
		}
		
		private function filtreUnite(ticket:Object):Boolean
		{
			
			var verif:Boolean=false;
						
			for(var i:int=0;i<arrayUnite.length;i++)
			{
				if((arrayUnite.getItemAt(i).selected)&&(ticket.UNITE_VOLUME==arrayUnite.getItemAt(i).unite))
				{
					verif=true;
				}
			}
						
			return verif;
			
		}
		
		/**
		 * appelée par filterFunction lorsque txtFiltre change
		 * Cette fonction recherche dans toutes les colonnes d'un dg qui ont un dataField != null le texte entré dans txtFiltre
		 * @param : item est un objet du dataprovider.
		 *
		 * */
		private function filtreText(item:Object):Boolean
		{
			for (var i:int = 0; i < myPaginatedatagrid.columns.length; i++)
			{
				if (myPaginatedatagrid.columns[i].dataField != null)
				{
					var propriete:String = (myPaginatedatagrid.columns[i] as DataGridColumn).dataField;
					if (item[propriete])
					{
						if (((item[propriete].toString()).toLowerCase()).search(myPaginatedatagrid.txtFiltre.text.toLowerCase()) != -1)
						{
							return true;
						}
					}
				}
			}
			return false;
		}
		private function actualiseUnite(event:Event):void
		{
			for(var i:int=0;i<arrayUnite.length;i++)
			{
				if(arrayUnite.getItemAt(i).unite==(event.currentTarget as CheckBox).label)
				{
					arrayUnite.getItemAt(i).selected = (event.currentTarget as CheckBox).selected;
				}
			}
		}
		
		private function isInArray(value:String,array:ArrayCollection):Boolean
		{
			
			var verif:Boolean=false;
			
			if(array.length!=0){
				for(var i:int=0;i<array.length;i++)
				{
					if(array.getItemAt(i).unite==value)
					{
						verif=true;
					}
				}
			}
			
			return verif;
			
		}
		
		protected function search(event:SearchPaginateDatagridEvent):void
		{
			
			var searchable:String=mySearchPaginateDatagrid.parametresRechercheVO.SEARCH_TEXT;
			var orderable:String=mySearchPaginateDatagrid.parametresRechercheVO.ORDER_BY;
			
			if (!searchable)
			{
				searchable=",";
			}
			
			if (!orderable)
			{
				orderable="cout op,DESC";
			}
			
			var ar1:Array = searchable.split(",");
			var ar2:Array = orderable.split(",");
			
			params.searchColonne = ar1[0];
			params.searchText = ar1[1];
			
			params.orderColonne = ar2[0];
			params.orderBy = ar2[1];
			
			params.offset=1;
			
			serviceFicheTicket.getFicheTicket(ligne,facture,params);
			
		}
		
		protected function exportDetailAppel():void
		{
			if(nbTotalItem!=0){
				serviceExport.getExportDetailAppel(ligne,numero,facture,params,nbTotalItem);
			}
		}
		
		protected function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			
			if (myPaginatedatagrid.currentIntervalle.indexDepart == 0)
			{
				params.offset=1;
			}
			else
			{
				params.offset=myPaginatedatagrid.currentIntervalle.indexDepart;
			}
			
			serviceFicheTicket.getFicheTicket(ligne,facture,params);
			
		}
		
		protected function formateDate(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return DateFunction.formatDateAsString(item[column.dataField]);
			else
				return "-";
		}
		
		protected function formateNumber(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
			else
				return "-";
		}
		
		protected function formateEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return moduleTableauDeBordE0IHM.formatNumberCurrency(item[column.dataField], 2);
			else
				return "-";
		}

	}
}