package composants.tb.tableaux.surtheme
{
	import composants.util.ConsoviewFormatter;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.utils.ObjectUtil;
	
	public class SurThemeAbonnement
	{
		
		public var surThemeAbonnementFixe		:ArrayCollection = new ArrayCollection();
		public var surThemeAbonnementMobile		:ArrayCollection = new ArrayCollection();
		public var surThemeAbonnementData		:ArrayCollection = new ArrayCollection();
				
		
		public function SurThemeAbonnement()
		{
			themeLoaded(CvAccessManager.getSession().INFOS_DIVERS.SURTHEME);
		}
		
		private function themeLoaded(surTheme:ArrayCollection):void
		{
			surThemeAbonnementFixe	= getSurThemeAbonnementFixe(surTheme);
			surThemeAbonnementMobile= getSurThemeAbonnementMobile(surTheme);
			surThemeAbonnementData	= getSurThemeAbonnementData(surTheme);
		}

		private function getSurThemeAbonnementFixe(values:ArrayCollection):ArrayCollection
		{
			var fixe	:ArrayCollection = new ArrayCollection()
			var thm		:Object;
			var len		:int = values.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(values[i].IDSEGMENT_THEME == 1 && values[i].IDTYPE_THEME == 1)
				{
					thm = new Object();
					thm.LIBELLE 			= values[i].LIBELLE_SUR_THEME ;
					thm.SEGMENT 			= values[i].LIBELLE_SEGMENT_THEME;
					thm.TYPE 				= "SURTHEME";
					thm.LIBELLE_SURTHEME 	= values[i].LIBELLE_SUR_THEME;
					thm.IDSEGMENT_THEME 	= values[i].IDSEGMENT_THEME;
					thm.IDTYPE_THEME 		= values[i].IDTYPE_THEME;
					thm.IDSUR_THEME 		= values[i].IDSUR_THEME;
					thm.ORDRE_AFFICHAGE		= values[i].ORDRE_AFFICHAGE;
					thm.ID_LIBELLE 			= values[i].ORDRE_AFFICHAGE;
					thm.QUANTITE			= "0";
					thm.NBAPPELS			= "0";
					thm.MONTANT_TOTAL		= ConsoviewFormatter.formatNumber(0,2);
					
					fixe.addItem(thm);
				}
			}
			
			return sortFunction(fixe, "ORDRE_AFFICHAGE");
		}
		
		private function getSurThemeAbonnementMobile(values:ArrayCollection):ArrayCollection
		{
			var mobile	:ArrayCollection = new ArrayCollection()
			var thm		:Object;
			var len		:int = values.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(values[i].IDSEGMENT_THEME == 2 && values[i].IDTYPE_THEME == 1)
				{
					thm = new Object();
					thm.LIBELLE 			= values[i].LIBELLE_SUR_THEME ;
					thm.SEGMENT 			= values[i].LIBELLE_SEGMENT_THEME;
					thm.TYPE 				= "SURTHEME";
					thm.LIBELLE_SURTHEME 	= values[i].LIBELLE_SUR_THEME;
					thm.IDSEGMENT_THEME 	= values[i].IDSEGMENT_THEME;
					thm.IDTYPE_THEME 		= values[i].IDTYPE_THEME;
					thm.IDSUR_THEME 		= values[i].IDSUR_THEME;
					thm.ORDRE_AFFICHAGE		= values[i].ORDRE_AFFICHAGE;
					thm.ID_LIBELLE 			= values[i].ORDRE_AFFICHAGE;
					thm.QUANTITE			= "0";
					thm.NBAPPELS			= "0";
					thm.MONTANT_TOTAL		= ConsoviewFormatter.formatNumber(0,2);
					
					mobile.addItem(thm);
				}
			}
			
			return sortFunction(mobile, "ORDRE_AFFICHAGE");
		}
		
		private function getSurThemeAbonnementData(values:ArrayCollection):ArrayCollection
		{
			var data	:ArrayCollection = new ArrayCollection()
			var thm		:Object;
			var len		:int = values.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(values[i].IDSEGMENT_THEME == 3 && values[i].IDTYPE_THEME == 1)
				{
					thm = new Object();
					thm.LIBELLE 			= values[i].LIBELLE_SUR_THEME ;
					thm.SEGMENT 			= values[i].LIBELLE_SEGMENT_THEME;
					thm.TYPE 				= "SURTHEME";
					thm.LIBELLE_SURTHEME 	= values[i].LIBELLE_SUR_THEME;
					thm.IDSEGMENT_THEME 	= values[i].IDSEGMENT_THEME;
					thm.IDTYPE_THEME 		= values[i].IDTYPE_THEME;
					thm.IDSUR_THEME 		= values[i].IDSUR_THEME;
					thm.ORDRE_AFFICHAGE		= values[i].ORDRE_AFFICHAGE;
					thm.ID_LIBELLE 			= values[i].ORDRE_AFFICHAGE;
					thm.QUANTITE			= "0";
					thm.NBAPPELS			= "0";
					thm.MONTANT_TOTAL		= ConsoviewFormatter.formatNumber(0,2);
					
					data.addItem(thm);
				}
			}
			
			return sortFunction(data, "ORDRE_AFFICHAGE");
		}
		
		private function sortFunction(value:ArrayCollection, fieldname:String):ArrayCollection
	    { 
	    	var sortField:SortField = new SortField(fieldname,false,false,true);
           	var tri:Sort = new Sort();
	    		tri.fields 	 = [sortField];
	    		

			value.sort 	 = tri;
			value.refresh();
			value.sort = null;
			
			return value;
	    }	    

	}
}