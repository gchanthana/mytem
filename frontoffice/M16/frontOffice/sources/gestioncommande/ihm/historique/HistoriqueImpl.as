package gestioncommande.ihm.historique
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.TypeCommandeEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.popup.PopupChoosePoolIHM;
	import gestioncommande.services.DataGridDataExporter;
	import gestioncommande.services.PoolService;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class HistoriqueImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cbxPools						:ComboBox;
		public var cbxDepuis					:ComboBox;
		public var cbxType						:ComboBox;
		public var cbxPar						:ComboBox;
		
		public var txtClef						:TextInput;
		public var txtFiltre					:TextInput;
		
		public var dcDebut						:CvDateChooser;
		public var dcFin						:CvDateChooser;
		
		public var rdbtnTous					:RadioButton;
		public var rdbtnCent					:RadioButton;
		
		public var dgListHistorique				:DataGrid;
		private var _popUpChoosePool			:PopupChoosePoolIHM;
		private var _poolSelected				:Boolean = false;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		private var _myCommande					:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
	
		private var _wflwSrv					:WorkflowService = new WorkflowService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listeHistoPool				:ArrayCollection 	= new ArrayCollection();
		public var listeDepuis					:ArrayCollection 	= new ArrayCollection();

		public var dateFinReference				:Date 				= new Date();

		private var _listeDepuis				:ArrayCollection = new ArrayCollection([	{label:ResourceManager.getInstance().getString('M16','Tous'), value:-1},
																							{label:ResourceManager.getInstance().getString('M16','24_heures'), value:1},
																							{label:ResourceManager.getInstance().getString('M16','1_semaine'), value:7},
																							{label:ResourceManager.getInstance().getString('M16','1_mois'),    value:31}
																						]);

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function HistoriqueImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}

		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//

		protected function cbxPoolsChangeHandler(e:Event):void
		{
			if(cbxPools.selectedItem != null  && dcDebut.selectedDate != null && dcFin.selectedDate != null)
				getHistoriquePool(false);
		}
		
		protected function cbxChangeHandler(e:Event):void
		{
			filterData();
		}

		protected function btnRetourClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new ViewStackEvent(ViewStackEvent.VS_VIEW_LISTECMDES, true));
		}
		
		protected function btnSearchClickhandler(me:MouseEvent):void
		{
			if(dcDebut.selectedDate != null && dcFin.selectedDate != null)
			{
				var idx:int = ObjectUtil.dateCompare(dcDebut.selectedDate,dcFin.selectedDate);
				
				if(idx < 1)
					getHistoriquePool(false);
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Votre_date_de_d_but_doit__tre_ult_rieure'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_date_de_d_but_e'), 'Consoview');
		}
		
		protected function btnExporterClickHandler(me:MouseEvent):void
		{
			exportCSVLignes();
		}
		
		protected function btnReinitClickHandler(me:MouseEvent):void
		{
			txtFiltre.text = '';
			
			filterData();
		}

		protected function txtFiltreChangeHandler(e:Event):void
		{
			filterData();
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
		}
		
		private function showHandler(fe:FlexEvent):void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function historiquePoolHandler(cmde:CommandeEvent):void
		{
			listeHistoPool = _wflwSrv.listeHistoPool;

			attributDataToCombobox(cbxDepuis, _listeDepuis, 'Aucun');
			attributDataToCombobox(cbxType, _wflwSrv.listeHistoType, 'Aucune action', 'ACTION', 'IDACTION');
			attributDataToCombobox(cbxPar, _wflwSrv.listeHistoUser, 'Aucune Gestionnaire', 'QUI', 'APP_LOGINID');
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function setPoolsGestionnaire():void
		{
			var pools:ArrayCollection = trierLesPools(ObjectUtil.copy(SessionUserObject.singletonSession.USERPOOLS) as ArrayCollection);
			
			attributDataToCombobox(cbxPools, pools, 'Aucun pools');
			
			if(pools.length == 1)
				getHistoriquePool();
		}
		
		private function trierLesPools(myPools:ArrayCollection):ArrayCollection
		{
			var _arrayPools:Array = myPools.toArray();
			_arrayPools.sortOn("LIBELLE_POOL", Array.CASEINSENSITIVE);
			myPools.source = _arrayPools;
			myPools.refresh();
			return myPools;
		}
		
		private function getHistoriquePool(boolSilent:Boolean = true):void
		{
			listeHistoPool.removeAll();

			_wflwSrv.getHistoriquePool(cbxPools.selectedItem.IDPOOL, dcDebut.selectedDate, dcFin.selectedDate, txtClef.text, boolSilent);
			_wflwSrv.addEventListener(CommandeEvent.LISTED_POOL_HIST, historiquePoolHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//

		private function exportCSVLignes():void 
		{
			if(listeHistoPool != null && listeHistoPool.length > 0) 
			{	
				var dataToExport	:String = DataGridDataExporter.getCSVString(dgListHistorique,";","\n",false);
				var url				:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/gridexports/exportCSVString.cfm";
				
				DataGridDataExporter.exportCSVString(url, dataToExport, "export");
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es___exporter__'), 'Consoview', null);
		}
		
		private function attributDataToCombobox(myCbx:ComboBox, dataValues:ArrayCollection, myError:String, datafield:String = '', value:String = ''):void 
		{
			var lenDataValues:int = dataValues.length;
			
			myCbx.errorString = "";

			if(lenDataValues != 0)
			{
				if(datafield != '')
				{
					addFirstItem(datafield, value, dataValues);
					
					lenDataValues = dataValues.length;
				}
				
				myCbx.prompt 				= '';	
				myCbx.dataProvider 			= dataValues;
				myCbx.dropdown.dataProvider = dataValues;
				myCbx.selectedIndex 		= 0;
				
				if(lenDataValues > 5)
					myCbx.rowCount = 5;
				else
					myCbx.rowCount = lenDataValues;
				
				(myCbx.dataProvider as ArrayCollection).refresh();
			}
			else
				myCbx.prompt = myError;
		}
		
		private function addFirstItem(datafield:String, value:String, items:ArrayCollection):void
		{
			var myObject:Object 	= new Object();
				myObject[datafield] = ResourceManager.getInstance().getString('M16', 'Tous');
				myObject[value] 	= -1;
			
			items.addItemAt(myObject, 0);
		}
		
		public function btImgChoosePool_clickHandler(e:Event):void
		{
			var pools:ArrayCollection = cbxPools.dataProvider as ArrayCollection;
			this.majSelectPool(pools);
			_popUpChoosePool = new PopupChoosePoolIHM();
			var point:Point = new Point(stage.mouseX,stage.mouseY);
			_popUpChoosePool.x = point.x - cbxPools.width; 
			_popUpChoosePool.y = point.y;
			_popUpChoosePool.listePool = pools;
			if(_poolSelected == false)
				pools[0].SELECTED = true;
			_popUpChoosePool.addEventListener('VALID_CHOIX_POOL', getItemPoolHandler);
			PopUpManager.addPopUp(_popUpChoosePool, this, true);
		}
		
		private function majSelectPool(_pools:ArrayCollection):void
		{
			var longPool:int = _pools.length;
			var i:int = 0;
			while(i < longPool)
			{
				_pools[i].SELECTED = false;
				if(_pools[i].IDPOOL == cbxPools.selectedItem.IDPOOL)
				{
					_pools[i].SELECTED = true;
					_poolSelected = true;
				}
				i++;
			}
		}
		
		protected function getItemPoolHandler(e:Event):void
		{
			_popUpChoosePool.removeEventListener('VALID_CHOIX_POOL', getItemPoolHandler);
			cbxPools.selectedItem = _popUpChoosePool.itemCurrent;
			cbxPoolsChangeHandler(null);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
		
		private function filterData():void
		{
			if(dgListHistorique.dataProvider != null)
			{
				(dgListHistorique.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListHistorique.dataProvider as ArrayCollection).refresh();
			}
		}
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && ( 
										(item.DATE_EFFET != null && item.DATE_EFFET.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1) ||
										(item.ACTION != null && item.ACTION.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1) ||
										(item.DESC_ACTION != null && item.DESC_ACTION.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1) ||
										(item.QUI != null && item.QUI.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1) ||
										(item.DATE_ACTION != null && item.DATE_ACTION.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)||
										(item.COMMANDE != null && item.COMMANDE.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)||
										(item.SAV != null && item.SAV.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)
									)
				
				&& searchDepuis(item) && searchType(item) && searchPar(item);
			
			return rfilter;
		}
		
		
		private function searchDepuis(item:Object):Boolean
		{
			var OK:Boolean = true;
			
			if(cbxDepuis.selectedItem != null && cbxDepuis.selectedItem.value != -1)
			{
				var today:Date = new Date();	
				
				today.date = today.date - Number(cbxDepuis.selectedItem.value);
									
				var idx:int = ObjectUtil.dateCompare(today, item.DATE_EFFET);
				
				if(idx > 0)
					OK = false;				
			}
			
			return OK;
		}
		
		private function searchType(item:Object):Boolean
		{
			var OK:Boolean = true;
			
			if(cbxType.selectedItem != null && cbxType.selectedItem.IDACTION != -1)
			{
				if(item.IDACTION != cbxType.selectedItem.IDACTION)
					OK = false;
			}
			
			return OK;
		}
		
		private function searchPar(item:Object):Boolean
		{
			var OK:Boolean = true;
			
			if(cbxPar.selectedItem != null && cbxPar.selectedItem.APP_LOGINID != -1)
			{
				if(item.APP_LOGINID != cbxPar.selectedItem.APP_LOGINID)
					OK = false;
			}
			
			return OK;
		}

	}
}
