package
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import appli.control.AppControlEvent;
	import appli.events.ConsoViewEvent;
	import appli.login.LoginEvent;
	
	import composants.HintTextInput;
	import composants.util.ConsoviewAlert;
	
	import entity.CodeLangue;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import interfaceClass.IModuleLogin;
	
	import modulelogin.ihm.retrievepassword.RetrievePasswordIHM;

	public class LoginImpl extends Canvas
	{
		private static const default_code_langue 	:String = "default";
		private var appVersion						:String;
		private var authOp							:AbstractOperation;
		private var sendMailOp						:AbstractOperation;
		private var lastLoginOp						:AbstractOperation;
		private var authInfos						:Object;
		private var infosMessage					:String;
		private var _currentlogin					:String;
		private var passwordPopup					:RetrievePasswordIHM;
		private var idLang							:int = 0;
		private var code_langue						:String = default_code_langue;
		private var messColor						:int;
		private var code_application				:Number =  101;		
		private var save_local						:AbstractOperation;
		public var _appliVersion					:String;
		public var _infosMessage					:String;
		
		public var loginText		:HintTextInput;
		public var pwdText			:HintTextInput;
		public var btnAcrobat		:Image;
		public var btnPwd			:Label;
		public var stdConnectButton	:Button;
		public var btnFR 			:Image;
		public var btnES 			:Image;
		public var btnGB 			:Image;
		public var btnUS 			:Image;
		public var btnEU 			:Image;
		public var btnNL 			:Image;
		public var selectedflag		:Image;
		[Bindable]
		public var ckxSeSouvenir	:CheckBox;
		
		public function setInformations(appliVersion:String, infosMessage:String):void{
			this._appliVersion = appliVersion;
			this._infosMessage = infosMessage;
		}
		
		
		public function LoginImpl()
		{	
			appVersion = this._appliVersion;
	
			authInfos = null;
			sendMailOp = RemoteObjectUtil.getOperation("fr.consotel.consoprod.mail.SendMail", "sendSingleMail", mailSent);
			save_local = RemoteObjectUtil.getOperation("fr.consotel.consoview.M00.connectApp.AppUtil", "getLastCodePays", autoHandler);
			lastLoginOp = RemoteObjectUtil.getOperation("fr.consotel.consoview.M00.connectApp.AppUtil","getLastLogin",processLastLogin);
			
			messColor = 0xF5930A;
			
			switch (this._infosMessage)
			{
				case AppControlEvent.LOGOFF_EVENT:
					infosMessage = ResourceManager.getInstance().getString('M05', 'D_connexion_effectu_e_avec_succ_s');
					messColor = 0x0E8C34;									
					break;
				case ConsoViewEvent.NO_ACCESS_EXCEPTION:
					infosMessage = ResourceManager.getInstance().getString('M05', 'Aucun_acc_s_param_tre_pour_l_utilisateur');
					break;
				case ConsoViewEvent.UNABLE_TO_CONNECT:
					infosMessage = ResourceManager.getInstance().getString('M05', 'Erreur__Donn_es_incoh_rentes_pour_le_p_rim_tre');
					break;
				case ConsoViewEvent.FAULT_REMOTE_OPERATION:
					infosMessage = ResourceManager.getInstance().getString('M05', 'Erreur__1002');
					break;
				case ConsoViewEvent.EXPIRED_SESSION:
					infosMessage = ResourceManager.getInstance().getString('M05', 'Session_expir_e');
					break;
				default:
					infosMessage = "";
					break;
			}
			this.addEventListener(FlexEvent.CREATION_COMPLETE, afterCreationComplete);
		}
		
		public function getAuthInfos():Object
		{
			return authInfos;
		}
		
		private function afterCreationComplete(event:FlexEvent):void
		{		
			loginText.text = currentlogin;
			pwdText.text = "";
			
			//listeners for actions
			btnAcrobat.addEventListener(MouseEvent.CLICK, getAcrobat);
			btnPwd.addEventListener(MouseEvent.CLICK, askPwd);
			btnFR.addEventListener(MouseEvent.CLICK, btnlanguageFrClickHandler);
			btnES.addEventListener(MouseEvent.CLICK, btnlanguageEsClickHandler);
			btnGB.addEventListener(MouseEvent.CLICK, btnlanguageGBClickHandler);
			btnUS.addEventListener(MouseEvent.CLICK, btnlanguageUsClickHandler);
			btnEU.addEventListener(MouseEvent.CLICK, btnlanguageEuClickHandler);
			btnNL.addEventListener(MouseEvent.CLICK, btnlanguageNlClickHandler);
			
			selectedflag.source  = btnFR.source;
			selectedflag.toolTip  = btnFR.toolTip;
			
			stdConnectButton.addEventListener(MouseEvent.CLICK, connectItemHandler);
			this.addEventListener(KeyboardEvent.KEY_DOWN, connectItemHandler);
			//appel de save_local
			RemoteObjectUtil.callService(save_local);
			RemoteObjectUtil.invokeService(lastLoginOp,null);
			activeControls(true);
		}
		
		protected function btnlanguageFrClickHandler(evt:MouseEvent):void
		{
			switchLang("french","fr_FR",3);
		}
		
		private function switchLang(codeLangue:String, code:String, idLang:int = 3):void{
			ResourceManager.getInstance().localeChain = [code,'fr_FR'];
			this.idLang = idLang;
			if(code)code_langue = code else code = default_code_langue;			
			selectFlagToDisplay(code);
		}
		
		protected function btnlanguageGBClickHandler(evt:MouseEvent):void
		{
			switchLang("english","en_GB",1);
		}
		
		protected function btnlanguageNlClickHandler(evt:MouseEvent):void
		{
			switchLang("nederlands","nl_NL",2);
		}
		
		
		protected function btnlanguageUsClickHandler(evt:MouseEvent):void
		{
			switchLang("english","en_US",1);
		}
		
		protected function btnlanguageEuClickHandler(evt:MouseEvent):void
		{
			switchLang("english","en_EU",1);
		}
		
		protected function btnlanguageEsClickHandler(evt:MouseEvent):void
		{
			switchLang("spanish","es_ES",6);
		}
		
		private function activeControls(state:Boolean):Boolean
		{
			loginText.enabled = pwdText.enabled = stdConnectButton.enabled = btnPwd.enabled = state;
			return state;
		}
		
		private function connectItemHandler(event:Event):void
		{
			if (event is MouseEvent)
			{
				if ((loginText.text.length == 0) || pwdText.text.length == 0)
				{
					var myAlert:Alert = Alert.show(ResourceManager.getInstance().getString('M05', 'Acc_s_Refus____Login_ou_Mot_de_passe_invalide_s_'), ResourceManager.getInstance().getString('M04', 'Connexion'));
					PopUpManager.centerPopUp(myAlert);
				}
				else
					letsCOnnect();
			}
			else if (event is KeyboardEvent)
			{
				if ((event as KeyboardEvent).keyCode == 13)
				{
					letsCOnnect();
				}
			}
		}
		private function letsCOnnect():void
		{
			var cdeLg:CodeLangue = new CodeLangue(code_langue,"","",idLang,"");
			
			var objLogin:Object = new Object();
			objLogin.login = loginText.text;
			objLogin.mdp = pwdText.text;
			objLogin.codeLangue = cdeLg;
			if(ckxSeSouvenir != null)
				objLogin.boolSeSouvenir = ckxSeSouvenir.selected;
			else
				objLogin.boolSeSouvenir = false;
			
			(parentDocument as IModuleLogin).validateLogin(objLogin);
		}
		//fonction analysant le CODE_PAYS
		private function autoHandler(evt:ResultEvent):void{
			if (evt.result != null)
			{
				trace("result Last Country ***",evt.result.toString());
				var resultat:String = evt.result.toString();
				if((resultat == "NO_CODE_PAYS") || (resultat == "NO_PAYS")){
					switchLang("french","fr_FR",3);
				}else{
					var num:int = 3;
					
					var tooltip:String = "";
					
					switch(resultat)
					{
						case "fr_FR":num = 3;
							break;
						case "en_US": num = 1;
							break;
						case "en_GB": num = 1;
							break;
						case "en_EU": num = 1;							
							break;		
						case "es_ES": num = 6;							
							break;
						case "nl_NL": num = 2;							
							break;
						default : num = 3;break;
					}
					
					switchLang("",resultat, num);
				}
			} 
		}
		
		private function processLastLogin(event:Event):void 
		{
			currentlogin = (event as ResultEvent).result as String;
			
			loginText.setFocus();
				
			if(loginText.text != "")
			{
				pwdText.setFocus();
			}
		}
		
		private function getAcrobat(event:Event):void
		{
			var url:String = "http://www.adobe.com/fr/products/acrobat/readstep2.html";
			var variables:URLVariables = new URLVariables();
			var request:URLRequest = new URLRequest(url);
			request.method = "GET";
			navigateToURL(request, "_blank");
		}
		
		/**
		 * Ouverture de la popUp de demande d'envoi
		 * du mot de passe
		 * @param event
		 *
		 */
		private function askPwd(event:Event):void
		{
			passwordPopup = new RetrievePasswordIHM();
			PopUpManager.addPopUp(passwordPopup, this, true);
			PopUpManager.centerPopUp(passwordPopup);
			
			if (loginText.text != "")
			{
				passwordPopup.tiEmail.text = loginText.text;
			}
		}
		
		
		private function mailSent(e:ResultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M05','La_demande_de_mot_de_passe_a_ete_envoy_e__'));
			
		}
		
		/**
		 * selection du drapeau a afficher pres du boutonde login
		 * @param code_langue un code langue au format i18n
		 * */
		private function selectFlagToDisplay(code_langue:String):void
		{
			try
			{
				var flag:String = code_langue.split("_")[1];
				selectedflag.source = this["btn"+flag].source; 
				selectedflag.toolTip = this["btn"+flag].toolTip; 
			}
			catch(error:Error)
			{
				selectedflag.source = "https://cache.consotel.fr/flag/FR.png" 
				selectedflag.toolTip = "Français | €";	
			}
		}
		
		
		[Bindable]
		public function get currentlogin():String
		{
			return _currentlogin;
		}

		public function set currentlogin(value:String):void
		{
			_currentlogin = value;
		}
		
		
	}
}