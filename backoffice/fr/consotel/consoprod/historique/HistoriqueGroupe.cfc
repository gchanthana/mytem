<!---
Package : fr.consotel.consoview.facturation.optimisation.historique
--->
<cfcomponent name="HistoriqueGroupe">
	<!---
		Comparaison sur le format "YYYY/MM"
		-1 si date1 est antÃ©rieur ï¿½ date2
		0 si date1 est Ã©gal ï¿½ date2
		1 si date1 est postï¿½rieur ï¿½ date2
	--->
	<cffunction name="comparePeriodMonth" access="private" returntype="numeric" output="false">
		<cfargument name="date1" required="true" type="date"/>
		<cfargument name="date2" required="true" type="date"/>
		<cfset yearCompare = dateCompare(date1,date2,"YYYY")>
		<cfif yearCompare EQ 0>
			<cfreturn dateCompare(date1,date2,"M")>
		<cfelse>
			<cfreturn yearCompare>
		</cfif>
	</cffunction>

	<cffunction name="getHistoriqueData" access="remote" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="DATEFIN" required="true" type="string"/>
        <cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl.eco_histo">
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#DATEDEB#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#DATEFIN#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetSummaryData" resultset="1">
	        <cfprocresult name="qGetHistoData" resultset="2">
        </cfstoredproc>
		<cfset dateDebObj = parseDateTime(DATEDEB)>
		<cfset dateFinObj = parseDateTime(DATEFIN)>
		<cfset currentDateObj = parseDateTime(DATEDEB)>
		<cfset nbMonth = dateDiff("m",dateDebObj,dateFinObj) + 1>
		<cfset monthArray = arrayNew(1)>
 		<cfset dummyValues = arrayNew(1)>
 		<cfset maxValues = arrayNew(1)>
		<cfloop index="i" from="1" to="#qGetSummaryData.recordcount#">
			<cfset dummyValues[i] = 0>
			<cfset maxValues[i] = 0>
			<cfif LEN(qGetSummaryData['OFFRENOM'][i]) NEQ 0>
				<cfset offreBool = FIND("-",qGetSummaryData['OFFRENOM'][i],1)>
				<cfif offreBool NEQ 0>
					<cfset offreNomMod = ListDeleteAt(qGetSummaryData['OFFRENOM'][i],1,"-")>
					<cfset querySetCell(qGetSummaryData,"OFFRENOM",offreNomMod,i)>
				</cfif>
			</cfif>
		</cfloop>
		<cfset queryAddColumn(qGetSummaryData,"MAX_VALUE","INTEGER",maxValues)>
		<cfloop index="i" from="1" to="#nbMonth#">
			<cfset monthArray[i] = currentDateObj>
			<cfset currentColumnName = "M_" & replace(lsDateFormat(monthArray[i],"YYYY/MM"),"/","_","ALL")>
  			<cfset queryAddColumn(qGetSummaryData,"#currentColumnName#","INTEGER",dummyValues)>
			<cfset currentDateObj = dateAdd("m",1,currentDateObj)>
		</cfloop>
		
		<cfset summaryIndex = 1>
 		<cfoutput query="qGetHistoData" group="TYPEDATA">
			<cfoutput group="IDREF_CLIENT">
				<cfoutput group="OPERATEURID">
					<cfoutput group="OFFRECLIENTID">
						<cfoutput group="MOIS">
							<cfif TYPEDATA EQ "A">
								<cfset currentMonthFirstDate = parseDateTime(MOIS_FIRST)>
								<cfif LEN(MOIS_LAST) EQ 0>
									<cfloop index="j" from="1" to="#arrayLen(monthArray)#">
										<cfset tmpAboMonthColumnName = "M_" & replace(lsDateFormat(monthArray[j],"YYYY/MM"),"/","_","ALL")>
										<cfif tmpAboMonthColumnName EQ ("M_" & replace(MOIS,"/","_","ALL"))>
											<cfset querySetCell(qGetSummaryData,tmpAboMonthColumnName,2,summaryIndex)>
										</cfif>
										<cfif comparePeriodMonth(currentMonthFirstDate,monthArray[j]) LTE 0>
											<cfset currentColumnValue = qGetSummaryData[tmpAboMonthColumnName][summaryIndex]>
											<cfif currentColumnValue EQ 0>
												<cfset querySetCell(qGetSummaryData,tmpAboMonthColumnName,1,summaryIndex)>
											</cfif>
										</cfif>
									</cfloop>
								<cfelse>
 									<cfset currentMonthLastDate = parseDateTime(MOIS_LAST)>
									<cfloop index="j" from="1" to="#arrayLen(monthArray)#">
										<cfset tmpAboMonthColumnName = "M_" & replace(lsDateFormat(monthArray[j],"YYYY/MM"),"/","_","ALL")>
										<cfif tmpAboMonthColumnName EQ ("M_" & replace(MOIS,"/","_","ALL"))>
											<cfset querySetCell(qGetSummaryData,tmpAboMonthColumnName,2,summaryIndex)>
										</cfif>
 										<cfif (comparePeriodMonth(currentMonthFirstDate,monthArray[j]) LTE 0) AND
												(comparePeriodMonth(monthArray[j],currentMonthLastDate) LTE 0)>
											<cfset currentColumnValue = qGetSummaryData[tmpAboMonthColumnName][summaryIndex]>
											<cfif currentColumnValue EQ 0>
												<cfset querySetCell(qGetSummaryData,tmpAboMonthColumnName,1,summaryIndex)>
											</cfif>
										</cfif>
									</cfloop>
								</cfif>
							<cfelse>
 								<cfif MOIS NEQ "0">
									<cfset tmpFactuMonthColumnName = "M_" & replace(MOIS,"/","_","ALL")>
									<cfset tmpFactuValue = NBFACT>
						  			<cfset querySetCell(qGetSummaryData,tmpFactuMonthColumnName,tmpFactuValue,summaryIndex)>
						  			<cfif tmpFactuValue GT qGetSummaryData['MAX_VALUE'][summaryIndex]>
										<cfset querySetCell(qGetSummaryData,"MAX_VALUE",NBFACT,summaryIndex)>
									</cfif>
						  		</cfif>
							</cfif>
						</cfoutput>
						<cfif TYPEDATA EQ "A">
							<cfset summaryIndex = summaryIndex + 1>
						</cfif>
					</cfoutput>
					<cfif TYPEDATA EQ "F">
						<cfset summaryIndex = summaryIndex + 1>
					</cfif>
				</cfoutput>
			</cfoutput>
		</cfoutput>
		<cfreturn qGetSummaryData>
	</cffunction>
</cfcomponent>
