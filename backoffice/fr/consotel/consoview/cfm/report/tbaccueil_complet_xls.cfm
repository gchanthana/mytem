<cfheader name="Content-Disposition" value="inline;filename=TableauBord.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>CONSOTEL</Author>
  <LastPrinted>2006-05-16T16:14:38Z</LastPrinted>
  <Created>2006-05-16T15:44:05Z</Created>
  <LastSaved>2006-05-16T16:40:29Z</LastSaved>
  <Company>CONSOTEL</Company>
  <Version>11.6568</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12405</WindowHeight>
  <WindowWidth>18780</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>60</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16" ss:Name="Euro">
   <NumberFormat
    ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s18" ss:Name="Milliers">
   <NumberFormat
    ss:Format="_-* #,##0.00\ _&euro;_-;\-* #,##0.00\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="m162247916">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="14" ss:Color="#000000"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="m162247926">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="14" ss:Color="#000000"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Vertical="Center"/>
  </Style>
  <Style ss:ID="s25">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Color="#003366" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s26">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8"/>
  </Style>
  <Style ss:ID="s27" ss:Parent="s18">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_-* #,##0\ _&euro;_-;\-* #,##0\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s28" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s33">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8"/>
   <Interior/>
  </Style>
  <Style ss:ID="s34" ss:Parent="s18">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="_-* #,##0\ _&euro;_-;\-* #,##0\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s35" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s37">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8"/>
   <Interior/>
  </Style>
  <Style ss:ID="s38" ss:Parent="s18">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="_-* #,##0\ _&euro;_-;\-* #,##0\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s39" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s40">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8"/>
  </Style>
  <Style ss:ID="s41" ss:Parent="s18">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_-* #,##0\ _&euro;_-;\-* #,##0\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s42" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Tahoma" x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s43">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s44">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s45">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s46">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
   <NumberFormat
    ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s47">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s48" ss:Parent="s18">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_-* #,##0\ _&euro;_-;\-* #,##0\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s50">
   <Alignment ss:Vertical="Center"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s51">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s52">
   <Alignment ss:Vertical="Center"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s53">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Font ss:Size="14"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s54">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center"/>
   <Font x:Family="Swiss" ss:Size="12" ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s55">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s56">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s57">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
   <NumberFormat
    ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s58">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s59" ss:Parent="s18">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1" ss:Italic="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_-* #,##0\ _&euro;_-;\-* #,##0\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s60">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s61">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s63">
   <Alignment ss:Vertical="Center"/>
   <Font ss:Size="8"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Vertical="Center"/>
   <Font ss:Size="8"/>
   <NumberFormat ss:Format="General Date"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s66" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="8" ss:Color="#003366"
    ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_-* #,##0\ _&euro;_-;\-* #,##0\ _&euro;_-;_-* &quot;-&quot;??\ _&euro;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Verdana" x:Family="Swiss" ss:Size="18" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="TB">
  <Table x:FullColumns="1"
   x:FullRows="1" ss:StyleID="s24" ss:DefaultColumnWidth="60">
   <Column ss:StyleID="s24" ss:AutoFitWidth="0" ss:Width="161.25"/>
   <Column ss:Index="3" ss:StyleID="s24" ss:Width="90"/>
   <Column ss:StyleID="s24" ss:AutoFitWidth="0" ss:Width="165"/>
   <Column ss:StyleID="s24" ss:Width="71.25"/>
   <Column ss:StyleID="s24" ss:AutoFitWidth="0" ss:Width="200"/>
   <Column ss:StyleID="s24" ss:Width="100"/>
	<Row ss:AutoFitHeight="0" ss:Height="18">
    <Cell ss:MergeDown="1" ss:StyleID="s78"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><B><I><Font html:Color="#008000">Conso</Font><Font
         html:Color="#FF9900">View</Font></I></B></ss:Data></Cell>
   </Row>
   <Row>
		<Cell ss:Index="6" ss:StyleID="s63"><Data ss:Type="String">&Eacute;dit&eacute; par :</Data></Cell>
   		<Cell ss:Index="7" ss:StyleID="s51"><Data ss:Type="String"><cfoutput>#session.user.prenom# #session.user.nom#</cfoutput></Data></Cell>    
   </Row>
   <Row ss:Index="4" ss:Height="14.25">
   		<Cell ss:Index="6" ss:StyleID="s63"><Data ss:Type="String">&nbsp;&nbsp;&nbsp;Le</Data></Cell>
    	<Cell ss:StyleID="s51"><Data ss:Type="String">&nbsp;<cfoutput>#Lsdateformat(now(), 'dd/mm/yyyy')#</cfoutput></Data></Cell>
   </Row>
	<Row ss:Index="6" ss:AutoFitHeight="0" ss:Height="22.5">
    <Cell ss:Index="4" ss:StyleID="s52"/>
    <Cell ss:StyleID="s52"/>
    <Cell ss:StyleID="s52"/>
    <Cell ss:StyleID="s53"><Data ss:Type="String">Tableau de bord <cfoutput>#lcase(tb)#</cfoutput> par Cat&eacute;gorie</Data></Cell>
   </Row>
   <Row ss:Index="8" ss:AutoFitHeight="0" ss:Height="22.5">
	<Cell ss:Index="3" ss:StyleID="s50"/>
	<Cell ss:Index="4" ss:StyleID="s50"/>
    <Cell ss:Index="5" ss:StyleID="s50"/>
    <Cell ss:StyleID="s50"/>
    <Cell ss:StyleID="s54"><Data ss:Type="String"><cfoutput>#raisonsociale#</cfoutput></Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s25"><Data ss:Type="String">P&eacute;riode d'&eacute;mission des factures : <cfoutput>#Lsdateformat(datedeb, 'dd mmmm yyyy')# - #Lsdateformat(datefin, 'dd mmmm yyyy')#</cfoutput></Data></Cell>
   </Row>
   <Row ss:Index="11" ss:AutoFitHeight="0" ss:Height="29.25">
    <Cell ss:MergeAcross="2" ss:StyleID="m162247916"><Data ss:Type="String"><cfoutput>Abonnements</cfoutput></Data></Cell>
    <Cell ss:MergeAcross="3" ss:StyleID="m162247926"><Data ss:Type="String"><cfoutput>Consommations</cfoutput></Data></Cell>
   </Row>
   <Row ss:Height="21.75">
    <Cell ss:StyleID="s60"><Data ss:Type="String">Type de Produits</Data></Cell>
    <Cell ss:StyleID="s61"><Data ss:Type="String">Qt&eacute; factur&eacute;e</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="String">Montant (&euro; H.T.)</Data></Cell>
    <Cell ss:StyleID="s60"><Data ss:Type="String">Type de Produits</Data></Cell>
    <Cell ss:StyleID="s61"><Data ss:Type="String">Nb. Appels</Data></Cell>
    <Cell ss:StyleID="s61"><Data ss:Type="String">Volume (min ou kO)</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="String">Montant (&euro; H.T.)</Data></Cell>
   </Row>
   <cfif compteurFixe gt 0>
	<Row ss:AutoFitHeight="0" ss:Height="20.25">
    <Cell ss:StyleID="s55"><Data ss:Type="String">T&eacute;l&eacute;phonie fixe</Data></Cell>
    <Cell ss:StyleID="s56"/>
    <Cell ss:StyleID="s57" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurFixe#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s55"><Data ss:Type="String">T&eacute;l&eacute;phonie fixe</Data></Cell>
    <Cell ss:StyleID="s59" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurFixe#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s59" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurFixe#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s57" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurFixe#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
   </Row>
	<cfloop from="1" to="#maxCompteur#" index="i">
	<cfif resultsetAbo['segment_theme'][i] eq 'Fixe' OR resultsetConso['segment_theme'][i] eq 'Fixe'>
	<cfoutput>
		
		<Row ss:Height="13.5">
		    <Cell ss:StyleID="s26"><Data ss:Type="String">#resultsetAbo['theme_libelle'][i]#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#val(resultsetAbo['qte'][i])#</Data></Cell>
		    <Cell ss:StyleID="s28"><Data ss:Type="Number">#val(replace(resultsetAbo['montant_final'][i],",","."))#</Data></Cell>
		    <Cell ss:StyleID="s26"><Data ss:Type="String">#resultsetConso['theme_libelle'][i]#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#val(resultsetConso['nombre_appel'][i])#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#round(val(resultsetConso['duree_appel'][i]))#</Data></Cell>
		    <Cell ss:StyleID="s28"><Data ss:Type="Number">#val(replace(resultsetConso['montant_final'][i],",","."))#</Data></Cell>
		</Row>
	</cfoutput>
	</cfif>
	</cfloop>
	</cfif>
	<cfif compteurGSM gt 0>
   <Row ss:AutoFitHeight="0" ss:Height="20.25">
    <Cell ss:StyleID="s44"><Data ss:Type="String">T&eacute;l&eacute;phonie mobile</Data></Cell>
    <Cell ss:StyleID="s45"/>
    <Cell ss:StyleID="s46" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurGSM#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s44"><Data ss:Type="String">T&eacute;l&eacute;phonie mobile</Data></Cell>
    <Cell ss:StyleID="s48" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurGSM#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s48" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurGSM#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s46" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurGSM#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
   </Row>
   <cfloop from="1" to="#maxCompteur#" index="i">
	<cfif resultsetAbo['segment_theme'][i] eq 'Mobile' OR resultsetConso['segment_theme'][i] eq 'Mobile'>
	<cfoutput>
		<Row ss:Height="13.5">
		    <Cell ss:StyleID="s26"><Data ss:Type="String">#resultsetAbo['theme_libelle'][i]#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#val(resultsetAbo['qte'][i])#</Data></Cell>
		    <Cell ss:StyleID="s28"><Data ss:Type="Number">#val(replace(resultsetAbo['montant_final'][i],",","."))#</Data></Cell>
		    <Cell ss:StyleID="s26"><Data ss:Type="String">#resultsetConso['theme_libelle'][i]#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#val(resultsetConso['nombre_appel'][i])#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#round(val(resultsetConso['duree_appel'][i]))#</Data></Cell>
		    <Cell ss:StyleID="s28"><Data ss:Type="Number">#val(replace(resultsetConso['montant_final'][i],",","."))#</Data></Cell>
		</Row>
	</cfoutput>
	</cfif>
	</cfloop>
	</cfif>
	<cfif compteurData gt 0>
	<Row ss:AutoFitHeight="0" ss:Height="20.25">
    <Cell ss:StyleID="s55"><Data ss:Type="String">Data</Data></Cell>
    <Cell ss:StyleID="s56"/>
    <Cell ss:StyleID="s57" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurData#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s55"><Data ss:Type="String">Data</Data></Cell>
    <Cell ss:StyleID="s59" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurData#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s59" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurData#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s57" ss:Formula="=SUM(R[1]C:R[<cfoutput>#compteurData#</cfoutput>]C)"><Data ss:Type="Number"></Data></Cell>
   </Row>
	<cfloop from="1" to="#maxCompteur#" index="i">
	<cfif resultsetAbo['segment_theme'][i] eq 'Data' OR resultsetConso['segment_theme'][i] eq 'Data'>
	<cfoutput>
		<Row ss:Height="13.5">
		    <Cell ss:StyleID="s26"><Data ss:Type="String">#resultsetAbo['theme_libelle'][i]#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#val(resultsetAbo['qte'][i])#</Data></Cell>
		    <Cell ss:StyleID="s28"><Data ss:Type="Number">#val(replace(resultsetAbo['montant_final'][i],",","."))#</Data></Cell>
		    <Cell ss:StyleID="s26"><Data ss:Type="String">#resultsetConso['theme_libelle'][i]#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#val(resultsetConso['nombre_appel'][i])#</Data></Cell>
		    <Cell ss:StyleID="s27"><Data ss:Type="Number">#round(val(resultsetConso['duree_appel'][i]))#</Data></Cell>
		    <Cell ss:StyleID="s28"><Data ss:Type="Number">#val(replace(resultsetConso['montant_final'][i],",","."))#</Data></Cell>
		</Row>
	</cfoutput>
	</cfif>
	</cfloop>
	</cfif>
   <Row ss:AutoFitHeight="0" ss:Height="20.25">
    <Cell ss:StyleID="s65"><Data ss:Type="String">TOTAL</Data></Cell>
    <Cell ss:StyleID="s23"/>
    <Cell ss:StyleID="s66" ss:Formula="=SUM(R[-<cfoutput>#Evaluate(compteurGSM+compteurFixe+compteurData+3)#</cfoutput>]C:R[-1]C)/2"><Data
      ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s65"><Data ss:Type="String">TOTAL</Data></Cell>
    <Cell ss:StyleID="s67" ss:Formula="=SUM(R[-<cfoutput>#Evaluate(compteurGSM+compteurFixe+compteurData+3)#</cfoutput>]C:R[-1]C)/2"><Data
      ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s67" ss:Formula="=SUM(R[-<cfoutput>#Evaluate(compteurGSM+compteurFixe+compteurData+3)#</cfoutput>]C:R[-1]C)/2"><Data
      ss:Type="Number"></Data></Cell>
    <Cell ss:StyleID="s66" ss:Formula="=SUM(R[-<cfoutput>#Evaluate(compteurGSM+compteurFixe+compteurData+3)#</cfoutput>]C:R[-1]C)/2"><Data
      ss:Type="Number"></Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Layout x:Orientation="Landscape" x:CenterHorizontal="1" x:CenterVertical="1"/>
    <Header x:Margin="0.51181102362204722"/>
    <Footer x:Margin="0.51181102362204722"/>
    <PageMargins x:Bottom="0.98425196850393704" x:Left="0.78740157480314965"
     x:Right="0.78740157480314965" x:Top="0.98425196850393704"/>
   </PageSetup>
   <FitToPage/>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <Scale>87</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <DoNotDisplayGridlines/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
