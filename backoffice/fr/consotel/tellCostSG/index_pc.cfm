	
	<?xml version="1.0" encoding="iso-8859-1" ?>
	<!DOCTYPE html PUBLIC
	"-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
	"http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
		<title>
			TELLCOST
		</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<meta name="robots" content="all"/>
		<meta name="classification" content="Services"/>
		<meta name="copyright" content="ConsoTel SAS"/>
		<meta name="author" content="Guillaume RAMOS"/>
		<meta name="language" content="fr"/>
		<meta name="description" content="TELLCOST"/>
		<meta name="keywords" content="TELLCOST"/>
	
		<STYLE TYPE="TEXT/CSS">
			#MONCADRE 		{
								WIDTH: 1050PX;
								HEIGHT:100%;
						 		PADDING:4PX; 
								MARGIN-LEFT:10PX;
								MARGIN-RIGHT:10PX;
								MARGIN-TOP:10PX;
								MARGIN-BOTTOM:10PX; 
							}
			#MONCADREBAS 	{
								WIDTH: 1100PX;
								MARGIN-RIGHT:75PX;
							}
							
			#CADREHAUT 		{
								WIDTH:1024PX;
								HEIGHT:100PX;
							}
			
			#CADRECENTRALE 	{ 
								MARGIN-LEFT:100PX;
								MARGIN-RIGHT:100PX;
								MIN-HEIGHT:5PX;
								BORDER-LEFT:1PX DASHED #FFFFFF; 
								BORDER-RIGHT:1PX DASHED #FFFFFF;
								BORDER-BOTTOM:1PX DASHED #FFFFFF;
								BORDER-TOP:1PX DASHED #FFFFFF;
							}
			
			#CADRECENTRALE2 { 
								BORDER-LEFT:1PX SOLID #000000; 
								BORDER-RIGHT:1PX SOLID #000000;
								BORDER-BOTTOM:1PX SOLID #000000;
								BORDER-TOP:1PX SOLID #000000;
								PADDING:10PX;
								WIDTH: 650PX;
								-MOZ-BORDER-RADIUS:10PX;
								-WEBKIT-BORDER-RADIUS:10PX;
								-KHTML-BORDER-RADIUS:10PX;
					  			BORDER-RADIUS:10PX;
							}
			
			#MYFIELDSET 	{ 
								-MOZ-BORDER-RADIUS:8PX;
								-WEBKIT-BORDER-RADIUS:8PX;
								-KHTML-BORDER-RADIUS:8PX;
					  			BORDER-RADIUS:8PX;
							}
			 	
		 	.submit 		{
								WIDTH: 200PX;
		 					}
		 	.sub 			{
								WIDTH: 100PX;
		 					}
		</style>
	
		<script language="Javascript">
			if (document.images) 
			{
				button1 = new Image
				button2 = new Image
			
				button1.src = "tellCostImg/Connexion_n.png"
				button2.src = "tellCostImg/Connexion_s.png"
			}
		</script>
	</head>

<!--- Variables --->

	<cfset objectValidate = structNew()>
	<cfset ValidateLog = -1>
	<cfset MessageLogError = "">
	<cfset obj = createobject('component','TellCostService')>	
	
<!--- Fin variables --->

<!--- Session --->
	
	<cfset SESSION.IS_CONNECTED = 0>
	<cfset SESSION.SESSION_ID = -1>
	<cfset SESSION.PHONENUMBER = "">
	<cfset SESSION.IDSOUS_TETE = "">
	<cfset SESSION.IDRACINE_MASTER = "">
	<cfset SESSION.IDPERIODE = "">
	<cfset SESSION.IDTHEME = "">
	<cfset SESSION.LIBELLE_MOIS = "">
	<cfset SESSION.PATH_CONNECT = 'details.cfm?'&'#SESSION.URLTOKEN#'>
	
<!--- Fin session --->

<!--- Function --->

	<cffunction name="zCheckValidateLog" access="private" returntype="numeric">
	
			<cfset objectValidate = obj.zQueryCheckLog(FORM._Login,FORM._Password,SESSION)>
			<cfset ValidateLog = objectValidate.RETOUR>
			<cfset rslt = -1>
			
			<cfif ValidateLog GT 0>
				<cfset MessageLogError = "">
				<cfset SESSION = objectValidate>
				<cfset SESSION.IS_CONNECTED = 1>
				<cfset SESSION.SESSION_ID = ValidateLog>
				<cfset SESSION.PHONENUMBER = FORM._Login>
				<cfset rslt = 1>
			<cfelseif ValidateLog EQ -4>
				<cfset MessageLogError = "Numéro et mot de passe invalide">
				<cfset SESSION.IS_CONNECTED = 0>
				<cfset SESSION.SESSION_ID = -1>
				<cfset rslt = -1>
			<cfelseif ValidateLog EQ 0>
				<cfset MessageLogError = "Numéro et mot de passe invalide">
				<cfset SESSION.IS_CONNECTED = 0>
				<cfset SESSION.SESSION_ID = -1>
				<cfset rslt = -1>
			</cfif> 

 		<cfreturn rslt> 
	</cffunction> 

<!--- Fin function --->

<!--- Tests --->

	<cfif structKeyExists(FORM,'CONNEXION')>
		<cfif (FORM._Login NEQ "" AND FORM._Password NEQ "")>
		
			<cfif zCheckValidateLog() EQ 1>
				<cflocation url="#SESSION.PATH_CONNECT#">
			<cfelse>
			</cfif>
		<cfelse>
			<cfset MessageLogError = "Numéro ou mot de passe invalide">	
		</cfif>
	</cfif>			

<!--- Fin tests --->

	<FONT face="Arial" size="2">	
		<div align="center">
			<DIV id="CADREHAUT">
				 <br/><br/>
			 	<img src="tellCostImg/SocieteGeneral.jpg" alt="header" height="45" align="left"/>
			 	<img src="tellCostImg/tellCostSGMini.png" alt="header" height="45" align="right"/>
			</DIV>
			<DIV ID="MONCADRE">
				<DIV ID="CADRECENTRALE">
 				  <FORM name="testForm" method="post">
					  	<FIELDSET align="center" id="MYFIELDSET">
						    <LEGEND>Authentification</LEGEND>
						    <cfoutput>Bienvenue sur votre espace de suivi des consomations de la téléphonie Mobile</cfoutput>
						    <br/><br/>
						    <cfoutput>Veuillez vous authentifier</cfoutput>
						    <br/><br/>
						    
							
							<DIV ID="CADRECENTRALE2">
								<TABLE WIDTH="600" STYLE="BORDER:0PX SOLID #000000;MARGIN:AUTO;BORDER-COLLAPSE: COLLAPSE;" ID="TABLEID"> 
									<TR> 
										<TD align="right">
											<form name="testForm2" >
												<br/>
												<cfoutput>N° de ligne mobile :</cfoutput>
												<input type="texte" name="_Login" class="submit" style="WIDTH:100"/>
												<br/><br/>
												<cfoutput>Mot de passe :</cfoutput>
										        <input type="password" name="_Password" class="submit" style="WIDTH:100"/>
												<br/><br/>
												<div align="center" style="MARGIN-LEFT:35PX;">
													<input type="submit" name="CONNEXION" value="Connexion" onclick="va();" class="sub" style="WIDTH:200"/>
												</div>
											</form>
										</TD>      
										<TD width="250">
											<FONT COLOR="#FF0000">
												<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;#MessageLogError#</cfoutput> 
											</FONT>
										</TD> 
									</TR>
								</TABLE>
							</DIV>
							<br/>
 						</FIELDSET>
					</FORM>
					<br/><br/>
					<FORM>
					  	<FIELDSET align="center" id="MYFIELDSET">
						    <LEGEND>Liens utiles</LEGEND>
							<div align="left">
								Pour des raisons de sécurité, ces liens ne sont accessibles que depuis un poste Société Générale : <br/>
								
								<TABLE WIDTH="600" STYLE="BORDER:0PX SOLID #000000;MARGIN:AUTO;BORDER-COLLAPSE: COLLAPSE;" ID="TABLEID"> 

									<tr>
										<td style="width: 300px;">
											<span style="font-family: Arial;">&nbsp;</span><a style="font-family: Arial;" href="http://messervicesauquotidien.corporate.si.socgen/siteaplat/context/ispicfr/lang/fr/net/arp/pid/76/rubid/6197/object/rubriqueuw/id/6197/staticmode/static/nodoctype/0.htm" target="_blank">Les bonnes pratiques</a><br>
										</td>
										<td style="width: 433px;">
											<span style="font-family: Arial;"> </span><a style="font-family: Arial;" href="http://messervicesauquotidien.corporate.si.socgen/file/fichieruw/ispicfr/ispicfr-20101105-152724-infoconsogts.pdf" target="_blank"> INFO CONSO GTS</a> <br><small><a style="font-family: Arial;"> (Principe et fonctionnement)</a></small>
										</td>
									</tr>
									<tr>
										<td style="width: 300px;">
											<span style="font-family: Arial;"></span><a style="font-family: Arial;" href="http://canal.achats.socgen/ebn.ebn?pid=14&amp;objet=Etape&amp;action=liste&amp;produitid=377&amp;typeEtape=2&amp;universid=5" target="_blank">&nbsp;Les offres de la téléphonie mobile</a>
										</td>
										<td style="width: 433px;">
											<span style="font-family: Arial;"></span><a style="font-family: Arial;" href="http://messervicesauquotidien.corporate.si.socgen/file/fichieruw/ispicfr/ispicfr-20101104-184712-glossairegts.pdf" target="_blank">Glossaire</a>
										</td>
									</tr>
								</TABLE>

								<br/>
								Pour toute demande d'information sur le service de suivi de la consommation de la téléphonie mobile, un simple mail à l'adresse suivante : 
								<div align="center">
									<a href="mailto:Monsuivi.consomobile@socgen.com?subject=Question tellcost" accesskey="3">Monsuivi.consomobile@socgen.com</a>
								</div>
							</div>
						</FIELDSET>
					</FORM>
					<br/><br/>
					<FORM>
					  	<FIELDSET align="center" id="MYFIELDSET">
						    <LEGEND>CNIL</LEGEND>
							<div align="center">
								Tout utilisateur est informé que l'accès de ce logiciel doit se faire dans le respect des 
								dispositions prévues par la CNIL en matière de protection de données personnelles et nominatives. Consotel 
								ne pourra pas être tenu responsable d'une utilisation non conforme. Les informations vous concernant ont été 
								recueillies auprès de votre opérateur télécom. Elles font l'objet d'un traitement informatique de gestion 
								de la téléphonie effectué pour le compte de votre entreprise. Seules sont diffusées des informations agrégées. La 
								campagne d’information Tellcost est conforme aux recommandations de la CNIL. La loi Informatique et libertés du 
								6 janvier 1978 vous accorde un droit d'accès et de rectification aux informations qui vous concernent.
								<a href="http://www.cnil.fr"> http://www.cnil.fr </a><br/>
								Toutes les tentatives de connexion sont enregistrées.<br/>
								Contact mail : <a href="mailto:Monsuivi.consomobile@socgen.com?subject=Question tellcost" accesskey="3">Monsuivi.consomobile@socgen.com</a>
							</div>
						</FIELDSET>
					</FORM>
				</DIV>
			</DIV>	

			<DIV ID="MONCADREBAS">
				<TABLE WIDTH="100%" STYLE="BORDER:0PX SOLID #000000;MARGIN:AUTO;BORDER-COLLAPSE: COLLAPSE;"> 
					<tr>
						<td  align="left">
							<img src="tellCostImg/tellCostTestPowered.png" alt="header" height="35" align="right"/>
						</td>
						<td style="width: 100%;">
						</td>
						<td style="width: 100%;" align="right">
							<img src="tellCostImg/tellCostTestMiniMini.png" border="0"  weight="0"  align="left"/>
						</td>
					</tr>
				</TABLE>
			</DIV>
		</div>
	</font>
</html>