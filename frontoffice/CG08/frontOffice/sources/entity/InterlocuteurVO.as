package entity
{
	[Bindable]
	public class InterlocuteurVO
	{
		private var _id:int;
		private var _civilite:String;
		private var _nom:String;
		private var _prenom:String;
		private var _qualite:String;
		private var _email:String;
		private var _principal:int;// 0 ou 1
		private var _langue:String;
		private var _dateDebut:Date;
		private var _dateFin:Date;
		private var _isActive:Boolean;
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_INTERLOCUTEUR"))
					this.id = obj.IDFC_INTERLOCUTEUR;
				
				if(obj.hasOwnProperty("CIVILITE"))
					this.civilite = obj.CIVILITE;
				
				if(obj.hasOwnProperty("NOM"))
					this.nom = (obj.NOM)?obj.NOM:'';
				
				if(obj.hasOwnProperty("PRENOM"))
					this.prenom = (obj.PRENOM)?obj.PRENOM:'';
				
				if(obj.hasOwnProperty("QUALITE"))
					this.qualite = (obj.QUALITE != null)?obj.QUALITE:'';
				
				if(obj.hasOwnProperty("EMAIL_INTER"))
					this.email = (obj.EMAIL_INTER)?obj.EMAIL_INTER:'';
				
				if(obj.hasOwnProperty("BOOL_PRINCIPAL_INTER"))
					this.principal = obj.BOOL_PRINCIPAL_INTER;
				
				if(obj.hasOwnProperty("DEFAULT_LANGUE"))
					this.langue = obj.DEFAULT_LANGUE ;
				
				if(obj.hasOwnProperty("DATE_DEB"))
					this.dateDebut = obj.DATE_DEB as Date;
				
				if(obj.hasOwnProperty("DATE_FIN"))
					this.dateFin = obj.DATE_FIN as Date;
				
				if(obj.hasOwnProperty("IS_ACTIVE"))
					this.isActive = (obj.IS_ACTIVE == 1)?true:false;
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet InterlocuteurVO erroné ");
			}
		}
		
		public function get isActive():Boolean { return _isActive; }
		
		public function set isActive(value:Boolean):void
		{
			if (_isActive == value)
				return;
			_isActive = value;
		}
		
		public function get qualite():String { return _qualite; }
		
		public function set qualite(value:String):void
		{
			if (_qualite == value)
				return;
			_qualite = value;
		}
		
		public function InterlocuteurVO()
		{
		}
		
		public function get dateFin():Date { return _dateFin; }
		
		public function set dateFin(value:Date):void
		{
			if (_dateFin == value)
				return;
			_dateFin = value;
		}
		
		public function get dateDebut():Date { return _dateDebut; }
		
		public function set dateDebut(value:Date):void
		{
			if (_dateDebut == value)
				return;
			_dateDebut = value;
		}
		
		public function get langue():String { return _langue; }
		
		public function set langue(value:String):void
		{
			if (_langue == value)
				return;
			_langue = value;
		}
		
		public function get principal():int { return _principal; }
		
		public function set principal(value:int):void
		{
			if (_principal == value)
				return;
			_principal = value;
		}
		
		
		public function get email():String { return _email; }
		
		public function set email(value:String):void
		{
			if (_email == value)
				return;
			_email = value;
		}
		
		public function get prenom():String { return _prenom; }
		
		public function set prenom(value:String):void
		{
			if (_prenom == value)
				return;
			_prenom = value;
		}
		
		public function get nom():String { return _nom; }
		
		public function set nom(value:String):void
		{
			if (_nom == value)
				return;
			_nom = value;
		}
		
		
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
		public function get civilite():String { return _civilite; }
		
		public function set civilite(value:String):void
		{
			if (_civilite == value)
				return;
			_civilite = value;
		}
		
	}
}