package events
{
	import flash.events.Event;

	public class CampagneEvent extends Event
	{
		
		public static const UUID					:String = 'UUID'; 
		
		public static const IMPORTDATA				:String = 'IMPORTDATA';
		
		public static const SETDATATOCSV			:String = 'SETDATATOCSV';

		public static const LAUNCH_CAMPAGNE			:String = 'LAUNCH_CAMPAGNE';
		
		public static const DETAILS_CAMPAGNE		:String = 'DETAILS_CAMPAGNE';
		public static const DETAILS_STATUT			:String = 'DETAILS_STATUT';

		public static const LISTECAMPAGNE			:String = 'LISTECAMPAGNE';
		
		public static const NBRLIGNECAMPAGNE		:String = 'NBRLIGNECAMPAGNE';
		
		public static const CAMPAGNESAVED			:String = 'CAMPAGNESAVED';
		public static const CAMPAGNESENT			:String = 'CAMPAGNESENT';
		public static const CAMPAGNELISTELIGNE		:String = 'CAMPAGNELISTELIGNE';
		
		public static const LIGNEHISTORIQUE			:String = 'LIGNEHISTORIQUE';


		
		public function CampagneEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new CampagneEvent(type, bubbles, cancelable);
		}
		
	}
}