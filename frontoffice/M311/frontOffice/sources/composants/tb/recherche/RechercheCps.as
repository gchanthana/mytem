package composants.tb.recherche
{
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewUtil;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.events.DropdownEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
		
	public class RechercheCps extends RecherchePanel_IHM
	{
		private var cbxOpen : Boolean;//true si la combo peut etre ouverte
		
		private var myComboMask : TextField = new TextField();
		
		private var teta : uint = 0;//le decalage des popups
		private var delta : uint = 24;//la taille des items de la liste des sauvegardes
		private var nbMaxRecherhce : int = 3;//nombre max de fenetre de recherche (resultat)
		private var nbMaxRechercheSaved : int = 3;//nombre max de résultat de recherche suvegardée
	
		private var myResultatRechercheArray : Array = new Array();//tableau contenant les ref des fenetres de résultat
		
		[Bindable]
		private var last3RechercheArray : ArrayCollection = new ArrayCollection();//tableau contenant les ref des résultats sauvegardées 
		[Bindable]
		private var last3RechercheArraySorted : ArrayCollection = new ArrayCollection();//tableau contenant les chaines recherchées du tableau ci-dessus
		//ce tableau est trié//
		 
		//data provider de la liste
		private var listeSauvegarde : ArrayCollection = new ArrayCollection();
			
		private var myAlertBox : Alert;//une alert box
		private const alertMessage : String = ResourceManager.getInstance().getString('M311', 'Vous_avez_atteind_le_nombre_maximum_de_r');
		
		//params passé au fenêtre (résultat)
		private var _chaine : String; // la chaine en cours de recherche
		private var _datedebut : String;//la date de début
		private var _datefin : String;	//la date de fin
		
		[Embed(source='/assets/images/warning.png')]
	 	private var myAlertImg : Class;
		
		/**
		 * Constructeur
		 * */
		public function RechercheCps()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);			
		}
		
		/**
		 * Permet de netoyer la recherche et d'interrompre les remoting
		 * de reinitialiser la recherche
		 * */
		 public function clean():void{
		 	try{		 		 
		 		
		 		removePanelsResultatRecherche();	
		 		removeSessionResultatRecherche();
		 				 		
		 		last3RechercheArray = null;	 		
		 		last3RechercheArray = new ArrayCollection();
		 		
		 		last3RechercheArraySorted = null;
		 		last3RechercheArraySorted = new ArrayCollection();
		 		
		 				 		
		 		myResultatRechercheArray = null;
		 		myResultatRechercheArray = new Array();
		 		
		 		cbxInput.close();
		 		cbxInput.dataProvider = null;	
		 		autoCompleteTexte.dataProvider = null;
		 		cbxInput.text = "";	 		
		 		
		 		myImgRetour.visible = false;
		 			 			 						 		
		 	}catch(e : Error){
		 		trace("errrorr clean");
		 		trace(e.message,e.getStackTrace());
		 	}
		 }
		 
		 /**
		 * Permet de passer les params de periode au composants
		 * @param datedebut la date de debut de la periode
		 * @param datefin   la date de fin de la periode
		 * */		 		 
		 public function setParameters(datedebut : String , datefin : String ):void{
		 	_datedebut = datedebut;
			_datefin = datefin;						
		 }
		 
		/**
		 * Permet de cacher les panels de resultat de recherche 
		 * */
		 public function setVisibility():void{
		 	myResultatRechercheArray.forEach(processVisibilite);
		 }
		 
		 /**
		 * Permet de supprimer les fenetres de résultat 
		 * */
		 public function removePanelsResultatRecherche():void{
		 	myResultatRechercheArray.forEach(processRemove);
		 	myResultatRechercheArray.forEach(processDestroy);		 	
		 }
		 
		 /**
		 * Permet de deselctionner les éléments des fenêtre de recherche
		 * */
		 public function deselctRecherche():void{
		 	myResultatRechercheArray.forEach(processDeselct);
		 }
		 
		//------------- PRIVATE METHODS ----------------------------------------------------------------
		//initialise le composant
		private function init(fe : FlexEvent):void{	
			cbxOpen = false;					
			initListeRecherche();
			myImg.addEventListener(MouseEvent.CLICK,imgClikHandler);						
			myImgRetour.addEventListener(MouseEvent.CLICK,retourClickHandler);		
			//txtInput.addEventListener(Event.CHANGE,filtrerTableauSauvegarde);	
			//autoCompleteTexte.dataProvider = last3RechercheArray;
		}
		
		//-----Handlers----------------------------------------------------------------------------------
		//gere la touche enter pour le combobox
		private function cbxInputEnterHandler(fe : FlexEvent):void{
			
			 if (cbxInput.text.length != 0){
			 	
			 	_chaine = cbxInput.text;
			 	
			 	//cbxInput.text = _chaine;
			 	//cbxInput.selectedIndex = -1;
			 	
			 	//if (isInLast3SavedStrict(_chaine) == -1) cbxInput.selectedItem = _chaine;			 	
			 	
			 	afficherResultatRecherche();						 	
			 }else{			 
			 	afficherAlertBox(ResourceManager.getInstance().getString('M311', 'Vous_devez_saisir_un_crit_re_de_recherch'),ResourceManager.getInstance().getString('M311', 'Attention___'));
			 	
			 }
		}
		
		//autocomplete enter handler
		private function autoCompleteEnterHandler(fe : FlexEvent):void{
			
			 if (autoCompleteTexte.text.length != 0){
			 	
			 	_chaine = autoCompleteTexte.typedText;
			 	
			 	//cbxInput.text = _chaine;
			 	//cbxInput.selectedIndex = -1;
			 	
			 	//if (isInLast3SavedStrict(_chaine) == -1) cbxInput.selectedItem = _chaine;			 	
			 	afficherResultatRecherche();						 	
			 }else{			 
			 	afficherAlertBox(ResourceManager.getInstance().getString('M311', 'Vous_devez_saisir_un_crit_re_de_recherch'),ResourceManager.getInstance().getString('M311', 'Attention___'));			 	
			 }
		}
		
		//gere le click sur l'image Rechercher
		private function imgClikHandler(me : MouseEvent):void{
			if (autoCompleteTexte.text.length != 0){
				
				_chaine = autoCompleteTexte.typedText;						
				//cbxInput.selectedIndex = -1;
				//if (isInLast3SavedStrict(_chaine) == -1) cbxInput.selectedItem = _chaine;				
			 	afficherResultatRecherche();			 			
			 }else{ 			 	
			 	afficherAlertBox(ResourceManager.getInstance().getString('M311', 'Vous_devez_saisir_un_crit_re_de_recherch'),ResourceManager.getInstance().getString('M311', 'Attention___'));
			 }			
		}
				
		//rend la combo editable
		private function disableRecherche():void{
			if (myResultatRechercheArray.length == nbMaxRecherhce){
				cbxInput.editable = false;				
				cbxInput.setStyle("fillColors" , [0xff0000, 0xff6600, 0xff6600, 0xff0000])
				afficherAlertBox(alertMessage,ResourceManager.getInstance().getString('M311', 'Attention___'));				
			} 
		}
		
		//rend la commbo non editable
		private function enableRecherche():void{
			cbxInput.editable = true;
			cbxInput.clearStyle("fillColors");
		}
		
		//gere le click sur le bouton pour afficher le tableau de bord d'une recherche
		private function resultatClickHandler(re : ResultatRechercheEvent):void{
			var rechercheEvent : RechercheEvent = new RechercheEvent("InitTableauDeBord");
			rechercheEvent.ID = re.ID;	
			rechercheEvent.perimetreRecherche = re.perimetreRecherche;
			rechercheEvent.libelle = re.libelle;		
			dispatchEvent(rechercheEvent);
			myImgRetour.visible = true;
			myResultatRechercheArray.forEach(processDeselct);
		}
		
		//gere la fermeture des fenetres de résultat pour une recherche
		private function fermeturePanelRechercheHandler(re : ResultatRechercheEvent):void{
			var index : int = myResultatRechercheArray.indexOf(re.currentTarget);
			myResultatRechercheArray.splice(index,1);	
			enableRecherche();				
		}
		
		//gere le Event.CHANGE sur le textInput de recherche
		private function filtrerTableauSauvegarde(ev : Event):void{
			
			
		}
		
		//gere le Event.CHANGE sur la combobox de recherche
		private function filtreLast3saved(ev : Event):void{				 
				var index : int = isInLast3Saved(cbxInput.text);				
				
				//trace("index",index);
//				cbxInput.selectedIndex = index;
				if( index != -1){				
				
					if(!cbxOpen) cbxInput.open();
					cbxOpen = true;
				} 
			
			

			/* last3RechercheArray.filterFunction = processFiltre;
			last3RechercheArray.refresh();	
			*/						 
		}	
		
		//c'est ce qui se passe a chaque fois que la liste de la combobox se replie
		private function closeLast3saved(ev : DropdownEvent):void{
			cbxOpen = false;
///////////////////////////////////////////////////// section a haut risque //////////////////////////////////////////////////////////////
			try{
				if (ev.currentTarget.selectedIndex != -1){
					
					var	checkNb : Boolean = checkNumberResultatRechercheOK();				
					var idx : int = isInLast3RechercheArray(ev.currentTarget.selectedItem);			
					var re : ResultatRecherche_IHM = last3RechercheArray[idx]; 	
					var checkPopup : Boolean = re.isPopUp;		
					
				 	if (!checkNb){
				 		if (checkPopup){
				 			EffectProvider.FadeThat(re);
				 			PopUpManager.bringToFront(re);
				 		}else{			 			
				 			afficherAlertBox(alertMessage,ResourceManager.getInstance().getString('M311', 'Attention___'));
				 		}
				 	}else if (checkPopup){		 		
				 		EffectProvider.FadeThat(re);
				 		PopUpManager.bringToFront(re);
				 	}else{			 		
				 		PopUpManager.addPopUp(re,this);
				 		myResultatRechercheArray.push(re);	
				 		disableRecherche();
				 		re.setFocus();
				 	}				 	
				}	
			}catch(e : Error){
				trace("err : closeLast3saved");
			}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
			//cbxInput.selectedItem = _chaine;								
		}
		
		//Gere le click sur l'image de retour au tableau de bord du périmetre initiale
		private function retourClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event("RetourRechercheEvent"));
			myImgRetour.visible = false;
		}
		
		//--------------------------------------------------------------------------------------------------
		private function afficherAlertBox(text : String , titre : String):void{					
			
			myAlertBox = new Alert();
			myAlertBox.text = text;
			myAlertBox.title = titre;
			myAlertBox.iconClass = myAlertImg;		
			myAlertBox.includeInLayout = true;				
			
			ConsoviewUtil.scale(DisplayObject(myAlertBox),ConsoViewModuleObject.W_ratio,ConsoViewModuleObject.H_ratio);
					
			PopUpManager.addPopUp(myAlertBox,UIComponent(parentApplication),true);						     		
			PopUpManager.centerPopUp(myAlertBox);
		}
		
		
		//affichage du resultat de la recherche
		private function afficherResultatRecherche():void{
			var idx : int = isInMyResultatRechercheArray(_chaine);
			var idx2 : int =isInLast3RechercheArray(_chaine);
			
			//on regarde si la recherche n'est pas déja affichée
			if (idx >= 0){
				//si oui on lui donne le focus
				EffectProvider.FadeThat(ResultatRecherche_IHM(myResultatRechercheArray[idx]));				
				PopUpManager.bringToFront(ResultatRecherche_IHM(myResultatRechercheArray[idx]));
			}else if(idx2 >= 0){//sinon on regarde si la recherche est dans le tabelau des 3 dernieres recherches
				
				
					//si oui on regarde si on peut l'afficher 					
					if (checkNumberResultatRechercheOK()){
						//si oui on l'affiche
						var ihm : ResultatRecherche_IHM = ResultatRecherche_IHM(last3RechercheArray.getItemAt(idx2));
						ConsoviewUtil.scale(DisplayObject(ihm),ConsoViewModuleObject.W_ratio,ConsoViewModuleObject.H_ratio);
						 
			
						PopUpManager.addPopUp(ihm,UIComponent(parentApplication));
						
		 				myResultatRechercheArray.push(ihm);	
		 				disableRecherche();
		 				 						
					}else{
						//sinon on affiche un message d'erreur
						afficherAlertBox(alertMessage,ResourceManager.getInstance().getString('M311', 'Attention___'));
					}
					
					
			}else if(checkNumberResultatRechercheOK()){//sinon on regarde si on peut l'afficher 
					
						//si oui on l'ajoute au tableau de 3 derniere recherche et au tableaux qui compte le nb de recherche max;
						var myResultatRecherche : ResultatRecherche_IHM;
						myResultatRecherche = PanelRechercheFactory.createPanelRecherche(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE,_chaine,_datedebut,_datefin);
						trace("-----------------------------------------"+CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE);
						myResultatRechercheArray.push(myResultatRecherche);		
						disableRecherche();				
						callLater(configPanel,[myResultatRecherche]);	
						
			}else{	
				afficherAlertBox(alertMessage,ResourceManager.getInstance().getString('M311', 'Attention___'));
				//cbxInput.selectedIndex = -1;
				//cbxInput.text = _chaine;
			}
						
		}	
		
		//Retourne l'index de la fenêtre de resultat stockée dans myResultatRechercheArray si la chaine passer en parametre y est présente sinon 
		//retourne -1;
		private function isInMyResultatRechercheArray(text : String):int{
			var len : int =  myResultatRechercheArray.length;
			var i : int;
			for ( i = 0; i<len;i++){
				if (myResultatRechercheArray[i].chaine == text)
					return i;
			}			
			return -1;		 
		}
		
		
		//idem mais pour le tableau des 3 dernières recherches
		private function isInLast3RechercheArray(text : String):int{
			var obj:String;
			var idx : int;
		  	
		  	idx = 0;
		  	for (obj in last3RechercheArray){
		  		if(last3RechercheArray[obj].chaine == text){
		  			return idx;
		  		}		  		
		  		idx++;
		  	}
			return -1;
		}	
		
		//Retourne true si on peut encore afficher des fenêtres de résultat sinon retourne false (on peut afficher nbMaxRecherhce fenêtres)
		private function checkNumberResultatRechercheOK():Boolean{			
			if (myResultatRechercheArray.length < nbMaxRecherhce){				
				return true;
			}else{				
				return false;
			}
		}
		
		//sauvegarde la recherche
		private function sauvegarderRecherche(panel : ResultatRecherche_IHM):void{
			
			if (last3RechercheArray.length < nbMaxRechercheSaved){		
	    		 
	    		last3RechercheArray.addItemAt(panel,0);	
	    		constsructListe3LastToDsiplay();
						
			}else{
				var obj : Object;
				
				var s : String;
				obj  = last3RechercheArray.removeItemAt(last3RechercheArray.length-1);
				s = obj.chaine;
				
				if (!obj.isPopUp)
					removeUnUsedSessionResultatRecherche(s);					 				
				constsructListe3LastToDsiplay();
				
			}					
			
		}
		
		//sauvegarder liste recherche
		private function sauvegarderListeRecherche(panel : ResultatRecherche_IHM):void{			
			if (listeSauvegarde.length < nbMaxRechercheSaved){		
	    		listeSauvegarde.addItemAt(panel,0);						
			}else{
				var obj : Object;
				
				var s : String;
				obj  = listeSauvegarde.removeItemAt(last3RechercheArray.length-1);
				s = obj.chaine;
				
				if (!obj.isPopUp)
					removeUnUsedSessionResultatRecherche(s);					 
				listeSauvegarde.addItemAt(panel,0);				
			}								
		}
		
		
		//A partir du tableau last3RechercheArray constuit un tableau trié des chaines déja recherchées (c'est ce qui sera affiché dans la liste de la combobox)
		private function constsructListe3LastToDsiplay():void{
				var ar : Array = new Array();
				for (var i : int = 0 ; i < last3RechercheArray.length ; i++)
					ar.push((last3RechercheArray[i].chaine));
									
				last3RechercheArraySorted.source = ar;
	    		
	    		var sort:Sort = new Sort();   
    			sort.fields = [new SortField(null, true)];
     			last3RechercheArraySorted.sort = sort;
       			last3RechercheArraySorted.refresh(); 
       			cbxInput.dataProvider = last3RechercheArraySorted;
       			autoCompleteTexte.dataProvider = last3RechercheArraySorted;	       			
		}
		
		//config du panel recherche
		private function configPanel(panel : ResultatRecherche_IHM):void{
			
			panel.focusEnabled = true;
			
			panel.showCloseButton = true;
			panel.addEventListener("ResultatClickEvent",resultatClickHandler);			
			panel.addEventListener("fermeturePanelRecherche",fermeturePanelRechercheHandler);			
			
			panel.x = Math.round((parentDocument.width - panel.width) / 2) + teta;
			panel.y = Math.round((parentDocument.height - panel.height) / 2)+ teta;
			
			teta = teta +20
			if (teta > (nbMaxRecherhce-1)*20) teta = 0;			 
			ConsoviewUtil.scale(DisplayObject(panel),ConsoViewModuleObject.W_ratio,ConsoViewModuleObject.H_ratio);
			 
			
			PopUpManager.addPopUp(panel,this);
			
			sauvegarderRecherche(panel);				
			trace(myResultatRechercheArray.length);
		}
				
		//config la liste des 3 dernieres recherches
		private function initListeRecherche():void{									
			cbxInput.dataProvider = last3RechercheArraySorted;		
			
			autoCompleteTexte.dataProvider = last3RechercheArraySorted;
			autoCompleteTexte.labelField = "chaine";
			autoCompleteTexte.addEventListener(FlexEvent.ENTER,autoCompleteEnterHandler);
			
			cbxInput.labelField = "chaine";		
			cbxInput.addEventListener(Event.CHANGE,filtreLast3saved);
			cbxInput.addEventListener(DropdownEvent.CLOSE,closeLast3saved);						 	
			cbxInput.addEventListener(FlexEvent.ENTER,cbxInputEnterHandler);
			
		}	
		
		
		//retourne l'index de la chaine entrée dans la combo si elle est présente dans la liste sinon retourne -1					
		private function isInLast3Saved(s : String):int{	
			if (last3RechercheArraySorted)			
				for (var i :uint = 0; i < last3RechercheArray.length; i++){
					
					if ((last3RechercheArraySorted[i].substr(0,s.length).toLowerCase().search(s.toLowerCase()) > -1)
						&&(s.length > 0))
						 return i;
				}
				return -1;			
		}
		
		//retourne -1 si la chaine passée en parametre n'est pas == a une des chaine du tableau sinon retourne l'index de la chaine dans le tableau
		private function isInLast3SavedStrict(s : String):int{	
			if (last3RechercheArraySorted)			
				for (var i :uint = 0; i < last3RechercheArray.length; i++){					
					if ((last3RechercheArraySorted[i].toLowerCase() == s.toLowerCase())
						&&(s.length > 0))
						 return i;
				}
				return -1;			
		}
		
		//filtre pour le grid des sous-tete
		private function processFiltre(value : Object):Boolean{
		
				if (String(value.chaine.toLowerCase()).search((cbxInput.text).toLowerCase()) != -1) {
					
					return (true);
				} else {
					return (false);
				}
			
		}		
				
		//fonctions foreach
		
		//cache la fenêtre de résultat si elle est visible sinon la montre (f appelante : setVisibility)
		private function processVisibilite(element:*, index:int, arr:Array):void{
			if (element.visible) element.visible = false;
			else element.visible = true;
		}
		
		//efface les fenêtres "popped-up" par le pop-up manager (f appelante : removePanelsResultatRecherche)
		private function processRemove(element:*, index:int, arr:Array):void{
			PopUpManager.removePopUp(IFlexDisplayObject(element));			
		}
		
		//deselectionne la recherche afin de pouvoir clicker dessus a nouveau
		private function processDeselct(element:*, index:int, arr:Array):void{
			
			var lastIndex : int = DataGrid(element.myGrid).selectedIndex;			
			if (lastIndex != -1){
				DataGrid(element.myGrid).selectedIndex = -1;			
				DataGrid(element.myGrid).scrollToIndex(lastIndex);													
			}			
		}
				
		//supprime les reférence sur les fentres de resultat des recherches (f appelante : removePanelsResultatRecherche)
		private function processDestroy(element:*, index:int, arr:Array):void{
			element = null;
		}
			
		//_______________________- un peu de remoting -_______________________________//
		
		//detruit la structure en session resultatRechercheTb
		private function removeSessionResultatRecherche():void{
			var opt : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M311.recherche.facade",
																			"deleteSessionResultatRecherche",
																			removeSessionResultatRechercheResultHandler);
			RemoteObjectUtil.callService(opt);
		}
		
		
		private function removeSessionResultatRechercheResultHandler(re : ResultEvent):void{
			trace("session deleted");
		}
		
		private function removeSessionResultatRechercheFaultHandler(fe : FaultEvent):void{
			trace("erreur supression Session ResultatRechercheTb");
		}
		//______________________________________________________________________________//
		
		//detruit un element de la structure en session resultatRechercheTb
		private function removeUnUsedSessionResultatRecherche(cle : String):void{
			var opt : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M311.recherche.facade",
																			"deleteUnUsedSessionResultatRecherche",
																			removeUnUsedSessionResultatRechercheResultHandler,null);
			RemoteObjectUtil.callService(opt,cle);
		}
		
		private function removeUnUsedSessionResultatRechercheResultHandler(re : ResultEvent):void{
			trace("session UnUsed deleted");
		}
		
		private function removeUnUsedSessionResultatRechercheFaultHandler(fe : FaultEvent):void{
			trace("erreur supression Session UnUsed Resultat Recherche");
		}
				
	}
}