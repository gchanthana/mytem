package composants.tb.recherche
{
	public class PanelRechercheFactory
	{
		public static function createPanelRecherche(type : String, chaine :  String, datedebut : String , datefin : String):ResultatRecherche_IHM{
			switch(type.toUpperCase()){
				case "GROUPE" : return new ResultatRechercheGroupeRacine(chaine,datedebut,datefin);break;	
				case "GROUPELIGNE" : return new ResultatRechercheGroupeLigne(chaine,datedebut,datefin);break;
				case "GROUPEANA" : return new ResultatRechercheGroupeAnalitique(chaine,datedebut,datefin);break;				
				default : throw new PanelNotFoundError("Impossible de créer le panel " + type);break; 
			}
			return null;			
		}
	}
}