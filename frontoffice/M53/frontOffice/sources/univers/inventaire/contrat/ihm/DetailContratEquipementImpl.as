package univers.inventaire.contrat.ihm
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Button;
	
	
	[Bindable]
	public class DetailContratEquipementImpl extends VBox implements IFenetreContrat
	{
		public var btValider:Button;
		
		private var _selectedContrat:Object;
						
		public function DetailContratEquipementImpl()
		{
		}

		public function getDisplayObject():DisplayObject
		{	 
			return this;
		}
		
		public function btValiderClickHandler(event:MouseEvent):void
		{
			//TODO: implement function
		}
		
		public function initialiser():void
		{
			//TODO: implement function
		}		

		public function set selectedContrat(value:Object):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():Object
		{
			return _selectedContrat;
		}

	}
}