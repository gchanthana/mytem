package service.diffusion.modifierdiffusion
{
	import composants.util.ConsoviewAlert;
	
	import event.diffusion.DiffusionEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;

	internal class ModifierDiffusionModel extends EventDispatcher
	{

		public function ModifierDiffusionModel()
		{
		}

		internal function modifierDiffusion(re:ResultEvent):void
		{
			if (re.result == 1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M331', 'Votre_diffusion___bien__t__modifi_e'));
				dispatchEvent(new DiffusionEvent(DiffusionEvent.REFRESH_LIST_DIFFUSION_EVENT,true));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M331', 'Une_erreur_s_est_produit_lors_de_la_modi'), ResourceManager.getInstance().getString('M331', 'Erreur_modification_diffusion'));
			}
		}
	}
}
