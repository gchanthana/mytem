package univers.parametres.parametrelogin.event
{
	import flash.events.Event;

	public class RevendeursAutorisesServiceEvent extends Event
	{
		//RASE = RevendeursAutorisesServiceEvent
		public static const RASE_REQUEST_COMPELTE:String = "raseRequestCompelte"; 
		public static const RASE_REQUEST_ERROR:String = "raseRequestError";
		
		public function RevendeursAutorisesServiceEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		
		override public function clone():Event
		{
			return new RevendeursAutorisesServiceEvent(type,bubbles,cancelable);
		}
	}
}