package utils.abstract
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class AbstractRemoteService
	{
		private var _ApiConsoleGestion		:String = "fr.consotel.consoview.M28.APIconsolegestion";
		
		public function AbstractRemoteService()
		{
		}
		public function doRemoting(fct:Function, key:String, code:String, data:Object=null):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				_ApiConsoleGestion,
				"execute",
				fct);
			if(data==null)
				data=new Object();
			RemoteObjectUtil.callService(op,key,code,data);
		}
	}
}