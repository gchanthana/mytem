package ihm.destinataire.perimetre
{
	import appli.events.PerimetreDataProxyEvent;
	import flash.events.Event;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.IViewCursor;
	import mx.collections.XMLListCollection;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import mx.events.TreeEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import rapport.utils.perimetreselector.GenericPerimetreTree;
	import rapport.utils.perimetreselector.PerimetreEvent;
	import rapport.utils.perimetreselector.PerimetreTreeEvent;

	public class PerimetreTree_V2 extends GenericPerimetreTree
	{
		private var currentOpenNode:Object;
		protected var rootId:Number;
		protected var currentIndex:Number=0;
		private var _orgActif:Boolean=true;
		private var _orgInactif:Boolean=true;
		private var getChildOp:AbstractOperation;
		private var nodeToLoadChildren:XML;

		public function PerimetreTree_V2()
		{
			trace("(GenericPerimetreTree) Instance Creation");
			this.itemRenderer=new ClassFactory(ihm.destinataire.perimetre.PerimetreTreeItemRenderer_V2);
			this.addEventListener(FlexEvent.CREATION_COMPLETE, afterCreationComplete);
			nodeToLoadChildren=null;
		}

		override protected function afterCreationComplete(event:Event):void
		{
			currentOpenNode=null;
			addEventListener(TreeEvent.ITEM_OPENING, onItemOpening);
			setStyleProps();
		}

		override public function updateDataProvider(xmlDataProvider:XML, dataLabel:String):void
		{
			this.enabled=false;
			dataProvider=null;
			labelField="@" + dataLabel;
			dataProvider=xmlDataProvider;
			rootId=Number(xmlDataProvider.@NID);
			callLater(dataProviderUpdated);
		}

		public function setTreeDataProvider(nodesDataSet:Object, dataLabel:String, dataDesc:ITreeDataDescriptor, modeActif:String=null):void
		{
			trace("Perimetre_V2 setTreeDataProvider");
			// Détermnine le filtrage en fonction de l'état
			this.enabled=false;
			rootId=0;
			dataProvider=null;
			dataProvider=nodesDataSet[0];
			// gestion de l'affichage tous/actif/inactif
			labelField="@" + dataLabel;
			dataDescriptor=null;
			dataDescriptor=dataDesc;
			(dataDescriptor as FilteredTreeDataDescriptor).addEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, onChildDataAdded);
			rootId=XML(dataProvider[0]).@NID;
			callLater(dataProviderUpdated);
		}

		override public function setDataProvider(nodesDataSet:Object, dataLabel:String, dataDesc:ITreeDataDescriptor):void
		{
			this.enabled=false;
			rootId=0;
			dataProvider=null;
			dataProvider=nodesDataSet[0];
			labelField="@" + dataLabel;
			dataDescriptor=null;
			dataDescriptor=dataDesc;
			(dataDescriptor as FilteredTreeDataDescriptor).addEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, onChildDataAdded);
			rootId=XML(dataProvider[0]).@NID;
			callLater(dataProviderUpdated);
		}

		/**
		 * Handler utilisé par le callLater de ce Tree
		 * */
		override protected function dataProviderUpdated():void
		{
			//this.visible = true;
			this.enabled=true;
			dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.DATA_PROVIDER_SET));
		}

		/**
		 * Ne pas mettre en protected car n'est utilisé que dans cette classe
		 * */
		override protected function onItemOpening(evt:TreeEvent):void
		{
			var node:XML=(evt.item as XML);
			var childrenList:XMLList=node.children();
			currentIndex=scrollPositionToIndex(horizontalScrollPosition, verticalScrollPosition);
			if (parseInt(node.@NTY, 10) > 0)
			{
				if (childrenList.length() == 0)
				{
					currentOpenNode=node;               
					loadNodeChildren(XML(evt.item));
					expandItem(node, false);
				}
			}
		}

		public function loadNodeChildren(node:XML):void
		{
			nodeToLoadChildren=node;
			getChildOp=RemoteObjectUtil.getOperation("fr.consotel.consoview.access.PerimetreManager", "getNodeChild_v2", addNodeChildren);
			RemoteObjectUtil.invokeService(getChildOp, parseInt(nodeToLoadChildren.@NID, 10));
		}

		private function addNodeChildren(event:Event):void
		{
			var i:int;
			var childCollection:XMLListCollection=new XMLListCollection(((event as ResultEvent).result as XML).children());
			for (i=0; i < childCollection.length; i++)
			{
				if (nodeToLoadChildren)
				{
					nodeToLoadChildren.appendChild(childCollection.getItemAt(i));
				}
			}
			nodeToLoadChildren=null;
			dispatchEvent(new PerimetreDataProxyEvent(PerimetreDataProxyEvent.NODE_CHILD_LOADED));
			onChildDataAdded(null);
		}

		override protected function onItemOpen(evt:TreeEvent):void
		{
			var node:XML=(evt.item as XML);
			var childrenList:XMLList=node.children();
			if (childrenList.length() == 0)
				expandItem(node, false);
			else
				expandItem(node, true);
		}

		override protected function onChildDataAdded(event:PerimetreEvent):void
		{
			var nombre:int=0;
			if (dataProvider != null)
			{
				var arr:Array=openItems as Array;
				if (dataProvider[0])
				{
					var localNodID:Number=XML(dataProvider[0]).@NID;
					var collection:XMLListCollection=new XMLListCollection(XMLList(dataProvider));
					var cursor:IViewCursor=collection.createCursor();
					if (cursor.current && localNodID == (cursor.current).@NID)
					{
						nombre=-1;
					}
					else
					{
						nombre=localNodID;
					}
					if (nombre != rootId || (currentOpenNode == dataProvider[0]))
					{
						validateNow();
						var tmpData:Object=dataProvider;
						var i:int=0;
						var size:int=(tmpData as XMLListCollection).length;
						while (i < size && XML(tmpData[i]).@NID != rootId)
						{
							i++;
						}
						var dataset:XML=tmpData[i];
						dataProvider=dataset;
						openItems=arr;
						refreshData=true;
					}
				}
				expandItem(currentOpenNode, false);
				expandItem(currentOpenNode, true);
				invalidateList();
				validateNow();
			}
		}
		private var refreshData:Boolean=false;

		override protected function commitProperties():void
		{
			super.commitProperties();
			if (refreshData)
			{
				refreshData=false;
				scrollToIndex(currentIndex);
				verticalScrollPosition=currentIndex;
				getFocus();
			}
		}
	}
}
