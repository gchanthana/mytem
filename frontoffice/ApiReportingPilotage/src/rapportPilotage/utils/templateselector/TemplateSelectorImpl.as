package rapportPilotage.utils.templateselector
{
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.event.TemplateEvent;
	import rapportPilotage.service.templateselector.TemplateSelectorService;
	import rapportPilotage.utils.SelectorAbstract;
	import rapportPilotage.utils.serialization.json.*;
	import rapportPilotage.vo.Parametre;
	import rapportPilotage.vo.SelectorParametre;
	import rapportPilotage.vo.ValeurSelector;

	public class TemplateSelectorImpl extends SelectorAbstract
	{
		
		[Bindable]public var templateSelectorService:TemplateSelectorService;
		[Bindable]public var comboBoxTemplate:ComboBox;
		
		public var hboxTemplate:HBox;	
		public var arrayCleLibelle :Object;
		
		public function TemplateSelectorImpl(selector:Object=null)
		{
			super(selector);
		}
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
			    this.CleAndLibelleParametreComposant=selector.cleJson;
								
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
				
			}
		}
		
		/* appeler le service pour initialiser le dataprovider de comboBox */
		
		public function templateselector_creationCompleteHandler(event:FlexEvent):void
		{
			templateSelectorService=new TemplateSelectorService();
			templateSelectorService.model.addEventListener(RapportEvent.USER_TEMPLATE_SELECTOR_EVENT,fillData)
			templateSelectorService.getValuesTemplate(selectedRapport.idRapportRacine);
		}
		
		/* initialiser le dataprovider puis afficher l'image de help */
		
		private function fillData(event:RapportEvent):void
		{
			comboBoxTemplate.dataProvider=templateSelectorService.model.templates;
			
			if(templateSelectorService.model.templates.length<2) 
			{
				visible =false;
				includeInLayout = false;
			}
			else
			{
				visible = true;
				includeInLayout = true;
			}
			showHelp();
		}
		
		// permert de déclencher un evenement avec l'id template 
		public function comboBoxTemplate_changeHandler(event:ListEvent):void
		{
			var myEvent:TemplateEvent = new TemplateEvent(TemplateEvent.DATA_TRANSFER,(comboBoxTemplate.selectedItem as ValeurSelector).id );
			comboBoxTemplate.dispatchEvent(myEvent);
		}
	
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			
			var parametre :Parametre=  new Parametre();
			parametre.value=[comboBoxTemplate.selectedItem.valeur];
			parametre.cle =arrayCleLibelle[0].NOM_CLE;
			parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			parametre.is_system = arrayCleLibelle[0].IS_SYSTEM;
			parametre.typeValue=this.typeParametreComposant.toUpperCase();
			
			tabValeur[0]=parametre;
			return tabValeur;
		}
	}
}