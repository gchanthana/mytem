package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import session.SessionUserObject;
	
	public class EquipementService extends EventDispatcher
	{
		public var listEngagement		:ArrayCollection;
		public var listeEquipements		:ArrayCollection;
		public var listeAccessoires		:ArrayCollection;
		public var listeSimSpare		:ArrayCollection;
		
		[Bindable]
		public var TERMINAUX			:ArrayCollection = new ArrayCollection();
		public var ENGAGEMENTS			:ArrayCollection;
		
		public var listeFeature			:ArrayCollection;
		public var listePhotos			:ArrayCollection;

		public var simcard				:EquipementsElements;
		public var cartesimAssociate	:ArrayCollection;
		
		private var _isAccPort			:Boolean = false;
		private var _isCatalogueClient	:Boolean = false;
		private var _isClient			:Boolean = false;
		
		public function EquipementService()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC METHODE - APPEL DE PROCEDURE
		//--------------------------------------------------------------------------------------------//		
		
		//obtient les accessoires compatibles avec l'appareil sélectionné
		public function fournirEngagement(idOperateur:int): void
	    {
	    	listEngagement = new ArrayCollection();
			
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.OperateurService",
	    																		"getInfosClientOperateurActuelV2",
	    																		fournirEngagementResultHandler);		
	    	RemoteObjectUtil.callService(op,idOperateur);
	    }
		
		public function fournirNouvelleEngagement(idOperateur:int, isClient:Boolean = false): void
		{
			listEngagement = new ArrayCollection();
			
			_isClient = isClient;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.OperateurService",
																				"getInfosClientOperateurActuelV2",
																				fournirNouvelleEngagementResultHandler);		
			RemoteObjectUtil.callService(op,idOperateur);
		}
		
		public function fournirReengagement(idOperateur:int):void
		{
			listEngagement = new ArrayCollection()
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.OperateurService",
																				"getInfosClientOperateurActuelV2",
																				fournirReengagementResultHandler);		
			RemoteObjectUtil.callService(op,idOperateur);
		}
		
		public function fournirListeEquipementsClient(idPoolGestionnaire:int, idProfilEquipement:int, idFournisseur:int, idSegment:int, clefRecherche:String = ''): void
		{
			listeEquipements 	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirListeEquipementsClientV2",
																				fournirListeEquipementsClientResultHandler);
			RemoteObjectUtil.callService(op,idPoolGestionnaire,
											idProfilEquipement,
											idFournisseur,
											idSegment,
											clefRecherche);	
		}
	    
		public function fournirListeTerminaux(idGestionnaire:int, idPoolGestionnaire:int, idProfilEquipement:int, idFournisseur:int, clefRecherche:String = ""): void
		{
			listeEquipements 	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirListeEquipements",
																				fournirListeTerminauxResultHandler);
			RemoteObjectUtil.callService(op,idGestionnaire,
											idPoolGestionnaire,
											idProfilEquipement,
											idFournisseur,
											clefRecherche,
											SessionUserObject.singletonSession.IDSEGMENT);	
		}
		
		public function fournirListeTerminauxV2(idGestionnaire:int, idPoolGestionnaire:int, idProfilEquipement:int, idFournisseur:int, clefRecherche:String = ""): void
		{
			listeEquipements 	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirListeTerminaux",
																				fournirListeTerminauxResultHandler);
			RemoteObjectUtil.callService(op,idGestionnaire,
											idPoolGestionnaire,
											idProfilEquipement,
											idFournisseur,
											clefRecherche,
											SessionUserObject.singletonSession.IDSEGMENT);	
		}
		
	    //obtient les accessoires compatibles avec l'appareil sélectionné
		public function getProductRelated(idEquipement:int, idtypeccommande:int, chaine:String = ""): void
	    {
	    	_isAccPort 			= true;
		    listeAccessoires 	= new ArrayCollection();

	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.EquipementService",
	    																		"getProductRelated",
	    																		fournirListeAccessoiresResultHandler);		
	    	RemoteObjectUtil.callService(op,idEquipement,
											idtypeccommande,
									    	1,
									    	chaine,
									    	3);//CHANGER '3' POUR L'INTERNATIONNALISATION (LE RECUPERER EN SESSION)
	    }
		
		//obtient les accessoires compatibles avec l'appareil sélectionné
		public function fournirListeAccessoiresCompatibles(idEquipement:int, idtypeccommande:int, chaine:String = ""): void
		{
			listeAccessoires = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"getProductRelated",
																				fournirListeAccessoiresCompatiblesResultHandler);		
			
			RemoteObjectUtil.callService(op,idEquipement,
											idtypeccommande,
											1,
											chaine,
											3);//CHANGER '3' POUR L'INTERNATIONNALISATION (LE RECUPERER EN SESSION)
		}
	    
	    public function fournirListeAccessoiresCatalogueClient(idrevendeur:Number, idtypeccommande:int, clef:String, segment:String="MOBILE"): void
	    {			
			listeAccessoires 	= new ArrayCollection();
	    	_isAccPort			= false;
	    	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirListeAccessoiresCatalogueClient", 
																				fournirListeAccessoiresCatalogueClientResultHandler);
			RemoteObjectUtil.callService(op,idrevendeur,
											idtypeccommande,
											clef,
											SessionUserObject.singletonSession.IDSEGMENT);
	    		
		}
		
		public function fournirListeAccessoires(idrevendeur:Number, idtypeccommande:int, clef:String = "", segment:String="MOBILE"): void
		{			
			listeAccessoires 	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirListeAccessoiresCatalogueClient", 
																				fournirListeAccessoiresResultHandler);
			
			RemoteObjectUtil.callService(op,idrevendeur,
											idtypeccommande,
											clef,
											SessionUserObject.singletonSession.IDSEGMENT);
			
		}
		
		public function fournirSimCard(idRevendeur:Number, idOperateur:Number, idTypeCommande:int):void
		{
			simcard = new EquipementsElements();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirCarteSim", 
																				fournirSimCardResultHandler);
			
			RemoteObjectUtil.callService(op, idRevendeur, idOperateur, idTypeCommande);
		}

	    public function manageLivraisonEquipements(listeArticles:XML):void
	    {	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.EquipementService",
	    																		"manageLivraisonEquipements",
	    																		manageLivraisonEquipementsResultEvent);
																			    	
			RemoteObjectUtil.callService(op,listeArticles);
	    }

	    public function fournirAssociatesSim(idsSousTete:Array, idPool:int):void
		{
			cartesimAssociate = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirAssociatesSim", 
																				fournirAssociatesSimResultHandler);
			RemoteObjectUtil.callService(op,idsSousTete,
											idPool);
		}
		
		public function desaffectesimterm(articles:Array):void
		{			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"desaffectesimterm", 
																				desaffectesimtermResultHandler);
			RemoteObjectUtil.callService(op,articles);
		}
		
		public function getProductFeature(idEquipement:int): void
		{
			listeFeature 	= new ArrayCollection();
			listePhotos 	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"getProductFeature",
																				getProductFeatureResultHandler);		
			RemoteObjectUtil.callService(op, idEquipement);	
		}
		
		public function fournirSimInSpare(idoperateur:int, idpool:int): void
		{
			listeSimSpare 	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirSimInSpare",
																				fournirSimInSpareResultHandler);		
			RemoteObjectUtil.callService(op, idoperateur, idpool);	
		}

		public function fournirTerminauxSimCardEngagements(idpool:int, idprofil:int, idrevendeur:int, idoperateur:int, idsegment:int, isNewCommande:int): void
		{
			TERMINAUX.removeAll();
			ENGAGEMENTS = new ArrayCollection();
			
			simcard 	= new EquipementsElements();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.EquipementService",
																				"fournirTerminauxSimCardEngagements",
																				fournirTerminauxSimCardEngagementsResultHandler);		
			
			RemoteObjectUtil.callService(op, idpool, idprofil, idrevendeur, idoperateur, idsegment, isNewCommande);	
		}
		
		public function fournirTerminauxSimCardEngagementsForSAV(idpool:int, idprofil:int, idrevendeur:int, idoperateur:int, idsegment:int, isNewCommande:int, fromStock:int): void
		{
			TERMINAUX.removeAll();
			ENGAGEMENTS = new ArrayCollection();
			
			simcard 	= new EquipementsElements();
			if (fromStock==1)
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M16.v2.EquipementService",
					"fournirTerminauxSimCardEngagements",
					fournirTerminauxSimCardEngagementsResultHandler);		
			else
				var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoview.M16.v2.EquipementService",
					"fournirTerminauxSimCardEngagements",
					fournirTerminauxSimCardEngagementsFromStockResultHandler);
			
			RemoteObjectUtil.callService(op, idpool, idprofil, idrevendeur, idoperateur, idsegment, isNewCommande, fromStock);	
		}
		

		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE - RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//

		private function fournirEngagementResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		if(SessionUserObject.singletonSession.COMMANDE != null)
	    		{
	    			if(SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.NOUVELLES_LIGNES ||
	    				SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.NVL_LIGNE_FIXE)
		    			listEngagement = attributEngagementNew(re.result[0] as Object);
		    		else
		    			listEngagement = attributEngagementRenouvellement(re.result[0] as Object);
		    	}
	    		else
	    			listEngagement = attributEngagementRenouvellement(re.result[0] as Object);

	    		dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ENGAGEMENTS));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
	    }
		
		private function fournirNouvelleEngagementResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(!_isClient)
					listEngagement = attributEngagementNew(re.result[0] as Object);
				else
					listEngagement = attributEngagementClient(re.result[0] as Object);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ENGAGEMENTS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}
		
		private function fournirReengagementResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				listEngagement = attributEngagementRenouvellement(re.result[0] as Object);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ENGAGEMENTS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}
		
		private function fournirListeEquipementsClientResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var equipements	:ArrayCollection = new ArrayCollection(re.result as Array);
				var len			:int = equipements.length;
				
				if(len > 0)
				{					
					for(var i:int = 0;i < len;i++)
					{
						if(equipements[i] as ArrayCollection)
						{
							var eqpts:ArrayCollection = equipements[i];
							var lenEq:int= eqpts.length;
							
							for(var j:int = 0;j < lenEq;j++)
							{
									if(i == 0)
										listeEquipements.addItem(EquipementsElements.formatTerminal(eqpts[j]));
									else
										listeEquipements.addItem(EquipementsElements.formatAccessoire(eqpts[j]));
							}
						}
					}
					
					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_EQUIPEMENTS));
				}
				else
					ConsoviewAlert.afficherError("Aucun équipements", 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des__quipements'), 'Consoview');
		}
		
		private function fournirListeTerminauxResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result.length > 0)
				{
					var arrayResult	:ArrayCollection = re.result as ArrayCollection;
					var len			:int = arrayResult.length;
					
					for(var i:int = 0;i < len;i++)
					{
						if(arrayResult[i].IDTYPE_EQUIPEMENT != 71)
							listeEquipements.addItem(EquipementsElements.formatTerminal(arrayResult[i]));
					}
					
					if(listeEquipements.length == 1)
						(listeEquipements[0] as EquipementsElements).SELECTED = true;
					
					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_EQUIPEMENTS));
				}
				else
					ConsoviewAlert.afficherError("Aucun équipements", 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_de_la_liste_des__quipements'), 'Consoview');
		}

	    private function fournirListeAccessoiresCatalogueClientResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		if((re.result as ArrayCollection) != null)
	    		{
	    			if((re.result as ArrayCollection).length > 1)
	    			{
	    				if(re.result[0].hasOwnProperty("ERREUR"))
	    				{
	    					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACCESSOIRES));
	    					return;
	    				}
	    			}
	    			else
	    			{
	    				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACCESSOIRES));
	    				return;
	    			}
	    		}
	    		else
	    		{
					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACCESSOIRES));
					return;
	    		}
			}
	    }
		
		private function fournirListeAccessoiresResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				if(re.result.length > 0)
				{
					var arrayResult	:ArrayCollection = re.result as ArrayCollection;
					var len			:int = arrayResult.length;
					
					for(var i:int = 0;i < len;i++)
					{
						listeAccessoires.addItem(EquipementsElements.formatAccessoire(arrayResult[i]));
					}
					
					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACCESSOIRES));
				}
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}
		
		private function fournirListeAccessoiresCompatiblesResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				var arrayResult	:ArrayCollection = re.result as ArrayCollection;
				var len			:int = arrayResult.length;
				
				for(var i:int = 0;i < len;i++)
				{
					listeAccessoires.addItem(EquipementsElements.formatAccessoire(arrayResult[i]));
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_ACCESSOIRES));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}
		
		private function fournirSimCardResultHandler(re:ResultEvent):void 
		{
			if(re.result)
				simcard =  EquipementsElements.formatSim(re.result[0] as Object) ;	
			
			dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_SIM));	
		}
		
		private function manageLivraisonEquipementsResultEvent(re:ResultEvent):void
		{
    		if(re.result > 0)	    	
	    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_MANAGED));
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Mise___jour_des_articles_impossible__'), 'Consoview');
		}
		
		private function fournirAssociatesSimResultHandler(re:ResultEvent):void
		{
			if(re.result)
	    		cartesimAssociate = new ArrayCollection(re.result as Array);
			
			dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_SIMASSOCIATE));	
		}
		
		private function desaffectesimtermResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if(re.result > 0)
					dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_MAJ_SIMTERM));	
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'La_mise___jour_des__quipements_impo'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'La_mise___jour_des__quipements_impo'), 'Consoview');
		}
		
		private function getProductFeatureResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				listeFeature 	= re.result[0] as ArrayCollection;
				listePhotos 	= re.result[1] as ArrayCollection;
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_DETAILS_EQ));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}
		
		private function fournirSimInSpareResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				listeSimSpare 	= re.result as ArrayCollection;
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_SIM_SPARE));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}
		
		private function fournirTerminauxSimCardEngagementsFromStockResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				simcard = EquipementsElements.formatSim(re.result.SIMCARD[0] as Object) ;	
				
				if(re.result.TERMINAUX.length > 0)
				{
					var arrayResult	:ArrayCollection = re.result.TERMINAUX as ArrayCollection;
					var len			:int = arrayResult.length;
					
					for(var i:int = 0;i < len;i++)
					{
						var equiElet:EquipementsElements = new EquipementsElements();
						equiElet.fillEquipFromStock(arrayResult[i]);
						TERMINAUX.addItem(equiElet);
					}
					
					if(TERMINAUX.length == 1)
						(TERMINAUX[0] as EquipementsElements).SELECTED = true;
				}
				
				if(re.result.ISNEWENGAG > 0)
					ENGAGEMENTS = attributEngagementNew(re.result.ENGAGEMENTS[0]);
				else
					ENGAGEMENTS = attributEngagementRenouvellement(re.result.ENGAGEMENTS[0]);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.TERMSIMCARDENGAG));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}	

		private function fournirTerminauxSimCardEngagementsResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				simcard = EquipementsElements.formatSim(re.result.SIMCARD[0] as Object) ;	

				if(re.result.TERMINAUX.length > 0)
				{
					var arrayResult	:ArrayCollection = re.result.TERMINAUX as ArrayCollection;
					var len			:int = arrayResult.length;
					
					for(var i:int = 0;i < len;i++)
					{
						var equiElet:EquipementsElements = new EquipementsElements();
						equiElet.fill(arrayResult[i]);
						TERMINAUX.addItem(equiElet);
					}
					
					if(TERMINAUX.length == 1)
						(TERMINAUX[0] as EquipementsElements).SELECTED = true;
				}
				
				if(re.result.ISNEWENGAG > 0)
					ENGAGEMENTS = attributEngagementNew(re.result.ENGAGEMENTS[0]);
				else
					ENGAGEMENTS = attributEngagementRenouvellement(re.result.ENGAGEMENTS[0]);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.TERMSIMCARDENGAG));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es_'), 'Consoview');
		}		
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE
		//--------------------------------------------------------------------------------------------//
	    
	    private function attributEngagementNew(object:Object):ArrayCollection
	    {
	    	var obj:Engagement;
	    	var arrayC:ArrayCollection = new ArrayCollection();
	    	
	    	if(object.FPC_UNIQUE == null)
	    	{
		    	if(object.ENGAG_12 == 1)
		    	{
		    		obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '12_mois');
		    		obj.VALUE 		= "12";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "EDM";//Engagement Douze Mois
		    		arrayC.addItem(obj);
		    	}
		    	
		    	if(object.ENGAG_24 == 1)
		    	{
					obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '24_mois');
		    		obj.VALUE 		= "24";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "EVQM";
		    		arrayC.addItem(obj);
		    	}
		    	
		    	if(object.ENGAG_36 == 1)
		    	{
					obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '36_mois');
		    		obj.VALUE 		= "36";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "ETSM";
		    		arrayC.addItem(obj);
		    	}
		    	
		    	if(object.ENGAG_48 == 1)
		    	{
					obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '48_mois');
		    		obj.VALUE 		= "48";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "EQHM";
		    		arrayC.addItem(obj);
		    	}
	    	}
	    	else
	    	{
				obj 			= new Engagement();
	    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', 'Date_de_fin_de_p_riode_contractuelle_le') + DateFunction.formatDateAsString(object.FPC_UNIQUE);
	    		obj.VALUE 		= "24";
				obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
	    		obj.CODE  		= "EVQMFCP"
	    		obj.FPC  		= Formator.formatReverseDate(object.FPC_UNIQUE);
	    		arrayC.addItem(obj);
	    	}
	    	
	    	SessionUserObject.singletonSession.ELIGIBILITE = object.DUREE_ELIGIBILITE;
	    	
	    	return arrayC;
	    }
	    
	    private function attributEngagementRenouvellement(object:Object):ArrayCollection
	    {
	    	var obj:Engagement;
	    	var arrayC:ArrayCollection = new ArrayCollection();
	    	
	    	if(object.FPC_UNIQUE == null)
	    	{
		    	if(object.ENGAG_12 == 1)
		    	{
		    		obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '12_mois__renouvellement_');
		    		obj.VALUE 		= "12";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "EDMAR";//Engagement Douze Mois Avec Renouvellement
		    		arrayC.addItem(obj);
		    	}
		    	
		    	if(object.ENGAG_24 == 1)
		    	{
		    		obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '24_mois__renouvellement_');
		    		obj.VALUE 		= "24";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "EVQMAR";
		    		arrayC.addItem(obj);
		    	}
		    	
		    	if(object.ENGAG_36 == 1)
		    	{
		    		obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '36_mois__renouvellement_');
		    		obj.VALUE 		= "36";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "ETSMAR";
		    		arrayC.addItem(obj);
		    	}
		    	
		    	if(object.ENGAG_48 == 1)
		    	{
		    		obj 			= new Engagement();
		    		obj.DUREE 		= ResourceManager.getInstance().getString('M16', '48_mois__renouvellement_');
		    		obj.VALUE 		= "48";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
		    		obj.CODE 		= "EQHMAR";
		    		arrayC.addItem(obj);
		    	}
	    	}
	    	else
	    	{
	    		obj 			= new Engagement();
				obj.DUREE 		= ResourceManager.getInstance().getString('M16', 'Date_de_fin_de_p_riode_contractuelle_le') + DateFunction.formatDateAsString(object.FPC_UNIQUE);
	    		obj.VALUE 		= "24";
				obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
	    		obj.CODE  		= "EVQMFCP"
	    		obj.FPC   		= Formator.formatReverseDate(object.FPC_UNIQUE);
	    		arrayC.addItem(obj);
	    	}
	    	
	    	SessionUserObject.singletonSession.ELIGIBILITE = object.DUREE_ELIGIBILITE;
	    	
	    	return arrayC;
	    }
	    
		private function attributEngagementClient(object:Object):ArrayCollection
		{
			var obj:Engagement;
			var arrayC:ArrayCollection = new ArrayCollection();
			
			if(object.FPC_UNIQUE == null)
			{
				if(object.ENGAG_12 == 1)
				{
					obj 			= new Engagement();
					obj.DUREE 		= "12" + ResourceManager.getInstance().getString('M16', 'mois_acquisition_') + " / 12 "+ ResourceManager.getInstance().getString('M16', 'mois_renouvellement_');
					obj.VALUE 		= "12";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
					obj.CODE 		= "EDM";//Engagement Douze Mois
					arrayC.addItem(obj);
				}
				
				if(object.ENGAG_24 == 1)
				{
					obj 			= new Engagement();
					obj.DUREE 		= "24 " + ResourceManager.getInstance().getString('M16', 'mois_acquisition_') + " / 24 "+ ResourceManager.getInstance().getString('M16', 'mois_renouvellement_');
					obj.VALUE 		= "24";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
					obj.CODE 		= "EVQM";
					arrayC.addItem(obj);
				}
				
				if(object.ENGAG_36 == 1)
				{
					obj 			= new Engagement();
					obj.DUREE 		= "36 " + ResourceManager.getInstance().getString('M16', 'mois_acquisition_') + " / 36 "+ ResourceManager.getInstance().getString('M16', 'mois_renouvellement_');
					obj.VALUE 		= "36";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
					obj.CODE 		= "ETSM";
					arrayC.addItem(obj);
				}
				
				if(object.ENGAG_48 == 1)
				{
					obj 			= new Engagement();
					obj.DUREE 		= "48 " + ResourceManager.getInstance().getString('M16', 'mois_acquisition_') + " / 48 "+ ResourceManager.getInstance().getString('M16', 'mois_renouvellement_');
					obj.VALUE 		= "48";
					obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
					obj.CODE 		= "EQHM";
					arrayC.addItem(obj);
				}
			}
			else
			{
				obj 			= new Engagement();
				obj.DUREE 		= "FPC - " + ResourceManager.getInstance().getString('M16', '24_mois')+" - " + Formator.formatDate(object.FPC_UNIQUE);
				obj.VALUE 		= "24";
				obj.ELEGIBILITE	= object.DUREE_ELIGIBILITE;
				obj.CODE  		= "EVQMFCP"
				obj.FPC  		= Formator.formatReverseDate(object.FPC_UNIQUE);
				arrayC.addItem(obj);
			}
						
			return arrayC;
		}
		
	    private function searchError(value:String):String
	    {
	    	var newListe	:String = "";
	    	var splited		:Array = value.split(",");
	    	var cptr		:int = 0;

	    	for(var i:int = 0; i < splited.length;i++)
	    	{
	    		if(splited[i] != "NaN" && splited[i] != null && splited[i] != "null" && splited[i] != "")
	    		{
	    			newListe = newListe + "," + splited[i];
	    			cptr++;
	    			
	    			if(cptr == 1)
	    				newListe.replace(",", "");
	    		}
	    	}
	    	
	    	if(newListe.length > 0)
	    		newListe = newListe.slice(1);
	    	
	    	return newListe;
	    }

	}
}