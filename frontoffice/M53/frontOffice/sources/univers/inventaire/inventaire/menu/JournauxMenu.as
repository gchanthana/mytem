package univers.inventaire.inventaire.menu
{
	import flash.events.Event;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.controls.Menu;
	import univers.inventaire.inventaire.journaux.AfficherJournalEvent;
	import flash.events.MouseEvent;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	
	import composants.util.SearchTree;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.cible.InvSearchTree;
	import mx.controls.Alert;
	import univers.inventaire.inventaire.journaux.JournalFactory;
	import composants.access.perimetre.tree.SearchPerimetreWindow;
	import composants.access.perimetre.tree.PerimetreTreeWindow;
	
	/**
	 * Classe Onglet Journaux
	 * 
	 * */
	public class JournauxMenu extends JournauxMenuIHM
	{
		
		//Référence vres le menu du PopUpMenuButton 'Format' 
		private var myMenu:Menu;
		
		//Constante definissant le format CSV
		private const CSV : String = "CSV";

		//Constante definissant le format PDF		
		private const PDF : String = "PDF";;

		//Constante definissant le format XLS			
		private const XLS : String = "XLS";
		
		//format choisi dans le menu d'export
		private var choix : String;
		
		//Journal à exporter
		private var journal : String;
		
		//Libelle du journal à exporter
		private var libelleJournal : String;
		
		
		
		/**
		 * Constructeur 
		 * */
		public function JournauxMenu()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		
		//Initialisation de l'IHM 
		//Affectation des ecouteurs d'évènements
		private function initIHM(event:Event):void {
			initMenu();
			myGrid.addEventListener(Event.CHANGE,afficherJournal);
			btExport.addEventListener(MouseEvent.CLICK,exporter);			 
			//initTree();
			
		}
		
		//Formate l'arbre
		private function initTree():void{
			//tree
			/* treeOrga.percentWidth=100;
			treeOrga.percentHeight=100;
			SearchPerimetreWindow(treeOrga.searchTree).resultControl.setStyle("horizontalGap",0);
			SearchPerimetreWindow(treeOrga.searchTree).btnBack.width = 51;
			SearchPerimetreWindow(treeOrga.searchTree).searchInput.width = 75;
			PerimetreTreeWindow(treeOrga.perimetreTree).searchInput.width = 75;		
			SearchPerimetreWindow(treeOrga.searchTree).searchInput.styleName = "TextInputInventaire";
			PerimetreTreeWindow(treeOrga.perimetreTree).searchInput.styleName = "TextInputInventaire";
			SearchPerimetreWindow(treeOrga.searchTree).btnBack.styleName = "MainButton";
			SearchPerimetreWindow(treeOrga.searchTree).searchTree.setStyle("borderStyle","solid");
			PerimetreTreeWindow(treeOrga.perimetreTree).perimetreTree.setStyle("borderStyle","solid");
			treeOrga.addEventListener(InvSearchTree.NODE_CHANGED,afficherRapport);	
			
			SearchPerimetreWindow(treeOrga.searchTree).lblRecherche.visible = false;
			SearchPerimetreWindow(treeOrga.searchTree).lblRecherche.width = 0;	
			SearchPerimetreWindow(treeOrga.searchTree).bxMain.setStyle("paddingRight",0);
			SearchPerimetreWindow(treeOrga.searchTree).bxMain.setStyle("paddingLeft",0); */
		}

        // Initialise le menu control PopUpMenuButton 'Format' 
        private function initMenu():void {
            myMenu = new Menu();
            myMenu.styleName="MainMenu";
            var dp:Object = [{label: "Export XLS",value : "XLS"},{label: "Export CSV",value : "CSV"}, 
            					{label: "Export PDF",value : "PDF"}];     
               
            myMenu.dataProvider = dp;
            myMenu.selectedIndex = 0;   
                
            myMenu.addEventListener("itemClick", itemClickHandler);
            btExport.popUp = myMenu;
            btExport.label = myMenu.dataProvider[myMenu.selectedIndex].label;
            choix =  myMenu.dataProvider[myMenu.selectedIndex].value;
        }

        //Handler du click sur le menu du PopUpMenuButton 'Format'
        private function itemClickHandler(event:MenuEvent):void {
            var label:String = event.item.label;        
            choix = event.item.value;                                    
            btExport.label = label;                 
            btExport.close();                       
            myMenu.selectedIndex = event.index;    
        }
        
        //Affiche le journal selectionné
        private function afficherJournal( ev : Event):void{
        	if (myGrid.selectedIndex != -1){
        		try{
	        		lblError.text = "";
	        		var evtObj : AfficherJournalEvent = new AfficherJournalEvent();
	        		evtObj.typeJournal = String(myGrid.selectedItem.type);
	        		journal = String(myGrid.selectedItem.type);
	        		libelleJournal = String(myGrid.selectedItem.rapport);
	        		dispatchEvent(evtObj);
	        		txtDescription.text = String(myGrid.selectedItem.rapport);
	        	}catch(er : Error){
	        		trace(er.message,er.getStackTrace());
	        	}	
        	}        	
        }
        
        //Export ----------------------------------------------------------------------------------
        
        //Handler du clique sur le bouton exporter
        private function exporter(me : MouseEvent):void{
        	if (myGrid.selectedIndex == -1/*  && treeOrga.nodeInfo == null */){
        		lblError.text = "Sélectionnez un journal";
        	}else{
        		this["exporter"+choix]();
        	}        	       
        }
        
        //Exporte le journal affiché au format CSV
        private function exporterCSV():void{        
        	displayExport(CSV);
        }
       
        //Exporte le journal affiché au format PDF 
        private function exporterPDF():void{        
        	displayExport(PDF);
        }
		
		 //Exporte le journal affiché au format XLS        
        private function exporterXLS():void{
        	displayExport(XLS);
        }
        
         //Exporte le journal affiché suivant le format passé en paramètre        
         //param in format le format sous lequel on souhaite exporter
        private function displayExport(format : String):void {
			 
				var url:String = cv.NonSecureUrlBackoffice+"/fr/consotel/consoview/cfm/cycledevie/journaux/Display_Journal.cfm";
	            var variables:URLVariables = new URLVariables();
	            variables.FORMAT = format;	  
	            variables.JOURNAL =  journal; 
				variables.JOURNAL_LIBELLE = libelleJournal;	 
				variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;           
	            var request:URLRequest = new URLRequest(url);
	            request.data = variables;
	            request.method=URLRequestMethod.POST;
	            navigateToURL(request,"_blank");
        } 			    
        
        //Fin Export ------------------------------------------------------------------------------
        
        
        //Affiche le journal selectionné        
        private function afficherRapport( ev : Event):void{
        
        	/* if (treeOrga.nodeInfo != null && treeOrga.nodeInfo.hasOwnProperty("TYPE_LOGIQUE")){
				if (String(treeOrga.nodeInfo.TYPE_LOGIQUE) == "ANA"){						
					SearchPerimetreWindow(treeOrga.searchTree).searchTree.selectedIndex = 0;
					PerimetreTreeWindow(treeOrga.perimetreTree).perimetreTree.selectedIndex = 0;
					treeOrga.nodeInfo = null;
					lblErrorRapp.text = "Noeud non sélectionnable";				
					ev.preventDefault();
				}else{						 
					lblErrorRapp.text = "";
					afficherJournalDuNoeud();
				}
			}else{
				SearchPerimetreWindow(treeOrga.searchTree).searchTree.selectedIndex = 0;
				PerimetreTreeWindow(treeOrga.perimetreTree).perimetreTree.selectedIndex = 0;
				treeOrga.nodeInfo = null;
				lblErrorRapp.text = "Noeud non sélectionnable";				
				ev.preventDefault();
			} */		
        }
        
        //Affiche la synthese pour le noeud selectionné
        private function afficherJournalDuNoeud():void{
/*         	lblErrorRapp.text = "";
    		var evtObj : AfficherJournalEvent = new AfficherJournalEvent();
    		
    		var nodeInfo : Object = null;
    		nodeInfo = treeOrga.nodeInfo;


    		trace("Journal historique ----------------\nNodeId : " + nodeInfo.NID 
    		+ "\nLibelle : "+ nodeInfo.LBL 
    		+ "\nType logique : " + nodeInfo.TYPE_LOGIQUE
    		+ "\nType perimetre : " + nodeInfo.TYPE_PERIMETRE
    		+ "\n------------------------------------");
    		
    		evtObj.typeJournal = JournalFactory.HISTO_INV_NOEUD;
    		evtObj.nodeInfos = nodeInfo;
    		journal = String(JournalFactory.HISTO_INV_NOEUD);
    		libelleJournal = String("Historique du noeud " + nodeInfo.LBL);
    		dispatchEvent(evtObj);
    		txtDescription.text = String(libelleJournal); */ 
        }
        
		/**
		*  Fait les actrions nécessaire au changement de périmetre
		* */
		public function onPerimetreChange():void{
		
			//dispatchEvent(new Event("PerimetreChange"));
			//synthese.removeChild(treeOrga);
			//treeOrga = null;
			//treeOrga = new InvSearchTree();			
			//synthese.addChildAt(treeOrga,0);
			//callLater(initTree);
			
		}
	}
}