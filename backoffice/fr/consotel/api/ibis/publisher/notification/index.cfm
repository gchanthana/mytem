<!---
Auteur : Cédric 
Page de traitement des notification provenant de BIP pour la destination NOTIFICATION : /fr/consotel/api/ibis/publisher/notification/index.cfm
A chaque notification HTTP provenant de BIP correspond (Stateless) :
- Une requete HTTP
- Une instance de l'implémentation IReportingService (NotificationService)
- Une récupération des credentials utilisés par l'implémentation IReportingService (ReportingService)
--->
<!--- Traitement de la notification provenant de BIP --->
<cftry>
	<cflog type="information" file="REPORTING" text=">> BIP NOTIFICATION PAGE : #CGI.SCRIPT_NAME# [#CGI.REMOTE_HOST#]">
	<!--- Service de Reporting --->
	<cfset service=createObject("component","fr.consotel.api.ibis.publisher.reporting.impl.NotificationService").getService("","")>
	<!--- Traitement de la notification provenant de BIP --->
	<cfset service.handleNotification({})>
	<cfcatch type="any">
		<cflog file="NOTIFICATION" type="error" text="#CFCATCH.message#">
		
		<cfsavecontent variable="body">
			<cfoutput>
				BackOffice : #CGI.SERVER_NAME#<br>
				Remote Host : #CGI.REMOTE_HOST#<br><br>
				La page de traitement des notifications BIP : #CGI.SCRIPT_NAME# a rencontré l'exception suivante :<br>
				- Code d'erreur : #CFCATCH.ERRORCODE#<br>
				- Message : #CFCATCH.MESSAGE#<br>
				- Detail : #CFCATCH.DETAIL#<br><br>
			</cfoutput>
			<cfdump var="#CFCATCH.TAGCONTEXT#" label="Contexte d'exécution">
		</cfsavecontent>
		
		<cftry>
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(13)>
			<cfset mLog.setIDMNT_ID(28)>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		
		</cftry>
		
		
		<!---
		<cfmail from="error@consotel.fr" to="monitoring@saaswedo.com" subject="Page de notification BIP [#CGI.SERVER_NAME#] : #CFCATCH.message#" type="html">
			<cfoutput>
				BackOffice : #CGI.SERVER_NAME#<br>
				Remote Host : #CGI.REMOTE_HOST#<br><br>
				La page de traitement des notifications BIP : #CGI.SCRIPT_NAME# a rencontré l'exception suivante :<br>
				- Code d'erreur : #CFCATCH.ERRORCODE#<br>
				- Message : #CFCATCH.MESSAGE#<br>
				- Detail : #CFCATCH.DETAIL#<br><br>
			</cfoutput>
			<cfdump var="#CFCATCH.TAGCONTEXT#" label="Contexte d'exécution">
		</cfmail>
		--->
	</cfcatch>
</cftry>