﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">

	<cffunction name="executeAct" access="public" returntype="struct" output="false">
		<cfargument name="data" type="Struct" required="true">
		<cftry>
			<cfset idRacine 	= SESSION.USER.APP_LOGINID>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m28_client.list_racine">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_app_loginid" 	value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VarCHAR" 	variable="p_code_lang" 		value="#getCodeLangue()#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VarCHAR" 	variable="p_text" 			value="#data.p_text#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VarCHAR" 	variable="p_search_column" 	value="#data.p_search_column#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VarCHAR" 	variable="p_order_column" 	value="#data.p_order_column#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idx_debut" 		value="#data.p_idx_debut#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_nb" 			value="#data.p_nb#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL034", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL034", 0, err)/>
			</cfif>
			
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL034", 0, ThrowCfCatchError("AL034",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>

	