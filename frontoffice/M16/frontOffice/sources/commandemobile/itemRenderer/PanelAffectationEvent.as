package commandemobile.itemRenderer
{
	import gestioncommande.entity.Action;
	
	import flash.events.Event;

	public class PanelAffectationEvent extends Event
	{
		public static const DISPLAY_PANEL:String = "displayPanel";
		public static const VALID_PANEL:String = "validPanel";
		public static const CANCEL_PANEL:String = "cancelPanel";
		private var _codeItem:String;
		private var _action:Action;
				
		
		public function PanelAffectationEvent(type:String, bubbles:Boolean = false, code:String = "", action:Action = null) {
			super(type, bubbles);
			this._codeItem = code;
			this._action = action;
		}
		
		public function get codeItem():String {
			return _codeItem;
		}
		
		public function get actionItem():Action {
			return _action;
		}
		
 		override public function clone():Event {
			return new PanelAffectationEvent(type, bubbles, codeItem,actionItem);
		} 
	
	}
}