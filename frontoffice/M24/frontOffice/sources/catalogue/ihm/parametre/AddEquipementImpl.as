package catalogue.ihm.parametre
{
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.strategie.ICatalogue;
	import catalogue.strategie.strategieconcrete.ConstructeurCS;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;


	[Bindable]
	public class AddEquipementImpl extends TitleWindow
	{
		
		private var _catalogueCS:ICatalogue;
		private var _equipement:EquipementCatalogueVO = new EquipementCatalogueVO();
		private var boolContinueAdd:Boolean = false;
		private var _popUpFilter:PopupFilterIHM = new PopupFilterIHM();
		
		//Public UI componant
		public var comboType : ComboBox;
		
		public var comboConstructeur : ComboBox;
		public var inputLib:TextInput;
		public var inputRef:TextInput;
		
		
		public function AddEquipementImpl()
		{
				
		}
		public function initData():void
		{
			catalogueCS = new ConstructeurCS();
			
			if(equipement.IDEQUIPEMENT>0)
			{
				initMode_update();
			}
			else
			{
				initMode_create();
				
			}
			
			catalogueCS.rechercheTypeEquipement();
			catalogueCS.rechercheContructeur();
		}
		
		private function initMode_update():void
		{
			registerClassAlias("catalogue.vo.EquipementCatalogueVO",EquipementCatalogueVO);  
			equipement= EquipementCatalogueVO(ObjectUtil.copy(equipement))  // delete les ref sur le grid
			
			
			catalogueCS.addEventListener(CatalogueEvent.LISTE_TYPE_EQUIPEMENT_COMPLETE,listeTypeComplete);
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,listeConstructeurComplete);
			
		}
		
		private function listeTypeComplete(evt : CatalogueEvent):void
		{
			utilFunctionInitCombo(comboType,'ID_TYPE',equipement.IDTYPE);
		}
		private function listeConstructeurComplete(evt : CatalogueEvent):void
		{
			utilFunctionInitCombo(comboConstructeur,'ID_CONSTRUCTEUR',equipement.IDCONSTRUCTEUR);
		}
		private function utilFunctionInitCombo(combo: ComboBox, labelFieldOfID : String, searchValue : Number):void
		{
			for(var i:int=0;i<(combo.dataProvider as ArrayCollection).length;i++)
			{
				if((combo.dataProvider as ArrayCollection).getItemAt(i)[labelFieldOfID]==searchValue)
				{
					combo.selectedIndex = i;
					break;
				}
			}
		}
		private function initMode_create():void
		{
						
		}
		private function initAttribut():void
		{
			
		}
		public function fermer(event : Event=null):void
		{
			PopUpManager.removePopUp(this);
		}
		public function valider():void
		{
			
		}
		public function combo_revendeur_commande_handler(evt : Event):void
		{
			
		}
		public function btValiderContinuerHandler():void
		{
			boolContinueAdd = true;
			
			validerPop();
			
		}
		public function btValiderFermerHandler():void
		{
			boolContinueAdd = false;
			validerPop();
		}
		private function validerPop():void
		{
			
			if(equipement.IDEQUIPEMENT>0)
			{
				//update
				catalogueCS.addEventListener(CatalogueEvent.UPDATE_EQUIPEMENT_COMPLETE,update_equipement_complete_handler);
				catalogueCS.updateEquipement(equipement);
			}
			else
			{
				//add
				catalogueCS.addEventListener(CatalogueEvent.ADD_EQUIPEMENT_COMPLETE,add_equipement_complete_handler);
				catalogueCS.addEquipement(equipement);
			}
		}
		private function update_equipement_complete_handler(evt : CatalogueEvent):void
		{
			
			if(evt.objectReturn> 0)
			{
			
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Equipement_modifi__'));
				dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			}
						
			if(!boolContinueAdd)
			{
				fermer();
			}
		}
		private function add_equipement_complete_handler(evt : CatalogueEvent):void
		{
			//dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			if(evt.objectReturn.IDEQUIP == -10)
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Le_mod_le_existe_d_ja_dans_le_catalogue'));
			}
			else
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Equipement_ajout___R_f_rence_consotel___')+evt.objectReturn.REF);
					
				if(boolContinueAdd)
				{
					equipement = new EquipementCatalogueVO;
					comboConstructeur.selectedIndex = -1 ;
					comboType.selectedIndex = -1;
					inputLib.errorString = '';
					inputRef.errorString = '';
				}
				else
				{
					
					fermer();
				}
				dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			}
		}

		public function set catalogueCS(value:ICatalogue):void
		{
			_catalogueCS = value;
		}

		public function get catalogueCS():ICatalogue
		{
			return _catalogueCS;
		}

		public function set equipement(value:EquipementCatalogueVO):void
		{
			_equipement = value;
		}

		public function get equipement():EquipementCatalogueVO
		{
			return _equipement;
		}
		public function comboTypeHandler():void
		{
			if(comboType.selectedItem)
			{
				equipement.IDTYPE = (comboType.selectedItem as TypeEquipementVO).ID_TYPE
			}
		}
		
		public function comboConstructeurHandler():void
		{
			if(comboConstructeur.selectedItem)
			{
				equipement.IDCONSTRUCTEUR = (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;
				resetSelection((comboConstructeur.dataProvider as ArrayCollection).source);
				comboConstructeur.selectedItem.SELECTED = true;

			}
		}
		
		public function btImgClickHandler():void
		{
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.LIBELLE_ITEM = 'LIBELLE_CONSTRUCTEUR';
			_popUpFilter.ID_ITEM = 'ID_CONSTRUCTEUR';
			_popUpFilter.listeFilter =  ObjectUtil.copy(catalogueCS.listeConstructeur) as ArrayCollection;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_constructeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', _popUpFilterValidHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		private function _popUpFilterValidHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', _popUpFilterValidHandler);
			resetSelection((comboConstructeur.dataProvider as ArrayCollection).source);
			comboConstructeur.selectedIndex = _popUpFilter.getItemCurrentIndex();
			comboConstructeur.selectedItem.SELECTED = true;
			equipement.IDCONSTRUCTEUR = (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;
		}
		
		private function resetSelection(tab:Array):void
		{
			if(tab)
			{
				var len:Number =  tab.length;
				for(var index:Number = 0; index > len; index++)
				{
					tab[index].SELECTED = false;
				}
			}
		}
		
	}
}