<!--- =========================================================================
Name: TableauBord
Original Author: 
$History: $
$NoKeywords$
========================================================================== --->
<cfcomponent displayname="TableauBord"  hint="This class is configured with a ConcreteStrategy object, maintains a reference to a Strategy object and may define an interface that lets Strategy access its data.">
	<cfset dataset="">
	<cfset strategy="">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" returntype="void" access="public" displayname="" hint="Initialize the TableauBord object">
		<cfargument name="perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
			
		<cfset objStrategy=createObject("component","#perimetre##ModeCalcul#Strategy")>
		<cfset Variables.Strategy=objStrategy>
	</cffunction>

<!--- =========================================================================
METHODS
========================================================================== --->

	<cffunction name="getDatasetRecordCount" returntype="numeric" access="public">
		<cfreturn dataset.recordcount>
	</cffunction>

	<cffunction name="queryData" access="public" returntype="query" output="false" displayname="" hint="Va chercher les données d'abonnement dans la base." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />
		<cfset dataset=Strategy.getData(arguments.ID,arguments.DateDebut,arguments.DateFin,tb)>
		
		<cfreturn dataset>
	</cffunction>
	
	<cffunction name="getRepartOperateur" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetRepartOperateur" dbtype="query">
			select type_theme, nom, idtype_theme,
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final
			from dataset
			where montant_final <> 0
			group by type_theme, nom, idtype_theme
		</cfquery>
		<cfreturn qgetRepartOperateur>
	</cffunction>
	
	<cffunction name="getRepartDuree" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetRepartDuree" dbtype="query">
			select nom,
		       	sum(duree_appel) AS duree_appel
			from dataset
			where montant_final <> 0
			group by nom
		</cfquery>
		<cfreturn qgetRepartDuree>
	</cffunction>
	
	<cffunction name="getRepartOperateurBySegment" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfargument name="segment" type="string">
		<cfargument name="segmentid" type="Numeric">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetRepartOperateur" dbtype="query">
			select type_theme, nom, idtype_theme,
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final
			from dataset
			where montant_final <> 0 AND idsegment_theme=#segmentid#
			group by type_theme, nom, idtype_theme
		</cfquery>
		<cfreturn qgetRepartOperateur>
	</cffunction>
	
	<cffunction name="getRepartDureeBySegment" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfargument name="segment" type="string">
		<cfargument name="segmentid" type="Numeric">
		
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetRepartDuree" dbtype="query">
			select nom,
		       	sum(duree_appel) AS duree_appel
			from dataset
			where montant_final <> 0 AND idsegment_theme=#segmentid#
			group by nom
		</cfquery>
		<cfreturn qgetRepartDuree>
	</cffunction>
	
	<cffunction name="getEvolution" returntype="struct">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfargument name="tb" required="false" type="string" default="complet" displayname="tb" />
		<cfdump var='#Strategy#'>
		<cfreturn Strategy.getEvolution(arguments.ID,arguments.DateDebut,arguments.DateFin,tb)>
	</cffunction>
	
	<cffunction name="getSurthemeAbos" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qGetSurtheme" dbtype="query">
			select segment_theme, sur_theme as theme_libelle, type_theme, 0 as idtheme_produit,idsur_theme, 
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final,
	    	   	sum(nombre_appel) AS nombre_appel,
	    	   	sum(qte) AS qte
			from dataset
			where idtype_theme=1
			group by segment_theme, sur_theme, type_theme, idsur_theme
			order by ordre_affichage
		</cfquery>
		<cfreturn qGetSurtheme>
	</cffunction>
	
	<cffunction name="getSurthemeConsos" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qGetSurtheme" dbtype="query">
			select segment_theme, sur_theme as theme_libelle, type_theme, 0 as idtheme_produit,idsur_theme, 
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final,
	    	   	sum(nombre_appel) AS nombre_appel,
	    	   	sum(qte) AS qte
			from dataset
			where idtype_theme=2
			group by segment_theme, sur_theme, type_theme, idsur_theme
			order by ordre_affichage
		</cfquery>
		<cfreturn qGetSurtheme>
	</cffunction>
	
	<cffunction name="setStrategy" returntype="void" access="public" displayname="" hint="Change le type de requete pour les abos du tableau de bord">
		<cfargument name="perimetre" type="string" required="true">
		<cfargument name="ModeCalcul" type="string" required="true">
		<cfset objStrategy=createObject("component","#perimetre##ModeCalcul#Strategy")>
		<cfset Variables.Strategy=objStrategy>
	</cffunction>
	
	<cffunction name="getLibelleAbo" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn Strategy.getLibelleAbo()>
	</cffunction>
	
	<cffunction name="getLibelleConso" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn Strategy.getLibelleConso()>
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false" hint="Raméne le périmétre de la requéte et le mode de calcul (Usage/facturation)">
		<cfreturn Strategy.getStrategy()>
	</cffunction>
	
	<cffunction name="getAbo" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetAbo" dbtype="query">
			select type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,
				sum(qte) AS qte,
		       	sum(nombre_appel) AS nombre_appel, 
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final, ordre_affichage
			from dataset
			where idtype_theme=1
			group by type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,ordre_affichage
			order by ordre_affichage
		</cfquery>
		<cfreturn qgetAbo>
	</cffunction>
	
	<cffunction name="getAboBySegment" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfargument name="segment" 		type="string">
		<cfargument name="segmentid" 	type="Numeric">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetAbo" dbtype="query">
			select type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,
				sum(qte) AS qte,
		       	sum(nombre_appel) AS nombre_appel, 
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final, ordre_affichage
			from dataset
			where idtype_theme=1 AND idsegment_theme=#segmentid#
			group by type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,ordre_affichage
			order by ordre_affichage
		</cfquery>
		<cfreturn qgetAbo>
	</cffunction>
	
	<cffunction name="getTotalAboBySegment" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfargument name="segment" type="string">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetAbo" dbtype="query">
			select segment_theme as SEGMENT_THEME,'Segment ' +  segment_theme as LIBELLE,sum(montant_final) AS MONTANT_TOTAL
			from dataset
			where idtype_theme = 1
			group by segment_theme			 
		</cfquery>
		<cfreturn qgetAbo>
	</cffunction>
	
	<cffunction name="getTotalConsosBySegment" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfargument name="segment" type="string">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetAbo" dbtype="query">
			select SEGMENT_THEME, 'Segment ' + segment_theme as LIBELLE,sum(montant_final) AS MONTANT_TOTAL
			from dataset
			where idtype_theme = 2
			group by segment_theme			 
		</cfquery>
		<cfreturn qgetAbo>
	</cffunction>
	
	<cffunction name="getConso" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetConso" dbtype="query">
			select type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,
				sum(qte) AS qte,
		       	sum(nombre_appel) AS nombre_appel, 
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final,ordre_affichage
			from dataset
			where idtype_theme = 2
			group by type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,ordre_affichage
			order by ordre_affichage
		</cfquery>
		<cfreturn qgetConso>
	</cffunction>
	
	<cffunction name="getConsoBySegment" returntype="query" access="public" output="false" hint="Lorsqu'il n'est pas nécessaire d'effectuer la requete, va chercher les données mais en mémoire">
		<cfargument name="segment" type="string">
		<cfargument name="segmentid" type="Numeric">
		<cfset dataset = session.RECORDSET> 
		<cfquery name="qgetConso" dbtype="query">
			select type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,
				sum(qte) AS qte,
		       	sum(nombre_appel) AS nombre_appel, 
		       	sum(duree_appel) AS duree_appel,
	    	   	sum(montant_final) AS montant_final,ordre_affichage
			from dataset
			where idtype_theme=2 AND idsegment_theme=#segmentid#
			group by type_theme, theme_libelle,idtheme_produit, sur_theme, segment_theme,ordre_affichage
			order by ordre_affichage
		</cfquery>
		<cfreturn qgetConso>
	</cffunction>
	
	<cffunction name="getCompte" returntype="string" access="public" output="false">
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfset compte=Strategy.getNumero(arguments.ID)>
		<cfreturn compte>
	</cffunction>
	
	<cffunction name="setSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction>
	
	<cffunction name="fournirListeTheme_SurTheme" access="public" returntype="Struct" output="false">

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m24.getListTheme">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_code_langue" 	value="#session.user.globalization#">
				<cfprocresult name="p_retour"  resultset="1">
				<cfprocresult name="p_retour2" resultset="2">
			</cfstoredproc>
  			
			<cfset retour 			= structNew()>
 			<cfset retour.SURTHEME 	= p_retour>
			<cfset retour.THEME 	= p_retour2>
			
		<cfreturn retour>
	</cffunction>
	
</cfcomponent>