package gestionparc.ihm.action.ligne.actionLigneRenouveler
{
	import gestionparc.ihm.action.ICommand;

	public class ActionLigneRenouvelerCommand implements ICommand
	{
		private var receiver:ActionLigneRenouvelerReceiver=null;

		public function ActionLigneRenouvelerCommand(rec:ActionLigneRenouvelerReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
