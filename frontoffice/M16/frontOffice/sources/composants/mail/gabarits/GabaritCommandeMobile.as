package composants.mail.gabarits
{
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.collections.XMLListCollection;
	import mx.resources.ResourceManager;
	
	import composants.util.DateFunction;
	
	import gestioncommande.entity.Commande;
	
	public class GabaritCommandeMobile extends AbstractGabarit
	{
		
		public static const TYPE_COMMANDE_FIXEDATE : String = 'CommandeFixeData';
		public static const TYPE_COMMANDE_MOBILE : String = 'CommandeMobile';
		public static const TYPE_RESILIATION_FIXEDATE : String = 'ResiliationFixeData';
		public static const TYPE_MVT_SOUSTETE : String = 'MouvementDeSousTete';
		public static const BLANK : String = 'Blank';
		
		public var _commande:Commande;
		private var _listeArticles:XML;
		
		public function GabaritCommandeMobile()
		{
			super();
		}
		
		override public function getGabarit():String
		{
			_commande = infos.commande;
			_listeArticles = infos.listeArticles;
			
			var listeLignes : String = '';
			if (_listeArticles != null)
			{
				var collection:XMLListCollection = new XMLListCollection(_listeArticles.children());
				listeLignes = printListeLignes(collection);
			}
			
			var cnom : String = (_commande.PATRONYME_CONTACT != null )? _commande.PATRONYME_CONTACT : '';
			var cemail: String = (_commande.EMAIL_CONTACT!=null)?_commande.EMAIL_CONTACT:'';
			var csociete : String = (_commande.LIBELLE_REVENDEUR !=null)?_commande.LIBELLE_REVENDEUR:'';
			var refclient : String = (_commande.REF_CLIENT1!=null)?_commande.REF_CLIENT1:'';
			var refclient2 : String = (_commande.REF_CLIENT2!=null)?_commande.REF_CLIENT2:'';
			var refOperateur : String = (_commande.REF_OPERATEUR!=null)?_commande.REF_OPERATEUR:'';
			var compte : String = (_commande.LIBELLE_COMPTE!=null)?_commande.LIBELLE_COMPTE:'';
			var sousCompte : String = (_commande.LIBELLE_SOUSCOMPTE!=null)?_commande.LIBELLE_SOUSCOMPTE:'';
			var corpMessage : String = (infos.corpMessage!=null)?infos.corpMessage:'';
			
			var _body:String = 
			 
					'<b>'+infos.PRENOM_EXPEDITEUR+' '+infos.NOM_EXPEDITEUR +'</b>'
					+'<br/>'
					+infos.MAIL_EXPEDITEUR
					+'<br/>'
					+ResourceManager.getInstance().getString('M16', '_br___u_Date__u____')+DateFunction.formatDateAsString(new Date())
					+'<br/>'
					+ResourceManager.getInstance().getString('M16', '_br__Soci_t_____b_')+'<b>'+infos.SOCIETE_EXPEDITEUR+'</b>'		
					+'<br/>'
					+'<br/>'
					+ResourceManager.getInstance().getString('M16', '_br__Libell__de_la_demande__b_') +'<b>'+ _commande.LIBELLE_COMMANDE+'</b>'
					+'<br/>'
					+ResourceManager.getInstance().getString('M16', '_br__Num_ro_de_la_demande__b_') +'<b>'+ _commande.NUMERO_COMMANDE+'</b>'
					+'<br/>'
					+ResourceManager.getInstance().getString('M16', '_br__R_f_rence_client____b_') +'<b>'+ refclient +'</b>'
					+'<br/>'
					+'<br/>'
					+'<br/>'
					+ corpMessage.replace('*numerocommande*',_commande.NUMERO_COMMANDE).replace('*listelignes*',listeLignes)
					+'<br/>'					
					+ResourceManager.getInstance().getString('M16', 'Cordialement_')
					+'<br/>'					
					+'<b>'+infos.PRENOM_EXPEDITEUR+' '+infos.NOM_EXPEDITEUR +'.</b>'
					
			return	_body
		}
		
		protected function printListeArticles(liste :ICollectionView):String
		{
			var output : String = '<br/>';
			/* 
			if (liste != null){
				var cursor:IViewCursor = liste.createCursor();
				//loop over articles
				while(!cursor.afterLast)
				{
					var article:XML = (cursor.current as XML);
					
					
					var ligne : Object = liste[i];
					var equipements : String = ligne.LIBELLE_PRODUIT;
					var ressources : String = ligne.LIBELLE_PRODUIT;
					var numligne : String = ligne.SOUS_TETE;		
				}	
				}
			} */
			return output;
		}
		
		protected function printListeLignes(liste:ICollectionView):String
		{
			var output : String = '<br/>';
			if (liste != null){
				var cursor:IViewCursor = liste.createCursor();
				//loop over articles
				while(!cursor.afterLast)
				{
					var article:XML = (cursor.current as XML);
					var numligne : String = article.soustete[0];
					output = output + numligne + '<br/>'
					cursor.moveNext();
				}
				output = output + '<br/>';
			}
			
			return output;		
		}
	
	}
}
