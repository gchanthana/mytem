package rapport.ihm
{
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import rapport.event.RapportEvent;
	import rapport.event.RapportIHMEvent;
	import rapport.ihm.infofacture.IntegrationDesFacturesIHM;
	import rapport.utils.ManagerPopUp;
	import rapport.vo.ApiReportingSingleton;
	import rapport.vo.Rapport;
	
	[Bindable]
	public class ModuleRapport extends ManagerPopUp
	{
		public var selectedRapport: Rapport;			
		public var rapportE1IHM :RapportEtapeUneIHM;
		public var rapportE2IHM :RapportEtapeDeuxIHM;
		public var myViewStack:ViewStack;
		public var paramRapportButton :Button;
		public var choixRapportButton :Button;
		private var managerPopUp:ManagerPopUp;
		private var integrationFacture:IntegrationDesFacturesIHM;
		
		public static var isClose:Boolean=false; // pour ne pas afficher le message (Il n\'y pas de rapport qui corresponds à vos critères ) 2 fois lors de la fermeture
		
		public function ModuleRapport()
		{		
			addEventListener(FlexEvent.ADD,addHandler); 	
		}				
		
		private function addHandler(evt:FlexEvent):void
		{
			if(this.rapportE2IHM != null)
			{
				this.rapportE2IHM.rapportInfo.initDataIntegrationFacture(); // actualiser les données des factures
			}
			if(isPopUp)
			{
				isClose=false;
				if(rapportE1IHM!=null && ApiReportingSingleton.nbInstanciation ==0 )
				{
					rapportE1IHM.initModuleRapport();// initialiser les services qui permettent d'afficher la liste des rapports lors que on appelle le module comme pop up plusieurs fois 
				}
				width=1000;
				height=700;
				styleName="popUpBox";
				ApiReportingSingleton.nbInstanciationPopUP+=1;
				showCloseButtonRapport(isPopUp);
			}
			else
			{
				width=1000;
				isClose=false;
				styleName="composant";
				ApiReportingSingleton.nbInstanciation+=1;
				hideCloseButtonRapport(false);
			}
		}
		
		public function NextClickEventHandler(event:RapportIHMEvent):void
		{		
			selectedRapport=rapportE1IHM.getRapportSelected();
			rapportE2IHM.initData(selectedRapport);
			myViewStack.selectedIndex=1;
			paramRapportButton.styleName="buttonRapportActif";
			choixRapportButton.styleName="butonRapportInactif";
			paramRapportButton.enabled=true;
		}
		public function module1_creationCompleteHandler(event:FlexEvent):void
		{
			paramRapportButton.styleName="buttonRapportDesactivate";
			paramRapportButton.enabled=false;
			rapportE1IHM.addEventListener(RapportIHMEvent.CLOSE_CLICK_EVENT,closePopUPRapport);
			rapportE2IHM.addEventListener(RapportIHMEvent.CLOSE_CLICK_EVENT,closePopUPRapport);
			rapportE2IHM.addEventListener(RapportIHMEvent.VALIDER_RETOUR_EVENT,retourEtapeUne);
			rapportE1IHM.moduleService.model.addEventListener(RapportEvent.USER_MODULE_UPDATED,getAllRapport);// lors que les modules sont prêts on appelle la fonction getAllRapport
		}
	
		private function getAllRapport(event:RapportEvent):void
		{
			rapportE1IHM.getListeRapport();
		}
		
		private function retourEtapeUne(event:RapportIHMEvent):void
		{
			myViewStack.selectedChild=rapportE1IHM;// passer l'etape 1 		
			paramRapportButton.styleName="buttonRapportDesactivate";
			choixRapportButton.styleName="buttonRapportActif";
			paramRapportButton.enabled=false;
		}
		
		public function closePopUPRapport(event:RapportIHMEvent):void
		{
			PopUpManager.removePopUp(this);	// fermer le popup	
			this.x=0;
			this.y=0;
			isClose=true;
			rapportE1IHM.resetDataIHMSingleton();// initialiser les datas sur les deux etapes 
			rapportE2IHM.resetDataIHMSingleton();	
			myViewStack.selectedChild=rapportE1IHM;// passer l'etape 1 		
			paramRapportButton.styleName="buttonRapportDesactivate";
			choixRapportButton.styleName="buttonRapportActif";
			paramRapportButton.enabled=false;
		}
		
		// permert d'afficher la croix ( pour fermer la fênetre )et le button annuler
		public function showCloseButtonRapport(value: Boolean):void
		{
			this.showCloseButton=value;// afficher pas le button
			
			if(rapportE1IHM != null)
			{
				rapportE1IHM.btnCancel.visible=value;		
			}
			if(rapportE2IHM != null)
			{
				rapportE2IHM.fermerModule.visible=value;
			}
			
			if(ApiReportingSingleton.nbInstanciation >=1 )
			{	
				rapportE1IHM.resetDataIHMSingleton();// initialiser les datas sur les deux etapes 
				rapportE2IHM.resetDataIHMSingleton();
				rapportE1IHM.rapportFiltre.comboBoxModule.selectedIndex=0;
				rapportE1IHM.rapportFiltre.defautSelectedIndex=0;
				myViewStack.selectedChild=rapportE1IHM;// passer l'etape 1 		
				paramRapportButton.styleName="buttonRapportDesactivate";
				choixRapportButton.styleName="buttonRapportActif";
				paramRapportButton.enabled=false;			
				rapportE1IHM.initModuleRapport(); // après l'initialisation des datas on appelle encore les services pour re-executer les requêtes
			}
		}
		
		// permert de masquer la croix qui ferme la fênetre & la button annuler
		
		public function hideCloseButtonRapport(value:Boolean):void
		{
			this.showCloseButton=value;
			
			if(rapportE1IHM != null)
			{
				rapportE1IHM.btnCancel.visible=value;
			}
			
			if(rapportE2IHM != null)
			{
				rapportE2IHM.fermerModule.visible=value;
			}
			/* pour initialiser et passer à l'etape 1  lors que on appelle le module rapport comme composants( on appelle  plusieur fois 
			avant d'appeler le module comme popup ou lors que on appelle le module rapport comme composant la première 
			fois après l'appelle du module comme popup plusieurs fois )*/
			
			if(ApiReportingSingleton.nbInstanciation >1 ||  ApiReportingSingleton.nbInstanciationPopUP >=1)
			{	
				isClose=true;
				rapportE1IHM.resetDataIHMSingleton();// initialiser les datas sur les deux etapes 
				rapportE2IHM.resetDataIHMSingleton();
				rapportE1IHM.rapportFiltre.comboBoxModule.selectedIndex=0;
				rapportE1IHM.rapportFiltre.defautSelectedIndex=0;
				myViewStack.selectedChild=rapportE1IHM;// passer l'etape 1 		
				paramRapportButton.styleName="buttonRapportDesactivate";
				choixRapportButton.styleName="buttonRapportActif";
				paramRapportButton.enabled=false;
				
				rapportE1IHM.initModuleRapport();
			}
		}
	}
}