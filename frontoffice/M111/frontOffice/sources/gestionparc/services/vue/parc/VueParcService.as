package gestionparc.services.vue.parc
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.services.servicelocator.IServiceView;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;

	public class VueParcService implements IServiceView 
	{
		[Bindable]
		public var model:VueParcData;
		public var myHandler:VueParcHandler;
		private var myData: ArrayCollection;

		public function VueParcService() 
		{
			model=new VueParcData();
			myHandler=new VueParcHandler(model);
			myData=new ArrayCollection();
		}

		public function getData(indexDebut:int, nbItem:int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.vue.VueParc", "getVueParc", myHandler.ViewResultHandler);
			
			RemoteObjectUtil.callService(op, 
				SpecificationVO.getInstance().idPool,
				SpecificationVO.getInstance().keyWord, 
				SpecificationVO.getInstance().idKeyWordScope,
				SpecificationVO.getInstance().keyWord2, 
				SpecificationVO.getInstance().idKeyWordScope2,
				SpecificationVO.getInstance().labelSort, 
				indexDebut, 
				nbItem,  
				SpecificationVO.getInstance().idRestriction,
				SpecificationVO.getInstance().orderByReq);
		}
		
		public function getModel():EventDispatcher
		{
			return model;
		}
		
		public function restoreData():ArrayCollection
		{
			myData=model.listeResultat;
			return myData;
		}
		
		public  function getAllnbRow(): Number
		{
			return model.NB_ROWS; 
		}
	}
}