package composants.tb.tableaux
{
	import composants.tb.tableaux.surtheme.SurThemeAbonnement;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;

	import event.LigneEvent;

	import flash.events.Event;

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import mx.charts.HitData;
	import mx.charts.events.ChartItemEvent;
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	import services.ligne.LigneService;

	public class TableauAbo extends TableauAbo_IHM implements ITableauModel
	{
		[Bindable]
		protected var _dataProviders:ArrayCollection;
		protected var _lastDataProviders:ArrayCollection;
		protected var opTitre:AbstractOperation;
		protected var op:AbstractOperation;
		protected var identifiant:String;
		protected const TYPE_SEGMENT:String='SEGMENT';
		protected const TYPE_SURTHEME:String='SURTHEME';
		
		[Bindable]	protected var _total:Number=0;

		private var perimetre:String;
		private var modeSelection:String;
		private var _moisDeb:String; //le debut de la periode 
		private var _moisFin:String; //la fin de la periode
		private var _segmentDataProvider:ArrayCollection;
		private var _nbrLigneData:String="-";
		private var _nbrLigneFixe:String="-";
		private var _nbrLigneMobile:String="-";

		public var ligneService:LigneService=new LigneService();

		/**
		* Constructeur
		**/
		public function TableauAbo()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau.
		**/
		public function set dataProviders(d:ArrayCollection):void
		{
			_dataProviders=_formatterDataProvider(d);
			_lastDataProviders=d;
			drawTabs();
		}

		/**
		*  Retourne le dataproviders
		**/
		[Bindable]
		public function get dataProviders():ArrayCollection
		{
			return _dataProviders;
		}


		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/
		public function set changeHandlerFunction(changeFunction:Function):void
		{
			//_changeHandlerFunction = changeFunction;
		}


		/**
		*  Retourne une reference vers la fonction qui traite le data provider
		**/
		public function get changeHandlerFunction():Function
		{
			return null; //_changeHandlerFunction;
		}

		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void
		{
			chargerDonnees();

		}

		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void
		{
			myGrid.dataProvider=null;
		}

		/**
		 * Met à jour la periode
		 * */
		public function updatePeriode(mois_Deb:String=null, mois_Fin:String=null):void
		{
			_moisDeb=mois_Deb;
			_moisFin=mois_Fin;
		}

		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...)
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre:String=null, modeSelection:String=null):void
		{
			this.perimetre=perimetre;
			this.modeSelection=modeSelection;
		}


		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight)
			if (myPieGraph && myPieGraph.myPie)
			{
				myPieGraph.dataProvider=segmentDataProvider;
				myPieGraph.field="MONTANT_TOTAL_AFFICHEE";
				myPieGraph.nameField="LIBELLE";
			}
		}

		//initialisation du composant
		protected function init(fe:FlexEvent):void
		{
			myPieGraph.myPie.addEventListener(ChartItemEvent.ITEM_CLICK, _myPieItemClickHandler);
			myPieGraph.labelFunction=pielabelFunction;
			myPieGraph.dataTipsFunction=pieDataTipFunction;
		}

		private function drawTabs():void
		{
			rep.dataProvider=dataProviders;


			if (rep.dataProvider != null)
				for (var i:int=0; i < rep.dataProvider.length; i++)
				{
					myGrid[i].addEventListener(Event.CHANGE, myGridChangeEventHandler);
				}
			myFooterGrid.columns[1].headerText=moduleTableauDeBordE0IHM.formatNumberCurrency(_total, 2);
			var myEv:Event=new Event("totalSet");
			dispatchEvent(myEv);
		}

		protected function myGridChangeEventHandler(ev:Event):void
		{
			var eventObj:TableauChangeEvent=new TableauChangeEvent("tableauChange");

			eventObj.LIBELLE=ev.currentTarget.selectedItem.LIBELLE;
			eventObj.MONTANT_TOTAL=ev.currentTarget.selectedItem.MONTANT_TOTAL;
			eventObj.ID_LIBELLE=ev.currentTarget.selectedItem.ID_LIBELLE;
			eventObj.IDSEGMENT_THEME=ev.currentTarget.selectedItem.IDSEGMENT_THEME;
			eventObj.ID=ev.currentTarget.selectedItem.ID;
			eventObj.QUANTITE=ev.currentTarget.selectedItem.QUANTITE;
			eventObj.TYPE_THEME=ev.currentTarget.selectedItem.TYPE_THEME;
			eventObj.SUR_THEME=ev.currentTarget.selectedItem.SUR_THEME;
			eventObj.IDSUR_THEME=ev.currentTarget.selectedItem.IDSUR_THEME;
			eventObj.SEGMENT=ev.currentTarget.selectedItem.SEGMENT;
			eventObj.TYPE=ev.currentTarget.selectedItem.TYPE;
			eventObj.SOURCE=ev.currentTarget;
			eventObj.IDENTIFIANT=identifiant;
			dispatchEvent(eventObj);
		}

		protected function _myPieItemClickHandler(evt:ChartItemEvent):void
		{
			var eventObj:TableauChangeEvent=new TableauChangeEvent("tableauChange");
			eventObj.LIBELLE=evt.hitData.item.LIBELLE;
			eventObj.SEGMENT=evt.hitData.item.SEGMENT_THEME;
			eventObj.TYPE=TYPE_SEGMENT;
			eventObj.SOURCE=myPieGraph;
			eventObj.IDENTIFIANT=identifiant;

			dispatchEvent(eventObj);
		}

		private var _surthmeAbo:SurThemeAbonnement=new SurThemeAbonnement();

		// permet de formatter la collection de DataProvider 
		protected function _formatterDataProvider(d:ArrayCollection):ArrayCollection
		{
			var tmpCollection:ArrayCollection=new ArrayCollection();
			var o:Object;
			var cptr:int=0;
			var arrayCol:ArrayCollection;
			var mobileArray:Array=new Array();
			var fixeArray:Array=new Array();
			var dataArray:Array=new Array();
			_total=0;

			for (o in d)
			{
				var sgmtThem:String=d[o].segment_theme.toUpperCase();

				var idsegment_theme:Number=d[o].idsegment_theme;

				switch (idsegment_theme)
				{
					case 2:
						mobileArray.push(formateObject(d[o], idsegment_theme));
						break;
					case 1:
						fixeArray.push(formateObject(d[o], idsegment_theme));
						break;
					case 3:
						dataArray.push(formateObject(d[o], idsegment_theme));
						break;
				}
			}

			if (fixeArray.length > 0)
			{
				arrayCol=new ArrayCollection();
				var obj0:Object=new Object();
				obj0["LIBELLE"]=fixeArray[0].SEGMENT;
				obj0["SEGMENT"]=fixeArray[0].SEGMENT;
				obj0["NBLIGNE"]=this.nbrLigneFixe.toString();
				obj0["TYPE"]="SEGMENT";
				obj0["QUANTITE"]=" ";
				obj0["MONTANT_TOTAL"]=calculTotal(fixeArray, "MONTANT_TOTAL");
				obj0["IDSEGMENT_THEME"]=1;
				_total=_total + obj0["MONTANT_TOTAL"];

				arrayCol=new ArrayCollection(fixeArray);

				for (cptr=0; cptr < _surthmeAbo.surThemeAbonnementFixe.length; cptr++)
				{
					if (!ConsoviewUtil.isIdInArray(_surthmeAbo.surThemeAbonnementFixe[cptr].IDSUR_THEME, "IDSUR_THEME", arrayCol.source))
					{
						arrayCol.addItemAt(_surthmeAbo.surThemeAbonnementFixe[cptr], cptr);
					}
				}
				arrayCol.addItemAt(obj0, 0);
				tmpCollection.addItem(arrayCol.source);
			}
			else
			{
				arrayCol=new ArrayCollection();
				var obj01:Object=new Object();
				obj01["LIBELLE"]=ResourceManager.getInstance().getString('M311', 'Fixe');
				
				obj01["NBLIGNE"]=this.nbrLigneFixe.toString();
				obj01["TYPE"]="SEGMENT";
				obj01["QUANTITE"]=" ";
				obj01["IDSEGMENT_THEME"]=1;
				obj01["MONTANT_TOTAL"]=calculTotal(new Array, "MONTANT_TOTAL");
				_total=_total + obj01["MONTANT_TOTAL"];

				for (cptr=0; cptr < _surthmeAbo.surThemeAbonnementFixe.length; cptr++)
				{
					arrayCol.addItem(_surthmeAbo.surThemeAbonnementFixe[cptr]);
				}
				obj01["SEGMENT"]=_surthmeAbo.surThemeAbonnementFixe[cptr-1].SEGMENT;
				arrayCol.addItemAt(obj01, 0);
				tmpCollection.addItem(arrayCol.source);
			}

			if (mobileArray.length > 0)
			{
				arrayCol=new ArrayCollection();
				var obj:Object=new Object();
				obj["LIBELLE"]=mobileArray[0].SEGMENT;
				obj["SEGMENT"]=mobileArray[0].SEGMENT;
				obj["NBLIGNE"]=this.nbrLigneMobile.toString();
				obj["TYPE"]="SEGMENT";
				obj["QUANTITE"]=" ";
				obj["IDSEGMENT_THEME"]=2;
				obj["MONTANT_TOTAL"]=calculTotal(mobileArray, "MONTANT_TOTAL");
				_total=_total + obj["MONTANT_TOTAL"];

				arrayCol=new ArrayCollection(mobileArray);

				for (cptr=0; cptr < _surthmeAbo.surThemeAbonnementMobile.length; cptr++)
				{
					if (!ConsoviewUtil.isIdInArray(_surthmeAbo.surThemeAbonnementMobile[cptr].IDSUR_THEME, "IDSUR_THEME", arrayCol.source))
					{
						arrayCol.addItemAt(_surthmeAbo.surThemeAbonnementMobile[cptr], cptr);
					}
				}
				arrayCol.addItemAt(obj, 0);
				tmpCollection.addItem(arrayCol.source);
			}
			else
			{
				var obj11:Object=new Object();
				obj11["LIBELLE"]=ResourceManager.getInstance().getString('M311', 'Mobile');
				obj11["SEGMENT"]=ResourceManager.getInstance().getString('M311', 'SGMT_MOBILE');
				obj11["NBLIGNE"]=this.nbrLigneMobile.toString();
				obj11["TYPE"]="SEGMENT";
				obj11["QUANTITE"]=" ";
				obj11["IDSEGMENT_THEME"]=2;
				obj11["MONTANT_TOTAL"]=calculTotal(new Array, "MONTANT_TOTAL");
				_total=_total + obj11["MONTANT_TOTAL"];

				arrayCol=new ArrayCollection();

				for (cptr=0; cptr < _surthmeAbo.surThemeAbonnementMobile.length; cptr++)
				{
					arrayCol.addItem(_surthmeAbo.surThemeAbonnementMobile[cptr]);
				}
				obj11["SEGMENT"]=_surthmeAbo.surThemeAbonnementMobile[cptr-1].SEGMENT;
				arrayCol.addItemAt(obj11, 0);
				tmpCollection.addItem(arrayCol.source);
			}

			if (dataArray.length > 0)
			{
				arrayCol=new ArrayCollection();
				var obj1:Object=new Object();
				obj1["LIBELLE"]=dataArray[0].SEGMENT;
				obj1["SEGMENT"]=dataArray[0].SEGMENT;
				obj1["NBLIGNE"]=this.nbrLigneData.toString();
				obj1["TYPE"]="SEGMENT";
				obj1["QUANTITE"]=" ";
				obj1["IDSEGMENT_THEME"]=3;
				obj1["MONTANT_TOTAL"]=calculTotal(dataArray, "MONTANT_TOTAL");
				_total=_total + obj1["MONTANT_TOTAL"];

				arrayCol=new ArrayCollection(dataArray);

				for (cptr=0; cptr < _surthmeAbo.surThemeAbonnementData.length; cptr++)
				{
					if (!ConsoviewUtil.isIdInArray(_surthmeAbo.surThemeAbonnementData[cptr].IDSUR_THEME, "IDSUR_THEME", arrayCol.source))
					{
						arrayCol.addItemAt(_surthmeAbo.surThemeAbonnementData[cptr], cptr);
					}
				}
				
				arrayCol.addItemAt(obj1, 0);
				tmpCollection.addItem(arrayCol.source);
			}
			else
			{
				var obj12:Object=new Object();
				obj12["LIBELLE"]=ResourceManager.getInstance().getString('M311', 'Data');
				obj12["SEGMENT"]=ResourceManager.getInstance().getString('M311', 'SGMT_DATA');
				obj12["NBLIGNE"]=this.nbrLigneData.toString();
				obj12["TYPE"]="SEGMENT";
				obj12["QUANTITE"]=" ";
				obj12["IDSEGMENT_THEME"]=3;
				obj12["MONTANT_TOTAL"]=calculTotal(new Array, "MONTANT_TOTAL");
				_total=_total + obj12["MONTANT_TOTAL"];

				arrayCol=new ArrayCollection();

				for (cptr=0; cptr < _surthmeAbo.surThemeAbonnementData.length; cptr++)
				{
					arrayCol.addItem(_surthmeAbo.surThemeAbonnementData[cptr]);
				}
				
				obj12["SEGMENT"]=_surthmeAbo.surThemeAbonnementData[cptr-1].SEGMENT;
				
				arrayCol.addItemAt(obj12, 0);
				tmpCollection.addItem(arrayCol.source);
			}

			return tmpCollection;
		}

		protected function formateObject(obj:Object, idsegment_theme:int=-1):Object
		{
			var o:Object=new Object();

			o["LIBELLE"]=obj.theme_libelle;
			o["SEGMENT"]=obj.segment_theme;
			o["QUANTITE"]=ConsoviewFormatter.formatNumber(Number(obj.qte), 2);
			o["TYPE_THEME"]=obj.type_theme;
			o["SUR_THEME"]=obj.theme_libelle;
			o["IDSUR_THEME"]=obj.idsur_theme;
			o["ORDRE_AFFICHAGE"]=obj.ordre_affichage;
			o["MONTANT_TOTAL"]=obj.montant_final;
			o["TYPE"]="SURTHEME";
			o["ID"]=obj.idtheme_produit;
			o["ID_LIBELLE"]=obj[""];
			o["IDSEGMENT_THEME"]=idsegment_theme;

			return o;
		}

		protected function calculTotal(arr:Array, colname:String):Number
		{
			var total:Number=0;

			for (var i:int=0; i < arr.length; i++)
			{

				total=total + parseFloat(arr[i][colname]);
			}
			return total;
		}

		// Chargement des données par remoting
		protected function getSurThemeAbos():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M311.accueil.facade",
				"getSurThemeAbos", getSurThemeAbosResultHandler);
			RemoteObjectUtil.callService(op);
		}

		protected function getAboBySegment():void
		{
			var op2:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M311.accueil.facade", 
				"getTotalAboBySegment", getAboBySegmentResultHandler);
			RemoteObjectUtil.callService(op2);
		}

		protected function chargerDonnees():void
		{
			getSurThemeAbos();
			getAboBySegment();
//			this.ligneService.getNbreLigneParSerment(moisDeb, moisFin);
			this.ligneService.getNbreLigneParSerment();
			this.ligneService.myDatas.addEventListener(LigneEvent.LIGNES_PAR_SEGMENT_RETURNED, lignesParSegmentHandler);
			//getTitre();
		}

		protected function lignesParSegmentHandler(event:Event):void
		{
			this.nbrLigneData=this.ligneService.myDatas.nbrLigneData.toString();
			this.nbrLigneFixe=this.ligneService.myDatas.nbrLigneFixe.toString();
			this.nbrLigneMobile=this.ligneService.myDatas.nbrLigneMobile.toString();

			dataProviders=_lastDataProviders;

		}

		protected function chargerDonnneesFaultHandler(fe:FaultEvent):void
		{
			trace(fe.fault.faultString, fe.fault.name);
		}

		protected function getSurThemeAbosResultHandler(re:ResultEvent):void
		{
			try
			{
				dataProviders=re.result as ArrayCollection;
			}
			catch (er:Error)
			{
				trace(er.message, "fr.consotel.consoview.M311.accueil.facade  getSurThemeAbos ");
			}
		}

		protected function getAboBySegmentResultHandler(re:ResultEvent):void
		{
			try
			{
				segmentDataProvider=re.result as ArrayCollection;
				var cursor:IViewCursor=segmentDataProvider.createCursor();
				var valAbsolue:Number=0;
				while (!cursor.afterLast)
				{
					if (Number(cursor.current.MONTANT_TOTAL) == 0 && Number(cursor.current.QUANTITE) == 0)
					{
						cursor.remove();
					}
					else
					{
						if (cursor.current.MONTANT_TOTAL < 0)
						{
							valAbsolue=Math.abs(cursor.current.MONTANT_TOTAL);
							cursor.current.MONTANT_TOTAL_AFFICHEE=valAbsolue;
						}
						else
						{
							cursor.current.MONTANT_TOTAL_AFFICHEE=cursor.current.MONTANT_TOTAL;
						}
						cursor.moveNext();
					}

				}
				segmentDataProvider.refresh();
			}
			catch (er:Error)
			{
				trace(er.message, "fr.consotel.consoview.M311.accueil.facade  getSurThemeAbos ");
			}
		}


		/*
		 protected function getTitre():void{
		  opTitre = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.M311.accueil.facade",
																			"getLibelleAbo",
																			getTitreResultHandler,null);


			RemoteObjectUtil.callService(opTitre,perimetre,modeSelection);
		 }

		 protected function getTitreResultHandler(re : ResultEvent):void{
		  title = String(re.result);
		 }

		*/

		[Bindable]
		public function set segmentDataProvider(value:ArrayCollection):void
		{
			if (value != null && value != _segmentDataProvider)
			{
				_segmentDataProvider=value;
				invalidateProperties();
				invalidateSize();
				invalidateDisplayList();
			}
		}

		public function get segmentDataProvider():ArrayCollection
		{
			return _segmentDataProvider;
		}

		protected function pielabelFunction(data:Object, field:String, index:Number, percentValue:Number):String
		{
			var temp:String=String(" " + percentValue).substr(0, 6);

			return data.LIBELLE + ": " + '\n' + ResourceManager.getInstance().getString('M311', 'Total___') + moduleTableauDeBordE0IHM.formatNumberCurrency(data.MONTANT_TOTAL, 2) + '\n' + temp + "%";
		}

		protected function pieDataTipFunction(hitData:HitData):String
		{
			var libelle:String=hitData.item.LIBELLE;
			var montant:Number=hitData.item.MONTANT_TOTAL;
			var prop:Number=Math.abs((montant * 100 / _total));

			return ResourceManager.getInstance().getString('M311', 'Libell_____b_') + libelle + ResourceManager.getInstance().getString('M311', '__b__br_montant____b__font_color____ff00') + moduleTableauDeBordE0IHM.formatNumberCurrency(montant, 2) + ResourceManager.getInstance().getString('M311', '__font___b__br_Proportion___b_') + prop.toPrecision(4) + "%</b>";
		}

		[Bindable]
		public function get moisFin():String
		{
			return _moisFin;
		}

		public function set moisFin(value:String):void
		{
			_moisFin=value;
		}

		[Bindable]
		public function get moisDeb():String
		{
			return _moisDeb;
		}

		public function set moisDeb(value:String):void
		{
			_moisDeb=value;
		}

		public function get nbrLigneMobile():String
		{
			return _nbrLigneMobile;
		}

		public function set nbrLigneMobile(value:String):void
		{
			_nbrLigneMobile=value;
		}

		public function get nbrLigneFixe():String
		{
			return _nbrLigneFixe;
		}

		public function set nbrLigneFixe(value:String):void
		{
			_nbrLigneFixe=value;
		}

		public function get nbrLigneData():String
		{
			return _nbrLigneData;
		}

		public function set nbrLigneData(value:String):void
		{
			_nbrLigneData=value;
		}

		/**
		 * Retourne le montant total
		 * */
		public function get total():Number
		{
			return _total;
		}
	}
}
