package ihm.popup.interlocuteur
{
	import composants.ui.TitleWindowBounds;
	import composants.util.CvDateChooser;
	
	import entity.CiviliteVO;
	import entity.CodeAction;
	import entity.InterlocuteurVO;
	import entity.LangueVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.EmailValidator;
	import mx.validators.StringValidator;
	import mx.validators.Validator;
	
	import service.FctGestionPartenaireService;
	
	public class PopUpCreateInterlocuteurImpl extends TitleWindowBounds
	{
		private var _iServiceGestPartenaire	:FctGestionPartenaireService;
		private var _idSelectedPartn		:int;
		
		public var combo_civilite	:ComboBox;
		[Bindable]
		public var ti_nom			:TextInput;
		public var ti_prenom		:TextInput;
		public var ti_qualite		:TextInput;
		[Bindable]
		public var ti_email			:TextInput;
		public var cbx_principal	:CheckBox;
		public var combo_langue		:ComboBox;
		[Bindable]
		public var df_dateDebut		:CvDateChooser;
		[Bindable]
		public var df_dateFin		:CvDateChooser;
		public var sv_nom			:StringValidator;
		public var ev_mail			:EmailValidator;
		
		public function PopUpCreateInterlocuteurImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		private function init(event:FlexEvent):void
		{
			callLater(combo_civilite.setFocus, null);
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValider_clickHandler(evt:MouseEvent):void
		{
			if(validateEnteredInterlocuteur() == true){
				var nvInterloc:InterlocuteurVO = new InterlocuteurVO();
				nvInterloc.id 			= 0; // pour un nouveau interloc Id = 0
				nvInterloc.civilite 	= (combo_civilite.selectedItem != null)?(combo_civilite.selectedItem as CiviliteVO).libelleCivilite:'';
				nvInterloc.nom 			= ti_nom.text;
				nvInterloc.prenom		= ti_prenom.text;
				nvInterloc.qualite		= ti_qualite.text;
				nvInterloc.email		= ti_email.text;
				nvInterloc.principal	= (cbx_principal.selected == true)?1:0;
				nvInterloc.langue 		= (combo_langue.selectedItem != null)?(combo_langue.selectedItem as LangueVO).CODELANGUE:'fr_FR';
				nvInterloc.dateDebut	= DateField.stringToDate(df_dateDebut.text, ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_'));
				nvInterloc.dateFin		= DateField.stringToDate(df_dateFin.text, ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_'));
				
				this.iServiceGestPartenaire.ajouterEditerInterloc(CvAccessManager.CURRENT_FUNCTION, CodeAction.CREATE_UPD_INTERLOC, idSelectedPartn, nvInterloc);				
				this.close_clickHandler(evt);	
			}
		}
		
		private function validateEnteredInterlocuteur():Boolean
		{
			var result:Boolean = false;
			var validationResult:Array = Validator.validateAll([sv_nom, ev_mail]);
			
			if (validationResult.length == 0)
			{
				result = true;
			}
			else
			{
				result = false;
			}
			
			return result;
		}
		
		protected function dateField_labelFormat(item:Date):String
		{
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_');
			return dateFormatter.format(item);
			
		}
		
		public function get iServiceGestPartenaire():FctGestionPartenaireService
		{
			return _iServiceGestPartenaire;
		}
		
		public function set iServiceGestPartenaire(value:FctGestionPartenaireService):void
		{
			_iServiceGestPartenaire = value;
		}
		
		public function get idSelectedPartn():int { return _idSelectedPartn; }
		
		public function set idSelectedPartn(value:int):void
		{
			if (_idSelectedPartn == value)
				return;
			_idSelectedPartn = value;
		}
		
	}
}