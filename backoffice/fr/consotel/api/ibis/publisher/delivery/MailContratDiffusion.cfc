<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.MailContratDiffusion" extends="Mail"
hint="Stratégie identifiée par le type MailContratDiffusion basée sur Mail permettant d'utiliser un contenu prédéfini pour le corps du mail
Le contenu prédéfini est fonction de la valeur de la propriété ID qui a été fournie à la stratégie de diffusion
Aucun contenu prédéfini n'est utilisé dans le corps du mail lorsque le contenu du rapport est déjà à diffuser dans le corps du mail">
	<cffunction access="private" name="getEmailBody" returntype="Any" hint="Retourne le contenu utilisé dans pour la diffusion identifiée par la propriété ID fournie à la stratégie de diffusion">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent" required="true">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<cfset var reportingEvent=emailEvent.getReportingEvent()>
		<cfset var deliveryProps=reportingEvent.getDelivery()>
		<cfif structKeyExists(deliveryProps,"ID") AND isValid("Array",deliveryProps["ID"]) AND (arrayLen(deliveryProps["ID"]) GT 0)>
			<cfset var idDiffusion=VAL(deliveryProps["ID"][1])>
			<cfset var procName="PKG_PFGP_SUIVI_CHARGEMENT.getMailBody">
			<cfstoredproc datasource="ROCOFFRE" procedure="#procName#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDiffusion#">
				<cfprocresult name="qMailBody">
			</cfstoredproc>
			<!--- Vérification du code d'erreur retourné par la procédure --->
			<cfif structKeyExists(qMailBody,"ERROR")>
				<cfset var errorMsg="">
				<cfif qMailBody.recordcount GT 0>
					<cfset errorMsg=qMailBody["ERROR"][1]>
				</cfif>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La procédure #procName# a retourné un code d'erreur" detail="Contenu du code d'erreur (Colonne ERROR) : #errorMsg#">
			<cfelse>
				<!--- Vérification du contenu de la procédure --->
				<cfif qMailBody.recordcount EQ 1>
					<cfif structKeyExists(qMailBody,"MAIL_BODY")>
						<cfreturn qMailBody["MAIL_BODY"][1]>
					<cfelse>
						<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La procédure #procName# doit contenir une colonne MAIL_BODY" detail="Id de diffusion : #idDiffusion#">
					</cfif>
				<cfelse>
					<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
					message="La procédure #procName# a retourné #qMailBody.recordcount# enregistrements pour l'ID de diffusion #idDiffusion#. La valeur doit être exactement : 1">
				</cfif>
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La propriété ID n'est pas été fournie ou est mal définie pour la stratégie de diffusion">
		</cfif>
	</cffunction>
</cfcomponent>