package ihm.diffusion
{
	import event.diffusion.DiffusionEvent;
	
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	
	import rapport.event.RapportEvent;
	import rapport.event.RapportIHMEvent;
	import rapport.ihm.RapportEtapeUneIHM;
	import rapport.vo.Rapport;


	public class NouvelleDiffusionImpl extends VBox
	{
		public var selectedRapport:Rapport;
		public var myViewStack:ViewStack;
		public var rapportE1IHM:RapportEtapeUneIHM;
		public var diffusionIHM:DiffusionIHM;
		public var btn_paramDiffusion:Button;
		public var choixRapportButton:Button;
	
		public function NouvelleDiffusionImpl()
		{
			super();
		}

		/**
		 * initialiser le IHM et appeler la mothode  getAllRapport
		 * @param event
		 */
		protected function nouvelleDiffusionImplHandler(event:FlexEvent):void
		{
			btn_paramDiffusion.styleName="buttonRapportDesactivate";
			btn_paramDiffusion.enabled=false;
			rapportE1IHM.rechercherRapportService.showAll=0;
			rapportE1IHM.moduleService.model.addEventListener(RapportEvent.USER_MODULE_UPDATED, getAllRapport); // lors que les modules sont prêts on appelle la fonction getAllRapport
			diffusionIHM.addEventListener(DiffusionEvent.BACK_CLICK_EVENT,retourHandler);
			diffusionIHM.addEventListener(DiffusionEvent.CANCEL_CLICK_EVENT,cancelHandler);
		}
		
		/**
		 * retouner à l'etape 1 en cliquant sur le button retour de DiffusionIHM
		 * @param evt
		 */		
		private function retourHandler(evt:DiffusionEvent):void
		{
			myViewStack.selectedChild=rapportE1IHM;// passer l'etape 1 		
			btn_paramDiffusion.styleName="buttonRapportDesactivate";
			choixRapportButton.styleName="buttonRapportActif";
			btn_paramDiffusion.enabled=false;
		}
		
		/**
		 * permet d'initialiser L'ihm (étape 2)  
		 * @param evt
		 */		
		private function cancelHandler(evt:DiffusionEvent):void
		{
			retourHandler(null);
			getAllRapport(null);
			rapportE1IHM.resetDataIHMSingleton();
			rapportE1IHM.initModuleRapport();// appeller le service qui permet de récupérer la liste des module
		}
		/**
		 * permet de charger la liste des rapport d'un module
		 * @param event
		 */
		private function getAllRapport(event:RapportEvent):void
		{
			rapportE1IHM.getListeRapport();
		}

		/**
		 * permet d'activer le rapportE1IHM et mettre un style à chauqe button
		 * @param event
		 */
		protected function choixRapportButton_clickHandler(event:MouseEvent):void
		{
			myViewStack.selectedChild=rapportE1IHM;
			btn_paramDiffusion.styleName="buttonRapportDesactivate";
			choixRapportButton.styleName="buttonRapportActif";
			btn_paramDiffusion.enabled=false;
		}

		/**
		 * permet de passer de l'etape une à l'étape 2 de l'ihm diffusion
		 */
		protected function NextClickEventHandler():void
		{
			selectedRapport=rapportE1IHM.getRapportSelected();
			diffusionIHM.initData(selectedRapport);
			myViewStack.selectedIndex=1;
			btn_paramDiffusion.styleName="buttonRapportActif";
			choixRapportButton.styleName="butonRapportInactif";
			btn_paramDiffusion.enabled=true;
		}
	}
}
