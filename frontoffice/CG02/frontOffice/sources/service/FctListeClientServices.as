package service
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import utils.abstract.AbstractRemoteService;

	public class FctListeClientServices extends AbstractRemoteService
	{
		private var _ApiConsoleGestion		:String = "fr.consotel.consoview.M28.APIconsolegestion";
		[Bindable] public var myDatas 		: FctListeClientDatas;
		[Bindable] public var myHandlers 	: FctListeClientHandlers;
		
		private var composantfctClient		:String = "fr.consotel.consoview.M28.fct.FctLCLT"
		
		public function FctListeClientServices()
		{
			myDatas 	= new FctListeClientDatas();
			myHandlers 	= new FctListeClientHandlers(myDatas);
		}
		public function getListeClientContractuel(codeApp:int=0,partenaireid:int=0,offreid:int=0,strRecherche:String=""):void
		{
			 var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				 composantfctClient, 
				 "getListeClientContractuel",
				 myHandlers.getListeClientHandler);
			 
			 RemoteObjectUtil.callService(op,codeApp,partenaireid,offreid,strRecherche);
		}
		public function getData():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				composantfctClient, 
				"getData",
				myHandlers.getDataHandler);
			
			RemoteObjectUtil.callService(op);
		}
		public function getListePartenaireOffreFromApplication(codeApp:int = 0):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				composantfctClient, 
				"getDataFromApplication",
				myHandlers.getListePartenaireOffreFromApplicationHandler);
			
			RemoteObjectUtil.callService(op,codeApp);
		}
	}
}