﻿
	<!---
	
	CODE_INTERNE FLAG_TRAITE IDARTICLE IDCOMMANDE IDEMPLOYE IDSIM IDSOUS_TETE IDTERMINAL IDTYPE_EQUIPEMENT IMEI
	LIBELLE_EQ MATRICULE NB_TERM NOM NUMERO_OPERATION SEGMENT_DATA SEGMENT_FIXE SEGMENT_MOBILE SOUS_TETE S_IMEI S_PIN1 S_PUK1
	
	--->

	<!--- VARIABLES --->

	<cfset libelleheader 	= FORM.LIBELLE_HEADER>
	<cfset idscommande	 	= FORM.DATA_TO_EXPORT>
	<cfset idpool		 	= FORM.IDPOOL>
	<cfset idsegment		= FORM.SEGMENT>
	<cfset filename		 	= FORM.FILE_NAME>
	
	<cfset reffile	 	= ''>
	
	<cfset idscommande 	= ListToArray(idscommande)>
	<cfset directory 	= "/container/importdemasse/">
	<cfset uuid 		= createUUID()>
	<cfset path 		= #directory# & #uuid#>

	<cfset commandeObject = createObject('component',"fr.consotel.consoview.M16.v2.CommandeService")>
	
	<!--- VARIABLES --->

	<cfif idsegment GT 1><!--- SEGMENT FIXE/DATA --->
		
		<cfset reffile = 'exportmultireferences_fixe.xls'>

	<cfelse><!--- SEGMENT MOBILE --->

		<cfset reffile = 'exportmultireferences_mobile.xls'>

	</cfif>

	<!--- CREATION DU REPERTOIRE ET COPIE DU FICHIER *.XLS POUR POUVOIR ECRIRE DEDANS --->

	<cfif not DirectoryExists('#path#')>

		<cfdirectory action="Create" directory="#path#" type="dir" mode="777">
	
	<cfelse>
		
		<cfset uuid = createUUID()>
		
		<cfset path = #directory# & #uuid#>
		
		<cfdirectory action="Create" directory="#path#" type="dir" mode="777">
	
	</cfif>
		
	<cfif DirectoryExists('#path#')>
		
		<cfset templatepath 	= GetCurrentTemplatePath()/>
		<cfset currentdirectory = GetDirectoryFromPath(templatepath)/>
		
		<!--- <cfset currentdirectory = GetDirectoryFromPath("/webroot/backoffice-nicolas/fr/consotel/consoview/importdemasse/exports/")/> --->
							
		<cffile action="copy" source="#currentdirectory#/#reffile#" destination="#path#" mode="777">
				
		<cffile action="rename" source="#path#/#reffile#" destination="#path#/#filename#" attributes="normal">
	
	</cfif>
		
	<!--- LECTURE DU FICHIER VIDE POUR OBJTENIR UN SPREADSHEET --->
	<cfspreadsheet action="read" src="#path#/#filename#" sheetname="Sheet1" name="sObj"/> 
		
	<!--- CREATION DU REPERTOIRE ET COPIE DU FICHIER *.XLS POUR POUVOIR ECRIRE DEDANS --->
	
	<!--- TETE DE COLONNES --->
	<cfset SpreadsheetAddRow(sObj,"#libelleheader#")>
	
	<cfloop index="idx" from="1" to="#ArrayLen(idscommande)#">
	
		<cfset idcommande = idscommande[idx]>
	
		<cfset commande = commandeObject.fournirDetailOperation(#idcommande#)>
		
		<cfdump var="#commande#">
		
		<cfif commande.recordcount GT 0>

			<cfset idtypecmd	= commande['IDTYPE_OPERATION'][commande.recordcount]>
			<cfset numeroope	= commande['NUMERO_OPERATION'][commande.recordcount]>
			<cfset idrevendeur 	= commande['IDREVENDEUR'][commande.recordcount]>
			<cfset librevendeur = commande['LIBELLE_REVENDEUR'][commande.recordcount]>
			<cfset idoperateur 	= commande['OPERATEURID'][commande.recordcount]>
			<cfset liboperateur = commande['LIBELLE_OPERATEUR'][commande.recordcount]>
			<cfset idpool 		= commande['IDPOOL_GESTIONNAIRE'][commande.recordcount]>
		
			<cfset refinfos = commandeObject.fournirReferencesInfos(#idcommande#,#idpool#)>

			<cfquery name="getDataExport" dbtype="query">
				SELECT *
				FROM refinfos
				ORDER BY IDSOUS_TETE
			</cfquery>
			
			<cfset datatoexport = getDataExport>

			<cfif idsegment GT 1><!--- SEGMENT FIXE/DATA --->

				<!--- AFFECTATION DES DONNÉES AU FICHIER *.XLS --->
				<cfloop index="idx1" from="1" to="#datatoexport.recordcount#">

					<cfset libelleequ 	= ToString(datatoexport['LIBELLE_EQ'][idx1])>
					<cfset numeroeequ 	= ToString(datatoexport['IMEI'][idx1])>
					<!---<cfset soustete 	= "n° " & ToString(datatoexport['SOUS_TETE'][idx1])>--->
					<cfset soustete 	= ToString(datatoexport['SOUS_TETE'][idx1])>
					<cfset idsoustete	= ToString(datatoexport['IDSOUS_TETE'][idx1])>
	
					<cfif idtypecmd EQ 1583><!--- Commande de nouvelles lignes fixes --->
						
						<cfset libelleequ = "">
						<cfset numeroeequ = "">
						
						<cfset SpreadsheetAddRow(sObj,"#numeroope#,#libelleequ#,#numeroeequ#,#soustete#,#idcommande#,#idsoustete#")>
			
					<cfelseif idtypecmd EQ 1584><!--- Commande d'équipements fixe nus --->
						
						<cfset soustete = "">
						
						<cfset SpreadsheetAddRow(sObj,"#numeroope#,#libelleequ#,#numeroeequ#,#soustete#,#idcommande#,#idsoustete#")>
			
					<cfelseif idtypecmd EQ 1587><!--- Réengagement fixe --->
					
						<cfset libelleequ = "">
						<cfset numeroeequ = "">
					
						<cfset SpreadsheetAddRow(sObj,"#numeroope#,#libelleequ#,#numeroeequ#,#soustete#,#idcommande#,#idsoustete#")>
				
					</cfif>
				
				</cfloop>
				
			<cfelse><!--- SEGMENT MOBILE --->
			
				<!--- AFFECTATION DES DONNÉES AU FICHIER *.XLS --->
				<cfloop index="idx1" from="1" to="#datatoexport.recordcount#">
				
					<cfset codeinterne 	= ToString(datatoexport['CODE_INTERNE'][idx1])>
					<cfset nomcollab 	= ToString(datatoexport['NOM'][idx1])>
					<cfset libelleequ 	= ToString(datatoexport['LIBELLE_EQ'][idx1])>
					<cfset numeroeequ 	= ToString(datatoexport['IMEI'][idx1])>
					<!---<cfset soustete 	= "n° " & ToString(datatoexport['SOUS_TETE'][idx1])>--->
					<cfset soustete 	= ToString(datatoexport['SOUS_TETE'][idx1])>
					<cfset idsoustete	= ToString(datatoexport['IDSOUS_TETE'][idx1])>
					<cfset numerosim 	= ToString(datatoexport['S_IMEI'][idx1])>
					<cfset codepuk		= ToString(datatoexport['S_PUK1'][idx1])>

					<cfif idtypecmd EQ 1083><!--- Commande de nouvelles lignes mobiles --->
		
						<cfif datatoexport['IDTERMINAL'][idx1] EQ 0><!--- nouvelle ligne sans equipements --->
						
						<cfset numeroeequ = "">
						
							<cfset SpreadsheetAddRow(sObj,"#numeroope#,#codeinterne#,#nomcollab#,#libelleequ#,#numeroeequ#,#soustete#,#numerosim#,#codepuk#,#idcommande#,#idsoustete#")>
						
						<cfelse><!--- nouvelle ligne avec equipements --->
						
							<cfset SpreadsheetAddRow(sObj,"#numeroope#,#codeinterne#,#nomcollab#,#libelleequ#,#numeroeequ#,#soustete#,#numerosim#,#codepuk#,#idcommande#,#idsoustete#")>
						
						</cfif>
			
					<cfelseif idtypecmd EQ 1182><!--- Commande d'équipements mobile nus --->
						
						<cfset soustete 	= "">
						<cfset numerosim 	= "">
						<cfset codepuk 		= "">
						
						<cfset SpreadsheetAddRow(sObj,"#numeroope#,#codeinterne#,#nomcollab#,#libelleequ#,#numeroeequ#,#soustete#,#numerosim#,#codepuk#,#idcommande#,#idsoustete#")>
			
					<cfelseif idtypecmd EQ 1382><!--- Réengagement mobile --->
					
						<cfset SpreadsheetAddRow(sObj,"#numeroope#,#codeinterne#,#nomcollab#,#libelleequ#,#numeroeequ#,#soustete#,#numerosim#,#codepuk#,#idcommande#,#idsoustete#")>
				
					</cfif>

				</cfloop>
			
			</cfif>
		
		</cfif>
	
	</cfloop>
	
	<cfset format 			= structnew()>
	<cfset format.hidden 	= false>
	
	<cfif idsegment GT 1><!--- SEGMENT FIXE/DATA --->
	
		<cfset SpreadsheetFormatColumns(sObj,format,"1,2")>
	
		<cfset format.dataformat 	= "0">
	
		<cfset SpreadsheetFormatColumns(sObj,format,"3,4")>

		<cfset format.hidden 		= true>
		<cfset format.locked 		= true>
		
		<cfset spreadsheetformatcolumns(sObj,format,"5,6")>
	
	<cfelse><!--- SEGMENT MOBILE --->
		
		<cfset SpreadsheetFormatColumns(sObj,format,"1,2,3,4")>
	
		<cfset format.dataformat 	= "0">
	
		<cfset SpreadsheetFormatColumns(sObj,format,"5,6,7,8")>

		<cfset format.hidden 		= true>
		<cfset format.locked 		= true>
		
		<cfset spreadsheetformatcolumns(sObj,format,"9,10")>

	</cfif>
	
	<!--- SAVE IT --->
	<cfspreadsheet action="write" name="sObj" filename="#path#/#filename#" overwrite="true" sheetname="Sheet1"/>
	
	<!--- TELECHARGEMENT --->
	<cffile action="readbinary" variable="fileContent" file="#path#/#filename#">
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#">
	<cfcontent type="application/vnd.ms-excel" variable="#fileContent#">
