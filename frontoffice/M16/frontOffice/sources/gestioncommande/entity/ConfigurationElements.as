package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class ConfigurationElements
	{

		public var PORTABILITE			:Portabilite = new Portabilite();
		
		public var TOOLTIP_SPARE		:String = '';
		public var TOOLTIP_PORTA		:String = '';
		public var COLLABORATEUR		:String = '';
		public var MATRICULE			:String = '';
		public var LIBELLE				:String = '';
		public var LIBELLE_ORGA			:String = '';
		public var LIBELLE_FEUILLE		:String = '';
		public var ERROR				:String = '';
		public var TYPELIGNE			:String = '';
		public var IMEISIM				:String = '';
		
		public var IDSIM				:int = 0;
		public var IDEQUCLIENT  		:int = 0;
		public var IDEQUFOURNI  		:int = 0;
		public var LIGNE_ELIGIBLE  		:int = 0;
		public var ID_TYPELIGNE  		:int = -1;
		public var ID_COLLABORATEUR		:int = -1;
		public var ID_ORGA				:int = -1;
		public var ID_FEUILLE			:int = -1;
		public var ID_ORGA_SELECTED_IDX	:int = -1;
		
		public var IS_MOBILE			:Boolean = true;
		public var IS_PORTABILITE		:Boolean = false;
		public var IS_COLLABORATEUR		:Boolean = false;
		public var IS_ERROR				:Boolean = false;
		public var NEW_SIM				:Boolean = false;

		public var ORGANISATIONS		:ArrayCollection = new ArrayCollection();
		
		private var _ZONE:Number = 0;
		private var _PREFIX:Number = 0;
		private var _PAYSCONSOTELID:Number;
		private var _LISTE_OPERATEURS:ArrayCollection;
		
		public function ConfigurationElements()
		{
		}
		
		public function get PREFIX():Number { return _PREFIX; }
		
		public function set PREFIX(value:Number):void
		{
			if (_PREFIX == value)
				return;
			_PREFIX = value;
		}
		
		public function get ZONE():Number { return _ZONE; }
		
		public function set ZONE(value:Number):void
		{
			if (_ZONE == value)
				return;
			_ZONE = value;
		}
		
		public function get PAYSCONSOTELID():Number { return _PAYSCONSOTELID; }
		
		public function set PAYSCONSOTELID(value:Number):void
		{
			if (_PAYSCONSOTELID == value)
				return;
			_PAYSCONSOTELID = value;
		}
		
		public function get LISTE_OPERATEURS():ArrayCollection { return _LISTE_OPERATEURS; }
		
		public function set LISTE_OPERATEURS(value:ArrayCollection):void
		{
			if (_LISTE_OPERATEURS == value)
				return;
			_LISTE_OPERATEURS = value;
		}
	}
}