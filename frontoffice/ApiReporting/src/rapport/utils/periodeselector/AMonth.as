package rapport.utils.periodeselector
{
	import mx.formatters.DateFormatter;
	
	public class AMonth
	{		
		private var _debut : String;
		private var _fin : String;
		
		private var df : DateFormatter;
		private var _dateFin : Date;
		private var _dateDeb : Date;
		
		private var _idDateSelected:Number;
		
		private var _libellePeriodeSelected:String;
		
		
		public function AMonth(thisDay : Date)
		{			
			var thisMonth : Date = new Date(thisDay.getFullYear(),thisDay.getMonth()-1);
			
			df = new DateFormatter();
			
			df.formatString = "DD/MM/YYYY";			
			
			_dateFin = new Date(thisMonth.getFullYear(),thisMonth.getMonth()+1,thisMonth.getDate()-1);
			_dateDeb = thisMonth;
			
			_debut = df.format(thisMonth);				
			_fin = df.format(new Date(thisMonth.getFullYear(),thisMonth.getMonth()+1,thisMonth.getDate()-1));
		}
				

		public function get idDateSelected():Number
		{
			return _idDateSelected;
		}

		public function set idDateSelected(value:Number):void
		{
			_idDateSelected = value;
		}

		public function getDateDebut():String{
			return _debut;			
		}
		
		public function get dateDebut() : Date{
			return _dateDeb;
		}
		
		
		public function getDateFin():String{
			return _fin;
		}
		
		public function get dateFin() : Date{
			return _dateFin;
		}
		
		public function get libellePeriodeSelected():String
		{
			return _libellePeriodeSelected;
		}
		
		public function set libellePeriodeSelected(value:String):void
		{
			_libellePeriodeSelected = value;
		}
		

	}
}