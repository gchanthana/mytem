package service
{
	import composants.util.ConsoviewUtil;
	
	import event.InventaireServiceEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;

	public class InventaireModel extends EventDispatcher
	{
		//Les données ===
		[Bindable]public var listMarque:ArrayCollection;
		[Bindable]public var listModele:ArrayCollection;
		[Bindable]public var listEquipement:ArrayCollection;
		
		[Bindable]public var datePeriodeCompleteInventaire:Date;
		[Bindable]public var idDatePeriodeCompleteInventaire:Number = 0;;
		
		public function InventaireModel()
		{
			listMarque = new ArrayCollection();
			listModele = new ArrayCollection();
			listEquipement = new ArrayCollection();
			
		}
		
		public function resetMarqueEquipement():void
		{
			listMarque.removeAll();	
		}
		
		public function resetModeleEquepement():void
		{
			listModele.removeAll();	
		}
		
		public function resetListEquipement():void
		{
			listEquipement.removeAll();	
		}
		
		
		
		
		
		internal function updateDatePeriodeCompleteInventaire(value:Date,idValue:Number):void
		{
			if(value)
			{
				datePeriodeCompleteInventaire = value;
				idDatePeriodeCompleteInventaire = idValue;
				dispatchEvent(new InventaireServiceEvent(InventaireServiceEvent.INVENTAIRE_SERVICE_MODEL_DATE_COMPLETE_INVENTAIRE_UPDATED));
			}
		}
		
		internal function updateMarqueEquipement(values:ArrayCollection):void
		{
			listMarque.removeAll();			
			if(values)
			{
				var cursor:IViewCursor = values.createCursor();
				while(!cursor.afterLast)
				{
					listMarque.addItem(cursor.current);
					cursor.moveNext();
					
				}
				dispatchEvent(new InventaireServiceEvent(InventaireServiceEvent.INVENTAIRE_SERVICE_MODEL_LMARQUE_UPDATED));
			}
		} 
		
		internal function updateModeleEquepement(values:ArrayCollection):void
		{
			listModele.removeAll();
			
			if(values)
			{
				var cursor:IViewCursor = values.createCursor();
				while(!cursor.afterLast)
				{
					listModele.addItem(cursor.current)
					cursor.moveNext();
				}
				dispatchEvent(new InventaireServiceEvent(InventaireServiceEvent.INVENTAIRE_SERVICE_MODEL_LMODELE_UPDATED));
			}
		}
		
		internal function updateListEquipement(values:ArrayCollection):void
		{
			listEquipement.removeAll();
			
			if(values)
			{
				var cursor:IViewCursor = values.createCursor();
				while(!cursor.afterLast)
				{
					listEquipement.addItem(cursor.current);
					cursor.moveNext();
				}
				dispatchEvent(new InventaireServiceEvent(InventaireServiceEvent.INVENTAIRE_SERVICE_MODEL_LEQPT_UPDATED));
			}
		}		
		
		
	}
}