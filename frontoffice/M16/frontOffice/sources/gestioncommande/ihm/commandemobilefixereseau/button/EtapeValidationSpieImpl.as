package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.controls.TextArea;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import commandemobile.ihm.mail.MailCOmmandeIHM;
	
	import composants.util.ConsoviewFormatter;
	
	import gestioncommande.custom.CustomRadioButton;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.ArticleItemVO;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class EtapeValidationSpieImpl extends AbstractWorflow implements IWorflow
	{
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//		
		
		public var mailCommande					:MailCOmmandeIHM;
		
		public var listConfig					:List;
		
		public var vbxAction					:VBox;
		
		//		public var hbxRecapitulatif				:Box;
		
		public var txaCommentaires				:TextArea;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _workFlow					:WorkflowService 	= new WorkflowService();
		
		private var _engagement					:EquipementService 	= new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _myElements					:ElementsCommande2	= new ElementsCommande2();
		
		private var _cmd						:Commande;
		
		public var selectedAction				:Action;
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES BUTTON CUSSTOM
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 3;
		
		public var ACCESS						:Boolean = false;
		
		private var _razl						:Boolean = false;		
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var myItems						:ArrayCollection = new ArrayCollection();
		public var listActions					:ArrayCollection = new ArrayCollection();
		
		private var _idRandom					:int = 0;
		
		private var _idLastTypeCommande			:int = -1;
		private var _idLastGenrateNumber		:int = int(Math.random() * 1000000);
		private var _totalPrice					:Number = -1;
		private var _addArticles				:ArrayCollection;
		
		private const IDETAT					:int = 3066;
		
		private var _totaleCmd					:String = '';

		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function EtapeValidationSpieImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER DE L'IHM
		//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener('ERASE_CONFIGURATION',	earseConfigurationHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function viewStackChange():void
		{
			var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var len			:int = elements.length;
			
			_cmd		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			if(len > 0)
			{
				if(!isRandomContains(elements, _myElements))
					elements.addItem(setIdConfiguration(_myElements, len));
			}
			else
			{
				_idRandom = _myElements.RANDOM;
				elements.addItem(setIdConfiguration(_myElements, len));
			}
			
			if(vbxAction.numChildren == 0)
				fournirListeActionsPossibles();
			
			setDataToGrid();
			
		}
		
		public function reset():void
		{
		}
		
		public function setConfiguration():void
		{
			_razl = false;
			eraseLastConfiguration();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					VERIFICATION ET CONTRUCTION DES DONNÉES CHOISIES
		//--------------------------------------------------------------------------------------------//
		
		public function checkData():Boolean
		{
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
			
			return true;
		}
		
		public function buildParameters():void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION PROPRE A L'ETAPE 
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function btValidAndPurchaseClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('VALID_CONTINU', true));
		}
		
		public var lblRecapitulatif			:Label;
		public var lblEditerMail			:Label;
		
		public var isActionSelected			:Boolean = false;
		public var isViewMail				:Boolean = false;
		
		protected function lblEditerMailClickHandler(me:MouseEvent):void
		{
			if(!lblEditerMail.enabled) return;
			
			isViewMail = true;
			
			lblRecapitulatif.setStyle('textDecoration', 'underline');
			lblRecapitulatif.useHandCursor 	= true;
			lblRecapitulatif.buttonMode 	= true;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'none');
			lblEditerMail.useHandCursor = false;
			lblEditerMail.buttonMode 	= false;
			lblEditerMail.validateNow();
		}
		
		protected function lblRecapitulatifClickHandler(me:MouseEvent):void
		{
			isViewMail = false;
			
			lblRecapitulatif.setStyle('textDecoration', 'none');
			lblRecapitulatif.useHandCursor 	= false;
			lblRecapitulatif.buttonMode 	= false;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'underline');
			lblEditerMail.useHandCursor = true;
			lblEditerMail.buttonMode 	= true;
			lblEditerMail.validateNow();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function fournirListeActionsPossibles():void
		{
			_workFlow.fournirActionsNoRemote(IDETAT, _cmd, SessionUserObject.singletonSession.IDSEGMENT);
			
			addRecordOnly();
			
			if(_workFlow.listeActions.length > 0)
			{
				for(var i:int = 0;i < _workFlow.listeActions.length;i++)
				{
					if(_workFlow.listeActions[i].CODE_ACTION != 'ANNUL' && _workFlow.listeActions[i].CODE_ACTION != null && _workFlow.listeActions[i].CODE_ACTION != "")
					{
						var cstmRdbtn:CustomRadioButton = new CustomRadioButton();
						cstmRdbtn.ACTION			= _workFlow.listeActions[i] as Action;
						cstmRdbtn.addEventListener('RADIOBUTTON_CLICKHANDLER', cstmRdbtnClickHandler);
						
						vbxAction.addChild(cstmRdbtn);
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function cstmRdbtnClickHandler(e:Event):void
		{
			var myChidldren	:Array = vbxAction.getChildren();
			var lenChildren :int = vbxAction.numChildren;
			
			selectedAction = (e.target as CustomRadioButton).ACTION
			
			for(var i:int = 0; i < lenChildren;i++)
			{
				if(myChidldren[i] as CustomRadioButton)
				{
					if((myChidldren[i] as CustomRadioButton).ACTION.IDACTION != selectedAction.IDACTION)
						(myChidldren[i] as CustomRadioButton).selected = false;
				}
			}
			
			if(selectedAction.IDACTION > 0)
			{				
				isActionSelected = true;
				
				lblEditerMail.enabled = true;
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'underline');
				lblEditerMail.useHandCursor = true;
				lblEditerMail.buttonMode 	= true;
				lblEditerMail.validateNow();
			}
			else
			{
				isActionSelected = false;
				
				lblEditerMail.enabled = false;
				
				lblRecapitulatifClickHandler(new MouseEvent(MouseEvent.CLICK));
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'none');
				lblEditerMail.useHandCursor = false;
				lblEditerMail.buttonMode 	= false;
				lblEditerMail.validateNow();
			}
			
			this.validateNow();
			
			mailCommande.setAction(selectedAction);
			
			dispatchEvent(new Event("VALIDER_ENABLED", true));
		}
		
		//---> EFFACE LE CONFIGURATION SELECTIONNEE
		private function earseConfigurationHandler(e:Event):void
		{
			if(listConfig.selectedItem != null)
			{
				var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
				var len			:int = elements.length;
				var rand		:int = listConfig.selectedItem.RANDOM;
				
				(listConfig.dataProvider as  ArrayCollection).removeItemAt(listConfig.selectedIndex);
				
				for(var i:int = 0;i < len;i++)
				{
					if(elements[i].RANDOM == rand)
					{
						elements.removeItemAt(i);
						return;
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//	
		
		private function eraseLastConfiguration():void
		{
			var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var elts		:ElementsCommande2 = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			var lenElts		:int = elements.length;
			var idrand		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM;
			
			if(isRandomContains(elements, elts))
			{
				for(var i:int = 0;i < lenElts;i++)
				{
					if(SessionUserObject.singletonSession.COMMANDEARTICLE[i].RANDOM == idrand)
					{
						SessionUserObject.singletonSession.COMMANDEARTICLE.removeItemAt(i);
						return;
					}
				}
			}
		}
		
		private function isRandomContains(values:ArrayCollection, randObject:ElementsCommande2):Boolean
		{
			return values.contains(randObject);
		}
		
		private function setIdConfiguration(value:ElementsCommande2, longueur:int):ElementsCommande2
		{
			value.ID 	= longueur + 1;
			value.TYPE 	= SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			return value;
		}
		
		private function addRecordOnly():void
		{
			var cstmRdbtn:CustomRadioButton 		= new CustomRadioButton();
			cstmRdbtn.ACTION					= new Action();
			cstmRdbtn.ACTION.LIBELLE_ACTION		= ResourceManager.getInstance().getString('M16','Enregistrer_la_commande_sans_l_envoyer');
			cstmRdbtn.ACTION.IDACTION			= -1;	
			cstmRdbtn.addEventListener("RADIOBUTTON_CLICKHANDLER", cstmRdbtnClickHandler);
			
			vbxAction.addChild(cstmRdbtn);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - RECUPERATION DES ELEMENTS ET SET TO LIST
		//--------------------------------------------------------------------------------------------//
		
		private function setDataToGrid():void
		{
			var elements	:ArrayCollection = SessionUserObject.singletonSession.CURRENTCONFIGURATION.POSTES_INFRAST;
			var len			:int = elements.length;
			
			myItems.removeAll();
			var totale:Number = 0;
			for(var i:int = 0 ; i<len ; i++)
			{
				myItems.addItem(elements[i]);
				totale = totale + (elements[i] as ArticleItemVO).prixTotale;
				totaleCmd = ConsoviewFormatter.formatEuroCurrency(totale, 2)
			}
			
			
			SessionUserObject.singletonSession.COMMANDEARTICLE = myItems;
		}
		
		private function addEquipements(value:ElementsCommande2):Object
		{
			var equi	:ArrayCollection = new ArrayCollection();
			var elts	:Object = new Object();
			var lenTer	:int = value.TERMINAUX.length;
			var lenAcc	:int = value.ACCESSOIRES.length;
			var i		:int = 0;
			var qte		:int = value.CONFIGURATIONS.length;
			var prix	:Number = 0;
			
			
			for(i = 0;i < lenTer;i++)//TERMINAUX
			{
				if(value.TERMINAUX[i].IDEQUIPEMENT > 0)
				{
					elts 			= new Object();
					elts.EQUIPEMENT = value.TERMINAUX[i].EQUIPEMENT;
					elts.LIBELLE 	= value.TERMINAUX[i].LIBELLE;
					elts.QUANTITE 	= qte;
					elts.MENSUEL	= "";
					elts.PRIX		= Formator.formatTotalWithSymbole(value.TERMINAUX[i].PRIX);
					
					prix  = prix + value.TERMINAUX[i].PRIX;
					
					equi.addItem(elts);
				}
			}
			
			for(i = 0;i < lenAcc;i++)//ACCESSOIRES
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.ACCESSOIRES[i].EQUIPEMENT;
				elts.LIBELLE 	= value.ACCESSOIRES[i].LIBELLE;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= "";
				elts.PRIX		= Formator.formatTotalWithSymbole(value.ACCESSOIRES[i].PRIX);
				
				prix  = prix + value.ACCESSOIRES[i].PRIX;
				
				equi.addItem(elts);
			}
			
			var prixRessources:Number = addRessources(value, equi);
			
			prix = qte *(prix + prixRessources);
			
			var obj:Object 	= new Object();
			obj.ITEMS 	= equi;
			obj.TOTAL	= Formator.formatTotalWithSymbole(prix);
			obj.RANDOM	= value.RANDOM;
			
			return obj;
		}
		
		private function addRessources(value:ElementsCommande2, ress:ArrayCollection):Number
		{
			var elts	:Object = new Object();
			var lenAbo	:int = value.ABONNEMENTS.length;
			var lenOpt	:int = value.OPTIONS.length;
			var i		:int = 0;
			var qte		:int = value.CONFIGURATIONS.length;
			var prix	:Number = 0;
			
			for(i = 0;i < lenAbo;i++)//ABONNEMENT
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.ABONNEMENTS[i].LIBELLETHEME;
				elts.LIBELLE 	= value.ABONNEMENTS[i].LIBELLE;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= value.ABONNEMENTS[i].PRIX_STRG;
				elts.PRIX		= "";
				
				prix  = prix + value.ABONNEMENTS[i].PRIX_UNIT;
				
				ress.addItem(elts);
			}
			
			for(i = 0;i < lenOpt;i++)//OPTIONS
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.OPTIONS[i].LIBELLETHEME;
				elts.LIBELLE 	= value.OPTIONS[i].LIBELLE;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= value.OPTIONS[i].PRIX_STRG;
				elts.PRIX		= "";
				
				prix  = prix + value.OPTIONS[i].PRIX_UNIT;
				
				ress.addItem(elts);
			}
			
			return prix;
		}
		
		public function setSomme(item:Object, column:DataGridColumn):String
		{
			return ConsoviewFormatter.formatEuroCurrency((item as ArticleItemVO).prixTotale, 2);;
		}
		
		public function get totaleCmd():String { return _totaleCmd; }
		
		public function set totaleCmd(value:String):void
		{
			if (_totaleCmd == value)
				return;
			_totaleCmd = value;
		}
	}
}