package univers.parametres.rago.wizard.ihm
{
	import mx.collections.ArrayCollection;
	
	import univers.parametres.rago.entity.TypeRegle;
	
	[Bindable]
	public class ListeEtapesWorkFlowImpl extends EtapeWorkflow
	{
		private var _selectedIndex:int;
		
		override protected function commitProperties():void
		{
			if(wizzard.myRegle != null)
			{
				dataProvider[0].detail = (wizzard.myRegle.TYPE_REGLE == TypeRegle.REGLE_ANNUAIRE)?'Annuaire':(wizzard.myRegle.TYPE_REGLE == TypeRegle.REGLE_OPERATEUR)?'Opérateur':'';  
				dataProvider[1].detail = wizzard.myRegle.LIBELLE_SOURCE;
				dataProvider[2].detail = wizzard.myRegle.LIBELLE_CIBLE;
				dataProvider[3].detail = wizzard.listeExceptions;
				executeBindings(true);
			}
		} 
		
		private var _dataProvider:ArrayCollection = new ArrayCollection(
						[	{titre:" 1) Type de règle",detail:"xxxx"},
							{titre:" 2) source ",detail:"xxxx"},
							{titre:" 3) cible ",detail:"xxx"},
							{titre:" 4) Exceptions ",detail:"xxxx"},
							{titre:" 5) Informations ",detail:"xxxxxx"}
						]);
		
		public function ListeEtapesWorkFlowImpl()
		{
			 
		}

		public function set selectedIndex(value:int):void
		{
			_selectedIndex = value;
		}

		public function get selectedIndex():int
		{
			return _selectedIndex;
		}		
		
		public function get dataProvider():ArrayCollection
		{
			return _dataProvider;
		}
	}
}