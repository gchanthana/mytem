<cfcomponent name="SendMail" alias="fr.consotel.consoview.M111.SendMail">

	<cfset properties 	 = structNew()>
		
	<!--- TRAITEMENT DES DONNEES DE L'EVENT RECUPERER --->
	<cffunction access="remote" name="sendMail" returntype="void">
		<cfargument name="eventObject" 	type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
			
			<cfset var mailing  	 = createObject("component","fr.consotel.consoview.M01.Mailing")>				

			<cfset var uuidlog		 = #ARGUMENTS.eventObject.getEventTarget()#>
			<cfset var jobid		 = #ARGUMENTS.eventObject.getJobId()#>
			<cfset var tag			 = #ARGUMENTS.eventObject.getEventType()#>
			
			<cfset var ImportTmp	 = "/container/M111/">
			<cfset var pathEntier	 = "#ImportTmp##uuidlog#/">

			<cfset rsltinfos  = ''>
			<cfset emailinfos = ''>
			
			<cfset rsltinfos = updateMailContent(uuidlog, tag)>
			
			<cfset create_result = 0>
			<cfset q = rsltinfos.FILES[1]>
			
			<cfloop index="i" from="1" to="3">
				<cfset create_result = create_result + q['IS_CRATE'][#i#]/>
			</cfloop>
			
			<cfif create_result EQ 2>
				
				<cfset emailinfos = rsltinfos.MAIL>
				<cfset sendMailContent(pathEntier, uuidlog, emailinfos)>
				
			<cfelse>
			
				<cfreturn>
			
			</cfif>

	</cffunction>

	<!--- RECUPERE LES PARAMETRES DU MAIL ET LES MISE A JOUR DES PARAMETRES--->
	<cffunction access="remote" name="updateMailContent" returntype="Struct">
		<cfargument name="uuid" 	type="String" 	required="true">
 		<cfargument name="tag" 		type="String" 	required="true"/>
			
			<cfset mailing = createObject("component","fr.consotel.consoview.M01.Mailing")>
			
			<cfset mailinginfos = ArrayNew(1)>
			<cfset mailinfos 	= structnew()>
			<cfset rsltinfos 	= structnew()>
			
			<cfset p_email = mailing.getMailToDatabase(#uuid#)>
			
			<cfloop query="p_email">
				
				<cfif p_email.TAG EQ tag>
					
					<cfset p_email.IS_CRATE = mailing.updateMailToDatabase(1, uuid, tag)>
					
					<cfif p_email.IS_CRATE EQ 1>
				
						<cfset ArrayAppend(mailinginfos, p_email)>
					
					</cfif>	
						
				</cfif>
				
				<cfif p_email.TAG EQ "INF">
					
					<cfset email = structnew()>
					<cfset email.FROM 			= p_email.MAIL_FROM>
					<cfset email.GESTIONNAIRE	= p_email.MAIL_TO>
					<cfset email.REVENDEUR 		= p_email.MAIL_BCC>
					<cfset email.SUBJECT 		= p_email.MAIL_SUBJECT>
					<cfset email.IDPROFIL	 	= p_email.MAIL_BODY>
					<cfset email.CODE_APPLI 	= p_email.CODE_APPLI>
					
					<cfset mailinfos = email>

				</cfif>
			</cfloop>
			
			<cfset properties = getProperties(mailinfos.CODE_APPLI, 'NULL')>
			
			<cfif NOT isDefined('properties.MAIL_SERVER')>
				<cfset properties.MAIL_SERVER = 'mail-cv.consotel.fr'>
			</cfif>
			
			<cfif NOT isDefined('properties.MAIL_PORT')>
				<cfset properties.MAIL_PORT = 25>
			</cfif>
			
			<cfset rsltinfos.FILES = mailinginfos>
			<cfset rsltinfos.MAIL  = mailinfos>
			
		<cfreturn rsltinfos>
	</cffunction>

	<!--- RECUPERER LES INFORMATIONS POUR LE SERVEUR DE MAILS --->
	<cffunction access="remote" name="getProperties" returntype="Struct">
		<cfargument name="codeapp" 	type="Numeric" 	required="true">
 		<cfargument name="key" 		type="String" 	required="true"/>

			<cfset mailProperties  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
			
			<cfset rsltProperties  	= mailProperties.get_appli_properties(codeapp, key)>
			
			<cfset properties 	 	= structNew()>
			<cfset codeappPropert	= structNew()>
			
			<cfloop query="rsltProperties">
				<cfset codeappPropert[rsltProperties.KEY] = rsltProperties.VALUE>
			</cfloop>
			
		<cfreturn codeappPropert>
	</cffunction>

	<!--- ENVOI DU MAIL --->
	<cffunction access="remote" name="sendMailContent" returntype="void">
		<cfargument name="pathEntier" 	type="String" required="true">
		<cfargument name="uuidlog" 		type="String" required="true">
		<cfargument name="mail" 		type="Struct" required="true">
			
			<cfset var myDir 	= GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset var myVar 	= false>
			<cfset var myFile 	= "body.html">
			<cfset var ImportTmp= "/container/M111/">
			<cfset var uuid = ''>

			<cfloop condition="myVar eq false">
			  <cfset uuid = createUUID()>
			  <cfif NOT DirectoryExists('#myDir##uuid#')>
				<cfdirectory action="Create" directory="#myDir##uuid#" type="dir" mode="777">
				<cfset myVar="true">
			  </cfif>
			</cfloop>
			
			<cfset root = "#myDir##uuid#">
			
			<cfdirectory name="dGetDir" directory="#pathEntier#" action="List">
			
			<cffile action="copy" source="#ImportTmp##uuidlog#/#myFile#" destination="#root#">
			
			<cfif mail.IDPROFIL EQ 1>
					
				<cfset destinataire = mail.GESTIONNAIRE>
				<cfset copydest		= mail.REVENDEUR>
				
			<cfelse>
			
				<cfset destinataire	= mail.REVENDEUR>
				<cfset copydest  	= mail.GESTIONNAIRE>
			
			</cfif>
			
			<cfmail from="#mail.FROM#" to="#destinataire#" subject="#copydest#" type="text/html" 
					server="#properties.MAIL_SERVER#" port="#properties.MAIL_PORT#">		
				<cfinclude template="/fr/consotel/consoview/M111/#uuid#/#myFile#">
				<cfmailparam file="#ImportTmp##uuidlog#/#uuidlog#.pdf">
			</cfmail>
			
			<cfdirectory name="dGetDir" directory="#root#" action="List"> 
						
			<cfloop query="dGetDir">
				<cfif type NEQ "DIR">
					<cffile action="delete" file="#root#/#dGetDir.name#" variable="content">
				</cfif> 
			</cfloop>
			
			<cfdirectory action="delete" directory="#myDir##uuid#">

	</cffunction>

</cfcomponent>