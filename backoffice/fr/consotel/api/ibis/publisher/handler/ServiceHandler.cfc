<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.ServiceHandler" implements="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Event Handler Implementation">
	
	<!--- EVENT_HANDLER INTERFACE (Auteur : Cedric) --->
	
	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
			
			<cftrace category="DEBUG" type="information" text="Handling event of TYPE #ARGUMENTS.eventObject.getEventType()# with TARGET #ARGUMENTS.eventObject.getEventTarget()#">
	
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
		
		<cfmail from="error@saaswedo.com" to="monitoring@saaswedo.com" subject="GENERIC ERROR HANDLER - FAILED HANDLING REPORT EVENT" type="html">
			<cfoutput>SERVER : #CGI.SERVER_NAME#</cfoutput><br />
			<cfdump var="#ARGUMENTS#">
		</cfmail>
		
	</cffunction>
	
</cfcomponent>