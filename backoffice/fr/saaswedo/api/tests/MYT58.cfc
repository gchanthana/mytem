<cfcomponent displayname="fr.saaswedo.api.tests.MYT58" hint="Composant contenant toutes les méthodes retournant des données factices pour le module M111">
	<cfset dataSource=(isDefined("SESSION") AND structKeyExists(SESSION,"OFFREDSN")) ? SESSION.OFFREDSN:"ROCOFFRE">

	<!--- [FACTICE : REPLACED] Table de correspondance entre les noms des propriétés provenant du MDM et ceux qui sont dans le FrontOffice (e.g SYSTEM_PLATFORM = plateforme)
	fr.saaswedo.api.myTEM.mdm.MyTEM.getDevicePropertyMap()
	[Demande de requete] R1 - Table de correspondance des libellés des propriétés
	Paramètres : ID_LANGUE (Code langue) - INTEGER (Provient de SESSION.USER.GLOBALIZATION)
	Retour : Une requete contenant la liste des propriétés d'un device avec les colonnes suivantes
	- PROPERTY : Nom technique de la propriété d'un device (e.g SYSTEM_PLATFORM) - VARCHAR
	- LABEL : Libellé de la propriété (e.g plateforme) - VARCHAR
	- CATEGORY : Catégorie de la propriété (e.g Information système) - VARCHAR
	Contraintes : Trier par CATEGORY ASC
	--->
	<cffunction access="public" name="getDevicePropertyMap" returntype="Struct"
	hint="Retourne une structure correspondant à une table de correspondance pour les propriétés d'un Device<br>
	La structure retournée contient autant de clés que de propriétés du Device. Chaque clé est associée à une structure contenant les clés suivantes :
	<br>LABEL : Libellé de la propriété<br>CATEGORY : Libellé de la catégorie de la propriété">
		<cfreturn {
			TOTAL_DISK_SPACE={LABEL="Propriété TOTAL_DISK_SPACE", CATEGORY="Espace de stockage"},
			FREEDISK={LABEL="Propriété FREEDISK", CATEGORY="Espace de stockage"},
			SYSTEM_PLATFORM={LABEL="plateforme", CATEGORY="Information système"},
			SYSTEM_OS_VERSION={LABEL="Propriété SYSTEM_OS_VERSION", CATEGORY="Information système"},
			SYSTEM_OEM={LABEL="Propriété SYSTEM_OEM", CATEGORY="Information système"}
		}>
	</cffunction>

	<!--- [FACTICE - REPLACED] Enregistre les infos qui seront utilisées dans la page d'enrollement :
	fr.consotel.api.mdm.EnrollDevice.execute()
	TODO : Fournir les paramètres SERIALNUMBER et CODEAPP (SESSION.CODEAPPLICATION) à la requete PKG_MDM.enroll_ligne qui se trouve dans MDMService.setEnrollDevice()
	[Demande de requete] R2 - Modifier la requete existante PKG_MDM.enroll_ligne ET Supprimer l'appel à cette méthode de test
	Paramètres (à ajouter à la requete existante) :
	SERIALNUMBER (Serial Number du device) - VARCHAR
	CODEAPP (Code de l'application) - INTEGER
	--->
	<cffunction access="public" name="storeEnrollParameters" returntype="Boolean"
	hint="Enregistre les infos qui seront utilisées dans la page d'enrollement. Retourne TRUE si l'enregistrement a été effectuée avec succès et FALSE sinon<br>
	Les infos sont enregistrées dans une structure au format JSON ayant les mêmes clés que les paramètres de cette méthode<br>
	La structure est enregistrée avec la procédure PKG_M00.setmail_v3 dans le champs correspondant au paramètre p_param_sys">
		<cfargument name="datasource"	type="String" required="true" hint="Datasource dans lequel les infos seront enregistrées">
		<cfargument name="uuid" type="String" required="true" hint="UUID utilisé comme identifiant pour récupérer les infos enregistrées">
		<cfargument name="idPool" type="Numeric" required="true" hint="Identifiant du pool contenant le device (serialNumber,imei)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du device">
		<cfargument name="soustete" type="String" required="true" hint="Numéro de ligne">
		<cfthrow type="Custom" errorcode="MYT58" message="Method storeEnrollParameters() is disabled cause only used for tests">
		<cfset var idUser=-1>
		<cfif (isDefined("SESSION") AND structKeyExists(SESSION,"USER"))>
			<cfset idUser=structKeyExists(SESSION["USER"],"CLIENTACCESSID") ? VAL(SESSION["USER"]["CLIENTACCESSID"]):0>
		</cfif>
		<cfset var enrollParameters=serializeJSON(ARGUMENTS)>
		<cfset var enrollParameters=serializeJSON({
			idPool=ARGUMENTS["idPool"], serialNumber=" " & ARGUMENTS["serialNumber"] & " ", imei=" " & ARGUMENTS["imei"] & " ",soustete=" " & ARGUMENTS["soustete"] & " "
		})>
		<cfstoredproc datasource="#ARGUMENTS.datasource#" procedure="PKG_M00.setmail_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_iduser" value="#idUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_userlogin" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_uuid_mail" value="#ARGUMENTS.uuid#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_module" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_from_mail" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_to_mail" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_subject_mail" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_bcc_mail" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_coprs_mail" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_mail" value="#LSDATEFORMAT(Now(),'YYYY/MM/DD')#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_tag" value="DEV">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_create" value="0">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_code_appli" value="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" variable="p_param_sys" value="#enrollParameters#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" variable="p_param_tech" value="">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour GT 0>
	</cffunction>
	
	<!--- [FACTICE - REPLACED] Récupère et retourne les infos enregistrées avec storeEnrollParameters() et utilisées dans la page d'enrollment :
	Page : detection.cfm
	[Demande de requete] R3 - Modifier la requete existante pkg_mdm.get_infos_uuid_cle ET Supprimer l'appel à cette méthode de test
	TODO : Modifier la page detection.cfm pour récupérer les valeur serialNumber, IMEI, soustete à partir de MDMService.getEnrollDevice() au lieu de cette méthode de test
	Retour : Une requete ramènant les memes colonnes que la requete d'origine et avec les colonnes supplémentaires suivantes :
	SERIALNUMBER : Valeur du paramètre IMEI fourni à la requete PKG_MDM.enroll_ligne - VARCHAR
	IMEI : Valeur du paramètre IMEI fourni à la requete PKG_MDM.enroll_ligne - VARCHAR
	SOUSTETE : Valeur du paramètre SOUSTETE fourni à la requete PKG_MDM.enroll_ligne - VARCHAR
	CODEAPP : Valeur du paramètre CODEAPP fourni à la requete PKG_MDM.enroll_ligne - INTEGER
	--->
	<cffunction access="public" name="requestEnrollParameters" returntype="Struct" hint="Récupère et retourne les infos enregistrées avec storeEnrollParameters()
	<br>Retourne une structure contenant les mêmes clés que les paramètres fournis à storeEnrollParameters()">
		<cfargument name="datasource"	type="String" required="true" hint="Datasource utilisé lors de l'appel à storeEnrollParameters()">
		<cfargument name="uuid" type="String" required="true" hint="UUID utilisé comme identifiant lors de l'enregistrement des infos avec storeEnrollParameters()">
		<cfthrow type="Custom" errorcode="MYT58" message="Method requestEnrollParameters() is disabled cause only used for tests">
		<cfstoredproc datasource="#ARGUMENTS.datasource#" procedure="PKG_M00.getmail">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_uuid_mail" value="#ARGUMENTS.uuid#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfif p_retour.RECORDCOUNT GT 0>
			<cfif isJSON(p_retour["SYS_PARAM"][1])>
				<cfset var cfmxObject=deserializeJSON(p_retour["SYS_PARAM"][1])>
				<!--- Supprimer les espaces en prefixe et suffixe pour chaque valeur de chaque clé --->
				<cfloop item="key" collection="#cfmxObject#">
					<cfset cfmxObject[key]=TRIM(cfmxObject[key])>
				</cfloop>
				<cfreturn cfmxObject>
			<cfelse>
				<cfthrow type="Custom"
					message="La valeur SYS_PARAM retournée par PKG_M00.getmail pour #ARGUMENTS.uuid# n'est pas au format JSON (Datasource : #ARGUMENTS.datasource#)">
			</cfif>
		<cfelse>
			<cfthrow type="Custom"
				message="La procédure PKG_M00.getmail n'a pas trouvé d'enregistrement pour UUID : #ARGUMENTS.uuid# (Datasource : #ARGUMENTS.datasource#)">
		</cfif>
	</cffunction>

	<!--- [FACTICE - REPLACED] Infos du serveur MDM pour un groupe et utilisés pour les procédures d'enrollment des devices :
	fr.consotel.api.mdm.CodeFuction.get_ListeCodeFunction_ServeurMDM()
	[Demande de requete] R4 - Infos du serveur MDM pour une racine
	Paramètres : IDRACINE (Identifiant de la racine) - INTEGER
	Retour : Une requete contenant exactement 1 et 1 seul enregistrement avec les colonnes suivantes
	- MDM : Adresse ou IP du serveur MDM (e.g mdm.saaswedo.com ou 192.0.0.0) - VARCHAR
	- USERNAME : Login MDM utilisé pour accéder au WebService - VARCHAR
	- PASSWORD : Mot de passe MDM du login USERNAME et utilisé pour accéder au WebService - VARCHAR
	- ENROLL_USERNAME : Login MDM utilisé pour l'enrollement - VARCHAR
	- ENROLL_PASSWORD : Mot de passe MDM du login ENROLL_USERNAME et utilisé pour l'enrollement - VARCHAR
	- ADMIN_EMAIL : Email de l'administrateur du serveur MDM - VARCHAR
	- USE_SSL : 0 si le protocole à utiliser pour accéder au serveur MDM est HTTP et 1 pour HTTPS - INTEGER
	Contraintes : Si la racine IDRACINE n'a pas de serveur MDM qui lui est propre alors ce sont les infos du serveur MDM par défaut qui sont retournées par la requete
	--->
	<cffunction access="public" name="getMdmRegistrationInfo" returntype="Query">
		<cfargument name="idRacine" type="numeric" default="0" hint="Identifiant du groupe pour lequel les infos MDM sont retournée">
		<cfset var mdmServerInfos=getMdmServerInfo(ARGUMENTS["idRacine"])>
		<cfquery name="qMdmServerInfos" dbtype="query">
			select	MDM, ENROLL_USERNAME, ENROLL_PASSWORD, ADMIN_EMAIL, USE_SSL
			from	mdmServerInfos
		</cfquery>
		<cfreturn qMdmServerInfos>
	</cffunction>

	<!--- [FACTICE - REPLACED] Infos du serveur MDM pour un groupe et utilisés pour les appels de WebServices :
	fr.saaswedo.api.myTEM.mdm.MyTEM.requestMdmProperties()
	[Demande de requete] R4 - Infos du serveur MDM pour une racine
	--->
	<cffunction access="public" name="getMdmServerInfo" returntype="Query">
		<cfargument name="idRacine" type="numeric" default="0" hint="Identifiant du groupe pour lequel les infos MDM sont retournée">
		<cfset var idGroupe=ARGUMENTS["idRacine"]>
		<!--- TODO : La requete retournée par cette méthode est factice pour le moment et sert de données de test --->
		<cfset var qMdmServiceInfos=queryNew(
			"IDGROUPE,MDM,USERNAME,PASSWORD,ENROLL_USERNAME,ENROLL_PASSWORD,ADMIN_EMAIL,USE_SSL","Integer,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,Integer"
		)>
		<cfset queryAddRow(qMdmServiceInfos,2)>
		<!--- MDM Server 1 (Tous les autres groupes autres que __DEMO-CONSOTEL) --->
		<cfset querySetCell(qMdmServiceInfos, "IDGROUPE",0,1)>
		<cfset querySetCell(qMdmServiceInfos, "MDM","mdm.consotel.fr",1)>
		<cfset querySetCell(qMdmServiceInfos, "USERNAME","axel",1)><!--- Login pour les appels au WebService --->
		<cfset querySetCell(qMdmServiceInfos, "PASSWORD","consotel",1)><!--- Mdp pour les appels au WebService --->
		<cfset querySetCell(qMdmServiceInfos, "ENROLL_USERNAME","demo",1)><!--- Login pour l'action d'enrollment --->
		<cfset querySetCell(qMdmServiceInfos, "ENROLL_PASSWORD","123456",1)><!--- Mdp pour l'action d'enrollment --->
		<cfset querySetCell(qMdmServiceInfos, "ADMIN_EMAIL","cedric.rapiera@saaswedo.com,soufiane.ghenimi@saaswedo.com",1)>
		<cfset querySetCell(qMdmServiceInfos, "USE_SSL",1,1)>
		<!--- MDM Server 2 (Groupe : __DEMO-CONSOTEL) --->
		<cfset querySetCell(qMdmServiceInfos, "IDGROUPE",2458788,2)>
		<cfset querySetCell(qMdmServiceInfos, "MDM","mdm.saaswedo.com",2)>
		<cfset querySetCell(qMdmServiceInfos, "USERNAME","admin",2)>
		<cfset querySetCell(qMdmServiceInfos, "PASSWORD","zenprise",2)>
		<cfset querySetCell(qMdmServiceInfos, "ENROLL_USERNAME","dev",2)>
		<cfset querySetCell(qMdmServiceInfos, "ENROLL_PASSWORD","dev",2)>
		<cfset querySetCell(qMdmServiceInfos, "ADMIN_EMAIL","cedric.rapiera@saaswedo.com,soufiane.ghenimi@saaswedo.com",2)>
		<cfset querySetCell(qMdmServiceInfos, "USE_SSL",1,2)>
		<!--- Requete de test : Ramène les propriétés du serveur MDM pour la racine --->
		<cfquery name="qMdmServerInfos" dbtype="query">
			select	MDM, USERNAME, PASSWORD, ENROLL_USERNAME, ENROLL_PASSWORD, ADMIN_EMAIL, USE_SSL
			from		qMdmServiceInfos
			where	IDGROUPE = <cfqueryparam value="#(idGroupe LT 0 ? idGroupe: idGroupe EQ 2458788 ? 2458788:0)#" cfsqltype="CF_SQL_INTEGER">
		</cfquery>
		<cfreturn qMdmServerInfos>
	</cffunction>

	<!--- [FACTICE - DISABLED] Liste devices du parc de type "MobileParc" pour __DEMO-CONSOTEL :
	fr.consotel.consoview.M111.Recherche.ViewMobileParc()
	[Demande de requete] R5 - Utiliser la requete existante i.e Supprimer l'appel à cette méthode de test
	Mais ne fonctionnera que lorsque les référentiels Saaswedo et Zenprise seront interconnectés
	--->
	<cffunction access="public" name="getMobileParc" returntype="Query">
		<cfargument name="idPool" type="Numeric" required="true">
		<cfset var poolHasDevices=ARGUMENTS.idPool EQ -1 ? 1:0>
		<!--- Requete vide représentant la liste des devices --->
		<cfquery name="allDevicesQuery" datasource="#dataSource#">
			select	'Prénom Nom' as COLLABORATEUR, 'Test Distributeur' as DISTRIBUTEUR, 0 as IDDISTRIBUTEUR, 0 as IDEMPLOYE, 1 as IDETAT_LIGNE, 0 as IDOPERATEUR, 0 as TERM_REBUT, 0 as TERM_SAV,
					/* Fiche ligne par défaut pour tous les devices de ce parc */
					null as ID, null as IDSOUS_TETE,
					0 as IDREVENDEUR_TERMINAL, -1 as IDTERMINAL, 1 as INOUT, 0 as ISLIGNEFXE, 1 as ISLIGNEMOBILE, 'ligne' as LIGNE, 'matricule' as MATRICULE, 'OS' as OS,
					'modèle du device' as MODELE, 0 as NB_ROWS, 0 as IDSIM, 'num sim' as NUM_SIM, 'Opérateur' as OPERATEUR, 0 as REAL_IDOPERATEUR, 0 as ROWN, 0 as SIM_REBUT, 0 as SIM_SAV, 'Terminal' as TERMINAL,
					/* Champs communs des référentiels Saaswedo et Zenprise */
					1 as BOOL_ISMANAGED, 'imei dans MyTEM' as IMEI, 'imei dans Zenprise' as T_ZDM_IMEI, 'SerialNumber dans Zenprise' as T_ZDM_SERIAL_NUMBER,
					/* Champs spécifiques au référentiel Zenprise */
					0 as T_ZDM_DEVICE_ID
			from	DUAL
			where	1=0
		</cfquery>
		<!--- Alimentation du parc du pool de test --->
		<cfif poolHasDevices>
			<!--- Liste des devices Zenprise --->
			<cfset var myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
			<cfset var iDeviceInfo=myTEM.getIDeviceInfo()>
			<cfset var allDevices=iDeviceInfo.getAllDevices()>
			<!--- Ajout des devices de Zenprise dans la requete --->
			<cfif arrayLen(allDevices) GT 0>
				<cfset queryAddRow(allDevicesQuery,arrayLen(allDevices))>
				<cfloop index="i" from="1" to="#arrayLen(allDevices)#">
					<!--- Données spécifiques au référentiel Zenprise --->
					<cfset var isManaged=iDeviceInfo.getManagedStatus(allDevices[i].getSerialNumber(),toString(allDevices[i].getImei())) EQ "MANAGED" ? 1:0>
					<cfset querySetCell(allDevicesQuery,"BOOL_ISMANAGED",isManaged,i)>
					<cfset querySetCell(allDevicesQuery,"IMEI",TRIM(allDevices[i].getImei()) EQ "" ? allDevices[i].getSerialNumber():allDevices[i].getImei(),i)>
					<cfset querySetCell(allDevicesQuery,"T_ZDM_IMEI",TRIM(allDevices[i].getImei()) EQ "" ? allDevices[i].getSerialNumber():allDevices[i].getImei(),i)>
					<cfset querySetCell(allDevicesQuery,"T_ZDM_SERIAL_NUMBER",allDevices[i].getSerialNumber(),i)>
					<!--- Données spécifiques au référentiel Saaswedo mais récupérables depuis le référentiel Zenprise --->
					<cfset var deviceInfo=iDeviceInfo.getDeviceInfo(allDevices[i].getSerialNumber(),toString(allDevices[i].getImei()))>
					<cfset var deviceProperties=deviceInfo["PROPERTIES"]>
					<cfloop index="j" from="1" to="#arrayLen(deviceProperties)#">
						<cfif deviceProperties[j].getName() EQ "TEL_NUMBER"><!--- Une ligne est définie --->
							<cfset querySetCell(allDevicesQuery,"LIGNE",deviceProperties[j].getValue(),i)>
						<cfelseif deviceProperties[j].getName() EQ "SYSTEM_OEM">
							<cfset querySetCell(allDevicesQuery,"MODELE",deviceProperties[j].getValue(),i)>
						<cfelseif deviceProperties[j].getName() EQ "SYSTEM_PLATFORM">
							<cfset querySetCell(allDevicesQuery,"OS",deviceProperties[j].getValue(),i)>
						</cfif>
					</cfloop>
					<!--- Données spécifiques au référentiel Saaswedo --->
					<cfset querySetCell(allDevicesQuery,"IDTERMINAL",(-1 * i),i)>
					<cfset querySetCell(allDevicesQuery,"IDSOUS_TETE",TRIM(allDevicesQuery["LIGNE"][i]) EQ "" ? 0:1,i)>
					<cfset querySetCell(allDevicesQuery,"ID",VAL(allDevicesQuery["IDSOUS_TETE"][i]) GT 0 ? 1:0,i)>
					<cfset querySetCell(allDevicesQuery,"IDSIM",VAL(allDevicesQuery["IDSOUS_TETE"][i]) GT 0 ? 1:0,i)>
					<cfset querySetCell(allDevicesQuery,"NUM_SIM",VAL(allDevicesQuery["IDSOUS_TETE"][i]) GT 0 ? "Numéro de SIM":"",i)>
					<cfset querySetCell(allDevicesQuery,"OPERATEUR",VAL(allDevicesQuery["IDSOUS_TETE"][i]) GT 0 ? "OP SIM/Réseau":"",i)>
					<!--- Permet d'afficher les onglets MDM pour les tablettes et l'action "Gérer le terminal" pour les Smartphones équipées d'une ligne --->
					<cfset querySetCell(allDevicesQuery,"ISLIGNEMOBILE",(
						isManaged ? 1:VAL(allDevicesQuery["IDSOUS_TETE"][i]) GT 0 ? 1:0
					),i)>
					<cfset querySetCell(allDevicesQuery,"NB_ROWS",arrayLen(allDevices),i)>
					<cfset querySetCell(allDevicesQuery,"ROWN",i,i)>
				</cfloop>
			</cfif>
		</cfif>
		<cfreturn allDevicesQuery>
	</cffunction>
	
	<!--- [FACTICE - DISABLED] Fiche Terminal pour __DEMO-CONSOTEL
	fr.consotel.consoview.M111.fiches.FicheTerminal.getInfosFicheTerminal()
	[Demande de requete] R6 - Utiliser la requete existante i.e Supprimer l'appel à cette méthode de test
	--->
	<cffunction access="public" name="getFicheTerminal" returntype="Array">
		<cfargument name="idTerminal" type="Numeric" required="true">
		<!--- 2ème élément du tableau qui est retourné par la méthode d'origine --->
		<cfquery name="dataIndex2" datasource="#dataSource#">
			select	null as IMEI, null as SERIAL_NUMBER,
					null as COMMENTAIRE_EQ, null as DATE_ACHAT, null as DATE_COMMANDE, null as DATE_LIVRAISON, null as IDCATEGORIE_EQUIPEMENT, null as IDCOMMANDE,
					null as IDEMPLACEMENT, null as IDFABRIQUANT, null as IDFOURNISSEUR, null as IDSITE_PHYSIQUE, null as IDTERMINAL, null as IDTYPE_EQUIPEMENT,
					null as LIBELLE_CATEGORIE, null as LIBELLE_REVENDEUR, null as MARQUE, null as MODELE, null as MONTANT, null as NIVEAU_TERM, null as REF_COMMANDE_TERMINAL,
					null as REF_LIVRAISON, null as TERMINAL, null as TYPE_EQUIPEMENT
			from	DUAL
			where	1=1
		</cfquery>
		<!--- 3ème élément du tableau qui est retourné par la méthode d'origine --->
		<cfquery name="dataIndex3" datasource="#dataSource#">
			select	null as ID, null as IDREVENDEUR, null as IDSOUS_TETE, null as LIBELLE_TYPE_LIGNE, null as NUM_SIM,
					null as OPERATEUR, null as OPERATEURID, null as REVENDEUR_SIM, null as SOUS_TETE
			from	DUAL
			where	1=0
		</cfquery>
		<!--- 4ème élément du tableau qui est retourné par la méthode d'origine --->
		<cfquery name="dataIndex4" datasource="#dataSource#">
			select	null as COMMENTAIRES, null as DATE_CREATE, null as DATE_DEBUT, null as DATE_FIN, null as DESCRIPTION, null as IDRACINE, null as ID_SAV,
					null as IS_SAV, null as NB_INTERVENTIONS, null as NUMERO_TICKET, null as REFERENCE_SAV, null as SOLUTION_SAV, null as TOTAL_COUT_INTERVENTION,
					null as USER_CLOSE, null as USER_CREATE, null as USER_MODIF
			from	DUAL
			where	1=0
		</cfquery>
		<!--- 5ème élément du tableau qui est retourné par la méthode d'origine --->
		<cfquery name="dataIndex5" datasource="#dataSource#">
			select	null as COMMENTAIRE_CONTRAT, null as DATE_CREATE, null as DATE_DEBUT, null as DATE_ECHEANCE, null as DATE_ELLIGIBILITE, null as DATE_MODIF,
					null as DATE_RENOUVELLEMENT, null as DATE_RESI, null as DATE_RESILIATION, null as DATE_SIGNATURE, null as DUREE_CONTRAT, null as DUREE_ELLIGIBILITE,
					null as IDCONTRAT, null as IDFOURNISSEUR, null as IDRACINE, null as IDTYPE_CONTRAT, null as IDUSER_CREATE, null as IDUSER_MODIF, null as IDUSER_RESI,
					null as LOYER, null as MONTANT_CONTRAT, null as NOM_FOURNISSEUR, null as NOM_USER_CREATE, null as NOM_USER_MODIF, null as NOM_USER_RESI, null as PREAVIS,
					null as REFERENCE_CONTRAT, null as TACITE_RECONDUCTION, null as TARIF, null as TARIF_MENSUEL, null as TYPE_CONTRAT
			from	DUAL
			where	1=0
		</cfquery>
		<!--- 6ème élément du tableau qui est retourné par la méthode d'origine --->
		<cfquery name="dataIndex6" datasource="#dataSource#">
			select	null as CATEGORIE_EQUIPEMENT, null as DISTRIBUTEUR, null as ID, null as IDCATEGORIE_EQUIPEMENT, null as IDDISTRIBUTEUR, null as IDEQP, null as IDSIM,
					null as IDSOUS_TETE, null as IDTYPE_EQUIPEMENT, null as LIGNE, null as MARQUE, null as MODELE, null as NO_SERIE_ENFANT, null as S_NO_SERIE, null as TYPE_EQUIPEMENT
			from	DUAL
			where	1=0
		</cfquery>
		<cfreturn [1,dataIndex2,dataIndex3,dataIndex4,dataIndex5,dataIndex6]>
	</cffunction>
	
	<!--- [FACTICE - DISABLED] Liste des pools pour __DEMO-CONSOTEL
	fr.consotel.consoview.M111.UtilisateurService.getListePool()
	[Demande de requete] R7 - Utiliser la requete existante i.e Supprimer l'appel à cette méthode de test
	--->
	<cffunction access="public" name="getListePool" returntype="Query">
		<cfquery name="listePoolFactice" datasource="#dataSource#">
			select	-1 as IDPOOL, 0 as ACCES_COLLABORATEUR, '' as CODE_INTERNE_POOL, 'Pool factice de test avec __DEMO-CONSOTEL' as COMMENTAIRE_POOL, 0 as GEST_BUDGET, null as GEST_CATALOGUE_CLIENT,
							null as GEST_CATALOGUE_FOURNIS, 0 as IDREVENDEUR, 'Pool de test __DEMO-CONSOTEL' as LIBELLE_POOL, 'Demo' as LIBELLE_PROFIL, 1 as IDPROFIL
			from	DUAL
			UNION
			select	-2 as IDPOOL, 0 as ACCES_COLLABORATEUR, '' as CODE_INTERNE_POOL, 'Autre Pool factice de test avec __DEMO-CONSOTEL' as COMMENTAIRE_POOL, 0 as GEST_BUDGET, null as GEST_CATALOGUE_CLIENT,
							null as GEST_CATALOGUE_FOURNIS, 0 as IDREVENDEUR, 'Autre Pool de test __DEMO-CONSOTEL' as LIBELLE_POOL, 'Autre Demo' as LIBELLE_PROFIL, 0 as IDPROFIL
			from	DUAL
			order by	IDPOOL desc	
		</cfquery>
		<cfreturn listePoolFactice>
	</cffunction>
	
	<!--- [FACTICE - DISABLED] Liste des données d'initialisation Gestion de Parc pour __DEMO-CONSOTEL (e.g Droits Utilisateur, Droits Commande, Liste mois, etc...)
	fr.consotel.consoview.M111.UtilisateurService.threadInitData()
	[Demande de requete] R8 - Utiliser la requete existante i.e Supprimer l'appel à cette méthode de test
	--->
	<cffunction access="public" name="getInitData" returntype="Struct">
		<cfargument name="idPool" type="Numeric" required="true">
		<cfset var droitMobile=ARGUMENTS.idPool EQ -1 ? 1:0>
		<cfquery name="droitsUser" datasource="#dataSource#">
			select	<cfqueryparam value="#droitMobile#"> as DROIT_MOBILE, <cfqueryparam value="#droitMobile#"> as DROIT_VUE_MOBILE,
					0 as DROIT_COMPTE, 0 as DROIT_FIXE, 0 as DROIT_VUE_ALERTE, 0 as DROIT_VUE_CONSO, 0 as DROIT_VUE_FIXE
			from	DUAL
		</cfquery>
		<cfquery name="droitsCommande" datasource="#dataSource#">
			select	'Code Action' as CODE_ACTION, 0 as IDINV_ACTIONS, 'Libellé Action' as LIBELLE_ACTION, 'Libellé Action Client' as LIBELLE_ACTION_CLIENT,
					1 as ORDRE_AFFICHAGE, 'm' as SEGMENT, 1 as SELECTED
			from	DUAL
			where	1=0
		</cfquery>
		<cfquery name="listeMois" datasource="#dataSource#">
			select	0 as IDPERIODE_MOIS, 'Libellé Période' as LIBELLE_PERIODE
			from	DUAL
			where	1=0
		</cfquery>
		<cfquery name="listeOrga" datasource="#dataSource#">
			select	0 as IDGROUPE_CLIENT, 'Libellé Groupe Client' as LIBELLE_GROUPE_CLIENT
			from	DUAL
			where	1=0
		</cfquery>
		<cfquery name="listeSite" datasource="#dataSource#">
			select	'Adresse' as ADRESSE, 0 as IDSITE_PHYSIQUE, 0 as IS_FACTURATION, 0 as IS_LIVRAISON, 'Nom du Site' as NOM_SITE, 0 as PAYCONSOTELID, 'Pays' as PAYS,
					'SP Code Interne' as SP_CODE_INTERNE, 'Code Postal' as SP_CODE_POSTAL, 'Commentaire' as SP_COMMENTAIRE, 'Commune' as SP_COMMUNE, 'Référence' as SP_REFERENCE
			from	DUAL
			where	1=0
		</cfquery>
		<cfquery name="listeOperateur" datasource="#dataSource#">
			select	'Opérateur' as NOM, 0 as OPERATEURID
			from	DUAL
			where	1=0
		</cfquery>
		<cfquery name="listeRevendeur" datasource="#dataSource#">
			select	'Adresse 1' as ADRESSE1, 'Adresse 2' as ADRESSE2, 'Code Interne' as CODE_INTERNE, 'Commentaire' as COMMENTAIRE, 'Commune' as COMMUNE, 0 as CONTACTS_PARTAGES, 0 as CONTACTS_PRIVES,
					'Date Create' as DATE_CREATE, 'Date Modif' as DATE_MODIF, 'Délai minimum livraison' as DELAI_MIN_LIVRAISON, null as FAX, null as FPC, 0 as IDCDE_CONTACT_SOCIETE, 0 as IDRACINE,
					0 as IDREVENDEUR, 0 as ISFIXE, 0 as ISMOBILE, null as LIBELLE, 0 as NB_AGENCE, null as PAYS, 0 as PAYSCONSOTELID, null as SIREN, null as TELEPHONE, null as TYPE, null as ZIPCODE
			from	DUAL
			where	1=0
		</cfquery>
		<cfreturn {
			DROITGP=droitsUser, LISTEDROITCOMMANDE=droitsCommande, LISTEMOIS=listeMois, LISTEORGA=listeOrga, LISTESITE=listeSite, OPERATEUR=listeOperateur, REVENDEUR=listeRevendeur
		}>
	</cffunction>
</cfcomponent>