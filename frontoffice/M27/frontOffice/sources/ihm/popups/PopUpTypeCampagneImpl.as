package ihm.popups
{
	import composants.util.ConsoviewAlert;
	
	import entities.DataInfos;
	
	import events.CampagneEvent;
	import events.PopUpEvent;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	
	import services.CampagneService;
	
	public class PopUpTypeCampagneImpl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var cbxTypeDeploiement			:ComboBox;
		
		public var btnValiderType						:Button;

		
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//

		private var _popUpImportLigne					:PopUpImportLignesIhm;
		
		private var _popUpSelectOrganiation				:PopUpSelectOrganiationIhm;
		
		private var _popUpLibelleCampagne				:PopUpLibelleCampagneIhm;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _campagneSrv						:CampagneService = new CampagneService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var typeDeploiement			:ArrayCollection = new ArrayCollection([
																									{libelle:"Toutes les lignes mobiles", id:0},
																									{libelle:"Importer un fichier de lignes", id:1},
																									{libelle:"Utiliser le périmètre suivant", id:2}
																								]);
		
		[Bindable]public var currentDataInfos			:DataInfos = new DataInfos();

		
		[Bindable]public var isEnabled					:Boolean = false;
		
		[Bindable]public var nbrLignes					:String = '0';
				
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpTypeCampagneImpl()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(ce:CloseEvent):void
		{			
			PopUpManager.removePopUp(this);
		} 
		
		protected function cbxTypeDeploiementChangeHandler(le:ListEvent):void
		{
			currentDataInfos = new DataInfos();
		
			isEnabled = false;
			
			if(cbxTypeDeploiement.selectedItem != null)
			{
				if(cbxTypeDeploiement.selectedItem.id == 0)
				{
					setCurrentPerimetreInfos();
					
					btnValiderType.enabled = false;
				}
				else
					btnValiderType.enabled = true;
			}
			else
				btnValiderType.enabled = false;
			
			nbrLignes = '0';
		}
		
		protected function btnValiderTypeClickHandler(me:MouseEvent):void
		{
			if(cbxTypeDeploiement.selectedItem != null)
			{
				if(cbxTypeDeploiement.selectedItem.id == 1)
					popUpImportLigne();
				else if(cbxTypeDeploiement.selectedItem.id == 2)
						popUpSelectOrganiation();
			}
		}
		
		protected function btnSauvegarderClickHandler(me:MouseEvent):void
		{
			if(currentDataInfos != null)
			{
				currentDataInfos.IDDEPLOY = 0;
				
				popUpLibelleCampagne();
			}
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(currentDataInfos != null)
			{
				currentDataInfos.IDDEPLOY = 1;
				
				popUpLibelleCampagne();
			}
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		private function setCurrentPerimetreInfos():void
		{			
			currentDataInfos.IDPERIMETRE 	= CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			currentDataInfos.PERIMETRE 		= CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			
			isEnabled = true;
			
			btnValiderType.enabled = false;
			
			getNombreLignes();
		}
			
		private function setNombreLigneToLabel(nbrTotalLignes:String):void
		{
			nbrLignes = nbrTotalLignes;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS SERVICE
			//--------------------------------------------------------------------------------------------//
		
		private function getNombreLignes():void
		{
			_campagneSrv.getNombreLignes(currentDataInfos);
			_campagneSrv.addEventListener(CampagneEvent.NBRLIGNECAMPAGNE, nombreLignesHandler);
		}
		
		private function saveCampagne():void
		{
			_campagneSrv.saveCampagne(currentDataInfos);
			_campagneSrv.addEventListener(CampagneEvent.CAMPAGNESAVED, saveCampagneHandler);
		}
		
		private function sendCampagne():void
		{
			_campagneSrv.sendCampagne(currentDataInfos);
			_campagneSrv.addEventListener(CampagneEvent.CAMPAGNESENT, sendCampagneHandler);
		}
		
		private function nombreLignesHandler(ce:CampagneEvent):void
		{
			setNombreLigneToLabel(_campagneSrv.currentNbrLignes.toString());
		}
		
		private function saveCampagneHandler(ce:CampagneEvent):void
		{
			PopUpManager.removePopUp(this);
			
			ConsoviewAlert.afficherOKImage("Campagne sauvegarder", this);
		}
		
		private function sendCampagneHandler(ce:CampagneEvent):void
		{
			PopUpManager.removePopUp(this);
			
			ConsoviewAlert.afficherOKImage("Campagne envoyer", this);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					GESTIONS DES POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private function popUpImportLigne():void
		{
			_popUpImportLigne = new PopUpImportLignesIhm();
			_popUpImportLigne.setInfos(currentDataInfos);
			_popUpImportLigne.addEventListener(PopUpEvent.POPUP_VALIDATE, popUpImportLigneValidateHandler);
			
			PopUpManager.addPopUp(_popUpImportLigne, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpImportLigne);
		}
		
		private function popUpSelectOrganiation():void
		{
			_popUpSelectOrganiation = new PopUpSelectOrganiationIhm();
			_popUpSelectOrganiation.setInfos(currentDataInfos);
			_popUpSelectOrganiation.addEventListener(PopUpEvent.POPUP_VALIDATE, popUpSelectOrganiationValidatHandler);
			
			PopUpManager.addPopUp(_popUpSelectOrganiation, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpSelectOrganiation);
		}
		
		private function popUpLibelleCampagne():void
		{
			_popUpLibelleCampagne = new PopUpLibelleCampagneIhm();
			_popUpLibelleCampagne.setInfos(currentDataInfos);;
			_popUpLibelleCampagne.addEventListener(PopUpEvent.POPUP_VALIDATE, popUpLibelleCampagneValidateHandler);
			
			PopUpManager.addPopUp(_popUpLibelleCampagne, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpLibelleCampagne);
		}

			//--------------------------------------------------------------------------------------------//
			//					GESTIONS DES POPUPS EVENT
			//--------------------------------------------------------------------------------------------//
		
		private function popUpImportLigneValidateHandler(pue:PopUpEvent):void
		{
			if(pue.type == PopUpEvent.POPUP_VALIDATE)
			{
				currentDataInfos = _popUpImportLigne.getInfos();
				
				isEnabled = true;
				
				setNombreLigneToLabel(currentDataInfos.DATASELECTED.length);
			}
			
			PopUpManager.removePopUp(_popUpImportLigne);
		}
		
		private function popUpSelectOrganiationValidatHandler(pue:PopUpEvent):void
		{
			if(pue.type == PopUpEvent.POPUP_VALIDATE)
			{
				currentDataInfos = _popUpSelectOrganiation.getInfos();
				
				isEnabled = true;

				getNombreLignes();
			}
			
			PopUpManager.removePopUp(_popUpSelectOrganiation);
		}
		
		private function popUpLibelleCampagneValidateHandler(pue:PopUpEvent):void
		{
			if(pue.type == PopUpEvent.POPUP_VALIDATE)
			{
				currentDataInfos = _popUpLibelleCampagne.getInfos();
				
				if(currentDataInfos.IDDEPLOY > 0)
					sendCampagne();
				else
					saveCampagne();
			}
			
			PopUpManager.removePopUp(_popUpLibelleCampagne);
			
			dispatchEvent(new PopUpEvent(PopUpEvent.POPUP_VALIDATE));
		}
	}
}