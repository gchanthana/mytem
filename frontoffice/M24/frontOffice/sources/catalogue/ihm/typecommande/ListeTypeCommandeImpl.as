package catalogue.ihm.typecommande
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import catalogue.event.CatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	
	[Bindable]
	public class ListeTypeCommandeImpl extends TitleWindow
	{
		public var globalService : GlobalCatalogueService = new GlobalCatalogueService();
		public var listeIDEquipement : String ;
		public var listeEquipement : ArrayCollection ;
		public var groupeIndex : int;
		public var idClient : int;
		
		//COMPONANT
		public var dg: DataGrid;
		public var cbAll : CheckBox;		
		
		public function ListeTypeCommandeImpl()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			initData();
		}
		public function initData():void 
		{
			globalService.getTypeCommande(listeIDEquipement,idClient);
			dg.addEventListener(ListEvent.ITEM_CLICK,onItemChanged);
		}
		public function fermer():void
		{
			PopUpManager.removePopUp(this);
		}
		public function onItemChanged(evt : Event):void
		{
			var item : Object = (evt.target as DataGrid).selectedItem;
				
			if(item.ETAT != 1) 
			{
				item.ETAT = 1;
				
			}
			else
			{
				item.ETAT = 0;
			}
			
			var boolExiste : Boolean = false;
			for(var i:int=0;i<globalService.listeTypeCommandeSelected.length;i++)
			{
				if(globalService.listeTypeCommandeSelected[i]==item.idTypeCmd)
				{
					globalService.listeTypeCommandeSelected.removeItemAt(i);
					globalService.updateTypeCmdEquipement(listeIDEquipement,Number([item.idTypeCmd]),0);
					boolExiste = true;
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				globalService.listeTypeCommandeSelected.addItem(item.idTypeCmd);
				globalService.updateTypeCmdEquipement(listeIDEquipement,Number([item.idTypeCmd]),1);
			}
			globalService.listeTypeCommandeSelected.refresh();
		}
		
		protected function txtFilterChangeHandler(evt:Event):void
		{
			if(evt.currentTarget.text == null) return;
			globalService.listeTypeCommande.filterFunction = function filtreGrid(item:Object):Boolean
			{	
				if (
					((item.libelleTypeCmd != null) && item.libelleTypeCmd.toString().toLowerCase().search(evt.currentTarget.text.toLowerCase()) !=-1)
					||
					((item.libelleTypeCmd != null) && item.libelleTypeCmd.toString().toLowerCase().search(evt.currentTarget.text.toLowerCase()) !=-1)
					||
					((item.commentaire != null) && item.commentaire.toString().toLowerCase().search(evt.currentTarget.text.toLowerCase()) !=-1)
					)
				{
					return true;
				}
				else
				{
					return false;
				}
			};
			globalService.listeTypeCommande.refresh();
		}
		
		private function update_type_commande_handler(evt : CatalogueEvent):void
		{
			//dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,true));	
		}
		public function add():void
		{
			
		}
		
		
	}
}