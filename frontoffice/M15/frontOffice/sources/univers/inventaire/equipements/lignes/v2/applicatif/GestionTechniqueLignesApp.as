package univers.inventaire.equipements.lignes.v2.applicatif
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.utils.describeType;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.lignes.v2.util.viewsHelpers;
	import univers.inventaire.equipements.lignes.v2.vo.Ligne;
	import univers.inventaire.equipements.lignes.v2.vo.OngletEtatVo;
	import univers.inventaire.equipements.lignes.v2.vo.OngletGeneralVo;
	
	 
//----- Events -----------------------------------
	
	
	[Event(name="searchLignesComplete")]
	
	
	
	
	[Event(name="ListeTetesLigneUpdated")]
	[Event(name="getInfosLigneComplete")]
	[Event(name="UpdateInfosLigneComplete")]
	
	
	
	
	[Event(name="getEtatLigneComplete")]
	[Event(name="updateEtatLigneComplete")]
	
	
	[Event(name="teteUpdated")]
	
	
	
	
//----------------------------------------------	
	
	
	
	[Bindable]
	public class GestionTechniqueLignesApp extends EventDispatcher
	{
		//const 
		public static const LIGNE_MOBILE : Number = 707;
		
		public static const TYPE_FICHE_MOBILE : String = "MOB";
		
		
		
		//---------- Eevent const type ------------------------
		public static const SEARCHLIGNES_COMPLETE : String = "searchLignesComplete";
		public static const TETE_UPDATED : String = "teteUpdated";
		public static const LISTETETESLIGNE_COMPLETE : String =  "ListeTetesLigneUpdated";
		public static const GETINFOSLIGNE_COMPLETE : String = "getInfosLigneComplete";
		public static const UPDATEINFOSLIGNE_COMPLETE : String = "UpdateInfosLigneComplete";
		
		
		
		
		public static const GETETATLIGNE_COMPLETE : String = "getEtatLigneComplete";
		public static const UPDATEETATLIGNE_COMPLETE : String = "updateEtatLigneComplete";
		//----------Fin Eevent const type -----------------------		
		
		//Données pour les combos et autres
		private var _viewHelper : viewsHelpers;
		public function get viewHelper():viewsHelpers{
			return _viewHelper;
		}
		
		
		
		public function set viewHelper(vh : viewsHelpers):void{
			_viewHelper = vh;
			_viewHelper.getListeTypesLigne();
			_viewHelper.getListeTypeRaccordement();
			_viewHelper.getListeUsages();
			_libellesPersos.getLibellesPersos();
		}
			
		
			
		
		
		//Gestion des libellés persos
		private var _libellesPersos : LibellesPersos = new LibellesPersos();
		public function get libellesPersos():LibellesPersos{
			return _libellesPersos;
		}
		public function set libellesPersos(lp : LibellesPersos):void{
			_libellesPersos = lp;
		}
		
		
		
				
		
		
		
		
		
		
		
		
		
		//Le mode ecriture ou consultation
		public function get modeEcriture():Boolean{
			return true;
		}
		
		
		
		
		
			
		
		
		
		
		
		
		
		
		
		
		
		
		///Constructeur	
		public function GestionTechniqueLignesApp(target:IEventDispatcher=null)
		{
			//TODO: implement function
			super(target);
			viewHelper = new viewsHelpers();
		}
		
		
		
		
		
		
		//*===================== LISTE DES LIGNES =============================*//
		private var _listeLignes : ArrayCollection;
		public function get listeLignes():ArrayCollection{
			return _listeLignes;
		}		
		public function set listeLignes(liste : ArrayCollection):void{
			if (_listeLignes != null) _listeLignes = null;
			_listeLignes = new ArrayCollection();
			
			for (var item : Object in liste){
				var ligne : Ligne = new Ligne();
				doMapping(liste[item],ligne);
				_listeLignes.addItem(ligne); 
					
			}
		 
		}
		
		//Pour l'export 
		private var _tmpIdGroupeClient : Number = 0;
		private var _tmpChaine : String = "";
				
		public function getListeLignes(idGroupe_client : Number,chaine : String):void{
			_tmpIdGroupeClient = idGroupe_client;
			_tmpChaine = chaine;			
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"searchLignes",
																				getListeLignesResultHandler);
			RemoteObjectUtil.callService(op,idGroupe_client,chaine)
		}		
		
		
		private function getListeLignesResultHandler(re :ResultEvent):void{
			if(re.result){
				dispatchEvent(new Event(SEARCHLIGNES_COMPLETE));
				listeLignes = re.result as ArrayCollection;
				
			}else{
				listeLignes = null;
			}
		}		
		
		
		public function exporterListeLignes():void{
			 var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																"exporterListeLignes",
																exporterListeLignesResultHandler);
			RemoteObjectUtil.callService(opData,_tmpIdGroupeClient,_tmpChaine);
		}
		
		private function exporterListeLignesResultHandler(re : ResultEvent):void{
			if (String(re.result) != "error"){
				displayExport(String(re.result));				
			}else{
				Alert.show("Erreur export");				
			}
		}
		
		private function displayExport(name : String):void {			 
			var url:String = cv.NonSecureUrlBackoffice + "/fr/consotel/consoview/inventaire/equipement/lignes/csv/exportCSV.cfm";
            var request:URLRequest = new URLRequest(url);         
           	var variables:URLVariables = new URLVariables();
            variables.FILE_NAME = name;
            request.data = variables;
            request.method = URLRequestMethod.POST;
  			navigateToURL(request,"_blank");
         } 
		//*===================== FIN LISTE LIGNES =============================*//
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//*==================== ONGLET ========================================*//
		private var _ligne : Ligne;
		public function get ligne():Ligne{
			return _ligne;
		}		
		public function set ligne(lg : Ligne):void{			
			_ligne = lg;
			
			
			
			//Configuration de la fiche affectation
			if (gestionAffectationStrategy != null) gestionAffectationStrategy = null;
			switch(_ligne.TYPE_FICHELIGNE){
				case TYPE_FICHE_MOBILE : {
					gestionAffectationStrategy = new GestionAffectationMobile(_ligne);
					break;
				}
				default:{
					gestionAffectationStrategy = new GestionAffectationDefaut(_ligne);
					break;
				}
			}
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//*===================== ONGLET LIGNE ==================================*//
		private var _listeTetesLigne : ArrayCollection;
		public function get listeTetesLigne():ArrayCollection{
			return _listeTetesLigne;
		}
		public function set listeTetesLigne(data : ArrayCollection):void{
			_listeTetesLigne = data;
		}
		
		private var _listeLigneGroupement : ArrayCollection;
		public function get listeLigneGroupement():ArrayCollection{
			return _listeLigneGroupement;
		}
		public function set listeLigneGroupement(data : ArrayCollection):void{
			_listeLigneGroupement = data;
		}
		
		
		private var _dataGenerale : OngletGeneralVo;
		public function get dataGenerale():OngletGeneralVo{
			return _dataGenerale;
		}
		public function set dataGenerale(data : OngletGeneralVo):void{
			_dataGenerale = data;
		}
		
		
		
		//Liste des tete de lignes
		private function getListeTetesLigne(idSousTete : Number):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getTeteLigne",
																				getListeTetesLigneResultHandler);
			RemoteObjectUtil.callService(op,idSousTete);			
		}
		
		public function getListeTetesLigneResultHandler(re : ResultEvent):void{			
			if(re.result){
				listeTetesLigne= re.result as ArrayCollection;	
				listeTetesLigne.refresh();
				//listeTetesLigne.addItemAt({TETE:"----------"},0);			
			}else{
				listeTetesLigne = null;
			}
			dispatchEvent(new Event(LISTETETESLIGNE_COMPLETE));
			getDataOngletGenerale();
		}
		
		public function getInfosLigne():void{
			getListeTetesLigne(ligne.IDSOUS_TETE);		
			getListeLignesGroupement(ligne.IDSOUS_TETE);
			getListeTetesLigne(ligne.IDSOUS_TETE);	
		}
		
		private function getDataOngletGenerale():void{
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"getInfosLigne",
									getInfosLigneResultHandler);
									
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		
		private function getInfosLigneResultHandler(re :ResultEvent):void{
			if(re.result && (re.result as ArrayCollection).length > 0){
				var data : Object = re.result[0];
				dataGenerale = new OngletGeneralVo();
				doMapping(data,dataGenerale);
				dispatchEvent(new Event(GETINFOSLIGNE_COMPLETE));
			}else{
				dataGenerale = null;
			}
		}
		
		//Liste des lignes du groupement
		public function getListeLignesGroupement(idSousTete:Number):void{
			var op :AbstractOperation = 
						RemoteObjectUtil.
							getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
									"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
									"getLignesGroupement",
									getListeLignesGroupementResultHandler);							
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		}
		
		public function getListeLignesGroupementResultHandler(re : ResultEvent):void{			
			if(re.result){
				listeLigneGroupement = re.result as ArrayCollection;	
			}else{
				listeLigneGroupement = new ArrayCollection();				
			}
			dispatchEvent(new Event(LISTETETESLIGNE_COMPLETE));
		}
		
		public function updateInfosLigne(data : OngletGeneralVo):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateInfosLigne",
																				updateInfosLigneResultHandler);
			RemoteObjectUtil.callService(op,data);
		}
		
		private function updateInfosLigneResultHandler(re : ResultEvent):void{
			if (re.result > 0){				
				Alert.okLabel = "Fermer";
				Alert.show("Champs mis à jour","INFO",Alert.OK);
				doMapping((re.token.message.body[0] as OngletGeneralVo),_dataGenerale);
				
				_ligne.LIBELLE_TYPE_LIGNE = (re.token.message.body[0] as OngletGeneralVo).LIBELLE_TYPE_LIGNE;
				_ligne.IDTYPE_LIGNE = (re.token.message.body[0] as OngletGeneralVo).IDTYPE_LIGNE;
				_ligne.TETE_LIGNE = (re.token.message.body[0] as OngletGeneralVo).TETE_LIGNE;
				_ligne.IDTETE_LIGNE = (re.token.message.body[0] as OngletGeneralVo).IDTETE_LIGNE;
				_ligne.IDTYPE_RACCORDEMENT = (re.token.message.body[0] as OngletGeneralVo).IDTYPE_RACCORDEMENT;
				_ligne.TYPE_RACCORDEMENT = (re.token.message.body[0] as OngletGeneralVo).LIBELLE_RACCORDEMENT;
				_ligne.USAGE = (re.token.message.body[0] as OngletGeneralVo).USAGE;
				viewHelper.getListeUsages();
				dispatchEvent(new Event(UPDATEINFOSLIGNE_COMPLETE));
				
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur Remoting");
			}
		}
		
		
		private var sousTete : Ligne;
		private var teteLigne : 	Ligne;
		public function setTeteLigne(sous_tete : Ligne,tete : Ligne):void{
			
			sousTete = sous_tete;
			teteLigne = tete;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"setTeteLigne",
																				setTeteLigneResultHandler);
			RemoteObjectUtil.callService(op,sousTete.IDSOUS_TETE, teteLigne.IDSOUS_TETE);
		}
		
		public function setTeteLigneResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				var index : Number = ConsoviewUtil.getIndexById(listeLignes,"IDSOUS_TETE",sousTete.IDSOUS_TETE); 
				if (index > 0){
					var ligneToUpdate : Ligne = Ligne(listeLignes.getItemAt(index));
					ligneToUpdate.IDTETE_LIGNE = teteLigne.IDSOUS_TETE;
					ligneToUpdate.TETE_LIGNE = teteLigne.SOUS_TETE;
					listeLignes.itemUpdated(ligneToUpdate);
				}
			}
			getInfosLigne()
		}
		//*===================== FIN ONGLET LIGNE ==============================*//
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				
		
		
		
		
		
		
		//*==================== ONGLET ETAT ====================================*//
		private var _dataEtat : OngletEtatVo;
		public function get dataEtat():OngletEtatVo{
			return _dataEtat;
		}
		public function set dataEtat(data : OngletEtatVo):void{
			_dataEtat = data;
		}
		
		
		
		
		
		
		
		public function getEtatLigne():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getEtatLigne",
																				getEtatLigneResultHandler);
			RemoteObjectUtil.callService(op,ligne.IDSOUS_TETE);
		
		
		
			
		}
		private function getEtatLigneResultHandler(re :ResultEvent):void{
			if(re.result && (re.result as ArrayCollection).length > 0){
				var data : Object = re.result[0];
				dataEtat = new OngletEtatVo();
				doMapping(data,dataEtat);
				dispatchEvent(new Event(GETETATLIGNE_COMPLETE));
			}else{
				dataEtat = null;
			}
		}
		
		public function updateEtatLigne(data : OngletEtatVo):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateEtatLigne",
																				updateEtatLigneResultHandler);
			RemoteObjectUtil.callService(op,data);
		}
		
		private function updateEtatLigneResultHandler(re : ResultEvent):void{
			if (re.result > 0){				
				Alert.okLabel = "Fermer";
				Alert.show("Champs mis à jour","INFO",Alert.OK);
				
				doMapping((re.token.message.body[0] as OngletEtatVo),_dataEtat);
				
				_ligne.ETAT = (re.token.message.body[0] as OngletEtatVo).ETAT;
				_ligne.DATE_COMMANDE = (re.token.message.body[0] as OngletEtatVo).DATE_MISE_EN_SERV;
				_ligne.DATE_RESILIATION = (re.token.message.body[0] as OngletEtatVo).DATE_RESILIATION;
				_ligne.DATE_SUSPENSION = (re.token.message.body[0] as OngletEtatVo).DATE_SUSPENSION;
				dispatchEvent(new Event(UPDATEETATLIGNE_COMPLETE));
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur Remoting");
			}
		}
		
		//*==================== FIN ONGLET ETAT ================================*//
		
		
		
		
		
		
		
		
		
		
		
		//*==================== ONGLET AFFECTATION ===================================*//		
		private var _gestionAffectationStrategy : GestionAffectationStrategy;
		public function get gestionAffectationStrategy():GestionAffectationStrategy{
			return _gestionAffectationStrategy;
		}
		public function set gestionAffectationStrategy(gestionAffectation : GestionAffectationStrategy):void{
			_gestionAffectationStrategy = gestionAffectation;
		}		
		//*==================== FIN ONGLET AFFECTATION ===============================*//
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		private function doMapping(src : Object, dest : Object):void{
			var classInfo : XML = describeType(dest);
			
			
            for each (var v:XML in classInfo..accessor) {
               if (src.hasOwnProperty(v.@name) && src[v.@name] != null){
               		dest[v.@name] = src[v.@name];
               }else{
               	trace("-----> echec mapping ["+v.@name+"] ou valeur null" );
               }
            }            				 		
		}
	}
}