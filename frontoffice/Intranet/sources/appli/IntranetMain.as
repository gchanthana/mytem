package appli {
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.net.sendToURL;
	
	import intranet.EspacePerso.LienPerso;
	import intranet.EspacePerso.TelIP;
	import intranet.GestionClient;
	import intranet.GestionCtrl;
	import intranet.GestionDesLogin.GestionDesLoginIHM;
	import intranet.GestionDroits.GestionDroitIHM;
	import intranet.GestionProduit.GestionProduitIHM;
	import intranet.M25.ihm.GestionCollecteIHM;
	import intranet.M25.ihm.TemplateCollecteListeIHM;
	import intranet.Main;
	import intranet.cataloguepublic.vues.GrilleTarifaireIHM;
	import intranet.cliche.GestionCliche;
	import intranet.gestionGroupe.GestionImport;
	import intranet.gestionLoginClasse.GestionLogin;
	
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	
	public class IntranetMain extends IntranetMainIHM {
		
		/*-------------------------*/
		[Embed(source="../assets/images/suivant.gif")]
            public var icon_yahoo:Class;
              [Embed(source="../assets/images/house.png")]
            public var icon_house:Class;
               [Embed(source="../assets/images/element_stop.png")]
            public var icon_quit:Class;
  			[Embed(source="../assets/images/windows.png")]
            public var windows:Class;
            
           [Bindable]
           public var menuBarCollection:XMLListCollection;
    
            private var menubarXML:XMLList =
                <>
                  	<menuitem label="Intranet" data="Top"icon="icon_house">
                  		<menuitem label="Home" data="1A" funct="displayHome"/>
                    </menuitem>
                   <menuitem label="En production" data="top"icon="icon_yahoo" toggled="true">
                   		<menuitem label="ConsoView v3" data="1A" id="btnCv3_Prod" funct="ouvrirUrl"/>
                        <menuitem label="Mobile Manager Client v1" data="1A" id="btnMmc_Prod" funct="ouvrirUrl"/>
                        <menuitem label="Consotarifs" data="1A" id="btnCtA_Prod" funct="ouvrirUrl"/>
                   </menuitem>
                     <menuitem label="En développement " data="top"icon="icon_yahoo" toggled="true">
                        <menuitem label="ConsoView v3" data="1A" id="btnCv3_dev" funct="ouvrirUrl"/>
                        <menuitem label="ConsoView v2" data="1A" id="btnCv2_dev" funct="ouvrirUrl"/>
                        <menuitem label="Mobile Manager Client v1" data="1A" id="btnMmc_dev" funct="ouvrirUrl"/>
                        <menuitem label="Consotarifs Consotel" data="1A" id="btnCt_dev" funct="ouvrirUrl"/>
                        <menuitem type="separator"/>
                        <menuitem label="Brice" data="1A" id="btnBri_dev" funct="ouvrirUrl"/>
                        <menuitem label="Cedric" data="1A" id="btnCed_dev" funct="ouvrirUrl"/>
                        <menuitem label="Vincent" data="1A" id="btnVin_dev" funct="ouvrirUrl"/>
                        <menuitem label="Samuel" data="1A" id="btnSam_dev" funct="ouvrirUrl"/>
                        <menuitem type="separator"/>
                        <menuitem label="Description de la Base" data="1A" id="btnDesc_BDD" funct="ouvrirUrl"/>
                        <menuitem label="Description backOffice" data="1A" id="btnDesc_BO" funct="ouvrirUrl" enabled="false"/>
                        <menuitem label="Description Consoview v3" data="1A" id="btnDesc_CV" funct="ouvrirUrl"enabled="false" />
                        
                     </menuitem>
                    <menuitem label="Outils Internes" data="top"icon="icon_yahoo" toggled="true">
                        <menuitem  label="Gestion Login" data="1A" funct="displayGestionLogin" enabled="false"/>
                        <menuitem type="separator"/>
                        <menuitem label="Contrôle de facturation" data="1A" funct="displayCtrl"/>
                        <menuitem label="Gestion catalogue" data="1A" funct="displayCatalogue"/>
                      	<menuitem type="separator"/>
                      	<menuitem label="Gestion groupes" data="1A" funct="displayImport"/>
                        <menuitem label="Gestion Organisation" data="1A" funct="displayClient"/>                   
                        <menuitem label="Consoprod" data="1A" id="btnProd_Prod" funct="ouvrirUrl"/>
                        <menuitem label="ConsoAdmin" data="1A" id="btnAdmin" funct="ouvrirUrl"/>
                        <menuitem label="Gestion des services" id="btnBatch" funct="ouvrirUrl"/>
                        <menuitem label="Gestion des produits" data="1A" funct="displayProduit"/>
                        <menuitem label="Administration extranet opérateurs" data="1A" funct="displayGestionDesLogin"/>
                        <menuitem label="Gestion des droits" data="1A" funct="displayGestionDroit"/>
                        <menuitem label="Gestion des collectes" data="1A" funct="displayGestionCollecte"/>
                       
                    </menuitem>
                  	 <menuitem label="Espace personnel" data="top"icon="icon_yahoo" toggled="true">
                         <menuitem label="Gestion téléphone IP" data="1A" funct="displayTelIP"/>
                        <menuitem label="Liens personnels" data="1A" funct="displayLienPerso"/>
                     </menuitem>
                    
                    
                   
                </>;
		/*--- masquage ref ticket #2207 à replacer si besoin dans outils internes après gestion oganisation
			<menuitem label="Gestion des clichés" data="1A" funct="displayCliche"/>
			
		-------------------------*/
		
		
		private var autoConnectionFirstTime:Boolean;
		private var mainIntra:Main = new Main();
		private var mainGestion:GestionClient = new GestionClient();
		private var mainImport:GestionImport = new GestionImport();
		private var mainCtrl:GestionCtrl = new GestionCtrl();
		private var gestionLogin : GestionLogin = new GestionLogin();
		private var mainTelIP : TelIP = new TelIP();
		private var mainLienPerso : LienPerso = new LienPerso();
		private var grilleTarifaire : GrilleTarifaireIHM = new GrilleTarifaireIHM();
		private var mainGestionCliche : GestionCliche = new GestionCliche();
		private var mainGestionProduit:GestionProduitIHM = new GestionProduitIHM();
		private var mainGestionDesLogin:GestionDesLoginIHM = new GestionDesLoginIHM();
		private var mainGestionDroit:GestionDroitIHM = new GestionDroitIHM();
		private var mainGestionCollecte:GestionCollecteIHM = new GestionCollecteIHM();
		
		
		//private var indexErgonomie : Index = new Index();
	
	
		public function IntranetMain() { 
			autoConnectionFirstTime = true;
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:Event):void {
					
			addChild(mainIntra);
			title = index.version + " - " + Intranet.mySession.nom;
			/* Desactive gestion des logins pour autres que la prod et it */
		//	if (Intranet.mySession.department=="IT" || Intranet.mySession.department=="Production") {
				menubarXML.child("menuitem")[17].@enabled=true;
		//	};
			initCollections();
			menuBar.dataProvider=menuBarCollection;
			menuBar.addEventListener(MenuEvent.ITEM_CLICK,menuHandler);
		}
		
		private function menuHandler(event:MenuEvent):void
       	{
	      	// Don't open the Alert for a menu bar item that 
	        // opens a popup submenu.
	        if (event.item.@data != "top") {
	        	this[event.item.@funct](event);
	           /* Alert.show("Label: " + event.item.@label + "\n" + 
	            "Data: " + event.item.@data, "Clicked menu item");*/
	        }        
   	 	}
   	 	// Event handler to initialize the MenuBar control.
        private function initCollections():void {
           menuBarCollection = new XMLListCollection(menubarXML);
        }
       // Event handler for the MenuBar control's itemClick event.
     	private function foo():void{
    		Alert.show("foo");
   		}
		
		private function displayClient(event:MenuEvent):void {
			removeChildAt(1);
			mainGestion = new GestionClient()
			addChild(mainGestion);
			
		}
		
		private function displayImport(event:MenuEvent):void {
			removeChildAt(1);
			mainImport = new GestionImport();
			addChild(mainImport);
			
		}
		private function displayTelIP(event:MenuEvent):void {
			removeChildAt(1);
			mainTelIP = new TelIP();
			addChild(mainTelIP);
			
		}
		private function displayLienPerso(event:MenuEvent):void {
			removeChildAt(1);
			mainLienPerso = new LienPerso();
			addChild(mainLienPerso);
		}
		
		private function displayProduit(event:MenuEvent):void{
			removeChildAt(1);
			mainGestionProduit = new GestionProduitIHM();
			addChild(mainGestionProduit);
		}
		
		private function displayGestionDesLogin(event:MenuEvent):void{
			removeChildAt(1);
			mainGestionDesLogin = new GestionDesLoginIHM();
			addChild(mainGestionDesLogin);
		}
		
		private function displayGestionDroit(event:MenuEvent):void{
			removeChildAt(1);
			mainGestionDroit = new GestionDroitIHM ;
			addChild(mainGestionDroit);
		}
		
		private function displayHome(event:MenuEvent):void{
			removeChildAt(1);
			mainIntra = new Main();
			addChild(mainIntra);
			
		}
		private function displayCtrl(event:MenuEvent):void {
			removeChildAt(1);
			mainCtrl = new GestionCtrl();
			addChild(mainCtrl);
			
		}
		private function displayGestionLogin(event:MenuEvent):void {
			removeChildAt(1);
			gestionLogin = new GestionLogin();
			addChild(gestionLogin);
			
		}
		private function displayCliche(event:MenuEvent):void{
			removeChildAt(1);
			mainGestionCliche = new GestionCliche();
			addChild(mainGestionCliche);
		}
		private function displayCatalogue(event:MenuEvent):void{
			removeChildAt(1);
			grilleTarifaire = new GrilleTarifaireIHM();
			addChild(grilleTarifaire);
		}
		
		private function displayGestionCollecte(event:MenuEvent):void{
			removeChildAt(1);
			mainGestionCollecte = new GestionCollecteIHM();
			addChild(mainGestionCollecte);
		}
		
		
		//A delete :
		private function displayTestErgonomie(event:MenuEvent):void {
			removeChildAt(1);
			//addChild(indexErgonomie);
			
		}
		//Fin a delete
		private function displayIntra(event:MenuEvent):void {
			//removeChildAt(1);
			//addChild(mainIntra);
		}
		
	
		
		/*-*************************************************************************************************
		*****************************GESTION DES LIENS*****************************************************
		**************************************************************************************************-*/
		
		
		private function ouvrirUrl(event:MenuEvent):void {
		//	var event2 : Event = event.item.@id
			//Alert.show("-------------->"+event.item.@id);
			
			switch (event.item.@id+"") {
				// Dev
				
				case "btnCv3_dev":
					allerAurl("http://cv-dev.consotel.fr","/index.html","GET");
					break;
				case "btnCv2_dev":
					allerAurl("http://192.168.3.220","/processlogin.cfm","POST");
					break;
				case "btnMmc_dev":
					allerAurl("http://192.168.3.127","/index.html","GET");
					break;
				case "btnCt_dev":
					allerAurl("http://192.168.3.227","/index.cfm","POST");
					break;
				case "btnBri_dev":
					allerAurl("http://192.168.3.25:8080","","GET");
					break;
				case "btnCed_dev":
					
					allerAurl("http://rapiera.consotel.fr","","GET");
					break;
				case "btnVin_dev":
					allerAurl("http://192.168.3.10","","GET");
					break;
				case "btnSam_dev":
					allerAurl("http://192.168.3.31:8080","","GET");
					break;
				// Production
				case "btnCv3_Prod":
					allerAurl("http://cv.consotel.fr","/index.html","GET");
					break;
				case "btnActifs_Prod":
					allerAurl("http://192.168.1.200","/processlogin.cfm","POST");
					break;
				case "btnColt_Prod":
					allerAurl("http://192.168.1.203","/index.cfm","POST");
					break;
				case "btnCtA_Prod":
					allerAurl("http://192.168.3.237","/index.cfm","POST");
					break;
				case "btnCtO_Prod":
					allerAurl("http://192.163.3.237","/index.cfm","POST");
					break;
				case "btnWww_Prod":
					allerAurl("http://192.168.1.201","","GET");
					break;
				case "btnMmc_Prod":
					allerAurl("mmc.consotel.fr","","GET");
					break;
				// Outils Internes
				case "btnProd_Prod":
					allerAurl("http://192.168.3.228","/index.cfm","POST");
					break;
				case "btnAdmin":
					allerAurl("http://192.168.3.222","/index.cfm","POST");
					break;
				case "btnBatch":
					allerAurl("http://192.168.3.125","/index.cfm","POST");
					break;
				case "btnDesc_BDD":
					allerAurl("http://192.168.3.236","/index.html","GET");
				break;
				//lien a definir 
				/*
				case "btnDesc_BO":
					allerAurl("http://192.168.3.236","/index.html","GET");
				break;
				case "btnDesc_C":
					allerAurl("http://192.168.3.236","/index.html","GET");
				break;
				*/
				
			}
		}
		
		private function ouvrirSimpleUrl(event:Event):void {
			switch (event.currentTarget.id) {
				// Paramètres personnels
				case "btnTel":
					allerAsimpleUrl(Intranet.mySession.urlTelephone,"GET");
					break;
			}
		}
		
		private function processEnter(event:KeyboardEvent):void {
			if (event.keyCode==13) {
				//callNumber();
			}
		}
		
		private function allerAurl(adresse:String, page:String, methode:String):void {
			
			var url:String = adresse + page;
            var variables:URLVariables = new URLVariables();
            if (methode=="POST") {
            	variables.code=Intranet.mySession.code;
            	variables.id=Intranet.mySession.id;
            	variables.logon=Intranet.mySession.logon;
            	variables.username=Intranet.mySession.username;
            }
          //  Alert.show("les variables de sessions : "+variables.toString());
            var request:URLRequest = new URLRequest(url);
            request.method=methode;
            request.data=variables;
            navigateToURL(request,"_blank");
		}
		
		private function allerAsimpleUrl(adresse:String, methode:String):void {
			var url:String = "http://" + adresse;
            var variables:URLVariables = new URLVariables();
            if (methode=="POST") {
            	variables.code=Intranet.mySession.code;
            	variables.id=Intranet.mySession.id;
            	variables.logon=Intranet.mySession.logon;
            	variables.username=Intranet.mySession.username;
            	request.data=variables;
            }
            var request:URLRequest = new URLRequest(url);
            request.method=methode;
            navigateToURL(request,"_blank");
		}
		
		private function allerAfichier(adresse:String):void {
			var url:String = adresse;
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            navigateToURL(request,"_blank");
		}
		
	
		
		private function processGz(event:MouseEvent):void {
			var url:String = "http://192.168.3.125/processGz.cfm";
            var variables:URLVariables = new URLVariables();
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            request.data=variables;
            sendToURL(request); 
            Alert.show("Fichiers en cours de traitement");
		}
		
		
	}
}
