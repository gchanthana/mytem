<!--- =========================================================================
Classe: EquipementsUtils
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="EquipementsUtils" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="EquipementsUtils" hint="Remplace le constructeur de EquipementsUtils.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<!--- 
		fournit la liste des �quipements du catalogue (1=client,0 = revendeur)pour un revendeur , 
		un segment (fixe/data ou mobile) et suivant une clef de recherche (libelle equipement, type, mod�le)
	 --->
	<cffunction name="EquipementsUtils" access="public" returntype="query" output="false" hint="" >
		<cfargument name="idRacine" required="true" type="numeric" default="" displayname="numeric idRacine" hint="Initial value for the idRacineproperty." />
		<cfargument name="catalogue" required="true" type="numeric" default="" displayname="numeric catalogue" hint="Initial value for the catalogueproperty." />
		<cfargument name="revendeur" required="true" type="struct" default="" displayname="struct revendeur" hint="Initial value for the revendeurproperty." />
		<cfargument name="segment" required="true" type="numeric" default="" displayname="numeric segment" hint="Initial value for the segmentproperty." />
		<cfargument name="clef" required="true" type="string" default="" displayname="string clef" hint="Initial value for the clefproperty." />
	</cffunction>
	
	<!--- 
		fournit le d�tail d'un �quipement 
		catalogue = PARC | CLIENT | REV
	 --->
	<cffunction name="fournirDetailEquipement" access="public" returntype="query" output="false" hint="" >
		<cfargument name="equipementId" required="false" type="numeric" default="" displayname="numeric equipementId" hint="Initial value for the equipementIdproperty." />
		<cfargument name="catalogue" required="false" type="string" default="" displayname="string catalogue" hint="Initial value for the catalogueproperty." />
	</cffunction>
	
	<!--- 
		fournit la liste des equipements d'une liste de lignes (sims, accessoires, terminaux)		
	 --->
	<cffunction name="fournirListeEquipementsLignes" access="public" returntype="query" output="false" hint="" >
		<cfargument name="lignes" required="false" type="array" default="" displayname="array lignes" hint="Initial value for the lignesproperty." />
	</cffunction>
	
	
	<!--- 
		retourne la derniere carte sim cr�er pour l'operateur distribué par le revendeur
		si l'operateur n'est pas spécifié alors la procédure retourne la derniere carte sim créée pour le revendeur
		si pas de carte sim on la crée.
	--->
	<cffunction name="fournirCarteSim" access="public" returntype="query" output="false" hint="">
		<cfargument name="idRevendeur" required="true" type="numeric" displayname="ID REVENDEUR"/>
		<cfargument name="idOperateur" required="false" type="numeric" displayname="ID OPERATEUR"/>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.getSimCard_v2">
			
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#">
			
			<cfprocresult name="p_result">
			
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="fournirEquipementParent" access="public" returntype="query" output="false">
		<cfargument name="idEquipement" required="true" type="numeric" displayname="ID EQ"/>
		
		<cfquery datasource="#session.offreDSN#" name="p_result">
			SELECT equipement.* FROM equipement, (select * from equipement where equipement.idequipement = #idEquipement#)fils WHERE equipement.idequipement = fils.idequipement_parent
		</cfquery>
		<cfreturn p_result>
	</cffunction>
	
	<!--- 
		efface la photo indiqu�e dans les params
		retourne 1 si ok sinon -1
	 --->
	<cffunction name="effacerPhoto" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="urlPhoto" required="false" type="string" default="" displayname="string urlPhoto" hint="Initial value for the urlPhotoproperty." />
	</cffunction>
	
	<cffunction name="fournirListeEquipementsCatalogueRevendeur" returntype="query" access="remote">
		<cfargument name="idRevendeur" required="true" type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string"  displayname="clef" hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="string"  displayname="segment" hint="le filtre par segment"/>
<!---	
	PROCEDURE
	
		pkg_cv_equipement.SearchCatalogueRevendeur
		
			p_idcategorie_equipement	IN INTEGER
			p_idtype_equipement			IN INTEGER
			p_idfournisseur				IN INTEGER
			p_idgamme_fournis			IN INTEGER
			p_chaine					IN VARCHAR2
			p_idgroupe_client			IN INTEGER
			p_type_fournisseur			IN INTEGER
			p_retour 					OUT SYS_REFCURSOR
--->		

		<cfset p_idcategorie_equipement = 0>
		<cfset p_idtype_equipement = 0>
		<cfset p_idfournisseur = idRevendeur>
		<cfset p_idgamme_fournis = 0>
		<cfset p_chaine = clef>
		<cfset p_idgroupe_client = SESSION.PERIMETRE.ID_GROUPE>
		<cfset p_type_fournisseur = 0>
				
		<cfif p_type_fournisseur eq 0>
			<cfset flag="true">
		<cfelse>
			<cfset flag="false">
		</cfif>
				
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.SearchCatalogueRevendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#" null="#flag#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<!--- 
		lISTE DES MOBILES SEULEMENT		
	 --->
	<cffunction name="fournirListeMobileCatalogueClient" returntype="query" access="remote">
		<cfargument name="idRevendeur" required="true" type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string"  displayname="clef" hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="string"  displayname="segment" hint="le filtre par segment"/>
		
		<cfset resultProc= fournirListeEquipementsCatalogueRevendeur(idRevendeur,clef,segment)>	
		
		<cfquery name="getListe" dbtype="query">
			select *
			from resultProc
			where IDTYPE_EQUIPEMENT = 9 or IDTYPE_EQUIPEMENT = 70
		</cfquery>
		<cfreturn getListe>
		
	</cffunction>
	
	<!--- 
		lISTE DES ACCESSOIRES	
	 --->
	<cffunction name="fournirListeAccessoiresCatalogueClient" returntype="query" access="remote">
		<cfargument name="idRevendeur" required="true" type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string"  displayname="clef" hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="string"  displayname="segment" hint="le filtre par segment"/>
		
		<cfset resultProc= fournirListeEquipementsCatalogueRevendeur(idRevendeur,clef,segment)>	
		
		<cfquery name="getListe" dbtype="query">
			select *
			from resultProc
			where IDTYPE_EQUIPEMENT = 2191
		</cfquery>

		<cfreturn getListe>
		
	</cffunction>
	
	<!--- 
		lISTE DES ACCESSOIRES DU MOBILE
	 --->
	<cffunction name="fournirListeAccessoiresDuMobileCatalogueClient" returntype="query" access="remote">
		<cfargument name="idRevendeur" required="true" type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string"  displayname="clef" hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="string"  displayname="segment" hint="le filtre par segment"/>
		<cfargument name="idEquipements" required="false" type="numeric"  displayname="mobile" hint="id du mobile"/>
		
		<cfset resultProc= fournirListeEquipementsCatalogueRevendeur(idRevendeur,clef,segment)>	
		
		<cfquery name="getListe" dbtype="query">
			select *
			from resultProc
			where IDTYPE_EQUIPEMENT = 2191
		</cfquery>

		<cfreturn getListe>
		
	</cffunction>
	
	<cffunction name="fournirListeEquipementsCatalogueClient" returntype="query" access="remote">
		<cfargument name="idRevendeur" required="true" type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string"  displayname="clef" hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="string"  displayname="segment" hint="le filtre par segment"/>
<!---
	PROCEDURE
		
		pkg_cv_equipement.searchcatalogueclient
			p_idracine					IN INTEGER
			p_idcategorie_equipement	IN INTEGER
			p_idtype_equipement			IN INTEGER
			p_idfournisseur				IN INTEGER
			p_idgamme_fournis			IN INTEGER
			p_chaine					IN VARCHAR2
			p_idgroupe_client			IN INTEGER
			p_retour 					OUT SYS_REFCURSOR
--->		
		<cfset p_idracine = SESSION.PERIMETRE.ID_GROUPE>		
		<cfset p_idcategorie_equipement = 0>		
		<cfset p_idtype_equipement = 0>		
		<cfset p_idfournisseur = idRevendeur>
		<cfset p_type_fournisseur = 1>
		<cfset p_idgamme_fournis = 0>
		<cfset p_chaine = clef>
		<cfset p_idgroupe_client = SESSION.PERIMETRE.ID_PERIMETRE>
		
		 
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.searchcatalogueclient_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgroupe_client#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#">
			<cfprocresult name="p_result">			
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="majPrixEquipementCatalogueRevendeur" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idequipementFour" required="true" type="numeric"/>
		<cfargument name="prix_catalogue" required="true" type="numeric"/>
		<cfargument name="prix_2" required="true" type="numeric"/>
		<cfargument name="prix_3" required="true" type="numeric"/>
		<cfargument name="prix_4" required="true" type="numeric"/>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_EQUIPEMENT.MAJPRIXEQFOUR">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipement"value="#idequipementFour#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_catalogue#" null="#iif((prix_catalogue lt 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_2#" null="#iif((prix_2 lt 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_3#" null="#iif((prix_3 lt 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_4#" null="#iif((prix_4 lt 0), de("yes"), de("no"))#">						
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	
	<cffunction name="majPrixEquipementCatalogueClient" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idequipementClient" required="true" type="numeric"/>
		<cfargument name="prix_catalogue" required="true" type="numeric"/>
		<cfargument name="prix_2" required="true" type="numeric"/>
		<cfargument name="prix_3" required="true" type="numeric"/>
		<cfargument name="prix_4" required="true" type="numeric"/>			
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_EQUIPEMENT.MAJPRIXEQCLIENT">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipement"value="#idequipementClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_catalogue#" null="#iif((prix_catalogue lt 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_2#" null="#iif((prix_2 lt 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_3#" null="#iif((prix_3 lt 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_prix" value="#prix_4#" null="#iif((prix_4 lt 0), de("yes"), de("no"))#">						
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	
	<!--- LISTE DES LIGNES D'UN EQUIPEMENT  --->
	<cffunction name="fournirListeLignes" returntype="query" access="remote">
		<cfargument name="idequipement" required="true" type="numeric">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_EQUIPEMENT.GETEQLIGNE">
			<cfprocparam type="in" cfsqltype="cf_SQL_NUMERIC" value="#idequipement#">
			<cfprocresult name="pFournirListeLignes">
		</cfstoredproc>
		<cfreturn pFournirListeLignes>
		
	</cffunction>
	
	
	<!--- INFOS TERMINAL ET MODELE  --->
	
	<cffunction name="fournirInfosModeleTerminal" returntype="query" access="remote">
		<cfargument name="idEquipement" required="true" type="numeric"/>
				
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_equipement.infosup_equipement">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#">			
			<cfprocresult name="p_result">			
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	
	<cffunction name="majInfosModeleTerminal" returntype="query" access="remote">		
		<cfargument name="themodele" required="true" type="struct"/>
								
		<cfset idracine = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_equipement.infosup_eq_maj">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.IDEQUIPEMENT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.LIBELLE_EQ#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NO_SERIE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_DATE"	value="#themodele.DATE_ACHAT#" null="#iif((themodele.DATE_ACHAT eq ""), de("yes"), de("no"))#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_DATE"	value="#themodele.DATE_MES#" null="#iif((themodele.DATE_MES eq ""), de("yes"), de("no"))#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_DATE"	value="#themodele.DATE_ENLEVEMENT#" null="#iif((themodele.DATE_ENLEVEMENT eq ""), de("yes"), de("no"))#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.ETAT_INVENTAIRE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_DATE"	value="#themodele.DATE_AJOUT#" null="#iif((themodele.DATE_AJOUT eq ""), de("yes"), de("no"))#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NUMERO_LICENCE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_LICENCE#" >
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.DUREE_LICENCE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NOMBRE_LICENCE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.REFERENCE_EQ#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CODE_INTERNE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.COMMENTAIRE_EQ#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.HAUTEU_U#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.POIDS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.BRUIT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MTBF#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.PUISSANCE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MONTAGE_RACK#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.PRIX_ACHAT_UNITAIRE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_DATE"	value="#themodele.DATE_LIVRAISON#" null="#iif((themodele.DATE_LIVRAISON eq ""), de("yes"), de("no"))#">
			
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.MEMOIRE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CAPACITE_CF#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.EMPLACEMENT_INTERNE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.SUPPORT_POE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NBR_ALIM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.SUPPORT_QOS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.SUPPORT_VPN#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.SUPPORT_NAT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.EN_RESERVE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_SLOTS_MAX#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_SLOTS_CMM_MAX#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_SLOTS_SFM_MAX#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_ALIM_MAX#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MAX_HEAT_DISSIPATION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.IMEI#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.DIMENSIONS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.AFFICHAGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.RESOLUTION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.MEMOIRE_INTERNE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.RESEAU#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_CARTE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NB_ENTREE_SA#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NB_ENTREE_S#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NUM_SIM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.DRAM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.RAM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MEMOIRE_FLASH#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PROT_MANAGEMENT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.STANDARDS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.INTERFACE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBPORT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.TAUX_TRANSFERT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBCARTE_RESEAU#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TECHNOLOGIE_RAM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MEMOIRE_CACHE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBSLOT_RAM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CHIPSET_CARTE_MERE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CARTE_GRAPHIQUE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CARTE_SON#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_ECRAN#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TAILLE_ECRAN#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.AUTONOMIE_VEILLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.AUTONOMIE_FONCTIONNEMENT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_BATTERIE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.VITESSE_IMPRESSION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TAILLE_MAX_DOCUMENT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_CARTOUCHE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.INDICATEUR_ETATS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_ALIMENTATION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.COULEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.URL_FABRICANT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PROTOCOLE_CONTROLE_STANDARD#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_CANAUX_IP#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_DISQUE_DUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_DISQUE_DUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.VITESSE_ECRITURE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.IDCONNECTEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.INTERRUPTEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_PRISE_SORTIE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.DISJONCTEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.DISPOSITIF_RESEAUX#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PROTOCOLE_DE_ROUTAGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.EMPILABLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TAILLE_TABLE_ADRESSE_MAC#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CAPACITE_COMMUNICATION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.MODEM_ADSL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_MODEM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.DBI_ANTENNES#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_CRYPTAGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.BANDE_FREQUENCE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.PORTEE_CHAMPS_LIBRE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.PORTEE_BATIMENT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.SUPPORT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.COMPOSITION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CABLAGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CATEGORIE_CABLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.BLINDAGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.INDICATEUR_PROTECTION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.CHARGE_ADMISSIBLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.HAUTEUR_INTERIEURE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.FORMAT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_VENTILATEURS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_VENTILATEURS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.FORMAT_VENTILATEUR_TOITURE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.DEBIT_CPL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.FICHE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PORTEE_TYPIQUE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.EMISSIONS_ELECTROMAGNETIQUES#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_MONITEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TECHNOLOGIE_WIRELESS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TECHNOLOGIE_IMPRESSION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_BAC_ALIMENTATION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.SONORITE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.IMPRESSION_RECTO_VERSO#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TECHNOLOGIE_SCANNER#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.RESOLUTION_OPTIQUE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.RESOLUTION_INTERPOLEE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.REDUCTION_MAXIMAL_DOC#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_COPIE_MAX#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.PAS_PIXEL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.LUMINOSITE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.RAPPORT_CONTRASTE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CONNEXION_IMPRIMANTE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TECHNOLOGIE_CONNECTIVITE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PROT_RESEAU#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PROT_LIAISON_DONNEE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PLATEFORME_SUPPORTE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NORME_WIFI#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.METHODE_IMPRESSION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.RESOLUTION_IMPRESSION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.MEMOIRE_TAMPON_PAGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.VITESSE_MODEM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.SERVICE_VOIP#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PROTOCOLE_IP#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.VIBREUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_COULEURS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.DAS#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.VERSION_WAP#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CARTE_SIM_INTEGRE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.DRIVER#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.SLOTABLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.INTEGRE_A#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PIN1#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PIN2#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PUK1#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PUK2#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.HLR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.LANGUE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.COMPRESSION_VOIE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NOMBRE_LIGNE_TEL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPES_LIGNES#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPES_PASSERELLES#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.SIM_COMPATIBLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_GSM#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.VITESSE_ROTATION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.BAIE_DISQUE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.EXTERNE_INTERNE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.GRAVEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.VITESSE_ECRITURE_CD#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.VITESSE_LECTURE_CD#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.TEMPS_ACCES_MOYEN#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_SUPPORT_AMOVIBLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.LONGUEUR_CABLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.DUREE_EXECUTION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.CAPACITE_BATTERIE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NORME_SURTENSION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.INDICE_CONSOMMATION_ENERGIE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.COURANT_ELECTRIQUE_MAXIMAL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_BANDES_RESEAU#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.EDGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MODEM_INTEGRE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.BLUTOOTH#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.INFRAROUGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.PLATEFORMES#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.CLIENT_EMAIL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MAIN_LIBRE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CALLERID_STANDARD#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MURAL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.REPONDEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.MANAGEABLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.INTERFACE_CONTROLLER#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.TYPE_STOCKAGE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NB_UTILISATEUR_LOCAL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.TEMPERATURE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.ALIMENTATION#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_NUMERIC"	value="#themodele.AGRANDISSEMENT_MAXIMAL_DOC#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.DEVELOPPEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.EDITEUR#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.RACKABLE#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.NBR_ANTENNES#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_INTEGER"	value="#themodele.EQ_3G#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.VERSION_LOGICIEL#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.NBR_UTILISATEURS_SIMULT#">
			<cfprocparam type="In"	cfsqltype="CF_SQL_VARCHAR"	value="#themodele.CLASSE_GPRS#">
			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="qUpdate">			
		</cfstoredproc>
		<cfreturn qUpdate>
	</cffunction>		
		
	
	
	
	<cffunction name="fournirInfosTerminal" returntype="query" access="remote">
		<cfargument name="idEquipement" required="true" type="numeric"/>
		<cfset idracine = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_equipement.getInfoTerminal">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocresult name="p_result">			
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="majInfosTerminal"   returntype="numeric" access="remote">
		<cfargument name="idequipement"   required="true"  type="numeric"/>
		<cfargument name="idcontrat"      required="true"  type="numeric"/>
		<cfargument name="idcommande"     required="true"  type="numeric"/>
		<cfargument name="date_achat"     required="true"  type="string"/>
		<cfargument name="duree_garantie" required="true"  type="numeric"/>
		<cfargument name="ref_commande"   required="true"  type="string"/>
		<cfargument name="imei" 		  required="true"  type="string"/>
		<cfargument name="commentaires"   required="false" type="string" />
		<cfargument name="date_livraison" required="true"  type="string" />
		<cfargument name="ref_livraison"  required="false" type="string" />
		<cfargument name="prix_terminal"  required="false" type="numeric" />
		<cfargument name="idEqtFournis"	  required="false" type="numeric" />
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_equipement.updateInfoTerminal_v3">
						
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idequipement#">
			
			<cfif idcontrat gt 0>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcontrat#">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcontrat#" null="true">
			</cfif>
			
			<cfif idcommande gt 0>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcommande#">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcommande#" null="true">
			</cfif>	
			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_achat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#duree_garantie#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_commande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#imei#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_livraison#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ref_livraison#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#prix_terminal#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEqtFournis#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="qUpdate">
		</cfstoredproc>
		
		<cfreturn qUpdate>
	</cffunction>
	
	<!--- ENREGISTRER UNE COMMANDE MOBILE POUR L'EQUIPEMENT --->
	<cffunction name="enregistrerCommande" access="public" returntype="numeric" output="false" hint="">		
		<!--- pour la commande --->
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut" hint="" />
		<cfargument name="idCompte" 			required="false" type="numeric" default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="idSousCompte" 		required="false" type="numeric" default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="idOperateur" 			required="false" type="numeric" default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment" hint="" />
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="dateEffetPrevue"		required="true" type="string" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut" hint="" />
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin" hint="" />
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef" hint="" />
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment" hint="" />
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin" hint="" />
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin" hint="" />
		
		<!--- pour le contrat de garantie --->
		<cfargument name="idEquipement" 		required="true" type="numeric" 	default="" 		displayname="idequipement" hint="" />
		<cfargument name="dateDebutContrat" 	required="true" type="string" 	default="" 		displayname="date debut contrat" hint="" />
		<cfargument name="dureeContrat" 		required="true" type="numeric" 	default="" 		displayname="duree contrat" hint="" />
		<cfargument name="idTypeContrat" 		required="true" type="numeric" 	default="" 		displayname="id type contrat = 3" hint="" />
		<cfargument name="prixAchat" 			required="true" type="numeric" 	default="" 		displayname="prix achat" hint="" />
		<cfargument name="refLivraison" 		required="true" type="string" 	default="" 		displayname="reference livraison" hint="" />
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date livraison" hint="" />

		<!--- <cfargument name="articles" 			required="true" type="string" 	default="" 		displayname="string clef" hint="" /> --->		
		
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_gestionoperation.enregistrerCdEquExist">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			
			<cfif idCompte gt 0>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcompte" 					value="#idCompte#">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="true" variable="p_idcompte" 		value="#idCompte#">
			</cfif>
			<cfif idSousCompte gt 0>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_compte"				value="#idSousCompte#">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="true" variable="p_idsous_compte" 	value="#idSousCompte#">
			</cfif>
			<cfif idOperateur gt 0>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperateur" 					value="#idOperateur#">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="true" variable="p_idoperateur" 		value="#idOperateur#">
			</cfif>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcontact_revendeur" 				value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtransporteur" 					value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_livraison" 				value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_date_effet_prevue" 				value="#dateEffetPrevue#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfif idGroupeRepere gt 0>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_repere" 				value="#idGroupeRepere#">
			<cfelse>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  null="true" variable="p_idgroupe_repere" value="#idGroupeRepere#">
			</cfif>
			<!--- <cfprocparam type="In" cfsqltype="CF_SQL_CLOB" null="false" variable="p_article" 				value="#tostring(articles)#"> --->
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idequipement" 		value="#idEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_debut"	 		value="#dateDebutContrat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_duree"		 		value="#dureeContrat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_type"		 		value="#idTypeContrat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FloAT" null="false" variable="p_prix"		 		value="#prixAchat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref"			 		value="#refLivraison#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date"		 		value="#dateLivraison#">
			
			
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" >
		</cfstoredproc>
				
		<cfreturn p_idoperation>
	</cffunction>
	
	
	
	<!--- 
		catalogue = CLIENT | PARC | REV
		retour = urlPhoto si ok sinon error
		
		extait, de la bdd, la photo d'un équipement l'ecrit dans un fichier  et fournit son url.
		Ne pas oublier d'effacer la photo é la fin du téléchargement par le client flex ou autre.
	 --->
	<cffunction name="fournirPhoto" access="public" returntype="string" output="false" hint="" >
		<cfargument name="idequipement" required="false" type="numeric" default="" displayname="numeric idequipement" hint="Initial value for the idequipementproperty." />
		<cfargument name="catalogue" required="false" type="string" default="" displayname="string catalogue" hint="Initial value for the catalogueproperty." />
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
	
	<cffunction name="NoSerieExists" access="public" returntype="numeric" output="false">
		<cfargument name="numero_serie" required="true" type="String" hint="le numero de série correspondant au type ci dessus"/>
		<cfargument name="type" 		required="true" type="numeric" hint="(1:IMEI,2:NUM_SIM,3:No_serie)"/>
		<cfset idracine = Session.PERIMETRE.ID_GROUPE>
				
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_flotte.NoSerieExists">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" value="#idracine#">						
			<cfprocparam type="In" 	cfsqltype="CF_SQL_VARCHAR" value="#numero_serie#">
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" value="#type#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result"/>						     
		</cfstoredproc>
		<cfreturn result/>
	</cffunction>

</cfcomponent>
