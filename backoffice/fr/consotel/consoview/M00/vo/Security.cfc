﻿component  displayname="Security" hint="Gestion de la sécurilté" output="false"
{
	property name="isInitialized" type="boolean" displayname="isInitialized" hint="Indique si le composant a ete initialisé via la fonction init best pratice" default="0" getter="true" setter="false" ;
	
	
	/**
	*@displayname 'init'
	*@hint 'constructeur'
	*@output false
	**/
	public void function init(required string apiKey)
	{
		var api_key = createUUID();
		if(isdefined(session))
		{
			session[api_key]={};					
		}
		else
		{
			throw("Seesion undifeined","application","","E0001")
		}
		
	}  
	
	private boolean function isSessionInitialized()
	 displayname="isSessionInitialized" hint="retourne true si la session mytem a été initilialisé sinon retourne flase" output="false" returnFormat="plain"
	{
		if(structKeyExists(SESSION,ukey))
		{
			return true;	
		}
		else
		{
			return false;
		}
	}

}