<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="getListCarriers" returntype="query">
			<cfset RacineId = SESSION.PERIMETRE.ID_GROUPE>
			<cfset locale = SESSION.USER.GLOBALIZATION>
			<cfset DSN = SESSION.OFFREDSN>

            <cfreturn listCarriers(DSN,RacineId,locale)>
      </cffunction> 

      <cffunction access="remote" output="false" name="listCarriers" returntype="query">
      		<cfargument name="DSN" type="string" required="true">
			<cfargument name="RacineId" type="numeric" required="true">
            <cfargument name="locale" type="string" required="true">
	
			<cfstoredproc datasource="#ARGUMENTS['DSN']#" procedure="pkg_cv_grcl_v3.sf_listeop_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#ARGUMENTS['RacineId']#" variable="p_idracine">
				<cfprocresult name="qListCarriers">
			</cfstoredproc>	

			<cfquery name="qCarrier" dbtype="query">
				select OPERATEURID as ID, OPNOM as LIBELLE from qListCarriers ORDER BY OPNOM DESC
			</cfquery>

            <cfreturn qCarrier>
      </cffunction> 

</cfcomponent>