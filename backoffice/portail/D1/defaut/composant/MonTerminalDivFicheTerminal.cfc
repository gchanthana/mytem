<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="true" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	<!---  --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- CONSTRUCTEUR --->
	</cffunction>
	
	
	<!--- ONGLET DE DETAIL D'UN TERMINAL --->
	<cffunction name="getDivFicheTerminal" access="public" output="true" returnType="string"
				displayname="" 
				hint="Génère la div contenant la fiche du détail complet d'un terminal / template D1" >
				
		<cfargument name="idSousTete" required="false" type="numeric" default=0>
		
		<cfset segment = 1 >
		<cfset sous_tete = "">
		<cfset idTerminal = 0>
		<cfset idSIM = 0>
		<cfset idID = 0>
		<cfset IMEI = "">
		<cfset boolIsManaged = -1>
		<cfset mdmDeviceID = "">
		<cfset mdmImei = "">
		<cfset serial = "">
		
		
		<cfif idSousTete eq 0> <!---  si première navigation sur cette page, affiche premier terminal de la liste --->
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
							
					<!--- initialisation pour combo mon terminal / page mon terminal --->
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfset idTerminal = val(SESSION.queryMesTerminaux['IDTERMINAL'][currentRow]) >
					<cfset idID = val(SESSION.queryMesTerminaux['ID'][currentRow]) >
					<cfset IMEI = SESSION.queryMesTerminaux['T_IMEI'][currentRow] >
					<cfset boolIsManaged = SESSION.queryMesTerminaux['BOOL_ISMANAGED'][currentRow] >
					<cfset idSIM = val(SESSION.queryMesTerminaux['IDSIM'][currentRow]) >
					<cfset idSousTete = val(SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow]) >
					<cfset mdmDeviceID = val(SESSION.queryMesTerminaux['ZDM_DEVICE_ID'][currentRow]) >
					<cfset mdmImei = SESSION.queryMesTerminaux['ZDM_IMEI'][currentRow] >
					<cfset serial = SESSION.queryMesTerminaux['ZDM_SERIAL_NUMBER'][currentRow] >
					<cfbreak>
				</cfif>
			</cfloop>
			
		<cfelse>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] eq idSousTete>
					<!--- initialisation pour combo ma ligne / page accueil --->
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfset idTerminal = val(SESSION.queryMesTerminaux['IDTERMINAL'][currentRow]) >
					<cfset idID = val(SESSION.queryMesTerminaux['ID'][currentRow]) >
					<cfset IMEI = SESSION.queryMesTerminaux['T_IMEI'][currentRow] >
					<cfset boolIsManaged = SESSION.queryMesTerminaux['BOOL_ISMANAGED'][currentRow] >
					<cfset idSIM = val(SESSION.queryMesTerminaux['IDSIM'][currentRow]) >
					<cfset idSousTete = val(SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow]) >
					<cfset mdmDeviceID = val(SESSION.queryMesTerminaux['ZDM_DEVICE_ID'][currentRow]) >
					<cfset mdmImei = SESSION.queryMesTerminaux['ZDM_IMEI'][currentRow] >
					<cfset serial = SESSION.queryMesTerminaux['ZDM_SERIAL_NUMBER'][currentRow] >
					<cfbreak>
				</cfif>
			</cfloop>
		
		</cfif>
		
		<cfset sim = " -- ">
		<cfloop query="SESSION.queryMesTerminaux" >
			<cfif SESSION.queryMesTerminaux.T_IMEI eq idTerminal>
				<cfset sim = SESSION.queryMesTerminaux.SIM>
				<cfbreak>
			</cfif>
		</cfloop>
		
		
		<cfset query = createObject("component","fr.consotel.consoview.M111.fiches.FicheTerminal").getInfosFicheTerminal(idTerminal,segment,SESSION.IDPOOL,idID) >
		<cfset querySim = createObject("component","fr.consotel.consoview.M111.fiches.FicheSim").getInfosFicheSim(idSIM,idSousTete,SESSION.USER.CLIENTID,idID) >
		
		<!--- ON RECUPERE LES QUERY DONT NOUS AVONS BESOIN DANS LES ARRAY --->
		<cfset queryInfoFicheTerminal = query[2]>
		<cfset queryInfoSim = querySim[2]>
		<cfset queryInfoFicheCaracteristik = query[5]>
		
		
		<cfset queryInfoFicheEquipement = SESSION.queryDeviceInfo.DEVICEPROPERTIES>
		<cfset queryInfoFicheLogiciel = SESSION.queryDeviceInfo.SOFTWAREINVENTORY>
		
		<cfset roaming = " -- ">
		<cfset sim_entreprise = " -- ">
		<cfset operateur_entreprise = " -- ">
		<cfset operateur_reseau = " -- ">
		<cfset cpt = -1>
		<cfloop query="queryInfoFicheEquipement">
			<cfif NOT compare(queryInfoFicheEquipement.name,"Romaing Data autorisé")>
				<cfset roaming = queryInfoFicheEquipement.value>
			</cfif>
			<cfif NOT compare(queryInfoFicheEquipement.name,"Numéro de carte SIM")>
				<cfset sim_entreprise = queryInfoFicheEquipement.value>
			</cfif>
			<cfif NOT compare(queryInfoFicheEquipement.name,"Réseau opérateur actuel")>
				<cfset operateur_entreprise = queryInfoFicheEquipement.value>
			</cfif>
			<cfif NOT compare(queryInfoFicheEquipement.name,"Réseau opérateur de la carte SIM")>
				<cfset operateur_reseau = queryInfoFicheEquipement.value>
			</cfif>
			<cfset cpt = cpt + 1>
		</cfloop>
		
		<cfset pin1 = " -- ">
		<cfset pin2 = " -- ">
		<cfset puk1 = " -- ">
		<cfset puk2 = " -- ">
		<cfset marque = " -- ">
		<cfset modele = " -- ">
		<cfset serieIMEI = " -- ">
		
		<cfif queryInfoSim.PIN1 neq "">
			<cfset pin1 = queryInfoSim.PIN1>
		</cfif>
		<cfif queryInfoSim.PIN2 neq "">
			<cfset pin2 = queryInfoSim.PIN2>
		</cfif>
		<cfif queryInfoSim.PUK1 neq "">
			<cfset puk1 = queryInfoSim.PUK1>
		</cfif>
		<cfif queryInfoSim.PUK2 neq "">
			<cfset puk2 = queryInfoSim.PUK2>
		</cfif>
		<cfif queryInfoFicheTerminal.MARQUE neq "">
			<cfset marque = queryInfoFicheTerminal.MARQUE>
		</cfif>
		<cfif queryInfoFicheTerminal.MODELE neq "">
			<cfset modele = queryInfoFicheTerminal.MODELE>
		</cfif>
		<cfif queryInfoFicheTerminal.IMEI neq "">
			<cfset serieIMEI = queryInfoFicheTerminal.IMEI>
		</cfif>
		
		
		<!--- ------------------------------------ --->
		<!--- TABLEAU CARACTERISTIQUES PRINCIPALES --->
		<!--- ------------------------------------ --->
		
		<cfset garantie = "">
		<cfloop query="queryInfoFicheCaracteristik">
			<cfset currentRow = queryInfoFicheCaracteristik.currentrow>
			
			<cfif queryInfoFicheCaracteristik['IDTYPE_CONTRAT'][currentRow] eq 3>
				<cfset date_echeance = queryInfoFicheCaracteristik['DATE_ECHEANCE'][currentRow]>
				<cfset garantie = DateCompare(date_echeance, Now()) >
				<cfbreak>
			</cfif>
			
		</cfloop>
		
		
		<cfset sous_garantie = "non">
		<cfif garantie eq 1>
			<cfset sous_garantie = "oui">
		</cfif>
		
		<cfset date_achat = " -- " >
		<cfset euro = "" >
		<cfset date_renouvell = " -- " >
		<cfif queryInfoFicheTerminal.DATE_ACHAT neq "">
			<cfset date_achat = RemoveChars(queryInfoFicheTerminal.DATE_ACHAT, 11, 11) >
			<cfset date_achat = LSDateFormat(#date_achat#, "dd/mm/yyyy") >
		</cfif>
		<cfif queryInfoSim.DATE_RENOUVELLEMENT neq "">
			<cfset date_renouvell = RemoveChars(queryInfoSim.DATE_RENOUVELLEMENT, 11, 11) >
			<cfset date_renouvell = LSDateFormat(date_renouvell, "dd/mm/yyyy") >
		</cfif>
		<cfif queryInfoFicheTerminal.MONTANT neq "">
			<cfset euro = "&nbsp;€" >
		</cfif>
		
		<!--- formatage numero --->
		<cfset numm = createObject("component","portail.class.classData.DataService").phoneFormat("#sous_tete#")>
		<cfif sous_tete eq ""> <cfset numm = " -- -- -- -- -- "> </cfif>
		
		<cfif boolIsManaged eq 0> <!--- TERMINAL EST ENROLLÉ --->
			<cfset class = "inputActionTerminal" > <!--- boutons non grisés --->
			<cfset btnType = "submit" >
			<cfset onclick = "onclick='if(confirmEnrollement()) return true; else return false;'">
			<cfset disabledEnrollement = "">
			<cfset title = 'title="Intégrer mon terminal dans la politique de sécurité de l''entreprise"'>
			
		<cfelse> <!--- TERMINAL N'EST PAS ENROLLE --->
			
			<cfset class = "inputActionGrisTerminal" > <!--- boutons grisés --->
			<cfset btnType = "button" >
			<cfset onclick = "">
			<cfset disabledEnrollement = "disabled='disabled'">
			<cfset title = "">
		</cfif>
		
		<cfif boolIsManaged eq -1 && cpt eq -1>
			<cfset disabledEnrollement = "disabled='disabled'">
			<cfset class = "inputActionGrisTerminal" >
		</cfif>
		
		<cfset ficheTerminal = ''>
		
		<!--- SI CLIC SUR GERER MON TERMINAL --->
		<cfif isDefined("FORM.enroll") && FORM.enroll eq "Gerer mon terminal" >
			
			<cfset retourEnroll = createObject("component","fr.consotel.consoview.M111.MDMService").simpleEnrollement(IMEI,serial,mdmImei,idTerminal,sous_tete,idSousTete,SESSION.IDPOOL) >
			
			<cfset ficheTerminal &= '
				<div id="popupValidEnrollement" class="divValidEnrollement" style="">
					<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
						<tr>
							<td align="right"><a style="cursor:pointer" onclick="closePopup(''popupValidEnrollement'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center" style="font-weight:bold;font-size:12px; color:##009fe3;">Votre demande a bien été prise en compte</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr style="height:0px;">
			              <td align="center" style="">
				              <input type="button" value="Fermer" onclick="closePopup(''popupValidEnrollement'')" style="">
				              <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
						  </td>
			            </tr>
					</table>
				</div>
			'>
		</cfif>
		
		<cfset ficheTerminal &= '
				<!--- div horizontal ligne associée et bouton gerer mon terminal --->
				<div align="left" class="" style="margin-bottom:0px;font-size:14px;font-weight:bold;">
					<div style="float:left;">Ligne associé au terminal&nbsp;&nbsp;&nbsp;<span style="color:##009fe3;">#numm#</span></div>
					<div align="center" class="" style="float:left;width:50%;display:inline-block">
						<form method="post" action="">
							<input type="#btnType#" #title# #disabledEnrollement# #onclick# name="enroll" value="Gerer mon terminal" class="#class#" >
						</form>
					</div>
					<div style="clear:both;">&nbsp;</div>
				</div>
				
				<div align="left" class="enteteCaracteristik" style="font-size:14px;">Principales caractéristiques</div>
				<div align="left" class="divTablesDetailsTerminal" style="">
					<div class="tableDetailTerminalGauche" style="">
						<table class="tableCaracteristikGauche" width="100%" style="">
							<tbody>
								<tr>
									<td align="left" class="tdCaracteristik1" style="">Sim entreprise</td>
									<td>:&nbsp;#sim_entreprise#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik1" style="">Sim détectée sur réseau</td>
									<td>:&nbsp;#sim#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik1" style="">Opérateur entreprise</td>
									<td>:&nbsp;#operateur_entreprise#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik1" style="">Opérateur sur réseau</td>
									<td>:&nbsp;#operateur_reseau#</td>
								</tr>
								<tr style="height:10px;">
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik1" style="">Série / IMEI</td>
									<td>:&nbsp;#serieIMEI#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik1" style="">Marque</td>
									<td>:&nbsp;#marque#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik1" style="">Modèle</td>
									<td>:&nbsp;#modele#</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="tableDetailTerminalDroite" style="">
						<table class="tableCaracteristikDroite" width="100%" style="">
							<tbody>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Code pin 1</td>
									<td >:&nbsp;#PIN1#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Code pin 2</td>
									<td style="" width="">:&nbsp;#pin2#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Code puk 1</td>
									<td >:&nbsp;#puk1#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Code puk 2</td>
									<td >:&nbsp;#puk2#</td>
								</tr>
								<tr style="height:10px;">
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Roaming data autorisé</td>
									<td >:&nbsp;#roaming#</td><!--- #euro# --->
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Date d''achat</td>
									<td >:&nbsp;#date_achat#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Date de renouvellement</td>
									<td >:&nbsp;#date_renouvell#</td>
								</tr>
								<tr>
									<td align="left" class="tdCaracteristik2" style="">Sous garantie</td>
									<td >:&nbsp;#sous_garantie#</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div style="clear:both;">&nbsp;</div>
				</div>
		'>
		
		<!--- TABLEAU D'ONGLETS --->
		
		<!--- ON RECUPERE LE 5EME QUERY DE L'ARRAY --->
		<cfset queryInfoFicheTerminal = query[5]>
		
		
		<!--- POUR SCROLLER OU NON LE TABLEAU SELON LE NOMBRE D'ENREGISTRMENTS --->
		<cfif queryInfoFicheLogiciel.RecordCount gt 0 && queryInfoFicheLogiciel.RecordCount lte 10 >
			<cfset scrollHauteur = 'style="height:auto;width:650px;border-bottom:1px solid ##b8b8b8;"'>
			<cfset hauteur = 'style="width:650px;"'>
			<cfset col = '<col></col>'>
			<cfset td = '<td style="width:9px;">&nbsp;</td>'>
			<cfset tdEmpty = ''>
		<cfelseif queryInfoFicheLogiciel.RecordCount eq 0>
			<cfset scrollHauteur = 'style="height:auto;width:650px;"'>
			<cfset hauteur = 'style="width:650px;"'>
			<cfset col = '<col ></col>'>
			<cfset td = '<td style="width:9px;">&nbsp;</td>'>
			<cfset tdEmpty = '<tr class="color1" style="height:21px;">
									<td align="center" class="" style="width:600px;" colspan="4">Aucune information</td>
									<td style="width:9px;">&nbsp;</td>
							  </tr>
			'>
		<cfelse>
			<cfset scrollHauteur = 'style="height:200px;border-bottom:1px solid ##b8b8b8;"'>
			<cfset hauteur = 'style="height:200px;"'>
			<cfset col = ''>
			<cfset td = ''>
			<cfset tdEmpty = ''>
		</cfif>
		
		
		<!--- POUR SCROLLER OU NON LE TABLEAU SELON LE NOMBRE D'ENREGISTRMENTS --->
		<cfif queryInfoFicheEquipement.RecordCount gt 0 && queryInfoFicheEquipement.RecordCount lte 8  >
			<cfset scrollHauteur2 = 'style="height:auto;width:650px;border-bottom:1px solid ##b8b8b8;"'>
			<cfset hauteur2 = 'style="width:650px;"'>
			<cfset col2 = '<col ></col>'>
			<cfset td2 = '<td></td>'>
			<cfset td2Entete = '<td class="sousTitreTableEquipement">&nbsp;</td>'>
			<cfset tdTitreEnteteStockage2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Espace de stockage</td>
					#td2Entete#
				</tr>
			'>
			<cfset tdTitreEnteteReseau2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Information réseau</td>
					#td2Entete#
				</tr>
			'>
			<cfset tdTitreEnteteSysteme2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Information système</td>
					#td2Entete#
				</tr>
			'>
			<cfset tdTitreEnteteSecurite2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Information sécurité</td>
					#td2Entete#
				</tr>
			'>
			<cfset tdEmpty2 = ''>
		<cfelseif queryInfoFicheEquipement.RecordCount eq 0>
			<cfset scrollHauteur2 = 'style="height:auto;width:650px;"'>
			<cfset hauteur2 = 'style="width:650px;"'>
			<cfset col2 = '<col ></col>'>
			<cfset td2 = ''>
			<cfset td2Entete = '<td class="sousTitreTableEquipement">&nbsp;</td>'>
			<cfset tdTitreEntete2 = ''>
			<cfset tdTitreEnteteStockage2 = ''>
			<cfset tdTitreEnteteReseau2 = ''>
			<cfset tdTitreEnteteSysteme2 = ''>
			<cfset tdTitreEnteteSecurite2 = ''>
			<cfset tdEmpty2 = '<tr class="color1" style="height:21px;">
									<td align="center" class="" style="width:601px;" colspan="2">Aucune information</td>
									<td style="width:9px;">&nbsp;</td>
								</tr>
			'>
		<cfelse>
			<cfset scrollHauteur2 = 'style="height:200px;border-bottom:1px solid ##b8b8b8;"'>
			<cfset hauteur2 = 'style="height:200px;"'>
			<cfset col2 = ''>
			<cfset td2 = ''>
			<cfset tdEntete2 = ''>
			<cfset tdTitreEnteteStockage2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Espace de stockage</td>
					#tdEntete2#
				</tr>
			'>
			<cfset tdTitreEnteteReseau2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Information réseau</td>
					#tdEntete2#
				</tr>
			'>
			<cfset tdTitreEnteteSysteme2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Information système</td>
					#tdEntete2#
				</tr>
			'>
			<cfset tdTitreEnteteSecurite2 = '
				<tr>
					<td colspan="2" style="width:680px" class="sousTitreTableEquipement">Information sécurité</td>
					#tdEntete2#
				</tr>
			'>
			<cfset tdEmpty2 = ''>
		</cfif>
		
		<!--- VERIFICATION AUTORISATION MDM --->
		<cfset MDMok = 0>
		<cfif (isDefined("SESSION.BOOL_MDM_CODEPARAMS") && SESSION.BOOL_MDM_CODEPARAMS is "MDM_N1") && 
			  (isDefined("SESSION.BOOL_MDM_RAC") && SESSION.BOOL_MDM_RAC eq 1) && boolIsManaged eq 1>
					<cfset MDMok = 1>
		</cfif>

			<!--- DEBUT DE LA DIV CONTENANT LES ONGLETS --->
			<cfset ficheTerminal &= '
					
				<div class="divOngletTerminal"> <!--- systeme_onglets --->
			        <div class="onglets" align="left" style="margin-left:5px;width:99%;">
			'>
			
			<cfif MDMok eq 1>
				<cfset ficheTerminal &= '
			            <span class="onglet_1 onglet" id="onglet_equipement" onclick="javascript:change_onglet(''equipement'');" title="Équipement">Equipement</span>
			            <span class="onglet_0 onglet" id="onglet_logiciel" onclick="javascript:change_onglet(''logiciel'');" title="Logiciel">Logiciel</span>
			            <span class="onglet_0 onglet" id="onglet_contrat_de_service" onclick="javascript:change_onglet(''contrat_de_service'');" title="Contrat de service">Contrat de service</span>
				'>
			<cfelse>
				<cfset ficheTerminal &= '
						<span class="onglet_0 onglet" id="onglet_contrat_de_service" onclick="javascript:change_onglet(''contrat_de_service'');" title="Contrat de service">Contrat de service</span>
				        <span class="onglet_0 onglet_grise" id="onglet_equipement">Equipement</span>
			            <span class="onglet_0 onglet_grise" id="onglet_logiciel">Logiciel</span>
				'>
			</cfif>
			
			<cfset ficheTerminal &= '
			        </div>
			        <div class="contenu_onglets">
			'>
			           
        	<cfif MDMok eq 1>
				<cfset ficheTerminal &= ' 
			            <div class="contenu_onglet" id="contenu_onglet_equipement">
			            	<div class="tableEquipement">
								<table cellpadding="0" cellspacing="0" width="100%">
								<thead>
								    <tr>
								    	<td>
								        	<table class="tableEnteteEquipement" cellpadding="0" cellspacing="0" >
								        		<colgroup>
								        			<col ></col><!--- width="550px" --->
								        			<col ></col><!--- width="280px" --->
								        			<col ></col><!--- width="20px" --->
								        		</colgroup>
									        	<tr>
									                <td style="" class="td1_entete_equipement"><span><span  class="" onclick="">Description</span> <span id="" onclick=""></span></span></td>
									                <td style="" class="td2_entete_equipement"><span class="" onclick="">Valeur</span></td>
									                <td style="" class="td3_entete_equipement">&nbsp;</td>
									            </tr>
								            </table>
								      	</td>
								    </tr>
								</thead>
								<tbody>
									<tr>
								    	<td>
								        <div class="scrollageEquipement" #scrollHauteur2#>
									        <table class="tableDetailEquipement" #hauteur2# cellpadding="0" cellspacing="0">
												<colgroup>
								        			<col ></col><!--- width="550px" --->
								        			<col ></col><!--- width="280px" --->
								        			#col2#
								        		</colgroup>
								        		#tdTitreEnteteStockage2#
												#tdEmpty2#
				'>
				
				
				<cfset color = "color1">
				
				<cfloop query="queryInfoFicheEquipement" >
					<cfset currentRow = queryInfoFicheEquipement.currentrow>
					
					<cfset name1 = "&nbsp;" >
					<cfset value1 = "  " >
					<cfif color eq "color1">
						<cfset color = "color2">
					<cfelseif color eq "color2">
						<cfset color = "color1">
					</cfif>
					
					<cfif NOT queryInfoFicheEquipement["NAME"][currentRow] eq "" >
						<cfset name1 = queryInfoFicheEquipement["NAME"][currentRow] >
					</cfif>
					<cfif NOT queryInfoFicheEquipement["VALUE"][currentRow] eq "" >
						<cfset value1 = queryInfoFicheEquipement["VALUE"][currentRow] >
					</cfif>
					
					<!--- SI LES VALEURS SONT IDENTIQUES / RETURN 0 OU FALSE --->
					<cfif NOT compare(queryInfoFicheEquipement.categorie,"Espace de stockage")>
						<cfset ficheTerminal &= '
												<tr class="#color#" >
													<td class="td1_corps_equipement" style="">#name1#</td>
													<td class="td2_corps_equipement" style="">#value1#</td>
													#td2#
												</tr>
						'>
					</cfif>
				</cfloop>
				
				
				<cfset ficheTerminal &= ' #tdTitreEnteteReseau2# '>
				
				<cfloop query="queryInfoFicheEquipement" >
					<cfset currentRow = queryInfoFicheEquipement.currentrow>
					
					<cfset name2 = "&nbsp;" >
					<cfset value2 = "  " >
					<cfif NOT queryInfoFicheEquipement["NAME"][currentRow] eq "" >
						<cfset name2 = queryInfoFicheEquipement["NAME"][currentRow] >
					</cfif>
					<cfif NOT queryInfoFicheEquipement["VALUE"][currentRow] eq "" >
						<cfset value2 = queryInfoFicheEquipement["VALUE"][currentRow] >
					</cfif>
					
					<cfif NOT compare(queryInfoFicheEquipement.categorie,"Information réseau")>
						<cfset ficheTerminal &= '
												<tr class="#color#">
													<td class="td1_corps_equipement" style="">#name2#</td>
													<td class="td2_corps_equipement" style="">#value2#</td>
													#td2#
												</tr>
						'>
					</cfif>
				</cfloop>
				
				
				<cfset ficheTerminal &= ' #tdTitreEnteteSysteme2# '>
				
				<cfloop query="queryInfoFicheEquipement" >
					<cfset currentRow = queryInfoFicheEquipement.currentrow>
					
					<cfset name3 = " &nbsp;" >
					<cfset value3 = "  " >
					<cfif NOT queryInfoFicheEquipement["NAME"][currentRow] eq "" >
						<cfset name3 = queryInfoFicheEquipement["NAME"][currentRow] >
					</cfif>
					<cfif NOT queryInfoFicheEquipement["VALUE"][currentRow] eq "" >
						<cfset value3 = queryInfoFicheEquipement["VALUE"][currentRow] >
					</cfif>
					<cfif NOT compare(queryInfoFicheEquipement.categorie,"Information système")>
						<cfset ficheTerminal &= '
												<tr class="#color#">
													<td class="td1_corps_equipement" style="">#name3#</td>
													<td class="td2_corps_equipement" style="">#value3#</td>
													#td2#
												</tr>
						'>
					</cfif>
				</cfloop>
				
				<cfset ficheTerminal &= ' #tdTitreEnteteSecurite2# '>
				<cfloop query="queryInfoFicheEquipement" >
					<cfset currentRow = queryInfoFicheEquipement.currentrow>
					
					<cfset name4 = "&nbsp;" >
					<cfset value4 = "  " >
					<cfif NOT queryInfoFicheEquipement["NAME"][currentRow] eq "" >
						<cfset name4 = queryInfoFicheEquipement["NAME"][currentRow] >
					</cfif>
					<cfif NOT queryInfoFicheEquipement["VALUE"][currentRow] eq "" >
						<cfset value4 = queryInfoFicheEquipement["VALUE"][currentRow] >
					</cfif>
					
					<cfif NOT compare(queryInfoFicheEquipement.categorie,"Information sécurité")>
						<cfset ficheTerminal &= '
												<tr class="#color#">
													<td class="td1_corps_equipement" style="">#name4#</td>
													<td class="td2_corps_equipement" style="">#value4#</td>
													#td2#
												</tr>
						'>
					</cfif>
				</cfloop>
				
				<cfset ficheTerminal &= '
									        </table>
								        </div>
								        </td>
								    </tr>
								</tbody>
								</table>
							</div>
			            </div>
				'>
				
				
				<cfset ficheTerminal &= '
						<div class="contenu_onglet" id="contenu_onglet_logiciel">
			                <div class="tableLogiciel"  >
								<table id="tableSortable2" class="tableMonTerminal" cellpadding="0" cellspacing="0" width="100%">
									<thead>	
									        	<tr>
													<th class="td1_entete_logiciel" title="">Nom</th>
													<th class="td2_entete_logiciel" title="">Version</th>
													<th class="td3_entete_logiciel" title="">Taille(en Bytes)</th>
													<th class="td4_entete_logiciel" title="">Auteur</th>
									            </tr>
									</thead>
									<tbody>
									
				'>
				
				<cfset color = "color1">
				<cfset nameLog = "&nbsp;" >
				<cfset version = "  " >
				<cfset size = "&nbsp;" >
				<cfset author = "  " >
				
				<cfloop query="queryInfoFicheLogiciel" >
					<cfset currentRow = queryInfoFicheLogiciel.currentrow>
					
					<cfif color eq "color1">
						<cfset color = "color2">
					<cfelseif color eq "color2">
						<cfset color = "color1">
					</cfif>
					
					<cfif NOT queryInfoFicheLogiciel["NAME"][currentRow] eq "" >
						<cfset nameLog = queryInfoFicheLogiciel["NAME"][currentRow] >
					</cfif>
					<cfif NOT queryInfoFicheLogiciel["VERSION"][currentRow] eq "" >
						<cfset version = queryInfoFicheLogiciel["VERSION"][currentRow] >
					</cfif>
					<cfif NOT queryInfoFicheLogiciel["SIZE"][currentRow] eq "" >
						<cfset size = queryInfoFicheLogiciel["SIZE"][currentRow] >
					</cfif>
					<cfif NOT queryInfoFicheLogiciel["AUTHOR"][currentRow] eq "" >
						<cfset author = queryInfoFicheLogiciel["AUTHOR"][currentRow] >
					</cfif>
			                
			          <cfset ficheTerminal &= '
			          							<tr>
													<td align="left" class="td1_corps_logiciel" style="padding-left:5px;">#nameLog#</td>
													<td align="left" class="td2_corps_logiciel" style="padding-left:5px;">#version#</td>
													<td align="right" class="td3_corps_logiciel" style="">#size#</td>
													<td align="left" class="td4_corps_logiciel" style="padding-left:5px;">#author#</td>
												</tr>
						'>
					</cfloop>
					
			        <cfset ficheTerminal &= '
									</tbody>
								</table>
							</div> 
			            </div>
			    	'>
		    </cfif>
		    
		    <!--- si c'est le seul onglet à afficher , il faut lepasser en display block --->
		    <cfif MDMok neq 1>
			    <cfset display='style="display:block;"'>
			<cfelse>
				<cfset display="">
			</cfif>
		    <cfset ficheTerminal &= '
		            <div class="contenu_onglet" #display# id="contenu_onglet_contrat_de_service">
		                <div class="tableContratService">
							<table id="tableSortable3" width="100%" border="0" cellspacing="0" cellpadding="0">
								
								<thead>
									<tr class="tableContratServiceEntete" style="">
										<th align="" class="td1_entete_contratservice" style="">Type</th>
										<th class="td2_entete_contratservice" style="">Distributeur</th>
										<th class="td3_entete_contratservice" style="">Référence</th>
										<th class="td4_entete_contratservice" style="">Date début</th>
										<th class="td5_entete_contratservice" style="">Durée (mois)</th>
										<th class="td6_entete_contratservice" style="">Echéance</th>
										<th class="td7_entete_contratservice" style="" title="Tacite reconduction">T. R.</th>
										<th class="td8_entete_contratservice" style="">Montant annuel</th>
									</tr>
								</thead>
								<tbody>
			'>
			
			<cfset type = " &nbsp; " >
			<cfset distrib = "  " >
			<cfset ref = "  " >
			<cfset dateDebut = " &nbsp; " >
			<cfset dureeMois = " &nbsp; " >
			<cfset echeance = "  " >
			<cfset tr = "  " >
			<cfset montantAnu = "  " >
			<cfset color = "color1">
			
			<cfloop query="queryInfoFicheTerminal" >
				<cfset currentRow = queryInfoFicheTerminal.currentrow>
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif NOT queryInfoFicheTerminal["TYPE_CONTRAT"][currentRow] eq "" >
					<cfset type = queryInfoFicheTerminal["TYPE_CONTRAT"][currentRow] >
				</cfif>
				<cfif NOT queryInfoFicheTerminal["NOM_FOURNISSEUR"][currentRow] eq "" >
					<cfset distrib = queryInfoFicheTerminal["NOM_FOURNISSEUR"][currentRow] >
				</cfif>
				<cfif NOT queryInfoFicheTerminal["REFERENCE_CONTRAT"][currentRow] eq "" >
					<cfset ref = queryInfoFicheTerminal["REFERENCE_CONTRAT"][currentRow] >
				</cfif>
				<cfif NOT queryInfoFicheTerminal["DATE_DEBUT"][currentRow] eq "" >
					<cfset dateDebut = queryInfoFicheTerminal["DATE_DEBUT"][currentRow] >
					<cfset dateDebut = RemoveChars(dateDebut, 11, 11) >
					<cfset dateDebut = LSDateFormat(dateDebut, "dd/mm/yyyy") >
				</cfif>
				<cfif NOT queryInfoFicheTerminal["DUREE_CONTRAT"][currentRow] eq "" >
					<cfset dureeMois = queryInfoFicheTerminal["DUREE_CONTRAT"][currentRow] >
				</cfif>
				<cfif NOT queryInfoFicheTerminal["DATE_ECHEANCE"][currentRow] eq "" >
					<cfset echeance = queryInfoFicheTerminal["DATE_ECHEANCE"][currentRow] >
					<cfset echeance = RemoveChars(echeance, 11, 11) >
					<cfset echeance = LSDateFormat(echeance, "dd/mm/yyyy") >
				</cfif>
				<cfif NOT queryInfoFicheTerminal["TACITE_RECONDUCTION"][currentRow] eq "" >
					<cfset tr = queryInfoFicheTerminal["TACITE_RECONDUCTION"][currentRow] >
				</cfif>
				<cfif NOT queryInfoFicheTerminal["MONTANT_CONTRAT"][currentRow] eq "" >
					<cfset montantAnu = queryInfoFicheTerminal["MONTANT_CONTRAT"][currentRow] >
					<cfset montantAnu = NumberFormat(montantAnu, "___.__") >
				</cfif>
				
				
				<cfset ficheTerminal &= '
										<tr class="#color#" style="">
											<td class="" style="">#type#</td>
											<td class="">#distrib#</td>
											<td class="">#ref#</td>
											<td align="right" class="">#dateDebut#</td>
											<td align="right" class="">#dureeMois#</td>
											<td align="right" class="">#echeance#</td>
											<td class="">#tr#</td>
											<td align="right" class="">#montantAnu#&nbsp;€</td>
										</tr>
				'>
			</cfloop>
			
			<cfif MDMok eq 1>
				<cfset ongletActive = "equipement">
			<cfelse>
				<cfset ongletActive = "contrat_de_service">
			</cfif>
			
			<cfset ficheTerminal &= '
								</tbody>
							</table>
						</div>
		            </div>
		        
		        </div>
		    </div>
		    
			    <script type="text/javascript">
		        //<!--
		                var anc_onglet = ''#ongletActive#'';
		                change_onglet(anc_onglet);
		        //-->
		        </script>
			'>
			
			<cfreturn ficheTerminal >
		</cffunction>
		
	</cfcomponent>