package vo.news
{
	public class NewsVo
	{
		
		private var _visible : Boolean;
		private var _position : Number;
		private var _titre :String;
		private var _detail :String;
		private var _dateParution : String;
		
		public function NewsVo()
		{
		}
		
		public function fill(news:Object):void
		{
			this._detail=news.DETAILS;
			this._visible=(news.VISIBLE == 0)?false:true;
			this._position=news.POSITION;
			this._titre=news.LABEL;
			this._dateParution=news.DATE_PARUTION ;	
		}
		
		public function get visible():Boolean
		{
			return _visible;
		}

		public function set visible(value:Boolean):void
		{
			_visible = value;
		}

		public function get position():Number
		{
			return _position;
		}

		public function set position(value:Number):void
		{
			_position = value;
		}
		
		
		[Bindable]
		public function get titre():String
		{
			return _titre;
		}

		public function set titre(value:String):void
		{
			_titre = value;
		}

		public function get detail():String
		{
			return _detail;
		}

		public function set detail(value:String):void
		{
			_detail = value;
		}

		public function get dateParution():String
		{
			return _dateParution;
		}
		public function set dateParution(value:String):void
		{
			_dateParution = value;
		}

	}
}