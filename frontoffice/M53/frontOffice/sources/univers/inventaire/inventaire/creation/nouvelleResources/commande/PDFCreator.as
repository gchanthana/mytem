package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import flash.net.URLVariables;
	import composants.util.ConsoviewUtil;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import composants.util.DateFunction;
	
	
	/**
	 * Classe permettant de sortir les PDF en rapport avec une Commande
	 * */
	public class PDFCreator
	{
		
		//le format 
		private var format : String = "PDF";
		
		//Reference vers la commande pour laquelle on affiche le PDF
		private var _commande : Commande;
		
		//Reference vers le panier pour lequel on affiche le PDF
		private var _panier : ElementCommande;
		
		/**
		 * Constructeur
		 * @param commande la commande pour laquelle on affiche le PDF
		 * @param le panier de la commande
		 * */
		public function PDFCreator(commande : Commande,panier : ElementCommande){
			_commande = commande;
			_panier = panier
		}
		
		
		/**
		 * Affiche le PDF dans une nouvelle fenetre
		 * */
		public function displayExport():void {	
					 
			var url:String = cv.NonSecureUrlBackoffice + "/fr/consotel/consoview/cfm/cycledevie/demandes/Display_Demande.cfm";
            var variables:URLVariables = new URLVariables();
            
            variables.FORMAT = format;	             
            variables.TYPE = _commande.typeCommande;
			variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			//infos commandes
			
			variables.LIBELLE = _commande.libelle;
			variables.REF_CLIENT = (_commande.refClient != null)?_commande.refClient:"---";				
			variables.REF_OPERATEUR =( _commande.refOperateur != null)?_commande.refOperateur:"---";				
			variables.CMDE_COMMENTAIRES = ( _commande.commentaire != null)?_commande.commentaire:"---";
			
			variables.DATE_EFFECTIVE = (_commande.dateEffective.length > 10)?DateFunction.formatDateAsString(new Date(_commande.dateEffective)):_commande.dateEffective;
			variables.DATE_LIVRAISON_PRE = (_commande.dateLivraisonPrevue.length > 10)?DateFunction.formatDateAsString(new Date(_commande.dateLivraisonPrevue)):_commande.dateLivraisonPrevue;
 
			
			//operateur
			variables.CONTACTID = _commande.contactID;
			variables.OPERATEURID = _commande.operateurID;				
			variables.SOCIETEID = _commande.societeID;
		
			variables.FLAG_CIBLE = _commande.flagCible;
			variables.CIBLE = _commande.idGroupeCible;
			
			if (_panier.liste != null){
				variables.LI_PRODUITS = ConsoviewUtil.extraireColonne(_panier.liste,"libelleProduit");			
				variables.LI_LIGNES = ConsoviewUtil.extraireColonne(_panier.liste,"libelleLigne");
				variables.LI_THEMES = ConsoviewUtil.extraireColonne(_panier.liste,"libelletheme");
				variables.LI_COMS = ConsoviewUtil.extraireColonne(_panier.liste,"commentaire");	
			}else{
				variables.LI_PRODUITS = [" "];
				variables.LI_LIGNES = [" "];
				variables.LI_THEMES = [" "];
				variables.LI_COMS = [" "];
			}
			
			
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            
            navigateToURL(request,"_blank");
        } 
           
	}
}