package univers.parametres.perimetres.rightSide
{
    import mx.resources.ResourceManager;
    import flash.events.Event;
    import flash.events.FocusEvent;
    import flash.events.MouseEvent;
    import mx.collections.ArrayCollection;
    import mx.containers.FormItem;
    import mx.containers.VBox;
    import mx.controls.Button;
    import mx.controls.Image;
    import mx.controls.Label;
    import mx.controls.TextInput;
    import mx.events.FlexEvent;
    import mx.formatters.DateFormatter;
    import mx.managers.PopUpManager;
    import univers.parametres.perimetres.main;

    [Bindable]
    public class BudgetEnfantImpl_V2 extends VBox
    {
        /**************************************
         *		VARIABLES PUBLIQUES
         **************************************/ // TextInput
        public var tiParentbudget:TextInput;
        public var tiRestbudget:TextInput;
        public var tiCurrentbudget:TextInput;
        public var tiChildbudget:TextInput;
        // Button
        public var btTakeRest:Button;
        public var btTakeChild:Button;
        public var btDistribute:Button;
        public var btNewExercice:Button;
        public var btEditExercice:Button;
        public var btMAJNode:Button;
        // Image
        public var btRight:Image;
        public var btLeft:Image;
        // Label	
        public var lbName:Label;
        // Date Formatter
        public var df:DateFormatter;
        // Form Item
        public var fiCurrentNode:FormItem;
        public var myBudgetService:BudgetService_V2;
        public var myBudgetUtils:BudgetUtils_V2;
        public var nodeInfos:Object;
        /**************************************
         *		VARIABLES PRIVEES
         **************************************/
        private var _mainRef:main;
        private var _refSet:Boolean = false;
        private var _resultData:ArrayCollection;
        private var _currentExercice:Number;
        private var _nodeData:Object;

        public function BudgetEnfantImpl_V2()
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
        }

        private function initIHM(evt:FlexEvent):void
        {
            myBudgetService = new BudgetService_V2(this);
            myBudgetUtils = new BudgetUtils_V2(this);
            initListener();
            initDisplay();
        }

        private function initDisplay():void
        {
            /* btTakeRest.visible = myBudgetUtils.modeEcriture;
             btTakeChild.visible = myBudgetUtils.modeEcriture; */
            btDistribute.visible = myBudgetUtils.modeEcriture;
            tiCurrentbudget.editable = myBudgetUtils.modeEcriture;
            btMAJNode.enabled = myBudgetUtils.modeEcriture;
            btNewExercice.enabled = myBudgetUtils.modeEcriture;
            btEditExercice.enabled = myBudgetUtils.modeEcriture;
        }

        private function initListener():void
        {
            // CLICK
            btNewExercice.addEventListener(MouseEvent.CLICK, callParentPopUpForNew)
            btEditExercice.addEventListener(MouseEvent.CLICK, callParentPopUpForUpdate);
            btLeft.addEventListener(MouseEvent.CLICK, onclickbtLeft);
            btRight.addEventListener(MouseEvent.CLICK, onclickbtRight);
            btTakeRest.addEventListener(MouseEvent.CLICK, onbtTakeRestClicked);
            btTakeChild.addEventListener(MouseEvent.CLICK, onbtTakeChildClicked);
            btMAJNode.addEventListener(MouseEvent.CLICK, onclickbtMAJNode);
            btDistribute.addEventListener(MouseEvent.CLICK, myBudgetService.btDistributeClicked);
            // FOCUS
            tiCurrentbudget.addEventListener(FocusEvent.FOCUS_IN, myBudgetUtils.disableFormatTxt);
            tiCurrentbudget.addEventListener(FocusEvent.FOCUS_OUT, myBudgetUtils.enableFormatTxt);
            // CHANGE
            tiCurrentbudget.addEventListener(Event.CHANGE, onCurrentBudgetChange);
        }

        private function callParentPopUpForNew(me:MouseEvent):void
        {
            var myBudgetParent:BudgetParentIHM_V2 = new BudgetParentIHM_V2();
            myBudgetParent.newExercice = true;
            myBudgetParent.title = ResourceManager.getInstance().getString('M22', 'Nouvel_exercice_pour_le_compte___') + CvAccessManager.getSession().
                CURRENT_PERIMETRE.RACINE_LIBELLE;
            myBudgetParent.enfant = this;
            PopUpManager.addPopUp(myBudgetParent, this, true);
            PopUpManager.centerPopUp(myBudgetParent);
        }

        private function callParentPopUpForUpdate(me:MouseEvent):void
        {
            var myBudgetParent:BudgetParentIHM_V2 = new BudgetParentIHM_V2();
            myBudgetParent.newExercice = false;
            myBudgetParent.title = ResourceManager.getInstance().getString('M22', 'Mis___jour_de_l_exercice___') + resultData[currentExercice].LIBELLE_EXERCICE;
            myBudgetParent.enfant = this;
            PopUpManager.addPopUp(myBudgetParent, this, true);
            PopUpManager.centerPopUp(myBudgetParent);
        }

        private function onclickbtMAJNode(event:MouseEvent):void
        {
            myBudgetService.updateNodeBudget();
        }

        private function onbtTakeRestClicked(event:MouseEvent):void
        {
            try
            {
                tiCurrentbudget.text = myBudgetUtils.formatAmount(myBudgetUtils.getAroundValue(0, "MONTANTBUDGET") - myBudgetUtils.getAroundValue(1, "MONTANTBUDGET"));
            }
            catch(error:Error)
            {
            }
            myBudgetUtils.refreshTxtValues();
            btMAJNode.enabled = true;
            //updateNodeBudget();
        }

        private function onbtTakeChildClicked(event:MouseEvent):void
        {
            try
            {
                tiCurrentbudget.text = myBudgetUtils.formatAmount(myBudgetUtils.getAroundValue(3, "MONTANTBUDGET"));
            }
            catch(error:Error)
            {
            }
            myBudgetUtils.refreshTxtValues();
            btMAJNode.enabled = true;
            //updateNodeBudget();
        }

        private function onclickbtLeft(event:MouseEvent):void
        {
            if(currentExercice > 0)
            {
                currentExercice--;
            }
            myBudgetUtils.refreshExerciceView();
        }

        private function onclickbtRight(event:MouseEvent):void
        {
            if(currentExercice < resultData.length - 1)
            {
                currentExercice++;
            }
            myBudgetUtils.refreshExerciceView();
        }

        private function onCurrentBudgetChange(event:Event):void
        {
            btMAJNode.enabled = true;
            myBudgetUtils.refreshTxtValues();
        }

        public function setMainRef(ref:main):void
        {
            mainRef = ref;
            if(mainRef != null)
            {
                refSet = true;
                myBudgetService.getExerciceData();
            }
            else
            {
                refSet = false;
            }
            invalidateProperties();
        }

        public function mainOrganisationChanged():void
        {
            myBudgetService.getNodeBudget();
        }

        public function mainTreeSelectionChanged():void
        {
            if(_mainRef.getSelectedNode())
            {
                nodeInfos = mainRef.getSelectedItemNode();
                myBudgetService.getNodeBudget();
            }
            else
            {
                myBudgetUtils.refreshBudgetItems();
                nodeInfos = null;
            }
        }

        /**************************************
         *			GETTER / SETTER
         **************************************/
        public function set mainRef(value:main):void
        {
            _mainRef = value;
        }

        public function get mainRef():main
        {
            return _mainRef;
        }

        public function set refSet(value:Boolean):void
        {
            _refSet = value;
        }

        public function get refSet():Boolean
        {
            return _refSet;
        }

        public function set resultData(value:ArrayCollection):void
        {
            _resultData = value;
        }

        public function get resultData():ArrayCollection
        {
            return _resultData;
        }

        public function set currentExercice(value:Number):void
        {
            _currentExercice = value;
        }

        public function get currentExercice():Number
        {
            return _currentExercice;
        }

        public function set nodeData(value:Object):void
        {
            _nodeData = value;
        }

        public function get nodeData():Object
        {
            return _nodeData;
        }
    }
}