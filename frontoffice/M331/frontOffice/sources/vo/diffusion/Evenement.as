package vo.diffusion
{
	public class Evenement
	{
		
		private var _idEvent :int;
		private var _nomEvent:String;

		public function Evenement()
		{
		}

		public function fill(unEvent:Object):void
		{
			this._idEvent=unEvent.IDEVENT ;
			this._nomEvent=unEvent.NOMEVENT;
		}
		
		public function get idEvent():int
		{
			return _idEvent;
		}
		
		public function set idEvent(value:int):void
		{
			_idEvent = value;
		}
		
		public function get nomEvent():String
		{
			return _nomEvent;
		}
		
		public function set nomEvent(value:String):void
		{
			_nomEvent = value;
		}

	}
}