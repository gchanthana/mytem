package univers.facturation.suivi.factures.supprimerfactures
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import univers.facturation.suivi.controles.controls.ControleFacture;
	import univers.facturation.suivi.controles.vo.ParamsRecherche;
	
	[Bindable]
	public class AfficheFacture extends ControleFacture
	{
		public function AfficheFacture()  
		{
			//TODO: implement function
			super();
		}
		
		public function set paramsRecherches(p : ParamsRecherche):void{
			_paramsRecherche = p;
	    }
	    public function get paramsRecherches():ParamsRecherche{
	    	return _paramsRecherche;
	    }
		
		override protected function exporterListeFacturesCSV(bool:int):void{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																"fr.consotel.consoview.facturation.suiviFacturation.supprimerFacture.FactureGateWay",
																"exporterListeFacturesSupprimablesCSV",
																exporterListeFacturesCSVResultHandler);
			RemoteObjectUtil.callService(opData,_paramsRecherche);
		}
		
	}
}