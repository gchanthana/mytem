<cfcomponent displayname="Mailing" output="false">

	<cffunction name="setMailToDatabase" access="public" returntype="Numeric" output="true">
	    <cfargument name="idusercv" 	required="true" 	type="Numeric"/>
 	    <cfargument name="userlogin"	required="true" 	type="String"/>
	    <cfargument name="mailuuid"		required="true" 	type="String"/>
	    <cfargument name="module"		required="true" 	type="String"/>
	    <cfargument name="maitag"		required="true" 	type="String"/>
		<cfargument name="mailfrom" 	required="true" 	type="String"/>
		<cfargument name="mailto" 		required="true" 	type="String"/>
		<cfargument name="mailsubject"	required="true" 	type="String"/>
		<cfargument name="mailbcc"		required="true" 	type="String"/>
		<cfargument name="mailcorps"	required="true" 	type="String"/>
		<cfargument name="codeApp"		required="false" 	type="Numeric" default="1"/>
				
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.setmail_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_iduser" 			value="#idusercv#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_userlogin" 		value="#userlogin#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_uuid_mail" 		value="#mailuuid#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_module" 			value="#module#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_from_mail" 		value="#mailfrom#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_to_mail" 		value="#mailto#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_subject_mail" 	value="#mailsubject#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_bcc_mail" 		value="#mailbcc#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_coprs_mail" 		value="#mailcorps#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_date_mail" 		value="#LSDATEFORMAT(Now(),'YYYY/MM/DD')#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_tag" 			value="#maitag#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_create" 			value="0">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_code_appli" 		value="#codeApp#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="updateMailToDatabase" access="public" returntype="Numeric" output="true">
	    <cfargument name="updateCreate" 	required="true" 	type="Numeric"/>
	    <cfargument name="updateuuid" 		required="true" 	type="String"/>
		<cfargument name="filetag" 			required="true" 	type="String"/>
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.updatemail">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_create" 	  value="#updateCreate#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_uuid_mail" value="#updateuuid#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_tag" 	  value="#filetag#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="updateMailInfos" access="public" returntype="Numeric" output="true">
	    <cfargument name="updateuuid" 	required="true" 	type="String"/>
	   	<cfargument name="filetag" 		required="true" 	type="String"/>
	   	<cfargument name="iscreate" 	required="true" 	type="Numeric"/>
	    <cfargument name="tomail" 		required="true" 	type="String"/>
	    <cfargument name="bccmail" 		required="true" 	type="String"/>
		<cfargument name="subjectmail" 	required="true" 	type="String"/>
		<cfargument name="corpsmail" 	required="true" 	type="String"/>
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.updatemail_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_create" 	value="#iscreate#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_to" 	 	value="#tomail#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_bcc" 	value="#bccmail#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_subject" value="#subjectmail#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_body" 	value="#corpsmail#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_uuid" 	value="#updateuuid#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_tag" 	value="#filetag#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getMailToDatabase" access="public" returntype="Query" output="true">
	    <cfargument name="uuidmail" 	required="true" 	type="String"/>
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.getmail">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_uuid_mail" value="#uuidmail#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="add_appli_property" access="public" returntype="Numeric" output="true">
	    <cfargument name="codeapp" 		required="true" 	type="Numeric"/>
	    <cfargument name="key" 			required="true" 	type="String"/>
	    <cfargument name="value" 		required="true" 	type="String"/>
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.add_appli_property">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_code_appli" 	value="#codeapp#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_key" 		value="#key#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_value" 		value="#value#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour>
	</cffunction>
	
	<!--- procédure pour avoir la liste des propriétes d'une application, si key null, on retourne toutes les valeurs de cette application --->
	
	<cffunction name="get_appli_properties" access="public" returntype="Query" output="true">
	    <cfargument name="codeapp" 		required="true" 	type="Numeric"/>
	    <cfargument name="key" 			required="true" 	type="String"/>
			
			<cfset bool = 'yes'>
		
			<cfif key NEQ 'NULL'>
				<cfset bool = 'no'>
			</cfif>
		
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.get_appli_properties">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_code_appli" 	value="#codeapp#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_key" 		value="#key#" null="#bool#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour>
	</cffunction>

</cfcomponent>