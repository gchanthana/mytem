<cfinterface author="Cedric" displayname="fr.saaswedo.api.remoting.IRemoteClient"
hint="Interface d'un client permettant l'accès à des services distants (e.g WebService,HTTP,etc...) et qui se base sur les entités suivantes :
<br>- Service : Entité qui identifie la destination et les autres infos qui la concernent (e.g Credentiels).
<br>- Appel de service : Entité qui effectue l'accès à la destination et aux fonctionnalités distantes (e.g Axis Call/Stub,CFHTTP,etc...).
<br><br>Principe d'utilisation :
<br>- Un service est enregistré avec register() qui retourne un identifiant pour la destination (endpoint) et les crédentiels fournis.
<br>- L'identifiant obtenu est fourni à getServiceCall() qui retourne l'implémentation d'appel pour le service correspondant.
<br>- L'implémentation appel de service obtenu est fourni à invoke() pour effectuer l'accès au service">
	<cffunction name="getServiceCall" returntype="Any" hint="Retourne l'implémentation d'appel de service (e.g Axis Call,CFHTTP,etc...)">
		<cfargument name="serviceId" type="String" required="true" hint="Identifiant du service obtenu avec register()">
	</cffunction>

	<cffunction name="invoke" returntype="Any" hint="Effectue un appel de service avec serviceCall et retourne le résultat de l'appel">
		<cfargument name="serviceCall" type="Any" required="true" hint="Implémentation d'appel de service obtenu avec getServiceCall()">
		<cfargument name="method" type="String" required="true" hint="Procédure du service">
		<cfargument name="parameters" type="Array" required="true" hint="Paramètres de l'appel. Un tableau vide signifie pas de paramètres">
	</cffunction>

	<cffunction name="register" returntype="String" hint="Enregistre le service correspondant
	aux paramètres fournis (endpoint,username). Retourne une valeur qui identifie les paramètres fournis et qui permet d'appeler le service
	correspondant avec invoke(). Toute exception capturée est passée à dispatchError() mais pas à nouveau levée et une chaine vide est retournée">
		<cfargument name="endpoint" type="String" required="true" hint="Endpoint d'un service">
		<cfargument name="username" type="String" required="false" default="" hint="Login d'accès au service">
		<cfargument name="password" type="String" required="false" default="" hint="Password d'accès au service">
	</cffunction>
	
	<cffunction name="getService" returntype="Any" hint="Retourne le service identifié par serviceId.
	L'instance retournée sert à encapsuler les infos concernant un service et ne doit pas etre utilisée pour effectuer les appels au service">
		<cfargument name="serviceId" type="String" required="true" hint="Identifiant d'un service obtenu avec register()">
	</cffunction>
</cfinterface>