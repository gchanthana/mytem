package gestionparc.entity
{

	[Bindable]
	public class Colonne
	{
		private var _LIBELLE:String;
		private var _DATAFIELD:String=null;
		private var _ITEMRENDERER:Class;
		private var _WIDTH:int=-1;
		private var _VISIBLE:Boolean;
		private var _CODE : String;
		private var _ID : Number;
		private var _LABEL_FUNCTION:Function=null;
		private var _DEFAUT:int;

		public function Colonne()
		{
		}
		
		/**
		 * permet de creer un objet de type colonne
		 * on initilaise quelles propriétés par cette methode  et les autres sont initialisées par le methode  createColonne de la class AbstractVue
		 */		
		public function createObject(obj: Object):void
		{
			this._CODE=obj.CODE;
			this._DEFAUT=obj.IS_DEFAULT;
			this._LIBELLE=obj.LIBELLE ;
			this._ID=obj.IDGP_LIST_COLUMN;
			this._DATAFIELD=obj.DATAFIELD ;
			(obj.VISIBLE==1) ? this._VISIBLE=true : this._VISIBLE=false;
			//la colonne Libelle pool (code = col_31) n'est visible que pour les deux vues Parc global et Tout le parc (idpool= 0 et idpool= -10)
			if(_CODE == 'col_31')
				if(SpecificationVO.getInstance().idPoolNotValid > 0)
					this._VISIBLE = false;
		}
		public function get LIBELLE():String
		{
			return _LIBELLE;
		}

		public function set LIBELLE(value:String):void
		{
			_LIBELLE=value;
		}

		public function get DATAFIELD():String
		{
			return _DATAFIELD;
		}

		public function set DATAFIELD(value:String):void
		{
			_DATAFIELD=value;
		}

		public function get ITEMRENDERER():Class
		{
			return _ITEMRENDERER;
		}

		public function set ITEMRENDERER(value:Class):void
		{
			_ITEMRENDERER=value;
		}

		public function get WIDTH():int
		{
			return _WIDTH;
		}

		public function set WIDTH(value:int):void
		{
			_WIDTH=value;
		}

		public function get LABEL_FUNCTION():Function
		{
			return _LABEL_FUNCTION;
		}

		public function set LABEL_FUNCTION(value:Function):void
		{
			_LABEL_FUNCTION=value;
		}
		public function get VISIBLE():Boolean
		{
			return _VISIBLE;
		}
		
		public function set VISIBLE(value:Boolean):void
		{
			_VISIBLE = value;
		}
		
		public function get ID():Number
		{
			return _ID;
		}
		
		public function set ID(value:Number):void
		{
			_ID = value;
		}
		
		public function get CODE():String
		{
			return _CODE;
		}
		
		public function set CODE(value:String):void
		{
			_CODE = value;
		}
		
		public function get DEFAUT():int
		{
			return _DEFAUT;
		}
		
		public function set DEFAUT(value:int):void
		{
			_DEFAUT = value;
		}
	}
}
