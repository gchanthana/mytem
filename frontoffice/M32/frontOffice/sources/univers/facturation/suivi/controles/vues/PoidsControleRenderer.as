package univers.facturation.suivi.controles.vues {

    // myComponents/CellField.as
    import mx.resources.ResourceManager;
	import composants.util.ConsoviewFormatter;
    
    import mx.controls.*;
    
    import univers.facturation.suivi.controles.vo.Facture;   
   

    public class PoidsControleRenderer extends TextInput
    {
        // Define the constructor and set properties.
        public function PoidsControleRenderer() {
            super();           
            setStyle("borderStyle", "none");
            setStyle("backgroundAlpha","0");
            editable=false;
        }

        // Override the set method for the data property.
        override public function set data(value:Object):void {
            super.data = value;
            text 	= "0";
            toolTip = "";
            if (value != null)
            {
                if (Facture(value).montant != 0)
				{text = ConsoviewFormatter.formatNumber(Facture(value).montantCalculeFacture/Facture(value).montant*100,2);
				}
                toolTip = ResourceManager.getInstance().getString('M32', '__contr_l____') + text.toString() + "%";
            }
            super.invalidateDisplayList();
        }
    }  
}

