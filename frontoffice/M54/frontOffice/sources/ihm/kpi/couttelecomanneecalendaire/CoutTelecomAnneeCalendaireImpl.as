package ihm.kpi.couttelecomanneecalendaire
{
	import event.selector.MonoPeriodeEvent;
	
	import ihm.kpi.kpiabstract.KpiAbstract;

	
	public class CoutTelecomAnneeCalendaireImpl extends KpiAbstract
	{
		public function CoutTelecomAnneeCalendaireImpl()
		{
			super();
		}

		
		override protected function createChildren():void
		{
			super.createChildren();
		}
		
		
		
		override public function fillDataPeriode(e:MonoPeriodeEvent):void
		{
			selectorPeriod.periodes=_monoPeriodeSelectorService.model.getValuesMonoPeriode;
			selectorPeriod.selectedLibelleMois=commun.getLibellePeriode(selectorPeriod.defaultValue);
			//showHideSelector(lblPeriode,choixPeriode,selectorPeriod.periodes);
			_monoPeriodeSelectorService.model.removeEventListener(MonoPeriodeEvent.USER_MONO_PERIODE_SELECTOR_EVENT, fillDataPeriode);
		}
		
	}
}