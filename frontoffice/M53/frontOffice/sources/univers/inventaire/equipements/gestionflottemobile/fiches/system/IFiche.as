package univers.inventaire.equipements.gestionflottemobile.fiches.system
{
	public interface IFiche
	{
		function getDate():void;
		function updateDate():void;
		function set tmpData(value:Object):void;
		function get tmpData():Object;
	}
}