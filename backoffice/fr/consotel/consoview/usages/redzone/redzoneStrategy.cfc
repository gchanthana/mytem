<!--- =========================================================================
Classe: redzoneStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="redzoneStrategy" >

	<cffunction name="getSites" access="public" returntype="query" output="false"  >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
	        <cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getAppelsBySite" access="public" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getAppelsByRefClient" access="public" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getAppelsByGroupe" access="public" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getAppelsByVille" access="public" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getAppelsByZip" access="public" returntype="query" output="false"  >
		<cfargument name="data" required="false" type="array" default="" displayname="array data" hint="Initial value for the dataproperty." />
		<cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="dirquery2treequery" access="public" returntype="query" output="false"  >
		<cfargument name="dquery" required="false" type="query" default="" displayname="query dquery" hint="Initial value for the dqueryproperty." />
		<cfset var tquery=QueryNew("display,value,parent,societe")>
		<cfset var tempPath=""> 
		<cfset var i=0> 

		<!--- Ajout de la racine --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.libelle_groupe_client[1])>
		<cfset QuerySetCell(tquery,"value",0)>
		<cfset QuerySetCell(tquery,"parent",dquery.libelle_groupe_client[1])>
		
		<!--- Ajout societe initiale --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.libelle[1])>
		<cfset QuerySetCell(tquery,"value",dquery.idref_client[1])>
		<cfset QuerySetCell(tquery,"parent",0)>
		
		<!--- Ajout du Departement --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.dep[1])>
		<cfset QuerySetCell(tquery,"value",dquery.zipkey[1])>
		<cfset QuerySetCell(tquery,"parent",dquery.idref_client[1])>
				
		<!--- Ajout de la commune --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.commune[1])>
		<cfset QuerySetCell(tquery,"value",dquery.communekey[1])>
		<cfset QuerySetCell(tquery,"parent",dquery.zipkey[1])>
		<!--- Ajout du Site --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.adresse[1])>
		<cfset QuerySetCell(tquery,"value",dquery.siteID[1])>
		<cfset QuerySetCell(tquery,"parent",dquery.communekey[1])>
		<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
		
		<cfset cntj=2>
		<cfloop from="2" to="#dquery.RecordCount#" index="i">
			<cfif dquery.libelle[cntj] neq dquery.libelle[cntj-1]>
				<!--- Ajout des societes --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.libelle[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.idref_client[cntj])>
				<cfset QuerySetCell(tquery,"parent",0)>
				<!--- Ajout du Departement --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.dep[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.zipkey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.idref_client[cntj])>
				<!--- Ajout de la commune --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.commune[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.zipkey[cntj])>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			<cfelseif dquery.dep[cntj] neq dquery.dep[cntj-1]>
				<!--- Ajout du Departement --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.dep[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.zipkey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.idref_client[cntj])>
				<!--- Ajout de la commune --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.commune[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.zipkey[cntj])>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			<cfelseif dquery.commune[cntj] neq dquery.commune[cntj-1]>
				<!--- Ajout de la commune --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.commune[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.zipkey[cntj])>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			<cfelse>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			</cfif>
		</cfloop>
		<!--- And return the new query --->
		<cfreturn tquery>
	</cffunction>
</cfcomponent>