package appli.impersonnification
{
	import appli.app.AppCV4;
	import appli.app.AppConsoleGestion;
	import appli.app.AppMYTEM360;
	import appli.app.AppMytem;
	import appli.app.AppPilotage;
	
	import flash.events.Event;
	
/**
 * Classe dédiée au refresh en cas d'accès à un autre compte
 */
	public class refreshManager
	{
		public static var chosenAppCG:AppConsoleGestion = null;
		public static var chosenAppCV4:AppCV4 = null;
		public static var chosenAppMytem:AppMytem = null;
		public static var chosenAppMytem360:AppMYTEM360 = null;
		public static var chosenAppPilotage:AppPilotage = null;
		public static var impersonnification:Boolean = false;//indique si on est loggé sur un autre compte ou pas.
		public static var previousAccountName:String = "";//nom du compte de départ => il ne peut s'agir que d'un compte saaswedo
		public static var currentAccountName:String = "";//nom du compte utilisé à cet instant
		public static var isReloading:Boolean = false;//sert à demander au créateur de header de nous envoyer sur l'accueil.
		public static var failedRelogging:Boolean = false;//sert à bloquer la fonction login en cas d'échec
		
		public function refreshManager()
		{
		}
		
		public static function getImpersString():String
		{
			if (previousAccountName != currentAccountName)
				return previousAccountName + " as " + currentAccountName;
			else
			{
				impersonnification = false;
				return currentAccountName;
			}	
		}
		
		public static function refreshApp():void
		{
			if(chosenAppCG != null)
			{
				chosenAppCG.displayIHM();
			}
			else if (chosenAppCV4 != null) 
			{
				trace ("refresh de CV4");
				chosenAppCV4.displayIHM();
			}
			else if (chosenAppMytem != null) 
			{
				chosenAppMytem.displayIHM();
			}
			else if (chosenAppMytem360 != null)
			{
				chosenAppMytem360.displayIHM();
			}
			else if (chosenAppPilotage != null)
			{
				chosenAppPilotage.displayIHM();
			}
			else
			{
				trace("chosenApp est null");
			}
		}
		
		public static function setChosenAppCG(value:AppConsoleGestion):void
		{
			chosenAppCG = value;
			chosenAppCV4 = null;
			chosenAppMytem = null;
			chosenAppMytem360 = null;
		}
		public static function setChosenAppCV4(value:AppCV4):void
		{
			chosenAppCG = null;
			chosenAppCV4 = value;
			chosenAppMytem = null;
			chosenAppMytem360 = null;
		}
		public static function setChosenAppMytem(value:AppMytem):void
		{
			chosenAppCG = null;
			chosenAppCV4 = null;
			chosenAppMytem = value;
			chosenAppMytem360 = null;
		}
		public static function setChosenAppMytem360(value:AppMYTEM360):void
		{
			chosenAppCG = null;
			chosenAppCV4 = null;
			chosenAppMytem = null;
			chosenAppMytem360 = value;
		}
		public static function setChosenAppPilotage(value:AppPilotage):void
		{
			chosenAppCG = null;
			chosenAppCV4 = null;
			chosenAppMytem = null;
			chosenAppMytem360 = null;
			chosenAppPilotage = value;
		}
	}
}