package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.mdm
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class RuleMenuAction implements IRuleDisplayItem
	{
		public function RuleMenuAction()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new RuleVerrouillerTerminal();
			var r2:IRuleDisplayItem=new RuleGererTerminal();

			if (r1.isDisplay() || r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
