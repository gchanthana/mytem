package univers.facturation.grilletarif.renderer
{
	import mx.controls.dataGridClasses.DataGridItemRenderer;

	public class CustomHeaderRenderer extends DataGridItemRenderer
	{
		public function CustomHeaderRenderer()
		{
			super();
			setStyle("textAlign","center");
		}
		     
	}
}