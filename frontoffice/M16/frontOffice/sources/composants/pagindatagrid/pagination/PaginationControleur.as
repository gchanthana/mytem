package composants.pagindatagrid.pagination
{
	import composants.pagindatagrid.pagination.event.PaginationEvent;
	import composants.pagindatagrid.pagination.vo.ItemIntervalleVO;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.controls.VRule;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	[Bindable]
	public class PaginationControleur extends HBox
	{
		public var colIntervalle : ArrayCollection;
		public var myIndex : int = 0;
		
		//PUBLIC UI composnant
		public var comboPaginate : ComboBox;
		
		public function PaginationControleur() 
		{
			
		}
		public function creation_complete_handler(evt : FlexEvent):void
		{
			
		}
		public function initPagination(nbTotalItem : int,nbItemParPage : int):void
		{
			var nbTotalPage : int = Math.ceil(nbTotalItem / nbItemParPage);
			var nextDepIntervalle :int = 1;
			var intervalleHaut : int;
			
			if(colIntervalle == null)
				colIntervalle = new ArrayCollection();
			else
				colIntervalle.removeAll();
			
			for(var i:int=1;i<=nbTotalItem;i++)
			{
				if(nextDepIntervalle == i)
				{
					if((i+nbItemParPage)>nbTotalItem)
					{
						intervalleHaut = nbItemParPage = i+(nbTotalItem - i);//Dernier Intervalle
					}
					else
					{
						intervalleHaut = i+nbItemParPage-1; // intervalle normal
					}
					colIntervalle.addItem(itemFactory(i+'-'+intervalleHaut+ ' ' +ResourceManager.getInstance().getString('M16', 'sur') + ' ' +nbTotalItem,i-1,(intervalleHaut-i+1)));
					nextDepIntervalle = i+nbItemParPage;
				}
			}
			
			colIntervalle.refresh();
			
			comboPaginate.selectedIndex = myIndex;
		}
		public function itemFactory(label:String,indexDepart : int, taille : int):ItemIntervalleVO
		{
			var obj : ItemIntervalleVO= new ItemIntervalleVO();
			obj.indexDepart = indexDepart;
			obj.tailleIntervalle = taille;
			obj.libelle = label;
			return obj;
		}
		public function vRulFactory():VRule
		{
			var vr : VRule = new VRule();
			vr.height = 12;
			vr.width = 1;
			return vr;
		}
		public function setSelectedIndexOfComboPaginate(sens : int):void 
		{
			comboPaginate.selectedIndex = comboPaginate.selectedIndex + sens;
			comboPaginateHandler();
		}
		public function selectFirstIntervalle():void
		{
			comboPaginate.selectedIndex = 0;
			comboPaginateHandler();
		}
		public function selectLastIntervalle():void
		{
			comboPaginate.selectedIndex = (comboPaginate.dataProvider as ArrayCollection).length-1;
			comboPaginateHandler();
		}
		public function comboPaginateHandler(evt : Event=null):void
		{
			if(comboPaginate.selectedIndex==-1)
			{
				comboPaginate.selectedIndex = 0;
			}
			myIndex = comboPaginate.selectedIndex;
			dispatchEvent(new PaginationEvent(PaginationEvent.INTERVALLE_SELECTED_CHANGE,comboPaginate.selectedItem as ItemIntervalleVO,true));
		}
	}
}