﻿<cfcomponent>
	
	<cffunction access="remote" name="getValuesMonoPeriode"  output="false" returntype="query">		
		
			<cfset LOCAL.dateDebut = lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,'dd/mm/yyyy')>
			<cfset LOCAL.dateFin = lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN,'dd/mm/yyyy')>
			<cfset LOCAL.maxMonthToDisplay = SESSION.PERIMETRE.STRUCTDATE.PERIODS.MAX_MONTH_TO_DISPLAY>
			<cfset LOCAL.idLang = SESSION.USER.IDGLOBALIZATION>
			
			<cfif LOCAL.idLang eq 0>
				<cfset LOCAL.idLang = 3>
			</cfif>
		
			<cfstoredproc datasource="ROCOFFRE"  procedure="PKG_M331.get_MonoPeriodeSelector">		
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#LOCAL.dateDebut#" variable="p_datedeb">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#LOCAL.dateFin#" variable="p_datefin">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#LOCAL.maxMonthToDisplay#" variable="p_nb_mois_max">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#LOCAL.idLang#" variable="p_langid">
				<cfprocresult name="qMonoPeriode">
			</cfstoredproc>
			
	        <cfreturn qMonoPeriode>			
	</cffunction>
	
</cfcomponent>