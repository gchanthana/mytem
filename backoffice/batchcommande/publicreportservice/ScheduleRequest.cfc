<cfcomponent name="ScheduleRequest" displayname="ScheduleRequest">

	<cfset variables.biServer="http://bip-prod.consotel.fr/xmlpserver/services/PublicReportService?wsdl">
		
	<cfobject name="deliveryRequest" type="component" component="batchcommande.publicreportservice.DeliveryRequest">
	<cfobject name="reportRequest" type="component" component="batchcommande.publicreportservice.ReportRequest">
	<cfset variables.ScheduleRequest=structNew()>
	<cfset variables.ScheduleReportRequest=structNew()>
	<!--- <cfset variables.ScheduleRequest.cronExpression="">
	<cfset variables.ScheduleRequest.jobCalendar="Gregorian">
	<cfset variables.ScheduleRequest.useUTF8Option=true> --->
	
	<cffunction name="setReportRequest" access="public" returntype="void">
		<cfargument name="reportAbsolutePath" type="String" required="true">
		<cfargument name="attributeTemplate" type="String" required="true">
		<cfargument name="attributeFormat" type="String" required="true">
		<cfargument name="attributeLocale" type="String" required="false" default="fr-FR">
		<cfset variables.ScheduleRequest.jobLocale=attributeLocale>
		<cfset reportRequest.setReportRequest(reportAbsolutePath,attributeTemplate,attributeFormat,attributeLocale)>
		<cfset structInsert(variables.ScheduleRequest,"reportRequest",reportRequest.getReportRequest(),true)>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="AddParameter" access="remote" returntype="void">
		<cfargument name="paramName" type="string" required="true">
		<cfargument name="paramValues" type="string" required="true">
		<cfset reportRequest.AddParameter(paramName,paramValues)>
		<cfset structInsert(variables.ScheduleRequest,"reportRequest",reportRequest.getReportRequest(),true)>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setScheduleRequest" access="public" returntype="void">
		<cfargument name="userID" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		<cfargument name="userJobName" type="String" required="false" default="#createUUID()#">
		<cfset variables.ScheduleReportRequest.userID=userID>
		<cfset variables.ScheduleReportRequest.password=password>
		<cfset variables.ScheduleRequest.userJobName=userJobName>
		<cfset variables.ScheduleRequest.saveDataOption=true>
		<cfset variables.ScheduleRequest.saveOutputOption=true>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setNotificationScheduleRequest" access="public" returntype="void">
		<cfargument name="notificationTo" type="String" required="true" default="false">
		<cfargument name="notifyWhenFailed" type="boolean" required="true" default="false">
		<cfargument name="notifyWhenSuccess" type="boolean" required="true" default="false">
		<cfargument name="notifyWhenWarning" type="boolean" required="true" default="false">
		<cfset structInsert(ScheduleRequest,"notificationTo",notificationTo,true)>
		<cfset structInsert(ScheduleRequest,"notifyWhenFailed",notifyWhenFailed,true)>
		<cfset structInsert(ScheduleRequest,"notifyWhenSuccess",notifyWhenSuccess,true)>
		<cfset structInsert(ScheduleRequest,"notifyWhenWarning",notifyWhenWarning,true)>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setTimeScheduleRequest" access="public" returntype="void">
		<cfargument name="startDate" type="String" required="false">
		<cfargument name="endDate" type="string" required="false">
		<cfargument name="repeatCount" type="numeric" required="false">
		<cfargument name="repeatInterval" type="numeric" required="false">
		<cfset variables.ScheduleRequest.endDate=endDate>
		<cfset variables.ScheduleRequest.repeatCount=repeatCount>
		<cfset variables.ScheduleRequest.repeatInterval=repeatInterval>
		<cfset variables.ScheduleRequest.startDate=startDate>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setBurstScheduleRequest" access="public" returntype="void">
		<cfargument name="scheduleBurstringOption" type="boolean" required="true" default="false">
		<cfset variables.ScheduleRequest.scheduleBurstringOption=scheduleBurstringOption>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setEmailDeliveryRequest" access="public" returntype="void">
		<cfargument name="emailTo" type="String" required="true" default="">
		<cfargument name="emailCC" type="String" required="false" default="">
		<cfargument name="emailBCC" type="String" required="false" default="">
		<cfargument name="emailReplyTo" type="String" required="true" default="production@consotel.fr">
		<cfargument name="emailFrom" type="String" required="false" default="production@consotel.fr">
		<cfargument name="emailBody" type="String" required="false" default="Rapport CONSOTEL">
		<cfargument name="emailSubject" type="String" required="false" default="Rapport CONSOTEL">
		<cfset deliveryRequest.setEmailDeliveryRequest(emailBCC,emailBody,emailCC,emailFrom,emailReplyTo,emailSubject,emailTo)>
		<cfset structInsert(variables.ScheduleRequest,"deliveryRequest",deliveryRequest.getDeliveryRequest(),true)>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setFaxDeliveryRequest" access="public" returntype="void">
		<cfargument name="faxNumber" type="String" required="false" default="">
		<cfargument name="faxServer" type="String" required="false" default="">
		<cfset deliveryRequest.setFaxDeliveryRequest(faxNumber,faxServer)>
		<cfset structInsert(variables.ScheduleRequest,"deliveryRequest",deliveryRequest.getDeliveryRequest(),true)>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setFtpDeliveryRequest" access="public" returntype="void">
		<cfargument name="ftpServerName" type="String" required="true">
		<cfargument name="ftpUserName" type="String" required="true">
		<cfargument name="ftpUserPassword" type="String" required="true">
		<cfargument name="remoteFile" type="String" required="true">
		<cfargument name="sftpOption" type="boolean" required="false" default="false">
		<cfset deliveryRequest.setFtpDeliveryRequest(ftpServerName,ftpUserName,ftpUserPassword,remoteFile,sftpOption)>
		<cfset structInsert(variables.ScheduleRequest,"deliveryRequest",deliveryRequest.getDeliveryRequest(),true)>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="setLocalDeliveryRequest" access="public" returntype="void">
		<cfargument name="destination" type="String" required="true">
		<cfset deliveryRequest.setLocalDeliveryRequest(destination)>
		<cfset structInsert(variables.ScheduleRequest,"deliveryRequest",deliveryRequest.getDeliveryRequest(),true)>
		<cfset structInsert(variables.ScheduleReportRequest,"scheduleRequest",variables.ScheduleRequest,true)>
	</cffunction>
	
	<cffunction name="scheduleReport" access="remote" returntype="any">
		<cfinvoke webservice="#variables.biServer#" returnvariable="variables.scheduleReportReturn" method="scheduleReport"
				argumentCollection="#variables.ScheduleReportRequest#">
		</cfinvoke>
		<cfreturn variables.scheduleReportReturn>
	</cffunction>
	
	<cffunction name="getScheduleRequest" access="public" returntype="struct">
		<cfreturn variables.ScheduleRequest>
	</cffunction>
	
	<cffunction name="getScheduleReportRequest" access="public" returntype="struct">
		<cfreturn variables.ScheduleReportRequest>
	</cffunction>
	
	<cffunction name="getJobID" access="public" returntype="string">
		<cfreturn variables.scheduleReportReturn>
	</cffunction>
</cfcomponent>
	
