<cfcomponent author="Cedric" displayName="api.da.Application" extends="fr.saaswedo.api.da.app.Datalert" hint="Application Datalert! RT">
	<cfsetting enableCFOutputOnly="true">
	<cfset THIS.name="DATALERT_RT_v0-2">
	<!--- La durée d'expiration de l'application doit etre supérieure à celle de la session --->
	<cfset THIS.applicationTimeout=createtimespan(1,0,0,0)>
	<!--- La durée d'expiration de la session doit etre inférieure à celle de l'application
	<cfset THIS.sessionTimeout=createtimespan(0,0,1,0)>
	--->
	<cfset THIS.sessionManagement=FALSE>
	<cfset THIS.setClientCookies=TRUE>
	<cfset THIS.setDomainCookies=TRUE>
	<cfset THIS.clientManagement=FALSE>
	
	<cffunction access="public" name="onApplicationStart" returntype="Boolean" hint="Handler du démarrage de l'application">
		<cflog type="information" text="Application #THIS.NAME# started">
		<cfreturn TRUE>
	</cffunction>
	
	<cffunction access="public" name="onSessionStart" returnType="void" hint="Handler du démarrage de la session">
		<cfset var sessionId=isDefined("SESSION") ? getSessionId(SESSION):"N.D">
		<cflog type="information" text="Session #sessionId# started">
	</cffunction>
	
	<cffunction access="public" name="onRequestStart" returntype="Boolean" hint="Handler du début de traitement d'une requete">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfreturn SUPER.onRequestStart(argumentCollection=ARGUMENTS)>
	</cffunction>

	<cffunction access="public" name="onRequestEnd" returntype="void"  hint="Handler de fin de traitement d'une requete">
		<cfargument name="targetPage" type="string" required="true" hint="URI de la ressource">
	</cffunction>
	
	<cffunction access="public" name="onMissingTemplate" returntype="Boolean" hint="Handler de ressource introuvable">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfreturn SUPER.onMissingTemplate(argumentCollection=ARGUMENTS)>
	</cffunction>
	
	<cffunction access="public" name="onSessionEnd" returnType="void" hint="Handler d'expiration d'une session">
		<cfargument name="sessionScope" type="Any" required="true" hint="Scope Session">
		<cfargument name="applicationScope" type="Any" required="true" hint="Scope Application">
		<cfset var sessionId=getSessionId(ARGUMENTS.sessionScope)>
		<cfset var appName=getApplicationName(ARGUMENTS.applicationScope)>
		<cflog type="information" text="Session #sessionId# ended on application #appName#">
	</cffunction>
	
	<cffunction access="public" name="onApplicationEnd" returntype="void" hint="Handler d'expiration de l'application">
		<cfargument name="applicationScope" type="Any" required="true" hint="Scope Application">
		<cfset var appName=getApplicationName(ARGUMENTS.applicationScope)>
		<cflog type="information" text="Application #appName# ended">
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="ON_ERROR handler">
		<cfargument name="exceptionObject" type="Any" required="true" hint="Implémentation de l'exception correspondante">
		<cfargument name="eventName" type="String" required="false" hint="Nom de l'évènement correspondant">
		<cfset SUPER.onError(argumentCollection=ARGUMENTS)>
	</cffunction>
</cfcomponent>