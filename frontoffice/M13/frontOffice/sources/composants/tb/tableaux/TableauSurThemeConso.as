package composants.tb.tableaux
{
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class TableauSurThemeConso extends TableauSurThemeConso_IHM implements ITableauModel
	{
		private var _dataProviders : ArrayCollection; 	
		
		[Bindable]	
		private var _changeHandlerFunction : Function;				
 
		private var _surTheme : String;
		
		[Bindable]
		private var _total : Number = 0;
		[Bindable]
		private var _totalVol : int = 0; 
		[Bindable]
		private var _totalNbAppels : int = 0;
		
		[Bindable]
		private var _title : String;		
		
		private var moisDeb : String;		
		
		private var moisFin : String;
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		private var identifiant : String;
		
		private var segment : String;
						
		/**
		 * Constructeur 
		 **/
		public function TableauSurThemeConso(ev : TableauChangeEvent = null){
			
			_surTheme = ev.SUR_THEME;
			_title = ev.SUR_THEME;
			identifiant = ev.IDENTIFIANT;
			segment = ev.SEGMENT;
			addEventListener(FlexEvent.CREATION_COMPLETE,init);	
		}
		
		/**
		 * Retourne le montant total
		 * */
		public function get total():Number{
			return _total;
		}
		
		
		 
		/**
		* Permet de passer les valeurs aux tableaux du composant.
		* <p>Pour chaque tableau de la collection, un DataGrid sera affiché à la suite du précédant</p>
		* @param d Une collection de tableau. 
		**/				
		public function set dataProviders(d : ArrayCollection):void{
					
			_dataProviders = _formatterDataProvider(d);
			drawTabs();
			
		}
		
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
		
		/**
		* Permet d'affecter une fonction pour traiter les Clicks sur Les DataGrids du composant
		* @param changeFunction La fonction qui traite les clicks.<p> Elle prend un paramètre de type Object qui représente une ligne du DataGrid</p>
		**/		
		public function set changeHandlerFunction ( changeFunction : Function ):void{
			_changeHandlerFunction = changeFunction;
		}		
		
		/**
		*  Retourne une reference vers la fonction qui traite le data provider
		**/			
		public function get changeHandlerFunction():Function{
			return _changeHandlerFunction;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		public function update():void{							
			//chargerDonnnees(); deprecated
		}
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void{
			//to do
		}	
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;
		}		
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null):void
		{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;				
		}			
		/*---------- PRIVATE -----------------------*/
		
		//initialisation du composant
		private function init(fe :  FlexEvent):void{				
			title = _title;			
			//update();
		}
		
		private function drawTabs():void{
			/* rep.dataProvider = dataProviders;			
			if (rep.dataProvider != null)	
				for (var i:int = 0; i < rep.dataProvider.length; i++){
					myGrid[i].addEventListener(Event.CHANGE,foo);									
				}  		 */	
				
			myGrid.dataProvider = dataProviders;
			myGrid.addEventListener(Event.CHANGE,foo);	
			
			myFooterGrid.columns[1].headerText = nf.format(_totalNbAppels);
			myFooterGrid.columns[2].headerText = nf.format(_totalVol);
			myFooterGrid.columns[3].headerText = cf.format(_total);	
		}	
		
		private function foo(ev :Event):void{
			var eventObj : TableauChangeEvent = new TableauChangeEvent("tableauChange");
			
			eventObj.LIBELLE = ev.currentTarget.selectedItem.LIBELLE;
			eventObj.MONTANT_TOTAL = ev.currentTarget.selectedItem.MONTANT_TOTAL;
			eventObj.ID = ev.currentTarget.selectedItem.ID;
			eventObj.QUANTITE = ev.currentTarget.selectedItem.QUANTITE;
			eventObj.TYPE_THEME	= ev.currentTarget.selectedItem.TYPE_THEME;
			eventObj.SUR_THEME = ev.currentTarget.selectedItem.SUR_THEME;
			eventObj.SEGMENT = ev.currentTarget.selectedItem.SEGMENT;
			eventObj.TYPE = ev.currentTarget.selectedItem.TYPE;		
			eventObj.NBAPPELS = ev.currentTarget.selectedItem.NBAPPELS;		
			eventObj.SOURCE = ev.currentTarget;	
			eventObj.IDENTIFIANT = identifiant;
			dispatchEvent(eventObj);			
		}
			
		// permet de formatter la collection de DataProvider 
		private function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();
			var o : Object;
		
			var surThemeArray : Array = new Array();
			
			_total = 0;
			_totalNbAppels = 0;
			_totalVol = 0;
			
		
			
			for	(o in d){		
				//surThemeArray.push(formateObject(d[o]));	
				tmpCollection.addItem(formateObject(d[o]));
										   
			}
			
			//tmpCollection.addItem(surThemeArray);
			 
		
		
			return tmpCollection;
			
		}
		
		private function calculTotal(arr : Array, colname : String):Number{
			var total : Number = 0;
			
			for(var i:int=0; i< arr.length;i++){
				
				total = total + parseFloat(arr[i][colname]);
			}
			return total;
		}		
		
		private function formateObject(obj : Object):Object{
			_totalVol = _totalVol + (parseFloat(obj.DUREE_APPEL));	
			_totalNbAppels = _totalNbAppels + parseInt(obj.NOMBRE_APPEL);	
			_total = _total +  parseFloat(obj.MONTANT_FINAL);	

			var o : Object = new Object();			
			o["NBAPPELS"]= nf.format(obj.NOMBRE_APPEL);
			o["LIBELLE"] = obj.THEME_LIBELLE;
			o["SEGMENT"] = segment;
			o["QUANTITE"] = nf.format((parseFloat(obj.DUREE_APPEL)));
			o["TYPE_THEME"] = obj.TYPE_THEME;
			o["SUR_THEME"] = "";
			o["MONTANT_TOTAL"]= cf.format(obj.MONTANT_FINAL);
			o["TYPE"] = "THEME";	
			o["ID"] = obj.IDTHEME_PRODUIT;				
			return o;
		 
		}	
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected function chargerDonnnees():void{
			/* var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.surtheme.facade",
																			"getListeThemesSurTheme",
																			chargerDonnneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op,
										perimetre,
										modeSelection,
										identifiant,
										_surTheme,
										moisDeb,
										moisFin); */
			
		}
		protected function chargerDonnneesFaultHandler(fe :FaultEvent):void{
		 	/* trace(fe.fault.faultString,fe.fault.name);	 */
		 	 
		 }     
		  
		protected function chargerDonnneesResultHandler(re :ResultEvent):void{
		 	/* try{
		 		 
		  		dataProviders =  re.result as ArrayCollection;	
		  		
		  	}catch(er : Error){		  	
		  		trace(er.message,"fr.consotel.consoview.tb.surtheme.facade getListeThemesSurTheme");
		  	} */
		  
		 }	
	}
}