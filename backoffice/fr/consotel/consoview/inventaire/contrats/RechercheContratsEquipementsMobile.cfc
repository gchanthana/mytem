<cfcomponent output="false">
	
	<cffunction name="findContratsEqts" access="remote" output="false" returntype="query">
		<cfargument name="idpool" type="Numeric" required="false"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.FIND_CONTRATSEQ">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idpool" value="#idpool#"/>
			<cfprocresult name="p_result">		  
		</cfstoredproc>
		
		<cfreturn listeContrats/>	
	</cffunction>
	
</cfcomponent>