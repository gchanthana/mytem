package univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts
{
	import flash.events.Event;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;

	public class ContactChangeEvent extends Event
	{
		private var _operateur : OperateurVO;
		private var _societe : Societe;
		
		public function ContactChangeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function 
			CvAccessManager.getSession().USER.CLIENTACCESSID
			super(type, bubbles, cancelable);
		}
		
		public function get operateur():OperateurVO{
			return _operateur
		}
		
		public function set operateur(op : OperateurVO):void{
			_operateur = op;
		}
		
		public function get societe():Societe{
			return _societe
		}
		
		public function set societe(ste : Societe):void{
			_societe = ste;
		}
	}
}