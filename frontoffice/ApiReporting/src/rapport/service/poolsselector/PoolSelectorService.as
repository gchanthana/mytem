package rapport.service.poolsselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	
	public class PoolSelectorService
	{
		
		private var _model:PoolSelectorModel;
		public var handler:PoolSelectorHandler;
		
		public function PoolSelectorService():void
		{
			this._model = new PoolSelectorModel();
			this.handler = new PoolSelectorHandler(model);
		}		
		
		
		public function get model():PoolSelectorModel
		{
			return this._model;
		}
		
		public function getValuesPool():void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.selector.PoolSelectorService","getValuesPool",handler.getValuesPoolHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
		
		
	}
}