﻿ 			
	<cfset biServer="http://bip-commande.consotel.fr/xmlpserver/services/PublicReportService_v11?WSDL">	
	<cfset _locale = SESSION.USER.GLOBALIZATION>
	<cfset _racine = Session.perimetre.RAISON_SOCIALE>	
	<cfset _idtype_commande = FORM.IDTYPE_COMMANDE>
	<cfset _idCommande = FORM.IDCOMMANDE>
	<cfset _numCommande = FORM.NUMERO_COMMANDE>
	<cfset _idoperateur = 512><!---FORM.IDOPERATEUR--->
	<cfset _idGestionnaire = Session.user.CLIENTACCESSID>
	<cfset _gestionnaire = Session.user.PRENOM & ' ' & Session.user.NOM>
	<cfset _mailGestionnaire = Session.user.EMAIL>
	
	
	
	<cfset _templateName = createObject("component","fr.consotel.consoview.M16.v2.TemplateUtil").getTemplateNameByOpeNType(_idtype_commande,_idoperateur)/>
	<cfset _format = "pdf">
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDCOMMANDE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=_idCommande>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDGESTIONNAIRE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=_idGestionnaire>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_RACINE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=_racine>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	
		
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_GESTIONNAIRE">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=_gestionnaire>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_MAIL">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=_mailGestionnaire>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_LANGUE_PAYS">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=_locale>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[6]=ParamNameValue>
	
	
	<cfset myParamReportRequest=structNew()>	
	<cfset myParamReportRequest.reportAbsolutePath="/consoview/gestion/flotte/BDC/BDC.xdo">
	<cfset myParamReportRequest.attributeTemplate= _templateName>
	<cfset myParamReportRequest.attributeLocale=_locale>
	<cfset myParamReportRequest.attributeFormat=LCase(_format)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">	
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>
	<cfset filename = "BDC" & reporting.getFormatFileExtension(_format)>
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">
	<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">
