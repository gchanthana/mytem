﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">

	<!--- recuperer la liste des clients--->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc procedure="pkg_m28.getListClientsByUser" datasource="ROCOFFRE" >
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_iduser" type="in" value="#data.id_user#">
				<cfprocresult name="p_retour" >
			</cfstoredproc>
			
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.LISTE_CLIENTS = p_retour>
				<cfreturn CreateReponse("AL001",1,data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(-1,"","Erreur de procédure","","")>
				<cfreturn CreateReponse("AL001",0,err)/>
			</cfif>
			
			<cfcatch type="any" >
				<cfreturn CreateReponse("AL001", 0, ThrowCfCatchError("AL001",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
				
			</cfcatch>
		</cftry>
		
	</cffunction>
</cfcomponent>