<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAL007" hint="liste des langues selon l'application"  output="false">
	
	<!--- 
		retourne la liste des langues d'une application
	 --->
	<cffunction name="executeAct" access="public" returntype="Struct" output="false"
				hint="retourne la liste des langues d'une application">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.getListeLangueForApplication">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_codeApp" type="in" value="#data.CODEAPP#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_langid" type="in" value="#data.IDLANGUE#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			
			<cfreturn CreateReponse("AL007", 1, qResult)>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AL007", 0, ThrowCfCatchError("AL007",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>