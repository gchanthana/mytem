package univers.inventaire.contrat.system
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[RemoteClass(alias="fr.consotel.consoview.inventaire.contrats.ContratAbonnementMobile")]

	[Bindable]
	public class ContratAbonnementMobile implements IContrat
	{
		private const remoteClass:String="fr.consotel.consoview.inventaire.contrats.ContratAbonnementMobile";
		
		
		public var SOUS_TETE:String = "";
		public var IDSOUS_TETE:Number = 0;
		public var IDCONTRAT:Number = 0;
		public var IDTYPE_CONTRAT:Number = 0;
		public var IDFOURNISSEUR:Number = 0;
		public var REFERENCE_CONTRAT:String = "";
		public var DATE_SIGNATURE:Date = null;
		public var DATE_ECHEANCE:Date = null;
		public var COMMENTAIRE_CONTRAT:String = "";
		public var MONTANT_CONTRAT:Number = 0;
		public var DUREE_CONTRAT:Number = 0;
		public var TACITE_RECONDUCTION:Number = 0;
		public var DATE_DEBUT:Date = null;
		public var PREAVIS:Number = 0;
		public var DATE_DENONCIATION:Date = null;
		public var IDCDE_CONTACT_SOCIETE:Number = 0;
		public var LOYER:Number = 0;
		public var PERIODICITE:Number = 0;
		public var MONTANT_FRAIS:Number = 0;
		public var NUMERO_CLIENT:String = "";
		public var NUMERO_FOURNISSEUR:String = "";
		public var NUMERO_FACTURE:String = "";
		public var CODE_INTERNE:String = "";
		public var BOOL_CONTRAT_CADRE:Number = 0;
		public var BOOL_AVENANT:Number = 0;
		public var ID_CONTRAT_MAITRE:Number = 0;
		public var FRAIS_FIXE_RESILIATION:Number = 0;
		public var MONTANT_MENSUEL_PEN:Number = 0;
		public var DATE_RESILIATION:Date = null;
		public var PENALITE:Number = 0;
		public var DESIGNATION:String = "";
		public var DATE_RENOUVELLEMENT:Date = null;
		public var OPERATEURID:Number = 0;
		public var IDCOMPTE_FACTURATION:Number = 0;
		public var IDSOUS_COMPTE:Number = 0;
		public var NUMERO_CONTRAT:String = "";
		public var IDRACINE:Number = 0;
		public var DATE_ELLIGIBILITE:Date = null;
		public var DUREE_ELLIGIBILITE:Number = 0;
						
		protected var _items:ICollectionView;
		
		public function ContratAbonnementMobile()
		{
		}

		public function createContrat(items:ICollectionView):void
		{
			_items = items;
		}
		
		public function getDetailContrat():void
		{
			var op:AbstractOperation = RemoteObjectUtil
							.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							remoteClass,
							"getDetailContratAbonnementMobile",getDetailContratResultHandler);
			
			if(IDCONTRAT > 0)
			{
				RemoteObjectUtil.callService(op,this.IDCONTRAT);
			}
			
		}
		
		public function udpateInfosContrat():void
		{
															
		}
		
		public function deleteContrat():void
		{
		}
		
		public function findItems():void
		{
				
		}
		
		public function get items():ICollectionView
		{
			return new ArrayCollection();
		}
		
		
		
		private function getDetailContratResultHandler(event:ResultEvent):void
		{
			if (!event.result) return;
			if((event.result as ArrayCollection).length == 0)return;
			
			var obj:Object = event.result[0]; 
			
			
			this.SOUS_TETE = obj.SOUS_TETE;
			this.IDSOUS_TETE = obj.IDSOUS_TETE;		
			this.NUMERO_CONTRAT = obj.NUMERO_CONTRAT;
			this.IDCONTRAT = obj.IDCONTRAT;
			this.IDTYPE_CONTRAT = obj.IDTYPE_CONTRAT;
			this.IDFOURNISSEUR = obj.IDFOURNISSEUR;
			this.REFERENCE_CONTRAT = obj.REFERENCE_CONTRAT;
			this.DATE_SIGNATURE = obj.DATE_SIGNATURE;
			this.DATE_ECHEANCE = obj.DATE_ECHEANCE;
			this.COMMENTAIRE_CONTRAT = obj.COMMENTAIRE_CONTRAT;
			this.MONTANT_CONTRAT = obj.MONTANT_CONTRAT;
			this.DUREE_CONTRAT = obj.DUREE_CONTRAT;
			this.TACITE_RECONDUCTION = obj.TACITE_RECONDUCTION;
			this.DATE_DEBUT = obj.DATE_DEBUT;
			this.PREAVIS = obj.PREAVIS;
			this.DATE_DENONCIATION = obj.DATE_DENONCIATION;
			this.IDCDE_CONTACT_SOCIETE = obj.IDCDE_CONTACT_SOCIETE;
			this.LOYER = obj.LOYER;
			this.PERIODICITE = obj.PERIODICITE;
			this.MONTANT_FRAIS = obj.MONTANT_FRAIS;
			this.NUMERO_CLIENT = obj.NUMERO_CLIENT;
			this.NUMERO_FOURNISSEUR = obj.NUMERO_FOURNISSEUR;
			this.NUMERO_FACTURE = obj.NUMERO_FACTURE;
			this.CODE_INTERNE = obj.CODE_INTERNE;
			this.BOOL_CONTRAT_CADRE = obj.BOOL_CONTRAT_CADRE;
			this.BOOL_AVENANT = obj.BOOL_AVENANT;
			this.ID_CONTRAT_MAITRE = obj.ID_CONTRAT_MAITRE;
			this.FRAIS_FIXE_RESILIATION = obj.FRAIS_FIXE_RESILIATION;
			this.MONTANT_MENSUEL_PEN = obj.MONTANT_MENSUEL_PEN;
			this.DATE_RESILIATION = obj.DATE_RESILIATION;
			this.PENALITE = obj.PENALITE;
			this.DESIGNATION = obj.DESIGNATION;
			this.DATE_RENOUVELLEMENT = obj.DATE_RENOUVELLEMENT;
		}
	}
}