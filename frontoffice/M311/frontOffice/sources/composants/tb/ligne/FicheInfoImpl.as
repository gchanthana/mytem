package composants.tb.ligne
{
	import composants.tb.ligne.entity.FicheInfoVO;
	import composants.tb.ligne.event.FicheEvent;
	import composants.tb.ligne.services.FicheInfoService;
	
	import flash.events.Event;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	
	[Bindable]
	public class FicheInfoImpl extends VBox
	{
		
		private var _info : FicheInfoVO;
		private var _ligne : Number;
		public var txtFonction:TextInput;
		
		private var serviceInfoFiche : FicheInfoService = new FicheInfoService();
		
		public function FicheInfoImpl()
		{
		}
		
		public function get ligne():Number
		{
			return _ligne;
		}
		
		public function set ligne(value:Number):void
		{
			_ligne = value;
		}

		public function init():void
		{
			serviceInfoFiche.getFicheInfo(ligne);
			serviceInfoFiche.model.addEventListener(FicheEvent.FICHEINFO,ficheInfoHandler);
		}
		
		public function get info():FicheInfoVO
		{
			return _info;
		}
		
		public function set info(idinf:FicheInfoVO):void
		{
			_info = idinf;		
		}
		
		public function ficheInfoHandler(ev:FicheEvent):void
		{
			info = serviceInfoFiche.model.ficheInfo;
		}
		
		public function btnModifierFonction_clickHandler(event:Event):void
		{
			serviceInfoFiche.setFonctionLigne(ligne, txtFonction.text);
			
		}
		
	}
}