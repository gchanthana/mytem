package gestionparc.ihm.action.dissocier.actionDissocierCollabfromExt
{
	import gestionparc.ihm.question.extention.QuestionFicheDissocierCollabFromExt;

	public class ActionDissocierCollabFromExtReceiver
	{
		private var qn:QuestionFicheDissocierCollabFromExt;

		public function ActionDissocierCollabFromExtReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			qn=new QuestionFicheDissocierCollabFromExt();
		}
	}
}