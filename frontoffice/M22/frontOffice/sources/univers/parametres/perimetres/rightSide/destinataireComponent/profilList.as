package univers.parametres.perimetres.rightSide.destinataireComponent
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import event.ProfilEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;


	public class profilList extends TitleWindow
	{
		public var btAdd:Button;
		public var btRemove:Button;
		public var btFermer:Button;
		public var btModify:Button;
		public var dgProfils:DataGrid;
		public var lb_profil:Label;
		public var  txtFilterProfil:TextInput;
		
		private var _profilData:ArrayCollection;

		public function get modeEcriture():Boolean
		{
			return true
		}

		public function profilList()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		public function formatColonneDate(item:Object, column:DataGridColumn):String
		{
			if (!item[column.dataField])
				return "";
			var output:String=DateFunction.formatDateAsString(item[column.dataField]);
			return output;
		}

		protected function initIHM(e:Event):void
		{
			addEventListener(CloseEvent.CLOSE, closeWindow);
			fillDg();
			btAdd.visible=modeEcriture;
			btAdd.addEventListener(MouseEvent.CLICK, btAddClicked);

			btFermer.addEventListener(MouseEvent.CLICK, btn_fermer_clickHandler);

			dgProfils.addEventListener(ProfilEvent.MODIFIER_PROFIL_EVENT, btModifyClicked); // pour modifier un profil
			dgProfils.addEventListener(ProfilEvent.DELETE_PROFIL_EVENT, btRemoveClicked); // pour supprimer un profil
			
			txtFilterProfil.addEventListener(Event.CHANGE, filterDataGrid); // pour filtrer

		}

		private function btModifyClicked(e:ProfilEvent):void
		{
			var modifyProfilWin:modifyProfil=PopUpManager.createPopUp(this, modifyProfil, true) as modifyProfil;
			modifyProfilWin.setData(dgProfils.selectedItem);
			modifyProfilWin.addEventListener("modifyItem", modifyItemEvent);
			PopUpManager.centerPopUp(modifyProfilWin);

		}

		private function btRemoveClicked(e:ProfilEvent):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "removeProfil", removeProfilResult, throwError);
			RemoteObjectUtil.callService(op, dgProfils.selectedItem.IDPROFIL_ANNUAIRE);
		}

		private function removeProfilResult(e:ResultEvent):void
		{
			if (e.result > 0)
			{
				_profilData.removeItemAt(dgProfils.selectedIndex);
				fillDg();
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M22', 'Ce_profil_est_associ____un_ou_plusieurs_') , ResourceManager.getInstance().getString('M22', 'Attention'), removeProfilHandler);
			}
		}
		
		private function removeProfilHandler(e:CloseEvent):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M22.service.destinataires.destProfilService","removeProfilCascade",RemoveProfilCascadeResultHandler,throwError);
			RemoteObjectUtil.callService(op, dgProfils.selectedItem.IDPROFIL_ANNUAIRE);
		}
		
		private function RemoveProfilCascadeResultHandler(e:ResultEvent):void
		{
			if (e.result > 0)
			{
				_profilData.removeItemAt(dgProfils.selectedIndex);
				fillDg();
				dispatchEvent(new Event("UPDATE_PROFILE_DEST_LISTE"));
			}
			else
			{
				
			}
		}
		private function fillDg():void
		{
			var idGroupeMaitre:Number=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "getAllProfils", fillDgResult, throwError);
			RemoteObjectUtil.callService(op, idGroupeMaitre);
		}

		private function fillDgResult(e:ResultEvent):void
		{
			_profilData=ArrayCollection(e.result);
			dgProfils.dataProvider=_profilData;
			initLabel();
		}

		protected function btAddClicked(e:MouseEvent):void
		{
			var createProfilWin:createProfil=PopUpManager.createPopUp(this, createProfil, true) as createProfil;
			createProfilWin.addEventListener("addItem", addItemEvent);
			PopUpManager.centerPopUp(createProfilWin);
		}

		protected function modifyItemEvent(e:modifyProfilEvent):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "updateProfil", addProfilResult, throwError);
			RemoteObjectUtil.callService(op, e.id, e.libelle, e.codeInterne, e.commentaire);

		}

		protected function addItemEvent(e:createProfilEvent):void
		{
			var idGroupeMaitre:Number=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "addProfil", addProfilResult, throwError);
			RemoteObjectUtil.callService(op, idGroupeMaitre, e.libelle, e.codeInterne, e.commentaire);

		}

		private function addProfilResult(e:ResultEvent):void
		{
			fillDg();
		}

		/**
		 * Initilaiser le label qui permet d'afficher le nb des profils dans le popup gerer profils
		 */
		private function initLabel():void
		{
			var len:Number=_profilData.length;
			lb_profil.text=len + " " + ResourceManager.getInstance().getString('M22', '_profil_s_');
			dispatchEvent(new Event("UPDATE_PROFILE_LISTE")); // mettre a jour la liste des profils
		}

		protected function closeWindow(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function btn_fermer_clickHandler(event:MouseEvent):void
		{
			closeWindow(null);
			dispatchEvent(new Event("UPDATE_PROFILE_LISTE"));
		}

		private function throwError(result:FaultEvent):void
		{

		}
		
		/*-----------------------------------------------------------------------------------------------------------------------------*/
		
		/**
		 * filtrer le data grid
		 * @param event : Event
		 * @return void
		 */
		private function filterDataGrid(event:Event):void
		{
			if (dgProfils.dataProvider != null)
			{
				dgProfils.dataProvider.filterFunction=filterMyCollection; // le nom de la fonction ;
				dgProfils.dataProvider.refresh();
				
				initLabel();
			}
		}
		
		/**
		 *
		 * permet de filtrer le data grid
		 * @param item : Objet (chaque ligne de data provider)
		 * @return Boolean
		 */
		private function filterMyCollection(item:Object):Boolean
		{
			var itemNomProfil:String="";
			var itemDate:String="";
			var itemCode:String="";
			var itemComment:String="";
			
			var searchString:String=StringUtil.trim(txtFilterProfil.text.toLowerCase()); // le filtre
					
			if (item.CODE_INTERNE!= null)
			{
				itemCode=(item.CODE_INTERNE as String).toLowerCase();
			}
			
			if (item.NOM_PROFIL != null)
			{
				itemNomProfil=(item.NOM_PROFIL as String).toLowerCase();
			}
								
			if ((itemCode.indexOf(searchString) > -1) || (itemNomProfil.indexOf(searchString) > -1))
			{
				return true;
			}
			return false;
		}
		/*-----------------------------------------------------------------------------------------------------------------------------*/

	}
}
