 <cfcomponent name="facade" displayname="facade">	
	
	<cffunction name="rechercherListeLigneGroupe" access="public" returntype="query">
		
		<cfargument name="perimetre" type="string" required="false">	
		<cfargument name="idGroupe" type="numeric" required="false">
		<cfargument name="chaine" type="string" required="true">
		<!--- <cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false"> --->
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.recherche.Recherche")>
		<cfset obj.init('Groupe')>
				
		<cfset result=obj.queryData(idGroupe,chaine)>										
		 <cfreturn result>
	</cffunction>  
 	
	<cffunction name="rechercherListeLigne" access="public" returntype="query">
		<cfargument name="perimetre" type="string" required="false">	
		<cfargument name="idGroupe" type="numeric" required="true">
		
		
		<cfset obj= CreateObject("component","fr.consotel.consoview.tb.recherche.Recherche")>
		<cfset obj.init('GroupeLigne')>
		<cfset result=obj.listeLigne(idGroupe)>				
		<cfreturn result>
	</cffunction>
	
</cfcomponent>