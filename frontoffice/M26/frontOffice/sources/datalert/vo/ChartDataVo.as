package datalert.vo
{
	public class ChartDataVo
	{
		[Bindable]
		public var DAY:String;
		public var HOUR:String;		
		public var DATAWIFI:Number=0;
		public var DATADOMESTIC:Number=0;
		public var DATAROAMING:Number=0;
		
		public function ChartDataVo()
		{
		}
	}
}