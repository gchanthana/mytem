package commandemobile.itemRenderer
{
	import commandemobile.entity.Commande;
	import commandemobile.system.AbstractGestionPanier;
	import commandemobile.temp.temp;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.List;
	
	[Bindable]
	public class DetailsItemRendererImpl extends Box
	{
		
		//COMPOSANTS
		public var listConfig 			:List;
		
		//OBJETS
		public var _cmd					:Commande;
		private var _temp				:temp = new temp();
		private var _gPanier			:AbstractGestionPanier = new AbstractGestionPanier()
 		
		//VARIABLES LOCALES
		public var listArticles			:ArrayCollection = new ArrayCollection();
		public var _articlesXML			:XML = new XML();

		
		//CONSTRUCTEUR (>LISTNER)
		public function DetailsItemRendererImpl()
		{
			_temp.addEventListener("articlesArrived",	articlesArrivedHandler);
			addEventListener("getProcedure",			getProcedure);
			addEventListener("eraseThisConfig",			eraseConfigurationClickHandler);
		}
		
		private function eraseConfigurationClickHandler(e:Event):void
		{
			if(listArticles.length > 1)
			{
				for(var i:int = 0;i < listArticles.length;i++)
				{
					try
					{
						if(listArticles[i].IDSOUSTETE == listConfig.selectedItem.IDSOUSTETE)
						{
							listArticles.removeItemAt(i);
						}
					}
					catch(e:Error)
					{
						var bool:Boolean = false;
					}
				} 
			}
		}
		
		private function getProcedure(e:Event):void
		{
			_temp.fournirArticlesCommande(_cmd);
		}
		
		private function articlesArrivedHandler(e:Event):void
		{
			_articlesXML = _temp.listeArticles;
			formatXMLToArrayCollection();
		}
		
		private function formatXMLToArrayCollection():void
		{
			listArticles = new ArrayCollection();
			var xmlList:XMLList = _articlesXML.children();
			var nbre:int = xmlList.length();
			if(nbre > 1)
			{
				formatXMLList(xmlList);
			}
			else
			{
				formatXML(xmlList[0]);
			}
		}
		
		private function formatXMLList(xmlList:XMLList):void
		{	
			for(var i:int = 0;i < xmlList.length();i++)
			{
				formatXML(xmlList[i]);
			}
		}
		
		private function giveMeDate(strg:String):String
		{
			var day:String 		= strg.substr(8,2);
			var month:String	= strg.substr(5,2);
			var year:String		= strg.substr(0,4);
			return day + "/" + month + "/" + year;
		}
		
		private function formatXML(xml:XML):void
		{
			
			if(!xml) return;
			
			var object			:Object = new Object();
			var xmlListEquip	:XMLList = 	xml.equipements.children();
			var xmlListRess		:XMLList =	xml.ressources.children();
			var arrayEquip		:ArrayCollection = new ArrayCollection();
			var arrayRess		:ArrayCollection = new ArrayCollection();
			var items			:ArrayCollection = new ArrayCollection();
			var arrayResult		:ArrayCollection = new ArrayCollection();
			 	
			if(xml.fpc[0] != "")
			{
				object.ENGAGEMENT_RESSOURECES 	= giveMeDate(String(xml.fpc[0]));
			}
			else
			{
				object.ENGAGEMENT_RESSOURECES   = Number(xml.engagement_res[0]);
				object.ENGAGEMENT_EQUIPEMENTS 	= Number(xml.engagement_eq[0]);
			}
			object.NOMEMPLOYE 				= lookForCollaborateur(String(xml.nomemploye[0]));
			object.CODE_INTERNE				= String(xml.code_interne[0]);
			object.SOUSTETE 				= String(xml.soustete[0]);
			object.IDSOUSTETE 				= Number(xml.idsoustete[0]);
			
			arrayEquip 	= addEquipements(xmlListEquip);
			arrayRess 	= addRessources(xmlListRess);
			items = addToArrayCollection(arrayEquip,arrayRess);
			
			listArticles.addItem(addToObject(items,object));
		}
		
		private function addEquipements(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				object = new Object();
				object.TYPE_EQUIPEMENT 	= String(xmlList[i].type_equipement);
				object.CODE_IHM 		= String(xmlList[i].code_ihm);
				object.LIBELLE_PRODUIT 	= String(xmlList[i].libelle);
				object.PRIX_UNIT 		= searchVirguleOrPoint(String(xmlList[i].prix));
				object.TOTAL			= searchVirguleOrPoint(String(xmlList[i].prix));
				arrayCollection.addItem(object);
			}
			return arrayCollection;
		}
		
		private function searchVirguleOrPoint(price:String):Number
		{
			if(Number(price) >= 0)
			{
			}
			else
			{
				price = price.replace(",",".");
			}
			return Number(price);
		}
		
		private function addRessources(xmlList:XMLList):ArrayCollection
		{
			var object:Object = new Object();
			var arrayCollection:ArrayCollection = new ArrayCollection();
			for(var i:int = 0; i < xmlList.length();i++)
			{
				if(String(xmlList[i].libelle) != "")
				{
					object = new Object();
					object.TYPE_EQUIPEMENT 	= String(xmlList[i].theme);
					object.LIBELLE_PRODUIT 	= String(xmlList[i].libelle);
					object.PRIX_UNIT 		= " - ";
					object.TOTAL 			= " - ";
					arrayCollection.addItem(object);
				}
			}
			return arrayCollection;
		}
				
		private function addToArrayCollection(equipements:ArrayCollection,ressources:ArrayCollection):ArrayCollection
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			
			for(var i:int = 0; i < equipements.length;i++)
			{
				arrayCollection.addItem(equipements[i]);
			}
			for(var j:int = 0; j < ressources.length;j++)
			{
				arrayCollection.addItem(ressources[j]);
			}
			return arrayCollection;
		}
		
		private function addToObject(allItems:Object,info:Object):Object
		{
			var arrayCollection:ArrayCollection = new ArrayCollection();
			var object:Object = new Object();
			
			object.ITEMS 			= allItems;
			if(info.NOMEMPLOYE == " - ")
			{
				object.COLLABORATEUR 	= "Libellé : " + info.CODE_INTERNE;
				object.VIEWLIBELLE = true;
			}
			else
			{
				object.COLLABORATEUR 	= "Collaborateur : " + info.NOMEMPLOYE;
				object.VIEWLIBELLE = false;
			}
			object.ENGAGEMENT 		= info.ENGAGEMENT_RESSOURECES;
			object.SOUSTETE 		= info.SOUSTETE;
			object.IDSOUSTETE		= info.IDSOUSTETE;
			object.PRIX 			= calculPrice(allItems as ArrayCollection);
			return object;
		}
		
		private function calculPrice(allItems:ArrayCollection):String
		{
			var total:Number = 0;
			for(var i:int = 0; i < allItems.length;i++)
			{
				if(allItems[i].PRIX_UNIT != " - ")
				{
					total = total + Number(allItems[i].PRIX_UNIT);
				}
			}
			return total.toString();
		}
		
		private function lookForCollaborateur(name:String):String
		{
			var nameCollaborateur:String = "";
			if(name == "")
			{
				nameCollaborateur = " - " 
			}
			else
			{
				nameCollaborateur = name;
			}
			return nameCollaborateur;
		}

	}
}