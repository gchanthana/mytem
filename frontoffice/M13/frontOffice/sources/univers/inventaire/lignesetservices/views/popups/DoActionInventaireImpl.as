package univers.inventaire.lignesetservices.views.popups
{
	import mx.controls.Label;
	
	[Bindable]
	public class DoActionInventaireImpl extends DoActionRaccoImpl
	{	
		 
		 
		public var lblAction : Label;
		 
		
						
		public function DoActionInventaireImpl()
		{			
			super();
		}
		
		override protected function commitProperties():void{			
			super.commitProperties();
			configurerDateSelector();
		}
		
		override protected function configurerDateSelector():void{
			if (app != null){
				var selectableRange : Object = new Object();
				if(app.historiqueRessource != null && app.historiqueRessource.length > 0){
					selectableRange.rangeStart = app.historiqueRessource[0].DATE_ACTION;
					dcDateRacco.selectableRange = selectableRange;
					dcDateRacco.selectedDate = new Date();
				} else {
					dcDateRacco.selectedDate = new Date();
				}
			}
		}
	}
}