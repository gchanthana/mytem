package univers.inventaire.equipements.lignes.v2.vo
{
	import com.adobe.utils.ArrayUtil;
	
	[Bindable]
	public class LibellePerso
	{
		public var libelle: String= "Sans titre";
		public var libelleName: String= "";
		public var idLibelle: Number= 0;
		public var idGroupeClient: Number= 0;
	}
}