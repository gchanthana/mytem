<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Ajout / MAJ des participants d'une formation" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient IDFORMATIONPART et L_APPLOGINID">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.IUinterv_formation">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.IDFORMATIONPART#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.L_APPLOGINID#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_nb_del">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_nb_add">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
				
			<cfif p_retour EQ 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ013", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ013", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ013", 0, ThrowCfCatchError("AJ013",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>