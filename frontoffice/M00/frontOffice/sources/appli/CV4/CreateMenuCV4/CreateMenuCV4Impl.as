package appli.CV4.CreateMenuCV4
{
	import appli.impersonnification.popup.PopupChooseOtherAccountIHM;
	
	import composants.gradientcomponents.GradientBox;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import general.appliComposants.ClickableCheminPerimetre.ClickableCheminPerimetreIHM;
	import general.appliComposants.CvPerimetreNodeVO;
	import general.appliComposants.ListePerimetreWindow.ListePerimetresWindow;
	import general.appliComposants.customClassButton.CreateMenuCv4CustomClassButton;
	import general.interfaceClass.IMenu;
	import general.service.InfosChargementService;
	
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.core.IFlexDisplayObject;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import rapport.vo.ApiReportingSingleton;
	
	public class CreateMenuCV4Impl extends GradientBox implements IMenu
	{
		[Bindable] protected var moduleRapportEnable:Boolean;
		
		[Bindable] protected var BACKGROUND_COLOR:uint = new uint(333333);
		
		[Bindable] public var boxRapport:VBox;	
		
		[Bindable] protected var chemin:String
		[Bindable]public var infosChargementService:InfosChargementService;
		public var hbMenu:HBox
		
		public var btnModifierPerimetre:Button
		public var ccp:ClickableCheminPerimetreIHM;
		
		private var _boolAfficherDates:Boolean;
		private var positionPoint:Point;
		private var listePerimetreWindow:ListePerimetresWindow;
		
		public function CreateMenuCV4Impl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		public function init(e:FlexEvent):void
		{
			infosChargementService = new InfosChargementService();
			infosChargementService.getInfosDates();
			listePerimetreWindow = new ListePerimetresWindow();
//			listePerimetreWindow.addEventListener(PerimetreEvent.PERIMETRE_NODE_CHANGED,handleChangeNodeEvent);
//			listePerimetreWindow.addEventListener(ConsoViewEvent.GROUP_CHANGED,handleGroupChanged);
			positionPoint = new Point();
		}
	// Quand le perimetre change, on réinitialise le composant
		public function updatePerimetre():void
		{
			if(listePerimetreWindow.isPopUp)
				PopUpManager.removePopUp(listePerimetreWindow);
			updateMenu();
			listePerimetreWindow.updatePerimetre();
			initAffichagePerimetre();
			afterUniversFunctionUpdated("");
		}
		public function updateMenu():void
		{
			var XMLUnivers:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData
			var xmlList:XMLList = XMLUnivers.children()
			hbMenu.removeAllChildren()
			for each(var xml:XML in xmlList)
			{
				var btn:CreateMenuCv4CustomClassButton = new CreateMenuCv4CustomClassButton();
				btn._XMLUnivers = xml
				hbMenu.addChild(btn)
			}
		}
		public function afterUniversFunctionUpdated(actionType:String):void
		{
			if( CvAccessManager.CURRENT_UNIVERS == "HOME" || CvAccessManager.CURRENT_UNIVERS == "HOME_E0") 
			{				
				boolAfficherDates = true;
			}
			if( CvAccessManager.CURRENT_FUNCTION == "FACT_DASHBOARD_E0" || CvAccessManager.CURRENT_FUNCTION == "FACT_DASHBOARD_E0")
			{	
				boolAfficherDates = true;
			}
			var accessData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			if((accessData.UNIVERS.(@KEY=='FACT_REPORT_E0').@USR == 1) || (accessData.UNIVERS.(@KEY=='FACT_REPORT_360').@USR == 1))
			{	
				moduleRapportEnable = true;
			}
			else
			{
				moduleRapportEnable = false;
			}
			
		} 
		
		
		
		public function logoff():void
		{
			closeAllPopups();
		} 
		protected function btnTEMClickHandler():void
		{
			var urlRequest:URLRequest = new URLRequest("https://saaswedo.zendesk.com/categories/20045831-guide-utilisateur ");
			navigateToURL(urlRequest,"CustomName");


		}
		/* ---------------------------------------------------------------------------------
		ferme toutes les popups 
		--------------------------------------------------------------------------------- */
		private function closeAllPopups():void
		{
			var len:Number=systemManager.numChildren - 1;
			var i:Number=len;
			
			while (i >= 0)
			{
				if (systemManager.getChildAt(i).hasOwnProperty("isPopUp"))
				{
					PopUpManager.removePopUp(IFlexDisplayObject(systemManager.getChildAt(i)));
				}
				i--;
			}
		}
		/* ---------------------------------------------------------------------------------
		Fonction qui désaffiche les autres menus pour ne laisser 
		que le menu de ce bouton affiché
		--------------------------------------------------------------------------------- */		
		public function refresh(btn:CreateMenuCv4CustomClassButton):void
		{
			for each(var bt:CreateMenuCv4CustomClassButton in hbMenu.getChildren())
			{
				if(bt != btn)
					bt.AfficherMenu(false);
			}
		} 
		/* ---------------------------------------------------------------------------------
		Cette fonction va demander à tous les boutons de rafraichir leur style
		--------------------------------------------------------------------------------- */		
		public function refreshBtnStyle():void
		{
			for each(var bt:CreateMenuCv4CustomClassButton in hbMenu.getChildren())
			{
				bt.refreshThisBtnStyle();
			}
		} 
/*	------------------------------------------------------------------------------------------
								LISTEPERIMETREWINDOW
------------------------------------------------------------------------------------------  */		
		public function displayListePerimetres(event:MouseEvent):void 
		{
			PopUpManager.addPopUp(listePerimetreWindow,this.parentApplication as DisplayObject,true);
			PopUpManager.bringToFront(listePerimetreWindow);
			PopUpManager.centerPopUp(listePerimetreWindow)
		}
//		public function handleChangeNodeEvent(evt:Event):void 
//		{
//			var selectedNode:XML = listePerimetreWindow.getSelectedNode();
//			ccp.updateAffichage((listePerimetreWindow.perimetreTree as SimplePerimetreTree).listeNode);
//			dispatchEvent(evt);
//		}
//		public function handleGroupChanged(evt:Event):void 
//		{
//			dispatchEvent(evt);
//		}
		public function initAffichagePerimetre():void
		{
			var arr:Array = []
			var sess:ConsoViewSessionObject = CvAccessManager.getSession() as ConsoViewSessionObject
			var idperimetre:int = sess.CURRENT_PERIMETRE.PERIMETRE_INDEX
			var nodeListXml:XML = sess.CURRENT_PERIMETRE.nodeList
			var xmlSelected:XMLList = nodeListXml.descendants("*").(@NID== idperimetre)
			var parentXml:Object = xmlSelected.parent()
		
		// Si nous sommes à la racine, on ajoute qu'un élément à l'affichage
			if(nodeListXml.@NID == idperimetre)
			{
				var x:CvPerimetreNodeVO = new CvPerimetreNodeVO()
				x.LBL = nodeListXml.@LBL
				x.NID = nodeListXml.@NID
				if(nodeListXml.@STC == 0)
					x.HASRIGHT = false
				else
					x.HASRIGHT = true
				arr.push(x)
			}	
		// Si nous sommes en sous périmètre, on récupère le parent
			else
			{
			// on ajoute le premier élément
				var x1:CvPerimetreNodeVO = new CvPerimetreNodeVO()
				x1.LBL = xmlSelected.@LBL
				x1.NID = xmlSelected.@NID
				if(xmlSelected.@STC == 0)
					x1.HASRIGHT = false
				else
					x1.HASRIGHT = true
				arr.push(x1)
			// tant qu'un parent existe, on ajoute un élément 	
				while(parentXml != null)
				{
					if(parentXml.@NID > 0)
					{
						var x2:CvPerimetreNodeVO = new CvPerimetreNodeVO()
						x2.LBL = parentXml.@LBL
						x2.NID = parentXml.@NID
						if(parentXml.@STC == 0)
							x2.HASRIGHT = false
						else
							x2.HASRIGHT = true
						arr.push(x2)
					}
					parentXml = parentXml.parent()	
				}
			}
			ccp.updateAffichage(arr)
		}
		protected function btnRapportClickHandler():void
		{
			var current_module:String = CvAccessManager.CURRENT_FUNCTION;
			if((CvAccessManager.CURRENT_FUNCTION  != 'FACT_REPORT') &&
				(CvAccessManager.CURRENT_FUNCTION  != 'FACT_REPORT_E0') &&
				(CvAccessManager.CURRENT_FUNCTION  != 'FACT_REPORT_PILOTAGE') &&
				(CvAccessManager.CURRENT_FUNCTION  != 'FACT_REPORT_360'))
			{
				ApiReportingSingleton.getInstance().afficherPOpup();
			}	
		}
/*	------------------------------------------------------------------------------------------
									GETTER SETTER 
------------------------------------------------------------------------------------------  */	
		[Bindable]
		public function set boolAfficherDates(value:Boolean):void
		{	
			_boolAfficherDates = value;
		}
		public function get boolAfficherDates():Boolean
		{
			return _boolAfficherDates;
		}	
//		public function getSelectedGroupIndex():int {
//			return listePerimetreWindow.getSelectedGroupIndex();
//		}
		public function getSelectedNode():XML {
			return listePerimetreWindow.getSelectedNode();
		}
	}
}