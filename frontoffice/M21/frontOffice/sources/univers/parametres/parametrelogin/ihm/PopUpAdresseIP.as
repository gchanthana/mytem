package univers.parametres.parametrelogin.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.formatters.NumberFormatter;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.util.ComposantInputAdrIp;
	
	


	public class PopUpAdresseIP extends PopUpAdresseIPIHM
	{
		private var format : NumberFormatter;
		private var _masque : String;
		private var _commentaire : String ;
		private var _old_APP_LOGIN_ID :int;
		private var _old_ADRESSE_IP :String;
		private var _old_MASQUE1 : String;
		
		//private var panelLogin : GestionLogin;
		private var composantIp : ComposantInputAdrIp;
		private var composantIp_perso : ComposantInputAdrIp;
		private var APP_LOGIN_ID : int;
		
		
		private var _isModif :Boolean=false;
		
		public var classeIp:ArrayCollection = new ArrayCollection(
                [ {label:ResourceManager.getInstance().getString('M21', 'Reseau_de_classe_A'), data:"255.0.0.0"}, 
                  {label:ResourceManager.getInstance().getString('M21', 'Reseau_de_classe_B'), data:"255.255.0.0"}, 
                  {label:ResourceManager.getInstance().getString('M21', 'Reseau_de_classe_C'), data:"255.255.255.0"},
                  {label:ResourceManager.getInstance().getString('M21', 'Ordinateur'), data:"255.255.255.255"},
                  {label:ResourceManager.getInstance().getString('M21', 'Personnalis_'),data:ResourceManager.getInstance().getString('M21', 'perso')}
                ]);
		public function PopUpAdresseIP(APP_LOGIN_ID : int,_ipPart1 : String= "-1" ,_ipPart2 : String = "-1",_ipPart3 : String= "-1",_ipPart4 : String= "-1",_masque :String= "",_commentaire : String = ""){
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			//this.panelLogin = panelLogin;
			this.APP_LOGIN_ID = APP_LOGIN_ID;
			this._masque = _masque;
			this._commentaire = _commentaire;
			this._old_APP_LOGIN_ID = APP_LOGIN_ID;
			this._old_ADRESSE_IP = _ipPart1+"."+_ipPart2+"."+_ipPart3+"."+_ipPart4;
			this._old_MASQUE1 = _masque;
				
			if(_ipPart1 == "-1")
			{				
				_isModif =false;
				composantIp = new ComposantInputAdrIp();
			}
			else
			{
				_isModif =true;
				composantIp = new ComposantInputAdrIp(_ipPart1,_ipPart2,_ipPart3,_ipPart4);
			}	
			
			 
		}
		private function initIHM(evt : FlexEvent):void
		{
			//composantIp_perso = new ComposantInputAdrIp();
			//canvasPerso.addChild(composantIp_perso);
			
			boxIp.addChild(composantIp);
			var labInfo :Label = new Label();
			labInfo.text=(ResourceManager.getInstance().getString('M21', '_Station_ou_Reseau_'));
			labInfo.setStyle("fontStyle","italic");
			boxIp.addChild(labInfo);
			
			labErreurMasque.setVisible(false);
			comboClasse.dataProvider=classeIp;
			comboClasse.addEventListener(Event.CHANGE,updateClasseIp);
			comboClasse.selectedIndex=0;
			updateClasseIp(null);
			btEnregistrer.addEventListener(MouseEvent.CLICK,enregistrer);
			btFeremer.addEventListener(MouseEvent.CLICK,fermer);
			addEventListener(CloseEvent.CLOSE,fermer);
			
			//Nouveau
			if(_isModif == false)
			{				
				
			}
			//Modif
			else
			{
				comboClasse.selectedIndex = 4;
				viewstackMasque.selectedIndex = 1;
				//inMasqueModif.text = _masque;
				inCommentaire.text=_commentaire;
				//callLater(initMasque);
				//inMasqueModif.text = _masque;
				inMasqueModif.text = _masque;
			}	
				
		}
		private function initMasque():void{
			
		}
		private function fermer(Evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
	private function updateClasseIp(evt : Event):void
	{
		if(comboClasse.selectedItem.data == ResourceManager.getInstance().getString('M21', 'perso')){
			
			viewstackMasque.selectedIndex = 1;
		}
		else
		{
			viewstackMasque.selectedIndex = 0;
			labIpInvar.text=comboClasse.selectedItem.data;
		}
	}
		
		private function enregistrer(evt : MouseEvent):void
		{
			
			if(_isModif == true)
			{
				enregistrerModif();
			}
			else
			{
				enregistrerNouveau();
			}
		}
		private function enregistrerModif():void
		{
			var classe : String
			//Alert.show(ResourceManager.getInstance().getString('M21', 'Modif'));
			var adresseIP : String = composantIp.inIp1.text+"."+composantIp.inIp2.text+"."+composantIp.inIp3.text+"."+composantIp.inIp4.text;
			if (comboClasse.selectedIndex == 4 )
			{
				//classe = composantIp_perso.inIp1.text+"."+composantIp_perso.inIp2.text+"."+composantIp_perso.inIp3.text+"."+composantIp_perso.inIp4.text; 
				classe = inMasqueModif.text;
			}
			else
			{
				classe = comboClasse.selectedItem.data ;
			}
			
			var op1:AbstractOperation =	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.login.GestionClient",
																	"UpdateAdresseIP",
																	processModifIp,null);
			//Alert.show("param envoyé : "+APP_LOGIN_ID+" ---- "+_old_ADRESSE_IP+" ---- "+_old_MASQUE1+" ---- "+adresseIP+" ---- "+inCommentaire.text+" ---- "+APP_LOGIN_ID+" ---- "+classe+" ---- "+32);
			RemoteObjectUtil.callService(op1,APP_LOGIN_ID,_old_ADRESSE_IP,_old_MASQUE1,adresseIP,inCommentaire.text,APP_LOGIN_ID,classe,32);
		}
		private function processModifIp(evt : Event):void{
			
			var msg: String = ResourceManager.getInstance().getString('M21', 'L_adresse_IP_a_bien__t__modifi_e_');
			ConsoviewAlert.afficherOKImage(msg);
			//panelLogin.initGridAdresseIP();
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.PARAMETRE_IP_COMPLETE));
			PopUpManager.removePopUp(this);
			//PopUpManager.removePopUp(this);
			
		}
		private function enregistrerNouveau():void
		{
			
			var classe : String ;
			var adresseIP : String = composantIp.inIp1.text+"."+composantIp.inIp2.text+"."+composantIp.inIp3.text+"."+composantIp.inIp4.text;
			
			if (comboClasse.selectedIndex == 4 )
			{
				//classe= composantIp_perso.inIp1.text+"."+composantIp_perso.inIp2.text+"."+composantIp_perso.inIp3.text+"."+composantIp_perso.inIp4.text; 
				classe = inMasqueModif.text;
			}
			else
			{
				classe = comboClasse.selectedItem.data ;
			}
			
			var op1:AbstractOperation =	RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.login.GestionClient",
																		"addAdresseIp",
			/*
																	"fr.consotel.consoprod.GestionClient",
																	"addAdresseIp",
			*/													
																	processNewIp,faultNewIp);
			
			op1.addEventListener(ResultEvent.RESULT,processNewIp);
			op1.addEventListener(FaultEvent.FAULT,faultNewIp);
			
			RemoteObjectUtil.callService(op1,APP_LOGIN_ID,adresseIP,inCommentaire.text,APP_LOGIN_ID,classe,32);
			//RemoteObjectUtil.callService(zzzz,null);
		}
		
		private function faultNewIp(evt : FaultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Erreur___n') + evt.toString());
		}
		
		private function processNewIp(evt : ResultEvent):void
		{
			//Alert.show(ResourceManager.getInstance().getString('M21', 'Enregistrement___Ok'));
			//panelLogin.initGridAdresseIP();
			var msg: String = ResourceManager.getInstance().getString('M21', 'L_adresse_IP_a_bien__t__enregistr_e_');
			ConsoviewAlert.afficherOKImage(msg);
			
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.PARAMETRE_IP_COMPLETE));
			PopUpManager.removePopUp(this);
		}
	
	}
}