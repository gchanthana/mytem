package catalogue.vo
{
	import mx.utils.ObjectUtil;
	
	import catalogue.util.CatalogueFormatteur;

	/* ==================
	PRIX_7	Null	
	PRIX_6	Null	
	PRIX_9	Null	
	BOOL_SPECIFIC	String Reference	1
	PRIX_8	Null	
	BOOL_ACCES	String Reference	1
	PRIX_1	Null	
	PRIX_3	Null	
	PRIX_2	Null	
	PRIX_5	Null	
	PRIX_4	Null	
	PRIX	String	39.99
	SHOW_PRIX	String Reference	1
	NBRECORD	String Reference	119
	NIVEAU	Null	
	REFERENCE	String	ATTDCU4G
	IDSEGMENT	String Reference	1
	TYPE	String Reference	Mobile
	NB_PROFILS	String Reference	1
	LIBELLE	String	DataConnect Unlimited for 4G LTE Devices for Government - ATTDCU4G
	THEME	String	Data subscriptions
	IDABONNEMENT	String	386787
	LIBELLE_DEFAULT	String	Rate Plan DataConnect  Government Unlimited for 4G LTE Laptops
	RN	String	2
	IDTHEME_PRODUIT	String	54
	===================== */
	[Bindable]
	public class AbonnementVO
	{
		private var _IDABO:int;
		private var _REFERENCE:String;
		private var _TYPE:String;
		private var _TARIF:Number;
		private var _LIBELLE:String;
		private var _NIVEAU:String;
		private var _NBRECORD:String;
		private var _THEME:String; 
		private var _LIBELLE_OP:String;      
		private var _SELECTED:Boolean=false;  
		private var _DEFAUT:Number=0;  
		private var _SEGMENT:Number=1;   
		private var _IDTHEMETYPE:int = 1;
			
		private var _LIBELLE_DEFAULT:String;
		private var _IS_SPECIFIC:int = 0;
		
		private var _ISVISIBLETOCOMMANDE:Boolean=false;
		
		private var _PRIX_1:Number = 0;
		private var _PRIX_2:Number = 0;
		private var _PRIX_3:Number = 0;
		private var _PRIX_4:Number = 0;
		private var _PRIX_5:Number = 0;
		private var _PRIX_6:Number = 0;
		private var _PRIX_7:Number = 0;
		private var _PRIX_8:Number = 0;
		private var _PRIX_9:Number = 0;
		
		private var _IDOPERATEUR:int = 0;
		private var _IDCLIENT:int= 0;
		
	
		
		
		public function AbonnementVO()
		{
		}
		
		

		public function fill(value:Object):void
		{
			this.IDABO					= value.IDABONNEMENT;
			this.REFERENCE				= value.REFERENCE;
			this.TYPE					= value.TYPE;
			this.LIBELLE				= value.LIBELLE;
			this.NIVEAU					= value.NIVEAU;
			this.NBRECORD				= value.NBRECORD;
			this.THEME					= value.THEME;    
			this.TARIF 					= value.PRIX; 
			this.DEFAUT	 				= value.NB_PROFILS;
			this.IDTHEMETYPE			= addTypeThemeID(value.BOOL_ACCES);
			this.SEGMENT				= value.IDSEGMENT;
			this.IS_SPECIFIC			= value.BOOL_SPECIFIC;
			this.ISVISIBLETOCOMMANDE	= formatIntToBoolean(int(value.SHOW_PRIX));
			this.LIBELLE_DEFAULT		= value.LIBELLE_DEFAULT;
			
			this.REFERENCE				= value.REFERENCE;
			
			this.PRIX_1					= (!isNaN(value.PRIX_1))?value.PRIX_1:0;
			this.PRIX_2					= (!isNaN(value.PRIX_2))?value.PRIX_2:0;
			this.PRIX_3					= (!isNaN(value.PRIX_3))?value.PRIX_3:0;
			this.PRIX_4					= (!isNaN(value.PRIX_4))?value.PRIX_4:0;
			this.PRIX_5					= (!isNaN(value.PRIX_5))?value.PRIX_5:0;
			this.PRIX_6					= (!isNaN(value.PRIX_6))?value.PRIX_6:0;
			this.PRIX_7					= (!isNaN(value.PRIX_7))?value.PRIX_7:0;
			this.PRIX_8					= (!isNaN(value.PRIX_8))?value.PRIX_8:0;
			this.PRIX_9					= (!isNaN(value.PRIX_9))?value.PRIX_9:0;
			
			this.IDOPERATEUR			= (!isNaN(value.IDOPERATEUR))?value.IDOPERATEUR:0;
		}
		
		
		
		private function addTypeThemeID(acces:int):int
		{
			var idthemetype:int = 1;//1->ABO, 2->OPT
			
			if(acces == 0)
				idthemetype = 2;
			
			return idthemetype;
		}
		
		private function formatIntToBoolean(intBool:int):Boolean
		{
			var bool:Boolean = false;
			
			if(intBool > 0)
				bool = true;
			
			return bool;
		}
		

		public function get PRIX_9():Number
		{
			return _PRIX_9;
		}

		public function set PRIX_9(value:Number):void
		{
			_PRIX_9 = value;
		}

		public function get PRIX_8():Number
		{
			return _PRIX_8;
		}

		public function set PRIX_8(value:Number):void
		{
			_PRIX_8 = value;
		}

		public function get PRIX_7():Number
		{
			return _PRIX_7;
		}

		public function set PRIX_7(value:Number):void
		{
			_PRIX_7 = value;
		}

		public function get PRIX_6():Number
		{
			return _PRIX_6;
		}

		public function set PRIX_6(value:Number):void
		{
			_PRIX_6 = value;
		}

		public function get PRIX_5():Number
		{
			return _PRIX_5;
		}

		public function set PRIX_5(value:Number):void
		{
			_PRIX_5 = value;
		}

		public function get PRIX_4():Number
		{
			return _PRIX_4;
		}

		public function set PRIX_4(value:Number):void
		{
			_PRIX_4 = value;
		}

		public function get PRIX_3():Number
		{
			return _PRIX_3;
		}

		public function set PRIX_3(value:Number):void
		{
			_PRIX_3 = value;
		}

		public function get PRIX_2():Number
		{
			return _PRIX_2;
		}

		public function set PRIX_2(value:Number):void
		{
			_PRIX_2 = value;
		}

		public function get PRIX_1():Number
		{
			return _PRIX_1;
		}

		public function set PRIX_1(value:Number):void
		{
			_PRIX_1 = value;
		}

		public function set LIBELLE_DEFAULT(value:String):void
		{
			_LIBELLE_DEFAULT = value;
		}
		
		public function get LIBELLE_DEFAULT():String
		{
			return _LIBELLE_DEFAULT;
		}
		
		public function set IS_SPECIFIC(value:int):void
		{
			_IS_SPECIFIC = value;
		}
		
		public function get IS_SPECIFIC():int
		{
			return _IS_SPECIFIC;
		}
		
		public function set ISVISIBLETOCOMMANDE(value:Boolean):void
		{
			_ISVISIBLETOCOMMANDE = value;
		}
		
		public function get ISVISIBLETOCOMMANDE():Boolean
		{
			return _ISVISIBLETOCOMMANDE;
		}
		
		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}

		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}
		public function set LIBELLE_OP(value:String):void
		{
			_LIBELLE_OP = value;
		}

		public function get LIBELLE_OP():String
		{
			return _LIBELLE_OP;
		}
		public function set IDABO(value:int):void
		{
			_IDABO = value;
		}
		public function get IDABO():int
		{
			return _IDABO;
		}
		
		public function set IDTHEMETYPE(value:int):void
		{
			_IDTHEMETYPE = value;
		}
		public function get IDTHEMETYPE():int
		{
			return _IDTHEMETYPE;
		}
		
		public function set TARIF(value:Number):void
		{
			_TARIF = value;
		}

		public function get TARIF():Number
		{
			return _TARIF;
		}
		
		
		public function set DEFAUT(value:Number):void
		{
			_DEFAUT = value;
		}
		
		public function get DEFAUT():Number
		{
			return _DEFAUT;
		}
		
		public function set SEGMENT(value:Number):void
		{
			_SEGMENT = value;
		}
		
		public function get SEGMENT():Number
		{
			return _SEGMENT;
		}
		
		
		public function set REFERENCE(value:String):void
		{
			_REFERENCE = value;
		}

		public function get REFERENCE():String
		{
			return _REFERENCE;
		}
		public function set TYPE(value:String):void
		{
			_TYPE = value;
		}

		public function get TYPE():String
		{
			return _TYPE;
		}
		public function set LIBELLE(value:String):void
		{
			_LIBELLE = value;
		}

		public function get LIBELLE():String
		{
			return _LIBELLE;
		}
		public function set NIVEAU(value:String):void
		{
			_NIVEAU = value;
		}

		public function get NIVEAU():String
		{
			return _NIVEAU;
		}
	
		public function set NBRECORD(value:String):void
		{
			_NBRECORD = value;
		}

		public function get NBRECORD():String
		{
			return _NBRECORD;
		}
		public function set THEME(value:String):void
		{
			_THEME = value;
		}

		public function get THEME():String
		{
			return _THEME;
		}
		
		
		public function get IDOPERATEUR():int
		{
			return _IDOPERATEUR;
		}
		
		public function set IDOPERATEUR(value:int):void
		{
			_IDOPERATEUR = value;
		}
		
		public function get IDCLIENT():int
		{
			return _IDCLIENT;
		}
		
		public function set IDCLIENT(value:int):void
		{
			_IDCLIENT = value;
		}
		
		public function get TARIF_DEVISE():String
		{
			//var formattedTarif:String = _TARIF+ ' €';
			var formattedTarif:String = CatalogueFormatteur.getInstance().operatorCurrency.format(Number(_TARIF));
			return formattedTarif;
		}
		
		public function toXMl():XML
		{
			
			var xmlplan:XML=
			<plan>
				<id>{this.IDABO}</id> 
				<operateurid>{this.IDOPERATEUR}</operateurid>				 
				<libelle>{this.LIBELLE}</libelle>   
				<reference>{this.REFERENCE}</reference>			 
				<showPrice>{this.ISVISIBLETOCOMMANDE?1:0}</showPrice>
				<price>{this.TARIF}</price>
				<price1>{this.PRIX_1}</price1>
				<price2>{this.PRIX_2}</price2>
				<price3>{this.PRIX_3}</price3>
				<price4>{this.PRIX_4}</price4>
				<price5>{this.PRIX_5}</price5>
				<price6>{this.PRIX_6}</price6>
				<price7>{this.PRIX_7}</price7>
				<price8>{this.PRIX_8}</price8>
				<price9>{this.PRIX_9}</price9>				 
			</plan>
			
			trace(ObjectUtil.toString(xmlplan));

			return xmlplan;
		}
			
	}
}