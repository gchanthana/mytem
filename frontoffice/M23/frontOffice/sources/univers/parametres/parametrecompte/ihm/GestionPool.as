package univers.parametres.parametrecompte.ihm
{
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	
	import univers.parametres.contrat.ihm.HistoriqueInfosClientOperateurIHM;
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	
	
	
	public class GestionPool extends VBox
	{
		public static const COLOR_MESSAGE : uint = 0x006CFF;
		
		public var ongletListeProfilCompte : ListeProfilCompteIHM;
		public var ongletOperateur : HistoriqueInfosClientOperateurIHM;
		public var ongletListeOperateur : ListeCompteOperateurCompteIHM;
		
		
		public function GestionPool()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			
		}
		private function creationCompleteHandler(evt : FlexEvent):void
		{
			ongletOperateur.addEventListener(GestionPoolEvent.UPDATE_LISTE_OPERATEUR,updateListeOperateurHandler);
		}
		private function updateListeOperateurHandler(evt : GestionPoolEvent):void
		{
			ongletListeOperateur.init();
		}
		public static function afficherMessage(msg:String,titre : String,labelBT:String="OK",color:uint = 0x000000):void
		{
			Alert.okLabel = "OK";
			var a : Alert = Alert.show(msg,titre,4,null,handleAlertClose);
			a.setStyle('color',color);
		}
 		public static function handleAlertClose(e:CloseEvent):void
		{
			Alert.okLabel = "OK";
		}

		
	}
}