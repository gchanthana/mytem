<cfcomponent output="false">

<cffunction name="sendMailLogin" access="remote">
	<cfargument name="URL" type="string">
	<cfargument name="login" type="string">
	<cfargument name="mdp" type="string">
	
	

	<cfmail from="mbp@consotel.fr" 
			to="#login#" 
			server="mail.consotel.fr" 
			port="26" 
			subject="votre identifiant mot de passe pour l’espace web « les packs utilisateurs »- Orange Business Services">
	Madame, Monsieur,

Nous avons le plaisir de vous confirmer l’identifiant et le mot de passe qui vous permettront d’accéder en toute sécurité à votre espace web personnalisé mis à disposition dans le cadre de  la solution «  les packs Utilisateurs ».
Grâce à cet outil pratique vous pouvez gérer, administrer, suivre, vérifier les informations sur la flotte des packs utilisateurs qui vous concerne. 

Il vous suffit de vous connecter via le lien ci-dessous :

URL : #URL#

identifiant  : #login#
mot de passe : #mdp#

Nous attirons votre attention sur l’importance des identifiants et mots de passe délivrés dans ce mail, veillez les conserver et les garder confidentiels. 

Vous disposez d’un guide et d’une démonstration en ligne pour vous permettre d’exploiter au mieux votre espace web personnalisé. En cas de besoin vous pouvez contacter votre Assistance au numéro XXXX

Restant à votre disposition pour toutes informations complémentaires, nous vous prions d’agréer, Madame, Monsieur, l’expression de nos considérations distinguées.
	
	
	
															Orange Business Services
Cet e-mail est envoyé automatiquement.
Nous vous invitons à ne pas y répondre : votre réponse ne pourrait être traitée.
			
				
			
	</cfmail>
</cffunction>

<cffunction name="sendMailDemande" access="remote">
	<cfargument name="login" type="string">
	<cfargument name="reference" type="string">

	<cfmail from="mbp@consotel.fr" 
			to="#login#" 
			server="mail.consotel.fr" 
			port="26" 
			subject="accusé réception de votre demande - référence N° #reference#">
	Madame, Monsieur,

Nous avons bien reçu votre demande passée via votre espace web «  les packs utilisateurs » et vous en remercions.

Nous vous encourageons à consulter ce même espace pour suivre l’évolution du traitement de votre demande. 

Nous attirons votre attention sur le faite qu’exceptionnellement certaines commandes peuvent être rejetées, dans ce cas, une alerte est visible dans votre tableau de suivi.  

Vous pouvez vous référez au guide administrateur pour plus d’information ou appeler votre service d’Assistance.  


Restant à votre disposition pour toutes informations complémentaires, nous vous prions d’agréer, Madame, Monsieur, l’expression de nos considérations distinguées.
	
	
	
															Orange Business Services 
Cet e-mail est envoyé automatiquement.
Nous vous invitons à ne pas y répondre : votre réponse ne pourrait être traitée.

			
	</cfmail>
</cffunction>
</cfcomponent>