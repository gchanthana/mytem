package gestioncommande.entity
{
	import flash.utils.describeType;
	
	
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class ItemSelected
	{

//VARIABLES GLOBALES---------------------------------------------------------------------

		public var RANDOM					:int = 0;
		
		public var RESILIATION				:ArrayCollection 		= new ArrayCollection();
		public var TERMINAUX				:ArrayCollection 		= new ArrayCollection();
		public var ACCESSOIRES				:ArrayCollection		= new ArrayCollection();
		public var ABO						:ArrayCollection 		= new ArrayCollection();
		public var OPTIONS					:ArrayCollection 		= new ArrayCollection();
		public var COLLABORATEUR			:Object 				= new Object();
		public var EMPLOYE_CONFIGURATION	:Configuration 			= new Configuration();
		public var LIGNES_ELIGIBLE			:Boolean				= false;
		
		public var ELEMENTS_CONFIGURATION	:ConfigurationElements = new ConfigurationElements()

//VARIABLES GLOBALES---------------------------------------------------------------------		
		
//CONSTRUCTEUR---------------------------------------------------------------------------

		public function ItemSelected()
		{
			this.RANDOM = randomNumber();
		}
		
//CONSTRUCTEUR---------------------------------------------------------------------------

		public function randomNumber():int
		{
			return Math.random() * 1000000;
		}
		
		public function copyItemSelected():ItemSelected
		{
			var copyItemSelected	:ItemSelected  	= new ItemSelected();
			var classInfo 			:XML 			= describeType(this);

            for each (var v:XML in classInfo..accessor)
            {
               if (this.hasOwnProperty(v.@name))
               {
               		copyItemSelected[v.@name] = this[v.@name];
               }else{
              // 	trace("-----> echec mapping ["+v.@name+"] ou valeur null" );
               }
            }   
            
            return copyItemSelected;         				 		
		}

	}
}
