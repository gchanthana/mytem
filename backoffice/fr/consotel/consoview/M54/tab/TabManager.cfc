<cfcomponent displayname="TabManager" hint="Gestion des onglets" output="false">
	
	<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
	<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
	<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
	<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	
	<!--- 
		créer un nouvel onglet
	 --->
	<cffunction name="createTab" access="remote" output="false" returntype="numeric"
				hint="créer un nouvel onglet">
		
		<cfargument name="nomTab" 		type="string" 	required="true">
		<cfargument name="positionTab" 	type="numeric" 	required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m54.createOnglet">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_app_loginid" value="#idUser#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idracine" value="#idRacine#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR2" type="in" variable="p_nom" value="#nomTab#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_position" value="#positionTab#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour><!--- 1 ou -1 --->
	</cffunction>
	
	<!--- 
		supprimer l'onglet choisi et tout ce qui s'y rattache(widget/KPI)
	 --->
	<cffunction name="deleteTab" access="remote" output="false" returntype="numeric"
				hint="supprimer l'onglet choisi et tout ce qui s'y rattache(widget/KPI)">
		
		<cfargument name="ongletid" type="numeric" required="true">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m54.deleteOnglet">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_ongletid" value="#ongletid#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour><!--- 1 ou -1 --->
	</cffunction>
	
</cfcomponent>