package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleDissocierCollabForExt implements IRuleDisplayItem
	{
		public function RuleDissocierCollabForExt()
		{
		}
		public function isDisplay():Boolean
		{
			var r3:IRuleDisplayItem = new HasCollaborateurAssociated();
			if(r3.isDisplay())
				return true;
			else
				return false;
		}
	}
}