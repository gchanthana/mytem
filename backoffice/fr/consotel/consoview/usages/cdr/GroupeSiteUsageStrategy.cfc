<!--- =========================================================================
Classe: GroupeSiteUsageStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
 Remarques: Cette classe sert juste à ramener la requête permettant de 
 remplir l'arbre utilisé dans la section Annuaire/Postes pour le
 périmètre GROUPE
========================================================================== --->
<cfcomponent name="GroupeSiteUsageStrategy" extends="SiteUsageStrategy">
	
	<cffunction name="getSynthese" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
			<cfif var[5] eq "">
				<cfset flag="true">
			<cfelse>
				<cfset flag="false">
			</cfif>
			<cfstoredproc datasource="#session.CDRDSN#" procedure="PKG_CV_GRCL_CDR.LAP_SYNTHESE">
		        <cfprocparam value="#var[1]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#var[3]#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#var[4]#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#var[5]#" cfsqltype="CF_SQL_VARCHAR" null="#flag#">
		        <cfprocresult name="qGetAppels">
		    </cfstoredproc>
		    
			<cfset obj=createObject("component","fr.consotel.consoview.usages.session")>
			<cfset obj.setData(qGetAppels,"listeLignes")>
			<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getDestinations" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL.LAP_DESTINATION">
	        <cfprocparam value="#session.dataUsage.listeLignes.CLIID[var[4]]#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam value="#session.dataUsage.listeLignes.SDAID[var[4]]#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam value="#var[2]#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam value="#var[3]#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetAppels">
	    </cfstoredproc>
	    <cfset obj=createObject("component","fr.consotel.consoview.usages.session")>
		<cfset obj.setData(qGetAppels,"listeDestinations")>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getListeAppels" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL.LAP_LISTE_APPELS">
			<cfprocparam value="#session.dataUsage.listeDestinations.CLIID[var[1]]#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam value="#session.dataUsage.listeDestinations.SDAID[var[1]]#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam value="#session.dataUsage.listeDestinations.PAYSCONSOTELID[var[1]]#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam value="#session.dataUsage.listeDestinations.PAYSID[var[1]]#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam value="#var[2]#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam value="#var[4]#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam value="#var[5]#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetAppels">
	    </cfstoredproc>
		<cfset obj=createObject("component","fr.consotel.consoview.usages.session")>
		<cfset obj.setData(qGetAppels,"listeAppelsParDestinations")>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getDetailAppels" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
	
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL.LAP_CDR1">
		        <cfprocparam value="#session.dataUsage.listeDestinations.CLIID[var[3]]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#session.dataUsage.listeDestinations.SDAID[var[3]]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#session.dataUsage.listeAppelsParDestinations.APPELE[var[1]]#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#session.dataUsage.listeDestinations.PAYSCONSOTELID[var[3]]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#session.dataUsage.listeDestinations.PAYSID[var[3]]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#var[5]#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#var[6]#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#var[2]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocresult name="qGetAppels">
		    </cfstoredproc>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfset obj=createObject("component","fr.consotel.consoview.usages.session")>
		<cfset obj.setData(qGetAppels,"detailAppels")>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getDetailAppelsByIdGroupe" access="remote" returntype="query" output="false"  >
		<cfargument name="index" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="flagdisplay" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datedeb" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datefin" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="chaine" required="false" type="string" default="" displayname="" hint="" />
		<cfif len(trim(chaine)) eq 0>
			<cfset lachaine="">
			<cfset flag="true">
		<cfelse>
			<cfset lachaine="#chaine#">
			<cfset flag="false">
		</cfif>
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL.LAP_CDR4">
		        <cfprocparam value="#index#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#datedeb#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#datefin#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#flagdisplay#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#chaine#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocresult name="qGetAppels">
		    </cfstoredproc>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getDetailAppelsByCLIID" access="remote" returntype="query" output="false"  >
		<cfargument name="index" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="flagdisplay" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datedeb" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datefin" required="false" type="string" default="" displayname="" hint="" />
		
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL.LAP_CDR3">
		        <cfprocparam value="#session.dataUsage.listeLignes.CLIID[index]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#session.dataUsage.listeLignes.SDAID[index]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#datedeb#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#datefin#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#flagdisplay#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocresult name="qGetAppels">
		    </cfstoredproc>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getDetailAppelsByDest" access="remote" returntype="query" output="true"  >
		<cfargument name="index" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="flagdisplay" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datedeb" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datefin" required="false" type="string" default="" displayname="" hint="" />

		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL.LAP_CDR2">
		        <cfprocparam value="#session.dataUsage.listeDestinations.CLIID[index]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#session.dataUsage.listeDestinations.SDAID[index]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#session.dataUsage.listeDestinations.PAYSCONSOTELID[index]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#session.dataUsage.listeDestinations.PAYSID[index]#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#datedeb#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#datefin#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocparam value="#flagdisplay#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocresult name="qGetAppels">
		    </cfstoredproc>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getData" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="index" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="flagdisplay" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datedeb" required="false" type="string" default="" displayname="" hint="" />
		<cfargument name="datefin" required="false" type="string" default="" displayname="" hint="" />
		<cfswitch expression="#var#">
			<cfcase value="ListeAppel">
				<cfreturn getDetailAppelsByDest(index,flagdisplay,datedeb,datefin)>
			</cfcase>
			<cfcase value="ListeDest">
				<cfreturn getDetailAppelsByCLIID(index,flagdisplay,datedeb,datefin)>
			</cfcase>
			<cfcase value="Liste">
				<cfreturn getDetailAppelsByIDgroupe(index,flagdisplay,datedeb,datefin)>
			</cfcase>
			<cfdefaultcase>
				<cfset q=querynew("a")>
				<cfreturn q>
			</cfdefaultcase>
		</cfswitch>
	</cffunction>
</cfcomponent>