package gestionparc.ihm.action.associer.actionAssocierCollabToExt
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierCollabToExtCommand implements ICommand
	{
		private var receiver:ActionAssocierCollabToExtReceiver=null;

		public function ActionAssocierCollabToExtCommand(rec:ActionAssocierCollabToExtReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
