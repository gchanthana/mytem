package composants.access.perimetre
{
    import mx.controls.treeClasses.TreeItemRenderer;
    import flash.utils.getQualifiedClassName;

    public class PerimetreTreeItemRendererRightTree extends TreeItemRenderer
    {
        public function PerimetreTreeItemRendererRightTree()
        {
            super();
        }

        public override function set data(value:Object):void
        {
            super.data = value;
            if(value != null)
            {
                if(value.@IS_ACTIVE)
                {
                    if(value.@IS_ACTIVE == 0)
                    {
                        setStyle("color", "#FF0000");
                    }
                    else
                    {
                        setStyle("color", "#000000");
                    }
                }
            }
        }

        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            //super.label.text =  "Le libellé";
        }
    }
}