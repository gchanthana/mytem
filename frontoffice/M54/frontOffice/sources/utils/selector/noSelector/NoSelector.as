package utils.selector.noSelector
{	
	import utils.SelectorAccueilAbstract;
	
	public class NoSelector extends SelectorAccueilAbstract
	{
		
		public function NoSelector(selector:Object=null)
		{
			super(selector);	
		}		
	}
}