package univers.inventaire.lignesetservices.applicatif
{
	import univers.inventaire.lignesetservices.vo.OngletAffectationVo;
	import flash.errors.IllegalOperationError;
	import univers.inventaire.lignesetservices.vo.Ligne;
	import mx.collections.ArrayCollection;
	
	
	[Event(name="getAffectationComplete")]
	[Event(name="updateAffectation")]
	
	
	
	
	[Bindable]
	public class GestionAffectationStrategy
	{
		
		//---const		
		public static const GETAFFECTATION_COMPLETE : String = "getAffectationComplete";
		public static const UPDATEAFFECTATION_COMPLETE : String = "updateAffectation";			
		//--
		
		
		
		public function GestionAffectationStrategy(myLigne : Ligne){
			_ligne = myLigne;
		}
		
		
		
		
		private var _dataAffectation : OngletAffectationVo;
		public function get dataAffectation():OngletAffectationVo{
			return _dataAffectation;
		}
		public function set dataAffectation(data : OngletAffectationVo):void{
			_dataAffectation = data;
		}
		
				
		
		private var _ligne : Ligne;
		public function get ligne():Ligne{
			return _ligne;
		}
		public function set ligne(st : Ligne):void{
			_ligne = st;
		}
		
		private var _listeCompte : ArrayCollection = new ArrayCollection();
		public function get listeCompte():ArrayCollection{
			return _listeCompte;
		}
		public function set listeCompte(mylist : ArrayCollection):void{
			_listeCompte = mylist;
		}
		
		
		
		
		public function getInfosAffectation():void{	
			throw IllegalOperationError("Methode Abtsraite");
		}		
		
		public function updateInfosAffectation(data : OngletAffectationVo):void{			
			throw IllegalOperationError("Methode Abtsraite");
		}
		
		public function getListeSousComptes():void{
			throw IllegalOperationError("Methode Abtsraite");
		}
		
	}
}