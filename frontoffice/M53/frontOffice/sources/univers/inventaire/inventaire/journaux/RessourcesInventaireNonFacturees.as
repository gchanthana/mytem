package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import composants.util.DateFunction;
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.ConsoviewFormatter;
	import mx.controls.Alert;
	
	
	
	/**
	 * Journal des ressources qui sont dans l'inventaire mais qui ne sont pas facturé (qui n'apparaissent dans aucune ligne de facturation)
	 * */
	public class RessourcesInventaireNonFacturees extends RessourcesInventaireNonFactureesIHM implements IJournal
	{
		//Date de début de périiode
		private var dateDebut : String;
		
		//Date de fin de période
		private var dateFin : String;
		
				
		//ArrayCollection contenant les lignes du journal
		[Bindable]
		private var elementsDuJournal : ArrayCollection;
		
		//Reference vers la méthode distante qui ramène les lignes du journal
		private var opElementDuJournal : AbstractOperation;
		
		//les information du noeud (inutilisé ici)
		private var _nodeInfos : Object;
		
		
		/**
		 * Constructeur
		 * */			
		public function RessourcesInventaireNonFacturees()
		{
			//TODO: implement function
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		/**
		 * Affecte les infos sur le noeud au journal
		 * */
		 public function set nodeInfos(infos : Object):void{
		 	_nodeInfos = infos;
		 }
			
		/**
		 * Recharge les données du journal
		 * */
		public function refresh():void{
			//tente de couper le remoting		 
			if((dateDebut != null) && (dateFin != null))
				chargerDonnees();
			 
		}
		
		/**
		* Fonction qui permet d'affecter la période sur laquelle on souhaite travailler
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		* 
		* */
		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
															
			var formMoisD : String = moisDebut.substr(0,2) 
									+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
									+ moisDebut.substr(6,4);
									
			var formMoisF : String = moisFin.substr(0,2)
								   	+ " " 
								   	+ DateFunction.moisEnLettre(parseInt(moisFin.substr(3,2),10)-1) 
								   	+ " " 
								   	+ moisFin.substr(6,4);

			labelPeriode.text = "Factures émises du " + formMoisD + " au " + formMoisF;					
		}
		
		/**
		* Gere le changement de périmetre
		* */
		public function onPerimetreChange():void{
			//tente de couper le remoting
			
			//init filtre
			txtFiltre.text = "";
			
			//init periode
			initPeriode();
			setSelectedPeriode(dateDebut,dateFin);	
			
			//init le data provider
			elementsDuJournal.source = null
			elementsDuJournal.refresh();
			
			//données
			chargerDonnees();
			
		}
		
		/**
		 * Interrompt les remotings en cours
		 * Méthode non utilisée
		 * */
		public function cancelRemotings():void{
			try{
				 
			}catch(e : Error){
				trace("ok");
			}
		}
		
//--- INITIALISATION ---------------------------------------------------------------------------------------------------------------------------
	
		//Initialisation de l'IHM
		//et charge les données pour la periode
		private function initIHM(fe : FlexEvent):void{
			//Periode
			initPeriode();				
			myPeriodeSelector.addEventListener("periodeChange",updatePeriode); 				
			setSelectedPeriode(dateDebut,dateFin);							
						
			//Filtre
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
			
			//grid					 
			numligne.labelFunction = formateLigne;		 					
			chargerDonnees();		
		}
		
		//Initialisation de la periode
		private function initPeriode():void{
			var periodeArray : Array = myPeriodeSelector.getTabPeriode();
		    var len : int = periodeArray.length;
		    var month : AMonth;
		    					   
			month = periodeArray[len-2];	
							
			dateDebut = month.getDateDebut();	
			dateFin = month.getDateFin();
		}
				
//--- FIN INITIALISATION -----------------------------------------------------------------------------------------------------------------------

//--- FORMATAGE ---------------------------------------------------------------------------------------------------------------------------------				

		//Formate les numéros de téléphone  d'une colonne de DataGrid
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}

//--- FIN FORMATAGE ---------------------------------------------------------------------------------------------------------------------------------				
	

//--- PERIODE SELECTOR -------------------------------------------------------------------------------------------------------------------------
		
		//Handler du chagement de date
		//Met à jour la date pour le journal, affiche la date et charge les données pour cette date
		private function updatePeriode(pe : PeriodeEvent):void{
			
			dateDebut = pe.moisDeb;
			dateFin = pe.moisFin;		
			setSelectedPeriode(dateDebut,dateFin);
			chargerDonnees();	
			
		}
		
//--- FIN PERIODE SELECTOR ---------------------------------------------------------------------------------------------------------------------
		
				
//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------
		
		
		//Gere le filtre du tableau
		private function filtrerGrid(ev :Event):void{
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();		
		}
		
		//filtre sur les attributs OPNOM, LIBELLE_PRODUIT, SOUS_TETE des elements du Tabelau
		private function filterFunc(value : Object):Boolean{				
			if ((String(value.OPNOM.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.LIBELLE_PRODUIT.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.SOUS_TETE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1))
			{
				return (true);
			} else {
				return (false);
			}	
		}
		
		
		
//--- FIN FILTRE -------------------------------------------------------------------------------------------------------------------------------

//--- GRID -------------------------------------------------------------------------------------------------------------------------------------
		
//--- FIN GRID ---------------------------------------------------------------------------------------------------------------------------------

//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		
		//Charge les donnees du Journal
		private function chargerDonnees():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			 
			lblError.visible = false;		
			opElementDuJournal = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getRessourcesInventaireNonFacturee",
																				chargerDonneesResultHandler);						
			RemoteObjectUtil.callService(opElementDuJournal,
										idGroupeMaitre,
										dateDebut,
										dateFin);		
		 
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerDonnees' à l'ArrayCollection 'elementsDuJournal' et les affiches
		private function chargerDonneesResultHandler(re :ResultEvent):void{		
			elementsDuJournal = re.result as ArrayCollection;	
			if (elementsDuJournal.length > 0){
				txtFiltre.editable = true;				
			} else {
				txtFiltre.editable = false;
				lblError.visible = true;
			}
			myGrid.dataProvider = elementsDuJournal;
			elementsDuJournal.refresh();			 
		}
		
//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------


	}
}