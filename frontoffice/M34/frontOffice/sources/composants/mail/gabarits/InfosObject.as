package composants.mail.gabarits
{
	public dynamic class InfosObject extends Object
	{
		public var NOM_EXPEDITEUR : String = CvAccessManager.getSession().USER.NOM;
		public var PRENOM_EXPEDITEUR : String = CvAccessManager.getSession().USER.PRENOM;
		public var MAIL_EXPEDITEUR : String = CvAccessManager.getSession().USER.EMAIL;
		public var SOCIETE_EXPEDITEUR : String = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
		
		public function InfosObject()
		{
			super();
		}
		
	}
}