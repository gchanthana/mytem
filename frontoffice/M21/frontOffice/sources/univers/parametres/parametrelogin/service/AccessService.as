package univers.parametres.parametrelogin.service
{
	import mx.rpc.AbstractOperation;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	[Bindable]
	public class AccessService
	{
				
		private var _model:AccessModel;
		public function get model():AccessModel
		{
			return _model	
		}
		
		private var _handler:AccessHandler;
		public function get handler():AccessHandler
		{
			return _handler	
		}
		
		public function AccessService()
		{
			_model = new AccessModel();
			_handler = new AccessHandler(model);
		}
		
		public function getAccess(mod:String,idUser:Number,idNoeud:Number):void
		{	
			model.module=mod;
			
			if(model.module != '')
			{
				model.resetAccess();	
			}
			else
			{
				model.resetAccessTotal();
			}
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.Access",
				"getUserAccess",
				handler.getAccessHandler);
			RemoteObjectUtil.callService(opData,mod,idUser,idNoeud);
			
		}
		
		public function activeParam():void
		{			
						
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.Access",
				"activeParam",
				handler.activeParamHandler);
			RemoteObjectUtil.callService(opData,this.model.access);
			
		}
		
	}
}