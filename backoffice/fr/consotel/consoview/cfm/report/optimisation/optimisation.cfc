<cfcomponent>
	<cffunction name="RapportRTCL" returnType="any">	
		<cfargument name="le_idperimetre" 	type="String"	>
		<cfargument name="le_datedeb" 		type="String"	>
		<cfargument name="le_datefin" 		type="String"	>
		
		<cfargument name="format"			type="String"	>
		<cfargument name="appName"			type="String"	>
		<cfargument name="moduleName"		type="String"	>
		<cfargument name="codeRapport"		type="String" 	>
					
		<cfset SUPPORT_EMAIL="production@consotel.fr">
		<cfset SERVICE_IT_EMAIL="dev@consotel.fr">
		<cfset MAIL_SERVER="192.168.3.119:26">
		
	<!--- Instanciation et Initialisation du Service --->
		<cfset developerReportService=createObject("component","fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService")>
		
		
	<!--- Initialisation du Service avec les paramètres obligatoires : IDRACINE, XDO BI Publisher --->
		<cfset initStatus=developerReportService.init(SESSION.PERIMETRE.ID_GROUPE,
					"/consoview/facturation/rapport/EtatConsoGsm/EtatConsoGsm.xdo","ConsoView DEV","CONSOTEL","MODULE RAPPORT-CONTAINER")>
	
	
	<!--- Passages des paramètres avec leur(s) valeur(s) --->
		<cfset valeursPourIdRacine=arrayNew(1)>
		<cfset valeursPourIdRacine[1]=SESSION.PERIMETRE.ID_GROUPE>
		<cfset setIdRacine=developerReportService.setParameter("G_IDRACINE",valeursPourIdRacine)>
		
		<cfset valeursPourIdPerimetre=arrayNew(1)>
		<cfset valeursPourIdPerimetre[1]=arguments.le_idperimetre>
		<cfset setIdPerimetre=developerReportService.setParameter("S_IDPERIMETRE",valeursPourIdPerimetre,"CLIENT")>
		
		<cfset valeursPourDatedeb=arrayNew(1)>
		<cfset valeursPourDatedeb[1]=arguments.le_datedeb>
		<cfset setDatedeb=developerReportService.setParameter("Q_SHORT_DEB",valeursPourDatedeb,"CLIENT")>
		
		<cfset valeursPourDatefin=arrayNew(1)>
		<cfset valeursPourDatefin[1]=arguments.le_datefin>
		<cfset setDatefin=developerReportService.setParameter("Q_SHORT_FIN",valeursPourDatefin,"CLIENT")>
		
		<cfset valeursPourUserId=arrayNew(1)>
		<cfset valeursPourUserId[1]=SESSION.USER.CLIENTACCESSID>
		<cfset setUserId=developerReportService.setParameter("Q_USERID",valeursPourUserId)>

	<!--- Mise à jour des paramètres de restitution --->
		<cfset filename="RTCL1 - " & lsDateFormat(now(),"MM-YYYY")>
		<cfset extension="NULL">
		<cfif UCase(arguments.FORMAT) EQ "EXCEL">
			<cfset extension="xls">
		<cfelseif UCase(arguments.FORMAT) EQ "PDF">
			<cfset extension="pdf">
		<cfelseif UCase(arguments.FORMAT) EQ "CSV">
			<cfset extension="csv">
		<cfelse>
				<cfset extension="xml">
		</cfif>

		<cfset setOutputStatus=developerReportService.setOutput("cv",TRIM(LCASE(arguments.FORMAT)),extension,filename,"JOB - REPORT TEST")>
		<cflog text="format extension #arguments.FORMAT#">

	<!--- RUN DU RAPPORT --->
		<cftry>
			<cfset JOB_ID=developerReportService.runTask(SESSION.USER.CLIENTACCESSID)>
		<cfcatch type="any">
				<cfmail from="consoview@consotel.fr"
							to="#SERVICE_IT_EMAIL#"
							subject="EXCEPTION: Etat des consommations par familles par #SESSION.USER.EMAIL# (BackOffice : #CGI.SERVER_NAME#)"
							type="html"
							charset="utf-8"
							wraptext="72"
							server="#MAIL_SERVER#">
					<cfdump var="#developerReportService.getInitInfos()#" label="DUMP INIT INFOS"><br />
					<cfdump var="#developerReportService.getOutputInfos()#" label="DUMP OUTPUT INFOS"><br />
					<cfdump var="#developerReportService.getParametersInfos()#" label="DUMP PARAMETERS INFOS"><br />
					DUMP XML_TASK :<br />
					<cfdump var="#developerReportService.getDebug('XML_TASK').TASK#"><br />
					<cfdump var="#CFCATCH#" label="DUMP CFCATCH">
				</cfmail>
		</cfcatch>
		</cftry>
		<cfreturn 1>
	</cffunction>
</cfcomponent>