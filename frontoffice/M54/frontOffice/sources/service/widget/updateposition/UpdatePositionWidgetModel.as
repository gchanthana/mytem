package service.widget.updateposition
{
	import event.widget.WidgetEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.rpc.events.ResultEvent;
	
	internal class UpdatePositionWidgetModel extends EventDispatcher
	{
		private var _widgetNumTab_id:Number;

		public function UpdatePositionWidgetModel()
		{
		}

		public function get widgetNumTab_id():Number
		{
			return _widgetNumTab_id;
		}

		internal function updateValueWidget(res:ResultEvent):void
		{
			_widgetNumTab_id=res.result[0];
			
			if(_widgetNumTab_id > 0)
			{
				dispatchEvent(new WidgetEvent(WidgetEvent.UPDATE_PARAMTAB_WIDGET_EVENT));/**  capture dans WidgetAbstact */
			}
			
		}
	}
}
