<cfcomponent output="false">

	<!--- RECUPERE TOUTES LES LIGNES SUIVANT L'OPERATEUR --->
	
	<cffunction name="fournirListeLignesByOperateur" access="remote" returntype="Query" output="false" hint="RECUPERE TOUTES LES LIGNES SUIVANT L'OPERATEUR">
		<cfargument name="idRacine" 		required="true" type="Numeric"/>
		<cfargument name="idPool" 			required="true" type="Numeric"/>
		<cfargument name="idOperateur" 		required="true" type="Numeric"/>
		<cfargument name="clef" 			required="true" type="String"/>
		<cfargument name="eligibilite" 		required="true" type="Numeric"/>
		<cfargument name="idrecherche" 		required="true" type="Numeric"/>
		<cfargument name="idsegment" 		required="true" type="Numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getlignesbyoperateur">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idracine" 		value="#idRacine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 		value="#idPool#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperateur" 	value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" 	variable="p_clef" 			value="#clef#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_eligibilite" 	value="#eligibilite#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idrecherche" 	value="#idrecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_segment" 		value="#idsegment#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>

</cfcomponent>