package datalert.event
{
	import flash.events.Event;
	
	public class NewLigneEvent extends Event
	{
		
		public static const CHECK_SINGLE_DOUBLONS:String = 'CHECK_SINGLE_DOUBLONS';
		public static const CHECK_VALIDATION:String = 'CHECK_VALIDATION';
		public static const REQUESTS_LAUNCHED:String = 'REQUESTS_LAUNCHED';
		public static const REQUESTS_FINISHED:String = 'REQUESTS_FINISHED';
		
		private var _objectReturn:Object;
		
		public function NewLigneEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,objectReturn : Object = null)
		{
			this.objectReturn = objectReturn;
			super(type, bubbles, cancelable);
		}
		
		
		public function set objectReturn(value:Object):void
		{
			_objectReturn = value;
		}
		
		public function get objectReturn():Object
		{
			return _objectReturn;
		}
		
		override public function clone():Event
		{
			return new DatalertEvent(type,bubbles,cancelable,objectReturn)
		}
	}
}