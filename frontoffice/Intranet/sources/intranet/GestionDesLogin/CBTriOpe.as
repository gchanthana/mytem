package intranet.GestionDesLogin
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class CBTriOpe extends ComboBox
	{
		private var TabOperateur:ArrayCollection=new ArrayCollection(['Tous']);
		private var TabOpeOk:Boolean = false;
		
		public function CBTriOpe()
		{
			super();
			this.addEventListener(FlexEvent.INITIALIZE,init)
		}

// REMPLIR LA COMBOBOX PAR LA LISTE DES OPERATEURS		
		public function init(e:FlexEvent):void{
			if(!this.TabOpeOk){				
				var tmp:AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestiondeslogins.GestionDesLogins",
					"getListeOperateur",
					OpeOk,OpeNo);
				RemoteObjectUtil.callService(tmp);
				this.TabOpeOk=true;
			}else{
				var dataSortField:SortField = new SortField();
            	dataSortField.name = "NOM";
            	var DataSort:Sort = new Sort();
            	DataSort.fields = [dataSortField];
				
				this.dataProvider=this.TabOperateur;
				this.labelField="NOM";
			}
		}
		
		public function OpeOk(e:ResultEvent):void{
			var item:Object=new Object();
			item.OPERATEURID=0;
			item.NOM="** Tous **";
			
			var dataSortField:SortField = new SortField();
            dataSortField.name = "NOM";
            var DataSort:Sort = new Sort();
            DataSort.fields = [dataSortField];            
		
			this.TabOperateur=e.result as ArrayCollection;
			this.TabOperateur.addItemAt(item,0);
			this.TabOperateur.sort=DataSort;
			this.TabOperateur.refresh();

			this.dataProvider=this.TabOperateur;
			this.labelField="NOM";
			dispatchEvent(new Event("LISTE_OP_LOADED_EVENT"));

		}
		
		public function OpeNo(e:FaultEvent):void{
			trace("Erreur CBTriOpe: "+e.message);
		}
	}
}