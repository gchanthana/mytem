package service.destinataires.listedestinataireprofil
{
	import event.DestinataireEvent;
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import vo.DestinataireVo;
	

	internal class ListeDestinatairesProfilModel  extends EventDispatcher
	{
		private var _destinatairesProfil :ArrayCollection;
		
		public function ListeDestinatairesProfilModel()
		{
			_destinatairesProfil = new ArrayCollection();	
		}
		public function get destinatairesProfil():ArrayCollection
		{
			return _destinatairesProfil;
		}
		
		internal function listeDestinatairesProfil(value:ArrayCollection):void
		{	
			_destinatairesProfil.removeAll();// vider l'ArrayCollection
			
			var unDestinataire:DestinataireVo;	
			
			if(value.length > 0)
			{
				for(var i:int=0;i<value.length;i++)
				{
					unDestinataire = new DestinataireVo();
					unDestinataire.fill(value[i]);
					_destinatairesProfil.addItem(unDestinataire); 
				}
			}			
			this.dispatchEvent(new DestinataireEvent(DestinataireEvent.LISTE_DESTINATAIRE_PROFIL_EVENT));
		}
	}
}