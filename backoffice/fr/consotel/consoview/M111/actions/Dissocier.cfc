﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>

	
	<!--- ----------------- COLLABORATEUR ----------------- --->
	 	<!--- EQP ---> 
	 
	<cffunction name="reprendreEqpFromCollab1" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="reprendreEqpFromCollab2" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="reprendreEqpFromCollab3" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
		<!--- LIGNE SIM --->
	<cffunction name="reprendreLigneSimFromCollab1" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- reprendre la ligne et l'extensiion  --->
	
	<cffunction name="reprendreLigneExtFromCollab1" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"			required="true" type="string" />
		<cfargument name="ITEM2"			required="true" type="string" />
		<cfargument name="ID_TERMINAL"		required="true" type="Numeric" />
		<cfargument name="ID_EXT"			required="true" type="any" />
		<cfargument name="ID_EMPLOYE"		required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"		required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"		required="true" type="string" />
		<cfargument name="IS_PRETER"		required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="reprendreLigneSimFromCollab2" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	
		<cffunction name="reprendreLigneExtFromCollab2" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="reprendreLigneSimFromCollab3" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- reprendre que l'extension --->
	<cffunction name="reprendreExtFromCollab" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
		<!--- LIGNE --->
	<cffunction name="reprendreLigneFromCollab1" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="reprendreLigneFromCollab2" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="reprendreLigneFromCollab3" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneCollab">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<!--- ----------------- EQUIPEMENT ----------------- --->
		<!--- COLLAB --->
	<cffunction name="dissocierCollabFromEqp1" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierCollabFromEqp2" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierCollabFromEqp3" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="3" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierCollabFromEqp4" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(4,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- LIGNE SIM --->
	<cffunction name="dissocierLigneSimFromEqp1" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- ligne et extension --->
	<cffunction name="dissocierLigneExtFromEqp" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierLigneSimFromEqp2" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierLigneExtFromEqp2" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"			required="true" type="string" />
		<cfargument name="ITEM2"			required="true" type="string" />
		<cfargument name="ID_TERMINAL"		required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierLigneSimFromEqp3" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		

		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="dissocierLigneExtFromEqp3" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"	required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		

		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- LIGNE --->
	<cffunction name="dissocierLigneFromEqp1" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierLigneFromEqp2" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierLigneFromEqp3" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<!--- 
		dissocierSousEqpFromEqp3 : dissocie un sous équipement de l'équipement
	 --->
	<cffunction name="dissocierSousEqpFromEqp1" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSsEqpTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(29,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(29,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierSousEqpFromEqp2" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSsEqpTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 	cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(29,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(29,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierSousEqpFromEqp3" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSsEqpTerm">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 			cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 	cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(29,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(29,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- ----------------- LIGNE ----------------- --->
		<!--- COLLAB --->
	<cffunction name="dissocierCollabFromLigne" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabLigne">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(5,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
		<!--- EQUIPEMENT --->
	<cffunction name="dissocierEqpFromLigne1" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermLigne">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierEqpFromLigne2" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<!--- SIM --->
	<cffunction name="dissocierSimFromLigne" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimLigne">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierExtFromLigne" access="remote" returntype="numeric">
		
		<cfargument name="iddestination" required="true" type="Numeric" />
		<cfargument name="ITEM1"		 required="true" type="string" />
		<cfargument name="ITEM2"		 required="true" type="string" />
		<cfargument name="ID_TERMINAL"	 required="true" type="Numeric" />
		<cfargument name="ID_EXT"	     required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	 required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	 required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	 required="true" type="string" />
		<cfargument name="IS_PRETER"	 required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierSimLigne">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="dissocierEqpFromLigneSim1" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierEqpFromLigneSim2" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<cffunction name="dissocierEqpFromLigneSim3" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmptmpresult = logger.sendMailAction(9,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	<!--- ----------------- SIM ----------------- --->
		<!--- COLLAB --->
	<cffunction name="dissocierCollabFromSim1" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierCollabFromExt1" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmptmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="dissocierCollabFromSim2" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="dissocierCollabFromExt2" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" any="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierCollabFromSim3" access="remote" returntype="numeric">
		
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierCollabFromExt3" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierCollabSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result"			cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(6,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
		<!--- EQP --->
	<cffunction name="dissocierEqpFromSim1" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierEqpFromExt1" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="dissocierEqpFromSim2" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierEqpFromExt2" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="2" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction name="dissocierEqpFromSim3" access="remote" returntype="numeric">
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="dissocierEqpFromExt3" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierTermSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="1" 				cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" 		cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
		
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(12,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	
	<!--- LIGNE --->
	<cffunction name="dissocierLigneFromSim" access="remote" returntype="numeric">
				<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"	required="true" type="string" />
		<cfargument name="ITEM2"	required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"	required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="dissocierLigneFromExt" access="remote" returntype="numeric">
		
		<cfargument name="iddestination"	required="true" type="Numeric" />
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ITEM2"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_EXT"		required="true" type="any" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cflog text="--------------- DEBUT DISSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ITEM2 : #ITEM2#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_EXT : #ID_EXT#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
		
			<cftry>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.dissocierLigneSim">
					<cfprocparam  value="#iddestination#" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  variable="result" cfsqltype="CF_SQL_INTEGER"  type="out">
				</cfstoredproc>
				
			<cfcatch type="any" >
				<cfset result = -10/>
			</cfcatch>
			</cftry>
			
		<cfif result eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				<cfset tmpresult = logger.logAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset tmpresult = logger.sendMailAction(10,1,ITEM1,ITEM2,ID_TERMINAL,ID_EXT,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>		
		
		<cflog text="action = #result#">
		
		<cfreturn result>
	</cffunction>

</cfcomponent>