<cfcomponent output="false">

	<cfset exce	=" [API SMS]-Exception dans la methode : " >
	<cfset newLine = Chr(13) & Chr(10)>

	<!---  table mnt_event - tous les events --->
	<cfset idMntRequiredParam	   = 210>
	<cfset idMntInvalidParam       = 211>
	<cfset idMntInvalidPhoneNum    = 212>
	<cfset idMntInvalidSMSContent  = 213>
	<cfset idMntInvalidOperator    = 214>
	<cfset idMntInvalidUUID 	   = 215>
	<cfset idMntServiceIndisonible = 216>

	<!--- permet de traiter l'exception et former un string  --->
	<cffunction access="public" name="bodyException" returntype="String">
		<cfargument name="cfCatchObject" type="any"  required="true">
		<cfset message="">
		<cfset detail="">
		<cfset tag_context="">
		<cftry>
			<cfset message="Error Handling FAILED. Cause : #cfCatchObject.Message# #newLine#" >
			<cfset detail= "Detail : #cfCatchObject.ExtendedInfo# #newLine#" >

			<cfsavecontent variable="bodyContext">
				<cfoutput>
					<cfif structKeyExists(cfCatchObject,"TagContext")>
						<cfdump var="#cfCatchObject.TagContext#" format="text">
					</cfif>
				</cfoutput>
			</cfsavecontent>

			<cfset body= "#message##detail#Tag content : #trim(bodyContext)#">
			<cfreturn body>
		<cfcatch>
			<cflog type="error" text="[API SMS logger] Error: #cfCatchObject.message# - Detail: #cfCatchObject.detail#">
		</cfcatch>
		</cftry>
	</cffunction>

	<!--- permet d'enregistrer le log dans la table mt_detail  --->
	<cffunction access="public" name="saveInMntDetail" output="true" returntype="void">
		<cfargument name="uid" 		type="string"  required="true">
		<cfargument name="subject" 	type="string"  required="true">
		<cfargument name="body" 	type="string"  required="true">
		<cfargument name="idmnt" 	type="numeric" required="true">
		<cftry>
			<cfset mLog=createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(61)> <!--- id=61 le catégorie pour API SMS  --->
			<cfset mLog.setIDMNT_ID(idmnt)>
			<cfset mLog.setSUBJECTID(subject)>
			<cfset mLog.setBODY(body)>
			<cfset mLog.setUUID(uid)>
			<cfset createObject('component','fr.consotel.api.sms.ApiSmsLogger').logIt(mLog)>
			<cfcatch type="any">
				<cflog type="error" text="[API SMS logger] Error: #CFCATCH.message# - Detail: #CFCATCH.detail#">
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- get idmnt detail par ErrorCode  --->
	<cffunction access="public" name="getIdEventByErrorCode" output="true" returntype="any">
		<cfargument name="errorCode" 		type="string"  required="true">

		<cfswitch expression="#errorCode#">
			<cfcase value="apiSMS_01">
				<cfreturn idMntRequiredParam >
			</cfcase>
			<cfcase value="apiSMS_02">
				<cfreturn idMntInvalidParam>
			</cfcase>
			<cfcase value="apiSMS_03">
				<cfreturn idMntInvalidPhoneNum>
			</cfcase>
			<cfcase value="apiSMS_04">
				<cfreturn idMntInvalidSMSContent>
			</cfcase>
			<cfcase value="apiSMS_05">
				<cfreturn idMntInvalidOperator>
			</cfcase>
			<cfcase value="apiSMS_07">
				<cfreturn idMntInvalidUUID>
			</cfcase>
			<cfcase value="apiSMS_099">
				<cfreturn idMntServiceIndisonible>
			</cfcase>
		</cfswitch>

	</cffunction>

</cfcomponent>