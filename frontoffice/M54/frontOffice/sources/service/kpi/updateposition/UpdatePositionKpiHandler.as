package service.kpi.updateposition
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class UpdatePositionKpiHandler
	{
		private var _model:UpdatePositionKpiModel;
		
		public function UpdatePositionKpiHandler(_model:  UpdatePositionKpiModel):void
		{
			this._model = _model;
		}	
		internal function UpdatePositionKpiResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValueKpi(evt);
			}
		}
	}
}