package univers.parametres.perimetres.rightSide
{
	import flash.events.Event;
	
	import mx.containers.Canvas;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.perimetres.main;

	public class AssignementAutoImpl extends Canvas
	{
		public function get modeEcriture ():Boolean{ return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)}		
		public function get modeRestreint ():Boolean{
			return false
		}
		public function set modeRestreint (b:Boolean):void{
		}
		
		public function AssignementAutoImpl()
		{
			super();
		}		
		
		public function onbtProprieteClicked(event : Event):void
		{	
		}
		
		public function onbtAfficherLignesClicked(event: Event):void
		{
		}
		
		public function onbtExproterClicked(event: Event):void
		{
		}
		
		public function mainDateChanged():void
		{
		}
		
		public function mainOrganisationChanged():void
		{
		}
		
		public function mainTreeSelectionChanged():void
		{		
		}
		
		public function organisationChanged():void
		{			
		}
		
		public function treeSelectionChanged():void
		{
		}
		
		public function selectedLineChanged():void
		{	
		}
		
		public function fillOrganisation():void
		{	
		}
		public function assignLineResult(event: ResultEvent):void
		{
		}
		
		public function setMainRef(ref:main):void
		{
		}
		
		public function refreshLineGrid():void
		{			
		}
		
		public function mainTreeModified():void
		{			
		}
		
		public function clearGridLines():void
		{			
		}
		
		public function isLastSelectedChild():Boolean
		{	
			return false;
		}
	}
}