package composants.importmasse
{
	import mx.resources.ResourceManager;

	public class ImportMasseText
	{

		public static var myTextAide:String = 
				"<PRE>"+
				ResourceManager.getInstance().getString('M16', 'Le_principe_des_imports_de_masse_est_l_int_gration_d')+"<br>"+
				ResourceManager.getInstance().getString('M16', 'L_import_de_masse_suppose_un_soin_particulier_afin_d')+"<br>"+
				ResourceManager.getInstance().getString('M16', 'Les_r_gles_sont_les_suivantes__le_non_respect_de_ces')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '___________Collaborateur__Matricule___vous_devez_rem')+
				ResourceManager.getInstance().getString('M16', '____________ce_matricule_doit_d_j__exister_dans_Cons')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '___________Pour_cocher_la_portabilit__vous_devez_sai')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '____________n_importe_quel_valeur__par_exemple___Oui')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '___________La_portabilit__n_est_pas_obligatoire__mai')+
				ResourceManager.getInstance().getString('M16', '___________code_RIO_et_date_de_portabilit__doivent__')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '___________La_date_de_portabilit___si_elle_est_rense')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '___________et_doit_être_au_plus_t_t_J_10_de_la_date_')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '___________Le_code_RIO_doit_comporter_12_caract_res_')+"<br><br>"+
				ResourceManager.getInstance().getString('M16', '___________Le_num_ro_doit_comporter_que_des_chiffres')+"<br><br>"+
				"<br>"+
				"<br>"+
				"</PRE>";
				
		public static var myTextImporter:String = 
				"<PRE>"+
				ResourceManager.getInstance().getString('M16', '_br_La_fonction_de_chargement_des_donn_e')+
				"<br>"+
				ResourceManager.getInstance().getString('M16', '_br_Pour_les_autres_navigateurs_il_faut_')+
				"<br>"+
				ResourceManager.getInstance().getString('M16', '_br_Pour_importer_des__l_ments_depuis_un')+
				"<br>"+
				"<br>"+
				ResourceManager.getInstance().getString('M16', '___________Dans_Excel__s_lectionnez_tout')+
				ResourceManager.getInstance().getString('M16', '____________Clique_droit_puis_copier_ou_')+
				"<br>"+
				"<br>"+
				ResourceManager.getInstance().getString('M16', '___________Dans_Consoview__cliquez_sur_l') + 
				ResourceManager.getInstance().getString('M16', '____________Pour_coller_dans_Consoview_u')+
				"<br>"+
				"<br>"+
				ResourceManager.getInstance().getString('M16', '_B_Attention____B__br_')+
				ResourceManager.getInstance().getString('M16', 'Lorsque_vous_s_lectionnez_des__l_ments_d') + 
				"<br>"+
				"<br>"+
				"<br>"+				
				"</PRE>";

	}
}