package gestionparc.ihm.main.menu.itemmenurenderer
{
	import mx.resources.ResourceManager;
	
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.action.associer.actionAssocierCollabToLigne.ActionAssocierCollabToLigneCommand;
	import gestionparc.ihm.action.associer.actionAssocierCollabToLigne.ActionAssocierCollabToLigneReceiver;
	import gestionparc.ihm.action.associer.actionAssocierEqpToLigne.ActionAssocierEqpToLigneCommand;
	import gestionparc.ihm.action.associer.actionAssocierEqpToLigne.ActionAssocierEqpToLigneReceiver;
	import gestionparc.ihm.action.associer.actionAssocierExtToLigne.ActionAssocierExtToLigneCommand;
	import gestionparc.ihm.action.associer.actionAssocierExtToLigne.ActionAssocierExtToLigneReceiver;
	import gestionparc.ihm.action.associer.actionAssocierSimToLigne.ActionAssocierSimToLigneCommand;
	import gestionparc.ihm.action.associer.actionAssocierSimToLigne.ActionAssocierSimToLigneReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabFromLigne.ActionDissocierCollabFromLigneReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabFromLigne.actionDissocierCollabFromLigneCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierEqpFromLigne.ActionDissocierEqpFromLigneCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierEqpFromLigne.ActionDissocierEqpFromLigneReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierExtFromLigne.ActionDissocierExtFromLigneCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierExtFromLigne.ActionDissocierExtFromLigneReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierSimFromLigne.ActionDissocierSimFromLigneCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierSimFromLigne.ActionDissocierSimFromLigneReceiver;
	import gestionparc.ihm.action.ligne.actionCommanderNvlSim.ActionCommanderNvlSimCommand;
	import gestionparc.ihm.action.ligne.actionCommanderNvlSim.ActionCommanderNvlSimReceiver;
	import gestionparc.ihm.action.ligne.actionFournirSimStock.ActionFournirSIMStokReceiver;
	import gestionparc.ihm.action.ligne.actionFournirSimStock.ActionFournirSimStockCommand;
	import gestionparc.ihm.action.ligne.actionLigneCommanderOption.ActionLigneCommanderOptionCommand;
	import gestionparc.ihm.action.ligne.actionLigneCommanderOption.ActionLigneCommanderOptionReceiver;
	import gestionparc.ihm.action.ligne.actionLignePortabiliter.ActionPortabiliterNumCommand;
	import gestionparc.ihm.action.ligne.actionLignePortabiliter.ActionPortabilterNumReceiver;
	import gestionparc.ihm.action.ligne.actionLigneRenouveler.ActionLigneRenouvelerCommand;
	import gestionparc.ihm.action.ligne.actionLigneRenouveler.ActionLigneRenouvelerReceiver;
	import gestionparc.ihm.action.ligne.actionLigneRenumeroter.ActionLigneRenumeroterCommand;
	import gestionparc.ihm.action.ligne.actionLigneRenumeroter.ActionLigneRenumeroterReceiver;
	import gestionparc.ihm.action.ligne.actionLigneResiliation.ActionLigneResiliationCommand;
	import gestionparc.ihm.action.ligne.actionLigneResiliation.ActionLigneResiliationReceiver;
	import gestionparc.ihm.action.ligne.actionLigneVoirFiche.ActionLigneVoirFicheCommand;
	import gestionparc.ihm.action.ligne.actionLigneVoirFiche.ActionLigneVoirFicheReceiver;
	import gestionparc.ihm.action.ligne.actionLigneVoirHistorique.ActionLigneVoirHistoriqueCommand;
	import gestionparc.ihm.action.ligne.actionLigneVoirHistorique.ActionLigneVoirHistoriqueReceiver;
	import gestionparc.ihm.action.ligne.actionLigneVoirTicketsAppels.ActionLigneVoirTicketsAppelsCommand;
	import gestionparc.ihm.action.ligne.actionLigneVoirTicketsAppels.ActionLigneVoirTicketsAppelsReceiver;
	import gestionparc.ihm.action.ligne.actionlinestatus.ActionLigneActivateReceiver;
	import gestionparc.ihm.action.ligne.actionlinestatus.ActionLigneStatusCommand;
	import gestionparc.ihm.action.ligne.actionlinestatus.ActionLigneSuspendReceiver;
	import gestionparc.ihm.main.menu.ItemMenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierCollabForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierEqpForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierExtForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierSimForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierCollabForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierEqpForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierExtForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierSimForLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleCommanderNvlSIM;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleFournirSIMStock;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneActiver;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneCommanderOption;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RulePortabiliterNum;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneRenouveler;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneRenumeroter;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneResiliation;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneSuspendre;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneVoirFiche;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneVoirHistorique;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne.RuleLigneVoirTicketsAppels;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuLigneAssocier;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuLigneDissocier;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuSIMLigne;

	public class ItemMenuContextualOfLigne extends AbstractItemMenuContextual
	{

		[Embed(source='/assets/images/coll+.png')]
		private var myIconGiveColl:Class;

		[Embed(source='/assets/images/coll-.png')]
		private var myIconGetColl:Class;

		[Embed(source='/assets/images/give_terminal.png')]
		private var myIconGiveTerm:Class;

		[Embed(source='/assets/images/get_terminal.png')]
		private var myIconGetTerm:Class;

		[Embed(source='/assets/images/Newfixe+.png')]
		private var myIconNewFixeP:Class;

		[Embed(source='/assets/images/Newfixe-.png')]
		private var myIconNewFixeM:Class;

		[Embed(source='/assets/images/echange.png')]
		private var myIconActionEchanger:Class;

		[Embed(source='/assets/images/terminal_sav.png')]
		private var myIconActionSAV:Class;

		[Embed(source='/assets/images/action_supprimer.png')]
		private var myIconActionAuRebut:Class;

		[Embed(source='/assets/images/voir.png')]
		private var myIconVoir:Class;

		[Embed(source='/assets/images/historique.png')]
		private var myIconHistorique:Class;

		[Embed(source='/assets/images/phoneCalls.png')]
		private var myIconTicketsAppel:Class;
		
		public function ItemMenuContextualOfLigne(value:AbstractMatriceParcVO)
		{
			super(value);
		}
		
		
		/**
		 * Cette fonction créer la liste exaustive des items du menu contextuel d'une ligne.
		 * @return ArrayCollection
		 */
		override protected function fillArrayItemMenuContextual():Array
		{
			var res:Array=new Array();

			// MENU - Associer
			var rule1:RuleMenuLigneAssocier=new RuleMenuLigneAssocier();
			var obj1:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'ASSOCIER'), false, null, null, rule1);
			res.push(obj1);

			// Associer à un collaborateur
			var recAssocCollab:ActionAssocierCollabToLigneReceiver=new ActionAssocierCollabToLigneReceiver();
			var cmdAssocCollab:ActionAssocierCollabToLigneCommand=new ActionAssocierCollabToLigneCommand(recAssocCollab);
			var rule2:RuleAssocierCollabForLigne=new RuleAssocierCollabForLigne();
			var obj2:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'un_collaborateur'), true, myIconGiveColl, cmdAssocCollab, rule2);
			res.push(obj2);

			// Associer avec un équipement
			var recAssocEqp:ActionAssocierEqpToLigneReceiver=new ActionAssocierEqpToLigneReceiver();
			var cmdAssocEqp:ActionAssocierEqpToLigneCommand=new ActionAssocierEqpToLigneCommand(recAssocEqp);
			var rule3:RuleAssocierEqpForLigne=new RuleAssocierEqpForLigne();
			var obj3:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'un__quipement'), true, myIconGiveTerm, cmdAssocEqp, rule3);
			res.push(obj3);

			// Associer une SIM
			var recAssocSim:ActionAssocierSimToLigneReceiver=new ActionAssocierSimToLigneReceiver();
			var cmdAssocSim:ActionAssocierSimToLigneCommand=new ActionAssocierSimToLigneCommand(recAssocSim);
			var rule31:RuleAssocierSimForLigne=new RuleAssocierSimForLigne();
			var obj31:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_SIM'), true, myIconGiveTerm, cmdAssocSim, rule31);
			res.push(obj31);
			
			// Associer à une extension
			var recAssocExt:ActionAssocierExtToLigneReceiver=new ActionAssocierExtToLigneReceiver();
			var cmdAssocExt:ActionAssocierExtToLigneCommand=new ActionAssocierExtToLigneCommand(recAssocExt);
			var rule32:RuleAssocierExtForLigne=new RuleAssocierExtForLigne();
			var obj32:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_extension'), true, myIconGiveTerm, cmdAssocExt, rule32);
			res.push(obj32);
			

			// MENU - dissocier
			var rule4:RuleMenuLigneDissocier=new RuleMenuLigneDissocier();
			var obj4:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'DISSOCIER'), false, null, null, rule4);
			res.push(obj4);

			// dissocier du collaborateur
			var recDissocCollab:ActionDissocierCollabFromLigneReceiver=new ActionDissocierCollabFromLigneReceiver();
			var cmdDissocCollab:actionDissocierCollabFromLigneCommand=new actionDissocierCollabFromLigneCommand(recDissocCollab);
			var rule5:RuleDissocierCollabForLigne=new RuleDissocierCollabForLigne();
			var obj5:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'du_collaborateur'), true, myIconGetColl, cmdDissocCollab, rule5);
			res.push(obj5);

			// Dissocier de l'équipement
			var recDissocEqp:ActionDissocierEqpFromLigneReceiver=new ActionDissocierEqpFromLigneReceiver();
			var cmdDissocEqp:ActionDissocierEqpFromLigneCommand=new ActionDissocierEqpFromLigneCommand(recDissocEqp);
			var rule6:RuleDissocierEqpForLigne=new RuleDissocierEqpForLigne();
			var obj6:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'de_l__quipement'), true, myIconGetTerm, cmdDissocEqp, rule6);
			res.push(obj6);

			// Dissocier de la sim
			var recDissocSim:ActionDissocierSimFromLigneReceiver=new ActionDissocierSimFromLigneReceiver();
			var cmdDissocSim:ActionDissocierSimFromLigneCommand=new ActionDissocierSimFromLigneCommand(recDissocSim);
			var rule61:RuleDissocierSimForLigne=new RuleDissocierSimForLigne();
			var obj61:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'de_la_SIM'), true, myIconGetTerm, cmdDissocSim, rule61);
			res.push(obj61);
			
			// Dissocier de l'extension
			var recDissocExt:ActionDissocierExtFromLigneReceiver=new ActionDissocierExtFromLigneReceiver();
			var cmdDissocExt:ActionDissocierExtFromLigneCommand=new ActionDissocierExtFromLigneCommand(recDissocExt);
			var rule62:RuleDissocierExtForLigne=new RuleDissocierExtForLigne();
			var obj62:ItemMenuContextual=new ItemMenuContextual("de l'extension", true, myIconGetTerm, cmdDissocExt, rule62);
			res.push(obj62);
			
////	/////// MENU - SIM-LIGNE
			var ruleSimLigne:RuleMenuSIMLigne=new RuleMenuSIMLigne();
			var objSimLigne:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Changer_de_carte_sim_'), false, null, null, ruleSimLigne);
			res.push(objSimLigne);

////	///// action Fournir une carte sim en stock (1)
			var recFournirSIMStok:ActionFournirSIMStokReceiver = new ActionFournirSIMStokReceiver();
			var cmdFournirSIMStock:ActionFournirSimStockCommand = new ActionFournirSimStockCommand(recFournirSIMStok);
			var ruleFournirSIMStock:RuleFournirSIMStock= new RuleFournirSIMStock();
			var objFournirSIMStock:ItemMenuContextual = new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Carte_SIM_en_stock_'), true, null, cmdFournirSIMStock, ruleFournirSIMStock);
			res.push(objFournirSIMStock);
			
////	///// action Commander une nouvelle carte sim (2)
			var recCommanderNvlSIM:ActionCommanderNvlSimReceiver = new ActionCommanderNvlSimReceiver();
			var cmdCommanderNvlSIM:ActionCommanderNvlSimCommand = new ActionCommanderNvlSimCommand(recCommanderNvlSIM);
			var ruleCommanderNvlSIM:RuleCommanderNvlSIM = new RuleCommanderNvlSIM();
			var objCommanderNvlSIM:ItemMenuContextual = new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Nouvelle_carte_SIM_'), true, null, cmdCommanderNvlSIM, ruleCommanderNvlSIM);
			res.push(objCommanderNvlSIM);			
			
			// MENU - Ligne
			var rule7:RuleMenuLigne=new RuleMenuLigne();
			var obj7:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'LIGNE'), false, null, null, rule7);
			res.push(obj7);

			// Renumeroter
			var recRenumeroter:ActionLigneRenumeroterReceiver=new ActionLigneRenumeroterReceiver();
			var cmdRenumeroter:ActionLigneRenumeroterCommand=new ActionLigneRenumeroterCommand(recRenumeroter);
			var ruleRenumeroter:RuleLigneRenumeroter=new RuleLigneRenumeroter();
			var objRenumeroter:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Renumeroter'), true, null, cmdRenumeroter, ruleRenumeroter);
			res.push(objRenumeroter);

			// Renouveler
			var recRenouveler:ActionLigneRenouvelerReceiver=new ActionLigneRenouvelerReceiver();
			var cmdRenouveler:ActionLigneRenouvelerCommand=new ActionLigneRenouvelerCommand(recRenouveler);
			var rule8:RuleLigneRenouveler=new RuleLigneRenouveler();
			var obj8:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Renouveler'), true, myIconActionEchanger, cmdRenouveler, rule8);
			res.push(obj8);

			// Commander option
			var recCommanderOption:ActionLigneCommanderOptionReceiver=new ActionLigneCommanderOptionReceiver();
			var cmdCommanderOption:ActionLigneCommanderOptionCommand=new ActionLigneCommanderOptionCommand(recCommanderOption);
			var rule9:RuleLigneCommanderOption=new RuleLigneCommanderOption();
			var obj9:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Commander_options'), true, myIconNewFixeM, cmdCommanderOption, rule9);
			res.push(obj9);

			// Résiliation
			var recResilier:ActionLigneResiliationReceiver=new ActionLigneResiliationReceiver();
			var cmdResilier:ActionLigneResiliationCommand=new ActionLigneResiliationCommand(recResilier);
			var rule10:RuleLigneResiliation=new RuleLigneResiliation();
			var obj10:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'R_siliation'), true, myIconActionAuRebut, cmdResilier, rule10);
			res.push(obj10);
			
			// Activation 			
			var recActivate:ActionLigneActivateReceiver=new ActionLigneActivateReceiver(selectedObject);			
			var cmdActivate:ActionLigneStatusCommand=new ActionLigneStatusCommand(recActivate);
			var rule14:RuleLigneActiver=new RuleLigneActiver();
			var obj14:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Activer'), true, null, cmdActivate, rule14);
			res.push(obj14);
			
			// Suspension 			
			var reSuspension:ActionLigneSuspendReceiver=new ActionLigneSuspendReceiver(selectedObject);			
			var cmdSuspend:ActionLigneStatusCommand=new ActionLigneStatusCommand(reSuspension);
			var rule15:RuleLigneSuspendre=new RuleLigneSuspendre();
			var obj15:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Suspendre'), true, null, cmdSuspend, rule15);
			res.push(obj15);
			
			// Portabilité de la ligne
			var recCommanderPortabilité:ActionPortabilterNumReceiver = new ActionPortabilterNumReceiver();
			var cmdCommanderPortabilité:ActionPortabiliterNumCommand = new ActionPortabiliterNumCommand(recCommanderPortabilité);
			var rule16:RulePortabiliterNum=new RulePortabiliterNum();
			var obj16:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Portabilit_'), true, null, cmdCommanderPortabilité, rule16);
			res.push(obj16);

			// Voir fiche
			var recVoirFiche:ActionLigneVoirFicheReceiver=new ActionLigneVoirFicheReceiver();
			var cmdVoirFiche:ActionLigneVoirFicheCommand=new ActionLigneVoirFicheCommand(recVoirFiche);
			var rule11:RuleLigneVoirFiche=new RuleLigneVoirFiche();
			var obj11:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_fiche'), true, myIconVoir, cmdVoirFiche, rule11);
			res.push(obj11);

			// voir historique
			var recVoirHisto:ActionLigneVoirHistoriqueReceiver=new ActionLigneVoirHistoriqueReceiver();
			var cmdVoirHisto:ActionLigneVoirHistoriqueCommand=new ActionLigneVoirHistoriqueCommand(recVoirHisto);
			var rule12:RuleLigneVoirHistorique=new RuleLigneVoirHistorique();
			var obj12:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_historique'), true, myIconHistorique, cmdVoirHisto, rule12);
			res.push(obj12);
			
			//voir tickets d'appel
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("D_CDR") && moduleGestionParcIHM.globalParameter.userAccess.D_CDR == 1)
			{
				var recVoirTickets:ActionLigneVoirTicketsAppelsReceiver = new ActionLigneVoirTicketsAppelsReceiver();
				var cmdVoirTickets:ActionLigneVoirTicketsAppelsCommand = new ActionLigneVoirTicketsAppelsCommand(recVoirTickets);
				var rule13:RuleLigneVoirTicketsAppels = new RuleLigneVoirTicketsAppels();
				var obj13:ItemMenuContextual = new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Liste_des_appels'), true, myIconTicketsAppel, cmdVoirTickets, rule13);
				res.push(obj13);
			}
			return res;
		}
	}
}
