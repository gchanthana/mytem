<cfcomponent name="SuiviBudgetDetail" alias="fr.consotel.consoview.reporting.report.budgetaire.SuiviBudgetDetail">
	<cffunction name="runReport" access="remote" output="true" returntype="numeric">
		<cfargument name="parameters" required="true" type="struct">
		<cfset biPub=createObject("component","fr.consotel.consoview.api.bi.BIPublisher")>
		<cfset biPub.validateLogin("consoview","public")>
		<cfset arrayOfParamNameValue=arrayNew(1)>
		 
		<cfset value=structNew()>
		<cfset value.name="P_IDCLICHE">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDCLICHE"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[1]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDNOEUD">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDGROUPE_CLIENT"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[2]=value>
		
		<cfset value=structNew()>
		<cfset value.name="P_IDEXERCICE">
		<cfset values=arrayNew(1)>
		<cfset values[1]=arguments.parameters["P_IDEXERCICE"]>
		<cfset value.values=values>
		<cfset arrayOfParamNameValue[3]=value>
		<cfset job_id=biPub.scheduleReport("/~consoview/budgetaire/Suivi Budget Detail/Suivi Budget Detail.xdo","v1",FORMAT,"OBIEE/SCHEDULED/",arrayOfParamNameValue,"consoview","public","FTP")>
		<cfreturn job_id>
	</cffunction>
</cfcomponent>
