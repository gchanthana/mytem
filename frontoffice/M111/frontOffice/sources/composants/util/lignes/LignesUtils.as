package composants.util.lignes
{

	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;

	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

	/**
	 * @author samuel.divioka
	 * @version 1.0
	 * @created 08-oct.-2008 12:41:48
	 */
	[Bindable]
	public class LignesUtils extends EventDispatcher
	{
		private static const ERROR_MESSAGE:String=ResourceManager.getInstance().getString('M111', 'Le_num_ro_de_ligne_n_est_pas_disponible');
		private static const OK_MESSAGE:String=ResourceManager.getInstance().getString('M111', 'Le_num_ro_de_ligne_est_valide');
		public static const GETINFOSFICHESOUSTETEOK:String="getInfosFicheSousTeteOK";
		public static const LIST_INVENTAIRE_SOUSTETE_LOADED:String="listInventaireSousteteLoaded";
		public static const LIST_HISTO_INVENTAIRE_SOUSTETE_LOADED:String="listHistoInventaireSousteteLoaded";
		public static const RESSOURCE_ACTION_INVENTAIRE_SAVED:String="ressourceActionInventaireSaved";
		public static const LIST_SIM_SOUSTETE_LOADED:String="listSimSousteteLoaded";
		public static const UPDATEINFOSFICHESOUSTETEOK:String="upDateInfosFicheSousTeteOk";
		public static const UPDATEINFOSABOOPTIONSOK:String="upDateInfosAboOptionsOk";
		public static const UPDATEINFOSCONTRATOPERATEUROK:String="upDateInfosContratOperateurOk";

		public static const AFFECTATION_LIGNE_ORGA_OK:String="affectationLigneOrgaOk";
		public static const REGLE_ORGA_LIGNE_LOADED:String="regleOrgaLigneLoaded";
		public static const LIST_ORGA_CLIENTE_LOADED:String="listRegleOrgaClienteLoaded";
		public static const MODELE_STRUCTURE_ORGA_LOADED:String="modeleStructureOrgaLoaded";

		public static const NUM_SERIE_EXISTS_TRUE:String="numSerieExistsTrue";
		public static const NUM_SERIE_EXISTS_FALSE:String="numSerieExistsFalse";

		public static const LISTE_USAGE_LOADED:String="ListeUsageLoaded";
		public static const LISTE_COMPTES_SOUSCOMPTES_LOADED:String="listeComptesSouscomptesLoaded";
		public static const LISTE_OPERATEURS_LOADED:String="listeOperateursLoaded";
		public static const RETURN_OF_IS_UNIQUE_NUMBER:String="returnOfIsUniqueNumber";

		public var boolOK:Boolean=true;


		private var _selectedLigne:Object;
		private var _infosFicheSousTete:ArrayCollection=new ArrayCollection();
		private var _listeInventaireSousTete:ArrayCollection=new ArrayCollection();
		private var _listeHistoriqueInventaireSousTete:ArrayCollection=new ArrayCollection();
		private var _listeSim:ArrayCollection=new ArrayCollection();
		private var _listeRegleOrga:ArrayCollection;
		private var _listeOrgaCliente:ArrayCollection;
		private var _listeUsages:ArrayCollection;
		private var _tmpLigne:Object;
		private var _message:String="";
		private var _numeroUnique:String;
		private var _listeNumerosValides:Array=[];
		private var _modeleStructureOrga:ArrayCollection;
		private var _positionListeOrgaCliente:int;
		private var _listeComptes:ArrayCollection=new ArrayCollection();
		private var _listeSousComptes:ArrayCollection=new ArrayCollection();
		private var _listeOperateurs:ArrayCollection=new ArrayCollection();
		private var _uniqueNumber:Boolean;

		public function LignesUtils()
		{
			Alert.okLabel=ResourceManager.getInstance().getString('M111', 'Fermer');
			Alert.buttonWidth=100;
		}

		/**
		 * Génère un numéro unique
		 */
		public function genererNumeroLigneUnique():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "genererNumeroLigneUnique", genererNumeroLigneUniqueResultHandler);
			RemoteObjectUtil.callService(op)
		}

		/**
		 * si ok alors numeroUnique = event.Result
		 */
		protected function genererNumeroLigneUniqueResultHandler(e:ResultEvent):void
		{
			numeroUnique=e.result.toString();

			listeNumerosValides.push(numeroUnique);
			boolOK=true;
		}

		public function verifierUniciteNumeroLigne(value:String):void
		{
			if (value != null)
			{
				boolOK=false;
				var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "verifierUniciteNumeroLigne", verifierUniciteNumeroLigneResultHandler);
				RemoteObjectUtil.callService(op, value)
			}
		}

		protected function verifierUniciteNumeroLigneResultHandler(e:ResultEvent):void
		{
			if (e.result > 0)
			{
				message=ERROR_MESSAGE;
				boolOK=false;
				ConsoviewAlert.afficherAlertInfo(message, ResourceManager.getInstance().getString('M111', 'Info'), null);
			}
			else if (e.result == 0)
			{
				message=OK_MESSAGE;
				ConsoviewAlert.afficherOKImage(message, Application.application as DisplayObject);
				listeNumerosValides.push(e.token.message.body[0]);
				boolOK=true;
			}
			else if (e.result < 0)
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Une_erreur_s_est_produite_lors_de_la_v_r'));
				boolOK=false;
			}
		}

		public function isUniqueNumber(value:String):void
		{
			if (value != null)
			{
				var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "verifierUniciteNumeroLigne", isUniqueNumberResultHandler);
				RemoteObjectUtil.callService(op, value)
			}
		}

		protected function isUniqueNumberResultHandler(e:ResultEvent):void
		{
			if (e.result == 0)
			{
				uniqueNumber=true;
			}
			else
			{
				uniqueNumber=false;
			}
			dispatchEvent(new Event(RETURN_OF_IS_UNIQUE_NUMBER));
		}

		/**
		 *	Retourne les infos sur l'historique d'une ligne (ds l'inventaire ou non)
		 **/
		public function getHistoriqueInventaireSousTete(idInventaireProduit:Number):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "getHistoriqueActionRessource", getHistoriqueInventaireSousTeteResultHandler);
			RemoteObjectUtil.callService(op, idInventaireProduit);
		}

		private function getHistoriqueInventaireSousTeteResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				listeHistoriqueInventaireSousTete=evt.result as ArrayCollection;
				dispatchEvent(new Event(LIST_HISTO_INVENTAIRE_SOUSTETE_LOADED));
				var tmpArr:Array=listeHistoriqueInventaireSousTete.source;
				for (var i:int=0; i < tmpArr.length; i++)
				{
					listeHistoriqueInventaireSousTete.getItemAt(i).DATE_ACTION=DateFunction.formatDateAsString(tmpArr[i].DATE_ACTION);
				}
			}
		}

		/**
		 *	Affecte une ligne à une organisation
		 **/
		public function saveOrgaCliente(idCible:Number, idSource:Number):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "assignLine", assignLineResultHandler);

			RemoteObjectUtil.callService(op, idCible, idSource);
		}


		private function assignLineResultHandler(evt:ResultEvent):void
		{
			if (evt.result > 0)
			{
				dispatchEvent(new Event(AFFECTATION_LIGNE_ORGA_OK));
			}
		}

		public function getListOrgaCliente():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.RAGO.Rules", "getListOrgaCliente", getListOrgaClienteResultHandler);

			RemoteObjectUtil.callService(op);
		}

		private function getListOrgaClienteResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeOrgaCliente=evt.result as ArrayCollection;
				this.dispatchEvent(new Event(LIST_ORGA_CLIENTE_LOADED));
			}
		}


		public function getListOrgaCliente_V2():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "GetOrgaLigne", getListOrgaCliente_V2ResultHandler);

			RemoteObjectUtil.callService(op, _selectedLigne.IDSOUS_TETE);
		}

		private function getListOrgaCliente_V2ResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeOrgaCliente=evt.result as ArrayCollection;
				var longueur:int=_listeOrgaCliente.length;
				var dateLastModifPosition:Date=new Date(0);

				positionListeOrgaCliente=-1;

				for (var index:int=0; index < longueur; index++)
				{
					if (_listeOrgaCliente[index].IDNOEUD != null)
					{
						getRegleOrgaLigne(_listeOrgaCliente[index].IDNOEUD, _selectedLigne.IDSOUS_TETE);
						break;
					}
				}

				if (positionListeOrgaCliente != -1)
				{
					getRegleOrgaLigne(_listeOrgaCliente[positionListeOrgaCliente].IDGROUPE_CLIENT, _selectedLigne.IDSOUS_TETE);
				}

				this.dispatchEvent(new Event(LIST_ORGA_CLIENTE_LOADED));
			}
		}

		/**
		 *	Retourne la règle orga d'une ligne affectée à une organisation
		 **/
		public function getRegleOrgaLigne(idOrga:Number, idSousTete:Number):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "getRegleOrgaLigne", getRegleOrgaLigneResultHandler);

			RemoteObjectUtil.callService(op, idOrga, idSousTete);
		}

		private function getRegleOrgaLigneResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeRegleOrga=evt.result as ArrayCollection;
				dispatchEvent(new Event(REGLE_ORGA_LIGNE_LOADED));
			}
		}

		/**
		 *	Retourne le modele structure des organisations
		 **/
		public function getInfosOrganisation(id:Number):void
		{
			var idOrganisation:Number=id;

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.parametres.perimetres.OngletOrganisation", "getInfosOrganisation", getInfosOrganisationResultHandler);
			RemoteObjectUtil.callService(opData, idOrganisation);
		}

		private function getInfosOrganisationResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				getModeleStructure((evt.result as ArrayCollection)[0].IDORGA_MODELE);
			}
		}

		private function getModeleStructure(id:Number):void
		{
			var idOrgaModele:Number=id;
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay", "getModeleStructure", getModeleStructureResultHandler);
			RemoteObjectUtil.callService(opData, idOrgaModele);
		}

		private function getModeleStructureResultHandler(evt:ResultEvent):void
		{

			if (evt.result != null)
			{
				_modeleStructureOrga=evt.result as ArrayCollection;
				dispatchEvent(new Event(MODELE_STRUCTURE_ORGA_LOADED));
			}
		}

		/**
		 *	Retourne la liste des produits abo d'une ligne dans l'inventaire
		 **/
		public function getLigneProduitAbo(idSousTete:Number):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.lignes.LignesUtils", "getLigneProduitAbo", getLigneProduitAboResultHandler);
			RemoteObjectUtil.callService(op, idSousTete);
		}

		private function getLigneProduitAboResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeInventaireSousTete=evt.result as ArrayCollection;
				dispatchEvent(new Event(LIST_INVENTAIRE_SOUSTETE_LOADED));
			}
		}

		/**
		 *	Enregistre les infos d'une ligne au niveau de l'inventaire
		 **/
		public function saveRessourceActionInventaire(listeInventaireProduit:Array, idTypeAction:int, dateAction:String, commentaireAction:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "entrerSortirRessources", saveRessourceActionInventaireResultHandler);
			RemoteObjectUtil.callService(op, listeInventaireProduit, idTypeAction, dateAction, commentaireAction);
		}

		private function saveRessourceActionInventaireResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				this.dispatchEvent(new Event(RESSOURCE_ACTION_INVENTAIRE_SAVED));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			}
		}

		/**
		 *	Retourne les infos d'une ligne
		 **/
		public function getInfosFicheSousTete(idSousTete:Number, idSim:Number, idEmploye:Number):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "getInfosFicheSousTete", getInfosFicheSousTeteResultHandler);

			RemoteObjectUtil.callService(op, idSousTete, idSim, idEmploye);
		}

		/**
		 *	Retourne la liste des sim d'une ligne
		 **/
		public function getListeSimSousTete(idSousTete:Number):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "getListeSimSousTete", getListeSimSousTeteResultHandler);

			RemoteObjectUtil.callService(op, idSousTete);
		}

		private function getListeSimSousTeteResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				listeSim=evt.result as ArrayCollection;
				this.dispatchEvent(new Event(LIST_SIM_SOUSTETE_LOADED));
			}
		}

		public function getListeUsages():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes", "getUsages", getListeUsagesResultHandler);
			RemoteObjectUtil.invokeService(op);
		}

		private function getListeUsagesResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				listeUsages=new ArrayCollection();
				for each (var obj:Object in re.result)
				{
					listeUsages.addItem(obj.USAGE);
				}
				this.dispatchEvent(new Event(LISTE_USAGE_LOADED));
			}
		}

		private function getInfosFicheSousTeteResultHandler(e:ResultEvent):void
		{
			if (e.result)
			{
				infosFicheSousTete=e.result as ArrayCollection;

				selectedLigne.DATE_RESILIATION=e.result[0].DATE_RESILIATION;
				selectedLigne.CODE_LISTE=e.result[0].CODE_LISTE;
				selectedLigne.NB_JRS_FIN_LIGNE=e.result[0].NB_JRS_FIN_LIGNE;
				selectedLigne.ACCES_LAST_FACTURE=e.result[0].ACCES_LAST_FACTURE;
				selectedLigne.DATE_OUVERTURE=e.result[0].DATE_OUVERTURE;
				selectedLigne.DATE_DERNIER_ENGAG=e.result[0].DATE_DERNIER_ENGAG;
				selectedLigne.NUM_CONTRAT_ABO=e.result[0].NUM_CONTRAT_ABO;
				selectedLigne.IDCONTRAT_ABO=e.result[0].IDCONTRAT_ABO;
				selectedLigne.DUREE_CONTRAT=e.result[0].DUREE_CONTRAT;
				selectedLigne.DATE_ELLIGIBILITE=e.result[0].DATE_ELLIGIBILITE;
				selectedLigne.DUREE_ELLIGIBILITE=e.result[0].DUREE_ELLIGIBILITE;

				selectedLigne.ABO_PRINCIPAL=e.result[0].ABO_PRINCIPAL;
				selectedLigne.LIBELLE_TYPE_LIGNE=e.result[0].LIBELLE_TYPE_LIGNE;
				selectedLigne.SOUS_COMPTE=e.result[0].SOUS_COMPTE;
				selectedLigne.COMPTE_FACTURATION=e.result[0].COMPTE_FACTURATION;
				selectedLigne.DATE_FPC=e.result[0].DATE_FPC;
				selectedLigne.CODE_LISTE=e.result[0].CODE_LISTE;
				selectedLigne.LIBELLE_SI_OPERATEUR=e.result[0].LIBELLE_SI_OPERATEUR;

				selectedLigne.PUK1=e.result[0].PUK1;
				selectedLigne.PUK2=e.result[0].PUK2;
				selectedLigne.PIN1=e.result[0].PIN1;
				selectedLigne.PIN2=e.result[0].PIN2;

				selectedLigne.SOUS_TETE=e.result[0].SOUS_TETE;
				selectedLigne.IDSIM=e.result[0].IDEQUIPEMENT;

				selectedLigne.COMMENTAIRES=e.result[0].E;
				selectedLigne.V1=e.result[0].V1;
				selectedLigne.V2=e.result[0].V2;
				selectedLigne.V3=e.result[0].V3;
				selectedLigne.V4=e.result[0].V4;

				selectedLigne.BOOL_TITULAIRE=e.result[0].BOOL_TITULAIRE;
				selectedLigne.BOOL_PAYEUR=e.result[0].BOOL_PAYEUR;

				selectedLigne.IDSITE_PHYSIQUE=e.result[0].IDSITE_PHYSIQUE;

				selectedLigne.USAGE=e.result[0].USAGE;
				selectedLigne.ETAT_LIGNE=e.result[0].ETAT_LIGNE;

				tmpLigne=ObjectUtil.copy(selectedLigne);

				dispatchEvent(new Event(GETINFOSFICHESOUSTETEOK));
			}
		}

		public function upDateInfosFicheSousTete(tmp:Object, type:String):void
		{
			/* new package pour optimi
			ancien :
			"fr.consotel.consoview.inventaire.lignes.LignesUtils",
			"upDateInfosFicheSousTete_v5",
			*/
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M11.FicheLigne", "upDateInfosFicheSousTete", function getInfosSousTeteResultHandler(e:ResultEvent):void
			{
				if (e.result > 0)
				{
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
					switch (type)
					{
						case "all":
							dispatchEvent(new Event(UPDATEINFOSFICHESOUSTETEOK));
							break;
						case "aboOptions":
							dispatchEvent(new Event(UPDATEINFOSABOOPTIONSOK));
							break;
						case "contratOperateur":
							dispatchEvent(new Event(UPDATEINFOSCONTRATOPERATEUROK));
							break;
					}
				}
				else
				{
					Alert.show(e.result.toString(), ResourceManager.getInstance().getString('M111', 'Erreur'));
				}
			});

			RemoteObjectUtil.callService(op, tmp.IDSOUS_TETE, (tmp.SOUS_TETE != null) ? tmp.SOUS_TETE : '', tmp.IDCONTRAT_ABO, (tmp.DATE_RESILIATION != null) ? DateFunction.formatDateAsInverseString(tmp.DATE_RESILIATION) : '', (tmp.DATE_DERNIER_ENGAG != null) ? DateFunction.formatDateAsInverseString(tmp.DATE_DERNIER_ENGAG) : '', (tmp.DATE_OUVERTURE != null) ? DateFunction.formatDateAsInverseString(tmp.DATE_OUVERTURE) : '', (tmp.DUREE_CONTRAT > 0) ? tmp.DUREE_CONTRAT : 0, (tmp.NUM_CONTRAT_ABO != null) ? tmp.NUM_CONTRAT_ABO : '', (Number(tmp.IDSIM) > 0) ? Number(tmp.IDSIM) : 0, (tmp.PIN1 != null) ? tmp.PIN1 : '', (tmp.PIN2 != null) ? tmp.PIN2 : '', (tmp.PUK1 != null) ? tmp.PUK1 : '', (tmp.PUK2 != null) ? tmp.PUK2 : '', (tmp.NUM_SIM != null) ? tmp.NUM_SIM : '', (tmp.DUREE_ELLIGIBILITE > 0) ? tmp.DUREE_ELLIGIBILITE : 10, tmp.COMMENTAIRES, tmp.V1, tmp.V2, tmp.V3, tmp.V4, tmp.BOOL_TITULAIRE, tmp.BOOL_PAYEUR, tmp.USAGE);
		}

		public function numSerieExists(idSousTete:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.lignes.LignesUtils", "verifierUniciteNumeroLigne", function numSerieExistsResultHandler(e:ResultEvent):void
			{
				if (e.result == 0)
				{
					dispatchEvent(new Event(NUM_SERIE_EXISTS_FALSE));
				}
				else
				{
					dispatchEvent(new Event(NUM_SERIE_EXISTS_TRUE));
				}
			});

			RemoteObjectUtil.callService(op, idSousTete);
		}

		public function getListeComptesOperateurByLoginAndPool(idRealOperateur:Number, idPool:Number):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M11.CompteFacturationOperateurServices", "getListeComptesOperateurByLoginAndPool", getListeComptesOperateurByLoginAndPoolResultHandler);
			RemoteObjectUtil.callService(op, idRealOperateur, idPool);
		}

		private function getListeComptesOperateurByLoginAndPoolResultHandler(re:ResultEvent):void
		{
			listeComptes=new ArrayCollection();
			listeSousComptes=new ArrayCollection();
			if (re.result)
			{
				for each (var obj:* in re.result)
				{
					// traitement des comptes facturations
					if (!listeComptes.contains(obj.COMPTE_FACTURATION))
					{
						listeComptes.addItem(obj.COMPTE_FACTURATION)
					}

					// traitement des sous comptes facturations
					if (!listeSousComptes.contains(obj.SOUS_COMPTE))
					{
						listeSousComptes.addItem(obj.SOUS_COMPTE)
					}
				}
				dispatchEvent(new Event(LISTE_COMPTES_SOUSCOMPTES_LOADED));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'Recup_ration_des_comptes_et_des_sous_com'), ResourceManager.getInstance().getString('M111', 'Consoview'));
			}
		}

		public function getListeOperateurs():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.association.", "getFabOp", getListeOperateursResultHandler);
			RemoteObjectUtil.callService(op);
		}

		private function getListeOperateursResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				listeOperateurs=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_OPERATEURS_LOADED));
			}
		}

		public function getListeOperateurs_V2():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.operateurs.OperateursUtils", "fournirListeOperateursSegment", getListeOperateursResultHandler_V2);
			RemoteObjectUtil.callService(op, "MOBILE");
		}

		private function getListeOperateursResultHandler_V2(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				listeOperateurs=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_OPERATEURS_LOADED));
			}
		}

		public function get message():String
		{
			return _message
		}

		public function set message(value:String):void
		{
			_message=value;
		}

		public function set infosFicheSousTete(value:ArrayCollection):void
		{
			_infosFicheSousTete=value;
		}

		public function get infosFicheSousTete():ArrayCollection
		{
			return _infosFicheSousTete;
		}

		public function set listeInventaireSousTete(value:ArrayCollection):void
		{
			_listeInventaireSousTete=value;
		}

		public function get listeInventaireSousTete():ArrayCollection
		{
			return _listeInventaireSousTete;
		}

		public function set listeHistoriqueInventaireSousTete(value:ArrayCollection):void
		{
			_listeHistoriqueInventaireSousTete=value;
		}

		public function get listeHistoriqueInventaireSousTete():ArrayCollection
		{
			return _listeHistoriqueInventaireSousTete;
		}

		public function get listRegleOrga():ArrayCollection
		{
			return _listeRegleOrga;
		}

		public function set listeSim(value:ArrayCollection):void
		{
			_listeSim=value;
		}

		public function get listeSim():ArrayCollection
		{
			return _listeSim;
		}

		public function set listeUsages(value:ArrayCollection):void
		{
			_listeUsages=value;
		}

		public function get listeUsages():ArrayCollection
		{
			return _listeUsages;
		}

		public function set selectedLigne(value:Object):void
		{
			_selectedLigne=value;
		}

		public function get selectedLigne():Object
		{
			return _selectedLigne;
		}

		public function set tmpLigne(value:Object):void
		{
			_tmpLigne=value;
		}

		public function get tmpLigne():Object
		{
			return _tmpLigne;
		}

		public function get numeroUnique():String
		{
			return _numeroUnique
		}

		public function set numeroUnique(value:String):void
		{
			_numeroUnique=value
		}

		public function set listeNumerosValides(value:Array):void
		{
			_listeNumerosValides=value;
		}

		public function get listeNumerosValides():Array
		{
			return _listeNumerosValides;
		}


		public function set modeleStructureOrga(value:ArrayCollection):void
		{
			_modeleStructureOrga=value;
		}

		public function get modeleStructureOrga():ArrayCollection
		{
			return _modeleStructureOrga;
		}

		public function set listeOrgaCliente(value:ArrayCollection):void
		{
			_listeOrgaCliente=value;
		}

		public function get listeOrgaCliente():ArrayCollection
		{
			return _listeOrgaCliente;
		}

		public function set positionListeOrgaCliente(value:int):void
		{
			_positionListeOrgaCliente=value;
		}

		public function get positionListeOrgaCliente():int
		{
			return _positionListeOrgaCliente;
		}

		public function set listeComptes(value:ArrayCollection):void
		{
			_listeComptes=value;
		}

		public function get listeComptes():ArrayCollection
		{
			return _listeComptes;
		}

		public function set listeSousComptes(value:ArrayCollection):void
		{
			_listeSousComptes=value;
		}

		public function get listeSousComptes():ArrayCollection
		{
			return _listeSousComptes;
		}

		public function set listeOperateurs(value:ArrayCollection):void
		{
			_listeOperateurs=value;
		}

		public function get listeOperateurs():ArrayCollection
		{
			return _listeOperateurs;
		}

		public function set uniqueNumber(value:Boolean):void
		{
			_uniqueNumber=value;
		}

		public function get uniqueNumber():Boolean
		{
			return _uniqueNumber;
		}
	}
}
