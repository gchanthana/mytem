<cfcomponent output="false">
	<cfproperty name="idGroupeMaitre" type="numeric" default="0">
	<cfproperty name="idGroupeClient" type="numeric" default="0">
	<cfproperty name="type_perimetre" type="string" default="Groupe">
	   
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupeMaitre = session.perimetre.ID_GROUPE;
		variables.idGroupeClient = session.perimetre.ID_PERIMETRE;
		variables.type_perimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getIdGroupeMaitre" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeMaitre>
	</cffunction>
	
	<cffunction name="getIdGroupeClient" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeClient>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.type_perimetre>
	</cffunction>
	
	
	
	<cffunction name="getGrpProduitsFactureGroupePerimetre" access="remote" output="false" returntype="any">
		<cfargument name="myFacture" type="Facture" required="true"/>
		
		<cfset alisteGrpProduits = Evaluate("getGrpProduitsFacture#getTypePerimetre()#AsQuery(myFacture)")>
		<cfset qlisteGrpProduitsAbos = alisteGrpProduits[1]>
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		
		<!--- =============== TOTAUX ============== --->
		<cfset montantTotal = 0>
		<cftry>
			<cfquery name="totalAbos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsAbos
			</cfquery>
			<cfquery name="totalConsos" dbtype="query">
				select sum(MONTANT) as TOTAL from qlisteGrpProduitsConsos
			</cfquery>	
			<cfif totalAbos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalAbos.TOTAL[1]>
				<cfelse>
				<cfset montantTotal = 0>
			</cfif>
			<cfif totalConsos.recordCount gt 0>
				<cfset montantTotal = montantTotal + totalConsos.TOTAL[1]>				
			</cfif>	
		<cfcatch type="any">
			<cfset montantTotal = 0>
		</cfcatch>	
		</cftry>
		<!--- =============== FIN TOTAUX ========== --->
		
		
		<!--- =============== GRP PRODUITS ABOS ========== --->
		<cfset tabProduitsAbos = arrayNew(1)>	
		
		<cfloop query="qlisteGrpProduitsAbos">
			<cfset grpProduitsAbos = createObject("component","GroupeProduitControle")>	
			<cfscript>		
				grpProduitsAbos.setId(qlisteGrpProduitsAbos.IDGRP_CTL);
				grpProduitsAbos.setLibelle(qlisteGrpProduitsAbos.GRP_CTL);
				grpProduitsAbos.setSegment(formatSegment(qlisteGrpProduitsAbos.GRP_SEGMENT));				
				grpProduitsAbos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsAbos.setPoids(0);
				}else{
					grpProduitsAbos.setPoids(qlisteGrpProduitsAbos.MONTANT/montantTotal*100);
				}
			  	grpProduitsAbos.setPourControle(qlisteGrpProduitsAbos.A_CONTROLER);
				grpProduitsAbos.setPrixUnitaire(qlisteGrpProduitsAbos.TARIF_BRUT);
				grpProduitsAbos.setPrixRemise(qlisteGrpProduitsAbos.TARIF_REMISE);
				grpProduitsAbos.setMontant(qlisteGrpProduitsAbos.MONTANT); 
				grpProduitsAbos.setNbVersion(1); 
				grpProduitsAbos.setRemiseContrat(qlisteGrpProduitsAbos.PCT_REMISE); 
				grpProduitsAbos.setDateDebut(qlisteGrpProduitsAbos.DATEDEB); 
				grpProduitsAbos.setDateFin(qlisteGrpProduitsAbos.DATEFIN); 
				grpProduitsAbos.setType("ABO");
			</cfscript>
			<cfset arrayAppend(tabProduitsAbos,grpProduitsAbos)>
		</cfloop>
		
		<!--- =============== FIN GRP PRODUITS ABOS ========== --->
		
		
		<!--- =============== GRP PRODUITS CONSOS ========== --->
		<cfset qlisteGrpProduitsConsos = alisteGrpProduits[2]>
		<cfset tabProduitsConsos = arrayNew(1)>
		<cfloop query="qlisteGrpProduitsConsos">
			<cfset grpProduitsConsos = createObject("component","GroupeProduitControle")>			
			<cfscript>		
			//Initialize the CFC with the properties values.
				grpProduitsConsos.setId(qlisteGrpProduitsConsos.IDGRP_CTL);
				grpProduitsConsos.setLibelle(qlisteGrpProduitsConsos.GRP_CTL);
				grpProduitsConsos.setSegment(formatSegment(qlisteGrpProduitsConsos.GRP_SEGMENT));				
				grpProduitsConsos.setOperateurId(pRecherche.getOperateurId());
				if (montantTotal eq 0){
					grpProduitsConsos.setPoids(0);
				}else{
					grpProduitsConsos.setPoids(qlisteGrpProduitsConsos.MONTANT/montantTotal*100);
				}
			  	grpProduitsConsos.setPourControle(qlisteGrpProduitsConsos.A_CONTROLER);
				grpProduitsConsos.setPrixUnitaire(qlisteGrpProduitsConsos.TARIF_BRUT);
				grpProduitsConsos.setPrixRemise(qlisteGrpProduitsConsos.TARIF_REMISE);
				grpProduitsConsos.setMontant(qlisteGrpProduitsConsos.MONTANT); 
				grpProduitsConsos.setNbVersion(1); 
				grpProduitsConsos.setRemiseContrat(qlisteGrpProduitsConsos.PCT_REMISE); 
				grpProduitsConsos.setDateDebut(qlisteGrpProduitsConsos.DATEDEB); 
				grpProduitsConsos.setDateFin(qlisteGrpProduitsConsos.DATEFIN); 
				grpProduitsConsos.setType("CONSO");
			</cfscript>
			<cfset arrayAppend(tabProduitsConsos,grpProduitsConsos)>
		</cfloop>
		<!--- =============== FIN PRODUITS CONSOS ========== --->
		
		<cfset tabProduits = arrayNew(1)>		
		<cfset arrayappend(tabProduits,tabProduitsAbos)>
		<cfset arrayappend(tabProduits,tabProduitsConsos)>
				
		<cfreturn tabProduits>
	</cffunction>
	
	<cffunction name="formatSegment" access="private" output="false" returntype="string">
		<cfargument name="nSegment" type="numeric" required="true">
		<cfswitch expression="#nSegment#">
			<cfcase value="1">
				<cfreturn "Fixe">
			</cfcase>
			<cfcase value="2">
				<cfreturn "Mobile">
			</cfcase>
			<cfcase value="3">
				<cfreturn "Data">
			</cfcase>
			<cfdefaultcase>
				<cfreturn "Segment inconnu">
			</cfdefaultcase>
		</cfswitch>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des groupe de produits Ã©laborÃ©s par Consotel pour une Racine.
					  
		Param in 
			ParamsRecherche
		Params out
			query
			  		  	
	--->
	<cffunction name="getGrpProduitsFactureGroupeAsQuery" access="remote" output="false" returntype="void">
		<cfargument name="myFacture" type="Facture" required="true"/>
		<!---pkg_cv_grcl_facturation.liste_grp_ctl_abos( p_operateurid => 	:p_operateurid,
			                                             p_libelle => 		:p_libelle,
			                                             p_idracine => 		:p_idracine,
			                                             p_retour => 		:p_retour);													
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation.liste_grp_ctl_abos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsAbos"/>        
		</cfstoredproc> 
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation.liste_grp_ctl_Consos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#THIS.getIdGroupeMaitre()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsConsos"/>        
		</cfstoredproc>
		
		<cfset tabQListeGrpProduits =  arraynew(1)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsAbos)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsConsos)>		
		<cfreturn tabQListeGrpProduits> --->
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des groupe de produits Ã©laborÃ©s par Consotel pour un groupe de ligne.
					  
		Param in 
			ParamsRecherche
		Params out
			query
			  		  	
	--->
	<cffunction name="getGrpProduitsFactureGroupeLigneAsQuery" access="Remote" output="false" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true"/>
		 
		<!---pkg_cv_glig_facturation.liste_grp_ctl_abos( p_operateurid => 	:p_operateurid,
			                                             p_libelle => 		:p_libelle,
			                                             p_idracine => 		:p_idGroupeLigne,
			                                             p_retour => 		:p_retour);
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_facturation.liste_grp_ctl_abos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsAbos"/>        
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_facturation.liste_grp_ctl_Consos">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#paramsRecherche.getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value="#paramsRecherche.getChaine()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeLigne" 		value="#THIS.getIdGroupeClient()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl"		 	value="0" null="true"/>
			<cfprocresult name="qListeGrpProduitsConsos"/>        
		</cfstoredproc>
		
		<cfset tabQListeGrpProduits =  arraynew(1)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsAbos)>
		<cfset arrayappend(tabQListeGrpProduits,qListeGrpProduitsConsos)>		--->											
		<cfreturn tabQListeGrpProduits>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : 	Liste des produits d'un groupe de produit
						
		Param in 
			groupe_produit(produit)
		Params out
			produit[]	
	--->
	<cffunction name="getDetailGroupeProduit" access="Remote" output="false" returntype="query">		
    	<cfargument name="gpe" type="GroupeProduitControle" required="true"/>  	
		<!---   pkg_cv_grcl_facturation.liste_produit_grp_ctl(p_idgrp_ctl => :p_idgrp_ctl,
                                                p_idracine => :p_idracine,
                                                p_retour => :p_retour);
	  	--->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.liste_produit_grp_ctl">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idgrp_ctl" 	value="#gpe.getID()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idracine" 	value="#this.getIdGroupeMaitre()#"/>
			<cfprocresult name="qListeProduits"/>        
		</cfstoredproc>		
		
		<cfreturn qListeProduits/>
	</cffunction>
	
</cfcomponent>