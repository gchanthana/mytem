package gestioncommande.services
{
	import composants.util.DateFunction;
	
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.RessourcesElements;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;

	public class CommandeArticles
	{
		public function CommandeArticles()
		{
		}
		
		public function addEquipements(value:ElementsCommande2, isEligible:Boolean = true):XML
		{    		
			var equipements :XML = <equipements></equipements>;
			var lenTer		:int = value.TERMINAUX.length;
			var lenAcc		:int = value.ACCESSOIRES.length;
			var i			:int = 0;
			
			for(i = 0;i <  lenTer;i++)
			{
				if(value.TERMINAUX[i].IDFOURNISSEUR > 0) 
					equipements.appendChild(xmlEquipement(value.TERMINAUX[i] as EquipementsElements, isEligible));
			}
			
			for(i = 0;i <  lenAcc;i++)
			{
				equipements.appendChild(xmlEquipement(value.ACCESSOIRES[i] as EquipementsElements, isEligible));
			}
		
			return equipements;
		}
		
		public function addRessources(value:ElementsCommande2):XML
		{    		
			var ressources 	:XML = <ressources></ressources>;
			var lenAbo		:int = value.ABONNEMENTS.length;
			var lenOpt		:int = value.OPTIONS.length;
			var i			:int = 0;
			
			for(i = 0;i <  lenAbo;i++)
			{
				ressources.appendChild(xmlRessource(value.ABONNEMENTS[i] as RessourcesElements));
			}
			
			for(i = 0;i <  lenOpt;i++)
			{
				ressources.appendChild(xmlRessource(value.OPTIONS[i] as RessourcesElements));
			}
			
			return ressources;
		}
		
		public function addRessourcesByLignes(addOpt:ArrayCollection, remOpt:ArrayCollection):XML
		{    		
			var ressources 	:XML = <ressources></ressources>;
			var lenOptA		:int = addOpt.length;
			var lenOptR		:int = remOpt.length;
			var i			:int = 0;
			
			for(i = 0;i < lenOptA;i++)
			{
				ressources.appendChild(xmlRessource(addOpt[i]));
			}
			
			for(i = 0;i < lenOptR;i++)
			{
				ressources.appendChild(xmlRessource(remOpt[i]));
			}
			
			return ressources;
		}
		
		public function addConfiguration(configuration:XML, conElts:ConfigurationElements, engagement:Engagement, idConfiguration:int):XML
		{
			configuration.appendChild(<numero_config>{idConfiguration}</numero_config>);	
			configuration.appendChild(<code_interne>{conElts.LIBELLE}</code_interne>);
			
			configuration.appendChild(<idSegment>{SessionUserObject.singletonSession.IDSEGMENT}</idSegment>);
			configuration.appendChild(<idtypecommande>{SessionUserObject.singletonSession.IDTYPEDECOMMANDE}</idtypecommande>);
			
			if (SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 13) //SAV
			{
				configuration.appendChild(<idequipement>{CvAccessManager.getSession().INFOS_DIVERS.IDEQUIPEMENT}</idequipement>);
			}

			if(conElts.ID_TYPELIGNE > 0)
			{
				configuration.appendChild(<idtype_ligne>{conElts.ID_TYPELIGNE}</idtype_ligne>);
				configuration.appendChild(<type_ligne>{conElts.TYPELIGNE}</type_ligne>);
			}
			else
			{
				configuration.appendChild(<idtype_ligne></idtype_ligne>);
				configuration.appendChild(<type_ligne></type_ligne>);
			}

			if(conElts.IS_PORTABILITE)
			{
				configuration.appendChild(<code_rio>{conElts.PORTABILITE.CODERIO}</code_rio>);
				configuration.appendChild(<date_portage>{DateFunction.formatDateAsInverseString(conElts.PORTABILITE.DATEPORTABILITE)}</date_portage>);
				
				
				
				//Portabilité US
				if(conElts.PORTABILITE.NUMERO_PORTER.length > 0)
				{
					//num ligne
					configuration.appendChild(<soustete>{conElts.PORTABILITE.NUMERO_PORTER}</soustete>);
					
					// ajouté pour portabilité specifique opérateurs USA				
					configuration.appendChild(<number_to_be_transfered>{conElts.PORTABILITE.NUMERO_PORTER}</number_to_be_transfered>);
					configuration.appendChild(<previous_carrier_name>{conElts.PORTABILITE.OPERATEUR_PRECEDENT}</previous_carrier_name>);
					configuration.appendChild(<previous_carrier_account_num>{conElts.PORTABILITE.NUM_COMPTE_FACT_PRECEDENT}</previous_carrier_account_num>);
					configuration.appendChild(<previous_carrier_pin>{conElts.PORTABILITE.CODE_PIN_PWD}</previous_carrier_pin>);
					configuration.appendChild(<mailing_addr_cur_phone_bill>{conElts.PORTABILITE.ADR_FACT_PRECEDENT}</mailing_addr_cur_phone_bill>);
					configuration.appendChild(<account_number>{conElts.PORTABILITE.NUM_COMPTE_FACT_NV}</account_number>);
					configuration.appendChild(<billing_account_address>{conElts.PORTABILITE.ADR_FACT_NV}</billing_account_address>);
					configuration.appendChild(<device_phone_number>{conElts.PORTABILITE.NUMERO_PORTER}</device_phone_number>);//ref pour insérer dans la table US
				}else
				//champs portab FR
				if(conElts.PORTABILITE.NUMERO.length > 0)
				{	
					//numéro de ligne saisi pour la porta FR
					configuration.appendChild(<soustete>{conElts.PORTABILITE.NUMERO}</soustete>);

					if(conElts.PORTABILITE.IDSOUSTETE > 0)
						configuration.appendChild(<idsoustete>{conElts.PORTABILITE.IDSOUSTETE}</idsoustete>);
					else
						configuration.appendChild(<idsoustete></idsoustete>);
				}
				else
				{
					
					configuration.appendChild(<soustete>{conElts.PORTABILITE.NUMERO_HIDDEN}</soustete>);
					configuration.appendChild(<idsoustete></idsoustete>);
				}
				
				
			}
			else
			{
				configuration.appendChild(<code_rio></code_rio>);
				configuration.appendChild(<date_portage></date_portage>);
				// ajouté pour portabilité specifique opérateurs USA		
				configuration.appendChild(<number_to_be_transfered></number_to_be_transfered>);
				configuration.appendChild(<previous_carrier_name></previous_carrier_name>);
				configuration.appendChild(<previous_carrier_account_num></previous_carrier_account_num>);
				configuration.appendChild(<previous_carrier_pin></previous_carrier_pin>);
				configuration.appendChild(<mailing_addr_cur_phone_bill></mailing_addr_cur_phone_bill>);
				configuration.appendChild(<account_number></account_number>);
				configuration.appendChild(<billing_account_address></billing_account_address>);
				
				//numero temp pour les nouvelles lignes
				if(conElts.PORTABILITE.NUMERO.length > 0)
				{
					configuration.appendChild(<soustete>{conElts.PORTABILITE.NUMERO}</soustete>);
					if(conElts.hasOwnProperty("NEW_SIM") && (conElts.NEW_SIM == true))	configuration.appendChild(<idsoustete>{conElts.PORTABILITE.IDSOUSTETE}</idsoustete>);
					
				}else{
					configuration.appendChild(<soustete>{conElts.PORTABILITE.NUMERO_HIDDEN}</soustete>);
				}
					
	
			}			

			
			
			
			
			if(conElts.ID_ORGA > 0 && conElts.ID_FEUILLE > 0)
			{
				configuration.appendChild(<idSource>{conElts.ID_ORGA}</idSource>);
				configuration.appendChild(<idCible>{conElts.ID_FEUILLE}</idCible>);
			}
			else
			{
				configuration.appendChild(<idSource></idSource>);
				configuration.appendChild(<idCible></idCible>);
			}
			
			if(conElts.ID_COLLABORATEUR > 0)
			{
				configuration.appendChild(<nomemploye>{conElts.COLLABORATEUR}</nomemploye>);
				configuration.appendChild(<idemploye>{conElts.ID_COLLABORATEUR}</idemploye>);
				configuration.appendChild(<matricule>{conElts.MATRICULE}</matricule>);
			}
			else
			{
				configuration.appendChild(<nomemploye></nomemploye>);
				configuration.appendChild(<idemploye></idemploye>);
				configuration.appendChild(<matricule></matricule>);
			}

			if(engagement.ELEGIBILITE > 0)
				configuration.appendChild(<duree_elligibilite>{engagement.ELEGIBILITE}</duree_elligibilite>);
			else
				configuration.appendChild(<duree_elligibilite>18</duree_elligibilite>);
			
			if(engagement.FPC != "")
			{
				configuration.appendChild(<fpc>{engagement.FPC}</fpc>);
				configuration.appendChild(<engagement_eq>24</engagement_eq>);
				configuration.appendChild(<engagement_res>24</engagement_res>);
			}
			else
			{
				configuration.appendChild(<fpc></fpc>);
				configuration.appendChild(<engagement_eq>{engagement.VALUE}</engagement_eq>);
				configuration.appendChild(<engagement_res>{engagement.VALUE}</engagement_res>);
			}
			//Prefix
			if(String(conElts.PREFIX) !='NaN')
				configuration.appendChild(<prefix>{String(conElts.PREFIX)}</prefix>);
			else
				configuration.appendChild(<prefix></prefix>);
			// Zone
			if(String(conElts.ZONE) !='NaN')
				configuration.appendChild(<zone>{String(conElts.ZONE)}</zone>);
			else
				configuration.appendChild(<zone></zone>);
			
			return configuration;
		}
		
		private function xmlEquipement(equElt:EquipementsElements, isEligible:Boolean):XML
		{
			var equipement 	:XML = <equipement></equipement>;	
			var PRIX		:String = '';
			
			if(isEligible)
				PRIX = equElt.PRIX.toString().replace(",",".");
			else
				PRIX = equElt.PRIX_C.toString().replace(",",".");
			
			equipement.appendChild(<idarticle_equipement></idarticle_equipement>);
			equipement.appendChild(<idequipement>{equElt.IDEQUIPEMENTPARC}</idequipement>);
			equipement.appendChild(<numeroserie></numeroserie>);
			equipement.appendChild(<code_ihm></code_ihm>);
			equipement.appendChild(<codepin></codepin>);
			equipement.appendChild(<codepuk></codepuk>);
			equipement.appendChild(<modele></modele>);
			equipement.appendChild(<commentaire></commentaire>);
			equipement.appendChild(<contrats></contrats>);
			
			equipement.appendChild(<reference>{equElt.REF_REVENDEUR}</reference>);

			equipement.appendChild(<idequipementfournis>{equElt.IDFOURNISSEUR}</idequipementfournis>);
			equipement.appendChild(<idequipementclient></idequipementclient>);	
			equipement.appendChild(<libelle>{equElt.LIBELLE}</libelle>);
			equipement.appendChild(<type_equipement>{equElt.EQUIPEMENT}</type_equipement>);
			equipement.appendChild(<idtype_equipement>{equElt.IDEQUIPEMENT}</idtype_equipement>);
			equipement.appendChild(<prix>{PRIX}</prix>);
			equipement.appendChild(<prixAffiche>{equElt.PRIX_C}</prixAffiche>);
			equipement.appendChild(<bonus>{Formator.formatBoolean(isEligible).toString()}</bonus>);//---> SINON PAR DEFAUT 0	
			
			
			if(equElt.IDPARENT > 0)
				equipement.appendChild(<idequipementfournisparent>{equElt.IDPARENT}</idequipementfournisparent>);
			else
				equipement.appendChild(<idequipementfournisparent></idequipementfournisparent>);

			return equipement;
		}
		
		private function xmlRessource(resElt:Object):XML
		{
			var ressource 	:XML = <ressource></ressource>;				
			
			if(resElt.PRIX_STRG == ResourceManager.getInstance().getString('M16', 'n_c'))
				resElt.PRICE = '';
			else
				resElt.PRICE = resElt.PRIX_UNIT.toString().replace(",",".");
			
			ressource.appendChild(<idarticle_ressource></idarticle_ressource>);
			ressource.appendChild(<bool_rapproche></bool_rapproche>);
			ressource.appendChild(<commentaire></commentaire>);
			
			ressource.appendChild(<reference>{resElt.REFERENCE_PRODUIT}</reference>);
			ressource.appendChild(<ajouter>ajouter</ajouter>);
			ressource.appendChild(<idproduit_catalogue>{resElt.IDCATALOGUE}</idproduit_catalogue>);
			ressource.appendChild(<prix>{resElt.PRICE}</prix>);
			ressource.appendChild(<prixAffiche>{resElt.PRICE}</prixAffiche>);
			ressource.appendChild(<bonus>{Formator.formatBoolean(resElt.OBLIGATORY)}</bonus>);
			ressource.appendChild(<libelle>{resElt.LIBELLE}</libelle>);
			ressource.appendChild(<theme>{resElt.LIBELLETHEME}</theme>);
			ressource.appendChild(<idThemeProduit>{resElt.IDPRODUIT}</idThemeProduit>);
			ressource.appendChild(<bool_acces>{resElt.BOOLACCES}</bool_acces>);
			ressource.appendChild(<action>{resElt.ACTION}</action>);		
			
			
			if(resElt.IDINVENTAIRE > 0)
				ressource.appendChild(<idressource>{resElt.IDINVENTAIRE}</idressource>);
			else
				ressource.appendChild(<idressource></idressource>);
			
			return ressource;
		}

	}
}