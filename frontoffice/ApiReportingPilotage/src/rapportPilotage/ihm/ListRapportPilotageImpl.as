package rapportPilotage.ihm
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.GroupingCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.events.FlexEvent;
	
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.service.chercherlisterapport.ChercherListeRapportService;
	import rapportPilotage.utils.CustomAdvancedDataGrid;
	import rapportPilotage.vo.Rapport;

	public class ListRapportPilotageImpl extends VBox
	{
		
		[Bindable]public var chercherListeRapportService:ChercherListeRapportService;
		[Bindable]public var allRapports :ArrayCollection;
		public var gc : GroupingCollection;
		public var  dgProperties:CustomAdvancedDataGrid;
		
		public function ListRapportPilotageImpl()
		{
			super();
			chercherListeRapportService=new ChercherListeRapportService();
		}
		
		
		
		/*----------------------------------------------------------------------------------
		**  permet de récupérer la liste des rapports lors que on appelle le composant 
		**  (popUP) plusieur fois 
		----------------------------------------------------------------------------------*/
	
		public  function getAllListeRapport():void
		{
			chercherListeRapportService.getListeRapport();
		}
		
		
		/*----------------------------------------------------------------------------------
		**  appelle le service permettant de recupérer les rapports 
		**  puis appeler la methode fillData lors que les data sont prêtes
		----------------------------------------------------------------------------------*/
		
		public function listrapportpilotage_creationCompleteHandler(event:FlexEvent):void
		{
			chercherListeRapportService.model.addEventListener(RapportEvent.USER_LIST_RAPPORT_EVENT,fillData);
			chercherListeRapportService.getListeRapport();	// appelle le service qui permet de recupérer les rapports puis appeler la methode fillData			
		}
		
		private function fillData(event:RapportEvent):void
		{
			allRapports=chercherListeRapportService.model.rapports;// rapports : contient tous les rapport de l'user
			gc.refresh();	// actualiser le GroupingCollection
		}
		
		
		protected function dataGridStyleFunction (data:Object, column:AdvancedDataGridColumn) : Object  
		{  
			var output:Object;  
			
			if ( data.children != null )  
			{  
				output = {color :0xB4131D , fontWeight:"bold", fontSize:12}  
			}  
			
			return output;  
		}
		
		protected function numberLabelFunction(item:Object, column:AdvancedDataGridColumn):String
		{
			var result:String = "";
			
			if(item.hasOwnProperty(column.dataField) && item[column.dataField])
			{
				result = ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
			}
			return result;
		}
	

	}
}