package intranet.gestionLoginClasse.services.user
{
    import composants.util.ConsoviewAlert;
    import flash.events.EventDispatcher;
    import intranet.gestionLoginClasse.entity.AccessVO;
    import intranet.gestionLoginClasse.entity.UserVO;
    import intranet.gestionLoginClasse.event.UserEvent;
    import mx.collections.ArrayCollection;
    import mx.rpc.events.ResultEvent;

    public class UserHandlers extends EventDispatcher
    {
        private var myDatas:UserDatas;

        public function UserHandlers(instanceOfDatas:UserDatas)
        {
            myDatas = instanceOfDatas;
        }

        internal function getUserAccessResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                myDatas.userVO = new UserVO();
                if (ArrayCollection(re.result).length > 0)
                {
                    var o:Object = ArrayCollection(re.result).getItemAt(0);
                    var access:AccessVO = new AccessVO();
                    access.deserialize(o);
                    myDatas.userVO.access = access;
                    dispatchEvent(new UserEvent(UserEvent.USER_ACCESS_LOAD_SUCCESS));
                }
            }
            else
            {
                dispatchEvent(new UserEvent(UserEvent.USER_ACCESS_LOAD_ERROR));
                trace("Erreur dans la récupération des types d'accès de l'utilisateur");
            }
        }

        internal function duplicateUserProfileResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                var o:Object = re.result;
                dispatchEvent(new UserEvent(UserEvent.DUPLICATE_PROFILE_SUCCESS));
            }
            else
            {
                dispatchEvent(new UserEvent(UserEvent.DUPLICATE_PROFILE_ERROR));
                trace("Erreur dans la duplication des données utilisateurs");
            }
        }
    }
}