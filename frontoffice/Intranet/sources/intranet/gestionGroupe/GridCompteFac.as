package intranet.gestionGroupe
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import mx.managers.PopUpManager;
	import mx.events.FlexEvent;
	import mx.controls.Alert;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.ConsoViewErrorManager;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	
	public class GridCompteFac extends GridCompteFacIHM
	{
		[Bindable]
		private var myFilterArrayCF:ArrayCollection=new ArrayCollection();
		private var objGestionImport : GestionImport;
		public function GridCompteFac(objGestionImport : GestionImport)
		{
			this.objGestionImport=objGestionImport;
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(event:Event):void
		{
			btnDeleteCF.enabled=false;
			btnModifCF.enabled=false;
			txtFiltreCF.addEventListener(Event.CHANGE,filtreCF);
			/*Gestion des comptes de facturation*/
			btnAddCF.addEventListener(MouseEvent.CLICK,msgBoxAddCompteFac);
			btnDeleteCF.addEventListener(MouseEvent.CLICK,msgDeleteCompteFac);
			btnModifCF.addEventListener(MouseEvent.CLICK,msgBoxModifCompteFac);
			txtFiltreCF.addEventListener(Event.CHANGE,filtreCF);
			dgCF.addEventListener(Event.CHANGE,checkOrga);
			
		}
		private function msgBoxAddCompteFac(event : MouseEvent):void{
			if (getSociete().dgRefClient.selectedIndex!=-1){
			var monPopUp : FenetreAjouterCompteFac = new FenetreAjouterCompteFac(this,false);
			PopUpManager.addPopUp(monPopUp,this,true);
			PopUpManager.centerPopUp(monPopUp);
			}
			else
			{
				Alert.show("Selectionnez une société");
			}
		}
		private function processGetCF(event:ResultEvent):void {
			myFilterArrayCF=event.result as ArrayCollection;
			dgCF.dataProvider=myFilterArrayCF;
		}
		private function filtreCF(e:Event):void {
			myFilterArrayCF.filterFunction=utilFiltreGridCF;
			myFilterArrayCF.refresh();
		}
		public function getCF(idRefClient : int):void {
				var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.procedureGestionGroupeCompteFacturation",
																		"getListeCF",
																		processGetCF,remoteError);
					RemoteObjectUtil.callService(op,idRefClient);
				
		}
		private function utilFiltreGridCF(item:Object):Boolean {
			if (((item.CF as String).toLowerCase()).search(txtFiltreCF.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		
		private function msgDeleteCompteFac(event : MouseEvent):void{
			if (dgCF.selectedIndex !=-1){
				Alert.show("Confirmez la suppression du compte de facturation svp","Confirmation",Alert.OK | Alert.CANCEL,null,deleteCF,null,Alert.OK);
			}
			else
			{
				Alert.show("Sélectionnez un compte de facturation");
			}
		}
	
		private function processDeleteCompteFac(event:ResultEvent):void {
			trace("Suppression compte fac-----"+event.toString());
			if (event.result as int == -1){
				Alert.show("Impossible de supprimer ce compte de facturation. Certains éléments lui font référence.");
			}
			else
			{
				Alert.show("Suppression compte Fac : ok");
						
			}
			getCF(getSociete().dgRefClient.selectedItem.IDREF_CLIENT);
		}
		private function msgBoxModifCompteFac(event : MouseEvent):void{
		if(dgCF.selectedIndex!=-1){
			var monPopUp : FenetreAjouterCompteFac = new FenetreAjouterCompteFac(this,true,dgCF.selectedItem.CF,dgCF.selectedItem.OPERATEURID);
			PopUpManager.addPopUp(monPopUp,this,true);
			PopUpManager.centerPopUp(monPopUp);
			}
			else
			{
				Alert.show("Sélectionnez un compte de facturation");
			}
		}
		public function addCF(oldNumCoresp: String,oldIdOpe : int,numero_correspond: String,idOperateur : int, modif : Boolean=false):void {
			if (modif==false)
			{
				var op : AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.procedureGestionGroupeCompteFacturation",
																		"addCF",
																		processaddCF,remoteError);
				RemoteObjectUtil.callService(op,getSociete().dgRefClient.selectedItem.IDREF_CLIENT,idOperateur,numero_correspond);
				
			}
			else
			{
				var op2 : AbstractOperation= RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.procedureGestionGroupeCompteFacturation",
																		"updateCF",
																		processModifCF,remoteError);
				RemoteObjectUtil.callService(op2,getSociete().dgRefClient.selectedItem.IDREF_CLIENT,oldNumCoresp,oldIdOpe,getSociete().dgRefClient.selectedItem.IDREF_CLIENT,numero_correspond,idOperateur);
				
			}
					
		}
		private function remoteError(faultEvent:FaultEvent):void {
			ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
		}
		private function deleteCF(evt_obj : Object):void {
			if (evt_obj.detail == Alert.OK) {
 				var op:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.procedureGestionGroupeCompteFacturation",
																		"deleteCF",
																		processDeleteCompteFac,remoteError);
			RemoteObjectUtil.callService(op,dgCF.selectedItem.IDREF_CLIENT,dgCF.selectedItem.OPERATEURID,dgCF.selectedItem.CF);
 				trace("------------------------>   "+dgCF.selectedItem.OPERATEURID+"---"+dgCF.selectedItem.IDREF_CLIENT+"---"+dgCF.selectedItem.CF);
		 	}
		 	else
		 	{ 
		 		if (evt_obj.detail == Alert.CANCEL) 
		 		{
 				//Alert.show("annulé");
 				}
			}
					
		}
		
		private function processaddCF(event:ResultEvent):void {
			getCF(getSociete().dgRefClient.selectedItem.IDREF_CLIENT);
		}
		private function processModifCF(event:ResultEvent):void {
			if (event.result as int == -2){
				Alert.show("Impossible de modifier ce compte de facturation. Certains éléments lui font référence.");
			}
			else
			{
				Alert.show("Modification : ok");
				//getListeGroupe(null);
				
			}
			getCF(getSociete().dgRefClient.selectedItem.IDREF_CLIENT);
		}
		private function checkOrga(event:Event):void {
			if (dgCF.selectedIndex!=-1) {
				btnDeleteCF.enabled=true;
				btnModifCF.enabled= true;
			}
			else
			{
				btnDeleteCF.enabled=false;
				btnModifCF.enabled=false;
			}
		
		}	
		public function getSociete():GridSocieteAffectee
		{
			return objGestionImport.getSocieteSelectionne();
		}		
		
	}
}