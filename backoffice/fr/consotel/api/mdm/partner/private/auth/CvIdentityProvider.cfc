<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.auth.CvIdentityProvider"
extends="fr.consotel.api.mdm.partner.private.auth.ClientIdentityProvider" hint="Implémentation simple pour ConsoView
L'utilisateur est dit authentifié ssi les scopes APPLICATION et SESSION sont définis et une clé SESSION.AUTH_STATE existe et vaut 1">
	<cffunction access="package" name="assertAuthIdentity" returntype="boolean" hint="Retourne TRUE si les condition suivantes sont vérifiées et FALSE sinon :
	- L'utilisateur est dit authentifié ssi les scopes APPLICATION et SESSION sont définis
	- La clé SESSION.AUTH_STATE existe et vaut 1">
		<cfargument name="authIdentity" type="Struct" required="true" hint="Valeur retournée par la méthode getAuthIdentity() de cette implémentation">
		<cfset var userIdentity=ARGUMENTS["authIdentity"]>
		<cfset var authAssertion=(isDefined("APPLICATION") AND isDefined("SESSION"))>
		<cfif authAssertion>
			<cfset authAssertion=authAssertion AND structKeyExists(SESSION,"AUTH_STATE") AND (VAL(SESSION["AUTH_STATE"]) EQ 1)>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les scopes APPLICATION et SESSION doivent être définis pour cette stratégie d'authentification">
		</cfif>
		<cfreturn authAssertion>
	</cffunction>

	<cffunction access="package" name="getAuthIdentity" returntype="Struct" hint="La structure retournée est authCredentials">
		<cfargument name="authCredentials" type="Struct" required="true">
		<cfreturn ARGUMENTS["authCredentials"]>
	</cffunction>
</cfcomponent>