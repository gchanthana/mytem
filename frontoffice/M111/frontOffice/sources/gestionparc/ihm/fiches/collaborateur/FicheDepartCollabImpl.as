package gestionparc.ihm.fiches.collaborateur
{
	import composants.controls.TextInputLabeled;
	import composants.util.CvDateChooser;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.popup.PanelConfirmationRebus;
	import gestionparc.services.collaborateur.CollaborateurServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheDepartCollabImpl extends TitleWindow
	{
		public var btAnnuler:Button;
		public var btValider:Button;
		public var calendar:CvDateChooser;
		public var dgListeTerm:DataGrid;
		public var dgListeSIM:DataGrid;
		public var dgListeEqpFixe:DataGrid;
		public var lab_emp:Label;
		public var txtFiltre:TextInputLabeled;
		public var txtFiltreSIM:TextInputLabeled;
		public var txtFiltreEqpFixe:TextInputLabeled;
		
		[Bindable]
		public var vbEquipement:VBox;
		[Bindable]
		public var vbSim:VBox;
		[Bindable]
		public var vbEqpFixe:VBox;

		[Bindable]
		public var serv:CollaborateurServices;

		public var ligne:AbstractMatriceParcVO;
		private var PanelConf:PanelConfirmationRebus;
		private var boolVide:Boolean=true;

		public function FicheDepartCollabImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			serv=new CollaborateurServices();
		}

		private function initIHM(evt:FlexEvent):void
		{
			initListerner();
			initData();
			initDisplay();
		}

		private function initListerner():void
		{
			txtFiltre.addEventListener(Event.CHANGE, txtFiltreChangeHandlerTerm);
			txtFiltreSIM.addEventListener(Event.CHANGE, txtFiltreChangeHandlerSIM);
			txtFiltreEqpFixe.addEventListener(Event.CHANGE, txtFiltreChangeHandlerEqpFixe);
			addEventListener(CloseEvent.CLOSE, fermer);
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);

			serv.myDatas.addEventListener(gestionparcEvent.INIT_DEPART_ENTREPRISE_LOADED, refreshDisplay)
		}

		private function refreshDisplay(event:gestionparcEvent=null):void
		{
			// EQuiPEMENT
			if (serv.myDatas.listeEqp != null && serv.myDatas.listeEqp.length > 0)
			{
				vbEquipement.visible=true;
				boolVide=false;
			}
			else
				vbEquipement.visible=false;

			// SIM
			if (serv.myDatas.listeSim != null && serv.myDatas.listeSim.length > 0)
			{
				vbSim.visible=true;
				boolVide=false;
			}
			else
				vbSim.visible=false;

			// EQP FIXE
			if (serv.myDatas.listeEqpFixe != null && serv.myDatas.listeEqpFixe.length > 0)
			{
				vbEqpFixe.visible=true;
				boolVide=false;
			}
			else
				vbEqpFixe.visible=false;

			if (boolVide)
				valider(null);
		}

		private function initDisplay():void
		{
			lab_emp.text=ligne.COLLABORATEUR;
			calendar.selectedDate=new Date();
		}

		private function initData():void
		{
			serv.initDepartEntreprise(ligne.IDEMPLOYE, SpecificationVO.getInstance().idPool);
		}

		private function txtFiltreChangeHandlerTerm(ev:Event):void
		{
			try
			{
				serv.myDatas.listeEqp.filterFunction=filtrerLeGridTerm;
				serv.myDatas.listeEqp.refresh();
			}
			catch (e:Error)
			{
				//trace("Erreur Gestion Flotte GSM");
			}
		}

		private function filtrerLeGridTerm(item:Object):Boolean
		{
			if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MAARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}

		private function txtFiltreChangeHandlerSIM(ev:Event):void
		{
			try
			{
				serv.myDatas.listeSim.filterFunction=filtrerLeGridSIM;
				serv.myDatas.listeSim.refresh();
			}
			catch (e:Error)
			{
				//trace("Erreur Gestion Flotte GSM");
			}
		}

		private function filtrerLeGridSIM(item:Object):Boolean
		{
			if ((String(item.NUM_SIM).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1) || (String(item.REVENDEUR).toLowerCase().search(txtFiltreSIM.text.toLowerCase()) != -1))
				return true
			else
				return false
		}

		private function txtFiltreChangeHandlerEqpFixe(ev:Event):void
		{
			try
			{
				serv.myDatas.listeEqpFixe.filterFunction=filtrerLeGridEqpFixe;
				serv.myDatas.listeEqpFixe.refresh();
			}
			catch (e:Error)
			{
				//trace("Erreur Gestion Flotte GSM");
			}
		}

		private function filtrerLeGridEqpFixe(item:Object):Boolean
		{
			if ((String(item.MARQUE).toLowerCase().search(txtFiltreEqpFixe.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltreEqpFixe.text.toLowerCase()) != -1) || (String(item.IMEI).toLowerCase().search(txtFiltreEqpFixe.text.toLowerCase()) != -1))
				return true
			else
				return false
		}

		private function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function valider(evt:MouseEvent):void
		{

			var vbox_rendu:VBox=new VBox();
			var vbox_garde:VBox=new VBox();

			var xml:String="<elements>";

			var listeID:Array=[];

			xml+="<terminaux>";
			
			for (var i:int=0; i < (dgListeTerm.dataProvider as ArrayCollection).length; i++)
			{
				var unLabel:Label=new Label();
				unLabel.text=ResourceManager.getInstance().getString('M111', 'Terminal___') + (dgListeTerm.dataProvider as ArrayCollection).getItemAt(i).IMEI;

				if ((dgListeTerm.dataProvider as ArrayCollection).getItemAt(i).SELECTED == 1)
				{
					vbox_rendu.addChild(unLabel);
					xml+="<terminal><ID>" + (dgListeTerm.dataProvider as ArrayCollection).getItemAt(i).ID + "</ID></terminal>";
				}
				else
				{
					vbox_garde.addChild(unLabel);
				}
			}
			xml+="</terminaux><sims>";

			for (var a:int=0; a < (dgListeSIM.dataProvider as ArrayCollection).length; a++)
			{
				var unLabelSIM:Label=new Label();
				unLabelSIM.text=ResourceManager.getInstance().getString('M111', 'Carte_SIM___') + (dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).NUM_SIM;

				if ((dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).SELECTED == 1)
				{
					xml+="<sim><ID>" + (dgListeSIM.dataProvider as ArrayCollection).getItemAt(a).ID + "</ID></sim>";
					vbox_rendu.addChild(unLabelSIM);
				}
				else
				{
					vbox_garde.addChild(unLabelSIM);
				}
			}
			xml+="</sims><eqpfixes>";

			for (var b:int=0; b < (dgListeEqpFixe.dataProvider as ArrayCollection).length; b++)
			{
				var unLabelFIXE:Label=new Label();
				unLabelFIXE.text=ResourceManager.getInstance().getString('M111', 'EQP') + ' : ' + (dgListeEqpFixe.dataProvider as ArrayCollection).getItemAt(b).MARQUE + ' ' + (dgListeEqpFixe.dataProvider as ArrayCollection).getItemAt(b).MODELE;

				if ((dgListeEqpFixe.dataProvider as ArrayCollection).getItemAt(b).SELECTED == 1)
				{
					xml+="<eqpfixe><ID>" + (dgListeEqpFixe.dataProvider as ArrayCollection).getItemAt(b).ID + "</ID></eqpfixe>";
					vbox_rendu.addChild(unLabelFIXE);
				}
				else
				{
					vbox_garde.addChild(unLabelFIXE);
				}
			}
			xml+="</eqpfixes>";
			xml+="</elements>";

			PanelConf=new PanelConfirmationRebus(vbox_rendu, vbox_garde, calendar.selectedDate, xml, SpecificationVO.getInstance().elementDataGridSelected, boolVide);
			PanelConf.serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, fermer);
			PopUpManager.addPopUp(PanelConf, this, true);
			PopUpManager.centerPopUp(PanelConf);

		}
	}
}
