package univers.inventaire.lignesetservices.views
{
	import mx.containers.VBox;
	import univers.inventaire.lignesetservices.applicatif.GestionTechniqueLignesApp;
	import mx.controls.ComboBox;
	import composants.controls.TextInputLabeled;
	import mx.controls.TextArea;
	import mx.containers.FormItem;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.controls.DataGrid;
	
	
	[Bindable]
	public class OngletOperateurImpl extends BaseView
	{
		
		public var OPERATEUR_CREATEUR_NUMERO : TextInputLabeled;
		public var OPERATEUR_ACCES : TextInputLabeled;
		public var COMPTE_FACTURATION : TextInputLabeled;
		public var SOUS_COMPTE_FACTURATION : TextInputLabeled;
		public var COMMENTAIRE_OPERATEUR_INDIRECT_2 : TextArea;
		public var IDAGENCE_OPERATEUR : TextInputLabeled;
		public var PRESTATAIRE : TextInputLabeled;
		public var COMMENTAIRE_OPERATEUR : TextArea;
		public var LISTE_OPERATEURS : DataGrid;
		
		public var libelle9 : FormItem;
		public var libelle10 : FormItem;	
		public var V9 : TextInputLabeled;
		public var V10 : TextInputLabeled;
		public var btChampPerso1 : Button;
		public var btChampPerso2 : Button;
		
		public var btUpdate : Button;
		
		
		public function OngletOperateurImpl()
		{
			//TODO: implement function
			super();
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
		}
		
				
		//======================== HANDLERS ==========================================
		protected function btUpdateClickHandler(me : MouseEvent):void{
			
		}
		//======================== FIN HANDLERS ======================================
		
	}
}