package service.destinataires.listedestinataireprofil
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeDestinatairesProfilHandler
	{
		private var _model:ListeDestinatairesProfilModel;
		
		public function ListeDestinatairesProfilHandler(_model:ListeDestinatairesProfilModel):void
		{
			this._model = _model;
		}
		
		internal function getListeDestinatairesProfilResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.listeDestinatairesProfil(event.result as ArrayCollection);
			}
		}	
	}
}