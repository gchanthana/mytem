﻿<cfcomponent displayname="fr.consotel.consoview.M16.v2.ArticleService" hint="Gestion des commandes pour articles SPIE" output="false">
	
	<cffunction name="fournirListeArticle" access="remote" returntype="Query" output="false" hint="retourne la liste des articles spie">
		<cfargument name="OK" type="String" required="true" >
		<cfargument name="data" type="Struct" required="true">
		
		<cfset p_idgroupe_client = session.perimetre.id_groupe >
		<cfset p_code_langue= session.user.globalization >
		<cfset p_app_loginid = session.user.clientaccessid>
		
		<cftry>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.ReferenceProfil">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#data.p_operateurid#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#p_app_loginid#" null="true">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#p_idgroupe_client#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#data.p_idprofil_equipement#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" value="#p_code_langue#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#data.p_segment#">
				
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfreturn p_retour>
			
		<cfcatch type="Any">
			<cfrethrow>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
	<!--- FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT SPIE--->

	<cffunction name="fournirTerminaux" access="remote" returntype="query" output="false" hint="FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT">
		<cfargument name="idpoolgestionnaire" 		required="true" type="Numeric"/>
		<cfargument name="idprofilequipement" 		required="true" type="Numeric"/>
		<cfargument name="idfournisseur" 			required="true" type="Numeric"/>
		<cfargument name="idoperateur"	 			required="true" type="Numeric"/>
		<cfargument name="segment" 					required="true" type="Numeric"/>
		<cfargument name="isnew" 					required="true" type="Numeric"/>
	
			<cfset equInfos = structnew()>
		
			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idcodelangue 	= session.user.idglobalization>
			
				<cfset idtypeequipement = 70>
				
				<cfif Arguments.segment GT 1>
					<cfset idtypeequipement = 0>		
				</cfif>
				
				<cfset idcategorieequipement = 0>		
				<cfset typefournis = 1>
				<cfset idgammefournis = 0>
				<cfset niveau = 1>
				<cfset clefrecherche = ''>
				
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SEARCHCATALOGUECLIENT_V6">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idgestionnaire#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#idPoolGestionnaire#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idcategorieequipement#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idtypeequipement#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idFournisseur#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idgammefournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefrecherche#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#idracine#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#idcodelangue#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#segment#">
					<cfprocresult name="p_retour">
				</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
</cfcomponent>