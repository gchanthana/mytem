<cfdump var="#SERVER#">
<cfdump var="#APPLICATION#">

<cfobject name="prs" type="component" component="fr.consotel.consoview.inventaire.commande.export.ExportCommandePDF">

<cfif isDefined("form.IDTYPE_COMMANDE")>
	<cfset prs.afficherCommandeEnPDF(form.IDCOMMANDE, form.IDTYPE_COMMANDE)>
<cfelse>
 	<cfset prs.afficherCommandeEnPDF(form.IDCOMMANDE,1083)>
</cfif>

<cfheader name="Content-Disposition"  value="inline;filename=Commande_#trim(form.NUMERO_COMMANDE)#.pdf">
<cfcontent type="#prs.getRunReportContentType()#" variable="#prs.getRunReportData()#">