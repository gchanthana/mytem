package service.diffusion.nombredestinataire
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class NombreDestinataireHandler
	{
		private var _model:NombreDestinataireModel;

		public function NombreDestinataireHandler(_model:NombreDestinataireModel)
		{
			this._model=_model;
		}

		internal function nbProfilHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.nbProfilDiffusion(event.result as ArrayCollection);
			}
		}
	}
}
