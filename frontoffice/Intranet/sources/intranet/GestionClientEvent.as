package intranet
{
	import flash.events.Event;

	public class GestionClientEvent extends Event
	{
		public static const REFRESH_DATAGRID:String = "REFRESH_DATAGRID";
		
		public function GestionClientEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}