<cfcomponent author="Cedric" displayname="fr.consotel.api.remoting.AxisUtils" hint="Ensemble d'utilitaires pour le client Apache Axis">
	<cffunction access="public" name="getAxisFaultStruct" returntype="Struct"
	hint="La structure contenant les clés suivantes est retournée si catchInfos contient une clé faultString qui correspond à une erreur de type AxisFault au format JSON :
	faultCode, faultSubcode, faultString, faultActor, faultNode, faultDetail, xhtmlFaultDetail (Chaine vide ou détail d'erreur en HTML). Sinon une strucure vide est retournée">
		<cfargument name="catchInfos" type="Any" required="true" hint="Structure CFCATCH correspondant à l'exception capturée par CFTRY/CFCATCH">
		<cfset var catchStruct=ARGUMENTS["catchInfos"]>
		<cfset var faultStruct={}>
		<cfif structKeyExists(catchInfos,"faultString")>
			<!--- Contenu type d'une message Apache AxisFault --->
			<cfset var faultCheckItems=reFindNoCase("^AxisFault[[:space:]]+",catchInfos["faultString"],0,TRUE)>
			<cfset var isAxisFault=yesNoFormat(faultCheckItems["POS"][1])>
			<cfif isAxisFault>
				<cfset var axisJsonFault="">
				<cfset var CHARS={QOT="""", JSEP=","}>
				<cfset var startPos=faultCheckItems["POS"][1]+faultCheckItems["LEN"][1]>
				<cfset var nbChars=1+ LEN(catchInfos["faultString"]) - startPos>
				<cfset var axisFaultInfos=MID(catchInfos["faultString"],startPos,nbChars)>
				<!--- Liste des clés utilisées par Apache Axis --->
				<cfset var openDelimitersRgx="(faultCode|faultSubcode|faultString|faultActor|faultNode|faultDetail)(:)[[:blank:]]*">
				<!--- Séparateur supposé utilisé par Apache Axis pour les clés --->
				<cfset var closeDelimitersRgx="([\n\r])[[:blank:]]+">
				<cfset axisFaultInfos=reReplaceNoCase(axisFaultInfos,openDelimitersRgx,"#CHARS.QOT#\1#CHARS.QOT# \2 #CHARS.QOT#","ALL")>
				<cfset axisFaultInfos=reReplaceNoCase(axisFaultInfos,closeDelimitersRgx,"#CHARS.QOT##CHARS.JSEP# ","ALL")>
				<cfset axisJsonFault="{" & TRIM(axisFaultInfos) & "#CHARS.QOT#}">
				<!--- Désérialisation du message d'erreur Apache AxisFault --->
				<cfif isJSON(axisJsonFault)>
					<cfset faultStruct=deserializeJSON(axisJsonFault)>
					<cfset faultStruct["xhtmlFaultDetail"]="">
					<!--- Désérialisation d'un eventuel détail d'erreur AxisFault au format HTML --->
					<cfif structKeyExists(faultStruct,"faultDetail")>
						<cfset var htmlContentRgx="&lt;!DOCTYPE[[:space:][:print:]]+&gt;[[:space:]]*&lt;HTML&gt;[[:space:][:print:]]+&lt;/HTML&gt;">
						<cfset var htmlContentFind=reFindNoCase(htmlContentRgx,faultStruct["faultDetail"],0,TRUE)>
						<cfif htmlContentFind["POS"][1] GT 0>
							<!--- Détail d'erreur au format HTML --->
							<cfset faultStruct["xhtmlFaultDetail"]=xmlParse("<XMLDOC>" & MID(faultStruct["faultDetail"],htmlContentFind["POS"][1],htmlContentFind["LEN"][1]) & "</XMLDOC>")>
							<!--- Suprresion du détail d'erreur au format HTML du détail AxisFault --->
							<cfset faultStruct["faultDetail"]=reReplaceNoCase(faultStruct["faultDetail"],htmlContentRgx,",","ALL")>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn faultStruct>
	</cffunction>
</cfcomponent>