package univers.facturation.suivi.factures.services
{
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.rpc.AbstractOperation;

    public class FactureServices
    {
        //VARIABLES------------------------------------------------------------------------------
        public var myDatas:FactureDatas;
        public var myHandlers:FactureHandlers;

        //METHODE PUBLIC-------------------------------------------------------------------------
        public function FactureServices()
        {
            myDatas = new FactureDatas();
            myHandlers = new FactureHandlers(myDatas);
        }

        public function ajouterProduitAMesFavoris(value:Object):void
        {
            if (value.hasOwnProperty("IDPRODUIT_CATALOGUE") && value.IDPRODUIT_CATALOGUE > 0)
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.addFacture", "ajouterProduitAMesFavoris", myHandlers.ajouterProduitAMesFavorisResultHandler);
                RemoteObjectUtil.callService(op, value.IDPRODUIT_CATALOGUE);
            }
        }

        public function supprimerProduitDeMesFavoris(value:Object):void
        {
            if (value.hasOwnProperty("IDPRODUIT_CATALOGUE") && value.IDPRODUIT_CATALOGUE > 0)
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.addFacture", "supprimerProduitDeMesFavoris", myHandlers.supprimerProduitDeMesFavorisResultHandler);
                RemoteObjectUtil.callService(op, value.IDPRODUIT_CATALOGUE);
            }
        }

        public function fournirListeProduit(pOperateur_id:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.addFacture", "getAllProduit", myHandlers.fournirListeProduitResultHandler);
            RemoteObjectUtil.callService(op, pOperateur_id);
        }

        public function fournirListeProduitFavoris():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M32.addFacture", "getAllProduit", myHandlers.fournirListeProduitFavoriResultHandler);
            RemoteObjectUtil.callService(op);
        }
        //METHODE PUBLIC-------------------------------------------------------------------------
    }
}