<!--- =========================================================================
Classe: GroupeLignePartielStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeLignePartielStrategy" hint=""  extends="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GroupeLignePartielStrategy" hint="Remplace le constructeur de GroupeLignePartielStrategy.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="query" output="false" hint="Ramène les données du tableau de bord niveau thème." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDCompte" required="false" type="numeric" default="" displayname="numeric IDCompte" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="numeric" default="" displayname="numeric DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="numeric" default="" displayname="numeric DateFin" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getLibelle" access="public" returntype="string" output="false" hint="Retourne le libéllé du thème passé en paramètre." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getNumero" access="public" returntype="string" output="false" hint="Retourne le libéllé du périmètre." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>