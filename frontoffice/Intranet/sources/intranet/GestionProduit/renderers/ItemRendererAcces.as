package intranet.GestionProduit.renderers
{
	import intranet.GestionProduit.DataSample;
	import intranet.GestionProduit.event.ChangeData_EVENT;
	
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;

	public class ItemRendererAcces extends HBox
	{
		public var cbo:ComboBox;  // remise = 1, produit = 2, aucun = 3
		
		public function ItemRendererAcces()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function init(e:FlexEvent):void{
			cbo.addEventListener(ListEvent.CHANGE,changeAcces);
		}
 		
		override public function set data(value:Object):void{
			if(value)
			{
				super.data=value;
				
				this.removeAllChildren();	
				cbo.dataProvider=DataSample.TabAcces;
				cbo.width=80;
				cbo.dropdownWidth=150;
				cbo.rowCount=4;
				cbo.prompt="";
							
					if(data.BOOL_ACCES==1){//prod access.
						cbo.selectedIndex = 2;
					}else if(data.TOUT_ACCES == 1){//remise
						cbo.selectedIndex = 1;
					}else if(data.BOOL_ACCES == 0){//aucun
						cbo.selectedIndex = 3;
					}else //rien
					{
						cbo.selectedIndex = 0;		
					}
					this.addChild(cbo);
	 		}
		}
		
		public function changeAcces(e:ListEvent):void{
			var item:Object=new Object();
			
			switch (cbo.selectedIndex){
				case 0:
					data.BOOL_ACCES=-1;
					data.TOUT_ACCES=-1;
					break;
				case 1: 
					data.BOOL_ACCES=0;
					data.TOUT_ACCES=1;
					break;
				case 2:
					data.BOOL_ACCES=1;
					data.TOUT_ACCES=1;
					break;
				case 3:
					data.BOOL_ACCES=0;
					data.TOUT_ACCES=0;
					break;
			}
			
			item.ok= true;
			item.BOOL_ACCES= data.BOOL_ACCES;
			item.TOUT_ACCES= data.TOUT_ACCES;
			item.IDPRODUIT_CATALOGUE= data.IDPRODUIT_CATALOGUE;
			item.IDTHEME_PRODUIT=data.IDTHEME_PRODUIT;
			
			dispatchEvent(new ChangeData_EVENT(ChangeData_EVENT.ChangeData_EVENT,item,true,false));	
		}
	}
}