<cfcomponent>

	<cffunction name="getListeFactures" access="remote" returntype="query">
		<cfargument name="idsous_tete" type="numeric" required="true">
	
		<cfset idRacine = session.perimetre.id_groupe>
		<cfset idracine_master = session.perimetre.idracine_master>
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111B.getLineBills">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#idracine_master#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete" value="#idsous_tete#" null="false">
	
		    <cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>

</cfcomponent>