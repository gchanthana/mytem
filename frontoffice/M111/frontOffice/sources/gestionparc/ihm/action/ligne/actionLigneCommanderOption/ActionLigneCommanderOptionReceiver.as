package gestionparc.ihm.action.ligne.actionLigneCommanderOption
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.gestion.GestionCommandeMobile;

	public class ActionLigneCommanderOptionReceiver
	{
		private var srcObject:Object;
		private var gestionCommmande:GestionCommandeMobile;

		public function ActionLigneCommanderOptionReceiver()
		{
		}

		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 * @return void
		 */
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}

		/**
		 * Cette fonction permet d'afficher l'IHM permettant de faire l'action.
		 */
		private function displayIHM(srcObject:Object):void
		{
			gestionCommmande=new GestionCommandeMobile(srcObject);
			gestionCommmande.goModification();
		}

		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.ligne=SpecificationVO.getInstance().elementDataGridSelected;
			srcObject.IDTYPECOMMANDE=3;
			srcObject.IDPOOL=SpecificationVO.getInstance().idPool;
			srcObject.IDPROFIL=SpecificationVO.getInstance().idProfil;
			srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeur;
			srcObject.LIBELLE_CONCAT=SpecificationVO.getInstance().labelPool + " - " + SpecificationVO.getInstance().labelProfil;
			srcObject.LIBELLE_POOL=SpecificationVO.getInstance().labelPool;
			srcObject.LIBELLE_PROFIL=SpecificationVO.getInstance().labelProfil;
			srcObject.BOOLNEWCOMMANDE=true;

		}
	}
}
