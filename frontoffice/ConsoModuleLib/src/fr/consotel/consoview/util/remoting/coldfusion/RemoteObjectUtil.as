package fr.consotel.consoview.util.remoting.coldfusion {
	import appli.events.ConsoViewEvent;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import flash.errors.IllegalOperationError;
	import flash.events.EventDispatcher;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.utils.ObjectUtil;

	public class RemoteObjectUtil extends EventDispatcher {
		public static const DEFAULT_DESTINATION:String = "ColdFusion";
		public static const EXPIRED_SESSION:int = 1000; // Session Invalide ou ExpirÃ©e (BackOffice).
		public static const ACCESS_REFUSED:int = 1001; // Session Valide mais credentials invalides (BackOffice)
		public static const CONNECT_FAULT_ERROR:int = 1002; // FAULT durant le remoting de connexion
		public static const DISCONNECTED_SESSION:int = 1003; // Remoting lancÃ© alors que Session dÃ©connectÃ©e
		public static const LOGOFF_FAULT_ERROR:int = 1004; // FAULT durant un remoting de dÃ©connexion
		public static const EMPTY_NODE_ACCESS:int = 1005; // Arbre des pÃ©rimÃ¨tres vide
		public static const FIRST_NODE_NO_ACCESS:int = 1006; // Le 1er noeud d'accÃ¨s n'a pas d'accÃ¨s (STC = 0)
		public static const BROKEN_PARENTAL_LINK:int = 1007; // La hiÃ©rarchie des noeud est incorrecte (Parent -> Enfant)
		public static const XML_PARAM_ERROR:int = 1008; // Erreur Param?tre XML (Remoting).
		public static const IPADDR_ACCESS_REFUSED:int = 1009; // Adresse IP refus?e (Connexion).
		
		//public static const INVALID_GROUP_NODES_ERROR:int = 1007; // Arbre PÃ©rimÃ¨tres non valide
		private static const ACCESS_COMPONENT:String = "fr.consotel.consoview.access.AccessManager";
		private static var remotingCount:Number;
		private static var loginString:String;
		private static var currentCancelableOperation:AbstractOperation;
		private static var tabCancelableOperations : Array;
		private var credentialsObject:Object;
		
		
		private static var singletonInstance:RemoteObjectUtil = null;
		public function RemoteObjectUtil() {
			if(RemoteObjectUtil.singletonInstance == null) {
				RemoteObjectUtil.singletonInstance = this;
				RemoteObjectUtil.remotingCount = 0;
				RemoteObjectUtil.tabCancelableOperations = [];
			} 
			else
			{
				throw new IllegalOperationError("(RemoteObjectUtil) Singleton Creation Error");
			}	
		}
		public static function getSession():RemoteObjectUtil
		{
			return singletonInstance;
		}
		public function logoff():void{
			singletonInstance = null;
			RemoteObjectUtil.tabCancelableOperations = null;
			if(RemoteObjectUtil.singletonInstance == null) 
			{
				RemoteObjectUtil.singletonInstance = this;
				RemoteObjectUtil.tabCancelableOperations = [];
			}
			else
			{
				throw new IllegalOperationError("(RemoteObjectUtil) Singleton Creation Error");
			}	
		}

		public static function getOperation(source:String, method:String,
											resultHandler:Function, cursorBusy:Boolean = false):AbstractOperation {
			var tmpRemoteObj:RemoteObject = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			tmpRemoteObj.showBusyCursor = false;
			tmpRemoteObj.source = source;
			
			var tmpOperation:AbstractOperation = tmpRemoteObj.getOperation(method);
			tmpOperation.addEventListener(ResultEvent.RESULT,resultHandler);
			tmpOperation.addEventListener(ResultEvent.RESULT,RemoteObjectUtil.resultHandlerMethod);
			tmpOperation.addEventListener(FaultEvent.FAULT,RemoteObjectUtil.faultHandlerMethod);
			
			tmpRemoteObj = null;
			return tmpOperation;
		}
		
		
		
		//================ OPERATION SILENCIEUSE ===================================
		
		public static function getSilentOperation(source:String, method:String,	resultHandler:Function, cursorBusy:Boolean = false):AbstractOperation 
		{
			var tmpRemoteObj:RemoteObject = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			tmpRemoteObj.showBusyCursor = false;
			tmpRemoteObj.source = source;
			var tmpOperation:AbstractOperation = tmpRemoteObj.getOperation(method);
			tmpOperation.addEventListener(ResultEvent.RESULT,resultHandler);
			tmpOperation.addEventListener(FaultEvent.FAULT,function errorhandler(event:FaultEvent):void
															{
																	trace("Echec de l'opération silencieuse : " + method); 
															}
			);
			tmpRemoteObj = null;
			return tmpOperation;
		}
			
		public static function callSilentService(absOp:AbstractOperation,...params):void
		{	
			absOp.send.apply(null,params);	
		}
		
		//================ FIN OPERATION SILENCIEUSE ===================================
		
		
		public static function invokeService(absOp:AbstractOperation,...params):void {			
			
			var tmpEvent:ConsoViewEvent = new ConsoViewEvent(ConsoViewEvent.INVOKE_REMOTE_OPERATION);
			RemoteObjectUtil.remotingCount ++;
			RemoteObjectUtil.singletonInstance.dispatchEvent(tmpEvent);
			absOp.send.apply(null,params);
			
			//=========== TRACKING =========================================
			if (absOp.name.toUpperCase() == "VALIDATELOGIN" 
				|| absOp.name.toUpperCase() == "CONNECTTO"
				|| absOp.name.toUpperCase() == "CONNECTSSO")
			{
				RemoteObjectUtil.loginString = params[2].login.toString();
			}
		}
		
		public static function get RemotingCount():Number{
			return RemoteObjectUtil.remotingCount;
		}
		
				
		private static function resultHandlerMethod(evt:ResultEvent):void {
			var targetOp:AbstractOperation = evt.target as AbstractOperation;
			
			if (targetOp.name.toUpperCase() == "CONNECTSSO"
				&& evt.result.hasOwnProperty("EMAIL"))
			{
				RemoteObjectUtil.loginString = evt.result.EMAIL;
			}
			
			if (((targetOp.name.toUpperCase()== "VALIDATELOGIN") 
				|| (targetOp.name.toUpperCase() == "CONNECTTO")
				|| (targetOp.name.toUpperCase() == "CONNECTSSO")) && evt.result.hasOwnProperty("user"))
			{
				if(evt.result.user.hasOwnProperty("EMAIL"))					
				{
					RemoteObjectUtil.loginString = evt.result.user.EMAIL;
				}
			}
			
			trace("(RemoteObjectUtil) RPC-RESULT " + (targetOp.service as RemoteObject).source + "." + targetOp.name + "()");			

			RemoteObjectUtil.remotingCount --;
			var tmpEvent:ConsoViewEvent = new ConsoViewEvent(ConsoViewEvent.RESULT_REMOTE_OPERATION);
						
			RemoteObjectUtil.singletonInstance.dispatchEvent(tmpEvent);			
			
			//Si le remoting etait annulable on le supprime du tableaux des remotings annulable
			var index : Number = RemoteObjectUtil.tabCancelableOperations.indexOf(targetOp);
			if(index != -1){
				RemoteObjectUtil.tabCancelableOperations.splice(index,1);
			}
		}

		private static function faultHandlerMethod(evt:FaultEvent):void {
			var targetOp:AbstractOperation = evt.target as AbstractOperation;
			trace("(RemoteObjectUtil) RPC-FAULT " + (targetOp.service as RemoteObject).source + "." + targetOp.name + "()");			
			trace(ObjectUtil.toString(evt.fault.faultString));
			
			var tmpEvent:ConsoViewEvent;
			if (evt.fault.faultString.toUpperCase().search("SESSION") != -1
			|| evt.fault.faultCode == "-99")
			{
				tmpEvent = new ConsoViewEvent(ConsoViewEvent.EXPIRED_SESSION);
			}
			else
			{
				tmpEvent = new ConsoViewEvent(ConsoViewEvent.FAULT_REMOTE_OPERATION);
				tmpEvent.stack = evt.fault.getStackTrace();
				tmpEvent.code = evt.fault.faultCode;
				tmpEvent.detail = evt.fault.faultDetail;
				tmpEvent.message = evt.fault.faultString;
				tmpEvent.date = new Date();
			}		
				
			RemoteObjectUtil.remotingCount --;
			RemoteObjectUtil.getSession().dispatchEvent(tmpEvent);			
			cancelRemoting();
			
			// TRACKING ==========================================================================
			var errorCode:int = parseInt(evt.fault.faultDetail,10);
			var remoteObjTracker:RemoteObject = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			remoteObjTracker.showBusyCursor = false;
			remoteObjTracker.source = "fr.consotel.util.consoview.tracking.SessionTracker";
			var trackingOp:AbstractOperation = remoteObjTracker.getOperation("remotingLog");
			
			trackingOp.addEventListener(ResultEvent.RESULT,RemoteObjectUtil.resultTracking);
			trackingOp.addEventListener(FaultEvent.FAULT,RemoteObjectUtil.faultTracking);
			
			var tmpRemoteObject:RemoteObject = (evt.target as AbstractOperation).service as RemoteObject;
		
			var t1:String = evt.fault.faultString;
			var t2:String = evt.fault.faultDetail;
			trackingOp.send.apply(null,[tmpEvent.type,errorCode,RemoteObjectUtil.loginString,tmpRemoteObject.source,(evt.target as AbstractOperation).name,t1,t2]);
			// ===============================================================================
		}
		
		
		// Pour compatibilité !!!!
		public static function getOperationFrom(destination:String, source:String, method:String,
									resultHandler:Function, faultHandler:Function = null,
									cursorBusy:Boolean = true):AbstractOperation {
			return RemoteObjectUtil.getOperation(source,method,resultHandler);
		}
		
		public static function getHandledOperationFrom(destination:String, source:String, method:String,
									resultHandler:Function, faultHandler:Function = null,
									cursorBusy:Boolean = true):AbstractOperation {
			return RemoteObjectUtil.getOperation(source,method,resultHandler);
		}
		
		public static function callService(absOp:AbstractOperation,...params):void {
			var tmpEvent:ConsoViewEvent = new ConsoViewEvent(ConsoViewEvent.CALL_REMOTE_OPERATION);
			RemoteObjectUtil.remotingCount ++;
			RemoteObjectUtil.singletonInstance.dispatchEvent(tmpEvent);
			absOp.send.apply(null,params);
			RemoteObjectUtil.tabCancelableOperations.unshift(absOp);
			
			if (absOp.name.toUpperCase() == "VALIDATELOGIN" 
				|| absOp.name.toUpperCase() == "CONNECTTO"
				|| absOp.name.toUpperCase() == "CONNECTSSO")
			{
				if(params[2].hasOwnProperty('login'))
				{
					RemoteObjectUtil.loginString = (params[2].login as String).toString().toLowerCase();	
				}
			}
		}
		
		public static function cancelRemoting():void {
			trace("(RemoteObjectUtil)->cancelRemoting() : Annulation des opérations de remoting en cours");
			trace("(RemoteObjectUtil)->cancelRemoting()   remotingCount " + RemoteObjectUtil.remotingCount);
			while (RemoteObjectUtil.tabCancelableOperations.length  > 0)
			{
				var currentOperation:AbstractOperation = RemoteObjectUtil.tabCancelableOperations.shift() as AbstractOperation;
				if(currentOperation != null)
				{												
					currentOperation.cancel();
					currentOperation.clearResult(false);
					RemoteObjectUtil.remotingCount --;
					trace("(RemoteObjectUtil)->cancelRemoting()   remotingCount -- = " + RemoteObjectUtil.remotingCount);
					trace("(RemoteObjectUtil)->cancelRemoting()   tabCancelableOperations.length -- = " + RemoteObjectUtil.tabCancelableOperations.length);
				}else
				{
					trace("(RemoteObjectUtil)->cancelRemoting()   currentOperation == null");
				}
			}
			RemoteObjectUtil.singletonInstance.dispatchEvent(new ConsoViewEvent(ConsoViewEvent.CANCEL_REMOTE_OPERATIONS));
		}
		
		
		
		
		//======== TRACKING ==================================================================
		private static function resultTracking(event:ResultEvent):void {
			
		}

		private static function faultTracking(evt:FaultEvent):void 
		{	
			trace(evt.toString(),"faultTracking");
		}
		//======== FIN  TRACKING ==================================================================
		
	}
}
