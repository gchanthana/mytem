<cfcomponent displayname="SendMail" hint="i send your mail to administor" output="false">

	<cffunction  name="SendMessageToAdmin" access="remote" hint="I send your mail" output="true">
		
		 <cfargument name="catch" type="any" required="true">
		 <cfargument name="uuid"  type="any"  required="false" hint="UUID compaign">
		 
		 <cfif isDefined("uuid")>
			<cfset uuid=#uuid#>
		 <cfelse>
			<cfset uuid="">
		 </cfif>
		 
		 <cfset todayDate = Now()> 
		 
		 <!---------------------------------------- envoyer un mail  ---------------------------------------------------->
		 <cfmail to="monitoring@saaswedo.com" 
		 	     from="api_sms_saaswedo@saaswedo.com" subject="Erreur API SMS" type="html" server="mail-cv.consotel.fr" port="25" >
						
			 <html>
				<head>
					<style>
					   ##tab, ##tab caption
						{
						    margin: auto;
						}
						
						##tab
						{
						    border: ##DDEEFF 2px solid;
						    border-collapse: separate;
						    border-spacing: 2px;
						    empty-cells: hide;
						    width:100%;
						}
						
						caption
						{
						    background-color: ##DDEEFF;
							font-size: 1.2em;
						}
						
						##tab th
						{
						    color: ##996600;
						    background-color: ##FFCC66;
						    border: ##FFCC66 1px solid;
						    font-variant: small-caps;
						    font-size: 1em;
						    letter-spacing: 1px;
						}
						
						##tab ##one_th
						{
							width:20%;
						}
						##tab ##two_th
						{
							width:30%;
						}
						##tab td
						{
						    border: ##DDEEFF 1px solid;
						    padding-left: 10px;
						}
						
						
						.foot
						{
						    font-size: 0.9em;
						    background-color: ##FFCC66;
						    color: ##996600;
						    letter-spacing: 1px;
						}															
					 </style>
				</head>
				<body>
					 <table id="tab">
					    <caption>Description de l'erreur lors de l'envoi de SMS </caption>
					    			  			   
					    <thead>
					        <tr>
					            <th id="one_th">Libelle</th>
					            <th id="two_th">Description</th>
					        </tr>
					    </thead>
					    
					    <tbody>
					        <tr>
					            <td>Détails</td>
					            <td>#catch.detail#</td>
					        </tr>
					        <tr>
					            <td>Code de l'erreur</td>
					            <td>#catch.ErrorCode#</td>
					        </tr>
					         <tr>
					            <td>Identifiant campagne UUID</td>
					            <td>#uuid#</td>
					        </tr>
					        <tr>
					            <td>Plus d'info</td>
					            <td>#catch.ExtendedInfo#</td>
					        </tr>
					    </tbody>
					    
					    	<tr class="foot">
					            <td rowspan="2">Créé par</td>
					            <td>API SMS- Saas We Do</td>
					        </tr>
					        <tr class="foot">
					            <td>Date :#DateFormat(todayDate, "dd/mm/yyyy")# A #TimeFormat(now(), "HH:mm:ss") #  </td>
					        </tr>
			
					</table>
				</body>
			</html>		
		 </cfmail>
		 
		 <!---------------------------------------- pour l'affichage à l' écran  ---------------------------------------------------->
		 <html>
			<head>
				 <link href="/fr/consotel/api/sms/css/style.css" rel="stylesheet" type="text/css" media="screen" />
			</head>
			<body>
				<table id="tab">
				    <caption>Description de l'erreur lors de l'envoi de SMS </caption>
				    			  			   
				    <thead>
				        <tr>
				            <th id="one_th">Libelle</th>
				            <th id="two_th">Description</th>
				        </tr>
				    </thead>
				    
				    <tfoot>
				    	<tr>
				            <td rowspan="2">Créé par</td>
				            <td>API SMS- Saas We Do</td>
				        </tr>
				        <tr>
				            <td>Date :#DateFormat(todayDate, "dd/mm/yyyy")# A #TimeFormat(now(), "HH:mm:ss") #  </td>
				        </tr>
				    </tfoot>
				
				    <tbody>
				        <tr>
				            <td>Détails</td>
				            <td>#catch.detail#</td>
				        </tr>
				        <tr>
				            <td>Code de l'erreur</td>
				            <td>#catch.ErrorCode#</td>
				        </tr>
				        <tr>
					        <td>Plus d'info</td>
					        <td>#catch.ExtendedInfo#</td>
					    </tr>
				        <tr>
				            <td>Identifiant campagne UUID</td>
				            <td>#uuid#</td>
				        </tr>
				    </tbody>
				</table>
			</body>
		</html>
		
	</cffunction>

</cfcomponent>