package composants.util
{
	import mx.controls.Alert;
	
	/*
		Cette classe permet de supprimer les enfants d'un noeud passé en paramêtre
	*/
	public class NodeRefresh
	{
		private var node : XML;
		public function NodeRefresh(node :XML)
		{
			this.node = node;
			if(node!=null){
				deleteChildOfNode();
			}	
		}
		private function deleteChildOfNode():void
		{
			
			var children : XMLList= node.children();


			while(children.length()>0)
			{	
				
				delete children[0];
				
				
			}	
			
			
			
			}
		}
		
}