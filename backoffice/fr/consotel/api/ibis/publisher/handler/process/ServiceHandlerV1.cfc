<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.process.ServiceHandlerV1"
			extends="fr.consotel.api.ibis.publisher.handler.ServiceHandler" hint="Exemple de handler de service">

	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
			
			<cfset SUPER.run(ARGUMENTS.eventObject)>
			
			<!--- CREATION DES COMPONENT --->
			<cfset cmdeventobj  = createObject('component',"fr.consotel.api.ibis.publisher.handler.event.CommandeEvent")>
			<cfset logObject    = createObject('component',"fr.consotel.consoview.M16.LogsCommande")>
			<cfset attachObject = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>
			<cfset mailObject	= createObject('component',"fr.consotel.consoview.M16.v2.SendMail")>
			
			<cfset uuidlog		= #ARGUMENTS.eventObject.getEventTarget()#>
			<cfset ImportTmp	= "/container/M16/">
			<cfset contentMail 	= structnew()>
			
			<!--- VERIFICATION DE L'ETAT DU RAPPORT --->
			<cfset rsltRapportObj 	= cmdeventobj.comandeEventInit(ARGUMENTS.eventObject)>
			
			<!--- RECUPERATION MAIL INSERE CONTENANT TOUS LES INFOS --->
			<cfset rsltLogObj 		= logObject.getlogsCommandeuuid(#uuidlog#)>
	
			<!--- RECUPERATION DES PEICES JOINTES A JOINDRE AU MAIL OU NON --->
			<cfset rsltAttachOb 	= attachObject.fournirAttachements(#rsltLogObj.IDCOMMANDE#)>

			<cfloop query="rsltLogObj">
				<cfset contentMail.BCC				= rsltLogObj.BCC>
				<cfset contentMail.CC				= rsltLogObj.CC>
				<cfset contentMail.CODE_TRAITEMENT	= rsltLogObj.CODE_TRAITEMENT>
				<cfset contentMail.DATE_LOG			= rsltLogObj.DATE_LOG>
				<cfset contentMail.GESTIONNAIRE		= rsltLogObj.GESTIONNAIRE>
				<cfset contentMail.IDCOMMANDE		= rsltLogObj.IDCOMMANDE>
				<cfset contentMail.IDCV_LOG_MAIL	= rsltLogObj.IDCV_LOG_MAIL>
				<cfset contentMail.IDGESTIONNAIRE	= rsltLogObj.IDGESTIONNAIRE>
				<cfset contentMail.IDRACINE			= rsltLogObj.IDRACINE>
				<cfset contentMail.JOB_ID			= rsltLogObj.JOB_ID>
				<cfset contentMail.MAIL				= rsltLogObj.MAIL>
				<cfset contentMail.MAIL_FROM		= rsltLogObj.MAIL_FROM>
				<cfset contentMail.MAIL_TO			= rsltLogObj.MAIL_TO>
				<cfset contentMail.MESSAGE			= rsltLogObj.MESSAGE>
				<cfset contentMail.MODULE			= rsltLogObj.MODULE>
				<cfset contentMail.RACINE			= rsltLogObj.RACINE>
				<cfset contentMail.STATUS			= rsltLogObj.STATUS>
				<cfset contentMail.SUBJECT			= rsltLogObj.SUBJECT>
				<cfset contentMail.UUID				= rsltLogObj.UUID>
			</cfloop>

 			<cfswitch expression="#cmdeventobj.getReportStatus()#"> 
				<cfcase value="S"> 
					<cfset mailObject.sendMailDBC(eventObject, contentMail, rsltAttachOb)>
				</cfcase> 
				<cfcase value="F"> 
					<cfset mailObject.sendMailDBCError(eventObject, contentMail, rsltAttachOb)>
				</cfcase>
				<cfcase value="R"> 
					<cfset mailObject.sendMailRespositoryError(eventObject, contentMail, rsltAttachOb)>
				</cfcase> 
				<cfdefaultcase> 
					<cfset mailObject.myMailFonction(eventObject, contentMail, rsltAttachOb)>
				</cfdefaultcase> 
			</cfswitch>
			
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
		
			<cfmail from="bdcmobile@consotel.fr" to="nicolas.renel@consotel.fr" subject="SERVICE ERROR HANDLER V1" type="html" server="mail-cv.consotel.fr" >
				<cfoutput>
					JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br />
					REPORT_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br />
					REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br />
					BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br />
					EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br />
					EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br />
					EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					EVENT_HANDLER : #ARGUMENTS.eventHandler#
				</cfoutput>
				<cfdump var="#ARGUMENTS.exceptionObject#" label="EXCEPTION">
			</cfmail>
		
	</cffunction>
	
</cfcomponent>
