package ihm.popup.interlocuteur
{
	import composants.ui.TitleWindowBounds;
	import composants.util.CvDateChooser;
	
	import entity.CiviliteVO;
	import entity.CodeAction;
	import entity.InterlocuteurVO;
	import entity.LangueVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.EmailValidator;
	import mx.validators.StringValidator;
	import mx.validators.Validator;
	
	import service.FctGestionPartenaireService;
	
	import utils.Constantes;
	
	public class PopUpEditionInterlocuteurImpl extends TitleWindowBounds
	{
		private var _iServiceGestPartenaire	:FctGestionPartenaireService;
		private var _idSelectedPartn:int;
		private var _selectedInterloc:InterlocuteurVO;
		
		public var combx_civilite	:ComboBox;
		[Bindable]public var ti_nom	:TextInput;
		public var ti_prenom		:TextInput;
		public var ti_qualite		:TextInput;
		[Bindable]public var ti_email:TextInput;
		public var cbx_principal	:CheckBox;
		public var combo_langue		:ComboBox;
		public var df_dateDebut		:CvDateChooser;
		public var df_dateFin		:CvDateChooser;
		public var ti_appLoginId	:TextInput; 
		public var sv_nom			:StringValidator;
		public var ev_mail			:EmailValidator;
		
		public function PopUpEditionInterlocuteurImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		private function init(event:FlexEvent):void
		{
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValider_clickHandler(evt:MouseEvent):void
		{
			if(validateEnteredInterlocuteur() == true){
				var modifiedInterloc:InterlocuteurVO = new InterlocuteurVO();
				modifiedInterloc.id 		= selectedInterloc.id;
				modifiedInterloc.civilite 	= (combx_civilite.selectedItem != null)?(combx_civilite.selectedItem as CiviliteVO).libelleCivilite:'';
				modifiedInterloc.nom 		= ti_nom.text;
				modifiedInterloc.prenom		= ti_prenom.text;
				modifiedInterloc.qualite	= ti_qualite.text;
				modifiedInterloc.email		= ti_email.text;
				modifiedInterloc.principal	= (cbx_principal.selected == true)?1:0;
				modifiedInterloc.langue 	= (combo_langue.selectedItem != null)?(combo_langue.selectedItem as LangueVO).CODELANGUE:'fr_FR';
				modifiedInterloc.dateDebut	= DateField.stringToDate(df_dateDebut.text, ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_'));
				modifiedInterloc.dateFin	= DateField.stringToDate(df_dateFin.text, ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_'));
				
				this.iServiceGestPartenaire.ajouterEditerInterloc(CvAccessManager.CURRENT_FUNCTION, CodeAction.CREATE_UPD_INTERLOC, idSelectedPartn, modifiedInterloc);				
				this.close_clickHandler(evt);	
			}
		}
		
		private function validateEnteredInterlocuteur():Boolean
		{
			var result:Boolean = false;
			var validationResult:Array = Validator.validateAll([sv_nom, ev_mail]);
			
			if (validationResult.length == 0)
			{
				result = true
			}
			else
			{
				result = false
			}
			
			return result;
		}
		
		protected function dateField_labelFormat(item:Date):String
		{
			var dateFormatter:DateFormatter = new DateFormatter();
			dateFormatter.formatString = ResourceManager.getInstance().getString('CG06', '_DD_MM_YYYY_');
			return dateFormatter.format(item);
			
		}
		
		protected function comboLangueFunc(myInterloc:InterlocuteurVO):int
		{
			var indexLangue:int = -1;
			for(var i:int = 0; i < Constantes.listeLangues.length; i++)
			{
				if(myInterloc.langue == (Constantes.listeLangues.source[i] as LangueVO).CODELANGUE)
				{
					indexLangue = i;
					break;
				}
			}
			
			return indexLangue;
		}
		
		protected function comboCiviliteFunc(myInterloc:InterlocuteurVO):int
		{
			var indexCivilite:int = -1;
			for(var i:int = 0; i < Constantes.listeCivilites.length; i++)
			{
				if(myInterloc.civilite == (Constantes.listeCivilites[i] as CiviliteVO).libelleCivilite)
				{
					indexCivilite = i;
					break;
				}
			}
			
			return indexCivilite;
		}
		
		public function get iServiceGestPartenaire():FctGestionPartenaireService
		{
			return _iServiceGestPartenaire;
		}
		
		public function set iServiceGestPartenaire(value:FctGestionPartenaireService):void
		{
			_iServiceGestPartenaire = value;
		}
		
		public function get idSelectedPartn():int { return _idSelectedPartn; }
		
		public function set idSelectedPartn(value:int):void
		{
			if (_idSelectedPartn == value)
				return;
			_idSelectedPartn = value;
		}
		
		[Bindable]
		public function get selectedInterloc():InterlocuteurVO { return _selectedInterloc; }
		
		public function set selectedInterloc(value:InterlocuteurVO):void
		{
			if (_selectedInterloc == value)
				return;
			_selectedInterloc = value;
		}
		
	}
}