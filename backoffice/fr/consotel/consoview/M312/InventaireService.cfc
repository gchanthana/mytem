﻿<cfcomponent displayname="fr.consotel.consoview.M312.InventaireService">

	<cffunction name="getMarqueEquipement" output="false" access="remote" returntype="Struct" hint="Donne le nombre d'équipements par marque et la date du dernier inventaire complet connu">
		<cfargument name="_typeperimetre" required="true" type="string">		
		
		<cfset obj=createobject("component","fr.consotel.consoview.M312.PerimetreE0")>
		<cfset obj.init(_typeperimetre)>
			 	 
		<cfset _idperiode_mois = SESSION.DATES.LASTPFGPIDPERIODE>
		<cfset _periode_mois = 	SESSION.DATES.LASTPFGPLOAD>			 
		<cfset _idracineMaster = SESSION.PERIMETRE.IDRACINE_MASTER>
		<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset _idPerimetre = SESSION.PERIMETRE.SELECTED_IDPERIMETRE>
		<cfset _idorga = SESSION.PERIMETRE.SELECTED_ID_ORGA>
		<cfset _niveau = SESSION.PERIMETRE.SELECTED_NIVEAU>
		 
		<cfstoredproc datasource="#SESSION.OFFREDSN#" blockfactor="1" procedure="pkg_m00.get_last_periode_parc">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_racine" value="#_idracine#">
			<cfprocresult name="p_date_complete_inventaire">
		</cfstoredproc>
		
		<cfif p_date_complete_inventaire.recordCount gt 0>
			<cfset _idperiode_mois = p_date_complete_inventaire["LAST_IDPERIODE_MOIS"][1]>	
			<cfset _periode_mois = p_date_complete_inventaire["LAST_MOIS"][1]>
			
			<cfelse>
				<cfset _idperiode_mois = SESSION.DATES.LASTPFGPIDPERIODE>
				<cfset _periode_mois = SESSION.DATES.LASTPFGPLOAD>			
		</cfif>
		
		<cfquery name="qListeMarque" datasource="e0">		
			select count(distinct pp.IDSOUS_TETE) as QTE,
	               pp.MARQUE_TEL as MARQUE
			from  offre.pfgp_parc pp, test2_orgaclient o
			where o.idracine = #_idracine#
			and o.b = #_idorga#
			and  o.#_niveau# = #_idPerimetre#
			and pp.idracine = #_idracine# 
			and pp.idperiode_mois = #_idperiode_mois# 
			and o.idperiode_mois = #_idperiode_mois# 
			and o.idsous_tete = pp.idsous_tete
			group by pp.marque_tel
		</cfquery>	
		
		<cfset structResult = structnew()>
		<cfset structResult.LISTE_MARQUE = qListeMarque>
		<cfset structResult.DATE_COMPLETE_INVENTAIRE = _periode_mois>
		<cfset structResult.IDDATE_COMPLETE_INVENTAIRE = _idperiode_mois>
		
		<cfreturn structResult>
	</cffunction>
	
	<cffunction name="getModeleEquepement" output="true" access="remote" returntype="Query" hint="Donne le nombre d'équipements d'une marque par modèle">
		<cfargument name="_marque" required="true" type="string">
		<cfargument name="_typeperimetre" required="true" type="string">
		<cfargument name="_idperiode_mois" required="true" type="numeric">
		
		<cfset obj=createobject("component","fr.consotel.consoview.M312.PerimetreE0")>		
		<cfset obj.init(_typeperimetre)>
		
		
				 
			 
			 
		<cfset _idracineMaster = SESSION.PERIMETRE.IDRACINE_MASTER>
		<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset _idPerimetre = SESSION.PERIMETRE.SELECTED_IDPERIMETRE>
		<cfset _idorga = SESSION.PERIMETRE.SELECTED_ID_ORGA>
		<cfset _niveau = SESSION.PERIMETRE.SELECTED_NIVEAU>
		
		<cfquery name="qListeMarqueModele" datasource="BI_TEST">
			SET VARIABLE 	p_idracine_master = #_idracineMaster#, 
							p_idracine = #_idracine#, 
							p_idorga = #_idorga#, 
							p_idperimetre = #_idPerimetre#, 
							p_niveau = '#_niveau#', 
							p_langue_pays = 'fr_FR';
							
			SELECT 			
			PFGP_PARC.MODELE_TEL MODELE,
			COUNT(DISTINCT ORGA1CLIENT_HIST.SOUS_TETE) QTE
			FROM E0_ENPROD_HIST_RH
			WHERE PERIODE.IDPERIODE_MOIS = #_idperiode_mois#
			AND PFGP_PARC.MARQUE_TEL = '#_marque#'
			ORDER BY MODELE
		</cfquery>			 		
					
		<cfreturn qListeMarqueModele>
	</cffunction>
	
	<cffunction name="getListEquepement" output="true" access="remote" returntype="Query" hint="Liste les equipements du parc du modèle spécifié ,donne les infos du collaborateur s'il y en a un">		
		<cfargument name="_marque" required="true" type="string">
		<cfargument name="_modele" required="true" type="string">
		<cfargument name="_typeperimetre" required="true" type="string">
		<cfargument name="_idperiode_mois" required="true" type="numeric">
		
		<cfset obj=createobject("component","fr.consotel.consoview.M312.PerimetreE0")>
		<cfset obj.init(_typeperimetre)>
				 
				 
						 
		<cfset _idracineMaster = SESSION.PERIMETRE.IDRACINE_MASTER>
		<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset _idPerimetre = SESSION.PERIMETRE.SELECTED_IDPERIMETRE>
		<cfset _idorga = SESSION.PERIMETRE.SELECTED_ID_ORGA>
		<cfset _niveau = SESSION.PERIMETRE.SELECTED_NIVEAU>
		
		
		<cfquery name="qListe" datasource="BI_TEST">
			SET VARIABLE 	p_idracine_master = #_idracineMaster#, 
							p_idracine = #_idracine#, 
							p_idorga = #_idorga#, 
							p_idperimetre = #_idPerimetre#, 
							p_niveau = '#_niveau#', 
							p_langue_pays = 'fr_FR';
							
			SELECT ORGA1CLIENT_HIST.SOUS_TETE SOUS_TETE,			
			ORGAOPERATEUR.NOM NOM, 
		 
			
			ORGA1CLIENT_HIST.CHEMIN CHEMIN 
			FROM E0_ENPROD_HIST_RH
			WHERE PERIODE.IDPERIODE_MOIS = #_idperiode_mois#
			AND PFGP_PARC.MARQUE_TEL = '#_marque#'
			AND PFGP_PARC.MODELE_TEL = '#_modele#'
			ORDER BY NOM,CHEMIN,SOUS_TETE
		</cfquery>
		
		
		<cfreturn qListe>
	</cffunction>	
	


	
	
</cfcomponent>