package composants.tb.ligne
{
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class FicheLigneImpl extends TitleWindow
	{
		
		private var _ligne : Number;
		private var _facture : Number;
		private var _numero : String;
		
		public function FicheLigneImpl()
		{
		}
		
		public function get numero():String
		{
			return _numero;
		}

		public function set numero(value:String):void
		{
			_numero = value;
		}

		public function get facture():Number
		{
			return _facture;
		}

		public function set facture(value:Number):void
		{
			_facture = value;
		}

		public function get ligne():Number{
			return _ligne;
		}
		public function set ligne(idst:Number):void{
			_ligne = idst;
		}
		
		public function fermer(ev:Event):void{
			PopUpManager.removePopUp(this);
		}
		
	}
}