package composants.util
{
	import mx.formatters.DateFormatter;
	
	
	public class DateFunction {
		/**
		 * Tableau contenant les libéllés (symboles de 1 lettre) des jours de la semaine
		 * */
		public static const DAY_NAMES:Array =
			["D","L","M","M","J","V","S"];
		/**
		 * Tableau contenant les noms des mois de l'année
		 * */
		public static const MONTH_NAMES:Array =
			["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Décembre"];
		/**
		 * Date du jour (De type Date).
		 * */
		public static const NOW_DATE:Date = new Date();
	
   /**
	* Retourne le tableau  des 12 dernier mois 
	* 
	**/ 
	public static function getPeriode():Array{
		var df : DateFormatter = new DateFormatter();    	
		var tabPeriode : Array = new Array();
		var invoiceDate:Date = new Date();					
		var endDate:Date = new Date(invoiceDate.getTime() - (30 * millisecondsPerDay));		
	  	var startDate:Date = new Date(invoiceDate.getTime() - (365 * millisecondsPerDay));
	  	
	  	var j :int = 0;
	  	
	  	df.formatString = "DD/MM/YYYY";
	  	for (var i:int = 30 ; i < 365; i=i+30){		
	  		var newDate:Date = new Date(invoiceDate.getTime() - (i * millisecondsPerDay));	

			newDate.setDate(1);				
			tabPeriode.unshift(df.format(newDate.toDateString()));
	  	}
		
		return tabPeriode;  		             
	}
	
	 /**
	 * 
	 * Retourne le mois en lettre ex 0 -> Janvier
	 * @param mois : Number Le nombre représentant le mois 
	 * 
	 **/
	 public static function moisEnLettre(a:Number):String {
    	 var v:Array=new Array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
 	    return v[a];
    } 
	 
	 /**
	 dateDiff(datePart:String, date1:Date, date2:Date):Number<BR>
	 returns the difference between 2 dates<BR>
	 valid dateParts:<BR>
	 s: Seconds<BR>
	 n: Minutes<BR>
	 h: Hours<BR>
	 d: Days<BR>
	 m: Months<BR>
	 y: Years<BR>
	 */
	 public static function dateDiff(datePart:String, date1:Date, date2:Date):Number{
	 	return getDatePartHashMap()[datePart.toLowerCase()](date1,date2);
	 }
	 
	 
	/** dateAdd(datePart:String,date:Date,num:Number):Date<BR>
	 returns a new date object with the appropriate date/time settings<BR>
	*/
	public static function dateAdd(datePart:String,date:Date,num:Number):Date{
		// get date part object;
		var dpo : Object = getDateAddPartHashMap()[datePart.toLowerCase()];
		// create new date as a copy of date passed in
		var newDate : Date = new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes(),date.getSeconds(),date.getMilliseconds());
		// set the appropriate date part of the new date
		newDate[dpo.set](date[dpo.get]()+num);
		// return the new date
		return newDate;
	}

	public static function formatDateAsString(d:Date):String{
	 	if(d != null)
		{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString();
			var v:String=jour+"/"+mois+"/"+annee;
			return v;
		}
		else
		{
			return "";
		}
	}
	
	public static function formatDateAsShortString(d:Date):String{
		if(d != null)
		{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString().substr(2,2);
			var v:String=jour+"/"+mois+"/"+annee;
			return v;
		}
		else
		{
			return "";
		}
	}
		
	public static function formatDateAsInverseString(d:Date):String{
		if(d != null)
		{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString();
			var v:String=annee+"/"+mois+"/"+jour;
			return v;	
		}
		else
		{
			return "";
		}
	}
	
	
	public static function getDateFromInverseDateString(value:String,delimiter:String):Date
	{
		if(value){
			var stringDate:String = value;
			var tabDate:Array = stringDate.split(delimiter);
			switch(tabDate.length)
			{
								
				case 3 :
				{
					var annee:Number = Number(tabDate[0]);
					var mois:Number = Number(tabDate[1])-1;
					var jour:Number = Number(tabDate[2]);
					return new Date(annee,mois,jour);
				}
				case 2 : 
				{
					var annee2:Number = Number(tabDate[0]);
					var mois2:Number = Number(tabDate[1])-1;
					return new Date(annee2,mois2);	
				}
				case 1 :
				{
					var annee3:Number = Number(tabDate[0]);
					return new Date(annee3);
				}
				
				default: return null				
					
			}	
		}
		else
		{
			return null
		}
		
	}
	
	public static function formatODBCDATEToFrenchLiteralDate(d:Date):String{
		var jour:String=d.getDate().toString();		
		var mois:String=(d.getMonth()).toString();
		var annee:String=d.getFullYear().toString();
		var v:String=jour+" "+
			DateFunction.moisEnLettre(parseInt(mois))+" "+annee;
		
		return  v;
	}
	 
	/*------------------------------------------------ PRIVATE --------------------------------------------------*/ 
	 
	public static const millisecondsPerMinute:int = 1000 * 60;
	public static const millisecondsPerHour:int = 1000 * 60 * 60;
	public static const millisecondsPerDay:int = 1000 * 60 * 60 * 24;
	 
	 
	private static function getDateAddPartHashMap():Object{
		var dpHashMap : Object = new Object();
		dpHashMap["s"] = new Object();
		dpHashMap["s"].get = "getSeconds";
		dpHashMap["s"].set = "setSeconds";
		dpHashMap["n"] = new Object();
		dpHashMap["n"].get = "getMinutes";
		dpHashMap["n"].set = "setMinutes";
		dpHashMap["h"] = new Object();
		dpHashMap["h"].get = "getHours";
		dpHashMap["h"].set = "setHours";
		dpHashMap["d"] = new Object();
		dpHashMap["d"].get = "getDate";
		dpHashMap["d"].set = "setDate";
		dpHashMap["m"] = new Object();
		dpHashMap["m"].get = "getMonth";
		dpHashMap["m"].set = "setMonth";
		dpHashMap["y"] = new Object();
	  	dpHashMap["y"].get = "getFullYear";
	  	dpHashMap["y"].set = "setFullYear";
		return dpHashMap;
 	}

	 private static function getDatePartHashMap():Object{
	  var dpHashMap:Object = new Object();
	  dpHashMap["s"] = getSeconds;
	  dpHashMap["n"] = getMinutes;
	  dpHashMap["h"] = getHours;
	  dpHashMap["d"] = getDays;
	  dpHashMap["m"] = getMonths;
	  dpHashMap["y"] = getYears;
	  return dpHashMap;
	 }
	 
	 private static function compareDates(date1:Date,date2:Date):Number{
	  return date1.getTime() - date2.getTime();
	 }
	 
	 private static function getSeconds(date1:Date,date2:Date):Number{
	  return Math.floor(compareDates(date1,date2)/1000);
	 }
	 
	 private static function getMinutes(date1:Date,date2:Date):Number{
	  return Math.floor(getSeconds(date1,date2)/60);
	 }
	 
	 private static function getHours(date1:Date,date2:Date):Number{
	  return Math.floor(getMinutes(date1,date2)/60);
	 }
	 
	 private static function getDays(date1:Date,date2:Date):Number{
	  return Math.floor(getHours(date1,date2)/24);   
	 }   
	 
	 private static function getMonths(date1:Date,date2:Date):Number{
	  var yearDiff : int = getYears(date1,date2);
	  var monthDiff : int = date1.getMonth() - date2.getMonth();
	  if(monthDiff < 0){
	   monthDiff += 12;
	  }
	  if(date1.getDate()< date2.getDate()){
	   monthDiff -=1;
	  }
	  return 12 *yearDiff + monthDiff;
	 }
	 
	 private static function getYears(date1:Date,date2:Date):Number{
	  return Math.floor(getDays(date1,date2)/365);
	 }
	}
}

