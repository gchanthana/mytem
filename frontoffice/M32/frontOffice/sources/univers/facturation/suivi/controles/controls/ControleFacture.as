package univers.facturation.suivi.controles.controls
{
    import composants.util.ConsoviewAlert;
    import composants.util.ConsoviewFormatter;
    import composants.util.ConsoviewUtil;
    import composants.util.DateFunction;
    
    import flash.events.Event;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.collections.IViewCursor;
    import mx.controls.Alert;
    import mx.events.CollectionEvent;
    import mx.resources.ResourceManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    
    import univers.facturation.suivi.controles.vo.GroupeProduitControle;
    import univers.facturation.suivi.controles.vo.LigneFacturation;
    import univers.facturation.suivi.controles.vo.Operateur;
    import univers.facturation.suivi.controles.vo.OrganisationVo;
    import univers.facturation.suivi.controles.vo.Ressource;
    import univers.facturation.suivi.factures.event.FactureEvent;

    [Bindable]
    public class ControleFacture extends AbstractControleFacture
    {
        public function ControleFacture()
        {
            //TODO: implement function
            super();
            listeLignesFacturationsAbos.addEventListener(CollectionEvent.COLLECTION_CHANGE, totauxChangeHandler);
            listeLignesFacturationsConsos.addEventListener(CollectionEvent.COLLECTION_CHANGE, totauxChangeHandler);
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/ /**
         *
         * Fournit la liste des opérateurs du Groupe ou  groupe de ligne
         */
        override public function getListeOperateurs():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.OperateurGateWay", "getListeOperateursPerimetre", getListeOperateursResultHandler);
            RemoteObjectUtil.callService(op);
        }

        private function getListeOperateursResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                var ops:Operateur = new Operateur();
                ops.nom = ResourceManager.getInstance().getString('M32', 'Tous_les_op_rateurs');
                ops.id = 0;
                var arr:Array = re.result as Array;
                arr.unshift(ops);
                _listeOperateurs.source = arr;
                _listeOperateurs.refresh();
            }
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/ /**
         * Fournit la liste des organisations
         * */
        override public function getListeOrganisations():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "getListeOrganistaionsAsQuery", getListeOrganisationsResultHandler);
            RemoteObjectUtil.callService(op);
        }

        private function getListeOrganisationsResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                var col:ArrayCollection = re.result as ArrayCollection;
                var len:Number = col.length;
                listeOrganistaion = new ArrayCollection();
                for (var i:Number = 0; i < len; i++)
                {
                    var orga:OrganisationVo = new OrganisationVo();
                    orga.IDGROUPE_CLIENT = col[i].IDGROUPE_CLIENT;
                    orga.LIBELLE_GROUPE_CLIENT = col[i].LIBELLE_GROUPE_CLIENT;
                    orga.COMMENTAIRES = col[i].COMMENTAIRES;
                    orga.IDORGA_NIVEAU = col[i].IDORGA_NIVEAU;
                    orga.TYPE_ORGA = col[i].TYPE_ORGA;
                    orga.OPERATEURID = col[i].OPERATEURID;
                    listeOrganistaion.addItem(orga);
                }
                listeOrganistaion.refresh();
            }
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        override public function setParamsRecherches(etatControle:Number, etatInventaire:Number, etatExporte:Number, etatVise:Number, etatViseAna:Number, operateurId:Number, chaine:String, dateDebut:Date, dateFin:Date):void
        {
            _paramsRecherche.operateurId = operateurId;
            _paramsRecherche.etatControle = etatControle;
			_paramsRecherche.etatInventaire = etatInventaire;
            _paramsRecherche.etatExporte = etatExporte;
            _paramsRecherche.etatVise = etatVise;
            _paramsRecherche.etatViseAna = etatViseAna;
            _paramsRecherche.chaine = chaine;
            _paramsRecherche.dateDebut = dateDebut;
            _paramsRecherche.dateFin = dateFin;
            getListeFactures();
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        override protected function getListeFactures():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "getListeFacturesPerimetre", getListeFacturesResultHandler);
            RemoteObjectUtil.callService(op, _paramsRecherche);
        }

		private function getListeFacturesResultHandler(re:ResultEvent):void
		{
			//Alert.show(ObjectUtil.toString(re.token.message.body[0]));
			//var arrayC:ArrayCollection = new ArrayCollection();
			if (listeFactures)
			{
				listeFactures.removeAll();
			}
			
			//listeFactures = new ArrayCollection();
			//var object:Object = new Object();
			if (re.result)
			{
				var values:ArrayCollection = new ArrayCollection(re.result as Array);
				var cursor:IViewCursor = values.createCursor();
				while(!cursor.afterLast)
				{
					listeFactures.addItem(cursor.current);
					cursor.moveNext();
				}
				
				
				if (listeFactures.length == 0)
				{   
					Alert.show(ResourceManager.getInstance().getString('M32', 'Pas_de_donn_e'));
					dispatchEvent(new Event("FACTURE_LISTED"));
				}
			}
			else
			{                
				Alert.show(ResourceManager.getInstance().getString('M32', 'Pas_de_donn_e'));
			}
		}
		/*------------------------------------------------------------------------------------------------------------------------------------------------*/
		override public function calculMontantFacture(tab:Array):void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "calculMontantFacture", getListeFacturesResultHandler);
			RemoteObjectUtil.callService(opData, tab);
		}
	
		/*------------------------------------------------------------------------------------------------------------------------------------------------*/
		

        private function calculPurcent(object:Object):Number
        {
            var montant:Number = object.montant;
            var montantControle:Number = object.montantCalculeFacture;
            var ratio:Number = (montant != 0) ? (montantControle / montant) * 100 : 100;
            return ratio;
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        override public function updateVisaFacture():void
        {
            var callBackMethode:Function;
            if (_selectedFacture.visee == 1)
            {
                _selectedFacture.visee = 0;
                callBackMethode = annulerVisaFactureResultHandler;
            }
            else
            {
                _selectedFacture.visee = 1;
                callBackMethode = viserFactureResultHandler;
            }
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "updateEtatVise", callBackMethode);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        private function viserFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.visee = 1;
            }
            else
            {
                _selectedFacture.visee = 0;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }

        private function annulerVisaFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.visee = 0;
            }
            else
            {
                _selectedFacture.visee = 1;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        override public function updateVisaAnaFacture():void
        {
            var callBackMethode:Function;
            if (_selectedFacture.viseeAna == 1)
            {
                _selectedFacture.viseeAna = 0;
                callBackMethode = annulerVisaAnaFactureResultHandler;
            }
            else
            {
                _selectedFacture.viseeAna = 1;
                callBackMethode = viserAnaFactureResultHandler;
            }
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "updateEtatViseAna", callBackMethode);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        override public function updateVisaAnaFactureEditer():void
        {
            var callBackMethode:Function;
            if (_selectedFacture.viseeAna == 1)
            {
                _selectedFacture.viseeAna = 1;
                callBackMethode = editerAnaFactureResultHandler;
            }
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "updateEtatViseAna", callBackMethode);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        private function editerAnaFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.viseeAna = 1;
            }
            else
            {
                _selectedFacture.viseeAna = 1;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }

        private function viserAnaFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.viseeAna = 1;
            }
            else
            {
                _selectedFacture.viseeAna = 0;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
			
			dispatchEvent(new FactureEvent(FactureEvent.LISTED_FACTURE_CHANGE));
        }

        private function annulerVisaAnaFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.viseeAna = 0;
            }
            else
            {
                _selectedFacture.viseeAna = 1;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
			
			dispatchEvent(new FactureEvent(FactureEvent.LISTED_FACTURE_CHANGE));
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------*/
        override public function updateVisaControleFacture():void
        {
            var callBackMethode:Function;
            if (_selectedFacture.controlee == 1)
            {
                _selectedFacture.controlee = 0;
                callBackMethode = annulerVisaControleFactureResultHandler;
            }
            else
            {
                _selectedFacture.controlee = 1;
                callBackMethode = viserFactureControleResultHandler;
            }
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "updateEtatControle", callBackMethode);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }
		
		//
		override public function updateVisaControleFactureInventaire():void
		{
			var callBackMethode:Function;
			if (_selectedFacture.controleeInv == 1)
			{
				_selectedFacture.controleeInv = 0;
				callBackMethode = annulerVisaControleInventaireFactureResultHandler;
			}
			else
			{
				_selectedFacture.controleeInv = 1;
				callBackMethode = viserFactureControleInventaireResultHandler;
			}
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "updateEtatControleInventaire", callBackMethode);
			RemoteObjectUtil.callService(opData, _selectedFacture);
		}

        private function viserFactureControleResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.controlee = 1;
            }
            else
            {
                _selectedFacture.controlee = 0;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }

        private function annulerVisaControleFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.controlee = 0;
            }
            else
            {
                _selectedFacture.controlee = 1;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }
		
		
		private function viserFactureControleInventaireResultHandler(re:ResultEvent):void
		{
			if (re.result > 0)
			{
				_selectedFacture.controleeInv = 1;
			}
			else
			{
				_selectedFacture.controleeInv = 0;
				Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
			}
		}
		
		private function annulerVisaControleInventaireFactureResultHandler(re:ResultEvent):void
		{
			if (re.result > 0)
			{
				_selectedFacture.controleeInv = 0;
			}
			else
			{
				_selectedFacture.controleeInv = 1;
				Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
			}
		}
        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        override public function updateVisaExporteFacture():void
        {
            var callBackMethode:Function;
            if (_selectedFacture.exportee == 3)
            {
                //	    		_selectedFacture.exportee = 0;
                callBackMethode = annulerVisaExporteFactureResultHandler;
            }
            else if (_selectedFacture.exportee == 0)
            {
                //	    		_selectedFacture.exportee = 2;
                callBackMethode = decisionExporteFactureResultHandler;
            }
            else if (_selectedFacture.exportee == 2)
            {
                //	    		_selectedFacture.exportee = 3;
                callBackMethode = viserFactureExporteResultHandler;
            }
            else
            {
                callBackMethode = null;
                return;
            }
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "updateEtatExporte", callBackMethode);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        private function annulerVisaExporteFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.exportee = 3;
            }
            else
            {
                _selectedFacture.exportee = 0;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }

        private function viserFactureExporteResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.exportee = 2;
            }
            else
            {
                _selectedFacture.exportee = 0;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }

        private function decisionExporteFactureResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                _selectedFacture.exportee = 0;
            }
            else
            {
                _selectedFacture.exportee = 0;
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur'));
            }
        }

        override public function exportERPFacture():void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterCSV", exporterLaFactureResultHandler);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        private function exporterLaFactureResultHandler(re:ResultEvent):void
        {
            switch (String(re.result))
            {
                case "error":
                {
                    if (_selectedFacture.exportee != 1)
                    {
                        _selectedFacture.exportee = 0;
                    }
                    Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_export'));
                    break;
                }
                case "production":
                {
                    Alert.show(ResourceManager.getInstance().getString('M32', 'Votre_demande_a_bien__t__prise_en_compte'));
                    break;
                }
                default:
                {
                    displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
                    _selectedFacture.exportee = 1;
                }
            }
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        override public function initSectionControle():void
        {
            listeLignesFacturationsAbos.source = new Array();
            listeLignesFacturationsConsos.source = new Array();
            listeLignesFacturationsAbos.refresh();
            listeLignesFacturationsConsos.refresh();
        }

        override public function controlerFacture():void
        {
            listeLignesFacturationsAbos.source = new Array();
            listeLignesFacturationsConsos.source = new Array();
            listeLignesFacturationsAbos.refresh();
            listeLignesFacturationsConsos.refresh();
            /*var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.LigneFacturationGateWay", "getListeGrpProduitFacture", controlerFactureResultHandler);
             RemoteObjectUtil.callService(opData, _selectedFacture);*/
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.ControleFacture", "getListeGrpProduitFacture", controlerFactureResultHandler);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        private function controlerFactureResultHandler(re:ResultEvent):void
        {
            if (re.result != null && ((re.result as Array).length > 0))
            {
                if ((re.result[0] as Array).length > 0)
                    listeLignesFacturationsAbos.source = re.result[0] as Array;
                else
                    listeLignesFacturationsAbos.source = new Array();
                if ((re.result[1] as Array).length > 0)
                    listeLignesFacturationsConsos.source = re.result[1] as Array;
                else
                    listeLignesFacturationsConsos.source = new Array();
                listeLignesFacturationsAbos.refresh();
                listeLignesFacturationsConsos.refresh();
            }
            else
            {
                listeLignesFacturationsAbos.source = new Array();
                listeLignesFacturationsConsos.source = new Array();
                listeLignesFacturationsAbos.refresh();
                listeLignesFacturationsConsos.refresh();
            }
        }

        /*------------------------------------------------------------------------------------------------------------------------------------------------*/
        private function displayExport(name:String, urlCfm:String):void
        {
            var url:String = moduleSuiviFacturationIHM.NonSecureUrlBackoffice + urlCfm;
            var request:URLRequest = new URLRequest(url);
            var variables:URLVariables = new URLVariables();
            variables.FILE_NAME = name;
            request.data = variables;
            request.method = URLRequestMethod.POST;
            navigateToURL(request, "_blank");
        }

        override public function exporterLaFacture(type:String,bool:int):void
        {
            switch (type)
            {
                case AbstractControleFacture.PDF_FACTURE:
                {
                    displayFacture()
                    break;
                }
                case AbstractControleFacture.PDF_FACTURECAT:
                {
                    displayFactureCat();
                    break;
                }
                case AbstractControleFacture.XLS_LISTEFACTURE:
                {
                    exporterListeFacturesXLS(bool);
                    break;
                }
                case AbstractControleFacture.XLS_LISTEFACTURESUPP:
                {
                    exporterListeFacturesSupprimablesXLS();
                    break;
                }
                case AbstractControleFacture.CSV_LISTEFACTURE:
                {
                    exporterListeFacturesCSV(bool);
                    break;
                }
                default:
                {
                    throw new Error(ResourceManager.getInstance().getString('M32', 'Fromat_invalide'))
                }
            }
        }

        override public function getDetailGroupeProduit(groupeproduit:GroupeProduitControle):void
        {
            listeProduitsGroupeProduits = null;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.GroupeProduitFactureGateWay", "getDetailGroupeProduit", getDetailGroupeProduitResultHandler);
            RemoteObjectUtil.callService(opData, groupeproduit);
        }

        private function getDetailGroupeProduitResultHandler(re:ResultEvent):void
        {
            listeProduitsGroupeProduits = re.result as ArrayCollection;
        }

        /*----------------------------------------------------------------------------------------------------------------------*/
        override public function getJustificatif():void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.ProduitGateWay", "getListeProduitsGroupeDeProduit", function getListeProduitsGroupeDeProduitResultHandler(re:ResultEvent):void
                {
                    if ((re.result != null) && ((re.result as Array).length) > 0)
                    {
                        var arr:ArrayCollection = new ArrayCollection();
                        arr.source = re.result as Array;
                        var listeId:String = ConsoviewUtil.arrayToList(arr, "idCat", ",");
                        displayFacture(listeId);
                    }
                });
            RemoteObjectUtil.callService(opData, _selectedLigneFacturation);
        }

        /*----------------------------------------------------------------------------------------------------------------------*/
        override protected function getEvolutionCout():void
        {
            if (_boolEvolution && (_selectedFacture != null) && (_selectedFacture.periodeId > 0))
            {
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "getEvolutionCoutByCF", getEvolutionCoutResultHandler);
                RemoteObjectUtil.callService(opData, _selectedFacture);
            }
            else
            {
                evolutionCouts = new ArrayCollection();
                evolutionCouts.refresh();
            }
        }

        private function getEvolutionCoutResultHandler(re:ResultEvent):void
        {
            if ((re.result != null) && ((re.result as Array).length) > 0)
            {
                evolutionCouts.source = re.result as Array
            }
            else
            {
                evolutionCouts = new ArrayCollection();
            }
            evolutionCouts.refresh();
        }

        /*-----------------------HORS INVENTAIRE----------------------------------------------------------------------------------------------*/
        override public function initSectionInventaire():void
        {
            abosInv = new Ressource();
            abosHInv = new Ressource();
            consosHInv = new Ressource();
            listeRessourcesHInv = new ArrayCollection();
            listeRessourcesHInv.refresh();
            listeRessourcesInv = new ArrayCollection();
            listeRessourcesInv.refresh();
        }

        override public function getRessourcesHorsInventaire():void
        {
            abosHInv = new Ressource();
            consosHInv = new Ressource();
            if (_boolInventaire)
            {
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "getMontantRessourcesHorsInventaire", getRessourcesHorsInventaireResultHandler);
                RemoteObjectUtil.callService(opData, _selectedFacture);
            }
        }

        private function getRessourcesHorsInventaireResultHandler(re:ResultEvent):void
        {
            if ((re.result != null) && ((re.result as Array).length > 0))
            {
                abosHInv = re.result[0] as Ressource;
                consosHInv = re.result[1] as Ressource;
            }
        }

        override public function getDetailRessourcesHorsInventaire(typeTheme:String = "tout"):void
        {
            listeRessourcesHInv = new ArrayCollection();
            listeRessourcesHInv.refresh();
            abosHInv = new Ressource();
            consosHInv = new Ressource();
            if (_boolInventaire)
            {
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "getDetailRessourcesHorsInventaire", getDetailRessourcesHorsInventaireResultHandler);
                RemoteObjectUtil.callService(opData, _selectedFacture, typeTheme);
            }
        }

        private function getDetailRessourcesHorsInventaireResultHandler(re:ResultEvent):void
        {
            if (!re.result)
                return;
            var tabResult:Array = re.result as Array;
            listeRessourcesHInv = tabResult[0];
            listeRessourcesHInv.refresh();
            if ((tabResult[1] as ArrayCollection).length >= 0)
            {
                //abosHInv.quantite = tabResult[1][0].quantite;
                //abosHInv.typeTheme = tabResult[1][0].typeTheme;
               	//abosHInv.montant = tabResult[1][0].montant;
            }
            if ((tabResult[1] as ArrayCollection).length >= 2)
            {
                consosHInv.quantite = tabResult[1][1].quantite;
                consosHInv.typeTheme = tabResult[1][1].typeTheme;
                consosHInv.montant = tabResult[1][1].montant;
            }
        }

        public override function exportContent(queryType:String, typeTheme:String = "tout"):void
        {
            if (_boolInventaire)
            {
                var urlback:String = moduleSuiviFacturationIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/export.cfm";
                var variables:URLVariables = new URLVariables();
                variables.IDINVENTAIRE_PERIODE = _selectedFacture.periodeId;
                variables.TYPE_THEME = typeTheme;
                variables.QUERY_TYPE = queryType;
                var request:URLRequest = new URLRequest(urlback);
                request.data = variables;
                request.method = URLRequestMethod.POST;
                navigateToURL(request, "_blank");
            }
        }

        override public function exporterDetailRessourcesHorsInventaire(typeTheme:String = "tout"):void
        {
            if (_boolInventaire)
            {
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterRessourcesHICSV", exporterDetailRessourcesHorsInventaireResultHandler);
                RemoteObjectUtil.callService(opData, _selectedFacture, typeTheme);
            }
        }

        private function exporterDetailRessourcesHorsInventaireResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
            }
            else
            {
                Alert.show("Erreur export");
            }
        }

        /*-----------------------DANS INVENTAIRE----------------------------------------------------------------------------------------------*/
        override public function getRessourcesInventaire():void
        {
            abosInv = new Ressource()
            if (_boolInventaire)
            {
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "getMontantRessourcesInventaireNonFacturee", getRessourcesInventaireResultHandler);
                RemoteObjectUtil.callService(opData, _selectedFacture);
            }
        }

        private function getRessourcesInventaireResultHandler(re:ResultEvent):void
        {
            if (re.result != null)
            {
                abosInv = re.result as Ressource;
            }
            //getDetailRessourcesInventaire();
        }

        override public function getDetailRessourcesInventaire(typeTheme:String = "tout"):void
        {
            listeRessourcesInv = new ArrayCollection();
            listeRessourcesInv.refresh();
            if (_boolInventaire)
            {
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "getDetailRessourcesInventaireNonFacturee", getDetailRessourcesInventaireResultHandler);
                RemoteObjectUtil.callService(opData, _selectedFacture, typeTheme);
            }
        }

        private function getDetailRessourcesInventaireResultHandler(re:ResultEvent):void
        {
            if ((re.result != null) && ((re.result as Array).length > 0))
            {
                listeRessourcesInv.source = re.result as Array;
                listeRessourcesInv.refresh();
            }
            else
            {
                listeRessourcesInv = new ArrayCollection();
                listeRessourcesInv.refresh();
            }
        }

        override public function exporterDetailRessourcesInventaire(typeTheme:String = "tout"):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterRessourcesICSV", exporterDetailRessourcesInventaireResultHandler);
            RemoteObjectUtil.callService(opData, _selectedFacture, typeTheme);
        }

        private function exporterDetailRessourcesInventaireResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
            }
            else
            {
                Alert.show("Erreur export");
            }
        }

        /*----------------------------------------------------------------------------------------------------------------------*/ /*----------------------------------------------------------------------------------------------------------------------*/
        override public function exporterEvolutionRatioCoutByCf(format:String = "CSV"):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterEvolutionRatioCoutByCf" + format, exporterEvolutionRatioCoutCfResultHandler);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        private function exporterEvolutionRatioCoutCfResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_export'));
            }
        }

        override public function exporterEvolutionCoutByCf(format:String = "CSV"):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterEvolutionCoutByCf" + format, exporterEvolutionCoutCfResultHandler);
            RemoteObjectUtil.callService(opData, _selectedFacture);
        }

        private function exporterEvolutionCoutCfResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_export'));
            }
        }
        /*----------------------------------------------------------------------------------------------------------------------*/
        private var formatG:String = CSV;

        override public function exporterDetailFactureByOrga(format:String = "CSV"):void
        {
            formatG = format;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterDetailFactureByOrga", exporterDetailFactureByOrgaResultHandler);
            RemoteObjectUtil.callService(opData, selectedFacture, seletcedOrganisation.IDGROUPE_CLIENT, format);
        }

        private function exporterDetailFactureByOrgaResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                switch (formatG)
                {
                    case CSV:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
                        break;
                    }
                    case PDF:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/pdf/exportPDF.cfm");
                        break;
                    }
                    case XLS:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/exportXLS.cfm");
                        break;
                    }
                    default:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
                    }
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_export'));
            }
        }

        ///// ---------------- ////
        override public function exporterFactureByOrga(format:String = "CSV"):void
        {
            formatG = format;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterFactureByOrga", exporterDetailFactureByOrgaResultHandler);
            RemoteObjectUtil.callService(opData, selectedFacture, seletcedOrganisation.IDGROUPE_CLIENT, format);
        }

        /*private function exporterFactureByOrgaResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                switch (formatG)
                {
                    case CSV:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
                        break;
                    }
                    case PDF:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/pdf/exportPDF.cfm");
                        break;
                    }
                    case XLS:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/exportXLS.cfm");
                        break;
                    }
                    default:
                    {
                        displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
                    }
                }
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_export'));
            }
        }*/

        /*----------------------------------------------------------------------------------------------------------------------*/
        override public function exporterLignesNonAffecteesCA(format:String = "PDF"):void
        {
            var url:String = moduleSuiviFacturationIHM.urlBackoffice + "/fr/consotel/consoview/facturation/suiviFacturation/controles/pdf/exportLignesPDF.cfm";
            var variables:URLVariables = new URLVariables();
            variables.IDINVENTAIRE_PERIODE = selectedFacture.periodeId;
            variables.IDORGANISATION = seletcedOrganisation.IDGROUPE_CLIENT;
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = "POST";
            navigateToURL(request, "_blank");
        }

        /*----------------------------------------------------------------------------------------------------------------------*/ //--Totaux
        private function totauxChangeHandler(ce:CollectionEvent):void
        {
            var total:LigneFacturation = new LigneFacturation();
            total.libelleProduit = ResourceManager.getInstance().getString('M32', 'TOTAL_pour_une_facture_de_') + ConsoviewFormatter.formatEuroCurrency(selectedFacture.montant, 2);
            total.montantTotal = ConsoviewUtil.calculTotal(listeLignesFacturationsAbos, "montantTotal") + ConsoviewUtil.calculTotal(listeLignesFacturationsConsos, "montantTotal");
            total.montantMoyen = ConsoviewUtil.calculTotal(listeLignesFacturationsAbos, "montantCalculeRemise") + ConsoviewUtil.calculTotal(listeLignesFacturationsConsos, "montantMoyen");
            total.difference = total.montantTotal - total.montantMoyen;
            total.poidsDifference = (1 - (total.montantMoyen / total.montantTotal)) * 100;
            totaux = new ArrayCollection();
            totaux.refresh();
            totaux.addItem(total);
            totaux.refresh();
        }

        /*----------------------------------------------------------------------------------------------------------------------*/
        private function displayFacture(listeIdCat:String = ""):void
        {
            var url:String = moduleSuiviFacturationIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/display_Facture.cfm";
            var variables:URLVariables = new URLVariables();
            variables.PRODUIT_RECHERCHE = listeIdCat;
            variables.ID_FACTURE = selectedFacture.periodeId;
            variables.IDX_PERIMETRE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            variables.prixCat = prixReference;
            var request:URLRequest = new URLRequest(url);
            variables.type_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
            request.data = variables;
            request.method = "POST";
            navigateToURL(request, "_blank");
        }

        private function displayFactureCat():void
        {
            var url:String = moduleSuiviFacturationIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/gabaritFacture.cfm";
            var variables:URLVariables = new URLVariables();
            variables.ID_FACTURE = selectedFacture.periodeId;
            variables.IDX_PERIMETRE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            variables.DATE_EMISSION = selectedFacture.dateEmission.toLocaleDateString();
            variables.DATEDEB = selectedFacture.dateDebut.toLocaleDateString();
            variables.DATEFIN = selectedFacture.dateFin.toLocaleDateString();
            variables.OPNOM = selectedFacture.operateurLibelle;
            variables.NUMERO_FACTURE = selectedFacture.numero;
            variables.IDREF_CLIENT = selectedFacture.refClientId;
            variables.LIBELLE = selectedFacture.libelle;
            variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
            var request:URLRequest = new URLRequest(url);
            variables.type_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
            request.data = variables;
            request.method = "POST";
            navigateToURL(request, "_blank");
        }

 
        protected function exporterListeFacturesXLS(boolcalc:int):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterListeFacturesXLS", exporterListeFacturesXLSResultHandler);
            RemoteObjectUtil.callService(opData, _paramsRecherche,boolcalc);
        }

        protected function exporterListeMesFacturesXLS():void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.supprimerFacture.FactureGateWay", "exporterListeMesFacturesXLS", exporterListeFacturesXLSResultHandler);
            RemoteObjectUtil.callService(opData, _paramsRecherche);
        }

        protected function exporterListeFacturesSupprimablesXLS():void
        {
            exporterListeMesFacturesXLS();
        }

        override protected function exporterListeFacturesCSV(boolcalc:int):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "exporterListeFacturesCSV", exporterListeFacturesCSVResultHandler);
            RemoteObjectUtil.callService(opData, _paramsRecherche,boolcalc);
        }

        protected function exporterListeFacturesCSVResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/exportFactureCSV.cfm");
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_export'));
            }
        }

        protected function exporterListeFacturesXLSResultHandler(re:ResultEvent):void
        {
            if (String(re.result) != "error")
            {
                displayExport(String(re.result), "/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/exportXLS.cfm");
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Erreur_export'));
            }
        }

        override public function detailFactureByOrga(orga:Object):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "detailFactureByOrga", detailFactureByOrgaResultHandler);
            RemoteObjectUtil.callService(opData, selectedFacture, orga.IDGROUPE_CLIENT);
        }

        override public function factureByOrga(orga:Object):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "factureByOrga", detailFactureByOrgaResultHandler);
            RemoteObjectUtil.callService(opData, selectedFacture, orga.IDGROUPE_CLIENT);
        }

        private function detailFactureByOrgaResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                montantfactureEclateeByOrga = re.result as ArrayCollection;
            }
            else
            {
                montantfactureEclateeByOrga = null;
            }
        }

        override public function totalFactureByOrga(orga:Object):void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.controles.FactureGateWay", "totalFactureByOrga", totalFactureByOrgaResultHandler);
            RemoteObjectUtil.callService(opData, selectedFacture, orga.IDGROUPE_CLIENT);
        }

        private function totalFactureByOrgaResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                var col:ArrayCollection = re.result as ArrayCollection;
                var len:Number = col.length;
                if (len > 0)
                {
                    totalfactureByOrga = col[0].MONTANT;
                    if (selectedFacture.montant != 0)
                    {
                        percentMontantViseAnalytique = Number(Number(totalfactureByOrga * 100 / selectedFacture.montant).toFixed(2));
                        trace("***********************************************")
                        trace(Number(totalfactureByOrga * 100 / selectedFacture.montant).toFixed(2));
                         trace(Number(Number(totalfactureByOrga * 100 / selectedFacture.montant).toFixed(2)));
                         trace("***********************************************")
            
                    }
                    else
                    {
                        percentMontantViseAnalytique = 0;
                    }
                    montantNonAffecte = selectedFacture.montant - totalfactureByOrga;
                    if (!ConsoviewUtil.isIdInArray(seletcedOrganisation.IDGROUPE_CLIENT, "IDGROUPE_CLIENT", visaAnalytiqueleSaveList.source))
                    {
                        var item:Object = new Object();
                        item.LIBELLE_GROUPE_CLIENT = seletcedOrganisation.LIBELLE_GROUPE_CLIENT;
                        item.IDGROUPE_CLIENT = seletcedOrganisation.IDGROUPE_CLIENT;
                        item.TOTAL_ANALYTIQUE = totalfactureByOrga;
                        item.TOTAL_NONAFFECTE = montantNonAffecte;
                        item.PERCENT = percentMontantViseAnalytique;
                        item.GARDER = false;
                        visaAnalytiqueleSaveList.addItem(item);
                    }
                }
                else
                {
                    totalfactureByOrga = 0
                    percentMontantViseAnalytique = 0;
                    montantNonAffecte = selectedFacture.montant;
                }
            }
        }

        //Recharge le resultat du controle analytique pour une orga donnée
        override public function chargerResultatAnalityque(orga:Object):void
        {
            factureByOrga(orga);
            totalFactureByOrga(orga);
        }

        override public function construireMemoControleAnalytique():String
        {
            var corps:String = ResourceManager.getInstance().getString('M32', 'Organiosation_s__vis_e_s___');
            for (var i:Number = 0; i < visaAnalytiqueleSaveList.length; i++)
            {
                if (visaAnalytiqueleSaveList[i].GARDER)
                {
                    corps = corps + " \n- " + visaAnalytiqueleSaveList[i].LIBELLE_GROUPE_CLIENT;
                }
            }
            return corps + construireSignature();
        }

        override public function construireSignature():String
        {
            var user:String = CvAccessManager.getSession().USER.NOM + " " + CvAccessManager.getSession().USER.PRENOM;
            var userMail:String = CvAccessManager.getSession().USER.EMAIL;
            return ResourceManager.getInstance().getString('M32', '_nPar_') + user + "  " + userMail + ResourceManager.getInstance().getString('M32', '_nle_') + DateFunction.formatDateAsString(new Date());
        }
		
		override public function rapportProduitErreur():void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.RapportProduitErreur",
				"getRapportProduitErreur",
				getRapportProduitErreurResultHandler);
			RemoteObjectUtil.callService(opData,_paramsRecherche);
		}
		
		private function getRapportProduitErreurResultHandler(event:ResultEvent):void
		{
			if(event.result!=-1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M32', 'Votre_demande_de_rapport____t__prise_en_'));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M32', 'Votre_demande_de_rapport_n___pu__tre_tra'));
			}
		}
		
    }
}