<cfcomponent displayname="fr.consotel.consoview.M16.v2.util.Proc" hint="Procedure stockee :-( CF9.0.2" output="false">
	
	<cffunction name="getSimInfo" access="remote" returntype="Query" output="false" hint="retourne la sim par defaut">
		<cfargument name="context" type="Struct" required="true" >
        <cfset var ctx = arguments.context>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.GETSIMCARD">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#ctx.idRevendeur#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#ctx.idOperateur#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#ctx.idTypeOpe#">		
				<cfprocresult name="qSim">
			</cfstoredproc>
			
			<cfreturn qSim>
		
	</cffunction>
	
	
	
</cfcomponent>