package rapportPilotage.service.periodecomboboxselector
{
	import flash.events.EventDispatcher;
	import mx.collections.ArrayCollection;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.vo.ValeurSelector;

	internal class PeriodeComboBoxSelectorModel  extends EventDispatcher
	{
		private var _periodes :ArrayCollection;
		
		public function PeriodeComboBoxSelectorModel()
		{
			_periodes = new ArrayCollection();
			
		}
		public function get periodes():ArrayCollection
		{
			return _periodes;
		}
		
		//permert de remplir l'array collection _periodes 	
		internal function updateValues(value:ArrayCollection):void
		{	
			_periodes.removeAll();// vider l'ArrayCollection
		
			var valeurSelector:ValeurSelector;			
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelector();
				valeurSelector.fill(value[i]);
				_periodes.addItem(valeurSelector); 
			}	
			this.dispatchEvent(new RapportEvent(RapportEvent.USER_PERIODE_SELECTOR_EVENT));
		}
	}
}