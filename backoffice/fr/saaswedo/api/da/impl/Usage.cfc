<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.Usage" extends="fr.saaswedo.api.da.impl.DataService"
hint="Implémentation des services de traitement des données d'Usage Data dans Datalert! RT">
	<!--- =========== URI des services =========== --->
	
	<cffunction access="public" name="CUMULATIVE_USAGE_URI" returntype="String" hint="URI du service Cumulative Usage Upload">
		<cfreturn "/api/da/cu.cfm">
	</cffunction>
	
	<cffunction access="public" name="COMPLETE_APP_USAGE_URI" returntype="String" hint="URI du service Complete Application Usage Upload">
		<cfreturn "/api/da/app.cfm">
	</cffunction>
	
	<cffunction access="public" name="COMPLETE_USAGE_URI" returntype="String" hint="URI du service Complete Usage Upload">
		<cfreturn "/api/da/co.cfm">
	</cffunction>
	
	<!--- Clés correspondant à des champs métiers --->
	
	<cffunction access="public" name="APP_KEY" returntype="String" hint="Champ APP: Nom de l'application (e.g com.google.gmail)">
		<cfreturn "APP">
	</cffunction>
	
	<cffunction access="public" name="CRU_KEY" returntype="String" hint="Champ CRU: Usage Roaming total en Ko">
		<cfreturn "CRU">
	</cffunction>
	
	<cffunction access="public" name="CRUU_KEY" returntype="String" hint="Champ CRU: Usage Roaming upload en Ko">
		<cfreturn "CRUU">
	</cffunction>
	
	<cffunction access="public" name="CRUD_KEY" returntype="String" hint="Champ CRU: Usage Roaming Download en Ko">
		<cfreturn "CRUD">
	</cffunction>
	
	<cffunction access="public" name="CDU_KEY" returntype="String" hint="Champ CDU: Usage Domestique total en Ko">
		<cfreturn "CDU">
	</cffunction>
	
	<cffunction access="public" name="CDUU_KEY" returntype="String" hint="Champ CDU: Usage Domestique upload en Ko">
		<cfreturn "CDUU">
	</cffunction>
	
	<cffunction access="public" name="CDUD_KEY" returntype="String" hint="Champ CDU: Usage Domestique download en Ko">
		<cfreturn "CDUD">
	</cffunction>
	
	<cffunction access="public" name="CWU_KEY" returntype="String" hint="Champ CWU: Usage Wifi tital en Ko">
		<cfreturn "CWU">
	</cffunction>
	
	<cffunction access="public" name="CWUU_KEY" returntype="String" hint="Champ CWU: Usage Wifi upload en Ko">
		<cfreturn "CWUU">
	</cffunction>
	
	<cffunction access="public" name="CWUD_KEY" returntype="String" hint="Champ CWU: Usage Wifi download en Ko">
		<cfreturn "CWUD">
	</cffunction>
	
	<!--- =========== Implémentation du service =========== --->
	
	<cffunction access="private" name="serviceRequest" returntype="void" hint="Effectue le traitement du service
	puis appele l'implémentation parent pour envoyer la réponse HTTP par défaut de traitement avec succès">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Délégation du traitement : Stockage de test
		- Soit cette fonction lève une exception pour arreter le traitement et donc ne pas envoyer la réponse par défaut de succès
		- Sinon (i.e Dans tous les cas) : La réponse HTTP par défaut de traitement avec succès sera envoyée au client
		--->
		<cfset processData(targetURI,requestData)>
	</cffunction>
	
	<!--- =========== Vérification des données métiers =========== --->
	
	<cffunction access="private" name="checkData" returntype="Boolean" hint="Effectue les vérifications métiers pour le flux correspondant à targetPage">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var httpCode=AUTHORIZED()>
		<cfset var clientInfo=getClientStringInfo()>
		<cfset var jsonResponse={"Message"=""}>
		<!--- Vérification des données métiers de l'implémentation parent : Renvoi une réponse HTTP si les données sont invalides --->
		<cfset var areRequestDataValid=SUPER.checkData(targetURI,requestData)>
		<cfif areRequestDataValid>
			<cfset var requestParams=getRequestParameters(requestData)>
			<cfset var payload=requestParams[DATA_KEY()]>
			<!--- Cumulative Usage Upload : Format JSON (String) --->
			<cfif compare(targetURI,CUMULATIVE_USAGE_URI()) EQ 0>
				<!--- Vérifie le format de la valeur du paramètre Data : JSON --->
				<cfset var isDataFormatValid=(NOT isBinary(payload)) AND isJSON(payload)>
				<cfif isDataFormatValid>
					<cfreturn checkCumulativeUsage(requestData)>
				<cfelse>
					<!--- Réponse HTTP : Si la vérification Check 3 n'est pas validée --->
					<cfset httpCode=INVALID_DATA_FORMAT()>
					<cfset jsonResponse={"Message"="Data usage : bad format"}>
					<!--- Traitement des données invalides --->
					<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
					<cfreturn FALSE>
				</cfif>
			<!--- Complete Application Usage Upload : CSV au format GZIP (Base64) --->
			<cfelseif compare(targetURI,COMPLETE_APP_USAGE_URI()) EQ 0>
				<cfreturn checkCompleteAppUsage(requestData)>
			<!--- Complete Usage Upload : CSV au format GZIP (Base64) --->
			<cfelseif compare(targetURI,COMPLETE_USAGE_URI()) EQ 0>
				<cfreturn checkCompleteUsage(requestData)>
			<!--- La valeur de targetPage n'est pas reconnue --->
			<cfelse>
				<cfset httpCode=BAD_REQUEST()>
				<cfset jsonResponse={"Message"="Bad HTTP request"}>
				<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
				<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - Unknown URI #targetURI# ('#CGI.SCRIPT_NAME#')">
				<!--- Traitement des données invalides --->
				<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
				<cfreturn FALSE>
			</cfif>
		<cfelse>
			<cfreturn areRequestDataValid>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="checkCumulativeUsage" returntype="Boolean" hint="Effectue les vérifications du flux Cumulative Usage Upload.
	Vérifie la présence et la validité des valeurs des clés d'usage ainsi que la présence et la validité pour la clé UTS_KEY()">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var scriptName=CGI.SCRIPT_NAME>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<!--- Valeurs par défaut --->
		<cfset var httpCode=AUTHORIZED()>
		<cfset var jsonResponse={"Message"="Payload accepted"}>
		<!--- Liste des clés de type données d'usage (Tous de type Numeric) : Toutes facultatives --->
		<cfset var dataUsageKeyList=[CRUU_KEY(),CRUD_KEY(),CDUU_KEY(),CDUD_KEY(),CWUU_KEY(),CWUD_KEY()]>
		<cfset var keyCount=arrayLen(dataUsageKeyList)>
		<!--- Payload contenant les données métiers --->
		<cfset var payload=getPayload(requestData)>
		<!--- Vérification de la clé UTS qui est obligatoire --->
		<cfset var utsIsValid=structKeyExists(payload,UTS_KEY())>
		<cfif utsIsValid>
			<cfset utsIsValid=isValidUTC(payload[UTS_KEY()])>
			<cfif (NOT utsIsValid) OR (payload[UTS_KEY()] LTE 0)>
				<!--- Champ UTS invalide --->
				<cfset httpCode=INVALID_UTS()>
				<cfset jsonResponse={"Message"="Unix Timestamp : bad format."}>
				<cflog type="error" text="[#log#][#scriptName#] HTTP #httpCode# returned to #clientInfo# - Invalid UTC '#payload[UTS_KEY()]#'">
			</cfif>
		<cfelse>
			<!--- Le champ UTS est absent --->
			<cfset httpCode=MISSING_UTS()>
			<cfset jsonResponse={"Message"="Unix Timestamp not present in the Payload."}>
		</cfif>
		<!--- Vérification des champs d'usage : Ces champs sont tous facultatifs et tous de type Numeric --->
		<cfloop index="i" from="1" to="#keyCount#">
			<cfset var key=dataUsageKeyList[i]>
			<cfset var keyExists=structKeyExists(payload,key)>
			<cfif keyExists>
				<cfset var valueIsValid=isValid("Numeric",payload[key])>
				<!--- Valeur invalide : La vérification est arretée immédiatement --->
				<cfif (NOT valueIsValid) OR (payload[key] LT 0)>
					<cfset httpCode=INVALID_DATA_FORMAT()>
					<cfset jsonResponse={"Message"="Data usage : bad format"}>
					<cfbreak/>
				</cfif>
			</cfif>
		</cfloop>
		<cfset var areRequestDataValid=httpCode EQ AUTHORIZED()>
		<!--- Réponse en cas d'invalidité des données --->
		<cfif NOT areRequestDataValid>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cflog type="error" text="[#log#][#scriptName#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
			<!--- Traitement des données invalides --->
			<cfset handleInvalidRequestData(scriptName,requestData,httpCode,jsonResponse)>
		</cfif>
		<cfreturn areRequestDataValid>
	</cffunction>
	
	<cffunction access="private" name="checkCompleteAppUsage" returntype="Boolean" hint="Effectue les vérifications du flux Complete Application
	Usage Upload. Retourne TRUE si les données sont valides et sinon FALSE en renvoyant la réponse HTTP correspondante">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var scriptName=CGI.SCRIPT_NAME>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<!--- Vérification des données métiers spécifiques à cette implémentation --->
		<cfset var requestParams=getRequestParameters(requestData)>
		<cfset var base64Payload=requestParams[DATA_KEY()]>
		<cfset var dataAreBase64=isBase64(base64Payload)>
		<!--- Vérification du format Base64 --->
		<cfif NOT dataAreBase64>
			<cfset var httpCode=INVALID_B64_COMPLETE_USAGE()>
			<cfset var jsonResponse={"Message"="Not a base64 value"}>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cflog type="error" text="[#log#][#scriptName#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
			<!--- Traitement des données invalides --->
			<cfset handleInvalidRequestData(scriptName,requestData,httpCode,jsonResponse)>
			<cfreturn FALSE>
		<cfelse>
			<cfreturn TRUE>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="checkCompleteUsage" returntype="Boolean"
	hint="Effectue les vérifications du flux Complete Usage Upload : Même implémentation que checkCompleteAppUsage()">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfreturn checkCompleteAppUsage(argumentCollection=ARGUMENTS)>
	</cffunction>
	
	<!--- Traitement des données métiers valides --->
	
	<cffunction access="private" name="processData" returntype="void" hint="Effectue le traitement du flux correspondant à targetPage.
	Les exceptions ne sont pas capturée par cette fonction. Elles sont traitées par l'application qui va ensuite envoyer une réponse HTTP au client">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<!--- La valeur de targetPage a déjà été vérifiée dans checkData() --->
		<cfset var targetURI=ARGUMENTS.targetPage>
		<!--- Données métiers --->
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Cumulative Usage Upload --->
		<cfif compare(targetURI,CUMULATIVE_USAGE_URI()) EQ 0>
			<cfset cumulativeUsageProcess(targetURI,requestData)>
		<!--- Complete Application Usage Upload : CDUU,CDUD,CWUU,CWUD,CRUU,CRUD --->
		<cfelseif compare(targetURI,COMPLETE_APP_USAGE_URI()) EQ 0>
			<cfset completeApplicationUsageProcess(targetURI,requestData)>
		<!--- Complete Usage Upload --->
		<cfelseif compare(targetURI,COMPLETE_USAGE_URI()) EQ 0>
			<cfset completeUsageProcess(targetURI,requestData)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="cumulativeUsageProcess" returntype="void" hint="Effectue le traitement du flux Cumulative Usage
	<br>Ce traitement préfixe toujours la valeur du champ UUID_HEADER() avec un caractère spécial pour éviter les problème de désérialization JSON">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<!--- Données métiers --->
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Cumulative Usage Upload --->
		<cfset var payload=getPayload(requestData)>
		<cfset var payloadToStore=DUPLICATE(payload)>
		<!--- Ajout du champs UUID dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
		<cfset var escapeAsciiCode=jsonNumberEscapeChar()>
		<cfset payloadToStore[UUID_HEADER()]=CHR(escapeAsciiCode) & getHeaderValue(UUID_HEADER(),requestData)>
		<!--- Domestic Usage --->
		<cfset payloadToStore[CDUU_KEY()]=structKeyExists(payload,CDUU_KEY()) ? VAL(payload[CDUU_KEY()]):-1>
		<cfset payloadToStore[CDUD_KEY()]=structKeyExists(payload,CDUD_KEY()) ? VAL(payload[CDUD_KEY()]):-1>
		<!--- Roaming Usage --->
		<cfset payloadToStore[CRUU_KEY()]=structKeyExists(payload,CRUU_KEY()) ? VAL(payload[CRUU_KEY()]):-1>				
		<cfset payloadToStore[CRUD_KEY()]=structKeyExists(payload,CRUD_KEY()) ? VAL(payload[CRUD_KEY()]):-1>
		<!--- WIFI Usage --->
		<cfset payloadToStore[CWUU_KEY()]=structKeyExists(payload,CWUU_KEY()) ? VAL(payload[CWUU_KEY()]):-1>
		<cfset payloadToStore[CWUD_KEY()]=structKeyExists(payload,CWUD_KEY()) ? VAL(payload[CWUD_KEY()]):-1>
		<!--- Stockage des données --->
		<cfset saveCumulativeUsage(payloadToStore)>
		<!--- Si le stockage s'est déroulé sans erreur : Envoie au client la réponse HTTP par défaut de traitement avec succès --->
		<cfset SUPER.serviceRequest(DEFAULT_URI(),requestData)>
	</cffunction>
	
	<cffunction access="private" name="completeApplicationUsageProcess" returntype="void" hint="Effectue le traitement du flux Complete Application Usage
	<br>Ce traitement retournera une réponse HTTP avec le code INVALID_DATA_FORMAT() si le champs APP_KEY() est invalide (N'est pas une chaine vide)
	<br>Ce traitement retournera une réponse HTTP avec le code INVALID_UTS() si le champs UTS_KEY() est invalide
	<br>Ce traitement préfixe toujours la valeur du champ UUID_HEADER() avec un caractère spécial pour éviter les problème de désérialization JSON">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var processResult=csvUsageProcess(targetURI,requestData,completeAppUsageColumns())>
		<cfset var httpCode=processResult.HTTP_CODE>
		<cfset var jsonResponse={"Message"=processResult.MESSAGE}>
		<!--- Si les données sont invalides --->
		<cfif httpCode NEQ 0>
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfset var clientInfo=getClientStringInfo()>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
			<!--- Traitement des données invalides --->
			<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
		<cfelse>
			<!--- Stockage des données --->
			<cfset saveCompleteApplicationUsage(processResult.PAYLOAD)>
			<!--- Si le stockage s'est déroulé sans erreur : Envoie au client la réponse HTTP par défaut de traitement avec succès --->
			<cfset SUPER.serviceRequest(DEFAULT_URI(),requestData)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="completeUsageProcess" returntype="void" hint="Effectue le traitement du flux CompleteUsage
	<br>Ce traitement préfixe toujours la valeur du champ UUID_HEADER() avec un caractère spécial pour éviter les problème de désérialization JSON">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var processResult=csvUsageProcess(targetURI,requestData,completeUsageColumns())>
		<cfset var httpCode=processResult.HTTP_CODE>
		<cfset var jsonResponse={"Message"=processResult.MESSAGE}>
		<!--- Si les données sont invalides --->
		<cfif httpCode NEQ 0>
			<cfset var log=GLOBALS().CONST('Datalert.LOG')>
			<cfset var clientInfo=getClientStringInfo()>
			<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
			<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
			<!--- Traitement des données invalides --->
			<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
		<cfelse>
			<!--- Stockage des données --->
			<cfset saveCompleteUsage(processResult.PAYLOAD)>
			<!--- Si le stockage s'est déroulé sans erreur : Envoie au client la réponse HTTP par défaut de traitement avec succès --->
			<cfset SUPER.serviceRequest(DEFAULT_URI(),requestData)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="csvUsageProcess" returntype="Struct" hint="Convertit les données d'un flux CSV compressé (GZIP) au format CFMX.
	<br>Ce traitement préfixe toujours la valeur du champ UUID_HEADER() avec un caractère spécial pour éviter les problème de désérialization JSON
	<br>Retourne une structure avec les clés :<br>- HTTP_CODE: 0 si les données sont valides. Sinon c'est le code à envoyer dans la réponse au client
	<br>- MESSAGE: Une chaine vide si les données sont valides. Sinon c'est le message a envoyer dans la réponse au client
	<br>- PAYLOAD : Données CSV converties au format CFMX (Tableau de structure dont les clés correspondantes au champs métiers du flux)
	<br>Cette fonction n'envoi pas de réponse HTTP au client. C'est celle qui l'utilise qui doit se charger de cela en fonction du résultat retourné">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfargument name="columnMetaData" type="Query" required="true" hint="Requete contenant la liste des colonnes CSV. Ex : completeUsageColumns()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<!--- Liste ordonnée des colonnes du flux : Ce traitement rajoutera toujours un champ UUID_HEADER() dans les données retournées --->
		<cfset var columnList=ARGUMENTS.columnMetaData>
		<cfset var columnCount=columnList.recordcount>
		<!--- Données métiers --->
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var requestParams=getRequestParameters(requestData)>
		<cfset var base64Payload=requestParams[DATA_KEY()]>
		<!--- Caractère spécial utilisé comme préfixe de la valeur du champ UUID_HEADER() --->
		<cfset var escapeAsciiCode=jsonNumberEscapeChar()>
		<cfset var httpCode=0>
		<cfset var jsonResponse={"Message"=""}>
		<!--- Complete Usage --->
		<cfset var csvDataArray=[]>
		<cfset var csvDeflatedString="">
		<cftry>
			<cfset csvDeflatedString=inflateBase64Gzip(base64Payload)>
			<cfcatch type="Any">
				<cfset var exceptionObject=CFCATCH>
				<!--- Extraction de la cause de l'exception : Exception Java --->
				<cfif structKeyExists(CFCATCH,"cause")>
					<cfset exceptionObject=CFCATCH.cause>
				</cfif>
				<!--- Type de l'exception Java levée par DatatypeConverter quand le format UTC est invalide --->
				<cfif structKeyExists(exceptionObject,"TYPE") AND (compare(exceptionObject.TYPE,"java.util.zip.DataFormatException") EQ 0)>
					<cfset var log=GLOBALS().CONST('Datalert.LOG')>
					<cfset var clientInfo=getClientStringInfo()>
					<cfset httpCode=INVALID_DATA_FORMAT()>
					<cfset jsonResponse["Message"]="Invalid GZIP Format : #exceptionObject.message#">
					<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
				<!--- Toute autre exception est levée à nouveau --->
				<cfelse>
					<cfrethrow>
				</cfif>
			</cfcatch>
		</cftry>
		<!--- Si le format GZIP des données est valide --->
		<cfif httpCode EQ 0>
			<cfset var csvLines=listToArray(csvDeflatedString,CHR(10),FALSE,FALSE)>
			<cfset var lineNb=arrayLen(csvLines)>
			<!--- Parcours des lignes CSV --->
			<cfloop index="i" from="1" to="#lineNb#">
				<cfset var lineValues=listToArray(csvLines[i],",",TRUE,FALSE)>
				<cfset var valueCount=arrayLen(lineValues)>
				<!--- Une ligne valide doit contenir toutes les colonnes du flux : Le traitement est arreté si la ligne est invalide --->
				<cfif valueCount LT columnCount>
					<!--- Seules les lignes non vides sont prises en compte : Au moins une valeur et la 1ère valeur est renseignée --->
					<cfif (valueCount GT 0) AND (LEN(TRIM(lineValues[1])) NEQ 0)>
						<cfset httpCode=INVALID_DATA_FORMAT()>
						<cfset jsonResponse["Message"]="One or more value are missing at line #i#">
						<cfbreak>
					</cfif>
				<cfelse>
					<!--- Traitement des données après vérification du champ UTS_KEY() --->
					<cfset var lineStruct={}>
					<!--- Ajout du champs UUID dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
					<cfset lineStruct[UUID_HEADER()]=CHR(escapeAsciiCode) & getHeaderValue(UUID_HEADER(),requestData)>
					<!--- Ajout du champs User-Agent dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
					<cfset lineStruct[USER_AGENT_KEY()]=CHR(escapeAsciiCode) & getHeaderValue(USER_AGENT_HEADER(),requestData)>
					<cfloop index="j" from="1" to="#columnCount#">
						<cfset var columnName=columnList["JSON_KEY"][j]>
						<!--- Par défaut la valeur d'une colonne est considérée comme de type STRING (UTC est aussi considéré comme STRING) --->
						<cfset var columnValue=lineValues[j]>
						<!--- Si la valeur de la colonne est obligatoire et qu'elle n'est pas renseignée alors le traitement est arreté --->
						<cfset var valueIsMandatory=yesNoFormat(columnList["IS_MANDATORY"][j])>
						<cfset var isValueEmpty=(LEN(TRIM(columnValue)) EQ 0)>
						<cfif valueIsMandatory AND isValueEmpty>
							<cfset httpCode=INVALID_DATA_FORMAT()>
							<cfset jsonResponse["Message"]="Column #columnName# (#j#) value at line #i# is empty or not specified">
							<!--- Le traitement est arreté si une ou plusieurs valeurs sont manquantes --->
							<cfbreak>
						</cfif>
						<!--- Un caractère spécial est utilisé comme contournement pour représenter les nombres
						en tant que chaines lors de la sérialisation JSON : Uniquement pour les champs dont VALUE_TYPE vaut SSTRING --->
						<cfif compareNoCase(TRIM(columnList["VALUE_TYPE"][j]),"SSTRING") EQ 0>
							<cfset columnValue=CHR(escapeAsciiCode) & columnValue>
						<!--- Valeur de type NUMERIC --->
						<cfelseif compareNoCase(TRIM(columnList["VALUE_TYPE"][j]),"NUMERIC") EQ 0>
							<cfset columnValue=VAL(TRIM(columnValue))>
						<!--- Valeur de type UTC --->
						<cfelseif compareNoCase(TRIM(columnList["VALUE_TYPE"][j]),"UTC") EQ 0>
							<cfif NOT isValidUTC(columnValue)>
								<cfset httpCode=INVALID_UTS()>
								<cfset jsonResponse["Message"]="Column #columnName# (#j#) value specified at line #i# is not a valid UTC datetime">
							</cfif>
						<!--- Valeur de type MCCMNC : Un caractère spécial est ajouté en préfixe de la valeur reçue --->
						<cfelseif compareNoCase(TRIM(columnList["VALUE_TYPE"][j]),"MCCMNC") EQ 0>
											
							<!--- DEBUT PATCH MNC sur 1 digit --->
							<cfset MCC = Left(columnValue,3)>
							<cfset MNC = Mid(columnValue,4,2)>							
							<cfif len(MNC) eq 1>
								<cfset MNC = "0" & MNC>
							</cfif>							
							<cfset columnValue = MCC & MNC >
							<!--- FIN PATCH MNC sur 1 digit --->
							

							<cfif NOT isValidMCCMNC(columnValue)>
								<cfset httpCode=BAD_REQUEST()>
								<cfset jsonResponse["Message"]="Column #columnName# (#j#) value specified at line #i# is not a valid MCC/MNC">
							</cfif>
							<!--- Ajout du champs CODE dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
							<cfset columnValue=CHR(escapeAsciiCode) & columnValue>								
						<!--- Valeur de type USAGE : Valeur numérique qui est remplacée par -1 si elle est non renseignée --->
						<cfelseif compareNoCase(TRIM(columnList["VALUE_TYPE"][j]),"USAGE") EQ 0>
							<cfset columnValue=(isValueEmpty EQ FALSE) ? VAL(TRIM(columnValue)):-1>
						</cfif>
						<!--- Le traitement est arreté si la valeur d'une colonne est invalide --->
						<cfif httpCode NEQ 0>
							<cfbreak>
						<cfelse>
							<cfset lineStruct[columnName]=columnValue>
						</cfif>
					</cfloop>
					<!--- Le traitement est arreté si une ligne CSV est invalide --->
					<cfif httpCode NEQ 0>
						<cfbreak>
					<cfelse>
						<!--- Ajout de la ligne CSV valide dans les données métiers à traiter --->
						<cfset arrayAppend(csvDataArray,lineStruct)>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>
		<!--- Résultat de la conversion --->
		<cfreturn {HTTP_CODE=httpCode,MESSAGE=jsonResponse.Message,PAYLOAD=csvDataArray}>
	</cffunction>
	
	<!--- Stockage des données --->
	
	<cffunction access="private" name="saveCumulativeUsage" returntype="void" hint="Stocke le flux Cumulative Usage au format JSON">
		<cfargument name="datalertPayload" type="Struct" required="true" hint="Données du flux : Structure dont les clés sont les champs métiers">
		<cfset var currentDateString=lsDateFormat(NOW(),"DD_MM_YYYY")>
		<cfset var fileName="CumulativeUsage_" & currentDateString & ".txt">
		<cfset var fileTitle="Datalert Cumulative Usage data of " & NOW()>
		<cfset saveDataInFile(serializeJSON(ARGUMENTS.datalertPayload),fileName,fileTitle)>
	</cffunction>
	
	<cffunction access="private" name="saveCompleteApplicationUsage" returntype="void" hint="Stocke le flux Complete Application Usage au format JSON">
		<cfargument name="datalertPayload" type="Array" required="true" hint="Données CSV : Tableau de structure dont les clés sont les champs métiers">
		<cfset var currentDateString=lsDateFormat(NOW(),"DD_MM_YYYY")>
		<cfset var fileName="CompleteApplicationUsage_" & currentDateString & ".txt">
		<cfset var fileTitle="Datalert Complete Application Usage data of " & NOW()>
		<cfset saveDataInFile(serializeJSON(ARGUMENTS.datalertPayload),fileName,fileTitle)>
	</cffunction>
	
	<cffunction access="private" name="saveCompleteUsage" returntype="void" hint="Stocke le flux Complete Usage au format JSON">
		<cfargument name="datalertPayload" type="Array" required="true" hint="Données CSV : Tableau de structure dont les clés sont les champs métiers">
		<cfset var currentDateString=lsDateFormat(NOW(),"DD_MM_YYYY")>
		<cfset var fileName="CompleteUsage_" & currentDateString & ".txt">
		<cfset var fileTitle="Datalert Complete Usage data of " & NOW()>
		<cfset saveDataInFile(serializeJSON(ARGUMENTS.datalertPayload),fileName,fileTitle)>
	</cffunction>
	
	<cffunction access="private" name="completeAppUsageColumns" returntype="Query" hint="Liste des colonnes du flux Complete Application Usage">
		<!--- Description
		Valeurs possibles pour VALUE_TYPE : STRING, SSTRING, UTC, MCCMNC, NUMERIC, USAGE (Valeur remplacée par -1 quand elle n'est pas renseignée)
		Valeurs possibles pour IS_MANDATORY : 0, 1
		Requete d'origine dans les specs
		<cfquery datasource="ROCOFFRE" name="keyList">
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#APP_KEY()#"> as JSON_KEY, 'STRING' as VALUE_TYPE, 1 as IS_MANDATORY, 1 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CDUU_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 2 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CDUD_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 3 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CWUU_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 4 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CWUD_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 5 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CRUU_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 6 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CRUD_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 7 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#UTS_KEY()#"> as JSON_KEY, 'UTC' as VALUE_TYPE, 1 as IS_MANDATORY, 8 as POSTITION
			from		DUAL
			order by POSTITION ASC
		</cfquery>
		--->
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m26.getCompleteKeys">
         <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="DT_FLUX_APPLICATION_USAGE">
			<cfprocresult name="keyList">
		</cfstoredproc>
		<cfreturn keyList>
	</cffunction>
	
	<cffunction access="private" name="completeUsageColumns" returntype="Query" hint="Liste des colonnes du flux Complete Usage">
		<!--- Description
		Valeurs possibles pour VALUE_TYPE : STRING, SSTRING, UTC, MCCMNC, NUMERIC, USAGE (Valeur remplacée par -1 quand elle n'est pas renseignée)
		Valeurs possibles pour IS_MANDATORY : 0, 1
		Requete d'origine dans les specs
		<cfquery datasource="ROCOFFRE" name="keyList">
			select	'SSID' as JSON_KEY, 'STRING' as VALUE_TYPE, 0 as IS_MANDATORY, 1 as POSTITION
			from		DUAL
			UNION
			select	'CELLID' as JSON_KEY, 'STRING' as VALUE_TYPE, 0 as IS_MANDATORY, 2 as POSTITION
			from		DUAL
			UNION
			select	'CurrentMCCMNC' as JSON_KEY, 'MCCMNC' as VALUE_TYPE, 0 as IS_MANDATORY, 3 as POSTITION
			from		DUAL
			UNION
			select	'OperatorName' as JSON_KEY, 'STRING' as VALUE_TYPE, 0 as IS_MANDATORY, 4 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CDUU_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 5 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CDUD_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 6 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CWUU_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 7 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CWUD_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 8 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CRUU_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 9 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#CRUD_KEY()#"> as JSON_KEY, 'USAGE' as VALUE_TYPE, 0 as IS_MANDATORY, 10 as POSTITION
			from		DUAL
			UNION
			select	'Altitude' as JSON_KEY, 'NUMERIC' as VALUE_TYPE, 0 as IS_MANDATORY, 11 as POSTITION
			from		DUAL
			UNION
			select	'Latitude' as JSON_KEY, 'NUMERIC' as VALUE_TYPE, 0 as IS_MANDATORY, 12 as POSTITION
			from		DUAL
			UNION
			select	'Longitude' as JSON_KEY, 'NUMERIC' as VALUE_TYPE, 0 as IS_MANDATORY, 13 as POSTITION
			from		DUAL
			UNION
			select	'Battery' as JSON_KEY, 'NUMERIC' as VALUE_TYPE, 0 as IS_MANDATORY, 14 as POSTITION
			from		DUAL
			UNION
			select	<cfqueryparam cfsqltype="cf_sql_varchar" value="#UTS_KEY()#"> as JSON_KEY, 'UTC' as VALUE_TYPE, 1 as IS_MANDATORY, 15 as POSTITION
			from		DUAL
			order by POSTITION ASC
		</cfquery>
		--->
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m26.getCompleteKeys">
         <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="DT_FLUX_CONSOS">
			<cfprocresult name="keyList">
		</cfstoredproc>
		<cfreturn keyList>
	</cffunction>
</cfcomponent>