package rapportPilotage.utils.produitdynamiqueselector
{
	import composants.util.ConsoviewAlert;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.CursorManager;
	import mx.utils.StringUtil;
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.service.produitdynamiqueselector.ProduitDynamiqueSelectorService;
	import rapportPilotage.utils.SelectorAbstract;
	import rapportPilotage.utils.serialization.json.JSON;
	import rapportPilotage.vo.Parametre;
	import rapportPilotage.vo.Produit;
	import rapportPilotage.vo.SelectorParametre;

	public class ProduitDynamiqueSelectorImpl extends SelectorAbstract 
	{
		// les data provider des listes (produit+ produit selectionné)
		[Bindable] public var  produitCollection :ArrayCollection;
		[Bindable] public var  produitSelectedCollection:ArrayCollection;   
		[Bindable] public var  labelProduit:Label;
		
		public var txtfiltreProduit:TextInput;
		public var txtfiltreProduitSelected:TextInput;
		
		public var sourceListeProduit:DataGrid;
		public var destinationListeProduit:DataGrid;
		public var checkBoxAbo:CheckBox;
		public var checkBoxConso :CheckBox;
		
		public var checkBoxAboProduit :CheckBox;
		public var checkBoxConsoProduit :CheckBox;
		
		private var indexPanel:Number=1;
		
		public var arrayCleLibelle :Object;
		
		public var produitSelectorService:ProduitDynamiqueSelectorService;
		
		public function ProduitDynamiqueSelectorImpl(selector:Object=null)
		{
			super(selector);
		}
		
		public function produitdynamique_creationCompleteHandler(event:FlexEvent):void
		{
			txtfiltreProduit.addEventListener(Event.CHANGE, filterListeProduit);
			txtfiltreProduitSelected.addEventListener(Event.CHANGE, filterListeProduit);
			
			checkBoxAbo.addEventListener(Event.CHANGE ,filterListeProduit);
			checkBoxConso.addEventListener(Event.CHANGE ,filterListeProduit);
			
			checkBoxAboProduit.addEventListener(Event.CHANGE ,filterListeProduit);
			checkBoxConsoProduit.addEventListener(Event.CHANGE ,filterListeProduit);
			
			produitSelectorService=new ProduitDynamiqueSelectorService();
			produitSelectorService.model.addEventListener(RapportEvent.USER_LIST_PRODUIT_EVENT,fillData);
			produitSelectorService.getListeProduit();
			
			produitSelectedCollection=new ArrayCollection();
		}
		
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			this.label="";
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
		
		/**
		 * permet de remplir la dataprovider du datagrid 
		 * @param event
		 * 
		 */		
		private function fillData(event:RapportEvent):void
		{
			produitCollection=produitSelectorService.model.produits;
			showHelp();
		}
		
		/**
		 * permet de récuperer les id des produits sous forme d'un paramètre
		 * @return Array
		 */		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var listeIdProduit:Array=[];
			
			if(produitSelectedCollection.length > 0)
			{
				for(var i : int = 0 ; i< produitSelectedCollection.length ; i++)
				{					
					var idProduit:Number=(produitSelectedCollection.getItemAt(i) as Produit).id
					listeIdProduit[i]=idProduit;
				}
				
				var parametre :Parametre=  new Parametre();
				
				parametre.value=[listeIdProduit.toString()]; // listeIdProduit.toString() convertir le tableau en chaine séparé par un , 
				parametre.cle =arrayCleLibelle[0].NOM_CLE;
				parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
				parametre.is_system=false;
				parametre.typeValue=this.typeParametreComposant.toUpperCase();
				
				tabValeur[0]=parametre;
				this._isOk=true;
				
				return tabValeur;
			}
			else
			{
				this._errorMessage="Vous devez sélectionner au moins un produit pour valider le lancement du rapport";
				this._isOk=false;
			}
			return tabValeur;
		}
		/** ----------------------------------------   déplacer item d'un datagrid à un autre --------------------------------------------------------*/
			
		public function onClickImageAdd(item:Produit):void
		{			
			this.addProduitToListeproduitSelected(item);
		}
			
		public function onClickImageRemove(item:Produit):void
		{
			this.removeProduitFromListeproduitSelected(item);
		}
		
		/**
		 * ajouter un produit au datagrid des produit selectionné
		 * @param item:produit 
		 */	
		private function addProduitToListeproduitSelected(item :Produit):void
		{
			if(produitSelectedCollection.length <= 9)// nb max autorisé à ajouter
			{
				produitSelectedCollection.addItem(item);  // ajouter au dataprovider 
				produitCollection.removeItemAt(produitCollection.getItemIndex(item));
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo("Vous avez atteint le nombre max de produits pouvant être analysés sur le même rapport",'Alerte',null);
			}
		}
		
		/**
		 * supprimer un produit du datagrid des produit selectionné
		 * @param item:produit 
		 */
		private function removeProduitFromListeproduitSelected(item:Produit):void
		{
			produitCollection.addItem(item);
			produitSelectedCollection.removeItemAt(produitSelectedCollection.getItemIndex(item)); 
		}
		
		/** ---------------------------------------------------   Flitrer les deux datagrid ------------------------------------------------------------
		 * 
		 * filtrer le data grid des produits à inclure
		 * @param event : Event 
		 * @return void
		 */
		private function filterListeProduit(event:Event):void
		{
			//  détermine le textInput dans laquelle on va saisir et le dataprovider	
			if((event.target.id == "txtfiltreProduit") || (event.target.id == "checkBoxAbo")|| (event.target.id == "checkBoxConso"))
			{
				if(produitCollection != null)
				{
					indexPanel=1;
					produitCollection.filterFunction = filterMyArrayCollection;// le nom de la fonction ;
					produitCollection.refresh();					
				}
			}
			else if((event.target.id == "txtfiltreProduitSelected") ||(event.target.id == "checkBoxAboProduit")||(event.target.id == "checkBoxConsoProduit"))
			{
				if(produitSelectedCollection != null)
				{
					indexPanel=2;
					produitSelectedCollection.filterFunction = filterMyArrayCollection;// le nom de la fonction ;
					produitSelectedCollection.refresh();
				}
			}
		}
		
		/**
		 * 
		 * permet de filtrer le data grid des produits
		 * @param item : Objet (chaque ligne du datagrid) 
		 * @return Boolean
		 */
		private function filterMyArrayCollection(item:Object):Boolean 
		{
			var itemLabelProduit :String="";
			var txtFiltre:TextInput;
			var checkBoxFiltreProduit:CheckBox;
			var checkBoxFiltreProduitSelected:CheckBox;
			
			if(item.label != null)
			{
				itemLabelProduit= (item.label as String).toLowerCase();// label produit
			}
			
			if(indexPanel==1)// 1ème panel de l'ihm
			{
				txtFiltre=txtfiltreProduit;// le text de l'utilisateur
				checkBoxFiltreProduit=checkBoxAbo;
				checkBoxFiltreProduitSelected=checkBoxConso;
			}
			else
			{
				txtFiltre=txtfiltreProduitSelected;
				checkBoxFiltreProduit=checkBoxAboProduit;
				checkBoxFiltreProduitSelected=checkBoxConsoProduit;
			}
			var searchString:String = StringUtil.trim(txtFiltre.text.toLowerCase());// le filtre
								
			if( (itemLabelProduit.indexOf(searchString) > -1 )&& ((item.typeTheme==1) && (checkBoxFiltreProduit.selected==true)))
			{
				return true;
			}
			if( (itemLabelProduit.indexOf(searchString) > -1 )&& ((item.typeTheme==2) && (checkBoxFiltreProduitSelected.selected==true)))
			{
				return true;
			}
			
			/*if( (checkBoxFiltre1.selected==false) && (checkBoxFiltre.selected==false) && (itemLabelProduit.indexOf(searchString) > -1 ))
			{
				return true;
			}*/
			return false;
		}
		/** -----------------------------------------------------------------------------------------------------------------------------------------*/
	}
}