package gestionparc.ihm.question.equipement
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheDissocierLigneFromEqp
	{
		private var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheDissocierLigneFromEqp()
		{
			
			Alert.yesLabel=ResourceManager.getInstance().getString('M111', 'oui');
			Alert.noLabel=ResourceManager.getInstance().getString('M111', 'non');
			Alert.cancelLabel=ResourceManager.getInstance().getString('M111', 'Annuler');
			Alert.okLabel=ResourceManager.getInstance().getString('M111', 'Valider');
			
			var ruleMobile:IRuleDisplayItem=new IsLigneMobile();
			var ruleFixe:IRuleDisplayItem=new IsLigneFixe();
			var ruleCollab:IRuleDisplayItem=new HasCollaborateurAssociated();

			if (ruleMobile.isDisplay())
			{
				if (ruleCollab.isDisplay())
					Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERLIGNEFROMEQPMOBILE"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseMobile)
				else
				{
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etre_vous_s_r_de_vouloir_dissocier_l__qu_sim'), ResourceManager.getInstance().getString('M111', 'Dissocier__quipement_de_la_ligne_SIM'), alertClickUserHandler);				
				}
					
			}
			else if (ruleFixe.isDisplay())
			{
				if (ruleCollab.isDisplay())
					Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERLIGNEFROMEQPFIXE"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseFixe)
				else
				{
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etre_vous_s_r_de_vouloir_dissocier_l__qu'), ResourceManager.getInstance().getString('M111', 'Dissocier__quipement_de_la_ligne'), alertClickHandler);				
				}				
			}
		}

		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				dissocierLigneFromEqp3();
			}
		}
		
		private function alertClickUserHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				dissocierLigneSimFromEqp3();
			}
		}
		
		private function qnCloseMobile(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				dissocierLigneSimFromEqp1();

			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
				dissocierLigneSimFromEqp2()
		}

		private function dissocierLigneSimFromEqp1():void
		{
			serv.dissocierLigneSimFromEqp1(setObjMobile());
		}

		private function dissocierLigneSimFromEqp2():void
		{
			serv.dissocierLigneSimFromEqp2(setObjMobile());
		}

		private function dissocierLigneSimFromEqp3():void
		{
			serv.dissocierLigneSimFromEqp3(setObjMobile());
		}

		private function setObjMobile():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.T_MODELE;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}

		/* LIGNE FIXE */
		private function qnCloseFixe(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				dissocierLigneFromEqp1()

			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
				dissocierLigneFromEqp2()
		}

		private function dissocierLigneFromEqp1():void
		{
			serv.dissocierLigneFromEqp1(setObjFixe());
		}

		private function dissocierLigneFromEqp2():void
		{
			serv.dissocierLigneFromEqp2(setObjFixe());
		}

		private function dissocierLigneFromEqp3():void
		{
			serv.dissocierLigneFromEqp3(setObjFixe());
		}

		private function setObjFixe():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.T_MODELE;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString(new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}
	}
}
