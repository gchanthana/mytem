package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;

	import gestionparc.entity.TrancheVO;
	import gestionparc.event.gestionparcEvent;

	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de la popup d'ajout et d'edition de tranche SDA
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 23.11.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class PopUpAddOrEditTrancheImpl extends TitleWindow
	{
		// VARIABLES------------------------------------------------------------------------------

		// IHM components
		public var btValid:Button;
		public var btCancel:Button;

		public var tiLibelle:TextInput;
		public var tiNbNum:TextInput;
		public var tiNumDepart:TextInput;
		public var tiNumFin:TextInput;

		public var cbTypeTranche:ComboBox;

		// publics variables
		public var isNewTranche:Boolean=false;
		public var objToEdit:TrancheVO=null;

		// VARIABLES------------------------------------------------------------------------------

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe PopUpAddOrEditTrancheImpl.
		 * </pre></p>
		 */
		public function PopUpAddOrEditTrancheImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #initListener()
		 * @see #initDisplay()
		 */
		private function init(event:FlexEvent):void
		{
			initListener();
			initDisplay();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initDisplay():void
		{
			if (isNewTranche)
			{
				this.title=ResourceManager.getInstance().getString('M111', 'Ajouter_une_tranche');
			}
			else
			{
				this.title=ResourceManager.getInstance().getString('M111', 'Editer_une_tranche');
				tiLibelle.text=objToEdit.libelle;
				tiNbNum.text=(objToEdit.nbNum) ? objToEdit.nbNum.toString() : "";
				tiNumDepart.text=(objToEdit.numDepart) ? objToEdit.numDepart.toString() : "";
				tiNumFin.text=(objToEdit.numFin) ? objToEdit.numFin.toString() : "";

				if (objToEdit.typeTranche == "SDA")
				{
					cbTypeTranche.selectedIndex=0;
				}
				else
				{
					cbTypeTranche.selectedIndex=1;
				}
			}
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #closePopup()
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ferme la popup courante.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Valide la pop up
		 * Si c'est une nouvelle tranche alors on l'ajoute dans le tableau, sinon on met ce dernier a jour.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			var obj:TrancheVO=new TrancheVO();
			obj.libelle=tiLibelle.text;
			obj.nbNum=(tiNbNum.text != "") ? parseInt(tiNbNum.text) : -1;
			obj.numDepart=(tiNumDepart.text != "") ? parseInt(tiNumDepart.text) : -1
			obj.numFin=(tiNumFin.text != "") ? parseInt(tiNumFin.text) : -1
			obj.typeTranche=cbTypeTranche.selectedItem.label;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_TRANCHE_VALIDATED, obj));
			closePopup(null);
		}

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

	}
}
