package gestionparc.services
{	
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.NumberBaseRoundType;
	import mx.formatters.NumberFormatter;
	import mx.resources.ResourceManager;
	
	public class Formator
	{
		public function Formator()
		{
		}
		
		public static function formatOctetsToMegaOctets(size:Number):String
		{
			var value:Number = size/1048576;
			
			return value.toPrecision(2);
		}

		public static function formatTotalWithSymbole(totalNbr:Number):String
		{
			var numberFormatter:NumberFormatter 		= new NumberFormatter();
				numberFormatter.precision 				= 2;
				numberFormatter.rounding 				= NumberBaseRoundType.NEAREST;
				numberFormatter.useThousandsSeparator	= false;
				numberFormatter.decimalSeparatorFrom	= ".";
				numberFormatter.decimalSeparatorTo		= ".";
				numberFormatter.thousandsSeparatorFrom	= "";
				numberFormatter.thousandsSeparatorTo	= "";
					
			var totalStg	:String = totalNbr.toString();
			var value		:String = numberFormatter.format(totalStg);
			
			var priceFormat:CurrencyFormatter = createCurrencyFormatter();
			var formatedPrice:String = priceFormat.format(value);
			
			var lastIdx:int = formatedPrice.lastIndexOf('0');
			
			var priceReturn:String = priceFormat.format(0);
			
			if(formatedPrice.length > 0)
			{
				var sub1:String = formatedPrice.substr(0, lastIdx);
				var sub2:String = formatedPrice.substr(lastIdx+1, formatedPrice.length);
				
				priceReturn = sub1 + sub2;
			}

			return priceReturn;
		}
		
		private static function createCurrencyFormatter():CurrencyFormatter
	    {
			var formateur_align_symbol				:String = ResourceManager.getInstance().getString('M16','formateur_align_symbol');
			var formateur_thousandsSeparatorTo		:String = ResourceManager.getInstance().getString('M16','formateur_thousandsSeparatorTo');;
			var formateur_decimalSeparatorTo		:String = ResourceManager.getInstance().getString('M16','formateur_decimalSeparatorTo');
			var _signe_de_la_devise__				:String = ResourceManager.getInstance().getString('M16','_signe_de_la_devise__');
			
	    	var priceFormat:CurrencyFormatter 		= new CurrencyFormatter();
		    	priceFormat.precision 				= 3;
				priceFormat.useThousandsSeparator	= true;
				priceFormat.useNegativeSign			= true;
		    	priceFormat.thousandsSeparatorTo	= formateur_thousandsSeparatorTo;
				priceFormat.decimalSeparatorFrom	= '.';
				priceFormat.decimalSeparatorTo		= formateur_decimalSeparatorTo;
		    	priceFormat.alignSymbol				= formateur_align_symbol;
		    	priceFormat.currencySymbol			= _signe_de_la_devise__;
			
			return priceFormat;
	    }
		
		public static function formatTotalNoSymbole(totalNbr:Number):String
		{
			var strg0:String = "0.00";
			if(totalNbr > 0.0000001)
			{
				var strg:String = totalNbr.toString();
				var strg1:String = "";
				var strg2:String = "";
				var strg3:String = "";
				var strg4:String = "";
				var strg5:String = "";
				var nbr:Number = -1;
				var len:Number = -1;
				var price:Number = 0;
																			
				len = strg.length;	
				nbr = strg.lastIndexOf(".",0);
																			
				if(totalNbr.toString().toLowerCase().indexOf(".") == -1)
				{
					strg0 = strg + ".00";
				}
				else
				{
					nbr = totalNbr.toString().toLowerCase().lastIndexOf(".");
					strg1 = strg.substring(0,nbr);
					strg3 = strg.substring(nbr+1,len);
					if(strg3.length > 2)
					{
						strg4 = strg.substring(nbr+3,nbr+4);
						if(Number(strg4) > 4)
						{
							strg5 = strg.substring(nbr+1,nbr+3);
							strg2 = (Number(strg5)+1).toString();
						}
						else
						{
							strg2 = strg.substring(nbr+1,nbr+3);
						}
					}
					else
					{
						strg2 = strg.substring(nbr+1,nbr + 3);
						if(strg2.length < 2)
						{
							strg2 = strg2 + "0";
						}
					}
					strg0 = strg1 + "." + strg2;
				}
			}
			return strg0;	
		}
		
		public static function calculPrice(allItems:ArrayCollection):String
		{
			var total:Number = 0;
			for(var i:int = 0; i < allItems.length;i++)
			{
				if(allItems[i].PRIX_UNIT != " - ")
				{
					total = total + Number(allItems[i].PRIX_UNIT);
				}
			}
			return total.toString();
		}
		
		public static function lookForCollaborateur(name:String):String
		{
			var nameCollaborateur:String = "";
			
			if(name == "")
				nameCollaborateur = " - " 
			else
				nameCollaborateur = name;
			
			return nameCollaborateur;
		}
		
		public static function giveMeDate(strg:String):String
		{
			var day		:String = strg.substr(8,2);
			var month	:String	= strg.substr(5,2);
			var year	:String	= strg.substr(0,4);
			
			return day + "/" + month + "/" + year;
		}
		
		public static function formatReverseDate(date:Date):String
		{
			var day:String = date.date.toString();
			
			if(day.length < 2) 
				day = "0" + day;
			
			var monthnbr :int = Number(date.month) + 1;
			var month	 :String = monthnbr.toString();
			
			if(month.length < 2)
				month = "0" + month;
			
			var year:String = date.fullYear.toString();
			
			return year + "/" + month + "/" + day;
		}
		
		public static function formatHourConcat(dateobject:Date):String
		{
			var hours	:String = dateobject.hours.toString();
			var minutes	:String = dateobject.minutes.toString();
			var seconds	:String = dateobject.seconds.toString();

			return hours + ":" + minutes + ":" + seconds;
		}
		
		public static function formatHour(dateobject:Date):Object
		{
			var hoursParsing	:Object = new Object();
			var hours			:String = dateobject.hours.toString();
			var minutes			:String = dateobject.minutes.toString();
			var seconds			:String = dateobject.seconds.toString();
			
			hoursParsing.HOURS	= hours;
			hoursParsing.MIN	= minutes;
			hoursParsing.SEC	= seconds;
			
			return hoursParsing;
		}
		
		public static function giveMeReverseStringDate(strg:String):String
		{
			var day:String 		= strg.substr(8,2);
			var month:String	= strg.substr(5,2);
			var year:String		= strg.substr(0,4);
			
			return  year + "/" + month + "/" + day;
		}
		
		public static function formatDateStringInDate(dateString:String):Date
		{
			if(dateString == null)
				return null;
			var dateInArray:Array = dateString.split("/");
			var dateFormated:Date = new Date(dateInArray[2],int(dateInArray[1])-1,dateInArray[0]);
			return dateFormated;
		}
		
		public static function searchVirguleOrPoint(price:String):Number
		{
			if(Number(price) < 0)
				price = price.replace(",",".");
			
			return Number(price);
		}
		
		public static function formatInClob(arrayInString:Array):String
		{
			var dataInClob:String = "";
			
			for(var i:int = 0; i < arrayInString.length;i++)
			{
				if(i == 0)
					dataInClob = arrayInString[i];
				else
					dataInClob = dataInClob + arrayInString[i];
				
				if(i != arrayInString.length-1)
					dataInClob = dataInClob + ",";
			}
			
			var lastIndex:int = dataInClob.lastIndexOf(',', dataInClob.length);
			
			if(lastIndex == (dataInClob.length -1))
				dataInClob = dataInClob.slice(0, lastIndex);
			
			return dataInClob;
		}
		
		public static function formatInClobWithDatafieled(arrayInString:ArrayCollection, dataField:String):String
		{
			var dataInClob:String = "";
			
			if(arrayInString == null) return '';
			
			for(var i:int = 0; i < arrayInString.length;i++)
			{
				if(i == 0)
					dataInClob = arrayInString[i][dataField];
				else
					dataInClob = dataInClob + arrayInString[i][dataField];
				
				if(i != arrayInString.length-1)
					dataInClob = dataInClob + ",";
			}
			
			var lastIndex:int = dataInClob.lastIndexOf(',', dataInClob.length);
			
			if(lastIndex == (dataInClob.length -1))
				dataInClob = dataInClob.slice(0, lastIndex);
			
			return dataInClob;
		}
	    
	    public static function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			
			if(day.length < 2)
				day = "0" + day;
			
			var temp	:int = Number(dateobject.month) + 1;
			var month	:String = temp.toString();
			
			if(month.length < 2)
				month = "0" + month;
			
			var year:String = dateobject.fullYear;
			
			return day + "/" + month + "/" + year;
		}
		
		public static function formatInteger(intToFormat:int):Boolean
		{
			var bool:Boolean = false;
			
			if(intToFormat == 1)
				bool = true;
			
			return bool;
		}
		
		public static function formatBoolean(boolToFormat:Boolean):int
		{
			var integer:int = 0;
			
			if(boolToFormat)
				integer = 1;
			
			return integer;
		}
		
		public static function giveMeFPCDate(fpc:String):String
		{
			var datefpc	:String = fpc;
			var day		:String = datefpc.substr(8,2);
			var month	:String	= datefpc.substr(5,2);
			var year	:String	= datefpc.substr(0,4);
			
			return day + "/" + month + "/" + year;
		}
		
		public static  function getSuffixeFileName():String
		{
			var fileName:String = '';
			var myDate	:Date = new Date();
			var dateStrg:String = DateFunction.formatDateAsString(myDate);
			var timeStrg:String = getZeroOrNot(myDate.getHours().toString()) + '_' +  getZeroOrNot(myDate.getMinutes().toString()) + '_' + getZeroOrNot(myDate.getSeconds().toString()) + '.xls';
			var dateArry:Array = dateStrg.split('/');
			var lenDate	:int = dateArry.length;
			
			for(var i:int = 0;i < lenDate;i++)
			{
				fileName = fileName + dateArry[i] + '_';
			}
			
			fileName = fileName + timeStrg;
			
			return fileName;
		}
		
		public static  function getZeroOrNot(value:String):String
		{
			var newValue:String = '';
			
			if(value.length == 1)
				newValue = '0' + value;
			else
				newValue = value;
			
			return newValue;
		}
		
		public static function sortFunction(value:ArrayCollection, label:String, isDescending:Boolean = false):ArrayCollection
	    {
			if(value == null) return new ArrayCollection();
			
	    	var tri:Sort = new Sort();
	    		tri.fields 	 = [new SortField(label, false, isDescending)];
			
			value.sort 	 = tri;
			value.refresh();
			value.sort = null;
			
			return value;
	    }

	}
}