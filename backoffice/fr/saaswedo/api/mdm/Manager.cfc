<cfinterface author="Cedric" hint="Interface des fonctionnalités de gestion MDM">
	<cffunction name="getMdmServer" returntype="fr.saaswedo.api.mdm.MdmServer" hint="Retourne les infos serveur MDM actuellement utilisées"> 
	</cffunction>
	
	<cffunction name="lock" returntype="void" hint="Vérrouille l'écran d'un device et modifie le code de vérrouillage si possible">
		<cfargument name="serial" type="String" required="true" hint="Numéro de série du device">
		<cfargument name="imei" type="String" required="false" default="" hint="IMEI du device">
		<cfargument name="passcode" type="String" required="false" default="" hint="Nouveau code de vérrouillage">
	</cffunction>
</cfinterface>