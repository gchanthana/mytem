package gestionparc.services.vue.toutparc
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import gestionparc.entity.vue.MatriceParcGlobalVO;
	import gestionparc.entity.vue.MatriceParcVO;
	import gestionparc.event.gestionparcEvent;

	public class VueToutParcData extends EventDispatcher
	{
		private var _listeResultat:ArrayCollection = null;
		private var _NB_ROWS:Number;
		
		public function VueToutParcData()
		{
		}
		
		public function setViewParcGlobal(resultObject:ArrayCollection):void
		{
			listeResultat = new ArrayCollection();
			
			for each(var obj:Object in resultObject)
			{
				listeResultat.addItem(createViewParcGlobalItemVo(obj));
			}
			
			if(listeResultat.length > 0)
				NB_ROWS = (listeResultat.getItemAt(0) as MatriceParcGlobalVO).NB_ROWN;
			else
				NB_ROWS = 0;
			
			listeResultat.refresh();
			
			dispatchEvent(new gestionparcEvent(gestionparcEvent.RECHERCHER_FINISHED,NB_ROWS));
		}
		
		private function createViewParcGlobalItemVo(obj:Object):MatriceParcGlobalVO
		{
			var item : MatriceParcGlobalVO = new MatriceParcGlobalVO();
			item.createObject(obj);
			return item;
		}
		
		public function set listeResultat(value:ArrayCollection):void
		{
			_listeResultat = value;
		}
		
		[Bindable] 
		public function get listeResultat():ArrayCollection
		{
			return _listeResultat;
		}
		
		public function get NB_ROWS():Number
		{
			return _NB_ROWS;
		}
		
		public function set NB_ROWS(value:Number):void
		{
			_NB_ROWS = value;
		}
	}
}