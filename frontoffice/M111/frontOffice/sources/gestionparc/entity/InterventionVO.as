package gestionparc.entity
{

	[Bindable]
	public class InterventionVO
	{
		public var idIntervention:int=-1;
		public var numIntervention:String="";
		public var dateIntervention:Date=null;
		public var idTypeIntervention:int=0;
		public var typeIntervention:String=""; //pour le dataField
		public var libelle:String="";
		public var savedByUser:String="";
		public var horaire:int=0; // 0 = matin | 1 = après midi
		public var prix:Number=0;
		public var description:String="";
		public var isClose:int=0;
	}
}
