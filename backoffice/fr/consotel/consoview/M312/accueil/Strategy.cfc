<!--- =========================================================================
Name: AboStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="Strategy" hint="" extends="AbstractStrategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="Strategy" displayname="Strategy init()" hint="Initialize the AboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="integer ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfreturn queryNew("null","VarChar")>
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfreturn "">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfreturn "">
	</cffunction>
</cfcomponent>