package event
{
	import flash.events.Event;
	
	public class LigneEvent extends Event
	{
		public static const LIGNES_PAR_SEGMENT_RETURNED:String = "ligne_par_segment";
		
		public function LigneEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}