package composants.util
{
	import datalert.vo.ChartDataVo;
	
	import mx.collections.ArrayCollection;

	public class ChartDataUtil
	{
		public function ChartDataUtil()
		{
		}
		
		/**Classe servant à faire les calculs avant affichage des graphiques ChartData
		*/
		
		public static function toFullListPerHour(listData:ArrayCollection):ArrayCollection
		{
			var i:int = 0;
			var zeroList:ArrayCollection = new ArrayCollection();
			
			while (i < 24)//on remplit une liste de données vide
			{
				var item:ChartDataVo = new ChartDataVo();
				item.HOUR 			= (i < 10) ? "0" + i.toString() : i.toString();
				item.DATADOMESTIC 	= 0;
				item.DATAROAMING 	= 0;
				item.DATAWIFI 		= 0;
				
				zeroList.addItem(item);
				i++;
			}
			i = 0;
			while(i < listData.length)//on insère les données disponibles parmis les données vides
			{
				zeroList[listData[i].HOUR].HOUR = listData[i].HOUR;
				zeroList[listData[i].HOUR].DATADOMESTIC = listData[i].DATADOMESTIC;
				zeroList[listData[i].HOUR].DATAROAMING = listData[i].DATAROAMING;
				zeroList[listData[i].HOUR].DATAWIFI = listData[i].DATAWIFI;
				i++;
			}
			
			return zeroList;// on renvoie les données disponibles + des 0 là ou les données manquent
		}
		
		public static function toFullListPerDay(listData:ArrayCollection, dayStart:Date, dayEnd:Date):ArrayCollection
		{
			var i:int = 0;
			var _day:Date = dayStart;
			var zeroList:ArrayCollection = new ArrayCollection();
			
			while(DateFunction.dateDiff("d", _day, dayEnd) <= 0)
			{
				var item:ChartDataVo = new ChartDataVo();
				
				item.DAY = (_day.date < 10) ? "0" + _day.date.toString() : _day.date.toString();
				item.DATADOMESTIC = 0;
				item.DATAROAMING = 0;
				item.DATAWIFI = 0;
				if(i < listData.length)
				{
					if(listData[i].DAY == item.DAY)
					{
						item.DATADOMESTIC = listData[i].DATADOMESTIC;
						item.DATAROAMING = listData[i].DATAROAMING;
						item.DATAWIFI = listData[i].DATAWIFI;
						i++;
					}
				}
				zeroList.addItem(item);
				_day = addDay(_day, 1);//_day++;
			}
			
			return zeroList;// on renvoie les données disponibles + des 0 là ou les données manquent
		}
		
		public static function addDay(date:Date,i:int):Date
		{		
			return new Date(date.fullYear, date.month, date.date + i);
		}
	}
}