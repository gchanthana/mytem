<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.handler.IPostProcess"
hint="Interface qui spécifie les méthodes pour un traitement de mise à jour utilisé avec la stratégie : fr.consotel.api.ibis.publisher.delivery.Evenement
Ce type de traitement est appelée uniquement la méthode getStatus() de l'évènement IReportingEvent du reporting n'est pas égal à FAILURE() ou EXCEPTION()">
	<cffunction name="handleUpdateEvent" returntype="void" hint="Méthode appelée par la stratégie fr.consotel.api.ibis.publisher.delivery.Evenement">
		<cfargument name="updateEvent" type="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent" required="true"
		hint="Evènement fourni par la stratégie Evenement et qui contient les infos utilisées pour le traitement de mise à jour
		La valeur retournée par updateEvent.getStatus() est toujours différente de updateEvent.FAILURE() et updateEvent.EXCEPTION()">
	</cffunction>
</cfinterface>