package gestionparc.entity
{
	[RemoteClass(alias="fr.consotel.consoview.M111.Vo.ContratVo")]

	[Bindable]
	public class ContratVO
	{
		public var idContrat:int=-1;
		public var refContrat:String="";

		public var idTypeContrat:int=0;
		public var typeContrat:String="";

		public var idUserCreate:int=0;
		public var idUserModif:int=0;
		public var idUserResi:int=0;

		public var nomUserCreate:String="";
		public var nomUserModif:String="";
		public var nomUserResi:String="";

		public var dateCreate:Date;
		public var dateDebut:Date;
		public var dateEcheance:Date;
		public var dateEligibilite:Date;
		public var dateModif:Date;
		public var dateRenouvellement:Date;
		public var dateResi:Date;
		public var dateResiliation:Date;
		public var dateSignature:Date;

		public var commentaires:String="";

		public var dureeContrat:int=0;
		public var dureeEligibilite:int=0;

		public var tarifMensuel:Number=0;
		public var tarif:Number=0;
		private var _tarifAnnuel:Number=0;

		public var preavis:int=0;
		public var taciteReconduction:int=0;

		public var idRevendeur:int=0;
		public var revendeur:String=""; // pour le dataField

		public function get tarifAnnuel():Number
		{
			_tarifAnnuel=(tarifMensuel * 12);
			return _tarifAnnuel;
		}
	}
}
