package composants.DroitModule
{
	import composants.DroitModule.event.GestionLoginEvent;
	import composants.DroitModule.service.LoginService;
	
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.RadioButton;
	import mx.core.Repeater;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class CompDroitModuleImpl extends VBox
	{
		private var _idRacine: int = 0;
		private var _idUser  : int = 0;
		private var _code_app:int = 0;
		public var loginService : LoginService= new LoginService();
		public var rp:Repeater
		
	/* GETTER SETTER*/
		
		public function get idRacine():int
		{
			return _idRacine
		}
		public function set idRacine(obj:int):void
		{
			_idRacine = obj
		}
		public function get code_app():int
		{
			return _code_app
		}
		public function set code_app(obj:int):void
		{
			_code_app= obj
		}
		public function get idUser():int
		{
			return _idUser
		}
		public function set idUser(obj:int):void
		{
			_idUser = obj
		}
	/* CONSTRUCTEUR */	
		public function CompDroitModuleImpl():void
		{
		}
	/* FONCTION */
		public function refresh():void
		{
			getData()
		}
		public function getData(e:FlexEvent=null):void
		{
			loginService.ListeModuleRacine(idRacine,code_app)	
		}
		private function getListeModuleEcriture():String
		{
			var str:String = ""
			for each(var obj:Object in loginService.arrListeModule)
			{
				if(obj.ACCESS_RACINE == 2)
					str+=","+obj.MODULE_CONSOTELID
			}
			str = str.substr(1,str.length);
			if(str.length == 0)
				str = "0"
			return str
		}
		private function getListeModuleLecture():String
		{
			var str:String = ""
			for each(var obj:Object in loginService.arrListeModule)
			{
				if(obj.ACCESS_RACINE == 1)
					str+=","+obj.MODULE_CONSOTELID
			}
			str = str.substr(1,str.length);
			if(str.length == 0)
				str = "0"
			return str
		}
		public function sauvegarderDroit():void
		{
			var listeModEcriture:String = getListeModuleEcriture()
			var listeModLecture:String  = getListeModuleLecture()
			loginService.addEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_COMPLETE,save_handler)
			loginService.addEventListener(GestionLoginEvent.SAUVEGARDER_DROIT_ERROR,error_handler)
			loginService.SauvegarderDroitracine(idRacine,listeModEcriture,listeModLecture);
		}
		private function save_handler(e:GestionLoginEvent):void
		{
			Alert.show("Sauvegarde réussie avec succés","",Alert.OK,this);
			PopUpManager.removePopUp(this);
		}
		private function error_handler(e:GestionLoginEvent):void
		{
			Alert.show("Impossible de sauvegarder les droits","Une erreur est survenue",Alert.OK,this);
		}
				
	/* HANDLER */
		protected function radiobutton_clickHandler(event:MouseEvent):void
		{
			if(event != null)
			{
				for each(var obj:Object in loginService.arrListeModule)
				{
					if(obj.MODULE_CONSOTELID == parseInt((event.target as RadioButton).groupName))
					{
						obj.ACCESS_RACINE = (event.target as RadioButton).value
						break
					}
				}
				
			}
		}
	}
}