package univers.parametres.gestionnaire
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestionnaire.classemetier.PoolGestionnaire;
	
	public class CreerPoolGestionnaire extends CreerPoolGestionnaireIHM
	{
		private var _input_libelle : String;
		private var _input_code_interne: String;
		private var _input_commentaire: String;
		private var gestionPool : GererPoolsGestionnaires;
		public function CreerPoolGestionnaire(gestionPool : GererPoolsGestionnaires)
		{
			super();
			this.gestionPool = gestionPool;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(evt : FlexEvent):void
		{
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer_handler);
			bt_valider.addEventListener(MouseEvent.CLICK,valider_handler);
			chekRevendeur.addEventListener(Event.CHANGE,chekRevendeur_handler);
			comboRevendeur.enabled=false;
		}
		
		private function chekRevendeur_handler(evt : Event):void
		{	
			if(chekRevendeur.selected==true)
			{
				var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getlisteRevendeursLibres",
																		process_getPoolRevendeur);
																						
				var boolSegmentFixe:Number = 1;
				var boolSegmentMobile:Number = 1;				
				RemoteObjectUtil.callService(op,boolSegmentFixe,boolSegmentMobile);
			}
			else
			{
				comboRevendeur.enabled=false;
			}
		}
		private function erreur (evt : FaultEvent):void
		{
			Alert.show("Erreur"+evt.toString());
		}
		private function process_getPoolRevendeur(evt : ResultEvent):void
		{
			var liste:ArrayCollection = evt.result as ArrayCollection;
			comboRevendeur.rowCount = liste.length;
			comboRevendeur.enabled = true;
			comboRevendeur.dataProvider = evt.result;
		}
		private function valider_handler(evt : MouseEvent):void
		{	
			var newPool : PoolGestionnaire = new PoolGestionnaire();
			newPool.codeInterne_pool = input_code_interne.text;
			newPool.commentaire_pool = input_commentaire.text;
			newPool.libelle_pool = input_libelle.text;
			if(comboRevendeur.enabled && comboRevendeur.selectedIndex != -1 && chekRevendeur.selected)
			{
				newPool.IDPoolRevendeur = comboRevendeur.selectedItem.IDFOURNISSEUR;	
			}
			
			newPool.add_pool_gestionnaire();
			
			
			
			gestionPool.init_dg_pool_gestionnaire(); // Rafraichir la liste
			PopUpManager.removePopUp(this);
		}
		private function fermer_handler(evt : MouseEvent):void
		{
			PopUpManager.removePopUp(this);
			//Alert.show("--->"+UniversManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		
	}
}