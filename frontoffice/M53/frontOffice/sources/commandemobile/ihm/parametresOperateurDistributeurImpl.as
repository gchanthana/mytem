package commandemobile.ihm
{
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	public class parametresOperateurDistributeurImpl extends TitleWindow
	{
		
		private const VALIDER	:String = "btnValiderClicked";
		private const FERMER	:String = "btnFermerClicked";
		
		public function parametresOperateurDistributeurImpl()
		{
		}
		
		protected function _closeHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnValiderClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function btnFermerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}

		protected function creationCompleteHandler(nbIndex:int):void
		{
			
		}

	}
}