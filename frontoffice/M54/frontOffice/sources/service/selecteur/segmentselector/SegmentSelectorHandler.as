package service.selecteur.segmentselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class SegmentSelectorHandler
	{
		private var _model: SegmentSelectorModel;
		
		public function SegmentSelectorHandler(_model:SegmentSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValueSegmentHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValues(evt.result as ArrayCollection);
			}
		}	
	}
}