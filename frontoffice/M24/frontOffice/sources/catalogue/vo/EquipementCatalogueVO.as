package catalogue.vo
{
	[Bindable]
	public class EquipementCatalogueVO
	{
		private var _TYPE_CREATION:Number = 1;
		private var _IDEQUIPEMENT:Number;
		private var _IDEQUIPFOURNISSEUR:Number; // Utilisée dans le cat client pour la gestion des types de cmd
		private var _REFERENCE:String = '';
		private var _TYPE:String;
		private var _CLASSE:String;
		private var _ORIGINE:String;
		private var _MODELE:String;
		private var _NBRECORD:Number;
		private var _MARQUE:String;
		private var _CODE_INTERNE:String;
		
		private var _DATEVALIDITE:Date = null;
		private var _DELAIALERT:Number  = 0;
		private var _SUSPENDU:Number = 0;
		
		private var _DATEVALIDITE_CLT:Date = null;
		private var _SUSPENDU_CLT:Number = 0;
		
		private var _PATH_LOW:String = '';
		private var _PATH_THUMB:String = '';
		private var _imageByUser:Number = 0;
		
		/**
		 * _ADDINCATCONSTR
		 * prend 0 ou 1 : Utiliser dans l'ajout d'un équipement au catalogue districbuteur
		 * pour indiquer si on doit aussi l'ajouter au catalogue constructeur
		 * Par defaut à oui
		 */
		private var _ADDINCATCONSTR:Number = 1; 
		private var _REFCONSTRUCTEUR:String = '';
		private var _REFDISTRIBUTEUR:String = '';
		
		private var _REFCLIENT:String = '';
		
		
		private var _SELECTED:Boolean=false;
		
		private var _IDCONSTRUCTEUR:Number;
		private var _NIVEAU:Number;
		private var _IDREVENDEUR:Number;
		private var _IDTYPE:Number;
		private var _IDCLASSE:Number;
		private var _PRIX_CATALOGUE:String;
	
		private var _PRIX_1:String;
		private var _PRIX_2:String;
		private var _PRIX_3:String;
		private var _PRIX_4:String;
		private var _PRIX_5:String;
		private var _PRIX_6:String;
		private var _PRIX_7:String;
		private var _PRIX_8:String;
		private var _PRIX_9:Number; //Utiliser dans les details de l'équipement
		
		private var _COMMENT:String='';
		
		public function EquipementCatalogueVO()
		{
		}
		public function set REFERENCE(value:String):void
		{
			_REFERENCE = value;
		}

		public function get REFERENCE():String
		{
			return _REFERENCE;
		}
		
		public function set TYPE(value:String):void
		{
			_TYPE = value;
		}

		public function get TYPE():String
		{
			return _TYPE;
		}
		public function set CLASSE(value:String):void
		{
			_CLASSE = value;
		}

		public function get CLASSE():String
		{
			return _CLASSE;
		}
		public function set ORIGINE(value:String):void
		{
			_ORIGINE = value;
		}

		public function get ORIGINE():String
		{
			return _ORIGINE;
		}
		public function set MODELE(value:String):void
		{
			_MODELE = value;
		}

		public function get MODELE():String
		{
			return _MODELE;
		}
		public function set DATEVALIDITE_CLT(value:Date):void
		{
			_DATEVALIDITE_CLT = value;
		}
		
		public function get DATEVALIDITE_CLT():Date
		{
			return _DATEVALIDITE_CLT;
		}
		
		public function set SUSPENDU_CLT(value:Number):void
		{
			_SUSPENDU_CLT = value;
		}
		
		public function get SUSPENDU_CLT():Number
		{
			return _SUSPENDU_CLT;
		}
		
		public function set DATEVALIDITE(value:Date):void
		{
			_DATEVALIDITE = value;
		}
		
		public function get DATEVALIDITE():Date
		{
			return _DATEVALIDITE;
		}
		
		public function set SUSPENDU(value:Number):void
		{
			_SUSPENDU = value;
		}
		
		public function get SUSPENDU():Number
		{
			return _SUSPENDU;
		}
		
		public function get DELAIALERT():Number
		{
			return _DELAIALERT;
		}
		
		public function set DELAIALERT(value:Number):void
		{
			_DELAIALERT = value;
		}

		public function set IDEQUIPEMENT(value:Number):void
		{
			_IDEQUIPEMENT = value;
		}
		public function set IDEQUIPFOURNISSEUR(value:Number):void
		{
			_IDEQUIPFOURNISSEUR = value;
		}
		public function get IDEQUIPFOURNISSEUR():Number
		{
			return _IDEQUIPFOURNISSEUR;
		}

		public function get NIVEAU():Number
		{
			return _NIVEAU;
		}

		public function set NIVEAU(value:Number):void
		{
			_NIVEAU = value;
		}
		public function get IDEQUIPEMENT():Number
		{
			return _IDEQUIPEMENT;
		}

		public function set NBRECORD(value:Number):void
		{
			_NBRECORD = value;
		}

		public function get NBRECORD():Number
		{
			return _NBRECORD;
		}

		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}

		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}

		public function set IDCONSTRUCTEUR(value:Number):void
		{
			_IDCONSTRUCTEUR = value;
		}

		public function get IDCONSTRUCTEUR():Number
		{
			return _IDCONSTRUCTEUR;
		}
		public function set IDTYPE(value:Number):void
		{
			_IDTYPE = value;
		}

		public function get IDTYPE():Number
		{
			return _IDTYPE;
		}
		public function set IDCLASSE(value:Number):void
		{
			_IDCLASSE = value;
		}

		public function get IDCLASSE():Number
		{
			return _IDCLASSE;
		}

		public function set MARQUE(value:String):void
		{
			_MARQUE = value;
		}

		public function get MARQUE():String
		{
			return _MARQUE;
		}

		public function set REFCONSTRUCTEUR(value:String):void
		{
			_REFCONSTRUCTEUR = value;
		}

		public function get REFCONSTRUCTEUR():String
		{
			return _REFCONSTRUCTEUR;
		}
		public function set REFCLIENT(value:String):void
		{
			_REFCLIENT = value;
		}

		public function get REFCLIENT():String
		{
			return _REFCLIENT;
		}
		public function set REFDISTRIBUTEUR(value:String):void
		{
			_REFDISTRIBUTEUR = value;
		}

		public function get REFDISTRIBUTEUR():String
		{
			return _REFDISTRIBUTEUR;
		}
		
		public function set PRIX_9(value:Number):void
		{
			_PRIX_9 = value;
		}

		public function get PRIX_9():Number
		{
			return _PRIX_9;
		}



		public function set IDREVENDEUR(value:Number):void
		{
			_IDREVENDEUR = value;
		}

		public function get IDREVENDEUR():Number
		{
			return _IDREVENDEUR;
		}

		public function set ADDINCATCONSTR(value:Number):void
		{
			_ADDINCATCONSTR = value;
		}

		public function get ADDINCATCONSTR():Number
		{
			return _ADDINCATCONSTR;
		}
	

		public function set PRIX_1(value:String):void
		{
			_PRIX_1 = value;
		}

		public function get PRIX_1():String
		{
			return _PRIX_1;
		}
		public function set PRIX_2(value:String):void
		{
			_PRIX_2 = value;
		}

		public function get PRIX_2():String
		{
			return _PRIX_2;
		}
		public function set PRIX_3(value:String):void
		{
			_PRIX_3 = value;
		}

		public function get PRIX_3():String
		{
			return _PRIX_3;
		}
		public function set PRIX_4(value:String):void
		{
			_PRIX_4 = value;
		}

		public function get PRIX_4():String
		{
			return _PRIX_4;
		}
		public function set PRIX_5(value:String):void
		{
			_PRIX_5 = value;
		}

		public function get PRIX_5():String
		{
			return _PRIX_5;
		}
		public function set PRIX_6(value:String):void
		{
			_PRIX_6 = value;
		}

		public function get PRIX_6():String
		{
			return _PRIX_6;
		}
		public function set PRIX_7(value:String):void
		{
			_PRIX_7 = value;
		}

		public function get PRIX_7():String
		{
			return _PRIX_7;
		}
		public function set PRIX_8(value:String):void
		{
			_PRIX_8 = value;
		}

		public function get PRIX_8():String
		{
			return _PRIX_8;
		}

		public function set PRIX_CATALOGUE(value:String):void
		{
			_PRIX_CATALOGUE = value;
		}

		public function get PRIX_CATALOGUE():String
		{
			return _PRIX_CATALOGUE;
		}
		public function set CODE_INTERNE(value:String):void
		{
			_CODE_INTERNE = value;
		}

		public function get CODE_INTERNE():String
		{
			return _CODE_INTERNE;
		}
		

		public function set TYPE_CREATION(value:Number):void
		{
			_TYPE_CREATION = value;
		}

		public function get TYPE_CREATION():Number
		{
			return _TYPE_CREATION;
		}
		
		public function set PATH_LOW(value:String):void
		{
			_PATH_LOW = "/NoImage.png";
			if(value)
			{
				_PATH_LOW = value;
			}
		}
		
		public function get PATH_LOW():String
		{
			return _PATH_LOW;
		}
		
		public function set PATH_THUMB(value:String):void
		{ 
			_PATH_THUMB = "/NoImage.png";
			if(value)
			{
				_PATH_THUMB = value;
			}
			
		}
		
		public function get PATH_THUMB():String
		{
			return _PATH_THUMB;
		}public function get COMMENT():String { return _COMMENT; }
		
		public function set COMMENT(value:String):void
		{
			if (_COMMENT == value)
				return;
			_COMMENT = value;
		}
		
		public function get imageByUser():Number { return _imageByUser; }
		
		public function set imageByUser(value:Number):void
		{
			if (_imageByUser == value)
				return;
			_imageByUser = value;
		}
	}
}