package univers.inventaire.lignesetservices.vo
{
	 
	[Bindable]
	public class LibellePerso
	{
		public var libelle: String= "Sans titre";
		public var libelleName: String= "";
		public var idLibelle: Number= 0;
		public var idGroupeClient: Number= 0;
	}
}