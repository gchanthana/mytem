package univers.parametres.poolgestionnaire.ihm
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	
	import univers.parametres.poolgestionnaire.event.GestionPoolEvent;
	import univers.parametres.poolgestionnaire.service.CompteOperateurService;
	import univers.parametres.poolgestionnaire.util.CompteHelper;
	import univers.parametres.poolgestionnaire.vo.CompteOperateur;
	
	[Bindable]
	public class ListeCompteOperateurCompte extends VBox
	{
		public var compteOPerateurService : CompteOperateurService= new CompteOperateurService();
		
		public var groupeIndex : int;
		
		//COMPONANT
		public var txtFiltre :TextInput;
		public var dg_compteOP: DataGrid;
		public var cbAll : CheckBox;	
		public var combo_operateur : ComboBox;	
		
		public function ListeCompteOperateurCompte()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			
		}
		public function init(evt : FlexEvent):void
		{
			compteOPerateurService.fournirListeOperateursClient();
		}
		public function onItemChanged(item : Object):void
		{
			var action : Number; // 1 pour affecter et 0 pour desaffecter
			var compteHelper : CompteHelper = new CompteHelper();
			var arrIDSousCompte : Array = new Array();
			
			if(item.boolSelected)
			{
				action = 1;
			}
			else
			{
				action = 0;
			}
			//Si c'est un compte on selectionne tous les sous comptes : 
			if(item.isCompteMaitre)
			{
				arrIDSousCompte = compteHelper.getIDSousCompteOfCompteNotFormated(item as CompteOperateur,compteOPerateurService.col_compteOP_compte,action==1)
			}
			//Sinon on ne prend que le compte
			else
			{
				arrIDSousCompte.push(item.idSousCompteOP);
			}
			if(action==1)
			{
				compteOPerateurService.nbCompteOpSelected_Compte = compteOPerateurService.nbCompteOpSelected_Compte + arrIDSousCompte.length
			}
			else
			{
				compteOPerateurService.nbCompteOpSelected_Compte = compteOPerateurService.nbCompteOpSelected_Compte - arrIDSousCompte.length
			}
			compteHelper.formatListeCompte(compteOPerateurService.col_compteOP_compte);
			
			compteOPerateurService.updateXCompteOpeCompte(groupeIndex,arrIDSousCompte,action);
		}
		
		public function cbAllHandler(evt : Event):void
		{
			var action : Number; // 1 pour affecter et 0 pour desaffecter
			if(cbAll.selected)
			{
				action = 1;
			}
			else
			{
				action = 0;
			}
			
			var colCompte : ArrayCollection = compteOPerateurService.col_compteOP_compte;
			var arr : Array = new Array();
			for(var a:int=0;a<colCompte.source.length;a++)
			{
				var curentObj : CompteOperateur = colCompte.source[a] as CompteOperateur;
				if(!curentObj.isCompteMaitre)
				{
					arr.push(curentObj.idSousCompteOP);
				}
				curentObj.boolSelected = cbAll.selected;
				colCompte.itemUpdated(curentObj);
			}
			if(action==1)
			{
				compteOPerateurService.nbCompteOpSelected_Compte =  compteOPerateurService.col_compteOP_compte.source.length
			}
			else
			{
				compteOPerateurService.nbCompteOpSelected_Compte = 0;
			}
			compteOPerateurService.updateXCompteOpeCompte(groupeIndex,arr,action);
		}
		protected function filtreHandler(e:Event=null):void{
			compteOPerateurService.col_compteOP_compte.filterFunction= filtreGrid;
			compteOPerateurService.col_compteOP_compte.refresh();
		}
		
		private function filtreGrid(item:Object):Boolean{
			
			if (((item.libelle as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 )return true;
			else return false;
		}
		public function combo_operateur_handler(evt : Event):void
		{
			cbAll.selected = false;
			
			if(combo_operateur.selectedIndex!=-1)
			{
				compteOPerateurService.addEventListener(GestionPoolEvent.LISTE_COMPTE_OPERATEUR_COMPLETE,getCompteOpe_handler);
				compteOPerateurService.getCompteOpeCompte(groupeIndex,combo_operateur.selectedItem.OPERATEURID);
				combo_operateur.errorString = "";
			}
			else
			{
				combo_operateur.errorString = "Sélectionnez un opérateur";
				compteOPerateurService.col_compteOP_compte = null;
			}
		}
		private function getCompteOpe_handler(evt : GestionPoolEvent):void
		{
			filtreHandler();
		}
	}
}