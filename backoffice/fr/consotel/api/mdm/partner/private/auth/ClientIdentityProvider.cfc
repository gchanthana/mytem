<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.auth.ClientIdentityProvider"
hint="Implémentation abstraite utilisée par MdmServiceProvider pour vérifier l'identité et l'authentification d'un utilisateur accédant à l'API">
	<cffunction access="package" name="getIdentityProvider" returntype="fr.consotel.api.mdm.partner.private.auth.ClientIdentityProvider" hint="Retourne une instance de cette classe">
		<cfreturn THIS>
	</cffunction>

	<cffunction access="package" name="getAuthIdentity" returntype="Struct" hint="Retourne les infos d'identification et d'authentification de l'utilisateur
	La structure retournée est à utiliser comme valeur du paramètre authIdentity de la méthode assertAuthIdentity()
	Les clés contenues dans la structure retournée sont fonction de l'implémentation de cette classe et peuvent dépendre de l'implémentation MdmAuthManager concernée">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Fournie à l'implémentation MdmServiceProvider comme infos d'authentification">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée par cette classe">
	</cffunction>

	<cffunction access="package" name="assertAuthIdentity" returntype="boolean" hint="Retourne TRUE si les infos d'authentification authIdentity sont validées et FALSE sinon
	Cette méthode est appelée par une implémentation MdmServiceProvider pour vérifier la validité des infos d'authentification qui lui sont fournies">
		<cfargument name="authIdentity" type="Struct" required="true" hint="Valeur retournée par la méthode getAuthIdentity() de cette implémentation">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée par cette classe">
	</cffunction>
</cfcomponent>