package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class HasExtAssociated implements IRuleDisplayItem
	{
		public function HasExtAssociated()
		{
		}

		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			
			if (ligne !=null && ligne.IDEXTENSION  > 0)
				return true
			else
				return false
		}
	}
}