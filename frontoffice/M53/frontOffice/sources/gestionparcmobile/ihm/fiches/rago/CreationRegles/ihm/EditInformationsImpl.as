package univers.parametres.rago.CreationRegles.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	
	import univers.parametres.rago.CreationRegles.event.WizardEvent;
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	
	
	[Event(name="validerCreationRegle",type="univers.parametres.rago.wizard.event.WizardEvent")]
	[Event(name="validerCreationEtExecuterRegle",type="univers.parametres.rago.wizard.event.WizardEvent")]
	
	[Bindable]
	public class EditInformationsImpl extends MainCreateRegleImpl
	{

//VARIABLES GLOBALES----

		public var _editInfowizzard:Wizzard;
		
		public var txtLibelle:TextInput;
		public var txtCommentaire:TextArea;
		
		public var txtLignesSuivent:String = "";
		
		private var _libellesExceptions:ArrayCollection;
		
		public var _cboxOpe:Object;
		public var _cboxClient:Object;
		
//FIN VARIABLES GLOBALES----

//PROPRIETEES PUBLICS----
		
		public function set libellesExceptions(value:ArrayCollection):void
		{ _libellesExceptions = value; }

		public function get libellesExceptions():ArrayCollection
		{ return _libellesExceptions; }
		
//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur
		public function EditInformationsImpl()
		{ addEventListener(FlexEvent.SHOW, _localeShowHandler); }

//FIN METHODES PUBLICS----

//METHODES PROTECTED----
		
		protected function prevaliderLeFormulaire():void
		{  }
		
		protected function initEditInfoIHM(event:Event):void
		{  }
		
		//Formateur boolean en string
		protected function zFormatBooleanToString(value:Boolean):String
		{  
			var converted:String;
			if(value)
			{ converted = "OUI"; }
			else
			{ converted = "NON"; }
			return converted;
		}

		//Format la date sous le type jj/mm/aaaa
		protected function formatDate(dateToFormat:Date):String
		{
			if(dateToFormat == null)
			{ return "";}
			var realDateInString:String = "";
			if(dateToFormat!=null)
			{
				var realDate:Object = new Object();
			
				realDate = {date:dateToFormat.date + "/" , month:zformateMonth(dateToFormat.month) + "/" , fullyear:dateToFormat.fullYear};
			
				var dayInString:String = realDate.date.toString();
				var monthInString:String = realDate.month.toString();
				var yearInString:String = realDate.fullyear.toString();
				
				if(dayInString.length == 1) { dayInString = "0" + dayInString; }
				
				realDateInString = dayInString + monthInString + yearInString;
			}
			return realDateInString;
		}
		
		//Format type de regle 
		protected function formatTypeRegle(typeRegle:String):String
		{
			var realType:String;
			if(typeRegle == "OPE")
			{ realType = "OPERATEUR"; }
			else
			{ realType = "ANNUAIRE"; }
			return realType;
		}
		
		protected function commentChangeHandler(event:Event):void
		{ _editInfowizzard.myRegle.REGLE_COMMENT = txtCommentaire.text; }
		
		protected function valider():void
		{
			//Alert.show("Voulez - vous executer la règle?","!",Alert.YES|Alert.CANCEL|Alert.NO,this,closeHandlerFunction);
			ConsoviewAlert.afficherOKImage("Voulez - vous executer la règle?",this);
		}
		
		protected function closeHandlerFunction(event:CloseEvent):void
		{
			mappData();
			
			if(event.detail == Alert.YES)			
			{ dispatchEvent(new WizardEvent(_editInfowizzard.myRegle,WizardEvent.VALIDER_CREATION_ET_EXECUTER_REGLE,true)); }
			else if(event.detail == Alert.NO)
			{ dispatchEvent(new WizardEvent(_editInfowizzard.myRegle,WizardEvent.VALIDER_CREATION_REGLE,true)); }
		}
		
		protected function mappData():void
		{
			_editInfowizzard.myRegle.REGLE_NOM = txtLibelle.text;
			_editInfowizzard.myRegle.REGLE_COMMENT = txtCommentaire.text;	
		}

		protected function _localeShowHandler(event:FlexEvent):void
		{
			if(_editInfowizzard.myRegle)
			{
				if(_editInfowizzard.myRegle.BOOL_SUIVRE_AUTO)
				{ txtLignesSuivent = "Les lignes affectées suivront cette règle."; }
				else
				{ txtLignesSuivent = "Les lignes affectées ne suivront pas cette règle."; }
			}
		}
		
//FIN METHODES PROTECTED----		

//METHODES PRIVATE----
		
		//Formate le mois 0->01(janvier), 1->02(février), ..., 11->12(décembre)
		private function zformateMonth(monthToFormate:int):String
		{
			var month:String = null;
			
			switch (monthToFormate)
			{
					case 0:month = "01";break;
					case 1:month = "02";break;
					case 2:month = "03";break;
					case 3:month = "04";break;
					case 4:month = "05";break;
					case 5:month = "06";break;
					case 6:month = "07";break;
					case 7:month = "08";break;
					case 8:month = "09";break;
					case 9:month = "10";break;
					case 10:month = "11";break;
					case 11:month = "12";break;
			}
			
			return month;
		}

//FIN METHODES PRIVATE----
		
	}
}