package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;

	public class RuleDissocierEqpForExt implements IRuleDisplayItem
	{
		public function RuleDissocierEqpForExt()
		{
		}

		public function isDisplay():Boolean
		{
			var r3:IRuleDisplayItem=new HasEqpAssociated();
			
			if (r3.isDisplay())
				return true;
			else
				return false;
		}
	}
}
