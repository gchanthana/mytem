<cfcomponent colddoc:abstract="true"
			implements="portail.class.specialClass.InterfaceComposant" 
			displayname="Classe abstraite qui hérite de InterfaceComposant" 
			hint="Contient des méthodes utilisés dans les classes de Composants"
			output="false">

	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>
	
	
	<!--- ISOLE LE PARAMATRE UUID DE L'URL --->
   	<cffunction name="createURLWithUUID" access="public" output="false" returnType="string"
				displayname="méthode implémenté"
				hint="Crée une struct et stocke les paramètres URL dans une structure dans la structure de session SESSION.UUID">
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passés dans l'URL" >
		
		<cfset tab = structNew() >
		
		<!--- reinitialisation d''une structure avant son ajout ds la structure sessionUUUID --->
		<cfloop item="x" collection="#context#" >
			<cfset tab[x] = #context[x]#>
		</cfloop>
		
		<cfset ref = createUUID()> <!--- create registrationKey --->
		<cfset struct['#ref#'] = #tab#>
		<cfset structAppend(SESSION.UUID,struct)> <!--- saveQueryString --->
		<cfset stringUrl = "index.cfm?"&#ref# >
		
		<cfreturn stringUrl>
	</cffunction>

	
	<!--- ISOLE LE PARAMATRE UUID DE L'URL --->
   	<cffunction name="createURLWithLienMail" access="public" output="false" returnType="any"
				displayname="méthode implémenté"
				hint="Crée une struct et stocke les paramètres URL dans une structure dont le nom est l'UUID du lien recu dans le mail de 'premiere connexion'">
		
		<cfargument name="UUID" required="true" type="string" displayname="" hint="Structure contenant les données passés dans l'URL" >
		<cfargument name="sous_tete" required="true" type="string" displayname="" hint="Structure contenant les données passés dans l'URL" >
		
		<cfset tab = {idPage = "0", sendSMSAfterEmail = true, sous_tete = "#sous_tete#" }>
		
		<cfset ref = UUID> <!--- create registrationKey --->
		<cfset struct['#ref#'] = #tab#>
		<cfset structAppend(SESSION.UUID,struct)> <!--- saveQueryString --->
		
		<cfreturn struct>
	</cffunction>
	
	
	<!--- STOCKE LES PARAMS URL DANS UNE STRUCTURE --->
   	<cffunction name="createRefURL" access="public" output="false" returnType="string"
				displayname="méthode implémenté"
				hint="Stocke les paramètres URL dans une structure dans la structure de session SESSION.UUID.">
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passés dans l'URL" >
		
		<cfset tab = structNew() >
		
		<!--- reinitialisation d''une structure avant son ajout ds la structure sessionUUUID --->
		<cfloop item="x" collection="#context#" >
			<cfset tab[x] = #context[x]#>
		</cfloop>
		
		<cfset ref = createUUID()> <!--- create registrationKey --->
		<cfset struct['#ref#'] = #tab#>
		<cfset structAppend(SESSION.UUID,struct)> <!--- saveQueryString --->

		<cfset stringUrl = "index.cfm?"&#ref# >
		
		<cfreturn stringUrl>
	</cffunction>
	
	
	<!--- CREATION DUNE STRUC ET RECUPERATION DES DONNEES PASSEES EN URL DANS CETTE STRUCTURE --->
	<cffunction name="getInfoSructURL" access="public" output="false" returnType="struct">
		<cfargument name="context" required="false" type="struct">
		
		<cfset refParam = structNew()>
		<cfif NOT structIsEmpty(context)>
			<cfloop item="x" collection="#context#" >
				<cfset ref = #x#>
			</cfloop>
			<cfif structKeyExists(SESSION.UUID,"#ref#") >
				<cfset refParam = #SESSION.UUID['#ref#']# >
			<cfelse>
				<!--- aller à index sans url visible --->
				<cflocation url="index.cfm" addtoken="no">
			</cfif>
		</cfif>
		
		<cfreturn refParam >
	</cffunction>
	
	
	<!--- ALLER À LA PAGE SPÉCIFIÉ DANS LES PARAMETRES --->
	<!--- puis on formate lurl avec la clé de la structure ref --->
 	<cffunction name="goToPage" access="public" output="false" returnType="void" 
				displayname="Méthode implémenté" 
				hint="Redirige vers le numéro de page spécifié en paramètre idPage">
		<cfargument name="idPage" required="true" type="numeric" displayname="" hint="ID de la page">
		<cfargument name="context" required="true" type="struct" displayname="" hint="Structure contenant les données passé dans l'URL">
		
		<cfset context.idPage = idPage >
		<!--- créer une structure ds laquelle on met les parametre a transmettre à l'autre page --->
		<cfset tab = structNew() >
		
		<!--- initialisation de la nouvelle structure par les parametres recupérés, via structure CONTEXT --->
		<cfloop item="x" collection="#context#" >
			<cfset tab[x] = #context[x]#>
		</cfloop>
		
		<cfset ref = createUUID()>
		<cfset struct['#ref#'] = #tab#>
		<cfset structAppend(SESSION.UUID,struct)>

		<cflocation url="index.cfm?#ref#" addtoken="no">
	</cffunction>

	
</cfcomponent>