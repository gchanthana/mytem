package gestionparc.entity
{

	[Bindable]
	public class LiaisonVO
	{
		// Equipement
		public var idTypeEquipement:int=0;
		public var typeEquipement:String="";

		// SIM
		public var idSim:int=0;
		public var idFabricantSim:int=0;
		public var idRevendeurSim:int=0;
		public var revendeurSim:String="";
		public var numSim:String="";
		public var operateurId:int=0;
		public var operateur:String="";

		// terminal
		public var idTerm:int=0;
		public var idFabricantTerm:int=0;
		public var idRevendeurTerm:int=0;
		public var revendeurTerm:String="";
		public var marqueTerm:String="";
		public var imei:String="";
		public var modele:String="";
		public var idCategorie:int=0;
		public var libelleCategorie:String="";

		// Ligne
		public var idLigne:int=0; // idSousTete
		public var numLigne:String=""; // sousTete
		public var libelleTypeLigne:String="";
		//SLOT
		public var idRow:Number=-1;
	}
}
