package univers.facturation.suivi.controles.vo
{
	[Bindable]
	public class SiteVO
	{
		
		public var nomSite:String = "";
		public var adresse:String = "";
		public var commune:String = "";
		public var zipCode:String = "";
		public var pays:String = "";
		
		public function SiteVO()
		{
		}
		
		public function addSite(value:Object):void
		{
			this.nomSite = value[0].NOM_SITE;
			this.adresse = value[0].SP_COMMUNE;
			this.commune = value[0].PAYS;
			this.zipCode = value[0].SP_ADRESSE1;
			this.pays = value[0].SP_CODE_POSTAL;
		}
		
	}
}