﻿<cfcomponent extends="AbstractConnectToApp">
<!--- SSO SFR :  CONNECTION PAR SSO SFR--->		
	<cfset THIS.PILOTAGEAPPID = "pilotage">
	<cfset THIS.URLWEBSERVICESSOSFR = "https://sso.sfr.com/extranet_sso/services/SsoService.wsdl">
	
<!---- CHAQUE FONCTION GERE CES ERREURS ET RETOURNE UN OBJECT ERROR AVEC UN CODEERREUR
	   IL EST POSSIBLE DE PRECISER LE TYPE, LE MESSAGE ET LE DETAIL
	   <cfreturn ThrowCfCatchError(CODEERREUR,TYPE,MESSAGE,DETAIL)>
	   codeerreur : int, type : string, message : string, detail  : string --->
	<cffunction name="ConnectTo" 
				displayname="ConnectToApp" 
				description="Permet de se logger à une application" 
				access="remote" 
				output="false" 
				returntype="struct">
					
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
	
		<cftry>
			<cfset clearOk = clearSession()>
			<cfif clearOk eq 1>
				<cfif objConnection.etape eq 3>
					<cfset result = connectToSSOetape3(objConnection)/>
				<cfelse>
					<cfset result = connectToSSOetape1(objConnection.token)/>
				</cfif>
				<cfreturn result>
			</cfif>
			<cfcatch type="any" >
				<cfmail from="no-reply@saaswedo.com" subject="[SSO-0054][4] Dump SSO ConnectTo" to="monitoring@saaswedo.com"  type="text/html" >
				 
					<cfdump var="#CFCATCH#">
					 
				</cfmail>
				<cfreturn ThrowCfCatchError(-13,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
		<cfreturn SESSION>
	</cffunction>	
	<cffunction name="ChangeGroupe" 
				displayname="ChangeGroupe" 
				description="Gère le changement de Groupe de la personne connectée" 
				access="remote" 
				output="false" 
				returntype="struct">
					
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		
		<cflog text="Connection2.ChangeGroupe(objConnection.idperimetre = #objConnection.idperimetre#)">
		
		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfset result = connectOnGroup(objConnection.idperimetre)>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<cfset result = setPerimetre(objConnection.idperimetre)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
					<cfset result = setXMLAccess()>
					<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					 </cfif>
				</cfif>
			</cfif>
			<cfset str= structNew()>
			<cfset str.xml_perimetre = SESSION.XML_PERIMETRE>
			<cfset str.perimetre = SESSION.PERIMETRE>
			 <cfset str.xml_access = SESSION.XML_ACCESS>
			<cfreturn str>
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-16,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
	</cffunction>			
	<cffunction name="ChangePerimetre" 
				displayname="ChangePerimetre" 
				description="Gère le changement de périmètre de la personne connectée" 
				access="remote" 
				output="false" 
				returntype="struct">
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		
		<cflog text="Connection1.ChangePerimetre(objConnection.idgroupe = #objConnection.idgroupe#, objConnection.idperimetre=#objConnection.idperimetre#)">
		
		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfif objConnection.idgroupe neq SESSION.PERIMETRE.ID_GROUPE>
				<cfset result = connectOnGroup(objConnection.idgroupe)>
			</cfif>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
				<cfset result = setChangePerimetre(objConnection.idperimetre)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					
					<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					</cfif>
				</cfif>
			</cfif>
			<cfset str= structNew()>
			<cfset str.perimetre = SESSION.PERIMETRE>
			<cfset str.xml_access = SESSION.XML_ACCESS>
			<cfreturn str>
			
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-15,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
		
	</cffunction>	
	<!--- SSO : Etape 1 et 2 --->
	<cffunction name="connectToSSOetape1"
				access="private" 
				returntype="Struct" 
				output="false" 
				description="se connecte à pilotage en SSO etape 1 : clé SSO et liste titu">
		<cfargument name="tokenSSO" required="true" type="string">
		<cflog text="connection2.connectToSSOetape1(token = #tokenSSO#)" >
		<cfif isDefined("SESSION")>
			<cftry>
				<!--- 1) DEFINITION DE VARIABLES PRIVEES --->
				<cfset SSOCle = "">
				<cfset SSOEmail = "">
				<cfset SSOListeTitu = "">
				<cfset SSOVerif = 1>
				<cfset SSOip = #COOKIE.IP_ORIGINE#>
				
				<!--- 2) NETTOYAGE DE LA SESSION SI BESOIN --->
				<cfif structKeyExists(SESSION,"AUTH_STATE")>
					<cfif SESSION.AUTH_STATE EQ 1>
					   	<cflog type="Information" text="AUTH REQUEST WITH CONNECTED SESSION">
						<cfset clearSession()>
					</cfif>
				</cfif>
				<!---3) MIS A JOUR DE DONNEES EN SESSION/COOKIE --->		
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 51 >
				<cfset COOKIE.CODEAPPLICATION = 51 >
				<!--- 4) RECUPERATION DE L'OBJET POL VIA LE WEBSERVICE SSO --->
				<cfset objPolToken = getPolToken(tokenSSO,THIS.PILOTAGEAPPID,SSOip)>
				<cfif isStruct(objPolToken) AND structKeyExists(objPolToken,"CODEERREUR")>
					<cfreturn objPolToken>	
				<cfelse>	
					<!--- 5)a) VERIFICATION DES DONNEES RECUES : POLToken est une structure ? --->
					<cfif isStruct(objPolToken) > 
						<!--- 5)a) VERIFICATION DE LA CLE SSO --->
						<cfset SSOVerifCleSSO = 0>
						<cfset SSOCle = objPolToken["visibilite"]>	
						<cfif SSOCle neq "">
							<cfset SSOVerifCleSSO = 1>		
						</cfif>
						<cfset goToListeTitu = 0>
						<cfset goToSession = 0>
						<cfset SSOVerif = SSOVerif * SSOVerifCleSSO>
							<!--- 6) CONNECTION VIA CLE SSO --->
							<!--- 6)a) SI LA VERIF EST BONNE, on tente de se connecter via la CléSSO  --->
							
							<cfif SSOVerifCleSSO eq 1>
								<cfset SESSION.POLTOKEN = objPolToken>
								<cfset authSSOResult = validateLoginByCleSSOForSFR(SSOCle)>
								<!--- 7) VERIFICATION DES DONNEES RECUES : AUTH_RESULT = 1 ? --->
									<!---<cfset authSSOResult.AUTH_RESULT = 0>--->
									<cfif isStruct(authSSOResult) AND structKeyExists(authSSOResult,"CODEERREUR")>
									<!--- 7)b) la clé SSO est introuvable ou autres erreurs ( login expiré ) --->
										<!---
										<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO-002] Connection impossible par la clé SSO : #SSOCle#" to="monitoring@saaswedo.com"  type="text/html" >
											<cfoutput>
												La connection est en échec.
												<br>
												CléSSO : #SSOCle#
												<br>
												ip : #SSOip#
												<br>
												token : #tokenSSO#
												<br>
												<cfdump var="#authSSOResult#">
											</cfoutput>
										</cfmail>
										--->
										<cfsavecontent variable="body">
										   <cfoutput>
												La connection est en échec.
												<br>
												CléSSO : #SSOCle#
												<br>
												ip : #SSOip#
												<br>
												token : #tokenSSO#
												<br>
												<cfdump var="#authSSOResult#">
											</cfoutput>
										</cfsavecontent>
										
										<cftry>
											<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
											<cfset mLog.setIDMNT_CAT(14)>
											<cfset mLog.setIDMNT_ID(62)>
											<cfset mLog.setSUBJECTID("#SSOCle#")>
											<cfset mLog.setBODY(body)>
											
											<cfinvoke 
												component="fr.consotel.api.monitoring.MLogger"
												method="logIt"  returnVariable = "result">
												
												<cfinvokeargument name="mLog" value="#mLog#">  
												
											</cfinvoke>	
											
										<cfcatch type="any">
											<cfmail from="MLog <monitoring@saaswedo.com>" 
													to="monitoring@saaswedo.com" 
													server="mail-cv.consotel.fr" 
													port="25" type="text/html"
													subject="test log monitor">
														<cfdump var="#cfcatch#">						
											</cfmail>
										</cfcatch>
										</cftry>
										
										
										
										<cfset goToListeTitu = 1>
									<!--- 7)a) l'utilisateur est authentifié, on envoit les informations de l'utilisateur au front --->
									<cfelse>
										<cfset goToSession = 1>
									</cfif>	
							<!--- 6)b) SI LA VERIF EST INCORRECTE, on léve une exception --->
							<cfelse>
								<cfset goToListeTitu = 1>
							</cfif>
							<!--- 6)c) SI LA CONNEXION PAR TOKEN EST INCORRECTE, ON TENTE PAR LA LISTE DES TITUS  ---->
							<cfif goToListeTitu eq 1>
								<cfset result = validateLoginByListeTituAndMail(objPolToken)>
								<cflog text="--->>> #serializeJSON(result)#">
								<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
									<cfset SESSION.CODEERREUR = result["CODEERREUR"]>
									<cfset envoiMailErreur()>
									<cfreturn result>
								<cfelse>
									<cfset goToSession = 1>
								</cfif>
							</cfif>	
							<!--- 7)a) SI L'UTILISATEUR A ETE TROUVE, ON CREE LE RESTE DE LA SESSION ---->
							<cfif goToSession eq 1>
								<cfset result = createSession()>
								<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
									<cfreturn result>
								</cfif>	
							</cfif>	
					<!--- 5)b) VERIFICATION DES DONNEES RECUES : POLToken est une exception --->
					<cfelse>
						<!---
						<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] PolToken invalide" to="monitoring@saaswedo.com"  type="text/html" >
							<cfoutput>
								objPolToken reçu est incorrect.
								<br>
							</cfoutput>
						</cfmail>
						--->
						
						
						<cfsavecontent variable="body">
						   <cfoutput>
								objPolToken reçu est incorrect #tokenSSO#.
								<br>
							</cfoutput>
						</cfsavecontent>
						
						<cftry>
							<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
							<cfset mLog.setIDMNT_CAT(14)>
							<cfset mLog.setIDMNT_ID(65)>
							<cfset mLog.setSUBJECTID("#tokenSSO#")>
							<cfset mLog.setBODY(body)>
							
							<cfinvoke 
								component="fr.consotel.api.monitoring.MLogger"
								method="logIt"  returnVariable = "result">
								
								<cfinvokeargument name="mLog" value="#mLog#">  
								
							</cfinvoke>	
							
						<cfcatch type="any">
							<cfmail from="MLog <monitoring@saaswedo.com>" 
									to="monitoring@saaswedo.com" 
									server="mail-cv.consotel.fr" 
									port="25" type="text/html"
									subject="test log monitor">
										<cfdump var="#cfcatch#">						
							</cfmail>
						</cfcatch>
						</cftry>
							
						
						<cfreturn ThrowCfCatchError(-32,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>	
					</cfif>
				</cfif>
			<cfcatch type="any" >	
			<!---
				<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] CFCATCH de ConnectSSO" to="monitoring@saaswedo.com"  type="text/html" >
					<cfdump var="#cfcatch#">
					<cfif IsDefined("objPolToken") eq true>
						<cfdump var="#objPolToken#">
					</cfif>	
				</cfmail>
			--->
				<cfsavecontent variable="body">
				   <cfdump var="#cfcatch#">
					<cfif IsDefined("objPolToken") eq true>
						<cfdump var="#objPolToken#">
					</cfif>	
				</cfsavecontent>
				
				<cftry>
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(14)>
					<cfset mLog.setIDMNT_ID(69)>
					<cfset mLog.setSUBJECTID("")>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfdump var="#cfcatch#">						
					</cfmail>

				</cfcatch>
				</cftry>
					<cfmail from="no-reply@saaswedo.com" subject="[SSO-0048][4]Dump SSO connectToSSOetape1" to="monitoring@saaswedo.com"  type="text/html" >
							<cfdump var="#cfcatch#">						
					</cfmail>
				<cfreturn ThrowCfCatchError(-30,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>		
			</cftry>
		<cfelse>
			<!--- <cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] SESSION IS UNDEFINED" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput >
					La session n'est pas définie.<br><br>
					Impossible d'établir une connection SSO
				</cfoutput>
			</cfmail> --->
			
			
			<cfsavecontent variable="body">
			   <cfoutput >
					La session n'est pas définie.<br><br>
					Impossible d'établir une connection SSO
				</cfoutput>
			</cfsavecontent>
			
			<cftry>
				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(14)>
				<cfset mLog.setIDMNT_ID(68)>
				<cfset mLog.setSUBJECTID("")>
				<cfset mLog.setBODY(body)>
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			</cftry>
			<cfreturn ThrowCfCatchError(-33,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>	
		</cfif>	
			
		<!--- <cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] Dump SSO validateLoginByCleSSOForSFR" to="monitoring@saaswedo.com"  type="text/html" >
			<cfoutput >
				SESSION : <cfdump var="#SESSION#">
			</cfoutput>
		</cfmail> --->
		
		
		<cfsavecontent variable="body">
		    <cfoutput>
				SESSION : <cfdump var="#SESSION#">
			</cfoutput>
		</cfsavecontent>
		
		<cftry>
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(14)>
			<cfset mLog.setIDMNT_ID(81)>
			<cfset mLog.setSUBJECTID("Cle SSO : #SSOCle#")>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		</cftry>
		
		
		
		<cfreturn SESSION>
	</cffunction>	
	<!--- SSO : Etape 3 --->
	<cffunction name="connectToSSOetape3" 
				access="private" 
				returntype="Struct" 
				output="false" 
				description="se connecte à pilotage en SSO etape 3 : création login à partir des titus">
		<cfargument name="objConnection" required="true" type="struct" >
		
		<cflog text="connection2.connectToSSOetape3 ()" >
		<cflog text="CREATION LOGIN : #SESSION.POLTOKEN.login# - clé sso : #SESSION.POLTOKEN.visibilite#">
		
		<cfif isDefined("SESSION")>
			<cftry>
				<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.connectClient_v5b">
					<cfprocparam type="in" value="#trim(SESSION.POLTOKEN.login)#" 			cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(SESSION.POLTOKEN.nom)#" 				cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(SESSION.POLTOKEN.prenom)#" 			cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(SESSION.POLTOKEN.telFix)#" 			cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(SESSION.POLTOKEN.visibilite)#" 		cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(createListeTitu(SESSION.POLTOKEN).listeTitu)#" 	cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(createListeTitu(SESSION.POLTOKEN).listeContrat)#" 	cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#objConnection.codeapp#" 		cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="qValidateLogin">
				</cfstoredproc>
				
				<cfmail from="no-reply@saaswedo.com" subject="[SSO-0049][4] Dump SSO connectToSSOetape3" to="monitoring@saaswedo.com"  type="text/html" >
					  <cfdump var="#qValidateLogin#">
					  <cfdump var="#p_retour#">
				</cfmail>
				
				
				
				<cfsavecontent variable="body">
				   <cfoutput >
						qValidateLogin : <cfdump var="#qValidateLogin#"><br>
						p_retour : #p_retour#
					</cfoutput>
				</cfsavecontent>
				
				<cftry>
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(14)>
					<cfset mLog.setIDMNT_ID(66)>
					<cfset mLog.setSUBJECTID("#p_retour#")>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfdump var="#cfcatch#">						
					</cfmail>
				</cfcatch>
				</cftry>
				
				<cfif p_retour eq 1>
					<cfif qValidateLogin.recordcount EQ 1>
						<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
						<cfif boolCheckIP EQ 1>
							<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
							<cfif checkIpResult LT 1>
								<cfreturn ThrowCfCatchError(-2)>
								<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
							<cfelse>
								<cfset result = setupSessionUser(qValidateLogin)>
								<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
									<cfreturn result>
								<cfelse>
									<cfreturn 1>
								</cfif>
								<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
							</cfif>
						<cfelse>
							<cfset result = setupSessionUser(qValidateLogin)>
							<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
								<cfreturn result>
							<cfelse>
								<cfset result = createSession()>
								<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
									<cfreturn result>
								</cfif>		
							</cfif>
						</cfif>
					<cfelse>
						<cfreturn ThrowCfCatchError(-3)>
					</cfif>
				<cfelse>
					<cfreturn ThrowCfCatchError(p_retour)>
				</cfif>
				<cfcatch type="any"  > 	
					<cfmail from="no-reply@saaswedo.com" subject="[SSO-0050][4] Dump SSO connectToSSOetape3" to="monitoring@saaswedo.com"  type="text/html" >
								<cfdump var="#cfcatch#">						
					</cfmail>
					<cfreturn ThrowCfCatchError(-30,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
				</cfcatch>
			</cftry>
		<cfelse>
		
			<!--- <cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] SESSION IS UNDEFINED" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput >
					La session n'est pas définie.<br><br>
					Impossible d'établir une connection SSO
				</cfoutput>
			</cfmail> --->
			
			
			<cfsavecontent variable="body">
			   <cfoutput>
					La session n'est pas définie.<br><br>
					Impossible d'établir une connection SSO
				</cfoutput>
			</cfsavecontent>
			
			<cftry>
				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(14)>
				<cfset mLog.setIDMNT_ID(82)>
				<cfset mLog.setSUBJECTID("")>
				<cfset mLog.setBODY(body)>
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			</cftry>
			<cfreturn ThrowCfCatchError(-33,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>	
		</cfif>
		<cfreturn SESSION>
	</cffunction>	
	<!--- SSO : RECUPERATION DE L'OBJET POLTOKEN --->
	<cffunction name="getPolToken" 
			access="private" 
			returntype="Any" 
			description="Interroge la base de données SSO SFR pour récupèrer l'objet POL">
		<cfargument name="tokenID" 	type="string" 	required="true" >
		<cfargument name="appID" 	type="string" 	required="true" >
		<cfargument name="IP" 		type="string" 	required="true" >
		<cflog text="connection2.getPoltoken(tokenID = #tokenID# - appID = #appID# - IP = #IP#)" >
		<cftry>
			
			<cfset tok = replace(tokenID,'##',"")>
			<cfset strParam = structNew()>
			<cfset strParam.tokenId = tok>
			<cfset strParam.appId = appID>
			<cfset strParam.ip = IP>
			
			<cfset webService=createObject("webservice",THIS.URLWEBSERVICESSOSFR)>
			<cfset resObjPOlToken = webService.ValidateParamToken(strParam)>
			
			<cfscript>
				response = {};
				response["tokenId"] = resObjPOlToken.getTokenId();
 				response["appId"] = resObjPOlToken.getAppId();
 				response["login"] = resObjPOlToken.getLogin();
 				response["clientId"] = resObjPOlToken.getClientId();
 				response["paramList"] = resObjPOlToken.getParamListe();

 				paramArray= response.paramList.getParam();
 				len = arrayLen(paramArray);

 				for (i=1; i LTE len; i++)
 				{
 					if(structKeyExists(response, paramArray[i].getIdParam())) {

 						p = paramArray[i].getIdParam();
 						response[p] = response[p] & "," & getToken(paramArray[i].getValue(),1,";");
 						writeLog("2- #response[paramArray[i].getIdParam()]#");
 						
 					}else{
 						response[paramArray[i].getIdParam()] = " " & getToken(paramArray[i].getValue(),1,";");
 						writeLog("1- #response[paramArray[i].getIdParam()]#");
 					}
 				}
 				
 				
				writelog(serializeJson(response));
			</cfscript>

			<cfmail from="no-reply@saaswedo.com" subject="[SSO-0051][4] Status validatePolToken" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput>
					<br>
					<cfif isDefined("strParam")><cfdump var="#strParam#"><cfelse>strParam undefined</cfif><br>
					<cfif isDefined("resObjPOlToken")>
						<cfdump var="#response#">
						<cfdump var="#GetSOAPResponse(webService)#">
					<cfelse>resObjPOlToken undefined</cfif><br>
				</cfoutput>
				
			</cfmail>

			<cfreturn response> 
		<cfcatch type="any" >	
			<!--- <cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] WebService Erreur" to="monitoring@saaswedo.com"  type="text/html" >
				<cfoutput>
					Une erreur est survenue pendant l'appel du webService.
					<br>
					#cfcatch.Detail# : #cfcatch.Message#
					<br><br>
					<cfif isDefined("resObjPOlToken") eq TRUE ><cfdump var="#resObjPOlToken#" label="resObjPOlToken"><cfelse>resObjPOlToken = undefined </cfif>
				</cfoutput>
			</cfmail> --->
			
			<cfmail from="no-reply@saaswedo.com" subject="[SSO-0052][4] Status validatePolToken" to="monitoring@saaswedo.com"  type="text/html" >
					<cfdump var="#cfcatch#">
			</cfmail>
			
			<cfsavecontent variable="body">
			   <cfoutput>
					Une erreur est survenue pendant l'appel du webService.
					<br>
					#cfcatch.Detail# : #cfcatch.Message#
					<br><br>
					<cfif isDefined("resObjPOlToken") eq TRUE ><cfdump var="#resObjPOlToken#" label="resObjPOlToken"><cfelse>resObjPOlToken = undefined </cfif>
				</cfoutput>
			</cfsavecontent>
			
			<cftry>
				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(14)>
				<cfset mLog.setIDMNT_ID(63)>
				<cfset mLog.setSUBJECTID("")>
				<cfset mLog.setBODY(body)>
								
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			</cftry>
			<cfreturn ThrowCfCatchError(-31,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>			
		</cfcatch>
		</cftry>
	</cffunction>
	<!--- SSO : VALIDATION DU LOGIN PAR LA LISTE TITU ET LE MAIL --->
	<cffunction name="validateLoginByListeTituAndMail" 
				access="private" 
				output="false" 
				description="définit l'objet USER à partir du mail et de la liste des titus fournis par SFR"
				returntype="Any">
		<cfargument name="objPolToken" required="true" type="any" >
		
		<cflog text="connection2.validateLoginByListeTituAndMail">
		
		<cftry>
			<!---- 1) VERIFICATION DES DONNEES --->
			<cfset SSOVerif = 1>
				<!--- 1)a) VERIFICATION DU MAIL --->
			<cfset SSOVerifMailSSO = 0>
			<cfif structKeyExists(objPolToken,"email")>
				<cfif isValid("email",objPolToken["email"]) >
					<cfset SSOEmail = objPolToken["email"]> 
					<cfset SSOVerifMailSSO = 1>
				</cfif>		
			</cfif>	
			<cfset SSOVerif = SSOVerif * SSOVerifMailSSO>
				<!--- 1)b) CONSTRUCTION ET VERIFICATION DE LA LISTE DE TITUS --->
					<!--- 1)b)1) CONSTRUCTION --->
			<cfset _result = createListeTitu(objPolToken)>
			<cfif len(_result.listeTitu) gt 0 or len(_result.listeContrat) ge 0>	
				<cfset SSOVerifListeTitu = 1>
			<cfelse>
				<cfset SSOVerifListeTitu = 0>
			</cfif>	
			
					<!--- 1)b)2) VERIFICATION --->
			<cfset SSOVerif = SSOVerif * SSOVerifListeTitu>
			<!--- 2) CONNECTION --->
			<!--- 2)a) Si les données sont valables, on tente de se connecter ---> 
			<cfif SSOVerif eq 1>
				<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.connectClient_v4b">
					<cfprocparam type="in" value="#trim(SSOEmail)#" 		cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(_result.listeTitu)#" 		cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="in" value="#trim(_result.listeContrat)#" 		cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="qValidateLogin">
				</cfstoredproc>
				
				 
				<cfmail from="no-reply@saaswedo.com" subject="[SSO-0053][4] Dump SSO validateLoginByListeTituAndMail" to="monitoring@saaswedo.com"  type="text/html" >
					<cfoutput >
						qValidateLogin : <cfdump var="#qValidateLogin#">
						p_retour : <cfdump var="#p_retour#">
					</cfoutput>
				</cfmail>
			 
				
				<cfsavecontent variable="body">
				   <cfoutput>
						qValidateLogin : <cfdump var="#qValidateLogin#">
					</cfoutput>
				</cfsavecontent>
				
				<cftry>
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(14)>
					<cfset mLog.setIDMNT_ID(83)>
					<cfset mLog.setSUBJECTID("")>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfdump var="#cfcatch#">						
					</cfmail>
				</cfcatch>
				</cftry>
				
				<cflog text="p_retour = #p_retour#">
				<cfif p_retour eq 1>
					<cfif qValidateLogin.recordcount EQ 1>
						<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
						<cfif boolCheckIP EQ 1>
							<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
							<cfif checkIpResult LT 1>
								<cfreturn ThrowCfCatchError(-2)>
								<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
							<cfelse>
								<cfset result = setupSessionUser(qValidateLogin)>
								<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
									<cfreturn result>
								<cfelse>
									<cfreturn 1>
								</cfif>
								<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
							</cfif>
						<cfelse>
							<cfset result = setupSessionUser(qValidateLogin)>
							<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
								<cfreturn result>
							<cfelse>
								<cfreturn 1>
							</cfif>
						</cfif>
					<cfelse>
						<cfreturn ThrowCfCatchError(-3)>
					</cfif>
				<cfelse>
					<cfreturn ThrowCfCatchError(p_retour)>
				</cfif>
			<!--- 2)b) LES DONNEES SONT INCORRECTES --->
			<cfelse>
				<cflog text="831: SSOVerifListeTitu  #SSOVerifListeTitu#">
				<cfif SSOVerifListeTitu eq 0>
					<cfif SSOVerifMailSSO eq 0>
						<cfreturn ThrowCfCatchError(-38)>
					<cfelse>	
						<cfreturn ThrowCfCatchError(-35)>
					</cfif>
				<cfelse>	
					<cfreturn ThrowCfCatchError(-37)>
				</cfif>	
			</cfif>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-34,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>	
		</cfcatch>	
		</cftry>
	</cffunction>
	<!--- SSO : VALIDATION DU LOGIN PAR LE TOKEN SSO FOURNI EN PARAMETRE --->
	<cffunction name="validateLoginByCleSSOForSFR" 
				access="private" 
				output="false" 
				description="retourne l'objet USER à partir d'une Clé SSO fourni par SFR"
				returntype="Any">
		<cfargument name="cleSSO" required="true" type="string">
		<cflog text="connection2.validateLoginByCleSSOforSFR ( cleSSO = #cleSSO# )" >
		<cftry>
			<!--- cfcase sur le code_appli pour redefinir le AUTH_DSN, et le package global --->
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.connectClient_v3">
				<cfprocparam type="in" value="#trim(cleSSO)#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qValidateLogin">
			</cfstoredproc>

			<cfif p_retour eq 1>
				<cfif qValidateLogin.recordcount EQ 1>
					<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
					<cfif boolCheckIP EQ 1>
						<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
						<cfif checkIpResult LT 1>
							<cfreturn ThrowCfCatchError(-2)>
							<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
						<cfelse>
							<cfset result = setupSessionUser(qValidateLogin)>
							<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
								<cfreturn result>
							<cfelse>
								<cfreturn 1>
							</cfif>
							<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
						</cfif>
					<cfelse>
						<cfset result = setupSessionUser(qValidateLogin)>
						<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
							<cfreturn result>
						<cfelse>
							<cfreturn 1>
						</cfif>
					</cfif>
				<cfelse>
					<cfreturn ThrowCfCatchError(-3)>
				</cfif>
			<cfelse>
				<cfreturn ThrowCfCatchError(p_retour)>
			</cfif>
		<cfcatch type="any" >	
			<cfreturn ThrowCfCatchError(-31,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>	
		</cfcatch>
		</cftry>
	</cffunction>
	<!---  CREATION DU XML D'ACCESS DES MODULES DU USER / PERIMETRE--->
	<cffunction name="setXMLAccess"  access="private" output="false" returntype="Any" >
		<cflog text="connection2.setXMLAccess()">
		<cftry>
			<!--- --->
			<!--- DEFINITION DU NIVEAU D'ORGA DONT ON VEUT CONNAITRE LES ACCES --->
			<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_GROUPE>
			<cfif structkeyExists(SESSION.PERIMETRE,"ID_PERIMETRE")>
				<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
			</cfif>
			
			<cflog text="connection2.setXMLAccess - #SESSION.USER.CLIENTACCESSID# - #SESSION.PERIMETRE.ID_GROUPE# - #SESSION.PERIMETRE.ID_PERIMETRE#">
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getUniversApplication">
				<cfprocparam value="#SESSION.USER.CLIENTACCESSID#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qResult">
			</cfstoredproc>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getAccesModApplication_v2">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
				<cfprocparam value="51"			type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="1"			type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qResult2">
			</cfstoredproc>	
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getDefaultModule">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
				<cfprocresult name="qResult3">
			</cfstoredproc>
			
			<cfset createSessionXmlPFGP(qResult,qResult2,qResult3)>
			<cfset createSessionXmlPFGPAccueil(qResult,qResult2)>
			<cfreturn 1>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-24,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="createSessionXmlPFGPAccueil" access="private" output="false" returntype="void" hint="Créer le xml qui sert au menu de l'accueil">
		<cfargument name="qResult" type="query" required="true" >
		<cfargument name="qResult2" type="query" required="true" >
		
		<cflog text="connection2.createSessionXmlPFGPAccueil">
		<!---PHASE 0 : on créé le xml racine--->
		<cfset xmlAcces = xmlNew(true)/>
		<cfset xmlAcces.xmlRoot = xmlElemNew(xmlAcces,"ROOT")/>
		<!--- PHASE 1 : on créé le xml des univers--->
		<cfset idx=1/>
		<cfloop query="qResult">
		<!---Si le type est celui désiré. 
			 Type = 1 -> Menu M00. Type = 2 -> Accueil M521--->
			<cfif qResult.TYPE eq 2>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx] = XmlElemNew(xmlAcces,"UNIVERS")>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM#>
			<!---Si l'univers est SYS = 1, cad qu'un module charge quand on clique dessus--->
				<cfif qResult.SYS eq 1>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.MODULE_KEY#>
				<cfelse>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
				</cfif>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.SYS = #qResult.SYS#>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.USR = "2">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.type = "radio">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.toggled = "false">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ENABLED = "true">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ICON = #qResult.ICONE#>
				<cfset idx = idx +1>
			</cfif>
		</cfloop>
		<!--- PHASE 2: on ajoute les fonctions--->
		<!---1) on boucle sur chaque univers--->
		<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
			<cfset keyUniv = xmlAcces.xmlRoot.XmlChildren[i].xmlAttributes.KEY>
		<!---2) on boucle sur les fonctions pour trouver les fonctions enfants de l'univers parent--->	
			<cfloop query="qResult2">
				<!---Si l'univers est différent, on passe à la fonction suivante--->
				<cfif qResult2.UNIVERS_BLOC eq keyUniv>
					<!---Si la clé de la fonction est égale à la clé de l'univers parent, on ignore. Sinon on continue le traitement--->
					<cfif qResult2.KEY neq qResult2.UNIVERS_BLOC>
						<cfset idx = arraylen(xmlAcces.xmlRoot.xmlChildren[i].xmlChildren)+1>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx] = XmlElemNew(xmlAcces,"FONCTION")>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.LBL = #qResult2.NOM_BLOC#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.DSC = #qResult2.DESCRIPTION_BLOC#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.KEY = #qResult2.KEY#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ENABLED = "true">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ICON = #qResult2.ICONE_BLOC#>
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>
		<cflog text="connection2.xmlAcces = #xmlAcces#" >
		<cfset SESSION.XML_ACCESS_PFGP_ACCUEIL = xmlAcces>
	</cffunction>
	<cffunction name="createSessionXmlPFGP" access="private" returntype="void" >
		<cfargument name="qResult" type="query" required="true" >
		<cfargument name="qResult2" type="query" required="true" >
		<cfargument name="qResult3" type="query" required="true" >
		
		<cflog text="connection2.createSessionXmlPFGP">
		<!---PHASE 0 : on créé le xml racine--->
		<cfset xmlAcces = xmlNew(true)/>
		<cfset xmlAcces.xmlRoot = xmlElemNew(xmlAcces,"ROOT")/>
		<!--- PHASE 1 : on créé le xml des univers--->
		<cfset idx=1/>
		<cfloop query="qResult">
		<!---Si le type est celui désiré. 
			 Type = 1 -> Menu M00. Type = 2 -> Accueil M521--->
			<cfif qResult.TYPE eq 1>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx] = XmlElemNew(xmlAcces,"UNIVERS")>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM#>
			<!---Si l'univers est SYS = 1, cad qu'un module charge quand on clique dessus--->
				<cfif qResult.SYS eq 1>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.MODULE_KEY#>
				<cfelse>
					<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
				</cfif>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.SYS = #qResult.SYS#>
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.USR = "2">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.type = "radio">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.toggled = "false">
				<cfset xmlAcces.xmlRoot.xmlChildren[idx].XmlAttributes.ENABLED = "true">
				<cfset idx = idx +1>
			</cfif>
		</cfloop>
		<!--- PHASE 2: on ajoute les fonctions--->
		<!---1) on boucle sur chaque univers--->
		<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
			<cfset keyUniv = xmlAcces.xmlRoot.XmlChildren[i].xmlAttributes.KEY>
		<!---2) on boucle sur les fonctions pour trouver les fonctions enfants de l'univers parent--->	
			<cfloop query="qResult2">
				<!---Si l'univers est différent, on passe à la fonction suivante--->
				<cfif qResult2.UNIVERS_MENU eq keyUniv>
					<!---Si la clé de la fonction est égale à la clé de l'univers parent, on ignore. Sinon on continue le traitement--->
					<cfif qResult2.KEY neq qResult2.UNIVERS_MENU>
						<cfset idx = arraylen(xmlAcces.xmlRoot.xmlChildren[i].xmlChildren)+1>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx] = XmlElemNew(xmlAcces,"FONCTION")>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.LBL = #qResult2.NOM_MENU#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.KEY = #qResult2.KEY#>
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset xmlAcces.xmlRoot.xmlChildren[i].xmlChildren[idx].XmlAttributes.ENABLED = "true">
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>
		<!--- PHASE 3: on definit la fonction affichée par défaut--->
		<cfset boolDefault = 0>
		<cfloop query="qResult3">
			<cfset keyDefault = qResult3.KEY>
			<!--- On boucle sur les univers --->
			<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren)#" index="i">
				<cfset keyUniv= xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.KEY/>
				<!--- Si l'univers correspond à l'univers par défaut, on modifie la propriété toggled à true et on sort de la boucle --->
				<cfif keyDefault eq keyUniv>
					<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.toggled = 'true'>
					<cfset boolDefault = 1>
					<cfbreak>
				<cfelse>
					<!--- Sinon on vérifie que l'univers a des enfants --->
					<cfif arraylen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren) gt 0>
						<!--- On boucle sur les fonctions de l'univers --->	
						<cfloop from="1" to="#ArrayLen(xmlAcces.xmlRoot.XmlChildren[i].XmlChildren)#" index="j">
							<cfset keyFonction= xmlAcces.xmlRoot.XmlChildren[i].XmlChildren[j].XmlAttributes.KEY>
							<!--- Si la fonction correspond à la fonction par défaut, on modifie la propriété toggled à true et on sort de la boucle --->
							<cfif keyFonction eq keyDefault>
								<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlChildren[j].XmlAttributes.toggled = 'true'>
								<cfset xmlAcces.xmlRoot.XmlChildren[i].XmlAttributes.toggled = 'true'>
								<cfset boolDefault = 1>	
								<cfbreak>
							</cfif> 
						</cfloop>
					</cfif>
				</cfif>
			</cfloop>
			<cfif boolDefault eq 0>
				<cfif arraylen(xmlAcces.xmlRoot.XmlChildren) gt 0>
					<cfset xmlAcces.xmlRoot.XmlChildren[1].XmlAttributes.toggled = 'true'>
					<cfif arraylen(xmlAcces.xmlRoot.XmlChildren[1].XmlChildren) gt 0>
						<cfset xmlAcces.xmlRoot.XmlChildren[1].XmlChildren[1].XmlAttributes.toggled = 'true'>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<cflog text="connection2.xmlAcces = #xmlAcces#">
		<cfset SESSION.XML_ACCESS = xmlAcces>
	</cffunction>		
	<!---  CREATION DE L'OBJET PERIMETRE DE LA SESSION  --->
	<cffunction name="setPerimetre" access="private" returntype="any" >
		<cfargument name="idGroupeClient" required="true" type="numeric">
		<cflog text="connection2.setPerimetre(idGroupeClient = #idGroupeClient#)" >
		<cftry>	
			<cfset setPeriod(idGroupeClient)>
			<cfset accessnodeId = buildGroupXmlTree(idGroupeClient)>
			
			<cfif isStruct(accessnodeId) AND structKeyExists(accessnodeId,"CODEERREUR")>
				<cfreturn accessnodeId>
			<cfelse>
				<cflog text="connection2.setPerimetre(#accessnodeId#,#SESSION.USER.APP_LOGINID#)">
				
				<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.detail_noeud_v2">
					<cfprocparam  value="#accessnodeId#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#SESSION.USER.APP_LOGINID#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="qGetNodeInfos">
				</cfstoredproc>
				
				<cflog text="qGetNodeInfos.recordcount = #qGetNodeInfos.recordcount#">
				
				<cfif qGetNodeInfos.recordcount EQ 1>
					<cfset SESSION.PERIMETRE.ID_PERIMETRE=qGetNodeInfos['IDGROUPE_CLIENT'][1]>
					<cfset SESSION.PERIMETRE.RAISON_SOCIALE=qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
					<cfset SESSION.PERIMETRE.GROUP_CODE_STYLE=qGetNodeInfos['CODE_STYLE'][1]>
					<cfset SESSION.PERIMETRE.DROIT_GESTION_FOURNIS=qGetNodeInfos['DROIT_GESTION_FOURNIS'][1]>
					
					<cfif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPE">			
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="Groupe">
					<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPELIGNE">			
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="GroupeLigne">
					<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "SOUSTETE">			
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="SousTete">
					<cfelse>
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE=qGetNodeInfos['TYPE_PERIMETRE'][1]>
					</cfif>
					<cfif compareNoCase(SESSION.PERIMETRE.TYPE_PERIMETRE,"GROUPE") eq 0>
                        <cfset SESSION.PERIMETRE.TYPE_LOGIQUE="ROOT">
                        <cfelse>
                        <cfset SESSION.PERIMETRE.TYPE_LOGIQUE=qGetNodeInfos['TYPE_ORGA'][1]>
                    </cfif>					
					<cfcookie name="CODE_STYLE" value="#qGetNodeInfos['CODE_STYLE'][1]#" expires="never">
				</cfif>
				<cfreturn 1>
			</cfif>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-25,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<!--- ENVOI DE MAIL DE STATUS DU PROCESS SSO --->
	<cffunction name="envoiMailConnection" 
				access="private">
		<!--- <cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] Connection réussie à pilotage" to="monitoring@saaswedo.com"  type="text/html" >
			<cfoutput>
				La connection est réussie.
				<br><br>
					Paramètres complémentaires :<br>
					<cfif isDefined("objPolToken")>
						- mail = <cfif structKeyExists(SESSION.POLTOKEN,"email")>#SESSION.POLTOKEN.email#<cfelse>undefined</cfif><br>
						- clé SSO = <cfif structKeyExists(SESSION.POLTOKEN,"visibilite")>#SESSION.POLTOKEN.visibilite#<cfelse>undefined</cfif><br>
						- liste titu = #createListeTitu(SESSION.POLTOKEN)#
					</cfif>
				<br>
			</cfoutput>			
		</cfmail> --->
		
		
		<cfsavecontent variable="body">
		   <cfoutput>
				La connection est réussie.
				<br><br>
					Paramètres complémentaires :<br>
					<cfif isDefined("objPolToken")>
						- mail = <cfif structKeyExists(SESSION.POLTOKEN,"email")>#SESSION.POLTOKEN.email#<cfelse>undefined</cfif><br>
						- clé SSO = <cfif structKeyExists(SESSION.POLTOKEN,"visibilite")>#SESSION.POLTOKEN.visibilite#<cfelse>undefined</cfif><br>
						- liste titu = #trim(createListeTitu(SESSION.POLTOKEN).listeTitu)#
						- liste Contrat = #trim(createListeTitu(SESSION.POLTOKEN).listeContrat)#
					</cfif>
				<br>
			</cfoutput>
		</cfsavecontent>
		
		<cftry>
			<cfset email = "undefined">
			<cfif structKeyExists(SESSION.POLTOKEN,'email')>
				<cfset email = SESSION.POLTOKEN.email>
			</cfif>
			
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(14)>
			<cfset mLog.setIDMNT_ID(61)>
			<cfset mLog.setSUBJECTID("#email#")>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="envoiMailErreur" 
				access="private">					
		<!---
		<cfmail from="no-reply@sfrbusinessteam.fr" subject="[SSO] nection" to="monitoring@saaswedo.com"  type="text/html" >
			<cfoutput>
				Une erreur a été rencontrée et ne permet pas de se connecter.
				<br><br>
				Raison : <br>
				<cfset val = #SESSION.CODEERREUR#>
				<cflog text = "val =  #val#">
				<cfswitch expression="#val#" >
					<cfcase value="5" >
						Le login n'a pas d'accès à la racine
					</cfcase>
					<cfcase value="4" >
						Aucune collecte active sur ces titulaires
					</cfcase>
					<cfcase value="3" >
						Les titulaires appartiennent à plus d'une racine
					</cfcase>
					<cfcase value="-2" >
						IP invalide : #CGI.REMOTE_ADDR# 
					</cfcase>
					<cfcase value="-3" >
						Utilisateur introuvable dans notre base de données
					</cfcase>
					<cfcase value="-5" >
						Erreur lors de la création de l'objet USER dans la SESSION
					</cfcase>
					<cfcase value="-17" >
						Erreur procédure : connectClient_V4
					</cfcase>
					<cfcase value="-35" >
						Aucun titulaire n'est disponible pour le compte<br>
					</cfcase>
					<cfcase value="-37" >
						Format de mail invalide ou mail absent.
					</cfcase>
					<cfdefaultcase>
						undefined
					</cfdefaultcase>
				</cfswitch>
				
				<br><br>
				Paramètres complémentaires :<br>
				<cfif structKeyExists(SESSION,"POLTOKEN")>
					- mail = <cfif structKeyExists(SESSION.POLTOKEN,"email")>#SESSION.POLTOKEN.email#<cfelse>undefined</cfif><br>
					- clé SSO = <cfif structKeyExists(SESSION.POLTOKEN,"visibilite")>#SESSION.POLTOKEN.visibilite#<cfelse>undefined</cfif><br>
					- liste titu = 
					<cfif structKeyExists(SESSION.POLTOKEN,"porteeTituListe")>
						<cfset porteeTitu = SESSION.POLTOKEN["porteeTituListe"]["porteeTitu"]>
						<cfif isDefined("porteeTitu")>
							<cfset listeTitu = "">
							<cfset taillePorteeTituListe = arraylen(porteeTitu)>
							<cfif  taillePorteeTituListe gt 0>
								<cfloop index="idx" from="1" to="#taillePorteeTituListe#">	
									<cfset item = porteeTitu[#idx#]["titu"]>
									<cfif listeTitu neq "">
										<cfset listeTitu = '#listeTitu#,''#item#'''/>
									<cfelse>
										<cfset listeTitu = '''#item#'''/>
									</cfif>
								</cfloop>
								#listeTitu#
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfoutput>
		</cfmail>
		--->
		
		
		
		
		
		
		
		<cfsavecontent variable="body">
		   <cfoutput>
				Une erreur a été rencontrée et ne permet pas de se connecter.
				<br><br>
				Raison : <br>
				<cfset val = #SESSION.CODEERREUR#>
				<cflog text = "val =  #val#">
				<cfswitch expression="#val#" >
					<cfcase value="5" >
						Le login n a pas d accès à la racine
					</cfcase>
					<cfcase value="4" >
						Aucune collecte active sur ces titulaires
					</cfcase>
					<cfcase value="3" >
						Les titulaires appartiennent à plus d une racine
					</cfcase>
					<cfcase value="-2" >
						IP invalide : #CGI.REMOTE_ADDR# 
					</cfcase>
					<cfcase value="-3" >
						Utilisateur introuvable dans notre base de données
					</cfcase>
					<cfcase value="-5" >
						Erreur lors de la création de l objet USER dans la SESSION
					</cfcase>
					<cfcase value="-17" >
						Erreur procédure : connectClient_V4
					</cfcase>
					<cfcase value="-35" >
						Aucun titulaire n est disponible pour le compte<br>
					</cfcase>
					<cfcase value="-37" >
						Format de mail invalide ou mail absent.
					</cfcase>
					<cfdefaultcase>
						undefined
					</cfdefaultcase>
				</cfswitch>
				
				<br><br>
				Paramètres complémentaires :<br>
				<cfif structKeyExists(SESSION,"POLTOKEN")>
					- mail = <cfif structKeyExists(SESSION.POLTOKEN,"email")>#SESSION.POLTOKEN.email#<cfelse>undefined</cfif><br>
					- clé SSO = <cfif structKeyExists(SESSION.POLTOKEN,"visibilite")>#SESSION.POLTOKEN.visibilite#<cfelse>undefined</cfif><br>
					- liste titu = 
					<cfif structKeyExists(SESSION.POLTOKEN,"porteeTituListe")>
						<cfset porteeTitu = SESSION.POLTOKEN["porteeTituListe"]["porteeTitu"]>
						<cfif isDefined("porteeTitu")>
							<cfset listeTitu = "">
							<cfset taillePorteeTituListe = arraylen(porteeTitu)>
							<cfif  taillePorteeTituListe gt 0>
								<cfloop index="idx" from="1" to="#taillePorteeTituListe#">	
									<cfset item = porteeTitu[#idx#]["titu"]>
									<cfif listeTitu neq "">
										<cfset listeTitu = '#listeTitu#,''#item#'''/>
									<cfelse>
										<cfset listeTitu = '''#item#'''/>
									</cfif>
								</cfloop>
								#listeTitu#
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfoutput>
		</cfsavecontent>
		
		<cftry>
			
			<cfset _email = "email: n.c.">
			<cfset _email_racine = -1>
			<cfif structKeyExists(SESSION,"USER")>
				<cfset _email = SESSION.USER.EMAIL>
			</cfif>
			<cfif structKeyExists(SESSION,"PERIMETRE")>
				<cfset _email_racine = SESSION.USER.CLIENTID>
			</cfif>
			
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(14)>
			<cfset mLog.setIDMNT_ID(121)>
			<cfset mLog.setSUBJECTID("#_email#")>
			<cfset mLog.setIDRACINE(_email_racine)>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result1">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		</cftry>
										
										
	</cffunction>

	<cffunction name="createListeTitu" access="private" returntype="any" >
		<cfargument name="objPolToken" type="any" >	
		<cfscript>
			writeLog("connection2.createListeTitu() titu + contrat");
			var listeTitu = "";
			var listeContrat = "";
			var liste = {};
			var token = arguments.objPolToken;

		
			if(structKeyExists(token, "PORTEE_CONTRAT")){
				listeContrat = replace(token.PORTEE_CONTRAT, ";", ",","ALL");
			}

			if(structKeyExists(token, "PORTEE_TITU")){
				listeTitu = replace(token.PORTEE_TITU, ";", ",","ALL");
			}

			writelog("#listeTitu#");
			writelog("#listeContrat#");
			liste.listeTitu= listeTitu;
			liste.listeContrat = listeContrat;
			writelog("#serializeJson(liste)#");
			return liste;
		</cfscript>
	</cffunction>

	<cffunction name="createSession"  access="private" returntype="Any" >
		<cflog text="connection2.createSession()" >
		<cfset envoiMailConnection()>
			<!--- GET GROUPLIST : récupération de la liste des racines du login --->
		<cfset result = getGroupList(SESSION.USER.APP_LOGINID)>
		<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
			<cfreturn result>
		<cfelse>
			<!--- CONNECTONGROUP : se connecte à la racine_titu prioritairement 
				sinon au groupe correspondant à la racine rattaché à l'utilisateur à la création 
				sinon connectOnGroup se connectea au premier groupe de listeracine--->
			<cfif structKeyExists(SESSION,"PERIMETRE") AND structKeyExists(SESSION.PERIMETRE,"RACINE_TITU")>
				<cfset result = connectOnGroup(SESSION.PERIMETRE.RACINE_TITU)>
			<cfelse>	
				<cfset result = connectOnGroup(SESSION.USER.CLIENTID)>	
			</cfif>	
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETPERIMETRE : définit le périmètre --->
				<cfset result = setPerimetre(SESSION.PERIMETRE.ID_GROUPE)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
					<cfset result = setXMLAccess()>
					<cfif isDefined("result") AND isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					 </cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn 1>
	</cffunction>
</cfcomponent>