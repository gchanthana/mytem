<cfcomponent name="ListeOperateurGroupeLigne">
	<cffunction name="getListeOperateur" access="public" returntype="query" output="false">	
		<cfargument name="idGroupe" type="numeric" required="false" default="1">				
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_v3.sf_listeop_v2">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idGroupe#"/>		
			<cfprocresult name="listeOP"/>
		</cfstoredproc>
						
		<cfquery name="getListe" dbtype="query">		
			select nom as OPNOM, operateurID as OPERATEURID, IDORGA as NODID				
			from listeOP		
		</cfquery>			
		<cfreturn getListe>
	</cffunction>
</cfcomponent>