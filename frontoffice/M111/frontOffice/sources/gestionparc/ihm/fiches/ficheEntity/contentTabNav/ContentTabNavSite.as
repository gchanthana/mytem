package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.FicheSiteIHM;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.site.SiteServices;
	
	import mx.containers.VBox;
	import mx.controls.Label;
	import mx.controls.Text;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de l'onglet site.
	 * 
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 06.10.2010
	 * 
	 * </pre></p>
	 */
	[Bindable]
	public class ContentTabNavSite extends VBox
	{
		// VARIABLES------------------------------------------------------------------------------
		
		// IHM components		
		public var txtMsgSite		:Text;
		public var txtEmplacement	:Text;
		public var txtSite			:Text;
		public var txtAdresse		:Text;
		public var txtBatiment		:Text;
		public var txtEtage			:Text;
		public var txtSalle			:Text;
		public var txtCouloir		:Text;
		public var txtArmoire		:Text;
		
		public var lbSite			:Label;
		public var lbEmplacement	:Label;
		public var lbCodePostal		:Label;
		public var lbVille			:Label;
		public var lbPays			:Label;		
		
		public var vbEmplacement	:VBox;
				
		// publics variables
		public var myServicesCache 	: CacheServices = null
		
		// privates variables
		private var myServices		: SiteServices = null;
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLICS FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ContentTabNavSite.
		 * </pre></p>
		 */	
		public function ContentTabNavSite()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		// PUBLICS FUNCTIONS-------------------------------------------------------------------------

		// PRIVATES FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Appel de la fonction <code>initListener()</code>.
		 * - Initialisation du service.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 * 
		 * @see #initListener()
		 * @see gestionparc.services.site.SiteServices
		 */
		private function init(event:FlexEvent):void
		{
			//myServices = new SiteServices();
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la fiche site.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */
		private function displayFicheSite(isNewFiche:Boolean,idSite:int):void
		{
			var ficheSite : FicheSiteIHM = new FicheSiteIHM();
			ficheSite.isNewSite = isNewFiche;
			
			if(!isNewFiche)
			{
				ficheSite.idSite = idSite;
			}
			
			ficheSite.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED,onInfosFicheSiteUploadedHandler);
			PopUpManager.addPopUp(ficheSite, this.parent.parent, true);
			PopUpManager.centerPopUp(ficheSite);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appellée après la mise a jour des infos d'un site depuis sa fiche.
		 * </pre></p>
		 * 
		 * @param event
		 * @return void
		 */
		private function onInfosFicheSiteUploadedHandler(event:gestionparcEvent):void
		{
			//resetInfosEmplacement();
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED,true));
		}			
	}
}