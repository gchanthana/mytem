package gestionparc.ihm.question.sim
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.DateField;

	public class QuestionAssocierEqpToSim
	{
		import flash.display.Sprite;

		import gestionparc.ihm.fiches.listedispo.IListeDispo;

		import mx.controls.Alert;
		import mx.core.Application;
		import mx.resources.ResourceManager;

		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionAssocierEqpToSim(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=obj_dispo;
			this.check=check;
			this.date=date;
			associerEqpToSim();
		}

		private function associerEqpToSim():void
		{
			serv.associerEqpToSim(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;
			obj.ITEM2=liste.getSelection().IMEI;
			obj.ID_TERMINAL=liste.getSelection().IDTERM;
			obj.ID_SIM=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
