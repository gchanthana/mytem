<cfcomponent author="Cedric" extends="fr.saaswedo.utils.remoting.RemotingClient"
hint="Utilitaire de remoting (HTTP). Le username et le password doivent etre renseignés pour que les crédentiels HTTP soient fournis au service">
	<cffunction access="public" name="invoke" returntype="Struct" output="false"
	hint="Instancie le WebService puis effectue l'appel de la méthode et retourne le résultat comme spécifié par la fonction parent.
	Le contenu de la requete cliente n'est pas présent dans le résultat retourné car il n'est pas possible de l'obtenir avec ColdFusion.
	Le contenu de la réponse serveur n'est pas présent dans le résultat retourné car il correspond déjà à la valeur associé à la clé RESULT().
	Toute exception causée par le WebService est capturée puis loggée (sans stockage) avant d'etre ajoutée dans la structure retournée">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfargument name="procedure" type="String" required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="parameters" type="Array" required="true" hint="Liste des headers et paramètres HTTP qui doivent tous avoir une valeur">
		<cfset var result={}>
		<cfset var serviceInfos=ARGUMENTS.service>
		<cftry>
			<cfset var endpoint=SUPER.getEndpoint(serviceInfos)>
			<cfif LEN(TRIM(endpoint)) GT 0>
				<cfset var procParams=ARGUMENTS.parameters>
				<cfset var parameterCount=arrayLen(procParams)>
				<!--- Requete HTTP --->
				<cfset var httpConfig={url=endpoint,method=getHttpMethod(serviceInfos),result="httpResponse"}>
				<cfset setHttpCredentials(serviceInfos,httpConfig)>
				<cfhttp attributecollection="#httpConfig#">
					<cfloop index="i" from="1" to="#parameterCount#">
						<cfset var param=procParams[i]>
						<cfset var paramAttributes={type=getParamType(param),value=SUPER.getParamValue(param)}>
						<cfset var paramName=SUPER.getParamName(param)>
						<cfif LEN(TRIM(paramName)) GT 0>
							<cfset paramAttributes.name=paramName>
						</cfif>
						<cfhttpparam attributecollection="#paramAttributes#">
					</cfloop>
				</cfhttp>
				<cfset result[SUPER.STATUS()]=isDefined("httpResponse") ? SUPER.DEFINED():SUPER.UNDEFINED()>
				<cfif result[SUPER.STATUS()] EQ SUPER.DEFINED()>
					<cfset result[SUPER.RESULT()]=httpResponse>
				</cfif>
			<cfelse>
				<cfthrow type="Custom" message="Unable to invoke service" detail="Endpoint is undefined or empty">
			</cfif>
			<cfcatch type="Any">
				<cfset result=(isDefined("httpResponse") EQ TRUE) ? getErrorResult(CFCATCH,httpResponse):getErrorResult(CFCATCH)>
				<cfset var errorMessage=CFCATCH.message>
				<cfif LEN(TRIM(CFCATCH.detail)) GT 0>
					<cfset errorMessage=errorMessage & " [Detail:" & CFCATCH.detail & "]">
				</cfif>
				<cflog type="error" text="HTTP invoke() error: #errorMessage#">
			</cfcatch>
			<cffinally>
				<cfreturn result>
			</cffinally>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getResponseHttpCode" returntype="Numeric" hint="Retourne la code HTTP d'un résultat de invoke() ou -1 sinon">
		<cfargument name="invokeResult" type="Struct" required="true" hint="Résultat retourné par un appel à invoke()">
		<cfset var httpResult=ARGUMENTS.invokeResult>
		<cfset var httpCode=-1>
		<cfif httpResult[STATUS()] EQ DEFINED()>
			<cfset var httpResponse=httpResult[RESULT()]>
			<cfif structKeyExists(httpResponse,"ResponseHeader") AND structKeyExists(httpResponse.ResponseHeader,"Status_Code")>
				<cfset httpCode=VAL(httpResponse.ResponseHeader.Status_Code)>
			</cfif>
		</cfif>
		<cfreturn httpCode>
	</cffunction>
	
	<cffunction access="public" name="getParamType" returntype="String" hint="Retourne le type du paramètre HTTP ou lève une exception sinon.
	Un paramètre doit avoir un type CFHTTPPARAM valide : BODY,CGI,COOKIE,FILE,FORMFIELD,HEADER,URL,XML">
		<cfargument name="parameter" type="Struct" required="true" hint="Paramètre d'une liste de paramètres à fournir à une procédure">
		<cfif structKeyExists(ARGUMENTS.parameter,"TYPE")>
			<cfif LEN(TRIM(ARGUMENTS.parameter.TYPE)) GT 0>
				<cfreturn ARGUMENTS.parameter.TYPE>
			<cfelse>
				<cfthrow type="Custom" message="Invalid HTTP parameter" detail="Parameter HTTP type is empty">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" message="Invalid HTTP parameter" detail="Parameter HTTP type is undefined">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getHttpMethod" returntype="String" hint="Retourne la méthode HTTP à utiliser pour invoke() ou HTTP_GET() sinon">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfset var httpMethod=SUPER.getServiceProperty(ARGUMENTS.service,"HTTP_METHOD")>
		<cfif LEN(TRIM(httpMethod))>
			<cfreturn httpMethod>
		<cfelse>
			<cfreturn HTTP_GET()>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="HTTP_GET" returntype="String" hint="Valeur correspondant à la méthode HTTP GET">
		<cfreturn "GET">
	</cffunction>

	<cffunction access="public" name="HTTP_POST" returntype="String" hint="Valeur correspondant à la méthode HTTP POST">
		<cfreturn "POST">
	</cffunction>
	
	<cffunction access="public" name="HTTP_PUT" returntype="String" hint="Valeur correspondant à la méthode HTTP PUT">
		<cfreturn "PUT">
	</cffunction>
	
	<cffunction access="public" name="HTTP_DELETE" returntype="String" hint="Valeur correspondant à la méthode HTTP DELETE">
		<cfreturn "DELETE">
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.utils.remoting.HttpClient" hint="Constructeur">
		<cfset SUPER.init()>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="setHttpCredentials" returntype="void" hint="Appelée par invoke() pour ajouter définir les crédentiels HTTP">
		<cfargument name="service" type="Struct" required="true" hint="Informations concernant le service">
		<cfargument name="httpConfig" type="Struct" required="true" hint="AttributeCollection fourni par invoke() à CFHTTP">
		<cfset var username=SUPER.getUsername(ARGUMENTS.service)>
		<cfset var password=SUPER.getPassword(ARGUMENTS.service)>
		<cfif (LEN(TRIM(username)) GT 0) AND (LEN(TRIM(password)) GT 0)>
			<cfset var cfHttpConfig=ARGUMENTS.httpConfig>
			<cfset cfHttpConfig.username=username>
			<cfset cfHttpConfig.password=password>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="setServerResponse" returntype="void" hint="Appelée par invoke() pour ajouter la réponse SOAP (XML)
	dans le résultat qui sera retourné par invoke() si elle est accessible et qu'elle n'a pas encore été ajoutée dedans">
		<cfargument name="invokeResult" type="Struct" required="true" hint="Structure provenant de invoke() et qui sera retournée par invoke()">
		<cfargument name="httpResponse" type="Struct" required="true" hint="Réponse HTTP du service (CFHTTP)">
		<cfset var result=ARGUMENTS.invokeResult>
		<cftry>
			<cfif NOT structKeyExists(result,SUPER.SERVER_RESPONSE())>
				<cfset result[SUPER.SERVER_RESPONSE()]=ARGUMENTS.httpResponse>
			</cfif>
			<cfcatch type="Any">
				<cfset var errorMessage=CFCATCH.message>
				<cfif LEN(TRIM(CFCATCH.detail)) GT 0>
					<cfset errorMessage=errorMessage & " [Detail:" & CFCATCH.detail & "]">
				</cfif>
				<!--- Si la réponse client ne peut pas etre obtenue alors c'est un avertissement non bloquant (i.e Pas d'erreur) --->
				<cflog type="warning" text="Unable to add HTTP response in invoke() result: #errorMessage#">
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="getErrorResult" returntype="Struct"
	hint="Appelée par invoke() pour retourner affecter l'exception error à la clé RESULT(). Le paramètre httpResponse est fourni s'il est défini">
		<cfargument name="error" type="Any" required="true" hint="Exception ColdFusion durant la fonction invoke()">
		<cfargument name="httpResponse" type="Struct" required="false" hint="Réponse HTTP du service (CFHTTP)">
		<cfset var errorResult={}>
		<cfset var cfException=ARGUMENTS.error>
		<cfset errorResult[SUPER.STATUS()]=SUPER.ERROR()>
		<cfif structKeyExists(ARGUMENTS,"httpResponse") AND isDefined("ARGUMENTS.httpResponse")>
			<cfset setServerResponse(errorResult,ARGUMENTS.httpResponse)>
			<!--- Erreur de type AxisFault --->
			<cfif structKeyExists(cfException,"faultString")>
				<cfset errorResult[SUPER.STATUS()]=FAULT()>
			</cfif>
		</cfif>
		<!--- L'exception error est affectée à la clé RESULT() --->
		<cfset errorResult[SUPER.RESULT()]=ARGUMENTS.error>
		<cfreturn errorResult>
	</cffunction>
</cfcomponent>