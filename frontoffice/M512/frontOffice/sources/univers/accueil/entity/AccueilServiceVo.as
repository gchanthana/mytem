package accueil.entity
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.ByteArray;

	[Bindable]
	public class AccueilServiceVo extends EventDispatcher
	{
		public function AccueilServiceVo(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		private var _DESC:String;
		private var _ID:Number;
		private var _IMAGE:ByteArray;
		private var _URL:String;
		
		

		public function set DESC(value:String):void
		{
			_DESC = value;
		}

		public function get DESC():String
		{
			return _DESC;
		}
		public function set ID(value:Number):void
		{
			_ID = value;
		}

		public function get ID():Number
		{
			return _ID;
		}
		public function set IMAGE(value:ByteArray):void
		{
			_IMAGE = value;
		}

		public function get IMAGE():ByteArray
		{
			return _IMAGE;
		}
		public function set URL(value:String):void
		{
			_URL = value;
		}

		public function get URL():String
		{
			return _URL;
		}
		
		public function fill(value:Object):void
		{
			IMAGE = value.IMAGE;
			URL = value.URL;
			DESC = value.DESC;
			ID = value.ID;
		}
	}
}