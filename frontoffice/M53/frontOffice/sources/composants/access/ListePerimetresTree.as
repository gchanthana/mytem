package composants.access {
	import mx.events.FlexEvent;
	
	/*
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.controls.Tree;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import flash.utils.getQualifiedClassName;
	import mx.collections.XMLListCollection;
	import mx.collections.ICollectionView;
	import mx.collections.ArrayCollection;
	*/
	
	public class ListePerimetresTree extends ListePerimetresTreeIHM {
		/*
		protected var xmlResult:XMLList = null;
		protected var xmlNodeIdResult:XMLList = null;
		*/
		
		public function ListePerimetresTree() {
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			/*
			this.dataProvider = CvAccessManager.getListePerimetres() as XMLList;
			this.labelField = "@LABEL";
			*/
		}
		
		/*
		public function xmlSearch(nodeItem:Object,attributeName:String,keyword:String):XMLList {
			return nodeItem.(@[attributeName].toString().toLowerCase().search(keyword.toLowerCase()) > -1);
		}

		public function xmlSearchByNodeId(nodeItem:Object,nodeId:int):XMLList {
			return nodeItem.(@NODE_ID == nodeId);
		}

		protected function expandToNode(nodeItem:Object,open:Boolean):Object {
			var tmpParent:XML = nodeItem.parent() as XML;
			while(tmpParent != null) {
				expandItem(tmpParent,open);
				tmpParent = tmpParent.parent() as XML;
			}
			return nodeItem;
		}

		protected function selectGivenNode(nodeItem:Object,scrollTo:Boolean):void {
			selectedItem = nodeItem;
			if(scrollTo)
				scrollToIndex(selectedIndex);
		}

		public function selectNodeByIndex(nodeIndex:int,scrollTo:Boolean):void {
			expandToNode(xmlResult[nodeIndex],scrollTo);
			selectGivenNode(xmlResult[nodeIndex],scrollTo);
		}

		public function selectAutoNodeByIndex(nodeIndex:int,scrollTo:Boolean):void {
			expandToNode(xmlNodeIdResult[nodeIndex],scrollTo);
			selectGivenNode(xmlNodeIdResult[nodeIndex],scrollTo);
		}

		public function clearLastSearch():void {
			xmlResult = null;
		}

		public function searchNode(keyword:String):int {
			var selectedObj:XML = null;
			var nbChild:int = 0;
			var childNodes:XMLList = null;
			var tmpParent:XML = null;
			if(selectedItem == null) { // Aucun noeud séléctionné
				selectedIndex = 0;
				selectedObj = selectedItem as XML;
			}
			childNodes = selectedItem.descendants();
			xmlResult = xmlSearch(childNodes,"LABEL",keyword);
			if(xmlResult.length() > 0) {
				return xmlResult.length();
			} else {
				xmlResult = null;
				return 0;
			}
		}

		public function searchNodeByNodeId(nodeId:int):int {
			var selectedObj:XML = null;
			var nbChild:int = 0;
			var childNodes:XMLList = null;
			var tmpParent:XML = null;
			if(selectedItem == null) { // Aucun noeud séléctionné
				selectedIndex = 0;
				selectedObj = selectedItem as XML;
			}
			childNodes = selectedItem.descendants();
			xmlResult = xmlSearchByNodeId(childNodes,nodeId);
			if(xmlResult.length() > 0) {
				return xmlResult.length();
			} else {
				xmlResult = null;
				return 0;
			}
		}
		*/
	}
}
