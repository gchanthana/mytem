<cfheader name="Content-Disposition" value="inline;filename=rapport_par_famille_#replace(rapportParams.RAISON_SOCIALE,' ','_')#.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY euro "&#8364;">]>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>CONSOTEL</Author>
  <LastAuthor>cedric.rapiera</LastAuthor>
  <Created>2007-02-05T08:50:51Z</Created>
  <LastSaved>2007-02-05T16:30:22Z</LastSaved>
  <Version>11.6408</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10005</WindowHeight>
  <WindowWidth>10005</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>135</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font x:Family="Swiss" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s48" ss:Name="Mon�taire">
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="m169322572">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169322582">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169322592">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169322602">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169322612">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m169322622">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m169322632">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m169326780">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169326790">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169326800">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169326810">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169326820">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m169326830">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m169326840">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s62">
   <Font x:Family="Swiss" ss:Size="14" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s70">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s71">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s72">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s74" ss:Parent="s48">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s75" ss:Parent="s48">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s76" ss:Parent="s48">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s77">
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s78">
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
 </Styles>

<cfset MAX_ROWS = 50000>
<cfset p = 0>
<cfloop index="i" from="1" to="#qGetData.recordcount#" step="#(MAX_ROWS * 6)#">
	<cfset p = p + 1>
	<cfoutput>
 <Worksheet ss:Name="Feuille #p#">
  <Table x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="90">
   <Column ss:AutoFitWidth="0" ss:Width="270"/>
   <Column ss:Index="2" ss:Width="130"/>
   <Column ss:Index="3" ss:Width="150"/>
   <Column ss:Width="250"/>
   <Column ss:Width="120"/>
   <Column ss:Width="120"/>
   <Column ss:Width="120"/>
   <Column ss:Width="120"/>
   <Column ss:Width="120"/>
   <Column ss:Width="120"/>
   <Column ss:Width="100"/>
   <Column ss:AutoFitWidth="0" ss:Width="75"/>
   <Row ss:AutoFitHeight="0" ss:Height="18">
    <Cell ss:StyleID="s62"><Data ss:Type="String">Etat des Consommations</Data></Cell>
    <Cell ss:Index="4" ss:StyleID="s62"><Data ss:Type="String">#rapportParams.RAISON_SOCIALE#</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">P�riode</Data></Cell>
    <Cell><Data ss:Type="String">#LSDateFormat(parseDateTime(rapportParams.DATEDEB),'Mmmm YYYY')# � fin #LSDateFormat(parseDateTime(rapportParams.DATEFIN),'Mmmm YYYY')#</Data></Cell>
	</cfoutput>
   </Row>
   <Row ss:Index="4">
    <Cell ss:MergeDown="1" ss:StyleID="m169322582"><Data ss:Type="String">Organisation</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m169322592"><Data ss:Type="String">Num�ro</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m169322602"><Data ss:Type="String">Fonction</Data></Cell>
	
    <Cell ss:MergeDown="1" ss:StyleID="m169322602"><Data ss:Type="String">Site G�ographique</Data></Cell>
	
    <Cell ss:MergeAcross="1" ss:StyleID="m169322612"><Data ss:Type="String">Data</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="m169322622"><Data ss:Type="String">Filaire</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="m169322632"><Data ss:Type="String">Mobile</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m169322572"><Data ss:Type="String">Total</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="13.5">
    <Cell ss:Index="5" ss:StyleID="s69"><Data ss:Type="String">Montant Abonnement</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">Montant Consos</Data></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Montant Abonnement</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">Montant Consos</Data></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">Montant Abonnement</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">Montant Consos</Data></Cell>
   </Row>
	<cfset k = i>
	<cfset nbl = 0>
	<cfloop index="y" from="1" to="#MAX_ROWS#">
		<cfif k GTE qGetData.recordcount>
			<cfbreak>
		</cfif>
		<cfoutput>
		   <Row ss:AutoFitHeight="0" ss:Height="13.5">
		    <Cell ss:StyleID="s71"><Data ss:Type="String">#qGetData['LIBELLE_GROUPE_CLIENT'][k]#</Data></Cell>
		    <Cell ss:StyleID="s72"><Data ss:Type="String" x:Ticked="1">#qGetData['SOUS_TETE'][k]#</Data></Cell>
		    <Cell ss:StyleID="s72"><Data ss:Type="String">#qGetData['USAGES'][k]#</Data></Cell>
		    <Cell ss:StyleID="s72"><Data ss:Type="String" x:Ticked="1">#qGetData['NOM_SITE'][k]#</Data></Cell>
		    <Cell ss:StyleID="s74"><Data ss:Type="Number">#qGetData['MONTANT_FINAL'][k]#</Data></Cell>
		    <Cell ss:StyleID="s75"><Data ss:Type="Number">#qGetData['MONTANT_FINAL'][k + 1]#</Data></Cell>
		    <Cell ss:StyleID="s74"><Data ss:Type="Number">#qGetData['MONTANT_FINAL'][k + 2]#</Data></Cell>
		    <Cell ss:StyleID="s75"><Data ss:Type="Number">#qGetData['MONTANT_FINAL'][k + 3]#</Data></Cell>
		    <Cell ss:StyleID="s74"><Data ss:Type="Number">#qGetData['MONTANT_FINAL'][k + 4]#</Data></Cell>
		    <Cell ss:StyleID="s75"><Data ss:Type="Number">#qGetData['MONTANT_FINAL'][k + 5]#</Data></Cell>
		    <Cell ss:StyleID="s76" ss:Formula="=SUM(RC[-6]:RC[-1])"><Data ss:Type="Number"></Data></Cell>
		   </Row>
		</cfoutput>
		<cfset nbl = nbl + 1>
		<cfset k = k + 6>
	</cfloop>
   <Row>
	<cfoutput>
	    <Cell ss:Index="4" ss:StyleID="s77"><Data ss:Type="String">Total</Data></Cell>
	    <Cell ss:StyleID="s78" ss:Formula="=SUM(R[-#nbl#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	    <Cell ss:StyleID="s78" ss:Formula="=SUM(R[-#nbl#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	    <Cell ss:StyleID="s78" ss:Formula="=SUM(R[-#nbl#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	    <Cell ss:StyleID="s78" ss:Formula="=SUM(R[-#nbl#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	    <Cell ss:StyleID="s78" ss:Formula="=SUM(R[-#nbl#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	    <Cell ss:StyleID="s78" ss:Formula="=SUM(R[-#nbl#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	    <Cell ss:StyleID="s78" ss:Formula="=SUM(R[-#nbl#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	</cfoutput>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <RangeSelection>R1</RangeSelection>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</cfloop>
</Workbook>
