package service.fonction.FctGestionCollecte
{
    import composants.util.ConsoviewAlert;
    
    import entity.GestionCollecte.vo.CollecteClientVO;
    import entity.GestionCollecte.vo.CollecteTypeVO;
    import entity.GestionCollecte.vo.OperateurVO;
    import entity.GestionCollecte.vo.SimpleEntityVO;
    
    import event.fonction.fctGestionCollecte.FctGestionCollecteEvent;
    
    import flash.events.EventDispatcher;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.events.ResultEvent;
    import mx.utils.StringUtil;
    
    import utils.MessageAlerte;

    [Bindable]
    public class FctGestionCollecteHandlers extends EventDispatcher
    {
        private var myDatas:FctGestionCollecteDatas;

        public function FctGestionCollecteHandlers(instanceOfDatas:FctGestionCollecteDatas)
        {
            myDatas = instanceOfDatas;
        }

        /******************************************************************************
         *                         Admin et Client : Procédures communes
         ******************************************************************************/ /**
         * Récupération de la liste des opérateurs
         *
         */
        internal function getOperateurListResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatOperateurList(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /**
         * Récupération de la liste des opérateurs possédant un template
         *
         */
        internal function getOperateurWithTemplateListResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatOperateurWithTemplateList(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /**
         * Récupération de la liste des types de données
         *
         */
        internal function getDataTypeListResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatDataTypeList(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /**
         * Récupération de la liste des modes de récupération
         *
         */
        internal function getRecupModeListResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatRecupModeList(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /******************************************************************************
         *                         Admin : Gestion des Templates
         ****************************************************************************/ /**
         * Retourne la liste des Template de collectes
         * @param re
         *
         */
        internal function getCollecteTypeListResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatCollecteTypeList(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /**
         * Si l'édition/création s'est bien déroulée , retourne l'id du template
         * sinon code erreur (TODO)
         * @param re
         *
         */
        internal function createEditCollecteTypeVoResultHandler(re:ResultEvent):void
        {
            if(re.result.DATA.RESULT)
            {
                var num:Number = re.result.DATA.RESULT as Number;
                switch(num)
                {
                    case -10:
                        trace("Champs obligatoires vides");
                        dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
                        break;
                    case -11:
                        trace("Template existant");
                        dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
                        break;
                    case -12:
                        trace("Impossible de publier");
                        dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, num));
                        break;
                    default:
                        dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS, num));
                        break;
                }
            }
            else
            {
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR));
                trace("Erreur dans l'édition,création du template de collecte");
            }
        }

        /**
         * Retourne la liste des groupes maitres
         *
         */
        internal function getRacineListResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatRacineList(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /**
         * Retourne l'UID créee pour le stockage des docs associé à un
         * template
         * @param re
         *
         */
        internal function createUIDStorageResultHandler(re:ResultEvent):void
        {
            myDatas.uid = "";
            if(re.result != null)
            {
                myDatas.uid = re.result.DATA.RESULT as String;
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS));
            }
            else
            {
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR));
                trace("Erreur dans la creation d'un uid");
            }
        }

        /**
         * Réponse serveur sur la destruction de l'uid
         *
         */
        internal function destroyLastUIDResultHandler(re:ResultEvent):void
        {
            if(re.result as Number == 1)
            {
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_SUCCESS));
            }
            else
            {
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_UID_DESTROY_ERROR));
                trace("Erreur dans la destruction du dossier UID de la collecte");
            }
        }

        /******************************************************************************
         *                         Client : Gestion des Collectes
         ****************************************************************************/ /**
         * Retourne la liste des collectes clientes pour un client donné
         * @param re
         *
         */
        internal function getCollecteClientListResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatCollecteClientList(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /**
         * Retourne la liste des Template à disposition du client
         * pour la création de sa collecte
         * @param re
         *
         */
        internal function getPublishedTemplateResultHandler(re:ResultEvent):void
        {
			if(re.result != null && re.result.DATA is Object)
			{
				myDatas.treatPublishedTemplate(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(), MessageAlerte.msgErrorPopupTitle);
			}
        }

        /**
         * Si l'édition/création d'une collecte s'est bien déroulée , retourne l'id du template
         * sinon code erreur (TODO)
         * @param re
         *
         */
        internal function createEditCollecteClientVoResultHandler(re:ResultEvent):void
        {
            if(re.result.DATA.RESULT > 0)
            {
                var idCollecte:Number = re.result.DATA.RESULT as Number;
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_SUCCESS, idCollecte));
            }
            else
            {
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.COLLECTE_CREATED_UPDATED_ERROR));
                trace("Erreur dans l'édition,création de la collecte");
            }
        }

        /**
         *  Résultat des test d'identification
         *
         */
        internal function testIdentificationResultHandler(re:ResultEvent):void
        {
            if(re.result == 1)
            {
                myDatas.identificationSuccess = true;
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEST_IDENTIFICATION_SUCCESS));
            }
            else
            {
                dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.RECUPMODE_LIST_LOADED_ERROR));
                myDatas.identificationSuccess = false;
                trace("Erreur dans le test d'identification");
            }
        }
    }
}