package gestionparc.services.associer
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class ActionAssocierServices
	{
		
		public var myDatas 		: ActionAssocierDatas;
		public var myHandlers 	: ActionAssocierHandlers;
		
		private var urlBackoffice:String = "fr.consotel.consoview.M111.actions.Associer";
		
		public function ActionAssocierServices()
		{
			myDatas 	= new ActionAssocierDatas();
			myHandlers 	= new ActionAssocierHandlers(myDatas);
		}
		
		public function associerFournirEqp1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirEqp1",
				myHandlers.associerFournirEqp1ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}

		public function associerFournirEqp2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirEqp2",
				myHandlers.associerFournirEqp1ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}

		public function associerFournirEqp3(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirEqp3",
				myHandlers.associerFournirEqp3ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}

		public function associerFournirEqp4(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirEqp4",
				myHandlers.associerFournirEqp4ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		/* LIGNE SIM */
		public function associerFournirLigneSim1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigneSim1",
				myHandlers.associerFournirLigneSim1ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerFournirLigneExt1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigneExt1",
				myHandlers.associerFournirLigneExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerFournirLigneSim2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigneSim2",
				myHandlers.associerFournirLigneSim2ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerFournirLigneExt2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigneExt2",
				myHandlers.associerFournirLigneExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerFournirLigneSim3(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigneSim3",
				myHandlers.associerFournirLigneSim3ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerFournirLigneExt3(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigneExt3",
				myHandlers.associerFournirLigneExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
	
		/* LIGNE */
		public function associerFournirLigne1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigne1",
				myHandlers.associerFournirLigne1ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerFournirLigne2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigne2",
				myHandlers.associerFournirLigne2ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerFournirLigne3(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigne3",
				myHandlers.associerFournirLigne3ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerFournirLigne4(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerFournirLigne4",
				myHandlers.associerFournirLigne3ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		/**	 ASSOCIER A UN EQP	*/
		
		// COLLABORATEUR
		public function associerCollabToEqp1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToEqp",
				myHandlers.associerCollabToEqpResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		//LIGNESIM
		public function associerLigneSimToEqp1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerLigneSimToEqp",
				myHandlers.associerLigneSimToEqpResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		// ligne / extesion
		public function associerLigneExtToEquipement(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerLigneExtToEqp",
				myHandlers.associerLigneExtToEqpResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerExtToEquipementWithSim(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerExtToEquipementWithSim",
				myHandlers.associerLigneExtToEqpResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		
		//LIGNE
		public function associerLigneToEqp1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerLigneToEqp",
				myHandlers.associerLigneToEqpResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		//SOUS EQP
		public function associerSousEqpToEqp1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerSousEqpToEqp",
				myHandlers.associerSousEqpToEqpResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		/** 	ASSOCIER A UNE LIGNE  */
		// COLLABORATEUR A UNE LIGNE MOBILE
		public function associerCollabToLigneSim1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToLigneSim1",
				myHandlers.associerCollabToLigne1ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerCollabToLigneSim2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToLigneSim2",
				myHandlers.associerCollabToLigne2ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerCollabToLigneSim3(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToLigneSim3",
				myHandlers.associerCollabToLigne3ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		//COLLABORATEUR A UNE LIGNE FIXE
		public function associerCollabToLigne1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToLigne1",
				myHandlers.associerCollabToLigne1ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerCollabToLigne2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToLigne2",
				myHandlers.associerCollabToLigne2ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerCollabToLigne3(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToLigne3",
				myHandlers.associerCollabToLigne3ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerCollabToLigne4(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToLigne4",
				myHandlers.associerCollabToLigne4ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		// EQUIPEMENT
		public function associerEqpToLigne1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerEqpToLigne1",
				myHandlers.associerEqpToLigneResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		public function associerEqpToLigne2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerEqpToLigne2",
				myHandlers.associerEqpToLigneResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		// SIM
		public function associerSimToLigne(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerSimToLigne",
				myHandlers.associerSimToLigneResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerExtToLigne(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerExtToLigne",
				myHandlers.associerExtToLigneResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		
		/**	 ASSOCIER A UNE SIM	*/
		//COLLABORATEUR
		public function associerCollabToSim1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToSim1",
				myHandlers.associerCollabToSim1ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		
		public function associerCollabToExt1(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToExt1",
				myHandlers.associerCollabToExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerCollabToSim2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToSim2",
				myHandlers.associerCollabToSim2ResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerCollabToExt2(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerCollabToExt2",
				myHandlers.associerCollabToExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		// LIGNE
		public function associerLigneToSim(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerLigneToSim",
				myHandlers.associerLigneToSimResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerLigneToExt(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerLigneToExt",
				myHandlers.associerLigneToExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		// extension equipement
		public function associerEqpToSim(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerEqpToSim",
				myHandlers.associerEqpToSimResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerEqpToExt(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerEqpToExt",
				myHandlers.associerEqpToExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
		
		public function associerEqpWithSimToExt(obj:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				urlBackoffice,
				"associerEqpWithSimToExt",
				myHandlers.associerEqpToExtResultHandler);
			
			RemoteObjectUtil.callService(op,obj); 
		}
	}
}