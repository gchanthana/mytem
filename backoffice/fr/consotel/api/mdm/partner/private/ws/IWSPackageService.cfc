<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSPackageService"
hint="Interface décrivant les méthodes du Service de gestion des entités de type Package">
	<cffunction access="public" name="getPackage" returntype="String" hint="Retourne la définition du package au format JSON<br>
	Le contenu de la définition est décrit comme suit par la documentation Zenprise :<br>
<pre>General information
  id (int): package internal id
  name (string): package name
  fullPath (string): package fullpath (include package name)
  osFamily (string): package OS family (can be &quot;WINDOWS&quot;, &quot;iOS&quot;, or &quot;ANDROID&quot;)
  defaultDeploy (boolean): package used as default deployment
Rule conditions
  rule (optional string): XML describing deployment conditions. See getPackageRule(String) for details.
Scheduling conditions
  scheduling (optional string): XML describing deployment scheduling. See getPackageSchedule(String) for details.
  deployOnce (boolean): package will not be deployed if server know a successfull deployment from history) 
Target groups
  recipients (array of string): list of target groups
  deployToAnonymous (boolean): deploy to anonymous 
Sub packages
  subPackages
      array of string : list of sub package names (to get sub-packages fullpath use package fullpath, append &quot;/&quot; and the name of sub-package)
      package json object : list of package for deep request (getPackages(String, boolean)) 
Package resources
  resources (array of object): list of resources. Resource object contains:
      type (string): resource type can be &quot;SERVERGROUP&quot;, , &quot;REGISTRY&quot;, &quot;XML&quot;, &quot;CAB&quot;, &quot;APK&quot;, &quot;SCRIPT&quot;, &quot;FILE&quot;, &quot;SOFTWARE_INVENTORY&quot;
      id (int): resource internal id
      name (string): resource name
      group (string): resource group
          same as type for &quot;WINDOWS&quot; OS family
          &quot;CONFIG&quot; for &quot;iOS&quot; and &quot;ANDROID&quot; OS families for resources of type &quot;REGISTRY&quot; and &quot;XML&quot;, same as type either
  Notes:
      Only type, id and name are important.
      For &quot;SOFTWARE_INVENTORY&quot; only type is important (id and name are set with default values). Considere this resource like a boolean (present or not).
      Console use group to abstract graphic user interface. Internally and for compatiility reasons configuration will be registries or xml. You can use group for your presentation.
      But only type information will be necessary for update/delete operation.</pre>">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>

	<cffunction access="public" name="setPackage" returntype="Void" hint="Spécifie la définition du package au format JSON">
		<cfargument name="jsonPkg" type="String" required="true" hint="Définition du package au format JSON<br>
		Le contenu de la définition est décrit comme suit par la documentation Zenprise :<br>
<pre>General information
  fullPath (mandatory string): package fullpath. For a rename this must be the old package path.
  name (optional string): This can be a new package name
  osFamily (mandatory string for creating a root package): package OS family (can be &quot;WINDOWS&quot;, &quot;iOS&quot;, or &quot;ANDROID&quot;)
  Inner package must follow parent&quot;s OS family. If a value is given, a check will occurs.
  defaultDeploy (optional boolean, default to false): this package must be used as default deployment. A default package cannot have recipients.
Rule conditions
  rule (optional string, default no rule): XML describing deployment conditions. See getPackageRule(String) for details.
Scheduling conditions
  scheduling (optional string, default deploy on device connection): XML describing deployment scheduling. See getPackageSchedule(String) for details.
  deployOnce (optional boolean, default deploy when scheduling condition occur): package will not be deployed if server know a successfull deployment from history) 
Target groups
  recipients (optional array of string, default no recipient): array of string recipients. A default package (defaultDeploy set to true) cannot have recipients.
  deployToAnonymous (boolean, default to false): deploy to anonymous 
Package resources
  resources (array of object, default empty resources): list of resources. Resource object contains:
      name (string): resource name
      type (string): resource type can be &quot;SERVERGROUP&quot;, &quot;TUNNEL&quot;, &quot;REGISTRY&quot;, &quot;XML&quot;, &quot;CAB&quot;,
		&quot;APK&quot;, &quot;SCRIPT&quot;, &quot;FILE&quot;, &quot;SOFTWARE_INVENTORY&quot;
  Notes:
      For &quot;SOFTWARE_INVENTORY&quot; only type is important (name will be set with default values). Considere this resource like a boolean (present or not).</pre><br>
	Voir aussi les méthodes : setPackageRule() et setPackageSchedule()">
	</cffunction>
	
	<cffunction access="public" name="getPackages" returntype="String" hint="Retourne la liste des définitions des packages au format JSON">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package ou une chaine vide pour indiquer <b>le package racine</b>">
		<cfargument name="deep" type="Boolean" required="true" hint="TRUE pour retourne la liste complète détaillée et FALSE sinon">
	</cffunction>
	
	<cffunction access="public" name="removePackage" returntype="Void" hint="Supprime la définition d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="setPackageRule" returntype="Void" hint="Définit la règle d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlRule" type="String" required="true" hint="Définition XML de la règle ou une chaine vide pour pour ignorer cette règle">
	</cffunction>
	
	<cffunction access="public" name="renamePackage" returntype="Void" hint="Renomme un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Actuel chemin absolu du package">
		<cfargument name="newPackageName" type="String" required="true" hint="Nouveau nom à donner au package">
	</cffunction>
	
	<cffunction access="public" name="checkPackageRule" returntype="Boolean" hint="Vérifie la définition de la règle et retourne TRUE si elle est valide et FALSE sinon">
		<cfargument name="xmlRule" type="String" required="true" hint="Définition XML de la règle">
	</cffunction>
	
	<cffunction access="public" name="getPackageRule" returntype="String" hint="Retourne la définition XML de la règle du package (Voir documentation Zenprise pour la DTD)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="removePackageRule" returntype="Void" hint="Supprime la règle du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="isDeployOnce" returntype="Boolean" hint="Retourne TRUE si la propriété deployOnce du package est activé et FALSE sinon. Voir setDeployOnce()">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="setDeployOnce" returntype="Void" hint="Affecte la valeur deployOnce à la propriété correspondante du package<br>
	Lorsque cette propriété vaut TRUE alors le serveur ne renvoie pas de données au client si un précédent déploiement s'est déjà effectué avec succès (Via l'historique de déploiement)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="deployOnce" type="Boolean" required="true" hint="Valeur à affecter à la propriété deployOnce du package">
	</cffunction>
	
	<cffunction access="public" name="deploy" returntype="Void"
	hint="Effectue une demande de déploiement pour les équipement qui sont connectés au serveur (Android et Windows). Envoie une notification PUSH pour iOS">
	</cffunction>
	
	<cffunction access="public" name="getPackageList" returntype="Array" hint="Retourne la liste des noms des packages racines dans un tableau">
	</cffunction>
	
	<cffunction access="public" name="createPackage" returntype="String" hint="Crée un package avec le nom fourni dans pkgFullPath et retourne le nom">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="getPackageTunnelList" returntype="Array" hint="Retourne la liste des noms des tunnels du package dans un tableau">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageTunnel" returntype="Void" hint="Ajoute un tunnel dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageTunnel" returntype="Void" hint="Supprime un tunnel d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getTunnelPackageList" returntype="Array" hint="Retourne la liste des packages utilisant le tunnel tunnelId dans un tableau">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel">
	</cffunction>
	
	<cffunction access="public" name="getPackageGroupList" returntype="Array" hint="Retourne la liste des groupes déployés dans le package dans un tableau">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageGroup" returntype="Void" hint="Déploie le groupe groupId dans le package pkgFullPath">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
	</cffunction>
	
	<cffunction access="public" name="removePackageGroup" returntype="Void" hint="Supprime un groupe du package pkgFullPath">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
	</cffunction>
	
	<cffunction access="public" name="getGroupPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages dans lesquels le groupe groupId a été déployé">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
	</cffunction>
	
	<cffunction access="public" name="getPackageOSFamily" returntype="String" hint="Retourne l'OS cible du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="getPackageSchedule" returntype="String"
	hint="Retourne la configuration XML de la planification du package ou une chaine vide s'il n'est pas planifié (Voir documentation Zenprise pour la DTD)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="checkPackageSchedule" returntype="Boolean" hint="Retourne TRUE si la configuration XML de planification xmlSchedule est valide et FALSE sinon">
		<cfargument name="xmlSchedule" type="String" required="true" hint="Configuration XML de planification de package">
	</cffunction>
	
	<cffunction access="public" name="setPackageSchedule" returntype="Void" hint="Définit la configuration XML de planification xmlSchedule pour le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlSchedule" type="String" required="true" hint="Configuration XML de planification de package ou une chaine vide pour ignorer la planification">
	</cffunction>
	
	<cffunction access="public" name="removePackageSchedule" returntype="Void" hint="Supprime la configuration XML de planification pour le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="getPackageFileList" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers de ressources utilisés dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageFile" returntype="Void" hint="Ajoute un fichier de ressources dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources à ajouter sans préfixe">
	</cffunction>
	
	<cffunction access="public" name="removePackageFile" returntype="Void" hint="Supprime le fichier de ressources fileName du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources à supprimer sans préfixe">
	</cffunction>
	
	<cffunction access="public" name="getFilePackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant le fichier de ressources">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources">
	</cffunction>
	
	<cffunction access="public" name="getPackageXMLList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources XML utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageXML" returntype="Void" hint="Ajoute une ressource XML dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageXML" returntype="Void" hint="Supprime une ressource XML du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à suprimer">
	</cffunction>
	
	<cffunction access="public" name="getXMLPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à suprimer">
	</cffunction>
	
	<cffunction access="public" name="getPackageCABList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources CAB utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageCAB" returntype="Void" hint="Ajoute une ressource CAB dans un package de type WINDOWS">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageCAB" returntype="Void" hint="Supprime une ressource CAB d'un package de type WINDOWS">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getCABPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource CAB">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB">
	</cffunction>
	
	<cffunction access="public" name="getPackageAPKList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources APK d'un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageAPK" returntype="Void" hint="Ajoute une ressource APK à un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageAPK" returntype="Void" hint="Supprime une ressource APK d'un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
	</cffunction>
	
	<cffunction access="public" name="getAPKPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource APK">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
	</cffunction>
	
	<cffunction access="public" name="getPackageServerGroupList" returntype="Array" hint="Retourne un tableau contenant la liste des groupes de serveurs utilisés par le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageServerGroup" returntype="Void" hint="Ajoute un groupe de serveur à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageServerGroup" returntype="Void" hint="Supprime un groupe de serveur d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getServerGroupPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant le groupe de serveur">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur">
	</cffunction>
	
	<cffunction access="public" name="getPackageRegistriesList" returntype="Array" hint="Retourne un tableau contenant la liste des registres utilisés par le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageRegistry" returntype="Void" hint="Ajoute un registre à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageRegistry" returntype="Void" hint="Supprime un registre d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getRegistryPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant le registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : WINDOWS, ANDROID)">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à supprimer">
	</cffunction>
	
	<cffunction access="public" name="hasPackageSoftwareInventory" returntype="Boolean" hint="Retourne TRUE si le package contient un inventaire logiciel">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageSoftwareInventory" returntype="Void" hint="Définit l'inventaire logiciel du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="removePackageSoftwareInventory" returntype="Void" hint="Supprime l'inventaire logiciel du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="getSoftwareInventoryPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des package ayant un inventaire logiciel">
	</cffunction>
	
	<cffunction access="public" name="getPackageCfgList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources CFG utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageCfg" returntype="Void" hint="Ajoute une ressource CFG dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageCfg" returntype="Void" hint="Supprime une ressource CFG d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getCfgPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource CFG">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getPackageExtAppList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources EXT d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageExtApp" returntype="Void" hint="Ajoute une ressource EXT dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageExtApp" returntype="Void" hint="Supprime une ressource EXT d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getExtAppPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource EXT">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getPackageIpaList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources IPA d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
	</cffunction>
	
	<cffunction access="public" name="addPackageIpa" returntype="Void" hint="Ajoute un ressource IPA à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA à ajouter">
	</cffunction>
	
	<cffunction access="public" name="removePackageIpa" returntype="Void" hint="Supprime une ressource IPA d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA à supprimer">
	</cffunction>
	
	<cffunction access="public" name="getIpaPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource IPA">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA">
	</cffunction>
</cfinterface>