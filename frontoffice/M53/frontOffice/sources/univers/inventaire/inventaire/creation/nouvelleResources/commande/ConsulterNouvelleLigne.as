package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.info.StatuTInfo;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements.RapprochementsProduits;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements.RapprochementsProduitsIHM;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.CommandeExportable;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.selectionDesProduits.PanelDemandeEvent;
	import univers.inventaire.inventaire.export.ExportBuilder;
	
	/**
	 * Classe gérant l'ecran de visualisation d'une commande de 'Nouvelles Lgnes'
	 * */
	public class ConsulterNouvelleLigne extends ConsulterNouvelleLigneIHM implements IDemande
	{
		//Booléen placé à true lorsque l'on clique sur le bouton 'Envoyer'
		private var flagEnvoi : Boolean = false;
			
		
		//Référence vers l'ecran de rapprochement		
		private var rapprochement : RapprochementsProduitsIHM;
		
		
		//Réference vers la commande
		private var commande : Commande;
		
		
		//Réference vers le panier de la commande
		[Bindable]
		private var panier : ElementCommande;
		
		
		//identifiant de la commande que l'on souhaite afficher	
		private var _idDemande : int = 0;
		
		/**
		 *Constructeur 
		 * */
		public function ConsulterNouvelleLigne()
		{
			super();
			addEventListener(FlexEvent.INITIALIZE,preInit);
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		
		/**
		 * Appel la methode clean 
		 * Ne fait rien pour le moment
		 * */	
		public function clean():void{						
			if (commande != null)commande.clean();
			if (panier != null) panier.clean();	
			if (rapprochement != null) rapprochement.clean();		
			pnlContact.logOff();
			
		}
		
		
		
		
		/**
		 * Setter pour l'identifiant de la commande que l'on souhaite afficher
		 * @param idd l'identifiant de la commande
		 * */
		public function set idDemande(idd : int):void{
			_idDemande = idd;
		}
		
		/**
		 * Retourne l'identifiant de la commande
		 * @return idDemande l'identifiant de la commande
		 * */
		public function get idDemande():int{
			return _idDemande;
		}
		
		/**
		 * Setter pour la commande que l'on souhaite afficher
		 * @param c la commande que l'on souhaite afficher
		 * */
		public function setCommande(c : Commande):void{
			commande = c;
		} 
		
		//Emmet un Flash avant d'afficher l'ecran
		private function preInit(fe :FlexEvent):void{
			EffectProvider.FadeThat(this,2000);
		}
		
		private var _modeEcriture : Boolean;
		[Bindable]
		public function get modeEcriture():Boolean{
			return _modeEcriture 
		}
		
		public function set modeEcriture(mode : Boolean):void{
			_modeEcriture = mode;
		}
		
		
		//Initilisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			//visible = false;
			
			btAnnuler.enabled = modeEcriture;
			btSortir.addEventListener(MouseEvent.CLICK,sortirDeLaCommande);
			btPDF.addEventListener(MouseEvent.CLICK,exporterPDF);									
			btRapprocher.addEventListener(MouseEvent.CLICK,rapprocherLigne);
			btAnnuler.addEventListener(MouseEvent.CLICK,btAnnulerClickHandler);
			btAccuserReception.addEventListener(MouseEvent.CLICK,btAccuserReceptionClickHandler);
			txtFiltreLignes.addEventListener(Event.CHANGE,filtrerGrid);
			
			myGridLignes.editable = modeEcriture;
			if (modeEcriture){
				myGridLignes.addEventListener(DataGridEvent.ITEM_EDIT_END,modifierCetteLigne);	
			}
			
			
			colRapp.labelFunction = formateFlagRapproche;
			
			if ((commande == null) && (idDemande  > 0)){
				commande = new Commande(idDemande);	
			}	
			initCommande();			
		}
		
		private function initCvDateEnvoi():void{
			if (pnlInfos.txtDateEffet.text != null){
				
				
					
				var selectableRange : Object = new Object();
				
				selectableRange.rangeStart = new Date(	pnlInfos.txtDateEffet.text.substr(6,4),
														Number(pnlInfos.txtDateEffet.text.substr(3,2))-1,
														Number(pnlInfos.txtDateEffet.text.substr(0,2)));
				cvDateEnvoi.selectableRange = selectableRange;
				cvDateEnvoi.selectedDate = new Date();
			}
			
		}
		
		//-----FILTRE
		
		//filtre le panier
		private function filtrerGrid(ev : Event):void{
			if (myGridLignes.dataProvider != null){
				(myGridLignes.dataProvider as ArrayCollection).filterFunction = filterFunc;
				(myGridLignes.dataProvider as ArrayCollection).refresh();	
			}			
		}
		
		//filtre sur les attribut libelletheme, libelleLigne, libelleProduit, flagRapproche commentaire des elements du panier
		private function filterFunc(value : ElementCommande):Boolean{
			if ((String(value.libelletheme).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1) 
				||
				(String(value.libelleLigne).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1)
				||
				(String(value.libelleProduit).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1)
				||
				(String(value.commentaire).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1)
				||
				(String(value.flagRapproche).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1))
			{
				return true;
			}else{
				return false;
			}
				
		}
		
		//----- FORMAT			
		
		//Formate les éléments de la colonne 'Rapproché' du tableau des lignes de la commande
		//Affiche R si flagRapproche = 1, si non affiche NR
		//Voir l'attribut labelFunction du DataGrid pour les parametres		
		private function formateFlagRapproche(item : Object, column : DataGridColumn):String{
			return (int(item[column.dataField]) == 1)? "R" : "NR";
		}
		
		//------COMMANDE		
		
		//Initialisation de la commande 
		//Affectation des écouteurs d'evenements
		private function initCommande():void{
			commande.addEventListener(Commande.CREATION_COMPLETE,afficherCommande);										
			commande.addEventListener(Commande.UPDATE_COMPLETE,commandeUpdated);
			commande.addEventListener(Commande.UPDATESTATUT_COMPLETE,commandeStatutUpdated);												
			panier = new ElementCommande();
			initPanier();				
		}
		
		//Dispatch un evenement 'PanelDemandeEvent' de type 'PanelDemandeEvent.SORTIR' signifiant que l'on souhaite sortir du module
		private function sortirDeLaCommande(me : MouseEvent):void{
			clean();
			rapprochement = null;
			dispatchEvent(new PanelDemandeEvent(PanelDemandeEvent.SORTIR));			
		}
		
		
		//Change le statut de la commande avec celui spécifié
		//@param idStatut le nouveau statut de la commande
		private function changerStatutCommande(idStaut : int):void{
			pnlInfos.changerStatutCommande(idStaut);
		}
		
		//Affiche la commande
		private function afficherCommande(ev : Event):void{
			if (flagEnvoi) dispatchEvent(new PanelDemandeEvent(PanelDemandeEvent.UPDATE_COMPLETE));
			//1.Informations
			pnlInfos.afficherInfosCommande(commande);
			callLater(initCvDateEnvoi);
			//2.Contact
			pnlContact.afficherContactCommande(commande);
			
			//3.Cible
			pnlCible.affciherCibleCommande(commande);
			//4.lignes						
			panier.prepareList(commande.commandeID);
			
			//date livraison
			setDateLivraisonRange();
			
			configRapprochement();
		}	
			
		private function setDateLivraisonRange():void{
			var selectableRange : Object = new Object();
			var dateStart : Date = new Date(commande.dateEnvoi);
			selectableRange.rangeStart = new Date(dateStart.fullYear,dateStart.month,dateStart.date + 1);	
			dcDateLivraison.selectableRange = selectableRange;
		}	
		
		//dispatch un evenement signifiant que les modifs ont été faites
		private function commandeUpdated(ev : Event):void{
			dispatchEvent(new PanelDemandeEvent(PanelDemandeEvent.UPDATE_COMPLETE));
			configRapprochement();
		}
		
		
		//dispatch un evenement signifiant que les modifs ont été faites
		private function commandeStatutUpdated(ev : Event):void{
			dispatchEvent(new PanelDemandeEvent(PanelDemandeEvent.UPDATE_COMPLETE));
		}
		
		//Ferme la fenetre des rapprochements et dispatche un evenement signifiant que l'on l'à validé
		private function fermerCommande(ev : Event):void{
			if (rapprochement != null){
				PopUpManager.removePopUp(rapprochement);	
				var evtObj : PanelDemandeEvent = new PanelDemandeEvent(PanelDemandeEvent.FERMER);
				evtObj.commande = commande;
				dispatchEvent(evtObj);					
			}			
		}

		//envoi la commande avec ou sans mail
		//verifie qu'il y ait un destinataire si on choisit l'option avec mail
		private function envoyerCommande(me : MouseEvent):void{
			
			if (cbMail.selected){				
				if (commande.contactID > 0){
					processEnvoiCommande(1);
				}else{
					Alert.show("Le contact est obligatoire pour l'envoi de mail");
				}
			}else{
				processEnvoiCommande(-1);
			}
			
		}
		
		private function processEnvoiCommande(flagMail : int):void{
			if(cvDateEnvoi.selectedDate != null){
				commande.flagMail = flagMail;
				commande.envoyer(cvDateEnvoi.text);	
				flagEnvoi = true;			
			}else{
				Alert.show("Il faut spécifier une date d'envoi");
			}
				
		}
		
		
		//Affiche le bouton rapprochement.
		private function configRapprochement():void{				
			switch(commande.statutID){
				case StatuTInfo.EN_ATTENTE_ENVOI : {
					btAccuserReception.enabled = false;
					btRapprocher.enabled = false;
					
					sectionEnvoyer.visible = true && modeEcriture;
					btEnvoyer.addEventListener(MouseEvent.CLICK,envoyerCommande);
					if (commande.contactID > 0){
						cbMail.visible = true && modeEcriture;
					}else{
						cbMail.visible = false;
					}
					break;
				}
				case StatuTInfo.EN_ATTENTE_LIVRAISON : {
					btAccuserReception.enabled = true && modeEcriture;
					btRapprocher.enabled = false;//true											 
					//btRapprocher.label = "Rapprocher";	
					sectionEnvoyer.visible = false;				
					break;
				}
				
				case StatuTInfo.EN_ATTENTE_RAPPROCHEMENT : {
					commande.statut = "En attente Rapprochement";
					btAccuserReception.enabled = false;
					btRapprocher.enabled = true && modeEcriture;
					btRapprocher.label = "Rapprocher";	
					sectionEnvoyer.visible = false;			
					break;
				}
				
				case StatuTInfo.RAPPROCHEE : {				
					btRapprocher.enabled = false;						 								
					sectionEnvoyer.visible = false;		
					btAccuserReception.enabled = false;
					break;
				}
				
				case StatuTInfo.COMMANDE_ANNULEE_APRES_ENVOI : {
					btAccuserReception.visible = false;
					btRapprocher.visible = false;
					btAnnuler.enabled = false;
					pnlContact.btRenvoyer.visible = false;
					sectionEnvoyer.visible = false;
					sectionEnvoyer.visible = false;	
					break;
				}
				case StatuTInfo.COMMANDE_ANNULEE_AVANT_ENVOI : {
					btAccuserReception.visible = false; 
					btRapprocher.visible = false;
					btAnnuler.enabled = false;				 								
					sectionEnvoyer.visible = false;
					sectionEnvoyer.visible = false;		
					break;
				}
				default : break;
			}
		}
		
		
		//Gere le click sur le bouton annuler
		//param in MouseEvent
		private function btAnnulerClickHandler(me : MouseEvent):void{
			annulerCommande();
		}	
		
		//Annule la commande 
		private function annulerCommande():void{
			ConsoviewAlert.afficherAlertConfirmation("Êtes vous sur de vouloir annuler cette commande","Demande de confirmation",processAnnulerCommandeHandler);
		}
		
		private function processAnnulerCommandeHandler(cle : CloseEvent):void{
			if (cle.detail == Alert.OK){
				var statut : int;
			
				switch (commande.statutID){
					case StatuTInfo.EN_ATTENTE_ENVOI : {
						statut = StatuTInfo.COMMANDE_ANNULEE_AVANT_ENVOI; 
						break;
					}
					case StatuTInfo.EN_ATTENTE_LIVRAISON : {
						statut = StatuTInfo.COMMANDE_ANNULEE_APRES_ENVOI;
						break;
					}
					case StatuTInfo.EN_ATTENTE_RAPPROCHEMENT : {
						statut = StatuTInfo.COMMANDE_ANNULEE_APRES_ENVOI;
						break;
					}
					default:throw new Error("Erreur statu");break;
				}
				
				changerStatutCommande(statut);		
			}
			
		}
		
		
		//Gerer le click sur le bouton btAccuserReception
		//param in MouseEvent
		private function btAccuserReceptionClickHandler(me : MouseEvent):void{
			//if (cle.detail == Alert.OK){
			//var statut : int  = StatuTInfo.EN_ATTENTE_RAPPROCHEMENT;
			if (dcDateLivraison.selectedDate !=  null){
				
				commande.dateCreation = DateFunction.formatDateAsString(new Date(commande.dateCreation));
				commande.dateEffective = DateFunction.formatDateAsString(new Date(commande.dateEffective));
				commande.dateEnvoi = DateFunction.formatDateAsString(new Date(commande.dateEnvoi));
				commande.dateLivraisonPrevue = DateFunction.formatDateAsString(new Date(commande.dateLivraisonPrevue));				
				commande.dateLivraison = dcDateLivraison.text;
				commande.statutID = StatuTInfo.EN_ATTENTE_RAPPROCHEMENT;
				commande.updateCommande();	
			}else{
				Alert.show("Vous devez sélectionner une date de réception","Erreur");
			}
			
			//}
			//ConsoviewAlert.afficherAlertConfirmation("Êtes vous sur de vouloir annuler cette commande","Demande de confirmation",processAccuserReceptionHandler);
		}
		
		private function processAccuserReceptionHandler(cle : CloseEvent):void{
			
		}
		
		
		///-----------PANIER
		
		//initialisation du panier de la commande
		//affectation des écouteurs d'évènements
		//affiche la commande le cas échéant
		private function initPanier():void{			
			panier.addEventListener(ElementCommande.LISTE_COMPLETE,afficherPanier);	
			panier.addEventListener(ElementCommande.UPDATE_COMPLETE,panierUpdated);
			if (commande){
				if (commande.commandeID > 0)afficherCommande(null);
			}
		}
		
		
		//recharge le panier
		private function panierUpdated(ev : Event):void{
			rafraichirPanier();
		}
		
		
		//recharge le panier de la commande		
		private function rafraichirPanier():void{
			panier.prepareList(commande.commandeID);	
			panier.liste.refresh();		
								
		}
		
		
		//affiche le Panier de la commande
		private function afficherPanier(ev : Event):void{
								
			myGridLignes.dataProvider = panier.liste;									
			panier.liste.refresh();								
		}

		
		//recharge les données du panier apres un changement d'etat d'une de ces lignes (NR->R)
		private function changerEtatLigne(ev : Event):void{			
			panier.prepareList(commande.commandeID);				
			
		}

		//Modifie le libelle d'une lignes et enregistre les modifications
		protected function modifierCetteLigne(de : DataGridEvent):void{	
			if (myGridLignes.dataProvider != null){										
				var myEditor:TextInput = TextInput(de.currentTarget.itemEditorInstance);                
                // Get the new value from the editor.
                var newVal:String = myEditor.text;	                
                // Get the old value.
                var oldVal:String = de.currentTarget.editedItemRenderer.data[de.dataField]; 
                                         					
				if (de.dataField == "commentaire")ElementCommande(myGridLignes.selectedItem).commentaire = newVal;					
				else if (de.dataField == "libelleLigne")ElementCommande(myGridLignes.selectedItem).libelleLigne = newVal;
				
				if (commande.commandeID > 0){
					ElementCommande(myGridLignes.selectedItem).update();	
				}														 					
			}else{		
				de.preventDefault();					
			}   
		}
 
		//-------RAPPROCHEMENT ------------------------------------



		//Ouvre la fentetre des rapprochements
		private function rapprocherLigne(me : MouseEvent):void{
			if(commande.commandeID > 0){				
				
				rapprochement = new RapprochementsProduitsIHM();
				rapprochement.setCommande(commande,panier);
				rapprochement.addEventListener(RapprochementsProduits.UPDATE_PRODUITS_RAPPROCHER,changerEtatLigne);
			    rapprochement.addEventListener(RapprochementsProduits.SORTIR,sortirDuRapprochement);								
				rapprochement.addEventListener(RapprochementsProduits.VALIDER,fermerCommande);
				
				ConsoviewUtil.scale(DisplayObject(rapprochement),cv.W_ratio,cv.H_ratio);
				
				PopUpManager.addPopUp(rapprochement,UIComponent(parentApplication),true);					
				PopUpManager.centerPopUp(rapprochement);		
					
			}			
		}
		
		//Ne fait rien pour le moment
		private function sortirDuRapprochement(ev : Event):void{						
			// to do ---- 
		}
	
		//--- EXPORT PDF --------------------
		
		//Affiche la commande au format PDF
		private function exporterPDF(me : MouseEvent):void{
			if (commande.commandeID != 0){
				exporterLePdf();
			}
		}
		
		//Affiche la commande au format PDF
		private function exporterLePdf():void{
			displayExport(Commande.FORMAT_PDF);
		}
		
		
		
		//Affiche la commande dans une nouvelle fenetre au format spécifié
		//param in format , le format sous lequel on souhaite afficher la commande
		private function displayExport(format : String):void {		
				 
			var commandeToExp : CommandeExportable = new CommandeExportable(commande,panier);				 
			var expB : ExportBuilder = new ExportBuilder(commandeToExp);
			expB.exporter(format);
			
			
				/* switch (format.toUpperCase()){
				case "PDF" : {
					var pdf : PDFCreator = new PDFCreator(commande,panier);
					pdf.displayExport();					
					break
				}
			}				  */	
			
        } 			
        //------ FIN EXPORT PDF -----------------		
	}
}