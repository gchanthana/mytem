package composants.util
{
	import composants.access.PerimetreTreeEvent;
	import composants.access.perimetre.PerimetreTreeDataDescriptor;
	import composants.controls.TextInputLabeled;
	import composants.parametres.perimetres.tree.PerimetreTree;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.perimetres.search.SpecialSearch;
	
	[Bindable]
	public class OrgaSearchTreeImpl extends Box
	{
		
		//conts
		public static const ORGANISATION_AFFICHEE : String = "organisationAffichee";
		
		//Elements graphique
		public var treePerimetre : PerimetreTree
		public var cboOrganisation : ComboBox;
		public var txtRecherche : TextInputLabeled;
		public var imgRechercher : Image;
		public var imgNext : Image;
		public var imgPrevious : Image;
		public var lblNbResult : Label;
		//Fin Element graphique
		
		
		
		private var _treeNodes : Object;
		private var _searchResult : Array;
		private var _searchItemIndex : Number;
		private var _searchExecQuery:Boolean;	
		private var _search : SpecialSearch;	//recherche dans les noeud pas encore en memoire
		
		/**		  
		 * Reference vers la collection contenant la liste des organisations
		 * */
		private var _listeOrganisations : ArrayCollection;
		public function get listeOrganisations ():ArrayCollection{
			if (_listeOrganisations == null) _listeOrganisations = new ArrayCollection(); 
			return _listeOrganisations;
		}
		public function set listeOrganisations( liste : ArrayCollection):void{
			_listeOrganisations = liste;			
		}
		
		
		private var _modeRestreint : Boolean;
		public function set modeRestreint(mode : Boolean):void{
			_modeRestreint = mode;
		}
		public function get modeRestreint():Boolean{
			return _modeRestreint;
		}
		
		
		private var _autoSelectNode : Boolean = false;
		private var _nodeToAutoSelect : Object;
		
		public function OrgaSearchTreeImpl()
		{
			super();
			_searchExecQuery = true;
		}
		
		
		//============ PUBLIC ==============================================================
		public function selctionnerLeNoeudCible(idgroupeClient : Number):void{
			if (initialized){
				_autoSelectNode = true;
				getNodeOrganisation(idgroupeClient);	
			}
		}
		
		//==================================================================================	
		
		//============ HANDLERS ============================================================
		
		//
		protected function getNodeOrganisationResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				_nodeToAutoSelect = new Object();
				_nodeToAutoSelect.IDGROUPE_CLIENT = Number(re.token.message.body[0]);
				_nodeToAutoSelect.IDORGA = Number(re.result);
				cboOrganisation.selectedIndex = ConsoviewUtil.getIndexById(listeOrganisations,"IDGROUPE_CLIENT",_nodeToAutoSelect.IDORGA);
				cboOrganisation.dispatchEvent(new ListEvent(ListEvent.CHANGE));	
			}else{
				_autoSelectNode = false;
			}
		}
		
		
		//
		protected function cboOrganisationChangeHandler(le : ListEvent):void{
			afficherOrganisationSelectionnee();
			clearSearch(true);
		}
		
		//
		protected function txtRechercheChangeHandler(ev : Event):void{
			_searchExecQuery = true;
		}
		
		//
		protected function imgRechercherClickHandler(me : MouseEvent):void
		{	if (txtRecherche.text != ""){
				treePerimetre.selectedIndex = 0;
				callLater(rechercher,[txtRecherche.text]);
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez saisir une clé de recherche.","Erreur");
			}
				
		}
		
		//
		protected function txtRechercheEnterHandler(fe : FlexEvent):void{
			if (txtRecherche.text != ""){
				treePerimetre.selectedIndex = 0;
				callLater(rechercher,[txtRecherche.text]);
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez saisir une clé de recherche.","Erreur");
			}
		}
		
		protected function imgNextClickHandler(me : MouseEvent):void{
			if (txtRecherche.text != ""){
				treePerimetre.selectedIndex = 0;
				callLater(rechercher,[txtRecherche.text]);
			}
		}
		
		
		//
		protected function treePerimetreChangeHandler(le : ListEvent):void{
			clearSearch();
			_searchExecQuery = true;
		}
		
		
		//
		protected function treePerimetreDataProviderSetHandler(pte : PerimetreTreeEvent):void{
			if (_autoSelectNode && _nodeToAutoSelect != null){
					treePerimetre.selectedIndex = 0;
					setDataResult([_nodeToAutoSelect]);
					_autoSelectNode = false;
			}
			dispatchEvent(new Event(ORGANISATION_AFFICHEE));
		}
		
		//
		protected function searchNodeResultHandler(re : ResultEvent):void
		{	
			
			if ((re.result != null) && ((re.result as ArrayCollection).length > 0)){
				setDataResult((re.result as ArrayCollection).source);			
			}else{
				var message : String = String(re.token.message.body[1]);
				Alert.okLabel = "Fermer";
				Alert.show(message,"Pas de résultat pour :");
			}	
		}
		//============ PROTECTED ===========================================================
		protected function searchNode(nodeName:String):void
		{
			 var selectedNodeID : Number= treePerimetre.selectedItem.@NID;
			 var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"searchNode",
																				searchNodeResultHandler);																				
			RemoteObjectUtil.callService(op, selectedNodeID, nodeName, 0, null);
		}
		
		protected function getNodeOrganisation(idgroupeClient : Number):void{
			 var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getNodeOrganisation",
																				getNodeOrganisationResultHandler);																				
			RemoteObjectUtil.callService(op, idgroupeClient);
		}
		
		
		override protected function commitProperties():void{
			cboOrganisation.addEventListener(ListEvent.CHANGE,cboOrganisationChangeHandler);
			imgRechercher.addEventListener(MouseEvent.CLICK,imgRechercherClickHandler);
			txtRecherche.addEventListener(FlexEvent.ENTER,txtRechercheEnterHandler);
			txtRecherche.addEventListener(Event.CHANGE,txtRechercheChangeHandler);
			treePerimetre.addEventListener(ListEvent.CHANGE,treePerimetreChangeHandler);
			treePerimetre.addEventListener(PerimetreTreeEvent.DATA_PROVIDER_SET,treePerimetreDataProviderSetHandler);
		} 
		
		
		
		//============ PRIVATE =============================================================
		//Affiche l'organisation selectionnée
		private function afficherOrganisationSelectionnee():void{
			var rootId:Number;			
			//Si on est en mode restreint on n'affiche que les noeuds sous le perimetre courant
			if (modeRestreint){
				rootId = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			}else{
				rootId = (cboOrganisation.selectedItem != null)?cboOrganisation.selectedItem.IDGROUPE_CLIENT: -1;
			}
			
			if(rootId != -1){
				var dataset:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
				var resultList:XMLList = dataset.descendants().(@NID == rootId);			
				_treeNodes = resultList;
				if(resultList.length() == 1){
					treePerimetre.setDataProvider(resultList[0],"LBL",new PerimetreTreeDataDescriptor());
				}	
			}	
		} 
		
		
		//selectionne un élément de l'arbre suivant un attribut
		private function selectNodeByAttribute(attribute:String, value:String):void
		{	
			try{
				var dept:XMLList;
				dept = _treeNodes.descendants().(@[attribute] == value);
				
				//Si le noeud recherche n'est pas encore chargé alors on fait un recherche spéciale
				if(dept[0] == null && _searchResult){					 								
																
					_search = new SpecialSearch(this);
					_search.initSearch(treePerimetre,Number(value));
					return;
				}else{
					_search = null;
				}
				
				//recherche dans les noeuds déjà en mémoire
				var pr:Object = dept[0];
				while ((pr = pr.parent()) != null) {
					treePerimetre.expandItem(pr,true);
				}
				treePerimetre.selectedItem = dept[0];
				treePerimetre.scrollToIndex(treePerimetre.selectedIndex);
				
			}catch(e : Error){
				_search = null;
				trace(e.getStackTrace());			
			}
		}
		
		
		//Lance la recherche
		private function rechercher(chaine : String):void{
			if (_searchExecQuery)
			{	
				searchNode(chaine);
			}else if (_searchResult){
				selectNextNode();
			}
		} 
				
		
		//Affecte le tableau avec les resultat de la recherche
		private function setDataResult(data:Array):void		
		{
			clearSearch();
			 _searchResult = data;
			 _searchItemIndex = 0;
			 if (_searchResult.length >1){
			 	lblNbResult.visible = true;
			 	imgNext.visible = true;
			 	imgNext.addEventListener(MouseEvent.CLICK,imgNextClickHandler);
			 }
			 selectNextNode();
		}
		
		// Select the next found node //
		private function selectNextNode():void
		{
			if (_searchItemIndex >= _searchResult.length)return;			
			selectNodeByAttribute("NID", _searchResult[_searchItemIndex].IDGROUPE_CLIENT);			
			
			_searchItemIndex++;
				
			if (_searchItemIndex >= _searchResult.length){
				_searchItemIndex = 0;
			} 
			
			if (lblNbResult.visible){
				lblNbResult.text = Number(_searchItemIndex+1) +"/"+ _searchResult.length.toString();	
			}
			
		}
		
		private function clearSearch(boolEffacerCle : Boolean = false):void
		{
			_searchExecQuery = false;
			_search = null;
			_searchItemIndex = 0;
			if (boolEffacerCle)	txtRecherche.text = "";
			
			lblNbResult.text = ""; 
			lblNbResult.visible = false;			
			imgNext.visible = false;
			imgNext.removeEventListener(MouseEvent.CLICK,imgNextClickHandler);
		}
	}
}