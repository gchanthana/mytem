<cfcomponent  output="false"  
			displayname="GestionDroitService" 
			hint="Classe permettant la gestion des logs des actes de gestion ">
	
	<!---  --->
	<cffunction name="gestionDroits" access="public" returntype="query" output="false" hint="Liste l'ensemble des modules auxquels a droit un client">
		<cfargument name="appLogin"			required="true" type="numeric"/>
		<cfargument name="idracine"			required="true" type="numeric"/>
		<cfargument name="codeapp"			required="false" type="numeric" default="1"/>
	
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M21.ListeModule_v4">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#appLogin#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeapp#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#_idlang#">
			<cfprocresult name="qResult">
		</cfstoredproc>
		
		
		<!--- BOOLEEN MODULE --->
		<cfset pilotageFin1 = false>
		<cfset pilotageFin2 = false>
		<cfset pilotageFin3 = false>
		<cfset pilotageFin4 = false>
		<cfset mdm1 = false>
		<cfset mdm2 = false>
		<cfset SESSION.GSTDROITS.pilotageFinancier = false >
		<cfset SESSION.GSTDROITS.droitsMDM = false >
		
		
		<cfloop query="qResult">
			<cfset currentRow = #qResult.currentrow#>
			<!--- module mdm --->
			<cfif qResult['ACCESS_RACINE'][#currentRow#] eq 2 && qResult['CODE_MODULE'][#currentRow#] eq "M111">
				<cfset mdm1 = true>
			</cfif>
		</cfloop>
		
		<cfloop query="qResult">	
			<!--- module pilotage financier --->
			<cfif qResult['ACCESS_RACINE'][#currentRow#] eq 2 && qResult['CODE_MODULE'][#currentRow#] eq "M331">
				<cfset pilotageFin1 = true>
			</cfif>
		</cfloop>
		
		<cfloop query="qResult">
			<cfif qResult['ACCESS_RACINE'][#currentRow#] eq 2 && qResult['CODE_MODULE'][#currentRow#] eq "M311">
				<cfset pilotageFin2 = true>
			</cfif>
		</cfloop>
		<cfloop query="qResult">
			<cfif qResult['ACCESS_RACINE'][#currentRow#] eq 2 && qResult['CODE_MODULE'][#currentRow#] eq "M511">
				<cfset pilotageFin3 = true>
			</cfif>
		</cfloop>
		<cfloop query="qResult">
			<cfif qResult['ACCESS_RACINE'][#currentRow#] eq 2 && qResult['CODE_MODULE'][#currentRow#] eq "M61">
				<cfset pilotageFin4 = true>
			</cfif>
		</cfloop>
		
		<!--- module mdm --->
		<cfif SESSION.BOOL_MDM_RAC eq "1"> <!--- racine --->
			<cfset mdm2 = true>
		</cfif>
		
		
		<cfif (pilotageFin1 eq true && pilotageFin2 eq true && pilotageFin3 eq true && pilotageFin4 eq true )&& (mdm1 eq false || mdm2 eq false)>
			<cfset SESSION.GSTDROITS.pilotageFinancier = true >
		</cfif>
		<cfif (pilotageFin1 eq false || pilotageFin2 eq false || pilotageFin3 eq false || pilotageFin4 eq false ) && (mdm1 eq true && mdm2 eq true)>
			<cfset SESSION.GSTDROITS.droitsMDM = true >
		</cfif>
		<cfif (pilotageFin1 eq true && pilotageFin2 eq true && pilotageFin3 eq true && pilotageFin4 eq true) && (mdm1 eq true && mdm2 eq true)>
			<cfset SESSION.GSTDROITS.pilotageFinancier = true >
			<cfset SESSION.GSTDROITS.droitsMDM = true >
		</cfif>
		
		<cfreturn qResult>
	</cffunction>
	
	
	<!--- AFFICHE LES DIVS DE LA PAGE 2 SELON LES DROITS DE LUTILISATEUR --->
	<cffunction name="gestionDroitPage2" access="public" returntype="any" output="true" hint="Gère affichage des divs de la page2 selon les droits des clients">
		<cfargument name="head" required="true" type="string">
		<cfargument name="structParam" required="true" type="struct">
		
		
		<!--- -------------------------------- --->
		<!--- SI ONLY DROIT PILOTAGE FINANCIER --->
		<!--- -------------------------------- --->
		<cfif SESSION.GSTDROITS.pilotageFinancier eq true && SESSION.GSTDROITS.droitsMDM eq false >
			
			<cfset objHeader = createObject("component","portail.D1.defaut.composant.Header") >
			<cfif NOT isDefined("structParam.idSous_tete") >
				<cfset header_Pilotage = objHeader.getHeader() >
			<cfelse>
				<cfset header_Pilotage = objHeader.getHeader(structParam.idPage,structParam.idSous_tete,structParam.sous_tete) >
			</cfif>
			
			
			<cfset divMesInfos_Pilotage = "">
			<cfset objAccueilDivInfos = createObject("component","portail.D1.defaut.composant.AccueilDivInfos") >
			<cfset divMesInfos_Pilotage = objAccueilDivInfos.getAccueilDivInfos() >
			
			
			<cfset maLigneCombo_Pilotage = "">
			<cfset objAccueilDivSelectLigne = createObject("component","portail.D1.defaut.composant.AccueilDivSelectLigne") >
			<cfif NOT isDefined("structParam.sous_tete")>
				<cfset maLigneCombo_Pilotage = objAccueilDivSelectLigne.getAccueilDivSelectLigne() >
			<cfelse>
				<cfset maLigneCombo_Pilotage = objAccueilDivSelectLigne.getAccueilDivSelectLigne(structParam.sous_tete) >
			</cfif>
			
			
			<cfset accueilLastFacture_Pilotage = "">
			<cfset objAccueilDivLastFacture = createObject("component","portail.D1.defaut.composant.AccueilDivLastFacture") >
			<cfif NOT isDefined("structParam.idSous_tete")>
				<cfset accueilLastFacture_Pilotage = objAccueilDivLastFacture.getAccueilDivLastFacture()>
			<cfelse>
				<cfset accueilLastFacture_Pilotage = objAccueilDivLastFacture.getAccueilDivLastFacture(structParam.idSous_tete)>
			</cfif>
			
			
			<cfset objDivEmail = createObject("component","portail.D1.defaut.composant.DivEmail") >
			<cfif NOT isDefined("structParam.idSous_tete") > <!--- boolIsManaged --->
				<cfset divEmail = objDivEmail.getDivEmail(0,2) >
			<cfelse>
				<cfset divEmail = objDivEmail.getDivEmail(structParam.idSous_tete,2) >
			</cfif>
						
			
			<cfset divDivContactGest_Pilotage = "">
			<cfset objAccueilDivContactGest = createObject("component","portail.D1.defaut.composant.AccueilDivContactGestionnaire") >
			<cfif NOT isDefined("structParam.idSous_tete") >
				<cfset divDivContactGest_Pilotage = objAccueilDivContactGest.getAccueilDivContactGestionnaire() >
			<cfelse>
				<cfset divDivContactGest_Pilotage = objAccueilDivContactGest.getAccueilDivContactGestionnaire(structParam.idSous_tete) >
			</cfif>
			
			<!--- le thread est commenté car il provoque un probleme de timing(des données ne s'affichent pas) dans le process --->
			<!--- <cfthread action="join" name="t1" ></cfthread> --->
			
		
		
		<!--- ----------------- --->
		<!--- SI ONLY DROIT MDM --->
		<!--- ----------------- --->
		<cfelseif SESSION.GSTDROITS.droitsMDM eq true && SESSION.GSTDROITS.pilotageFinancier eq false >
			
			<cfset objHeader = createObject("component","portail.D1.defaut.composant.Header") >
			<cfif NOT isDefined("structParam.idSous_tete") >
				<cfset header_Mdm = objHeader.getHeader() >
			<cfelse>
				<cfset header_Mdm = objHeader.getHeader(structParam.idPage,structParam.idSous_tete,structParam.sous_tete) >
			</cfif>
			
			
			<cfset maLigneCombo_Mdm = "">
			<cfset objAccueilDivSelectLigne = createObject("component","portail.D1.defaut.composant.AccueilDivSelectLigne") >
			<cfif NOT isDefined("structParam.sous_tete")>
				<cfset maLigneCombo_Mdm = objAccueilDivSelectLigne.getAccueilDivSelectLigne() >
			<cfelse>
				<cfset maLigneCombo_Mdm = objAccueilDivSelectLigne.getAccueilDivSelectLigne(structParam.sous_tete) >
			</cfif>
			
			
			<cfset divMesInfosAndTerminalMDM_Mdm = "">
			<cfset objAccueilDivInfosEtTerminalMDM = createObject("component","portail.D1.defaut.composant.AccueilDivInfosEtTerminalMDM") >
			<cfif NOT isDefined("structParam.idSous_tete")>
				<cfset divMesInfosAndTerminalMDM_Mdm = objAccueilDivInfosEtTerminalMDM.getAccueilDivInfosEtTerminalMDM() >
			<cfelse>
				<cfset divMesInfosAndTerminalMDM_Mdm = objAccueilDivInfosEtTerminalMDM.getAccueilDivInfosEtTerminalMDM(structParam.idSous_tete) >
			</cfif>
			
			
			<cfset divDivHistorique_Mdm = "">
			<cfset objAccueilDivHistorique = createObject("component","portail.D1.defaut.composant.AccueilDivHistorique") >
			<cfif NOT isDefined("structParam.idSous_tete") >
				<cfset divDivHistorique_Mdm = objAccueilDivHistorique.getAccueilDivHistorique() >
			<cfelse>
				<cfset divDivHistorique_Mdm = objAccueilDivHistorique.getAccueilDivHistorique(structParam.idSous_tete) >
			</cfif>
		
		
		<!--- ----------------------------------- --->
		<!--- SI DROITS MDM ET PILOTAGE FINANCIER --->
		<!--- ----------------------------------- --->
		<cfelseif SESSION.GSTDROITS.droitsMDM eq true && SESSION.GSTDROITS.pilotageFinancier eq true >
			
			<cfset objHeader = createObject("component","portail.D1.defaut.composant.Header") >
			<cfif NOT isDefined("structParam.idSous_tete") >
				<cfset header_Tout = objHeader.getHeader() >
			<cfelse>
				<cfset header_Tout = objHeader.getHeader(structParam.idPage,structParam.idSous_tete,structParam.sous_tete) >
			</cfif>
			
			
			<cfset maLigneCombo_Tout = "">
			<cfset objAccueilDivSelectLigne = createObject("component","portail.D1.defaut.composant.AccueilDivSelectLigne") >
			<cfif NOT isDefined("structParam.sous_tete")>
				<cfset maLigneCombo_Tout = objAccueilDivSelectLigne.getAccueilDivSelectLigne() >
			<cfelse>
				<cfset maLigneCombo_Tout = objAccueilDivSelectLigne.getAccueilDivSelectLigne(structParam.sous_tete) >
			</cfif>
			
			
			<cfset accueilLastFacture_Tout = "">
			<cfset objAccueilDivLastFacture = createObject("component","portail.D1.defaut.composant.AccueilDivLastFacture") >
			<cfif NOT isDefined("structParam.idSous_tete")>
				<cfset accueilLastFacture_Tout = objAccueilDivLastFacture.getAccueilDivLastFacture()>
			<cfelse>
				<cfset accueilLastFacture_Tout = objAccueilDivLastFacture.getAccueilDivLastFacture(structParam.idSous_tete)>
			</cfif>
			
			
			<cfset divMesInfosAndTerminal_Tout = "">
			<cfset objAccueilDivInfosEtTerminal = createObject("component","portail.D1.defaut.composant.AccueilDivInfosEtTerminal") >
			<cfif NOT isDefined("structParam.idSous_tete")>
				<cfset divMesInfosAndTerminal_Tout = objAccueilDivInfosEtTerminal.getAccueilDivInfosEtTerminal() >
			<cfelse>
				<cfset divMesInfosAndTerminal_Tout = objAccueilDivInfosEtTerminal.getAccueilDivInfosEtTerminal(structParam.idSous_tete) >
			</cfif>
			
			
			<cfset objDivEmail = createObject("component","portail.D1.defaut.composant.DivEmail") >
			<cfif NOT isDefined("structParam.idSous_tete") > <!--- boolIsManaged --->
				<cfset divEmail = objDivEmail.getDivEmail(0,2) >
			<cfelse>
				<cfset divEmail = objDivEmail.getDivEmail(structParam.idSous_tete,2) >
			</cfif>
						
			
			<cfset divDivLastAction_Tout = "">
			<cfset objAccueilDivLastActions = createObject("component","portail.D1.defaut.composant.AccueilDivLastActions") >
			<cfif NOT isDefined("structParam.idSous_tete") >
				<cfset divDivLastAction_Tout = objAccueilDivLastActions.getAccueilDivLastActions() >
			<cfelse>
				<cfset divDivLastAction_Tout = objAccueilDivLastActions.getAccueilDivLastActions(structParam.idSous_tete) >
			</cfif>
			
			<!--- le thread est commenté car il provoque un probleme de timing(des données ne s'affichent pas) dans le process --->
			<!--- <cfthread action="join" name="t1" ></cfthread> --->
			
		</cfif>
		
		
		<cfoutput>
			<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
				#head#
				<body onunload="disableLoadsablier();">
					<div class="ombre" id="ombre"><!--- div qui doit fr toute la page --->
					    <div class="background_ecran">
					        <img class="loading_image" id="imgLoading1" src="img/loading-icon.gif" />
					    </div>
					</div>
					<div class="ombre2" id="ombre2">
					    <div class="background_ecran2">
					        <img class="loading_image" id="imgLoading2" src="img/mytem/loading_bleu_mail.gif" />
					    </div>
					</div>
					
					<!--- ---------------------------------------- --->
					<!--- SI ONLY DROITS MDM ET PILOTAGE FINANCIER --->
					<!--- ---------------------------------------- --->
					<cfif SESSION.GSTDROITS.droitsMDM eq true && SESSION.GSTDROITS.pilotageFinancier eq true >
						
						<div class="divModalTransparenteEmail2" id="ombreee"></div>
						#divEmail#
						
						<div id="global" style="">
							#header_Tout#
							<div style="text-align:left;padding:0 5px;">
								<div>
									#maLigneCombo_Tout#
									<div style="text-align:center;">
										
										#accueilLastFacture_Tout#
										<div style="margin-left:10px;float:left;">
											#divMesInfosAndTerminal_Tout#
										</div>
										<div style="clear:both;">
									</div>
								</div>
								#divDivLastAction_Tout#
							</div>
						</div>
					
					<!--- -------------------------------- --->
					<!--- SI ONLY DROIT PILOTAGE FINANCIER --->
					<!--- -------------------------------- --->
					<cfelseif SESSION.GSTDROITS.pilotageFinancier eq true && SESSION.GSTDROITS.droitsMDM eq false >
					
						<div class="divModalTransparenteEmail2" id="ombreee"></div>
						#divEmail#
						
						<div id="global" >
							#header_Pilotage#
							<div style="text-align:left;padding:0 5px;">
								<div>
									#maLigneCombo_Pilotage#
									<div style="text-align:center;">
										#accueilLastFacture_Pilotage#
										<div style="text-align:left;margin-left:5px;float:left;">
											#divMesInfos_Pilotage#
											#divDivContactGest_Pilotage#
										</div>
										<div style="clear:both;">
									</div>
								</div>
							</div>
						</div>
					
					<!--- ----------------- --->
					<!--- SI ONLY DROIT MDM --->
					<!--- ----------------- --->
					<cfelseif SESSION.GSTDROITS.droitsMDM eq true && SESSION.GSTDROITS.pilotageFinancier eq false >
						
						<div id="global" >
							#header_Mdm#
							<div style="text-align:left;padding:0 5px;">
								<div style="">
									#maLigneCombo_Mdm#
									<div style="">
										<div style="margin-left:5px;float:left;">
											#divMesInfosAndTerminalMDM_Mdm#
										</div>
										<div style="clear:both;height:1px">
									</div>
									#divDivHistorique_Mdm#
								</div>
								
							</div>
						</div>	
						
					</cfif>
						
				</body>
			</html>
		</cfoutput>
		
	</cffunction>
	
</cfcomponent>