package appli.ConsoleGestion
{

	import appli.impersonnification.refreshManager;
	
	import flash.events.Event;
	
	import general.abstract.AbstractCreateApplication;
	import general.interfaceClass.ICreateApplication;
	import general.service.thread.ThreadServices;
	
	import mx.events.FlexEvent;

	
	public class CreateConsoleGestionImpl extends AbstractCreateApplication implements ICreateApplication
	{
		
		
		public function CreateConsoleGestionImpl()
		{
			
			
			super();
		}
		override public function updatePerimetre():void
		{
			menu.updatePerimetre();
			initDisplay();
		}
		override public function displayModule(key:String=null):void
		{

			var menuXML:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var selectedMenu:XMLList = menuXML.descendants("*").(@SYS == 1).(@toggled == 'true'); 
			moduleLoader.loadCgModule(selectedMenu.@KEY);

			if (refreshManager.isReloading == true)
			{//Exception dans le cas où il s'agit d'une connexion sur un autre compte, le point de départ doit devenir le HOME
				refreshManager.isReloading = false;
				CvAccessManager.changeModule("HOME");
			}
			else
			{
				(moduleLoader.dispobj as IModule).setInfosMessage(key);
				super.displayModule();
			}

		}
		
		override protected function init(event:FlexEvent=null):void
		{
			super.init();
		}
		override protected function initDisplay():void
		{
			displayModule();
		}
		override protected function initData():void
		{
			var serv:ThreadServices = new ThreadServices();
			serv.getThreadInfos();
		}
		public function refreshModule(e:Event):void
		{
			trace("refresh depuis CreateConsoleGestion");
			displayModule();
		}
	}
}