package entity
{
	public class ApplicationVO
	{
		private var _CODEAPP:int;
		private var _LABEL:String;
		private var _SELECTED:Boolean;
		
		
		public function fill(obj:Object):void
		{
			if(obj.hasOwnProperty("CODEAPP"))
				this._CODEAPP = obj.CODEAPP;
			if(obj.hasOwnProperty("LABEL"))
				this._LABEL = obj.LABEL;
		}
		
		// GETTERS - SETTERS
		public function get LABEL():String 
		{ 
			return _LABEL;
		}
		public function set LABEL(value:String):void
		{
			if (_LABEL == value)
				return;
			_LABEL = value;
		}
		public function get CODEAPP():int 
		{ 
			return _CODEAPP;
		}
		public function set CODEAPP(value:int):void
		{
			if (_CODEAPP == value)
				return;
			_CODEAPP = value;
		}

		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}

		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}

		
	}
}