package  commandemobile.system
{
	/**
	 * Class ValueObject Opérateur
	 * */
	public dynamic class OperateurVO
	{
		/**
		 * Le nom de l'opérateur
		 * **/		
		public var nom : String = "";
		
		/**
		 * l'identifiant de l'opérateur
		 * */
		public var id : int = 0;
	}
}