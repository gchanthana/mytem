package gestionparc.ihm.fiches.listedispo.listeparcollab
{
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.liste.ListeService;
	
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;

	public class ListeSousEqpOfEqp extends ListeSousEqpOfEqpIhm implements IListeDispo
	{
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")]
		private var adrImg:Class;
		public var serv:ListeService=new ListeService();

		public function ListeSousEqpOfEqp()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			txtFiltreFixe.setFocus();
			initData();
		}

		public function getSelection():Object
		{
			if (dgListeSousEqp.selectedIndex != -1)
			{
				return dgListeSousEqp.selectedItem;
			}
			else
			{
				return null;
			}
		}

		public function getIDSelection():int
		{
			if(dgListeSousEqp.selectedItem != null)
			{
				return dgListeSousEqp.selectedItem.IDTERMINAL;
			}
			else
			{
				return -1;
			}
			
		}

		public function getDataGrid():DataGrid
		{
			return dgListeSousEqp;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_sseqp');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		public function getLibelleSelection():String
		{
			if (dgListeSousEqp.selectedIndex != -1)
			{
				return dgListeSousEqp.selectedItem.IMEI;
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		private function initData():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_SOUSEQPOFEQP, updateDG);
			serv.ListeSsEqpOfEqp();
		}

		private function updateDG(re:gestionparcEvent):void
		{
			dgListeSousEqp.dataProvider=serv.myDatas.dataListeSousEqpOfEqp;
		}
	}
}
