package univers.facturation.suivi.factures.supprimerfactures
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.facturation.suivi.controles.vo.Facture;
	import univers.facturation.suivi.controles.vo.ParamsRecherche;
	 
	
	[Bindable]
	public class SuppFactureApp
	{
		
		
		
		
		//====================== PROPRIETES  ===============================================				
		
		//La liste des factures que l'on peut supprimmer
		private var _listeFactures : ArrayCollection = new ArrayCollection();		
		public function get listeFactures ():ArrayCollection{
			return _listeFactures;
		}
		
		public function set listeFactures (liste : ArrayCollection):void{
			_listeFactures = liste;
		}
		
		
		
		
		
		
		
		
		
		
		//Les parametres de la recherche
		private var _paramsRecherche : ParamsRecherche = new ParamsRecherche();
		public function get paramsRecherche ():ParamsRecherche{
			return _paramsRecherche;
		}
		
		public function set paramsRecherche (params : ParamsRecherche):void{
			_paramsRecherche = params;
		}
		
		
		//====================== PROPRIETES  FIN =============================================		
		
		
		
		
		
		
		
		
		
		
		
		//====================== LISTE DES FACTURES SUPPRIMABLE FIN ===============================================		
				
		/**
		 * Fournit la liste des factures que l'on peut supprimer.
		 * C'est-a-dire. Les factures qui ont été rajoutées manuellement
		 * @param  ParamsRecherche (numéro, operateur, date emission)
		 **/		
		public function getListeFactureSupprimables():void{
			
			var op : AbstractOperation = 
					RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
															 "fr.consotel.consoview.facturation.suiviFacturation.supprimerFacture.FactureGateWay",
															 "getListeFactureSupprimables",
															 getListeFactureSupprimablesResultHandler);
			RemoteObjectUtil.callService(op,paramsRecherche);
															
		}
		
		//Traite le résultat de l'appel à getListeFactureSupprimables
		private function getListeFactureSupprimablesResultHandler(re : ResultEvent):void{
			if (re.result != null){				
				if((re.result as Array).length == 0){
					_listeFactures.source = new Array();
				}else if( ((re.result as Array)[0] as Facture).periodeId == -1 ){
					_listeFactures.source = new Array();
				}else{
					_listeFactures.source = re.result as Array;					
				}
			}else{
				_listeFactures.source = new Array();
			}	
		}
		
		//====================== LISTE DES FACTURES SUPPRIMABLE FIN ===============================================		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//====================== SUPPRIMER UNE FACTURE ===============================================
		
		/**
		 * Supprilme la facture passée en paramètre
		 * @param Facture
		 **/
		 public function supprimerLaFacture(factures : Array):void{	
		 			 	
			var op : AbstractOperation = 
					RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
															 "fr.consotel.consoview.facturation.suiviFacturation.supprimerFacture.FactureGateWay",
															 "supprimerListeFactures",
															 supprimerLaFactureResultHandler);
			
			
			RemoteObjectUtil.callService(op,factures);												
		}
		
		
		
		//Traite le résultat de l'appel à supprimerLaFacture
		private function supprimerLaFactureResultHandler(re : ResultEvent):void{
			var factures : Array = re.token.message.body[0] as Array;			
			var message : String = "\n";			
			
			if (re.result != null){				
				var tmpTab : Array = (re.result as Array);
				for (var i: Number = 0; i < tmpTab.length; i++){
					switch((tmpTab[i] as Facture).libelle){
						case "-2":{
							message = message + "- " + (tmpTab[i] as Facture).numero+ResourceManager.getInstance().getString('M32', '__Etat_de_la_facture')+"\n";						
							break;
						}
						
						case "-1":{
							message = message + "- " + (tmpTab[i] as Facture).numero+ResourceManager.getInstance().getString('M32', '__Erreur')+"\n";
							break;
						}
						
						default :{
							_listeFactures.removeItemAt(ConsoviewUtil.getIndexById(_listeFactures,"periodeId",(tmpTab[i] as Facture).periodeId));
							break;
						}
					}
					
				}					 
				Alert.okLabel = ResourceManager.getInstance().getString('M32', 'OK');
				if (message != "\n")Alert.show(message,ResourceManager.getInstance().getString('M32', 'Les_factures_suivantes_n_ont_pas_pu__tre'),Alert.OK);
			}
			
		}
		
		//====================== SUPPRIMER UNE FACTURE FIN ===============================================
		
		
		
		
		
		
		//======================= FILTRE LA LISTE DE FACTURES ==========================================
		/**
		 * filtre la liste des factures
		 * @param chaine la chaine servant de filtre
		 * */
		 public function filtrerLaListe(chaine : String):void{
		 	
		 	var functionFiltre : Function = function traitement(facture : Facture):Boolean{
		 		if ((facture.operateurLibelle.toLowerCase().search(chaine.toLowerCase()) != -1)
					||				
					(facture.numero.toLowerCase().search(chaine.toLowerCase()) != -1)
					||
					(facture.compteFacturation.toLowerCase().search(chaine.toLowerCase()) != -1)
					||
					(facture.dateEmission.toDateString().toLowerCase().search(chaine.toLowerCase()) != -1)
					||
					(facture.dateDebut.toDateString().search(chaine.toLowerCase()) != -1)
					||
					(facture.dateFin.toDateString().toLowerCase().search(chaine.toLowerCase()) != -1)
					||
					(facture.montant.toString().toLowerCase().search(chaine.toLowerCase()) != -1)
					||
					(facture.creeePar.toLowerCase().search(chaine.toLowerCase()) != -1))					
					return true;
				else
					return false;
		 	}		 	
		 	
		 	if (listeFactures != null){
		 		listeFactures.filterFunction = functionFiltre;
		 		listeFactures.refresh();
		 	}
		 	
		 }
		 
		//======================= FILTRE LA LISTE DE FACTURES ==========================================	
	}
}