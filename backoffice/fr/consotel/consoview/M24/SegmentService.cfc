<cfcomponent displayname="fr.consotel.consoview.M24.SegmentService" output="false">
	<cffunction name="getAllSegment" access="remote" output="false" returntype="Query">
		<cfargument name="CODE_LANGUE" type="string" required="false">
		
		<cfset _CODE_LANGUE = SESSION.USER.GLOBALIZATION>		
		<cfif isDefined("CODE_LANGUE")>
			<cfset _CODE_LANGUE = CODE_LANGUE>
		</cfif>
		
		<cfset qAllSegment = ''>				
		<cfquery datasource="#SESSION.OFFREDSN#" name="qAllSegment">
			SELECT 0 as IDSEGMENT, Decode('#left(_CODE_LANGUE,2)#','fr','Tous','en','All','es','Todos','Tous') as LIBELLE FROM DUAL
			UNION
			SELECT 2 as IDSEGMENT, Decode('#left(_CODE_LANGUE,2)#','fr','Fixe','en','Landline','es','Fije','Fixe') as LIBELLE FROM DUAL
			UNION
			SELECT 1 as IDSEGMENT, Decode('#left(_CODE_LANGUE,2)#','fr','Mobile','en','Mobile','es','Móvil','Mobile') as LIBELLE FROM DUAL
			UNION
			SELECT 3 as IDSEGMENT, Decode('#left(_CODE_LANGUE,2)#','fr','Réseaux','en','NetWorks','es','Redes','Réseaux') as LIBELLE FROM DUAL
		</cfquery>
		
		<cfreturn qAllSegment/>
	</cffunction>
	
</cfcomponent>