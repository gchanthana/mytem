package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import composants.util.DateFunction;
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import univers.inventaire.inventaire.menu.JournauxMenu;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import univers.inventaire.inventaire.menu.OperationMenuEvent;
	
	/**
	 * Journal des opérations avec retard  
	 * */
	public class OperationAvecRetard extends OperationAvecRetardIHM implements IJournal
	{
		//reference vers la date pour laquelle on souhaite afficher l'inventaire
		private var laDate : String;	
				
		//ArrayCollection contenant les lignes du journal
		[Bindable]
		private var elementsDuJournal : ArrayCollection;
		
		//Reference vers la méthode distante qui ramène les lignes du journal
		private var opElementDuJournal : AbstractOperation;
		
		//les information du noeud (inutilisé ici)
		private var _nodeInfos : Object;
		
		
		/**
		 * Constructeur
		 * */		
		public function OperationAvecRetard()
		{	
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
			
		/**
		 * Recharge les données du journal
		 * */
		public function refresh():void{
			//tente de couper le remoting			 
			if (laDate != null)
				chargerDonnees();
		 
		}
		
		/**
		* Fonction qui permet d'afficher la période sur laquelle on travaille, au format jour mois annee 
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		*
		* (ne fait rien) 
		* */
		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
															
			var formMois: String = moisDebut.substr(0,2) 
									+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
								   	+ moisDebut.substr(6,4);
			
			//labelPeriode.text = "Date :" + formMois;					
		}
		
		/**
		 * Affecte les infos sur le noeud au journal
		 * */
		 public function set nodeInfos(infos : Object):void{
		 	_nodeInfos = infos;
		 }
		 
		
		/**
		 * Gere le changement de périmetre
		 * */
		public function onPerimetreChange():void{
		 
			//init filtre
			txtFiltre.text = "";
			
			//init periode
			initPeriode();
			setSelectedPeriode(laDate,null);	
			
			//init le data provider
			elementsDuJournal.source = null
			elementsDuJournal.refresh();
			
			//données
			chargerDonnees();
			
		}
		
		/**
		 * Interrompt les remotings en cours
		 * (ne fait rien pour cette classe)
		 * */
		public function cancelRemotings():void{
			try{
				 
			}catch(e : Error){
				trace("ok");
			}
		}
//--- INITIALISATION ---------------------------------------------------------------------------------------------------------------------------
		//init IHM
		private function initIHM(fe : FlexEvent):void{
			//Periode
			initPeriode();				
			
			setSelectedPeriode(laDate,null);							
						
			//Filtre
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
			
			//grid
			dateAction.labelFunction = formateDates;
			dateAction.dataTipFunction = formateDatesTip;			
			myGrid.addEventListener(Event.CHANGE,goToselectedOperation);			
			chargerDonnees();
		}
		
		
		//formate date grid tooltip
		private function formateDatesTip(item : Object):String{
			var ladate:Date = new Date(item.DATE_ACTION);
			return DateFunction.formatDateAsString(ladate);	
		}
		
		//formate date gris
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			return DateFunction.formatDateAsString(ladate);						
		}
				
		//init Periode
		private function initPeriode():void{			
			laDate = DateFunction.formatDateAsString(new Date());
		}		
//--- FIN INITIALISATION -----------------------------------------------------------------------------------------------------------------------

	

//--- PERIODE SELECTOR -------------------------------------------------------------------------------------------------------------------------
		private function updatePeriode(pe : PeriodeEvent):void{
						
		}
//--- FIN PERIODE SELECTOR ---------------------------------------------------------------------------------------------------------------------
		
				
//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------
		//filtre
		private function filtrerGrid(ev :Event):void{
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();		
		}
						
		///filtre sur les elements du DataGrid
		private function filterFunc(value : Object):Boolean{				
			if ((String(value.TYPE_OPERATION).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.LIBELLE_ETAT).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.DUREE_ETAT).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.RETARD).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.SOUS_TETE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
			{
				return (true);
			} else {
				return (false);
			}	
		}
				
		
//--- FIN FILTRE -------------------------------------------------------------------------------------------------------------------------------

//--- GRID -------------------------------------------------------------------------------------------------------------------------------------
		
		//Dispatche un evenement SelectionOperationEvent de type SelectionOperationEvent.OPERATION_SELECTED
		//Permet de passer l'identifiant de la commande
		private function goToselectedOperation(ev : Event):void{
			
			if (myGrid.selectedIndex != -1){
				var evtObj : SelectionOperationEvent = new SelectionOperationEvent(SelectionOperationEvent.OPERATION_SELECTED);			
				evtObj.idOperation = myGrid.selectedItem.IDINV_OPERATIONS;			
				callLater(dispatchEvent,[evtObj]);	
			}
			
						
		}
//--- FIN GRID ---------------------------------------------------------------------------------------------------------------------------------

//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		
		//Charge les donnees du Journal
		private function chargerDonnees():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
		 
			lblError.visible = false;		
			opElementDuJournal = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getListeOperationEnRetard",
																				chargerDonneesResultHandler);	
			
			
			RemoteObjectUtil.callService(opElementDuJournal,
										idGroupeMaitre,
										laDate)
			 
											
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerDonnees' à l'ArrayCollection 'elementsDuJournal' et les affiches
		private function chargerDonneesResultHandler(re :ResultEvent):void{						 
			elementsDuJournal = re.result as ArrayCollection;	
			if (elementsDuJournal.length > 0){
				txtFiltre.editable = true;
				
			} else {
				txtFiltre.editable = false;
				lblError.visible = true;
			}
			myGrid.dataProvider = elementsDuJournal;
			elementsDuJournal.refresh();
		}
//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------
	}
}