package classe
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.Socket;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Endian;
	
	[Event(name="enableChange", type="myEvents.EnableChangeEvent")]
	
	public class AccesSocket extends Socket
	{
		private var hostname:String 			= "pelican"
		private var hostIp:String				= ""	
		private var port:int 					= 21
		private var reponse:String				= ""
		private var zeFile:File 	
		private var zStream:FileStream
		private var bool_PassiveMode:Boolean	= true;			// PassiveMode indique à la socket de chercher tout seul le port du service FTP. True par default
			
		// variables for reading fixed portion of file header
		private var fileName:String = new String();
		private var flNameLength:uint;
		private var xfldLength:uint;
		private var offset:uint;
		private var compSize:uint;
		private var uncompSize:uint;
		private var compMethod:int;
		private var signature:int;
		private var bytes:ByteArray = new ByteArray();
	
				
		public function AccesSocket(f:File):void
		{
			super();
			configureListeners()
			zeFile = f;
			zStream = new FileStream();
			super.connect(hostname,port);
		}
/* -------------------------- FONCTION PUBLIQUES ---------------------- */
		public function connectToFtp():void
		{
			super.connect(hostname,port);
		}
		public function disconnectToFtp():void
		{
			
		}
/* -------------------------- METHODE PRIVEE -------------------------- */
		private function configureListeners():void {
	        addEventListener(Event.CLOSE, closeHandler);
	        addEventListener(Event.CONNECT, connectHandler);
	        addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
	        addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
	        addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
	    }	
/* -------------------------- HANDLER -------------------------------- */
		private function closeHandler(event:Event):void 
		{
	        trace("closeHandler: " + event);
	      // trace(response.toString());
	    }
	
	    private function connectHandler(event:Event):void 
	    {
	        trace("connectHandler: " + event);
	    }
	
	    private function ioErrorHandler(event:IOErrorEvent):void 
	    {
	        trace("ioErrorHandler: " + event);
	    }
	
	    private function securityErrorHandler(event:SecurityErrorEvent):void 
	    {
	        trace("securityErrorHandler: " + event);
	    }
	
	    private function socketDataHandler(event:ProgressEvent):void 
	    {
	    	reponse = super.readUTFBytes(super.bytesAvailable)
	    	trace(reponse)
	    	if(reponse.indexOf("220") != -1)
	    		super.writeUTFBytes("USER container\n")
	    	if(reponse.indexOf("331") != -1)
	    		super.writeUTFBytes("PASS container\n")	
	    	if(reponse.indexOf("230") != -1)
	    		super.writeUTFBytes("PORT 192,168,3,67,4,151\n")
	     	if(reponse.indexOf("200 PORT") != -1)
	    		super.writeUTFBytes("TYPE A\n");
		 	if(reponse.indexOf("200 Type") != -1)
		 		//super.writeUTFBytes("MKD olivier\n");
				super.writeUTFBytes("STOR "+zeFile.nativePath+"\n")
	    	super.flush()
	    }
	    private function createByteArrayFromFile(zef:File):void
	    {
	   		zStream.open(zef, FileMode.READ);
			bytes.endian = Endian.LITTLE_ENDIAN;
			while (zStream.position < zef.size)
    		{
    			// read fixed metadata portion of local file header
       			zStream.readBytes(bytes, 0, 30);
				bytes.position = 0;
		        signature = bytes.readInt();
		        // if no longer reading data files, quit
		        if (signature != 0x04034b50)
		        {
		            break;
		        }
				bytes.position = 8;
        		compMethod = bytes.readByte();  // store compression method (8 == Deflate)
				offset = 0;    // stores length of variable portion of metadata 
		        bytes.position = 26;  // offset to file name length
		        flNameLength = bytes.readShort();    // store file name
		        offset += flNameLength;     // add length of file name
		        bytes.position = 28;    // offset to extra field length
		        xfldLength = bytes.readShort();
		        offset += xfldLength;    // add length of extra field
				// read variable length bytes between fixed-length header and compressed file data
       			zStream.readBytes(bytes, 30, offset);
       			bytes.position = 30; 
		        fileName = bytes.readUTFBytes(flNameLength); // read file name
		        bytes.position = 18;
		        compSize = bytes.readUnsignedInt();  // store size of compressed portion
		        bytes.position = 22;  // offset to uncompressed size
		        uncompSize = bytes.readUnsignedInt();  // store uncompressed size
				// read compressed file to offset 0 of bytes; for uncompressed files
			    // the compressed and uncompressed size is the same
			    zStream.readBytes(bytes, 0, compSize);
				if (compMethod == 8) // if file is compressed, uncompress
		        {
		            bytes.uncompress(CompressionAlgorithm.DEFLATE);
		        }
		        outFile(fileName, bytes);   // call outFile() to write out the file
			} // end of while loop
		} // end of init() method
		private function outFile(fileName:String, data:ByteArray):void
		{
		    /* var outFile:File = File.desktopDirectory; // dest folder is desktop
		    outFile = outFile.resolvePath(fileName);  // name of file to write
		    var outStream:FileStream = new FileStream();
		    // open output file stream in WRITE mode
		    outStream.open(outFile, FileMode.WRITE);
		    // write out the file
		    outStream.writeBytes(data, 0, data.length);
		    // close it
		    outStream.close(); */
		    super.writeUTFBytes("STOR "+data+"\n")
		    super.flush()
		}
	}
}