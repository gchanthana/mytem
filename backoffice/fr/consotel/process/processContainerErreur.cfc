<cfcomponent output="false">
	<cfset containerProxy = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
	<!--- MISE A JOUR DU STATUS DU RAPPORT 
		http://download.oracle.com/docs/cd/E10415_01/doc/bi.1013/e12188/T421739T524310.htm#jobhistoryinfo
		Status : "Completed", "Error", "Running", "Scheduled", "Suspended" and "Unknown" 
	--->
	<cfset STATUS_CODES=structNew()>
	<cfset STATUS_CODES["S"]="Completed">
	<cfset STATUS_CODES["F"]="Error">
	<cfset STATUS_CODES["W"]="Unknown">
	<cfset STATUS_CODES["P"]="Suspended">
	
	<cfset mailDumpInfos ={}>
	<cfset mailDumpInfos["expediteur"] = "process_container@saaswedo.com">
	<cfset mailDumpInfos["destinataire"] = "monitoring@saaswedo.com">
	<cfset mailDumpInfos["server"] = "mail-cv.consotel.fr">
	<cfset mailDumpInfos["serverPort"] = "25">
	<!--- ********************************************** --->	
	<cffunction name="run" access="public" returntype="Numeric">
		<cfargument name="JOB_ID"		type="Numeric"	required="true">
		<cfargument name="STATUS"		type="String" 	required="true">
		<cfargument name="BI_SERVER"	type="String" 	required="true">
			
		<cflog text="********************************************** PROCESSCONTAINERERREUR - function run ">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #arguments.JOB_ID#">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #arguments.STATUS#">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #arguments.BI_SERVER#">
	<!--- INSTANCIATION DE L'API CONTAINER --->	
		
		<cfset objRapport = containerProxy.getInfosFromJOBID(JOB_ID,BI_SERVER)>
		
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #objRapport.APP_LOGINID#">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #objRapport.IDRACINE#  ">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #STATUS#  ">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #objRapport.GLOBALIZATION#  ">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #objRapport.LASTNAME#  #objRapport.FIRSTNAME#">		
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #objRapport.UUID# ">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - #objRapport.ColumnList#">
		
		<cftry>
			<cfset changeStatus = fctChangeStatus(objRapport.APP_LOGINID,objRapport.IDRACINE,objRapport.UUID,objRapport.NOM_DETAILLE_RAPPORT,objRapport.DATE_DEMANDE,objRapport.RACINE,objRapport.MAIL,STATUS,objRapport.GLOBALIZATION,objRapport.LASTNAME, objRapport.FIRSTNAME,objRapport.CODE_APP)>
			<cfset deleteStatus = DELETEDocumentToFTP(objRapport.UUID)>
		<cfcatch>
			<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
			<cfmail from="#mailDumpInfos.expediteur#" 
							  to="#mailDumpInfos.destinataire#" 
							  server="#mailDumpInfos.server#" 
							  port="#mailDumpInfos.serverPort#" 
							  charset="utf-8" wraptext="74" subject="dump function RUN()" type="html">
				<cfdump var="#str#"/>
			</cfmail>
			<cfreturn -1>	
		</cfcatch>	
		</cftry>
	</cffunction>
	
	<cffunction name="fctChangeStatus" access="private" returntype="Numeric">
		<cfargument name="APP_LOGINID"			type="Numeric"	required="true">
		<cfargument name="IDRACINE"				type="Numeric"	required="true">
		<cfargument name="UUID" 				type="String"	required="true">
		<cfargument name="NOM_DETAILLE_RAPPORT" type="String"	required="true">
		<cfargument name="DATE_DEMANDE" 		type="String"	required="true">
		<cfargument name="PERIMETRE" 			type="String"	required="true">
 		<cfargument name="MAIL_DESTINATAIRE" 	type="String"	required="true">
		<cfargument name="STATUS" 				type="String"	required="true">
		<cfargument name="GLOBALIZATION" 		type="String"	required="true">
		<cfargument name="LASTNAME"				type="string"	required="true">
		<cfargument name="FIRSTNAME"				type="string"	required="true">
		<cfargument name="CODEAPPLICATION"				type="numeric" 	required="true">
		<cflog text="********************************************** PROCESSCONTAINERERREUR - function fcfChangeStatus">
		
		<!---
		<cfset maj = containerProxy.MAJStatus(UUID, 3)>
		--->

	<!--- ENVOI D'UN MAIL AU CLIENT POUR L'AVERTIR QUE LE RAPPORT EST DISPONIBLE SUR CONTAINER --->	
		<cfset knowStruct1=structNew()>
		<cfset knowStruct1["NOM_DETAILLE_RAPPORT"]	=#arguments.NOM_DETAILLE_RAPPORT#>
		<cfset knowStruct1["DATE_DEMANDE"]			=#arguments.DATE_DEMANDE#>
		<cfset knowStruct1["PERIMETRE"]				=#arguments.PERIMETRE#>
<!--- 		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#> --->
		<cfset knowStruct1["MAIL_DESTINATAIRE"]		=#arguments.MAIL_DESTINATAIRE#>
		<cfset knowStruct1["APP_LOGINID"]			=#arguments.APP_LOGINID#>
		<cfset knowStruct1["IDRACINE"]				=#arguments.IDRACINE#>
		<cfset knowStruct1["GLOBALIZATION"]			=#arguments.GLOBALIZATION#>
		<cfset knowStruct1["LASTNAME"]				=#arguments.LASTNAME#>
		<cfset knowStruct1["FIRSTNAME"]				=#arguments.FIRSTNAME#>	
		<cfset knowStruct1["CODEAPPLICATION"]				=#arguments.CODEAPPLICATION#>	
		<!--- Pour connaitre le droit de notification du client demandeur du rapport --->
		<cftry>
			<cfset knowStatus=containerProxy.SENDMailFactory("Erreur","Production",knowStruct1)>
			<cflog text="********************************************** PROCESSCONTAINERERREUR - send Mail Production : #knowstatus#">

			<cfset knowStatus=containerProxy.KNOWMailRight(APP_LOGINID,IDRACINE,"Erreur")>
			<cflog text="************************** PROCESSCONTAINERERREUR - droit de notification : #knowstatus#">
			<cfif knowStatus EQ 1>
				<!--- si le client a le droit de recevoir un mail --->
				<cfset knowStatus=containerProxy.SENDMailFactory("Erreur","Client",knowStruct1)>
				<cflog text="********************************************** PROCESSCONTAINERERREUR - send mail client : #knowstatus#">
			<cfelse>
				<cfset knowStatus = -1>
			</cfif>
		<cfcatch type="any">
			<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
			<cflog text="********************************************** #str#">
			<cfset knowStatus = -1>
		</cfcatch>
		</cftry>
		<cfreturn knowStatus>
	</cffunction>
	<cffunction name="DELETEDocumentToFTP" 			access="public" 	returntype="string" 	hint="méthode de suppression de document sur le FTP">	
			<cfargument name="uuid"	type="string"	required="true">
			
			<cfset retour = 0>
			<!---
			<cftry>
				<cffile action="delete" file="/container/#uuid#">
				<cfdirectory action="list" directory="/container/#arguments.uuid#" name="listDir" listInfo="all">
				<cfif listDir.recordCount eq 1>
					<cflog text="************** containerService.DELETEDocumentToFTP : ERREUR">
				<cfelse>
					<cflog text="************** containerService.DELETEDocumentToFTP : SUCCES">
				</cfif>
				
				<cfcatch>
					<cfset retour = -1>
				</cfcatch>
			</cftry>
			--->
			<cfset retour = uuid>
			<cfreturn retour>
		</cffunction>
</cfcomponent>
