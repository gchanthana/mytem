package univers.parametres.parametrecompte.vo
{
	[Bindable]
	public class ProfilValidationFacturation
	{
		public var idProfil : int;
		public var libelle : String;
		public var codeInterne : String;
		public var nbUser : int;
		public var commentaire : String;
		
		public function ProfilValidationFacturation()
		{
		} 

	}
}