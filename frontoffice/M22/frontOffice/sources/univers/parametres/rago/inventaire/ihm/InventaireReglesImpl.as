package univers.parametres.rago.inventaire.ihm
{
    import mx.resources.ResourceManager;
    import composants.datagrid.RowColorDataGrid;
    import composants.util.ConsoviewAlert;
    import composants.util.URLUtilities;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    import flash.system.System;
    import mx.collections.ArrayCollection;
    import mx.containers.Box;
    import mx.controls.ComboBox;
    import mx.controls.DataGrid;
    import mx.controls.Image;
    import mx.controls.Label;
    import mx.controls.RadioButton;
    import mx.controls.TextInput;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.events.DataGridEvent;
    import mx.events.ListEvent;
    import mx.formatters.DateFormatter;
    import mx.managers.PopUpManager;
    import mx.utils.ObjectUtil;
    import univers.parametres.rago.CreationRegles.event.WizardEvent;
    import univers.parametres.rago.CreationRegles.ihm.MainCreateRegleIHM;
    import univers.parametres.rago.entity.Regle;
    import univers.parametres.rago.entity.Rules;
    import univers.parametres.rago.inventaire.event.InventaireReglesEvent;
    import univers.parametres.rago.inventaire.system.GestionRegles;

    [Bindable]
    public class InventaireReglesImpl extends Box
    {
        public var gestionRegles:GestionRegles = new GestionRegles();
        public var _rules:Rules = new Rules();
        public var selectedRegle:Regle;
        public var popup:DetailRegleIHM;
        //controls ------------------------------
        public var dgListe:RowColorDataGrid;
        public var cbofiltreStatut:ComboBox;
        public var txtFiltre:TextInput;
        public var lbl_nbRegles:Label;
        public var rdTous:RadioButton;
        public var rdActives:RadioButton;
        public var rdInactives:RadioButton;
        //----------------------------------------
        public var img:Image;
        [Embed(source="/assets/images/Help3.png", mimeType='image/png')]
        public var imgTrue:Class;
        private var _newRegle:Object;

        public function set newRegle(value:Object):void
        {
            _newRegle = value;
        }

        public function get newRegle():Object
        {
            return _newRegle;
        }

        public function InventaireReglesImpl()
        {
            addEventListener(InventaireReglesEvent.ACTIVER_REGLE_CLICKED, _localeActiverRegleClickedHandler);
            addEventListener(InventaireReglesEvent.ANNULER_REGLE_CLICKED, _localeAnnulerRegleClickedHandler);
            addEventListener(InventaireReglesEvent.DESACTIVER_REGLE_CLICKED, _localeDesactiverRegleClickedHandler);
            addEventListener(InventaireReglesEvent.EXECUTER_REGLE_CLICKED, _localeExecuterRegleClickedHandler);
            addEventListener(InventaireReglesEvent.VOIR_REGLE_CLICKED, _localeVoirRegleClickedHandler);
            addEventListener(InventaireReglesEvent.CREER_REGLE_CLICKED, _localeCreerRegleClickedHandler);
//			addEventListener(InventaireReglesEvent.ATTRIBUT_IN_DATAGRID, executeAttributInDatagrid);
            addEventListener(InventaireReglesEvent.ATTRIBUT_IN_DATAGRID, initialiseRegle);
            addEventListener(InventaireReglesEvent.REFRESH_DATAGRID, initialiseRegleReturn);
            addEventListener("regleIsModified", initialiseRegle);
            addEventListener("viewMessageConfirmationErase", confirmationErasedRegle);
            //addEventListener("regleIsModifiedAndSendNewRegle",executeSendRegle);
        }

//---------------------------- FORMATS DES COLONNES ---------------------------------------------------------		
        protected function formatStatut(item:Object, col:DataGridColumn):String
        {
            if(item == null)
                return '';
            if(item.BOOL_ACTIVE)
            {
                return ResourceManager.getInstance().getString('M22', 'Actif');
            }
            else if(item.BOOL_ACTIVE == false)
            {
                return ResourceManager.getInstance().getString('M22', 'Inactif');
            }
            else
            {
                return "";
            }
        }

        protected function formatType(item:Object, col:DataGridColumn):String
        {
            if(item == null)
                return '';
            return item.OPERATEUR;
        }

        protected function formatColonneDate(item:Object, col:DataGridColumn):String
        {
            if(item == null)
                return '';
            var df:DateFormatter = new DateFormatter();
            df.formatString = 'DD/MM/YYYY';
            return df.format(item[col.dataField]);
        }

        protected function rowColorFunction(datagrid:DataGrid, rowIndex:int, color:uint):uint
        {
            var rColor:uint;
            var item:Object = datagrid.dataProvider.getItemAt(rowIndex);
            if(item == null)
                return color;
            if(!item.BOOL_ACTIVE)
            {
                rColor = 0xF5E49A;
            }
            else
                rColor = color;
            return rColor;
        }

        protected function btRetourRAGOClickHandler(evt:MouseEvent):void
        {
            dispatchEvent(new Event("noVIewRAGO", true));
        }

//------------------------------------------- SORTS ---------------------------------------------------------------------
        protected function dateCompareFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.dateCompare(itemA.CREATE_DATE, itemB.CREATE_DATE)
        }

//-------------------------------------------- FILTRES ------------------------------------------------------------------
        protected function listeReglesFilterFunction(item:Regle):Boolean
        {
            var boolStatut:Boolean = true;
            var boolText:Boolean = false;
            if(item.REGLE_NOM.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1 || item.NOM_USER_CREATE.toLowerCase().search(txtFiltre.text.toLowerCase()) !=
                -1)
            {
                boolText = true;
            }
            return boolStatut && boolText;
        }

        protected function returnRegle(event:Event):void
        {
        }

        protected function txtInputChangeHandler(event:Event):void
        {
            if(txtFiltre.text == "")
            {
                reListRegle();
            }
        }

        protected function btReinitialisationClickHandler(event:Event):void
        {
            txtFiltre.text = "";
            reListRegle();
        }

        private function zRefreshDatagridRegle():void
        {
            var arrayC:ArrayCollection = new ArrayCollection();
            if(rdTous.selected)
            {
                arrayC = _rules.listRegle;
            }
            if(rdActives.selected)
            {
                arrayC = _rules.listRegleActive;
            }
            if(rdInactives.selected)
            {
                arrayC = _rules.listRegleInactive;
            }
            dgListe.dataProvider = arrayC;
            lbl_nbRegles.text = arrayC.length.toString() + ' ' + ResourceManager.getInstance().getString('M22', '_r_gles');
        }

//-------------------------------------------- HANDLERS ------------------------------------------------------------------
        protected function imgClickHandler():void ///////////////***********************************---------------------------UUURRRLLLLL
        {
            var u:URLRequest = new URLRequest("http://www.consotel.fr/elearningrago/RAGOdef.htm");
            URLUtilities.openWindow(u.url);
        }

        protected function imgCreationCompletehandler():void
        {
            img.visible = true;
            img.source = imgTrue;
        }

        protected function _localeActiverRegleClickedHandler(event:InventaireReglesEvent):void
        {
            gestionRegles.rules = _rules;
            gestionRegles.activerDesactiverRegle(dgListe.selectedItem as Regle);
            callLater(reListRegle);
        }

        protected function _localeAnnulerRegleClickedHandler(event:InventaireReglesEvent):void //////////////////////////////////////////////////////////////
        {
            var popUpErasrConfir:PopUpConfirmationEraseRegleIHM = new PopUpConfirmationEraseRegleIHM();
            popUpErasrConfir.addEventListener("eraseRegleNow", eraseNowRegle);
            popUpErasrConfir.addEventListener("noEraseRegleNow", noEraseNowRegle);
            PopUpManager.addPopUp(popUpErasrConfir, this, true);
            PopUpManager.centerPopUp(popUpErasrConfir);
        }

        protected function eraseNowRegle(event:Event):void
        {
            gestionRegles.rules = _rules;
            gestionRegles.listeRegles.addItem(_rules.listRegle);
            gestionRegles.supprimerRegle((dgListe.selectedItem as Regle), dgListe.selectedIndex);
            reListRegle();
        }

        protected function noEraseNowRegle(event:Event):void
        {
        }

        private function confirmationErasedRegle(event:Event):void
        {
            ConsoviewAlert.afficherOKImage("Votre règle a été effacée.", this);
        }

        protected function _localeDesactiverRegleClickedHandler(event:InventaireReglesEvent):void
        {
            gestionRegles.rules = _rules;
            gestionRegles.activerDesactiverRegle(dgListe.selectedItem as Regle);
        }

        protected function _localeVoirRegleClickedHandler(event:InventaireReglesEvent):void
        {
            gestionRegles.rules = _rules;
            gestionRegles.detaillerRegle(dgListe.selectedItem as Regle);
        }

        protected function _localeExecuterRegleClickedHandler(event:InventaireReglesEvent):void
        {
            gestionRegles.rules = _rules;
            gestionRegles.executerRegle(dgListe.selectedItem as Regle);
            gestionRegles.addEventListener("isUsed", showDetails);
        }

        protected function _dgListeChangeHandler(event:ListEvent):void
        {
            selectedRegle = dgListe.selectedItem as Regle;
        }

        protected function _localRefreshDatagrid(event:Event):void
        {
        }

        protected function _dgListeEditBeginningHandler(event:DataGridEvent):void
        {
            if(event.itemRenderer != null && event.itemRenderer.data != null && event.dataField != null)
            {
                if(event.itemRenderer.data[event.dataField] != null)
                {
                    var txt:String = event.itemRenderer.data[event.dataField];
                    System.setClipboard(txt);
                }
            }
            event.preventDefault();
        }

        protected function _localeCreerRegleClickedHandler(event:InventaireReglesEvent):void
        {
            //do Nothing
        }

        protected function _txtFiltreChangeHandler(event:Event):void
        {
            if(dgListe.dataProvider != null)
            {
                (dgListe.dataProvider as ArrayCollection).filterFunction = listeReglesFilterFunction;
                (dgListe.dataProvider as ArrayCollection).refresh();
            }
        }

        protected function _cbofiltreStatutChangeHandler(event:ListEvent):void
        {
            if(cbofiltreStatut.selectedIndex == -1)
                cbofiltreStatut.selectedIndex = 0;
            if(dgListe.dataProvider != null)
            {
                (dgListe.dataProvider as ArrayCollection).filterFunction = listeReglesFilterFunction;
                (dgListe.dataProvider as ArrayCollection).refresh();
            }
        }

        protected function NewReglesClickHandler(event:Event):void
        {
            dispatchEvent(new Event("viewNewRegle", true));
        }

        /**
         * Exportation des règles au format CSV
         * */
        protected function OnExportRulesHandler(event:MouseEvent):void
        {
            exportRulesCSV(event);
        }

        private function exportRulesCSV(event:MouseEvent):void
        {
            /* var csvData:String = DataGridDataExporter.getCSVString(dgListe, ";", "\n", false);
               var url:String = modulePerimetreIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/gridexports/exportCSVString.cfm";
             DataGridDataExporter.exportCSVString(url, csvData, "export");*/
            var url:String = modulePerimetreIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M22/exportRagoCSV.cfm";
            // ref ticket #1659
            var variables:URLVariables = new URLVariables();
            variables.IDSOURCE = 0;
            variables.IDCIBLE = 0;
            variables.CLEF = "";
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = URLRequestMethod.POST;
            navigateToURL(request, "_blank");
        }

        private function afficherWizardCreationRegle():void
        {
            var p:MainCreateRegleIHM = new MainCreateRegleIHM();
            p.newRegle = newRegle;
            p.addEventListener(WizardEvent.VALIDER_CREATION_ET_EXECUTER_REGLE, _localValiderCreationEtExecuterRegleHandler);
            p.addEventListener(WizardEvent.VALIDER_CREATION_REGLE, _localValiderCreationHandler);
            p.addEventListener("regleCreated", initialiseRegleReturn);
            PopUpManager.addPopUp(p, this, true);
//			PopUpManager.centerPopUp(p);
            reListRegle();
        }

        protected function _localValiderCreationEtExecuterRegleHandler(event:WizardEvent):void
        {
            gestionRegles.ajouterRegle(event.regle, true);
        }

        protected function _localValiderCreationHandler(event:WizardEvent):void
        {
            gestionRegles.ajouterRegle(event.regle);
        }

        protected function initialiseRegleReturn(event:Event):void
        {
            _rules.listRulesRacine(0, 0, "");
            _rules.addEventListener("listRacine", executeAttributInDatagrid);
        }

        protected function initialiseRegle(event:Event):void
        {
            _rules.listRulesRacine(0, 0, "");
            _rules.addEventListener("listRacine", executeAttributInDatagrid);
        }

        protected function reListRegle():void
        {
            _rules.listRulesRacine(0, 0, "");
            _rules.addEventListener("listRacine", executeAttributInDatagrid);
        }

        //Attribut au datagrid les différents champs retourné par la procédure <<<<<<< PROBLEME A REMETTRE >>>>>>
        private function executeAttributInDatagrid(event:Event):void
        {
            lbl_nbRegles.text = _rules.listRegle.length.toString() + ' ' + resourceManager.getString('M22', '_r_gles');
            refreshDatagrid();
        }

        private function refreshDatagrid():void
        {
            var arrayC:ArrayCollection = new ArrayCollection();
            if(rdTous.selected)
            {
                arrayC = _rules.listRegle;
            }
            if(rdActives.selected)
            {
                arrayC = _rules.listRegleActive;
            }
            if(rdInactives.selected)
            {
                arrayC = _rules.listRegleInactive;
            }
            dgListe.dataProvider = arrayC;
            lbl_nbRegles.text = arrayC.length.toString() + ' ' + resourceManager.getString('M22', '_r_gles');
        }

        protected function rdBtnGrpChangeHandler(event:Event, rdbtnSelectedNbr:int):void
        {
            var tempArrayC:ArrayCollection = new ArrayCollection();
            if(_rules.listRegle.length != 0)
            {
                switch(rdbtnSelectedNbr)
                {
                    case 0:
                        tempArrayC = _rules.listRegle;
                        break;
                    case 1:
                        tempArrayC = _rules.listRegleActive;
                        break;
                    case 2:
                        tempArrayC = _rules.listRegleInactive;
                        break;
                }
                dgListe.dataProvider = tempArrayC;
            }
            else
            {
                dgListe.dataProvider = _rules.listRegle;
            }
            lbl_nbRegles.text = tempArrayC.length.toString() + ' ' + ResourceManager.getInstance().getString('M22', '_r_gles');
        }

        private function showDetails(e:Event):void
        {
            popup = new DetailRegleIHM();
            popup.SelectedRegle = gestionRegles.tempRegle;
            popup.dispatchEvent(new Event("SEARCH_NODE"));
            gestionRegles.addEventListener("exceptionWasRefresh", listExceptionSendToPopUpNewException);
            popup.addEventListener("newExceptionIsCreated", executeListNewException);
            popup.addEventListener("regleIsModifiedAndSendNewRegle", gestionRegles.executeSendRegle);
            popup.addEventListener("findNewExceptionCreatedNow", gestionRegles.sendToPopUpNewExceptionReceived);
            popup.addEventListener("exceptionsIsClosed", refreshGrid);
            PopUpManager.addPopUp(popup, this, true);
            PopUpManager.centerPopUp(popup);
        }

        private function refreshGrid(e:Event):void
        {
            gestionRegles.closeExceptions(e);
            dispatchEvent(new Event("regleIsModified"));
        }

        //Rafraichie le grid de la popUp 'newExceptionIHM'
        private function listExceptionSendToPopUpNewException(event:Event):void
        {
            popup.SelectedRegle = gestionRegles.tempRegle;
            popup.dispatchEvent(new Event("exceptionWasRefresh", true));
        }

        //Rafraichie le grid de la popUp 'newExceptionIHM'
        private function executeListNewException(event:Event):void
        {
            popup.SelectedRegle = gestionRegles.tempRegle;
            popup.dispatchEvent(new Event("exceptionWasRefresh", true));
        }
    }
}