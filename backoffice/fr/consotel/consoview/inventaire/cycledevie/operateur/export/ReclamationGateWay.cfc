<cfcomponent name="ReclamationGateWay" output="false">
	
	<cffunction name="envoyer" returntype="numeric" access="public">		
		
		<cfargument name="Reclamation" required="true" type="Reclamation">
		
		<cfreturn Reclamation.envoyer()>
		
	</cffunction>
	
	
	<cffunction name="afficher" returntype="numeric" access="public">				
		<cfargument name="Reclamation" required="true" type="Reclamation">
		<cfreturn Reclamation.afficher()>
	</cffunction>
	
</cfcomponent>