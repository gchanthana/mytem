package catalogue.util
{
	
	

	/**
	 * Cette classe gère la devise d'origine du catalogue client et sa syntaxe.
	 */
	public class CatalogueFormatter
	{
		
		public var operatorCurrency:Currency;
		public var distributorCurrency:Currency;
		
		private static var _catalogueFormatter:CatalogueFormatter;
		
		public static function getInstance():CatalogueFormatter
		{
			if (!_catalogueFormatter)
			{
				_catalogueFormatter = new CatalogueFormatter();
			}
			return _catalogueFormatter;
		}
		
		public function formatOperateurCurrency(num:Number):String
		{
			
			return operatorCurrency.format(num);
		}
		
		//formate la monnaie avec la précision passée en parametre	
		public function formatRevendeurCurrency(num:Number, precision:uint = 2):String
		{
			
			return distributorCurrency.format(num);
		}
		
		
		
		public function setRevendeurCurrency(value:String):void
		{
			distributorCurrency = null;
			distributorCurrency = new Currency(value);			
		}
		
		public function setOperateurCurrency(value:String):void
		{
			operatorCurrency = null;
			operatorCurrency = new Currency(value);			
		}
	}
}