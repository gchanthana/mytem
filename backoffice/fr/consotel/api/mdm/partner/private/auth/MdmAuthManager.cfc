<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.auth.MdmAuthManager" hint="Implémentation utilisée pour controler l'accès aux API partenaires privées">
	<cffunction access="public" name="getAuthManager" returntype="fr.consotel.api.mdm.partner.private.auth.MdmAuthManager" hint="Retourne une instance de cette classe">
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="isAuthValid" returntype="boolean"
	hint="Retourne TRUE si les infos d'authentification authCredentials sont validées par l'implémentation ClientIdentityProvider correspondante">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Structure représentant les infos d'authentification et contenant au moins les clés suivantes :
		- DOMAIN : Valeur utilisée par l'API pour déterminer l'implémentation fr.consotel.api.mdm.auth.ClientIdentityProvider à utiliser pour vérifier l'authentification utilisateur">
		<cfset var userCredentials=ARGUMENTS["authCredentials"]>
		<cfif (NOT structIsEmpty(userCredentials)) AND structKeyExists(userCredentials,"DOMAIN") AND isValid("String",userCredentials["DOMAIN"])>
			<cfset var authDomain=userCredentials["DOMAIN"]>
			<!--- Implémentation ClientIdentityProvider à utiliser pour valider les infos d'authentification fournis  --->
			<cfset var identityProvider=getIdentityProvider(authDomain)>
			<cfset var authIdentity=identityProvider.getAuthIdentity(userCredentials)>
			<cfreturn identityProvider.assertAuthIdentity(authIdentity)>
		<cfelse>
			<!--- Le contenu des credentials d'authentification fournis sont invalides --->
			<cfreturn FALSE>
		</cfif>
	</cffunction>
	
	<cffunction access="package" name="getIdentityProvider" returntype="fr.consotel.api.mdm.partner.private.auth.ClientIdentityProvider"
	hint="Retourne l'instance ClientIdentityProvider utilisée par cette classe pour valider les infos d'authentification pour le domaine : authDomain">
		<cfargument name="authDomain" type="String" required="true" hint="Valeur associée à la clé DOMAIN dans la structure qui est fournie à isAuthValid()">
		<cfset var identityDomain=ARGUMENTS["authDomain"]>
		<cfset var identityProviders=getIdentityProviders()>
		<cfif structKeyExists(identityProviders,identityDomain)>
			<cfset var identityProvider=identityProviders[identityDomain]["AUTHORITY"]>
			<cfreturn identityProvider>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Aucune autorité d'authentification cliente ne correspond au domaine #identityDomain#">
		</cfif>
	</cffunction>

	<cffunction access="package" name="getIdentityProviders" returntype="Struct"
	hint="Retourne une structure où le nom de chaque clé correspondan à une valeur valide pour la clé DOMAIN contenu dans la structure passée à isAuthValid()
	Chaque clé est associée à une structure contenant au moins les clés suivantes :
	- AUTHORITY : Implémentation de type ClientIdentityProvider et qui se trouve dans le package fr.consotel.api.mdm.auth
	- AUTH_DESC : Description correspondant à la valeur de AUTHORITY et/ou au nom de la clé">
		<cfif NOT structKeyExists(VARIABLES,"AUTH_DOMAINS")>
			<cfset var authDomainList=authDomainsList()>
			<cfset VARIABLES["AUTH_DOMAINS"]={}>
			<cfloop index="i" from="1" to="#authDomainList.RECORDCOUNT#">
				<cfset var domainName=authDomainList["DOMAIN"][i]>
				<cfset var authorityImpl=authDomainList["AUTHORITY"][i]>
				<!--- Implémentation fr.consotel.api.mdm.partner.private.auth.ClientIdentityProvider --->
				<cfset var identityProvider=createObject("component",authorityImpl).getIdentityProvider()>
				<cfif NOT structKeyExists(VARIABLES["AUTH_DOMAINS"],domainName)>
					<!--- Validation de confiance auprès de l'entité Identity Provider --->
					<cfset VARIABLES["AUTH_DOMAINS"][domainName]={
						AUTHORITY=identityProvider, AUTH_DESC=authDomainList["AUTH_DESC"][i]
					}>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn VARIABLES["AUTH_DOMAINS"]>
	</cffunction>

	<cffunction access="package" name="authDomainsList" returntype="Query" hint="Retourne une requête contenant les colonnes suivantes :
	- DOMAIN : Valeur identifiant l'implémentation spécifiée dans AUTHORITY (Chaque valeur doit être unique)
	- AUTHORITY : Nom d'une classe du package fr.consotel.api.mdm.auth de type ClientIdentityProvider (Sans le nom du package)
	- AUTH_DESC : Description correspondant à la valeur de AUTHORITY et/ou de DOMAIN">
		<cftry>
			<cfquery name="qAuthDomainList" datasource="ROCOFFRE">
				SELECT		'demo.consotel.fr' as DOMAIN, 'DemoIdentityProvider' as AUTHORITY, 'Exemple' as AUTH_DESC
				FROM		DUAL
				UNION
				SELECT		'cv.consotel.fr' as DOMAIN, 'CvIdentityProvider' as AUTHORITY, 'Implémentation simple pour ConsoView' as AUTH_DESC
				FROM		DUAL
				/* Stratégie d'authentification utilisée par les API Partenaires Publiques (Aucune authentification utilisateur n'est requise) */
				UNION
				SELECT		'public.consotel.fr' as DOMAIN, 'PublicIdentityProvider' as AUTHORITY, 'Stratégie utilisée par les API Partenaires Publiques' as AUTH_DESC
				FROM		DUAL
				ORDER BY AUTHORITY
			</cfquery>
			<cfreturn qAuthDomainList>
			<cfcatch type="database">
				<cflog type="error" text="Using default query : #CFCATCH.Detail# (#CFCATCH.Message#)">
				<cfset var qAuthDomainList=queryNew("DOMAIN,AUTHORITY,AUTH_DESC","VarChar,VarChar,VarChar")>
				<cfset queryAddRow(qAuthDomainList,3)>
				<cfset querySetCell(qAuthDomainList, "DOMAIN","demo.consotel.fr",1)>
				<cfset querySetCell(qAuthDomainList, "AUTHORITY","DemoIdentityProvider",1)>
				<cfset querySetCell(qAuthDomainList, "AUTH_DESC","Exemple",1)>
				<cfset querySetCell(qAuthDomainList, "DOMAIN","cv.consotel.fr",2)>
				<cfset querySetCell(qAuthDomainList, "AUTHORITY","CvIdentityProvider",2)>
				<cfset querySetCell(qAuthDomainList, "AUTH_DESC","Implémentation simple pour ConsoView",2)>
				<cfset querySetCell(qAuthDomainList, "DOMAIN","public.consotel.fr",3)>
				<cfset querySetCell(qAuthDomainList, "AUTHORITY","PublicIdentityProvider",3)>
				<cfset querySetCell(qAuthDomainList, "AUTH_DESC","Stratégie utilisée par les API Partenaires Publiques",3)>
				<cfreturn qAuthDomainList>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>