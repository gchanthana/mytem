package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	
	public class ViewStackObject
	{
		
		public var pool					:Pool = null;
		public var operateur			:Object = null;
			
		public var compte				:ArrayCollection = null;
		public var sousCompte			:ArrayCollection = null;
		public var compteSousCompte		:ArrayCollection = null;
		public var numberLinesSelected	:ArrayCollection = null;
		
		public var isMultiCompte		:Boolean = false;
		public var isMultiSousCompte	:Boolean = false;
		
		
		
		public function ViewStackObject()
		{
		}

	}
}