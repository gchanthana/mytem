<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>
	
	
	<!--- ------------- --->
	<!--- ENVOI DE MAIL --->
	<!--- ------------- --->
	<cffunction name="getDivEmail" access="public" output="true" returnType="string"
				displayname="" 
				hint="Génère la div contenant les infos d'un colaborateur et de son terminal / template D1" >
				
		<cfargument name="idSousTete" required="false" type="numeric" default=0 >
		<cfargument name="pageRetour" required="true" type="numeric" >
		<!--- <cfargument name="confirmEmail" required="true" type="boolean"> --->
		
		<cfset idTerminal = 0>
		<cfset idSIM = 0>
		<cfset sous_tete = 0>
		
		<cfif idSousTete eq 0><!--- PREMIERE FOIS SUR LA PAGE D'ACCUEIL (AUCUN IDSOUSTETE N'A ETE SPECIFIE) --->
			
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
							
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfbreak>
					
				</cfif>
			</cfloop>
		
		<!--- SI CE N'EST PAS LE PREMIER ACCES À LA PAGE D'ACCUEIL / VARIABLES DEJA INITIALISÉES / RECUPERATION DES AUTRE VARIABLES DONT ON A BESOIN --->
		<cfelse>

			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] eq idSousTete>
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfbreak>
				</cfif>
			</cfloop>
		
		</cfif>
		
		
		<!--- query gestionnaire --->
		<cfset queryGestionnaire = createObject("component","fr.consotel.consoview.parametres.gestionnaire.gestionnaire").getGestionnaire_pool(SESSION.IDPOOL) >
		
		
		<!--- CREATION D'UNE STRUCTURE POUR LA CONSTRUCTION DE L'URL --->
		<cfset struct = {idPage = "SendMailANDLog",
						pageRetour = "#pageRetour#",
						sous_tete = "#sous_tete#",
						idSIM = "#idSIM#",
						idSous_tete = "#idSousTete#",
						idTerminal = "#idTerminal#"
		} >
		<cfset urlString = createRefURL(struct)>
		<!--- DIV POPUP EMAIL --->
		<cfset mail = '
				
			<div class="divModalTransparenteEmail" id="ombreee2" style="display:none;">
				<div id="popupEmaill" class="popupEmaill" style="">
				<form action="#urlString#" id="popupEnvoiMail" method="post">
					<input type="hidden" name="formExp" value="#SESSION.USER.EMAIL#">
					<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
						<colgroup>
							<col width="5%"></col>
							<col width="15%"></col>
							<col width="75%"></col>
							<col width="5%"></col>
						</colgroup>
						<tr>
							<td align="center" colspan="3" ><span id="erreurEmptyDestinataireEmail" style="color:red;font-weight:bold;display:none;">Veuillez saisir une adresse</span></td>
							<td align="right" colspan="1"><a style="cursor:pointer" onclick="closePopup(''ombreee2'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="text-indent:10px;">À</td>
		'>
		
		<cfif queryGestionnaire.RecordCount eq 0 || queryGestionnaire.RecordCount eq 1>
		<cfif queryGestionnaire.RecordCount eq 1>
			<cfset gestDest = queryGestionnaire.LOGIN_EMAIL>
		<cfelseif queryGestionnaire.RecordCount eq 0 >
			<cfset gestDest = "">
		</cfif>
			<cfset mail &= '
							<td>:&nbsp;<input id="dest" name="dest" type="text" style="width:96%" value="#gestDest#"></td>
		    '>
		
		<cfelse>
		
			<cfset mail &= '
							<td>:&nbsp;<select id="dest" name="dest" class="" style="">
			'>
		
			<cfloop query="queryGestionnaire">
				<cfset currentRow = queryGestionnaire.currentrow>
				
				<cfset adrEmail = "" >
				<cfif queryGestionnaire["LOGIN_EMAIL"][currentRow] neq "" >
					<cfset adrEmail = queryGestionnaire["LOGIN_EMAIL"][currentRow] >
				</cfif>
				
				<cfset mail &= '<option value="#adrEmail#">#adrEmail#</option>'>
			</cfloop>
			
			<cfset mail &= '				
							</select>
							</td>
			'>
	
		</cfif>
		
		<cfset mail &= '					
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="text-indent:10px">CC</td>
							<td>:&nbsp;<input id="exped" name="exped" type="text" style="width:96%" value=""></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="text-indent:10px">Objet</td>
							<td>:&nbsp;<input id="obj" name="obj" type="text" style="width:96%" value=""></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td colspan="2">
								<textarea id="email" name="email" rows="10" cols="47"></textarea>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td></td>
							<td align="right"><input style="width:120px;" type="submit" onclick="javascript:if(verificationEnvoiMail()){ document.forms[''popupEnvoiEmail''].submit();loadForEnvoiEmail(); }else{ return false;}" value="Envoyer" id="sendEmailToGest" name="sendEmailToGest"></td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</form>
				</div>
			</div>
		'>
		
		<cfreturn mail >
	</cffunction>
	
	
	<!--- -------------------------- --->
	<!--- CONFIRMATION ENVOI DE MAIL --->
	<!--- -------------------------- --->
	<!--- <cffunction name="confirmEnvoiMail" access="public" output="true" returnType="string"
				displayname="" 
				hint="Génère la div contenant confirmant l'envoi d'un mail / template D1" >
		
		<cfargument name="confirmEmail" required="false" type="boolean" default="false" >
		
		<cfif confirmEmail eq true>
			<cfset popupConfirmEmail = "display:block;">
		<cfelse >
			<cfset popupConfirmEmail = "display:none;">
		</cfif>
		
		<cfset confirm = '
			<div class="divModalTransparentePopulEmail" id="divModalTransparentePopulEmail" style="#popupConfirmEmail#">
				<div id="popupConfirmEmail" class="popupConfirmEmail" style="">
					<form action="" method="post">
						<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
							<tr>
								<td align="right"><a style="cursor:pointer" onclick="closePopup(''divModalTransparentePopulEmail'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td align="center" style="font-weight:bold;font-size:12px;color:##009FE3;">L''email a bien été envoyé.</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr style="height:0px;">
				              <td align="center" style="">
					              <input type="button" value="Fermer" name="popupPriseEnCompte" onclick="closePopup(''divModalTransparentePopulEmail'')" style="" >
							  </td>
				            </tr>
						</table>
					</form>
				</div>
			</div>
		'>
		
		<cfreturn confirm >
	</cffunction> --->
	
	
</cfcomponent>