<cfcomponent name="DatasetExamples" alias="fr.consotel.consoview.api.Reporting.data.DatasetExamples">
	<cfset DATASOURCE="ROCOFFRE">
	<cffunction name="getDatasourceName" access="remote" returntype="any">
		<cfargument name="IDRACINE_MASTER" required="true" type="numeric"/>
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="PERIODICITE" required="true" type="numeric"/>
		<cfreturn getReportData(arguments.IDRACINE_MASTER,arguments.ID_PERIMETRE,arguments.DATEDEB,arguments.PERIODICITE)>
	</cffunction>
	
	<cffunction name="getReportData" access="remote" returntype="xml">
		<cfargument name="IDRACINE_MASTER" required="true" type="numeric"/>
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="PERIODICITE" required="true" type="numeric"/>
		<cfset DATEFIN=lsDateFormat(dateAdd("m",arguments.PERIODICITE,parseDateTime(arguments.DATEDEB)),"yyyy/mm/dd")>
        <cfstoredproc datasource="#DATASOURCE#" procedure="PKG_CV_REP_V3.REP1_DETAIL_FILAIRE_SYNT_GRP">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.IDRACINE_MASTER#">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ID_PERIMETRE#">
	        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#DATEFIN#">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PERIODICITE#">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfif qGetReportData.recordcount EQ 0>
			<cfquery datasource="#DATASOURCE#" name="qGetReportData">
				select -1 as NB, 'Aucun enregistrement' as HAVE_DATA from DUAL
			</cfquery>
		</cfif>
		<cfset queryUtils=createObject("component","fr.consotel.consoview.api.Reporting.export.ExportService")>
		<cfreturn queryUtils.queryToXml(qGetReportData)>
	</cffunction>
	
	<cffunction name="getInternational" access="remote" returntype="xml">
		<cfargument name="IDRACINE_MASTER" required="true" type="numeric"/>
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="PERIODICITE" required="true" type="numeric"/>
		<cfset DATEFIN=lsDateFormat(dateAdd("m",arguments.PERIODICITE,parseDateTime(arguments.DATEDEB)),"yyyy/mm/dd")>
       	<cfstoredproc datasource="#DATASOURCE#" procedure="PKG_CV_REP_V3.REP2_DETAIL_FILAIRE_SYNT_GRP">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.IDRACINE_MASTER#">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ID_PERIMETRE#">
	        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#DATEFIN#">
	        <cfprocresult name="qGetInternationalData">
        </cfstoredproc>
		<cfset queryUtils=createObject("component","fr.consotel.consoview.api.Reporting.export.ExportService")>
		<cfreturn queryUtils.queryToXml(qGetInternationalData)>
	</cffunction>
	
	<cffunction name="getListeAppels" access="remote" returntype="string" hint="ORA-Error : Unable to extend temp segment">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="DATEDEB" required="true" type="string"/>
		<cfargument name="PERIODICITE" required="true" type="numeric"/>
		<cfset DATEFIN=lsDateFormat(dateAdd("m",arguments.PERIODICITE,parseDateTime(arguments.DATEDEB)),"yyyy/mm/dd")>
        <cfstoredproc datasource="#DATASOURCE#" procedure="PKG_CV_REP_V3.REP3_DETAIL_FILAIRE_SYNT_GRP">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ID_PERIMETRE#">
	        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.DATEDEB#">
	        <cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.DATEFIN#">
	        <cfprocresult name="qGetAppelListData">
        </cfstoredproc>
		<cfset queryUtils=createObject("component","fr.consotel.consoview.api.Reporting.export.ExportService")>
		<cfreturn queryUtils.queryToXmlString(qGetAppelListData)>
	</cffunction>
</cfcomponent>
