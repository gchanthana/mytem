package univers.inventaire.equipements.lignes.v2.views.popups
{
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionEtatRaccordement;

	[Bindable]
	public class SelectRaccoImpl extends ResilierImpl
	{
		public var cbModeRacco : ComboBox;
		public var cbOperateur : ComboBox;
		
		private var _actionRacco : Object;
		public function get actionRacco():Object{
			return _actionRacco;
		} 
		public function set actionRacco(act :Object):void{
			_actionRacco = act;
		}
		
		
		
		public function SelectRaccoImpl()
		{
			super();
		}
		
		protected function setDateRange():void{
			var range : Object = new Object();
			//etc
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
		}
		
		//======== HANDLERS =========================================================================================
		protected function creationCompleteHandler(fe : FlexEvent):void{
			
		}
		//======== FIN HANDLERS =====================================================================================
	}
}