<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<cfreturn>
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getAccueilDivContactGestionnaire" access="public" output="true"  returnType="string"
				displayname="" 
				hint="Génère la div contenant les dernieres actions sur les lignes et terminal d'un collaborateur / template D1" >
		
		<cfargument name="idSousTete" required="false" type="numeric" default=0 >
		
		<cfset divContactGest = '
				
				<div class="divLastAction_pilotage" style="">
					<div class="divRightLasAction_pilotage" style="width:410px;">
						<div style="margin-bottom:10px;">
							<div class="pictoLastAction" style=""><img src="img/mytem/mes_dernieresActions.png"></div>
							<div class="tdLastActionIntitule" style="">Contacter mon gestionnaire</div>
							<div class="clear" style=""></div>
						</div>
					</div>
					<div align="center" class="divContactGest_pilotage" style="">
						<input align="center" type="button" onclick="popupEmail(''ombreee'')" class="btnContacterGestionnaireAccueil" name="contactGest" value="contacter mon gestionnaire">
					</div>
				</div>
		'>
		
		<cfreturn divContactGest >
	</cffunction>
	
</cfcomponent>