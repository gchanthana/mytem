package service.kpi.updateposition
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class UpdatePositionKpiService
	{
		private var _model:UpdatePositionKpiModel;
		public var handler:UpdatePositionKpiHandler;
		private var remote:RemoteObject;
	
		public function UpdatePositionKpiService()
		{
			this._model = new UpdatePositionKpiModel();
			this.handler = new UpdatePositionKpiHandler(_model);
		}		
		
		public function get model():UpdatePositionKpiModel
		{
			return this._model;
		}

		public function updatePositionKpi(kpiSource:Object,kpiDestination :Object):void
		{		
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.PositionManager";
			remote.addEventListener(ResultEvent.RESULT,handler.UpdatePositionKpiResultHandler,false,0,true);
				
			var tabObjet:Array=[];
			
			if(kpiDestination==null)
			{
				tabObjet[0]=kpiSource;
				remote.updatePosition(tabObjet, 1);
			}
			else
			{
				tabObjet[0]=kpiSource;
				tabObjet[1]=kpiDestination;
				remote.updatePosition(tabObjet, 1);	
			}
		}	
	}
}