package service.destinataire.ajoutermodifierprofil
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import vo.destinataire.Profil;
	
	public class AjouterModifierProfilService 
	{
		private var _model:AjouterModifierProfilModel;
		public var handler:AjouterModifierProfilHandler;	
		
		public function AjouterModifierProfilService()
		{
			this._model  = new AjouterModifierProfilModel();
			this.handler = new AjouterModifierProfilHandler(model);
		}		
		
		public function get model():AjouterModifierProfilModel
		{
			return this._model;
		}
		
		public function ajouterProfil(profil :Profil):void
		{		
			var callRemote:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.AjouterModifierProfilService","ajouterProfil",handler.ajouterProfilResultHandler); 
			
			RemoteObjectUtil.callService(callRemote,profil);
		}
		
		public function modifierProfil(profil :Profil):void
		{		
			var callRemote:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.AjouterModifierProfilService","modifierProfil",handler.modifierProfilResultHandler); 
			
			RemoteObjectUtil.callService(callRemote,profil);
		}
		
		public function supprimerProfil(id:int):void
		{		
			var callRemote:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.AjouterModifierProfilService","supprimerProfil",handler.supprimerResultHandler); 
			
			RemoteObjectUtil.callService(callRemote,id); 
		}
		
		public function supprimerPlusieursProfils(id:int):void
		{		
			var callRemote:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.destinataires.AjouterModifierProfilService","supprimerProfilCascade",handler.supprimerPlusieursProfilsResultHandler); 
			
			RemoteObjectUtil.callService(callRemote,id); 
		}
	}
}