package gestionparc.services.useracess
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	
	import gestionparc.entity.GlobalParameterPO;
	import gestionparc.entity.UserAccessPO;
	import gestionparc.entity.UserSessionVO;
	import gestionparc.event.UserAccessModelEvent;

	internal class UserAccessModel extends EventDispatcher
	{
		
		private var _userAccess			:UserAccessPO;
		private var _listOS				:ArrayCollection = new ArrayCollection();
		private var _racineInfos		:ArrayCollection = new ArrayCollection();
		
		private var _globalParameter	:GlobalParameterPO;
		
		
		public function UserAccessModel()
		{
			_userAccess = new UserAccessPO();
			_globalParameter = new GlobalParameterPO();
		}

		private var _checkPassWordLastResult:Number = -1;
		public function get checkPassWordLastResult():Number
		{
			return _checkPassWordLastResult;
		}
		
		
		//les droits de l'utilisateur 
		public function get userAccess():UserAccessPO
		{
			return _userAccess;
		}
		
		//les paramètres global (utilisateur, infos racine, os géré ....)
		public function get globalParameter():GlobalParameterPO
		{
			return _globalParameter
		}
		
		
		//infos sur la racine notemment le droit MDM
		public function get racineInfos():ArrayCollection
		{
			return _racineInfos;
		}
		
		//liste des os gérés
		public function get listOS():ArrayCollection
		{
			return _listOS;
		}

				
		internal function updateUserAccess(value:Array):void
		{	
			userAccess.removeAll();	
			racineInfos.removeAll();
			listOS.removeAll();
			var key_racine:String = "";
			var key_master:String = "";
			if(value)
			{	
				var cursor:IViewCursor = (value[0] as ArrayCollection).createCursor();
				
				while(!cursor.afterLast)
				{	
					key_racine = "RACINE_" + cursor.current.CODE_PARAMS;
					key_master = "MASTER_" + cursor.current.CODE_PARAMS;
//					_userAccess[cursor.current.CODE_PARAMS]= cursor.current.IS_ACTIF;
					if(cursor.current.CODE_PARAMS =='SER_FILTER')
						_userAccess[cursor.current.CODE_PARAMS]=cursor.current.IS_ACTIF_RACINE_PARAM;
					else
						_userAccess[cursor.current.CODE_PARAMS]=cursor.current.IS_ACTIF;
					
					_userAccess[key_racine]=cursor.current.IS_ACTIF_RACINE_PARAM;
					_userAccess[key_master]=cursor.current.IS_ACTIF_PARAM_MASTER;
					
					cursor.moveNext();
				}
				_globalParameter.userAccess = _userAccess;
			}	
			
			
			if(value[1])
			{	
				var cursor1:IViewCursor = (value[1] as ArrayCollection).createCursor();
				
				while(!cursor1.afterLast)
				{	
					racineInfos.addItem(cursor1.current);
					cursor1.moveNext();
				}
				
				_globalParameter.racineInfos = racineInfos;
			}
			
			if(value[2])
			{
				var cursor2:IViewCursor = (value[2] as ArrayCollection).createCursor();
				
				while(!cursor2.afterLast)
				{	
					listOS.addItem(cursor2.current);
					cursor2.moveNext();					
				}
				
				_globalParameter.listOS = listOS;
			}
			
			// USER SESSION INFOS
			if(value[3])
			{
				if(userAccess.SER_FILTER == 1)
				{
					var cursor3:IViewCursor = (value[3] as ArrayCollection).createCursor();
					while(!cursor3.afterLast)
					{	
						_globalParameter.userSession = new UserSessionVO();
						_globalParameter.userSession.IDPOOL 		= Number(cursor3.current.IDPOOL);
						_globalParameter.userSession.COLONNE_CODE 	= (cursor3.current.COLONNE_CODE != null) ? cursor3.current.COLONNE_CODE:'';
						_globalParameter.userSession.COLONNE_VALUE 	= (cursor3.current.COLONNE_VALUE !=null) ? cursor3.current.COLONNE_VALUE:'';
						
						cursor3.moveNext();					
					}
					
				}
			}
			
			dispatchEvent(new UserAccessModelEvent(UserAccessModelEvent.UAME_USER_ACCESS_UPDATED));
		}
		
		internal function updateCheckPassWordResult(value:Number):void
		{
			_checkPassWordLastResult = value;
			dispatchEvent(new UserAccessModelEvent(UserAccessModelEvent.UAME_CHECK_PASSWORD_UPDATED));
		}

	}
}