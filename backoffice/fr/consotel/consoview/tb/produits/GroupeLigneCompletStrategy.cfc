<!--- =========================================================================
Classe: GroupeLigneCompletStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeLigneCompletStrategy" hint=""  extends="Strategy" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GroupeLigneCompletStrategy" hint="Remplace le constructeur de GroupeLigneCompletStrategy.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_V3.TB_PRODUIT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idproduit_catalogue" value="#IDproduit#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
		<cfreturn p_result/> 
		
	</cffunction>
	
	<cffunction name="getData_cat" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_V3.TB_PRODUIT_CAT">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idproduit_catalogue" value="#IDproduit#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
		<cfreturn p_result/> 
		
	</cffunction>
	
	<cffunction name="getData_mobile" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_V3.TB_PRODUIT_MOBILE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idproduit_catalogue" value="#IDproduit#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
		<cfreturn p_result/> 
		
	</cffunction>
	
	<cffunction name="getData_cat_mobile" access="public" returntype="query" output="false" hint="Raménes les données principales pour le dernier niveau du tableau de bord (Détails des produits)." >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_V3.TB_PRODUIT_CAT_MOBILE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#DateDebut#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" variable="p_datefin" value="#DateFin#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idproduit_catalogue" value="#IDproduit#"/>			
	        <cfprocresult name="p_result"/>        
		</cfstoredproc>
		<cfreturn p_result/> 
		
	</cffunction>	
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" hint="Retourne le type du produit passé en paramétre." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getProduit" access="public" returntype="string" output="false" hint="Retourne le libellé du produit." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getOperateur" access="public" returntype="string" output="false" hint="Retourne le nom de l'opérateur pour le produit concerné." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" hint="Retourne le libéllé du périmétre." >
		<cfargument name="IDcompte" required="false" type="numeric" default="" displayname="numeric IDcompte" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" hint="Retourne l'appellation du périmétre (Ex: société = compte Hiérarchique)." >
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>