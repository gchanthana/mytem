package composants.historique {
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import mx.controls.Alert;
	import mx.collections.ArrayCollection;
	import flash.utils.getQualifiedClassName;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.utils.ObjectUtil;
	import flash.events.Event;
	import mx.core.ClassFactory;
	import mx.rpc.remoting.mxml.RemoteObject;
	import mx.rpc.events.FaultEvent;
	import univers.UniversManager;
	import mx.core.IFactory;
	import mx.controls.dataGridClasses.DataGridItemRenderer;

	public class HistoriqueDataGrid extends DataGrid {
		public static const HISTO_DATA_GRID_READY:String = "HISTO_DATA_GRID_READY";
		public static const RAISON_SOCIALE_COLUMN:String = "RAISON_SOCIALE";
		public static const OP_NOM_COLUMN:String = "OPNOM";
		public static const OFFRENOM_COLUMN:String = "OFFRENOM";
		public static const TYPEDATA_COLUMN:String = "TYPEDATA";
		public static const LIBELLE:String = "LIBELLE";
		private var dateDeb:Date;
		private var dateDebString:String;
		private var dateFin:Date;
		private var dateFinString:String;
		private var customRenderer:IFactory = new ClassFactory(historicItemRenderer);
		public var defaultRenderer:IFactory = new ClassFactory(DataGridItemRenderer);
		
		//private var histoRemoteObject:RemoteObject;
		
		public function HistoriqueDataGrid() {
			super();
			//histoRemoteObject = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			//histoRemoteObject.source = "fr.consotel.consoview.facturation.optimisation.historique.HistoriqueContext";
			//histoRemoteObject.showBusyCursor = false;
			defaultRenderer = this.itemRenderer;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			this.visible = false;
			this.showDataTips = true;
			//this.dataTipFunction = nbFormat;
		}
		
		public function nbFormat(c:Event):void {
			trace("HistoriqueDataGrid nbFormat() :\n" + c.toString());
		}
		
		public function get DATE_DEB():Date {
			return dateDeb;
		}

		public function get DATE_FIN():Date {
			return dateFin;
		}
		
		public function loadHistoData(dateDebParam:Date,dateFinParam:Date):void {
			this.visible = false;
			dateDeb = dateDebParam;
			dateFin = dateFinParam;
			// Format YYYY/MM/01
			dateDebString = dateDeb.fullYear.toString();
			var dateDebMonthString:String = null;
			dateFinString = dateFin.fullYear.toString();
			var dateFinMonthString:String = null;
			if((dateDeb.month + 1).toString().length == 1)
				dateDebMonthString = "0" + (dateDeb.month + 1).toString();
			else
				dateDebMonthString = (dateDeb.month + 1).toString();
				
			if((dateFin.month + 1).toString().length == 1)
				dateFinMonthString = "0" + (dateFin.month + 1).toString();
			else
				dateFinMonthString = (dateFin.month + 1).toString();
			dateDebString = dateDebString + "/" + dateDebMonthString + "/01";
			dateFinString = dateFinString + "/" + dateFinMonthString + "/01";
			loadHistoColumnMap();
		}
		
		private function loadHistoColumnMap():void {
			//histoRemoteObject.addEventListener(ResultEvent.RESULT,buildHistoColumn);
			//histoRemoteObject.addEventListener(FaultEvent.FAULT,histoRemotingFault);
			//histoRemoteObject.getHistoriqueColumnMap(this.dateDebString,this.dateFinString);
			
			var getHistoColumnMapOp:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.facturation.optimisation.historique.HistoriqueContext",
																	"getHistoriqueColumnMap",buildHistoColumn);
			RemoteObjectUtil.callService(getHistoColumnMapOp,this.dateDebString,this.dateFinString);
			
		}
		
		private function buildHistoColumn(event:ResultEvent):void {
			//histoRemoteObject.removeEventListener(ResultEvent.RESULT,buildHistoColumn);
			//histoRemoteObject.removeEventListener(FaultEvent.FAULT,histoRemotingFault);
			
			var tmpDataGridColumn:DataGridColumn;
			var tmpColumnArray:Array = event.result as Array;
			var tmpColumns:Array = new Array(tmpColumnArray.length);
			var i:int = 0;
			for(i = 0; i < tmpColumns.length; i++) {
				if (tmpColumnArray[i].COLUMN_FIELD != TYPEDATA_COLUMN)
				{
					tmpColumns[i] = new DataGridColumn(tmpColumnArray[i].COLUMN_LABEL);
					tmpDataGridColumn = tmpColumns[i] as DataGridColumn;
					tmpDataGridColumn.dataField = tmpColumnArray[i].COLUMN_FIELD;
					
					if(tmpDataGridColumn.dataField == "RAISON_SOCIALE")
						tmpDataGridColumn.width = 250;
					if(tmpDataGridColumn.dataField == HistoriqueDataGrid.OP_NOM_COLUMN)
						tmpDataGridColumn.width = 150;
					if(tmpDataGridColumn.dataField == HistoriqueDataGrid.OFFRENOM_COLUMN)
						tmpDataGridColumn.width = 150;
					if((tmpDataGridColumn.dataField.charAt(0) == "M") &&
						(tmpDataGridColumn.dataField.charAt(1) == "_")) {
						tmpDataGridColumn.width = 25;
						tmpDataGridColumn.headerText="";
					}
				}
			}
			// Colonne Supplémentaire
			var tmp:DataGridColumn = new DataGridColumn("N");
			tmp.headerText = "";
			tmpColumns.push(tmp);
			this.columns =
					tmpColumns;
			fillHistoDataGrid();
		}
		
		private function fillHistoDataGrid():void {
			//histoRemoteObject.addEventListener(ResultEvent.RESULT,displayHistoData);
			//histoRemoteObject.addEventListener(FaultEvent.FAULT,histoRemotingFault);
			var tmpTypePerimetre:String = UniversManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			var tmpPerimetreIndex:int = UniversManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			//histoRemoteObject.getHistoriqueData(tmpPerimetreIndex,tmpTypePerimetre,this.dateDebString,this.dateFinString);
			
			var getHistoOp:AbstractOperation =
						RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.facturation.optimisation.historique.HistoriqueContext",
																	"getHistoriqueData",displayHistoData);
			RemoteObjectUtil.callService(getHistoOp,tmpPerimetreIndex,tmpTypePerimetre,this.dateDebString,this.dateFinString);
			
		}
		
		private function displayHistoData(event:ResultEvent):void {
			//histoRemoteObject.removeEventListener(ResultEvent.RESULT,displayHistoData);
			//histoRemoteObject.removeEventListener(FaultEvent.FAULT,histoRemotingFault);
			
			var resultSet:ArrayCollection = event.result as ArrayCollection;
			this.dataProvider = resultSet;
			this.visible = true;
			this.itemRenderer = customRenderer;
			dispatchEvent(new Event(HistoriqueDataGrid.HISTO_DATA_GRID_READY));
		}
		
		private function formatDataTip(obj:Object):String {
			trace(obj.listData);
			return "test";
		}
		
		private function histoRemotingFault(event:FaultEvent):void {
			if(cv.verboseFaultHandler == true) {
				Alert.show(event.toString(),"HistoriqueDataGrid histoRemotingFault() VERBOSE MODE");
				trace("HistoriqueDataGrid histoRemotingFault() VERBOSE MODE : " + event.toString());
			} else {
				trace("HistoriqueDataGrid histoRemotingFault() : " + event.toString());
			}
		}
	}
}
