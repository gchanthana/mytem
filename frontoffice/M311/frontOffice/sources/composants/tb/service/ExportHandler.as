package composants.tb.service
{
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	import composants.util.ConsoviewAlert;
	
	public class ExportHandler
	{		
		public function ExportHandler()
		{
		}
		
		public function getExportInventaireHandler(event:ResultEvent):void
		{
			if(event.result!=-1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M311','Votre_demande_de_rapport_est_trait_'));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M311','Votre_demande_de_rapport_n_a_pu__tre_trait_'));
			}
		}
		
		public function getExportDetailAppelHandler(event:ResultEvent):void
		{
			if(event.result!=-1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M311','Votre_demande_de_rapport_est_trait_'));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M311','Votre_demande_de_rapport_n_a_pu__tre_trait_'));
			}
		}
		
	}
}