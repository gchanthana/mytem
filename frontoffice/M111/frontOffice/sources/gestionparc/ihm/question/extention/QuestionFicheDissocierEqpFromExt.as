package gestionparc.ihm.question.extention
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheDissocierEqpFromExt
	{
		private var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheDissocierEqpFromExt()
		{
			Alert.yesLabel=ResourceManager.getInstance().getString('M111', 'oui');
			Alert.noLabel=ResourceManager.getInstance().getString('M111', 'non');
			Alert.cancelLabel=ResourceManager.getInstance().getString('M111', 'Annuler')
			Alert.okLabel=ResourceManager.getInstance().getString('M111', 'Valider')

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			if (ligne.IDEMPLOYE > 0)
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIEREQPFROMEXT"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "Etre_vous_s_r_de_vouloir_dissocier_cette_extention_de_l__quipement"), ResourceManager.getInstance().getString("M111", "Dissocier_extention_de_l__quipement"), alertClickHandler);
			}
		}

		/**
		 * permet de capter la réponse de l'utilisateur
		 */
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				dissocierEqpFromExt();
			}
		}
		
		private function qnCloseHandler(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
			{
				dissocierEqpFromExtIfOK();
			}
			else if (evt.detail == Alert.NO)
			{
				dissocierEqpFromSimIfNotOk()
			}
			else if (evt.detail == Alert.CANCEL)
			{

			}
		}

		private function dissocierEqpFromExtIfOK():void
		{
			serv.dissocierEqpFromExt1(setObj());
		}

		private function dissocierEqpFromSimIfNotOk():void
		{
			serv.dissocierEqpFromExt2(setObj());
		}

		private function dissocierEqpFromExt():void
		{
			serv.dissocierEqpFromExt3(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_EXT=SpecificationVO.getInstance().elementDataGridSelected.IDEXTENSION;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}
	}
}