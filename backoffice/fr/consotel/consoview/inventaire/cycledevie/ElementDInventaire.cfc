<cfcomponent name="ElementDInventaire">	
	
	
	<!--- :::::::::::::::: la liste des produits de facturation des sous tetes selectionnees :::::::::::::::::::: --->
	<cffunction name="getListeElementIventaire" access="public" returntype="query">
		<cfargument name="listeDeSousTete" type="array" required="true">
		<cfset liste = arraytolist(listeDeSousTete,",")>
		<cfflush interval="100">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CYV_LISTEELEMENTINVENTAIRE">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_list_idsous_tete" value="#liste#"/>
			<cfprocresult name="p_result"/>        
		</cfstoredproc>		
		<cfreturn p_result/>		
	</cffunction>	
	
	<!--- :::::::::::::::: la liste des produits du client :::::::::::::::::::: --->
	<cffunction name="getListeProduitsClient" access="public" returntype="query">			
		<cfargument name="listeDeSousTete" type="array" required="true">
		<cfargument name="type" type="numeric" required="true">				
					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CATALOGUE_LIGNE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idsous_tete" value="#listeDeSousTete[1].IDSOUS_TETES#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_filtre" value="#type#"/>			
			<cfprocresult name="p_result"/>        
		</cfstoredproc>		
		
		<cfreturn p_result/>		
	</cffunction>			
	
	<!---::::::::::::::::::::::::::::: Liste des produits de l?opération ::::::::::::::::::::::::::::::::
     :: retourne la liste des produits concernés par l?opération  
     :: param in  : idinv_operations.
     :: param out : idinventaire_produit, le non de l?opérateur, idsous_tetes,sous_tetes
     :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->  
     <cffunction name="getListeProduitsOperation" access="public" returntype="query" output="false">                                          
		<cfargument name="idOperation" type="numeric" required="true">		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.LISTE_IPR_OPE">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>		
			<cfprocresult name="p_result"/>        
		</cfstoredproc>
		<cfreturn p_result/>						
     </cffunction>         

	<!---:::::::::::::::::::::::::: Inserer la liste des produits d?une opération dans la base ::::::::::::

     :: insert   la liste des produits concernés par l?operation  dans la base(table inv_liste_operations je pense)

     :: param in  : idinv_operations., une liste de idinventaire_produit (xxxxxxx,xxxxxxxxxx,xxxxxxxxx,xxxxxxx)

     :: param out :  1 en cas de succès 

     :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 
     <cffunction name="saveListeProduitsOperation" access="public" returntype="numeric" output="false">  
		<cfargument name="idOperation" type="numeric" required="true">                                        
	 	<cfargument name="listeDeProduits" type="array" required="true">			
		<cfset liste = arraytolist(listeDeProduits,",")>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.INSERT_IPR_ILO">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_liste_idinvproduit" value="#liste#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>					
			<cfprocparam cfsqltype="CF_SQL_NUMERIC"  type="out" variable="p_result"/>			  
		</cfstoredproc>	
				
		<cfreturn p_result/>					
     </cffunction>  
	
	   
<!---:::::::::::::::::::::::::: charge la liste des produits pour le rapprochement ::::::::::::
     :: liste des produits de l'inventaire appartenant a l'operation (passer en param : IDIN_VOPERATIONS)
	 :: avec les regul theorique etc 
     :: param in  : idinv_operations., dateRef date de reference pour le calcul
     :: 
     :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 
     <cffunction name="getListeElementFacturationTheoriqueOp" access="public" returntype="query" output="false">  
		<cfargument name="idOperation" type="numeric" required="true">                                        
	 	<cfargument name="dateRef" type="string" required="true">	
	 	<cfargument name="type" type="numeric" required="true">
	 		 			
		<cfset date_Ref = transformDate(dateRef)>					
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.CALCUL_REGUL_OP">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>					
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_ref" value="#date_Ref#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_creation" value="#type#"/>					
			<cfprocresult name="p_result">		  
		</cfstoredproc>			
		
		<!--- <cfset newQuery = formateQuerry(p_result)> --->
		 	
		<cfreturn p_result/>					
     </cffunction>
	
	<cffunction name="formateQuerry" access="private" returntype="query" output="false" >
		<cfargument name="myQuery" type="query" required="true">  
		
		<cfquery name="result" dbtype="query">
			select OPERATEURID,
				   OPNOM,
				   IDINV_ETAT,
				   LIBELLE_ETAT,
				   IDINVENTAIRE_PRODUIT,
				   IDSOUS_TETE,
				   SOUS_TETE,
				   IDPRODUIT_CLIENT,
				   LIBELLE_PRODUIT,
				   PRIX_UNIT,
				   DATE_DERNIERE_FACT,
				   NBJOUR,
				  ((PRIX_UNIT / 30) * NBJOUR )  as ESTIMATION
		 	from myQuery 		 	
		</cfquery>   
		
		<cfreturn result> 		
	</cffunction>     
	
<!---:::::::::::::::::::::::::: charge la liste des produits pour le rapprochement ::::::::::::
     :: 
     :: 
     :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 
     <cffunction name="getListeElementFacturationReel" access="public" returntype="query" output="false">                         
	 	
		<cfargument name="idOperation" type="numeric" required="true">     
		<cfargument name="dateRef" type="string" required="true">
		<cfargument name="type" type="numeric" required="true">
		
		<cfset date_Ref = transformDate(dateRef)>		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.LISTE_ELEMENT_FACTURE">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_operations" value="#idOperation#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_ref" value="#date_Ref#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_creation" value="#type#"/>						
			<cfprocresult name="p_result">		  
		</cfstoredproc>	
				
		<cfreturn p_result/>					
     </cffunction>     
 
<!--- ::::::::::::::::::::::::::: la liste des ressources d'un groupe de ligne :::::::::::::::::::::::::::::------------->
	<cffunction name="getListeRessources" access="public" returntype="query" output="false">                         	 	
		<cfargument name="idgroupe_client" type="numeric" required="true">    		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_CYCLEVIE_V3.RESSOURCE_RECHERCHE">		
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgroupe_client" value="#idgroupe_client#"/>	
			<cfprocresult name="p_result">		  
		</cfstoredproc>	
		<cfreturn p_result/>					
     </cffunction>     

	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="false" default="">		
		<cfif len(oldDateString) neq 0>
			<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
			<cfreturn newDateString> 
		<cfelse>
			<cfreturn oldDateString> 
		</cfif>		
	</cffunction>
			 	
</cfcomponent> 