package univers.parametres.rago.inventaire.ihm
{
	import flash.events.Event;
	
	import mx.containers.HBox;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpConfirmationImpl extends HBox
	{
		
		public function PopUpConfirmationImpl()
		{  }
		
		protected function btnYesClickHandler(event:Event):void
		{
			dispatchEvent(new Event("eraseExceptionNow",true));
			PopUpManager.removePopUp(this);
		}
		
		protected function btnNoClickHandler(event:Event):void
		{
			dispatchEvent(new Event("noEraseExceptionNow",true));
			PopUpManager.removePopUp(this);
		}
	}
}