package catalogue.vo
{
	public class RevendeurVO
	{
		private var _ID_REVENDEUR:Number;
		private var _LIBELLE_REVENDEUR:String;
		private var _SYMBOL_DEVISE:String;
		public var SELECTED:Boolean = false;
		
		public function RevendeurVO()
		{
		}


		public function set ID_REVENDEUR(value:Number):void
		{
			_ID_REVENDEUR = value;
		}

		public function get ID_REVENDEUR():Number
		{
			return _ID_REVENDEUR;
		}
		public function set LIBELLE_REVENDEUR(value:String):void
		{
			_LIBELLE_REVENDEUR = value;
		}

		public function get LIBELLE_REVENDEUR():String
		{
			if(_LIBELLE_REVENDEUR)
			{
				return _LIBELLE_REVENDEUR;
			}
			else
			{
				return "-";
			}
			
		}
		
		public function set SYMBOL_DEVISE(value:String):void
		{
			_SYMBOL_DEVISE = value;
		}
		
		public function get SYMBOL_DEVISE():String
		{
			return _SYMBOL_DEVISE;
		}
	}
}