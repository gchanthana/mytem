package commandemobile.popup
{
	import commandemobile.entity.Commande;
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	import commandemobile.ihm.mail.ActionMailBoxImpl;
	import commandemobile.itemRenderer.DetailsItemREndererIHM;
	import commandemobile.itemRenderer.GeneralitemREndererIHM;
	import commandemobile.itemRenderer.HistoriqueItemRendererIHM;
	import commandemobile.system.AbstractGestionPanier;
	import commandemobile.system.Action;
	import commandemobile.system.ActionEvent;
	import commandemobile.system.CommandeEvent;
	import commandemobile.system.Export;
	import commandemobile.system.GestionCommandeMobile;
	import commandemobile.system.GestionWorkFlow;
	import commandemobile.system.Transporteur;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritCommandeMobile;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpDetailsCommandeImpl extends TitleWindow
	{
		
		//IHM
		public var general					:GeneralitemREndererIHM;
		public var historique				:HistoriqueItemRendererIHM;
		public var details					:DetailsItemREndererIHM;
		private var _pUpMailBox				:ActionMailBoxIHM;
		private var _popUp					:PopUpSaisieTechniqueIHM;
		private var _pUpTransporteurs		:PopUpLivraisonIHM;
		private var pop						:PopUpAnnulerCommandeIHM

		//COMPOSANTS
		public var cbxActions				:ComboBox;
		public var btnValiderAction			:Button;
		
		
		//OBJETS
		public var _cmd						:Commande;
		public var _gestionCmd				:GestionCommandeMobile = new GestionCommandeMobile();
		private var _gestionPanier			:AbstractGestionPanier = new AbstractGestionPanier();
		private var selectedAction			:Action;
		public var _gworkFlow				:GestionWorkFlow = new GestionWorkFlow();
		private var _actionMail				:ActionMailBoxImpl;
		
		//VARIABLES LOCALES
		public var _listActions				:ArrayCollection = new ArrayCollection();
		public var idCommande				:int = 0;
		public var hbxVisible				:Boolean = false;
		public var active					:Boolean = true;
		
		private var _articlesXML			:XML = new XML();
		private var _articlesLeft			:ArrayCollection = new ArrayCollection();
		private var _listArticles			:ArrayCollection = new ArrayCollection();
		private var _listREquiredForPopUP	:ArrayCollection = new ArrayCollection();
		
		private var _infosMail:InfosObject = new InfosObject();
		public function set infosMail(value:InfosObject):void
		{
			_infosMail  = value
		}
		public function get infosMail():InfosObject
		{
			return _infosMail	
		}
		
		public static const MAIL_SENT:String = "mailSent";	
		
		private var gabFactory : GabaritCommandeMobile = new GabaritCommandeMobile();
		
		public function PopUpDetailsCommandeImpl()
		{
			addEventListener("commandeArrived",									commandeArrivedHandler);
			addEventListener("sendArticlesSelected",							sendMAJArticles);
//			addEventListener(CommandeEvent.COMMANDE_UPDATED,					closePopUp);
			addEventListener("thisMailSent",									sendActions);
			
			_gestionCmd.addEventListener("detailsFinished",						getDetailsOperationArrived);
			_gestionCmd.addEventListener(CommandeEvent.COMMANDE_UPDATED,		closePopUp);
			
			_gworkFlow.addEventListener("actionsFinished",						getActionsResultHandler);
			_gworkFlow.addEventListener("actionsWorkFlowFinished",				getactionsWorkFlowFinishedhandler);

			_gestionPanier.addEventListener("articlesSent",						sendMAJCommande);
			_gestionPanier.addEventListener("fournirArticlesCommandeFinished",	openLivraisosnIHM);
		}
		
////////////////////////////////		
		
		 protected function btLivraisonClickHandler(event:MouseEvent):void
	    {
	    	if(_gworkFlow.boolLivree)
	    	{
	    		sendActions(event);
	    	}
	    }

		protected function btExpeditionClickHandler(event:MouseEvent): void
	    {
	    	var boolNumTracking:Boolean = false;
	    	var boolTransporteur:Boolean = false;
	    	var message:String = "";
	    	if(_cmd.NUMERO_TRACKING != "")
	    	{
	    		boolNumTracking = true	    		
	    	}
	    	else
	    	{
	    		message += " - Pas de numéro de suivi spécifié\n"
	    	}
	    	if(_cmd.IDTRANSPORTEUR)
	    	{
	    		boolTransporteur = true	    		
	    	}
	    	else
	    	{
	    		message += " - Pas de transporteur spécifié"
	    	}
	    	
	    	if(boolTransporteur && boolNumTracking)
	    	{
	    		navigateToURL(new URLRequest(Transporteur(general.cbxTransporteur.selectedItem).URL_TRACKING+_cmd.NUMERO_TRACKING)/* */,"_blank")
	    		
	    	}
	    	else
	    	{
	    		Alert.show(message,"Erreur");
	    	}
	    }
	    
	    protected function btExporterClickHandler(event:MouseEvent):void
	    {
	    	if(_cmd.IDCOMMANDE > 0)
	    	{
	    		var export:Export = new Export();
	    		export.exporterCommandeEnPDF(_cmd);
	    	}	
	    }

/////////////////////////////		
		
		protected function _closeHandler(e:Event):void
		{
			dispatchEvent(new Event("popUpDetailsClosed"));
			PopUpManager.removePopUp(this);
		}
			
		protected function cbxActionsClosehandler():void
		{
			if(cbxActions.selectedItem != null)
			{
				btnValiderAction.enabled = true;
			}
		}
		
		private function getactionsWorkFlowFinishedhandler(e:Event):void
		{
			historique.dispatchEvent(new Event("getProcedure"));
			fournirListeActionsPossibles();
		}
		
		private function commandeArrivedHandler(e:Event):void
		{
			_gestionCmd.fournirDetailOperation(_cmd,idCommande);
		}
		
		private function getDetailsOperationArrived(e:Event):void
		{
			_cmd = new Commande();
			_cmd = _gestionCmd.selectedCommande;
			_gworkFlow.fournirListeActionsPossibles(_cmd.IDLAST_ETAT,_cmd);
			general._cmd 			= _cmd;
			historique.cmd			= _cmd;
			details._cmd 			= _cmd; 
			dispatchEventCommande();
		}
		
		private function fournirListeActionsPossibles():void
		{
			_gworkFlow.fournirListeActionsPossibles(_cmd.IDLAST_ETAT,_cmd);
		}
		
		private function getActionsResultHandler(e:Event):void
		{	
			if(_gworkFlow.listeActionsPossibles.length > 0)
			{
				hbxVisible = true;
				if(_gworkFlow.listeActionsPossibles.length > 1)
				{
					cbxActions.prompt = "Sélectionnez";
				}

				_listActions = _gworkFlow.listeActionsPossibles;
			}
			else
			{
				hbxVisible = false;
			}
		}
		
		protected function creationCompleteHandler(ihmNumber:int):void
		{

		}
		
		protected function btnValiderActionClickHandler():void
		{
			var bool:Boolean =	false;
			_listREquiredForPopUP = new ArrayCollection();
			if(cbxActions.selectedItem != null)
			{
				selectedAction = cbxActions.selectedItem as Action;
				if(cbxActions.selectedItem.CODE_ACTION != null)
				{
					if(cbxActions.selectedItem.CODE_ACTION == "ANNUL")
					{
						pop = new PopUpAnnulerCommandeIHM();
						pop.selectedAction = selectedAction;
						pop.libelleAction = selectedAction.LIBELLE_ACTION;
						pop.addEventListener("messageSaisie",messageSaisieHandler);
						PopUpManager.addPopUp(pop,this,true);
						PopUpManager.centerPopUp(pop);
					}
					else
					{
						if(cbxActions.selectedItem.CODE_ACTION == "EXPED")
						{
							afficherTransporteurs();
						}
						else
						{
							afficherMailBox();
						}
					}
				}
				else
				{
					dispatchEvent(new Event("thisMailSent"));
				}
				
			}
			else
			{
				Alert.show("Veuillez sélectionner une action!","Consoview");
			}
		}
		
		private function messageSaisieHandler(e:Event):void
		{
			pop.dispatchEvent(new Event("closeThis"));
			dispatchEvent(new Event("thisMailSent"));
		}
		
		private function sendActions(e:Event):void
		{
			_gworkFlow.faireActionWorkFlow(cbxActions.selectedItem as Action,_cmd,true);
			if(selectedAction.CODE_ACTION == "LIVRE")
			{
				manageLivraison();
			}
		}
		private function manageLivraison():void
        {	
			_gestionPanier.fournirArticlesCommande(_cmd);
        }
        
		protected function btnValiderClickHandler():void
		{
			checkCommandeChange();
			calculNewPrice();
			_cmd.MONTANT = calculNewPrice();
			var e:Event = new Event(Event.ACTIVATE);
			giveArticle(e);
			_cmd.MONTANT = calculNewPrice();
		}
		
		private function sendMAJCommande(e:Event):void
		{
			_gestionCmd.majCommande(_cmd);
		}
		
		private function calculNewPrice():Number
		{
			var calculDuPrix:Number = 0;
			for(var i:int = 0;i < details.listArticles.length;i++)
			{
				if(details.listArticles[i].PRIX != null || Number(details.listArticles[i].PRIX).toString() != "NaN")
				{
					calculDuPrix = calculDuPrix + Number(details.listArticles[i].PRIX);
				}
			}
			return	calculDuPrix;
		}
		
		private function checkCommandeChange():void
		{
			if(general.txtLibelle.text != _cmd.LIBELLE_COMMANDE)
			{
				_cmd.LIBELLE_COMMANDE = general.txtLibelle.text;
			}
			if(general.txtNumeroCommande.text != _cmd.NUMERO_COMMANDE)
			{
				_cmd.NUMERO_COMMANDE = general.txtNumeroCommande.text;
			}
			if(general.txtReferenceClient.text != _cmd.REF_CLIENT)
			{
				_cmd.REF_CLIENT = general.txtReferenceClient.text;
			}
			if(general.txtRefRevendeurOperateur.text != _cmd.REF_OPERATEUR)
			{
				_cmd.REF_OPERATEUR = general.txtRefRevendeurOperateur.text;
			}
			if(general.dcLivaraison.selectedDate != _cmd.LIVRAISON_PREVUE_LE)
			{
			 	_cmd.LIVRAISON_PREVUE_LE = general.dcLivaraison.selectedDate;
			}
			if(general.txtCommentaires.text != _cmd.COMMENTAIRES)
			{
			 	_cmd.COMMENTAIRES = general.txtCommentaires.text;
			}
			if(general.txtTracking.text != _cmd.NUMERO_TRACKING)
			{
				_cmd.NUMERO_TRACKING = general.txtTracking.text;
			}
			if(general.cbxTransporteur.selectedItem != null)
			{
				_cmd.LIBELLE_TRANSPORTEUR = general.cbxTransporteur.selectedItem.LIBELLE_TRANSPORTEUR;
				_cmd.IDTRANSPORTEUR = general.cbxTransporteur.selectedItem.IDTRANSPORTEUR;
			}
		}
		
		
		protected function btnAnnulerClickHandler():void
		{
			dispatchEvent(new Event("popUpDetailsClosed"));
			PopUpManager.removePopUp(this);
		}
			
		private function dispatchEventCommande():void
		{
			general.dispatchEvent(new Event("getProcedure"));
			historique.dispatchEvent(new Event("getProcedure"));
			details.dispatchEvent(new Event("getProcedure"));
		}
		
		private function giveArticle(e:Event):void
		{
			_articlesXML = details._articlesXML;
			_articlesLeft = details.listArticles;
			formatXMLToArrayCollection();
			eraseArticlesErased();
		}
		
		private function formatXMLToArrayCollection():void
		{
			_listArticles = new ArrayCollection();
			var xmlList:XMLList = _articlesXML.children();
			var nbre:int = xmlList.length();
			if(nbre > 1)
			{
				formatXMLList(xmlList);
			}
			else
			{
				formatXML(xmlList[0]);
			}
		}
		
		private function formatXMLList(xmlList:XMLList):void
		{	
			for(var i:int = 0;i < xmlList.length();i++)
			{
				formatXML(xmlList[i]);
			}
		}
		
		private function formatXML(xml:XML):void
		{
			for(var i:int = 0;i < _articlesLeft.length;i++)
			{
				if(Number(xml.idsoustete[0]) == _articlesLeft[i].IDSOUSTETE)
				{
					_listArticles.addItem(xml);
				}
			}
		}	

		private function eraseArticlesErased():void
		{
			_cmd.ARTICLES = <articles></articles>;
			for(var i:int = 0;i < _listArticles.length;i++)
			{
				_cmd.ARTICLES.appendChild(_listArticles[i]);
			}
			dispatchEvent(new Event("sendArticlesSelected"));
		}

		private function sendMAJArticles(e:Event):void
		{
			_gestionPanier.majArtilcesOperation(_cmd,_cmd.ARTICLES);
		}
		
		private function closePopUp(e:Event):void
		{
			dispatchEvent(new Event("popUpDetailsClosed"));
			PopUpManager.removePopUp(this);
		}
		
		protected function afficherTransporteurs():void
		{	
			_pUpTransporteurs = new PopUpLivraisonIHM();
			_pUpTransporteurs.cmd = _cmd;		
			_pUpTransporteurs.addEventListener("transporteurIsSelected",transporteurIsSelectedCloseHandler);
			PopUpManager.addPopUp(_pUpTransporteurs,this,true);
			PopUpManager.centerPopUp(_pUpTransporteurs);
		}
		
		private function transporteurIsSelectedCloseHandler(e:Event):void
		{
			afficherMailBox();
		}
		
		protected function afficherMailBox():void
		{	
			_pUpMailBox = new ActionMailBoxIHM();
			infosMail.commande = _cmd;
			infosMail.historiqueCommande = historique.listHistorique;
			infosMail.corpMessage = selectedAction.MESSAGE;
			var objet:String = "Réf. : "  + infosMail.commande.REF_CLIENT;
			_pUpMailBox.initMail("Commande Mobile",objet,"");
			_pUpMailBox.configRteMessage(infosMail,GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			_pUpMailBox.selectedAction = selectedAction;
			PopUpManager.addPopUp(_pUpMailBox,this,true);
			PopUpManager.centerPopUp(_pUpMailBox);
		}
		
		protected function _pUpMailBoxMailEnvoyeHandler(ev : Event):void
		{	
			selectedAction.COMMENTAIRE_ACTION = _pUpMailBox.txtCommentaire.text; //txtCommentaire.text;
	    	selectedAction.DATE_ACTION = new Date();//dcDateAction.selectedDate;
	    		    	
	    	var actionEvent:ActionEvent = new ActionEvent(ActionEvent.ACTION_CLICKED);
	    	actionEvent.action = selectedAction;
	    	dispatchEvent(actionEvent);	    	
	    	
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE,_pUpMailBoxMailEnvoyeHandler);
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
			dispatchEvent(new Event("thisMailSent"));
		}

        private function manageLivraison2():void
        {	
			_gestionPanier.fournirArticlesCommande(_cmd);
        }
        
        private function openLivraisosnIHM(e:Event):void
        {
        	if(_popUp != null)
        	{
	        	_popUp = new PopUpSaisieTechniqueIHM();
				PopUpSaisieTechniqueIHM(_popUp).commande = _cmd;
				_popUp.addEventListener("livraisonClosed",popUpSaisieTechniqueHandler);
				PopUpManager.addPopUp(_popUp,this,true);
				PopUpManager.centerPopUp(_popUp);
        	}
        }
        
        private function popUpSaisieTechniqueHandler(e:Event):void
        {
        	dispatchEvent(new Event("thisMailSent"));
        }

	}
}