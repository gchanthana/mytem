package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class AffecterSimLigne implements IComportementAffectation
	{
		 
		
		public function affecter(idSIM : int,idLigne : int,libelle_SIM : String, libelle_ligne : String, date : String, pret : int):void
		{
		 
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"affectLigneToSIM_V3",
																		affecter_handler);
																		
			RemoteObjectUtil.callService(op,idLigne,idSIM,libelle_ligne,libelle_SIM,date);
			
			
		 
		}
		
	 
		private function affecter_handler (evt : ResultEvent):void
		{
			if(evt.result > 0)
			{
				ConsoviewAlert.afficherOKImage("Affectation de la ligne à la SIM effectuée");
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
		public function annuler():void
		{
			Alert.show("fermer");
		}
	}
}