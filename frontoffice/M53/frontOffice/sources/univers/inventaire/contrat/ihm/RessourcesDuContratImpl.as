package univers.inventaire.contrat.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;

	[Event(name="btMajOptionsClicked")]
	[Event(name="btGererOptionsClicked")]
	public class RessourcesDuContratImpl extends VBox
	{
		public function RessourcesDuContratImpl()
		{
			super();
		}
		

		protected function _btMajOptionsClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new Event("btMajOptionsClicked"));
		}
		
		protected function _btGererOptionsClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new Event("btGererOptionsClicked"));
		}
	}
}