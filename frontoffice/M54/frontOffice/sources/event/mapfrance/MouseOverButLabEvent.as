package event.mapfrance
{
	import flash.events.Event;

	/**
	 * classe relative à l'événement MouseOverButLabEvent, ayant comme propriété _buttonId de type String
	 */
	public class MouseOverButLabEvent extends Event
	{
		private var _buttonId:String;

		//bubbles indiquent si l'evenement peut être propagé, cancelable s'il peut être arrêté en cours de propagation
		public function MouseOverButLabEvent(type:String, buttonId:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_buttonId=buttonId;
		}

		override public function clone():Event
		{
			return new MouseOverButLabEvent(type, buttonId, bubbles, cancelable);
		}

		/**
		 * Retourne l'identifiant de bouton.
		 */
		public function get buttonId():String
		{
			return _buttonId;
		}

		/**
		 * @private
		 */
		public function set buttonId(value:String):void
		{
			_buttonId=value;
		}

	}
}
