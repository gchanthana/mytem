package service.widget.getdatawidget
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GetDataWidgetHandler
	{
		private var _model:GetDataWidgetModel;
		
		public function GetDataWidgetHandler(_model:  GetDataWidgetModel):void
		{
			this._model = _model;
		}	
		internal function GetDataWidgetResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateListWidget(evt.result as ArrayCollection);
			}
		}
	}
}