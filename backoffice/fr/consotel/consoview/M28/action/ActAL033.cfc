﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
  <!--- Fournit la liste des modes de récupération  [id,libelle] (ex : PULL-FTP, PUSH-MAIL ...) --->
           		   
	<cffunction name="executeAct" access="public" returntype="struct" output="false">		
		<cftry>	
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_mode_collecte" >			
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL033", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL033", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL033", 0, ThrowCfCatchError("AL033",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		
		</cftry>
	</cffunction>
</cfcomponent>