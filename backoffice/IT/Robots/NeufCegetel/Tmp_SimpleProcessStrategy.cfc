<cfcomponent displayname="NeufProcessStrategy" extends="ProcessStrategy" output="true">				

	<cfset variables.repertoire="/app/fichiers/1-recup/sfr_fixe/facture">
	<cfset variables.cookie1="">
	<cfset variables.cookie2="">
	<cfset variables.nbTentative = 10>
	<cfset variable.idInterface = 0>
	
			
 	<cffunction name="Login" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		
		<cfoutput >aller se loguer</cfoutput>
		<cfset jCookie1 = "">
		<cfset jCookie2 = "">

		<!--- A cause des Probleme de connexion a l'extranet NeufCegetel on tente de se connecter x fois' --->
		<cfloop index="i" from="1" to="30">		
			<cfhttp redirect="true"  method="get" resolveurl="true"
					url="https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="10">
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
			</cfhttp>			
			<cfif structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>
				<cfbreak>
			</cfif>
		</cfloop>		
			
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>
			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="html" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Apres 30 tentatives le robot n'a pas pu se connecté à l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfreturn jCookie1>		
		</cfif>		 
		
		<!--- Recuperer le cookie --->
		<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie1=GetToken(GetToken(chaine,1,";"),2,"=")>
		 
		<!--- Redirection vers la page suivante --->
		<cfloop index="i" from="1" to="30">	
			<cfhttp method="POST" resolveurl="true"  redirect="false"			 
					url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/"									
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="10">										
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie1)#">
					<cfhttpparam type="formfield" name="id2_hf_0" value="">
					<cfhttpparam type="formfield" name="login" value="#login#">				
					<cfhttpparam type="formfield" name="password" value="#pwd#">
					<cfhttpparam type="formfield" name="codeSecu" value="">
					<cfhttpparam type="formfield" name="valider" value="submit">
					<cfhttpparam type="url"	name="wicket:interface" value=":0:loginForm::IFormSubmitListener::">
					<cfhttpparam type='header' name='Accept' value='text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'>
					<cfhttpparam type='header' name='Accept-Language' value='fr'>
					<cfhttpparam type='header' name='Accept-Encoding' value='gzip, deflate'>
					<cfhttpparam type='header' name='Accept-Charset' value='ISO-8859-1,utf-8;q=0.7,*;q=0.7'>
					<cfhttpparam type='header' name='Keep-Alive' value='115'>
					<cfhttpparam type='header' name='Connection' value='keep-alive'>			 	
			</cfhttp>  			
			<cfif structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>
				<cfbreak>
			</cfif>
		</cfloop>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Set-Cookie")>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Apres 30 tentatives le robot n'a pas pu se connecté à l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
				
			<cfreturn jCookie2>		
		</cfif>
	
		
		<!--- Permet de récupérer le 2eme Cookie, car dans le POST un autre cookie est généré qui est différent de cookie1 ce qui a généré un problème  --->
		<cfset chaine2=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie2=GetToken(GetToken(chaine2,1,";"),2,"=")>
		
		 <!--- Redirection vers la page suivante --->
		<cfhttp method="GET" resolveurl="true"  redirect="true" url="https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/wicket:interface/../../../"
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<cfhttpparam type="url"    name="wicket:interface"  value=":1::::">
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie2)#">
				<cfhttpparam type='header' name='Accept-Language' value='fr'>
				<cfhttpparam type='header' name='Accept-Encoding' value='gzip, deflate'>
				<cfhttpparam type='header' name='Accept-Charset' value='ISO-8859-1,utf-8;q=0.7,*;q=0.7'>
				<cfhttpparam type='header' name='Keep-Alive' value='115'>
				<cfhttpparam type='header' name='Connection' value='keep-alive'>
		</cfhttp>
			
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="html" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage/wicket:interface/../../../<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset jCookie2 = "">
			<cfreturn jCookie2>		
		</cfif>
				
		<!--- Redirection vers la page suivante --->
		<cfhttp method="GET" resolveurl="true"  redirect="false"		
				url="https://extranet.sfrbusinessteam.fr/extranet/cvg/accueil"
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<cfhttpparam type='header' name='Accept' value='image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/x-ms-application, application/x-ms-xbap, application/vnd.ms-xpsdocument, application/xaml+xml, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*'>
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie2)#">
				<cfhttpparam type='header' name='te' value='deflate;q=0'> 
				<cfhttpparam type='header' name='Connection' value='keep-alive'>
				<cfhttpparam type='header' name='Accept-Encoding' value='gzip, deflate'>
				<cfhttpparam type='header' name='Content-Type' value='application/x-www-form-urlencoded'>
				<cfhttpparam type='header' name='Cache-Control' value='no-cache'>
		</cfhttp>

		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/accueil<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset jCookie2 = "">
			<cfreturn jCookie2>		
		</cfif>
		
		
		<cfreturn jCookie2>
	</cffunction>
	
	<!--- Methode pour preparer l'extranet avant d'acceder aux factures --->
	<cffunction name="PrepareExtranet" access="remote" returntype="array" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		
		<!--- Menu "Vos factures" --->
		<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(1)>
			<cfreturn aTemp>	
		</cfif>
		
		<!--- Redirection vers la page suivante --->
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/factures" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
			
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/factures<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(1)>
			<cfreturn aTemp>
		 </cfif>
		
		<!--- Menu "Vos factures fixes" --->
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/generique/codeNoeud/94.4" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/sr/generique/codeNoeud/94.4<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(1)>
			<cfreturn aTemp>
		</cfif>
		
		<!--- Lien: Export facture CSV--->
		<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type='formfield' name='code_service' value='1368999'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">					
		</cfhttp>

		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(1)>
			<cfreturn aTemp>
		</cfif>
			
		<!--- Redirection vers la page suivante --->
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308225497839" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308225497839/br>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(1)>
			<cfreturn aTemp>
		 </cfif>		
		
				
		<!--- On va chercher l'id de l'interface pour le parmètres wicket:interface  --->
		<cfset pos = findNocase('wicket:interface=',cfhttp.FileContent)>
		<cfif #pos# eq 0 >
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Déconnexion avant d'aller chercher le paramètre "idInterface".<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfset aTemp=ArrayNew(1)>
			<cfreturn aTemp>
		</cfif>
		
		<cfset chaine = mid(cfhttp.FileContent,pos,50)>			
		<cfset tab = listToArray(chaine,':')>
		<cfset idInterface = tab[3]>	
		
		
	
		<!--- Permet de detecter le type de strategie --->
		<cfset aTemp=ArrayNew(1)>
		<!--- <cfset aTemp[1]="go"> --->
					
		<cfreturn aTemp>
	</cffunction>
	
	
	<!--- Aller chercher la liste des comptes --->
	<cffunction name="ListeCompte" access="remote" returntype="array" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		
		<!--- Récupérer la liste des compte --->		
		<cfset u=ArrayNew(1)>
		<cfset v=ArrayNew(1)>
		<cfset k=ArrayNew(1)>
		<cfset pos = findNocase('Compte facturation',cfhttp.FileContent)>
		<cfif #pos# gt 0>
				<cfset chaine = mid(cfhttp.FileContent,pos,9999999)>						
				<cfset v=ListToArray(chaine,"<")>			
				<cfset j = 1>
				<cfloop from="1" to="#ArrayLen(v)#" index="i">
					<cfif Left(v[i],13) eq "option value=">
						<cfset k=ListToArray(v[i],"#chr(34)#")>
						<cfset u[j]=k[2]>
						<cfset j = j+1>
					</cfif>
				</cfloop>
		<cfelse>
				<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
							server="192.168.3.119" 
							port="26"  type="HTML" 
							subject="[Robot1 SFR Fixe]- Erreur Robot">
					
							Bonjour. <br/>
							
							Pas possible de récupérer la liste des comptes, une deconnection a du avoir lieu.<br/>
							
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
							
							Cordialement,
				</cfmail> 
		</cfif>
								
		<cfreturn u>
	</cffunction>


	<!--- Mï¿½thode se connecter à une société --->
	<cffunction name="connectSociete" access="remote" returntype="string" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
		<cfargument name="IdCompte" type="String">
		<cfoutput>compte</cfoutput>	
		<cfreturn jsession>
	</cffunction>



	
	<!--- Mï¿½thode pour aller chercher la liste des factures --->
	<cffunction name="getListeFactures" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="nbreMois" type="numeric">
		<cfargument name="shiftMois" type="numeric" default="0">
		<cfargument name="NoCompte" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="String">
			

			
		<!--- On calcul la période des factures à téléchrager --->
		<cfset datedeb=DateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"mm")>
		
		<cfif month(datedeb)neq 10 and month(datedeb)neq 11 and month(datedeb) neq 12 >
				<cfset datedeb=LsDateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"1/m/yy")>					
		<cfelse> 
				<cfset datedeb=LsDateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"1/mm/yy")>
		</cfif>
		
		
		<cfset datefin=DateFormat(DateAdd("m",-#shiftMois#,now()),"mm")>
		
		<cfif month(datefin)neq 10 and month(datefin)neq 11 and month(datefin) neq 12 >
				<cfset datefin=LsDateFormat(DateAdd("m",-#shiftMois#,now()),"1/m/yy")>					
		<cfelse> 
				<cfset datefin=LsDateFormat(DateAdd("m",-#shiftMois#,now()),"1/mm/yy")>
		</cfif>
		
		<!--- <cfif NoCompte eq "2264128002">
			<cfoutput >#cfhttp.FileContent#</cfoutput>
			<cfdump var="#cfhttp.FileContent#">
			<cfabort showerror="fin">
		</cfif> --->
		

		<cfset TabListeLienFacture = arrayNew(1)>
		<cfset TempTabListeLienFacture = arrayNew(1)>
		<cfset TempTabListeLienFacture2 = arrayNew(1)>
		
		<cfset VarAction = "">	
		<!--- On va chercher l'ensemble des parametre a envoyer dans le cfhttp'...  --->	
		<cfset pos = findNocase('<form wicket:id="form"',cfhttp.FileContent)>
		<!--- Vérifier si la balise Form (formulaire) a changé ou pas...  --->
		<cfif pos gt 0>				
				<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>
				<cfif #pos# gt 0 and #pos1# gt 0 >
					<cfset pos1 = pos1-pos>		
					<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
					<cfset tab = listToArray(chaine,'#chr(34)#')>
					<cfset Varid = tab[4]>
					<cfset VarAction= tab[8]>
				</cfif>
			<cfelse >
				<cfset pos = findNocase('form id="',cfhttp.FileContent)>
				<cfset pos1 = findNocase('name=',cfhttp.FileContent,pos)>
				<cfif #pos# gt 0 and #pos1# gt 0 >
					<cfset pos1 = pos1-pos>		
					<cfset chaine = mid(cfhttp.FileContent,pos,pos1)>
					<cfset tab = listToArray(chaine,'#chr(34)#')>	
					<cfset Varid = tab[2]>
					<cfset VarAction= tab[6]>
				</cfif>				
		</cfif>
		
		<!--- Bouton "Rechercher", Lister les factures  --->		
		<cfif #VarAction# neq "">			
				<cfloop index="i" from="1" to="5">
					 <cfhttp method="POST" redirect="false" resolveurl="true" url="#VarAction#" 
							useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600" >
							<cfhttpparam type="formfield" name="id#Varid#_hf_0" value="">
							<cfhttpparam type="formfield" name="moisDebut" value="#datedeb#">
							<cfhttpparam type="formfield" name="moisFin" value="#datefin#">
							<cfhttpparam type="formfield" name="noCf" value="#NoCompte#">
							<cfhttpparam type="formfield" name="noContrat" value="TOUS">
							<cfhttpparam type="formfield" name="noFacture" value="">											
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					</cfhttp>				
					<cfif structkeyexists(cfhttp.ResponseHeader,"Location")>
						<cfbreak>
					</cfif>
				</cfloop>		
				
				<cfif not structkeyexists(cfhttp.ResponseHeader,"Location")>			
					<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
							server="192.168.3.119" 
							port="26"  type="HTML" 
							subject="[Robot1 SFR Fixe]- Erreur Robot">
					
							Bonjour. <br/>
							
							Probleme de connexion a l'url (Liste des factures):<br/>
							https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:interface=:#idInterface#:facture:form::IFormSubmitListener::<br/>
							
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
							
							Cordialement,
					</cfmail> 
					<cfset TabListeLienFacture[1]= -1>
					<cfreturn TabListeLienFacture>
				</cfif>
			 
				 <!--- On affiche la page listant les fcatures--->
				 <cfhttp method="get" redirect="false" resolveurl="true" url="#cfhttp.ResponseHeader["Location"]#" 
							useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">												
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				 </cfhttp>
		
				 <cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
								server="192.168.3.119" 
								port="26"  type="HTML" 
								subject="[Robot1 SFR Fixe]- Erreur Robot">
						
								Bonjour. <br/>
								
								Probleme d'affichage de la page':<br/>
								https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:interface=:#idInterface#:facture:form::IFormSubmitListener::<br/><br/>
								
								Compte = #compte#<br/>
								Login = #login#<br/>
								Mot de passe = #pwd#<br/>
								
								Cordialement,
						</cfmail> 
						<cfset TabListeLienFacture[1]= -1>
						<cfreturn TabListeLienFacture>
				  </cfif>			
				
				
				
				
				 <!--- Recuperer les liens des factures de la 1ere page --->
				 <cfset TempTabListeLienFacture = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
				 <cfset TabListeLienFacture.addAll(TempTabListeLienFacture)>		
				
					
				<!--- Recuperer le lien de la derniere page listante les facture si existe--->
				<cfset LienDernierePage = ArrayNew(1)>
				<cfset LienDernierePage = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:#idInterface#:facture:factures:datatable:topToolbars:[0-9]{1,4}:toolbar:span:navigator:last:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
				<cfdump var="#LienDernierePage#">
									
				<cfif arraylen(LienDernierePage) gt 0>
					<cfset URLLastPage = LienDernierePage[1] >		
					<!--- Recuperer la derniere page listante les factures--->
					<cfhttp method="get" redirect="false" resolveurl="true" url="#URLLastPage#"
						 	useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600">																	  
							<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
							<cfhttpparam type='header' name='te' value='deflate;q=0'> 
							<cfhttpparam type='header' name='Connection' value='keep-alive'>
							<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					</cfhttp>
						
					<cfif not structkeyexists(cfhttp.ResponseHeader,"Location")>			
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
								server="192.168.3.119" 
								port="26"  type="HTML" 
								subject="[Robot1 SFR Fixe]- Erreur Robot">
						
								Bonjour. <br/>
								
								Probleme de connexion a l'url :<br/>
								#URLLastPage#<br/>
								
								Compte = #compte#<br/>
								Login = #login#<br/>
								Mot de passe = #pwd#<br/>
								
								Cordialement,
						</cfmail> 
						<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
						<cfreturn TabListeLienFacture>
					</cfif>
			 	
				 	<!--- Afficher la page recuperée --->
					<cfhttp method="get" redirect="false" resolveurl="true" url="#cfhttp.ResponseHeader['Location']#"
								useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
								<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
								<cfhttpparam type='header' name='te' value='deflate;q=0'> 
								<cfhttpparam type='header' name='Connection' value='keep-alive'>
								<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					</cfhttp>
		
					<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
							<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="HTML" 
									subject="[Robot1 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Probleme de connexion a l'url :<br/>
									#URLLastPage#<br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									
									Cordialement,
							</cfmail> 
							<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
							<cfreturn TabListeLienFacture>
					</cfif>
									 	
					
					<!--- Recuperer les liens des factures de la derniere page --->
					<cfset TempTabListeLienFacture = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
															
				    <cfset TabListeLienFacture.addAll(TempTabListeLienFacture)>
		
					<!--- Recupere le nombre de page, pour aller sur chaqu'une des pages et recuperer la liste des liens des factures --->
					<cfset arrData = ArrayNew(1)>
					<cfset ListePage = ArrayNew(1)>
		
					<cfset arrData = arrayToList(REMatch('\<span wicket\:id\=\"pageNumber\"\>[0-9]{1,3}\<\/span\>',cfhttp.FileContent)) />
					<cfset ListePage = REMatch('[0-9]{1,3}',arrData)>				
					<cfif arraylen(ListePage) eq 0>
						<cfset arrData = arrayToList(REMatch('title="Page [0-9]{1,3}"\>\<span\>',cfhttp.FileContent)) />
						<cfset ListePage = REMatch('[0-9]{1,3}',arrData)>
					</cfif>
					
					
					<cfif arraylen(ListePage) gt 0>			
							<cfset NbPage = ListePage[arraylen(ListePage)]>
							<cfoutput>#NbPage#</cfoutput>
														
							<cfset n=3>
							
							<cfloop index="n" from="3" to="#NbPage#">					
								<!--- Recuperer le lien a executer pour aller sur la page precedente--->
								<cfset TabURL = ArrayNew(1)>
								<cfset TabURL = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:(\\*|[0-9]{0,4}):facture:factures:datatable:topToolbars:(\\*|[0-9]{0,4}):toolbar:span:navigator:prev:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
								<cfset VarURL = TabURL[1]>
											
								<cfhttp method="GET" redirect="false" resolveurl="true" url="#VarURL#"																																 
										 	useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
											<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
											<cfhttpparam type='header' name='te' value='deflate;q=0'> 
											<cfhttpparam type='header' name='Connection' value='keep-alive'>
											<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
								</cfhttp>
								
								<cfif not structkeyexists(cfhttp.ResponseHeader,"Location")>			
									<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
											server="192.168.3.119" 
											port="26"  type="HTML" 
											subject="[Robot1 SFR Fixe]- Erreur Robot">
									
											Bonjour. <br/>
											
											Pas possible d'accéder à une des pages des factues (lien: page précédente)<br/>
																				
											Compte = #compte#<br/>
											Login = #login#<br/>
											Mot de passe = #pwd#<br/>
											
											Cordialement,
									</cfmail> 
									<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
									<cfreturn TabListeLienFacture>
							 	</cfif>
								
								<!--- Affichage de la page--->						
								<cfhttp method="get" redirect="false" resolveurl="true" url="#cfhttp.ResponseHeader['Location']#"
											 	useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
												<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
												<cfhttpparam type='header' name='te' value='deflate;q=0'> 
												<cfhttpparam type='header' name='Connection' value='keep-alive'>
												<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
								</cfhttp>
								
								<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
									<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
											server="192.168.3.119" 
											port="26"  type="HTML" 
											subject="[Robot1 SFR Fixe]- Erreur Robot">
									
											Bonjour. <br/>
											
											Pas possible d'accéder à une des pages des factues (lien: page précédente)<br/>
											
											Compte = #compte#<br/>
											Login = #login#<br/>
											Mot de passe = #pwd#<br/>
											
											Cordialement,
									</cfmail> 
									<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
									<cfreturn TabListeLienFacture>
								 </cfif>
								<!--- Recuperer les liens des factures de la page en cours--->	
								<cfset TempTabListeLienFacture = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/\?wicket:interface=:[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',cfhttp.FileContent) />
								<cfset TabListeLienFacture.addAll(TempTabListeLienFacture)>									
							
							</cfloop>	
					
						<cfelse>
							
							<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
									server="192.168.3.119" 
									port="26"  type="HTML" 
									subject="[Robot1 SFR Fixe]- Erreur Robot">
							
									Bonjour. <br/>
									
									Une erreur s'est produit lors de la récupération de nombre de pages listant les factures. <br/>
									
									Compte = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									
									Cordialement,
							</cfmail> 
							<cfset ArrayInsertAt(TabListeLienFacture,1,-1)>
							<cfreturn TabListeLienFacture>
					</cfif>
			
				</cfif>
		<cfelse>
				<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
							server="192.168.3.119" 
							port="26"  type="HTML" 
							subject="[Robot1 SFR Fixe]- Erreur Robot">
					
							Bonjour. <br/>
							
							Pas possible de récupérer l'url de bouton rechercher' (Liste des factures):<br/>
														
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
							
							Cordialement,
					</cfmail> 
					<cfset TabListeLienFacture[1]= -1>
					<cfreturn TabListeLienFacture>		
		</cfif>	
								
		<cfreturn TabListeLienFacture>
	</cffunction>
	
	
	<!--- Methode pour aller telecharger le fichier --->
	<cffunction name="getFile" access="remote" returntype="string" output="false">
		<cfargument name="jsession" type="String">
		<cfargument name="ElementFacture" type="string">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="compte" type="string">
		    
		     
	    <!---  Récupérer le paramètre "wicket:interface" à partir de lien de la facture  --->
	    <cfset ListeParam = ArrayNew(1)>
		<cfset ListeParam= REMatch(':[0-9]{1,4}:facture:factures:datatable:body:rows:[0-9]{1,4}:cells:[0-9]{1,4}:cell:docLinks:1:docLink:[0-9]{1,4}:ILinkListener::',#ElementFacture#)>
        		<cfset ParamURL = ListeParam[1]>
		<cfdump var="#ListeParam#">
		<cfoutput >#ParamURL#</cfoutput>
		
		 <!---  Récupérer l'url à partir de lien de la facture --->
		<cfset ListeURL = ArrayNew(1)>
		<cfset ListeURL = REMatch('https://extranet\.sfrbusinessteam\.fr:?(\\*|[0-9]{0,4})/extranet/cvg/',#ElementFacture#)/>
		<cfset URLFacture = ListeURL[1]>

		<cfset FileName = "" >
		<!--- Récupéré le nom de fichier sans le télécharger --->
		<cfloop index="i" from="1" to="5">
			<cfhttp method="get" redirect="false" resolveurl="true" url="#URLFacture#" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600">
					<cfhttpparam type="URL" name="wicket:interface" value="#ParamURL#">																	       
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
			</cfhttp>
			<cfif structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
				<cfbreak>
			</cfif>
		</cfloop>				
	
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
							server="192.168.3.119" 
							port="26"  type="HTML" 
							subject="[Robot1 SFR Fixe]- Erreur Robot">
							
							Bonjour. <br/>
							
							Probleme de connexion a l'url :<br/>
							#ElementFacture#<br/>
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
													
							Cordialement,<br/>
	
			</cfmail> 
			<cfreturn #FileName#>		
		</cfif>	
		

		<cfset u=ArrayNew(1)>
		<cfset u=ListToArray(cfhttp.ResponseHeader["Content-Disposition"],"#chr(34)#")>
		<cfset FileName=u[2]>
		<cfoutput >Nom de fichier : #FileName#</cfoutput>
		
		<!--- Vérifier si le fichier a déjà été téléchargé --->
		<cfquery datasource="ROCOFFRE" name="qListe">
			SELECT *
			FROM file_source t
			WHERE lower(t.extranet_filename)=lower('#FileName#')
			AND t.archive_path IS NOT NULL
			AND t.operateurid=534
		</cfquery>
		
		
		<cfif qListe.recordcount eq 0>

			<!--- TELECHARGEMENT --->
			<cfloop index="i" from="1" to="5">
				<cfhttp method="get" redirect="false" getasbinary="yes" resolveurl="true" url="#URLFacture#" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" timeout="600">
						<cfhttpparam type="URL" name="wicket:interface" value="#ParamURL#">																		       
						<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
						<cfhttpparam type='header' name='te' value='deflate;q=0'> 
						<cfhttpparam type='header' name='Connection' value='keep-alive'>
						<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				</cfhttp>
				<cfif structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
					<cfbreak>
				</cfif>
			</cfloop>		
			
			<cfif not structkeyexists(cfhttp.ResponseHeader,"Content-Disposition")>
				<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
								server="192.168.3.119" 
								port="26"  type="html" 
								subject="[Robot1 SFR Fixe]- Erreur Robot">
								
								Bonjour. <br/>
								
								Problème de récupération de fichier de lien suivant:<br/>
								#ElementFacture#<br/>
							
								Compte = #compte#<br/>
								Login = #login#<br/>
								Mot de passe = #pwd#<br/>
								Facture = #FileName#<br/>
		
				</cfmail> 
				<cfreturn #FileName#>		
			</cfif>
		
			
			<!--- DEPOT DU FICHIER --->
			<cfdirectory action="list" directory="#variables.repertoire#" 
					 	 recurse="false" name="qget" filter="#compte#">
			<cfif qget.recordcount eq 0>
				<cfdirectory action="create" directory="#variables.repertoire#/#compte#" mode="777">
			</cfif>
			
			<cffile mode="777" action="write" file="#variables.repertoire#/#compte#/#FileName#" output="#cfhttp.FileContent#">
						
			<cfzip action="list" file="#variables.repertoire#/#compte#/#FileName#" name="qFile"/>
			
			<cfquery datasource="ROCOFFRE" name="qListe">
				insert into
				file_source(extranet_filename,operateurid,date_recuperation,extranet_path,etat,date_mise_a_dispo,date_etat,source_filename)
				VALUES('#FileName#',534,sysdate,'#variables.repertoire#/#compte#',1,
				to_date('#lsDateFormat(qFile.DATELASTMODIFIED,"dd/mm/yyyy")#','dd/mm/yyyy'),sysdate,'#FileName#')
			</cfquery>
		
				
		</cfif>
		
		<!--- Permet de revenir à la page précédente pour récupérer les autres factureet pourvoir se connecter aux autres compte--->	
		<!--- Lien: Export facture CSV--->
		<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type='formfield' name='code_service' value='1368999'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">					
		</cfhttp>

		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLoggerV2<br/>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>						
						
						Cordialement,
			</cfmail> 
			<cfreturn #FileName# >
		</cfif>
			
		<!--- Redirection vers la page suivante --->
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308225497839" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)" >
					<cfhttpparam type='header' name='accept-encoding' value='deflate;q=0'>
					<cfhttpparam type='header' name='te' value='deflate;q=0'> 
					<cfhttpparam type='header' name='Connection' value='keep-alive'>
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
		</cfhttp>
		
		<cfif not structkeyexists(cfhttp.ResponseHeader,"Connection")>			
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" cc="samuel.divioka@consotel.fr"
						server="192.168.3.119" 
						port="26"  type="HTML" 
						subject="[Robot1 SFR Fixe]- Erreur Robot">
				
						Bonjour. <br/>
						
						Probleme de connexion a l'url :<br/>
						https://extranet.sfrbusinessteam.fr/extranet/cvg/?wicket:bookmarkablePage=:com.sfr.facture.EntiteOrganiquePage&typeAffichage=RAPPORT&1308225497839/br>
						
						Compte = #compte#<br/>
						Login = #login#<br/>
						Mot de passe = #pwd#<br/>
						
						Cordialement,
			</cfmail> 
			<cfreturn #FileName# >
		 </cfif>		
			

		<cfreturn #FileName# >
	</cffunction>
</cfcomponent>