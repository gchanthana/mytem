package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
		
	public class AffecterTermCollab implements IComportementAffectation 
	{
		public function AffecterTermCollab()
		{
		}
		public function affecter(idTerm : int, idCollab : int,libelle_term : String , libelle_Collab : String,date_effet : String, pret : int):void
		{
			if(libelle_term == null)
			{
				libelle_term = "?"; //  la procédure de LOG bug si libelle term est vide
			}
			//Alert.show("AffecterTermCollab : "+idTerm+"--"+idCollab+"--"+libelle_term+"--"+libelle_Collab+"--"+date_effet);
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"affectTermToEmp_V3",
																		affecter_handler);
			RemoteObjectUtil.callService(op,idTerm,idCollab,libelle_term,libelle_Collab,date_effet,pret);
		}
		private function affecter_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				//Alert.show("Affectation du terminal éffectuée");
				ConsoviewAlert.afficherOKImage("Affectation du terminal effectuée");
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
		
		public function annuler():void
		{
			Alert.show("fermer");
		}
	}
}