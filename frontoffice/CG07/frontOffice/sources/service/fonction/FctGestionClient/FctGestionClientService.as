package service.fonction.FctGestionClient
{
	import utils.abstract.AbstractRemoteService;

	public class FctGestionClientService extends AbstractRemoteService
	{
		private var key	:String = CvAccessManager.CURRENT_FUNCTION; 
		
		[Bindable] 
		public var myDatas		:FctGestionClientDatas;
		public var myHandlers	:FctGestionClientHandlers;
		
		
		public function FctGestionClientService()
		{
			this.myDatas 		= new FctGestionClientDatas();
			this.myHandlers 	= new FctGestionClientHandlers(myDatas);
		}
		
		public function getListeClients():void
		{
			var data:Object = new Object();
			data.idUser = CvAccessManager.getSession().USER.CLIENTACCESSID;
			doRemoting(myHandlers.getListeClientsHandler,key, 'AL034');
		}
		
		
	}
}