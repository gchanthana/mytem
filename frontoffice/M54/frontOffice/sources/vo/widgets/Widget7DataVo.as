package vo.widgets
{
	public class Widget7DataVo
	{

		private var _nom:String; 
		private var _montant:Number;
		private var _digramme :String;
		
		public function Widget7DataVo()
		{
		}
		
		/**
		 * fonction de remplissage des propriétés d'un departement à partir d'un élémént de type Object 
		 * @param param1 de type Object
		 */
		public function fill(obj:Object):void
		{
			this._nom=obj.DEPT;
			this._montant=obj.MONTANT;
			this._digramme=obj.DIGRAMME;
		}

		/**
		 * Retourne un montant de type Number.
		 */
		public function get montant():Number
		{
			return _montant;
		}
		
		/**
		 * @private
		 */
		public function set montant(value:Number):void
		{
			_montant = value;
		}
		/**
		 * Retourne un nom de departement de type String.
		 */
		public function get nom():String
		{
			return _nom;
		}
		
		/**
		 * @private
		 */
		public function set nom(value:String):void
		{
			_nom = value;
		}
		
		public function get digramme():String
		{
			return _digramme;
		}
		
		public function set digramme(value:String):void
		{
			_digramme = value;
		}
	}
}
