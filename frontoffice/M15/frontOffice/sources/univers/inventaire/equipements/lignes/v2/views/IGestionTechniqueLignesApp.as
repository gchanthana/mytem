package univers.inventaire.equipements.lignes.v2.views
{
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	
	public interface IGestionTechniqueLignesApp
	{
		function get gestionTechnique():GestionTechniqueLignesApp;
		function set gestionTechnique(gestion : GestionTechniqueLignesApp):void;
		
	}
}