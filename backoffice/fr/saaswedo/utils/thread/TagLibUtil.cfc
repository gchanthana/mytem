component extends="com.adobe.coldfusion.base"
{

    public any function cacheScriptObjects()
    {
        local.tags = ["CFFTP","CFHTTP","CFMAIL","CFPDF","CFQUERY","CFPOP","CFIMAP","CFFEED","CFLDAP","CFSTOREDPROC"];
        for(local.tag in local.tags)
        {

            local.out = getSupportedTagAttributes(local.tag);
          
        }
    }

}
