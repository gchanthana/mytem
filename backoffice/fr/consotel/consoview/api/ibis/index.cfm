<!--- =========================================================================== --->
<cfset userId="developer">
<cfset userPwd="tred78">
<cfset taskUUID=createUUID()>
<!--- =========================================================================== --->
<cfxml variable="xmlReportTask">
	<TASK>
		<!-- Chemin absolu du rapport BI Publisher -->
		<REPORT_ABSOLUTE_PATH>/CONSOTEL/Rapport Ludo/Inventaire des lignes/Inventaire des lignes/Inventaire des lignes.xdo</REPORT_ABSOLUTE_PATH>
		<!-- Nom du template du rapport -->
		<TEMPLATE_ID>Modele_DATA</TEMPLATE_ID>
		<!-- Format du fichier de sortie (NO CASE) -->
		<OUTPUT_FORMAT>html</OUTPUT_FORMAT>
		<DELIVERY>
			<!-- Nom de la tache lorsque le rapport est en planification -->
			<JOB>TEST</JOB>
			<!-- Mode de stockage du fichier de sortie (NO CASE) : FTP, LOCAL, EMAIL -->
			<CHANNEL>FTP</CHANNEL>
		</DELIVERY>
	</TASK>
</cfxml>
<!--- =========================================================================== --->
<cfset reportService=createObject("component","fr.consotel.consoview.api.ibis.publisher.service.ReportService")>
<cfset requestObject=reportService.createReportRequest(xmlReportTask,userId,userPwd)>
<!--- SEULEMENT POUR SCHEDULEREQUEST : Un CFDUMP de requestObject permet de retrouver les options ci-dessous pour le channel FTP --->
<!--- 
<cfset requestObject["deliveryRequest"]["ftpOption"]["remoteFile"]="/app/pelican/services/OBIEE/CONSOTEL/DEV/" & taskUUID & ".pdf">
<cfdump var="#reportService.getParameters(requestObject['reportRequest'])#">
<cfset setParamValueStatus=reportService.changeParameterValues(requestObject["reportRequest"],"P_SEARCH",listToArray("PEUGEOT"))>
<cfset setParamValueStatus=reportService.changeParameterValues(requestObject["reportRequest"],"P_CLIENTS_V",listToArray("-1"))>
<cfdump var="#reportService.getParameters(requestObject['reportRequest'])#">
--->

<cfset resultObject=reportService.runReport(requestObject,userId,userPwd)>
<cfcontent type="#resultObject.getReportContentType()#" variable="#resultObject.getReportBytes()#">
