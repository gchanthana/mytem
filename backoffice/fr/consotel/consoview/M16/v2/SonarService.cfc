<cfcomponent output="false" alias="fr.consotel.consoview.M16.v2.SonarService">

	<cffunction name="CreateOrder" returntype="Any" access="public">
		<cfargument name="idRacine" 		required="true" type="Numeric"/>
		<cfargument name="idCommande" 		required="true" type="Numeric"/>

		<cfif isManageMobility(arguments.idcommande) eq 0>
				<cfreturn 0>
		</cfif>

		<cfset context = {}>
		<cfset context.RacineID = arguments.idRacine>
		<cfset context.OrderID = arguments.idcommande>
		<cfset factory = new fr.saaswedo.api.order.bridge.CallFactory('CREATE_ORDER',context)>
		<cfset call = factory.createCall() >


		<cfset result = {}>
		<cfset result.MytemOrderID = 	arguments.idcommande>
		<cfset result.MytemOrderRacineID = 	arguments.idRacine>
		<cfset call.addEventListener(result) >

		<cfset msg = call.getDefinition() & '<Br/>'>

		<cfif call.execute() eq true >
			<cfset  msg = msg & call.notifySucess()>
			<cfelse>
			<cfset  msg = msg & call.notifyFailure()>
		</cfif>

		<cfmail from="commande-sonar@saaswedo.com" to="samuel.divioka@saaswedo.com"
				failto="monitoring@saaswedo.com"
				subject="[M16][SONAR] Order/Care Ticket creation status" type="text/html"
				server="mail-cv.consotel.fr" port="25">
			#msg#
			<cfdump var="#result#">
		</cfmail>

		<cfreturn 1>
	</cffunction>

	<cffunction name="CreateTicket" returntype="Any" access="public">
		<cfargument name="idRacine" 		required="true" type="Numeric"/>
		<cfargument name="idcommande" 		required="true" type="Numeric"/>

		<cfif isManageMobility(arguments.idcommande) eq 0>
				<cfreturn 0>
		</cfif>

		<cfset context = {}>
		<cfset context.RacineID = arguments.idRacine>
		<cfset context.OrderID = arguments.idcommande>
		<cfset factory = new fr.saaswedo.api.order.bridge.CallFactory('CREATE_TICKET',context)>
		<cfset call = factory.createCall() >

		<cfset result = {}>
		<cfset result.MytemOrderID = 	arguments.idcommande>
		<cfset result.MytemOrderRacineID = 	arguments.idRacine>

		<cfset call.addEventListener(result) >

		<cfset msg = call.getDefinition() & '<Br/>'>

		<cfif call.execute() eq true >
			<cfset  msg = msg & call.notifySucess()>
			<cfelse>
			<cfset  msg = msg & call.notifyFailure()>
		</cfif>

		<cfmail from="commande-sonar@saaswedo.com" to="samuel.divioka@saaswedo.com"
				failto="monitoring@saaswedo.com"
				subject="[M16][SONAR] Order/Care Ticket creation status" type="text/html"
				server="mail-cv.consotel.fr" port="25">
			#msg#
			<cfdump var="#result#">
		</cfmail>

		<cfreturn 1>
	</cffunction>


	<cffunction name = "isManageMobility"
							returnType = "numeric"
							access = "public"
							description = "Returns 1 if the distributor is ManageMobility otherwise 0">
			<cfargument name="idcommande" 		required="true" type="Numeric"/>

			<cfquery name = "qGetCommande" datasource = "ROCOFFRE" >
			    select  IDRACINE,IDREVENDEUR  from commande where idcommande = #arguments.idcommande#
			</cfquery>

			<cfif qGetCommande.recordcount gt 0 and qGetCommande['IDREVENDEUR'][1] eq 36485>
					<cfreturn 1>
			</cfif>

	  <cfreturn 0>
	</cffunction>

</cfcomponent>
