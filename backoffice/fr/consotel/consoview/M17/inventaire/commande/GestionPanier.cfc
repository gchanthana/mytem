<!--- =========================================================================
Classe: GestionPanier
Auteur: samuel.divioka
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GestionPanier" hint="" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GestionPanier" hint="Remplace le constructeur de GestionPanier.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<!--- 
		Enregistre les articles d'une commande
		retourne 1 si ok sinon retourne -1
	 --->
	<cffunction name="enregistrerPanierCommande" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="commande" required="false" type="struct" default="" displayname="struct commande" hint="Initial value for the commandeproperty." />
		<cfargument name="panier" required="false" type="array" default="" displayname="array panier" hint="Initial value for the panierproperty." />
	</cffunction>
	
	<!--- 
		Fournit la liste des articles d'une commande
	 --->
	<cffunction name="fournirArticlesCommande" access="public" returntype="any" output="false" hint="" >
		<cfargument name="idCommande" required="true" type="numeric" displayname="struct commande"/>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.FOURNIRARTICLESOPERATION" blockfactor="10" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#idCommande#">			
			<cfprocparam type="Out" cfsqltype="CF_SQL_CLOB" variable="artilces">
		</cfstoredproc>
		<cfreturn artilces>
	</cffunction>
	
	
	<cffunction name="majPrixArticleRessource" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idArticleRessource" required="true" type="numeric" displayname="id de l'article"/>
		<cfargument name="prix" required="true" type="numeric" displayname="prix"/>
		<cfargument name="bonus" required="false" type="numeric" displayname="bonus" default="0"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.MAJPRIXARTICLERESSOURCE" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idarticle_ressource" value="#idArticleRessource#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" variable="p_prix" value="#prix#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bonnus" value="#bonus#">			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	
	<cffunction name="majPrixArticleEquipement" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idArticleEquipement" required="true" type="numeric" displayname="id de l'article"/>
		<cfargument name="prix" required="true" type="numeric" displayname="prix"/>
		<cfargument name="bonus" required="false" type="numeric" displayname="bonus" default="0"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.MAJPRIXARTICLEEQUIPEMENT" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idarticle_equipement" value="#idArticleEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" variable="p_prix" value="#prix#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_bonnus" value="#bonus#">			
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="majEmployeArticle" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idArticle" required="true" type="numeric" displayname="id de l'article"/>
		<cfargument name="idEmploye" required="true" type="numeric" displayname="id de l'employe"/>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.MAJEMPLOYEARTICLES" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idarticle" value="#idArticle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idemploye" value="#idEmploye#" null="#iif((idEmploye eq 0), de("yes"), de("no"))#"/>
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	
	<cffunction name="manageLivraisonEquipements" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="lIdEquipements" required="true" type="string"/>
		<cfargument name="lNumeros" required="true" type="string"/>
		<cfargument name="lPins" required="true" type="string"/>
		<cfargument name="lPuks" required="true" type="string"/>

		<!--- 
		PKG_M17.majNumSerieEquipements(p_lst_IDEQUIPEMENT IN VARCHAR2,
   											p_lst_NUMERO		 IN VARCHAR2,
                                    p_lst_PIN			 IN VARCHAR2,
                                    p_lst_PUK			 IN VARCHAR2,
                                    p_retour				 OUT INTEGER)
		
		 --->
		 
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.MAJNUMSERIEEQUIPEMENTS" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_lst_IDEQUIPEMENT" value="#lIdEquipements#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_lst_NUMERO" value="#lNumeros#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_lst_PIN" value="#lPins#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_lst_PUK" value="#lPuks#"/>
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	
	
	<cffunction name="majArtilcesOperation" access="public" returntype="numeric" output="false" hint="" blockfactor="1">
		<cfargument name="idOperation" required="true" type="numeric"/>
		<cfargument name="articles" required="true" type="string"/>
		
		<!--- 
		PKG_M17.UPDATE_ARTICLES_OP(        P_IDOPERATION     IN INTEGER,
                                    P_IDUSER                       IN INTEGER,
                                    P_ARTICLES                     IN CLOB,
                                    P_RETOUR                       OUT INTEGER)
		 --->
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M17.UPDATE_ARTICLES_OP" blockfactor="1">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idOperation#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_iduser" value="#idGestionnaire#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" variable="p_articles" value="#tostring(articles)#"/>
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>