﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
		<!--- Crée un identifiant unique associé à un template et 
	nécessaire pour stocker les documents sur le serveur
	   --->
	<cffunction name="executeAct"  returntype="any" displayname="UID associé à un template de collecte" description="crée un identifiant unique pour le stockage des fichiers sur le serveur" hint="crée un identifiant unique pour le stockage des fichiers sur le serveur" >
		<cfargument name="data" type="struct" required="true" />
		<cfset data = structNew()>
		<cfset data.RESULT = #createUUID()#>
		<cfreturn CreateReponse("AJ020", 1, data)/>

	</cffunction>
</cfcomponent>