package univers.inventaire.equipements.gestionflottemobile.comportementdesaffectation
{
		import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	public class DesaffecterSIMofCollab implements IComportementDesaffectation
	{
		
		public function desaffecter(idCollab : int,idSIM : int,libelleCollab : String,libelleSIM : String,date_effet : String):void
		{
		 var op  : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"releaseSIMToEmp",
							desaffectationResultHandler);
		   			
			RemoteObjectUtil.callService(op,idSIM,idCollab,libelleSIM,libelleCollab,date_effet);
		}
		private function desaffectationResultHandler(re : ResultEvent):void
		{
			if (re.result){
				
				ConsoviewAlert.afficherOKImage("Equipement desaffecté");
			}		
		}
		public function annuler():void
		{
			//Alert.show("AffecterSimCollab");
		}
	}
}