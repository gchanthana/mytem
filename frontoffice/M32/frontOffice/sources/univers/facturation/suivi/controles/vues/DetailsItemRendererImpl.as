package commandemobile.itemRenderer
{
	import commandemobile.entity.Commande;
	import commandemobile.system.AbstractGestionPanier;
	import commandemobile.temp.temp;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.List;
	
	[Bindable]
	public class DetailsItemRendererImpl extends Box
	{

//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//
		
		//COMPOSANTS
		public var listConfig 			:List;
 		
		//VARIABLES LOCALES
		public var listArticles			:ArrayCollection = null;


//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//

		//CONSTRUCTEUR (>LISTNER)
		public function DetailsItemRendererImpl()
		{
			addEventListener("eraseThisConfig",			eraseConfigurationClickHandler);
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE
//--------------------------------------------------------------------------------------------//
		
		//SUPPRIME LA CONFIGURATION SELECTIONNEES
		private function eraseConfigurationClickHandler(e:Event):void
		{
			if(listArticles.length > 1)
			{
				for(var i:int = 0;i < listArticles.length;i++)
				{
					try
					{
						if(listArticles[i].IDSOUSTETE == listConfig.selectedItem.IDSOUSTETE)
						{
							listArticles.removeItemAt(i);
						}
					}
					catch(e:Error)
					{
						var bool:Boolean = false;
					}
				} 
			}
		}
		
	}
}