package gestionparc.ihm.action.mdm.actionWypeComplet
{
	import gestionparc.ihm.action.ICommand;

	public class ActionWypeCompletCommand implements ICommand
	{
		private var receiver:ActionWypeCompletReceiver=null;

		public function ActionWypeCompletCommand(rec:ActionWypeCompletReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
