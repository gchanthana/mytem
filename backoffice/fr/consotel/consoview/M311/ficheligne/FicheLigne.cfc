<cfcomponent name="FicheLigne">

	<cffunction name="getInfosPanelLigne" access="remote" returntype="array">
		
		<cfargument name="idsous_tete" type="numeric" required="true">
		
		<cfset idracine = session.perimetre.ID_GROUPE>
		<cfset idlang = session.user.IDGLOBALIZATION>

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.get_info_sous_tete_v2">
			<cfprocparam  value="#idsous_tete#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#idlang#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetInfosPanelLigne">
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="e0.pkg_m311.getOperateur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete" value="#idsous_tete#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#" null="false">
			<cfprocresult name="qGetOperateur">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = qGetInfosPanelLigne >
		<cfset p_retour[2] = qGetOperateur >
		
		<cfreturn p_retour>
		
	</cffunction>

	<cffunction name="setFonctionLigne" access="remote" returntype="any">
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfargument name="fonction" type="string" required="true">
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.updateFonctionLigne">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete" value="#idsous_tete#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_fonction" value="#fonction#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getProduit" access="remote" returntype="query">
		
		<cfargument name="idsous_tete" type="numeric" required="true">
		
		<!--- intervalle de date --->	
		<cfset idDebut = SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB>
		<cfset idFin = SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN>
		
		<cfif SESSION.PERIMETRE.SELECTED_NIVEAU EQ "IDSOUS_TETE">
			<cfset idperimetre = "#idsous_tete#">
		<cfelse>
			<cfset idperimetre = SESSION.PERIMETRE.ID_PERIMETRE>
		</cfif>
		
		<cfquery datasource="BI_TEST" name="qGetProduit">				
			set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
							p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
							p_idorga=#SESSION.PERIMETRE.SELECTED_ID_ORGA#,
							p_idperimetre=#idperimetre#,								 
							p_idcliche=#SESSION.PERIMETRE.SELECTED_ID_CLICHE#,
							p_niveau='#SESSION.PERIMETRE.SELECTED_NIVEAU#',
							p_langue_pays='#SESSION.USER.GLOBALIZATION#';
							
			
			SELECT PRODUIT1GROUPE.TYPE_THEME TYPE,
			PRODUIT1GROUPE.IDTYPE_THEME IDTYPE,
			PRODUIT1GROUPE.THEME THEME,
			PRODUIT1GROUPE.ORDRE_AFFICHAGE ORDRE_AFFICHAGE,
			PRODUIT1GROUPE.LIBELLE_PRODUIT LIBELLE_PRODUIT,
			FACTURE.NUMERO_FACTURE NUMERO_FACTURE,
			CASE WHEN PRODUIT1GROUPE.TYPE_THEME = 'Consommations' THEN 0 ELSE FACTURATION.QTE END QTE,
			FACTURATION.NOMBRE_APPEL NOMBRE_APPEL,
			FACTURATION.MONTANT_LOC MONTANT_LOC,
			UNITE_VOLUME.UNITE_VOL_MN UNITE_VOL_MN,
			CASE WHEN UNITE_VOLUME.UNITE_TYPE= 'ko' THEN ROUND(FACTURATION.VOLUME_MN/1000.0,2) ELSE FACTURATION.VOLUME_MN END VOLUME_MN,
			"parametres avances"."Ignorer Orga" IgnorerOrga
			FROM E0_ENPROD_HIST
			WHERE (PERIODE.IDPERIODE_MOIS >= #idDebut#)
			AND (PERIODE.IDPERIODE_MOIS <= #idFin#)
			AND (FACTURATION.IDSOUS_TETE = #idsous_tete#)
			ORDER BY ORDRE_AFFICHAGE, LIBELLE_PRODUIT
			
		</cfquery>
		
		<!--- <cfmail from="reply@consotel.fr" to="mohamed.sahraoui@consotel.fr" server="mail.consotel.fr" port="26" subject="dump" type="html" >
		
		</cfmail> --->
		
		<cfreturn qGetProduit>
		
	</cffunction>
	
	<cffunction name="getEvolution" access="remote" returntype="query">
		
		<cfargument name="idsous_tete" type="numeric" required="true">
		
		<!--- intervalle de date --->	
		<cfset idDebut = SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB>
		<cfset idFin = SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN>
		
		<cfif SESSION.PERIMETRE.SELECTED_NIVEAU EQ "IDSOUS_TETE">
			<cfset idperimetre = "#idsous_tete#">
		<cfelse>
			<cfset idperimetre = SESSION.PERIMETRE.ID_PERIMETRE>
		</cfif>
		
		<cfquery datasource="BI_TEST" name="qGetEvolution">				
			set variable 	p_idracine_master=#SESSION.PERIMETRE.IDRACINE_MASTER#,
							p_idracine=#SESSION.PERIMETRE.ID_GROUPE#,
							p_idorga=#SESSION.PERIMETRE.SELECTED_ID_ORGA#,
							p_idperimetre=#idperimetre#,								 
							p_idcliche=#SESSION.PERIMETRE.SELECTED_ID_CLICHE#,
							p_niveau='#SESSION.PERIMETRE.SELECTED_NIVEAU#',
							p_langue_pays='#SESSION.USER.GLOBALIZATION#';
			
			
			
			SELECT PRODUIT1GROUPE.TYPE_THEME TYPE,
			PRODUIT1GROUPE.THEME THEME,
			PRODUIT1GROUPE.IDTYPE_THEME IDTYPE,
			PRODUIT1GROUPE.ORDRE_AFFICHAGE ORDRE_AFFICHAGE,
			PRODUIT1GROUPE.IDTHEME_PRODUIT PRODUIT,
			PERIODE.IDPERIODE_MOIS IDPERIODE,
			PERIODE.LIBELLE_PERIODE PERIODE,
			FACTURATION.MONTANT_LOC MONTANT,
			UNITE_VOLUME.UNITE_VOL_MN UNITE_VOL_MN,
			FACTURATION.VOLUME_MN VOLUME_MN,
			FACTURATION.QTE QTE,
			"parametres avances"."Ignorer Orga" IgnorerOrga,
			"parametres avances"."Densifier Periode" DensifierPeriode
			FROM E0_ENPROD_HIST
			WHERE (PERIODE.IDPERIODE_MOIS >= #idFin#-5)
			AND (PERIODE.IDPERIODE_MOIS <= #idFin#)
			AND (FACTURATION.IDSOUS_TETE = #idsous_tete#)
			ORDER BY ORDRE_AFFICHAGE, PRODUIT,IDPERIODE
			
		</cfquery>
		
		<cfreturn qGetEvolution>
		
	</cffunction>

	<cffunction name="getTicket" access="remote" returntype="query">
		
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfargument name="idinventaire_periode" type="numeric" required="true">
		
		<cfargument name="ORDER_BY_TEXTE_COLONNE" type="string" required="true">
		<cfargument name="ORDER_BY_TEXTE" type="string" required="true">
		
		<cfargument name="SEARCH_TEXT_COLONNE" type="string" required="true">
	    <cfargument name="SEARCH_TEXT" type="string" required="true">
		
	    <cfargument name="OFFSET" type="numeric" required="true">
	    <cfargument name="LIMIT" type="numeric" required="true">
		
		<cfset idracine_master = session.perimetre.idracine_master>
		<cfset _lang = session.user.globalization >		
	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="CDR.PKG_M311.GET_ALL_TICKET_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#idracine_master#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#idinventaire_periode#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_idsous_tete" value="#idsous_tete#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE_COLONNE" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE" value="#ORDER_BY_TEXTE#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT_COLONNE" value="#SEARCH_TEXT_COLONNE#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT" value="#SEARCH_TEXT#" null="false">        
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_OFFSET" value="#OFFSET#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_LIMIT" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LANGUE" value="#_lang#" null="false">			
		    <cfprocresult name="p_result">
	    </cfstoredproc>
		
		<cfreturn p_result>
		
	</cffunction>

</cfcomponent>