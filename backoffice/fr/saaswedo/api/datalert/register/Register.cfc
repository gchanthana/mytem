<cfcomponent output="false" hint=" pour enregistrer les devices IOS et android" extends="fr.saaswedo.api.datalert.Core">
	
	<cffunction access="private" name="URl_PLAN" returntype="String">
		<cfset url_plan="#url_server#/api/da/plan.cfm">
		<cfreturn  url_plan>
	</cffunction>
	
	<cffunction name="init" access="private" output="true">
	
		<cfargument name="httpRequestData" 	type="String" required="true">

		<cfset uuid =createuuid() >				
		<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
		<cfset jsonRequest=deserializejson(httpRequestData)>
		<cfset serverName = URl_PLAN()>	
					
		<!---  pour android  --->
		<cfset tokenId="">
		<cfset homeMCCMNC ="" >
		<cfset confirmNum = "" >
		
		<cfif structKeyExists(#jsonRequest#,"phoneNum") and jsonRequest.phoneNum neq "">
			<!--- si le phone_no incorrect l'API SWD renvoi {"Message":"Billing Plan not found"}  --->
			<cfset phoneNum = verifierNumPhone(#jsonRequest.phoneNum#)><!---  tester si le num phone commence par +  --->
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"devId") and jsonRequest.devId neq "">
			<cfset  devId = #jsonRequest.devId# >
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"tokenId") and jsonRequest.tokenId neq "">
			<cfset tokenId = jsonRequest.tokenId> 
		</cfif>
		 
		<cfif structKeyExists(#jsonRequest#,"mccmnc") and jsonRequest.mccmnc neq "">
			<cfset homeMCCMNC = Replace(#jsonRequest.mccmnc#,"-","","ALL")>
		</cfif>
		
		<cflog  text="initialisation variables pour enregistrer le telephone  est OK " type="information">
		
	</cffunction>
	
	<!---  enregistrer iphone --->
	<cffunction name="register_ios" access="public" returntype="any" output="true">
			
		<cfargument name="httpRequestData" type="String" required="true">
		<cfargument name="user_agent" type="String" required="true">
		<cfargument name="ip_origin" type="String" required="false">
			
		<cfset init(httpRequestData)>
		
		<!--- logger les données reçues --->
		<cfset saveLog(uuid,"#Debut# register_ios","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin#",idMntMobileProxy) >
		<cfset resReq=runRequest(user_agent,ip_origin,"register_ios")>
		<cfreturn resReq>
		
	</cffunction>
	
	<!---  enregister android --->	
	<cffunction name="register_android" access="public" returntype="any" output="true">
			
		<cfargument name="httpRequestData" type="String" required="true">
		<cfargument name="user_agent" type="String" required="true">
		<cfargument name="ip_origin" type="String" required="true">
		
		<cfset init(httpRequestData)>
		
		<!--- logger les données reçues --->
		<cfset saveLog(uuid,"#Debut# register_android","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin#",idMntMobileProxy) >
		<cfset resReq=runRequest(user_agent,ip_origin,"register_android")>
		<cfreturn resReq>
	
	</cffunction>
	
	<!---  permet d'executer la requete clients ' --->
	
	<cffunction name="runRequest" access="private" returntype="string">
		
		<cfargument name="user_agent" 		type="String" required="true">
		<cfargument name="ip_origin" 		type="String" required="true">
		<cfargument name="methode" 			type="String" required="true">
		
		<cftry>
			
			<cfif isDefined("devId") and devId neq "" >  
				
				<cfhttp method="post" url="#serverName#" useragent="#user_agent#">
					<cfhttpparam type="header" name="Authorization" value="#authorization#"><!--- Authentication basique obligatoire sinon erreur  --->
					<cfhttpparam type="header" name="Accept" value="application/json">
	 				<cfhttpparam type="formfield" name="phone_no" value="#phoneNum#"> 	<!--- <cfhttpparam type="formfield" name="phone_no" value=""> : {"Message":"Invalid phone_no"}  --->
				</cfhttp>
			
				<!---  logger les données envoyées à l'API SWD  --->	
				<cfset saveLog(uuid,"#invoke# #methode#","#lab_sentData# le phone_no = #phoneNum# comme cfhttpparam #newLine# #link# #serverName# ",idMntEventProxyAPI) >
		
				<cfif cfhttp.Responseheader.Status_Code eq "200"> <!--- si  plan existe on envoie un sms au tel   --->
			
					<cflog type="information" text="le plan existe, on envoie un sms au telephone">
				
					<!--- logger la réponse de l'API SWD  --->
					<cfset saveLog(uuid,"#responseSWD# #methode#","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName#",idMntEventAPIProxy) >
					
					<cfset confirmNum=getConfirmNum ()> <!--- générer un random  --->
					<cfset res=bdd.save(1,devId,confirmNum,phoneNum)>
									
 					<cfset isSendOK = sendSms(phoneNum,confirmNum)>
					
					<cfif isSendOK eq true>
						<cflog type="information" text="envoi SMS ok, on renvoie le random au telephone avec resp : ok ">
						<cfset responeJson='{"resp":"OK","cfmNum":"#confirmNum#"}'>
						<cfelse>
							<cfheader name="status" value="421">
							<cflog type="information" text="Impossible d'envoyer le SMS au telephone, on renvoie 421 au device">
							<cfset responeJson='{"resp":"Cannot send SMS to #phoneNum#"}'>	
					</cfif>
					<!---  logger la réponse envoyée au mobile --->
					<cfset saveLog(uuid,"#responseProxy# #methode#","#respHttp# #responeJson#",idMntEventProxyMobile) >
					<cfreturn responeJson>		
			
	 			<cfelse> <!--- plan n'existe pas et la ligne n'existe pas dans mytem  --->
					<cfset responeJson = deserializeJson(cfhttp.Filecontent) > 	<!---  convert json into struct with deserializeJson  --->
					<cfheader name="status" value="404">
					<cfset responeJson.cfmNum=0> <!--- Device non trouve dans base SWD, on renvoie un 404 au device --->
					<cflog type="information" text="plan non trouve dans base SWD, en renvoie 404 - register_ios">
					<!--- logger le message de l'API SWD en cas d'erreur, par exemple :  {"Message":"phone_no is not present"} et le meme message est renvoyé au mobile ---> 
					<cfset saveLog(uuid,"#responseSWD# #methode#","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName# #newLine# #info# en plus de cfmNum = 0",idMntEventAPIProxy) >
					<cfreturn serializeJson(responeJson)>
				</cfif>
			<cfelse>
				<cfset responeJson = '{"resp":"Variable devId is undefined."}'>
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# #methode#","#respHttp# #responeJson#",idMntEventProxyMobile) >
				<cfreturn responeJson>
			</cfif>
		<cfcatch type="any"> <!---  logger l'exception --->
		
			<cfset reponseHTTP=isDefined("cfhttp") ? cfhttp:{msg="N.D"}>
			<cfsavecontent variable="contenuHTTP">
				<cfoutput>#serverName#<hr></cfoutput>
				<cfdump var="#reponseHTTP#" format="text">
			</cfsavecontent>
			<cfset body=bodyException(CFCATCH)>
			<cfset body=body & "<hr>" & contenuHTTP & "<hr>">
			<cfset saveLog(uuid,"#exce# #methode#",body,idMntEventProxyException) >
			<cfset responeJson = '{"resp":"#cfcatch.Message#" }' >
			<cfreturn responeJson>
			
		</cfcatch>
		</cftry>
       		 
	</cffunction>
	
	<!--- envoyer un SMS --->	
	<cffunction name="sendSms" access="private" output="true" returntype="boolean">
		
		<cfargument name="phoneNumber" type="any" required="true">
		<cfargument name="confirmNum" type="any" required="true">
				
		<cfset	body="From Saaswedo#NewLine#Verification Number : #confirmNum#" >
		
		<cfset newApiSmsPublic = createObject("component","fr.consotel.api.sms.public.ApiSmsPublic") >
		<cfset uuidSMS=newApiSmsPublic.createCampagne() >
		<cfset operator=newApiSmsPublic.SFR_OPERATOR()>
			
		<cfif  IsDefined ("uuidSMS") and uuidSMS neq "" >  
			<cfset returnSend=newApiSmsPublic.sendMessageToSingleNumber(phoneNumber,body,operator,uuidSMS)>
			<cfif returnSend eq true>
				<cfreturn true>
			<cfelse>
				<!--- envoyer avec twillio --->
				<cfset uuidSMS=newApiSmsPublic.createCampagne() >
				<cfset operator=newApiSmsPublic.TWILIO_OPERATOR()>
				<cfif  IsDefined ("uuidSMS") and uuidSMS neq "" >  
					<cfset returnSend=newApiSmsPublic.sendMessageToSingleNumber(phoneNumber,body,operator,uuidSMS)>
					<cfif returnSend eq true>
						<cfreturn true>
					<cfelse>
						<cfreturn false>
					</cfif>
				</cfif>
			</cfif> 
		</cfif>
	
	</cffunction>
	
	<!---  générer un random --->	
	<cffunction name="getConfirmNum" returntype="numeric" access="private" output="true">
		<cfset myNumber = RandRange(100000, 999999)>
		<cfreturn myNumber>
	</cffunction>
	
	<!--- verifier num phone  --->
	<cffunction name="verifierNumPhone" returntype="any" access="private" output="true">
		
		<cfargument name="phoneNum" type="any" required="true">
		
		<cfset firstChar=Left(#phoneNum#, 1)>
		<cfif firstChar neq '+'>
			<cfif firstChar eq " ">
				<cfset phoneNum=RemoveChars(#phoneNum#, 1, 1)>
				<cfset phoneNum="+"&#phoneNum#>
				<cfelse>
					<cfset phoneNum="+"&#phoneNum#>					
			</cfif>
		</cfif>
		<cfreturn phoneNum >
	</cffunction>
		
</cfcomponent>