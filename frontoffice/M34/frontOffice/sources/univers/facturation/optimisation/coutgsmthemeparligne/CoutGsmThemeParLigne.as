package univers.facturation.optimisation.coutgsmthemeparligne {
	import univers.facturation.optimisation.AbstractReport;
	import mx.events.FlexEvent;
	import flash.net.URLRequest;
	import mx.collections.ArrayCollection;
	import univers.facturation.optimisation.deploiement.Deploiement;
	
	public class CoutGsmThemeParLigne extends CoutGsmThemeParLigneIHM {
		private var periodiciteArray:ArrayCollection =
				new ArrayCollection([{label: Deploiement.PERIODICITE_MENSUEL_LABEL, value: Deploiement.PERIODICITE_MENSUEL},
							{label: Deploiement.PERIODICITE_BIMESTRIEL_LABEL, value: Deploiement.PERIODICITE_BIMESTRIEL}]);
		
		public function CoutGsmThemeParLigne() {
			super();
			reportType = "CoutGsmThemeParLigne";
			selectorThumbCount = 1; // 1 Mois à choisir
			formatList = new Array(1);
			var xlsObj:Object = new Object();
			xlsObj.label = AbstractReport.XLS_FORMAT_LABEL;
			xlsObj.value = AbstractReport.XLS_FORMAT_VALUE;
			xlsObj.enabled = true;
			formatList[0] = xlsObj;
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected override function initIHM(event:FlexEvent):void {
			super.initIHM(event);
			cmbPeriodicite.dataProvider = periodiciteArray;
		}
		
		public override function getReportName():String {
			return "Extract GSM par Thème";
		}
		
		protected override function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			super.getReportUrlRequest(secureBool);
			urlRequestPath.data.PERIODICITE = cmbPeriodicite.selectedItem.value;
			return urlRequestPath;
		}
	}
}
