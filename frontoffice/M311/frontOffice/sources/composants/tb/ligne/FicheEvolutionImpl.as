package composants.tb.ligne
{
	import mx.resources.ResourceManager;
	import composants.tb.controls.TextInputLabeled;
	import composants.tb.ligne.entity.FicheEvolutionVO;
	import composants.tb.ligne.entity.FiltreVO;
	import composants.tb.ligne.event.FicheEvent;
	import composants.tb.ligne.services.FicheEvolutionService;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import mx.charts.ColumnChart;
	import mx.charts.HitData;
	import mx.charts.series.ColumnSeries;
	import mx.charts.series.items.ColumnSeriesItem;
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.graphics.RoundedRectangle;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class FicheEvolutionImpl extends VBox
	{
		
		private var _ligne:Number;
		private var _ficheEvolution:ArrayCollection;
		private var _filtre:FiltreVO;
		private var _total:ArrayCollection;
		
		private var periodeDebut:Number=0;
		private var periodeFin:Number=0;
		
		public var array_mois:Array = new Array();
		public var array_evolution:Array = new Array();
		public var evolution:Object = new Object();
		public var array_montant:ArrayCollection = new ArrayCollection();
		public var array_volumeMN:ArrayCollection = new ArrayCollection();
		public var array_volumeKO:ArrayCollection = new ArrayCollection();
		public var array_volumeUnite:ArrayCollection = new ArrayCollection();
		public var array_volumeAUTRE:ArrayCollection = new ArrayCollection();
		
		public var arrayGraphe:ArrayCollection = new ArrayCollection();
		
		public var tabColonne:Array = [];
		public var tabHColonne:Array = [];
		
		public var myHeaderGrid:DataGrid;
		public var dgEvolutionData:DataGrid;
		public var myFooterGrid:DataGrid;
		public var txtFiltreInventaire:TextInputLabeled;
		public var abo:CheckBox;
		public var conso:CheckBox;
		public var myChart:ColumnChart;
		
		public var colonnes:Array = new Array();
		
		public var arrayTab:ArrayCollection = new ArrayCollection();
		
		private var serviceFicheEvolution : FicheEvolutionService = new FicheEvolutionService();
		
		public function FicheEvolutionImpl()
		{
		}

		public function get total():ArrayCollection
		{
			return _total;
		}

		public function set total(value:ArrayCollection):void
		{
			_total = value;
		}

		public function get filtre():FiltreVO
		{
			return _filtre;
		}

		public function set filtre(value:FiltreVO):void
		{
			_filtre = value;
		}

		public function get ficheEvolution():ArrayCollection
		{
			return _ficheEvolution;
		}

		public function set ficheEvolution(value:ArrayCollection):void
		{
			_ficheEvolution = value;
		}

		public function get ligne():Number
		{
			return _ligne;
		}

		public function set ligne(value:Number):void
		{
			_ligne = value;
		}
		
		public function init():void
		{
			serviceFicheEvolution.getFicheEvolution(ligne);
			serviceFicheEvolution.model.addEventListener(FicheEvent.FICHEEVOLUTION,ficheEvolutionHandler);
		}
		
		public function ficheEvolutionHandler(ev:FicheEvent):void
		{
			
			filtre = new FiltreVO();
			
			ficheEvolution=serviceFicheEvolution.model.ficheEvolution;
			getPeriode();
			getArrayMois();
			getArrayEvolution();
			getEvolution();
			
			
			//tableau de colonnes du dgEvolutionData
			var theme:DataGridColumn=new DataGridColumn(ResourceManager.getInstance().getString('M311', 'Th_me'));
			theme.dataField = "theme";
			theme.showDataTips = true;
			theme.dataTipField = "theme";
			theme.width = 110;
			theme.resizable = false;
			tabColonne.push(theme);
			
			var themeH:DataGridColumn=new DataGridColumn(ResourceManager.getInstance().getString('M311', 'Th_me'));
			themeH.width = 110;
			themeH.resizable = false;
			tabHColonne.push(themeH);
				
			var t:DataGridColumn;
			
			for(var i:Number=0;i<array_mois.length;i++)
			{
				t = new DataGridColumn(array_mois[i].PERIODE);
				t.dataField = array_mois[i].PERIODE;
				t.headerText = array_mois[i].PERIODE;
				(i==array_mois.length-1) ? t.resizable = true : t.resizable = false;
				t.width = 110;
				t.labelFunction = formateNumber;
				t.setStyle("textAlign","right");
				
				tabColonne.push(t);
			}
							
			arrayTab = evolution.montant;
			
			getTotal(true);
			
			getGraphe();
			
			dgEvolutionData.columns = tabColonne;
			myFooterGrid.columns = tabColonne;
			dgEvolutionData.dataProvider = arrayTab;
			myFooterGrid.dataProvider = total;
						
		}
		
		public function filtreEvolution(event:Event):void
		{
			filtre.text = txtFiltreInventaire.text;
			filtre.abonnement = (abo.selected)? 1 : 0;
			filtre.consommation = (conso.selected)? 1 : 0;
			filtreEvolutionFactureChangeHandler(event);
		}
		
		private function filtreEvolutionFactureChangeHandler(event:Event):void
		{
			if(arrayTab != null){
				(arrayTab as ArrayCollection).filterFunction = getFiltre;
				(arrayTab as ArrayCollection).refresh();
				getTotal(false);
				getGraphe();
			}
		}
		
		private function getFiltre(produit:Object):Boolean
		{
			var result:Boolean = ((txtFiltre(produit))&&(typeFiltre(produit)));
			
			return result;
		}
		
		private function txtFiltre(produit:Object):Boolean
		{
			
			if (
				((produit.theme) && (produit.theme.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1))
			)
				return true;
			else
				return false;
			
		}
				
		private function typeFiltre(produit:Object):Boolean
		{
			
			if (
				((filtre.abonnement==1) && (produit.idtype == 1))
				||
				((filtre.consommation==1) && (produit.idtype == 2))
			)
				return true;
			else
				return false;
			
		}
		
		private function getTotal(value:Boolean):void
		{
			
			total = new ArrayCollection();
			
			var tab:ArrayCollection = new ArrayCollection();
			
			if(value){tab=(arrayTab as ArrayCollection)}else{tab=(dgEvolutionData.dataProvider as ArrayCollection);}
			
			total.addItem(new Object());
			
			total[0]['theme'] = 'Total';
			
			for(var i:Number=0;i<array_mois.length;i++)
			{
				total[0][array_mois[i].PERIODE] = 0;
			}
			
			for(i=0;i<array_mois.length;i++){
				for(var j:Number=0;j<tab.length;j++)
				{
					total[0][array_mois[i].PERIODE] += tab[j][array_mois[i].PERIODE];
				}
			}
			
		}
		
		private function getPeriode():void
		{
			if(ficheEvolution.length>0){
				periodeDebut=ficheEvolution[0].IDPERIODE;
				periodeFin=ficheEvolution[0].IDPERIODE;
				for(var i:int=0;i<ficheEvolution.length;i++)
				{
					periodeMinMax((ficheEvolution[i] as FicheEvolutionVO).IDPERIODE);
				}
			}
		}
		
		private function periodeMinMax(value:Number):void
		{
			if(value<periodeDebut){periodeDebut=value}
			if(value>periodeFin){periodeFin=value}
		}
		
		private function getArrayMois():void
		{
			var i:Number=0;
			for(var N:Number=periodeDebut;N<periodeFin+1;N++)
			{
				array_mois[i] = new Object();
				array_mois[i].IDPERIODE = N;
				array_mois[i].PERIODE = "";
				searchPeriode(i);
				i++;
			}
		}
		
		private function searchPeriode(i:Number):void
		{
			for(var N:Number=0;(array_mois[i].PERIODE == "")&&(N < ficheEvolution.length);N++)
			{
				if(ficheEvolution[N].IDPERIODE==array_mois[i].IDPERIODE){array_mois[i].PERIODE = ficheEvolution[N].PERIODE;}
			}
		}
		
		private function getArrayEvolution():void
		{
			var j:Number=0;
			for(var i:Number=0;i<ficheEvolution.length;i++)
			{
				if(ficheEvolution[i].THEME!=''){
					array_evolution[j] = new Object();
					array_evolution[j].theme = ficheEvolution[i].THEME;
					array_evolution[j].type = ficheEvolution[i].TYPE;
					array_evolution[j].idtype = ficheEvolution[i].IDTYPE;

					while((i<ficheEvolution.length)&&(array_evolution[j].theme == ficheEvolution[i].THEME))
					{
						for(var z1:Number=0;z1<array_mois.length;z1++)
						{
							array_evolution[j]["Vmn"+array_mois[z1].IDPERIODE]=0;
							array_evolution[j]["Vko"+array_mois[z1].IDPERIODE]=0;
							array_evolution[j]["Vunite"+array_mois[z1].IDPERIODE]=0;
							array_evolution[j]["Vautre"+array_mois[z1].IDPERIODE]=0;
							array_evolution[j]["M"+array_mois[z1].IDPERIODE]=0;
						}
						for(var z:Number=0;(i<ficheEvolution.length)&&(z<array_mois.length);z++)
						{
							if(array_mois[z].IDPERIODE==ficheEvolution[i].IDPERIODE){
								while((i<ficheEvolution.length)&&(array_mois[z].IDPERIODE==ficheEvolution[i].IDPERIODE)&&(array_evolution[j].theme == ficheEvolution[i].THEME))
								{
									switch(ficheEvolution[i].UNITE)
									{
										case 'MN':
											array_evolution[j]["Vmn"+array_mois[z].IDPERIODE] += parseInt(ficheEvolution[i].VOLUME);
											break;
										case 'KO':
											array_evolution[j]["Vko"+array_mois[z].IDPERIODE] += parseInt(ficheEvolution[i].VOLUME);
											break;
										case 'Unite':
											array_evolution[j]["Vunite"+array_mois[z].IDPERIODE] += parseInt(ficheEvolution[i].VOLUME);
											break;
										case '<vide>':
											array_evolution[j]["Vautre"+array_mois[z].IDPERIODE] += parseInt(ficheEvolution[i].VOLUME);
											break;
									}
									array_evolution[j]["M"+array_mois[z].IDPERIODE] += ficheEvolution[i].MONTANT;
									i++;
								}
							}
						}
					}
					j++;
					i--;
				}
			}
		}
		
		
		private function getArrayAffiche(value:String):ArrayCollection
		{
			
			var varray:ArrayCollection = new ArrayCollection();
			var t:Object;
			
			for(var i:Number=0;i<array_evolution.length;i++)
			{
				t =  new Object();
				t.theme = array_evolution[i].theme;
				t.type = array_evolution[i].type;
				t.idtype = array_evolution[i].idtype;
				for(var j:Number=0;j<array_mois.length;j++)
				{
					t[array_mois[j].PERIODE]= array_evolution[i][value+array_mois[j].IDPERIODE.toString()];
				}
				varray.addItem(t);
			}
			
			return varray;
		}
		
		private function getEvolution():void
		{
			array_montant = getArrayAffiche('M');
			array_volumeMN = getArrayAffiche('Vmn');
			array_volumeKO = getArrayAffiche('Vko');
			array_volumeUnite = getArrayAffiche('Vunite');
			array_volumeAUTRE = getArrayAffiche('Vautre');
			
			evolution.montant = new ArrayCollection();
			evolution.volumeMN = new ArrayCollection();
			evolution.volumeKO = new ArrayCollection();
			evolution.volumeUnite = new ArrayCollection();
			evolution.volumeAUTRE = new ArrayCollection();
			
			evolution.montant = array_montant;
			evolution.volumeMN = array_volumeMN;
			evolution.volumeKO = array_volumeKO;
			evolution.volumeUnite = array_volumeUnite;
			evolution.volumeAUTRE = array_volumeAUTRE;
		}
		
		private function getGraphe():void
		{
						
			var tab:Array = new Array();
			var tablo:Array = new Array();
						
			tab = arrayTab.toArray();
			
			for(var i:Number=0;i<array_mois.length;i++){
				tablo[i] = new Object();
				tablo[i].mois = array_mois[i].PERIODE;
				for(var j:Number=0;j<tab.length;j++)
				{
					tablo[i][tab[j].theme] =  tab[j][array_mois[i].PERIODE];
				}
			}
			
			arrayGraphe = new ArrayCollection(tablo);
			
		
			if(tab.length!=0){
				
				var t:ColumnSeries;
				colonnes = new Array();
			
				for(j=0;j<tab.length;j++)
				{
					t =  new ColumnSeries();
					t.xField = "mois";
					t.yField = tab[j].theme;
					t.displayName = tab[j].theme;
					colonnes.push(t);	
				}
				
			}
			
		}
		
		public function getTypeEvolution(event:Event):void
		{
			var value:Number;
			value=event.currentTarget.selectedValue;
			switch(value)
			{
				case 1:
					arrayTab = evolution.montant;
					break;
				case 2:
					arrayTab = evolution.volumeMN;
					break;
				case 3:
					arrayTab = evolution.volumeKO;
					break;
				case 4:
					arrayTab = evolution.volumeUnite;
					break;
				case 5:
					arrayTab = evolution.volumeAUTRE;
					break;
			}
			filtreEvolution(event);
			getGraphe();
		}
		
		protected function formateNumber(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
			else
				return "-";
		}
		
		protected function formatDataTip(hitData:HitData):String
		{
			if(hitData)
			{
				try
				{
					
						var itemsDictionary:Dictionary = new Dictionary();
						var total:Number = 0;
						
						//hitData.item holds name value pairs for each of the
						//dataProvider's rows(sales array in this case)
						for(var property:Object in hitData.item){
							itemsDictionary[property] = hitData.item[property];
						}
						
						// Have to get instance of hitData.element (a ColumnSeries) to
						// access element.series.stacker.series which holds the yField
						// values e.g. toys. Note: needed as hitData.element.
						// series.stacker is not accessible directly.
						var series:ColumnSeries = ColumnSeries(hitData.element);
						for( var key:Object in itemsDictionary)
						{
							if(series.stacker != null)
							{
								for( var i:int = 0; i < series.stacker.series.length; i++){
									// Need to check that we only add yField values, as "period-Q1"
									// is also in hitData.item along with "toys-900" etc
									if(series.stacker.series[i].yField == key.toString()){
										total += itemsDictionary[key];
									}
								}
							}
						}
						
						var item:ColumnSeriesItem = ColumnSeriesItem(hitData.chartItem);
						var quarter:Object = item.xValue;
						var value:Number = Number(item.yValue) - Number(item.minValue);
						// want 2 decimal place precision
						var percent:Number = Math.round(value / total * 1000) / 10;
						
						return  "<b>" + series.displayName + "</b>\n" +
							"" + quarter + 
							"\n" + ConsoviewFormatter.formatNumber(Number(value),2) + 
							" (" + percent + "%) \n" +
							ResourceManager.getInstance().getString('M311', '_b_Total____b_') + ConsoviewFormatter.formatNumber(Number(total),2);
					

				}
				catch(e:Error)
				{
					trace(e.getStackTrace())
					return "";
				}
			}
			else
			{
				return "";
			}
			return "";
		}	
								
	}
}