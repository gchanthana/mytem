package general.strategy
{
	import flash.display.DisplayObject;
	import flash.utils.getDefinitionByName;
	
	import mx.containers.VBox;
	
	import appli.app.AppConsoleGestion;
	import appli.app.AppMYTEM360;
	import appli.app.AppMytem;
	import appli.app.AppPilotage;
	
	import entity.AppParams;
	
	import general.entity.Appli;
	import general.entity.Constantes;
	
	import interfaceClass.IApplication;
	
	public class StrategyApplication
	{
/* ------------------------ CONSTANTES / VARIABLES -------------------------------- */
	// CONSTANTE	
		/**
		 * <code>listeApplication</code>
		 * 
		 * Array qui contient l'ensemble des applications sous la forme d'objet <code>Appli</code>
		 * 
		 * A chaque ajout d'une application, il faut :
		 * 		- ajouter un objet <code>Appli</code> à ce tableau 
		 * 		- importer la classe. Exemple : import appli."codeApp".App"appName"
		 * 		- créer une référence dans le constructeur
		 * @see general.entity.Appli
		 * @see appli.1.appCV4
		 * @see appli.51.appPilotage
		 * @see appli.101.appMYTEM360
		 */
		private const listeApplication:Array = [new Appli(Constantes.CODEAPP_CV4, "Mytem"),
												new Appli(Constantes.CODEAPP_PILOTAGE, "Pilotage"),
												new Appli(Constantes.CODEAPP_MYTEM360, "MYTEM360"),
												new Appli(Constantes.CODEAPP_CONSOLEGESTION,"ConsoleGestion")];
	// VARIABLE
		// PUBLIC
		private var _params:AppParams;
		
/* ------------------------ CONSTRUCTEUR ------------------------------------------ */				
		/**
		 * <code>StrategyApplication</code>
		 * 
		 * Constructeur de la classe 
		 * 
		 * on créé un lien vers les classes pouvant être créées dynamiquement dans le execute ( strategy pattern )
		 */
		public function StrategyApplication()
		{
			AppMytem;
			AppMYTEM360;
			AppPilotage;
			AppConsoleGestion;
		}
/* ------------------------ FONCTION PUBLIC --------------------------------------- */		
		/**
		 * <code>execute</code>
		 * @param codeApp Entier qui représente le code de l'application
		 * @return IStrategyApplication 
		 *  
		 * retourne la classe qui permet de crèer l'application
		 * Implémente la pattern strategy
		 */
		public function execute(param:AppParams):DisplayObject
		{
			trace("StrategyApplication.execute(codeApp)");
			
			this._params = param;
			
			if(_params.codeApp > 0)
			{
				for each (var app:Appli in listeApplication) 
				{
					if(_params.codeApp == app.codeApp)
					{
						var pathClass:String = "appli.app.App"+app.appName;
						var ClassReference:Class = getDefinitionByName(pathClass) as Class;
						var myClass:IApplication = new ClassReference();
						myClass.init(this._params);
						return myClass as DisplayObject;
						break;
					}
				}
			}
			return new VBox();
		}
	}
}