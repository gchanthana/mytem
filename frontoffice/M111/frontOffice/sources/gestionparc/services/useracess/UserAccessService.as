package gestionparc.services.useracess
{
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;


	public class UserAccessService
	{	
		private var _model:UserAccessModel;
		public var handler:UserAccessHandler;
		private var remote:RemoteObject;
				
		public function UserAccessService()
		{
			_model = new UserAccessModel();
			handler = new UserAccessHandler(model);
		}		

		public function get model():UserAccessModel
		{
			return _model;
		}
		
		/**
		 * Récupère les paramètres de l'utilisateur connecté pour un module (au niveau de l'abonnement)
		 */		
		public function getUserAccess():void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.UserAccessService",
				"getModuleUserAccess",
				handler.getUserAccessResultHandler);
			
				RemoteObjectUtil.callService(op);		
		}
		
		/**
		 * Verifie le mot de passe de l'utilisateur connecté
		 * @param password (obligatoire)
		 */		
		public function checkPassWord(value:String):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.UserAccessService",
				"checkPassWord",
				handler.checkPassWordResultHandler);
			
			RemoteObjectUtil.callService(op,value);		
		}
		
		public function setInfosUserSession(idpool:Number, codeColonne:String = '', valueColonne:String = ''):void
		{
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			remote.source = "fr.consotel.consoview.M111.UserAccessService";
			remote.addEventListener(ResultEvent.RESULT, handler.updateUserSessionResultHandler,false,0,true);
			remote.setUserSession(idpool, codeColonne, valueColonne);
			trace("##UserAccessService.setInfosUserSession("+idpool+","+ codeColonne+","+ valueColonne+")");
			
			
		}
	}
}