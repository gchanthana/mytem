package intranet.gestionGroupe
{
	import mx.managers.PopUpManager;
	import mx.controls.Alert;
	import flash.events.MouseEvent;
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.ArrayCollection;
	import fr.consotel.consoview.util.ConsoViewErrorManager;
	import mx.rpc.events.FaultEvent;
	
	public class FenetreAjouterCompteFac extends FenetreAjouterCompteFacIHM
	{
		private var objGridCompteFac : GridCompteFac ;
		private var modif : Boolean
		private var libelle : String;
		private var idOperateur : int;
  
        public function FenetreAjouterCompteFac(objGridCompteFac : GridCompteFac,modif : Boolean,libelle :String = "",idOperateur : int = -1)
        {
        	super();
         	this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
         	this.objGridCompteFac = objGridCompteFac;
         	this.modif=modif;
         	this.libelle=libelle;
         	this.idOperateur=idOperateur;
         	
        }
        public function initIHM(event:Event) : void
        {
        	btCancel.addEventListener(MouseEvent.CLICK,removeMe);    
            btValide.addEventListener(MouseEvent.CLICK, submitData);
            
           //labOpe.text = "To do";
           labSociete.text=objGridCompteFac.getSociete().dgRefClient.selectedItem.LIBELLE;
        	inputLibelle.text = libelle;
        	getListeOpera(null);
        	
        
        }
        private function submitData(event:MouseEvent):void {
        	if (modif==true){
           		objGridCompteFac.addCF(libelle,idOperateur,inputLibelle.text,comboOperateurCF.selectedItem.OPERATEURID,true);
         	}
         	else
         	{
         		objGridCompteFac.addCF(libelle,idOperateur,inputLibelle.text,comboOperateurCF.selectedItem.OPERATEURID,false);
         	}
         	PopUpManager.removePopUp(this);
       }
	   private function removeMe(event:MouseEvent):void {
            PopUpManager.removePopUp(this);
       }
       		private function getListeOpera(event : Event):void{
			var op2:AbstractOperation =	RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoprod.intranet.procedureGestionGroupeSociete",
																	"getListeOperateur",
																	processGetListeOperateur,remoteError);
			RemoteObjectUtil.callService(op2);
		}
		
		private function processGetListeOperateur(event:ResultEvent):void {
			comboOperateurCF.dataProvider=event.result as ArrayCollection;
			
			  for(var i :int =0;i<  (comboOperateurCF.dataProvider as ArrayCollection).length;i++)
           {
	           	if((comboOperateurCF.dataProvider as ArrayCollection).getItemAt(i).OPERATEURID == idOperateur)
	           	{
	           		comboOperateurCF.selectedItem = (comboOperateurCF.dataProvider as ArrayCollection).getItemAt(i) ;
	           	
	           	}
           }
		}
		private function remoteError(faultEvent:FaultEvent):void {
			ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
		}
	}
}