package gestionparc.ihm.action.fournir.actionfournireqp
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.ihm.fiches.collaborateur.FicheFournirEqp;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionFournirEqpReceiver implements IReceiver
	{
		private var fiche:FicheFournirEqp;
		
		public function ActionFournirEqpReceiver()
		{
		}
		public function action():void
		{
			fiche = new FicheFournirEqp();
			PopUpManager.addPopUp(fiche,Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}