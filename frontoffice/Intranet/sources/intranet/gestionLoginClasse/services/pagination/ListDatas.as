package intranet.gestionLoginClasse.services.pagination
{
    import mx.collections.ArrayCollection;

    public class ListDatas
    {
        private var _listLogin:ArrayCollection;

        public function ListDatas()
        {
            listLogin = new ArrayCollection();
        }

        public function get listLogin():ArrayCollection
        {
            return _listLogin;
        }

        public function set listLogin(value:ArrayCollection):void
        {
            _listLogin = value;
        }
    }
}