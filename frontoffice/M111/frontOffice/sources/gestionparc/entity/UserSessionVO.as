package gestionparc.entity
{
	public dynamic class UserSessionVO
	{
		private var _IDPOOL:Number;
		private var _COLONNE_CODE:String;
		private var _COLONNE_VALUE:String;
		
		public function UserSessionVO()
		{
		}
		
		public function get COLONNE_VALUE():String { return _COLONNE_VALUE; }
		
		public function set COLONNE_VALUE(value:String):void
		{
			if (_COLONNE_VALUE == value)
				return;
			_COLONNE_VALUE = value;
		}
		
		
		public function get COLONNE_CODE():String { return _COLONNE_CODE; }
		
		public function set COLONNE_CODE(value:String):void
		{
			if (_COLONNE_CODE == value)
				return;
			_COLONNE_CODE = value;
		}
		
		
		public function get IDPOOL():Number { return _IDPOOL; }
		
		public function set IDPOOL(value:Number):void
		{
			if (_IDPOOL == value)
				return;
			_IDPOOL = value;
		}
	}
}