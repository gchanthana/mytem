package gestionparc.ihm.action.fournir.actionfournireqp
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionFournirEqpCommand implements ICommand
	{
		private var receiver : ActionFournirEqpReceiver = null;
		
		public function ActionFournirEqpCommand(rec:ActionFournirEqpReceiver)
		{
			this.receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action()
		}
	}
}