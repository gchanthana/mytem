<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.DeliveryEvent"
extends="fr.consotel.api.ibis.publisher.handler.event.AbstractReportingEvent" implements="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"
hint="Implémentation abstraite qui sert de classe parent pour les évènements de type IDeliveryEvent
Ils sont utilisés pour encapsuler un évènement de type IReportingEvent et permettent d'accéder aux infos et contenu du reporting correspondant
L'évènement IReportingEvent encapsulé doit correspondre à celui dispatché par une implémentation IReportingService lors de l'exécution du reporting   
Les méthodes getStatus(), getReportingName() et getReportingService() sont redirigées vers l'instance retournée par getReportingEvent()
La méthode getDeliveryType() est redéfinie pour retourner la valeur de la propriété DIFFUSION provenant de la structure retournée par getDelivery()
Cette propriété permet d'indiquer la stratégie à laquelle sera déléguée le traitement de cette implémentation si elle est à nouveau dispatchée">
	<cffunction access="public" name="createDeliveryEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"
	hint="Constructeur. Une exception est levée si l'évènement eventSource est égal à la référence THIS.
	La comparaison des références est effectuée avec la méthode Java Object.equals() qui est récupérée par introspection Java">
		<cfargument name="eventSource" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true"
		hint="Implémentation de l'évènement à partir duquel les infos et le contenu du reporting sont extraites">
		<cfset var reportingEvent=ARGUMENTS["eventSource"]>
		<!--- Comparaison de la référence eventSource et THIS --->
		<cfif isSameEventReference(reportingEvent)>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'évènement source pointe sur la meme référence que cette instance">
		<cfelse>
			<!--- Implémentation AbstractReportingEvent des méthodes de IReportingEvent --->
			<cfset SUPER.createReportingEvent({})>
			<!--- Evènement IReportingEvent contenant les infos concernant le contenu du reporting --->
			<cfif NOT isDefined("VARIABLES.EVENT_SOURCE")>
				<cfset VARIABLES.EVENT_SOURCE=reportingEvent>
			</cfif>
			
			<!--- TEST --->
			<cfif NOT isDefined("VARIABLES.EVENT_DISPATCHER")>
				<cfset VARIABLES.EVENT_DISPATCHER=getEventSource().getDeliveryType()>
			</cfif>
			
			<cfreturn THIS>
		</cfif>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getTemplateId" returntype="String" hint="Retourne une chaine vide si le nom du template n'est pas défini">
		<cfset var reportingProperties=getReportingEvent().getReporting()>
		<cfif structKeyExists(reportingProperties,"xdoTemplateId")>
			<cfreturn reportingProperties.xdoTemplateId[1]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getFormat" returntype="String" hint="Retourne une chaine vide si le format n'est pas défini">
		<cfset var reportingProperties=getReportingEvent().getReporting()>
		<cfif structKeyExists(reportingProperties,"outputFormat")>
			<cfreturn reportingProperties.outputFormat[1]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getLocalization" returntype="String" hint="Retourne une chaine vide si la localisation n'est pas définie">
		<cfset var reportingProperties=getReportingEvent().getReporting()>
		<cfif structKeyExists(reportingProperties,"localization")>
			<cfreturn reportingProperties.localization[1]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getTimeZone" returntype="String" hint="Retourne une chaine vide si le TimeZone n'est pas défini">
		<cfset var reportingProperties=getReportingEvent().getReporting()>
		<cfif structKeyExists(reportingProperties,"timeZone")>
			<cfreturn reportingProperties.timeZone[1]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getReportParameters" returntype="Struct" hint="Retourne getReportingEvent().getXDO()">
		<cfreturn getReportingEvent().getXDO()>
	</cffunction>
	
	<cffunction access="public" name="getReportingEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent">
		<cfif isInstanceOf(getEventSource(),"fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent")>
			<cfreturn getEventSource().getReportingEvent()>
		<cfelse>
			<cfreturn getEventSource()>
		</cfif>
	</cffunction>

	<cffunction access="public" name="getEventSource" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent">
		<cfif structKeyExists(VARIABLES,"EVENT_SOURCE")>
			<cfreturn VARIABLES.EVENT_SOURCE>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
				message="L'évènement source n'est pas correctement défini" detail="Type de l'évènement : #getMetadata(THIS).NAME#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getContent" returntype="any" hint="N'est pas implémentée. Lève une exception">
		<cfargument name="localFileName" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>

	<cffunction access="public" name="getContentAsLocalFile" returntype="String" hint="N'est pas implémentée. Lève une exception">
		<cfargument name="localFileName" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getDeliveryInfos" returntype="any" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getContentType" returntype="String" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="isTextContent" returntype="boolean" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="isBinaryContent" returntype="boolean" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="isUnknownContent" returntype="boolean" hint="N'est pas implémentée. Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée">
	</cffunction>
	
	<cffunction access="public" name="getLocalPathSep" returntype="String" hint="Retourne la valeur : /">
		<cfreturn "/">
	</cffunction>

	<cffunction access="private" name="isDeliveryKeyDefined" returntype="boolean"
	hint="Retourne TRUE si la clé deliveryKey vérifie toutes les conditions suivantes :
	- La clé est présente dans la structure retournée par getReportingEvent().getDelivery()
	- La valeur associée à la clé est un tableau
	- Le tableau associé à la clé contient au moins un élément
	Une exception est levée si les 2 dernières conditions ci-dessus ne sont pas vérifiées">
		<cfargument name="deliveryKey" type="String" required="true" hint="Clé correspondant à une propriétés de la stratégie de diffusion">
		<cfset var key=ARGUMENTS["deliveryKey"]>
		<cfset var deliveryProperties=getReportingEvent().getDelivery()>
		<cfif NOT structKeyExists(deliveryProperties,key)>
			<cfreturn FALSE>
		<cfelse>
			<cfif isValid("Array",deliveryProperties[key]) AND (arrayLen(deliveryProperties[key]) GT 0)>
				<cfreturn TRUE>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La propriété #key# n'est pas correctement définie">
			</cfif> 
		</cfif>
	</cffunction>

	<cffunction access="private" name="getDeliveryKeyValue" returntype="String"
	hint="Retourne le 1er élément du tableau associé à la clé deliveryKey si isDeliveryKeyDefined(deliveryKey) vaut TRUE et une chaine vide sinon">
		<cfargument name="deliveryKey" type="String" required="true" hint="Clé correspondant à une propriétés de la stratégie de diffusion">
		<cfset var key=ARGUMENTS["deliveryKey"]>
		<cfif isDeliveryKeyDefined(key)>
			<cfset var deliveryProperties=getReportingEvent().getDelivery()>
			<cfreturn deliveryProperties[key][1]>
		<cfelse>
			<cfreturn ""> 
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="isSameEventReference" returntype="boolean" hint="Retourne TRUE si eventSource référence le meme objet que THIS
	La comparaison est effectuée avec la méthode Java Object.equals() qui est récupérée par introspection Java. Cette méthode est utilisée dans createDeliveryEvent()">
		<cfargument name="eventSource" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true">
		<cfset var reportingEvent=ARGUMENTS["eventSource"]>
		<!--- Récupération de la méthode java.lang.Object.equals() par introspection Java --->
		<cfset var javaObject=createObject("java","java.lang.Object")>
		<cfset var javaObjectClass=javaObject.getClass()>
		<cfset var equalsMethod=javaObjectClass.getMethod("equals",[javaObjectClass])>
		<cfreturn equalsMethod.invoke(reportingEvent,[THIS])>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.AbstractReportingEvent --->
	<cffunction access="public" name="getStatus" returntype="String" hint="Retourne la valeur de getReportingEvent().getStatus()">
		<cfreturn getReportingEvent().getStatus()>
	</cffunction>
	
	<cffunction access="public" name="getReportingName" returntype="String" hint="Retourne la valeur de getReportingEvent().getReportingName()">
		<cfreturn getReportingEvent().getReportingName()>
	</cffunction>
	
	<cffunction access="public" name="getReportingService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService"
	hint="Retourne la valeur de getReportingEvent().getReportingService()">
		<cfreturn getReportingEvent().getReportingService()>
	</cffunction>
	
	<cffunction access="public" name="getDeliveryType" returntype="String"
	hint="Retourne la valeur de la propriété DIFFUSION provenant de la structure retournée par getDelivery() ou une chaine vide si elle n'est pas définie">
		<cfreturn getDeliveryKeyValue("DIFFUSION")>
	</cffunction>
	
	<cffunction access="public" name="createReportingEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent" hint="Lève une exception">
		<cfargument name="eventData" type="Struct" required="true">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION"
		message="Le constructeur createReportingEvent() n'est supporté par cette implémentation" detail="Utiliser le constructeur createDeliveryEvent()">
	</cffunction>
</cfcomponent>