<cfcomponent name="JournalRessourcesNonFacturees">	
	<cffunction name="afficher"> 						
		<cfsetting enablecfoutputonly="true" />
		<cfset label="Journal">		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.util.utils").getQuery("session.dataInventaire.journal")>				
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv">
		<cfcontent type="text/plain">
		<cfoutput>Produit;Opérateur;N°opération;Type opération;Libellé opération;Etat#NewLine#</cfoutput>		
		<cfoutput query="qGetGrid">#TRIM(LIBELLE_PRODUIT)#;#TRIM(OPNOM)#;#TRIM(SOUS_TETE)#;#TRIM(NUMERO_OPERATION)#;#TRIM(TYPE_OPERATION)#;#TRIM(LIBELLE_OPERATIONS)#;#TRIM(LIBELLE_ETAT)##NewLine#</cfoutput>	
	</cffunction>
</cfcomponent>  
