package univers.inventaire.commande.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.commande.system.ArticleEvent;
	
	public class CreerEquipementNueImpl extends CreerArticleImpl
	{
		public function CreerEquipementNueImpl()
		{
			super();
		}
		
		override protected function txtFiltreChangeHandler(event:Event): void
	    {	    
	    		
	    	if(dgListe.dataProvider)
	    	{	
	    		(dgListe.dataProvider as XMLListCollection).filterFunction = listeEquipementsFilterFunction;
	    		(dgListe.dataProvider as XMLListCollection).refresh();
	    	}
	    }
		
		protected function listeEquipementsFilterFunction(item:Object):Boolean
	    {	
	    	
		    var rfilter:Boolean = true;
		    var node:XML = XML(item);
		    
		    // Filtre sur le libelle des équipements
		    if (node.libelle != undefined)
		    {  
		        rfilter = rfilter && (String(node.libelle ).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le libelle du type d équipements
		    if (node.type != undefined)
		    {  
		        rfilter = rfilter && (String(node.type).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le prix
		    if (node.prix != undefined)
		    {  
		        rfilter = rfilter && (String(node.prix).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }		    
		    return rfilter;
	    }
	    
	    override protected function btValiderClickHandler(event:MouseEvent):void
		{	
			var boolEquipement:Boolean = false;
			var message:String="\n";
			
			if(XMLList(article.article.equipements.equipement).length() >0)			
			{
				boolEquipement = true
			}
			else
			{
				boolEquipement = false
			}
				
			if (boolEquipement)
			{
				//article.sousTete = txtNumLigne.text;			
				article.codeInterne = txtCodeInterne.text;
				
				if (boolCreate)
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_CREATED));
				}
				else
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_UPDATED));
				}
				
				PopUpManager.removePopUp(this);	
			}
			else
			{
				Alert.show("Vous devez sélectionner un équipement","Erreur")
			}
			
		}
	}
}