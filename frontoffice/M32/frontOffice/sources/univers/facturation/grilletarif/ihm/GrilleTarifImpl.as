package univers.facturation.grilletarif.ihm
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.facturation.grilletarif.entity.GroupeProduitVO;
	import univers.facturation.grilletarif.entity.OperateurVO;
	import univers.facturation.grilletarif.entity.ParametreRechercheVO;
	import univers.facturation.grilletarif.event.GrilleTarifEvent;
	import univers.facturation.grilletarif.services.GrilleTarifService;
	import univers.facturation.grilletarif.utils.DataGridDataExporter;
	import univers.facturation.grilletarif.utils.GestionSuppressionTarif;
	import univers.facturation.grilletarif.utils.MessageGrilleTarif;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	
	[Bindable]
	public class GrilleTarifImpl extends VBox
	{
		public const NBITEMPARPAGE : int = 10;
		public static var LIBELLE_CF:String;
		public static var LIBELLE_SCPTE:String;

		
		public var serviceGrilleTarif : GrilleTarifService = new GrilleTarifService();		
		public var myPaginatedatagrid : PaginateDatagrid;
		public var parametreRecherche : ParametreRechercheVO = new ParametreRechercheVO();
		public var idRacine:int;
		
		//UI COMPONANT:
		public var comboOP:ComboBox;
		public var comboType:ComboBox;
		public var comboException:ComboBox;
		public var comboExceptionType:ComboBox;
		public var comboCatalogue:ComboBox;
		public var comboControle:ComboBox;
		public var inputProduit:TextInput;
		public var backup_last_param_recherche_vo : ParametreRechercheVO;
		public var popAjouterTarif : AjoutVersionTarifIHM;
		private var reselectTarifIndex : int = -1;
		
		
		public function GrilleTarifImpl()
		{
			
		}
	
		//Handler on perimetre change
		public function initData():void
		{
			addEventListener(GrilleTarifEvent.OPEN_POP_CREATE_VERSION_TARIF,ajouterTarif);
			addEventListener(GrilleTarifEvent.OPEN_POP_EDIT_TARIF,editerTarif);
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.REMOVE_VERSION_TARIF_COMPLETE,removeVersionTarifCompleteHandler);
			serviceGrilleTarif.addEventListener(GrilleTarifEvent.ADD_VERSION_TARIF_COMPLETE,addVersionTarifHandler);
			initEcranGrilleTarif();
			
		}
		
		private function initEcranGrilleTarif():void
		{
			idRacine = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			parametreRecherche.idracine = idRacine;
			parametreRecherche.number_of_records = NBITEMPARPAGE;
			serviceGrilleTarif.get_col_operateur(idRacine);
		}
		
		private function removeVersionTarifCompleteHandler(evt : GrilleTarifEvent):void
		{
			var obj : Object = evt.objectReturn;
			if(obj.typeSuppression==GestionSuppressionTarif.VERSION_TARIF_GENERAL)
			{
				if(obj.removeUniqueVersion)
				{
					rechercher()
				}
				else
				{
					onItemSelectedHandler();
				}
			}
		}
		private function addVersionTarifHandler(evt: GrilleTarifEvent):void
		{
			Alert.show(MessageGrilleTarif.MESSAGE_SUCCES,MessageGrilleTarif.TITRE_INFORMATION);
			onItemSelectedHandler();
		}
		private function changeLibelleCompteHandler(evt : GrilleTarifEvent):void
		{
			GrilleTarifImpl.LIBELLE_CF = (evt.objectReturn as OperateurVO).libelleCF;
			GrilleTarifImpl.LIBELLE_SCPTE = (evt.objectReturn as OperateurVO).libelleSCpte;
		}
		public function comboOPChangeHanlder(evt : Event):void
		{
			if(comboOP.selectedItem)
			{
				parametreRecherche.operateurid = (comboOP.selectedItem as OperateurVO).idOperateur;
				serviceGrilleTarif.addEventListener(GrilleTarifEvent.GET_LIBELLE_COMPTE_COMPLETE,changeLibelleCompteHandler);
				serviceGrilleTarif.get_opeLibelles(comboOP.selectedItem as OperateurVO);
			}
		}
		public function ajouterTarif(evt : GrilleTarifEvent=null):void
		{
			popAjouterTarif = new AjoutVersionTarifIHM();
			popAjouterTarif.title = ResourceManager.getInstance().getString('M32', 'Ajouter_une_version_de_tarif');
			popAjouterTarif.serviceGrilleTarif = serviceGrilleTarif;
			popAjouterTarif.idRacine = idRacine;
			popAjouterTarif.groupeProduit_current = myPaginatedatagrid.selectedItem as GroupeProduitVO ;
			PopUpManager.addPopUp(popAjouterTarif,this,true);
			PopUpManager.centerPopUp(popAjouterTarif);	
			reselectTarifIndex = myPaginatedatagrid.selectedIndex;
		}
		public function onItemSelectedHandler(event : PaginateDatagridEvent=null):void
		{
			if(myPaginatedatagrid.selectedItem)
			{
				var idRacineC : int = idRacine
				if((myPaginatedatagrid.selectedItem as GroupeProduitVO).isPublic)
				{
					idRacineC = 0;
				}
				serviceGrilleTarif.get_version_general(idRacineC,(myPaginatedatagrid.selectedItem as GroupeProduitVO).id_groupe_produit);
			}
		}
		public function showProduitOfGroupe():void
		{
			if(myPaginatedatagrid.selectedItem)
			{
				var pop : ProduitOfGroupeIHM = new ProduitOfGroupeIHM();
				pop.groupeProduits = myPaginatedatagrid.selectedItem as GroupeProduitVO;
				pop.serviceGrilleTarif = serviceGrilleTarif;
				PopUpManager.addPopUp(pop,this,true);
				PopUpManager.centerPopUp(pop);
			}
		}
		public function editerTarif(evt : GrilleTarifEvent=null):void
		{
			
				var popEditTarif : EditionVersionTarifIHM = new EditionVersionTarifIHM();
				popEditTarif.addEventListener(GrilleTarifEvent.REFRESHGRIDTARIF,rechercher);
				popEditTarif.serviceGrilleTarif = serviceGrilleTarif;
				popEditTarif.groupeProduits = (myPaginatedatagrid.selectedItem as GroupeProduitVO);
				popEditTarif.operateur_current = (comboOP.selectedItem 	as OperateurVO);
				popEditTarif.col_version_general = serviceGrilleTarif.col_version_tarif_general;
				popEditTarif.idRacine = idRacine;
				PopUpManager.addPopUp(popEditTarif,this,true);
				PopUpManager.centerPopUp(popEditTarif);
				
			
		}
		
		
		public function onIntervalleChangeHandler(evt : PaginateDatagridEvent):void
		{
			rechercher()
		}
		public function isControleHandler(gp : GroupeProduitVO,event : Event):void
		{
			if(gp.aucune_version)
			{
				Alert.show(MessageGrilleTarif.MESSAGE_CONTROLE,MessageGrilleTarif.TITRE_INFORMATION);
				var cb : CheckBox = (event.target as CheckBox);
				cb.selected = false;
			}
			else
			{
				gp.isControle = (event.target as CheckBox).selected;
				serviceGrilleTarif.updateIsControle(gp.id_groupe_produit,idRacine);
			}
			
		}
		public function isControleMessage(gp : GroupeProduitVO):void
		{
		
		}
		public function btRechercheHandler():void
		{
			rechercher(null,true);
		}
		
		
		//Le param nouvelle recherche est à true quand on clic sur le bt recherche, sinon c'est une actualisation ou un changement de page
		public function rechercher(evt : GrilleTarifEvent=null,nouvelleRecherche:Boolean = false):void
		{
			if(comboOP.selectedItem)
			{
				
				
				if(!parametreRecherche.callForExport)
				{
					serviceGrilleTarif.col_version_tarif_general.removeAll();
				}
				
				
				
				if(nouvelleRecherche)
				{
					myPaginatedatagrid.refreshPaginateDatagrid(true); // il faut remettre la pagination à l'état initial
					parametreRecherche.index_debut = 1;
				}
				else
				{
					if(myPaginatedatagrid.currentIntervalle)
					{
						parametreRecherche.index_debut = myPaginatedatagrid.currentIntervalle.indexDepart+1;
					}
					else //Si c'est la première recherche
					{ 
						parametreRecherche.index_debut = 1;
					}
				}
			
				
				serviceGrilleTarif.rechercherGroupeProduit(parametreRecherche);
				
				//AFTER RECHERCHE:
				//Gestion du bouton calculer le montant des 6 derniers mois : 
				//C'est un param de la recherhce : avec_calcul = 0 / 1 
				//Si l utilisateur change un param avant de cliquer sur le bt calculer le montant 6 dernier mois : les groupe de prod affichés ne seront pas identiques
				//Donc on stock un cliché de la recherhce dans backup_last_param_recherche_vo et on se servira de cette object avec les mêmes param si on demande un calcul des montant 6 derniers mois
							
				parametreRecherche.avec_calcul = 0; // On remet a NON le calcul 6 derniers mois
				parametreRecherche.callForExport = false; // On remet a NON le call for export
				
				backup_last_param_recherche_vo = ObjectUtil.copy(parametreRecherche) as ParametreRechercheVO; // On stock les params de la recherche
				registerClassAlias("univers.facturation.grilletarif.entity.ParametreRechercheVO",ParametreRechercheVO);
				backup_last_param_recherche_vo=ParametreRechercheVO(ObjectUtil.copy(parametreRecherche)) // delete les ref sur l'objet (clone sans reférence)
			}
			else
			{
				Alert.show(MessageGrilleTarif.MESSAGE_NO_OPERATEUR_SELECTED,MessageGrilleTarif.TITRE_INFORMATION);
			}	
		
		}
		public function btExportDataHandler(evt : MouseEvent):void
		{
			if(myPaginatedatagrid.dataprovider.length>0)
			{
				parametreRecherche = backup_last_param_recherche_vo;
				parametreRecherche.callForExport = true;
				parametreRecherche.number_of_records=(myPaginatedatagrid.dataprovider.getItemAt(0) as GroupeProduitVO).NBRECORD;
				serviceGrilleTarif.addEventListener(GrilleTarifEvent.DATA_EXPORT_COMPLETE,exportCSV_handler);
				rechercher();
			}
			else
			{
				Alert.show(MessageGrilleTarif.MESSAGE_NO_DATA,MessageGrilleTarif.TITRE_INFORMATION);
			}
		}
		public function btAvCalculHandler(evt : MouseEvent):void
		{
			if(myPaginatedatagrid.dataprovider.length>0)
			{
				parametreRecherche = backup_last_param_recherche_vo;
				parametreRecherche.avec_calcul = 1;
				rechercher();
			}
			else
			{
				Alert.show(MessageGrilleTarif.MESSAGE_NO_DATA,MessageGrilleTarif.TITRE_INFORMATION);
			}
			
		}
   		
		public function exportCSV_handler(evt:GrilleTarifEvent):void
		{
			serviceGrilleTarif.removeEventListener(GrilleTarifEvent.DATA_EXPORT_COMPLETE,exportCSV_handler);
			
			if(serviceGrilleTarif.col_data_export.length > 0) 
			{     
				var dataToExport  :String = DataGridDataExporter.getCSVString(myPaginatedatagrid.dgPaginate,serviceGrilleTarif.col_data_export,";","\n",false);
				var url :String = moduleSuiviFacturationIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M32/exportCSVString.cfm";
				DataGridDataExporter.exportCSVString(url,dataToExport,"export");
			 }
			else
			{
				Alert.show(MessageGrilleTarif.MESSAGE_NO_DATA,MessageGrilleTarif.TITRE_INFORMATION);
			}
		}
		public function comboTypeChangeHanlder(evt : Event):void
		{
			if(!comboType.selectedItem)
			{
				comboType.selectedIndex=0;				
			}
			// 0=tous; 1=abonnements; 2=consommations
			parametreRecherche.type_grp_ctl = comboType.selectedItem.value;
		}
		public function comboExceptionChangeHanlder(evt : Event):void
		{
			if(!comboException.selectedItem)
			{
				comboException.selectedIndex=0;	
			}
			// -1=tous; 1=avec; 0=sans
			parametreRecherche.avec_exception = comboException.selectedItem.value;
			
		} 
		public function comboExceptionTypeChangeHanlder(evt : Event):void
		{
			if(!comboExceptionType.selectedItem)
			{
				comboExceptionType.selectedIndex = 0;
			}
			// -2=périmé; 3=en cours; 4=a venir
			parametreRecherche.etat_exception = comboExceptionType.selectedItem.value;
		}
		public function comboCatalogueChangeHanlder(evt : Event):void
		{
			if(!comboCatalogue.selectedItem)
			{
				comboCatalogue.selectedIndex = 0;
			}
			// 0=tous; 1=public; 2=client
			parametreRecherche.code_type_tarif = comboCatalogue.selectedItem.value;
		}
		public function comboControleChangeHanlder(evt : Event):void
		{
			if(!comboControle.selectedItem)
			{
				comboControle.selectedIndex=0;
			}
			// -1=tous; 1=oui; 0=non
			parametreRecherche.controler = comboControle.selectedItem.value;
		}
		public function inputProduitChangeHanlder(evt : Event):void
		{
			parametreRecherche.libelle_grp = inputProduit.text;
		}
		
		//Appelé au chgt de périmètre
		//gardé pour des raison de compatibilité		
		public function onPerimetreChange():void
		{
		
		}
		
   	}
}