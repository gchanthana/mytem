<cfcomponent output="false">

	<!--- CREATION UUID (ID UNIQUE SUR 35 CAR) --->
	<cffunction name="initUUID" access="remote" output="false" returntype="String" hint="CREATION UUID (ID UNIQUE SUR 35 CAR)">
		
			<cfset uuid = createUUID()>
					
		<cfreturn uuid> 
	</cffunction>

</cfcomponent>