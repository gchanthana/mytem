package univers.facturation.suivi.controles.vues
{
    import mx.resources.ResourceManager;
	import composants.util.ConsoviewFormatter;
    import composants.util.DateFunction;
    
    import flash.events.MouseEvent;
    
    import mx.charts.CategoryAxis;
    import mx.charts.ColumnChart;
    import mx.charts.HitData;
    import mx.charts.Legend;
    import mx.containers.VBox;
    import mx.controls.Image;
    
    import univers.facturation.suivi.controles.controls.AbstractControleFacture;

    public class GraphEvolutionCout extends VBox
    {
        [Bindable]
        public var myChartHistorique:ColumnChart;
        [Bindable]
        public var myLegend:Legend;
        public var imgCSV:Image;
        private var _ctrlFactureApp:AbstractControleFacture;

        public function GraphEvolutionCout()
        {
            //TODO: implement function
            super();
        }

        [Bindable]
        public function get ctrlFactureApp():AbstractControleFacture
        {
            return _ctrlFactureApp;
        }

        public function set ctrlFactureApp(ctrl:AbstractControleFacture):void
        {
            _ctrlFactureApp = ctrl;
        }

        protected function formateDates(categoryValue:Object, previousCategoryValue:Object, axis:CategoryAxis, categoryItem:Object):String
        {
            if (categoryValue != null)
            {
                return formatDateAsShortString(categoryValue as Date).substr(3, 5);
                ;
            }
            else
                return "-";
        }

        public static function formatDateAsShortString(d:Date):String
        {
            if (d != null)
            {
                var jour:String = d.getDate().toString();
                if (jour.length == 1)
                    jour = "0" + jour;
                var mois:String = (d.getMonth() + 1).toString();
                if (mois.length == 1)
                    mois = "0" + mois;
                var annee:String = d.getFullYear().toString().substr(2, 2);
                var v:String = jour + "/" + mois + "/" + annee;
                return v;
            }
            else
            {
                return "";
            }
        }

        //formate les tootips du graph des themes
        protected function formatDataTip(obj:HitData):String
        {
            var periode:String = DateFunction.formatDateAsString(obj.item["MOIS"] as Date).substr(3, 7);
            var montantConso:Number = obj.item["COUTCONSO"];
            var montantAbo:Number = obj.item["COUTABO"];
            var strMontantAbo:String = ConsoviewFormatter.formatEuroCurrency(montantAbo, 2);
            var strMontantConso:String = ConsoviewFormatter.formatEuroCurrency(montantConso, 4);
            if (obj.element.name == "abo")
            {
                return ResourceManager.getInstance().getString('M32', 'Mois____b_') + periode + ResourceManager.getInstance().getString('M32', '__b__br_Montant_Abo____b__font_color____') + strMontantAbo + "</font></b>";
            }
            else if (obj.element.name == "conso")
            {
                return ResourceManager.getInstance().getString('M32', 'Mois____b_') + periode + ResourceManager.getInstance().getString('M32', '__b__br_Montant_Conso____b__font_color__') + strMontantConso + "</font></b>"
            }
            return ResourceManager.getInstance().getString('M32', 'Mois____b_') + periode + ResourceManager.getInstance().getString('M32', '__b__br_Montant_Conso____b__font_color__') + strMontantConso + "</font></b>" + ResourceManager.getInstance().getString('M32', '_br_Montant_Abo____b__font_color____ff00') + strMontantAbo + "</font></b>";
        }

        protected function imgCSVClickHandler(me:MouseEvent):void
        {
            if (ctrlFactureApp.evolutionCouts.length != 0)
                ctrlFactureApp.exporterEvolutionCoutByCf();
        }
    }
}