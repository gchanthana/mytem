package gestionparc.entity
{
	[Bindable]
	public class OperateurVO
	{

		public var idOperateur:int=0;

		public var nom:String="";
		public var contact:String="";

		public var adresse:String="";
		public var codePostale:String="";
		public var commune:String="";
		public var pays:String="";

	}
}