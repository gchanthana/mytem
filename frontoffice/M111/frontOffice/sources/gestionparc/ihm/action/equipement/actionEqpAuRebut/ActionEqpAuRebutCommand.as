package gestionparc.ihm.action.equipement.actionEqpAuRebut
{
	import gestionparc.ihm.action.ICommand;

	public class ActionEqpAuRebutCommand implements ICommand
	{
		private var receiver:ActionEqpAuRebutReceiver=null;

		public function ActionEqpAuRebutCommand(rec:ActionEqpAuRebutReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
