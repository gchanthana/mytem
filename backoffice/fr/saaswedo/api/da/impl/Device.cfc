<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.Device" extends="fr.saaswedo.api.da.impl.DataService" hint="Register Device">
	<!--- =========== URI des services =========== --->
	
	<cffunction access="public" name="REGISTER_DEVICE_URI" returntype="String" hint="URI du service Register Device">
		<cfreturn "/api/da/vd.cfm">
	</cffunction>
	
	<!--- Clés correspondant à des champs métiers --->

	<cffunction access="public" name="MCCMNC" returntype="String" hint="Clé pour mccmnc">
		<cfreturn "mccmnc">
	</cffunction>
	
	<cffunction access="public" name="DEVID" returntype="String" hint="Clé pour devId">
		<cfreturn "devId">
	</cffunction>
	
	<cffunction access="public" name="TOKENID" returntype="String" hint="Clé pour tokenId">
		<cfreturn "tokenId">
	</cffunction>
	
	<cffunction access="public" name="CFM_NUM" returntype="String" hint="Clé pour cfmNum">
		<cfreturn "cfmNum">
	</cffunction>

	<!--- =========== Codes HTTP =========== --->

	<cffunction access="public" name="MISSING_MCCNC" returntype="Numeric" hint="Code pour l'absence de DEVID()">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<cffunction access="public" name="MISSING_DEVID" returntype="Numeric" hint="Code pour l'absence de DEVID()">
		<cfreturn BAD_REQUEST()>
	</cffunction>
	
	<!--- =========== Implémentation du service =========== --->

	<cffunction access="private" name="serviceRequest" returntype="void" hint="Effectue le traitement du service
	puis appele l'implémentation parent pour envoyer la réponse HTTP par défaut de traitement avec succès">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<!--- Délégation du traitement : Stockage de test
		- Soit cette fonction lève une exception pour arreter le traitement et donc ne pas envoyer la réponse par défaut de succès
		- Sinon (i.e Dans tous les cas) : La réponse HTTP par défaut de traitement avec succès sera envoyée au client
		--->
		<cfset processData(targetURI,requestData)>
	</cffunction>

	<!--- =========== Vérification des données métiers =========== --->
	
	<cffunction access="private" name="checkData" returntype="Boolean" hint="Effectue les vérifications métiers pour le flux correspondant à targetPage.
	<br>- La clé UTS_KEY() est obligatoire et sa valeur doit etre une chaine correspondant à une date au format UTC (ISO 8601)">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var targetURI=ARGUMENTS.targetPage>
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<!--- Vérification des données métiers de l'implémentation parent : Renvoi une réponse HTTP si les données sont invalides --->
		<cfset var areRequestDataValid=SUPER.checkData(targetURI,requestData)>
		<cfif areRequestDataValid>
			<!--- Utilisés pour la réponse HTTP en cas d'invalidité des données --->
			<cfset var httpCode=AUTHORIZED()>
			<cfset var jsonResponse={"Message"="Bad HTTP request"}>
			<!--- Vérification des données métiers spécifiques à cette implémentation --->
			<cfset var payload=getPayload(requestData)>
			<!--- Vérifie le format des données : JSON (STRING) --->
			<cfset var isDataFormatValid=(NOT structIsEmpty(payload))>
			<cfif NOT isDataFormatValid>
				<!--- Réponse HTTP : Si la vérification Check 3 n'est pas validée --->
				<cfset httpCode=INVALID_DATA_FORMAT()>
				<cfset jsonResponse={"Message"="Data usage : bad format"}>
			</cfif>
			<cfif isDataFormatValid>
				<!--- Vérification du champ MCC_MNC (MCC/MNC) --->
				<cfif structKeyExists(payload,MCCMNC())>
					<cfif NOT isValidMCCMNC(payload[MCCMNC()])>
						<!--- Champ CODE invalide --->
						<cfset httpCode=INVALID_CODE()>
						<cfset jsonResponse={"Message"="MCC/MNC bad : format."}>
					</cfif>
				<cfelse>
					<cfset httpCode=MISSING_MCCNC()>
					<cfset jsonResponse={"Message"="Missing #MCCMNC()#"}>
				</cfif>
				<!--- Vérification du champ DEVID --->
				<cfif NOT structKeyExists(payload,DEVID())>
					<cfset httpCode=MISSING_DEVID()>
					<cfset jsonResponse={"Message"="Missing #DEVID()#"}>
				</cfif>
			</cfif>
			<!--- MAJ du statut de validité de la requete HTTP cliente : areRequestDataValid vaut TRUE avant cette affectation --->
			<cfset areRequestDataValid=httpCode EQ AUTHORIZED()>
			<!--- Réponse en cas d'invalidité des données --->
			<cfif NOT areRequestDataValid>
				<!--- Les cas d'invalidité de requete sont toujours mentionnés dans les logs --->
				<cflog type="error" text="[#log#][#targetURI#] HTTP #httpCode# returned to #clientInfo# - #jsonResponse.message#">
				<!--- Traitement des données invalides --->
				<cfset handleInvalidRequestData(targetURI,requestData,httpCode,jsonResponse)>
			</cfif>
		</cfif>
		<cfreturn areRequestDataValid>
	</cffunction>
	
	<!--- Traitement des données métiers valides --->
	
	<cffunction access="private" name="processData" returntype="void" hint="Effectue le traitement du flux Register device">
		<cfargument name="targetPage" type="String" required="true" hint="URI de la ressource">
		<cfargument name="httpRequestData" type="Struct" required="true" hint="Structure équivalente à celle retournée par getHttpRequestData()">
		<cfset var requestData=ARGUMENTS.httpRequestData>
		<cfset var payload=getPayload(requestData)>
		<!--- Si le champs CODE est présent alors les données sont traitées --->
		<cfif structKeyExists(payload,MCCMNC())>
			<!--- Données métiers --->
			<cfset var registerDeviceData=DUPLICATE(payload)>
			<!--- Ajout du champs UUID dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
			<cfset var escapeAsciiCode=jsonNumberEscapeChar()>
			<cfset registerDeviceData[UUID_HEADER()]=CHR(escapeAsciiCode) & getHeaderValue(UUID_HEADER(),requestData)>
			<!--- Ajout du champs MCCMNC dans les données métiers : Sa valeur est toujours préfixée par un caractère spécial --->
			<cfset registerDeviceData[MCCMNC()]=CHR(escapeAsciiCode) & payload[MCCMNC()]>
			<!--- Stockage des données --->
			<cfset saveRegisterDevice(registerDeviceData)>
		</cfif>
		<!--- Envoie au client la réponse HTTP par défaut de traitement avec succès --->
		<cfset SUPER.serviceRequest(DEFAULT_URI(),requestData)>
	</cffunction>
	
	<!--- Stockage des données --->
	
	<cffunction access="private" name="saveRegisterDevice" returntype="void" hint="Stocke le flux Register Deviceau format JSON">
		<cfargument name="datalertPayload" type="Struct" required="true" hint="Données du flux">
		<cfset var currentDateString=lsDateFormat(NOW(),"DD_MM_YYYY")>
		<cfset var fileName="RegisterDevice_" & currentDateString & ".txt">
		<cfset var fileTitle="Datalert Register device data of " & NOW()>
		<cfset saveDataInFile(serializeJSON(ARGUMENTS.datalertPayload),fileName,fileTitle)>
	</cffunction>
</cfcomponent>