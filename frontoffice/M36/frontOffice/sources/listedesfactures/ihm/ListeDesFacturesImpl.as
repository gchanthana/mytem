package listedesfactures.ihm
{
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import composants.tb.periode.PeriodeSelector;
	import composants.tb.periode.PeriodeSelector_IHM;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.sampler.NewObjectSample;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import listedesfactures.entity.CriteresRechercheVo;
	import listedesfactures.entity.FactureDisplayParamsVo;
	import listedesfactures.entity.FactureVo;
	import listedesfactures.event.ListeFacturesEvent;
	import listedesfactures.event.ListeOperateursEvent;
	import listedesfactures.services.affichagefacture.DisplayFactureAnnexeHTMLService;
	import listedesfactures.services.affichagefacture.DisplayFactureHTMLService;
	import listedesfactures.services.affichagefacture.DisplayFactureService;
	import listedesfactures.services.exports.ExportsService;
	import listedesfactures.services.facture.ListeFacturesService;
	import listedesfactures.services.operateurs.OperateursService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.VRule;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.SearchPaginateDatagrid;
	import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;
	
	/**
	 * <b>Classe <code>ListeDesFacturesImpl</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */	
	
	[Bindable]
	public class ListeDesFacturesImpl extends Canvas
	{
		private const ndItemMax:Number=70000;
		
		//variables composant
		public var mySelector:PeriodeSelector;
		public var myPeriode:Label;
		public var comboOperateur:ComboBox;
		public var test:DataGrid;
		public var myPaginatedatagrid:PaginateDatagrid;
		public var mySearchPaginateDatagrid:SearchPaginateDatagrid;
		public var imFacture:Image;
		public var imFactureIndex:Image;
		public var imPDF:Image;
		public var imXLS:Image;
		public var imCSV:Image;
		
		//variables objet et service
		public var params:CriteresRechercheVo=new CriteresRechercheVo();
		public var listeFacture:ListeFacturesService=new ListeFacturesService();
		public var listeOperateurs:OperateursService=new OperateursService();
		public var export:ExportsService=new ExportsService();
		public var factureHTML:FactureDisplayParamsVo;
		public var displayer:DisplayFactureService;
		
		//les variables session
		private var type_perimetre:String;
		private var id_perimetre:int;
		private var raison_sociale:String;
		
		//date début et date de fin
		private var datedeb:Date;
		private var datefin:Date;

		internal var _moisDeb:String; //le debut de la periode 
		internal var _moisFin:String; //la fin de la periode

		//import des icones
		[Embed(source="/assets/images/note.png")]
		public static const note:Class;

		[Embed(source="/assets/images/note_off.png")]
		public static const note_off:Class;

		[Embed(source="/assets/images/note_view.png")]
		public static const noteView:Class;

		[Embed(source="/assets/images/note_view_off.png")]
		public static const noteView_off:Class;

		[Embed(source="/assets/images/SpreadSheet_Total.png")]
		public static const SpreadSheet_Total:Class;

		[Embed(source="/assets/images/SpreadSheet_Total_off.png")]
		public static const SpreadSheet_Total_off:Class;

		[Embed(source="/assets/images/text.png")]
		public static const text:Class;

		[Embed(source="/assets/images/text_off.png")]
		public static const text_off:Class;

		[Embed(source="/assets/images/icon_pdf.png")]
		public static const icon_pdf:Class;

		[Embed(source="/assets/images/icon_pdf_off.png")]
		public static const icon_pdf_off:Class;
		
		[Embed(source="/assets/images/warning.png")]
		public static const icon_warning:Class;

		/**
		 * <b>Fonction <code>ListeDesFacturesImpl()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation après changement de périmètres.
		 * </pre></p>
		 *
		 */
		public function ListeDesFacturesImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, Init);
		}

		//Initialisation apres changement de périmètres
		public function afterPerimetreUpdated():void
		{

		}

		/**
		 * <b>Fonction <code>Init(event:Event)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialiser l'IHM.
		 * </pre></p>
		 *
		 * @param event FlexEvent.CREATION_COMPLETE
		 *
		 */
		public function Init(event:Event):void
		{

			type_perimetre=CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			id_perimetre=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			raison_sociale=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;


			//Gestion de la date
			mySelector.onPerimetreChange();
			initDate();
			mySelector.addEventListener("periodeChange", updatePeriode);
			setSelectedPeriode(_moisDeb, _moisFin);

			//Lister les opérateurs
			listeOperateurs.getListeOperateurs();
			listeOperateurs.model.addEventListener(ListeOperateursEvent.LF_REQUEST_COMPELTE, listeOperateurRequestCompleteHandler);

			//Cliquer sur une facture
			myPaginatedatagrid.dgPaginate.addEventListener(MouseEvent.CLICK, factureVisible);

			//Exporter la liste des factures
			listeFacture.model.addEventListener(ListeFacturesEvent.LISTE_FACTURE_COMPELTE, exporterResultat);

		}

		/**
		 * <b>Fonction <code>exporterResultat(event:ListeFacturesEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Rendre les icones de l'export en couleur et cliquable ou grise et non cliquable.
		 * </pre></p>
		 *
		 * @param event <code>ListeFacturesEvent</code> LISTE_FACTURE_COMPELTE
		 *
		 */
		public function exporterResultat(event:ListeFacturesEvent=null):void
		{
			var value:int=listeFacture.model.nbTotalItem
			if (value != 0)
			{
				imPDF.source=icon_pdf;
				imXLS.source=SpreadSheet_Total;
				imCSV.source=text;
				imPDF.buttonMode=true;
				imXLS.buttonMode=true;
				imCSV.buttonMode=true;
			}
			else
			{
				imPDF.source=icon_pdf_off;
				imXLS.source=SpreadSheet_Total_off;
				imCSV.source=text_off;
				imPDF.buttonMode=false;
				imXLS.buttonMode=false;
				imCSV.buttonMode=false;
			}
		}

		/**
		 * <b>Fonction <code>exporterResultat(event:ListeFacturesEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Rendre les icones de l'export en couleur et cliquable ou grise et non cliquable.
		 * </pre></p>
		 *
		 * @param action : <code>Boolean</code> tue si une facture est saisie et false dinon
		 *
		 */
		public function factureVisible(action:Boolean=false):void
		{
			if (action && (myPaginatedatagrid.dgPaginate.selectedIndex != -1))
			{
				imFacture.source=note;
				imFactureIndex.source=noteView;
				imFacture.buttonMode=true;
				imFactureIndex.buttonMode=true;
				
				var facture:FactureVo = new FactureVo();
				facture.fill(listeFacture.model.listeFacture[myPaginatedatagrid.dgPaginate.selectedIndex]);
				
				factureHTML=new FactureDisplayParamsVo(facture,'HTML',1);
				factureHTML.facture=facture;
				factureHTML.format='HTML';
				factureHTML.type=1;
				factureHTML.perimetre_id=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				factureHTML.perimetre_libelle=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
				factureHTML.perimetre_type=CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
				
			}
			else
			{
				imFacture.source=note_off;
				imFactureIndex.source=noteView_off;
				imFacture.buttonMode=false;
				imFactureIndex.buttonMode=false;
			}
		}

		/**
		 * <b>Fonction <code>onSelect()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Met le selectedIndex=0 si selectedIndex=-1.
		 * </pre></p>
		 *
		 *
		 */
		public function onSelect():void
		{
			if (comboOperateur.selectedIndex == -1)
			{
				comboOperateur.selectedIndex=0;
			}
		}

		/**
		 * <b>Fonction <code>listeOperateurRequestCompleteHandler(event:ListeOperateursEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Appel la fonction recherche(nouvelleRecherche:Boolean = false).
		 * </pre></p>
		 *
		 * @param event : <code>ListeOperateursEvent</code>
		 *
		 */
		private function listeOperateurRequestCompleteHandler(event:ListeOperateursEvent):void
		{
			recherche(true);
		}

		/**
		 * <b>Fonction <code>initDate()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise la date de début et la date de fin de ma sélection
		 * </pre></p>
		 *
		 */
		private function initDate():void
		{

			var periodeArray:Array=mySelector.getTabPeriode();
			var len:int=periodeArray.length;
			var month:AMonth;

			month=periodeArray[len - 2];
			_moisDeb=month.getDateDebut();
			_moisFin=month.getDateFin();

			datedeb=month.dateDebut;
			datefin=month.dateFin;

		}

		/**
		 * <b>Fonction <code>updatePeriode(periode : PeriodeEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Met la periode choisie dans des variables globales.
		 * </pre></p>
		 *
		 * @param periode : <code>PeriodeEvent</code>
		 *
		 */
		private function updatePeriode(periode:PeriodeEvent):void
		{
			_moisDeb=periode.moisDeb;
			_moisFin=periode.moisFin;
			datedeb=periode.dateDeb;
			datefin=periode.dateFin;
			trace(periode.moisDeb, periode.moisFin);
			setSelectedPeriode(_moisDeb, _moisFin);
		}

		/**
		 * <b>Fonction <code>setSelectedPeriode(moisDebut : String , moisFin : String)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Affiche la période choisie dans l'interface.
		 * </pre></p>
		 *
		 * @param moisDebut : <code>String</code>
		 * @param moisFin : <code>String</code>
		 *
		 */
		public function setSelectedPeriode(moisDebut:String, moisFin:String):void
		{

			var formMoisD:String=moisDebut.substr(0, 2) + " " + DateFunction.moisEnLettre(parseInt(moisDebut.substr(3, 2), 10) - 1) + " " + moisDebut.substr(6, 4);

			var formMoisF:String=moisFin.substr(0, 2) + " " + DateFunction.moisEnLettre(parseInt(moisFin.substr(3, 2), 10) - 1) + " " + moisFin.substr(6, 4);

			myPeriode.text=ResourceManager.getInstance().getString('M36', 'Du__') + formMoisD + ResourceManager.getInstance().getString('M36', '_au_') + formMoisF;

		}

		/**
		 * <b>Fonction <code>search(event:SearchPaginateDatagridEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Appel la fonction recherche(nouvelleRecherche:Boolean = false).
		 * </pre></p>
		 *
		 * @param event : <code>SearchPaginateDatagridEvent</code>
		 *
		 */
		public function search(event:SearchPaginateDatagridEvent):void
		{

			recherche(true);

		}

		/**
		 * <b>Fonction <code>recherche(nouvelleRecherche:Boolean=false)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise le Paginatedatagrid. Appel la méthode ListeFacturesService.getListeFacture(values:CriteresRechercheVo):void.
		 * </pre></p>
		 * 
		 * @param nouvelleRecherche : <code>Boolean</code>
		 * 
		 */
		public function recherche(nouvelleRecherche:Boolean=false):void
		{

			factureVisible(false);

			if (nouvelleRecherche)
			{
				myPaginatedatagrid.refreshPaginateDatagrid(true); // il faut remettre la pagination à l'état initial
				myPaginatedatagrid.currentIntervalle=new ItemIntervalleVO;
				myPaginatedatagrid.currentIntervalle.indexDepart=0;
				if (!comboOperateur.selectedItem)
				{
					comboOperateur.selectedItem.data=0;
				}
			}

			var searchable:String=mySearchPaginateDatagrid.parametresRechercheVO.SEARCH_TEXT;
			var orderable:String=mySearchPaginateDatagrid.parametresRechercheVO.ORDER_BY;

			if (!searchable)
			{
				searchable="NUMERO_FACTURE,";
			}

			if (!orderable)
			{
				orderable="NOM,ASC";
			}

			params.dateDebut=datedeb;
			params.dateFin=datefin;
			params.operateurId=comboOperateur.selectedItem.data;
			params.cle=searchable;
			params.tri=orderable;
			params.offset=myPaginatedatagrid.currentIntervalle.indexDepart;

			listeFacture.getListeFacture(params);

		}
		
		/**
		 * <b>Fonction <code>onIntervalleChangeHandler(evt:PaginateDatagridEvent)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Pagination du Paginatedatagrid.
		 * </pre></p>
		 * 
		 * @param evt : <code>PaginateDatagridEvent</code>
		 * 
		 */		
		public function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			factureVisible(false);

			if (myPaginatedatagrid.currentIntervalle.indexDepart == 0)
			{
				params.offset=1;
			}
			else
			{
				params.offset=myPaginatedatagrid.currentIntervalle.indexDepart;
			}

			listeFacture.getListeFacture(params);
		}
		
		/**
		 * <b>Fonction <code>formateDates(item:Object, column:DataGridColumn):String</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Formate la date.
		 * </pre></p>
		 * 
		 * @param item : <code>Object</code>
		 * @param column : <code>DataGridColumn</code>
		 * @return : <code>String</code> une date formatée
		 * 
		 */		
		protected function formateDates(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField] as Date);
			}
			else
				return "-";
		}
		
		/**
		 * <b>Fonction <code>formateEuros(item:Object, column:DataGridColumn):String</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Ajoute l'unité monétaire.
		 * </pre></p>
		 * 
		 * @param item : <code>Object</code>
		 * @param column : <code>DataGridColumn</code>
		 * @return : <code>String</code> une somme monétaire
		 * 
		 */		
		protected function formateEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return moduleListeDesFacturesIHM.formatNumber(item[column.dataField], 2);
			else
				return "-";
		}
		
		/**
		 * <b>Fonction <code>numericSortFunction(itemA:Object, itemB:Object):int</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Ordonner la valeur monétaire.
		 * </pre></p>
		 * 
		 * @param itemA : <code>Object</code>
		 * @param itemB : <code>Object</code>
		 * @return 
		 * 
		 */		
		protected function numericSortFunction(itemA:Object, itemB:Object):int
		{
			return ObjectUtil.numericCompare(itemA.MONTANT, itemB.MONTANT);
		}
		
		/**
		 * <b>Fonction <code>getFacture(event:Event)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Donne accés à la facture.
		 * </pre></p>
		 * 
		 * @param event : <code>click</code>
		 * 
		 */		
		public function getFacture(event:Event):void
		{
			if (myPaginatedatagrid.selectedItem)
			{
				var disHtml:DisplayFactureHTMLService = new DisplayFactureHTMLService(factureHTML);				
				displayer = new DisplayFactureService(disHtml);
				displayer.displayFacture();			
			}
		}
		
		/**
		 * <b>Fonction <code>getFacture(event:Event)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Donne accés à l'annexe de la facture.
		 * </pre></p>
		 * 
		 * @param event : <code>click</code>
		 * 
		 */	
		public function getFactureAnnexe(event:Event):void
		{
			if (myPaginatedatagrid.selectedItem)
			{
				var disAnnexeHtml:DisplayFactureAnnexeHTMLService = new DisplayFactureAnnexeHTMLService(factureHTML);
				displayer = new DisplayFactureService(disAnnexeHtml);
				displayer.displayFacture();	
			}
		}
		
		/**
		 * <b>Fonction <code>getFacture(event:Event)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Exporter la liste des factures séléctionnée en format XLS, CSV ou PDF.
		 * </pre></p>
		 * 
		 * @param format : <code>String</code> le format d'export XLS, CSV ou PDF
		 * 
		 */	
		public function exportListeFactures(format:String):void
		{
			
			var nbTotal:Number =listeFacture.model.nbTotalItem + 1;
			
			if(nbTotal<ndItemMax){
				export.ExportsListeFactures(format,_moisDeb,_moisFin,nbTotal,params);
			}
			else{
				Alert.show(ResourceManager.getInstance().getString('M36', 'le_nombre_d_enregistrement_est_trop__lev'),ResourceManager.getInstance().getString('M36', 'Attention__'),4,null,null,icon_warning);
			}
			
		}

	}
}