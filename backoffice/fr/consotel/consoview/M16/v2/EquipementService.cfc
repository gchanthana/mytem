<cfcomponent displayname="fr.consotel.consoview.M16.v2.EquipementService" output="false">

	<!--- FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT AINSI QUE LES ENGAGEMENTS--->

	<cffunction name="fournirTerminauxSimCardEngagements" access="remote" returntype="Struct" output="false" hint="FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT">
		<cfargument name="idpoolgestionnaire" 		required="true" type="Numeric"/>
		<cfargument name="idprofilequipement" 		required="true" type="Numeric"/>
		<cfargument name="idfournisseur" 			required="true" type="Numeric"/>
		<cfargument name="idoperateur"	 			required="true" type="Numeric"/>
		<cfargument name="segment" 					required="true" type="Numeric"/>
		<cfargument name="isnew" 					required="true" type="Numeric"/>
		<cfargument name="fromStock"				required="false" type="Numeric" default=1>

			<cfset equInfos = structnew()>

			<cfset idgestionnaire 	= session.user.clientaccessid>
			<cfset idracine 		= session.perimetre.id_groupe>
			<cfset idcodelangue 	= session.user.idglobalization>

			<cfthread action="run" name="engagements" myoperateur="#idoperateur#">

				<cfset engagementObj = createObject("component","fr.consotel.consoview.M16.v2.OperateurService")>

				<cfset engagements.RESULT = engagementObj.getInfosClientOperateurActuelV2(myoperateur)>

			</cfthread>

			<cfthread action="run" name="terminaux" myfromStock="#fromStock#" mygestionnaire="#idgestionnaire#" mypool="#idPoolGestionnaire#" myprofil="#idProfilEquipement#"
													myfournis="#idFournisseur#" mysegment="#segment#" mycodelang="#idcodelangue#" myracine="#idracine#">

				<cfset idtypeequipement = 0>

				<cfif mysegment GT 1>
					<cfset idtypeequipement = 0>
				</cfif>

				<cfset idcategorieequipement = 0>
				<cfset typefournis = 1>
				<cfset idgammefournis = 0>
				<cfset niveau = 1>
				<cfset clefrecherche = ''>

				<cfif myfromStock eq 1>
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SEARCHCATALOGUECLIENT_V7">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#mygestionnaire#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#mypool#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#myprofil#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idcategorieequipement#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idtypeequipement#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#myfournis#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idgammefournis#">
						<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefrecherche#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#myracine#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#mycodelang#">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#mysegment#">
						<cfprocresult name="p_retour">
					</cfstoredproc>
				<cfelse>
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M16.listeTermDispo">
						<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#mypool#">
						<cfprocresult name="p_retour">
					</cfstoredproc>
				</cfif>

				<cfquery name = "qMobile_tablets" dbtype="query">
				    select * from p_retour where IDTYPE_EQUIPEMENT in (70,2192,2292)
				</cfquery>
				<!--- <cfset terminaux.RESULT = fournirListeTerminaux(mypool, myprofil,  myfournis, mysegment, '')> --->
				<cfset terminaux.RESULT = qMobile_tablets>

			</cfthread>

			<cfthread action="run" name="simcard" myfournis="#idFournisseur#" myoperateur="#idoperateur#" myprofil="#idProfilEquipement#">

				<cfset simcard.RESULT = fournirCarteSim(myfournis, myoperateur, myprofil)>

			</cfthread>

			<cfthread action="join" name="engagements, terminaux, simcard" timeout="40000"/>

			<cfset equInfos.SIMCARD 	= cfthread.simcard.RESULT>
			<cfset equInfos.TERMINAUX 	= cfthread.terminaux.RESULT>
			<cfset equInfos.ENGAGEMENTS	= cfthread.engagements.RESULT>
			<cfset equInfos.ISNEWENGAG 	= isnew>

		<cfreturn equInfos>
	</cffunction>

	<!--- FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT --->

	<cffunction name="fournirListeEquipements" access="remote" returntype="Query" output="false" hint="FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT">
		<cfargument name="idGestionnaire" 			required="true" type="Numeric"/>
		<cfargument name="idPoolGestionnaire" 		required="true" type="Numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="Numeric"/>
		<cfargument name="idFournisseur" 			required="true" type="Numeric"/>
		<cfargument name="clefRecherche" 			required="true" type="String"/>
		<cfargument name="segment" 					required="true" type="Numeric"/>

			<cfset idCategorieEquipement = 0>
			<cfset idTypeEquipement = 0>
			<cfset typeFournis = 1>
			<cfset idGammeFournis = 0>
			<cfset niveau = 1>
			<cfset idcode_langue = SESSION.USER.IDGLOBALIZATION>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SEARCHCATALOGUECLIENT_V4">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#idPoolGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idProfilEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idCategorieEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idTypeEquipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idFournisseur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idGammeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefRecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#idcode_langue#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#segment#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIS LA LISTE DES TERMIANUX DU CATALOGUE CLIENT --->

	<cffunction name="fournirListeTerminaux" access="remote" returntype="Query" output="false" hint="FOURNIS LA LISTE DES TERMIANUX DU CATALOGUE CLIENT">
		<cfargument name="idpoolgestionnaire" 		required="true" type="Numeric"/>
		<cfargument name="idprofilequipement" 		required="true" type="Numeric"/>
		<cfargument name="idfournisseur" 			required="true" type="Numeric"/>
		<cfargument name="segment" 					required="true" type="Numeric"/>
		<cfargument name="clefrecherche" 			required="true" type="String"/>

			<cfset idtypeequipement = 70>
			<cfset idcategorieequipement = 0>
			<cfset typefournis = 1>
			<cfset idgammefournis = 0>
			<cfset niveau = 1>
			<cfset idcode_langue = session.user.idglobalization>
			<cfset idgestionnaire = session.user.clientaccessid>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SEARCHCATALOGUECLIENT_V6">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#idPoolGestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idprofilequipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idcategorieequipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idtypeequipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idfournisseur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idgammefournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefrecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#session.perimetre.id_groupe#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typefournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#idcode_langue#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#segment#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT --->

	<cffunction name="fournirListeEquipementsClient" access="remote" returntype="Array" output="false" hint="FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT">
		<cfargument name="idPoolGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" 		required="true" type="numeric"/>
		<cfargument name="idFournisseur" 			required="true" type="numeric"/>
		<cfargument name="segment" 					required="true" type="numeric"/>
		<cfargument name="clefRecherche" 			required="true" type="string"/>

			<cfset equipements = ArrayNew(1)>

			<cfthread action="run" name="terminaux" mypool="#idPoolGestionnaire#" myprofil="#idProfilEquipement#"
													myfournis="#idFournisseur#" mysegment="#segment#" myclef="#clefRecherche#">

				<cfset idTypeEquipement = 70>
				<cfset idCategorieEquipement = 0>
				<cfset typeFournis = 1>
				<cfset idGammeFournis = 0>
				<cfset niveau = 1>
				<cfset idcode_langue = session.user.idglobalization>
				<cfset idGestionnaire = session.user.clientaccessid>

				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SEARCHCATALOGUECLIENT_V4">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#myprofil#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idCategorieEquipement#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idTypeEquipement#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#myfournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idGammeFournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#myclef#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#idcode_langue#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#mysegment#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

				<cfset terminaux.RESULT = p_retour>

			</cfthread>

			<cfthread action="run" name="accessoires" mypool="#idPoolGestionnaire#" myprofil="#idProfilEquipement#"
														myfournis="#idFournisseur#" mysegment="#segment#" myclef="#clefRecherche#">

			    <cfset idTypeEquipement = 2191>
			    <cfset idCategorieEquipement = 0>
				<cfset typeFournis = 1>
				<cfset idGammeFournis = 0>
				<cfset niveau = 1>
				<cfset idcode_langue = session.user.idglobalization>
				<cfset idGestionnaire = session.user.clientaccessid>

				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.SEARCHCATALOGUECLIENT_V4">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idGestionnaire#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#mypool#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#myprofil#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idCategorieEquipement#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idTypeEquipement#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#myfournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idGammeFournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#myclef#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#SESSION.PERIMETRE.ID_GROUPE#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typeFournis#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#idcode_langue#">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#mysegment#">
					<cfprocresult name="p_retour">
				</cfstoredproc>

				<cfset accessoires.RESULT = p_retour>

			</cfthread>

			<cfthread action="join" name="terminaux,accessoires" timeout="40000"/>

			<cftry>

				<cfset ArrayAppend(equipements, cfthread.accessoires.RESULT)>

			<cfcatch>

				 <cfset ArrayAppend(equipements, ArrayNew(1))>

			</cfcatch>
			</cftry>


			<cftry>

				<cfset ArrayAppend(equipements, cfthread.terminaux.RESULT)>

			<cfcatch>

				 <cfset ArrayAppend(equipements, ArrayNew(1))>

			</cfcatch>
			</cftry>

			<!--- <cfset ArrayAppend(equipements, cfthread.accessoires.RESULT)>
			<cfset ArrayAppend(equipements, cfthread.terminaux.RESULT)> --->

		<cfreturn equipements>
	</cffunction>

	<cffunction name="fournirListeEquipementsClientV2" access="remote" returntype="Array" output="false" hint="FOURNIS LA LISTE DES EQUIPEMENTS DU CATALOGUE CLIENT">
		<cfargument name="idpoolgestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idprofilequipement" 		required="true" type="numeric"/>
		<cfargument name="idfournisseur" 			required="true" type="numeric"/>
		<cfargument name="segment" 					required="true" type="numeric"/>
		<cfargument name="clefrecherche" 			required="true" type="string"/>

			<cfset equipements = ArrayNew(1)>

			<cfset query_retour = ''>

			<cfset idtypeequipement = 0>
			<cfset idcategorieequipement = 0>
			<cfset typefournis = 1>
			<cfset idgammefournis = 0>
			<cfset niveau = 1>

			<cfset idcodelangue   = session.user.idglobalization>
			<cfset idgestionnaire = session.user.clientaccessid>
			<cfset idracine		  = session.perimetre.id_groupe>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.searchcatalogueclient_v7">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 			value="#idgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 					value="#idpoolgestionnaire#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 		value="#idprofilequipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idcategorieequipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idtypeequipement#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idfournisseur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idgammefournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine" 					value="#clefrecherche#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#idracine#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournis" 			value="#typefournis#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_niveau" 					value="#niveau#" null="true">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idcode_langue"			value="#idcodelangue#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#segment#">
				<cfprocresult name="query_retour">
			</cfstoredproc>

			<cfset ArrayAppend(equipements, query_retour)>

		<cfreturn equipements>
	</cffunction>

	<!--- FOURNIS LA CARTE SIM D'UN OPÉRATEUR SÉLECTIONNÉ (MOBILE) --->

	<cffunction name="fournirCarteSim" access="remote" returntype="query" output="false" hint="FOURNIS LA CARTE SIM D'UN OPÉRATEUR SÉLECTIONNÉ (MOBILE)">
		<cfargument name="idRevendeur" required="true"  type="numeric" displayname="ID REVENDEUR"/>
		<cfargument name="idOperateur" required="false" type="numeric" displayname="ID OPERATEUR"/>
		<cfargument name="idTypeCommande" required="false" type="numeric" displayname="ID OPERATEUR"/>

			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M16.GETSIMCARD">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idTypeCommande#">
				<cfprocresult name="p_result">
			</cfstoredproc>

		<cfreturn p_result>
	</cffunction>

	<!--- FOURNIS LA CARTE SIM D'UN OPÉRATEUR SÉLECTIONNÉ (MOBILE) --->

	<cffunction name="fournirTerminauxCarteSimEngagement" access="remote" returntype="Struct" output="false" hint="FOURNIS LA CARTE SIM D'UN OPÉRATEUR SÉLECTIONNÉ (MOBILE)">
		<cfargument name="idRevendeur" required="true"  type="numeric" displayname="ID REVENDEUR"/>
		<cfargument name="idOperateur" required="false" type="numeric" displayname="ID OPERATEUR"/>
		<cfargument name="idTypeCommande" required="false" type="numeric" displayname="ID OPERATEUR"/>

			<cfset equipementsAndInfos 				= structnew()>

			<!--- <cfthread action="run" name="terminaux" mypool="#idPoolGestionnaire#" myprofil="#idProfilEquipement#"
													myfournis="#idFournisseur#" mysegment="#segment#" myclef="#clefRecherche#">

				<cfset terminaux.EQUIPEMENTS = fournirListeEquipements(idGestionnaire, idpoolgestionnaire, idfournisseur, '', segment)>

			</cfthread>

			<cfthread action="run" name="cartesim" mypool="#idPoolGestionnaire#" myprofil="#idProfilEquipement#"
													myfournis="#idFournisseur#" mysegment="#segment#" myclef="#clefRecherche#">

				<cfset cartesim.CARTESIM = fournirCarteSim(idoperateur)>

			</cfthread>

			<cfthread action="run" name="engagements" mypool="#idPoolGestionnaire#" myprofil="#idProfilEquipement#"
													myfournis="#idFournisseur#" mysegment="#segment#" myclef="#clefRecherche#">

				<cfset infosEngagement = createObject("component","fr.consotel.consoview.M16.v2.OperateurService")>

				<cfset engagements.ENGAGEMENTS = infosEngagement.getInfosClientOperateurActuel(idoperateur)>

			</cfthread>

			<cfthread action="join" name="terminaux, cartesim, engagements" timeout="40000"/>

			<cfset equipementsAndInfos.TERMINAUX	= cfthread.terminaux.RESULT>
			<cfset equipementsAndInfos.CARTESIM 	= cfthread.cartesim.RESULT>
			<cfset equipementsAndInfos.ENGAGEMENTS 	= cfthread.engagements.RESULT> --->

		<cfreturn equipementsAndInfos>
	</cffunction>

	<!--- FOURNIS LA LISTE DE TOUS LES ACCESOIRES DU CATALOGUE CLIENT --->

	<cffunction name="fournirListeAccessoiresCatalogueClient" returntype="query" access="remote" hint="FOURNIS LA LISTE DE TOUS LES ACCESOIRES DU CATALOGUE CLIENT">
		<cfargument name="idRevendeur" 		required="true" type="numeric"  displayname="revendeur" hint="le revendeur"/>
		<cfargument name="idtypecommande" 	required="true" type="numeric" 	default=""/>
		<cfargument name="clef" 			required="true" type="string"   displayname="clef" 		hint="clef de recherche"/>
		<cfargument name="segment" 			required="true" type="numeric"  displayname="segment" 	hint="le filtre par segment"/>

			<cfset idcategorie_equipement = 0>
			<cfset idtype_equipement = 2191> <!--- Pour filtrer sur les accessoires --->
			<cfset idgamme_fournis = 0>
			<cfset idgroupe_client = SESSION.PERIMETRE.ID_GROUPE>
			<cfset type_fournisseur = 0>

			<cfif type_fournisseur eq 0>
				<cfset flag="true">
			<cfelse>
				<cfset flag="false">
			</cfif>

			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m16.searchcataloguerevendeur_V5">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcategorie_equipement" 	value="#idcategorie_equipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idtype_equipement" 		value="#idtype_equipement#" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idfournisseur" 			value="#idRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgamme_fournis" 			value="#idgamme_fournis#">
				<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" variable="p_chaine" 					value="#clef#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" 			value="#idgroupe_client#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_type_fournisseur" 		value="#type_fournisseur#" null="#flag#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" 					value="#segment#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_type_commande" 			value="#idtypecommande#">
				<cfprocresult name="p_result">
			</cfstoredproc>

		<cfreturn p_result>
	</cffunction>

	<!--- FOURNIS LA LISTE D'ACCESOIRES DU CATALOGUE CLIENT POUR UN TERMINAL SÉLECTIONNÉ--->

	<cffunction name="getProductRelated" access="remote" returntype="query" output="false" hint="FOURNIS LA LISTE D'ACCESOIRES DU CATALOGUE CLIENT POUR UN TERMINAL SÉLECTIONNÉ">
		<cfargument name="idequipement" 	required="true"  type="numeric" 	default=""/>
		<cfargument name="idtypecommande" 	required="true"  type="numeric" 	default=""/>
		<cfargument name="samerevendeur" 	required="true"  type="numeric" 	default=""/>
		<cfargument name="chaine" 			required="true"  type="string" 		default="null"/>
		<cfargument name="code_langue" 		required="false" type="string"/>


		<cfif isDefined("code_langue")>
			<cfset _code_langue = code_langue>
		<cfelse>
			<cfset _code_langue = session.user.globalization>
		</cfif>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.get_product_related_v4">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idequipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_chaine" 		value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_same_rev" 		value="#samerevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_code_langue" 	value="#_code_langue#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_type_commande" 	value="#idtypecommande#">
			<cfprocresult name="p_retour">
		</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIS LA FICHE DÉTAILLÉE D'UN TERMINAL OU D'UN ACCESSOIRE --->

	<cffunction name="getProductFeature" access="remote" returntype="array" output="false" hint="FOURNIS LA FICHE DÉTAILLÉE D'UN TERMINAL OU D'UN ACCESSOIRE">
		<cfargument name="idEquipement" required="true"  type="numeric" default=""/>
		<cfargument name="code_langue" 	required="false" type="string"/>

			<cfif isDefined("code_langue")>
				<cfset _code_langue = code_langue>
			<cfelse>
				<cfset _code_langue = SESSION.USER.GLOBALIZATION>
			</cfif>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.get_product_feature_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idequipement" 	value="#idEquipement#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 	variable="p_langid" 		value="#_code_langue#">
				<cfprocresult name="p_retour" 		resultset="1">
				<cfprocresult name="p_retour_photo" resultset="2">
			</cfstoredproc>

			<cfset result = arrayNew(1)>
			<cfset arrayAppend(result,p_retour)>
			<cfset arrayAppend(result,p_retour_photo)>

		<cfreturn result>
	</cffunction>

	<!--- MAJ DES CARACTERISTIQUES DES ARTICLES(N° DE SÉRIE, IMEI, SOUS TETE, ETC) --->

	<cffunction name="manageLivraisonEquipements" access="remote" returntype="Numeric" output="false" hint="MAJ DES CARACTERISTIQUES DES ARTICLES(N° DE SÉRIE, IMEI, SOUS TETE, ETC)">
		<cfargument name="articles" required="true" type="String"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.majEquipementLigne_v6" blockfactor="1">
				<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" 	variable="p_articles" value="#tostring(articles)#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

	<!--- FOURNIS LA CARTE SIM ASSOCIÉ A UNE SOUS TETE (MOBILE) --->

	<cffunction name="fournirAssociatesSim" access="remote" returntype="Array" output="false" hint="FOURNI LA CARTE SIM ASSOCIÉ A UNE SOUS TETE (MOBILE)">
		<cfargument name="idssoustete" 	required="true" type="array"/>
		<cfargument name="idpool" 		required="true" type="numeric"/>

			<cfset cartesSim = ArrayNew(1)>

 			<cfloop index="idx" from="1" to="#ArrayLen(idssoustete)#">
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getassociatesim_v2" blockfactor="1">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete"	value="#idssoustete[idx]#"/>
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 		value="#idpool#"/>
					<cfprocresult name="p_retour">
				</cfstoredproc>

				<cfset carteSim = structNew()>
				<cfset carteSim.IDSOUSTETE = idssoustete[idx]>
				<cfset carteSim.IDCARTESIM 	= p_retour.IDSIM>
				<cfset carteSim.NUMEROSIM 	= p_retour.NUM_SIM>
				<cfset carteSim.PUK 		= p_retour.S_PUK1>
				<cfset carteSim.PIN 		= p_retour.S_PIN1>
				<cfset ArrayAppend(cartesSim, carteSim)>
			</cfloop>

		<cfreturn cartesSim>
	</cffunction>

	<!--- MET A JOUR L'ÉTAT DE PARC DANS M11 (ASSOCIE/DISSOCIE LE MOBILE DE LA CARTE SIM, DÉSAFECTATION DU MOBILE DU COLLABORATEUR, MISE AU REBUT DE L'ANCIEN MOBILE) --->

	<cffunction name="desaffectesimterm" access="remote" returntype="Numeric" output="false" hint="MET A JOUR L'ÉTAT DE PARC DANS M11 (ASSOCIE/DISSOCIE LE MOBILE DE LA CARTE SIM, DÉSAFECTATION DU MOBILE DU COLLABORATEUR, MISE AU REBUT DE L'ANCIEN MOBILE)">
		<cfargument name="articles" required="true" type="Array" default=""/>

			<cfset var idgestionnaire = session.user.clientaccessid >
			<cfset rsltArticles = ArrayNew(1)>
			<cfset rslt 		= 1>
			<cfset checkOK 		= 1>

			<cfloop index="idx" from="1" to="#ArrayLen(articles)#">
				<cfif rslt GTE 1>
					<cfset idarticle = articles[idx].IDARTICLE>
					<cfset idsim	 = articles[idx].IDSIM>
					<cfset boolterm	 = articles[idx].BOOLTERM>
					<cfset boolcoll	 = articles[idx].BOOLCOLL>
					<cfset boolrebu	 = articles[idx].BOOLREBU>

					<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.desaffectesimterm_v3">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idarticle" 		value="#idarticle#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idsim" 			value="#idsim#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_bool_terminal" 	value="#boolterm#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_bool_collab" 	value="#boolcoll#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_bool_rebut" 	value="#boolrebu#">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_iduser" 		value="#idgestionnaire#">
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER"  variable="p_retour">
					</cfstoredproc>

					<cfif NOT p_retour GT 0>
						<cfset checkOK =  p_retour>
					</cfif>
				</cfif>
			</cfloop>

		<cfreturn checkOK>
	</cffunction>

	<!--- RECUPERE LA LISTE DES SIMS EN SPARE --->

	<cffunction name="fournirSimInSpare" access="remote" returntype="Query" output="false" hint="RECUPERE LA LISTE DES SIMS EN SPARE">
		<cfargument name="idoperateur" required="true" type="Numeric"/>
		<cfargument name="idpool" required="true" type="Numeric"/>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.getSimSpare_v2">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idoperateur#">
				<cfprocresult name="p_retour"/>
			</cfstoredproc>

		<cfreturn p_retour/>
	</cffunction>

</cfcomponent>
