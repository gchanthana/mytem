package service.diffusion.gethistoriqueenvoi
{
	import event.diffusion.DiffusionEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.diffusion.HistoriqueEnvoi;
	
	internal class GetHistoriqueEnvoiModel extends EventDispatcher
	{
		
		private var _historiqueEnvoi:ArrayCollection;
				
		public function GetHistoriqueEnvoiModel()
		{
			_historiqueEnvoi=new ArrayCollection();
		}

		public function get historiqueEnvoi():ArrayCollection
		{
			return _historiqueEnvoi;
		}

		internal function getHistoriqueEnvoi(value:ArrayCollection):void
		{
			_historiqueEnvoi.removeAll();
			
			var historique:HistoriqueEnvoi;
							
			if (value.length > 0)
			{
				for (var i:int=0; i < value.length; i++)
				{
					historique=new HistoriqueEnvoi();
					historique.fill(value[i]);
					_historiqueEnvoi.addItem(historique);
				}
			}
			dispatchEvent(new DiffusionEvent(DiffusionEvent.HISTORIQUE_ENVOI_EVENT));// capturé par listeDiffusionIHM
		}
	}
}
