package gestionparc.ihm.action.ligne.actionLigneResiliation
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.gestion.GestionCommandeMobile;

	public class ActionLigneResiliationReceiver
	{
		private var srcObject:Object;
		private var gestionCommmande:GestionCommandeMobile;

		public function ActionLigneResiliationReceiver()
		{
		}

		/**
		 * Cette fonction appelle l'affichage de l'IHM et renseigne au préalable les sources de l'action.
		 */
		public function action():void
		{
			setSources();
			displayIHM(srcObject);
		}

		/**
		 * Cette fonction permet d'afficher l'IHM permettant de faire l'action.
		 */
		private function displayIHM(srcObject:Object):void
		{
			gestionCommmande=new GestionCommandeMobile(srcObject);
			gestionCommmande.goResiliation();
		}

		/**
		 * Cette fonction renseigne les sources pour réaliser l'action.
		 */
		private function setSources():void
		{
			srcObject=new Object();
			srcObject.ligne=SpecificationVO.getInstance().elementDataGridSelected;
			//--- 1-> NOUVELLE LIGNE OU EQ (mobile, fixe, data), 2-> REENGAGEMENT, 3-> MODIFICATION OPTION, 4-> SUSPENSION, 	5-> REACTIVATION, 6-> RESILIATION
			srcObject.IDTYPECOMMANDE=6;
			srcObject.IDPOOL=SpecificationVO.getInstance().idPool;
			srcObject.IDPROFIL=SpecificationVO.getInstance().idProfil;
			srcObject.IDREVENDEUR=SpecificationVO.getInstance().idRevendeur;
			srcObject.LIBELLE_CONCAT=SpecificationVO.getInstance().labelPool + " - " + SpecificationVO.getInstance().labelProfil;
			srcObject.LIBELLE_POOL=SpecificationVO.getInstance().labelPool;
			srcObject.LIBELLE_PROFIL=SpecificationVO.getInstance().labelProfil;
			srcObject.BOOLNEWCOMMANDE=true;
			srcObject.IDSEGMENT=(SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE) ? 1 : 2;

		}
	}
}
