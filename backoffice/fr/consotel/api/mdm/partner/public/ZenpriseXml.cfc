<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseXml" extends="fr.consotel.api.mdm.partner.public.ZenpriseWhitelist" hint="Implémentation Xml Service">
	<!--- Xml Service --->
	<cffunction access="remote" name="addXMLFile" returntype="Void" hint="Ajoute un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="newFile" type="String" required="true" hint="Nom à donner au fichier">
		<cfargument name="fileText" type="String" required="true" hint="Contenu du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaires à associer au fichier">
		<cfset var xmlService=getManagementService().getXmlService()>
		<cfset xmlService.addXMLFile(ARGUMENTS["osFamily"],ARGUMENTS["newFile"],ARGUMENTS["fileText"],ARGUMENTS["comment"])>
	</cffunction>
	
	<cffunction access="remote" name="updateXMLFile" returntype="Void" hint="Met à jour un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom actuel du fichier">
		<cfargument name="newFile" type="String" required="true" hint="Nom à donner au fichier ou une chaine vide pour garder le nom fileName">
		<cfargument name="fileText" type="String" required="true" hint="Contenu du fichier">
		<cfargument name="comment" type="String" required="true" hint="Commentaires à associer au fichier">
		<cfset var xmlService=getManagementService().getXmlService()>
		<cfset xmlService.updateXMLFile(ARGUMENTS["osFamily"],ARGUMENTS["fileName"],ARGUMENTS["newFile"],ARGUMENTS["fileText"],ARGUMENTS["comment"])>
	</cffunction>
	
	<cffunction access="remote" name="removeXMLFile" returntype="Void" hint="Supprime un fichier XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier à supprimer">
		<cfset var xmlService=getManagementService().getXmlService()>
		<cfset xmlService.removeXMLFile(ARGUMENTS["osFamily"],ARGUMENTS["fileName"])>
	</cffunction>
	
	<cffunction access="remote" name="getXMLFileInfo" returntype="String"
	hint="Retourne les infos du fichier dans un tableau contenant les éléments suivants dans l'ordre : Nom, Contenu, Commentaires, Date de création ou d'upload, Date de modification">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier à supprimer">
		<cfset var xmlService=getManagementService().getXmlService()>
		<cfset var resultList=xmlService.getXMLFileInfo(ARGUMENTS["osFamily"],ARGUMENTS["fileName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getXMLFileList" returntype="String" hint="Retourne la liste des fichier XML dans un tableau">
		<cfset var xmlService=getManagementService().getXmlService()>
		<cfset var resultList=xmlService.getXMLFileList()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="validateXML" returntype="String" hint="Retourne TRUE si le contenu XML est lève une exception sinon">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileText" type="String" required="true" hint="Contenu à valider">
		<cfset var xmlService=getManagementService().getXmlService()>
		<cfset var boolResult=xmlService.validateXML(ARGUMENTS["osFamily"],ARGUMENTS["fileText"])>
		<cfreturn serializeJSON(boolResult)>
	</cffunction>
</cfcomponent>