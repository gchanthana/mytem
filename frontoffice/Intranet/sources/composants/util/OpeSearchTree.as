package composants.util
{
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.controls.Tree;
	import mx.collections.ArrayCollection;
	import mx.managers.CursorManager;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import composants.tb.recherche.renderer.TIRFolderLines;
	import mx.core.ClassFactory;
	import univers.UniversManager;
	import mx.utils.ObjectUtil;
	
	public class OpeSearchTree extends SearchTreeIHM
	{
		
		private var currentSelectedIndex:int = 0; // Index du groupe
		private var currentXmlResultIndex:int = 0; // Index de la recherche
		private var searchResult:int = 0; // Nombre de résultat de la recherche de noeuds
		
		[Bindable]
		private var datagridData : ArrayCollection = new ArrayCollection();// les donnees pour le DataGrid
		
		[Bindable]
		private var treeData : XMLList = new XMLList();//les donnees pour le Tree
		
		protected var xmlResult:XMLList = null;
		
		public function OpeSearchTree(){
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
				
		public function initIHM(e:FlexEvent):void {
			myTree.labelField="@LABEL";
			initDp(0);
			nodeSearchItem.addEventListener(MouseEvent.CLICK,searchNode);
			previousItem.toolTip = "Précédent\n(Flèche du Haut)";
			previousItem.addEventListener(MouseEvent.CLICK,previousSearchNode);
			nextItem.toolTip = "Suivant\n(Flèche du Bas)";
			nextItem.addEventListener(MouseEvent.CLICK,nextSearchNode);
			this.addEventListener(KeyboardEvent.KEY_DOWN,processKey);
			myTree.percentHeight=100;
			myTree.percentWidth=100;			
			disableSearch();
		}
		
	
		
		public function initDp(idop : int):void {
			clearSearch(null);
			var _treeNodes:XMLList = UniversManager.getListePerimetres() as XMLList;
			var value:int;
			var dept:XMLList;
			value = UniversManager.getPerimetre().PERIMETRE_INDEX;
	
			if (value == 1) {
				
				setDp(null);
				
				dept = _treeNodes.descendants().(@TYPE_ORGA=="OPE" && @OPERATEURID == idop);
				if (dept.length() == 0) disableSearch() else enabledSearch();
				trace("dept = " + ObjectUtil.toString(dept));
				setDp(dept[0]);
			} else {

				setDp(null);
				dept = _treeNodes.descendants().(@NODE_ID.toString()==value.toString());
				setDp(dept[0]);
			}
		}
		
		public function setDp(xml:Object):void {
			myTree.dataProvider = null;
			myTree.dataProvider = new XMLList(xml);
		}
		
		private function processKey(event:KeyboardEvent):void {
	      	if((event.keyCode == 13)) { // 38 et 40
		      	searchNode(null);
	      	} else if((nextItem.enabled) && (event.keyCode == 40)) {
					nextSearchNode(null);
	      	} else if((previousItem.enabled) && (event.keyCode == 38)) {
					previousSearchNode(null);
	      	}
		}
		
		/**
		 * Filtre l'objet nodeItem (XML ou XMLList) en renvoyant tous les noeuds dont
		 * l'attribut attributeName contient le mot clé keyword dans sa valeur.
		 * */
		public function xmlSearch(nodeItem:Object,attributeName:String,keyword:String):XMLList {
			return nodeItem.(@[attributeName].toString().toLowerCase().search(keyword.toLowerCase()) > -1);
		}
		
		/**
		 *  Recherche le ou les noeuds contenant le mot clé dans la zone de texte searchInput.
		 **/
		private function searchNode(event:Event):void {			
			myTree.selectedIndex = -1;
			if (myTree.selectedIndex >= 0) {
				closeTreeNodes(myTree.selectedItem);	
			}
			currentXmlResultIndex = 0;
			CursorManager.setBusyCursor();
			//searchResult = myTree.searchNode(searchInput.text);
			var tmpIndex:int=0;
			var keyword:String=searchInput.text;
			var selectedObj:XML = null;
			var nbChild:int = 0;
			var childNodes:XMLList = null;
			var tmpParent:XML = null;
			myTree.selectedIndex = 0;
			selectedObj = myTree.selectedItem as XML;
			/*if(myTree.selectedItem == null) { // Aucun noeud séléctionné
				myTree.selectedIndex = 0;
				selectedObj = myTree.selectedItem as XML;
			}
			*/
			childNodes = myTree.selectedItem.descendants();
			xmlResult = xmlSearch(childNodes,"LABEL",keyword);
			if(xmlResult.length() > 0) {
				searchResult = xmlResult.length();
			} else {
				xmlResult = null;
				searchResult = 0;
			}
			
			if(searchResult == 1) {
				currentXmlResultIndex =currentXmlResultIndex +1
				tmpIndex = currentXmlResultIndex;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			} else if(searchResult > 1) {
				bxArrow.visible=true;
				currentXmlResultIndex =currentXmlResultIndex+1;
				tmpIndex = currentXmlResultIndex;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			} else { // Aucun résultat
				bxArrow.visible=false;
				tmpIndex = 0;
			}
			searchIndex.text = tmpIndex + "/" + searchResult;
			CursorManager.removeBusyCursor();
		}
		
		/**
		 * Séléctionne le résultat précédent dans la recherche.
		 * */
		private function previousSearchNode(event:Event):void {
			if(currentXmlResultIndex > 1 && searchResult!=0) {
				currentXmlResultIndex = currentXmlResultIndex - 1;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
				
			}else if(currentXmlResultIndex == 1 && searchResult!=0) {
				currentXmlResultIndex = searchResult;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			}
		}

		/**
		 * Séléctionne le résultat suivant dans la recherche.
		 * */
		private function nextSearchNode(event:Event):void {
			//Alert.show(currentXmlResultIndex+":"+searchResult);
			if(currentXmlResultIndex < searchResult && searchResult!=0) {
				currentXmlResultIndex = currentXmlResultIndex + 1;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			} else if(currentXmlResultIndex == searchResult && searchResult!=0) {
				currentXmlResultIndex = 1;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			}
		}
		
		/**
		 * Déroule ou ferme suivant la valeur du paramètre open, tous les noeuds
		 * parents du noeud nodeItem (XML) passé en paramètre jusqu'à atteindre la racine.
		 * Puis renvoie le noeud passé en paramètre (nodeItem).
		 * */
		public function expandToNode(nodeItem:Object,open:Boolean):Object {
			var tmpParent:XML = nodeItem.parent() as XML;
			while(tmpParent != null) {
				myTree.expandItem(tmpParent,open);
				tmpParent = tmpParent.parent() as XML;
			}
			return nodeItem;
		}
		
		public function closeTreeNodes(nodeItem:Object):Object {
			var tmpParent:XML = nodeItem.parent() as XML;
			while(tmpParent != null) {
				myTree.expandItem(tmpParent,false);
				tmpParent = tmpParent.parent() as XML;
			}
			return nodeItem;
		}
		
		/**
		 * Séléctionne le noeud (XML) passé en paramètre, puis positionne la
		 * scroll bar à l'index correspondant si le paramètre scrollTo vaut true.
		 * */
		public function selectGivenNode(nodeItem:Object,scrollTo:Boolean):void {
			myTree.selectedItem = nodeItem;
			myTree.dispatchEvent(new Event(Event.CHANGE));
			if(scrollTo) {
				myTree.scrollToIndex(myTree.selectedIndex);
			}
		}
		
		/**
		 * Efface la dernière recherche effectuée.
		 * */
		private function clearSearch(event:Event):void {
			bxArrow.visible=false;
			searchInput.text="";
			nextItem.enabled = previousItem.enabled = false;
			currentXmlResultIndex = searchResult = 0;
			searchIndex.text = currentXmlResultIndex + "/" + searchResult;
			xmlResult = null;
		}
		/**
		 * Active la recherche 
		 * */
		public function enabledSearch():void{
			searchInput.enabled = true;
			nodeSearchItem.enabled = true;
			nodeSearchItem.visible = true;
			myTree.enabled = true;
			myTree.doubleClickEnabled =true;
		}
		
		/**
		 * Desactive la recherche
		 * */
		public function disableSearch():void{
			searchInput.enabled = false;
			nodeSearchItem.enabled = false;
			nodeSearchItem.visible = false;
			myTree.enabled = false;			
			myTree.doubleClickEnabled = false;
		}
	}
}