<cfoutput >RECUPERATION DES FICHIERS DANS LA TABLE CONTAINER<br><br></cfoutput>
<cfquery datasource="ROCOFFRE" result="rslt" name="result">
	SELECT UUID FROM detail_container
</cfquery>
<cfdump var="#rslt#"/>
<cfdump var="#result#"/>

<cfif rslt.recordcount gt 0>
	<cfoutput ><br><br>RECUPERATION DE LA LISTE DES FICHIERS DU FTP CONTAINER<br></cfoutput>
	<cfdirectory action="list" directory="/container/" name="listDir" listInfo="all">
	<cfoutput ><br>Liste des fichiers : </cfoutput>
	<cfdump var="#listDir#">
	 
	<cfif listDir.recordcount gt 0>
		<cfloop query="listDir">
			<cfset boolExist = false>
			<cfif ucase(#listDir.type#) eq 'DIR'>
				<cfif ucase(#listDir.name#) neq 'LIENS'>
					<cfoutput >Le répertoire [ #listDir.name# - taille  : #listDir.size# - type  : #listDir.type# ] a été supprimé<br></cfoutput>
					<cfdirectory action="delete" directory="#listDir.DIRECTORY#/#listDir.NAME#" recurse="true">
				</cfif>
			<cfelseif ucase(#listDir.type#) eq 'FILE'>
				<cfloop query="result">
					<cfif ucase(#result.UUID#) eq ucase(#listDir.name#)>
						<cfset boolExist = true>
 						<cfbreak> 
					</cfif>
			    </cfloop> 
				<cfif boolExist eq false>
					<cffile action="delete" file="/container/#listDir.name#">
					<cfoutput>Le fichier #listDir.name# a été supprimé<br></cfoutput>
				</cfif>
			</cfif>
		</cfloop>
	</cfif>			
	<cfoutput><br><br></cfoutput>
</cfif>
