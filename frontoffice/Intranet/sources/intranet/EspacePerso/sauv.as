package intranet.EspacePerso
{
	import mx.events.FlexEvent;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import mx.collections.ArrayCollection;
	import mx.formatters.SwitchSymbolFormatter;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLLoader;
	import appli.Login;
	import flash.events.KeyboardEvent;
	import flash.net.sendToURL;
	import mx.collections.XMLListCollection;
	import mx.events.MenuEvent;
	import intranet.EspacePerso.indexEspacePerso;
	
	public class sauv extends indexEspacePerso
	{
			

		
		public function Main()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			
			
			
		}

		protected function initIHM(event:Event):void {
			//logoffBtn.addEventListener(MouseEvent.CLICK,dispatchLogoff);
			btnProd_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCv2_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCv3_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnActifs_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnColt_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCtA_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCtO_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnWww_Prod.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnAdmin.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnBatch.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCopt.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnBri_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCed_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCt_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCv2_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnCv3_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnMed_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnMmc_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnSam_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnNia_dev.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			// Liens persos
			btnLink1.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnLink2.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnLink3.addEventListener(MouseEvent.CLICK,ouvrirUrl);
			btnLink4.addEventListener(MouseEvent.CLICK,processGz);
			btnTel.addEventListener(MouseEvent.CLICK,ouvrirSimpleUrl);
			txtNumber.addEventListener(KeyboardEvent.KEY_DOWN,processEnter);
			// Met a jour l'interface
			lbDesc.text=Intranet.mySession.description;
			lbNom.label=Intranet.mySession.nom;
			
			
		}
		
		private function dispatchLogoff(event:Event):void {
			dispatchEvent(new Event(Intranet.LOGOFF_EVENT));
		}
		
		private function ouvrirUrl(event:Event):void {
			switch (event.currentTarget.id) {
				// Dev
				case "btnCv3_dev":
					allerAurl("https://cv-dev.consotel.fr","/index.html","GET");
					break;
				case "btnCv2_dev":
					allerAurl("http://192.168.3.220","/processlogin.cfm","POST");
					break;
				case "btnMmc_dev":
					allerAurl("http://192.168.3.127","/index.html","GET");
					break;
				case "btnCt_dev":
					allerAurl("http://192.168.3.227","/index.cfm","POST");
					break;
				case "btnBri_dev":
					allerAurl("http://192.168.3.25:8080","","GET");
					break;
				case "btnCed_dev":
					allerAurl("http://192.168.3.35","","GET");
					break;
				case "btnMed_dev":
					allerAurl("http://192.168.3.30:8080","","GET");
					break;
				case "btnSam_dev":
					allerAurl("http://192.168.3.31:8080","","GET");
					break;
				case "btnNia_dev":
					allerAurl("http://192.168.3.124","","GET");
					break;
				// Production
				case "btnCv3_Prod":
					allerAurl("http://cv.consotel.fr","/index.html","GET");
					break;
				case "btnCv2_Prod":
					allerAurl("http://consoview.consotel.fr","/processlogin.cfm","POST");
					break;
				case "btnActifs_Prod":
					allerAurl("http://192.168.1.200","/processlogin.cfm","POST");
					break;
				case "btnColt_Prod":
					allerAurl("http://192.168.1.203","/index.cfm","POST");
					break;
				case "btnCtA_Prod":
					allerAurl("http://192.168.1.204","/index.cfm","POST");
					break;
				case "btnCtO_Prod":
					allerAurl("http://192.168.1.205","/index.cfm","POST");
					break;
				case "btnWww_Prod":
					allerAurl("http://192.168.1.201","","GET");
					break;
				case "btnMmc_Prod":
					allerAurl("mmc.consotel.fr","","GET");
					break;
				// Outils Internes
				case "btnProd_Prod":
					allerAurl("http://192.168.3.228","/index.cfm","POST");
					break;
				case "btnCopt":
					allerAurl("http://192.168.3.5","/index.cfm","POST");
					break;
				case "btnAdmin":
					allerAurl("http://192.168.3.222","/index.cfm","POST");
					break;
				case "btnBatch":
					allerAurl("http://192.168.3.125","/index.cfm","POST");
					break;
				// Liens persos
				case "btnLink1":
					allerAfichier("\\\\storage-1\\Racine\\IT");
					break;
				case "btnLink2":
					allerAfichier("\\\\dbcl2\\web_directory\\import\\factures\\Infofacture\\temp");
					break;
				case "btnLink3":
					allerAfichier("\\\\storage-1\\Racine\\Dossiers Clients\\2- En cours de traitement");
					break;
			}
		}
		
		private function ouvrirSimpleUrl(event:Event):void {
			switch (event.currentTarget.id) {
				// Paramètres personnels
				case "btnTel":
					allerAsimpleUrl(Intranet.mySession.urlTelephone,"GET");
					break;
			}
		}
		
		private function processEnter(event:KeyboardEvent):void {
			if (event.keyCode==13) {
				callNumber();
			}
		}
		
		private function allerAurl(adresse:String, page:String, methode:String):void {
			var url:String = adresse + page;
            var variables:URLVariables = new URLVariables();
            if (methode=="POST") {
            	variables.code=Intranet.mySession.code;
            	variables.id=Intranet.mySession.id;
            	variables.logon=Intranet.mySession.logon;
            	variables.username=Intranet.mySession.username;
            }
            var request:URLRequest = new URLRequest(url);
            request.method=methode;
            request.data=variables;
            navigateToURL(request,"_blank");
		}
		
		private function allerAsimpleUrl(adresse:String, methode:String):void {
			var url:String = "http://" + adresse;
            var variables:URLVariables = new URLVariables();
            if (methode=="POST") {
            	variables.code=Intranet.mySession.code;
            	variables.id=Intranet.mySession.id;
            	variables.logon=Intranet.mySession.logon;
            	variables.username=Intranet.mySession.username;
            	request.data=variables;
            }
            var request:URLRequest = new URLRequest(url);
            request.method=methode;
            navigateToURL(request,"_blank");
		}
		
		private function allerAfichier(adresse:String):void {
			var url:String = adresse;
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            navigateToURL(request,"_blank");
		}
		
		private function callNumber():void {
			var url:String = "http://" + Intranet.mySession.urlTelephone + "/command.htm";
            var variables:URLVariables = new URLVariables();
           	variables.number=txtNumber.text;
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            request.data=variables;
            sendToURL(request);
		}
		
		private function processGz(event:MouseEvent):void {
			var url:String = "http://192.168.3.125/processGz.cfm";
            var variables:URLVariables = new URLVariables();
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            request.data=variables;
            sendToURL(request);
            Alert.show("Fichiers en cours de traitement");
		}
	
	}
}
}