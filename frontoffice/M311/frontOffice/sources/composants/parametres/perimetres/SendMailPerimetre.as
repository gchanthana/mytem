package composants.parametres.perimetres
{
	import mx.resources.ResourceManager;
	import composants.util.SendMail;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	import mx.containers.TitleWindow;
	import composants.mail.MailVO;
	import flash.events.MouseEvent;
	import mx.controls.Button;
	import mx.controls.RichTextEditor;
	import mx.controls.TextInput;
	import mx.controls.Label;
	import mx.controls.CheckBox;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	 
	import composants.parametres.perimetres.gabarits.GabaritAffectations;
	import composants.parametres.perimetres.gabarits.AbstractGabarit;
	import composants.util.ConsoviewFormatter;
	import mx.controls.TextArea;


	
	
	
	//---- EVENTS  ---------------------------------------------------
	[Event(name='mail_sent')]
	//--- FIN EVENTS --------------------------------------------------
	
	
	
	
	
	
	[Bindable]
	public class SendMailPerimetre extends TitleWindow
	{	
		
		//--- VARS -------------------------------------------------
		
		//composants
		public var btnClose : Button;
		public var btnEnvoyer : Button;
		public var rteMessage : TextArea;
		
		public var txtExpediteur : Label;
		public var txtDest : Label;
		public var txtModule : Label;
		public var txtcc : TextInput;
		public var txtcci : TextInput;
		public var txtSujet : TextInput;
		public var cbCopie : CheckBox;
		public var cbCopieOperateur : CheckBox;		
		//fin composants
		
		//Les infos pour le mask du mail
		protected var _infosObject : Object;	
		//Le mail
		protected var _mail : MailVO;	

		//--- FIN VARS ---------------------------------------------
		
		
		
		//--- CONSTANTES --------------------------------------------
		public static const MAIL_ENVOYE : String = 'mail_sent';				
		//---- FIN CONSTANTES ----------------------------------------
		
		
		
		
		
		
		
		//--- HANDLERS ----------------------------------------------
		protected function creationCompleteHandler(event:FlexEvent):void {
			PopUpManager.centerPopUp(this);
		}
		
		protected function closeEventHandler(ce : CloseEvent):void {
			PopUpManager.removePopUp(this);
		}
		
		protected function btnCloseClickHandler(me : MouseEvent):void {
			PopUpManager.removePopUp(this);
		}
		protected function btnEnvoyerClickHandler(me : MouseEvent):void{
			sendTheMail();	
		}
		//--- FIN HANDLERS -------------------------------------------
		
		
		
		
		
		
		
		
		//-------- PUBLIC -------------------------------------------
		
		//Les infos pour le corps du mail
		public function set infosObject(infos : Object):void{
			_infosObject = infos;
			configRteMessage();
			
		}
		public function get infosObject():Object{
			return _infosObject;
		}
		
		
		
		//initialisation des donnees du mail
		public function initMail(module:String,sujet:String,dest:String):void {
			
			txtModule.text=module;
			txtSujet.text=sujet; 
			txtExpediteur.text=CvAccessManager.getSession().USER.PRENOM + " " +
					CvAccessManager.getSession().USER.NOM;								
			txtDest.text = dest;
			
			_mail = new MailVO();
			_mail.expediteur = CvAccessManager.getSession().USER.EMAIL;
			_mail.destinataire = dest;
		}
		//------ FIN PUBLIC -----------------------------------------
		
		
		
		
		
		
		
		//-------- PROTECTED -------------------------------------------
		override protected function commitProperties():void{
			super.commitProperties();
			
			addEventListener(CloseEvent.CLOSE,closeEventHandler);
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			
			btnClose.addEventListener(MouseEvent.CLICK,btnCloseClickHandler);
			btnEnvoyer.addEventListener(MouseEvent.CLICK,btnEnvoyerClickHandler);
						
			rteMessage.styleName="TextInputAccueil";
			//rteMessage.percentHeight=100;			
			//rteMessage.showControlBar = false;	
		}
		
		
		//Envoi du message
		protected function sendTheMail():void {
						
			_mail.cc = txtcc.text;
			_mail.bcc = txtcci.text;
			_mail.module = txtModule.text;			
			_mail.copiePourExpediteur = (cbCopie.selected)?"YES":"NO";
			_mail.copiePourOperateur = (cbCopieOperateur.selected)?"YES":"NO";			
			_mail.sujet = txtSujet.text;
			_mail.message = rteMessage.htmlText;
//			rteMessage.fontSizeCombo.selectedIndex = 1;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.parametres.perimetres.mail.MailSender",
								"envoyer",
								sendTheMailResultHandler);			
								
			RemoteObjectUtil.callService(op,_mail);		
		}
		
		
		
		//Affichage du corps du message
		protected function configRteMessage():void{
			var raison_sociale : String = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
			var gestionnaire : String = CvAccessManager.getSession().USER.PRENOM + " " + CvAccessManager.getSession().USER.NOM;
			var gestionnaireCible : String = infosObject.INFOS_DEST[0][0].PRENOM +" "+infosObject.INFOS_DEST[0][0].NOM;
			var listeLignes : String = printListeSousTete(infosObject.LIGNES);			
			
			
			rteMessage.htmlText =  
					ResourceManager.getInstance().getString('M311', '_p_Soci_t_____b_')+raison_sociale+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_Bonjour___p_')	
					+"<br/>"				
					+ResourceManager.getInstance().getString('M311', '_p_Les_lignes_ci_dessous_font_l_objet_d_')
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', 'Origine_des_lignes__br__')
					+ infosObject.INFOS_DEST[2][0].TYPE_NIVEAU + " : <b>" +infosObject.INFOS_DEST[2][0].LIBELLE_GROUPE_CLIENT  + ResourceManager.getInstance().getString('M311', '__b___Gestionnaire____b_') + gestionnaire + "</b>. <br/>"
					+ResourceManager.getInstance().getString('M311', 'Destination__br__') 
					+ infosObject.INFOS_DEST[1][0].TYPE_NIVEAU + " : <b>" +infosObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT  + ResourceManager.getInstance().getString('M311', '__b___Gestionnaire____b_') + gestionnaireCible + "</b>. <br/>"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+ResourceManager.getInstance().getString('M311', '_br___font_color___FF1200___b_Message___')+ gestionnaireCible +".</b></font>"
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_Merci_de_prendre_bonne_note_de_ce_tra')+ infosObject.INFOS_DEST[1][0].TYPE_NIVEAU + " " +infosObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT + ResourceManager.getInstance().getString('M311', '___b__par__b_') + gestionnaire + "</b></p>"
					+ResourceManager.getInstance().getString('M311', '_p_En_cas_d_erreur_merci_de_prendre_cont')+ gestionnaire + "</b> "+ CvAccessManager.getSession().USER.EMAIL + ".</p>"
					+"<br/>_______________________________________________________________________________________________"
					+ResourceManager.getInstance().getString('M311', '_br___font_color___FF1200___b_Message___')
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_1_____Merci_de_proc_der___la_correcti')+raison_sociale+ResourceManager.getInstance().getString('M311', '__b__de_ces_lignes__si_renseign_____p_')	
					+ResourceManager.getInstance().getString('M311', '_p_2_____Merci_de_proc_der_au_changement')	
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M311', '_p_Cordialement___p_')
					+"<br/>"
					+"<b>"+gestionnaire+ ".</b>"
					+"<br>"
					+"<b>"+raison_sociale+"</b>";
			updateDisplayList(unscaledHeight,unscaledWidth);
		}
		
		
		//------------- FIN PROTECTED -------------------------------------
		
		
		
		
		
		
		
		
		
		//------------- PRIVATE --------------------------------------------
		private function sendTheMailResultHandler(re : ResultEvent):void{	
			if (re.result > 0){
				dispatchEvent(new Event(MAIL_ENVOYE));
			}else{
				Alert.show(ResourceManager.getInstance().getString('M311', 'erreur_') + re.result , ResourceManager.getInstance().getString('M311', 'Erreur'));
			}
			PopUpManager.removePopUp(this);
		}
		
		private function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var titulaire: String = "";
					if (liste[i].COMMENTAIRES != null && String(liste[i].COMMENTAIRES).length > 0) titulaire = " (" + liste[i].COMMENTAIRES + ")";
					output = output + liste[i].LIBELLE_TYPE_LIGNE + " : <b>"+ ConsoviewFormatter.formatPhoneNumber(liste[i].SOUS_TETE) + titulaire  + ResourceManager.getInstance().getString('M311', '__b__t_t_t_tcompte_actuel_______________') + ResourceManager.getInstance().getString('M311', 'compte_cible_________________br__');
				}
			}
			return output;
		}
		//------------ FIN PRIVATE ---------------------------------------
		
		
	}
}