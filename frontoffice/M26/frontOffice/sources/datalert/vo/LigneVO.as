package datalert.vo
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import datalert.services.ActionMdmService;

	[Bindable]
	dynamic public class LigneVO extends EventDispatcher
	{
		private var _LIGNE:String=null;
		private var _NOM:String=null;
		private var _CURRENTDOMESTICVOLUMEDAY:Number=0;
		private var _CURRENTDOMESTICVOLUMEWEEK:Number=0;
		private var _CURRENTROAMINGVOLUMEDAY:Number=0;
		private var _CURRENTROAMINGVOLUMEWEEK:Number=0;
		private var _JOURFACTURATION:int;
		private var _DOMESTIQUEDATA:Number=0;
		private var _FORMATDOMESTIQUEDATA:Number=0;//0 => Mo, 1 => Go
		private var _DOMESTIQUEALERT:Number;
		private var _DOMESTIQUEALERTS:Object;//Contient les alertes dom après parsing du JSON
		private var _DOMESTIQUEPLAN:Number=0;
		private var _FORMATDOMESTIQUEPLAN:Number=0;//0 => Mo, 1 => Go
		private var _BOOLFAIRUSEDOM:int;
		private var _ROAMINGDATA:Number=0;
		private var _FORMATROAMINGDATA:Number=0;//0 => Mo, 1 => Go
		private var _ROAMINGALERT:Number;
		private var _ROAMINGALERTS:Object;//Contient les alertes roam après parsing du JSON
		private var _ROAMINGPLAN:Number=0;
		private var _FORMATROAMINGPLAN:Number=0;//0 => Mo, 1 => Go
		private var _LASTCONNEXION:String= null;
		private var _LASTAUTOREFRESH:String= null;
		private var _ROAMINGZONE:String;
		private var _OS:int;
		private var _BOOLDRT:Number;
		private var _IDSOUSTETE:Number;
		private var _COUNTRY:String="";
		private var _SELECTED:Boolean=false;
		private var _NBRECORD:Number;
		private var _LAST_ACTION:String ="";
		private var _BOOL_MDM:Number=0;
		private var _UUID:String="";
		private var _MDM_INFO:LigneMdmInfos;
		private var _mdmServerManager:ActionMdmService = new ActionMdmService();	
		
		public function LigneVO(target:IEventDispatcher = null) 
		{
			_MDM_INFO = new LigneMdmInfos(this);
		}
		
		[Bindable(event="MDM_INFOChange")]
		public function get MDM_INFO():LigneMdmInfos
		{	
			if(_MDM_INFO.bool_updated == false)
			{
				_mdmServerManager.loadMdmInfo(_MDM_INFO);
			}
			return _MDM_INFO;
		}

		public function get BOOLDRT():Number
		{
			return _BOOLDRT;
		}

		public function set BOOLDRT(value:Number):void
		{
			_BOOLDRT = value;
		}
		
		public function get IDSOUSTETE():Number
		{
			return _IDSOUSTETE;
		}
		
		public function set IDSOUSTETE(value:Number):void
		{
			_IDSOUSTETE = value;
		}

		public function set LIGNE(value:String):void
		{
			_LIGNE = value;
		}
		
		public function get LIGNE():String
		{
			return _LIGNE;
		}
		public function set COLLAB(value:String):void
		{
			_NOM = value;
		}
		
		public function get COLLAB():String
		{
			return _NOM;
		}

		public function set CURRENTDOMESTICVOLUMEDAY(value:Number):void
		{
			_CURRENTDOMESTICVOLUMEDAY = value;
		}
		public function get CURRENTDOMESTICVOLUMEDAY():Number
		{
			return _CURRENTDOMESTICVOLUMEDAY;
		}
		public function set CURRENTDOMESTICVOLUMEWEEK(value:Number):void
		{
			_CURRENTDOMESTICVOLUMEWEEK = value;
		}
		public function get CURRENTDOMESTICVOLUMEWEEK():Number
		{
			return _CURRENTDOMESTICVOLUMEWEEK;
		}
		
		public function set CURRENTROAMINGVOLUMEDAY(value:Number):void
		{
			_CURRENTROAMINGVOLUMEDAY = value;
		}
		public function get CURRENTROAMINGVOLUMEDAY():Number
		{
			return _CURRENTROAMINGVOLUMEDAY;
		}
		public function set CURRENTROAMINGVOLUMEWEEK(value:Number):void
		{
			_CURRENTROAMINGVOLUMEWEEK = value;
		}
		public function get CURRENTROAMINGVOLUMEWEEK():Number
		{
			return _CURRENTROAMINGVOLUMEWEEK;
		}
		public function set JOURFACTURATION(value:Number):void
		{
			_JOURFACTURATION = value;
		}
		
		public function get JOURFACTURATION():Number
		{
			return _JOURFACTURATION;
		}
		
		public function set DOMESTIQUEDATA(value:Number):void
		{
			_DOMESTIQUEDATA = value;
		}
		
		public function get DOMESTIQUEDATA():Number
		{
			return _DOMESTIQUEDATA;
		}
		
		public function set FORMATDOMESTIQUEDATA(value:Number):void
		{
			_FORMATDOMESTIQUEDATA = value;
		}
		
		public function get FORMATDOMESTIQUEDATA():Number
		{
			return _FORMATDOMESTIQUEDATA;
		}
		
		public function set DOMESTIQUEPLAN(value:Number):void
		{
			_DOMESTIQUEPLAN = value;
		}
		
		public function get DOMESTIQUEPLAN():Number
		{
			return _DOMESTIQUEPLAN;
		}
		
		public function set FORMATDOMESTIQUEPLAN(value:Number):void
		{
			_FORMATDOMESTIQUEPLAN = value;
		}
		
		public function get FORMATDOMESTIQUEPLAN():Number
		{
			return _FORMATDOMESTIQUEPLAN;
		}
		
		public function set BOOLFAIRUSEDOM(value:Number):void
		{
			_BOOLFAIRUSEDOM = value;
		}
		
		public function get BOOLFAIRUSEDOM():Number
		{
			return _BOOLFAIRUSEDOM;
		}
		
		public function set DOMESTIQUEALERT(value:Number):void
		{
			_DOMESTIQUEALERT = value;
		}
		
		public function get DOMESTIQUEALERT():Number
		{
			return _DOMESTIQUEALERT;
		}
		
		public function set DOMESTIQUEALERTS(value:Object):void
		{
			_DOMESTIQUEALERTS = value;
		}
		
		public function get DOMESTIQUEALERTS():Object
		{
			return _DOMESTIQUEALERTS;
		}
		
		public function set ROAMINGDATA(value:Number):void
		{
			_ROAMINGDATA = value;
		}
		
		public function get ROAMINGDATA():Number
		{
			return _ROAMINGDATA;
		}
		
		public function set FORMATROAMINGDATA(value:Number):void
		{
			_FORMATROAMINGDATA = value;
		}
		
		public function get FORMATROAMINGDATA():Number
		{
			return _FORMATROAMINGDATA;
		}
		
		public function set ROAMINGPLAN(value:Number):void
		{
			_ROAMINGPLAN = value;
		}
		
		public function get ROAMINGPLAN():Number
		{
			return _ROAMINGPLAN;
		}
		
		public function set FORMATROAMINGPLAN(value:Number):void
		{
			_FORMATROAMINGPLAN = value;
		}
		
		public function get FORMATROAMINGPLAN():Number
		{
			return _FORMATROAMINGPLAN;
		}
		
		public function set ROAMINGALERT(value:Number):void
		{
			_ROAMINGALERT = value;
		}
		
		public function get ROAMINGALERT():Number
		{
			return _ROAMINGALERT;
		}
		
		public function set ROAMINGALERTS(value:Object):void
		{
			_ROAMINGALERTS = value;
		}
		
		public function get ROAMINGALERTS():Object
		{
			return _ROAMINGALERTS;
		}
		
		public function set LASTCONNEXION(value:String):void
		{
			_LASTCONNEXION = value;
		}
		
		public function get LASTCONNEXION():String
		{
			return _LASTCONNEXION;
		}
		
		public function set LASTAUTOREFRESH(value:String):void
		{
			_LASTAUTOREFRESH = value;
		}
		
		public function get LASTAUTOREFRESH():String
		{
			return _LASTAUTOREFRESH;
		}
		
		public function set ROAMINGZONE(value:String):void
		{
			_ROAMINGZONE = value;
		}
		
		public function get ROAMINGZONE():String
		{
			return _ROAMINGZONE;
		}
		
		public function set OS(value:Number):void
		{
			_OS = value;
		}
		
		public function get OS():Number
		{
			return _OS;
		}
		
			
		
		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}
		
		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}
		
		public function set NBRECORD(value:Number):void
		{
			_NBRECORD = value;
		}
		
		public function get NBRECORD():Number
		{
			return _NBRECORD;
		}
		
		public function set LAST_ACTION(value:String):void
		{
			_LAST_ACTION = value;
		}
		
		public function get LAST_ACTION():String
		{
			return _LAST_ACTION;
		}
		
		public function set BOOL_MDM(value:Number):void
		{
			_BOOL_MDM = value;
		}
		
		public function get BOOL_MDM():Number
		{
			return _BOOL_MDM;
		}
		
		public function set UUID(value:String):void
		{
			_UUID = value;
		}
		
		public function get UUID():String
		{
			return _UUID;
		}
		
		public function set COUNTRY(value:String):void
		{
			_COUNTRY = value;
		}
		
		public function get COUNTRY():String
		{
			return _COUNTRY;
		}
	}
}