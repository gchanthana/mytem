package ihm.destinataire.perimetre
{
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;

	public class SpecialTreeMain extends PerimetreTree_V2
	{
		private var _currentRollOverItem:Object;
		private var _dataArray:ArrayCollection;
		public var _lastSelectedItem:Number;
		
		public function SpecialTreeMain()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(event:Event):void
		{
			this.addEventListener(ListEvent.ITEM_ROLL_OVER, treeItemRollover);
			this.addEventListener(ListEvent.ITEM_ROLL_OUT, treeItemRollout);
			this.addEventListener(ListEvent.ITEM_CLICK, treeItemClick);
		}
		
		public function setContextMenu(data:ArrayCollection):void
		{
			_dataArray = data;
			var cm:ContextMenu = new ContextMenu();
			cm.hideBuiltInItems();
			
			for (var i:int = 0; i < data.length; i++)
			{
				var item:ContextMenuItem = new ContextMenuItem(data.getItemAt(i).label);
				if (data.getItemAt(i).separate == true)
					item.separatorBefore = true;				
				item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, data.getItemAt(i).callback);
				cm.customItems.push(item);
			}
			this.contextMenu = cm;
			
			cm.addEventListener(ContextMenuEvent.MENU_SELECT, displayContextOptions);
		}
		
		private function treeItemClick(event:ListEvent):void
		{
			_currentRollOverItem = this.selectedItem;	
		}
		private function treeItemRollover(evt:ListEvent):void
		{
			_currentRollOverItem = evt.itemRenderer.data;
		}
		
		private function treeItemRollout(event:ListEvent):void
		{
			_currentRollOverItem = null;
		}
		
		private function displayContextOptions(evt:Event):void
		{
			var activate:Boolean = true;
			if (_currentRollOverItem == null)
			{
				this.selectedIndex = -1;
				activate= false;
			}
			else
			{
				this.selectedItem = _currentRollOverItem;
				this.dispatchEvent(new ListEvent("change"));
				activate= true;
			}
			for (var i:int = 0; i < _dataArray.length; i++)
			{
				evt.currentTarget.customItems[i].enabled=activate;
				if (_dataArray[i].forceEnabled == true)
					evt.currentTarget.customItems[i].enabled= false;
			}
		}
	}
}