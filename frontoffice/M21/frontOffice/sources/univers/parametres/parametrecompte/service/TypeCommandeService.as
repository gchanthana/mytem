package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	import univers.parametres.parametrecompte.vo.TypeCommande;

	[Bindable]
	public class TypeCommandeService extends EventDispatcher
	{
		public var idPoolSelected : int = -1;
		public var col_typeCMD : ArrayCollection = new ArrayCollection();
		public var col_typeCMD_of_login : ArrayCollection = new ArrayCollection();
		public var tab_typeCMD_of_login: ArrayCollection = new ArrayCollection();
		
		public var col_pool_of_type_CMD : ArrayCollection = new ArrayCollection();
		public var col_all_typeCMD : ArrayCollection = new ArrayCollection();
		public var tab_idTypeCmd_of_pool: ArrayCollection = new ArrayCollection(); //Liste d'id type commande affectés au pool (ihm univers.parametres.parametrecompte.ihm.parametrePoolIHM)
		public var tab_idPool_of_typeCommande: ArrayCollection = new ArrayCollection();
		
		public var col_contrainte : ArrayCollection = new ArrayCollection();
		
		public function TypeCommandeService()
		{
			
		}
		public function getTypeCommandePool(groupeIndex:int,idRevendeur : int,idPool:int,addAllitem:Boolean = false):void
		{
			
			idPoolSelected = idPool;
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getTypeCommandePool",
																		getTypeCommandePool_handler,getTypeCommandePool_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex,idRevendeur,idPool,addAllitem);
		}
		public function listeContrainteSuppression(idTypeCOmmande:int):void
		{
			
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getContrainteOfTypeCommande",
																		listeContrainteSuppression_handler,listeContrainteSuppression_fault_handler);
			RemoteObjectUtil.callService(op,idTypeCOmmande);
		}
		private function listeContrainteSuppression_handler(evt : ResultEvent):void
		{
			col_contrainte = evt.result as ArrayCollection;
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.LISTE_CONTRAINTE_COMPLETE));
		}
		public function getTypeCommandeLogin(groupeIndex:int,idLogin:int):void
		{
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getTypeCommandeLogin",
																		getTypeCommandeLogin_handler,getTypeCommandeLogin_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex,idLogin);
		}
		private function getTypeCommandeLogin_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_typeCMD_of_login.removeAll();
			tab_typeCMD_of_login.removeAll();
			for(i=0; i < tmpArr.length;i++){	
				var  item : TypeCommande = new TypeCommande();
				item.idTypeCmd=tmpArr[i].IDPROFIL_EQUIPEMENT;
				item.libelleTypeCmd=tmpArr[i].LIBELLE_PROFIL;
				item.codeTypeCmd=tmpArr[i].CODE_INTERNE;
				if(tmpArr[i].SELECTED == '1')
				{
					item.boolSelected = true;
					tab_typeCMD_of_login.addItem(tmpArr[i].IDPROFIL_EQUIPEMENT);
				}
				col_typeCMD_of_login.addItem(item);
			}
		}
		public function getPoolOfTypeCommande(groupeIndex:int,idTypeCMD:int, idRevendeur : int):void
		{
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getPoolTypeCommandeDemo",
																		getPoolOfTypeCommande_handler,getPoolOfTypeCommande_fault_handler);
			RemoteObjectUtil.callService(op);
		}
		public function updateInfoTypeCommande(idTypecmd:int,libelle:String,code : String,com : String, fixe:int, mobile:int, data:int):void
		{
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateInfoTypeCommande",
																		updateInfoTypeCommande_handler,updateInfoTypeCommande_fault_handler);
			RemoteObjectUtil.callService(op,idTypecmd,libelle,code,com,fixe,mobile,data);
		}
	
		private function updateInfoTypeCommande_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.UPDATE_INFO_TYPE_CMD_COMPLETE,false,false,Number(re.result)));
			}
		}
		public function addTypeCommande(groupeIndex:int,libelle:String,code : String, com : String, fixe:int, mobile:int, data:int):void
		{
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"createTypeCommande",
																		addTypeCommande_handler,addTypeCommande_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex,libelle,code,com,fixe,mobile,data);
		}
		private function addTypeCommande_handler(re : ResultEvent):void
		{
			if(re.result>0)
			{
				dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.UPDATE_INFO_TYPE_CMD_COMPLETE,false,false,Number(re.result)));
			}
		}
		private function getPoolOfTypeCommande_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_pool_of_type_CMD.removeAll();//Vider la collection
			tab_idPool_of_typeCommande.removeAll();
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : PoolGestionnaire = new PoolGestionnaire();
				item.IDPool=tmpArr[i].ID;
				item.libelle_pool=tmpArr[i].LIBELLE;
				item.codeInterne_pool=tmpArr[i].CODE;
				/*item.commentaire_pool=tmpArr[i].COMMENTAIRES;
				item.IDPoolRevendeur=tmpArr[i].IDREVENDEUR;				
				item.nbGestionnaire=tmpArr[i].NB_GEST;
				item.nbTypeCmd=tmpArr[i].NB_TYPE_CDE;
				item.libelleRevendeur = tmpArr[i].NOM_FOURNISSEUR;*/
				
				if(tmpArr[i].SELECTED == '1')
				{
					item.boolSelected = true;
					tab_idPool_of_typeCommande.addItem(tmpArr[i].ID);
				}
				col_pool_of_type_CMD.addItem(item);
			}
		}
		public function getTypeCommande(groupeIndex:int):void
		{
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"getListeTypeCommande",
																		getTypeCommande_handler,getTypeCommande_fault_handler);
			//RemoteObjectUtil.callService(op,groupeIndex);
			RemoteObjectUtil.callService(op,groupeIndex);
		}
		public function removeTypeCommande(idTypeCommande:int):void
		{
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"removeTypeCommande",
																		removeTypeCommande_handler,removeTypeCommande_fault_handler);
			RemoteObjectUtil.callService(op,idTypeCommande);
		}
		private function removeTypeCommande_handler(re : ResultEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.REMOVE_COMPLETE));
		}
		private function getTypeCommande_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_all_typeCMD.removeAll();
			for(i=0; i < tmpArr.length;i++){	
				var  item : TypeCommande = new TypeCommande();
				item.idTypeCmd=tmpArr[i].IDPROFIL_EQUIPEMENT;
				item.libelleTypeCmd=tmpArr[i].LIBELLE_PROFIL;
				item.codeTypeCmd=tmpArr[i].CODE_INTERNE;
				item.nbPool = tmpArr[i].NB_POOL;
				item.nbGestionnaire = tmpArr[i].NB_GEST;
				item.commentaire = tmpArr[i].COMMENTAIRE
				if(tmpArr[i].SEGMENT_FIXE == 1)
					item.segmentFixe = true
				else
					item.segmentFixe = false
				if(tmpArr[i].SEGMENT_MOBILE == 1)
					item.segmentMobile = true
				else
					item.segmentMobile = false
				if(tmpArr[i].SEGMENT_DATA == 1)
					item.segmentReseau = true
				else
					item.segmentReseau = false
				col_all_typeCMD.addItem(item);
			}
		}
		public function updateTypeCommandeOfPool(idPool:int,tab_idTypeCOmmande:Array,idRevendeur : int,value : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateXTypeCMD",
																		updateTypeCommandeOfPool_handler,updateTypeCommandeOfPool_fault_handler);
			RemoteObjectUtil.callService(op,idPool,tab_idTypeCOmmande,idRevendeur,value);
		}
		public function updateTypeCommandeOfLogin(idLogin:int,tab_idTypeCOmmande:Array,value : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateXTypeCommandeOfLogin",
																		updateTypeCommandeOfPool_handler,updateTypeCommandeOfPool_fault_handler);
			RemoteObjectUtil.callService(op,idLogin,tab_idTypeCOmmande,value);
		}
		private function updateTypeCommandeOfPool_handler(re : ResultEvent):void
		{
			dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.UPDATE_TYPE_COMMANDE_LOGIN));		
		}
		private function getTypeCommandePool_handler (re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_typeCMD.removeAll();
			tab_idTypeCmd_of_pool.removeAll();
				
			var  item : TypeCommande;
			
			for(i=0; i < tmpArr.length;i++){
				item = new TypeCommande();
				item.idTypeCmd=tmpArr[i].IDTYPECMD;
				item.libelleTypeCmd=tmpArr[i].LIBELLETYPECMD;
				item.codeTypeCmd=tmpArr[i].CODE;
				
				if(tmpArr[i].SELECTED == '1')
				{
					item.isInPool = true;
					tab_idTypeCmd_of_pool.addItem(tmpArr[i].IDTYPECMD);
				}
				col_typeCMD.addItem(item);
			}
			dispatchEvent(new GestionPoolEvent(GestionPoolEvent.LISTE_TYPECMD_COMPLETE));			
		}
		private function liste_type_cmd_of_pool_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			for(var a :int = 0;a<col_typeCMD.length;a++)
			{
				for(i=0; i < tmpArr.length;i++){
					
					if(tmpArr[i].ID == col_typeCMD.getItemAt(a).idTypeCmd) // Si ce type de commande est dans la liste des type de cmd  du pool 
					{
						col_typeCMD.getItemAt(a).isInPool= true;
						tab_idTypeCmd_of_pool.addItem(tmpArr[i].ID);
					}
				}
			}
			col_typeCMD.refresh();
		}
		
		private var _listeTypeCommandeOfPool:ArrayCollection = new ArrayCollection();
		public function get listeTypeCommandeOfPool():ArrayCollection
		{
			return _listeTypeCommandeOfPool;
		}
		public function getTypeCommandeByPool(idPool:Number):void
		{
			_listeTypeCommandeOfPool.removeAll();
			
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
				"getTypeCommandeByPool",
				getTypeCommandeByPool_handler,_fault_handler);
			RemoteObjectUtil.callService(op,idPool);
		}
		
		private function getTypeCommandeByPool_handler(re:ResultEvent):void
		{
			var col:ArrayCollection = re.result as ArrayCollection;
			if(col)
			{	 
				_listeTypeCommandeOfPool.addAll(col);	 
			}
			
		}
		
		private function _fault_handler(fe:FaultEvent):void
		{	
		}
		
		
		private function listeContrainteSuppression_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeContrainteSuppression_fault_handler'+evt.toString());
		}
		private function updateTypeCommandeOfPool_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateTypeCommandeOfPool_fault_handler'+evt.toString());
		}
		private function getTypeCommandePool_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getTypeCommandePool_fault_handler'+evt.toString());
		}
		private function getTypeCommande_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getTypeCommande_fault_handler'+evt.toString());
		}
		private function getPoolOfTypeCommande_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getTypeCommande_fault_handler'+evt.toString());
		}
		private function updateInfoTypeCommande_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateInfoTypeCommande_handler'+evt.toString());
		}
		private function addTypeCommande_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateTypeCommande_fault_handler'+evt.toString());
		}
		private function getTypeCommandeLogin_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.getTypeCommandeLogin_fault_handler'+evt.toString());
		}
		private function removeTypeCommande_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.removeTypeCommande_fault_handler'+evt.toString());
		}
	}
}