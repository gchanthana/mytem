package commandemobile.itemRenderer
{
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	
	import gestioncommande.entity.Action;
	
	import mx.controls.Menu;
	import mx.core.Application;
	import mx.events.MenuEvent;

	public class CustomMenu2 extends Menu
	{
		private var codeItemMenu	:String;
		private var actionItemMenu	:Action;
		
		public function CustomMenu2()
		{
			super();
		}

		/**
		 * Fonction de création du menu
		 **/
		public function dataMenu(arr:Array):void
		{
			Menu.createMenu(null, arr, false);
		}		
		
		public static function createCustomMenu(parent:DisplayObjectContainer, mdp:Object, showRoot:Boolean = true):CustomMenu2
	    {
	        var menu:CustomMenu2 = new CustomMenu2();
		        menu.tabEnabled  = false;
		        menu.owner 		 = DisplayObjectContainer(Application.application);
		        menu.showRoot 	 = showRoot;
				
	        popUpMenu(menu, parent, mdp);
			
	        return menu;
	    }
		
		/**
		 * Fonction d'affichage du menu
		 **/
		public function showMenu(posX:Number, posY:Number):void
		{
			x = posX - width;
			y = posY;
			
			//---> écouteurs sur le menu
			addEventListener(MenuEvent.CHANGE, menuChangeEvent);
			addEventListener(MouseEvent.ROLL_OVER, menuRollEvent);
			addEventListener(MouseEvent.ROLL_OUT, menuRollEvent);
			
			
			show();//---> affichage du menu
		}
		
		/**
		 * Fonction de masquage du menu
		 **/
		public function maskMenu():void
		{
			hide();
			
			removeEventListener(MenuEvent.CHANGE, menuChangeEvent);
			removeEventListener(MouseEvent.ROLL_OVER, menuRollEvent);
			removeEventListener(MouseEvent.ROLL_OUT, menuRollEvent);
		}
	
		/**
		 * Fonction de sélection d'un item dans le menu
		 **/									
		private function menuChangeEvent(evt:MenuEvent):void
		{
			codeItemMenu 	= selectedItem.code;
			actionItemMenu 	= convertToAction(selectedItem);
			
			dispatchEvent(new CustomMenuEvent(CustomMenuEvent.MENU_ITEM_CLICK, true, codeItemMenu, actionItemMenu));
		}
		
		private function convertToAction(object:Object):Action
		{
			var action:Action;
			
			if(object.hasOwnProperty('IDACTION'))
			{
				action 						= new Action();
				action.CODE_ACTION			= object.CODE_ACTION;
				action.COMMENTAIRE_ACTION	= object.COMMENTAIRE_ACTION;
				action.DATE_ACTION			= object.DATE_ACTION;
				action.DEST					= object.DEST;
				action.EXP					= object.EXP;
				action.IDACTION				= object.IDACTION;
				action.IDETAT				= object.IDETAT;
				action.LIBELLE_ACTION		= object.LIBELLE_ACTION;
				action.LIBELLEETAT_ENGENDRE	= object.LIBELLEETAT_ENGENDRE;
				action.MESSAGE				= object.MESSAGE;
			}
			
			return action;
		}
		
		
		/**
		 * Fonction de roll sur le menu
		 **/
		private function menuRollEvent(evt:*):void
		{
			var type:String = evt.type;
			
			if(type == MouseEvent.ROLL_OVER) 
				dispatchEvent(new MenuRollEvent(MenuRollEvent.MENU_ROLL_OVER, true));
			else if(type == MouseEvent.ROLL_OUT)
			{
				dispatchEvent(new MenuRollEvent(MenuRollEvent.MENU_ROLL_OUT, true));
				maskMenu();
			}
		}			
	}
}