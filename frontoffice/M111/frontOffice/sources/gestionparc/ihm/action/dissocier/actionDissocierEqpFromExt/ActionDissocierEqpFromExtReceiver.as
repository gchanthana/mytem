package gestionparc.ihm.action.dissocier.actionDissocierEqpFromExt
{
	import gestionparc.ihm.question.extention.QuestionFicheDissocierEqpFromExt;

	public class ActionDissocierEqpFromExtReceiver
	{
		private var qn:QuestionFicheDissocierEqpFromExt;
		
		public function ActionDissocierEqpFromExtReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			qn = new QuestionFicheDissocierEqpFromExt();
		}
	}
}