package composants.access {
	import mx.events.FlexEvent;
	
	/*
	import mx.controls.Tree;
	import mx.events.FlexEvent;
	import flash.utils.getQualifiedClassName;
	import mx.collections.XMLListCollection;
	import flash.events.Event;
	import flash.errors.IllegalOperationError;
	*/

	public class GenericTree extends Tree {
		/*
		public static const TREE_READY:String = "TREE_READY";
		public static const NO_SEARCH_NEXT_OR_PREVIOUS_MSG:String = "Aucune recherche effectuée";
		private var resultAttributeSearch:XMLList = null;
		private var resultAttributeSearchIndex:int = -1;
		private var resultAttributeKeywordSearch:XMLList = null;
		private var resultAttributeKeywordSearchIndex:int = -1;
		*/

		public function GenericTree() {
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}

		protected function initIHM(event:FlexEvent):void {
			// Rien à effectuer ici pour l'instant...
		}

		public final function setDataProvider(dataProvider:Object,labelField:String):void {
			dataProvider = null;
			dataProvider = dataProvider;
			labelField = "@" + labelField;
			// Il faut attendre que l'arbre soit prêt à chaque fois que l'on change le dataProvider ! //
			callLater(treeDataProviderUpdated); 
			/*
			clearSearch(true); // Efface toutes les recherches précédentes
			clearSearch(false); // Efface toutes les recherches précédentes
			*/
		}

		private function treeDataProviderUpdated():void {

		}
		
		/*
		public function clearSearch(thatContains:Boolean):Boolean {
			if(thatContains) {
				resultAttributeKeywordSearch = null;
				resultAttributeKeywordSearchIndex = -1;
			} else {
				resultAttributeSearch = null;
				resultAttributeSearchIndex = -1;
			}
			return thatContains;
		}

		public function searchXmlAttribute(xmlNode:XML,attributeName:String,
														attributeValue:String,thatContains:Boolean,onlyRoot:Boolean):int {
			var tmpXmlListCollection:XMLListCollection = dataProvider as XMLListCollection;
			var resultRef:XMLList = null;
			clearSearch(thatContains); // On efface la recherche précédente qui a utilisé le même thatContains
			if(xmlNode == null) {
				xmlNode = tmpXmlListCollection.getItemAt(0) as XML; // On prend la racine
			}
			if(thatContains) { // Attribut contenant la valeur de attributevalue
				resultRef = xmlAttributeKeywordSearch(xmlNode,attributeName,attributeValue,onlyRoot);
				resultAttributeKeywordSearchIndex = 0; // Remise à 0 de l'index de recherche
			} else { // Attribut ayant la même valeur que attributevalue
				resultRef = xmlAttributeSearch(xmlNode,attributeName,attributeValue,onlyRoot);
				resultAttributeSearchIndex = 0; // Remise à 0 de l'index de recherche
			}
			if(resultRef.length() > 0) { // Si on a trouvé au moins 1 noeud
				selectNode(resultRef[0],true); // on séléctionne le noeud
				if(thatContains)
					resultAttributeKeywordSearchIndex = 0; // On est sur le 1er résultat
				else
					resultAttributeSearchIndex = 0; // On est sur le 1er résultat
			}
			return resultRef.length();
		}

		public function selectNextAttributeSearch(thatContains:Boolean):int {
			var tmpIndex:int = -1;
			if(thatContains) {
				if(resultAttributeKeywordSearch == null) { // Aucune recherche effectuée
					throw new IllegalOperationError(GenericTree.NO_SEARCH_NEXT_OR_PREVIOUS_MSG);
				} else {
					if((resultAttributeKeywordSearchIndex + 1) ==
										resultAttributeKeywordSearch.length()) // Si on était sur le dernier résultat
						resultAttributeKeywordSearchIndex = 0; // On revient sur le 1er résultat
					else
						resultAttributeKeywordSearchIndex++; // On incrémente l'index de la recherche
					tmpIndex = resultAttributeKeywordSearchIndex;
					selectNode(resultAttributeKeywordSearch[tmpIndex],true); // On séléctionne le noeud
					return (resultAttributeKeywordSearch.length() - 1) - tmpIndex;
				}
			} else {
				if(resultAttributeSearch == null) { // Aucune recherche effectuée
					throw new IllegalOperationError(GenericTree.NO_SEARCH_NEXT_OR_PREVIOUS_MSG);
				} else {
				if((resultAttributeSearchIndex + 1) ==
									resultAttributeSearch.length()) // Si on était sur le dernier résultat
					resultAttributeSearchIndex = 0; // On revient sur le 1er résultat
				else
					resultAttributeSearchIndex++; // On incrémente l'index de la recherche
				tmpIndex = resultAttributeSearchIndex;
				selectNode(resultAttributeSearch[tmpIndex],true); // On séléctionne le noeud
				return (resultAttributeSearch.length() - 1) - tmpIndex;
				}
			}
		}

		public function selectPreviousAttributeSearch(thatContains:Boolean):int {
			var tmpIndex:int = -1;
			if(thatContains) {
				if(resultAttributeKeywordSearch == null) { // Aucune recherche effectuée
					throw new IllegalOperationError(GenericTree.NO_SEARCH_NEXT_OR_PREVIOUS_MSG);
				} else {
					if(resultAttributeKeywordSearchIndex == 0) // Si on était sur le 1er résultat
						resultAttributeKeywordSearchIndex =
								resultAttributeKeywordSearch.length() - 1; // On revient sur le dernier résultat
					else
						resultAttributeKeywordSearchIndex--; // On décrémente l'index de la recherche
					tmpIndex = resultAttributeKeywordSearchIndex;
					selectNode(resultAttributeKeywordSearch[tmpIndex],true); // On séléctionne le noeud
				}
			} else {
				if(resultAttributeSearch == null) { // Aucune recherche effectuée
					throw new IllegalOperationError(GenericTree.NO_SEARCH_NEXT_OR_PREVIOUS_MSG);
				} else {
					if(resultAttributeSearchIndex == 0) // Si on était sur le 1er résultat
						resultAttributeSearchIndex =
								resultAttributeSearch.length() - 1; // On revient sur le dernier résultat
					else
						resultAttributeSearchIndex--; // On décrémente l'index de la recherche
					tmpIndex = resultAttributeSearchIndex;
					selectNode(resultAttributeSearch[tmpIndex],true); // On séléctionne le noeud
				}
			}
			return tmpIndex;
		}

		private function xmlAttributeSearch(xmlNode:XML,attributeName:String,attributeValue:String,onlyRoot:Boolean):XMLList {
			var tmpCollection:XMLListCollection = null;
			var resultRef:XMLList =
					xmlNode.(@[attributeName].toString().toLowerCase() == attributeValue);
			var childResultRef:XMLList =
					xmlNode.descendants().(@[attributeName].toString().toLowerCase() == attributeValue);
			if(onlyRoot == true) {
				if(resultRef.length() > 0) {
					resultAttributeSearch = resultRef;
					return resultRef;
				} else {
					resultAttributeSearch = childResultRef;
					return childResultRef;
				}
			} else {
				resultAttributeSearch = childResultRef;
				return resultAttributeSearch;
			}
		}

		private function xmlAttributeKeywordSearch(xmlNode:XML,attributeName:String,attributeKeyword:String,onlyRoot:Boolean):XMLList {
			var tmpCollection:XMLListCollection = null;
			var resultRef:XMLList =
					xmlNode.(@[attributeName].toString().toLowerCase().search(attributeKeyword.toLowerCase()) > -1);
			var childResultRef:XMLList =
					xmlNode.descendants().(@[attributeName].toString().toLowerCase().search(attributeKeyword.toLowerCase()) > -1);
			if(onlyRoot == true) {
				if(resultRef.length() > 0) {
					resultAttributeKeywordSearch  = resultRef;
					return resultRef;
				} else {
					resultAttributeKeywordSearch  = childResultRef;
					return childResultRef;
				}
			} else {
				resultAttributeKeywordSearch  = childResultRef;
				return childResultRef;
			}
		}

		private function selectNode(xmlNode:XML,openStatus:Boolean):Object {
			var parentNode:Object = null;
			parentNode = xmlNode.parent();
			while(parentNode != null) {
				var tmpParentNode:Object = parentNode;
				if(openStatus && (isItemOpen(tmpParentNode) == false)) {
					expandItem(tmpParentNode,openStatus);
				}
				parentNode = (tmpParentNode as XML).parent();
			}
			selectedItem = xmlNode;
			scrollToIndex(selectedIndex);
			return xmlNode;
		}
	}
}
