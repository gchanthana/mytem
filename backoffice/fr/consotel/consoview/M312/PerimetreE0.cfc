<cfcomponent output="false">


	<cfset dataSourceName = "BI_TEST">	
	<cfset query = "">	
	<cfset idtdb = -1>

	
<cffunction name="init" access="remote" returntype="Numeric" >
	<cfargument name="perimetre" type="string" required="false">
	<cftry>
    	<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
	   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
	    <cfelse>
	    	<cfset rsltIdOrga = zgetOrga()>
	   		<cfset idorga = Val(rsltIdOrga)>
	    </cfif>
	    <cfset SESSION.PERIMETRE.SELECTED_ID_ORGA = idorga>	    
		<cfset idCliche = zGetCliche(idorga)>
		<cfset SESSION.PERIMETRE.SELECTED_ID_CLICHE = idCliche>		
		<cfif comparenocase(perimetre,"SOUSTETE") eq 0>
			<cfset niveau 	= 'IDSOUS_TETE'>
			<cfset idniveau 	= 10>
		<cfelse>
			<cfset niveau 	= zgetNiveauPerimetre(SESSION.PERIMETRE.SELECTED_IDPERIMETRE)>
			<cfset idniveau 	= getIdNiveau(niveau)>
		</cfif>
		
		<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = niveau>
		<cfset SESSION.PERIMETRE.SELECTED_IDNIVEAU = idniveau>
		<cfreturn 1> 
   	<cfcatch type="Any">
    	<cfreturn -1>
    </cfcatch>	
    </cftry>
</cffunction>

<cffunction name="getPerimetreInfos" access="remote" returntype="Struct">
		<cfset SESSION.DATES = {}>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_m00.get_last_periode_complete">			
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qDATELASTPFGP">
		</cfstoredproc>
		
		<cflog text="qDATELASTPFGP.recordcount = #qDATELASTPFGP.recordcount#">
		<cfif qDATELASTPFGP.recordcount GT 0>
			<cfset SESSION.DATES.LASTPFGPLOAD = qDATELASTPFGP['LAST_MOIS'][1]>
			<cfset SESSION.DATES.LASTPFGPIDPERIODE = qDATELASTPFGP['LAST_IDPERIODE_MOIS'][1]>
		<cfelse>
			
			<cfquery datasource="#SESSION.OFFREDSN#" name="rsltIdPeriodeMois">
				SELECT CV_GET_IDPERIODE_MOIS('#lsDateFormat(SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN,'yyyy/mm/dd')#')  as IDPERIOD FROM DUAL
			</cfquery>
			<cfset SESSION.DATES.LASTPFGPLOAD = SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN>
			<cfset SESSION.DATES.LASTPFGPIDPERIODE = rsltIdPeriodeMois.IDPERIOD>
		</cfif>
		
	<cfset infos = structNew()>
	
	<cfset infos.DATE_PERIMETRE_COMPLET = SESSION.DATES.LASTPFGPLOAD/>	
	<cfset infos.ID_DATE_PERIMETRE_COMPLET = SESSION.DATES.LASTPFGPIDPERIODE/>
	
	<cfreturn infos>
</cffunction>
	
	
<!---  Récupération de la liste du detail produit alimentant un datagrid paginé   --->
<cffunction name="getPaginateDetailProduit" access="remote" returntype="query">
		
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="idproduit" type="numeric" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		<cfargument name="segment" type="string" required="false">
		<!--- Paramètres du paginate --->
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="LANG" type="string" required="false" default="#SESSION.USER.GLOBALIZATION#">
				
	    <cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
		
		<!--- récupération des paramètres de session --->
		<!--- racine master et racine --->
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idracine_master = #SESSION.PERIMETRE.IDRACINE_MASTER#>
		<!--- orga --->
		
		<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga = Val(rsltIdOrga)>
		</cfif>
		<cfset SESSION.PERIMETRE.SELECTED_ID_ORGA = idorga>
		
		<!--- cliché --->  
			<cfset idCliche = zGetCliche(idorga)>
			<cfset SESSION.PERIMETRE.SELECTED_ID_CLICHE = idCliche>			
			<cfif idCliche EQ "">
				<cfabort showerror="Aucun cliché disponible, merci de contacter le service client.">
			</cfif>
		
		<!--- niveau et perimetre --->
			<cfif compareNoCase(perimetre,'soustete') eq 0>
				<cfset niveau 	= 'IDSOUS_TETE'>
				<cfset idperimetre = numero>
			<cfelse>
				<cfset niveau 	= zgetNiveauPerimetre(SESSION.PERIMETRE.ID_PERIMETRE)>
				<cfset idperimetre = SESSION.PERIMETRE.ID_PERIMETRE>
			</cfif>
			<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = niveau>
		
		<!--- intervalle de date --->
			<cfset idDebut 	= zgetIdPeriodeMois(transformDate(datedeb))>	
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB = idDebut>
					
			<cfset idFin 	= zgetIdPeriodeMois(transformDate(datefin))>
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN = idFin>
		
		
	 		 
		<cfstoredproc datasource="E0" procedure="E0.pkg_M311.getPaginateDetailProduit">
		
		
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine_master#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idperimetre#" null="false">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#niveau#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idorga#" null="false">
	        <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idproduit#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDebut#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idFin#" null="false">
		
			<!--- Paramètres du paginate --->
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">														
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">				
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#LANG#" null="false">							
			<cfprocresult name="p_result">
									
		</cfstoredproc>
	
		<cfquery name="q2" dbtype="query">
			select 
			ROW_TOTAL as NBRECORD,
			C8 as SITE,
			C5 as SOUS_TETE,
			C6 as COMMENTAIRES,
			C17 as NBAPPELS ,			
			C15 as  QTE,
			C18 as  VOLUME,
			<!---C5 as LIGNE,--->
			C16  as  MONTANT  ,		
			C16_TOTAL as  TOTAL_MONTANT ,
			C18_TOTAL as  TOTAL_VOLUME,
			C15_TOTAL as  TOTAL_QUANTITE,
			C17_TOTAL as  TOTAL_NBAPPELS ,
			C14 as  NUMERO_FACTURE  ,
			C2 as  IDPRODUIT_CATALOGUE , 			
			C4 as  IDSOUS_TETE  , 
			C13 as  IDINVENTAIRE_PERIODE  						 
			from p_result			 
			</cfquery>
			
			
				
		<cfif structKeyExists(SESSION,"PARAMS_EXPORT")>
			<cfset structdelete(SESSION,"PARAMS_EXPORT")>
		</cfif>
		
		<cfset PARAMS_EXPORT = structnew()>
		
		<cfset PARAMS_EXPORT.IDRACINE_MASTER=idracine_master>
		<cfset PARAMS_EXPORT.IDRACINE=idRacine>
		<cfset PARAMS_EXPORT.IDPERIMETRE=idperimetre>
		<cfset PARAMS_EXPORT.NIVEAU=niveau>
		<cfset PARAMS_EXPORT.IDORGA=idorga>
	    <cfset PARAMS_EXPORT.IDPRODUIT = idProduit>
		<cfset PARAMS_EXPORT.IDDEBUT=idDebut>
		<cfset PARAMS_EXPORT.IDFIN=idFin>
		
		<cfset PARAMS_EXPORT.ORDER_BY_TEXTE_COLONNE = ORDER_BY_TEXTE_COLONNE>
		<cfset PARAMS_EXPORT.ORDER_BY_TEXTE = ORDER_BY_TEXTE>
		<cfset PARAMS_EXPORT.SEARCH_TEXT_COLONNE = SEARCH_TEXT_COLONNE>
		<cfset PARAMS_EXPORT.SEARCH_TEXT = SEARCH_TEXT>
		<cfset PARAMS_EXPORT.OFFSET = OFFSET>			

		
		<cfif q2.RecordCount gt 0>	
			<cfset PARAMS_EXPORT.LIMIT = q2["NBRECORD"][1]>
			<cfelse>
			<cfset PARAMS_EXPORT.LIMIT = 0>
		</cfif>
		
		<!--- <cfset PARAMS_EXPORT.LIMIT = LIMIT> --->
		<cfset PARAMS_EXPORT.LANG = LANG>
		<cfset SESSION.PARAMS_EXPORT = PARAMS_EXPORT>
		
	<cfreturn q2 >
</cffunction>	
	
	<cffunction name="getQueryByWebService" output="true" access="public">
			
			<cfset res = CreateObject(	"webservice",
										"http://sun-bi.consotel.fr/analytics/saw.dll?WSDL",
										"ReportEditingServiceSoap")>
			
			<cfset reportRef = zcreateComplexObject()>
			<cfset reportParams = structNew()>
 			<cfset query = res.generateReportSQL(reportRef, reportParams, sessionId)>
	
		<cfreturn query>
	</cffunction>
	
	<cffunction name="getQuery" output="true" access="public" returntype="Query">
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_surtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="idDebut" 					required="true" type="Numeric" default="0">
		<cfargument name="idFin" 					required="true" type="Numeric" default="0">
		<cfargument name="idCliche" 				required="true" type="Numeric" default="0">
		<cfargument name="idorga" 					required="true" type="Numeric" default="0">
		
			<cfset dataTable = "E0_AGGREGS">
			<cfswitch expression="#niveau#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS_DEFRAG">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
						
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					<cfquery datasource="#dataSourceName#" name="queryrslt0">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idcliche				= #idCliche#,
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
					 	 SELECT 
							 	PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
							 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
					 	 		ROUND(FACTURATION.VOLUME_MN,0) as duree_appel, 
					 	 		UNITE_VOLUME.UNITE_SOMMEE as unite,
								PRODUIT1GROUPE.IDTHEME_PRODUIT as idtheme_produit,
								DEVISE.TAUX_CONV as TAUX_CONV,
								FACTURATION.MONTANT_LOC as montant_final,
								PRODUIT1GROUPE.OPERATEUR_PRODUIT as nom, 
								FACTURATION.NOMBRE_APPEL as nombre_appel, 
								PRODUIT1GROUPE.ORDRE_AFFICHAGE as ordre_affichage, 
								FACTURATION.QTE as qte, 
								PRODUIT1GROUPE.SEGMENT_THEME as segment_theme, 
								PRODUIT1GROUPE.SUR_THEME as sur_theme, 
								PRODUIT1GROUPE.THEME as theme_libelle, 
								PRODUIT1GROUPE.TYPE_THEME as type_theme,
								PRODUIT1GROUPE.IDSUR_THEME as idsur_theme
							FROM #dataTable#
							WHERE PERIODE.IDPERIODE_MOIS BETWEEN  #idDebut#
							AND  #idFin# ORDER BY ordre_affichage
					</cfquery>
					<cfset query = queryrslt0>
				</cfcase>
				
				<cfcase value="1">
					<!--- THEME --->
					<cfquery datasource="#dataSourceName#" name="queryrslt1">
							set variable 	p_idracine_master		= #arguments.p_idracine_master#,
											p_idracine				= #arguments.p_idracine#,
											p_idorga				= #idorga#,
											p_idperimetre			= #arguments.p_idperimetre#,
											p_niveau				= '#niveau#',
											p_idniveau				= #idniveau#,
											p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
					 	 	SELECT PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT, 
									PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CLIENT, 
									PRODUIT1GROUPE.LIBELLE_PRODUIT as LIBELLE_PRODUIT, 
									PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
								 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
									PRODUIT1GROUPE.THEME as THEME_LIBELLE, 
									PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
									PRODUIT1GROUPE.OPERATEUR_PRODUIT as NOM, 
									FACTURATION.QTE as QTE, 
									DEVISE.TAUX_CONV as TAUX_CONV,
									FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
									FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
									ROUND(FACTURATION.VOLUME_MN,0) as DUREE_APPEL,
									UNITE_VOLUME.UNITE_SOMMEE as unite																	
							FROM #dataTable#							
							WHERE PERIODE.IDPERIODE_MOIS BETWEEN #idDebut# AND #idFin#
							AND PRODUIT1GROUPE.IDTHEME_PRODUIT =  #p_idtheme#
							ORDER BY MONTANT_FINAL DESC
					</cfquery>
					<cfset query = queryrslt1>
				</cfcase>
			</cfswitch>	
		<cfreturn query>
	</cffunction>

	<cffunction name="executQuery" output="true" access="public" returntype="Query">
		<cfargument name="idTabBord" 				required="true"	type="Numeric" default="0">
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_surtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="dateDebutProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="dateFinProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="perimetreParm" 			required="true" type="String"  default="0">		
			
			<cfset idtdb 	 = idTabBord>
			<cfset queryrslt = "NULL">
			<cfset query 	 = "NULL">
			
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIMETRE = p_idperimetre>
			<cfif compareNoCase(perimetreParm,"SOUSTETE") eq 0>
				<cfset niveau 	= 'IDSOUS_TETE'>
			<cfelse>
				<cfset niveau 	= zgetNiveauPerimetre(p_idperimetre)>
			</cfif>
			<cfset idniveau 	= getIdNiveau(niveau)>
			<cfset SESSION.PERIMETRE.SELECTED_IDNIVEAU = idniveau>
			<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = niveau>
			<!--- 
			le mot clé var devant après cfset signifie que la variable est locale à la fonction.
			Donc elle disparait lorsque l'on sort de la fonction
			 --->
			<cfset idDebut 	= zgetIdPeriodeMois(dateDebutProvenantDuTDB)>	
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB = idDebut>
					
			<cfset idFin 	= zgetIdPeriodeMois(dateFinProvenantDuTDB)>
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN = idFin>
						
			<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga = Val(rsltIdOrga)>
		    </cfif>
		    <cfset SESSION.PERIMETRE.SELECTED_ID_ORGA = idorga>
		    
			<cfset idCliche = zGetCliche(idorga)>
			<cfset SESSION.PERIMETRE.SELECTED_ID_CLICHE = idCliche>
					
			<cfset rsltHandlerquery = getQuery(p_idproduit_catalogue, p_idtheme, p_surtheme, p_idracine_master, p_idracine, p_idperimetre, idDebut, idFin, idCliche, idorga)>

		<cfreturn rsltHandlerquery>
	</cffunction>



	<cffunction name="transformDate" access="public" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "/" & mid(oldDateString,4,2) & "/" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>

<!---
	FUNCTION PRIVATE
--->

	<cffunction name="zgetIdPeriodeMois" output="true" access="private" returntype="Numeric">
		<cfargument name="thisDate" type="String" required="true" hint="yyyy/mm/dd">
		
		<cfset var tmpDate = parseDateTime(ARGUMENTS.thisDate,"yyyy/mm/dd")>
		<cfset var tmpDateString = MID(lsDateFormat(tmpDate,"mm/yyyy"),1,7)>
		
		<cfquery datasource="#dataSourceName#" name="qGetIdperiodeMois">	
		 	 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS
		 	 FROM	E0_AGGREGS
		 	 WHERE	PERIODE.SHORT_MOIS='#tmpDateString#'
		 	 ORDER BY IDPERIODE_MOIS
		</cfquery>
		<cfif qGetIdperiodeMois.recordcount EQ 1>
			<cfreturn qGetIdperiodeMois["IDPERIODE_MOIS"][1]>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
	<cffunction name="zgetOrga" output="false" access="private" returntype="Any">
			<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
				SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#) as IDORGA FROM DUAL
			</cfquery>
		<cfreturn rsltOrga['IDORGA'][1]>
	</cffunction>
	
	<cffunction name="zgetNiveauPerimetre" output="false" access="public" returntype="String">
		 	<cfargument name="perimetre" type="numeric" required="true">
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_consoview_v3.get_numero_niveau">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgroupe_client" value="#perimetre#">			
				<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" 	variable="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="getIdNiveau" output="false" access="public" returntype="numeric">		
		<cfargument name="niveau" type="string" required="true">			 				
		<cfstoredproc procedure="pkg_consoview_v3.niveau_to_integer" datasource="#SESSION.OFFREDSN#">	
			<cfprocparam type="in" value="#niveau#" variable="p_niveau" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam type="out"  variable="p_idniveau" cfsqltype="CF_SQL_iNTEGER">
		</cfstoredproc>
		<cfreturn p_idniveau>
	</cffunction>
	
	<cffunction name="zGetCliche" output="true" access="public" returntype="Any">
		<cfargument name="idorga" type="numeric" required="true">
			<cfquery datasource="E0" name="clicheRslt">
				SELECT idcliche FROM 
				( SELECT c.idcliche,c.idperiode_mois,MAX(c.idperiode_mois) over () MAX_p FROM cliche c WHERE c.idorganisation=#idorga# ) 
				WHERE max_p=idperiode_mois
			</cfquery>
		<cfreturn clicheRslt['IDCLICHE'][1]>
	</cffunction>
	
</cfcomponent>