<cfcomponent name="societeService">
	<cffunction name="getListePortefeuille" access="remote" returntype="query" description="">
		<cfargument name="idActeur" type="numeric" required="true">
		<cfargument name="cle_raisonSociale" type="string" required="false" default="">
		<cfargument name="cle_SIREN" type="string" required="false" default="">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_mbp_global.GET_LIST_SOCIETE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idActeur#" null="false">
			<cfprocparam type="In" cfsqltype="cf_sql_vARCHAR" value="#cle_raisonSociale#" null="false">
			<cfprocparam type="In" cfsqltype="cf_sql_vARCHAR" value="#cle_SIREN#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getListeCliente" access="remote" returntype="query" description="">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_accreditation.listcompany">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getListeSociete" access="remote" returntype="query" description="">
		<cfargument name="idActeur" type="numeric" required="true">
		<cfargument name="cle_raisonSociale" type="string" required="false" default="">
		<cfargument name="cle_SIREN" type="string" required="false" default="">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_mbp_global.GET_LIST_SOCIETE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idActeur#" null="false">
			<cfprocparam type="In" cfsqltype="cf_sql_vARCHAR" value="#cle_raisonSociale#" null="false">
			<cfprocparam type="In" cfsqltype="cf_sql_vARCHAR" value="#cle_SIREN#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getListeAccr" access="remote" returntype="query" description="">
		<cfargument name="companyid" type="Numeric" default="">
		<cfargument name="corporatename" type="string" default="">
		<cfargument name="bool_ext" type="Numeric" default="">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_accreditation.SearchAccr">
			<cfprocparam value="#companyid#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#corporatename#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#bool_ext#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getListeLogin" access="remote" returntype="query" description="">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_accreditation.getListeLogin">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getDetailSociete" access="remote" returntype="query" description="">
		<cfargument name="idSociete" type="numeric" required="true">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_mbp_global.getDetailSociete">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idSociete#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getPersonsSociete" access="remote" returntype="query" description="">	
		
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_accreditation.listPerson">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="getAdminsSociete" access="remote" returntype="query" description="">	
		
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.getAdminSociete">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	<cffunction name="majAdminsSociete" access="remote" returntype="numeric" description="">
		
		
		<cfargument name="idCompany" type="numeric" required="true">
		
		<cfargument name="idAdmin" type="numeric" required="true">
		<cfargument name="idAdminDelegue" type="numeric" required="true">
		
		<cfargument name="idihm_user" type="numeric" required="true">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="Pkg_mbp_global.majAdminSociete">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCompany#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idAdmin#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idAdminDelegue#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idihm_user#" null="false">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
</cfcomponent>