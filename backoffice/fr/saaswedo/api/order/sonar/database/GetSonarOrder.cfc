﻿component extends="Proc"   
{
	public GetSonarOrder function init(string datasource)
	output="false"
	{	
		super.init();
		
		if(isdefined('arguments.datasource'))
		{
			this.datasource = arguments.datasource;
		}
		
		return( this );
	}
	
	public any function execute(required numeric MytemOrderId)
	 displayname="execute" description="execute the stored procedure" hint="execute" output="true"
	{
		//create a new storedproc service
		var spService = new storedproc();
		var result = {};
		
		spService.setDatasource(this.datasource);
		spService.setProcedure("PKG_M16.GET_SONARORDER");
			  
		spService.addParam(type="in", cfsqltype="cf_sql_integer", dbvarname=":p_idcommande", value=javaCast('int',arguments.MytemOrderId));
		
	    spService.addProcResult(name="p_ref_1",resultset=1);//detail commande
	    spService.addProcResult(name="p_ref_2",resultset=2);//infos articles
	    spService.addProcResult(name="p_ref_3",resultset=3);//infos devices
	    spService.addProcResult(name="p_ref_4",resultset=4);//infos accessories
	    spService.addProcResult(name="p_ref_5",resultset=5);//infos plans and option
	    
	    //execute the stored procedure
	    procresult = spService.execute();
	    
	    result.commande = procresult.getProcResultSets().p_ref_1;
	    result.article = procresult.getProcResultSets().p_ref_2;
	    result.device = procresult.getProcResultSets().p_ref_3;
	    result.accessory = procresult.getProcResultSets().p_ref_4;
	    result.plan = procresult.getProcResultSets().p_ref_5;
	  	result.status = 1;
	  	writeoutput("qResule<br/>"); 
		writedump(result);
		return result;
	}

}