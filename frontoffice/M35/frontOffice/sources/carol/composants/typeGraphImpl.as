package carol.composants
{
	import carol.event.CarolEvent;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.ComboBox;
	
	public class typeGraphImpl extends VBox
	{

//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - COMPOSANTS FLEX
		//--------------------------------------------------------------------------------------------//
		
		public var cbxGraphType	:ComboBox;
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - GLOBAL
		//--------------------------------------------------------------------------------------------//
		[Bindable] public var VALUE				:ArrayCollection = null;
		[Bindable] public var LABEL				:String 		 = "";
		[Bindable] public var SELECTEDINDEX 	:int			 = 0
		
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//
		
		public function typeGraphImpl()
		{
		}
		
//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED - EVENT COMPOSANTS - IBM
		//--------------------------------------------------------------------------------------------//
		
		protected function cbxGraphTypeChangeHandler(e:Event):void
		{
			dispatchEvent(new CarolEvent(CarolEvent.CHANGE_CBX, true, cbxGraphType.selectedItem));
			
		}
	}
}