package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	
	public class IsLigneSuspended implements IRuleDisplayItem
	{
		public function IsLigneSuspended() 
		{
		}
		
		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			return (ligne.ST_IDETAT == 2)?true:false;				 		
		}
		
		
	}
}