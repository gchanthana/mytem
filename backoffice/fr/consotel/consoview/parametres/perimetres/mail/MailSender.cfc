<cfcomponent extends="fr.consotel.consoview.access.AccessObject" output="false">
	
	
	<!--- Creation du dossier temporaire --->
	<cffunction name="createFolder" access="private" returntype="numeric" output="true">
		<cfargument name="folderName" type="string" required="true">
		<cfset racine = "listemailsclient">
		<cfset qDir = 1>
		<cfdirectory action="List" directory="/#racine#/#folderName#" name="qDir">
		<cftry>
			<cfdirectory action="create" directory="/#racine#/#folderName#">	
			<cfcatch type="any">
			</cfcatch>
		</cftry>
		<cfreturn qDir.recordcount>
	</cffunction>
	
	<cffunction name="envoyer" access="public" returntype="numeric">
		<cfargument name="mail" type="fr.consotel.consoview.util.Mail" required="true">
		<cfreturn sendSingleMail(mail)>
	</cffunction>		
		
	<cffunction name="sendSingleMail" access="private" returntype="numeric" output="true">
		<cfargument name="mail" type="any" required="true" >
		
		<cfset p_retour = -1>
		
		<cfset str1 = 'SIZE="10"'>
		<cfset str2 = 'SIZE="2"'>		
		<cfset message1 = #Replace(mail.getMessage(),str1,str2,"all")#>

		<cfset str1 = "&apos;">
		<cfset str2 = "'">
		<cfset message = #Replace(message1,str1,str2,"all")#>

		<cfset gestPerimetre = createobject("component","fr.consotel.consoview.parametres.perimetres.gestionPerimetre")>


		<cfset listeMailEmp = "">
		<cfset cc = mail.getCc()>		
		<cfif mail.getCopiePourExpediteur() eq "YES">
			<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
			<cfset qProfileOperateur = gestPerimetre.getEmployesWithProfile(getidGroupe(),getprofileOperateur())>
			
			<cfif (mail.getCopiePourOperateur() eq "YES") and (qProfileOperateur.recordcount gt 0)>				
				<cfset tmpListe = "">
				<cfloop query="qProfileOperateur">
					<cfset tmpListe = tmpListe & qProfileOperateur.EMAIL & ",">
				</cfloop>
				<cfif len(tmpListe) gt 0>
					<cfset listeMailEmp = left(tmpListe,len(tmpListe)-1)>
				</cfif>
				<cfset cc = mail.getCc() & "," & mail.getExpediteur()& "," & listeMailEmp>
			</cfif>			
		</cfif>
			
			
			<!--- envoi du message --->
			<cftry>
				<cfmail from="#mail.getExpediteur()#"
						to="#mail.getDestinataire()#"
						subject="#mail.getModule()# : #mail.getSujet()#"
						type="#mail.getType()#"
						charset="#mail.getCharset()#"
						bcc="#mail.getBcc()#"
						cc="#cc#"
						replyto="#mail.getRepondreA()#"
						server="mail.consotel.fr:26">
						
					 
						<html lang='fr'>
						<head>
						<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
						<body>									
							#message#
						</body>
						</html>
				</cfmail>
					<cfset p_retour = 1>
				<cfcatch>
					<cfset p_retour = -1>
				</cfcatch>
			</cftry>
			<!--- fin envoi du message --->					
			
			
			<!--- Sauvegarde du message --->
			<cftry>
				<cfset idMessage = createUUID()>
				<cfset resFolder = createFolder( session.perimetre.RAISON_SOCIALE & "/" & session.user.NOM & "_" & session.user.PRENOM & '/' &LSDATEFORMAT(now(),"dd_mm_yyyy") )>
				<cfset FILE_NAME = "/listemailsclient/" 
									& session.perimetre.RAISON_SOCIALE & "/" & session.user.NOM & "_" & session.user.PRENOM & "/"									
									& LSDATEFORMAT(now(),"dd_mm_yyyy") 
									& "/" 
									& idMessage 
									& "_" 
									& resFolder
									& ".pdf">
								
				<cfcontent type="FlashPaper/pdf"> 
					<cfheader name="Content-Disposition" value="inline;filename=#FILE_NAME#">
					<cfdocument format="PDF" backgroundvisible="yes" fontembed = "yes" unit="CM" margintop="5" orientation="portrait" filename="#FILE_NAME#">							
					 
						<html lang='fr'>
						<head>
						<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
						<body>									
							#message#
						</body>
						</html>
					</cfdocument>	
					<cfset p_retour = 2>
				<cfcatch type="any">
					<cfset p_retour = -11>
				</cfcatch>
			</cftry>
			<!--- Fin sauvegarde du message --->
			<cfreturn p_retour>
	 </cffunction>
	
</cfcomponent>