<cfcomponent name="RapportBudgetByNodeGroupeLigneStrategy"
			alias="fr.consotel.consoview.facturation.budget.BudgetByNode.RapportBudgetByNodeGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="ID_BUDGET" required="true" type="numeric"/>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_GLIG_V3.RP_BUDG1">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#ID_BUDGET#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetReportData">
		</cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
				
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="rapportParams" required="true" type="struct"/>
		<cfset qReportData = getReportData(ID_PERIMETRE,rapportParams.BUDGET_ID)>
		<cfif qReportData.recordcount GT 0>
			<cfset rapportParams.BUDGET_DATEDEB = parseDateTime(rapportParams.BUDGET_DATEDEB)>
			<cfset rapportParams.BUDGET_DATETEMP = parseDateTime(rapportParams.BUDGET_DATEDEB)>
			<cfset rapportParams.BUDGET_DATEFIN = parseDateTime(rapportParams.BUDGET_DATEFIN)>
			<cfset rapportParams.nbMonth = dateDiff("m",rapportParams.BUDGET_DATEDEB,rapportParams.BUDGET_DATEFIN) + 1>
	
			<cfset monthXmlMap = XmlNew()>
			<cfset monthXmlMap.xmlRoot = XmlElemNew(monthXmlMap,"Periode")>
			<cfset rootNode = monthXmlMap.Periode>
			
			<cfset rapportParams.BUDGET_DATETEMP = parseDateTime(rapportParams.BUDGET_DATEDEB)>
			<cfoutput>
				<cfset monthIndex = 2>
				<cfloop from="1" to="#rapportParams.nbMonth#" index="i" step="1">
					<cfset monthKey = lsDateformat(rapportParams.BUDGET_DATETEMP,"YYYY/MM")>
					<cfset nbChild = arrayLen(rootNode.XmlChildren)>
					<cfset rootNode.XmlChildren[nbChild + 1] = XmlElemNew(monthXmlMap,"mois")>
					<cfset rootNode.XmlChildren[nbChild + 1].XmlAttributes.KEY = "#monthKey#">
					<cfset rootNode.XmlChildren[nbChild + 1].XmlAttributes.VAL = 0>
					<cfset rootNode.XmlChildren[nbChild + 1].XmlAttributes.BDG = 0>
					<cfset rootNode.XmlChildren[nbChild + 1].XmlAttributes.SET = 0>
					<cfset rootNode.XmlChildren[nbChild + 1].XmlAttributes.POS = monthIndex + i>
					<cfset monthIndex = monthIndex + 1>
					<cfset rapportParams.BUDGET_DATETEMP = dateAdd("m",1,rapportParams.BUDGET_DATETEMP)>
				</cfloop>
			</cfoutput>
			<cfinclude template="./BudgetByNode_XLS.cfm">
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
