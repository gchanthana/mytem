<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.IDelivery"
hint="Interface qui spécifie les méthodes à implémenter pour définir le DELIVERY pour un reporting<br>
Le rôle de cette interface est de convertir en tant que structure l'objet IReporting<br> 
La méthode deliver() doit être appelée pour obtenir la structure à utilisée comme paramètre à fr.consotel.api.ibis.IBIS.invokeService()<br>
L'implémentation peut spécifier une valeur par défaut pour la clé EVENT_HANDLER qui est retournée par deliver() lorsque reporting.reportingHandler() n'est pas renseigné<br>
Le cas échéant, l'implémentation précisera les infos stockées par cette méthode et restituées au HANDLER (EVENT_HANDLER)">
<!--- *************************************************************************************************************************************** --->
	<cffunction name="deliver" returntype="struct" hint="Retourne la structure à passer comme paramètre parameters à fr.consotel.api.ibis.IBIS.invokeService()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.reporting.IReporting" hint="Reporting à diffuser">
	</cffunction>

	<cffunction name="createInstance" returntype="fr.consotel.api.ibis.publisher.delivery.IDelivery" hint="Méthode appelée en tant que constructeur de l'implémentation">
	</cffunction>
</cfinterface>