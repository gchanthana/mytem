<cfcomponent displayname="ConcreteFactureCreator">	
	<cffunction name="createJournal" access="public" returntype="any" output="false" hint="crée la bonne instance de Facture en fonction de l'opérateur">
		<cfargument name="type" required="true" type="string" />	
		<cfargument name="format" required="true" type="string">		
		<cfset journal=createObject("component","fr.consotel.consoview.journaux." & #format# & ".Journal" & #type# )>		
		<cfreturn Journal>
	</cffunction>	
</cfcomponent>
  