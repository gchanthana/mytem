package univers.inventaire.equipements.lignes.v2.util
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	
	
	
	
	[Bindable]
	public class viewsHelpers extends EventDispatcher
	{
		
		//Onglet Ligne ================================================================
		public var listeTypesLigne : ArrayCollection;
		public var listeUsages : ArrayCollection;
		public var listeTypeRaccordement : ArrayCollection;
		
		public function getListeTypesLigne():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getTypeLigne",
																				getListeTypesLigneResultHandler);
			RemoteObjectUtil.callService(op);							
		}
		public function getListeTypesLigneResultHandler(re : ResultEvent):void{
			if(re.result){
				listeTypesLigne = re.result as ArrayCollection;
			}else{
				listeTypesLigne = null;
			}
		}
		
		
		
				
		public function getListeUsages():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getUsages",
																				getListeUsagesResultHandler); 
			RemoteObjectUtil.callService(op);
		}
		public function getListeUsagesResultHandler(re : ResultEvent):void{
			if(re.result){
				listeUsages = re.result as ArrayCollection;
			}else{
				listeUsages = null;
			}
		}
		
		public function getListeTypeRaccordement():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getTypeRacco",
																				getListeTypeRaccordementResultHandler);
			RemoteObjectUtil.callService(op);
		}
		public function getListeTypeRaccordementResultHandler(re : ResultEvent):void{
			if(re.result){
				listeTypeRaccordement = re.result as ArrayCollection;
			}else{
				listeTypeRaccordement = null;
			}
		}
		
		
		
		//Onglet Ligne ================================================================
		
		
		
		//Onglet affcetation ==========================================================
		public var listeCollaborateurANU:ArrayCollection;		
		public function getListeCollaborateurANU():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getListeCollaborateur",
																				listeCollaborateurANUResultHandler);
			RemoteObjectUtil.callService(op);
		}
		public function listeCollaborateurANUResultHandler(re : ResultEvent):void{
			if(re.result){
				listeCollaborateurANU = re.result as ArrayCollection;
			}else{
				listeCollaborateurANU = null;
			}
		}
		
		
		//Fin onglet affectation ======================================================		
			
		public function viewsHelpers()
		{	
			super();
		}
		

		
	}
}