package paginatedatagrid.event
{
	import flash.events.Event;
	
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	public class PaginateDatagridEvent extends Event
	{
		public static const PAGE_CHANGE:String = 'onIntervalleChange';
		public static const ON_SELECT_ALL_CHANGE:String = 'onSelectAllChange';
		public static const ON_ITEM_SELECTED:String = 'onItemSelected';
		public static const ON_BT_EXPORT_PRESSED:String = 'onBTExportPressed';
		
		public var itemIntervalleVO:ItemIntervalleVO;
		
		public function PaginateDatagridEvent(type:String,itemIntervalleVO : ItemIntervalleVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.itemIntervalleVO = itemIntervalleVO;
		}

	}
}