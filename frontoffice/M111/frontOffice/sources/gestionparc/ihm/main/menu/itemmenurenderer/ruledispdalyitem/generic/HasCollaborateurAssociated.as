package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	
	public class HasCollaborateurAssociated implements IRuleDisplayItem
	{
		public function HasCollaborateurAssociated()
		{
		}
		
		public function isDisplay():Boolean
		{
			var rule:IRuleDisplayItem = new HasCollaborateurAssociated()
			if(SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE> 0)
				return true;
			else
				return false;
		}
	}
}