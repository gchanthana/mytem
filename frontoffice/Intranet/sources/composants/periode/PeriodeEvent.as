package composants.periode
{
	import flash.events.Event;

	public class PeriodeEvent extends Event
	{
		public function PeriodeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		//la date de debut au format jj/mm/aaaa		
		private var _moisDeb : String;
		
		//la date de fin au format jj/mm/aaaa
		private var _moisFin : String;
		
		
		//la date de debut de periode
		private var _dateDeb : Date;
		//la date de fin de periode
		private var _dateFin : Date;
		/**
		 * Affecte la date de debut de la periode
		 * @param mois Une date au format jj/mm/aaaa
		 **/
		public function set moisDeb(mois : String):void{
			_moisDeb = mois;
		}
		
		public function get moisDeb():String{
			return _moisDeb;
		}
		
		/**
		 * Affecte la de fin de la periode
		 * @param mois Une date au format jj/mm/aaaa
		 **/		
		public function set moisFin(mois : String):void{
			_moisFin = mois;
		}
		
		public function get moisFin():String{
			return _moisFin;
		}
		
		/**
		 * Affecte la date de debut de la periode
		 * @param Date
		 **/
		public function set dateDeb(date : Date):void{
			_dateDeb = date;
		}
		
		public function get dateDeb():Date{
			return _dateDeb;
		}
		
		/**
		 * Affecte la de fin de la periode
		 * @param Date
		 **/		
		public function set dateFin(date : Date):void{
			_dateFin = date;
		}
		
		public function get dateFin():Date{
			return _dateFin;
		}
		
	}
}