package gestionparc.ihm.main.menu.itemmenurenderer
{
	import gestionparc.ihm.action.collaborateur.actionCollabDepartEntreprise.ActionCollabDepartEntrepriseCommand;
	import gestionparc.ihm.action.collaborateur.actionCollabDepartEntreprise.ActionCollabDepartEntrepriseReceiver;
	import gestionparc.ihm.action.collaborateur.actionCollabVoirFiche.ActionCollabVoirFicheCommand;
	import gestionparc.ihm.action.collaborateur.actionCollabVoirFiche.ActionCollabVoirFicheReceiver;
	import gestionparc.ihm.action.collaborateur.actionCollabVoirHistorique.ActionCollabVoirHistoriqueCommand;
	import gestionparc.ihm.action.collaborateur.actionCollabVoirHistorique.ActionCollabVoirHistoriqueReceiver;
	import gestionparc.ihm.action.fournir.actionFournirlignesim.actionFournirLigneSimCommand;
	import gestionparc.ihm.action.fournir.actionFournirlignesim.actionFournirLigneSimReceiver;
	import gestionparc.ihm.action.fournir.actionfournireqp.ActionFournirEqpCommand;
	import gestionparc.ihm.action.fournir.actionfournireqp.ActionFournirEqpReceiver;
	import gestionparc.ihm.action.fournir.actionfournirligne.ActionFournirLigneCommand;
	import gestionparc.ihm.action.fournir.actionfournirligne.ActionFournirLigneReceiver;
	import gestionparc.ihm.action.fournir.actionfournirligneext.actionFournirLigneExtCommand;
	import gestionparc.ihm.action.fournir.actionfournirligneext.actionFournirLigneExtReceiver;
	import gestionparc.ihm.action.reprendre.actionReprendreEqp.ActionReprendreEqpCommande;
	import gestionparc.ihm.action.reprendre.actionReprendreEqp.ActionReprendreEqpReceiver;
	import gestionparc.ihm.action.reprendre.actionReprendreLigne.ActionReprendreLigneCommand;
	import gestionparc.ihm.action.reprendre.actionReprendreLigne.ActionReprendreLigneReceiver;
	import gestionparc.ihm.action.reprendre.actionReprendreLignesim.ActionReprendreLigneSimCommand;
	import gestionparc.ihm.action.reprendre.actionReprendreLignesim.ActionReprendreLigneSimReceiver;
	import gestionparc.ihm.action.reprendre.actionreprendreligneext.ActionReprendreLigneExtCommand;
	import gestionparc.ihm.action.reprendre.actionreprendreligneext.ActionReprendreLigneExtReceiver;
	import gestionparc.ihm.main.menu.ItemMenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.collab.RuleCollabDepartEntreprise;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.collab.RuleCollabVoirHistorique;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.collab.RuleCollabVoirfiche;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir.RuleFournirEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir.RuleFournirLigneExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir.RuleFournirLigneSim;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir.RulefournirLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuCollab;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuCollabFournir;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleMenuCollabReprendre;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre.RuleReprendreEqp;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre.RuleReprendreLigne;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre.RuleReprendreLigneExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre.RuleReprendreLigneSim;
	
	import mx.resources.ResourceManager;

	public class ItemMenuContextualOfCollaborateur extends AbstractItemMenuContextual
	{

		[Embed(source='/assets/images/give_terminal.png')]
		private var myIconGiveTerm:Class;

		[Embed(source='/assets/images/get_terminal.png')]
		private var myIconGetTerm:Class;

		[Embed(source='/assets/images/Newfixe+.png')]
		private var myIconNewFixeP:Class;

		[Embed(source='/assets/images/Newfixe-.png')]
		private var myIconNewFixeM:Class;

		[Embed(source='/assets/images/action_supprimer.png')]
		private var myIconActionSupprimer:Class;

		[Embed(source='/assets/images/voir.png')]
		private var myIconVoir:Class;

		[Embed(source='/assets/images/historique.png')]
		private var myIconHistorique:Class;


		/**
		 * Cette fonction créer la liste exaustive des items du menu contextuel du collaborateur.
		 * @return ArrayCollection
		 */
		override protected function fillArrayItemMenuContextual():Array
		{
			var res:Array=new Array();

			// MENU Fournir
			var rmCollab:RuleMenuCollabFournir=new RuleMenuCollabFournir();
			var objFounrir:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'FOURNIR'), false, null, null, rmCollab);
			res.push(objFounrir);

			// Fournir un equipement
			var recFournirEqp:ActionFournirEqpReceiver=new ActionFournirEqpReceiver();
			var cmdFournirEqp:ActionFournirEqpCommand=new ActionFournirEqpCommand(recFournirEqp);
			var rFourEqp:RuleFournirEqp=new RuleFournirEqp();
			var objFounrirEqpt:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'un__quipement'), true, myIconGiveTerm, cmdFournirEqp, rFourEqp);
			res.push(objFounrirEqpt);

			// Fournir une ligne/sim
			var receiverFournirLigneSim:actionFournirLigneSimReceiver=new actionFournirLigneSimReceiver();
			var actionFournirLigneSim:actionFournirLigneSimCommand=new actionFournirLigneSimCommand(receiverFournirLigneSim);
			var rFourLigSim:RuleFournirLigneSim=new RuleFournirLigneSim();
			var objFounrirLigneSim:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_SIM'), true, myIconNewFixeP, actionFournirLigneSim, rFourLigSim);
			res.push(objFounrirLigneSim);

			/** fournir ligne / extension */
			var receiverFournirLigneExt:actionFournirLigneExtReceiver=new actionFournirLigneExtReceiver();
			var actionFournirLigneExt:actionFournirLigneExtCommand=new actionFournirLigneExtCommand(receiverFournirLigneExt);
			var rFourLigExt:RuleFournirLigneExt=new RuleFournirLigneExt();
			var objFounrirLigneExt:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_Extension'), true, myIconNewFixeP, actionFournirLigneExt, rFourLigExt);
			res.push(objFounrirLigneExt);
			
			
			// Fournir une ligne
			var receiverFournirLigne:ActionFournirLigneReceiver=new ActionFournirLigneReceiver();
			var actionFournirLigne:ActionFournirLigneCommand=new ActionFournirLigneCommand(receiverFournirLigne);
			var rFourLig:RulefournirLigne=new RulefournirLigne();
			var objFounrirLigne:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne'), true, myIconNewFixeP, actionFournirLigne, rFourLig);
			res.push(objFounrirLigne);

			// MENU Reprendre
			var rmColRep:RuleMenuCollabReprendre=new RuleMenuCollabReprendre();
			var objReprendre:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'REPRENDRE'), false, null, null, rmColRep);
			res.push(objReprendre);

			// Reprendre un equipement
			var recRepEqp:ActionReprendreEqpReceiver=new ActionReprendreEqpReceiver()
			var cmdRepEqp:ActionReprendreEqpCommande=new ActionReprendreEqpCommande(recRepEqp);
			var ruleRepEqp:RuleReprendreEqp=new RuleReprendreEqp();
			var objReprendreEqpt:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'un__quipement'), true, myIconGetTerm, cmdRepEqp, ruleRepEqp);
			res.push(objReprendreEqpt);

			// Reprendre une ligne/sim
			var recRepLigSim:ActionReprendreLigneSimReceiver=new ActionReprendreLigneSimReceiver();
			var cmdRepLigSim:ActionReprendreLigneSimCommand=new ActionReprendreLigneSimCommand(recRepLigSim);
			var rRepLigSim:RuleReprendreLigneSim=new RuleReprendreLigneSim();
			var objReprendreLigneSim:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_SIM'), true, myIconNewFixeM, cmdRepLigSim, rRepLigSim);
			res.push(objReprendreLigneSim);
			
			/** reprendre une ligne / extension */
			var recRepLigneExt:ActionReprendreLigneExtReceiver=new ActionReprendreLigneExtReceiver();
			var cmdRepLigneExt:ActionReprendreLigneExtCommand=new ActionReprendreLigneExtCommand(recRepLigneExt);
			var rRepLigneExt:RuleReprendreLigneExt=new RuleReprendreLigneExt();
			var objReprendreLigneExt:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne_Extension'), true, myIconNewFixeM, cmdRepLigneExt, rRepLigneExt);
			res.push(objReprendreLigneExt);
			
			// Reprendre une ligne
			var recRepLig:ActionReprendreLigneReceiver=new ActionReprendreLigneReceiver();
			var cmdRepLig:ActionReprendreLigneCommand=new ActionReprendreLigneCommand(recRepLig);
			var rRepLig:RuleReprendreLigne=new RuleReprendreLigne();
			var objReprendreLigne:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'une_ligne'), true, myIconNewFixeM, cmdRepLig, rRepLig);
			res.push(objReprendreLigne);

			// MENU Collaborateur
			var rmColl:RuleMenuCollab=new RuleMenuCollab();
			var objCollab:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'COLLABORATEUR'), false, null, null, rmColl);
			res.push(objCollab);

			// Depart de l'entreprise
			var recDepart:ActionCollabDepartEntrepriseReceiver=new ActionCollabDepartEntrepriseReceiver();
			var cmdDepartEntreprise:ActionCollabDepartEntrepriseCommand=new ActionCollabDepartEntrepriseCommand(recDepart);
			var rDepEntr:RuleCollabDepartEntreprise=new RuleCollabDepartEntreprise();
			var objCollabDepartEntreprise:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'D_part_de_l_entreprise'), true, myIconActionSupprimer, cmdDepartEntreprise, rDepEntr);
			res.push(objCollabDepartEntreprise);

			// Voir fiche
			var receiverVoirFiche:ActionCollabVoirFicheReceiver=new ActionCollabVoirFicheReceiver();
			var actionVoirFiche:ActionCollabVoirFicheCommand=new ActionCollabVoirFicheCommand(receiverVoirFiche);
			var rVoirFic:RuleCollabVoirfiche=new RuleCollabVoirfiche();
			var objCollabFiche:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_fiche'), true, myIconVoir, actionVoirFiche, rVoirFic);
			res.push(objCollabFiche);

			// Voir historique
			var receiverVoirHisto:ActionCollabVoirHistoriqueReceiver=new ActionCollabVoirHistoriqueReceiver();
			var actionVoirHisto:ActionCollabVoirHistoriqueCommand=new ActionCollabVoirHistoriqueCommand(receiverVoirHisto);
			var rVoirHisto:RuleCollabVoirHistorique=new RuleCollabVoirHistorique();
			var objCollabHisto:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_historique'), true, myIconHistorique, actionVoirHisto, rVoirHisto);
			res.push(objCollabHisto);

			return res;
		}
	}
}
