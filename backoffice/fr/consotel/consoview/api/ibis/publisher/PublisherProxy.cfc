<cfcomponent name="PublisherProxy" displayname="fr.consotel.consoview.api.ibis.publisher.PublisherProxy" implements="fr.consotel.consoview.api.ibis.publisher.IPublisherProxy">
	<cfset VARIABLES["APPLICATION_PROXY"]="NULL">
	<cfset VARIABLES["BIP_WEBSERVICE_WSDL"]="NULL">
	
	<cffunction name="init" access="public" returntype="fr.consotel.consoview.api.ibis.publisher.IPublisherProxy" description="Package Constructor. Use getInstance instead.">
		<cfset VARIABLES["APPLICATION_PROXY"]=createObject("component","fr.consotel.consoview.api.ibis.admin.ApplicationProxy")>
		<cfset VARIABLES["BIP_WEBSERVICE_WSDL"]=VARIABLES["APPLICATION_PROXY"].getConfigurationMapProxy().getPublisherWSDL()>
		<cfreturn THIS>
	</cffunction>

	<cffunction name="getReportDefinition" access="public" returntype="fr.consotel.consoview.api.ibis.publisher.report.IReportDefinition"
		description="Returns Report Definition (Name,Parameters,Templates,etc...). Target BIP report must have at least one template.">
		<cfargument name="reportAbsolutePath" type="string" required="true" hint="Report Absolute Path" >
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfset var bipService="NULL">
		<cfset var reportDefinition="NULL">
		<cfif isInstanceOf(VARIABLES["APPLICATION_PROXY"],"fr.consotel.consoview.api.ibis.admin.ApplicationProxy") AND
				VARIABLES["BIP_WEBSERVICE_WSDL"] NEQ "NULL">
			<cfset bipService=createObject("webservice",VARIABLES["BIP_WEBSERVICE_WSDL"])>
			<cfif bipService.hasReportAccess(ARGUMENTS.reportAbsolutePath,ARGUMENTS.userId,ARGUMENTS.pwd)>
				<cftry>
					<cfset reportDefinition=bipService.getReportDefinition(ARGUMENTS.reportAbsolutePath,ARGUMENTS.userId,ARGUMENTS.pwd)>
					<cfreturn createObject("component","fr.consotel.consoview.api.ibis.publisher.report.ReportDefinitionSupport").init(ARGUMENTS.reportAbsolutePath,reportDefinition)>
					<cfcatch type="any">
						<cfthrow type="Custom" errorcode="3000" message="FAILS TO GET BIP REPORT DEFINITION" detail="fr.consotel.consoview.api.ibis.publisher.PublisherProxy.getReportDefinition() fails">
					</cfcatch>
				</cftry>
			<cfelse>
				<cfthrow type="Custom" errorcode="3001" message="INVALID CREDENTIALS - REPORT DEFINITION" detail="fr.consotel.consoview.api.ibis.publisher.PublisherProxy.getReportDefinition() fails due to invalid credentials">
			</cfif>
		</cfif>
		<cfreturn createObject("component","fr.consotel.consoview.api.ibis.publisher.report.ReportDefinitionSupport")>
	</cffunction>
	
	<cffunction name="getReportDefinitionInfos" access="private" returntype="struct"
		description="(DEVELOPEMENT USE ONLY) Returns Report Definition (Name,Parameters,Templates,etc...) as CF Structure. an empty structure is returned on failure">
		<cfargument name="reportAbsolutePath" type="string" required="true" hint="Report Absolute Path" >
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfset var bipService="NULL">
		<cfset var reportDefinition="NULL">
		<cfset var paramNameValuesArray="NULL">
		<cfset var templateArray="NULL">
		<cfset var formatArray="NULL">
		<cfset var reportDefinitionStruct=structNew()>
		<cfif isInstanceOf(VARIABLES["APPLICATION_PROXY"],"fr.consotel.consoview.api.ibis.admin.ApplicationProxy") AND
				VARIABLES["BIP_WEBSERVICE_WSDL"] NEQ "NULL">
			<cfset bipService=createObject("webservice",VARIABLES["BIP_WEBSERVICE_WSDL"])>
			<cfif bipService.hasReportAccess(ARGUMENTS.reportAbsolutePath,ARGUMENTS.userId,ARGUMENTS.pwd)>
				<cftry>
					<cfset reportDefinition=bipService.getReportDefinition(ARGUMENTS.reportAbsolutePath,ARGUMENTS.userId,ARGUMENTS.pwd)>
					<!--- ======================================================================================= --->
					<cfset reportDefinitionStruct["INFOS"]=structNew()>
					<cfset reportDefinitionStruct["INFOS"]["REPORT_NAME"]=reportDefinition.getReportName()>
					<cfset reportDefinitionStruct["INFOS"]["REPORT_DEFN_TITLE"]=reportDefinition.getReportDefnTitle()>
					<cfset reportDefinitionStruct["INFOS"]["REPORT_DESC"]=reportDefinition.getReportDescription()>
					<!--- ======================================================================================= --->
					<cfset paramNameValuesArray=reportDefinition.getReportParameterNameValues()>
					<cfset reportDefinitionStruct["PARAMETERS"]=structNew()>
					<cfloop index="i" from="1" to="#arrayLen(paramNameValuesArray)#">
						<cfset reportDefinitionStruct["PARAMETERS"][paramNameValuesArray[i].getName()]=structNew()>
						<cfset reportDefinitionStruct["PARAMETERS"][paramNameValuesArray[i].getName()]["MULTI_VALUES_ALLOWED"]=paramNameValuesArray[i].isMultiValuesAllowed()>
						<cfset reportDefinitionStruct["PARAMETERS"][paramNameValuesArray[i].getName()]["VALUES"]=paramNameValuesArray[i].getValues()>
					</cfloop>
					<!--- ======================================================================================= --->
					<cfset templateArray=reportDefinition.getListOfTemplateFormatsLabelValues()>
					<cfset reportDefinitionStruct["VIEWS"]=structNew()>
					<cfset reportDefinitionStruct["VIEWS"]["DEFAULT"]=structNew()>
					<cfset reportDefinitionStruct["VIEWS"]["DEFAULT"]["DEFAULT_TEMPLATE_ID"]=reportDefinition.getDefaultTemplateId()>
					<cfset reportDefinitionStruct["VIEWS"]["DEFAULT"]["DEFAULT_OUTPUT_FORMAT"]=reportDefinition.getDefaultOutputFormat()>
					<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"]=arrayNew(1)>
					<cfloop index="i" from="1" to="#arrayLen(templateArray)#">
						<cfset formatArray=templateArray[i].getListOfTemplateFormatLabelValue()>
						<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]=structNew()>
						<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]["TEMPLATE_ID"]=templateArray[i].getTemplateID()>
						<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]["TEMPLATE_TYPE"]=templateArray[i].getTemplateType()>
						<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]["TEMPLATE_URL"]=templateArray[i].getTemplateURL()>
						<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]["FORMATS"]=arrayNew(1)>
						<cfloop index="j" from="1" to="#arrayLen(formatArray)#">
							<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]["FORMATS"][j]=structNew()>
							<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]["FORMATS"][j]["LABEL"]=formatArray[j].getTemplateFormatLabel()>
							<cfset reportDefinitionStruct["VIEWS"]["TEMPLATES"][i]["FORMATS"][j]["VALUE"]=formatArray[j].getTemplateFormatValue()>
						</cfloop>
					</cfloop>
					<!--- ======================================================================================= --->
					<cfset reportDefinitionStruct["VIEWS"]["TEMPLATE_IDS"]=reportDefinition.getTemplateIds()>
					<!--- ======================================================================================= --->
					<cfreturn reportDefinitionStruct>
					<cfcatch type="any">
						<cfthrow type="Custom" errorcode="3000" message="FAILS TO GET BIP REPORT DEFINITION" detail="fr.consotel.consoview.api.ibis.publisher.PublisherProxy.getReportDefinition() fails">
					</cfcatch>
				</cftry>
			<cfelse>
				<cfthrow type="Custom" errorcode="3001" message="INVALID CREDENTIALS - REPORT DEFINITION" detail="fr.consotel.consoview.api.ibis.publisher.PublisherProxy.getReportDefinition() fails due to invalid credentials">
			</cfif>
		</cfif>
		<cfreturn structNew()>
	</cffunction>
</cfcomponent>
