<cfcomponent name="ElementCommande" output="false">			
	<!--- insert la liste des produits dans la commande --->
	<cffunction name="saveProduits" access="remote" returntype="numeric">					
		
		<cfargument name="commandeID" type="numeric" required="true">		
		<cfargument name="produitIDs" type="array" required="true" default="0">
		<cfargument name="libelleLignes" type="array" required="true">
		<cfargument name="commentaires" type="array" required="true">
		<cfargument name="affectes" type="array" required="true">
		
		<cfset result = 0>
		<cfset count = 0>
		
		<cfset len = arrayLen(produitIDs)>		
			<cfLoop index="i" from="1" to="#len#">		
				<cfset count = insertProduitInCommande(commandeID,
												produitIDs[i],
												libelleLignes[i],
												commentaires[i],
												affectes[i])>	
														
				<cfset result = result + count>
			</cfloop>					
		<cfreturn result>
	</cffunction>
		
	<cffunction name="insertProduitInCommande" access="private" returntype="numeric">							
		<cfargument name="commandeID" type="numeric" required="true">		
		<cfargument name="produitID" type="numeric" required="true" >
		<cfargument name="libelleLigne" type="string" required="true">
		<cfargument name="commentaire" type="string" required="true">
		<cfargument name="affecte" type="numeric" required="true" default="0">
		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.I_PRODUIT">				
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_commande" value="#commandeID#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idproduit_catalogue" value="#produitID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_libelle_ligne" value="#libelleLigne#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire" value="#commentaire#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_flag_affecte" value="#affecte#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result">		  
		</cfstoredproc>
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="updateProduit" access="public" returntype="numeric">	
		<cfargument name="fakeValue" type="numeric" required="true" >								
		<cfargument name="pdt" type="struct" required="true" >		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.U_PRODUIT_V2">												
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_detail" value="#pdt.elementID#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_libelle_ligne" value="#pdt.libelleLigne#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_commentaire" value="#pdt.commentaire#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result">		  
		</cfstoredproc>					
		<cfreturn result/>			
	</cffunction>
	
	<cffunction name="deleteProduit" access="remote" returntype="numeric" >		
		<cfargument name="ID" type="numeric" required="true" >
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.D_PRODUIT">						
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_detail" value="#ID#"/>		
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result">		  
			</cfstoredproc>				
		<cfcatch type="database">
			<cfthrow type="Erreur" message="Une erreur s'est produite lors de la supression de l'Ã©lÃ©ment" extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		<cfcatch type="application">
			<cfthrow type="Log" message="Votre session a exprirÃ©e.Veuillez vous connecter nouveau." extendedinfo="Plusieurs raisons possibles ....">
		</cfcatch>
		</cftry>
		<cfreturn result/>
	</cffunction>
	
	<cffunction name="deleteAllProduits" access="remote" returntype="numeric" >
		<cfargument name="IDs" type="array" required="true" >
		<cfset result = 0>
		<cfset count = 0>
		
		<cfset len = arrayLen(IDs)>		
			<cfLoop index="i" from="1" to="#len#">		
				<cfset count = deleteProduit(IDs[i])>	
				<cfset result = reslut + count>
			</cfloop>					
		<cfreturn result>					
	</cffunction>
	
	<cffunction name="getProduit" access="remote" returntype="query">
		<cfargument name="ID" type="numeric" required="true" >
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.G_PRODUIT_V2">						
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_detail" value="#ID#"/>		
			<cfprocresult name="result">		  
		</cfstoredproc>	
		<cfreturn result/>				
	</cffunction>
	
	<cffunction name="getProduitsCommande" access="remote" returntype="query">
		<cfargument name="commandeID" type="numeric" required="true" >
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.G_PRODUITS_COMMANDE_V2">						
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcde_detail" value="#commandeID#"/>		
			<cfprocresult name="result">		  
		</cfstoredproc>	
		<cfreturn result/>				
	</cffunction>
	
	<cffunction name="rapprocherProduit" access="remote" returntype="numeric">
		<cfargument name="ID" type="numeric" required="true" >
		<cfargument name="ressourceID" type="numeric" required="true" >
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.R_PRODUIT_V2">					
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idccde_detail" value="#ID#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinv_produit" value="#ressourceID#"/>		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result">			  
		</cfstoredproc>	
		
		<cfreturn result/>				
	</cffunction>
	
	<cffunction name="validerRapprochementProduitsCommande" access="remote" returntype="numeric">
		<cfargument name="commandeID" type="numeric" required="true" >		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.V_PRODUIT_V1">					
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idCommande" value="#commandeID#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result">			  
		</cfstoredproc>
		<cfreturn result/>				
	</cffunction>
	
	<cffunction name="annulerRapprochementProduit" access="remote" returntype="numeric">
		<cfargument name="ID" type="numeric" required="true" >
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M17.AR_PRODUIT_V2">					
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idccde_detail" value="#ID#"/>		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result">			  
		</cfstoredproc>	
		<cfreturn result/>				
	</cffunction>
	
	

	
	
	<cffunction name="annulerTousLesRapprochements" access="remote" returntype="any">
		<cfargument name="IDs" type="array" required="true">
		<cfargument name="commandeID" type="numeric" required="true">
		
		<cfset len = arrayLen(IDs)>	
			<cfset result = 0>	
			<cfLoop index="i" from="1" to="#len#">		
				<cfset result = annulerRapprochementProduit(IDs[i])>					
				<cfif result lt 0 >
					<cfreturn -10>	
				</cfif>
			</cfloop>
			<cfset presult = getProduitsCommande(commandeID)>
		<cfreturn presult>	
	</cffunction>	
		
</cfcomponent>