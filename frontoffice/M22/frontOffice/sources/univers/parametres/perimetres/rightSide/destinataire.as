package univers.parametres.perimetres.rightSide
{
	import event.DestinataireEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import service.destinataires.associedestinatairenoeud.AssocierDestinataireNoeudService;
	import service.destinataires.destinataireprofil.DestinataireProfilService;
	import service.destinataires.listedestinatairenoeud.ListeDestinatairesNoeudService;
	
	import univers.parametres.perimetres.main;
	import univers.parametres.perimetres.rightSide.destinataireComponent.destinataireWin;
	import univers.parametres.perimetres.rightSide.destinataireComponent.profilListIHM;
	
	import vo.DestinataireVo;

	public class destinataire extends destinataireIHM
	{
		public var _mainRef:main;
		private var _destWin:destinataireWin;
		private var _profilDispo:ArrayCollection;
		private var _profilDest:ArrayCollection;
		private var _selectedIndex:Number;
		private var _initCompelte:Boolean=false;
		private var _refSet:Boolean=false;
		public var listeDestNoeudService:ListeDestinatairesNoeudService;
		public var destProfilService:DestinataireProfilService;
		public var assoicerDestinataireNoeudService:AssocierDestinataireNoeudService;
		private var _destinataireData:ArrayCollection;
		
		public function get modeEcriture():Boolean
		{
			return true
		}

		public function destinataire()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		override protected function commitProperties():void
		{
			if (_initCompelte)
			{
				if (_refSet)
				{
					fillDestinataire();
				}
			}
		}

		public function setMainRef(ref:main):void
		{
			_mainRef=ref;

			if (_mainRef != null)
			{
				_refSet=true;
			}
			else
			{
				_refSet=false;
			}

			invalidateProperties();
		}

		public function mainTreeSelectionChanged():void
		{
			invalidateProperties();
		}

		protected function initIHM(e:Event):void
		{

			parentDocument.addEventListener("treeSelectionChanged", treeSelectionChanged);
			parentDocument.addEventListener("organisationChanged", organisationChanged);

			dgDestinataire.addEventListener(Event.CHANGE, onSelectDestChange);
			dgDestinataire.addEventListener(DestinataireEvent.DELETE_DESTINATAIRE_EVENT, removeDestinataire);

			btAdd.visible=modeEcriture;
			btAdd.addEventListener(MouseEvent.CLICK, onBtAddClicked);

			btAddProfil_dest.visible=modeEcriture;
			btAddProfil_dest.addEventListener(MouseEvent.CLICK, onAddProfilToDest);

			btRemoveProfil_dest.visible=modeEcriture;
			btRemoveProfil_dest.addEventListener(MouseEvent.CLICK, onRemoveProfilToDest);

			btAddProfil.visible=modeEcriture;
			btAddProfil.addEventListener(MouseEvent.CLICK, onAddProfil);

			dgProfil_dest.addEventListener(Event.CHANGE, onrefreshProfilsButtons);
			dgProfil_dispo.addEventListener(Event.CHANGE, onrefreshProfilsButtons);

			txtFilterDest.addEventListener(Event.CHANGE, filterDataGrid); // pour filtrer
				
			_selectedIndex=-1;
			dgDestinataire.dataProvider=null;
			dgProfil_dest.dataProvider=null;
			dgProfil_dispo.dataProvider=null;

			refreshProfilsButtons();

			_initCompelte=true;

			listeDestNoeudService=new ListeDestinatairesNoeudService();
			listeDestNoeudService.model.addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT, fillDestinataireResult);

			destProfilService=new DestinataireProfilService();
			destProfilService.model.addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_PROFIL_EVENT, profilToDestResult);
			destProfilService.model.addEventListener(DestinataireEvent.DELETE_DESTINATAIRE_PROFIL_EVENT, profilToDestResult);

			assoicerDestinataireNoeudService=new AssocierDestinataireNoeudService();
			assoicerDestinataireNoeudService.model.addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT, fillDestinataire);
		}
	
		protected function destinataireShowHandler(evt:FlexEvent):void
		{
			if (_mainRef.getSelectedItemNode() != null)
			{
				treeSelectionChanged(new Event(""));
			}
		}

		public function mainOrganisationChanged():void
		{
			treeSelectionChanged(new Event(""));
		}

		private function reSetProfilList():void
		{
			try
			{
				dgProfil_dest.dataProvider=null;
				dgProfil_dispo.dataProvider=null;
			}
			catch (_error:Error)
			{
				trace(_error.getStackTrace());
			}
		}


		protected function onrefreshProfilsButtons(e:Event):void
		{
			refreshProfilsButtons();
		}

		private function refreshProfilsButtons():void
		{
			btAddProfil_dest.enabled=true;
			btRemoveProfil_dest.enabled=true;
			if (dgProfil_dispo.selectedIndex < 0)
				btAddProfil_dest.enabled=false;
			if (dgProfil_dest.selectedIndex < 0)
				btRemoveProfil_dest.enabled=false;
		}

		protected function organisationChanged(e:Event):void
		{
			treeSelectionChanged(new Event(""));
		}

		protected function onSelectDestChange(e:Event):void
		{
			fillProfils();
		}

		protected function closeFiche(e:CloseEvent):void
		{
			fillDestinataire();
		}

		protected function onBtAddClicked(e:MouseEvent):void
		{
			_destWin=PopUpManager.createPopUp(parentApplication as DisplayObject, destinataireWin, true) as destinataireWin;
			_destWin.setMainRef(_mainRef);
			_destWin.assoicerDestNoeudService=assoicerDestinataireNoeudService; /* ici  */
			_destWin.addEventListener(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT, fillDestinataire);
			PopUpManager.centerPopUp(_destWin);
			_destWin.addEventListener("refreshDestinataire", refreshDestinataire);
		}

		protected function onAddProfilToDest(e:MouseEvent):void
		{
			if (dgProfil_dispo.selectedIndex < 0)
				return;

			var IDProfil:Number=dgProfil_dispo.selectedItem.IDPROFIL_ANNUAIRE;
			var IDnode:Number=_mainRef.getSelectedNode();
			var IDDest:int=(dgDestinataire.selectedItem as vo.DestinataireVo).id;

			destProfilService.associerDestinataireProfil(IDDest, IDProfil, IDnode);

			dgProfil_dispo.selectedItem.ASSIGN=1;
			_profilDest=ObjectUtil.copy(_profilDispo) as ArrayCollection;
			refreshDgProfil();
		}

		protected function profilToDestResult(e:DestinataireEvent):void
		{
			refreshProfilsButtons();
			fillDestinataire();
		}

		protected function onAddProfil(e:MouseEvent):void
		{
			var p:profilListIHM=PopUpManager.createPopUp(parentApplication as DisplayObject, profilListIHM, true) as profilListIHM;
			PopUpManager.centerPopUp(p);
			p.addEventListener(CloseEvent.CLOSE, refreshProfils);
			p.addEventListener('UPDATE_PROFILE_LISTE', refreshProfils);
			p.addEventListener('UPDATE_PROFILE_DEST_LISTE', profilListeDest);
		}

		private function refreshProfils(e:Event):void
		{
			fillProfils();
		}
		
		private function profilListeDest(e:Event):void
		{
			refreshProfilsButtons();
			fillDestinataire();
		}
		
		protected function onRemoveProfilToDest(e:MouseEvent):void
		{
			if (dgProfil_dest.selectedIndex < 0)
				return;

			var IDProfil:Number=dgProfil_dest.selectedItem.IDPROFIL_ANNUAIRE;
			var IDnode:Number=_mainRef.getSelectedNode();
			var IDDest:int=(dgDestinataire.selectedItem as vo.DestinataireVo).id;

			destProfilService.removeDestinataireProfil(IDDest, IDProfil, IDnode);

			dgProfil_dest.selectedItem.ASSIGN=0;
			_profilDispo=ObjectUtil.copy(_profilDest) as ArrayCollection;
			refreshDgProfil();
		}

		protected function refreshDestinataire(e:Event):void
		{
			fillDestinataire();
		}

		protected function treeSelectionChanged(e:Event):void
		{
			dgDestinataire.selectedIndex=-1;
			dgProfil_dest.dataProvider=null;
			dgProfil_dest.dataProvider.refresh();
			dgProfil_dispo.dataProvider=null;
			dgProfil_dispo.dataProvider.refresh();

			fillDestinataire();
		}

		private function fillDestinataire(event:Event=null):void
		{
			_selectedIndex=dgDestinataire.selectedIndex;
			var IDnode:Number=_mainRef.getSelectedNode();

			listeDestNoeudService.getListeDestinatairesNoeud(IDnode);
		}

		protected function fillDestinataireResult(e:DestinataireEvent):void
		{
			var Oui:String=ResourceManager.getInstance().getString('M22', 'Oui');
			var Non:String=ResourceManager.getInstance().getString('M22', 'Non');

			dgDestinataire.dataProvider=listeDestNoeudService.model.destinatairesNoeud;
			
			initLabel();

			for each (var obj:vo.DestinataireVo in listeDestNoeudService.model.destinatairesNoeud)
			{
				var prenom:String="";
				var nom:String="";

				if (obj != null)
				{
					if (obj.prenom != null)
					{
						prenom=(obj.prenom).toLowerCase(); // convertir 1 eme lettre en maj
						var firstLetter:String=prenom.charAt(0).toUpperCase();
						var restWord:String=prenom.substr(1, prenom.length);
						prenom=firstLetter + restWord;
					}
					if (obj.nom != null)
					{
						nom=(obj.nom).toUpperCase();
					}
					obj.patronyme=prenom + " " + nom;

					if (obj.profil == "0")
					{
						obj.profil=obj.profil;

					}
					else
						obj.profil=obj.profil;
				}
			}
			dgProfil_dest.dataProvider=null;
			dgProfil_dispo.dataProvider=null;
			selectDest();
		}

		private function selectDest():void
		{
			if (_selectedIndex != -1)
			{
				dgDestinataire.selectedIndex=_selectedIndex;
				dgDestinataire.dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}
		}

		protected function removeDestinataire(e:DestinataireEvent):void
		{
			if (dgDestinataire.selectedIndex < 0)
				return;

			var IDnode:Number=_mainRef.getSelectedNode();
			var IDDest:int=(dgDestinataire.selectedItem as vo.DestinataireVo).id;

			assoicerDestinataireNoeudService.removeDestinataireNoeud(IDDest, IDnode);

			dgDestinataire.dataProvider.removeItemAt(dgDestinataire.selectedIndex);
			initLabel();
		}
		/**
		 * Initilaiser le label qui permet d'afficher le nb des destinataires 
		 */		
		private function initLabel():void
		{
			_destinataireData=listeDestNoeudService.model.destinatairesNoeud;
			var len:Number=_destinataireData.length;
			lb_dest.text=len +" "+ ResourceManager.getInstance().getString('M22', '_destinataire_s_');
		}
		
		private function fillProfils():void
		{
			reSetProfilList();

			if (!dgDestinataire.selectedItem)
			{
				labelProfil.text=ResourceManager.getInstance().getString('M22', 'Profils_du_destinataire_');
				return;
			}
			else
				getNomPrenomDestinataire(dgDestinataire.selectedItem as vo.DestinataireVo);

			var IDnode:Number=_mainRef.getSelectedNode();
			var IDDest:int=(dgDestinataire.selectedItem as vo.DestinataireVo).id;
			
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M22.service.destinataires.destProfilService", "getProfils", getProfilsResult);
			RemoteObjectUtil.callService(op, IDnode.toString(), IDDest.toString(), _mainRef.getSelectedDate());
		}
		
		
		private function getNomPrenomDestinataire(destinataire :DestinataireVo):void
		{
			
			if (destinataire.prenom !=null && destinataire.nom != null) 
				labelProfil.text= ResourceManager.getInstance().getString('M22', 'Profils_du_destinataire_') + destinataire.prenom+" "+ destinataire.nom.toUpperCase();
			else if ( destinataire.prenom ==null && destinataire.nom != null)
				labelProfil.text= ResourceManager.getInstance().getString('M22', 'Profils_du_destinataire_')+destinataire.nom.toUpperCase();
			else if (destinataire.prenom !=null && destinataire.nom == null)
				labelProfil.text= ResourceManager.getInstance().getString('M22', 'Profils_du_destinataire_') + destinataire.prenom;
			else
				labelProfil.text=ResourceManager.getInstance().getString('M22', 'Profils_du_destinataire_');
		}
		/**
		 * remplir les dataprovider des arraycollection des profils  
		 * @param e
		 * 
		 */		
		protected function getProfilsResult(e:ResultEvent):void
		{
			_profilDispo=ArrayCollection(e.result);

			_profilDest=ObjectUtil.copy(_profilDispo) as ArrayCollection;
			refreshDgProfil();
			refreshProfilsButtons();
		}

		private function refreshDgProfil():void
		{
			refreshDgDest();
			refreshDgDispo();
		}

		private function refreshDgDest():void
		{
			dgProfil_dest.dataProvider=_profilDest;
			dgProfil_dest.dataProvider.filterFunction=filterDgDest;
			dgProfil_dest.dataProvider.refresh();
		}

		private function refreshDgDispo():void
		{
			dgProfil_dispo.dataProvider=_profilDispo;
			dgProfil_dispo.dataProvider.filterFunction=filterDgDispo;
			dgProfil_dispo.dataProvider.refresh();
		}

		private function filterDgDest(item:Object):Boolean
		{
			if (item.ASSIGN == "1")
				return true;
			return false;
		}

		private function filterDgDispo(item:Object):Boolean
		{
			if (item.ASSIGN != "1")
				return true;
			return false;
		}

		private function throwResult(result:ResultEvent):void
		{

		}

		private function throwError(result:FaultEvent):void
		{

		}

		/*-----------------------------------------------------------------------------------------------------------------------------*/

		/**
		 * filtrer le data grid
		 * @param event : Event
		 * @return void
		 */
		private function filterDataGrid(event:Event):void
		{
			if (dgDestinataire.dataProvider != null)
			{
				dgDestinataire.dataProvider.filterFunction=filterMyCollection; // le nom de la fonction ;
				dgDestinataire.dataProvider.refresh();
					
				initLabel();
			}
		}

		/**
		 *
		 * permet de filtrer le data grid
		 * @param item : Objet (chaque ligne de data provider)
		 * @return Boolean
		 */
		private function filterMyCollection(item:Object):Boolean
		{
			var itemPrenom:String="";
			var itemNom:String="";

			var searchString:String=StringUtil.trim(txtFilterDest.text.toLowerCase()); // le filtre

			var itemMail:String=(item.mail as String).toLowerCase();

			if (item.prenom != null)
			{
				itemPrenom=(item.prenom as String).toLowerCase();
			}

			if (item.nom != null)
			{
				itemNom=(item.nom as String).toLowerCase();
			}

			if ((itemMail.indexOf(searchString) > -1) || (itemNom.indexOf(searchString) > -1) || (itemPrenom.indexOf(searchString) > -1))
			{
				return true;
			}
			return false;
		}
	/*-----------------------------------------------------------------------------------------------------------------------------*/

	}
}
