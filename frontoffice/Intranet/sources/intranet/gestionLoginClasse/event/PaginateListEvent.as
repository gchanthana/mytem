package intranet.gestionLoginClasse.event
{
    import flash.events.Event;

    public class PaginateListEvent extends Event
    {
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES STATIC - EVENT NAME
        //--------------------------------------------------------------------------------------------//
        public static const LOGIN_LIST_BY_ORGA_LOADED_SUCCESS:String = "login_list_by_orga_loaded_success";
        public static const LOGIN_LIST_BY_ORGA_LOADED_ERROR:String = "login_list_by_orga_loaded_error";
        public static const LOGIN_LIST_LOADED_ERROR:String = "login_list_loaded_error";
        public static const LOGIN_LIST_LOADED_SUCCESS:String = "login_list_loaded_success";
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES PASSEES EN ARGUMENTS
        //--------------------------------------------------------------------------------------------//		
        public var obj:Object = null;

        //--------------------------------------------------------------------------------------------//
        //					CONSTRUCTEUR
        //--------------------------------------------------------------------------------------------//		
        public function PaginateListEvent(type:String, objet:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
            this.obj = objet;
        }

        //--------------------------------------------------------------------------------------------//
        //					METHODES PUBLIC OVERRIDE - COPY OF EVENT
        //--------------------------------------------------------------------------------------------//
        override public function clone():Event
        {
            return new PaginateListEvent(type, obj, bubbles, cancelable);
        }
    }
}