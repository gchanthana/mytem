package utils.utilsForFonction
{
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	
	public class UtilsCommunication
	{
		//----------- METHODES -----------//
		
		/* */
		public function UtilsCommunication()
		{
		}
		
		/* formater le string position */
		public static function strPosition(pos:int):String
		{
			var strPos:String = "";
			switch(pos)
			{
				case 0: strPos = ResourceManager.getInstance().getString('CG05', 'Non_positionn_'); break;
				case 1: strPos = ResourceManager.getInstance().getString('CG05', '1ere'); break;
				case 2: strPos = ResourceManager.getInstance().getString('CG05', '2eme'); break;
				case 3: strPos = ResourceManager.getInstance().getString('CG05', '3eme'); break;
				case 4: strPos = ResourceManager.getInstance().getString('CG05', '4eme'); break;
				case 5: strPos = ResourceManager.getInstance().getString('CG05', '5eme'); break;
				default: strPos = ResourceManager.getInstance().getString('CG05', 'Non_positionn_'); break;
			}
			return strPos;
		}
		
		/* recuperer extension d'un fichier */
		public static function getDetailNameFile(file:String):Object
		{
			var filename		:String = file;
			var extensionIndex	:Number = filename.lastIndexOf( '.' );
			var name			:String = filename.substr( 0, extensionIndex );
			var extension		:String = filename.substr( extensionIndex + 1, filename.length );
			
			var objNameFile		:Object = new Object();
			objNameFile.extension = extension;
			objNameFile.name = name;
			
			return objNameFile;
		}
		
		/* remplacement de characteres avec accents */
		public static function replaceDiacritic(nomPJ:String):String
		{
			var sdiak:String = "àáäčçďéêěéèëïíĺľňóôöŕšťúůüýřžÁÄČĎÉĚEÍÏĹĽŇÓÔÖŔŠŤÚŮÜÝŘŽ";
			var bdiak:String = "aaaccdeeeeeeiillnooorstuuuyrzAACDEEËIILLNOOORSTUUUYRZ";
			var sdiakA:Array = new Array();
			var bdiakA:Array = new Array();
			
			// init replacement characters
			for (var i:int=0; i<sdiak.length; i++)
				sdiakA.push(new RegExp(sdiak.charAt(i), "g"))
			for (var j:int=0; j<sdiak.length; j++)
				bdiakA.push(bdiak.charAt(j))
			
			// replacement characters
			for (var k:int=0; k<sdiakA.length; k++)
				nomPJ = nomPJ.replace(sdiakA[k], bdiakA[k]);
			
			return nomPJ;
		}
	}
}