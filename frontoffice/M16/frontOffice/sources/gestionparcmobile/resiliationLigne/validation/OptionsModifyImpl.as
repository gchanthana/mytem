package gestionparcmobile.resiliationLigne.validation
{
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class OptionsModifyImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListOptions			:DataGrid;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		private var _myElements				:ElementsCommande2 = new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
				
		public var myRessources				:ArrayCollection = new ArrayCollection();
		
		public var numberLine				:int = 0;
		
		//--------------------------------------------------------------------------------------------//
		//					EVENEMENTS VIEWSTACK
		//--------------------------------------------------------------------------------------------//
		
		private static const ETAPE_OPTIONS	:String 			= "ETAPE_OPTIONS";
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function OptionsModifyImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function setMyDataToGrid():void
		{
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			numberLine = 0;
			
			callLater(setDataToGrid);
		}
		
		public function validateCurrentPage():Boolean
		{
			return true;
		}
		
		public function reset():void
		{
			myRessources.removeAll();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//	
		
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
		}
		
		private function showHandler(fe:FlexEvent):void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//	

		private function setDataToGrid():void
		{
			var soustete	:ArrayCollection = Formator.sortFunction(_myElements.LINEOPTIONS, 'SOUSTETE');
			var mySoustete	:String = '';
			var lenOptA		:int = _myElements.OPTIONS.length;
			var lenOptR		:int = _myElements.LINEOPTIONS.length;

			myRessources = new ArrayCollection();
			
			if(lenOptR > 0)
			{
				for(var i:int = 0;i < lenOptR;i++)
				{
					if(mySoustete != soustete[i].SOUSTETE)
					{
						mySoustete = soustete[i].SOUSTETE;
						numberLine++;
						
						for(var j:int = 0;j < lenOptA;j++)
						{
							var ress:RessourcesElements = (_myElements.OPTIONS[j] as RessourcesElements).copyRessources();
								ress.SOUSTETE = mySoustete;
							
								myRessources.addItem(ress);
						}
					}
					
					myRessources.addItem(soustete[i]);
				}
			}
			else
			{
				var myLigne  :ArrayCollection = _myElements.CONFIGURATIONS;
				var lenligne :int = myLigne.length;
				
				for(var k:int = 0;k < lenligne;k++)
				{
					mySoustete = myLigne[k].LIGNE;
					numberLine++;
					
					for(var l:int = 0;l < lenOptA;l++)
					{
						var ress1:RessourcesElements = (_myElements.OPTIONS[l] as RessourcesElements).copyRessources();
							ress1.SOUSTETE = mySoustete;
						
						myRessources.addItem(ress1);
					}
				}
			}
		}
		
		private function findAllSousTete(valueCollection:ArrayCollection):ArrayCollection
		{
			var sousTeteCollection	:ArrayCollection = Formator.sortFunction(valueCollection,"SOUS_TETE");
			var lenSoustete			:int = sousTeteCollection.length;
			var sousTeteInteger		:int = 0;

			
			for(var j:int = 0;j < lenSoustete;j++)
			{
				var sstt:int = int(valueCollection[j].SOUS_TETE)
				
				if(sousTeteInteger != sstt)
				{
					sousTeteInteger = sstt;
					sousTeteCollection.addItem(valueCollection[j])
				}
			}
			
			return sousTeteCollection;
		}

	}
}