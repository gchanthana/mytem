package general.service
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class InfosChargementService extends EventDispatcher
	{
		
		private var _dateInfos:Object;
		
		public function InfosChargementService() 
		{
		}
		
		public function getInfosDates():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.E0DateService",
				"getInfosDates",
				getInfosDatesHandler);				
			op.makeObjectsBindable = true;
			RemoteObjectUtil.callService(op);
		}
		private function getInfosDatesHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				dateInfos = evt.result;
				dispatchEvent(new Event("DATE_LOADED"));
			}
		}
		[Bindable]
		public function set dateInfos(value:Object):void
		{
			_dateInfos = value;
		}
		
		public function get dateInfos():Object
		{
			return _dateInfos;
		}
	}
}