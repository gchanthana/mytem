package service.destinataire.ajoutermodifierdestinataire
{
	import mx.rpc.events.ResultEvent;
	

	internal class AjouterModifierDestinataireHandler
	{
		private var _model:AjouterModifierDestinataireModel;
		
		public function AjouterModifierDestinataireHandler(_model:AjouterModifierDestinataireModel):void
		{
			this._model = _model;
		}
		
		internal function ajouterModifierResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.ajouterModifierResult(evt);
			}
		}	
		internal function supprimerResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.supprimerResult(evt);
			}
		}
		internal function supprimerPlusieursResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.supprimerPlusieursResult(evt);
			}
		}
		
	}
}