package ihm.fonction.FctGestionCatalogue
{
	import composants.util.ConsoviewAlert;
	
	import entity.EqpFournisseurVo;
	import entity.EqpIcecatVo;
	
	import event.fonction.fctGestionCatalogue.FctGestionCatalogueEvent;
	
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import paginatedatagrid.pagination.Pagination;
	
	import service.fonction.FctGestionCatalogue.FctGestionCatalogueServices;
	
	import utils.abstract.AbstractFonction;
	
	public class FctGestionCatalogueImpl extends AbstractFonction
	{
/* ******************************************************************************************************
		1) PUBLIC
****************************************************************************************************** */				
		public var compPaginationDroit:Pagination;
		public var compPaginationGauche:Pagination;
		
		public var dgEquipement:DataGrid;
		public var dgImport:DataGrid;
		
		public var tiFiltreDroite:TextInput;
		public var tiFiltreGauche:TextInput;
		
		public var btnRechercherGauche:Button;
		public var btnRechercherDroit:Button;
		public var btnCorrespondance:Button;
		public var btnAutoriser:Button;
		public var btnInjecter:Button;
		public var btnLier:Button;
		
		
		private var _listeImport:ArrayCollection;
		private var _listeEquipement:ArrayCollection;
		
		
		public function FctGestionCatalogueImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
/* ******************************************************************************************************
		2) PRIVATE
****************************************************************************************************** */		
		private const NBITEM : int = 100;
		
		private var serv:FctGestionCatalogueServices;
		
/* 2.1) PHASE D'INITIALISATION */
		private function init(evt:FlexEvent):void
		{
			initVar();
			initListener();
			initData();
		}
		private function initVar():void
		{
			this.serv = new FctGestionCatalogueServices();
		}
		private function initListener():void
		{
			btnAutoriser.addEventListener(MouseEvent.CLICK,btnAutoriser_clickHandler);
			btnCorrespondance.addEventListener(MouseEvent.CLICK,btnCorrespondance_clickHandler);
			btnInjecter.addEventListener(MouseEvent.CLICK,btnInjecter_clickHandler);
			btnLier.addEventListener(MouseEvent.CLICK,btnLier_clickHandler);
			btnRechercherDroit.addEventListener(MouseEvent.CLICK,btnRechercherDroit_clickHandler);
			btnRechercherGauche.addEventListener(MouseEvent.CLICK,btnRechercherGauche_clickHandler);
			tiFiltreDroite.addEventListener(KeyboardEvent.KEY_UP,tiFiltreDroite_clickHandler);
			tiFiltreGauche.addEventListener(KeyboardEvent.KEY_UP,tiFiltreGauche_clickHandler);
		}
		private function initData():void
		{
			this.serv.myDatas.addEventListener(FctGestionCatalogueEvent.GET_DATA_EVENT,getDataHandler);
			this.serv.getData();
		}
/* 2.2) PHASE HANDLER */
		private function getDataHandler(evt:FctGestionCatalogueEvent):void
		{
			this.dgImport.dataProvider = this.serv.myDatas.LISTEEQPICECAT;
			this.dgEquipement.dataProvider = this.serv.myDatas.LISTEEQPFOURNISSEUR;
			this.compPaginationGauche.initPagination(this.serv.myDatas.NB_EQPICECAT, NBITEM);
			this.compPaginationDroit.initPagination(this.serv.myDatas.NB_EQUIPEMENT, NBITEM);
			
			if(this.serv.myDatas.hasEventListener(FctGestionCatalogueEvent.GET_DATA_EVENT))
				this.serv.myDatas.removeEventListener(FctGestionCatalogueEvent.GET_DATA_EVENT,getDataHandler);
		}
		private function btnRechercherGauche_clickHandler(event:MouseEvent=null):void
		{
			chercherEqpIcecat(tiFiltreGauche.text);
		}
		private function btnCorrespondance_clickHandler(event:MouseEvent):void
		{
			var eqpIcecat:EqpIcecatVo = getSelectedEqpIcecat(true,false)[0];
			
			if(eqpIcecat.LIBELLE )
				chercherEqpCatalogueFournisseur(eqpIcecat.LIBELLE );
			else
			{
				showMsgError()
			}
		}
		private function btnAutoriser_clickHandler(event:MouseEvent):void
		{
			var str:String = getSelectedEqpIcecat()[0] as String;
			
			if(str.length > 0)
				ConsoviewAlert.afficherAlertConfirmation(
					ResourceManager.getInstance().getString('CG08','_fct_gest_catalogue_btn_autoriser__alert__') ,
					ResourceManager.getInstance().getString('CG08','_titre_alert__') ,
					function (evt:CloseEvent):void
					{ 
						if(evt.detail == Alert.YES)
							autoriserEqp(str);
					});
			else
			{
				showMsgError()
			}
		}
		private function btnInjecter_clickHandler(event:MouseEvent):void
		{
			var str:String = getSelectedEqpIcecat()[0] as String;
			
			if(str.length > 0)
				ConsoviewAlert.afficherAlertConfirmation(
					ResourceManager.getInstance().getString('CG08','_fct_gest_catalogue_btn_Injecter__alert__') ,
					ResourceManager.getInstance().getString('CG08','_titre_alert__') ,
					function (evt:CloseEvent):void 
					{ 
						if(evt.detail == Alert.YES)
							injecterEqp(str);
					});
			else
			{
				showMsgError()
			}
		}
		private function btnLier_clickHandler(event:MouseEvent):void
		{
			var eqpIcecat:EqpIcecatVo = getSelectedEqpIcecat(true,false)[0];
			var eqpCatalogue : EqpFournisseurVo = null;
			if(dgEquipement.selectedIndex > 0)
				eqpCatalogue = dgEquipement.selectedItem as EqpFournisseurVo;
			
			if(eqpIcecat != null && eqpCatalogue != null)
			{
				var msg:String = ResourceManager.getInstance().getString('CG08','_fct_gest_catalogue_btn_Lier__alert__part_one')+eqpIcecat.LIBELLE+ResourceManager.getInstance().getString('CG08','_fct_gest_catalogue_btn_Lier__alert__part_two')+eqpCatalogue.LIBELLE;	
				ConsoviewAlert.afficherAlertConfirmation(
					msg ,
					ResourceManager.getInstance().getString('CG08','_titre_alert__') ,
					function (evt:CloseEvent):void 
					{ 
						if(evt.detail == Alert.YES)
							lierEqp(eqpIcecat.ID, eqpCatalogue.ID);
					});
			}
			else
			{
				showMsgError()
			}
		}
		private function btnRechercherDroit_clickHandler(event:MouseEvent=null):void
		{
			chercherEqpCatalogueFournisseur(tiFiltreDroite.text);
		}
		private function tiFiltreDroite_clickHandler(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
				btnRechercherDroit_clickHandler();
		}
		private function tiFiltreGauche_clickHandler(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
				btnRechercherGauche_clickHandler();
		}
/* 2.3) PHASE SERVICE */
	/* RECUP LA LISTE DES EQUIPEMENTS ICECAT */
		private function chercherEqpIcecat(str:String=""):void
		{
			this.serv.myDatas.addEventListener(FctGestionCatalogueEvent.LISTE_EQPICECAT_EVENT,chercherEqpIcecatHandler);
			this.serv.getListeEqpIcecat(str,NBITEM,compPaginationGauche.getCurrentIntervall().indexDepart);
		}
		private function chercherEqpIcecatHandler(evt:FctGestionCatalogueEvent):void
		{
			if(this.serv.myDatas.hasEventListener(FctGestionCatalogueEvent.LISTE_EQPICECAT_EVENT))
				this.serv.myDatas.removeEventListener(FctGestionCatalogueEvent.LISTE_EQPICECAT_EVENT,chercherEqpIcecatHandler);
		}
	/* RECUP LA LISTE DES EQUIPEMENTS DU CATALOGUE FOURNISSEUR */
		private function chercherEqpCatalogueFournisseur(str:String=""):void
		{
			this.serv.myDatas.addEventListener(FctGestionCatalogueEvent.LISTE_EQPFOURNISSEUR_EVENT,chercherEqpCatalogueFournisseurHandler);
			this.serv.getListeFournisseur(str,NBITEM,compPaginationDroit.getCurrentIntervall().indexDepart);
		}
		private function chercherEqpCatalogueFournisseurHandler(evt:FctGestionCatalogueEvent):void
		{
			if(this.serv.myDatas.hasEventListener(FctGestionCatalogueEvent.LISTE_EQPFOURNISSEUR_EVENT))
				this.serv.myDatas.removeEventListener(FctGestionCatalogueEvent.LISTE_EQPFOURNISSEUR_EVENT,chercherEqpCatalogueFournisseurHandler);
		}
	/* AUTORISE UN EQP A ETRE IMPORTE */
		private function autoriserEqp(lstEqpIcecat:String):void
		{
			this.serv.myDatas.addEventListener(FctGestionCatalogueEvent.GIVE_AUTORISATION_EVENT,autoriserEqpHandler);
			this.serv.giveAutorisation(lstEqpIcecat);
		}
		private function autoriserEqpHandler(evt:FctGestionCatalogueEvent):void
		{
			if(this.serv.myDatas.hasEventListener(FctGestionCatalogueEvent.GIVE_AUTORISATION_EVENT))
				this.serv.myDatas.removeEventListener(FctGestionCatalogueEvent.GIVE_AUTORISATION_EVENT,autoriserEqpHandler);
		}
/* INJECTE UN EQUIPEMENT ICECAT DANS LE CATALOGUE FOURNISSEUR */
		private function injecterEqp(listeEqpIcecat:String):void
		{
			this.serv.myDatas.addEventListener(FctGestionCatalogueEvent.INJECTER_EQP_EVENT,injecterEqpHandler);
			this.serv.injecterEqpIcecat(listeEqpIcecat);
		}
		private function injecterEqpHandler(evt:FctGestionCatalogueEvent):void
		{
			if(this.serv.myDatas.hasEventListener(FctGestionCatalogueEvent.INJECTER_EQP_EVENT))
				this.serv.myDatas.removeEventListener(FctGestionCatalogueEvent.INJECTER_EQP_EVENT,injecterEqpHandler);
		}
	/* LIE UN EQUIPEMENT ICECAT ET UN EQUIPEMENT DU CATALOGUE FOURNISSEUR */	
		private function lierEqp(idEqpIcecat:int, idEqpCatFournisseur:int):void
		{
			this.serv.myDatas.addEventListener(FctGestionCatalogueEvent.LIER_EQP_EVENT,lierEqpHandler);
			this.serv.lierEqp(idEqpIcecat,idEqpCatFournisseur);
		}
		private function lierEqpHandler(evt:FctGestionCatalogueEvent):void
		{
			if(this.serv.myDatas.hasEventListener(FctGestionCatalogueEvent.LIER_EQP_EVENT))
				this.serv.myDatas.removeEventListener(FctGestionCatalogueEvent.LIER_EQP_EVENT,lierEqpHandler);
		}
/* 2.4) PHASE UTILS */	
		/**
		 * retourne la liste des équipements sélectionnés
		 * @param oneEqp : Boolean -> 
		 * 		si true, retourne un unique équipement de type eqpIcecatVo
		 * 		si false retourne un array d'objet de type eqpIcecatVo 
		 * @param returnListeIDstring : Boolean -> 
		 * 		si true, retourne un array dont le premier élément est la liste des id des équipements regroupés dans une chaine de caractère au format "id1,id2,id3...".
		**/
		private function getSelectedEqpIcecat(oneEqp:Boolean = false, returnListeIDstring:Boolean = true):Array
		{
			var rsl:Array = [];
			var lg:int = dgImport.dataProvider.length;
			var str:String = ""
			var nbEqp:int = 0;
				
			if(returnListeIDstring)
			{
				rsl.push(str);
			}
			
			for(var i:int = lg-1; i > -1; i --)
			{
				if( (dgImport.dataProvider[i] as EqpIcecatVo).SELECTED )
				{
					if(returnListeIDstring)
						rsl[0] += (dgImport.dataProvider[i] as EqpIcecatVo).ID+",";
					else
						rsl.push(dgImport.dataProvider[i] as EqpIcecatVo);
					if(oneEqp)
						break;
				}
			}
			if(rsl.length < 1 )
			{
				if(!returnListeIDstring && dgImport.selectedIndex > -1)
					rsl.push(dgImport.selectedItem as EqpIcecatVo);
				else
					rsl.push(new EqpIcecatVo());
			}
			return rsl;
		}
		private function showMsgError():void
		{
			ConsoviewAlert.afficherError( ResourceManager.getInstance().getString('CG08','_fct_gest_catalogue_error__alert__'), ResourceManager.getInstance().getString('CG08','_titre_alert__'));
		}
/* ******************************************************************************************************
		3) GETTER / SETTER
****************************************************************************************************** */
		public function get listeImport():ArrayCollection { return _listeImport; }
		[Bindable] public function set listeImport(value:ArrayCollection):void
		{
			if (_listeImport == value)
				return;
			_listeImport = value;
		}
		public function get listeEquipement():ArrayCollection { return _listeEquipement; }
		[Bindable] public function set listeEquipement(value:ArrayCollection):void
		{
			if (_listeEquipement == value)
				return;
			_listeEquipement = value;
		}
	}
}