package gestioncommande.entity
{
	import flash.external.ExternalInterface;
	
	import mx.collections.ArrayCollection;

	public class ArticleItemVO extends ArticleItemBaseVO
	{
		public function ArticleItemVO()
		{
			super.isItem = true;
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty('IDPROFIL_EQUIPEMENT')){
					this.idArticle = obj.IDPROFIL_EQUIPEMENT;
				}
				
				if(obj.hasOwnProperty('CODE_INTERNE')){
					this.referenceArticle = obj.CODE_INTERNE;
				}
				
				if(obj.hasOwnProperty('LIBELLE_PROFIL')){
					this.designationArticle = obj.LIBELLE_PROFIL;
				}
				
				if(obj.hasOwnProperty('PRIX_UNIT')){
					this.prixUnitaire = Number(obj.PRIX_UNIT);
				}
				/******* A supprimer le commentaire******/
				if(obj.hasOwnProperty('COMMENTAIRE')){
					this.commentaire = obj.COMMENTAIRE;
				}
				
				if(obj.hasOwnProperty('IDPRODUIT_CATALOGUE')){
					this.idProduitCatalogue = Number(obj.IDPRODUIT_CATALOGUE);
				}
				
				if(obj.hasOwnProperty('IDTHEME_PRODUIT')){
					this.idThemeProduit = Number(obj.IDTHEME_PRODUIT);
				}
				
				if(obj.hasOwnProperty('LIBELLE_PRODUIT')){
					this.libelleProduit = obj.LIBELLE_PRODUIT;
				}
				
				
				if(obj.hasOwnProperty('SEGMENT_THEME')){
					this.segmentTheme = obj.SEGMENT_THEME;
				}
				
				if(obj.hasOwnProperty('SUR_THEME')){
					this.surTheme = obj.SUR_THEME;
				}
				
				if(obj.hasOwnProperty('THEME_LIBELLE')){
					this.themeLibelle = obj.THEME_LIBELLE;
				}
				
				if(obj.hasOwnProperty('TYPE_ABO')){
					this.typeAbonnement = obj.TYPE_ABO;
				}
				
				if(obj.hasOwnProperty('TYPE_THEME')){
					this.typeTheme = obj.TYPE_THEME;
				}
				
				if(obj.hasOwnProperty('BOOL_ACCES'))
					this.boolAcces = obj.BOOL_ACCES;
				
			}catch(err:Error)
			{
				trace("##Initialisation d'un objet EQUIPEMENT SPIE erronée ");
			}
		}
		
		public function updateData(value:ArticleItemVO):void
		{
			this.idArticle 				= value.idArticle
			this.referenceArticle 		= value.referenceArticle;
			this.designationArticle 		= value.designationArticle;
			this.prixUnitaire 			= value.prixUnitaire;
			this.quantite				= value.quantite;
			this.prixTotale				= value.prixUnitaire * quantite;
			
			this.listReference			= value.listReference;
			this.selected				= value.selected;
			
			this.commentaire				= value.commentaire;
			this.idProduitCatalogue		= value.idProduitCatalogue;
			this.idThemeProduit			= value.idThemeProduit;
			this.libelleProduit			= value.libelleProduit;
			this.segmentTheme			= value.segmentTheme;
			this.surTheme				= value.surTheme;
			this.themeLibelle			= value.themeLibelle;
			this.typeAbonnement			= value.typeAbonnement;
			this.typeTheme				= value.typeTheme;
			this.boolAcces				= value.boolAcces;
		}
		
	}
}