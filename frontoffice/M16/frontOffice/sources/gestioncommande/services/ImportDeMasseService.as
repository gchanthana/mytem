package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class ImportDeMasseService extends EventDispatcher
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		private static var _path			:String = "fr.consotel.consoview.M16.importdemasse.imports.ImportDeMasseService";
		
			//--------------------------------------------------------------------------------------------//
			//					GLOBALES
			//--------------------------------------------------------------------------------------------//
		
		public var infosImport				:ArrayCollection = new ArrayCollection();
		
			//--------------------------------------------------------------------------------------------//
			//					GLOBALES
			//--------------------------------------------------------------------------------------------//
		
		private var _tabColumnsNbr			:int = 0;
		
			//--------------------------------------------------------------------------------------------//
			//					LIBELLE A TRADUIRE - LUPO
			//--------------------------------------------------------------------------------------------//
		
		
		//--------------------------------------------------------------------------------------------//
		//					CONSRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function ImportDeMasseService()
		{
		}

		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		public function getInfosFileImported(uuid:String, fileName:String, nbrColumn:int): void
		{
			infosImport = new ArrayCollection();
			
			_tabColumnsNbr = nbrColumn;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"getInfosFileImported",
																				getInfosFileImportedResultHandler);		
			RemoteObjectUtil.callService(op,uuid, fileName);	
		}
		
		public function getInfosFileImportedMultiReferences(uuid:String, fileName:String, causes:String, subject:String, succesContent:String, errorContent:String, result:String): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"getInfosFileImportedMultiReferences",
																				getInfosFileImportedMultiReferencesResultHandler);		
			RemoteObjectUtil.callService(op,uuid,
											fileName,
											causes,
											subject,
											succesContent,
											errorContent,
											result);	
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					RESULTEVENT PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		//** Récuperer les données de l'import - Handler **//
		private function getInfosFileImportedResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				infosImport = eraseEmptyData(ObjectUtil.copy(re.result as ArrayCollection) as ArrayCollection);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.INFOS_IMPORT));
			}
			else
				popup("Import en erreur.");
		}
		
		private function getInfosFileImportedMultiReferencesResultHandler(re:ResultEvent):void
		{
			if(re.result)
				dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
			else
				popup("Import en erreur.");
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function popup(txt:String):void
		{
			ConsoviewAlert.afficherError(txt, 'Consoview');
		}
		
		private function eraseEmptyData(myColumnsReceived:ArrayCollection):ArrayCollection
		{
			var columns		:ArrayCollection = new ArrayCollection();
			var colNumber	:ArrayCollection = new ArrayCollection();
			var lenColumns	:int = myColumnsReceived.length;
			var lenCol		:int = 0;
			var cptrEmpty	:int = 0;
			
			if(lenColumns > 0)
				colNumber = findHeaderLigne(myColumnsReceived[0]);
			
			lenCol = colNumber.length;
			
			var idxStart	:int = findFirstItemNotEmpty(myColumnsReceived);
			
			for(var i:int = 0;i < lenColumns;i++)
			{
				var myInfos:Object = myColumnsReceived[i]; // données import à la position [i] / toutes les données colonnes à la ligne[i]
				
				cptrEmpty = 0;
				
				for(var j:int = 0;j < lenCol;j++)
				{
					var colName:String = colNumber[j].toString();
					
					if(myInfos[colName] == null || myInfos[colName] == '' || myInfos[colName] == ' ') // si un champs est vide ou null
						cptrEmpty++;
				}
				
				if( _tabColumnsNbr == cptrEmpty) // si nb de colonnes = le nb de champs vide à la ligne [i]
				{
				}
				else
					columns.addItem(myColumnsReceived[i]);
					
			}
			
			return columns;
		}
		
		private function findHeaderLigne(value:Object):ArrayCollection
		{
			var proper	:Object = ObjectUtil.getClassInfo(value);			
			
			return new ArrayCollection(proper.properties);
		}
		
		private function findFirstItemNotEmpty(myColumnsReceived:ArrayCollection):int
		{
			var columns		:ArrayCollection = new ArrayCollection();
			var colNumber	:ArrayCollection = new ArrayCollection();
			var lenColumns	:int = myColumnsReceived.length;
			var lenCol		:int = 0;
			var idxStart	:int = 0;
			
			if(lenColumns > 0)
				colNumber = findHeaderLigne(myColumnsReceived[0]);
						
			lenCol = colNumber.length;
			
			for(var i:int = 1;i < lenColumns;i++)
			{
				var myInfos:Object = myColumnsReceived[i];
				
				for(var j:int = 0;j < lenCol;j++)
				{
					var colName:String = colNumber[j].toString();
					
					if(myInfos[colName] != null && myInfos[colName] != '' && myInfos[colName] != ' ')
					{
						if(idxStart == 0)
							idxStart = i;
					}
				}
			}
			
			return idxStart;
		}
		
	}
}