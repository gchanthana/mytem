package contrat.system
{
	import contrat.entity.InfosClientOperateurVo;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	[Bindable]
	public class GestionInfosClientOperateur extends EventDispatcher
	{
		private var _listeInfosClientOperateur:ArrayCollection=new ArrayCollection();
		private var _listeOperateur:ArrayCollection=new ArrayCollection();

		private static const CFC_GESTIONINFOSCLIENTOPERATEUR:String="fr.consotel.consoview.inventaire.contrats.GestionInfosClientOperateur";

		public function GestionInfosClientOperateur(target:IEventDispatcher=null)
		{
			super(target);
		}

		public function getHistoriqueInfosClientOperateur():void
		{
			var handler:Function=function(event:ResultEvent):void
			{
				if (event.result)
				{
					init(event.result as ArrayCollection);
					dispatchEvent(new Event("getHistoriqueInfosClientOperateur"));
				}

			};

			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, CFC_GESTIONINFOSCLIENTOPERATEUR, "getHistoriqueInfosClientOperateur", handler);
			op.makeObjectsBindable = true;

			RemoteObjectUtil.callService(op);
		}

		public function updateInfosClientOperateur(value:InfosClientOperateurVo, oldValue:InfosClientOperateurVo=null):void
		{
			var handler:Function=function(event:ResultEvent):void
			{
				if (event.result && event.result.OPERATEUR_CLIENT_ID > 0)
				{
					value = InfosClientOperateurVo(event.result) ;
				}
				else
				{
					if (oldValue != null)
					{
						value=oldValue;
					}
				}
			};

			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, CFC_GESTIONINFOSCLIENTOPERATEUR, "updateInfosClientOperateur", handler);


			RemoteObjectUtil.callService(op, value);
		}

		public function saveInfosClientOperateur(value:InfosClientOperateurVo):void
		{
			var handler:Function=function(event:ResultEvent):void
			{
				if (event.result && event.result.OPERATEUR_CLIENT_ID > 0)
				{	
					listeInfosClientOperateur.addItem(event.result);
				}
			};
			
			
				var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, CFC_GESTIONINFOSCLIENTOPERATEUR, "saveInfosClientOperateur", handler);
				op.makeObjectsBindable = true;
	
				RemoteObjectUtil.callService(op, value);	
			 
		}

		public function deleteInfosClientOperateur(value:InfosClientOperateurVo):void
		{
			var handler:Function=function(event:ResultEvent):void
			{
				if(event.result > 0)
				{	
					listeInfosClientOperateur.removeItemAt(listeInfosClientOperateur.getItemIndex(value));
				}
			}
			if(value.BOOL_ACTUEL)
			{
				Alert.show("Vous ne pouvez pas supprimer une entrée courrante.","Infos");
			}
			else
			{
				var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, CFC_GESTIONINFOSCLIENTOPERATEUR, "deleteInfosClientOperateur", handler);


				RemoteObjectUtil.callService(op, value);
			}
		}


		 
		private function init(values:ArrayCollection,boolfiltreActuel:Boolean = true):void
		{
			listeInfosClientOperateur = new ArrayCollection();
			var obj:InfosClientOperateurVo;
			var filterFunction:Function = function (item:InfosClientOperateurVo):Boolean
			{
				if(item.BOOL_ACTUEL) return true
				else return false;
			}

			for (var i:int=0; i < values.length; i++)
			{
				obj=new InfosClientOperateurVo();
				obj.fill(values[i])
				listeInfosClientOperateur.addItem(obj);
			}
			
			if(boolfiltreActuel)
			{
				listeInfosClientOperateur.filterFunction = filterFunction;
				listeInfosClientOperateur.refresh();	
			}
		}


		public function getListOperateurs():void
		{
			var handler:Function=function(event:ResultEvent):void
			{
				var objOpe:Object;

				objOpe=new Object();
				objOpe.OPERATEURID=512;
				objOpe.NOM_OPE="Orange Mobile";
				listeOperateur.addItem(objOpe);

				objOpe=new Object();
				objOpe.OPERATEURID=533;
				objOpe.NOM_OPE="SFR";
				listeOperateur.addItem(objOpe);

				objOpe=new Object();
				objOpe.OPERATEURID=190;
				objOpe.NOM_OPE="Bouygues Telecom";
				listeOperateur.addItem(objOpe);

			};
			handler(null)
		}

		/// ======================= ACCESSORS ================================================================
		public function set listeInfosClientOperateur(value:ArrayCollection):void
		{
			_listeInfosClientOperateur=value;
		}

		public function get listeInfosClientOperateur():ArrayCollection
		{
			return _listeInfosClientOperateur;
		}

		public function set listeOperateur(value:ArrayCollection):void
		{
			_listeOperateur=value;
		}

		public function get listeOperateur():ArrayCollection
		{
			return _listeOperateur;
		}
	}
}