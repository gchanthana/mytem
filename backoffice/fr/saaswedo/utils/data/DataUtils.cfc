<cfcomponent author="Cedric" hint="Utilitaire de manipulation de données (e.g Conversion, Compression, etc...)">
	<cffunction access="public" name="stringByteLength" returntype="Numeric" hint="Retourne la taille d'une chaine en nombre de d'octets (Byte)">
		<cfargument name="stringValue" type="String" required="true" hint="Chaine à mesurer">
		<cfargument name="encoding" type="String" required="false" default="UTF-8" hint="Encodage de la chaine stringValue">
		<cfreturn arrayLen(ARGUMENTS.stringValue.getBytes(ARGUMENTS.encoding))>
	</cffunction>
	
	<cffunction access="public" name="isBase64" returntype="String" hint="Retourne TRUE si base64string est au format base64">
		<cfargument name="base64string" type="String" required="true" hint="Chaine au format base64">
		<!--- ColdFusion 9.0.x (JRun4,Weblogic) :
		- Solution : http://commons.apache.org/proper/commons-codec/
		- Autre solution : Vérifier si toBinary() lève une exception ou pas
		--->
		<cfset var Base64=createObject("java","org.apache.commons.codec.binary.Base64")>
		<!--- Méthode disponible dans la version fournie avec ColdFusion 9.0.2+ --->
		<cfreturn Base64.isArrayByteBase64(ARGUMENTS.base64string.getBytes("UTF-8"))>
	</cffunction>
	
	<cffunction access="public" name="base64ToString" returntype="String" hint="Retourne la conversion base64string en String">
		<cfargument name="base64string" type="String" required="true" hint="Chaine au format base64">
		<!--- ColdFusion 9 (JRun4,Weblogic) :
		- Solution : http://commons.apache.org/proper/commons-codec/
		- Autre solution (Java 6 SDK): http://docs.oracle.com/javase/6/docs/api/index.html?javax/xml/bind/DatatypeConverter.html
		--->
		<cfset var Base64=createObject("java","org.apache.commons.codec.binary.Base64")>
		<!--- Méthode disponible dans la version fournie avec ColdFusion 9.0.2+ --->
		<cfreturn createObject("java","java.lang.String").init(Base64.decodeBase64(ARGUMENTS.base64string.getBytes("UTF-8")))>
	</cffunction>
	
	<cffunction access="public" name="byteArrayToString" returntype="String"
	hint="Convertit le contenu d'un ByteArrayOutputStream Java en String. Une chaine vide est retournée si byteArray n'est pas défini">
		<cfargument name="byteArray" type="Any" required="true" hint="Objet Java de type java.io.ByteArrayOutputStream">
		<cfif isDefined("ARGUMENTS.byteArray")>
			<cfset var javaByteArray=ARGUMENTS.byteArray>
			<cfset var javaByteArrayClass="java.io.ByteArrayOutputStream">
			<!--- Une comparaison avec getClass().getName() peut etre erronée car héritage possible --->
			<cfif isInstanceOf(javaByteArray,javaByteArrayClass)>
				<cfreturn javaByteArray.toString()>
			<cfelse>
				<cfset var javaClass=javaByteArray.getClass().toString()>
				<cfthrow type="Custom" message="Unable to convert Java byte array to String"
				detail="Java object of type '#javaClass#' is not supported. Should be of type #javaByteArrayClass#">
			</cfif>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="utcToLocalDate" returntype="Date" hint="Retourne la date locale correspondant à la date UTC utcString">
		<cfargument name="utcString" type="String" required="true" hint="Date UTC (ISO 8601)">
		<cfset var utcDateString=ARGUMENTS.utcString>
		<cfset var datatypeConverter=createObject("java","javax.xml.bind.DatatypeConverter")>
		<!--- Pour une raison inconnue l'appel à DatatypeConverter.parseDateTime() ne fonctionne ici que par introspection Java --->
		<cfset var datatypeConverterClass=createObject("java","javax.xml.bind.DatatypeConverter").getClass()>
		<cfset var stringClass=createObject("java","java.lang.String").getClass()>
		<cfset var parseMethod=datatypeConverterClass.getMethod("parseDateTime",[stringClass])>
		<!--- Appel à DatatypeConverter.parseDateTime() par introspection Java --->
		<cfset var javaCalendar=parseMethod.invoke(datatypeConverter,[utcDateString])>
		<cfreturn javaCalendar.getTime()>
	</cffunction>
	
	<cffunction access="public" name="isValidUTC" returntype="Boolean"
	hint="Retourne TRUE si utcString est une date UTC (ISO 8601) valide. Si une exception liée au parsing du format UTC est rencontrée alors
	l'exception est ignorée et la valeur FALSE est retournée. Tout autre type d'exception est à nouveau levé">
		<cfargument name="utcString" type="String" required="true" hint="Date UTC (ISO 8601)">
		<cfset var utcDateString=ARGUMENTS.utcString>
		<!--- Une valeur numérique est considérée comme valide par le SDK. Donc on vérifie que la valeur contienne une lettre T --->
		<cfset var utcRegexPattern="[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2})?)+([\+,-]?[0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2})?)?">
		<cfif isValid("regex",utcDateString,utcRegexPattern)>
			<cfset var datatypeConverter=createObject("java","javax.xml.bind.DatatypeConverter")>
			<!--- Pour une raison inconnue l'appel à DatatypeConverter.parseDateTime() ne fonctionne ici que par introspection Java --->
			<cfset var datatypeConverterClass=createObject("java","javax.xml.bind.DatatypeConverter").getClass()>
			<cfset var stringClass=createObject("java","java.lang.String").getClass()>
			<cfset var parseMethod=datatypeConverterClass.getMethod("parseDateTime",[stringClass])>
			<cftry>
				<!--- Appel à DatatypeConverter.parseDateTime() par introspection Java --->
				<cfset var javaCalendar=parseMethod.invoke(datatypeConverter,[utcDateString])>
				<cfreturn TRUE>
				<cfcatch type="Any">
					<cfset var exceptionObject=CFCATCH>
					<!--- Extraction de la cause de l'exception : Exception Java --->
					<cfif structKeyExists(CFCATCH,"cause")>
						<cfset exceptionObject=CFCATCH.cause>
					</cfif>
					<!--- Type de l'exception Java levée par DatatypeConverter quand le format UTC est invalide --->
					<cfif structKeyExists(exceptionObject,"TYPE") AND (compare(exceptionObject.TYPE,"java.lang.IllegalArgumentException") EQ 0)>
						<cfreturn FALSE>
					<!--- Toute autre exception est levée à nouveau --->
					<cfelse>
						<cfrethrow>
					</cfif>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="uncompressBase64GZIP" returntype="String"
	hint="Retourne au format String le résultat de la décompression GZIP des données compressedBase64data (Données au format Base64).
	Lève une exception Java de type java.util.zip.DataFormatException si le format GZIP est invalide">
		<cfargument name="compressedBase64data" type="String" required="true" hint="Données compressées au format GZIP et encodées en Base64">
		<!--- Taille fixe du tampon de décompression --->
		<cfset var DEFLATER_BUFFER_INIT_SIZE=64 * 1024>
		<cfset var inflatedString="">
		<!--- Décodage Base64 --->
		<cfset var base64data=ARGUMENTS.compressedBase64data>
		<cfset var Base64=createObject("java","org.apache.commons.codec.binary.Base64")>
		<cfset var deflaterBuffer=Base64.decode(base64data.getBytes("UTF-8"))>
		<!--- Buffer de décompression de taille dynamique : La taille est une expression pour forcer le cast en nombre dans Java --->
		<cfset var outputStringBuffer=createObject("java","java.lang.StringBuffer").init(64 * 1024)>
		<!--- Tampon de décompression --->
		<cfset var ByteBuffer=createObject("java","java.nio.ByteBuffer").allocate(DEFLATER_BUFFER_INIT_SIZE)>
		<cfset var ByteBufferWithZero=createObject("java","java.nio.ByteBuffer").allocate(DEFLATER_BUFFER_INIT_SIZE)>
		<cfset var inflaterBuffer=ByteBuffer.array()>
		<!--- Décompression GZIP --->
		<cfset var hasMoreDataToInflate=TRUE>
		<cfset var inflater=createObject("java","java.util.zip.Inflater").init()>
		<cfset inflater.setInput(deflaterBuffer,0,arrayLen(deflaterBuffer))>
		<cfloop condition="hasMoreDataToInflate EQ TRUE">
			<cfset inflaterByteNb=inflater.inflate(inflaterBuffer)>
			<!--- Le nombre de bytes décompressés est utilisé dans la condition car il arrive que inflater.finished() ne retourne jamais TRUE --->
			<cfset hasMoreDataToInflate=(inflaterByteNb GT 0)>
			<cfif hasMoreDataToInflate>
				<cfset inflatedString=createObject("java","java.lang.String").init(inflaterBuffer,0,inflaterByteNb,"UTF-8")>
				<cfset outputStringBuffer.append(inflatedString)>
				<!--- Réinitialisation du buffer et de son contenu --->
				<cfset ByteBuffer.clear()>
				<cfset ByteBuffer.put(ByteBufferWithZero.array())>
				<cfset inflaterBuffer=ByteBuffer.array()>
			</cfif>
		</cfloop>
		<cfset inflater.end()>
		<cfreturn outputStringBuffer.toString()>
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.utils.data.DataUtils" hint="Constructeur">
		<cfreturn THIS>
	</cffunction>
</cfcomponent>