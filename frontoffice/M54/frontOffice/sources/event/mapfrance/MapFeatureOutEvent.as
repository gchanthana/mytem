package event.mapfrance
{
	import flash.events.Event;
	
	/**
	 * classe relative à l'événement MapFeatureOutEvent
	 */
	public class MapFeatureOutEvent extends Event
	{
		//bubbles indiquent si l'evenement peut être propagé, cancelable s'il peut être arrêté en cours de propagation
		public function MapFeatureOutEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event	
		{
			return new MapFeatureOutEvent(type,bubbles,cancelable);
		}
	}
}