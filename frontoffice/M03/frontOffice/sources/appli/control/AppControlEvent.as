package appli.control {
	import flash.events.Event;

	public class AppControlEvent extends Event {
		public static const LOGOFF_EVENT:String = "USER LOGOFF";
		public static const LOGOFF_FAULT_EVENT:String = "USER LOGOFF FAULT";
		public static const BACKOFFICE_LOGOFF_EVENT:String = "BACKOFFICE USER SESSION LOGOFF";
		
		public function AppControlEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
	}
}
