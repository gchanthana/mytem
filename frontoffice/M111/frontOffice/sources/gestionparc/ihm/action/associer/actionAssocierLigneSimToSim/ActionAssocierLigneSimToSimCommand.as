package gestionparc.ihm.action.associer.actionAssocierLigneSimToSim
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierLigneSimToSimCommand implements ICommand
	{
		private var receiver:ActionAssocierLigneSimToSimReceiver=null;

		public function ActionAssocierLigneSimToSimCommand(rec:ActionAssocierLigneSimToSimReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
