package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Facture")]
	
	 
	[Bindable]
	public class Facture
	{

		public var operateurId:Number = 0;
		public var operateurLibelle:String = "";
		public var numero:String = "";
		public var compteFacturation:String = "-----------";
		public var compteFacturationId:Number = 0;
		public var periodeId:Number = 0;
		public var dateDebut:Date = null;
		public var dateFin:Date = null;
		public var dateEmission:Date = null;
		public var montant:Number = 0;
		public var montantVerifiable:Number = 0;
		public var montantCalculeFacture:Number = 0;
		public var montantCalcule:Number = 0;
		public var libelle:String = "";
		public var creeePar:String = "";
		public var visee:Number = 0;
		public var controlee:Number = 0;		
		public var controleeInv:Number = 0;
		
		public var exportee:Number = 0;
		public var commentaireViser:String = "";
		public var commentaireControler:String = "";
		public var commentaireControlerInv:String = "";
		public var commentaireExporter:String = "";
		
		
		public var dateViser:Date = null;
		public var dateControler:Date = null;
		public var dateControlerInv:Date = null;
		public var dateExporter:Date = null;
		public var refClientId:Number = 0;
		public var ratio:Number = 0;
		
		//--ITERATION 5
		public var viseeAna:Number=0;		
		public var commentaireViserAna:String="";
		public var dateViserAna:Date = null;
		//--
		
		
		public function Facture()
		{
		}
	}
}
