package appli.connection
{
	import mx.core.Container;
	
	import appli.events.LoginEvent;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import entity.AppParams;
	import entity.ObjectError;
	
	import general.abstract.AbstractConnection;
	
	import interfaceClass.IConnection;
	
	public class ConnectionSSOsfr extends AbstractConnection implements IConnection
	{
		public function ConnectionSSOsfr(progress:ConsoProgressBar)
		{
			trace("ConnectionSSOsfr.ConnectionSSOsfr(progress)");
			
			super(progress);
		}
		override public function MustBeDisplay():Boolean
		{
			trace("ConnectionSSOsfr.MustBeDisplay()");
			
			return false;
		}
		// --------------------------------------------------------------------------------------------------------------- //
		// ---------------------------------------- FONCTION PUBLIC ------------------------------------------------------ //
		// --------------------------------------------------------------------------------------------------------------- //
		override public function goLogin(param:AppParams, container:Container = null):void
		{
			trace("ConnectionSSOsfr.login(param, container)");
			
			super.goLogin(param,container);
			
			loginService.connectViaSSOSFR(params.token,params.codeApp,params.codeConnection);
		}
		/**
		 *	CreateSession() 
		 * 	Override de cette function car selon le résultat de l'étape 1 ( connectViaSSOSFR), 
		 * 		soit on créé la session "classiquement"
		 * 		soit on passe à l'étape 3 de la connection SSO 
		 */
		override public function CreateSession(evt:LoginEvent):void
		{
			trace("ConnectionSSOsfr.CreateSession(evt)");
			
			try
			{
				// Si on récupère un object SESSION 
				if(evt.authInfos != null
					&& !evt.authInfos.hasOwnProperty("CODEERREUR") )
				{
					if(CvAccessManager.singletonInstance != null)
					{
						// Si la session n'est pas créée correctement, on retourne l'erreur -998 
						if(!updateSession(evt.authInfos))
						{
							avertirApplication(false,-998);
						}
							// Sinon, tout va bien
						else
						{
							avertirApplication(true);
						}
					}
				}
					// Sinon, on récupère un object ERROR, on avertit de l'erreur en passant l'object en paramètre afin que l'app gère l'affichage de l'erreur.
					// Si le code erreur = -10 ( login introuvable ) , on passe à la 3ème étape du login SSO : la création du login automatiquement.
					// Ceci implique une modification de la progressBar
				else
				{
					if(evt.authInfos.CODEERREUR == -10)
					{
						progress.setTitle("Création du login ...");
						loginService.createLoginByTitu(params.codeApp,params.codeConnection);
					}
					else
					{
						avertirApplication(false,-999,evt.authInfos);
					}
				}
			}
			// En cas d'erreur, on retourne un objet ERROR à l'application
			catch(err:Error)
			{
				var error:ObjectError = new ObjectError();
				error.codeErreur = -999;
				error.message = err.message;
				avertirApplication(false,-999,error);
			}
		}
		// --------------------------------------------------------------------------------------------------------------- //
		// ---------------------------------------- FONCTION PRIVATE ----------------------------------------------------- //
		// --------------------------------------------------------------------------------------------------------------- //
		// EVENT HANDLER
	}
}