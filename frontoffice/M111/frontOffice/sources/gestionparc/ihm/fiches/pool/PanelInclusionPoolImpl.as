package gestionparc.ihm.fiches.pool
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.CheckBoxChangeEvent;
	import gestionparc.utils.AbstractBaseViewImpl;
	import gestionparc.utils.PanelExclusionInclusionUtils;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	[Bindable]
	public class PanelInclusionPoolImpl extends AbstractBaseViewImpl
	{
		public var ti_search:TextInput;
		public var rbg_display_search:RadioButtonGroup;
		public var rb_1:RadioButton;
		public var chbx_all:CheckBox;
		public var chbx_no_pool:CheckBox;
		public var dg_inclusion_elements:DataGrid;
		public var btn_valid:Button;
		public var btn_search:Button;
		public var nbEltTotal:int;
		public var nbEltSelected:int=0;
		public var cb_rechercher_dans:ComboBox;
		public var txtFilter:TextInputLabeled;

		private var gestionPoolData:GestionPool=new GestionPool();
		private var listeTemp:ArrayCollection=new ArrayCollection();
		private var tab:Array=[];
		private var typeElement:String="";
		private var typeIndex:int;
		private var myPanelExclusionInclusionUtils:PanelExclusionInclusionUtils;

		public static const REINIT_DATAGRID:String="reinitDatagrid";
		public static const SEARCH_DATAGRID:String="searchDatagrid";


		public function PanelInclusionPoolImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			this.addEventListener(CheckBoxChangeEvent.CHBX_SELECTED, chbxChangeEvent);
			this.addEventListener(CheckBoxChangeEvent.CHBX_NO_SELECTED, chbxChangeEvent);
			txtFiltre.addEventListener(Event.CHANGE, filtrerGird);
			if (SpecificationVO.getInstance().idPoolNotValid > 0 && !SpecificationVO.getInstance().idPool)
			{
				this.title+=SpecificationVO.getInstance().labelPoolNotValid;
			}
			else
			{
				this.title+=SpecificationVO.getInstance().labelPool;
			}
		}

		/**
		 *	Selection de tous les elements dans le datagrid
		 **/
		public function selectAllChbx(evt:Event):void
		{
			if (listeTemp != null)
			{
				var longueur:int=(listeTemp as ArrayCollection).length;
				for (var i:int=0; i < longueur; i++)
				{
					(dg_inclusion_elements.dataProvider as ArrayCollection).getItemAt(i).SELECTED=(evt.target as CheckBox).selected;
				}
				listeTemp.refresh();
				if (chbx_all.selected == true)
				{
					nbEltSelected=nbEltTotal;
					btn_valid.enabled=true;
				}
				else
				{
					nbEltSelected=0;
					btn_valid.enabled=false;
				}
			}
		}

		/**
		 *	Selection / deselection d'un element dans le datagrid
		 **/
		private function chbxChangeEvent(evt:*):void
		{
			var type:String=evt.type;

			if (type == CheckBoxChangeEvent.CHBX_SELECTED)
			{
				nbEltSelected++;
				if (nbEltSelected == nbEltTotal)
				{
					chbx_all.selected=true;
				}

				if (nbEltSelected >= 1)
				{
					btn_valid.enabled=true;
				}
			}
			else if (type == CheckBoxChangeEvent.CHBX_NO_SELECTED)
			{
				nbEltSelected--;
				if (nbEltSelected != nbEltTotal)
				{
					chbx_all.selected=false;
				}

				if (nbEltSelected == 0)
				{
					btn_valid.enabled=false;
				}
			}
		}

		/**
		 *	(FlexEvent) Validation texte de recherche
		 **/
		private function textSearchEnterHandler(evt:FlexEvent):void
		{
			if (cb_rechercher_dans.selectedIndex != -1)
			{
				chbx_all.selected=false;
				nbEltSelected=0;
				nbEltTotal=0;
				btn_valid.enabled=false;
				getElementsToAdd();
			}
		}

		/**
		 *	(MouseEvent) Click bouton de recherche
		 **/
		public function clickSearchHandler(evt:MouseEvent):void
		{
			if (cb_rechercher_dans.selectedIndex != -1)
			{
				chbx_all.selected=false;
				nbEltSelected=0;
				nbEltTotal=0;
				btn_valid.enabled=false;
				getElementsToAdd();
			}
		}

		/**
		 *	@params: type (int) = 1 --> collaborateur
		 * 				 		= 2 --> terminal
		 * 				 		= 3 --> ligne
		 * 				 		= 4 --> sim
		 *			 etat (int) = 0 --> dans un autre pool
		 * 					    = 1 --> dans aucun pool
		 * 					    = 2 --> dans un autre pool + dans aucun pool
		 * 					    = 3 --> dans le pool (idPool)
		 *	@return: Affichage de la liste des éléments à inclure dans le pool
		 * **/
		public function getElementsToAdd():void
		{
			var rechercherDans:int=1;
			var segment:int=0;

			if (cb_rechercher_dans.selectedIndex != -1) // by default
			{
				rechercherDans=cb_rechercher_dans.selectedItem.data;
			}

			gestionPoolData.getListCollabOutWithElements(SpecificationVO.getInstance().idPoolNotValid, (chbx_no_pool.selected) ? 0 : 2, (rbg_display_search.selectedValue == 0) ? 0 : 1, rechercherDans, textSearch, segment);
			gestionPoolData.addEventListener(GestionPool.LISTE_COLLAB_LOADED, listeElementHandler);
		}

		/**
		 *	Chargement OK de la liste des éléments à inclure dans le pool
		 **/
		private function listeElementHandler(evt:Event):void
		{
			var type:String=evt.type;
			switch (type)
			{
				case GestionPool.LISTE_COLLAB_LOADED:
					formatData(gestionPoolData.listeCollab);
					break;
				case GestionPool.LISTE_TERM_LOADED:
					formatData(gestionPoolData.listeTerm);
					break;
				case GestionPool.LISTE_LIGNE_LOADED:
					formatData(gestionPoolData.listeLigne);
					break;
				case GestionPool.LISTE_SIM_LOADED:
					formatData(gestionPoolData.listeSim);
					break;
			}

			dg_inclusion_elements.dataProvider=listeTemp;

			//nb total d'éléments dans le datagrid
			nbEltTotal=listeTemp.length;
		}

		private function formatData(value:ArrayCollection):void
		{
			if (value)
			{
				var newColl:ArrayCollection=new ArrayCollection();
				var tmpArr:Array=value.source;
				var longueur:int=tmpArr.length;

				for (var i:int=0; i < longueur; i++)
				{
					var item:AbstractMatriceParcVO=new AbstractMatriceParcVO();

					item.IDEMPLOYE=tmpArr[i].IDEMPLOYE;
					item.IDSIM=tmpArr[i].IDSIM;
					item.IDSOUS_TETE=tmpArr[i].IDSOUS_TETE;
					item.IDTERMINAL=tmpArr[i].IDTERMINAL;

					item.COLLABORATEUR=(tmpArr[i].COLLABORATEUR) ? tmpArr[i].COLLABORATEUR : "";
					item.DISTRIBUTEUR=(tmpArr[i].DISTRIBUTEUR) ? tmpArr[i].DISTRIBUTEUR : "";
					item.T_IMEI=(tmpArr[i].IMEI) ? tmpArr[i].IMEI : "";
					item.SOUS_TETE=(tmpArr[i].LIGNE) ? tmpArr[i].LIGNE : "";
					item.T_MODELE=(tmpArr[i].MODELE) ? tmpArr[i].MODELE : "";
					item.S_IMEI=(tmpArr[i].NUM_SIM) ? tmpArr[i].NUM_SIM : "";
					item.OPERATEUR=(tmpArr[i].OPERATEUR) ? tmpArr[i].OPERATEUR : "";
					/**item.TERMINAL=(tmpArr[i].TERMINAL) ? tmpArr[i].TERMINAL : "";
					item.LIGNE_ETAT=(tmpArr[i].LIGNE_ETAT) ? tmpArr[i].LIGNE_ETAT : "";  pas utilisé*/
					item.E_MATRICULE=(tmpArr[i].MATRICULE) ? tmpArr[i].MATRICULE : "";

					item.COMPTE_FACTURATION=(tmpArr[i].COMPTE_FACTURATION) ? tmpArr[i].COMPTE_FACTURATION : "";
					item.SOUS_COMPTE=(tmpArr[i].SOUS_COMPTE) ? tmpArr[i].SOUS_COMPTE : "";
					item.T_NO_SERIE=(tmpArr[i].NO_SERIE) ? tmpArr[i].NO_SERIE : "";

					newColl.addItem(item);
				}
				listeTemp=newColl;
			}
		}

		/**
		 *	(MouseEvent) Fonction d'initialisation de la recherche
		 **/
		public function clickInitHandler(evt:MouseEvent):void
		{
			ti_search.text="";
			nbEltSelected=0;
			nbEltTotal=0;
			cb_rechercher_dans.selectedIndex=-1;
			chbx_all.selected=false;
			chbx_no_pool.selected=false;
			rb_1.selected=true;
			dg_inclusion_elements.dataProvider=null
			btn_valid.enabled=false;
			btn_search.enabled=false;
			txtFiltre.text="";
		}

		public function get textSearch():String
		{
			return ti_search.text;
		}

		private function filtrerGird(ev:Event):void
		{
			if (dg_inclusion_elements.dataProvider != null)
			{
				(dg_inclusion_elements.dataProvider as ArrayCollection).filterFunction=filtrer;
				(dg_inclusion_elements.dataProvider as ArrayCollection).refresh();
			}
		}

		private function filtrer(item:Object):Boolean
		{
			if ((String(item.COLLABORATEUR).toLowerCase().search(txtFiltre.text) != -1) || (String(item.MATRICULE).toLowerCase().search(txtFiltre.text) != -1) || (String(item.IMEI).toLowerCase().search(txtFiltre.text) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltre.text) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltre.text) != -1) || (String(item.NUM_SIM).toLowerCase().search(txtFiltre.text) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltre.text) != -1) || (String(item.COMPTE_FACTURATION).toLowerCase().search(txtFiltre.text) != -1) || (String(item.SOUS_COMPTE).toLowerCase().search(txtFiltre.text) != -1))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private function buildTabElements():void
		{
			var doublon:Number=0;

			for each (var obj:AbstractMatriceParcVO in listeTemp)
			{
				if (obj.SELECTED == true)
				{
					//stocker dans le tableau une liste d'idemploye, d'idequipement (terminal/sim) ou d'idsoustete

					if (Number(obj.IDEMPLOYE) > 0)
					{
						if (doublon != Number(obj.IDEMPLOYE))
						{
							doublon=Number(obj.IDEMPLOYE);
							tab.push(doublon);
						}
						typeElement="collab";
					}
					else if (Number(obj.IDTERMINAL) > 0)
					{
						if (doublon != Number(obj.IDTERMINAL))
						{
							doublon=Number(obj.IDTERMINAL);
							tab.push(doublon);
						}
						typeElement="term";
					}
					else if (obj.IDSOUS_TETE > 0)
					{
						if (doublon != obj.IDSOUS_TETE)
						{
							doublon=obj.IDSOUS_TETE;
							tab.push(doublon);
						}
						typeElement="ligne";
					}
					else if (obj.IDSIM > 0)
					{
						if (doublon != obj.IDSIM)
						{
							doublon=obj.IDSIM;
							tab.push(doublon);
						}
						typeElement="sim";
					}
				}
			}
		}

		public function getElementForInclusion():String
		{
			var myXML:String="<elements>";
			for each (var obj:AbstractMatriceParcVO in dg_inclusion_elements.dataProvider)
			{
				if (obj.SELECTED)
					myXML+="<element><id>" + obj.ID + "</id><idemploye>" + obj.IDEMPLOYE + "</idemploye><idsim>" + obj.IDSIM + "</idsim><idterminal>" + obj.IDTERMINAL + "</idterminal><idsoustete>" + obj.IDSOUS_TETE + "</idsoustete></element>";
			}
			myXML+="</elements>";
			return myXML;
		}

		public function validPopup(evt:MouseEvent):void
		{
			buildTabElements();
			if (tab.length > 0 && nbEltSelected <= 100)
			{
				myPanelExclusionInclusionUtils=new PanelExclusionInclusionUtils(null, null);

				if (SpecificationVO.getInstance().idPoolNotValid > 0 && !SpecificationVO.getInstance().idPool)
				{
					myPanelExclusionInclusionUtils.addElementsToPoolV2(SpecificationVO.getInstance().idPoolNotValid, getElementForInclusion());
				}
				else
				{
					myPanelExclusionInclusionUtils.addElementsToPoolV2(SpecificationVO.getInstance().idPool, getElementForInclusion());
				}

				myPanelExclusionInclusionUtils.gestionPoolData.addEventListener(GestionPool.LISTE_INCLUDED, closePanelHandler);
			}
			else if (tab.length > 0 && nbEltSelected > 100)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_pouvez_inclure_que_100__l_ments___l'), ResourceManager.getInstance().getString('M111', 'Attention__'));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_au_moins_un__l_m'), ResourceManager.getInstance().getString('M111', 'Attention__'));
			}
		}

		public function notCallPanelConfirmation(evt:Event):void
		{
			closePanel(null);
		}

		public function cancelPopup(evt:MouseEvent):void
		{
			closePanel(null);
		}

		public function closePanel(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function closePanelHandler(evt:Event):void
		{
			closePanel(null);
			this.dispatchEvent(new Event(PanelExclusionInclusionUtils.OPERATION_POOL_DONE));
		}

		private function initBuildingTabHandler(evt:Event):void
		{
			tab=null;
			tab=[];
		}

		/**
		 *	AJOUT DU POPUP
		 **/
		private function addPopup(popupToAdd:TitleWindow):void
		{
			PopUpManager.addPopUp(popupToAdd, parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(popupToAdd);
		}
	}
}
