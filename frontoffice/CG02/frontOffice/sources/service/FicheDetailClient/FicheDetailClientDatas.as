package service.FicheDetailClient
{
	import entity.ClientVO;
	
	import event.ficheDetailClient.FicheDetailClientEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class FicheDetailClientDatas extends EventDispatcher
	{
		private var _client:ClientVO;
		
		public function treatProcessGetDetailClient(objResult:ArrayCollection):void
		{
			_client = new ClientVO();
			_client.fill(objResult.getItemAt(0),objResult);
			this.dispatchEvent(new FicheDetailClientEvent(FicheDetailClientEvent.proc_DETAIL_CLIENT_CONTRACTUEL_EVENT));
		}
		
		
		public function get client():ClientVO { return _client; }
		public function set client(value:ClientVO):void
		{
			if (_client == value)
				return;
			_client = value;
		}
	}
	
}