<cfif isDefined('url.period')>
	<cfswitch expression="#url.period#" >
		<cfcase value="this_month" >
			<cfquery name="qGetShoot" dataSource="ROCOFFRE">
				SELECT user_jira, count(*) nb 
				FROM CIR_URL 
				where to_char(date_page,'yyyy/mm')=to_char(sysdate,'yyyy/mm')
				group by user_jira
				order by nb desc
			</cfquery>
		</cfcase>
		<cfdefaultcase>
			<cfquery name="qGetShoot" dataSource="ROCOFFRE">
				SELECT user_jira, count(*) nb 
				FROM CIR_URL 
				group by user_jira
				order by nb desc
			</cfquery>
		</cfdefaultcase>
	</cfswitch>
<cfelse>
	<cfquery name="qGetShoot" dataSource="ROCOFFRE">
		SELECT user_jira, count(*) nb 
		FROM CIR_URL 
		where to_char(date_page,'yyyy/mm')=to_char(sysdate,'yyyy/mm')
		group by user_jira
		order by nb desc
	</cfquery>
</cfif>



<html>
	<head>
		<title>CIR SHOOTER v1.1</title>
		 <style type="text/css">
		 	td {
			 	font-family: Verdana,Geneva,sans-serif;
			 	font-size: 10px;
			 }
			 .ligne {
				 background-color: gray;
			 }
			 .titre {
				 font-size: 24px;
				 text-align: right;
				 vertical-align: middle;
			 }
			 body {
				 background-color: rgba(191,191,191,0.36);
			 }
		 </style>
  	</head>
	<body>
	<table width="650">
		<tr>
			<td><img src="img/saaswedo_relief_sansfond_moyen.png" width="150"></td>
			<td class="titre">CIR SHOOTER v1.2</td>
		</tr>
		<tr height="3">
			<td class="ligne" width="100%" colspan="2"></td>
		</tr>
		<tr height="5">
			<td align="left" colspan="2">
				<input type="button" name="btn1" value="Liste complète" onclick="javascript:location.href='get_url.cfm';">
			</td>
		</tr>
		<tr height="3">
			<td class="ligne" width="100%" colspan="2"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		<tr>
			<td><b>Collaborateur<b></td>
			<td><b>Nombre de shoot</b></td>
		</tr>
		<tr height="1">
			<td class="ligne" width="100%" colspan="2"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		<cfoutput query="qGetShoot">
		<tr>
			<td><a href="get_url.cfm?user=#user_jira#">#user_jira#</a></td>
			<td>#nb#</td>
		</tr>
		</cfoutput>
		
	</table>
	</body>
</html>