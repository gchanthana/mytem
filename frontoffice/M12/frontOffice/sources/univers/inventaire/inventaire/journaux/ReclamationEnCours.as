package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import composants.util.DateFunction;
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.Alert;
	
	
	/**
	 * Journal des opération de réclamation en cours
	 * */
	public class ReclamationEnCours extends ReclamationsEnCoursIHM implements IJournal
	{
		
		//reference vers la date pour laquelle on souhaite afficher l'inventaire
		private var laDate : String;	
				
		//ArrayCollection contenant les lignes du journal
		[Bindable]
		private var elementsDuJournal : ArrayCollection;
		
		//Reference vers la méthode distante qui ramène les lignes du journal
		private var opElementDuJournal : AbstractOperation;
		
		//les information du noeud (inutilisé ici)
		private var _nodeInfos : Object;
		
		
		/**
		 * Constructeur
		 * */		
		public function ReclamationEnCours()
		{	
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		} 
		
		
		/**
		 * Affecte les infos sur le noeud au journal
		 * */
		 public function set nodeInfos(infos : Object):void{
		 	_nodeInfos = infos;
		 }
		 
		 	
		/**
		 * Recharge les données du journal
		 * */
		public function refresh():void{
			//tente de couper le remoting
			try{
				 
			}catch(e : Error){
				trace("ok");
			}finally{
				if (laDate != null)
					chargerDonnees();
			}
		}
		
		/**
		* Fonction qui permet d'afficher la période sur laquelle on travaille, au format jour mois annee 
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		* (ne fait rien pour cette classe)
		* */
		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
			var formMois: String = moisDebut.substr(0,2) 
									+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
								   	+ moisDebut.substr(6,4);
			
			//labelPeriode.text = "Date :" + formMois;									
						
		}
		
		/**
		 * Gere le changement de périmetre
		 * */
		public function onPerimetreChange():void{
			//tente de couper le remoting
			try{
				 
			}catch(e : Error){
				trace("ok");
			}finally{
				//init filtre
				txtFiltre.text = "";
				
				//init periode
				initPeriode();
				setSelectedPeriode(laDate,null);	
				
				//init le data provider
				elementsDuJournal.source = null
				elementsDuJournal.refresh();
				
				//données
				chargerDonnees();
			}
			
		}
		
		/**
		 * Interrompt les remotings en cours
		 * (ne fait rien)
		 * */
		public function cancelRemotings():void{
			try{
				 
			}catch(e : Error){
				trace("ok");
			}
		}
//--- INITIALISATION ---------------------------------------------------------------------------------------------------------------------------

		//Initialisation de l'ihm
		//Affecte les écouteurs d'évènements
		//Initialise la date pou laquel on affiche le journal
		//Affiche la date du journal
		//Charge les données 
		private function initIHM(fe : FlexEvent):void{
			//Periode
			initPeriode();				
			
			setSelectedPeriode(laDate,null);							
						
			//Filtre
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
				
			//grid
			DataGridColumn(myGrid.columns[1]).labelFunction = formateDates;
			DataGridColumn(myGrid.columns[2]).labelFunction = formateDates;			
			myGrid.addEventListener(Event.CHANGE,goToselectedOperation);
			chargerDonnees();
		}
						
						
		//formate une colonne de date dans un DataGrid	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres						
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			return DateFunction.formatDateAsString(ladate);						
		}
				
		//Initilalisation de la periode
		private function initPeriode():void{			    
		    laDate = DateFunction.formatDateAsString(new Date());		
		}		
		
//--- FIN INITIALISATION -----------------------------------------------------------------------------------------------------------------------

	

//--- PERIODE SELECTOR -------------------------------------------------------------------------------------------------------------------------
		
		//mise à jour de la periode (ne fait rien pour cette classe)
		private function updatePeriode(pe : PeriodeEvent):void{
					
		}
		
//--- FIN PERIODE SELECTOR ---------------------------------------------------------------------------------------------------------------------
		
				
//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------
		
		//Gere le filtre du tableau
		private function filtrerGrid(ev :Event):void{
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();		
		}
		
		//filtre sur les attributs LIBELLE_OPERATIONS, DATE_ACTION, LIBELLE_ETAT, NUMERO_OPERATION des elements du Tabelau
		private function filterFunc(value : Object):Boolean{				
			if ((String(value.LIBELLE_OPERATIONS).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||(String(value.DATE_ACTION).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||(String(value.LIBELLE_ETAT).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||(String(value.NUMERO_OPERATION).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				{
				return (true);
			} else {
				return (false);
			}	
		}
		
		
//--- FIN FILTRE -------------------------------------------------------------------------------------------------------------------------------

//--- GRID -------------------------------------------------------------------------------------------------------------------------------------
		
		//Dispatche un evenement SelectionOperationEvent de type SelectionOperationEvent.OPERATION_SELECTED
		//Permet de passer l'identifiant de la commande
		private function goToselectedOperation(ev : Event):void{
			var evtObj : SelectionOperationEvent = new SelectionOperationEvent(SelectionOperationEvent.OPERATION_SELECTED);			
			evtObj.idOperation = myGrid.selectedItem.IDINV_OPERATIONS;			
			dispatchEvent(evtObj);
		}
		
//--- FIN GRID ---------------------------------------------------------------------------------------------------------------------------------

//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		
		//Charge les donnees du Journal
		private function chargerDonnees():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			try{
				 
			}catch(e : Error){
				trace("ok");
			}finally{
				lblError.visible = false;
				opElementDuJournal = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getListeOperationReclamationsEnCours",
																				chargerDonneesResultHandler);		
			
				RemoteObjectUtil.callService(opElementDuJournal,
											idGroupeMaitre,
											laDate)
			}														
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerDonnees' à l'ArrayCollection 'elementsDuJournal' et les affiches
		private function chargerDonneesResultHandler(re :ResultEvent):void{						 
			elementsDuJournal = re.result as ArrayCollection;	
			if (elementsDuJournal.length > 0){
				txtFiltre.editable = true;				
			} else {
				txtFiltre.editable = false;
				lblError.visible = true;
			}
			myGrid.dataProvider = elementsDuJournal;
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();	 
		}
	
//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------

	}
}