<!---
Dans : fr.consotel.consoview.util
---> 
<cfcomponent name="SendMail">

	<cffunction name="sendSingleMail" access="remote" returntype="void" output="true">
		<cfargument name="arr" type="array" required="true">
		<cfif arr[8] eq "YES">
			<cfset arr[3]=arr[3] & "," & arr[1]>
		</cfif>
		<cfmail from="support@consotel.fr"
				to="#arr[2]#"
				subject="#arr[5]# : #arr[6]#"				 
				type="html"
				charset="utf-8"
				wraptext="72"
				replyto="#arr[1]#"
				server="mail@mail.consotel.fr:26">
				#arr[7]#
		</cfmail>
	 </cffunction>

</cfcomponent>
