package univers.inventaire.inventaire.journaux
{
	/**
	 * Classe charger de construire les differents journaux
	 * D.P. Factory
	 * */
	public class JournalFactory
	{
		/**
		 * Constante static definissant le type de journal InventaireComplet
		 * */
		public static const INVENTAIRE_COMPLET : String = "InventaireComplet";
		
		
		/**
		 * Constante static definissant le type de journal InventaireComplet
		 * */
		public static const RESSOURCES_RESILIEES_TOUJOURS_FACTUREES : String = "RessourcesResilieesFacturees";
		
		/**
		 * Constante static definissant le type de journal RessourcesFactureesNonLivrees
		 * */
		public static const RESSOURCES_FACTUREES_NON_LIVREES : String = "RessourcesFactureesNonLivrees";
		
		/**
		 * Constante static definissant le type de journal ConsoHorsInventaire
		 * */
		public static const CONSOS_HORS_INVENTAIRE : String = "ConsoHorsInventaire";		
		
		/**
		 * Constante static definissant le type de journal RessourcesInventaireNonFacturees
		 * */
		public static const RESSOURCES_INVENTAIRE_LIVREES_NON_FACTUREES : String = "RessourcesInventaireNonFacturees";		
		
		/**
		 * Constante static definissant le type de journal OperationAvecRetard
		 * */
		public static const OPERATION_AVEC_RETARD : String = "OperationAvecRetard";
		
		/**
		 * Constante static definissant le type de journal ReclamationEnCours
		 * */
		public static const RECLAMATION_EN_COURS : String = "ReclamationEnCours";
		
		/**
		 * Constante static definissant le type de journal DemandesEnRetard
		 * */
		public static const DEMANDES_AVEC_RETARD : String = "DemandesEnRetard";
		
		/**
		 * Constante static definissant le type de journal HistoriqueInvNoeud
		 * */
		public static const HISTO_INV_NOEUD : String = "HistoriqueInvNoeud";
		
		/**
		 * Constante static definissant le type de journal Ressources hors inventaire facturees
		 * */
		public static const RESSOURCES_HORS_INV_FACTUREES : String = "RessourcesHorsInvFacturees";
		
		
		/**
		 * Retourne une instance IJournal suivant le type passé en parametre, si le type est inconnu, la methode lance une Exception JournalError
		 * @param type le type de journal
		 * @return journal une instance de journal
		 * @throw JournalError une exception journal inconnu
		 * */
		public static function createJournal(type : String):IJournal{
			switch(type){
				case JournalFactory.INVENTAIRE_COMPLET : return new InventaireComplet();break;
				case JournalFactory.RESSOURCES_RESILIEES_TOUJOURS_FACTUREES:return new RessourcesResilieesFacturees();break;
				case JournalFactory.RESSOURCES_FACTUREES_NON_LIVREES : return new RessourcesFactureesNonLivrees();break;
				case JournalFactory.CONSOS_HORS_INVENTAIRE : return new ConsoHorsInventaire();break;
				case JournalFactory.RESSOURCES_INVENTAIRE_LIVREES_NON_FACTUREES : return new RessourcesInventaireNonFacturees();break;
				case JournalFactory.OPERATION_AVEC_RETARD : return new OperationAvecRetard();break;
				case JournalFactory.RECLAMATION_EN_COURS : return new ReclamationEnCours();break;
				case JournalFactory.DEMANDES_AVEC_RETARD : return new DemandesEnRetard();break;	
				case JournalFactory.HISTO_INV_NOEUD : return new HistoriqueNoeud();break;
				case JournalFactory.RESSOURCES_HORS_INV_FACTUREES : return new RessourcesHorsInvFacturees();break;				
				default: throw new JournalError("Journal Inconnu");			
			}
		}
	}
}