package appli.impersonnification.popup
{
	import appli.events.LoginEvent;
	import appli.impersonnification.event.PaginateListEvent;
	import appli.impersonnification.event.RelogEvent;
	import appli.impersonnification.refreshManager;
	import appli.impersonnification.service.ListServices;
	import appli.impersonnification.service.relog.RelogServices;
	
	import entity.CodeLangue;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import general.abstract.AbstractConnection;
	import general.abstract.AbstractModuleLoader;
	import general.service.LoginServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;
	import searchpaginatedatagrid.SearchPaginateDatagrid;
	import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;

	public class PopupChooseOtherAccountImpl extends TitleWindow
	{
		[Bindable] public var nbTotalElement:int;
		[Bindable] public var arrayTousLesLogins:ArrayCollection = new ArrayCollection();
		[Bindable] public var nbItemPerPage:int = 20;
		[Bindable] public var dgTousLesLogins:PaginateDatagrid;
		[Bindable] public var mySearchPaginate:SearchPaginateDatagrid;
		[Bindable] private var dglog:DataGrid;
		
		
		private var listServices:ListServices;
		
		private var loginServices:LoginServices = new LoginServices();
		private var relogServices:RelogServices = new RelogServices();


		
		
		public function PopupChooseOtherAccountImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function initIHM(event:FlexEvent):void
		{
			dglog = dgTousLesLogins.dgPaginate;
			mySearchPaginate.addEventListener(SearchPaginateDatagridEvent.SEARCH, onSearchHandler);
			dgTousLesLogins.addEventListener(PaginateDatagridEvent.PAGE_CHANGE, onIntervalleChangeHandler);
			
			/*Ouverture de la popup, affichage de tous les logins par défaut*/
			servicePourColdFusion(mySearchPaginate.parametresRechercheVO, dgTousLesLogins.currentIntervalle);
			
			loginServices.myDatas.addEventListener(RelogEvent.OTHERCONNECTION_ERROR, otherConnection_ErrorHandler);
			relogServices.myHandlers.addEventListener(RelogEvent.SESSION_CHANGED, updateSession);
			relogServices.myHandlers.addEventListener(RelogEvent.INIT_LOGINS, processInitTousLesLogins);
			relogServices.myHandlers.addEventListener(RelogEvent.LISTLOGINS_ERROR, erreur);
			mySearchPaginate.comboRecherche.selectedIndex = 2;
			dgTousLesLogins.dgPaginate.doubleClickEnabled = true;
			dgTousLesLogins.dgPaginate.addEventListener(ListEvent.ITEM_DOUBLE_CLICK, doubleClickHandler);
			dgTousLesLogins.dgPaginate.addEventListener(ListEvent.CHANGE, dgChangeHandler);
		}
		public function onSearchCreationComplete():void
		{
			
		}
		
		public function onSearchHandler(evt:SearchPaginateDatagridEvent):void
		{/*On recharge tous les logins correpondants aux critères de la recherche.*/
			if(dgTousLesLogins && mySearchPaginate)
			{
				servicePourColdFusion(mySearchPaginate.parametresRechercheVO);
			}
		}
		
		public function getImpersValue():Boolean
		{
			relogServices.getImpersonnification();
			return this.relogServices.myDatas.impersonnification;
		}

		
		public function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			servicePourColdFusion(mySearchPaginate.parametresRechercheVO, dgTousLesLogins.currentIntervalle);
		}
		private function servicePourColdFusion(parametresRechercheVO:ParametresRechercheVO, itemIntervalleVO:ItemIntervalleVO = null):void
		{
			if(!itemIntervalleVO) // Si c'est une nouvelle recherche
			{
				dgTousLesLogins.refreshPaginateDatagrid();
				itemIntervalleVO = new ItemIntervalleVO();
				itemIntervalleVO.indexDepart = 0;
				itemIntervalleVO.tailleIntervalle = nbItemPerPage;
			}
			listServices = new ListServices();
			listServices.myHandlers.addEventListener(PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS, onListServiceResultHandler);
			listServices.myHandlers.addEventListener(PaginateListEvent.LOGIN_LIST_LOADED_ERROR, onListServiceResultHandler);

			listServices.getListeLogins(parametresRechercheVO, itemIntervalleVO, -1, relogServices.myDatas.firstLoading);
			relogServices.myDatas.firstLoading = false;

		}
		
		/**
		 * Fonction remplissant dgTousLesLogins et nbTotalElement */
		public function onListServiceResultHandler(evt:PaginateListEvent):void
		{
			switch(evt.type)
			{
				case PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS:
					arrayTousLesLogins = listServices.myDatas.listLogin;
					if(arrayTousLesLogins.length > 0)
					{
						nbTotalElement = arrayTousLesLogins.getItemAt(0).NBRECORD;
						trace("onListServiceResultHandler nbTotalElement " + nbTotalElement);
						
						dgTousLesLogins.dataprovider = new ArrayCollection();
						for(var i:int = 0; i < arrayTousLesLogins.length; i++)
						{
							dgTousLesLogins.dataprovider.addItem(arrayTousLesLogins.getItemAt(i));
						}
					}
					else
					{
						nbTotalElement = 0;
					}

					listServices.myHandlers.removeEventListener(PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS, onListServiceResultHandler);
					break;
				case PaginateListEvent.LOGIN_LIST_LOADED_ERROR:
					trace("GestionLogin-onResultListServicesHandler() : Erreur dans la récupérations de la liste des utilisateurs ");
					break;
				default:
					break;
			}
			
			for each (var item:Object in listServices.myDatas.listLogin)
			{
				trace("Valeur de LOGIN_STATUS : " + item.LOGIN_STATUS);
				if (item.LOGIN_STATUS != 1)
				{
					//trace ("Valeur de LOGIN_STATUS : " + item.LOGIN_STATUS);
					//trace("longueur de dgPaginate : " + dgTousLesLogins.dgPaginate.);
					//dgTousLesLogins.dgPaginate.
				}
			}
		}
		
		public function codeAppRecieved(evt:Event):void
		{
			relogServices.setImpersonnification();
			refreshManager.isReloading = true;
			refreshManager.failedRelogging = false;
			if (refreshManager.currentAccountName != CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM)
			{/* petit trick ici : il y a un seul moment ou cette condition est remplie : lors de la première impersonnification. Dans les autres cas, currentAccountName a été préalablement setté par le refresh du header.*/
				refreshManager.previousAccountName = CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM;
			}
			if (dgTousLesLogins.selectedItem != null)
			{	
				relogServices.myDatas.codeLangue = CvAccessManager.getSession().AppPARAMS.codeLangue;
				if (relogServices.myDatas.codeApp == 351)//CG
				{
					relogServices.setImpersonnification();
					trace("re-login : Console de gestion");
					loginServices.login(dgTousLesLogins.selectedItem.LOGIN_EMAIL, dgTousLesLogins.selectedItem.LOGIN_PWD, relogServices.myDatas.codeApp, 5, relogServices.myDatas.codeLangue, false,true);
				}
				else if (relogServices.myDatas.codeApp == 1)//CV4
				{
					relogServices.setImpersonnification();
					trace("re-login : CV4");
					loginServices.login(dgTousLesLogins.selectedItem.LOGIN_EMAIL, dgTousLesLogins.selectedItem.LOGIN_PWD, relogServices.myDatas.codeApp, 1, relogServices.myDatas.codeLangue, false,true);
				}
				else if (relogServices.myDatas.codeApp == 101)//Mytem360
				{
					relogServices.setImpersonnification();
					trace("re-login : Mytem360");
					loginServices.login(dgTousLesLogins.selectedItem.LOGIN_EMAIL, dgTousLesLogins.selectedItem.LOGIN_PWD, relogServices.myDatas.codeApp, 4, relogServices.myDatas.codeLangue, false,true);
				}
				else if (relogServices.myDatas.codeApp == 51)//Pilotage
				{
					relogServices.setImpersonnification();
					trace("re-login : PILOTAGE");
					loginServices.login(dgTousLesLogins.selectedItem.LOGIN_EMAIL, dgTousLesLogins.selectedItem.LOGIN_PWD, relogServices.myDatas.codeApp, 1, relogServices.myDatas.codeLangue, false,true);
				}
				else
				{
					trace("Warning : Aucun codeApp correspondant : " + relogServices.myDatas.codeApp);
					loginServices.login(dgTousLesLogins.selectedItem.LOGIN_EMAIL, dgTousLesLogins.selectedItem.LOGIN_PWD, relogServices.myDatas.codeApp, 4, relogServices.myDatas.codeLangue, false,true);
				}
				relogServices.setImpersonnification();
			}
		}
		

		protected function updateSession(evt:Event):Boolean
		{
			var obj:Object = relogServices.myDatas.infosSession;
			try
			{
				if(CvAccessManager.singletonInstance != null && obj != null)
				{
					if(obj.hasOwnProperty("user"))
						CvAccessManager.singletonInstance.updateUser(obj.user);
					if(obj.hasOwnProperty("cg"))
					{
						CvAccessManager.getSession().INFOS_DIVERS.CG = new Object();
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER = new Object();
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDPROFIL = obj.cg.CG_IDPROFIL;
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.LABEL_PROFIL = obj.cg.CG_LABEL_PROFIL;
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDNIVEAU = obj.cg.CG_IDNIVEAU;
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.LABEL_NIVEAU = obj.cg.CG_LABEL_NIVEAU;
					}
					if(obj.hasOwnProperty("liste_racine"))
						CvAccessManager.singletonInstance.updateGroupList(obj.liste_racine);
					if(obj.hasOwnProperty("perimetre"))
						CvAccessManager.singletonInstance.updatePerimetre(obj.perimetre);
					if(obj.hasOwnProperty("PERIMETRE"))
						CvAccessManager.singletonInstance.updatePerimetre(obj.PERIMETRE);
					if(obj.hasOwnProperty('codeapplication'))
						CvAccessManager.singletonInstance.updateCodeApplication(obj.codeapplication);
					/*if(obj.hasOwnProperty("xml_access"))
					buildMenuXml(obj.xml_access as XML);
					if(obj.hasOwnProperty("XML_ACCESS"))
					buildMenuXml(obj.XML_ACCESS as XML);*/
					if(obj.hasOwnProperty("xml_perimetre"))
						CvAccessManager.singletonInstance.updatePerimetreXML(obj.xml_perimetre as XML);
					if(obj.hasOwnProperty("XML_PERIMETRE"))
						CvAccessManager.singletonInstance.updatePerimetreXML(obj.XML_PERIMETRE as XML);
				}
				else
					trace("_singletonInstance == NULL");
			}
			catch(error:Error)
			{
				return false;
			}
			this.dispatchEvent(new RelogEvent(RelogEvent.LOGIN_CHOSEN));//Cet event est écouté dans les créateurs de headers
			return true;			
		}

		public function processInitTousLesLogins(event:ResultEvent):void
		{
			arrayTousLesLogins = event.result as ArrayCollection;
			dglog.dataProvider = arrayTousLesLogins;
		}
		
		private function loginChosen(evt:LoginEvent):void
		{
			relogServices.updateCv();			
			refreshManager.impersonnification = this.getImpersValue();
		}
		
		public function otherConnection_ErrorHandler(evt:Event):void
		{
			trace("Suppression du listener pour loginChosen");
			this.removeEventListener(LoginEvent.VALIDE, loginChosen);
			refreshManager.isReloading = false;
			Alert.show(ResourceManager.getInstance().getString("M00", "Connexion_refus_e___cette_personne_n_a_probablement"));
		}
		
		public function erreur(evt:FaultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString("M00", "Erreur_de_chargement___") + evt.toString());
		}
		
		public function dgChangeHandler(evt:ListEvent):void
		{
			if (evt.currentTarget.selectedIndex == -1) return;
			if (evt.currentTarget.selectedItem.LOGIN_STATUS != 1)
				evt.currentTarget.selectedIndex = -1;
		}
		
		/**
		 * Handlers des boutons Valider et Annuler
		 */
		protected function btnValidate_clickHandler(event:MouseEvent):void
		{
			if (!this.loginServices.myDatas.hasEventListener(LoginEvent.VALIDE))
				this.loginServices.myDatas.addEventListener(LoginEvent.VALIDE, loginChosen);
			relogServices.myHandlers.addEventListener(RelogEvent.CODEAPP_RECIEVED, codeAppRecieved);
			relogServices.getCodeApplication();
			
			PopUpManager.removePopUp(this);
		}
		protected function doubleClickHandler(event:ListEvent):void
		{
			if (!this.loginServices.myDatas.hasEventListener(LoginEvent.VALIDE))
				this.loginServices.myDatas.addEventListener(LoginEvent.VALIDE, loginChosen);
			relogServices.myHandlers.addEventListener(RelogEvent.CODEAPP_RECIEVED, codeAppRecieved);
			relogServices.getCodeApplication();
			
			PopUpManager.removePopUp(this);
		}
		protected function btnCancel_clickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		

	}
}