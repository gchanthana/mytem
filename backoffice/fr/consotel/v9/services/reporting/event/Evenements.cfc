<cfcomponent author="Cedric" displayname="fr.consotel.v9.services.reporting.event.Evenements" hint="Web Service de lancement des Evènements">
	<cffunction access="remote" name="executeP1" returntype="void" hint="Exécute l'évènement P1">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement1">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP2" returntype="void" hint="Exécute l'évènement P2">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement2">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP3" returntype="void" hint="Exécute l'évènement P3">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement3">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP4" returntype="void" hint="Exécute l'évènement P4">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement4">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP5" returntype="void" hint="Exécute l'évènement P5">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement5">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP6" returntype="void" hint="Exécute l'évènement P6">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement6">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP7" returntype="void" hint="Exécute l'évènement P7">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement7">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP8" returntype="void" hint="Exécute l'évènement P8">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement8">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP9" returntype="void" hint="Exécute l'évènement P9">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement9">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP10" returntype="void" hint="Exécute l'évènement P10">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement10">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP11" returntype="void" hint="Exécute l'évènement P11">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement11">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP15" returntype="void" hint="Exécute l'évènement P15">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement15">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP16" returntype="void" hint="Exécute l'évènement P16">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement16">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP17" returntype="void" hint="Exécute l'évènement P17">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement17">
		<cfset execute(classEvenement)>
	</cffunction>
	
	<cffunction access="remote" name="executeP18" returntype="void" hint="Exécute l'évènement P18">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement18">
		<cfset execute(classEvenement)>
	</cffunction>

	<cffunction access="remote" name="executeP19" returntype="void" hint="Exécute l'évènement P19">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement19">
		<cfset execute(classEvenement)>
	</cffunction>

	<cffunction access="remote" name="executeP20" returntype="void" hint="Exécute l'évènement P20">
		<cfset var classEvenement="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement20">
		<cfset execute(classEvenement)>
	</cffunction>

	<cffunction access="private" name="execute" returntype="void" hint="Exécute une méthode executePx()">
		<cfargument name="dataProviderClass" type="String" required="true" hint="Nom complet de la classe qui dérive de fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement">
		<cftry>
			<!--- Propriétés Mail utilisés par cette classe --->
			<cfset var serverDomain="saaswedo.com">
			<cfset var mailProperties={from="error@" & serverDomain, to="monitoring@saaswedo.com", type="html"}>
			<!--- API Reporting --->
			<cfset var reportingService=createObject("component","fr.consotel.api.ibis.publisher.reporting.impl.ReportingService").getService("scheduler","public")>
			<!--- Implémentation qui dérive doit dériver de fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement  --->
			<cfset var classeEvenement=ARGUMENTS["dataProviderClass"]>
			<cfset var evenement=createEvenement(classeEvenement)>
			<!--- Exécute les diffusions listées dans l'évènement désigné par dataProviderClass --->
			<cfloop condition="evenement.haveNext()">
				<cfset var reportingProperties=evenement.nextReporting()>
				<cfset var reporting=reportingService.createReporting(reportingProperties)>
				<cfset reportingService.execute(reporting)>
			</cfloop>
			<cfcatch type="any">
				<cflog type="error" text="Evenements.execute(#classeEvenement#) : #CFCATCH.message#">
				<cfmail attributecollection="#mailProperties#" subject="Evenements : #classeEvenement#">
					<cfoutput>
						Message : #CFCATCH.MESSAGE#<br>
						Detail : #CFCATCH.DETAIL#<br>
					</cfoutput>
					<cfif isDefined("reportingProperties")>
						<cfdump var="#reportingProperties#" label="Propriétés du reporting"><br>
					</cfif>
					<cfdump var="#CFCATCH.TAGCONTEXT#" label="Contexte d'exécution">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="createEvenement" returntype="fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement"
	hint="Retourne l'implémentation Evènement correspondant à dataProviderClass">
		<cfargument name="dataProviderClass" type="String" required="true" hint="Nom complet de la classe correspondant à l'évènement">
		<cfreturn createObject("component",ARGUMENTS["dataProviderClass"]).createReporting()>
	</cffunction>
</cfcomponent>