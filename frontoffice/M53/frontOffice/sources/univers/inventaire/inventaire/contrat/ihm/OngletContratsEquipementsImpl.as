package univers.inventaire.contrat.ihm
{
	import flash.events.Event;
	
	import mx.containers.VBox;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.contrat.system.IResultatRecherche;

	public class OngletContratsEquipementsImpl extends VBox
	{
		public var cpDetailContratEquipement:DetailContratEquipementIHM;
		public var cpListeEquipements:ListeContratsEquipementsIHM;
		public var ppCreerContratEquipement:CreerContratEquipementIHM;
		
		
		

	/* 	public function get listeContrats ():IResultatRecherche
		{
			reuturn
		} */


		
		public function OngletContratsEquipementsImpl()
		{
			super();
		}
		
		
		
		protected function btCreerContratClickedHandler(event:Event):void
		{
			ppCreerContratEquipement= new CreerContratEquipementIHM();
			
			PopUpManager.addPopUp(ppCreerContratEquipement,this,true);
			PopUpManager.centerPopUp(ppCreerContratEquipement);
		}		
	}
}