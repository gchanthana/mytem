package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets
{
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class Commande extends EventDispatcher
	{
		public static const FORMAT_PDF : String = "PDF";
		
		public static const CREATION_COMPLETE : String = "commandeOk";
		public static const UPDATE_COMPLETE : String = "commandeUpdated";
		public static const SAVE_COMPLETE : String = "commandeSaved";
		public static const LISTE_COMPLETE : String = "commandeListOk";
		public static const DELETE_COMPLETE : String = "commandeDeleted";
		public static const UPDATESTATUT_COMPLETE : String = "commandeStatutUpdated";
		public static const UPDATERATIO_COMPLETE : String = "commandeRatioUpdated";
		public static const UPDATEOPERATION_COMPLETE : String = "commandeopeUpdated";
		public static const CMDE_OPE_CREATION_COMPLETE : String = "operarionCommandeOk";
		public static const SEND_COMPLETE : String = "commandeSent";
				
		public static const CREATION_ERROR : String = "commandeCreateError";
		public static const UPDATE_ERROR : String ="commandeUpdateError";  
		public static const SAVE_ERROR : String = "commandeSaveError";
		public static const LISTE_ERROR : String = "commandeListError";
		public static const DELETE_ERROR : String = "commandeDeleteError";
		public static const UPDATESTATUT_ERROR : String = "commandeStatutUpdateError";
		public static const UPDATERATIO_ERROR : String = "commandeRatioUpdateError";
		public static const UPDATEOPERATION_ERROR : String = "commandeopeUpdateError";
		public static const CMDE_OPE_CREATION_ERROR : String = "operarionCommandeCreateError";
		public static const SEND_ERROR : String = "commandeSentError";
		
				
		public static const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande";
  		public static const COLDFUSION_GET : String = "get";
  		public static const COLDFUSION_UPDATE : String = "update";
  		public static const COLDFUSION_CREATE : String = "create";
  		public static const COLDFUSION_CREATE_ALL : String = "createCommandeSousTeteAndWorkFlow";
		public static const COLDFUSION_GETLISTE : String = "getList";
		public static const COLDFUSION_GETUSERLISTE : String = "getUserList";		
		public static const COLDFUSION_DELETE : String = "delete";
		public static const COLDFUSION_UPDATE_STATUT : String ="updateStatut";
		public static const COLDFUSION_UPDATE_RATIO : String ="updateRatio";
		public static const COLDFUSION_UPDATE_OPERATION : String ="updateCommandeOperation";
		public static const COLDFUSION_CREATE_OPE : String ="createOperation";
		public static const COLDFUSION_SEND : String ="sendCommande";
		
		public static const NOUVELLES_LIGNES : int = 1;
		public static const NOUVEAUX_PRODUITS_SUR_LIGNES_EX : int = 2;
		
		
	
										
		
		private var opGet : AbstractOperation;
		private var opCreate : AbstractOperation;
		private var opUpdate : AbstractOperation;		
		private var opDelete : AbstractOperation;
		private var opList : AbstractOperation;
		
		
		public function Commande(commande_ID : int = 0){
			if (commande_ID != 0){				
				commandeID = commande_ID;
				getCommande(commandeID);
			}
		}
		
		public function clean():void{			
			 
		}
		
		private var  _elementsCommande : ArrayCollection;
		
		public function get elementsCommande():ArrayCollection{
			return _elementsCommande;
		}		
		public function set elementsCommande(elements : ArrayCollection):void{
			_elementsCommande = elements;
		}		
						
		//identifiant de la commande
		private var _commandeID : int;
		public function get commandeID():int{
			return _commandeID;
		}
		public function set commandeID(id : int):void{
			_commandeID = id;
		}
		
		private var _flagCible : int = 0;
		public function get flagCible():int{
			return _flagCible;
		}
		public function set flagCible(id : int):void{
			_flagCible = id;
		}
		
		private var _typeCommande : int = -1;
		public function get typeCommande():int{
			return _typeCommande;
		}
		public function set typeCommande(id : int):void{
			_typeCommande = id;
		}
				
		//libellé de la commande
		private var _libelle : String = "";
		public function get libelle():String{
			return _libelle;
		}
		public function set libelle(l : String):void{
			_libelle = l;
		}
		
		//Référence client de la commande
		private var _refClient : String = " ";
		public function get refClient():String{
			return _refClient;
		}
		public function set refClient(ref : String):void{
			_refClient = ref;
		}
		
		
		//le ratio trouvé par le rapprochement
		private var _ratio : int = 0;
		public function get ratio():int{
			return _ratio;
		}
		public function set ratio(r : int):void{
			_ratio = r;
		}
		
		
		//Référence operateur de la commande
		private var _refOperateur : String = " ";
		public function get refOperateur():String{
			return _refOperateur;
		}
		public function set refOperateur(ref : String):void{
			_refOperateur = ref;
		}
		
		private var _operateurID : int = -1;
		public function get operateurID ():int{
			return _operateurID ;
		}
		public function set operateurID(id : int):void{
			_operateurID = id;
		}
		
		private var _operateurNom : String = " ";
		public function get operateurNom():String{
			return _operateurNom;
		}
		public function set operateurNom(opnom : String):void{
			_operateurNom = opnom;
		}
		
		private var _compteID: int = -1;
		public function get compteID():int{
			return _compteID;
		}
		public function set compteID(id : int):void{
			_compteID = id;
		}
		
		private var _sousCompteID : int = -1;
		public function get sousCompteID():int{
			return _sousCompteID;
		}
		public function set sousCompteID(id : int):void{
			_sousCompteID = id;
		}

				
		//Commentaire de la commande
		private var _commentaire : String = " ";
		public function get commentaire():String{
			return _commentaire;
		}
		public function set commentaire(com : String):void{
			_commentaire = com;
		}
		
		//Date effective de la commande
		private var _dateEffective : String ="";
		public function get dateEffective():String{
			return _dateEffective;
		}
		public function set dateEffective(da : String):void{
			_dateEffective = da;
		}
		
		//Date de livraison de la commande
		private var _dateLivraison : String ="";
		public function get dateLivraison():String{
			return _dateLivraison;
		}
		public function set dateLivraison(da : String):void{
			_dateLivraison = da;
		}
		
		//Date de livraison prevue de la commande
		private var _dateLivraisonPrevue : String ="";
		public function get dateLivraisonPrevue():String{
			return _dateLivraisonPrevue;
		}
		public function set dateLivraisonPrevue(dap : String):void{
			_dateLivraisonPrevue = dap;
		}
		public function get ddateLivraisonPrevue():Date{
			return new Date(dateLivraisonPrevue);
		}
		public function get ddateLivraison():Date{
			return new Date(dateLivraison);
		}
		public function get ddateEffective():Date{
			return new Date(dateEffective);
		}
		public function set ddateLivraisonPrevue(value : Date):void{
			
		}
		public function set ddateLivraison(value : Date):void{
		
		}
		public function set ddateEffective(value : Date):void{
		
		}
		//identifiant de l'opération
		private var _operationID : int;
		public function get operationID():int{
			return _operationID;
		}
		public function set operationID(opId : int):void{
			_operationID = opId;
		}
				
		//login id du createur de la commande
		private var _idUserCreate : int = -1;
		public function get idUserCreate ():int{
			return _idUserCreate ;
		}
		public function set idUserCreate(id : int):void{
			_idUserCreate = id;
		}
		
		//login id dernier utilisateur ayant modifier la commande
		private var _idUserModif : int = -1;
		public function get idUserModif ():int{
			return _idUserModif ;
		}
		public function set idUserModif (id : int):void{
			_idUserModif  = id;
		}
		
		//date de creation
		private var _dateCreation : String;
		public function get dateCreation ():String{
			return _dateCreation  ;
		}
		
		public function set dateCreation(d : String):void{
			_dateCreation = d;
		}
		
		//date de creation
		private var _dateEnvoi : String;
		public function get dateEnvoi ():String{
			return _dateEnvoi  ;
		}
		
		public function set dateEnvoi(d : String):void{
			_dateEnvoi = d;
		}
		
		//date modification
		private var _dateModif : String
		public function get dateModif ():String{
			return _dateModif;
		}
		
		//id du groupe client auxquel appartient la commande
		private var _idGroupeClient : int = -1;
		public function get idGroupeClient ():int{
			return _idGroupeClient ;
		}
		public function set idGroupeClient (id : int):void{
			_idGroupeClient = id;
		}
		
		//id du groupe client cible de la commande
		private var _idGroupeCible : int = -1;
		public function get idGroupeCible():int{
			return _idGroupeCible;
		}		
		public function set idGroupeCible(id : int):void{
			_idGroupeCible = id;
		}
		
		//le libellé du groupe client cible de la commande
		private var _groupeCibleLibelle : String;
		public function get groupeCibleLibelle():String{
			return _groupeCibleLibelle;
		}
		public function set groupeCibleLibelle(libelle : String):void{
			_groupeCibleLibelle = libelle;
		}
		
		//l'id du statut de la commande
		private var _statutID : int = -1;
		public function get statutID ():int{
			return _statutID;
		}		
		public function set statutID (st : int):void{			
			_statutID  = st;
			/* switch(_statutID){
				case StatuTInfo.EN_ATTENTE_ENVOI :  statut = 		"En attente d'envoi";break;
				case StatuTInfo.EN_ATTENTE_DECISION_CTRL : statut = "En attente ctrl.";break;
				case StatuTInfo.EN_ATTENTE_LIVRAISON : statut = 	"En attente livraison";break;
				case StatuTInfo.RAPPROCHEE : statut = "Rapprochée";break;
				default : statut = " ";break;
			} */
		}
		
		//libellé du statut de la commande
		private var _statut : String = " ";
		public function get statut():String{
			return _statut;
		}
		public function set statut( s : String ):void{
			_statut = s;
		}
		
		
		//id du contact de la commande
		private var _contactID : int;
		public function get contactID():int{
			return _contactID;
		}
		public function set contactID(c : int):void{
			_contactID = c;
		}
		
		//id du distributeur ou de l'agence de la commande
		private var _societeID : int;
		public function get societeID():int{
			return _societeID;
		}		
		public function set societeID(id : int):void{			
			_societeID = id;
		}
		
		//flag servant à savoir si la commande à été envoyer avec mail
		private var _flagMail : int = 0;
		public function get flagMail():int{
			return _flagMail;
		}		
		public function set flagMail(id : int):void{			
			_flagMail = id;
	
		}
		
		
		//------------------ REMOTING -------------
						
		//---------------------------------------------------------/
		
		private function fill(value : Object):void{				
				contactID = value.IDCDE_CONTACT;
				operationID = value.IDINV_OPERATION;
				ratio = value.LAST_RATIO;			
				dateEffective = value.DATE_COMMANDE; 
				dateLivraison = value.DATE_LIVRAISON;	
				dateLivraisonPrevue = value.DATE_LIVRAISON_PREVUE;	 
				_dateCreation = value.DATE_CREATE;																						
				_dateModif = value.DATE_MODIF;
				idGroupeCible = value.IDGROUPE_REPERE;
				groupeCibleLibelle = value.GROUPE_LIBELLE;			
				idGroupeClient = value.IDGROUPE_CLIENT;
				idUserCreate = value.USER_CREATE;
				idUserModif = value.USER_MODIF;
				libelle = value.LIBELLE_COMMANDE;
				refClient = value.REF_CLIENT;			
				refOperateur = value.REF_OPERATEUR;						
				contactID = value.IDCDE_CONTACT;
				societeID = value.IDCDE_CONTACT_SOCIETE;			
				statutID = value.IDCDE_STATUS_CDE;	
				statut = value.STATUS;
				typeCommande = value.TYPE_COMMANDE;		
				operateurNom = value.OPNOM;
				flagCible = value.FLAG_NOUVEAU_REPERE;
				flagMail = value.FLAG_MAIL;				
				dateEnvoi = value.DATE_ENVOI;
				operateurID = value.OPERATEURID;
				commentaire = value.COMMENTAIRES;
				compteID = value.IDCOMPTE_FACTURATION;
				sousCompteID = value.IDSOUS_COMPTE;
		}
		
		///---------------- REMOTING ------------------------------/
		
		///--------GET
		public function refresh():void{
			getCommande(commandeID);
		}
		
		private function getCommande(id : int):void{
			opGet = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_GET,
													getCommandeResultHandler,null);
			RemoteObjectUtil.callService(opGet,id);
		}
		
		private function getCommandeResultHandler(re : ResultEvent):void{
			
			if (re.result){							
				fill(re.result[0]);		
				dispatchEvent(new Event(Commande.CREATION_COMPLETE));				
			}else{
				dispatchEvent(new Event(Commande.CREATION_ERROR));
			}
		}
		
		//--------DELETE	
		public function deleteCommande(idCom : int):void{
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_DELETE,
													deleteCommandeResultHandler,null);

			RemoteObjectUtil.callService(opDelete,idCom);
		}
		
		private function deleteCommandeResultHandler(re : ResultEvent):void{
			if (re.result >= 0){							
				dispatchEvent(new Event(Commande.DELETE_COMPLETE));				
			}else{
				dispatchEvent(new Event(Commande.DELETE_ERROR));
			}			
		}
		
		//--------UPDATE					
		public function updateCommande():void{
			 
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_UPDATE,
													updateCommandeResultHandler);
			
			RemoteObjectUtil.callService(opUpdate,10,this);						
		}	
		
		private function updateCommandeResultHandler(re : ResultEvent):void{
			if (re.result > -1){
				refresh()
				dispatchEvent(new Event(Commande.UPDATE_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.UPDATE_ERROR));
			}
		}
		
		public function updateStatut():void{
			 
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_UPDATE_STATUT,
													updateStatutResultHandler,null);

			RemoteObjectUtil.callService(opUpdate,this.commandeID,this.statutID);						
		}	
		
		private function updateStatutResultHandler(re : ResultEvent):void{
			if (re.result > 0){				
				refresh();				
				dispatchEvent(new Event(Commande.UPDATESTATUT_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.UPDATESTATUT_ERROR));
			}
		}
		
		public function updateRatio():void{
		 
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_UPDATE_RATIO,
													updateRatioHandler,null);

			RemoteObjectUtil.callService(opUpdate,this.commandeID,this.ratio);						
		}	
		
		private function updateRatioHandler(re : ResultEvent):void{
			if (re.result > 0){								
				dispatchEvent(new Event(Commande.UPDATERATIO_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.UPDATERATIO_ERROR));
			}
		}
		
		
		//envoyer
		public function envoyer(myDate : String):void{
		 
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_SEND,
													envoyerHandler,null);

			RemoteObjectUtil.callService(opUpdate,this.commandeID,this.flagMail,myDate);						
		}	
		
		private function envoyerHandler(re : ResultEvent):void{
			if (re.result > 0){								
				dispatchEvent(new Event(Commande.SEND_COMPLETE));
				refresh();
			}else{
				dispatchEvent(new Event(Commande.SEND_ERROR));
			}
		}
		
		


		//---------- SAVE
		public function sauvegarder(idgroupe : int):void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_CREATE,
													sauvegarderResultHandler,null);
													

			RemoteObjectUtil.callService(opCreate,idgroupe,this);					 											
		}		
		
		public function sauverCommandeCreerLignesRessourcesEtWorkflow(panier : ElementCommande):void{
			if(panier.liste != null){
				opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_CREATE_ALL,
													sauvegarderResultHandler,null);
																											
				RemoteObjectUtil.callService(			opCreate,
														this,												
														ConsoviewUtil.extraireColonne(panier.liste,"libelleLigne"),
														ConsoviewUtil.extraireColonne(panier.liste,"produitCatID"),
														ConsoviewUtil.extraireColonne(panier.liste,"quantite"),
														ConsoviewUtil.extraireColonne(panier.liste,"commentaire"));	
			}else{
				Alert.okLabel = "Fermer";
				Alert.show("Vous devez sélectionner des produits","Erreur");
				dispatchEvent(new Event(Commande.SAVE_ERROR));
			}
				
		}
		
		private function sauvegarderResultHandler(re : ResultEvent):void{
			
			if (re.result > 0){				
				operationID = Number(re.result);				
				dispatchEvent(new Event(Commande.SAVE_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.SAVE_ERROR));
			}
						
		}
		//------OPERATION ------------------
		public function createOperation(produitsRapp : Array):void{			
			opCreate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_CREATE_OPE,
													createOperationHandler,null);
													
			var fake : int = 0;
			RemoteObjectUtil.callService(opCreate,fake,this,produitsRapp);					 											
		}		
		
		private function createOperationHandler(re : ResultEvent):void{
			if (re.result[0].IDINV_OPERATIONS > 0){				
				_operationID = int(re.result[0].IDINV_OPERATIONS);
				dispatchEvent(new Event(Commande.CMDE_OPE_CREATION_COMPLETE));
			}else{
				dispatchEvent(new Event(Commande.CMDE_OPE_CREATION_ERROR));
			}			
		}
						
//---------LISTE---------------------------------------------------------------------------		
		private var _liste : ArrayCollection;
		private function setliste(values : ArrayCollection):void{
			var ligne : Object;
			_liste = new ArrayCollection();
			for (ligne in values){
				var cmde : Commande = new Commande();
				cmde.commandeID = values[ligne].IDCDE_COMMANDE;
				cmde.commentaire = values[ligne].COMMENTAIRES;				
				cmde.statut = values[ligne].STATUS;								
				cmde.ratio = values[ligne].LAST_RATIO;
				_liste.addItem(cmde);			
			}			
		}
		
		public function get liste():ArrayCollection{
			return _liste;
		} 
		
		public function prepareList(idGpe : int):void{
			 		
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_GETLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		public function  prepareUserList(idGpe : int):void{			
			 		
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Commande.COLDFUSION_COMPONENT,
													Commande.COLDFUSION_GETUSERLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		private function getListResultHandler(re : ResultEvent):void{
			try{
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(Commande.LISTE_COMPLETE));
			}catch(e : Error){
				dispatchEvent(new Event(Commande.LISTE_ERROR));
			}			
		}
	}
}
