﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	
	<!--- Supprimer un droit spécifique à une fonction --->
	<cffunction name="executeAct" access="remote" returntype="struct" >
		<cfargument name="data"	type="struct" required="true">
		<cftry>
			<!---Suppression du droit spécifique --->
			<cfstoredproc procedure="pkg_m28.deleteParametreFonction" datasource="ROCOFFRE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idParamFct#" type="in" >
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out" >
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.RESULT = p_retour>
			<cfreturn CreateReponse("AS004", 1, qResult)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AS004", 0, ThrowCfCatchError("AS004",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>