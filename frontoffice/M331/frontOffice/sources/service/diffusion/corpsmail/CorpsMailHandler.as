package service.diffusion.corpsmail
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class CorpsMailHandler
	{
		private var _model:CorpsMailModel;
		
		public function CorpsMailHandler(_model:CorpsMailModel)
		{
			this._model=_model;
		}
		
		internal function getCorpsMailAppHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.corpsMail(event.result as ArrayCollection);
			}
		}
	}
}