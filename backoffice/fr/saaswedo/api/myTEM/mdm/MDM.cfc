<!--- Spécifications
Factory pour instancier l'implémentation fr.saaswedo.api.mdm.core.services.IMDM correspondant à la propriété PROVIDER des infos MDM
Les instances IMDM sont créées avec les infos MDM correspondant à l'implémentation de ce composant (Infos MDM provenant de la session)
Une instance de l'implémentation IMDM de chaque provider est créée et placée la structure associée à la clé mdmApplicationKey() dans APPLICATION
L'implémentation IMDM utilisée pour une session utilisateur est determinée par la valeur retournée par getProvider()
Toutes les sessions utilisateur ayant la meme valeur getProvider() utiliseront la meme instance de la meme implémentation IMDM
Les infos MDM sont récupérées par le composant parent à l'initialisation en utilisant le groupe qui se trouve dans la session
Ces infos sont initialisées ou mises à jour quand une conditions suivantes est vérifiée :
	- La clé retournée par mdmSessionInfoKey() n'est pas présente dans SESSION.USER (Déconnection/Reconnexion de l'utilisateur)
	- La valeur de SESSION.USER[mdmSessionInfoKey()].ID_GROUPE est différente de SESSION.PERIMETRE.ID_GROUPE (Changement de groupe)
Structure qui stocke les instances IMDM dans APPLICATION {
	<Nom complet du composant qui implémente IMDM>:Instance du composant
} --->
<cfcomponent author="Cedric" displayName="fr.saaswedo.api.myTEM.mdm.MDM" extends="fr.saaswedo.api.myTEM.mdm.Infos"
hint="Extension utilisée dans MyTEM permettant d'obtenir l'implémentation IMDM correspondant aux infos MDM de session
provenant des fonctions héritées. Les instances IMDM sont placées dans un emplacement prédéfini du scope APPLICATION">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.myTEM.mdm.MDM">
		<cfreturn "fr.saaswedo.api.myTEM.mdm.MDM">
	</cffunction>
	
	<!--- Refactoring de fr.saaswedo.api.myTEM.mdm.AdminMdm --->
	<cffunction access="public" name="forceMdmInfosUpdate" returntype="void" description="LOCK:Exclusive" hint="Force la mise à jour des infos MDM">
		<!--- Mise à jour forcée des infos MDM en session : Le paramètre de forceMdmInfosUpdate() est ignoré --->
		<cfset SUPER.setMdmInfos({INFO="Ce paramètre est ignorée par l'implémentation IMdmServerInfo utilisée dans M27"},TRUE)>
	</cffunction>
	
	<!--- Refactoring de fr.saaswedo.api.myTEM.mdm.AdminMdm --->
	<cffunction access="public" name="clearIMdmProviders" returntype="void" description="LOCK:Exclusive"
	hint="Supprime les instances IMDM créées et placées dans le scope APPLICATION. Les prochains appels à getIMDM() créeront de nouvelles instances IMDM">
		<cflock scope="APPLICATION" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<!--- La suppression est effectuée avec structClear() --->
			<cfif structKeyExists(APPLICATION,mdmApplicationKey())>
				<cfset structClear(APPLICATION[mdmApplicationKey()])>
				<!--- Log d'information concernant l'action effectuée --->
				<cfset var log=GLOBALS().CONST('Remoting.LOG')>
				<cfset var appName=structKeyExists(APPLICATION,"applicationName") ? APPLICATION.applicationName:"N.D">
				<cfset var cgiScript=CGI.SCRIPT_NAME>
				<cfset var remoteHost=CGI.REMOTE_HOST>
				<cfif isDefined("COOKIE") AND structKeyExists(COOKIE,"IP_ORIGINE")>
					<cfset remoteHost=urlDecode(COOKIE.IP_ORIGINE)>
				</cfif>
				<cfset var adminLog="#remoteHost# using #cgiScript#">
				<cflog type="information" text="[#log#] IMDM Providers cleared on #CGI.SERVER_NAME# on application #appName# by #adminLog#">
			</cfif>
		</cflock>
	</cffunction>
	
	<!--- =========== Fonctions de gestion des instances des implémentations IMDM du scope APPLICATION =========== --->
	
	<cffunction access="public" name="getIMDM" returntype="fr.saaswedo.api.mdm.core.services.IMDM" description="LOCK:Exclusive"
	hint="Retourne l'instance IMDM correspondant à getProvider() et qui a été placée dans une structure du scope APPLICATION. Si l'instance n'existe pas
	encore alors elle sera créée avec la référence THIS fournie à son constructeur init(). Une exception est levée si le provider n'est pas supporté">
		<cflock scope="APPLICATION" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset var provider=getProvider()>
			<!--- Crée la structure qui va stocker les instances IMDM dans APPLICATION --->
			<cfif NOT structKeyExists(APPLICATION,mdmApplicationKey())>
				<cfset APPLICATION[mdmApplicationKey()]={}>
			</cfif>
			<cfset var mdmInstances=APPLICATION[mdmApplicationKey()]>
			<!--- L'instance est créée si elle n'exsite pas encore --->
			<cfif NOT structKeyExists(mdmInstances,provider)>
				<!--- Chaque instance aura le provider correspondant comme clé --->
				<cfset mdmInstances[provider]=createIMDM(THIS)>
				<!--- Log d'information concernant l'action effectuée --->
				<cfset var log=GLOBALS().CONST('Remoting.LOG')>
				<cfset var appName=structKeyExists(APPLICATION,"applicationName") ? APPLICATION.applicationName:"N.D">
				<cflog type="information" text="[#log#] IMDM instance of provider '#provider#' created for application #appName#">
			</cfif>
			<cfreturn mdmInstances[provider]>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="createIMDM" returntype="fr.saaswedo.api.mdm.core.services.IMDM" description="LOCK:Exclusive"
	hint="Retourne une instance IMDM correspondant à la valeur de mdmInfos.getProvider(). L'instance est créée avec mdmInfos fourni à son constructeur">
		<cfargument name="mdmInfos" type="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" required="true" hint="Infos MDM">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<!--- Vérification de la valeur du provider --->
		<cfset var mdmProvider=ARGUMENTS.mdmInfos.getProvider()>
		<cfif (TRIM(mdmProvider) EQ "") OR (TRIM(mdmProvider) NEQ mdmProvider)>
			<cfthrow type="Custom" errorcode="#code#" message="Invalid MDM provider value '#mdmProvider#'">
		</cfif>
		<!--- Détermine l'implémentation IMDM correspondant à la valeur retournée par getProvider() --->
		<cfset var mdmServerCfc="">
		<cfswitch expression="#mdmProvider#">
			<cfcase value="zenprise">
				<cfset mdmServerCfc="fr.saaswedo.api.mdm.impl.zenprise.Zenprise"> 
			</cfcase>
			<cfcase value="mobileiron">
				<cfset mdmServerCfc="fr.saaswedo.api.mdm.impl.mobileIron.MobileIron">
			</cfcase>
			<cfcase value="airwatch">
				<cfset mdmServerCfc="fr.saaswedo.api.mdm.impl.airwatch.Airwatch">
			</cfcase>
			<cfdefaultcase>
				<cfthrow type="Custom" errorcode="#code#" message="Unsupported MDM provider value '#mdmProvider#'">
			</cfdefaultcase>
		</cfswitch>
		<cfreturn createObject("component",mdmServerCfc).init(ARGUMENTS.mdmInfos)>
	</cffunction>
	
	<cffunction access="private" name="mdmApplicationKey" returntype="String" hint="Clé placée dans APPLICATION contenant les instances IMDM">
		<cfreturn "MDM">
	</cffunction>
</cfcomponent>