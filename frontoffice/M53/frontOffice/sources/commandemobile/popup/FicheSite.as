package commandemobile.popup
{
	import commandemobile.system.Site;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class FicheSite extends TitleWindow
	{
		public var idPoolGestionnaire	:int;
		[Bindable]
		public var site 				:Site = new Site(); 
		public var inpup_cp 			:TextInput;
		public var input_code			:TextInput;
		public var input_commentaire	:TextArea;
		public var input_libelle		:TextInput;
		public var input_ville			:TextInput;
		public var input_adresse		:TextArea;
		public var input_pays			:ComboBox;
		public var input_ref			:TextInput;
		public var creation 			:Boolean;
		public var bt_enregistrer		:Button;
		public var bt_fermer			:Button;
				
		public function FicheSite(site : Site = null, creation :Boolean = false)
		{
			this.creation = creation;
			this.site = new Site();
			super();
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			this.site.addEventListener("addSite",affecter_site_to_gestionnaire_handler);
			this.site.listePays();
		}
		private function initIHM(evt : FlexEvent):void
		{
			bt_enregistrer.addEventListener(MouseEvent.CLICK,enregistrer_handler);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer_handler);
		}
		
		protected function textInputHandler(idTxtBx:int):void
		{
			switch(idTxtBx)
			{
				case 0: input_libelle.errorString = "";
						break;
				case 1: input_adresse.errorString = "";
						break;
				case 2: inpup_cp.errorString = "";
						break;
				case 3:	input_ville.errorString = "";
						break;
				case 4:	input_pays.errorString = "";
						break;
			}
		}
		
		protected function enregistrer_handler(evt : Event):void
		{
			var ok:Boolean = true;
			if(input_libelle.text == "")
			{
				ok = false;
				input_libelle.errorString = "Champ obligatoire!";
			}
			if(input_adresse.text == "")
			{
				ok = false;
				input_adresse.errorString = "Champ obligatoire!";
			}
			if(inpup_cp.text == "")
			{
				ok = false;
				inpup_cp.errorString = "Champ obligatoire!";
			}
			if(input_ville.text == "")
			{
				ok = false;
				input_ville.errorString = "Champ obligatoire!";
			}
			if(input_pays.selectedIndex == -1)
			{
				ok = false;
				input_pays.errorString = "Champ obligatoire!";
			}
				
			if(ok)
			{
				site.cp_site			= inpup_cp.text;
				site.ref_site 			= input_ref.text;
				site.code_interne_site	= input_code.text;
				site.commentaire_site 	= input_commentaire.text;
				site.libelle_site		= input_libelle.text;
				site.commune_site		= input_ville.text;
				site.adr_site 			= input_adresse.text;
				site.pays_site 			= input_pays.selectedItem.PAYSCONSOTELID;
				site.save_site();
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo("Vous devez saisir tous les champs obligatoires !","Consoview",null);
			}
		}

		private function affecter_site_to_gestionnaire_handler(e:Event):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"affecte_site_pool",
																		process_affecte_site_pool);
			RemoteObjectUtil.callService(op,site.id,idPoolGestionnaire);
		}
		
		private function process_affecte_site_pool(re : ResultEvent):void
		{
			if(re.result > 0)
			{
				Alert.show("Site créé et affecté au pool");
				dispatchEvent(new Event("refreshSite",true));
				PopUpManager.removePopUp(this);
			}
			else
			{
				Alert.show("Affectation du site au pool impossible!","Consoview");
			}
		}
		
		protected function fermer_handler(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}

	}
}