<cfcomponent displayname="LockZP002" >

	<cffunction name="action">
	
		<cfargument name="param" type="struct" required="true">
		
		<cfset service = createObject("component","fr.consotel.api.mdm.partner.public.Zenprise").getPublicZenprise()>
		<cfset variable = service.unlockDevice(param.serialNumber,param.imei,param.waitTime) >
				
	</cffunction>

</cfcomponent>
