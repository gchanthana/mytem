package univers.inventaire.equipements.lignes.ihm {
	import flash.events.Event;
	import mx.events.FlexEvent;
	import univers.inventaire.equipements.lignes.recherche.Recherche;
	import mx.controls.Alert;
	import univers.inventaire.equipements.lignes.recherche.RechercheEvent;
	import mx.utils.ObjectUtil;
	import univers.inventaire.equipements.lignes.fiche.FicheLigneEvent;
	import univers.inventaire.equipements.lignes.fiche.aut.FicheLigneAUT;
	import univers.inventaire.equipements.lignes.fiche.FicheLigneIHM;
	import univers.inventaire.equipements.lignes.fiche.FicheLigneFactory;
	import flash.errors.IllegalOperationError;
	
	public class SitesLignes extends SitesLignesIHM {
		private var ficheLigneFactory:FicheLigneFactory;
		
		public function SitesLignes() {
			ficheLigneFactory = FicheLigneFactory.getFactory();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:Event):void {
			// IHM de Recherche
			//selectedChild = rechercheIHM;
			//rechercheIHM.addEventListener(RechercheEvent.GRIDLIGNES_CHANGE_EVENT,displayFicheLigne);
			// IHM de Fiche Ligne
			ficheLigneIHM.addEventListener(FicheLigneEvent.FICHELIGNE_RETURN_CLICK,showRechercheIHM);
		}
		
		public function init():void {
			displayFicheLigne(new Event(null))
		}
		
		private function displayFicheLigne(event:Event):void {
			var sousteteObject:Object = null; //(event as RechercheEvent).getSelectedItem();
			if(ficheLigneIHM != null)
				removeChild(ficheLigneIHM);
			try {
				ficheLigneIHM = ficheLigneFactory.createFicheLigne("ANA",sousteteObject);
				// Ré-Affectation du handler pour le click de retour.
				ficheLigneIHM.addEventListener(FicheLigneEvent.FICHELIGNE_RETURN_CLICK,showRechercheIHM);
				addChild(ficheLigneIHM);
				this.selectedChild = ficheLigneIHM;
			} catch(error:IllegalOperationError) {
				ficheLigneIHM = null;
				Alert.show(error.message);
			}
		}
		
		private function showRechercheIHM(event:Event):void {
			//this.selectedChild = rechercheIHM;
		}
	}
}
