package intranet.GestionProduit.renderers
{
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;

	public class UISelectUnlimitedPlanIR extends CheckBox
	{
		public function UISelectUnlimitedPlanIR()
		{
			super();
		}
		
		override public function set data(value:Object):void
		{
			selected = false;// TODO Auto Generated method stub
			if(value != null)
			{
				super.data = value;
				enabled  = (data.BOOL_DRT == 0)?false:true;
				selected = (data.PLAN_MCAP==-1)?true:false;
				if(data.BOOL_DRT==0)
				{
					selected=false;
					data.PLAN_MCAP=0;
				}
			}
		}
		
		override protected function clickHandler(event:MouseEvent):void
		{
			super.clickHandler(event);
			data.PLAN_MCAP = (selected)?-1:0;
		}
		
		
		
	}
}