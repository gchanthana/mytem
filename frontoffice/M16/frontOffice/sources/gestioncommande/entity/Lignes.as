package gestioncommande.entity
{
	public class Lignes
	{
		
		public var TYPE					:String = "";		//String contenant "Ajout" ou "Résiliation"
		public var IDTYPE				:int 	= -1;		//id de l'Ajout (1) ou de la Résiliation (0)
		public var LIGNE				:String = "";		//numero de la ligne
		public var IDSOUS_TETE			:int 	= 0;		//id de la ligne
		public var IDPRODUIT_CATALOGUE	:int 	= 0;		//id de l'option
		public var LIBELLE_PRODUIT		:String = "";		//libelle de l'option

		public function Lignes()
		{
		}

	}
}