package univers.parametres.parametrelogin.ihm.customs
{
	import mx.controls.dataGridClasses.DataGridColumn;

	public class SpecialDataGridColumn extends DataGridColumn
	{
		private var _customProperty:Object;
		
		public function SpecialDataGridColumn(columnName:String=null)
		{
			super(columnName);
		}
		
		
		public function get customProperty():Object
		{
			return _customProperty;
		}
		
		[Bindable]
		public function set customProperty(value:Object):void
		{
			_customProperty = value;
		}
		
	}
}