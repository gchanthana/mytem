package univers.parametres.perimetres.mainComponent
{
	import flash.events.Event;

	public class FicheCollaborateurEvent extends Event
	{
		public var _label:String;
		
		public function FicheCollaborateurEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}