<cfcomponent displayname="fr.consotel.api.AppProxy" hint="Proxy used to access current APPLICATION Scope if extending AbstractApplication">
	<!--- APPLICATION PROXY REFERENCE (Auteur : Cedric)
		STORAGE PATTERN : Proxy isntance is a SINGLETON stored by APPLICATION scope
		VARIABLES.TARGET_APP : Target Application
	--->

	<cffunction access="public" name="setApplicationTarget" returntype="fr.consotel.api.AppProxy"
																	hint="Used by Application to set its proxy. Call currentApplicationProxy to get proxy instance instead">
		<cfargument name="appObject" type="fr.consotel.api.AbstractApplication" required="true" hint="Instance of AbstractApplication for which to create Proxy">
		<cfif NOT isDefined("APPLICATION.APP_PROXY_KEY")>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE" message="An error occurred when trying to get create application proxy"
													detail="Unable to create proxy for current application or it does not support proxy">
		<cfelse>
			<cfif NOT structKeyExists(APPLICATION,APPLICATION.APP_PROXY_KEY)>
				<cfset VARIABLES.TARGET_APP=ARGUMENTS.appObject>
				<cfreturn THIS>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_INVOCATION" message="Current application proxy is already set"
													detail="Call currentApplicationProxy() to get current application proxy instead">
			</cfif>
		</cfif>
	</cffunction>

	<cffunction access="public" name="currentApplicationProxy" returntype="fr.consotel.api.AppProxy"
			hint="Returns an initialized Proxy instance for current application.
			Throws ILLEGAL_STATE if application is not accessible through proxy. Throws ILLEGAL_ACCESS if unable to access proxy">
		<cfif NOT isDefined("APPLICATION.APP_PROXY_KEY")>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE" message="An error occurred when trying to get current application proxy"
																			detail="Unable to get current application proxy or does not support proxy">
		<cfelse>
			<cfif NOT structKeyExists(APPLICATION,APPLICATION.APP_PROXY_KEY)>
				<cfthrow type="Custom" errorcode="ILLEGAL_ACCESS" message="An error occurred when trying to get current application proxy"
																				detail="Unable to get current application proxy or does not support proxy">
			<cfelse>
				<cfreturn APPLICATION[APPLICATION.APP_PROXY_KEY]>
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getApplicationLogger" returntype="fr.consotel.api.utils.logging.ILogger" hint="Returns current application logger">
		<cfreturn VARIABLES.TARGET_APP.getAppLogger()>
	</cffunction>
	
	<cffunction access="public" name="getApplicationTimeOut" returntype="date" hint="Returns application timeout">
		<cfreturn VARIABLES.TARGET_APP.getAppTimeOut()>
	</cffunction>
</cfcomponent>