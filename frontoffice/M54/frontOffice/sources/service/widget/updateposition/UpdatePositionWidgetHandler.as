package service.widget.updateposition
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class UpdatePositionWidgetHandler
	{
		private var _model:UpdatePositionWidgetModel;
		
		public function UpdatePositionWidgetHandler(_model:  UpdatePositionWidgetModel):void
		{
			this._model = _model;
		}	
		internal function UpdatePositionWidgetResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValueWidget(evt);
			}
		}
	}
}