package service.fonction.FctGestionPartenaire
{
	import composants.util.ConsoviewAlert;
	
	import entity.CertificationVO;
	import entity.CiviliteVO;
	import entity.ContratVO;
	import entity.FormationVO;
	import entity.InterlocuteurVO;
	import entity.LangueVO;
	import entity.LieuFormationVO;
	import entity.ModuleFormationVO;
	import entity.PartenaireSimpleVO;
	import entity.PartenaireVO;
	import entity.ParticipantFormationVO;
	import entity.SiteVO;
	import entity.SujetFormationVO;
	import entity.TypePlanTarifVO;
	
	import event.fonction.fctGestionPartenaire.FctGestionPartenaireEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import utils.Constantes;
	
	public class FctGestionPartenaireDatas extends EventDispatcher
	{
//		private var _listePartenaire		:ArrayCollection = new ArrayCollection();
		private var _listePartenaireSimple	:ArrayCollection = new ArrayCollection();
		private var _listeSites				:ArrayCollection = new ArrayCollection();
		private var _listeContrats			:ArrayCollection = new ArrayCollection();
		private var _listeInterlocuteurs	:ArrayCollection = new ArrayCollection();
		private var _listeFormations		:ArrayCollection = new ArrayCollection();
		private var _listeParticipants		:ArrayCollection = new ArrayCollection();
		
		public function treatListePartenaires(retour:Object):void
		{
			_listePartenaireSimple.source = new Array();
			var tailleRet:int = (retour.LISTEPARTENAIRES as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				
				// A ne pas supprimer les 2 ligne suivants pour partenaire.
				var partn:PartenaireVO = new PartenaireVO();
				partn.fill(retour.LISTEPARTENAIRES[i]);
				// PartenaireSimpleVO et PartenaireVO sont liées
				var partnSimple:PartenaireSimpleVO = new PartenaireSimpleVO(partn)
				listePartenaireSimple.addItem(partnSimple);
			}
			
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_PARTENAIRE));
		}
		
		public function treatListeSites(retour:Object):void
		{
			listeSites.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var site:SiteVO = new SiteVO();
				site.fill(retour.RESULT[i]);
				listeSites.addItem(site);
			}
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_SITES));
		}
		
		public function treatListeContrats(retour:Object):void
		{
			listeContrats.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var contrat:ContratVO = new ContratVO();
				contrat.fill(retour.RESULT[i]);
				listeContrats.addItem(contrat);
			}
			
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_CONTRATS));
		}
		
		public function treatListeFormations(retour:Object):void
		{
			listeFormations.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var formation:FormationVO = new FormationVO();
				formation.fill(retour.RESULT[i]);
				listeFormations.addItem(formation);
			}
			
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_FORMATIONS));
		}
		
		public function treatListeInterlocuteurs(retour:Object):void
		{
			listeInterlocuteurs.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var interloc:InterlocuteurVO = new InterlocuteurVO;
				interloc.fill(retour.RESULT[i]);
				listeInterlocuteurs.addItem(interloc);
			}
			
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_INTERLOCUTEURS));
		}
		
		public function treatNiveauxCertifications(retour:Object):void
		{
			Constantes.listeCertifications.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var nivCertif:CertificationVO = new CertificationVO();
				nivCertif.fill(retour.RESULT[i]);
				Constantes.listeCertifications.addItem(nivCertif);
			}
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.NIVEAUX_CERTIFICATION));
		}
		
		public function treatTypesPlanTarifaire(retour:Object):void
		{
			Constantes.listeTypesPlanTarifaire.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var typePlanTarif:TypePlanTarifVO = new TypePlanTarifVO;
				typePlanTarif.fill(retour.RESULT[i]);
				Constantes.listeTypesPlanTarifaire.addItem(typePlanTarif);
			}
			
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.TYPES_PLAN_TARIFAIRE));
		}
		
		public function treatAjoutEditSite(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.EDITION_SITE));
		}
		
		public function treatModifContart(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.MODIF_CONTRAT_PARTN));
		}
		
		public function treatDesactiveContrat(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.DESACTIVE_CONTRAT_PARTN));
		}
		
		public function treatAjoutEditInterloc(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.EDITION_INTERLOC));
		}
		
		public function treatAjoutEditParticipants(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.EDITION_PARTICIPANTS));
			
		}
		
		public function treatAjoutEditFormation(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.EDITION_FORMATION));
		}
		
		public function treatAjoutContrat(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.AJOUT_CONTRAT_PARTN));
		}
		
		public function treatListeSujetsFormation(retour:Object):void
		{
			Constantes.listeSujetsFormation.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var module:SujetFormationVO = new SujetFormationVO();
				module.fill(retour.RESULT[i]);
				Constantes.listeSujetsFormation.addItem(module);
			}
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_SUJETS_FORMATION));
		}
		
		public function treatListeLieuxFormation(retour:Object):void
		{
			Constantes.listeLieuxFormation.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var lieu:LieuFormationVO = new LieuFormationVO();
				lieu.fill(retour.RESULT[i]);
				Constantes.listeLieuxFormation.addItem(lieu);
			}
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_LIEUX_FORMATION));
		}
		
		public function treatListeLangues(retour:Object):void
		{
			Constantes.listeLangues.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var langue:LangueVO = new LangueVO();
				langue.fill(retour.RESULT[i]);
				Constantes.listeLangues.addItem(langue);
			}
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_LANGUES));
		}
		
		public function treatParticipantsFormation(retour:Object):void
		{
			listeParticipants.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var participant:ParticipantFormationVO = new ParticipantFormationVO();
				participant.fill(retour.RESULT[i]);
				listeParticipants.addItem(participant);
			}
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_PARTICIPANTS_FORMATION));
		}
		
		public function treatListeCivilites(retour:Object):void
		{
			Constantes.listeCivilites.source = new Array();
			var tailleRet:int = (retour.RESULT as ArrayCollection).length;
			for(var i:int=0; i<tailleRet; i++)
			{
				var civilite:CiviliteVO = new CiviliteVO();
				civilite.fill(retour.RESULT[i]);
				Constantes.listeCivilites.addItem(civilite);
			}
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.LISTE_CIVILITES));
		}
		
		public function treatAjouterPartenaireHandler(retour:Object):void
		{
			this.dispatchEvent(new FctGestionPartenaireEvent(FctGestionPartenaireEvent.AJOUT_PARTENAIRE));
		}
		
		// GETTERS et SETTERS
		[Bindable]
		public function get listeFormations():ArrayCollection { return _listeFormations; }
		
		public function set listeFormations(value:ArrayCollection):void
		{
			if (_listeFormations == value)
				return;
			_listeFormations = value;
		}
		
		[Bindable]
		public function get listeInterlocuteurs():ArrayCollection { return _listeInterlocuteurs; }
		
		public function set listeInterlocuteurs(value:ArrayCollection):void
		{
			if (_listeInterlocuteurs == value)
				return;
			_listeInterlocuteurs = value;
		}
		
		[Bindable]
		public function get listeContrats():ArrayCollection { return _listeContrats; }
		
		public function set listeContrats(value:ArrayCollection):void
		{
			if (_listeContrats == value)
				return;
			_listeContrats = value;
		}
		
		[Bindable]
		public function get listeSites():ArrayCollection { return _listeSites; }
		
		public function set listeSites(value:ArrayCollection):void
		{
			if (_listeSites == value)
				return;
			_listeSites = value;
		}
		
		[Bindable]
		public function get listeParticipants():ArrayCollection { return _listeParticipants; }
		
		public function set listeParticipants(value:ArrayCollection):void
		{
			if (_listeParticipants == value)
				return;
			_listeParticipants = value;
		}
		
		public function get listePartenaireSimple():ArrayCollection { return _listePartenaireSimple; }
		
		public function set listePartenaireSimple(value:ArrayCollection):void
		{
			if (_listePartenaireSimple == value)
				return;
			_listePartenaireSimple = value;
		}
	}
}