<cfcomponent displayname="ConcreteDemandeCreator">
	
	<cffunction name="createDemande" access="public" returntype="any" output="false">
		<cfargument name="format" required="true" type="string">	
		<cfargument name="type" required="true" type="string" default="DemandeDeResiliation">	
		<cfset demande=createObject("component","fr.consotel.consoview.inventaire.cycledevie.export." & #format# & "." & #type#)>				
		<cfreturn demande>
	</cffunction>
	
</cfcomponent>
  