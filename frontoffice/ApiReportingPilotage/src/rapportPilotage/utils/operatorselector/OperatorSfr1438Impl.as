package rapportPilotage.utils.operatorselector
{
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	import rapportPilotage.utils.SelectorAbstract;
	import rapportPilotage.utils.serialization.json.*;
	import rapportPilotage.vo.SelectorParametre;

	public class OperatorSfr1438Impl extends SelectorAbstract  
	{
		public var operateur :Array=[{libelle:'Sfr',selected:true}];
				
		[Bindable]public var arrayOperateur:ArrayCollection=new ArrayCollection(operateur);
				
		public var comboBoxOperatorSfr1438:ComboBox;
		public var arrayCleLibelle :Object;
		
		public function OperatorSfr1438Impl(selector:Object=null)
		{
			super(selector);	
		}
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
		
		protected function operatorsfr1438impl1_creationCompleteHandler(event:FlexEvent):void
		{
			comboBoxOperatorSfr1438.dataProvider=arrayOperateur;
		}
	}
}