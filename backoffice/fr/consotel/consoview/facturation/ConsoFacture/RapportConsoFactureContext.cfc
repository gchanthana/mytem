<!---
Package : fr.consotel.consoview.facturation.consofacture
--->
<cfcomponent name="RapportConsoFactureContext">
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="PERIMETRE_INDEX" required="true" type="numeric"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="REPORT_TYPE" required="true" type="string"/>
		<cfargument name="RAISON_SOCIALE" required="true" type="string"/>
		<cfset idPerimetre = PERIMETRE_INDEX>
		<cfif Not StructIsEmpty(FORM)>
			<cfset rapportParams = structNew()>
			<cfloop collection="#FORM#" item="i">
				<cfif i neq "FIELDNAMES" AND i neq "CHARTAREA">
					<cfset structInsert(rapportParams,"#i#","#FORM[i]#")>
				</cfif>
			</cfloop>
		</cfif>
		<cfset reportProcessor = createObject("component",
					"fr.consotel.consoview.facturation.#REPORT_TYPE#.Rapport#REPORT_TYPE##TYPE_PERIMETRE#Strategy")>
		<cfset reportProcessor.displayRapport(idPerimetre,rapportParams)>
	</cffunction>
	
	<cffunction name="getAboData" access="remote" returntype="query">
		<cfargument name="PERIMETRE_INDEX" required="true" type="numeric"/>
		<cfargument name="TYPE_PERIMETRE" required="true" type="string"/>
		<cfargument name="dateDeb" required="true" type="string"/>
		<cfargument name="dateFin" required="true" type="string"/>
		<cfargument name="REPORT_TYPE" required="true" type="string"/>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",DATEDEB)>
		<cfset DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<cfset DATE_YEAR_YYYY = datePart("YYYY",DATEFIN)>
		<cfset DATE_MONTH_M = datePart("M",DATEFIN)>
		<cfset DATEFIN =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
		<cfset idPerimetre = PERIMETRE_INDEX>
		<cfset reportProcessor = createObject("component",
					"fr.consotel.consoview.facturation.#REPORT_TYPE#.Rapport#REPORT_TYPE##TYPE_PERIMETRE#Strategy")>
		<cfset qGetReportData = reportProcessor.getAboData(idPerimetre,dateDeb,dateFin)>
		<cfreturn qGetReportData>
	</cffunction>
</cfcomponent>
