<cfquery name="qGetResolution" dataSource="ROCOFFRE">
	SELECT id_resolution, descrip_resolution
	FROM CIR_RESOLUTION
	order by id_resolution
</cfquery>

<html>
	<head>
		<title>CIR SHOOTER v1.1</title>
		<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
		<script type="text/javascript">
			//<![CDATA[
		        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
			//]]>
		 </script>

		 <style type="text/css">
		 	td {
			 	font-family: Verdana,Geneva,sans-serif;
			 	font-size: 10px;
			 }
			 .ligne {
				 background-color: gray;
			 }
			 .titre {
				 font-size: 24px;
				 text-align: right;
				 vertical-align: middle;
			 }
			 body {
				 background-color: rgba(191,191,191,0.36);
			 }
		 </style>
  	</head>
	<body>
<cfoutput>
<form action="process_url.cfm" method="post">
	<input type="hidden" name="u" value="#url.u#">
	<input type="hidden" name="titre" value="#url.t#">
	<table width="650">
		<tr>
			<td><img src="img/saaswedo_relief_sansfond_moyen.png" width="150"></td>
			<td class="titre">CIR SHOOTER v1.2</td>
		</tr>
		<tr height="3">
			<td class="ligne" width="100%" colspan="2"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		<tr>
			<td>Collaborateur</td>
			<td><input name="user" value="#url.user#" size="32" type="email"></td>
		</tr>
		<tr>
			<td>Ticket JIRA</td>
			<td><input type="text" name="jirakey"></td>
		</tr>
		<tr>
			<td>Contexte</td>
			<td>Cette page 
				<select name="resolution">
					<cfloop query="qGetResolution">
						<option value="#qGetResolution.id_resolution#" <cfif qGetResolution.id_resolution eq "3">selected="true"</cfif>>#qGetResolution.descrip_resolution#</option>
					</cfloop>
				</select>
			</td>
		</tr>
		<tr height="1">
			<td class="ligne" width="100%" colspan="2"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		<tr height="20">
			<td>Titre</td>
			<td><b>#url.t#</b></td>
		</tr>
		<tr height="20">
			<td>URL</td>
			<td>#url.u#</td>
		</tr>
		<tr>
			<td>Sauvegarder la page:</td>
			<td><input type="checkbox" name="savepage" value="0"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		<tr>
			<td valign="top">Commentaire <br><i>(4000 caractères max)</i></td>
			<td>
				<textarea name="comment" style="width: 100%;height:40;">
				</textarea>
			</td>
		</tr>
		<tr>
			<td valign="top">Contenu <br><i>(4000 caractères max)</i></td>
			<td>
				<textarea name="contenu" style="width: 100%;height:120;">
				#url.x#
				</textarea>
			</td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="2"></td>
		</tr>
		
		<tr>
			<td></td>
			<td><input type="submit" name="btnSubmit" value="Shoot !!"></td>
		</tr>
	</table>
</form>
</cfoutput>

	</body>
</html>