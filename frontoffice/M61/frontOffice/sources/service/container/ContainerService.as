package service.container
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.events.CollectionEvent;
	import mx.formatters.NumberBaseRoundType;
	import mx.formatters.NumberFormatter;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import classe.Client;
	import classe.Comment;
	import classe.Doc;
	import classe.Login;
	import classe.ModuleVO;
	
	import events.container.ContainerEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import popup.ContainerprogressBar;
	
	[Bindable] public class ContainerService extends EventDispatcher
	{
	
	//--------------------- VARIABLES ---------------------//	
		
		// Liste des logins d'un client		
		public var listeLogin:ArrayCollection;
		// Liste des logins d'un client	pour l'onglet depot en masse de fichier	
		public var listeLoginDepot:ArrayCollection;		
		// Liste des Docs
		public var listeDoc:ArrayCollection;
		// Liste des Commentaires d'un rapport
		public var listeCommentaire:ArrayCollection;	
		// Liste des Commentaires d'un rapport
		public var listeModule:ArrayCollection;
		// Liste des clients CV 
		public var listeClientCV:ArrayCollection
		// Bytearray du fichier ZIP créé aprés download
		public var zipByteArray:ByteArray	
		// Liste des parametres d'un document
		public var listeParam:ArrayCollection
		// Liste des accés de l'utilisateur
		public var listeAccesGroupe:ArrayCollection
	
		private static var TEMPS_LIMITE:int = 6 * 3600000
		private var zipToDownload:FileReference
		private var progress:ContainerprogressBar
		
		
		
		//--------------------- METHODES ---------------------//
		
		/* */
		public function ContainerService()
		{
			listeLogin 			= new ArrayCollection()
			listeLoginDepot 	= new ArrayCollection()
			listeCommentaire 	= new ArrayCollection()
			listeModule 		= new ArrayCollection()
			listeClientCV 		= new ArrayCollection()
			listeParam 			= new ArrayCollection()
			listeAccesGroupe	= new ArrayCollection()
		}
		
		/* appel la procedure qui renvoit la liste des logins d'un client donné. */
		public function getlisteLogin(idracine:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerlistuserbyracine", 
																		listeLogin_handler);
			RemoteObjectUtil.callService(op,idracine);
		}
		
		/* appel la procedure qui renvoit la liste des logins d'un client donné dans l'optique du partage */
		public function getlisteLoginShare(idracine:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerlistuserbyracinetoShare", 
																		listeLogin_handler);
			RemoteObjectUtil.callService(op,idracine);
		}
		
		/* appel la procedure qui renvoit la liste des logins d'un client donné pour l'onglet dépot en masse de fichier. */
		public function getlisteLoginDepot(idracine:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerlistuserbyracine", 
																		listeLoginDepot_handler);
			RemoteObjectUtil.callService(op,idracine);
		}
		
		/* appelle la procédure qui renvoit la liste des commentaires d'un rapport */	
		public function getlisteCommentaire(iddoc:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerListComments", 
																		listeComment_handler);
			RemoteObjectUtil.callService(op,iddoc); 
		}
		
		/* appel à la procédure qui renvoit la liste des Docs */
		public function getListeDoc(iduser:int,type:int,search:String,module:String,nbstart:int,nbaffiche:int,idracine:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerListRapport_V2", 
																		listeDoc_handler);
			RemoteObjectUtil.callService(op,iduser,type,search,module,nbstart,nbaffiche,idracine); 
		}
		
		/* appelle à la procédure qui supprime des Docs */
		public function suppListeDoc(arr:ArrayCollection,app_loginid:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"DELETEDocumentTableContainer", 
																		DELETEDocumentTableContainer_handler);
			RemoteObjectUtil.callService(op,arr,app_loginid); 
		}
		
		/* appelle à la procédure qui télécharge les Docs via AIR */
		public function downloadListeDoc(arr:ArrayCollection,archivename:String = "defaultName"):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"DOWNLOADListDocumentClient", 
																		DOWNLOADListDocument_handler);
			RemoteObjectUtil.callService(op,arr,archivename); 
		}
		
		/* appelle à la procédure qui télécharge les Docs via SAAS */
		public function downloadListeDocSAAS(arr:ArrayCollection,archivename:String = "defaultName"):void
		{
			zipToDownload = new FileReference()
			var request:URLRequest = new URLRequest()
			request.url = moduleContainerIHM.NonSecureUrlBackoffice+"/fr/consotel/consoview/M61/ZipUploadProcess.cfm"
			request.method = URLRequestMethod.GET
	 		var variables:URLVariables = new URLVariables();
			variables.ARUUID = arrayToStringComma(arr)
			variables.ARCHIVENAME = archivename
			variables.APPLOGINID = CvAccessManager.getSession().USER.CLIENTACCESSID
			variables.USERNAME = CvAccessManager.getSession().USER.NOM+' '+CvAccessManager.getSession().USER.PRENOM
			request.data = variables 
			
			zipToDownload.download(request,archivename+".zip")
			zipToDownload.addEventListener(IOErrorEvent.IO_ERROR,ioerrorhandler)
			zipToDownload.addEventListener(Event.SELECT,zipselectHAndler)
			zipToDownload.addEventListener(Event.COMPLETE,zipcompleteHandler)
//			var sendRequest:URLRequest = new URLRequest(moduleContainerIHM.NonSecureUrlBackoffice+"/fr/consotel/consoview/M61/ZipUploadProcess.cfm");
//				sendRequest.method = "POST";
//				var variables:URLVariables = new URLVariables();
//				variables.ARUUID = arrayToStringComma(arr)
//				variables.ARCHIVENAME = archivename
//				variables.APPLOGINID = CvAccessManager.getSession().USER.CLIENTACCESSID
//				variables.USERNAME = CvAccessManager.getSession().USER.NOM+' '+CvAccessManager.getSession().USER.PRENOM
//				sendRequest.data = variables 
//				navigateToURL(sendRequest, "_blank");
		}
		
		/* */
		private function arrayToStringComma(arr:ArrayCollection):String
		{
			var str:String =""
			for each(var obj:Object in arr)
			{
				str +=obj.UUID+","
			}
			return str.substring(0,str.length-1)
		}
		
		/* appelle à la procédure qui enregistre un commentaire */
		public function enregistrerCommentaire(iddoc:int,app_loginid:int,comment:String,date:String,heure:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerAddComments", 
																		addComment_handler);
			RemoteObjectUtil.callService(op,iddoc,app_loginid,comment,date,heure);
		}
		
		/* appelle à la procédure qui renvoit la liste des modules auquel l'utilisateur a accés. */
		public function getListeModule(iduser:int,idracine:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerListModuleByUser", 
																		listemodule_handler);
			RemoteObjectUtil.callService(op,iduser,idracine);
		}
		
		/* appelle à la procédure qui renvoit la liste des modules auquel l'utilisateur a accés. */
		public function getGroupeList(iduser:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.access.AccessManager",
																		"getGroupList", 
																		getGroupList_handler);
			RemoteObjectUtil.callService(op,iduser);
		}
		
		/* appelle à la procédure qui retourne la liste des clients CV */
		public function getListeClientCV():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerListClientCV", 
																		listeClient_handler);
			RemoteObjectUtil.callService(op);
		}
		
		/* appelle à la procédure qui envoit un mail pour notifier le client que plusieurs documents ont été déposé sur son container */
		public function SENDMailPublicationParProductionClient(obj:Object):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"SENDMailPublication", 
																		SENDMailPublication_handler);
			RemoteObjectUtil.callService(op,obj.APP_LOGINID,obj.IDRACINE,obj);
		}
		
		/* apppelle à la procédure qui partage une liste de fichier à une liste d'utilisateur */
		public function shareDoc(arrLogin:ArrayCollection,arrFile:ArrayCollection,app_loginid:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerShareRapport", 
																		containerShareRapport_handler);
			RemoteObjectUtil.callService(op,arrLogin,arrFile,app_loginid);
		}
		
		/* apppelle à la procédure qui partage une liste de fichier à une liste d'utilisateur */
		public function depotDoc(arrLogin:ArrayCollection,arrFile:Array,app_loginid:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"containerDepotRapport", 
																		containerDepotRapport_handler);
			RemoteObjectUtil.callService(op,arrLogin,arrFile,app_loginid);
		}
		
		/* récupére les paramètres d'un document */
		public function getParam(idfile:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"LISTEparams", 
																		LISTEparams_handler);
			RemoteObjectUtil.callService(op,idfile);
		}
		
		/*	Change le statut d'un fichier */
		private function setStatus(uuid:String,idstatus:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M61.ContainerService",
																		"setStatus", 
																		setStatus_handler);
			RemoteObjectUtil.callService(op,uuid,idstatus);
		}	
		
		/* envoit un mail d'erreur */
//		private function sendMailErreur():void
//		{
//			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
//																		"fr.consotel.consoview.M61.ContainerService",
//																		"SENDMailPublicationErreur", 
//																		SENDMailPublicationErreur_handler);
//			RemoteObjectUtil.callService(op);
//		}
		
		
		
		//---------------- RETOURS - HANDLERS ----------------//
		
		/* Gère la liste des fichiers dans le répertoire de dépôt des fichiers */
		private function listeDoc_handler(re:ResultEvent):void
		{
			if(listeDoc == null)
				listeDoc = new ArrayCollection();
			else
				listeDoc.removeAll();
			
			listeDoc.addEventListener(CollectionEvent.COLLECTION_CHANGE,dataChangeHandler);

			if(re.result != null && re.result.length > 0)
			{
				for each(var obj:Object in re.result)
				{
					var rp:Doc 			= 	new Doc();
					rp.DATE_DEMANDE		= 	obj.DATE_DEMANDE;
					rp.DEMANDEUR_NOM	=	obj.LOGIN_NOM;
					rp.DEMANDEUR_PRENOM	=	obj.LOGIN_PRENOM;	
					if(obj.ISINERROR == 0)
						rp.BOOL_ERROR	=	false;
					else
						rp.BOOL_ERROR	=	true;
					rp.FORMAT			=	obj.FORMAT_FICHIER;
					rp.LIBELLE			=	obj.NOM_DETAILLE_RAPPORT;
					rp.MODULE			=	obj.NOM_MODULE_SOURCE;
					rp.OBJPARAMS		=	new XML(obj.PARAMETRES);	
					rp.SELECTED			=	false;
					if(obj.ISSHARED == 0)
						rp.SHARED		=	false;
					else
						rp.SHARED		=	true;
					rp.LIBELLE_SHORT	=	obj.SHORT_NAME_RAPPORT;
					rp.FILEID 			= 	obj.FILEID;
					rp.UUID				=	obj.UUID;
					rp.STATUS			=	obj.STATUS;
					rp.STATUS_ID		= 	obj.IDSTATUS;
					var today:Date 		= 	new Date();
					
					// si le rapport a été publié plus de 6 heures avant
					if(today.time - rp.DATE_DEMANDE.time > TEMPS_LIMITE && rp.STATUS_ID == 1)
					{
						rp.STATUS_ID 	= 3;
						rp.STATUS		= ResourceManager.getInstance().getString('M61', 'En_erreur');
						setStatus(rp.UUID,3);
//						sendMailErreur()
					}
					else
					{
						switch(obj.IDSTATUS.toString())
						{
							case "1" : rp.STATUS_ID= 1; break;
							case "2" : rp.STATUS_ID= 2; break;
							case "3" : rp.STATUS_ID= 3;	break;
						}						
					}
					var nbformat:NumberFormatter = new NumberFormatter();
					nbformat.precision 	= 1;
					nbformat.rounding 	= NumberBaseRoundType.UP;
					rp.TAILLE			=	parseFloat(nbformat.format(obj.FILE_SIZE / 1000000));
					if(obj.ISENCOURS == 0)
						rp.ENCOURS		= 	false;
					else
						rp.ENCOURS		= 	true;
					rp.NBRECORD 		= obj.NBRECORD;
					rp.APP_LOGINID		= obj.APP_LOGINID;
					listeDoc.addItem(rp);
				}
			}
			listeDoc.refresh();
			dispatchEvent(new ContainerEvent(ContainerEvent.LISTEDOC_RESULTEVENT));
		}
		
		/* */
		private function dataChangeHandler(ce:CollectionEvent):void
		{
			
		}
		
		/* */
		private function listeLogin_handler	(re:ResultEvent):void
		{
			listeLogin = new ArrayCollection()
			if(re.result != null && re.result.length > 0)
			{
				for each(var obj:Object in re.result as ArrayCollection)
				{
					var lo:Login = new Login();
					lo.LOGIN 		= obj.LOGIN;
					lo.NOM			= obj.LOGIN_NOM;
					lo.PRENOM		= obj.LOGIN_PRENOM;
					lo.NOMCOMPLET	= lo.PRENOM+' '+lo.NOM;
					lo.APP_LOGINID 	= obj.APP_LOGINID;
					listeLogin.addItem(lo);
				}
				sortListeDG(listeLogin);
				
			}
		}
		
		protected function sortByName(obj1:Object, obj2:Object):int
		{
			return ObjectUtil.stringCompare(obj1.NOMCOMPLET, obj2.NOMCOMPLET);
		}
		
		protected function sortListeDG(arrColl:ArrayCollection):void
		{
			var sortField:SortField = new SortField();
			sortField.compareFunction = sortByName;
			sortField.descending = false;
			
			var sort:Sort = new Sort();
			sort.fields = [sortField];
			
			arrColl.sort = sort;
			arrColl.refresh();
		}
		
		/* */
		private function listeLoginDepot_handler(re:ResultEvent):void
		{
			listeLoginDepot = new ArrayCollection()
			if(re.result != null && re.result.length > 0)
			{
				for each(var obj:Object in re.result as ArrayCollection)
				{
					var lo:Login = new Login();
					lo.LOGIN 		= obj.LOGIN;
					lo.NOM			= obj.LOGIN_NOM;
					lo.PRENOM		= obj.LOGIN_PRENOM;
					lo.NOMCOMPLET	= lo.PRENOM+' '+lo.NOM;
					lo.APP_LOGINID 	= obj.APP_LOGINID;
					listeLoginDepot.addItem(lo);
				}
			}
		}
		
		/* */
		private function listeComment_handler(re:ResultEvent):void
		{
			listeCommentaire = new ArrayCollection();
			if(re.result != null && re.result.length > 0)
			{
				for each(var obj:Object in re.result)
				{
					var cm:Comment = new Comment();
					cm.DATE 			=	obj.DATE_ADDED;
					cm.IDCOMMENTAIRE	=	obj.APP_LOGINID;
					cm.LIBELLE			=	obj.COMMENTAIRES;
					cm.NOM				=	obj.LOGIN_NOM;
					cm.PRENOM			=	obj.LOGIN_PRENOM;
					
					listeCommentaire.addItem(cm);
				}
			}
		}
		
		/* */
		private function addComment_handler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				
			}
		}
		
		/* */
		private function LISTEparams_handler(re:ResultEvent):void
		{
			listeParam = re.result as ArrayCollection;
			dispatchEvent(new ContainerEvent(ContainerEvent.LISTEPARAMS_RESULTEVENT));
		}
		
		/* */
		private function listemodule_handler(re:ResultEvent):void
		{
			listeModule = new ArrayCollection();
			
			var m:ModuleVO = new ModuleVO();
			m.LIBELLE = ResourceManager.getInstance().getString('M61', 'Tous_les_modules');
			listeModule.addItem(m);
			
			if(re.result != null && re.result.length >0)
			{
				for each(var obj:Object in re.result)
				{
					var md:ModuleVO = new ModuleVO();
					md.IDMODULE		= obj.IDMODULE;
					md.LIBELLE		= obj.MODULE;
					listeModule.addItem(md);
				}
			}
		}
		
		/* */
		private function listeClient_handler(re:ResultEvent):void
		{
			listeClientCV = new ArrayCollection();
			if(re.result != null && re.result.length > 0)
			{
				for each(var obj:Object in re.result)
				{
					var cl:Client = new Client();
					cl.IDRACINE	= obj.IDRACINE;
					cl.LIBELLE	= obj.LIBELLE_GROUPE_CLIENT;
					listeClientCV.addItem(cl);
				}
			}
		}
		
		/* */
		private function SENDMailPublication_handler(re:ResultEvent):void
		{
			trace('[ContainerService] Mail envoyé avec succés');
		}
		
		/* */
		private function DELETEDocumentTableContainer_handler(re:ResultEvent):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.VALIDATE_EVENT));
		}
		
		/* */
		private function containerShareRapport_handler(re:ResultEvent):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.VALIDATE_EVENT));
		}
		
		/* */
		private function containerDepotRapport_handler(re:ResultEvent):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.VALIDATEDEPOT_EVENT));
		}
		
		/* */
		private function DOWNLOADListDocument_handler(re:ResultEvent):void
		{
			zipByteArray = re.result as ByteArray;
			dispatchEvent(new ContainerEvent(ContainerEvent.ZIP_READY,null,true));
		}
		
		/* */
		private function zipselectHAndler(evt:Event):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.ZIP_SELECT));
		}
		
		/* */
		private function zipcompleteHandler(evt:Event):void
		{	
			dispatchEvent(new ContainerEvent(ContainerEvent.ZIP_READY));
		}
		
		/* */
		private function ioerrorhandler(ioe:IOErrorEvent):void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.ZIP_ERROR));
		}
		
		/* */
		private function setStatus_handler(re:ResultEvent):void
		{
			trace('[CONTAINER] résultat du changement de status du fichier : '+re.result);
		}
		
		/* */
		private function getGroupList_handler(re:ResultEvent):void
		{
			if(listeAccesGroupe != null && listeAccesGroupe.length > 0)
				listeAccesGroupe.removeAll();
			else
				listeAccesGroupe = new ArrayCollection();
			
			listeAccesGroupe = re.result as ArrayCollection;
			var obj:Object = new Object();
			obj.IDCLIENTS_PV = "";
			obj.IDGROUPE_CLIENT = -1;
			obj.LIBELLE_GROUPE_CLIENT = ResourceManager.getInstance().getString('M61', 'Tous_les_clients');
			listeAccesGroupe.addItemAt(obj,0); 
		}
	}
}