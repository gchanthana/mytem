package paginatedatagrid.columns
{
    import mx.controls.Alert;
    import mx.controls.dataGridClasses.DataGridColumn;

    public class SortableColumn extends DataGridColumn
    {
        private var _columDBName:String;
        private var _isSearchable:Boolean = true;
        private var _isOrderable:Boolean = true;

        public function SortableColumn(columnName:String = null)
        {
            super(columnName);
        }

        public function get columDBName():String
        {
            return _columDBName;
        }

        public function set columDBName(columDBName:String):void
        {
            this._columDBName = columDBName;
        }

        public function get isSearchable():Boolean
        {
            return _isSearchable;
        }

        public function set isSearchable(value:Boolean):void
        {
            _isSearchable = value;
        }

        public function get isOrderable():Boolean
        {
            return _isOrderable;
        }

        public function set isOrderable(value:Boolean):void
        {
            _isOrderable = value;
        }
    }
}