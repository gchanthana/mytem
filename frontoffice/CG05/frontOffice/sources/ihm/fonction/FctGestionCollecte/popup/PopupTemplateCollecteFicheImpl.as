package ihm.fonction.FctGestionCollecte.popup
{
    import entity.CodeAction;
    import entity.GestionCollecte.ModelLocator;
    import entity.GestionCollecte.StaticData;
    import entity.GestionCollecte.enum.EBoolean;
    import entity.GestionCollecte.enum.ETypePJ;
    import entity.GestionCollecte.vo.CollecteTypeVO;
    
    import event.fonction.fctGestionCollecte.FctGestionCollecteEvent;
    
    import flash.events.Event;
    import flash.events.FocusEvent;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.ui.Keyboard;
    
    import ihm.fonction.FctGestionCollecte.component.alert.AlertExtended;
    import ihm.fonction.FctGestionCollecte.component.alert.AlertExtendedPopup;
    import ihm.fonction.FctGestionCollecte.component.upload.UploadCompIHM;
    
    import mx.containers.Form;
    import mx.containers.FormItem;
    import mx.containers.FormItemDirection;
    import mx.containers.HBox;
    import mx.containers.TitleWindow;
    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.CheckBox;
    import mx.controls.ComboBox;
    import mx.controls.Label;
    import mx.controls.LinkButton;
    import mx.controls.RadioButtonGroup;
    import mx.controls.Spacer;
    import mx.controls.TextInput;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.events.ValidationResultEvent;
    import mx.managers.PopUpManager;
    import mx.validators.NumberValidator;
    import mx.validators.Validator;
    
    import service.fonction.FctGestionCollecte.FctGestionCollecteService;
    
    import utils.GestionCollecte.ConversionUtils;

    [Bindable]
    /**
     * Cette classe étend la classe <code>TitleWindow</code> et permet d'afficher une popup
     * visant à créer/éditer/consulter un template de collecte.
     * Elle assure le <b>Pilotage</b> de l'IHM  associé <code>NewCollecteTypeIHM</code>
     */
    public class PopupTemplateCollecteFicheImpl extends TitleWindow
    {
        public var dataLocator:ModelLocator;
        public static const MODE_CREATION:String = "mode_creation";
        public static const MODE_EDITION:String = "mode_edition";
        public static const W_FORM:Number = 600;
        // IHM
        public var i_vbSouscription:VBox;
        public var i_hbMode:HBox;
        public var i_lbTitle:Label;
        public var i_fiExtranet:FormItem;
        public var i_tiExtranet:TextInput;
        public var i_uploadCompContrat:UploadCompIHM;
        public var i_rb_souscription:RadioButtonGroup;
        public var i_form:Form;
        public var i_fiPublish:FormItem;
        public var i_cbxPublish:ComboBox;
        public var i_cbxRecupMode:ComboBox;
        public var i_cbxOperateur:ComboBox;
        public var i_cbxDataType:ComboBox;
        public var i_cbxRecupRoic:ComboBox;
        public var i_txiRoicInfo:TextInput;
        public var i_btOk:Button;
        public var i_btCancel:Button;
        public var i_lbRocfName:Label;
        public var i_taAnteriorite:FormItem;
        public var i_taContraintes:FormItem;
        public var i_taCout:FormItem;
        public var i_cbxContrat:CheckBox;
        public var i_cbxExtranet:CheckBox;
        public var i_txiExtranet:TextInput;
        public var i_delaiDispo:TextInput;
        public var cbPublishEdit:ComboBox;
        // Validateurs
        public var name_validator:Validator;
        public var prestation_validator:Validator;
        public var operateur_validator:NumberValidator;
        public var dataType_validator:NumberValidator;
        public var recupMode_validator:NumberValidator;
        public var roic_validator:Validator;
        public var validatorEnabled:Boolean = false;
        // Controle
        private var _mode:String = "";
        private var _currentCollectTypeVO:CollecteTypeVO;
        private var _validatorArr:Array;
        private var collecteTypeService:FctGestionCollecteService;
        public var formIsValidForValidation:Boolean;
        public var templateIsCompleted:Boolean = false;
        public var infoFicheHasChanged:Boolean = false;

        public function PopupTemplateCollecteFicheImpl()
        {
            super();
        }

        /**
         * Initialisation générale de cette classe posant les écouteurs captant
         * la fin de l'initialisation de tous les composants enfants.
         */
        public function init():void
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
        }

        /**
         * Initialisation générale de cette classe.
         */
        protected function onCreationCompleteHandler(event:FlexEvent):void
        {
            dataLocator = ModelLocator.getInstance();
            this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
            initData();
            initListeners();
            initDisplay();
        }

        /**
         * Initialise l'affichage de cette interface.
         */
        private function initDisplay():void
        {
            i_taAnteriorite.visible = false;
            i_taCout.visible = false;
            i_taContraintes.visible = false;
        }

        /**
         * Initialise les validateurs de champs en fonction
         * du mode de la fiche (création / édition)
         */
        private function initValidators():void
        {
            validatorEnabled = true;
            validatorArr = new Array();
            if(mode == MODE_CREATION)
            {
                validatorArr.push(name_validator);
                validatorArr.push(prestation_validator);
                validatorArr.push(operateur_validator);
                validatorArr.push(dataType_validator);
            }
            else
            {
                validatorArr.push(prestation_validator);
                validatorArr.push(recupMode_validator);
            }
            validatorArr.push(roic_validator);
            validatorArr.push(recupMode_validator);
        }

        /**
         * Initialise les écouteurs.
         */
        private function initListeners():void
        {
            i_btOk.addEventListener(MouseEvent.CLICK, i_btOk_clickHandler);
            i_btCancel.addEventListener(MouseEvent.CLICK, i_btCancel_clickHandler);
            this.addEventListener(CloseEvent.CLOSE, i_btCancel_clickHandler);
			this.addEventListener(KeyboardEvent.KEY_DOWN, testKey);
        }

        /**
         * Initialise et récupère les données nécessaires
         * à l'initialisation du composants.
         */
        private function initData():void
        {
            currentCollectTypeVO = new CollecteTypeVO();
        }

        /**
         * Configure et met à jour les éléments de la fiche en fonction
         * de son mode d'utilisation (EDITION ou CREATION) de façon
         * à rendre certains champs non modifiables dans le cas de l'édition.
         */
        public function updateDisplay():void
        {
            if(mode == MODE_EDITION)
            {
                this.titleIcon = StaticData.imgEdit;
                this.title = resourceManager.getString('M28', '_Edition_du_template_') + currentCollectTypeVO.libelleTemplate;
                i_hbMode.removeAllChildren();
                var lbName:Label = new Label();
                var lbOperateur:Label = new Label();
                var lbDataType:Label = new Label();
                var lbPublish:Label = new Label();
                cbPublishEdit = new ComboBox();
                cbPublishEdit.styleName = "square";
                cbPublishEdit.addEventListener(Event.CHANGE, checkFormCompletion);
                var spacer:Spacer = new Spacer();
                spacer.width = 20;
                lbName.setStyle("fontSize", 15);
                lbOperateur.setStyle("fontSize", 15);
                lbDataType.setStyle("fontSize", 15);
                lbDataType.setStyle("fontWeigth", "bold");
                lbOperateur.setStyle("fontWeigth", "bold");
                lbPublish.text = resourceManager.getString('M28', 'Publier');
                cbPublishEdit.dataProvider = EBoolean.getCollection();
                cbPublishEdit.selectedIndex = currentCollectTypeVO.isPublished.value as Number;
                lbName.htmlText = resourceManager.getString('M28', 'Nom____b__') + currentCollectTypeVO.libelleTemplate + "</b>";
                lbOperateur.htmlText = resourceManager.getString('M28', 'Op_rateur____b__') + currentCollectTypeVO.operateurNom.toLocaleUpperCase() + "</b>";
                lbDataType.htmlText = resourceManager.getString('M28', '____Types_de_donn_es____b__') + dataLocator.getTypeCollecteLabel(currentCollectTypeVO.idcollectType).toLocaleUpperCase() +
                    "</b>";
                i_hbMode.addChild(lbPublish);
                i_hbMode.addChild(cbPublishEdit);
                i_hbMode.addChild(spacer);
                i_hbMode.addChild(lbOperateur);
                i_hbMode.addChild(lbDataType);
                // maj des champs mode de récup et souscrption en fonction des valeurs du template
                i_cbxRecupMode.selectedIndex = dataLocator.getRecupModeIndex(currentCollectTypeVO.idcollectMode);
                currentCollectTypeVO.anteriorite.value == 0 ? i_taAnteriorite.visible = false : i_taAnteriorite.visible = true;
                currentCollectTypeVO.couts == 0 ? i_taCout.visible = false : i_taCout.visible = true;
                currentCollectTypeVO.contraintes.value == 0 ? i_taContraintes.visible = false : i_taContraintes.visible = true;
                i_lbRocfName.text = currentCollectTypeVO.libelleRocf;
                i_txiExtranet.text = currentCollectTypeVO.modeSouscriptionUrl;
            }
            else
            {
                i_cbxRecupMode.selectedIndex = -1;
                i_cbxOperateur.selectedIndex = -1;
                i_cbxDataType.selectedIndex = -1;
                this.title = resourceManager.getString('M28', 'Cr_ation_d_un_nouveau_template_de_collecte');
            }
        }

        /**
         * A l'ouverture d'une fiche en mode création , un <b>identifiant
         * unique</b> est nécéssaire à transmettre au template pour stocker
         * les documents associés.
         * Appel et prépare donc le service de creation d'un UID
         */
        private function createUIDStorage():void
        {
            ModelLocator.getInstance().createUIDStorage();
            ModelLocator.getInstance().addEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS, onUidCreatedHandler);
            ModelLocator.getInstance().addEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR, onUidCreatedHandler);
            ModelLocator.getInstance().collecteTypeService.createUIDStorage("GCOL", CodeAction.CREATE_UIDSTORAGE);
        }

        /**
         * Capte la reponse serveur de création de l'UID
         * Affecte le VO en conséquence
         */
        private function onUidCreatedHandler(event:FctGestionCollecteEvent):void
        {
            switch(FctGestionCollecteEvent(event).type)
            {
                case FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR:
					trace("onUidCreatedHandler- Erreur dans la création d'un UID");
                    break;
                case FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS:
                    currentCollectTypeVO.uidcollecteTemplate = dataLocator.collecteTypeService.myDatas.uid;
                    break;
                default:
                    break;
            }
            dataLocator.collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_SUCCESS, onUidCreatedHandler);
            dataLocator.collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_UID_CREATED_ERROR, onUidCreatedHandler);
        }

        protected function closeHandler(event:Event):void
        {
            // i_btCancel_clickHandler(event);
        }

        /**
         *  teste l'url indiqué dans l'input <code> i_tiExtranet </code>
         */
        private function btnTestUrl_clickHandler(event:MouseEvent):void
        {
            navigateToURL(new URLRequest(i_tiExtranet.text));
        }

        /**
         *  Au focus out sur le champs extranet teste la validité de l'url
         *  indiquée
         */
        protected function i_tiExtranet_focusOutHandler(event:FocusEvent):void
        {
            // url  invalide
            if(!ConversionUtils.urlIsValid(i_tiExtranet.text))
            {
                dataLocator.consalerte(this, resourceManager.getString('M28', 'Url_invalide__V_rifier_votre_saisie'), resourceManager.getString('M28', 'Champs_invalides'), StaticData.imgNoValid);
            }
            i_tiExtranet.removeEventListener(FocusEvent.FOCUS_OUT, i_tiExtranet_focusOutHandler);
        }

		protected function testKey(evt:KeyboardEvent):void
		{
			if (evt.keyCode == Keyboard.ESCAPE)
			{
				this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
			}
		}
		
		
        /**
         * Ouvre une popup de demande de confirmation d'annulation
         * 2 options:
         * - <b>Quitter sans sauvegarder</b>
         * - <b>Revenir à la fiche</b>
         */
        protected function askForConfirmation(event:Event):void
        {
            var popUp:AlertExtendedPopup = new AlertExtendedPopup();
            popUp.addEventListener(AlertExtended.USER_ACTION_EVENT, onConfirmationCancelPopupBtn_clickHandler);
            PopUpManager.addPopUp(popUp, this, true);
            PopUpManager.centerPopUp(popUp);
            popUp.titlePopup = resourceManager.getString('M28', 'Confirmer_l_annulation');
            popUp.libelle = resourceManager.getString('M28', 'Vous__tes_sur_le_point_de_quitter_la_fiche_Que');
            popUp.btn1.label = resourceManager.getString('M28', 'Quitter_sans_sauvegarder');
            popUp.btn2.label = resourceManager.getString('M28', '_Revenir___la_fiche');
        }

        /**
         * Capte la réponse utilisateur de confirmation d'Annulation
         * de création/édition d'un template
         */
        protected function onConfirmationCancelPopupBtn_clickHandler(event:Event):void
        {
            var popup:AlertExtendedPopup = AlertExtendedPopup(Event(event).currentTarget);
            popup.removeEventListener(AlertExtended.USER_ACTION_EVENT, onConfirmationCancelPopupBtn_clickHandler);
            // click sur la coche 'ne plus me demander'
            dataLocator.askForConfirmCancelPopup = !popup.rememberDecision;
            // Cas Quitter sans sauvegarder
            if(popup.btn1Clicked)
            {
                switch(mode)
                {
                    case MODE_CREATION:
                        dataLocator.destroyLastUID(currentCollectTypeVO.uidcollecteTemplate);
                        break;
                    case MODE_EDITION:
                        this.dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_CANCEL));
                        break;
                    default:
                        break;
                }
                popup.close();
                PopUpManager.removePopUp(this);
            }
            else
            {
                // Cas:  Revenir à la fiche	
                popup.close();
            }
        }

        /**
         * Ouvre et configure une popup de confirmation déclenchée à la demande
         * de sauvegarde ('Bouton Valider') demandant à l'utilisateur s'il
         * souhaite effectuer une publication(si complet) ou compléter son formulaire
         * (si incomplet) avant la sauvegarde du template
         */
        protected function askForPublication():void
        {
            var libel:String = "";
            var libel_btn1:String = "";
            var libel_btn2:String = "";
            if(mode == MODE_EDITION)
            {
                currentCollectTypeVO.isPublished = cbPublishEdit.selectedItem as EBoolean;
            }
            // cas le template est déja attribué à la publication , on sauvegarde directement
            if(currentCollectTypeVO.isPublished.value == 1)
            {
                createEditCollecteTemplate(currentCollectTypeVO);
            }
            else
            {
                // Cas : la fiche est complete et peut être publiée
                if(currentCollectTypeVO.isCompleted.value == 1 && currentCollectTypeVO.isPublished.value == 0)
                {
                    libel = resourceManager.getString('M28', 'Ce_template_dispose_des_informations_n_cessaires_pour__tre_publi__') + resourceManager.getString('M28', 'Que_souhaitez_vous_faire__');
                    libel_btn1 = resourceManager.getString('M28', 'Sauvegarder_sans_Publier');
                    libel_btn2 = resourceManager.getString('M28', 'Publier_le_template');
                }
                // Cas : la fiche est incomplete 
                else if(currentCollectTypeVO.isCompleted.value == 0)
                {
                    libel = resourceManager.getString('M28', 'Ce_template_dispose_des_informations_n_cessaires_pour__tre_valid__') + resourceManager.getString('M28', 'Toutefois__toutes_les_informations_demandées_sont_requises_pour') +
						resourceManager.getString('M28', '__de_publier_ce_template_aupr_s_des_clients_Consoview_Que_souhait');
                    libel_btn1 = resourceManager.getString('M28', 'Sauvegarder_le_template_sans_Publier');
                    libel_btn2 = resourceManager.getString('M28', 'Revenir___la_fiche_pour_compl_ter_les_informations');
                }
                // Activation de l'alerte						
                var popUp:AlertExtendedPopup = new AlertExtendedPopup();
                popUp.titlePopup = resourceManager.getString('M28', 'Sauvegarde_et_publication_de_template');
                popUp.addEventListener(AlertExtended.USER_ACTION_EVENT, onPublicationPopupBtn_clickHandler);
                PopUpManager.addPopUp(popUp, this.parent, true);
                PopUpManager.centerPopUp(popUp);
                popUp.libelle = libel;
                popUp.btn1.label = libel_btn1;
                popUp.btn2.label = libel_btn2;
            }
        }

        /**
         * Capte l'action utilisateur  depuis la popup de confirmation de publication
         * et effectue les actions nécessaires au choix :
         * - <b>Sauvegarde sans publier</b> : fermeture popup de confirme et de la fiche puis sauvegarde
         * - <b>Sauvegarde en publiant</b> : affectation de la propiété isPublished, fermeture des popup et sauvegarde
         * - Si incomplète,<b> Revenir à la fiche</b>
         * la sauvegarde est déclenché par <code>createEditCollecteTemplate</code>
         *
         */
        protected function onPublicationPopupBtn_clickHandler(event:Event):void
        {
            var popup:AlertExtendedPopup = AlertExtendedPopup(Event(event).currentTarget);
            popup.removeEventListener(AlertExtended.USER_ACTION_EVENT, onPublicationPopupBtn_clickHandler);
            dataLocator.askForPublishPopup = !popup.rememberDecision;
            // Cas : La fiche est complete 
            if(currentCollectTypeVO.isCompleted.value == 1)
            {
                // Sauvegarde sans publier
                if(popup.btn1Clicked)
                {
                    popup.close();
                    PopUpManager.removePopUp(this);
                }
                //  publication
                else
                {
                    currentCollectTypeVO.isPublished = EBoolean.TRUE;
                    popup.close();
                    PopUpManager.removePopUp(this);
                }
                // sauvegarde
                trace("Sauvergarde du template : \n " + currentCollectTypeVO.toXML());
                createEditCollecteTemplate(currentCollectTypeVO);
            }
            // Cas : La fiche est incomplete 
            else
            { // Sauvegarde sans publier
                if(popup.btn1Clicked)
                {
                    popup.close();
                    createEditCollecteTemplate(currentCollectTypeVO);
                    PopUpManager.removePopUp(this);
                }
                // Revenir à la fiche
                else
                {
                    popup.close();
                }
            }
        }

        public function get mode():String
        {
            return _mode;
        }

        /**
         * Mode du formulaire de la fiche (Edition ou création)
         * <code>MODE_CREATION </code> |  <code>MODE_EDITION </code>
         * @param :
         *
         */
        public function set mode(value:String):void
        {
            _mode = value;
            if(mode == MODE_CREATION)
            {
                createUIDStorage();
                currentCollectTypeVO.idcollecteTemplate = 0;
            }
        }

        public function get currentCollectTypeVO():CollecteTypeVO
        {
            return _currentCollectTypeVO;
        }

        /**
         * le vo courant de type <code>CollecteTypeVO</code> qui peuple le formulaire
         *  (DataBinding Bidirectionnel : le vo affecte les champs , et la modif des champs)
         *
         */
        public function set currentCollectTypeVO(value:CollecteTypeVO):void
        {
            _currentCollectTypeVO = value;
        }

        public function get validatorArr():Array
        {
            return _validatorArr;
        }

        public function set validatorArr(value:Array):void
        {
            _validatorArr = value;
        }

        /**
         * Capte le clicked sur le bouton de validation du formulaire.
         * Déclenche les validateurs de champs du formulaire.
         * Si tout est OK ,Ouvre une popup de demande de publication ou non avant la sauvegarde
         *
         */
        protected function i_btOk_clickHandler(event:MouseEvent):void
        {
            // si l'utilisateur a modifié une entrée
            if(infoFicheHasChanged)
            {
                initValidators();
                if(validatorArr)
                {
                    var validatorErrorArray:Array = Validator.validateAll(validatorArr);
                    var isValidForm:Boolean = validatorErrorArray.length == 0;
                    if(isValidForm)
                    {
                        if(dataLocator.askForPublishPopup)
                        {
                            askForPublication();
                        }
                        else
                        {
                            createEditCollecteTemplate(currentCollectTypeVO);
                        }
                    }
                }
            }
            else
            {
                PopUpManager.removePopUp(this);
            }
        }

        protected function i_btCancel_clickHandler(event:Event):void
        {
            if(infoFicheHasChanged)
            {
                if(dataLocator.askForConfirmCancelPopup)
                {
                    askForConfirmation(event);
                }
                else
                {
                    this.dispatchEvent(new FctGestionCollecteEvent(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_CANCEL))
                    PopUpManager.removePopUp(this);
                }
            }
            else
            {
                PopUpManager.removePopUp(this);
            }
        }

        /**
         * Déclenche et prépare l'appel des services distant pour la création/édition
         * d'un template dont toutes les infos sont incluses dans le VO <code>CollecteTypeVO</code>
         * passé en param ( à noter que si <code>vo.idcollecteTemplate=0</code> : mode création
         * sinon édition du template dont l'id est indiqué).
         */
        private function createEditCollecteTemplate(vo:CollecteTypeVO):void
        {
            // instanstion dynamique des champs n'ayant pas de binding bidirectionnel
            if(mode == MODE_EDITION)
            {
                // vo.isPublished = cbPublishEdit.selectedItem as EBoolean;
            }
            else
            {
                vo.iduserCreate = ModelLocator.getInstance().userId;
            }
            vo.iduserModif = ModelLocator.getInstance().userId;
            vo.recuperationRoic = i_cbxRecupRoic.selectedItem as String;
            vo.roicInfos = i_txiRoicInfo.text;
            vo.delaiDispo = new Number(i_delaiDispo.text);
            if(i_cbxExtranet.selected)
            {
                vo.modeSouscription = 1;
            }
            else
            {
                vo.modeSouscription = 2;
            }
            collecteTypeService = new FctGestionCollecteService();
            collecteTypeService.createEditCollecteTypeVo("GCOL", CodeAction.CREATE_COLLECTETYPEVO, vo);
            collecteTypeService.myHandlers.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS, onVoCreateUpdateHandler);
            collecteTypeService.myHandlers.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, onVoCreateUpdateHandler);
        }

        /**
         * Capte la réponse serveur concernant le bon déroulement de la création/édition
         * d'un template .
         * Informe l'utilisateur et dispatch de l'event
         */
        private function onVoCreateUpdateHandler(event:FctGestionCollecteEvent):void
        {
            var mess:String = "";
            collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS, onVoCreateUpdateHandler);
            collecteTypeService.myHandlers.removeEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, onVoCreateUpdateHandler);
            // création du message
            switch(FctGestionCollecteEvent(event).type)
            {
                // Mise à jour OK
                case FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS:
                    mode == MODE_CREATION ? mess = resourceManager.getString('M28', 'Le_Template_') + currentCollectTypeVO.libelleTemplate + resourceManager.getString('M28', '_a__t__cr_ee_avec_succ_s_') : mess = resourceManager.getString('M28', 'Le_Template_') +
                        currentCollectTypeVO.libelleTemplate + resourceManager.getString('M28', '_a_bien__t__mis___jour');
                    PopUpManager.removePopUp(this);
                    break;
                // Mise à jour KO
                case FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR:
                    mode == MODE_CREATION ? mess = resourceManager.getString('M28', 'Erreur_dans_la_cr_ation_du_template_') + currentCollectTypeVO.libelleTemplate + ".\n" : mess = resourceManager.getString('M28', 'Erreur_dans_la_mise___jour_du_template_') +
                        currentCollectTypeVO.libelleTemplate + ".\n";
                    switch(FctGestionCollecteEvent(event).obj as Number)
                {
                    case -10:
                        mess += resourceManager.getString('M28', 'Champs_obligatoires_vides__Certains_champs_sont_obligatoires_pour_cr_er');
                        break;
                    case -11:
                        mess += resourceManager.getString('M28', 'Template_existant__Un_template_du_nom_et_de_m_me');
                        break;
                    case -12:
                        mess += resourceManager.getString('M28', 'Impossible_de_publier__Tous_les_champs_sont_obli');
                        break;
                    default:
                        break;
                }
                    break;
                default:
                    break;
            }
			FctGestionCollecteEvent(event).obj = mess as String;
            dispatchEvent(event);
        }

        /**
         * A chaque focus out sur un champ du formulaire, vérifie la complétion des infos
         * du template de facon à rendre actif/inactif la combo 'publier' et le bouton valider
         * Ajourne la propriété  du VO <code>currentCollectTypeVO.isCompleted</code> en
         * conséquence
         */
        public function checkFormCompletion(event:Event):void
        {
            infoFicheHasChanged = true;
            trace("checkFormCompletion");
            var vo:CollecteTypeVO = currentCollectTypeVO;
            formIsValidForValidation = true;
            templateIsCompleted = true;
            // Champs obligatoires	
            vo.libelleTemplate == "" ? formIsValidForValidation = false : null;
            //vo.operateur.label == undefined ? formIsValidForValidation = false : null;
            //vo.dataType.value == undefined ? formIsValidForValidation = false : null;
            vo.prestaPerifacturation == "" ? formIsValidForValidation = false : null;
            vo.contraintes.value as Number == 1 && vo.contraintesInfos == "" ? formIsValidForValidation = false : null;
            vo.anteriorite.value as Number == 1 && vo.anterioriteInfos == "" ? formIsValidForValidation = false : null;
            //vo.couts.value as Number == 1 && vo.anterioriteInfos == "" ? formIsValidForValidation = false : null;
            //i_btOk.enabled = formIsValidForValidation;
            formIsValidForValidation == false ? templateIsCompleted = false : null;
            vo.contact == "" ? templateIsCompleted = false : null;
            vo.contactInfos == "" ? templateIsCompleted = false : null;
            //vo.recuperationRoic == "" ? templateIsCompleted = false : null;
            vo.contraintes.value as Number == 1 && vo.contraintesInfos == "" ? templateIsCompleted = false : null;
            vo.anteriorite.value as Number == 1 && vo.anterioriteInfos == "" ? templateIsCompleted = false : null;
            vo.couts as Number == 1 && vo.coutsInfos == "" ? templateIsCompleted = false : null;
            vo.roicInfos == "" ? templateIsCompleted = false : null;
            vo.localisationRoic == "" ? templateIsCompleted = false : null;
            //vo.localisationRoicPath == "" ? templateIsCompleted = false : null;
            //vo.libelleRocf == "" ? templateIsCompleted = false : null;
            vo.localisationRocf == "" ? templateIsCompleted = false : null;
            // vo.localisationRocfPath == "" ? templateIsCompleted = false : null;
            // affectation de l'état d'avancement de la complétion du template
            templateIsCompleted ? vo.isCompleted = EBoolean.TRUE : vo.isCompleted = EBoolean.FALSE;
            vo.isCompleted.value as Number == 0 ? vo.isPublished == false : null;
            if(mode == MODE_EDITION)
            {
                if(vo.isCompleted.value as Number == 1)
                {
                    vo.isPublished = cbPublishEdit.selectedItem as EBoolean;
                }
                else
                {
                    vo.isPublished = EBoolean.FALSE;
                }
            }
            i_fiPublish.enabled = templateIsCompleted;
        }

        private function resetForm(evt:MouseEvent):void
        {
        }
    }
}