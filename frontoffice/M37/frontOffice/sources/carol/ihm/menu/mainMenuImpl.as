package carol.ihm.menu
{
	import carol.VO.AnalyseVO;
	import carol.VO.ElementVO;
	import carol.VO.PerimetreVO;
	import carol.composants.progressBarIHM;
	import carol.event.CarolEvent;
	import carol.popUp.PopupExportPdf;
	import carol.popUp.openAnalyseIHM;
	import carol.popUp.resetAnalyseIHM;
	import carol.popUp.saveAsAnalyseIHM;
	import carol.service.CarolService;
	
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import ilog.charts.OLAPChartLegend;
	import ilog.charts.PivotAttribute;
	import ilog.charts.PivotChart;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Container;
	import mx.core.IFlexDisplayObject;
	import mx.events.CubeEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.olap.IOLAPCube;
	import mx.olap.OLAPCube;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.mxml.HTTPService;
	
	import org.alivepdf.layout.Align;
	import org.alivepdf.layout.Mode;
	import org.alivepdf.layout.Orientation;
	import org.alivepdf.layout.Position;
	import org.alivepdf.layout.Resize;
	import org.alivepdf.layout.Size;
	import org.alivepdf.layout.Unit;
	import org.alivepdf.pdf.PDF;
	import org.alivepdf.saving.Download;
	import org.alivepdf.saving.Method;
	
	[Bindable]
	public class mainMenuImpl extends VBox
	{

//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - POPUP
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpOpen			:openAnalyseIHM;
		private var _popUpSaveAs		:saveAsAnalyseIHM;
		private var _popUpReset			:resetAnalyseIHM;
		private var _popUpProgressBar	:progressBarIHM;
		private var _popUpPerimetre		:perimetreIHM;
		private var _popUpPdf			:PopupExportPdf
		private var ihm					:actionsIHM;
		private var BOOLDATASETLOADED	:Boolean=false
		private var boolErrorInvoked	:Boolean = false;
		private var boolReseted			:Boolean = false;
		private var analyseVoSelected	:AnalyseVO
		private var boolChargerDataset	:Boolean
		private var boolChargerAnalyse	:Boolean
		private var boolAnalyseLoading	:Boolean = false
		public var http					:HTTPService
		public var cube5				:OLAPCube;
		public var cube4				:OLAPCube;
		public var cube3				:OLAPCube;
		public var cube2				:OLAPCube;
		public var cube1				:OLAPCube;
		public var myPivotChart			:PivotChart;
		public var Legend				:OLAPChartLegend;
		public var cs					:CarolService;
		protected var n1				:String = "";
		protected var n2				:String = "";
		protected var n3				:String = "";
		protected var n4				:String = "";
		protected var n5				:String = "";
		
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//

		public function mainMenuImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE ,  creationCompleteHandler);			
			addEventListener(CarolEvent.RESET_DATA, 		resetClickHandler);
			addEventListener(CarolEvent.OPEN__DATA, 		openClickHandler);
			addEventListener(CarolEvent.SAVE__DATA, 		saveAsClickHandler);
			addEventListener(CarolEvent.CHANGE_PERIMETRE,	changePerimetreHandler)
			addEventListener(CarolEvent.PAGE_NUMBER_CHANGED,pageChangeHandler);
			addEventListener(CarolEvent.EXPORT_PDF_EVENT,	exportPdfHandler);
			cs = new CarolService();
			analyseVoSelected = new AnalyseVO(this)
			analyseVoSelected.PERIMETRE = new PerimetreVO()
			analyseVoSelected.PERIMETRE.cs = this.cs
		}

		// ------------------------------------------------------------------------------- //	
		// --------------------------------- EXPORT PDF ---------------------------------- //
		// ------------------------------------------------------------------------------- //
	
		private var mypdf:PDF;
		private var boolContinuePdf	:Boolean = false;
		private var boolExportPdf	:Boolean = false;
		private var arrCharts		:Array = []
		private function exportPdfHandler(e:CarolEvent):void
		{
			Legend = ihm.Legend
			_popUpPdf = new PopupExportPdf()
			_popUpPdf.addEventListener(CarolEvent.EXPORT_PDF_AND_SAVE_ANALYSE_EVENT,exportPdfAndAnalyse);
			_popUpPdf.addEventListener(CarolEvent.EXPORT_PDF_EVENT,exportPdfAlone);
			PopUpManager.addPopUp(_popUpPdf,this)
			PopUpManager.centerPopUp(_popUpPdf)
		}
		private function exportPdfAndAnalyse(e:CarolEvent):void
		{
			PopUpManager.removePopUp(_popUpPdf)
			analyseVoSelected.LIBELLE = _popUpPdf.tiNom.text
			analyseVoSelected.DESCRIPTION = _popUpPdf.tiDescription.text
			analyseVoSelected.sauvegarderAnalyse(myPivotChart);
			exportPdfAlone();
		}
		private function exportPdfAlone(e:CarolEvent=null):void
		{
			if(_popUpPdf.isPopUp)
			{
				analyseVoSelected.LIBELLE = _popUpPdf.tiNom.text
				analyseVoSelected.DESCRIPTION = _popUpPdf.tiDescription.text
				PopUpManager.removePopUp(_popUpPdf)
			}
				
			if(myPivotChart != null && myPivotChart.charts != null && myPivotChart.charts.length >0)
			{
				var con1:Container =myPivotChart.getChildAt(1) as Container;
				var ar:Array = con1.getChildren()
				var dispobj:DisplayObject = ar[ar.length-1]
					
				mypdf = new PDF(Orientation.PORTRAIT,Unit.POINT,Size.A4);
//				addEventListener(FlexEvent.UPDATE_COMPLETE,exportPdfUpdateComplete)
//				callLater(exportPdf)
				exportPdf()
			}
			else
			{
				Alert.show("Veuillez effectuer une analyse avant d'exporter en PDF","avertissement",Alert.OK,this);
			}
		}
		private function exportPdfUpdateComplete(e:FlexEvent):void
		{
			if(boolExportPdf)
			{
				if(boolContinuePdf)
				{
					boolContinuePdf = true
					callLater(exportPdf)
				}
				else
				{
					boolContinuePdf = false
					boolExportPdf = false;
					Alert.show("Appuyez sur 'ok' pour enregistrer le PDF","Enregistrement",Alert.YES | Alert.NO,this,sendPDF);
				}
			}
		}
		private function exportPdf():void
		{
			boolExportPdf = true;
			
			if(myPivotChart.charts != null && myPivotChart.charts.length > 0)
				boolContinuePdf = true;
			
			if(boolContinuePdf)
//			{
				generatePagePDF()				
				
//				if(myPivotChart.hasNextPage)
//				{	
//					myPivotChart.nextPage()
//					boolContinuePdf = true;
//				}
//				else
//					boolContinuePdf = false;
//			}			
//			callLater(invalidateDisplayList)
		} 
		private function generatePagePDF():void
		{
			for(var i:int=0;i<myPivotChart.charts.length;i++)
			{
				var dispObjPagePDF:DisplayObject 	= myPivotChart.charts[i]
				var titreGraph:String 				= myPivotChart.charts[i].parent.title
				var periStr:String					= analyseVoSelected.PERIMETRE.libelle_orga
				var niveauStr:String				= analyseVoSelected.PERIMETRE.niveau
				var dateStr:String					= ""
				var diff:int = 0
				var facteurLegende:Number = 0.50
				
				if(analyseVoSelected.PERIMETRE.libelle_periodedeb.getMonth() == analyseVoSelected.PERIMETRE.libelle_periodefin.getMonth())
					dateStr	= DateField.dateToString(analyseVoSelected.PERIMETRE.libelle_periodedeb,'MM/YYYY')
				else
					dateStr = DateField.dateToString(analyseVoSelected.PERIMETRE.libelle_periodedeb,'MM/YYYY')+' à '+DateField.dateToString(analyseVoSelected.PERIMETRE.libelle_periodefin,'MM/YYYY')
					
				mypdf.addPage()
			
				if(i == 0)
					mypdf.addBookmark(analyseVoSelected.LIBELLE,0)
				mypdf.addBookmark(titreGraph,1)
				
				mypdf.addCell(550,60,analyseVoSelected.LIBELLE.toUpperCase(),1,1,Align.CENTER);
				mypdf.addCell(550,20,'Description : '+analyseVoSelected.DESCRIPTION,1,1,Align.LEFT);
				mypdf.addCell(550,20,'',1,1,Align.LEFT);
				mypdf.addCell(550,20,'Périmètre de la requête initiale :',1,1,Align.LEFT);
				mypdf.addCell(550,20,'	- Périmètre : '+periStr,1,1,Align.LEFT);
				mypdf.addCell(550,20,'	-     Niveau : '+niveauStr,1,1,Align.LEFT);
				mypdf.addCell(550,20,'	-    Période : '+dateStr,1,1,Align.LEFT);
				
				mypdf.addText(titreGraph,30,250);
				
				switch(myPivotChart.charts.length)
				{
					case 1:
						diff = 50
						mypdf.addImage(dispObjPagePDF,new Resize(Mode.FIT_TO_PAGE,Position.LEFT),0,225,750,550);
						break;
					case 2:
						diff = 100
						mypdf.addImage(dispObjPagePDF,new Resize(Mode.NONE,Position.LEFT),0,225,375*0.75,550*0.75);
						break;
					case 3:
						diff = 200
						mypdf.addImage(dispObjPagePDF,new Resize(Mode.NONE,Position.LEFT),0,225,350,200);
						break;
					case 4:
						diff = 200
						mypdf.addImage(dispObjPagePDF,new Resize(Mode.NONE,Position.LEFT),0,225,350,200);
						break;	
					default:
						diff = 250
						mypdf.addImage(dispObjPagePDF,new Resize(Mode.FIT_TO_PAGE,Position.LEFT),0,225);
						break;	
				}
				mypdf.addImage(Legend,new Resize(Mode.NONE,Position.LEFT),0,dispObjPagePDF.height+diff,Legend.width*facteurLegende,Legend.height*facteurLegende);
				dispObjPagePDF = null;
			}
			sendPDF()
		}
		private function sendPDF():void
		{
			mypdf.save(Method.REMOTE,moduleCarloPilotageIHM.NonSecureUrlBackoffice+"/fr/consotel/consoview/M37/pdfDownload.cfm"	,Download.ATTACHMENT,"CarolExport.pdf")
		}
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED - EVENT COMPOSANTS - IBM
	//--------------------------------------------------------------------------------------------//

		protected function cubeCompleteHandler(e:Event):void
		{
			progressBarVisible(false)
			myPivotChart.maxNumCharts = int(actionsImpl.GRAPH_PER_PAGE);
		}
	//--------------------------------------------------------------------------------------------//
	//					REFRESH
	//--------------------------------------------------------------------------------------------//			
		public function refresh():void
		{
			resetPivotHandler()
			(myPivotChart.cube as IOLAPCube).dataProvider = null
			myPivotChart.cube.refresh()
		}		
				
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED - EVENT COMPOSANTS - FLEX
	//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fl:FlexEvent):void
		{
			cube1.addEventListener(CubeEvent.CUBE_COMPLETE,cubeCompletedHandler);
			cube1.addEventListener(CubeEvent.CUBE_PROGRESS,cubeProgressHandler);
			cube2.addEventListener(CubeEvent.CUBE_COMPLETE,cubeCompletedHandler);
			cube2.addEventListener(CubeEvent.CUBE_PROGRESS,cubeProgressHandler);
			cube3.addEventListener(CubeEvent.CUBE_COMPLETE,cubeCompletedHandler);
			cube3.addEventListener(CubeEvent.CUBE_PROGRESS,cubeProgressHandler);
			cube4.addEventListener(CubeEvent.CUBE_COMPLETE,cubeCompletedHandler);
			cube4.addEventListener(CubeEvent.CUBE_PROGRESS,cubeProgressHandler);
			cube5.addEventListener(CubeEvent.CUBE_COMPLETE,cubeCompletedHandler);
			cube5.addEventListener(CubeEvent.CUBE_PROGRESS,cubeProgressHandler);
			
			myPivotChart.addEventListener(ResultEvent.RESULT,myPivotChartInvokeHandler)
			
			addActionsToPivotChart()
		}
		protected function btnDecoClickHandler(e:Event):void
		{
			dispatchEvent(new CarolEvent(CarolEvent.GOTO_DICONNECT, true));
		}
		protected function cbxGraphTypeChangeHandler(ce:CarolEvent):void
		{
			
		}
		private function changePerimetreHandler(e:CarolEvent):void
		{
			afficherPerimetrage()
		}
		private function myPivotChartInvokeHandler(e:ResultEvent):void
		{
			if(!boolAnalyseLoading)
			{
				resetLibelleAnalyse()
			}
			boolAnalyseLoading = false
		}
//		private function myPivotChartInvokeHandler(e:InvokeEvent):void
//		{
//			resetLibelleAnalyse()
//		}
//		private function myPivotChartInvokeHandler(e:InvokeEvent):void
//		{
//			resetLibelleAnalyse()
//		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					METHODES
		//--------------------------------------------------------------------------------------------//
		
		public function afficherPerimetrage():void
		{
			closeAllPopups();
			_popUpPerimetre = new perimetreIHM()
			_popUpPerimetre.analyseVoSelected = this.analyseVoSelected
			PopUpManager.addPopUp(_popUpPerimetre,this,true)
			PopUpManager.centerPopUp(_popUpPerimetre);
			analyseVoSelected.PERIMETRE.cs.addEventListener(CarolEvent.REFRESH_DATASET_INTERNATIONAL, loadPerimetre);
			analyseVoSelected.PERIMETRE.cs.addEventListener(CarolEvent.FAULT_DATASET_INTERNATIONAL, faultPerimetre);
		}
		private function faultPerimetre(e:CarolEvent):void
		{
			ConsoviewAlert.afficherError("Une erreur est survenue","erreur");
		}
		private function loadPerimetre(e:CarolEvent):void
		{
			if(_popUpPerimetre.isPopUp)
			{
				analyseVoSelected.PERIMETRE = _popUpPerimetre.analyseVoSelected.PERIMETRE
				analyseVoSelected.setDefautAnalyse()
				PopUpManager.removePopUp(_popUpPerimetre)
			}

			PopUpManager.removePopUp(_popUpOpen)
			resetPivotHandler()
			progressBarVisible(true)
			
			for each(var obj:Object in analyseVoSelected.PERIMETRE.cs.arrListeNiveauOrga)
			{
				if(analyseVoSelected.PERIMETRE.niveau == obj.NIVEAU_CHOISI)
					analyseVoSelected.PERIMETRE.idx_niveau = obj.IDNIVEAU_CHOISI
				if(analyseVoSelected.PERIMETRE.niveau_aggreg == obj.NIVEAU_CHOISI)
					analyseVoSelected.PERIMETRE.idx_niveau_aggreg = obj.IDNIVEAU_CHOISI 
			}
			
			for(var i:int = analyseVoSelected.PERIMETRE.cs.arrListeNiveauOrga.length;i>0;i--)
			{
				if(analyseVoSelected.PERIMETRE.cs.arrListeNiveauOrga[i-1].IDNIVEAU_CHOISI > analyseVoSelected.PERIMETRE.idx_niveau_aggreg)
				{
					analyseVoSelected.PERIMETRE.cs.arrListeNiveauOrga.removeItemAt(i-1);
				}
			}
			
			switch(analyseVoSelected.PERIMETRE.idx_niveau_aggreg+ 1)
			{
				case 1:
					myPivotChart.cube = cube1;
					break;
				case 2:
					myPivotChart.cube = cube2;
					break;
				case 3:
					myPivotChart.cube = cube3;
					break;
				case 4:
					myPivotChart.cube = cube4;
					break;
				case 5:
					myPivotChart.cube = cube5;
					break;
			}
			(myPivotChart.cube as OLAPCube).dataProvider = analyseVoSelected.PERIMETRE.cs.arrDatasetCarol
			majPerimetre()
		}
		private function AfficherDefaultAnalyse():void
		{
			var x:XML = 
				<analyse>
				  <series>
				    <item>
				      <nom>TypedeTheme</nom>
				      <selecteditems/>
				    </item>
				  </series>
				  <attribute>
				    <item>
				      <nom>Segment</nom>
				      <selecteditems/>
				    </item>
				  </attribute>
				  <categorie>
				    <item>
				      <nom>Mois</nom>
				      <selecteditems/>
				    </item>
				  </categorie>
				</analyse>
			analyseVoSelected.getArrFromXml(x)
			boolChargerAnalyse = true
		}
		private function majPerimetre():void
		{
			ihm.perimAna.lblPerimetre.text 	= analyseVoSelected.PERIMETRE.libelle_orga
			if(analyseVoSelected.PERIMETRE.libelle_periodedeb.getMonth() == analyseVoSelected.PERIMETRE.libelle_periodefin.getMonth())
				ihm.perimAna.lblPeriode.text	= analyseVoSelected.PERIMETRE.displayDateDeb()
			else
				ihm.perimAna.lblPeriode.text	= analyseVoSelected.PERIMETRE.displayDateDeb()+' à '+analyseVoSelected.PERIMETRE.displayDateFin()
			if(analyseVoSelected.isDefaultAnalyse)
				AfficherDefaultAnalyse()
		}
		private function resetLibelleAnalyse():void
		{
			ihm.perimAna.lblNomAnalyse.text = "";
		}
		private function OuvrirAnalyse(e:CarolEvent = null, analyse:AnalyseVO = null):void
		{
			if(e != null)
			{
				resetPivotHandler();
				boolChargerDataset = _popUpOpen.boolChargerDataset
				boolChargerAnalyse = true
				if(boolChargerDataset)
				{
					analyseVoSelected.chargerNiveauOrga()
					analyseVoSelected.chargerPerimetreAnalyse();
				}
				else
				{
					boolAnalyseLoading = true
					analyseVoSelected.afficheAnalyse(myPivotChart,ihm.perimAna);
				}
			}
			else if(analyse != null)
			{
				resetPivotHandler();
				boolAnalyseLoading = true
				analyse.afficheAnalyse(myPivotChart,ihm.perimAna);
			}
			else
			{
				trace("Aucune analyse");
			}
		}
		private function addActionsToPivotChart():void
		{
			ihm = new actionsIHM();
			ihm.myPivotChart = this.myPivotChart
			var vb:VBox = myPivotChart.getChildByName("rightBox") as  VBox;
			var arr:Array = vb.getChildren()
			if(vb.getChildren().length < 4)
			{
				vb.setStyle("paddingRight",2);
				vb.setStyle("paddingLeft",2);
				vb.setStyle("paddingTop",2);
				vb.setStyle("paddingBottom",2);
				vb.addChildAt(ihm,2);
			}
		} 
		private function resetPivotHandler():void
		{
			try
			{
				myPivotChart.chartsAttributes	= [];
				myPivotChart.seriesAttributes	= [];
				myPivotChart.categoryAttributes	= [];
			}
			catch(e:Error)
			{
				boolErrorInvoked = true;
				while(myPivotChart.chartsAttributes.length > 0)
				{
					try
					{
						myPivotChart.chartsAttributes	= [];
					}
					catch(e:Error)
					{
					}
				}
				while(myPivotChart.seriesAttributes.length > 0)
				{
					try
					{
						myPivotChart.seriesAttributes	= [];
					}
					catch(e:Error)
					{
					}				
				}
				while(myPivotChart.categoryAttributes.length > 0)
				{
					try
					{
						myPivotChart.categoryAttributes	= [];
					}
					catch(e:Error)
					{
					}	
				}
			}
			boolReseted = false;
		}
		private function progressBarVisible(boolAction:Boolean,indeterminate:Boolean = false, str:String = ""):void
		{
			if(boolAction)
			{
				_popUpProgressBar 				= new progressBarIHM();
				_popUpProgressBar.indeterminate = indeterminate;
				if(str == "")
					_popUpProgressBar.strTitre =  ResourceManager.getInstance().getString('M37', 'Chargement_des_donn_es');
				else
					_popUpProgressBar.strTitre = str 
				PopUpManager.addPopUp(_popUpProgressBar, this, true);
				PopUpManager.centerPopUp(_popUpProgressBar);
			}
			else
			{
				PopUpManager.removePopUp(_popUpProgressBar);
			}
		}
		
		private function saveAnalyseVo():void
		{
			for each(var attr:PivotAttribute in myPivotChart.chartsAttributes)
			{
				var obj:ElementVO = new ElementVO();
				obj.LIBELLE = attr.displayName;
				obj.SELECTEDITEMS = []
				for(var i:int=1;i<attr.items.length;i++)
				{
					var strtmp:String = attr.items.getItemAt(i).name
					for(var ii:int=0;ii<attr.selectedItems.length;ii++)
					{
						if(attr.selectedItems[ii].name == strtmp)
						{
							obj.SELECTEDITEMS.push(strtmp);
							break;
						}
					}
				}
				analyseVoSelected.ARR_ATTRIBUTE.push(obj);
			}
			
			for each(var attr2:PivotAttribute in myPivotChart.categoryAttributes)
			{
				var obj1:ElementVO = new ElementVO();
				obj1.LIBELLE = attr2.displayName;
				obj1.SELECTEDITEMS = []
				for(var j:int;j<attr2.items.length;j++)
				{
					var strtmp1:String = attr2.items.getItemAt(j).name 
					for(var jj:int=0;jj<attr2.selectedItems.length;jj++)
					{
						if(attr2.selectedItems[jj].name == strtmp1)
						{
							obj1.SELECTEDITEMS.push(strtmp1);
							break;
						}
					}
				}
				analyseVoSelected.ARR_CAT.push(obj1);
			}
			
			for each(var attr3:PivotAttribute in myPivotChart.seriesAttributes)
			{
				var obj2:ElementVO = new ElementVO();
				obj2.LIBELLE = attr3.displayName;
				obj2.SELECTEDITEMS = []
				for(var k:int;k<attr3.items.length;k++)
				{
					var strtmp2:String = attr3.items.getItemAt(k).name 
					for(var kk:int=0;kk<attr3.selectedItems.length;kk++)
					{
						if(attr3.selectedItems[kk].name == strtmp2)
						{
							obj2.SELECTEDITEMS.push(strtmp2);
							break;
						}
					}
				}
				analyseVoSelected.ARR_SERIES.push(obj2)
			}
		}
		private function createAnalyseVo():AnalyseVO
		{
			var ana:AnalyseVO = new AnalyseVO()
			for each(var attr:PivotAttribute in myPivotChart.chartsAttributes)
			{
				var obj:ElementVO = new ElementVO();
				obj.LIBELLE = attr.displayName;
				obj.SELECTEDITEMS = []
				for(var i:int=1;i<attr.items.length;i++)
				{
					var strtmp:String = attr.items.getItemAt(i).name
					for(var ii:int=0;ii<attr.selectedItems.length;ii++)
					{
						if(attr.selectedItems[ii].name == strtmp)
						{
							obj.SELECTEDITEMS.push(strtmp);
							break;
						}
					}
				}
				ana.ARR_ATTRIBUTE.push(obj);
			}
			
			for each(var attr2:PivotAttribute in myPivotChart.categoryAttributes)
			{
				var obj1:ElementVO = new ElementVO();
				obj1.LIBELLE = attr2.displayName;
				obj1.SELECTEDITEMS = []
				for(var j:int;j<attr2.items.length;j++)
				{
					var strtmp1:String = attr2.items.getItemAt(j).name 
					for(var jj:int=0;jj<attr2.selectedItems.length;jj++)
					{
						if(attr2.selectedItems[jj].name == strtmp1)
						{
							obj1.SELECTEDITEMS.push(strtmp1);
							break;
						}
					}
				}
				ana.ARR_CAT.push(obj1);
			}
			
			for each(var attr3:PivotAttribute in myPivotChart.seriesAttributes)
			{
				var obj2:ElementVO = new ElementVO();
				obj2.LIBELLE = attr3.displayName;
				obj2.SELECTEDITEMS = []
				for(var k:int;k<attr3.items.length;k++)
				{
					var strtmp2:String = attr3.items.getItemAt(k).name 
					for(var kk:int=0;kk<attr3.selectedItems.length;kk++)
					{
						if(attr3.selectedItems[kk].name == strtmp2)
						{
							obj2.SELECTEDITEMS.push(strtmp2);
							break;
						}
					}
				}
				ana.ARR_SERIES.push(obj2)
			}
			return ana;
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE - EVENT CUBE
//--------------------------------------------------------------------------------------------//

		private function cubeCompletedHandler(e:CubeEvent):void
		{
			BOOLDATASETLOADED = true;
			progressBarVisible(false);
			if(boolChargerAnalyse)
			{
				callLater(callaterAfficheAnalyse)	
			}
		}
		private function callaterAfficheAnalyse():void
		{
			resetPivotHandler()
			boolAnalyseLoading = true
			analyseVoSelected.afficheAnalyse(myPivotChart,ihm.perimAna);
			ihm.perimAna.lblNomAnalyse.text = analyseVoSelected.LIBELLE
			boolChargerAnalyse = false
		}
		private function cubeProgressHandler(e:CubeEvent):void
		{
			if((myPivotChart.cube as OLAPCube).dataProvider != null)
			{
				_popUpProgressBar.setProgressBar(e);
			}
		}
		private function pageChangeHandler(ce:CarolEvent):void
		{
			if (ce.obj != null)
			{
				if (ce.obj.hasOwnProperty("PAGE_NUMBER"))
				{
					myPivotChart.maxNumCharts = ce.obj.PAGE_NUMBER;
				}
			}
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE - EVENT CAROL
//--------------------------------------------------------------------------------------------//
		
		private function openClickHandler(ce:CarolEvent):void
		{
			_popUpOpen = new openAnalyseIHM();
			_popUpOpen.analyseVoSelected = this.analyseVoSelected
			PopUpManager.addPopUp(_popUpOpen, this.parent, true);
			PopUpManager.centerPopUp(_popUpOpen);
			_popUpOpen.addEventListener(CarolEvent.OPEN_ANALYSE, 	OuvrirAnalyse)
		}
		private function resetClickHandler(ce:CarolEvent):void
		{
			_popUpReset = new resetAnalyseIHM();
			PopUpManager.addPopUp(_popUpReset, this, true);
			PopUpManager.centerPopUp(_popUpReset);
			_popUpReset.addEventListener(CarolEvent.POPUP__VALIDATE, resetCloseHandler);
		}
		private function saveAsClickHandler(ce:CarolEvent):void
		{
			if(BOOLDATASETLOADED)
			{
				saveAnalyseVo()
				_popUpSaveAs = new saveAsAnalyseIHM();
				_popUpSaveAs.analyseVoSelected = this.analyseVoSelected;
				PopUpManager.addPopUp(_popUpSaveAs, this, true);
				PopUpManager.centerPopUp(_popUpSaveAs);
				_popUpSaveAs.addEventListener(CarolEvent.SAVE_ANALYSE_OK, saveAsCloseHandler);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M37', 'Veuillez_p_rim_trer_puis_effectuer_une_a'),ResourceManager.getInstance().getString('M37', 'Erreur'))
			}
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE - CAROLRESULTEVENT POPUP
//--------------------------------------------------------------------------------------------//
		
		private function openCloseHandler(ce:CarolEvent):void
		{
		}
		private function saveAsCloseHandler(ce:CarolEvent):void
		{
			_popUpSaveAs.analyseVoSelected.sauvegarderAnalyse(myPivotChart)
		}
		private function exportCloseHandler(ce:CarolEvent):void
		{
		}
		private function resetCloseHandler(ce:CarolEvent):void
		{
			PopUpManager.removePopUp(_popUpReset)
			resetPivotHandler();
		}
		/* ---------------------------------------------------------------------------------
		ferme toutes les popups 
		--------------------------------------------------------------------------------- */
		private function closeAllPopups():void
		{
			var len:Number=systemManager.numChildren - 1;
			var i:Number=len;
			
			while (i >= 0)
			{
				if (systemManager.getChildAt(i).hasOwnProperty("isPopUp"))
				{
					PopUpManager.removePopUp(IFlexDisplayObject(systemManager.getChildAt(i)));
				}
				i--;
			}
		}
	}
}
