package gestionparc.entity
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class OrgaVO
	{
		public var idOrga:int=0;
		public var libelleOrga:String="";

		public var idCible:int=0;
		public var idSource:int=0; // corrrespond à l'idGpeClient

		public var position:int=0;
		public var chemin:String="null";

		public var idRegleOrga:int=0;

		public var dateModifPosition:Date=null;

		public function fillListOrgaVO(obj:Object):ArrayCollection
		{
			var myListOrgaVO:ArrayCollection=new ArrayCollection();
			var longueur:int=(obj as ArrayCollection).length;

			for (var i:int=0; i < longueur; i++)
			{
				var myOrga:OrgaVO=new OrgaVO();
				myOrga.libelleOrga=obj[i].LIBELLE_GROUPE_CLIENT;
				myOrga.idOrga=obj[i].IDGROUPE_CLIENT;

				myListOrgaVO.addItem(myOrga);
			}

			return myListOrgaVO;
		}
	}
}
