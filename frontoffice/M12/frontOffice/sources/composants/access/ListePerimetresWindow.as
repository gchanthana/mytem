package composants.access {
	import appli.events.ConsoViewEvent;
	
	import composants.access.perimetre.IPerimetreControl;
	import composants.access.perimetre.PerimetreEvent;
	import composants.access.perimetre.tree.SearchPerimetreWindowImpl;
	import composants.access.perimetre.tree.SimplePerimetreTree;
	import composants.rechercheglobale.RechObjectVO;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.controls.Tree;
	import mx.events.DropdownEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.perimetres.search.SpecialSearch;
	
	public class ListePerimetresWindow extends ListePerimetresWindowIHM implements IPerimetreControl {
		public static const CHANGE_GROUP_ACTION:int = 1;
		public static const CHANGE_PERIMETRE_ACTION:int = 2;
		
		private var perimetreBack : XML;
		private var _selectedNode : XML;  
		private var _boolNewPerimetreFromSearch : Boolean;
		
		
		/**
		 * Référence vers la classe responsable du chargement d'un noeud donné lorsqu'il n'est pas encore 
		 * en mémoire.
		 * */
		private var _search : SpecialSearch;
		
		
		//private var perimetreModel:IPerimetresModel;
		//private var perimetreTreeItemRenderer:IFactory;
		//private var actionPerformedCode:int;
		
		/*
		public function getPerimetreTreeDataDescriptor():ITreeDataDescriptor {
			//return perimetreModel.getPerimetreTreeDataDescriptor();
			return null;
		}
		*/
		
		public function getSelectedGroupIndex():int {
			return groupListItem.selectedIndex;
		}

		public function getSelectedNode():XML{			
			return _selectedNode;
		}
		
		public function ListePerimetresWindow() {
			super();
			trace("(ListePerimetresWindow) Instance Creation");
			//actionPerformedCode = -1;
			/*
			perimetreModel = new PerimetreModel();
			perimetreTreeItemRenderer = new ClassFactory(composants.access.perimetre.PerimetreTreeItemRenderer);
			*/
			
		}
		
		protected override function initIHM(event:FlexEvent):void {
			super.initIHM(event);
			trace("(ListePerimetresWindow) Performing IHM Initialization");
			mainView.selectedIndex = 0;
			this.setStyle("borderStyle","solid");
			this.width = ((cv.cvWidth * 35) / 45); // Coeff : 0.77
			this.height = cv.cvHeight / 2;
			
			perimetreTree.addEventListener(PerimetreEvent.PERIMETRE_NODE_CHANGED,handleChangeNodeEvent);
			searchTree.addEventListener(ConsoViewDataEvent.BACK_NODE_SEARCH,handleSearchControlEvents);
			
			nodeSearchItem.addEventListener(MouseEvent.CLICK,handlePerimetreControlEvents);
			searchInput.addEventListener(FlexEvent.ENTER,handlePerimetreControlEvents);
			
			cancelItem.addEventListener(MouseEvent.CLICK,cancelPerimetre);
			groupListItem.addEventListener(DropdownEvent.CLOSE,changeGroup);
			groupListItem.labelField = "LIBELLE_GROUPE_CLIENT";
			groupListItem.dataProvider = CvAccessManager.getSession().GROUP_LIST;
			groupListItem.selectedIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_IDX;
			searchInput.text = "";
			searchInput.enabled = nodeSearchItem.visible = true;
			
			_selectedNode = perimetreTree.getSelectedItem() as XML;
			/*ICI*/
			 if(event.type == FlexEvent.CREATION_COMPLETE) {
				/* perimetreTree.connectedNodeIsRoot = connectedNodeIsRoot;
				perimetreTree.loadDataOnSelection = loadDataOnSelection;
				perimetreTree.showGroupeRacine = showGroupeRacine; 
				
				cancelItem.addEventListener(MouseEvent.CLICK,cancelPerimetre);
				groupListItem.addEventListener(DropdownEvent.CLOSE,changeGroup);
				(perimetreTree as SimplePerimetreTree).addEventListener(MouseEvent.DOUBLE_CLICK,changePerimetre); */
				
				//--AJOUT SAMUEL pour les GroupeLigne SAV
				(perimetreTree as SimplePerimetreTree).addEventListener(Event.CHANGE,afficherBtSupprimerSAV);
				//--AJOUT SAMUEL pour la recherche --------------//
				searchTree.addEventListener(SearchPerimetreWindowImpl.NODE_DD_CLICKED,changePerimetreFromSearch);				
				//-- FIN AJOUT SAMUEL pour la recherche --------------//	
				
				
				
				/* searchInput.addEventListener(KeyboardEvent.KEY_DOWN,keyPerformSearch);
				nodeSearchItem.addEventListener(MouseEvent.CLICK,performSearch);
				
				perimetreUpdated(); */
			}
			/*fin ICI*/
		}
		
		public function updatePerimetre():void {
			trace("(ListePerimetresWindow) Updating Perimetre");
			if(perimetreTree != null)
				perimetreTree.onPerimetreChange(); // If Perimetre Data has been updated
		}

		private function changeGroup(event:DropdownEvent):void {
			var currentGroupIndex:int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_IDX;
			if(groupListItem.selectedIndex != currentGroupIndex) {
				searchInput.text = "";
				PopUpManager.removePopUp(this);
				selectGroup(groupListItem.selectedIndex);
			}
		}
		
		public function selectGroup(groupIndex:int):void {
			 
			//actionPerformedCode = ListePerimetresWindow.CHANGE_GROUP_ACTION;
			trace("(ListePerimetresWindow) Changing group - Selected Index is " + groupIndex);
			//perimetreModel.selectGroup(groupIndex);
			dispatchEvent(new ConsoViewEvent(ConsoViewEvent.GROUP_CHANGED));
		}
		
		private function handleChangeNodeEvent(event:PerimetreEvent):void {
			trace("(ListePerimetresWindow) Handling Change Node Event");
			_selectedNode = perimetreTree.getSelectedItem() as XML;
			PopUpManager.removePopUp(this);
			dispatchEvent(event);
			
			/*
			if((perimetreTree as SimplePerimetreTree).selectedItem.@NID !=
					CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX) {
				if((perimetreTree as SimplePerimetreTree).selectedItem.@STC > 0) {
					trace("ListePerimetreWindow changePerimetre() :\n" +
							"Noeud : " + (perimetreTree as SimplePerimetreTree).selectedItem.@LBL +
							"\nId : " + (perimetreTree as SimplePerimetreTree).selectedItem.@NID +
							"\nSTC : " + (perimetreTree as SimplePerimetreTree).selectedItem.@STC +
							"\nLast Périmètre : " + CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX.toString());
					actionPerformedCode = ListePerimetresWindow.CHANGE_PERIMETRE_ACTION;
					PopUpManager.removePopUp(this);
					dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_CHANGING_PERIMETRE));
					perimetreModel.addEventListener(PerimetreEvent.PERIMETRE_GROUP_CHANGED,onGroupChanged);
					perimetreModel.selectAccessNode((perimetreTree as SimplePerimetreTree).selectedItem.@NID);
				}
			}
			*/
		}
		
		private function handlePerimetreControlEvents(event:Event):void{
						 
			if(searchInput.text.length > 2) {
				
				var xmlAll : XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList;
				
				var idPerimtre : Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				
				
				if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE.toUpperCase() == "GROUPELIGNE"){
					var node : XMLList = xmlAll.descendants("NODE").(@NID == idPerimtre);
					perimetreBack = ObjectUtil.copy(node[0]) as XML;
				}else{
					perimetreBack = ObjectUtil.copy(xmlAll) as XML;
				} 				
				
				_selectedNode = perimetreTree.getSelectedItem() as XML;
				trace("(ListePerimetresWindow) Searching from node " + getSelectedNode().@LBL +
							" criteria : " + searchInput.text);
				searchTree.performSearch(parseInt(getSelectedNode().@NID,10),searchInput.text);
				mainView.selectedIndex = 1;
			} else {
				Alert.show("Merci de saisir au moins 3 caractères","Aide");
			}
			
			/*
			var paramObject:Object = new Object();
			if((perimetreTree as SimplePerimetreTree).selectedItem != null)
				paramObject.NID = (perimetreTree as SimplePerimetreTree).selectedItem.@NID;
			else
				paramObject.NID = 0;
			paramObject.KEY = searchInput.text;
			perimetreTree.dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.PERFORM_NODE_SEARCH,paramObject));
			*/
		}

		private function handleSearchControlEvents(event:Event):void {
			trace("(ListePerimetresWindow) Restore Perimetre Tree Control (From Search IHM)");
			 
			mainView.selectedIndex = 0;
			retourPerimetreInitiale();
			
		}

		private function cancelPerimetre(event:Event):void {
			PopUpManager.removePopUp(this);
		}
		
		public function logoff():void {
			//actionPerformedCode = -1;
			if(this.initialized == true)
				PopUpManager.removePopUp(this);
			//perimetreModel.clearPerimetreModel();
		}
		
/*=====================================================================================================================

		public function getNodeList():Object {
			return perimetreModel.getGroupNodes();
		}
		
		public function getSelectedNodeInfos():Object {
			return perimetreModel.getSelectedNodeInfos();
		}
		
		public function getSelectedGroupIndex():int {
			return perimetreModel.getSelectedGroupIndex();
		}
		
		public function perimetreUpdated():void {
			//if(this.initialized) {
				groupListItem.labelField = "LIBELLE_GROUPE_CLIENT";
				groupListItem.dataProvider = perimetreModel.groupList;
				groupListItem.selectedIndex = perimetreModel.getSelectedGroupIndex();
				searchInput.text = "";
			//}
			if(actionPerformedCode == ListePerimetresWindow.CHANGE_GROUP_ACTION)
				perimetreTree.onPerimetreChange();
		}

		private function onPerimetreInitialized(event:PerimetreEvent):void {
			perimetreModel.removeEventListener(PerimetreEvent.PERIMETRE_MODEL_INITIALIZED,onPerimetreInitialized);
			trace("ListePerimetresWindow onPerimetreInitialized() : Liste Groupes Racines Initialisée");
			dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_MODEL_INITIALIZED));
		}


		ICI*/
		
		
		///---- AJOUT SAMUEL ------- pour les Groupe de Lignes ////
		//Handler du click sur un noeud de l'arbre
		//(Affiche le bouton Supprimer la recherche lorsqu'on click sur un Noeud de type SAV
		//@param Event
		private function afficherBtSupprimerSAV(evt : Event):void{
			var tree : Tree = Tree(evt.currentTarget);
			if( tree != null){				
				var nid : Number =Number((tree.selectedItem as XML).@NID);
				if ((tree.selectedItem as XML).parent() != null){
					if ((Number((tree.selectedItem as XML).parent().@NID) == -4) 
						&& (nid > 0)){
						btSupGPSAV.visible = true;	
						btSupGPSAV.addEventListener(MouseEvent.CLICK,btSupGPSAVClickHandler);					
					}else{
						btSupGPSAV.visible = false;
					}
				}else{
					btSupGPSAV.visible = false;
				}				
			}else{
				btSupGPSAV.visible = false;
			}
		}
		
		private function checkDeleteNode(nid : Number):Boolean{
			var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;	
			var parent : XML = (xmlPerimetreData.NODE[2].NODE.(@NID == nid)[0] as XML).parent();
			var parentID : int = parseInt(parent.@NID,10);
			if (CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX == parentID) return false;
			else return true;
			
		}
		
		//Handler du click sur le bouton supprimer le noeud 
		//param in MosueEvent		
		private function btSupGPSAVClickHandler(me : MouseEvent):void{
			var nid : Number = perimetreTree.getSelectedItem().@NID;
			var parent : XML = (perimetreTree.getSelectedItem() as XML).parent();
			var child : XML = ((perimetreTree.getSelectedItem() as XML).children()[0] != null)?
						 (perimetreTree.getSelectedItem() as XML).children()[0] : <NODE NID="0"/> ;
			
				
			if ((CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX != nid) &&
			(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX != child.@NID))
			{	
				var rech : RechObjectVO = new RechObjectVO();	
				rech.ID = nid;		    
				
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																						"fr.consotel.consoview.rechercheglobale.RLigneGateWay",
																						"deleteRech",supprimerSavResult);
				RemoteObjectUtil.callService(op,rech); 
				
			}else{
				Alert.show("Impossible de supprimer un noeud sur lequel vous êtes");
				
			}
			 
		}
		
		private function supprimerSavResult(re : ResultEvent):void{
			if (re.result > 0){
				var nid : Number = perimetreTree.getSelectedItem().@NID;	
				var parent : XML = (perimetreTree.getSelectedItem() as XML).parent();
						
				var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;	
				var node : Object = xmlPerimetreData.NODE[2].NODE.(@NID == nid);			
				
				(perimetreTree as Tree).expandItem(parent,false);
				var col : XMLListCollection = new XMLListCollection(parent.children());
				if (col.length == 1) parent.@NTY = 0;
				col.removeItemAt(col.getItemIndex(xmlPerimetreData.NODE[2].NODE.(@NID == nid)[0]));	
				(perimetreTree as Tree).selectedIndex = -1;				
				(perimetreTree as Tree).expandItem(parent,true);
				btSupGPSAV.visible = false;
				Alert.show("Sauvegarde éffacée");	
			}else{
				Alert.show("Echec lors de la suppression");
			}
			
				
					
		}		
		///---- FIN AJOUT -----------------------------------////
		
		
		///---- AJOUT SAMUEL ------- pour la recherche///
		
		//retour au perimetre avant la recherche		
		private function retourPerimetreInitiale():void{
			if(_boolNewPerimetreFromSearch){
				_selectedNode = ObjectUtil.copy(perimetreBack) as XML;
				perimetreBack = null;
				_boolNewPerimetreFromSearch = false;
				dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_NODE_CHANGED));	
			}
			PopUpManager.removePopUp(this);
		}
		
	
		//change le perimetre depuis la recherche
		private function changePerimetreFromSearch(event:Event):void {
			
			_selectedNode = SearchPerimetreWindowImpl(searchTree).searchTree.selectedItem as XML;
			if (_selectedNode != null){
				_boolNewPerimetreFromSearch = true;				
				Tree(perimetreTree).selectedItem = _selectedNode;
				
				
				var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;			  
				var resultList:XMLList = xmlPerimetreData.descendants("NODE").(@NID == _selectedNode.@NID);
				
				if(resultList.length() == 0){//->le noeud n'est pas encore en mémoire
					deroulerLesNoeuds(Number(_selectedNode.@NID));	
				}else{
					PopUpManager.removePopUp(this);
					dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_NODE_CHANGED));
				}
					
			}else{
				_boolNewPerimetreFromSearch = false;
				Alert.show("Vous devez sélectionner un noeud","Erreur");
			}
		}
		
		private function deroulerLesNoeuds(idNode : Number):void{
			//aller chercher l'orga du noeud		
			
			//le noeud n'est pas encore en mémoire alors on le charge
			_search = new SpecialSearch(this);
			_search.initSearch(perimetreTree as GenericPerimetreTree,idNode);			
			PopUpManager.removePopUp(this);
			dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_NODE_CHANGED));
		}
		
		//-------FIN AJOUT SAMUEL --------------//
		/*
		public function getGroupList():Object {
			return perimetreModel.groupList;
		}
		
		private function onGroupChanged(event:PerimetreEvent):void {
			perimetreModel.removeEventListener(PerimetreEvent.PERIMETRE_GROUP_CHANGED,onGroupChanged);
			dispatchEvent(new PerimetreEvent(PerimetreEvent.PERIMETRE_GROUP_CHANGED));
		}
		
		public function getSelectedGroup():Object {
			return perimetreModel.getGroupByIndex(perimetreModel.getSelectedGroupIndex());
		}
		
		private function keyPerformSearch(event:Event):void {
			if((event as KeyboardEvent).keyCode == 13) {
				trace("ListePerimetreWindow keyPerformSearch()");
				var paramObject:Object = new Object();
				if((perimetreTree as SimplePerimetreTree).selectedItem != null)
					paramObject.NID = (perimetreTree as SimplePerimetreTree).selectedItem.@NID;
				else
					paramObject.NID = 0;
				paramObject.KEY = searchInput.text;
				perimetreTree.dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.PERFORM_NODE_SEARCH,paramObject));
			}
		}
		*/
	}
}
