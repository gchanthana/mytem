package entity.vo
{
    import mx.collections.ArrayCollection;

    [Bindable]
    public class OperateurVO extends SimpleEntityVO
    {
        private var _libelleRocf:String;

        public function OperateurVO()
        {
            super();
        }

        public override function deserialize(object:Object):IValueObject
        {
            this.value = object.OPERATEURID;
            this.label = object.NOM;
            this.libelleRocf = object.LIBELLE_ROCF;
            return this;
        }

        public override function serialize(object:IValueObject):Object
        {
            return null;
        }

        public override function toString():String
        {
            return "OperateurVO " + label + "\n" + "value :" + value + " \n";
        }

        public override function toXML():XML
        {
            var xml:XML = voToXml(this, "OperateurVO");
            return xml;
        }

        public override function decode(object:Object):IValueObject
        {
            var vo:OperateurVO = new OperateurVO();
            vo.label = object.CollecteTypeVO.label;
            vo.value = object.CollecteTypeVO.value as Number;
            vo.libelleRocf = object.CollecteTypeVO.libelleRocf;
            return vo;
        }

        public function get libelleRocf():String
        {
            return _libelleRocf;
        }

        public function set libelleRocf(value:String):void
        {
            _libelleRocf = value;
        }
    }
}