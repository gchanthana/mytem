package rapportPilotage.service.monoperiodeselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;


	public class MonoPeriodeSelectorService
	{
		
		private var _model:MonoPeriodeSelectorModel;
		public var handler:MonoPeriodeSelectorHandler;
		
		public function MonoPeriodeSelectorService()
		{
			this._model = new MonoPeriodeSelectorModel();
			this.handler = new MonoPeriodeSelectorHandler(model);
		}		
		public function get model():MonoPeriodeSelectorModel
		{
			return this._model;
		}
		
		public function getValuesMonoPeriode():void
		{				
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M332.service.selector.MonoPeriodeSelectorService","getValuesMonoPeriode",handler.getValuesMonoPeriodeHandler); 
			
			RemoteObjectUtil.callService(op);// appel au service coldfusion qui contient des fonctions pour les opérations de service
		}
	}
}