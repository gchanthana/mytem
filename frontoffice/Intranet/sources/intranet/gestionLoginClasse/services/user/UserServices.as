package intranet.gestionLoginClasse.services.user
{
    import composants.util.ConsoviewAlert;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;

    public class UserServices
    {
        public var myDatas:UserDatas;
        public var myHandlers:UserHandlers;

        public function UserServices()
        {
            myDatas = new UserDatas();
            myHandlers = new UserHandlers(myDatas);
        }

        /**
         * Récupère les caractèristiques d'accès associés à un utilisateur
         * @param appLoginId : l'id de l'utilisateur
         */
        public function getUserAccess(appLoginId:String, groupeMaitreId:String):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "getAccess_v2", myHandlers.getUserAccessResultHandler, null);
            RemoteObjectUtil.callService(op1, appLoginId, groupeMaitreId);
        }

        /**
         * Affecte les accès et les styles d'un utilisateur à un autre
         * @param userRef_id: l'id de l'utilisateur de référence
         * @param userToAffectId :  l'id de l'utilisateur à affecter
         * @param id_racine : l'id du groupe racine
         */
        public function duplicateUserProfile(userRef_id:Number, userToAffectId:Number, id_racine:Number):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "duplicateAccessR", myHandlers.duplicateUserProfileResultHandler, null);
            RemoteObjectUtil.callService(op1, userRef_id, userToAffectId, id_racine);
        }

        /**
         *
         * Affecte tous accès  d'un utilisateur à un autre ( sur otutes les racines)
         * @param userRef_id: l'id de l'utilisateur de référence
         * @param userToAffectId :  l'id de l'utilisateur à affecter
         * @param id_racine : l'id du groupe racine
         */
        public function duplicateAllUserProfile(userRef_id:Number, userToAffectId:Number):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "duplicateAllAccessR", myHandlers.duplicateUserProfileResultHandler, null);
            RemoteObjectUtil.callService(op1, userRef_id, userToAffectId, Number(Intranet.mySession.id));
        }
    }
}