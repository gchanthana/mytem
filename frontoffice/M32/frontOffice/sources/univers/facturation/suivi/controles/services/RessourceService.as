//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 20, 2011								*
//	owner  : Consotel								*
//***************************************************
package univers.facturation.suivi.controles.services
{
	import composants.util.ConsoviewUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	import univers.facturation.grilletarif.renderer.TarifCellEditor;
	import univers.facturation.suivi.controles.vo.ProduitVO;
	import univers.facturation.suivi.controles.vo.Ressource;
	import univers.facturation.suivi.controles.vo.StatutVO;

	public class RessourceService
	{	
	
		
		[Bindable]
		public var model:RessourceModel;
		private var _handler:RessourceHandler;		
		
		//RemoteServiceName
		private const REMOTE_SERVICE:String = "fr.consotel.consoview.M32.RessourceService";
		
		//operations
		private var updateStatusAOp:AbstractOperation;
		private var updateInventoryStateAOp:AbstractOperation;
		private var mergeProductAOp:AbstractOperation;
		
		public function RessourceService()
		{
			model = new RessourceModel();
			_handler = new RessourceHandler(model);
		}
		
		//remoting
		
		/**
		 * Mettre à jour le statut d'un enssemble de ressource
		 * 0=cloturé, 1= non traité, 2= en cours de reclamation
		 * 
		 * @param tRessource, le tableau de ressource
		 * @param targetStatus, le statut cible 
		 * 
		 */		
		public function updateStatus(tRessource:Array,targetStatus:StatutVO):void
		{	
			var _tIpr:Array = extractIdRessource(tRessource);
			
			updateStatusAOp = RemoteObjectUtil.getOperation(REMOTE_SERVICE,"updateStatus",_handler.updateStatusResultHandler);
			RemoteObjectUtil.callService(updateStatusAOp,_tIpr,targetStatus.ID);
		}
		
		
		/**
		 * Mettre à jour l'état inventaire d'un enssemble de ressource
		 * 0=hors inventaire, 1= dans l'inventaire
		 * 
		 * @param tRessource, le tableau de ressource.
		 * @param targetState, l'état cible.
		 * @param dDateDeb, la date de début d'effet.
		 * @param commentaire, un commentaire pour l action.
		 * @param dDateFin, la date de fin d'éffet.(pas encore utilisé)		 
		 **/
		public function updateInventoryState(tRessource:Array,targetState:Number,dDateDeb:Date,commentaire:String = null,dDateFin:Date=null):void
		{	
			var _tIpr:Array = extractIdRessource(tRessource);
			
			updateInventoryStateAOp = RemoteObjectUtil.getOperation(REMOTE_SERVICE,"updateInventoryState",_handler.updateInventoryStateResultHandler);
			
			if(dDateFin)
			{
				RemoteObjectUtil.callService(updateInventoryStateAOp,_tIpr,targetState,dDateDeb,dDateFin,commentaire);	
			}
			else
			{
				RemoteObjectUtil.callService(updateInventoryStateAOp,_tIpr,targetState,dDateDeb,commentaire);	
			}
		}
		
		
		//private methods
		private function extractIdRessource(tRessource:Array):Array
		{
			return ConsoviewUtil.extractIDs("idProduit",tRessource);
		}
		
		public function mergeProduct(ressourceFacturee:ProduitVO,ressourceNonFacturee:ProduitVO):void
		{
			if(ressourceFacturee.idProduit > 0 && ressourceNonFacturee.idProduit > 0)
			{
				mergeProductAOp = RemoteObjectUtil.getOperation(REMOTE_SERVICE,"mergeProduct",_handler.mergeProductResultHandler);
				RemoteObjectUtil.callService(mergeProductAOp,ressourceNonFacturee.idProduit,ressourceFacturee.idProduit);
			}
		}
	}
}