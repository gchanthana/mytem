//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 21, 2011								*
//	owner  : Consotel								*
//***************************************************
package univers.facturation.suivi.controles.vues
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import univers.facturation.suivi.controles.event.RessourceEvent;
	import univers.facturation.suivi.controles.services.RessourceService;
	import univers.facturation.suivi.controles.vo.ProduitVO;
	
	
	[Bindable]
	public class EntrerProduitDansInventaireImpl extends TitleWindow
	{
		//const
		private static const IN_INVENTORY:Number = 1;
		
		//Services
		private var ressourceService:RessourceService;
		
		
		//Properties
		private var _dataProvider:ArrayCollection;
		private var _dataProviderChanged:Boolean = false;
		
		private var _selecetedDate:Date;
		private var _selecetedDateChanged:Boolean = false;
		
		private var _nbProduitlibelle:String;
		
		public function EntrerProduitDansInventaireImpl()
		{
			super();			
			
			selecetedDate = new Date();
			ressourceService = new RessourceService();			
		}
		
		
		//frameWork
		override protected function commitProperties():void
		{
			if(_dataProviderChanged)
			{
				formatLabelNbProduitSelected();
			}
		}
		
		public function set dataProvider(values:ArrayCollection):void
		{
			if(values && values != _dataProvider)
			{
				_dataProvider = values;
				_dataProviderChanged = true;
				invalidateProperties();
				invalidateDisplayList();
			}
			else
			{
				_dataProviderChanged = false;
			}
			
		}

		public function get dataProvider():ArrayCollection
		{
			return _dataProvider;
		}

		public function set selecetedDate(value:Date):void
		{
			if(value && value != _selecetedDate)
			{
				_selecetedDate = value;
				_selecetedDateChanged = true;
				invalidateProperties();
				invalidateDisplayList();
			}
			else
			{
				_selecetedDateChanged = false;
			}
		}

		public function get selecetedDate():Date
		{
			return _selecetedDate;
		}
		
		
		public function set nbProduitlibelle(value:String):void
		{
			_nbProduitlibelle = value;
		}
		
		public function get nbProduitlibelle():String
		{
			return _nbProduitlibelle;
		}
		
		public function reset():void
		{
			nbProduitlibelle = "";
			if(dataProvider)dataProvider.removeAll();
			selecetedDate = new Date();
			
		}
			
		//protected functions ------------------------------------------------------------------------------------
		
		//Listners =======================
		protected function dcDateSortieChangeHandler(event:CalendarLayoutChangeEvent):void
		{
			selecetedDate = event.newDate;
		}
		
		protected function btValiderClickHandler(event:MouseEvent):void
		{
			var message:String = formaterMessageConfirmation(dataProvider,selecetedDate);
			if(dataProvider && dataProvider.length > 0)
			{
				ConsoviewAlert.afficherAlertConfirmation(message,"Confirmation",mettreRessourceHorsInventaire);	
			}
		}
		
		protected function btAnnulerClickHandler(event:MouseEvent):void
		{
			closeMe()
		}
		
		protected function closeHandler():void
		{
			closeMe();
		}
		
		protected function _modelUpdateInventoryStateEventHandler(event:RessourceEvent):void
		{
			ressourceService.model.removeEventListener(RessourceEvent.UPDATE_INVENTORY_STATE_EVENT, _modelUpdateInventoryStateEventHandler);

			if(ressourceService.model.lastNbRessInvStateUpDated > 0)
			{
//				updateDataprovider(dataProvider);
				
				dispatchEvent(new Event('UPDATE_DATAPROVIDER', true));
				
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M32','Les_produits_ont__t__entr_s_dans_l_inventaire'),this);
				
				dispatchEvent(new RessourceEvent(RessourceEvent.UPD_INV_FACTURE_SUCCES, true));
				closeMe();
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M32','Les_produits_n_ont_pu__tre_entr__dans_l_inventaire'),"Attention");
			}
		}
		
		//private functions -------------------------------------------------------------------------------------------------------------		
		private function closeMe():void
		{
			PopUpManager.removePopUp(this);	
		}
		
		
		//format le libellé du nombre de produits
		private function formatLabelNbProduitSelected():void
		{
			nbProduitlibelle = dataProvider.length + " produit(s)";
		}		
		
		//appel du service pour mettre les ressources hors inventaire
		private function mettreRessourceHorsInventaire(event:CloseEvent):void
		{
			if(!dataProvider) return;
			if(event.detail == Alert.OK)
			{
				ressourceService.model.addEventListener(RessourceEvent.UPDATE_INVENTORY_STATE_EVENT, _modelUpdateInventoryStateEventHandler);			
				ressourceService.updateInventoryState(dataProvider.source,IN_INVENTORY,selecetedDate);	
			}
		}
		
		//formatage du message de confirmation
		private function formaterMessageConfirmation(ressources:ArrayCollection,date:Date):String
		{
			var message:String = ResourceManager.getInstance().getString('M32','Etes_vous_sur_de_vouloir_passer_dans_l_inventaire')+" \n";
			var taille:int = ressources.length;
			var i:int = 0;
			if(taille > 10)
			{
				for (i=0; i<10; i ++)
				{
					message = message + "- " + (ressources[i] as ProduitVO).libelleProduit+ "\n";
				}
				message = message + '- ...\n';
			}
			else
			{
				for (i=0; i<ressources.length; i ++)
				{
					message = message + "- " + (ressources[i] as ProduitVO).libelleProduit+ "\n";
				}
			}
			
			return message;
		}
		
		
		//Mis à jour du dataprovider pour ne pas avoir a relancer la req to refresh the data
		private function updateDataprovider(values:ArrayCollection):void
		{
			var cursor:IViewCursor = values.createCursor();
			while(!cursor.afterLast)
			{
				(cursor.current as ProduitVO ).dateSortie = selecetedDate;
				cursor.moveNext();	
			}
		}
		
	}
}