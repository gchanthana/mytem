package gestionparc.ihm.fiches.equipement
{
	import flash.events.MouseEvent;

	import gestionparc.ihm.fiches.listedispo.DispoLigne;
	import gestionparc.services.associer.ActionAssocierServices;

	import mx.containers.TitleWindow;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;

	public class FicheAssocierLigneToEqpImpl extends TitleWindow
	{
		[Bindable]
		public var liste:DispoLigne;
		public var labDgSelection:Label;

		private var serv:ActionAssocierServices=new ActionAssocierServices();

		public function FicheAssocierLigneToEqpImpl()
		{
			super();
			liste.addEventListener(MouseEvent.CLICK, listeClickHandler);
		}

		protected function btnValiderClickHandler():void
		{
		}

		protected function listeClickHandler(e:MouseEvent):void
		{
			if (liste.getIDSelection() > -1)
			{
				labDgSelection.text=liste.getLibelleSelection();
			}
		}

		protected function close(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		/* APPEL BACKOFFICE */
		private function associerLigneToEqp():void
		{
			var SendObj:Object=new Object();
//			serv.associerLigneToEqp(SendObj);
		}
	}
}
