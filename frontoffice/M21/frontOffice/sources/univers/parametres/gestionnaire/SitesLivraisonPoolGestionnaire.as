package univers.parametres.gestionnaire
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestionnaire.classemetier.PoolGestionnaire;
	import univers.parametres.gestionnaire.classemetier.Site;
		
	
	public class SitesLivraisonPoolGestionnaire extends SitesLivraisonPoolGestionnaireIHM
	{
		private var poolGestionnaire : PoolGestionnaire;
		public function SitesLivraisonPoolGestionnaire(poolGestionnaire : PoolGestionnaire)
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer_handler);
			this.poolGestionnaire = poolGestionnaire;
		}
		private function initIHM(evt : FlexEvent):void
		{			
			label_libelle_pool.text = poolGestionnaire.libelle_pool;
			bt_affecter_site.addEventListener(MouseEvent.CLICK,affecter_site_handler);
			bt_desaffecter_site.addEventListener(MouseEvent.CLICK,deaffecter_site_handler);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer_handler);
			bt_fiche_site.addEventListener(MouseEvent.CLICK,fiche_site_handler);
			text_input_filtre.addEventListener(Event.CHANGE,filtreSite);
			initData();
		}
		private function filtreSite(evt : Event):void
		{
			poolGestionnaire.col_site.filterFunction=filtreGridPoolGestionnaire;
			poolGestionnaire.col_site.refresh();
		}
		public function filtreGridPoolGestionnaire(item:Object):Boolean {
	
			if (((item.libelle_site as String).toLowerCase()).search(text_input_filtre.text.toLowerCase())!=-1 ||((item.ref_site as String).toLowerCase()).search(text_input_filtre.text.toLowerCase())!=-1 ||((item.code_interne_site as String).toLowerCase()).search(text_input_filtre.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		private function initData():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getSite_pool",
																		process_init_dg_site_pool,erreur);
			RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,text_input_rechercher.text);
		}
		private function process_init_dg_site_pool(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			for(i=0; i < tmpArr.length;i++){	
								
				var  item : Site = new Site();
				item.id_site=tmpArr[i].IDPool;
				item.adr_site=tmpArr[i].adr_site;
				item.code_interne_site=tmpArr[i].code_interne_site;
				item.commentaire_site=tmpArr[i].commentaire_site;
				item.commune_site=tmpArr[i].commune_site;
				item.cp_site=tmpArr[i].cp_site;
				item.libelle_site=tmpArr[i].libelle_site;
				item.ref_site = tmpArr[i].ref_site;
				item.IS_FACTURATION = convertIntToBool(tmpArr[i].ISLIVRAISON);
				item.IS_LIVRAISON 	= convertIntToBool(tmpArr[i].ISFACTURAITON);
				
				poolGestionnaire.col_site.addItem(item);
			}
			dg_site_of_pool.dataProvider = poolGestionnaire.col_site;
		}
		
		private function convertIntToBool(value:int):Boolean
		{
			var bool:Boolean = false;
			
			if(value == 0)
				bool = false;
			if(value == 1)
				bool = true;
				
			return bool;
		}
		
		private function erreur(evt : FaultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Erreur'));
		}
		private function affecter_site_handler(evt : MouseEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'affecter_site_handler'));
		}
		private function deaffecter_site_handler(evt : MouseEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'deaffecter_site_handler'));	
		}
		private function fermer_handler(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		private function fiche_site_handler(evt : MouseEvent):void
		{
			if(dg_site_of_pool.selectedIndex != -1)
			{
				var panel_fiche : FicheSite = new FicheSite(dg_site_of_pool.selectedItem as Site);
				PopUpManager.addPopUp(panel_fiche,this,true);
				PopUpManager.centerPopUp(panel_fiche);
				panel_fiche.addEventListener(Event.REMOVED,ficheSite_close);
			}
		}
		private function ficheSite_close(evt : Event):void
		{
			//permet de de recharger la collection et d'actualiser les infos.
			dg_site_of_pool.dataProvider = poolGestionnaire.col_site
		}
	}
}