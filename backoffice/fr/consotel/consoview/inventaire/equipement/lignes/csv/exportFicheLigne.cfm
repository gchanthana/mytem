<cfset etat="/consoview/gestion/lignesetservices/ficheLigne/ficheLigne.xdo">
<cfset template="ficheWord">
<cfset format="pdf">
<cfset locale="fr-FR">
<cfif StructKeyExists(session,"perimetre")>
	<!--- <cfdump var="#form#"><cfdump var="#session#"><cfabort> --->
	<cfset o=CreateObject("component","fr.consotel.consoview.util.PublicReportService")>
	<cfset d=o.createReportRequest(etat,template,format,locale)>
	<cfset d=o.AddParameter("P_IDRACINE",session["perimetre"]["id_groupe"])>
	<cfset d=o.AddParameter("P_IDSOUS_TETE",form.idsous_tete)>
	<cfset contenu=o.runReport()>
	
	<cfheader name="Content-Disposition" value="inline;filename=FicheLigne_#LsDateFormat(Now(),'yyyymmdd')#.pdf">
	<cfcontent type="#contenu.getReportContentType()#" variable="#contenu.getReportBytes()#">
<cfelse>
Session expirée.
</cfif>