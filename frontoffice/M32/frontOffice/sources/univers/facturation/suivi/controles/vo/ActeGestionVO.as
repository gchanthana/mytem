package univers.facturation.suivi.controles.vo
{
	public class ActeGestionVO
	{
		
		public var typeOperation:String = "";
		public var libelle:String = "";
		public var refClient:Number = 0;
		public var distributeur:String = "";
		public var commande:Number = 0;
		public var dateCommande:Date = null;
		public var etat:String = "";
		public var montant:Number = 0;
		
		public function ActeGestionVO()
		{
		}
		
		public function addActeGestion(value:Object):void
		{
			this.typeOperation = value.TYPE_OPERATION;
			this.libelle = value.LIBELLE;
			this.refClient = value.REF_CLIENT;
			this.distributeur = value.OPNOM ;
			this.commande = value.IDCOMMANDE;
			this.dateCommande = value.DATE_OPERATION;
			this.etat = value.LIBELLE_ETAT;
			this.montant = value.MONTANT;
		}
		
	}
}