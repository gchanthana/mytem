package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.FileUpload;
	import gestioncommande.entity.PieceJointeVO;
	import gestioncommande.events.PiecesJointesEvent;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class PiecesJointesService extends EventDispatcher
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		private static var _path				:String = "fr.consotel.consoview.M16.v2.AttachementService";
		
		//--------------------------------------------------------------------------------------------//
		//					GLOBALES
		//--------------------------------------------------------------------------------------------//
		
		public var lastFilesAttached			:ArrayCollection;
		
		public var UUID							:String = "";
		
		//--------------------------------------------------------------------------------------------//
		//					LIBELLE A TRADUIRE - LUPO
		//--------------------------------------------------------------------------------------------//
		
		private var text_Error_createUUID		:String = ResourceManager.getInstance().getString('M16', 'Impossible_de_cr_er_un_dossier');
		private var text_Error_attachFile		:String = ResourceManager.getInstance().getString('M16', 'Impossible_d_uploader_vos_fichiers');
		private var text_Error_majAttachJoin	:String = ResourceManager.getInstance().getString('M16', 'Impossible_de_mettre___jour_le_fichier');
		private var text_Error_editFile			:String = ResourceManager.getInstance().getString('M16', 'Impossible_de_remomer_le_fichier');
		private var text_Error_eraseFile		:String = ResourceManager.getInstance().getString('M16', 'Impossible_de_supprimer_le_fichier');
		private var text_Error_dowloadFile		:String = ResourceManager.getInstance().getString('M16', 'Impossible_de_t_l_charger_le_fichier');
		
	//--------------------------------------------------------------------------------------------//
	//					CONSRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PiecesJointesService()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function createUUID(): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"initUUID",
																				createUUIDResultHandler);		
			RemoteObjectUtil.callService(op);	
		}
		
		public function createUUIDSilent(): void
		{
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation(_path,
																			"initUUID",
																			createUUIDResultHandler);		
			RemoteObjectUtil.callSilentService(op);	
		}
		
		public function fournirAttachements(idcommande:int):void
		{
			lastFilesAttached = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"fournirAttachements",
																				fournirAttachementsResultHandler);		
			RemoteObjectUtil.callService(op,idcommande);
		}
		
		public function fournirAttachementsSilent(idcommande:int):void
		{
			lastFilesAttached = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation(_path,
																			"fournirAttachements",
																			fournirAttachementsResultHandler);		
			RemoteObjectUtil.callSilentService(op,idcommande);
		}
		
		public function majAttachementJoin(idcommande:int, idfile:int, bool:int):void
		{			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"majAttachementJoin",
																				majAttachementJoinResultHandler);		
			RemoteObjectUtil.callService(op,idcommande,
											idfile,
											bool);
		}
		
		public function attachFile(idcommande:int, value:Array, uuid:String = ""):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"queryAppendAttachements",
																				attachFileResultHandler);		
			RemoteObjectUtil.callService(op,idcommande,
											value,
											uuid);
		}
		
		public function renameFile(newNameFile:String, doc:FileUpload):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"editAttachement",
																				renameFileResultHandler);		
			RemoteObjectUtil.callService(op,doc.fileName,
											doc.fileName,
											newNameFile,
											doc.fileId);
		}
		
		public function eraseFile(doc:FileUpload):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_path,
																				"eraseAttachement",
																				eraseFileResultHandler);		
			RemoteObjectUtil.callService(op,doc.fileUUID,
											doc.fileName,
											doc.fileId);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					RESULTEVENT PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function createUUIDResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				UUID = re.result as String;
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_UUID_CREATED));
			}
			else
				popup(text_Error_createUUID);
		}
		
		private function fournirAttachementsResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				lastFilesAttached = mappingAttachement(re.result as ArrayCollection);
				
				if(lastFilesAttached == null)
					lastFilesAttached = new ArrayCollection();
				
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.LAST_FILES_ATTACHED));
			}
			else
				popup(text_Error_createUUID);
		}
		
		private function majAttachementJoinResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_ATTACH_JOIN));
			}
			else
				popup(text_Error_majAttachJoin);
		}
		
		private function attachFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_ATTACHED));
			else
				popup(text_Error_attachFile);
		}
		
		private function renameFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_RENAMED));
			else
				popup(text_Error_editFile);
		}
		
		private function eraseFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_ERASED));
			else
				popup(text_Error_eraseFile);
		}
		
		private function downloadFileResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
				dispatchEvent(new PiecesJointesEvent(PiecesJointesEvent.FILE_DOWNLOADED));
			else
				popup(text_Error_dowloadFile);
		}
	
		//--------------------------------------------------------------------------------------------//
		//					MAPPING
		//--------------------------------------------------------------------------------------------//
		
		private function mappingAttachement(values:ArrayCollection):ArrayCollection
		{
			var attach	:ArrayCollection;
			var pjvo	:FileUpload;
			
			if(values == null) return attach;
			
			var len		:int = values.length;
			
			attach = new ArrayCollection();
			
			for(var i:int = 0;i < len;i++)
			{
				pjvo 					= new FileUpload();
				pjvo.fileName 			= values[i].FILE_NAME;
				pjvo.fileBy 			= values[i].CREATE_NAME;
//				pjvo.MODIF_PAR 			= values[i].MODIFY_NAME;
				pjvo.fileExt 			= values[i].FORMAT;
				pjvo.fileUUID			= values[i].PATH;
				pjvo.fileJoin		 	= values[i].JOIN_MAIL;
				pjvo.fileId 			= values[i].FILEID;
				pjvo.fileIdCommande		= values[i].IDCOMMANDE;
				pjvo.fileSize			= values[i].FILE_SIZE;
				pjvo.fileDate 			= values[i].DATE_CREATE;
//				pjvo.DATE_MODIF_DATE 	= values[i].DATE_MODIFY;
				pjvo.fileDateStrg		= Formator.formatDate(pjvo.fileDate);
//				pjvo.BTN_DL_VISIBLE		= true;
				
				attach.addItem(pjvo);
			}
			
			return attach;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE
		//--------------------------------------------------------------------------------------------//
		
		private function popup(txt:String):void
		{
			ConsoviewAlert.afficherError(txt, 'Consoview');
		}
		
	}
}