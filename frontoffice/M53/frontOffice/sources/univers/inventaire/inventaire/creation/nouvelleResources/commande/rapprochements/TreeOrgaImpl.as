package univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements
{
	import composants.util.OrgaSearchTreeIHM;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class TreeOrgaImpl extends Box
	{
		/**
		 * Reference vers le composant affichant l'arbre des organisations
		 * */
		public var cpArbreAffectation : OrgaSearchTreeIHM;
				
		/**		  
		 * Reference vers la collection contenant la liste des organisations
		 * */
		private var _listeOrganisations : ArrayCollection;
		public function get listeOrganisations ():ArrayCollection{
			if (_listeOrganisations == null) _listeOrganisations = new ArrayCollection(); 
			return _listeOrganisations;
		}
		public function set listeOrganisations( liste : ArrayCollection):void{
			_listeOrganisations = liste;
		}
		
		private var _selectNodeParDefaut : Boolean = false;
		private var _noeudSelectionneParDefaut : Number;
		public function get noeudSelectionneParDefaut ():Number{			
			return _noeudSelectionneParDefaut;
		}
		public function set noeudSelectionneParDefaut( idGroupeClient : Number):void{
			_selectNodeParDefaut = true;
			_noeudSelectionneParDefaut = idGroupeClient;
		}
		
		public function get selectedItem():Object{
			if (cpArbreAffectation != null){
				if (cpArbreAffectation.treePerimetre != null){
					return cpArbreAffectation.treePerimetre.selectedItem;
				}else{
					return null;
				}
			}else{
				return null;
			}
		}
		
		
				
		public function TreeOrgaImpl()
		{
			super();
		}
		
				
		protected function fillOrganisation():void
		{
			
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getOrganisationCUSGEOANU",
																				fillOrganisationResult);				
			RemoteObjectUtil.callService(op, idGroupeMaitre);
		}
		
		private function fillOrganisationResult(re:ResultEvent):void{
			listeOrganisations = re.result as ArrayCollection;
			if (_selectNodeParDefaut){
				selectionnerLeNoeudSuivant(noeudSelectionneParDefaut);
				_selectNodeParDefaut = false;	
			}
			
		}
		
		
		
		private function selectionnerLeNoeudSuivant(idgroupeClient : Number):void{			
			cpArbreAffectation.selctionnerLeNoeudCible(idgroupeClient);
		}
	}
}