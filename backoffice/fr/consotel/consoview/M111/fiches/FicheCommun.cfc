<cfcomponent name="FicheCommun" alias="fr.consotel.consoview.M111.fiches.FicheCommun">

	<cfset init()>

	<cffunction name="init" hint="initialisation  des variables de session" access="remote">
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser = SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue = SESSION.USER.GLOBALIZATION>
		<cfset idLangue = SESSION.USER.IDGLOBALIZATION>
		<cfset idGroupeClient = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>
	
	<!--- ----------------- DEBUT INCIDENT ----------------- --->
	<!--- 
	    Mise à jour des infos de la fiche incident ou ajout d'un incident.
	--->
	
	<cffunction name="setInfosIncident" access="remote" returntype="any" output="false"
	            hint="Mise à jour des infos de la fiche incident ou ajout d'un incident.">
		<cfargument name="idIncident" required="true" type="numeric"/>
		<cfargument name="idEquipement" required="true" type="numeric"/>
		<cfargument name="idRevendeur" required="true" type="numeric"/>
		<cfargument name="refIncident" required="true" type="String"/>
		<cfargument name="description" required="true" type="String"/>
		<cfargument name="isClose" required="true" type="numeric"/>
		<cfargument name="solution" required="true" type="String"/>
		<cfargument name="putActif" required="true" type="numeric"/>
		<cfargument name="libelleEqpt"	 		required="true" type="String" />
		<cfargument name="idcause"		 		required="true" type="Numeric" />		
		<cfargument name="revendeuremail" 		required="true" type="String"/>
		<cfargument name="subject" 				required="true" type="String"/>
		<cfargument name="lang" 				required="true" type="String"/>
		
		<cfargument name="ITEM1"		required="true" type="string" />
		<cfargument name="ID_TERMINAL"	required="true" type="Numeric" />
		<cfargument name="ID_SIM"		required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE"	required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE"	required="true" type="Numeric" />
		<cfargument name="DATE_EFFET"	required="true" type="string" />
		<cfargument name="IS_PRETER"	required="true" type="numeric" />
		
		<cfset chaineDebug = "#idRacine#,#idIncident#,#idEquipement#,#idRevendeur#,
							#refIncident#,#description#,#idUser#,
							#isClose#,#solution#,#putActif#,#ITEM1#,#SESSION.PERIMETRE.ID_PERIMETRE#">
		
		<cflog text="--------------- DEBUT ASSOCIATION --------------- ">
		<cflog text="ITEM1 : #ITEM1#">
		<cflog text="ID_TERMINAL : #ID_TERMINAL#">
		<cflog text="ID_SIM : #ID_SIM#">
		<cflog text="ID_EMPLOYE : #ID_EMPLOYE#">
		<cflog text="IDSOUS_TETE : #IDSOUS_TETE#">
		<cflog text="DATE_EFFET : #DATE_EFFET#">
		
			<cftry>
				<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.setInfosIncidents">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIncident#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refIncident#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#description#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isClose#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#solution#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#putActif#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ITEM1#">
					<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
					<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result_idIncident"/>
					<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" variable="p_result_numIncident"/>
				</cfstoredproc>
	
				<cfset p_retour = ArrayNew(1)>
				<cfset p_retour[1] = p_result>
				<cfset p_retour[2] = p_result_idIncident>
				<cfset p_retour[3] = p_result_numIncident>
			<cfcatch type="any" >
				<cflog text="#cfcatch.Message# : #cfcatch.Detail#" >
				<cfset p_retour = ArrayNew(1)>
				<cfset p_retour[1] = -10/>
			</cfcatch>
			</cftry>
		<cfif p_retour[1] eq 1>
			<cftry>
				<cfset uid_action = createUUID()/>
				
				<cfset incidentresult = setMailIncident(p_retour[1], p_retour[2], idEquipement, p_retour[2], p_retour[3], revendeuremail, subject, lang)>
				<cftry>
					<cfset result = logger.logAction(19,1,ITEM1,"",ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET)/>
				<cfcatch type="any" >
					<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
					<cfset result = logger.sendMailAction(19,1,ITEM1,"",ID_TERMINAL,ID_SIM,ID_EMPLOYE,IDSOUS_TETE,uid_action,DATE_EFFET,ERROR)/>
				</cfcatch>
				</cftry>
			<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type,cfcatch.stackTrace)>
				<cfset result = logger.sendMailAction(19,1,"Erreur d'envoi de mail incident","",0,0,0,0,uid_action,DATE_EFFET,ERROR)/>
			</cfcatch>
			</cftry>
		</cfif>	

		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    suppresion d'un incident
	 --->
	
	<cffunction name="deleteIncident" access="remote" output="false" returntype="Numeric"
	            hint="suppresion d'un incident">
		<cfargument name="idIncident" type="Numeric" required="true"/>
		<cfargument name="putActif" type="Numeric" required="true"/>
		<cfargument name="idEquipement" type="Numeric" required="true"/>
	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111.del_Ticket">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIncident#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#putActif#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquipement#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- ----------------- FIN INCIDENT ----------------- --->
	<!--- -------------- DEBUT INTERVENTION -------------- --->
	<!--- 
	    Mise à jour des infos de la fiche intervention ou ajout d'une intervention.
	--->
	
	<cffunction name="setInfosIntervention" access="remote" returntype="Array" output="false"
	            hint="Mise à jour des infos de la fiche intervention ou ajout d'une intervention.">
		<cfargument name="idIncident" required="true" type="numeric"/>
		<cfargument name="idIntervention" required="true" type="numeric"/>
		<cfargument name="libelle" required="true" type="String"/>
		<cfargument name="dateIntervention" required="true" type="String"/>
		<cfargument name="horaire" required="true" type="numeric"/>
		<cfargument name="idTypeIntervention" required="true" type="numeric"/>
		<cfargument name="prix" required="true" type="numeric"/>
		<cfargument name="description" required="true" type="String"/>
		<cfargument name="isClose" required="true" type="numeric"/>
	
		<cfset chaineDebug = "#idIncident#,#idIntervention#,#libelle#,
							#idUser#,#dateIntervention#,#horaire#,#idTypeIntervention#,
							#prix#,#description#,#isClose#">
	
		<!--- <cfabort showError="#chaineDebug#"> --->
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.setInfosIntervention">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIncident#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIntervention#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dateIntervention#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#horaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idTypeIntervention#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#prix#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#description#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isClose#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result_idIntervention"/>
			<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" variable="p_result_numIntervention"/>
		</cfstoredproc>
	
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = p_result>
		<cfset p_retour[2] = p_result_idIntervention>
		<cfset p_retour[3] = p_result_numIntervention>
	
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    Suppression d'une intervention
	 --->
	
	<cffunction name="deleteIntervention" access="remote" output="false" returntype="Numeric"
	            hint="Suppression d'une intervention">
		<cfargument name="idIntervention" type="Numeric" required="true"/>
	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111.del_Intervention">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIntervention#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour/>
	</cffunction>
	
	<!--- 
	    Recupère la liste des type d'intervention
	 --->
	
	<cffunction name="getListeTypeIntervention" access="remote" output="false" returntype="Query"
	            hint="Recupère la liste des type d'intervention">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111.getTypeIntervention">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<!--- 
	    Recupère la liste des interventions d'un incident
	 --->
	
	<cffunction name="getListeIntervention" access="remote" output="false" returntype="Query"
	            hint="Recupère la liste des interventions d'un incident">
		<cfargument name="idIncident" type="Numeric" required="true"/>
	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111.getListeIntervention">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIncident#">
			<cfprocresult name="p_result">
		</cfstoredproc>
	
		<cfreturn p_result/>
	</cffunction>
	
	<!--- --------------- FIN INTERVENTION --------------- --->
	<!--- ----------------- DEBUT GENERAL ----------------- --->
	<!--- 
	    Fournis la liste des civilites
	 --->
	
	<cffunction name="getCacheData_OLD" access="remote" returntype="Any" 
	            hint="Fournis la liste des civilites">
		<cfset initDataStruct = structNew()>
		<cfset initDataStruct.PROFIL_ACTION = getMatriceDroit()>
		<cftry>
			
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getCivilite">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
					<cfprocresult name="p_retour1">
				</cfstoredproc>
				
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.Liste_Champs_Perso">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
					<cfprocresult name="p_retour2">
				</cfstoredproc>
				
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.Liste_Champs_Perso_st">
					<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
					<cfprocresult name="p_retour3">
				</cfstoredproc>
				
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.Liste_Usage">
					<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="p_retour4">
				</cfstoredproc>
				
				<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_M111B.L_TYPES_CTRT">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
					<cfprocresult name="p_retour5">
				</cfstoredproc>
				
				<cfstoredproc datasource="#Session.offreDSN#" 
				              procedure="pkg_cv_gestion_fournisseur_v1.get_liste_pays">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
					<cfprocresult name="p_retour6">
				</cfstoredproc>
				
				<cfstoredproc datasource="ROCOFFRE" procedure="pkg_M111B.listeConstructeurs">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
					<cfprocresult name="results7">
				</cfstoredproc>
				
				<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m111.get_catalogue_revendeur">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0">
					<cfprocresult name="p_result8">
				</cfstoredproc>
				
				<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_M111B.getlistecategorie_v2">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLangue#"/>
					<cfprocresult name="p_retour">
				</cfstoredproc>
				
				<cfstoredproc datasource="ROCOFFRE" procedure="pkg_M111B.List_profil_gestionnaire">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#idUser#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idRacine#"
					             null="false">
					<cfprocresult name="p_result9">
				</cfstoredproc>
				
				<cfset initDataStruct.CATEGORIE		= p_retour>
				<cfset initDataStruct.CIVILITE 		= p_retour1>
				<cfset initDataStruct.LIBELLE		= p_retour2>
				<cfset initDataStruct.LIBELLELIGNE	= p_retour3>
				<cfset initDataStruct.USAGE			= p_retour4>
				<cfset initDataStruct.CONTRAT		= p_retour5>
				<cfset initDataStruct.PAYS			= p_retour6>
				<cfset initDataStruct.CONSTRUCTEUR	= results7>
				<cfset initDataStruct.REVENDEUR		= p_result8>
				<cfset initDataStruct.PROFILEQP		= p_result9>
				<cfset initDataStruct.TYPELIGNE 	= getTypeLigne()>

		<cfcatch type="any" >
			
			<cflog text="cfcatch.detail = #cfcatch.detail#" >
		</cfcatch>
		</cftry>
		
		<cfreturn initDataStruct>
	</cffunction>

	<cffunction name="getCacheData" access="remote" returntype="Any" hint="Fournis la liste des civilites">
		<!---cflog text = "-------------------------------------------------------- log to NEW getCacheData"--->
		<cftry>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getcache_parc">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idLangue#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
				
				<cfprocresult name="p_retour1" resultset="1">
				<cfprocresult name="p_retour2" resultset="2">
				<cfprocresult name="p_retour3" resultset="3">
				<cfprocresult name="p_retour4" resultset="4">
				<cfprocresult name="p_retour5" resultset="5">
				<cfprocresult name="p_retour6" resultset="6">
				<cfprocresult name="results7" resultset="7">
				<cfprocresult name="p_result8" resultset="8">
				<cfprocresult name="p_retour" resultset="9">
				<cfprocresult name="p_result9" resultset="10">
				<cfprocresult name="p_result10" resultset="11">
				<cfprocresult name="p_result11" resultset="12">
			</cfstoredproc>

			<cfset initDataStruct = structNew()>
			
			<cfset initDataStruct.CIVILITE = p_retour1>
			<cfset initDataStruct.LIBELLE= p_retour2>
			<cfset initDataStruct.LIBELLELIGNE= p_retour3>
			<cfset initDataStruct.USAGE= p_retour4>
			<cfset initDataStruct.CONTRAT= p_retour5>
			<cfset initDataStruct.PAYS= p_retour6>
			<cfset initDataStruct.CONSTRUCTEUR= results7>
			<cfset initDataStruct.REVENDEUR= p_result8>
			<cfset initDataStruct.CATEGORIE= p_retour>
			<cfset initDataStruct.PROFILEQP= p_result9>
			<cfset initDataStruct.PROFIL_ACTION = p_result10>
			<cfset initDataStruct.TYPELIGNE = p_result11>
		<cfcatch type="any" >
			<cflog text="cfcatch.detail = #cfcatch.detail#" >
		</cfcatch>
		</cftry>
	<cfreturn initDataStruct>
</cffunction>

	<!--- 
	    Fournis la liste des pays
	 --->
	
	<cffunction name="getListePays" access="remote" returntype="query" output="false"
	            hint="Fournis la liste des pays">
		<cfstoredproc datasource="#Session.offreDSN#" 
		              procedure="pkg_cv_gestion_fournisseur_v1.get_liste_pays_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<!--- 
	    Fournis la liste des sites
	 --->
	
	<cffunction name="fournirListeSitesLivraisonsPoolGestionnaire" access="remote" returntype="query" 
	            output="false" hint="Fournis la liste des sites">
		<cfargument name="idPoolGestion" required="true" type="numeric"/>
		<cfargument name="clef" required="false" type="string" default=""/>
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.GETSITEOFPOOL" 
		              blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#idPoolGestion#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    Fournis la liste des civilites
	 --->
	
	<cffunction name="getCivilite" access="remote" returntype="query" 
	            hint="Fournis la liste des civilites">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getCivilite">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    Fournis la liste des libellés perso
	 --->
	
	<cffunction name="getLibellePerso" access="remote" returntype="query" 
	            hint="Fournis la liste des libellés perso">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.Liste_Champs_Perso">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    Retourne la liste des constructeurs
	--->
	
	<cffunction name="getConstructeur" returntype="query" access="remote" output="true"
	            hint="Retourne la liste des constructeurs">
	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111.listeConstructeurs">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocresult name="results">
		</cfstoredproc>
	
		<cfreturn results>
	</cffunction>
	
	<!--- 
	    Retourne la liste des modèles    
	 --->
	
	<cffunction name="fournirListeMobileCatalogueClient" returntype="query" access="remote" 
	            hint="Retourne la liste des modèles">
		<cfargument name="idRevendeur" required="true" type="numeric" displayname="revendeur"
		            hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string" displayname="clef"
		            hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="string" displayname="segment"
		            hint="le filtre par segment"/>
	
		<cfset resultProc = fournirListeEquipementsCatalogueRevendeur(idRevendeur, clef, segment)>
		<cfquery name="getListe" dbtype="query">
			select *
			from resultProc
			where IDTYPE_EQUIPEMENT = 9 or IDTYPE_EQUIPEMENT = 70
		</cfquery>
		<cfreturn getListe>
	
	</cffunction>
	
	<!--- 
	    Retourne la liste des equipement catalogue
	--->
	
	<cffunction name="fournirListeEquipementsCatalogueRevendeur" returntype="query" access="remote" 
	            hint="Retourne la liste des equipement catalogue">
		<cfargument name="idRevendeur" required="true" type="numeric" displayname="revendeur"
		            hint="le revendeur"/>
		<cfargument name="clef" required="true" type="string" displayname="clef"
		            hint="clef de recherche"/>
		<cfargument name="segment" required="false" type="string" displayname="segment"
		            hint="le filtre par segment"/>
	
		<cfset p_idcategorie_equipement = 0>
		<cfset p_idtype_equipement = 0>
		<cfset p_idfournisseur = idRevendeur>
		<cfset p_idgamme_fournis = 0>
		<cfset p_chaine = clef>
		<cfset p_type_fournisseur = 0>
	
		<cfif p_type_fournisseur eq 0>
			<cfset flag = "true">
		<cfelse>
			<cfset flag = "false">
		</cfif>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111.SearchCatalogueRevendeur_v4">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcategorie_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idtype_equipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idfournisseur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idgamme_fournis#">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#p_chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_type_fournisseur#" null="#flag#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<!--- 
	    Retourne la liste des revendeurs
	--->
	
	<cffunction name="getListeRevendeur" access="remote" output="false" returntype="Query">
		<cfargument name="segment" required="true" type="numeric" default="0"
		            displayname="numeric p_segment" hint="Initial value for the p_segmentproperty."/>
		<cfargument name="chaine" required="false" type="string" default=""
		            displayname="string chaine" hint="paramNotes"/>
	
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.get_catalogue_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
	
		<cfreturn p_result>
	</cffunction>
	
	<!--- 
	    Retourne la liste des types de contrats
	--->
	
	<cffunction name="getListeTypeContrat" access="remote" returntype="Query" output="false"
	            hint="Retourne la liste des types de contrats">
	
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_M111B.L_TYPES_CTRT">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
			<cfprocresult name="qGetListeTypesContrat">
		</cfstoredproc>
	
		<cfreturn qGetListeTypesContrat>
	</cffunction>
	
	<!--- 
	    Retourne la liste des usages (aussi appelé "fonction")
	--->
	
	<cffunction name="getUsages" access="remote" returntype="query" 
	            hint="Retourne la liste des usages (aussi appelé 'fonction')">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.Liste_Usage">
			<cfprocparam value="#getidGroupeLigne()#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetUsages">
		</cfstoredproc>
		<cfreturn qGetUsages>
	</cffunction>
	
	<!--- 
	    Retourne la liste des profiles
	--->
	
	<cffunction name="getListProfilEquipement" access="remote" returntype="Query" 
	            description="liste profile" hint="Retourne la liste des profiles">
		<cfstoredproc datasource="#session.OFFREDSN#" procedure="pkg_M111B.List_profil_gestionnaire">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#idUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#"
			             null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<!--- 
	    Retourne la liste des revendeurs pour un profile d'equipement
	--->
	
	<cffunction name="getListRevendeurForProfEqpt" access="remote" returntype="Query" output="false"
	            hint="Retourne la liste des revendeurs pour un profile d'equipement">
		<cfargument name="idPoolGestion" required="true" type="numeric"/>
		<cfargument name="idProfilEquipement" required="true" type="numeric"/>
		<cfargument name="clefRecherche" required="true" type="String"/>
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.get_revendeur">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#idUser#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#idPoolGestion#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil_equipement" 
			             value="#idProfilEquipement#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_Chaine" value="#clefRecherche#">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" variable="p_idracine" value="#idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    Récupère les liées a un operateur
	--->
	
	<cffunction name="getInfoClientOp" access="remote" returntype="query" output="false"
	            hint="Récupère les liées a un operateur">
		<cfargument name="idOperateur" required="true" type="numeric" default=""
		            displayname="numeric idOperateur" hint="ID de l'operateur"/>
	
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m111.getInfoclientOp">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idOperateur" value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idRacine" value="#idRacine#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
	
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    Récupère la liste des types de ligne
	--->
	
	<cffunction name="getTypeLigne" access="remote" returntype="query" 
	            hint="Récupère la liste des types de ligne">
	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.l_type_ligne_v2">
			<cfprocparam value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="retour">
		</cfstoredproc>
		<cfreturn retour>
	</cffunction>
	
	<!--- 
	    Récupère la liste des catégories d'équipements
	--->
	
	<cffunction name="getCategoriesEquipement" access="remote" returntype="query" 
	            hint="Récupère la liste des catégories d'équipements">
	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111_ia.get_categorieEquipement">
			<cfprocparam value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="retour">
		</cfstoredproc>
		<cfreturn retour>
	</cffunction>
	
	<!--- 
	    Récupère la liste des types d'équipements
	--->
	
	<cffunction name="getTypesEquipement" access="remote" returntype="query" 
	            hint="Récupère la liste des types d'équipements">
	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111_ia.get_typeEquipement">
			<cfprocparam value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="retour">
		</cfstoredproc>
		<cfreturn retour>
	</cffunction>
	
	<!--- 
	    Récupère la liste des types d'équipements avec leurs categories
	--->
	
	<cffunction name="getTypesEquipementWithCategorie" access="remote" returntype="query" 
	            hint="Récupère la liste des types d'équipements avec leurs categories">
	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.get_typeEquipement_v2">
			<cfprocparam value="#codeLangue#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="retour">
		</cfstoredproc>
		<cfreturn retour>
	</cffunction>
	
	<!--- 
	    Récupère la liste des operateurs par segment
	--->
	
	<cffunction name="fournirListeOperateursSegment" access="remote" returntype="query" output="false"
	            hint="Récupère la liste des operateurs par segment">
		<cfargument name="segment" required="true" type="string" default=""
		            displayname="string segment" hint="Initial value for the segmentproperty."/>
	
		<cfset qResult = "">
		
		<cfswitch expression="#ucase(segment)#">
		
			<cfcase value="FIXEDATA">
				<!--- <cfquery name="qResult" datasource="#Session.OFFREDSN#" blockfactor="10">
				    SELECT op.operateurid, op.nom as libelle FROM operateur op 
				</cfquery> --->
			</cfcase>
			<cfcase value="MOBILE">
			
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.GETFABOP_ACTIF">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idRacine" value="#idRacine#"/>
					<cfprocresult name="qResult">
				</cfstoredproc>
			
			</cfcase>
			<cfdefaultcase>
				<!--- <cfquery name="qResult" datasource="#Session.OFFREDSN#" blockfactor="10">
				    SELECT op.operateurid, op.nom as libelle FROM operateur op 
				</cfquery> --->
			</cfdefaultcase>
		
		</cfswitch>
		
		<cfreturn qResult/>
	</cffunction>
	
	<!--- 
	    Fournis la liste des comptes et sous comptes pour un gestionnaire, un pool et un opérateur 
	donné.
	 --->
	
	<cffunction name="getListeComptesOperateurByLoginAndPool" access="remote" returntype="Query" 
	            output="false" 
	            hint="Fournis la liste des comptes et sous comptes pour un gestionnaire, un pool et un opérateur donné.">
		<cfargument name="idOperateur" required="true" type="numeric"/>
		<cfargument name="idPool" required="true" type="numeric"/>
	
		<cfstoredproc datasource="#Session.OFFREDSN#" 
		              procedure="pkg_m111.getcompteopeloginpool">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idPool#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
	    Fournis la liste des lignes/sim sans terminal
	 --->
	
	<cffunction name="listeLigneSimSansTerm" 
	            description="retourne une liste des sim sans terminaux associés" access="remote" 
	            returntype="query" hint="Fournis la liste des lignes/sim sans terminal">
		<cfargument name="idPool" required="true" type="Numeric"/>
	
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_FLOTTE.Liste_SIM_sans_term_v2" 
		              blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IdRacine#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IdPool#">
			<cfprocresult name="result">
		</cfstoredproc>
	
		<cfreturn result/>
	</cffunction>
	
	<!--- 
	    Fournis la liste des lignes sans équipement
	 --->
	
	<cffunction name="listeLigneSansTerm" 
	            description="retourne une liste des lignes fixes sans terminaux associés" 
	            access="remote" returntype="query" hint="Fournis la liste des lignes sans équipement">
		<cfargument name="idPool" required="true" type="Numeric"/>
	
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111.listeLigneSansTerm" 
		              blockfactor="100">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IdRacine#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IdPool#">
			<cfprocresult name="result">
		</cfstoredproc>s
	
		<cfreturn result/>
	</cffunction>
	
	<!--- ----------------- GESTION DE L'HISTORIQUE ----------------- --->
	<cffunction name="getHistorique" access="remote" returntype="query" hint="Recupere l'historique">
		<cfargument name="idEmploye" required="true" type="Numeric"/>
		<cfargument name="idTerminal" required="true" type="Numeric"/>
		<cfargument name="idSim" required="true" type="Numeric"/>
		<cfargument name="idSousTete" required="true" type="Numeric"/>
	
		<cfset chaineDebug = "#idRacine#,#idEmploye#,#idTerminal#,#idSim#,#idSousTete#,#codeLangue#">
	
		<!--- <cfabort showError="#chaineDebug#"> --->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.Historique_action">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#idEmploye#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#idTerminal#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#idSim#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#idSousTete#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
			<cfprocresult name="qGetHistorique">
		</cfstoredproc>
		<cfreturn qGetHistorique>
	</cffunction>

		<!--- 
		Mise à jour de l'incident et envoi un email gestionnaire -> revendeur SAV , revendeur SAV -> gestionnaire
	 --->
	<cffunction name="setMailIncident" access="remote" returntype="Numeric" output="false" hint="TYPE PROFIL (CLIENT USER DISTIB)">
		<cfargument name="infosincident"		required="true" type="numeric"/>
		<cfargument name="idincident"			required="true" type="Numeric"/>
		<cfargument name="idequipement"			required="true" type="Numeric"/>
		<cfargument name="idintervention"		required="true" type="Numeric"/>		
		<cfargument name="numerointervention"	required="true" type="String"/>
		<cfargument name="revendeuremail" 		required="true" type="String"/>
		<cfargument name="subject" 				required="true" type="String"/>
		<cfargument name="lang" 				required="true" type="String"/>
		
			<cfset mailing = createObject("component","fr.consotel.consoview.M01.Mailing")>
			
			<cfset idprofil = session.user.idtypeprofil>
			<cfset numinter = "OPER" & Mid(numerointervention, 6, Len(numerointervention))>
			<cfset M111_UUID = 'Rapport-' & idintervention & '-' & numinter>
			<cfset rsltMail = "">
			
			<cfset mailinginfos = ArrayNew(1)>
			<cfset mailinfos 	= structnew()> 

			<cfset p_email = mailing.getMailToDatabase(#M111_UUID#)>

			<cfloop query="p_email">
				<cfif p_email.TAG EQ "INF">
					
					<cfset email = structnew()>
					<cfset email.FROM 			= p_email.MAIL_FROM>
					<cfset email.GESTIONNAIRE	= p_email.MAIL_TO>
					<cfset email.REVENDEUR 		= p_email.MAIL_BCC>
					<cfset email.SUBJECT 		= p_email.MAIL_SUBJECT>
					<cfset email.CODE_APPLI 	= p_email.CODE_APPLI>
					
					<cfset mailinfos = email>
				
				<cfelse>

					<cfset maj = mailing.updateMailToDatabase(0, M111_UUID, p_email.TAG)>
					
					<cfif p_email.TAG EQ "COR" || p_email.TAG EQ "PDF">
						
						<cfset ArrayAppend(mailinginfos, p_email)>
						
					</cfif>
					
				</cfif>
			</cfloop>
			
			<cfif ArrayLen(mailinginfos) EQ 0>
			
				<cfset rsltMail = setEmailInfos(idincident, idintervention, numinter, M111_UUID, revendeuremail, subject)>
				
				<cfset p_email = mailing.getMailToDatabase(#M111_UUID#)>

				<cfloop query="p_email">
					<cfif p_email.TAG EQ "INF">
						
						<cfset email = structnew()>
						<cfset email.FROM 			= p_email.MAIL_FROM>
						<cfset email.GESTIONNAIRE	= p_email.MAIL_TO>
						<cfset email.REVENDEUR 		= p_email.MAIL_BCC>
						<cfset email.SUBJECT 		= p_email.MAIL_SUBJECT>
						<cfset email.CODE_APPLI 	= p_email.CODE_APPLI>
						
						<cfset mailinfos = email>
					
					<cfelse>
	
						<cfset maj = mailing.updateMailToDatabase(0, M111_UUID, p_email.TAG)>
						
						<cfif p_email.TAG EQ "COR" || p_email.TAG EQ "PDF">
							
							<cfset ArrayAppend(mailinginfos, p_email)>
							
						</cfif>
						
					</cfif>
				</cfloop>
				
			<cfelse>
		
				<cfif idprofil EQ 1>
					
					<cfset gestionnaire = session.user.email>
					<cfset revendeur 	= email.REVENDEUR>
					
				<cfelse>
				
					<cfset gestionnaire = email.GESTIONNAIRE>
					<cfset revendeur 	= email.REVENDEUR>
				
				</cfif>
		
				<cfset rsltMail = mailing.updateMailInfos(#M111_UUID#, "INF", 0, #gestionnaire#, #revendeur#, #p_email.MAIL_SUBJECT#, #idprofil#)>
			
			</cfif>
			
			<cfif idprofil EQ 1>
				
				<cfset gestionnaire = session.user.email>
				<cfset revendeur 	= email.REVENDEUR>
				
			<cfelse>
			
				<cfset gestionnaire = email.GESTIONNAIRE>
				<cfset revendeur 	= email.REVENDEUR>
			
			</cfif>
			<cfset infosincident = sendEmail(infosincident, M111_UUID, idincident, idequipement)>
			<cflog text="infosincident = #infosincident#">			
		<cfreturn infosincident>
	</cffunction>

	<!--- 
		Insert les données du mail
	 --->
	<cffunction name="setEmailInfos" access="remote" returntype="Numeric" output="false" hint="TYPE PROFIL (CLIENT USER DISTIB)">
		<cfargument name="idincident"			required="true" type="Numeric"/>
		<cfargument name="idintervention"		required="true" type="Numeric"/>		
		<cfargument name="numerointervention"	required="true" type="String"/>
		<cfargument name="m111_uuid"				required="true" type="String"/>
		<cfargument name="revendeuremail" 		required="true" type="String"/>
		<cfargument name="subject" 				required="true" type="String"/>
		
			<cfset mailing = createObject("component","fr.consotel.consoview.M01.Mailing")>
			
			<cfset idprofil = session.user.idtypeprofil>
			<cfset username = SESSION.USER.NOM & " " & SESSION.USER.PRENOM>
			<cfset subject 	= subject & " " & numerointervention>
			<cfset from 	= "GestionParc-sav@consotel.fr">
			
			<cfset revendeur 	= ''>
			<cfset gestionnaire = ''>
			
			<cfif idprofil NEQ 2>
					
				<cfset gestionnaire = session.user.email>
				<cfset revendeur 	= revendeuremail>
				
			<cfelse>
			
				<cfset gestionnaire = session.user.email>
				<cfset revendeur 	= session.user.email>
			
			</cfif>
			
			<cfset dbg = "#session.user.clientaccessid#, #username#, #m111_uuid#, #from#, #gestionnaire#, #subject#, #revendeur#, #session.codeapplication#, #rsltMail#">
			<cflog text="#dbg#"/>
			
			<cfset rsltMail = mailing.setMailToDatabase(#session.user.clientaccessid#, #username#, #m111_uuid#, "M111", "INF", #from#, 
															#gestionnaire#, #subject#, #revendeur#, #idprofil#, #session.codeapplication#)>
															
			<cfset rsltMail1 = mailing.setMailToDatabase(#session.user.clientaccessid#, #username#, #m111_uuid#, "M111", "COR", #from#, 
															#gestionnaire#, #subject#, #revendeur#, #idprofil#, #session.codeapplication#)>
															
			<cfset rsltMail2 = mailing.setMailToDatabase(#session.user.clientaccessid#, #username#, #m111_uuid#, "M111", "PDF", #from#, 
															#gestionnaire#, #subject#, #revendeur#, #idprofil#, #session.codeapplication#)>
		<cfreturn rsltMail>
	</cffunction>	

	<!--- 
		Envoi un email gestionnaire -> revendeur SAV , revendeur SAV -> gestionnaire
	 --->
	<cffunction name="sendEmail" access="remote" returntype="Numeric" output="false" hint="TYPE PROFIL (CLIENT USER DISTIB)">
		<cfargument name="infosincident" required="true" type="numeric"/>
		<cfargument name="m111_uuid"		 required="true" type="String"/>
		<cfargument name="idincident" 		 required="true" type="numeric" />
		<cfargument name="idequipement" 		 required="true" type="numeric" />
			<cftry>
			
				<cfset var mailing  = createObject("component","fr.consotel.consoview.M01.Mailing")>				
				<cfset var api = createObject("component","fr.consotel.consoview.api.CV")>	
				
				<cfset var ImportTmp 		= "/container/M111/">
				<cfset var rsltApi  		= -4>
				<cfset var resltRecordMail 	= 0>
				<cfset var directory		= #ImportTmp# & #m111_uuid#>
			
				<!--- <cfdirectory action="Create" directory="#ImportTmp#" type="dir" mode="777"> --->
	
				<cfif NOT DirectoryExists('#directory#')>
					
					<cfdirectory action="Create" directory="#directory#" type="dir" mode="777">
	
				</cfif>
					
			<!--- COMMUN A TOUS --->	
				<!--- BIP REPORT 
					Chemin du rapport : /consoview/M11/SAV/SAV.xdo
					Nom du template : SAV
					Paramètres : P_IDEQUIPEMENT, P_IDRACINE, P_LANGID, P_IDSAV
				--->
				<cfset parameters["bipReport"] = structNew()>
				<cfset parameters["bipReport"]["xdoAbsolutePath"]	= "/consoview/M111/SAV/SAV.xdo">
				<cfset parameters["bipReport"]["localization"]		= codeLangue>
				
				<!--- DONNEES ENVOYEES A BIP --->
				<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
				<!---- IDEQUIPEMENT ---->
				<cfset parameters["bipReport"]["reportParameters"]["P_IDEQUIPEMENT"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["P_IDEQUIPEMENT"]["parameterValues"]= [idequipement]>
				<!---- IDRACINE ---->
				<cfset parameters["bipReport"]["reportParameters"]["P_IDRACINE"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["P_IDRACINE"]["parameterValues"]= [idRacine]>
				<!---- LANGID ---->
				<cfset parameters["bipReport"]["reportParameters"]["P_LANGID"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["P_LANGID"]["parameterValues"]= [idLangue]>
				<!---- IDSAV : id de l'incident ---->
				<cfset parameters["bipReport"]["reportParameters"]["P_IDSAV"] = structNew()>
				<cfset parameters["bipReport"]["reportParameters"]["P_IDSAV"]["parameterValues"]= [idincident]>
				<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
				<cfset parameters.bipReport.delivery.parameters.ftpUser 	= "container">
				<cfset parameters.bipReport.delivery.parameters.ftpPassword = "container">
									
				<cfset parameters.EVENT_TARGET = m111_uuid>
				<cfset parameters.EVENT_HANDLER = "M11">
				
				<!--- BIP REPORT --->
			<!--- COMMUN A TOUS --->		
				
			<!--- CORPS DU MAIL --->
				<cfset parameters["bipReport"]["outputFormat"] 	= "html">
				<cfset parameters["bipReport"]["xdoTemplateId"]	= "">
				<cfset parameters.bipReport.delivery.parameters.fileRelativePath = "M111/#m111_uuid#/body.html">
				
				<cfset parameters.EVENT_TYPE = "COR">
				<cfset rsltApi = api.invokeService("IbIs", "scheduleReport", parameters)>
			<!--- RAPPORT PIECE JOINTE --->
				<cfset parameters["bipReport"]["outputFormat"] 	= "pdf">
				<cfset parameters["bipReport"]["xdoTemplateId"]	= "">
				<cfset parameters.bipReport.delivery.parameters.fileRelativePath = "M111/#m111_uuid#/#m111_uuid#.pdf">
				
				<cfset parameters.EVENT_TYPE = "PDF">
				<cfset rsltApi = api.invokeService("IbIs", "scheduleReport", parameters)>
			<!--- CFCATCH --->
				<cfcatch type="any" >
					<cfmail from="monitoring@saaswedo.com" 
						subject="[M111 - SAV]Cfcatch - impossible de lancer le rapport vers BIP" 
						to="monitoring@saaswedo.com" type="text/html" >
						<cfoutput>		
							<br><br><br>
							<cfdump var="#parameters#" label="paramètre BIP"/>
							<br><br><br>		
							<cfdump var="#cfcatch#" label="cfcatch"/>
						</cfoutput>
					</cfmail>
				</cfcatch>
			</cftry>
		
		<cfreturn infosincident>
	</cffunction>	
	<!--- 
		Récupère le profil de la personne connectée
	 --->
	<cffunction name="getUserProfil" access="remote" returntype="Numeric" output="false" hint="TYPE PROFIL (CLIENT USER DISTIB)">

			<cfset idtypeprofil = session.user.idtypeprofil>
			
		<cfreturn idtypeprofil>
	</cffunction>
	
	<!--- 
		Récupère les emails revendeur
	 --->
	<cffunction name="getEmailsRevendeur" access="remote" returntype="Query" output="false" hint="Emails des revendeurs SAV">
		<cfargument name="idRevendeur" required="true" type="Numeric" />
			
			<cfset idracine = session.perimetre.id_groupe>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m11.getContactSAV">
				<cfprocparam type="In" variable="p_idrevendeur" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
				<cfprocparam type="In" variable="p_idracine" 	cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>


	<cffunction name="getMatriceDroit" access="remote" returntype="Any">
		<cfset init()>
		 
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M21.GETMATRICE_PROFIL">
			<cfprocparam value="#iduser#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>

		
		
		<cfreturn p_retour>
	</cffunction> 

</cfcomponent>
