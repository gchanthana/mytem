<cfcomponent  extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- empty --->
	</cffunction>


	<!---  --->
	<cffunction name="getMesFacturesDiv" access="public" output="true"  returnType="string"
				displayname="" 
				hint="Génère la div contenant la table du detail de la facture mensuelle d'une ligne / template D1" >
		
		<cfargument name="sous_tete" required="false" type="string" default="0">
		<cfargument name="idSousTete" required="false" type="numeric" default=0>
		<cfargument name="idInventaire_periode" required="false" type="numeric" default=0>		
		
		<cfif idSousTete eq 0>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
					SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
					SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
						<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow]>
						<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow]>
						<cfbreak>
						
				</cfif>
			</cfloop>
			
		</cfif>
		
		<!--- -------------------------------------- --->
		<!--- SI LE COLLABORATEUR A PLUSIEURS LIGNES --->
		<!--- -------------------------------------- --->
		<cfif SESSION.terminauxAvailable gt 1>
			
			<!--- COMBO CHOIX LIGNE --->
			<cfset mesFactures = '
					
					<div class="divVerticale_gauche" style=""><!--- debut div verticale --->
						<div style="height:250px;width:100%;"><!--- debut div contenant tableau montant et select --->
						
							<div class="divMaLigneComboMesfactures" align="left" style="margin-bottom:20px;"> <!--- debut div combo facture --->
								<form>
									<span style="font-size:14px;font-weight:600;">Ma ligne&nbsp;</span>&nbsp;
									<select id="selectLigne" class="selectMaLigneCombo" onchange="comboLigne();loadsablier();" style="">
			'>
			
			<cfloop query="SESSION.queryMesTerminaux"><!--- loop pour combo sous_tete --->
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>

				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >

					<cfset sousTete_ = SESSION.queryMesTerminaux["SOUS_TETE"][currentRow] >
					<cfset idSous_tete_ = SESSION.queryMesTerminaux["IDSOUS_TETE"][currentRow] >
					
					<!--- CREATION D'UNE STRUCTURE POUR LA CONSTRUCTION DE L'URL --->
					<cfset struct = {idPage = "3",
									sous_tete = "#sousTete_#",
									idSous_tete = "#idSous_tete_#",
									idInventaire_periode = 0 } 
					>
					<cfset urlString = createRefURL(struct)>
						
					<!--- formattage numero de telephone / espace entre les chiffres --->
					<cfset numm = createObject("component","portail.class.classData.DataService").phoneFormat("#sousTete_#")>
					
					<cfif sousTete_ eq sous_tete>
						<cfset mesFactures &= '<option selected="selected" id="" value="#urlString#" >#numm#</option>'> <!--- affiche la ligne selectionnée --->
					<cfelse>
						<cfset mesFactures &= '<option id="" value="#urlString#" >#numm#</option>'>
					</cfif>
					
				</cfif>
			</cfloop>
			
			<cfset mesFactures &= '
								</select>
							</form>
						</div> <!--- fin div select facture --->
			'>
		
		
		<!--- OU SI LE  COLLABORATEUR A UNE SEULE LIGNE --->
		<cfelse>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
					<cfset sous_tete = SESSION.queryMesTerminaux["SOUS_TETE"][currentRow] >
				
				</cfif>
				
			</cfloop>
			
			<!--- formatage numero de telephone / espace entre les chiffres --->
			<cfset numm = createObject("component","portail.class.classData.DataService").phoneFormat("#sous_tete#")>
			<cfif sous_tete eq "0"> <cfset numm = " -- -- -- -- -- "> </cfif>
			
			<cfset mesFactures = '<div class="divVerticale_gauche" style=""> <!--- debut div verticale --->
									<div style="height:250px;width:100%;"><!--- debut div contenant tableau montant et select --->
									
										<div class="divMaLigneComboMesfactures" align="left" style="font-weight:bold;margin-bottom:20px;"> <!--- debut div combo facture --->
											Ma ligne&nbsp;&nbsp;<span style="color:##009fe3;">#numm#</span>
										</div>
			' >
		
		</cfif>
		
		<!--- ----------------------------------------------- --->
		<!--- SELECT DATE FACTURE + TABLEAU FACTURE MENSUELLE --->
		<!--- ----------------------------------------------- --->
		
		<!--- QUERY DATE DE FACTU --->
		<!--- si la STRUCTURE EN SESSION QUERYFACT n'existe pas ou si la variable de SESSION IDSOUS_TETE pas egale au parametre idsous_tete --->
 		<cfif NOT isDefined("SESSION.queryFact") || (isDefined("SESSION.clone_IdSous_tete") && SESSION.clone_IdSous_tete neq idSousTete) >
			<cfset SESSION.queryFact = createObject("component","portail.class.classData.DataService").getDateFactByLigne(idSousTete,SESSION.PERIMETRE.ID_GROUPE,6,SESSION.USER.GLOBALIZATION) >
		</cfif> 
		
		
		<cfset euro = "">
		<cfset ht = "">
		<cfset totalAbo = " -- ">
		<cfset totalConso = " -- ">
		<cfset totalAFacturer = " -- ">
		<cfset totalAppel = " -- ">
		<cfset totalVolume = " -- ">
		
		<cfset totalAbo_ = 0>
		<cfset totalConso_ = 0>
		<cfset totalFacturation = 1>
		<cfset dateFact = "Votre facture du - au - " >
		
		<cfset dateFactAbo = "Vos abonnements et options du - au - " >
		<cfset dateFactConso = "Vos consommations du - au - "  >
		<cfset chart = "<img src='img/mytem/PieGraph_empty_bleu.png' width='487' height='248'>"  >
		

		<cfif SESSION.queryFact.recordcount gt 0><!--- SI DATE DE FACTU DE MOINS DE 6 MOIS --->

			<cfif idInventaire_periode eq 0> 
				<cfloop query = "SESSION.queryFact"><!--- loop sur query periode --->
					<cfset currentRow = SESSION.queryFact.currentrow >
					
					<cfset lastFact = SESSION.queryFact["DATE_EMISSION"][currentRow] >
					<cfset idPeriode = SESSION.queryFact["IDPERIODE_MOIS"][currentRow] >
					<cfset idInventaire_periode = SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] >
					<cfset date_deb = SESSION.queryFact["DATEDEB"][currentRow] > <!--- intervalle date conso --->
					<cfset date_fin = SESSION.queryFact["DATEFIN"][currentRow] >
					<cfset date_deb_abo = SESSION.queryFact["DATE_DEB_ABO"][currentRow] > <!--- intervalle date abo --->
					<cfset date_fin_abo = SESSION.queryFact["DATE_FIN_ABO"][currentRow] >
					<cfset num_facture = SESSION.queryFact["NUMERO_FACTURE"][currentRow] >
					<cfbreak>
				</cfloop>
			
			<cfelse>
				<!--- loop sur query periode --->
				<cfloop query = "SESSION.queryFact">
					<cfset currentRow = SESSION.queryFact.currentrow >
					
					<cfif SESSION.queryFact['IDINVENTAIRE_PERIODE'][SESSION.queryFact.currentrow] eq idInventaire_periode>
						<cfset lastFact = SESSION.queryFact["DATE_EMISSION"][currentRow] >
						<cfset idPeriode = SESSION.queryFact["IDPERIODE_MOIS"][currentRow] >
						<cfset idInventaire_periode = SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] >
						<cfset date_deb = SESSION.queryFact["DATEDEB"][currentRow] ><!--- intervalle date conso --->
						<cfset date_fin = SESSION.queryFact["DATEFIN"][currentRow] >
						<cfset date_deb_abo = SESSION.queryFact["DATE_DEB_ABO"][currentRow] ><!--- intervalle date abo --->
						<cfset date_fin_abo = SESSION.queryFact["DATE_FIN_ABO"][currentRow] >
						<cfset num_facture = SESSION.queryFact["NUMERO_FACTURE"][currentRow] >
						<cfbreak>
					</cfif>
				</cfloop>
				
			</cfif>
			
			<!--- formatage date pour le premier choix du select --->
			<cfif lastFact neq "">
				<cfset dateFact = "Votre facture du&nbsp;"&LSDateFormat(lastFact, "dd/mm/yyyy") >
				<cfset dateDeb_fact = LSDateFormat(date_deb, "dd/mm/yyyy") >
				<cfset dateFin_fact = LSDateFormat(date_fin, "dd/mm/yyyy") >
				<cfset dateDeb_fact_abo = LSDateFormat(date_deb_abo, "dd/mm/yyyy") >
				<cfset dateFin_fact_abo = LSDateFormat(date_fin_abo, "dd/mm/yyyy") >
				
				<cfset dateFactAbo = "Vos abonnements et options du "&dateDeb_fact_abo&" au "&dateFin_fact_abo >
				<cfset dateFactConso = "Vos consommations du "&dateDeb_fact&" au "&dateFin_fact >
				<cfset dateGraph = "Répartition de votre facture du "&dateDeb_fact&" au "&dateFin_fact>
			</cfif>
			
			
			<!--- QUERY MONTANT FACTU --->
			<!--- si la STRUCTURE EN SESSION QUERYFACT n'existe pas ou si la variable de SESSION IDSOUS_TETE pas egale au parametre idsous_tete --->
			<cfif NOT isDefined("SESSION.queryMontant") ||
					 (isDefined("SESSION.clone_IdInventaire_periode") && SESSION.clone_IdInventaire_periode neq idInventaire_periode) ||
					 (isDefined("SESSION.clone_IdSous_tete") && SESSION.clone_IdSous_tete neq idSousTete) >

				<cfset SESSION.queryMontant = createObject("component","portail.class.classData.DataService").getMontantFactureByLigne(SESSION.PERIMETRE.IDRACINE_MASTER,idSousTete,idInventaire_periode,SESSION.USER.GLOBALIZATION) >
				<cfset SESSION.clone_IdSous_tete = idSousTete>
				<cfset SESSION.clone_IdInventaire_periode = idInventaire_periode>
				
			</cfif>
			
			
			<cfquery dbtype="query" name="totalFacture">
				SELECT SUM(MONTANT_LOC) TOTAL_FACTURE FROM SESSION.queryMontant
			</cfquery>
			
			<cfquery dbtype="query" name="totalFactureAbo">
				SELECT SUM(MONTANT_LOC) TOTAL_ABO FROM SESSION.queryMontant WHERE TYPE = 'Abonnements'
			</cfquery>
			
			<cfquery dbtype="query" name="totalFactureConso">
				SELECT SUM(MONTANT_LOC) TOTAL_CONSO FROM SESSION.queryMontant WHERE TYPE = 'Consommations'
			</cfquery>
			
			<cfquery dbtype="query" name="totalAppel">
				SELECT SUM(NOMBRE_APPEL) TOTAL_APPEL FROM SESSION.queryMontant WHERE TYPE = 'Consommations'
			</cfquery>
			
			<cfquery dbtype="query" name="totalVolume">
				SELECT SUM(VOLUME_MN) TOTAL_VOL FROM SESSION.queryMontant WHERE TYPE = 'Consommations'
			</cfquery>
			
			
			<cfset euro = "&nbsp;€">
			<cfset ht = "&nbsp;HT">
			<cfset totalAbo = DecimalFormat(totalFactureAbo.TOTAL_ABO) >
			<cfset totalConso = DecimalFormat(totalFactureConso.TOTAL_CONSO) >
			<cfset totalAFacturer = DecimalFormat(totalFacture.TOTAL_FACTURE) >
			<cfset totalAppel = totalAppel.TOTAL_APPEL >
			<cfset totalVolume = DecimalFormat(totalVolume.TOTAL_VOL) >
			
			<cfset totalAbo_ = totalFactureAbo.TOTAL_ABO>
			<cfset totalConso_ = totalFactureConso.TOTAL_CONSO>
			<cfset totalFacturation = totalFacture> <!--- ajouter .TOTAL_FACTURE à totalFacturation --->
			<cfset totalFacturation = totalFacturation.TOTAL_FACTURE>
			

			<cfset mesFactures &= '
					<div class="divFactureMensuelle_Facture" style=""> <!--- debut div select date factu --->
						<div align="left" class="divSelectDateFacture" style="margin-left:30px;font-size:13px;">
							<span style="font-weight:600;">Votre facture du</span>&nbsp;
							<select id="selectFacture" class="selectMaFacture" onchange="comboFacture();loadsablier();" >
			'>
			
			<!--- loop sur query periode pour le select (sauf premier choix) --->
			<cfloop query="SESSION.queryFact">
				<cfset currentRow = SESSION.queryFact.currentrow >
				
				<cfif SESSION.queryFact["NUMERO_FACTURE"][currentRow] neq "" >
					<cfset num_facture = SESSION.queryFact["NUMERO_FACTURE"][currentRow] >
				</cfif>
				<cfif SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] neq "" >
					<cfset idInventaire_periode_ = SESSION.queryFact["IDINVENTAIRE_PERIODE"][currentRow] >
				</cfif>
				
				
				<!--- formatage date pour le reste du select --->
				<cfset date_emission1 = SESSION.queryFact["DATE_EMISSION"][currentRow] ><!--- #queryFact.DATE_EMISSION# --->
				<cfset date_emission2 = LSDateFormat(date_emission1, "dd/mm/yyyy") >
				<cfset last_date = LSDateFormat(lastFact, "dd/mm/yyyy") >
				
				<!--- CREATION D'UNE STRUCTURE POUR LA CONSTRUCTION DE L'URL --->
				<cfif date_emission1 neq "">
					<cfset struct = {idPage = "3",
									sous_tete = "#sous_tete#",
									idSous_Tete = "#idSousTete#",
									idInventaire_periode = "#idInventaire_periode_#"
					} >
					<cfset urlString = createRefURL(struct)>
				</cfif>
				
				
				<cfif date_emission2 eq last_date >
					<cfset mesFactures &= '<option selected="selected" id="" value="#urlString#">#date_emission2# - n° de facture : #num_facture#</option>'>
				<cfelse>
					<cfset mesFactures &= '<option id="" value="#urlString#" >#date_emission2# - n° de facture : #num_facture#</option>'>
				</cfif>
						
			</cfloop>
			
			
			<cfset mesFactures &= '
							</select>
						</div> <!--- fin div select date factu --->
			'>
		
		
		<!--- --------------------------------------------- --->
		<!--- OU SI AUCUNE DATE DE FACTU DE MOINS DE 6 MOIS --->
		<!--- --------------------------------------------- --->
		<cfelse> 
		
		
			<!--- QUERY MONTANT FACTU --->
			<!--- si la STRUCTURE EN SESSION QUERYFACT n'existe pas ou si la variable de SESSION IDSOUS_TETE pas egale au parametre idsous_tete --->
			<cfif NOT isDefined("SESSION.queryMontant") ||
					 (isDefined("SESSION.clone_IdInventaire_periode") && SESSION.clone_IdInventaire_periode neq idInventaire_periode) ||
					 (isDefined("SESSION.clone_IdSous_tete") && SESSION.clone_IdSous_tete neq idSousTete) >

				<cfset SESSION.queryMontant = createObject("component","portail.class.classData.DataService").getMontantFactureByLigne(SESSION.PERIMETRE.IDRACINE_MASTER,idSousTete,idInventaire_periode,SESSION.USER.GLOBALIZATION) >
				<cfset SESSION.clone_IdSous_tete = idSousTete>
				<cfset SESSION.clone_IdInventaire_periode = idInventaire_periode>
				
			</cfif>
			
			
			<cfset mesFactures &= '
					<div class="divFactureMensuelle" style=""> <!---  debut div select date factu --->
						<div align="left" class="divSelectDateFacture" style="margin-left:30px;">
							<span style="font-weight:600;">Aucune facture de moins de 6 mois disponible</span>&nbsp;
							<!--- <select id="selectFacture" class="selectMaFacture" onchange="comboFacture();loadsablier();" >
								<option id="" value="" > - </option>
							</select> --->
						</div>
			'>
		</cfif><!--- FIN QUERY FACT --->
		
		
		
		<cfset mesFactures &= '
					<div class="divTableFacture2" style=""> <!--- debut div tableau montant factu --->
						<table class="tableFacture" cellspacing="0" cellpadding="0" style="" width="100%">
							<colgroup>
								<col width="60%"></col>
								<col width="40%"></col>
							</colgroup>
							<thead>
								<tr style="">
									<td colspan="2" style="border-top:0px;border-left:0px;border-right:0px;">#dateFact#</td>
								</tr>
							</thead>
							<tbody>
								<tr style="">
									<td style="">Montant total facture </td>
									<td align="center" style="">#totalAFacturer##euro##ht#</td>
								</tr>
								<tr style="">
									<td style="">Dont abonnements</td>
									<td align="center" style="">#totalAbo##euro##ht#</td>
								</tr>
								<tr style="">
									<td style="">Dont consommations </td>
									<td align="center" style="">#totalConso##euro##ht#</td>
								</tr>
							</tbody>
						</table>
					</div> <!--- fin div tableau montant factu --->
				</div><!--- fin div select date factu --->
				
			</div><!--- fin div contenant tableau montant et select --->
		'>
		
		
		<cfif totalAbo_ eq "">
			<cfset totalAbo_ = 0>
		</cfif>
		<cfif totalConso_ eq "">
			<cfset totalConso_ = 0>
		</cfif>
		<cfif totalFacturation eq "">
			<cfset totalFacturation = 1>
		</cfif>
		
		<!--- génération graphique --->
		<cfset aboP = val(totalAbo_) / val(totalFacturation) >
		<cfset consoP = val(totalConso_) / val(totalFacturation) >
		
		
		<cfif totalAbo_ neq 0 && totalConso_ neq 0>
			<cfset chart = createObject("component","portail.class.classData.DataService").getChartPie(aboP,consoP,dateGraph)>
		</cfif>
		
		<!--- GRAPHIQUE --->
		<cfset mesFactures &= '
						<div style="height:250px;width:100%;"> <!--- debut div contenant graphique --->
						
							<div class="" style="float:left;height:250px;width:490px;border:1px solid ##e4e4e4;">
								#chart#
							</div>
							
						</div> <!--- fin div contenant graphique --->
						
					<div class="clear" style=""></div>
				</div> <!--- fin div horizontal --->
		'>
		
		
		<!--- TABLES CONSO + ABO MENSUELS --->
		
		<!--- table abonnement mensuel --->
		
		<cfset mesFactures &= '
				<div class="divVerticale_droite" style=""> <!--- debut div vertical --->	
		           	<div class="divTableAboMensuel" style="" ><!--- debut div table abo --->
			           	<div class="intitule_dateFactu_conso" align="left" style="">#dateFactAbo#</div>
							<table cellpadding="0" cellspacing="0" width="100%">
								<thead>
								    <tr>
								    	<td>
								        	<table class="tableEnteteAbo" cellpadding="0" cellspacing="0" >
								        		<colgroup>
								        			<col ></col>
								        			<col ></col>
								        			<col ></col>
								        			<col ></col>
								        		</colgroup>
								        		<tr>
									                <td colspan="3" style="" class="td0_entete_abo">Abonnements</td>
									                <td style="" class="td0bis_entete_abo">&nbsp;</td>
									            </tr>
									        	<tr class="tableSousEnteteAbo" style="">
									                <td style="" class="td1_entete_abo">Produits</td>
									                <td style="" class="td2_entete_abo">Thème</td>
									                <td align="right" style="" class="td3_entete_abo">Montants</td>
									                <td style="" class="td4_entete_abo">&nbsp;</td>
									            </tr>
								            </table>
								      	</td>
								    </tr>
								</thead>
		'>
		
		
		<!--- POUR SCROLLER OU NON LE TABLEAU SELON LE NOMBRE D'ENREGISTRMENTS --->
		
		<cfset cptAbo = 0>
		<cfloop query = "SESSION.queryMontant">
			<cfif SESSION.queryMontant["TYPE"][SESSION.queryMontant.currentrow] eq "Abonnements" >
				<cfset cptAbo = cptAbo + 1>
			</cfif>
		</cfloop>
		
		<cfif cptAbo gt 0 && cptAbo lte 11 >
			<cfset scrollHauteur = 'style="height:auto;width:490px;"'>
			<cfset largeur = 'style="width:490px;"'>
			<cfset col = '<col></col>'>
			<cfset td = '<td class="td4_corps_abo" style="">&nbsp;</td>'>
			<cfset tdEmpty = ''>
		<cfelseif cptAbo eq 0>
			<cfset scrollHauteur = 'style="height:auto;width:490px;"'>
			<cfset largeur = 'style="width:490px;"'>
			<cfset col = '<col ></col>'>
			<cfset td = '<td class="td4_corps_abo" style="">&nbsp;</td>'>
			<cfset tdEmpty = '
					<tr class="color1">
						<td align="center" colspan="3" style="height:25px;width:480px;font-size:12px;">Aucune information</td>
						<td style="width:15px;"></td>
					</tr>
			'>
		<cfelse><!--- plus de 10 enregistrements --->
			<cfset scrollHauteur = 'style="height:243px;"'>
			<cfset largeur = 'style="height:243px;"'>
			<cfset col = ''>
			<cfset td = ''>
			<cfset tdEmpty = ''>
		</cfif>
		
		
		<cfset mesFactures &= '
				<tbody>
					<tr>
				    	<td>
					        <div class="scrollageAbo" #scrollHauteur#>
						        <table class="tableCorpsAbo" #largeur#  border="0" cellpadding="0" cellspacing="0">
									<colgroup>
					        			<col ></col>
					        			<col ></col>
					        			<col ></col>
					        			#col#
					        		</colgroup>
					        		#tdEmpty#
		'>
		
		
		<cfset color = "color1">
		
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Abonnements" >
				
				<cfset produit = "" >
				<cfset montant = "" >
				<cfset theme = "">
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset produit = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["THEME"][currentRow] neq "" >
					<cfset theme = SESSION.queryMontant["THEME"][currentRow] >
				</cfif>
				
				<cfset mesFactures &= '
											<tr class="#color#">
												<td class="td1_corps_abo" style="">#produit#</td>
												<td class="td2_corps_abo" style="">#theme#</td>
												<td align="right" class="td3_corps_abo" style="">#DecimalFormat(montant)##euro#</td>
												#td#
											</tr>
				'>
		
			</cfif>
		</cfloop>
		
		
		<cfset mesFactures &= '
								</table>
							</div>
						</td>
				</tbody>
		'>
		
		<cfset mesFactures &= '
					<tfoot>
					    <tr>
					    	<td>
					        	<table class="tableFooterAbo" cellpadding="0" cellspacing="0" >
					        		<colgroup>
					        			<col ></col><!--- width="550px" --->
					        			<col ></col><!--- width="280px" --->
					        			<col ></col><!--- width="20px" --->
					        			<col ></col>
					        		</colgroup>
						        	<tr class="" style="">
						                <td colspan="2" style="" class="td1_foot_abo">Total</td>
						                <td align="right" style="" class="td2_foot_abo">#totalAbo##euro#</td>
						                <td class="td3_foot_abo" style="">&nbsp;</td>
						            </tr>
					            </table>
					      	</td>
					    </tr>
					</tfoot>
		        </table>
	        </div><!--- fin div table abo --->
		'>
		
		
		<!--- table conso mensuelle --->
		
		
		<cfset mesFactures &= '
				
				<div class="divTableConsoMensuelle" style="" > <!--- debut div table conso --->
				<div class="intitule_dateFactu_conso" align="left" style="">#dateFactConso#</div>
						<table cellpadding="0" cellspacing="0" width="100%">
							<thead>
							    <tr>
							    	<td>
							        	<table class="tableEnteteConso" cellpadding="0" cellspacing="0" >
							        		<colgroup>
							        			<col ></col>
							        			<col ></col>
							        			<col ></col>
							        			<col ></col>
							        			<col ></col>
							        			<col ></col>
							        		</colgroup>
							        		<tr>
								                <td colspan="5" style="" class="td0_entete_conso">Consommations</td>
								                <td align="right" style="" class="td0bis_entete_conso">&nbsp;</td>
								            </tr>
								        	<tr class="tableSousEnteteConso" style="">
								                <td style="" class="td1_entete_conso">Consommations</td>
								                <td align="right" style="" class="td2_entete_conso">Qté</td>
								                <td align="right" style="" class="td3_entete_conso">Nb app.</td>
								                <td align="right" style="" class="td4_entete_conso">Vol.</td>
								                <td align="right" style="" class="td5_entete_conso">Montants</td>
												<td align="right" style="" class="td6_entete_conso">&nbsp;</td>
								            </tr>
							            </table>
							      	</td>
							    </tr>
							</thead>
		'>
		
		<!--- POUR SCROLLER OU NON LE TABLEAU SELON LE NOMBRE D'ENREGISTRMENTS --->
		
		<cfset cptConso = 0>
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				<cfset cptConso = #cptConso# + 1>
			</cfif>
		</cfloop>
		
		<cfif cptConso gt 0 && cptConso lte 11 >
			<cfset scrollHauteur2 = 'style="height:auto;width:490px;"'>
			<cfset largeur2 = 'style="width:490px;"'>
			<cfset col2 = '<col></col>'>
			<cfset td2 = '<td align="right" class="td6_corps_conso" style="">&nbsp;</td>'>
			<cfset tdTheme = '<td class="td7_corps_conso">&nbsp;</td>' >
			<cfset tdEmpty2 = ''>
		<cfelseif cptConso eq 0>
			<cfset scrollHauteur2 = 'style="height:auto;width:490px;"'>
			<cfset largeur2 = 'style="width:490px;"'>
			<cfset col2 = '<col ></col>'>
			<cfset td2 = '<td align="right" class="td6_corps_conso" style="">&nbsp;</td>'>
			<cfset tdTheme = '<td class="td7_corps_conso">&nbsp;</td>' >
			<cfset tdEmpty2 = '
					<tr class="color1">
						<td align="center" colspan="5" style="height:25px;width:480px;font-size:12px;">Aucune information</td>
						<td style="width:15px;"></td>
					</tr>
			'>
		<cfelse> <!--- plus de 10 enregistrements --->
			<cfset scrollHauteur2 = 'style="height:243px;"'>
			<cfset largeur2 = 'style="height:243px;"'>
			<cfset col2 = ''>
			<cfset tdTheme = '' >
			<cfset td2 = ''>
			<cfset tdEmpty2 = ''>
		</cfif>
		
		
		<cfset mesFactures &= '
				<tbody>
					<tr>
				    	<td>
					        <div class="scrollageConso" #scrollHauteur2#>
						        <table class="tableCorpsConso" #largeur2# cellpadding="0" cellspacing="0">
									<colgroup>
					        			<col ></col>
					        			<col ></col>
					        			<col ></col>
					        			<col ></col>
					        			<col ></col>
					        			#col2#
					        		</colgroup>
					        		#tdEmpty2#
		'>
		
		
		
		<cfset color = "color1">
		<cfset them1 = false>
		<cfset them2 = false>
		<cfset them3 = false>
		<cfset them4 = false>
		<cfset them5 = false>
		<cfset them6 = false>
		<cfset them7 = false>
		<cfset them8 = false>
		<cfset them9 = false>
		<cfset them10 = false>
		<cfset them11 = false>
		
		
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"Fixe et GSM")>
					<cfif #them1# eq false >
						<cfset them1 = true >
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">Fixe et GSM</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
			</cfif>
		</cfloop>
			
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"International")>
					<cfif #them2# eq false >
						<cfset them2 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">International</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
			</cfif>
		</cfloop>
				
				
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"Reçus depuis l''étranger ")>
					<cfif #them3# eq false >
						<cfset them3 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">Reçus depuis l''étranger </td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
			</cfif>
		</cfloop>
				
				
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"Emis depuis l''étranger")>
					<cfif #them4# eq false >
						<cfset them4 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">Emis depuis l''étranger</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
			</cfif>
		</cfloop>
				
				
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"N spéciaux ")> 
					<cfif #them5# eq false >
						<cfset them5 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">N° spéciaux </td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
			</cfif>
		</cfloop>
				
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"SMS")> 
					<cfif #them6# eq false >
						<cfset them6 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">SMS</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
			</cfif>
		</cfloop>
				
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"MMS")> 
					<cfif #them7# eq false >
						<cfset them7 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">MMS</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
				
			</cfif>
		</cfloop>
		
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"Data National")> 
					<cfif #them8# eq false >
						<cfset them8 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">Data National</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
				
			</cfif>
		</cfloop>
		
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"Data Roaming")> 
					<cfif #them9# eq false >
						<cfset them9 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">Data Roaming</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
				
			</cfif>
		</cfloop>
		
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"Data Divers")> 
					<cfif #them10# eq false >
						<cfset them10 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">Data Divers</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
				
			</cfif>
		</cfloop>
		
		<cfloop query = "SESSION.queryMontant">
			<cfset currentRow = SESSION.queryFact.currentrow >
			
			<cfif SESSION.queryMontant["TYPE"][currentRow] eq "Consommations" >
				
				<cfset conso = "" >
				<cfset montant = "" >
				<cfset qte = "" >
				<cfset nbAppel = "" >
				<cfset montant = "" >
				
				<cfif color eq "color1">
					<cfset color = "color2">
				<cfelseif color eq "color2">
					<cfset color = "color1">
				</cfif>
				
				<cfif SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] neq "" >
					<cfset conso = SESSION.queryMontant["LIBELLE_PRODUIT"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["VOLUME_MN"][currentRow] neq "" >
					<cfset vol = SESSION.queryMontant["VOLUME_MN"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["QTE"][currentRow] neq "" >
					<cfset qte = SESSION.queryMontant["QTE"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["NOMBRE_APPEL"][currentRow] neq "" >
					<cfset nbAppel = SESSION.queryMontant["NOMBRE_APPEL"][currentRow] >
				</cfif>
				<cfif SESSION.queryMontant["MONTANT_LOC"][currentRow] neq "" >
					<cfset montant = SESSION.queryMontant["MONTANT_LOC"][currentRow] >
				</cfif>
				
				<cfif NOT compare(SESSION.queryMontant.THEME,"Remises et régul")> 
					<cfif #them11# eq false >
						<cfset them11 = true>
						<cfset mesFactures &= '
												<tr><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
													<td colspan="5" style="" class="themeConso">Remises et régul</td>
													#tdTheme#
												</tr>
						'>
					</cfif>
					<cfset mesFactures &= '
											<tr class="#color#"><!--- text-overflow:ellipsis;overflow:hidden;white-space:nowrap; --->
												<td class="td1_corps_conso" style="">#conso#</td>
												<td align="right" class="td2_corps_conso" style="">#qte#</td>
												<td align="right" class="td3_corps_conso" style="">#nbAppel#</td>
												<td align="right" class="td4_corps_conso" style="">#DecimalFormat(vol)#</td>
												<td align="right" class="td5_corps_conso" style="">#DecimalFormat(montant)##euro#</td>
												#td2#
											</tr>
					'>
				</cfif>
				
			</cfif>
		</cfloop>
		
		
		<cfset mesFactures &= '
								</table>
							</div>
						</td>
					</tr>
				</tbody>
		'>
		
		<cfset mesFactures &= '
							<tfoot>
							    <tr>
							    	<td>
							        	<table class="tableFooterConso" cellpadding="0" cellspacing="0" >
								        	<tr class="" style="">
								                <td colspan="2" style="" class="td1_foot_conso">Total</td>
								                <td align="right" style="" class="td2_foot_conso">#totalAppel#</td>
								                <td align="right" style="" class="td3_foot_conso">#totalVolume#</td>
								                <td align="right" style="" class="td4_foot_conso">#totalConso##euro#</td>
												<td align="right" class="td5_foot_conso" style=""></td>
								            </tr>
							            </table>
							      	</td>
							    </tr>
							</tfoot>
				        </table>
			        </div> <!--- debut div table abo --->
			        
			    <div class="clear" style=""></div>
			</div><!--- fin div verticale droite --->
			<br /><br />
		'>
		
		<cfreturn mesFactures >
	</cffunction>
	
	
</cfcomponent>