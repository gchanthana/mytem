<cfcomponent output="false" extends="fr.consotel.consoview.inventaire.contrats.Contrat">
	
	<cffunction name="getDetailContratAbonnementMobile" access="remote" output="false" returntype="query">
		<cfargument name="idContrat" type="Numeric" required="true" />
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.GET_CONTRATABO">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcontrat" value="#idContrat#"/>
			<cfprocresult name="qDetailContrat">		  
		</cfstoredproc>
		
		<cfargument name="id" type="Contrat" required="true"/>
		
		
		<cfscript>
			obj = createObject("component", "fr.consotel.consoview.inventaire.contrats.ContratAbonnementMobile").init();
			
			if(qRead.recordCount gt 0)
			{	
				obj.setIDCONTRAT(qRead.IDCONTRAT);
				obj.setIDTYPE_CONTRAT(qRead.IDTYPE_CONTRAT);
				obj.setIDFOURNISSEUR(qRead.IDFOURNISSEUR);
				obj.setREFERENCE_CONTRAT(qRead.REFERENCE_CONTRAT);
				obj.setDATE_SIGNATURE(qRead.DATE_SIGNATURE);
				obj.setDATE_ECHEANCE(qRead.DATE_ECHEANCE);
				obj.setCOMMENTAIRE_CONTRAT(qRead.COMMENTAIRE_CONTRAT);
				obj.setMONTANT_CONTRAT(qRead.MONTANT_CONTRAT);
				obj.setDUREE_CONTRAT(qRead.DUREE_CONTRAT);
				obj.setTACITE_RECONDUCTION(qRead.TACITE_RECONDUCTION);
				obj.setDATE_DEBUT(qRead.DATE_DEBUT);
				obj.setPREAVIS(qRead.PREAVIS);
				obj.setDATE_DENONCIATION(qRead.DATE_DENONCIATION);
				obj.setIDCDE_CONTACT_SOCIETE(qRead.IDCDE_CONTACT_SOCIETE);
				obj.setLOYER(qRead.LOYER);
				obj.setPERIODICITE(qRead.PERIODICITE);
				obj.setMONTANT_FRAIS(qRead.MONTANT_FRAIS);
				obj.setNUMERO_CLIENT(qRead.NUMERO_CLIENT);
				obj.setNUMERO_FOURNISSEUR(qRead.NUMERO_FOURNISSEUR);
				obj.setNUMERO_FACTURE(qRead.NUMERO_FACTURE);
				obj.setCODE_INTERNE(qRead.CODE_INTERNE);
				obj.setBOOL_CONTRAT_CADRE(qRead.BOOL_CONTRAT_CADRE);
				obj.setBOOL_AVENANT(qRead.BOOL_AVENANT);
				obj.setID_CONTRAT_MAITRE(qRead.ID_CONTRAT_MAITRE);
				obj.setFRAIS_FIXE_RESILIATION(qRead.FRAIS_FIXE_RESILIATION);
				obj.setMONTANT_MENSUEL_PEN(qRead.MONTANT_MENSUEL_PEN);
				obj.setDATE_RESILIATION(qRead.DATE_RESILIATION);
				obj.setPENALITE(qRead.PENALITE);
				obj.setDESIGNATION(qRead.DESIGNATION);
				obj.setDATE_RENOUVELLEMENT(qRead.DATE_RENOUVELLEMENT);
				obj.setOPERATEURID(qRead.OPERATEURID);
				obj.setIDCOMPTE_FACTURATION(qRead.IDCOMPTE_FACTURATION);
				obj.setIDSOUS_COMPTE(qRead.IDSOUS_COMPTE);
				obj.setNUMERO_CONTRAT(qRead.NUMERO_CONTRAT);
				obj.setIDRACINE(qRead.IDRACINE);
				obj.setDATE_ELLIGIBILITE(qRead.DATE_ELLIGIBILITE);
				obj.setDUREE_ELLIGIBILITE(qRead.DUREE_ELLIGIBILITE);
			}
			return obj;
		</cfscript>
		
	</cffunction>
	
	
	
	<!--- <cffunction name="getUdapteContratAbonnementMobile" access="remote" output="false" returntype="query">
		<cfargument name="idContrat" type="Numeric" required="true" />
				
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_CONTRAT.GET_CONTRATABO">	
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idpool" value="#idpool#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_clef" value="#clef#"/>
			<cfprocresult name="listeContrats">		  
		</cfstoredproc>
		
		<cfreturn listeContrats/>
	</cffunction> --->
	
</cfcomponent>