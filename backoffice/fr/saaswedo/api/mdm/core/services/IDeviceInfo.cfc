<cfinterface author="Cedric" displayname="fr.saaswedo.api.mdm.core.services.IDeviceInfo" hint="Décrit les fonctions d'accès aux infos des devices">
	<cffunction name="deviceExists" returntype="Boolean" hint="Retourne TRUE le device existe dans le MDM et FALSE sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>

	<cffunction name="getLastUser" returntype="String" hint="Retourne le login d'enrollment ou une chaine vide s'il n'est pas enrollé ou n'existe pas">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>

	<cffunction name="getManagedStatus" returntype="String" hint="Retourne le statut du device. Toujours UNMANAGED() si deviceExists() vaut FALSE">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>
	
	<cffunction name="getAllDevices" returntype="Array"
	hint="Retourne la liste des devices du MDM. Chaque élément est une structure contenant au moins les clés serialNumber et IMEI">
	</cffunction>
	
	<cffunction name="getDeviceInfo" returntype="Struct" hint="Retourne une structure contenant au moins les clés suivantes :
	<br>- GENERAL (Struct): Serial_Number (Numéro de série),IMEI (IMEI),STATUS (Statut du device),OS,firstConnectionDate,lastAuthDate
	<br>- PROPERTIES (Array): Chaque élément est une structure contenant les clés NAME,VALUE
	<br>- SOFTWARE (Array): Chaque élément est une structure contenant les clés NAME,AUTHOR,VERSION,SIZE,InstallTimeStamp
	<br>Toute valeur de type Date peut etre remplacée par une chaine vide lorsqu'elle n'est pas définie (e.g Valeur null)
	<br><b>Cette fonction peut lever une exception si le device n'existe pas ou n'est pas enrollé e.g getManagedStatus() vaut UNMANAGED()</b>">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
	</cffunction>
</cfinterface>