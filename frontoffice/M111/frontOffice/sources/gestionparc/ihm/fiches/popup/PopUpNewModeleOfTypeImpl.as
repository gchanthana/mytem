package gestionparc.ihm.fiches.popup
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.terminal.TerminalServices;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * Classe concernant l'ajout de nouveaux modeles
	 */
	[Bindable]
	public class PopUpNewModeleOfTypeImpl extends TitleWindow
	{
		public var tiModele					:TextInput;
		
		public var tiCategorie				:Label;
		public var tiType					:Label;
		public var cbRevendeur				:ComboBox;
		public var cbMarque					:ComboBox;
		
		public var btnValid					:Button;
		public var btnCancel				:Button;
		
		public var objCategorie				:Object;	// CATEGORIE_EQUIPEMENT  -  IDCATEGORIE_EQUIPEMENT 
		public var objType					:Object;	// TYPE_EQUIPEMENT 	-  IDTYPE_EQUIPEMENT 
		
		public var myServicesTerm 			:TerminalServices = new TerminalServices();
		
		private var objResult : Object;
		
		public function PopUpNewModeleOfTypeImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		private function init(event:FlexEvent):void
		{
			initDisplay()
			initListener();
		}
		private function initDisplay():void
		{
			if(CacheDatas.listeRevendeur)
			{
				onListeRevendeurLoadedHandler(null);
			}
			tiModele.maxChars = ( objType.IDTYPE_EQUIPEMENT == 70 ) ? 15 : 50;
		}
		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE,closePopup);
			btnCancel.addEventListener(MouseEvent.CLICK,cancelPopup);
			btnValid.addEventListener(MouseEvent.CLICK,validClickHandler);
			
			myServicesTerm.myDatas.addEventListener(gestionparcEvent.ADDNEW_MODELE_LOADED,modeleAddedHandler);
		}
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		private function validClickHandler(event:MouseEvent):void
		{
			objResult = new Object();
			objResult.libelleModele = tiModele.text;
			objResult.idTypeEquipement = objType.IDTYPE_EQUIPEMENT;
			objResult.idrevendeur = cbRevendeur.selectedItem.IDREVENDEUR;
			objResult.idMarque = cbMarque.selectedItem.IDFOURNISSEUR;
			objResult.prix = 0;
			objResult.reference = "";
			objResult.referenceDistributeur = "";
					
			myServicesTerm.addNewModele(objResult);
		}
		private function onModeleAddedHandler(evt:gestionparcEvent):void
		{
			objResult.IDEQUIPEMENT = parseInt(evt.obj.toString());	
			dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED,objResult));
			closePopup(null);
		}
		private function onListeRevendeurLoadedHandler(event:gestionparcEvent):void
		{
			cbRevendeur.dataProvider = CacheDatas.listeRevendeur;
		}
		private function modeleAddedHandler(evt:gestionparcEvent):void
		{
			objResult.IDEQUIPEMENT = parseInt(evt.obj.toString());
			
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString("M111","Nouveau_MODELE_added"),Application.application as DisplayObject);
			dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED,objResult));
			closePopup(null);
		}
	}
}