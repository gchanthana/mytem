<cfinterface author="Cedric" hint="Interface des fonctionnalités d'inventaire MDM">
	<cffunction name="getMdmServer" returntype="fr.saaswedo.api.mdm.MdmServer" hint="Retourne les infos serveur MDM actuellement utilisées"> 
	</cffunction>
	
	<cffunction name="isManaged" returntype="Boolean" hint="Retourne TRUE si le device est géré par le MDM (enrolled)">
		<cfargument name="serial" type="String" required="true" hint="Numéro de série du device">
		<cfargument name="imei" type="String" required="false" default="" hint="IMEI du device">
	</cffunction>
</cfinterface>