package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.MouseEvent;
	
	import gestionparc.entity.TrancheVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.popup.PopUpAddOrEditTrancheIHM;
	import gestionparc.services.ligne.LigneServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations de l'onglet cablage/SDA
	 * pour les lignes fixe de type T2, T0, ANA
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 23.11.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class ContentCablageSdaTypeT2T0AnaImpl extends HBox
	{
		// VARIABLES------------------------------------------------------------------------------

		// IHM components
		public var dgSDA:DataGrid;

		public var lbNbTranchesSDA:Label;

		public var btNewTranche:Button;

		public var tiPaire2:TextInput;
		public var tiPaire1:TextInput;
		public var tiReglette2:TextInput;
		public var tiReglette1:TextInput;
		public var tiAmorce:TextInput;

		public var myServiceLigne:LigneServices;

		// VARIABLES------------------------------------------------------------------------------

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ContentCablageSdaTypeT2T0AnaImpl.
		 * </pre></p>
		 */
		public function ContentCablageSdaTypeT2T0AnaImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #initListener()
		 * @see #initDisplay()
		 */
		private function init(event:FlexEvent):void
		{
			initListener();
			initDisplay();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initListener():void
		{
			btNewTranche.addEventListener(MouseEvent.CLICK, onBtNewTrancheClickHandler);
			this.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV, onBtDeleteTrancheClickHandler)
			this.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onBtEditTrancheClickHandler)
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise l'affichage de cette IHM.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initDisplay():void
		{
			dgSDA.dataProvider=new ArrayCollection();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met a jour l'affichage du nombre de tranche du grid
		 * </pre></p>
		 *
		 * @return void
		 */
		private function updateNbTranche():void
		{
			lbNbTranchesSDA.text=(myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection).length + ResourceManager.getInstance().getString('M111', '_tranches_SDA');
			dgSDA.dataProvider=myServiceLigne.myDatas.ligne.listeTranche;
		}

		public function setInfos():void
		{
			tiAmorce.text=myServiceLigne.myDatas.ligne.amorce.toString();

			tiReglette1.text=myServiceLigne.myDatas.ligne.reglette1.toString();
			tiReglette2.text=myServiceLigne.myDatas.ligne.reglette2.toString();

			tiPaire1.text=myServiceLigne.myDatas.ligne.paire1.toString();
			tiPaire2.text=myServiceLigne.myDatas.ligne.paire2.toString();

			if (myServiceLigne.myDatas.ligne.listeTranche != null && myServiceLigne.myDatas.ligne.listeTranche.length > 0)
			{
				dgSDA.dataProvider=myServiceLigne.myDatas.ligne.listeTranche;
				myServiceLigne.myDatas.ligne.listeTranche.refresh()
			}
			updateNbTranche()
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Apres l'ecoute de l'événement CLICK sur le bouton "btNewTranche".
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onBtNewTrancheClickHandler(event:MouseEvent):void
		{
			var popupTranche:PopUpAddOrEditTrancheIHM=new PopUpAddOrEditTrancheIHM();
			popupTranche.addEventListener(gestionparcEvent.POPUP_TRANCHE_VALIDATED, addTrancheToGrid);
			popupTranche.isNewTranche=true;

			PopUpManager.addPopUp(popupTranche, this.parent.parent, true);
			PopUpManager.centerPopUp(popupTranche);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Apres l'ecoute de l'événement CLICK sur le bouton du grid permettant d'editer une tranche
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onBtEditTrancheClickHandler(event:gestionparcEvent):void
		{
			var popupTranche:PopUpAddOrEditTrancheIHM=new PopUpAddOrEditTrancheIHM();
			popupTranche.addEventListener(gestionparcEvent.POPUP_TRANCHE_VALIDATED, editTrancheToGrid);
			popupTranche.isNewTranche=false;

//			var objToEdit : Object = new Object();
//			objToEdit.libelle = dgSDA.selectedItem.libelle;
//			objToEdit.typetranche = dgSDA.selectedItem.typeTranche;
//			objToEdit.nbNum = dgSDA.selectedItem.nbNum;
//			objToEdit.numDepart = dgSDA.selectedItem.numDepart;
//			objToEdit.numFin = dgSDA.selectedItem.numFin;
			popupTranche.objToEdit=dgSDA.selectedItem as TrancheVO;

			PopUpManager.addPopUp(popupTranche, this.parent.parent, true);
			PopUpManager.centerPopUp(popupTranche);
		}


		protected function formatNumber(item:Object, column:DataGridColumn):String
		{
			var result:String="";

			if (item.hasOwnProperty(column.dataField) && item[column.dataField])
			{
				result=ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
			}


			return result;
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Apres l'ecoute de l'événement CLICK sur le bouton du grid permettant de supprimer une tranche
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onBtDeleteTrancheClickHandler(event:gestionparcEvent):void
		{
			var lenDataProvider:int=(myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection).length;

			for (var i:int=0; i < lenDataProvider; i++)
			{
				if ((myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection)[i] == dgSDA.selectedItem)
				{
					(myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection).removeItemAt(i);
					break;
				}
			}
			updateNbTranche();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ajoute une tranche au grid.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * */
		private function addTrancheToGrid(event:gestionparcEvent):void
		{
			// ajout de la nouvelle tranche (avec les mise a jour)
			(myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection).addItem(event.obj);
			updateNbTranche();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Modifie une tranche du grid.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function editTrancheToGrid(event:gestionparcEvent):void
		{
			var lenDataProvider:int=(myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection).length;

			// suppression de la tranche selectionnee
			for (var i:int=0; i < lenDataProvider; i++)
			{
				if ((myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection)[i] == dgSDA.selectedItem)
				{
					(myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection).removeItemAt(i);
					break;
				}
			}
			// ajout de la nouvelle tranche (avec les mise a jour)
			(myServiceLigne.myDatas.ligne.listeTranche as ArrayCollection).addItem(event.obj);
			updateNbTranche();
		}

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

	}
}
