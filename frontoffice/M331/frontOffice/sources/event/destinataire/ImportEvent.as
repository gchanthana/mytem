package event.destinataire
{
	import flash.events.Event;

	public class ImportEvent extends Event
	{
		public static const UUID_EVENT:String 	  						  = 'UUIDEvent'; 
		public static const INFOS_CSV_EVENT:String 						  = 'InfosCsvEvent'; 
		public static const IMPORT_LISTE_DESTINATRAIRE_EVENT:String 	  = 'ImportListeDestinatrairEvent';
		public static const BTN_IMPORT_DESTINATRAIR_ACTIF_EVENT:String 	  = 'btnImportDestinatrairActifEvent';
		public static const BTN_IMPORT_DESTINATRAIR_NOACTIF_EVENT:String  = 'btnImportDestinatrairNoActifEvent';
				
				
		public function ImportEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ImportEvent(type,bubbles,cancelable);
		}
		
	}
}