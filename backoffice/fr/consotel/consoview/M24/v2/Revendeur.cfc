<cfcomponent name="Constructeur">
		<cfset PATH_LOW_IMAGES = "/product/low/">
		<cfset PATH_THUMB_IMAGES = "/product/thumb/">
	
		<cffunction name="addEquipementRevendeur" access="remote" returntype="struct" description="add equip in catalogue revendeur">	
			<cfargument name="libelle" type="string" required="true">
			<cfargument name="reference_constr" type="string" required="true">
			<cfargument name="reference_distrib" type="string" required="true">
			<cfargument name="idConstructeur" type="numeric" required="true">
			<cfargument name="idType" type="numeric" required="true">
			<cfargument name="idRevendeur" type="numeric" required="true">
			<cfargument name="insertInConstr" type="numeric" required="true">		
			<cfargument name="prixCat" type="numeric" required="true">
			<cfargument name="tarif1" type="numeric" required="true">
			<cfargument name="tarif2" type="numeric" required="true">
			<cfargument name="tarif3" type="numeric" required="true">
			<cfargument name="tarif4" type="numeric" required="true">
			<cfargument name="tarif5" type="numeric" required="true">
			<cfargument name="tarif6" type="numeric" required="true">
			<cfargument name="tarif7" type="numeric" required="true">
			<cfargument name="tarif8" type="numeric" required="true">
			<cfargument name="dateValidite"	type="String" required="false">
			<cfargument name="delaiAlert" type="numeric" required="false">
			<cfargument name="boolSuspendu" type="numeric" required="false">
			<cfargument name="listClt" type="array" required="false">
			<cfargument name="path_low" type="string" required="false" default="">
			<cfargument name="path_thumb" type="string" required="false" default="">
			<cfargument name="comercial_comment" type="string" required="false" default="no comment">
			<cfargument name="reference_interne" type="string" required="false">
			<cfargument name="imageByUser" type="numeric" required="true" default="0" >

		<cfif imageByUser EQ 1 >
			<cfset mypathlow = "<![CDATA[" & PATH_LOW_IMAGES & #Arguments.path_low# & "]]>">
			<cfset mypaththumb = "<![CDATA[" & PATH_THUMB_IMAGES & #Arguments.path_thumb# & "]]>">
			<cfset mypromotion = "<![CDATA[" & #Arguments.comercial_comment# & "]]>">
			<cfxml variable="xmlEqpt">
				<cfoutput>
					<devices>	
						<device>
							<model>#Arguments.libelle#</model>
							<deviceType>#Arguments.idType#</deviceType>
							<manufacturer>#Arguments.idConstructeur#</manufacturer>
							<promotion>#mypromotion#</promotion>
							<refmanufacturer>#Arguments.reference_constr#</refmanufacturer>
							<refdistributor>#Arguments.reference_distrib#</refdistributor>
							<refInternal>#Arguments.reference_interne#</refInternal>
							<pricecat>#Arguments.prixCat#</pricecat>
							<price2>#Arguments.tarif2#</price2>
							<price3>#Arguments.tarif3#</price3>
							<price4>#Arguments.tarif4#</price4>
							<price5>#Arguments.tarif5#</price5>
							<price6>#Arguments.tarif6#</price6>
							<price7>#Arguments.tarif7#</price7>
							<price8>#Arguments.tarif8#</price8>
							<dateValid>#Arguments.dateValidite#</dateValid>
							<delaiAlert>#Arguments.delaiAlert#</delaiAlert>
							<suspended>#Arguments.boolSuspendu#</suspended>
							<pathlow>#mypathlow#</pathlow>
							<paththumb>#mypaththumb#</paththumb>
						</device>
					</devices>
				</cfoutput>
			</cfxml>
		<cfset iduser 	= session.user.clientaccessid>
		<cfset idracine = session.perimetre.id_groupe>
		

			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M24.uploadDevices">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#iduser#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(listClt,',')#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#toString(xmlEqpt)#">	
				
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<!---cfmail from="soufiane.ghenimi@saaswedo.com" subject="Test Email" to="soufiane.ghenimi@saaswedo.com" type="html" >
				iduser : <cfdump var="#iduser#" ><br/>
				idracine : <cfdump var="#idracine#" ><br/>
				idRevendeur : <cfdump var="#idRevendeur#">
				listClt : <cfdump var="#arraytoList(listClt)#" ><br/>
				xmlEqpt : <cfdump var="#toString(xmlEqpt)#" ><br/>
			</cfmail--->
			<cfset myStruct=structNew()>
			<cfset myStruct.IDEQUIP = 100>
	 		<cfset myStruct.REF = 100>
			<cfreturn myStruct>
		<cfelse>
				<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M24.addEquipementRevendeur_v2">	
					<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference_constr#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference_distrib#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRevendeur#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#insertInConstr#" null="false">				
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#prixCat#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif1#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif2#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif3#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif4#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif5#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif6#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif7#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#tarif8#" null="false">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#path_low#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#path_thumb#">
					<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#comercial_comment#">
					
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
					<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="ref">
				</cfstoredproc> 
				
				<cfset myStruct=structNew()>
				<cfset myStruct.IDEQUIP = p_retour>
				<cfset myStruct.REF = ref>
				<cfreturn myStruct>
		</cfif>
	</cffunction>

	<cffunction name="rechercheImageFromLibrary" access="remote" returntype="query" description="search photo from Icecat library">
			<cfargument name="libelle" type="string" required="true">
			<cfargument name="idType_equipement" type="numeric" required="true">
			<cfargument name="idConstructeur" type="numeric" required="true">
			<cfargument name="startIndex" type="numeric" required="true">
			<cfargument name="nombreParPage" type="numeric" required="true">
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M24.getImageLibrary">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType_equipement#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#startIndex#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="3000" null="false">
								
				<cfprocresult name="p_result">
			</cfstoredproc>
			
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="updateEquipementRevendeur" access="remote" returntype="any" description="update equip in catalogue constructeur">
		<cfargument name="id" type="string" required="true">
		<cfargument name="libelle" type="string" required="true">
		<cfargument name="reference_constr" type="string" required="true">
		<cfargument name="reference_distrib" type="string" required="true">
		<cfargument name="idType" type="numeric" required="true">
		<cfargument name="tarif1" type="numeric" required="true">
		<cfargument name="tarif2" type="numeric" required="true">
		<cfargument name="tarif3" type="numeric" required="true">
		<cfargument name="tarif4" type="numeric" required="true">
		<cfargument name="tarif5" type="numeric" required="true">
		<cfargument name="tarif6" type="numeric" required="true">
		<cfargument name="tarif7" type="numeric" required="true">
		<cfargument name="tarif8" type="numeric" required="true">	
		<cfargument name="idConstructeur" type="numeric" required="true">
		<cfargument name="prixCat" type="numeric" required="true">
		<cfargument name="dateValidite"	type="String" required="true">
		<cfargument name="delaiAlert" type="numeric" required="true">
		<cfargument name="boolSuspendu" type="numeric" required="true">	
		<cfargument name="listClt" type="Array" required="true">
		<cfargument name="path_low" type="string" required="false" default="">
		<cfargument name="path_thumb" type="string" required="false" default="">
		<cfargument name="comercial_comment" type="string" required="false" default="no comment">
		<cfargument name="reference_interne" type="string" required="false">
		<cfargument name="imageByUser" type="numeric" required="true" default="0" >


		<cfif imageByUser EQ 1 >
			<cfset mypathlow = PATH_LOW_IMAGES & #Arguments.path_low# >
			<cfset mypaththumb = PATH_THUMB_IMAGES & #Arguments.path_thumb#>
		<cfelse>
			<cfset mypathlow = #Arguments.path_low# >
			<cfset mypaththumb = #Arguments.path_thumb#>
		</cfif>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M24.updateEquRevendeur_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference_constr#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference_distrib#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif1#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif2#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif3#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif4#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif5#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif6#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif7#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#tarif8#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#prixCat#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#dateValidite#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#delaiAlert#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#boolSuspendu#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#arraytoList(listClt,',')#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#mypathlow#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#mypaththumb#">
				<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#comercial_comment#">
						
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			
			<cfreturn p_retour>
		
</cffunction>

</cfcomponent>
