package univers.parametres.perimetres.mainComponent
{
	import flash.events.Event;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class ArbreficheSite extends ArbreficheSiteIHM
	{
		public var _idgeo:String;
		
		public function ArbreficheSite()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(event:Event):void
		{
			this.addEventListener(CloseEvent.CLOSE, closeWin);
			arbreSite._idgeo = _idgeo;
			arbreSite.refreshTree();
			//arbreSite.initDP(18153);
		}
		
		protected function closeWin(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
	}
}