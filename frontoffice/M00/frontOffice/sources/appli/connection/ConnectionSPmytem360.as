package appli.connection
{
	import mx.core.Container;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import entity.AppParams;
	 
	import general.abstract.AbstractConnection;
	
	import interfaceClass.IConnection;
	
	public class ConnectionSPmytem360 extends AbstractConnection implements IConnection
	{
		public function ConnectionSPmytem360(progress:ConsoProgressBar)
		{
			super(progress);
		}
		
		override public function MustBeDisplay():Boolean
		{
			return false;
		}
		
		override public function goLogin(param:AppParams, container:Container = null):void
		{	
			super.goLogin(param,container);
			loginService.login(params.token,params.mdp,params.codeApp,params.codeConnection,params.codeLangue,params.seSouvenir);
		}
	}
}