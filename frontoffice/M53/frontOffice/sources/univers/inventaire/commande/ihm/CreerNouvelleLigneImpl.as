package univers.inventaire.commande.ihm
{
	import composants.tb.effect.EffectProvider;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.commande.system.ArticleEvent;
	
	[Bindable]
	public class CreerNouvelleLigneImpl extends CreerArticleImpl
	{
		
	 	
		private var dueDate:Date = new Date(new Date().getTime() + (10 * DateFunction.millisecondsPerDay));
		public var selectableRange:Object = {rangeStart:dueDate};
		
		private var _lignesUtils : LignesUtils = new LignesUtils();
		public function get lignesUtils():LignesUtils
		{
			return _lignesUtils;
		}
		
		
		
		
		
		/*------------------
		
			Constructor
		
		/*--------------------*/		
		public function CreerNouvelleLigneImpl()
		{
			super();
			lignesUtils.genererNumeroLigneUnique();	
		}
		
				
		override protected function commitProperties():void
		{	
			super.commitProperties();
					
			if(article != null)
			{
				cboEngagementEq.selectedIndex = ConsoviewUtil.getIndexByLabel(cboEngagementEq.dataProvider as ArrayCollection,"value",article.dureeEngagementEquipement.toString()); 
				cboEngagementRes.selectedIndex = ConsoviewUtil.getIndexByLabel(cboEngagementRes.dataProvider as ArrayCollection,"value",article.dureeEngagementRessources.toString());
				lignesUtils.listeNumerosValides.push(article.sousTete.toString());	
			}			
		}
		
		override protected function txtFiltreChangeHandler(event:Event): void
	    {		
	    	if(dgListe.dataProvider)
	    	{	
	    		(dgListe.dataProvider as XMLListCollection).filterFunction = listeEquipementsFilterFunction;
	    		(dgListe.dataProvider as XMLListCollection).refresh();
	    	}
	    }
	    
	    
	    protected function txtFiltreRessourcesChangeHandler(event:Event): void
	    {		
	    	if(dgListeRessources.dataProvider)
	    	{	
	    		(dgListeRessources.dataProvider as XMLListCollection).filterFunction = listeRessourcesFilterFunction;
	    		(dgListeRessources.dataProvider as XMLListCollection).refresh();
	    	}
	    }
	    
	    
	    protected function listeRessourcesFilterFunction(item:Object):Boolean
	    {	
	    	
		    var rfilter:Boolean = true;
		    var node:XML = XML(item);
		    
		    // Filtre sur le libelle des équipements
		    if (node.libelle != undefined)
		    {  
		        rfilter = rfilter && (String(node.libelle ).toLowerCase().search(txtFiltreRessources.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le libelle du type d équipements
		    if (node.theme != undefined)
		    {  
		        rfilter = rfilter && (String(node.type).toLowerCase().search(txtFiltreRessources.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le prix
		    if (node.prix != undefined)
		    {  
		        rfilter = rfilter && (String(node.prix).toLowerCase().search(txtFiltreRessources.text.toLowerCase()) != -1);
		    }		    
		    return rfilter;
	    }
	    
	    protected function listeEquipementsFilterFunction(item:Object):Boolean
	    {	
	    	
		    var rfilter:Boolean = true;
		    var node:XML = XML(item);
		    
		    // Filtre sur le libelle des équipements
		    if (node.libelle != undefined)
		    {  
		        rfilter = rfilter && (String(node.libelle ).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le libelle du type d équipements
		    if (node.type != undefined)
		    {  
		        rfilter = rfilter && (String(node.type).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }
		    
		    // Filtre sur le prix
		    if (node.prix != undefined)
		    {  
		        rfilter = rfilter && (String(node.prix).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1);
		    }		    
		    return rfilter;
	    }
	    
	  
		
		override protected function btValiderClickHandler(event:MouseEvent):void
		{
			var boolRessources:Boolean = false;
			var boolEquipement:Boolean = false;
			var boolPortage:Boolean = false;
			var message:String="\n";
			
			if(XMLList(article.article.equipements.equipement).length() >0)			
			{
				boolEquipement = true
			}
			else
			{
				boolEquipement = false
			}
			
			if(XMLList(article.article.ressources.ressource).length() >0)
			{
				boolRessources = true
			}
			else
			{	
				boolRessources = false	
			}
			
			if(cbxPortage.selected)
			{
				if(txtCodeRIO.text != "" || dcDatePortage.selectedDate != null)
				{					
					boolPortage = true
				}
				else
				{
					boolPortage = false
				}
			}
			else
			{
				boolPortage = true
			}
			 
				
			if ((boolEquipement || boolRessources) && boolPortage)
			{
				article.sousTete = txtNumLigne.text;
			
				article.codeInterne = txtCodeInterne.text;
				
				article.codeRio = txtCodeRIO.text;
				
				if(dcDatePortage.selectedDate != null)
				{
					article.date_portage = dcDatePortage.selectedDate;
				}
				
				if(dcDateFPC && dcDateFPC.selectedDate != null)
				{
					article.fpc = dcDateFPC.selectedDate;
				}
				
				//la duree d'engagement pour le contrat des ressources
				if(cbxContratRessources.selected && cboEngagementRes.selectedItem != null)
				{
					article.dureeEngagementRessources = Number(cboEngagementRes.selectedItem.value);
					article.libelleContratRes =  txtDesignationContratRes.text;	
				}
				else
				{
					article.dureeEngagementRessources = -1;
				}
				
				if(cbxContratEq.selected && cboEngagementEq.selectedItem != null)
				{
					article.dureeEngagementEquipement = Number(cboEngagementEq.selectedItem.value);	
					article.libelleContratEq =  txtDesignationContratEq.text;
				}
				else
				{
					article.dureeEngagementEquipement = -1;
				}
				
				
				
				
				
				if (boolCreate)
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_CREATED));
				}
				else
				{
					dispatchEvent(new ArticleEvent(article,ArticleEvent.ARTICLE_UPDATED));
				}
				
				PopUpManager.removePopUp(this);	
			}
			else
			{
				message = "- Vous devez sélectionner des produits et un équipement\n";
				
				if(!boolPortage)
				{
					message += "-Vous devez saisir le code RIO pour un portage de ligne\n"
				}
				
				Alert.show(message,"Erreur")
			}
		}
		
		

		protected function _cbxOuvertureLigneChangeHandler(event:Event):void
		{
			if(cbxOuvertureLigne.selected)
			{
				lignesUtils.genererNumeroLigneUnique();	
			}
		}
		
		protected function txtNumLIgneChangeHandler(event:Event):void
		{
			if(ConsoviewUtil.isPresent(txtNumLigne.text,lignesUtils.listeNumerosValides))
			{
				lignesUtils.boolOK = true;
				btVerifierNumLigne.visible = false;
				btVerifierNumLigne.width = 0;
				return;
			}
			else
			{
				lignesUtils.boolOK = false;
				btVerifierNumLigne.visible = true;
				btVerifierNumLigne.width = 80;
				EffectProvider.FadeThat(btVerifierNumLigne);	
			}
						
		}
		
		protected function btVerifierNumLigneClickHandler(event:Event):void
		{
			if(txtNumLigne.text!="")
			{
				btValider.enabled = false;
				lignesUtils.verifierUniciteNumeroLigne(txtNumLigne.text);
			}
		}
	}
}