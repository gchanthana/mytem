package univers.facturation.suivi.controles.vues
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
 
	
	[Bindable]
	public class PopUpBaseImpl extends TitleWindow
	{	
		public static const VALIDER_CLICKED : String = "validerCliked";
		
		public var btAnnuler : Button;
		public var btValider : Button;
		
	 
			
		public function PopUpBaseImpl()
		{
			super();
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
			btAnnuler.addEventListener(MouseEvent.CLICK,btAnnulerClickHandler);
			btValider.addEventListener(MouseEvent.CLICK,btValiderClickHandler);
			addEventListener(CloseEvent.CLOSE,btCloseHandler);
		}
		
		protected function btCloseHandler(ce : CloseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		protected function btAnnulerClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		protected function btValiderClickHandler(me : MouseEvent):void{
			verifierDonnees();
		}
		
		//pour verifier les données si nécessaire overrider cette fonction 
		protected function verifierDonnees():void{
			dispatchEvent(new Event(VALIDER_CLICKED));
			PopUpManager.removePopUp(this);
		}
		
		
		
	}
}