package intranet.GestionDesLogin
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import intranet.GestionDesLogin.CBTriOpeIHM;
    import mx.collections.ArrayCollection;
    import mx.containers.VBox;
    import mx.controls.AdvancedDataGrid;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.TextInput;
    import mx.core.IFlexDisplayObject;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.events.ListEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import paginatedatagrid.PaginateDatagrid;
    import searchpaginatedatagrid.SearchPaginateDatagrid;

    [Bindable]
    public class GestionDesLogin extends VBox
    {
        public static var TabLogin:ArrayCollection;
        public var txtFiltre:TextInput;
        public var btCreer:Button;
        public var btModifier:Button;
        public var btSupprimer:Button;
        public var adgLogin:AdvancedDataGrid;
        public var cbOpe:CBTriOpeIHM;
        public var myPaginatedatagrid:PaginateDatagrid;
        public var mySearchPaginateDatagrid:SearchPaginateDatagrid;

        public function GestionDesLogin()
        {
            super();
        }

        public function GestionDesLoginCreationCompleteHandler(e:FlexEvent):void
        {
            getData(0);
            //cbOpe.addEventListener(ListEvent.CHANGE,cbOpeDataChangeHandler);
        }

        public function btCreerClickHandler(e:MouseEvent):void
        {
            var a:IFlexDisplayObject;
            if (!a)
            {
                a = PopUpManager.createPopUp(this, Alert_Creer_LoginIHM, true);
            }
            else
            {
                PopUpManager.addPopUp(a, this, true);
            }
            PopUpManager.centerPopUp(a);
            a.addEventListener(CloseEvent.CLOSE, aCloseHandler);
        }

        public function aCloseHandler(e:CloseEvent):void
        {
            getData();
            Alert_Creer_Login.obj = null;
        }

        public function txtFiltreChangeHandler(e:Event):void
        {
            if (TabLogin != null)
            {
                TabLogin.filterFunction = filtrer;
                TabLogin.refresh();
            }
        }

        public function filtrer(item:Object):Boolean
        {
            if (String(item.LOGIN_EXTRANET).match(new RegExp(txtFiltre.text, 'i')))
                return true;
            else
                return false;
        }

        public function btModifierClickHandler(e:MouseEvent):void
        {
            var a:IFlexDisplayObject;
            Alert_Creer_Login.obj = adgLogin.selectedItem;
            if (!a)
            {
                a = PopUpManager.createPopUp(this, Alert_Creer_LoginIHM, true);				
            }
            else
            {
                PopUpManager.addPopUp(a, this, true);
            }
            PopUpManager.centerPopUp(a);
            a.addEventListener(CloseEvent.CLOSE, aCloseHandler);
        }

        public function btSupprimerClickHandler(e:MouseEvent):void
        {
            if (adgLogin.selectedItem != null)
            {
                Alert.show("Etes vous sur de vouloir supprimer ce login ?", "Confirmation de suppression", Alert.YES | Alert.NO, this, AlertbtSupprimerHandler);
            }
        }

        public function AlertbtSupprimerHandler(e:CloseEvent):void
        {
            if (e.detail == Alert.YES)
            {
                var tmpope:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.gestiondeslogins.GestionDesLogins", "DeleteLogin", suppOk, suppNo);
                RemoteObjectUtil.callService(tmpope, adgLogin.selectedItem.LOGIN_EXTRANET);
            }
        }

        public function suppOk(e:ResultEvent):void
        {
            getData(0);
        }

        public function suppNo(e:FaultEvent):void
        {
            trace('GestionDesLogin - AlertbtSupprimerHandler :' + e.message);
        }

        public function cbOpeDataChangeHandler(e:ListEvent):void
        {
            if (cbOpe.selectedIndex > -1)
            {
                getData(cbOpe.selectedItem.OPERATEURID);
            }
        }

        public function txtFiltreClickHandler(e:Event):void
        {
            txtFiltre.text = "";
        }

        public function getData(operateurid:int = 0):void
        {
            var tmpope:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.gestiondeslogins.GestionDesLogins", "DisplayLogin", getDataOk, getDataNo);
            RemoteObjectUtil.callService(tmpope, operateurid);
        }

        public function getDataOk(e:ResultEvent):void
        {
            TabLogin = e.result as ArrayCollection;
            TabLogin.refresh();
        }

        public function getDataNo(e:FaultEvent):void
        {
            trace('GestionDesLogin - getData :' + e.message);
        }
    }
}