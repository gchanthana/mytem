<cfcomponent displayname="Access" output="false">
	
	<cffunction access="public" output="false" name="getUserAccess" returntype="query">
			
			<cfargument name="codeModule" type="string" required="true">
			<cfargument name="idUser" type="string" required="true">
			<cfargument name="idNoeud" type="string" required="true">
			
			
			
			<cfset idracine = session.perimetre.id_groupe>
			<cfset codeLangue=session.user.idglobalization>
			
			
            <cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M00.GET_APP_USERPARAMS_V2">
				
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUser#" variable="p_app_loginid">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idracine#" variable="p_idracine">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeLangue#" variable="p_langid">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idNoeud#" variable="p_idgroupe_client">
                  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeModule#">

                  <cfprocresult name="qUserAccess">

            </cfstoredproc>        
				
						
            <cfreturn qUserAccess>

      </cffunction>

	
	<cffunction access="public" output="false" name="activeParam" returntype="array">
			
			<cfargument name="array_param" type="array" required="true">
									
			<cfloop from="1" to="#arraylen(array_param)#" index="paramName">
				
	            <cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M00.ILoginModuleParam">
					
	                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#array_param[paramName].appLogModuleId#" variable="p_applogmodid">
	                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#array_param[paramName].idParam#" variable="p_idparam">
	                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#array_param[paramName].isActif#" variable="p_isactif">
	
	                  <cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	
	            </cfstoredproc>
	            
				<cfset array_param[paramName].isActif = p_retour>
				<cfset array_param[paramName].isUpdated = p_retour>
				
			</cfloop>
						
            <cfreturn array_param>

      </cffunction>

</cfcomponent>