<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.container.Container"
extends="fr.consotel.api.ibis.publisher.handler.process.PostProcess"
hint="Extension de PostProcess pour l'implémentation du HANDLER de diffusion Container">
<!--- *********************************************************************************************************************** --->
	<!--- fr.consotel.api.ibis.publisher.handler.process.PostProcess --->
	<cffunction access="public" name="run" returntype="void">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfset var apiContainer=createObject("component",VARIABLES.CONTAINER)><!--- API Container --->
		<!--- API Container (PostProcess) : Méthode 1 --->
		<cfset var processName=apiContainer.getCodeProcess(ARGUMENTS.eventObject.getJobId(),"BIP")>
		<cfif processName EQ "-1"><!--- Condition d'erreur --->
			<cfmail attributecollection="#getMailAttributes()#" subject="PostProcess - Container.run() : Valeur non valide retournée par CONTAINER.getCodeProcess()">
				<cfoutput>
					JOBID : #ARGUMENTS.eventObject.getJobId()#<br>
					Valeur du 2ème paramètre : BIP<br>
					Valeur retournée : #processName#<br>
				</cfoutput>
			</cfmail>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="PostProcess - Container.run() : CONTAINER.getCodeProcess() INVALID VALUE">
		</cfif>
		<!--- Fin Méthode 1 --->
		<!--- API Container (PostProcess) : Instanciation de API Container Process --->
		<cfset var processContainer=createObject("component","fr.consotel.process.process" & processName)>
		<!--- API Container (PostProcess) : Méthode 2 --->
		<cfset var runStatus=processContainer.run(ARGUMENTS.eventObject.getJobId(),getContainerStatus(ARGUMENTS.eventObject.getReportStatus()),"BIP")>
		<cfif runStatus NEQ 1><!--- Condition d'erreur --->
			<cfset var apiContainerErreur=createObject("component",VARIABLES.CONTAINER_ERREUR)><!--- API Container Erreur--->
			<cftry>
				<!--- Notification d'erreur de l'API Container --->
				<cfset var runErrorStatus=apiContainerErreur.run(ARGUMENTS.eventObject.getJobId(),getContainerStatus(ARGUMENTS.eventObject.getReportStatus()),"BIP")>
				<cfcatch type="any">
					<cfmail attributecollection="#getMailAttributes()#" subject="PostProcess - Container.run() : Valeur non valide retournée par #VARIABLES.CONTAINER_ERREUR#.run()">
						<cfoutput>
							Process Name (Spécifique API Container) : #processName#<br>
							Nom complet (Spécifique API Container) : #VARIABLES.CONTAINER_ERREUR#<br> 
							Statut du rapport : #ARGUMENTS.eventObject.getReportStatus()#<br>
							Statut du rapport (Valeur API Container) : #getContainerStatus(ARGUMENTS.eventObject.getReportStatus())#<br>
							Valeur du 3ème paramètre : BIP<br>
							Valeur retournée : #runErrorStatus#<br>
							Valeur attendue : 1
						</cfoutput>
					</cfmail>
					<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="PostProcess - Container.run() : #VARIABLES.CONTAINER_ERREUR#.run() INVALID VALUE">
				</cfcatch>
			</cftry>
			<cfmail attributecollection="#getMailAttributes()#" subject="PostProcess - Container.run() : Valeur non valide retournée par fr.consotel.process.process#processName#.run()">
				<cfoutput>
					Process Name (Spécifique API Container) : #processName#<br>
					Nom complet (Spécifique API Container) : fr.consotel.process.process#processName#<br>
					Statut du rapport : #ARGUMENTS.eventObject.getReportStatus()#<br>
					Statut du rapport (Valeur API Container) : #getContainerStatus(ARGUMENTS.eventObject.getReportStatus())#<br>
					Valeur du 3ème paramètre : BIP<br>
					Valeur retournée : #runStatus#<br>
					Valeur attendue : 1
				</cfoutput>
			</cfmail>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="PostProcess - Container.run() : fr.consotel.process.process#processName#.run() INVALID VALUE">
		</cfif>
		<!--- Fin Méthode 2 --->
		<!--- Implémentation de la classe parent : MAJ de l'ID de MAJ --->
		<cfset SUPER.run(ARGUMENTS.eventObject)>
	</cffunction>

	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Appelée en tant que constructeur">
		<cfset SUPER.createInstance()><!--- Constructeur de la classe parent --->
		<cflog text="PostProcess - Container.createInstance()" file="#getLog()#" type="information">
		<cfset VARIABLES.CONTAINER="fr.consotel.consoview.api.container.ApiContainerV2"><!--- API Container --->
		<cfset VARIABLES.CONTAINER_ERREUR="fr.consotel.process.processContainerErreur"><!--- API Container Erreur --->
		<!--- Liste des valeurs API Container pour le statut du rapport --->
		<cfset VARIABLES.CONTAINER_STATUS={S="Completed",F="Error",W="Unknown",P="Suspended"}>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="private" name="getEventMailAttributes" returntype="Struct" hint="Retourne les propriétés du serveur MAIL à utiliser pour sendReporting()">
		<cfargument name="codeApp" type="numeric" required="false" default="0" hint="Code App">
		<cfreturn SUPER.getMailAttributes()>
	</cffunction>

	<!--- Copie du code de MailHandler --->
	<cffunction access="public" name="onError" returntype="void" hint="Envoi 2 mails de notification d'erreur et affiche le log d'erreur">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true">
		<cfargument name="eventHandler" type="string" required="true">
		<cfargument name="exceptionObject" type="any" required="false" default="">
		<cfset var errorMsg="AN EXCEPTION OCCURED">
		<cfif isDefined("ARGUMENTS.exceptionObject.MESSAGE")>
			<cfset var errorMsg=ARGUMENTS.exceptionObject.MESSAGE>
		</cfif>
		<cflog text="PostProcess - Container.onError() : #errorMsg#" file="#getLog()#" type="error">
		<cfset var eventTargetInfos=SUPER.getTargetInfos(ARGUMENTS.eventObject)>
		<cfmail attributecollection="#getMailAttributes()#" subject="[*Error*] PostProcess - Container.onError() : #errorMsg#">
			<cfoutput>
				Evènement :<br>
					- EVENT_ID : #ARGUMENTS.eventObject.getId()#<br>
					- EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br>
					- EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br>
					- EVENT_HANDLER : #ARGUMENTS.eventObject.getEventHandler()#<br>
					- JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br>
					- JOB_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br>
					- JOB_STATUS (Container) : #getContainerStatus(ARGUMENTS.eventObject.getReportStatus())#<br>
					- REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br>
				Autres :<br>
					- BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br>
					- EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#<br><br>
			</cfoutput>
			<cfdump var="#eventTargetInfos#" label="EventTarget Infos">
			<cfdump var="#ARGUMENTS.exceptionObject#" label="Exception Rencontrée">
		</cfmail>
	</cffunction>
	
	<cffunction access="private" name="getContainerStatus" returntype="String" hint="Retourne la valeur correspondante au statut pour l'API Container">
		<cfargument name="reportStatus" type="String" required="true" hint="Statut du rapport">
		<cfset var containerStatus="W"><!--- Status par défaut : Unknown --->
		<cfif structKeyExists(VARIABLES.CONTAINER_STATUS,ARGUMENTS.reportStatus)>
			<cfset containerStatus=VARIABLES.CONTAINER_STATUS[ARGUMENTS.reportStatus]>
		</cfif>
		<cfreturn containerStatus>
	</cffunction>
</cfcomponent>