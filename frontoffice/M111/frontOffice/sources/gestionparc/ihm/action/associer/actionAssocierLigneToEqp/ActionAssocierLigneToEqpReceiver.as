package gestionparc.ihm.action.associer.actionAssocierLigneToEqp
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.equipement.FicheAssocierLigneToEqp;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierLigneToEqpReceiver
	{
		private var fiche:FicheAssocierLigneToEqp;

		public function ActionAssocierLigneToEqpReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierLigneToEqp();
			PopUpManager.addPopUp(fiche, (Application.application as DisplayObject), true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
