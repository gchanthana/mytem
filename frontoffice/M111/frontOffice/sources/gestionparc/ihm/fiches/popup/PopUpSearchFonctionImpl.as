package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.engine.BreakOpportunity;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;

	public class PopUpSearchFonctionImpl extends TitleWindow
	{
		public var dgFonctions:DataGrid;

		public var txtSearch:TextInput;

		public var chckSelectFonction:RadioButton;

		public var btnValider:Button;
		public var btnAnnuler:Button;

		private var _fonctions:Object;

		[Bindable]
		public var listeFonctions:ArrayCollection=new ArrayCollection;

		public function PopUpSearchFonctionImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(fe:FlexEvent):void
		{
			dgFonctions.addEventListener(ListEvent.ITEM_CLICK, dgFonctionItemClickHandler);
			txtSearch.addEventListener(Event.CHANGE, txtSearchHandler);
			btnValider.addEventListener(MouseEvent.CLICK, btnValiderHandler);
			btnAnnuler.addEventListener(MouseEvent.CLICK, btnAnnulerHandler);
		}

		private function dgFonctionItemClickHandler(le:ListEvent):void
		{
			var itemRenderer:Object=le.itemRenderer;
			var currentItem:Object=itemRenderer.data;
			_fonctions=currentItem;
			setSelectedFonction(currentItem);
			btnValider.enabled=true;
		}

		private function setSelectedFonction(currentFonction:Object):void
		{
			var lenFonctions:int=listeFonctions.length;

			for (var i:int=0; i < lenFonctions; i++)
			{
				if (currentFonction != null && listeFonctions[i].USAGE == currentFonction.USAGE)
				{
					listeFonctions[i].SELECTED=true;
				}
				else
				{
					listeFonctions[i].SELECTED=false;
				}
				listeFonctions.itemUpdated(listeFonctions[i]);
			}
		}

		protected function txtSearchHandler(e:Event):void
		{
			if (listeFonctions != null)
			{
				listeFonctions.filterFunction=filterFunction;
				listeFonctions.refresh();
			}
		}

		private function btnValiderHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('GET_SELECTED_FONCTION_VALIDEE'));
		}

		private function btnAnnulerHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		public function closeWindowHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean=true;

			rfilter=rfilter && (item.USAGE != null && item.USAGE.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1);

			return rfilter;
		}

		public function getFonctions():Object
		{
			return _fonctions;
		}

		public function setFonctions(values:ArrayCollection, currentValue:Object=null):void
		{
			listeFonctions=values;
			if (currentValue != null)
				_fonctions=currentValue;

			setSelectedFonction(currentValue);
		}
	}
}
