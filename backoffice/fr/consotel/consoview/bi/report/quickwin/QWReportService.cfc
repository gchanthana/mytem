<cfcomponent name="QWReportService" alias="fr.consotel.consoview.bi.report.quickwin.QWReportService">
	<cffunction name="scheduleQWReport" access="public" returntype="any">
		<cfargument name="qwInfos" required="true" type="struct">
		<cfif checkQWInfos(arguments.qwInfos) EQ TRUE>
			<cfset sansConsoReport=createObject("component","fr.consotel.consoview.bi.report.sansconso.SansConsoReport")>
			<cfset deploiementReport=createObject("component","fr.consotel.consoview.bi.report.deploiement.DeploiementReport")>
			<cfset ratioReport=createObject("component","fr.consotel.consoview.bi.report.ratiogp.RatioGroupeProduitReport")>
			<cfset accueilReport=createObject("component","fr.consotel.consoview.bi.report.quickwin.Accueil")>
			<cfset perimetreReport=createObject("component","fr.consotel.consoview.bi.report.quickwin.Perimetre")>
			<cfset qwReportInfos=structNew()>
			<cfset qwReportInfos["SANS_CONSO"]=sansConsoReport.createReportInfos(arguments.qwInfos)>
			<cfset qwReportInfos["DEPLOIEMENT"]=deploiementReport.createReportInfos(arguments.qwInfos)>
			<cfset qwReportInfos["RATIO_GPRODUITS"]=ratioReport.createReportInfos(arguments.qwInfos)>
			<cfset qwReportInfos["ACCUEIL"]=accueilReport.createReportInfos(arguments.qwInfos)>
			<cfset qwReportInfos["PERIMETRE"]=perimetreReport.createReportInfos(arguments.qwInfos)>
 			<!--- SCHEDULE REPORT --->
			<cfset scheduleService=createObject("component","fr.consotel.consoview.bi.service.ScheduleService")>
			<cfset authResult=scheduleService.authLogin("cvuser","consotel09")>
			<cfset jobIdArray=arrayNew(1)>
			<cfset k=1>
			<cfset jobIdArray[k]=scheduleService.scheduleReport(qwReportInfos["SANS_CONSO"],"cvuser","consotel09")>
			<cfif jobIdArray[k] LTE 0>
				<cfreturn -2>
			</cfif>
			<cfset k=k+1>
			<cfset jobIdArray[k]=scheduleService.scheduleReport(qwReportInfos["DEPLOIEMENT"],"cvuser","consotel09")>
			<cfif jobIdArray[k] LTE 0>
				<cfreturn -3>
			</cfif>
			<!--- 
			<cfset k=k+1>
			<cfset jobIdArray[k]=scheduleService.scheduleReport(qwReportInfos["RATIO_GPRODUITS"],"cvuser","consotel09")>
			<cfif jobIdArray[k] LTE 0>
				<cfreturn -4>
			</cfif>
			 --->
			<cfset k=k+1>
			<cfset jobIdArray[k]=scheduleService.scheduleReport(qwReportInfos["ACCUEIL"],"cvuser","consotel09")>
			<cfif jobIdArray[k] LTE 0>
				<cfreturn -5>
			</cfif>
			<cfset k=k+1>
			<cfset jobIdArray[k]=scheduleService.scheduleReport(qwReportInfos["PERIMETRE"],"cvuser","consotel09")>
			<cfif jobIdArray[k] LTE 0>
				<cfreturn -6>
			</cfif>
			<cfreturn jobIdArray>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
	<cffunction name="checkQWInfos" access="private" returntype="boolean">
		<cfargument name="qwInfos" required="true" type="struct">
		<cfset boolIdRacineMaster=structKeyExists(arguments.qwInfos,"IDRACINE_MASTER")>
		<cfset boolIdRacine=structKeyExists(arguments.qwInfos,"IDRACINE")>
		<cfset boolSansConsoParam=FALSE>
		<cfif structKeyExists(arguments.qwInfos,"SANS_CONSO") EQ TRUE>
			<cfset boolDateDeb1=structKeyExists(arguments.qwInfos["SANS_CONSO"],"DATEDEB")>
			<cfset boolDateFin1=structKeyExists(arguments.qwInfos["SANS_CONSO"],"DATEFIN")>
			<cfset boolProduitAcces=structKeyExists(arguments.qwInfos["SANS_CONSO"],"PRODUIT_ACCES")>
			<cfset boolMobileInclus=structKeyExists(arguments.qwInfos["SANS_CONSO"],"MOBILE_INCLUS")>
			<cfset boolSansConsoParam=(boolDateDeb1 AND boolDateFin1 AND boolProduitAcces AND boolMobileInclus)>
		</cfif>
		<cfset boolDeploiementParam=FALSE>
		<cfif structKeyExists(arguments.qwInfos,"DEPLOIEMENT") EQ TRUE>
			<cfset boolDateDeb2=structKeyExists(arguments.qwInfos["DEPLOIEMENT"],"DATEDEB")>
			<cfset boolPeriodicite=structKeyExists(arguments.qwInfos["DEPLOIEMENT"],"PERIODICITE")>
			<cfset boolMontantFtMin=structKeyExists(arguments.qwInfos["DEPLOIEMENT"],"MONTANT_FT_MIN")>
			<cfset boolDeploiementParam=(boolDateDeb2 AND boolPeriodicite AND boolMontantFtMin)>
		</cfif>
		<cfset boolRatioGpParam=FALSE>
		<cfif structKeyExists(arguments.qwInfos,"RATIO_GPRODUITS") EQ TRUE>
			<cfset boolCategorieGp=structKeyExists(arguments.qwInfos["RATIO_GPRODUITS"],"CATEGORIE_GP")>
			<cfset boolDateDeb3=structKeyExists(arguments.qwInfos["RATIO_GPRODUITS"],"DATEDEB")>
			<cfset boolDateFin3=structKeyExists(arguments.qwInfos["RATIO_GPRODUITS"],"DATEFIN")>
			<cfset boolIdGroupeProduit=structKeyExists(arguments.qwInfos["RATIO_GPRODUITS"],"IDGROUPE_PRODUIT")>
			<cfset boolRatioGpParam=(boolCategorieGp AND boolDateDeb3 AND boolDateFin3 AND boolIdGroupeProduit)>
		</cfif>
		<cfreturn (boolIdRacineMaster AND boolIdRacine AND boolSansConsoParam AND boolDeploiementParam AND boolRatioGpParam)>
	</cffunction>
</cfcomponent>
