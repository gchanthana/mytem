<cfcomponent displayname="KpiDataManager" hint="traitement des données KPI" output="false">
	
	<cfset datasourceBI = "BI_TEST">
	
	<!--- 
		appeler la methode recuperant les données KPI
	 --->
	<cffunction name="getDataKPI" access="remote" output="true" returntype="Any" hint="appeler la methode recuperant les données KPI">
				
		<cfargument name="code" 		type="string" 	required="true" hint="le code du KPI est le nom de la methode à appeler">
		<cfargument name="array" 		type="Array" 	required="true">
			
		<cfset arrayParam = initStructParam(array)>
						
		<cftry>
			<cfinvoke method="#code#" returnvariable="queryRetour">
				<cfloop array="#arrayParam#" index="x"><!--- boucle sur l'array contenant une structure clé/valeur --->
					<cfif isStruct(x)>
						<cfloop collection="#x#" item="item">
							<cfinvokeargument name="#item#" value="#x[item]#">
						</cfloop>
					</cfif>
				</cfloop>
			</cfinvoke>
			<cfreturn queryRetour>
			<cfcatch>
				<cflog type="error" text="[Page accueil-getDataKPI] Error: #CFCATCH.message# - Detail: #CFCATCH.detail#">
				<cfreturn cfcatch>
		</cfcatch>
		</cftry>
		
	</cffunction>
		
	<!--- 
		KPI ARPU mobile 
	--->
	<cffunction name="KPI_1" access="remote" output="false" returntype="Query"
				hint="KPI ARPU mobile">
		
		<cfargument name="P_IDSEGMENT" type="numeric" required="true">
		<cfargument name="L_PERIODE" type="numeric" required="true">
		
		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
		
		<!---
			 pkg_m54
				PROCEDURE getKPICoutParLigne(
				p_idracine IN INTEGER,
				p_idracine_master IN INTEGER,
				p_idorga IN integer,
				p_idperimetre IN integer,
				p_niveau IN varchar2,
				p_idperiode IN INTEGER,
				p_segment IN INTEGER,
				p_code_langue IN VARCHAR2,
				p_retour OUT sys_refcursor); 
		--->
		<cfstoredproc procedure="pkg_m54.getKPICoutParLigne" datasource="#SESSION.OFFREDSN#" >
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode" value="#L_PERIODE#">				
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
				<cfprocresult name="qResult" >
			</cfstoredproc>
		
		
		
		<!--- formatage query ---> 
		<cfset retourQuery = createQueryValue(#qResult['ARPU_M'][1]#, "DOUBLE", #qResult['DELTA_ARPU'][1]#, "VARCHAR", #qResult['devise'][1]#, "VARCHAR")>
		
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		KPI coûts télécom abos consos 
	--->
	<cffunction name="KPI_2" access="remote" output="false" returntype="Query"
				hint="KPI coûts télécom abos consos">
			
			<cfargument name="P_IDSEGMENT" type="numeric" required="true">
			<cfargument name="L_PERIODE" type="numeric" required="true">
			<!---
			PROCEDURE getKPICoutMensuel(    p_idracine            IN INTEGER,
                                  p_idracine_master     IN INTEGER,
                                  p_idorga              IN integer,
                                  p_idperimetre         IN integer,
                                  p_niveau              IN varchar2,
                                  p_idperiode           IN INTEGER,
                                  p_segment            IN INTEGER,
                                  p_code_langue         IN VARCHAR2,
                                  p_retour              OUT sys_refcursor);
			--->
			
			<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
		 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
		  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
		  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
		  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
		  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
		  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
			
			
			<cfstoredproc procedure="pkg_m54.getKPICoutMensuel" datasource="#SESSION.OFFREDSN#" >
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode" value="#L_PERIODE#">				
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
				<cfprocresult name="qResult" >
			</cfstoredproc>
			
			<!--- formatage query --->
			<cfset retourQuery = createQueryValue(#qResult['COUT_M'][1]#, "DOUBLE", #qResult['DELTA_COUT'][1]#, "VARCHAR", #qResult['devise'][1]#, "VARCHAR")>
			
			<cfreturn retourQuery>
	
	</cffunction>

	<!--- 
		KPI coût télécom annuel 
	--->
	<cffunction name="KPI_3" access="remote" output="false" returntype="Query"
				hint="KPI coût télécom annuel">
		
		<cfargument name="P_IDSEGMENT" type="numeric" required="true">
		<cfargument name="L_PERIODE" type="numeric" required="true">
		<!--- 
		 PROCEDURE getKPICoutAnnuel(     p_idracine            IN INTEGER,
                                  p_idracine_master     IN INTEGER,
                                  p_idorga              IN integer,
                                  p_idperimetre         IN integer,
                                  p_niveau              IN varchar2,
                                  p_idperiode           IN INTEGER,
                                  p_segment            IN INTEGER,
                                  p_code_langue         IN VARCHAR2,
                                  p_retour              OUT sys_refcursor);
         --->
         
        <cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
		  
		<cfstoredproc procedure="pkg_m54.getKPICoutAnnuel" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode" value="#L_PERIODE#">				
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>                         		
		
		
		<!--- formatage query --->
		<cfset retourQuery = createQueryValue(#qResult['COUT_A'][1]#, "DOUBLE","","VARCHAR", #qResult['devise'][1]#, "VARCHAR")>
		
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		KPI Roaming mobile data 
	--->
	<cffunction name="KPI_4" access="remote" output="false" returntype="Query"
				hint="KPI Roaming mobile data">
		
		<cfargument name="L_PERIODE" type="numeric" required="true">
		<!---
		PROCEDURE getKPIRoamingMobData( p_idracine            IN INTEGER,
                                  p_idracine_master     IN INTEGER,
                                  p_idorga              IN integer,
                                  p_idperimetre         IN integer,
                                  p_niveau              IN varchar2,
                                 p_idperiode           IN INTEGER,
                                  p_code_langue         IN VARCHAR2,
                                  p_retour              OUT sys_refcursor);
        --->    
        <cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
		         
		<cfstoredproc procedure="pkg_m54.getKPIRoamingMobData" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>    
		
		<!--- formatage query --->
		<cfset retourQuery = createQueryValue(#qResult['ROAD_M'][1]#, "DOUBLE", #qResult['DELTA_ROAD'][1]#, "VARCHAR", #qResult['devise'][1]#, "VARCHAR")>
		
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		KPI Roaming mobile voix in&out
	 --->
	<cffunction name="KPI_5" access="remote" output="false" returntype="Query"
				hint="KPI Roaming mobile voix in&out">
		
		<cfargument name="L_PERIODE" type="numeric" required="true">
		
		<!---
		PROCEDURE getKPIRoamingMobVoix( p_idracine            IN INTEGER,
                                  p_idracine_master     IN INTEGER,
                                  p_idorga              IN integer,
                                  p_idperimetre         IN integer,
                                  p_niveau              IN varchar2,
                                  p_idperiode           IN INTEGER,
                                  p_code_langue         IN VARCHAR2,
                                  p_retour              OUT sys_refcursor);
		--->
		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
		  
		<cfstoredproc procedure="pkg_m54.getKPIRoamingMobVoix" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>
		
		<!--- formatage query --->
		<cfset retourQuery = createQueryValue(#qResult['ROAV_M'][1]#, "DOUBLE", #qResult['DELTA_ROAV'][1]#, "VARCHAR", #qResult['devise'][1]#, "VARCHAR")>
		
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		KPI nb lignes facturées
	 --->
	<cffunction name="KPI_6" access="remote" output="false" returntype="Query"
				hint="KPI nb lignes facturées">
		
		<cfargument name="P_IDSEGMENT" type="numeric" required="true">
		<cfargument name="L_PERIODE" type="numeric" required="true">
		
		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
		
		
		
		<cfstoredproc procedure="pkg_m54.getLigneParSegment" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_deb" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_fin" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>
		
		<!--- formatage query --->
		<cfset retourQuery = createQueryValue(#qResult['NB_LIGNES'][1]#, "INTEGER", #qResult['MONTANT'][1]#, "INTEGER", #qResult['DEVISE'][1]#, "VARCHAR")>
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		Couts télécoms année calendaire
	 --->
	<cffunction name="KPI_7" access="remote" output="false" returntype="Query"
				hint="cout telecom année calendaire">
		<cfargument name="P_IDSEGMENT" type="numeric" required="true">	
		
		<!---
		PROCEDURE getKPICoutAnnee_calendaire(    p_idracine            IN INTEGER,
                                           p_idracine_master     IN INTEGER,
                                           p_idorga              IN integer,
                                           p_idperimetre         IN integer,
                                           p_niveau              IN varchar2,
                                           p_idperiodefin        IN INTEGER,
                                           p_segment                IN INTEGER,
                                           p_code_langue         IN VARCHAR2,
                                           p_retour              OUT sys_refcursor);
                                           
        --->
        
		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
		
		
		
		<cfstoredproc procedure="pkg_m54.getKPICoutAnnee_calendaire" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>
		
		<!--- formatage query --->
		<cfset retourQuery = createQueryValue(#qResult['cout_cal'][1]#, "DOUBLE", "", "INTEGER", #qResult['devise'][1]#, "VARCHAR")>
		
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		KPI nb lignes sans conso 
	--->
	<cffunction name="KPI_8" access="remote" output="false" returntype="Query"
				hint="KPI nb lignes sans conso">
		
		<cfargument name="P_IDSEGMENT" type="numeric" required="true">
		<cfargument name="L_PERIODE" type="numeric" required="true">
		
		<!---
		PROCEDURE getLigneSansConsos(
								p_idracine IN INTEGER,
        						p_idracine_master IN INTEGER,
                                p_idorga IN integer,
                                p_idperimetre IN integer,
                                p_niveau IN varchar2,
                                p_idperiode_deb IN INTEGER, 
                                p_idperiode_fin IN INTEGER,
                                p_segement     IN INTEGER,
                                p_code_langue IN VARCHAR2,
                                p_retour OUT sys_refcursor);  
		--->
		<cfset p_idracine_master = SESSION.PERIMETRE.IDRACINE_MASTER>
	 	<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_idorga        	= SESSION.PERIMETRE.ID_ORGANISATION>
	  	<cfset p_idperimetre   	= SESSION.PERIMETRE.ID_PERIMETRE>
	  	<cfset p_niveau        	= SESSION.PERIMETRE.NIVEAU>
	  	<cfset p_idniveau      	= SESSION.PERIMETRE.ID_NIVEAU>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
		
		<cfstoredproc procedure="pkg_m54.getlignesansconsos" datasource="#SESSION.OFFREDSN#" >
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#p_idracine_master#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idorga" value="#p_idorga#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#p_idperimetre#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_niveau" value="#p_niveau#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_deb" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_idperiode_fin" value="#L_PERIODE#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_segement" value="#P_IDSEGMENT#">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#p_langue_pays#">
			<cfprocresult name="qResult" >
		</cfstoredproc>
				
		<!--- formatage query --->
			
		<cfset retourQuery = createQueryValue(#qResult['NB_LIGNES'][1]#, "INTEGER", #qResult['MONTANT'][1]#, "INTEGER", #qResult['DEVISE'][1]#, "VARCHAR")>
		<cfreturn retourQuery>
		
	</cffunction>
	
	
	<cffunction name="KPI_9" access="remote" output="false" returntype="any" hint="KPI commandes en cours (montant | nombre)">
		<cfargument name="P_POOL" type="any" required="false" default="-1">
		
		<cfset p_idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset p_langue_pays	 = SESSION.USER.GLOBALIZATION>
		
		<cfif P_POOL EQ -1 >
			<cfset req='"- ENTETE_COMMANDE".IDRACINE=#p_idracine#'>
		<cfelse>
			<cfset req='"- POOL_COMMANDE".IDPOOL=#P_POOL#'>	
		</cfif>
		
		<cfquery name="qgetCommandeEnCours" datasource="#datasourceBI#">
			set variable
				p_idracine = #p_idracine#,
				p_langue_pays	 = '#p_langue_pays#';
				
			select "- ENTETE_COMMANDE"."Nombre de commandes en cours" NBCommandesEncours, 
				   "- ENTETE_COMMANDE".MONTANT_CMD_ENCOURS_LOC EncoursDesCommandes ,
					DEVISE.SYMBOLE_LOC devise
			from E0_COMMANDE 
			
			where #req#
		</cfquery>
		
		
		<cfset qgetCommandeEnCours = {
										value="#qgetCommandeEnCours.ENCOURSDESCOMMANDES#",
								        variation =qgetCommandeEnCours.NBCommandesEncours,
								        devise="#qgetCommandeEnCours.devise#"				            		  					      
		       	    				 }/>
		        	    				 
		<cfset retourQuery = createQueryValue(#qgetCommandeEnCours.value#, "INTEGER",#qgetCommandeEnCours.variation#, "VARCHAR", #qgetCommandeEnCours.devise#, "VARCHAR")>
		
		<cfreturn retourQuery>
		
	</cffunction>
	
	<cffunction name="KPI_10" access="remote" output="false" returntype="any" hint="KPI encours des commandes">
		<cfargument name="P_POOL" type="any" required="false">
		
		<cfset p_idracine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset p_langue_pays	 = SESSION.USER.GLOBALIZATION>
		
		<cfif P_POOL EQ -1 >
			<cfset req='"- ENTETE_COMMANDE".IDRACINE=#p_idracine#'>
		<cfelse>
			<cfset req='"- POOL_COMMANDE".IDPOOL=#P_POOL#'>	
		</cfif>
		
		<cfquery name="qgetEnCoursDesCommandes" datasource="#datasourceBI#">
			set variable
				p_idracine = #p_idracine#,
				p_langue_pays	 = '#p_langue_pays#';
				
			select PREZ_EQPT."Encours de commandes" EncoursDesCommandes ,DEVISE.SYMBOLE_DOC devise
			from E0_COMMANDE 
			where #req#
		</cfquery>
			
		<cfset qgetEnCoursDesCommandes = {
										 	value=qgetEnCoursDesCommandes.EncoursDesCommandes,
									        variation ="",
									        devise="#qgetEnCoursDesCommandes.devise#"				            		  					      
		        	    				 }/>
		        	    				  
		<cfset retourQuery = createQueryValue(#qgetEnCoursDesCommandes.value#, "INTEGER",#qgetEnCoursDesCommandes.variation#, "VARCHAR", #qgetEnCoursDesCommandes.devise#, "VARCHAR")>
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		KPI_11 Total des commandes (montant | nombre)
	--->
	<cffunction name="KPI_11" access="remote" output="false" returntype="Query"	hint="KPI_11 Total des commandes (montant | nombre)">
		<!---<cfargument name="P_IDSEGMENT" type="numeric" required="true">--->
		<cfargument name="L_PERIODE" type="numeric" required="true">
		
		<cfset p_idracine      	= SESSION.PERIMETRE.ID_GROUPE>
	  	<cfset p_langue_pays	= SESSION.USER.GLOBALIZATION>
	  	
		<cfif P_POOL EQ -1 >
			<cfset req='"- ENTETE_COMMANDE".IDRACINE=#p_idracine#'>
		<cfelse>
			<cfset req='"- POOL_COMMANDE".IDPOOL=#P_POOL#'>	
		</cfif> 
		         
	 	<cfquery name="qgetTotalCommandes" datasource="#datasourceBI#">
		  
			set variable
					p_idracine = #p_idracine#,
					p_langue_pays	 = '#p_langue_pays#';
					
			select	"- ENTETE_COMMANDE".NB_COMMANDES nbCommande,
			        "- ENTETE_COMMANDE".MONTANT_LOC mntCommande, 
				    DEVISE.SYMBOLE_LOC devise
			from E0_COMMANDE 
			where #req# and PERIODE.IDPERIODE_MOIS  = #L_PERIODE#
		
		</cfquery>
		
		<cfset qgetTotalCommandes = {
										value="#qgetTotalCommandes.mntCommande#",
								        variation =qgetTotalCommandes.nbCommande,
								        devise="#qgetTotalCommandes.devise#"				            		  					      
		       	    				 }/>
		 
		<!--- formatage query --->
		<cfset retourQuery = createQueryValue(#qgetTotalCommandes.value#, "INTEGER",#qgetTotalCommandes.variation#, "VARCHAR", #qgetTotalCommandes.devise#, "VARCHAR")>
		
		<cfreturn retourQuery>
		
	</cffunction>
	
	<!--- 
		créer une struct de parametres 
	--->
	<cffunction name="initStructParam" access="private" output="false" returntype="Any"  hint="créer une struct de parametres">
		
		<cfargument name="array" type="array" required="true">
		
		<cfset arrayParam = arrayNew(1)>
		
		<cfset x = 1>
		<cfloop from="1" to="#ArrayLen(array)#" index="i" step="1">
			<cfloop from="1" to="#ArrayLen(array[i])#" index="j" step="1">
				<cfset cle = ARGUMENTS["array"][i][j]["cle"]>
				<cfset valeur = ARGUMENTS["array"][i][j]["value"][1]>
				<cfset arrayParam[x] = { #cle# = valeur}>
				<cfset x++>
			</cfloop>
		</cfloop>
		
		<!--- <cfscript>
			x = 1;
			for (item in array) //boucler sur array principal
			{
				for (ssArray in item) //boucler sur sous_array (qui contient une structure)
				{
			 		cle = structfind(ssArray,"cle"); // valeur de la propriete "cle"
					valueArray = structfind(ssArray,"value"); // recuperer l'array de la propriete "value"

					for (value in valueArray) //boucler sur array
					{
						arrayParam[x] = { #cle# = value };
						x++;
					}
				}
			}
		</cfscript> --->
		
		<cfreturn arrayParam>
		
	</cffunction>
	
	<!--- 
		renvoie un query avec les valeurs necessaires pour affichage dans KPI/Widget
	 --->
	<cffunction name="createQueryValue" access="private" output="false" returntype="Query" 
				hint="renvoie un query avec les valeurs necessaires pour affichage dans KPI/Widget">
					
		<cfargument name="value" type="any"  required="true">
		<cfargument name="valueType" type="string"  required="true">
		<cfargument name="variation" type="any" required="false">
		<cfargument name="variationType" type="string" required="false">
		<cfargument name="devise" type="any" required="false">
		<cfargument name="deviseType" type="string" required="false">
		
		<cfset qry = queryNew("value, variation,devise","#valueType#, #variationType#,#deviseType#")> <!--- new query --->
		<cfset newRow = QueryAddRow(qry, 1)> <!--- ajout d'un row --->
		
		<cfset queryRow = querySetCell(qry, "value", #value#, 1)> <!--- remplir les cellules du query --->
		<cfset queryRow = querySetCell(qry, "variation", #variation#, 1)>
		<cfset queryRow = querySetCell(qry, "devise", #devise#, 1)>
		
		<cfreturn qry>
		
	</cffunction>
	
	
</cfcomponent>