package appli.impersonnification.service
{
    import composants.util.ConsoviewAlert;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    //import intranet.gestionLoginClasse.GestionLogin;
    import mx.controls.listClasses.ListData;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    import paginatedatagrid.pagination.vo.ItemIntervalleVO;
    import searchpaginatedatagrid.ParametresRechercheVO;

    public class ListServices
    {
        //VARIABLES------------------------------------------------------------------------------
        public var myDatas:ListDatas;
        public var myHandlers:ListHandlers;

        public function ListServices()
        {
            myDatas = new ListDatas();
            myHandlers = new ListHandlers(myDatas);
        }

        //METHODE PUBLIC-------------------------------------------------------------------------
        public function getListeLogins(parametresRechercheVO:ParametresRechercheVO = null, itemIntervalleVO:ItemIntervalleVO = null, operateurid:Number = 0, firstLoading:Boolean = false):void
        {
            var st:String;
            var ob:String;
            if (!parametresRechercheVO.SEARCH_TEXT)
            {
                st = "LOGIN_NOM,";
            }
            else
            {
                st = parametresRechercheVO.SEARCH_TEXT;
                // on vérifie que la valeur nulle n'est pas passée en paramètre
                var ar:Array = st.split(",");
                if (ar[1] == "null")
                {
                    st == ar[0] + ", ";
                }
            }
            if (!parametresRechercheVO.ORDER_BY)
            {
                ob = "LOGIN_NOM,ASC";
            }
            else
            {
                ob = parametresRechercheVO.ORDER_BY;
                var arr:Array = ob.split(",");
                var order:String;
                switch (arr[1])
                {
                    case "Croissant":
                        order = "ASC";
                        break;
                    case "Décroissant":
                        order = "DESC";
                        break;
                    case "ASC":
                        order = "ASC";
                        break;
                    case "DESC":
                        order = "DESC";
                        break;
                    default:
                        order = "ASC";
                        break;
                }
                ob = String(arr[0]) + "," + order;
                st = parametresRechercheVO.SEARCH_TEXT;
            }
            if (firstLoading)
            {
                st = "LOGIN_NOM,";
            }
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M00.SupportService", "getPaginateListeLogin", myHandlers.getListeLoginsResultHandler, null);
            RemoteObjectUtil.callService(op, operateurid, st, ob, itemIntervalleVO.indexDepart, itemIntervalleVO.tailleIntervalle);
        }
    }
}