package composants.popuputils
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.event.gestionparcEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class ConfirmerOrAnnulerMsgImpl extends TitleWindow
	{
		public var btConfirmer:Button;
		public var btAnnuler:Button;
		public var taMsg:TextArea;

		public var btConfirmerLabel:String;
		public var btAnnulerLabel:String;

		private var _msg:String;

		/**
		 * Cette fonction est le contructeur de la classe.
		 */
		public function ConfirmerOrAnnulerMsgImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialise l'affichage du message et les label des boutons.
		 */
		private function init(event:FlexEvent):void
		{
			taMsg.text=msg;

			btConfirmer.label=btConfirmerLabel;
			btAnnuler.label=btAnnulerLabel;

			btAnnuler.addEventListener(MouseEvent.CLICK, annulerHandler);
			btConfirmer.addEventListener(MouseEvent.CLICK, confirmerHandler);
		}

		/**
		 * Annule le message de la popup.
		 */
		private function annulerHandler(event:MouseEvent):void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_CONFIRMER_ACTION_ANNULER));
			PopUpManager.removePopUp(this);
		}

		/**
		 * Confirme le message de la popup.
		 */
		private function confirmerHandler(event:Event):void
		{
			dispatchEvent(new gestionparcEvent(gestionparcEvent.POPUP_CONFIRMER_ACTION_CONFIRMER));
			PopUpManager.removePopUp(this);
		}

		public function get msg():String
		{
			return _msg;
		}

		public function set msg(value:String):void
		{
			_msg=value;
		}
	}
}
