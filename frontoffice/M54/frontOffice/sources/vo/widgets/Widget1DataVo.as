package vo.widgets
{
	import composants.util.ConsoviewFormatter;

	/**
	 * classe relative à l'objet Pays, ayant comme propriétés volume d'appels roming,
	 * le nom de pays, le code trigramme, le montant et la data
	 */
	public class Widget1DataVo
	{
		private var _trigramme:String;
		private var _pays:String; 
		private var _montantData:Number=0;
		private var _volumeData:Number=0; 
		private var _montantVoixIn:Number=0;
		private var _volumeVoixIn:Number=0;
		private var _montantVoixOut:Number;
		private var _volumeVoixOut:Number;
		private var _montantVoix:Number;
		private var _volumeVoix:Number;
		private var _montantTotal:Number;
		private var _devise:String;
		
		
		/**
		 * variable stockant les donnees demandees pour un array local
		 */
		private var _donnees:Number;
		
		private var _unite:String;
		
		public function Widget1DataVo()
		{
		}
		
		/**
		 * fonction de remplissage des propriétés d'un pays à partir d'un élémént de type Object 
		 * @param param1 de type Object
		 */
		public function fillData(obj:Object):void
		{
			this.pays=obj.PAYS;
			this.trigramme=obj.TRIGRAMME;
			this.montantData=obj.MNT_DATA;
			this.volumeData=obj.VOL_DATA;
			this.montantVoixIn=obj.MNT_VOIX_IN;
			this.volumeVoixIn=obj.VOL_VOIX_IN;
			this.montantVoixOut=obj.MNT_VOIX_OUT;
			this.volumeVoixOut=obj.VOL_VOIX_OUT;
			this.montantVoix=obj.MNT_VOIX;
			this.volumeVoix=obj.VOL_VOIX;
			this.montantTotal=obj.MNT_TOTAL;
			this.devise=obj.DEVISE;
		}
		
		/**
		 * fonction de filtrage des propriétés d'un pays à partir d'un élémént de type Object
		 * pour stocker dans la propriété "data" les données indiquées dans le param2
		 * @param param1 de type Object
		 * @param param2 de type String
		 */
		public function fillDataSelectionnee (ob:Object, codeChoix: String):void
		{
			switch(codeChoix)
			{
				case "MNT_TOTAL_ROAM":
					this.donnees = ob.montantTotal;
					this.unite = ob.devise;
					break;
				
				case "MNT_DATA_ROAM":
					this.donnees = ob.montantData;
					this.unite = ob.devise;
					break;
				
				case "VOL_DATA_ROAM":
					this.donnees = ob.volumeData/1024;
					this.unite = 'MO';
					break;
				
				case "MNT_VOIX_ROAM_IN":
					this.donnees = ob.montantVoixIn;
					this.unite = ob.devise;
					break;
				
				case "VOL_VOIX_ROAM_IN":
					this.donnees=ob.volumeVoixIn;
					this.unite = 'mn'
					break;
				
				case "MNT_VOIX_ROAM_OUT":
					this._donnees=ob.montantVoixOut;
					this.unite = ob.devise;
					break;
				
				case "VOL_VOIX_ROAM_OUT":
					this._donnees=ob.volumeVoixOut;
					this.unite = 'mn'
					break;
				
				case "MNT_VOIX_ROAM_IN_OUT":
					this._donnees=ob.montantVoix;
					this.unite = ob.devise;
					break;
				
				case "VOL_VOIX_ROAM_IN_OUT":
					this._donnees=ob.volumeVoix;
					this.unite = 'mn'
					break;
			}
		}
		
		/**
		 * Retourne une devise de type String.
		 */
		public function get devise():String
		{
			return _devise;
		}

		/**
		 * @private
		 */
		public function set devise(value:String):void
		{
			_devise = value;
		}

		/**
		 * Retourne un montant total (in+out, voix+data) de type Number.
		 */
		public function get montantTotal():Number
		{
			return _montantTotal;
		}

		/**
		 * @private
		 */
		public function set montantTotal(value:Number):void
		{
			_montantTotal = value;
		}

		/**
		 * Retourne un volume voix (in+out) de type Number.
		 */
		public function get volumeVoix():Number
		{
			return _volumeVoix;
		}

		/**
		 * @private
		 */
		public function set volumeVoix(value:Number):void
		{
			_volumeVoix = value;
		}

		/**
		 * Retourne un montant voix (in+out) de type Number.
		 */
		public function get montantVoix():Number
		{
			return _montantVoix;
		}

		/**
		 * @private
		 */
		public function set montantVoix(value:Number):void
		{
			_montantVoix = value;
		}

		/**
		 * Retourne un volume voix out (volume d'appels sortants) de type Number.
		 */
		public function get volumeVoixOut():Number
		{
			return _volumeVoixOut;
		}

		/**
		 * @private
		 */
		public function set volumeVoixOut(value:Number):void
		{
			_volumeVoixOut = value;
		}

		/**
		 * Retourne un montant voix out (montant d'appels sortants) de type Number.
		 */
		public function get montantVoixOut():Number
		{
			return _montantVoixOut;
		}

		/**
		 * @private
		 */
		public function set montantVoixOut(value:Number):void
		{
			_montantVoixOut = value;
		}

		/**
		 * Retourne un volume voix in (volume d'appels entrants) de type Number.
		 */
		public function get volumeVoixIn():Number
		{
			return _volumeVoixIn;
		}

		/**
		 * @private
		 */
		public function set volumeVoixIn(value:Number):void
		{
			_volumeVoixIn = value;
		}

		/**
		 * Retourne un montant voix in (montant d'appels entrants) de type Number.
		 */
		public function get montantVoixIn():Number
		{
			return _montantVoixIn;
		}

		/**
		 * @private
		 */
		public function set montantVoixIn(value:Number):void
		{
			_montantVoixIn = value;
		}

		/**
		 * Retourne une donnee roaming de type Number.
		 */
		public function get donnees():Number
		{
			return _donnees;
		}

		/**
		 * @private
		 */
		public function set donnees(value:Number):void
		{
			_donnees = value;
		}

		/**
		 * Retourne un montant data selon le pays de type Number.
		 */
		public function get montantData():Number
		{
			return _montantData;
		}

		/**
		 * @private
		 */
		public function set montantData(value:Number):void
		{
			_montantData = value;
		}

		/**
		 * Retourne un code trigramme de pays de type String.
		 */
		public function get trigramme():String
		{
			return _trigramme;
		}
		
		/**
		 * @private
		 */
		public function set trigramme(value:String):void
		{
			_trigramme = value;
		}

		/**
		 * Retourne un volume data selon le pays de type Number.
		 */
		public function get volumeData():Number
		{
			return _volumeData;
		}

		/**
		 * @private
		 */
		public function set volumeData(value:Number):void
		{
			_volumeData = value;
		}

		/**
		 * Retourne un nom de pays de type String.
		 */
		public function get pays():String
		{
			return _pays;
		}
		
		/**
		 * @private
		 */
		public function set pays(value:String):void
		{
			_pays = value;
		}
		
		public function get unite():String { return _unite; }
		
		public function set unite(value:String):void
		{
			if (_unite == value)
				return;
			_unite = value;
		}
		
	}
}