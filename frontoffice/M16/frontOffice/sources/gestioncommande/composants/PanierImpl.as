package gestioncommande.composants
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Panel;
	import mx.events.FlexEvent;
	
	import composants.util.CommandeFormatter;
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class PanierImpl extends Panel
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var NOMBRE_CONFIGURATION					:int = 0;
		public var MONTANT_COMMANDE_TOTAL_CONFIRMED		:String = "0";
		public var MONTANT_COMMANDE_ENCOURS				:String = "0";
		public var MONTANT_TOTAL				:String = "0";
		public var currenciesAreDifferent				:Boolean = false;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function PanierImpl()
		{
			NOMBRE_CONFIGURATION 		= 0;
			MONTANT_COMMANDE_ENCOURS 	= Formator.formatTotalWithSymbole(0);
			MONTANT_COMMANDE_TOTAL_CONFIRMED 		= Formator.formatTotalWithSymbole(0);
			MONTANT_TOTAL 				= Formator.formatTotalWithSymbole(0);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
		
		public function attributMontantConfirme():void
		{
			var qte		:Number = SessionUserObject.singletonSession.COMMANDEARTICLE.length;			
			var idtype	:int = SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE;

			NOMBRE_CONFIGURATION = qte;
			//1382/1587
			if(idtype == TypesCommandesMobile.RENOUVELLEMENT || idtype == TypesCommandesMobile.RENOUVELLEMENT_FIXE)
			{
				var prixEligible	:Number = SessionUserObject.singletonSession.NBRLIGNEELIGIBLE * SessionUserObject.singletonSession.PRIXELIGIBLE;
				var prixNonEligible	:Number = SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE * SessionUserObject.singletonSession.PRIXNONELIGIBLE;
				var prixAccessoires	:Number = calculAccessoires();
				var nbrLigne		:Number = SessionUserObject.singletonSession.NBRLIGNEELIGIBLE + SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE;
				var total			:Number = prixEligible + prixNonEligible + (nbrLigne*prixAccessoires);
				
				MONTANT_COMMANDE_TOTAL_CONFIRMED 		= Formator.formatTotalWithSymbole(total);
			}
			else
			{
				var prixTotal	:Number = calculTotal();
				MONTANT_COMMANDE_TOTAL_CONFIRMED 		= Formator.formatTotalWithSymbole(prixTotal);
			}
		}
		
		public function attributMontantEnPreparation(stepValidation:Boolean = false):void
		{
			var qte		:Number = SessionUserObject.singletonSession.COMMANDEARTICLE.length;			
			var idtype	:int = SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE;
			
			NOMBRE_CONFIGURATION = qte;
			//1382/1587
			if(idtype == TypesCommandesMobile.RENOUVELLEMENT || idtype == TypesCommandesMobile.RENOUVELLEMENT_FIXE)
			{
				var prixEligible	:Number = SessionUserObject.singletonSession.NBRLIGNEELIGIBLE * SessionUserObject.singletonSession.PRIXELIGIBLE;
				var prixNonEligible	:Number = SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE * SessionUserObject.singletonSession.PRIXNONELIGIBLE;
				var prixAccessoires	:Number = calculAccessoires();
				var nbrLigne		:Number = SessionUserObject.singletonSession.NBRLIGNEELIGIBLE + SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE;
				var total			:Number = prixEligible + prixNonEligible + (nbrLigne*prixAccessoires);
				
				MONTANT_COMMANDE_ENCOURS 	= Formator.formatTotalWithSymbole(total);
			}
			else
			{
				var prixCurrent	:Number = calculCurrent();
				MONTANT_COMMANDE_ENCOURS 	= Formator.formatTotalWithSymbole(prixCurrent);
				if(stepValidation)
					MONTANT_COMMANDE_ENCOURS 	= Formator.formatTotalWithSymbole(0);
					
			}
		}
		
		public function attributMontantTotal(stepValidation:Boolean = false):void
		{
			var qte		:Number = SessionUserObject.singletonSession.COMMANDEARTICLE.length;			
			var idtype	:int = SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE;
			
			NOMBRE_CONFIGURATION = qte;
			//1382/1587
			if(idtype == TypesCommandesMobile.RENOUVELLEMENT || idtype == TypesCommandesMobile.RENOUVELLEMENT_FIXE)
			{
				var prixEligible	:Number = SessionUserObject.singletonSession.NBRLIGNEELIGIBLE * SessionUserObject.singletonSession.PRIXELIGIBLE;
				var prixNonEligible	:Number = SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE * SessionUserObject.singletonSession.PRIXNONELIGIBLE;
				var prixAccessoires	:Number = calculAccessoires();
				var nbrLigne		:Number = SessionUserObject.singletonSession.NBRLIGNEELIGIBLE + SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE;
				var total			:Number = prixEligible + prixNonEligible + (nbrLigne*prixAccessoires);
				
				MONTANT_TOTAL 				= Formator.formatTotalWithSymbole(total);
			}
			else
			{
				var prixTotal	:Number = calculTotal();
				var prixCurrent	:Number = calculCurrent();
				MONTANT_TOTAL 	= Formator.formatTotalWithSymbole(prixTotal);
				if(stepValidation)
					MONTANT_TOTAL 	= Formator.formatTotalWithSymbole(prixTotal);
			}
		}
		
		public function currencyChanged_handler(e:Event):void
		{
			currenciesAreDifferent = false;
			if (CommandeFormatter.getInstance().commandeCurrencySymbol != resourceManager.getString('M16', '_signe_de_la_devise__'))
			{
				currenciesAreDifferent = true;
				return;
			}
		}
		
		protected function panierimpl_creationCompleteHandler(event:FlexEvent):void
		{
			if (!CommandeFormatter.getInstance().hasEventListener("CURRENCY_CHANGED"))
			{
				CommandeFormatter.getInstance().addNewEventListener("CURRENCY_CHANGED", currencyChanged_handler);	
			}
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//
		
		private function calculCurrent():Number
		{
			var nbreTerminaux	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var nbreAccessoires	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES.length;
			//var nbreAbonnements	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ABONNEMENTS.length;
			//var nbreOptions		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.OPTIONS.length;
			
			var nbreConfigurations		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			var qte		:int = 1;
			var i		:int = 0;
			var prix	:Number = 0;
			
			if(nbreConfigurations != 0)
				qte = nbreConfigurations;
			
			for(i = 0; i < nbreTerminaux;i++)
			{
				prix = prix + SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].PRIX;
			}
			
			for(i = 0; i < nbreAccessoires;i++)
			{
				prix = prix + SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES[i].PRIX;
			}
			
			prix = qte * prix;
			return prix;
		}
			
		private function calculTotal():Number
		{
			
			var articles	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var nbreArticles:int = articles.length;
			var total		:Number = 0;
			
			for(var i:int = 0; i < nbreArticles;i++)
			{
				var lengthTerminaux		:int = articles[i].TERMINAUX.length;
				var lengthAccessoires	:int = articles[i].ACCESSOIRES.length;
								
				var qte		:int = articles[i].CONFIGURATIONS.length;
				var j		:int = 0;
				var prix	:Number = 0;
				
				if(qte == 0)
					qte = 1;
				
				for(j = 0; j < lengthTerminaux;j++)
				{
					prix = prix + articles[i].TERMINAUX[j].PRIX;
				}
				
				for(j = 0; j < lengthAccessoires;j++)
				{
					prix = prix + articles[i].ACCESSOIRES[j].PRIX;
				}
				
				prix = qte * prix;
				total = total + prix;
			}
			return total;
		}
		
		private function calculAccessoires():Number
		{
			var cmdart	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var lenart	:int = cmdart.length;
			var prix	:Number = 0;
						
			for(var i:int = 0; i < lenart;i++)
			{
				var eltsCmd:ElementsCommande2 = cmdart[i] as ElementsCommande2;
				
				var lenAcc:int = eltsCmd.ACCESSOIRES.length;
				
				for(var j:int = 0; j < lenAcc;j++)
				{
					prix = prix + eltsCmd.ACCESSOIRES[j].PRIX;
				}
			}
			
			return prix;
		}
		
	}
}