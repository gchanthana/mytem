package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;
	
	public class RuleLigneRenumeroter implements IRuleDisplayItem
	{
		public function RuleLigneRenumeroter()
		{
		}
		
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new IsTypeLigneMobile();
			return r1.isDisplay();
		}
	}
}