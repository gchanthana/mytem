package univers.facturation.suivi.controles.event
{
	import flash.events.Event;

	public class CommandeEvent extends Event
	{
		public static var COMMANDE_GENERALE_COMPELTE:String = "CommandeGeneraleComplete"; 
		
		public function CommandeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new FicheLigneEvent(type,bubbles,cancelable);
		}
	}
}