package entity
{
	public class AppParams
	{
		private var _codeApp:int=0;
		private var _codeConnection:int=0;
		private var _codeModule:String="";
		private var _codeLangue:CodeLangue;
		private var _login:String="";
		private var _mdp:String="";
		private var _token:String="";
		private var _seSouvenir:Boolean=false;
		
		public function AppParams()
		{
		}
/* --------------------------- GETTER / SETTER ------------------------- */		
		public function get login():String
		{
			return _login;
		}
		public function set login(value:String):void
		{
			_login = value;
		}
		public function get mdp():String
		{
			return _mdp;
		}
		public function set mdp(value:String):void
		{
			_mdp = value;
		}
		public function get token():String
		{
			return _token;
		}
		public function set token(value:String):void
		{
			_token = value;
		}

		public function get codeApp():int
		{
			return _codeApp;
		}

		public function set codeApp(value:int):void
		{
			_codeApp = value;
		}

		public function get codeConnection():int
		{
			return _codeConnection;
		}

		public function set codeConnection(value:int):void
		{
			_codeConnection = value;
		}

		public function get codeModule():String
		{
			return _codeModule;
		}

		public function set codeModule(value:String):void
		{
			_codeModule = value;
		}

		public function get codeLangue():CodeLangue
		{
			return _codeLangue;
		}

		public function set codeLangue(value:CodeLangue):void
		{
			_codeLangue = value;
		}

		public function get seSouvenir():Boolean
		{
			return _seSouvenir;
		}

		public function set seSouvenir(value:Boolean):void
		{
			_seSouvenir = value;
		}


	}
}