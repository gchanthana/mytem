package entity
{
	[Bindable]
	public class SiteVO
	{
		private var _id					:Number;
		private var _libelle			:String;
		private var _adresse1			:String;
		private var _adresse2			:String;
		private var _codePostale		:String;
		private var _ville				:String;
		private var _pays				:String;
		private var _adresseFacturation	:int;
		private var _dateDebut			:Date;
		private var _dateFin			:Date;
		private var _isActive			:Boolean;
		
		public function SiteVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_SITE"))
					this.id = obj.IDFC_SITE;	
				
				if(obj.hasOwnProperty("LIBELLE_SITE"))
					this.libelle = obj.LIBELLE_SITE;
				
				if(obj.hasOwnProperty("ADRESSE1"))
					this.adresse1 = obj.ADRESSE1;
				
				if(obj.hasOwnProperty("ADRESSE2"))
					this.adresse2 = obj.ADRESSE2;
				
				if(obj.hasOwnProperty("CODE_POSTAL"))
					this.codePostale = obj.CODE_POSTAL;	
				
				if(obj.hasOwnProperty("VILLE"))
					this.ville = (obj.VILLE != null)?obj.VILLE:'';
				
				if(obj.hasOwnProperty("PAYS"))
					this.pays = (obj.PAYS != null)?obj.PAYS:'';
				
				if(obj.hasOwnProperty("BOOL_ADRESSE_FACTURATION"))
					this.adresseFacturation = obj.BOOL_ADRESSE_FACTURATION;
				
				if(obj.hasOwnProperty("DATE_DEBUT"))
					this.dateDebut = obj.DATE_DEBUT as Date;
				
				if(obj.hasOwnProperty("DATE_FIN"))
					this.dateFin = obj.DATE_FIN as Date;
				
				if(obj.hasOwnProperty("IS_ACTIVE"))
					this.isActive = (obj.IS_ACTIVE == 1)?true:false;
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet SiteVO erroné ");
			}
			
		}
		
		public function get isActive():Boolean { return _isActive; }
		
		public function set isActive(value:Boolean):void
		{
			if (_isActive == value)
				return;
			_isActive = value;
		}
		
		public function get dateFin():Date { return _dateFin; }
		
		public function set dateFin(value:Date):void
		{
			if (_dateFin == value)
				return;
			_dateFin = value;
		}
		
		public function get dateDebut():Date { return _dateDebut; }
		
		public function set dateDebut(value:Date):void
		{
			if (_dateDebut == value)
				return;
			_dateDebut = value;
		}
		
		public function get adresseFacturation():int { return _adresseFacturation; }
		
		public function set adresseFacturation(value:int):void
		{
			if (_adresseFacturation == value)
				return;
			_adresseFacturation = value;
		}
		
		public function get pays():String { return _pays; }
		
		public function set pays(value:String):void
		{
			if (_pays == value)
				return;
			_pays = value;
		}
		
		public function get ville():String { return _ville; }
		
		public function set ville(value:String):void
		{
			if (_ville == value)
				return;
			_ville = value;
		}
		
		public function get codePostale():String { return _codePostale; }
		
		public function set codePostale(value:String):void
		{
			if (_codePostale == value)
				return;
			_codePostale = value;
		}

		public function get adresse2():String { return _adresse2; }
		
		public function set adresse2(value:String):void
		{
			if (_adresse2 == value)
				return;
			_adresse2 = value;
		}
		
		public function get adresse1():String { return _adresse1; }
		
		public function set adresse1(value:String):void
		{
			if (_adresse1 == value)
				return;
			_adresse1 = value;
		}
		
		public function get libelle():String { return _libelle; }
		
		public function set libelle(value:String):void
		{
			if (_libelle == value)
				return;
			_libelle = value;
		}
		
		public function get id():Number { return _id; }
		
		public function set id(value:Number):void
		{
			if (_id == value)
				return;
			_id = value;
		}
	}
}