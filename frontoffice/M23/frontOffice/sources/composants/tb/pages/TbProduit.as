package composants.tb.pages
{
	import composants.tb.tableaux.TableauChangeEvent;
	import mx.events.FlexEvent;
	import flash.display.DisplayObject;
	import mx.core.UIComponent;
	import composants.tb.tableaux.TableauAboFactoryStratey;
	import composants.tb.tableaux.TableauConsoFactoryStrategy;
	import composants.tb.graph.IGraphModel;
	import composants.tb.tableaux.ITableauModel;
	import composants.tb.tableaux.TableauFactory;
	import mx.containers.Grid;
	import mx.controls.DataGrid;
	import mx.events.DataGridEvent;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	 
	import flash.events.Event;
	import flash.net.URLRequestMethod;
	import mx.utils.ObjectUtil;
	import mx.controls.Alert;
	
	public class TbProduit extends TbProduit_IHM implements IPage
	{
		private var _tf : TableauFactory = new TableauFactory();
	 
	 	private const _pageType : String = "PRODUIT";
		
		private var _tb : ITableauModel;
	 
		private var lastMoisFin : String = "x";
		
		private var lastMoisDebut : String = "x";		
		 
		private var moisDeb : String;
		 
		private var moisFin : String;
	 
	 	private var perimetre : String;
		
		private var modeSelection : String;
		
		private var _nextPageType : String; 
		
		private var _pageLibelle : String;
		
		private var _mode : String;
		
		private var _goToNextPage : Function ;
		
		private var lastIdentifiant : String = "z";
		
		private var identifiant : String;
				
		private var _type : TableauChangeEvent;
		
		private var _total : Number;
		
		/**
		* Constructeur 
		**/
		public function TbProduit(type : TableauChangeEvent = null)
		{
			super();
			_type = type;
			_mode = type.TYPE_THEME;	
			_pageLibelle = "Produit : "+ type.LIBELLE;						
			addEventListener(FlexEvent.CREATION_COMPLETE,init);					
		}
			
				
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function get nextPageType():String{
			//TODO: implement function
			return _nextPageType;
		}
		
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function set nextPageType(type : String):void{
			_nextPageType = type;
		}
		
		public function get pageType():String{
			return _pageType;
		}
		
		
		public function clean():void
		{
			//TODO: implement function
		}
		
		public function getLibelle():String
		{
			//TODO: implement function
			return "Détail du produit";
		}
		
		public function getTotal():Number
		{
			//TODO: implement function
			return _total;
		}			
				
		public function updateSegment(segment : String = ""):void
		{
			
		}
		
		public function goToNextPageHandler(f : Function):void{
			_goToNextPage = f;								
			
		}
		
		public function update():void{
			if (lastMoisDebut.concat(lastMoisFin).concat(lastIdentifiant) != moisDeb.concat(moisFin).concat(identifiant)){
				_tb.update(); 								
				lastMoisDebut = moisDeb;
				lastMoisFin = moisFin;
				lastIdentifiant = identifiant;
			}	
		}
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;			
			_tb.updatePeriode(moisDeb,moisFin);
			 
		}	
		
		/**
		 * Permet d'exporter un rapport flash,pdf ou excel 
		 * @param format Le format (flashpaper, excel ou pdf)
		 * */
		public function exporterRapport( format: String, rSociale : String = ""):void{			
			var template : String;				 	
            var variables : URLVariables = new URLVariables();
            
			if (perimetre.toLowerCase() == "soustete"){
				template = "/fr/consotel/consoview/cfm/tb/pdf/produit/print_document.cfm";	
				 variables.numero = _type.IDENTIFIANT;
			}else{
				template = "/fr/consotel/consoview/cfm/tb/pdf/produit/print_document_cat.cfm";	
				variables.numero = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			}
			 			
			var urlback : String = cv.NonSecureUrlBackoffice+template;		
            variables.perimetre = perimetre;         
            variables.raisonsociale = rSociale;
            variables.idproduit_client = _type.IDPRODUIT;
            variables.format = format;
            variables.tb = _type.SEGMENT;
            variables.datedeb = moisDeb;
            variables.datefin = moisFin;
            variables.modeCalcul = modeSelection;
            variables.mode = _mode.toUpperCase();
          	 
            var request:URLRequest = new URLRequest(urlback);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            navigateToURL(request,"_blank");
           
            
		}
		 
			
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null , idt : String= null):void
		{
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;	
			this.identifiant = idt
			_tb.updatePerimetre(perimetre,modeSelection);						
		}
		
		private function setTabFactoryStrategy():void
		{
			//TODO: implement function
			switch(_mode.toUpperCase()){
				case "ABONNEMENTS" : _tf.setStrategy(new TableauAboFactoryStratey());break;
				case "CONSOMMATIONS" : _tf.setStrategy(new TableauConsoFactoryStrategy());break;
				default : _tf.setStrategy(new TableauAboFactoryStratey());break;
			}
		}
		
		//initialisation de l'IHM
		private function init(fe:FlexEvent):void{
			setTabFactoryStrategy();
								
			_tb = _tf.createTableau(_type);							
			
			 
			
			//Affichage des elements		
			myTabContener.addChild(DisplayObject(_tb));		
		 
			//affectation des Handlers et des listeners			
			UIComponent(_tb).addEventListener("tableauChange",_goToNextPage);
		
		}	
						
	}
}