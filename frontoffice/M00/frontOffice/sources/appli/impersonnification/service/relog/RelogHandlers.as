package appli.impersonnification.service.relog
{
	import appli.impersonnification.event.PaginateListEvent;
	import appli.impersonnification.event.RelogEvent;
	import appli.impersonnification.refreshManager;
	import appli.impersonnification.service.ListServices;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import general.abstract.AbstractModuleLoader;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class RelogHandlers extends EventDispatcher
	{
		private var listServices:ListServices;
		public var myDatas:RelogDatas;
		private var moduleLoader:AbstractModuleLoader = new AbstractModuleLoader();
		
		public function RelogHandlers(datas:RelogDatas)
		{
			myDatas = datas;
		}


		
		public function getImpersonnificationHandler(re:ResultEvent):void
		{
			myDatas.impersonnification = re.result;
			trace("Valeur de l'impersonnification : " + re.result);
		}
		
		public function getCodeApplicationHandler(re:ResultEvent):void
		{
			trace("code de l'application réceptionné");
			myDatas.codeApp = parseInt(re.result.toString());
			trace("valeur de codeApp : " + myDatas.codeApp);
			this.dispatchEvent(new RelogEvent(RelogEvent.CODEAPP_RECIEVED));
		}
		
		public function setImpersonnificationHandler(value:Boolean):void
		{
			trace("Changement de session : " + value.toString());
			myDatas.impersonnification = true;
			refreshManager.impersonnification = true;
		}
		public function stopImpersonnificationHandler(value:Boolean):void
		{
			trace("Arret de l'impersonnification. Nouvelle valeur : " + value.toString());
		}
			
		public function updateCvHandler(re:ResultEvent):void
		{
			myDatas.infosSession = re.result;
			this.dispatchEvent(new RelogEvent(RelogEvent.SESSION_CHANGED));
			trace(re.result.user.PRENOM);//Si une erreur est repérée ici, checker le codeConnection utilisé.
			
		}
		
		public function processInitTousLesLogins(evt:Event):void
		{
			dispatchEvent(new RelogEvent(RelogEvent.INIT_LOGINS));
		}
		
		public function erreur(evt:Event):void
		{
			dispatchEvent(new RelogEvent(RelogEvent.LISTLOGINS_ERROR));
		}

	}
}