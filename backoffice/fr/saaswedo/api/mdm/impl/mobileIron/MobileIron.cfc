<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.mobileIron.MobileIron" extends="fr.saaswedo.api.mdm.impl.MdmServer"
hint="Implémentation d'un serveur MobileIron. L'implémentation IRemoteClient par défaut est fr.saaswedo.api.mdm.impl.mobileIron.RestClient
<br>L'implémentation IDeviceInfo est fr.saaswedo.api.mdm.impl.mobileIron.DeviceInfo
<br>L'implémentation IDeviceManagement est fr.saaswedo.api.mdm.impl.mobileIron.DeviceManagement">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.mobileIron.MobileIron">
		<cfreturn "fr.saaswedo.api.mdm.impl.mobileIron.MobileIron">
	</cffunction>
	
	<cffunction access="public" name="checkConnection" returntype="Struct" hint="Vérifie la connexion avec le serveur MDM et retourne une structure
	contenant :<br>- IS_TEST_OK: TRUE si la connexion en succès<br>- TEST_MSG: Message explicatif lié à la vérification de la connexion">
		<cfset var checkResult={IS_TEST_OK=createObject("java","java.lang.Boolean").init("false"),TEST_MSG="NONE"}>
		<cftry>
			<cfset var httpResponse={}>
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset var infos=getMdmServerInfo()>
			<cfset var service={
				endpoint=infos.getEndpoint() & "/api/v1/sm/authentication",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfset var params=[
				{type="header",name="accept",value="application/json"},
				{TYPE="formfield",NAME="username",VALUE=service.username},
				{TYPE="formfield",NAME="Password",VALUE=service.password}
			]>
			<cfset service.HTTP_METHOD=remoting.HTTP_POST()>
			<cfset var result=remoting.invoke(service,"",params)>
			<!--- Erreur durant le reporting --->
			<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
				<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset var httpCode=remoting.getResponseHttpCode(result)>
				<cfset httpResponse=result[remoting.RESULT()]>
				<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
				<cfset device.RESULT=result[remoting.RESULT()]>
				<!--- La connexion est en succès si le code HTTP est 200 ou 401 (username/password invalide)--->
				<cfif (httpCode EQ 200) OR (httpCode EQ 401)>
					<cfset checkResult.IS_TEST_OK=createObject("java","java.lang.Boolean").init("true")>
				<!--- La connexion est en echec sinon (e.g 404,500,etc...) --->
				<cfelse>
					<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfif structKeyExists(httpResponse,"errorDetail")>
						<cfset checkResult.TEST_MSG=httpResponse.errorDetail>
					<cfelseif structKeyExists(httpResponse,"statusCode")>
						<cfset checkResult.TEST_MSG=httpResponse.statusCode>
					<cfelse>
						<cfset checkResult.TEST_MSG="MDM server returned HTTP Code : #httpCode#">
					</cfif>
				</cfif>
			</cfif>
			<cfreturn checkResult>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfset checkResult.TEST_MSG=CFCATCH.message>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfreturn checkResult>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="PROVIDER" returntype="String" hint="Retourne MobileIron">
		<cfreturn "MobileIron">
	</cffunction>

	<cffunction access="public" name="getEnrollmentURL" returntype="String" hint="Retourne le endpoint MDM si aucune URL ne correspond aux paramètres">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<cfargument name="requireAgent" type="Boolean" required="false" default="true" hint="TRUE pour un enrollment qui requiert un agent MDM">
		<cfset var os=ARGUMENTS.osFamily>
		<cfset var urlSuffix="">
		<cfset var mdmInfos=getMdmServerInfo()>
		<cfif os EQ mdmInfos.ANDROID()>
			<cfset urlSuffix="/c/d/android.html">
		<cfelseif os EQ mdmInfos.IOS()>
			<cfset urlSuffix="/c/d/ios.html">
		</cfif>
		<cfreturn mdmInfos.getEndpoint() & urlSuffix>
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentServer" returntype="String" hint="Retourne le DNS ou l'IP du serveur d'enrollment">
		<cfset var mdmInfos=getMdmServerInfo()>
		<cfset var endpoint=mdmInfos.getEndpoint()>
		<cfset var mdmServerURL=createObject("java","java.net.URL").init(endpoint)>
		<cfreturn mdmServerURL.getHost()>
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentServerPort" returntype="Numeric" hint="Retourne le numéro de port du serveur d'enrollment">
		<cfreturn 50621>
	</cffunction>

	<!--- =========== Fonctions de gestion des services du serveur =========== --->
	
	<!--- La fonction init() qui appelle cette fonction avec un verrou CFLOCK exclusif --->
	<cffunction access="private" name="initMdmServices" returntype="void" hint="Instancie et initialise les implémentations des services">
		<!--- API d'accès aux infos des devices --->
		<cfset setIDeviceInfo(new fr.saaswedo.api.mdm.impl.mobileIron.DeviceInfo(THIS))>
		<!--- API de gestion des devices --->
		<cfset setIDeviceManagement(new fr.saaswedo.api.mdm.impl.mobileIron.DeviceManagement(THIS))>
	</cffunction>
</cfcomponent>