package univers.parametres.gestiondesdroitsdefacturation.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Panel;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestiondesdroitsdefacturation.system.GestionDroitClass;

	public class GestionUtilisateur extends VBox
	{
		public var dgUser:DataGrid;
		public var dgProfil1:DataGrid;
		public var dgProfil2:DataGrid;
		public var dgProfil3:DataGrid;
		public var adgAfficheProfil:AdvancedDataGrid;
		public var txtFiltre:TextInput;
		public var cboListeRacine:ComboBox; 
		public var pnlProfil:Panel;
		
		private var o:Object=new Object();
		private var aucun:Boolean=false;
		private var gestionUtilisateurClass:GestionDroitClass;
		
		
		[Bindable(event="racineChange")]	
		private var _idracine:int;	
		protected function get idracine():int{
			return _idracine;
		}			
		protected function set idracine(value:int):void{
			if(value != _idracine){
				_idracine= value;
				dispatchEvent(new Event("racineChange"));
			}
		}			
			
		[Event("profilEvent")]
				
		[Bindable]
		public var origine:String="";
		
		[Bindable]
		protected var tabProfil:ArrayCollection=new ArrayCollection();
		
		[Bindable]
		protected var tabListeProfil:ArrayCollection=new ArrayCollection();
		
		[Bindable]
		protected var tabUtilisateur:ArrayCollection=new ArrayCollection();
		
		[Bindable]
		protected var tabRacine:ArrayCollection=new ArrayCollection();
		
		public function GestionUtilisateur()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		protected function init(e:FlexEvent):void{
			gestionUtilisateurClass= new GestionDroitClass();
			cboListeRacine.addEventListener(ListEvent.CHANGE,cboListeRacineChangeHandler);
			txtFiltre.addEventListener(Event.CHANGE,filtrer);
			txtFiltre.addEventListener(MouseEvent.ROLL_OVER,filtreReset);
			this.addEventListener("racineChange",RacineChangeHandler);
			
			if(origine=="cv"){
				refreshGestionUtilisateur();
			}
						
			switch(this.origine.toLowerCase()){
				case "intranet":
						cboListeRacine.visible= true;
						gestionUtilisateurClass.getListeRacine(tmpListeRacineYes);	
						break;
				case "cv" :
						gestionUtilisateurClass.getListeLogin(tmpUserYes,-1);
						cboListeRacine.visible= false;
						idracine=0;
						break;
			}
		}
		
		protected function cboListeRacineChangeHandler(e:ListEvent):void{
			idracine= cboListeRacine.selectedItem.IDGROUPE_CLIENT;
		}
		
		protected function RacineChangeHandler(e:Event):void{
			refreshGestionUtilisateur();
		}

		private function tmpListeRacineYes(e:ResultEvent):void{
			tabRacine= e.result as ArrayCollection;
			tabRacine.refresh();
		}
		
		private function tmpListeProfileYes(e:ResultEvent):void{
			creerListeProfil((e.result as ArrayCollection));
		}

		private function tmpUserYes(e:ResultEvent):void{
			tabUtilisateur= e.result as ArrayCollection;
			tabUtilisateur.refresh();
		}

		protected function dispAffecterClickHandler(e:MouseEvent):void{
			if(dgUser.selectedIndex > -1){
				if(adgAfficheProfil.selectedIndex > -1){
					if(aucun){
						gestionUtilisateurClass.addProfil(affecterYes
															   ,parseInt(dgUser.selectedItem.APP_LOGINID)
															   ,parseInt(adgAfficheProfil.selectedItem.IDPROFIL)
															   ,idracine);
					}else{
						ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M21', 'Etes_vous_sur_de_vouloir_modifier_le_pro')+dgUser.selectedItem.LENOM+ResourceManager.getInstance().getString('M21', '_'),ResourceManager.getInstance().getString('M21', 'Confirmation'),modifCloseEvent);						
					}
				}else{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Veuillez_s_lectionner_un_profil___affect'),ResourceManager.getInstance().getString('M21', 'Erreur_de_s_lection'),null);
				}
			}else{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Veuillez_s_lectionner_un_utilisateur'), ResourceManager.getInstance().getString('M21', 'Erreur_de_s_lection'),null);
			}
		}
		
		private function modifCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				gestionUtilisateurClass.addProfil(affecterYes
													,parseInt(dgUser.selectedItem.APP_LOGINID),parseInt(adgAfficheProfil.selectedItem.IDPROFIL)
													,idracine);
			}
		}
		
		private function affecterYes(e:ResultEvent):void{
			gestionUtilisateurClass.okProfil(okProfilYes,parseInt(dgUser.selectedItem.APP_LOGINID),idracine);			
		}
		
		protected function dispDesaffecterClickHandler(e:MouseEvent):void{
			if(dgUser.selectedIndex > -1){
				aucun=true;
				gestionUtilisateurClass.removeProfil(removeProfilYes,parseInt(dgUser.selectedItem.APP_LOGINID),parseInt(o.IDPROFIL))
			}else{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Veuillez_s_lectionner_un_utilisateur_ava'),ResourceManager.getInstance().getString('M21', 'erreur'),null);
			}
		}	
		
		private function removeProfilYes(e:ResultEvent):void{
			creerProfil(e.result as ArrayCollection);
			pnlProfil.title=ResourceManager.getInstance().getString('M21', 'Profil');
		}
		
		protected function dgUserItemClick(e:ListEvent):void{
			gestionUtilisateurClass.okProfil(okProfilYes,parseInt(dgUser.selectedItem.APP_LOGINID),idracine);
		}		
		
		private function okProfilYes(e:ResultEvent):void{
			var i:int= e.result as int;
			if(i > -1){
				o.IDPROFIL=i;
				gestionUtilisateurClass.getProfil(getProfilYes,i);
				aucun=false;	
			}else{
				pnlProfil.title=ResourceManager.getInstance().getString('M21', 'Profil');
				tabProfil.removeAll();
				initO();
				tabProfil.addItem(o);
				tabProfil.refresh();
				aucun=true;
			}
		}
		
		private function getProfilYes(e:ResultEvent):void{
			creerProfil(e.result as ArrayCollection);
			pnlProfil.title=ResourceManager.getInstance().getString('M21', 'Profil___')+o.PROFIL_NAME;
		}
		
		protected function filtrer(e:Event):void{
			(dgUser.dataProvider as ArrayCollection).filterFunction= filtreProduit;
			(dgUser.dataProvider as ArrayCollection).refresh();
		}
		
		private function filtreProduit(item:Object):Boolean{
			if(String(item.LENOM).match(new RegExp(txtFiltre.text,'i')) || String(item.LOGIN_EMAIL).match(new RegExp(txtFiltre.text,'i'))) return true;
			else return false;
		}
				
		private function filtreReset(e:MouseEvent):void{
			txtFiltre.text="";
		}
		
		private function creerListeProfil(t:ArrayCollection):void{
			tabListeProfil.removeAll();
			for(var i:int=0;i<t.length;i+=11){
				var o:Object= new Object();
				o.obj1= t[i+0].BTN_RIGHT;
				o.obj2= t[i+1].BTN_RIGHT;
				o.obj3= t[i+2].BTN_RIGHT;
				o.obj4= t[i+3].BTN_RIGHT;
				o.obj5= t[i+4].BTN_RIGHT;
				o.obj6= t[i+5].BTN_RIGHT;
				o.obj7= t[i+6].BTN_RIGHT;
				o.obj8= t[i+7].BTN_RIGHT;
				o.obj9= t[i+8].BTN_RIGHT;
				o.obj10= t[i+9].BTN_RIGHT;
				o.obj11=t[i+10].BTN_RIGHT;
				o.IDPROFIL= t[i].IDPROFIL;
				o.PROFIL_NAME=t[i].PROFIL_NAME;
				o.EDITABLE=false;
				tabListeProfil.addItem(o);
			}
			tabListeProfil.refresh();				
		}
		
		private function creerProfil(t:ArrayCollection):void{
			tabProfil.removeAll();
			initO();
			if(t != null){
				for(var i:int=0;i<t.length;i++){
					switch(t[i].BOUTON_CODE.toString()){
						case "B1":
							o.obj1= 1;
							break;
						case "B2":
							o.obj2= 1;
							break;
						case "B3":
							o.obj3= 1;
							break;
						case "B4":
							o.obj4= 1;
							break;
						case "B5":
							o.obj5= 1;
							break;
						case "B6":
							o.obj6= 1;
							break;
						case "B7":
							o.obj7= 1;
							break;
						case "B8":
							o.obj8= 1;
							break;
						case "B9":
							o.obj9= 1;
							break;
						case "B10":
							o.obj10= 1;	
							break;	
						case "B11":
							o.obj11= 1;	
							break;			
					}
				}
			}
			tabProfil.addItem(o);
			tabProfil.refresh();				
		}		
		
		private function initO():void{
			o.obj1= 0;
			o.obj2= 0;
			o.obj3= 0;
			o.obj4= 0;
			o.obj5= 0;
			o.obj6= 0;
			o.obj7= 0;
			o.obj8= 0;
			o.obj9= 0;
			o.obj10= 0;
			o.obj11= 0;
			o.PROFIL_NAME=ResourceManager.getInstance().getString('M21', 'Aucun_droit');
			o.EDITABLE=false;
		}
		
		public function refreshGestionUtilisateur():void{
			gestionUtilisateurClass.getListeLogin(tmpUserYes,-1);
					
			gestionUtilisateurClass.getListeProfils(tmpListeProfileYes,-1);
		}
	}
}