package catalogue.ihm.importequipement
{
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.fiche.DetailProduitIHM;
	import catalogue.ihm.fiche.FicheEquipementIHM;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.service.ImportService;
	import catalogue.service.SegmentService;
	import catalogue.util.DataGridDataExporter;
	import catalogue.vo.ClasseVO;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	import catalogue.vo.SegmentVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;
	import searchpaginatedatagrid.SearchPaginateDatagrid;
	import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;
	
	[Bindable]
	public class ImportSourceIsRevendeurImpl extends TitleWindow
	{
		//Public UI componant
		public var comboType : ComboBox;
		public var comboClasse : ComboBox;
		public var comboConstructeur : ComboBox;
		public var comboSegment		 : ComboBox;
		public var comboExport : ComboBox;
		public var myPaginatedatagrid : PaginateDatagrid;
		public var titre : String;
		public var idClient : Number;
		public var idRevendeur : Number;
		private var _popDetailEquipement : FicheEquipementIHM;
		public var importService : ImportService = new ImportService();
		public var segmentService : SegmentService = new SegmentService();
		private var boolContinuer : Boolean;
		public var cbAjouterEquipOtherCat : CheckBox;
		public var cbPrevenirClient : CheckBox;
		public const NBITEMPARPAGE : int = 30;
		public var mySearchPaginateDatagrid : SearchPaginateDatagrid;
		private var _popUpFilter:PopupFilterIHM = new PopupFilterIHM();
		
		public function ImportSourceIsRevendeurImpl()
		{
			segmentService.getAllSegment();	
		}
		/**
		 * 
		 * Handler : SearchPaginateDatagridEvent.SEARCH
		 */
		public function onSearchHandler(event:SearchPaginateDatagridEvent):void
		{
			rechercher(event.parametresRechercheVO);
		}
		/**
		*
		* Handler : PaginateDatagridEvent.PAGE_CHANGE
		* Componante 
		* Procédure getData : avec Segment (page cible) + nombre d'items par page
		* Limite haute = segement * nombre d'items par page
		* 
		* */
		public function onIntervalleChangeHandler(evt : PaginateDatagridEvent):void
		{
			rechercher(mySearchPaginateDatagrid.parametresRechercheVO,myPaginatedatagrid.currentIntervalle);
		}
		public function fermer(event : Event=null):void
		{
			PopUpManager.removePopUp(this);
		}
		public function showDetailHandler(equip : Object):void
		{
			var pop : DetailProduitIHM = new DetailProduitIHM();
			pop.equipement = equip as EquipementCatalogueVO;
			PopUpManager.addPopUp(pop,this,true);
			PopUpManager.centerPopUp(pop);
		}
		 public function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
			
			if(comboConstructeur.selectedItem)
			{
				//Selectionne 'tous'
				if(comboClasse.selectedIndex==-1)
				{
					comboClasse.selectedIndex = 0;
				}
				comboTypeHandler();
							
				if(!itemIntervalleVO) // Si c'est une nouvelle recherche
				{
					myPaginatedatagrid.refreshPaginateDatagrid(true);
					itemIntervalleVO = new ItemIntervalleVO();
					itemIntervalleVO.indexDepart = 0;
					itemIntervalleVO.tailleIntervalle = NBITEMPARPAGE;
				}
				
				//Ajout des 3 paramêtres spécifique au catalogue :
				var parametreSearchCatalogueVO : ParametreSearchCatalogueVO = new ParametreSearchCatalogueVO();
				parametreSearchCatalogueVO.IDCLASSE =(comboClasse.selectedItem as ClasseVO).ID_CLASSE;
				parametreSearchCatalogueVO.IDCONSTRUCTEUR =(comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;
				parametreSearchCatalogueVO.IDTYPEEQUIPEMENT =(comboType.selectedItem as TypeEquipementVO).ID_TYPE;
				parametreSearchCatalogueVO.ORDER_BY=parametresRechercheVO.ORDER_BY;
				parametreSearchCatalogueVO.SEARCH_TEXT=parametresRechercheVO.SEARCH_TEXT;
				parametreSearchCatalogueVO.GROUPE_INDEX =CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
				parametreSearchCatalogueVO.IDREVENDEUR = idRevendeur;	
				parametreSearchCatalogueVO.IDCLIENT = idClient;		
				parametreSearchCatalogueVO.IDSEGMENT = 	(comboSegment.selectedItem as SegmentVO).IDSEGMENT;
				importService.rechercheListeEquipementRevendeur(parametreSearchCatalogueVO,itemIntervalleVO);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Un_constructeur_doit__tre_s_lectionn__'),ResourceManager.getInstance().getString('M24', 'Attention'));	
			}
		}
		public function btExportHandler(evt : PaginateDatagridEvent):void
		{
			if(comboExport.selectedItem)
			{
				var dataExp : Object;
				if(comboExport.selectedIndex == 0)
				{
					dataExp = myPaginatedatagrid.selectedItems;
				}
				else if(comboExport.selectedIndex == 1)
				{
					dataExp = myPaginatedatagrid.dataprovider;
				}
				if(dataExp.length > 0) 
				{     
					var dataToExport  :String = DataGridDataExporter.getCSVString(myPaginatedatagrid.dgPaginate,dataExp,";","\n",false);
					var url :String = moduleCatalogueIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/images/exportCSVString.cfm";
					DataGridDataExporter.exportCSVString(url,dataToExport,"export");
				  }
				  else
				  {
				        Alert.show(ResourceManager.getInstance().getString('M24', 'Pas_de_donn_es___exporter__'),ResourceManager.getInstance().getString('M24', 'Consoview'));
				  }
            }
		}
		public function initData():void
		{
			importService.addEventListener(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,liste_construct_complete_handler);
			importService.rechercheContructeur(true);
			importService.addEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);
			importService.rechercheClasseEquipement(true);
			//Par defaut, le tri se fait sur le modèle:			
			mySearchPaginateDatagrid.comboRecherche.selectedIndex = 5; // Sélectionne modèle dans la combo Clé de recherche
			mySearchPaginateDatagrid.comboTrieColonne.selectedIndex = 5; // Sélectionne modèle dans la combo ORDERBY
		}
		
		private function listeClasseEquipementCompleteHandler(evt:CatalogueEvent):void
		{
			if(importService.hasEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE))
			{
				importService.removeEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);	
			}
			
			comboClasseHandler();	
		}
		
		
		private function liste_construct_complete_handler(evt : CatalogueEvent):void
		{
			comboConstructeur.selectedIndex = 0;
			mySearchPaginateDatagrid.txtCle.setFocus();
		}
		public function comboTypeHandler():void
		{
			if(comboType.selectedIndex == -1)
			{
				if(!(comboType.dataProvider as ArrayCollection).getItemAt(0))
				{
					var  specialitem : TypeEquipementVO = new TypeEquipementVO();
					specialitem.ID_TYPE=-1;
					specialitem.LIBELLE_TYPE = ResourceManager.getInstance().getString('M24','Tous');
					(comboType.dataProvider as ArrayCollection).addItemAt(specialitem,0);
					
				}
				comboType.selectedIndex=0;
			}
		}
		public function comboClasseHandler():void
		{
			if(comboClasse.selectedIndex == -1)
			{
				comboClasse.selectedIndex=0;
			}
			importService.rechercheTypeEquipement((comboClasse.selectedItem as ClasseVO).ID_CLASSE,true);
		}
		public function comboConstructeurHandler():void
		{
			if(comboConstructeur.selectedItem)
			{
				resetSelection((comboConstructeur.dataProvider as ArrayCollection).source);
				comboConstructeur.selectedItem.SELECTED = true;
			}
		}
	
		private function addEquipementCompleteHandler(evt : PopUpCatalogueEvent):void
		{
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboConstructeur.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		public function btValiderContinuerHandler(boolContinuerValue : Boolean):void
		{
			boolContinuer = boolContinuerValue;
			if(myPaginatedatagrid.selectedItems.length>0)
			{
				importService.addEventListener(CatalogueEvent.IMPORT_EQUIPEMENT_COMPLETE,importEquipementCompleteHandler)
				importService.importXEquipInCatClient(idClient,ObjectUtil.copy(myPaginatedatagrid.selectedItems) as ArrayCollection);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Aucun__quipement_s_lectionn_'));
			}
		}
		public function importEquipementCompleteHandler(evt : CatalogueEvent):void
		{
			dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Equipements_import_s_'));
			rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			
			if(!boolContinuer)
			{
				fermer();
			}
		}
		
		public function btImgClickHandler():void
		{
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.LIBELLE_ITEM = 'LIBELLE_CONSTRUCTEUR';
			_popUpFilter.ID_ITEM = 'ID_CONSTRUCTEUR';
			_popUpFilter.listeFilter =  ObjectUtil.copy(importService.listeConstructeur) as ArrayCollection;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_constructeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', _popUpFilterValidHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		private function _popUpFilterValidHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', _popUpFilterValidHandler);
			resetSelection((comboConstructeur.dataProvider as ArrayCollection).source);
			comboConstructeur.selectedIndex = _popUpFilter.getItemCurrentIndex();
			comboConstructeur.selectedItem.SELECTED = true;
			
		}
		private function resetSelection(tab:Array):void
		{
			if(tab)
			{
				var len:Number =  tab.length;
				for(var index:Number = 0; index > len; index++)
				{
					tab[index].SELECTED = false;
				}
			}
		}
		
		
		
	}
}