package univers.parametres.parametrelogin.util
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	
	public class UtilFunction extends EventDispatcher
	{
		
		private var unAcces : UtilAcces;
		private var listeAccesTraite : ArrayCollection = new ArrayCollection();
		private var arbreAccesInterdit : ArrayCollection = new ArrayCollection();
		
		
		public function UtilFunction(){
			
		}
		public function newAcces(id : int):void{
				unAcces=new UtilAcces(id);
				termineTraitement();
		}
		public function ajouterUnPere(id : int):void{
				unAcces.ajouteUnPere(new UtilAcces(id));
		}
		public function termineTraitement():void{
			listeAccesTraite.addItem(unAcces);
		}
		//Cette méthode return une liste d'accès unique
		//Traitement effectué : Supprimer les doublons d'une liste
		public function result():ArrayCollection{
			var size : int = listeAccesTraite.length;
			var listeSansDoublon : ArrayCollection = new ArrayCollection();
				
			var existe : Boolean = false;
			for (var i : int=0;i< size;i++){
				existe = false;
				
				for (var a : int =0;a<listeSansDoublon.length;a++){
					if((listeAccesTraite.getItemAt(i) as UtilAcces).getId()==(listeSansDoublon.getItemAt(a) as UtilAcces).getId()){
						existe = true;
					}
				}
				if (existe == false){
					listeSansDoublon.addItem(listeAccesTraite.getItemAt(i));
				}
			}
			return listeSansDoublon;
		} 
		
		/*
		Cette méthode parcourt le XML. 
		Création d’une collection d’objet Acces
		+ Suppresion des doublons de la collection avec la méthode result()
		*/
		public function donneListeAccesInterdit(TabArbreAccesInterdit : Array):void{
					listeAccesTraite.removeAll();
				//this.arbreAccesInterdit = arbreAccesInterdit;
				trace("-taille de array --->  "+TabArbreAccesInterdit.length);
				
				for (var i : int; i < TabArbreAccesInterdit.length;i++){
					rechercherLesFils(TabArbreAccesInterdit[i] as XML);
				}
				dispatchEvent(new GestionLoginEvent(GestionLoginEvent.TRAITEMENT_ARBRE_COMPLETE));
				
				
			//	rechercherLesFils(arbreAccesInterdit);
			
		}
		
	private function rechercherLesFils(unNoeud :XML):void{
		newAcces(unNoeud.@NID);
		var xmlListDesNoeuds : XMLList = unNoeud.children();
		for(var i:int=0;i<xmlListDesNoeuds.length();i++)
		{	
			
			rechercheAccesInArbre(xmlListDesNoeuds[i]);
		}	
	}
	private function rechercheAccesInArbre (node:XML):void{
		for each(var uneRacine:XML in node){
			
					newAcces(uneRacine.@NID);
									
					
					var objPereAtester : Object = uneRacine; 	
					var continuer : Boolean = true;
					
				
	 				rechercherLesFils(node);
	 		
		}
	}
		
		
			
		}
	}
