package univers.inventaire.inventaire.etapes
{
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	
	
	/**
	 * Classe evenement signifiant la cloture d'une Opération
	 * Permet le passage de parametres relatif à  la cloture
	 * */
	public class CloturerOperationEvent extends Event
	{
		//l'identifiant de l'opération à cloturer
		private var _idOperation : int;
		
		//le numéro de l'operation
		private var _numOperation : String;
		
		//le type d'opération
		private var _typeOperation : int;
		
		//les ressources de l'opération 
		private var _tabProduit : Array;
		
		//la date de reference pour le calcul des régules
		private var _dateRef : String;
		
		//le libelle de l'opération
		private var _libelle : String;
		
		//l'idgroupe client auquel appartient l'opération
		private var _idGroupeClient : int;
		
		//Constante definissant le type d'evenement cloturer avec Verification
		public static const AVEC_RAPPROCHEMENT : String = "avecRapprochement";

		//Constante definissant le type d'evenement cloturer sans Verification		
		public static const SANS_RAPPROCHEMENT : String = "sansRapprochement";
		
		
		/**
		 * Constructeur
		 * voir la classe flash.events.Event
		 * */
		public function CloturerOperationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		/**
		 * Setter pour l'idGroupe client
		 * @param idgc l'idGoupeClient de l'opération
		 * */
		public function set idGroupeClient(idgc : int):void{
			_idGroupeClient = idgc;
		}
		
		
		/**
		 * Setter pour l'identifiant de l'operation à cloturer
		 * @param idop l'identifiant de l'opération
		 * */
		public function set idOperation(idop : int):void{
			_idOperation = idop;
		}
		
		
		/**
		 * Setter pour le numéro de l'operation à cloturer
		 * @param numop le numéro de l'opération
		 * */
		public function set numOperation(numop : String):void{
			_numOperation = numop;
		}
		
		
		/**
		 * Setter pour la date de reference pour le calcul des regules
		 * @param date la date de reference
		 * */
		public function set dateRef(date : String):void{
			_dateRef = date;
		}
		
		
		/**
		 * Setter pour le type d'opération que l'on veut cloturer
		 * @param tyop le type d'opération 
		 * */
		public function set typeOperation(typop : int):void{
			_typeOperation = typop;
		}
		
		
		/**
		 * Setter pour le tableau des ressources
		 * @param tab le tableau contenant les identifiants des ressources pour l'opération
		 * */
		public function set tabProduit(tab : Array):void{
			_tabProduit = tab;
		}
		
		
		/**
		 * Setter pour le libelle de l'opération
		 * @param lib le libelle de l'opération
		 * */
		public function set libelle(lib : String):void{
			_libelle = lib;
		}
		
		
		/**
		 * Retourne le libelle de l'opération
		 * @return _libelle , le libelle de l'operation 
		 * */
		public function get libelle():String{
			return _libelle;
		}
		
		/**
		 * Retourne l'identifiant de l'opération
		 * @return _idOperation l'identifiant de l'opération
		 * */
		public function get idOperation():int{
			return _idOperation;
		}
		
		
		/**
		 * Retourne le numéro de l'operation
		 * @return _numOperation le numéro de l'opération
		 * */
		public function get numOperation():String{
			return _numOperation;
		}
		
		/**
		 * Retourne la date de référence au fromat jj/mm/aaaa
		 * @return la date de référence
		 * */
		public function get dateRef():String{
			return _dateRef;
		}
		
		/**
		 * Retourne le type d'operation à cloturer 1 pour résiliation 2 pour commande
		 * @return _typeOperation le type d'opération
		 * */
		public function get typeOperation():int{
			return _typeOperation;
		}	
		
		/**
		 * Retourne un tableau contenant les identifiants des ressources de l'opération
		 * @return _tabProduit un tableau d'identifiant
		 * */
		public function get tabProduit():Array{
			return _tabProduit;
		}	
		
		
		/**
		 * Retourne l'identifiant du 'groupe client' de l'opération
		 * @return _idGroupeClient de l'operation
		 * */		
		public function get idGroupeClient():int{
			return _idGroupeClient;
		}	
		
	}
}