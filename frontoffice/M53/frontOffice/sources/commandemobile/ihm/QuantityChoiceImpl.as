package commandemobile.ihm
{
	import commandemobile.composants.PanierIHM;
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.popup.PopUpLibelleConfigurationIHM;
	import commandemobile.system.Export;
	import commandemobile.temp.commandeSNCF;
	
	import composants.util.ConsoviewAlert;
	import composants.util.article.Article;
	import composants.util.article.ArticleNouvelleLigneMobile;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class QuantityChoiceImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------
		
		//IHM
		private var _popUplibelle		:PopUpLibelleConfigurationIHM;
		public var composantPanier		:PanierIHM;
		
		//COMPOSANTS
		public var dgListEquipements	:DataGrid;
		public var dgListAboOptions		:DataGrid;
		public var txtptNumberConfig	:TextInput;

		//OBJETS
		public var _cmd					:Commande;
		public var _itemsSelected		:ItemSelected;
		public var _panier				:Panier;
		private var _cmdSNCF			:commandeSNCF 	= new commandeSNCF();
		private var _article			:Article 				= new Article();
		
		//VARIABLES LOCALES
		public var compte				:Number = 0;
		public var convertedToInt		:Boolean = true;
		public var newView				:Boolean = false;
		public var parameterFill		:Boolean = false;
		public var _listEquipements		:ArrayCollection = new ArrayCollection();
		public var _listAboOptions		:ArrayCollection = new ArrayCollection();
		public var _listCommande		:ArrayCollection = new ArrayCollection();

//VARIABLES------------------------------------------------------------------------------
		
//PROPRIETEES PUBLIC---------------------------------------------------------------------

		//LISTE TOUS LES EQUIPEMENTS SELECTIONNES PRECEDEMENT
		public function get listEquipements(): ArrayCollection
	    {
	    	return _listEquipements;
	    }
	    
	    public function set listEquipements(value:ArrayCollection): void
	    {
	    	_listEquipements = value;
	    }
	    
	    //LISTE TOUTES LES OPTIONS ET TOUS LES ABONNEMENTS SELECTIONNES PRECEDEMENT
	    public function get listAboOptions(): ArrayCollection
	    {
	    	return _listAboOptions;
	    }
	    
	    public function set listAboOptions(value:ArrayCollection): void
	    {
	    	_listAboOptions = value;
	    }
	    
	    //LISTE TOUTES LES COMMANDES SELECTIONNES PRECEDEMENT
	    public function get listCommande(): ArrayCollection
	    {
	    	return _listCommande;
	    }
	    
	    public function set listCommande(value:ArrayCollection): void
	    {
	    	_listCommande = value;
	    }

//PROPRIETEES PUBLIC---------------------------------------------------------------------
		
//METHODE PUBLIC-------------------------------------------------------------------------		
		
		//CONTRUTEUR (>LISTENER)
		public function QuantityChoiceImpl()
		{
			addEventListener("nextPageClicked",checkIfAttributToCommandeFill);
			_cmdSNCF.addEventListener("sauvegardeModele",listnerModeleRecorded);
		}

//METHODE PUBLIC-------------------------------------------------------------------------

//METHODE PROTECTED----------------------------------------------------------------------
		
		protected function composantPanierCreationCompleteHandler():void
		{
			composantPanier.panier = _panier;
		}
		
		//AFFECTE TOUS LES EQUIPEMENTS ET RESSOURCES SELECTIONNES PRECEDEMENT AUX PROPRIETEES 
		//CONCERNEES ET COMPTE LE TOTAL DE LA CONFIGURATION ET RAFRAICHI LE DATAGRID
		protected function showHandler():void
		{
			listEquipements = new ArrayCollection();
			listAboOptions 	= new ArrayCollection();
			attributToEquipments();
			attributToAboOptions();
			compteTotal();
			callLater(refreshDatagrid);
			composantPanierCreationCompleteHandler();
		}
		
		//VERIFIE QUE LE TEXTE SAISIE EST BIEN UN ENTIER ET VERIFIE QU'IL EST >= 1 
		protected function txtptNumberConfigHandler():void
		{
			var configNumber:int = 0;
			try
			{
				configNumber = Number(txtptNumberConfig.text);
				if(configNumber < 1)
				{
					txtptNumberConfig.text = "1";
					convertedToInt = false;
				}
				else
				{
					convertedToInt = true;
				}
			}
			catch(erreur:Error)
			{
				Alert.show("Veuillez saisir un nombre!","Consoview");
				convertedToInt = false;
			}
		}
		
		//AFFICHE LA FATURE EN 'PDF'
		protected function btExporterClickHandler(event:MouseEvent):void
	    {
    		if(_cmd.IDCOMMANDE > 0)
	    	{
	    		var export:Export = new Export();
	    		export.exporterCommandeEnPDF(_cmd);
	    	}	
	    }
	    
	    protected function btnRecordModeleClickHandler():void
	    {
	    	_popUplibelle = new PopUpLibelleConfigurationIHM();
			_popUplibelle.addEventListener("messageSaisie",messageSaisieHandler);
			PopUpManager.addPopUp(_popUplibelle,this);
			PopUpManager.centerPopUp(_popUplibelle);
	    }
	    
	    private function messageSaisieHandler(e:Event):void
	    {
	    	_cmd.LIBELLE_CONFIGURATION = _popUplibelle.libelleConfiguration;
	    	sendCommandeHandler();
	    }

	    private function listnerModeleRecorded(e:Event):void
	    {
	    	_popUplibelle.dispatchEvent(new Event("closeThis"));
	    	ConsoviewAlert.afficherOKImage("Modèle enregistré !");
	    }
	    
///////\\\\\\\\\\\\\\\\\\\\\\
		private function sendCommandeHandler():void
		{
			_cmd.ARTICLES = <articles></articles>;
			convertToArticles();
			_cmdSNCF.sauvegardeModele(_cmd,_cmd.ARTICLES);
		}
		
		private function addArticleToArray():ArrayCollection
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			arrayC.addItem(ObjectUtil.copy(_itemsSelected));
			return arrayC;
		}
		
		private function convertToArticles():void
		{
			var COMMANDE_TEMPORAIRE:ArrayCollection = addArticleToArray();
			for(var i:int = 0;i < COMMANDE_TEMPORAIRE.length;i++)
			{
				var tmpArticle : Article = new ArticleNouvelleLigneMobile(XML(_article.article).copy());
				_cmd.ARTICLES.appendChild(convertArticlesInXML(COMMANDE_TEMPORAIRE[i],tmpArticle,tmpArticle.article));
			}
		}
		
		private function convertArticlesInXML(object:Object,article:Article,newArticle:XML):XML
		{
			addRessourcesArticles(article,object);
			addEquipementsArticles(article,object);
			addArticle(newArticle,object.EMPLOYE_CONFIGURATION);
			return newArticle;
		}

		private function addRessourcesArticles(article:Article,object:Object):void
		{
			var allArticles:ArrayCollection = new ArrayCollection();

			//ABONNEMENTS
			for(var j:int = 0;j < object.ABO.length;j++)
			{
				allArticles.addItem(object.ABO[j]);
			}
			//OPTIONS
			for(var k:int = 0;k < object.OPTIONS.length;k++)
			{
				allArticles.addItem(object.OPTIONS[k]);
			}
	
			convertToArticle(article,allArticles,true);
		}
		
		private function addEquipementsArticles(article:Article,object:Object):void
		{
			var allArticles:ArrayCollection = new ArrayCollection();

			//TELEPHONE + CARTE SIM
			for(var h:int = 0;h < object.TERMINAUX.length;h++)
			{
				allArticles.addItem(object.TERMINAUX[h]);
			}
			//ACCESSOIRES
			for(var g:int = 0;g < object.ACCESSOIRES.length;g++)
			{
				allArticles.addItem(object.ACCESSOIRES[g]);
			}
			
			convertToArticle(article,allArticles,false);
		}
		
		private function addArticle(newArticle:XML,object:Object):void
		{
			if(object.FPC != "")
			{
				newArticle.appendChild(<fpc>{object.FPC}</fpc>);
				newArticle.appendChild(<engagement_eq></engagement_eq>);
				newArticle.appendChild(<engagement_res></engagement_res>);
			}
			else
			{
				newArticle.appendChild(<fpc></fpc>);
				newArticle.appendChild(<engagement_eq>{/*object.ENGAGEMENT.VALUE*/0}</engagement_eq>);//A VOIR**********************************************
				newArticle.appendChild(<engagement_res>{/*object.ENGAGEMENT.VALUE*/0}</engagement_res>);//A VOIR**********************************************
			}
			if(object.IDCOLLABORATEUR != 0)
			{
				newArticle.appendChild(<nomemploye>{object.COLLABORATEUR}</nomemploye>);
				newArticle.appendChild(<idemploye>{object.IDCOLLABORATEUR}</idemploye>);
			}

			newArticle.appendChild(<libelle>{object.LIBELLE_CONFIGURATION}</libelle>);
			
			if(object.PORTABILITE)
			{
				newArticle.appendChild(<soustete>{object.NUM_LINE}</soustete>);
				newArticle.appendChild(<code_rio>{object.NUM_RIO}</code_rio>);
				newArticle.appendChild(<date_portage>{formatDate(object.DATE_PORTABILITE)}</date_portage>);				
			}
			else
			{
				newArticle.appendChild(<soustete>{object.NUM_LINE}</soustete>);
			}
		}
		
		private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return year + "/" + temp + "/" + day;
		}
		
		private function convertToArticle(article:Article,arrayC:ArrayCollection,ressources:Boolean):void
		{
			var len:int = arrayC.length;
			if(ressources)
			{
				for(var i:int = 0;i < len;i++)
				{
					article.addRessource(arrayC[i]);
				}
			}
			else
			{
				for(var j:int = 0;j < len;j++)
				{
					article.addEquipement(arrayC[j],_cmd.PRICE_EQUIPEMENT);
				}
			}
		}	    
	    

//METHODE PROTECTED----------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------
		
		//AFFECTE LE NOMBRE DE CONBFIGURATION QUE L'ON A SAISI A 'COMMANDE_TEMPORAIRE'
		private function addToCommande():void
		{
			if(_cmd.COMMANDE_TEMPORAIRE.length == _cmd.CONFIG_NUMBER)
			{
				_cmd.COMMANDE_TEMPORAIRE = new ArrayCollection();
				for(var i:int = 0;i < _cmd.CONFIG_NUMBER;i++)
				{
					_cmd.COMMANDE_TEMPORAIRE.addItem(ObjectUtil.copy(_itemsSelected));
				}
			}
			else
			{	
				_cmd.COMMANDE_TEMPORAIRE = new ArrayCollection();
				for(var j:int = 0;j < _cmd.CONFIG_NUMBER;j++)
				{
					_cmd.COMMANDE_TEMPORAIRE.addItem(ObjectUtil.copy(_itemsSelected));
				}
			}
		}
		
		//ATTRIBUT A LA PROPRIETE 'listEquipements' LES TERMINAUX ET ACCESSOIRES SELECTIONNES PRECEDEMENT
		private function attributToEquipments():void
		{
			var newObject:Object;
			
			for(var i:int = 0;i < _itemsSelected.TERMINAUX.length;i++)
			{
				newObject = new Object();
				if(_itemsSelected.TERMINAUX[i] != null)
				{
					newObject.TYPE_EQUIPEMENT 	= _itemsSelected.TERMINAUX[i].TYPE_EQUIPEMENT;
					newObject.LIBELLE_PRODUIT	= _itemsSelected.TERMINAUX[i].LIBELLE_EQ;
					newObject.PRIX_UNIT 		= recuperationPrixSelectedEquipements(_itemsSelected.TERMINAUX[i],_itemsSelected);
					newObject.IDTYPE_EQUIPEMENT	= _itemsSelected.TERMINAUX[i].IDTYPE_EQUIPEMENT;					listEquipements.addItem(newObject);
					listCommande.addItem(newObject);
				}
			}
			for(var j:int = 0;j < _itemsSelected.ACCESSOIRES.length;j++)
			{
				newObject = new Object();
				newObject.TYPE_EQUIPEMENT 	= _itemsSelected.ACCESSOIRES[j].TYPE_EQUIPEMENT;
				newObject.LIBELLE_PRODUIT 	= _itemsSelected.ACCESSOIRES[j].LIBELLE_PRODUIT;
				newObject.PRIX_UNIT 		= _itemsSelected.ACCESSOIRES[j].PRIX_UNIT;
				newObject.IDTYPE_EQUIPEMENT	= _itemsSelected.ACCESSOIRES[j].IDTYPE_EQUIPEMENT;
				listEquipements.addItem(newObject);
				listCommande.addItem(newObject);
			}
		}
		
		//ATTRIBUT A LA PROPRIETE 'listAboOptions' LES OPTIONS ET ABONNEMENTS SELECTIONNES PRECEDEMENT
		private function attributToAboOptions():void
		{
			var newObject:Object;
			
			for(var i:int = 0;i < _itemsSelected.ABO.length;i++)
			{
				newObject = new Object();
				newObject.TYPE_EQUIPEMENT 	= _itemsSelected.ABO[i].TYPE_THEME;
				newObject.THEME_LIBELLE 	= _itemsSelected.ABO[i].THEME_LIBELLE;
				newObject.LIBELLE_PRODUIT	= _itemsSelected.ABO[i].LIBELLE_PRODUIT;
				newObject.PRIX_UNIT 		= " - ";
				listAboOptions.addItem(newObject);
				listCommande.addItem(newObject);
			}
			for(var j:int = 0;j < _itemsSelected.OPTIONS.length;j++)
			{
				newObject = new Object();
				newObject.TYPE_EQUIPEMENT 	= _itemsSelected.OPTIONS[j].SUR_THEME;
				newObject.THEME_LIBELLE 	= _itemsSelected.OPTIONS[j].THEME_LIBELLE;
				newObject.LIBELLE_PRODUIT 	= _itemsSelected.OPTIONS[j].LIBELLE_PRODUIT;
				newObject.PRIX_UNIT 		= " - ";
				listAboOptions.addItem(newObject);
				listCommande.addItem(newObject);
			}
		}
		
		private function recuperationPrixSelectedEquipements(totalNbr:Object,itmSlctd:ItemSelected):Number
		{
			var prixTotal:Number = 0;
			prixTotal = Number(totalNbr.PRIX_CATALOGUE);
			return prixTotal;
		}
		
		//COMPTE LE TOTAL DES CONFIGURATIONS QUE L'ON VIENT DE FAIRE
		private function compteTotal():void
		{	
			compte = 0;
			for(var i:int = 0;i < listEquipements.length;i++)
			{
				if(listEquipements[i].PRIX_UNIT.toString() != "NaN" && listEquipements[i].PRIX_UNIT != null)
				{
					compte = compte + Number(listEquipements[i].PRIX_UNIT);
				}
			}
		}
		
		//CHERCHE LE TYPE D'OPTIONS ET RETOURNE UN STRING PLUS COURT
		private function checkAbo(valueObject:Object):String
		{
			var value:String = "";

			switch(valueObject.THEME_LIBELLE)
			{
				case "Options Voix": 		value = "Voix"; 	break;
				case "Options Data": 		value = "Data"; 	break;
				case "Options Diverses": 	value = "Diverses"; break;
			}
			return value;
		}
		
		//RAFRAICHI LE DATAGRID
		private function refreshDatagrid():void
		{
			dgListEquipements.dataProvider 	= listEquipements;
			dgListAboOptions.dataProvider 	= listAboOptions;
		}

//EVENT----------------------------------------------------------------------------------
		
		//AFFECTE LE NOMBRE DE CONFIGURATION QUE L'ON VEUT ET AFFECTE A 'COMMANDE' 'COMMANDE_TEMPORAIRE'
		private function checkIfAttributToCommandeFill(e:Event):void
		{
			if(convertedToInt)
			{
				_cmd.CONFIG_NUMBER = Number(txtptNumberConfig.text);
				addToCommande();
				parameterFill = true;
			}
			else
			{
				parameterFill = false;
				Alert.show("Veuillez saisir un nombre!","Consoview");
			}
		}	

//EVENT----------------------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------
		
	}

}