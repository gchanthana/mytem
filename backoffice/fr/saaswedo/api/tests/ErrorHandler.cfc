<cfcomponent author="Cedric" displayname="fr.saaswedo.api.tests.ErrorHandler" extends="fr.saaswedo.api.patterns.error.ErrorHandler">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.tests.ErrorHandler">
		<cfreturn "fr.saaswedo.api.tests.ErrorHandler">
	</cffunction>
	
	<cffunction name="getId" returntype="String" hint="Retourne la valeur de LOCKID()">
		<cfreturn "fr.saaswedo.api.tests.ErrorHandler">
	</cffunction>
	
	<cffunction access="public" name="handleError" returntype="void" hint="Ecrit dans les logs les infos contenues dans cfCatchObject">
		<cfargument name="errorTarget" type="fr.saaswedo.api.patterns.error.IErrorTarget" required="true">
		<cfargument name="cfCatchObject" type="Any" required="true">
		<cfargument name="errorInfos" type="Struct" required="false" hint="Ce paramètre est ignoré par cette implémentation">
		<!---
		<cfset var mailTo="cedric.rapiera@saaswedo.com">
		<cfmail type="html" from="monitoring@saaswedo.com" to="#mailTo#" subject="[Datalert] Test ErrorHandler">
			<cfdump var="#ARGUMENTS#" label="ARGUMENTS - handleError()">
		</cfmail>
		--->
	</cffunction>
</cfcomponent>