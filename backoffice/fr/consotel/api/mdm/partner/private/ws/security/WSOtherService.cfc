<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.security.WSOtherService"
implements="fr.consotel.api.mdm.partner.private.ws.IWSOtherService" hint="Implémentation fournissant les méthodes : Other Services
Dans la version actuelle de cette classe : Les méthodes retournent des implémentations CFMX correspondant aux types utilisés par le service FSecure
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<!--- fr.consotel.api.mdm.partner.private.ws.security.WSOtherService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSOtherService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.security.FSecure" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.security.FSecure" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<!--- Other Services --->
	<cffunction access="public" name="sendActivationSMS" returntype="String" hint="Envoi un SMS d'activation à un équipement et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<!--- Validation des credentials d'authentification --->
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<!--- Implémentation du service FSecure correspondant --->
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.sendActivationSMS(ARGUMENTS["singleLicenseKey"])))>
	</cffunction>
	
	<cffunction access="public" name="sendDownloadLink" returntype="String" hint="Envoi un lien de téléchargement à un équipement et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn yesNoFormat(toString(serviceStub.sendDownloadLink(ARGUMENTS["singleLicenseKey"])))>
	</cffunction>
	
	<cffunction access="public" name="getLicenseXML" returntype="String" hint="Retourne le contenu XML de la licence à utiliser pour activer le service de sécurité FSecure">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Multi License Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getLicenseXML(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="getSMSLicenseKey" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour activer le service de sécurité FSecure">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.getSMSLicenseKey(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="public" name="pregenerateKeycodes" returntype="Array" hint="Génère NumberOfKeysToGenerate Single Key Codes dans le Multi License par défaut et retourne la liste">
		<cfargument name="numberOfKeysToGenerate" type="String" required="true" hint="Nombre de Single Key Code à générer">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfset var resultArray=serviceStub.pregenerateKeycodes(ARGUMENTS["numberOfKeysToGenerate"])>
		<cfif isDefined("resultArray")>
			<cfreturn resultArray>
		<cfelse>
			<cfreturn []>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="echo" returntype="String" hint="Retourne un ECHO du message">
		<cfargument name="echoMessage" type="String" required="true" hint="Message qui sera retourné par cette méthode">
		<cfset var fsecure=getWSRepository()>
		<cfset fsecure.validateAuth()>
		<cfset var serviceStub=fsecure.getCorporationService()>
		<cfreturn toString(serviceStub.echo(ARGUMENTS["echoMessage"]))>
	</cffunction>
</cfcomponent>