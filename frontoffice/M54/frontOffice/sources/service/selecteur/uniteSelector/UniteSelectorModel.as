package service.selecteur.uniteSelector
{
	import event.selector.UniteEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.selector.ValeurSelectorVo;

	internal class UniteSelectorModel  extends EventDispatcher
	{
		private var _unites :ArrayCollection;
		
		public function UniteSelectorModel():void
		{
		}
		public function get unites():ArrayCollection
		{
			return _unites;
		}
		
		internal function updateValues(value:ArrayCollection):void
		{	
			_unites= new ArrayCollection();
			
			var valeurSelector:ValeurSelectorVo;
			
			for(var i:int=0;i<value.length;i++)
			{
				valeurSelector = new ValeurSelectorVo();
				valeurSelector.fill(value[i],i);
				_unites.addItem(valeurSelector); 
			}
			this.dispatchEvent(new UniteEvent(UniteEvent.CHANGER_VALUE_UNITE_EVENT));/** capturé dans UniteSelectorImpl */
		}
	}
}