<!---
Package : fr.consotel.consoview.facturation.optimisation.detailfilaireSynthese
--->
<cfcomponent name="RapportDetailFilaireSyntheseGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP1_DETAIL_FILAIRE_SYNT_LIG">
	        <cfprocparam  cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam value="#rapportParams.PERIODICITE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>

	<cffunction name="GetTableau" access="private" returntype="query" output="true">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetperiod" dbtype="query">
			SELECT 	type_theme, sur_theme, theme_libelle, idtheme_produit,
					SUM(qte) AS qte, SUM(montant_final) AS montant_final, SUM(nombre_appel) AS nombre_appel,
    				SUM(duree_appel) AS duree_appel, ordre_affichage, 0 as montant_precedent, 0 as mois_report
			from p_detail_lignes
			where mois_report >= #ParseDateTime(RapportParams.date_m)# 
				and mois_report <= #ParseDateTime(RapportParams.date_m2)# 
			group by type_theme, sur_theme, theme_libelle, idtheme_produit, ordre_affichage
		</cfquery>
		<cfreturn qgetperiod>
	</cffunction>

	<cffunction name="GetTableauPrec" access="private" returntype="query" output="true">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetperiodprec" dbtype="query">
			SELECT 	idtheme_produit, SUM(montant_final) AS montant_precedent
			from p_detail_lignes
			where mois_report >= #parseDateTime(RapportParams.date_m_moins_deux)# 
			and mois_report <= #parseDateTime(RapportParams.date_m_moins_un)# 
			group by idtheme_produit
		</cfquery>
		<cfreturn qgetperiodprec>
	</cffunction>

	<cffunction name="GetListIdtheme" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetid" dbtype="query">
			SELECT 	idtheme_produit
			from p_detail_lignes
			group by idtheme_produit
		</cfquery>
		<cfreturn qgetid>
	</cffunction>

	<cffunction name="GetHisto" access="private" returntype="query" output="true">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetperiod" dbtype="query" maxrows="8">
			SELECT 	type_theme, SUM(montant_final) AS montant_final, mois_report
			from p_detail_lignes
			group by type_theme, mois_report
			order by mois_report desc
		</cfquery>
		<cfquery name="qgetbis" dbtype="query">
			SELECT 	*
			from qgetperiod
			order by mois_report asc
		</cfquery>
		<cfreturn qgetbis>
	</cffunction>

	<cffunction name="getInternational" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP2_DETAIL_FILAIRE_SYNT_LIG">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetInternationalData">
        </cfstoredproc>
		<cfreturn qGetInternationalData>
	</cffunction>
	
	<cffunction name="getListeAppels" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP3_DETAIL_FILAIRE_SYNT_LIG">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetAppelListData">
        </cfstoredproc>
		<cfreturn qGetAppelListData>
	</cffunction>
	
	<cffunction name="GetdataTriee" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetperiodprec" dbtype="query">
				SELECT 	*
				from p_detail_lignes
				order by type_theme, sur_theme,ordre_affichage
		</cfquery>
		<cfreturn qgetperiodprec>
	</cffunction>
	
	<cffunction name="GetListConsos" access="private" returntype="query" output="true">
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetid" dbtype="query" maxrows="10">
			SELECT 	sous_tete, patronyme,sum(montant_final) as montant_final
			from p_detail_lignes
			where type_theme='Consommations'
			group by sous_tete, patronyme
			order by montant_final desc
		</cfquery>
		<cfreturn qgetid>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<!---====== FORMATAGE DES DATES ======--->
		<cfset RapportParams.date_m = LSDateFormat(ParseDateTime(RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset RapportParams.date_m2=
				LSDateFormat(DateAdd("m",RapportParams.periodicite,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_un=
				LSDateFormat(DateAdd("m",-1,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_deux=
				LSDateFormat(DateAdd("m",(-1 - (RapportParams.periodicite)),ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_debut_annee = year(ParseDateTime(RapportParams.DATEDEB)) & "/01/01">
		<cfset rapportParams.chaine_date = rapportParams.DATE_M>
		<!---====================================================--->
		<cfset dataset2=getReportData(ID_PERIMETRE,RapportParams)>
		<cfset dataset=GetTableau(RapportParams,dataset2)>
		<cfset dTotal=GetListConsos(getListeAppels(ID_PERIMETRE,RapportParams))>
		<cfset datasetInter=getInternational(ID_PERIMETRE,RapportParams)>
		<!--- Création liste de clés --->
		<cfset datefin=LsDateFormat(parseDateTime(RapportParams.date_m2),"yyyy/mm")>
		<cfset datedeb=LsDateFormat(parseDateTime(RapportParams.date_m_moins_deux),"yyyy/mm")>
		<!---
		<cfset datefin=LsDateFormat(createDate(right(RapportParams.date_m2,4),mid(RapportParams.date_m2,4,2),"01"),"yyyy/mm")>
		<cfset datedeb=LsDateFormat(createDate(right(RapportParams.date_m_moins_deux,4),mid(RapportParams.date_m_moins_deux,4,2),"01"),"yyyy/mm")>
		--->
		<cfset compteur=Evaluate(datediff("m",datedeb,datefin)+1)>
		<cfset aDate=arrayNew(1)>
		<cfloop from="1" to="#compteur#" index="i">
			<cfset aDate[i]=LsDateFormat(dateadd("m",i-1,datedeb),"yyyy/mm")>
		</cfloop>
		<!--- Creation des structures --->
		<cfset stFinal=structNew()>
		<cfset stTheme=structNew()>
		<!--- Remplie les structures complètes avec des 0 --->
		<cfset qTemp=GetListIdtheme(dataset)>
		<cfset ListTheme=ValueList(qTemp.idtheme_produit)>
		<cfloop list="#ListTheme#" index="id">
			<cfif Not structKeyExists(stFinal,id)>
				<cfset stTemp=structNew()>
				<cfloop list="#dataset.columnlist#" index="obj">
					<cfset stTemp['#obj#']=0>
				</cfloop>
				<cfset stTemp['montant_precedent']=0>
				<cfset structInsert(stFinal,id,stTemp)>
			</cfif>
		</cfloop>
		<!--- Remplie avec les bonnes valeurs pour la periode --->
		<cfloop query="dataset">
			<cfset stFinal[idtheme_produit].montant_final=montant_final>
			<cfset stFinal[idtheme_produit].qte=qte>
			<cfset stFinal[idtheme_produit].sur_theme=sur_theme>
			<cfset stFinal[idtheme_produit].THEME_LIBELLE=THEME_LIBELLE>
			<cfset stFinal[idtheme_produit].TYPE_THEME=TYPE_THEME>
			<cfset stFinal[idtheme_produit].ORDRE_AFFICHAGE=ORDRE_AFFICHAGE>
			<cfset stFinal[idtheme_produit].NOMBRE_APPEL=NOMBRE_APPEL>
			<cfset stFinal[idtheme_produit].DUREE_APPEL=DUREE_APPEL>
			<cfset stFinal[idtheme_produit].IDTHEME_PRODUIT=IDTHEME_PRODUIT>
		</cfloop>
		<!--- Remplie avec valeurs precedentes --->
		<cfset dataset3=GetTableauPrec(RapportParams,dataset2)>
		<cfloop query="dataset3">
			<cfif structKeyExists(stFinal,idtheme_produit)>
				<cfset stFinal[idtheme_produit].montant_precedent=montant_precedent>
			</cfif>
		</cfloop>
		<!--- Création de la requête --->
		<cfset qTemp=GetListIdtheme(dataset2)>
		<cfset ListTheme=ValueList(qTemp.idtheme_produit)>
		<cfset ListeColonne=dataset2.columnlist>
		<cfset ListeColonne=ListAppend(ListeColonne,"montant_precedent")>
		<cfset qFinale=queryNew(ListeColonne)>
		<cfif Not StructIsEmpty(stFinal)>
		<cfloop list="#ListTheme#" index="id">
			<cfif structKeyExists(stFinal,id)>
				<cfset queryAddRow(qFinale)>
			</cfif>
				<cfloop list="#ListeColonne#" index="obj">
					<cfif structKeyExists(stFinal,id)>
						<cfset querySetCell(qFinale,"#obj#",stFinal['#id#']['#obj#'])>
					</cfif>
				</cfloop>
		</cfloop>
		</cfif>
		<cfset qFinale=GetdataTriee(qFinale)>

		<cfif qFinale.recordcount neq 0>
			<cfif rapportparams.format eq "excel">
				<!--- Appel création du XLS --->
				<cfinclude template="./detailgsmGroupe_xls.cfm">
			<cfelse>
				<cfset histo=getHisto(rapportParams,dataset2)>
				<cfinclude template="./detailFilaireSyntheseGroupe.cfm">
			</cfif>
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
