package accueil.ihm.kpi
{
	import flash.events.Event;
	
	import mx.containers.Canvas;
	import mx.controls.DateField;
	import mx.controls.Label;
	
	import accueil.event.AccueilEvent;
	import accueil.service.data.DataModel;
	import accueil.utils.preloader.Spinner;
	
	public class KpiArpuImpl extends Canvas
	{
		public var lblValidite:Label;
		public var mois1:Label;
		public var mois2:Label;
		
		private var _progressBar:Spinner;		
		private var _dataModel:DataModel;
		private var _message:String;
		
		private var _dateMessage:String = "";
		private var _dataModelChange:Boolean = false;
		
		
		public function KpiArpuImpl()
		{
			super();
		}
		
		public function get dateMessage():String
		{
			return _dateMessage;
		}
		
		[Bindable]
		public function set dateMessage(value:String):void
		{
			_dateMessage = value;
		}

		public function launchLoader():void {			
			_progressBar = new Spinner();
			_progressBar.x = this.width/2;
			_progressBar.y = this.height/2;
			this.addChild(_progressBar);			
			_progressBar.play();
		}
		
		public function stopLoader():void
		{
			if (_progressBar)
			{
				_progressBar.stop();
				this.removeChild(_progressBar);
				_progressBar = null;
			}	
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			if(_dataModelChange == true)
			{
				//dataModel				
				_dataModel.addEventListener(AccueilEvent.LASTE_DATE_COMPLETE_UPDATED,lasteDateCompleteUpdatedHandler);
				_dataModel.addEventListener(AccueilEvent.INDICATEUR_UPDATED,indicateurUpdatedHandler);	
				 
				//fin dataModel
				_dataModelChange = false;
			}
			
		}
		
		[Bindable]
		public function set dataModel(value:DataModel):void
		{	
			if(value != null && value != _dataModel)
			{
				_dataModel = value;
				_dataModelChange = true;
				invalidateProperties();
			}
		}
		
		protected function indicateurUpdatedHandler(event:Event):void
		{
			stopLoader();
		}
		
		protected function lasteDateCompleteUpdatedHandler(event:Event):void
		{
			if(_dataModel.pfgpDateLastLoadComplete.IDPERIODE > 0)
			{
				var dt:Date = new Date(	_dataModel.pfgpDateLastLoadComplete.DATE.fullYear,
										_dataModel.pfgpDateLastLoadComplete.DATE.month-1);
				
				mois1.text = DateField.dateToString(dt,'MM/YYYY')
				mois2.text = DateField.dateToString(_dataModel.pfgpDateLastLoadComplete.DATE,'MM/YYYY');
				
				dateMessage = "validité des données de parc : " + DateField.dateToString(dataModel.pfgpDateLastLoadComplete.DATE,'MM/YYYY');
			}
			else
			{
				_message = "Erreur de chargement";
				stopLoader();
			}
		}
		
		public function get dataModel():DataModel
		{
			return _dataModel;
		}
		
	}
}