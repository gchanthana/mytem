package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.resources.ResourceManager;
	
	import univers.parametres.parametrecompte.event.ChampPersoEvent;
	import univers.parametres.parametrelogin.service.ChampsPersosService;
	import univers.parametres.parametrelogin.vo.ChampsPersosVO;	public class FicheEtapesCommandesImpl extends VBox	{		[Bindable] public var cpservice	:ChampsPersosService		
		public var champs1				:FicheECMobileIHM;		public var champs2				:FicheECFixeReseauIHM;
		public var cloture				:FicheClotureWorkflowIHM;		
		private var listeLibelle		:String = "";		private var listeIdAction		:String = "";				public function FicheEtapesCommandesImpl()		{			super();		}		public function init():void		{			champs1.adgChampsPersos.addEventListener(KeyboardEvent.KEY_DOWN,MobileListenKeyDownHandler)			champs2.adgChampsPersos.addEventListener(KeyboardEvent.KEY_DOWN,FixeListenKeyDownHandler)		}		public function initData():void		{			if(cpservice == null)				cpservice = new ChampsPersosService()			cpservice.getListeChampsPersosFixe()			cpservice.getListeChampsPersosMobile()		}		// VALIDATION DES CHANGEMENTS		protected function btnValiderClickHandler():void		{			createListeAction();
			
			cloture.setInfosCloture();		
			cpservice.addEventListener(ChampPersoEvent.SAVE_PROFIL_EVENT,saveOk);			cpservice.saveChampsPersos(listeIdAction,listeLibelle);		}		private function saveOk(e:ChampPersoEvent):void		{			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Les_changements_ont__t__sauvegard__avec_'), this);		}	// CREER LES DEUX LISTES D'ACTION ET DE LIBELLE 		private function createListeAction():void		{			listeLibelle = "";			listeIdAction = "";			for each(var cp:ChampsPersosVO in cpservice.listeChampsPersosMobile)			{				listeIdAction += cp.CP_ID+","				if(cp.CP_Libelle_Client == "")					listeLibelle += ","				else				listeLibelle += cp.CP_Libelle_Client+","			}
						for each(var cp1:ChampsPersosVO in cpservice.listeChampsPersosFixe)			{				listeIdAction += cp1.CP_ID+","				if(cp1.CP_Libelle_Client == "")					listeLibelle += ","				else					listeLibelle += cp1.CP_Libelle_Client+","			}
						listeIdAction = listeIdAction.substr(0,listeIdAction.length -1);			listeLibelle = listeLibelle.substr(0,listeLibelle.length -1);		}
			// GESTION DE LA TABULATION		public function MobileListenKeyDownHandler(e:KeyboardEvent):void		{			if(e.keyCode == 9)			{				if(champs1.adgChampsPersos.selectedIndex+1 < champs1.DATAPROVIDER.length)				{ 					(champs1.adgChampsPersos.selectedItem as ChampsPersosVO).boolFocus = false;					champs1.adgChampsPersos.selectedIndex = (e.currentTarget as DataGrid).selectedIndex + 1 ;					(champs1.adgChampsPersos.selectedItem as ChampsPersosVO).boolFocus = true;					champs1.DATAPROVIDER.refresh();				}				else				{					(champs1.adgChampsPersos.selectedItem as ChampsPersosVO).boolFocus = false;					champs1.adgChampsPersos.selectedIndex = 0;					(champs1.DATAPROVIDER.getItemAt(0) as ChampsPersosVO).boolFocus = true//					(champs1.adgChampsPersos.selectedItem as ChampsPersosVO).boolFocus = true;					champs1.DATAPROVIDER.refresh();				}			}		}		public function FixeListenKeyDownHandler(e:KeyboardEvent):void		{			if(e.keyCode == 9)			{				if(champs1.adgChampsPersos.selectedIndex+1 < champs1.DATAPROVIDER.length)				{ 					(champs2.adgChampsPersos.selectedItem as ChampsPersosVO).boolFocus = false;					champs2.adgChampsPersos.selectedIndex = (e.currentTarget as DataGrid).selectedIndex + 1 ;					(champs2.adgChampsPersos.selectedItem as ChampsPersosVO).boolFocus = true;					champs2.DATAPROVIDER.refresh();				}				else				{					(champs2.adgChampsPersos.selectedItem as ChampsPersosVO).boolFocus = false;					champs2.adgChampsPersos.selectedIndex = 0;					(champs2.DATAPROVIDER.getItemAt(0) as ChampsPersosVO).boolFocus = true					champs2.DATAPROVIDER.refresh();				}			}		}	}}