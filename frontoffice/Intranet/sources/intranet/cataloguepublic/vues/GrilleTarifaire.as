package intranet.cataloguepublic.vues
{
	import composants.controls.TextInputLabeled;
	import composants.periode.PeriodeEvent;
	import composants.periode.PeriodeSelector;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.TabNavigator;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import intranet.cataloguepublic.controles.AbstractGestionTarifs;
	import intranet.cataloguepublic.controles.GestionTarifsGroupe;
	import intranet.cataloguepublic.controles.GestionTarifsSociete;
	import intranet.cataloguepublic.vo.GroupeProduitControle;
	import intranet.cataloguepublic.vo.Societe;
	import intranet.cataloguepublic.vo.VersionDeTarif;
	
	

	public class GrilleTarifaire extends VBox
	{
		public var dgVersionsTarifs : DataGrid;
		
		[Bindable]
		public var dgTarifsAbos : DataGrid;
		
		[Bindable]
		public var dgTarifsConsos : DataGrid;
		
		
		public var tnGrids : TabNavigator;
		public var tbConsos : Box;
		public var tbAbos : Box;
		
		public var btTaguer : Button;
		public var btAjouterVersion : Button;
		public var btSupprimerVersion : Button;
		public var btModifierVersion : Button;
		public var btRecherche : Button;
		public var cbOperateur : ComboBox;
		public var cbSociete : ComboBox;
		public var txtRecherche : TextInputLabeled;
		public var txtFiltre : TextInputLabeled;
		public var psPeriode : PeriodeSelector;		
		public var lblPeriode : Label;
		public var lblPoidsTotal : Label;
		public var rbgPerimetre : RadioButtonGroup;
		
		public var ckbFixe : CheckBox;
		public var ckbMobile : CheckBox;
		public var ckbData : CheckBox;		
		
		//Fenetre d'ajout de version 
		private var twAjouterVersion : AjoutVersionTarifIHM;
		
		protected var dateDebut : Date;
		protected var dateFin : Date;
		
		protected static const SEGMENT_FIXE : String = "fixe";
		protected static const SEGMENT_MOBILE : String = "mobile";
		protected static const SEGMENT_DATA : String = "data";
		
		//gestion de tarifs		
		[Bindable]
		protected var gestionTarifs : AbstractGestionTarifs;				
		
		//Constructeur
		public function GrilleTarifaire()
		{	
			super(); 
		}
	
		public function onPerimetreChange():void{
			//rbgPerimetre.selectedValue = "groupe";
			gestionTarifs = new GestionTarifsGroupe();
			
			gestionTarifs.listeOperateurs.refresh();
			gestionTarifs.listeProduitsAbos.refresh();
			gestionTarifs.listeProduitsConsos.refresh();
			gestionTarifs.listeSocietes.refresh();
			gestionTarifs.listeVesrions.refresh();
			
			//gestionTarifs.setListeOperateurs(cbOperateur.dataProvider as ArrayCollection);
			gestionTarifs.getListeOperateurs();
			initPeriode();	
			//psPeriode.onPerimetreChange();
			txtFiltre.text = "";
			txtRecherche.text = "";
		}
		
		//LABEL FUNCTION POUR DATAGRIDS		
		protected function formateDates(item : Object, column : DataGridColumn):String{			
			if (item[column.dataField] != null){
				var ladate:Date = new Date(item[column.dataField]);
				return DateFunction.formatDateAsString(ladate);				
			}else return "-";
			
		}
		
		protected function formateEuros(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);	
		}
		
		protected function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		protected function formateNumber(item : Object, column : DataGridColumn):String{
			return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
		}
		
		protected function formateEurosP4(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],4);	
		}
		
				
		protected function ckbFixeChangeHandler(ev : Event):void{
			txtFiltreChangeHandler(ev);
		}
		
		protected function ckbMobileChangeHandler(ev : Event):void{
			txtFiltreChangeHandler(ev);
		}
		
		protected function ckbDataChangeHandler(ev : Event):void{
			txtFiltreChangeHandler(ev);
		}
		
		protected function txtFiltreChangeHandler(ev : Event):void{
			
			if(tnGrids.selectedChild == tbAbos){
				
				if(dgTarifsAbos != null && dgTarifsAbos.initialized){
					if (dgTarifsAbos.dataProvider != null){
						(dgTarifsAbos.dataProvider as ArrayCollection).filterFunction = filtrerGridTarifs;
						(dgTarifsAbos.dataProvider as ArrayCollection).refresh();
					}	
				}	
			}
			else
			{
				if(dgTarifsConsos != null &&  dgTarifsConsos.initialized){
					if (dgTarifsConsos.dataProvider != null){
						(dgTarifsConsos.dataProvider as ArrayCollection).filterFunction = filtrerGridTarifs;
						(dgTarifsConsos.dataProvider as ArrayCollection).refresh();
					}	
				}	
			}
		}
		
		//filtre avec le segment fixe
		private function filtrerSegmentFixe(produit : GroupeProduitControle):Boolean{
			if (ckbFixe.selected && ckbFixe.enabled && (produit.segment.toLowerCase() == SEGMENT_FIXE)) return true
			else return false
		}
		
		private function filtrerSegmentMobile(produit : GroupeProduitControle):Boolean{
			if (ckbMobile.selected && ckbMobile.enabled && (produit.segment.toLowerCase() == SEGMENT_MOBILE)) return true
			else return false
		}
		
		private function filtrerSegmentData(produit : GroupeProduitControle):Boolean{
			if (ckbData.selected && ckbData.enabled && (produit.segment.toLowerCase() == SEGMENT_DATA)) return true
			else return false
		}
		
		private function filtrerLesChamps(produit : GroupeProduitControle):Boolean{
			if (	(produit.libelle.toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)				
					||
					(produit.prixUnitaire.toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)				
					||
					(produit.prixRemise.toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)				
					||
					(produit.remiseContrat.toString().toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				)return true;
			else
				return false;	
		}
				
		private function filtrerGridTarifs(produit : GroupeProduitControle):Boolean{			
			if (filtrerSegmentFixe(produit)){
				if (filtrerLesChamps(produit))return true;
				else return false;		
			}else if (filtrerSegmentMobile(produit)){
				if (filtrerLesChamps(produit))return true;
				else return false;		
			}else if (filtrerSegmentData(produit)){
				if (filtrerLesChamps(produit))return true;
				else return false;		
			}else return false
		}
		
		
		//
		protected function selectPerimetreHandler(ice : ItemClickEvent):void{
			reset();
			if(ice.currentTarget.selectedValue == "groupe"){			
				gestionTarifs = new GestionTarifsGroupe();
				//gestionTarifs.setListeOperateurs(cbOperateur.dataProvider as ArrayCollection);								
				gestionTarifs.getListeOperateurs();
			}else{
				gestionTarifs = new GestionTarifsSociete();
				 
				//gestionTarifs.setlisteSocietes(cbSociete.dataProvider as ArrayCollection);
				//gestionTarifs.setListeOperateurs(cbOperateur.dataProvider as ArrayCollection);
				gestionTarifs.getListeSocietes();
			}
		}
		
		///HANDLERS
		protected function init(fe : FlexEvent):void{
			gestionTarifs = new GestionTarifsGroupe();
			//gestionTarifs.setListeOperateurs(cbOperateur.dataProvider as ArrayCollection);
			gestionTarifs.getListeOperateurs();
			gestionTarifs.selectedProduit = new GroupeProduitControle();
			initPeriode();			
		}
		
		//handler de la selecetion d'une societe
		protected function cbSocieteChangeHandler(le : ListEvent):void{
			if ((cbSociete.selectedItem != null)&&(gestionTarifs != null)){
				gestionTarifs.selectedSociete = cbSociete.selectedItem as Societe;
				gestionTarifs.getListeOperateurs();
			}
		}
		
		//Mise à jour de la période
		protected function periodeEventHandler(pe : PeriodeEvent):void{			
			//--- NOT USED ---//
			/* dateDebut = pe.dateDeb;
			dateFin = pe.dateFin;
			setSelectedPeriode(pe.moisDeb,pe.moisFin); */
		}
		
		//Handler du bouton rechercher
		protected function btRechercheClickHandler(me : MouseEvent):void{
			//gestionTarifs.setListeProduits(dgTarifs.dataProvider as ArrayCollection);
			if (cbOperateur.selectedItem != null){
				gestionTarifs.setParamsRecherches(cbOperateur.selectedItem.id,txtRecherche.text,dateDebut,dateFin);	
				txtFiltre.text = "";
				ckbData.selected = ckbFixe.selected = ckbMobile.selected = true;
			}else{
				Alert.show("Pas d'opérateur sélèctionné");
			}
		}
		
		protected function btEnterHandler(fe : FlexEvent):void{
			//gestionTarifs.setListeProduits(dgTarifs.dataProvider as ArrayCollection);
			if (cbOperateur.selectedItem != null){ 
				gestionTarifs.setParamsRecherches(cbOperateur.selectedItem.id,txtRecherche.text,dateDebut,dateFin);	
				txtFiltre.text = "";
				ckbData.selected = ckbFixe.selected = ckbMobile.selected = true;
			}else{
				Alert.show("Pas d'opérateur sélèctionné");
			}
		}
		
		protected function tnGridsChangeHandler(ev : Event):void{			
			if ((tnGrids.selectedChild == tbAbos)&&(dgTarifsAbos.selectedIndex != -1)){
				var produitAbo : GroupeProduitControle = dgTarifsAbos.selectedItem as GroupeProduitControle;
				gestionTarifs.selectedProduit = produitAbo;				
				btTaguerUpdateLabel(produitAbo);
			}else if((tnGrids.selectedChild == tbConsos)&&(dgTarifsConsos.selectedIndex != -1)){
				var produitConso : GroupeProduitControle = dgTarifsConsos.selectedItem as GroupeProduitControle;
				gestionTarifs.selectedProduit = produitConso;				
				btTaguerUpdateLabel(produitConso);
			}else{
				var produit : GroupeProduitControle = new GroupeProduitControle();
				gestionTarifs.selectedProduit = produit;				
				btTaguerUpdateLabel(produit);
			}
		}
		
		
		protected function tbAbosShowHandler(fe : FlexEvent):void{
			/* if (dgTarifsConsos != null && dgTarifsConsos.initialized)
				if ( dgTarifsConsos.selectedIndex != -1) dgTarifsConsos.selectedIndex = -1;
			if(dgTarifsAbos.selectedIndex != -1){
				gestionTarifs.selectedProduit = dgTarifsAbos.selectedItem as GroupeProduitControle;
				btTaguerUpdateLabel(gestionTarifs.selectedProduit);
			}
			txtFiltreChangeHandler(null); */
			//txtFiltre.text = "";
			//txtFiltre.dispatchEvent(new Event(Event.CHANGE));
		}
		//
		protected function gdTarifsAbosChangeHandler(le : ListEvent):void{
			
			if (dgTarifsAbos.selectedIndex != -1){
				var produit : GroupeProduitControle = dgTarifsAbos.selectedItem as GroupeProduitControle;
				gestionTarifs.selectedProduit = produit;				
				btTaguerUpdateLabel(produit);
			}else{
				gestionTarifs.selectedProduit = new GroupeProduitControle();
				btTaguerUpdateLabel(null);
			}
		}
		
		protected function gdTarifsAbosUpdateCompleteHandler(ev : Event):void{
			if (gestionTarifs != null)
				if (dgTarifsAbos.selectedIndex != -1){
					var produit : GroupeProduitControle = dgTarifsAbos.selectedItem as GroupeProduitControle;
					gestionTarifs.selectedProduit = produit;
					btTaguerUpdateLabel(produit);
				}else{
					gestionTarifs.selectedProduit = new GroupeProduitControle();
					btTaguerUpdateLabel(null);
				}
		}
		
		
		protected function gdTarifsConsosChangeHandler(le : ListEvent):void{
			if (dgTarifsConsos.selectedIndex != -1){
				var produit : GroupeProduitControle = dgTarifsConsos.selectedItem as GroupeProduitControle;
				gestionTarifs.selectedProduit = produit;				
				btTaguerUpdateLabel(gestionTarifs.selectedProduit);
			}else{
				gestionTarifs.selectedProduit = null;
				btTaguerUpdateLabel(null);
			}
		}
		
		protected function tbConsosShowHandler(fe : FlexEvent):void{
			/* if (dgTarifsAbos != null && dgTarifsAbos.initialized)
				if (dgTarifsAbos.selectedIndex != -1) dgTarifsAbos.selectedIndex = -1;	
				
			if(dgTarifsConsos.selectedIndex != -1){
				gestionTarifs.selectedProduit = dgTarifsConsos.selectedItem as GroupeProduitControle;				
				btTaguerUpdateLabel(gestionTarifs.selectedProduit);
			}
			txtFiltreChangeHandler(null); */
			//txtFiltre.text = "";
			//.dispatchEvent(new Event(Event.CHANGE));
		}
		
		protected function gdTarifsConsosUpdateCompleteHandler(ev : Event):void{
			if (gestionTarifs != null)
				if (dgTarifsConsos.selectedIndex != -1){
					var produit : GroupeProduitControle = dgTarifsConsos.selectedItem as GroupeProduitControle;
					gestionTarifs.selectedProduit = produit;
					btTaguerUpdateLabel(produit);
				}else{
					gestionTarifs.selectedProduit = null;
					btTaguerUpdateLabel(null);
			}
			
			trace("gdTarifsConsosUpdateCompleteHandler");
		}
		
			
		//
		protected function btTaguerClickHandler(me : MouseEvent):void{
			if (tnGrids.selectedChild == tbAbos){
				if(dgTarifsAbos.selectedIndex != -1){				
					ConsoviewAlert.afficherAlertConfirmation(btTaguer.label,"Confirmer",executerBtTaguerClick);
				}	
			}else{
				if(dgTarifsConsos.selectedIndex != -1){				
					ConsoviewAlert.afficherAlertConfirmation(btTaguer.label,"Confirmer",executerBtTaguerClick);
				}
			}
				
		}
		
		
		//Execute l'action du click sur le bouton taguer apres confirmation
		private function executerBtTaguerClick(eventObj:CloseEvent):void{
			if (tnGrids.selectedChild == tbAbos){
				if (eventObj.detail == Alert.OK){	
					var produitAbo : GroupeProduitControle = dgTarifsAbos.selectedItem as GroupeProduitControle;
					if (produitAbo.pourControle){
						callLater(gestionTarifs.detaguerProduit);
						
					}else{
						callLater(gestionTarifs.taguerProduit);			
					}
				}	
			}else{
				if (eventObj.detail == Alert.OK){	
					var produitConso : GroupeProduitControle = dgTarifsConsos.selectedItem as GroupeProduitControle;
					if (produitConso.pourControle){
						callLater(gestionTarifs.detaguerProduit);
					}else{
						callLater(gestionTarifs.taguerProduit);
					}
				}
			}
		}
		
		//Met à jour le libellé du bouton taguer le produit
		//suivant l'état du produit sélectionné
		protected function btTaguerUpdateLabel(produit : GroupeProduitControle):void{
			if (produit != null){
				if (produit.dateDebut != null){
					if (produit.pourControle){
						btTaguer.label = "Ne pas contrôler";
						btTaguer.enabled = true;
					}else if (!produit.pourControle){
						btTaguer.label = "Contrôler";
						btTaguer.enabled = true;
					}	
				}else{
					btTaguer.label = "----";
					btTaguer.enabled = false;
				}
				
			}else{
				btTaguer.label = "----";
				btTaguer.enabled = false;
			}
		}
		
		protected function dgVersionsTarifsChangeHandler(event : ListEvent):void{
			activerBoutonEditSuppVersion();
			
		}
		
		protected function dgVersionsTarifsUpdateCompleteHandler(ev : Event):void{
			if (dgVersionsTarifs) {
				activerBoutonEditSuppVersion();
				activerBoutonAjouterVersion();				
			}
		}
		
		//mettre à jour la version de tarif selectionne
		protected function btModifierVersionClickHandler(me : MouseEvent):void{
			if (dgVersionsTarifs.selectedIndex != -1){
				twAjouterVersion = new AjoutVersionTarifIHM();
				twAjouterVersion.title = "Modifier une version de tarif";
				gestionTarifs.selectedVersion = dgVersionsTarifs.selectedItem as VersionDeTarif;
				twAjouterVersion.gestionTarif = gestionTarifs;
				PopUpManager.addPopUp(twAjouterVersion,DisplayObject(parentApplication),true);
				PopUpManager.centerPopUp(twAjouterVersion);				
			}
		}
		
		protected function btAjouterVersionClickHandler(me : MouseEvent):void{
		
			dgVersionsTarifs.selectedIndex = -1;
			twAjouterVersion = new AjoutVersionTarifIHM();
			twAjouterVersion.title = "Ajouter une version de tarif";
			gestionTarifs.creerVesrionTarif();
			twAjouterVersion.gestionTarif = gestionTarifs;
			twAjouterVersion.addEventListener(AjoutVersionTarif.AJOUTER,twAjouterVersionAjouterHandler);
			PopUpManager.addPopUp(twAjouterVersion,DisplayObject(parentApplication),true);
			PopUpManager.centerPopUp(twAjouterVersion);				
								
		}
		
		protected function btSupprimerVersionClickHandler(me : MouseEvent):void{			
			if (dgVersionsTarifs.selectedIndex != -1){				
				ConsoviewAlert.afficherAlertConfirmation(btSupprimerVersion.label,"Confirmer",executeSupprimerVersionClick);
			}			
		}		
		
		private function executeSupprimerVersionClick(eventObj:CloseEvent):void{
			if (eventObj.detail == Alert.OK){	
				var version : VersionDeTarif = dgVersionsTarifs.selectedItem as VersionDeTarif;
				if (version != null){
					gestionTarifs.selectedVersion = version;
					gestionTarifs.supprimerVersion();
				}
			}
		}
		
		private function twAjouterVersionAjouterHandler(ev : Event):void{
			
		}
		
		private function reset():void{
					
		}
		
		/*
		* Fonction qui permet d'affecter la période sur laquelle on souhaite travailler
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		* 
		* */
		private  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
					//---- NOT USED ----//										
			/* var formMoisD : String = moisDebut.substr(0,2) 
									+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
									+ moisDebut.substr(6,4);
									
			var formMoisF : String = moisFin.substr(0,2)
								   	+ " " 
								   	+ DateFunction.moisEnLettre(parseInt(moisFin.substr(3,2),10)-1) 
								   	+ " " 
								   	+ moisFin.substr(6,4);

			lblPeriode.text = "Factures émises entre le  " + formMoisD + " et le " + formMoisF;			 */		
		}
		
		
		//Initialisation de la periode
		private function initPeriode():void{
				//--- NOT USED ---//
			/* var periodeArray : Array = psPeriode.getTabPeriode();
		    var len : int = periodeArray.length;
		    var month : AMonth;		    					   
			month = periodeArray[len-2];								
			dateDebut = month.dateDebut;	
			dateFin = month.dateFin;
			setSelectedPeriode(month.getDateDebut(),month.getDateFin()); */
		}	
		
		protected function activerBoutonEditSuppVersion(bool : Boolean = true):void{
			if (dgVersionsTarifs.selectedIndex != -1 && bool){
				btModifierVersion.enabled = true;
				btSupprimerVersion.enabled = true;
			} 
			else {
				btModifierVersion.enabled = false;
				btSupprimerVersion.enabled = false;						
			}
		}
		
		protected function activerBoutonAjouterVersion(bool : Boolean = true):void{
			if (dgTarifsAbos.selectedIndex != -1 && bool){
				btAjouterVersion.enabled = true;
			}else if (dgTarifsConsos.selectedIndex != -1 && bool){
				btAjouterVersion.enabled = true;
			}else {
				btAjouterVersion.enabled = false;
			}
		}
		
		
		
		
	}
}