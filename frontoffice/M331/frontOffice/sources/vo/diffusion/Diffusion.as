package vo.diffusion
{
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	
	public class Diffusion
	{
		private var _idRapport:int;
		private var _idRapportFormat:int;
		private var _idDiffusion:int;
		private var _idRacine:int;
		private var _noContrat:int;
		private var _nomProfil:String;
		private var _dateProchainEnvoi:String;
		private var _dateDernierEnvoi:String;
		private var _nbDestinataire:int;
		private var _idProfil:int;
		private var _periodeEtude:String;
		private var _nomOrga:String;
		private var _nomRapport:String;
		private var _jourEnvoi:int;
		private var _lblJourEnvoi:String;
		private var _active:int;
		private var _libelle:String;
		private var _destinatairesCaches:String;
		private var _objetMail:String;
		private var _nomAttachementFile:String;
		private var _corpsMail:String;
		private var _idOrga:int;
		private var _idFinPeriode:int; // id periode fin 
		private var _idRapportRacine:int;

		public function Diffusion()
		{

		}

		public function fill(uneDiffusion:Object):void
		{
			this._idRapportFormat=uneDiffusion.IDRAPPORT_FORMAT;
			this._idRapport=uneDiffusion.IDRAPPORT;
			this._idDiffusion=uneDiffusion.IDDIFFUSION;
			this._idRacine=uneDiffusion.IDRACINE;
			this._noContrat=uneDiffusion.NO_CONTRAT;
			this._nomProfil=uneDiffusion.NOM_PROFIL;

			if (uneDiffusion.NOM_PROFIL == null)
			{
				this._nomProfil=ResourceManager.getInstance().getString('M331', 'Tous');
			}
			this._dateProchainEnvoi=dateFormat(uneDiffusion.DATEPROCHAINENVOI);
			this._dateDernierEnvoi=dateFormat(uneDiffusion.DATEDERNIERENVOI);
			if (uneDiffusion.DATEDERNIERENVOI == null)
			{
				_dateDernierEnvoi=ResourceManager.getInstance().getString('M331', 'Pas_de_date');
			}
			this._nbDestinataire=uneDiffusion.NBDESTINATAIRES;
			this._idProfil=uneDiffusion.IDPROFIL;
			this._periodeEtude=uneDiffusion.PERIODEETUDE;
			this._nomOrga=uneDiffusion.NOMORGA;
			this._nomRapport=uneDiffusion.NOMRAPPORT;
			this._jourEnvoi=uneDiffusion.JOURENVOI;

			if (uneDiffusion.JOURENVOI == -1)
			{
				lblJourEnvoi=ResourceManager.getInstance().getString('M331', 'Dernier_jour');
			}
			else
			{
				lblJourEnvoi=_jourEnvoi.toString();
			}

			this._active=uneDiffusion.ACTIVE;
			this._libelle=uneDiffusion.LIBELLE;
			this._objetMail=uneDiffusion.EMAIL_SUBJECT;
			this._nomAttachementFile=uneDiffusion.FILE_NAME;
			this._corpsMail=uneDiffusion.BODY;
			this._destinatairesCaches=uneDiffusion.EMAIL_BCC;
			this._idOrga=uneDiffusion.IDORGA;
			this._idFinPeriode=uneDiffusion.PERIODICITE_FIN;
			this._idRapportRacine=uneDiffusion.IDRAPPORT_RACINE;
		}

		private function dateFormat(pDate:Date):String
		{
			var lDf:DateFormatter=new DateFormatter();
			lDf.formatString="DD/MM/YYYY";
			var lDateFormatee:String=lDf.format(pDate);
			return lDateFormatee;
		}

		public function get idRapport():int
		{
			return _idRapport;
		}

		public function set idRapport(value:int):void
		{
			_idRapport=value;
		}

		public function get idDiffusion():int
		{
			return _idDiffusion;
		}

		public function set idDiffusion(value:int):void
		{
			_idDiffusion=value;
		}

		public function get idRacine():int
		{
			return _idRacine;
		}

		public function set idRacine(value:int):void
		{
			_idRacine=value;
		}

		public function get noContrat():int
		{
			return _noContrat;
		}

		public function set noContrat(value:int):void
		{
			_noContrat=value;
		}

		public function get nomProfil():String
		{
			return _nomProfil;
		}

		public function set nomProfil(value:String):void
		{
			_nomProfil=value;
		}

		public function get dateProchainEnvoi():String
		{
			return _dateProchainEnvoi;
		}

		public function set dateProchainEnvoi(value:String):void
		{
			_dateProchainEnvoi=value;
		}

		public function get dateDernierEnvoi():String
		{
			return _dateDernierEnvoi;
		}

		public function set dateDernierEnvoi(value:String):void
		{
			_dateDernierEnvoi=value;
		}

		public function get nbDestinataire():int
		{
			return _nbDestinataire;
		}

		public function set nbDestinataire(value:int):void
		{
			_nbDestinataire=value;
		}

		public function get idProfil():int
		{
			return _idProfil;
		}

		public function set idProfil(value:int):void
		{
			_idProfil=value;
		}

		public function get periodeEtude():String
		{
			return _periodeEtude;
		}

		public function set periodeEtude(value:String):void
		{
			_periodeEtude=value;
		}

		public function get nomOrga():String
		{
			return _nomOrga;
		}

		public function set nomOrga(value:String):void
		{
			_nomOrga=value;
		}

		public function get nomRapport():String
		{
			return _nomRapport;
		}

		public function set nomRapport(value:String):void
		{
			_nomRapport=value;
		}

		public function get jourEnvoi():int
		{
			return _jourEnvoi;
		}

		public function set jourEnvoi(value:int):void
		{
			_jourEnvoi=value;
		}
		
		[Bindable]
		public function get lblJourEnvoi():String
		{
			return _lblJourEnvoi;
		}
	
		public function set lblJourEnvoi(value:String):void
		{
			_lblJourEnvoi = value;
		}

		public function get active():int
		{
			return _active;
		}

		public function set active(value:int):void
		{
			_active=value;
		}

		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle=value;
		}

		public function get destinatairesCaches():String
		{
			return _destinatairesCaches;
		}

		public function set destinatairesCaches(value:String):void
		{
			_destinatairesCaches=value;
		}

		public function get objetMail():String
		{
			return _objetMail;
		}

		public function set objetMail(value:String):void
		{
			_objetMail=value;
		}

		public function get nomAttachementFile():String
		{
			return _nomAttachementFile;
		}

		public function set nomAttachementFile(value:String):void
		{
			_nomAttachementFile=value;
		}

		public function get corpsMail():String
		{
			return _corpsMail;
		}

		public function set corpsMail(value:String):void
		{
			_corpsMail=value;
		}

		public function get idOrga():int
		{
			return _idOrga;
		}

		public function set idOrga(value:int):void
		{
			_idOrga=value;
		}

		public function get idRapportFormat():int
		{
			return _idRapportFormat;
		}

		public function set idRapportFormat(value:int):void
		{
			_idRapportFormat=value;
		}
		
		public function get idFinPeriode():int
		{
			return _idFinPeriode;
		}
		
		public function set idFinPeriode(value:int):void
		{
			_idFinPeriode = value;
		}
		
		public function get idRapportRacine():int
		{
			return _idRapportRacine;
		}
		
		public function set idRapportRacine(value:int):void
		{
			_idRapportRacine = value;
		}
	}
}
