<!--- =========================================================================
Name: AboStrategy
Original Author: Brice Miramont
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="Strategy" hint="" extends="AbstractStrategy">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="Strategy" displayname="Strategy init()" hint="Initialize the AboStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- ================================= ========================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		<cfreturn queryNew("null","VarChar")>
	</cffunction>
	
	<cffunction name="getData_cat" access="public" returntype="query" output="false" displayname="query getData(integer, string, date, date)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="date DateDebut" hint=%qt%%paramNotes%%qt% />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="date DateFin" hint=%qt%%paramNotes%%qt% />
		<cfreturn queryNew("null","VarChar")>
	</cffunction>
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" hint="Retourne le type du produit passé en paramétre." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getProduit" access="public" returntype="string" output="false" hint="Retourne le libellé du produit." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getOperateur" access="public" returntype="string" output="false" hint="Retourne le nom de l'opérateur pour le produit concerné." >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getCompte" access="public" returntype="string" output="false" hint="Retourne le libéllé du périmétre." >
		<cfargument name="IDcompte" required="false" type="numeric" default="" displayname="numeric IDcompte" hint=%qt%%paramNotes%%qt% />
	</cffunction>
	<cffunction name="getStrategy" access="public" returntype="string" output="false" hint="Retourne l'appellation du périmétre (Ex: société = compte Hiérarchique)." >
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>

</cfcomponent>