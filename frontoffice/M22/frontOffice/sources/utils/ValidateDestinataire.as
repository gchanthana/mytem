package utils
{
	import vo.DestinataireImport;

	public class ValidateDestinataire
	{			
		private var _ISMAILOK:Boolean;
		private var _ISNOMOK:Boolean;
		private var _ISPRENOMOK:Boolean;
		private var _destinataire:DestinataireImport;
				
		public function ValidateDestinataire(dest:DestinataireImport)
		{
			this._destinataire=dest;
		}
		
		public function get isPrenomOk():Boolean
		{
			return _ISPRENOMOK;
		}

		public function set isPrenomOk(value:Boolean):void
		{
			_ISPRENOMOK = value;
		}

		public function get isNomOk():Boolean
		{
			return _ISNOMOK;
		}

		public function set isNomOk(value:Boolean):void
		{
			_ISNOMOK = value;
		}

		public function get isMailOk():Boolean
		{
			return _ISMAILOK;
		}

		public function set isMailOk(value:Boolean):void
		{
			_ISMAILOK = value;
		}

		public function valide():void
		{
			_ISNOMOK=false;
			_ISPRENOMOK=false;
			_ISMAILOK=false;
			

			
			var emailExpression:RegExp = /^([a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4})$/;
			
			var nomPrenomExpresion:RegExp=/^[àâäáãæÀÁÂÃÄÆîïìíÌÍÎÏôöòóõÒÓÔÖÕçÇùûüúÙÚÛÜéèêëÉÈÊËa-zA-Z0-9 ]*$/;
			
			if(emailExpression.test(_destinataire.mail))
			{
				_ISMAILOK=true
			}
			if(nomPrenomExpresion.test(_destinataire.nom))
			{
				_ISNOMOK=true
			}
			if(nomPrenomExpresion.test(_destinataire.prenom))
			{
				_ISPRENOMOK=true;
			}
		}
		
		/**
		 *  
		 * permet de verifier si l'objet destinataire a une property 
		 * et mettre a jour les properties de (_ISMAILOK,_ISNOMOK,_ISPRENOMOK)
		 */		
		public function isPropertyValid(value:String):Boolean
		{
			if (_destinataire.hasOwnProperty(value))
			{
				valide();
				return this["_IS"+(value).toUpperCase()+"OK"];
			}
			
			return false;
		}
		

	}
}