package appli.CV4.CreateBanniereCV4
{
	import appli.control.AppControlEvent;
	import appli.impersonnification.popup.PopupChooseOtherAccountIHM;
	import appli.impersonnification.refreshManager;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import general.interfaceClass.ICreateBanniere;
	
	import mx.containers.Canvas;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.LinkButton;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class CreateBanniereCV4Impl extends Canvas implements ICreateBanniere
	{
		public var lblLoggedUserName:Label;
		public var lblToDay:Label;
		public var lkbDeconnexion:LinkButton;
		public var imgDeconnexion:Image;
		public var lblChangeAccount:Label;
		public var popupChooseOtherAccount:PopupChooseOtherAccountIHM = new PopupChooseOtherAccountIHM();
		
		public function CreateBanniereCV4Impl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function logoff(event:Event):void
		{
			lblLoggedUserName.text = "";
			lblToDay.text = "";
			lkbDeconnexion.removeEventListener(MouseEvent.CLICK,logoff);
			CvAccessManager.deconnectSession();
		}
		
		public function init(evt:FlexEvent = null):void
		{
			lkbDeconnexion.addEventListener(MouseEvent.CLICK, _lkbDeconnexionClickHandler);
			imgDeconnexion.addEventListener(MouseEvent.CLICK, _lkbDeconnexionClickHandler);
			lblLoggedUserName.text = ResourceManager.getInstance().getString('M00', 'Bonjour') + " "+CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM; 
			
			var myDate :Date = new Date();
			var myFormatter : DateFormatter = new DateFormatter();
			
			myFormatter.formatString = ResourceManager.getInstance().getString('M00', 'EEEE_D_MMMM_YYYY'); 
			
			lblToDay.text = myFormatter.format(myDate);
			
			// maj du jour de la semaine dans la langue correspondante a l'utilisateur
			switch (myDate.getDay())
			{
				case 0 : // Sunday
					lblToDay.text = lblToDay.text.replace("Sunday",ResourceManager.getInstance().getString('M00','Sunday'));
					break;
				case 1 : // Monday
					lblToDay.text = lblToDay.text.replace("Monday",ResourceManager.getInstance().getString('M00','Monday'));
					break;
				case 2 : // Tuesday
					lblToDay.text = lblToDay.text.replace("Tuesday",ResourceManager.getInstance().getString('M00','Tuesday'));
					break;
				case 3 : // Wednesday
					lblToDay.text = lblToDay.text.replace("Wednesday",ResourceManager.getInstance().getString('M00','Wednesday'));
					break;
				case 4 : // Thursday
					lblToDay.text = lblToDay.text.replace("Thursday",ResourceManager.getInstance().getString('M00','Thursday'));
					break;
				case 5 : // Friday
					lblToDay.text = lblToDay.text.replace("Friday",ResourceManager.getInstance().getString('M00','Friday'));
					break;
				case 6 : // Saturday
					lblToDay.text = lblToDay.text.replace("Saturday",ResourceManager.getInstance().getString('M00','Saturday'));
					break;
			}
			
			// maj du mois de l'année dans la langue correspondante a l'utilisateur
			switch (myDate.getMonth())
			{
				case 0 : // January
					lblToDay.text = lblToDay.text.replace("January",ResourceManager.getInstance().getString('M00','January'));
					break;
				case 1 : // February
					lblToDay.text = lblToDay.text.replace("February",ResourceManager.getInstance().getString('M00','February'));
					break;
				case 2 : // March
					lblToDay.text = lblToDay.text.replace("March",ResourceManager.getInstance().getString('M00','March'));
					break;
				case 3 : // April
					lblToDay.text = lblToDay.text.replace("April",ResourceManager.getInstance().getString('M00','April'));
					break;
				case 4 : // May
					lblToDay.text = lblToDay.text.replace("May",ResourceManager.getInstance().getString('M00','May'));
					break;
				case 5 : // June
					lblToDay.text = lblToDay.text.replace("June",ResourceManager.getInstance().getString('M00','June'));
					break;
				case 6 : // July
					lblToDay.text = lblToDay.text.replace("July",ResourceManager.getInstance().getString('M00','July'));
					break;
				case 7 : // August
					lblToDay.text = lblToDay.text.replace("August",ResourceManager.getInstance().getString('M00','August'));
					break;
				case 8 : // September
					lblToDay.text = lblToDay.text.replace("September",ResourceManager.getInstance().getString('M00','September'));
					break;
				case 9 : // October
					lblToDay.text = lblToDay.text.replace("October",ResourceManager.getInstance().getString('M00','October'));
					break;
				case 10 : // November
					lblToDay.text = lblToDay.text.replace("November",ResourceManager.getInstance().getString('M00','November'));
					break;
				case 11 : // December
					lblToDay.text = lblToDay.text.replace("December",ResourceManager.getInstance().getString('M00','December'));
					break;
			}
		}
		public function chooseOtherAccount():void
		{
			PopUpManager.addPopUp(popupChooseOtherAccount, this, true);
			PopUpManager.centerPopUp(popupChooseOtherAccount);
			
			popupChooseOtherAccount.addEventListener("LOGIN_CHOSEN", accountChosenHandler);
		}
		public function chooseOtherAccount_mouseOverHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#358dd7'); /**set*/
		}
		public function chooseOtherAccount_mouseOutHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#ffffff');
		}
		private function refreshName():void
		{
			trace("refresh en cours");
			refreshManager.refreshApp();
		}
		public function accountChosenHandler(evt:Event):void
		{
			refreshName();
		}

		private function _lkbDeconnexionClickHandler(event:MouseEvent):void
		{
			
			logoff(null); 
		}
	}
}