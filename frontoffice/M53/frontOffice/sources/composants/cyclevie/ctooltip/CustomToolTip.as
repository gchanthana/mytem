package composants.cyclevie.ctooltip
{
	
	import mx.core.*;
    import mx.containers.*;

    public class CustomToolTip extends VBox implements IToolTip
    {
	   public function CustomToolTip()
	   {
	       mouseEnabled = false;
	       mouseChildren = false;
	       setStyle("paddingLeft", 10);
	       setStyle("paddingTop", 10);
	       setStyle("paddingBottom", 10);
	       setStyle("paddingRight", 10);
	   }
	
	   public function get text():String {     
	           return null; 
	   }
	   public function set text(value:String):void {}
	}	
}