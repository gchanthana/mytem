package composants.util
{
	import mx.collections.ArrayCollection;
	import flash.display.DisplayObject;
	
	public class ConsoviewUtil
	{
		public static function scale(d : DisplayObject, sx : Number, sy : Number):void{
			d.scaleX = sx;
			d.scaleY = sy;
		}
		
		public static function getIndexById(source : ArrayCollection,nomColonne : String,id:int):int{
			var len : int = source.length;
			for (var i : int = 0; i < len; i++){				
				if (source[i][nomColonne] == id) return i;
			}
			return -1;
		}
		
		public static function getIndexByLabel(source : ArrayCollection,nomColonne : String,label:String):int{
			var len : int = source.length;
			for (var i : int = 0; i < len; i++){				
				if (source[i][nomColonne] == label) return i;
			}
			return -1;
		}
		
		public static function extractIDs(nomColonne : String,source : Array):Array{
			var len : int = source.length;
			var newArray : Array = new Array();
			for (var i : int = 0; i < len; i++){								
				newArray.push(source[i][nomColonne]);	
			}
			return newArray;			
		} 
		
		public static function extractCollectionIDs(nomColonne : String,source : ArrayCollection):Array{
			var len : int = source.length;
			var newArray : Array = new Array();
			for (var i : int = 0; i < len; i++){								
				newArray.push(source[i][nomColonne]);	
			}
			return newArray;			
		} 
		 
		
		public static function isPresent(s:String, a : Array):Boolean{			
			
			for (var i:int = 0; i < a.length; i++){				 
					 if ( s == a[i].toString() )  return true; 				 
			}
			return false;
		}
		
		public static  function isLabelInArray(label:String, colname:String, acol : Array):Boolean{			
			
			var ligne : Object;				
			var len : int = acol.length;
			for (ligne in acol){				
 				if (label == acol[ligne][colname])  return true; 
			}		
			return false;
		}	
		
		public static  function isIdInArray(id : int, colname:String, acol : Array):Boolean{			
			
			var ligne : Object;				
			var len : int = acol.length;
			for (ligne in acol){				
 				if (id == acol[ligne][colname]) return true; 
			}		
			return false;
		}
		
		public static function is2InArray(param1:String,param2:String, colname1:String, colname2:String, acol : Array):Boolean{			
			var ligne : Object;				
			var len : int = acol.length;
			for (ligne in acol){
				if ((param1 == acol[ligne][colname1])&&(param2 == acol[ligne][colname2])) return true; 
			}
			
			return false;
		}		
		
		public static function extraireColonne(liste : ArrayCollection, nomColonne : String):Array{
			var len : int = liste.length;
			var tab: Array = new Array();
			if (nomColonne != null){
				for (var i : int = 0; i < len ; i++){
					tab.push(liste[i][nomColonne]);
				}	
			}else{
				for (var j : int = 0; j < len ; j++){
					tab.push(0);
				}
			}
			return tab;
		}
		
		public static function arrayToList(myCollection : ArrayCollection, nomColonne : String,separateur : String):String{
			var liste : String = "";
			
			if (nomColonne != null){
				var tab : Array = ConsoviewUtil.extraireColonne(myCollection,nomColonne);
				var len : int = tab.length;
				for (var i : int = 0; i < len; i ++){
					liste = liste + tab[i] + separateur + " ";
				}
				var lg : int = liste.length;
				return liste.slice(0,lg-2);
			}			
			return " ";
		}		
		
		
		/**
		* Met à jour le total dans la case total
		* @param source l'ArrayCollection source
		* @param nomCol le nom de la colone pour laquelle on fait le total
		* @return total le total
		**/
		public static function calculTotal(source : ArrayCollection,nomCol : String):Number{
			if (source == null)
				return 0
			else if (source.length == 0)
				return 0
			else if (source[0].hasOwnProperty(nomCol) && !isNaN(source[0][nomCol])){
				var total : Number = 0;
				for (var i : int = 0; i < source.length; i ++){
					total = total + source[i][nomCol];
				}
				return total;	
			}else{
				return 0	
			}
			return 0
		}
		
		/**
		* Met à jour le total dans la case total en excluant certaine valeurs
		* @param source l'ArrayCollection source
		* @param nomCol le nom de la colone pour laquelle on fait le total
		* @return total le total
		**/
		public static function calculTotalEx(source : ArrayCollection,nomCol : String,nomColEx : String,valueEx : Object):Number{
			if (source == null)
				return 0
			else if (source.length == 0)
				return 0
			else if (source[0].hasOwnProperty(nomCol) && !isNaN(source[0][nomCol])){
				var total : Number = 0;
				for (var i : int = 0; i < source.length; i ++){
					if (source[i][nomColEx] != valueEx)	total = total + source[i][nomCol];
				}
				return total;	
			}else{
				return 0	
			}
			return 0
		}
	}
}