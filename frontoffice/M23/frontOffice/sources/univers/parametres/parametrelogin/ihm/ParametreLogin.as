package univers.parametres.parametrelogin.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.parametres.parametrecompte.ihm.GestionPool;
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrelogin.event.GestionLoginEvent;
	import univers.parametres.parametrelogin.service.LoginService;



	[Bindable]
	public class ParametreLogin extends TitleWindow
	{
		
		public var login : Gestionnaire = new Gestionnaire();
		public var loginService : LoginService = new LoginService(); 
		
		//UI COMPONANT
		public var checkRestrictionIP : CheckBox;
		
		public function ParametreLogin()
		{
			
		}
		public function checkRestrictionIPHandler():void
		{
			if(checkRestrictionIP.selected)
			{
				GestionPool.afficherMessage("Attention, si vous activez la restriction d'adresse IP sans configurer cette option dans l'onglet 'Adresse IP', l'utilisateur ne pourra plus accéder à ConsoView.",
				"Information",
				"OK",
				GestionPool.COLOR_MESSAGE);
			}
		}
		public function init(evt : FlexEvent):void
		{
			
			if(login.id>0)
			{
				initMode_update();
			}
			else
			{
				initMode_create();
				
			}
		}
		private function initMode_update():void
		{
			registerClassAlias("univers.parametres.parametrelogin.vo.Gestionnaire",Gestionnaire);  
			login= Gestionnaire(ObjectUtil.copy(login))  // delete les ref sur le grid
		}
		private function initMode_create():void
		{
			login.password = generateMotDePasse(7);
			
		}
		private function initAttribut():void
		{
			loginService.col_acces = new ArrayCollection();
			loginService.col_acces_not_formated = new ArrayCollection();
		}
		public function fermer(event : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		public function valider():void
		{
			var adrIpActiver : int;
			if(checkRestrictionIP.selected)
			{
				adrIpActiver = 1;
			}
			else
			{
				adrIpActiver = 0;
			}
			loginService.addEventListener(GestionLoginEvent.LOGIN_INFO_CHANGE_COMPLETE,login_info_change_handler);
			if(login.id>0)
			{
				//update
				loginService.updateUser(login.id,login.nom,login.prenom,login.adresse,login.cp,login.ville,login.tel,login.direction,login.mail,login.password,adrIpActiver);
			}
			else
			{
				//add
				loginService.createUser(login.nom,login.prenom,login.adresse,login.cp,login.ville,login.tel,login.direction,login.mail,login.password,adrIpActiver);
			}
		}
		private function login_info_change_handler(evt : GestionLoginEvent):void
		{
			dispatchEvent(new GestionLoginEvent(GestionLoginEvent.PARAMETRE_LOGIN_COMPLETE));
			var msg : String;
			if(login.id>0)
			{
				msg = "Les modifications ont bien été enregistrées.";
				ConsoviewAlert.afficherOKImage(msg);
				PopUpManager.removePopUp(this);
			}
			else
			{
				login.id = evt.idLogin; // La vbox contenant le grid des attribut du profil est bindable avec cette propriété
				initAttribut(); // Rempli le grid
			}
		}
	
	
		public function combo_revendeur_commande_handler(evt : Event):void
		{
			
		}
		private function generateMotDePasse(longueur :int):String
		{
		   var chars :String  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
		   var pass : String = "";
		   
		    for(var i :int=0;i<longueur;i++)
		    {
		    	var x : int = Math.floor((Math.random() * 62)); // 62 nbChoix
		       	pass += chars.charAt(x);
		    }
		   	return pass;
		}
		
		
		public function sendPass():void
		{
			loginService.addEventListener(GestionLoginEvent.SEND_PASS_COMPLETE,sendPassCompleteHandler);
			loginService.envoyerMDPParMail(login.nom,login.prenom,login.mail,login.password);
		}
		public function sendLogin():void
		{
			loginService.addEventListener(GestionLoginEvent.SEND_MAIL_COMPLETE,sendMailCompleteHandler);
			loginService.envoyerLoginParMail(login.nom,login.prenom,login.mail);
		}
		private function sendMailCompleteHandler(evt : GestionLoginEvent):void
		{
			loginService.removeEventListener(GestionLoginEvent.SEND_MAIL_COMPLETE,sendMailCompleteHandler);
			var msg : String = "L'Email a bien été envoyé à l'utilisateur.";
			ConsoviewAlert.afficherOKImage(msg);
		}
		private function sendPassCompleteHandler(evt : GestionLoginEvent):void
		{
			loginService.removeEventListener(GestionLoginEvent.SEND_PASS_COMPLETE,sendPassCompleteHandler);
			var msg : String = "Le mot de passe a bien été envoyé à l'utilisateur.";
			ConsoviewAlert.afficherOKImage(msg);
		}
	}
}