package ihm {
	import flash.events.Event;

	/**
	 * Controller : Entité du modèle MVC qui écoute les évènements émis par les entité View et Model.
	 * Les évènements émis par l'entité View correspondent uniquement à des actions utilisateur.
	 * Les évènements émis par l'entité Model correspondent à des mises à jour concernant les données.
	 * Cette entité est chargée d'effectuer et coordonner les traitements métiers à effectuer suite à ces évènements. 
	 * */
	public class MainImpl {
		private var viewInstance:Main; // Vue
		private var modelInstance:MainModel; // Model
		 
		public function MainImpl(view:Main=null,model:MainModel=null) {
			this.view=view;
			modelInstance=model;
		}
		
		// Définit la référence vers l'entité View et celle de l'entité Model (obtenue à partir de View.model) 
		public function set view(implInstance:Main):void {
			viewInstance=implInstance; // Vue
			if(viewInstance) {
				registerViewListeners();
				modelInstance=viewInstance.model; // Model
				registerModelListeners();
			}
		}
		
		/** Met à jour les valeurs des propriétés suivantes au niveau de l'entité View :
		 * 		- Statut d'activation des controles utilisateur
		 * 		- Statut de validité des propriétés : Email administrateur MDM, Mots de passes
		 * Chaque paramètre correspond aux controles suivants : lockEnable (Verrouillage), cancelEnable (Annulation),
		 * resetEnable (Réinitialisation), updateEnable (Enregistrement des modifications), unlockEnable (Déverrouilage)
		 * Chaque paramètre doit avec la valeur TRUE pour activer le controle correspondant et FALSE sinon.
		 * Règles métiers qui sont appliquées quelque soit les valeurs fournies pour les paramètres :
		 * 		- Si lockEnable vaut FALSE : La valeur FALSE sera utilisée pour unlockEnable.
		 * 		- Si la donnée IS_DEFAULT vaut TRUE : La valeur FALSE sera utilisée pour resetEnable.
		 * 		- Si les valeurs des données sont invalides : La valeur FALSE sera utilisée pour updateEnable.
		 * */
		private function updateViewConfiguration(lockEnable:Boolean,cancelEnable:Boolean,resetEnable:Boolean,updateEnable:Boolean,unlockEnable:Boolean):void {
			var isMdmDefaultValues:Boolean=Boolean(modelInstance.getCurrentMdmServerProperty("IS_DEFAULT"));
			var dataAreValid:Boolean=dataAreValid();
			// Mise à jour des statut de validité des propriétés (Admin Email, Mots de passes)
			viewInstance.displayMdmAdminEmailStatus(modelInstance.isAdminEmailValid());
			viewInstance.displayPasswordConfirmStatus(modelInstance.wsPasswordConfirmed);
			viewInstance.displayEnrollPasswordConfirmStatus(modelInstance.enrollPasswordConfirmed);
			// Mise à jour des statuts d'activation des controles utilisateur
			viewInstance.lockInfos=lockEnable;
			viewInstance.cancelEnabled=cancelEnable;
			viewInstance.resetValuesEnabled=(isMdmDefaultValues ? false:resetEnable);
			viewInstance.updateEnabled=(dataAreValid ? updateEnable:false);
			viewInstance.unlockEnabled=(lockEnable ? unlockEnable:false);
		}
		
		private function dataAreValid():Boolean
		{
			var valid:Boolean = modelInstance.wsPasswordConfirmed && modelInstance.enrollPasswordConfirmed 
				&& modelInstance.isAdminEmailValid() && (viewInstance.mdmProviders.selectedIndex != -1)
				&& (viewInstance.mdmIn.text != "") && (viewInstance.mdmWSUserIn.text != "")
				&& (viewInstance.mdmWSPwdIn.text != "") && (viewInstance.mdmEnrollUserIn.text != "")
				&& (viewInstance.mdmEnrollPwdIn.text != "");
			return valid;
		}
		/**
		 * View Listener : L'utilisateur a spécifié le type de mise à jour qu'il souhaite effectuer (i.e Avec ou sans test de connexion)
		 * Le type de mise à jour est déterminé par le type de l'évènement eventObject qui est fourni en paramètre.
		 * Si eventObject.type vaut Main.UPDATE_WITH_SERVER_CHECK : Un test de connexion avec le serveur MDM sera effectué avant la mise à jour des infos
		 * Sinon si eventObject.type vaut Main.UPDATE_WITHOUT_SERVER_CHECK : Pas de test de connexion avec le serveur MDM avant la mise à jour des infos
		 * Dans tous les cas les infos seront mises à jour. L'entité View procèdera à la validation du choix utilisateur puis délèguera à l'entité Model la mise à jour.
		 * */
		private function viewUpdateOptionsConfirmedAction(eventObject:Event):void {
			var updateOptions:Object=new Object();
			updateOptions[MainModel.CHECK_SERVER_CONNECTION]=(eventObject.type == Main.UPDATE_WITH_SERVER_CHECK ? true:false);
			modelInstance.updateMdmServerInfos(updateOptions); // Mise à jour des infos actuelles
		}
		
		/**
		 * View Listener : L'utilisateur interagit avec un des controles Déverrouillage, Enregistrement (des modifications), Annulation
		 * L'état d'activation des controles sont d'abord mis à jour puis l'action correspondante est effectuée
		 * Règles métiers appliquées lors des interactions avec les controles utilisateur : 
		 * 		- Si l'utilisateur interagit avec le controle Déverrouillage : Les infos sont déverrouillées
		 * 		- Si l'utilisateur interagit avec le controle d'Annulation : Les infos sont verrouillées et les valeurs d'origines sont restaurées
		 * 		- Si l'utilisateur interagit avec le controle Enregistrement :
		 * 			- Les infos sont verrouillées avant d'effectuer l'enregistrement
		 * 			- Si la valeur de la propriété IS_DEFAULT est FALSE : Il sera demandé à l'utilisateur de choisir des options avant l'enregistrement
		 * 			- Sinon : L'enregistrement des modifications est effectué sans choix d'options (La valeur nulle sera fournie en tant qu'options)
		 * */
		private function viewControlAction(eventObject:Event):void {
			if(eventObject) {
				switch(eventObject.type) {
					case Main.USER_UPDATE_INFOS: // Enregistrement
						var isMdmDefaultValues:Boolean=Boolean(modelInstance.getCurrentMdmServerProperty("IS_DEFAULT"));
						updateViewConfiguration(true,false,false,false,false);
						if(isMdmDefaultValues)
							modelInstance.updateMdmServerInfos(null); // Enregistrement de la réinitialisation des valeurs des infos 
						else
							viewInstance.displayServerCheckBeforeUpdate(); // Demande à l'utilisateur de choisir des options avant l'enregistrement
						break;
					case Main.USER_UNLOCK_INFOS: // Déverrouillage
						updateViewConfiguration(false,true,true,false,false);
						break;
					case Main.USER_CANCEL_CHANGES: // Annulation
						modelInstance.cancelModifications();
						var allowValidation:Boolean = modelInstance.mdmServerInfosJustGotReseted();//on regarde si un reset a été fait avant le cancel
						updateViewConfiguration(true,false,false,allowValidation,true);//false en 4e argument avant, 
															//on autorise la validation après annulation pour gérer le cas où l'utilisateur annule un reset (sauvegardé) => MYT-956
						break;
				}
			}
		}		
		
		/**
		 * Model Listener : Les infos du serveur MDM de la racine ont été mises à jour
		 * La configuration des controles doit correspondre à l'état "Initial" - Voir viewComplete()
		 * */
		private function modelMdmInfosUpdated(event:Event):void {
			updateViewConfiguration(true,false,false,false,true);
		}
		
		/**
		 * Model Listener : Les valeurs des infos ont été modifiées
		 * Ce listener est exécuté pour les types d'évènement suivants :
		 * 		- MainModel.MDM_SERVER_INFOS_RESETED : Les valeurs ont été réinitialisées par leur valeurs par défaut.
		 * 		- MainModel.MDM_SERVER_INFOS_CHANGED : Les valeurs sont différentes de celles des infos actuelles.
		 * 		- MainModel.MDM_SERVER_INFOS_NOT_CHANGED : Les valeurs sont restées identiques (inchangées) à celles d'origine.
		 * Règles métiers appliquées lors de la modification des valeurs pour les cas : MainModel.MDM_SERVER_INFOS_CHANGED, MainModel.MDM_SERVER_INFOS_NOT_CHANGED
		 * 		-  La valeur utilisée pour resetEnable est l'inverse de celle de la propriété IS_DEFAULT
		 * */
		private function modelDataChanged(eventObject:Event):void {
			if(eventObject) {
				var isMdmDefaultValues:Boolean=Boolean(modelInstance.getCurrentMdmServerProperty("IS_DEFAULT"));
				switch(eventObject.type) {
					case MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED:
						modelInstance.saveMdmServerInfosBeforeReset();
						updateViewConfiguration(false,true,false,false,false);//true en premier argument avant pour le reset, modifié dans MYT-956
						break;
					case MainModel.MDM_SERVER_INFOS_RESETED:
						modelInstance.saveMdmServerInfosBeforeReset();
						updateViewConfiguration(true,true,false,true,false);
						break;
					case MainModel.MDM_SERVER_PASSWORD_CHANGED: // Géré comme MainModel.MDM_SERVER_INFOS_CHANGED
					case MainModel.MDM_SERVER_INFOS_CHANGED:
						updateViewConfiguration(false,true,(! isMdmDefaultValues),true,false);
						break;
					case MainModel.MDM_SERVER_INFOS_NOT_CHANGED:
						updateViewConfiguration(false,true,(! isMdmDefaultValues),false,false);
						break;
				}
			}
			
		}

		/**
		 * Model Listener : Les infos MDM de la racine ont été récupérées	
		 * Les valeurs d'activation des controles sont mis à jour pour correspondre aux états :
		 * "Infos récupérées", "Enregistrement des modifications"
		 * */
		private function modelMdmInfosLoaded(event:Event):void {
			updateViewConfiguration(true,false,false,false,true);
		}
		
		/**
		 * Model Listener : Une erreur s'est produite au niveau de l'entité Model
		 * Les valeurs d'activation des controles sont mis à jour pour correspondre à l'état "Erreur/Annulation"
		 * L'utilisateur est notifié par un un message d'erreur
		 * Les valeurs d'origine des infos sont restaurées par annulation des modifications 
		 * */
		private function modelFault(event:Event):void {
			updateViewConfiguration(true,false,false,false,true);
			viewInstance.displayErrorMessage();
			modelInstance.cancelModifications(); // Annulation des modifications et restitutions des valeurs d'origine
		}
		
		// Listeners sur l'entité View
		private function registerViewListeners():void {
			viewInstance.addEventListener(Main.UPDATE_WITH_SERVER_CHECK,viewUpdateOptionsConfirmedAction);
			viewInstance.addEventListener(Main.UPDATE_WITHOUT_SERVER_CHECK,viewUpdateOptionsConfirmedAction);
			// Listener des actions utilisateur sur les controles
			viewInstance.addEventListener(Main.USER_UPDATE_INFOS,viewControlAction);
			viewInstance.addEventListener(Main.USER_UNLOCK_INFOS,viewControlAction);
			viewInstance.addEventListener(Main.USER_CANCEL_CHANGES,viewControlAction);
		}
		
		// Listeners sur l'entité Model
		private function registerModelListeners():void {
			modelInstance.addEventListener(MainModel.FAULT,modelFault);
			modelInstance.addEventListener(MainModel.MDM_SERVER_PASSWORD_CHANGED,modelDataChanged);
			modelInstance.addEventListener(MainModel.MDM_SERVER_INFOS_RESETED_NOT_CHANGED,modelDataChanged);
			modelInstance.addEventListener(MainModel.MDM_SERVER_INFOS_RESETED,modelDataChanged);
			modelInstance.addEventListener(MainModel.MDM_SERVER_INFOS_CHANGED,modelDataChanged);
			modelInstance.addEventListener(MainModel.MDM_SERVER_INFOS,modelMdmInfosLoaded);
			modelInstance.addEventListener(MainModel.MDM_SERVER_INFOS_NOT_CHANGED,modelDataChanged);
			modelInstance.addEventListener(MainModel.MDM_SERVER_INFOS_UPDATED,modelMdmInfosUpdated);
		}
	}
}