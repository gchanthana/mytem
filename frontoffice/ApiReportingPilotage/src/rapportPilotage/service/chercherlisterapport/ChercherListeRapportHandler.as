package rapportPilotage.service.chercherlisterapport
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ChercherListeRapportHandler
	{
		private var _model:ChercherListeRapportModel;
		
		public function ChercherListeRapportHandler(_model:ChercherListeRapportModel)
		{
			this._model = _model;
		}
		
		internal function getListeRapportResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateListeRapports(event.result as ArrayCollection);
			}
		}	
	}
}