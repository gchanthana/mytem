/*
Cette classe étend FenetreAjouterSocieteIHM
Action : Affecter des sociétés à un groupe racine / modifier le libellé d'une société
*/
package intranet.gestionGroupe
{
	
	import mx.managers.PopUpManager;
	import mx.controls.Alert;
	import flash.events.MouseEvent;
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	import fr.consotel.consoview.util.ConsoViewErrorManager;
	import mx.rpc.events.FaultEvent;
	
	public class FenetreAjouterSociete extends FenetreAjouterSocieteIHM
	{  
		private var objGridSocieteAll : GridSocieteAll ;
		private var modif : Boolean;
		private var libelle : String;
		private var libelleOpe : String;
        public function FenetreAjouterSociete(objGridSocieteAll : GridSocieteAll,modif : Boolean,libelle : String = "",libelleOpe :String ="")
        {
        	super();
         	this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
      		this.objGridSocieteAll= objGridSocieteAll;   	
      		this.modif=modif;
      		this.libelle=libelle;
      		this.libelleOpe=libelleOpe;
      	}
        public function initIHM(event:Event) : void
        {
        	btCancel.addEventListener(MouseEvent.CLICK,removeMe);
        	btValide.addEventListener(MouseEvent.CLICK, submitData);
            
            comboOperateur.dataProvider=objGridSocieteAll.getListeOperateur();
            //Suppression de l'item "tous" :    
           (comboOperateur.dataProvider as ArrayCollection).removeItemAt(0);
           
           for(var i :int =0;i<  (comboOperateur.dataProvider as ArrayCollection).length;i++)
           {
	           	if((comboOperateur.dataProvider as ArrayCollection).getItemAt(i).NOM == libelleOpe)
	           	{
	           		comboOperateur.selectedItem = (comboOperateur.dataProvider as ArrayCollection).getItemAt(i) ;
	           	}
           }
           inputLibelleSct.text = libelle;
        }
        private function submitData(event:MouseEvent):void {
        	if (comboOperateur.selectedIndex==-1){
        		Alert.show("Choisissez un opérateur")
        	}
        	else
        	{
        		if(inputLibelleSct.text == ""){
        			Alert.show("erreur : Le libellé de société est vide")
        		}
        		else
        		{
	        		if(modif==false)
	        		{
		           		objGridSocieteAll.addSociete(inputLibelleSct.text);
		       		}	
	           		else
	           		{
	           			objGridSocieteAll.addSociete(inputLibelleSct.text,true);
	           		}
	           		PopUpManager.removePopUp(this);
         		}
         	}
        }

        // Cancel button click event listener.
        private function removeMe(event:MouseEvent):void {
            PopUpManager.removePopUp(this);
        }
        private function remoteError(faultEvent:FaultEvent):void {
			ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
		}	
        
	}
}