package univers.parametres.parametrecompte.ihm
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.RadioButton;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.GestionProfilEvent;
	import univers.parametres.parametrecompte.service.ProfilService;
	import univers.parametres.parametrecompte.vo.Action;
	import univers.parametres.parametrecompte.vo.ProfilCommande;
	import univers.parametres.parametrelogin.vo.ChampsPersosVO;
	

	[Bindable]
	public class ParametreProfil extends TitleWindow
	{
	
		public var ckxFixeAll:CheckBox
		public var ckxMobileAll:CheckBox
	
		public var profil : ProfilCommande = new ProfilCommande();
		public var profilService : ProfilService = new ProfilService();

		public var actionMobileCount:int = 0;
		public var actionFixeCount:int = 0;
				
		public var boolReadOnly:Boolean = false;
		public var boolNewProfil:Boolean = false;
		public var boolFixeVisible:Boolean = false;
		public var boolMobileVisible:Boolean = false;
		public var boolReseauVisible:Boolean = false;
		
		private var groupeIndex : int;
		
		public var col_actionOfProfil : ArrayCollection = new ArrayCollection();
		public var tab_actionOfProfil_selected: ArrayCollection = new ArrayCollection();
				
		//UI COMPONANT
		public var cbAll : CheckBox;
		public var cbMob : RadioButton;
		public var cbFix : RadioButton;
		public var vbReseau:VBox
		public var vbFixe:VBox
		public var vbMobile:VBox
				
		public function ParametreProfil()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			
			if(profil.idProfil>0)
			{
				initMode_updateProfil();
			}
			else
			{
				initMode_createProfil();
			}
		}
		private function initMode_createProfil():void
		{
			profilService.addEventListener(GestionProfilEvent.LISTE_ACTION_COMPLETE,listeActionComplete);
			profilService.ListeActionProfilCommande(profil.idProfil);
		}
		private function initMode_updateProfil():void
		{
//			registerClassAlias("univers.parametres.parametrecompte.vo.ProfilCommande",Profil);  
//			profil= ProfilCommande(ObjectUtil.copy(profil))  // delete les ref sur le grid
			initAttribut();
		}
		private function initAttribut():void
		{
			profilService.addEventListener(GestionProfilEvent.LISTE_ACTION_COMPLETE,listeActionComplete);
			profilService.ListeActionProfilCommande(profil.idProfil);
		}
		private function listeActionComplete (evt : GestionProfilEvent):void
		{
			if(profilService.col_actionOfProfil_fixe.length > 0)
				boolFixeVisible = true
			if(profilService.col_actionOfProfil_mobil.length > 0)
				boolMobileVisible = true
			if(profilService.col_actionOfProfil_Reseau.length > 0)
				boolReseauVisible = true
			countFixe()
			countMobile()
		}
		public function cbAllActionHandler(evt : Event):void
		{
			tab_actionOfProfil_selected = new ArrayCollection();
			
			var tmpArray : ArrayCollection = new ArrayCollection();
			for each (var item:Action in col_actionOfProfil.source){
				//On ne met ajour que les item affichés	
		
				item.autorise = cbAll.selected;
				if(item.autorise){
					tab_actionOfProfil_selected.addItem(item.idAction);
				}
				tmpArray.addItem(item.idAction);
				
			}
			if(cbAll.selected)
			{
				profilService.updateActionOfProfil(profil.idProfil,tmpArray.source,1);
			}
			else
			{
				profilService.updateActionOfProfil(profil.idProfil,tmpArray.source,0);
			}
			col_actionOfProfil.refresh();
		}
		public function onItemActionChanged(item : Object):void
		{
			var boolExiste : Boolean = false;
						
			for(var i:int=0;i<tab_actionOfProfil_selected.length;i++)
			{
				if(tab_actionOfProfil_selected[i]==item.idAction)
				{
					tab_actionOfProfil_selected.removeItemAt(i);
					
					profilService.updateActionOfProfil(profil.idProfil,[item.idAction],0);
					
					boolExiste = true;
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				tab_actionOfProfil_selected.addItem(item.idAction);
				
				profilService.updateActionOfProfil(profil.idProfil,[item.idAction],1);
			}
			tab_actionOfProfil_selected.refresh();
		}
		public function valider():void
		{
			profilService.addEventListener(GestionProfilEvent.INFO_PROFIL_COMPLETE,profil_info_change_handler);
			if(profil.idProfil>0)
			{
				//update
				profilService.updateInfoProfil(profil.idProfil,profil.libelle,profil.codeInterne);
			}
			else
			{
				//add
				profilService.createProfil(groupeIndex,profil.libelle,profil.codeInterne);
			}
		}
		private function profil_info_change_handler(evt : GestionProfilEvent):void
		{
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.PARAMETRE_PROFIL_COMPLETE));
			
			var msg : String;
			if(profil.idProfil>0)
			{
				msg = ResourceManager.getInstance().getString('M21', 'Les_modifications_ont_bien__t__enregistr');
				ConsoviewAlert.afficherOKImage(msg);
				PopUpManager.removePopUp(this);
			}
			else
			{
				profil.idProfil = evt.idProfil; // La vbox contenant le grid des attribut du profil est bindable avec cette propriété
				initAttribut(); // Rempli le grid
			}
		}
		public function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		public function filtre(evt : GestionProfilEvent = null):void
		{
			var str : String;
			var boolAlert : Boolean = false;
			if(cbFix.selected)
			{
				if(profilService.tab_actionOfProfil_mob_selected.length>0)
				{
					boolAlert = true;
				}
				str = ResourceManager.getInstance().getString('M21', 'Mobile');
			}
			else
			{
				if(profilService.tab_actionOfProfil_fixe_selected.length>0)
				{
					boolAlert = true;
				}
				str = ResourceManager.getInstance().getString('M21', 'Fixe___R_seau');
			}
			if(boolAlert)
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M21', '_tes_vous_s_r_de_vouloir_changer_l__tat_')+str+ResourceManager.getInstance().getString('M21', '_seront_perdues__'),ResourceManager.getInstance().getString('M21', 'Confirmation'),filtreHandler);
			}
			else
			{
				initData();
			}			
		}

		private function filtreHandler(e:CloseEvent):void{
			if(e.detail == Alert.OK)
			{
				initData(true);
			}
			else
			{
				if(cbFix)
				{
					cbMob.selected = true;
				}
				else
				{
					cbFix.selected = true;
				}
			}
		}
		
		private function initData(removeOldData : Boolean = false):void
		{
			
			cbAll.selected = false;
			if(cbFix.selected)
			{
				col_actionOfProfil = profilService.col_actionOfProfil_fixe;
				tab_actionOfProfil_selected = profilService.tab_actionOfProfil_fixe_selected;
				if(removeOldData){removeAllAction(profilService.col_actionOfProfil_mobil,profilService.tab_actionOfProfil_mob_selected)};
				
			}
			else
			{
				col_actionOfProfil = profilService.col_actionOfProfil_mobil;
				tab_actionOfProfil_selected = profilService.tab_actionOfProfil_mob_selected;
				if(removeOldData){removeAllAction(profilService.col_actionOfProfil_fixe,profilService.tab_actionOfProfil_fixe_selected)};
			}
		}
		private function removeAllAction(arr : ArrayCollection, tab : ArrayCollection):void
		{
			tab.removeAll() // vide les item selected
			profilService.updateActionOfProfil(profil.idProfil,ConsoviewUtil.extractCollectionIDs("idAction",arr),0); // enlever tous les droits
			
			for(var i:int=0;i<arr.length;i++) // maj des items du grid
			{
				arr.getItemAt(i).autorise = false;
			}
		}
		public function btValiderClickHandler():void
		{
			if(boolReadOnly)
			{
				PopUpManager.removePopUp(this)
			}
			else
			{
				var str:String = createListeIdAction()
				profilService.addEventListener(GestionProfilEvent.ENREGISTREMENT_PROFILCOMMANDE, EnregistrementProfilCommande_handler)
				if(profil.idProfil > 0)
					profilService.EnregistrerActionsProfilCommande(profil.idProfil,str,profil.libelle,profil.codeInterne);
				else
					profilService.CreateProfilCommande(profil.codeInterne,profil.libelle,"",str)
			}
		}
		private function EnregistrementProfilCommande_handler(e:GestionProfilEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Enregistrement_effectu__avec_succ_s'),this);
			PopUpManager.removePopUp(this)
		}
		private function createListeIdAction():String
		{
			var str:String = "";
			
			if(boolFixeVisible)
				str += 	createListeIdActionFixe()
			if(boolMobileVisible)
				str += 	createListeIdActionMobile()
			if(boolReseauVisible)
				str += 	createListeIdActionReseau()
			if(str.length > 0)
				str = str.substr(0,str.length-1)
			return str
		}
		private function createListeIdActionFixe():String
		{
			var str:String = ""
			
			for each(var cp:ChampsPersosVO in profilService.col_actionOfProfil_fixe)
			{
				if (cp.Selected)
					str += cp.CP_ID+","
			}
			return str
		}
		private function createListeIdActionMobile():String
		{
			var str:String = ""
			
			for each(var cp:ChampsPersosVO in profilService.col_actionOfProfil_mobil)
			{
				if (cp.Selected)
					str += cp.CP_ID+","
			}
			return str
		}
		private function createListeIdActionReseau():String
		{
			var str:String = ""
			
			for each(var cp:ChampsPersosVO in profilService.col_actionOfProfil_Reseau)
			{
				if (cp.Selected)
					str += cp.CP_ID+","
			}
			return str
		}
		public function CkxMobileChangeHandler(cp:ChampsPersosVO, Selected:Boolean):void
		{
			cp.Selected =Selected
			countMobile()
		}
		public function CkxFixeChangeHandler(cp:ChampsPersosVO, Selected:Boolean):void
		{
			cp.Selected = Selected
			countFixe()
		}
		public function ckxMobileAllChangeHandler():void
		{
			for each(var cp:ChampsPersosVO in profilService.col_actionOfProfil_mobil)
			{
				cp.Selected = ckxMobileAll.selected
			}
			profilService.col_actionOfProfil_mobil.refresh()
			countMobile()
		}
		public function ckxFixeAllChangeHandler():void
		{
			for each(var cp:ChampsPersosVO in profilService.col_actionOfProfil_fixe)
			{
				cp.Selected = ckxFixeAll.selected
			}
			profilService.col_actionOfProfil_fixe.refresh()
			countFixe()
		}
		private function countFixe():void
		{
			actionFixeCount = 0
			for each(var cp:ChampsPersosVO in profilService.col_actionOfProfil_fixe)
			{
				if(cp.Selected)
					actionFixeCount ++
			}
			if(actionFixeCount == profilService.col_actionOfProfil_fixe.length)
				ckxFixeAll.selected = true
			else
				ckxFixeAll.selected = false
		}
		private function countMobile():void
		{
			actionMobileCount = 0
			for each(var cp:ChampsPersosVO in profilService.col_actionOfProfil_mobil)
			{
				if(cp.Selected)
					actionMobileCount++
			}
			if(actionMobileCount == profilService.col_actionOfProfil_mobil.length)
				ckxMobileAll.selected = true
			else
				ckxMobileAll.selected = false
		}
	}
}