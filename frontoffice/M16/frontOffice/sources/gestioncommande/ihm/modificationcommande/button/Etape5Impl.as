package gestioncommande.ihm.modificationcommande.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpLibelleModeleIHM;
	import gestioncommande.services.Formator;
	
	import session.SessionUserObject;
	
	
	[Bindable]
	public class Etape5Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListEquipements	:DataGrid;
		public var dgListAboOptions		:DataGrid;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUplibelle		:PopUpLibelleModeleIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var _cmd					:Commande;
		
		private var _myElements			:ElementsCommande2 = new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listEquipements		:ArrayCollection = new ArrayCollection();
		public var listAboOptions		:ArrayCollection = new ArrayCollection();
		
		public var prixEquipements		:Number = 0;
		public var prixRessources		:Number = 0;
		public var prixAccessoires		:Number = 0;
		
		public var convertedToInt		:Boolean = true;
		public var isRenouvellement		:Boolean = false;
		
		
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE				:int = 4;
		public var ACCESS				:Boolean = true;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function Etape5Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		private function terminalPriceChangedHandler(e:Event):void
		{
			var price	:Object = e.target.data;
			var lenTer	:int = _myElements.TERMINAUX.length;
			
			for(var i:int = 0;i < lenTer;i++)
			{
				if( _myElements.TERMINAUX[i].IDEQUIPEMENT != 71 &&  _myElements.TERMINAUX[i].IDEQUIPEMENT != 2191)
				{
					_myElements.TERMINAUX[i].PRIX	 = price.PRIX;
					_myElements.TERMINAUX[i].PRIX_S	 = Formator.formatTotalWithSymbole(price.PRIX);
					_myElements.TERMINAUX[i].PRIX_C	 = price.PRIX_C;
					_myElements.TERMINAUX[i].PRIX_CS = Formator.formatTotalWithSymbole(price.PRIX_C);
					
					SessionUserObject.singletonSession.PRIXELIGIBLE 	= price.PRIX;
					SessionUserObject.singletonSession.PRIXNONELIGIBLE	= price.PRIX_C;
				}
			}
			
			compteTotal();
			
			dispatchEvent(new Event('PRICE_CHANGE', true));
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function creationCompleteHandler(fl:FlexEvent):void
		{
			dgListEquipements.addEventListener('TERMINAL_PRICE_CHANGED' , terminalPriceChangedHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function viewStackChange():void
		{
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			if(_cmd.IDTYPE_COMMANDE != 1382 && _cmd.IDTYPE_COMMANDE != 1587)
				isRenouvellement = false;
			else
				isRenouvellement = true;
			
			attributToEquipments();
			attributToAboOptions();
			compteTotal();
			
			callLater(refreshDatagrid);
		}

		public function checkData():Boolean
		{
			return true;
		}
		
		public function buildParameters():void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//

		protected function btnRecordModeleClickHandler(me:MouseEvent):void
		{
			_popUplibelle 			 = new PopUpLibelleModeleIHM();
			_popUplibelle.myCommande = _cmd;
			
			PopUpManager.addPopUp(_popUplibelle, this);
			PopUpManager.centerPopUp(_popUplibelle);
		}

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function attributToEquipments():void
		{			
			var lenTer	:int = _myElements.TERMINAUX.length;
			var lenAcc	:int = _myElements.ACCESSOIRES.length;
			var i		:int = 0;
			
			listEquipements = new ArrayCollection();
			prixAccessoires = 0;
			
			if(isRenouvellement)
			{
				for(i = 0;i < lenTer;i++)
				{
					if(_myElements.TERMINAUX[i].IDEQUIPEMENT > 0)
					{
						_myElements.TERMINAUX[i].PRIX_REN = Formator.formatTotalWithSymbole(SessionUserObject.singletonSession.PRIXELIGIBLE) + 
															'|' + 
															Formator.formatTotalWithSymbole(SessionUserObject.singletonSession.PRIXNONELIGIBLE);
						listEquipements.addItem(_myElements.TERMINAUX[i]);
					}
				}
				
				for(i = 0;i < lenAcc;i++)
				{
					_myElements.ACCESSOIRES[i].PRIX_REN = Formator.formatTotalWithSymbole(_myElements.ACCESSOIRES[i].PRIX);
					
					prixAccessoires = prixAccessoires + _myElements.ACCESSOIRES[i].PRIX;
					
					listEquipements.addItem(_myElements.ACCESSOIRES[i]);
				}
			}
			else
			{
				for(i = 0;i < lenTer;i++)
				{
					if(_myElements.TERMINAUX[i].IDEQUIPEMENT > 0)
						listEquipements.addItem(_myElements.TERMINAUX[i]);
				}
				
				for(i = 0;i < lenAcc;i++)
				{
					listEquipements.addItem(_myElements.ACCESSOIRES[i]);
				}
			}
			
			(dgListEquipements.dataProvider as ArrayCollection).refresh();
		}
		
		private function attributToAboOptions():void
		{	
			var lenAbo	:int = _myElements.ABONNEMENTS.length;
			var lenOpt	:int = _myElements.OPTIONS.length;
			var i		:int = 0;
			
			listAboOptions = new ArrayCollection();
			
			for(i= 0;i < lenAbo;i++)
			{
				listAboOptions.addItem(_myElements.ABONNEMENTS[i]);
			}
			
			for(i = 0;i < lenOpt;i++)
			{
				listAboOptions.addItem(_myElements.OPTIONS[i]);
			}
			
			(dgListAboOptions.dataProvider as ArrayCollection).refresh();
		}
		
		private function refreshDatagrid():void
		{
			dgListEquipements.dataProvider 	= listEquipements;
			dgListAboOptions.dataProvider 	= listAboOptions;
		}
		
		//---> COMPTE LE TOTAL DES CONFIGURATIONS QUE L'ON VIENT DE FAIRE
		private function compteTotal():void
		{	
			prixEquipements = 0;
			prixRessources  = 0;
			
			for(var i:int = 0;i < listEquipements.length;i++)
			{
				if(listEquipements[i].PRIX.toString() != "NaN" && listEquipements[i].PRIX != null)
					prixEquipements = prixEquipements + Number(listEquipements[i].PRIX);
			}
			
			for(var j:int = 0;j < listAboOptions.length;j++)
			{
				if(listAboOptions[j].PRIX_UNIT.toString() != "NaN" && listAboOptions[j].PRIX_UNIT != null)
					prixRessources = prixRessources + Number(listAboOptions[j].PRIX_UNIT);
			}
		}

	}
}