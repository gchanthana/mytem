<cfapplication name="APPLI1" 
				sessionmanagement="Yes" 
					sessiontimeout="#createTimeSpan(0,2,0,0)#" 
						setclientcookies="Yes">
						

<cffunction name="onApplicationStart">
	<cfargument name="ApplicationScope"/>
 </cffunction>

<cffunction name="onApplicationEnd">
	<cfargument name="ApplicationScope" required="true"/>
</cffunction>

<cffunction name="onSessionStart">
	<cfset SESSION.IS_CONNECTED=0> 
	<cfset SESSION.SessionID= -1>
	 
</cffunction>

<cffunction name="onRequestStart" access="remote" returntype="boolean">
	
   	<cfreturn TRUE>
</cffunction>

<cffunction name="onRequestEnd">
	<cfargument type="String" name="targetTemplate"required="true"/>
</cffunction>

<cffunction name="onSessionEnd">
	<cfargument name="SessionScope" required="true"/>
</cffunction>

