<!--- =========================================================================
Name: AbstractStrategy
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="AbstractStrategy" hint="This class declares an interface common to all supported algorithms. Context uses this interface to call the algorithm defined by a ConcreteStrategy.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="AbstractStrategy" displayname="AbstractStrategy init()" hint="Initialize the AbstractStrategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" displayname="getData()" >
		<cfabort showerror="This Method is Abstract and needs to be overridden">
	</cffunction>
	
	<cffunction name="getStrategy" returntype="string" access="public" output="false">
		<cfabort showerror="This Method is Abstract and needs to be overridden">
	</cffunction>
	
	<cffunction name="getNumero" returntype="string" access="public" output="false">
		<cfabort showerror="This Method is Abstract and needs to be overridden">
	</cffunction>
</cfcomponent>