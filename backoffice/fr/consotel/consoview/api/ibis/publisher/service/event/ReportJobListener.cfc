<cfcomponent name="ReportJobListener" displayname="fr.consotel.consoview.api.ibis.publisher.service.event.ReportJobListener">
	<cffunction name="eventHandler" returntype="void" access="remote" output="false" description="Handle HTTP notification events">
		<cfargument name="event" type="struct" required="true">
		<cfset var MAIL_SERVER="mail-cv@consotel.fr">
		<cfset var userId="NULL">
		<cfset var jobId="NULL">
		<cfset var remoteEvent=ARGUMENTS.event.URL>
		<cftry>
			<cfset eventArray=listToArray(remoteEvent["EVENT"],".")>
			<cfif arrayLen(eventArray) EQ 2>
				<cfset userId=eventArray[1]>
				<cfset jobId=eventArray[2]>
				<cfset bipService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
				<cfset scheduleReportInfosArray=bipService.getScheduledReportInfo(jobId,"consoview","public","All")>
				<cfset historyArray=bipService.getScheduledReportHistoryInfo(jobId,"consoview","public","All",FALSE)>
				<cfset nbReportParams=0>
				<cfif arrayLen(scheduleReportInfosArray) EQ 1>
					<cfset scheduleReportInfos=scheduleReportInfosArray[1]>
					<cfset nbReportParams=arrayLen(scheduleReportInfos.getReportParameters())>
				</cfif>
				<cfmail from="sun-dev-cedric@consotel.fr" to="monitoring@saaswedo.com" subject="BI Publisher - CFMX EVENT HANDLER NOTIFICATION" 
					type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
					<cfdump var="#ARGUMENTS.event#" expand="true" label="EVENT"><br />
					<cfoutput>
						UserId : #userId#<br />
						JobId : #jobId#<br />
					</cfoutput>
					<cfdump var="#historyArray#" label="HISTORY ARRAY">
					<cfdump var="#scheduleReportInfos#" label="SCHEDULE REPORT INFOS"><br />
				</cfmail>
			</cfif>
			<cfcatch type="any">
				<cflog text="====== REPORT JOB LISTENER CFC - EXCEPTION DURING EVENT HANDLING - #CGI.REMOTE_HOST# ======">
				<cfset MAIL_SERVER="mail-cv@consotel.fr">
				<cfmail from="sun-dev-cedric@consotel.fr" to="monitoring@saaswedo.com" subject="#CGI.SERVER_NAME# - EXCEPTION DURING EVENT HANDLING" 
					type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
					<cfdump var="#ARGUMENTS.event#" label="EVENT">
					<cfoutput>
						HTTP SERVER : #CGI.HTTP_HOST#<br />
						BACKOFFICE : #CGI.SERVER_NAME#
					</cfoutput>
					<cfdump var="#CFCATCH#" expand="true" label="EXCEPTION">
				</cfmail>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>