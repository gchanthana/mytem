<cfcomponent displayname="fr.consotel.consoview.api.ibis.ReportingService" extends="fr.consotel.consoview.api.ibis.AbstractReportingService"
		hint="Reporting Service Implementation">
	<cffunction access="private" name="demanderRapport" returntype="any">
		<cfargument name="parameters" type="struct" required="true">
		<cfset var paramKeys=structKeyList(arguments["parameters"]["reportParameters"])>
		<cfset var oldApi=createObject("component","fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService")>
		<cflog type="information" text="ReportingService [demanderRapport]">
		<cfset initStatus=oldApi.init(arguments["parameters"]["idRacine"],arguments["parameters"]["xdoAbsolutePath"],
						arguments["parameters"]["appName"],arguments["parameters"]["moduleName"],arguments["parameters"]["codeRapport"])>
		<cfset setStatus=TRUE>
		<cfloop index="key" list="#paramKeys#">
			<cfset setStatus=setStatus AND oldApi.setParameter(key,arguments["parameters"]["reportParameters"][key]["parameterValues"],
				arguments["parameters"]["reportParameters"][key]["parameterType"],arguments["parameters"]["reportParameters"][key]["parameterLabel"])>
		</cfloop>
		<cfset outputStatus=oldApi.setOutput(arguments["parameters"]["xdoTemplateId"],arguments["parameters"]["outputFormat"],
			arguments["parameters"]["outputExtension"],arguments["parameters"]["outputName"],arguments["parameters"]["codeRapport"])>
		<cfreturn oldApi.runTask(arguments["parameters"]["userIdCv"],arguments["parameters"]["idGestionnaire"])>
	</cffunction>
	
	<cffunction access="private" name="scheduleReport" returntype="any">
		<cfargument name="parameters" type="struct" required="true">
		<cfset var scheduleRequest=createScheduleRequest(arguments["parameters"])>
		<cfset var bipWebService=createObject("webservice",VARIABLES["BIP-WSDL"])>
		<cfset var paramKeyList=structKeyList(arguments["parameters"]["reportParameters"])>
		<cfset var xdoAbsolutePath=arguments["parameters"]["xdoAbsolutePath"]>
		<cflog type="information" text="ReportingService - [scheduleReport] BIP-WSDL : #VARIABLES["BIP-WSDL"]#">
		<cflog type="information" text="ReportingService - [scheduleReport] report : #xdoAbsolutePath#">
		<cflog type="information" text="ReportingService - [scheduleReport] method with parameters :">
		<cfloop index="paramKey" list="#paramKeyList#">
			<cfset parameterValues=arguments["parameters"]["reportParameters"][paramKey]["parameterValues"]>
			<cflog type="information" text=">> #paramKey# : [#arrayToList(parameterValues)#]">
		</cfloop>
		<cfset addJobEventListener(scheduleRequest,"EVENT-HANDLER")>
		<cfreturn bipWebService.scheduleReport(scheduleRequest,VARIABLES["DEFAULT-BIP-USER"],VARIABLES["DEFAULT-BIP-PWD"])>
	</cffunction>
</cfcomponent>
