package service.diffusion.listeorgaclient
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeOrgaClientHandler
	{
		private var _model:ListeOrgaClientModel;
		
		public function ListeOrgaClientHandler(_model:ListeOrgaClientModel)
		{
			this._model=_model;
		}
		
		internal function getListeOragClientHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.listeOrgaClient(evt.result as ArrayCollection);
			}
		}
	}
}