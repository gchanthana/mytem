package gestionparc.ihm.fiches.listedispo
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.PieceJointeIHM;
	
	import gestionparc.entity.FileUpload;
	import gestionparc.event.ImportDeMasseEvent;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.equipement.EquipementServices;
	import gestionparc.services.importmasse.ImportMasseServices;
	import gestionparc.services.ligne.LigneServices;
	import gestionparc.services.liste.ListeService;

	[Bindable]
	public class DispoSimMobile extends DispoSimMobileIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/SIM50x50.png")]
		private var adrImg:Class;
		public var avecLigne:int=1;
		public var avecTerm:int=0;
		public var avecCollab:int=0;
		public var avecAddElement:int = 0;
		private var serv:ListeService=new ListeService();
		public var _popUpUpload:PieceJointeIHM;
		public var enteredSIM			:String = '';
		public var enteredFileUpload    :FileUpload = null;
		private var _isValidSim			:Boolean = false;
		private var _ajoutSimHorsParc	:Boolean = false;
		private var _isFileUploadPresent	:Boolean = false;
		
		[Embed(source="/assets/images/warningSaisie.png")]
		public var imgSaisieWarning:Class;
		[Embed(source="/assets/images/okSaisie.png")]
		public var imgSaisieOk:Class;

		public const IDTYPEEQPCARTESIM  :Number=71;
		
		private var myServicesImport			:ImportMasseServices;
		public var myServiceTerminal			:EquipementServices;
		private var ligneService				:LigneServices;

		public function DispoSimMobile(boolVisibleAvecLigne:int=1, boolVisibleAvecTerm:int=0, boolVisibleAvecCollab:int=0, boolVisibleAddElement:int = 0)
		{
			this.avecLigne=boolVisibleAvecLigne;
			this.avecTerm=boolVisibleAvecTerm;
			this.avecCollab=boolVisibleAvecCollab;
			this.avecAddElement = boolVisibleAddElement;
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			initListeners();
			initData();
			ajoutSimHorsParc = false;
			txtFiltreMySim.setFocus();
			rdWithSIM.visible = rdWithSIM.includeInLayout = (avecLigne == 0) ? false : true;
			rdWithSIM.selected=(avecLigne == 0) ? false : true;
			rdNoneSIM.selected=(avecLigne == 0) ? true : false;
			columnIMEI.visible=(this.avecTerm != 0) ? true : false;
			columnLibelleIMEI.visible=(this.avecTerm != 0) ? true : false;
			btAddSimElement.visible = (this.avecAddElement == 1)? true : false;
			containerInitial.visible = containerInitial.includeInLayout = true;
			containerAddElement.visible = containerAddElement.includeInLayout = false;
			
		}
		
		public function initListeners():void
		{
			txtFiltreMySim.addEventListener(Event.CHANGE, txtFiltreChangeHandler);
			dgListeMySIM.addEventListener(MouseEvent.CLICK, dgVisibleChanged);
			rdWithSIM.addEventListener(Event.CHANGE, rdWithSIMChangeHandler);
			rdNoneSIM.addEventListener(Event.CHANGE, rdNoneSIMChangeHandler);
			btAddSimElement.addEventListener(MouseEvent.CLICK, addElementHandler);
		}
		
		private function tiNumSimHandler(te:Event):void
		{
			enteredSIM = ti_SIM.text;
			displayImgEtat(-1);
			isValidSIM = false;
			if((enteredSIM.length > 12) && (enteredSIM.length < 21))
			{
				ti_SIM.errorString = '';
				myServicesImport.checkNSIM(ti_SIM.text);
				myServicesImport.myDatas.addEventListener(ImportDeMasseEvent.CHECKED_NUMSIM,checkNSIMHandler);
			}
			else
			{
				ti_SIM.errorString = ResourceManager.getInstance().getString('M111','Numero_SIM_entre_13_et_20_caract_res_');
			}
		}
		
		/* retour des verifs - Handler */
		public function checkNSIMHandler(impe:ImportDeMasseEvent):void
		{
			myServicesImport.myDatas.removeEventListener(ImportDeMasseEvent.CHECKED_NUMSIM,checkNSIMHandler);
			displayImgEtat(impe.objRet as int);
		}

		/**
		 * 	1 : nouveau SIM, 
		 *	2 : très grand
		 *	3 : Existe déjà
		 *	-1: erreur inconnue
		 **/
		private function displayImgEtat(etat:int):void
		{
			switch(etat)
			{
				case 1:
					img.source = imgSaisieOk;
					img.buttonMode = false;
					img.visible = true;
					img.includeInLayout = true;
					isValidSIM = true;
					ti_SIM.errorString = '';
					img.toolTip = ResourceManager.getInstance().getString('M111','Ce numéro de Sim est valide');
					break;
				case 2:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					ti_SIM.errorString = ResourceManager.getInstance().getString('M111','Numero_SIM_entre_13_et_20_caract_res_');
					img.toolTip = ResourceManager.getInstance().getString('M111','Numero_SIM_entre_13_et_20_caract_res_');
					break;
				case 3:
					img.source = imgSaisieWarning;
					img.buttonMode = true;
					img.visible = true;
					img.includeInLayout = true;
					ti_SIM.errorString = ResourceManager.getInstance().getString('M111','Ce_num_ro_de_sim_existe_d_j___');
					img.toolTip = ResourceManager.getInstance().getString('M111','Ce_num_ro_de_sim_existe_d_j___');
					break;
				case -1:
					img.source = imgSaisieWarning;
					img.buttonMode = false;
					img.visible = true;
					img.includeInLayout = true;
					ti_SIM.errorString = ResourceManager.getInstance().getString('M111','Une_erreur_est_survenue');
					img.toolTip = ResourceManager.getInstance().getString('M111','Une_erreur_est_survenue');
					break;
				default:
					img.source = imgSaisieWarning;
					img.buttonMode = false;
					img.visible = true;
					img.includeInLayout = true;
					ti_SIM.errorString = ResourceManager.getInstance().getString('M111','Une_erreur_est_survenue');
					img.toolTip = ResourceManager.getInstance().getString('M111','Une_erreur_est_survenue');
					break;
			}
		}
		
		private function undisplayIMG():void
		{
			img.includeInLayout = false;
			img.visible = false;
		}
		
		public function clickUploadHandler(evt:MouseEvent):void
		{
			_popUpUpload = new PieceJointeIHM(); 
			_popUpUpload.addEventListener('POPUP_CLOSED', popUpClosedHandler);
			_popUpUpload.addEventListener('POPUP_CLOSED_UPLOADED', popUpClosedHandler);
			PopUpManager.addPopUp(_popUpUpload,parent, true);
			PopUpManager.centerPopUp(_popUpUpload);
		}
		
		private function popUpClosedHandler(e:Event):void
		{
			if (e.type == 'POPUP_CLOSED_UPLOADED')
			{
				enteredFileUpload = _popUpUpload.fileUploaded;
				isFileUploadPresent = _popUpUpload.isFileUploadPresent;
				btn_upload.visible = btn_upload.includeInLayout = false;
				lbl_uplod.text =   ResourceManager.getInstance().getString('M111','Piece_s__Jointe_s__')+ ': '+ _popUpUpload.fileUploaded.fileName;
			}
		}
		
		public function addElementHandler(evt:MouseEvent):void
		{
			ajoutSimHorsParc = true;
			myServicesImport = new ImportMasseServices();
			myServiceTerminal = new EquipementServices();
			undisplayIMG();
			containerInitial.visible = containerInitial.includeInLayout = false;
			containerAddElement.visible = containerAddElement.includeInLayout = true;
			
			btn_upload.addEventListener(MouseEvent.CLICK, clickUploadHandler);
			ti_SIM.addEventListener(Event.CHANGE,tiNumSimHandler);
			cbOperateur.addEventListener(ListEvent.CHANGE, cbMarque_changeHandler);
			
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_MODELES_LOADED, modeleLoadedHandler);
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_MARQUES_LOADED, listeMarquesLoadedHandler);
			getListeMarque();
		}
		
		private function modeleLoadedHandler(evt:gestionparcEvent):void
		{
			/*cbModele.dataProvider=myServiceTerminal.myDatas.listeModele;
			cbModele.dropdown.dataProvider=myServiceTerminal.myDatas.listeModele;
			
			if (cbModele.dataProvider.length == 1)
			{
				cbModele.selectedIndex=0;
			}
			*/
			myServiceTerminal.myDatas.listeModele;
			
		}
		
		private function listeMarquesLoadedHandler(evt:gestionparcEvent):void
		{
			cbOperateur.dataProvider=myServiceTerminal.myDatas.listeMarque;
			cbOperateur.dropdown.dataProvider=myServiceTerminal.myDatas.listeMarque;
			
			if (cbOperateur.dataProvider.length == 1)
			{
				cbOperateur.selectedIndex=0;
				cbMarque_changeHandler(null);
			}
		}
		
		private function dgVisibleChanged(ev:Event):void
		{
			if (dgListeMySIM.selectedIndex > -1)
				dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		private function dgListeOtherSIMHandler(evt:Event):void
		{
			dgListeMySIM.dataProvider=serv.myDatas.dataListeligne; // Declenche un evt donc actualise le champs libelleSelection Dan PanelAffectation.as
		}

		public function getSelection():Object
		{
			var obj:Object=new Object();
			if((this.ajoutSimHorsParc == true) && (this.isValidSIM == true))
			{
				obj.NUM_SIM = this.enteredSIM;
				obj.ID = 0;
			}
			else if ((this.ajoutSimHorsParc == false) && (dgListeMySIM.selectedItem != null))
			{
				obj=dgListeMySIM.selectedItem;
			}
			return obj;
		}

		public function getLibelleSelection():String
		{
			if((this.ajoutSimHorsParc == true) && (this.isValidSIM == true))
			{
					return this.enteredSIM;
			}
			else if ((this.ajoutSimHorsParc == false) && (dgListeMySIM.selectedIndex > -1))
			{
				if (dgListeMySIM.selectedItem.IMEI != null)
				{
					return dgListeMySIM.selectedItem.IMEI;
				}
				else
				{
					return dgListeMySIM.selectedItem.NUM_SIM;
				}
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		public function getDataGrid():DataGrid
		{
			return dgListeMySIM;
		}

		public function getIDSelection():int
		{
			if (dgListeMySIM.selectedIndex != -1)
				return dgListeMySIM.selectedItem.IDSIM;
			else
				return -1;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_cartes_SIM_disponibles');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		private function initData():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_SIMSSTERM, updateDGssTerm)
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_SIMSSLIGNE, updateDGssLigne)

			if (!avecLigne)
				serv.ListeSimSsLigne(avecTerm, avecCollab);
			else
				serv.ListeSimAvecLigne(avecTerm, avecCollab);
		}

		private function updateDGssTerm(ev:gestionparcEvent):void
		{
			dgListeMySIM.dataProvider=serv.myDatas.dataListeSIMssTerm;
			serv.myDatas.dataListeSIMssTerm.refresh();
		}

		private function updateDGssLigne(ev:gestionparcEvent):void
		{
			dgListeMySIM.dataProvider=serv.myDatas.dataListeSIMssLigne;
			serv.myDatas.dataListeSIMssLigne.refresh();
		}

		protected function rdWithSIMChangeHandler(e:Event):void
		{
			avecLigne=1;
			txtFiltreMySim.setFocus();
			initData();
			resetSelection()
		}

		protected function rdNoneSIMChangeHandler(e:Event):void
		{
			avecLigne=0;
			txtFiltreMySim.setFocus();
			initData();
			resetSelection()
		}

		private function resetSelection():void
		{
			dgListeMySIM.selectedIndex=-1;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		private function txtFiltreChangeHandler(ev:Event):void
		{
			try
			{
				dgListeMySIM.dataProvider.filterFunction=filtrerLeGrid;
				dgListeMySIM.dataProvider.refresh();
			}
			catch (e:Error)
			{
			}
		}

		private function filtrerLeGrid(item:Object):Boolean
		{
			if ((String(item.NUM_SIM).toLowerCase().search(txtFiltreMySim.text.toLowerCase()) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltreMySim.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltreMySim.text.toLowerCase()) != -1) || (String(item.REVENDEUR).toLowerCase().search(txtFiltreMySim.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
		protected function cbMarque_changeHandler(le:ListEvent):void
		{
			refreshListe();
		}
		
		// RAFRAICHIR LES LISTES
		private function refreshListe():void
		{
				if (cbOperateur.selectedIndex > -1)
				{
					getListeModele();
				}
				else
				{
					getListeMarque();
				}
		}
		
		private function getListeModele():void
		{
			var idmarque:int=cbOperateur.selectedItem.IDFOURNISSEUR;
			myServiceTerminal.getListeModele(IDTYPEEQPCARTESIM, idmarque);
		}
		
		private function getListeMarque():void
		{
			myServiceTerminal.getListeMarqueWithType(IDTYPEEQPCARTESIM);
		}
		
		public function get isValidSIM():Boolean { return _isValidSim; }
		
		public function set isValidSIM(value:Boolean):void
		{
			if (_isValidSim == value)
				return;
			_isValidSim = value;
		}
		
		public function get ajoutSimHorsParc():Boolean
		{
			return _ajoutSimHorsParc;
		}
		
		public function set ajoutSimHorsParc(value:Boolean):void
		{
			_ajoutSimHorsParc = value;
		}
		
		public function get isFileUploadPresent():Boolean
		{
			return _isFileUploadPresent;
		}
		
		public function set isFileUploadPresent(value:Boolean):void
		{
			_isFileUploadPresent = value;
		}
	}
}
