package gestionparc.ihm.main.menu.itemmenurenderer
{
	import gestionparc.ihm.action.associer.ActionAssocierEqpToExt.ActionAssocierEqpToExtCommand;
	import gestionparc.ihm.action.associer.ActionAssocierEqpToExt.ActionAssocierEqpToExtReceiver;
	import gestionparc.ihm.action.associer.actionAssocierCollabToExt.ActionAssocierCollabToExtCommand;
	import gestionparc.ihm.action.associer.actionAssocierCollabToExt.ActionAssocierCollabToExtReceiver;
	import gestionparc.ihm.action.associer.actionAssocierLigneExtToExt.ActionAssocierLigneExtToExtCommand;
	import gestionparc.ihm.action.associer.actionAssocierLigneExtToExt.ActionAssocierLigneExtToExtReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabfromExt.ActionDissocierCollabFromExtCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierCollabfromExt.ActionDissocierCollabFromExtReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierEqpFromExt.ActionDissocierEqpFromExtCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierEqpFromExt.ActionDissocierEqpFromExtReceiver;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneExtFromExt.ActionDissocierLigneExtFromExtCommand;
	import gestionparc.ihm.action.dissocier.actionDissocierLigneExtFromExt.ActionDissocierLigneExtFromExtReceiver;
	import gestionparc.ihm.action.extension.actionextechanger.ActionExtEchangerCommand;
	import gestionparc.ihm.action.extension.actionextechanger.ActionExtEchangerReceiver;
	import gestionparc.ihm.action.extension.actionextvoirhistorique.ActionExtVoirHistoriqueCommand;
	import gestionparc.ihm.action.extension.actionextvoirhistorique.ActionExtVoirHistoriqueReceiver;
	import gestionparc.ihm.main.menu.ItemMenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierCollabForExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierEqpForExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer.RuleAssocierLigneForExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierCollabForExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierEqpForExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.dissocier.RuleDissocierLigneForExt;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.extension.RuleExtGeneric;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.menu.RuleRubriqueMenu;
	
	import mx.resources.ResourceManager;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.extension.RuleExtensionEchanger;

	public class ItemMenuContextualOfExt extends AbstractItemMenuContextual
	{

		[Embed(source='/assets/images/coll+.png')]
		private var myIconGiveColl:Class;

		[Embed(source='/assets/images/coll-.png')]
		private var myIconGetColl:Class;

		[Embed(source='/assets/images/give_terminal.png')]
		private var myIconGiveTerm:Class;

		[Embed(source='/assets/images/get_terminal.png')]
		private var myIconGetTerm:Class;

		[Embed(source='/assets/images/Newfixe+.png')]
		private var myIconNewFixeP:Class;

		[Embed(source='/assets/images/Newfixe-.png')]
		private var myIconNewFixeM:Class;

		[Embed(source='/assets/images/echange.png')]
		private var myIconActionEchanger:Class;

		[Embed(source='/assets/images/terminal_sav.png')]
		private var myIconActionSAV:Class;

		[Embed(source='/assets/images/action_supprimer.png')]
		private var myIconActionAuRebut:Class;

		[Embed(source='/assets/images/voir.png')]
		private var myIconVoir:Class;

		[Embed(source='/assets/images/historique.png')]
		private var myIconHistorique:Class;

		/**
		 * Cette fonction créer la liste exaustive des items du menu contextuel d'un extention.
		 */
		override protected function fillArrayItemMenuContextual():Array
		{
			var res:Array=new Array();

			/**  rubrique - Associer */ 
			var rule1:RuleRubriqueMenu=new RuleRubriqueMenu();
			var obj1:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'ASSOCIER'), false, null, null, rule1);
			res.push(obj1);

			/** Associer extention à un collaborateur */
			var recAssocCollabExt:ActionAssocierCollabToExtReceiver=new ActionAssocierCollabToExtReceiver();
			var cmdAssocCollabExt:ActionAssocierCollabToExtCommand=new ActionAssocierCollabToExtCommand(recAssocCollabExt);
			var rule2:RuleAssocierCollabForExt=new RuleAssocierCollabForExt();
			var obj2:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'a_un_collaborateur'), true, myIconGiveColl, cmdAssocCollabExt, rule2);
			res.push(obj2);
			
			/** Associer extention à un terminal ( equipmenet )  */
			var recAssocEqpExt:ActionAssocierEqpToExtReceiver=new ActionAssocierEqpToExtReceiver();
			var cmdAssocEqpExt:ActionAssocierEqpToExtCommand=new ActionAssocierEqpToExtCommand(recAssocEqpExt);
			var rule3:RuleAssocierEqpForExt=new RuleAssocierEqpForExt();		
			var obj3:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'avec_un__quipement'), true, myIconGiveTerm, cmdAssocEqpExt, rule3);
			res.push(obj3);
			
			/** Associer extention à une ligne */
			var recAssocLigExt:ActionAssocierLigneExtToExtReceiver=new ActionAssocierLigneExtToExtReceiver();
			var cmdAssocLigSim:ActionAssocierLigneExtToExtCommand=new ActionAssocierLigneExtToExtCommand(recAssocLigExt);
			var rule4:RuleAssocierLigneForExt=new RuleAssocierLigneForExt();
			var obj4:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', '__une_ligne'), true, myIconGiveTerm, cmdAssocLigSim, rule4);
			res.push(obj4);
			
			/**  rubrique - dissocier */
			var rule5:RuleRubriqueMenu=new RuleRubriqueMenu();
			var obj5:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'DISSOCIER'), false, null, null, rule5);
			res.push(obj5);
			
			/**  dissocier extention du collaborateur */ 
			var recDisoCollabExt:ActionDissocierCollabFromExtReceiver=new ActionDissocierCollabFromExtReceiver();
			var cmdDisoCollabExt:ActionDissocierCollabFromExtCommand=new ActionDissocierCollabFromExtCommand(recDisoCollabExt);
			var rule6:RuleDissocierCollabForExt=new RuleDissocierCollabForExt();
			var obj6:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'du_collaborateur'), true, myIconGetColl, cmdDisoCollabExt, rule6);
			res.push(obj6);
			
			/** Dissocier une extention de l'équipement */
			var recDisocEqpExt:ActionDissocierEqpFromExtReceiver=new ActionDissocierEqpFromExtReceiver();
			var cmdDisocEqpExt:ActionDissocierEqpFromExtCommand=new ActionDissocierEqpFromExtCommand(recDisocEqpExt);
			var rule7:RuleDissocierEqpForExt=new RuleDissocierEqpForExt();
			var obj7:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'de_l__quipement'), true, myIconGetTerm, cmdDisocEqpExt, rule7);
			res.push(obj7);
			
			/** Dissocier une extention de la ligne */
			var recDisocLigExt:ActionDissocierLigneExtFromExtReceiver=new ActionDissocierLigneExtFromExtReceiver();
			var cmdDisocLigExt:ActionDissocierLigneExtFromExtCommand=new ActionDissocierLigneExtFromExtCommand(recDisocLigExt);
			var rule8:RuleDissocierLigneForExt=new RuleDissocierLigneForExt();
			var obj8:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'de_la_ligne'), true, myIconGetTerm, cmdDisocLigExt, rule8);
			res.push(obj8);	
			
			/** rubrique - Extention */
			var rule9:RuleRubriqueMenu=new RuleRubriqueMenu();
			var obj9:ItemMenuContextual=new ItemMenuContextual("Extention", false, null, null, rule9);
			res.push(obj9);
			
			/** Echanger */ 
			var recEchanger:ActionExtEchangerReceiver=new ActionExtEchangerReceiver();
			var cmdEchanger:ActionExtEchangerCommand=new ActionExtEchangerCommand(recEchanger);
			var rule10:RuleExtensionEchanger=new RuleExtensionEchanger();
			var obj10:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Echange'), true, myIconActionEchanger, cmdEchanger, rule10);
			res.push(obj10);
						
			/** voir historique */ 
			var recVoirHistoExt:ActionExtVoirHistoriqueReceiver=new ActionExtVoirHistoriqueReceiver();
			var cmdVoirHistoExt:ActionExtVoirHistoriqueCommand=new ActionExtVoirHistoriqueCommand(recVoirHistoExt);
			var obj11:ItemMenuContextual=new ItemMenuContextual(ResourceManager.getInstance().getString('M111', 'Voir_historique'), true, myIconHistorique, cmdVoirHistoExt, rule1);
			res.push(obj11);
		
			return res;
		}
	}
}