<cfcomponent output="false">

	<cffunction name="SendMailSupp" access="remote" returntype="numeric">
		<cfargument name="idgroupe" required="true" type="Numeric">
		<cfargument name="groupeproduit" required="true" type="String">	
			<cfset adressMail = "olivier.boulard@consotel.fr">
	      	<cfset rsltMail = 0>
				<cftry>
	            	<cfmail from="olivier.boulard@consotel.fr" to="#adressMail#" subject="Votre mot depasse ConsoTel" server="mail.consotel.fr:26">
Une demande de suppression de groupe de rationalisation a ete effectue. Elle concerne :
	- le produit "#groupeproduit#"
	- l'id produit "#idgroupe#"
					
Ne pas répondre à  ce mail, s'il vous plait !
	                </cfmail>
	            	<cfset rsltMail = 1>
	          	<cfcatch>
	                  <cfset rsltMail = 0>
	            </cfcatch>
	            </cftry>
	      <cfreturn rsltMail>
	</cffunction>


	
	<cffunction  name="getListeOperateur" returntype="query" access="remote">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.disp_operateur">
			<cfprocresult name="qGetOpe">
		</cfstoredproc>
		
		<cfreturn qGetOpe>
	</cffunction>

	<cffunction  name="getListeTheme" returntype="query" access="remote">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.disp_abo_conso">
			<cfprocresult name="qGetTheme">
		</cfstoredproc>
		
		<cfreturn qGetTheme>
	</cffunction>
		
	<cffunction name="getListeGroupe" returntype="query" access="remote">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.liste_groupe">
			<cfprocresult name="qGetListeGroupe">
		</cfstoredproc>
		
		<cfreturn qGetListeGroupe>
	</cffunction>
	
		
		
	<cffunction name="getProduitNonAffecte" returntype="query" access="remote">
		<cfargument name="OpeId" type="Numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.Disp_noaffect_produit">
			<cfprocparam value="#OpeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetPNA">
		</cfstoredproc>
		
		<cfreturn qGetPNA>
	</cffunction>

	<cffunction name="getAccesProduit" returntype="query" access="remote">
		<cfargument name="OpeId" type="Numeric" required="true">
		<cfargument name="p_type_acces" type="Numeric" required="false" default="0">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.disp_acces_produit">
			<cfprocparam value="#OpeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#p_type_acces#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetAccesProduit">
		</cfstoredproc>
		
		<cfreturn qGetAccesProduit>
	</cffunction>
		
	<cffunction name="getGroupeProduit" returntype="query" access="remote">
		<cfargument name="OpeId" type="Numeric" required="false">
		<cfargument name="ThemeProduitId" type="numeric" required="false">
		<cfargument name="GroupId" type="numeric" required="true">
		<cfargument name="pdispnoaffect" type="numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.disp_groupe_produit">
			<cfprocparam value="#OpeId#" cfsqltype="cF_SQL_INTEGER" dbvarname="operateurid">
			<cfprocparam value="#ThemeProduitId#" cfsqltype="CF_SQL_INTEGER" dbvarname="idtheme_produit">
			<cfprocparam value="#GroupId#" cfsqltype="CF_SQL_INTEGER" dbvarname="idgroupe_produit">
			<cfprocparam value="#pdispnoaffect#" cfsqltype="cF_SQL_INTEGER" dbvarname="p_disp_noaffect">
			<cfprocresult name="qGetGroupeProduit">
		</cfstoredproc>
		
		<cfreturn qGetGroupeProduit>
	</cffunction>
	
	<cffunction name="getThemesProduits" returntype="query" access="remote">
		<cfargument name="ThemeProduitId" type="Numeric" required="true">
		<cfargument name="OperateurId" type="Numeric" required="false" default="0">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.disp_theme_produit_v2">
	        <cfprocparam value="#ThemeProduitId#" cfsqltype="CF_SQL_INTEGER" dbvarname="idtheme_produit">
	        <cfprocparam value="#OperateurId#" cfsqltype="CF_SQL_INTEGER" dbvarname="operateurid">
	        <cfprocresult name="qGetCF">
	    </cfstoredproc>
	    
		<cfreturn qGetCF>
	</cffunction>

 	<cffunction name="SearchProduit" returntype="query" access="remote">
		<cfargument name="libelle_produit" type="String" required="true">
		<cfargument name="operateurid" type="Numeric" required="false">
		<cfargument name="themeid" type="Numeric" required="false">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.disp_recherche_produit">
			<cfprocparam value="#libelle_produit#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#operateurid#" cfsqltype="CF_SQL_NUMERIC" type="in">
			<cfprocparam value="#themeid#" cfsqltype="CF_SQL_NUMERIC" type="in">
			<cfprocresult name="qSearch">
		</cfstoredproc>
		
		<cfreturn qSearch>
	</cffunction> 

 	<cffunction name="displayProduit" returntype="query" access="remote">
		<cfargument name="idproduit" type="Numeric" required="true">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.disp_client">
			<cfprocparam value="#idproduit#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qSearch">
		</cfstoredproc>
		
		<cfreturn qSearch>
	</cffunction> 
 

	
	<cffunction name="updAcces" returntype="Numeric" access="remote">
		<cfargument name="Tab" type="Array" required="true">
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.update_acces">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['bool_acces'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['tout_acces'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam variable="updAcces" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
		</cfloop>		
		<cfreturn updAcces>
	</cffunction>
	
	<cffunction name="updTheme" returntype="Numeric" access="remote">
		<cfargument name="Tab" type="Array" required="true">





		
		<cfset var _nbUpTheme = 0>
		<cfset var _nbUpDrt = 0>
		<cfset var _len = ArrayLen(Tab)>
		
		<cfloop index="i" from="1" to=#_len#>		
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.Update_theme">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['idtheme_produit'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam variable="updTheme" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			


			<cfif updTheme gt 0>
				<cfset _nbUpTheme = _nbUpTheme + 1>
			</cfif>
			
			<cftry>
			
			<cfset _bool_drt = iif(val("#Tab[i]['bool_drt']#") gt -2,false,true)>
			<cfset _plan_type = iif(val("#Tab[i]['plan_type']#") gt -2,false,true)>
			<cfset _plan_mcap = iif(val("#Tab[i]['plan_mcap']#") gt -2,false,true)>
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.update_product_plan">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['bool_drt'])#" cfsqltype="CF_SQL_INTEGER" null="#_bool_drt#">
				<cfprocparam value="#val(Tab[i]['plan_type'])#" cfsqltype="CF_SQL_INTEGER" null="#_plan_type#">
				<cfprocparam value="#val(Tab[i]['plan_mcap'])#" cfsqltype="CF_SQL_INTEGER" null="#_plan_mcap#">
				<cfprocparam variable="p_retour1" cfsqltype="CF_SQL_INTEGER" type="out"> <!--- IDPRODUIT_CATALOGUE --->
				<cfprocparam variable="p_retour2" cfsqltype="CF_SQL_INTEGER" type="out"><!--- 1 ou -1 --->
				<cfprocparam variable="p_retour3" cfsqltype="CF_SQL_CLOB" type="out"><!--- message  succes  / L exception levé --->
			</cfstoredproc>

			
			<cfcatch type="any">
				<cfset p_retour2 = -1>
			</cfcatch>
						
			</cftry>

			<cfif p_retour2 gt 0>
				<cfset _nbUpDrt = _nbUpDrt + 1>
			</cfif>
		</cfloop> 

	 
		<cfreturn i-1>

	</cffunction>

	<cffunction name="updPNA" returntype="Numeric" access="remote">
		<cfargument name="Tab" type="Array" required="true">
		<cfloop index="i" from="1" to=#ArrayLen(Tab)#>		
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.Update_theme">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['idtheme_produit'])#" cfsqltype="CF_SQL_INTEGER">
		
				<cfprocparam variable="updTheme" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.Update_acces">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['bool_acces'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['tout_acces'])#" cfsqltype="CF_SQL_INTEGER">
				
				<cfprocparam variable="updAcces" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
		</cfloop> 


	<cfif updAcces eq 1 && updTheme eq 1>
		<cfreturn 1>
	<cfelse>
		<cfreturn -1>
	</cfif>
	
	</cffunction>
	
	<cffunction name="setMultiProduitsNonAffectes" returntype="Numeric" access="remote">
		<cfargument name="listeProduits" 	type="Array" 	required="true">
		<cfargument name="idthemeproduit" 	type="Numeric" 	required="true">
		<cfargument name="boolAcces" 		type="Numeric" 	required="true">
		<cfargument name="boolToutAcces" 	type="Numeric" 	required="true">		
		
			<cfset var resultUpdate	= ArrayNew(1)>
			<cfset var lenProduits	= ArrayLen(listeProduits)>
			<cfset var resultReturn	= -1>
		
			<cfloop index="i" from="1" to="#lenProduits#">		
				
				<cfset currentIdProduitCatalogue = listeProduits[i]['IDPRODUIT_CATALOGUE']>

				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_prod_global.update_theme">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#currentIdProduitCatalogue#" >
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idthemeproduit#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retourTheme" >
				</cfstoredproc>
				
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_prod_global.update_acces">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#currentIdProduitCatalogue#">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#boolAcces#">
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#boolToutAcces#">
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retourAcces">
				</cfstoredproc>
				
				
				<cfif p_retourTheme EQ 1 && p_retourAcces EQ 1>
				
					<cfset ArrayAppend(resultUpdate, currentIdProduitCatalogue)>
				
				</cfif>

			</cfloop> 

			<cfif ArrayLen(resultUpdate) EQ lenProduits>				
				<cfset resultReturn	= 1>			
			<cfelse>				
				<cfset resultReturn	= -1>			
			</cfif>
	
		<cfreturn resultReturn>
	</cffunction>
	
	
	<cffunction name="upd_PNA_DRT" returntype="array" access="remote">
		<cfargument name="Tab" type="Array" required="true">
		
		<cfset var _tabUpdate = []>
		<cfset var _len = ArrayLen(Tab)>
		<cfset var _struResult = {}>
		
		<cfloop index="i" from="1" to=#_len#>		
			
			<cfset _struResult.idproduit = val(Tab[i]['idproduit_catalogue'])>
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.Update_theme">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['idtheme_produit'])#" cfsqltype="CF_SQL_INTEGER">		
				<cfprocparam variable="updTheme" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
			
			<cfset _struResult.updTheme =  updTheme>
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.Update_acces">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['bool_acces'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['tout_acces'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam variable="updAcces" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>			
			
			<cfset _struResult.updAcces =  updAcces>
			
			<cfset _bool_drt = iif(val("#Tab[i]['bool_drt']#") gt -2,false,true)>
			<cfset _plan_type = iif(val("#Tab[i]['plan_type']#") gt -2,false,true)>
			<cfset _plan_mcap = iif(val("#Tab[i]['plan_mcap']#") gt -2,false,true)>
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.update_product_plan">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['bool_drt'])#" cfsqltype="CF_SQL_INTEGER" null="#_bool_drt#">
				<cfprocparam value="#val(Tab[i]['plan_type'])#" cfsqltype="CF_SQL_INTEGER" null="#_plan_type#">
				<cfprocparam value="#val(Tab[i]['plan_mcap'])#" cfsqltype="CF_SQL_INTEGER" null="#_plan_mcap#">
				<cfprocparam variable="p_retour1" cfsqltype="CF_SQL_INTEGER" type="out"> <!--- IDPRODUIT_CATALOGUE --->
				<cfprocparam variable="p_retour2" cfsqltype="CF_SQL_INTEGER" type="out"><!--- 1 ou -1 --->
				<cfprocparam variable="p_retour3" cfsqltype="CF_SQL_CLOB" type="out"><!--- message  succes  / L exception levé --->
			</cfstoredproc>
			
			<cfset _struResult.updPlan =  {}>
			<cfset _struResult.updPlan.idproduit = p_retour1>
			<cfset _struResult.updPlan.message = p_retour3>
			<cfset _struResult.updPlan.result = p_retour2>
			
			<cfset ArrayAppend(_tabUpdate,_struResult)>			
			
		</cfloop> 


	
		<cfreturn _tabUpdate>
	 
	
	</cffunction>
	
	<cffunction name="updGroupe" returntype="numeric" access="remote">
		<cfargument name="Tab" type="array" required="true">
		<cfloop index="i" from="1" to=#ArrayLen(tab)#>
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.update_groupe">
				<cfprocparam value="#val(Tab[i]['idproduit_catalogue'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#val(Tab[i]['idgroupe_produit'])#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam variable="updGroupe" cfsqltype="CF_SQL_INTEGER" type="out">
			</cfstoredproc>
		</cfloop>
		<cfreturn updGroupe>
	</cffunction>

	<cffunction name="majGroupe" returntype="Numeric" access="remote">
		<cfargument name="libelle_groupe_produit" type="String" required="true">
		<cfargument name="idgroupe_produit" type="Numeric" required="false" default="-1">
		<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_PROD_GLOBAL.update_groupe_produit">
			<cfprocparam value="#libelle_groupe_produit#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#idgroupe_produit#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="updgroupeproduit" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn updgroupeproduit>
	</cffunction>
	
</cfcomponent>





