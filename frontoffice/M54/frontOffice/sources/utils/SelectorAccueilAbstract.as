package utils
{
	import mx.containers.Canvas;
	
	import utils.serialization.json.JSON;
	
	import vo.selector.Parametre;
	import vo.selector.SelectorParametreVo;

	[Bindable]
	public class SelectorAccueilAbstract extends Canvas
	{
		public var typeSelecteur : String;
		public var typeParametre :Number;
		public var defaultValue: String;
		public var idKpiOrWidget :Number;
		public var widgetParametreid :Number;
		public var isKpi :Boolean=true;
		public var value :String;
		
		public var CleAndLibelleParametreComposant :String;
		public var arrayCleLibelle:Object;
		
		public function SelectorAccueilAbstract(selector:Object=null)
		{
		}

		public function getSelectedValue():Array{return null}
		
		public function setSelector(selector:SelectorParametreVo):void
		{
		}
		
		public function init(selector :Object):void
		{
			 this.typeSelecteur=selector.typeSelecteur;
			 this.typeParametre=selector.typeParametre;
			 this.defaultValue=selector.defaultValue;
			 this.widgetParametreid=selector.widgetParam_id;
			 this.CleAndLibelleParametreComposant=selector.cleJson;
			 arrayCleLibelle=(JSON.decode(CleAndLibelleParametreComposant)); // au format json
		}
		
		public function prepareCleJson():Array
		{
			var tabValeur:Array=[];			
			var param:Parametre=new Parametre();
			
			if(arrayCleLibelle[0].VALUE !=null)
			{
				param.value=[arrayCleLibelle[0].VALUE];
			}
			else
			{
				param.value=[defaultValue];
			}
			param.cle=arrayCleLibelle[0].NOM_CLE;
			param.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			tabValeur[0]=param;
			return tabValeur;
		}
	}
}