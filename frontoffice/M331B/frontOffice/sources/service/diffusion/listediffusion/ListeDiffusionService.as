package service.diffusion.listediffusion
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class ListeDiffusionService
	{
		public var _model:ListeDiffusionModel;
		public var handler:ListeDiffusionHandler;	
		
		public function ListeDiffusionService()
		{
			this._model  = new ListeDiffusionModel();
			this.handler = new ListeDiffusionHandler(model);
		}
		public function get model():ListeDiffusionModel
		{
			return this._model;
		}	
		public function listeRapportsEnDiffusion(): void
		{
			var callOp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ListeRapportEnDiffusionService","listeRapportsEnDiffusion",handler.getlisteRapportsEnDiffusionHandler); 
			
			RemoteObjectUtil.callService(callOp);	
		}
	}
}