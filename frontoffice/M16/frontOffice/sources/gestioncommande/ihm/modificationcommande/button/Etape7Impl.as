package gestioncommande.ihm.modificationcommande.button
{//MODIFIER COMMANDE
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Label;
	import mx.controls.List;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import commandemobile.ihm.mail.MailCOmmandeIHM;
	
	import gestioncommande.custom.CustomRadioButton;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Etape7Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var mailCommande				:MailCOmmandeIHM;
		
		public var listConfig				:List;
		
		public var vbxAction				:VBox;
		
//		public var hbxRecapitulatif			:Box;
		
		public var txaCommentaires			:TextArea;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		public var selectedAction			:Action;
		
		private var _cmd					:Commande;
		private var _myElements				:ElementsCommande2 = new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _workFlow				:WorkflowService 	= new WorkflowService();
		private var _engagement				:EquipementService 	= new EquipementService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var myItems					:ArrayCollection = new ArrayCollection();
		public var listActions				:ArrayCollection = new ArrayCollection();
		
		public var isRenouvellement			:Boolean = false;
		
		public var prixAccessoires			:Number = 0;
		
		private var _idRandom				:int = 0;
		
		private const IDETAT				:int = 3066;
		
		//--------------------------------------------------------------------------------------------//
		//					COCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE					:int = 5;
		public var ACCESS					:Boolean = true;
		
		private var _libelle				:String  = ResourceManager.getInstance().getString('M16','_Validation');
		private var _libelleButton			:String  = "";
		private var _visibleButton			:Boolean = true;
		private var _idButton				:int	 = 7;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function Etape7Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);			
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function creationCompleteHandler(fl:FlexEvent):void
		{
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}
		
		public function viewStackChange():void
		{
			var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var len			:int = elements.length;
			
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			_cmd		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			if(vbxAction.numChildren == 0)
				fournirListeActionsPossibles();
			
			setDataToGrid();
		}
		
		public function checkData():Boolean
		{
			return true;
		}
		
		public function buildParameters():void
		{
		}

		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		public var lblRecapitulatif			:Label;
		public var lblEditerMail			:Label;
		
		public var isActionSelected			:Boolean = false;
		public var isViewMail				:Boolean = false;
		
		protected function lblEditerMailClickHandler(me:MouseEvent):void
		{
			if(!lblEditerMail.enabled) return;
			
			isViewMail = true;
			
			lblRecapitulatif.setStyle('textDecoration', 'underline');
			lblRecapitulatif.useHandCursor 	= true;
			lblRecapitulatif.buttonMode 	= true;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'none');
			lblEditerMail.useHandCursor = false;
			lblEditerMail.buttonMode 	= false;
			lblEditerMail.validateNow();
		}
		
		protected function lblRecapitulatifClickHandler(me:MouseEvent):void
		{
			isViewMail = false;
			
			lblRecapitulatif.setStyle('textDecoration', 'none');
			lblRecapitulatif.useHandCursor 	= false;
			lblRecapitulatif.buttonMode 	= false;
			lblRecapitulatif.validateNow();
			
			lblEditerMail.setStyle('textDecoration', 'underline');
			lblEditerMail.useHandCursor = true;
			lblEditerMail.buttonMode 	= true;
			lblEditerMail.validateNow();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function fournirListeActionsPossibles():void
		{
			_workFlow.fournirActionsNoRemote(3066, _cmd, SessionUserObject.singletonSession.IDSEGMENT);
			
			addRecordOnly();
			
			if(_workFlow.listeActions.length > 0)
			{
				for(var i:int = 0;i < _workFlow.listeActions.length;i++)
				{
					if(_workFlow.listeActions[i].CODE_ACTION != 'ANNUL' && _workFlow.listeActions[i].CODE_ACTION != null && _workFlow.listeActions[i].CODE_ACTION != "")
					{
						var cstmRdbtn:CustomRadioButton = new CustomRadioButton();
						cstmRdbtn.ACTION			= _workFlow.listeActions[i] as Action;
						cstmRdbtn.addEventListener('RADIOBUTTON_CLICKHANDLER', cstmRdbtnClickHandler);
						
						vbxAction.addChild(cstmRdbtn);
					}
				}
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function cstmRdbtnClickHandler(e:Event):void
		{
			var myChidldren	:Array = vbxAction.getChildren();
			var lenChildren :int = vbxAction.numChildren;
			
			selectedAction = (e.target as CustomRadioButton).ACTION
			
			for(var i:int = 0; i < lenChildren;i++)
			{
				if(myChidldren[i] as CustomRadioButton)
				{
					if((myChidldren[i] as CustomRadioButton).ACTION.IDACTION != selectedAction.IDACTION)
						(myChidldren[i] as CustomRadioButton).selected = false;
				}
			}
			
			if(selectedAction.IDACTION > 0)
			{				
				isActionSelected = true;
				
				lblEditerMail.enabled = true;
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'underline');
				lblEditerMail.useHandCursor = true;
				lblEditerMail.buttonMode 	= true;
				lblEditerMail.validateNow();
			}
			else
			{
				isActionSelected = false;
				
				lblEditerMail.enabled = false;
				
				lblRecapitulatifClickHandler(new MouseEvent(MouseEvent.CLICK));
				
				lblRecapitulatif.setStyle('textDecoration', 'none');
				lblRecapitulatif.useHandCursor 	= false;
				lblRecapitulatif.buttonMode 	= false;
				lblRecapitulatif.validateNow();
				
				lblEditerMail.setStyle('textDecoration', 'none');
				lblEditerMail.useHandCursor = false;
				lblEditerMail.buttonMode 	= false;
				lblEditerMail.validateNow();
			}
			
			this.validateNow();
			
			mailCommande.setAction(selectedAction);
			
			dispatchEvent(new Event('VALIDER_ENABLED', true));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - RECUPERATION DES ELEMENTS ET SET TO LIST
		//--------------------------------------------------------------------------------------------//
		
		private function setDataToGrid():void
		{
			var elements	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var len			:int = elements.length;
			
			myItems.removeAll();
			
			for(var i:int = 0;i < len;i++)
			{
				var elts:Object 	= new Object();
					elts			= addEquipements(elements[i] as ElementsCommande2);
					elts.LIBELLE 	= ResourceManager.getInstance().getString('M16', 'Configuration_') + (i+1).toString();
				
				myItems.addItem(elts);
			}
		}
		
		private function addEquipements(value:ElementsCommande2):Object
		{
			var equi	:ArrayCollection = new ArrayCollection();
			var elts	:Object = new Object();
			var lenTer	:int = value.TERMINAUX.length;
			var lenAcc	:int = value.ACCESSOIRES.length;
			var i		:int = 0;
			var qte		:int = value.CONFIGURATIONS.length;
			var prix	:Number = 0;
			var nbrLnes :int = SessionUserObject.singletonSession.NBRLIGNEELIGIBLE + SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE;
			
			if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length > 0)
				nbrLnes = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			
			if(_cmd.IDTYPE_COMMANDE != 1382 && _cmd.IDTYPE_COMMANDE != 1587)
				isRenouvellement = false;
			else
				isRenouvellement = true;
			
			for(i = 0;i < lenTer;i++)//TERMINAUX
			{
				if(value.TERMINAUX[i].IDEQUIPEMENT > 0)
				{
					elts 			= new Object();
					elts.EQUIPEMENT = value.TERMINAUX[i].EQUIPEMENT;
					elts.LIBELLE 	= value.TERMINAUX[i].LIBELLE;
					elts.REFERENCE_PRODUIT = value.TERMINAUX[i].REF_REVENDEUR;
					elts.QUANTITE 	= qte;
					elts.MENSUEL	= "";
					elts.NBRLIGNES	= nbrLnes;
					
					if(_cmd.IDTYPE_COMMANDE != 1382 && _cmd.IDTYPE_COMMANDE != 1587)
						elts.PRIX = Formator.formatTotalWithSymbole(value.TERMINAUX[i].PRIX);
					else
					{
						if(SessionUserObject.singletonSession.NBRLIGNEELIGIBLE > 0 && SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE > 0)
						{
							elts.PRIX = Formator.formatTotalWithSymbole(SessionUserObject.singletonSession.PRIXELIGIBLE) + 
										'|' + 
										Formator.formatTotalWithSymbole(SessionUserObject.singletonSession.PRIXNONELIGIBLE);
						}
						else if(SessionUserObject.singletonSession.NBRLIGNEELIGIBLE > 0)
							{
								elts.PRIX = Formator.formatTotalWithSymbole(SessionUserObject.singletonSession.PRIXELIGIBLE);
							}
							else if(SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE > 0)
								{
									elts.PRIX = Formator.formatTotalWithSymbole(SessionUserObject.singletonSession.PRIXNONELIGIBLE);
								}
						
					}					
					
					prix  = prix + value.TERMINAUX[i].PRIX;
					
					equi.addItem(elts);
				}
			}
			
			prixAccessoires = 0;
			
			for(i = 0;i < lenAcc;i++)//ACCESSOIRES
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.ACCESSOIRES[i].EQUIPEMENT;
				elts.LIBELLE 	= value.ACCESSOIRES[i].LIBELLE;
				elts.REFERENCE_PRODUIT = value.ACCESSOIRES[i].REF_REVENDEUR;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= "";
				elts.NBRLIGNES	= nbrLnes;
				elts.PRIX		= Formator.formatTotalWithSymbole(value.ACCESSOIRES[i].PRIX);
				
				prix  = prix + value.ACCESSOIRES[i].PRIX;
				
				prixAccessoires = prixAccessoires + value.ACCESSOIRES[i].PRIX;
				
				equi.addItem(elts);
			}
			
			var prixRessources:Number = addRessources(value, equi);
			
			prix = qte *(prix + prixRessources);
			
			var obj:Object 		= new Object();
				obj.ITEMS 		= equi;
				obj.TOTONECONF	= Formator.formatTotalWithSymbole(prix /nbrLnes);
				obj.TOTAL		= Formator.formatTotalWithSymbole(prix);
				obj.RANDOM		= value.RANDOM;
				obj.RNVLT		= isRenouvellement;
				obj.PRIXACC		= prixAccessoires;
				obj.NBRLIGNES	= nbrLnes;

			return obj;
		}
		
		private function addRessources(value:ElementsCommande2, ress:ArrayCollection):Number
		{
			var elts	:Object = new Object();
			var lenAbo	:int = value.ABONNEMENTS.length;
			var lenOpt	:int = value.OPTIONS.length;
			var i		:int = 0;
			var qte		:int = value.CONFIGURATIONS.length;
			var prix	:Number = 0;
			
			for(i = 0;i < lenAbo;i++)//ABONNEMENT
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.ABONNEMENTS[i].LIBELLETHEME;
				elts.LIBELLE 	= value.ABONNEMENTS[i].LIBELLE;
				elts.REFERENCE_PRODUIT = value.ABONNEMENTS[i].REFERENCE_PRODUIT;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= value.ABONNEMENTS[i].PRIX_STRG;
				elts.PRIX		= "";
				
				prix  = prix + value.ABONNEMENTS[i].PRIX_UNIT;
				
				ress.addItem(elts);
			}
			
			for(i = 0;i < lenOpt;i++)//OPTIONS
			{
				elts 			= new Object();
				elts.EQUIPEMENT = value.OPTIONS[i].LIBELLETHEME;
				elts.LIBELLE 	= value.OPTIONS[i].LIBELLE;
				elts.REFERENCE_PRODUIT = value.OPTIONS[i].REFERENCE_PRODUIT;
				elts.QUANTITE 	= qte;
				elts.MENSUEL	= value.OPTIONS[i].PRIX_STRG;
				elts.PRIX		= "";
				
				prix  = prix + value.OPTIONS[i].PRIX_UNIT;
				
				ress.addItem(elts);
			}
			
			return prix;
		}
		
		private function isRandomContains(values:ArrayCollection, randObject:ElementsCommande2):Boolean
		{
			return values.contains(randObject);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - MISE EN FORME DES RADIOBUTTON
		//--------------------------------------------------------------------------------------------//
		
		private function addRecordOnly():void
		{
			var cstmRdbtn:CustomRadioButton 		= new CustomRadioButton();
				cstmRdbtn.ACTION					= new Action();
				cstmRdbtn.ACTION.LIBELLE_ACTION		= ResourceManager.getInstance().getString('M16','Enregistrer_la_commande_sans_l_envoyer');
				cstmRdbtn.ACTION.IDACTION			= -1;
				cstmRdbtn.addEventListener('RADIOBUTTON_CLICKHANDLER', cstmRdbtnClickHandler);
			
			vbxAction.addChild(cstmRdbtn);
		}

	}
}