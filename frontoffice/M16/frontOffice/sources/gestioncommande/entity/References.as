package gestioncommande.entity
{
	import commandemobile.utils.VerificationImport;
	
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;

	[Bindable]
	public class References
	{
		public var OLD_IDSOUSTETE			:int = 0;
		public var IDSOUSTETE				:int = 0;
		public var IDEQUMODILE				:int = 0;
		public var IDEQUSIM					:int = 0;
		public var IDCOLLAB					:int = 0;
		public var IDARTICLE				:int = 0;
		public var IDSEGEMENT				:int = 1;

		public var TRAITE					:int = 0;
		
		public var VERIFICATION				:int = 1; //---> 1-> VALIDE, 2-> VERIFICATION EN COURS, 3-> ERREUR
		
		public var CHAR_MAX					:int = 20;
		
		public var SOUSTETE_IS_ENBASE		:Boolean = false;
		public var SOUSTETE_IS_DOUBLON		:Boolean = false;
		
		public var IMEI_IS_ENBASE			:Boolean = false;
		public var IMEI_IS_DOUBLON			:Boolean = false;
		
		public var SIM_IS_ENBASE			:Boolean = false;
		public var SIM_IS_DOUBLON			:Boolean = false;
		
		private var _SOUSTETE					:String = '';
		private var _NUM_IMEI					:String = '';
		private var _NUM_SIM					:String = '';
		
		public var SOUSTETE_INIT			:String = '';
		public var NUM_IMEI_INIT			:String = '';
		public var NUM_SIM_INIT				:String = '';
		
		/*
			// ETAT_IMEI = 1 OUx 2 OUx 3 OUx 4 OUx 51 ( OUx ~~ ou exclusif)  
			// 1-> VALIDE (imei  ni ds base ni ds en double ds la grille), 
			// 2-> ERREUR (Doublon ou Dans-la-base)
			// 3-> VERIFICATION_EN_COURS, ou EN_ATTENTE de valeur								
			// 4-> le champ saisie est vide ( == '')
			// 51-> Commande de Type Fixe
		*/
		public var ETAT_IMEI				:Number = 1; 
		public var ETAT_SOUSTETE			:Number = 1;
		public var ETAT_SIM					:Number = 1;
		public var ETAT_PUK					:Number = 1;
			
		public var OLD_SOUSTETE				:String = '';
		public var OLD_IMEI					:String = '';
		public var OLD_SIM					:String = '';
		
		public var LIBELLE_EQU				:String = '';
		public var LIBELLE_EMP				:String = '';
		public var LIBELLE_CMD				:String = '';
		
		public var CHAR_RESTRICT			:String = '0-9';
		
		public var CODE_PIN					:String = '';
		private var _CODE_PUK					:String = '';
		
		public var SOUSTETE_LIBELLE_ERROR	:String = '';
		public var IMEI_LIBELLE_ERROR		:String = '';
		public var SIM_LIBELLE_ERROR		:String = '';
		
		public var IS_LIV_PART				:Boolean = false;
		public var LIV_PART_ACTIF			:Boolean = false;
		
		public var IS_ALLOCATE				:Boolean = false;
		public var IS_RESTORE				:Boolean = false;
		public var IS_SCRAP					:Boolean = false;
		
		public var ISVALIDE					:Boolean = true;

		public var MYCOMMANDE				:Commande = new Commande();
		
		public var ACTION_SELECTED			:Action = new Action();
		
		public var LIGNESUTILS				:LignesUtils = new LignesUtils();
		
		public var verifImport				:VerificationImport = new VerificationImport();
		
		private var _arrayReferences		:ArrayCollection;
		
		// ajouter pour produit SPIE
		public var idProduit				: int = 0;
		public var libelleProduit 			:String = '';
		public var libelleTypeCommande		:String = '';
		public var boolAcces				:int;
		
		
		public function References(arrayRef:ArrayCollection)
		{
			var date:Date = new Date();
			
			ACTION_SELECTED.DEST				= "C";
			ACTION_SELECTED.DATE_ACTION			= date;
			ACTION_SELECTED.IDACTION 			= 2073;
			ACTION_SELECTED.CODE_ACTION			= "EXPED";
			ACTION_SELECTED.EXP					= "R";
			ACTION_SELECTED.DATE_HOURS 			= Formator.formatReverseDate(date) + " " + Formator.formatHourConcat(date);
			ACTION_SELECTED.COMMENTAIRE_ACTION 	= ResourceManager.getInstance().getString('M16','Expedier_la_commande');
			ACTION_SELECTED.LIBELLE_ACTION 		= ResourceManager.getInstance().getString('M16','Expedier_la_commande');
			ACTION_SELECTED.MESSAGE				= "";
			_arrayReferences 							= arrayRef;
		}
		
		public function get CODE_PUK():String
		{
			return _CODE_PUK;
		}

		public function set CODE_PUK(value:String):void
		{
			if(NUM_SIM == ''){
				this.ETAT_PUK = 4;
			}else{
				_CODE_PUK = value;
			}
		}

		public function get NUM_SIM():String
		{
			return _NUM_SIM;
		}

		public function set NUM_SIM(value:String):void
		{
			if(value == null){
				this.ETAT_SIM = 4;
				_NUM_SIM = '';
			}
			else if((_NUM_SIM != value) && (value != ''))
			{
				_NUM_SIM = value;				
				verifImport.verifierSimDoublon(this);
				
				if(this.SIM_IS_DOUBLON == false)
				{
					if(NUM_SIM_INIT != value)	
						verifImport.verifierSimEnBase(this);
				}
			}
			else// dans le cas ou Num_SIM == '' (si le text entré est vide)
			{
				this.ETAT_SIM = 3;
				_NUM_SIM = '';
				this.SIM_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16', 'Valeur_invalide');
			}
		}

		public function get SOUSTETE():String
		{
			return _SOUSTETE;
		}

		public function set SOUSTETE(value:String):void
		{
			
			if(this.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.NVL_LIGNE_FIXE)
			{
				if((_SOUSTETE != value) && (value != ''))
				{
					_SOUSTETE = value;		
					verifImport.verifierLigneDoublon(this);
					
					if(this.SOUSTETE_IS_DOUBLON == false)
					{
						if(SOUSTETE_INIT != value)	
							verifImport.verifierLigneEnBase(this);
					}
				}
				else // value = ''
				{
					this.ETAT_SOUSTETE = 3;
					_SOUSTETE = '';
					this.SOUSTETE_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16', 'Valeur_invalide');
				}
				
				
			}
			else// tous les autres types de commandes
			{
				if(value == null){
					this.ETAT_SOUSTETE = 4;
					_SOUSTETE = '';
				}
				else if((_SOUSTETE != value) && (value != ''))
				{
					_SOUSTETE = value;		
					verifImport.verifierLigneDoublon(this);
					
					if(this.SOUSTETE_IS_DOUBLON == false)
					{
						if(SOUSTETE_INIT != value)	
							verifImport.verifierLigneEnBase(this);
					}
				}
				else // value = ''
				{
					this.ETAT_SOUSTETE = 3;
					_SOUSTETE = '';
					this.SOUSTETE_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16', 'Valeur_invalide');
				}
			}
		}
		
		public function get NUM_IMEI():String
		{
			return _NUM_IMEI;
		}

		public function set NUM_IMEI(value:String):void
		{
			if(this.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.EQU_NUS_FIXE)
			{
				if((_NUM_IMEI != value) && (value != ''))
				{
					_NUM_IMEI = value;				
					verifImport.verifierIMEIDoublon(this);
					if(this.IMEI_IS_DOUBLON == false)
					{
						if(NUM_IMEI_INIT != value)
						verifImport.verifierImeiEnBase(this);
					}
				} 
				else// value == ''
				{
					this.ETAT_IMEI = 3;
					_NUM_IMEI = '';
					this.IMEI_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16', 'Valeur_invalide');
				}
			}
			else // tous les autres types commandes
			{
				if(value == null){
					this.ETAT_IMEI = 4;
					_NUM_IMEI = '';
				}
				else if((_NUM_IMEI != value) && (value != ''))
				{
					_NUM_IMEI = value;				
					verifImport.verifierIMEIDoublon(this);
					if(this.IMEI_IS_DOUBLON == false)
					{
						if(NUM_IMEI_INIT != value)
							verifImport.verifierImeiEnBase(this);
					}
				}
				else// value == ''
				{
					this.ETAT_IMEI = 3;
					_NUM_IMEI = '';
					this.IMEI_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16', 'Valeur_invalide');
				}
			}
		}
		
		public function get arrayReferences():ArrayCollection
		{
			return _arrayReferences;
		}
		
		public static function mappingReferences(value:Object, currentCommande:Commande, arrayRef:ArrayCollection):References
		{
			var myRefInfos:References 		= new References(arrayRef);
			
				myRefInfos.MYCOMMANDE		= currentCommande.copyCommande();
				myRefInfos.IDSEGEMENT 		= getSegment(value.SEGMENT_MOBILE, value.SEGMENT_FIXE, value.SEGMENT_DATA);
			
				myRefInfos.NUM_SIM_INIT		= value.S_IMEI;	
				myRefInfos.OLD_SIM			= value.S_IMEI;
				myRefInfos.NUM_SIM 			= value.S_IMEI;
					
				
				myRefInfos.IDCOLLAB 		= value.IDEMPLOYE;
				myRefInfos.LIBELLE_EMP 		= value.NOM;
				myRefInfos.LIBELLE_CMD 		= value.CODE_INTERNE;
				myRefInfos.LIBELLE_EQU 		= value.LIBELLE_EQ;
				myRefInfos.IDEQUMODILE 		= value.IDTERMINAL;
				
				myRefInfos.NUM_IMEI_INIT	= value.IMEI;
				myRefInfos.OLD_IMEI			= value.IMEI;
				myRefInfos.NUM_IMEI 		= value.IMEI;
				
				myRefInfos.IDEQUSIM 		= value.IDSIM;
				
				myRefInfos.IDSOUSTETE 		= value.IDSOUS_TETE;
				myRefInfos.TRAITE 			= value.FLAG_TRAITE;
				myRefInfos.CODE_PIN 		= value.S_PIN1;
				myRefInfos.IDARTICLE 		= value.IDARTICLE;
				myRefInfos.CODE_PUK 		= value.S_PUK1;
				
				myRefInfos.SOUSTETE_INIT	= value.SOUS_TETE;
				myRefInfos.OLD_SOUSTETE		= value.SOUS_TETE;
				myRefInfos.SOUSTETE			= value.SOUS_TETE;
				
				myRefInfos.idProduit			= value.ID_PRODUIT;
				myRefInfos.libelleProduit		= value.LIBELLE_PRODUIT;
				myRefInfos.libelleTypeCommande 	= value.LIBELLE_TYPE_CMD;
				myRefInfos.boolAcces			= value.BOOL_ACCES;
				
				
				if(currentCommande.IDTYPE_COMMANDE == TypesCommandesMobile.RENOUVELLEMENT && myRefInfos.IDEQUSIM == 0)
					myRefInfos.IDEQUSIM = -1;
				
				if(myRefInfos.IDSEGEMENT > 1)
				{
					myRefInfos.CHAR_MAX 	 = 20;
					myRefInfos.CHAR_RESTRICT = '0-9a-zA-Z';
				}
			
			return myRefInfos;
		}
		
		public static function mappingArrayCollection(values:ArrayCollection, currentCommande:Commande):ArrayCollection
		{
			var refInfosList	:ArrayCollection = new ArrayCollection();
			var len				:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				refInfosList.addItem(mappingReferences(values[i], currentCommande, refInfosList));
			}
			
			return refInfosList;
		}
		
		public static function mappingArrayCollectionProduitSpie(values:ArrayCollection, currentCommande:Commande):ArrayCollection
		{
			var refInfosList	:ArrayCollection = new ArrayCollection();
			var len				:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].BOOL_ACCES == 1)
				{
					refInfosList.addItem(mappingReferences(values[i], currentCommande, refInfosList));
					break;
				}
			}
			
			return refInfosList;
		}
		
		private static function getSegment(mobile:int, fixe:int, data:int):int
		{
			var segment:int = 1;
			
			if(mobile == 1 && fixe == 0 && data == 0)
			{
				segment = 1;
			}
			else if(mobile == 0 && fixe == 1 && data == 0)
				{
					segment = 2;
				}
				else if(mobile == 0 && fixe == 0 && data == 1)
					{
						segment = 3;
					}
			
			return segment;
		}
	}
}