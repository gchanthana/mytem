<cfcomponent author="Cedric" extends="fr.saaswedo.api.mdm.impl.Commons"
implements="fr.saaswedo.api.mdm.MdmServer" hint="Implémentation des infos serveur MDM utilisant le parc local">
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.mdm.MdmServer" hint="Constructeur que doivent avoir toutes les implémentations">
		<cfargument name="idGroup" type="Numeric" required="true" hint="Identifiant du groupe pour identifier le Parc et les infos serveur MDM">
		<cfset SUPER.init()>
		<cfset VARIABLES.idGroup=ARGUMENTS.idGroup>
		<!--- Récupération et mise à jour des infos serveur MDM --->
		<cfset var serverInfos=requestServerInfos()>
		<cfset structAppend(VARIABLES,serverInfos,true)>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getIdGroup" returntype="Numeric" hint="Retourne l'identifiant du groupe ou -1 s'il n'est pas défini">
		<cfreturn structKeyExists(VARIABLES,"idGroup") ? VAL(VARIABLES.idGroup):-1>
	</cffunction>
	<cffunction access="public" name="getVendor" returntype="String" hint="Retourne un code identifiant le vendor ou une chaine vide sinon">
		<cfreturn structKeyExists(VARIABLES,"vendor") ? VARIABLES.vendor:"">
	</cffunction>
	
	<cffunction access="public" name="getAdminEmail" returntype="String"
	hint="Retourne l'adresse mail d'administrateur qui sera notifié par mail en cas d'erreur ou une chaine vide sinon">
		<cfreturn structKeyExists(VARIABLES,"adminEmail") ? VARIABLES.adminEmail:"">
	</cffunction>

	<cffunction access="public" name="endpointURL" returntype="String">
		<cfreturn (useSSL() EQ TRUE ? "https":"http") & "://" & endpoint()>
	</cffunction>

	<cffunction access="public" name="endpoint" returntype="String" hint="URI complète vers l'API du vendor ou une chaine vide sinon">
		<cfreturn structKeyExists(VARIABLES,"endpointURI") ? VARIABLES.endpointURI:"">
	</cffunction>

	<cffunction access="public" name="useSSL" returntype="Boolean" hint="Retourne TRUE si l'accès à l'API du vendor se fait en HTTPS (Par défaut: TRUE)">
		<cfreturn structKeyExists(VARIABLES,"isSSL") ? yesNoFormat(VARIABLES.isSSL):TRUE>
	</cffunction>
	
	<cffunction access="public" name="getUsername" returntype="String"
	hint="Retourne le login utilisateur pour l'accès à l'API du vendor ou une chaine vide sinon">
		<cfreturn structKeyExists(VARIABLES,"ws_username") ? VARIABLES.ws_username:"">
	</cffunction>
	
	<cffunction access="public" name="getPassword" returntype="String"
	hint="Retourne le password utilisateur pour l'accès à l'API du vendor ou une chaine vide sinon">
		<cfreturn structKeyExists(VARIABLES,"ws_password") ? VARIABLES.ws_password:"">
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentUsername" returntype="String"
	hint="Retourne le login utilisateur pour l'enrollment ou une chaine vide sinon">
		<cfreturn structKeyExists(VARIABLES,"enrollment_username") ? VARIABLES.enrollment_username:"">
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentPassword" returntype="String"
	hint="Retourne le password utilisateur pour l'enrollment ou une chaine vide sinon">
		<cfreturn structKeyExists(VARIABLES,"enrollment_password") ? VARIABLES.enrollment_password:"">
	</cffunction>
	
	<cffunction access="public" name="getClientPort" returntype="Numeric" hint="Retourne le port client ou -1 s'il n'est pas défini pour le vendor">
		<cfreturn structKeyExists(VARIABLES,"client_port") ? VAL(VARIABLES.client_port):-1>
	</cffunction>
	
	<cffunction access="public" name="getDeviceId" returntype="Any" output="false"
	hint="Toute exception capturée sera stockée (et donc loggée). Dans ce cas la valeur retournée sera indéfinie (i.e nulle)">
		<cfargument name="serial" type="String" required="true" hint="Numéro de série du device">
		<cfargument name="imei" type="String" required="false" default="" hint="IMEI du device">
		<cfset var qDeviceId="">
		<cfset var procedureName="PKG_MDM.getDeviceMdmInfos">
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#getIdGroup()#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.serial#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ARGUMENTS.imei#">
				<cfprocresult name="qDeviceId">
			</cfstoredproc>
			<cfif qDeviceId.recordcount GT 0>
				<cfset var osFamilly=qDeviceId["PLATFORM"][1]>
				<cfif findNoCase("iOS",osFamilly) GT 0>
					<cfset osFamilly=SUPER.iOS()>
				<cfelseif findNoCase("ANDROID",osFamilly) GT 0>
					<cfset osFamilly=SUPER.ANDROID()>
				<cfelse>
					<cfset osFamilly=SUPER.UNKNOWN()>
				</cfif>
				<cfreturn {
					mdmPoolId=VAL(qDeviceId["IDMDM_FLUX_PARC"][1]),idGroup=VAL(qDeviceId["IDRACINE"][1]),
					vendor=qDeviceId["MDM_VENDOR"][1],user=qDeviceId["USERNAME_MDM"][1],
					serial=qDeviceId["NUM_SERIE"][1],imei=qDeviceId["IMEI"][1],mdmId=qDeviceId["ID_MDM"][1],
					osFamilly=osFamilly,phoneNumber=qDeviceId["NUM_TEL"][1],dateAdded=qDeviceId["IDMDM_FLUX_PARC"][1]
				}>
			<cfelse>
				<cfreturn {}>
			</cfif>
			<cfcatch type="Any">
				<!--- Stockage et log de l'erreur: Cette implémentation remplace les valeurs de IMNT_CAT,SUBJECTID,BODY --->
				<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
				<cfset logStorage.logIt({
					UUID=createUUID(),BODY_FORMAT=logStorage.HTML_BODY(),LOG_TYPE=logStorage.ERROR_LOG(),IDMNT_ID=logStorage.DRT_002(),
					SERIAL=ARGUMENTS.serial,IMEI=ARGUMENTS.imei,MDM_SERVER=THIS,EXCEPTION=CFCATCH,IDGROUP=getIdGroup()
				})>
				<cfreturn javaCast("null",0)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="updateServerInfos" returntype="void">
		<cfargument name="mdmInfos" type="Struct" required="true" hint="Infos serveur MDM (Voir la documentation développeur de l'API)">
		<cfthrow type="Custom" message="This function is not implemented" detail="MdmPool.updateServerInfos()">
	</cffunction>
	
	<cffunction access="public" name="resetServerInfos" returntype="void">
		<cfthrow type="Custom" message="This function is not implemented" detail="MdmPool.resetServerInfos()">
	</cffunction>
	
	<cffunction access="private" name="requestServerInfos" returntype="Any" hint="Récupère et retourne les infos serveur MDM provenant de la base.
	Toute exception capturée sera stockée (et donc loggée). Dans ce cas la valeur retournée sera indéfinie (i.e nulle)">
		<cfset var qDeviceId="">
		<cfset var procedureName="PKG_MDM.get_server_racine_v2">
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="#procedureName#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#getIdGroup()#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procReturnStatus">
				<cfprocresult name="qMdmServerInfos">
			</cfstoredproc>
			<cfif procReturnStatus LT 0>
				<cfthrow type="Custom" message="Unable to request MDM server infos with #procedureName#" detail="Return status is #procReturnStatus#">
			<cfelse>
				<cfif qMdmServerInfos.recordcount GT 0>
					<cfset var concatVendorEndpoint=qMdmServerInfos["MDM"][1]>
					<cfset var vendorEndpointValues=listToArray(concatVendorEndpoint,"://",FALSE,TRUE)>
					<cfif arrayLen(vendorEndpointValues) EQ 2>
						<cfreturn {
							vendor=vendorEndpointValues[1],endpointURI=vendorEndpointValues[2],client_port=0,
							isSSL=yesNoFormat(VAL(qMdmServerInfos["USE_SSL"][1])),adminEmail=qMdmServerInfos["ADMIN_EMAIL"][1],
							ws_username=qMdmServerInfos["USERNAME"][1],ws_password=qMdmServerInfos["PASSWORD"][1],
							enrollment_username=qMdmServerInfos["ENROLL_USERNAME"][1],enrollment_password=qMdmServerInfos["ENROLL_PASSWORD"][1],
							TEST_MSG=qMdmServerInfos["TEST_MSG"][1],IS_TEST_OK=yesNoFormat(VAL(qMdmServerInfos["IS_TEST_OK"][1])),
							IS_DEFAULT=yesNoFormat(VAL(qMdmServerInfos["IS_DEFAULT"][1])),USERID=VAL(qMdmServerInfos["USERID"][1])
						}>
					<cfelse>
						<cfthrow type="Custom" message="Unable to identify MDM vendor and endpoint"
						detail="Invalid vendor://endpoint format '#concatVendorEndpoint#'">
					</cfif>
				<cfelse>
					<cfreturn {}>
				</cfif>
			</cfif>
			<cfcatch type="Any">
				<!--- Stockage et log de l'erreur: Cette implémentation remplace les valeurs de IMNT_CAT,SUBJECTID,BODY --->
				<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
				<cfset logStorage.logIt({
					UUID=createUUID(),BODY_FORMAT=logStorage.HTML_BODY(),LOG_TYPE=logStorage.ERROR_LOG(),IDMNT_ID=logStorage.DRT_002(),
					MDM_SERVER=THIS,EXCEPTION=CFCATCH,IDGROUP=getIdGroup()
				})>
				<cfreturn javaCast("null",0)>
			</cfcatch>
		</cftry>
		<cfreturn deviceId>
	</cffunction>
</cfcomponent>