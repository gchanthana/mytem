<cfcomponent name="QwReportRunner" alias="fr.consotel.consoview.bi.report.quickwin.QwReportRunner">
	<cffunction name="runQwReportForRacine" access="remote" returntype="numeric"> 
		<cfargument name="idracine_master" required="true" type="numeric">
		<cfargument name="idracine" required="true" type="numeric">
		<cfargument name="datedeb_sans_conso" required="true" type="string">
		<cfargument name="datefin_sans_conso" required="true" type="string">
		<cfargument name="datedeb_deploiement" required="true" type="string">
		<cfargument name="datefin_deploiement" required="true" type="string">
		<cfargument name="datedeb_ratio_gp" required="true" type="string">
		<cfargument name="datefin_ratio_gp" required="true" type="string">
		<cfargument name="produit_acces" required="true" type="numeric">
		<cfargument name="mobile_inclus" required="true" type="numeric">
		<cfargument name="periodicite" required="true" type="numeric">
		<cfargument name="montant_min_conso_ft" required="true" type="numeric">
		<cfargument name="categorie_gp" required="true" type="string">

		<cfset qwInfos=structNew()>
		<cfset qwInfos["IDRACINE_MASTER"]=arguments.idracine_master>
		<cfset qwInfos["IDRACINE"]=arguments.idracine>
		
		<cfset qwInfos["SANS_CONSO"]=structNew()>
		<cfset tmpStruct=qwInfos["SANS_CONSO"]>
		<cfset tmpStruct["DATEDEB"]=arguments.datedeb_sans_conso>
		<cfset tmpStruct["DATEFIN"]=arguments.datefin_sans_conso>
		<cfset tmpStruct["PRODUIT_ACCES"]=arguments.produit_acces>
		<cfset tmpStruct["MOBILE_INCLUS"]=arguments.mobile_inclus>
		
		<cfset qwInfos["DEPLOIEMENT"]=structNew()>
		<cfset tmpStruct=qwInfos["DEPLOIEMENT"]>
		<cfset tmpStruct["DATEDEB"]=arguments.datedeb_deploiement>
		<cfset tmpStruct["PERIODICITE"]=arguments.periodicite>
		<cfset tmpStruct["MONTANT_FT_MIN"]=arguments.montant_min_conso_ft>
		
		<cfset qwInfos["RATIO_GPRODUITS"]=structNew()>
		<cfset tmpStruct=qwInfos["RATIO_GPRODUITS"]>
		<cfset tmpStruct["DATEDEB"]=arguments.datedeb_ratio_gp>
		<cfset tmpStruct["DATEFIN"]=arguments.datefin_ratio_gp>
		<cfset tmpStruct["CATEGORIE_GP"]=arguments.categorie_gp>
		<cfset tmpStruct["IDGROUPE_PRODUIT"]=-1>
		
		<cfset qwInfos["ACCUEIL"]=structNew()>
		<cfset tmpStruct=qwInfos["ACCUEIL"]>
		<cfset tmpStruct["DATEDEB1"]=arguments.datedeb_ratio_gp>
		<cfset tmpStruct["DATEFIN1"]=arguments.datefin_ratio_gp>
		<cfset tmpStruct["DATEDEB2"]=arguments.datedeb_sans_conso>
		<cfset tmpStruct["DATEFIN2"]=arguments.datefin_sans_conso>
		<cfset tmpStruct["DATEDEB3"]=arguments.datedeb_deploiement>
		<cfset tmpStruct["DATEFIN3"]=arguments.datefin_deploiement>
		<cfset tmpStruct["CATEGORIE_GP"]=arguments.categorie_gp>
		
		<cfset qwInfos["PERIMETRE"]=structNew()>
		<cfset tmpStruct=qwInfos["PERIMETRE"]>
		<cfset tmpStruct["DATEDEB1"]=arguments.datedeb_ratio_gp>
		<cfset tmpStruct["DATEFIN1"]=arguments.datefin_ratio_gp>
		<cfset tmpStruct["DATEDEB2"]=arguments.datedeb_sans_conso>
		<cfset tmpStruct["DATEFIN2"]=arguments.datefin_sans_conso>
		<cfset tmpStruct["DATEDEB3"]=arguments.datedeb_deploiement>
		<cfset tmpStruct["DATEFIN3"]=arguments.datefin_deploiement>
		
		<cfset qwReportService=createObject("component","fr.consotel.consoview.bi.report.quickwin.QWReportService")>
		<cfset qwJobIdArray=qwReportService.scheduleQWReport(qwInfos)>
		<cfset returnCode=0>
		<cfset FINISHED=FALSE>
		<cfloop condition="#FINISHED# EQ FALSE">
			<cfquery name="qGetJobs" datasource="DW">
				select a.total, a.job_id, a.owner, a.user_job_name, a.status_value, a.status,
				       SUM(a.status_value) OVER (PARTITION BY a.total) as total_value
				from   (
				        select 1 as total, t.job_id, t.owner, t.user_job_name,
				        	   o.status, decode(o.status,'S',1,0) as status_value
				        from   xmlp_sched_job t, xmlp_sched_output o
				        where  t.job_id in (#arrayToList(qwJobIdArray,",")#)
				               and t.job_id=o.job_id
				        group by t.job_id, t.owner, t.user_job_name, o.status
				       ) a
				order by a.user_job_name
			</cfquery>
			<cfif qGetJobs.recordcount GT 0>
				<cfset nbSuccess=qGetJobs["total_value"][1]>
				<cfif nbSuccess EQ arrayLen(qwJobIdArray)>
					<cfset FINISHED=TRUE>
					<cfset returnCode=1>
				</cfif>
			<cfelse>
				<cfset FINISHED=TRUE>
				<cfset returnCode=0>
			</cfif>
		</cfloop>
		<cfreturn returnCode>
	</cffunction>
	
	<cffunction name="runQwReportXmlStatus" access="remote" returntype="any"> 
		<cfargument name="idracine_master" required="true" type="numeric">
		<cfargument name="idracine" required="true" type="numeric">
		<cfargument name="datedeb_sans_conso" required="true" type="string">
		<cfargument name="datefin_sans_conso" required="true" type="string">
		<cfargument name="datedeb_deploiement" required="true" type="string">
		<cfargument name="datefin_deploiement" required="true" type="string">
		<cfargument name="datedeb_ratio_gp" required="true" type="string">
		<cfargument name="datefin_ratio_gp" required="true" type="string">
		<cfargument name="produit_acces" required="true" type="numeric">
		<cfargument name="mobile_inclus" required="true" type="numeric">
		<cfargument name="periodicite" required="true" type="numeric">
		<cfargument name="montant_min_conso_ft" required="true" type="numeric">
		<cfargument name="categorie_gp" required="true" type="string">
		<cfset qwInfos=structNew()>
		<cfset qwInfos["IDRACINE_MASTER"]=arguments.idracine_master>
		<cfset qwInfos["IDRACINE"]=arguments.idracine>
		
		<cfset qwInfos["SANS_CONSO"]=structNew()>
		<cfset tmpStruct=qwInfos["SANS_CONSO"]>
		<cfset tmpStruct["DATEDEB"]=arguments.datedeb_sans_conso>
		<cfset tmpStruct["DATEFIN"]=arguments.datefin_sans_conso>
		<cfset tmpStruct["PRODUIT_ACCES"]=arguments.produit_acces>
		<cfset tmpStruct["MOBILE_INCLUS"]=arguments.mobile_inclus>
		
		<cfset qwInfos["DEPLOIEMENT"]=structNew()>
		<cfset tmpStruct=qwInfos["DEPLOIEMENT"]>
		<cfset tmpStruct["DATEDEB"]=arguments.datedeb_deploiement>
		<cfset tmpStruct["PERIODICITE"]=arguments.periodicite>
		<cfset tmpStruct["MONTANT_FT_MIN"]=arguments.montant_min_conso_ft>
		
		<cfset qwInfos["RATIO_GPRODUITS"]=structNew()>
		<cfset tmpStruct=qwInfos["RATIO_GPRODUITS"]>
		<cfset tmpStruct["DATEDEB"]=arguments.datedeb_ratio_gp>
		<cfset tmpStruct["DATEFIN"]=arguments.datefin_ratio_gp>
		<cfset tmpStruct["CATEGORIE_GP"]=arguments.categorie_gp>
		<cfset tmpStruct["IDGROUPE_PRODUIT"]=-1>
		
		<cfset qwInfos["ACCUEIL"]=structNew()>
		<cfset tmpStruct=qwInfos["ACCUEIL"]>
		<cfset tmpStruct["DATEDEB1"]=arguments.datedeb_ratio_gp>
		<cfset tmpStruct["DATEFIN1"]=arguments.datefin_ratio_gp>
		<cfset tmpStruct["DATEDEB2"]=arguments.datedeb_sans_conso>
		<cfset tmpStruct["DATEFIN2"]=arguments.datefin_sans_conso>
		<cfset tmpStruct["DATEDEB3"]=arguments.datedeb_deploiement>
		<cfset tmpStruct["DATEFIN3"]=arguments.datefin_deploiement>
		<cfset tmpStruct["CATEGORIE_GP"]=arguments.categorie_gp>
		
		<cfset qwInfos["PERIMETRE"]=structNew()>
		<cfset tmpStruct=qwInfos["PERIMETRE"]>
		<cfset tmpStruct["DATEDEB1"]=arguments.datedeb_ratio_gp>
		<cfset tmpStruct["DATEFIN1"]=arguments.datefin_ratio_gp>
		<cfset tmpStruct["DATEDEB2"]=arguments.datedeb_sans_conso>
		<cfset tmpStruct["DATEFIN2"]=arguments.datefin_sans_conso>
		<cfset tmpStruct["DATEDEB3"]=arguments.datedeb_deploiement>
		<cfset tmpStruct["DATEFIN3"]=arguments.datefin_deploiement>
		
		<cfset qwReportService=createObject("component","fr.consotel.consoview.bi.report.quickwin.QWReportService")>
		<cfset qwJobIdArray=qwReportService.scheduleQWReport(qwInfos)>
		<cfset returnCode=0>
		<cfset FINISHED=FALSE>
		<cfloop condition="#FINISHED# EQ FALSE">
			<cfquery name="qGetJobs" datasource="DW">
				select a.total, a.job_id, a.owner, a.user_job_name, a.status_value, a.status,
				       SUM(a.status_value) OVER (PARTITION BY a.total) as total_value
				from   (
				        select 1 as total, t.job_id, t.owner, t.user_job_name,
				        	   o.status, decode(o.status,'S',1,0) as status_value
				        from   xmlp_sched_job t, xmlp_sched_output o
				        where  t.job_id in (#arrayToList(qwJobIdArray,",")#)
				               and t.job_id=o.job_id
				        group by t.job_id, t.owner, t.user_job_name, o.status
				       ) a
				order by a.user_job_name
			</cfquery>
			<cfif qGetJobs.recordcount GT 0>
				<cfset nbSuccess=qGetJobs["total_value"][1]>
				<cfif nbSuccess EQ arrayLen(qwJobIdArray)>
					<cfset FINISHED=TRUE>
					<cfset returnCode=1>
				</cfif>
			<cfelse>
				<cfset FINISHED=TRUE>
				<cfset returnCode=0>
			</cfif>
		</cfloop>
		<cfmail to="cedric.rapiera@consotel.fr" from="quickwin@consotel.fr" subject="Rapport de planification Quickwin" server="mail.consotel.fr" port="26">
			<cfoutput>
Quickwin genere dans le repertoire :
ftp://sun-bi/OBIEE/QUICKWIN/LABS/#arguments.idracine#/
			</cfoutput>
		</cfmail>
		<cfxml variable="xmlDataset">
			<ROWSET>
				<cfoutput>
					<cfloop query="qGetJobs">
						<ROW>
							<IDRACINE>#arguments.idracine#</IDRACINE>
							<JOB_ID>#JOB_ID#</JOB_ID>
							<OWNER>#OWNER#</OWNER>
							<USER_JOB_NAME>#USER_JOB_NAME#</USER_JOB_NAME>
							<STATUS>#STATUS#</STATUS>
							<STATUS_VALUE>#STATUS_VALUE#</STATUS_VALUE>
							<TOTAL_VALUE>#TOTAL_VALUE#</TOTAL_VALUE>
						</ROW>
					</cfloop>
				</cfoutput>
			</ROWSET>
		</cfxml>
		<cfreturn toString(xmlDataset)>
	</cffunction>
</cfcomponent>
