package univers.parametres.parametrecompte.service
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import multizone.utils.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class WorkflowCloture extends EventDispatcher
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		public var isClotureAuto	:Boolean = false;
		
		public var nbrJoiursCloture	:Number = 60;;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function WorkflowCloture()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS - APPELS DE PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		public function getInfosClotureAutomatique():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.M21.WorkflowService',
																				'getInfosClotureAutomatique',
																				getInfosClotureAutomatiqueResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		public function setInfosClotureAutomatique(isAuto:Boolean, nbrJours:Number):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				'fr.consotel.consoview.M21.WorkflowService',
																				'setInfosClotureAutomatique',
																				setInfosClotureAutomatiqueResultHandler);
			
			RemoteObjectUtil.callService(op,Formator.convertBoolToInt(isAuto),
											nbrJours);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVEES - RETOURS DE PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getInfosClotureAutomatiqueResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if((re.result as ArrayCollection).length > 0)
				{
					isClotureAuto 		= Formator.convertIntToBool(int(re.result[0].IS_ACTIF));
					
					if(isClotureAuto)
						nbrJoiursCloture = int(re.result[0].DELAI);
					else
						nbrJoiursCloture = 60;
				}
				else
				{
					isClotureAuto 		= false;
					nbrJoiursCloture	= 60;
				}
				
				dispatchEvent(new Event('GET_INFOS_CLOTURE_WFLW'));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Erreur_de_r_cup_ration_des_informations_'), ResourceManager.getInstance().getString('M21', 'Consoview'), null);
		}
		
		private function setInfosClotureAutomatiqueResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)
			{
				dispatchEvent(new Event('SET_INFOS_CLOTURE_WFLW'));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M21', 'Erreur_de_mise___jour_des_informations_d'), ResourceManager.getInstance().getString('M21', 'Consoview'), null);
		}
		
	}
}