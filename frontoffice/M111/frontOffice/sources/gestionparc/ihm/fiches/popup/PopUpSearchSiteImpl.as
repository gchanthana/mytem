package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;

	public class PopUpSearchSiteImpl extends TitleWindow
	{
		public var dgSites				:DataGrid;
		public var txtSearch			:TextInput;
		public var chckSelectSite		:RadioButton;
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		public var itemCurrentLabel		:Label;
		private var _site				:Object;
		private var _listeSites:ArrayCollection=new ArrayCollection;
		
		private var _itemSelectedFirst 	: Object = null;

		public function PopUpSearchSiteImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(fe:FlexEvent):void
		{
			dgSites.addEventListener(ListEvent.ITEM_CLICK, dgSiteItemClickHandler);
			txtSearch.addEventListener(Event.CHANGE, txtSearchHandler);
			btnValider.addEventListener(MouseEvent.CLICK, btnValiderHandler);
			btnAnnuler.addEventListener(MouseEvent.CLICK, onCloseHandler);
			
			// initialisation de l'affichage du label du pool
			afficherInitialPoolLabel();
			initCurseur();
			
		}
		
		private function initCurseur():void
		{
			callLater(txtSearch.setFocus);
		}
		
		// initialisation de l'affichage du label du pool
		private function afficherInitialPoolLabel():void
		{
			if(listeSites != null)
			{
				var longPool:int = listeSites.length;
				for (var i:int = 0; i < longPool; i++) 
				{
					if(listeSites[i].SELECTED)
					{
						_site = listeSites[i];
						_itemSelectedFirst = listeSites[i]; 
						break;
					}
				}
			}
			itemCurrentLabel.text = getItemCurrentLabel();
		}

		private function dgSiteItemClickHandler(le:ListEvent):void
		{
			var itemRenderer:Object=le.itemRenderer;
			var currentItem:Object=itemRenderer.data;
			_site=currentItem;
			setSelectedSite(currentItem);
			itemCurrentLabel.text = getItemCurrentLabel();
			btnValider.enabled=true;
		}

		private function setSelectedSite(currentSite:Object):void
		{
			var lenSites:int=listeSites.length;

			for each (var item:Object in listeSites)
			{
				if (currentSite != null)
				{
					if (item.libelleSite == currentSite.libelleSite)
						item.SELECTED=true;
					else
						item.SELECTED=false;
				}

				listeSites.itemUpdated(item);
			}
		}

		protected function txtSearchHandler(e:Event):void
		{
			if (listeSites != null)
			{
				listeSites.filterFunction=filterFunction;
				listeSites.refresh();
			}
		}

		private function btnValiderHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event('GET_SELECTED_SITE_ISVALIDATE'));
			onCloseHandler(null);
		}

		private function onCloseHandler(me:MouseEvent):void
		{
			this.initPopUp();
			PopUpManager.removePopUp(this);
		}
		
		// initialise la popup vers le choix du pool initial
		private function initPopUp():void
		{
			txtSearch.text = '';
			txtSearch.dispatchEvent(new Event(Event.CHANGE));
			itemCurrentLabel.text = getItemCurrentLabel();
			
			for (var i:int = 0; i < listeSites.length; i++) 
			{
				listeSites[i].SELECTED = false;
				if((_itemSelectedFirst != null)&&(listeSites[i].idSite == _itemSelectedFirst.idSite))
				{
					listeSites[i].SELECTED = true;
					listeSites.itemUpdated(listeSites[i]);
					break;
				}
			}
					
		}
		
		public function getItemCurrentLabel():String
		{
			if (_site != null)
				return _site.libelleSite;
			else
				return '';
		}

		public function closeWindowHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean=true;

			rfilter=rfilter && (item.libelleSite != null && item.libelleSite.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1);

			return rfilter;
		}

		public function getSite():Object
		{
			return _site;
		}

		public function setSite(values:ArrayCollection, currentValue:Object=null):void
		{
			listeSites=values;
			if (currentValue != null)
				_site=currentValue;

			setSelectedSite(currentValue);
		}

		[Bindable]
		public function get listeSites():ArrayCollection
		{
			return _listeSites;
		}

		public function set listeSites(value:ArrayCollection):void
		{
			_listeSites = value;
		}

	}
}