package intranet.EspacePerso
{
	import flash.events.KeyboardEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import mx.events.FlexEvent;
	import flash.net.URLVariables;
	import flash.net.sendToURL;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	public class TelIP extends TelIpIHM
	{
		public function TelIP()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		public function initIHM(event:Event):void {
			lbNom.label=Intranet.mySession.nom;
			txtNumber.addEventListener(KeyboardEvent.KEY_DOWN,processEnter);
			btnTel.addEventListener(MouseEvent.CLICK,ouvrirSimpleUrl);
			
		}
		private function processEnter(event:KeyboardEvent):void {
			if (event.keyCode==13) {
				callNumber();
			}
		}
		private function ouvrirSimpleUrl(event:Event):void {
			switch (event.currentTarget.id) {
				// Paramètres personnels
				case "btnTel":
					allerAsimpleUrl(Intranet.mySession.urlTelephone,"GET");
					break;
			}
		}
	
		private function callNumber():void {
			var url:String = "http://" + Intranet.mySession.urlTelephone + "/command.htm";
            var variables:URLVariables = new URLVariables();
           	variables.number=txtNumber.text;
            var request:URLRequest = new URLRequest(url);
            request.method="GET";
            request.data=variables;
            sendToURL(request);
		}
		private function allerAsimpleUrl(adresse:String, methode:String):void {
			var url:String = "http://" + adresse;
            var variables:URLVariables = new URLVariables();
            if (methode=="POST") {
            	variables.code=Intranet.mySession.code;
            	variables.id=Intranet.mySession.id;
            	variables.logon=Intranet.mySession.logon;
            	variables.username=Intranet.mySession.username;
            	request.data=variables;
            }
            var request:URLRequest = new URLRequest(url);
            request.method=methode;
            navigateToURL(request,"_blank");
		}
	}
}