package ihm.widget.roaminginternationalvoixdata
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	
	import event.widget.WidgetEvent;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import ihm.widget.widgetabstract.WidgetAbstract;
	
	import ilog.maps.MapBase;
	import ilog.maps.MapFeature;
	import ilog.maps.WorldCountriesMap;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.Text;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.graphics.SolidColor;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	
	import utils.DataGridDataExporter;
	
	import vo.widgets.Widget1DataVo;

	public class RoamingInternationalVoixDataImpl extends WidgetAbstract
	{
		private var _dataChange:Boolean=false;

		public var world:WorldCountriesMap;

		public var btn_1:Button;
		public var btn_2:Button;
		public var btn_3:Button;
		public var btn_4:Button;

		public var lbl_1:Label;
		public var lbl_2:Label;
		public var lbl_3:Label;
		public var lbl_4:Label;

		public var box_lbl_1:Box;
		public var box_lbl_2:Box;
		public var box_lbl_3:Box;
		public var box_lbl_4:Box;

		public var box1:HBox;
		public var box2:HBox;
		public var box3:HBox;
		public var box4:HBox;

		private var _textInfo:Text;
		private var _paysRoaming:ArrayCollection;

		public var canvasMap:Canvas;
		public var cbx_choix:ComboBox;
		public var roamingData :DataGrid

		[Bindable]
		public var sLText:String='';
		public var sText:String='';

		/**
		 * tableau de couleurs
		 */
		private var colors:Array=[0xC2E699, 0x3cbc23, 0x248d0f, 0x006837];
		/**
		 * variable contenant une liste locale de la data selectionnee par utilisateur a partir d'un combobox,
		 * la liste comporte 2 champs (trigramme et data)
		 */
		[Bindable]
		private var paysDataSelectionnee:ArrayCollection=new ArrayCollection();

		override protected function setWidgetDataHandler(e:WidgetEvent):void
		{
			super.stopSpinnerWidget();
			dataProviderWidget=getDataWidgetService.model.dataWidget;
			paysRoaming=dataProviderWidget;
			if (dataProviderWidget != null && world != null)
			{
				canvasMap.visible=true;
				updatePaysDataSelectionnees(paysRoaming, cbx_choix.selectedItem.code);
			}
		}

		override protected function createChildren():void
		{
			super.createChildren();

			settingIcon.visible=false;
			labelSetting.visible=false;
			choixUniteWidget.visible=false;
			labelUniteWidget.visible=false;
			choixThemeIcon.visible=false;
			labelThemeWidget.visible=false;
			choixUniteWidget.includeInLayout=false;
			labelUniteWidget.includeInLayout=false;
			choixPoolWidget.visible=false;
			labelPoolWidget.visible=false;
		}

		/**
		 * expoter les datas en csv 
		 */		
		override protected function onClickCsvWidgetIcon (e:MouseEvent):void
		{
			if(dataProviderWidget != null)
			{
				if (dataProviderWidget.length > 0)
				{
					/** ; pour le csv | false : pour exporter le datagrid si true : on exporte que l'element selectionné */ 
					
					var dataToExport:String=DataGridDataExporter.getCSVString(roamingData, ";", "\n", false); 
					var url:String=moduleAccueilE0IHM.urlBackoffice + "/fr/consotel/consoview/M54/exportCSV/ExportCSV.cfm";
					
					DataGridDataExporter.exportCSVString(url, dataToExport, "export");
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M54', 'Pas_de_donn_es___exporter'), 'Consoview', null);
			}
		}
		
		public function RoamingInternationalVoixDataImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * fonction d'initialisation d'écoute d'évenements
		 */
		private function init(e:FlexEvent):void
		{
			cbx_choix.addEventListener(ListEvent.CHANGE, comboChoixChangeHandler);
			world.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			world.addEventListener(MouseEvent.MOUSE_OVER, world_mouseOverHandler);
			world.addEventListener(MouseEvent.MOUSE_OUT, world_mouseOutHandler);

			box1.addEventListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
			box2.addEventListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
			box3.addEventListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
			box4.addEventListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);

			box1.addEventListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
			box2.addEventListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
			box3.addEventListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
			box4.addEventListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);
		}

		/**
		 * fonction qui permet l'affichage d'un textarea contenant un nom de pays et l'indicateur
		 * du volume, le textarea suit les mouvements de la souris, la fonction traite un MouseEvent
		 * @param param1 de type MouseEvent
		 */
		private function onMouseMove(e:MouseEvent):void
		{

			var m:Point=new Point(e.stageX - 30, e.stageY + 30);
			sLText="";
			sText="";

			if (_textInfo == null)
			{
				_textInfo=new Text;
				_textInfo.setStyle("fontWeight", 'bold');
				_textInfo.width=140;
				_textInfo.maxHeight=100;
				_textInfo.height=66;
				_textInfo.alpha=0.5;
			}
			var f:MapFeature=e.target as MapFeature;

			var sValue:String='';
			var sUnite:String='';
			if (f != null)
			{
				_textInfo.alpha=0.6;
				sText=world.getString(f.key) + '<br/>';
				for each (var p:Widget1DataVo in paysDataSelectionnee)
				{
					if (f.key == p.trigramme)
					{
						sValue=ConsoviewFormatter.formatNumber(Number(p.donnees), 2);
						sUnite=p.unite;
						sText=sText + cbx_choix.selectedLabel + ":" + '<br/>' + '<b><font color ="#CC0000">' + sValue + ' ' + sUnite + '</font></b>';
					}
				}

				_textInfo.htmlText=sText;
				sLText=StringUtil.substitute(sText, '<br/>', ' ');
				m=world.globalToLocal(m);
				_textInfo.move(m.x, m.y);
			}
			else
			{
				_textInfo.alpha=0;
				_textInfo.text="";
			}
			world.addChild(_textInfo);
		}

		/**
		 * fonction de coloration des elements de carte selon les données reçues en argument
		 * @param param1 de type ArrayCollection
		 */
		private function colorize(countries:ArrayCollection):void
		{
			for each (var k:String in world.featureNames)
			{
				var m:MapFeature=world.getFeature(k);

				for each (var p:Widget1DataVo in countries)
				{
					if (k == (p.trigramme))
					{
						var v:int=p.donnees;
						if (v >= 0 && v < 100)
							m.setStyle(MapBase.FILL, new SolidColor(colors[0]));
						else if (v >= 100 && v < 300)
							m.setStyle(MapBase.FILL, new SolidColor(colors[1]));
						else if (v >= 300 && v < 1000)
							m.setStyle(MapBase.FILL, new SolidColor(colors[2]));
						else
							m.setStyle(MapBase.FILL, new SolidColor(colors[3]));
					}
				}
			}
		}

		/**
		 * fonction de traitement d'événement de souris: souris au-dessus d'un bouton ou d'un label, de type MouseEvent
		 * @param param1 de type MouseEvent
		 */
		protected function onMouseOverHandler(evt:MouseEvent):void
		{
			this.clear(world);
			for each (var k:String in world.featureNames)
			{
				var m:MapFeature=world.getFeature(k);
				if (m.highlighted == true)
				{
					m.highlighted=false;
				}
				for each (var p:Widget1DataVo in paysDataSelectionnee)
				{
					if (k == p.trigramme)
					{
						if ((evt.currentTarget == box1) && (p.donnees >= 0 && p.donnees < 100))
						{
							world.getFeature(p.trigramme).setStyle(MapBase.FILL, new SolidColor((colors[0])));
						}
						else if ((evt.currentTarget == box2) && (p.donnees >= 100 && p.donnees < 300))
						{
							world.getFeature(p.trigramme).setStyle(MapBase.FILL, new SolidColor(colors[1]));
						}
						else if ((evt.currentTarget == box3) && (p.donnees >= 300 && p.donnees < 1000))
						{
							world.getFeature(p.trigramme).setStyle(MapBase.FILL, new SolidColor(colors[2]));
						}
						else if ((evt.currentTarget == box4) && (p.donnees >= 1000))
						{
							world.getFeature(p.trigramme).setStyle(MapBase.FILL, new SolidColor(colors[3]));
						}
					}
				}
			}
		}


		/**
		 * fonction de traitement d'événement de souris: souris n'est plus au_dessus d'un bouton, de type MouseEvent
		 * @param param1 de type MouseEvent
		 */
		protected function onMouseOutHandler(evt:MouseEvent):void
		{
			colorize(paysDataSelectionnee);
		}

		/**
		 * fonction de traitement d'événement de souris: souris au-dessus d'un élément de carte, de type MouseEvent
		 * @param param1 de type MouseEvent
		 */
		protected function world_mouseOverHandler(e:MouseEvent):void
		{
			if (e.target is MapFeature)
			{
				for each (var p:Widget1DataVo in paysDataSelectionnee)
				{
					if (e.target.key == p.trigramme)
					{
						if (p.donnees >= 0 && p.donnees < 99)
						{
							box_lbl_1.setStyle("borderStyle", "solid");
							box_lbl_1.setStyle("cornerRadius", 5);
							lbl_1.setStyle("borderColor", colors[0]);
						}
						else if (p.donnees >= 100 && p.donnees < 299)
						{
							box_lbl_2.setStyle("borderStyle", "solid");
							box_lbl_2.setStyle("cornerRadius", 5);
							lbl_2.setStyle("borderColor", colors[1]);
						}
						else if (p.donnees >= 300 && p.donnees < 1000)
						{
							box_lbl_3.setStyle("borderStyle", "solid");
							box_lbl_3.setStyle("cornerRadius", 5);
							lbl_3.setStyle("borderColor", colors[2]);
						}
						else if (p.donnees >= 1000)
						{
							box_lbl_4.setStyle("borderStyle", "solid");
							box_lbl_4.setStyle("cornerRadius", 5);
							lbl_4.setStyle("borderColor", colors[3]);
						}
					}
				}
			}
		}

		/**
		 * fonction de traitement d'événement de souris: souris n'est plus au-dessus d'un élément de carte, de type MouseEvent
		 * @param param1 de type MouseEvent
		 */
		protected function world_mouseOutHandler(event:MouseEvent):void
		{
			box_lbl_1.setStyle("borderStyle", "none");
			box_lbl_2.setStyle("borderStyle", "none");
			box_lbl_3.setStyle("borderStyle", "none");
			box_lbl_4.setStyle("borderStyle", "none");
		}

		/**
		 * fonction qui efface la coloration des elements de la carte
		 * @param param1 de type WorldCountriesMap
		 */
		public function clear(map:WorldCountriesMap):void
		{
			for each (var k:String in map.featureNames)
			{
				var m:MapFeature=map.getFeature(k);
				m.setStyle(MapBase.FILL, new SolidColor(0xFFFFFF));
			}
		}

		protected function comboChoixChangeHandler(event:ListEvent):void
		{
			updatePaysDataSelectionnees(paysRoaming, cbx_choix.selectedItem.code);
		}

		/**
		 * fonction de mise a jour du tableau de données "paysDataSelectionnee" pour y stocker les donnees
		 * selectionnees par l'utilisateur dans le ComboBox, coloriage de la carte en fonction des donnees
		 * de "paysDataSelectionnee"
		 * @param param1 de type ArrayCollection
		 * @param param2 de type String
		 */
		private function updatePaysDataSelectionnees(arrayCol:ArrayCollection, codeChoix:String):void
		{
			var dataPays:Widget1DataVo;

			paysDataSelectionnee.removeAll();

			if (arrayCol.length > 0)
			{
				for (var i:int=0; i < arrayCol.length; i++)
				{
					dataPays=new Widget1DataVo;
					dataPays=arrayCol[i] as Widget1DataVo;
					dataPays.fillDataSelectionnee(arrayCol[i], codeChoix);
					paysDataSelectionnee.addItem(dataPays);
				}
				colorize(paysDataSelectionnee);
			}
		}

		[Bindable]
		public function get paysRoaming():ArrayCollection
		{
			return _paysRoaming;
		}

		public function set paysRoaming(value:ArrayCollection):void
		{
			_paysRoaming=value;
		}
	}
}
