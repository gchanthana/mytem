package univers.inventaire.inventaire.etapes
{
	import composants.util.ConsoviewUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	public class RenameSousTete
	{
		
		private var _listeProduitsCommande : ArrayCollection;
		private var _listeProduitsOperation : ArrayCollection;
		
		public function RenameSousTete(listeProduitsOperation : ArrayCollection, listeProduitsCommande : ArrayCollection)
		{
			_listeProduitsCommande = listeProduitsCommande;
			_listeProduitsOperation = listeProduitsOperation;
		}
		
		/**
		 * Met à jour les libelle des lignes du panier
		 * */
		public function renameSousTete(oldValue : String, newValue : String, idSousTete : Number):void{			
			if (_listeProduitsCommande != null && _listeProduitsOperation != null){
				renommerSousTete(oldValue,newValue,idSousTete);
			}	
		}
		
		private function renommerSousTete(oldValue : String, newValue : String,idSousTete : Number):void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
											  "fr.consotel.consoview.inventaire.cycledevie.Inventaire",
											  "renommerSousTete",
											 renommerSousTeteResultHandler,null);
			
			var indexDans_listeProduitsCommande : Number = ConsoviewUtil.getIndexById(_listeProduitsCommande,"ligneID",idSousTete);			
			if (indexDans_listeProduitsCommande > -1){
				var prdt : ElementCommande = _listeProduitsCommande[indexDans_listeProduitsCommande];
				RemoteObjectUtil.callService(op,prdt,newValue,oldValue);	
			}else{
				Alert.show("Ce numéro de ligne n'est pas disponible !","Erreur");
				trace("renommerSousTete erreur");
				var ligneDans_listeProduitsOperation : Object;
				for (var j : int = 0; j < _listeProduitsOperation.source.length; j++){
					if(_listeProduitsOperation.source[j].IDSOUS_TETE == idSousTete){
						ligneDans_listeProduitsOperation = _listeProduitsOperation.source[j];
						ligneDans_listeProduitsOperation.SOUS_TETE = oldValue;					
						_listeProduitsOperation.itemUpdated(ligneDans_listeProduitsOperation);
						_listeProduitsOperation.refresh();
					}
				}	
			}
		}
				
				
		private  function renommerSousTeteResultHandler(re : ResultEvent):void{			
			var idSousTete : Number = re.token.message.body[0].ligneID;
			var newValue : String = re.token.message.body[1];			
			var oldValue : String = re.token.message.body[2];
			var finalValue : String = "";			
			
			if (re.result == -1){
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Erreur : " + re.result.toString() ,"Erreur!");
				finalValue = oldValue;
			}
			
			if (re.result == -2){
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Ce numéro de téléphone n'est pas disponible","Erreur!");
				finalValue = oldValue;
			}
			
			if (re.result > -1){
				trace("RenameSousTete ok");		
				finalValue = newValue;
			}
			
			var ligneDans_listeProduitsCommande : Object;
			for (var i : int = 0; i < _listeProduitsCommande.source.length; i++){
				if(_listeProduitsCommande.source[i].ligneID == idSousTete){
					ligneDans_listeProduitsCommande = _listeProduitsCommande.source[i];
					ligneDans_listeProduitsCommande.libelleLigne = finalValue;
					_listeProduitsCommande.itemUpdated(ligneDans_listeProduitsCommande);
				}
			}
			
		
			var ligneDans_listeProduitsOperation : Object;
			for (var j : int = 0; j < _listeProduitsOperation.source.length; j++){
				if(_listeProduitsOperation.source[j].IDSOUS_TETE == idSousTete){
					ligneDans_listeProduitsOperation = _listeProduitsOperation.source[j];
					ligneDans_listeProduitsOperation.SOUS_TETE = finalValue;					
					_listeProduitsOperation.itemUpdated(ligneDans_listeProduitsOperation);
				}
			}
					
	  	}
	}
}