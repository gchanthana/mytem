package event.destinataire
{
	import flash.events.Event;

	public class ProfilEvent extends Event
	{

		public static const MODIFIER_PROFIL_EVENT:String='ModifierProfilEvent';
		public static const DELETE_PROFIL_EVENT:String='DeleteProfilEvent';
		public static const DELETE_PROFIL_CASCADE_EVENT:String='DeleteProfilCascadeEvent';
		public static const LIST_PROFIL_EVENT:String='ListProfilEvent';
		public static const AJOUTER_PROFIL_EVENT:String='AjouterProfilEvent';
		public static const MODIFIER_PROFIL_CLICK_EVENT:String='ModifierProfilClickEvent';
		public static const DELETE_PROFIL_CLICK_EVENT:String='DeleteProfilClickEvent';
		public static const LIST_PROFIL_DESTINATAIRE_EVENT :String="listProfilDestinataireEvent";
		public static const MAJ_LISTE_PROFIL_DISPONIBLE: String="MajListeProfilDisponible";

				
		public function ProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ProfilEvent(type, bubbles, cancelable);
		}
	}
}