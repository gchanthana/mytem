package gestionparc.ihm.fiches.importmasse.checkingEtatDataClass
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import gestionparc.entity.ImportDeMasseVO;
	import gestionparc.event.ImportDeMasseEvent;
	import gestionparc.ihm.fiches.importmasse.itemRenderer.ItemRendererCollabImpl;
	import gestionparc.services.collaborateur.CollaborateurDatas;
	import gestionparc.services.importmasse.ImportMasseServices;
	
	
	public class CheckingEtatDataCollab
	{
		
		//--------------- VARIABLES ----------------//
				
		private var _myTimer				:Timer 	= new Timer(_myDelayTimer,0);
		private var _myDelayTimer			:Number = 1000;
		private var _maxcharsMatricule		:int 	= 20;
		private var _maxcharsNom			:int 	= 20;
		private var _maxcharsPrenom			:int 	= 20;
		
		private var _itemData1				:ImportDeMasseVO;
		private var _itemImp				:ImportDeMasseVO;
		private var _objImp					:ImportMasseServices;
		
		private var _itemRend				:ItemRendererCollabImpl;
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function CheckingEtatDataCollab(itemRend:ItemRendererCollabImpl)
		{
			_objImp = new ImportMasseServices();
			this._itemRend = itemRend;
		}
		
		/* */
		public function checkEtatData(obj:ImportDeMasseVO):void
		{
			if(obj.MATRICULE.length > 20 || obj.NOM.length > 20 || obj.PRENOM.length > 20)
			{
				obj.ETAT_COLLAB = 11; //etat trop long (à l'import)
			}
			else if((CollaborateurDatas.matriculeIsActif==0 && obj.MATRICULE.length==0) || obj.NOM.length==0 || obj.PRENOM.length==0)
			{
				obj.ETAT_COLLAB = 9; //etat champs vide (à l'import)
			}
			else
			{
				obj.ETAT_COLLAB = -10 //etat spinner
				callServiceCheckEtat(obj);
			}
			_itemImp = obj;
		}
		
		/* */
		public function callServiceCheckEtat(obj:ImportDeMasseVO):void
		{
			removeListener();
			
			_objImp.checkCollab(obj.NOM,obj.PRENOM,obj.MATRICULE);
			_objImp.myDatas.addEventListener(ImportDeMasseEvent.CHECKED_COLLAB,checkCollabHandler);
		}
		
		/* */
		public function processCheckingEtatData():void
		{
			stopTimer();
			removeListener();
			var imp:ImportDeMasseVO = new ImportDeMasseVO();
			imp = _itemRend.data as ImportDeMasseVO;
			//si maxChar de l'input est dépassé à l'import
			if ((_itemRend.data.MATRICULE as String).length > _maxcharsMatricule
				|| (_itemRend.data.NOM as String).length > _maxcharsNom
				|| (_itemRend.data.PRENOM as String).length > _maxcharsPrenom)
			{
				imp.ETAT_COLLAB = -100;
				imp.ETAT_COLLAB = 11;
			}
			else
			{
				//si input vide
				if ((CollaborateurDatas.matriculeIsActif==0 && _itemRend.data.MATRICULE=="") 
					|| _itemRend.data.NOM=="" 
					|| _itemRend.data.PRENOM=="")
				{
					imp.ETAT_COLLAB = -100;
					imp.ETAT_COLLAB = 9;
				}
				
				//si input contient au moins un caractere
				if ((CollaborateurDatas.matriculeIsActif==1 || (CollaborateurDatas.matriculeIsActif==0 && _itemRend.data.MATRICULE!="")) 
					&& _itemRend.data.NOM!="" 
					&& _itemRend.data.PRENOM!="")
				{
					_myTimer.addEventListener(TimerEvent.TIMER, checkEtatHandler);
					_myTimer.start();
				}
			}
		}
		
		/* */
		private function checkEtatHandler(e:Event):void
		{
			_myTimer.removeEventListener(TimerEvent.TIMER, checkEtatHandler);
			processVerifEtat();
		}
		
		/* appel du service pour verif etat */
		private function processVerifEtat():void
		{
			_itemData1 = new ImportDeMasseVO();
			_itemData1 = _itemRend.data as ImportDeMasseVO;
//			_itemData1.checkCurrentData(this);
			checkEtatData(_itemData1);
		}
		
		/* retour des verifs - Handler */
		public function checkCollabHandler(impe:ImportDeMasseEvent):void
		{
			_itemImp.ETAT_COLLAB = int(impe.objRet);
			
			if(int(impe.objRet)==6 || int(impe.objRet)==7 || int(impe.objRet)==8)
				_itemImp.COLLAB_OK=true;
			else
				_itemImp.COLLAB_OK=false;
		}
		
		/* */
		private function removeListener():void
		{
			_objImp.myDatas.removeEventListener(ImportDeMasseEvent.CHECKED_COLLAB,checkCollabHandler);
		}
		
		/* */
		private function stopTimer():void
		{
			_myTimer.stop();
		}
	}
}