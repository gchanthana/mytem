package composants.util
{
	import mx.managers.PopUpManager;
	import mx.events.FlexEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import univers.accueil.UniversAccueil;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	import univers.UniversManager;
	import mx.events.CloseEvent;
	import composants.util.MailVO;
	import univers.parametres.perimetres.rightSide.destinataire;
	
	public class SendMail extends SendMailIHM
	{
		
		/* private var destinataireInterne:String="";
		private var mailExpediteur:String="";
		 */
		private var mail : MailVO;
		
		public static const MAIL_ENVOYE : String = "MailEnvoyer";
		
		public function SendMail()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			btnClose.addEventListener(MouseEvent.CLICK,btnCloseClickHandler);
			btnAjout.addEventListener(MouseEvent.CLICK,sendTheMail);
			PopUpManager.centerPopUp(this);
			rteMessage.textArea.styleName="TextInputAccueil";
			//rteMessage.removeChild(rteMessage.linkTextInput);
			rteMessage.textArea.percentHeight=100;
			addEventListener(CloseEvent.CLOSE,closeEventHandler);
		}
		
		private function closeEventHandler(ce : CloseEvent):void {
			PopUpManager.removePopUp(this);
		}
		
		private function btnCloseClickHandler(me : MouseEvent):void {
			PopUpManager.removePopUp(this);
		}
		
		public function initMail(module:String,sujet:String,dest:String):void {
			
			txtModule.text=module;
			txtSujet.text=sujet; 
			txtExpediteur.text=UniversManager.getSession().USER.PRENOM + " " +
					UniversManager.getSession().USER.NOM;								
			txtDest.text = dest;
			
			mail = new MailVO;
			mail.expediteur = UniversManager.getSession().USER.EMAIL;
			mail.destinataire = dest;
		}
		
		private function sendTheMail(me:MouseEvent):void {
			mail.cc = txtcc.text;
			mail.bcc = txtcci.text;
			mail.module = txtModule.text;
			mail.copiePourExpediteur = (cbCopie.selected)?"YES":"NO";
			mail.sujet = txtSujet.text;
			mail.message = rteMessage.htmlText;
			rteMessage.fontSizeCombo.selectedIndex = 0;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.util.sendMail",
								"sendSingleMail",
								processSendTheMail);			
			RemoteObjectUtil.callService(op,mail);		
		}
		
		private function processSendTheMail(r:ResultEvent):void
		{	
			dispatchEvent(new Event(MAIL_ENVOYE));
			PopUpManager.removePopUp(this);
		}
		
	}
}