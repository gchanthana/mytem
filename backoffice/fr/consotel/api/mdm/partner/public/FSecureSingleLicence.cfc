<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.FSecureSingleLicence" extends="fr.consotel.api.mdm.partner.public.FSecureOther" hint="Implémentation Single Licence FSecure">
	<!--- Single Licence Service --->
	<cffunction access="remote" name="searchSingleLicenses" returntype="String" hint="Retourne le résultat d'une recherche de single licence.
	Le résultat est une structure contenant les clés suivantes :<br>
	- TotalSearchResults : Nombre total de résultats de la recherche<br>
	- LastPageNumber : Numéro de la dernière page de la recherche<br>
	- IsLastPage : TRUE si la page retournée correspond à la dernière page<br>
	- SearchResultsOnPage : Nombre de résultats de la page retournée<br>
	- PageNumber : Numéro de la page retournée<br><br>
	- SingleLicenseRTO : Tableau contenant où chaque élément contient les clés suivantes :<br>
	- DbVersion : Version de la base de données<br>
	- ApplicationVersion : Version de l'application<br>
	- ImeiChanges : Nombre de changement d'IMEI<br>
	- ApplicationLang : Langage de l'application<br>
	- DeviceLang : Langage de l'équipement<br>
	- LastSWUpdate : Timestamp de la dernière mise à jour applicative<br>
	- LastDBUpdate : Timestamp de la dernière mise à jour de la base<br>
	- LastUpdateCheck : Timestamp de la dernière vérification de mise à jour<br>
	- NumberOfUpdateCheck : Nombre de vérification de mise à jour<br>
	- ExpirationDate : Date d'expiration de la licence<br>
	- ActivationDate : Date d'activation de la licence<br>
	- OperatingSystem : Système de l'équipement<br>
	- Imsi : Code IMSI de la carte SIM<br>
	- Imei : Code IMEI de l'équipement<br>
	- Phone : Numéro de téléphone<br>
	- Email : Email du souscripteur<br>
	- LastName : Nom du souscripteur<br>
	- FirstName : Prénom du souscripteur<br>
	- SingleLicenseKey : Clé de la single licence<br>
	- MultiLicenseKey : Clé de la licence multiple">
		<cfargument name="singleLicenseKeySearch" type="String" required="true" hint="Chaine à recherche dans la valeur de la clé">
		<cfargument name="phoneSearch" type="String" required="true" hint="Chaine à recherche dans le numéro de téléphone">
		<cfargument name="firstNameSearch" type="String" required="true" hint="Chaine à recherche dans le prénom">
		<cfargument name="lastNameSearch" type="String" required="true" hint="Chaine à recherche dans le nom">
		<cfargument name="imeiSearch" type="String" required="true" hint="Chaine à recherche dans IMEI">
		<cfargument name="stateSearch" type="String" required="true" hint="Etat à recherche. Valeurs possibles : 0 (Not activated), 1 (Up to date), 3 (Very old), 4 (Out dated). La valeur 2 ne semble pas définie">
		<cfargument name="pageNumber" type="String" required="true" hint="Numéro de la page de résultat à retourner">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfset var wsResult=wsService.searchSingleLicenses(
			ARGUMENTS["singleLicenseKeySearch"],ARGUMENTS["phoneSearch"],ARGUMENTS["firstNameSearch"],ARGUMENTS["lastNameSearch"],
			ARGUMENTS["imeiSearch"],ARGUMENTS["stateSearch"],ARGUMENTS["pageNumber"]
		)>
		<cfset var resultList={
			TotalSearchResults=toString(wsResult.getTotalSearchResults()), LastPageNumber=toString(wsResult.getLastPageNumber()), IsLastPage=toString(wsResult.getIsLastPage()),
			SearchResultsOnPage=toString(wsResult.getSearchResultsOnPage()), PageNumber=toString(wsResult.getPageNumber()), SingleLicenseRTO=[]
		}>
		<cfset var wsRTO=wsResult.getSingleLicenceRTO()>
		<cfif isDefined("wsRTO")>
			<cfset var rtoCount=arrayLen(wsRTO)>
			<cfloop index="i" from="1" to="#rtoCount#">
				<cfset resultList.SingleLicenseRTO[i]={
					DbVersion=toString(wsRTO[i].getDbVersion()), ApplicationVersion=toString(wsRTO[i].getApplicationVersion()), ImeiChanges=toString(wsRTO[i].getImeiChanges()),
					ApplicationLang=toString(wsRTO[i].getApplicationLang()), DeviceLang=toString(wsRTO[i].getDeviceLang()), LastSWUpdate=toString(wsRTO[i].getLastSWUpdate()),
					LastDBUpdate=toString(wsRTO[i].getLastDBUpdate()), LastUpdateCheck=toString(wsRTO[i].getLastUpdateCheck()), NumberOfUpdateCheck=toString(wsRTO[i].getNumberOfUpdateChecks()),
					ExpirationDate=toString(wsRTO[i].getExpirationDate()), ActivationDate=toString(wsRTO[i].getActivationDate()), OperatingSystem=toString(wsRTO[i].getOperatingSystem()),
					Imsi=toString(wsRTO[i].getImsi()), Imei=toString(wsRTO[i].getImei()), Phone=toString(wsRTO[i].getPhone()),
					Email=toString(wsRTO[i].getEmail()), LastName=toString(wsRTO[i].getLastName()), FirstName=toString(wsRTO[i].getFirstName()),
					SingleLicenseKey=toString(wsRTO[i].getSingleLicenseKey()), MultiLicenseKey=toString(wsRTO[i].getMultiLicenseKey())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="searchSingleLicenseByIMSI" returntype="String" hint="Recherche une single licence à partir du code IMSI et retourne la clé correspondante">
		<cfargument name="IMSI" type="String" required="true" hint="Code IMSI">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.searchSingleLicenseByIMSI(ARGUMENTS["IMSI"]))>
	</cffunction>

	<cffunction access="remote" name="deleteSingleLicense" returntype="String" hint="Supprime une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la licence">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.deleteSingleLicense(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getSingleLicenseCount" returntype="String" hint="Retourne le nombre de single licence d'une licence multiple">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.getSingleLicenseCount(ARGUMENTS["multiLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getSingleLicenseExpirationDate" returntype="String"
	hint="Retourne la date d'expiration d'une single licence au format YYYY-MM-DD. Format optionnel du time zone : EET (GMT+2, GMT+3)">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la licence">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.getSingleLicenseExpirationDate(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="getDefaultSingleLicenseCount" returntype="String" hint="Retourne le nombre de single licence dans la licence multiple par défaut">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.getDefaultSingleLicenseCount())>
	</cffunction>
	
	<cffunction access="remote" name="createSingleLicense" returntype="String" hint="Crée une single licence dans le contrat par défaut du partenaire et retourne la clé correspondante">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.createSingleLicense(
			ARGUMENTS["expirationDate"],ARGUMENTS["firstName"],ARGUMENTS["lastName"],ARGUMENTS["email"],
			ARGUMENTS["phone"],ARGUMENTS["imei"],ARGUMENTS["imsi"]
		))>
	</cffunction>
	
	<cffunction access="remote" name="createSingleLicenseInMultiLicense" returntype="String" hint="Crée une single licence dans une licence multiple et retourne la clé correspondante">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Clé de la licence multiple">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.createSingleLicenseInMultiLicense(
			ARGUMENTS["multiLicenseKey"],ARGUMENTS["firstName"],ARGUMENTS["lastName"],ARGUMENTS["email"],
			ARGUMENTS["phone"],ARGUMENTS["imei"],ARGUMENTS["imsi"]
		))>
	</cffunction>
	
	<cffunction access="remote" name="resetSingleLicenseImeiChanges" returntype="String"
	hint="Réinitialise le nombre de changement IMEI si le seuil max a été atteint. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.resetSingleLicenseImeiChanges(ARGUMENTS["singleLicenseKey"]))>
	</cffunction>
	
	<cffunction access="remote" name="updateSingleLicenseUserProperties" returntype="String" hint="Met à jour une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfargument name="firstName" type="String" required="true" hint="Prénom du souscripteur (120 caractères max)">
		<cfargument name="lastName" type="String" required="true" hint="Nom du souscripteur (120 caractères max)">
		<cfargument name="email" type="String" required="true" hint="Email du souscripteur (120 caractères max)">
		<cfargument name="phone" type="String" required="true" hint="Numéro de téléphone (e.g : +000000000000). 9 à 20 caractères">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (22 caractères max)">
		<cfargument name="imsi" type="String" required="true" hint="Code IMSI de la carte SIM (15 caractères max)">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.updateSingleLicenseUserProperties(
			ARGUMENTS["singleLicenseKey"],ARGUMENTS["firstName"],ARGUMENTS["lastName"],ARGUMENTS["email"],
			ARGUMENTS["phone"],ARGUMENTS["imei"],ARGUMENTS["imsi"]
		))>
	</cffunction>
	
	<cffunction access="remote" name="getSingleLicenseProperties" returntype="String"
	hint="Retourne les propriétés d'une single licence dans un tableau où chaque éléments contient les clés suivantes : Un tableau vide est retourné si aucune propriété n'est définie<br>
	- MultiLicenseKey : Clé de la licence multiple<br>
	- SingleLicenseKey : clé de la single licence<br>
	- FirstName : Prénom du souscripteur<br>
	- LastName : Nom du souscripteur<br>
	- Email : Email du souscripteur<br>
	- Phone : Numéro de téléphone<br>
	- Imei : Code IMEI de l'équipement<br>
	- Imsi : Code IMSI de la carte SIM<br>
	- OperatingSystem : Système de l'équipement<br>
	- ActivationDate : Date d'activation de la licence<br>
	- ExpirationDate : Date d'expiration de la licence<br>
	- NumberOfUpdateChecks : Nombre de vérification de mise à jour<br>
	- LastUpdateCheck : Timestamp de la dernière vérification de mise à jour<br>
	- LastDBUpdate : Timestamp de la dernière mise à jour de la base<br>
	- LastSWUpdate : Timestamp de la dernière mise à jour applicative<br>
	- DeviceLang : Langage de l'équipement<br>
	- ApplicationLang : Langage de l'application<br>
	- ImeiChanges : Nombre de changement d'IMEI<br>
	- ApplicationVersion : Version de l'application<br>
	- DbVersion : Version de la base<br>
	- IsApplicationUpToDate : TRUE si le client dispose de la dernière version de l'application et FALSE sinon<br>
	- IsDbUpToDate : TRUE si le client dispose de la dernière version de la base de définition des virus">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfset var wsResult=wsService.getSingleLicenseProperties(ARGUMENTS["singleLicenseKey"])>
		<cfset var wsRTO=wsResult.getSingleLicenceRTO()>
		<cfset var props=[]>
		<cfif isDefined("wsRTO")>
			<cfset rtoCount=arrayLen(wsRTO)>
			<cfloop index="i" from="1" to="#rtoCount#">
				<cfset props[i]={
					MultiLicenseKey=toString(wsRTO[i].getMultiLicenseKey()), SingleLicenseKey=toString(wsRTO[i].getSingleLicenseKey()), FirstName=toString(wsRTO[i].getFirstName()),
					LastName=toString(wsRTO[i].getLastName()), Email=toString(wsRTO[i].getEmail()), Phone=toString(wsRTO[i].getPhone()),
					Imei=toString(wsRTO[i].getImei()), Imsi=toString(wsRTO[i].getImsi()), OperatingSystem=toString(wsRTO[i].getOperatingSystem()),
					ActivationDate=toString(wsRTO[i].getActivationDate()), ExpirationDate=toString(wsRTO[i].getExpirationDate()),
					LastUpdateCheck=toString(wsRTO[i].getLastUpdateCheck()), LastDBUpdate=toString(wsRTO[i].getLastDBUpdate()), LastSWUpdate=toString(wsRTO[i].getLastSWUpdate()),
					DeviceLang=toString(wsRTO[i].getDeviceLang()), ApplicationLang=toString(wsRTO[i].getApplicationLang()), ImeiChanges=toString(wsRTO[i].getImeiChanges()),
					ApplicationVersion=toString(wsRTO[i].getApplicationVersion()), DbVersion=toString(wsRTO[i].getDbVersion()), IsApplicationUpToDate=toString(wsRTO[i].isIsApplicationUpToDate()),
					NumberOfUpdateChecks=toString(wsRTO[i].getNumberOfUpdateChecks()), IsDbUpToDate=toString(wsRTO[i].isIsDbUpToDate())
				}>
			</cfloop>
		</cfif>
		<cfreturn serializeJSON(props)>
	</cffunction>	
	
	<cffunction access="remote" name="getSingleLicenseUserProperties" returntype="String"
	hint="Retourne les infos utilisateur d'une single licence dans une structure contenant les clés suivantes : Une structure vide est retournée si aucune propriété n'est définie<br>
	- FirstName : Prénom du souscripteur<br>
	- LastName : Nom du souscripteur<br>
	- Email : Email du souscripteur<br>
	- Phone : Numéro de téléphone<br>
	- Imei : Code IMEI de l'équipement<br>
	- Imsi : Code IMSI de la carte SIM">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfset var wsResult=wsService.getSingleLicenseUserProperties(ARGUMENTS["singleLicenseKey"])>
		<cfset var props={}>
		<cfif isDefined("wsResult")>
			<cfset props={
				FirstName=toString(wsResult.getFirstName()), LastName=toString(wsResult.getLastName()), Email=toString(wsResult.getEmail()),
				Phone=toString(wsResult.getPhone()), Imei=toString(wsResult.getImei()), Imsi=toString(wsResult.getImsi())
			}>
		</cfif>
		<cfreturn serializeJSON(props)>
	</cffunction>
	
	<cffunction access="remote" name="updateSingleLicenseExpirationDate" returntype="String" hint="Met à jour la date d'expiration d'une single licence. Retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Clé de la single licence">
		<cfargument name="expirationDate" type="String" required="true" hint="Date d'expiration de la licence au format YYYY-MM-DD">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.updateSingleLicenseExpirationDate(ARGUMENTS["singleLicenseKey"],ARGUMENTS["expirationDate"]))>
	</cffunction>
	
	<cffunction access="remote" name="searchSingleLicenseByMSISDN" returntype="String" hint="Recherche la single licence correspondant au MSISDN fourni">
		<cfargument name="MSISDN" type="String" required="true" hint="MSISDN (e.g : +000000000000)">
		<cfset var wsService=getSecurityService().getSingleLicenseService()>
		<cfreturn serializeJSON(wsService.searchSingleLicenseByMSISDN(ARGUMENTS["MSISDN"]))>
	</cffunction>
</cfcomponent>