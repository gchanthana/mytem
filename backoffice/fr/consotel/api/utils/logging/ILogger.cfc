<cfinterface displayName="fr.consotel.api.utils.logging.ILogger" hint="Logger utility interface">
	<!--- LOGGING UTILITY INTERFACE (Author : Cedric)
		- AVAILABLE LEVELS : CFDUMP result of getLogger to get level public available values
		- Log filename and location : Depends on implementing classes
	 	- Best Practice : Use CFTRACE before calling these methods to get FILENAME, LINE and COLUMN NUMBER in log files
	 --->
	
	<cffunction access="public" name="getLogger" returntype="fr.consotel.api.utils.logging.ILogger" hint="Find or create a logger for a named subsystem">
		<cfargument name="name" type="string" required="true" hint="A name for the logger">
	</cffunction>

	<cffunction access="public" name="getName" returntype="string" hint="Get the name for this logger">
	</cffunction>
	
	<cffunction access="public" name="logInfo" returntype="void" hint="Log an INFORMATION type message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog)">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object">
	</cffunction>
	
	<cffunction access="public" name="logWarning" returntype="void" hint="Log a WARNING message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog)">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object">
	</cffunction>
	
	<cffunction access="public" name="logError" returntype="void" hint="Log an ERROR message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog)">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object">
	</cffunction>
	
	<cffunction access="public" name="logFatalError" returntype="void" hint="Log a FATAL type message, with one object parameter">
		<cfargument name="level" type="string" required="true" hint="Log level according to Logger (SEVERE,WARNING,INFO,CONFIG,FINE,FINER,FINEST)">
		<cfargument name="msg" type="string" required="true" hint="The string message (or a key in the message catalog)">
		<cfargument name="paramObject" type="any" required="false" hint="Parameter Object">
	</cffunction>
</cfinterface>