package entity
{
	[Bindable]
	public class SujetFormationVO
	{
		private var _id:int;
		private var _libelle:String;
		private var _description:String;
		
		public function SujetFormationVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_SUJET_FORMATION"))
					this.id = obj.IDFC_SUJET_FORMATION;
				
				if(obj.hasOwnProperty("SUJET_FORMATION"))
					this.libelle = obj.SUJET_FORMATION;
				
				if(obj.hasOwnProperty("DESCRIPTION_SUJET_FORMATION"))
					this.description = obj.DESCRIPTION_SUJET_FORMATION;
				
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet SujetFormationVO erroné ");
			}
			
		}
		
		public function get description():String { return _description; }
		
		public function set description(value:String):void
		{
			if (_description == value)
				return;
			_description = value;
		}
		
		
		public function get libelle():String { return _libelle; }
		
		public function set libelle(value:String):void
		{
			if (_libelle == value)
				return;
			_libelle = value;
		}
		
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
	}
}