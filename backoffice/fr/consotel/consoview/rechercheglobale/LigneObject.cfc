<cfcomponent displayname="LigneObject" output="false" alias="fr.consotel.consoview.rechercheglobale.LigneObject">

	<cfproperty name="OPERATEUR" displayname="OPERATEUR" hint="le nom de l'opérateur" type="string" default="""" />
	<cfproperty name="OPERATEURID" displayname="OPERATEURID" hint="l'identifiant de l'opérateur" type="numeric" default="0" />
	<cfproperty name="COMPTE" displayname="COMPTE" hint="Le compte de facturation" type="string" />
	<cfproperty name="SOUS_COMPTE" displayname="SOUS_COMPTE" hint="Le compte de rattachement" type="string" default="""" />
	<cfproperty name="SOUS_TETE" displayname="SOUS_TETE" hint="la sous-tête" type="string" default="""" />
	<cfproperty name="IDSOUS_TETE" displayname="IDSOUS_TETE" hint="l'identifiant de la sous-tête" type="string" default="0" />
	<cfproperty name="FONCTION" displayname="FONCTION" hint="la fonction de la sous-tête" type="string" default="""" />
	
	<cfproperty name="NOM_SITE_INSTALL" displayname="NOM_SITE_INSTALL" hint="le nom du site d'installation" type="string" default="""" />
	<cfproperty name="ADRESSE1_INSTALL" displayname="ADRESSE1_INSTALL" hint="l'adresse du site d'installation" type="string" default="""" />
	<cfproperty name="ZIPCODE_INSTALL" displayname="ZIPCODE_INSTALL" hint="le code postal du site d'installation" type="string" default="""" />
	<cfproperty name="VILLE_INSTALL" displayname="VILLE_INSTALL" hint="la ville du site d'installation" type="string" default="""" />
		
	<cfproperty name="SITE" displayname="SITE" hint="le nom du site" type="string" default="""" />
	<cfproperty name="ADRESSE1" displayname="ADRESSE1" hint="l'adresse du site" type="string" default="""" />
	<cfproperty name="ZIPCODE" displayname="ZIPCODE" hint="le code postal du site" type="string" default="""" />
	<cfproperty name="COMMUNE" displayname="COMMUNE" hint="la ville du site" type="string" default="""" />

	<cfproperty name="BY_ADRESSE1" displayname="BY_ADRESSE1" hint="l'adresse du site de facturation" type="string" default="""" />
	<cfproperty name="BY_ZIPCODE" displayname="BY_ZIPCODE" hint="le code postal du site de facturation" type="string" default="""" />
	<cfproperty name="BY_COMMUNE" displayname="BY_COMMUNE" hint="la ville du site de facturation" type="string" default="""" />
	
	<cfproperty name="AUTRE" displayname="AUTRE" hint="la ville du site de facturation" type="string" default="""" />
	
	<cfscript>
		//Initialise le CFC avec les valeurs par defauts
		variables.OPERATEUR = "";
		variables.OPERATEURID = 0;
		variables.COMPTE = "";
		variables.SOUS_COMPTE = "";
		variables.SOUS_TETE = "";
		variables.IDSOUS_TETE = 0;
		variables.FONCTION = "";
		
		
		variables.NOM_SITE_INSTALL = "";		
		variables.ADRESSE1_INSTALL = "";
		variables.ZIPCODE_INSTALL = "";
		variables.VILLE_INSTALL = "";	

		variables.SITE = "";		
		variables.ADRESSE1 = "";
		variables.ZIPCODE = "";
		variables.COMMUNE = "";
		
		
		variables.BY_ADRESSE1 = "";
		variables.BY_ZIPCODE = "";
		variables.BY_COMMUNE = "";		
		
		variables.AUTRE = "";
	</cfscript>
	
	<cffunction name="init" hint="Initialise le composant" access="public" output="false" returntype="LigneObject">	
		<cfargument name="params" required="false" type="any">
		<cfscript>			
			load(params);			
			return this;
		</cfscript>
	</cffunction>

	
	<cffunction name="getOPERATEUR" access="public" output="false" returntype="string">
		<cfreturn variables.OPERATEUR />
	</cffunction>

	<cffunction name="setOPERATEUR" access="public" output="false" returntype="void">
		<cfargument name="OPERATEUR" type="string" required="true" />
		<cfset variables.OPERATEUR = arguments.OPERATEUR />
		<cfreturn />
	</cffunction>

	<cffunction name="getOPERATEURID" access="public" output="false" returntype="numeric">
		<cfreturn variables.OPERATEURID />
	</cffunction>

	<cffunction name="setOPERATEURID" access="public" output="false" returntype="void">
		<cfargument name="OPERATEURID" type="numeric" required="true" />
		<cfset variables.OPERATEURID = arguments.OPERATEURID />
		<cfreturn />
	</cffunction>

	<cffunction name="getCOMPTE" access="public" output="false" returntype="string">
		<cfreturn variables.COMPTE />
	</cffunction>

	<cffunction name="setCOMPTE" access="public" output="false" returntype="void">
		<cfargument name="COMPTE" type="string" required="true" />
		<cfset variables.COMPTE = arguments.COMPTE />
		<cfreturn />
	</cffunction>

	<cffunction name="getSOUS_COMPTE" access="public" output="false" returntype="string">
		<cfreturn SOUS_COMPTE />
	</cffunction>

	<cffunction name="setSOUS_COMPTE" access="public" output="false" returntype="void">
		<cfargument name="SOUS_COMPTE" type="string" required="true" />
		<cfset variables.SOUS_COMPTE = arguments.SOUS_COMPTE />
		<cfreturn />
	</cffunction>

	<cffunction name="getSOUS_TETE" access="public" output="false" returntype="string">
		<cfreturn variables.SOUS_TETE />
	</cffunction>

	<cffunction name="setSOUS_TETE" access="public" output="false" returntype="void">
		<cfargument name="SOUS_TETE" type="string" required="true" />
		<cfset variables.SOUS_TETE = arguments.SOUS_TETE />
		<cfreturn />
	</cffunction>

	<cffunction name="getIDSOUS_TETE" access="public" output="false" returntype="string">
		<cfreturn variables.IDSOUS_TETE />
	</cffunction>

	<cffunction name="setIDSOUS_TETE" access="public" output="false" returntype="void">
		<cfargument name="IDSOUS_TETE" type="string" required="true" />
		<cfset variables.IDSOUS_TETE = arguments.IDSOUS_TETE />
		<cfreturn />
	</cffunction>

	<cffunction name="getFONCTION" access="public" output="false" returntype="string">
		<cfreturn variables.FONCTION />
	</cffunction>

	<cffunction name="setFONCTION" access="public" output="false" returntype="void">
		<cfargument name="FONCTION" type="string" required="true" />
		<cfset variables.FONCTION = arguments.FONCTION />
		<cfreturn/>
	</cffunction>
	
	
    <!--- Usage: GetNOM_SITE_INSTALL / SetNOM_SITE_INSTALL methods for NOM_SITE_INSTALL value --->
    <cffunction name="getNOM_SITE_INSTALL" access="public" output="false" returntype="string">
       <cfreturn variables.instance.NOM_SITE_INSTALL />
    </cffunction>

    <cffunction name="setNOM_SITE_INSTALL" access="public" output="false" returntype="void">
       <cfargument name="NOM_SITE_INSTALL" type="string" required="true" />
       <cfset variables.instance.NOM_SITE_INSTALL = arguments.NOM_SITE_INSTALL />
    </cffunction>
	
	
    <!--- Usage: GetADRESSE1_INSTALL / SetADRESSE1_INSTALL methods for ADRESSE1_INSTALLs value --->
    <cffunction name="getADRESSE1_INSTALL" access="public" output="false" returntype="string">
       <cfreturn variables.instance.ADRESSE1_INSTALL />
    </cffunction>

    <cffunction name="setADRESSE1_INSTALL" access="public" output="false" returntype="void">
       <cfargument name="ADRESSE1_INSTALL" type="string" required="true" />
       <cfset variables.instance.ADRESSE1_INSTALL = arguments.ADRESSE1_INSTALL />
    </cffunction> 


    <!--- Usage: GetZIPCODE_INSTALL / SetZIPCODE_INSTALL methods for ZIPCODE_INSTALL value --->
    <cffunction name="getZIPCODE_INSTALL" access="public" output="false" returntype="string">
       <cfreturn variables.instance.ZIPCODE_INSTALL />
    </cffunction>

    <cffunction name="setZIPCODE_INSTALL" access="public" output="false" returntype="void">
       <cfargument name="ZIPCODE_INSTALL" type="string" required="true" />
       <cfset variables.instance.ZIPCODE_INSTALL = arguments.ZIPCODE_INSTALL />
    </cffunction>


    <!--- Usage: GetVILLE_INSTALL / SetVILLE_INSTALL methods for VILLE_INSTALL value --->
    <cffunction name="getVILLE_INSTALL" access="public" output="false" returntype="string">
       <cfreturn variables.instance.VILLE_INSTALL />
    </cffunction>

    <cffunction name="setVILLE_INSTALL" access="public" output="false" returntype="void">
       <cfargument name="VILLE_INSTALL" type="string" required="true" />
       <cfset variables.instance.VILLE_INSTALL = arguments.VILLE_INSTALL />
    </cffunction>
	
	
    <!--- Usage: GetSITE / SetSITE methods for SITE value --->
    <cffunction name="getSITE" access="public" output="false" returntype="string">
       <cfreturn variables.instance.SITE />
    </cffunction>

    <cffunction name="setSITE" access="public" output="false" returntype="void">
       <cfargument name="SITE" type="string" required="true" />
       <cfset variables.instance.SITE = arguments.SITE />
    </cffunction>
	

    <!--- Usage: GetADRESSE1 / SetADRESSE1 methods for ADRESSE1 value --->
    <cffunction name="getADRESSE1" access="public" output="false" returntype="string">
       <cfreturn variables.instance.ADRESSE1 />
    </cffunction>

    <cffunction name="setADRESSE1" access="public" output="false" returntype="void">
       <cfargument name="ADRESSE1" type="string" required="true" />
       <cfset variables.instance.ADRESSE1 = arguments.ADRESSE1 />
    </cffunction>


    <!--- Usage: GetZIPCODE / SetZIPCODE methods for ZIPCODE value --->
    <cffunction name="getZIPCODE" access="public" output="false" returntype="string">
       <cfreturn variables.instance.ZIPCODE />
    </cffunction>

    <cffunction name="setZIPCODE" access="public" output="false" returntype="void">
       <cfargument name="ZIPCODE" type="string" required="true" />
       <cfset variables.instance.ZIPCODE = arguments.ZIPCODE />
    </cffunction>
	

    <!--- Usage: GetCOMMUNE / SetCOMMUNE methods for COMMUNE value --->
    <cffunction name="getCOMMUNE" access="public" output="false" returntype="string">
       <cfreturn variables.instance.COMMUNE />
    </cffunction>

    <cffunction name="setCOMMUNE" access="public" output="false" returntype="void">
       <cfargument name="COMMUNE" type="string" required="true" />
       <cfset variables.instance.COMMUNE = arguments.COMMUNE />
    </cffunction>


    <!--- Usage: GetBY_ADRESSE1 / SetBY_ADRESSE1 methods for BY_ADRESSE1 value --->
    <cffunction name="getBY_ADRESSE1" access="public" output="false" returntype="string">
       <cfreturn variables.instance.BY_ADRESSE1 />
    </cffunction>

    <cffunction name="setBY_ADRESSE1" access="public" output="false" returntype="void">
       <cfargument name="BY_ADRESSE1" type="string" required="true" />
       <cfset variables.instance.BY_ADRESSE1 = arguments.BY_ADRESSE1 />
    </cffunction>


    <!--- Usage: GetBY_ZIPCODE / SetBY_ZIPCODE methods for BY_ZIPCODE value --->
    <cffunction name="getBY_ZIPCODE" access="public" output="false" returntype="string">
       <cfreturn variables.instance.BY_ZIPCODE />
    </cffunction>

    <cffunction name="setBY_ZIPCODE" access="public" output="false" returntype="void">
       <cfargument name="BY_ZIPCODE" type="string" required="true" />
       <cfset variables.instance.BY_ZIPCODE = arguments.BY_ZIPCODE />
    </cffunction>
	


    <!--- Usage: GetBY_COMMUNE / SetBY_COMMUNE methods for BY_COMMUNE value --->
    <cffunction name="getBY_COMMUNE" access="public" output="false" returntype="string">
       <cfreturn variables.instance.BY_COMMUNE />
    </cffunction>

    <cffunction name="setBY_COMMUNE" access="public" output="false" returntype="void">
       <cfargument name="BY_COMMUNE" type="string" required="true" />
       <cfset variables.instance.BY_COMMUNE = arguments.BY_COMMUNE />
    </cffunction>
	
	
	  <!--- Usage: GetBY_COMMUNE / SetBY_COMMUNE methods for BY_COMMUNE value --->
    <cffunction name="getAUTRE" access="public" output="false" returntype="string">
       <cfreturn variables.instance.AUTRE />
    </cffunction>

    <cffunction name="setAUTRE" access="public" output="false" returntype="void">
       <cfargument name="AUTRE" type="string" required="true" />
       <cfset variables.instance.AUTRE = arguments.AUTRE />
    </cffunction>


	<cffunction name="load" displayname="load" hint="load" access="public" output="true" returntype="void">
		<cfargument name="params" required="true" >
		
		<cfscript>
			//Initialise le CFC avec les valeurs par defauts
			setOPERATEUR(params.NOM[params.currentRow]);
			setOPERATEURID(params.OPERATEURID[params.currentRow]);
			setCOMPTE(params.COMPTE_FACTURATION[params.currentRow]);
			setSOUS_COMPTE(params.SOUS_COMPTE[params.currentRow]);
			setSOUS_TETE(params.SOUS_TETE[params.currentRow]);
			setIDSOUS_TETE(params.IDSOUS_TETE[params.currentRow]);
			setFONCTION(params.USAGES[params.currentRow]);	
			
			setNOM_SITE_INSTALL(params.NOM_SITE_INSTALL[params.currentRow]);			
			setADRESSE1_INSTALL(params.ADRESSE1_INSTALL[params.currentRow]);
			setZIPCODE_INSTALL(params.ZIPCODE_INSTALL[params.currentRow]);
			setVILLE_INSTALL(params.VILLE_INSTALL[params.currentRow]);	
			
			setSITE(params.NOM_SITE[params.currentRow]);			
			setADRESSE1(params.ADRESSE1[params.currentRow]);
			setZIPCODE(params.ZIPCODE[params.currentRow]);
			setCOMMUNE(params.COMMUNE[params.currentRow]);	
			
			
			setBY_ADRESSE1(params.BY_ADRESSE1[params.currentRow]);
			setBY_ZIPCODE(params.BY_ZIPCODE[params.currentRow]);
			setBY_COMMUNE(params.BY_COMMUNE[params.currentRow]);

			setAUTRE(params.COMMENTAIRES[params.currentRow]);
			
		</cfscript>
			
		<cfreturn />
	</cffunction>
</cfcomponent>