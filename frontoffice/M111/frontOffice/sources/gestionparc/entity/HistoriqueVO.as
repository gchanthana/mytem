package gestionparc.entity
{
	import mx.resources.ResourceManager;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Value Object représentant les propriétés pour un historique.
	 *
	 */
	[Bindable]
	public class HistoriqueVO
	{
		public var idHistorique:int=-1;

		public var dateEffet:Date=null;

		public var description:String="";

		public var qui:String="";
		public var quand:Date=null;

		public function fillHistoriqueVO(obj:Object):HistoriqueVO
		{
			var myNewHisto:HistoriqueVO=new HistoriqueVO();

			myNewHisto.idHistorique=obj.IDINV_OP_ACTION;
			myNewHisto.dateEffet=obj.DATE_ACTION;
			myNewHisto.qui=obj.UTILISATEUR;
			myNewHisto.quand=obj.DATE_INSERT;
			if (obj.DANS_INVENTAIRE == 1 && obj.LIBELLE_ETAT == "Initial")
			{
				myNewHisto.description=ResourceManager.getInstance().getString('M111', 'Le_produit_est_entr__dans_l_inventaire');
			}
			else if (obj.DANS_INVENTAIRE == 0 && obj.LIBELLE_ETAT == "Initial")
			{
				myNewHisto.description=ResourceManager.getInstance().getString('M111', 'Le_produit_est_sorti_dans_l_inventaire');
			}
			else
			{
				myNewHisto.description=obj.LIBELLE_ETAT;
			}

			return myNewHisto;
		}

		public function fillHistoriqueFiche(obj:Object):HistoriqueVO
		{
			var myNewHisto:HistoriqueVO=new HistoriqueVO();

			myNewHisto.dateEffet=obj.DATE_ACTION;
			myNewHisto.qui=obj.NOM_AUTEUR;
			myNewHisto.quand=obj.DATE_ACTION;
			myNewHisto.description=obj.WORD1 + ' ' + obj.ITEM1 + ' ' + obj.WORD2 + ' ' + obj.ITEM2;

			if (obj.hasOwnProperty("LIBELLE_CAUSE") && obj.LIBELLE_CAUSE != null && obj.LIBELLE_CAUSE != "")
				myNewHisto.description+=" - " + obj.LIBELLE_CAUSE;

			if (obj.hasOwnProperty("COMMENTAIRES") && obj.COMMENTAIRES != null && obj.COMMENTAIRES != "")
				myNewHisto.description+=" (" + obj.COMMENTAIRES + ")";

			return myNewHisto;
		}
	}
}
