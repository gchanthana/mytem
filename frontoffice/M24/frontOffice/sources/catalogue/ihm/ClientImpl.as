package catalogue.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import catalogue.AbstractCatalogue;
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.cataloguemodele.CreateModeleIHM;
	import catalogue.ihm.fiche.DetailProduitIHM;
	import catalogue.ihm.fiche.FicheEquipementIHM;
	import catalogue.ihm.importequipement.ImportSourceIsRevendeurIHM;
	import catalogue.ihm.parametre.UpdateTarifEquipementClientIHM;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.ihm.typecommande.ListeTypeCommandeIHM;
	import catalogue.profil.GestionProfil;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.util.CatalogueFormatteur;
	import catalogue.util.Currency;
	import catalogue.util.DataGridDataExporter;
	import catalogue.vo.ClasseVO;
	import catalogue.vo.ClientCatalogueVO;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	import catalogue.vo.RevendeurVO;
	import catalogue.vo.SegmentVO;
	import catalogue.vo.TypeCommandeVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import paginatedatagrid.columns.event.StandardColonneEvent;
	import paginatedatagrid.event.PaginateDatagridEvent;
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;
	
	[Bindable]
	public class ClientImpl extends AbstractCatalogue
	{
		/**
		 * 
		 */		
		//Public UI componant
		public var comboClient : ComboBox;
		public var comboRevendeur : ComboBox;
		public var comboConstructeur : ComboBox;
		public var comboSegment : ComboBox;
		public var comboEtat : ComboBox;
		
		public var comboTypeCMD : ComboBox;
		public var labelClient : Label;
		public var popUpdateEquipement : UpdateTarifEquipementClientIHM;
		private var _popDetailEquipement : FicheEquipementIHM;
		public var labelRevendeur : Label;
		public var boxDistrib : HBox;
		public const NBITEMPARPAGE : int = 30;
		public var btCreateEquipement : Button;
		public var btCreateModele : Button;
		private var popImport : ImportSourceIsRevendeurIHM; 
		private var _popUpFilter		:PopupFilterIHM;
		public var listFilter:ArrayCollection;
		public var currenciesAreDifferent:Boolean = false;
		public var globalCatalogueService : GlobalCatalogueService = new GlobalCatalogueService();
		
		public function ClientImpl()
		{
		}
		public function initData():void
		{
			//Listerner
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR,comboClientDataChangeHandler);
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,liste_constructeur_complete_handler);
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);
			//Remoting
			catalogueCS.rechercheContructeur(true);
			catalogueCS.rechercheClasseEquipement(true);
			
			//Util
			thisComponentIsComplete = true;
			//INIT IHM
			updateProfil();
			//Par defaut, le tri se fait sur le modèle:			
			mySearchPaginateDatagrid.comboRecherche.selectedIndex = 6; // Sélectionne modèle dans la combo Clé de recherche
			mySearchPaginateDatagrid.comboTrieColonne.selectedIndex = 6; // Sélectionne modèle dans la combo ORDERBY
			
			myPaginatedatagrid.dgPaginate.rowColorFunction=selectColor; // selection des couleurs des lignes de la dataGrid
		} 
		
		private function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if (item.DATEVALIDITE_CLT != null && DateFunction.dateDiff("d",item.DATEVALIDITE_CLT,new Date()) +1 < 0 ) // fin de vie	
				{	
					rColor = 0xFF0000;
				}
				else if (item.SUSPENDU_CLT == 1)//En rupture
				{	
					rColor = 0xF7FF04;
				}
				else if (item.DATEVALIDITE_CLT != null) 	//Bientot fin de vie
				{	 
					rColor = 0xFEC46D;
				}				
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		public function btImgChooseConstructeur_clickHandler(e:Event):void
		{
			var listconstucteurs:ArrayCollection = catalogueCS.listeConstructeur as ArrayCollection;
			this.refreshlistFilter(listconstucteurs,1)
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_constructeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		public function btImgChooseDistributeur_clickHandler(e:Event):void
		{
			var listDistributeur:ArrayCollection = catalogueCS.listeRevendeur as ArrayCollection;
			this.refreshlistFilter(listDistributeur,2)
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_distributeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		public function btImgChooseClient_clickHandler(e:Event):void
		{
			var listClient:ArrayCollection = catalogueCS.listeClient as ArrayCollection;
			this.refreshlistFilter(listClient,3)
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.listeFilter = listFilter;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_client');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', getItemFilterHandler2);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		protected function getItemFilterHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler);
			refreshComboConstructeur();
		}
		protected function getItemFilterHandler1(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler1);
			refreshComboDistributeur();
		}
		
		protected function getItemFilterHandler2(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', getItemFilterHandler2);
			refreshComboClient();
		}
		
		
		
		private function majSelectConstructeur():void
		{
			for (var i:int=0; i<catalogueCS.listeConstructeur.length;i++)
			{
				(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =false;
				if ((catalogueCS.listeConstructeur[i] as ConstructeurVO).ID_CONSTRUCTEUR == (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR)
				{
					(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =true;
				}
			}
		}
		
		private function majSelectDistributeur():void
		{
			for (var i:int=0; i<catalogueCS.listeRevendeur.length;i++)
			{
				(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =false;
				if ((catalogueCS.listeRevendeur[i] as RevendeurVO).ID_REVENDEUR == (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR)
				{
					(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =true;
				}
			}
		}
		
		private function majSelectClient():void
		{
			for (var i:int=0; i<catalogueCS.listeClient.length;i++)
			{
				(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =false;
				if ((catalogueCS.listeClient[i] as ClientCatalogueVO).ID_CLIENT == (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT)
				{
					(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =true;
				}
			}
		}
		
		
		private function refreshComboConstructeur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeConstructeur.length;i++)
			{
				(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =false;
				if ((catalogueCS.listeConstructeur[i] as ConstructeurVO).ID_CONSTRUCTEUR == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeConstructeur[i] as ConstructeurVO).SELECTED =true;
					comboConstructeur.selectedIndex = i;
				}
			}
		}
		
		private function refreshComboDistributeur():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeRevendeur.length;i++)
			{
				(catalogueCS.listeRevendeur[i] as RevendeurVO ).SELECTED =false;
				if ((catalogueCS.listeRevendeur[i] as RevendeurVO).ID_REVENDEUR == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeRevendeur[i] as RevendeurVO).SELECTED =true;
					comboRevendeur.selectedIndex = i;
				}
			}
			comboRevendeurHandler();
		}
		
		private function refreshComboClient():void
		{
			var k:int;
			k=_popUpFilter.getItemCurrentID();
			for (var i:int=0; i<catalogueCS.listeClient.length;i++)
			{
				(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =false;
				if ((catalogueCS.listeClient[i] as ClientCatalogueVO).ID_CLIENT == _popUpFilter.getItemCurrentID())
				{
					(catalogueCS.listeClient[i] as ClientCatalogueVO).SELECTED =true;
					comboClient.selectedIndex = i;					
				}
			}
			comboClientHandler();
		}
		
		private function refreshlistFilter(listdata:ArrayCollection, dataFilter:int):void
		{
			listFilter =new ArrayCollection();
			switch (dataFilter)
			{
				case 1:
					for (var i:int=0; i<listdata.length;i++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(i) as ConstructeurVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(i) as ConstructeurVO).ID_CONSTRUCTEUR;
						item.LIBELLE_ITEM = (listdata.getItemAt(i) as ConstructeurVO).LIBELLE_CONSTRUCTEUR;
						listFilter.addItem(item);
					}
					break;
				case 2:
					for (var j:int=0; j<listdata.length;j++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(j) as RevendeurVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(j) as RevendeurVO).ID_REVENDEUR;
						item.LIBELLE_ITEM = (listdata.getItemAt(j) as RevendeurVO).LIBELLE_REVENDEUR;
						listFilter.addItem(item);
					}
					break;
				case 3:
					for (var j:int=0; j<listdata.length;j++)
					{
						var item:Object = new Object();
						item.SELECTED = (listdata.getItemAt(j) as ClientCatalogueVO).SELECTED;
						item.IDITEM = (listdata.getItemAt(j) as ClientCatalogueVO).ID_CLIENT;
						item.LIBELLE_ITEM = (listdata.getItemAt(j) as ClientCatalogueVO).LIBELLE_CLIENT;
						listFilter.addItem(item);
					}
					break;
				default:
			}
			
		}
		
		
		public function comboClientDataChangeHandler(evt : CatalogueEvent):void
		{
			if(comboClient.dataProvider)
			{
				if((comboClient.dataProvider as ArrayCollection).length==1)
				{
					comboClient.selectedIndex = 0;
					comboClientHandler();
				}
			}
		}
		public function comboNiveauHandler(evt : Event):void
		{
			
			var combo : ComboBox = evt.target as ComboBox;
			if(combo.selectedItem)
			{
				catalogueCS.updateNiveau((myPaginatedatagrid.selectedItem as EquipementCatalogueVO).IDEQUIPEMENT,(comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT,Number(combo.selectedItem.toString()));
			}
		}
		public function comboActionHandler(event : PaginateDatagridEvent):void
		{
			if(comboClient.selectedItem) 
			{
				if(myPaginatedatagrid.dataprovider.length>0)
				{
					btExportHandler(event,(comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M24', '_Aucun__quipement_s_l_ctionn__'));
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', '_Un_client_doit__tre_selectionn__'));
			}
		}
		/**
		 * 
		 * La function showDetailHandler est redéfinit car showDetailHandler atend un idEquipFournisseur
		 * Et non un idEquipCLient
		 * 
		 **/
		override public function showDetailHandler(equip : Object):void
		{		
			//arf l'as3 fait  du passage par référence et non par valeur.
			
			var idEquiFournisseur : Number = (equip as EquipementCatalogueVO).IDEQUIPFOURNISSEUR;
			registerClassAlias("catalogue.vo.EquipementCatalogueVO",EquipementCatalogueVO);
			var customEquip : EquipementCatalogueVO = EquipementCatalogueVO(ObjectUtil.copy(equip));
			customEquip.IDEQUIPEMENT = idEquiFournisseur;
			
			var pop : DetailProduitIHM = new DetailProduitIHM();
			pop.equipement = customEquip;
			PopUpManager.addPopUp(pop,this,true);
			PopUpManager.centerPopUp(pop);
		}
		override public function rechercher(parametresRechercheVO : ParametresRechercheVO, itemIntervalleVO : ItemIntervalleVO = null):void
		{
			
			if(comboClient.selectedItem && comboRevendeur.selectedItem){
				//Selectionne 'tous'
				if(comboClasse.selectedIndex==-1)
				{
					comboClasse.selectedIndex = 0;
				}
				if(comboTypeCMD.selectedIndex==-1)
				{
					
					comboTypeCMD.selectedIndex = 0;
				}
				comboTypeHandler();
							
				if(!itemIntervalleVO) // Si c'est une nouvelle recherche
				{
					myPaginatedatagrid.refreshPaginateDatagrid(true);
					itemIntervalleVO = new ItemIntervalleVO();
					itemIntervalleVO.indexDepart = 0;
					itemIntervalleVO.tailleIntervalle = NBITEMPARPAGE;
				}
				//Ajout des 3 paramêtres spécifique au catalogue :
				var parametreSearchCatalogueVO : ParametreSearchCatalogueVO = new ParametreSearchCatalogueVO();
				parametreSearchCatalogueVO.IDCLASSE =(comboClasse.selectedItem as ClasseVO).ID_CLASSE;
				parametreSearchCatalogueVO.IDETAT = comboEtat.selectedIndex;
				parametreSearchCatalogueVO.IDCONSTRUCTEUR =(comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;
			
				parametreSearchCatalogueVO.IDREVENDEUR =(comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
				parametreSearchCatalogueVO.IDTYPEEQUIPEMENT =(comboType.selectedItem as TypeEquipementVO).ID_TYPE;
				parametreSearchCatalogueVO.ORDER_BY=parametresRechercheVO.ORDER_BY;
				parametreSearchCatalogueVO.SEARCH_TEXT=parametresRechercheVO.SEARCH_TEXT;
				parametreSearchCatalogueVO.GROUPE_INDEX =groupeIndex;
				parametreSearchCatalogueVO.IDCLIENT =(comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
				parametreSearchCatalogueVO.IDTYPECMD =(comboTypeCMD.selectedItem as TypeCommandeVO).idTypeCmd;
				parametreSearchCatalogueVO.IDSEGMENT =(comboSegment.selectedItem as SegmentVO).IDSEGMENT;
				catalogueCS.rechercheListeDataCatalogue(parametreSearchCatalogueVO,itemIntervalleVO);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Un_client_et_un_distributeur_doivent__tr'),ResourceManager.getInstance().getString('M24', 'Attention'));	
			}
		}
		public function importEquipementHandler(evt : MouseEvent):void
		{
			if(comboRevendeur.selectedItem && comboClient.selectedItem){
				popImport = new ImportSourceIsRevendeurIHM();
				popImport.titre = ResourceManager.getInstance().getString('M24', 'Import_d__quipements_')+(comboRevendeur.selectedItem as RevendeurVO).LIBELLE_REVENDEUR+ResourceManager.getInstance().getString('M24', '_au_catalogue_client')
				popImport.idRevendeur = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
				popImport.idClient = (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
				addPopImport();
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Un_client_et_un_distributeur_doivent__tr'),ResourceManager.getInstance().getString('M24', 'Attention'));	
			}
			
		}
		private function addPopImport():void
		{
			popImport.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,addEquipementCompleteHandler);
			PopUpManager.addPopUp(popImport,this,true);
			PopUpManager.centerPopUp(popImport);
		}
		
		public function liste_constructeur_complete_handler(evt : CatalogueEvent):void
		{
			comboConstructeur.selectedIndex = 0;
		}
		override public function updateProfil():void
		{
			if(thisComponentIsComplete){
				
				mySearchPaginateDatagrid.enabled=false;
				btCreateEquipement.enabled = false;
				btCreateModele.enabled = false;
				
				switch (gestionProfil.CURRENT_PROFIL)
				{
					case (GestionProfil.CONSOTEL_PROFIL):
						
						comboClientVisible(true);
						comboRevendeurVisible(true);
						catalogueCS.rechercheClient(-1);
						comboRevendeur.enabled = false;
					
					break;
					case (GestionProfil.DISTRIBUTEUR_PROFIL):
										
						comboRevendeurVisible(false);
						comboClientVisible(true);
						catalogueCS.rechercheClient(gestionProfil.ID_CURRENT_PROFIL);
											
					break;
					case (GestionProfil.CLIENT_PROFIL):
						
						comboClientVisible(false);
						comboRevendeurVisible(true);
						catalogueCS.rechercheRevendeur(gestionProfil.ID_CURRENT_PROFIL);
						globalCatalogueService.getTypeCommandeOfClient(gestionProfil.ID_CURRENT_PROFIL);
											
					break;
				}
			}
		}
		
		private function listeClasseEquipementCompleteHandler(evt:CatalogueEvent):void
		{
			if(catalogueCS.hasEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE))
			{
				catalogueCS.removeEventListener(CatalogueEvent.LISTE_CLASSE_EQUIPEMENT_COMPLETE,listeClasseEquipementCompleteHandler);	
			}
			
			comboClasseHandler();	
		}
		
		public function btCreateModeleHandler(event:MouseEvent):void
		{
			var pop : CreateModeleIHM = new CreateModeleIHM();
			pop.idClient = (comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT;
			pop.idDistrib = (comboRevendeur.selectedItem as RevendeurVO).ID_REVENDEUR;
			//pop.typeEquip = 1; //tague le modèle de type cat équipement
			PopUpManager.addPopUp(pop,this,true);
			PopUpManager.centerPopUp(pop);
		}
			
		private function comboRevendeurVisible(bool : Boolean):void
		{
			boxDistrib.visible = bool;
			boxDistrib.includeInLayout = bool;
			
			if(bool)
			{
				comboRevendeur.selectedIndex = -1;
				comboRevendeur.enabled = true;
				comboRevendeur.visible = true;
				comboRevendeur.includeInLayout = true;
				labelRevendeur.text = "";
			}
			else
			{
				var rev : RevendeurVO = new RevendeurVO();
				rev.ID_REVENDEUR = gestionProfil.ID_CURRENT_PROFIL;
				rev.LIBELLE_REVENDEUR = gestionProfil.LIBELLE_CURRENT_PROFIL;
				catalogueCS.listeRevendeur.removeAll();
				catalogueCS.listeRevendeur.addItem(rev);
				comboRevendeur.selectedIndex = 0;
				//comboRevendeur.enabled = false;
				comboRevendeur.visible = false;
				comboRevendeur.includeInLayout = false;
				labelRevendeur.text = rev.LIBELLE_REVENDEUR;
				
			}
		}
		private function comboClientVisible(bool : Boolean):void
		{
			if(bool)
			{
				comboClient.selectedIndex = -1;
				comboClient.enabled = true;
				comboClient.visible = true;
				comboClient.includeInLayout = true;
				labelClient.text = "";
			}
			else
			{
				var cli : ClientCatalogueVO = new ClientCatalogueVO();
				cli.ID_CLIENT = gestionProfil.ID_CURRENT_PROFIL;
				cli.LIBELLE_CLIENT = gestionProfil.LIBELLE_CURRENT_PROFIL;
				catalogueCS.listeClient.removeAll();
				catalogueCS.listeClient.addItem(cli);
				comboClient.selectedIndex = 0;
				comboClient.enabled = false;
				comboClient.visible = false;
				comboClient.includeInLayout = false;
				labelClient.text = cli.LIBELLE_CLIENT;
			}
		}
		public function comboConstructeurHandler():void
		{
			if(comboConstructeur.selectedIndex == -1)
			{
				comboConstructeur.selectedIndex = 0;
			}
			majSelectConstructeur();
		}
		public function comboClientHandler():void
		{
			
			majSelectClient();	
			if(gestionProfil.CURRENT_PROFIL != GestionProfil.DISTRIBUTEUR_PROFIL)
			{
				if(comboClient.selectedItem != null)
				{
					catalogueCS.rechercheRevendeur((comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
					comboRevendeur.enabled = true;
				}
				else
				{
					comboRevendeur.enabled = false;
					comboRevendeur.selectedIndex = -1;
				}
			}
			
			if(comboClient.selectedItem != null)
			{
				globalCatalogueService.getTypeCommandeOfClient((comboClient.selectedItem as ClientCatalogueVO).ID_CLIENT);
			}
			
			if(comboRevendeur.selectedIndex != -1 && comboClient.selectedIndex !=-1)
			{
				mySearchPaginateDatagrid.enabled=true;
				btCreateEquipement.enabled = true;
				btCreateModele.enabled = true;
			}
			else
			{
				mySearchPaginateDatagrid.enabled=false;
				btCreateEquipement.enabled = false;
				btCreateModele.enabled = false;
			}		
		}
		public function comboRevendeurHandler():void
		{
			if(comboRevendeur.selectedIndex != -1 && comboClient.selectedIndex !=-1)
			{
				mySearchPaginateDatagrid.enabled=true;
				btCreateEquipement.enabled = true;
				btCreateModele.enabled = true;
				CatalogueFormatteur.getInstance().distributorCurrency = new Currency(comboRevendeur.selectedItem.SYMBOL_DEVISE);
				if (comboRevendeur.selectedItem.SYMBOL_DEVISE != resourceManager.getString('M24', '_4635'))
					currenciesAreDifferent = true;
			}
			else
			{
				mySearchPaginateDatagrid.enabled=false;
				btCreateEquipement.enabled = false;
				btCreateModele.enabled = false;
			}
			majSelectDistributeur();
		}
		
		private function addPop():void
		{
			popUpdateEquipement.addEventListener(PopUpCatalogueEvent.REFRESH_PARENT_DATA,addEquipementCompleteHandler);
		
			PopUpManager.addPopUp(popUpdateEquipement,this,true);
			PopUpManager.centerPopUp(popUpdateEquipement);
		}
		private function addEquipementCompleteHandler(evt : PopUpCatalogueEvent):void
		{
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboConstructeur.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		override protected function paginateItemEditHandler(evt : StandardColonneEvent):void
		{
			popUpdateEquipement = new UpdateTarifEquipementClientIHM();
			popUpdateEquipement.equipement = evt.item as EquipementCatalogueVO;
			addPop();
		}
		
		public function supprimer(data : EquipementCatalogueVO):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M24', '_tes_vous_s_r_de_vouloir_supprimer_l__qu')+data.MODELE+ResourceManager.getInstance().getString('M24', '__'),ResourceManager.getInstance().getString('M24', 'Confirmation'),supprimerBtnValiderCloseEvent);
			
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				catalogueCS.addEventListener(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,removeEqCompleteHandler);
				catalogueCS.removeEquipement(myPaginatedatagrid.selectedItem as EquipementCatalogueVO);
			}
		}
		private function removeEqCompleteHandler(evt : CatalogueEvent):void
		{
			
			catalogueCS.removeEventListener(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,removeEqCompleteHandler);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'L__quipement_a_bien__t__supprim__'));
			if(mySearchPaginateDatagrid.parametresRechercheVO && comboConstructeur.selectedItem){
				rechercher(mySearchPaginateDatagrid.parametresRechercheVO);
			}
		}
		private var popTypeCMD : ListeTypeCommandeIHM;
		
		override public function btExportHandler(evt : PaginateDatagridEvent,idClientTypeCMD:Number=-1):void
		{
			if(comboExport.selectedItem)
			{
				var isTypeCmd : Boolean = false;
				
				var dataExp : Object = new Object();
				if(comboExport.selectedIndex == 0)
				{
					dataExp = myPaginatedatagrid.selectedItems;
				}
				else if(comboExport.selectedIndex == 1)
				{
					dataExp = myPaginatedatagrid.dataprovider;
				}
				else if(comboExport.selectedIndex == 2)
				{
					popTypeCMD = new ListeTypeCommandeIHM()
					popTypeCMD.idClient = idClientTypeCMD;
					
					for(var i:int=0;i<myPaginatedatagrid.selectedItems.length;i++)
					{
						if(popTypeCMD.listeIDEquipement)
						{
							popTypeCMD.listeIDEquipement = (myPaginatedatagrid.selectedItems.getItemAt(i) as EquipementCatalogueVO).IDEQUIPFOURNISSEUR + ","+ popTypeCMD.listeIDEquipement;
						}
						else
						{
							popTypeCMD.listeIDEquipement = (myPaginatedatagrid.selectedItems.getItemAt(i) as EquipementCatalogueVO).IDEQUIPFOURNISSEUR.toString();
						}
						
					}
					popTypeCMD.listeEquipement = myPaginatedatagrid.selectedItems;
					PopUpManager.addPopUp(popTypeCMD,this,true);
					PopUpManager.centerPopUp(popTypeCMD);
					isTypeCmd = true;
				}
				if(!isTypeCmd)
				{
					if(dataExp.length > 0) 
					{     
						var dataToExport  :String = DataGridDataExporter.getCSVString(myPaginatedatagrid.dgPaginate,dataExp,";","\n",false);
						var url :String = moduleCatalogueIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M24/exportCSVString.cfm";
						DataGridDataExporter.exportCSVString(url,dataToExport,"export");
					 }
					 else
					 {
					        Alert.show(ResourceManager.getInstance().getString('M24', 'Pas_de_donn_es___exporter__'),ResourceManager.getInstance().getString('M24', 'Consoview'));
					 }
				 }
            }
  		}
	}
}