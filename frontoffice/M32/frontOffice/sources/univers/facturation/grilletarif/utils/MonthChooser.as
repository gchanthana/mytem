package univers.facturation.grilletarif.utils
{
	import mx.controls.DateChooser;
	import mx.core.mx_internal;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.DateChooserEvent;
	import mx.resources.ResourceManager;
	
	
	use namespace mx_internal;
	
	public class MonthChooser extends DateChooser
	{
		
		public function MonthChooser()
		{
			monthNames=[ResourceManager.getInstance().getString('M32', 'Janvier'),ResourceManager.getInstance().getString('M32', 'F_vrier'),ResourceManager.getInstance().getString('M32', 'Mars'),ResourceManager.getInstance().getString('M32', 'Avril'),ResourceManager.getInstance().getString('M32', 'Mai'),ResourceManager.getInstance().getString('M32', 'Juin'),ResourceManager.getInstance().getString('M32', 'Juillet'),ResourceManager.getInstance().getString('M32', 'Ao_t'),ResourceManager.getInstance().getString('M32', 'Septembre'),ResourceManager.getInstance().getString('M32', 'Octobre'),ResourceManager.getInstance().getString('M32', 'Novembre'),ResourceManager.getInstance().getString('M32', 'D_cembre')];
		}
		override protected function createChildren():void
		{
			super.createChildren();
			dateGrid.addEventListener(DateChooserEvent.SCROLL,dateGrid_scrollHandler);
		}
		
	 	override protected function measure():void
	    {
         	super.measure();
         	dateGrid.visible = false;
       		measuredHeight = measuredHeight - dateGrid.getExplicitOrMeasuredHeight();
		}
		
		private function dateGrid_scrollHandler(event:DateChooserEvent):void
		{
		    var month:int = event.currentTarget.displayedMonth;
		    var year:int = event.currentTarget.displayedYear;
		    
		    selectedDate = new Date(year, month, 1);
		    
		    
		    var e:CalendarLayoutChangeEvent = new CalendarLayoutChangeEvent(CalendarLayoutChangeEvent.CHANGE);
        	e.newDate = selectedDate;
	        dispatchEvent(e);
		    
		}
	
	}
}