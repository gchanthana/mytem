<cfcomponent displayname="Unlock" implements="fr.consotel.api.mdm.IFunction" >
	
	<cffunction name="execute">
		
		<cfargument name="serialNumber" type="String" 	required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" 		type="String" 	required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="newPinCode" 	type="String" 	required="true" hint="Nouveau code PIN du vérrouillage (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime"		type="Numeric"	required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfargument name="idracine" 	type="Numeric" 	required="true" hint="Idracine courant">
			
			<cfset var infos			= structNew()><!--- cette structure est obligatoire pour récupérer les infos sur le serveur MDM ainsi que le code function --->
			<cfset var infos.idracine 	= idracine><!--- il faudra rajouter des informations dans la structure et les gérer dans get_ListeCodeFunction_ServeurMDM() --->
			
			<cfset var infosMDM = createObject("component", "fr.consotel.api.mdm.CodeFunction").get_ListeCodeFunction_ServeurMDM(infos)>
			
			<cfset param = structNew() >
			<cfset StructInsert(param, "serialNumber", "#serialNumber#", 1)>
			<cfset StructInsert(param, "imei", "#imei#", 1)>
			<cfset StructInsert(param, "newPinCode", "#newPinCode#", 1)>
			<cfset StructInsert(param, "waitTime", "#waitTime#", 1)>
			<cfset StructInsert(param, "servermdmpwd", "#infosMDM.servermdmpwd#", 1)>
			<cfset StructInsert(param, "servermdmuser", "#infosMDM.servermdmuser#", 1)>
			<cfset StructInsert(param, "servermdm", "#infosMDM.servermdm#", 1)>

			<cfset unlockDevice = createObject("component","fr.consotel.api.mdm.unlock.Unlock#infosMDM.codefunction#")>
			<!---<cfset unlockDevice.action(param)>--->
			<cfset var myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
			<cfset var iDeviceMngt=myTEM.getIDeviceManagement()>
			<cfset lockDevice = iDeviceMngt.unlock(serialNumber, imei, 3000)>
		
	</cffunction>

</cfcomponent>
