package univers.parametres.perimetres.mainComponent
{
	import composants.parametres.perimetres.OrgaStructure;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class orgaSite extends OrgaStructure
	{
		public var _idgeo:String;
		
		public function orgaSite()
			{
			//TODO: implement function
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHMSite);	
		}
		
		private function initIHMSite(e:Event):void
		{
			//refreshTree();
			cmbOrganisation.visible = false;
			btnFilterCmb.visible = false;
		}
		
		override public function refreshTree():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getNode",
																				fillTreeResult, throwError);
			RemoteObjectUtil.callService(op, _idgeo, 0, null);	
		}
		
		private function fillTreeResult(event: ResultEvent):void
		{
			/** Todo 
			 * Get the nodes of the tree 
			 * */
			 
			_treeNodes = XML(event.result).children();
			setTreeData(XML(event.result).children().children(), "@LABEL");
			//setTreeData(tmpXML2, "@LABEL");
		}
		

		override public function searchNode(nodeName:String):void
		{
			/** Todo
			 * Change the request 
			 * */
			_searchTreeResult = _treeNodes.descendants().(@LABEL.toString().toLowerCase().search(nodeName.toLowerCase()) > -1);
			_searchItemIndex = 0;
			selectNextNode();
			_searchExecQuery = false;
				
		}
		
		private function searchNodeResult(event : ResultEvent):void
		{
			/** ToDo
			 * Get the result of the search : ok
			 * */
		}
			
		override public function selectNextNode():void
		{
			if (_searchTreeResult[_searchItemIndex] == null)
				_searchItemIndex = 0;
			if (_searchTreeResult[_searchItemIndex] == null)
				return ;
			selectNodeByAttribute("VALUE", _searchTreeResult[_searchItemIndex].@VALUE);
			_searchItemIndex++;
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}
		
	}
}