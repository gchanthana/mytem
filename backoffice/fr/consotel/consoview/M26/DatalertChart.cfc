﻿<cfcomponent name="DatalertChart">
	
	<cffunction name="getDataPerHour" access="remote" returntype="query">		
		<cfargument name="DAY" type="date" required="true">	
		<cfargument name="IDSOUSTETE" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>
					
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.GetDataPerHour">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDSOUSTETE#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsDateFormat(DAY,'YYYY/MM/DD')#" null="false">
				
			<cfprocresult name="p_result">
		</cfstoredproc> 
		<cfreturn p_result>		

	</cffunction>
	
	<cffunction name="getDataPerDay" access="remote" returntype="query">		
		<cfargument name="DAYSTART" type="date" required="true">
		<cfargument name="DAYEND" type="date" required="true">	
		<cfargument name="IDSOUSTETE" type="numeric" required="true">

		<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>
					
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m26.GetDataPerDay">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDSOUSTETE#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsDateFormat(DAYSTART,'YYYY/MM/DD')#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsDateFormat(DAYEND,'YYYY/MM/DD')#" null="false">
				
			<cfprocresult name="p_result">
		</cfstoredproc> 
		<cfreturn p_result>		

	</cffunction>
	
	

</cfcomponent>