package gestionparc.ihm.fiches.listedispo
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.liste.ListeService;
	
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;


	public class DispoEqp extends DispoEqpIHM implements IListeDispo
	{

		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")]
		private var adrImg:Class;
		private var _showDgAvecSIM:Boolean;
		private var _originClick:String;
		private var _isMobile:Boolean=false;

		[Bindable]
		private var datagridVisible:DataGrid=new DataGrid;
		private var libelleSelection:String;
		private var serv:ListeService=new ListeService();
		public var typeEqp:Number;

		public function DispoEqp(boolAvecSim:Boolean=true, source:String="")
		{
			_showDgAvecSIM=boolAvecSim;
			_originClick=source;
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			dgListeTerm.addEventListener(MouseEvent.CLICK, dgVisibleChanged);
			dgListeTermAvecSIM.addEventListener(MouseEvent.CLICK, dgVisibleChanged);
			dgListeTermFixe.addEventListener(MouseEvent.CLICK, dgVisibleChanged);
			dgListeTermFixeAvecEqpFixe.addEventListener(MouseEvent.CLICK, dgVisibleChanged);

			if (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE)
			{
				typeEqp=1; // eqp mobile

				/** tester si l'origin de click = collaborateur*/
				if (_originClick == 'collab')
				{
					fromCollabSegmentMobile();
					hBoxCbox.includeInLayout=false;
				}
				else
				{
					notFromCollabSegmentMobile(); /** on clique sur une autre colonne */
				}
				_isMobile=true;
			}
			if (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEFIXE)
			{
				typeEqp=2; // eqp fixe

				/** je clique sur la colonne collaborateur */

				if (_originClick == 'collab')
				{
					fromCollabSegmentFixe();
					hBoxCbox.includeInLayout=false;
				}
				else
				{
					notFromCollabSegmentFixe();
				}
			}
		}

		private function fromCollabSegmentMobile():void
		{
			initDataMobile();
			initDataMobileAvecSIM();

			radioSansSIM.visible=true;
			radioAvecSIM.visible=true;
			radioSansSIM.selected=true;

			datagridVisible=dgListeTerm;

			radioSansSIM.addEventListener(Event.CHANGE, radioSansSIM_handler);
			radioAvecSIM.addEventListener(Event.CHANGE, radioAvecSIM_handler);

			txtFiltre.addEventListener(Event.CHANGE, txtFiltreChangeHandler);
			txtFiltreAvecSim.addEventListener(Event.CHANGE, txtFiltreAvecSimChangeHandler);
		}

		private function notFromCollabSegmentMobile():void
		{
			initDataMobile();

			radioSansSIM.visible=true;
			radioAvecSIM.visible=false;

			radioSansSIM.selected=true;
			radioSansSIM_handler(null); // réinitialiser la liste  lors qu'on décoche afficher d'autre equipement mobile

			radioSansSIM.addEventListener(Event.CHANGE, radioSansSIM_handler);
			txtFiltre.addEventListener(Event.CHANGE, txtFiltreChangeHandler);
			cbBoxEqpMobile.addEventListener(MouseEvent.CLICK, cbBoxMobileClickHandler);
			cbBoxEqpMobile.visible=true;
		}

		private function fromCollabSegmentFixe():void
		{
			initDataFixe();
			initDataFixeAvecEqpFixe();

			radioAvecEqpFixe.visible=true;
			radioSansEqpFixe.visible=true;

			if (!_isMobile)
			{
				radioSansEqpFixe.selected=true;
				datagridVisible=dgListeTermFixe;// la fonction initDataFixe permet de mettre à jour dgListeTermFixe
			}
			else
			{
				radioSansSIM.selected=true;
				radioSansSIM_handler(null); // on appelle cette fonction parce que initDataFixe,initDataFixeAvecEqpFixe ne permettent pas de mettre a jour dgListeTerm
			}
			radioAvecEqpFixe.addEventListener(Event.CHANGE, radioAvecEqpFixe_handler);
			radioSansEqpFixe.addEventListener(Event.CHANGE, radioSansEqpFixe_handler);

			txtFiltreFixe.addEventListener(Event.CHANGE, txtFiltreFixeChangeHandler);
			txtFiltreFixeAvecEqpFixe.addEventListener(Event.CHANGE, txtFiltreFixeAvecEqpFixeChangeHandler);
		}

		private function notFromCollabSegmentFixe():void
		{
			initDataFixe();

			radioSansEqpFixe.visible=true;
			radioAvecEqpFixe.visible=false;

			if (!_isMobile)
			{
				radioSansEqpFixe.selected=true;
				datagridVisible=dgListeTermFixe;
			}
			else
			{
				radioSansSIM.selected=true; // selectionner terminal sans sim 
				radioSansSIM_handler(null); // réinitialiser la liste  lors qu'on décoche afficher d'autre equipement fixe
			}
			radioSansEqpFixe.addEventListener(Event.CHANGE, radioSansEqpFixe_handler);
			txtFiltreFixe.addEventListener(Event.CHANGE, txtFiltreFixeChangeHandler);
			cbBoxEqpFixe.addEventListener(MouseEvent.CLICK, cbBoxFixeClickHandler);
			cbBoxEqpFixe.visible=true;
		}

		private function cbBoxMobileClickHandler(evt:MouseEvent):void
		{
			if (evt.currentTarget.selected == true)
			{
				fromCollabSegmentMobile();
			}
			else
			{
				notFromCollabSegmentMobile();
			}
		}

		private function cbBoxFixeClickHandler(evt:MouseEvent):void
		{
			if (evt.currentTarget.selected == true)
			{
				fromCollabSegmentFixe();
			}
			else
			{
				notFromCollabSegmentFixe();
			}
		}

		private function dgVisibleChanged(ev:Event):void
		{
			if (datagridVisible && datagridVisible.selectedIndex > -1)
				dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		public function getSelection():Object
		{
			var obj:Object=new Object();
			if (datagridVisible.selectedItem != null)
			{
				obj=datagridVisible.selectedItem;
			}
			return obj;
		}

		public function getLibelleSelection():String
		{
			if (datagridVisible.selectedIndex != -1)
			{
				if (datagridVisible.selectedItem.hasOwnProperty("IMEI"))
					return datagridVisible.selectedItem.IMEI;
				else if (datagridVisible.selectedItem.hasOwnProperty("NO_SERIE"))
					return datagridVisible.selectedItem.NO_SERIE
				else
					return ""
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		public function getDataGrid():DataGrid
		{
			return datagridVisible;
		}

		public function getIDSelection():int
		{
			if (datagridVisible.selectedIndex != -1)
				return datagridVisible.selectedItem.IDTERM;
			else
				return -1;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_terminaux_disponibles');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		private function radioSansSIM_handler(evt:Event):void
		{
			if (radioSansSIM.selected == true)
			{
				viewstack_dispo_term.selectedIndex=0; // enfant selectionné de viewstack
				datagridVisible=dgListeTerm;
				txtFiltre.setFocus();
				resetSelection();
				typeEqp=1; // eqp mobile
			}
		}

		private function radioAvecSIM_handler(evt:Event):void
		{
			if (radioAvecSIM.selected == true)
			{
				viewstack_dispo_term.selectedIndex=1;
				datagridVisible=dgListeTermAvecSIM;
				txtFiltreAvecSim.setFocus();
				resetSelection();
				typeEqp=1; // eqp mobile
			}
		}

		private function radioSansEqpFixe_handler(evt:Event):void
		{
			if (radioSansEqpFixe.selected == true)
			{
				viewstack_dispo_term.selectedIndex=2;
				datagridVisible=dgListeTermFixe;
				txtFiltreFixe.setFocus();
				serv.myDatas.dataListeTermFixeSansEqpFixe.filterFunction=filtrerLeGrid;
				serv.myDatas.dataListeTermFixeSansEqpFixe.refresh();
				resetSelection();
				typeEqp=2; // eqp fixe
			}
		}

		private function radioAvecEqpFixe_handler(evt:Event):void
		{
			if (radioAvecEqpFixe.selected == true)
			{
				viewstack_dispo_term.selectedIndex=3;
				datagridVisible=dgListeTermFixeAvecEqpFixe;
				txtFiltreFixeAvecEqpFixe.setFocus();
				serv.myDatas.dataListeTermFixeAvecEqpFixe.filterFunction=filtrerLeGrid;
				serv.myDatas.dataListeTermFixeAvecEqpFixe.refresh();
				resetSelection();
				typeEqp=2; // eqp fixe
			}
		}

		private function resetSelection():void
		{
			datagridVisible.selectedIndex=-1;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		/* DATA MOBILE */
		private function initDataMobile():void
		{
			datagridVisible=dgListeTerm;
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_TERMSANSSIM, updateDGSansSim);
			serv.listeTermDispo();
		}

		private function updateDGSansSim(re:gestionparcEvent):void
		{
			dgListeTerm.dataProvider=serv.myDatas.dataListeTermSansSim;
			serv.myDatas.dataListeTermSansSim.refresh();
		}

		private function initDataMobileAvecSIM():void
		{
			datagridVisible=dgListeTermAvecSIM;
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_TERMAVECSIM, updateDGAvecSim);
			serv.listeTermAvecSimDispo();
		}

		private function updateDGAvecSim(re:gestionparcEvent):void
		{
			dgListeTermAvecSIM.dataProvider=serv.myDatas.dataListeTermAvecSim;
			serv.myDatas.dataListeTermAvecSim.refresh();
		}

		private function initDataFixe():void
		{
			datagridVisible=dgListeTermFixe;
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_TERMFIXESANSEQPFIXE, updateDGFixeSansEqpFixe);
			serv.listeTermFixeDispo();
		}

		private function updateDGFixeSansEqpFixe(re:gestionparcEvent):void
		{
			dgListeTermFixe.dataProvider=serv.myDatas.dataListeTermFixeSansEqpFixe;
			serv.myDatas.dataListeTermFixeSansEqpFixe.refresh();
		}

		private function initDataFixeAvecEqpFixe():void
		{
			datagridVisible=dgListeTermFixeAvecEqpFixe;
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_TERMFIXEAVECEQPFIXE, updateDGFixeAvecEqpFixe);
			serv.listeTermFixeAvecEqpFixeDispo();
		}

		private function updateDGFixeAvecEqpFixe(re:gestionparcEvent):void
		{
			dgListeTermFixeAvecEqpFixe.dataProvider=serv.myDatas.dataListeTermFixeAvecEqpFixe;
			serv.myDatas.dataListeTermFixeAvecEqpFixe.refresh();
		}


		private function txtFiltreChangeHandler(ev:Event):void
		{
			try
			{
				serv.myDatas.dataListeTermSansSim.filterFunction=filtrerLeGrid;
				serv.myDatas.dataListeTermSansSim.refresh();
			}
			catch (e:Error)
			{
			}
		}

		private function filtrerLeGrid(item:Object):Boolean
		{
			if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1) || (String(item.TERMINAL).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}

		private function txtFiltreAvecSimChangeHandler(ev:Event):void
		{
			try
			{
				serv.myDatas.dataListeTermAvecSim.filterFunction=filtrerLeGridAvecSIM;
				serv.myDatas.dataListeTermAvecSim.refresh();
			}
			catch (e:Error)
			{
			}
		}

		private function filtrerLeGridAvecSIM(item:Object):Boolean
		{
			if ((String(item.IMEI).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1) || (String(item.NUM_SIM).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1) || (String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1) || (String(item.REVENDEUR).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1))
				return true
			else
				return false
		}

		private function txtFiltreFixeChangeHandler(evt:Event):void
		{
			try
			{
				serv.myDatas.dataListeTermFixeSansEqpFixe.filterFunction=filtrerLeGridFixe;
				serv.myDatas.dataListeTermFixeSansEqpFixe.refresh();
			}
			catch (e:Error)
			{
			}
		}

		private function filtrerLeGridFixe(item:Object):Boolean
		{
			if ((String(item.NO_SERIE).toLowerCase().search(txtFiltreFixe.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltreFixe.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltreFixe.text.toLowerCase()) != -1))
				return true
			else
				return false
		}

		private function txtFiltreFixeAvecEqpFixeChangeHandler(evt:Event):void
		{
			try
			{
				serv.myDatas.dataListeTermFixeAvecEqpFixe.filterFunction=filtrerLeGridFixeAvecEqpFixe;
				serv.myDatas.dataListeTermFixeAvecEqpFixe.refresh();
			}
			catch (e:Error)
			{
			}
		}

		private function filtrerLeGridFixeAvecEqpFixe(item:Object):Boolean
		{
			if ((String(item.NO_SERIE).toLowerCase().search(txtFiltreFixeAvecEqpFixe.text.toLowerCase()) != -1) || (String(item.MARQUE).toLowerCase().search(txtFiltreFixeAvecEqpFixe.text.toLowerCase()) != -1) || (String(item.MODELE).toLowerCase().search(txtFiltreFixeAvecEqpFixe.text.toLowerCase()) != -1) || (String(item.LIGNE).toLowerCase().search(txtFiltreFixeAvecEqpFixe.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
	}
}
