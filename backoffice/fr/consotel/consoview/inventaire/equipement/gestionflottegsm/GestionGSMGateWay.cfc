<cfcomponent output="false">
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/15/2007
		
		Description : recherche des utilisateurs
	
	--->
	<cffunction name="rechercherCollaborateurs" access="remote" output="false" returntype="query">	
		<cfargument name="chaine" type="string" required="true">	
		<!--- 
			--------------------------------------------------------------------------
		    -- recherche dans la flotte mobile par collaborateur                    --
		   	--------------------------------------------------------------------------
		       PROCEDURE searchFlotte_Collaborateur ( 	p_idracine                IN INTEGER,
		                                              	p_idgroupe                 IN INTEGER,
		                                              	p_chaine                   IN VARCHAR2,
		                                                p_retour                                    OUT SYS_REFCURSOR); 

		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.searchFlotte_Collaborateur">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_idgroupe" value="#chaine#"/>
			<cfprocresult name="qData"/>        
		</cfstoredproc>				
		<cfreturn qData>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/15/2007
		
		Description : recherche des cartes SIM
	
	--->
	<cffunction name="rechercherSims" access="remote" output="false" returntype="query">		
		<cfargument name="chaine" type="string" required="true">	
		<!--- 
			--------------------------------------------------------------------------
		    -- recherche dans la flotte searchflotte_sim                    --
		   	--------------------------------------------------------------------------
		       PROCEDURE searchflotte_sim ( 	p_idracine                IN INTEGER,
		                                              	p_idgroupe                 IN INTEGER,
		                                              	p_chaine                   IN VARCHAR2,
		                                                p_retour                                    OUT SYS_REFCURSOR); 

		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.searchflotte_sim">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_idgroupe" value="#chaine#"/>
			<cfprocresult name="qData"/>        
		</cfstoredproc>				
		<cfreturn qData>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/15/2007
		
		Description : recherche des mobiles
	
	--->
	<cffunction name="rechercherMobiles" access="remote" output="false" returntype="query">		
		<cfargument name="chaine" type="string" required="true">	
		<!--- 
			--------------------------------------------------------------------------
		    -- recherche dans la flotte mobile par collaborateur                    --
		   	--------------------------------------------------------------------------
		       PROCEDURE searchFlotte_mobile ( 	p_idracine                IN INTEGER,
		                                              	p_idgroupe                 IN INTEGER,
		                                              	p_chaine                   IN VARCHAR2,
		                                                p_retour                                    OUT SYS_REFCURSOR); 

		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.searchFlotte_mobile">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_idgroupe" value="#chaine#"/>
			<cfprocresult name="qData"/>        
		</cfstoredproc>				
		<cfreturn qData>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/15/2007
		
		Description : recherche des lignes de mobiles
	
	--->
	<cffunction name="rechercherLignes" access="remote" output="false" returntype="query">		
		<cfargument name="chaine" type="string" required="true">	
		<!--- 
			--------------------------------------------------------------------------
		    -- recherche dans la flotte mobile par Ligne 		                    --
		   	--------------------------------------------------------------------------
		       PROCEDURE searchFlotte_Ligne 		( 	p_idracine                IN INTEGER,
		                                              	p_idgroupe                 IN INTEGER,
		                                              	p_chaine                   IN VARCHAR2,
		                                                p_retour                                    OUT SYS_REFCURSOR); 

		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_EQUIPEMENT.searchFlotte_Ligne">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_idgroupe" value="#chaine#"/>
			<cfprocresult name="qData"/>        
		</cfstoredproc>				
		<cfreturn qData>
	</cffunction>
	
	
	
</cfcomponent>