<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.Attachments" extends="fr.saaswedo.api.support.BaseClass" hint="Gestion des attachments">
	<cffunction access="public" name="show" returntype="Struct" hint="Retourne les infos concernant un attachement (Version Beta : fonctionnalité Zendesk non officielle)">
		<cfargument name="attachmentId" type="String" required="true" hint="Identifiant de l'attachement">
		<cfthrow type="Custom" message="Cette fonctionnalité n'est pas supportée. Utiliser : fr.saaswedo.api.support.zendesk.Attachments.show()">
	</cffunction>

	<cffunction access="public" name="uploadContent" returntype="Struct" hint="Upload le contenu fileContent avec le nom fileName et retourne les infos correspondant à l'opération">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier pour l'upload (Incluant l'extension)">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="uploadToken" type="String" required="false" default="" hint="Token correspondant à un upload existant <b>(A renseigner en cas d'attachements multiples)</b>">
		<cfset var attachments=getAttachments()>
		<cfset var uploadResult=attachments.uploadContent(ARGUMENTS["fileName"],ARGUMENTS["fileContent"],ARGUMENTS["uploadToken"])>
		<cfreturn uploadResult>
	</cffunction>
</cfcomponent>