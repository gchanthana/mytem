<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSPackageService" implements="fr.consotel.api.mdm.partner.private.ws.IWSPackageService"
hint="Implémentation fournissant les méthodes du Service EveryWanPackage. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<cffunction access="public" name="getPackage" returntype="String" hint="Retourne la définition du package au format JSON (Voir fr.consotel.api.mdm.partner.private.ws.IWSPackageService)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackage(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="setPackage" returntype="Void" hint="Spécifie la définition du package au format JSON">
		<cfargument name="jsonPkg" type="String" required="true" hint="Définition du package au format JSON (Voir fr.consotel.api.mdm.partner.private.ws.IWSPackageService)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cfset var pkgJSONDef=ARGUMENTS["jsonPkg"]>
		<cfif isJSON(pkgJSONDef)>
			<cftry>
				<cfset packageService.setPackage(pkgJSONDef)>
				<cfcatch type="any">
					<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
					<cfrethrow>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le format JSON de la définition de package fournie est invalide">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getPackages" returntype="String" hint="Retourne la liste des définitions des packages au format JSON">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package ou une chaine vide pour indiquer <b>le package racine</b>">
		<cfargument name="deep" type="Boolean" required="true" hint="TRUE pour retourne la liste complète détaillée et FALSE sinon">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackages(asJavaNullValue(ARGUMENTS["pkgFullPath"]),ARGUMENTS["deep"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackage" returntype="Void" hint="Supprime la définition d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackage(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setPackageRule" returntype="Void" hint="Définit la règle d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlRule" type="String" required="true" hint="Définition XML de la règle ou une chaine vide pour pour ignorer cette règle">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.setPackageRule(ARGUMENTS["pkgFullPath"],asJavaNullValue(ARGUMENTS["xmlRule"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="renamePackage" returntype="Void" hint="Renomme un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Actuel chemin absolu du package">
		<cfargument name="newPackageName" type="String" required="true" hint="Nouveau nom à donner au package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.renamePackage(ARGUMENTS["pkgFullPath"],ARGUMENTS["newPackageName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="checkPackageRule" returntype="Boolean" hint="Vérifie la définition de la règle et retourne TRUE si elle est valide et FALSE sinon">
		<cfargument name="xmlRule" type="String" required="true" hint="Définition XML de la règle">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.checkPackageRule(ARGUMENTS["xmlRule"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageRule" returntype="String" hint="Retourne la définition XML de la règle du package (Voir documentation Zenprise pour la DTD)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageRule(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageRule" returntype="Void" hint="Supprime la règle du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageRule(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="isDeployOnce" returntype="Boolean" hint="Retourne TRUE si la propriété deployOnce du package est activé et FALSE sinon. Voir setDeployOnce()">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.isDeployOnce(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setDeployOnce" returntype="Void" hint="Affecte la valeur deployOnce à la propriété correspondante du package<br>
	Lorsque cette propriété vaut TRUE alors le serveur ne renvoie pas de données au client si un précédent déploiement s'est déjà effectué avec succès (Via l'historique de déploiement)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="deployOnce" type="Boolean" required="true" hint="Valeur à affecter à la propriété deployOnce du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.setDeployOnce(ARGUMENTS["pkgFullPath"],ARGUMENTS["deployOnce"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="deploy" returntype="Void"
	hint="Effectue une demande de déploiement pour les équipement qui sont connectés au serveur (Android et Windows). Envoie une notification PUSH pour iOS">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.deploy()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageList" returntype="Array" hint="Retourne la liste des noms des packages racines dans un tableau">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageList()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="createPackage" returntype="String" hint="Crée un package avec le nom fourni dans pkgFullPath et retourne le nom">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.createPackage(ARGUMENTS["osFamily"],ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageTunnelList" returntype="Array" hint="Retourne la liste des noms des tunnels du package dans un tableau">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageTunnelList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageTunnel" returntype="Void" hint="Ajoute un tunnel dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageTunnel(ARGUMENTS["pkgFullPath"],ARGUMENTS["tunnelId"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageTunnel" returntype="Void" hint="Supprime un tunnel d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageTunnel(ARGUMENTS["pkgFullPath"],ARGUMENTS["tunnelId"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getTunnelPackageList" returntype="Array" hint="Retourne la liste des packages utilisant le tunnel tunnelId dans un tableau">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="tunnelId" type="String" required="true" hint="Nom du tunnel">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getTunnelPackageList(ARGUMENTS["osFamily"],ARGUMENTS["tunnelId"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageGroupList" returntype="Array" hint="Retourne la liste des groupes déployés dans le package dans un tableau">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageGroupList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageGroup" returntype="Void" hint="Déploie le groupe groupId dans le package pkgFullPath">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["groupId"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageGroup" returntype="Void" hint="Supprime un groupe du package pkgFullPath">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["groupId"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getGroupPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages dans lesquels le groupe groupId a été déployé">
		<cfargument name="groupId" type="String" required="true" hint="Nom hiérarchique du groupe (e.g : group.subgroup)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getGroupPackageList(ARGUMENTS["groupId"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageOSFamily" returntype="String" hint="Retourne l'OS cible du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageOSFamily(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageSchedule" returntype="String"
	hint="Retourne la configuration XML de la planification du package ou une chaine vide s'il n'est pas planifié (Voir documentation Zenprise pour la DTD)">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn toString(packageService.getPackageSchedule(ARGUMENTS["pkgFullPath"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="checkPackageSchedule" returntype="Boolean" hint="Retourne TRUE si la configuration XML de planification xmlSchedule est valide et FALSE sinon">
		<cfargument name="xmlSchedule" type="String" required="true" hint="Configuration XML de planification de package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.checkPackageSchedule(ARGUMENTS["xmlSchedule"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setPackageSchedule" returntype="Void" hint="Définit la configuration XML de planification xmlSchedule pour le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlSchedule" type="String" required="true" hint="Configuration XML de planification de package ou une chaine vide pour ignorer la planification">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.setPackageSchedule(ARGUMENTS["pkgFullPath"],asJavaNullValue(ARGUMENTS["xmlSchedule"]))>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageSchedule" returntype="Void" hint="Supprime la configuration XML de planification pour le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageSchedule(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageFileList" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers de ressources utilisés dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageFileList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageFile" returntype="Void" hint="Ajoute un fichier de ressources dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources à ajouter sans préfixe">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageFile(ARGUMENTS["pkgFullPath"],ARGUMENTS["fileName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageFile" returntype="Void" hint="Supprime le fichier de ressources fileName du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources à supprimer sans préfixe">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageFile(ARGUMENTS["pkgFullPath"],ARGUMENTS["fileName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getFilePackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant le fichier de ressources">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier de ressources">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getFilePackageList(ARGUMENTS["fileName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageXMLList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources XML utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageXMLList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageXML" returntype="Void" hint="Ajoute une ressource XML dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageXML(ARGUMENTS["pkgFullPath"],ARGUMENTS["xmlName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageXML" returntype="Void" hint="Supprime une ressource XML du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à suprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageXML(ARGUMENTS["pkgFullPath"],ARGUMENTS["xmlName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getXMLPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource XML">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="xmlName" type="String" required="true" hint="Nom de la ressource XML à suprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getXMLPackageList(ARGUMENTS["osFamily"],ARGUMENTS["xmlName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageCABList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources CAB utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageCABList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageCAB" returntype="Void" hint="Ajoute une ressource CAB dans un package de type WINDOWS">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageCAB(ARGUMENTS["pkgFullPath"],ARGUMENTS["cabName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageCAB" returntype="Void" hint="Supprime une ressource CAB d'un package de type WINDOWS">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageCAB(ARGUMENTS["pkgFullPath"],ARGUMENTS["cabName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getCABPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource CAB">
		<cfargument name="cabName" type="String" required="true" hint="Nom de la ressource CAB">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getCABPackageList(ARGUMENTS["cabName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageAPKList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources APK d'un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageAPKList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageAPK" returntype="Void" hint="Ajoute une ressource APK à un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageAPK(ARGUMENTS["pkgFullPath"],ARGUMENTS["apkName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageAPK" returntype="Void" hint="Supprime une ressource APK d'un package de type ANDROID">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageAPK(ARGUMENTS["pkgFullPath"],ARGUMENTS["apkName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getAPKPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource APK">
		<cfargument name="apkName" type="String" required="true" hint="Nom de la ressource APK à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getAPKPackageList(ARGUMENTS["apkName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageServerGroupList" returntype="Array" hint="Retourne un tableau contenant la liste des groupes de serveurs utilisés par le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageServerGroupList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageServerGroup" returntype="Void" hint="Ajoute un groupe de serveur à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageServerGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["sgName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageServerGroup" returntype="Void" hint="Supprime un groupe de serveur d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageServerGroup(ARGUMENTS["pkgFullPath"],ARGUMENTS["sgName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getServerGroupPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant le groupe de serveur">
		<cfargument name="sgName" type="String" required="true" hint="Nom du groupe de serveur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getServerGroupPackageList(ARGUMENTS["sgName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageRegistriesList" returntype="Array" hint="Retourne un tableau contenant la liste des registres utilisés par le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageRegistriesList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageRegistry" returntype="Void" hint="Ajoute un registre à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageRegistry(ARGUMENTS["pkgFullPath"],ARGUMENTS["regName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageRegistry" returntype="Void" hint="Supprime un registre d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageRegistry(ARGUMENTS["pkgFullPath"],ARGUMENTS["regName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getRegistryPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant le registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : WINDOWS, ANDROID)">
		<cfargument name="regName" type="String" required="true" hint="Nom du registre à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getRegistryPackageList(ARGUMENTS["osFamily"],ARGUMENTS["regName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="hasPackageSoftwareInventory" returntype="Boolean" hint="Retourne TRUE si le package contient un inventaire logiciel">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.hasPackageSoftwareInventory(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageSoftwareInventory" returntype="Void" hint="Définit l'inventaire logiciel du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageSoftwareInventory(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageSoftwareInventory" returntype="Void" hint="Supprime l'inventaire logiciel du package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageSoftwareInventory(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getSoftwareInventoryPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des package ayant un inventaire logiciel">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getSoftwareInventoryPackageList()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageCfgList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources CFG utilisées dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageCfgList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageCfg" returntype="Void" hint="Ajoute une ressource CFG dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageCfg(ARGUMENTS["pkgFullPath"],ARGUMENTS["cfgName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageCfg" returntype="Void" hint="Supprime une ressource CFG d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageCfg(ARGUMENTS["pkgFullPath"],ARGUMENTS["cfgName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getCfgPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource CFG">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="cfgName" type="String" required="true" hint="Nom de la ressource CFG à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getCfgPackageList(ARGUMENTS["osFamily"],ARGUMENTS["cfgName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageExtAppList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources EXT d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageExtAppList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageExtApp" returntype="Void" hint="Ajoute une ressource EXT dans le package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageExtApp(ARGUMENTS["pkgFullPath"],ARGUMENTS["extName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageExtApp" returntype="Void" hint="Supprime une ressource EXT d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageExtApp(ARGUMENTS["pkgFullPath"],ARGUMENTS["extName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getExtAppPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource EXT">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="extName" type="String" required="true" hint="Nom de la ressource EXT à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getExtAppPackageList(ARGUMENTS["osFamily"],ARGUMENTS["extName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getPackageIpaList" returntype="Array" hint="Retourne un tableau contenant la liste des ressources IPA d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getPackageIpaList(ARGUMENTS["pkgFullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addPackageIpa" returntype="Void" hint="Ajoute un ressource IPA à un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA à ajouter">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.addPackageIpa(ARGUMENTS["pkgFullPath"],ARGUMENTS["ipaName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removePackageIpa" returntype="Void" hint="Supprime une ressource IPA d'un package">
		<cfargument name="pkgFullPath" type="String" required="true" hint="Chemin absolu du package">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfset packageService.removePackageIpa(ARGUMENTS["pkgFullPath"],ARGUMENTS["ipaName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getIpaPackageList" returntype="Array" hint="Retourne un tableau contenant la liste des packages utilisant la ressource IPA">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Doit etre iOS pour la version actuelle de l'API Zenprise">
		<cfargument name="ipaName" type="String" required="true" hint="Nom de la ressource IPA">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var packageService=zdmRepository.getPackageService()>
		<cftry>
			<cfreturn packageService.getIpaPackageList(ARGUMENTS["osFamily"],ARGUMENTS["ipaName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSPackageService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
</cfcomponent>