<cfinterface displayName="fr.consotel.api.platform.Platform" hint="Singleton">
	<cffunction access="public" name="getInstance" returntype="fr.consotel.api.platform.Platform" hint="Singleton method">
	</cffunction>
	
	<cffunction access="public" name="getLevel" returntype="string" hint="Returns platform level">
	</cffunction>
</cfinterface>