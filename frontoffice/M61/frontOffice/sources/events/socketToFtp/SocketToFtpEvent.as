package events.socketToFtp
{
	import flash.events.Event;

	public class SocketToFtpEvent extends Event
	{
		
		//------------ METHODES ------------//
		
		/* */
		public function SocketToFtpEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}