<cfcomponent output="false" extends="AccessObject">
	<cfproperty name="ABO" type="string" default="Abonnements">
	<cfproperty name="CONSO" type="string" default="Consommations">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.ABO = "Abonnements";
		variables.CONSO = "Consommations";
	</cfscript>
	
		<!---
		
		Auteur : samuel.divioka
		
		Date : 12/6/2007
		
		Description : Ressources hors inventaire facturï¿½es
		
		Ressoures (couple produit-ligne) facturï¿½es hors inventaire = 
		-	ressources qui n?ont jamais fait partie de l?inventaire et qui sont facturï¿½es ou 
		-	qui sont sortis de l?inventaire avant le debut de la pï¿½riode de la facture mais qui sont toujours facturï¿½es ou
		-	qui sont entrï¿½s dans l?inventaire aprï¿½s la fin de la facture et qui sont dï¿½jï¿½ facturï¿½es
		
		1 - On est d?accord que pour la partie abos, je prends tout ce qui est facturï¿½ sur ces ressources hors inventaire, et qui est de type de theme Abos.
		
		2 - Pour ce qui est des consos, tu me demandes de prendre, parmi les lignes des ressources hors inventaire, ceux qui ont un abo de sur theme ï¿½ Lignes ï¿½, et d?afficher les consos sur ces lignes.
		C?est bien ï¿½a ?
		
		Dans la feuille explicative, partie consos, tu me dis ï¿½ Faire remonter tous les produits, sauf ceux dï¿½jï¿½ identifiï¿½ par le processus ci-dessus afin d?eviter une remontï¿½ en double ï¿½. OK sauf que dans le 1er cas je ramene des abos et dans l?autre des consos.
		Donc il ne peut y avoir de remontï¿½ de doublon, je me trompe ?

	--->
	<cffunction access="remote" name="getMontantRessourcesHorsInventaire" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true">		 
		<cfset qListeRessources = Evaluate("getMontantRessourcesHorsInventaire#getTypePerimetre()#AsQuery(myFacture)")>		
		
		<cfquery name="abos" dbtype="query">
			select SUM(QTE) as quantite, SUM(MONTANT) as montant
			from qListeRessources 
			where TYPE_THEME = '#variables.ABO#' 
		</cfquery>
				
		<cfquery name="consos" dbtype="query">
			select SUM(QTE) as volume, SUM(MONTANT) as montant
			from qListeRessources
			where TYPE_THEME = '#variables.CONSO#'
		</cfquery>				
		
		<cfset tab = arrayNew(1)>
			
		<cfset abo = createObject("component","Ressource")>	
		<cfset conso = createObject("component","Ressource")>
		<cfset abo.setTypeTheme('Abonnements')>	
		<cfset abo.setQuantite(0)>
		<cfset abo.setMontant(0)>			
		<cfset conso.setTypeTheme('Consommations')>		
		<cfset conso.setVolume(0)>
		<cfset conso.setMontant(0)>
		
		
		<cfloop query="abos">
			<cfset abo.setQuantite(abos.quantite)>
			<cfset abo.setMontant(abos.montant)>						
		</cfloop>
		
		<cfloop query="consos">						
			<cfset conso.setVolume(consos.volume)>
			<cfset conso.setMontant(consos.montant)>			
		</cfloop>
		
		<cfset arrayappend(tab,abo)>
		<cfset arrayappend(tab,conso)>
		
		<cfreturn tab>
	</cffunction>
	
	<cffunction access="remote" name="getMontantRessourcesHorsInventaireGroupeAsQuery" returntype="query">		 
		<cfargument name="myFacture" type="Facture" required="true">		 
		<!---
			PROCEDURE PKG_CV_GRCL_FACTURATION.Res_HorsInv_facturee_v2(					
									p_idracine     				IN INTEGER,	
									p_idinventaire_periode		IN INTEGER,			
									p_retour 	               OUT SYS_REFCURSOR) IS 			
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION.RES_HORSINV_FACTUREE_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="qListeRessources"/>        
		</cfstoredproc>
		<cfreturn qListeRessources>
	</cffunction>
	
	<cffunction access="remote" name="getMontantRessourcesHorsInventaireGroupeLigneAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">		 
		 <!---
			PROCEDURE PKG_CV_GLIG_FACTURATION.Res_HorsInv_facturee_v2(					
									p_idracine     				IN INTEGER,	
									p_idinventaire_periode		IN INTEGER,			
									p_retour 	               OUT SYS_REFCURSOR) IS 				

				
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_FACTURATION.RES_HORSINV_FACTUREE_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupeLigne" value="#getIdGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="qListeRessources"/>        
		</cfstoredproc>
		<cfreturn qListeRessources>
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : Liste des ressources hors inventaire de la facture
		cf->getMontantRessourcesHorsInventaire
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaire" returntype="Ressource[]">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		
		<cfset qListeRessources = Evaluate("getDetailRessourcesHorsInventaire#getTypePerimetre()#AsQuery(myFacture,typeTheme)")>		
		<cfset tabRessources = arrayNew(1)>
		<cfLoop query="qListeRessources">
			<cfset myRessource = createObject("component","Ressource")>
			 
			<cfscript>	
				myRessource.setlibelleProduit(qListeRessources.LIBELLE_PRODUIT);
				myRessource.setproduitClientId(qListeRessources.IDPRODUIT_CLIENT);
				myRessource.setsousTeteId(qListeRessources.IDSOUS_TETE);
				myRessource.setsousCompte(qListeRessources.SOUS_COMPTE);
				myRessource.setcompteFacturation(qListeRessources.COMPTE_FACTURATION); 
				myRessource.setsousTete(qListeRessources.SOUS_TETE);
				myRessource.setprixUnitaire(qListeRessources.PRIX_UNIT);
				myRessource.setdateDebut(qListeRessources.DATEDEB);
				myRessource.setdateFin(qListeRessources.DATEFIN);
				myRessource.setdateEntree(qListeRessources.DATE_ENTREE);
				myRessource.setdateSortie(qListeRessources.DATE_SORTIE);
				myRessource.settypeTheme(qListeRessources.TYPE_THEME);
				myRessource.setquantite(qListeRessources.QTE);
				myRessource.setmontant(qListeRessources.MONTANT);
				myRessource.setvolume(qListeRessources.DUREE_VOLUME);			
			</cfscript>
			 
		 
			<cfset arrayAppend(tabRessources,myRessource)>
		</cfLoop>
		<cfreturn tabRessources />
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : Detail des produits hors inventaire pour un groupe
		cf -> getMontantRessourcesHorsInventaire 
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaireGroupeAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		 <!---
			pkg_cv_grcl_facturation.det_res_horsinv_facturee_v2(p_idracine => :p_idracine,
            			                                     p_idinventaire_periode => :p_idinventaire_periode,
                                                             p_retour => :p_retour); 				
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION.DET_RES_HORSINV_FACTUREE_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="qListeRessources"/>
		</cfstoredproc>
		
		<cfswitch expression="#typeTheme#">
			<cfcase value="Consommations">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.CONSO#'
				</cfquery>
				<cfreturn qListe> 	
			</cfcase>
			
			<cfcase value="Abonnements">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.ABO#' 
				</cfquery>					
				<cfreturn qListe>
			</cfcase>
			
			
			<cfdefaultcase>							
				<cfreturn qListeRessources>
			</cfdefaultcase>
			
		</cfswitch>
				
		<cfreturn qListeRessources>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : Detail des produits hors inventaire un groupeLigne
		cf -> getMontantRessourcesHorsInventaire
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaireGroupeLigneAsQuery" returntype="query">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		 <!---
			pkg_cv_glig_facturation.det_res_horsinv_facturee_v2(p_idracine => :p_idracine,
            			                                     p_idinventaire_periode => :p_idinventaire_periode,
                                                             p_retour => :p_retour); 				
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GL_FACTURATION.DET_RES_HORSINV_FACTUREE_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getIdGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idinventaire_periode" value="#myFacture.getPeriodeId()#"/>
			<cfprocresult name="qListeRessources"/>
		</cfstoredproc>
		
		<cfswitch expression="#typeTheme#">
			<cfcase value="Consommations">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.CONSO#'
				</cfquery>
				<cfreturn qListe> 	
			</cfcase>
			
			<cfcase value="Abonnements">
				<cfquery name="qListe" dbtype="query">
					select * from qListeRessources where TYPE_THEME = '#variables.ABO#'
				</cfquery>					
				<cfreturn qListe>
			</cfcase>			
			
			<cfdefaultcase>							
				<cfreturn qListeRessources>
			</cfdefaultcase>
			
		</cfswitch>
				
		<cfreturn qListeRessources>
	</cffunction>
	
	
</cfcomponent>