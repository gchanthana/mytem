package event.selector
{
	import flash.events.Event;

	public class SegmentEvent extends Event
	{
		public static const CHANGER_VALUE_SEGMENT_EVENT :String ="changerValueSegmentEvent";
		public static const VALIDER_VALUE_SEGMENT_EVENT :String ="validerValueSegmentEvent";
		
		public function SegmentEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new SegmentEvent(type, bubbles, cancelable);
		}
	}
}