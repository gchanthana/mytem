package general.service.thread{	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.formatters.SwitchSymbolFormatter;
	import mx.rpc.events.ResultEvent;
	
	import utils.MessageAlerte;
	public class ThreadHandlers	{		private var myDatas : ThreadDatas;		public function ThreadHandlers(instanceOfDatas:ThreadDatas)		{			myDatas = instanceOfDatas;		}		internal function getThreadHandler(re:ResultEvent):void		{			if(re.result != null				&& re.result is Object				&& re.result.hasOwnProperty("STATUS")				&& re.result.STATUS == 1)				myDatas.treatProcessThread(re.result);			else				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.CODEERREUR),MessageAlerte.msgErrorPopupTitle);		}	}}