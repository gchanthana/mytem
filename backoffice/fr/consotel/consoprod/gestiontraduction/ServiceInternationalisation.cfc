<cfcomponent output="false">
	<cfset langid=1>
	
	<!-- FONCTION DE REMPLISSAGE DES COMBOS POUR LA RECHERCHE D'UNE PHRASE -->
	<cffunction name="fillComboLangue" access="remote" returntype="Query">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getLanguageList">
			<cfprocresult name="qGetLanguageList">
		</cfstoredproc>
		<cfreturn qGetLanguageList>
	</cffunction>
	
	<cffunction name="fillComboStatut" access="remote" returntype="Query">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getStatusList">
			<cfprocresult name="qGetStatusList">
		</cfstoredproc>
		<cfreturn qGetStatusList>
	</cffunction>
	
	<cffunction name="fillComboModule" access="remote" returntype="Query">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getModuleList">
			<cfprocresult name="qGetModuleList">
		</cfstoredproc>
		<cfreturn qGetModuleList>
	</cffunction>
	
	<!-- FONCTION DE RECUPERATION DE LA LISTE DES PHRASES -->
	<cffunction name="getListePhrases" access="remote" returntype="Query">
		<cfargument name="langid" type="string">
		<cfargument name="etat" type="string">
		<cfargument name="module" type="numeric">
		<cfargument name="cle" type="string">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getTransletedWords"> 
			<cfprocparam value="#langid#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#etat#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#module#" 	cfsqltype="cf_SQL_INTEGER" type="in">
			<cfprocparam value="#cle#" 		cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="qGetTransletedWords">
		</cfstoredproc>
		
		<cfreturn qGetTransletedWords>
	</cffunction>	
	
	<!-- FONCTION DE RECUPERATION DE LA LISTE DES PHRASES CORRESPONDANT A UNE LANGUE -->
	<cffunction name="getListePhrasesLangue" access="remote" returntype="Query">
		<cfargument name="langid" type="string">
		<cfargument name="etat" type="string">
		<cfargument name="module" type="numeric">
		<cfargument name="cle" type="string">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getTransWordsLang"> 
			<cfprocparam value="#langid#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#etat#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#module#" 	cfsqltype="cf_SQL_INTEGER" type="in">
			<cfprocparam value="#cle#" 		cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="qGetTransWordsLang">
		</cfstoredproc>
		
		<cfreturn qGetTransWordsLang>
	</cffunction>
		
		
<!-- 		<cfset qListePhrases = queryNew('ID,MODULE,PHRASE_SOURCE,STATUT,LANGUE,TRADUCTION','Integer,varchar,varchar,varchar,varchar,varchar')>

		<cfset queryAddRow(qListePhrases,3)>
		
		<cfset querySetCell(qListePhrases,"ID",1,1)>
		<cfset querySetCell(qListePhrases,"MODULE","tableau de bord",1)>
		<cfset querySetCell(qListePhrases,"PHRASE_SOURCE","Période étudiée",1)>
		<cfset querySetCell(qListePhrases,"STATUT","source proposée",1)>
		<cfset querySetCell(qListePhrases,"LANGUE","anglais",1)>
		<cfset querySetCell(qListePhrases,"TRADUCTION","Studied period",1)>
		
		<cfset querySetCell(qListePhrases,"ID",2,2)>
		<cfset querySetCell(qListePhrases,"MODULE","tableau de bord",2)>
		<cfset querySetCell(qListePhrases,"PHRASE_SOURCE","Tarif proposé",2)>
		<cfset querySetCell(qListePhrases,"STATUT","source proposée",2)>
		<cfset querySetCell(qListePhrases,"LANGUE","anglais",2)>
		<cfset querySetCell(qListePhrases,"TRADUCTION","",2)>
		
		<cfset querySetCell(qListePhrases,"ID",3,3)>
		<cfset querySetCell(qListePhrases,"MODULE","tableau de bord",3)>
		<cfset querySetCell(qListePhrases,"PHRASE_SOURCE","Tarif proposé",3)>
		<cfset querySetCell(qListePhrases,"STATUT","source validée",3)>
		<cfset querySetCell(qListePhrases,"LANGUE","anglais",3)>
		<cfset querySetCell(qListePhrases,"TRADUCTION","",3)>
				
		<!--- <cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_grcl_facturation_v3.get_list_profils">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="qListePhrases">
		</cfstoredproc> --->
		
		<cfreturn qListePhrases> -->
	
	
	<!-- FONCTION DE CREATION / MODIFICATION D'UNE PHRASE -->
	<cffunction name="createPhrase" access="remote" returntype="any">
		<cfargument name="statut" type="string">
		<cfargument name="module" type="numeric">
		<cfargument name="phrase" type="string">
		<cfargument name="commentaire" type="string">
		<cfargument name="langid" type="string">
		<cfargument name="traduction" type="string">
		<cfargument name="application" type="numeric">
		<cfargument name="univers" type="string">
		<cfargument name="fonction" type="string">
		<cfargument name="page" type="numeric">
		<cfargument name="type" type="string">
		<cfargument name="id" type="numeric">
						
		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_intranet.addwordtotranslate">
			<cfprocparam value="#statut#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#module#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#phrase#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#commentaire#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#langid#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#traduction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#application#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#univers#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#fonction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#page#" cfsqltype="CF_SQL_FLOAT" type="in">
			<cfprocparam value="#type#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<!-- cas de la création d'une phrase -->
			<cfif id eq -1>
				<cfprocparam value="#id#" cfsqltype="cf_SQL_FLOAT" type="in" null="true">
			<!-- cas de la modification d'une phrase -->
			<cfelse>
				<cfprocparam value="#id#" cfsqltype="cf_SQL_FLOAT" type="in" null="false">		
			</cfif>		
			<cfprocparam variable="qCreatePhrase" cfsqltype="CF_SQL_FLOAT" type="out">	
		</cfstoredproc>
					
		<cfreturn qCreatePhrase>
	
	</cffunction>
	
	<!-- FONCTION DE SUPPRESSION D'UNE PHRASE -->
	<cffunction name="deletePhrase" access="remote" returntype="numeric">
		<cfargument name="idPhrase" type="numeric" required="true">
 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.deleteWord">
			<cfprocparam value="#idPhrase#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="qDeleteWord">
			<!--- 
			<cfprocresult name="qDeleteWord">
			 --->
		</cfstoredproc>
		<cfreturn qDeleteWord>
	</cffunction>
	
	<!-- FONCTION D'ECRITURE DU FICHIER XML -->
	<cffunction name="createXmlFile" access="remote" returntype="any">
		<cfargument name="objetXML" type="XML" required="true">
		<cfargument name="fileName" type="string" required="true">
		
		<cftry>
			<cffile action="write" file="/webroot/backoffice-reda/fr/consotel/consoprod/gestiontraduction/dictionnairesXML/#fileName#" output="#objetXML#">
			<CFCatch type="any">
				<CFReturn cfcatch.Message> 			
			</CFCatch>
				
		</cftry>
		
		<cfreturn objetXML>
	</cffunction>
	
	<cffunction name="createXmlFileLocalisation" access="remote" returntype="xml">
		<cfargument name="objetXML" type="XML" required="true">
		<cfargument name="fileName" type="string" required="true">
		<cftry>
			<cffile action="write" file="/webroot/backoffice-reda/fr/consotel/consoprod/gestiontraduction/localisationsXML/#fileName#" output="#objetXML#">
			<cfreturn objetXML>
		<cfcatch>
			<cfreturn "ERROR">
		</cfcatch>
		</cftry>
	</cffunction>
	
	<!-- FONCTION D'EXPORT AU FORMAT CSV -->
	<cffunction name="exportCsvFile" access="remote" returntype="any">
	    <cfargument name="langid" type="string">
		<cfargument name="etat" type="string">
		<cfargument name="module" type="numeric">
		<cfargument name="cle" type="string">
		<cfargument name="nameFile" type="string">
		
	    <cftry> 
	       <!---  <cfset rootPath=expandPath("/")> --->
	        <cfset UnicId = createUUID()>
	        <cfset fileName = UnicId&"_"&"#nameFile#">                
	        <cfsavecontent variable="contentObj">
	            <cfsetting enablecfoutputonly="true"/>
	                            
	            <cfset qDetail= evaluate("getListePhrases(langid,etat,module,cle)")>                
	            <cfset NewLine = Chr(13) & Chr(10)>
	            <cfset space = Chr(13) & Chr(10)>        
	            <cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="UTF-8">
	            <cfcontent type="text/plain">
	            <!---            
	            <cfloop query="qDetail">
		            
	                <cfoutput>#qDetail['module'][i]#;#qDetail.module#;#NewLine#</cfoutput>
	            </cfloop>
	             ---> 
	            <cfoutput>identifiant;module;libelle_source;statut;traduction;#NewLine#</cfoutput>                
	            <cfloop query="qDetail">
	                <cfoutput>#TRIM(qDetail.ID)#;#TRIM(qDetail.NOM_MODULE)#;#TRIM(qDetail.LIBELLE_INITIAL)#;#TRIM(qDetail.STATUT)#;#TRIM(qDetail.TRADUCTION)#;#NewLine#</cfoutput>
	            </cfloop>
	        </cfsavecontent>        
	        <!--- Création du fichier CSV --->             
	        <cffile action="write" file="/webroot/backoffice-reda/fr/consotel/consoprod/gestiontraduction/fichiersatraduire/#fileName#" charset="Utf-8"
	                addnewline="true" fixnewline="true" output="#contentObj#">        
	        <cfreturn "#fileName#">
	     <cfcatch>
	     	<cfreturn "ERROR">
	     </cfcatch>
	     </cftry>
	</cffunction>
	
	
</cfcomponent>