<cfcomponent name="ScheduleService" alias="fr.consotel.consoview.bi.service.ScheduleService">
	<!---
	ATTENTION NE MARCHERA PAS SUR BI-1.CONSOTEL.FR EN RAISON DU SEPARATEUR DE CHEMIN DE FICHIER / (UNIX/SOLARIS)
	--->
	<cfset biServer="sun-bi">
	<cfset biService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
	<cfset authLogin=''>
	<cfset authPwd=''>
	
	<cffunction name="authLogin" access="public" returntype="numeric">
		<cfargument name="loginString" type="string" required="true">
		<cfargument name="pwdString" type="string" required="true">
		<cfset authLogin=arguments.loginString>
		<cfset authPwd=arguments.pwdString>
		<cfset authResult=biService.validateLogin(authLogin,authPwd)>
		<cfif authResult EQ TRUE>
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	
	<cffunction name="scheduleReport" access="public" returntype="any">
		<cfargument name="reportInfos" type="struct" required="true">
		<cfif validateReportInfos(arguments.reportInfos) EQ TRUE>
			<cfset reportAccess=biService.hasReportAccess(arguments.reportInfos["ABSOLUTE_PATH"],authLogin,authPwd)>
			<cfif reportAccess EQ TRUE>
				<cfset scheduleRequest=structNew()>
				<cfset scheduleRequest["jobLocale"]="fr-FR">
				<cfset scheduleRequest["notifyWhenFailed"]=FALSE>
				<cfset scheduleRequest["notifyWhenSuccess"]=FALSE>
				<cfset scheduleRequest["notifyWhenWarning"]=FALSE>
				<cfset scheduleRequest["schedulePublicOption"]=TRUE>
				<cfset scheduleRequest["notificationTo"]="cedric.rapiera@consotel.fr">
				<cfset scheduleRequest["reportRequest"]=createReportRequest(arguments.reportInfos)>
				<cfset scheduleRequest["deliveryRequest"]=createDeliveryRequest(arguments.reportInfos)>
				<cfset scheduleRequest["scheduleBurstringOption"]=arguments.reportInfos["BURST_OPTION"]>
				<cfset scheduleRequest["userJobName"]=arguments.reportInfos["JOB_NAME"]>
				<cfset scheduleReportReturn=biService.scheduleReport(scheduleRequest,authLogin,authPwd)>
				<cfreturn VAL(scheduleReportReturn)>
			<cfelse>
				<cfreturn -2>
			</cfif>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	 
	<cffunction name="validateReportInfos" access="private" returntype="boolean">
		<cfargument name="reportInfos" type="struct" required="true">
		<cfset boolAbsolutePath=structKeyExists(arguments.reportInfos,"ABSOLUTE_PATH")>
		<cfset boolArrayOfParameters=structKeyExists(arguments.reportInfos,"ARRAY_OF_PARAMETERS")>
		<cfset boolTemplate=structKeyExists(arguments.reportInfos,"TEMPLATE")>
		<cfset boolOutputDirectory=structKeyExists(arguments.reportInfos,"OUTPUT_DIRECTORY")>
		<cfset boolOutputFilename=structKeyExists(arguments.reportInfos,"OUTPUT_FILENAME")>
		<cfset boolOutputFormat=structKeyExists(arguments.reportInfos,"OUTPUT_FORMAT")>
		<cfset boolBurstOption=structKeyExists(arguments.reportInfos,"BURST_OPTION")>
		<cfset boolJobName=structKeyExists(arguments.reportInfos,"JOB_NAME")>
		<cfreturn (boolAbsolutePath AND boolArrayOfParameters AND boolOutputFilename AND boolBurstOption)>
	</cffunction>
	
	<cffunction name="createReportRequest" access="private" returntype="struct">
		<cfargument name="reportInfos" type="struct" required="true">
		<cfset newReportRequest=structnew()>
		<cfset newReportRequest["attributeLocale"]="fr-FR">
		<cfset newReportRequest["reportAbsolutePath"]=arguments.reportInfos["ABSOLUTE_PATH"]>
		<cfset newReportRequest["attributeTemplate"]=arguments.reportInfos["TEMPLATE"]>
		<cfset newReportRequest["attributeFormat"]=arguments.reportInfos["OUTPUT_FORMAT"]>
		<cfset newReportRequest["parameterNameValues"]=arguments.reportInfos["ARRAY_OF_PARAMETERS"]>
		<cfreturn newReportRequest>
	</cffunction>
	
	<cffunction name="createDeliveryRequest" access="private" returntype="struct">
		<cfargument name="reportInfos" type="struct" required="true">
		<cfset newLocalDeliveryOption=structNew()>
		<cfset newLocalDeliveryOption["destination"]= arguments.reportInfos["OUTPUT_DIRECTORY"] &
					arguments.reportInfos["OUTPUT_FILENAME"] & getFormatFileExtension(arguments.reportInfos["OUTPUT_FORMAT"])>
		<cfset newDeliveryRequest=structNew()>
		<cfset newDeliveryRequest["localOption"]=newLocalDeliveryOption>
		<cfreturn newDeliveryRequest>
	</cffunction>
	
	<cffunction name="getFormatFileExtension" access="private" returntype="string">
		<cfargument name="format" type="string" required="true">
		<cfif format EQ "EXCEL">
			<cfreturn ".xls">
		<cfelseif format EQ "PDF">
			<cfreturn ".pdf">
		<cfelseif format EQ "CSV">
			<cfreturn ".csv">
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
</cfcomponent>
