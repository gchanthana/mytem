<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSOtherService" hint="Interface décrivant les méthodes sur service : Other Services">
	<cffunction access="public" name="sendActivationSMS" returntype="String" hint="Envoi un SMS d'activation à un équipement et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="sendDownloadLink" returntype="String" hint="Envoi un lien de téléchargement à un équipement et retourne TRUE en cas de succès et FALSE sinon">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="getLicenseXML" returntype="String" hint="Retourne le contenu XML de la licence à utiliser pour activer le service de sécurité FSecure">
		<cfargument name="multiLicenseKey" type="String" required="true" hint="Multi License Key">
	</cffunction>
	
	<cffunction access="public" name="getSMSLicenseKey" returntype="String" hint="Retourne un SMS qui peut être envoyé par SMSC pour activer le service de sécurité FSecure">
		<cfargument name="singleLicenseKey" type="String" required="true" hint="Single Licence Key">
	</cffunction>
	
	<cffunction access="public" name="pregenerateKeycodes" returntype="Array" hint="Génère NumberOfKeysToGenerate Single Key Codes dans le Multi License par défaut et retourne la liste">
		<cfargument name="numberOfKeysToGenerate" type="String" required="true" hint="Nombre de Single Key Code à générer">
	</cffunction>
	
	<cffunction access="public" name="echo" returntype="String" hint="Retourne un ECHO du message">
		<cfargument name="echoMessage" type="String" required="true" hint="Message qui sera retourné par cette méthode">
	</cffunction>
</cfinterface>