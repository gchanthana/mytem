package datalert.ihm.fiches
{
	import flash.events.Event;
	
	import mx.containers.TabNavigator;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import datalert.event.DatalertEvent;
	import datalert.ihm.fiches.detailsFicheLigne.AlertesRenderer;
	import datalert.ihm.fiches.detailsFicheLigne.MessageRenderer;
	import datalert.ihm.fiches.detailsFicheLigne.SeuilsRenderer;
	import datalert.ihm.fiches.detailsFicheLigne.UsageRenderer;
	import datalert.services.ParamSeuilCS;
	import datalert.vo.LigneVO;

	public class FicheLigneImpl extends TitleWindow
	{
		[Bindable]
		public var ligne				:LigneVO;		
		public var usage				:UsageRenderer;
		public var seuil				:SeuilsRenderer;
		public var message				:MessageRenderer;
		public var alertes				:AlertesRenderer;
		public var paramseuilCS			:ParamSeuilCS;
		public var tabNavCommande		:TabNavigator;
		public var btnValider			:Button;
		public var btnFermer			:Button;
		
		public var imgTitle				:Image;
		
		public var boolAlertsHaveChanged:Boolean = false;
		
		[Embed(source="/assets/images/withAlert.png")]
		[Bindable]private var imgClass:Class;
		
		[Embed(source="/assets/images/withoutAlert.png")]
		[Bindable]private var imgClass1:Class;		
		
		
		public function FicheLigneImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);			
		}
		
		private function setfichesSeuilInfos(e:DatalertEvent):void
		{
			seuil.setCurrentData(ligne,paramseuilCS.seuilLigneVo);	
			
			/* test sur Ligne en situation d'alert */
			if (((ligne.BOOLDRT==1) && (ligne.ROAMINGALERT>=0) && (paramseuilCS.seuilLigneVo.BOOLDRTROAM == 1) && (ligne.ROAMINGDATA>ligne.ROAMINGALERT))
				||((ligne.BOOLDRT==1) && (ligne.DOMESTIQUEALERT>=0) && (paramseuilCS.seuilLigneVo.BOOLDRTDOM == 1) && (ligne.DOMESTIQUEDATA>ligne.DOMESTIQUEALERT)))
			{imgTitle.source=imgClass;}
			else
			{imgTitle.source=imgClass1;}
		}
		
		private function setfichesInfos():void
		{
			usage.setCurrentData(ligne);			
			message.setCurrentData(ligne.IDSOUSTETE);	
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{				
			setfichesInfos();
			paramseuilCS= new ParamSeuilCS();
			
			if (ligne.BOOLDRT==1) /* Tester si le service Datalert est activé*/
			{
				paramseuilCS.rechercheSeuilLigne(ligne.IDSOUSTETE);
			}else
			{
				paramseuilCS.rechercheSeuilLigne(-1);
			}
			paramseuilCS.addEventListener(DatalertEvent.PARAM_SEUILLIGNE_EVENT,setfichesSeuilInfos);
		}

		protected function tabNavCommande_ChangeHandler():void
		{
			if (tabNavCommande.selectedIndex != 2)
			{
				btnValider.visible = false;
				btnValider.x = 750;
				btnFermer.x = 410;
				btnFermer.label = resourceManager.getString('M26','Fermer');
			}
			else if(boolAlertsHaveChanged)
			{
				btnValider.visible = true;
				btnValider.x = 802;
				btnFermer.x = 660;
				btnFermer.label = resourceManager.getString('M26','Annuler');
			}
		}
		
		public function setExitMode(i:int):void
		{
			if(i == 0)
			{				
				btnValider.visible = false;
				btnValider.x = 750;
				btnFermer.x = 410;
				btnFermer.label = resourceManager.getString('M26','Fermer');
			}
			else if(i == 1)
			{
				btnValider.visible = true;
				btnValider.x = 802;
				btnFermer.x = 660;
				btnFermer.label = resourceManager.getString('M26','Annuler');
				boolAlertsHaveChanged = true;
			}
		}
		protected function closeHandler(ce:Event):void
		{
			dispatchEvent(new Event('DETAILS_CLOSED'));
			PopUpManager.removePopUp(this);
		}
		
		protected function validerHandler():void
		{
			alertes.btValider_ClickHandler();
		}
		
		
	}
}