package intranet.gestionLoginClasse
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class PanelAdresseIPseveralLogin extends PanelAdresseIPIHM
	{
		private var composantIp : ComposantInputAdrIp;
		private var colAdresseIp : ArrayCollection;
		public var classeIp:ArrayCollection = new ArrayCollection(
                [ {label:"Reseau de classe A", data:"255.0.0.0"}, 
                  {label:"Reseau de classe B", data:"255.255.0.0"}, 
                  {label:"Reseau de classe C", data:"255.255.255.0"},
                  {label:"Ordinateur", data:"255.255.255.255"},
                  {label:"Personnalisé",data:"perso"}
                ]);
		public function PanelAdresseIPseveralLogin(colAdresseIp)
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			this.colAdresseIp=colAdresseIp;
			composantIp = new ComposantInputAdrIp();
		}
		private function initIHM(evt : FlexEvent):void{
			boxIp.addChild(composantIp);
			var labInfo :Label = new Label();
			labInfo.text=("(Station ou Reseau)");
			labInfo.setStyle("fontStyle","italic");
			boxIp.addChild(labInfo);
			
			labErreurMasque.setVisible(false);
			comboClasse.dataProvider=classeIp;
			comboClasse.addEventListener(Event.CHANGE,updateClasseIp);
			comboClasse.selectedIndex=0;
			updateClasseIp(null);
			btEnregistrer.addEventListener(MouseEvent.CLICK,enregistrer);
			btFeremer.addEventListener(MouseEvent.CLICK,fermer);
			addEventListener(CloseEvent.CLOSE,fermer);
		}
		private function updateClasseIp(evt : Event):void
		{
			if(comboClasse.selectedItem.data == "perso"){
				
				viewstackMasque.selectedIndex = 1;
			}
			else
			{
				viewstackMasque.selectedIndex = 0;
				labIpInvar.text=comboClasse.selectedItem.data;
			}
		}
		private function enregistrer(evt : Event):void{
			var obj = new Object();
			obj.ADRESSE_IP = "111";
			obj.MASQUE1="4545";
			colAdresseIp.addItem(obj);
		}
		private function fermer(evt : Event):void{
			PopUpManager.removePopUp(this);	
		}
		

	}
}