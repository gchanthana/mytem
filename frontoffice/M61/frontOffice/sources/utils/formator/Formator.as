package utils.formator
{	
import mx.formatters.DateFormatter;
	
	public class Formator
	{
		//----------- METHODES -----------//
		
		public function Formator()
		{
		}
		
		/* convertir octets en megasOctets */
		public static function formatOctetsToMegaOctets(size:Number):String
		{
			var value:Number = size/1048576;
			
			return value.toPrecision(2);
		}
		
		/* formater la date */
		public static function format(str_dateString:String, str_dateFormat:String):String
		{
			var df:DateFormatter= new DateFormatter();
			df.formatString = str_dateFormat;
			return df.format(str_dateString);
		}
		
		
//		public static function giveMeDate(strg:String):String
//		{
//			var day		:String = strg.substr(8,2);
//			var month	:String	= strg.substr(5,2);
//			var year	:String	= strg.substr(0,4);
//			
//			return day + "/" + month + "/" + year;
//		}
//		
//		public static function formatReverseDate(date:Date):String
//		{
//			var day:String = date.date.toString();
//			
//			if(day.length < 2) 
//				day = "0" + day;
//			
//			var monthnbr :int = Number(date.month) + 1;
//			var month	 :String = monthnbr.toString();
//			
//			if(month.length < 2)
//				month = "0" + month;
//			
//			var year:String = date.fullYear.toString();
//			
//			return year + "/" + month + "/" + day;
//		}
//		
//		public static function formatHourConcat(dateobject:Date):String
//		{
//			var hours	:String = dateobject.hours.toString();
//			var minutes	:String = dateobject.minutes.toString();
//			var seconds	:String = dateobject.seconds.toString();
//
//			return hours + ":" + minutes + ":" + seconds;
//		}
//		
//		public static function formatHour(dateobject:Date):Object
//		{
//			var hoursParsing	:Object = new Object();
//			var hours			:String = dateobject.hours.toString();
//			var minutes			:String = dateobject.minutes.toString();
//			var seconds			:String = dateobject.seconds.toString();
//			
//			hoursParsing.HOURS	= hours;
//			hoursParsing.MIN	= minutes;
//			hoursParsing.SEC	= seconds;
//			
//			return hoursParsing;
//		}
//		
//		public static function giveMeReverseStringDate(strg:String):String
//		{
//			var day:String 		= strg.substr(8,2);
//			var month:String	= strg.substr(5,2);
//			var year:String		= strg.substr(0,4);
//			
//			return  year + "/" + month + "/" + day;
//		}
//		
//		public static function formatDateStringInDate(dateString:String):Date
//		{
//			if(dateString == null)
//				return null;
//			var dateInArray:Array = dateString.split("/");
//			var dateFormated:Date = new Date(dateInArray[2],int(dateInArray[1])-1,dateInArray[0]);
//			return dateFormated;
//		}
//		
//		public static function searchVirguleOrPoint(price:String):Number
//		{
//			if(Number(price) < 0)
//				price = price.replace(",",".");
//			
//			return Number(price);
//		}
//		
//		public static function formatInClob(arrayInString:Array):String
//		{
//			var dataInClob:String = "";
//			
//			for(var i:int = 0; i < arrayInString.length;i++)
//			{
//				if(i == 0)
//					dataInClob = arrayInString[i];
//				else
//					dataInClob = dataInClob + arrayInString[i];
//				
//				if(i != arrayInString.length-1)
//					dataInClob = dataInClob + ",";
//			}
//			
//			var lastIndex:int = dataInClob.lastIndexOf(',', dataInClob.length);
//			
//			if(lastIndex == (dataInClob.length -1))
//				dataInClob = dataInClob.slice(0, lastIndex);
//			
//			return dataInClob;
//		}
//		
//		public static function formatInClobWithDatafieled(arrayInString:ArrayCollection, dataField:String):String
//		{
//			var dataInClob:String = "";
//			
//			if(arrayInString == null) return '';
//			
//			for(var i:int = 0; i < arrayInString.length;i++)
//			{
//				if(i == 0)
//					dataInClob = arrayInString[i][dataField];
//				else
//					dataInClob = dataInClob + arrayInString[i][dataField];
//				
//				if(i != arrayInString.length-1)
//					dataInClob = dataInClob + ",";
//			}
//			
//			var lastIndex:int = dataInClob.lastIndexOf(',', dataInClob.length);
//			
//			if(lastIndex == (dataInClob.length -1))
//				dataInClob = dataInClob.slice(0, lastIndex);
//			
//			return dataInClob;
//		}
//	    
	    public static function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			
			if(day.length < 2)
				day = "0" + day;
			
			var temp	:int = Number(dateobject.month) + 1;
			var month	:String = temp.toString();
			
			if(month.length < 2)
				month = "0" + month;
			
			var year:String = dateobject.fullYear;
			
			return day + "/" + month + "/" + year;
		}
//		
//		public static function formatInteger(intToFormat:int):Boolean
//		{
//			var bool:Boolean = false;
//			
//			if(intToFormat == 1)
//				bool = true;
//			
//			return bool;
//		}
//		
//		public static function formatBoolean(boolToFormat:Boolean):int
//		{
//			var integer:int = 0;
//			
//			if(boolToFormat)
//				integer = 1;
//			
//			return integer;
//		}
//		
//		public static function giveMeFPCDate(fpc:String):String
//		{
//			var datefpc	:String = fpc;
//			var day		:String = datefpc.substr(8,2);
//			var month	:String	= datefpc.substr(5,2);
//			var year	:String	= datefpc.substr(0,4);
//			
//			return day + "/" + month + "/" + year;
//		}
//		
//		public static  function getSuffixeFileName():String
//		{
//			var fileName:String = '';
//			var myDate	:Date = new Date();
//			var dateStrg:String = DateFunction.formatDateAsString(myDate);
//			var timeStrg:String = getZeroOrNot(myDate.getHours().toString()) + '_' +  getZeroOrNot(myDate.getMinutes().toString()) + '_' + getZeroOrNot(myDate.getSeconds().toString()) + '.xls';
//			var dateArry:Array = dateStrg.split('/');
//			var lenDate	:int = dateArry.length;
//			
//			for(var i:int = 0;i < lenDate;i++)
//			{
//				fileName = fileName + dateArry[i] + '_';
//			}
//			
//			fileName = fileName + timeStrg;
//			
//			return fileName;
//		}
//		
//		public static  function getZeroOrNot(value:String):String
//		{
//			var newValue:String = '';
//			
//			if(value.length == 1)
//				newValue = '0' + value;
//			else
//				newValue = value;
//			
//			return newValue;
//		}
//		
//		public static function sortFunction(value:ArrayCollection, label:String, isDescending:Boolean = false):ArrayCollection
//	    {
//			if(value == null) return new ArrayCollection();
//			
//	    	var tri:Sort = new Sort();
//	    		tri.fields 	 = [new SortField(label, false, isDescending)];
//			
//			value.sort 	 = tri;
//			value.refresh();
//			value.sort = null;
//			
//			return value;
//	    }

	}
}