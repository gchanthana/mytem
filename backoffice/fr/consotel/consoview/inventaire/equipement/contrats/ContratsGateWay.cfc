<cfcomponent output="false">
	
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : retourne un objet de type Contrat
		
		param in 
			- id l'identifiant du contrat
		param out 
			- un objet de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat		
			
	--->	
	<cffunction name="get" output="false" access="remote">
		<cfargument name="id" required="true" />
 		<cfreturn createObject("component", "Contrat").init(arguments.id)>
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : enregistre les propriÃ©tÃ©s d'un objet de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat
		
		param in 
			- l'objet ï¿½ enregistrer
		param out 
			- l'id de l'objet enregistrÃ©
	--->
	<cffunction name="save" output="false" access="remote">
		<cfargument name="obj" type="Contrat" required="true" />
 		<cfreturn obj.save() />
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : enregistre les propriÃ©tÃ©s d'un objet de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat
		
		param in 
			- l'objet ï¿½ effacÃ©
		param out 
			- l'id de l'objet Ã©ffacÃ©
	--->
	<cffunction name="delete" output="false" access="remote">
		<cfargument name="id" required="true" />
		<cfset var obj = get(arguments.id)>
		<cfset obj.delete()>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
				
		Description : retourne un tableau de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat[] avec tous les contrats de la base
		
		param out 
			- un tableau d'objets de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat
	--->
	<cffunction name="getAll" output="false" access="remote" returntype="fr.consotel.consoview.inventaire.equipement.contrats.Contrat[]">
		<cfset var qRead="">
		<cfset var obj="">
		<cfset var ret=arrayNew(1)>

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select IDCONTRAT
			from OFFRE.CONTRAT
		</cfquery>

		<cfloop query="qRead">
		<cfscript>
			obj = createObject("component", "Contrat").init(qRead.IDCONTRAT);
			ArrayAppend(ret, obj);
		</cfscript>
		</cfloop>
		<cfreturn ret>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
				
		Description : retourne un tableau de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat[]
					  avec tous les contrats d'un equipement
		param in 
			- l'identifiant de l'equipement
		param out 
			- un tableau d'objets de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat
	--->
	<cffunction name="getAllForEquipement" output="false" access="remote" returntype="fr.consotel.consoview.inventaire.equipement.contrats.Contrat[]">
		<cfargument name="idEquipement" required="true"/>
		<cfset var qRead="">
		<cfset var obj="">
		<cfset var ret=arrayNew(1)>
		
		 <cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			SELECT *
			from CONTRAT_EQUIPEMENT CE, 
			CONTRAT CTRT
			where CE.IDEQUIPEMENT = #idEquipement#
			AND CTRT.IDCONTRAT = CE.IDCONTRAT 
			order by CTRT.DATE_SIGNATURE DESC
	 
		</cfquery>
 
		<cfloop query="qRead">
			<cfset obj = createObject("component", "Contrat").init(qRead.IDCONTRAT)/>
			<cfset ArrayAppend(ret, obj)/>
		</cfloop>  
		<cfreturn ret>
	</cffunction>


	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
				
		Description : retourne une requete avec tous les contrats de la base
		
		param out 
			- un tableau d'objets de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat
	--->
	<cffunction name="getAllAsQuery" output="false" access="remote" returntype="query">
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">

		<cfset var qRead="">

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#
			from OFFRE.CONTRAT
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
				
		Description : retourne une requete avec tous les contrats pour un equipement

		param in 
			- l'identifiant de l'equipement
		param out 
			- un tableau d'objets de type fr.consotel.consoview.inventaire.equipement.contrats.Contrat
	--->
	<cffunction name="getAllForEquipementAsQuery" output="false" access="remote" returntype="query">
		<cfargument name="idEquipement" required="true"/>
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">

		<cfset var qRead="">

		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			 
			
			
			SELECT *
			from CONTRAT_EQUIPEMENT CE, 
			CONTRAT CTRT
			where CE.IDEQUIPEMENT = #idEquipement#
			AND CTRT.IDCONTRAT = CE.IDCONTRAT 
			order by CTRT.DATE_SIGNATURE DESC
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/16/2007
		
		Description : retourne la liste des types de contrat 

		param in			
			- la liste des parametres de sortie de la requete (xxx,xxx,xxx,xxx,xxxx,xxxx)
		param out 
			- la requete
	--->	
	<cffunction name="getTypes" output="false" access="remote" returntype="query">
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">
		<cfset var qRead="">
		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#
			from OFFRE.TYPE_CONTRAT
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 8/14/2007
		
		Description : retourne la liste des fournisseur du groupe et les fournisseurs globaux

		param in			
			- la liste des parametres de retour de la requete
		param out 
			- la requete
	--->	
	<cffunction name="getFournisseurs" output="false" access="remote" returntype="query">		
		<cfargument name="fieldlist" default="*" hint="List of columns to be returned in the query.">
		<cfset var qRead="">
		<cfquery name="qRead" datasource="#SESSION.OFFREDSN#">
			select #arguments.fieldList#			       			   
			from   fournisseur
			where  idgroupe_client = #session.perimetre.id_groupe# 
			or     idgroupe_client = #session.perimetre.id_perimetre# 
			or     idgroupe_client is null			 
		</cfquery>

		<cfreturn qRead>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/15/2007
		
		Description : recherche des contrats
	
	--->
	<cffunction name="rechercherContrats" access="remote" output="false" returntype="array">	
		<cfargument name="chaine" type="string" required="true">	
		<cfset p_chaine = LCASE(chaine)>
		<!--- 
			--------------------------------------------------------------------------
		    -- recherche des contrats 								                --
		   	--------------------------------------------------------------------------
		        pkg_cv_equipement.search_contrat(p_idracine => :p_idracine,
                                   p_idgroupe => :p_idgroupe,
                                   p_chaine => :p_chaine,
                                   p_retour => :p_retour);
								
				<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_equipement.search_contrat">
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#1463#"/>
					<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#18153#"/>
					<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_idgroupe" value="#chaine#"/>
					<cfprocresult name="qCont"/>        
					
				</cfstoredproc>	


		 --->
		
			<cfquery name="qCont" datasource="#SESSION.OFFREDSN#">
				SELECT  e.idequipement,
	              e.libelle_eq AS materiel,
	              tc.type_contrat,
	              contrat.idcontrat,
	              contrat.reference_contrat,
	              contrat.date_signature,
	              ' <' || (floor((contrat.date_echeance - SYSDATE) / 30.5) + 1) ||
	              ' mois' AS echeance,
	              contrat.duree_contrat AS duree,
	              fo.nom_fournisseur AS fournisseur,
	              decode(contrat.tacite_reconduction, 1, 'Oui', 'Non') AS tacite_reconduction,
	              contrat.montant_contrat AS montant
				FROM   contrat,
				              type_contrat tc,
				              contrat_equipement coe,
				              equipement e,
				       fournisseur fo
				WHERE  contrat.idtype_contrat = tc.idtype_contrat
				              AND contrat.idcontrat = coe.idcontrat
				              AND contrat.date_echeance <= add_months(SYSDATE, 30)
				              AND contrat.date_echeance > SYSDATE
				              AND coe.idequipement = e.idequipement
				              AND contrat.idracine=#getidGroupe()#
	      		 AND contrat.idfournisseur=fo.idfournisseur
	              AND (lower(TRIM(e.libelle_eq)) LIKE '%' || '#p_chaine#' || '%' OR
	              lower(TRIM(type_contrat)) LIKE '%' || '#p_chaine#' || '%' OR
	              lower(TRIM(contrat.reference_contrat)) LIKE
	              '%' || '#p_chaine#' || '%' OR
	              lower(TRIM(to_char(contrat.date_signature, 'dd/mm/yyyy'))) LIKE
	              '%' || '#p_chaine#' || '%' OR
	              lower(TRIM(to_char(contrat.date_echeance, 'dd/mm/yyyy'))) LIKE
	              '%' || '#p_chaine#' || '%' OR lower(TRIM(contrat.duree_contrat)) LIKE
	              '%' || '#p_chaine#' || '%' OR
	              lower(TRIM(fo.nom_fournisseur)) LIKE '%' || '#p_chaine#' || '%')

			</cfquery>
		
					
		<cfquery name="qTotal" dbtype="query">
			select sum(montant) as MONTANT_TOTAL, 'TOTAL' as TOTAL
			from qCont
		</cfquery>
		<cfset data = arrayNew(1)>
		<cfset arrayAppend(data,qCont)>
		<cfset arrayAppend(data,qTotal)>			
		<cfreturn data>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/15/2007
		
		Description : recherche des contrats
	
	--->
	<cffunction name="rechercherEcheances" access="remote" output="false" returntype="array">	
		<cfargument name="chaine" type="string" required="true">	
		<!--- 
			--------------------------------------------------------------------------
		    -- recherche des contrats arrivant ï¿½ Ã©cheance						    --
		   	--------------------------------------------------------------------------
		        pkg_cv_equipement.search_echeance(p_idracine => :p_idracine,
                                    p_idgroupe => :p_idgroupe,
                                    p_chaine => :p_chaine,
                                    p_retour => :p_retour);


		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_equipement.search_echeance">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_idgroupe" value="#chaine#"/>
			<cfprocresult name="qCont"/>        
		</cfstoredproc>				
		<cfquery name="qTotal" dbtype="query">
			select sum(MONTANT) as MONTANT_TOTAL, 'TOTAL' as TOTAL
			from qCont
		</cfquery>
		<cfset data = arrayNew(1)>
		<cfset arrayAppend(data,qCont)>
		<cfset arrayAppend(data,qTotal)>			
		<cfreturn data>
	</cffunction>
	 
</cfcomponent>