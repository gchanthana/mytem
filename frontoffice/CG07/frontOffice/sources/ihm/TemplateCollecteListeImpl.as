package ihm
{
    import entity.ModelLocator;
    import entity.StaticData;
    import entity.vo.CollecteTypeVO;
    import entity.vo.FilterCollecteTypeVO;
    import entity.vo.OperateurVO;
    import entity.vo.SimpleEntityVO;
    
    import event.FctGestionCollecteEvent;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import ihm.component.alert.AlertExtended;
    import ihm.component.alert.AlertExtendedPopup;
    import ihm.popup.PopupTemplateCollecteFicheIHM;
    import ihm.popup.PopupTemplateCollecteFicheImpl;
    
    import mx.collections.ArrayCollection;
    import mx.containers.Accordion;
    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.Button;
    import mx.controls.ComboBox;
    import mx.controls.DataGrid;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.core.UIComponent;
    import mx.events.DataGridEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.resources.ResourceManager;
    import mx.utils.ObjectUtil;
    
    import service.FctGestionCollecteService;
    
    /**
     *
     *
     */
    [Bindable]
    public class TemplateCollecteListeImpl extends VBox
    {
        // INDEX -----------------------
        public static const VIEW_GLOBAL:String = "view_global";
        public static const VIEW_PJ:String = "view_pj";
        // global
        public var dataLocator:ModelLocator;
        public var collecteService:FctGestionCollecteService;
        // IHM -------------------------
        public var i_btnNewCollecte:Button;
        public var i_btnExportCollecte:Button;
        public var i_btnRetour:Button;
        public var i_accordeon:Accordion;
        public var i_cbxVue:ComboBox;
        public var i_cbxOperateur:ComboBox;
        public var i_cbxDataType:ComboBox;
        public var i_cbxRechercheRapide:ComboBox;
        public var i_cbxRecupMode:ComboBox;
        public var i_dgListCollecteClient:DataGrid;
        public var i_dgListCollecteType:DataGrid;
        public var i_colEdit:DataGridColumn;
        public var i_colName:DataGridColumn;
        public var i_colIdTemplate:DataGridColumn;
        public var i_colPublish:DataGridColumn;
        public var i_colComplete:DataGridColumn;
        public var i_colOperateur:DataGridColumn;
        public var i_colData:DataGridColumn;
        public var i_colRecup:DataGridColumn;
        public var i_colPresta:DataGridColumn;
        public var i_colContraintes:DataGridColumn;
        public var i_colAnterio:DataGridColumn;
        public var i_colCout:DataGridColumn;
        public var i_colSouscription:DataGridColumn;
        public var i_colContact:DataGridColumn;
        public var i_colRoic:DataGridColumn;
        public var i_colRocf:DataGridColumn;
        public var i_colDelai:DataGridColumn;
        public var i_colDateCreate:DataGridColumn;
        public var i_colDateModif:DataGridColumn;
        // DATA LOCALES
        public var operateurList:ArrayCollection = new ArrayCollection();
        // ressouces locales
        [Embed(systemFont='Arial', fontName='ArialR', mimeType='application/x-font')]
        public var ArialR:Class;
        protected var imageEdit:Class;
        protected var imageNew:Class;
        protected var imageDelete:Class;
        // gestion du grid
        private var col:String;
        private var colIndex:int;
        // objets metier
        public var filterVO:FilterCollecteTypeVO;
        private var _collecteVOListeFiltered:ArrayCollection;

        public function TemplateCollecteListeImpl()
        {
            super();
        }

        public function init():void
        {
            dataLocator = ModelLocator.getInstance();
            dataLocator.initGlobalDataApplication(); //=> déplacé vers CollecteClientListeImpl.as car onglets intervertis
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
        }

        public function onCreationCompleteHandler(e:FlexEvent):void
        {
            this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
            initListeners();
            initDisplay();
            //initData();
        }

        /**
         * Initialise l'affichage de cette interface.
         *
         */
        private function initDisplay():void
        {
            imageEdit = StaticData.imgEdit;
            imageDelete = StaticData.imgDelete;
            imageNew = StaticData.imgNew;
            // Attachement des colonnes au datagrid
            buildDatagrid(VIEW_GLOBAL);
        }

        /**
         * Initialise les écouteurs.
         */
        private function initListeners():void
        {
            i_dgListCollecteType.addEventListener(DataGridEvent.HEADER_RELEASE, i_dgListCollecteType_headerRelease);
            i_cbxVue.addEventListener(Event.CHANGE, i_cbxVue_changeHandler);
            i_cbxRechercheRapide.addEventListener(Event.CHANGE, i_cbxRechercheRapide_changeHandler);
            i_cbxDataType.addEventListener(Event.CHANGE, i_cbxDataType_changeHandler);
            i_cbxOperateur.addEventListener(Event.CHANGE, i_cbxOperateur_changeHandler);
            i_cbxRecupMode.addEventListener(Event.CHANGE, i_cbxRecupMode_changeHandler);
            // Bouton
            i_btnExportCollecte.addEventListener(MouseEvent.CLICK, i_btnExportCollecte_clickHandler);
            i_btnNewCollecte.addEventListener(MouseEvent.CLICK, i_btnNewCollecte_clickHandler);
            // Model de données
            dataLocator.addEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS, templateListe_loadHandler);
        }

        /**
         * Au click sur le bouton "Exporter en cvs"
         * Déclenche la fonction d'export de la liste des collec
         */
        public function i_btnExportCollecte_clickHandler(event:MouseEvent):void
        {
            // à décommenter dès mise en place de l'export coté BI
            dataLocator.exportListeCollecteType(filterVO);
            //dataLocator.consalerte(this, "Export en cours d'implémentation", "Export de la liste des templates", StaticData.imgTodo);
        }

        /**
         * Au click sur le bouton "Creer une nouvelle collecte Type"
         * Déclenche l'ouverture d'un popup de création d'un template
         * @return void.
         *
         */
        public function i_btnNewCollecte_clickHandler(event:MouseEvent):void
        {
            createNewTemplate(event);
        }

        /**
         * Handler captant le succés du (re)chargement
         * de la liste des template depuis la base
         * Applique le filtre sur datagrid pour rafraichir la vue
         */
        private function templateListe_loadHandler(event:FctGestionCollecteEvent):void
        {
            dataLocator.removeEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS, templateListe_loadHandler);
            // initialisation du filtre
            if(!filterVO)
            {
                filterVO = new FilterCollecteTypeVO();
            }
            filterDg(filterVO);
        }

        /**
         * Handler captant la demande de (re)chargement
         *  de la liste des templates et l'ouverture
         *  d'une alerte indiquant que le template
         *  a bien été crée/édité
         */
        private function templateListe_refreshHandler(event:FctGestionCollecteEvent):void
        {
            dataLocator.addEventListener(FctGestionCollecteEvent.TEMPLATE_LIST_LOADED_SUCCESS, templateListe_loadHandler);
            dataLocator.feedTemplateListInitial();
			FctGestionCollecteEvent(event).currentTarget.removeEventListener(FctGestionCollecteEvent(event).type, templateListe_refreshHandler);
        }

        /**
         * Capte le click sur le header du datagrid de la liste des templates
         */
        private function i_dgListCollecteType_headerRelease(event:DataGridEvent):void
        {
            colIndex = DataGridEvent(event).columnIndex;
            col = i_dgListCollecteType.columns[colIndex].dataField;
        }

        /**
         * Assure le Tri numérique d'une colonne :
         * <code> sortCompareFunction = "numericSortFunction"</code>
         */
        public function numericSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.numericCompare(itemA[col], itemB[col]);
        }

        /**
         * Assure le Tri d'item de type <code>Date</code> d'une colonne :
         * <code> sortCompareFunction = "dateSortFunction"</code>
         */
        public function dateSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.dateCompare(itemA[col] as Date, itemB[col] as Date);
        }

        /**
         * Assure le Tri alphabétique  d'une colonne :
         * <code> sortCompareFunction = "stringSortFunction"</code>
         */
        public function stringSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.stringCompare(itemA[col], itemB[col]);
        }

        /**
         * Assure le Tri alphabétique d'items comprenant un champs label d'une colonne :
         * <code> sortCompareFunction = "stringLabelSortFunction"</code>
         */
        public function stringLabelSortFunction(itemA:Object, itemB:Object):int
        {
            return ObjectUtil.stringCompare(itemA[col].label, itemB[col].label);
        }

        /**
         * Reconstruit l'agencement des colonnes du Datagrid listant les templates
         * <code> i_dgListCollecteType</code> selon la vue demandée par l'utilisateur
         * (cf combo de filtre : Vue Générale/Vue Pièce Jointe)
         */
        private function buildDatagrid(view:String):void
        {
            switch(view)
            {
                case VIEW_GLOBAL:
                    this.addColumnInDG(i_dgListCollecteType, [ i_colEdit, i_colIdTemplate, i_colName, i_colPublish, i_colComplete, i_colOperateur, i_colData,
                                                               i_colRecup, i_colPresta, i_colContraintes, i_colAnterio, i_colCout, i_colSouscription, i_colContact,
                                                               i_colRoic, i_colRocf, i_colDelai, i_colDateCreate, i_colDateModif ]);
                    break;
                case VIEW_PJ:
                    this.addColumnInDG(i_dgListCollecteType, [ i_colEdit, i_colIdTemplate, i_colName, i_colPresta, i_colSouscription, i_colRoic, i_colRocf,
                                                               i_colPublish, i_colComplete, i_colOperateur, i_colData, i_colRecup, i_colContraintes, i_colAnterio,
                                                               i_colCout, i_colContact, i_colDelai, i_colDateCreate, i_colDateModif ]);
                    break;
                default:
                    break;
            }
        }

        /**
         *
         * @param event
         */
        protected function onData_loadHandler(event:FctGestionCollecteEvent):void
        {
			trace("Entrée dans onData_loadHandler");
            switch(FctGestionCollecteEvent(event).type)
            {
                // vue générale
                case FctGestionCollecteEvent.OPERATEUR_LIST_LOADED_SUCCESS:
                    operateurList = collecteService.myDatas.operateurList;
                    operateurList.refresh();
                    break;
                default:
                    break;
            }
        }

        /**
         * <p>
         *  Capte le changement de selection du combo de filtre Vue
         * Déclenche la reconstruction du datagrid
         * </p>
         * @return void.
         *
         */
        protected function i_cbxVue_changeHandler(event:Event):void
        {
            var cb:ComboBox = Event(event).currentTarget as ComboBox;
            switch(cb.selectedIndex)
            {
                // vue générale
                case 0:
                    buildDatagrid(VIEW_GLOBAL);
                    break;
                // Vue pièce jointe
                case 1:
                    buildDatagrid(VIEW_PJ);
                    break;
                default:
                    buildDatagrid(VIEW_GLOBAL);
                    break;
            }
        }

        /**
         * Capte le changement de selection du combo de filtre 'Vue'
         * Déclenche la reconstruction du datagrid.
         *
         */
        protected function i_cbxRechercheRapide_changeHandler(event:Event):void
        {
            if(filterVO)
            {
                //  recherche de tous les templates
                if(i_cbxRechercheRapide.selectedIndex == 0)
                {
                    filterVO.templateComplete = 2;
                }
                else if(i_cbxRechercheRapide.selectedIndex == 1)
                    // recherche des templates avec info manquantes			
                {
                    filterVO.templateComplete = 0;
                }
                else
                {
                    filterVO.templateComplete = 2;
                }
                filterDg(filterVO);
            }
        }

        /**
         * Capte le changement de selection du combo de filtre 'Type de données'.
         * Déclenche le filtrage du datagrid <code> i_dgListCollecteType</code> en
         * fonction du type de données selectionné .
         */
        protected function i_cbxDataType_changeHandler(event:Event):void
        {
            if(filterVO)
            {
                if((i_cbxDataType.selectedItem as SimpleEntityVO).value as Number != 0)
                {
                    filterVO.selectedDataTypeId = SimpleEntityVO(i_cbxDataType.selectedItem).value as Number;
                }
                else
                {
                    filterVO.selectedDataTypeId = 0;
                }
                filterDg(filterVO);
            }
        }

        /**
         * Capte le changement de selection du combo de filtre 'Opérateur'
         * Déclenche le filtrage du datagrid <code>i_dgListCollecteType</code> en
         * fonction de l'operateur selectionné .
         */
        protected function i_cbxOperateur_changeHandler(event:Event):void
        {
            if(filterVO)
            {
                if((i_cbxOperateur.selectedItem as OperateurVO).value as Number != 0)
                {
                    filterVO.selectedOperateurId = OperateurVO(i_cbxOperateur.selectedItem).value as Number;
                }
                else
                {
                    filterVO.selectedOperateurId = 0;
                }
                filterDg(filterVO);
            }
        }

        /**
         * Capte le changement de selection du combo de filtre 'Mode de récupération'
         * Déclenche le filtrage du datagrid <code> i_dgListCollecteType</code> en
         * fonction du mode de récupération selectionné .
         */
        protected function i_cbxRecupMode_changeHandler(event:Event):void
        {
            if(filterVO)
            {
                if((i_cbxRecupMode.selectedItem as SimpleEntityVO).value as Number != 0)
                {
                    filterVO.selectedRecuperationModeId = SimpleEntityVO(i_cbxRecupMode.selectedItem).value as Number;
                }
                else
                {
                    filterVO.selectedRecuperationModeId = 0;
                }
                filterDg(filterVO);
            }
        }

        /**
         * Parcours la liste  des templates de <code>i_dgListCollecteType</code>
         * pour lui appliquer un filtre <code>filterVO</code> en fonction
         * de l'opérateur,du type de données et du mode de récupération selectionnés
         * dans la section filtre.
         * Met à jour le dataprovider du Datagrid <code>collecteVOListeFiltered</code>
         */
        public function filterDg(filterVO:FilterCollecteTypeVO):void
        {
            collecteVOListeFiltered = new ArrayCollection();
            var collec:ArrayCollection = dataLocator.collecteTypeList;
            // si la valeur du filtre est à null , retour de tous les champs
            for(var i:int = 0; i < collec.length; i++)
            {
                var vo:CollecteTypeVO = collec.getItemAt(i) as CollecteTypeVO;
                if((filterVO.selectedDataTypeId == 0 || vo.idcollectType == filterVO.selectedDataTypeId) && (filterVO.selectedOperateurId == 0 || vo.operateurId ==
                    filterVO.selectedOperateurId) && (filterVO.selectedRecuperationModeId == 0 || vo.idcollectMode == filterVO.selectedRecuperationModeId) &&
                    (filterVO.templateComplete == 2 || vo.isCompleted.value == filterVO.templateComplete))
                {
                    collecteVOListeFiltered.addItem(vo);
                }
            }
            i_dgListCollecteType.dataProvider = collecteVOListeFiltered;
            ArrayCollection(i_dgListCollecteType.dataProvider).sort;
            (i_dgListCollecteType.dataProvider as ArrayCollection).refresh();
        }

        /**
         *
         * @param event
         */
        protected function onNavToRetourHandler(event:MouseEvent):void
        {
            var popUp:AlertExtendedPopup = new AlertExtendedPopup();
            popUp.addEventListener(AlertExtended.USER_ACTION_EVENT, onActionFromPopupHandler);
            PopUpManager.addPopUp(popUp, this, true);
            PopUpManager.centerPopUp(popUp);
            popUp.btn1.label = ResourceManager.getInstance().getString('CG07', 'Conserver_les_infos_de_la_Fiche');
            popUp.btn2.label = ResourceManager.getInstance().getString('CG07', '_Quitter_la_Fiche_sans_sauvegarder');
        }

        /**
         *
         * @param event
         */
        private function onActionFromPopupHandler(event:Event):void
        {
            var popup:AlertExtendedPopup = AlertExtendedPopup(Event(event).currentTarget);
            popup.removeEventListener(AlertExtended.USER_ACTION_EVENT, onActionFromPopupHandler)
        }

        /**
         * Au click sur le bouton "Créer un nouveau template",
         * Ouvre et prépare  la fiche popup <code>NewCollecteTypeIHM</code>
         * de création d'un template
         */
        public function createNewTemplate(event:MouseEvent):void
        {
            var popUp:PopupTemplateCollecteFicheIHM = new PopupTemplateCollecteFicheIHM();
            popUp.currentCollectTypeVO = new CollecteTypeVO();
            popUp.mode = PopupTemplateCollecteFicheImpl.MODE_CREATION;
            popUp.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS, templateCreateUpdate_handler);
            popUp.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, templateCreateUpdate_handler);
            popUp.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_CANCEL, templateCreateUpdate_handler);
            //popUp.updateDisplay();
            PopUpManager.addPopUp(popUp, this, true);
            PopUpManager.centerPopUp(popUp);
        }

        private function templateCreateUpdate_handler(event:FctGestionCollecteEvent):void
        {
            var icone:Class = StaticData.imgValid;
            switch(FctGestionCollecteEvent(event).type)
            {
                // Mise à jour OK
                case FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS:
                    templateListe_refreshHandler(event);
                    icon = StaticData.imgValid;
                    dataLocator.consalerte(this, FctGestionCollecteEvent(event).obj as String, "", icone);
                    break;
                // Mise à jour KO
                case FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR:
                    icone = StaticData.imgNoValid;
                    dataLocator.consalerte(this, FctGestionCollecteEvent(event).obj as String, "", icone);
                    break;
                case FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_CANCEL:
                    // rafraichissement de la liste
                    templateListe_refreshHandler(event);
                    break;
                default:
                    break;
            }
        }

        /**
         * Au click sur l'icone d'edition présent devant chaque template de la liste,
         * Ouvre et prépare  la fiche popup <code>NewCollecteTypeIHM</code> en
         * Mode édition
         */
        public function btnEditSelectedCollecteTypeClickHandler(event:MouseEvent):void
        {
            var popUp:PopupTemplateCollecteFicheIHM = new PopupTemplateCollecteFicheIHM();
            popUp.mode = PopupTemplateCollecteFicheImpl.MODE_EDITION;
            PopUpManager.addPopUp(popUp, this, true);
            PopUpManager.centerPopUp(popUp);
            popUp.currentCollectTypeVO = i_dgListCollecteType.selectedItem as CollecteTypeVO;
            popUp.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_SUCCESS, templateCreateUpdate_handler);
            popUp.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_ERROR, templateCreateUpdate_handler);
            popUp.addEventListener(FctGestionCollecteEvent.TEMPLATE_CREATED_UPDATED_CANCEL, templateCreateUpdate_handler);
            popUp.updateDisplay();
        }

        /**
         * Ajoutes toutes les DataGridColumn contenu dans le tableau
         * passé en param
         */
        public function addColumnInDG(dg:DataGrid, myColumns:Array):void
        {
            var dgColumns:Array = dg.columns;
            dgColumns = new Array();
            for each(var obj:Object in myColumns)
            {
                //dg.columns.unshift(obj);
                dgColumns.push(obj);
            }
            dg.columns = dgColumns;
        }

        /**
         * Collection de template filtré et peuplant le datagrid
         * <code>i_dgListCollecteType</code>
         * @return
         */
        public function get collecteVOListeFiltered():ArrayCollection
        {
            return _collecteVOListeFiltered;
        }

        public function set collecteVOListeFiltered(value:ArrayCollection):void
        {
            _collecteVOListeFiltered = value;
        }
    }
}