package entities
{
	import mx.collections.ArrayCollection;

	public class HistoricCampagne
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		public var SELECTED					:Boolean = false;

		public var SYSTEMPLATFORM			:String = '';
		public var UUID 					:String = '';
		public var CLE 						:String = '';
		public var IMEI 					:String = '';
		public var IPADRESSE 				:String = '';
		public var PASSWORD 				:String = '';
		public var USERNAME  				:String = '';
		public var SOUSTETE 				:String = '';
		public var ETAT 					:String = '';

		
		public var DATE_ENROLL				:Date = null;
		public var DATE_STATUT 				:Date = null;
		
		public var IDMDMLIGNE 				:int = 0;
		public var STATUT 					:int = 0;
		public var IDCAMPAGNE				:int = 0;	
		public var IDTERMINAL				:int = 0;
		public var IDMDMLOG					:int = 0;
		public var IDUSER					:int = 0;
		public var IDPOOL 					:int = 0;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function HistoricCampagne()
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					COPIE OBJET COURANT
		//--------------------------------------------------------------------------------------------//
		
		public function copyHistoricCampagne():HistoricCampagne
		{
			var copyHistoricCampagne:HistoricCampagne 	= new HistoricCampagne();
				copyHistoricCampagne.SELECTED			= this.SELECTED;
				copyHistoricCampagne.SYSTEMPLATFORM		= this.SYSTEMPLATFORM;
				copyHistoricCampagne.UUID				= this.UUID;
				copyHistoricCampagne.CLE				= this.CLE;
				copyHistoricCampagne.IMEI				= this.IMEI;
				copyHistoricCampagne.IPADRESSE			= this.IPADRESSE;	
				copyHistoricCampagne.PASSWORD			= this.PASSWORD;
				copyHistoricCampagne.USERNAME			= this.USERNAME;
				copyHistoricCampagne.SOUSTETE			= this.SOUSTETE;
				copyHistoricCampagne.DATE_ENROLL		= this.DATE_ENROLL;
				copyHistoricCampagne.DATE_STATUT		= this.DATE_STATUT;
				copyHistoricCampagne.IDMDMLIGNE			= this.IDMDMLIGNE;
				copyHistoricCampagne.STATUT				= this.STATUT;
				copyHistoricCampagne.IDCAMPAGNE			= this.IDCAMPAGNE;
				copyHistoricCampagne.IDTERMINAL			= this.IDTERMINAL;
				copyHistoricCampagne.IDMDMLOG			= this.IDMDMLOG;
				copyHistoricCampagne.IDUSER				= this.IDUSER;
				copyHistoricCampagne.IDPOOL				= this.IDPOOL;
				copyHistoricCampagne.ETAT				= this.ETAT;
			
			return copyHistoricCampagne;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					MAPPING ET FORMATTEUR
		//--------------------------------------------------------------------------------------------//
		
		public static function mappingHistoricCampagne(value:Object):HistoricCampagne
		{
			var mpHistoricCampagne:HistoricCampagne 	= new HistoricCampagne();
				mpHistoricCampagne.SYSTEMPLATFORM		= value.SYSTEM_PLATFORM;
				mpHistoricCampagne.UUID					= value.UUID;
				mpHistoricCampagne.CLE					= value.CLE;
				mpHistoricCampagne.IMEI					= value.IMEI;
				mpHistoricCampagne.IPADRESSE			= value.IP_ADRESSE;	
				mpHistoricCampagne.PASSWORD				= value.USERPWD;
				mpHistoricCampagne.USERNAME				= value.USERNAME;
				mpHistoricCampagne.SOUSTETE				= value.SOUS_TETE;
				mpHistoricCampagne.DATE_ENROLL			= value.DATE_ENROLL;
				mpHistoricCampagne.DATE_STATUT			= value.DATE_MODIF_STATUT;
				mpHistoricCampagne.IDMDMLIGNE			= value.IDMDM_LIGNE;
				mpHistoricCampagne.STATUT				= int(value.IDMDM_STATUT);
				mpHistoricCampagne.IDCAMPAGNE			= value.IDMDM_CAMPAGNE;
				mpHistoricCampagne.IDTERMINAL			= value.IDTERMINAL;
				mpHistoricCampagne.IDMDMLOG				= value.IDMDM_LOG_CAMPAGNE;
				mpHistoricCampagne.IDUSER				= value.APP_LOGINID_CREATE;
				mpHistoricCampagne.IDPOOL				= value.IDPOOL;
				mpHistoricCampagne.ETAT					= value.LIBELLE_STATUT;
			
			return mpHistoricCampagne;
		}
		
		public static function formatHistoricCampagne(values:ArrayCollection):ArrayCollection
		{
			var historique	:ArrayCollection = new ArrayCollection();
			var len			:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				historique.addItem(mappingHistoricCampagne(values[i]));
			}
			
			return historique;
		}
	}
}