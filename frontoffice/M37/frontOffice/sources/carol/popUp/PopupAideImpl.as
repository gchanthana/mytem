package carol.popUp
{
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;

	public class PopupAideImpl extends TitleWindow
	{
		public var btnElearning:Button
		public var btnConseil:Button
		public var btnModele:Button
		
		private var _urlConsotel:String 	= "https://cache.consotel.fr/Carol/"+CvAccessManager.getUserObject().GLOBALIZATION
		private var _urlElearning:String 	= _urlConsotel+"/E-learning-CAROL.htm"
		private var _urlConseil:String 		= _urlConsotel+"/conseils_et_bonnes_pratiques-CAROL.pdf"
		private var _urlModele:String 		= _urlConsotel+"/modeles_analyses-CAROL.pdf"
		private var _mail:String			= "carol@consotel.fr"
		private var _mailSubject:String		= "Demande de formation C.A.R.O.L."
		private var _mailBody:String		= 'Nom : %0Aprénom : %0ALogin consoview : %0ASociété : %0ADescription de la demande : %0A'
		
		public function PopupAideImpl()
		{
			super();
		}
		public function init():void
		{
			btnElearning.setStyle( "backgroundColor", 0x000000 );
			btnConseil.setStyle( "backgroundColor", 0x000000 );
			btnModele.setStyle( "backgroundColor", 0x000000 );
		}
		
		public function btnElearningClickHandler():void
		{
			var url			:String 		= _urlElearning;
	        var variables	:URLVariables 	= new URLVariables();
	        var request		:URLRequest 	= new URLRequest(url);
	        	           
	        request.data 	= variables;
	        request.method	= URLRequestMethod.POST;
	        navigateToURL(request, "_blank");  
		}
		public function btnConseilClickHandler():void
		{
			var url			:String 		= _urlConseil;
	        var variables	:URLVariables 	= new URLVariables();
	        var request		:URLRequest 	= new URLRequest(url);
	        	           
	        request.data 	= variables;
	        request.method	= URLRequestMethod.POST;
	        navigateToURL(request, "_blank");  
		}
		public function btnModeleClickHandler():void
		{
			var url			:String 		= _urlModele;
	        var variables	:URLVariables 	= new URLVariables();
	        var request		:URLRequest 	= new URLRequest(url);
	        	           
	        request.data 	= variables;
	        request.method	= URLRequestMethod.POST;
	        navigateToURL(request, "_blank");  
		}
		public function sendMail():void 
		{
			var str:String = "mailto:"+_mail
			str += '?subject='+_mailSubject+'&body='+_mailBody
	        var url			:URLRequest = new URLRequest(str);
	        navigateToURL(url,"_self");
	 	}
	}
}