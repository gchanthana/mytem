﻿<cfoutput>
Hello,<br/><br/><br/>
Below, you will find a summary of the changes made to <b>#groupe#</b>'s MDM information performed by <b>#userInfos["USER"]#</b>.
The application of these modifications could depend on an initial approval from your technical support team.
<br/><br/>
<b>Carrier : </b>#serverManager.getProvider()#<br/>
<b>MDM Server : </b>#serverManager.getMdm()#<br/>
<b>Protocol : </b>#(serverManager.useSSL() ? "HTTPS":"HTTP")#<br/>
<b>MDM Administrator Email : </b>#serverManager.getAdminEmail()#<br/>
<b>Username – WebService : </b>#serverManager.getUsername()#<br/>
<b>Password – WebService : </b>*************<br/>
<b>Username – Enrollment : </b>#serverManager.getEnrollUsername()#<br/>
<b>Password – Enrollment : </b>#serverManager.getEnrollPassword()#<br/><br/><br/>
Best regards,<br/>
Tech support.
</cfoutput>