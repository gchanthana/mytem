package univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements
{
	import mx.containers.TitleWindow;
	import mx.controls.List;
	import mx.events.ListEvent;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	

	/**
	 * Classe gérant la fenêtre 'Technique de rapprochement'
	 * */
	public class RapWindow extends TitleWindow
	{
		/**
		 * Reference vers la List des thecnique disponible
		 * */
		public var listeAlgo : List;
		
		/**
		 * Constructeur
		 * */
		public function RapWindow()
		{
			super();
			
		}
		
		/**
		 * Gere le click sur la List
		 * Dispatche un evenement signifiant que l'on a choisit une technique
		 * Ferme la fenêtre
		 * */
		protected function changeHandler(le:ListEvent):void{
			if (listeAlgo.selectedIndex != -1){
				var evtObj : AlgoSelectedEvent = new AlgoSelectedEvent(AlgoSelectedEvent.ALGO_SELECTED);
				evtObj.selectedAlgo = le.currentTarget.selectedItem.type;
				dispatchEvent(evtObj);
				closeWindow(null);
			}							
		}		
		
		/**
		 * Ferme la fenêtre
		 * */
		protected function closeWindow(ce : CloseEvent):void{
			PopUpManager.removePopUp(this);
		}
	}
}