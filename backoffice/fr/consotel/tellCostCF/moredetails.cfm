	<cfif IsDefined ('SESSION')>
			<cfif IsDefined ('SESSION.IS_CONNECTED')>
				<cfif SESSION.IS_CONNECTED EQ 0> 		
					<cflocation url="index_pc.cfm">
				</cfif>
			<cfelse>	 
				<cflocation url="index_pc.cfm">
			</cfif>	
	<cfelse>	 
			<cflocation url="index_pc.cfm">
	</cfif>
	
	<?xml version="1.0" encoding="iso-8859-1" ?>
	<!DOCTYPE html PUBLIC
	"-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
	"http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
		<title>
			TELLCOST
		</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<meta name="robots" content="all"/>
		<meta name="classification" content="Services"/>
		<meta name="copyright" content="ConsoTel SAS"/>
		<meta name="author" content="Guillaume RAMOS"/>
		<meta name="language" content="fr"/>
		<meta name="description" content="TELLCOST"/>
		<meta name="keywords" content="TELLCOST"/>

		<STYLE TYPE="TEXT/CSS">
			#MONCADRE {
			HEIGHT:100%;
			WIDTH:90%;
  		 	PADDING:4PX; 
			MARGIN-LEFT:10PX;
			MARGIN-RIGHT:10PX;
			MARGIN-TOP:10PX;
			MARGIN-BOTTOM:10PX; 
			}
			.CADREDUMENUGAUCHE {
			FLOAT:LEFT;
			TEXT-ALIGN:LEFT;
			WIDTH:90%;
			HEIGHT:100%;
			}
			.CADREDUMENUDROITE {
			FLOAT:RIGHT;
			TEXT-ALIGN:LEFT;
			WIDTH:20%;
			HEIGHT:100%;
			}
			#CADREHAUT {
			WIDTH:1024PX;
			HEIGHT:100PX;
			}
			#CADREHAUT2 {
			TEXT-ALIGN:CENTER;
			WIDTH:90%;
			HEIGHT:100%;
			PADDING:4PX; 
			MARGIN-LEFT:10PX;
			MARGIN-RIGHT:10PX;
			MARGIN-TOP:10PX;
			MARGIN-BOTTOM:10PX; 
			}
			#CADRECENTRALE { 
			MARGIN-LEFT:180PX;
			MIN-HEIGHT:150PX;
			MARGIN-RIGHT:180PX;
			BORDER-LEFT:1PX DASHED #FFFFFF; 
			BORDER-RIGHT:1PX DASHED #FFFFFF;
			BORDER-BOTTOM:1PX DASHED #FFFFFF;
			BORDER-TOP:1PX DASHED #FFFFFF;
			BACKGROUND-COLOR:#99CC99;
			}
			.MYBOUTON { 
			HEIGHT: 25PX; 
			WIDTH: 200PX;} 
			.SUBMIT {
			WIDTH: 400PX;
		 	}
		 	.SPACER {
			WIDTH: 100%;
		 	} 
		 	.NOBORDER {
			TEXT-DECORATION:NONE;
		 	} 
		 	<!--- body {
			background:url(tellCostImg/fondtellcost.jpg) center repeat #fff;
			HEIGHT:100%;
			}   --->
			
			td{ 
			border: solid black; 
			}
			
		</STYLE>

		<script language="Javascript">
			if (document.images) 
			{
			button1 = new Image
			button2 = new Image
		
			button1.src = "tellCostImg/deconnexion_n.gif"
			button2.src = "tellCostImg/deconnexion_n.gif"
			}
			
			if (document.images) 
			{
			button3 = new Image
			button4 = new Image
		
			button3.src = "tellCostImg/plus2detail_n.gif"
			button4.src = "tellCostImg/plus2detail_n.gif"
			}
			
			if (document.images) 
			{
			button5 = new Image
			button6 = new Image
		
			button5.src = "tellCostImg/mesoptions_n.gif"
			button6.src = "tellCostImg/mesoptions_n.gif"
			}
			
			if (document.images) 
			{
			button7 = new Image
			button8 = new Image
		
			button7.src = "tellCostImg/crmf_n.gif"
			button8.src = "tellCostImg/crmf_n.gif"
			}
			
			if (document.images) 
			{
			button9 = new Image
			button10 = new Image
		
			button9.src = "tellCostImg/Acceuil_n.gif"
			button10.src = "tellCostImg/Acceuil_n.gif"
			}
		</script> 
	</head>
	
	<cfset obj = createobject('component','TellCostService')>	
	
<!--- Variables query --->
	
	<cfset TOTNA = "0">
	<cfset TOTM = "0">
	<cfset TOTU = "0">
	<cfset TOTK = "0">	
	<cfset TOTMONT = "0">
	<cfset TOTV = "0">
	<cfset TOTPREC = "0">

<!--- Fin variables query--->

	<BODY>
		<FONT face="Arial" size="3">
			<div align="center">
				<DIV id="CADREHAUT">
					<img src="tellCostImg/head.gif" alt="header"/> 
					<img src="tellCostImg/Rapport_tellcost.gif" alt="header"/> 
				</DIV> 
		
				<DIV ID="CADREHAUT2">
					<table border="0" style="border:0px solid #000000;margin:auto;border-collapse: collapse;">
						<tr> 
							<td rowspan="2" style="border:0px">
							<a href="<cfoutput>details.cfm?#SESSION.URLTOKEN#</cfoutput>" style="text-decoration:none">
								<img src="tellCostImg/Acceuil_n.gif" alt="DETAILS" title="DETAILS" border="0" hspace="10%">
							</a>
							</td>
							<td rowspan="2" style="border:0px">
							<a style="text-decoration:none">
								<img src="tellCostImg/visible/moredetails_blue.png" alt="PLUSDEDETAILS" title="PLUS DE DETAILS" border="0" hspace="10%">
							</a>
							</td>
							<td rowspan="2" style="border:0px">
							<a href="<cfoutput>mesoptions.cfm?#SESSION.URLTOKEN#</cfoutput>" style="text-decoration:none">
								<img src="tellCostImg/visible/mesoptions_n.png" alt="MESOPTIONS" border="0" title="MES OPTIONS" hspace="10%">	
							</a>
							</td>
							<td rowspan="2" style="border:0px">
							<a href="<cfoutput>reduiremafacture.cfm?#SESSION.URLTOKEN#</cfoutput>" style="text-decoration:none">
								<img src="tellCostImg/crmf_n.gif" alt="REDUIRE" title="REDUIRE" border="0" hspace="10%"/>
							</a>
							</td>
							<td rowspan="2" style="border:0px">
							<a href="<cfoutput>index_pc.cfm</cfoutput>" style="text-decoration:none">
								<img src="tellCostImg/deconnexion_n.gif" alt="DECONNEXION" title="DECONNEXION" border="0" hspace="10%"/>
							</a>
							</td>
						</tr>
					</table>	
				</DIV> 	
						
				<DIV ID="MONCADRE">
					<img src="tellCostImg/tellCostTestMini.png" border="0"  weight="10"  align="left"/>
					<br/><br/><br/>
					<div align="left">
						Votre ligne :  <cfoutput>#SESSION.PHONENUMBER#</cfoutput> <!--- A METTRE --->
					</div>
					<br/>
					<div align="left">
						Période de facturation des consommations du : <cfoutput>#SESSION.DATEDEBUT#</cfoutput> Au <cfoutput>#SESSION.DATEFIN#</cfoutput> <br /><br />
						<cfset dataConverted = DateFormat(#SESSION.DATEFACTURE#,"dd/mm/yyyy")>
						Date de la facture opérateur : <cfoutput>#dataConverted#</cfoutput>
					</div>
					<br/><br/>		
					<table width="100%" border="1" style="border:2px solid #000000;margin:auto;border-collapse: collapse;">
						<tr align="center" > 
							<td rowspan="2"></td>
							<td rowspan="2"> <div align="center"><B>Nombre d'appels</B></div> </td>   
							<td colspan="3"> <div align="center"><B>Volumes</B></div> </td> 
							<td rowspan="2"> <div align="center"><B>Montant (€) HT</B></div> </td>  
							<td rowspan="2"> <div align="center"><B>Variation mois précédent</B></div> </td>  
						</tr> 
						<tr align="center">
							<td> <div align="center"><B>Minutes</B></div> </td>  
							<td> <div align="center"><B>Unités</B></div> </td>  
							<td width="90"> <div align="center"><B>Ko</B></div> </td>  
						</tr> 
						
						<cfset q = obj.zQueryConso(SESSION.IDSOUS_TETE,SESSION.IDCAMPAGNE,2,SESSION.IDRACINE)> 
						
						<cfoutput>
							<cfloop query="q">
								<tr align="center"> 
									<td height="35" style="padding: 5px;"> 
										<div align="left">
											<B>#THEME_LIBELLE#</B>
										</div> 
									</td>
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(NB_APPEL_EN_COURS), "_________")# >
											<cfif val(NB_APPEL_EN_COURS) EQ 0>
												-
											<cfelse>
												#temp#
											</cfif>
										</div> 
									</td>   
									<td height="35" style="padding: 5px;"> 
										<div align="right">  
											<cfset temp = #LSNumberFormat(val(DURRE_APPEL_EN_COURS_MIN), "_________.__")#>
											<cfif val(DURRE_APPEL_EN_COURS_MIN) EQ 0>
												-
											<cfelse>
												#temp#
											</cfif>
										</div> 
									</td>
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											 <cfset temp = #LSNumberFormat(val(DURRE_APPEL_EN_COURS_UN), "_________")#>
											
											<cfif val(DURRE_APPEL_EN_COURS_UN) EQ 0>
												-
											<cfelse>
												<cfset TOTU = TOTU + val("#DURRE_APPEL_EN_COURS_UN#")>
												#temp#
											</cfif>
										</div> 
									</td>
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(DURRE_APPEL_EN_COURS_KO), "_________")#>
											<cfif val(DURRE_APPEL_EN_COURS_KO) EQ 0>
												-
											<cfelse>
												#temp#
											</cfif>
										</div> 
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(MONTANT_EN_COURS), "_________.__")#>
											<cfif val(MONTANT_EN_COURS) EQ 0>
												-
											<cfelse>
												#temp# €
											</cfif>
										</div> 
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(VARIATION), "_________")#>
											<cfif val(VARIATION) EQ 0>
												-
											<cfelse>
												<cfif val(VARIATION) LT -100>
													-100 %
												<cfelse>
													#temp# %
												</cfif>
											</cfif>
										</div>
									</td> 
								</tr> 
								
			 					<cfset TOTNA = TOTNA + val("#NB_APPEL_EN_COURS#")>
								<cfset TOTM = TOTM + val("#DURRE_APPEL_EN_COURS_MIN#")>
								<cfset TOTK = TOTK + val("#DURRE_APPEL_EN_COURS_KO#")>
								<cfset TOTMONT = TOTMONT + val("#MONTANT_EN_COURS#")>
								<cfset TOTV = TOTV + val("#VARIATION#")>
								<cfset TOTPREC = TOTPREC + val("#MONTANT_PREC#")>
							</cfloop>
						</cfoutput>
					
						<tr align="center"> 
							<td height="10"></td>
							<td height="10"></td>
							<td height="10"></td>
							<td height="10"></td>
							<td height="10"></td>
							<td height="10"></td>
							<td height="10"></td>
						</tr>
						<cfoutput>
									<td style="padding: 5px;">  
										<b>
											Total
										</b>
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right">  
											<cfset temp = #LSNumberFormat(val(TOTNA), "_________")#>
											<cfif val(TOTNA) EQ 0>
												<B>-</B>
											<cfelse>
												<B>#temp#</B>
											</cfif>
										</div>
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(TOTM), "_________.__")#>
											<cfif val(TOTM) EQ 0>
												<B>-</B>
											<cfelse>
												<B>#temp#</B>
											</cfif>
										</div>
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(TOTU), "_________")#>
											<cfif val(TOTU) EQ 0>
												<B>-</B>
											<cfelse>
												<B>#temp#</B>
											</cfif>
										</div>
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(TOTK), "_________")#>
											<cfif val(TOTK) EQ 0>
												<B>-</B>
											<cfelse>
												<B>#temp#</B>
											</cfif>
										</div>
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right"> 
											<cfset temp = #LSNumberFormat(val(TOTMONT), "_________.__")#>
											<cfif val(TOTMONT) EQ 0>
												<B>-</B>
											<cfelse>
												<B>#temp# €</B>
											</cfif>
										</div>
									</td>  
									<td height="35" style="padding: 5px;"> 
										<div align="right">
											
											<cfset TOTALTEMP3 = 0>
											<cfif val(TOTPREC) EQ 0>
												<cfset VARIATION = 100>
											<cfelse>
												<cfset TOTALTEMP1 = TOTMONT - TOTPREC>
												<cfset TOTALTEMP2 = TOTALTEMP1 / TOTPREC>
												<cfset TOTALTEMP3 = TOTALTEMP2 * 100>
												<cfset VARIATION = #LSNumberFormat(val(TOTALTEMP3), "_________")#>
											</cfif>
											<cfif val(TOTALTEMP3) EQ 0>
												<B> - </B>
											<cfelse>
												<B>#VARIATION# %</B>
											</cfif>
										</div>
									</td>  
						</cfoutput>
					</table>
				</DIV>
			
				<div align="center"> 
					<p><a href="mailto:bal-telephonie-mobile@creditfoncier.fr ?subject=Question tellcost" accesskey="3">bal-telephonie-mobile@creditfoncier.fr</a></p>
				</div>
						
				<DIV ID="MONCADRE">
					<table width="100%" border="0" style="border:0px solid #000000;margin:auto;border-collapse: collapse;"> 
						<tr> 
							<td rowspan="2" align="right" style="border:0px"> 
								<FONT face="Arial" size="2">
									<div align="left"> 
										Les montants des thèmes de consommations sont en valeur brute.<br/>
										La ligne remise & régul correspond aux remises globales sur consommations négociées dans le cadre de l’accord cadre de votre entreprise avec l’opérateur.<br/> 
										L’information de l’opérateur sur ces remises est globale et ne permet pas de les affecter à chacun des thèmes de consommations.<br/>
										La variation à 100% signifie qu'aucun montant n'est présent le mois précédent.<br/>
										La variation à -100% signifie qu'il y avait un montant le mois précédent et aucun le mois en cours.<br/>
									</div>
								</FONT>
							</td>   
							<td rowspan="2" width="250" style="border:0px">
								<img src="tellCostImg/tellCostTestPowered.png" alt="header" height="35" align="right"/> 
							</td>  
						</tr> 
					</table> 
				</DIV>
		</FONT>
	</BODY>
</html>