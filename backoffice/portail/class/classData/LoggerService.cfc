<cfcomponent  output="false"  
			displayname="LoggerService" 
			hint="Classe permettant la gestion des logs des actes de gestion ">
	
	<cffunction name="logLastAction" access="remote" returntype="numeric" 
				hint="Log un acte de gestion" ><!--- joan.cirencien@saaswedo.com --->
		<cfargument name="USERACCESSID" required="true" type="Numeric" />
		<cfargument name="ID_WORDS" required="true" type="Numeric" />
		<cfargument name="ACTION_PRINCIPALE" required="true" type="numeric" />
		<cfargument name="ITEM1" required="true" type="String" />
		<cfargument name="ITEM2" required="true" type="String" />
		<cfargument name="ID_TERMINAL" required="true" type="Numeric" />
		<cfargument name="ID_SIM" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfargument name="IDSOUS_TETE" required="true" type="Numeric" />
		<cfargument name="UID_ACTION" required="true" type="String" />
		
		<cfset IDGROUPE_RACINE = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111.log_action_flotte_v2">
			<cfprocparam value="#USERACCESSID#" variable="p_app_loginid" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_WORDS#" variable="p_IDWORDS" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ACTION_PRINCIPALE#" variable="p_action_principale" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ITEM1#" variable="p_ITEM1" cfsqltype="CF_SQL_VARCHAR2" type="in">
			<cfprocparam value="#ITEM2#" variable="p_ITEM2" cfsqltype="CF_SQL_VARCHAR2" type="in">
			<cfprocparam value="#ID_TERMINAL#" variable="p_IDTERMINAL" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_SIM#" variable="p_IDSIM" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#ID_EMPLOYE#" variable="p_IDEMPLOYE" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDSOUS_TETE#" variable="p_IDSOUS_TETE" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDGROUPE_RACINE#" variable="p_IDRACINE" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#IDGROUPE_CLIENT#" variable="p_IDGROUPE_CLIENT" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#UID_ACTION#" variable="p_UID_ACTION" cfsqltype="CF_SQL_VARCHAR2" type="in">
			<cfprocparam value="" variable="p_commentaire" cfsqltype="CF_SQL_VARCHAR2" type="in">
			<cfprocparam value="0" variable="p_idcause" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="1" variable="p_idstatut" cfsqltype="CF_SQL_INTEGER" type="in">
			
			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour"/>
		    

		    <cfprocresult name="p_retour"/>		
		</cfstoredproc>
		
		<cfreturn p_retour>			
	</cffunction>
	
</cfcomponent>