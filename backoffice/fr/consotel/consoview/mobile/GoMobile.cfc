<cfcomponent output="false" displayname="fr.consotel.consoview.mobile.GoMobile">
		
	<cffunction name="getSitePool" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Liste sites d'un pool:<br> '-' si pas de site<br> - 'access denied' si pas loggué<br> - datase BOOL_HAS_EQUIPEMENT;BOOL_HAS_SAV;COORD_LAT;COORD_LNG;IDSITE_PHYSIQUE;NB_EQUIPEMENT_POOL;NOM_SITE" 
				hint="liste des sites d'un pool" >
		<cfargument name="idPool" 	type="numeric" 	required="true" hint="id du pool">		
		<cfargument name="idRacine" 	type="numeric" 	required="true" hint="id de la racine">
		
		<cfset _idRacine = idRacine>
		<cfset _idPool = idPool>
		<cfset _qListeSite = '-'>
					
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>

		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_MOBILE.LISTESITEPOOL">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#_idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#_idPool#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#_appLoginID#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			<cfprocresult name="qListeSite">
		</cfstoredproc>
		
		<cfif p_retour gt 0>
			<cfset _qListeSite = qListeSite>
		</cfif>
				
		<cfreturn _qListeSite>
	</cffunction>
	
	<cffunction name="getDeviceInSite" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Liste des appareils d'un site :
				<br>- '-' si pas d'appareil'
				<br> - 'access denied' si pas loggué
				<br> - IDMATRICE;IDTERMINAL;NOM;NUMERO_SIM;PATH_LOW;PATH_THUMB;PRENOM;SOUS_TETE;SYSTEM_OEM;SYSTEM_OS_VERSION;SYSTEM_PLATFORM;T_BOOL_MDM;T_FABRIQUANT;T_IMEI;T_MODELE;T_PRODUCT_ID;ZDM_IMEI;ZDM_SERIAL_NUMBER" 
				hint="Liste des appareils d'un site" >
		<cfargument name="idPool" 	type="numeric" 	required="true" hint="id du pool">		
		<cfargument name="idSite" 	type="numeric" 	required="true" hint="id du site physique">
		
		
		<cfset _idPool = idPool>
		<cfset _idSite = idSite>
		<cfset _qListeDevice = '-'>
					
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>

		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_MOBILE.LISTETERMSITE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#_racineID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#_idPool#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_physique" value="#_idSite#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#_appLoginID#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			<cfprocresult name="qListeDevice">
		</cfstoredproc>
		
		<cfif p_retour gt 0>
			<cfset _qListeDevice = qListeDevice>
		</cfif>
				
		<cfreturn _qListeDevice>
	</cffunction>
	
	<cffunction name="getFicheRA" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Fiche RA
				<br>- '-' si pas de fiche
				<br> - 'access denied' si pas loggué
				<br> - APPLEID;APPLEPWD;CODE_ADMIN;DATE_NAISSANCE_ITUNES;DEBUT_GARANTIE;IDCONCESSION;IDEQUIPEMENT;MODELE;QUESTION_SECRETE;REPONSE;SERIAL;TYPE_CONTRAT;UNLOCK_CODE;WIFI_MAC" 
				hint="Fiche RA" >
		<cfargument name="idEquipement" 	type="numeric" 	required="true" hint="id du device">
		
		<cfset _idEquipement = idEquipement>		
		<cfset _qFicheRA = '-'>
					
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>

		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_MOBILE.GETFICHERA">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#_racineID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idequipement" value="#_idEquipement#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			<cfprocresult name="qFicheRA">
		</cfstoredproc>
		
		<cfif p_retour gt 0>
			<cfset _qFicheRA = qFicheRA>
		</cfif>
				
		<cfreturn _qFicheRA>
	</cffunction>
	
	
	<cffunction name="searchDeviceInSite" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Liste des appareils d'un site :
				<br>- '-' si pas d'appareil'
				<br> - 'access denied' si pas loggué
				<br> - IDMATRICE;IDTERMINAL;NOM;NUMERO_SIM;PATH_LOW;PATH_THUMB;PRENOM;SOUS_TETE;SYSTEM_OEM;SYSTEM_OS_VERSION;SYSTEM_PLATFORM;T_BOOL_MDM;T_FABRIQUANT;T_IMEI;T_MODELE;T_PRODUCT_ID;ZDM_IMEI;ZDM_SERIAL_NUMBER" 
				hint="Liste des appareils d'un site" >
		<cfargument name="idPool" 	type="numeric" 	required="true" hint="id du pool">		
		<cfargument name="idSite" 	type="numeric" 	required="true" hint="id du site physique">		
		
		<cfargument name="imei_serial" 	type="string" required="true" hint="clé de recherche sur le numero IMEI ou le numéro de série">
		<cfargument name="nom_pre_mat" 	type="string" required="true" hint="clé de recherche sur le nom, prénom ou matricule">
		<cfargument name="modele" 	type="string" required="true" hint="clé de recherche sur le le modèle">
		<cfargument name="num_ligne" 	type="string" required="true" hint="clé de recherche le numéro de ligne">	
				

		<cfset var _idPool = idPool>
		<cfset var _idSite = idSite>
		<cfset var _imei_serial = imei_serial>
		<cfset var _nom_pre_mat = nom_pre_mat>
		<cfset var _modele = modele>
		<cfset var _num_ligne = num_ligne>
		
		<cfset var _qListeDevice = '-'>
					
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>

		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_MOBILE.SEARCHTERMINAL">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#_racineID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" value="#_idPool#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" value="#_appLoginID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsite_physique" value="#_idSite#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_imei_serial" value="#_imei_serial#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_nom_pre_mat" value="#_nom_pre_mat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_modele" value="#_modele#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_num_ligne" value="#_num_ligne#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">		
			<cfprocresult name="qListeDevice">
		</cfstoredproc>
		
		<cfif p_retour gt 0>
			<cfset _qListeDevice = qListeDevice>
		</cfif>
				
		<cfreturn _qListeDevice>
	</cffunction>
	
	
	<cffunction name="getFicheTerminal" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Fiche Terminal">

		<cfargument name="idTerminal" 	required="true" type="numeric"/>
		<cfargument name="segment" 	required="true" type="numeric"/>
		<cfargument name="idpool" 	required="true" type="numeric"/>
		<cfargument name="idmtx" 	required="true" type="numeric"/>
		
		<cfset var _idTerminal = idTerminal>
		<cfset var _idSegment = segment>
		<cfset var _idPool = idpool>
		<cfset var _idmtx = idmtx/>
			
		<cfset var _qData = '-'>
					
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>
	
		<cfthread action="run" name="ficheEquipement"
					idTerminal="#_idTerminal#" idRacine="#_racineID#" idLangue="3" segment="#_idSegment#" idpool="#_idPool#" id="#_idmtx#" >
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getfiche_equipement_V2">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idTerminal#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idLangue#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#segment#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#id#">
				
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result"/>
				<cfprocresult name="terminal" 	resultset="1">
				<cfprocresult name="lignes" 	resultset="2">
				<cfprocresult name="ticket" 	resultset="3">
				<cfprocresult name="contrat" 	resultset="4">
			</cfstoredproc>
			
			<cfset ficheEquipement.result = p_result >
			<cfset ficheEquipement.terminal = terminal >
			<cfset ficheEquipement.lignes = lignes >
			<cfset ficheEquipement.ticket = ticket >
			<cfset ficheEquipement.contrat = contrat >
			
		</cfthread>
		
		
		
		<cfthread action="run" name="deviceInfo" idLangue="3" idRacine="#_racineID#" idpool="#_idPool#" id="#_idmtx#" >
			
			<cfset deviceInfo.result = -1 >
			<cfset deviceInfo.properties ='' >
			<cfset deviceInfo.software = '' >
			
			<cfset mtx_infos = createObject ("component" ,"fr.consotel.consoview.mobile.GetMatriceInfo").getData(idRacine,idpool,id)>
		 	<cfif mtx_infos.recordcount gt 0>
			
				
							
				<cfstoredproc datasource="#session.OFFREDSN#" procedure="pkg_m111.getzdmproperties">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR"  value="##">
					<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR"  value="#idLangue#">
					<cfprocresult name="p_general" 		resultset="1">
					<cfprocresult name="p_properties" 	resultset="2">
					<cfprocresult name="p_software" 	resultset="3">
				</cfstoredproc>
							
			</cfif>
						
			<cfset deviceInfo.result = p_general >
			<cfset deviceInfo.properties = p_properties >
			<cfset deviceInfo.software = p_software >
								
		</cfthread>
		
		
		
		<cfthread action="run" name="listeSousEqpOfTerm" idpool="#_idPool#" idTerminal="#_idTerminal#" >
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.listeSousEqpOfTerm">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idpool#">
				<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idTerminal#">
				
				<cfprocresult name="p_retour" >
			</cfstoredproc>
			
			<cfset listeSousEqpOfTerm.retour = p_retour>
			
		</cfthread>
		
		
		
		<cfthread action="join" name="ficheEquipement,listeSousEqpOfTerm,deviceInfo" timeout="20000"/>		
		
		
		<cfset _qData = ArrayNew(1)>
		<cfset _qData[1] = ficheEquipement.result >
		<cfset _qData[2] = ficheEquipement.terminal >
		<cfset _qData[3] = ficheEquipement.lignes >
		<cfset _qData[4] = ficheEquipement.ticket >
		<cfset _qData[5] = ficheEquipement.contrat >
		<cfset _qData[6] = listeSousEqpOfTerm.retour >
		<cfset _qData[7] = deviceInfo.result >
		<cfset _qData[8] = deviceInfo.properties >
		<cfset _qData[9] = deviceInfo.software >
		
		<cfreturn _qData>
			
	</cffunction>
	
	
	
	<!--- MDM TO DO ..... Ajout des threads pour ne pas attendre la réponse si ok alors 1 demande prise encompte --->		
	<cffunction name="enrollDevice" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="initier le process d'enrolement pour un terminal
				<br>- 'no - data' si pas d'appareil'
				<br> - 'access denied' si pas loggué
				<br> - 1  si ok sinon 'enrollment failed'" 
				hint="initier le process d'enrolement pour un terminal" >
					
		<cfargument name="IDPOOL" 		type="numeric" 	required="true" hint="id du pool de gestion du demandeur">
		<cfargument name="IDTERMINAL" 	type="numeric" 	required="true" hint="id du terminal dans le référentiel saaswedo">
		<cfargument name="SOUS_TETE" 	type="string" 	required="true" hint="le numéro de ligne du terminal a enroler">
		<cfargument name="ID" 			type="numeric" 	required="true" hint="id de la ligne de la matrice saaswedo">
		
		<!--- Ajouter un modèle --->
		<cfset var _id_pool = IDPOOL>
		<cfset var _id_terminal = IDTERMINAL>
		<cfset var _sous_tete = SOUS_TETE>
		<cfset var _id_mtx = ID>
				
		<cfset var _response = 'no - data'>
					
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>
		
		<cfset mtx_infos = createObject("component" ,"fr.consotel.consoview.mobile.GetMatriceInfo").getData(_racineID,_id_pool,_id_mtx)>
		 
		<cfif mtx_infos.recordcount gt 0> 
		
			<cfif compareNoCase(mtx_infos['SOUS_TETE'][1],_sous_tete)  eq 0>
				
				<cfthread action="run" name="t_enrollDevice" infos="#mtx_infos#">
					
				
								
					<cfset _response = createObject("component","fr.consotel.consoview.M111.MDMService").simpleEnrollement(	infos['T_IMEI'][1],
																														infos['T_IMEI'][1],
																														infos['T_ZDM_IMEI'][1],
																														infos['IDTERMINAL'][1],
																														infos['SOUS_TETE'][1],
																														infos['IDSOUS_TETE'][1],
																														infos['IDPOOL'][1])>
																														
				</cfthread>
				<cfset _response = 1>																										
			<cfelse>
				<cfreturn 'enrollment failed'>
			</cfif>
		</cfif>
		<cfreturn _response>
	</cffunction>
	
	<cffunction name="lockDevice" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Vérrouille le terminal donné
				<br> - 'access denied' si pas loggué
				<br> - 1  si ok sinon 'no - data'"  
				hint="Vérrouille le terminal donné" >					
		<cfargument name="IDPOOL" 		type="numeric" 	required="true" hint="id du pool de gestion du demandeur">
		<cfargument name="IDTERMINAL" 	type="numeric" 	required="true" hint="id du terminal dans le référentiel saaswedo">
		<cfargument name="ID" 			type="numeric" 	required="true" hint="id de la ligne de la matrice saaswedo">		
		<cfargument name="CODE_VERROU" 	type="string" 	required="false" hint="code verrou pour les endroid">
				
		<cfset var _id_pool = IDPOOL>
		<cfset var _id_terminal = IDTERMINAL>
		<cfset var _id_mtx = ID>
		<cfset var _code_pin = CODE_VERROU>
				
		<cfset var _response = 'no - data'>
		
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>
		
		<cfset mtx_infos = createObject("component" ,"fr.consotel.consoview.mobile.GetMatriceInfo").getData(_racineID,_id_pool,_id_mtx)>
		<cfif mtx_infos.recordcount gt 0>
			
			<cfthread action="run" name="t_lockDevice" infos="#mtx_infos#" code_pin="#_code_pin#">			
				<cfset _response = createObject("component","fr.consotel.consoview.M111.MDMService").lock(	infos['T_IMEI'][1],
																								  	infos['T_NO_SERIE'][1],
																									infos['T_ZDM_IMEI'][1],
																									infos['IDTERMINAL'][1],
																									code_pin)>		
			</cfthread>																							
			<cfset _response = 1>					
		</cfif>
		<cfreturn _response>
	 </cffunction>
	
	<cffunction name="unLockDevice" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Dévérrouille le terminal donné
				<br> - 'access denied' si pas loggué
				<br> - 1  si ok sinon 'no - data'" 
				hint="Dévérrouille le terminal donné" >					
		<cfargument name="IDPOOL" 		type="numeric" 	required="true" hint="id du pool de gestion du demandeur">
		<cfargument name="IDTERMINAL" 	type="numeric" 	required="true" hint="id du terminal dans le référentiel saaswedo">
		<cfargument name="ID" 			type="numeric" 	required="true" hint="id de la ligne de la matrice saaswedo">		
		<cfargument name="CODE_VERROU" 	type="string" 	required="false" hint="code verrou pour les endroid">
				
		<cfset var _id_pool = IDPOOL>
		<cfset var _id_terminal = IDTERMINAL>
		<cfset var _id_mtx = ID>
		<cfset var _code_pin = CODE_VERROU>
				
		<cfset var _response = 'no - data'>
		
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>
		
		<cfset mtx_infos = createObject("component" ,"fr.consotel.consoview.mobile.GetMatriceInfo").getData(_racineID,_id_pool,_id_mtx)>
		<cfif mtx_infos.recordcount gt 0>
			<cfthread action="run" name="t_unLockDevice" 
					  infos="#mtx_infos#" 
					  code_pin="#_code_pin#">				
				<cfset _response = createObject("component","fr.consotel.consoview.M111.MDMService").unlock(infos['T_IMEI'][1],
																								  	infos['T_NO_SERIE'][1],
																									infos['T_ZDM_IMEI'][1],
																									infos['IDTERMINAL'][1],
																									code_pin)>
			</cfthread>																							
			<cfset _response = 1>		
		</cfif>
		<cfreturn _response>
	 </cffunction>
	 
	 
	 
	 <cffunction name="wipeDevice" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Effacement total ou partiel des données du terminal donné
				<br> - 'access denied' si pas loggué
				<br> - 1  si ok sinon 'no - data'" 
				hint="Effacement total ou partiel des données du terminal donné" >					
		<cfargument name="IDPOOL" 		type="numeric" 	required="true" hint="id du pool de gestion du demandeur">
		<cfargument name="IDTERMINAL" 	type="numeric" 	required="true" hint="id du terminal dans le référentiel saaswedo">
		<cfargument name="ID" 			type="numeric" 	required="true" hint="id de la ligne de la matrice saaswedo">		
		<cfargument name="BOOLERASESD" 	type="boolean"	required="true" hint="true = effacer | false = ne pas effacer la carte SD">
		<cfargument name="BOOLERASEALL" 	type="boolean" required="true" hint="true = effacemment complet | false = effacement des données entreprise">
					
		<cfset var _id_pool = IDPOOL>
		<cfset var _id_terminal = IDTERMINAL>
		<cfset var _id_mtx = ID>
		<cfset var _bool_erase_sd = BOOLERASESD>
		<cfset var _bool_erase_all = BOOLERASEALL>
						
		<cfset var _response = 'no - data'>
		
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>
		
		<cfset mtx_infos = createObject("component" ,"fr.consotel.consoview.mobile.GetMatriceInfo").getData(_racineID,_id_pool,_id_mtx)>
		<cfif mtx_infos.recordcount gt 0>
			
			<cfthread action="run" name="t_wipeDevice" infos="#mtx_infos#" 
					  bool_erase_sd="#_bool_erase_sd#" 
					  bool_erase_all="#_bool_erase_all#">				
				<cfset _response = createObject("component","fr.consotel.consoview.M111.MDMService").wipe(	infos['T_IMEI'][1],
																								  	infos['T_NO_SERIE'][1],
																									infos['T_ZDM_IMEI'][1],
																									infos['IDTERMINAL'][1],
																									bool_erase_sd,
																									bool_erase_all)>
		 	</cfthread>																								
		 	<cfset _response = 1>				
		</cfif>
		<cfreturn _response>
	 </cffunction>
	 
	 
	 
	 
	 <cffunction name="revokeDevice" access="remote" returntype="any" returnformat="JSON" output="false" 				
				description="Sortie du terminal du MDM (revoke)
				<br> - 'access denied' si pas loggué
				<br> - 1  si ok sinon 'no - data'" 
				hint="Sortie du terminal du MDM (revoke)" >
		<cfargument name="IDPOOL" 		type="numeric" 	required="true" hint="id du pool de gestion du demandeur">
		<cfargument name="IDTERMINAL" 	type="numeric" 	required="true" hint="id du terminal dans le référentiel saaswedo">
		<cfargument name="ID" 			type="numeric" 	required="true" hint="id de la ligne de la matrice saaswedo">
		<cfargument name="BOOLAUTHORIZE" 	type="boolean"	required="true" hint="Enrollment automatique après revocation pour android">
					
		<cfset var _id_pool = IDPOOL>
		<cfset var _id_terminal = IDTERMINAL>
		<cfset var _id_mtx = ID>
		<cfset var _bool_authorize = BOOLAUTHORIZE>
						
		<cfset var _response = 'no - data'>
		
		<cfif checkSession() eq true>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
		<cfelse>
			<cfreturn 'access denied'>
		</cfif>
		
		
		<cfset mtx_infos = createObject("component" ,"fr.consotel.consoview.mobile.GetMatriceInfo").getData(_racineID,_id_pool,_id_mtx)>
		 
		<cfif mtx_infos.recordcount gt 0>
			
			<cfthread action="run" name="t_revokeDevice" infos="#mtx_infos#" 
					  bool_authorize="#_bool_authorize#">	
					  	  							
				<cfset _response = createObject("component","fr.consotel.consoview.M111.MDMService").revoke(infos['T_IMEI'][1],
																								  	infos['T_NO_SERIE'][1],
																									infos['T_ZDM_IMEI'][1],
																									infos['IDTERMINAL'][1],bool_authorize)>
			</cfthread>																										
			<cfset _response = 1>
		</cfif>
		<cfreturn _response>
	 </cffunction>
	 
	 <cffunction access="private" name="checkSession" returntype="boolean" description="Regarde si le demandeur est connecté">
	 	<cfif structkeyexists(SESSION,'USER') and structkeyexists(SESSION,'PERIMETRE')>			
			<cfset _appLoginID = SESSION.USER.CLIENTACCESSID>	
			<cfset _racineID = SESSION.PERIMETRE.ID_GROUPE>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>  
			<cfreturn false>
	 </cffunction>
	 
</cfcomponent>