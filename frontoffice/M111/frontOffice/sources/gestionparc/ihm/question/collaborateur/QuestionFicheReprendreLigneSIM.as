package gestionparc.ihm.question.collaborateur
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheReprendreLigneSIM
	{
		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheReprendreLigneSIM(obj_dispo:IListeDispo, date:Date)
		{
			this.liste=obj_dispo;
			this.check=check;
			this.date=date;

			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;

			if (obj_dispo.getSelection() && obj_dispo.getSelection().hasOwnProperty("IDTERMINAL") && obj_dispo.getSelection().IDTERMINAL != null && obj_dispo.getSelection().IDTERMINAL > -1)
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEREPRENDRELIGNESIM"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etre_vous_s_r_de_vouloir_dissocier_le_co'), ResourceManager.getInstance().getString('M111', 'Dissocier_collaborateur_de_la_ligne'), alertClickHandler);
			}
		}

		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				reprendreLigneSimFromCollab3();
			}
		}
		
		private function qnCloseHandler(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
			{
				reprendreLigneSimFromCollab1()
			}
			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
			{
				reprendreLigneSimFromCollab2()
			}
		}

		private function reprendreLigneSimFromCollab1():void
		{
			serv.reprendreLigneSimFromCollab1(setObj());
		}

		private function reprendreLigneSimFromCollab2():void
		{
			serv.reprendreLigneSimFromCollab2(setObj());
		}

		private function reprendreLigneSimFromCollab3():void
		{
			serv.reprendreLigneSimFromCollab3(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=liste.getSelection().NUM_SIM;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=0;
			obj.ID_SIM=liste.getSelection().IDSIM;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
