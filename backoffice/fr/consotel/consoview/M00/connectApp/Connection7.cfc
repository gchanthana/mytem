<cfcomponent extends="AbstractConnectToApp">
<!--- CONNECTION A MYTEM PAR LOGIN / MDP --->	
	
<!---- CHAQUE FONCTION GERE CES ERREURS ET RETOURNE UN OBJECT ERROR AVEC UN CODEERREUR
	   IL EST POSSIBLE DE PRECISER LE TYPE, LE MESSAGE ET LE DETAIL
	   <cfreturn ThrowCfCatchError(CODEERREUR,TYPE,MESSAGE,DETAIL)>
	   codeerreur : int, type : string, message : string, detail  : string --->
	<cffunction name="connectTo" 
				displayname="ConnectToApp" 
				description="Permet de se logger à une application" 
				access="public" 
				output="false" 
				returntype="struct">
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		<cflog text="connection4.connectToApp(login = #objConnection.login#, pwd = #objConnection.pwd#)">
		
		<cfif structKeyExists(objConnection,"codeLangue")>
			<cfset THIS.codeLangue = objConnection.codeLangue>
		<cfelse>
			<cfset THIS.codeLangue = createObject("component","fr.consotel.consoview.access.CodeLangue")>		
			<cfset THIS.codeLangue.CODE = "default">	
			<cfset THIS.codeLangue.ID_LANGUE = "default">	
			<cfset THIS.codeLangue.DEVISE = "-">
			<cfset THIS.codeLangue.LANGUE = "n.c.">		
		</cfif>
		
		<cfset clearOk = clearSession()>
		<cfif clearOk eq 1>
		<!--- VALIDATE LOGIN --->
			<cfset result = validateLogin(objConnection)>
			<!--- si validateLogin retourne un object et que celui ci contient un codeerreur
				  alors on retourne cet object pour afficher un message d'erreur
				 --->
			<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>				
			</cfif>	
			<!--- MSG D'ERREUR DE VALIDATE LOGIN --->			
		<cfelse>
			<cfreturn ThrowCfCatchError(result)>	
			<cflog text="connectTo failed">					
		</cfif>			
		<cfreturn SESSION>
	</cffunction>		
	
	<!--- CREATION DE L'OBJET USER DE LA SESSION --->		
	<cffunction name="validateLogin" access="private" output="false" returntype="Any" >
		<cfargument name="objConnection" required="true" type="struct" >
		<cflog text="AbstractConnectToApp.validateLogin()">
		<cftry>
			<cfif StructKeyExists(SESSION, "IMPERSONNIFICATION")>
				<cfif SESSION.IMPERSONNIFICATION eq FALSE>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">
				</cfif>
				<cfelse>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">
			 </cfif>
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.CONNECT_CLIENT#">
				<cfprocparam type="in" value="#objConnection.login#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#objConnection.pwd#" 	cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qValidateLogin">
			</cfstoredproc>
			<cfif p_retour eq 1>
				<cfif qValidateLogin.recordcount EQ 1>
					<cfset boolTokenHasToBeSaved = 0>
					<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
					<cfif boolCheckIP EQ 1>
						<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
						<cfif checkIpResult LT 1>
							<cfreturn ThrowCfCatchError(-2)>
						<cfelse>
							<cfif setupSessionUserPerimetre(qValidateLogin)>
								<cfif objConnection.seSouvenir eq 1>
									<cfset boolTokenHasToBeSaved = 1>
								</cfif>
							<cfelse>
								<cfreturn ThrowCfCatchError(-5)>
							</cfif>
							<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
						</cfif>
					<cfelse>
						<cfif setupSessionUserPerimetre(qValidateLogin)>
							<cfif objConnection.seSouvenir eq 1>
								<cfset boolTokenHasToBeSaved = 1>
							</cfif>
						<cfelse>
							<cfreturn ThrowCfCatchError(-5)>
						</cfif>
					</cfif>
			<!--- 11/07/2012 : AJOUT SSO SE SOUVENIR  --->	
					<cfif boolTokenHasToBeSaved eq 1>
						<cfset result = defineToken(objConnection)>
						 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
							<cfreturn result>
						<cfelse>
							<cfif result eq 0>
								<cfreturn ThrowCfCatchError(-27)>
							<cfelse>
								<cfreturn 1>
							</cfif>
						</cfif>
					<cfelse>	
						<cfreturn 1>
					</cfif>	
				<cfelse>
					<cfreturn ThrowCfCatchError(-3)>
				</cfif>
			<cfelse>
				<cfreturn ThrowCfCatchError(p_retour)>
				<cflog text="echec de la validation du login/mdp">
			</cfif>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-4,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>	
	</cffunction>	
	
	<cffunction name="setupSessionUserPerimetre"  access="private" output="false" returntype="boolean" >
		<cfargument name="qValidateLogin" required="true" type="query" >
		
		<cflog text="AbstractConnectToApp.setupSessionUserPerimetre()">
		
		<cftry>
			<cfset SESSION.AUTH_STATE = 1>
			<cfset SESSION.OFFREDSN = "ROCOFFRE">
			<cfset SESSION.CDRDSN = "ROCCDR">
			<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW">
			<cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
			<cfset SESSION.USER = structNew()>
			<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
			<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
			<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
			<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
			<cfset SESSION.USER.APP_LOGINID = qValidateLogin['CLIENTACCESSID'][1]>
			<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
			<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
			<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
			<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
			<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
			<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>
			<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
			<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
			
			<cfset SESSION.PERIMETRE = structNew()>
			<cfset SESSION.PERIMETRE.ID_GROUPE		= qValidateLogin['ID'][1]>
			<cfset SESSION.PERIMETRE.GROUPE			= qValidateLogin['LIBELLE'][1]>
			<cfset SESSION.PERIMETRE.IDRACINE_MASTER= qValidateLogin['IDRACINE_MASTER'][1]>

			<cfset getDeviseInfos()>

			<cfreturn TRUE>
		<cfcatch type="any">
			<cfreturn FALSE>
		</cfcatch>
		</cftry>
	</cffunction>	
		
	<cffunction name="getDeviseInfos" access="public" output="false" returntype="boolean" >
		
		<cftry>
			<cfquery name="p_retour" datasource="ROCOFFRE">
				SELECT * 
				FROM langue_pays l,devise d 
				WHERE d.iddevise = l.iddevise 
				AND l.langid = #SESSION.USER.IDGLOBALIZATION#
				AND l.code = '#SESSION.USER.GLOBALIZATION#'
			</cfquery>

			<cfset SESSION.USER.DEVISEINFOS = {}>
			<cfset SESSION.USER.DEVISEINFOS.SYMBOL = p_retour.SYMBOL>
			<cfset SESSION.USER.DEVIDEINFOS.NAME = p_retour.NAME>
			<cfset SESSION.USER.DEVISEINFOS.IDDEVISE = p_retour.IDDEVISE>
			<cfset SESSION.USER.DEVISEINFOS.SHORT_DESC = p_retour.SHORT_DESC>
			<cfreturn TRUE>
		<cfcatch type="any">
			<cfreturn FALSE>
		</cfcatch>
		</cftry>

	</cffunction>
	
</cfcomponent>