package univers.inventaire.contrat.system
{
	import mx.collections.ICollectionView;
	import mx.rpc.AbstractOperation;

	public class ContratEquipementMobile implements IContrat
	{
		
		protected var _items:ICollectionView;
		
		public function ContratEquipementMobile()
		{
		}

		public function createContrat(items:ICollectionView;):void
		{
			_items = items;
		}
		
		public function getDetailContrat():void
		{
		}
		
		public function udpateInfosContrat():void
		{
		}
		
		public function deleteContrat():void
		{
		}
		
		
	}
}