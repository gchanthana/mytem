﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAI001" hint="retourne les informations nécessaires à l'application."  output="false">
	
	<!--- V0 : non terminé : récupérer tous les informations prédéfinis dès le chargement de la console de gestion --->
		
		<!--- V1 : non implémenté : dans V0 il y aura beaucoup d'informations à récupérer pour chaque module de la console de gestion
					Il serait bien de migrer V0 vers des Threads, par exemple un Thread qui récupère les infos pour chaque module--->
					
	<!--- 
		retourne les informations nécessaires à l'application. Il s'agit de plusieurs listes prédéfinies
		déjà chez Saaswedo.
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="retourne les informations nécessaires à l'application." output="false">
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfset result = structNew()>
			<cfset result.LISTEAPPLICATION = getListeApplication()>
			<cfset result.LISTENIVEAU = getListeNiveau()>
			<cfset result.LISTEALLNIVEAU = getListeAllNiveau()>
			<cfset result.LISTE_RACINE = getListeRacine()>
			<cfset result.LISTE_UNIVERS = getListeRacine()>

			<cfif isStruct(result.LISTEAPPLICATION) AND structKeyExists(result.LISTEAPPLICATION,"CODEERREUR")>
				
				<cfreturn CreateReponse("AI002",0,result.LISTEAPPLICATION)/>
				
			<cfelseif isStruct(result.LISTENIVEAU) AND structKeyExists(result.LISTENIVEAU,"CODEERREUR")>
				
				<cfreturn CreateReponse("AI002",0,result.LISTENIVEAU)>
				
			<cfelseif isStruct(result.LISTEALLNIVEAU) AND structKeyExists(result.LISTEALLNIVEAU,"CODEERREUR")>
				
				<cfreturn CreateReponse("AI002",0,result.LISTEALLNIVEAU)>
				
			<cfelseif isStruct(result.LISTE_RACINE) AND structKeyExists(result.LISTE_RACINE,"CODEERREUR")>
				
				<cfreturn CreateReponse("AI002",0,result.LISTE_RACINE)>
				
			</cfif>

			<cfreturn CreateReponse("AI002",1,result)/>
			
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI002", 0, ThrowCfCatchError("PTHD0010",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>

<!--- Liste des application --->
	<cffunction name="getListeApplication" 
				access="private" 
				returntype="any" >
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.listeApplication">
				<cfprocresult name="qListe">
			</cfstoredproc>

			<cfreturn qListe/>
			
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI002", 0, ThrowCfCatchError("PTHD0011",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>

<!---Liste des niveaux --->
	<cffunction name="getListeNiveau" 
				access="private" 
				returntype="any" >
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.getNiveaux">
				<cfprocresult name="qListe">
			</cfstoredproc>

			<cfreturn qListe/>
			
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI002", 0, ThrowCfCatchError("PTHD0012",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>
	
<!---liste des niveaux --->
	<cffunction name="getListeAllNiveau" 
				access="private" 
				returntype="any" >
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.getAllNiveaux">
				<cfprocresult name="qListe">
			</cfstoredproc>

			<cfreturn qListe/>
			
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI002", 0, ThrowCfCatchError("PTHD0013",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>
	
<!--- Liste des racines --->
	<cffunction name="getListeRacine" 
				access="private" 
				returntype="any" >
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.getListClientsByUser">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.APP_LOGINID#">
				<cfprocresult name="qListe">
			</cfstoredproc>

			<cfreturn qListe/>
			
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI002", 0, ThrowCfCatchError("PTHD0014",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>
	
<!---liste des univers --->
	<cffunction name="getListeUnivers" 
				access="private" 
				returntype="Any" >
		<cftry>
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M28.getListeUnivers">
				<cfprocresult name="qListe">
			</cfstoredproc>

			<cfreturn qListe/>
			
		<cfcatch type="any" >
			<cfreturn CreateReponse("AI002", 0, ThrowCfCatchError("PTHD0015",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>