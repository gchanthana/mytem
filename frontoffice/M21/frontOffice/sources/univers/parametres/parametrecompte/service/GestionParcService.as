package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import univers.parametres.parametrecompte.event.GestionParcEvent;
	import univers.parametres.parametrecompte.vo.ActionGPVo;
	import univers.parametres.parametrecompte.vo.ProfilGestionParc;
	
	public class GestionParcService extends EventDispatcher
	{
		[Bindable] public var listeProfilGestionParc:ArrayCollection = new ArrayCollection();
		[Bindable] public var listeAction:ArrayCollection = new ArrayCollection();
		
		public function GestionParcService()
		{
		}
	// LISTE PROFIL GESTION DE PARC	
		public function getListeProfilGestionParc():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,	"fr.consotel.consoview.M21.m21service","getListProfilGestionParc",
																		listeProfilGestionParc_handler);
			RemoteObjectUtil.callService(op);
		}
		private function listeProfilGestionParc_handler(e:ResultEvent):void
		{
			listeProfilGestionParc = new ArrayCollection()
			if(e.result != null && e.result.length > 0 && !e.result[0].hasOwnProperty("ERROR"))
			{
				var Pgp:ProfilGestionParc;
				
				Pgp = new ProfilGestionParc();
				Pgp.codeInterne = '';
				Pgp.commentaire = '';
				Pgp.idProfil = -1;
				Pgp.libelle = ResourceManager.getInstance().getString('M21', 'Aucun_droit');
				Pgp.nbUser = 0;
				listeProfilGestionParc.addItem(Pgp);
				
				
				for each(var obj:Object in e.result)
				{
					Pgp = new ProfilGestionParc();
					Pgp.codeInterne = obj.CODE_INTERNE;
					Pgp.commentaire = "";
					Pgp.idProfil = obj.IDPROFIL;
					Pgp.libelle = obj.LIBELLE;
					Pgp.nbUser = obj.NBUSER;
					listeProfilGestionParc.addItem(Pgp);
				}
			}
			listeProfilGestionParc.refresh()
		}
	// RECUPERATION DES ACTIONS D'UN PROFIL GESTION DE PARC
		private var boolNewProfil:Boolean;
		public function getListeActionProfilGestionParc(idprofil:int,isnewprofil:Boolean=false):void
		{
			boolNewProfil = isnewprofil
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,	"fr.consotel.consoview.M21.m21service","RecupererActionProfilGestionParc",
																		RecupererActionProfilGestionParc_handler);
			RemoteObjectUtil.callService(op,idprofil);
		}
		private function RecupererActionProfilGestionParc_handler(e:ResultEvent):void
		{
			if(e.result != null && e.result.length > 0)
			{
				var act:ActionGPVo = new ActionGPVo()
				for each(var obj:Object in e.result)
				{
					act = new ActionGPVo()
					act.idAction = obj.IDACTION
					act.libelle = obj.LIBELLE
					if(boolNewProfil)
						act.selected = false
					else
					{
						if(obj.SELECTED == 1)
					 		act.selected = true
					 	else
					 		act.selected = false
					}
					act.type = obj.CODE_ACTION
					listeAction.addItem(act);
				}
				listeAction.refresh()
			}
		}
	// CREER UN PROFIL
		public function createProfilGestionParc(libelle:String, code:String, listeID:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M21.m21service","createProfilGestionParc",
																		createProfilGestionParc_handler);
			RemoteObjectUtil.callService(op,libelle,code,listeID);
		}
		private function createProfilGestionParc_handler(e:ResultEvent):void
		{
			dispatchEvent(new GestionParcEvent(GestionParcEvent.CREATE_PROFIL_EVENT,true));
		}
	// METTRE A JOUR UN PROFIL
		public function MAJProfilGestionParc(idprofil:int, listeID:String, libelle:String, code:String ):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M21.m21service","MAJProfilGestionParc",
																		MAJProfilGestionParc_handler);
			RemoteObjectUtil.callService(op,idprofil,listeID,libelle,code);
		}
		private function MAJProfilGestionParc_handler(e:ResultEvent):void
		{
			dispatchEvent(new GestionParcEvent(GestionParcEvent.MAJ_PROFIL_EVENT,true));
		}
	// SUPPRIMER PROFIL
		public function SupprimerProfilGestionParc(idprofil:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M21.m21service","SupprimerProfilGestionParc",
																		SupprimerProfilGestionParc_handler);
			RemoteObjectUtil.callService(op,idprofil);
		}
		private function SupprimerProfilGestionParc_handler(e:ResultEvent):void
		{
			dispatchEvent(new GestionParcEvent(GestionParcEvent.SUPPRIMER_PROFIL_EVENT,true));
		}
	// AFFECTER PROFIL A UN POOL ET UN UTILISATEUR
		public function AffecterProfilGestionParc(idpool:int,idprofil:int,apploginid:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M21.m21service","AffecterProfilGestionParc",
																		AffecterProfilGestionParc_handler);
			RemoteObjectUtil.callService(op,idpool,idprofil,apploginid);
		}
		private function AffecterProfilGestionParc_handler(e:ResultEvent):void
		{
			dispatchEvent(new GestionParcEvent(GestionParcEvent.AFFECTER_PROFIL_EVENT));
		}
	}
}