package intranet.GestionDroits.renderer
{
	import flash.events.MouseEvent;
	
	import mx.containers.Box;
	import mx.controls.Image;
	import mx.controls.listClasses.BaseListData;
	import mx.controls.listClasses.IDropInListItemRenderer;
	
	public class workFlowImageRendererDERP extends Box
		implements IDropInListItemRenderer
	{
		private var _listData:BaseListData;
		[Bindable("datachange")]
		public function get listData():BaseListData{
			return _listData;}
		public function set listData(value:BaseListData):void{
			_listData= value;}
		
		override protected function createChildren():void{
			super.createChildren();
			this.addChild(image);
		}
		private var image:Image;
		
		[Embed(source="../asset/IMAGE8.gif")]
		public var ValidSymbol:Class;
		
		[Embed(source="../asset/IMAGE9.gif")]
		public var InvalidSymbol:Class;

		private var bool:Boolean=true;

		public function workFlowImageRendererDERP()
		{
			super();
			setStyle("horizontalAlign","center");
			setStyle("verticalAlign","middle");
			image= new Image();
			this.addEventListener(MouseEvent.CLICK,thisClickHandler);
		}
		
		private function thisClickHandler(e:MouseEvent):void{
			if(data.export_erp_dec == 0){
				bool=true;
				data.export_erp_dec=1;
			}else{
				bool=false;
				data.export_erp_dec=0;
			}
			invalidateDisplayList();
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			if(data != null){
				if(bool){
					image.source= ValidSymbol;
				}else{
					image.source= InvalidSymbol;	
				}
			}
		}
		
		override public function set data(value:Object):void{
			if(value != null && value != ""){
				super.data=value;
				
				if(data.export_erp_dec != null && data.export_erp_dec != ""){
					var currentValue:String=listData.label;
					
					switch(currentValue){
						case "1":
							bool=true;
							break;
						case "0":
							bool=false;
							break;
						default:
							bool = false;
							break;
					}
				}else{
					bool=false;
					data.export_erp_dec=0;
				}
			}
			invalidateDisplayList();
		}
	}
}