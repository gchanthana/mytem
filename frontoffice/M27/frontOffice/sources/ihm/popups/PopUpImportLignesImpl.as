package ihm.popups
{
	import composants.util.ConsoviewAlert;
	
	import entities.DataInfos;
	import entities.ImportData;
	
	import events.CampagneEvent;
	import events.HeaderEvent;
	import events.PopUpEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import services.CampagneService;
	
	public class PopUpImportLignesImpl extends TitleWindow
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//

		public var dgListeLignesImported				:DataGrid;
		
		public var txtFiltre							:TextInput;
		
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//
			
		private var _popUpImportUpload					:PopUpImportUploadIhm;
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _campagneSrv						:CampagneService = new CampagneService();
		
			//--------------------------------------------------------------------------------------------//
			//					OBJETS TYPES
			//--------------------------------------------------------------------------------------------//
		
		private var _datainfos							:DataInfos = null;
		
		private var _fileRef							:FileReference;
		
			//--------------------------------------------------------------------------------------------//
			//					DIVERS
			//--------------------------------------------------------------------------------------------//
		
		[Bindable]public var listeLignesImported			:ArrayCollection = new ArrayCollection();
		[Bindable]public var listeLignesSelected			:ArrayCollection = new ArrayCollection();

		private var _libelleHeaders						:ArrayCollection = new ArrayCollection([
																									{header:"Ligne"},
																									{header:"Collaborateur"},
																									{header:"Matricule"}
																								]);
		
		[Bindable]public var isEnabled					:Boolean = false;
		
		private var _lignesValidate						:ArrayCollection = new ArrayCollection();

		private var _currentFile						:String = '';
		private var _currentUUID						:String = '';
		private var _urlMatriceImport					:String = moduleGestionMDMIHM.NonSecureUrlBackoffice + '/fr/consotel/consoview/M27/exportsimports/ExportType.cfm'
		private var _urlMatriceUpload					:String = moduleGestionMDMIHM.NonSecureUrlBackoffice  + '/fr/consotel/consoview/M27/processUpload.cfm';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEURS
		//--------------------------------------------------------------------------------------------//
		
		public function PopUpImportLignesImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PUBLIC FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		public function setInfos(dataInfos:DataInfos):void
		{
			_datainfos = new DataInfos();
			_datainfos = dataInfos.copyDataInfos();
			
			if(_datainfos.STATUT > 0)
				setLastInfos();
		}
		
		public function getInfos():DataInfos
		{
			_datainfos				= new DataInfos();
			_datainfos.ISIMPORT 	= true;
			_datainfos.DATA		 	= listeLignesImported;
			_datainfos.DATASELECTED = _lignesValidate;
			_datainfos.NBR_LIGNE 	= _lignesValidate.length;
			_datainfos.STATUT		= 1;
			_datainfos.UUID			= _currentUUID;
			_datainfos.PERIMETRE	= _currentFile;
			
			return _datainfos;
		}
		
		public function updateCampagnesSelected(isSelected:Boolean):void
		{
			listeLignesSelected = new ArrayCollection();
			
			if(isSelected)
			{
				for each (var currentImportData:ImportData in listeLignesImported)
				{
					if(currentImportData.SELECTED)
						listeLignesSelected.addItem(currentImportData);
				}
			}
		}
		
		public function deleteCampagnesSelected(dataCurrent:ImportData):void
		{			
			if(dataCurrent == null) return;
			
			for each (var currentImportData:ImportData in listeLignesImported)
			{
				if(dataCurrent.IDGENERATE == currentImportData.IDGENERATE)
					listeLignesImported.removeItemAt(listeLignesImported.getItemIndex(currentImportData));
			}
			
			updateCampagnesSelected(true);
			
			listeLignesSelected.refresh();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PROTECTED FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		protected function closeHandler(ce:CloseEvent):void
		{			
			PopUpManager.removePopUp(this);
		} 
		
		protected function btnImportClickHandler(me:MouseEvent):void
		{
			popUpImportUpload();
		}
		
		protected function btnMatriceImportClickHandler(me:MouseEvent):void
		{
			downloadMatriceImport();
		}
		
		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if(checkLignesSelected())
				ulpoadNewData();
			else
				ConsoviewAlert.afficherAlertInfo("Veuillez sélectionner au moins une ligne.", 'Consoview', null);
		}
		
		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function filtreChangeHandler(e:Event):void
		{
			if(dgListeLignesImported.dataProvider)
			{
				dgListeLignesImported.dataProvider.filterFunction = filterFunction;
				dgListeLignesImported.dataProvider.refresh();
			}
		}
		
		protected function dgListeLignesImportedItemClickHandler(le:ListEvent):void
		{
			dgListeLignesImported.dispatchEvent(new HeaderEvent(HeaderEvent.SELECTED_CHANGE, true));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//

			//--------------------------------------------------------------------------------------------//
			//					EVENTS
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initListeners();
		}
		
		private function checkboxHeaderClickedHandler(he:HeaderEvent):void
		{
			var value:Boolean = he.target.ckbxHeaderSelected.selected;
			
			for each (var currentImportData:ImportData in listeLignesImported)
			{
				currentImportData.SELECTED = value;
				
				listeLignesImported.itemUpdated(currentImportData);
			}
			
			updateCampagnesSelected(value);
		}
		
		private function sousTeteChangeHandler(e:Event):void
		{
			var isOk:Boolean = true;
			
			for each (var currentImportData:ImportData in listeLignesImported)
			{
				if(!currentImportData.ISVALIDATE)
					isOk = false;
			}
			
			isEnabled = isOk;
		}
		
		private function importDataToCSVHandler(ce:CampagneEvent):void
		{
			dispatchEvent(new PopUpEvent(PopUpEvent.POPUP_VALIDATE));
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function initListeners():void
		{
			dgListeLignesImported.addEventListener(HeaderEvent.CHECKBOX_HEADER_CLICKED, checkboxHeaderClickedHandler);
			dgListeLignesImported.addEventListener('SOUSTETE_CHANGED', sousTeteChangeHandler);
		}
		
		private function ulpoadNewData():void
		{
			_campagneSrv.setImportDataToCSV(_currentUUID, _lignesValidate);
			_campagneSrv.addEventListener(CampagneEvent.SETDATATOCSV, importDataToCSVHandler);
		}

		private function setLastInfos():void
		{
			listeLignesImported = new ArrayCollection();
			listeLignesImported = _datainfos.DATA as ArrayCollection;

			_currentUUID = _datainfos.UUID;
			_currentFile = _datainfos.PERIMETRE;
			
			isEnabled = checkLigne();
		}

		private function checkLignesSelected():Boolean
		{
			var isOk:Boolean = false;
			
			_lignesValidate = new ArrayCollection();
			
			for each (var currentImportData:ImportData in listeLignesImported)
			{
				if(currentImportData.ISVALIDATE && currentImportData.SELECTED)
					_lignesValidate.addItem(currentImportData);
			}
			
			if(_lignesValidate.length > 0)
				isOk = true;
			
			return isOk;
		}
		
		private function downloadMatriceImport():void
		{
			var request		:URLRequest 		= new URLRequest(_urlMatriceImport);         
			var variables	:URLVariables 		= new URLVariables();
				variables.LIBELLE_HEADER		= getHeaderForExport();
				variables.FILE_NAME				= "matrice_export" + '.xls';
			
			request.data 	= variables;
			request.method 	= URLRequestMethod.POST;
			
			navigateToURL(request, "_blank");
		}
		
		private function getHeaderForExport():Array
		{
			var header:Array = new Array();
			var lenLib:int = _libelleHeaders.length;
			
			for(var i:int = 0;i < lenLib;i++)
			{
				header[i] = _libelleHeaders[i].header;
			}
			
			return header;
		}
		
		private function checkLigne():Boolean
		{
			var isOk:Boolean = true;
			
			for each (var currentImportData:ImportData in listeLignesImported)
			{
				if(currentImportData.SOUSTETE.length == 10)
				{				
					var zSixIdx		:int = currentImportData.SOUSTETE.indexOf('06');
					var zSeptIdx	:int = currentImportData.SOUSTETE.indexOf('07');
					
					if(zSixIdx == 0 || zSeptIdx == 0)
					{
						
					}
					else
						isOk = false;
				}
				else
					isOk = false;
			}
			
			return isOk;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS SERVICE
			//--------------------------------------------------------------------------------------------//
		
		private function getImportData():void
		{
			listeLignesImported = new ArrayCollection();
			
			dgListeLignesImportedItemClickHandler(null);
			
			_campagneSrv.getImportData(_currentUUID, _currentFile);
			_campagneSrv.addEventListener(CampagneEvent.IMPORTDATA, importDataHandler);
		}
		
		private function importDataHandler(e:Event):void
		{
			listeLignesImported = _campagneSrv.listeImportData;
			
			isEnabled = checkLigne();
			
			updateCampagnesSelected(true);
			
			dgListeLignesImportedItemClickHandler(null);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					GESTIONS DES POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private function popUpImportUpload():void
		{
			_popUpImportUpload 			 	= new PopUpImportUploadIhm();
			_popUpImportUpload.addEventListener(PopUpEvent.POPUP_VALIDATE, popUpImportUploadValidate);
			
			PopUpManager.addPopUp(_popUpImportUpload, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpImportUpload);
		}
		
		private function popUpImportUploadValidate(pue:PopUpEvent):void
		{
			_currentUUID = _popUpImportUpload.uuid;
			_currentFile =  _popUpImportUpload.fileName;
			
			getImportData();
			
			PopUpManager.removePopUp(_popUpImportUpload);
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FILTER
			//--------------------------------------------------------------------------------------------//
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;
			
			if(String(item.SOUSTETE).toLocaleLowerCase().search(txtFiltre.text) != -1)
			{
				rfilter = true;
			}
			else
			{
				rfilter = false;
			}
			
			return rfilter;
		}
		
	}
}