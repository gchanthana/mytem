package gestionparc.event
{
	import flash.events.Event;

	public class PiecesJointesEvent extends Event
	{

		public static const FILE_RENAMED:String="FILE_RENAMED";
		public static const FILE_UUID_CREATED:String="FILE_UUID_CREATED";

//		public static const FILE_DOWNLOADED		:String = "FILE_DOWNLOADED";
//		public static const FILE_ATTACHED		:String = "FILE_ATTACHED";
//		public static const FILE_ERASED			:String = "FILE_ERASED";
//		public static const FILE_ATTACH_JOIN	:String = "FILE_ATTACH_JOIN";
//		public static const LAST_FILES_ATTACHED	:String = "LAST_FILES_ATTACHED";

		public function PiecesJointesEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new PiecesJointesEvent(type, bubbles, cancelable);
		}
	}
}
