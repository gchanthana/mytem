package gestionparc.ihm.fiches.equipement
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.FicheAffectationIHM;
	import gestionparc.ihm.fiches.listedispo.DispoExt;
	import gestionparc.services.associer.ActionAssocierServices;
	import gestionparc.utils.gestionparcConstantes;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheAssocierLigneExtToEqp extends FicheAffectationIHM
	{
		[Bindable]
		public var liste:DispoExt;
		private var serv:ActionAssocierServices=new ActionAssocierServices();

		public function FicheAssocierLigneExtToEqp()
		{
			super();
			liste=new DispoExt();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(ev:FlexEvent):void
		{
			initListeners();
			initIHM();
		}

		private function initListeners():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, fermer);
			liste.addEventListener(MouseEvent.CLICK, dgListeDispo_handler);
			liste.addEventListener(gestionparcEvent.REFRESH_LIBELLESELECTION, dgListeDispo_handler);
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);
			this.addEventListener(CloseEvent.CLOSE, fermer);
		}

		private function initIHM():void
		{
			this.title=ResourceManager.getInstance().getString("M111", "ASSOCIER_LIGNEEXTTOEQP");

			this.img_cellule.source=gestionparcConstantes.adrImgTerm;
			this.lab_cellule_name.text=SpecificationVO.getInstance().elementDataGridSelected.T_NO_SERIE;

			this.img_liste.source=gestionparcConstantes.adrImgTerm;
			this.img_liste_big.source=gestionparcConstantes.adrImgTerm;
			this.lab_titre.text=liste.getLibelleType()

			this.box_listeDispo.addChild(liste as VBox);
		}

		private function dgListeDispo_handler(evt:Event):void
		{
			labDgSelection.text=liste.getLibelleSelection();
		}

		private function valider(e:Event):void
		{
			if (liste.getSelection().IDEXTENSION  != null && liste.getSelection().IDEXTENSION  > -1)
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "Etre_vous_s_r_de_vouloir_associer_une_extention____quipement_avec_sim"), ResourceManager.getInstance().getString("M111", "Associer_extention___un__quipement"), alertClickHandler);
			}
			else if (liste.getIDSelection() > -1)
			{
				associerExtToEqp();
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
		}

		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				associerExtToEqp1();
			}
		}
		
		protected function listeClickHandler(e:MouseEvent):void
		{
			if (liste.getIDSelection() > -1)
			{
				labDgSelection.text=liste.getLibelleSelection();
			}
		}

		protected function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function associerExtToEqp():void
		{
			serv.associerLigneExtToEquipement(setObj());
		}

		private function associerExtToEqp1():void
		{
			serv.associerExtToEquipementWithSim(setObj());
		}
		
		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID
			obj.ITEM1=liste.getSelection().EX_NUMERO;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_EXT=liste.getSelection().IDEXTENSION;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString((calendar.selectedDate != null) ? calendar.selectedDate : new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=check_pret.selected;
			return obj;
		}
	}
}
