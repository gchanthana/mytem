<cfcomponent name="ConfigurationMapProxySupport" implements="fr.consotel.consoview.api.ibis.admin.configuration.IConfigurationMapProxy">
	<cfset VARIABLES["APPLICATION_PROXY"]="NULL">
	<cfset VARIABLES["VERSION_XPATH"]="NULL">
	
	<cffunction name="init" access="public" returntype="fr.consotel.consoview.api.ibis.admin.configuration.IConfigurationMapProxy" description="Constructor : Sets Configuration VERSION XML Tag Path (XPATH)">
		<cfset var levelXpath="NULL">
		<cfset var levelNodesArray="NULL">
		<cfset var httpHostArray=listToArray(CGI.HTTP_HOST,".")>
		<cfset VARIABLES["APPLICATION_PROXY"]=createObject("component","fr.consotel.consoview.api.ibis.admin.ApplicationProxy")>
		<cfif arrayLen(httpHostArray) GTE 1>
			<cfset levelXpath="/PROPERTIES/ENDPOINTS/ENDPOINT[child::HTTP_HOST='#httpHostArray[1]#']/LEVEL">
			<cfset levelNodesArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),levelXpath)>
			<cfif arrayLen(levelNodesArray) GTE 1>
				<cfset VARIABLES["VERSION_XPATH"]="/PROPERTIES/VERSIONS/VERSION[child::LEVEL='#levelNodesArray[1].xmlText#']">
			<cfelse>
				<cfset VARIABLES["VERSION_XPATH"]="/PROPERTIES/VERSIONS/VERSION[child::LEVEL='PROD']">
			</cfif>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="getLevel" access="public" returntype="string" description="Returns version level (Example : PROD,DEV,...)">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/LEVEL")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getCharset" access="public" returntype="string" description="Returns charset">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/GLOBAL/DEFAULT_CHARSET")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getMailServerHostname" access="public" returntype="string" description="Returns Mail Server Hostname (without server port)">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/GLOBAL/MAIL_SERVER")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getMailServerPort" access="public" returntype="numeric" description="Returns Mail Server port number (DEFAULT is 25)">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/GLOBAL/MAIL_SERVER_PORT")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn VAL(nodeArray[1].xmlText)>
		<cfelse>
			<cfreturn 25>
		</cfif>
	</cffunction>
	
	<cffunction name="getAdminEmailAddress" access="public" returntype="string" description="Returns Admin Email Address">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/GLOBAL/ADMIN_MAIL")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getSupportEmailAddress" access="public" returntype="string" description="Returns Support Email Address">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/GLOBAL/SUPPORT_MAIL")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getReplyToEmailAddress" access="public" returntype="string" description="Returns Reply To Email Address">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/GLOBAL/REPLYTO")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getFailToEmailAddress" returntype="string" description="Returns Fail To Email Address">
		<cfset var nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/GLOBAL/FAILTO")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getAvailableDsDesc" access="public" returntype="array" description="Returns Available datasource description as an array of XML Elements">
		<cfset nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/DATASOURCES")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlChildren>
		<cfelse>
			<cfreturn arrayNew(1)>
		</cfif>
	</cffunction>
	
	<cffunction name="getDsNameFromDesc" access="public" returntype="string" description="Returns ColdFusion Datasource Name as it was registered in ColdFusion Administration">
		<cfargument name="datasourceDesc" type="string" required="false" default="NULL"
					hint="Example : FACTURATION, OBIEE. Check available values by calling getAvailableDsDesc(). Default value is NULL">
		<cfset var nodeArray=
				xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/DATASOURCES/DATASOURCE[child::DATA='#ARGUMENTS.datasourceDesc#']/NAME")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
	
	<cffunction name="getPublisherWSDL" access="public" returntype="string" description="Returns Publisher WSDL URL">
		<cfset nodeArray=xmlSearch(VARIABLES["APPLICATION_PROXY"].getConfigurationMap(),VARIABLES["VERSION_XPATH"]&"/REPORTING/SERVER[child::DESC='BIP']/WSDL")>
		<cfif arrayLen(nodeArray) GTE 1>
			<cfreturn nodeArray[1].xmlText>
		<cfelse>
			<cfreturn "NULL">
		</cfif>
	</cffunction>
</cfcomponent>
