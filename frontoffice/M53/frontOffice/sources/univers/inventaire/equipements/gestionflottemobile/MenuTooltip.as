package univers.inventaire.equipements.gestionflottemobile
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.effects.Fade;
	import mx.events.CloseEvent;
	import mx.events.EffectEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class MenuTooltip extends MenuTooltipIHM
	{
		private var _target : DisplayObject;
		private var fadeOut:Fade;
		private var fadeIn:Fade;
		private var boolOver:Boolean = false;
		
		public function MenuTooltip(target : DisplayObject)
		{
			
			super();
			//this.scaleX = 1/2;
			//this.scaleY = 1/2;
			this._target = target;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer);
			addEventListener(MouseEvent.ROLL_OVER,roll_over_handler);
			addEventListener(MouseEvent.ROLL_OUT,roll_out_handler);
			
		}
		
		private function fermer(evt : CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function initIHM(evt : FlexEvent):void
		{	
			var t:Timer = new Timer(1500,1);
			t.addEventListener(TimerEvent.TIMER_COMPLETE, _tTimerCompleteHandler);
			t.start(); 
						
			addChild(_target);
			
			fadeIn = new Fade(this);
		 	fadeIn.duration = 500;
            fadeIn.alphaFrom = 0.0;
            fadeIn.alphaTo = 1.0;
            
           	fadeOut = new Fade(this);
			fadeOut.duration = 500;
            fadeOut.alphaFrom = 1.0;
            fadeOut.alphaTo = 0.0;	
            
            fadeIn.play(); 
       	}
       	
		private function roll_over_handler(evt : Event):void
		{
			boolOver = true;
			
			if(fadeOut.isPlaying==true)
			{
            	reverseFadeOut();
            }
		}
		
		private function roll_out_handler(evt : MouseEvent):void
		{
			fadeOut.play();
            fadeOut.addEventListener(EffectEvent.EFFECT_END,closeMenu);
   		}
		private function closeMenu(evt: EffectEvent):void
		{	
			PopUpManager.removePopUp(this);
		}
		private function reverseFadeOut():void
		{	
			fadeOut.removeEventListener(EffectEvent.EFFECT_END,closeMenu);
			fadeOut.reverse();
		}
		

		protected function _tTimerCompleteHandler(event:TimerEvent):void
		{
			if(!boolOver)
			{
				fadeOut.play();
            	fadeOut.addEventListener(EffectEvent.EFFECT_END,closeMenu);
			}
		}
	}
}