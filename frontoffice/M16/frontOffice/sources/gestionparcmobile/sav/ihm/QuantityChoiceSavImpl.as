package gestionparcmobile.sav.ihm
{
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	
	import commandemobile.ihm.QuantityChoiceImpl;
	
	import session.SessionUserObject;
	
	public class QuantityChoiceSavImpl extends QuantityChoiceImpl
	{
		public function QuantityChoiceSavImpl()
		{
			super();
		}
		
		private var _previousPageIndex:Number = 0;		
		public function get previousPageIndex():Number{
			return _previousPageIndex;
		}
		
		override protected function showHandler(fe:FlexEvent):void
		{
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			
			_previousPageIndex = SessionUserObject.singletonSession.LASTVSINDEX;
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
			
			//attributToEquipments();
			//attributToAboOptions();
			//compteTotal();
			//callLater(refreshDatagrid);
			checkIfOnly();
			//txtptNumberConfig.text = _cmd.CONFIG_NUMBER.toString();			
		}
		
		override public  function validateCurrentPage():Boolean
		{
			var isOk:Boolean = true;
			_cmd.CONFIG_NUMBER = 1;
			
			//if(convertedToInt)
				//_cmd.CONFIG_NUMBER = 1;
			//else
			//{
				//isOk = false;
				//ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_saisir_un_nombre_'), 'Consoview', null);
			//}
			
			return isOk;
		}
		
		private function checkIfOnly():void{
			var isPass:Boolean = true;			
			if(isPass)
			{
				//sdispatchEvent(new Event('ONE_ENGAGEMENT_RESSOURCES', true));
			}
		}
	}
}