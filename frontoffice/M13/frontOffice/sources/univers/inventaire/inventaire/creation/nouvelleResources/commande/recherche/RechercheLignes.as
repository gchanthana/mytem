package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	/**
	 * Classe gerant la section de recherche de ligne
	 * */
	[Bindable]
	public class RechercheLignes extends Canvas
	{
		/**
		 * Constante definissant le type d'evenement dispatché lors de la selection d'une ligne
		 * */
		public static const SELECTION_CHANGED : String = "selectionChanged";
		
		/**
		 * Constante référençant la classe distante permetant de faire des recherches de lignes
		 * */
		public static const REMOTE_CLASS : String = "fr.consotel.consoview.inventaire.cycledevie.recherche.facade";
		
		
		/**
		 * Constante référençant la classe distante permetant de faire des recherches de lignes
		 * */
		public static const REMOTE_CLASS_COMPTE : String = "fr.consotel.consoview.facturation.suiviFacturation.addFacture";
																				
		/**
		 * Constante référençant la methode distante permetant de faire des recherches de lignes d'un noeud
		 * */
		public static const GET_LINES_FROM_NODE : String = "rechercherListeLigne";
						
		/**
		 * Constante référençant la methode distante permetant de faire des recherches de lignes a partir d'un compte de facturation
		 * */
		public static const GET_LINES_FROM_COMPTE : String = "getAllLines";
		
		
		/**
		 * Constante référençant la methode distante permetant de faire des recherches de lignes a partir d'un compte de facturation
		 * */
		public static const GET_LINES_FROM_SOUS_COMPTE : String = "getAllLinesSousCompte";
		
		
		/**
		 * Référence vers le textInput de l'IHM
		 * */
		public var txtFiltreLigne : TextInputLabeled;
		
		/**
		 * Référence vers le DataGrid de l'IHM
		 * */
		public var myGridLignes : DataGrid;
		
		/**
		 * Référence vers l'operation charger de faire les appels à la méthode distante GET_LINES_FROM_NODE
		 * */
		private var op : AbstractOperation;
		
		/**
		 * Constructeur
		 * */
		public function RechercheLignes()
		{
			super();
		}
		
		//Reference local vers le panier de la commande
		private var _panier : ElementCommande;
		/**
		 * Setter pour le panier 
		 * @param panier  le panier de la commande
		 * */
		public function setPanier(panier : ElementCommande):void{
			_panier = panier;
		}
		
		//Reference local vers la commande
		private var _commande : Commande;
		/**
		 * Setter pour la commande
		 * @param commande la commande à référencer
		 * */
		public function affecterCommande(commande : Commande):void{
			 _commande = commande;			 	
		}
		
		
		//met à jour le libelle des ligne (ne fait rien ici)
		public function updateLigneLibelle( tabLigne : Array):void{
			
		}
		
		//ne fait rien
		public function clean():void{
		 	txtFiltreLigne.text = "";
		 	myGridLignes.dataProvider = new ArrayCollection();
		}
		
		//Retourne un Array conenant la liste des lignes sélectionnées
		//param out selectedItems la liste des lignes sélectionnées
		public function getSelectedItems():Array{
			return myGridLignes.selectedItems;
		}
		
		/**
		 * charge la liste des lignes suivant un type de perimetre et un noeud
		 * @param in perimetre le type de perimetre (groupe, groupeLigne ....)
		 * @param in nodeId l'identifiant du noeud
		 * */
		public function getLines(perimetre : String, nodeId : int):void{
			getLinesFromNode(perimetre, nodeId);	
		}
		
		/**
		 * Initialisation de l'IHM
		 * Affectation des écouteurs d'évenements
		 * */
		protected function init():void{
			txtFiltreLigne.addEventListener(Event.CHANGE,filtrerGrid);
			myGridLignes.addEventListener(ListEvent.CHANGE,selectionChanged);
		}
		
		/**
		 * Formate les numéros de téléphone d'une colonne de DataGrid
		 * Voir la propriété labelFunction d'un DataGrid pour les parametres
		 * */ 
		protected function formatPhoneNumber(item : Object,column : DataGridColumn):String{		
		 
			if (isNaN(item[column.dataField])){
				return item[column.dataField];
			}else{
				return ConsoviewFormatter.formatSimplePhoneNumber(item[column.dataField]);		
			}	
			
		}	
		
		/**
		 * Dispatche un évenement de type RechercheLignes.SELECTION_CHANGED signifiant que l'on vient de sélectionner des lignes
		 * */
		protected function selectionChanged(le : ListEvent):void{
			if (myGridLignes.selectedIndex != -1){
				dispatchEvent(new Event(RechercheLignes.SELECTION_CHANGED));	
			}
		}
		
		/**
		 * Gere le filtre du DataGrid des lignes
		 * */
		protected function filtrerGrid(ev : Event):void{			
			if (myGridLignes.dataProvider != null){
				(myGridLignes.dataProvider as ArrayCollection).filterFunction = filtrer;
				(myGridLignes.dataProvider as ArrayCollection).refresh();	
			}		
		}
		
		//Filtre les lignes sur la propriété libelleLigne
		//param ini ligne une ligne du DataGrid
		private function filtrer(ligne : ElementCommande):Boolean{
			if (String(ligne.libelleLigne).toLowerCase().search(txtFiltreLigne.text.toLowerCase()) != -1){
				return true;
			}else{
				return false;
			}
		}
		
		//-------- REMOTINGS ------------------------------------------------//
		
		
		//Formate les données du DataGird des lignes
		//param in values un ArrayCollection de sous-tete
		//param out dataLignes  un ArrayCollection contenant des éléments de type ElementCommande avec les infos des sous-tetes
		private function fillData(values : ArrayCollection):ArrayCollection{
			var ligne : Object;			
			var dataLines : ArrayCollection = new ArrayCollection();
			
			for (ligne in values){													
				var ec : ElementCommande = new ElementCommande();				
				
				ec.ligneID = values[ligne].IDSOUS_TETES;					
				ec.libelleLigne = values[ligne].SOUS_TETE;	
				dataLines.addItem(ec);
			}			
			
			dataLines.refresh();	
			return dataLines;
		}
		
		//charge la liste de lignes d'un noeud
		//param in perimetre le type de perimetre du noeud (groupe, groupeLigne, ...)
		//param in l'identifiant du noeud
		private function getLinesFromNode(perimetre : String,nodeId : int):void{			
			
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION
												  ,REMOTE_CLASS
												  ,GET_LINES_FROM_NODE
												  ,getLinesFromResultHandler);
												  
			RemoteObjectUtil.callService(op,perimetre,nodeId); 
		}
		
		//Handler de la fonction getLinesFromNode
		//Affiche la liste des lignes retrouvées dans le DataGrid
		private function getLinesFromResultHandler(event: ResultEvent):void
		{	
			myGridLignes.dataProvider = null;
			myGridLignes.dataProvider = fillData((event.result as ArrayCollection));
			//(myGridLignes.dataProvider as ArrayCollection).source.forEach(updatePanier);
			_panier.liste.removeAll();
			_panier.liste.refresh();
		}
		
		
		private function getLinesFromCompte(operateurId : int, compteId : int):void{			
			
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION
												  ,REMOTE_CLASS_COMPTE
												  ,GET_LINES_FROM_COMPTE
												  ,getLinesFromResultHandler);
			
			var idRacine : int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;												  
			RemoteObjectUtil.callService(op,idRacine,operateurId,compteId); 
		}
		
		
		private function getLinesFromSousCompte(sousCompteId : int):void{			
			
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION
												  ,REMOTE_CLASS_COMPTE
												  ,GET_LINES_FROM_SOUS_COMPTE
												  ,getLinesFromResultHandler);
														  
			RemoteObjectUtil.callService(op,sousCompteId); 
		}
		
		
		private var _compteFacturation : Object;
		public function set compteFacturation(value : Object):void
		{
			_compteFacturation = value;	
			
			if (value && value.hasOwnProperty('IDCOMPTE_FACTURATION') && value.hasOwnProperty('OPERATEURID'))
			{
				var _compteFacturationId : Number = _compteFacturation.IDCOMPTE_FACTURATION;
								
				getLinesFromCompte(_compteFacturation.OPERATEURID ,_compteFacturationId);
			}
		}
		public function get compteFacturation():Object
		{
			return _compteFacturation;
		}
		
		
		
		private var _sousCompteFacturation : Object;
		public function set sousCompteFacturation(value : Object):void
		{
			_sousCompteFacturation = value;	
			
			if (value && value.hasOwnProperty('IDSOUS_COMPTE'))
			{
				var _sousCompteFacturationId : Number = _sousCompteFacturation.IDSOUS_COMPTE;
								
				getLinesFromSousCompte(_sousCompteFacturationId);
			}
		}
		public function get sousCompteFacturation():Object
		{
			return _sousCompteFacturation;
		}
		
	}
}