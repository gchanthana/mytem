package service.destinataires.listedestinataires
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;


	public class ListeDestinatairesService
	{
		private var _model:ListeDestinatairesModel;
		public var handler:ListeDestinatairesHandler;	
		
		public function ListeDestinatairesService()
		{
			this._model  = new ListeDestinatairesModel();
			this.handler = new ListeDestinatairesHandler(model);
		}		
		
		public function get model():ListeDestinatairesModel
		{
			return this._model;
		}

		public function getListeDestinataires(idNode:Number):void
		{	
			// listeDestinataires : nom du fichier cfc 
			//getListeDest nom fonction dans le fichier cfc
	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M22.service.destinataires.listeDestinataires","getListeDest",handler.getListeDestinatairesResultHandler); 
			
			RemoteObjectUtil.callService(op,idNode);// appel au service coldfusion
			
		}
	}
}