package composants.tree.event
{
	import flash.events.Event;
	
	public class SearchTreeEvent extends Event
	{
		public static const NODE_SELETCED:String="nodeSelected";
		public static const NODE_FINDED:String="nodeFinded";
		
		private var _node:Object;
		
		public function SearchTreeEvent(targetNode:Object,type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{	
			super(type,bubbles,cancelable);
			
			_node = targetNode;
		}
		
		override public function clone():Event
		{
			return new SearchTreeEvent(node,type,bubbles,cancelable)
		}

		public function get node():Object
		{
			return _node;
		}
	}
}