﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAF002" hint="supprime les caractères spéciaux dans un texte" output="false">
	
	<!--- 
		supprime les caractères spéciaux dans un texte par exemple un nom de fichier
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="supprime les caractères spéciaux dans un texte" output="false">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			
			<cfset p_retour = createObject("component","fr.consotel.consoview.M28.RegexRenameFile").changeSpecialCaracters(#data.nameLinkedPJ#)>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			<cfreturn CreateReponse("AF002", 1, qResult)>
			
		<cfcatch type="Any">
			<cfreturn CreateReponse("AF002", 0, ThrowCfCatchError("AF002",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>