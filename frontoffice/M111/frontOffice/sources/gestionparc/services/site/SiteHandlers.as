package gestionparc.services.site
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.rpc.events.ResultEvent;

	public class SiteHandlers
	{
		
		private var myDatas : SiteDatas;
	
		public function SiteHandlers(instanceOfDatas:SiteDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getSiteEmplacement()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetSiteEmplacement()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getSiteEmplacementResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetSiteEmplacement(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_FICHE_SITE_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>getEmplacement()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetEmplacement()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getEmplacementResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatDataGetEmplacement(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_EMPLA_SITE_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>setSiteEmplacement()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataSetSiteEmplacement()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setSiteEmplacementResultHandler(re:ResultEvent):void
		{
			if(re.result == 1)
			{
				myDatas.treatDataSetSiteEmplacement();
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.UPDATE_FICHE_SITE_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
	}
}