package univers.inventaire.inventaire.etapes
{
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import composants.cyclevie.TabElFactOperationRapprochement;
	import composants.cyclevie.TabProduitsOperationRaprochement;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import mx.utils.ObjectUtil;
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;
	import composants.cyclevie.TabElReclamOperationRapprochement;
	import mx.events.MenuEvent;
	import mx.controls.Menu;
	import mx.managers.PopUpManager;
	import mx.charts.AreaChart;
	
	import univers.inventaire.inventaire.creation.operationresiliation.EnregistrerOperationEvent;
	import univers.usages.usage.detail;
	import mx.events.CollectionEvent;
	import composants.util.DateFunction;
	import composants.util.ConsoviewUtil;
	import flash.display.DisplayObject;
	import univers.inventaire.inventaire.INVTYPE_OPERATION;
	import univers.inventaire.inventaire.export.ReclamationVO;
	import univers.inventaire.inventaire.export.ExportReclamation;
	import univers.inventaire.inventaire.export.ExportBuilder;
	import univers.inventaire.inventaire.export.IExportable;
	
	/**
	 * Classe gerant l'ecran de gestion d'une réclamation
	 * */    
	public class EtapesOperationReclam extends EtapesOperationReclamIHM
	{ 
		//Constant definissant l identifiant de l'etat 'Réclamation en cours'
		private const RECENCOURS : int = 36;
		
		//
		private const SUR_MAITRE : int = 444;
		private const MAITRE : int = 333;
		
		//Constant definissant l identifiant de l'etat 'Réclamation en cours apres un lettrage'
		private const RECENCOURSLETTREE : int = 37;
		

		//Constant definissant l identifiant de l'action 'Valider reclamation'
		private const VALIDER_RECLAMATION : int = 36;
		
		//Constant definissant le titre du panel lorsque l'opération est en cours
		private const OPERATION_EN_COURS : String = "Opération en cours";
		
		//Constant definissant le titre du panel lorsque l'opération est cloturée		
		private const OPERATION_CLOSE : String = "Opération close";
		
		//Reference vers la méthode distante qui ramène la liste des opérations possible	
		private var opListeActionPossible : AbstractOperation;
		
		//Reference vers la méthode distante qui enregistre une action pour l'opération de réclamation
		private var opSaveAction : AbstractOperation;
		
		//Reference vers la méthode distante qui enregistre une action pour l'opération de verification dont est issue la réclamation
		private var opSaveActionOpMaitre : AbstractOperation;
		
		private var typeReclamation : int;
	 
		//Reference vers la méthode distante qui ramene l'historique des actions pour la réclamation
		private var opHistoOpe : AbstractOperation;
		
		//Reference vers la méthode distante qui cloture la reclamation
		private var opCloturerOpe : AbstractOperation;
		
		//Reference vers la méthode distante qui ramene les informations de la reclamation
		private var opDetailOpe : AbstractOperation;
		
		//Reference vers la méthode distante qui ramene la liste des lignes de facturations utilisées pour la réclamation 
		private var opListeFactReclam : AbstractOperation;
		
		//Booleen permettant de savoir si l'historique a été chargé		 
		private var historiqueActionOk : Boolean = false;
		
		//ArrayCollection contenant l'historique des actions pour la réclamation
		[Bindable]
		private var tabHistoEtap : ArrayCollection;
		
		//ArrayCollection contenant les informations de la réclamation
		private var OperationDetail : ArrayCollection;
		
		//ArrayCollection contenant les informations de l'opération de vérification dont est issue la réclamation
		private var OperationMaitreDetail : ArrayCollection;
		
		private var OperationSurMaitreDetail : ArrayCollection;
		
		//Référence vers le commentaire de l'action qui va etre faite
		private var commentaireAction : String;		
		
		//Reférence vers l'identifiant de la ressource sélectionnée
		private var selectedProduit : int;		
		
		//Reférence vers l'identifiant de l'action sélectionnée
		private var selectedAction : int;
		
		//Reférence vers la selectionné pour l'action qui va être faite
		private var produitSelectionnerPouAction : Object; 
		
		//Référence vers le menu du PopUpMenuButton 'Actions Possibles'
		private var myMenu : Menu;
		
		//ArrayCollection contenant la liste des action possibles
		[Bindable]
		private var tabActionsPossible : ArrayCollection;
		
		//Tableau tempon contenant la liste des ressources sélectionnées		
		private var TMPtabProduitsOpSelectionnee : Array;
		
		//Tableau tempon contenant la liste des lignes de facturation sélectionnées
		private var TMPtabProduitsFactSelectionnee : Array;	
		
		//Booléen placé à true si au moins une ressource a été sélectionnée 
		private var okOp : Boolean = false;
		
		//Booléen placé à true si au moins une ligne de facturation a été sélectionnée 
		private var okFact : Boolean = false;
		
		//Reference vers le tableau des lignes de facturation utilisées pour la réclamation
		private var myGridFacture : TabElReclamOperationRapprochement;
		
		//Réference vers le tableau des ressources de la réclamation
		private var myGridProduitsOP : TabProduitsOperationRaprochement;
		
		//Référence vers la boite de dialogue de confirmation
		private var confirmbox : ConfirmBox;		
		
		//Référence vers l'identifiant de l'opération de réclamation
		private var idOperation : int;	
		
		//Référence vers la	date de référence utilié pour le calcul des régules
		private var dateRef : String; 
		
		//Référence vers le type de l'operation qui à conduit à cette réclamation
		private var typeOpmaitre :int;
		
		//Référence vers le numéro de l'opération
		private var numeroOperation : String; 
		
		//Référence vers l'etat de l'operation de réclamation (1 = en cours, 0 = cloturée)
		private var operationEnCours : int;
		
		
		public function get modeEcriture():Boolean{
			return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
		}
		 
		/**
		 * Constructeur
		 * @param numero le numero de l'opération de réclamation
		 * @param idOp l'identifiant de l'opération
		 * @enCours l'état de l'operation (1 = en cours, 0 = cloturée)
		 * */						
		public function EtapesOperationReclam(numero : String,idOpe : int,enCours : int){
			//TODO: implement function
			super();
			numeroOperation = numero;
			idOperation = idOpe;
			operationEnCours = enCours;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}	
		
		/**
		 * Supprime les fenêtres surgissantes
		 * */
		public function clean():void{
			if (confirmbox != null){
				if (confirmbox.isPopUp) PopUpManager.removePopUp(confirmbox);
				confirmbox = null;
			}
		}		
				
		//pre initialisation de l'IHM
		//pre charge les infos pour l'opération et ensuite initialise les éléments de l'ihm		
		private function initIHM(fe : FlexEvent):void{
			addEventListener("detailOpcharge",init);
			chargerInfoOperation(idOperation);			
			btActionPossible.enabled = false;	
			nsDelai.enabled = modeEcriture;
			txtCommentaire.editable = modeEcriture;
			dcDateAction.enabled = modeEcriture;	
		}
		
		//Initilaise l'ihm
		//Affecte les écouteurs d'évènements
		//...
		private function init(ev : Event):void{			
			//title = "Opération n° " + numeroOperation;
			lblEtatOperation.text = (operationEnCours == 1) ? OPERATION_EN_COURS : OPERATION_CLOSE;	
			
			myGridProduitsOP = new TabProduitsOperationRaprochement();	
			
			
			
			if (typeOpmaitre == INVTYPE_OPERATION.VERIF_FACT_RESILIATION){
				myGridProduitsOP.initialiserGrid(idOperation,dateRef,INVTYPE_OPERATION.RESILIATION);			
			}else if (typeOpmaitre == INVTYPE_OPERATION.VERIF_FACT_COMMANDE){
				myGridProduitsOP.initialiserGrid(idOperation,dateRef,INVTYPE_OPERATION.COMMANDE);
			}else {
				throw new Error("Type d'opération inconnue");
			}
			
			
			myGridFacture = new TabElReclamOperationRapprochement();								
			myGridProduitsOP.addEventListener("ProduitsOPSelectionnes",triaterSelectionDeProduits);
							 
			myGridFacture.addEventListener("ProduitsFactSelectionnes",triaterSelectionDeProduits);
			
			boxTabProduit.addChild(myGridProduitsOP);
			myGridProduitsOP.titre = "Produits de l'opération";
			
			boxTabFacture.addChild(myGridFacture);			
			myGridFacture.titre = "Eléments de la facturation";
			btActionPossible.addEventListener(MouseEvent.CLICK,doAction);
			btActionPossible.enabled = (operationEnCours == 1) ?(true && modeEcriture) : false  ;
			btSortir.addEventListener(MouseEvent.CLICK,sortir);		
			btPDF.addEventListener(MouseEvent.CLICK,btPDFClickHandler);						
			chargerElementGridEltFacture(idOperation);
			dcDateAction.selectedDate = new Date();
			if (operationEnCours == 0 ) {
				callLater(disablePanel);
			}			
		}
		
		//Désactive l'opération de réclamation (l'ihm est grisée)
		private function disablePanel():void{
			btActionPossible.visible = false; 
			formActPoss.visible = false;		
			myGridFacture.enabled = false;
			myGridProduitsOP.enabled = false;
			boxAction.enabled = false;
			boxAction.visible = false;
			
		}
		
		//Dispatche un evenement de type SortirSuivitOperationReclam signifiant que l'on veut sortir du module
		private function sortir(me : MouseEvent):void{
			dispatchEvent(new Event("SortirSuivitOperationReclam"));			
		}
		
		//Charge les infos sur l'etat de l'objet passe en parametre afin de cloturer l'opération avec la bonne action
		//param in item un objet (la ressource selectionnée)
		private function traiterConsequencesEtat(item : Object):void{
			//var myEtapeFlag : EtapeFlag = new EtapeFlag(selectedAction,item.IDINV_OP_ACTION);
			var myEtapeFlag : EtapeFlag = new EtapeFlag(selectedAction,0);
			myEtapeFlag.addEventListener("EtapeFlagComplete",processConsequencesEtat);			
			addChild(myEtapeFlag).visible = false;
		}
		
		//Cloture l'operation
		private function processConsequencesEtat(ev : Event):void{
			var etape : EtapeFlag = EtapeFlag(ev.currentTarget);
			trace("processConsequencesEtat");
			if((etape.ENTRAINE_CLOTURE == 1)  && (operationEnCours == 1)){
				switch(etape.IDETAT){
					case RECENCOURS : {
						cloturerOperationRec(idOperation);
						break;
					}
					case RECENCOURSLETTREE : { 
						cloturerOperationRecLettrage(idOperation);
						break;
					}
				}
			}								
				
			EtapeFlag(ev.currentTarget).removeEventListener("EtapeFlagComplete",processConsequencesEtat);
			
		}
		
		//Gere la selection des éléments des tableaux
		//Si au moins un élément de chaque tableau est sélèctionné et que l'opération est en cours on charge les actions possibles 
		//et on active le bouton des actions possibles
		private function triaterSelectionDeProduits(spe : SelectionProduitEvent):void{
			
			 
			switch(spe.type){
				case "ProduitsOPSelectionnes" : {					
					try{
						if ((spe.tabProduits.length > 0) && (spe.tabProduits.length < 2)){
							TMPtabProduitsOpSelectionnee = spe.tabProduits;
							okOp = true;

							
							if ((spe.tabProduits[0].IDINV_ETAT == RECENCOURS)||
							   	(spe.tabProduits[0].IDINV_ETAT == RECENCOURSLETTREE)||
							   	(operationEnCours == 0)){							   	
							   		
									btActionPossible.enabled = false;
									btActionPossible.visible = false;
							}else{
							   		btActionPossible.enabled = true && modeEcriture;
							   		btActionPossible.visible = true && modeEcriture;
							   		
							}														   
							chargerActionPossible(spe.tabProduits[0].IDINV_ETAT);							 
							selectedProduit = spe.tabProduits[0].IDINVENTAIRE_PRODUIT;
							produitSelectionnerPouAction = spe.tabProduits[0];
							chargerHistoriqueLigne();							
							
						}else okOp = false;	
					}catch(e :Error){
						okOp = false;						
					}				
					break;
				}
				case "ProduitsFactSelectionnes" :{					
					try{
						if ((spe.tabProduits.length > 0) && (spe.tabProduits.length < 2)){
							TMPtabProduitsFactSelectionnee = spe.tabProduits;
							okFact = true;
						} 
						else okFact = false;
					}catch(e :Error){
						okFact = false;						
					}				
					break;
				}				
			}
			
		}
		
		
		
		//Extrait une colone d'un tableau et retourne le résultat sous forme de tableau
		//param in data le tableau source
		//		   colname le nom de la colonne à extraire
		private function formateData( data : Array , colname : String):Array{
			var s : String;
			var a : Array = new Array();
			for (var i : uint = 0 ; i < data.length ; i++ ){
				a.push(data[i][colname]);
			}
			return a;
		}
		
		
		
		/*------------------------------------------- ACTION -------------------------------------------*/

		//executer l'enregistrement de l'opération apres confirmation
		private function executerEnregistrerAction(ce : ConfirmationEvent):void{
			commentaireAction = ce.CommentaireConfirmation;				
			sauvegarderAction(selectedAction);
		}
		
		//gere le click sur le bouton action
		private function doAction(me : MouseEvent):void{
			confirmbox = new ConfirmBox();
			confirmbox.addEventListener(ConfirmationEvent.CONFIRMATION,executerEnregistrerAction);
			confirmbox.title = "Confirmer!!";
			confirmbox.message = "Etes vous sur de vouloir Enregistrer l'Action ";
			confirmbox.action = btActionPossible.label;
			ConsoviewUtil.scale(DisplayObject(confirmbox),cv.W_ratio,cv.H_ratio);
			PopUpManager.addPopUp(confirmbox,this,true);			
		}
		
		//initialise le popup bouton
		private function initMenu():void {
			myMenu = new Menu();
			myMenu.dataProvider = tabActionsPossible;
			myMenu.labelField = "LIBELLE_ACTION";			 
			try{
				myMenu.selectedIndex = 0;  	
				myMenu.addEventListener("itemClick", menuItemClickHandler);
			
				btActionPossible.label = myMenu.dataProvider[myMenu.selectedIndex].LIBELLE_ACTION;
				selectedAction = myMenu.dataProvider[myMenu.selectedIndex].IDINV_ACTIONS; 
			}catch(re : RangeError){
				trace("erreur d'index");
			}
				btActionPossible.popUp = myMenu;
				
		}

		//definit le listener du menu du pop up bouton action 
		private function menuItemClickHandler(event:MenuEvent):void {
			var label:String = event.item.LIBELLE_ACTION; 
			var idaction : int = event.item.IDINV_ACTIONS;       				
			btActionPossible.label = label;				
			selectedAction = idaction; 
			myMenu.selectedIndex = event.index;
			trace(selectedAction);
			btActionPossible.close();	
			btActionPossible.toolTip = event.item.COMMENTAIRE_ACTION;			
		}
		
		//cloture l'operaion ->retour en cours
		//param in idop l'identifiant de l'opération à cloturer
		private function cloturerOperationRec(idop : int):void{
			cloturerOperation(idop,RECENCOURS);
		}
		
		//cloture l'operaion ->retour lettree
		//param in idop l'identifiant de l'opération à cloturer
		private function cloturerOperationRecLettrage(idop : int):void{
			cloturerOperation(idop,RECENCOURSLETTREE);			
		}
		
		/*--------------------------------------- FIN --- ACTION --------------------------------*/
		
		//----------------------------- REMOTING -----------------------------------------------//
		
		
		//Charge l'historique des actions pour une ressource
		private function chargerHistoriqueLigne():void{
					
 			opHistoOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getHistoriqueProduitAction",
																			chargerHistoriqueLigneResultHandler);				
			RemoteObjectUtil.callService(opHistoOpe,
										 idOperation,	
										 produitSelectionnerPouAction.IDINVENTAIRE_PRODUIT);								
			
		}
		
		
		//Affecte les donnnées resultants de la méthode 'chargerHistoriqueLigne' à l'ArrayCollection 'tabHistoEtap' 
		//Met à jour le tableau des ressources utilisés pour la réclamation
		//Et configure le bouton action possible en conséquence 
		private function chargerHistoriqueLigneResultHandler(re : ResultEvent):void{			
			tabHistoEtap = re.result as ArrayCollection;
			
			if (historiqueActionOk){
				myGridProduitsOP.mettreAJourGrid();	
				historiqueActionOk = false;	
				traiterConsequencesEtat(produitSelectionnerPouAction);
			} 	
			else historiqueActionOk = false;											
		}
		
		//--------
		
		//Charge les actions possibles en fonction d'un etat
		//param in idEtat l'identifiant de l'etat pour lequel on veut charger les actions possibles
		private function chargerActionPossible(idEtat : int):void{		
 			opListeActionPossible = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getListeActionPossibles",
																			chargerActionPossibleResultHandler);				
			RemoteObjectUtil.callService(opListeActionPossible,
										 idEtat);
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerActionPossible' à l'ArrayCollection 'tabActionsPossible' 
		//Rend le bouton action possible inactif si aucune action n'est possible
		//Initialise le menu du PopUpMenuButton des action possible
		private function chargerActionPossibleResultHandler(re : ResultEvent):void{					
			tabActionsPossible = re.result as ArrayCollection;	
			if (tabActionsPossible.length < 1) btActionPossible.enabled = false;	
			else btActionPossible.enabled = true && modeEcriture;
			callLater(initMenu);				
		}
		
		
		//------------
		
		//Charge les informations d'une opération
		//param in idop l'identifiant de l'opération dont on veut charger les infos
		private function chargerInfoOperation(idop : int):void{		
	 		opDetailOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"getDetailOperation",
																			chargerInfoOperationResultHandler);				
			RemoteObjectUtil.callService(opDetailOpe,
									 idop);
		}
		
		
		//Affecte les donnnées resultants de la méthode 'chargerInfoOperation' à l'ArrayCollection 'OperationDetail' 
		//charge les infos concernant l'operation maitre de la réclamation référencée par (OperationDetail)
		private function chargerInfoOperationResultHandler(re : ResultEvent):void{		
			
			OperationDetail = re.result as ArrayCollection;		
			chargerInfoOperationMaitre(re.result[0].IDOPERATIONS_MAITRE);
			
		}
		
		//Charge les informations d'une opération
		//param in idop l'identifiant de l'opération dont on veut charger les infos
		private function chargerInfoOperationMaitre(idop : int,encestor : int = MAITRE):void{		
	 		var methodeHandler : Function = chargerInfoOperationMaitreResultHandler;
	 		if (encestor == SUR_MAITRE){
	 			methodeHandler = chargerInfoOperationSurMaitreResultHandler;
	 		};
	 		
	 		opDetailOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"getDetailOperation",
																			methodeHandler);				
			
			RemoteObjectUtil.callService(opDetailOpe,
									 idop);
		}
		
		
		//Affecte les donnnées resultants de la méthode 'chargerInfoOperationMaitre' à l'ArrayCollection 'OperationMaitreDetail' 
		//Renseigne la date de référence dateRef
		//Renseigne le type de l'operation maitre (résiliatio ou réclamation) typeOpmaitre
		//charge les infos concernant l'operation maitre de la réclamation référencée par (OperationDetail)
		private function chargerInfoOperationMaitreResultHandler(re : ResultEvent):void{		
			try{

				typeOpmaitre = re.result[0].IDINV_TYPE_OPE;			
				dateRef = DateFunction.formatDateAsString(new Date(re.result[0].DATE_REF_CALCUL));							 	
				lblDateRef.text = "Date de référence : "+ dateRef;
				OperationMaitreDetail = re.result as ArrayCollection;
				
				var typeOp :  String =  (typeOpmaitre == INVTYPE_OPERATION.VERIF_FACT_RESILIATION)? "RESILIATION":"COMMANDE DE PRODUITS";
				title = " ** " + typeOp +" : "+OperationMaitreDetail[0].LIBELLE_OPERATIONS+ " ** Opération de Réclamation **";
				chargerInfoOperationMaitre(OperationMaitreDetail[0].IDOPERATIONS_MAITRE,SUR_MAITRE);
			}
			catch(re :RangeError){
				trace("Erreur d'index : chargerInfoOperation",re.getStackTrace());
				 
			}catch(e : Error){
				trace("Erreur : chargerInfoOperation",e.getStackTrace());				
			}finally{
				
			}
		}		
		
		private function chargerInfoOperationSurMaitreResultHandler(re : ResultEvent):void{		
			try{
				OperationSurMaitreDetail = re.result as ArrayCollection;
			}
			catch(re :RangeError){
				trace("Erreur d'index : chargerInfoOperation",re.getStackTrace());
				 
			}catch(e : Error){
				trace("Erreur : chargerInfoOperation",e.getStackTrace());				
			}finally{
				dispatchEvent(new Event("detailOpcharge"));
			}
		}	
		
		//----------		
		
		//Pour la réclamation, enregistre une action pour la ressource sélectionnée
		//param in idAction l'identifiant de l'action que l'on veut enregistrer
		private function sauvegarderAction(idAction : int):void{		
			var flagEtat : int;	
			var a : Array = new Array();
			a.push(selectedProduit);
			
			if (idAction != VALIDER_RECLAMATION){
				opSaveAction = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"doAction",
																			sauvegarderActionResultHandler);				
				RemoteObjectUtil.callService(opSaveAction,
											 idOperation,
											 a,
											 idAction,
											 tabHistoEtap[tabHistoEtap.length-1].IDINV_ACTIONS,
											 commentaireAction,
											 "",
											 tabHistoEtap[tabHistoEtap.length-1].ETAT_ACTUEL,
											 tabHistoEtap[tabHistoEtap.length-1].DANS_INVENTAIRE, 
											 dcDateAction.text										 										 
											 )		
			}else{
				validerReclamation(idAction);	
			}
		}
		
		//Handler de la methode 'sauvegarderAction' 
		//si l'enregistrement c'est bien passé met a jour l'hitorique pour une ressource
		private function sauvegarderActionResultHandler(re : ResultEvent):void{			
			if (re.result > 0){
				historiqueActionOk = true;
				chargerHistoriqueLigne();	
				btActionPossible.enabled = false;
				
			}else{
				historiqueActionOk = false;
				Alert.show("Une erreur c'est produite durant l'enregistrement");
			}			 
		}
		
		//-----
		
		//Pour l'opération maitre de la réclamation, enregistre une action pour la ressource sélectionnée
		//param in idAction l'identifiant de l'action que l'on veut enregistrer
		private function validerReclamation(idAction : int):void{
			var flagEtat : int;	
			var a : Array = new Array();
			a.push(selectedProduit);
			
			
			
			
 			opSaveActionOpMaitre = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"doAction",
																			validerReclamationResultHandler);				
			RemoteObjectUtil.callService(opSaveActionOpMaitre,
										 OperationDetail[0].IDOPERATIONS_MAITRE,
										 a,
										 idAction,
										 tabHistoEtap[tabHistoEtap.length-1].IDINV_ACTIONS,
										 commentaireAction,
										 "",
										 tabHistoEtap[tabHistoEtap.length-1].ETAT_ACTUEL,
										 tabHistoEtap[tabHistoEtap.length-1].DANS_INVENTAIRE,
										 dcDateAction.text 										 										 
										 )									
		}
		
		
		//Handler de la methode 'sauvegarderActionOpMaitre' 		
		private function validerReclamationResultHandler(re : ResultEvent):void{			
			if (re.result > 0){
				cloturerOperation(idOperation,typeReclamation);
			}else{
				Alert.show("Une erreur c'est produite durant l'enregistrement");
			}			 
		}
		
		
		//-------
		
		
		//Cloture une operation
		//param in idop l'identifiant de l'opération que l'on veut cloturer		
		private function cloturerOperation(idop : int, typeReclama : int):void{
			typeReclamation = typeReclama;
			opCloturerOpe = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																			"cloturerOperation",
																			cloturerOperationResultHandler);
																			
			RemoteObjectUtil.callService(opCloturerOpe,
										 idop);																					
		}	
		
		//Handler de la methode 'cloturerOperation' 
		//en cas de succes :
		//change titre de la page
		//grise l'ihm
		//dispatch un evenement CloturerOperationEvent de type 'CloturerOperationReclamation' signifiant que l'opération à été cloturer
		//Affiche une alerte d'erreur en cas d'echec
		private function cloturerOperationResultHandler(re : ResultEvent):void{
			if (parseInt(String(re.result)) == 1){													
				lblEtatOperation.text = OPERATION_CLOSE;operationEnCours = 0;					
				disablePanel();		
				var evtObj : CloturerOperationEvent = new CloturerOperationEvent("CloturerOperationReclamation");
			 	evtObj.idOperation =  OperationDetail[0].IDOPERATIONS_MAITRE;			 		 	
				dispatchEvent(evtObj);	
			}else{
				Alert.show("Une erreur s'est produite lors de la cloture de l'operation");
			}	
		}		
		
		//-------------------------		
		
		//charge les lignes de facturation pour une opération
		//param in idop l'identifiant de l'operation concernée
		private function chargerElementGridEltFacture(idop : int):void{
			opListeFactReclam = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListFactureLettreeOperation",
																				chargerElementGridEltFactureResultHandler);				

																	
				RemoteObjectUtil.callService(opListeFactReclam ,idop);
		
		}
		
		//Handler de la methode 'chargerElementGridEltFacture'
		//charge les actions possibles pour le premier element de la liste ramenée
		//Affiche la liste  
		private function chargerElementGridEltFactureResultHandler(re : ResultEvent):void{	
			chargerActionPossible(re.result[0].IDINV_ETAT);				 						
			myGridFacture.setData(re.result as ArrayCollection);
			
		}
		
		//click sur le bouton PDF
		private function btPDFClickHandler(me : MouseEvent):void{
			if (OperationDetail != null){
				var reclamation : ReclamationVO = new ReclamationVO();
				reclamation.ID_CONTACT = OperationSurMaitreDetail[0].IDCDE_CONTACT ;
				reclamation.ID_SOCIETE = OperationSurMaitreDetail[0].IDCDE_CONTACT_SOCIETE;
				reclamation.IDINV_OPERATIONS = OperationDetail[0].IDINV_OPERATIONS;
				reclamation.IDINV_TYPEOP = (typeOpmaitre == INVTYPE_OPERATION.VERIF_FACT_RESILIATION) ? INVTYPE_OPERATION.RESILIATION : INVTYPE_OPERATION.COMMANDE;
				reclamation.LIBELLE_OPERATIONS = title;		
				reclamation.DATE_REF = dateRef;
				afficherPDF(reclamation);	
			}
			
		}
		
		//affiche la reclam sous forme de PDF
		private function afficherPDF(reclamation : ReclamationVO):void{
			var rappToExp : IExportable = new ExportReclamation(reclamation);				 
			var expB : ExportBuilder = new ExportBuilder(rappToExp);
			expB.exporter(ExportReclamation.PDF);
		}
		
		
		
		
	}
}