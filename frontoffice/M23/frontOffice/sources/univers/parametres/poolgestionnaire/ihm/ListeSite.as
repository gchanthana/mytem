package univers.parametres.poolgestionnaire.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.poolgestionnaire.event.ContrainteSuppressionEvent;
	import univers.parametres.poolgestionnaire.event.GestionSiteEvent;
	import univers.parametres.poolgestionnaire.service.SiteService;
	import univers.parametres.poolgestionnaire.vo.Site;
	
	[Bindable]
	public class ListeSite extends VBox
	{
		public var siteService : SiteService = new SiteService();
		
		
		//COMPONANT
		public var dg_site: DataGrid;
		private var popupParamSite : ParametreSiteIHM;
		private var popContrainteSuppressionIHM : ContrainteSuppressionIHM;
	
		
		public var radioAll : RadioButton;
		public var radioNotAll : RadioButton;
		
		
		public function ListeSite()
		{
			
		}
		public function init(evt : FlexEvent):void
		{
			siteService.listeSite(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,null,0);
		}
		public function modifier(site : Site):void
		{
			popupParamSite = new ParametreSiteIHM();
			popupParamSite.site = site;;
			addPop();
		}
		public function addSite(event:MouseEvent):void
		{
			
			popupParamSite = new ParametreSiteIHM();
			addPop();
		}
		

		private function addPop():void
		{
			popupParamSite.addEventListener(GestionSiteEvent.PARAMETRE_SITE_COMPLETE,parametreSiteCompleteHandler);
			PopUpManager.addPopUp(popupParamSite,this,true);
			PopUpManager.centerPopUp(popupParamSite);
			
		}
		public function filtre():void
		{
			var allResult : int;
			if(radioAll.selected)
			{
				allResult = 1;
			}
			else if(radioNotAll.selected)
			{
				allResult = 0
			}
			siteService.listeSite(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,null,allResult);
		}
		private function parametreSiteCompleteHandler(evt : GestionSiteEvent):void
		{
			filtre();
		}
		public function supprimer(data : Site):void
		{
			//ConsoviewAlert.afficherAlertConfirmation("Êtes vous sûr de vouloir supprimer le site "+data.libelle_site+" ?","Confirmation",supprimerBtnValiderCloseEvent);
				siteService.addEventListener(GestionSiteEvent.LISTE_CONTRAINTE_SITE_COMPLETE,listeContrainteOfSiteHandler);
				siteService.listeContrainteOfSite((dg_site.selectedItem as Site).id_site);
		}

		/*private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				siteService.addEventListener(GestionSiteEvent.LISTE_CONTRAINTE_SITE_COMPLETE,listeContrainteOfSiteHandler);
				siteService.listeContrainteOfSite((dg_site.selectedItem as Site).id_site);
			}
		}*/
		private function listeContrainteOfSiteHandler(evt : GestionSiteEvent):void
		{
			if(siteService.col_contrainte_of_site.length>0) // il existe des contraintes qui empêche la suppression
			{
				popContrainteSuppressionIHM = new ContrainteSuppressionIHM();
				popContrainteSuppressionIHM.addEventListener(ContrainteSuppressionEvent.VALIDATE_SUPPRESSION_POOL,supprimerSiteSelected);
				popContrainteSuppressionIHM.listeContraintes = siteService.col_contrainte_of_site;
				if(siteService.col_contrainte_of_site.length>1)
				{
					popContrainteSuppressionIHM.messageDecription = 'Des éléments sont rattachés au site '+(dg_site.selectedItem as Site).libelle_site+'. La suppression de ce site entrainera la suppression de ces liens.';
				}
				else
				{
					popContrainteSuppressionIHM.messageDecription = 'Un élément est rattaché au site '+(dg_site.selectedItem as Site).libelle_site+'. La suppression de ce site entrainera la suppression de ce lien.';
				}
				
				popContrainteSuppressionIHM.messageConfirmation = 'Êtes vous sûr de vouloir supprimer le site ?';
				PopUpManager.addPopUp(popContrainteSuppressionIHM,this,true);
				PopUpManager.centerPopUp(popContrainteSuppressionIHM);
			}
			else // pas de contrainte on peut supprimer
			{
				supprimerSiteSelected();
			}
		}
		private function supprimerSiteSelected(evt : ContrainteSuppressionEvent=null):void
		{
			//TODO REQUETE DE SUPPRESSION
			siteService.addEventListener(GestionSiteEvent.REMOVE_SITE_COMPLETE,remove_complete_handler);
			siteService.removeSite((dg_site.selectedItem as Site).id_site);
			
		}
		private function remove_complete_handler(evt : GestionSiteEvent=null):void
		{
			ConsoviewAlert.afficherOKImage("le site "+(dg_site.selectedItem as Site).libelle_site+" a bien été supprimé.");
			filtre();
		}
		
	}
}