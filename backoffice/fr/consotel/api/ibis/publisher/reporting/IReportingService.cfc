<cfinterface author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.IReportingService" hint="Représente le service de Reporting">
	<cffunction name="getService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService" hint="Retourne l'implémentation du service">
		<cfargument name="user" type="string"  required="true" hint="Login utilisateur pour le service">
		<cfargument name="pwd" type="string"  required="true" hint="Mot de passe utilisateur pour le service">
	</cffunction>

	<cffunction name="createReporting" returntype="fr.consotel.api.ibis.publisher.reporting.IReporting" hint="Retourne l'implémentation IReporting correspondante">
		<cfargument name="props" type="Struct"  required="true" hint="Contient les propriétés et paramètres du reporting à créer">
	</cffunction>
	
	<cffunction name="execute" returntype="void" hint="Exécute un reporting">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
	</cffunction>
	
	<cffunction name="handleNotification" returntype="void" hint="Appelée par l'API lors de la notification d'un reporting
	En principe la notification est créée ou récupérée par l'API et permet de créer l''évènement de type IReportingEvent correspondant
	Cet évènement est ensuite passé à la méthode dispatchReportingEvent() pour la dispatcher vers la stratégie de diffusion du reporting">
		<cfargument name="notification" type="Struct"  required="true" hint="Structure représentant la notification d'un reporting">
	</cffunction>
	
	<cffunction name="dispatchReportingEvent" returntype="void" hint="Dispatche l'évènement en la passant à la méthode handleReportingEvent() de la stratégie de diffusion
	La stratégie de diffusion est créée ou identifiée à partir de son type dont la valeur est retournée par event.getDeliveryType()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true" hint="Contient les infos concernant un reporting">
	</cffunction>
</cfinterface>