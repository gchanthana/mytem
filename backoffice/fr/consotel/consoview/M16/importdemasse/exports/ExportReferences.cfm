﻿	<!---
	
	CODE_INTERNE FLAG_TRAITE IDARTICLE IDCOMMANDE IDEMPLOYE IDSIM IDSOUS_TETE IDTERMINAL IDTYPE_EQUIPEMENT IMEI
	LIBELLE_EQ MATRICULE NB_TERM NOM NUMERO_OPERATION SEGMENT_DATA SEGMENT_FIXE SEGMENT_MOBILE SOUS_TETE S_IMEI S_PIN1 S_PUK1
	
	--->

	<!--- VARIABLES --->

	<cfset libelleheader 	= FORM.LIBELLE_HEADER>
	
	<cfset restrictheader	= FORM.RESTRICTED_HEADER>
	<cfset addheader	 	= FORM.ADDITIONAL_HEADER>
	
	<cfset idscommande	 	= FORM.DATA_TO_EXPORT>
	<cfset idpoolidtype	 	= FORM.ADDITIONAL_VAR>
	<cfset uuid			 	= FORM.SINGLE_UUID>
	<cfset filename		 	= FORM.FILE_NAME>
	
	<cfset idpool	 = ''>
	<cfset idtypecmd = ''>
	<cfset isnophone = ''>
	
	<cfset directory = "/container/importdemasse/">
	
	<cfset uuid = createUUID()>
	
	<cfset path = #directory# & #uuid#>
	
	<!--- CREATE NEW SPREADSHEET --->
	<!--- <cfset sObj = SpreadsheetNew()> --->

	<cfset referencesObject = createObject('component',"fr.consotel.consoview.M16.v2.CommandeService")>
	
	<!--- VARIABLES --->
	
	<cfset myArrayList = ListToArray(idpoolidtype)>
	
	<cfloop index="ix" from="1" to="#ArrayLen(myArrayList)#">
	
		<cfdump var="#ix#">
	
		<cfif ix EQ 1>
		
			<cfset idpool	 = myArrayList[ix]>
		
		<cfelseif ix EQ 2>
		
			<cfset idtypecmd = myArrayList[ix]>
		
		<cfelseif ix EQ 3>
		
			<cfset isnophone = myArrayList[ix]>
		<cfdump var="#isnophone#">
		</cfif>
	
	</cfloop>
	
	<!--- CREATION DU REPERTOIRE ET COPIE DU FICHIER *.XLS POUR POUVOIR ECRIRE DEDANS --->
	
	<cfif not DirectoryExists('#path#')>
		<cflog type="information" text="0-/ Export references">
		<cfdirectory action="Create" directory="#path#" type="dir" mode="777">
		<cflog type="information" text="1-/ Export references">
	<cfelse>
		<cflog type="information" text="1bis-/ Export references">
		<cfset uuid = createUUID()>
		
		<cfset path = #directory# & #uuid#>
		
		<cfdirectory action="Create" directory="#path#" type="dir" mode="777">
	
	</cfif>
	<cflog type="information" text="2-/ Export references">
	<cfif DirectoryExists('#path#')>
		<cflog type="information" text="2bis-/ Export references">
		<cfset templatepath 	= GetCurrentTemplatePath()/>
		<cfset currentdirectory = GetDirectoryFromPath(templatepath)/>
		
			
		<!--- <cfset currentdirectory = GetDirectoryFromPath("/webroot/backoffice-nicolas/fr/consotel/consoview/importdemasse/exports/")/> --->
			
		<!--- <cfset currentdirectory = GetDirectoryFromPath(GetCurrentTemplatePath())/> --->			
		<cflog type="information" text="3-/ Export references">		
		<cffile action="copy" source="#currentdirectory#/exportreferences.xls" destination="#path#" mode="777">
		<cflog type="information" text="4-/ Export references">
		<cffile action="rename" source="#path#/exportreferences.xls" destination="#path#/#filename#" attributes="normal">
		<cflog type="information" text="5-/ Export references">
	</cfif>
	<cflog type="information" text="6-/ Export references">
	<cfspreadsheet action="read" src="#path#/#filename#" sheetname="Sheet1" name="sObj"/> 
	<cflog type="information" text="7-/ Export references">
	<!--- CREATION DU REPERTOIRE ET COPIE DU FICHIER *.XLS POUR POUVOIR ECRIRE DEDANS --->
	
	<!--- TETE DE COLONNES --->
	<cfset SpreadsheetAddRow(sObj,"#libelleheader#")>
	<cflog type="information" text="8-/ Export references">
	<!--- DATA A EXPORTER --->
	<cfset datatoexport = referencesObject.fournirReferencesInfos(#idscommande#,#idpool#)>
	
	<cfquery name="getDataExport" dbtype="query">
		SELECT *
		FROM datatoexport
		ORDER BY IDSOUS_TETE
	</cfquery>
	
	<cfset datatoexport = getDataExport>
	
	<!--- AFFECTATION DES DONNÉES AU FICHIER *.XLS --->
	<cfloop index="idx" from="1" to="#datatoexport.recordcount#">

		<!---<cfset soustete = "n° " & ToString(datatoexport['SOUS_TETE'][idx])>--->
		<cfset soustete = ToString(datatoexport['SOUS_TETE'][idx])>				
		<cfif idtypecmd EQ 1083><!--- Commande de nouvelles lignes mobiles --->
		
			<cfif isnophone EQ true>
			
				<cfset SpreadsheetAddRow(sObj,"#datatoexport['CODE_INTERNE'][idx]#,#datatoexport['NOM'][idx]#,#datatoexport['LIBELLE_EQ'][idx]#,#soustete#,#datatoexport['S_IMEI'][idx]#,#datatoexport['S_PUK1'][idx]#")>
			
			<cfelse>
			
				<cfset SpreadsheetAddRow(sObj,"#datatoexport['CODE_INTERNE'][idx]#,#datatoexport['NOM'][idx]#,#datatoexport['LIBELLE_EQ'][idx]#,#ToString(datatoexport['IMEI'][idx])#,#soustete#,#datatoexport['S_IMEI'][idx]#,#datatoexport['S_PUK1'][idx]#")>
			
			</cfif>
		
		<cfelseif idtypecmd EQ 1583><!--- Commande de nouvelles lignes fixes --->
		
				<cfset SpreadsheetAddRow(sObj,"#datatoexport['CODE_INTERNE'][idx]##datatoexport['NOM'][idx]#,#datatoexport['LIBELLE_EQ'][idx]#,#soustete#")>
		
		<cfelseif idtypecmd EQ 1182><!--- Commande d'équipements mobile nus --->
		
			<cfset SpreadsheetAddRow(sObj,"#datatoexport['CODE_INTERNE'][idx]#,#datatoexport['NOM'][idx]#,#datatoexport['LIBELLE_EQ'][idx]#,#ToString(datatoexport['IMEI'][idx])#")>
		
		<cfelseif idtypecmd EQ 1584><!--- Commande d'équipements fixe nus --->
		
				<cfset SpreadsheetAddRow(sObj,"#datatoexport['CODE_INTERNE'][idx]##datatoexport['NOM'][idx]#,#datatoexport['LIBELLE_EQ'][idx]#,#ToString(datatoexport['IMEI'][idx])#")>
		
		<cfelseif idtypecmd EQ 1382><!--- Réengagement mobile --->
		
			<cfset SpreadsheetAddRow(sObj,"#datatoexport['CODE_INTERNE'][idx]#,#datatoexport['NOM'][idx]#,#datatoexport['LIBELLE_EQ'][idx]#,#ToString(datatoexport['IMEI'][idx])#,#soustete#,#datatoexport['S_IMEI'][idx]#,#datatoexport['S_PUK1'][idx]#")>
		
		<cfelseif idtypecmd EQ 1587><!--- Réengagement fixe --->
		
				<cfset SpreadsheetAddRow(sObj,"#datatoexport['LIBELLE_EQ'][idx]#,#ToString(datatoexport['IMEI'][idx])#,#soustete#")>
		
		</cfif>

	</cfloop>
	
	<cfset format={dataformat="0"}>
		
	<cfif idtypecmd EQ 1083><!--- Commande de nouvelles lignes mobiles --->
		
		<cfif isnophone EQ true>
		
			<cfset spreadsheetformatcolumns(sObj,format,"4,6,7")>
		
		<cfelse>
		
			<cfset spreadsheetformatcolumns(sObj,format,"4,5,6,7")>
		
		</cfif>

	<cfelseif idtypecmd EQ 1583><!--- Commande de nouvelles lignes fixes --->
	
		<cfset spreadsheetformatcolumns(sObj,format,"2,3")>
	
	<cfelseif idtypecmd EQ 1182><!--- Commande d'équipements mobile nus --->
	
		<cfset spreadsheetformatcolumns(sObj,format,"4,6,7")>
	
	<cfelseif idtypecmd EQ 1584><!--- Commande d'équipements fixe nus --->
	
		<cfset spreadsheetformatcolumns(sObj,format,"2,3")>
	
	<cfelseif idtypecmd EQ 1382><!--- Réengagement mobile --->
	
		<cfset spreadsheetformatcolumns(sObj,format,"4,6,7")>
	
	<cfelseif idtypecmd EQ 1587><!--- Réengagement fixe --->
	
		<cfset spreadsheetformatcolumns(sObj,format,"2,3")>
	
	</cfif>
	
	<!--- SAVE IT --->
	<cfspreadsheet action="write" name="sObj" filename="#path#/#filename#" overwrite="true" sheetname="Sheet1"/>

	<!--- TELECHARGEMENT --->
	<cffile action="readbinary" variable="fileContent" file="#path#/#filename#">
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#">
	<cfcontent type="application/vnd.ms-excel" variable="#fileContent#">
	