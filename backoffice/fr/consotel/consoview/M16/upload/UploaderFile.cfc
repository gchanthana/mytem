<cfcomponent output="false" displayname="fr.consotel.consoview.M16.upload.UploaderFile" hint="Gestionnaire d'upload de fichiers">

	<cfset error_from = "module.m16@saaswedo.com">
	<cfset mail_server = "mail-cv.consotel.fr">
	<cfset error_dest = "samuel.divioka@saaswedo.com">

	<cffunction name="uploadFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="currentFile" 			type="fr.consotel.consoview.M16.vo.FileUploadVo" required="true"/>
		<cfargument name="isDataBaseRecord" 	type="Numeric" required="true"/>

			<cfif len(currentFile.fileUUID) eq 0>
				<cfset currentFile.fileUUID = createUUID()>
			</cfif>
			
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var pathFile 		= '/container/M16/'& #currentFile.fileUUID#>
			<cfset var pathFileName 	= '/container/M16/'& #currentFile.fileUUID#>
			<cfset var statusReturn		= 0>

			<cftry> 

				<cfif NOT DirectoryExists('#pathFile#')>

					<cfdirectory action="Create" directory="#pathFile#" type="dir" mode="777">

				</cfif>

				<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M16.RegexRenameFile").changeSpecialCaracters(#currentFile.fileName#)>

				<cfset pathFileName = '/container/M16/'& #currentFile.fileUUID# & '/' & #currentFile.fileName#>

				<cffile action="write" output="#currentFile.fileData#" file="#pathFileName#" mode="777">

				<cfset statusReturn = 1>

			<cfcatch>

				<cflog type="error" text="#CFCATCH.Message#">

				<cfset statusReturn = 0>

				<cfmail server="#mail_server#" port="25" from="#error_from#" to="#error_dest#"
						 subject="[WARN-M16-BDCM]Erreur lors de l'upload de fichiers" type="html">

						#CFCATCH.Message#<br/><br/><br/><br/>

				</cfmail>

			</cfcatch>
			</cftry>

			<cfif isDataBaseRecord GT 0>

				<cfif statusReturn EQ 1>

					<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrementlistattachments">
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#currentFile.fileName#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#currentFile.fileUUID#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#idgestionnaire#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#currentFile.fileSize#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#currentFile.fileJoin#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#currentFile.fileExt#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#currentFile.fileIdCommande#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>

					<cfset statusReturn = p_retour>

				</cfif>

			</cfif>

		<cfreturn statusReturn>
	</cffunction>

	<cffunction name="uploadFiles" access="remote" output="false" returntype="Array" hint="Upload un fichier">
		<cfargument name="filesSelected" 		type="Array" required="true"/>
		<cfargument name="isDataBaseRecord" 	type="Numeric" required="true"/>

			<cfset var resultUpload	= ArrayNew(1)>
			<cfset var lenFiles 	= arrayLen(filesSelected)>
			<cfset var statusReturn	= 0>

			<cfloop index="idx" from="1" to="#lenFiles#">

				<cfset statusReturn = uploadFile(filesSelected[idx], isDataBaseRecord)>

				<cfset ArrayAppend(resultUpload, statusReturn)>

			</cfloop>

		<cfreturn resultUpload>
	</cffunction>

	<cffunction name="setDataBaseFilesUploaded" access="remote" output="false" returntype="Array" hint="Upload un fichier">
		<cfargument name="filesSelected" 	type="Array" required="true"/>
		<cfargument name="idcommande" 		type="Numeric" required="true"/>

			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var currentFile 		= structNew()>
			<cfset var resultUpload		= ArrayNew(1)>
			<cfset var lenFiles 		= arrayLen(filesSelected)>
			<cfset var statusReturn		= 0>

			<cfloop index="idx" from="1" to="#lenFiles#">

				<cfset currentFile = filesSelected[idx]>
				<cfset fileRenamed = createObject("component","fr.consotel.consoview.M16.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
				<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrementlistattachments">
					<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#fileRenamed#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#currentFile.fileUUID#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#idgestionnaire#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#currentFile.fileSize#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#currentFile.fileJoin#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#currentFile.fileExt#"/>
					<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#idcommande#"/>
					<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
				</cfstoredproc>

				<cfset ArrayAppend(resultUpload, p_retour)>

			</cfloop>

		<cfreturn resultUpload>
	</cffunction>

	<cffunction name="renameFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="currentFile" 			type="fr.consotel.consoview.M16.vo.FileUploadVo" required="true"/>
		<cfargument name="newFileName" 			type="String" required="true"/>
		<cfargument name="isDataBaseRecord" 	type="Numeric" required="true"/>

			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var pathFile 		= '/container/M16/'& #currentFile.fileUUID#>
			<cfset var pathFileName 	= '/container/M16/'& #currentFile.fileUUID# & '/' & #currentFile.fileName#>
			<cfset var pathFileNewName 	= '/container/M16/'& #currentFile.fileUUID# & '/' & #newFileName#>
			<cfset var statusReturn		= 0>

			<cftry>

				<cfif NOT DirectoryExists('#pathFile#')>

					<cfset statusReturn = -10>

				<cfelse>


					<cfif NOT FileExists('#pathFileName#')>

						<cfset statusReturn = -11>

					<cfelse>

						<cffile action="rename" source="#pathFileName#" destination="#pathFileNewName#" attributes="normal">

						<cfset statusReturn = 2>

					</cfif>

				</cfif>

			<cfcatch>

				<cflog type="error" text="#CFCATCH.Message#">

				<cfset statusReturn = 0>

				<cfmail server="#mail_server#" port="25" from="#error_from#" to="#error_dest#"
						 subject="[WARN-M16-BDCM]Erreur lors de la suppression de fichiers" type="html">

						#CFCATCH.Message#<br/><br/><br/><br/>

				</cfmail>

			</cfcatch>
			</cftry>

			<cfif isDataBaseRecord GT 0>

				<cfif statusReturn GT 0>

					<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.renameattachment">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_fileid"   value="#currentFile.fileId#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_new_name" value="#newFileName#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>

					<cfset statusReturn = p_retour>

				</cfif>

			</cfif>

		<cfreturn statusReturn>
	</cffunction>

	<cffunction name="removeFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="currentFile" 			type="fr.consotel.consoview.M16.vo.FileUploadVo" required="true"/>
		<cfargument name="isDataBaseRecord" 	type="Numeric" required="true"/>

			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var pathFile 		= '/container/M16/'& #currentFile.fileUUID#>
			<cfset var pathFileName 	= '/container/M16/'& #currentFile.fileUUID# & '/' & #currentFile.fileName#>
			<cfset var statusReturn		= 0>

			<cftry>

				<cfif NOT DirectoryExists('#pathFile#')>

					<cfset statusReturn = 10>

				<cfelse>


					<cfif NOT FileExists('#pathFileName#')>

						<cfset statusReturn = 11>

					<cfelse>

						<cffile action="delete" file="#pathFileName#">

						<cfset statusReturn = 2>

					</cfif>

				</cfif>

			<cfcatch>

				<cflog type="error" text="#CFCATCH.Message#">

				<cfset statusReturn = 0>

				<cfmail server="#mail_server#" port="25" from="#error_from#" to="#error_dest#"
						 subject="[WARN-M16-BDCM]Erreur lors de la suppression de fichiers" type="html">

						#CFCATCH.Message#<br/><br/><br/><br/>

				</cfmail>

			</cfcatch>
			</cftry>

			<cfif isDataBaseRecord GT 0>

				<cfif statusReturn GT 0>

					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.eraseAttachment">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_fileid" value="#currentFile.fileId#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>

					<cfset statusReturn = p_retour>

				</cfif>

			</cfif>

		<cfreturn statusReturn>
	</cffunction>

	<cffunction name="copyFiles" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="queyrFiles" type="Query" required="true"/>
		<cfargument name="idcommande" type="Numeric" required="true"/>

			<cfset var newUUID 			= CreateUUID()>
			<cfset var filesCopy 		= ArrayNew(1)>
			<cfset var filesRsltCopy 	= ArrayNew(1)>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var containerPath	= '/container/M16/'>
			<cfset var pathFile 		= '/container/M16/'& #newUUID#>
			<cfset var pathNFileName 	= ''>
			<cfset var pathLFileName 	= ''>
			<cfset var lenFiles			= queyrFiles.recordCount>
			<cfset var statusReturn		= 0>

			<cfif lenFiles GT 0>

				<cfloop query="queyrFiles">

					<cfset newFiles 				= structNew()>
					<cfset newFiles.fileData 		= structNew()>
					<cfset newFiles.fileDate 		= Now()>
					<cfset newFiles.fileName		= queyrFiles['FILE_NAME'][queyrFiles.currentRow]>
					<cfset newFiles.fileExt			= queyrFiles['FORMAT'][queyrFiles.currentRow]>
					<cfset newFiles.fileUUID		= newUUID>
					<cfset newFiles.fileSize		= queyrFiles['FILE_SIZE'][queyrFiles.currentRow]>
					<cfset newFiles.fileJoin		= queyrFiles['JOIN_MAIL'][queyrFiles.currentRow]>
					<cfset newFiles.fileIdRacine	= idracine>
					<cfset newFiles.fileIdCommande	= idcommande>
					<cfset newFiles.filePath		= pathFile>
					<cfset newFiles.fileLastPath	= queyrFiles['PATH'][queyrFiles.currentRow]>
					<cfset newFiles.fileStatus		= 0>

					<cfset pathLFileName = #containerPath# & #queyrFiles['PATH'][queyrFiles.currentRow]# & '/' & #queyrFiles['FILE_NAME'][queyrFiles.currentRow]#>

					<cfif FileExists('#pathLFileName#')>

						<cfset ArrayAppend(filesCopy, newFiles)>

					</cfif>

				</cfloop>

				<cfif NOT DirectoryExists('#pathFile#')>

					<cfdirectory action="Create" directory="#pathFile#" type="dir" mode="777">

				</cfif>

				<cfif DirectoryExists('#pathFile#')>

					<cfset lenFiles	= arrayLen(filesCopy)>

					<cfloop index="idxCopy" from="1" to="#lenFiles#">

						<cfset pathLFileName = #containerPath# & #filesCopy[idxCopy].fileLastPath# & '/' & #filesCopy[idxCopy].fileName#>
						<cfset pathNFileName = #containerPath# & #filesCopy[idxCopy].fileUUID# & '/' & #filesCopy[idxCopy].fileName#>

						<cftry>

							<cffile action="copy" destination="#pathNFileName#" source="#pathLFileName#">

							<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.enregistrementlistattachments">
								<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#filesCopy[idxCopy].fileName#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#filesCopy[idxCopy].fileUUID#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#idgestionnaire#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#filesCopy[idxCopy].fileSize#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#filesCopy[idxCopy].fileJoin#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#filesCopy[idxCopy].fileExt#"/>
								<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#filesCopy[idxCopy].fileIdCommande#"/>
								<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
							</cfstoredproc>

							<cfset filesCopy[idxCopy].fileStatus = p_retour>

						<cfcatch>

							<cflog type="error" text="#CFCATCH.Message#">

							<cfset statusReturn = -11>

							<cfmail server="#mail_server#" port="25" from="#error_from#" to="#error_dest#"
									 subject="[WARN-M16-BDCM]Erreur lors de la copy de fichiers" type="html">

									#CFCATCH.Message#<br/><br/><br/><br/>

							</cfmail>

						</cfcatch>
						</cftry>

						<cfset ArrayAppend(filesRsltCopy, statusReturn)>

					</cfloop>

					<cfif statusReturn EQ 0>

						<cfset statusReturn = 1>

					</cfif>

				</cfif>

			<cfelse>

				<cfset statusReturn = 2>

			</cfif>

		<cfreturn statusReturn>
	</cffunction>

</cfcomponent>
