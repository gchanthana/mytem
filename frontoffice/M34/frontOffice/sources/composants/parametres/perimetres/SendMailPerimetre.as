package composants.parametres.perimetres
{
	import composants.util.SendMail;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	import mx.containers.TitleWindow;
	import composants.mail.MailVO;
	import flash.events.MouseEvent;
	import mx.controls.Button;
	import mx.controls.RichTextEditor;
	import mx.controls.TextInput;
	import mx.controls.Label;
	import mx.controls.CheckBox;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	 
	import composants.parametres.perimetres.gabarits.GabaritAffectations;
	import composants.parametres.perimetres.gabarits.AbstractGabarit;
	import composants.util.ConsoviewFormatter;
	import mx.controls.TextArea;


	
	
	
	//---- EVENTS  ---------------------------------------------------
	[Event(name='mail_sent')]
	//--- FIN EVENTS --------------------------------------------------
	
	
	
	
	
	
	[Bindable]
	public class SendMailPerimetre extends TitleWindow
	{	
		
		//--- VARS -------------------------------------------------
		
		//composants
		public var btnClose : Button;
		public var btnEnvoyer : Button;
		public var rteMessage : TextArea;
		
		public var txtExpediteur : Label;
		public var txtDest : Label;
		public var txtModule : Label;
		public var txtcc : TextInput;
		public var txtcci : TextInput;
		public var txtSujet : TextInput;
		public var cbCopie : CheckBox;
		public var cbCopieOperateur : CheckBox;		
		//fin composants
		
		//Les infos pour le mask du mail
		protected var _infosObject : Object;	
		//Le mail
		protected var _mail : MailVO;	

		//--- FIN VARS ---------------------------------------------
		
		
		
		//--- CONSTANTES --------------------------------------------
		public static const MAIL_ENVOYE : String = 'mail_sent';				
		//---- FIN CONSTANTES ----------------------------------------
		
		
		
		
		
		
		
		//--- HANDLERS ----------------------------------------------
		protected function creationCompleteHandler(event:FlexEvent):void {
			PopUpManager.centerPopUp(this);
		}
		
		protected function closeEventHandler(ce : CloseEvent):void {
			PopUpManager.removePopUp(this);
		}
		
		protected function btnCloseClickHandler(me : MouseEvent):void {
			PopUpManager.removePopUp(this);
		}
		protected function btnEnvoyerClickHandler(me : MouseEvent):void{
			sendTheMail();	
		}
		//--- FIN HANDLERS -------------------------------------------
		
		
		
		
		
		
		
		
		//-------- PUBLIC -------------------------------------------
		
		//Les infos pour le corps du mail
		public function set infosObject(infos : Object):void{
			_infosObject = infos;
			configRteMessage();
			
		}
		public function get infosObject():Object{
			return _infosObject;
		}
		
		
		
		//initialisation des donnees du mail
		public function initMail(module:String,sujet:String,dest:String):void {
			
			txtModule.text=module;
			txtSujet.text=sujet; 
			txtExpediteur.text=CvAccessManager.getSession().USER.PRENOM + " " +
					CvAccessManager.getSession().USER.NOM;								
			txtDest.text = dest;
			
			_mail = new MailVO();
			_mail.expediteur = CvAccessManager.getSession().USER.EMAIL;
			_mail.destinataire = dest;
		}
		//------ FIN PUBLIC -----------------------------------------
		
		
		
		
		
		
		
		//-------- PROTECTED -------------------------------------------
		override protected function commitProperties():void{
			super.commitProperties();
			
			addEventListener(CloseEvent.CLOSE,closeEventHandler);
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			
			btnClose.addEventListener(MouseEvent.CLICK,btnCloseClickHandler);
			btnEnvoyer.addEventListener(MouseEvent.CLICK,btnEnvoyerClickHandler);
						
			rteMessage.styleName="TextInputAccueil";
			//rteMessage.percentHeight=100;			
			//rteMessage.showControlBar = false;	
		}
		
		
		//Envoi du message
		protected function sendTheMail():void {
						
			_mail.cc = txtcc.text;
			_mail.bcc = txtcci.text;
			_mail.module = txtModule.text;			
			_mail.copiePourExpediteur = (cbCopie.selected)?"YES":"NO";
			_mail.copiePourOperateur = (cbCopieOperateur.selected)?"YES":"NO";			
			_mail.sujet = txtSujet.text;
			_mail.message = rteMessage.htmlText;
//			rteMessage.fontSizeCombo.selectedIndex = 1;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.parametres.perimetres.mail.MailSender",
								"envoyer",
								sendTheMailResultHandler);			
								
			RemoteObjectUtil.callService(op,_mail);		
		}
		
		
		
		//Affichage du corps du message
		protected function configRteMessage():void{
			var raison_sociale : String = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
			var gestionnaire : String = CvAccessManager.getSession().USER.PRENOM + " " + CvAccessManager.getSession().USER.NOM;
			var gestionnaireCible : String = infosObject.INFOS_DEST[0][0].PRENOM +" "+infosObject.INFOS_DEST[0][0].NOM;
			var listeLignes : String = printListeSousTete(infosObject.LIGNES);			
			
			
			rteMessage.htmlText =  
					"<p>Société : <b>"+raison_sociale+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+"<p>Bonjour,</p>"	
					+"<br/>"				
					+"<p>Les lignes ci-dessous font l'objet d'un transfert de compte</p>"
					+"<br/>"
					+"Origine des lignes <br/>"
					+ infosObject.INFOS_DEST[2][0].TYPE_NIVEAU + " : <b>" +infosObject.INFOS_DEST[2][0].LIBELLE_GROUPE_CLIENT  + "</b>. Gestionnaire : <b>" + gestionnaire + "</b>. <br/>"
					+"Destination <br/>" 
					+ infosObject.INFOS_DEST[1][0].TYPE_NIVEAU + " : <b>" +infosObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT  + "</b>. Gestionnaire : <b>" + gestionnaireCible + "</b>. <br/>"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+"<br/><font color='#FF1200'><b>Message à "+ gestionnaireCible +".</b></font>"
					+"<br/>"
					+"<br/>"
					+"<p>Merci de prendre bonne note de ce transfert. Les lignes ont été affectées dans <b><i>CONSOVIEW</i></b> sur : <b> "+ infosObject.INFOS_DEST[1][0].TYPE_NIVEAU + " " +infosObject.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT + " </b> par <b>" + gestionnaire + "</b></p>"
					+"<p>En cas d’erreur merci de prendre contact avec <b> "+ gestionnaire + "</b> "+ CvAccessManager.getSession().USER.EMAIL + ".</p>"
					+"<br/>_______________________________________________________________________________________________"
					+"<br/><font color='#FF1200'><b>Message à l'operateur</b></font>"
					+"<br/>"
					+"<br/>"
					+"<p>1.    Merci de procéder à la correction des la Référence <b>"+raison_sociale+"</b> de ces lignes (si renseigné).</p>"	
					+"<p>2.    Merci de procéder au changement de compte (si renseigné).</p>"	
					+"<br/>"
					+"<br/>"
					+"<p>Cordialement,</p>"
					+"<br/>"
					+"<b>"+gestionnaire+ ".</b>"
					+"<br>"
					+"<b>"+raison_sociale+"</b>";
			updateDisplayList(unscaledHeight,unscaledWidth);
		}
		
		
		//------------- FIN PROTECTED -------------------------------------
		
		
		
		
		
		
		
		
		
		//------------- PRIVATE --------------------------------------------
		private function sendTheMailResultHandler(re : ResultEvent):void{	
			if (re.result > 0){
				dispatchEvent(new Event(MAIL_ENVOYE));
			}else{
				Alert.show("erreur " + re.result , "Erreur");
			}
			PopUpManager.removePopUp(this);
		}
		
		private function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var titulaire: String = "";
					if (liste[i].COMMENTAIRES != null && String(liste[i].COMMENTAIRES).length > 0) titulaire = " (" + liste[i].COMMENTAIRES + ")";
					output = output + liste[i].LIBELLE_TYPE_LIGNE + " : <b>"+ ConsoviewFormatter.formatPhoneNumber(liste[i].SOUS_TETE) + titulaire  + "</b>\t\t\t\tcompte actuel : ____________,\t\t" + "compte cible : ____________ <br/>";
				}
			}
			return output;
		}
		//------------ FIN PRIVATE ---------------------------------------
		
		
	}
}