package univers.inventaire.commande.system
{
	///////////////////////////////////////////////////////////
	//  GestionCommandeMobile.as
	//  Macromedia ActionScript Implementation of the Class GestionCommandeMobile
	//  Generated by Enterprise Architect
	//  Created on:      09-sept.-2008 17:18:02
	//  Original author: samuel.divioka
	///////////////////////////////////////////////////////////

	import composants.util.ConsoviewUtil;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	[Bindable]
	/**
	 * @author samuel.divioka
	 * @version 1.0
	 * @created 09-sept.-2008 17:18:02
	 */
	public class GestionCommandeMobile extends AbstractGestionCommande
	{
		
		private static const CFC_GestionCommande:String = "fr.consotel.consoview.inventaire.commande.GestionCommande";
		
		private var _selectedIndex:Number;
		public function get selectedIndex():Number
		{
			return _selectedIndex
		}
		public function set selectedIndex(value:Number):void
		{
			_selectedIndex = value
		}
		
		public var selectedCommande:Commande;
		
		
		
		public function GestionCommandeMobile(){
			Alert.okLabel = "Fermer";
			Alert.buttonWidth = 100;
		}
		
	    /**
		 * Enregistrement de la commande puis de son panier 
		 * @param commande
		 * @param panier
		 **/
	    override public function enregistrerCommande(commande:Commande, articles:XML): void
	    {	
	    	if(articles.localName() != "articles") return;
	    	
	    	commande.SEGMENT_MOBILE = 1;
	    	commande.SEGMENT_FIXE = 0;
	    	
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionCommande,
	    																			"enregistrerCommande",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result > 0)	    	{
																			    			commande.IDCOMMANDE = Number(event.result);
																				    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Erreur");
																				    	}	
																			    	}
	    																		);
			RemoteObjectUtil.callService(op,commande,articles);
	    }

	    /**
		 * fournit la liste des commandes d'un client (pour une racine) suivant la clef de
		 * recherche et l' intervale de dates.
		 * 
		 * param.dateDebut &lt; commande.DATE &lt; param.dateFin;
		 * clef de recherche =
		 * commande.NUMERO_COMMANDE ;
		 * commande.LIBELLE_COMMANDE;
		 * commande.REVENDEUR;
		 * commande.OPERATEUR;
		 * commande.REFERENCE_CLIENT;
		 * commande.REFERENCE_REVENDEUR;
		 * 
		 * 
		 * @param dateDebut
		 * @param dateFin
		 * @param Clef
		 * @param segment
		 */
	    override public function fournirInventaireCommandes(dateDebutPeriode:Date, dateFinPeriode:Date, clef:String, segment:Number = 2): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionCommande,
	    																			"fournirInventaireOperation",
	    																			fournirInventaireCommandesResultHandler);
			RemoteObjectUtil.callService(op,dateDebutPeriode,dateFinPeriode,clef,segment)
	    }

	    /**
		 * Met à jour l'inventaire des commandes
		 * 
		 * @param event
		 */
	    override protected function fournirInventaireCommandesResultHandler(event:ResultEvent): void
	    {
	    	inventaireCommandes = mappDataListeCommandes(event.result as ArrayCollection);
	    	if(selectedCommande != null)
	    	{
	    		selectedIndex = ConsoviewUtil.getIndexById(inventaireCommandes,"IDCOMMANDE",selectedCommande.IDCOMMANDE);
	    	}
	    	updateListeEtats(inventaireCommandes);
	    	updateListeRevendeurs(inventaireCommandes);
	    }
	    
		
		
		/**
		 * Met à jour les informations d'une commande
		 * 
		 * @param event
		 */
	    override public function majInfosLivraisonCommande(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionCommande,
	    																			"majInfosLivraisonCommande",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result > 0)	    	
																			    		{																			    			
																				    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_UPDATED));
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Erreur");
																				    	}	
																			    	});
			RemoteObjectUtil.callService(op,commande)
	    }
	    
	     /**
		 * Mis à jour des infos de la commande pass�e en parametre.
		 * 
		 * @param commande
		 */
	    override public function majCommande(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionCommande,
	    																			"majCommande",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result > 0)	    	
																			    		{								
																			    			Alert.show("Informations modifiés","Info");											    			
																				    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_UPDATED));
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Erreur");
																				    	}	
																			    	});
			RemoteObjectUtil.callService(op,commande,0)
	    }

	   
		/**
		 * Fournit le détail d'une commande
		 * 
		 * @param event
		 */
	    override public function fournirDetailOperation(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			CFC_GestionCommande,
	    																			"fournirDetailOperation",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result && (event.result as ArrayCollection).length > 0)	    	
																			    		{																			    			
																				    		mappDataCommande(commande,event.result[0]);
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Pas de donnée");
																				    	}	
																			    	});
			RemoteObjectUtil.callService(op,commande.IDCOMMANDE)
	    }


		/**
		 * Supprime une commande
		 * 						
		 * @return 	retour
		 * 		
		 * 	1 si l’action s’est bien déroulée, 
      	 *	
      	 *					-1: si la commande a des ressources qui sont attachés à des contrats.
         *                  -2:si la commande a des équipements attachés à des contrats
         *                  -3:si la commande est attachée à des contrats
         *                  -4:Autre erreur
         * 
		 * @param event
		 */
	    override public function supprimerCommande(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil
	    									.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	CFC_GestionCommande,
																	"supprimerCommande",
																	function(event:ResultEvent):void
															    	{
															    		if(event.result > 0)	    	
															    		{																			    			
																    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_DELETED));
																    		commande = null;
																    		updateListeEtats(inventaireCommandes);
																    	}
																    	else
																    	{
																    		Alert.show("Erreur");
																    	}	
															    	});
			RemoteObjectUtil.callService(op,commande)
	    }	    
	    
	    
	    
	    
	    private function mappDataListeCommandes(values : ICollectionView):ArrayCollection{
			var retour : ArrayCollection = new ArrayCollection();
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
				while(!cursor.afterLast){
					var commandeObj : Commande = new Commande();
					commandeObj.IDCOMMANDE = cursor.current.IDCOMMANDE;
					commandeObj.REF_CLIENT =cursor.current.REF_CLIENT;
					commandeObj.LIBELLE_REVENDEUR =cursor.current.LIBELLE_REVENDEUR;
					commandeObj.IDREVENDEUR =cursor.current.IDREVENDEUR;
					commandeObj.LIBELLE_COMMANDE =cursor.current.LIBELLE;
					commandeObj.TYPE_OPERATION =cursor.current.TYPE_OPERATION;
					commandeObj.LIBELLE_SITELIVRAISON =cursor.current.NOM_SITE;	
					commandeObj.NUMERO_COMMANDE =cursor.current.NUMERO_OPERATION;
					commandeObj.DATE_COMMANDE = cursor.current.DATE_ENVOI;
					commandeObj.LIVRAISON_PREVUE_LE = cursor.current.DATE_EFFET_PRE;	
					commandeObj.IDLAST_ETAT =cursor.current.IDINV_ETAT;
					commandeObj.IDLAST_ACTION =cursor.current.IDINV_ACTIONS;
					commandeObj.LIBELLE_LASTETAT =cursor.current.LIBELLE_ETAT;
					commandeObj.LIBELLE_LASTACTION =cursor.current.LIBELLE_ACTION;
					commandeObj.NUMERO_TRACKING =cursor.current.NUMERO_TRACKING;
					commandeObj.IDTRANSPORTEUR =cursor.current.IDTRANSPORTEUR;	
					commandeObj.MONTANT = Number(cursor.current.MONTANT);
					commandeObj.IDPOOL_GESTIONNAIRE = cursor.current.IDPOOL;
					commandeObj.LIVREE_LE = cursor.current.LIVREE_LE;
					commandeObj.EXPEDIE_LE = cursor.current.EXPEDIE_LE;
					commandeObj.ENCOURS= Number(cursor.current.EN_COURS);
					retour.addItem(commandeObj);
					cursor.moveNext();
				}
			}
			return retour 
		}
		
		private function mappDataCommande(commande : Commande, value : Object):void{
				commande.NUMERO_COMMANDE = value.NUMERO_OPERATION;
				commande.IDOPERATEUR = value.OPERATEURID;
				commande.LIBELLE_COMMANDE = value.LIBELLE;
				commande.REF_CLIENT = value.REF_CLIENT;
				commande.REF_REVENDEUR = value.REF_REVENDEUR;
				commande.LIVRAISON_PREVUE_LE = value.DATE_EFFET_PREVUE;
				commande.COMMENTAIRES = value.COMMENTAIRES;
				commande.LIBELLE_REVENDEUR = value.LIBELLE_REVENDEUR;
				commande.LIBELLE_OPERATEUR = value.LIBELLE_OPERATEUR;
				commande.LIBELLE_COMPTE = value.LIBELLE_COMPTE;
				commande.LIBELLE_SOUSCOMPTE = value.LIBELLE_SOUSCOMPTE;
				commande.NUMERO_TRACKING = value.NUMERO_TRACKING;
				commande.IDTRANSPORTEUR = value.IDTRANSPORTEUR;
				commande.IDSITELIVRAISON = value.IDSITE_LIVRAISON;
				commande.LIBELLE_SITELIVRAISON = value.LIBELLE_SITELIVRAISON;	
				commande.IDPOOL_GESTIONNAIRE = value.IDPOOL_GESTIONNAIRE;	
				commande.LIBELLE_POOL = value.LIBELLE_POOLGESTIONNAIRE;
				commande.IDGESTIONNAIRE_MODIF = value.IDGESTIONNAIRE_MODIF;
				commande.IDGESTIONNAIRE_CREATE = value.IDGESTIONNAIRE_CREATE;
				commande.CREEE_PAR = value.LIBELLEGESTIONNAIRE_CREATE;
				commande.MODIFIEE_PAR = value.LIBELLEGESTIONNAIRE_MODIF;
				commande.SEGMENT_FIXE = value.SEGMENT_FIXE;
				commande.SEGMENT_MOBILE = value.SEGMENT_MOBILE;
				commande.ENCOURS = Number(value.EN_COURS);
				commande.EXPEDIE_LE =  value.EXPEDIE_LE; 
				commande.PATRONYME_CONTACT = value.NOM_CONTACT;
				commande.IDCONTACT = value.IDCDE_CONTACT;
				commande.LIVREE_LE = value.LIVREE_LE;
				commande.MONTANT = Number(value.MONTANT);
				commande.IDSOCIETE = value.IDSOCIETE;
		}
		
		
		private function updateListeEtats(values:ICollectionView):void
		{	
			if(listeEtatsCommandes != null) listeEtatsCommandes = null
			listeEtatsCommandes = new ArrayCollection();
						
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.getIndexById(listeEtatsCommandes,"IDETAT",cursor.current.IDLAST_ETAT) < 0)
					{
						listeEtatsCommandes.source.push({IDETAT:cursor.current.IDLAST_ETAT,LIBELLE_ETAT:cursor.current.LIBELLE_LASTETAT});
					}	    			
					cursor.moveNext();
				}
			}
			
			listeEtatsCommandes.source.unshift({IDETAT:-1,LIBELLE_ETAT:"Tous"});
		}
	    
	    
	    private function updateListeRevendeurs(values:ICollectionView):void
		{	
			if(listeRevendeursCommandes != null) listeRevendeursCommandes = null
			listeRevendeursCommandes = new ArrayCollection();
						
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.getIndexById(listeRevendeursCommandes,"IDREVENDEUR",cursor.current.IDREVENDEUR) < 0)
					{
						listeRevendeursCommandes.source.push({IDREVENDEUR:cursor.current.IDREVENDEUR,LIBELLE_REVENDEUR:cursor.current.LIBELLE_REVENDEUR});
					}   				    			
					cursor.moveNext();
				}
			}
			listeRevendeursCommandes.source.unshift({IDREVENDEUR:-1,LIBELLE_REVENDEUR:"Tous"});
		}
		
	}//end GestionCommandeMobile
}	