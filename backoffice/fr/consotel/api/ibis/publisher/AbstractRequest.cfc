<cfcomponent displayname="fr.consotel.api.ibis.publisher.AbstractRequest" implements="fr.consotel.api.ibis.publisher.PublisherRequest">
	<cffunction access="public" name="selfFactoryMethod" returntype="fr.consotel.api.ibis.publisher.PublisherRequest" hint="Returns an initialized instance">
		<cfargument name="properties" type="struct" required="true" hint="Minimum properties required to build ReportRequest">
		<!--- KEY LIST VALIDATOR STRATEGY --->
		<cfset var tmpKeyValidator=createObject("component","fr.consotel.api.utils.map.DefaultKeyListValidator")>
		<!--- List of available key by property category (_REQ : Required, _OPT : Optional) --->
		<!--- KEYLIST CATEGORY SUFFIX NAMING --->
		<cfset THIS["REQ"]="_KEYS_REQ">
		<cfset THIS["OPT"]="_KEYS_OPT">
		<!--- REPORT PROPERTIES --->
		<cfset THIS["REPORT_KEYS_REQ"]="reportAbsolutePath">
		<cfset THIS["REPORT_KEYS_OPT"]="attributeTemplate">
		<!--- DATA PROPERTIES --->
		<cfset VARIABLES["DATA_TYPE"]="UNKNOWN">
		<!--- DATA TYPES --->
		<cfset THIS["dynamicFile"]="DYNAMIC_FILE">
		<cfset THIS["dynamicJdbc"]="DYNAMIC_JDBC">
		<cfset THIS["base64Data"]="BASE64_DATA">
		<cfset THIS["fileNameData"]="FILENAME_DATA">
		<cfset THIS["xmlData"]="XML_DATA">
		<!--- Keys used to determine type for dynamic datasource (TWO TYPES) : FILE,JDBC --->
		<!--- dynamicDataSource.FileDataSource : All keys are OPTIONAL in BIP SOAP API but this implementation has defined its own REQUIRED keys --->
		<cfset THIS[THIS["dynamicFile"] & THIS["REQ"]]="dynamicDataSourcePath,temporaryDataSource">
		<cfset THIS[THIS["dynamicFile"] & THIS["OPT"]]="">
		<!--- dynamicDataSource.JDBCDataSource : All keys are OPTIONAL in BIP SOAP API but this implementation has defined its own REQUIRED keys --->
		<cfset THIS[THIS["dynamicJdbc"] & THIS["REQ"]]="dataSourceName">
		<cfset THIS[THIS["dynamicJdbc"] & THIS["OPT"]]="JDBCDriverClass,JDBCDriverType,JDBCURL,JDBCUserName,JDBCPassword">
		<!--- Base64 Data --->
		<cfset THIS[THIS["base64Data"] & THIS["REQ"]]="">
		<cfset THIS[THIS["base64Data"] & THIS["OPT"]]="reportData">
		<!--- Filename for FILE datasource --->
		<cfset THIS[THIS["fileNameData"] & THIS["REQ"]]="">
		<cfset THIS[THIS["fileNameData"] & THIS["OPT"]]="reportDataFileName">
		<!--- XML filename for XML Data --->
		<cfset THIS[THIS["xmlData"] & THIS["REQ"]]="">
		<cfset THIS[THIS["xmlData"] & THIS["OPT"]]="reportRawData">
		<!--- OUTPUT PROPERTIES --->
		<cfset VARIABLES["OUTPUT_TYPE"]="UNKNOWN">
		<!--- OUTPUT TYPES --->
		<cfset THIS["runLocalType"]="RUN_LOCAL">
		<cfset THIS["localType"]="LOCAL">
		<cfset THIS["ftpType"]="FTP">
		<cfset THIS["emailType"]="EMAIL">
		<!--- RUN LOCAL : Generates report in local directory (runReport) --->
		<cfset THIS[THIS["runLocalType"] & THIS["REQ"]]="">
		<cfset THIS[THIS["runLocalType"] & THIS["OPT"]]="reportOutputPath">
		<!--- LOCAL destination : Generates report local directory (scheduleReport)--->
		<cfset THIS[THIS["localType"] & THIS["REQ"]]="">
		<cfset THIS[THIS["localType"] & THIS["OPT"]]="destination">
		<!--- FTP destination : Generates report in an FTP directory (scheduleReport) --->
		<cfset THIS[THIS["ftpType"] & THIS["REQ"]]="remoteFile">
		<cfset THIS[THIS["ftpType"] & THIS["OPT"]]="ftpServerName,ftpUserName,ftpUserPassword,sftpOption">
		<!--- EMAIL destination : Sends report in an EMAIL (scheduleReport) --->
		<cfset THIS[THIS["emailType"] & THIS["REQ"]]="emailFrom,emailTo,emailSubject">
		<cfset THIS[THIS["emailType"] & THIS["OPT"]]="emailServerName,emailBody,emailCC,emailBCC">
		<!--- Other optional OUTPUT options --->
		<cfset THIS["OUTPUT_KEYS_OPT"]="attributeFormat,scheduleBurstringOption,contentType">
		<!--- EXECUTION PROPERTIES : Only property jobLocale and jobCalendar are in ScheduleRequest --->
		<cfset THIS["EXECUTION_KEYS_OPT"]="jobLocale,jobCalendar,attributeLocale,flattenXML,sizeOfDataChunkDownload,attributeCalendar,byPassCache,useUTF8Option">
		<!--- NOTIFICATION PROPERTIES --->
		<cfset THIS["NOTIFICATION_KEYS_OPT"]="notifyWhenSuccess,notifyWhenWarning,notifyWhenFailed,notificationTo," &
				"httpNotificationServer,httpNotificationUserName,httpNotificationPassword">
		<!--- HISTORY PROPERTIES --->
		<cfset THIS["HISTORY_KEYS_OPT"]="saveDataOption,saveOutputOption,schedulePublicOption,userJobName">
		<!--- *********************************** --->
		<!--- SET CONTEXT KEY LIST VALIDATOR STRATEGY --->
		<cfset VARIABLES["keyListValidator"]=
						createObject("component","fr.consotel.api.utils.map.StructUtils").setKeyListValidatorStrategy(tmpKeyValidator)>
		<!--- REPORT REQUEST --->
		<cfset VARIABLES["reportRequest"]=structNew()>
		<!--- DELIVERY REQUEST --->
		<cfset VARIABLES["deliveryRequest"]=structNew()>
		<!--- SCHEDULE REQUEST --->
		<cfset VARIABLES["scheduleRequest"]=structNew()>
		<!--- COMPOSITION --->
		<cfset VARIABLES["scheduleRequest"]["reportRequest"]=getReportRequest()>
		<cfset VARIABLES["scheduleRequest"]["deliveryRequest"]=getDeliveryRequest()>
		<cftrace category="IBIS" type="warning" text="selfFactoryMethod() called : Current version ignores arguments.properties value">
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getParameters" returntype="array" hint="Return ReportRequest parameter list">
		<cfset var tmpReportRequest=getReportRequest()>
		<cfset var parametersKey="parameterNameValues">
		<cfif structKeyExists(tmpReportRequest,parametersKey)>
			<cfreturn tmpReportRequest[parametersKey]>
		<cfelse>
			<cfreturn arrayNew(1)>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="addParameter" returntype="boolean" hint="Adds a parameter to ReportRequest. TRUE if success and FALSE otherwise">
		<cfargument name="parameterName" type="string" required="true" hint="Parameter name">
		<cfargument name="parameterValues" type="array" required="true" hint="Parameter list of values">
		<cfset var parametersKey="parameterNameValues">
		<cfset var tmpParameters=getParameters()>
		<cftry>
			<cfloop index="i" from="1" to="#arrayLen(tmpParameters)#">
				<cfif UCASE(tmpParameters[i]["name"]) EQ UCASE(arguments["parameterName"])>
					<cfset tmpParameters[1]["values"]=arguments["parameterValues"]>
					<cfset VARIABLES["reportRequest"][parametersKey]=tmpParameters>
					<cfreturn TRUE>
				</cfif>
			</cfloop>
			<!--- Assertion : Add First OR Last Parameter THUS index i EQ 1 OR GT NB PARAMS --->
			<cfif (i EQ 1) OR (i GT arrayLen(tmpParameters))>
				<cfset tmpParameters[i]=structNew()>
				<cfset tmpParameters[i]["name"]=UCASE(arguments["parameterName"])>
				<cfset tmpParameters[i]["values"]=arguments["parameterValues"]>
				<cfset tmpParameters[i]["multiValuesAllowed"]=(arrayLen(arguments["parameterValues"]) GT 1)>
				<cfset VARIABLES["reportRequest"][parametersKey]=tmpParameters>
				<cfreturn TRUE>
			<cfelse>
				<cftrace category="IBIS" type="error" text="addParameter() - Assertion Failed : index i is #i# and parameter number is #arrayLen(tmpParameters)#">
				<cfreturn FALSE>
			</cfif>
			<cfcatch type="any">
				<cflog type="error" text="#CFCATCH.Message# || #CFCATCH.Detail#">
				<cftrace category="IBIS" type="error" text="addParameter() - Exception">
				<cfreturn FALSE>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getReportRequest" returntype="struct" hint="Returns ReportRequest">
		<cfreturn VARIABLES["reportRequest"]>
	</cffunction>
	
	<cffunction access="public" name="getScheduleRequest" returntype="struct" hint="Returns ScheduleRequest">
		<cfreturn VARIABLES["scheduleRequest"]>
	</cffunction>
	
	<cffunction access="public" name="getDeliveryRequest" returntype="struct" hint="Returns DeliveryRequest">
		<cfreturn VARIABLES["deliveryRequest"]>
	</cffunction>
	
	<cffunction access="public" name="getOutputType" returntype="string" hint="Returns Output properties TYPE. Default is UNKNOWN">
		<cfreturn VARIABLES["OUTPUT_TYPE"]>
	</cffunction>
	
	<cffunction access="public" name="getDataType" returntype="string" hint="Returns DATA properties TYPE (Last determined). Default is UNKNOWN">
		<cfreturn VARIABLES["DATA_TYPE"]>
	</cffunction>
	
	<cffunction access="public" name="setReportProperties" returntype="boolean" hint="Sets report properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Report properties">
		<cfset var isValid=VARIABLES["keyListValidator"].validateStructFromKeyList(
								arguments["properties"],THIS["REPORT_KEYS_REQ"],THIS["REPORT_KEYS_OPT"])>
		<cfif isValid EQ FALSE>
			<cftrace category="IBIS" type="error" text="setReportProperties() - Properties validation : #isValid#">
		</cfif>
		<cfreturn isValid>
	</cffunction>

	<cffunction access="public" name="setDataProperties" returntype="boolean" hint="Sets data properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Data properties">
		<!--- DATA TYPE CHECK IS PERFORMED FOR ALL AVAILABLE TYPES BEFORE RETURNING VALUE --->
		<cfset var isValid=FALSE>
		<cfset var outputProperties=arguments["properties"]>
		<cfset var keyListTypes=THIS["dynamicFile"] & "," & THIS["dynamicJdbc"] & "," & THIS["base64Data"] & "," & THIS["fileNameData"] & "," & THIS["xmlData"]>
		<cfset var determinedTypeList="">
		<cfset var searchKeyList="">
		<!--- (LOOP OVER DATA KEYLIST TYPES) : Determine type of DATA properties --->
		<cfloop index="keyListType" list="#keyListTypes#">
			<!--- Search key list includes both REQUIRED and OPTIONAL keys --->
			<cfset searchKeyList=THIS[keyListType & THIS["REQ"]] & "," & THIS[keyListType & THIS["OPT"]]>
			<!--- TYPE IS DETERMINED --->
			<cfif VARIABLES["keyListValidator"].mapContains(outputProperties,searchKeyList) EQ TRUE>
				<cfset determinedTypeList=listAppend(determinedTypeList,keyListType)>
				<!--- SET OUTPUT TYPE --->
				<cfset VARIABLES["DATA_TYPE"]=keyListType>
				<!--- Perform OUTPUT properties validation : {REQUIRED} and {OPTIONAL} --->
				<cfset isValid=VARIABLES["keyListValidator"].validateStructFromKeyList(outputProperties,
												THIS[keyListType & THIS["REQ"]],THIS[keyListType & THIS["OPT"]])>
			</cfif>
		</cfloop>
		<!--- TODO : Handle case with MULTIPLE output type --->
		<cfif listLen(determinedTypeList) GT 1>
			<cftrace category="IBIS" type="warning"
						text="setDataProperties() - OUTPUT TYPE WAS DETERMINED MORE THAN ONE - TYPE LIST [#determinedTypeList#]">
		</cfif>
		<cfif isValid EQ FALSE>
			<cftrace category="IBIS" type="error"
				text="setDataProperties() - Properties validation #isValid# - OUTPUT TYPE IS #getDataType()#">
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setOutputProperties" returntype="boolean" hint="Sets output properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Output properties">
		<!--- OUTPUT TYPE CHECK IS PERFORMED FOR ALL AVAILABLE TYPES BEFORE RETURNING VALUE --->
		<cfset var isValid=FALSE>
		<cfset var outputProperties=arguments["properties"]>
		<cfset var keyListTypes=THIS["runLocalType"] & "," & THIS["localType"] & "," & THIS["ftpType"] & "," & THIS["emailType"]>
		<cfset var determinedTypeList="">
		<cfset var searchKeyList="">
		<!--- (LOOP OVER OUTPUT KEYLIST TYPES) : Determine type of OUTPUT properties --->
		<cfloop index="keyListType" list="#keyListTypes#">
			<!--- Search key list includes both REQUIRED and OPTIONAL keys --->
			<cfset searchKeyList=THIS[keyListType & THIS["REQ"]] & "," & THIS[keyListType & THIS["OPT"]]>
			<!--- TYPE IS DETERMINED --->
			<cfif VARIABLES["keyListValidator"].mapContains(outputProperties,searchKeyList) EQ TRUE>
				<cfset determinedTypeList=listAppend(determinedTypeList,keyListType)>
				<!--- TODO : Handle case with MULTIPLE output type --->
				<!--- SET OUTPUT TYPE --->
				<cfset VARIABLES["OUTPUT_TYPE"]=keyListType>
				<!--- Perform OUTPUT properties validation : {REQUIRED} and {OPTIONAL,GENERAL OUTPUT KEYLIST} --->
				<cfset isValid=VARIABLES["keyListValidator"].validateStructFromKeyList(outputProperties,
						THIS[keyListType & THIS["REQ"]],THIS[keyListType & THIS["OPT"]] & "," & THIS["OUTPUT_KEYS_OPT"])>
			</cfif>
		</cfloop>
		<!--- IF MULTIPLE TYPES WAS DETERMINED --->
		<cfif listLen(determinedTypeList) GT 1>
			<cftrace category="IBIS" type="warning"
						text="setOutputProperties() - OUTPUT TYPE WAS DETERMINED MORE THAN ONE - TYPE LIST [#determinedTypeList#]">
		</cfif>
		<cfif isValid EQ FALSE>
			<cftrace category="IBIS" type="error"
				text="setOutputProperties() - Properties validation #isValid# - OUTPUT TYPE IS #getOutputType()#">
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setExecutionProperties" returntype="boolean" hint="Sets report execution properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Execution properties">
		<cfset var isValid=VARIABLES["keyListValidator"].validateStructFromKeyList(arguments["properties"],"",THIS["EXECUTION_KEYS_OPT"])>
		<cfif isValid EQ FALSE>
			<cftrace category="IBIS" type="error" text="setExecutionProperties() - Properties validation : #isValid#">
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setNotificationProperties" returntype="boolean" hint="Sets Notification Properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Notification properties">
		<cfset var isValid=VARIABLES["keyListValidator"].validateStructFromKeyList(arguments["properties"],"",THIS["NOTIFICATION_KEYS_OPT"])>
		<cfif isValid EQ FALSE>
			<cftrace category="IBIS" type="error" text="setNotificationProperties() - Properties validation : #isValid#">
		</cfif>
		<cfreturn isValid>
	</cffunction>
	
	<cffunction access="public" name="setHistoryProperties" returntype="boolean" hint="Sets data properties. TRUE if success FALSE otherwise">
		<cfargument name="properties" type="struct" required="true" hint="Data properties">
		<cfset var isValid=VARIABLES["keyListValidator"].validateStructFromKeyList(arguments["properties"],"",THIS["HISTORY_KEYS_OPT"])>
		<cfif isValid EQ FALSE>
			<cftrace category="IBIS" type="error" text="setHistoryProperties() - Properties validation : #isValid#">
		</cfif>
		<cfreturn isValid>
	</cffunction>
</cfcomponent>