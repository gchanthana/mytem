package gestionparc.ihm.renderer.entityrenderer
{
	


	public class ModelelItemRenderer extends EntityItemRenderer
	{
		public function ModelelItemRenderer():void
		{
			super();
		}

		override public function set data(value:Object):void
		{
			lbLibelle.text="";

			if (value != null)
			{
				super.data=value;

				lbLibelle.text=value.T_MODELE;
				this.img.visible=false;
				lbLibelle.useHandCursor=false;
				lbLibelle.buttonMode=false;
				lbLibelle.setStyle("textDecoration", "none");
			}
		}
	}
}
