<cfcomponent output="false" alias="fr.consotel.consoview.M16.Commande">
	
	<!--- cfproperty  --->

	<cfproperty name="IS_CONCERN" 				type="numeric" default="0">
	<cfproperty name="IDCOMMANDE" 				type="numeric" default="0">
	<cfproperty name="IDCONTACT" 				type="numeric" default="0">
	<cfproperty name="IDREVENDEUR" 				type="numeric" default="0">
	<cfproperty name="IDROLE" 					type="numeric" default="0">
	<cfproperty name="IDOPERATEUR" 				type="numeric" default="0">
	<cfproperty name="IDTYPE_COMMANDE" 			type="numeric" default="0">
	<cfproperty name="IDSOCIETE" 				type="numeric" default="0">
	<cfproperty name="IDCOMPTE_FACTURATION" 	type="numeric" default="0">
	<cfproperty name="IDSOUS_COMPTE" 			type="numeric" default="0">
	<cfproperty name="IDRACINE" 				type="numeric" default="0">
	<cfproperty name="IDLAST_ETAT" 				type="numeric" default="0">
	<cfproperty name="IDLAST_ACTION" 			type="numeric" default="0">
	<cfproperty name="IDSITELIVRAISON" 			type="numeric" default="0">
	<cfproperty name="IDTRANSPORTEUR" 			type="numeric" default="0">
	<cfproperty name="IDPOOL_GESTIONNAIRE" 		type="numeric" default="0">
	<cfproperty name="IDGESTIONNAIRE" 			type="numeric" default="0">
	<cfproperty name="IDPROFIL" 				type="numeric" default="0">
	<cfproperty name="IDINV_ETAT" 				type="numeric" default="0">
	<cfproperty name="IDSITEFACTURATION" 		type="numeric" default="0">
	<cfproperty name="IDEQUIPEMENTPARENT" 		type="numeric" default="0">
	<cfproperty name="IDACTEPOUR" 				type="numeric" default="0">
	<cfproperty name="IDPROFIL_EQUIPEMENT" 		type="numeric" default="0">
	<cfproperty name="BOOL_ENVOYER_VIAMAIL" 	type="numeric" default="0">
	<cfproperty name="MONTANT" 					type="numeric" default="0">
	<cfproperty name="MONTANT_CDE" 				type="numeric" default="0">
	<cfproperty name="SEGMENT_FIXE" 			type="numeric" default="0">
	<cfproperty name="USERID"  					type="numeric" default="0">
	<cfproperty name="SEGMENT_DATA" 			type="numeric" default="0">
	<cfproperty name="IDGESTIONNAIRE_MODIF" 	type="numeric" default="0">
	<cfproperty name="IDGESTIONNAIRE_CREATE" 	type="numeric" default="0">
	<cfproperty name="IDGROUPE_REPERE" 			type="numeric" default="0">
	<cfproperty name="BOOL_DEVIS" 				type="numeric" default="0">
	<cfproperty name="ID_MAIL" 					type="numeric" default="0">
	<cfproperty name="SEGMENT_MOBILE" 			type="numeric" default="0">
	<cfproperty name="LIVRAISON_DISTRIBUTEUR" 	type="numeric" default="0">
	<cfproperty name="ENCOURS" 					type="numeric" default="0">
	<cfproperty name="CONFIG_NUMBER" 			type="numeric" default="0">
	<cfproperty name="NBRECORD" 				type="numeric" default="0">
	<cfproperty name="NBROWS" 					type="numeric" default="0">
	<cfproperty name="NB_ROWS" 					type="numeric" default="0">
	<cfproperty name="USERCREATE" 				type="string" default="">
	<cfproperty name="DATE_CREATED" 			type="string" default="">
	<cfproperty name="DATE_LIV" 				type="string" default="">
	<cfproperty name="NUMERO_TRACKING" 			type="string" default="">
	<cfproperty name="NUMERO_COMMANDE" 			type="string" default="">
	<cfproperty name="REF_OPERATEUR" 			type="string" default="">
	<cfproperty name="REF_CLIENT1" 				type="string" default="">
	<cfproperty name="REF_CLIENT2" 				type="string" default="">
	<cfproperty name="REF_CLIENT11" 			type="string" default="">
	<cfproperty name="REF_CLIENT21" 			type="string" default="">
	<cfproperty name="LIBELLE_TO" 				type="string" default="">
	<cfproperty name="LIBELLE_PROFIL" 			type="string" default="">
	<cfproperty name="LIBELLE_COMMANDE"	 		type="string" default="">
	<cfproperty name="LIBELLE_POOL" 			type="string" default="">
	<cfproperty name="LIBELLE_REVENDEUR"		type="string" default="">
	<cfproperty name="LIBELLE_OPERATEUR" 		type="string" default="">
	<cfproperty name="LIBELLE_COMPTE" 			type="string" default="">
	<cfproperty name="LIBELLE_SOUSCOMPTE" 		type="string" default="">
	<cfproperty name="LIBELLE_TRANSPORTEUR" 	type="string" default="">
	<cfproperty name="LIBELLE_SITELIVRAISON" 	type="string" default="">
	<cfproperty name="LIBELLE_LASTETAT" 		type="string" default="">
	<cfproperty name="LIBELLE_LASTACTION" 		type="string" default="">
	<cfproperty name="COMMENTAIRES" 			type="string" default="">
	<cfproperty name="LIBELLE_SITEFACTURATION" 	type="string" default="">
	<cfproperty name="LIBELLE_CONFIGURATION" 	type="string" default="">
	<cfproperty name="DATE_HEURE" 				type="string" default="">
	<cfproperty name="TYPE_OPERATION" 			type="string" default="">
	<cfproperty name="PATRONYME_CONTACT" 		type="string" default="">
	<cfproperty name="CREEE_PAR" 				type="string" default="">
	<cfproperty name="MODIFIEE_PAR" 			type="string" default="">
	<cfproperty name="EMAIL_CONTACT" 			type="string" default="">
	<cfproperty name="ACTION" 					type="string" default="">
	<cfproperty name="V1" 						type="string" default="">
	<cfproperty name="V2" 						type="string" default="">
	<cfproperty name="V3" 						type="string" default="">
	<cfproperty name="V4" 						type="string" default="">
	<cfproperty name="V5"						type="string" default="">
	<cfproperty name="ENGAGEMENT" 				type="string" default="">
	<cfproperty name="BOOL_MAIL" 				type="boolean" default="1">
	<cfproperty name="SHAREMODELEALLPOOLS" 		type="boolean" default="1">
	<cfproperty name="IS_LIV_DISTRIB" 			type="boolean" default="1">	
	<cfproperty name="SELECTED" 				type="boolean" default="1">
	<cfproperty name="NEWCONFIG" 				type="boolean" default="1">
	<cfproperty name="CREEE_LE" 				type="any" default="">
	<cfproperty name="MODIFIEE_LE" 				type="any" default="">
	<cfproperty name="ENVOYER_LE" 				type="any" default="">
	<cfproperty name="LIVREE_LE" 				type="any" default="">
	<cfproperty name="LIVRAISON_PREVUE_LE" 		type="any" default="">
	<cfproperty name="DATE_COMMANDE" 			type="any" default="">
	<cfproperty name="EXPEDIE_LE" 				type="any" default="">
	<cfproperty name="DATE_CREATE" 				type="any" default="">
	<cfproperty name="COMMANDE" 				type="array" default="">
	<cfproperty name="COMMANDE_TEMPORAIRE" 		type="array" default="">
	<cfproperty name="CONFIGURATION_TEMPORAIRE" type="array" default="">
	<cfproperty name="IMPORTMASSE_COLLAB"		type="array" default="">
	<cfproperty name="ARTICLES" 				type="XML" default="">
	<cfproperty name="PRICE_EQUIPEMENT" 		type="struct" default="">

	<!--- Initialize the CFC with the default properties values. --->
	
	<cfscript>
		this.IS_CONCERN 			= 0;
		this.IDCOMMANDE 			= 0;
		this.IDCONTACT 				= 0;
		this.IDREVENDEUR 			= 0;
		this.IDROLE 				= 0;
		this.IDOPERATEUR 			= 0;
		this.IDTYPE_COMMANDE 		= 0;
		this.IDSOCIETE 				= 0;
		this.IDCOMPTE_FACTURATION 	= 0;
		this.IDSOUS_COMPTE 			= 0;
		this.IDRACINE 				= 0;
		this.IDLAST_ETAT 			= 0;
		this.IDLAST_ACTION 			= 0;
		this.IDSITELIVRAISON 		= 0;
		this.IDTRANSPORTEUR 		= 0;
		this.IDPOOL_GESTIONNAIRE 	= 0;
		this.IDGESTIONNAIRE 		= 0;
		this.IDPROFIL 				= 0;
		this.IDINV_ETAT 			= 0;
		this.IDSITEFACTURATION 		= 0;
		this.IDEQUIPEMENTPARENT 	= 0;
		this.IDACTEPOUR 			= 0;
		this.IDPROFIL_EQUIPEMENT 	= 0;
		this.BOOL_ENVOYER_VIAMAIL 	= 0;
		this.MONTANT 				= 0;
		this.MONTANT_CDE 			= 0;
		this.SEGMENT_FIXE 			= 0;
		this.USERID  				= 0;
		this.SEGMENT_DATA 			= 0;
		this.IDGESTIONNAIRE_MODIF 	= 0;
		this.IDGESTIONNAIRE_CREATE = 0;
		this.IDGROUPE_REPERE 		= 0;
		this.BOOL_DEVIS 			= 0;
		this.ID_MAIL 				= 0;
		this.SEGMENT_MOBILE 		= 0;
		this.LIVRAISON_DISTRIBUTEUR = 0;
		this.ENCOURS 				= 0;
		this.CONFIG_NUMBER 			= 0;
		this.NBRECORD 				= 0;
		this.NBROWS 				= 0;
		this.NB_ROWS 				= 0;
		this.USERCREATE 				= "";
		this.DATE_CREATED 				= "";
		this.DATE_LIV 					= "";
		this.NUMERO_TRACKING 			= "";
		this.NUMERO_COMMANDE 			= "";
		this.REF_OPERATEUR 			= "";
		this.REF_CLIENT1 				= "";
		this.REF_CLIENT2 				= "";
		this.REF_CLIENT11 				= "";
		this.REF_CLIENT21 				= "";
		this.LIBELLE_TO 				= "";
		this.LIBELLE_PROFIL 			= "";
		this.LIBELLE_COMMANDE	 		= "";
		this.LIBELLE_POOL 				= "";
		this.LIBELLE_REVENDEUR			= "";
		this.LIBELLE_OPERATEUR 		= "";
		this.LIBELLE_COMPTE 			= "";
		this.LIBELLE_SOUSCOMPTE 		= "";
		this.LIBELLE_TRANSPORTEUR 		= "";
		this.LIBELLE_SITELIVRAISON 	= "";
		this.LIBELLE_LASTETAT 			= "";
		this.LIBELLE_LASTACTION 		= "";
		this.COMMENTAIRES 				= "";
		this.LIBELLE_SITEFACTURATION 	= "";
		this.LIBELLE_CONFIGURATION 	= "";
		this.DATE_HEURE 				= "";
		this.TYPE_OPERATION 			= "";
		this.PATRONYME_CONTACT 		= "";
		this.CREEE_PAR 				= "";
		this.MODIFIEE_PAR 				= "";
		this.EMAIL_CONTACT 			= "";
		this.ACTION 					= "";
		this.V1 						= "";
		this.V2 						= "";
		this.V3 						= "";
		this.V4 						= "";
		this.V5							= "";
		this.ENGAGEMENT 				= "";
		this.BOOL_MAIL 					= 0;
		this.SHAREMODELEALLPOOLS 		= 0;
		this.IS_LIV_DISTRIB 			= 0;	
		this.SELECTED 					= 0;
		this.NEWCONFIG 					= 0;
		this.CREEE_LE 					= "";
		this.MODIFIEE_LE 				= "";
		this.ENVOYER_LE 				= "";
		this.LIVREE_LE 					= "";
		this.LIVRAISON_PREVUE_LE 		= "";
		this.DATE_COMMANDE 				= "";
		this.EXPEDIE_LE 				= "";
		this.DATE_CREATE 				= "";
		this.COMMANDE 					= "";
		this.COMMANDE_TEMPORAIRE 		= "";
		this.CONFIGURATION_TEMPORAIRE 	= "";
		this.IMPORTMASSE_COLLAB			= "";
		this.ARTICLES 					= "";
		this.PRICE_EQUIPEMENT 			= "";
	</cfscript>



<!--- 
<cfscript>
		
		this.IDCOMMANDE = 0;
		this.IDCONTACT = 0;
		this.IDREVENDEUR = 0;
		this.IDROLE = 0;
		this.IDOPERATEUR = 0;
		this.IDTYPE_COMMANDE = 0;
		this.IDSOCIETE = 0;
		this.IDCOMPTE_FACTURATION = 0;
		this.IDSOUS_COMPTE = 0;
		this.IDRACINE = 0;
		this.IDLAST_ETAT = 0;
		this.IDLAST_ACTION = 0;
		this.IDSITELIVRAISON = 0;
		this.IDTRANSPORTEUR = 0;
		this.IDPOOL_GESTIONNAIRE = 0;
		this.IDGESTIONNAIRE = 0;
		this.IDPROFIL = 0;
		this.IDINV_ETAT = 0;
		this.BOOL_MAIL = 1;
		this.ID_MAIL = 0;
		this.IDSITEFACTURATION = 0;
		this.IDEQUIPEMENTPARENT = 0;
		this.IDACTEPOUR = 0;
		this.IDPROFIL_EQUIPEMENT = 0;
		this.USERCREATE = "";
		this.USERID = 0;
		this.NUMERO_COMMANDE = "";
		this.REF_OPERATEUR = "";
		this.REF_CLIENT1 = "";
		this.REF_CLIENT2 = "";
		this.REF_CLIENT11 = "";
		this.REF_CLIENT21 = "";
		this.LIBELLE_TO = "";
		this.LIBELLE_PROFIL = "";
		this.LIBELLE_COMMANDE = "";
		this.LIBELLE_POOL = "";
		this.LIBELLE_REVENDEUR = "";
		this.LIBELLE_OPERATEUR = "";
		this.LIBELLE_COMPTE = "";
		this.LIBELLE_SOUSCOMPTE = "";
		this.LIBELLE_TRANSPORTEUR = "";
		this.LIBELLE_SITELIVRAISON = "";
		this.LIBELLE_LASTETAT = "";
		this.LIBELLE_LASTACTION = "";
		this.COMMENTAIRES = "";
		this.LIBELLE_SITEFACTURATION = "";
		this.LIBELLE_CONFIGURATION = "";
		this.SHAREMODELEALLPOOLS = 1;
		this.PRICE_EQUIPEMENT = "";
		this.DATE_HEURE = "";
		this.TYPE_OPERATION = "";
		this.PATRONYME_CONTACT = "";
		this.CREEE_PAR = "";
		this.MODIFIEE_PAR = "";
		this.BOOL_ENVOYER_VIAMAIL = 0;
		this.CREEE_LE = "";
		this.MODIFIEE_LE = "";
		this.ENVOYER_LE = "";
		this.LIVREE_LE = "";
		this.LIVRAISON_PREVUE_LE = "";
		this.DATE_COMMANDE = "";
		this.EXPEDIE_LE = "";
		this.DATE_CREATED = "";
		this.DATE_LIV = "";
		this.BOOL_DEVIS = 0;
		this.NUMERO_TRACKING = "";
		this.MONTANT = 0;
		this.MONTANT_CDE = 0;
		this.SEGMENT_FIXE = 0;
		this.SEGMENT_MOBILE = 0;
		this.IDGESTIONNAIRE_MODIF = 0;
		this.IDGESTIONNAIRE_CREATE = 0;
		this.IDGROUPE_REPERE = 0;
		this.ENCOURS = 0;
		this.EMAIL_CONTACT = "";
		this.DATE_CREATE = "";
		this.ENGAGEMENT = "";
		this.CONFIG_NUMBER = 0;
		this.ACTION = "";
		this.ARTICLES = "";
		this.COMMANDE = "";
		this.COMMANDE_TEMPORAIRE = "";
		this.CONFIGURATION_TEMPORAIRE = "";
		this.IMPORTMASSE_COLLAB = "";
		this.SELECTED = 1;
		this.NEWCONFIG = 1;
		this.V1 = "";
		this.V2 = "";
		this.V3 = "";
		this.V4 = "";
		this.V5 = "";
	</cfscript>
 --->

	<cffunction name="init" output="false" returntype="CommandeVO">
		<cfreturn this>
	</cffunction>


</cfcomponent>