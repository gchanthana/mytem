	<!--- URL VARIABLE --->
		
	<cfset uuid 		= ''>
	<cfset cle			= ''>
	
	<cfset ipAdresse 	= cgi.REMOTE_ADDR>
	<cfset redirection  = ''>
	<cfset _idcampagne  = 0>
	<cfset idlogCamp	= 0>
	
	<!--- PROCEDURES VARIABLE --->
	
	<cfset infosEnrollmentResult = structNew()>
	<cfset currentInfos 		 = structNew()>
	
	<!--- satus --->
	
	<cfset statusUrl 	= 0>
	<cfset isExist		= 0>
	<cfset statusSMS 	= 0>
	<cfset isManage		= 0>
	<cfset idOs			= 0>
	<cfset libOs		= ''>
	
	<!--- TYPE D'OS --->
	
	<cfset deviceIphone 	= "iphone">			<!--- idOs = 1 --->
	<cfset deviceIpod 		= "ipod">			<!--- idOs = 2 --->
	<cfset devicePalm 		= "palm">			<!--- idOs = 3 --->
	<cfset deviceS60 		= "series60">		<!--- idOs = 4 --->
	<cfset deviceSymbian 	= "symbian">		<!--- idOs = 5 --->
	<cfset engineWebKit 	= "webkit">			<!--- idOs = 6 --->
	<cfset deviceAndroid	= "android">		<!--- idOs = 7 --->
	<cfset deviceWinMob 	= "windows ce">		<!--- idOs = 8 --->
	<cfset deviceWinPhone 	= "windows phone">	<!--- idOs = 9 --->
	<cfset deviceBB 		= "blackberry">		<!--- idOs = 10 --->

	<!--- Numero de ligne --->
	<cfset ligne="">
	
	<cfif structKeyExists(URL, "uuid") AND structKeyExists(URL, "cle")>
	
		<cfif URL.uuid NEQ '' AND URL.cle NEQ ''>
			
			<cfset statusUrl = 1>
			<cfset uuid 	 = URL.uuid>
			<cfset cle		 = URL.cle>
			
			<cfset infosEnrollmentResult = createObject("component", "fr.consotel.consoview.M111.MDMService").getEnrollDevice(uuid, cle, 'ROCOFFRE')>
		
			<cfif structKeyExists(infosEnrollmentResult, "ETAT") AND structKeyExists(infosEnrollmentResult, "INFOS")>
			 	<!--- Conditions de l'implémentation existente
				<cfif infosEnrollmentResult.ETAT GT 0 AND infosEnrollmentResult.INFOS.RecordCount GT 0>
				--->
				<cfif infosEnrollmentResult.INFOS.RecordCount GT 0>
					<cfset isExist = 1>
					<cfset currentInfos=infosEnrollmentResult.INFOS>
					<!--- Données de l'implémentation existente --->
					<cfset _idcampagne=VAL(currentInfos['IDMDM_CAMPAGNE'][currentInfos.RecordCount])>
					<cfset idlogCamp=currentInfos['IDMDM_LOG_CAMPAGNE'][currentInfos.RecordCount]>
					<!--- Infos de la racine --->
					<cfset infosRacine=structNew()>
					<cfset infosRacine.idracine=currentInfos['IDRACINE'][currentInfos.RecordCount]>
					<cfset infosRacine.dtasource="ROCOFFRE">
					<cfset infosMDM=createObject("component", "fr.consotel.api.mdm.CodeFunction").get_ListeCodeFunction_ServeurMDM(infosRacine)>
					<!--- Nouvelle implémentation : Paramètres utilisés pour cette page d'enrollment --->
					<cfset idGroupe=VAL(infosRacine.idracine)>
					<cfset datasource=infosRacine.dtasource>
					<!--- Pour les tests : [FACTICE] Infos enregistrées et utilisées pour cette page d'enrollment
					<cfset myt58=createObject("component","fr.saaswedo.api.tests.MYT58")>
					<cfset enrollParameters=myt58.requestEnrollParameters(datasource,uuid)>
					<cfset serialNumber=enrollParameters["serialNumber"]>
					<cfset imei=enrollParameters["imei"]>
					<cfset ligne=enrollParameters["soustete"]>
					--->
					<cfset enrollParameters=currentInfos>
					<cfset serialNumber=enrollParameters["SERIALNUMBER"][1]>
					<cfset imei=enrollParameters["IMEI"][1]>
					<cfset ligne=TRIM(enrollParameters["SOUS_TETE"][1])>
					<cfset codeApp=VAL(enrollParameters["CODEAPP"][1])>
					
					<!--- API MDM MyTEM : Utilisée pour déterminer le statut du device dans le référentiel MDM --->
					<cfset myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance(idGroupe)>
					<cfset iDeviceInfo=myTEM.getIDeviceInfo()>
					<!--- Conditions de l'implémentation existente
					<cfif currentInfos['IDMDM_STATUT'][currentInfos.RecordCount] NEQ 30 AND currentInfos['IDMDM_STATUT'][currentInfos.RecordCount] GT 0>
					--->
					<!--- Si le device n'est pas enrollé : Détection du type de l'OS --->
					<cfset managedStatus=iDeviceInfo.getManagedStatus(serialNumber,imei)>
					<cflog type="information" text="[Page de detection MDM] Device [#serialNumber#,#imei#] status : #managedStatus#">
					<cfif managedStatus EQ "UNMANAGED">
						<cfif findNoCase(deviceIphone, cgi.http_user_agent, 1)>
							<cfoutput>iPhone</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 1>
							<cfset libOs		= 'iOS'>
						<cfelseif findNoCase(deviceIpod, cgi.http_user_agent, 1)>
							<cfoutput>iPod</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 2>
							<cfset libOs		= 'iOS'>
						<cfelseif findNoCase(devicePalm, cgi.http_user_agent, 1)>
							<cfoutput>Palm - Non géré</cfoutput>
							<cfset idOs			= 3>
							<cfset libOs		= 'Palm'>
						<cfelseif findNoCase(deviceS60, cgi.http_user_agent, 1)>
							<cfoutput>Symbian</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 4>
							<cfset libOs		= 'Symbian'>
							<cfset redirection 	= "http" & (infosMDM["useSSL"] ? "s":"") & "://" & infosMDM.servermdm & "/zdm/zenprise.sis">
						<cfelseif findNoCase(deviceSymbian, cgi.http_user_agent, 1)>
							<cfoutput>Symbian</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 5>
							<cfset libOs		= 'Symbian'>
							<cfset redirection 	= "http" & (infosMDM["useSSL"] ? "s":"") & "://" & infosMDM.servermdm & "/zdm/zenprise.sis">
						<cfelseif findNoCase(engineWebKit, cgi.http_user_agent, 1)>
							<cfoutput>Android</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 6>
							<cfset libOs		= 'Android'>
							<!--- URL d'origine
							<cfset redirection 	= "https://" & infosMDM.servermdm & "/zdm/zenprise.apk">
							--->
							<!--- URL de test pour contourner le problème de téléchargement HTTPS sous ANDROID (En WIFI) --->
							<cfset redirection 	= "http" & (infosMDM["useSSL"] ? "s":"") & "://" & infosMDM.servermdm & "/zdm/zenprise.apk">
						<cfelseif findNoCase(deviceAndroid, cgi.http_user_agent, 1)>
							<cfoutput>Android</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 7>
							<cfset libOs		= 'Android'>
							<!--- URL d'origine
							<cfset redirection 	= "https://" & infosMDM.servermdm & "/zdm/zenprise.apk">
							--->
							<!--- URL de test pour contourner le problème de téléchargement HTTPS sous ANDROID (En WIFI) --->
							<cfset redirection 	= "http" & (infosMDM["useSSL"] ? "s":"") & "://" & infosMDM.servermdm & "/zdm/zenprise.apk">
						<cfelseif findNoCase(deviceWinMob, cgi.http_user_agent, 1)>
							<cfoutput>Windows CE</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 8>
							<cfset libOs		= 'Windows CE'>
							<cfset redirection 	= "http" & (infosMDM["useSSL"] ? "s":"") & "://" & infosMDM.servermdm & "/zdm/setup.cab">
						<cfelseif findNoCase(deviceWinPhone, cgi.http_user_agent, 1)>
							<cfoutput>Windows Phone</cfoutput>
							<cfset isManage		= 1>
							<cfset idOs			= 9>
							<cfset libOs		= 'Windows Phone'>
							<cfset redirection 	= "http" & (infosMDM["useSSL"] ? "s":"") & "://" & infosMDM.servermdm & "/zdm/setup.cab">
						<!--- Blackberry - Non géré --->
						<cfelseif findNoCase(deviceBB, cgi.http_user_agent, 1)>
							<cfoutput>Blackberry - Non géré</cfoutput>
							<cfset idOs			= 10>
							<cfset libOs		= 'Blackberry'>
						<!--- Device non compatible avec les applications proposées --->
						<cfelse>
							<cfset isManage		= -1>
							<cfset libOs		= 'Mobile non géré'>
							<cfset libOs		= ''>
						</cfif>
					<!--- Enrollment déjà effectué : Affichage d'un message d'information --->
					<cfelse>
						<cfoutput>La procédure d'enrollment de votre terminal a déjà été correctement effectuée<br><br></cfoutput>
					</cfif>
				<cfelse>
					<!--- ETAT = 0 ou INFOS.RecordCount = 0 => ERREUR --->
					<cfset isExist = -2>
				</cfif>
			<cfelse>
				<!--- ETAT ou INFOS non définis  => ERREUR --->
				<cfset isExist = -1>
			</cfif>
		<cfelse>
			<!--- uuid ou cle vides  => ERREUR --->
			<cfset statusUrl = -2>
		</cfif>
	<!--- uuid ou cle non definis => ERREUR --->
	<cfelse>
		<cfset statusUrl = -1>
	</cfif>
	
	<!--- OS du terminal supporté --->
	<cfif isManage GT 0>
		<!--- OS différent de iOS --->
		<cfif idOs GT 1>
			<!--- uuid et cle => OK, ETAT et INFOS => OK, device compatible --->
			<cfif currentInfos['IDMDM_STATUT'][currentInfos.RecordCount] EQ 20>
				<!--- le status permet un envoi de sms car mobile non enrollé --->
				<cfset myUrlWebServiceFct = createObject("component", "fr.consotel.api.sms.public.ApiSmsPublic")>
				<cfset uuidSMS = myUrlWebServiceFct.createCampagne()>
				<cfset sousTete=ligne>
				<cfset enrollDevice=createObject("component","fr.consotel.api.mdm.EnrollDevice")>
				<cfset sousTeteWithIndicatif=enrollDevice.ligneToSmsFormat(ligne)>
				<!---
				<cfset sousTete 		= currentInfos['SOUS_TETE'][currentInfos.RecordCount]>
				<cfset rsltFindNoCase 	= FindNoCase("0", sousTete)>
				<cfif rsltFindNoCase EQ 1>
					<cfset sousTeteWithIndicatif = "+33" & Mid(sousTete, rsltFindNoCase+rsltFindNoCase, Len(sousTete))>
				</cfif>
				--->
				<cfset sautDeLigne	= CHR(13) & CHR(10)>
				<cfset contentSms	= "Connexion au MDM :" & #sautDeLigne# & "Login : " & #infosMDM.servermdmuser# & #sautDeLigne# & "Mot de passe :" & #infosMDM.servermdmpwd#>
				<cfset operator		= "SFR">
				<cfset delay		= "1000">
				<cfif isDefined("uuidSMS") AND uuidSMS NEQ "">
					<cfset returnSend = myUrlWebServiceFct.sendMessageToSingleNumber(sousTeteWithIndicatif,contentSms,operator,uuidSMS)>
					<cfif returnSend EQ true>
						<cfset statusSMS = 40>
					<cfelse><!--- SMS non envoyé --->
						<cfset statusSMS = -30>
					</cfif>
				<cfelse><!--- SMS non envoyé --->
					<cfset statusSMS = -30>
				</cfif>
				<cfset enrollmentResult = createObject("component", "fr.consotel.consoview.M111.MDMService").detectionSMS('LIGNE', ipAdresse, libOs, uuid, cle, statusSMS, idlogCamp, 'ROCOFFRE', _idcampagne)>
			<!--- CAS POUR LE TEST AVEC ANDROID : uuid et cle => OK, ETAT et INFOS => OK, device compatible mais en satus erreur d'envoi de SMS --->
			<cfelseif currentInfos['IDMDM_STATUT'][currentInfos.RecordCount] EQ -30>
				<!--- le status permet un envoi de sms car mobile non enrollé --->
				<cfset myUrlWebServiceFct = createObject("component", "fr.consotel.api.sms.public.ApiSmsPublic")>
				<cfset uuidSMS = myUrlWebServiceFct.createCampagne()>
				<cfset sousTete=ligne>
				<cfset enrollDevice=createObject("component","fr.consotel.api.mdm.EnrollDevice")>
				<cfset sousTeteWithIndicatif=enrollDevice.ligneToSmsFormat(ligne)>
				<cfset sautDeLigne	= CHR(13) & CHR(10)>
				<cfset contentSms	= "Connexion au MDM :" & #sautDeLigne# & "Login : " & #infosMDM.servermdmuser# & #sautDeLigne# & "Mot de passe :" & #infosMDM.servermdmpwd#>
				<cfset operator		= "SFR">
				<cfset delay		= "1000">
				<cfif isDefined("uuidSMS") AND uuidSMS NEQ "">
					<cfset returnSend = myUrlWebServiceFct.sendMessageToSingleNumber(sousTeteWithIndicatif, contentSms, operator, uuidSMS)>
					<cfif returnSend EQ true>
						<cfset statusSMS = 40>
					<cfelse>
						<cfset statusSMS = -30>
					</cfif>
				<cfelse>
					<cfset statusSMS = -30>
				</cfif>
				<cfset statusSMS = 0>
			<cfelseif currentInfos['IDMDM_STATUT'][currentInfos.RecordCount] EQ 40>
				<!--- C'est deja enroller et c'est terminé on propose juste le téléchargement de l'appli en cliquant sur un lien --->
				<cfset isSecure=TRUE>
				<cfset serverDNS="backoffice4.consotel.fr">
				<cfset contextPath="">
				<cfif isDebugMode()>
					<cfset serverDNS=CGI.SERVER_NAME>
					<cfset contextPath="/backoffice">
					<cfset isSecure=FALSE>
				</cfif>
				
				<cfset enrollPage="/resendloginpassword.cfm">
				<cfif fileExists(expandPath(contextPath & "/resendloginpassword.cfm"))>
					<cfset enrollPage=contextPath & "/resendloginpassword.cfm">
				</cfif>
				<cfset logmdpURL = urldecode("http" & (isSecure ? "s":"") & "://" & serverDNS & enrollPage & "?" & cgi.query_string)>
				<cfoutput>
					<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
						<head profile="http://gmpg.org/xfn/11">
							<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
							<meta name="description" content="Dectection OS"/>
							<meta http-equiv="pragma" content="no-cache"/>
							<title>Dectection OS</title>
						</head>
						<body>
							<center>
								<!--- téléchargement de l'appli --->
								<a href="#redirection#">Télécharger l'application</a><br><br>
								<!--- envoi du login / mot de passe associé
								<a href="#logmdpURL#">Me renvoyer mon login / mot de passe</a><br><br>
								--->
							</center>
						</body>
					</html>
				</cfoutput>
			</cfif>
		<!--- OS : iOS --->
		<cfelse>
			<!--- C'est un iOS donc en envoi pas de SMS => maj du status--->
			<cfset statusSMS = 40>
			<!--- uuid et cle => OK, ETAT et INFOS => OK, device compatible --->
			<cfif currentInfos['IDMDM_STATUT'][currentInfos.RecordCount] EQ 20 OR currentInfos['IDMDM_STATUT'][currentInfos.RecordCount] EQ -30>
				<cfset enrollmentResult = createObject("component", "fr.consotel.consoview.M111.MDMService").detectionSMS('LIGNE', ipAdresse, libOs, uuid, cle, statusSMS, idlogCamp, 'ROCOFFRE', _idcampagne)>
			</cfif>
		</cfif>
	<!--- OS du terminal non supporté --->
	<cfelse>
		<cfif statusUrl EQ -1 OR statusUrl EQ -2>
			<cfoutput>
				Page interdite
			</cfoutput>
		<cfelseif isExist EQ -1 OR isExist EQ -2>
			<cfoutput>
				Mobile inconnu
			</cfoutput>
		<cfelseif isManage EQ -1>
			<!--- on maj le status à -40 pour dire que le terminal n'est pas compatible --->
			<cfset enrollmentResult = createObject("component", "fr.consotel.consoview.M111.MDMService").detectionSMS('LIGNE', ipAdresse, libOs, uuid, cle, -40, idlogCamp, 'ROCOFFRE', _idcampagne)>
			<cfoutput>
				Mobile non compatible
			</cfoutput>
		</cfif>
	</cfif>
	
	<!--- SMS d'enrollment envoyé pour ANDROID et/ou Appel à detectionSMS() effectué pour iOS --->
	<cfif (statusSMS GT 0) AND isDefined("myTEM")>
		<!--- iOS : Affichage et authentification sur la page d'enrollment --->
		<cfif idOs EQ 0 OR idOs EQ 1>					
			<cfset createObject("component","fr.consotel.api.mdm.EnrollDevice").displayIOSEnrollPage(
				infosMDM.servermdm,infosMDM["useSSL"],infosMDM.servermdmuser,infosMDM.servermdmpwd,idGroupe,serialNumber,imei,myTEM
			)>
			
		<!--- Autres OS : Redirection vers une page --->
		<cfelse>
			<cflocation url="#redirection#">
		</cfif>
	<!--- erreur en amont de cette page donc pas de traitement, l'affichage de l'erreur est déjà fait --->
	<cfelseif statusSMS EQ 0>
		<!--- TODO : ??? --->
	<!--- texto non envoyé donc on recharge cette page e on la gère avec le status -30 --->
	<cfelseif statusSMS LT 0>
		<!--- TODO : ??? --->
		<cfoutput>
			Votre SMS n'a pas été envoyé.
		</cfoutput>
	</cfif>