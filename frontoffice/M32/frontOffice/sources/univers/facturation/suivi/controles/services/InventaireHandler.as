package univers.facturation.suivi.controles.services
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class InventaireHandler
	{
		private var _model:InventaireModel;
		
		public function InventaireHandler(model:InventaireModel):void
		{
			_model = model;
		}
		
		public function getInventaireFactureHandler(event:ResultEvent):void
		{
			_model.updateInventaireFacture(event.result as ArrayCollection);
		}
		
		public function getInventaireNonFactureHandler(event:ResultEvent):void
		{
			_model.updateInventaireNonFacture(event.result as ArrayCollection);
		}
		
		public function getLibelleCompteHandler(event:ResultEvent):void
		{
			_model.updategetLibelleCompte(event.result as Object);
		}
		
	}
}