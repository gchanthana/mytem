/*
   Description fonctionalitées :


   Cette classe contient  :

   - Appels des procédures
   - Gestion des évènements


 */
package intranet.gestionLoginClasse
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import intranet.gestionLoginClasse.entity.StyleVO;
    import intranet.gestionLoginClasse.entity.UserVO;
    import intranet.gestionLoginClasse.event.PaginateListEvent;
    import intranet.gestionLoginClasse.event.UserAccountStateEvent;
    import intranet.gestionLoginClasse.event.UserEvent;
    import intranet.gestionLoginClasse.ihm.DuplicateUserFeaturesIHM;
    import intranet.gestionLoginClasse.ihm.DuplicateUserFeaturesImpl;
    import intranet.gestionLoginClasse.services.pagination.ListServices;
    import intranet.gestionLoginClasse.services.user.UserServices;
    import intranet.gestionLoginClasse.services.useraccountstate.UserAccountService;
    
    import mx.collections.ArrayCollection;
    import mx.collections.IViewCursor;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;
    import mx.validators.RegExpValidator;
    
    import paginatedatagrid.event.PaginateDatagridEvent;
    import paginatedatagrid.pagination.vo.ItemIntervalleVO;
    
    import searchpaginatedatagrid.ParametresRechercheVO;
    import searchpaginatedatagrid.event.SearchPaginateDatagridEvent;

    public class GestionLogin extends GestionLoginIHM
    {
        [Embed(source="../../assets/images/nav_right_green.png")]
        private var imgIconAcces:Class;
        [Embed(source="../../assets/images/Symbol_Add.png")]
        private var imgAddAcces:Class;
        //[Embed(source="../../assets/images/Symbol Delete 2.png")]    
        //private var imgSupprim : Class;
        [Embed(source="../../assets/images/folder_forbidden.png")]
        private var imgIconVerouille:Class;
		
		[Embed(source="../../assets/images/warning.png")]
		private var iconWarning:Class;
		
        private var orga_ObjCourant:GestionLogin_Organisation;
        private var utilisateur_ObjCourant:GestionLogin_Utilisateur;
        private var tabAcces:ArrayCollection = new ArrayCollection;
        private var listeRacineDesAcces:Array = new Array;
        private var modeCreation:Boolean = false;
        //Collection de groupes Maitres :		
        [Bindable] private var arrayGroupeClient:ArrayCollection = new ArrayCollection();
        //Collection de tous les logins :
        [Bindable]
        //Collection de logins :
        [Bindable] private var arrayLogin:ArrayCollection = new ArrayCollection();
        //Collection de logins :
        //Collection de style :
        [Bindable] private var arrayStyle:ArrayCollection = new ArrayCollection();
        //Collection d'accèpar personne:
        [Bindable] private var arrayAcceParPersonne:ArrayCollection = new ArrayCollection();
        [Bindable]
        //On ne peut pas abonner un user à ces noeuds car il les possède déja :
        private var arrayAccesInterdit:ArrayCollection = new ArrayCollection();
        [Bindable] private var arrayAcceParOrgas:ArrayCollection = new ArrayCollection();
        private var objUtilFunction:UtilFunction = new UtilFunction();
        // le datagrid paginee de tous les logins
        private var dglog:DataGrid;
        // les services distants
        private var listServices:ListServices;
        private var firstLoading:Boolean;
		private static var LONGUEUR_MDP:int = 7
		
		
        public function GestionLogin()
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			
        }
		
        public function initIHM(event:FlexEvent):void
        {
            /**********************************PROVISOIRE************************************************
               dgAdresseIP.dataProvider = new ArrayCollection(
               [
               {ADRESSE_IP_ID:"1",APP_LOGIN_ID:"600", ADRESSE_IP:"155.155.155.155", COMMENTAIRE:"Adresse du PC de [---] depuis ...", DATE_CREATION:"Non Implémenté",CREATEUR:"0214",MASQUE1:"255.0.0.0"},
               {ADRESSE_IP_ID:"2",APP_LOGIN_ID:"600", ADRESSE_IP:"155.155.155.156", COMMENTAIRE:"Adresse du PC de [---] depuis ...", DATE_CREATION:"Non Implémenté",CREATEUR:"0214",MASQUE1:"255.255.0.0"},
               {ADRESSE_IP_ID:"3",APP_LOGIN_ID:"600", ADRESSE_IP:"155.155.155.157", COMMENTAIRE:"Adresse du PC de [---] depuis ...", DATE_CREATION:"Non Implémenté",CREATEUR:"0214",MASQUE1:"255.2555.255.0"},
               {ADRESSE_IP_ID:"4",APP_LOGIN_ID:"600", ADRESSE_IP:"155.155.155.158", COMMENTAIRE:"Adresse du PC de [---] depuis ...", DATE_CREATION:"Non Implémenté",CREATEUR:"0214",MASQUE1:"255.255.255.255"},
               {ADRESSE_IP_ID:"5",APP_LOGIN_ID:"600", ADRESSE_IP:"155.155.155.159", COMMENTAIRE:"Adresse du PC de [---] depuis ...", DATE_CREATION:"Non Implémenté",CREATEUR:"0214",MASQUE1:"155.155.100.024"},
               ]);

             /**************************************************************************************/
            dglog = dgTousLesLogins.dgPaginate;
            ongletIP.enabled = false;
            //Filtre pour les dataGrid
            inputFiltreLogin.addEventListener(Event.CHANGE, filtreLogin);
            inputFiltreGroupeClient.addEventListener(Event.CHANGE, filtreGroupeClient);
			tiFiltreModule.addEventListener(Event.CHANGE, filtreModule);
            // inputFiltreTousLesLogins.addEventListener(Event.CHANGE, filtreTousLesLogins);
            inputFiltreAccesParLogin.addEventListener(Event.CHANGE, filtreAccesParPersonne);
            inputFiltreStyle.addEventListener(Event.CHANGE, filtreStyle);
            //Les buttons Images
            imgEnvoyerEmailLogin.addEventListener(MouseEvent.CLICK, envoyerLoginParMail);
            imgEnvoyerEmailMotDePasse.addEventListener(MouseEvent.CLICK, envoyerMDPParMail);
            //Les Datagrids		
            dglog.addEventListener(Event.CHANGE, updateDgTousLesLogins);
            dgGroupeClient.addEventListener(Event.CHANGE, updateDgGroupeMaitre);
            dgAccesParPersonne.addEventListener(Event.CHANGE, updateDgAccesParPersonne);
            dgStyle.addEventListener(Event.CHANGE, updateStyle);
            dgAdresseIP.addEventListener(Event.CHANGE, updateDgAdresseIP);
            //Les arbres
            monTree.addEventListener(Event.CHANGE, updateArbreAcces);
            monTreeComplet.addEventListener(Event.CHANGE, updateArbreComplet);
            //Les buttonRadios :
            radioButtonsCycleDeVie.addEventListener(Event.CHANGE, updateCycleDeVie);
            radioButtonsGestionOrgas.addEventListener(Event.CHANGE, updateGestionOrgas);
            radioButtonsModuleBase.addEventListener(Event.CHANGE, updateModuleBase);
            radioButtonsUsages.addEventListener(Event.CHANGE, updateUsages);
            radioButtonFixData.addEventListener(Event.CHANGE, updateFixeData);
            radioButtonsMobile.addEventListener(Event.CHANGE, updateMobile);
            radioButtonsLogin.addEventListener(Event.CHANGE, updateLogin);
            //Les Bouttons
            btSupprimerUser.addEventListener(MouseEvent.CLICK, supprimerUser);
            btEnresgistrer_Nouveau.addEventListener(MouseEvent.CLICK, enregistrerInfosUser);
            btNouveauUtilisateur.addEventListener(MouseEvent.CLICK, nouveauUtilisateur);
            btNouveauAccesGrClient.addEventListener(MouseEvent.CLICK, creerAccesGroupeClient);
            btMultipleAcces.addEventListener(MouseEvent.CLICK, newPanelAccesMultiple);
            btNouveauAcces.addEventListener(MouseEvent.CLICK, creerAcces);
            btDuplicateAllAccess.addEventListener(MouseEvent.CLICK, duplicateAllAccess);
            btSupprimerAcces.addEventListener(MouseEvent.CLICK, supprimerAcces);
            btSupprimerAccesUser.addEventListener(MouseEvent.CLICK, supprimerAccesUser);
            btDuplicateAccesUser.addEventListener(MouseEvent.CLICK, duplicateAccessUserHandler);
            btGenererMDP.addEventListener(MouseEvent.CLICK, genererMDP);
            btAffecteCompte.addEventListener(MouseEvent.CLICK, affecteCompteClient);
            btAbonnement.addEventListener(MouseEvent.CLICK, newPopUpFicheCompteClient);
            btNouveauIP.addEventListener(MouseEvent.CLICK, newPopUpGestionIP);
            btSupprimerIP.addEventListener(MouseEvent.CLICK, deleteAdrIP);
            btnModifadrIP.addEventListener(MouseEvent.CLICK, modifIP);
            checkRestrictionIP.addEventListener(Event.CHANGE, updateRestrictionIP);
            checkAdmin.addEventListener(Event.CHANGE, changeDroitFournisseur);
            // DATAGRID PAGINEE
            mySearchPaginate.addEventListener(SearchPaginateDatagridEvent.SEARCH, onSearchHandler);
            dgTousLesLogins.addEventListener(PaginateDatagridEvent.PAGE_CHANGE, onIntervalleChangeHandler);
            firstLoading = true;
            servicePourColdFusion(mySearchPaginate.parametresRechercheVO, dgTousLesLogins.currentIntervalle);
            //Données de base :
            afficheGroupeMaitre();
            afficheStyle();
            //afficheLogin();
			
			mySearchPaginate.txtCle.setFocus();
        }

        private function newPopUpFicheCompteClient(evt:MouseEvent):void
        {
            if(dgGroupeClient.selectedIndex != -1)
            {
                var objPopUp:PopUpFicheCompteClient = new PopUpFicheCompteClient(dgGroupeClient.selectedItem as Object, afficheGroupeMaitre as Function);
                PopUpManager.addPopUp(objPopUp, this, true);
                PopUpManager.centerPopUp(objPopUp);
            }
        }

        private function updateRestrictionIP(evt:Event):void
        {
            if(checkRestrictionIP.selected == true)
            {
                ongletIP.enabled = true;
            }
            else
            {
                ongletIP.enabled = false;
            }
        }

        private function modifIP(evt:Event):void
        {
            if(dglog.selectedIndex != -1)
            {
                if(dgAdresseIP.selectedIndex != -1)
                {
                    var adresseIp:String = dgAdresseIP.selectedItem.ADRESSE_IP;
                    var tabIp:Array = adresseIp.split(".");
                    var unPanel:PanelAdresseIP = new PanelAdresseIP(this, dglog.selectedItem.APP_LOGINID, tabIp[0], tabIp[1], tabIp[2], tabIp[3], dgAdresseIP.
                                                                    selectedItem.MASQUE1, dgAdresseIP.selectedItem.COMMENTAIRE);
                    PopUpManager.addPopUp(unPanel, this, true);
                    PopUpManager.centerPopUp(unPanel);
                }
                else
                {
                    Alert.show("Sélectionnez une adresse IP");
                }
            }
            else
            {
                Alert.show("Sélectionnez un Login");
            }
        }

        private function newPopUpGestionIP(evt:MouseEvent):void
        {
            var unPanel:PanelAdresseIP = new PanelAdresseIP(this, dglog.selectedItem.APP_LOGINID);
            PopUpManager.addPopUp(unPanel, this, true);
            PopUpManager.centerPopUp(unPanel);
        }

        public function initGridAdresseIP():void
        {
            //initdgAdresseIPQ
            var opGetAdresseIP:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                            "getAdresseIp", processInitAdresseIP, null);
            RemoteObjectUtil.callService(opGetAdresseIP, dglog.selectedItem.APP_LOGINID);
        }

        private function processInitAdresseIP(event:ResultEvent):void
        {
            dgAdresseIP.dataProvider = event.result as ArrayCollection;
        }

        private function updateDgAdresseIP(evt:Event):void
        {
            if(dgAdresseIP.selectedIndex != -1)
            {
                btSupprimerIP.enabled = true;
            }
            else
            {
                btSupprimerIP.enabled = false;
            }
        }

        /************GESTION SUPPRESSION ADR IP*****************/ //Affichage du msg de confirmation
        private function deleteAdrIP(event:MouseEvent):void
        {
            if(dgAdresseIP.selectedIndex != -1)
            {
                Alert.show("Confirmez la suppression svp", "Confirmation", Alert.OK | Alert.CANCEL, null, confirmSuppressionIp, null, Alert.OK);
            }
            else
            {
                Alert.show("Sélectionnez une IP");
            }
        }

        //Gestion du résultat de la demande de confirmation de suppression d'une ip
        private function confirmSuppressionIp(evt_obj:Object):void
        {
            if(evt_obj.detail == Alert.OK)
            {
                var oPsupIp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                         "deleteAdresseIp", processSupIp, null);
                //Alert.show(dgTousLesLogins.selectedItem.APP_LOGINID+ " -----------" +dgAdresseIP.selectedItem.ADRESSE_IP+ " -----------" +dgAdresseIP.selectedItem.MASQUE1	);		
                //oPsupIp.addEventListener(FaultEvent.FAULT,test2);					
                RemoteObjectUtil.callService(oPsupIp, dglog.selectedItem.APP_LOGINID, dgAdresseIP.selectedItem.ADRESSE_IP, dgAdresseIP.selectedItem.MASQUE1);
            }
            else
            {
                if(evt_obj.detail == Alert.CANCEL)
                {
                    Alert.show("annulé");
                }
            }
        }

        private function processSupIp(evt:ResultEvent):void
        {
            Alert.show("Adresse IP supprimée");
            initGridAdresseIP();
        }

        //Deconnexion :
        private function dispatchLogoff(event:Event):void
        {
            dispatchEvent(new Event(Intranet.LOGOFF_EVENT));
        }

        //Afficher les groupe clients
        private function afficheGroupeMaitre():void
        {
            var opGetGroupe:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                         "getGroupesMaitres", processInitGroupeClient, erreur);
            RemoteObjectUtil.callService(opGetGroupe, -1);
        }

        private function processInitGroupeClient(event:ResultEvent):void
        {
            if(event != null)
            {
                arrayGroupeClient = event.result as ArrayCollection;
                dgGroupeClient.dataProvider = arrayGroupeClient;
                arrayGroupeClient.filterFunction = filtreGridGroupeClient;
                arrayGroupeClient.refresh();
				
            }
            else
            {
                for(var i:int = 0; i < (dgGroupeClient.dataProvider as ArrayCollection).length; i++)
                {
                    (dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).ignore = true;
                    for(var a:int = 0; a < arrayAccesInterdit.length; a++)
                    {
                        if((dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).IDGROUPE_CLIENT == (arrayAccesInterdit.getItemAt(a) as UtilAcces).
                            getId())
                        {
                            (dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).isInterdit = true;
                            (dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).ignore = false;
                        }
                    }
                    for(var b:int = 0; b < arrayAcceParPersonne.length; b++)
                    {
                        if((dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).IDGROUPE_CLIENT == (arrayAcceParPersonne.getItemAt(b).ID))
                        {
                            (dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).isInterdit = false;
                            (dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).isAcces = true;
                            (dgGroupeClient.dataProvider as ArrayCollection).getItemAt(i).ignore = false;
                        }
                    }
                }
                (dgGroupeClient.dataProvider as ArrayCollection).refresh();
            }
        }

        private function erreur(evt:FaultEvent):void
        {
            Alert.show("Erreur de chargement : " + evt.toString());
        }

        //Afficher les styles
        private function afficheStyle():void
        {
            var opGetStyle:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                        "getStyle", processInitStyle, erreur);
            RemoteObjectUtil.callService(opGetStyle, null);
        }

        //Afficher les logins
        public function afficheLogin():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                 "getListeLogin", processInitTousLesLogins, erreur);
            RemoteObjectUtil.callService(op1, -1);
        }

        /***
         *
         *  Fonction de tri et recherche de logins en fonction
         *  des paramètres renseignés dans la zone de recherche
         *  du datagrid Paginé
         *
         * */
        public function onSearchHandler(evt:SearchPaginateDatagridEvent):void
        {
            if(dgTousLesLogins && mySearchPaginate)
            {
                servicePourColdFusion(mySearchPaginate.parametresRechercheVO);
            }
        }

        public function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
        {
            servicePourColdFusion(mySearchPaginate.parametresRechercheVO, dgTousLesLogins.currentIntervalle);
        }

        /***
         *  Récupération de la liste de tous les logins en fonction des propriétés
         *  spécifiques de recherches et/ou de paginations passées en paramètres
         *  - Appel des services pour remplir le datagrid paginée
         *  @parametresRechercheVO = le VO stockant les param de récherches
         *  @itemIntervalleVO = le VO stockant le segment paginée à récupérer
         *
         * */
        private function servicePourColdFusion(parametresRechercheVO:ParametresRechercheVO, itemIntervalleVO:ItemIntervalleVO = null):void
        {
            if(!itemIntervalleVO) // Si c'est une nouvelle recherche
            {
                dgTousLesLogins.refreshPaginateDatagrid();
                itemIntervalleVO = new ItemIntervalleVO();
                itemIntervalleVO.indexDepart = 0;
                itemIntervalleVO.tailleIntervalle = nbItemPerPage;
            }
            listServices = new ListServices();
            listServices.myHandlers.addEventListener(PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS, onListServiceResultHandler);
            listServices.myHandlers.addEventListener(PaginateListEvent.LOGIN_LIST_LOADED_ERROR, onListServiceResultHandler);
            // au premier chargement du datagrid on définit les param par défaut
            /* if (firstLoading)
               {
               parametresRechercheVO.SEARCH_TEXT = "LOGIN_NOM,";
               firstLoading = false;
             }*/
            listServices.getListeLogins(parametresRechercheVO, itemIntervalleVO, -1, firstLoading);
            firstLoading = false;
        }

        public function onListServiceResultHandler(evt:PaginateListEvent):void
        {
            switch(evt.type)
            {
                case PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS:
                    arrayTousLesLogins = listServices.myDatas.listLogin;
                    if(arrayTousLesLogins.length > 0)
                    {
                        nbTotalElement = arrayTousLesLogins.getItemAt(0).NBRECORD;
                        trace("onListServiceResultHandler nbTotalElement " + nbTotalElement);
                        // TODO à retirer si instanciation via vo (additem)
                        dgTousLesLogins.dataprovider = new ArrayCollection();
                        for(var i:int = 0; i < arrayTousLesLogins.length; i++)
                        {
                            dgTousLesLogins.dataprovider.addItem(arrayTousLesLogins.getItemAt(i));
                        }
                    }
                    else
                    {
                        nbTotalElement = 0;
                    }
                    // dgTousLesLogins.dataChangeHandler();
                    //dgTousLesLogins.dgPaginate.dataProvider = arrayTousLesLogins
                    listServices.myHandlers.removeEventListener(PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS, onListServiceResultHandler);
                    listServices.myHandlers.removeEventListener(PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS, onListServiceResultHandler);
                    break;
                case PaginateListEvent.LOGIN_LIST_LOADED_ERROR:
                    trace("GestionLogin-onResultListServicesHandler() : Erreur dans la récupérations de la liste des utilisateurs ");
                    break;
                default:
                    break;
            }
        }

        /*----------------------------------------------------------------------------------------------------
         **************************************Evenement sur les dataGrids*************************************
         -----------------------------------------------------------------------------------------------------*/ //Charge tous les accès d'un client
        /*
           Lorsque l’utilisateur sélectionne un client, on récupère tous les accès du client grâce à la procédure « getAccess ».
           Traitement du résultat de la requête dans processDgAccesParPersonne (Result)
         */
        private function updateDgTousLesLogins(event:Event):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                 "getAccess_v2", processDgAccesParPersonne, null);
            RemoteObjectUtil.callService(op1, dglog.selectedItem.APP_LOGINID, -1);
            changeProfile();
            monTreeComplet.setVisible(false);
            afficheGroupeMaitre();
            inputFiltreGroupeClient.text = null;
            tabUser.selectedIndex = 0;
            modeCreation = false;
            initGridAdresseIP();
        }

        //Evènement sur l'arbre : gestion de l'activation des bouton supprimerAcces 
        //Si l'item est un accès on active le bt supprimer
        //(Arbre de gauche)
        private function updateArbreAcces(event:Event):void
        {
            if(monTree.selectedItem.hasOwnProperty("@isAcces"))
            {
                btSupprimerAcces.enabled = true;
            }
            else
            {
                btSupprimerAcces.enabled = false;
            }
        }

        //Evenement sur l'arbre : Définir le type de noeud selectioné : acces / interdi / verouillé
        //Afficher le message correspondant
        //Gérer l'activation des bt supprimer ou ajouter
        private function updateArbreComplet(event:Event):void
        {
            mettreAjourDGloginParOrgas(monTreeComplet.selectedItem.@NID);
            var autoriseCreation:Boolean = true;
            if(monTreeComplet.selectedItem.hasOwnProperty("@isAcces"))
            {
                if(monTreeComplet.selectedItem.@isAcces == true)
                {
                    btNouveauAcces.enabled = false;
                    labMsgInfos.text = "le client possède déja cet accès"
                    autoriseCreation = false;
                }
            }
            else
            {
                if(monTreeComplet.selectedItem.hasOwnProperty("@isInterdit"))
                {
                    if(monTreeComplet.selectedItem.@isInterdit == true)
                    {
                        btNouveauAcces.enabled = false;
                        labMsgInfos.text = "Supprimez les droits inférieurs"
                        autoriseCreation = false;
                    }
                }
                if(monTreeComplet.selectedItem.hasOwnProperty("@isVerouille"))
                {
                    if(monTreeComplet.selectedItem.@isVerouille == true)
                    {
                        btNouveauAcces.enabled = false;
                        labMsgInfos.text = "Le client possède des droits supérieurs"
                        autoriseCreation = false;
                    }
                }
            }
            if(autoriseCreation == true)
            {
                if((dglog.selectedIndex != -1) && (monTreeComplet.selectedIndex != -1))
                {
                    btNouveauAcces.enabled = true;
                    labMsgInfos.text = "";
                }
                else
                {
                    btNouveauAcces.enabled = false;
                    labMsgInfos.text = "Sélectionnez les informations Client + Orga"
                }
            }
        }

        //Evènement sur le dg Accès par personne
        //Appel de la méthode processInitInfoAcces
        private function updateDgAccesParPersonne(event:Event):void
        {
           // recupInfoGroupeMaitre();
            processInitInfoAcces();
			if(dgAccesParPersonne.selectedItem.TYPE_LOGIN == 1)
			{
				rbgCV.selectedValue = 'CV3';
			}
			else
			{
				rbgCV.selectedValue = 'CV4';
			}
        }

        private function recupInfoGroupeMaitre():void
        {
            if(dgAccesParPersonne.selectedIndex != -1)
            {
                var idGroupeClient:int = dgAccesParPersonne.selectedItem.idGroupeMaitre as int;
                var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                        "getGroupesMaitres", resultGetRacineInfos, null);
                RemoteObjectUtil.callService(opData, idGroupeClient);
            }
        }

        private function resultGetRacineInfos(evt:ResultEvent):void
        {
            if(evt.result != -1)
            {
                enabledRadioBt(evt.result[0] as Object);
            }
        }

        //Evenement sur le dg groupe Maitre
        //Affichage de l'arbre (droite)
        private function updateDgGroupeMaitre(event:Event):void
        {
            if(dgGroupeClient.selectedIndex != -1)
            {
                btAbonnement.enabled = true;
                btMultipleAcces.enabled = true;
                btNouveauAccesGrClient.enabled = true;
                var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                     "getArbre", processArbreComplet, null);
                RemoteObjectUtil.callService(op1, dgGroupeClient.selectedItem.IDGROUPE_CLIENT);
                mettreAjourDGloginParOrgas(dgGroupeClient.selectedItem.IDGROUPE_CLIENT);
            }
        }

        //Affichage des accès en fonction du login selectionné
        private function mettreAjourDGloginParOrgas(idOrga:int):void
        {
            // Mise à jour du dg dg Login :	
            var opUpdateDgLogin:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                             "getLogin", processInitdgLogin, null);
            RemoteObjectUtil.callService(opUpdateDgLogin, idOrga);
        }

        //Cette méthode implémente un tableau d'accès interdits pour un client et ses accès 
        /*
           Appel de la procédure « getAccesInterdit » avec « tabAcces » en paramètre.
           La procédure "getAccesInterdit" appelle, pour chaque accès de « tabAcces » passé en paramètre, la procédure « getGroupNodes ».
           « getGroupNodes » prend en paramètre un id_Groupe_maitre et un id_groupe. Cette procédure retourne  une liste XML contenant tous les nœuds entre le groupe maitre et l’organisation passés en paramètre.
           Grâce à cette procédure, on dispose d’une liste de tous les nœuds situés au dessus des accès du client sélectionné.
           Appel de la méthode processCoherence () : traitement du résultat de la procédure "getAccesInterdit".
         */
        private function traitementCoherence():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                 "getAccesInterdit", processCoherence, null);
            //RemoteObjectUtil.callService(op1,(dgAccesParPersonne.dataProvider as ArrayCollection).source);
            RemoteObjectUtil.callService(op1, (tabAcces).source);
        }

        //Initialisation de l'onglet Accès 
        //0 : pas d'acces. 1: ecriture. 2: lecture / ecriture.
        private function initGrilleAcces():void
        {
            radioButtonsModuleBase.selectedValue = dgAccesParPersonne.selectedItem.MODULE_FACTURATION;
            radioButtonsCycleDeVie.selectedValue = dgAccesParPersonne.selectedItem.MODULE_WORKFLOW;
            radioButtonsUsages.selectedValue = dgAccesParPersonne.selectedItem.MODULE_USAGE;
            radioButtonsGestionOrgas.selectedValue = dgAccesParPersonne.selectedItem.MODULE_GESTION_ORG;
            radioButtonFixData.selectedValue = dgAccesParPersonne.selectedItem.MODULE_FIXE_DATA;
            radioButtonsMobile.selectedValue = dgAccesParPersonne.selectedItem.MODULE_MOBILE;
            radioButtonsLogin.selectedValue = dgAccesParPersonne.selectedItem.MODULE_GESTION_LOGIN;
            if(dgAccesParPersonne.selectedItem.DROIT_GESTION_FOURNIS == 1)
            {
                checkAdmin.selected = true;
            }
            else
            {
                checkAdmin.selected = false;
            }
        /*
           AJOUTER LES NOUVEAUX ACCès : EN ATTENTE DE LA REQUETE
         */
        }

        //Initialisation de l'onglet style
        private function initStyle():void
        {
            var memoIndexStyle:int = dgAccesParPersonne.selectedItem.IDCONSO_STYLE;
            for(var a:int = 0; a < (dgStyle.dataProvider as ArrayCollection).length; a++)
            {
                if((dgStyle.dataProvider as ArrayCollection).getItemAt(a).IDCONSO_STYLE == memoIndexStyle)
                {
                    dgStyle.selectedItem = (dgStyle.dataProvider as ArrayCollection).getItemAt(a);
                }
            }
            dgStyle.setStyle("textSelectedColor", "RED");
            dgStyle.scrollToIndex(dgStyle.selectedIndex);
            dgStyle.enabled = true;
        }

        //Inisialisation de l'arbre de gauche ( Evenement déclenché par la selection d'un accès)
        private function initArbreAcces():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                 "getGroupNodes", processInitArbreAcces, null);
            RemoteObjectUtil.callService(op1, parseInt(dgAccesParPersonne.selectedItem.idGroupeMaitre), dgAccesParPersonne.selectedItem.APP_LOGINID);
        }

        /*----------------------------------------------------------------------------------------------------
         **************************************Traitement du résultat des Requettes****************************
         -----------------------------------------------------------------------------------------------------*/ //Actualiser l'interface
        private function processSupprimerUser(event:ResultEvent):void
        {
            actualise();
        }

        //Appelle de la méthode donneListeAccesInterdit sur l'objet "objUtilFunction" qui 
        //renvoie un tableau d'accès interdit sans doublon pour un client
        /*
           Appel de la méthode « donneListeAccesInterdit » sur l’objet objUtilFunction de la classe UtilFunction.
           « donneListeAccesInterdit » prend en paramètre le résultat de la requête  "getAccesInterdit" en XML.
         */
        private function processCoherence(event:ResultEvent):void
        {
            objUtilFunction.donneListeAccesInterdit(event.result as Array);
            callLater(resultTraitementCoherence);
        }

        //Implementation de la collection arrayAccesInterdit
        /*
           Cette méthode est appelée lorsque le traitement ci-dessus est terminé.
           La collection « arrayAccesInterdit » est définit et ne comporte pas de doublon.
           On peut verrouiller* certains accès en comparant chaque nœud affiché dans le dgGroupeMaitre ou dans le treeComplet avec les accès contenus dans la collection « arrayAccesInterdit »
         * verrouiller : Afficher une icône Warning ou Interdit, Désactivé le bouton de création d’accès…
         */
        private function resultTraitementCoherence():void
        {
            arrayAccesInterdit = objUtilFunction.result();
            processInitGroupeClient(null);
        }

        //Actualiser l'interface
        private function processCreerAcces(event:ResultEvent):void
        {
            actualise();
            Alert.show("Création ok");
        }

        private function processInitdgLogin(event:ResultEvent):void
        {
            arrayLogin = event.result as ArrayCollection;
            dgLoginParOrgas.dataProvider = arrayLogin;
        }

        private function processChangeDroit(event:ResultEvent):void
        {
            //Alert
            if(event.result == -1)
            {
                Alert.show("Erreur lors de la modification des droits");
            }
            else
            {
                //Alert.show("droit changé"+ ObjectUtil.toString(event.result));
            }
        }

        private function processSetStyle(event:ResultEvent):void
        {
            trace("Style changé");
            dgAccesParPersonne.selectedItem.IDCONSO_STYLE = dgStyle.selectedItem.IDCONSO_STYLE;
        }

        /*Fonction 1 : Gestion de l'activation du bt "creer un acces au groupe client"
           Fonction 2 : Gestion de l'état des branche de l'arbre  : 	isVerouillé : enfant d'un accès
           isInterdit : Parent d'un accès
           isAcces : est accès
         */
        private function processArbreComplet(event:ResultEvent):void
        {
            btMultipleAcces.enabled = true;
            btNouveauAccesGrClient.enabled = true;
            if(ArrayCollection(dgGroupeClient.dataProvider).length > 0 && dgGroupeClient.selectedItem != 1)
            {
                if((dgGroupeClient.selectedItem.hasOwnProperty("isInterdit")) || (dgGroupeClient.selectedItem.hasOwnProperty("isAcces")))
                {
                    if((dgGroupeClient.selectedItem.isInterdit == true) || (dgGroupeClient.selectedItem.isAcces == true))
                    {
                        btNouveauAccesGrClient.enabled = false;
                    }
                }
            }
            monTreeComplet.dataProvider = event.result as XML;
            for each(var uneRacine:XML in(event.result as XML).children())
            {
                if(dgGroupeClient.selectedItem.isAcces == true)
                {
                    uneRacine.@isVerouille = true;
                    monTreeComplet.setItemIcon(uneRacine, imgIconVerouille, imgIconVerouille);
                }
                else
                {
                    uneRacine.@isVerouille = false;
                }
                for(var a:int = 0; a < arrayAccesInterdit.length; a++)
                {
                    if(uneRacine.@NID == (arrayAccesInterdit.getItemAt(a) as UtilAcces).getId())
                    {
                        uneRacine.@isInterdit = true;
                        trace("uneRacine.@isInterdit =true");
                    }
                }
                for(var b:int = 0; b < arrayAcceParPersonne.length; b++)
                {
                    if(uneRacine.@NID == (arrayAcceParPersonne.getItemAt(b).ID))
                    {
                        uneRacine.@isInterdit = false;
                        uneRacine.@isAcces = true;
                        uneRacine.@isVerouille = true;
                        trace("uneRacine.@isInterdit =false;uneRacine.@isAcces =true;uneRacine.@isVerouille = true");
                    }
                }
            }
            monTreeComplet.showRoot = false;
            monTreeComplet.dataDescriptor = new Arbre(monTreeComplet, arrayAccesInterdit, arrayAcceParPersonne);
            monTreeComplet.setVisible(true);
        }

        /*
           private function gestionIcon( obj : Object):Class
           {
           if(obj.hasOwnProperty("isInterdit")){
           if(obj.isInterdit==true){
           return imgSupprim;
           }
           }
           else
           {
           //return imgAddAcces;
           }
           return null;

           }
         */
        private function processDeleteAcces(event:ResultEvent):void
        {
            monTree.dataProvider = null;
            actualise();
            Alert.show("Suppression de l'accès : OK ");
        }

        //Appel de la méthode "rechercherLesFils" afin d'aficher une icone dans l'arbre de gauche lorsque l'item est un accès
        private function processInitArbreAcces(event:ResultEvent):void
        {
            monTree.dataProvider = event.result as XML;
            callLater(rechercherLesFils, [ monTree.dataProvider[0] as XML ]);
        }

        private function processModifUser(event:ResultEvent):void
        {
            actualise();
            Alert.show("Modification OK");
        }

        private function processCreaUser(event:ResultEvent):void
        {
            //Alert.show(event.result.toString());
            actualise();
            Alert.show("Création OK");
            modeCreation = false;
        }

        //Initialisation de l'interface lorsqu'on selectionne un accès
        private function processInitInfoAcces():void
        {
            initArbreAcces();
            initGrilleAcces();
            initStyle();
			ccp.refresh();
            boxSupModAcces.enabled = true;
            labAccesSelection.text = "" + dgAccesParPersonne.selectedItem.LIBELLE;
            tabUser.selectedIndex = 1;
        }
        //Gestion des accès aux groupes maitres : ces accès ne sont pas traités dans la classe utilFonction --> Gain en rapidité 
        //Ces accès peuvent être gerés en comparant le dgAccèsParPersonne et le dgGroupeMaitre
        //Appel de la méthode "traitementCoherence" 
        /*
           En comparant la liste des accès (résultat de la procédure)  et la liste des groupes maitres, cette méthode effectue deux actions :
           -Elle identifie les accès aux groupes Maitres
           -Elle initialise la  collection « tabAcces » avec tous les accès d’un client excepté les accès aux groupes Maitres.
           Appel de la méthode traitementCoherence()

         */
        private function processDgAccesParPersonne(event:ResultEvent):void
        {
            arrayAcceParPersonne = event.result as ArrayCollection;
            dgAccesParPersonne.dataProvider = arrayAcceParPersonne;
            //---------------Modifié pour intern.Consotel --> Tous ses accès sont des groupes maitres 
            trace("taille avant " + arrayAcceParPersonne.length);
            var existeA:Boolean;
            tabAcces.removeAll();
            for(var i:int = 0; i < arrayAcceParPersonne.length; i++)
            {
                existeA = false;
                for(var a:int = 0; a < arrayGroupeClient.length; a++)
                {
                    //trace("compare : "+arrayAcceParPersonne.getItemAt(i).idGroupeMaitre +"=="+ arrayGroupeClient.getItemAt(a).IDGROUPE_CLIENT);
                    if(arrayAcceParPersonne.getItemAt(i).ID == arrayGroupeClient.getItemAt(a).IDGROUPE_CLIENT)
                    {
                        existeA = true;
                        arrayGroupeClient.getItemAt(a).isAcces = true;
                            //trace("ok true");
                    }
                }
                if(existeA != true)
                {
                    tabAcces.addItem(arrayAcceParPersonne.getItemAt(i));
                }
            }
            trace("taille après " + tabAcces.length);
            traitementCoherence();
            dgLoginParOrgas.dataProvider = null;
        }
		
		

        private function processInitStyle(event:ResultEvent):void
        {
            arrayStyle = event.result as ArrayCollection;
            dgStyle.dataProvider = arrayStyle;
        }

        //Si variable event != null --> Initialisation de dg groupeMaitre
        //Sinon Gestion des groupes maitres "accès" ou "interdit" du client selectionné 
        private function processInitTousLesLogins(event:ResultEvent):void
        {
            arrayTousLesLogins = event.result as ArrayCollection;
            dglog.dataProvider = arrayTousLesLogins;
            callLater(selectItemdgTousLesLogins);
        }

        //Gestion MAJ  interface : Reselectionné le client en cour de modification
        private function selectItemdgTousLesLogins():void
        {
            for(var i:int = 0; i < (dglog.dataProvider as ArrayCollection).length; i++)
            {
                if((dglog.dataProvider as ArrayCollection).getItemAt(i).LOGIN_EMAIL == inputEmail.text)
                {
                    dglog.selectedItem = (dglog.dataProvider as ArrayCollection).getItemAt(i);
                    dglog.scrollToIndex(dglog.selectedIndex);
                    updateDgTousLesLogins(null);
                }
            }
        }

        /*----------------------------------------------------------------------------------------------------
         **************************************Gestion des filtres**********************************************
         -----------------------------------------------------------------------------------------------------*/ //Filtre 
        public function filtreAccesParPersonne(e:Event):void
        {
            arrayAcceParPersonne.filterFunction = filtreGridAccesParPersonne;
            arrayAcceParPersonne.refresh();
        }

		//Filtre des modules
		public function filtreModule(e:Event):void
		{
			ccp.filtrer(tiFiltreModule.text);
		}
		
        //Filtre 
        public function filtreGroupeClient(e:Event):void
        {
            arrayGroupeClient.filterFunction = filtreGridGroupeClient;
            arrayGroupeClient.refresh();
        }

        //Filtre 
        private function filtreLogin(e:Event):void
        {
            arrayLogin.filterFunction = filtreGridLogin;
            arrayLogin.refresh();
        }

        //Filtre 
        private function filtreTousLesLogins(e:Event):void
        {
            arrayTousLesLogins.filterFunction = filtreGridtousLesLogins;
            arrayTousLesLogins.refresh();
        }

        //Filtre 
        private function filtreStyle(e:Event):void
        {
            arrayStyle.filterFunction = filtreGridStyle;
            arrayStyle.refresh();
        }

        //Filtre 
        public function filtreGridStyle(item:Object):Boolean
        {
            if(((item.LIBELLE_STYLE as String).toLowerCase()).search(inputFiltreStyle.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        //Filtre : chercher la chaine entrée
        public function filtreGridtousLesLogins(item:Object):Boolean
        {
            var chaine:String = "";
            if(item.hasOwnProperty("LIBELLE_GROUPE_CLIENT") == true)
            {
                if(item.LIBELLE_GROUPE_CLIENT != null)
                {
                    chaine = item.LIBELLE_GROUPE_CLIENT;
                }
            }
            var chaineLog:String = "";
            if(item.hasOwnProperty("LOGIN_EMAIL") == true)
            {
                if(item.LOGIN_EMAIL != null)
                {
                    chaineLog = item.LOGIN_EMAIL;
                        //Alert.show("----> "+chaine);
                }
            }
            /*   if ((((chaineLog as String).toLowerCase()).search(inputFiltreTousLesLogins.text.toLowerCase()) != -1) || (((chaine).toLowerCase()).search(inputFiltreTousLesLogins.text.toLowerCase()) != -1))
               {
               return (true);
               }
               else
               {
               return (false);
             } */
            return true;
        }

        //Filtre : chercher la chaine entrée
        public function filtreGridGroupeClient(item:Object):Boolean
        {
            if(((item.LIBELLE_GROUPE_CLIENT as String).toLowerCase()).search(inputFiltreGroupeClient.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        //Filtre : chercher la chaine entrée
        public function filtreGridLogin(item:Object):Boolean
        {
            if(((item.LOGIN_EMAIL as String).toLowerCase()).search(inputFiltreLogin.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        //Filtre : chercher la chaine entrée
        public function filtreGridAccesParPersonne(item:Object):Boolean
        {
            if(((item.LIBELLE as String).toLowerCase()).search(inputFiltreAccesParLogin.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        /*----------------------------------------------------------------------------------------------------
         **************************************Mise à jour de l'interface**************************************
         -----------------------------------------------------------------------------------------------------*/ //Evenement déclenché lorsqu'on selectionne un client dans dgTousLesLogin
        //Création d'un objet utilisateur pour toujours savoir sur qui on es entrain de travailler.
        private function changeProfile():void
        {
            setUtilisateurSelection_ObjCourant(new GestionLogin_Utilisateur(dglog.selectedItem.APP_LOGINID, dglog.selectedItem.LOGIN_NOM, dglog.selectedItem.
                                                                            LOGIN_PRENOM, dglog.selectedItem.LOGIN_EMAIL, inputPwd.text = dglog.selectedItem.
                                                                            LOGIN_PWD));
            if(dglog.selectedItem.RESTRICTION_IP == 0)
            {
                checkRestrictionIP.selected = false;
            }
            else
            {
                checkRestrictionIP.selected = true;
                initIp();
            }
        }

        private function initIp():void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                 "getAdresseIp", processGetAdresseIp, null);
            RemoteObjectUtil.callService(op1, dglog.selectedItem.APP_LOGINID);
        }

        private function processGetAdresseIp(evt:ResultEvent):void
        {
            dgAdresseIP.dataProvider = evt.result as ArrayCollection;
        }

        //Affichage des infos du nouvel utilisateur 
        private function setUtilisateurSelection_ObjCourant(utilisateurGestionLogin:GestionLogin_Utilisateur):void
        {
            utilisateur_ObjCourant = utilisateurGestionLogin;
            labNom_Acces.text = utilisateur_ObjCourant.getPrenomUtilisateur() + " " + utilisateur_ObjCourant.getNomUtilisateur();
            inputNom.text = dglog.selectedItem.LOGIN_NOM;
            inputPrenom.text = dglog.selectedItem.LOGIN_PRENOM;
            inputEmail.text = dglog.selectedItem.LOGIN_EMAIL;
            inputPwd.text = dglog.selectedItem.LOGIN_PWD;
            monTree.dataProvider = null;
            dgStyle.selectedIndex = -1;
            labAccesSelection.text = "---";
            boxSupModAcces.enabled = false;
            LIBELLE.headerText = "Accès de " + dglog.selectedItem.LOGIN_PRENOM + " " + dglog.selectedItem.LOGIN_NOM;
        }

        /*------------------------------------------------------------------------------------------------------
         *******************************Update*****************************************************************
         ------------------------------------------------------------------------------------------------------*/ //Cette méthode parcour chaque branche de l'arbre (gauche) pour trouver un accès et ajouter une icone si elle le trouve
        private function rechercherLesFils(unNoeud:XML):void
        {
            //Utile que lorsque le groupe racine est un accès	
            for(var a:int = 0; a < arrayAcceParPersonne.length; a++)
            {
                if(unNoeud.@NID == arrayAcceParPersonne.getItemAt(a).ID)
                {
                    monTree.setItemIcon(unNoeud, imgIconAcces, imgIconAcces);
                    unNoeud.@isAcces = true;
                }
            }
            var xmlListDesNoeuds:XMLList = unNoeud.children();
            for(var i:int = 0; i < xmlListDesNoeuds.length(); i++)
            {
                trace("----> " + xmlListDesNoeuds[i].@STC);
                rechercheAccesInArbre(xmlListDesNoeuds[i]);
            }
        }

        //Cette méthode est appelée par "rechercherLesFils" 
        //Recursivité entre "rechercheAccesInArbre" et "rechercherLesFils"
        //Cette méthode parcour chaque branche de l'arbre (gauche) pour trouver un accès et ajouter une icone si elle le trouve
        private function rechercheAccesInArbre(node:XML):void
        {
            for each(var uneRacine:XML in node)
            {
                if(uneRacine.@NID == dgAccesParPersonne.selectedItem.ID)
                {
                    objUtilFunction.newAcces(uneRacine.@NID);
                    monTree.setItemIcon(uneRacine, imgIconAcces, imgIconAcces);
                    //ouvrir l'arbre :
                    var objPereAtester:Object = uneRacine;
                    var continuer:Boolean = true;
                    while(continuer == true)
                    {
                        if(objPereAtester != null)
                        {
                            objUtilFunction.ajouterUnPere((objPereAtester as XML).@NID);
                            trace("idpere---> " + (objPereAtester as XML).@NID);
                            monTree.expandItem((objPereAtester as XML).parent(), true, true);
                            objPereAtester = (objPereAtester as XML).parent();
                        }
                        else
                        {
                            continuer = false;
                        }
                    }
                    rechercherLesFils(node);
                }
                else
                {
                    rechercherLesFils(node);
                }
            }
        }

        //Suppression d'un accès
        private function supprimerAcces(event:Event):void
        {
			var f:Function=function(event:CloseEvent):void
			{
				if(event.detail == Alert.YES)
				{
						var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
							"deleteAcces", processDeleteAcces, null);
						RemoteObjectUtil.callService(op1, [ dgAccesParPersonne.selectedItem.APP_LOGINID, dgAccesParPersonne.selectedItem.ID ]);
				}
			}
			
			if(dgAccesParPersonne.selectedIndex != -1)
			{
				Alert.yesLabel = "Oui";
				Alert.cancelLabel = "Annuler";
				Alert.buttonWidth = 100;
				var message:String ="Vous êtes sur le point de supprimer l'acces au compte :\n" +
					dgAccesParPersonne.selectedItem.LIBELLE + "\nEtes vous sûr de vouloir effectuer cette action ?";
				
				Alert.show(message,"Attention !!",Alert.YES|Alert.CANCEL,this,f,iconWarning,Alert.CANCEL);
			}
			else
			{
				Alert.show("Aucun accès sélectionné");
			}
        }

        private function supprimerAccesUser(event:Event):void
        {
			var selectedId:int = -1;
			var libelleNoeud: String = "";
			
            if((dgLoginParOrgas.selectedIndex != -1 && monTreeComplet.selectedIndex != -1) || (dgLoginParOrgas.selectedIndex != -1 && dgGroupeClient.selectedIndex !=
                -1))
            {
               
                //Définir si l'acces est dans le dgGroupClient ou dans l'arbre  + vérif si la sélection est bien un acces	
                if(dgGroupeClient.selectedIndex != -1 
					&& !(dgGroupeClient.selectedItem.hasOwnProperty("isInterdit") && dgGroupeClient.selectedItem.isInterdit))
					
					
                {
                    
						selectedId = dgGroupeClient.selectedItem.IDGROUPE_CLIENT;
						libelleNoeud = dgGroupeClient.selectedItem.LIBELLE_GROUPE_CLIENT;
				}
			
					
				if(monTreeComplet.selectedItem && monTreeComplet.selectedItem.hasOwnProperty("@isAcces"))
				{
					if(monTreeComplet.selectedItem.@isAcces == true)
					{
						selectedId = parseInt(monTreeComplet.selectedItem.@NID, 10);
						libelleNoeud = monTreeComplet.selectedItem.@LBL;
					}
				}
								
				
				
				var f:Function = function(event:CloseEvent):void
				{
					if(event.detail == Alert.YES && selectedId > 0)
					{
						var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
							"deleteAcces", processDeleteAcces, null);
						RemoteObjectUtil.callService(op1, [ dgLoginParOrgas.selectedItem.APP_LOGINID, selectedId ]);		
					}
				}
					
				Alert.yesLabel = "Oui";
				Alert.cancelLabel = "Annuler";
				Alert.buttonWidth = 100;
				
				
				if(selectedId > 0)
				{
					var message:String ="Vous êtes sur le point de supprimer l'acces au Compte/Noeud :\n" +
						libelleNoeud + "\nEtes vous sûr de vouloir effectuer cette action ?";
					
					Alert.show(message,"Attention !!",Alert.YES|Alert.CANCEL,this,f,iconWarning,Alert.CANCEL);	
				}
				else
				{
					Alert.show("Vous devez sélectionner un compte d'abonnement");
				}
				
            }
            else
            {
                if(dgLoginParOrgas.selectedIndex != -1)
                {
                    Alert.show("Sélectionnez une organisation");
                }
                else
                {
                    Alert.show("Sélectionnez un login");
                }
            }
        }

        /**
         * Ouvre une pop-up permettant d'affecter à d'autres utilisateurs
         * les mêmes accès et styles que celui sélectionnée dans la liste
         *
         * @param event
         *
         */
        private function duplicateAccessUserHandler(event:Event):void
        {
            if((dgLoginParOrgas.selectedIndex != -1 && monTreeComplet.selectedIndex != -1) || (dgLoginParOrgas.selectedIndex != -1 && dgGroupeClient.selectedIndex !=
                -1))
            {
                // creation de la popup d'affectation
                var duplicatePopup:DuplicateUserFeaturesIHM = new DuplicateUserFeaturesIHM();
                // instanciation du vo à partir de l'objet du dataprovider
                var user:UserVO = new UserVO();
                user.deserialize(dgLoginParOrgas.selectedItem);
                // ouverture de la popup de configuration
                PopUpManager.addPopUp(duplicatePopup, this, true);
                PopUpManager.centerPopUp(duplicatePopup);
                // affectation des données utiles
                duplicatePopup.refGroup = dgGroupeClient.selectedItem;
                duplicatePopup.refUser = user;
                duplicatePopup.styleList = dgStyle.dataProvider as ArrayCollection;
            }
            else
            {
                Alert.show("Sélectionnez un login");
            }
        }

        private function updateModuleBase(event:Event):void
        {
            setDroit("fac", radioButtonsModuleBase.selectedValue as int);
            dgAccesParPersonne.selectedItem.MODULE_FACTURATION = radioButtonsModuleBase.selectedValue;
        }

        private function updateCycleDeVie(event:Event):void
        {
            setDroit("wor", radioButtonsCycleDeVie.selectedValue as int);
            dgAccesParPersonne.selectedItem.MODULE_WORKFLOW = radioButtonsCycleDeVie.selectedValue;
        }

        private function updateUsages(event:Event):void
        {
            setDroit("usa", radioButtonsUsages.selectedValue as int);
            dgAccesParPersonne.selectedItem.MODULE_USAGE = radioButtonsUsages.selectedValue;
        }

        private function updateGestionOrgas(event:Event):void
        {
            setDroit("gor", radioButtonsGestionOrgas.selectedValue as int);
            dgAccesParPersonne.selectedItem.MODULE_GESTION_ORG = radioButtonsGestionOrgas.selectedValue;
        }

        private function updateFixeData(event:Event):void
        {
            setDroit("fid", radioButtonFixData.selectedValue as int);
            dgAccesParPersonne.selectedItem.MODULE_FIXE_DATA = radioButtonFixData.selectedValue;
        }

        private function updateMobile(event:Event):void
        {
            setDroit("mob", radioButtonsMobile.selectedValue as int);
            dgAccesParPersonne.selectedItem.MODULE_MOBILE = radioButtonsMobile.selectedValue;
        }

        private function updateLogin(event:Event):void
        {
            setDroit("glo", radioButtonsLogin.selectedValue as int);
            dgAccesParPersonne.selectedItem.MODULE_GESTION_LOGIN = radioButtonsLogin.selectedValue;
        }

        private function changeDroitFournisseur(event:Event):void
        {
            if(checkAdmin.selected == true)
            {
                setDroit("gfo", 1);
                dgAccesParPersonne.selectedItem.DROIT_GESTION_FOURNIS = 1;
            }
            else
            {
                setDroit("gfo", 0);
                dgAccesParPersonne.selectedItem.DROIT_GESTION_FOURNIS = 0;
            }
        }

        //Modification dnas l'onglet "Acces" des droit d'un utilisateur : requete lancée à chaque changement 
        private function setDroit(module:String, valeur:int):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                 "changeDroit", processChangeDroit, null);
            RemoteObjectUtil.callService(op1, [ module, valeur, dgAccesParPersonne.selectedItem.APP_LOGINID, dgAccesParPersonne.selectedItem.ID ]);
        }

        //Création d'un accès
        private function creerAccesGroupeClient(event:Event):void
        {
            if((dgGroupeClient.selectedIndex != -1) && (dglog.selectedIndex != -1))
            {
                var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                     "updateAffectation", processCreerAcces, null);
                RemoteObjectUtil.callService(op1, [ dgGroupeClient.selectedItem.IDGROUPE_CLIENT, dglog.selectedItem.APP_LOGINID ]);
            }
            else
            {
                Alert.show("Impossible de créer un accès, sélectionnez un client + une orga. ")
            }
        }

        private function creerAcces(event:Event):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                 "updateAffectation", processCreerAcces, null);
            var id:int = parseInt(monTreeComplet.selectedItem.@NID, 10);
            RemoteObjectUtil.callService(op1, [ id, dglog.selectedItem.APP_LOGINID ]);
        }

        /**
         *  Ouvre la fenêtre de Duplication de tous les accès ( pour toutes les racines )
         *  pour l'utilisateur séléctionné dans le DATAGIRD
         * */
        private function duplicateAllAccess(event:Event):void
        {
            if((dgTousLesLogins.selectedIndex != -1 && monTreeComplet.selectedIndex != -1) || (dgTousLesLogins.selectedIndex != -1))
            {
                // creation de la popup d'affectation
                var duplicatePopup:DuplicateUserFeaturesIHM = new DuplicateUserFeaturesIHM();
                // instanciation du vo à partir de l'objet du dataprovider
                var user:UserVO = new UserVO();
                user.deserialize(dgTousLesLogins.selectedItem);
                // ouverture de la popup de configuration
                PopUpManager.addPopUp(duplicatePopup, this, true);
                PopUpManager.centerPopUp(duplicatePopup);
                // affectation des données utiles
                //duplicatePopup.refGroup = dgGroupeClient.selectedItem;
                duplicatePopup.duplicateAll = true;
                duplicatePopup.refUser = user;
                //duplicatePopup.styleList = dgStyle.dataProvider as ArrayCollection;
            }
            else
            {
                Alert.show("Sélectionnez un login");
            }
        }

        //Evenement envoie du login par email
        private function envoyerLoginParMail(event:MouseEvent):void
        {
			if(utilisateur_ObjCourant == null) return;
			
			if(dgTousLesLogins.selectedItem == null) return;			
			
            var op1 : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoprod.M21.GestionMail",
                                                                                 	"send_Login",
																				 	mailEnvoye,
																				 	null);
			
            RemoteObjectUtil.callService(op1, [ utilisateur_ObjCourant.getNomUtilisateur(), 
												utilisateur_ObjCourant.getPrenomUtilisateur(), 
												utilisateur_ObjCourant.getMailUtilisateur(), 
												utilisateur_ObjCourant.getIdUtilisateur(),
												dgTousLesLogins.selectedItem.IDGROUPE_CLIENT,
												dgTousLesLogins.selectedItem.CODE_LANGUE]);
        }

        //MSG  login par mail
        private function mailEnvoye(envent:Event):void
        {
            Alert.show("Login envoyé par E-mail  à " + utilisateur_ObjCourant.getNomUtilisateur());
        }

        //MSG du mdt par mail
        private function mdpEnvoye(envent:Event):void
        {
            Alert.show("Mot de passe envoyé par E-mail  à " + utilisateur_ObjCourant.getNomUtilisateur());
        }

        //Evenement envoie du mdp par email
        private function envoyerMDPParMail(event:MouseEvent):void
        {
			if(utilisateur_ObjCourant == null) return;
			
			if(dgTousLesLogins.selectedItem == null) return;	
			
            var op1 : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoprod.M21.GestionMail",
                                                                                 	"send_Pass",
																					mdpEnvoye,
																					null);
			
            RemoteObjectUtil.callService(op1, [ utilisateur_ObjCourant.getNomUtilisateur(), 
												utilisateur_ObjCourant.getPrenomUtilisateur(), 
												utilisateur_ObjCourant.getMailUtilisateur(), 
												utilisateur_ObjCourant.getIdUtilisateur(),
												dgTousLesLogins.selectedItem.IDGROUPE_CLIENT,
												dgTousLesLogins.selectedItem.CODE_LANGUE, 
												utilisateur_ObjCourant.getMDPUtilisateur()]);
        }

        //Evenement générer un nouveau mot de passe 
        //Appel de la méthode generateMotDePasse (taille du mdp)
        private function genererMDP(event:MouseEvent):void
        {
            inputPwd.text = generateMotDePasse(7);
        }

        //Function génerant un mdp aléatoire 
        //return mdp
        private function generateMotDePasse(longueur:int):String
        {
			var chars:String = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";
			var integer:String = "23456789";
			var pass:String = "";
			while(pass.length < LONGUEUR_MDP)
			{
				var x:int = Math.floor((Math.random() * (chars.length-1)));
				pass += chars.charAt(x);
			}
			// Création de la pattern	
			var pattern:String = "[0-9]";
			// création du reg exp
			var reg:RegExp = new RegExp(pattern)
			// on teste si le password contient un chiffre
			// Si le pass contient un chiffre, on retourne le pass
			// Sinon on remplace un caractère par un chiffre au hasard sauf 0 et 1
			var obj:Object = reg.test(pass as String) 
			if(obj) 
			{
				return pass;
			} 
			else 
			{
				var rand:Number = Math.random() 
				var y:int = Math.floor(rand * (integer.length-1));
				var idx:int = Math.floor(rand * (pass.length-1))
				var arr:Array = pass.split("");
				arr[idx] = integer.charAt(y)
				pass = arr.join("");
				return pass;
			}
        }

        private function affecteCompteClient(event:MouseEvent):void
        {
            if(dglog.selectedIndex != -1)
            {
                var objPopUp:PopUpGestionCompteClient = new PopUpGestionCompteClient(this, ObjectUtil.copy(dgGroupeClient.dataProvider as ArrayCollection) as
                                                                                     ArrayCollection, utilisateur_ObjCourant.getIdUtilisateur(), utilisateur_ObjCourant.
                                                                                     getNomUtilisateur() + " " + utilisateur_ObjCourant.getPrenomUtilisateur());
                PopUpManager.addPopUp(objPopUp, this, true, null);
                PopUpManager.centerPopUp(objPopUp);
            }
            else
            {
                Alert.show("Sélectionnez un Login");
            }
        }

        //Appel de la requete setStyle
        private function updateStyle(event:Event):void
        {
            if(dgAccesParPersonne.selectedIndex != -1)
            {
                var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                     "setStyle", processSetStyle, null);
                RemoteObjectUtil.callService(op1, [ dgAccesParPersonne.selectedItem.APP_LOGINID, dgAccesParPersonne.selectedItem.ID, dgStyle.selectedItem.
                                                    IDCONSO_STYLE ]);
            }
            else
            {
                Alert.show("Sélectionnez un accès");
            }
        }

        //Evenement Enregistrer infosUser
        private function enregistrerInfosUser(event:MouseEvent):void
        {
            //Nouveau User :
            var restriction:int;
            //A implementer :
            var isAdmin:int;
			if(emailValidator.validate(inputEmail.text).type =="valid")
			{
				if(modeCreation == true)
				{
					if(inputNom.length == 0 || inputPrenom.length == 0 || inputEmail.length == 0)
					{
						Alert.show("Veuillez remplir tous les champs");
					}
					else
					{
						if(checkRestrictionIP.selected == true)
						{
							restriction = 1;
						}
						else
						{
							restriction = 0;
						}
						var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
							"addApp_Login", processCreaUser, test);
						RemoteObjectUtil.callService(op, [ inputNom.text, inputPrenom.text, inputEmail.text, inputPwd.text, restriction, -1 ]);
					}
				}
					//Modif d'un user existant :
				else
				{
					if(dgTousLesLogins.selectedIndex != -1)
					{
						if(checkRestrictionIP.selected == true)
						{
							restriction = 1;
						}
						else
						{
							restriction = 0;
						}
						var op2:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
							"updateLogin", processModifUser, null);
						RemoteObjectUtil.callService(op2, [ utilisateur_ObjCourant.getIdUtilisateur(), inputNom.text, inputPrenom.text, inputEmail.text, inputPwd.
							text, restriction ]);
					}
					else
					{
						Alert.show("Aucun login sélectionné");
					}
				}
			}
            
        }//End enregistrerInfosUser()

        private function test(ect:FaultEvent):void
        {
            Alert.show(ect.toString());
        }

        //Vide les champs de l'onglet utilisateur
        private function nouveauUtilisateur(event:MouseEvent):void
        {
            dgTousLesLogins.selectedIndex = -1;
            dgAccesParPersonne.dataProvider = null;
            inputNom.text = "";
            inputPrenom.text = "";
            inputEmail.text = "";
            inputPwd.text = generateMotDePasse(7);
            modeCreation = true;
            monTree.dataProvider = null;
        }

        //Initialise l'interface "remise à zero de tous les champs"
        private function actualise():void
        {
            afficheGroupeMaitre();
            afficheStyle();
            afficheLogin();
            dgLoginParOrgas.dataProvider = null;
            labMsgInfos.text = "";
        }

        //Affichage du msg de confirmation
        private function supprimerUser(event:MouseEvent):void
        {
            if(dgTousLesLogins.selectedIndex != -1)
            {
				Alert.yesLabel = "Oui";
				Alert.cancelLabel = "Annuler";
				Alert.buttonWidth = 100;
				var message:String ="Vous êtes sur le point de supprimer tous les accès de ce login!\n"+
					"Après cette action, ce login n'aura accès à aucun Compte!\n"+						
					"\tEtes vous sûr de vouloir effectuer cette action ?";
				
                Alert.show(message, "Attention !!", Alert.YES | Alert.CANCEL, this, confirmSuppression, iconWarning, Alert.CANCEL);
            }
            else
            {
                Alert.show("Sélectionnez un login");
            }
        }

        //Gestion du résultat de la demande de confirmation de suppression d'un client
        private function confirmSuppression(evt_obj:Object):void
        {
            if(evt_obj.detail == Alert.YES)
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient",
                                                                                    "deleteLogin", processSupprimerUser, null);
                RemoteObjectUtil.callService(op, [ utilisateur_ObjCourant.getIdUtilisateur() ]);
                //Vider les champs :
                nouveauUtilisateur(null);
            }           
        }

        private function newPanelAccesMultiple(evt:MouseEvent):void
        {
            var dg:DataGrid = dgTousLesLogins.dgPaginate;
            var objPopUp:PanelMultipleAcces = new PanelMultipleAcces(dgGroupeClient.selectedItem.IDGROUPE_CLIENT, dgGroupeClient.selectedItem.LIBELLE_GROUPE_CLIENT,
                                                                     ObjectUtil.copy(dgGroupeClient.dataProvider as ArrayCollection) as ArrayCollection,
                                                                     ObjectUtil.copy(dg.dataProvider as ArrayCollection) as ArrayCollection, ObjectUtil.
                                                                     copy(dgStyle.dataProvider as ArrayCollection) as ArrayCollection);
            PopUpManager.addPopUp(objPopUp, this, true);
            PopUpManager.centerPopUp(objPopUp);
        }

        /*----------------------------------------------------------------------------------------------------------------------------------
           ------------------------------------------------------------------------------------------------------------------------------------
           ------------------------------------------------------------------------------------------------------------------------------------
           ----------------------------------------------------COHERENCE DES RADIOBOUTONS------------------------------------------------------
           ------------------------------------------------------------------------------------------------------------------------------------
         ------------------------------------------------------------------------------------------------------------------------------------*/
        private function enabledRadioBt(objGroupeClient:Object):void
        {
            radioButtonFixData.enabled = true;
            radioButtonsCycleDeVie.enabled = true;
            radioButtonsGestionOrgas.enabled = true;
            radioButtonsLogin.enabled = true;
            radioButtonsMobile.enabled = true;
            radioButtonsModuleBase.enabled = true;
            radioButtonsUsages.enabled = true;
            trace("------------------objGroupeClient------------------->" + objGroupeClient.toString());
            if(objGroupeClient.MODULE_FACTURATION == 0)
            {
                radioButtonsModuleBase.getRadioButtonAt(1).enabled = false;
                radioButtonsModuleBase.getRadioButtonAt(2).enabled = false;
            }
            else
            {
                if(objGroupeClient.MODULE_FACTURATION == 1)
                {
                    radioButtonsModuleBase.getRadioButtonAt(2).enabled = false;
                }
            }
            if(objGroupeClient.MODULE_WORKFLOW == 0)
            {
                radioButtonsCycleDeVie.getRadioButtonAt(1).enabled = false;
                radioButtonsCycleDeVie.getRadioButtonAt(2).enabled = false;
            }
            else
            {
                if(objGroupeClient.MODULE_WORKFLOW == 1)
                {
                    radioButtonsCycleDeVie.getRadioButtonAt(2).enabled = false;
                }
            }
            if(objGroupeClient.MODULE_USAGE == 0)
            {
                radioButtonsUsages.getRadioButtonAt(1).enabled = false;
                radioButtonsUsages.getRadioButtonAt(2).enabled = false;
            }
            else
            {
                if(objGroupeClient.MODULE_USAGE == 1)
                {
                    radioButtonsUsages.getRadioButtonAt(2).enabled = false;
                }
            }
            if(objGroupeClient.MODULE_GESTION_ORG == 0)
            {
                radioButtonsGestionOrgas.getRadioButtonAt(1).enabled = false;
                radioButtonsGestionOrgas.getRadioButtonAt(2).enabled = false;
            }
            else
            {
                if(objGroupeClient.MODULE_GESTION_ORG == 1)
                {
                    radioButtonsGestionOrgas.getRadioButtonAt(2).enabled = false;
                }
            }
            if(objGroupeClient.MODULE_FIXE_DATA == 0)
            {
                radioButtonFixData.getRadioButtonAt(1).enabled = false;
                radioButtonFixData.getRadioButtonAt(2).enabled = false;
            }
            else
            {
                if(objGroupeClient.MODULE_FIXE_DATA == 1)
                {
                    radioButtonFixData.getRadioButtonAt(2).enabled = false;
                }
            }
            if(objGroupeClient.MODULE_MOBILE == 0)
            {
                radioButtonsMobile.getRadioButtonAt(1).enabled = false;
                radioButtonsMobile.getRadioButtonAt(2).enabled = false;
            }
            else
            {
                if(objGroupeClient.MODULE_MOBILE == 1)
                {
                    radioButtonsMobile.getRadioButtonAt(2).enabled = false;
                }
            }
            if(objGroupeClient.MODULE_GESTION_LOGIN == 0)
            {
                radioButtonsLogin.getRadioButtonAt(1).enabled = false;
                radioButtonsLogin.getRadioButtonAt(2).enabled = false;
            }
            else
            {
                if(objGroupeClient.MODULE_GESTION_LOGIN == 1)
                {
                    radioButtonsLogin.getRadioButtonAt(2).enabled = false;
                }
            }
            if(objGroupeClient.DROIT_GESTION_FOURNIS == "1")
            {
                checkAdmin.enabled = true;
            }
            else
            {
                checkAdmin.enabled = false;
            }
        }
		
		
		
    }
}
