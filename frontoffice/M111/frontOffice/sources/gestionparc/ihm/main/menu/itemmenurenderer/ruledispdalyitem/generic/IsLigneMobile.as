package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class IsLigneMobile implements IRuleDisplayItem
	{
		public function IsLigneMobile()
		{
		}

		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;		
			return ligne.ISLIGNEMOBILE;
		}
	}
}
