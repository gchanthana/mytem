﻿	
<!-- displayname : le nom de composant -->

<cfcomponent displayname="Modules" output="false">
	
	<!-- déclarer une fonction  name : le nom de la fonction ;returntype : le type du resultat retourné -->
	<!-- access : visibilité -->

	<cffunction access="public" output="true" name="getRapport" returntype="query">
			
		
			/* definir des valeur par défaut */
			
			<cfset idModule=0>/* 0= tous les modules */
			<cfset text_filtre="">/* pas de filtre */
			<cfset segment="0">/* 0= tous segement (fixe , mobile ,réseau) */
			<cfset typeRapport=0>/* rapport standard */
			<cfset indexDepart=1>/* rapport standard */
			
			<cfset listeTypeAnalyse=arraynew(1)>/* type analyse */
						
			<cfset listeTypeAnalyse[1]=true>/*  sur période choisie */
			<cfset listeTypeAnalyse[2]=true>/*  Suivi d'évolutions */
			<cfset listeTypeAnalyse[3]=true>/*  A l'instant T */
			
		
			<cfset listeTypeAgregation=arraynew(1)>/* type Agregation */
			<cfset listeTypeAgregation[1]=true>/*   infos lignes */
			<cfset listeTypeAgregation[2]=true>/*  infos Consolidées */ 
			
						
			<cfset nbItemParPage=100>
			<cfset stringAnalyse=''>
			<cfset stringAgreag=''>	
			
			<cfloop from="1" to="#ArrayLen(listeTypeAnalyse)#" index="i" step="1">		
				<cfset cleAnalyse=listeTypeAnalyse[i] />
				
				 <cfif cleAnalyse eq "YES">
					<cfset value="#i#">
					<cfelse>
					   <cfset value="0">
				 </cfif>
				<cfset stringAnalyse=stringAnalyse & value >	
		    </cfloop>
		<cfloop from="1" to="#ArrayLen(listeTypeAgregation)#" index="i" step="1">
			
				<cfset cleAgregation=listeTypeAgregation[i] />
				
				<cfif cleAgregation eq "YES">
					<cfset value="#i#">
					<cfelse>
					   <cfset value="0">
				 </cfif>
				<cfset stringAgreag=stringAgreag & value >
		    </cfloop>
		   
			<!-- déclarer des varaibles pour passer au procédure -->
			<!-- cette décalration n'est pas obligatoire et dépend de procédure' -->
			
			<cfset iduser   = SESSION.USER.CLIENTACCESSID>
			<cfset idRacine = SESSION.USER.CLIENTID> <!--- l'id de l'user conncté  --->
			<cfset codeApp  = SESSION.CODEAPPLICATION> 
			<!--- <cfset codeApp  =51> --->
			<cfset idLang  = SESSION.USER.IDGLOBALIZATION>
					
			<!-- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
			<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.-->
			 
            <cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M331.get_rapport_racine">			
				
				 <!-- paramtères : Si la procédure stockée utilise des paramètres d'entrée ou de sortie -->	
				 <!-- ces paramètres  dépendent de la procedure -->	
				  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">		 
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#iduser#" variable="p_app_loginid">
				  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeApp#" variable="p_application_consotelid">
				  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idModule#"  variable="p_module_consotelid">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#typeRapport#" variable="p_type_rapport ">
                  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#segment#" variable="p_segment_rapport ">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#stringAnalyse#" variable="p_idrapport_periodicite">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#stringAgreag#" variable="p_type_aggreg">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#text_filtre#" variable="p_texte_filtre">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#indexDepart#" variable="p_index_debut ">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#nbItemParPage#" variable="p_number_of_records">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idLang#" variable="p_langid">
                 
                  <cfprocresult name="qRapports"><!-- Si la procédure stockée retourne un résultat -->
            </cfstoredproc>        					
            <cfreturn qRapports><!-- le resultat de la procedure-->
      </cffunction>
      
</cfcomponent>