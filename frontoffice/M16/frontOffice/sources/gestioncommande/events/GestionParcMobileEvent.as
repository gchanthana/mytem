package gestioncommande.events
{
	import flash.events.Event;
	
	public class GestionParcMobileEvent extends Event
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - STATIC - EVENT POPUP
		//--------------------------------------------------------------------------------------------//	
			
		public static const POPUP_ABO_OPT_VALIDATED 			: String = "popupAboOptValidated";
		public static const POPUP_SIM_VALIDATED 				: String = "popupSimValidated";		
		
		public static const POPUP_CONFIRMER_ACTION_CONFIRMER	: String = "popupConfirmerActionConfirmer";
		public static const POPUP_CONFIRMER_ACTION_ANNULER		: String = "popupConfirmerActionAnnuler";
		
		public static const NEW_MODELE_ADDED					: String = "newModeleAdded";
		public static const NEW_LINE_MANUAL_ADDED_OK			: String = "NewLineManualAddedOk";
		public static const NEW_LINE_MANUAL_ADDED_KO			: String = "NewLineManualAddedKo";

		public static const EQPT_IMPORTE						: String = "eqptImporte";

		public static const OPEN_POPUP_CONFIRMATION				: String = "openPopupConfirmation";
		
		public static const POPUP_CONFIRMATION_IMPORT			: String = "popupConfirmationImport";
		
		// Fiche historique
		public static const INFOS_FICHE_HISTO_LOADED			: String = "infosFicheHistoLoaded";
		
		// Fiche collaborateur
		public static const INFOS_FICHE_COLLAB_LOADED			: String = "infosFicheCollabLoaded";
		public static const INFOS_FICHE_COLLAB_UPLOADED			: String = "infosFicheCollabUploaded";
		public static const NEW_COLLAB_ADDED					: String = "newCollabAdded";

		// Fiche équipement
		public static const INFOS_FICHE_TERM_LOADED				: String = "infosFicheTermLoaded";
		public static const INFOS_FICHE_TERM_UPLOADED			: String = "infosFicheTermUploaded";
		public static const INFOS_CMD_SAVED						: String = "infosCmdSaved";
		public static const UPLOAD_INFOS_CMD					: String = "uploadInfosCmd";
		public static const NUM_CMD_GENERATED					: String = "numCmdGenerated";
		public static const LISTE_CONSTRU_LOADED 				: String = "listeConstruLoaded";
		public static const LISTE_MODELES_LOADED 				: String = "listeModelesLoaded";
		public static const CHOICE_MODELE_DID	 				: String = "choiceModeleDid";
		
		// onglet contrat
		public static const INFOS_CONTRAT_SAVED	 				: String = "infosContratSaved";
		public static const UPDATE_LISTE_CONTRAT	 			: String = "updateListeContrat";
		public static const DELETE_CONTRAT			 			: String = "deleteContrat";
		public static const LISTE_INTERVENTION_LOADED 			: String = "listeInterventionLoaded";

		// onglet incident
		public static const INFOS_INCIDENT_SAVED	 			: String = "infosIncidentSaved";
		public static const UPDATE_LISTE_INCIDENT	 			: String = "updateListeIncident";
		public static const DELETE_INCIDENT			 			: String = "deleteIncident";
		public static const INFOS_INTERVENTION_SAVED 			: String = "infosInterventionSaved";
		public static const UPDATE_LISTE_INTERVENTION 			: String = "updateListeIntervention";
		public static const DELETE_INTERVENTION			 		: String = "deleteIntervention";
		public static const LISTE_INTER_LOADED			 		: String = "listeInterLoaded";
		
		public static const LISTE_REVENDEUR_LOADED 				: String = "listeRevendeurLoaded";
		public static const LISTE_CONTRAT_LOADED 				: String = "listeContratLoaded";
		
		// Fiche SIM
		public static const INFOS_FICHE_SIM_LOADED			: String = "infosFicheSimLoaded";
		public static const INFOS_FICHE_SIM_UPLOADED			: String = "infosFicheSimUploaded";
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES STATIC - EVENT NAME
		//--------------------------------------------------------------------------------------------//
		
		public static const LISTED_PROFILES_LOADED 				: String = "listedProfilesLoaded";
		public static const LISTED_ABO_OPT_LOADED 				: String = "listedAboOptLoaded";
		
		public static const LISTED_OPERATEUR_LOADED 			: String = "listedOperateurLoaded";
		public static const LISTED_REVENDEUR_LOADED 			: String = "listedRevendeurLoaded";
		public static const LISTED_MODELE_LOADED 				: String = "listedModeleLoaded";

		public static const TYPE_LIGNE_LOADED					: String = "typeLigneLoaded";
	
		public static const READ_CSV_AND_CSV_TO_ARRAY 			: String = "readCsvAndCsvToArray";
		public static const READ_XLS_AND_XLS_TO_ARRAY			: String = "readXlsAndXlsToArray";
	
		public static const THIS_IS_FAVORITE 					: String = "thisIsFavorite";
		public static const THIS_IS_NOT_FAVORITE 				: String = "thisIsNotFavorite";

		public static const INIT_COMBO_CONSTRUCTEUR_FINISH 		: String = "initComboConstructeurFinish";

		public static const LISTE_COMPTES_SOUSCOMPTES_LOADED 	: String = "listeComptesSouscomptesLoaded";

		public static const FICHE_NEW_LINE_ORGA_SELECTED		: String = "ficheNewLineOrgaSelected";


		public static const OBJECT_DESAFFECTED					: String = "objectDesaffected";
		public static const OBJECT_AFFECTED						: String = "objectAffected";

		public static const RECHERCHER_FINISHED					: String = "rechercherFinished";

		public static const LISTED_NODES_ORGA_LOADED 			: String = "listedNodesOrgaLoaded ";
		public static const LISTE_ORGA_LOADED		 			: String = "listeOrgaLoaded ";
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES STATIC - EVENT VALIDATION 
		//--------------------------------------------------------------------------------------------//
		
		public static const VALIDE_FICHE_ORGA		 			: String = "valideFicheOrga ";
		

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES PASSEES EN ARGUMENTS
		//--------------------------------------------------------------------------------------------//		
		
		public var obj								:Object = null;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//		
		
		public function GestionParcMobileEvent(	type		:String,
												objet		:Object  = null,
												bubbles		:Boolean = false,
												cancelable	:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.obj	= objet;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC OVERRIDE - COPY OF EVENT
		//--------------------------------------------------------------------------------------------//
				
		override public function clone():Event
		{
			return new GestionParcMobileEvent(type, obj, bubbles, cancelable);
		}
		
	}
}