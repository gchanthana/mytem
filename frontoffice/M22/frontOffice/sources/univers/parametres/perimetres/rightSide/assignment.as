package univers.parametres.perimetres.rightSide
{
    import composants.parametres.perimetres.SendMailPerimetre;
    import composants.parametres.perimetres.SendMailPerimetreView;
    import composants.util.ConsoviewUtil;
    import composants.util.DateFunction;
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.controls.Tree;
    import mx.core.IFactory;
    import mx.events.DragEvent;
    import mx.events.FlexEvent;
    import mx.managers.DragManager;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import univers.parametres.perimetres.main;
    import univers.parametres.perimetres.mainComponent.NodeProperties;
    import univers.parametres.perimetres.rightSide.assignmentComponent.AssignOrgaCmp;
    import univers.parametres.perimetres.rightSide.assignmentComponent.newAssignSimple;

    public class assignment extends assignmentIHM
    {
        private const TYPE_MOBILE:Number = 707;
        private const LIGNES:String = "Lignes";
        private const LIGNES_AFFECTEES:String = "LignesAffectees";
        private const LIGNES_NON_AFFECTEES:String = "LignesNonAffectees";
        private const LIBELLE_JLIGNES:String = "Lignes affectées et non affectées";
        private const LIBELLE_jLIGNES_AFFECTEES:String = "Lignes affectées";
        private const LIBELLE_jLIGNES_NON_AFFECTEES:String = "Lignes non affectées";
        private var _treeNodes:XML;
        private var _gridContextList:ArrayCollection;
        private var _treeContextList:ArrayCollection;
        private var _dataLines:ArrayCollection;
        private var _mainRef:main;
        private var _showLines:Boolean = false;
        private var myTreeItemRenderer:IFactory;
        private var _currentJournal:String = LIGNES;
        private var _currentJournalLibelle:String = LIBELLE_JLIGNES;
        private var _currentTypeOrga:String;
        private var _propertiesWin:NodeProperties;
        private var _propertiesData:ArrayCollection;
        private var _tempcurrentIdClient:Number;

        public function get modeEcriture():Boolean
        {
            //return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			return true
        }
        private var _modeRestreint:Boolean = false;

        public function get modeRestreint():Boolean
        {
            return _modeRestreint
        }

        public function set modeRestreint(b:Boolean):void
        {
            _modeRestreint = b;
            callLater(configModeRestreint);
        }
        private var _initCompelte:Boolean = false;
        private var _refSet:Boolean = false;

        override protected function commitProperties():void
        {
            if(_initCompelte)
            {
                if(_refSet)
                {
                }
            }
        }

        public function assignment()
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
            if(modeEcriture)
            {
                _gridContextList = new ArrayCollection([ /*{label:"Affecter", callback:onbtAffecterSimpleClicked, type:1},*/ { id:"mnAffecter", label:"Affecter",
                                                                                                                                 callback:onbtAffecterMultipleClicked,
                                                                                                                                 type:1 } ]);
            }
            else
            {
                _gridContextList = new ArrayCollection([ /*{label:"Affecter", callback:onbtAffecterSimpleClicked, type:1},*/ /* {label:"Affecter", callback:onbtAffecterMultipleClicked, type:1} */  ]);
            }
            _treeContextList = new ArrayCollection([ { label:resourceManager.getString('M22', 'Afficher_les_lignes'), callback:onbtAfficherLignesClicked,
                                                         type:1 }, { label:"Propriété", callback:onbtProprieteClicked, type:0 } ]);
        }

        protected function initIHM(event:Event):void
        {
            orgaStructure.treePerimetre.labelFunction = treeLabelFunction;
            orgaStructure.addEventListener(AssignOrgaCmp.SELECTION_CHANGE, orgaStructureSelectionChangeHandler);
            orgaStructure.setParentRef(this);
            btAssign.visible = modeEcriture;
            configModeRestreint();
            cbNotAssign.selected = true;
            cbNotAssign.addEventListener(MouseEvent.CLICK, checkBoxChange);
            cbAssign.addEventListener(MouseEvent.CLICK, checkBoxChange);
            cbAll.addEventListener(MouseEvent.CLICK, checkBoxChange);
            orgaStructure.setContextMenu(_treeContextList);
            orgaStructure.setXMLFilter(menuFilterData);
            grdLinesBloc.setContextMenu(_gridContextList);
            btnAfficherLignes.addEventListener(MouseEvent.CLICK, onbtAfficherLignesClicked);
            imgRefresh.addEventListener(MouseEvent.CLICK, rafraichirArbre);
            btAssign.addEventListener(MouseEvent.CLICK, onbtAffecterMultipleClicked);
            //btExport.addEventListener(MouseEvent.CLICK,exportOrgaStCSV);
            btExport.addEventListener(MouseEvent.CLICK, onExport);
            btExport.enabled = false;
            grdLinesBloc.gdrLignes.addEventListener(DragEvent.DRAG_COMPLETE, gdrLignesDragCompleteHandler);
            btExportGrid.visible = false;
            btExportGrid.enabled = false;
            btExportGrid.addEventListener(MouseEvent.CLICK, exporterGrid);
            _initCompelte = true;
        }

        private function treeLabelFunction(item:Object):String
        {
            if(XML(item).hasOwnProperty("@COMMENTAIRES") && String(item.@COMMENTAIRES).length > 0)
            {
                return item.@LABEL + " - " + item.@COMMENTAIRES;
            }
            else
            {
                return item.@LABEL;
            }
        }

        private function configModeRestreint():void
        {
            if(modeRestreint)
            {
                orgaStructure.modeRestreint = true;
            }
            else
            {
                orgaStructure.modeRestreint = false;
            }
        }

        /** ############################# EVENTS ######################## */
        private function orgaStructureSelectionChangeHandler(ev:Event):void
        {
            grdLinesBloc.setData(null);
            refreshButtons();
        }

        private function gdrLignesDragCompleteHandler(de:DragEvent):void
        {
            if(de.action == DragManager.LINK)
            {
                onbtAffecterMultipleClicked(null);
                return;
            }
            if(de.action == DragManager.MOVE)
            {
                return;
            }
        }

        protected function onbtAffecterMultipleClicked(event:Event):void
        {
            assignSelectedLine();
        }

        protected function checkBoxChange(event:MouseEvent):void
        {
            if(orgaStructure.getSelectedTypeOrga() == "OPE")
            {
                btExport.enabled = true;
            }
            else
            {
                //btExport.enabled = false;
            }
            var result:Number = 1;
            refreshFilters();
            clearGridLines();
            refreshButtons();
            orgaStructure.clearSearch();
            if(cbNotAssign.selected)
                result = 1;
            else
                result = 2;
            orgaStructure.setLineType(result);
        }

        public function onbtProprieteClicked(event:Event):void
        {
            if(orgaStructure.getSelectedTreeIndex() < 0)
                return;
            displayNodeProperties(orgaStructure.getSelectedTreeItemValue());
        }

        private function displayNodeProperties(IDnode:Number):void
        {
            if(_propertiesWin != null)
                PopUpManager.removePopUp(_propertiesWin);
            _propertiesWin = PopUpManager.createPopUp(this, NodeProperties, true) as NodeProperties;
            _propertiesWin.setParentRef(_mainRef);
            _propertiesWin.setDataRef(orgaStructure.getSelectedItem());
            _propertiesWin.setID(orgaStructure.getSelectedOrganisationValue(), orgaStructure.getSelectedTypeOrga(), IDnode);
        }

        public function onbtAfficherLignesClicked(event:Event):void
        {
            _showLines = true;
            var method:String = "getAllLines";
            _currentJournal = LIGNES;
            _currentJournalLibelle = LIBELLE_JLIGNES;
            _currentTypeOrga = orgaStructure.getSelectedTypeOrga();
            switch(orgaStructure._linesType)
            {
                case 1:
                {
                    method = "getNotAssignedLines";
                    _currentJournal = LIGNES_NON_AFFECTEES;
                    _currentJournalLibelle = LIBELLE_jLIGNES_NON_AFFECTEES;
                    break;
                }
                case 2:
                {
                    method = "getAssignedLines";
                    _currentJournal = LIGNES_AFFECTEES;
                    _currentJournalLibelle = LIBELLE_jLIGNES_AFFECTEES;
                    break;
                }
            }
            var id:Number = orgaStructure.getSelectedTreeItemValue();
            _tempcurrentIdClient = id;
            var orgaRef:Number = _mainRef.getSelectedOrganisation();
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
                                                                                method, getLinesResult);
            RemoteObjectUtil.callService(op, id, orgaRef, _mainRef.getSelectedDate());
        }

        private function onExport(evt:MouseEvent):void
        {
            if(orgaStructure.getSelectedTypeOrga() == "OPE")
            {
                onbtExproterClicked(evt);
            }
            if(orgaStructure.isMemeOrga)
            {
                exportOrgaStCSV(evt);
            }
        }

        // Migration de la fonction d'exportation des lignes affectées depuis la page d'accueil
        private function exportOrgaStCSV(event:MouseEvent):void
        {
            var url:String = modulePerimetreIHM.urlBackoffice + "/fr/consotel/consoview/cfm/tools/exportOrgaStCSV.cfm";
            var variables:URLVariables = new URLVariables();
            if(orgaStructure.cmbOrganisation.selectedItem != -1)
            {
                // Cas : le perimetre est la racine
                if(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT)
                {
                    if(orgaStructure.cmbOrganisation.selectedItem.hasOwnProperty("IDGROUPE_CLIENT") && orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT !=
                        null)
                    {
                        variables.id_perimetre = orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT
                        variables.libelle_perimetre = orgaStructure.cmbOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT
                    }
                }
                else
                {
                    variables.id_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
                    variables.libelle_perimetre = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
                }
            }
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = "POST";
            navigateToURL(request, "_blank");
        }

        public function onbtExproterClicked(event:Event):void
        {
            _showLines = true;
            var method:String = "getAllLines";
            _currentJournal = LIGNES;
            _currentJournalLibelle = LIBELLE_JLIGNES;
            _currentTypeOrga = orgaStructure.getSelectedTypeOrga();
            switch(orgaStructure._linesType)
            {
                case 1:
                {
                    method = "getNotAssignedLines";
                    _currentJournal = LIGNES_NON_AFFECTEES;
                    _currentJournalLibelle = LIBELLE_jLIGNES_NON_AFFECTEES;
                    break;
                }
                case 2:
                {
                    method = "getAssignedLines";
                    _currentJournal = LIGNES_AFFECTEES;
                    _currentJournalLibelle = LIBELLE_jLIGNES_AFFECTEES;
                    break;
                }
            }
            var id:Number = orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT;
            var orgaRef:Number = _mainRef.getSelectedOrganisation();
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
                                                                                method, getLinesOrgaResult);
            trace("-----onbtExproterClicked() parametres 3 : " + _mainRef.getSelectedDate());
            var currentDate:Date = new Date();
            RemoteObjectUtil.callService(op, id, orgaRef, DateFunction.formatDateAsInverseString(currentDate));
        }

        /*protected function onbtAffecterSimpleClicked(event: Event):void
           {
           switch (_mainRef.getSelectedTypeOrga())
           {
           case "ANA" :
           assignAnaSelectedLine();
           break;
           default:
           assignSelectedLine();
           }
         }*/
        public function mainDateChanged():void
        {
            orgaStructure.refreshTree();
            clearGridLines();
            orgaStructure.clearSearch();
            refreshButtons();
        }

        public function mainOrganisationChanged():void
        {
            orgaStructure.refreshTree();
            /*clearGridLines();*/
            organisationChanged();
        }

        public function mainTreeSelectionChanged():void
        {
            refreshButtons();
        }

        public function organisationChanged():void
        {
            checkBoxChange(new MouseEvent(""));
        }

        public function treeSelectionChanged():void
        {
            refreshButtons();
            clearGridLines();
        }

        public function selectedLineChanged():void
        {
            refreshButtons();
        }

        /** ########################### END EVENTS ######################## */
        public function fillOrganisation():void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetreStructure",
                                                                                "getOrganisation", fillOrganisationResult);
            RemoteObjectUtil.callService(op, idGroupeMaitre);
        }

        private function fillOrganisationResult(result:ResultEvent):void
        {
            var collec:ArrayCollection = ArrayCollection(result.result);
            // suppression du choix annuaire 
            for(var i:int = 0; i < collec.length; i++)
            {
                if(collec.getItemAt(i).TYPE_ORGA == "ANU")
                {
                    collec.removeItemAt(i);
                    break;
                }
            }
            orgaStructure.setCmbData(ArrayCollection(collec), "LIBELLE_GROUPE_CLIENT");
            refreshFilters();
            orgaStructure.setLineType(cbNotAssign.selected ? 1 : 2);
            clearGridLines();
            refreshButtons();
        }

        private function refreshButtons():void
        {
            btAssign.enabled = false;
            if(modeEcriture)
                _gridContextList[0].forceEnabled = true;
            //else _gridContextList[0].forceEnabled = false;
            btnAfficherLignes.enabled = false;
            if(orgaStructure.getSelectedTreeItemValue() > 0)
            {
                btnAfficherLignes.enabled = true;
            }
            if(_mainRef.isLastSelectedChild() && grdLinesBloc.getSelectedListGridItem() != "")
            {
                if(modeEcriture)
                {
                    btAssign.enabled = true;
                    _gridContextList[0].forceEnabled = false;
                }
            }
            if(_mainRef.getSelectedItemNode() && _mainRef.getSelectedItemNode().@IS_ACTIVE == 0)
            {
                btAssign.enabled = false;
            }
            if(!orgaStructure.isMemeOrga && !orgaStructure.getSelectedTypeOrga() != "OPE")
            {
                btExport.enabled = false;
            }
            if((orgaStructure.isMemeOrga && orgaStructure.cmbOrganisation.selectedIndex != -1) || orgaStructure.getSelectedTypeOrga() == "OPE")
            {
                btExport.enabled = true;
            }
        }

        private function getLinesOrgaResult(event:ResultEvent):void
        {
            exporterCSV();
        }

        private function getLinesResult(evt:ResultEvent):void
        {
            _dataLines = ArrayCollection(evt.result);
            if(_dataLines.length <= 0)
            {
                btExportGrid.visible = false;
                orgaStructure.refreshTree();
            }
            else
            {
                if(_currentTypeOrga != "ANA")
                {
                    btExportGrid.visible = true;
                    btExportGrid.enabled = true;
                }
                else
                {
                    btExportGrid.visible = false;
                    btExportGrid.enabled = false;
                }
            }
            grdLinesBloc.setData(_dataLines);
            refreshButtons();
        }
        //Boite de dialogue d'envoi de mail
        private var _mailDialogBox:SendMailPerimetreView;

        private function checkProfile():void
        {
            var idGroupeClientCible:Number = _mainRef.getSelectedNode();
            var idGroupeClientSrc:Number = orgaStructure.getSelectedTreeItemValue();
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
                                                                                "checkProfileAndGetInfos", checkProfileResultHandler);
            RemoteObjectUtil.callService(op, idGroupeClientCible, idGroupeClientSrc);
        }

        private function checkProfileResultHandler(re:ResultEvent):void
        {
            if(re.result)
            {
                if(re.result.length > 0)
                {
                    openMailBox(re.result);
                }
            }
        }

        private function openMailBox(infosDest:Object):void
        {
            var infosMail:Object = new Object();
            infosMail.DESTINATAIRE = "";
            infosMail.SOURCE = orgaStructure.getSelectedItem().@LABEL;
            infosMail.CIBLE = _mainRef.getSelectedItemNode().@LBL;
            infosMail.ORGANISATION = _mainRef.getSelectedOrganisationItem();
            infosMail.LIGNES = grdLinesBloc.gdrLignes.selectedItems;
            infosMail.GROUPE_RACINE = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
            infosMail.INFOS_DEST = infosDest;
            if(_mailDialogBox != null)
                _mailDialogBox = null;
            _mailDialogBox = new SendMailPerimetreView();
            _mailDialogBox.addEventListener(SendMailPerimetre.MAIL_ENVOYE, mailEnvoyerResultHandler);
            PopUpManager.addPopUp(_mailDialogBox, DisplayObject(parentApplication), true);
            _mailDialogBox.infosObject = infosMail;
            var listeMail:String = ConsoviewUtil.arrayToList(infosDest[0], "EMAIL", ",");
            _mailDialogBox.initMail("Parametre", "Transfert de compte & modification de référence " + infosMail.GROUPE_RACINE, listeMail);
        }

        private function mailEnvoyerResultHandler(ev:Event):void
        {
            trace("Le mail a été envoyé");
        }

        private function assignSelectedLine():void
        {
            var _list:String = grdLinesBloc.getSelectedListGridItem();
            var idGroupeClient:Number = _mainRef.getSelectedNode();
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
                                                                                "assignLine", assignLineResult);
            RemoteObjectUtil.callService(op, idGroupeClient, _list);
        }

        public function assignLineResult(event:ResultEvent):void
        {
            checkProfile();
            _mainRef.refreshLineGrid(true);
            _mainRef.clearSearch();
            onbtAfficherLignesClicked(new Event("refresh"));
        }

        private function assignAnaSelectedLine():void
        {
            var assignWin:newAssignSimple = PopUpManager.createPopUp(this, newAssignSimple, true) as newAssignSimple;
            assignWin.setParentRef(this);
            assignWin.setDataID(_mainRef.getSelectedNode(), grdLinesBloc.getSelectedListGridItem());
        }

        private function refreshFilters():void
        {
            if(modeRestreint)
            {
                if(_mainRef.getSelectedOrganisation() == orgaStructure.getSelectedOrganisationValue())
                {
                    orgaStructure.isMemeOrga = true;
                    cbAssign.visible = true;
                    cbAssign.selected = true;
                    cbNotAssign.visible = false;
                    cbNotAssign.selected = false;
                    btExport.enabled = true;
                }
                else
                {
                    orgaStructure.isMemeOrga = false;
                    cbAssign.visible = false;
                    cbAssign.selected = false;
                    cbNotAssign.visible = true;
                    cbNotAssign.selected = true;
                    btExport.enabled = true;
                }
            }
            else
            {
                if(_mainRef.getSelectedOrganisation() == orgaStructure.getSelectedOrganisationValue())
                {
                    orgaStructure.isMemeOrga = true;
                    cbAssign.selected = true;
                    cbAssign.visible = true;
                    cbNotAssign.visible = false;
                    cbNotAssign.selected = false;
                    btExport.enabled = false;
                }
                else
                {
                    orgaStructure.isMemeOrga = false;
                    cbAssign.visible = true;
                    cbNotAssign.visible = true;
                    btExport.enabled = true;
                }
            }
        /*
           cbNotAssign.visible = true;
           if (_mainRef.getSelectedOrganisation() == orgaStructure.getSelectedOrganisationValue())
           {
           orgaStructure.isMemeOrga = true;
           cbNotAssign.visible = false;

           if (!cbNotAssign.visible){
           cbAssign.selected = true;
           cbNotAssign.selected = false;
           }

           }else{
           orgaStructure.isMemeOrga = false;
         } */
        }

        private function rafraichirArbre(me:MouseEvent):void
        {
            grdLinesBloc.setData(null);
            if(orgaStructure._treeNodes.length() > 0)
            {
                orgaStructure.refreshTree();
            }
        }

        ////------------------------------ EXPORTS ---------------------------------------////        
        private function exporterGrid(me:MouseEvent):void
        {
            if(orgaStructure.treePerimetre.selectedIndex != -1)
            {
                exporterCSV();
            }
        }

        private function exporterCSV():void
        {
            displayExport("CSV");
        }

        private function exporterPDF():void
        {
            displayExport("PDF");
        }

        private function exporterODF():void
        {
            displayExport("ODF");
        }

        private function exporterXLS():void
        {
            displayExport("XLS");
        }

        private function displayExport(format:String):void
        {
            var url:String = modulePerimetreIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M22/exportUtils2.cfm";
            //var url:String = modulePerimetreIHM.urlBackoffice + "/fr/consotel/consoview/cfm/tools/exportOrgaStCSV.cfm";
            var variables:URLVariables = new URLVariables();
            variables.FORMAT = format;
            // variables.JOURNAL = _currentJournal;
            // variables.JOURNAL_LIBELLE = _currentJournalLibelle;
            // variables.PERIMETRE_LIBELLE = orgaStructure.cmbOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT;
            // variables.ID_ORGA = orgaStructure.getSelectedTreeItemValue();
            variables.ID_ORGA = _mainRef.getSelectedOrganisation();
            //variables.ID_CLIENT = orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT;
            variables.LIBELLE_ORGA = orgaStructure.treePerimetre.selectedItem.@LABEL;
			if(isNaN(_tempcurrentIdClient))
				variables.ID_CLIENT = orgaStructure.getSelectedTreeItemValue();
			else
				variables.ID_CLIENT = _tempcurrentIdClient;
            // variables.ID_PERIMETRE = orgaStructure.getSelectedTreeItemValue();
            variables.MODE = cbNotAssign.selected ? 0 : 1;
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = URLRequestMethod.POST;
            navigateToURL(request, "_blank");
            //var id:Number = orgaStructure.getSelectedTreeItemValue();
            //var mode:int = cbNotAssign.selected ? 0 : 1;
            //var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M22.exportUtils", "getExportOrgaCSV3", getExportOrgaCSV3Result);
            //RemoteObjectUtil.callService(op, id, mode);
        }

        private function getExportOrgaCSV3Result(event:ResultEvent):void
        {
            trace("getExportOrgaCSV3Result - lignes exportées");
        }

        //Fin Export ------------------------------------------------------------------------------
        /**############################### PUBLIC METHODS ########################### */ /** Set reference of the main side */
        public function setMainRef(ref:main):void
        {
            _mainRef = ref;
            if(_mainRef != null)
            {
                _refSet = true;
            }
            else
            {
                _refSet = false;
            }
            if(orgaStructure != null)
                orgaStructure.setMainRef(ref);
            invalidateProperties();
        }

        public function refreshLineGrid():void
        {
            if(_showLines == false)
                return;
            onbtAfficherLignesClicked(new Event("refresh"));
        }

        public function mainTreeModified():void
        {
            if(orgaStructure.getSelectedTypeOrga() == _mainRef.getSelectedTypeOrga())
                orgaStructure.refreshTree();
        }

        public function clearGridLines():void
        {
            grdLinesBloc.clearGridLines();
            _dataLines = null;
            _showLines = false;
            btExportGrid.enabled = false;
        }

        public function isLastSelectedChild():Boolean
        {
            var treeRef:Tree = orgaStructure.getTreeRef();
            if(treeRef.selectedIndex < 0)
                return false;
            var tmp:XML = XML(treeRef.selectedItem);
            if(tmp.children()[0] == null)
                return true;
            return false;
        }

        /**############################# END PUBLIC METHODS ######################### */
        private function myTreeDataTipFunction(item:Object):String
        {
            var coms:String = item.@COMMENTAIRES;
            var lbl:String = item.@LABEL;
            if((coms != null) && (coms.length > 0))
            {
                trace(lbl + " - " + coms);
                return lbl + " - " + coms;
            }
            else
            {
                trace(lbl);
                return lbl;
            }
        }

        // CONTEXT MENU ///---------------------
        private function throwError(result:FaultEvent):void
        {
        }
    }
}