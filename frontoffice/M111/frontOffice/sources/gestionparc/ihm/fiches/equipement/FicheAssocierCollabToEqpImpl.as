package gestionparc.ihm.fiches.equipement
{
	import flash.events.MouseEvent;
	
	import gestionparc.ihm.fiches.listedispo.DispoCollab;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.containers.TitleWindow;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;

	public class FicheAssocierCollabToEqpImpl extends TitleWindow
	{
		public var liste:DispoCollab;
		public var labDgSelection:Label;

		private var serv:ActionAssocierServices=new ActionAssocierServices();

		public function FicheAssocierCollabToEqpImpl()
		{
			super();
		}

		protected function btnValiderClickHandler():void
		{
			associerCollabToEqp();
		}

		protected function listeTermClickHandler(e:MouseEvent):void
		{
			if (liste.getIDSelection() > -1)
			{
				labDgSelection.text=liste.getLibelleSelection();
			}
		}

		protected function close(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		/* APPEL BACKOFFICE */
		private function associerCollabToEqp():void
		{
			var SendObj:Object=new Object();
//			serv.associerCollabToEqp();
		}
	}
}
