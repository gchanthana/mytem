<cfcomponent author="Cedric" hint="POC de chargement d'un parc MDM">
	<cffunction access="public" name="import" returntype="void" hint="Importe les devices enrollés d'un parc MDM">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant du groupe pour lequel les devices sont importés">
		<!--- v1 : Import synchrone --->
		<cfset var mdmInfos=getMdmServerInfos(ARGUMENTS.idGroupe)>
		<cfset var mdmVendor=mdmInfos.MDM_TYPE>
		<cfset var cfc="">
		<cfif compareNoCase(mdmVendor,"ZENPRISE") EQ 0>
			<cfset cfc="fr.saaswedo.api.tests.mdm.citrix.POC_MdmPool">
		<cfelseif compareNoCase(mdmVendor,"MOBILEIRON") EQ 0>
			<cfset cfc="fr.saaswedo.api.tests.mdm.mobileiron.POC_MdmPool">
		<cfelseif compareNoCase(mdmVendor,"AIRWATCH") EQ 0>
			<cfset cfc="fr.saaswedo.api.tests.mdm.airwatch.POC_MdmPool">
		<cfelse>
			<cfthrow type="Custom" message="Unknown/Unsupported MDM Provider '#mdmVendor#' for idGroupe #idRacine#">
		</cfif>
		<cfset var mdmPool=createObject("component",cfc)>
		<!--- Ajout de l'identifiant du groupe dans les infos MDM --->
		<cfset mdmInfos.IDGROUPE=ARGUMENTS.idGroupe>
		<cfset mdmPool.import(mdmInfos)>
	</cffunction>
	
	<cffunction access="public" name="getMdmServerUrl" returntype="string" hint="retourne l'url du serveur MDM">
		<cfargument name="idGroupe" type="Numeric" required="true">
		<cfreturn getMdmServerInfos(ARGUMENTS.idGroupe).mdm>
	</cffunction>
	
	<!--- Informations serveur MDM --->
	
	<cffunction access="private" name="getMdmServerInfos" returntype="Struct" hint="Retourne les infos du serveur MDM de la racine">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfset var idRacine=ARGUMENTS.idGroupe>
		<cfset var dataSource="ROCOFFRE">
		<!--- Récupération des infos MDM de la racine --->
		<cfset var procedureName="PKG_MDM.get_server_racine_v2">
		<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
			<cfprocresult name="qMdmServerInfos">
		</cfstoredproc>
		<cfif (qMdmServerInfos.recordcount GT 0) AND (procedure_status GT 0)>
			<!--- Code d'erreur --->
			<cfset var code="MDM.ERROR">
			<!--- Structure des infos MDM : La valeur ramenée de la propriété MDM doit etre de la forme "PROVIDER://URI" --->
			<cfset var mdmServerInfos={
				MDM_TYPE=qMdmServerInfos["MDM_TYPE"][1],MDM=qMdmServerInfos["MDM"][1],USE_SSL=yesNoFormat(VAL(qMdmServerInfos["USE_SSL"][1])),
				USERNAME=qMdmServerInfos["USERNAME"][1],PASSWORD=qMdmServerInfos["PASSWORD"][1],USERID=VAL(qMdmServerInfos["USERID"][1]),
				ENROLL_USERNAME=qMdmServerInfos["ENROLL_USERNAME"][1],ENROLL_PASSWORD=qMdmServerInfos["ENROLL_PASSWORD"][1],
				IS_DEFAULT=yesNoFormat(VAL(qMdmServerInfos["IS_DEFAULT"][1])),ADMIN_EMAIL=qMdmServerInfos["ADMIN_EMAIL"][1],
				TEST_MSG=qMdmServerInfos["TEST_MSG"][1],IS_TEST_OK=yesNoFormat(VAL(qMdmServerInfos["IS_TEST_OK"][1])),
				TOKEN=qMdmServerInfos["TOKEN"][1]
			}>
			<!--- Vérification de la valeur de la propriété MDM --->
			<cfset var mdmValue=mdmServerInfos.MDM>
			<cfif (TRIM(mdmValue) EQ "") OR (TRIM(mdmValue) NEQ mdmValue)>
				<cfset var extendedInfos=isDefined("qMdmServerInfos") ? serializeJSON(qMdmServerInfos):"">
				<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns invalid MDM '#mdmValue#' for idGroupe #idRacine#">
			<cfelse>
				<!--- Propriétés MDM et MDM_TYPE --->
				<cfset mdmServerInfos.MDM_TYPE=extractMdmProvider(mdmValue)>
				<cfset mdmServerInfos.MDM=extractMdm(mdmValue)>
				<!--- Traitement et conversion des valeurs (Le type Booléen de Java est utilisé pour compatibilité avec ActionScript) --->
				<cfset mdmServerInfos.IS_DEFAULT=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_DEFAULT ? "true":"false")>
				<cfset mdmServerInfos.USE_SSL=createObject("java","java.lang.Boolean").init(mdmServerInfos.USE_SSL ? "true":"false")>
				<cfset mdmServerInfos.IS_TEST_OK=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_TEST_OK ? "true":"false")>
				<cfreturn mdmServerInfos>
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns status #procedure_status# for idGroupe #idRacine#">
		</cfif>
	</cffunction>
		
	<cffunction access="private" name="extractMdmProvider" returntype="String" hint="Retourne la valeur du Provider MDM correspondant">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmProvider=mdmTokens[1]>
		<cfif TRIM(mdmProvider) NEQ mdmProvider>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmProvider#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid Provider #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn UCASE(mdmProvider)>
	</cffunction>
	
	<cffunction access="private" name="extractMdm" returntype="String" hint="Retourne l'URI d'accès au MDM sans le protocole (ou le Scheme)">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmValue=mdmTokens[2]>
		<cfif TRIM(mdmValue) NEQ mdmValue>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmValue#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid URI #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmValue>
	</cffunction>
	
	<cffunction access="private" name="extractMdmTokens" returntype="Array" hint="Retourne les éléments du découpage d'une la valeur MDM d'origine">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=listToArray(ARGUMENTS.mdm,"://",FALSE,TRUE)>
		<cfif arrayLen(mdmTokens) EQ 2>
			<cfreturn mdmTokens>
		<cfelse>
			<cfset var code="MDM.ERROR">
			<cfthrow type="Custom" errorCode="#code#" message="Cannot extract Provider and URI tokens from invalid MDM value '#ARGUMENTS.mdm#'">
		</cfif>
	</cffunction>
</cfcomponent>