package rapport.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import paginatedatagrid.PaginateDatagrid;
	
	import rapport.event.RapportEvent;
	import rapport.event.RapportIHMEvent;
	import rapport.service.chercherselector.RechercherSelectorService;
	import rapport.service.executerrapport.ExecuterRapportService;
	import rapport.utils.SelectorAbstract;
	import rapport.utils.SimpleFactory;
	import rapport.vo.ApiReportingSingleton;
	import rapport.vo.Rapport;
		public class RapportEtapeDeuxImpl extends VBox	{		[Bindable]public var executerRapportService : ExecuterRapportService;		[Bindable]public var rechercherSelectorService :RechercherSelectorService;		[Bindable]public var allSelector :ArrayCollection;		[Bindable]public var rapportDataGrid:PaginateDatagrid;				public var rapportParam:RapportParametreIHM;	    public var rapportInfo :RapportInfoImpl;		public var newComposant:SelectorAbstract;		public var tabComposantSelector:Array = new Array();		public var selectedRapport:Rapport;		[Bindable]public var fermerModule:Button;				public function RapportEtapeDeuxImpl()		{			super();			}				public function resetDataIHMSingleton():void		{			rapportParam.resetDataIHMEtape2();// re-initialiser les valeurs des composants lors qu'on fermer le popup sur l'etape 2		}				public function initData(selectedRapport:Rapport):void		{			if(allSelector!=null) // supprimer les composants ajoutés au rapportParam.formComposants par le factory			{				while(rapportParam.formComposants.numChildren >0)				{					var child:DisplayObject= rapportParam.formComposants.getChildAt(0); // l'index de l'element reste toujours 0 apères chaque suppression					rapportParam.formComposants.removeChildAt(0);					child=null;					}				tabComposantSelector=[];			}			rapportParam.valider_execution.enabled=true;						rechercherSelectorService= new RechercherSelectorService;				rechercherSelectorService.model.addEventListener(RapportEvent.USER_SELECTOR_UPDATED_EVENT,buildIHMHandler)			rechercherSelectorService.getSelector(selectedRapport.id);			rapportParam.labelRapportSelected.text=selectedRapport.nomLong;			rapportParam.labelSegmentAnalyse.text=selectedRapport.segemet;			this.selectedRapport=selectedRapport;			rapportInfo.selectedRapportUser=this.selectedRapport;		}				// creer IHM (un composant par selecteur )				public function buildIHMHandler(event:RapportEvent):void		{			allSelector=rechercherSelectorService.model.selectors;// contient tous les selecteur retourné du back office			var simpleFactory:SimpleFactory=new SimpleFactory();											for(var i:int=0; i<allSelector.length;i++) // creer les selector 					{				newComposant=simpleFactory.createComposants(allSelector[i]);// creer le selecteur				newComposant.initRapport(this.selectedRapport);								if(!newComposant.is_hiddenComposant)//ajouter le composant au IHM 				{					rapportParam.formComposants.addChild(newComposant)				}					tabComposantSelector.push(newComposant);// ajouter le composant dans un tableau 			}			}				public function rapportEtapeDeux_creationCompleteHandler(event:FlexEvent):void		{			rapportParam.addEventListener(RapportIHMEvent.VALIDER_CLICK_EVENT,validerClickHandler);							if(!ApiReportingSingleton.isPopUP)			{				fermerModule.visible=false;// masquer le button annuler dans l'ihm			}			else			{				fermerModule.visible=true;			}		}				//fontion qui permet d'envoyer les paramètres du rapport en vu de son execution 				private function validerClickHandler(evt:RapportIHMEvent):void		{			var tabParametre:Array = []; 
			var isOk:Boolean=true;
			var errorMessage:String="";			
			for(var i:int=0; i<allSelector.length;i++) 
			{
				tabParametre[i] = (tabComposantSelector[i] as SelectorAbstract).getSelectedValue();// récupérer les valeurs de chaque selector
				isOk=isOk && (tabComposantSelector[i] as SelectorAbstract).isOk;
				if(!isOk)
				{
					errorMessage=errorMessage+"\n"+(tabComposantSelector[i] as SelectorAbstract).errorMessage
				}
			}
			if(isOk)// pas d'erreur, on execute le rapport
			{
				executerRapportService=new ExecuterRapportService();
				executerRapportService.executeRapport(tabParametre,selectedRapport);// executer le rapport
			}			else			{
				ConsoviewAlert.afficherAlertInfo(errorMessage,'Alerte',null);			}		}				protected function retourEtape1_clickHandler(event:RapportIHMEvent):void		{			dispatchEvent(new RapportIHMEvent(RapportIHMEvent.VALIDER_RETOUR_EVENT)); // cette evenement est captué par le ModuleRapport		}		protected function fermerModule_clickHandler(event:RapportIHMEvent):void		{			dispatchEvent(new RapportIHMEvent(RapportIHMEvent.CLOSE_CLICK_EVENT));// cette evenement est captué par le ModuleRapport		}	}}