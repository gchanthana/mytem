package univers.facturation.grilletarif.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import univers.facturation.grilletarif.entity.GroupeProduitVO;
	import univers.facturation.grilletarif.event.GrilleTarifEvent;
	import univers.facturation.grilletarif.services.GrilleTarifService;
	import univers.facturation.grilletarif.utils.MonthChooser;	[Bindable]	public class AjoutVersionTarifImpl extends TitleWindow	{				public var txtPrixU : TextInput;		public var txtRemise : TextInput;		public var txtPrixURemise : TextInput;		//public var dfDateDebut : CvDateChooser;		public var btAjouter : Button;		public var btAnnuler : Button;		public var dfDateDebut : MonthChooser;		public var labelDate:Label;		public var dateFormatter : DateFormatter;		//Paramêtre pour le constructeur		public var serviceGrilleTarif : GrilleTarifService;		public var idRacine : int;				public var groupeProduit : GroupeProduitVO = new GroupeProduitVO;		public var groupeProduit_current : GroupeProduitVO;				public var idCompte:int = 0;		public var idSousCompte : int = 0;				public function AjoutVersionTarifImpl()		{			super();		}				public function initIHM():void		{			// dfDateDebut.selectedDate = dfDateDebut.selectedDate; //Force la première selection			dfDateDebut.selectedDate = new Date();			labelDate.text = dateFormatter.format(dfDateDebut.selectedDate)		}		private var chosenDate:Date = new Date();		private var chosenMonth:Date = new Date();						public function updateDates():void			{				chosenMonth = dfDateDebut.selectedDate;			}						protected function txtRemiseChangeHandler(event : Event):void		{	
			txtPrixU.text = txtPrixU.text.replace(",",".");
			txtRemise.text = txtRemise.text.replace(",",".");
			txtPrixURemise.text = txtPrixURemise.text.replace(",",".");
						var prixRemise : Number = Number(txtPrixU.text) - (Number(txtPrixU.text) * Number(txtRemise.text) / 100);			txtPrixURemise.text = prixRemise.toFixed(4);
					}		protected function txtPrixURemiseChangeHandler(event : Event):void{			txtPrixU.text = "0";			txtRemise.text = "0";		}		protected function btAjouterClickHandler(me : MouseEvent):void{			Alert.yesLabel = ResourceManager.getInstance().getString('M32', 'Oui');			Alert.cancelLabel = ResourceManager.getInstance().getString('M32', 'Annuler');			Alert.show(ResourceManager.getInstance().getString('M32', 'Confirmez_vous_cette_op_ration_avec_d_bu')+labelDate.text+ResourceManager.getInstance().getString('M32', '__'),"Confirmer",Alert.YES | Alert.CANCEL,this,confirmerAjout);			}		private function confirmerAjout(cle : CloseEvent):void{			var message : String = "";			var bool : Boolean = true;			if (cle.detail == Alert.YES){				if (txtPrixU.text.length == 0){					message = message + ResourceManager.getInstance().getString('M32', '__Le_prix_unitaire_n');					bool = false;				}				if (txtRemise.text.length == 0){					message = message + ResourceManager.getInstance().getString('M32', '__La_remise_n');					bool = false				}				if (!dfDateDebut.selectedDate){					message = message + ResourceManager.getInstance().getString('M32', '__La_date_de_d_but_de_validit__n');					bool = false				}								if	(bool){															var typeTarif : String = "R";					if(idCompte>0)					{						typeTarif = "C";					}					if(idSousCompte>0)					{						typeTarif = "S";					}										//var date : Date = new Date(dfDateDebut.text.substr(3,4),Number(dfDateDebut.text.substr(0,2))-1);										var date : Date = dfDateDebut.selectedDate;														serviceGrilleTarif.addVersionTarif(groupeProduit_current.id_groupe_produit,					idRacine,					typeTarif,					idCompte,					idSousCompte,					date,					Number(txtPrixU.text),					Number(txtPrixURemise.text),					Number(txtRemise.text)); 					PopUpManager.removePopUp(this);				}				else				{					Alert.show(message,ResourceManager.getInstance().getString('M32', 'Les_champs_suivants_sont_obligatoires'));				}									}else{				//On fait rien pour le moment				}		}		public function addVersionTarifHandler(evt : GrilleTarifEvent):void		{					}		public function fermer(evt : Event):void		{			PopUpManager.removePopUp(this);		}	  }}