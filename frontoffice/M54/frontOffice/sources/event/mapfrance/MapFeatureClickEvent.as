package event.mapfrance
{
	import flash.events.Event;
	
	import ilog.maps.MapFeature;
	
	public class MapFeatureClickEvent extends Event
	{
		private var _mapF:MapFeature;
		
		//bubbles indiquent si l'evenement peut être propagé, cancelable s'il peut être arrêté en cours de propagation
		public function MapFeatureClickEvent(type:String, mf:MapFeature, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_mapF=mf;
		}
		
		override public function clone():Event
		{	
			return new MapFeatureClickEvent( type, mf, bubbles,cancelable);	
		}

		public function get mapF():MapFeature
		{
			return _mapF;
		}

		public function set mapF(value:MapFeature):void
		{
			_mapF = value;
		}

	}
}