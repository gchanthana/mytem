package event
{
	import flash.events.Event;
	
	import vo.DestinataireImport;

	public class RemoveDestinataireEvent extends Event
	{

		public var dest:DestinataireImport;
		
		public static const DATA_TRANSFER:String = "data_transfer";
		
		public function RemoveDestinataireEvent(type:String,dest:DestinataireImport,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable)
			this.dest=dest;
		}
		
		override public function clone():Event
		{
			return new RemoveDestinataireEvent(type,dest,bubbles,cancelable);
		}
	}
}
