package catalogue.profil
{
	import catalogue.vo.RevendeurVO;

	[Bindable]
	public class GestionProfil
	{
		
		public static const CONSOTEL_PROFIL : String = "consotelView";
		public static const DISTRIBUTEUR_PROFIL : String = "distributeurView";
		public static const CLIENT_PROFIL : String = "clientView";
		
		
		private var _CURRENT_PROFIL : String;
		private var _ID_CURRENT_PROFIL:Number;
		private var _LIBELLE_CURRENT_PROFIL:String;
		private var _REVENDEUR:RevendeurVO;
		
		public function GestionProfil()
		{
		}
		public function set ID_CURRENT_PROFIL(value:Number):void
		{
			_ID_CURRENT_PROFIL = value;
		}

		public function get ID_CURRENT_PROFIL():Number
		{
			return _ID_CURRENT_PROFIL;
		}
		public function set CURRENT_PROFIL(value:String):void
		{
			_CURRENT_PROFIL = value;
		}

		public function get CURRENT_PROFIL():String
		{
			return _CURRENT_PROFIL;
		}
		public function set LIBELLE_CURRENT_PROFIL(value:String):void
		{
			_LIBELLE_CURRENT_PROFIL = value;
		}

		public function get LIBELLE_CURRENT_PROFIL():String
		{
			return _LIBELLE_CURRENT_PROFIL;
		}
		
		public function set REVENDEUR(value:RevendeurVO):void
		{
			_REVENDEUR = value;
		}
		
		public function get REVENDEUR():RevendeurVO
		{
			return _REVENDEUR;
		}

	}
}