<!---
Package : fr.consotel.consoview.facturation.rapports.JournalInventaireLignes
--->
<cfcomponent name="RapportJournalInventaireLignesStrategy">
	<cffunction name="getReportData" access="private" returntype="any">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset ID_ORGA = ID_PERIMETRE>
        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_CYCLEVIE_V3.LISTEINVENTAIRE">
	        <cfprocparam value="#ID_ORGA#" cfsqltype="CF_SQL_INTEGER">
 	        <cfprocparam  value="#RapportParams.DATEDEB#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset rapportParams.chaine_date = rapportParams.DATEDEB>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEFIN)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEFIN)>
		<cfset rapportParams.DATEFIN =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>
		<cfif #qGetData.recordcount# gt 0>
			<!---
			<cfset MAX_ROWS = 50000>
			<cfset NB_COL = qGetData['NB_COL'][1]>
			--->
   			<cfinclude template="./journalInventaireLignes_xls.cfm">
		<cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes donnÃ©es rÃ©pondant ï¿½ votre demande</h2></strong></center>
			</cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
