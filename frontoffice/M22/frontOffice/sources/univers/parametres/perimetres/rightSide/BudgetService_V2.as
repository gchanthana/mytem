package univers.parametres.perimetres.rightSide
{
	import mx.resources.ResourceManager;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class BudgetService_V2
	{

		/**************************************
		 *			VARIABLES PRIVEES
		 **************************************/

		private var _myBudgetEnfant_V2:BudgetEnfantImpl_V2;
		private var myParent : BudgetParentImpl_V2; // pour fermer la popup dans le resultHandler
		
		public function BudgetService_V2(instanceMyBudgetEnfant_V2 : BudgetEnfantImpl_V2)
		{
			myBudgetEnfant_V2 = instanceMyBudgetEnfant_V2;
		}
			
		/**************************************
		 *			GETTER / SETTER
		 **************************************/

		public function set myBudgetEnfant_V2(value:BudgetEnfantImpl_V2):void
		{
			_myBudgetEnfant_V2 = value;
		}

		public function get myBudgetEnfant_V2():BudgetEnfantImpl_V2
		{
			return _myBudgetEnfant_V2;
		}
		
		/**************************************
		 *				REMOTE 
		 **************************************/

		public function btDistributeClicked(event:MouseEvent):void
		{
			var idGroupe:Number = myBudgetEnfant_V2.mainRef.getSelectedNode();
			var idBudget:Number = myBudgetEnfant_V2.resultData[myBudgetEnfant_V2.currentExercice].IDBUDGET_EXERCICE;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreBudget",
																				"distributeNodeBudget",
																				distributeNodeBudgetResult
																				);				
			RemoteObjectUtil.callService(op, idGroupe, idBudget);	
		}
		
		public function getExerciceData():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreBudget",
																				"getExerciceList",
																				getExerciceDataResult);				
			RemoteObjectUtil.callService(op, idGroupeMaitre);	
		}		

		public function getNodeBudget():void
		{
			if (myBudgetEnfant_V2.myBudgetUtils.refreshBudgetItems())
			{
				myBudgetEnfant_V2.fiCurrentNode.label = myBudgetEnfant_V2.mainRef.getSelectedItemNode().@LABEL;
				var idGroupe:Number = myBudgetEnfant_V2.mainRef.getSelectedNode();
				var idBudget:Number = myBudgetEnfant_V2.resultData[myBudgetEnfant_V2.currentExercice].IDBUDGET_EXERCICE;
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.parametres.perimetres.gestionPerimetreBudget",
																					"getNodesBudget",
																					getNodeBudgetResult);				
				RemoteObjectUtil.callService(op, idGroupe, idBudget);	
			}
		}
		
		public function updateNodeBudget():void
		{
			var idGroupe:Number = myBudgetEnfant_V2.mainRef.getSelectedNode();
			var idBudget:Number = myBudgetEnfant_V2.resultData[myBudgetEnfant_V2.currentExercice].IDBUDGET_EXERCICE;
			var budget:String = myBudgetEnfant_V2.myBudgetUtils.cleanTxtInput(myBudgetEnfant_V2.tiCurrentbudget.text);
			var min:String = budget; // cleanTxtInput(txtCurrentmin.text);
			var max:String = budget; // cleanTxtInput(txtCurrentmax.text);
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.parametres.perimetres.gestionPerimetreBudget",
																			"updateNodeBudget",
																			updateNodeBudgetResult);				
			RemoteObjectUtil.callService(op, idGroupe, idBudget, min, max, budget);	
		}

		// parent
		
		public function updateExerciceData(parent : BudgetParentImpl_V2):void
		{
		 	var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var idBudget:Number = myBudgetEnfant_V2.resultData[myBudgetEnfant_V2.currentExercice].IDBUDGET_EXERCICE;
			var name:String = parent.tiName.text;
			var comment:String = parent.taComment.text;
			
			var budget:String = myBudgetEnfant_V2.myBudgetUtils.cleanTxtInput(parent.tiBudget.text);
			var min:String = budget; //cleanTxtInput(tiMin.text);
			var max:String =  budget; //cleanTxtInput(tiMax.text);
			var date_start:String = myBudgetEnfant_V2.myBudgetUtils.getDateWithFormat(parent.dtStart.selectedDate);
			var date_end:String = myBudgetEnfant_V2.myBudgetUtils.getDateWithFormat(parent.dtEnd.selectedDate);
 			myParent = parent;
 			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreBudget",
																				"updateExercice",
																				updateExerciceResult);				
			RemoteObjectUtil.callService(op, idGroupeMaitre, idBudget, name, comment, min, max, budget,
										date_start, date_end);	 
		}
		
		public function addExercice(parent : BudgetParentImpl_V2):void
		{
	 		var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var name:String = parent.tiName.text;
			var comment:String = parent.taComment.text;
			
			var budget:String = myBudgetEnfant_V2.myBudgetUtils.cleanTxtInput(parent.tiBudget.text);
			var min:String = budget; //cleanTxtInput(tiMin.text);
			var max:String =  budget; //cleanTxtInput(tiMax.text);
			var date_start:String = myBudgetEnfant_V2.myBudgetUtils.getDateWithFormat(parent.dtStart.selectedDate);
			var date_end:String = myBudgetEnfant_V2.myBudgetUtils.getDateWithFormat(parent.dtEnd.selectedDate);
 			myParent = parent;
 			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreBudget",
																				"addExercice",
																				addExerciceResult);				
			RemoteObjectUtil.callService(op, idGroupeMaitre, name, comment, min, max, budget,
										date_start, date_end);	  
		}
		
// HANDLER
		private function distributeNodeBudgetResult(event:ResultEvent):void
		{
			getNodeBudget();
		}

		private function getNodeBudgetResult(event:ResultEvent):void
		{
			myBudgetEnfant_V2.nodeData = event.result;
			myBudgetEnfant_V2.tiCurrentbudget.text = myBudgetEnfant_V2.myBudgetUtils.formatAmount(myBudgetEnfant_V2.myBudgetUtils.getAroundValue(2, "MONTANTBUDGET"));
			if (myBudgetEnfant_V2.mainRef.getSelectedItemNode())
			{
				if (myBudgetEnfant_V2.mainRef.getSelectedItemNode().@NIV == "2")
				{
					var obj:Object = {MONTANTMINI: myBudgetEnfant_V2.resultData[myBudgetEnfant_V2.currentExercice].MONTANTMINI,
										MONTANTMAXI: myBudgetEnfant_V2.resultData[myBudgetEnfant_V2.currentExercice].MONTANTMAXI,
										MONTANTBUDGET: myBudgetEnfant_V2.resultData[myBudgetEnfant_V2.currentExercice].MONTANTBUDGET};
					(myBudgetEnfant_V2.nodeData[0] as ArrayCollection).addItem(obj);
	
				}
				
			}
			myBudgetEnfant_V2.myBudgetUtils.refreshTxtValues();
		} 

		private function getExerciceDataResult(event:ResultEvent):void
		{
			myBudgetEnfant_V2.resultData = ArrayCollection(event.result);
			myBudgetEnfant_V2.currentExercice = myBudgetEnfant_V2.resultData.length -1;
			myBudgetEnfant_V2.myBudgetUtils.refreshExerciceView();
			getNodeBudget();
		}
		
		
		private function updateNodeBudgetResult(event:ResultEvent):void
		{
			myBudgetEnfant_V2.btMAJNode.enabled = false;
		}
		
		private function updateExerciceResult(event:ResultEvent):void
		{
			if(event.result != -1)
			{
				getExerciceDataResult(event) // maj des infos
				myParent.closePopup(null);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M22', 'La_mise___jour_de_l_exercice_n_a_pas__t_'),ResourceManager.getInstance().getString('M22', 'Erreur'));
			}
		}
		
		private function addExerciceResult(event : ResultEvent):void
		{
			if(event.result != -1)
			{
				getExerciceDataResult(event) // maj des infos
				myParent.closePopup(null);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M22', 'Le_nouvel_exercice_n_a_pas__t__cr_e___'),ResourceManager.getInstance().getString('M22', 'Erreur'));
			}
		}
		
	}
}