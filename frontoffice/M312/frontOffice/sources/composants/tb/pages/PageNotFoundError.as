package composants.tb.pages
{
	import mx.resources.ResourceManager;

	public class PageNotFoundError extends Error
	{
		public function PageNotFoundError(message:String="", id:int=0)
		{
			
			name = "PageNotFoundError";
			message = ResourceManager.getInstance().getString('M312', 'La_page_n_existe_pas');
			id = 1;
			super(message, id);
		}
		
	}
}