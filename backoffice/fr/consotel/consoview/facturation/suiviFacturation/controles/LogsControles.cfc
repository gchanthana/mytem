<cfcomponent output="false" alias="fr.consotel.consoview.facturation.suiviFacturation.controles.LogsControles">

	<cfparam name="IdRacine" default="#SESSION.PERIMETRE.ID_GROUPE#"  type="numeric">
<!--- 	<cfparam name="IdRacine" default="#1234#"  type="numeric"> --->
	<cfparam name="IdUser" default="#SESSION.user.CLIENTACCESSID#"  type="numeric"> 

<!--- Procedure pour avoir les droits  --->
<cffunction name="getEtatButtons" output="false" access="public" returntype="Query">
	<cfargument name="IdProfil" type="Numeric" required="true">
	<cfargument name="IdInventairePeriode" type="Numeric" required="true">
	 
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.getShowHiddenBt_v3"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IdRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil" value="#IdProfil#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#IdInventairePeriode#"> 
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Procedure pour insérer un log sur une actiond e controle de facture --->
<cffunction name="setLogsControleFacture" output="false" access="public" returntype="Numeric">
	<cfargument name="IdInventairePeriode" type="Numeric" required="true">
	<cfargument name="IdProfil" type="Numeric" required="true">
	<cfargument name="Action" type="String" required="true">
	<cfargument name="Commentaire" type="String" required="true">

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.insertLog"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#IdInventairePeriode#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_user" value="#IdUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil" value="#IdProfil#"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_action" value="#Action#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_commentaire" value="#Commentaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idorganisation" value="0" > 
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" variable="p_montant_ana" value="0" null="true"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" variable="p_montant_na" value="0" null="true"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IdRacine#"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_pourcentage" value="0" null="true"> 
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>

	<cfreturn p_retour>
</cffunction>

<!--- Procedure pour insérer un log sur une action de controle de facture --->
<cffunction name="setLogsControleFactureAnalytique" output="false" access="public" returntype="Numeric">
	<cfargument name="ArrayParameters" type="Array" required="true">
	<cfargument name="IdInventairePeriode" type="Numeric" required="true">
	<cfargument name="IdProfil" type="Numeric" required="true">
	<cfargument name="Action" type="String" required="true">
	<cfargument name="Commentaire" type="String" required="true">
	
	<cfset index = 1>
	<cfset checkOK = 0>
	<cfloop index="idx" from="1" to="#ArrayLen(ArrayParameters)#">

		<cfif checkOK EQ 0>
				<cfset IdOrganisation = ArrayParameters[index]>
				<cfset index = index+1>
				<cfset MontantAna = ArrayParameters[index]>
				<cfset index = index+1>
				<cfset MontantNa = ArrayParameters[index]>
				<cfset index = index+1>
				<cfset Pourcent = ArrayParameters[index]>
				<cfset index = index+1>
			
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.insertLog"> 
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#IdInventairePeriode#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_user" value="#IdUser#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idprofil" value="#IdProfil#"> 
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_action" value="#Action#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_commentaire" value="#Commentaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idorganisation" value="#IdOrganisation#"> 
						<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_montant_ana" value="#MontantAna#">
						<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_montant_na" value="#MontantNa#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IdRacine#"> 
						<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" variable="p_pourcentage" value="#Pourcent#"> 
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
					</cfstoredproc>
		</cfif>
		
		<cftry>
			<cfset MontantNa = ArrayParameters[index]> 
			<cfset checkOK = 0>
		<cfcatch> <cfset checkOK = -1></cfcatch>
		</cftry>  	
	</cfloop>
	
	<cfreturn p_retour>
</cffunction>

<!--- Procedure pour avoir le log de controle d'une facture  --->
<cffunction name="getLogsControleFacture" output="false" access="public" returntype="Query">
	<cfargument name="IdInventairePeriode" type="Numeric" required="true">

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.getLog"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#IdInventairePeriode#">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<cffunction name="getLogVisaAnalytique" output="false" access="public" returntype="Query">
	 <cfargument name="IdInventairePeriode" type="Numeric" required="true">
		<cfset logs = getLogsControleFacture(IdInventairePeriode)>
		<cfquery dbtype="query" name="p_retour">
		
			SELECT * FROM logs WHERE lower([ACTION]) LIKE '%'||'analytique'||'%' ORDER BY DATE_ACTION
		
		</cfquery>
		
	<cfreturn p_retour>
</cffunction>

<!--- Procedure pour avoir idProfile  --->
<cffunction name="getUserConnectedProfile" output="false" access="public" returntype="numeric">

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.getUserProfil"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_iduser" value="#IdUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#IdRacine#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

<!--- Procedure pour avoir si l'export est à faire ou non --->
<cffunction name="getExportDecision" output="false" access="public" returntype="numeric">
	<cfargument name="idInventairePeriode" type="Numeric" required="true">
	<cfargument name="etat" type="Numeric" required="true">
	<cfargument name="commentaires" type="string" required="true">
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.cf_exportee_dec"> 
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#idInventairePeriode#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_etat" value="#etat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_commentaires" value="#commentaires#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
	<cfreturn p_retour>
</cffunction>

</cfcomponent>