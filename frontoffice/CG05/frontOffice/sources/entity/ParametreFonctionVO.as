package entity
{
	public class ParametreFonctionVO
	{
		
		private var _idParametreFonction	:Number;
		private var _libelle				:String;
		private var _description			:String;
		private var _idFonction				:Number;
		private var _nivSaaswedo			:String;
		private var _nivAppMaster			:String;
		private var _nivPartenaire			:String;
		private var _nivClient				:String;
		
		public function ParametreFonctionVO()
		{
		}
		
		public function fill(obj:Object,fillClient:Boolean=true):void
		{
			try
			{
				if(obj.hasOwnProperty("IDPARAMETRE_FONCTION"))
					this.idParametreFonction = obj.IDPARAMETRE_FONCTION;
				if(obj.hasOwnProperty("NOM_PARAMETRE"))
					this._libelle = obj.NOM_PARAMETRE;
				if(obj.hasOwnProperty("DESCRIPTION"))
					this._description = obj.DESCRIPTION;
				if(obj.hasOwnProperty("IDFONCTION"))
					this.idFonction = obj.IDFONCTION;
				if(obj.hasOwnProperty("NIV1"))
					this.nivSaaswedo = obj.NIV1;
				if(obj.hasOwnProperty("NIV2"))
					this.nivAppMaster = obj.NIV2;
				if(obj.hasOwnProperty("NIV3"))
					this.nivPartenaire = obj.NIV3;
				if(obj.hasOwnProperty("NIV4"))
					this.nivClient = obj.NIV4;
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet ParametreFonction erronée ");
			}
		}
		
		[Bindable]
		public function get description():String { return _description; }
		
		public function set description(value:String):void
		{
			if (_description == value)
				return;
			_description = value;
		}
		
		[Bindable]
		public function get libelle():String { return _libelle; }
		
		public function set libelle(value:String):void
		{
			if (_libelle == value)
				return;
			_libelle = value;
		}
		
		public function get idFonction():Number { return _idFonction; }
		
		public function set idFonction(value:Number):void
		{
			if (_idFonction == value)
				return;
			_idFonction = value;
		}
		
		[Bindable]
		public function get nivClient():String { return _nivClient; }
		
		public function set nivClient(value:String):void
		{
			if (_nivClient == value)
				return;
			_nivClient = value;
		}
		
		[Bindable]
		public function get nivPartenaire():String { return _nivPartenaire; }
		
		public function set nivPartenaire(value:String):void
		{
			if (_nivPartenaire == value)
				return;
			_nivPartenaire = value;
		}
		
		[Bindable]
		public function get nivAppMaster():String { return _nivAppMaster; }
		
		public function set nivAppMaster(value:String):void
		{
			if (_nivAppMaster == value)
				return;
			_nivAppMaster = value;
		}
		
		[Bindable]
		public function get nivSaaswedo():String { return _nivSaaswedo; }
		
		public function set nivSaaswedo(value:String):void
		{
			if (_nivSaaswedo == value)
				return;
			_nivSaaswedo = value;
		}
		
		public function get idParametreFonction():Number { return _idParametreFonction; }
		
		public function set idParametreFonction(value:Number):void
		{
			if (_idParametreFonction == value)
				return;
			_idParametreFonction = value;
		}
		
	}
}