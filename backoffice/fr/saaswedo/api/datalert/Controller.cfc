<cfcomponent output="true"  extends="fr.saaswedo.api.datalert.Core">
	
	<cffunction name="setMethod" access="public" output="true" returntype="any">
		
		<cfargument name="nomMethode" type="string" required="true">
		<cfargument name="httpRequestData" type="struct" required="true">
	
		<cfset invokeMethod="">
		<cfset invokeComponent="">
		<cfset uuid =createuuid() >
		
		<cftry>
		
			<cfswitch expression="#Trim(nomMethode)#"> 
			   
				<cfcase value="register_ios">
					<cfset invokeMethod="register_ios">
					<cfset invokeComponent="fr.saaswedo.api.datalert.register.Register">
		    	</cfcase>
			    <cfcase value="register_android">
					<cfset invokeMethod="register_android">
					<cfset invokeComponent="fr.saaswedo.api.datalert.register.Register">
				</cfcase>
				<cfcase value="verify_user">
					<cfset invokeMethod="verify_user">
					<cfset invokeComponent="fr.saaswedo.api.datalert.register.Verify">
				</cfcase>
				<cfcase value="roaming_detection">
					<cfset invokeMethod="roaming_detection">
					<cfset invokeComponent="fr.saaswedo.api.datalert.datamanager.RoamingDetection">
				</cfcase>
				<cfcase value="get_planinfo">
					<cfset invokeMethod="get_planinfo">
					<cfset invokeComponent="fr.saaswedo.api.datalert.register.GetPlanInfo">
				</cfcase>
				<cfcase value="add_bulk_usage">
					<cfset invokeMethod="add_bulk_usage">
					<cfset invokeComponent="fr.saaswedo.api.datalert.datamanager.AddBulkUsage">
				</cfcase>
				<cfcase value="add_real_time_usage">
					<cfset invokeMethod="add_real_time_usage">
					<cfset invokeComponent="fr.saaswedo.api.datalert.datamanager.AddRealTimeUsage">
				</cfcase>
				<cfcase value="add_app_usage">
					<cfset invokeMethod="add_app_usage">
					<cfset invokeComponent="fr.saaswedo.api.datalert.datamanager.AddAppUsage">
				</cfcase>
				<cfcase value="get_config">
					<cfset invokeMethod="get_config">
					<cfset invokeComponent="fr.saaswedo.api.datalert.register.GetConfiguration">
				</cfcase>
									    
			</cfswitch> 
			
			<cflog  text="La methode appele est :  #invokeMethod# de la classe #invokeComponent# " type="information" application="no">
			
			<cfif compareNoCase(httpRequestData.method,"POST") eq 0 > <!---  test if post request --->
		
				<cfset jsString=toString(httpRequestData.content)>
				
				<cfset jsString=geTtoken(urldecode(jsString),2,"=")> <!---  supprimer data= --->
				
				<!---  une autre façon  --->
				<!--- 
				<cfif isbinary(form.data)>
					<cfset  jsString = toString(form.data)><!--- from.data donne la valeur = json  --->
					<cfelse>
						<cfset  jsString = form.data>
				</cfif>
				 --->
				<cfset user_agent ="">
				<cfset ip_origin ="">
				
				<cfif structKeyExists(CGI,"HTTP_USER_AGENT") and CGI.HTTP_USER_AGENT neq "">
					<cfset user_agent = lcase(CGI.HTTP_USER_AGENT)> <!---Initialisation + lowercase--->
				</cfif>
				
				 <cfif structKeyExists(CGI,"HTTP_COOKIE") and CGI.HTTP_COOKIE neq ""> <!--- pour récupérer ip_origin --->
					<cfset  httpCookie= #HTTP_COOKIE# >
					<cfset TabHttpCookie = httpCookie.split(";")>
					
					<cfloop array="#TabHttpCookie#" index="i" >
						<cfif findNOcase("ip_origin",i) gt 0>
							<cfset ip=i.split("=")>
							<cfset ip_origin= urldecode(#ip[2]#)>
						</cfif>
					</cfloop>
				</cfif>
				
				<!--- 
					<cfif isDefined("COOKIE")>
						<cfif structKeyExists(COOKIE,"IP_ORIGINE")>
						<cfset remoteHost="#COOKIE.IP_ORIGINE# (IP_ORIGINE) ">
					</cfif>
				 --->
				
				<cfif isjson(jsString)> <!---  tester format json --->
					<cflog  text="Requete de type POST et json au bon format #jsString#" type="information">
					<cfinvoke method="#invokeMethod#" component="#invokeComponent#" returnvariable="responseHttp">
						<cfinvokeargument name="httpRequestData" value="#jsString#">
						<cfinvokeargument name="user_agent" value="#user_agent#">
						<cfinvokeargument name="ip_origin" value="#ip_origin#">	
					</cfinvoke>
					<!---  supprimer les espaces dans la réponse --->
					<cfprocessingdirective suppressWhitespace="true">
						<cfcontent reset="true" type="text/plain">
						<cfoutput>#trim(responseHttp)#</cfoutput>
					</cfprocessingdirective>				
				<cfelse>
					<cfheader name="status" value="421">
					<cfset responseHttp = '{"resp":"Invalid json data format"}'>
					<cflog  text="Invalid json data format #jsString#" type="information">
					<cfset saveLog(uuid,"#responseProxy# setMethod","#trim(responseHttp)# #newLine#  data = #jsString#",idMntEventProxyMobile) >
					<cfprocessingdirective suppressWhitespace="true">
						<cfcontent reset="true" type="text/plain">
						<cfoutput>#trim(responseHttp)#</cfoutput>
					</cfprocessingdirective>
				</cfif>
			<cfelse>
				<cfheader name="status" value="420">
				<cfset responseHttp = '{"resp":"Invalid request"}'>
				<cflog  text="Invalid request - verifier si la methode est de type post " type="information">
				<cfset saveLog(uuid,"#responseProxy# setMethod","#trim(responseHttp)#",idMntEventProxyMobile) >
				<cfprocessingdirective suppressWhitespace="true">
					<cfcontent reset="true" type="text/plain">
					<cfoutput>#trim(responseHttp)#</cfoutput>
				</cfprocessingdirective>
			</cfif>	
		<cfcatch>
			<cfsavecontent variable="contenuHTTP">
				<cfdump var="#httpRequestData#" format="text">
			</cfsavecontent>
			<cfset body=bodyException(CFCATCH)>
			<cfset body=body & "<hr>" & contenuHTTP & "<hr>">
			<cfset saveLog(uuid,"#exce# setMethod",body,idMntEventProxyException) >
			<cfoutput>{"resp":"#cfcatch.Message#"}</cfoutput>
		</cfcatch>
		</cftry>		
		
	</cffunction>
	
</cfcomponent>