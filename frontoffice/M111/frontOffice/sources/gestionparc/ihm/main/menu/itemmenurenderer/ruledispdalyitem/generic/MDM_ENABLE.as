package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class MDM_ENABLE implements IRuleDisplayItem
	{
		public function MDM_ENABLE()
		{
		}

		public function isDisplay():Boolean
		{
			if (moduleGestionParcIHM.globalParameter.racineInfos && moduleGestionParcIHM.globalParameter.racineInfos.length > 0)
			{
				if ((moduleGestionParcIHM.globalParameter.racineInfos[0].BOOL_MDM == 1))
					return true;
				else
					return false;
			}
			else
				return false;
		}
	}
}
