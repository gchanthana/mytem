package univers.inventaire.inventaire
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Event(name="initActionComplete")]
	[Event(name="profilsLoaded")]
	[Event(name="listeActionsProfilLoaded")]
	
	
	[Bindable]
	public class GestionDroitCycleDeVie extends EventDispatcher
	{
		public static const CODE_CYCLE_DE_VIE:String = "CYV";
		
		public static const CREER_CMDE:Number = 29;
		public static const CREER_RESI:Number = 9;
		public static const CLOTURER_SANS_LETTRAGE:Number = 21;
		public static const CLOTURER_AVEC_LETTRAGE:Number = 23;
		
		private var _listeProfils:ArrayCollection;
		private var _listeActionsPossibles:ArrayCollection;
		private var _boolCtrlGestion:Boolean = false;
		private var _boolPrepareCreation:Boolean = false;
		private var _boolPrepareResiliation:Boolean = false;
		private var _boolCloturerAvecLettrage:Boolean = false;
		private var _boolCloturerSansLettrage:Boolean = false;
		private var _selectedProfil:Object;
		
			
		public function GestionDroitCycleDeVie(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function fournirListeActionsPossibles(idpool:Number, idEtat:Number = 1):void
		{	
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.commande.GestionWorkFlow",
																		"fournirListeActionsPossibles",
																		fournirListeActionsPossiblesResultHandler);
			RemoteObjectUtil.callService(op,idEtat,idpool);
		}
		
		private function fournirListeActionsPossiblesResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				var values:ArrayCollection = event.result as ArrayCollection;							
				var cursor : IViewCursor = values.createCursor();
				listeActionsPossibles =  new ArrayCollection();
				
				while(!cursor.afterLast)
				{	
					listeActionsPossibles.addItem(cursor.current);
					cursor.moveNext()
				}
				
				dispatchEvent(new Event("actionsPossibleLoaded"));
				
			}
		}
		
		private function initActionsCycleDeVie(idProfile:Number):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"fournirListeActProfil",
																	initActionsCycleDeVieResultHandler);
				

			RemoteObjectUtil.callService(op,idProfile);	
		}
		
		private function initActionsCycleDeVieResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				var values:ArrayCollection = event.result as ArrayCollection;							
				var cursor : IViewCursor = values.createCursor();
				
				boolPrepareCreation = false;
				boolPrepareResiliation = false;
				boolCloturerAvecLettrage = false;
				boolCloturerSansLettrage = false;
				
				while(!cursor.afterLast)
				{	
					var idaction:Number = cursor.current.IDINV_ACTIONS;
					
					switch(idaction)
					{
						case CREER_CMDE :
						{
							boolPrepareCreation = true;
							break;
						}
						case CREER_RESI :
						{
							boolPrepareResiliation = true;
							break;
						}
						case CLOTURER_AVEC_LETTRAGE :
						{
							boolCloturerAvecLettrage = true;
							break;
						}
						case CLOTURER_SANS_LETTRAGE :
						{
							boolCloturerSansLettrage = true;
							break;
						}
					}
					cursor.moveNext();
				}
				dispatchEvent(new Event("initActionComplete"));
				
			}
		}
		
		//Charge la liste des profils et les actions possible si un seul pool 
		public function getProfiles(boolInit:Boolean=false):void
		{	
			
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"fournirPoolsDuGestionnaire",
																		function getProfilesResultHandler(event : ResultEvent):void
																		{
																			if(event.result )
																			{	
																					var values:ArrayCollection = event.result as ArrayCollection;							
																					var cursor : IViewCursor = values.createCursor();
																					listeProfils =  new ArrayCollection();
																					
																					boolCtrlGestion = false;
																					while(!cursor.afterLast)
																					{	
																						if(cursor.current.CODE_INTERNE_POOL == "CYV")
																						{
																							listeProfils.addItem(cursor.current);
																						}
																						cursor.moveNext();
																					}
																					if(listeProfils.length == 1)
																					{
																						
																						selectedProfil = listeProfils[0];
																						boolCtrlGestion = (Number(listeProfils[0].GEST_BUDGET) == 0)?false:true;
																						
																						if(boolInit)
																						{
																							initActionsCycleDeVie(listeProfils[0].IDPROFIL);	
																						}
																						
																					}
																					
																					dispatchEvent(new Event("profilsLoaded"));
																			}
																		});
			RemoteObjectUtil.callService(op);
		}
		
		
		 
		
		

		public function set listeProfils(value:ArrayCollection):void
		{
			_listeProfils = value;
		}

		public function get listeProfils():ArrayCollection
		{
			return _listeProfils;
		}
		public function set listeActionsPossibles(value:ArrayCollection):void
		{
			_listeActionsPossibles = value;
		}

		public function get listeActionsPossibles():ArrayCollection
		{
			return _listeActionsPossibles;
		}
		public function set boolCtrlGestion(value:Boolean):void
		{
			_boolCtrlGestion = value;
		}

		public function get boolCtrlGestion():Boolean
		{
			return _boolCtrlGestion;
		}
		public function set boolPrepareCreation(value:Boolean):void
		{
			_boolPrepareCreation = value;
		}

		public function get boolPrepareCreation():Boolean
		{
			return _boolPrepareCreation;
		}
		public function set boolPrepareResiliation(value:Boolean):void
		{
			_boolPrepareResiliation = value;
		}

		public function get boolPrepareResiliation():Boolean
		{
			return _boolPrepareResiliation;
		}

		public function set selectedProfil(value:Object):void
		{
			if(value == null)
			{
				value = {IDPROFIL:0,IDPOOL:0}
			}
			else
			{
				_selectedProfil = value;	
			}
		}

		public function get selectedProfil():Object
		{
			return _selectedProfil;
		}

		public function set boolCloturerAvecLettrage(value:Boolean):void
		{
			_boolCloturerAvecLettrage = value;
		}

		public function get boolCloturerAvecLettrage():Boolean
		{
			return _boolCloturerAvecLettrage;
		}
		public function set boolCloturerSansLettrage(value:Boolean):void
		{
			_boolCloturerSansLettrage = value;
		}

		public function get boolCloturerSansLettrage():Boolean
		{
			return _boolCloturerSansLettrage;
		}
	}
}