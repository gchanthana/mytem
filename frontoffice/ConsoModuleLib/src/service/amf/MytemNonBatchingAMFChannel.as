package service.amf
{
	import flash.events.AsyncErrorEvent;
	import flash.events.IOErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.NetConnection;
	
	import mx.messaging.MessageResponder;
	import mx.messaging.channels.AMFChannel;
	
	public class MytemNonBatchingAMFChannel extends mx.messaging.channels.AMFChannel
	{
		public function MytemNonBatchingAMFChannel(id:String = "", uri:String = "")
		{
			super(id,uri);
		}
		
		override public function get protocol():String 
		{
			if(this.url.toLowerCase().search("https://") == -1)
			{
				return "http";
			}
			else 
			{
				return "https";
			}
		}
		
		override protected function internalSend(msgResp:MessageResponder):void
		{
			// AMFChannel internalSend
			super.internalSend(msgResp);
			// Reset the net connection.
			_nc = new NetConnection();
			_nc.addEventListener(NetStatusEvent.NET_STATUS, statusHandler); 
			_nc.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler); 
			_nc.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler); 
			_nc.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler); 
			_nc.connect(this.url);
		}
	}
}