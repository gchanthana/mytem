package intranet.GestionProduit.renderers
{
	import flash.events.Event;
	
	import intranet.GestionProduit.DataSample;
	import intranet.GestionProduit.entities.ProduitsVO;
	import intranet.GestionProduit.event.ChangeData_EVENT;
	
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.ListEvent;

	public class RendererProviderTheme extends HBox
	{
 		public var CBOabo:ComboBox;
		public var CBOconso:ComboBox;
		public var TIaffecte:Label;
		public var idabo:int=-1;
		public var idconso:int=-1;
		
		private var boolcombo:Boolean = false;
		private var boolAffecte:Boolean = false;
		 					
		public function RendererProviderTheme()
		{
			super();
			DataSample.boolcombo=false;
		}

// SURCHARGE DE LA FONCTION SET DATA - AFFICHAGE EN FONCTION DES DONNEES
 		override public function set data(value:Object):void
 		{
 			boolAffecte = false;
 			boolcombo = false;
 			
 			if(value)
 			{
 				super.data = value;
 				if(data.IDTHEME_PRODUIT)
 				{
					boolAffecte = true;
 				}
 				else
 				{
 					boolAffecte = false;
 				}
 				
 				if(data.hasOwnProperty("EDITED"))
 				{
	 				if(data.EDITED){
	 					boolcombo = true;
	 				}else{
	 					boolcombo = false;
	 				}
	 			}
 			} 				
			invalidateDisplayList();
			
			
 		}
 		
 		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
 			super.updateDisplayList(unscaledWidth,unscaledHeight);
 			
 				if(boolAffecte)
 				{
 					if(!boolcombo){
 						this.removeAllChildren();
 						TIaffecte.text= data.THEME;
						this.addChild(TIaffecte);
					}else{
						if(data.hasOwnProperty("CB")){
 							if(data.CB == "abo"){
 								CBOabo.selectedIndex=data.INDEX;
 								CBOconso.selectedIndex=-1;
 							}else{				
 								CBOconso.selectedIndex=data.INDEX;
								CBOabo.selectedIndex=-1;
							}
						}
					}
 				}
 				else
 				{
					this.removeAllChildren();
					this.addChild(CBOabo);
					this.addChild(CBOconso);
					
					if(boolcombo){
						if(data.hasOwnProperty("CB")){
 							if(data.CB == "abo"){
 								CBOabo.selectedIndex=data.INDEX;
 								CBOconso.selectedIndex=-1;
 							}else{				
 								CBOconso.selectedIndex=data.INDEX;
								CBOabo.selectedIndex=-1;
							}
						}else{
							CBOconso.selectedIndex=-1;
							CBOabo.selectedIndex=-1;
						}						
					}else{
						CBOconso.selectedIndex=-1;
						CBOabo.selectedIndex=-1;
					}
				}	  					
 		}

		// GESTION DU CHOIX DU THEME 
		public function GestionCBabo(e:Event):void
		{
			var item:Object=new Object();
			
			CBOconso.selectedIndex = idconso = -1;
			
			idabo = CBOabo.selectedIndex;
			
			data.IDTHEME_PRODUIT = CBOabo.selectedItem.IDTHEME_PRODUIT;
			data.EDITED = true;
			data.INDEX = CBOabo.selectedIndex;
			data.CB = "abo";
			data.THEME = CBOabo.selectedItem.THEME
			
			item.BOOL_ACCES= data.BOOL_ACCES;
			item.TOUT_ACCES= data.TOUT_ACCES;
			item.IDPRODUIT_CATALOGUE= data.IDPRODUIT_CATALOGUE;
			item.IDTHEME_PRODUIT=CBOabo.selectedItem.IDTHEME_PRODUIT;
			
			dispatchEvent(new ChangeData_EVENT(ChangeData_EVENT.ChangeData_EVENT,item,true,false));
		}
		
		public function GestionCBconso(e:Event):void
		{
			var item:Object=new Object();

			CBOabo.selectedIndex = idabo = -1;
			
			idconso = CBOconso.selectedIndex;
			
			data.IDTHEME_PRODUIT = CBOconso.selectedItem.IDTHEME_PRODUIT;
			data.EDITED=true;
			data.INDEX = CBOconso.selectedIndex;
			data.CB = "conso";
						
			item.IDPRODUIT_CATALOGUE= data.IDPRODUIT_CATALOGUE;
			item.IDTHEME_PRODUIT=CBOconso.selectedItem.IDTHEME_PRODUIT;

			data.TOUT_ACCES=0;
			data.BOOL_ACCES=0;			

			item.BOOL_ACCES= data.BOOL_ACCES;
			item.TOUT_ACCES= data.TOUT_ACCES;

			dispatchEvent(new ChangeData_EVENT(ChangeData_EVENT.ChangeData_EVENT,item,true,false));
		}
 	}
}
