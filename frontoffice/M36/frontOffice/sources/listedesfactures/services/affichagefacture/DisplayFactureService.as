package listedesfactures.services.affichagefacture
{
	import listedesfactures.entity.FactureDisplayParamsVo;

	public class DisplayFactureService implements IDisplayFacture
	{
		private var _displayer:IDisplayFacture;
		
		public function DisplayFactureService(value:IDisplayFacture)
		{
			_displayer = value
			
		}
		
		public function displayFacture():void
		{
			_displayer.displayFacture()
		}
	}
}