﻿<cfcomponent hint="Toutes les procédures concernant l'utilisateur" output="false">
	<cfset init()>

	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser = SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue = SESSION.USER.GLOBALIZATION>
		<cfset idLangue = SESSION.USER.IDGLOBALIZATION>
		<cfset idGroupeClient = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
	</cffunction>

	<cffunction name="threadInitData_OLD" access="remote" returntype="Any">
		<cfargument name="mypool" required="true" type="Numeric" />
		<cfargument name="myProfil" required="true" type="Numeric" />
<cflog text = "-------------------------------------------------------- log old threadInitData">
		<!--- Pour les tests : [FACTICE Désactivé par -1] Liste des vues pour __DEMO-CONSOTEL --->
		<cfif session.perimetre.ID_GROUPE EQ -1>
			<!---<cfreturn createObject("component","fr.saaswedo.api.tests.MYT58").getInitData(ARGUMENTS.mypool)>--->
			<cfthrow type="Custom" message="ID_GROUPE is negative">
		</cfif>
		<!--- Pour tous les autres groupes le fonctionnement reste inchangé --->
	
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getDroitGestionParc">
			<cfprocparam value="#session.user.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#mypool#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour1">
		</cfstoredproc>

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.get_catalogue_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="0">
			<cfprocresult name="p_result8">
		</cfstoredproc>
			
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getListeOrgaCus">
			<cfprocparam value="#session.perimetre.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour2">
		</cfstoredproc>
			
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getListeIdPeriodeMois">
			<cfprocresult name="p_retour3">
		</cfstoredproc>
			
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.GETSITEOFPOOL" blockfactor="100">
			<cfprocparam value="#mypool#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour4">
		</cfstoredproc>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListeActionProfilCommande">
			<cfprocparam value="#myProfil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.USER.GLOBALIZATION#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="p_retour5">
		</cfstoredproc>

		<cfset listeOperateurs.RESULT = getListeOperateurs(mypool)>
	
		<cfset initDataStruct = structNew()>
		<cfset initDataStruct.listeOrga  = p_retour2>
		<cfset initDataStruct.listeMois  = p_retour3>
		<cfset initDataStruct.droitGp  	 = p_retour1>		
		<cfset initDataStruct.listeSite	 = p_retour4>
		<cfset initDataStruct.REVENDEUR	 = p_result8>
		<cfset initDataStruct.OPERATEUR	 = listeOperateurs.RESULT>
		<cfset initDataStruct.listeDroitCommande	 = p_retour5>

		<cfreturn initDataStruct>
	</cffunction>

	<cffunction name="threadInitData" access="remote" returntype="Any">
		<cfargument name="mypool" required="true" type="Numeric" />
		<cfargument name="myProfil" required="true" type="Numeric" />
<!---cflog text = "-------------------------------------------------------- log to NEW threadInitData"--->
		<!--- Pour les tests : [FACTICE Désactivé par -1] Liste des vues pour __DEMO-CONSOTEL --->
		<cfif session.perimetre.ID_GROUPE EQ -1>
			<!---<cfreturn createObject("component","fr.saaswedo.api.tests.MYT58").getInitData(ARGUMENTS.mypool)>--->
			<cfthrow type="Custom" message="ID_GROUPE is negative">
		</cfif>

		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m111B.getthreadinit_parc">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idGroupeClient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#mypool#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeLangue#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#myProfil#">
				
				<cfprocresult name="p_retour1" resultset="1">
				<cfprocresult name="p_result8" resultset="2">
				<cfprocresult name="p_retour2" resultset="3">
				<cfprocresult name="p_retour3" resultset="4">
				<cfprocresult name="p_retour4" resultset="5">
				<cfprocresult name="p_retour5" resultset="6">
				
		</cfstoredproc>

		<cfset listeOperateurs.RESULT = getListeOperateurs(mypool)>
	
		<cfset initDataStruct = structNew()>
		<cfset initDataStruct.listeOrga  = p_retour2>
		<cfset initDataStruct.listeMois  = p_retour3>
		<cfset initDataStruct.droitGp  	 = p_retour1>		
		<cfset initDataStruct.listeSite	 = p_retour4>
		<cfset initDataStruct.REVENDEUR	 = p_result8>
		<cfset initDataStruct.OPERATEUR	 = listeOperateurs.RESULT>
		<cfset initDataStruct.listeDroitCommande	 = p_retour5>

		<cfreturn initDataStruct>
	</cffunction>
	

	
	
	<cffunction name="getListeOperateurs" access="remote" returntype="query">
		<cfargument name="idpool" type="numeric">> 
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.L_Operateur_pool_user">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idpool#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
			<cfprocresult name="qResult">
		</cfstoredproc>
		<cfreturn qResult>
	</cffunction>
	<cffunction name="getListePool" access="remote" returntype="Any">
		<!---<cftry>
			<cfset var obj=createObject("component","fr.consotel.consoview.M111.fiches.FicheCommun2")>
			<cflog text="FicheCommun2 : #structKeyList(obj)#">
			<cfcatch type="any">
				<cflog text="#CFCATCH.message#:#CFCATCH.Detail#">
			</cfcatch>
		</cftry>--->
		<!--- Pour les tests : [FACTICE Désactivé par -1] Liste des pools pour __DEMO-CONSOTEL --->
		<cfif session.perimetre.ID_GROUPE EQ -1>
			<!---<cfreturn createObject("component","fr.saaswedo.api.tests.MYT58").getListePool()>--->
			<cfthrow type="Custom" message="ID_GROUPE is negative">
		<!--- Pour tous les autres groupes le fonctionnement reste inchangé --->
		<cfelse>
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getPool_of_gestionnaire">
				<cfprocparam value="#session.user.CLIENTACCESSID#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#session.perimetre.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cfif>
	</cffunction>
	<cffunction name="getListeTypeContrat" access="remote" returntype="Any">
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m111.getTypeContrat">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>  
</cfcomponent>
