package gestionparc.ihm.renderer.entityrenderer
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.ihm.fiches.ficheEntity.FicheEquipementIHM;
	import gestionparc.ihm.main.menu.MenuContextual;
	import gestionparc.ihm.main.menu.itemmenurenderer.ItemMenuContextualOfEquipement;


	public class SerieItemRenderer extends EntityItemRenderer
	{
		public function SerieItemRenderer():void
		{
			super();
		}

		override public function set data(value:Object):void
		{
			lbLibelle.text="";
			lbLibelle.toolTip="";
			lbLibelle.useHandCursor=false;
			this.img.visible=false;
			
			if (value != null)
			{
				super.data=value;

				if (value.hasOwnProperty("T_NO_SERIE") && value.T_NO_SERIE)
				{
					if (SpecificationVO.getInstance().idPool == SpecificationVO.POOL_PARC_GLOBAL_ID) // si vue parc global on n'affiche pas l'image action
					{
						this.img.visible=false;
					}
					else
					{
						this.img.visible=true;
						this.img.addEventListener(MouseEvent.CLICK, lineSelected);
					}
					lbLibelle.text=value.T_NO_SERIE.split(" ").join("");				
					lbLibelle.toolTip=lbLibelle.text;
					lbLibelle.addEventListener(MouseEvent.CLICK, ClickFicheHandler);
					
					switch (value.T_ID_STATUT)
					{
						case 4:
							setStyle("backgroundColor", 0xFEC46D); // orange 
							setStyle("backgroundAlpha", 1);
							break;
						
						case 3:
							setStyle("backgroundColor", 0xFE9999); // rouge
							setStyle("backgroundAlpha", 1);
							break;
						
						default:
							setStyle("backgroundColor", 0xFFFFFF);
							setStyle("backgroundAlpha", 0);
							break;
					}
				}
			}
		}

		/**
		 * Cette fonction est appelée lors d'un clique sur l'image de l'item renderer,
		 * elle place les informations concernant le menu de cet item et
		 * appelle la fonction qui affiche se dernier.
		 */
		private function lineSelected(evt:Event):void
		{
			/**  on met a jour les information de la ligne selectionnée */
			SpecificationVO.getInstance().elementDataGridSelected=evt.currentTarget.owner.data;
			var ligne:Object=evt.currentTarget.owner.data;
			var Context:SpecificationVO = SpecificationVO.getInstance();
			
			if((Context.validSelectedIdPool == SpecificationVO.POOL_TOUT_PARC_ID) ) //pas de profil == 'Mes pools'
			{
				Context.updateProfilCommande(ligne);//MYT-1606
				Context.idPool = ligne.IDPOOL;
				Context.labelPool = ligne.POOL;
				var userPool:Object = Context.getUserPoolInfo(ligne.IDPOOL);
				if(userPool != null)
				{
					Context.idProfil = userPool.IDPROFIL;
					Context.labelProfil =  userPool.PROFIL;
				}
			}

			var myItemMenuContextualOfEquipement:ItemMenuContextualOfEquipement=new ItemMenuContextualOfEquipement();
			myItemMenuContextualOfEquipement.addEventListener(MDMDataEvent.MDM_DATA_COMPLETE, onItemMenuCreated);
			myItemMenuContextualOfEquipement.initItemMenu(); /** Dispatche un évènement : MDMDataEvent.MDM_DATA_COMPLETE*/
		}

		private function onItemMenuCreated(eventObject:Event):void
		{
			var myItemMenuContextualOfEquipement:ItemMenuContextualOfEquipement=new ItemMenuContextualOfEquipement();
			this.arrayItemMenu=new Array();
			this.arrayItemMenu=myItemMenuContextualOfEquipement.returnArrayItemMenuContextual();
			this.menu=new MenuContextual(this.arrayItemMenu);

			clickIconActionHandler();
		}

		override protected function setFiche():void
		{
			fiche=new FicheEquipementIHM();
			(fiche as FicheEquipementIHM).nomPrenomCollab=(data as AbstractMatriceParcVO).COLLABORATEUR;
			(fiche as FicheEquipementIHM).matriculeCollab=(data as AbstractMatriceParcVO).E_MATRICULE;
			(fiche as FicheEquipementIHM).idTerminal=(data as AbstractMatriceParcVO).IDTERMINAL;
		}
	}
}
