package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;

	public class RuleAssocierCollabForLigne implements IRuleDisplayItem
	{
		public function RuleAssocierCollabForLigne()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new IsTypeLigneMobile();
			var r2:IRuleDisplayItem = new HasSimAssociated();
			var r3:IRuleDisplayItem = new HasCollaborateurAssociated();
			
			if(r1.isDisplay() && r2.isDisplay() && !r3.isDisplay())	
				return true;
			else if(!r1.isDisplay() && !r2.isDisplay() && !r3.isDisplay())
				return true;
			else
				return false;
		}
	}
}