package composants.tb.ligne.renderer
{
	import flash.events.Event;
	
	import paginatedatagrid.PaginateDatagrid;

	public class PaginateDatagridExtendImpl extends PaginateDatagrid
	{
		public function PaginateDatagridExtendImpl()
		{
		}
		
		public var filtre:Function;
		
		protected override function filtreHandler(e:Event):void
		{
			dataprovider.filterFunction = filtre;
			dataprovider.refresh();
		}
		
		
		
	}
}