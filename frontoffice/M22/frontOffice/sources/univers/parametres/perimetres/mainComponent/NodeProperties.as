package univers.parametres.perimetres.mainComponent
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.perimetres.main;

	
	public class NodeProperties extends NodePropertiesIHM
	{
		private var _IDnode:Number;
		private var _IDorga:Number;
		private var _TypeOrga:String;
		private var _parentRef:main;
		private var _nodeSelected:Object;
		public function get modeEcriture ():Boolean{ return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)}		
		
		public function NodeProperties()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function setParentRef(ref:main):void
		{
			_parentRef = ref;
		}
		
		/*public function setNodeId(id:Number):void
		{
			_IDnode = id;
		}*/
		
		public function setID(orgaID:Number, typeOrga:String, nodeID:Number):void
		{
			_IDnode = nodeID;
			_IDorga = orgaID;
			_TypeOrga = typeOrga;
		}
		
		public function setDataRef(obj : Object):void{
			_nodeSelected = obj;
		}
		
		protected function initIHM(event:Event):void
		{
			
			PopUpManager.centerPopUp(this);
			this.addEventListener(CloseEvent.CLOSE, closeWin);
			btnCancel.addEventListener(MouseEvent.CLICK, onbtnCancelClicked);
			btnUpdate.visible = modeEcriture;
			btnUpdate.addEventListener(MouseEvent.CLICK, onbtnUpdateClicked);
							
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getNodeProperties",
																				getNodePropertiesResult);
			RemoteObjectUtil.callService(op, _IDnode);
			refreshWindow();
		}
		 
		private function refreshWindow():void
		{
			var hide:Boolean = true;
			switch (_TypeOrga)
			{
				case "GEO":
					cbTypeOrga.selected = true;
					break;
				case "CUS":
					break;
				default:
					hide = false;
			}
			
			if (_IDnode == _IDorga)
			{
				fiTypeOrga.visible = hide;
				fiNbLine.visible = false;
			}
			else
			{
				fiTypeOrga.visible = false;
				fiNbLine.visible = true;
			}
		}
		
		private function getNodePropertiesResult(event:ResultEvent):void
		{
							
			var d:ArrayCollection = ArrayCollection(event.result);
			txtComment.text = d[0].COMMENTAIRES;
			var newDate:Date = d[0].DATE_CREATION as Date;
			txtDate.text = d[0].DATE_CREATION_STRING;
			txtNbLine.text = d[0].NB_ELEM;
			txtTypeNiveau.text = d[0].TYPE_NIVEAU;
		}
		
		private function closeWin(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function onbtnCancelClicked(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function onbtnUpdateClicked(event:MouseEvent):void
		{
			var idOrga:Number;
			var typeOrga:String = "CUS";
			if (fiTypeOrga.visible)
				idOrga = _IDorga;
			if (cbTypeOrga.selected)
				typeOrga = "GEO";
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"updateNodeProperties",
																				updateNodePropertiesResult);
																				
			_TypeOrga = typeOrga;
			RemoteObjectUtil.callService(op, _IDnode, txtComment.text, idOrga, typeOrga);
		}
		
		private function updateNodePropertiesResult(event: ResultEvent):void
		{
			if (_nodeSelected != null){
				if (txtComment.text != null){					
					/* if (_nodeSelected.hasOwnProperty('@LABEL')){
						var newLabel:String = _nodeSelected.@LABEL + " - " + txtComment.text;
						_nodeSelected.@LABEL = newLabel;
					}*/
					_nodeSelected.@COMMENTAIRES = txtComment.text;	
				}
			}
			
			_parentRef.updateSelectedOrgaType(_TypeOrga);
			closeWin(new CloseEvent("close"));
		}
	}
}