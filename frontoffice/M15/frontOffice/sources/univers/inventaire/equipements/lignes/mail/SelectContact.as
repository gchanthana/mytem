package univers.inventaire.equipements.lignes.mail
{
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.GestionContactsIHM;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import flash.events.MouseEvent;
	import mx.managers.PopUpManager;
	import flash.events.Event;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts.GestionContacts;
	
	[Bindable]
	public class SelectContact extends TitleWindow
	{
		public var btFermer : Button;
		public var btValider : Button;
		public var gestionContact : GestionContactsIHM;
		
		
		private var _agence : Societe;
		private var _distributeur : Societe;
		private var _contact : Contact;
		private var _operateur : OperateurVO;
		
		public function set agence(a : Societe):void{
			_agence = a;	
		}		
		public function set distributeur(d : Societe):void{
			_distributeur = d;
		}
		public function set contact(c : Contact):void{
			_contact = c;
		}
		public function set operateur(op : OperateurVO):void{
			_operateur = op;
		}
		
		public function get agence():Societe{
			return _agence;
		}		
		public function get distributeur():Societe{
			return _distributeur;
		}
		public function get contact():Contact{
			return _contact;
		}
		public function get operateur():OperateurVO{
			return _operateur;
		}
		
			
		/**
		 * constante definissant l'envenment Mail Envoyer
		 * */
		public static const CONTACT_SELECTED : String = "contactSelectionne";
		
		
		
		
		
		
		protected override function commitProperties():void{
			
			gestionContact.lblTitre.text = "Distributeur";	
			gestionContact.cmbAgence.width = 200;
			gestionContact.cmbContact.width = 200;
			gestionContact.cmbDistributeur.width = 200;	
			
			gestionContact.addEventListener(GestionContacts.COMBO_SOCIETE_READY,comboSocieteReadyHandler);
			gestionContact.cmbContact.addEventListener(ListEvent.CHANGE,contactChangeHandler);
			gestionContact.lblMail.text = "";
			
			
			btFermer.addEventListener(MouseEvent.CLICK,btFermerClickHandler);
			btValider.addEventListener(MouseEvent.CLICK,btValiderClickHandler);		
			addEventListener(CloseEvent.CLOSE,closeHandler);
			
		}
		
		//---HANDLERS ----------------------------------------------------------
		
		protected function btValiderClickHandler(me : MouseEvent):void{
			if (gestionContact.cmbContact.selectedItem != null){
				dispatchEvent(new Event(CONTACT_SELECTED));
				PopUpManager.removePopUp(this);	
			}else{
				Alert.show("Vous devez sélectionner un constact","Attention");
			}
		}
		
		
		protected function btFermerClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		
		
		protected function closeHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		//selectionne le contact dans la combo
		private function comboSocieteReadyHandler(evt : Event):void{
			if (distributeur){
				gestionContact.autoSelectContact(_distributeur,_agence,_contact); 
			}else if(agence){
				gestionContact.autoSelectContact(agence,null,_contact); 
			}
		}
		
		
		//Handler du controle contact
		//param in
		//	ListEvent
		private function contactChangeHandler(le : ListEvent):void{			
			if (gestionContact.getContact() > 0){
				contact = Contact(gestionContact.cmbContact.selectedItem);
				distributeur = (Societe(gestionContact.cmbAgence.selectedItem).societeID > 0)?Societe(gestionContact.cmbAgence.selectedItem):Societe(gestionContact.cmbDistributeur.selectedItem);
				btValider.enabled = true;
			}else{
				btValider.enabled = false;
			}
			
		}	
		
		//--- FIN HANDLERS ------------------------------------------------------
		
	}
}