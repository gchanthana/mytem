<cfcomponent name="JournalCommandesFactureesAvLivraison">
	<cffunction name="afficher"> 				
		<cfsetting enablecfoutputonly="true" />
		<cfset label="Journal">		
		<cfset qGetGrid=createObject("component","fr.consotel.consoview.util.utils").getQuery("session.dataInventaire.journal")>				
		<cfset NewLine = Chr(13) & Chr(10)>		
		<cfheader name="Content-Disposition" value="inline;filename=rapport.csv">
		<cfcontent type="text/plain">
		<cfoutput>Produit;Ligne;Opérateur;Montant facturé (euro H.T);Date facturation;N°facture;N°opération;Type opération;Libellé opération;Etat;#NewLine#</cfoutput>		
		<cfoutput query="qGetGrid">#TRIM(LIBELLE_PRODUIT)#;#TRIM(SOUS_TETE)#;#TRIM(OPNOM)#;#TRIM(MONTANT)#;#TRIM(LSDateFormat(createDate(left(DATE_FACTURE,4),mid(DATE_FACTURE,6,2),mid(DATE_FACTURE,9,2)),"dd/mm/yyyy"))#;#TRIM(NUM_FACTURE)#;;#TRIM(NUMERO_OPERATION)#;#TRIM(TYPE_OPERATION)#;#TRIM(LIBELLE_OPERATIONS)#;#TRIM(LIBELLE_ETAT)##NewLine#</cfoutput>	
	</cffunction>			
</cfcomponent>