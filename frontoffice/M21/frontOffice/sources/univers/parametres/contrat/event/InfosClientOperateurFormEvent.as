package univers.parametres.contrat.event
{
	import flash.events.Event;
	
	import univers.parametres.contrat.entity.InfosClientOperateurVo;

	public class InfosClientOperateurFormEvent extends Event
	{
		public static const VALIDER_CLICKED:String = "validerClicked";
		public static const ANNULER_CLICKER:String = "annulerClicked";
		
		private var _infos:InfosClientOperateurVo;
		
		public function InfosClientOperateurFormEvent(value:InfosClientOperateurVo, type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_infos = value;
		}
		
		override public function clone():Event
		{
			return new InfosClientOperateurFormEvent(infos,type,bubbles,cancelable)	
		}
		
		public function get infos():InfosClientOperateurVo
		{
			return _infos;
		}
	}
}