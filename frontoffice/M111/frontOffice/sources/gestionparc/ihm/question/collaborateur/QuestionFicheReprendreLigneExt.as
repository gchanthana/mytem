package gestionparc.ihm.question.collaborateur
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheReprendreLigneExt
	{
		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheReprendreLigneExt()
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			
			if(ligne.IDTYPE_LIGNE > 0) // tester s'il y a une ligne d'associer à l'extension
			{
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEREPRENDRELIGNESIM"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnCloseHandler)
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "Etre_vous_s_r_de_vouloir_dissocier_l_extension_du_collaborateur"), ResourceManager.getInstance().getString("M111", "Dissocier_extension_du_collaborateur"), alertClickHandler);
			}
		}
		
		private function qnCloseHandler(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
			{
				reprendreLigneExtFromCollab1()
			}
			else if (evt.detail == Alert.CANCEL)
			{
				
			}
			else
			{
				reprendreLigneExtFromCollab2()
			}
		}
				
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				reprendreExtFromCollab();
			}
		}

		private function reprendreLigneExtFromCollab1():void
		{
			serv.reprendreLigneExtFromCollab(setObj());
		}

		private function reprendreLigneExtFromCollab2():void
		{
			serv.reprendreLigneExtFromCollab2(setObj());
		}
		
		private function reprendreExtFromCollab():void
		{
			serv.reprendreExtFromCollab(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=0;
			obj.ID_EXT=SpecificationVO.getInstance().elementDataGridSelected.IDEXTENSION;
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
