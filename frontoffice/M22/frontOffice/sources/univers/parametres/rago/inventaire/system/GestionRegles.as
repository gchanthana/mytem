package univers.parametres.rago.inventaire.system
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.entity.Rules;
	import univers.parametres.rago.inventaire.event.InventaireReglesEvent;
	
	
	public class GestionRegles extends EventDispatcher
	{

//VARIABLES GLOBALES----
		
		public var rules:Rules;
		public var tempRegle:Regle = new Regle();
		public var isUsed:Boolean = false;
		
		private var _listeRegles:ArrayCollection = new ArrayCollection();
		private var _listeExceptions:ArrayCollection = new ArrayCollection();

//FIN VARIABLES GLOBALES----

//PROPIETEES PUBLICS----
		
		//Récupère la liste des règles
		[Bindable]
		public function set listeRegles(value:ArrayCollection):void
		{ listeRegles = value; }
		
		public function get listeRegles():ArrayCollection
		{ return _listeRegles; }

//FIN PROPIETEES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur	
		public function GestionRegles()
		{  }

		//Si la regle est active, désactive la regle sinon active la règle		
		public function activerDesactiverRegle(regle:Regle):void
		{
			if(regle && regle.IDREGLE_ORGA > 0)
			{
				if(regle.BOOL_ACTIVE) { regle.BOOL_ACTIVE = false; }
				else { regle.BOOL_ACTIVE = true; }	
					
				listeRegles.itemUpdated(regle);

				rules.activeRules(regle.IDREGLE_ORGA,regle.REGLE_NOM,regle.TYPE_REGLE,regle.REGLE_COMMENT,
									regle.IDSOURCE,regle.IDCIBLE ,zFormatBoolToInt(regle.BOOL_ACTIVE),zFormatBoolToInt(regle.BOOL_SUIVRE_AUTO));
				rules.addEventListener("active",resultActive);
			}	
		}
		
		//Executer une regle
		public function executerRegle(regle:Regle):void
		{	
			if(regle && regle.IDREGLE_ORGA > 0)
			{
				tempRegle = regle;
				rules.GetRegleException(regle.IDREGLE_ORGA);
				rules.addEventListener("receivedExceptions",listException);
			}
		}
		
		//Suppression d'une règle
		public function supprimerRegle(regle:Regle,lineToErase:int):void
		{	
			if(regle && regle.IDREGLE_ORGA > 0)
			{	
				rules.eraseRules(regle.IDREGLE_ORGA);
				rules.addEventListener("erase",resultErase);
			}
		}
		
		//Affiche les règle dans le grid
		public function ajouterRegle(regle:Regle,execute:Boolean=false):void
		{	
			if(regle) { listeRegles.addItem(regle); }	
		}

		public function detaillerRegle(regle:Regle):void
		{	
			if(regle && regle.IDREGLE_ORGA > 0)
			{ trace("detaillerRegle"); }
		}
		
		//Liste les règles
		public function listerRegles(clefDeRecherche:String=""):void
		{  }
		
		//Met à jour le libellé et le commentaire d'une règle
		public function updateInfosRegle(regle:Regle):void
		{  }
		
		//Met à jour les exceptions d'une règle
		public function updateExceptionsRegele(regle:Regle,exceptions:ArrayCollection):void
		{  }

//FIN METHODES PROTECTED----
		
		protected function activerDesactiverRegleResultEvent(event:ResultEvent):void
		{  }

		protected function ajouterRegleHandler(event:ResultEvent):void
		{  } 

		protected function supprimerRegleResultHandler(event:ResultEvent):void
		{  }
		
		//Liste les règles dans le grid
		protected function listerReglesResultHandler():void
		{ dispatchEvent(new Event(InventaireReglesEvent.ATTRIBUT_IN_DATAGRID,true)); }

//FIN METHODES PROTECTED----

//METHODES PRIVATE----

		//Formateur boolean en integer
		private function zFormatBoolToInt(bool:Boolean):int
		{
			var rsltConvertBoolToInt:int;
			
			if(bool) { rsltConvertBoolToInt = 1; }
			else { rsltConvertBoolToInt = 0; }
			
			return rsltConvertBoolToInt;	
		}
		
		//Attribut les règles recus par la procédure dans le grid
		private function resultActive(event:Event):void
		{ listerReglesResultHandler(); }
		
		//Affiche la popUp 'DetailRegleIHM'
		private function listException(event:Event):void
		{
			if(isUsed)
			{
				tempRegle.LIST_EXCEPTION = rules.listRegleExceptions;
			}
			else
			{
				tempRegle.LIST_EXCEPTION = rules.listRegleExceptions;
				dispatchEvent(new Event("isUsed",true));
				isUsed = true;
			}
		}
		
		public function closeExceptions(event:Event):void
		{
			isUsed = false;
		}
		
		//Enregistre les modifications apportées à une regle
		public function executeSendRegle(event:Event):void
		{
				rules.IURulesOrganisation(		tempRegle.IDREGLE_ORGA,
												tempRegle.REGLE_NOM,
												tempRegle.TYPE_REGLE,
												tempRegle.REGLE_COMMENT,
												tempRegle.IDSOURCE,
												tempRegle.IDCIBLE,
												zActiveOrNot(tempRegle.BOOL_ACTIVE),
												zFormatBoolean(tempRegle.BOOL_SUIVRE_AUTO));
				
				rules.addEventListener("envoiRegle",executeSendException);

		}
		
		//Format un int en bool 0->false, 1->true
		private function zActiveOrNot(bool:Boolean):int
		{
			if(bool)return 1;
			else return 0;
		}
		
		
		//Format un int en bool 0->false, 1->true
		private function zFormatBoolean(bool:Boolean):int
		{
			if(bool)return 1;
			else return 0;
		}
		
		private function executeSendException(event:Event):void
		{}
		
		//Recupere les exceptions de la règle concernée
		public function sendToPopUpNewExceptionReceived(event:Event):void
		{
			if(tempRegle && tempRegle.IDREGLE_ORGA > 0)
			{
				rules.GetRegleException(tempRegle.IDREGLE_ORGA);
				rules.addEventListener("receivedExceptions",listExceptionSendToPopUpNewException);
			}
		}
		
		//Rafraichie le grid de la popUp 'newExceptionIHM'
		private function listExceptionSendToPopUpNewException(event:Event):void
		{
			dispatchEvent(new Event("exceptionWasRefresh"));
		}
		
		//Affiche un message comme quoi la règle sélectionnée à bien été supprimée
		private function resultErase(event:Event):void
		{
			Alert.show(ResourceManager.getInstance().getString('M22', 'Votre_r_gle_a__t__supprim_e_'),"CONSOVIEW");
//			ConsoviewAlert.afficherOKImage("Votre règle a été effacée.",this);
//			dispatchEvent(new Event("viewMessageConfirmationErase",true));
		}
		
		//Formate la date en JJ/MM/AAAA
		private function zformateDate(dateToFormat:Object):String
		{
			var realDate:Object = new Object();
			
			realDate = {date:dateToFormat.date + "/" , month:zformateMonth(dateToFormat.month) + "/" , fullyear:dateToFormat.fullYear};
			
			var dayInString:String = realDate.date.toString();
			var monthInString:String = realDate.month.toString();
			var yearInString:String = realDate.fullyear.toString();
			
			if(dayInString.length == 1){dayInString = "0" + dayInString;}
			
			var realDateInString:String = dayInString + monthInString + yearInString;
			realDate = {date:realDateInString};
			return realDateInString;
		}
		
		//Formate le mois 0->01(janvier), 1->02(février), ..., 11->12(décembre)
		private function zformateMonth(monthToFormate:int):String
		{
			var month:String = null;
			
			switch (monthToFormate)
			{
					case 0:month = "01";break;
					case 1:month = "02";break;
					case 2:month = "03";break;
					case 3:month = "04";break;
					case 4:month = "05";break;
					case 5:month = "06";break;
					case 6:month = "07";break;
					case 7:month = "08";break;
					case 8:month = "09";break;
					case 9:month = "10";break;
					case 10:month = "11";break;
					case 11:month = "12";break;
			}
			
			return month;
		}
		
		private function executeAttributInDatagrid(event:Event):void
		{  }
		
//FIN METHODES PRIVATE----		
		
	}
}