package intranet.GestionDesLogin.vo
{
	[Bindable]
	public class LoginExtranetVo
	{
		public function LoginExtranetVo()
		{
		}
		
		private var _NBRECORD:Number;
		private var _LOGIN_EXTRANET:String;
		private var _PASSWORD:String;
		private var _TYPE_EXTRANET:Number;
		private var _CDR:Number;
		private var _TELECHARGER:Number;
		private var _COMPTE:String;
		private var _OPERATEUR:String;
		private var _OPERATEURID:Number;
		
		private var _STR_CDR:String;
		private var _STR_TELECHARGER:String;
		private var _STR_TYPE_EXTRANET:String;
		
		public function fill(value:Object):void
		{
			NBRECORD = value.NBRECORD;
			LOGIN_EXTRANET = value.LOGIN_EXTRANET;
			PASSWORD = value.PASSWORD;
			TYPE_EXTRANET = value.TYPE_EXTRANET;
			STR_TYPE_EXTRANET = value.TYPE_EXTRANET;
			CDR = value.CDR;
			STR_CDR = value.CDR;
			TELECHARGER = value.TELECHARGER;
			STR_TELECHARGER = value.TELECHARGER;
			COMPTE = value.COMPTE;
			OPERATEUR = value.OPERATEUR;
			OPERATEURID = value.OPERATEURID; 
		}


		public function set NBRECORD(value:Number):void
		{
			_NBRECORD = value;
		}

		public function get NBRECORD():Number
		{
			return _NBRECORD;
		}
		public function set LOGIN_EXTRANET(value:String):void
		{
			_LOGIN_EXTRANET = value;
		}

		public function get LOGIN_EXTRANET():String
		{
			return _LOGIN_EXTRANET;
		}
		public function set PASSWORD(value:String):void
		{
			_PASSWORD = value;
		}

		public function get PASSWORD():String
		{
			return _PASSWORD;
		}
		public function set TYPE_EXTRANET(value:Number):void
		{
			_TYPE_EXTRANET = value;
		}

		public function get TYPE_EXTRANET():Number
		{
			return _TYPE_EXTRANET;
		}
		public function set CDR(value:Number):void
		{
			_CDR = value;
		}

		public function get CDR():Number
		{
			return _CDR;
		}
		public function set TELECHARGER(value:Number):void
		{
			_TELECHARGER = value;
		}

		public function get TELECHARGER():Number
		{
			return _TELECHARGER;
		}
		public function set COMPTE(value:String):void
		{
			_COMPTE = value;
		}

		public function get COMPTE():String
		{
			return _COMPTE;
		}
		public function set OPERATEUR(value:String):void
		{
			_OPERATEUR = value;
		}

		public function get OPERATEUR():String
		{
			return _OPERATEUR;
		}
		public function set OPERATEURID(value:Number):void
		{
			_OPERATEURID = value;
		}

		public function get OPERATEURID():Number
		{
			return _OPERATEURID;
		}
		

		public function set STR_CDR(value:String):void
		{
			_STR_CDR = value;
		}

		public function get STR_CDR():String
		{
			return _STR_CDR;
		}
		public function set STR_TELECHARGER(value:String):void
		{
			_STR_TELECHARGER = value;
		}

		public function get STR_TELECHARGER():String
		{
			return _STR_TELECHARGER;
		}
		public function set STR_TYPE_EXTRANET(value:String):void
		{
			_STR_TYPE_EXTRANET = value;
		}

		public function get STR_TYPE_EXTRANET():String
		{
			return _STR_TYPE_EXTRANET;
		}
	}
}