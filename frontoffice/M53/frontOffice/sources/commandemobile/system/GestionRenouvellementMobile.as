package commandemobile.system
{
	import composants.util.contrats.Contrat;
	
	import mx.collections.ArrayCollection;
	
	public class GestionRenouvellementMobile extends GestionOptionsMobile
	{
		public function GestionRenouvellementMobile()
		{
			
		}
		
		
		override protected function createCommandeFromContrat(ctrt:Contrat ,donnee:Array):void
		{
			if(donnee[0] != null && (donnee[0] as ArrayCollection).length > 0)
			{
				ctrt.IDTYPE_CONTRAT = donnee[0][0].IDTYPE_CONTRAT;
				ctrt.IDFOURNISSEUR = donnee[0][0].IDFOURNISSEUR;
				ctrt.REFERENCE_CONTRAT = donnee[0][0].REFERENCE_CONTRAT;
				ctrt.DATE_SIGNATURE = donnee[0][0].DATE_SIGNATURE;
				ctrt.DATE_ECHEANCE = donnee[0][0].DATE_ECHEANCE;
				ctrt.COMMENTAIRE_CONTRAT = donnee[0][0].COMMENTAIRE_CONTRAT;
				ctrt.MONTANT_CONTRAT = donnee[0][0].MONTANT_CONTRAT;
				ctrt.DUREE_CONTRAT = donnee[0][0].DUREE_CONTRAT;
				ctrt.TACITE_RECONDUCTION = donnee[0][0].TACITE_RECONDUCTION;
				ctrt.DATE_DEBUT = donnee[0][0].DATE_DEBUT;
				ctrt.PREAVIS = donnee[0][0].PREAVIS ;
				ctrt.DATE_DENONCIATION = donnee[0][0].DATE_DENONCIATION;
				ctrt.IDCDE_CONTACT_SOCIETE = donnee[0][0].IDCDE_CONTACT_SOCIETE;
				ctrt.LOYER = donnee[0][0].LOYER;
				ctrt.PERIODICITE = donnee[0][0].PERIODICITE;
				ctrt.MONTANT_FRAIS = donnee[0][0].MONTANT_FRAIS;
				ctrt.NUMERO_CLIENT = donnee[0][0].NUMERO_CLIENT;
				ctrt.NUMERO_FOURNISSEUR = donnee[0][0].NUMERO_FOURNISSEUR;
				ctrt.NUMERO_FACTURE = donnee[0][0].NUMERO_FACTURE;
				ctrt.CODE_INTERNE = donnee[0][0].CODE_INTERNE;
				ctrt.BOOL_CONTRAT_CADRE = donnee[0][0].BOOL_CONTRAT_CADRE;
				ctrt.BOOL_AVENANT = donnee[0][0].BOOL_AVENANT;
				ctrt.ID_CONTRAT_MAITRE = donnee[0][0].ID_CONTRAT_MAITRE;
				ctrt.FRAIS_FIXE_RESILIATION = donnee[0][0].FRAIS_FIXE_RESILIATION;
				ctrt.MONTANT_MENSUEL_PEN = donnee[0][0].MONTANT_MENSUEL_PEN;
				ctrt.DATE_RESILIATION = donnee[0][0].DATE_RESILIATION;
				ctrt.PENALITE = donnee[0][0].PENALITE;
				ctrt.DESIGNATION = donnee[0][0].DESIGNATION;
				ctrt.DATE_RENOUVELLEMENT = donnee[0][0].DATE_RENOUVELLEMENT;
				ctrt.OPERATEURID = donnee[0][0].OPERATEURID;
				ctrt.IDCOMPTE_FACTURATION = donnee[0][0].IDCOMPTE_FACTURATION;
				ctrt.IDSOUS_COMPTE = donnee[0][0].IDSOUS_COMPTE;
				ctrt.NUMERO_CONTRAT = donnee[0][0].NUMERO_CONTRAT;
				ctrt.IDRACINE = donnee[0][0].IDRACINE;
				ctrt.DATE_ELLIGIBILITE = donnee[0][0].DATE_ELLIGIBILITE;
				ctrt.DUREE_ELLIGIBILITE = donnee[0][0].DUREE_ELLIGIBILITE;		
				
				
					
				ctrt.IDCDE_CONTACT_SOCIETE = donnee[0][0].IDCONTACT;
				
				//mapping de la commande
				commande.IDOPERATEUR = ctrt.OPERATEURID;
				commande.LIBELLE_OPERATEUR = donnee[0][0].LIBELLE_OPERATEUR;
				
				commande.IDREVENDEUR = ctrt.IDFOURNISSEUR;
				commande.LIBELLE_REVENDEUR = donnee[0][0].LIBELLE_OPERATEUR;
				commande.IDRACINE = ctrt.IDRACINE;
				commande.IDCOMPTE_FACTURATION = ctrt.IDCOMPTE_FACTURATION;
				commande.IDSOUS_COMPTE = ctrt.IDSOUS_COMPTE;
				commande.LIBELLE_SOUSCOMPTE = donnee[0][0].SOUS_COMPTE;
				commande.LIBELLE_COMPTE = donnee[0][0].COMPTE_FACTURATION;
								
				article.dureeEngagementRessources = ctrt.DUREE_CONTRAT;
				article.idRevendeur = ctrt.IDFOURNISSEUR;
				
				
				
				var donneesEquipements : ArrayCollection = donnee[1] as ArrayCollection;
				var nbEquipements : Number = donneesEquipements.length;
				 
				for(var i:int = 0; i < nbEquipements; i ++)
				{
					article.addEquipement(donneesEquipements[i]);
				}
				
				
				var donneesRessources : ArrayCollection = donnee[2] as ArrayCollection;
				var nbRessources : Number = donneesRessources.length;
				 
				for(var j:int = 0; j < nbRessources ; j ++)
				{
					article.addRessource(donneesRessources[j]);
				}
			}
		}

	}
}