package intranet.gestionLoginClasse.services.user
{
    import intranet.gestionLoginClasse.entity.UserVO;

    public class UserDatas
    {
        private var _userVO:UserVO;
        private var _userObj:Object;

        public function UserDatas()
        {
            new UserVO();
        }

        public function get userVO():UserVO
        {
            return _userVO;
        }

        public function set userVO(value:UserVO):void
        {
            _userVO = value;
        }

        public function get userObj():Object
        {
            return _userObj;
        }

        public function set userObj(value:Object):void
        {
            _userObj = value;
        }
    }
}