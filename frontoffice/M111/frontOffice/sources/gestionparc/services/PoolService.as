package gestionparc.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.event.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	public class PoolService extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					GLOBALES
		//--------------------------------------------------------------------------------------------//

		public var listeActionsProfile		:ArrayCollection;
		public var listePoolsValues			:ArrayCollection;
	
	//--------------------------------------------------------------------------------------------//
	//					CONSRUCTEUR
	//--------------------------------------------------------------------------------------------//	
		
		public function PoolService()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLICS
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					PROCEDURES
		//--------------------------------------------------------------------------------------------//

		public function fournirListeActionProfilCommande(idprofil:int): void
		{
			listeActionsProfile = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.PoolService",
																				"fournirListeActionProfilCommande",
																				fournirListeActionProfilCommandeResultHandler);
			
			RemoteObjectUtil.callService(op, idprofil);	
		}
		
		public function fournirPools(isrevendeur:int): void
		{
			listePoolsValues = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.PoolService",
																				"fournirPools",
																				fournirPoolsResultHandler);
			
			RemoteObjectUtil.callService(op, isrevendeur);	
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATES
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					RESULT EVENT PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function fournirListeActionProfilCommandeResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var actionsArray	:ArrayCollection 	= re.result as ArrayCollection;
				var lenActions		:int 				= actionsArray.length;
				
				for(var i:int = 0; i < lenActions;i++)
				{
					if(actionsArray[i].IDINV_ACTIONS == 2066 || actionsArray[i].IDINV_ACTIONS == 2216)
					{
						var actionProfile:Object = new Object();
						
						if(actionsArray[i].IDINV_ACTIONS == 2066)
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.MOBILE		= "SEGMENT";
							actionProfile.ACTIF			= formatInteger(actionsArray[i].SELECTED);
						}
						else
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.FIXE			= "SEGMENT";
							actionProfile.ACTIF			= formatInteger(actionsArray[i].SELECTED);
						}
						
						listeActionsProfile.addItem(actionProfile);
					}
				}
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_PRF_ACTIONS));
			}
		}
		
		private function fournirPoolsResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				var pools		:ArrayCollection 	= re.result as ArrayCollection;
				var lenPools	:int 				= pools.length;
				
				for(var i:int = 0; i < lenPools;i++)
				{
					listePoolsValues.addItem(pools[i]);
				}
				
				dispatchEvent(new Event('POOLS_LISTED'));
			}
			else
				ConsoviewAlert.afficherError('Récupération des pools impossible', 'Consoview');
		}
		
		
		public function formatInteger(intToFormat:int):Boolean
		{
			var bool:Boolean = false;
			
			if(intToFormat == 1)
				bool = true;
			
			return bool;
		}
		
	}
}