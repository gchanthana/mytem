package univers.parametres.gestionnaire
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class FicheGestionnaire extends FicheGestionnaireIHM
	{
		private var col_gestionnaire : ArrayCollection = new ArrayCollection();
		public function FicheGestionnaire()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer);
		}
		private function initIHM(evt : FlexEvent):void
		{
			bt_autorisation.addEventListener(MouseEvent.CLICK,bt_autorisation_handler);
			getLoginOfGroupe();
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			inputFiltreTousLesLogins.addEventListener(Event.CHANGE,filtreTousLesLogins);
			dg_gestionnaire.addEventListener(Event.CHANGE,dg_gestionnaire_handler);
			
						
		}
		private function bt_autorisation_handler(evt : MouseEvent):void
		{
			if(dg_gestionnaire.selectedIndex!=-1){
				var panel_gestionAuto : GestionAutorisationAction = new GestionAutorisationAction(this);
				PopUpManager.addPopUp(panel_gestionAuto,this,true);
				PopUpManager.centerPopUp(panel_gestionAuto);
			}
		}
		private function dg_gestionnaire_handler(evt : Event):void
		{
			labNom.text = dg_gestionnaire.selectedItem.LOGIN_NOM;
			labPrenom.text =dg_gestionnaire.selectedItem.LOGIN_PRENOM;
		}
		//Filtre 
		private function filtreTousLesLogins(e:Event):void{
			col_gestionnaire.filterFunction=filtreGridtousLesLogins;
			col_gestionnaire.refresh(); 
		}
		public function filtreGridtousLesLogins(item:Object):Boolean {
	
			if (((item.LOGIN_NOM as String).toLowerCase()).search(inputFiltreTousLesLogins.text.toLowerCase())!=-1 || ((item.LOGIN_PRENOM as String).toLowerCase()).search(inputFiltreTousLesLogins.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
			
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
			
		}
		public function getLoginOfGroupe():void
		{
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
			var PERIMETRE_INDEX : int = perimetreObj.PERIMETRE_INDEX; // idPerimetre ( groupe client) 
						
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
							"getAbonnement",
							resultGetLoginOfGroupe,erreur);
			
			RemoteObjectUtil.callService(opData,GROUPE_INDEX);
		}
		private function resultGetLoginOfGroupe(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			//trace(ObjectUtil.toString(tmpArr));
			/*for(i=0; i < tmpArr.length;i++){	
							
				var  item : Gestionnaire = new Gestionnaire();
				item.id=tmpArr[i].APP_LOGINID;
				item.nom=tmpArr[i].LOGIN_NOM;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
				item.login=tmpArr[i].LOGIN_EMAIL
				col_gestionnaire.addItem(item);
			}
			
			dg_gestionnaire.dataProvider = col_gestionnaire;*/
			col_gestionnaire = re.result as ArrayCollection;
			dg_gestionnaire.dataProvider = col_gestionnaire;
		}
		private function erreur(evt : FaultEvent):void
		{
			Alert.show("Erreur fiche gestionnaire"+evt.toString());	
		}
		private function change():void
		{
			
		}
		
		
	}
}