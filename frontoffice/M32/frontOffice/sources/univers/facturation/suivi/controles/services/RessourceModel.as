//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 20, 2011								*
//	owner  : Consotel								*
//***************************************************
package univers.facturation.suivi.controles.services
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import univers.facturation.suivi.controles.event.InventaireEvent;
	import univers.facturation.suivi.controles.event.RessourceEvent;
	import univers.facturation.suivi.controles.vo.StatutVO;
		[Bindable]	public class RessourceModel extends EventDispatcher	{		public var listStatus:ArrayCollection;		/**		 * le nombre de ressource dont le statut a été mis a jour depuis le dernier appel		 * -1 si lappel est en échec		 */				public var lastNbRessStatusUpDated:Number = 0; 						/**		 * le nombre de ressource dont l'état d'inventaire a été mis a jour depuis le dernier appel		 * -1 si lappel est en échec 		 */				public var lastNbRessInvStateUpDated:Number = 0;						/**		 * le resultat de la dèrnière opération de rapprochement		 * -1 si lappel est en échec 		 */				public var lastMergeProductResult:Number = 0;						public function RessourceModel(target:IEventDispatcher=null)		{			super(target);						createStatus();		}						internal function updateStatus(value:Number):void		{			lastNbRessStatusUpDated = value;			dispatchEvent(new RessourceEvent(RessourceEvent.UPDATE_STATUS_EVENT))			}				internal function updateInventoryState(value:Number):void		{			lastNbRessInvStateUpDated = value;			dispatchEvent(new RessourceEvent(RessourceEvent.UPDATE_INVENTORY_STATE_EVENT))
		}				internal function mergeProduct(value:Number):void		{			lastMergeProductResult = value;			dispatchEvent(new RessourceEvent(RessourceEvent.MERGE_PRODUCT_EVENT))			}				//création de la liste des statut disponible		//0=cloturé, 1= non traité, 2= en cours de reclamation		private function createStatus():void		{			listStatus = new ArrayCollection();			var status:StatutVO;			status = new StatutVO(0,ResourceManager.getInstance().getString('M32', 'Cl_tur_'));			listStatus.addItem(status);						status = new StatutVO(1,ResourceManager.getInstance().getString('M32', 'Non_trait_'));			listStatus.addItem(status);						status = new StatutVO(2,ResourceManager.getInstance().getString('M32', 'En_cours_de_r_clamation'));			listStatus.addItem(status);					}			}}