package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.reprendre
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasSimAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleReprendreLigneExt implements IRuleDisplayItem
	{
		public function RuleReprendreLigneExt()
		{
		}

		public function isDisplay():Boolean
		{
			var r2:IRuleDisplayItem=new HasExtAssociated();

			if (r2.isDisplay())
				return true;
			else
				return false;
		}
	}
}
