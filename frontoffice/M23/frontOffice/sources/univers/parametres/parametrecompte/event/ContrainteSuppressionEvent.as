package univers.parametres.parametrecompte.event
{
	import flash.events.Event;

	public class ContrainteSuppressionEvent extends Event
	{
		
		public static const VALIDATE_SUPPRESSION_POOL:String= "validateSuppressionPool";
		
		
		
		public function ContrainteSuppressionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}