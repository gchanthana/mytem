package univers.inventaire.lignesetservices.applicatif
{
	public class ActionRaccordementValues
	{
		public static const RACCORDER    			:Number = 1;
		public static const CHANGER_RACCORDEMENT    :Number = 2;
		public static const SUSPENDRE    			:Number = 3;
		public static const RESILIER			    :Number = 4;
		public static const REACTIVER			    :Number = 5;	
		public static const RACCORDER_OPHISTORIQUE  :Number = 51;
	}
}