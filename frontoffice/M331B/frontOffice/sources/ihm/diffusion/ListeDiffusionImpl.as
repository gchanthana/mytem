package ihm.diffusion
{
	import composants.util.ConsoviewAlert;
	
	import event.diffusion.DiffusionEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	
	import service.diffusion.activerdiffusion.ActiverDiffusionService;
	import service.diffusion.desactiverdiffusion.DesactiverDiffusionService;
	import service.diffusion.gethistoriqueenvoi.GetHistoriqueEnvoiService;
	import service.diffusion.listediffusion.ListeDiffusionService;
	import service.diffusion.supprimerdiffusion.SupprimerDiffusionService;
	import service.diffusion.testerDiffusion.TesterDiffusionService;
	
	import utils.DataGridDataExporter;
	
	import vo.diffusion.Diffusion;

	public class ListeDiffusionImpl extends VBox
	{
		[Bindable]public var dpDiffusion:ArrayCollection;
		[Bindable]public var ListeDiffusion:DataGrid;

		public var txtFiltreDiffusion:TextInput;
		public var listeDiffusionPlanifier:DataGrid;
		public var modifierDiffusion:ModifierDiffusionIHM;

		private var listeDiffusionService:ListeDiffusionService;
		private var deleteDiffusionService:SupprimerDiffusionService;
		private var historiqueEnvoiService:GetHistoriqueEnvoiService;
		private var activeDiffusionService:ActiverDiffusionService;
		private var desactiveDiffusionService:DesactiverDiffusionService;
		private var testerDiffusionService:TesterDiffusionService;

		private var listeDiffPlanifier:ListeDiffusionPlanifierIHM;
		private var idEvent:int;
		private var noContrat:int;
		
		private var newDiffusion:Diffusion;
		
		public function ListeDiffusionImpl()
		{
			super();
		}

		/**
		 * permet d'instancier les service permettant de récupérer les données du back
		 * @param event
		 */
		protected function listeDiffusionCreationCompleteHandler(event:FlexEvent):void
		{
			listeDiffusionService=new ListeDiffusionService();
			listeDiffusionService.model.addEventListener(DiffusionEvent.LISTE_DIFFUSION_RAPPORT_EVENT, fillListeDiffusion);
			getListeRapportDiffusion();

			historiqueEnvoiService=new GetHistoriqueEnvoiService();
			historiqueEnvoiService.model.addEventListener(DiffusionEvent.HISTORIQUE_ENVOI_EVENT, filHistoriqueEnvoi);

			deleteDiffusionService=new SupprimerDiffusionService();

			activeDiffusionService=new ActiverDiffusionService();
			desactiveDiffusionService=new DesactiverDiffusionService();
			
			testerDiffusionService=new TesterDiffusionService();

			txtFiltreDiffusion.addEventListener(Event.CHANGE, filterDataGrid); // pour filtrer le datagrid
		}

		/**
		 * rafraichir le contenu du data grid
		 */
		protected function image1_clickHandler(event:MouseEvent):void
		{
			getListeRapportDiffusion();
		}

		/**
		 * recupérer la liste des diffuions
		 */
		public function getListeRapportDiffusion():void
		{
			listeDiffusionService.listeRapportsEnDiffusion();
		}

		/**
		 * initialise le dataprovider pour affichier la liste des diffusion
		 */
		private function fillListeDiffusion(evt:DiffusionEvent):void
		{
			dpDiffusion=listeDiffusionService.model.diffusions;
		}

		/**
		 * supprimer une diffusion (masquer la diffusion)
		 */
		public function onClick_supprimerDiffusion(uneDiffuion:Diffusion):void
		{
			newDiffusion=uneDiffuion;
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M331', 'Voulez_vous_supprimer_cette_diffusion__'),ResourceManager.getInstance().getString('M331', 'Confirmation') ,supprimerHandler);
		}
		
		private function supprimerHandler (e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				dpDiffusion.removeItemAt(dpDiffusion.getItemIndex(newDiffusion)); // enlever l'item du datagrid		
				idEvent=newDiffusion.idDiffusion;
				noContrat=newDiffusion.noContrat;
				deleteDiffusionService.supprimerDiffusion(idEvent, noContrat);
			}
		}
		/**
		 * activer une diffusion
		 * @param uneDiffuion
		 */
		public function onClick_activerDiffusion(uneDiffuion:Diffusion):void
		{
			idEvent=uneDiffuion.idDiffusion;
			noContrat=uneDiffuion.noContrat;
			activeDiffusionService.activerDiffusion(idEvent, noContrat);
			getListeRapportDiffusion();
		}

		/**
		 * désactiver une diffusion
		 */
		public function onClick_desactiverDiffusion(uneDiffuion:Diffusion):void
		{
			idEvent=uneDiffuion.idDiffusion;
			noContrat=uneDiffuion.noContrat;
			desactiveDiffusionService.desactiverDiffusion(idEvent, noContrat);
			getListeRapportDiffusion();
		}

		/**
		 * creer un pop up de type ListeDiffusionPlanifierIHM et l'afficher
		 * @param uneDiffuion
		 */
		public function onClick_getHistoriqueEnvoi(uneDiffuion:Diffusion):void
		{
			idEvent=uneDiffuion.idDiffusion;
			noContrat=uneDiffuion.noContrat;
			historiqueEnvoiService.getHistoriqueEnvoi(idEvent, noContrat);
			newDiffusion=uneDiffuion;
			listeDiffPlanifier=PopUpManager.createPopUp(parentApplication as DisplayObject, ListeDiffusionPlanifierIHM, true) as ListeDiffusionPlanifierIHM;
			PopUpManager.centerPopUp(listeDiffPlanifier);
		}

		/**
		 *  récupérer l'historique d'envoi et fill le data grid
		 */
		private function filHistoriqueEnvoi(evt:DiffusionEvent):void
		{
			// dpPlanification : arrayCollection déclaré dans ListeDiffusionPlanifierImpl	
			listeDiffPlanifier.dpPlanification=historiqueEnvoiService.model.historiqueEnvoi;
			
			// afficher le nom du rapport
			listeDiffPlanifier.lblNomRapport.text=ResourceManager.getInstance().getString('M331', 'D_tails_des_ex_cutions_planifi_es_pour_l') + newDiffusion.nomRapport;
		}
		/**
		 * permet de tester la diffusion , il s'agit d'envoyer la diffuion aux cci 
		 */		
		public function onClick_testerDiffusion(uneDiffuion:Diffusion):void
		{
			var cci :String=uneDiffuion.destinatairesCaches;
			
			newDiffusion=uneDiffuion;
			
			if(cci == null)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M331', 'Veuillez_ajouter_un_destinataire_en_copi'), ResourceManager.getInstance().getString('M331', 'Erreur'));
			}
			else
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M331', 'Cette_diffusion_va__tre_faite___destinat')+ cci +ResourceManager.getInstance().getString('M331', '__et_ne_sera_pas_envoy_e_aux_destinatair'), 
					                                      ResourceManager.getInstance().getString('M331', 'Confirmation') ,alertClickHandler);
			}
		}
		/**
		 * permet de capter la réponse de l'utilisateur
		 */
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				idEvent=Math.abs(newDiffusion.idDiffusion);
				noContrat=newDiffusion.noContrat;
				testerDiffusionService.testerDiffusion(idEvent,noContrat);
			}
		}
		/**
		 * modifier une diffusion
		 * @param uneDiffuion
		 */
		public function onClick_modifierDiffusion(uneDiffuion:Diffusion):void
		{
			modifierDiffusion=PopUpManager.createPopUp(parentApplication as DisplayObject, ModifierDiffusionIHM, true) as ModifierDiffusionIHM;
			PopUpManager.centerPopUp(modifierDiffusion);

			/**
			 *  set les valeurs des composant ModifierDiffusionIHM
			 */
			modifierDiffusion.noContrat=uneDiffuion.noContrat;
			modifierDiffusion.idEvenement=Math.abs(uneDiffuion.idDiffusion); // obtenir la valeur absolute
			modifierDiffusion.idRapport=uneDiffuion.idRapport;
			modifierDiffusion.idRapportRacine=uneDiffuion.idRapportRacine;

			modifierDiffusion.initData();

			modifierDiffusion.setNbDest(uneDiffuion.idOrga, uneDiffuion.idProfil);
			modifierDiffusion.idFinPeriode=uneDiffuion.idFinPeriode; /** pour déterminer le selectedIndex de fin de periode traitée */		
			modifierDiffusion.jLancement=uneDiffuion.jourEnvoi;
			modifierDiffusion.listeOragnisationClient.model.idOrgaSelected=uneDiffuion.idOrga; // pour déterminer le selectedIndex de cette Orag

			modifierDiffusion.idOrga=uneDiffuion.idOrga;
			modifierDiffusion.ListeProfilByOrga(uneDiffuion.idOrga);
			modifierDiffusion.listeProfilOrganisation.model.idProfilOrgaSelected=uneDiffuion.idProfil; // pour déterminer le selectedIndex des profil 

			modifierDiffusion.idDiffusion=uneDiffuion.idDiffusion;
		
			modifierDiffusion.nomRapport=uneDiffuion.nomRapport;
			modifierDiffusion.mailCopieCache.text=uneDiffuion.destinatairesCaches;
			modifierDiffusion.nomFile.text=uneDiffuion.nomAttachementFile;
			modifierDiffusion.objetEmail.text=uneDiffuion.objetMail;
			modifierDiffusion.bodyMail.text=uneDiffuion.corpsMail;

			modifierDiffusion.libelleEvent=uneDiffuion.libelle;
			modifierDiffusion.periodeEtude=Number(uneDiffuion.periodeEtude); /** determiner   le selectedIndex de Combbox Plage d'étude */
			modifierDiffusion.modifierDiffusionRapport.model.addEventListener(DiffusionEvent.REFRESH_LIST_DIFFUSION_EVENT, getListeDiffusion);
		}

		private function getListeDiffusion(evt:DiffusionEvent):void
		{
			modifierDiffusion.closePopup(null);
			getListeRapportDiffusion();
		}

		/** --------------------------------------------------------------------------------------------------------------------------------*/
		/**
		 * exporter la data grid au format csv
		 * moduleRapportE0IHM : le module principal
		 */
		protected function onClick_exporter():void
		{
			if (dpDiffusion.length > 0)
			{
				/** ; pour le csv
				 false : pour exporter le datagrid si true : on exporte que l'element selectionné
				 */
				var dataToExport:String=DataGridDataExporter.getCSVString(ListeDiffusion, ";", "\n", false); // les donneés à exporter
				var url:String=RevisionM331B.urlBackoffice + "/fr/consotel/consoview/M331/csv/ExportCSV.cfm";

				DataGridDataExporter.exportCSVString(url, dataToExport, "export");
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M331', 'Pas_de_donn_es___exporter'), 'Consoview', null);
		}

		/** --------------------------------------------------------------------------------------------------------------------------------*/
		/**
		 * filtrer le data grid
		 * @param event : Event
		 * @return void
		 */
		private function filterDataGrid(evt:Event):void
		{
			if (dpDiffusion != null)
			{
				dpDiffusion.filterFunction=filterMyArrayCollection; // le nom de la fonction ;
				dpDiffusion.refresh();
			}
		}

		/**
		 * permet de filtrer le data grid
		 * @param item : Objet (chaque ligne de data provider)
		 * @return Boolean
		 */
		private function filterMyArrayCollection(item:Object):Boolean
		{
			var retour:Boolean=false;

			var itemNomRapport:String="";
			var itemNomOrga:String="";
			var itemNomProfil:String="";

			var searchString:String=StringUtil.trim(txtFiltreDiffusion.text.toLowerCase()); // le filtre

			itemNomRapport=(item as Diffusion).nomRapport.toLowerCase();
			itemNomOrga=(item as Diffusion).nomOrga.toLowerCase();
			itemNomProfil=(item as Diffusion).nomProfil.toLowerCase();

			if ((itemNomRapport.indexOf(searchString) > -1) || (itemNomOrga.indexOf(searchString) > -1) || (itemNomProfil.indexOf(searchString) > -1))
			{
				retour=true;
			}
			return retour;
		}
	/** --------------------------------------------------------------------------------------------------------------------------------*/
	}
}
