package univers.parametres.perimetres.perimetresV2.views
{
	import mx.resources.ResourceManager;
	import composants.controls.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.perimetres.perimetresV2.controls.GestionModele;
	
	[Event(name="niveauSelected")]
	
	[Bindable]
	public class ChoixTypeNoeudImpl extends TitleWindow
	{
		public var cmbTypes : ComboBox;
		public var txtLibelle : TextInputLabeled;
		public var btValider : Button;
		public var btAnnuler : Button;
		public var lblError : Label;
					
		
		
		private var _gestionModele : GestionModele;
		public function get gestionModele():GestionModele{
			return _gestionModele;
		}		
		
		public function set gestionModele(gestion : GestionModele):void{
			_gestionModele = gestion;
		}
		
		public function get selectedNiveauId():Number{
			if (cmbTypes.selectedIndex != -1){
				return Number(cmbTypes.selectedItem.IDORGA_NIVEAU);	
			}else{
				return -1;
			}
		}
		
		public function get selectedNiveauNumero():String{
			if (cmbTypes.selectedIndex != -1){
				return String(cmbTypes.selectedItem.NUMERO_NIVEAU);	
			}else{
				return "-";
			}
		}
		
		
		private var _dataprovider : Object;
		public function set dataProvider(data : Object):void{
			_dataprovider = data;	
		}
		
		public function ChoixTypeNoeudImpl()
		{	 
			super();
		}
		
		
		override protected function commitProperties():void{
			showCloseButton = true;
			btAnnuler.addEventListener(MouseEvent.CLICK,btAnnulerClickHandler);
			btValider.addEventListener(MouseEvent.CLICK,btValiderClickHandler);			
			addEventListener(CloseEvent.CLOSE,closeEventHandler);	
			
			cmbTypes.dataProvider = _dataprovider;
			cmbTypes.labelField = "TYPE_NIVEAU";
		}
		
		
		
		
		protected function btValiderClickHandler(me : MouseEvent):void{
			if (cmbTypes.selectedItem != null){
				
				dispatchEvent(new Event("niveauSelected"));
						
			}else{
				lblError.text = ResourceManager.getInstance().getString('M22', 'Vous_devez_s_lectionner_un_type_de_noeud');
			}	
		}		
		
		protected function btAnnulerClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);	
		}
		
		protected function closeEventHandler(ce :CloseEvent):void{
			PopUpManager.removePopUp(this);	
		}
	
			
		
		
		
		
		
	}
}