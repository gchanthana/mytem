package composants.tb.ligne.services
{
	import composants.util.ConsoviewAlert;
	
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;

	public class FicheInfoHandler
	{
		private var _model:FicheInfoModel;
		
		public function FicheInfoHandler(model:FicheInfoModel)
		{
			_model = model;
		}

		public function get model():FicheInfoModel
		{
			return _model;
		}

		public function set model(value:FicheInfoModel):void
		{
			_model = value;
		}
		
		internal function getFicheInfoResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateFicheInfo(event.result as Array);
			}
		}
		
		internal function setFonctionLigne_resultHandler(re:ResultEvent):void
		{
			if (re.result >= 0)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M311', 'La_fonction_de_la_ligne_a__t__modifi_e_'));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M311', 'Une_erreur_est_survenue_lors_de_l_enregi'), ResourceManager.getInstance().getString('M311', 'Erreur'));
			}
		}

	}
}