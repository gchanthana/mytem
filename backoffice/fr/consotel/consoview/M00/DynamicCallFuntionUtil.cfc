<cfcomponent name="DynamicCallFuntionUtil" output="false">    
	
	<!---
		GETMETHOD
	--->
	<cffunction name="getMethod" access="public" output="false" returntype="any">
		
		<cfargument name="class" type="any" required="true" hint="classe de la methode passée en parametre">
		<cfargument name="strMethod" type="string" required="true">
		
		<cfreturn ARGUMENTS.class[ARGUMENTS.strMethod]>
	</cffunction>
	
</cfcomponent>