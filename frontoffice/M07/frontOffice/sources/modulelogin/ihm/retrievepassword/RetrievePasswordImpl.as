package modulelogin.ihm.retrievepassword
{
    import composants.util.ConsoviewAlert;
    
    import flash.events.Event;
    import flash.events.FocusEvent;
    import flash.events.MouseEvent;
    
    import modulelogin.event.RetrievePasswordEvent;
    import modulelogin.services.retrievepassword.RetrievePasswordServices;
    
    import mx.containers.TitleWindow;
    import mx.controls.Button;
    import mx.controls.Label;
    import mx.controls.TextInput;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.resources.ResourceManager;
    import mx.validators.EmailValidator;
    import mx.validators.Validator;

    [Bindable]
    public class RetrievePasswordImpl extends TitleWindow
    {
        // composant ihm	
        public var btValider:Button;

        public var btAnnuler:Button;

        public var tiEmail:TextInput;

        public var emailValidator:EmailValidator;

        // appels distants
        public var myService:RetrievePasswordServices;

        public function RetrievePasswordImpl()
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
        }

        private function init(evt:FlexEvent):void
        {
            try
            {
                myService = new RetrievePasswordServices();
                initListeners();
            }
            catch (e:Error)
            {
                //trace("RetrievePasswordImpl-init() Problème d'initialisation du composant : " + e);
            }
        }

        private function initListeners():void
        {
            btValider.addEventListener(MouseEvent.CLICK, onValidateHandler);
            btAnnuler.addEventListener(MouseEvent.CLICK, onCancelHandler);
            this.addEventListener(CloseEvent.CLOSE, onCancelHandler);
        }

        /********************************************************
         *   HANDLER
         *******************************************************/
        public function onValidateHandler(evt:MouseEvent):void
        {
            try
            {
                if (runValidators())
                {
                    myService.myHandlers.addEventListener(RetrievePasswordEvent.PASSWORD_EMAILED, onServiceResultHandler);
                    myService.myHandlers.addEventListener(RetrievePasswordEvent.USER_NOT_FOUND, onServiceResultHandler);
                    myService.getUserPassword(tiEmail.text, resourceManager.localeChain[0]);
                }
            }
            catch (e:Error)
            {
                throw(e);
            }
        }

        private function onCancelHandler(evt:Event):void
        {
            PopUpManager.removePopUp(this);
        }

        private function onServiceResultHandler(evt:RetrievePasswordEvent):void
        {
            switch (evt.type)
            {
                case RetrievePasswordEvent.PASSWORD_EMAILED:
                    PopUpManager.removePopUp(this);
                    ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M05','Un_email_vous_a__t__adress__'));
                    break;
                case RetrievePasswordEvent.USER_NOT_FOUND:
                    ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M05','Aucun_utilisateur_associ____cette_adresse_n_a__t__trouv__'), null);
                    break;
                default:
                    break;
            }
            myService.myHandlers.removeEventListener(RetrievePasswordEvent.PASSWORD_EMAILED, onServiceResultHandler);
            myService.myHandlers.removeEventListener(RetrievePasswordEvent.USER_NOT_FOUND, onServiceResultHandler);
        }

        private function runValidators():Boolean
        {
            var _validators:Array = [ emailValidator ];
            var results:Array = Validator.validateAll(_validators);
            if (results.length > 0)
            {
                ConsoviewAlert.afficherSimpleAlert(ResourceManager.getInstance().getString('M05','Email_invalide__'), "");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}