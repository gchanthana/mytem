<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.process.ServiceHandlerM16"
			extends="fr.consotel.api.ibis.publisher.handler.ServiceHandler" hint="Exemple de handler de service">
	
	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
			
			<cfset mailObject	= createObject('component',"fr.consotel.consoview.M16.v2.SendMail")>
			
			<cflog  text="State: #eventObject.getEventTarget()#">
			
			<cfset SUPER.run(ARGUMENTS.eventObject)>
			
			<cfset ImportTmp	= "/container/M16/">
			<cfset contentMail 	= structnew()>			
			
			<cfset eventStatut 	= ARGUMENTS.eventObject.getReportStatus()>
			<cfset eventType	= ARGUMENTS.eventObject.getEventType()>
			<cfset eventUuid	= ARGUMENTS.eventObject.getEventTarget()>
			<cfset eventJobId	= ARGUMENTS.eventObject.getJobId()>

			<cfif eventType EQ 'BDC'>
			
				<cfset cmdeventobj  = createObject('component',"fr.consotel.api.ibis.publisher.handler.event.CommandeEvent")>
				<cfset logObject    = createObject('component',"fr.consotel.consoview.M16.LogsCommande")>
				<cfset attachObject = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>

				<!--- VERIFICATION DE L'ETAT DU RAPPORT --->
				<cfset rsltRapportObj 	= cmdeventobj.comandeEventInit(ARGUMENTS.eventObject)>
				
				<!--- RECUPERATION MAIL INSERE CONTENANT TOUS LES INFOS --->
				<cfset rsltLogObj 		= logObject.getlogsCommandeuuid(#eventUuid#)>

				<cfif rsltLogObj.RecordCount GT 0>
				
					<cfloop query="rsltLogObj">
			
						<cfset contentMail.BCC				= rsltLogObj.BCC>
						<cfset contentMail.CC				= rsltLogObj.CC>
						<cfset contentMail.CODE_TRAITEMENT	= rsltLogObj.CODE_TRAITEMENT>
						<cfset contentMail.DATE_LOG			= rsltLogObj.DATE_LOG>
						<cfset contentMail.GESTIONNAIRE		= rsltLogObj.GESTIONNAIRE>
						<cfset contentMail.IDCOMMANDE		= rsltLogObj.IDCOMMANDE>
						<cfset contentMail.IDCV_LOG_MAIL	= rsltLogObj.IDCV_LOG_MAIL>
						<cfset contentMail.IDGESTIONNAIRE	= rsltLogObj.IDGESTIONNAIRE>
						<cfset contentMail.IDRACINE			= rsltLogObj.IDRACINE>
						<cfset contentMail.JOB_ID			= rsltLogObj.JOB_ID>
						<cfset contentMail.MAIL				= rsltLogObj.MAIL>
						<cfset contentMail.MAIL_FROM		= rsltLogObj.MAIL_FROM>
						<cfset contentMail.MAIL_TO			= rsltLogObj.MAIL_TO>
						<cfset contentMail.MESSAGE			= rsltLogObj.MESSAGE>
						<cfset contentMail.MODULE			= rsltLogObj.MODULE>
						<cfset contentMail.RACINE			= rsltLogObj.RACINE>
						<cfset contentMail.STATUS			= rsltLogObj.STATUS>
						<cfset contentMail.SUBJECT			= rsltLogObj.SUBJECT>
						<cfset contentMail.UUID				= rsltLogObj.UUID>
						<cfset contentMail.CODE_APP			= rsltLogObj.CODE_APPLI>
						
					</cfloop>
				
					<cfset thisIdcommande = #contentMail.IDCOMMANDE#>
					
					<cfset thisCodeApp	  = getRacineCodeApp(contentMail.IDRACINE)>
					
					<!--- RECUPERATION DES PEICES JOINTES A JOINDRE AU MAIL OU NON --->
					<cfset rsltAttachOb = attachObject.fournirAttachements(#thisIdcommande#)>
				
					<cflog  text="State: #eventStatut# -  #contentMail.MAIL_FROM# to #contentMail.MAIL_TO#">
					
					<cfswitch expression="#eventStatut#">
 				 
						<cfcase value="S"> 
							
							<cfset mailObject.sendMailDBC(eventObject, contentMail, rsltAttachOb, thisCodeApp)>
							
						</cfcase>
						 
						<cfcase value="F"> 
							
							<cfset mailObject.sendMailDBCError(eventObject, contentMail, rsltAttachOb, thisCodeApp)>
							
						</cfcase>
						
						<cfcase value="R"> 
							
							<cfset mailObject.sendMailRespositoryError(eventObject, contentMail, rsltAttachOb, thisCodeApp)>
						
						</cfcase> 
						
						<cfdefaultcase> 
							
							<cfset mailObject.sendMailDBCError(eventObject, contentMail, rsltAttachOb, thisCodeApp)>
							
						</cfdefaultcase> 
						
					</cfswitch>
					
				<cfelse>
				
					<cfmail from="fromM16@consotel.fr" to="monitoring@saaswedo.com" subject="[BDC-ERROR] Pas d'infos sur le destinataire du mail" type="html" server="mail-cv.consotel.fr" >
						
						<cfoutput>
							JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br />
							REPORT_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br />
							REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br />
							BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br />
							EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br />
							EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br />
							EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#<br />
							EVENT_HANDLER : #ARGUMENTS.eventHandler#
						</cfoutput>
						
					</cfmail>
				
				</cfif>
				
			
			<cfelseif eventType EQ 'MOD'>
				
				<cfswitch expression="#eventStatut#">
 				 
					<cfcase value="S"> 
						
						<cfset mailObject.sendMailMOD(eventObject)>
						
					</cfcase>
					 
					<cfcase value="F"> 
							
							<cfset mailObject.sendMailMODError(eventObject)>
							
					</cfcase>
					
					<cfcase value="R"> 
						
						<cfset mailObject.sendMailMODRespositoryError(eventObject)>
					
					</cfcase> 
					
					<cfdefaultcase> 
						
						<cfset mailObject.sendMailMODError(eventObject)>
						
					</cfdefaultcase> 
					
				</cfswitch>
							
			</cfif>

	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
		
			<cfmail from="fromM16@consotel.fr" to="monitoring@saaswedo.com" subject="[BDC-ERROR]" type="html" server="mail-cv.consotel.fr" >
				<cfoutput>
					JOB_ID : #ARGUMENTS.eventObject.getJobId()#<br />
					REPORT_STATUS : #ARGUMENTS.eventObject.getReportStatus()#<br />
					REPORT_URL : #ARGUMENTS.eventObject.getReportPath()#<br />
					BIP_SERVER : #ARGUMENTS.eventObject.getBipServer()#<br />
					EVENT_TARGET : #ARGUMENTS.eventObject.getEventTarget()#<br />
					EVENT_TYPE : #ARGUMENTS.eventObject.getEventType()#<br />
					EVENT_DISPATCHER : #ARGUMENTS.eventObject.getEventDispatcher()#<br />
					EVENT_HANDLER : #ARGUMENTS.eventHandler#
				</cfoutput>
				<cfdump var="#ARGUMENTS.exceptionObject#" label="EXCEPTION">
			</cfmail>
		
	</cffunction>
	
	<cffunction access="private" name="getRacineCodeApp" returntype="Numeric" 
				hint="Récupère le code application de la racine passé en paramètre">
		<cfargument name="idRacine"  type="numeric" required="true" hint="l'id de la racine">		
		
		<cfset code_app = 1>		
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_cv_global.getRacineInfo" >
			<cfprocparam type="In" 	cfsqltype="CF_SQL_INTEGER" variable="p_idcommande" value="#idRacine#">	
			<cfprocresult name="p_racine_infos">
		</cfstoredproc>
		
		<cfloop query="p_racine_infos">
			<cfset code_app = p_racine_infos.CODE_APPLI>
		</cfloop>
		
		<cfreturn code_app>
	</cffunction>
	
</cfcomponent>
