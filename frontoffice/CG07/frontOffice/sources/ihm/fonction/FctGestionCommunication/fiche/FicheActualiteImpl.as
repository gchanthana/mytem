package ihm.fonction.FctGestionCommunication.fiche
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import entity.ActualiteVO;
	import entity.ApplicationVO;
	import entity.CodeAction;
	import entity.FileUpload;
	import entity.LangueVO;
	
	import event.fonction.fctGestionCommunication.FctGestionCommunicationEvent;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileReference;
	
	import ihm.fonction.FctGestionCommunication.FctGestionCommunicationImpl;
	import ihm.fonction.FctGestionCommunication.popup.AddPiecesJointesIHM;
	import ihm.fonction.FctGestionCommunication.popup.ListePiecesJointesIHM;
	import ihm.fonction.FctGestionCommunication.popup.ListePiecesJointesToLinkIHM;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.LinkButton;
	import mx.controls.RichTextEditor;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.Validator;
	
	import service.*;
	import service.fonction.FctGestionCommunication.FctGestionCommunicationService;
	import service.pieceJointe.PiecesJointesService;
	import service.upload.UploadFileService;
	
	import utils.formator.Formator;
	import utils.utilsForFonction.UtilsCommunication;
	import utils.validator.ComboBoxValidator;
	
	[Bindable]
	public class FicheActualiteImpl extends TitleWindowBounds
	{
		//----------- VARIABLES -----------//
		
		private var _objComService			:FctGestionCommunicationService;
		private var _objPJointes			:PiecesJointesService;
		private var _fctGestionCom			:FctGestionCommunicationImpl;
		private var _addPJ					:AddPiecesJointesIHM;
		private var _listePJ				:ListePiecesJointesIHM;
		private var _listePJToLink			:ListePiecesJointesToLinkIHM;
		private var _fileUploadSrv			:UploadFileService = new UploadFileService();
		
		private var text_msg_upload			:String = ResourceManager.getInstance().getString('M28', 'Fichier_s__attach__s_');
		
		private var _langueSelectedForActu	:ArrayCollection = new ArrayCollection();
		private var _actuSelected			:ActualiteVO;
		private var _fileRef				:FileReference;
		private var _urlUpload				:String = moduleConsoleGestionIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M28/upload/processUpload.cfm";
		private var _UUIDFile				:String;
		private var _nameFile				:String;
		private var _sizeFile				:String;
		private var _dateParution			:String;
		private var _idActualite			:int;
		
		public var valApplication			:ComboBoxValidator;
		public var valLangue				:ComboBoxValidator;
		public var valTitre					:Validator;
		public var valTextAreaRTEditor		:Validator;
		
		public var infoActualite			:ActualiteVO;
		public var piecesjointes			:ArrayCollection = new ArrayCollection();
		public var arrayPJToSave			:ArrayCollection = new ArrayCollection();
		
		public var boolCreation				:Boolean = false;
		public var boolEdition				:Boolean = false;
		public var aAfficherEnCreation		:Boolean = false;
		public var aAfficherEnEdition		:Boolean = false;
		public var lkbAAfficherEnEdition	:Boolean = false;
		public var isEnabled				:Boolean = false;
		
		public var ckxAccueil				:CheckBox;
		public var cboApplication			:ComboBox;
		public var cboLangue				:ComboBox;
		public var lblApplication			:Label;
		public var lblLangue				:Label;
		public var lblPosition				:Label;
		public var lblAccueil				:Label;
		public var lblFichier				:Label;
		public var tiTitre					:TextInput;
		public var taTexte					:TextArea;
		public var rtEditor					:RichTextEditor;
		public var btnLinkToFile			:Button;
		public var btnFichier				:Button;
		public var btnAjouterPJ				:Button;
		public var btnListePJ				:Button;
		public var btnTestURL				:Button;
		public var btnValider				:Button;
		public var btnAnnuler				:Button;
		public var lkbFichier				:LinkButton;
		public var dfDateParution			:DateField;
		public var imgLangue				:Image;
		public var drapeau					:String;
		
		[Embed(source='/assets/images/link_to_file.png')]
		public static var linkToFile:Class;
		
		
		//----------- METHODES -----------//
		
		/* */
		public function FicheActualiteImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
			_objComService = new FctGestionCommunicationService();
			infoActualite = new ActualiteVO();
		}
		
		/* au Creation Complete - Handler */
		private function init(fle:FlexEvent):void
		{
			initListeners();
			initRtEditor();
			initData();
		}
		
		/* initialisation listeners */
		private function initListeners():void
		{
			btnValider.addEventListener(MouseEvent.CLICK, validerActualiteHandler);
			btnAnnuler.addEventListener(MouseEvent.CLICK, closePopupHandler);
			cboApplication.addEventListener(ListEvent.CHANGE, getLangueByAppHandler);
			dfDateParution.addEventListener(CalendarLayoutChangeEvent.CHANGE, dateSelectedHandler);
			
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.INFOS_ACTUALITE_EVENT, getInfosActuHandler);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.LISTE_APPLICATION_EVENT, getListeAppliHandler);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.LISTE_LANGUE_EVENT, getListeLangueHandler);
			this.addEventListener(FctGestionCommunicationEvent.ACTUALITE_MISE_A_JOUR_EVENT,updateDataProviderhandler);
			
			btnAjouterPJ.addEventListener(MouseEvent.CLICK,openPopupAjoutPJHandler);
			btnListePJ.addEventListener(MouseEvent.CLICK,openPopupListePJHandler);
			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.CREATE_ACTUALITE_EVENT,creationActualiteHandler);
//			objComService.myDatas.addEventListener(FctGestionCommunicationEvent.ADD_FILE_TO_ACTUALITE_EVENT,addFileToActualiteHandler);
		}
				
		/* Ajout d'un bouton dans le RichTextEditor */
		private function initRtEditor():void
		{
			btnLinkToFile = new Button();
			btnLinkToFile.id = "btnLinkToFile";
			btnLinkToFile.setStyle("icon",linkToFile);
			btnLinkToFile.toolTip = ResourceManager.getInstance().getString('M28', 'Liste_des_liens_des_PJ');
			btnLinkToFile.addEventListener(MouseEvent.CLICK,openPopupListePJToLinkHandler);
			rtEditor.toolbar.addChild(btnLinkToFile);
		}
		
		/* initialisation data */
		private function initData():void
		{
			initDataListeApplication(); // recupere d'autres infos, tel les infos fichier joint et le lien
			
			if(_actuSelected != null) // edition - visualisation actualité
			{
				boolEdition = aAfficherEnEdition = true;
				showActualite(_actuSelected);
				initDataInfosActualite();
			}
			else // creation actualité
			{
				var todayDate:Date = new Date();
				dfDateParution.text = Formator.format(todayDate.toDateString(),"DD/MM/YYYY");
				dateParution = Formator.format(todayDate.toDateString(),"YYYY/MM/DD");
				ckxAccueil.selected = boolCreation = aAfficherEnCreation = true;
			}
		}
		
		/* appel du service pour liste application */
		private function initDataListeApplication():void
		{
			objComService.getListeApplication(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_APPLICATIONS);
		}
		
		/* appel du service pour info actualité */
		private function initDataInfosActualite():void
		{
			objComService.getInfosActualite(CvAccessManager.CURRENT_FUNCTION, CodeAction.INFOS_ACTUALITES, _actuSelected.IDACTUALITE);
		}
		
		/* lors du choix application dans combobox - Handler */
		private function getLangueByAppHandler(le:ListEvent):void
		{
			var codeapp:int = le.currentTarget.selectedItem.CODEAPP;
			var idlang:Number = (CvAccessManager.getSession().AppPARAMS.codeLangue.ID_LANGUE!=0) ? CvAccessManager.getSession().AppPARAMS.codeLangue.ID_LANGUE : 3; //langue de connexion
			objComService.getLangueByApp(CvAccessManager.CURRENT_FUNCTION, CodeAction.LISTE_LANGUES, codeapp, idlang);
		}
		
		/* retour liste application - Handler */
		private function getListeAppliHandler(evt:Event):void
		{
			cboApplication.dataProvider = objComService.myDatas.listeApplication;
			cboApplication.dropdown.dataProvider = objComService.myDatas.listeApplication;
			cboApplication.prompt = ResourceManager.getInstance().getString('M28', 'choisir_mon_application');
			cboApplication.selectedIndex = -1;
		}
		
		/* retour liste langue selon application - Handler */
		private function getListeLangueHandler(evt:Event):void
		{
			cboLangue.dataProvider = objComService.myDatas.listeLangue;
			cboLangue.dropdown.dataProvider = objComService.myDatas.listeLangue;
			
			cboLangue.selectedIndex = 0; // si une seule langue
			if((cboLangue.dataProvider as ArrayCollection).length > 1) // si plusieurs langues
			{
				cboLangue.prompt = ResourceManager.getInstance().getString('M28', 'choisir_ma_langue');
				cboLangue.selectedIndex = -1;
			}
		}
		
		/* retour infos d'une actualité - Handler */
		private function getInfosActuHandler(evt:Event):void
		{
			infoActualite = objComService.myDatas.infosActualite;
			
			if(infoActualite.FICHIERNOM != null)
				lkbAAfficherEnEdition = true;
			else
				lkbAAfficherEnEdition = false;
		}
				
		/* data actualité à afficher dans la fiche Actualité */
		private function showActualite(item:ActualiteVO):void
		{
			if(item != null)
			{
				cboApplication.selectedItem = item;
				lblApplication.text = item.APPLICATION;
				(item.VISIBLE) ? lblAccueil.text=ResourceManager.getInstance().getString('M28', 'Oui') : lblAccueil.text=ResourceManager.getInstance().getString('M28', 'Non');
				lblPosition.text = UtilsCommunication.strPosition(item.POSITION); 
				tiTitre.text = item.TITRE;
				rtEditor.htmlText = item.DETAILS;
				dfDateParution.text = Formator.format(item.DATEPARUTION,"DD/MM/YYYY");
				dateParution = Formator.format(item.DATEPARUTION,"YYYY/MM/DD"); //formatage pour creation ou modification éventuelle de la ficheActu
				switch(item.IDLANGUE)
				{
					case 1: drapeau = moduleConsoleGestionIHM.urlAssets+"/flag/GB.png"; imgLangue.toolTip = ResourceManager.getInstance().getString('M28', 'Anglais'); break;
					case 2: drapeau = moduleConsoleGestionIHM.urlAssets+"/flag/NL.png"; imgLangue.toolTip = ResourceManager.getInstance().getString('M28', 'N_erlandais'); break;
					case 3: drapeau = moduleConsoleGestionIHM.urlAssets+"/flag/FR.png"; imgLangue.toolTip = ResourceManager.getInstance().getString('M28', 'Fran_ais'); break;
					case 6: drapeau = moduleConsoleGestionIHM.urlAssets+"/flag/ES.png"; imgLangue.toolTip = ResourceManager.getInstance().getString('M28', 'Espagnol'); break;
					default:drapeau = null; imgLangue.toolTip = ""; break;
				}
			}
		}
		
		/* lorsque date est selectionnée dans DateField - Handler */
		private function dateSelectedHandler(clce:CalendarLayoutChangeEvent):void
		{
			var date:String = clce.newDate.toDateString();
			dateParution = Formator.format(date,"YYYY/MM/DD");
		}
		
		/* fermeture de la FicheActualite - Handler */
		private function closePopupHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/* valider la FicheActualite - Handler */
		private function validerActualiteHandler(me:MouseEvent):void
		{
			if(runValidator())
			{
				// crée ou update une actualité
				objComService.createActualite(CvAccessManager.CURRENT_FUNCTION,CodeAction.CREATE_ACTUALITE,createObjActu());
			}
		}
		
		/* process de creation ou de modif - Handler */
		private function creationActualiteHandler(fgce:FctGestionCommunicationEvent):void
		{
			idActualite = objComService.myDatas.idActualite;
			
			if(arrayPJToSave.length > 0)
				uploadFile(idActualite);
			else
				updateDataProvider(); //update le dataProvider
		}
		
		/* process d'upload de fichier, si il y a un fichier à uploader */
		private function uploadFile(idActu:int):void
		{
			uploadFilesSelected(idActu);
		}
		
		/* appel service upload files */
		private function uploadFilesSelected(idActu:int):void
		{
			_fileUploadSrv.uploadFiles(arrayPJToSave.source, idActu);
			_fileUploadSrv.addEventListener('FILES_UPLOADED', uploadFilesSelectedHandler);
		}
		
		/* dès que l'upload de fichier est ok - Handler */
		private function uploadFilesSelectedHandler(e:Event):void
		{
			updateDataProvider(); //update le dataProvider
		}
		
		/* update l'item du dataprovider listeActualité */
		private function updateDataProvider():void
		{
			if(_actuSelected != null) // modif actu
			{
				for each(var item:ActualiteVO in fctGestionCom.listeActualite)
				{
					if(item.IDACTUALITE == infoActualite.IDACTUALITE)
					{
						item.IDACTUALITE=infoActualite.IDACTUALITE;
						item.CODEAPP=infoActualite.CODEAPP;
						item.APPLICATION=infoActualite.APPLICATION;
						item.IDLANGUE=infoActualite.IDLANGUE;
						item.POSITION = infoActualite.POSITION;
						item.DATEPARUTION = dateParution;
						item.VISIBLE = infoActualite.VISIBLE;
						item.TITRE = tiTitre.text;
						item.DETAILS = rtEditor.htmlText;
						break;
					}
				}
				this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.ACTUALITE_MISE_A_JOUR_EVENT));
			}
			else // creation actu
			{
				// simuler un click du btn visualiser pour reload le dataprovider listeActualité
				fctGestionCom.btnVisualiser.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				msgAfterProcessFicheActualite();
			}
		}
		
		private function updateDataProviderhandler(evt:Event):void
		{			
			this.removeEventListener(FctGestionCommunicationEvent.ACTUALITE_MISE_A_JOUR_EVENT,updateDataProviderhandler);
			(fctGestionCom.dgListeActualite.dataProvider as ArrayCollection).refresh();
			(fctGestionCom.dgActuVisible.dataProvider as ArrayCollection).refresh();
			(fctGestionCom.dgActuNonVisible.dataProvider as ArrayCollection).refresh();
			
			msgAfterProcessFicheActualite();
		}
		
		/* message à afficher lors de la fin du process de validation de la création/modification de la fiche Actualité */
		private function msgAfterProcessFicheActualite():void
		{
			if(_actuSelected == null)
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M28', 'La_creation_de_l_actualit__a_bien_ete_effectu_'),Application.application as DisplayObject);
			else
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M28', 'La_modification_de_l_actualit__a_bien_ete_effectu_'),Application.application as DisplayObject);
			
			closePopupHandler(null);
		}
		
		/* initialise un objet ActualiteVo pour la methode createActualite */
		private function createObjActu():ActualiteVO
		{
			var objActu:ActualiteVO = new ActualiteVO();
			
			(_actuSelected != null) ? objActu.IDACTUALITE=infoActualite.IDACTUALITE : objActu.IDACTUALITE=-1;
			(_actuSelected != null) ? objActu.VISIBLE=infoActualite.VISIBLE : objActu.VISIBLE=ckxAccueil.selected;
			(cboApplication.selectedItem != null) ? objActu.CODEAPP=(cboApplication.selectedItem as ApplicationVO).CODEAPP : objActu.CODEAPP=infoActualite.CODEAPP;
			(cboLangue.selectedItem != null) ? objActu.IDLANGUE=(cboLangue.selectedItem as LangueVO).IDLANGUE : objActu.IDLANGUE=infoActualite.IDLANGUE ;
			
			if(_actuSelected == null) // si statut CREATION actualité
			{
				if(ckxAccueil.selected)
					objActu.POSITION = 1;
				else
					objActu.POSITION = 0;
			}
			else // sinon statut MODIFICATION actualité
			{
				objActu.POSITION = infoActualite.POSITION;
			}
			
			objActu.DATEPARUTION = dateParution;
			objActu.TITRE = tiTitre.text;
			objActu.DETAILS = rtEditor.htmlText;
			
			return objActu;
		}
		
		/* ouverture popup Ajout Piece Jointe */
		private function openPopupAjoutPJHandler(me:MouseEvent):void
		{
			_addPJ = new AddPiecesJointesIHM();
			PopUpManager.addPopUp(_addPJ,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(_addPJ);
			
			_addPJ.btnValid.addEventListener(MouseEvent.CLICK, validAjoutPJHanlder);
		}
		
		/* ouverture popup Liste Piece Jointe - Handler */
		private function openPopupListePJHandler(me:MouseEvent):void
		{
			_listePJ = new ListePiecesJointesIHM();
			_listePJ.idActu = (_actuSelected != null) ? _actuSelected.IDACTUALITE : 0; // si fiche en modifiction ou en creation
			
			if(_addPJ != null)
				_listePJ.currentFilesToSave = arrayPJToSave;
			
			PopUpManager.addPopUp(_listePJ,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(_listePJ);
		}
		
		/* ouverture popup Liste Piece Jointe To Link In RichTextEditor - Handler */
		private function openPopupListePJToLinkHandler(me:MouseEvent):void
		{
			var hasSelection:Boolean = (rtEditor.textArea.selectionBeginIndex != rtEditor.textArea.selectionEndIndex);
			
			if(hasSelection)
			{
				_listePJToLink = new ListePiecesJointesToLinkIHM();
				_listePJToLink.idActu = (_actuSelected != null) ? _actuSelected.IDACTUALITE : 0;
				_listePJToLink.fiche = this;
				
				if(_addPJ != null)
					_listePJToLink.currentFilesToSave = arrayPJToSave;
				
				PopUpManager.addPopUp(_listePJToLink,Application.application as DisplayObject,true);
				PopUpManager.centerPopUp(_listePJToLink);
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M28', 'Veuillez_selectionner_un_texte'), 'Gestion des communications', null);
			}
		}
		
		/* lors de la validation de l'ajout de PJ - Handler */
		private function validAjoutPJHanlder(me:MouseEvent):void
		{
			piecesjointes = mergeArrays(arrayPJToSave,_addPJ.currentFilesToSave);
		}
		
		/* merger des arrays */
		private function mergeArrays(arrayA:ArrayCollection, arrayB:ArrayCollection):ArrayCollection
		{			
			for each(var item:FileUpload in arrayB)
			{
				arrayA.addItem(item);
			}
			return arrayA;
		}
		
		/* verifie si les champs obligatoires sont saisis */
		private function runValidator():Boolean
		{
			// NB : activation de deux process de validation supplementaires (valApplication + valApplication) au click sur btn "Valider"
			valApplication.enabled = valLangue.enabled = true;
			var validators:Array = null;
			var results:Array = null;
			
			if(_actuSelected == null) 
				validators = [valApplication,valLangue,valTitre,valTextAreaRTEditor];
			else
				validators = [valTitre,valTextAreaRTEditor]; // les infos [application] et [langue] sont en lecture -> pas besoin de verif
			
			results = Validator.validateAll(validators);
			
			if(results.length > 0)
				return false; //ConsoviewAlert.afficherError("veuillez renseigner les champs obligatoires", "Attention");
			else
				return true;
		}
		
//		/* AS3 Regular expression pattern match for URLs that start with http:// and https:// */
//		private function checkProtocol (flashVarURL:String):Boolean
//		{
//			// Build the RegEx to test the URL.
//			var pattern:RegExp = new RegExp("^http[s]?\:\\/\\/([^\\/]+)");
//			//var pattern:RegExp = new RegExp("^http[s]?\:\\/\\/([^\\/]+)\\/");
//			var result:Object = pattern.exec(flashVarURL);
//			if (result == null || flashVarURL.length >= 4096)
//			{
//				return false;
//			}
//			return true;
//		}
		
//		/* creer xml langue traduction */
//		private function createXmlLangueTraduction():String
//		{
//			var myXML : String = "<rowset>";
//			for each(var item:ArticleLangueVO in langueSelectedForActu)
//			{
//				if(!item.LANGUE_PRINCIPALE)
//				{
//					myXML += "<row>";
//					myXML += "<label>" + item.TITRE + "</label>";
//					myXML += "<details>" + item.DETAIL + "</details>";
//					myXML += "<langid>" + item.IDLANGUE + "</langid>";
//					myXML += "</row>";
//				}
//			}
//			myXML += "</rowset>";
//			return myXML;
//		}

		/* */
//		private function createObjFile():Object
//		{
//			var objFile:Object = new Object();
//			var objNameFile:Object = UtilsCommunication.getDetailNameFile(nameFile);
//			
//			(_actuSelected != null) ? objFile.IDACTUALITE=infoActualite.IDACTUALITE : objFile.IDACTUALITE = -1;
//			objFile.UUID = _objPJointes.UUID;
//			objFile.NOM = objNameFile.name;
//			objFile.EXTENSION = objNameFile.extension;
//			
//			return objFile;
//		}

		/* dès que le fichier est validée et enregistrée en base de données */
//		private function addFileToActualiteHandler(fgce:FctGestionCommunicationEvent):void
//		{
//			objComService.createActualite(CvAccessManager.CURRENT_FUNCTION,CodeAction.CREATE_ACTUALITE,createObjActu());
//		}
		
		
		//----------- GETTERS - SETTERS -----------//
		
		/* */
		public function set actuSelected(value:ActualiteVO):void
		{
			if(_actuSelected == value) 
				return;
			_actuSelected = value;
		}
		
		public function get objComService():FctGestionCommunicationService
		{
			return _objComService;
		}

		public function set objComService(value:FctGestionCommunicationService):void
		{
			_objComService = value;
		}
		
		public function get langueSelectedForActu():ArrayCollection
		{
			return _langueSelectedForActu;
		}

		public function set langueSelectedForActu(value:ArrayCollection):void
		{
			_langueSelectedForActu = value;
		}

		public function get nameFile():String
		{
			return _nameFile;
		}

		public function set nameFile(value:String):void
		{
			_nameFile = value;
		}

		public function get sizeFile():String
		{
			return _sizeFile;
		}

		public function set sizeFile(value:String):void
		{
			_sizeFile = value;
		}

		public function get UUIDFile():String
		{
			return _UUIDFile;
		}

		public function set UUIDFile(value:String):void
		{
			_UUIDFile = value;
		}

		public function get dateParution():String
		{
			return _dateParution;
		}

		public function set dateParution(value:String):void
		{
			_dateParution = value;
		}

		public function get idActualite():int
		{
			return _idActualite;
		}

		public function set idActualite(value:int):void
		{
			_idActualite = value;
		}

		public function get fctGestionCom():FctGestionCommunicationImpl
		{
			return _fctGestionCom;
		}

		public function set fctGestionCom(value:FctGestionCommunicationImpl):void
		{
			_fctGestionCom = value;
		}
	}
}