<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.E01" extends="fr.consotel.api.ibis.publisher.handler.process.E00"
hint="Traitement de mise à jour utilisant la procédure stockée : pkg_pfgp_suivi_chargement.valide_suivi_chargement. La source de donnée utilisée est ROCOFFRE">
	<cffunction access="private" name="updateId" returntype="void"
	hint="Appelle la procédure dont le nom complet est retourné par procedureFullName() avec updateEvent.getId() comme paramètre
	Un mail de notification d'erreur est envoyé au destinataire par défaut de l'API Reporting si la valeur retournée par la procédure est négative">
		<cfargument name="updateEvent" type="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent" required="true" hint="Provient de handleUpdateEvent()">
		<cfset eventImpl=ARGUMENTS["updateEvent"]>
		<cftry>
			<cfstoredproc datasource="#getDataSource()#" procedure="#procedureFullName()#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  value="#eventImpl.getId()#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#eventImpl.getStatus()#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="codeRetour">
			</cfstoredproc>
			<cfcatch type="any">
				<cfmail attributecollection="#MAIL_API.ERROR#" subject="Exception durant l'appel de la procédure : #procedureFullName()#">
					<cfoutput>#CFCATCH.Message#<br/>#CFCATCH.Detail#</cfoutput>
				</cfmail>
			</cfcatch>
		</cftry>
		<cfset var logMessage="Post process " & eventImpl.getPostProcess() & " update returns : " & codeRetour>
		<cfif codeRetour LT 0>
			<cfset logError(logMessage)>
			<cfset var MAIL_API=getMailProperties()>
			<cfmail attributecollection="#MAIL_API.ERROR#"
				subject="Statut d'erreur pour le traitement Evenement #eventImpl.getPostProcess()# avec l'ID : #eventImpl.getId()#">
				<cfoutput>
					Implémentation du traitement de mise à jour : #getMetadata(THIS).NAME#<br>
					La valeur retournée par la procédure <b>#procedureFullName()#</b> avec l'ID #eventImpl.getId()# : #codeRetour#<br><br>
					#eventImpl.getDeliveryInfos()#
				</cfoutput>
			</cfmail>
		<cfelse>
			<cfset logInfo(logMessage)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="procedureFullName" returntype="String"
	hint="Retourne le nom complet de la procédure appelée par ce traitement : pkg_pfgp_suivi_chargement.valide_suivi_chargement">
		<cfreturn "pkg_pfgp_suivi_chargement.valide_suivi_chargement_v2">
	</cffunction>
</cfcomponent>