package intranet.gestionLoginClasse
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.AbstractOperation;

    public class PopUpGestionCompteClient extends popUpGestionCompteClientIHM
    {
        [Bindable]
        private var listeGroupeClient:ArrayCollection = new ArrayCollection();
        private var idLogin:int;
        private var nomClient:String;
        private var fenetrePrincipale:GestionLogin;

        public function PopUpGestionCompteClient(fenetrePrincipale:GestionLogin, listeGroupeClient:ArrayCollection, idLogin:int, login:String):void
        {
            super();
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
            this.listeGroupeClient = listeGroupeClient;
            this.idLogin = idLogin;
            this.nomClient = login;
            this.fenetrePrincipale = fenetrePrincipale;
        }

        public function initIHM(event:Event):void
        {
            labLoginClient.text = nomClient;
            dgGroupeMaitre.dataProvider = listeGroupeClient;
            btCancel.addEventListener(MouseEvent.CLICK, removeMe);
            btValide.addEventListener(MouseEvent.CLICK, majCompte);
            inputFiltreGroupeClient.addEventListener(Event.CHANGE, filtreGroupeClient);
            addEventListener(CloseEvent.CLOSE, removeMe);
        }

        public function majCompte(event:Event):void
        {
            var idCompte:int;
            if (dgGroupeMaitre.selectedIndex != -1)
            {
                idCompte = dgGroupeMaitre.selectedItem.IDGROUPE_CLIENT;
                // Mise à jour du dg dg Login :	
                var opUpdateDgLogin:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoprod.GestionClient", "upd_racine_login", processMajCompte, null);
                RemoteObjectUtil.callService(opUpdateDgLogin, [ idLogin, idCompte ]);
            }
            else
            {
                Alert.show("sélectionnez un groupe");
            }
        }

        private function processMajCompte(event:Event):void
        {
            Alert.show("Enregistrement : OK");
            fenetrePrincipale.afficheLogin();
            //fenetrePrincipale.inputFiltreTousLesLogins.text="";
            PopUpManager.removePopUp(this);
        }

        private function removeMe(event:Event):void
        {
            PopUpManager.removePopUp(this);
        }

        public function filtreGridGroupeClient(item:Object):Boolean
        {
            if (((item.LIBELLE_GROUPE_CLIENT as String).toLowerCase()).search(inputFiltreGroupeClient.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        public function filtreGroupeClient(e:Event):void
        {
            listeGroupeClient.filterFunction = filtreGridGroupeClient;
            listeGroupeClient.refresh();
        }
    }
}