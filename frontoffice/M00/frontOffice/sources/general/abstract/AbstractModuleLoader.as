package general.abstract
{
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.ProgressBarMode;
	import mx.core.Application;
	import mx.core.Container;
	import mx.events.ModuleEvent;
	import mx.managers.SystemManager;
	import mx.modules.IModuleInfo;
	import mx.modules.ModuleManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class AbstractModuleLoader extends EventDispatcher
	{
		private var _container:Container;
		private var _dispobj:DisplayObject;
		private var infos:IModuleInfo;	
		private var _progress:ConsoProgressBar;
		private var _cache_dispobj:DisplayObject;
		private var _tmpKey:String = "";
		public function AbstractModuleLoader()
		{
		}
/* ----------------------------------------------------------------------------- 		
    DECHARGEMENT D'UN MODULE                             
   ----------------------------------------------------------------------------- */
	// PUBLIC	
		public function unloadModule():void
		{	
			if(dispobj)
			{	
				this.container.removeAllChildren();
				_dispobj=null;	
			}
			
			if(infos)
			{	
				infos.removeEventListener(ModuleEvent.ERROR, moduleErrorHandler);
				infos.removeEventListener(ModuleEvent.SETUP, moduleSetupHandler);
				infos.removeEventListener(ModuleEvent.READY, moduleReadyHandler);
				infos.unload();
				infos=null;
			}
		}
/* ----------------------------------------------------------------------------- 
 	CHARGEMENT D'UN MODULE                              
   ----------------------------------------------------------------------------- */
	// PUBLIC
		// CHARGEMENT D'UN MODULE DANS MYTEM
		public function loadModule(tmpFunctionKey:String):Boolean
		{	
			_tmpKey = tmpFunctionKey;
			if(this.container != null)
			{
				
				if(loadHomeFromCache())   return true
					
				
				var tmpGetModule:AbstractOperation=
					RemoteObjectUtil.getOperation("fr.consotel.consoview.module.ModuleManager",
						"getConsoviewModule",getConsoViewModuleResultHandler);
				RemoteObjectUtil.invokeService(tmpGetModule,tmpFunctionKey);
				return true;
			}
			else
				return false;
		}
		// CHARGEMENT D'UN MODULE DANS LA CONSOLE DE GESTION
		public function loadCgModule(tmpFunctionKey:String):Boolean
		{	
			_tmpKey = tmpFunctionKey;
			if(this.container != null)
			{
				
				if(loadHomeFromCache())   return true
				
				
				var tmpGetModule:AbstractOperation=
					RemoteObjectUtil.getOperation("fr.consotel.consoview.module.ModuleManager",
						"getCgModule",getCgModuleResultHandler);
				RemoteObjectUtil.invokeService(tmpGetModule,tmpFunctionKey);
				return true;
			}
			else
				return false;
		}
	// PRIVATE
		private function getConsoViewModuleResultHandler(e:ResultEvent):void
		{
			if(e.result != null && (e.result as ArrayCollection).length > 0){
				var ModuleInUse:String = "module"+(e.result as ArrayCollection)[0].FCT+"IHM.swf"
				infos=ModuleManager.getModule(ModuleInUse);
				if(infos)
				{
					if(container)
					{	
						this.container.removeAllChildren();
					}
					
					infos.addEventListener(ModuleEvent.ERROR, moduleErrorHandler);
					infos.addEventListener(ModuleEvent.SETUP, moduleSetupHandler);
					infos.addEventListener(ModuleEvent.READY, moduleReadyHandler);
					if(progress)
					{
						if(!progress.isPopUp)
						{
							progress.setSource(infos);
							progress.setCancelableItem(false);
							progress.show(Application.application as DisplayObject);
						}
					}
					else
					{
						progress.show(Application.application as DisplayObject);
					}
					infos.load();	
				}
			}else
			{
			}
		}
		private function getCgModuleResultHandler(e:ResultEvent):void
		{
			if(e.result is String && e.result.length > 0){
				var ModuleInUse:String = "moduleCg"+e.result+"IHM.swf"
				infos=ModuleManager.getModule(ModuleInUse);
				if(infos)
				{
					if(container)
					{	
						this.container.removeAllChildren();
					}
					
					infos.addEventListener(ModuleEvent.ERROR, moduleErrorHandler);
					infos.addEventListener(ModuleEvent.SETUP, moduleSetupHandler);
					infos.addEventListener(ModuleEvent.READY, moduleReadyHandler);
					if(progress)
					{
						if(!progress.isPopUp)
						{
							progress.setSource(infos);
							progress.setCancelableItem(false);
							progress.show(Application.application as DisplayObject);
						}
					}
					else
					{
						progress.show(Application.application as DisplayObject);
					}
					infos.load();	
				}
			}else
			{
			}
		}
		private function infosReady():void
		{
			
			progress.hide();
			progress.setCancelableItem(true);
			_dispobj= infos.factory.create() as DisplayObject;
			if(dispobj)
			{
//				dispobj.percentHeight=100;
//				dispobj.percentWidth=100;
				if(this.container != null)
				{
					this.container.removeAllChildren();
					this.container.addChild(dispobj);
					cacheHome();
				}
				dispatchEvent(new ModuleEvent(ModuleEvent.READY));
			}
		}
		
		private function cacheHome():void
		{
			if(_tmpKey == "HOME")
			{
				_cache_dispobj = _dispobj;
			}
		}
		private function loadHomeFromCache():Boolean
		{
			if(_tmpKey  == "HOME" && _cache_dispobj != null)
			{
				_dispobj = _cache_dispobj;
				if(this.container != null)
				{
					this.container.removeAllChildren();
					this.container.addChild(dispobj);
					cacheHome();
				}
				dispatchEvent(new ModuleEvent(ModuleEvent.READY));
				return true
			}
			return false
			
		}
		
		private function moduleErrorHandler(event:ModuleEvent):void
		{	
			trace("!! Erreur de chargement du module !!" );
			progress.hide();
			progress.setCancelableItem(true);
		}
		private function moduleSetupHandler(evt:ModuleEvent):void
		{	
			if (!evt.module) return;
			var url:String  = evt.module.url;
			if (url == null) 
			{       
				progress.hide();
				progress.setCancelableItem(true);
			} 
			else 
			{
				if(!progress.isPopUp)
				{
					progress.show(Application.application as DisplayObject);
					progress.setCancelableItem(false);
				}
			}
		}
		private function moduleReadyHandler(event:ModuleEvent):void
		{	
			if(infos
				&& infos.hasEventListener(ModuleEvent.READY))
				infos.removeEventListener(ModuleEvent.READY,moduleReadyHandler);
			progress.hide();
			progress.setCancelableItem(true);
			infosReady();
		}

		// GETTER SETTER
		public function get dispobj():DisplayObject
		{
			return _dispobj;
		}
		public function get container():Container
		{
			return _container;
		}
		public function set container(value:Container):void
		{
			_container = value;
		}

		public function get progress():ConsoProgressBar
		{
			return _progress;
		}

		public function set progress(value:ConsoProgressBar):void
		{
			_progress = value;
		}
	}
}