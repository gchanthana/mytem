package gestionparc.ihm.fiches.popup
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.UserAccessModelEvent;
	import gestionparc.services.useracess.UserAccessService;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class PopUpPinCodeDeviceImpl extends TitleWindow
	{
		public var tbxPinCode:TextInput;
		[Bindable]
		public var txtPassword:TextInput;

		[Bindable]
		public var btnValider:Button;

		[Bindable]
		public var lblError:Label;

		private var _currentEquipementInfos:AbstractMatriceParcVO;

		private var _userAccessSrv:UserAccessService;

		[Bindable]
		public var isAskCvPin:Boolean=false;
		[Bindable]
		public var isDroid:Boolean=false;
		[Bindable]
		public var isPinCodeEnabled:Boolean=true;


		[Bindable]
		public var currentTitle:String="";
		[Bindable]
		public var currentMessage:String="";
		[Bindable]
		public var currentInfos:String="";

		[Bindable]
		public var lblBtnCheckCvPassword:String=ResourceManager.getInstance().getString('M111', 'confirmer');
		;
		[Bindable]
		public var lblBtnValider:String=ResourceManager.getInstance().getString('M111', 'Valider');
		[Bindable]
		public var lblBtnAnnuler:String=ResourceManager.getInstance().getString('M111', 'Annuler');


		public function PopUpPinCodeDeviceImpl()
		{
			_userAccessSrv=new UserAccessService();
		}


		public function setInfos(currentEquipementInfos:AbstractMatriceParcVO, title:String, message:String, infos:String, isAskCvPin:Boolean, isDroid:Boolean, isPinCodeEnabled:Boolean=true):void
		{
			_currentEquipementInfos=currentEquipementInfos;

			this.currentTitle=title;
			this.currentMessage=message;
			this.currentInfos=infos;
			this.isAskCvPin=isAskCvPin;
			this.isDroid=isDroid;
			this.isPinCodeEnabled=isPinCodeEnabled;
		}

		public function getInfos():Object
		{
			var infos:Object=new Object();
			infos.CURRENTINFOS=_currentEquipementInfos;
			infos.PINCODE=tbxPinCode.text;

			return infos;
		}

		protected function closeHandler(ce:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function btnCheckCvPasswordClickHandler(me:MouseEvent):void
		{
			checkPassWord();
		}

		protected function btnCheckCvPasswordKeyboardHandler(ke:KeyboardEvent):void
		{
			if ((ke as KeyboardEvent).keyCode == 13)
				checkPassWord();
		}

		protected function tbxPinCodeKeyboardHandler(ke:KeyboardEvent):void
		{
			if ((ke as KeyboardEvent).keyCode == 13)
				btnValiderClickHandler(null);
		}

		protected function txtChange(e:Event):void
		{
			if (tbxPinCode.text.length > 0)
				btnValider.enabled=true;
			else
				btnValider.enabled=false;
		}

		protected function btnValiderClickHandler(me:MouseEvent):void
		{
			if (isDroid)
			{
				if (tbxPinCode.text != '' && tbxPinCode.text.length == 4)
					dispatchEvent(new Event('PIN_CODE_DEVICE'));
				else
					ConsoviewAlert.afficherAlertInfo("Veuillez saisir votre code pin (4 chiffres).", 'MDM', null);
			}
			else
				dispatchEvent(new Event('PIN_CODE_DEVICE'));
		}

		protected function btnAnnulerClickHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function checkPassWord():void
		{
			_userAccessSrv.checkPassWord(txtPassword.text);
			_userAccessSrv.model.addEventListener(UserAccessModelEvent.UAME_CHECK_PASSWORD_UPDATED, checkPassWordHandler);
		}

		private function checkPassWordHandler(uame:UserAccessModelEvent):void
		{
			if (_userAccessSrv.model.checkPassWordLastResult > 0)
			{
				isPinCodeEnabled=true;

				if (!isDroid)
					btnValider.enabled=true;

				lblError.text='';
			}
			else
			{
				isPinCodeEnabled=false;

				if (!isDroid)
					btnValider.enabled=false;

				lblError.text='Le mot de passe saisi est incorrect.';
			}
		}
	}
}
