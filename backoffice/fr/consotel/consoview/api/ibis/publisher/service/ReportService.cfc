<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.service.ReportService">
	<cfset init()>
	<cffunction name="init" returntype="fr.consotel.consoview.api.ibis.publisher.service.ReportService" access="private" hint="Constructor">
		<!--- NE FONCTIONNE PAS AVEC ETATCONSOGSM ACTUEL : XML_CONFIG IS UNDEFINED IN APPLICATION SCOPE
				<cfset var xmlConfig=createObject("component","fr.consotel.consoview.api.ibis.admin.configuration.ConfigurationProperties").init().getXmlConfiguration()>
		<cfset var xmlConfig=APPLICATION["XML_CONFIG"].getXmlConfiguration()>
		--->
		<!--- 
		<cfset VARIABLES["BIP_WEBSERVICE"]=createObject("webservice",TRIM(xmlConfig["PROPERTIES"]["VERSIONS"]["VERSION"]["REPORTING"]["SERVER"]["WSDL"].xmlText))>
		--->
		<cfset VARIABLES["BIP_WEBSERVICE"]=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="runReport" returntype="any" access="public" hint="Run BIP Report. Returns report response as a binary object (Use CFDUMP to view available methods)">
		<cfargument name="reportRequest" type="struct" required="true">
		<cfargument name="userId" type="string" required="true">
		<cfargument name="userPwd" type="string" required="true">
		<cfset var bipWebService=VARIABLES["BIP_WEBSERVICE"]>
		<cflog text="!!!!!!!!!!!!!!!"/>
		<cfreturn bipWebService.runReport(arguments.reportRequest,arguments.userId,arguments.userPwd)>
	</cffunction>
	
	<cffunction name="scheduleReport" returntype="any" access="public" hint="Schedule BIP Report and returns corresponding JOBID">
		<cfargument name="scheduleRequest" type="struct" required="true">
		<cfargument name="userId" type="string" required="true">
		<cfargument name="userPwd" type="string" required="true">
		<cfset var bipWebService=VARIABLES["BIP_WEBSERVICE"]>
		<cfset var tmpParams=arguments.scheduleRequest.reportRequest.parameterNameValues>
		
		<!--- DEBUG --->
		<cflog text="@@@@@@ DEBUT DU LOG @@@@@@ - PARAMETRES >>>> BIP">
		
		<cfloop index="i" from="1" to="#arrayLen(tmpParams)#">
			<cfset tmpValues=tmpParams[i].getValues()>
			<cflog text="PARAMETRE #tmpParams[i].getName()# - AVEC DES VALEUR(S) ? #isDefined('tmpValues')#">
			<cfif isDefined("tmpValues")>
				<cfloop index="j" from="1" to="#arrayLen(tmpValues)#">
					<cflog text="== VALEUR #j# POUR PARAMETRE #tmpParams[i].getName()# : #tmpValues[j]#">
				</cfloop>
			</cfif>
		</cfloop>
		<cflog text="@@@@@@ FIN DU LOG @@@@@@ - PARAMETRES >>>> BIP">
		<!--- DEBUG LOCALE --->
		<cflog text="*** ScheduleReport - jobLocale (attributeLocale) : #arguments.scheduleRequest.jobLocale# (#arguments.scheduleRequest.reportRequest.attributeLocale#) ***">
		<cfreturn bipWebService.scheduleReport(arguments.scheduleRequest,arguments.userId,arguments.userPwd)>
	</cffunction>
	
	<cffunction name="createReportRequest" returntype="struct" access="public" hint="Create a report request to run report (Use CFDUMP)">
		<cfargument name="xmlTask" type="XML" required="true">
		<cfargument name="userId" type="string" required="true">
		<cfargument name="userPwd" type="string" required="true">
		<cfset var bipWebService=VARIABLES["BIP_WEBSERVICE"]>
		<cfset var reportRequest=structNew()>
		<cfset var isValidFormat=FALSE>
		<cfset outputFormat=LCASE(TRIM(arguments.xmlTask["TASK"]["OUTPUT_FORMAT"].xmlText))>
		<cfif bipWebService.validateLogin(arguments.userId,arguments.userPwd) EQ TRUE>
			<cfset reportAbsPath=TRIM(arguments.xmlTask["TASK"]["REPORT_ABSOLUTE_PATH"].xmlText)>
			<cfif bipWebService.hasReportAccess(reportAbsPath,arguments.userId,arguments.userPwd) EQ TRUE>
				<cfset templateId=TRIM(arguments.xmlTask["TASK"]["TEMPLATE_ID"].xmlText)>
				<cfset reportDefinition=bipWebService.getReportDefinition(reportAbsPath,arguments.userId,arguments.userPwd)>
				<cfset templateArray=reportDefinition.getListOfTemplateFormatsLabelValues()>
				<cflog text="===== Nbre de template : #arrayLen(templateArray)#">
				<cfloop index="i" from="1" to="#arrayLen(templateArray)#">
					<cflog text="===== Nom du template choisi : #templateId# - Nom du template dans BIP : #templateArray[i].getTemplateId()#">
					<cfif templateId EQ templateArray[i].getTemplateId()>
						<cfset formatArray=templateArray[i].getListOfTemplateFormatLabelValue()>
						<cflog text="===== Nbre de formats autorises : #arrayLen(formatArray)#">
						<cfloop index="j" from="1" to="#arrayLen(formatArray)#">
							<cflog text="===== Format dans BIP #formatArray[j].getTemplateFormatValue()# - Format choisi par user cv : #outputFormat#">
							<cfif outputFormat EQ formatArray[j].getTemplateFormatValue()>
								<cfset reportRequest["reportAbsolutePath"]=reportAbsPath>
								<cfset reportRequest["attributeTemplate"]=templateId>
								<cfset reportRequest["attributeFormat"]=outputFormat>
<cflog text="1-/ @@@@@@@@@@@@@@@@@@@@@@@@@@"/>
								<cfif StructKeyExists(SESSION.USER,"GLOBALIZATION") AND Len(SESSION.USER.GLOBALIZATION) GT 0>
									<cflog text="********************* ReportService.attributeLocale : session.user.globalization defini">
									<cflog text="********************* ReportService.attributeLocale : Valeur = #SESSION.USER.GLOBALIZATION#">
									<cfset reportRequest["attributeLocale"]=#SESSION.USER.GLOBALIZATION#>
								<cfelse>
									<cflog text="********************* ReportService.attributeLocale : session.user.globalization non-defini">
									<cflog text="********************* ReportService.attributeLocale : DEBUG = #SESSION.USER.GLOBALIZATION#">
									<cflog text="********************* ReportService.attributeLocale : Valeur par défaut : fr_FR">
									<cfset reportRequest["attributeLocale"]="fr_FR">
								</cfif>
								<cfset reportRequest["parameterNameValues"]=reportDefinition.getReportParameterNameValues()>
								<cflog text="===== Format OK">
								<cfset isValidFormat=TRUE>
								<cfreturn reportRequest>
							</cfif>
						</cfloop>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>
		<cfif isValidFormat EQ FALSE>
			<cfset MAIL_SERVER="mail-cv@consotel.fr">
			<cfset HANDLER_FROM_ADDR="#CGI.SERVER_NAME#@consotel.fr">
			<cfset DEV_SUPPORT_ADDR="monitoring@saaswedo.com">
			<cfset EXCEPTION_SUBJECT_PREFIX="TASK-PROCESS BIP">
			<cflog text="Serveur BackOffice : #CGI.SERVER_NAME#">
			<cflog text="Serveur de mail (MAIL SERVER) : #MAIL_SERVER#">
			<cflog text="Expediteur (HANDLER_FROM_ADDR) : #HANDLER_FROM_ADDR#">
			<cflog text="Adresse Support de dev (DEV_SUPPORT_ADDR) : #DEV_SUPPORT_ADDR#">
			<cflog text="Prefixe du sujet (EXCEPTION_SUBJECT_PREFIX) : #EXCEPTION_SUBJECT_PREFIX#">
			<!--- ********************************************************************************** --->
			<cfset errorMsg="ERREUR FORMAT INVALIDE : " & outputFormat>
			<cflog text="#errorMsg#">
			<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
				type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
				<cfoutput>
					#errorMsg#<br />
					XDO : #reportAbsPath#<br />
				</cfoutput>
			</cfmail>
			<cfthrow type="Custom" message="INVALID FORMAT VALUE #outputFormat# for XDO : #reportAbsPath#">
		</cfif>
		<cfreturn reportRequest>
	</cffunction>
	
	<cffunction name="createScheduleRequest" returntype="struct" access="public" hint="Create a schedule request to schedule report (Use CFDUMP)">
		<cfargument name="xmlTask" type="XML" required="true">
		<cfargument name="userId" type="string" required="true">
		<cfargument name="userPwd" type="string" required="true">
		<cfset var bipWebService=VARIABLES["BIP_WEBSERVICE"]>
		<cfset var scheduleRequest=structNew()>
		<cfset var deliveryRequest=structNew()>
		<cfset scheduleRequest["reportRequest"]=createReportRequest(arguments.xmlTask,arguments.userId,arguments.userPwd)>
		<cfif structIsEmpty(scheduleRequest["reportRequest"]) EQ FALSE>
			<cfset deliveryXPath="/TASK/DELIVERY">
			<cfset deliveryArray=xmlSearch(arguments.xmlTask,deliveryXPath)>
			<cfif arrayLen(deliveryArray) EQ 1>
				<cfif (arrayLen(xmlSearch(arguments.xmlTask,deliveryXPath & "/JOB")) EQ 1) AND (arrayLen(xmlSearch(arguments.xmlTask,deliveryXPath & "/CHANNEL")) EQ 1)>
					<cfset scheduleRequest["deliveryRequest"]=getDeliveryRequest(UCASE(TRIM(arguments.xmlTask["TASK"]["DELIVERY"]["CHANNEL"].xmlText)))>
<cflog text="2-/ @@@@@@@@@@@@@@@@@@@@@@@@@@"/>
					<cfif StructKeyExists(SESSION.USER,"GLOBALIZATION") AND Len(SESSION.USER.GLOBALIZATION) GT 0>
						<cflog text="********************* ReportServicejobLocale : session.user.globalization defini">
						<cflog text="********************* ReportServicejobLocale : Valeur = #replace(SESSION.USER.GLOBALIZATION,'-','_')#">
						<cfset scheduleRequest["jobLocale"]=replace(SESSION.USER.GLOBALIZATION,'-','_')>
					<cfelse>
						<cflog text="********************* ReportServicejobLocale : session.user.globalization non-defini">
						<cflog text="********************* ReportServicejobLocale : DEBUG = #replace(SESSION.USER.GLOBALIZATION,'-','_')#">
						<cflog text="********************* ReportServicejobLocale : Valeur par défaut : fr_FR">
						<cfset scheduleRequest["jobLocale"]="fr_FR">
					</cfif>
					<cfset scheduleRequest["userJobName"]=TRIM(arguments.xmlTask["TASK"]["DELIVERY"]["JOB"].xmlText)>
				</cfif>
			</cfif>
		</cfif>
		<cflog text="*** Create ScheduleReport - jobLocale (attributeLocale) : #scheduleRequest.jobLocale# (#scheduleRequest.reportRequest.attributeLocale#) ***">
		<cfreturn scheduleRequest>
	</cffunction>
	
	<cffunction name="getParameters" returntype="array" access="public"
		hint="(USED FOR DEBUGGING) Returns parameters list defined in requestObject. Returned object is an array in which each element is a structure that represents parameter (Use CFDUMP)">
		<cfargument name="requestObject" type="struct" required="true" hint="Result from createReportRequest() OR createScheduleRequest()">
		<cfset var tmpRequestObject=arguments.requestObject>
		<cfset var tmpParamArray="NULL">
		<cfset var tmpParamInfosArray=arrayNew(1)>
			<cfif (structKeyExists(tmpRequestObject,"parameterNameValues") EQ TRUE) AND
						(arrayLen(tmpRequestObject["parameterNameValues"]) GT 0)>
				<cfset tmpParamArray=tmpRequestObject["parameterNameValues"]>
				<cfloop index="i" from="1" to="#arrayLen(tmpParamArray)#">
					<cfset tmpParamInfosArray[i]=structNew()>
					<cfset tmpParamInfosArray[i]["NAME"]=tmpParamArray[i].getName()>
					<cfset tmpValues=tmpParamArray[i].getValues()>
					<cfif isDefined("tmpValues") EQ TRUE>
						<cfset tmpParamInfosArray[i]["VALUES"]=tmpValues>
					<cfelse>
						<cfset tmpParamInfosArray[i]["VALUES"]=arrayNew(1)>
					</cfif>
				</cfloop>
		</cfif>
		<cfreturn tmpParamInfosArray>
	</cffunction>
	
	<cffunction name="changeParameterValues" returntype="boolean" access="public"
		hint="Changes values for parameterName defined in requestObject. MULTI VALUE ALLOWED is not checked. Returns FALSE if parameterName does not exists and TRUE otherwise">
		<cfargument name="requestObject" type="struct" required="true" hint="Result from createReportRequest() OR value of reportRequest KEY defined in createScheduleRequest() result">
		<cfargument name="parameterName" type="string" required="true" hint="Name of parameter to change values">
		<cfargument name="valueArray" type="array" required="true" hint="List of values for parameter parameterName as an array of string">
		<cfset var tmpRequestObject=arguments.requestObject>
		<cfset var tmpParamArray="NULL">
		
		<!--- DEBUG --->
		<cflog text="@@@@@@ DEBUT DU LOG @@@@@@ - PARAMETRES >>>> API RAPPORT">
			<cfset tmpValues=arguments.valueArray>
			<cflog text="PARAMETRE #arguments.parameterName# - AVEC DES VALEUR(S) ? #arrayLen(arguments.valueArray)#">
			<cfif arrayLen(arguments.valueArray) GT 0>
				<cfloop index="j" from="1" to="#arrayLen(arguments.valueArray)#">
					<cflog text="== VALEUR #j# POUR PARAMETRE #arguments.parameterName# : #arguments.valueArray[j]#">
				</cfloop>
			</cfif>
		<cflog text="@@@@@@ FIN DU LOG @@@@@@ - PARAMETRES >>>> API RAPPORT">
		<!--- DEBUG --->
		
		<cfif (structKeyExists(tmpRequestObject,"parameterNameValues") EQ TRUE) AND
					(arrayLen(tmpRequestObject["parameterNameValues"]) GT 0)>
			<cfset tmpParamArray=tmpRequestObject["parameterNameValues"]>
			<cfloop index="i" from="1" to="#arrayLen(tmpParamArray)#">
				<cfif UCASE(arguments.parameterName) EQ UCASE(tmpParamArray[i].getName())>
					<cfset tmpParamArray[i].setValues(arguments.valueArray)>
					<cfreturn TRUE>
				</cfif>
			</cfloop>
		</cfif>
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction name="getDeliveryRequest" returntype="struct" access="private">
		<cfargument name="deliveryChannel" type="string" required="true" hint="Available values are : LOCAL, FTP, EMAIL">
		<cfset var deliveryRequest=structNew()>
		<cfset deliveryClass="NULL">
		<cfswitch expression="#UCASE(arguments.deliveryChannel)#">
			<cfcase value="LOCAL">
				<cfset deliveryClass="fr.consotel.consoview.api.ibis.publisher.delivery.local.LocalDeliveryDefinitionSupport">
			</cfcase>
			<cfcase value="FTP">
				<cfset deliveryClass="fr.consotel.consoview.api.ibis.publisher.delivery.ftp.FtpDeliveryDefinitionSupport">
			</cfcase>
			<cfcase value="EMAIL">
				<cfset deliveryClass="fr.consotel.consoview.api.ibis.publisher.delivery.email.EmailDeliveryDefinitionSupport">
			</cfcase>
		</cfswitch>
		<cfif deliveryClass NEQ "NULL">
			<cfset deliveryDefinition=createObject("component",deliveryClass)>
			<cfset deliveryRequest[deliveryDefinition.getType()]=deliveryDefinition.getParameterInfos()>
		</cfif>
		<cfreturn deliveryRequest>
	</cffunction>
</cfcomponent>
