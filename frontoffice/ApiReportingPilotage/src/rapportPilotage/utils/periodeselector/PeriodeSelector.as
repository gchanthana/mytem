package rapportPilotage.utils.periodeselector
{
	
	import mx.collections.ArrayCollection;
	import rapportPilotage.vo.ValeurSelector;
	
	public class PeriodeSelector extends PeriodeSelector_IHM
	{
		private var aMonth : AMonth;
		private var _firstDay : Boolean = true;
		private var dateDebut : Date;
		private var dateFin : Date;
		
		[Bindable]private var moisDebut : String = "";	
		[Bindable]private var moisFin : String = "";	
		[Bindable]private var idPeriodeMoisDebut :Number;
		[Bindable]private var idPeriodeMoisFin :Number;
		
		[Bindable]private var dateMoisDebut :String;
		[Bindable]private var dateMoisFin :String;
		
		[Bindable]private var monthData:Array; //tableau contenant les mois dispo.
		
		
		public function PeriodeSelector()
		{			 
			super();			
		}
		/**
		 * Fonction qui retourne un tableau avec les dates de la periode [mois actuelle -13,mois actuelle-1]
		 * 
		 * @return  monthData : Array un tableau contenant les deates incluses dans la periode [mois actuelle -13,mois actuelle-1];
		 * 
		 * */
		public function getTabPeriode():Array{
			return monthData;
		}	
		// formate les tooltips du slider
		
		private function getSliderLabel(value:Number):String{
			
			if (Math.abs(slider.getThumbAt(0).xPosition-slider.mouseX) > Math.abs(slider.getThumbAt(1).xPosition-slider.mouseX))
			{			 
		    	return (monthData[value-1] as AMonth).getDateFin();
		 	} 
			else if (Math.abs(slider.getThumbAt(1).xPosition-slider.mouseX) >= Math.abs(slider.getThumbAt(0).xPosition-slider.mouseX))
			{		 		 
		 		
		 		return (monthData[value] as AMonth).getDateDebut();
		 	}
		 			 	
		 	return null;  	   
		}
		
		/*
		permet de initialiser le selector mono periode
		*/
		public function setValuePeriode(datesCollection :ArrayCollection):void
		{		
			var tabPeriode : Array = new Array();
			var idPeriodeMois :Number;
			var firstValue:String;
			var maDate:String;
			var libellePeriode:String;
			var firstDate:Date;
			var array:Array;
			var theMonth : AMonth;
			
			// creer un array des dates sous forme d'objet de type AMonth
			for(var i : int =0; i<datesCollection.length;i++)
			{
				maDate =(datesCollection.getItemAt(i) as ValeurSelector).dispo_mois+"/01";			
				idPeriodeMois=(datesCollection.getItemAt(i) as ValeurSelector).idPeriodeMois;
				libellePeriode=(datesCollection.getItemAt(i) as ValeurSelector).libelle_periode;
				array = maDate.split('/');
				firstDate = new Date(array[0],array[1],array[2]);			
				theMonth = new AMonth(firstDate);
				
				theMonth.idDateSelected = idPeriodeMois;
				theMonth.libellePeriodeSelected=libellePeriode;
				
				tabPeriode[i] = theMonth;		
			}			
			monthData = tabPeriode; 
			
			slider.maximum = monthData.length;// le nombre max autorise
			slider.values = [monthData.length -1, monthData.length];// positionner les deux curseurs
				
		
			slider.dataTipFormatFunction = getSliderLabel; 
			
			slider.getThumbAt(0).name = "getDateDebut";
			slider.getThumbAt(1).name = "getDateFin";
			
			slider.getThumbAt(0).setStyle("fillColors",["#909587","#909587"]);
			slider.getThumbAt(1).setStyle("fillColors",["#909587","#909587"]);	    		   	  	
			
		}
		
		/** 
		 * Fonction qui permet de récuperer les dates sélèctionnées
		 * @return Un Objet contenant les id des dates recupérées
		 * */
		public function getSelectedDate():Object
		{			
			var objSelectedDates:Object ={};
			
			if (_firstDay)			 
			{			
				idPeriodeMoisDebut= AMonth(getTabPeriode()[slider.values[0]]).idDateSelected;
				idPeriodeMoisFin = AMonth(getTabPeriode()[slider.values[1]-1]).idDateSelected;
			}else
			{
				
			}
			objSelectedDates.IdMoisDebut = idPeriodeMoisDebut;
			objSelectedDates.IdMoisFin = idPeriodeMoisFin;
					
			return objSelectedDates;
		}
		
		public function getSelectedLibellePeriode():Object
		{			
			var objSelectedLibellePeriode:Object ={};
			
			if (_firstDay)			 
			{			
				dateMoisDebut= AMonth(getTabPeriode()[slider.values[0]]).getDateDebut();
				dateMoisFin = AMonth(getTabPeriode()[slider.values[1]-1]).getDateFin();
			}
			objSelectedLibellePeriode.datePeriodeDebut = dateMoisDebut;
			objSelectedLibellePeriode.datePeriodeFin = dateMoisFin;
			
			return objSelectedLibellePeriode;
		}
		
	}
}


