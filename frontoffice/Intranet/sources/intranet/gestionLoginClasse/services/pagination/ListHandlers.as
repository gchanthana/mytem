package intranet.gestionLoginClasse.services.pagination
{
    import composants.util.ConsoviewAlert;
    import flash.events.EventDispatcher;
    import intranet.gestionLoginClasse.event.PaginateListEvent;
    import intranet.gestionLoginClasse.event.UserEvent;
    import mx.collections.ArrayCollection;
    import mx.rpc.events.ResultEvent;

    public class ListHandlers extends EventDispatcher
    {
        private var myDatas:ListDatas;

        public function ListHandlers(instanceOfDatas:ListDatas)
        {
            myDatas = instanceOfDatas;
        }

        //METHODE INTERNAL------------------------------------------------------------------------
        internal function getListeLoginsResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                myDatas.listLogin = new ArrayCollection();
                myDatas.listLogin = re.result as ArrayCollection;
                dispatchEvent(new PaginateListEvent(PaginateListEvent.LOGIN_LIST_LOADED_SUCCESS));
            }
            else
            {
                dispatchEvent(new PaginateListEvent(PaginateListEvent.LOGIN_LIST_LOADED_ERROR));
                trace("Erreur dans la récupération des le chargement du segement de la liste des utilisateurs");
            }
        }
    }
}