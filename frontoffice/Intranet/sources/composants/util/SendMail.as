package composants.util
{
	import mx.managers.PopUpManager;
	import mx.events.FlexEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import mx.messaging.Session;
	import univers.accueil.UniversAccueil;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	
	public class SendMail extends SendMailIHM
	{
		private var destinataireInterne:String="";
		private var mailExpediteur:String="";
		
		public function SendMail()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:FlexEvent):void {
			this.showCloseButton=true;
			btnClose.addEventListener(MouseEvent.CLICK,closeMe);
			btnAjout.addEventListener(MouseEvent.CLICK,sendTheMail);
			PopUpManager.centerPopUp(this);
			rteMessage.textArea.styleName="TextInputAccueil";
			//rteMessage.removeChild(rteMessage.linkTextInput);
			rteMessage.textArea.percentHeight=100;
		}
		
		private function closeMe(e:Event):void {
			PopUpManager.removePopUp(this);
		}
		
		public function initMail(module:String,sujet:String,dest:String):void {
			txtModule.text=module;
			txtSujet.text=sujet;
			txtExpediteur.text=CvAccessManager.getUser().PRENOM + " " + CvAccessManager.getUser().NOM;
			mailExpediteur=CvAccessManager.getUser().EMAIL;
			destinataireInterne=dest;
		}
		
		private function sendTheMail(me:MouseEvent):void {
			var op : AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.util.sendMail",
								"sendSingleMail",
								processSendTheMail,
								throwError);				
			RemoteObjectUtil.callService(op, 
				[mailExpediteur,destinataireInterne,txtcc.text,txtcci.text,txtModule.text,txtSujet.text,rteMessage.text,cbCopie.selected]);		
		}
		
		private function processSendTheMail(r:ResultEvent):void
		{
			Alert.show("Message Envoyé");
			closeMe(new Event(""));
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}
	}
}