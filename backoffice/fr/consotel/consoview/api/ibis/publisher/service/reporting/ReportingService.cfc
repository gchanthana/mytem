<cfcomponent name="ReportingService" displayname="fr.consotel.consoview.api.ibis.publisher.service.reporting.ReportingService">
	<cffunction name="runReport" returntype="any" access="public" description="Run report and returns report result as a base64 binary object. Returns NULL as String if fails.">
		<cfargument name="xmlTask" type="XML" required="true" hint="XML Task Definition">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfset var buildXmlConfig=buildXmlConfiguration(ARGUMENTS.xmlTask,ARGUMENTS.userId,ARGUMENTS.pwd)>
		<cfif buildXmlConfig NEQ "NULL">
			<cfreturn VARIABLES["API_SERVICE_OBJECT"].runReport(buildXmlConfig,ARGUMENTS.userId,ARGUMENTS.pwd)>
		<cfelse>
			<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION : UNABLE TO RUN REPORT" detail="Unable to build XML Configuration">
		</cfif>
		<cfreturn "NULL">
	</cffunction>
	
	<cffunction name="scheduleReport" returntype="numeric" access="public" description="Schedule report and returns schedule job_id.">
		<cfargument name="xmlTask" type="XML" required="true" hint="XML Task Definition">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfset var buildXmlConfig=buildXmlConfiguration(ARGUMENTS.xmlTask,ARGUMENTS.userId,ARGUMENTS.pwd)>
		<cfif buildXmlConfig NEQ "NULL">
			<cfreturn VARIABLES["API_SERVICE_OBJECT"].scheduleReport(buildXmlConfig,ARGUMENTS.xmlTask["TASK"]["JOB"].xmlText,ARGUMENTS.userId,ARGUMENTS.pwd)><br />
		<cfelse>
			<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION : UNABLE TO RUN REPORT" detail="Unable to build XML Configuration">
		</cfif>
		<cfreturn -1>
	</cffunction>
	
	<cffunction name="buildXmlConfiguration" returntype="XML" access="private" description="Build XML configuration used to execute report">
		<cfargument name="xmlTask" type="XML" required="true" hint="Report Task XML Definition">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
		<cfset var xmlConfig="NULL">
		<cfset var tmpParamName="NULL">
		<cftry>
			<!--- =========================================================================== --->
			<cfset VARIABLES["API_SERVICE_OBJECT"]=createObject("component","fr.consotel.consoview.api.ibis.publisher.service.XmlPublisherService").init()>
			<cfset apiService=VARIABLES["API_SERVICE_OBJECT"]>
			<!--- =========================================================================== --->
			<cfset xmlConfig=apiService.getReportDefaultXmlConfig(ARGUMENTS.xmlTask["TASK"]["REPORT_ABSOLUTE_PATH"].xmlText,ARGUMENTS.userId,ARGUMENTS.pwd)>
			<cfset setStatus=apiService.setTemplateId(xmlConfig,ARGUMENTS.xmlTask["TASK"]["TEMPLATE_ID"].xmlText)>
			<cfif setStatus NEQ TRUE>
				<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION" detail="Unable to set template ID">
			</cfif>
			<cfset setStatus=apiService.setOutputFormat(xmlConfig,ARGUMENTS.xmlTask["TASK"]["OUTPUT_FORMAT"].xmlText)>
			<cfif setStatus NEQ TRUE>
				<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION" detail="Unable to set Output Format">
			</cfif>
			<!--- =========================================================================== --->
			<cflog text="********************************** ReportingService.buildXmlConfiguration">
			<cfloop index="i" from="1" to="#arrayLen(ARGUMENTS.xmlTask.TASK.PARAMETERS.xmlChildren)#">
				<cfset setStatus=apiService.setParameterValues(xmlConfig,ARGUMENTS.xmlTask["TASK"]["PARAMETERS"].xmlChildren[i].xmlName,
															listToArray(ARGUMENTS.xmlTask["TASK"]["PARAMETERS"].xmlChildren[i].xmlText,","))>
				<cflog text="****************************** ReportingService.buildXmlConfiguration : nom = #ARGUMENTS.xmlTask["TASK"]["PARAMETERS"].xmlChildren[i].xmlName#">
				<cflog text="****************************** ________________________________________ text = #ARGUMENTS.xmlTask["TASK"]["PARAMETERS"].xmlChildren[i].xmlText#">
				<cfif setStatus NEQ TRUE>
					<cfset tmpParamName=ARGUMENTS.xmlTask["TASK"]["PARAMETERS"].xmlChildren[i].xmlName>
					<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION" detail="Unable to set parameter #tmpParamName#">
				</cfif>
			</cfloop>
			<!--- =========================================================================== --->
			<cfset deliveryDefinition=createObject("component",apiService.getDeliveryClass(TRIM(ARGUMENTS.xmlTask["TASK"]["DELIVERY"].xmlText))).init()>
			<cfset setStatus=deliveryDefinition.setParameter("remoteFile",ARGUMENTS.xmlTask["TASK"]["REMOTE_FILE"].xmlText)>
			<cfif setStatus NEQ TRUE>
				<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION" detail="Unable to set Delivery Parameter">
			</cfif>
			<cfset setStatus=apiService.setDeliveryConfig(xmlConfig,deliveryDefinition)>
			<cfif setStatus NEQ TRUE>
				<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION" detail="Unable to set Delivery Configuration">
			</cfif>
			<!--- =========================================================================== --->
			<cfcatch type="any">
				<cfthrow type="Custom" message="REPORTING SERVICE EXCEPTION : UNABLE BUILD XML CONFIGURATION" detail="#CFCATCH.MESSAGE#">
			</cfcatch>
		</cftry>
		<cfreturn xmlConfig>
	</cffunction>
</cfcomponent>
