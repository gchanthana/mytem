<cfcomponent author="Cedric" extends="fr.saaswedo.api.mdm.impl.AbstractService">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.airwatch.MdmService">
		<cfreturn "fr.saaswedo.api.mdm.impl.airwatch.MdmService">
	</cffunction>
	
	<!--- =========== Fonctions de gestion des services =========== --->

	<cffunction access="private" name="getDeviceService" returntype="Struct" hint="Retourne une copie des propriétés du Device qui seront
	fournies à la librairie de Remoting (REST). La structure retournée est une copie car son contenu peut etre modifié par les fonctions appelantes">
		<cftry>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var service={
				endpoint=infos.getEndpoint() & "/api/v1/mdm/devices",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfreturn DUPLICATE(service)>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH})>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- =========== Fonctions de gestion du cache de devices =========== --->

	<cffunction access="private" name="xmlDeviceKey" returntype="String"
	hint="Retourne la clé dans la structure retournée par findDeviceFromCache() et qui est associé au device provenant de l'API MDM du vendor">
		<cfreturn "xmlDevice">
	</cffunction>

	<!--- TODO: A implémenter pour Datalert --->
	<cffunction access="private" name="findDeviceFromCache" returntype="Struct" description="LOCK:Exclusive/ReadOnly"
	hint="Recherche le device dans le Parc MDM local puis récupère son statut sur le serveur MDM. Retourne une structure contenant :<br>
	- Clé xmlDeviceKey(): Structure contenant les infos provenant du MDM concernant le device ou une structure vide sinon (e.g Non enrollé sur le MDM)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var serialValue=ARGUMENTS.serialNumber>
		<cfset var imeiValue=ARGUMENTS.imei>
		<cfset var device={}>
		<cfset device[xmlDeviceKey()]={}>
		<!--- Infos d'identification MDM du device --->
		<cfset var mdmPool=new fr.saaswedo.api.mdm.impl.MdmPool(SESSION.PERIMETRE.ID_GROUPE)>
		<cfset var deviceId=mdmPool.getDeviceId(serialValue,imeiValue)>
		<!--- TRUE si le device n'est pas présent le parc MDM local --->
		<cfset var deviceNotInPool=TRUE>
		<!--- TRUE si le device n'est pas présent ou non enrollé sur le MDM --->
		<cfset var deviceNotInMDM=TRUE>
		<cfif isDefined("deviceId")>
			<cfif NOT structIsEmpty(deviceId) AND structKeyExists(deviceId,"MDMID")>
				<cfset var deviceStatus=UNMANAGED()>
				<cfset var deviceUUID=deviceId.MDMID>
				<cfset deviceNotInPool=FALSE>
				<!--- Récupération du device dans le MDM (Librairie Remoting : REST) --->
				<cfset var mdmServer=getMdmServer()>
				<cfset var infos=mdmServer.getMdmServerInfo()>
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset var params=[
					{type="header",name="accept",value="application/json"},
					{type="header",name="User-Agent",value="SaaSwedo"},
					{type="header",name="aw-tenant-code",value=infos.getToken()}
				]>
				<cfset service.endpoint=service.endpoint & "/" & deviceUUID>
				<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({
						UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],
						SERIAL=arguments.serialNumber,IMEI=arguments.imei,CLIENT_REQUEST={service=service,params=isDefined("params") ? params:[]}
					})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset var httpResponse=result[remoting.RESULT()]>
					<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
					<cfset device.RESULT=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({
							UUID=createUUID(),SERVER_RESPONSE=httpResponse,SERIAL=arguments.serialNumber,IMEI=arguments.imei,
							CLIENT_REQUEST={service=service,params=isDefined("params") ? params:[]}
						})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Device enrollé sur le MDM --->
							<cfif structKeyExists(jsonResponse,"enrollmentStatus")>
								<cfset device=extractDevice(jsonResponse)>
								<cfif compareNoCase(MANAGED(),getStatusValue(jsonResponse.enrollmentStatus)) EQ 0>
									<cfset deviceNotInMDM=FALSE>
									<cfset device[xmlDeviceKey()]=jsonResponse>
								</cfif>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
				<!--- Fin de la récupération du device dans le MDM (Librairie Remoting : REST) --->
			</cfif>
		</cfif>
		<!--- Stockage d'un log de warning:
		- Si le device n'est pas présent dans le parc MDM local
		- Si le device n'est pas enrollé ou n'est pas présent sur le MDM
		--->
		<cfif deviceNotInMDM EQ TRUE>
			<cfset var body="">
			<cfset var logUUID=createUUID()>
			<cfsavecontent variable="body">
				<cfoutput>
					Le device (SERIAL,IMEI) #serialValue#,#imeiValue#
					<cfif deviceNotInPool EQ TRUE>
						n'est pas présent dans le parc MDM local.
					<cfelse>
						est présent dans le parc MDM local.
						<cfif deviceNotInMDM EQ TRUE>
							Mais ce device n'est pas enrollé ou n'est pas présent sur le serveur MDM. 
						</cfif>
					</cfif>
				</cfoutput>
			</cfsavecontent>
			<cfset var logContent={UUID=logUUID,SERIAL=serialValue,IMEI=imeiValue,BODY=body}>
			<!--- Réponse HTTP du remoting --->
			<cfif structKeyExists(device,"RESULT")>
				<cfset logContent.SERVER_RESPONSE=device.RESULT>
			</cfif>
			<cfset storeWarning(logContent)>
		</cfif>
		<!--- Supression de la clé RESULT du résultat retourné --->
		<cfset structDelete(device,"RESULT",FALSE)>
		<cfreturn device>
	</cffunction>
	
	<cffunction access="private" name="extractDevice" returntype="Struct"
	hint="Retourne une structure contenant les infos utilisées le cache :<br>id_mdm (ID MDM dans le parc local),SERIAL,IMEI,OS,STATUS (Statut du device).
	Cette implémentation est est utilisée par la fonction findDeviceFromCache() de ce composant">
		<cfargument name="jsonDevice" type="Struct" required="true" hint="Device de l'API MDM">
		<!--- TODO: Peut etre utiliser l'adresse MAC Wifi comme valeur du SERIAL et IMEI quand ces derniers ne sont pas renseignés --->
		<cfset var jsonStruct=ARGUMENTS.jsonDevice>
		<cfset var deviceStatus=structKeyExists(jsonStruct,"enrollmentStatus") ? getStatusValue(jsonStruct.enrollmentStatus):UNMANAGED()>
		<cfset var device={
			serialNumber=structKeyExists(jsonStruct,"serialNumber") ? jsonStruct.serialNumber:"",
			IMEI=structKeyExists(jsonStruct,"IMEI") ? jsonStruct.IMEI:"",
			OS=getPlatformValue(structKeyExists(jsonStruct,"platform") ? jsonStruct.platform:""),
			WIFI_MAC=structKeyExists(jsonStruct,"macAddress") ? jsonStruct.macAddress:"",
			STATUS=deviceStatus,id_mdm=""
		}>
		<cfif structKeyExists(jsonStruct,"id") AND structKeyExists(jsonStruct.id,"value")>
			<cfset device.id_mdm=jsonStruct.id.value>
		</cfif>
		<cfreturn device>
	</cffunction>
	
	<!--- TODO: Vérifier si cette fonction est toujours utile pour Airwatch --->
	<!--- Cette fonction n'est pas utilisée dans ce composant. Elle sert aux implémentations qui héritent de ce composant --->
	<cffunction access="private" name="extractMdmProperty" returntype="Struct" hint="Retourne une structure contenant NAME (Propriété),VALUE (Valeur)
	<br>Elle correspond à l'équivalent de property dans la spécification de l'API MDM. Une structure vide est retournée si elle n'est pas reconnue">
		<cfargument name="property" type="String" required="true" hint="Nom de la propriété du provider MDM">
		<cfargument name="propertyValue" type="Any" required="true" hint="String ou une structure">
		<cfset var key=ARGUMENTS.property>
		<cfset var value=ARGUMENTS.propertyValue>
		<cfset var mdmProperty={}>
		<!--- Mappage des propriétés du provider MDM avec celles de la spécification de l'API MDM --->
		<cfif compareNoCase(key,"macAddress") EQ 0>
			<cfset mdmProperty={NAME="WIFI_MAC",VALUE=value}>
		<cfelseif compareNoCase(key,"platform") EQ 0>
			<cfset value=getPlatformValue(value)>
			<cfset mdmProperty={NAME="SYSTEM_PLATFORM",VALUE=value}>
		<cfelseif compareNoCase(key,"DeviceMCC") EQ 0>
			<cfset mdmProperty={NAME="DeviceMCC",VALUE=value}>
		<cfelseif compareNoCase(key,"phoneNumber") EQ 0>
			<cfset mdmProperty={NAME="PhoneNumber",VALUE=value}>
		<cfelseif compareNoCase(key,"RoamingStatus") EQ 0>
			<cfset mdmProperty={NAME="RoamingStatus",VALUE=yesNoFormat(value)}>
		<cfelseif compareNoCase(key,"VoiceRoamingEnabled") EQ 0>
			<cfset mdmProperty={NAME="VoiceRoamingEnabled",VALUE=yesNoFormat(value)}>
		<cfelseif compareNoCase(key,"DataRoamingEnabled") EQ 0>
			<cfset mdmProperty={NAME="DATA_ROAMING_ENABLED",VALUE=yesNoFormat(value)}>
		<!--- Structure de l'adresse IP : La valeur propertyValue doit etre celle associée à la clé IPAddress provenant du MDM --->
		<cfelseif compareNoCase(key,CELLULAR_IP_ADDRESS()) EQ 0>
			<cfset var ipAddress=structKeyExists(value,"CellularIPAddress") ? value.CellularIPAddress:"">
			<cfset mdmProperty={NAME=CELLULAR_IP_ADDRESS(),VALUE=ipAddress}>
		<cfelseif compareNoCase(key,ETHERNET_IP_ADDRESS()) EQ 0>
			<cfset var ipAddress=structKeyExists(value,"EthernetIPAddress") ? value.EthernetIPAddress:"">
			<cfset mdmProperty={NAME=ETHERNET_IP_ADDRESS(),VALUE=ipAddress}>
		<cfelseif compareNoCase(key,WIFI_IP_ADDRESS()) EQ 0>
			<cfset var ipAddress=structKeyExists(value,"WifiIPAddress") ? value.WifiIPAddress:"">
			<cfset mdmProperty={NAME=WIFI_IP_ADDRESS(),VALUE=ipAddress}>
		<!--- Infos de localisation --->
		<cfelseif compareNoCase(key,"Latitude") EQ 0>
			<cfset mdmProperty={NAME="GPS_LATITUDE_FROM_GPS",VALUE=VAL(value)}>
		<cfelseif compareNoCase(key,"Longitude") EQ 0>
			<cfset mdmProperty={NAME="GPS_LONGITUDE_FROM_GPS",VALUE=VAL(value)}>
		<cfelseif (key EQ "SampleTime")>
			<cfset mdmProperty={NAME="GPS_TIMESTAMP_FROM_GPS",VALUE=value}>
		</cfif>
		<cfreturn mdmProperty>
	</cffunction>
	
	<cffunction access="private" name="CELLULAR_IP_ADDRESS" returntype="String" hint="Clé à fournir à extractMdmProperty() pour l'adresse IP CellularIPAddress">
		<cfreturn "CELLULAR_IP_ADDRESS">
	</cffunction>
	
	<cffunction access="private" name="ETHERNET_IP_ADDRESS" returntype="String" hint="Clé à fournir à extractMdmProperty() pour l'adresse IP EthernetIPAddress">
		<cfreturn "ETHERNET_IP_ADDRESS">
	</cffunction>
	
	<cffunction access="private" name="WIFI_IP_ADDRESS" returntype="String" hint="Clé à fournir à extractMdmProperty() pour l'adresse IP WifiIPAddress">
		<cfreturn "WIFI_IP_ADDRESS">
	</cffunction>
	
	<!--- =========== Fonctions d'extraction des infos des devices =========== --->

	<!--- [feature/MYT-843] Cette implémentation est compatible avec ce qui est dans feature/MYT-843 pour le blocage du roaming --->
	<cffunction access="private" name="getPlatformValue" returntype="String" hint="Retourne une des valeurs ANDROID(),IOS(),UNKNOWN_OS()">
		<cfargument name="platformValue" type="String" required="true" hint="Valeur utilisée par le provider pour l'OS ou la plateforme du device">
		<cfset var devicePlatform=ARGUMENTS.platformValue>
		<cfif (findNoCase("iOS",devicePlatform) GT 0) OR (findNoCase("apple",devicePlatform) GT 0)>
			<cfreturn IOS()>
		<cfelseif findNoCase("Android",devicePlatform) GT 0>
			<cfreturn ANDROID()>
		<cfelse>
			<cfreturn UNKNOWN_OS()>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getStatusValue" returntype="String" hint="Retourne une des valeurs UNMANAGED(),MANAGED()">
		<cfargument name="statusValue" type="String" required="true" hint="Valeur utilisée par le provider pour le statut">
		<cfset var deviceStatus=ARGUMENTS.statusValue>
		<cfset var managedStatusList=["Enrolled"]>
		<cfloop array="#managedStatusList#" index="status">
			<cfif compareNoCase(deviceStatus,status) EQ 0>
				<cfreturn MANAGED()>
			</cfif>
		</cfloop>
		<cfreturn UNMANAGED()>
	</cffunction>
	
	<cffunction access="private" name="validStatus" returntype="Array" hint="Retourne la liste des statuts considérés comme valides">
		<cfset var listKey="VALID_STATUS">
		<cfif NOT structKeyExists(VARIABLES,listKey)>
			<!--- Liste des statuts compatible avec la version de l'alimentation du Parc MDM local --->
			<cfset VARIABLES[listKey]=["Enrolled"]>
		</cfif>
		<cfreturn VARIABLES[listKey]>
	</cffunction>
</cfcomponent>