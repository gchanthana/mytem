package rapport.service.chercherselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class RechercherSelectorHandler
	{
		private var _model:RechercherSelectorModel;
		
		public function RechercherSelectorHandler(_model:  RechercherSelectorModel):void
		{
			this._model = _model;
		}	
		internal function getSelectorResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateSelector(event.result as ArrayCollection);
			}
		}
	}
}