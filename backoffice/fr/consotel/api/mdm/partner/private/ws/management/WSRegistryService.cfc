<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSRegistryService" implements="fr.consotel.api.mdm.partner.private.ws.IWSRegistryService"
hint="Implémentation fournissant les méthodes du Service Registry Zenprise<br>
Les méthodes de cette implémentation sont soumises à la vérification de l'authentification. Le séparateur de chemin pour les clés est \ (WINDOWS)">
	<!--- fr.consotel.api.mdm.partner.private.ws.management.WSRegistryService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSRegistryService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
	
	<!--- Registry Service --->
	<cffunction access="public" name="addRegistryKey" returntype="Void" hint="Ajoute une nouvelle clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfset registryService.addRegistryKey(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="addRegistryValue" returntype="Void" hint="Ajoute une nouvelle propriété dans une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfargument name="value" type="String" required="true" hint="Valeur de la propriété">
		<cfargument name="valType" type="Numeric" required="true" hint="Type de la valeur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfset registryService.addRegistryValue(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"],ARGUMENTS["valName"],ARGUMENTS["value"],ARGUMENTS["valType"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="updateRegistryValue" returntype="Void" hint="Met à jour la valeur d'une propriété dans une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfargument name="value" type="String" required="true" hint="Valeur de la propriété">
		<cfargument name="valType" type="Numeric" required="true" hint="Type de la valeur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfset registryService.updateRegistryValue(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"],ARGUMENTS["valName"],ARGUMENTS["value"],ARGUMENTS["valType"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeRegistryKey" returntype="Void" hint="Supprime une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfset registryService.removeRegistryKey(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getRegistryValue" returntype="Any" hint="Retourne la valeur d'une propriété d'une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfset var soapRequest="">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfxml variable="soapRequest">
			<cfoutput>
				<soapenv:Envelope xmlns:zenprise="zenprise" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
					<soapenv:Body>
						<zenprise:getRegistryValue>
							<zenprise:osFamily xsi:nil="false">#ARGUMENTS["osFamily"]#</zenprise:osFamily>
							<zenprise:config xsi:nil="false">#ARGUMENTS["config"]#</zenprise:config>
							<zenprise:fullPath xsi:nil="false">#ARGUMENTS["fullPath"]#</zenprise:fullPath>
							<zenprise:valName xsi:nil="false">#ARGUMENTS["valName"]#</zenprise:valName>
						</zenprise:getRegistryValue>
					</soapenv:Body>
				</soapenv:Envelope>
			</cfoutput>
		</cfxml>
		<cfset var soapResponse=zdmRepository.sendRegistryRequest(soapRequest)>
		<cfif NOT yesNoFormat(arrayLen(xmlSearch(soapResponse,"//soapenv:Fault")))>
			<cfset var soapResultNode=soapResponse.xmlRoot.Body.getRegistryValueResponse.getRegistryValueReturn>
			<cfreturn toString(soapResultNode["xmlText"])>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Le service Registry mentionne un message d'erreur SOAP (Zenprise)" detail="#toString(soapResponse)#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="removeRegistryValue" returntype="Void" hint="Supprime des propriétés d'une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="Array" required="true" hint="Liste des propriétés à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfset registryService.removeRegistryValue(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"],ARGUMENTS["valName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="createTypedConfiguration" returntype="Void" hint="Crée une configuration registre typé">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration registre typé">
		<cfargument name="typeName" type="String" required="true" hint="Type de la configuration. Valeurs possibles : Chaine vide, ZDM, SCHEDULING, UNINSTALL">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfset registryService.createTypedConfiguration(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["typeName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeConfiguration" returntype="Void" hint="Supprime une configuration de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration registre typé">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfset registryService.removeConfiguration(ARGUMENTS["osFamily"],ARGUMENTS["config"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getConfigurationList" returntype="Array" hint="Retourne un tableau contenant la liste de toutes les configurations">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfreturn registryService.getConfigurationList()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getOsRegistryKeysByFullPath" returntype="Array" hint="Retourne un tableau contenant la liste des chemins des clés de registre pour un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre ou une chaine vide pour la racine">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfreturn registryService.getRegistryKeysByFullPath(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getRegistryKeysByFullPath" returntype="Array" hint="Retourne un tableau contenant la liste des chemins des clés de registre">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre ou une chaine vide pour la racine">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfreturn registryService.getRegistryKeysByFullPath(ARGUMENTS["config"],ARGUMENTS["fullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getRegistryValueNamesByFullPath" returntype="Array" hint="Retourne un tableau contenant la liste des noms de valeurs incluant par chemins">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var registryService=zdmRepository.getRegistryService()>
		<cftry>
			<cfreturn registryService.getRegistryValueNamesByFullPath(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>