package composants.tb.graph
{
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import composants.tb.tableaux.TableauChangeEvent;
	
   /**
	* A pour vocation de creer des objets de type 'Graphique'
	* @creator Samuel DIVIOKA pour la scociété CONSOTEL 
	**/
	public class GraphFactory
	{
		/**
		 * Constructeur
		 **/
		public function GraphFactory(){
			//init
		}
				
	   /**
		* Retourne un Objet Graphique suivant le type passé en paramètre
		* @param t Le type de page à créer		  
		* @throw GarphError le Graphique n'existe pas ou n'a pu êtres créé.
		**/
		public function createGraph(parameters : GraphParameters):IGraphModel{
			try{
				 var type : String = parameters.TYPE_GRAPH;
				 var segment : String = parameters.SEGMENT;
				 var surtheme : String = parameters.SUR_THEME;
				 var idtheme : String = parameters.ID_THEME;
			 	 var idproduit : String = parameters.ID_PRODUIT;				
				
				 
				switch (type.toUpperCase()){
					case "EVOLUTION" : return new GraphEvolution(segment);break;
					case "COUTPAROPERATEUR" : return new GraphCoutOperateur(segment);break;
					case "DUREEPAROPERATEUR" : return new GraphDureeConso(segment);break;
					case "COUTCONSOPAROPERATEUR" : return new GarphCoutConso(segment);break;					
					case "COUTPARTHEME" : return new GraphCoutTheme(surtheme);break;
					case "COUTPARPRODUIT" : return new GraphCoutProduit(idtheme);break;
					default : throw new GraphError();break; 
				}
			}catch(e:Error){				
				throw new GraphError();
			}
			return null;				
		}		
	}
}