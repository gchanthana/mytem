<cfcomponent output="false">

	<!--- CREATION UUID (ID UNIQUE SUR 35 CAR) --->
	
	<cffunction name="initUUID" access="remote" output="false" returntype="String" hint="CREATION UUID (ID UNIQUE SUR 35 CAR)">
		
			<cfset uuid = createUUID()>
					
		<cfreturn uuid> 
	</cffunction>

	<!--- RECUPERE LA LISTE DES PIECES JOINTES --->

	<cffunction name="fournirAttachements" access="remote" output="false" returntype="any" hint="RECUPERE LA LISTE DES PIECES JOINTES">
		<cfargument name="idcommande" required="true" type="Numeric"/>
				
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.commandeListAttachments">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande"  value="#idcommande#"/>
				<cfprocresult name="p_retour">
			</cfstoredproc>	
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- METS A JOUR LA PIECE L'ETAT DE LA PEICE JOINTE (JOINDRE OU PAS) --->

	<cffunction name="majAttachementJoin" access="remote" output="false" returntype="Numeric" hint="METS A JOUR LA PIECE L'ETAT DE LA PEICE JOINTE (JOINDRE OU PAS)">
		<cfargument name="idcommande" 	required="true" type="Numeric"/>
		<cfargument name="idfile" 		required="true" type="Numeric"/>
		<cfargument name="boolfile" 	required="true" type="Numeric"/>
				
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.majjoinmail">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#idcommande#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idfile"  	value="#idfile#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_bool"  		value="#boolfile#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>	
						
		<cfreturn p_retour>
	</cffunction>

	<!--- DEPOT DE PIECES JOINTES DANS REPERTOIRE UUID (ID UNIQUE SUR 35 CAR) + ENVOIE DES FICHIERS EN BDD --->
	
	<cffunction name="queryAppendAttachements" access="remote" output="false" returntype="any" hint="DEPOT DE PIECES JOINTES DANS REPERTOIRE UUID (ID UNIQUE SUR 35 CAR) + ENVOIE DES FICHIERS EN BDD">
		<cfargument name="idcommande" 		required="true"  type="Numeric"/>
		<cfargument name="files" 			required="true"  type="Array"/>
		<cfargument name="newuuid" 			required="false" type="String" default=""/>

			<cfset resltProc = 1>
			<cfset resltAppend = 0>
			<cfset rsltReturn = 0>
	
			<cfloop index="idx" from="1" to="#ArrayLen(files)#">
				<cfif resltProc GTE 0>
					
					<cfset rsltCompare = Compare(newuuid,"")>
					
					<cfif rsltCompare EQ 0>
						<cfset newuuid = #files[idx].UUID#>
					</cfif>
				
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.enregistrementListAttachments">
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_fileName"  	value="#files[idx].LABEL_PIECE#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_path"	  	value="#newuuid#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_userid" 	value="#SESSION.USER.CLIENTACCESSID#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_size" 		value="#files[idx].SIZE#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_joinMail" 	value="#files[idx].JOINDRE_CFVALUE#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_format" 	value="#files[idx].FORMAT#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" value="#idcommande#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>
					
					<cfset resltProc = p_retour>
					
					<cfif resltProc GTE 0>
						<cfset resltAppend = resltAppend+1>
					</cfif>
				</cfif>
			</cfloop>
							
			<cfif #ArrayLen(files)# EQ resltAppend>
				<cfset rsltReturn = 1>
			<cfelse>
				<cfset rsltReturn = -1>
			</cfif>
			
		<cfreturn rsltReturn> 
	</cffunction>

	<!--- RENOMMAGE DE PIECES JOINTES --->

	<cffunction name="editAttachement" access="remote" output="false" returntype="any" hint="RENOMMAGE DE PIECES JOINTES">
		<cfargument name="dir" 		required="true" type="String"/>
		<cfargument name="lastfile" required="true" type="String"/>
		<cfargument name="newfile" 	required="true" type="String"/>
		<cfargument name="idfile" 	required="true" type="Numeric"/>
		
			<cfset ImportTmp="/container/temporaire/File01/"> <!--- A SUPPRIMER --->
			
			<cftry>
				<cffile action="rename" source="#ImportTmp##dir#/#lastfile#" destination="#ImportTmp##dir#/#newfile#" mode="777">
				<cfset rsltedit = 1>
			<cfcatch>
				<cfset rsltedit = -1>
			</cfcatch>
			</cftry>
			
			<cfif rsltedit GT 0>
				<cfset rsltedit = queryEditAttachement(idfile, newfile)>
			</cfif>

		<cfreturn rsltedit> 
	</cffunction>

	<!--- RENOMMAGE DE PIECES JOINTES COTE BDD --->
	
	<cffunction name="queryEditAttachement" access="remote" output="false" returntype="any" hint="RENOMMAGE DE PIECES JOINTES COTE BDD">
		<cfargument name="idfile" 	required="true" type="Numeric"/>
		<cfargument name="newfile" 	required="true" type="String"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.renameAttachment">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_fileid"   value="#idfile#"/>
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" 	variable="p_new_name" value="#newfile#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>	
				
		<cfreturn p_retour> 
	</cffunction>

	<!--- EFFACEMENT DE PIECES JOINTES --->
	
	<cffunction name="eraseAttachement" access="remote" output="false" returntype="any" hint=" EFFACEMENT DE PIECES JOINTES">
		<cfargument name="dir" 		required="true" type="String"/>
		<cfargument name="file" 	required="true" type="String"/>
		<cfargument name="idfile" 	required="true" type="Numeric"/>

			<cfset ImportTmp="/container/temporaire/File01/">	<!--- A SUPPRIMER --->

			<cftry>
				<cffile action="delete" file="#ImportTmp##dir#/#file#">
				<cfset rslterase = 1>
			<cfcatch>
				<cfset rslterase = -1>
			</cfcatch>
			</cftry>

			<cfif rslterase GTE 1>
				<cfset rslterase = queryEraseAttachement(idfile)>
			</cfif>
		
		<cfreturn rslterase> 
	</cffunction>
	
	<!---  EFFACEMENT DE PIECES JOINTES --->
	
	<cffunction name="queryEraseAttachement" access="remote" output="false" returntype="any" hint=" EFFACEMENT DE PIECES JOINTES COTE BDD">
		<cfargument name="idfile" required="true" type="Numeric"/>
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.eraseAttachment">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_fileid" value="#idfile#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>	
				
		<cfreturn p_retour> 
	</cffunction>
	
	<!--- COPIE DE PIECES JOINTES --->

	<cffunction name="copyAttachement" access="remote" output="false" returntype="any" hint="COPIE DE PIECES JOINTES">
		<cfargument name="dirSource" 	required="true" type="String"/>
		<cfargument name="dirCible" 	required="true" type="String"/>

			<cfset ImportTmp="/container/temporaire/File01/">	<!--- A SUPPRIMER --->
			<cfset rsltcopy = 1>
			
			<cfif NOT DirectoryExists('#ImportTmp##dirCible#')>
				<cfdirectory action="Create" directory="#ImportTmp##dirCible#" type="dir" mode="777">
			<cfelse>
				<cfset rsltcopy = -1>
			</cfif>
			
			<cfif DirectoryExists('#ImportTmp##dirSource#')>
				<cfdirectory action="list" directory="#ImportTmp##dirSource#" name="dirList">
				<cfloop query="dirList">
					<cffile action="copy" source="#ImportTmp##dirSource#/#dirList.NAME#" destination="#ImportTmp##dirCible#/#dirList.NAME#" mode="777">
				</cfloop>
			<cfelse>
				<cfset rsltcopy = -1>
			</cfif>

		<cfreturn rsltcopy> 
	</cffunction>

</cfcomponent>