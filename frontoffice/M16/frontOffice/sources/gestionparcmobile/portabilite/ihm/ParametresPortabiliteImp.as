package gestionparcmobile.portabilite.ihm
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	import flash.ui.Mouse;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import commandemobile.popup.sitemodification.GestionSiteEvent;
	import commandemobile.popup.sitemodification.ParametreSiteIHM;
	import commandemobile.popup.sitemodification.Site;
	
	import composants.controls.TextInputLabeled;
	import composants.util.CommandeFormatter;
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.CompteEvent;
	import gestioncommande.popup.FicheSiteIHM;
	import gestioncommande.popup.PopUpChargerCommandeIHM;
	import gestioncommande.popup.PopUpChargerCommandeImpl;
	import gestioncommande.popup.PopUpChooseCompteIHM;
	import gestioncommande.popup.PopUpSearchSiteIHM;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.CompteService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.PoolService;
	import gestioncommande.services.RevendeurAutoriseService;
	import gestioncommande.services.RevendeurService;
	import gestioncommande.services.SiteService;
	
	import session.SessionUserObject;

	[Bindable]
	public class ParametresPortabiliteImp extends Box	
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cbxSites						:ComboBox;
		public var cbxSitesFacturation			:ComboBox;
		public var cbxCmdType					:ComboBox;
		public var cbxRevendeur					:ComboBox;
		public var cbxAgence					:ComboBox;
		public var cbxActeFor					:ComboBox;
		public var cbxOperateur					:ComboBox;
		public var cbxTitulaire					:ComboBox;
		public var cbxPointFacturation			:ComboBox;
		
		public var txtbxRIO						:TextInput;		
		public var txtLibelle					:TextInput;
		public var txtERPPrefixe				:TextInput;
		public var txtERPSuffixe				:TextInput;
		public var txtNumeroMarche				:TextInput;
		public var txtRefOp						:TextInput;
		
		public var rdbtnNewTerminal				:RadioButton;
		public var rdbtnNoTerminal				:RadioButton;
		public var rdbtnterminalOnly			:RadioButton;
		public var rdbtnChargerConfiguration	:RadioButton;
		
		public var btnNewSite					:Button;
		public var btnModSite					:Button;
		public var btnParcourir					:Button;
		
		public var txtNameConfig				:Label;
		public var txtDateCommande				:Label;	
		public var lblNumCmd					:Label;	
		
		public var dcLivraisonPrevue			:CvDateChooser;
		
		public var txtAreaCommentaires			:TextArea;
		
		public var txtbxTemp					:TextInput;
		
		public var txtbxTo						:TextInputLabeled;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUp						:PopUpChargerCommandeIHM;
		private var _popUpFicheSites			:FicheSiteIHM;
		private var _popupParamSite 			:ParametreSiteIHM;
		private var _popUpSearchSite			:PopUpSearchSiteIHM;
		private var _popUpChooseCompte			:PopUpChooseCompteIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var _cmd							:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _revAutorisesServ			:RevendeurAutoriseService;
		private var _cmdeServices				:CommandeService = new CommandeService();
		private var _cpteSrv					:CompteService = new CompteService();
		private var _poolService				:PoolService = new PoolService();
		private var _siteService				:SiteService = new SiteService();
		private var _revService					:RevendeurService = new RevendeurService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var dateDuJourPlus2JoursOuvrees	:Date = DateFunction.addXJoursOuvreesToDate(new Date(),2);
		
		public var periodeObject				:Object = {rangeStart:new Date(new Date().getTime() - (60 * DateFunction.millisecondsPerDay) )};
		
		public var dateOfToday					:String	= "";
		public var compteName					:String = ResourceManager.getInstance().getString('M16','Compte');
		public var souscompteName				:String = ResourceManager.getInstance().getString('M16','Sous_compte');
		
		public var isOrange						:Boolean = false;
		public var createLine					:Boolean = true;
		public var acte							:Boolean = false;
		
		private var _date						:Date = new Date();
		
		private var _libelleCompteOp			:ArrayCollection = new ArrayCollection();
		
		private var _boolRevAutorises			:Boolean = false;
		
		private var _compteSelected			:Boolean = false;
		
		private var _listeComptes			:ArrayCollection = new ArrayCollection();
		
		//--------------------------------------------------------------------------------------------//
		//					CHAMPS DYNAMIQUES
		//--------------------------------------------------------------------------------------------//
		
		public var hbxChamps1					:HBox;
		public var hbxChamps2					:HBox;
		
		public var LIBELLE_CHAMPS1				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_1');
		public var EXEMPLE_CHAMPS1				:String = "";
		public var LIBELLE_CHAMPS2				:String = ResourceManager.getInstance().getString('M16','R_f_rence_client_2');
		public var EXEMPLE_CHAMPS2				:String = "";
		
		public var CHAMPS1_ACTIF				:Boolean = false;
		public var CHAMPS2_ACTIF				:Boolean = false;
		
		public var CHAMPS1_OBLIGATOIRE			:Boolean = false;
		public var CHAMPS2_OBLIGATOIRE			:Boolean = false;
		public var CHAMPS1_EXEMPLE_VISIBLE		:Boolean = false;
		public var CHAMPS2_EXEMPLE_VISIBLE		:Boolean = false;
		
		private var champs1						:ArrayCollection;
		private var champs2						:ArrayCollection;
		
		//--------------------------------------------------------------------------------------------//
		//					CONCERNANT LA PAGE
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 1;
		public var ACCESS						:Boolean = true;
		private var _razl						:Boolean = false;
		
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//	
		public function ParametresPortabiliteImp()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC
		//--------------------------------------------------------------------------------------------//
		
		public function setCommande():void
		{
			_cmd = SessionUserObject.singletonSession.COMMANDE;
			
			autoCompleteRefClient();
		}
		
		protected function lblNumCmdDataChangeHandler(e:Event):void
		{
			autoCompleteRefClient();
		}
		
		public function validateCurrentPage():Boolean
		{
			return attributToCommande();
		}
		
		//reinitialise l'onglet
		public function reset():void
		{
		}
		
		public function setReferencesClientChild():void
		{
			var nbrChildren:int = hbxChamps1.numChildren;
			
			if(nbrChildren == 0)
			{
				champsPersoHandler();
			}
			
			var bool:Boolean = false;
		}
		
		public function setConfiguration():void
		{
			_razl = false;
		}
		
		//---> 'ENABLE' TOUS LES COMPOSANTS QUI NE DOIVENT PAS ETRE CHANGES
		public function enabledDisableComponents(isEnabled:Boolean):void
		{
			cbxSites.enabled 				= isEnabled;
			cbxSitesFacturation.enabled		= isEnabled;
			cbxCmdType.enabled 				= isEnabled;
			cbxRevendeur.enabled 			= isEnabled;
			cbxAgence.enabled 				= isEnabled;
			cbxOperateur.enabled 			= isEnabled;
			if (cbxTitulaire.selectedItem!=null)
				cbxTitulaire.enabled 			= isEnabled;
			if (cbxPointFacturation.selectedItem!=null)
				cbxPointFacturation.enabled 	= isEnabled;
			txtLibelle.enabled 				= isEnabled;
			txtRefOp.enabled 				= isEnabled;
			txtDateCommande.enabled 		= isEnabled;
			dcLivraisonPrevue.enabled 		= isEnabled;
			btnNewSite.enabled				= isEnabled;
			btnModSite.enabled				= isEnabled;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		//---> AFFECTE LE 'LIBELLE_COMMANDE' À 'RÉFÉRENCE CLIENT' OU 'LIBELLÉ' LORS DU CLIC SUR LE LABEL 'RECOPIER N° DE COMMANDE'
		protected function lblclickHendler(idTextInput:int):void
		{
			if(_cmd == null) return;
			
			switch(idTextInput)
			{
				case 0: txtLibelle.text 			= _cmd.NUMERO_COMMANDE;
					txtLibelle.errorString 		= "";
					break;
				case 1: insertNumeroCommandeInTextbox();
					break;
				case 2: txtRefOp.text 				= _cmd.NUMERO_COMMANDE;
					txtRefOp.errorString 		= "";
					break;
			}
		}
		
		protected function textInputHandler(txtbxNumber:int):void
		{
			switch(txtbxNumber)
			{
				case 0: txtLibelle.errorString 		= "";break;
				case 2: txtRefOp.errorString 		= "";break;
				case 3: lblNumCmd.errorString 		= "";break;
				case 4: txtERPPrefixe.errorString 	= "";break;
				case 5: txtERPSuffixe.errorString 	= "";break;
				case 6: txtNumeroMarche.errorString = "";break;
			}
		}
		
		protected function cbxActeForChangeHandler():void
		{
			if(cbxActeFor.selectedItem != null)
				cbxActeFor.errorString = "";
		}
		
		protected function btnNewSiteClickHandler():void
		{
			if(SessionUserObject.singletonSession.POOL != null)
			{
				_popUpFicheSites = new FicheSiteIHM();
				_popUpFicheSites.idPoolGestionnaire = SessionUserObject.singletonSession.POOL.IDPOOL;
				_popUpFicheSites.addEventListener("refreshSite", ficheSiteHandler);
				
				PopUpManager.addPopUp(_popUpFicheSites, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(_popUpFicheSites);
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16','Veuillez_s_lectionner_un_pool_'), 'Consoview', null);
		}
		
		protected function btnModSiteClickHandler(me:MouseEvent):void
		{
			if(cbxSites.selectedItem != null)
			{
				_popupParamSite = new ParametreSiteIHM();
				_popupParamSite.site = mappingSiteSelectedToSite();
				addPop();
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16','S_lectionnez_un_site_'), 'Consoview', null);
		}
		
		protected function imgSearchClickHandler(me:MouseEvent):void
		{
			var composant		:Image = me.currentTarget as Image;
			var currentData		:ArrayCollection = new ArrayCollection();
			
			_popUpSearchSite = new PopUpSearchSiteIHM();
			
			if(composant != null && composant.id == 'imgSearchL')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSites.dataProvider as ArrayCollection) as ArrayCollection, cbxSites.selectedItem, true);
			}
			else if(composant != null && composant.id == 'imgSearchF')
			{
				_popUpSearchSite.setInfos(ObjectUtil.copy(cbxSitesFacturation.dataProvider as ArrayCollection) as ArrayCollection, cbxSitesFacturation.selectedItem, false);
			}
			
			if(composant != null)
			{
				_popUpSearchSite.addEventListener('SEARCH_SITE_SELECTED', searchSiteHandler);
				
				PopUpManager.addPopUp(_popUpSearchSite, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(_popUpSearchSite);
			}
		}
		
		protected function cbxCmdTypeChangeHandler():void
		{
			if(cbxCmdType.selectedItem != null)
			{
				cbxCmdType.errorString = "";
				
				cbxRevendeur.dataProvider = null;
				cbxActeFor.dataProvider = null;
				cbxAgence.dataProvider = null;
				
				cbxRevendeur.prompt = "";
				cbxActeFor.prompt = "";
				cbxAgence.prompt = "";
				
				if(_boolRevAutorises)
				{
					cbxOperateur.dataProvider = null;					
					cbxPointFacturation.dataProvider = null;
					cbxTitulaire.dataProvider = null;
					
					cbxOperateur.prompt = "";
					cbxPointFacturation.prompt = "";
					cbxTitulaire.prompt = "";
					
					getOperateursAutorises();
				}
				else
					getRevendeurs();
				
				getActePour();
				
				dispatchEvent(new Event('PROFILE_CHANGED', true));
			}
		}
		
		// CHOIX DE LOPERATEUR DANS LE COMBO BOX
		protected function cbxOperateurChangeHandler():void
		{
			
			cbxTitulaire.dataProvider 			= null;//---> COMPTE
			cbxPointFacturation.dataProvider 	= null;//---> SOUS COMPTE
			
			cbxTitulaire.prompt			 		= "";//---> COMPTE
			cbxPointFacturation.prompt 			= "";//---> SOUS COMPTE
			
			if(cbxOperateur.selectedItem != null)
			{
				_cmd.PAYS_CONSOTELID = Number(cbxOperateur.selectedItem.PAYSCONSOTELID);
				_cmd.PAYS_OPERATEUR = cbxOperateur.selectedItem.PAYS;
				
				dynamicChamps(cbxOperateur.selectedItem.OPERATEURID);
				cbxOperateur.errorString = "";
				
				if(_boolRevAutorises)
				{
					cbxRevendeur.dataProvider 	= null;
					cbxAgence.dataProvider 		= null;
					
					cbxRevendeur.prompt			= "";
					cbxAgence.prompt			= "";
					
					getRevendeursAutorises();
				}
				
				getComptesSousComptes();
				
				getSLA();
				
				dispatchEvent(new Event('OPERATEUR_CHANGED', true));
			}
			else
				dynamicChamps(0);
		}
		
		//---> DE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un sous compte'
		protected function cbxTitulaireChangeHandler():void
		{
			cbxTitulaire.errorString = "";
			
			if(cbxTitulaire.selectedItem != null)
				getSousComptes();
		}
		
		//---> DE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un compte' (liste les sous comptes)
		protected function cbxPointFacturationChangeHandler():void
		{
		}
		
		//---> SE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un revendeur' (liste les agences)
		protected function cbxRevendeurChangeHandler():void
		{
			
			cbxAgence.dataProvider 	= null;
			cbxAgence.prompt		= "";
			
			if(cbxRevendeur.selectedItem != null)
			{
				CommandeFormatter.getInstance().setCommandeCurrencyEvent(cbxRevendeur.selectedItem.DEVISE);//A chaque clic sur un revendeur, sa devise est mémorisée et utilisée pour la suite de la commande.
				cbxRevendeur.errorString = '';
				getAgences();
				getSLA();
				getActePour();
			}
		}
		
		//---> SE PRODUIT LORS DE LA MODIFICATION DU DATAPROVIDER DU COMBOBOX 'Sélectionnez un revendeur'
		protected function cbxRevendeurDataChangeHandler():void
		{
			if (cbxRevendeur.selectedItem != null)
			{
				CommandeFormatter.getInstance().setCommandeCurrencyEvent(cbxRevendeur.selectedItem.DEVISE);//A chaque clic sur un revendeur, sa devise est mémorisée et utilisée pour la suite de la commande.
			}
			
		}
		
		//---> SE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez une agence' (liste les contacts)
		protected function cbxAgenceChangeHandler():void
		{
			cbxAgence.errorString = "";
		}
		
		//---> SE PRODUIT LORS DE LA FERMETURE DU COMBOBOX 'Sélectionnez un site'
		protected function cbxSitesCloseHandler(e:Event):void
		{
			cbxSites.errorString = "";
			
			if(cbxSites.selectedItem != null)
				searchInSiteFacturation(cbxSites.selectedItem.IDSITE_PHYSIQUE);
		}
		
		protected function cbxSitesFacturationCloseHandler():void
		{
			cbxSitesFacturation.errorString = "";
		}
		
		//---> ENVOIE A 'MainNewCommande' LE PASSAGE D'UNE COMMANDE NORMALE
		protected function rdbtnNewTerminalChangeHandler():void
		{
			setElementsRadioBtn(rdbtnNewTerminal, true, false, 1);
		}
		
		//---> ENVOIE A 'MainNewCommande' LE PASSAGE DIRECTE A L'ETAPE 'Choix des abonnements'
		protected function rdbtnNoTerminalChangeHandler():void
		{
			setElementsRadioBtn(rdbtnNoTerminal, true, false, 2);
		}
		
		//---> ENVOIE A 'MainNewCommande' LE PASSAGE DIRECTE A L'ETAPE 'Choix des équipements - Terminal (1/2)'
		/*protected function rdbtnterminalOnlyChangeHandler():void
		{
			setElementsRadioBtn(rdbtnterminalOnly, false, false, 3);
		}*/
		
		//---> ENVOIE A 'MainNewCommande' LE PASSAGE DIRECTE A L'ETAPE 'Choix des quantités'
		/*protected function rdbtnChargerConfigurationChangeHandler():void
		{			
			if(SessionUserObject.singletonSession.POOL != null)
			{
				if(cbxCmdType.selectedItem != null && cbxOperateur.selectedItem != null && cbxRevendeur.selectedItem != null)
					setElementsRadioBtn(rdbtnChargerConfiguration, true, true, 4);
				else
				{
					setElementsRadioBtn(rdbtnNewTerminal, true, false, 1);
					
					if(cbxCmdType.selectedItem == null)
						ConsoviewAlert.afficherAlertInfo(resourceManager.getString('M16','S_lectionnez_un_type_de_commande__'), 'Consoview', null);
					else if(cbxOperateur.selectedItem == null)
					{
						ConsoviewAlert.afficherAlertInfo(resourceManager.getString('M16','S_lectionnez_un_op_rateur'), 'Consoview', null);
					}
					else if(cbxRevendeur.selectedItem == null)
					{
						ConsoviewAlert.afficherAlertInfo(resourceManager.getString('M16','S_lectionnez_un_revendeur'), 'Consoview', null);
					}
				}
			}
		}*/
		
		/*protected function btnParcourirClickHandler():void
		{
			if(SessionUserObject.singletonSession.POOL != null && SessionUserObject.singletonSession.POOL.IDPOOL != 0)
			{
				_popUp = new PopUpChargerCommandeIHM();
				_popUp.idPool 	= SessionUserObject.singletonSession.POOL.IDPOOL;
				
				if(cbxCmdType.selectedItem != null)
					_popUp.idProfil = cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
				
				if(cbxOperateur.selectedItem != null)
					_popUp.idOperateur = cbxOperateur.selectedItem.OPERATEURID;
				
				if(cbxRevendeur.selectedItem != null)
					_popUp.idRevendeur = cbxRevendeur.selectedItem.IDREVENDEUR;
				
				_popUp.addEventListener(PopUpChargerCommandeImpl.SELECT_MODELE, modeleSelectedHandler);
				
				PopUpManager.addPopUp(_popUp, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(_popUp);
			}
			else
				ConsoviewAlert.afficherAlertInfo(resourceManager.getString('M16','S_lectionnez_un_pool__'), 'Consoview', null)
		}*/
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function showHandler(fe:FlexEvent):void
		{
			SessionUserObject.singletonSession.LASTVSINDEX = IDPAGE;
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			//			listenersComboBox();
			
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = 1;
			
			_boolRevAutorises = SessionUserObject.singletonSession.BOOLDISTRIBAUTORISES;
			
			dateOfToday = DateFunction.formatDateAsString(_date);
			
			txtbxTo.text = CvAccessManager.getUserObject().PRENOM + ' ' + CvAccessManager.getUserObject().NOM;
			
			champsPersoHandler();
			getLibelleProcedure();
			getProfilsEquipements();
			getSiteLivraisonFacturation();
			
			if(SessionUserObject.singletonSession.POOL != null && SessionUserObject.singletonSession.POOL.IDREVENDEUR > 0)
				acte = true;
			
			if(!_boolRevAutorises)
				getOperateurs();
		}
		
		private function listenersComboBox():void
		{
			cbxCmdType.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxCmdType.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
			cbxSites.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxSites.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
			cbxSitesFacturation.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxSitesFacturation.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
			cbxOperateur.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxOperateur.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
			cbxTitulaire.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxTitulaire.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
			cbxPointFacturation.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxPointFacturation.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
			cbxRevendeur.addEventListener(FlexEvent.DATA_CHANGE, cbxRevendeurDataHandler);
			cbxRevendeur.addEventListener(FlexEvent.UPDATE_COMPLETE, cbxRevendeurDataHandler);
			cbxAgence.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxAgence.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
			cbxActeFor.addEventListener(FlexEvent.DATA_CHANGE, comboboxDataHandler);
			cbxActeFor.addEventListener(FlexEvent.UPDATE_COMPLETE, comboboxDataHandler);
		}
		
		private function comboboxDataHandler(e:Event):void
		{
			var currentCombobox	:ComboBox = e.currentTarget as ComboBox;
			var currentItems	:ArrayCollection;
			
			if(currentCombobox != null)
			{
				currentItems = currentCombobox.dataProvider as ArrayCollection;
				
				if(currentCombobox.dataProvider as ArrayCollection && currentCombobox.dataProvider != null)
				{
					(currentCombobox.dataProvider as ArrayCollection).refresh();
					(currentCombobox.dropdown.dataProvider as ArrayCollection).refresh();
				}
			}
		}
		
		private function cbxRevendeurDataHandler(e:Event):void
		{
			var currentCombobox	:ComboBox = e.currentTarget as ComboBox;
			var currentItems	:ArrayCollection;
			
			if(currentCombobox != null)
			{
				currentItems = currentCombobox.dataProvider as ArrayCollection;
				
				if(currentCombobox.dataProvider as ArrayCollection && currentCombobox.dataProvider != null)
				{
					(currentCombobox.dataProvider as ArrayCollection).refresh();
					(currentCombobox.dropdown.dataProvider as ArrayCollection).refresh();
					
					if (cbxRevendeur.selectedItem != null)
					{
						CommandeFormatter.getInstance().setCommandeCurrencyEvent(cbxRevendeur.selectedItem.DEVISE);//A chaque clic sur un revendeur, sa devise est mémorisée et utilisée pour la suite de la commande.
					}
				}
			}
		}
		
		private function modeleSelectedHandler(e:Event):void
		{
			txtNameConfig.text = _popUp.libelle;
			SessionUserObject.singletonSession.CURRENTCONFIGURATION = _popUp.myElements;
			
			if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.ABONNEMENTS.length == 0 && SessionUserObject.singletonSession.CURRENTCONFIGURATION.OPTIONS.length == 0)
				createLine = false;
			else
				createLine = true;
		}
		
		public function btImgChooseCompteClickHandler(e:Event):void
		{
			if(cbxOperateur.selectedIndex >-1)
			{
				if(listeComptes.length > 0)
				{
					this.majSelectCompte(listeComptes);
					_popUpChooseCompte = new PopUpChooseCompteIHM();
					_popUpChooseCompte.listeComptes = listeComptes;
					
					_popUpChooseCompte.addEventListener(CompteEvent.VALID_CHOIX_COMPTE, getItemCompteHandler);
					PopUpManager.addPopUp(_popUpChooseCompte, this, true);
				}
				else
				{
					cbxTitulaire.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_titulaire__');
				}
			}
			else
			{
				cbxOperateur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_op_rateur__');
			}
		}
		
		private function majSelectCompte(_comptes:ArrayCollection):void
		{
			var longPool:int = _comptes.length;
			var i:int = 0;
			while(i < longPool)
			{
				_comptes[i].SELECTED = false;
				if((cbxTitulaire.selectedItem != null) && _comptes[i].IDCOMPTE_FACTURATION == cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION)
				{
					_comptes[i].SELECTED = true;
					_compteSelected = true;
				}
				
				i++;
			}
			
		}
		
		public function getItemCompteHandler(ce:CompteEvent):void
		{
			_popUpChooseCompte.removeEventListener(CompteEvent.VALID_CHOIX_COMPTE, getItemCompteHandler);
			
			for (var j:int = 0; j < listeComptes.length; j++) 
			{
				if((listeComptes[j].IDCOMPTE_FACTURATION) ==(ce.idCompte))
				{
					cbxTitulaire.selectedIndex = j;
					break;
				}
			}
			
			cbxTitulaireChangeHandler();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getLibelleProcedure():void
		{
			_cpteSrv.fournirLibelleCompteOperateur();
			_cpteSrv.addEventListener(CommandeEvent.LISTED_LIBELLE_OPE, getLibelleProcedureHandler);
		}
		
		private function getProfilsEquipements():void
		{
			_poolService.fournirProfilEquipements();
			_poolService.addEventListener(CommandeEvent.LISTED_PROFILES, profilsEquipementsHandler);
		}
		
		private function getSiteLivraisonFacturation():void
		{
			var object:Object = SessionUserObject.singletonSession;
			
			_siteService.fournirListeSiteLivraisonsFacturation(SessionUserObject.singletonSession.POOL.IDPOOL);
			_siteService.addEventListener(CommandeEvent.LISTED_SITES, siteLivraisonFacturationHandler);
		}
		
		private function getOperateurs():void
		{
			_revAutorisesServ.fournirListeOperateurs(SessionUserObject.singletonSession.IDSEGMENT);
			_revAutorisesServ.addEventListener(CommandeEvent.LISTED_OPERATEURS, operateursHandler);
		}
		
		private function getRevendeurs():void
		{
			_revService.fournirRevendeurs(SessionUserObject.singletonSession.POOL.IDPOOL,
				cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,
				SessionUserObject.singletonSession.POOL.IDREVENDEUR);
			_revService.addEventListener(CommandeEvent.LISTED_REVENDEURS, revendeursHandler);
		}
		
		private function getOperateursAutorises():void
		{
			_revAutorisesServ.fournirOperateursAutorises(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT);
			_revAutorisesServ.addEventListener(CommandeEvent.LISTED_OPEAUTORISES, operateursAutorisesHandler);
		}
		
		private function getRevendeursAutorises():void
		{
			_revAutorisesServ.fournirRevendeursAutorises(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,
				cbxOperateur.selectedItem.OPERATEURID,
				SessionUserObject.singletonSession.POOL.IDREVENDEUR);
			_revAutorisesServ.addEventListener(CommandeEvent.LISTED_REVAUTORISES, revendeursAutorisesHandler);
		}
		
		private function getComptesSousComptes():void
		{
			_cpteSrv.fournirCompteOperateurByLoginAndPoolV2(cbxOperateur.selectedItem.OPERATEURID,
				SessionUserObject.singletonSession.POOL.IDPOOL);
			_cpteSrv.addEventListener(CommandeEvent.LISTED_COMPTES, comptesSousComptesHandler);
		}		
		
		private function getAgences():void
		{
			_revService.fournirAgencesRevendeur(cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE);
			_revService.addEventListener(CommandeEvent.LISTED_AGENCES, agencesHandler);
		}
		
		private function getActePour():void
		{
			if(cbxCmdType.selectedItem == null || cbxRevendeur.selectedItem == null) return;
			
			_revService.fournirActePour(cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT,
				cbxRevendeur.selectedItem.IDREVENDEUR);
			_revService.addEventListener(CommandeEvent.LISTED_ACTE_POUR, actePourHandler);
		}
		
		private function getSLA():void
		{
			if(cbxRevendeur.selectedItem == null || cbxOperateur.selectedItem == null) return;
			
			_cmdeServices.fournirRevendeurSLA(cbxRevendeur.selectedItem.IDREVENDEUR, cbxOperateur.selectedItem.OPERATEURID);
			_cmdeServices.addEventListener(CommandeEvent.LISTED_REVENDEURSLA, slaHandler);
		}
		
		//---ATTRIBUT LES DONNEES A LA COMBOBOX PASSEE EN PARAMETRE
		private function attributDataToCombobox(cbx:ComboBox, dataValues:ArrayCollection, msgError:String):void
		{
			var lenDataValues:int = dataValues.length;
			
			cbx.errorString = "";
			
			if(lenDataValues != 0)
			{
				cbx.prompt 					= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
				cbx.dataProvider 			= dataValues;
				cbx.dropdown.dataProvider 	= dataValues;
				
				if(lenDataValues > 1)
					cbx.selectedIndex = -1;
				else
					cbx.selectedIndex = 0;
				
				if(lenDataValues > 5)
					cbx.rowCount = 5;
				else
					cbx.rowCount = lenDataValues;
				
				(cbx.dataProvider as ArrayCollection).refresh();
				cbx.dispatchEvent(new FlexEvent(FlexEvent.DATA_CHANGE, true));
				
				
			}
			else
				cbx.prompt = msgError;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getLibelleProcedureHandler(cmde:CommandeEvent):void
		{
			_libelleCompteOp = _cpteSrv.listeLibellesOperateur;
		}
		
		private function profilsEquipementsHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxCmdType, _poolService.listeProfiles, ResourceManager.getInstance().getString('M16', 'Aucun_profile'));
			
			if(_boolRevAutorises && _poolService.listeProfiles.length == 1)
			{
				getOperateursAutorises();
			}
			else if(!_boolRevAutorises && _poolService.listeProfiles.length == 1)
			{
				getRevendeurs();
			}
			
			getActePour();
		}
		
		private function siteLivraisonFacturationHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxSites, _siteService.listeSitesLivraison, ResourceManager.getInstance().getString('M16', 'Aucun_site'));
			attributDataToCombobox(cbxSitesFacturation, _siteService.listeSitesFacturation, ResourceManager.getInstance().getString('M16', 'Aucun_site'));
		}
		
		private function operateursAutorisesHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxOperateur, _revAutorisesServ.listeOperateurs, ResourceManager.getInstance().getString('M16', 'Aucun_op_rateurs_autoris_s'));
			
			if(_revAutorisesServ.listeOperateurs.length == 1)
			{
				_cmd.PAYS_CONSOTELID = Number(_revAutorisesServ.listeOperateurs[0].PAYSCONSOTELID);
				_cmd.PAYS_OPERATEUR = _revAutorisesServ.listeOperateurs[0].PAYS;
				
				dynamicChamps(_revAutorisesServ.listeOperateurs[0].OPERATEURID);
				
				getSLA();
				
				getRevendeursAutorises();
				getComptesSousComptes();
			}
		}
		
		private function revendeursAutorisesHandler(cmde:CommandeEvent):void
		{		
			acteToDo();
			
			attributDataToCombobox(cbxRevendeur, _revAutorisesServ.listeRevendeurs, ResourceManager.getInstance().getString('M16', 'Aucun_revendeurs_autoris_s'));
			
			if(_revAutorisesServ.listeRevendeurs.length == 1)
			{
				getAgences();
				
				getSLA();
				getActePour();
			}
		}
		
		private function operateursHandler(cmde:CommandeEvent):void
		{	
			attributDataToCombobox(cbxOperateur, _revAutorisesServ.listeOperateursNormal, ResourceManager.getInstance().getString('M16', 'Aucun_op_rateurs'));
			
			if(_revAutorisesServ.listeOperateursNormal.length == 1)
			{
				dynamicChamps(_revAutorisesServ.listeOperateursNormal[0].OPERATEURID);
				getSLA();
				getComptesSousComptes();
			}
		}
		
		private function revendeursHandler(e:Event):void
		{
			attributDataToCombobox(cbxRevendeur, _revService.listeRevendeurs, ResourceManager.getInstance().getString('M16', 'Aucun_revendeur'));
			
			acteToDo();
			
			if(_revService.listeRevendeurs.length == 1)
			{
				getAgences();
				getSLA();
				getActePour();
			}	
		}
		
		private function comptesSousComptesHandler(cmde:CommandeEvent):void
		{
			listeComptes.source = [];
			listeComptes = _cpteSrv.listeComptesSousComptes;//Formator.sortFunction(_cpteSrv.listeComptesSousComptes, 'COMPTE_FACTURATION');
			var lenComptes		:int = listeComptes.length;
			attributDataToCombobox(cbxTitulaire, listeComptes, ResourceManager.getInstance().getString('M16', 'Aucun_comptes'));
			
			if(lenComptes == 1)
				getSousComptes();
		}
		
		private function agencesHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxAgence, _revService.listeAgences, ResourceManager.getInstance().getString('M16', 'Aucune_agences'));
		}
		
		private function actePourHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxActeFor, _revService.listeActePour, ResourceManager.getInstance().getString('M16', 'Aucun_gestionnaires'));
		}
		
		private function slaHandler(cmde:CommandeEvent):void
		{
			if(_cmdeServices.revendeurSLA != null)
			{
				var hoursParsing	:Object = Formator.formatHour(_date);
				var delaiTime		:Array 	= (_cmdeServices.revendeurSLA.DELAI_HOUR_LIVRAISON as String).split(".");
				var delai			:int 	= Number(_cmdeServices.revendeurSLA.DELAI_LIVRAISON);
				
				if(Number(hoursParsing.HOURS) > Number(delaiTime[0]))
					delai = delai + 1;
				else
				{
					if(Number(hoursParsing.HOURS) == Number(delaiTime[0]))
					{
						if(Number(hoursParsing.MIN) > Number(delaiTime[1]))
							delai = delai + 1;
					}
				}
				
				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), delai);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (delai * DateFunction.millisecondsPerDay))};
			}
			else
			{
				dcLivraisonPrevue.selectedDate 		= DateFunction.addXJoursOuvreesToDate(new Date(), 2);
				dcLivraisonPrevue.selectableRange 	= {rangeStart:new Date(new Date().getTime() + (2 * DateFunction.millisecondsPerDay))};
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function autoCompleteRefClient():void
		{
			if(SessionUserObject.singletonSession.CHAMPS.AUTO1)
				insertNumeroCommandeInTextbox();
			
			if(SessionUserObject.singletonSession.CHAMPS.AUTO2)
				insertNumeroCommandeInTextbox2();
		}
		
		private function getSousComptes():void
		{
			var listeSSCPTE:ArrayCollection = _cpteSrv.searchSousCompteCorrespondant(cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION);
			
			attributDataToCombobox(cbxPointFacturation, listeSSCPTE, ResourceManager.getInstance().getString('M16', 'Aucun_sous_comptes'));
		}
		
		private function setElementsRadioBtn(rdbtn:RadioButton, isCreate:Boolean, isbtnParcourirEnabled:Boolean, idProfileSelected:int):void
		{
			SessionUserObject.singletonSession.IDTYPEDECOMMANDE = idProfileSelected;
			
			rdbtn.selected = true;
			createLine = isCreate;
			//btnParcourir.enabled = isbtnParcourirEnabled;
			//txtNameConfig.text = "";
			
			dispatchEvent(new Event('TYPE_CMD_CHANGED', true));
		}
		
		// 
		private function findLibelleOperateur():void
		{
			if(cbxOperateur.selectedItem != null)
			{
				var idop	:int = cbxOperateur.selectedItem.OPERATEURID;
				var len 	:int = _libelleCompteOp.length;
				
				for(var i:int = 0; i < len; i++)
				{
					if(_libelleCompteOp[i].OPERATEURID == idop)
					{
						if(_libelleCompteOp[i].COMPTE != null && _libelleCompteOp[i].COMPTE != '')
							compteName 		= _libelleCompteOp[i].COMPTE;
						else
							compteName 		= ResourceManager.getInstance().getString('M16','Compte');;
						
						if(_libelleCompteOp[i].SOUS_COMPTE != null && _libelleCompteOp[i].SOUS_COMPTE != '')
							souscompteName 	= _libelleCompteOp[i].SOUS_COMPTE;
						else
							souscompteName 	= ResourceManager.getInstance().getString('M16','Sous_compte');
						
						if(idop == 938)
							isOrange = true;
						
						return;
					}
				}	
			}
		}
		
		// SELON OPERATEUR AFFICHER COMPTE / SOUS COMPTE
		private function dynamicChamps(idOperateur:int):void
		{
			compteName		= resourceManager.getString('M16','Compte');
			souscompteName 	= resourceManager.getString('M16','Sous_compte');
			isOrange		= false;
			
			findLibelleOperateur();
		}
		
		private function addPop():void
		{
			_popupParamSite.addEventListener(GestionSiteEvent.PARAMETRE_SITE_COMPLETE, parametreSiteCompleteHandler);
			PopUpManager.addPopUp(_popupParamSite, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popupParamSite);
		}
		
		private function parametreSiteCompleteHandler(gse:GestionSiteEvent):void
		{
			ficheSiteHandler(gse);
		}
		
		private function searchSiteHandler(e:Event):void
		{
			var mySiteSelected	:Object = _popUpSearchSite.getInfos();			
			var lenSitesL		:int = (cbxSites.dataProvider as ArrayCollection).length;
			var lenSitesF		:int = (cbxSitesFacturation.dataProvider as ArrayCollection).length;
			var i				:int = 0;
			
			if(_popUpSearchSite.isSiteLivraison)
			{
				for(i = 0;i < lenSitesL;i++)
				{
					if((cbxSites.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSites.selectedIndex = i;
						
						break;
					}
				}
				
				searchInSiteFacturation(cbxSites.selectedItem.IDSITE_PHYSIQUE);
			}
			else
			{
				for(i = 0;i < lenSitesF;i++)
				{
					if((cbxSitesFacturation.dataProvider as ArrayCollection)[i].IDSITE_PHYSIQUE == mySiteSelected.IDSITE_PHYSIQUE)
					{
						cbxSitesFacturation.selectedIndex = i;
						
						break;
					}
				}
			}
			
			PopUpManager.removePopUp(_popUpSearchSite);
		}
		
		private function ficheSiteHandler(e:Event):void
		{
			getSiteLivraisonFacturation();
		}
		
		private function acteToDo():void
		{
			if(SessionUserObject.singletonSession.POOL != null)
			{
				if(SessionUserObject.singletonSession.POOL != null && SessionUserObject.singletonSession.POOL.IDREVENDEUR > 0)
				{
					acte = true;
					
					var revendeurs :ArrayCollection = new ArrayCollection();
					var isoRevend  :ArrayCollection = new ArrayCollection();
					
					if(_boolRevAutorises)
					{
						isoRevend  = new ArrayCollection(_revAutorisesServ.listeRevendeurs.source);
						revendeurs = _revAutorisesServ.listeRevendeurs;
					}
					else
					{
						isoRevend = new ArrayCollection(_revService.listeRevendeurs.source);
						revendeurs = _revService.listeRevendeurs;
					}
					
					var len		:int = revendeurs.length;
					var idRev	:int = SessionUserObject.singletonSession.POOL.IDREVENDEUR;
					
					for(var i:int = 0;i < len;i++)
					{
						if(revendeurs[i].IDREVENDEUR != idRev)
						{
							revendeurs.removeItemAt(i);
							i--;
							len--;
						}
					}
					
					if(len == 0)
					{
						acte = false;
						revendeurs = isoRevend;
					} 
				}
				else
					acte = false;
			}
		}
		
		private function searchInSiteFacturation(idSite:int):void
		{
			var sites	:ArrayCollection = cbxSitesFacturation.dataProvider as ArrayCollection;
			var len		:int 			 = sites.length;
			var isHere	:Boolean		 = false;
			
			for(var i:int = 0;i <len;i++)
			{
				if(sites[i].IDSITE_PHYSIQUE == idSite)
				{
					cbxSitesFacturation.selectedIndex = i;
					isHere = true;
					break;
				}
			}
			
			if(!isHere)
			{
				if(cbxSitesFacturation.selectedItem == null)
					cbxSitesFacturation.selectedIndex = -1;
			}
		}
		
		private function insertNumeroCommandeInTextbox():void
		{
			var childChamps1 	:Array 	 = hbxChamps1.getChildren();
			var lenChild	 	:int 	 = childChamps1.length;
			var lenNumCmd		:int 	 = _cmd.NUMERO_COMMANDE.length;
			var cptr			:int	 = -1
			var lenTxtIpt		:int 	 = -1;
			var i				:int 	 = 0;
			
			for(i = 0; i < lenChild; i++)
			{
				cptr = i*2;
				
				if(cptr < lenChild)
				{
					lenTxtIpt = (childChamps1[cptr] as TextArea).maxChars;
					
					if(lenNumCmd <= lenTxtIpt)
					{
						(childChamps1[cptr] as TextArea).text 		= _cmd.NUMERO_COMMANDE;
						(childChamps1[cptr] as TextArea).errorString = "";
					}
				}
			}
		}
		
		private function insertNumeroCommandeInTextbox2():void
		{
			var childChamps2 	:Array 	 = hbxChamps2.getChildren();
			var lenChild	 	:int 	 = childChamps2.length;
			var lenNumCmd		:int 	 = _cmd.NUMERO_COMMANDE.length;
			var cptr			:int	 = -1
			var lenTxtIpt		:int 	 = -1;
			var i				:int 	 = 0;
			
			for(i = 0; i < lenChild; i++)
			{
				cptr = i*2;
				
				if(cptr < lenChild)
				{
					lenTxtIpt = (childChamps2[cptr] as TextArea).maxChars;
					
					if(lenNumCmd <= lenTxtIpt)
					{
						(childChamps2[cptr] as TextArea).text 		= _cmd.NUMERO_COMMANDE;
						(childChamps2[cptr] as TextArea).errorString = "";
					}
				}
			}
		}
		
		//---> ATTRIBUT LES VALEURS SAISI PAR L'UTILISATEUR A L'OBJET 'Commande'
		private function attributToCommande():Boolean
		{
			var bool:Boolean = false;
			var check:Boolean = true;
			
			try{
				if(!acte)
					_cmd.IDGESTIONNAIRE = CvAccessManager.getSession().USER.CLIENTACCESSID;
				
				if(SessionUserObject.singletonSession.POOL != null)
				{
					if(!acte)
					{
						_cmd.IDPOOL_GESTIONNAIRE 	= SessionUserObject.singletonSession.POOL.IDPOOL;
						_cmd.IDPROFIL 				= SessionUserObject.singletonSession.POOL.IDPROFIL;
					}
					else
					{
						if(cbxActeFor.selectedItem != null)
						{
							_cmd.IDPOOL_GESTIONNAIRE 	= cbxActeFor.selectedItem.IDPOOL;
							_cmd.IDPROFIL 				= cbxActeFor.selectedItem.IDPROFILE;
						}
						else
						{
							cbxActeFor.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_gestionnaire__');
							check =  false;
						}
					}
				}
				
				if(txtLibelle.text != "")
					_cmd.LIBELLE_COMMANDE 		= txtLibelle.text;
				else
				{
					txtLibelle.errorString = resourceManager.getString('M16','Veuillez_saisir_le_libell__de_la_command');
					check =  false;
				}
				
				if(check)
					check = checkChamps();
				else
					checkChamps();
				
				if(txtRefOp.text != null)
					_cmd.REF_OPERATEUR 			= txtRefOp.text;
				else
				{
					txtRefOp.errorString = resourceManager.getString('M16','Veuillez_saisir_la_r_f_rence_op_rateur_');
					check =  false;
				}
				
				if(lblNumCmd.text != "")
					_cmd.NUMERO_COMMANDE 		= lblNumCmd.text;
				else
				{
					lblNumCmd.errorString = resourceManager.getString('M16','Pas_de_num_ro_de_commande_');
					check =  false;
				}
				
				if(dcLivraisonPrevue.selectedDate != null)
					_cmd.LIVRAISON_PREVUE_LE 	= dcLivraisonPrevue.selectedDate;
				else
				{
					dcLivraisonPrevue.errorString = resourceManager.getString('M16','Veuillez_saisir_une_date_de_livraison_');
					check =  false;
				}
				
				if(cbxSites.selectedItem != null)
				{
					_cmd.LIBELLE_SITELIVRAISON 	= cbxSites.selectedItem.NOM_SITE;
					_cmd.IDSITELIVRAISON 		= cbxSites.selectedItem.IDSITE_PHYSIQUE;
				}
				else
				{
					cbxSites.errorString = resourceManager.getString('M16','Veuillez_saisir_un_site_de_livraison_');
					check =  false;
				}
				
				if(cbxSitesFacturation.selectedItem != null)
				{
					_cmd.LIBELLE_SITEFACTURATION = cbxSitesFacturation.selectedItem.NOM_SITE;
					_cmd.IDSITEFACTURATION		 = cbxSitesFacturation.selectedItem.IDSITE_PHYSIQUE;
					_cmd.ADRESSE_FACTURATION	 = cbxSitesFacturation.selectedItem.NOM_SITE
						+ '\n' + cbxSitesFacturation.selectedItem.ADRESSE 
						+ '\n' + cbxSitesFacturation.selectedItem.SP_COMMUNE 
						+ ' ' + cbxSitesFacturation.selectedItem.SP_CODE_POSTAL 
						+ '\n ' + cbxSitesFacturation.selectedItem.PAYS ;
				}
				else
				{
					cbxSitesFacturation.errorString =  resourceManager.getString('M16','Veuillez_saisir_un_site_de_facturation__');
					check =  false;
				}
				
				if(cbxCmdType.selectedItem != null)
					_cmd.IDPROFIL_EQUIPEMENT 	= cbxCmdType.selectedItem.IDPROFIL_EQUIPEMENT;
				else
				{
					cbxCmdType.errorString = resourceManager.getString('M16','Veuillez_saisir_un_type_de_commande_');
					check =  false;
				}
				
				if(cbxOperateur.selectedItem != null)
				{
					_cmd.IDOPERATEUR 			= cbxOperateur.selectedItem.OPERATEURID;
					_cmd.LIBELLE_OPERATEUR 		= cbxOperateur.selectedItem.LIBELLE;
				}
				else
				{
					cbxOperateur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_op_rateur__');
					check =  false;
				}
				
				if(cbxTitulaire.selectedItem != null)
				{
					_cmd.IDCOMPTE_FACTURATION	= cbxTitulaire.selectedItem.IDCOMPTE_FACTURATION;
					_cmd.LIBELLE_COMPTE			= cbxTitulaire.selectedItem.COMPTE_FACTURATION;
				}
				else
				{
					if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1 || SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
					{
						cbxTitulaire.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_titulaire__');
						check =  false;
					}
					else
						cbxTitulaire.errorString = "";
				}
				
				if(cbxRevendeur.selectedItem != null)
				{
					_cmd.LIBELLE_REVENDEUR 		= cbxRevendeur.selectedItem.LIBELLE;
					_cmd.IDSOCIETE 				= cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE;
					_cmd.IDREVENDEUR			= cbxRevendeur.selectedItem.IDREVENDEUR;
				}
				else
				{
					cbxRevendeur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_revendeur_');
					check =  false;
				}
				
				if(!isOrange)
				{
					if(cbxPointFacturation.selectedItem != null)
					{
						_cmd.IDSOUS_COMPTE		= cbxPointFacturation.selectedItem.IDSOUS_COMPTE;
						_cmd.LIBELLE_SOUSCOMPTE	= cbxPointFacturation.selectedItem.SOUS_COMPTE;
					}
					else
					{
						if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1 || SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
						{
							cbxPointFacturation.errorString = resourceManager.getString('M16','Veuillez_saisir_un_point_de_facturation__');
							check =  false;
						}
						else
							cbxPointFacturation.errorString = "";
					}
				}
				else
				{
					if(cbxPointFacturation.selectedItem != null)
					{
						_cmd.IDSOUS_COMPTE		= cbxPointFacturation.selectedItem.IDSOUS_COMPTE;
						_cmd.LIBELLE_SOUSCOMPTE	= cbxPointFacturation.selectedItem.SOUS_COMPTE;
					}
					else
					{
						_cmd.IDSOUS_COMPTE		= 0;
						_cmd.LIBELLE_SOUSCOMPTE	= '';
					}
				}
				
				if(acte)
				{
					if(cbxActeFor.selectedItem != null)
					{
						_cmd.IDACTEPOUR				= cbxActeFor.selectedItem.APPLOGINID;
						_cmd.IDGESTIONNAIRE 		= cbxActeFor.selectedItem.APPLOGINID;
						_cmd.IDPOOL_GESTIONNAIRE 	= cbxActeFor.selectedItem.IDPOOL;
					}
					else
					{
						cbxActeFor.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_gestionnaire__');
						check =  false;
					}
					
					if(cbxRevendeur.selectedItem != null)
					{
						_cmd.LIBELLE_REVENDEUR 		= cbxRevendeur.selectedItem.LIBELLE;
						_cmd.IDSOCIETE 				= cbxRevendeur.selectedItem.IDCDE_CONTACT_SOCIETE;
						_cmd.IDREVENDEUR			= cbxRevendeur.selectedItem.IDREVENDEUR;
					}
					else
					{
						cbxRevendeur.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_revendeur_');
						check =  false;
					}
				}
				
				if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 4)
				{
					if(txtNameConfig.text == "")
					{
						rdbtnChargerConfiguration.errorString = resourceManager.getString('M16','Veuillez_s_lectionner_un_mod_le');
						check = false;
					}
					else
					{
						rdbtnChargerConfiguration.errorString = "";
					}
				}
				
				if(check)
				{
					_cmd.DATE_COMMANDE 				= _date; 
					_cmd.ENCOURS 					= 1;
					_cmd.ACTION			 			= "A";
					_cmd.IDTYPE_COMMANDE 		= giveMeTypeCommande();
					_cmd.LIBELLE_TO					= txtbxTo.text;
				}
				
				_cmd.V1 = _cmd.REF_CLIENT1;
				_cmd.V2 = _cmd.REF_CLIENT2;
				
				bool = check;
			}
			catch(e:Error)
			{
				bool = false;
			}
			
			return bool;
		}
		
		private function giveMeTypeCommande():Number
		{
			var typedecommande:Number = 0;
			
			if(createLine)
				typedecommande = TypesCommandesMobile.NOUVELLES_LIGNES;
			else
				typedecommande = TypesCommandesMobile.EQUIPEMENTS_NUS;
			
			return typedecommande;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - CHAMPS PERSO
		//--------------------------------------------------------------------------------------------//
		
		private function champsPersoHandler():void
		{
			var i:int = 0;
			
			champs1 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS1;
			champs2 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS2;
			
			if(champs1 != null && champs2 != null)
			{ 
				LIBELLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.LIBELLE1;
				EXEMPLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE1;
				CHAMPS1_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE1 as Boolean;
				LIBELLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.LIBELLE2;
				EXEMPLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE2;
				CHAMPS2_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE2 as Boolean;
			}
			
			if(EXEMPLE_CHAMPS1 == "")
				CHAMPS1_EXEMPLE_VISIBLE = false;
			else
				CHAMPS1_EXEMPLE_VISIBLE = true;
			
			if(EXEMPLE_CHAMPS2 == "")
				CHAMPS2_EXEMPLE_VISIBLE = false;
			else
				CHAMPS2_EXEMPLE_VISIBLE = true;
			
			if(SessionUserObject.singletonSession.CHAMPS.hbxChamps1 != null)
			{
				var nbrChild1	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox).numChildren;
				var hbx1		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox;
				var arrayTemp1	:Array 	= copyChampsPerso((hbx1 as HBox).getChildren());
				var nbrTemp1	:int 	= arrayTemp1.length;
				
				for(i = 0;i < nbrTemp1;i++)
				{
					var libelle1:String = arrayTemp1[i].text;
					
					hbxChamps1.addChild(arrayTemp1[i]);
					
					(hbxChamps1.getChildAt(i) as TextArea).text = libelle1;
				}
			}
			
			if(SessionUserObject.singletonSession.CHAMPS.hbxChamps2 != null)
			{
				var nbrChild2	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox).numChildren;
				var hbx2		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox;
				var arrayTemp2	:Array 	= copyChampsPerso((hbx2 as HBox).getChildren());
				var nbrTemp2	:int 	= arrayTemp2.length;
				
				for(i = 0;i < nbrTemp2;i++)
				{
					var libelle2:String = arrayTemp2[i].text;
					
					hbxChamps2.addChild(arrayTemp2[i]);
					
					(hbxChamps2.getChildAt(i) as TextArea).text = libelle2;
				}
			}
		}
		
		private function copyChampsPerso(champs:Array):Array
		{
			var newChildren	:ArrayCollection = new ArrayCollection();
			var lenChildren	:int = champs.length;
			
			for(var i:int = 0;i < lenChildren;i++)
			{
				var txtarea:TextArea 	= new TextArea();				
				txtarea.width		= (champs[i] as TextArea).width;
				txtarea.height		= (champs[i] as TextArea).height;
				txtarea.restrict	= (champs[i] as TextArea).restrict;
				txtarea.maxChars	= (champs[i] as TextArea).maxChars;
				txtarea.id			= (champs[i] as TextArea).id;
				txtarea.text		= (champs[i] as TextArea).text;
				txtarea.editable	= (champs[i] as TextArea).editable;
				txtarea.selectable	= (champs[i] as TextArea).selectable;	
				txtarea.addEventListener(Event.CHANGE, textinputChangeHandler);									
				
				if(txtarea != null)
					newChildren.addItem(txtarea);
				else
					newChildren.addItem(champs[i]);
			}
			
			return newChildren.source;
		}
		
		private function textinputChangeHandler(e:Event):void
		{
			(e.currentTarget as TextArea).errorString = "";
		}
		
		private function checkChamps():Boolean
		{
			var OK				:Boolean = true;
			var i				:int 	 = 0;
			var childChamps1 	:Array 	 = hbxChamps1.getChildren();
			var childChamps2 	:Array 	 = hbxChamps2.getChildren();
			
			if(CHAMPS1_ACTIF)
			{
				for(i = 0;i < champs1.length;i++)
				{
					if(champs1[i].SAISIE_ZONE)
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs1[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps1[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							if(champs1[i].EXACT_NBR)
							{
								if((childChamps1[i*2] as TextArea).text.length != champs1[i].COMPTEUR_OBLIG)
								{
									(childChamps1[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs1[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs1[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps1[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(CHAMPS2_ACTIF)
			{
				for(i = 0;i < champs2.length;i++)
				{
					if(champs2[i].SAISIE_ZONE)
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Veuillez_saisir_le_champ__');
							
							if(OK)
								OK = false;
						}
						else
						{
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
					else
					{
						if((childChamps2[i*2] as TextArea).text == "")
						{
							if(OK)
								OK = true;
						}
						else
						{
							if(champs2[i].EXACT_NBR)
							{
								if((childChamps2[i*2] as TextArea).text.length != champs2[i].COMPTEUR_OBLIG)
								{
									(childChamps2[i*2] as TextArea).errorString = ResourceManager.getInstance().getString('M16','Ce_champs_doit_contenir_') + champs2[i].STEPPEUR_ZONE + ResourceManager.getInstance().getString('M16','_caract_res_') + champs2[i].CONTENU_ZONE.LIBELLE_CARATERE;
									
									if(OK)
										OK = false;
								}
								else
									(childChamps2[i*2] as TextArea).errorString = "";						
							}
							else
							{
								if(OK)
									OK = true;
							}
						}
					}
				}
			}
			
			if(OK)
				attributDataToCommandeObject();
			
			return OK;
		}
		
		private function attributDataToCommandeObject():void
		{
			var i				:int 	= 0;
			var childChamps1 	:Array 	= hbxChamps1.getChildren();
			var childChamps2 	:Array 	= hbxChamps2.getChildren();
			var reference1		:String	= "";
			var reference2		:String	= "";
			
			for(i = 0;i < childChamps1.length;i++)
			{
				reference1 = reference1 + childChamps1[i].text;
			}
			
			for(i = 0;i < childChamps2.length;i++)
			{
				reference2 = reference2 + childChamps2[i].text;
			}
			
			_cmd.REF_CLIENT1 = reference1;
			_cmd.REF_CLIENT2 = reference2;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					MAPPING
		//--------------------------------------------------------------------------------------------//
		
		private function mappingSiteSelectedToSite():Site
		{
			var site:Site 				= new Site();
			site.cp_site 			= cbxSites.selectedItem.SP_CODE_POSTAL;
			site.adr_site 			= cbxSites.selectedItem.ADRESSE;
			site.id_site			= cbxSites.selectedItem.IDSITE_PHYSIQUE;
			site.ref_site 			= cbxSites.selectedItem.SP_REFERENCE;
			site.code_interne_site 	= cbxSites.selectedItem.SP_CODE_INTERNE;
			site.commentaire_site 	= cbxSites.selectedItem.SP_COMMENTAIRE;
			site.commune_site 		= cbxSites.selectedItem.SP_COMMUNE;
			site.libelle_site 		= cbxSites.selectedItem.NOM_SITE;
			site.idpays_site		= cbxSites.selectedItem.PAYCONSOTELID;
			site.IS_FACTURATION		= Formator.formatInteger(cbxSites.selectedItem.IS_FACTURATION);
			site.IS_LIVRAISON		= Formator.formatInteger(cbxSites.selectedItem.IS_LIVRAISON);
			
			return site;
		}
		
		public function get revAutorisesServ():RevendeurAutoriseService { return _revAutorisesServ; }
		
		public function set revAutorisesServ(value:RevendeurAutoriseService):void
		{
			if (_revAutorisesServ == value)
				return;
			_revAutorisesServ = value;
		}
		
		public function get listeComptes():ArrayCollection { return _listeComptes; }
		
		public function set listeComptes(value:ArrayCollection):void
		{
			if (_listeComptes == value)
				return;
			_listeComptes = value;
		}
		
	}
}

