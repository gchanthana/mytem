package commandemobile.temp
{
	import commandemobile.entity.Commande;
	
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class commandeSNCF extends EventDispatcher
	{

		//---------------------------------//
		//			CONSTANTES		       //
		//---------------------------------//
		
		private const path:String = "fr.consotel.consoview.inventaire.sncf.commande_sncf";
		
		private const ABO_VOIX			:String = "Abo Voix";
		private const ABO_DATA			:String = "Abo Data";
		private const OPT_VOIX			:String = "Options Voix";
		private const OPT_DATA			:String = "Options Data";
		private const OPT_PUSH			:String = "Abo Push Mail";
		private const OPT_PUSHMAIL		:String = "Options Diverses";

		//---------------------------------//
		//			VARIABLES PRIVEES      //
		//---------------------------------//
					
		private var _numeroCommande				:String = "";
		private var _numeroMarche				:String = "";
		private var _bool						:Boolean = true;
		

		private var _profilEquipements			:ArrayCollection = new ArrayCollection();
		private var _revendeurs					:ArrayCollection = new ArrayCollection();
		private var _operateur					:ArrayCollection = new ArrayCollection();
		private var _titulaire					:ArrayCollection = new ArrayCollection();
		private var _pointFacturation			:ArrayCollection = new ArrayCollection();
		private var _codeListe					:ArrayCollection = new ArrayCollection();
		private var _listeEquipements			:ArrayCollection = new ArrayCollection();
		private var _modele						:ArrayCollection = new ArrayCollection();
		private var _listeModele				:ArrayCollection = new ArrayCollection();
		private var _listeAbonnementsOptions	:ArrayCollection = new ArrayCollection();
		private var _listeProduitsCatalogue		:ArrayCollection = new ArrayCollection();
		private var _listeProduitsCatalogueFavoris:ArrayCollection = new ArrayCollection();
//		private var 				:;

		//---------------------------------//
		//		PROPRIETEES PUBLICS    	   //
		//---------------------------------//
		
	    public function set numeroCommande(values:String):void
	    {
	    	_numeroCommande = values;
	    }
	    public function get numeroCommande():String
	    {
	    	return _numeroCommande;
	    }
	    
	    public function set profilEquipements(values:ArrayCollection):void
	    {
	    	_profilEquipements = values;
	    }
	    public function get profilEquipements():ArrayCollection
	    {
	    	return _profilEquipements;
	    }
	    
	    public function set revendeurs(values:ArrayCollection): void
	    {
	    	_revendeurs = values;
	    }
	    public function get revendeurs():ArrayCollection
	    {
	    	return _revendeurs;
	    }

	    public function set operateur(values:ArrayCollection): void
	    {
	    	_operateur = values;
	    }
	    public function get operateur(): ArrayCollection
	    {
	    	return _operateur;
	    }

	    public function set titulaire(values:ArrayCollection): void
	    {
	    	_titulaire = values;
	    }
	    public function get titulaire():ArrayCollection 
	    {
	    	return _titulaire;
	    }

	    public function set pointFacturation(values:ArrayCollection): void
	    {
	    	_pointFacturation = values;
	    }
	    public function get pointFacturation(): ArrayCollection
	    {
	    	return _pointFacturation;
	    }

	    public function set codeListe(values:ArrayCollection): void
	    {
	    	_codeListe = values;
	    }
	    public function get codeListe():ArrayCollection 
	    {
	    	return _codeListe;
	    }

	    public function set listeEquipements(values:ArrayCollection): void
	    {
	    	_listeEquipements = values;
	    }
	    public function get listeEquipements():ArrayCollection 
	    {
	    	return _listeEquipements;
	    }
		
		public function set modele(values:ArrayCollection): void
	    {
	    	_modele = values;
	    }
	    public function get modele(): ArrayCollection
	    {
	    	return _modele;
	    }
		
	    public function set listeModele(values:ArrayCollection): void
	    {
	    	_listeModele = values;
	    }
	    public function get listeModele(): ArrayCollection
	    {
	    	return _listeModele;
	    }

	    public function set numeroMarche(values:String): void
	    {
	    	_numeroMarche = values;
	    }
	    public function get numeroMarche(): String
	    {
	    	return _numeroMarche;
	    }

	    public function set listeAbonnementsOptions(values:ArrayCollection): void
	    {
	    	_listeAbonnementsOptions = values;
	    }
	    public function get listeAbonnementsOptions():ArrayCollection 
	    {
	    	return _listeAbonnementsOptions;
	    }	
		
		public function set listeProduitsCatalogue(values:ArrayCollection): void
	    {
	    	_listeProduitsCatalogue = new ArrayCollection();
	    	_listeProduitsCatalogue = values;
	    }
	    public function get listeProduitsCatalogue():ArrayCollection 
	    {
	    	return _listeProduitsCatalogue;
	    }
	    
	    public function set listeProduitsCatalogueFavoris(values:ArrayCollection): void
	    {
	    	_listeProduitsCatalogueFavoris = new ArrayCollection();
	    	_listeProduitsCatalogueFavoris = values;
	    }
	    public function get listeProduitsCatalogueFavoris():ArrayCollection 
	    {
	    	return _listeProduitsCatalogueFavoris;
	    }
		
//	    public function set (values:): void
//	    {
//	    	 = values;
//	    }
//	    public function get (): 
//	    {
//	    	return ;
		
		//---------------------------------//
		//			CONSTRUCTEUR	       //
		//---------------------------------//
		
		public function commandeSNCF()
		{
		}
		
		
		
/////////////////////////////////////

		public  function viewInternetSite(): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			path,
	    																		  "viewInternetSite",
	    																		  viewInternetSiteResultHandler);
	    	RemoteObjectUtil.callService(op);
	    }
	    
	    private function viewInternetSiteResultHandler(re:ResultEvent):void
	    {}

/////////////////////////////////	
		
		
		//---------------------------------//
		//		METHODES PUBLICS    	   //
		//---------------------------------//
		
		
		//------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
		public function genererNumeroDeCommande(): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "genererNumeroDeCommande",
	    																		  genererNumeroDeCommandeResultHandler);
	    		RemoteObjectUtil.callService(op);
	    }
	
	    protected function genererNumeroDeCommandeResultHandler(re:ResultEvent):void
	    {
	    	var object:Object = re.result as Object;
	    	if(object.RESULT > 0)
	    	{
	    		numeroCommande = object.NUMERO_COMMANDE;
	    		dispatchEvent(new Event("numeroCommande"));
	    	}
	    	else
	    	{
	    		Alert.show("Génération du numéro de commande impossible!","Consoview");
	    	}
	    }

		//------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
		public function fournirProfilEquipements(): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirListeProfiles",
	    																		  fournirProfilEquipementsResultHandler);
	    		RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,CvAccessManager.getSession().USER.CLIENTACCESSID);	
	    }

	    protected function fournirProfilEquipementsResultHandler(re:ResultEvent):void
	    {
			if(re.result != null)
			{
				var arrayC:ArrayCollection = re.result as ArrayCollection;
				profilEquipements = new ArrayCollection();
				for(var i:int = 0; i < arrayC.length;i++)
				{
					if(arrayC[i].SELECTED == 1)
					{
		    			profilEquipements.addItem(arrayC[i]);
					}
				}
				dispatchEvent(new Event("profilEquipements"));
			}
			else
			{
				Alert.show("La récupération des profils des équipements est impossible!","Consoview");
			}
	    }

		//------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
		public function fournirRevendeurs(idPoolGestion:int,idProfilEquipement:int,clefRecherche:String = ""): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirRevendeurs",
	    																		  fournirRevendeursResultHandler);
	    	RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,idPoolGestion,idProfilEquipement,clefRecherche);	
	    }
	
	    protected function fournirRevendeursResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	revendeurs = sortCompare(re.result as ArrayCollection);
				dispatchEvent(new Event("revendeurs"));
			}
			else
			{
				Alert.show("La récupération des revendeurs est impossible!","Consoview");
			}
	    }
	    
	    private function sortCompare(arrayRevendeurs:ArrayCollection):ArrayCollection
	    {
	    	var tri:Sort = new Sort();
	    	 tri.fields = [new SortField("LIBELLE", true)];
	    	 arrayRevendeurs.sort = tri;
	    	 arrayRevendeurs.refresh();
	    	 arrayRevendeurs.sort = null;
	    	 
	    	 var bool1:Boolean = true;
	    	 var bool2:Boolean = true;
	    	 var bool3:Boolean = true;
	    	 var tempArray0:ArrayCollection = new ArrayCollection();
	    	 var tempArray1:ArrayCollection = new ArrayCollection();
	    	 for(var i:int = 0;i < arrayRevendeurs.length;i++)
	    	 {
	    	 	var len:int = 0;
	    	 	if(arrayRevendeurs[i].IDREVENDEUR != 13094 && arrayRevendeurs[i].IDREVENDEUR != 13100 && arrayRevendeurs[i].IDREVENDEUR != 13099)///CHANGER PAR LID REVENDEUR "Asystel"
	    	 	{
	    	 		tempArray0.addItem(arrayRevendeurs[i]);
	    	 	}
	    	 	if(arrayRevendeurs[i].IDREVENDEUR == 13094)///CHANGER PAR L'ID REVENDEUR "Asystel"
	    	 	{
	    	 		tempArray1.addItem(arrayRevendeurs[i]);
	    	 	}
	    	 	if(arrayRevendeurs[i].IDREVENDEUR == 13100)///CHANGER PAR L'ID REVENDEUR "Saphelec"
	    	 	{
	    	 		tempArray1.addItem(arrayRevendeurs[i]);
	    	 	}
	    	 	if(arrayRevendeurs[i].IDREVENDEUR == 13099)///CHANGER PAR L'ID REVENDEUR "SFD"
	    	 	{
	    	 		tempArray1.addItem(arrayRevendeurs[i]);
	    	 	}
	    	 }
			var objectAssystel:Object = null;
	    	 for(var j:int = 0;j < tempArray1.length;j++)
	    	 {
	    	 	if(tempArray1[j].IDREVENDEUR != 13094)
	    	 	{
	    	 		tempArray0.addItem(tempArray1[j]);
	    	 	}
	    	 	else
	    	 	{
	    	 		if(tempArray1[j].IDREVENDEUR == 13094)
		    	 	{
		    	 		objectAssystel = tempArray1[j];
		    	 	}
	    	 	}
	    	 }
	    	 if(objectAssystel != null)
	    	 {
	    	 	tempArray0.addItem(objectAssystel);
	    	 }
	    	 return tempArray0;
	    }
		//------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
		public function fournirOperateur(): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirOperateur",
	    																		  fournirOperateurResultHandler);
	    		RemoteObjectUtil.callService(op);	
	    }

	    protected function fournirOperateurResultHandler(re:ResultEvent):void
	    {
	    	operateur = re.result as ArrayCollection;
			dispatchEvent(new Event("operateur"));
	    }
	    
		//------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//		
		public function fournirTitulaire(idOperateur:int): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirTitulaire",
	    																		  fournirTitulaireResultHandler);
	    		RemoteObjectUtil.callService(op,idOperateur);	
	    }
	
	    protected function fournirTitulaireResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	titulaire = re.result as ArrayCollection;
				dispatchEvent(new Event("titulaire"));
			}
			else
			{
				Alert.show("Récupération des titulaire impossible!","Consoview");
			}
	    }

	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
	    public function fournirPointFacturation(idTitulaire:int): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirPointFacturation",
	    																		  fournirPointFacturationResultHandler);
	    		RemoteObjectUtil.callService(op,idTitulaire);	
	    }
	
	    protected function fournirPointFacturationResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	pointFacturation = re.result as ArrayCollection;
				dispatchEvent(new Event("pointFacturation"));
			}
			else
			{
				Alert.show("Récupération des points de facturation impossible!","Consoview");
			}
	    }

	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
	    public function fournirCodeListe(idCompteFacturation:int): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirCodeListe",
	    																		  fournirCodeListeResultHandler);
	    		RemoteObjectUtil.callService(op,idCompteFacturation);	
	    }
	
	    protected function fournirCodeListeResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	codeListe = re.result as ArrayCollection;
				dispatchEvent(new Event("codeListe"));
			}
			else
			{
				Alert.show("Récupération des codes liste impossible!","Consoview");
			}
	    }
	    
	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
	    public function fournirListeEquipements(idGestionnaire:int,idPoolGestionnaire:int,idProfilEquipement:int,idFournisseur:int,clefRecherche:String = ""): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirListeEquipements",
	    																		  fournirListeEquipementsResultHandler);
	    		RemoteObjectUtil.callService(op,idGestionnaire,idPoolGestionnaire,idProfilEquipement,idFournisseur,clefRecherche);	
	    }
	
	    protected function fournirListeEquipementsResultHandler(re:ResultEvent):void
	    {
	    	listeEquipements = new ArrayCollection();
	    	if(re.result != null)
			{
				if(re.result.length > 0)
				{
					if(re.result[0].ERREUR != "-1")
					{
						listeEquipements = re.result as ArrayCollection;
						dispatchEvent(new Event("listeEquipements"));
					}
					else
					{
						Alert.show("Récupération de la liste des équipements impossible!","Consoview");
					}
				}
				else
				{
					Alert.show("Récupération de la liste des équipements impossible!","Consoview");
				}
			}
			else
			{
				Alert.show("Récupération de la liste des équipements impossible!","Consoview");
			}
	    }

	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
	    public function fournirListeAbonnementsOptions(idGestionnaire:int,idOperateur:int,idProfilEquipement:int,abonnement:Boolean): void
	    {
	    	_bool = abonnement;
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirListeAbonnementsOptions",
	    																		  fournirListeAbonnementsOptionsResultHandler);
	    		RemoteObjectUtil.callService(op,idGestionnaire,idOperateur,idProfilEquipement);	
	    }
	
	    protected function fournirListeAbonnementsOptionsResultHandler(re:ResultEvent):void
	    {
	    	listeProduitsCatalogue = new ArrayCollection();
			listeProduitsCatalogueFavoris = new ArrayCollection();
	    	if(re.result != null)
			{
				if(re.result.length > 0)
				{
					if(re.result[0].ERREUR != "-1")
					{
						if(_bool)//ABONNEMENT
						{
							var abonnement:ArrayCollection = re.result as ArrayCollection;
							for(var i:int = 0;i < abonnement.length;i++)//THEME_LIBELLE
							{
								if(abonnement[i].THEME_LIBELLE == ABO_VOIX || abonnement[i].THEME_LIBELLE == ABO_DATA)
								{
									listeProduitsCatalogue.addItem(abonnement[i]);
								}
							}
							for(var j:int = 0;j < listeProduitsCatalogue.length;j++)//THEME_LIBELLE
							{
								listeProduitsCatalogue[j].SELECTED = false;
								if(listeProduitsCatalogue[j].FAVORI == 1)
								{
									listeProduitsCatalogueFavoris.addItem(listeProduitsCatalogue[j]);
								}
							}
						}
						else//OPTIONS
						{
							var options:ArrayCollection = re.result as ArrayCollection;
							for(var k:int = 0;k < options.length;k++)//THEME_LIBELLE
							{
								if(options[k].THEME_LIBELLE == OPT_DATA || options[k].THEME_LIBELLE == OPT_PUSH || options[k].THEME_LIBELLE == OPT_PUSHMAIL || options[k].THEME_LIBELLE == OPT_VOIX)
								{
									listeProduitsCatalogue.addItem(options[k]);
								}
							}
							for(var l:int = 0;l < listeProduitsCatalogue.length;l++)//THEME_LIBELLE
							{
								listeProduitsCatalogue[l].SELECTED = false;
								if(listeProduitsCatalogue[l].FAVORI == 1)
								{
									listeProduitsCatalogueFavoris.addItem(listeProduitsCatalogue[l]);
								}
							}
						}
						dispatchEvent(new Event("listeAbonnementsOptions"));
					}
					else
					{
						Alert.show("Récupération de la liste des abonnements et des options impossible!","Consoview");
					}
				}
				else
				{
					Alert.show("Récupération de la liste des abonnements et des options impossible!","Consoview");
				}
			}
			else
			{
				Alert.show("Récupération de la liste des abonnements et des options impossible!","Consoview");
			}
	    }

	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
	    public function sauvegardeModele(cmd:Commande,article:XML): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "sauvegardeModele",
	    																		  sauvegardeModeleResultHandler);
	    		RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,cmd.IDGESTIONNAIRE,cmd.IDPROFIL_EQUIPEMENT,cmd.IDPOOL_GESTIONNAIRE,cmd.IDOPERATEUR,cmd.IDREVENDEUR,cmd.IDCONTACT,
	    										cmd.IDSITELIVRAISON,cmd.LIBELLE_CONFIGURATION,cmd.REF_CLIENT,cmd.REF_OPERATEUR,cmd.COMMENTAIRES,cmd.MONTANT,
	    										cmd.SEGMENT_FIXE,cmd.SEGMENT_MOBILE,cmd.IDTYPE_COMMANDE,cmd.IDGROUPE_REPERE,article);	
	    }
	
	    protected function sauvegardeModeleResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	modele = re.result as ArrayCollection;
				dispatchEvent(new Event("sauvegardeModele"));
			}
			else
			{
				Alert.show("Enregistrement du modèle de configuration impossible!","Consoview");
			}
	    }
	    
	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
	    public function fournirListeModele(idPool:int,idProfilEquipement:int = 0): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirListeModele",
	    																		  fournirListeModeleResultHandler);
	    		RemoteObjectUtil.callService(op,idPool,idProfilEquipement);	
	    }
	
	    protected function fournirListeModeleResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	listeModele = re.result as ArrayCollection;
				dispatchEvent(new Event("listeModele"));
			}
			else
			{
				Alert.show("Récupération des modèles de configuration impossible!","Consoview");
			}
	    }

	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
	    public function fournirNumeroMarche(): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  path,
	    																		  "fournirNumeroMarche",
	    																		  fournirNumeroMarcheResultHandler);
	    		RemoteObjectUtil.callService(op);	
	    }
	
	    protected function fournirNumeroMarcheResultHandler(re:ResultEvent):void
	    {
	    	if(re.result != null)
			{
		    	numeroMarche = re.result.NUMERO_MARCHE;
				dispatchEvent(new Event("numeroMarche"));
			}
			else
			{
				Alert.show("Récupération du numéro de marché impossible!","Consoview");
			}
	    }

	    //------------------------------------------------------------------------------------------------------------------------------//
		//																																//
		//------------------------------------------------------------------------------------------------------------------------------//
//	    public function (): void
//	    {
//	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//	    																		  path,
//	    																		  "",
//	    																		  ResultHandler);
//	    		RemoteObjectUtil.callService(op,);	
//	    }
//	
//	    protected function (re:ResultEvent):void
//	    {
//	    	if(re.result ! null)
//			{
//		    	 = re.result as ArrayCollection;
//				dispatchEvent(new Event(""));
//			}
//			else
//			{
//				Alert.show("!","Consoview");
//			}
//	    }
	    
	}
}