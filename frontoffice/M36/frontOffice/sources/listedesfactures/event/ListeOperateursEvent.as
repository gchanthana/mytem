package listedesfactures.event
{
	import flash.events.Event;
	
	/**
	 * <b>Classe <code>ListeOperateursEvent</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */	
	
	public class ListeOperateursEvent extends Event
	{
		public static var LF_REQUEST_ERROR:String = "lfRequestError"; 
		public static var LF_REQUEST_COMPELTE:String = "lfRequestComplete";
		
		/**
		 * <b>Constructeur <code>ListeOperateursEvent()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ListeOperateursEvent.
		 * </pre></p>
		 *
		 */
		public function ListeOperateursEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		/**
		 * <b>Fonction <code>clone()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * appel un nouveau objet ListeOperateursEvent
		 * </pre></p>
		 * 
		 * @return ListeFacturesEvent
		 * 
		 */	
		override public function clone():Event
		{
			return new ListeOperateursEvent(type,bubbles,cancelable);
		}
	}
}