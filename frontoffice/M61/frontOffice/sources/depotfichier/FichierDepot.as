package depotfichier
{
	import classe.Doc;
	import classe.Login;
	
	import composants.util.ConsoviewAlert;
	
	import events.container.ContainerEvent;
	
	import flash.desktop.ClipboardFormats;
	import flash.events.FileListEvent;
	import flash.events.NativeDragEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButtonGroup;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;
	
	import popup.ContainerprogressBar;
	
	import service.container.ContainerService;

	public class FichierDepot extends VBox
	{
	// PARAMETRES	
		public var ckxNotif:CheckBox;
		public var rbg:RadioButtonGroup;
		public var vbUser:HBox;
		public var dtgUtilisateur:DataGrid;
		[Bindable] public var cboClient:ComboBox
		public var ckxSelectAll:CheckBox
		public var progress:ContainerprogressBar
		
		private var nbFileTotal:int = 0;				// nombre de fichier drag'n'drop total
		private var nbFileCopy:int  = 1;				// nombre de fichier drag'n'drop copié
		private var rp:Doc								// doc en cours de traitement dans le tableau des fichiers drag'n'drop
		private var listeUUID:Array						// tableau des uuid des fichiers coppiés sur le ftp	
		private var parcourirFile:File
		private var listeParcourirFileSelected:Array	// tableau des fichiers sélectionnés via le bouton parcourir
		
		[Bindable] public var uploadTotal			:int = 0;
		[Bindable] public var cs:ContainerService;
		[Bindable] public var listeFichier:ArrayCollection = new ArrayCollection();
		
		[Bindable] protected var listeLoginSelected:ArrayCollection;
		
/* -----------------------------------------------------------------------------------
						MAIN, INIT et CREATIONCOMPLETE	
/* -------------------------------------------------------------------------------  */		
		public function FichierDepot()
		{
			super();
		}
		public function init():void
		{
			listeLoginSelected = new ArrayCollection();
			
			cs.addEventListener(ContainerEvent.VALIDATEDEPOT_EVENT,validate);
			
			addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER,onDragIn);
			addEventListener(NativeDragEvent.NATIVE_DRAG_DROP,onDrop);
			addEventListener(NativeDragEvent.NATIVE_DRAG_EXIT,onDragExit);
		}
		private function validate(e:ContainerEvent):void
		{
			ConsoviewAlert.afficherOKImage("Opération réussie avec succés",this)
			reset()
		}
/* -----------------------------------------------------------------------------------
								COMBOBOX CLIENT	
/* -------------------------------------------------------------------------------  */		
		protected function cboClientChangeHandler():void
		{
			if(cboClient.selectedIndex > -1)
				cs.getlisteLoginDepot(cboClient.selectedItem.IDRACINE);
		}
/* -----------------------------------------------------------------------------------
								BOUTON VALIDER 	
/* -------------------------------------------------------------------------------  */
		protected function btnValiderHandler():void
		{
			if(listeLoginSelected != null && listeFichier != null && listeLoginSelected.length > 0 && listeFichier.length > 0)
			{
				nbFileTotal = listeFichier.length
	            nbFileCopy = 0
	            
				if(listeFichier != null && listeFichier.length > 0)
	            {
	            	progress = new ContainerprogressBar();
	            	progress.maximum = uploadTotal
	            	progress.setProgressBar(0,uploadTotal);
	            	PopUpManager.addPopUp(progress,this,true);
	        		PopUpManager.centerPopUp(progress)	
	            }
	            gestFile()
	  		}
	  		else
	  		{
	  			Alert.show("Veuillez sélectionner un document et un utilisateur, au minimum","Erreur")
	  		}
		}
		// Gestion des fichiers aprés drop de fichiers
		private function gestFile():void
		{
		// on uploade le fichier vers le FTP container via le backoffice	
			(listeFichier[nbFileCopy] as Doc).addEventListener(ProgressEvent.PROGRESS,uploadProgress);
			(listeFichier[nbFileCopy] as Doc).addEventListener(ContainerEvent.UPLOAD_COMPLETE,uploadComplete);
			(listeFichier[nbFileCopy] as Doc).addEventListener(ContainerEvent.UPLOAD_ERROR, uploadError);
			(listeFichier[nbFileCopy] as Doc).addEventListener(ContainerEvent.DOCUMENT_ADDED,documentAddedHandler);
			(listeFichier[nbFileCopy] as Doc).uploadToFTP();
		}
		private function uploadProgress(e:ProgressEvent):void
		{
			progress.setProgressBar(e.bytesLoaded,e.bytesTotal);
		}
		private function uploadComplete(e:ContainerEvent):void
		{
			(listeFichier[nbFileCopy] as Doc).addToTableContainer()
		}
		private function documentAddedHandler(e:ContainerEvent):void
		{
			if(listeUUID == null)
				listeUUID = new Array()
			listeUUID.push(e.obj)
			nbFileCopy ++
			if(nbFileCopy >= nbFileTotal)
			{
				PopUpManager.removePopUp(progress);
				cs.depotDoc(listeLoginSelected,listeUUID,641)
			}
			else
			{
				gestFile()
			}
		}
	// ajouter un rapport, aprés drag and drop, à la liste des rapports d'un container d'un client
		private function addRapport(f:File):void
		{
			rp 					= new Doc();
			rp.ENCOURS 			= false;
			rp.BOOL_ERROR 		= false;
			rp.SELECTED 		= false;
			rp.SHARED 			= false;
			
			rp.DATE_DEMANDE 	= new Date();
			rp.DEMANDEUR_NOM 	= "Service Client Consotel"
			rp.FORMAT 			= f.extension
			rp.LIBELLE 			= f.name.substr(0,f.name.length - 4);
			rp.LIBELLE_SHORT 	= rp.LIBELLE.replace(" ","_");
			rp.IDRACINE 		= cboClient.selectedItem.IDRACINE
			rp.APP_LOGINID 		= 641
			rp.APPLI 			= "ConsoView"
			rp.STATUS 			= "disponible"
			rp.TAILLE 			= f.size / 1000
			rp.MODULE 			= "Intranet"
			rp.f 				= f
			if(listeFichier == null)
				listeFichier = new ArrayCollection();
			listeFichier.addItem(rp); 
		}	
		private function uploadError(e:ContainerEvent):void
		{
			ConsoviewAlert.afficherSimpleAlert("Impossible de copier ce fichier : erreur I/O","Upload erreur");
			PopUpManager.removePopUp(progress);
		}
/* -----------------------------------------------------------------------------------
								BOUTON PARCOURIR 	
/* -------------------------------------------------------------------------------  */		
		protected function btnParcourirClickHandler():void
		{
			parcourirFile = File.desktopDirectory
			parcourirFile.addEventListener(FileListEvent.SELECT_MULTIPLE, filesSelected);
			parcourirFile.browseForOpenMultiple("Sélectionnez les fichiers à ajouter");
		}
		private function filesSelected(e:FileListEvent):void
		{
			for each(var f:File in e.files)
			{
				addRapport(f)
			}
		}
/* -----------------------------------------------------------------------------------
								BOUTON RESET	
/* -------------------------------------------------------------------------------  */
		protected function reset():void
		{
			listeFichier 		= new ArrayCollection()
			listeLoginSelected 	= new ArrayCollection()
			
			for each(var lo:Login in cs.listeLoginDepot)
			{
				lo.SELECTED = false
			}
		} 
/* -----------------------------------------------------------------------------------
								BOUTON SUPPRIMER (CROIX ROUGE)
/* -------------------------------------------------------------------------------  */
		public function suppFichier(f:Doc):void
		{
			var idx:int = listeFichier.getItemIndex(f)
			listeFichier.removeItemAt(idx);
			listeFichier.refresh()
		}
/* -----------------------------------------------------------------------------------
								DRAG AND DROP 	
/* -------------------------------------------------------------------------------  */

	// Quand le fichier déplacé arrive dans le datagrid
		private function onDragIn(e:NativeDragEvent):void
		{
			if(cboClient.selectedIndex > -1)
				DragManager.acceptDragDrop(this);
		}
	// Quand on "lache" le fichier dans le datagrid
		private function onDrop(event:NativeDragEvent):void
		{
			var dropFile:Array = event.clipboard.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array; 
            
            for each (var file:File in dropFile)
            {                      
        	    if(file.size > 99999999)
        	    {
        	    	Alert.show("\nImpossible de copier un fichier de plus de 100 mo");
        	    	break;
        	    }
        	    else
        	    {
        	    	uploadTotal += file.size
					addRapport(file)
        	    }
			}
		}
	// Quand le fichier quitte le datagrid
		private function onDragExit(e:NativeDragEvent):void
		{
			
		}
/* -------------------------------------------------------------------		
					GESTION DES CASES A COCHER		
   ------------------------------------------------------------------- */		
		public function updateAddSelectedItems(bool:Boolean):void
		{
			if(bool)
			{
				listeLoginSelected.addItem(dtgUtilisateur.selectedItem as Login);
			}
			else
			{
				listeLoginSelected.removeItemAt(listeLoginSelected.getItemIndex(dtgUtilisateur.selectedItem));
			}
			if(listeLoginSelected != null)
			{
				if(listeLoginSelected.length == 0)
					ckxSelectAll.selected = false;
				if(listeLoginSelected.length == cs.listeLoginDepot.length)
					ckxSelectAll.selected = true;
			}
		}
	// quand on coche sur la combobox pour tout sélectionner
		public function ckxSelectAllClickHandler():void
		{
			listeLoginSelected = new ArrayCollection();
			for each(var rp:Login in cs.listeLoginDepot)
			{
				rp.SELECTED = ckxSelectAll.selected;
				listeLoginSelected.addItem(rp);
			}
		}
	}
}