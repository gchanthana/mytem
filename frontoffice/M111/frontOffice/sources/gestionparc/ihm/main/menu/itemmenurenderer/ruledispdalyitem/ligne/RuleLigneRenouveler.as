package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RuleLigneRenouveler implements IRuleDisplayItem
	{
		public function RuleLigneRenouveler()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneMobile();
			//return r1.isDisplay();
			return (SpecificationVO.getInstance().hasCommandeFixe || SpecificationVO.getInstance().hasCommandeMobile);
		}
	}
}
