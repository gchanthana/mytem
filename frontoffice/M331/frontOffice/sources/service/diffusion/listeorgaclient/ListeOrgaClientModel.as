package service.diffusion.listeorgaclient
{
	import event.diffusion.DiffusionEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import vo.diffusion.Orga;

	internal class ListeOrgaClientModel extends EventDispatcher
	{

		private var _organisationsClient:ArrayCollection;
		private var _idOrgaSelected:int;
		
		private var _selectedIndexOrga :int=-1; // l'index -1 par défaut
		
		public function ListeOrgaClientModel()
		{
			_organisationsClient=new ArrayCollection();
		}

		public function get organisationsClient():ArrayCollection
		{
			return _organisationsClient;
		}
		
		public function get idOrgaSelected():int
		{
			return _idOrgaSelected;
		}

		public function set idOrgaSelected(value:int):void
		{
			_idOrgaSelected = value;
		}
		
		public function get selectedIndexOrga():int
		{
			return _selectedIndexOrga;
		}
		
		public function set selectedIndexOrga(value:int):void
		{
			_selectedIndexOrga = value;
		}


		internal function listeOrgaClient(value:ArrayCollection):void
		{
			_organisationsClient.removeAll();

			var uneOrga:Orga;

			if (value.length > 0)
			{
				for (var i:int=0; i < value.length; i++)
				{
					uneOrga=new Orga();
					uneOrga.fill(value[i]);
					
					if(uneOrga.typeOrga !="ANU")
					{
						_organisationsClient.addItem(uneOrga);
					}
					
					if(uneOrga.idOrga==this._idOrgaSelected)
					{
						selectedIndexOrga=i;
					}
				}
			}
			dispatchEvent(new DiffusionEvent(DiffusionEvent.LISTE_ORGA_CLIENT_EVENT));// capturé par diffusionImpl
		}
	}
}
