package gestionparc.ihm.action.dissocier.actionDissocierSimFromLigne
{
	import gestionparc.ihm.question.ligne.QuestionDissocierSimFromLigne;

	public class ActionDissocierSimFromLigneReceiver
	{
		private var qn:QuestionDissocierSimFromLigne;

		public function ActionDissocierSimFromLigneReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			qn=new QuestionDissocierSimFromLigne();
		}
	}
}
