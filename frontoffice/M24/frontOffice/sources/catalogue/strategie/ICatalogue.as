package catalogue.strategie
{
	import catalogue.vo.AbonnementVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.ParametreSearchCatalogueVO;
	
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	public interface ICatalogue extends IEventDispatcher
	{
		function rechercheListeDataCatalogue(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void;
		function rechercheAbonnementCatalogue(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void;
		function rechercheContructeur(boolAddItemAll:Boolean=false):void;
		
		function rechercheRevendeur(idClient : Number = -1):void;
		function rechercheClient(idDistributeur : Number):void;
		function rechercheTypeEquipement(idCategorie:int=-1,boolAddItemAll:Boolean=false):void;
		function rechercheTypeCommande():void;
		function addEquipementRev(equipement : EquipementCatalogueVO, listeClt:ArrayCollection):void;
		function updateEquipementRev(equipement : EquipementCatalogueVO, listeClt:ArrayCollection):void;
		
		
		function rechercheClasseEquipement(boolAddItemAll:Boolean=false):void;
		
		function addEquipement(equipement : EquipementCatalogueVO):void;
		function updateEquipement(equipement : EquipementCatalogueVO):void;
		function updateNiveau(id : int,idClient : int,niveau : int):void;
		function updateNiveauAbo(idAbo : int,idClient : int,niveau : int):void;
		function removeEquipement(equipement : EquipementCatalogueVO):void;
		function removeAbonnement(idClient : int,equipement : AbonnementVO):void;
		
		function get listeAbonnementCatalogue():ArrayCollection;
		function get listeDataCatalogue():ArrayCollection;
		function get listeConstructeur():ArrayCollection;
		function get listeTypeEquipement():ArrayCollection;
		function get listeClasseEquipement():ArrayCollection;
		function get listeRevendeur():ArrayCollection;
		function get listeClient():ArrayCollection;
		function get listeTypeCommande():ArrayCollection;
		
		
	}
}