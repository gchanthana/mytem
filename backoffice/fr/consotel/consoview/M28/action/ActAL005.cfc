﻿<cfcomponent  extends="AbstractAction">
	
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true" hint="contient idFonction">
		
		<cftry>
			<cfstoredproc procedure="pkg_M28.getParametreFonction2" datasource="ROCOFFRE">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idFonction#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#getCodeLangue()#">
				<cfprocresult name="p_retour" >
			</cfstoredproc>
			
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.LISTE_DROITS = p_retour>
				<cfreturn CreateReponse("AL005",1,data)/>
			 <cfelse>
                <cfset err = ThrowCfCatchError(PFCT0010,"AL005","Erreur de procédure","","")>
                <cfreturn CreateReponse("AL005",0,err)/>
			</cfif>
			<cfcatch type="any" >
				<cfset err = ThrowCfCatchError("b000",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
                <cfreturn CreateReponse("AL005",0,err)/>
			</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>