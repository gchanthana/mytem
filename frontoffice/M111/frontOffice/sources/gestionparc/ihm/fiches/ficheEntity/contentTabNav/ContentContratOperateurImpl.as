package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	
	import mx.containers.HBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;

	[Bindable]
	public class ContentContratOperateurImpl extends HBox
	{
		public var dcDateOuverture:CvDateChooser;
		public var dcDateRenouvellement:CvDateChooser;
		public var dcDateResiliation:CvDateChooser;

		public var lbDateEligibilite:Label;
		public var lbDateLastFacture:Label;
		public var lbDateFpc:Label;
		public var lbOperateur:Label;

		public var lbDureeContrat:Label;
		public var lbDateOuverture:Label;
		public var lbDateRenouvellement:Label;
		public var lbDateResiliation:Label;
		public var lbDureeEligibilite:Label;

		public var cbDureeContrat:ComboBox;
		public var cbDureeEligibilite:ComboBox;

		public function ContentContratOperateurImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialisation générale de cette classe  :
		 */
		private function init(event:FlexEvent):void
		{
			initListener();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			if (dcDateOuverture) // fiche ligne
			{
				cbDureeContrat.addEventListener(ListEvent.CHANGE, dcDateChangeHandler);
				cbDureeEligibilite.addEventListener(ListEvent.CHANGE, dcDateChangeHandler);
				dcDateOuverture.addEventListener(CalendarLayoutChangeEvent.CHANGE, dcDateChangeHandler);
				dcDateRenouvellement.addEventListener(CalendarLayoutChangeEvent.CHANGE, dcDateChangeHandler);
			}
		}

		/**
		 * Cette fonction est appelée après le changement de la date d'ouverture, de la date de renouvellement,
		 * de la duree d'eligibilite ou de la duree du contrat.
		 */
		private function dcDateChangeHandler(event:Event):void
		{
			if (cbDureeContrat.selectedIndex > -1 && cbDureeEligibilite.selectedIndex > -1 && (dcDateOuverture.selectedDate != null || dcDateRenouvellement.selectedDate != null))
			{
				lbDateEligibilite.text=DateFunction.formatDateAsString(calculDateEligibilite());
				lbDateFpc.text=DateFunction.formatDateAsString(calculDateFPC());
			}
		}

		/**
		 * Calcul la date d'eligibilite
		 */
		private function calculDateEligibilite():Date
		{
			if (dcDateRenouvellement.selectedDate != null)
			{
				return DateFunction.dateAdd("m", dcDateRenouvellement.selectedDate, cbDureeEligibilite.selectedItem.value);
			}
			else
			{
				return DateFunction.dateAdd("m", dcDateOuverture.selectedDate, cbDureeEligibilite.selectedItem.value);
			}
		}

		/**
		 * Calcul la date FPC
		 */
		private function calculDateFPC():Date
		{
			if (dcDateRenouvellement.selectedDate != null)
			{
				return DateFunction.dateAdd("m", dcDateRenouvellement.selectedDate, cbDureeContrat.selectedItem.value);
			}
			else
			{
				return DateFunction.dateAdd("m", dcDateOuverture.selectedDate, cbDureeContrat.selectedItem.value);
			}
		}
	}
}
