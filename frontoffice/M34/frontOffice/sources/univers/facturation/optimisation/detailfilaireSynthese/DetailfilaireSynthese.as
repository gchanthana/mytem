package univers.facturation.optimisation.detailfilaireSynthese {
	import univers.facturation.optimisation.AbstractReport;
	import mx.events.FlexEvent;
	import flash.net.URLRequest;
	import mx.collections.ArrayCollection;
	import univers.facturation.optimisation.deploiement.Deploiement;
	
	public class DetailfilaireSynthese extends DetailfilaireSyntheseIHM {
		private var periodiciteArray:ArrayCollection =
				new ArrayCollection([{label: Deploiement.PERIODICITE_MENSUEL_LABEL, value: Deploiement.PERIODICITE_MENSUEL},
							{label: Deploiement.PERIODICITE_BIMESTRIEL_LABEL, value: Deploiement.PERIODICITE_BIMESTRIEL}]);
		
		public function DetailfilaireSynthese() {
			super();
			reportType = "DetailFilaireSynthese";
			selectorThumbCount = 1; // 1 Mois à choisir
			formatList = new Array(2);
			var xlsObj:Object = new Object();
			xlsObj.label = AbstractReport.XLS_FORMAT_LABEL;
			xlsObj.value = AbstractReport.XLS_FORMAT_VALUE;
			var pdfObj:Object = new Object();
			pdfObj.label = AbstractReport.PDF_FORMAT_LABEL;
			pdfObj.value = AbstractReport.PDF_FORMAT_VALUE;
			xlsObj.enabled = pdfObj.enabled = true;
			formatList[0] = xlsObj;
			formatList[1] = pdfObj;
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected override function initIHM(event:FlexEvent):void {
			super.initIHM(event);
			cmbPeriodicite.dataProvider = periodiciteArray;
		}
		
		public override function getReportName():String {
			return "Rapport Téléphonie Fixe Global";
		}
		
		protected override function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			super.getReportUrlRequest(secureBool);
			urlRequestPath.data.PERIODICITE = cmbPeriodicite.selectedItem.value;
			return urlRequestPath;
		}
	}
}
