package gestionparc.ihm.main.restriction
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import gestionparc.entity.AbstractVue;
	import gestionparc.entity.RestrictionVO;

	public class GestionnaireRestriction
	{
		private var vueSelected:AbstractVue;

		private var listeRestriction:ArrayCollection;

		public function GestionnaireRestriction()
		{
			generateTableau();
		}

		public function buildListRestriction(v:AbstractVue):ArrayCollection
		{
			var arr:ArrayCollection=new ArrayCollection();
			vueSelected=v;

			for each (var item:RestrictionVO in listeRestriction)
			{
				if (item.ENABLED)
				{
					if ((item.ISRESTRICTIONCOLLABORATEUR && vueSelected.ISRESTRICTIONCOLLABORATEUR) || (item.ISRESTRICTIONCONTRAT && vueSelected.ISRESTRICTIONCONTRAT) || (item.ISRESTRICTIONEQUIPEMENT && vueSelected.ISRESTRICTIONEQUIPEMENT) || (item.ISRESTRICTIONLIGNE && vueSelected.ISRESTRICTIONLIGNE) || (item.ISRESTRICTIONSIM && vueSelected.ISRESTRICTIONSIM) || (item.ISCONSOMMATION && vueSelected.ISVUECONSOS))
					{
						arr.addItem(item);
					}
				}
			}
			return arr;
		}

		private function filtrer(item:Object):Boolean
		{
			if ((item as RestrictionVO).ENABLED)
			{

				if (((item as RestrictionVO).ISRESTRICTIONCOLLABORATEUR && vueSelected.ISRESTRICTIONCOLLABORATEUR) || ((item as RestrictionVO).ISRESTRICTIONSIM && vueSelected.ISRESTRICTIONSIM) || ((item as RestrictionVO).ISRESTRICTIONCONTRAT && vueSelected.ISRESTRICTIONCONTRAT) || ((item as RestrictionVO).ISRESTRICTIONEQUIPEMENT && vueSelected.ISRESTRICTIONEQUIPEMENT) || ((item as RestrictionVO).ISRESTRICTIONLIGNE && vueSelected.ISRESTRICTIONLIGNE))
					return true;
				else
					return false;

			}
			else
				return false;
		}

		private function generateTableau():void
		{
			listeRestriction=new ArrayCollection();

			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Aucun_crit_re'), 0, false, true, false, false, true, null, true, true, true, true, true));

			/** restriction équipement */
			
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'EQUIPEMENT'), -1, true, false, false, false, true, null, false, false, true, false, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'En_SAV'), 1, false, false, false, false, true, null, false, false, true, false, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Au_rebus'), 2, false, false, false, false, true, null, false, false, true, false, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'En_stock'), 3, false, false, false, false, true, null, false, false, true, false, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Non_restitu_'), 4, false, false, false, false, true, null, false, false, true, false, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Terminal_BYOD'), 31, false, false, false, false, true, null, false, false, true, false, false, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Terminal_pro'), 32, false, false, false, false, true, null, false, false, true, false, false, false));

			/** restriction collaborateur  */
			
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'COLLABORATEUR'), -1, true, false, false, false, true, null, false, false, false, true, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Sans__quipement'), 6, false, false, false, false, true, null, false, false, false, true, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Sans_ligne'), 7, false, false, false, false, true, null, false, false, false, true, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Avec_SIM_sans__quipement'), 8, false, false, false, false, true, null, false, false, false, true, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Avec_plusieurs__quipements'), 9, false, false, false, false, true, null, false, false, false, true, false));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Hors_de_l_entreprise'), 12, false, false, false, false, true, null, false, false, false, true, false));

			
			/** restriction ligne  */
			
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'LIGNE'), -1, true, false, false, false, true, null, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Sans__quipement'), 14, false, false, false, false, true, null, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Libres'), 15, false, false, false, false, true, null, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'R_sili_es'), 16, false, false, false, false, true, null, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Suspendues'), 17, false, false, false, false, true, null, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'C_d_es'), 18, false, false, false, false, true, null, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Eligibilit_s___N_jours'), 19, false, false, true, false, true, null, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Active_lines'), 35, false, false, false, false, true, null, true));
			
			
			
			/** restriction SIM  */
			
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'SIM'), -1, true, false, false, false, true, null, false, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'En_spare_SIM_sans_ligne_'), 21, false, false, false, false, true, null, false, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Au_rebus'), 22, false, false, false, false, true, null, false, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Sim_BYOD'), 33, false, false, false, false, true, null, false, true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Sim_pro'), 34, false, false, false, false, true, null, false, true));

			/** restriction extension  */
			
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'EXTENSION_'), -1, true, false, false, false, true, null, false,true));
			listeRestriction.addItem(createRestrictionVO(ResourceManager.getInstance().getString('M111', 'Non_attribu__'), 30, false, false, false, false, true, null, false, true));
			
		}

		/**
		 * Pour ajouter une restriction, il faut renseigner ces champs :
		 * 	- lib = le libellè visible dans la liste
		 *  - data = l'idrestriction, est envoyé à la procédure de recherche
		 *  - title = précise si cette restriction est un titre ou pas, ne concerne que le classement et l'itemrenderer de la liste
		 *  - mobile = précise si c'est une restriction mobile, en fonction du choix
		 *  - fixe = précise si c'est une restriction fixe, en fonction du choix
		 *  - factu = précise si c'est une restriction facturation, en fonction du choix
		 *  - saisie = précise si la restrction nécessite une saisie de la part de l'utilisateur
		 *  - value = précise si la restriction nécessite un choix dans une liste de la part de l'utilisateur
		 *  - enab = précise si la restriction est autorisée
		 *  - listeValue = la liste des values quand la restriction est "value = true"		 *
		 * **/
		private function createRestrictionVO(lib:String, data:int, title:Boolean, factu:Boolean, saisie:Boolean, value:Boolean, enab:Boolean,
											 listeValue:ArrayCollection=null, isligne:Boolean=false, isSIM:Boolean=false, isEquipmeent:Boolean=false,
											 isCollab:Boolean=false, isContrat:Boolean=false, isConso:Boolean=false, isExtension:Boolean=false):RestrictionVO
		{
			var myRest:RestrictionVO=new RestrictionVO();
			myRest.LIBELLE=lib;
			myRest.DATA=data;
			myRest.ISTITLE=title;
			myRest.ISFACTURATION=factu;
			myRest.SAISIE=saisie;
			myRest.VALUE=value;
			myRest.LISTE_VALUE=listeValue;
			myRest.ENABLED=enab;
			myRest.ISRESTRICTIONCOLLABORATEUR=isCollab;
			myRest.ISRESTRICTIONCONTRAT=isContrat;
			myRest.ISRESTRICTIONEQUIPEMENT=isEquipmeent;
			myRest.ISRESTRICTIONLIGNE=isligne;
			myRest.ISRESTRICTIONSIM=isSIM;
			myRest.ISCONSOMMATION=isConso;
			return myRest;
		}
	}
}