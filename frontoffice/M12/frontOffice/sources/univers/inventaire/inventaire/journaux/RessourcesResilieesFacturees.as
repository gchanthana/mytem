package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import composants.util.DateFunction;
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.ConsoviewFormatter;
	import mx.controls.Alert;
	
	
	/**
	 * Journal des ressources qui ont été résiliées et qui sont toujours facturées	
	 * */
	public class RessourcesResilieesFacturees extends RessourcesResilieesFactureesIHM implements IJournal
	{
		//Date de début de périiode
		private var dateDebut : String;
		
		//Date de fin de période
		private var dateFin : String;
		
				
		//ArrayCollection contenant les lignes du journal
		[Bindable]
		private var elementsDuJournal : ArrayCollection;
		
		//Reference vers la méthode distante qui ramène les lignes du journal
		private var opElementDuJournal : AbstractOperation;
		
		//les information du noeud (inutilisé ici)
		private var _nodeInfos : Object;
		
		
		/**
		 * Constructeur
		 * */			
		public function RessourcesResilieesFacturees()
		{
			//TODO: implement function
			super();			 
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
			
		/**
		 * Recharge les données du journal
		 * */
		public function refresh():void{
			//tente de couper le remoting
			if((dateDebut != null) && (dateFin != null))
				chargerDonnees();
		 
		}
		
		
		/**
		 * Affecte les infos sur le noeud au journal
		 * */
		 public function set nodeInfos(infos : Object):void{
		 	_nodeInfos = infos;
		 }
		 
		 
		/**
		* Fonction qui permet d'afficher la période sur laquelle on travaille, au format jour mois annee 
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		* 
		* */
		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
															
			var formMoisD : String = moisDebut.substr(0,2) 
									+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
									+ moisDebut.substr(6,4);
									
			var formMoisF : String = moisFin.substr(0,2)
								   	+ " " 
								   	+ DateFunction.moisEnLettre(parseInt(moisFin.substr(3,2),10)-1) 
								   	+ " " 
								   	+ moisFin.substr(6,4);

			//labelPeriode.text = "Factures émisent du " + formMoisD + " au " + formMoisF;					
		}
		
		
		/**
		 * Gere le changement de périmetre
		 * */
		public function onPerimetreChange():void{
			//tente de couper le remoting
			
			//init filtre
			txtFiltre.text = "";
			
			//init periode
			initPeriode();
			setSelectedPeriode(dateDebut,dateFin);	
			
			//init le data provider
			elementsDuJournal.source = null
			elementsDuJournal.refresh();
			
			//données
			chargerDonnees();
			
			
		}
		
		/**
		 * Interrompt les remotings en cours
		 * (ne fait rien)
		 * */
		public function cancelRemotings():void{
			try{
				 
			}catch(e : Error){
				trace("ok");
			}
		}
		
//--- INITIALISATION ---------------------------------------------------------------------------------------------------------------------------
		
		//Initialisation de l'IHM
		//et charge les données pour la periode
		private function initIHM(fe : FlexEvent):void{
			//Periode
			initPeriode();				
			myPeriodeSelector.addEventListener("periodeChange",updatePeriode); 				
			setSelectedPeriode(dateDebut,dateFin);							
						
			//Filtre
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
			
			//grid			
			montantfacture.labelFunction = formateEuros;
			numligne.labelFunction = formateLigne;
			colPeriode.labelFunction = null;		
			dateresi.labelFunction = formateDates;				
			chargerDonnees();		
		}
		
		//Initialisation de la periode 
		private function initPeriode():void{
			var periodeArray : Array = myPeriodeSelector.getTabPeriode();
		    var len : int = periodeArray.length;
		    var month : AMonth;
		    					   
			month = periodeArray[len-2];	
							
			dateDebut = month.getDateDebut();	
			dateFin = month.getDateFin();
		}		
		
		//Formate une colonne de DataGrid avec le symbole Euros	
		private function formateEuros(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);						
		}
		
		//Formate les numéros de téléphone  d'une colonne de DataGrid
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		//Formate les dates (jj/mm/aaaa) d'une colonne de DataGrid
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			return DateFunction.formatDateAsString(ladate);			
		}
		
//--- FIN INITIALISATION -----------------------------------------------------------------------------------------------------------------------

	

//--- PERIODE SELECTOR -------------------------------------------------------------------------------------------------------------------------
	
		//Handler du chagement de date
		//Met à jour la date pour le journal, affiche la date et charge les données pour cette date
		private function updatePeriode(pe : PeriodeEvent):void{
			
			dateDebut = pe.moisDeb;
			dateFin = pe.moisFin;			
			setSelectedPeriode(dateDebut,dateFin);
			chargerDonnees();	
			
		}
		
		
//--- FIN PERIODE SELECTOR ---------------------------------------------------------------------------------------------------------------------
		
				
//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------
		
		//Gere le filtre du tableau
		private function filtrerGrid(ev :Event):void{
			try{
				elementsDuJournal.filterFunction = filterFunc;
				elementsDuJournal.refresh();
			}catch(e : Error){
				trace("no data");
			}
					
		}
		
		
		//filtre sur les attributs OPNOM, LIBELLE_PRODUIT, SOUS_TETE, MONTANT_FACTURE des elements du Tabelau
		private function filterFunc(value : Object):Boolean{				
			if ((String(value.OPNOM.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.LIBELLE_PRODUIT.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.SOUS_TETE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.MONTANT_FACTURE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1))
			{
				return (true);
			} else {
				return (false);
			}	
		}
		
		
//--- FIN FILTRE -------------------------------------------------------------------------------------------------------------------------------


//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		
		//Charge les donnees du Journal
		private function chargerDonnees():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
			try{
				 
			}catch(e : Error){
				trace("ok");
			}finally{
				lblError.visible = false;		
				opElementDuJournal = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																					"getRessourcesResilieesTjrsFacturees",
																					chargerDonneesResultHandler);						
				RemoteObjectUtil.callService(opElementDuJournal,
											idGroupeMaitre);		
			}
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerDonnees' à l'ArrayCollection 'elementsDuJournal' et les affiches
		private function chargerDonneesResultHandler(re :ResultEvent):void{
			elementsDuJournal = re.result as ArrayCollection;	
			if (elementsDuJournal.length > 0){
				txtFiltre.editable = true;				
			} else {
				txtFiltre.editable = false;
				lblError.visible = true;
			}
			myGrid.dataProvider = elementsDuJournal;
			elementsDuJournal.refresh();
		}
						
	
//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------


	}
}