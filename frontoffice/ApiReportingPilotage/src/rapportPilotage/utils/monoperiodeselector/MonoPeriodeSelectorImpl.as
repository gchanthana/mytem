package rapportPilotage.utils.monoperiodeselector
{
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.events.SliderEvent;
	
	import rapportPilotage.event.RapportEvent;
	import rapportPilotage.event.RapportIHMEvent;
	import rapportPilotage.service.monoperiodeselector.MonoPeriodeSelectorService;
	import rapportPilotage.utils.SelectorAbstract;
	import rapportPilotage.utils.serialization.json.*;
	import rapportPilotage.vo.Parametre;
	import rapportPilotage.vo.SelectorParametre;
	
	public class MonoPeriodeSelectorImpl extends SelectorAbstract
	{
		[Bindable]public var monoPeriodeSelectorService:MonoPeriodeSelectorService;
		[Bindable]public var monoperiode:MonoPeriodeSelector;
		
		[Bindable]public var clesMonoPeriode:ArrayCollection;
		[Bindable]public var defaultValues:ArrayCollection;
		[Bindable]public var selectedLibelleMois:String;
		
		public var arrayCleLibelle :Object;
		
		public function MonoPeriodeSelectorImpl(selector:Object=null)
		{
			super(selector);			
		}
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;	
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
				
		protected function monoperiode_creationCompleteHandler(event:FlexEvent):void
		{
			monoPeriodeSelectorService=new MonoPeriodeSelectorService();
			monoPeriodeSelectorService.model.addEventListener(RapportEvent.USER_MONO_PERIODE_SELECTOR_EVENT,fillData);
			monoperiode.addEventListener(RapportIHMEvent.CHANGER_VALUE_HSLIDER_EVENT,getLibelleSelectedPeriode);
			monoPeriodeSelectorService.getValuesMonoPeriode();
			
			showHelp();			
		}
		
		private function fillData(event:RapportEvent):void
		{
			var valuesMonoPeriode: ArrayCollection=monoPeriodeSelectorService.model.getValuesMonoPeriode;
			monoperiode.setValuePeriode(valuesMonoPeriode);// permert d'initialiser les valeurs du selecteur
			getLibelleSelectedPeriode(null);// pour afficher le libelle lors de la creation
		}
		
		private function getLibelleSelectedPeriode(event:RapportIHMEvent):void
		{	
			selectedLibelleMois = monoperiode.getSelectedLibellePeriode().libellePeriode; 
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var is_system:Boolean;
			var valueParam:Array=[];
			
			var selecteddate:String = monoperiode.getSelectedIdDate().Idmois;
						
			valueParam.push(selecteddate);
			valueParam.push(selecteddate);
			
			valueParam.push(selectedLibelleMois);
						
			for(var i : int = 0 ; i< arrayCleLibelle.length ; i++)
			{
				var parametre :Parametre=  new Parametre();
				
				if(arrayCleLibelle[i].IS_SYSTEM== 1)
					is_system=true;
				else
					is_system=false;
				
				parametre.value=[valueParam[i]];	//arrayCleLibelle[i].NOM_CLE=U_IDPERIODE_DEBUT
				parametre.is_system = is_system;
				parametre.cle =arrayCleLibelle[i].NOM_CLE;
				parametre.libelle=arrayCleLibelle[i].LIBELLE_CLE;
				parametre.typeValue=this.typeParametreComposant.toUpperCase();
				
				tabValeur.push(parametre);
			}
			
			return tabValeur;
		}		
	}
}