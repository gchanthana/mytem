<!--- =========================================================================
Classe: GroupeSousCompteCompletStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="GroupeSousCompteCompletStrategy" hint="" extends="AbstractStrategy">
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="GroupeSousCompteCompletStrategy" hint="Remplace le constructeur de GroupeSousCompteCompletStrategy.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->
	<cffunction name="getProduit" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_catalogue=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.libelle_produit>
	</cffunction>
	
	<cffunction name="getTypeProduit" access="public" returntype="string" output="false" displayname="string getTypeProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qGetDetailTheme" datasource="#session.OffreDSN#">
			SELECT th.*, pcl.libelle_produit
			FROM theme_produit th, theme_produit_catalogue tpc, produit_catalogue pca, produit_client pcl
			WHERE th.idtheme_produit=tpc.idtheme_produit
			AND tpc.idproduit_catalogue=pca.idproduit_catalogue
			AND pca.idproduit_catalogue=pcl.idproduit_catalogue
			AND pcl.idproduit_catalogue=#IDproduit#
		</cfquery>
		<cfreturn qGetDetailTheme.theme_libelle>
	</cffunction>
	
	<cffunction name="getOperateur" access="public" returntype="string" output="false" displayname="string getProduit(numeric)" >
		<cfargument name="IDproduit" required="false" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfquery name="qgetOperateur" datasource="#session.OFFREDSN#">
			SELECT o.nom
			FROM produit_catalogue pca, operateur o
			WHERE pca.idproduit_catalogue=#IDproduit#
			AND pca.operateurID=o.operateurID
		</cfquery>
		<cfreturn qgetOperateur.nom>
	</cffunction>
	
	<cffunction name="getCompte" access="public" returntype="string" output="false" displayname="string getCompte(numeric)" >
		<cfargument name="IDcompte" required="false" type="numeric" default="" displayname="numeric IDcompte" hint="Initial value for the IDcompteproperty." />
		<cfquery name="qGetCompte" datasource="#session.OffreDSN#">
			select libelle_groupe_client
			from groupe_client gc
			where gc.idgroupe_client=#ID#
		</cfquery>
		<cfreturn qGetCompte.libelle_groupe_client>
	</cffunction>
	
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, date, date)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailProduit" datasource="#session.OFFREDSN#">
			SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
		           	SUM(df.duree_appel) AS duree_appel, df.idproduit_client,
		           	pc.idproduit_catalogue, pca.prorata, st.sous_tete, st.siteID, st.idsous_tete,
		           	st.nom_site, decode(st.adresse1,'Flotte Mobile','',st.adresse1) as adresse1, st.adresse2,decode(nvl(trim(st.zipcode),'-1'),'-1',' ',st.zipcode) as zipcode, 
					decode(st.commune,'Communes',' ',st.commune) as commune, ip.numero_facture, th.type_theme,ip.idinventaire_periode, st.commentaires
		    FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, 
		         	theme_produit th, theme_produit_catalogue tpc, 
	      			(	SELECT a.idinventaire_periode, a.date_emission, a.numero_facture
	      				FROM inventaire_periode a, 
	      						(	SELECT DISTINCT b.idcompte_facturation
	      							FROM compte_facturation b, groupe_client_ref_client c,
	      									sous_compte sco
	      							WHERE sco.idcompte_facturation=b.idcompte_facturation
	      									AND sco.idsous_compte=c.idref_client
	      									AND c.idgroupe_client=#ID# 
	      						) e
	      				WHERE e.idcompte_facturation=a.idcompte_facturation
                         		AND trunc(a.date_emission)<=trunc(last_day(#DateFin#))
									AND trunc(a.date_emission)>=trunc(#DateDebut#,'MM')
		    		   ) ip,
						(	SELECT f.*, i.*
							FROM sous_tete f, sous_compte g, groupe_client_ref_client h, site_client i
							WHERE f.idsous_compte=g.idsous_compte 
									AND g.idsous_compte=h.idref_client
									and g.siteid=i.siteid
									AND h.idgroupe_client=#ID#
									
						) st
		    WHERE 	df.idinventaire_periode=ip.idinventaire_periode
		           	AND df.idproduit_client=pc.idproduit_client 
		           	AND th.idtheme_produit=tpc.idtheme_produit
						AND tpc.idproduit_catalogue=pca.idproduit_catalogue
		           	AND pca.idproduit_catalogue=pc.idproduit_catalogue
		           	AND df.idsous_tete=st.idsous_tete
			GROUP BY df.idproduit_client, df.idinventaire_periode, pc.idproduit_catalogue, 
		           	pca.prorata, st.sous_tete, st.nom_site, decode(st.adresse1,'Flotte Mobile','',st.adresse1), st.adresse2,
		           	decode(nvl(trim(st.zipcode),'-1'),'-1',' ',st.zipcode), 
					decode(st.commune,'Communes',' ',st.commune),
				   	st.siteID, numero_facture, th.type_theme,ip.idinventaire_periode, st.idsous_tete, st.commentaires
		    HAVING pc.idproduit_catalogue=#IDproduit#
		    ORDER BY montant_final desc, siteID,nom_site, adresse1, sous_tete
		</cfquery>
		<cfreturn qGetDetailProduit>
	</cffunction>

	<cffunction name="getData_cat" access="public" returntype="query" output="false" displayname="query getData(numeric, date, date)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDproduit" required="true" type="numeric" default="" displayname="numeric IDproduit" hint="Initial value for the IDproduitproperty." />
		<cfargument name="DateDebut" required="true" type="string" default="" displayname="date DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="true" type="string" default="" displayname="date DateFin" hint="Initial value for the DateFinproperty." />
		<cfquery name="qGetDetailProduit" datasource="#session.OFFREDSN#">
			SELECT 	SUM(df.qte) AS qte, SUM(df.montant) AS montant_final, SUM(df.nombre_appel) AS nombre_appel,
		           	SUM(df.duree_appel) AS duree_appel, df.idproduit_client,
		           	pc.idproduit_catalogue, pca.prorata, st.sous_tete, st.siteID, st.idsous_tete,
		           	st.nom_site, decode(st.adresse1,'Flotte Mobile','',st.adresse1) as adresse1, st.adresse2,decode(nvl(trim(st.zipcode),'-1'),'-1',' ',st.zipcode) as zipcode, 
					decode(st.commune,'Communes',' ',st.commune) as commune, ip.numero_facture, th.type_theme,ip.idinventaire_periode
		    FROM 	detail_facture_abo df, produit_client pc, produit_catalogue pca, 
		         	theme_produit th, theme_produit_catalogue tpc, 
						(	SELECT a.idinventaire_periode, a.numero_facture
							FROM inventaire_periode a, compte_facturation b, groupe_client_ref_client c, sous_compte d
							WHERE a.idcompte_facturation=b.idcompte_facturation
									AND b.idcompte_facturation=d.idcompte_facturation
									AND d.idsous_compte=c.idref_client
									AND c.idgroupe_client=#ID#
                         	AND trunc(a.date_emission)<=trunc(last_day(#DateFin#))
									AND trunc(a.date_emission)>=trunc(#DateDebut#,'MM')
							group by a.idinventaire_periode, a.numero_facture
					   ) ip,
						(	SELECT f.*, i.*
							FROM sous_tete f, sous_compte g, groupe_client_ref_client h, site_client i
							WHERE f.idsous_compte=g.idsous_compte 
									AND g.idsous_compte=h.idref_client
									and g.siteid=i.siteid
									AND h.idgroupe_client=#ID#
									
						) st

		    WHERE 	df.idinventaire_periode=ip.idinventaire_periode
		           	AND df.idproduit_client=pc.idproduit_client 
		           	AND th.idtheme_produit=tpc.idtheme_produit
						AND tpc.idproduit_catalogue=pca.idproduit_catalogue
		           	AND pca.idproduit_catalogue=pc.idproduit_catalogue
		           	AND df.idsous_tete=st.idsous_tete
			GROUP BY df.idproduit_client, df.idinventaire_periode, pc.idproduit_catalogue, 
		           	pca.prorata, st.sous_tete, st.nom_site, decode(st.adresse1,'Flotte Mobile','',st.adresse1), st.adresse2,
		           	decode(nvl(trim(st.zipcode),'-1'),'-1',' ',st.zipcode), 
					decode(st.commune,'Communes',' ',st.commune),
				   	st.siteID, numero_facture, th.type_theme,ip.idinventaire_periode, st.idsous_tete
		    having pc.idproduit_catalogue=#IDproduit#
		    ORDER BY montant_final desc, siteID,nom_site, adresse1, sous_tete
		</cfquery>
		<cfreturn qGetDetailProduit>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn "Groupe de Sous Comptes">
	</cffunction>
</cfcomponent>