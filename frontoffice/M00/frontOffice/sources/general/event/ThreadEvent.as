package general.event
{
	import flash.events.Event;
	
	public class ThreadEvent extends Event
	{
		public static var THREAD_FINISH_EVENT:String = "THREAD_FINISH_EVENT";
		public static var GETDATAUSER_FINISH_EVENT:String = "GETDATAUSER_FINISH_EVENT";
		
		public function ThreadEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}