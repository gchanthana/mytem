<cfinterface displayName="fr.consotel.consoview.api.ibis.publisher.report.IReportDefinition">
	<cffunction name="getName" returntype="string" description="Returns report name"/>
	
	<cffunction name="getReportAbsolutePath" returntype="string" description="Returns report absolute path"/>
	
	<cffunction name="setParameterValues" returntype="boolean" description="Set parameter values. Returns TRUE if successful and FALSE otherwise.">
		<cfargument name="paramName" type="string" required="true" hint="Parameter Unique Name">
		<cfargument name="paramValues" type="array" required="true" hint="Parameter Values as an Array">
	</cffunction>

	<cffunction name="getReportParameterNameValues" returntype="array"
				description="Returns Report Parameters Array which is used/provided when executing report. An empty array is returned when fails."/>
	
	<cffunction name="getParametersInfos" returntype="struct"
				description="Returns Report Parameters Infos as a structure. An empty structure is returned if infos are not available"/>

	<cffunction name="getTemplateInfos" returntype="struct" description="Returns Template Infos as a structure which is empty if fails.">
		<cfargument name="templateId" type="string" required="true" hint="Template Unique ID">
	</cffunction>
				
	<cffunction name="getTemplateListInfos" returntype="struct" description="Returns Templates Infos as a structure which is empty if fails."/>
</cfinterface>
