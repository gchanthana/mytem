package composants.util
{
	[RemoteClass(alias="fr.consotel.consoview.util.Mail")]

	[Bindable]
	public class MailVO
	{

		public var expediteur:String = "";
		public var destinataire:String = "";
		public var sujet:String = "";
		public var module:String = "";
		public var type:String = "html";
		public var repondreA:String = "";
		public var cc:String = "";
		public var bcc:String = "";
		public var charset:String = "utf-8";
		public var message:String = "";
		public var copiePourExpediteur:String = "YES";
		public var copiePourOperateur:String = "YES";


		public function MailVO()
		{
		}

	}
}