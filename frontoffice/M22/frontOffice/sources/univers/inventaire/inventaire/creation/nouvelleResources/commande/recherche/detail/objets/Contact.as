package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class Contact extends EventDispatcher
	{
		public static const CREATION_COMPLETE : String = "contactOk";
		public static const UPDATE_COMPLETE : String = "contactUpdated";
		public static const SAVE_COMPLETE : String = "contactSaved";
		public static const LISTE_COMPLETE : String = "contactListOk";		
		public static const LISTE_SOCIETE_COMPELETE : String = "contactSocieteListOk";		
		public static const DELETE_COMPLETE : String = "contactDeleted";
							
		public static const CREATION_ERROR : String = "contactCreationError";
		public static const UPDATE_ERROR : String ="contactUpdateError";  
		public static const SAVE_ERROR : String = "contactSaveError";
		public static const LISTE_ERROR : String = "contactListError";
		public static const DELETE_ERROR : String = "contactDeleteError";
		public static const LISTE_SOCIETE_ERROR: String = "contactSocieteListError";		
		
		public static const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Contact";
  		public static const COLDFUSION_GET : String = "get";
  		public static const COLDFUSION_UPDATE : String = "update";
  		public static const COLDFUSION_CREATE : String = "create";
		public static const COLDFUSION_GETLISTE : String = "getList";
		public static const COLDFUSION_GETSOCIETELISTE : String = "getSocieteList";
		public static const COLDFUSION_DELETE : String = "delete";
		
		private var opDelete : AbstractOperation;
		private var opGet : AbstractOperation;
		private var opCreate : AbstractOperation;
		private var opUpdate : AbstractOperation;		
		private var opList : AbstractOperation;

		public function Contact(contact_ID : Number = 0){
			
			if (contact_ID != 0){
				contactID = contact_ID;	
				getContact(contactID);		
			}		
		}
		
		public function clean():void{						
			 
		}
		
		
		private var _idGroupe : int;		
		public function get idGroupe():int{
			return _idGroupe;
		}
		public function set idGroupe(id : int):void{
			_idGroupe = id;
		}
		
		private var _contactID : int;
		public function get contactID():int{
			return	_contactID;
		}
		public function set contactID(id : int):void{
			_contactID = id;
		}
		
		
		
		private var _civilite : String;
		public function set civilite( c : String):void{
			_civilite = c;
		}
		public function get civilite():String{
			return _civilite; 
		}
		
		private var _nom : String;
		public function set nom( n : String):void{
			_nom = n;
		}
		public function get nom():String{
			return _nom; 
		}
		
		private var _prenom : String;
		public function set prenom( p : String):void{
			_prenom = p;
		}
		public function get prenom():String{
			return _prenom; 
		}
		
		private var _email : String;
		public function set email( e : String):void{
			_email = e;
		}
		public function get email():String{
			return _email; 
		}
		
		private var _telephone : String;
		public function set telephone( t : String):void{
			_telephone = t;
		}
		public function get telephone():String{
			return _telephone; 
		}
		
		private var _fax : String;
		public function set fax( f : String ):void{
			_fax = f;
		}
		
		public function get fax():String{
			return _fax;
		}
		
		private var _societeID : int;
		public function set societeID( id : int):void{
			_societeID = id;
		}
		public function get societeID():int{
			return _societeID; 
		}
		
		private var _societeNom : String;
		public function set societeNom( n : String):void{
			_societeNom = n;
		}
		public function get societeNom():String{
			return _societeNom; 
		}
		
	//---------------------------------	
		private function fill(value : Object):void{
			civilite = value.CIVILITE;			
			email = value.EMAIL;
			fax = value.FAX;
			nom = value.NOM;
			prenom = value.PRENOM;
			societeID = value.IDCDE_CONTACT_SOCIETE;
			telephone = value.TELEPHONE;		
			societeNom = value.RAISON_SOCIALE;
			contactID = value.IDCDE_CONTACT;
			idGroupe = value.IDGROUPE_CLIENT;
		}
		
	//----------- REMOTING ------------
	
		private function getContact(id : int):void{
			opGet = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Contact.COLDFUSION_COMPONENT,
													Contact.COLDFUSION_GET,
													getContactResultHandler,null);

			RemoteObjectUtil.callService(opGet,id);

		}
		
		private function getContactResultHandler(re : ResultEvent):void{
			if (re.result){							
				fill(re.result[0]);				
				dispatchEvent(new Event(Contact.CREATION_COMPLETE));				
			}else{
				dispatchEvent(new Event(Contact.CREATION_ERROR));
			}			
		}
		//--------
		
		public function deleteContact(id : int):void{
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Contact.COLDFUSION_COMPONENT,
													Contact.COLDFUSION_DELETE,
													deleteContactResultHandler,null);

			RemoteObjectUtil.callService(opDelete,id);

		}
		
		private function deleteContactResultHandler(re : ResultEvent):void{
			if (re.result > 0){							
				dispatchEvent(new Event(Contact.DELETE_COMPLETE));				
			}else{
				dispatchEvent(new Event(Contact.DELETE_ERROR));
			}			
		}
		
		
		//--------			
		public function updateContact():void{
			opUpdate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Contact.COLDFUSION_COMPONENT,
													Contact.COLDFUSION_UPDATE,
													updateContactResultHandler,null);
			var fake : int = 0;
			RemoteObjectUtil.callService(opUpdate,fake,this);
						
		}	
		
		private function updateContactResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				dispatchEvent(new Event(Contact.UPDATE_COMPLETE));
			}else{
				dispatchEvent(new Event(Contact.UPDATE_ERROR));
			}
		}
		
		//----------
		public function sauvegarder(idGpe : int):void{			
			opCreate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Contact.COLDFUSION_COMPONENT,
													Contact.COLDFUSION_CREATE,
													sauvegarderResultHandler,null);
													
			RemoteObjectUtil.callService(opCreate,idGpe,this);
					 											
		}		
		
		private function sauvegarderResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				contactID = int(re.result);				
				dispatchEvent(new Event(Contact.SAVE_COMPLETE));
			}else{
				dispatchEvent(new Event(Contact.SAVE_ERROR));
			}			
		}
		
		//---------LISTE---------------------------------------------------------------------------
		
		private var _liste : ArrayCollection;
		private function setliste(values : ArrayCollection):void{
			var ligne : Object;
			_liste = new ArrayCollection();
			for (ligne in values){
				var ct : Contact = new Contact();
				ct.contactID = values[ligne].IDCDE_CONTACT; 
				ct.civilite = values[ligne].CIVILITE;			
				ct.email = values[ligne].EMAIL;
				ct.fax = values[ligne].FAX;
				ct.nom = values[ligne].NOM;
				ct.prenom = values[ligne].PRENOM;
				ct.societeID = values[ligne].IDCDE_CONTACT_SOCIETE;
				ct.telephone = values[ligne].TELEPHONE;		
				ct.societeNom = values[ligne].RAISON_SOCIALE;
				ct.idGroupe = values[ligne].IDGROUPE_CLIENT;
				_liste.addItem(ct);
			}
			
		}
		
		public function get liste():ArrayCollection{
			return _liste;
		} 
		
		public function prepareList(idGpe : int):void{
			 
			
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Contact.COLDFUSION_COMPONENT,
													Contact.COLDFUSION_GETLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		private function getListResultHandler(re : ResultEvent):void{
			try{
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(Contact.LISTE_COMPLETE));
			}catch(e : Error){
				dispatchEvent(new Event(Contact.LISTE_ERROR));
			}			
		}
		//---------	
		
		public function  prepareSocieteList(societe_ID : int):void{			
			 	
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Contact.COLDFUSION_COMPONENT,
													Contact.COLDFUSION_GETSOCIETELISTE,
													getSocieteListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,societe_ID);
		}
		
		
		private function getSocieteListResultHandler(re : ResultEvent):void{
			try{
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(Contact.LISTE_SOCIETE_COMPELETE));
			}catch(e : Error){
				dispatchEvent(new Event(Contact.LISTE_SOCIETE_ERROR));
			}			
		}
	}
}