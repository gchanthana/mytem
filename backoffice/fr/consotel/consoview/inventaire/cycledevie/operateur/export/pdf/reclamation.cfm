<table cellpadding="0" cellspacing="0" width="100%" align="left" width="100%" >
	<tr>			
		<td/>
		<td width="100"/>
		<td align="left" class="td51"><b>Contact :</b><blockquote>
			<cfoutput><b>#info.civilite# #info.prenom# #info.nom#</b><br>
			 
			<cfif len(info.telephone[1]) gt 0>
				tél. #info.telephone[1]#<br>										
			</cfif>
			<cfif len(info.fax[1]) gt 0>
				fax  #info.fax[1]#<br>										
			</cfif>
			<cfif len(info.email[1]) gt 0>
				email #info.email[1]#<br>										
			</cfif>
			<cfif len(info.raison_sociale[1]) gt 0>
				<br><b>#info.raison_sociale[1]#</b><br>										
			</cfif>
			<cfif len(info.adresse1[1]) gt 0>
				#info.adresse1[1]#<br>										
			</cfif>
			<cfif len(info.adresse2[1]) gt 0>
				#info.adresse2[1]#<br>										
			</cfif>
			<cfif len(info.zipcode[1]) gt 0>
				#info.zipcode[1]#<br>										
			</cfif>
			<cfif len(info.pays[1]) gt 0>
				#info.pays[1]#<br>										
			</cfif></blockquote></cfoutput>								
			
		</td>	
	</tr>				
	
	<tr>		
		<td align="left" class="td51" valign="top">
			<cfif len(reclamation.LIBELLE_OPERATIONS) gt 0>
				<b>Libellé de la commande : </b>&nbsp;<cfoutput>#reclamation.LIBELLE_OPERATIONS#</cfoutput><br><br>								
			</cfif>
			<cfif len(reclamation.DATE_REF) gt 0>
				<b>Date de référence : </b>&nbsp;<cfoutput>#reclamation.DATE_REF#</cfoutput><br><br>								
			</cfif>
			
		</td>	
		<td width="100"/>
		<td align="left" height="20" valign="top"/>
	</tr>
	<tr>
		<td colspan="3" height="40"/>
	</tr>	
	<tr>
		<td colspan="3"/>
	</tr>			
	<tr>
	<table align="left" width="100%" >
		<tr>	 	
			<td valign="top">
				<table cellspacing="0" align="left" class="cadre" border="0" cellpadding="0" width="100%">
					<tr>
						<td class="td5"><strong>Produit concerné par la réclamation</strong></td>
					</tr>			
					<tr>
						<table cellspacing="0" class="menuconsoview" border="0" align="left" cellpadding="0" width="100%">	
							<tr align="left">
								<cfif reclamation.IDINV_TYPEOP eq 0>
									<td>&nbsp;Dernier jour facturé&nbsp;</td>
								<cfelse>
									<td>&nbsp;Premier jour facturé&nbsp;</td>
								</cfif>
								<td>&nbsp;Jours à réguler&nbsp;</td>
								<td>&nbsp;Ligne&nbsp;</td>
								<td>&nbsp;Opérateur&nbsp;</td>																																																										
								<td>&nbsp;Produits&nbsp;</td>	
								<td>&nbsp;Estimation de la régul (&euro;)&nbsp;</td>																												
							</tr>							
					
							<tr>
								<td class="grid_normal_alt" align="left" valign="top"><cfoutput>&nbsp;#LSDateFormat(listeProduits.DATE_DERNIERE_FACT,'dd/mm/yy')#&nbsp;</cfoutput></td>
								<td class="grid_normal_alt" align="left" valign="top"><cfoutput>&nbsp;#listeProduits.NBJOUR#&nbsp;</cfoutput></td>
								<td class="grid_normal_alt" align="left" valign="top"><cfoutput>&nbsp;#listeProduits.SOUS_TETE#&nbsp;</cfoutput></td>
								<td class="grid_normal_alt" align="left" valign="top"><cfoutput>&nbsp;#listeProduits.OPNOM#&nbsp;</cfoutput></td>
								<td class="grid_normal_alt" align="left" valign="top"><cfoutput>&nbsp;#listeProduits.LIBELLE_PRODUIT#&nbsp;</cfoutput></td>
								<td class="grid_normal_alt" align="left" valign="top"><cfoutput>&nbsp; #LsEuroCurrencyFormat(listeProduits.ESTIMATION,'none')#&nbsp;</cfoutput></td>
							</tr>
						</table>		
					</tr>
				</table>
			</td>		
		</tr>
	</table>
	</tr>	
	<tr>
		<td colspan="3" height="40"/>
	</tr>	
	<tr>
		<td colspan="3" height="40"/>
	</tr>		
</table>

<table align="left" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="left" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong>Liste des éléments de la facturation</strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="left" cellpadding="0" width="100%">
							<tr align="left">																
								<td>&nbsp;Date facture&nbsp;</td>
								<td>&nbsp;Date début&nbsp;</td>
								<td>&nbsp;Date fin&nbsp;</td>
								<td>&nbsp;Jours régulés&nbsp;</td>									
								<td>&nbsp;Ligne&nbsp;</td>
								<td>&nbsp;Opérateur&nbsp;</td>																																																										
								<td>&nbsp;Produits&nbsp;</td>	
								<td>&nbsp;Montant (&euro;)&nbsp;</td>	
							</tr>
<cfset numRow = 1>
<cfoutput query="listeElementFactureRessource">	
	<cfif (numRow mod 28) eq 0>								
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
<table align="left" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="left" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong>Liste des éléments de facturation</strong></td>
				</tr>
				<tr>
					<td valign="top">
	 					<table cellspacing="0" class="menuconsoview" border="0" align="left" cellpadding="0" width="100%">
							<tr align="left">
								<td>&nbsp;Date facture&nbsp;</td>
								<td>&nbsp;Date début&nbsp;</td>
								<td>&nbsp;Date fin&nbsp;</td>
								<td>&nbsp;Jours régulés&nbsp;</td>									
								<td>&nbsp;Ligne&nbsp;</td>
								<td>&nbsp;Opérateur&nbsp;</td>																																																										
								<td>&nbsp;Produits&nbsp;</td>	
								<td>&nbsp;Montant (&euro;)&nbsp;</td>																												
							</tr>							
		<cfset numRow = numRow +1>
	<cfelse>
						<cfif (numRow mod 2) eq 0>
							<tr>
								<td class="grid_normal" align="left" valign="top">&nbsp;#LSDateFormat(listeElementFactureRessource.DATE_EMISSION,'dd/mm/yy')#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#LSDateFormat(listeElementFactureRessource.DATEDEB_REGUL,'dd/mm/yy')#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#LSDateFormat(listeElementFactureRessource.DATEFIN,'dd/mm/yy')#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#listeElementFactureRessource.JOURS_REGUL#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#listeElementFactureRessource.SOUS_TETE#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#listeElementFactureRessource.OPNOM#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#listeElementFactureRessource.LIBELLE_PRODUIT#&nbsp;</td>
								<td class="grid_normal" align="left" valign="top">&nbsp;#LsEuroCurrencyFormat(listeElementFactureRessource.MONTANT, 'none')#&nbsp;</td>
							</tr>
						<cfelse>
							<tr>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#LSDateFormat(listeElementFactureRessource.DATE_EMISSION,'dd/mm/yy')#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#LSDateFormat(listeElementFactureRessource.DATEDEB_REGUL,'dd/mm/yy')#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#LSDateFormat(listeElementFactureRessource.DATEFIN,'dd/mm/yy')#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#listeElementFactureRessource.JOURS_REGUL#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#listeElementFactureRessource.SOUS_TETE#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#listeElementFactureRessource.OPNOM#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#listeElementFactureRessource.LIBELLE_PRODUIT#&nbsp;</td>
								<td class="grid_normal_alt" align="left" valign="top">&nbsp;#LsEuroCurrencyFormat(listeElementFactureRessource.MONTANT, 'none')#&nbsp;</td>
							</tr>
						</cfif>							 												
		<cfset numRow = numRow +1>		
	</cfif>	
</cfoutput>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>