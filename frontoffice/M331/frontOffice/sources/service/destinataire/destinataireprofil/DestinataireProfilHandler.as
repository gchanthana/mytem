package service.destinataire.destinataireprofil
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	

	internal class DestinataireProfilHandler
	{
		
		private var _model:DestinataireProfilModel;
		
		public function DestinataireProfilHandler(_model:DestinataireProfilModel):void
		{
			this._model = _model;
		}
		
		internal function getAssocierResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.getResultHandler(evt);
			}
		}	
		internal function getRemoveResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.getRemoveResultHandler(evt);
			}
		}
		
		internal function getProfilsResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.getProfilsResult(evt.result as ArrayCollection)
			}
		}
	}
}