	
	<?xml version="1.0" encoding="utf-8" ?><!DOCTYPE html PUBLIC
		"-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
		"http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
		<title>
			TELLCOST
		</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="robots" content="all" />
		<meta name="classification" content="Services" />
		<meta name="copyright" content="ConsoTel SAS" />
		<meta name="author" content="Guillaume RAMOS" />
		<meta name="language" content="fr" />
		<meta name="description" content="TELLCOST" />
		<meta name="keywords" content="TELLCOST" />
	
		<STYLE TYPE="TEXT/CSS">
			#MONCADRE 		{
								WIDTH: 1050PX;
								HEIGHT:100%;
						 		PADDING:4PX; 
								MARGIN-LEFT:10PX;
								MARGIN-RIGHT:10PX;
								MARGIN-TOP:10PX;
								MARGIN-BOTTOM:10PX; 
							}
							
			.CADREDUMENUGAUCHE {
								FLOAT:LEFT;
								TEXT-ALIGN:LEFT;
								WIDTH:80%;
								HEIGHT:100%;
							}
								
			.CADREDUMENUDROITE {
								FLOAT:RIGHT;
								TEXT-ALIGN:LEFT;
								WIDTH:20%;
								HEIGHT:100%;
							}
							
			#CADREHAUT 		{
								WIDTH:90%;
								HEIGHT:100PX;
							}

			#CADRECENTRALE 	{ 
								MARGIN-LEFT:100PX;
								MARGIN-RIGHT:100PX;
								MIN-HEIGHT:5PX;
								
								BORDER-LEFT:1PX DASHED #FFFFFF; 
								BORDER-RIGHT:1PX DASHED #FFFFFF;
								BORDER-BOTTOM:1PX DASHED #FFFFFF;
								BORDER-TOP:1PX DASHED #FFFFFF;
							}
							
			#CADRECENTRALE2 { 
								BORDER-LEFT:1PX SOLID #000000; 
								BORDER-RIGHT:1PX SOLID #000000;
								BORDER-BOTTOM:1PX SOLID #000000;
								BORDER-TOP:1PX SOLID #000000;
								PADDING:10PX;
								WIDTH: 650PX;
								-MOZ-BORDER-RADIUS:10PX;
								-WEBKIT-BORDER-RADIUS:10PX;
								-KHTML-BORDER-RADIUS:10PX;
					  			BORDER-RADIUS:10PX;
							}
							
			#MYFIELDSET 	{ 
								-MOZ-BORDER-RADIUS:8PX;
								-WEBKIT-BORDER-RADIUS:8PX;
								-KHTML-BORDER-RADIUS:8PX;
					  			BORDER-RADIUS:8PX;
							}

		 	.submit 		{
								width: 200px;
		 					}
		</style>
	
		<script language="Javascript">
			if (document.images) 
			{
				button1 = new Image
				button2 = new Image
			
				button1.src = "tellCostImg/Connexion_n.png"
				button2.src = "tellCostImg/Connexion_s.png"
			}
		</script>
	</head>

<!--- Variables --->

	<cfset objectValidate = structNew()>
	<cfset ValidateLog = -1>
	<cfset MessageLogError = "">
	<cfset obj = createobject('component','TellCostService')>	
	
<!--- Fin variables --->

<!--- Session --->
	
	<cfset SESSION.IS_CONNECTED = 0>
	<cfset SESSION.SESSION_ID = -1>
	<cfset SESSION.PHONENUMBER = "">
	<cfset SESSION.IDSOUS_TETE = "">
	<cfset SESSION.IDRACINE_MASTER = "">
	<cfset SESSION.IDPERIODE = "">
	<cfset SESSION.IDTHEME = "">
	<cfset SESSION.LIBELLE_MOIS = "">
	<cfset SESSION.PATH_CONNECT = 'details.cfm?'&'#SESSION.URLTOKEN#'>
	
<!--- Fin session --->

<!--- Function --->

	<cffunction name="zCheckValidateLog" access="private" returntype="numeric">
	
			<cfset objectValidate = obj.zQueryCheckLog(FORM._Login,FORM._Password,SESSION)>
			<cfset ValidateLog = objectValidate.RETOUR>
			<cfset rslt = -1>
			
			<cfif ValidateLog GT 0>
				<cfset MessageLogError = "">
				<cfset SESSION = objectValidate>
				<cfset SESSION.IS_CONNECTED = 1>
				<cfset SESSION.SESSION_ID = ValidateLog>
				<cfset SESSION.PHONENUMBER = FORM._Login>
				<cfset rslt = 1>
			<cfelseif ValidateLog EQ -4>
				<cfset MessageLogError = "Numéro et mot de passe invalide">
				<cfset SESSION.IS_CONNECTED = 0>
				<cfset SESSION.SESSION_ID = -1>
				<cfset rslt = -1>
			<cfelseif ValidateLog EQ 0>
				<cfset MessageLogError = "Numéro et mot de passe invalide">
				<cfset SESSION.IS_CONNECTED = 0>
				<cfset SESSION.SESSION_ID = -1>
				<cfset rslt = -1>
			</cfif> 

 		<cfreturn rslt> 
	</cffunction> 

<!--- Fin function --->

<!--- Tests --->

	<cfif structKeyExists(FORM,'CONNEXION')>
		<cfif (FORM._Login NEQ "" AND FORM._Password NEQ "")>
			<cfif zCheckValidateLog() EQ 1>
				<cflocation url="#SESSION.PATH_CONNECT#">
			<cfelse>
			</cfif>
		<cfelse>
			<cfset MessageLogError = "Numéro ou mot de passe invalide">	
		</cfif>
	</cfif>			

<!--- Fin tests --->

	<FONT face="Arial" size="2">	
		<div align="center">
			<DIV id="CADREHAUT">
				<img src="images/header_consomobilescf.jpg" alt="header"/> 
			</DIV> 

			<DIV ID="MONCADRE">
				<DIV ID="CADRECENTRALE">
				     <FORM name="testForm" method="post">
					  	<FIELDSET align="center" id="MYFIELDSET">
						    <LEGEND>
							    <img src="tellCostImg/User 7 Security.png" width="64" height="64" alt="tellcost">
							</LEGEND>
							    <cfoutput>Veuillez, vous authentifier</cfoutput>
							    <br/><br/>
					    		<DIV ID="CADRECENTRALE2">
									<TABLE WIDTH="600" STYLE="BORDER:0PX SOLID #000000;MARGIN:AUTO;BORDER-COLLAPSE: COLLAPSE;" ID="TABLEID"> 
										<TR> 
											<TH COLSPAN=2>
												<img src="tellCostImg/tellCostTestMiniMini.png" border="0"  weight="0"  align="left"/>
											</TH> 
										</TR>
										<TR> 
											<TD align="right">
												<form name="testForm2" >
													<br/>
													<cfoutput>N° de ligne mobile :</cfoutput>
													<input type="texte" name="_Login" class="submit" style="WIDTH:100"/>
													<br/><br/>
													<cfoutput>Mot de passe :</cfoutput>
											        <input type="password" name="_Password" class="submit" style="WIDTH:100"/>
													<br/><br/>
													<div align="center" style="MARGIN-LEFT:20PX;">
														<input type="submit" name="CONNEXION" value="Connexion" onclick="va();" class="sub"/>
													</div>
												</form>
											</TD>      
											<TD width="250">
												<FONT COLOR="#FF0000">
													<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;#MessageLogError#</cfoutput> 
												</FONT>
											</TD> 
										</TR>
										<TR> 
											<TD align="left">
												<br/>
												<cfoutput>Accès réservé aux abonnés.</cfoutput> 
												<br/>
												<cfoutput>Toutes les tentatives de connexion sont enregistrées.</cfoutput> 
											</TD> 
											<TH>
												<br/>
												<img src="tellCostImg/tellCostTestPowered.png" alt="header" height="35" align="right"/>
											</TH> 
										</TR>
									</TABLE>
								</DIV>
								<br/>
						</FIELDSET>
					</FORM>
					<br/><br/>
					<FORM>
					  	<FIELDSET align="center" id="MYFIELDSET">
						    <LEGEND>CNIL</LEGEND>
							<div align="center">
								Tout utilisateur est informé que l'accès de ce logiciel doit se faire dans le respect des 
								dispositions prévues par la CNIL en matière de protection de données personnelles et nominatives. Consotel 
								ne pourra pas être tenu responsable d'une utilisation non conforme. Les informations vous concernant ont été 
								recueillies auprès de votre opérateur télécom. Elles font l'objet d'un traitement informatique de gestion 
								de la téléphonie effectué pour le compte de votre entreprise. Seules sont diffusées des informations agrégées. La 
								campagne d’information Tellcost est conforme aux recommandations de la CNIL. La loi Informatique et libertés du 
								6 janvier 1978 vous accorde un droit d'accès et de rectification aux informations qui vous concernent.
								<a href="http://www.cnil.fr"> http://www.cnil.fr </a> 
							</div>
						</FIELDSET>
					</FORM>
				</DIV>
			</DIV>	

			<div align="center"> 
					<p><a href="mailto:bal-telephonie-mobile@creditfoncier.fr ?subject=Question tellcost" accesskey="3">bal-telephonie-mobile@creditfoncier.fr</a></p>
				</div>
		</div>
	</font>
</html>