package listedesfactures.services.affichagefacture
{
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import listedesfactures.entity.FactureDisplayParamsVo;
	
	/**
	 * <b>Classe <code>DisplayFactureHTMLService</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 01.09.2010
	 * </pre></p> 
	 * 
	 */
	public class DisplayFactureHTMLService implements IDisplayFacture
	{
		private var _params:FactureDisplayParamsVo;
		
		/**
		 * <b>Constructeur <code>DisplayFactureHTMLService()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe DisplayFactureHTMLService.
		 * </pre></p>
		 * 
		 * @param params : <code>FactureDisplayParamsVo</code>
		 * 
		 */	
		public function DisplayFactureHTMLService(params:FactureDisplayParamsVo)
		{
			_params = params;
		}
		
		/**
		 * 
		 *<b>Fonction <code>displayFacture()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Ouvre une fenêtre pour afficher la facture
		 * </pre></p> 
		 * 
		 */	
		public function displayFacture():void
		{
			
			//TODO: implement function
			var url:String=moduleListeDesFacturesIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/gabaritFacture.cfm";
			
			var variables:URLVariables=new URLVariables();
			
			variables.IDX_PERIMETRE=_params.perimetre_id;
			variables.type_perimetre=_params.perimetre_type;
			variables.PERIMETRE_LIBELLE=_params.perimetre_libelle;
			
			variables.ID_FACTURE=_params.facture.IDINVENTAIRE_PERIODE;
			variables.DATE_EMISSION=(_params.facture.DATE_EMISSION as Date).toLocaleDateString();
			variables.DATE_EMISSION_STR=_params.facture.STR_DATE_EMISSION;
			variables.CODE_LANGUE = CvAccessManager.getSession().USER.GLOBALIZATION
			variables.DATEDEB=(_params.facture.DATEDEB as Date).toLocaleDateString();
			variables.DATEFIN=(_params.facture.DATEFIN as Date).toLocaleDateString();
			variables.OPNOM=_params.facture.NOM;
			variables.NUMERO_FACTURE=_params.facture.NUMERO_FACTURE;
			variables.IDREF_CLIENT=_params.facture.IDREF_CLIENT;
			variables.LIBELLE=_params.facture.LIBELLE;
			
			
			var request:URLRequest=new URLRequest(url);
			request.data=variables;
			request.method="POST";
			navigateToURL(request, "_blank");
		}
	}
}