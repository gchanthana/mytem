package intranet.M25.ihm
{
    import intranet.M25.entity.ModelLocator;
    import mx.containers.TitleWindow;
    import mx.events.FlexEvent;

	/**
	 * Classe pilotant le composant d'<b>entree</b> de la gestion des collectes
	 * 2 panels :
	 *  Gestion des templates : Gérer les Fiches de collectes opérateurs /
	 *  Gestion des collectes : Gérer les Collectes clientes 
	 * 
	 * 
	 */	
    public class GestionCollecteImpl extends TitleWindow
    {
        public function GestionCollecteImpl()
        {
            super();
        }

        /**
         * Initialisation générale de cette classe posant les écouteurs captant
         * la fin de l'initialisation de tous les composants enfants.
         */
        public function init():void
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
        }

        /**
         * Initialisation générale de cette classe.
         */
        public function onCreationCompleteHandler(event:FlexEvent):void
        {
        }
    }
}