﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
       
       <cffunction name="executeAct" access="remote" returntype="Struct" output="false" hint="retourne la liste des partenaires">
             <cfargument name="data" type="Struct" required="true">
             
             <cftry>
                    <cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_partenaire.List_Partenaire">
                           <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#data.idUser#">
                           <cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" value="#getCodeLangue()#">
                           <cfprocresult name="p_retour">
                    </cfstoredproc>
                    
                    <cfif isQuery(p_retour)>
                           <cfset data = structNew()>
                           <cfset data.LISTEPARTENAIRES = p_retour>
                           <cfreturn CreateReponse("AL014",1,data)/>
                    <cfelseif isnumeric(p_retour)>
                           <cfset err = ThrowCfCatchError(p_retour,"","Erreur de procédure","","")>
                           <cfreturn CreateReponse("AL014", 0, err)/>
                    </cfif>
                    
                    <cfcatch type="any" >
                           <cfreturn CreateReponse("AL014", 0, ThrowCfCatchError("AL014",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
             </cfcatch>
             </cftry>
             
       </cffunction>

</cfcomponent>