<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.ContainerHandler" extends="fr.consotel.api.ibis.publisher.delivery.FTP"
hint="Implémentation abstraite qui sert de classe parent à la stratégie Container pour contenir l'implémentation des méthodes de traitement container
Ces traitements correspondent aux appels aux procédures stockées container et sont délégués à un autre composant (CONTAINER).
Le type de l'implémentation utilisée pour l'UUID et les procédures stockées container est retourné par la méthode getContainerClass()
Le type du composant CONTAINER doit dériver de : fr.consotel.api.ibis.ext.ExtensionApiContainerV1
Les méthodes getContainerClass(), getProcessId(), getCodeApp() doivent etre redéfinies par les classes dérivées
Les propriétés de diffusion FTP doivent etre spécifiées par les classes dérivées (Nom du serveur, Répertoire, Utilisateur, etc...)
Les propriétés suivantes doivent etre retournée par la méthode getDeliveryProperties() de la stratégie utilisée pour l'exécution du reporting :
- FICHIER (Ticket 5698) : Libellé visible dans le module container
- FILE_EXT : Extension du fichier à générer (sans le séparateur point)
- USERID : Identifiant de l'utilisateur ConsoView
- IDRACINE : Identifiant du groupe utilisé pour les notifications container
Les propriétés suivantes sont facultatives :
- SHORT_NAME (Ticket 5698) : Nom du fichier téléchargeable depuis le module container (Par défaut : Valeur de la propriété FICHIER)
- CODE_APP : Code App utilisé pour les notifications container (Par défaut : 1)
Les spécifications suivantes doivent etre implémentées lorsque cette implémentation est utilisée comme classe parent :
- Les propriétés suivantes doivent etre retournée par getDeliveryProperties() : USERID, IDRACINE, CODE_APP, FICHIER, FILE_EXT
Les propriétés suivantes sont ignorées : FILE_NAME
- Le nom du fichier FTP généré doit etre égal à la valeur retournée par la méthode GETUuid() de ExtensionApiContainerV1 (Sans inclure son extension)">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="getDeliveryProperties" returntype="Struct" hint="Effectue dans l'ordre les opérations suivantes :
	Récupère la structure retournée par la méthode de la classe parent et ajoute les propriétés suivantes avant de la retourner :
	- FILE_NAME : Valeur retournée par CONTAINER.GETUuid()
	- CODE_APP : La valeur fournie par le reporting. Par défaut la valeur retournée par getCodeApp() si elle n'est pas fournie
	- PROCESS_ID : La valeur retournée par getProcessId()
	Une exception est levée si la propriété CONTAINER n'est pas présente dans la structure retournée par reporting.getProperties()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var reportingProperties=reportingImpl.getProperties()>
		<!--- Si la propriété CONTAINER n'est pas présente dans reporting.getProperties() : Une exception est levée --->
		<cfif NOT structKeyExists(reportingProperties,"CONTAINER")>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La propriété CONTAINER qui est créée par la stratégie Container est introuvable"
				detail="La propriété CONTAINER est créée par la stratégie Container et ne doit etre supprimée que par cette stratégie">
		<cfelse>
			<cfset var apiContainer=reportingProperties["CONTAINER"]>
			<cfset var deliveryProperties=SUPER.getDeliveryProperties(reportingImpl)>
			<!--- Remplace la valeur de la propriété FILE_NAME par la valeur retournée par CONTAINER.GETUuid() --->
			<cfset deliveryProperties["FILE_NAME"]=apiContainer.GETUuid()>
			<!--- Défini la propriété CODE_APP si elle n'est pas renseignée ou n'est pas fournie --->
			<cfif NOT structKeyExists(deliveryProperties,"CODE_APP")>
				<cfset deliveryProperties["CODE_APP"]=getCodeApp()>
				<cfset deliveryProperties["PROCESS_ID"]=getProcessId()>
			</cfif>
			<cfreturn deliveryProperties>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getDeliveryRequest" returntype="Struct" hint="Effectue dans l'ordre les opérations suivantes :
	- Insère une propriété CONTAINER dans reporting.getProperties()
	- Effectue les traitements container requis avant l'exécution du reporting avec CONTAINER et les propriétés getDeliveryProperties()
	- Stocke les propriétés et paramètres du reporting qui sont liés au module container en utilisant storeContainerProperties()
	- Récupère la structure retournée par la méthode de la classe parent avec le paramètre reporting
	- Remplace le nom du fichier FTP contenu dans cette structure par la valeur retournée par ExtensionApiContainerV1.GETUuid()
	- Remplace le JOBNAME par la concaténation Container : UUID avec UUID la valeur retournée par ExtensionApiContainerV1.GETUuid()
	- Retourne la structure ainsi modifiée
	La valeur associée à CONTAINER est créée à l'appel de cette méthode et sont type est retourné par la méthode getContainerClass()
	L'implémentation de ce type qui est utilisée par cette classe est retournée par getContainerClass()
	Une exception est levée si une propriété CONTAINER est présente dans la structure retournée par reporting.getProperties()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var reportingProperties=reportingImpl.getProperties()>
		<!--- Si la propriétés CONTAINER est présente dans reporting.getProperties() : Une exception est levée --->
		<cfif structKeyExists(reportingProperties,"CONTAINER")>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La propriété CONTAINER est réservée à la stratégie Container et ne doit pas etre utilisée"
				detail="La propriété CONTAINER est créée et supprimée par la stratégie Container et ne doit pas etre utilisée">
		<cfelse>
			<!--- Instancie le composant dont le type est retourné par getContainerClass() : Génère un UUID unique --->
			<cfset var apiContainer=createObject("component",getContainerClass())>
			<cfset reportingProperties["CONTAINER"]=apiContainer>
			<cfset var deliveryProperties=getDeliveryProperties(reportingImpl)>
			<!--- MAJ de la structure ScheduleRequest : Remplace le JOBNAME et le nom du fichier FTP --->
			<cfset var scheduleRequest=SUPER.getDeliveryRequest(reportingImpl)>
			<cfset scheduleRequest.userJobName="Container : " & apiContainer.GETUuid()>
			<cfset scheduleRequest.deliveryRequest.ftpOption.remoteFile=apiContainer.GETUuid()>
			<!--- Appel des procédures stockées container requises avant exécution du reporting --->
			<cfset containerAvantExecution(apiContainer,deliveryProperties)>
			<!--- Stocke les propriétés et paramètres liés au module container en utilisant storeContainerProperties() --->
			<cfset storeContainerProperties(reportingImpl,apiContainer)>
			<cfset logInfo("CONTAINER : UUID [#apiContainer.GETUuid()#]")>
			<cfreturn scheduleRequest>
		</cfif>
	</cffunction>

	<cffunction access="public" name="handleJobId" returntype="void" hint="Effectue dans l'ordre les opérations suivantes :
	- Récupère localement la valeur associée à la propriété CONTAINER puis la supprime de reporting.getProperties()
	- Effectue les traitements container requis après l'exécution du reporting avec CONTAINER, jobId et les propriétés getDeliveryProperties()
	- Appelle la méthode handleJobId() de la classe parent
	Une exception est levée si la propriété CONTAINER n'est pas présente dans la structure retournée par reporting.getProperties()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true">
		<cfargument name="jobId" type="Numeric" required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var reportingProperties=reportingImpl.getProperties()>
		<!--- Si la propriété CONTAINER n'est pas présente dans reporting.getProperties() : Une exception est levée --->
		<cfif NOT structKeyExists(reportingProperties,"CONTAINER")>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La propriété CONTAINER qui est créée par la stratégie Container est introuvable"
				detail="La propriété CONTAINER est créée par la stratégie Container et ne doit etre supprimée que par cette stratégie">
		<cfelse>
			<cfset var apiContainer=reportingProperties["CONTAINER"]>
			<!--- Appel des procédures stockées container requises après exécution du reporting et supprime la propriété CONTAINER --->
			<cfset var deliveryProperties=getDeliveryProperties(reportingImpl)>
			<cfset containerApresExecution(apiContainer,deliveryProperties,ARGUMENTS["jobId"])>
			<cfset structDelete(reportingProperties,"CONTAINER")>
			<cfset logInfo("CONTAINER : JOBID Updated [#apiContainer.GETUuid()#,#ARGUMENTS.jobId#]")>
			<cfset SUPER.handleJobId(reportingImpl,ARGUMENTS["jobId"])>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="handleReportingEvent" returntype="void" hint="Effectue dans l'ordre les opérations suivantes :
	- Crée un évènement de type fr.consotel.api.ibis.publisher.handler.event.ContainerEvent à partir de event
	- Instancie une nouvelle instance dont le type est retourné getContainerClass() pour appeler les procédures stockées container
	- Effectue les traitements container de fin d'exécution du reporting en utilisant l'évènement créé et la nouvelle instance ExtensionApiContainerV1
	- Dispatche l'évènement de type ContainerEvent à partir de celui qui est passé en paramètre">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent" required="true">
		<cfset var reportingEvent=ARGUMENTS["event"]>
		<!--- Effectue le traitement container de mise à jour du statut du JOB --->
		<cfset var apiContainer=createObject("component",getContainerClass())>
		<cfset var containerEvent=getDeliveryEvent(reportingEvent)>
		<cfset containerFinExecution(apiContainer,containerEvent)>
		<cfset logInfo("CONTAINER : Handling Success " &
			"[#containerEvent.getContainerStatus()#,#containerEvent.getFileUUID()#,#containerEvent..getJobId()#]")>
		<!--- Dispatch de l'évènement de type ContainerEvent --->
		<cfset dispatchDeliveryEvent(containerEvent)>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.delivery.ContainerHandler --->
	<cffunction access="public" name="getContainerClass" returntype="String"
	hint="Retourne le nom complet du composant utilisé pour appeler les procédures stockées container. N'est pas implémentée : Retourne une chaine vide">
		<cfreturn "">
	</cffunction>
	
	<cffunction access="public" name="getCodeApp" returntype="Numeric"
	hint="Retourne la valeur du CodeApp utilisé par défaut dans les traitements container. N'est pas implémentée : Retourne 0">
		<cfreturn 0>
	</cffunction>

	<cffunction access="public" name="getProcessId" returntype="Numeric"
	hint="Retourne la valeur du PROCESS_ID utilisé dans les traitements container. N'est pas implémentée : Retourne 0">
		<cfreturn 0>
	</cffunction>
	
	<cffunction access="private" name="storeContainerProperties" returntype="void"
	hint="Appelée par getDeliveryRequest() pour stocker les propriétés du reporting qui sont liées au module container. Cette méthode n'est pas implémentée">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting" required="true" hint="Provient de getDeliveryRequest()">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true" hint="Provient de getDeliveryRequest()">
	</cffunction>

	<cffunction access="private" name="containerAvantExecution" returntype="void"
	hint="Effectue les traitements container requis avant l'exécution du reporting. Cette méthode est appelée par getDeliveryRequest()
	Appelle la méthode CONTAINER.ADDDocumentTableContainer() qui correspond à la procédure : pkg_M61.CONTAINERPUBLISHRAPPORT_V4
	Une exception est levée si la valeur retournée par CONTAINER.ADDDocumentTableContainer() est différente CONTAINER.GETUuid()
	La méthode containerAvantExecutionError() est appelée avec les memes paramètres avant la levée de cette exception">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true" hint="Provient de getDeliveryRequest()">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Provient de getDeliveryRequest()">
		<cfset var apiContainer=ARGUMENTS["container"]>
		<cfset var deliveryProperties=ARGUMENTS["deliveryProps"]>
		<cfset var UUID=apiContainer.GETUuid()>
		<cfset var DATE_CONTAINER=lsDateFormat(NOW(),"yyyy/mm/dd")>
		<!--- SHORT_NAME pour le module container (Ticket 5698) --->
		<cfset var shortName=deliveryProperties["FICHIER"]>
		<cfif structKeyExists(deliveryProperties,"SHORT_NAME")>
			<cfset var shortName=deliveryProperties["SHORT_NAME"]>
		</cfif>
		<!--- Méthode 1 de container : ADDDocumentTableContainer() --->
		<cfset var addDocStatus=apiContainer.ADDDocumentTableContainer(
			deliveryProperties["FICHIER"], REPLACE(shortName," ","_","all"), -1, "C", deliveryProperties["USERID"],
			deliveryProperties["IDRACINE"], "M", deliveryProperties["FILE_EXT"], DATE_CONTAINER,
			UUID, "A", 0, 1, -1, -1, deliveryProperties["CODE_APP"]
		)>
		<!--- Si la valeur retournée n'est pas égale à UUID ou vaut -1 : Une notification mail est envoyée et une exception levée --->
		<cfif (addDocStatus NEQ UUID) OR (addDocStatus EQ -1)>
			<cfset containerAvantExecutionError(apiContainer,deliveryProperties)>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La valeur retournée par CONTAINER.ADDDocumentTableContainer() est invalide"
				detail="UUID : #UUID#. Valeur retournée : #addDocStatus#">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="containerApresExecution" returntype="void"
	hint="Effectue les traitements container requis avant l'exécution du reporting. Cette méthode est appelée par handleJobId()
	Appelle la méthode CONTAINER.ADDJobIdToDocument() qui correspond à la procédure : pkg_M61.ADDJobID
	Appelle la méthode CONTAINER.setCodeProcessForJOBID() qui correspond à la procédure : pkg_m61.addJobDetail_V2
	Une exception est levée si la valeur retournée par une des méthode ci-dessus est invalide
	La méthode containerApresExecutionError() est appelée avec les memes paramètres avant la levée de cette exception">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true" hint="Provient de handleJobid()">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Provient de handleJobId()">
		<cfargument name="jobId" type="Numeric"  required="true" hint="Provient de handleJobId()">
		<cfset var apiContainer=ARGUMENTS["container"]>
		<cfset var deliveryProperties=ARGUMENTS["deliveryProps"]>
		<cfset var JOB_ID=ARGUMENTS["jobId"]>
		<cfset var UUID=apiContainer.GETUuid()>
		<!--- Méthode 2 de container : ADDJobIdToDocument() --->
		<cfset var addJobIdStatus=apiContainer.ADDJobIdToDocument(UUID,JOB_ID)>
		<!--- Si la valeur retournée par la procédure stockée est invalide --->
		<cfif addJobIdStatus NEQ 1>
			<cfset containerApresExecutionError(apiContainer,deliveryProperties,JOB_ID)>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La valeur retournée par CONTAINER.ADDJobIdToDocument() est invalide"
				detail="UUID : #UUID#, JOBID : #JOB_ID#. Valeur retournée : #addJobIdStatus#">
		</cfif>
		<!--- Méthode 3 de container : setCodeProcessForJOBID() --->
		<cfset var setCodeStatus=apiContainer.setCodeProcessForJOBID(UUID,JOB_ID,getProcessId())>
		<!--- Si la valeur retournée par la procédure stockée est invalide --->
		<cfif setCodeStatus NEQ ARGUMENTS.jobId>
			<cfset containerApresExecutionError(apiContainer,deliveryProperties,JOB_ID)>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La valeur retournée par CONTAINER.setCodeProcessForJOBID() est invalide"
				detail="UUID : #UUID#, JOBID : #JOB_ID#. Valeur retournée : #setCodeStatus#">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="containerFinExecution" returntype="void"
	hint="Effectue les traitements container requis à la fin de l'exécution du reporting. Cette méthode est appelée par handleReportingEvent()
	Appelle la méthode container.getCodeProcess() qui correspond à la procédure : pkg_m61.getCodeProcess_V2
	Instancie un process container fr.consotel.process.processXXX avec XXX la valeur retournée par container.getCodeProcess()
	Appelle la méthode run() du process container qui a été créé (PROCESS)  
	Une exception est levée si une des conditions suivante est vérifiée :
	- La valeur retournée par container.getCodeProcess() est invalide
	- La valeur retournée par PROCESS.run() est différente de 1
	La méthode containerFinExecutionError() est appelée avec les memes paramètres avant la levée de cette exception">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true" hint="Provient de handleReportingEvent()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.ContainerEvent" required="true" hint="Provient de handleReportingEvent()">
		<cfset var apiContainer=ARGUMENTS["container"]>
		<cfset var containerEvent=ARGUMENTS["event"]>
		<cfset var UUID=containerEvent.getFileUUID()>
		<!--- Méthode 4 de container : getCodeProcess() --->
		<cfset var processName=apiContainer.getCodeProcess(containerEvent.getJobId(),UUID)>
		<!--- Si la valeur retournée par la procédure stockée est invalide --->
		<cfif processName EQ "-1">
			<cfset containerFinExecutionError(apiContainer,containerEvent)>
			<cfthrow attributecollection="#getErrorProperties()#"
				message="La valeur retournée par CONTAINER.getCodeProcess() est invalide"
				detail="UUID : #UUID#, JOBID : #containerEvent.getJobId()#. Valeur retournée : #processName#">
		<cfelse>
			<cfset var jobStatus=containerEvent.getContainerStatus()>
			<!--- Méthode 5 de container : ProcessContainer.run() --->
			<cfset var processContainer=createObject("component","fr.consotel.process.process" & processName)>
			<cfset var runStatus=processContainer.run(containerEvent.getJobId(),jobStatus,UUID)>
			<cfset logInfo("CONTAINER : fr.consotel.process.process" & processName & ".run() -> " & runStatus)>
			<!--- Si la valeur retournée par la procédure stockée est invalide --->
			<cfif runStatus NEQ 1>
				<cfset containerFinExecutionError(apiContainer,containerEvent)>
				<cfthrow attributecollection="#getErrorProperties()#"
					message="La valeur retournée par la méthode run() du process container est est invalide"
					detail="UUID : #UUID#, JOBID : #containerEvent.getJobId()#. Process : #processName# Valeur retournée : #runStatus#">
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="containerFinExecutionError" returntype="void"
	hint="Appelée par containerFinExecution() lorsque qu'une des conditions suivantes est vérifiées :
	- La valeur retournée par CONTAINER.getCodeProcess() est invalide
	- La valeur retournée par PROCESS.run() est différente de 1
	Instancie un composant fr.consotel.process.processContainerErreur et appelle sa méthode run() avec les paramètres suivants :
	- JOBID : Valeur de event.getJobId()
	- STATUS : Valeur de event.getContainerStatus()
	- BI_SERVER : Valeur de container.getBipServerParamValue()">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true" hint="Provient de containerFinExecution()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.ContainerEvent" required="true" hint="Provient de containerFinExecution()">
			<cfset var apiContainer=ARGUMENTS["container"]>
			<cfset var containerEvent=ARGUMENTS["event"]>
			<cfset var jobStatus=containerEvent.getContainerStatus()>
			<!--- Composant container pour le traitement des erreurs de fin d'exécution du reporting --->
			<cfset var apiContainerErreur=createObject("component","fr.consotel.process.processContainerErreur")>
			<cfset var runErrorStatus=apiContainerErreur.run(containerEvent.getJobId(),jobStatus,containerEvent.getFileUUID())>
			<cfset logInfo("CONTAINER : processContainerErreur.run() -> " & runErrorStatus)>
	</cffunction>
	
	<cffunction access="private" name="containerApresExecutionError" returntype="void" hint="Cette méthode n'est pas implémentée
	Appelée par containerApresExecution() lorsque la valeur retournée par une des méthodes suivantes est invalide :
	CONTAINER.ADDJobIdToDocument() ou CONTAINER.setCodeProcessForJOBID()">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true"
		hint="Provient de containerApresExecution()">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Provient de containerApresExecution()">
		<cfargument name="jobId" type="Numeric"  required="true" hint="Provient de containerApresExecution()">
	</cffunction>
	
	<cffunction access="private" name="containerAvantExecutionError" returntype="void" hint="Cette méthode n'est pas implémentée
	Appelée par containerAvantExecution() lorsque la valeur retournée par CONTAINER.ADDDocumentTableContainer() est invalide">
		<cfargument name="container" type="fr.consotel.api.ibis.ext.ExtensionApiContainerV1" required="true"
		hint="Provient de containerAvantExecution()">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Provient de containerAvantExecution()">
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.delivery.FTP --->
	<cffunction access="private" name="validateDeliveryProperties" returntype="void"
	hint="Vérifie la présence des propriétés suivantes : USERID,IDRACINE,CODE_APP,FICHIER
	Puis appelle la méthode de la classe parent. Une exception est levée si une de ces propriétés n'est pas présente">
		<cfargument name="deliveryProps" type="Struct"  required="true" hint="Structure retournée par getDeliveryProperties()">
		<cfset var deliveryProperties=ARGUMENTS["deliveryProps"]>
		<cfset var requiredKey="USERID,IDRACINE,CODE_APP,FICHIER,FILE_EXT">
		<cfloop index="key" list="#requiredKey#" delimiters=",">
			<cfif NOT structKeyExists(deliveryProperties,key)>
				<cfthrow attributecollection="#getErrorProperties()#" message="La propriété #key# n'est pas fournie pour la stratégie Container"
					detail="Cette propriété doit etre fournie par le reporting dans la clé DELIVERY">
			<cfelseif LEN(TRIM(deliveryProperties[key])) EQ 0>
				<cfthrow attributecollection="#getErrorProperties()#" message="La propriété #key# n'est pas renseignée pour la stratégie Container"
					detail="La valeur de cette propriété ne peut pas etre une chaine vide">
			</cfif>
		</cfloop>
		<cfset SUPER.validateDeliveryProperties(deliveryProperties)>
	</cffunction>
</cfcomponent>