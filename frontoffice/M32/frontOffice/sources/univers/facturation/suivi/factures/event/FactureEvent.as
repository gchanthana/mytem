package univers.facturation.suivi.factures.event
{
    import mx.resources.ResourceManager;
	import flash.events.Event;

    public class FactureEvent extends Event
    {
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES STATIC - EVENT NAME
        //--------------------------------------------------------------------------------------------//
        public static const THIS_IS_FAVORITE:String = "this_is_favorite";
        public static const THIS_IS_NOT_FAVORITE:String = "this_is_not_favorite";
        public static const LISTED_PRODUIT_LOADED:String = "listed_produit_loaded";
		public static const LISTED_FACTURE_CHANGE:String = "listed_Facture_change";
        public static const LISTED_PRODUIT_FAVORI_LOADED:String = "listed_produit_favori_loaded";
        public static const LISTED_PRODUIT_LOADED_ERROR:String = "listed_produit_loaded_error";
        public static const LISTED_PRODUIT_FAVORI_LOADED_ERROR:String = "listed_produit_favori_loaded_error";
        public static const MESSAGE_ERROR_FAVORIS:String = ResourceManager.getInstance().getString('M32', 'Gestion_des_favoris');
        public static const MESSAGE_ERROR_ADD_FAVORIS:String = ResourceManager.getInstance().getString('M32', 'Erreur_dans_l_ajout_du_produit_aux_favor');
        public static const MESSAGE_ERROR_DELETE_FAVORIS:String = ResourceManager.getInstance().getString('M32', 'Erreur_dans_la_suppression_du_produit_de');
        //--------------------------------------------------------------------------------------------//
        //					VARIABLES PASSEES EN ARGUMENTS
        //--------------------------------------------------------------------------------------------//		
        public var obj:Object = null;

        //--------------------------------------------------------------------------------------------//
        //					CONSTRUCTEUR
        //--------------------------------------------------------------------------------------------//		
        public function FactureEvent(type:String, objet:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
            this.obj = objet;
        }

        //--------------------------------------------------------------------------------------------//
        //					METHODES PUBLIC OVERRIDE - COPY OF EVENT
        //--------------------------------------------------------------------------------------------//
        override public function clone():Event
        {
            return new FactureEvent(type, obj, bubbles, cancelable);
        }
    }
}