package gestioncommande.services
{
	import composants.mail.MailVO;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.registerClassAlias;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.ArticlesSummary;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Historique;
	import gestioncommande.entity.References;
	import gestioncommande.events.CommandeEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.managers.SystemManager;
	import mx.messaging.messages.ErrorMessage;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import session.SessionUserObject;
	
	public class CommandeService extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		private var _thisCurrentCommande		:Commande;
		private var _myCommandeSelected			:Commande;
		
		public var listeCommandes				:ArrayCollection;
		public var listeRevendeursCommandes		:ArrayCollection = new ArrayCollection();
		public var allIdCommande				:ArrayCollection = new ArrayCollection();
		
		public var listeEtats					:ArrayCollection = new ArrayCollection();
		public var listeGestionnaires			:ArrayCollection = new ArrayCollection();
		public var listeOperateurs				:ArrayCollection = new ArrayCollection();
		public var listeComptes					:ArrayCollection = new ArrayCollection();
		public var listeSousComptes				:ArrayCollection = new ArrayCollection();
		public var listeActions					:ArrayCollection = new ArrayCollection();
		public var listeActionsProfil			:ArrayCollection = new ArrayCollection();
		
		public var LISTETRANSPORTEURS			:ArrayCollection;
		public var LISTELIBELLESCOMPTES			:ArrayCollection;
		public var LISTEADRESSEREVENDEUR		:ArrayCollection;
		public var LISTEHISTORIQUE				:ArrayCollection;
		public var LISTELIGNESAFFECTED			:ArrayCollection;
		
		public var LISTEREFERENCESINFOS			:ArrayCollection = new ArrayCollection();
		public var LISTEREFERENCESINFOS_PRODUIT	:ArrayCollection = new ArrayCollection();
		
		public var articlesConfiguration		:ArrayCollection;
		public var nbLigne						:int 	= 0;
				
	    public var idCommande					:int 	= 0;	   
		public var numCommande					:String = '';
			
		public var listeArticles 				:XML = <articles></articles>;
		
		public var COMMANDEPARTIELLE 			:Boolean = false;
		
		public var 	newidcommande				:int = 1;
		
		public static const LIST_SLA			:String = "LIST_SLA";
		public static const SELECT_SLA			:String = "SELECT_SLA";
		
		public var revendeurSLA					:Object;
		
		public var currentCommande:Commande;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//		    
		
		public function CommandeService()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODE PUBLIC
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					PUBLIC METHODE - APPEL DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
		
		//ENREGISTRER LA COMMANDE
		public function recordCommande(cmd:Commande, articles:Array, files:Array, UUIDDirectory:String):void
		{
			var idFixe:int = 0;
			var idMobi:int = 0;
			var idData:int = 0;
			
			_thisCurrentCommande = new Commande();
			_thisCurrentCommande = cmd;
			switch(SessionUserObject.singletonSession.IDSEGMENT)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: idFixe = 0;
						idMobi = 1;
						idData = 0;
						break;
				case 2: idFixe = 1;
						idMobi = 0;
						idData = 0;
						break;
				case 3: idFixe = 0;
						idMobi = 0;
						idData = 1;
						break;
			}
			
			if(cmd.LIBELLE_TO == ResourceManager.getInstance().getString('M16','nom_pr_nom___n__d_appel__mail'))
				cmd.LIBELLE_TO = "";
			
	    	cmd.DATE_HEURE = DateFunction.formatDateAsInverseString(cmd.DATE_COMMANDE) + " " + Formator.formatHourConcat(cmd.DATE_COMMANDE);
	    	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.M16.v2.CommandeService",
	    																			"enregistrerCommande",
	    																			recordCommandeResultHandler,
																					recordCommandeFaultHandler);
			
			op.addEventListener(FaultEvent.FAULT, recordCommandeFaultHandler);
			
				RemoteObjectUtil.callService(op,cmd.IDGESTIONNAIRE,
												cmd.IDPOOL_GESTIONNAIRE,	
												cmd.IDCOMPTE_FACTURATION,
												cmd.IDSOUS_COMPTE,
												cmd.IDOPERATEUR,
												cmd.IDREVENDEUR,
												cmd.IDCONTACT,
												cmd.IDTRANSPORTEUR,
												cmd.IDSITELIVRAISON,
												cmd.NUMERO_TRACKING,
												cmd.LIBELLE_COMMANDE,
												cmd.REF_CLIENT1,
												cmd.REF_CLIENT2,
												cmd.REF_OPERATEUR,
												cmd.DATE_HEURE,
												DateFunction.formatDateAsInverseString(cmd.LIVRAISON_PREVUE_LE),
												cmd.COMMENTAIRES,
												cmd.BOOL_DEVIS,
												cmd.MONTANT,
												idFixe,
												idMobi,
												idData,
												cmd.IDTYPE_COMMANDE,
												cmd.IDGROUPE_REPERE,
												cmd.NUMERO_COMMANDE,
												cmd.IDPROFIL_EQUIPEMENT,
												cmd.IDSITEFACTURATION,
												cmd.LIBELLE_TO,
												articles,
												cmd.V1,
												cmd.V2,
												cmd.V3,
												cmd.V4,
												cmd.V5,
												files,
												UUIDDirectory);
		}
		
		/**
		 * ENREGISTRER LA COMMANDE version 2 de type mobile, fixe ou reseau
		 **/
		public function recordCommandeV2(cmd:Commande, articles:Array, files:Array, mymail:MailVO, isSendMail:int, myaction:Action, UUIDDirectory:String):void
		{
			var idFixe:int = 0;
			var idMobi:int = 0;
			var idData:int = 0;
			
			_thisCurrentCommande = new Commande();
			_thisCurrentCommande = cmd;
			
			switch(SessionUserObject.singletonSession.IDSEGMENT)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: idFixe = 0;
						idMobi = 1;
						idData = 0;
						break;
				case 2: idFixe = 1;
						idMobi = 0;
						idData = 0;
						break;
				case 3: idFixe = 0;
						idMobi = 0;
						idData = 1;
						break;
			}
			
			if(cmd.LIBELLE_TO == ResourceManager.getInstance().getString('M16','nom_pr_nom___n__d_appel__mail'))
				cmd.LIBELLE_TO = '';
			
			cmd.DATE_HEURE = DateFunction.formatDateAsInverseString(cmd.DATE_COMMANDE) + " " + Formator.formatHourConcat(cmd.DATE_COMMANDE);
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"enregistrerCommandeV2",
																				recordCommandeResultHandler,
																				recordCommandeFaultHandler);
			
			op.addEventListener(FaultEvent.FAULT, recordCommandeFaultHandler);
			
			RemoteObjectUtil.callService(op,mymail,
											cmd.IDGESTIONNAIRE,
											cmd.IDPOOL_GESTIONNAIRE,	
											cmd.IDCOMPTE_FACTURATION,
											cmd.IDSOUS_COMPTE,
											cmd.IDOPERATEUR,
											cmd.IDREVENDEUR,
											cmd.IDCONTACT,
											cmd.IDTRANSPORTEUR,
											cmd.IDSITELIVRAISON,
											cmd.NUMERO_TRACKING,
											cmd.LIBELLE_COMMANDE,
											cmd.REF_CLIENT1,
											cmd.REF_CLIENT2,
											cmd.REF_OPERATEUR,
											cmd.DATE_HEURE,
											DateFunction.formatDateAsInverseString(cmd.LIVRAISON_PREVUE_LE),
											cmd.COMMENTAIRES,
											cmd.BOOL_DEVIS,
											cmd.MONTANT,
											idFixe,
											idMobi,
											idData,
											cmd.IDTYPE_COMMANDE,
											cmd.IDGROUPE_REPERE,
											cmd.NUMERO_COMMANDE,
											cmd.IDPROFIL_EQUIPEMENT,
											cmd.IDSITEFACTURATION,
											cmd.LIBELLE_TO,
											articles,
											cmd.V1,
											cmd.V2,
											cmd.V3,
											cmd.V4,
											cmd.V5,
											files,
											isSendMail,
											myaction,
											UUIDDirectory);
			
		}// fin recordCommandeV2
		
		/**
		 * ENREGISTRER LA COMMANDE (verion 2) pour une commande de SPIE 
		 */
		public function recordCommandeV2Spie(cmd:Commande, 
											 articles:Array, 
											 files:Array, 
											 mymail:MailVO, 
											 isSendMail:int, 
											 myaction:Action, 
											 UUIDDirectory:String, 
											 typeCMD:String = 'SPIE'):void
		{
			var idFixe:int = 0;
			var idMobi:int = 0;
			var idData:int = 0;
			
			_thisCurrentCommande = new Commande();
			_thisCurrentCommande = cmd;
			
			switch(SessionUserObject.singletonSession.IDSEGMENT)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: idFixe = 0;
					idMobi = 1;
					idData = 0;
					break;
				case 2: idFixe = 1;
					idMobi = 0;
					idData = 0;
					break;
				case 3: idFixe = 0;
					idMobi = 0;
					idData = 1;
					break;
			}
			
			if(cmd.LIBELLE_TO == ResourceManager.getInstance().getString('M16','nom_pr_nom___n__d_appel__mail'))
				cmd.LIBELLE_TO = '';
			
			cmd.DATE_HEURE = DateFunction.formatDateAsInverseString(cmd.DATE_COMMANDE) + " " + Formator.formatHourConcat(cmd.DATE_COMMANDE);
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.M16.v2.CommandeService",
																					"enregistrerCommandeV2",
																					recordCommandeResultHandler,
																					recordCommandeFaultHandler);
			
			op.addEventListener(FaultEvent.FAULT, recordCommandeFaultHandler);
			
			RemoteObjectUtil.callService(op,mymail,
				cmd.IDGESTIONNAIRE,
				cmd.IDPOOL_GESTIONNAIRE,	
				cmd.IDCOMPTE_FACTURATION,
				cmd.IDSOUS_COMPTE,
				cmd.IDOPERATEUR,
				cmd.IDREVENDEUR,
				cmd.IDCONTACT,
				cmd.IDTRANSPORTEUR,
				cmd.IDSITELIVRAISON,
				cmd.NUMERO_TRACKING,
				cmd.LIBELLE_COMMANDE,
				cmd.REF_CLIENT1,
				cmd.REF_CLIENT2,
				cmd.REF_OPERATEUR,
				cmd.DATE_HEURE,
				DateFunction.formatDateAsInverseString(cmd.LIVRAISON_PREVUE_LE),
				cmd.COMMENTAIRES,
				cmd.BOOL_DEVIS,
				cmd.MONTANT,
				idFixe,
				idMobi,
				idData,
				cmd.IDTYPE_COMMANDE,
				cmd.IDGROUPE_REPERE,
				cmd.NUMERO_COMMANDE,
				cmd.IDPROFIL_EQUIPEMENT,
				cmd.IDSITEFACTURATION,
				cmd.LIBELLE_TO,
				articles,
				cmd.V1,
				cmd.V2,
				cmd.V3,
				cmd.V4,
				cmd.V5,
				files,
				isSendMail,
				myaction,
				UUIDDirectory,
				typeCMD
			);
		}// fin recordCommandeV2Spie
		
		private function recordCommandeFaultHandler(fe:FaultEvent):void
		{
			var myerror:String = (fe.message as ErrorMessage).faultString.toLowerCase();
			
			if(myerror == 'error')
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenueNous_allons_v_rif_'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), verificationCmdHandler);
			}
			else if (myerror == 'channel disconnected')
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), null);
			}
			else if (myerror == 'send failed')
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), null);
			}
		}
		
		// vérifier si la commande était enregistré ou NON à la suite d'un problème reseau 
		private function verificationCmdHandler(evt:Event):void
		{
			dispatchEvent(new CommandeEvent('COMMANDE_ERROR_VERIF'));
		}
		
		/**
		 * Enregister la commande (version 2) lorsqu'il s'agit d'une opération sur ligneds existante
		 * */
		public function enregistrerCommandesResiliationV2(commandes:ArrayCollection, files:Array, mymail:MailVO, isSendMail:int, myaction:Action, uuidref:String): void
		{	
			allIdCommande = new ArrayCollection();
			
			registerClassAlias("fr.consotel.consoview.M16.CommandeVO",Commande);
			
			_thisCurrentCommande = new Commande();
			_thisCurrentCommande = commandes.source[0].cmd as Commande; 
			_thisCurrentCommande.LIVRAISON_PREVUE_LE.setHours(18,00);
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"enregistrerResiliationV2",
																				function(re:ResultEvent):void
																				{
																					if(re.result)	    	
																					{
																						allIdCommande.source = re.result as Array;
																						ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Commande_enregistr_e__'), SystemManager.getSWFRoot(this));
																						dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
																					}
																					else
																					{
																						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
																							ResourceManager.getInstance().getString('M16', 'Erreur'), null);
																						dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_ERROR));
																					}	
																				},recordCommandeFaultHandler);
			op.addEventListener(FaultEvent.FAULT, recordCommandeFaultHandler);
			
			RemoteObjectUtil.callService(op,mymail,
											commandes,
											files,
											uuidref,
											isSendMail,
											myaction);
		}
		
		
		/**
		 * Enregister la commande sur lignes existantes
		 **/
		public function enregistrerCommandesResiliation(commandes:ArrayCollection, files:Array, uuidref:String): void
		{	
			allIdCommande = new ArrayCollection();
			
			registerClassAlias("fr.consotel.consoview.M16.CommandeVO",Commande);
			
			_thisCurrentCommande = new Commande();
			_thisCurrentCommande = commandes.source[0].cmd as Commande; 
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"enregistrerResiliation",
																				function(re:ResultEvent):void
																				{
																					if(re.result)	    	
																					{
																						allIdCommande.source = re.result as Array;
																						dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
																					}
																					else
																					{
																						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Erreur'), 
																							'Consoview', null);
																						dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_ERROR));
																					}	
																				},recordCommandeFaultHandler);
			op.addEventListener(FaultEvent.FAULT, recordCommandeFaultHandler);
			
			RemoteObjectUtil.callService(op,commandes,
											files,
											uuidref);
		}
		
		//METTRE A JOUR LA COMMANDE
		public function enregistrerArticles(cmd:Commande, articles:Array):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
    																			"fr.consotel.consoview.M16.v2.CommandeService",
    																			"enregistrerArticles",
    																			enregistrerArticlesResultHandler,
																				enregistrerArticlesFaultHandler);
			
			op.addEventListener(FaultEvent.FAULT, enregistrerArticlesFaultHandler);
			
			RemoteObjectUtil.callService(op,cmd.IDCOMMANDE,
											cmd.IDPOOL_GESTIONNAIRE,
											articles);
		}

		private function enregistrerArticlesFaultHandler(fe:FaultEvent):void
		{
			var myerror:String = (fe.message as ErrorMessage).faultString.toLowerCase();
			
			if(myerror == 'error')
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Vos_articles_sont_enregistr_e__nMerci_d_'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), commandeArticleError);
			}
			else if (myerror == 'channel disconnected')
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Vos_articles_sont_enregistr_e__nMerci_d_'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), commandeArticleError);
			}
			else if (myerror == 'send failed')
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVos_articles_n'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), null);
			}
		}
		
		private function commandeArticleError(e:Event):void
		{
			dispatchEvent(new CommandeEvent('ARTICLE_ERROR'));
		}

		//METTRE A JOUR LA COMMANDE		
		public function majCommandeV2(cmd:Commande, articles:Array, files:Array, mymail:MailVO, isSendMail:int, myaction:Action, actionModification:Action, UUIDDirectory:String):void
		{
			if(cmd.LIBELLE_TO == ResourceManager.getInstance().getString('M16','nom_pr_nom___n__d_appel__mail'))
				cmd.LIBELLE_TO = "";
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"majCommandeV2",
																				majCommandeResultHandler,
																				majCommandeFaultHandler);
			
			op.addEventListener(FaultEvent.FAULT, majCommandeFaultHandler);
			
			RemoteObjectUtil.callService(op,cmd.IDCOMMANDE,
											cmd.IDGESTIONNAIRE,
											cmd.IDPOOL_GESTIONNAIRE,
											cmd.IDTRANSPORTEUR,
											cmd.IDSITELIVRAISON,
											cmd.IDCONTACT,
											cmd.NUMERO_COMMANDE,
											cmd.NUMERO_TRACKING,
											cmd.LIBELLE_COMMANDE,
											cmd.REF_CLIENT1,
											cmd.REF_CLIENT2,
											cmd.REF_OPERATEUR,
											DateFunction.formatDateAsInverseString(cmd.LIVRAISON_PREVUE_LE),
											cmd.COMMENTAIRES,
											cmd.MONTANT,
											cmd.LIBELLE_TO,
											cmd.IDCOMPTE_FACTURATION,
											cmd.IDSOUS_COMPTE,
											cmd.IDSITEFACTURATION,
											articles,
											UUIDDirectory,
											files,
											cmd.IDTYPE_COMMANDE,
											cmd.IDOPERATEUR,
											isSendMail,
											myaction,
											actionModification,
											mymail,
											ResourceManager.getInstance().getString('M16','Modification_de_la_commande_n_'));
		}
		
		//METTRE A JOUR LA COMMANDE
		public function majCommande(cmd:Commande):void
		{
			if(cmd.LIBELLE_TO == ResourceManager.getInstance().getString('M16','nom_pr_nom___n__d_appel__mail'))
				cmd.LIBELLE_TO = "";
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
    																			"fr.consotel.consoview.M16.v2.CommandeService",
    																			"majCommande",
    																			majCommandeResultHandler,
																				majCommandeFaultHandler);
			
			op.addEventListener(FaultEvent.FAULT, majCommandeFaultHandler);
			
			RemoteObjectUtil.callService(op,cmd.IDCOMMANDE,
											cmd.IDGESTIONNAIRE,
											cmd.IDPOOL_GESTIONNAIRE,
											cmd.IDTRANSPORTEUR,
											cmd.IDSITELIVRAISON,
											cmd.IDCONTACT,
											cmd.NUMERO_COMMANDE,
											cmd.NUMERO_TRACKING,
											cmd.LIBELLE_COMMANDE,
											cmd.REF_CLIENT1,
											cmd.REF_CLIENT2,
											cmd.REF_OPERATEUR,
											DateFunction.formatDateAsInverseString(cmd.LIVRAISON_PREVUE_LE),
											cmd.COMMENTAIRES,
											cmd.MONTANT,
											cmd.LIBELLE_TO,
											cmd.IDCOMPTE_FACTURATION,
											cmd.IDSOUS_COMPTE,
											cmd.IDSITEFACTURATION,
											cmd.LIVRAISON_DISTRIBUTEUR);
		}
		
		private function majCommandeFaultHandler(fe:FaultEvent):void
		{
			var myerror:String = (fe.message as ErrorMessage).faultString.toLowerCase();
			
			if(myerror == 'error' || myerror == 'channel disconnected' || myerror == 'send failed')
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), null);
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Echec_de_connexion_'), null);
			}
		}

		public function fournirArticlesCommande(idCommande:int): void
	    {	
			if(idCommande < 1)
				return;
				
    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
    																			"fr.consotel.consoview.M16.v2.CommandeService",
    																			"fournirArticlesCommande",
    																			fournirArticlesCommandeResultHandler);
			
			RemoteObjectUtil.callService(op,idCommande);
	    }

		public function getArticleSummary(myCommandeSelected:Commande): void
		{	
			if(myCommandeSelected == null || myCommandeSelected.IDCOMMANDE < 1)
				return;
			
			_myCommandeSelected = new Commande();
			_myCommandeSelected = myCommandeSelected;
			
			articlesConfiguration = new ArrayCollection();
			nbLigne = 0;
			
			var op:AbstractOperation = RemoteObjectUtil.getSilentOperation("fr.consotel.consoview.M16.v2.CommandeService",
																			"getArticleSummary",
																			getArticleSummaryResultHandler);
			
			RemoteObjectUtil.callSilentService(op,myCommandeSelected.IDCOMMANDE);
		}

		public function fournirRevendeurSLA(idRevendeur:int, idOperateur:int):void
	    {
	    	revendeurSLA = new Object();
	    	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																		"fr.consotel.consoview.M16.Commande",
	    																			"getRevendeurSLA",
	    																			fournirRevendeurSLAResultHanler);		
	    	RemoteObjectUtil.callService(op,idRevendeur,
											idOperateur);
	    }
	    
	    //SUPPRIMER LES ARTICLES DE LA COMMANDE
		public function eraseArticles(idcommande:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																		"fr.consotel.consoview.M16.v2.CommandeService",
	    																			"eraseAticles",
	    																			eraseArticlesResultHanler);		
	    	RemoteObjectUtil.callService(op,idcommande);
		}
		
		public function majArticles(cmdeID:int, idpool:int, artiles:Array):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																	"fr.consotel.consoview.M16.v2.CommandeService",
	    																		"majArticles",
	    																		majArticlesResultHandler);		
	    	RemoteObjectUtil.callService(op,cmdeID,
	    									idpool,
											artiles);
		}
		
		public function livraisonPartielle(cmdeID:int, clob:String):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"SavePartialDelivery",
																				livraisonPartielleResultHandler);		
			RemoteObjectUtil.callService(op,cmdeID,
											clob);
		}
		
		
		
		public function fournirDetailOperation(commande:Commande, idCommande:int): void
		{
			currentCommande = new Commande();
			currentCommande = commande;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"fournirDetailOperation",
																				fournirDetailOperationResultHandler);
			RemoteObjectUtil.callService(op, idCommande)
		}
		
		public function fournirListesCommandesAndFiltres(idpool:int, idprofil:int, isAllCommande:Boolean = true):void
		{
			listeCommandes = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"fournirInventaire2CommandeAndFiltresWithActions",
																				fournirListesCommandesAndFiltresResultHandler);
			RemoteObjectUtil.callService(op,'',
											idpool,
											idprofil,
											0,
											Formator.formatBoolean(isAllCommande));
		}
		
		public function fournirListesCommandes(idpool:int, isAllCommande:Boolean = true):void
		{
			listeCommandes = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"fournirInventaireCommande2",
																				fournirListesCommandesResultHandler);
			RemoteObjectUtil.callService(op,'',
											idpool,
											0,
											Formator.formatBoolean(isAllCommande));
		}

		public function getInfosCommande(commande:Commande, idsegment:int):void
		{			
			currentCommande = new Commande();
			currentCommande = commande;
			
			LISTETRANSPORTEURS		= new ArrayCollection();
			LISTELIBELLESCOMPTES	= new ArrayCollection();
			LISTEADRESSEREVENDEUR	= new ArrayCollection();
			LISTEHISTORIQUE			= new ArrayCollection();
			LISTELIGNESAFFECTED 	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"getInfosCommande",
																				getInfosCommandeResultHandler);
			RemoteObjectUtil.callService(op,commande.IDCOMMANDE,
											commande.IDOPERATEUR,
											commande.IDREVENDEUR,
											idsegment);
		}
		
		public function fournirReferenceInfos(idCommande:int, idpool:int, currentCommande:Commande): void
		{	
			LISTEREFERENCESINFOS = new ArrayCollection();
			
			_thisCurrentCommande = new Commande();
			_thisCurrentCommande = currentCommande.copyCommande();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.CommandeService",
																				"fournirReferenceInfos",
																				fournirReferenceInfosResultHandler);
			
			RemoteObjectUtil.callService(op,idCommande,
											idpool);
		}
		
		public function fournirReferenceInfosSpie(idCommande:int, idpool:int, currentCommande:Commande): void
		{	
			LISTEREFERENCESINFOS = new ArrayCollection();
			
			_thisCurrentCommande = new Commande();
			_thisCurrentCommande = currentCommande.copyCommande();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M16.v2.CommandeService",
				"fournirReferenceInfosSpie",
				fournirReferenceInfosSpieResultHandler);
			
			RemoteObjectUtil.callService(op,idCommande,
				idpool);
		}
		
		/**
		 * vérifier si une commande est enregistrée ou pas en cas de coupure réseau
		 **/ 
		public function verifierCommande(clefCmd:String = ''):void
		{ 	
			listeCommandes = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				'fr.consotel.consoview.M16.v2.ListeCommandesService',
				'fournirInventaireCommandePaginate',
				verifierCommandeResultHandler);
			
			op.addEventListener(FaultEvent.FAULT, majCommandeFaultHandler);
			
			RemoteObjectUtil.callService(op,-1,											// idsegment (egal -1 par défaut)
											clefCmd, 									// clef 
											-1,											// idpool
											-1,											// idstatut
											-1,											// idetat
											-1,											// idoperateur
											-1,											// idcompte
											-1,										// idsouscompte
											-1,										// idusercreate
											1,										// startindex
											30,										// nbrRecord
											0,										// idcritere
											''										// txtFilter
										);	
			
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODE PRIVATE
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					PRIVATE METHODE - RETOUR DE PROCEDURE
		//--------------------------------------------------------------------------------------------//
	    
		private function searchCommandeResultHandler(re:ResultEvent):void 
		{
			if((re.result as ArrayCollection).length > 0)	    	
			{				
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Commande_enregistr_e__'),SystemManager.getSWFRoot(this));
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Commande_non_trouv_e_'), 'Consoview', null);
		}
		
		private function verifierCommandeResultHandler(re:ResultEvent):void 
		{
			if((re.result as ArrayCollection).length > 0)	    	
			{			
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Commande_enregistr_e__'),SystemManager.getSWFRoot(this));
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
			}
			else
			{
				dispatchEvent(new CommandeEvent(CommandeEvent.CMD_INEXISTANT_V));
			}
				
		}
		
	    private function recordCommandeResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)	    	
			{
				SessionUserObject.singletonSession.COMMANDE.IDCOMMANDE = Number(re.result);
				idCommande 	= Number(re.result);
				numCommande = SessionUserObject.singletonSession.COMMANDE.NUMERO_COMMANDE;
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Commande_enregistr_e__'),SystemManager.getSWFRoot(this));
	    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
		
		private function enregistrerArticlesResultHandler(re:ResultEvent):void
		{
			if((re.result as Array).length > 0)	    	
			{
				var OK			:Boolean = true;
				var idArticles	:Array 	 = re.result as Array
				var len			:int 	 = idArticles.length;
				
				for(var i:int = 0;i < len;i++)
				{
					if(idArticles[i] > 0)
						trace("ARTICLES > 0");
					else
						OK = false;
				}
				
				if(OK)
				{					
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Commande_enregistr_e__'), SystemManager.getSWFRoot(this));
		    		
					dispatchEvent(new CommandeEvent(CommandeEvent.ARTICLES_UPDATED));
		  		}
		  		else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
						ResourceManager.getInstance().getString('M16', 'Erreur'));
	    	}
	    	else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
		
		private function majCommandeResultHandler(re:ResultEvent):void
		{
			if(re.result > 0)	    	
			{
				if(re.result > 1)
				{
					if(SessionUserObject.singletonSession.COMMANDE != null)
						SessionUserObject.singletonSession.COMMANDE.IDCOMMANDE = Number(re.result);
					
					idCommande  = Number(re.result);
					numCommande = SessionUserObject.singletonSession.COMMANDE.NUMERO_COMMANDE;
	   			}
	   			
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_UPDATED));
	    	}
	    	else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
	    
	    private function fournirRevendeurSLAResultHanler(re:ResultEvent):void
	    {
	    	if(re.result)
	    	{	
	    		var sla:ArrayCollection = re.result as ArrayCollection;
	    		var len:int				= sla.length;
	    		
	    		if(len > 0)
	    			revendeurSLA = sla[0];
	    		else
	    			revendeurSLA = null;

	    		dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_REVENDEURSLA));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));	
	    }
		
		private function eraseArticlesResultHanler(re:ResultEvent):void
		{
			if(re.result > 0)
	    		dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_REVENDEURSLA));
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Effacement_des_articles_impossible__'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));	
		}
		
		private function majArticlesResultHandler(re:ResultEvent):void
		{
			if((re.result as Array).length > 0)	    	
			{
				var OK			:Boolean = true;
				var idArticles	:Array 	 = re.result as Array
				var len			:int 	 = idArticles.length;
				
				for(var i:int = 0;i < len;i++)
				{
					if(idArticles[i] > 0)
						trace("ARTICLES > 0");
					else
						OK = false;
				}
				
				if(OK)
				{					
					ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Mise___jour_des_articles_effectu_s'), SystemManager.getSWFRoot(this));
		    		dispatchEvent(new CommandeEvent(CommandeEvent.ARTICLES_UPDATED));
		  		}
		  		else
	    			ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Mise__jour_des_articles_impossible__'), 
						ResourceManager.getInstance().getString('M16', 'Erreur'));
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Mise__jour_des_articles_impossible__'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
		
		private function fournirArticlesCommandeResultHandler(re:ResultEvent):void 
    	{
    		if(re.result)	    	
    		{																			    			
	    		listeArticles = new XML(re.result[0].MODELEXML);
				
	    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_ARRIVED));																	    		
	    	}
	    	else
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
    	}

		private function getArticleSummaryResultHandler(re:ResultEvent):void 
		{
			if(re.result)	    	
			{									
				LISTELIGNESAFFECTED = re.result.LIGNES as ArrayCollection;
				
				articlesConfiguration = ArticlesSummary.formatArticlesSummary(re.result.ARTICLES as ArrayCollection, _myCommandeSelected);
				
				nbLigne = re.result.NBLIGNE as int;
				
				dispatchEvent(new CommandeEvent(CommandeEvent.ARTICLES_XML_GROUP));																	    		
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
		
		private function livraisonPartielleResultHandler(re:ResultEvent):void 
		{
			if(re.result)	    	
			{																			    			
				newidcommande = int(re.result);
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_PARTIELLE));																	    		
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue__nVotre_commande'),
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
		
		private function fournirDetailOperationResultHandler(re:ResultEvent):void
		{
			if(re.result && (re.result as ArrayCollection).length > 0)	    	
			{																			    			
				mappDataCommande(currentCommande, re.result[0]);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_DETAILS));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}

		private function fournirListesCommandesAndFiltresResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				var noDoublon:ArrayCollection = new ArrayCollection(re.result.LISTECOMMANDES.source);
				
				listeCommandes = mappDataListeCommandes(noDoublon);
				
				listeEtats			 = addFirstItem(new ArrayCollection(re.result.LISTEETATS.source), 'IDETAT');
				listeGestionnaires	 = addFirstItem(new ArrayCollection(re.result.LISTEGESTIONNAIRES.source), 'USERID');
				listeOperateurs		 = addFirstItem(new ArrayCollection(re.result.LISTEOPERATEURS.source), 'IDOPERATEUR');
				listeComptes		 = addFirstItem(new ArrayCollection(re.result.LISTECOMPTES.source), 'IDCOMPTE_FACTURATION');
				listeSousComptes	 = addFirstItem(new ArrayCollection(re.result.LISTESOUSCOMPTE.source), 'IDSOUS_COMPTE');
				
				listeActions 		 = new ArrayCollection(re.result.LISTEACTIONS.source);
				listeActionsProfil 	 = addActionsProfil(new ArrayCollection(re.result.LISTEACTIONSPROFIL.source));
				
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_LISTED_FIL));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucune_commande'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'), null);
		}

		private function fournirListesCommandesResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				var noDoublon:ArrayCollection = updateListeDoublon(re.result as ArrayCollection);
				
				listeCommandes = mappDataListeCommandes(noDoublon);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_LISTED));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucune_commande'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'), null);
		}

		private function getInfosCommandeResultHandler(re:ResultEvent): void
		{
			if(re.result)
			{
				if(re.result.hasOwnProperty('HISTORIQUE') && re.result.HISTORIQUE as ArrayCollection)
					LISTEHISTORIQUE = formaterHistorique(new ArrayCollection((re.result.HISTORIQUE as ArrayCollection).source));
				
				if(re.result.hasOwnProperty('REVENDEURADRESSE') && re.result.REVENDEURADRESSE as ArrayCollection)
					LISTEADRESSEREVENDEUR = new ArrayCollection((re.result.REVENDEURADRESSE as ArrayCollection).source);
				
				if(re.result.hasOwnProperty('TRANSPORTEURS') && re.result.TRANSPORTEURS as ArrayCollection)
					LISTETRANSPORTEURS = new ArrayCollection((re.result.TRANSPORTEURS as ArrayCollection).source);
				
				if(re.result.hasOwnProperty('DETAILS') && re.result.DETAILS.length > 0)
					mappDataCommande(currentCommande, re.result.DETAILS[0]);

				if(re.result.hasOwnProperty('LIBELLECOMPTESOUSCOMPTE') && re.result.LIBELLECOMPTESOUSCOMPTE as ArrayCollection)
					LISTELIBELLESCOMPTES = new ArrayCollection((re.result.LIBELLECOMPTESOUSCOMPTE as ArrayCollection).source);

				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_INFOS));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucune_commande'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'), null);
		}

		private function fournirReferenceInfosResultHandler(re:ResultEvent): void
		{
			if(re.result)	    	
			{																			    			
				LISTEREFERENCESINFOS = References.mappingArrayCollection(re.result as ArrayCollection, _thisCurrentCommande);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.INFOS_REFERENCES));																	    		
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
		
		private function fournirReferenceInfosSpieResultHandler(re:ResultEvent): void
		{
			if(re.result)	    	
			{																			    			
				LISTEREFERENCESINFOS = References.mappingArrayCollection(re.result.EQUIPEMENTS as ArrayCollection, _thisCurrentCommande);
				LISTEREFERENCESINFOS_PRODUIT = References.mappingArrayCollectionProduitSpie(re.result.PRODUITS as ArrayCollection, _thisCurrentCommande);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.INFOS_REFERENCES));																	    		
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 
					ResourceManager.getInstance().getString('M16', 'Erreur'));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					PRIVATE FUNCTIONS
		//--------------------------------------------------------------------------------------------//

		private function formaterHistorique(values:ICollectionView):ArrayCollection
		{
			var retour:ArrayCollection = new ArrayCollection();
			
			if(values != null)
			{
				var cursor:IViewCursor = values.createCursor();
				var diff:int = -1;
				
				while(!cursor.afterLast)
					
				{
					var hitoriqueObj:Historique 				= new Historique();
						
						if(diff!=cursor.current.IDINV_ACTIONS)
						
							{
						hitoriqueObj.PATRONYME_USERACTION 		= cursor.current.NOM;
						hitoriqueObj.DATE_INSERT 				= cursor.current.DATE_INSERT;
						hitoriqueObj.DATE_ACTION 				= cursor.current.DATE_INSERT;
						hitoriqueObj.DATE_STRING				= Formator.formatDate(hitoriqueObj.DATE_ACTION);
						hitoriqueObj.IDACTION 					= cursor.current.IDINV_ACTIONS;
						hitoriqueObj.LIBELLE_ACTION 			= cursor.current.LIBELLE_ACTION;					
						hitoriqueObj.PATRONYME_USERACTION 		= cursor.current.NOM;					
						hitoriqueObj.IDETAT 					= cursor.current.IDINV_ETAT;
						hitoriqueObj.LIBELLE_ETAT 				= cursor.current.LIBELLE_ETAT;	
						hitoriqueObj.REF_CALCUL 				= cursor.current.REF_CALCUL;
						hitoriqueObj.ENTRAINE_CLOTURE 			= cursor.current.ENTRAINE_CLOTURE;
						hitoriqueObj.ENTRAINE_CREATION 			= cursor.current.ENTRAINE_CREATION;	
						hitoriqueObj.CHANGER_ETAT_INVENTAIRE 	= cursor.current.CHANGER_ETAT_INVENTAIRE;
						hitoriqueObj.COMMENTAIRE_ETAT 			= cursor.current.COMMENTAIRE_ETAT;
						hitoriqueObj.LIBELLE_ETAT 				= cursor.current.LIBELLE_ETAT;
						hitoriqueObj.DISPLAY_MAIL 				= cursor.current.DISPLAY_MAIL;
						hitoriqueObj.CODE_ETAT 					= cursor.current.CODE_ETAT;
						hitoriqueObj.EMAIL 						= cursor.current.EMAIL;
						hitoriqueObj.IDUSER_CREATE 				= cursor.current.IDUSER_CREATE;
						hitoriqueObj.CHANGER_ETAT_PARC 			= cursor.current.CHANGER_ETAT_PARC;
						
						diff=cursor.current.IDINV_ACTIONS;
					
					retour.addItem(hitoriqueObj);	    			
					cursor.moveNext();
											
						}else {cursor.moveNext();}
				}	
			}
			
			return retour;
		}
		
		private function addActionsProfil(values:ArrayCollection):ArrayCollection
		{
			var rsltActionsProfil:ArrayCollection = new ArrayCollection();
			
			if(values)
			{
				var lenActions:int = values.length;
				
				for(var i:int = 0; i < lenActions;i++)
				{
					if(values[i].IDINV_ACTIONS == 2066 || values[i].IDINV_ACTIONS == 2216)
					{
						var actionProfile:Object = new Object();
						
						if(values[i].IDINV_ACTIONS == 2066)
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.MOBILE		= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(values[i].SELECTED);
						}
						else
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.FIXE			= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(values[i].SELECTED);
						}
						
						rsltActionsProfil.addItem(actionProfile);
					}
				}
			}
			
			return rsltActionsProfil;
		}
		
		private function addFirstItem(values:ArrayCollection, idlibelle:String):ArrayCollection
		{
			var myFistObject:Object = new Object();
			
			if(values != null && values.length > 0)
			{
				var firstElement:Object = values[0];
				
				if(firstElement[idlibelle] > 0)
				{
					myFistObject.LIBELLE 	= ResourceManager.getInstance().getString('M16', 'Tous');
					myFistObject[idlibelle] = -1;
					
					values.addItemAt(myFistObject, 0);
				}
				else
				{
					firstElement.LIBELLE	= ResourceManager.getInstance().getString('M16', 'Aucun');
					
					myFistObject.LIBELLE 	= ResourceManager.getInstance().getString('M16', 'Tous');
					myFistObject[idlibelle] = -1;
					
					values.addItemAt(myFistObject, 0);
				}
			}
			else
			{
				values = new ArrayCollection();
				
				myFistObject.LIBELLE 	= ResourceManager.getInstance().getString('M16', 'Aucun');
				myFistObject[idlibelle] = -1;
				
				values.addItem(myFistObject);
			}
			
			return values;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTER
		//--------------------------------------------------------------------------------------------//
		
		private function updateListeDoublon(values:ICollectionView):ArrayCollection
		{
			var invTemporaire	:ArrayCollection 	= new ArrayCollection();
			var index			:int				= 0;
			
			if(values != null)
			{
				var cursor:IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					index = ConsoviewUtil.getIndexById(invTemporaire,"IDCOMMANDE", cursor.current.IDCOMMANDE);
					
					if(index < 0)
						invTemporaire.source.push(cursor.current);
					
					cursor.moveNext();
				}
			}
			
			return invTemporaire;
		}
		
		private function mappDataListeCommandes(values:ICollectionView):ArrayCollection
		{
			var retour : ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					var commandeObj:Commande= new Commande();
					
					mappDataCommande(commandeObj, cursor.current);
					
					retour.addItem(commandeObj);
					cursor.moveNext();
				}
			}
			
			return retour;
		}

		//--------------------------------------------------------------------------------------------//
		//					MAPPING
		//--------------------------------------------------------------------------------------------//
		
		private function mappDataCommande(commande:Commande, value:Object):void
		{
			if(value.hasOwnProperty('IDPOOL_GESTIONNAIRE'))
				commande.IDPOOL_GESTIONNAIRE = value.IDPOOL_GESTIONNAIRE;
			else
				commande.IDPOOL_GESTIONNAIRE= value.IDPOOL; 
			
			if(value.hasOwnProperty('IDCOMMANDE'))
				commande.IDCOMMANDE = value.IDCOMMANDE;
			else
				commande.IDCOMMANDE	= commande.IDCOMMANDE; 
			
			if(value.hasOwnProperty('DATE_EFFET_PREVUE'))
				commande.LIVRAISON_PREVUE_LE = value.DATE_EFFET_PREVUE;
			else
				commande.LIVRAISON_PREVUE_LE = value.LIVRAISON_PREVUE_LE;
			
			if(value.hasOwnProperty('LIBELLE_COMPTE'))
				commande.LIBELLE_COMPTE = value.LIBELLE_COMPTE;
			else
				commande.LIBELLE_COMPTE = value.LIBELLE_COMPTE;
			
			if(value.hasOwnProperty('LIBELLE_SOUSCOMPTE'))
				commande.LIBELLE_SOUSCOMPTE = value.LIBELLE_SOUSCOMPTE;
			else
				commande.LIBELLE_SOUSCOMPTE = value.LIBELLE_SSCOMPTE;
			
			if(value.hasOwnProperty('IDCOMPTE_FACTURATION'))
				commande.IDCOMPTE_FACTURATION = Number(value.IDCOMPTE_FACTURATION);
			else
				commande.IDCOMPTE_FACTURATION = Number(value.IDCOMPTE);
			
			if(value.hasOwnProperty('IDSOUS_COMPTE'))
				commande.IDSOUS_COMPTE = Number(value.IDSOUS_COMPTE);
			else
				commande.IDSOUS_COMPTE = Number(value.IDSSCOMPTE);
			
			if(value.hasOwnProperty('OPERATEURID'))
				commande.IDOPERATEUR = Number(value.OPERATEURID);
			else
				commande.IDOPERATEUR = Number(value.IDOPERATEUR);

			commande.SEGMENT_MOBILE 		= testValues(int(value.SEGMENT_MOBILE),commande.SEGMENT_MOBILE);
			commande.SEGMENT_FIXE 			= testValues(int(value.SEGMENT_FIXE),commande.SEGMENT_FIXE);
			commande.SEGMENT_DATA 			= testValues(int(value.SEGMENT_DATA),commande.SEGMENT_DATA);
			commande.IDLAST_ETAT 			= testValues(int(value.IDINV_ETAT),commande.IDLAST_ETAT);
			commande.IDLAST_ACTION 			= testValues(value.IDINV_ACTIONS,commande.IDLAST_ACTION);
			commande.LIBELLE_LASTACTION 	= testValues(value.LIBELLE_ACTION,commande.LIBELLE_LASTACTION);
			commande.MONTANT_CDE 			= testValues(Number(value.MONTANT_CDE),commande.MONTANT_CDE);			
			commande.USERCREATE 			= testValues(value.USERCREATE,commande.USERCREATE);
			commande.USERID 				= testValues(Number(value.USERID),commande.USERID);			
			commande.IS_CONCERN				= testValues(Number(value.IS_CONCERN),commande.IS_CONCERN);
			commande.NUMERO_COMMANDE 		= testValues(value.NUMERO_OPERATION,commande.NUMERO_COMMANDE);
			commande.LIBELLE_COMMANDE 		= testValues(value.LIBELLE,commande.LIBELLE_COMMANDE);
			commande.REF_CLIENT1 			= testValues(value.REF_CLIENT,commande.REF_CLIENT1);
			commande.REF_CLIENT2			= testValues(value.REF_CLIENT_2,commande.REF_CLIENT2);
			commande.REF_CLIENT11			= testValues(value.REF_CLIENT,commande.REF_CLIENT11);
			commande.REF_CLIENT21			= testValues(value.REF_CLIENT_2,commande.REF_CLIENT21);
			commande.REF_OPERATEUR 			= testValues(value.REF_REVENDEUR,commande.REF_OPERATEUR);
			commande.COMMENTAIRES 			= testValues(value.COMMENTAIRES,commande.COMMENTAIRES);
			commande.LIBELLE_OPERATEUR 		= testValues(value.LIBELLE_OPERATEUR,commande.LIBELLE_OPERATEUR);
			commande.NUMERO_TRACKING 		= testValues(value.NUMERO_TRACKING,commande.NUMERO_TRACKING);
			commande.IDTRANSPORTEUR 		= testValues(value.IDTRANSPORTEUR,commande.IDTRANSPORTEUR);
			commande.IDSITELIVRAISON 		= testValues(value.IDSITE_LIVRAISON,commande.IDSITELIVRAISON);
			commande.LIBELLE_SITELIVRAISON 	= testValues(value.LIBELLE_SITELIVRAISON,commande.LIBELLE_SITELIVRAISON);	
			commande.LIBELLE_POOL 			= testValues(value.LIBELLE_POOLGESTIONNAIRE,commande.LIBELLE_POOL);
			commande.IDGESTIONNAIRE_MODIF 	= testValues(value.IDGESTIONNAIRE_MODIF,commande.IDGESTIONNAIRE_MODIF);
			commande.IDGESTIONNAIRE_CREATE 	= testValues(value.IDGESTIONNAIRE_CREATE,commande.IDGESTIONNAIRE_CREATE);
			commande.CREEE_PAR 				= testValues(value.LIBELLEGESTIONNAIRE_CREATE,commande.CREEE_PAR);
			commande.MODIFIEE_PAR			= testValues(value.LIBELLEGESTIONNAIRE_MODIF,commande.MODIFIEE_PAR);
			commande.ENCOURS 				= testValues(Number(value.EN_COURS),commande.ENCOURS);
			commande.EXPEDIE_LE 			= testValues(value.EXPEDIE_LE,commande.EXPEDIE_LE); 
			commande.PATRONYME_CONTACT 		= testValues(value.NOM_CONTACT,commande.PATRONYME_CONTACT);
			commande.IDCONTACT 				= testValues(value.IDCDE_CONTACT,commande.IDCONTACT);
			commande.LIVREE_LE 				= testValues(value.LIVREE_LE,commande.LIVREE_LE);
			commande.IDTYPE_COMMANDE 		= testValues(Number(value.IDTYPE_OPERATION),commande.IDTYPE_COMMANDE);
			commande.LIBELLE_TO 			= testValues(value.A_L_ATTENTION,commande.LIBELLE_TO);
			commande.LIBELLE_SITEFACTURATION= testValues(value.LIBELLE_SITE_FACTURATION,commande.LIBELLE_SITEFACTURATION);
			commande.IDSITEFACTURATION		= testValues(value.IDSITE_FACTURATION,commande.IDSITEFACTURATION);
			commande.IDSOCIETE 				= testValues(value.IDSOCIETE,commande.IDSOCIETE);
			commande.LIBELLE_REVENDEUR		= testValues(value.LIBELLE_REVENDEUR,commande.LIBELLE_REVENDEUR);
			commande.IDREVENDEUR			= testValues(value.IDREVENDEUR,commande.IDREVENDEUR);
			commande.IDPROFIL_EQUIPEMENT	= testValues(value.IDTYPECOMMANDE,commande.IDPROFIL_EQUIPEMENT);
			commande.LIBELLE_PROFIL			= testValues(value.TYPECOMMANDE,commande.LIBELLE_PROFIL);
			commande.DATE_COMMANDE			= testValues(value.LIVRE_LE,commande.DATE_COMMANDE);
			commande.IDGESTIONNAIRE			= testValues(CvAccessManager.getUserObject().CLIENTACCESSID,commande.IDGESTIONNAIRE);
			commande.TYPE_OPERATION			= testValues(value.TYPE_OPERATION,commande.TYPE_OPERATION);
			commande.CREEE_LE				= testValues(value.DATE_CREATE,commande.CREEE_LE);
			commande.LIBELLE_LASTETAT		= testValues(value.LIBELLE_EAT,commande.LIBELLE_LASTETAT);
			commande.MONTANT				= testValues(value.MONTANT,commande.MONTANT);
			commande.LIVRAISON_DISTRIBUTEUR = testValues(int(value.LIVRAISON_DISTRIBUTEUR),commande.LIVRAISON_DISTRIBUTEUR);
			commande.IS_LIV_DISTRIB			= testValues(Formator.formatInteger(commande.LIVRAISON_DISTRIBUTEUR),commande.IS_LIV_DISTRIB);
		}
		
		private function testValues(value1:*,value2:*):*
		{
			if(value2 > value1)
			{
				return value2
			}
			return value1;
		}

	}
}