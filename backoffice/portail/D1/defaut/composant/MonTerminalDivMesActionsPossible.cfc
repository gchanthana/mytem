<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	
	<!---  --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- CONSTRUCTEUR --->
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getDivMesActionsPossible" access="public" output="true" returnType="string" 
				displayname="" 
				hint="Génère la div contenant les actions possibles sur un terminal / template D1" >
		
		<cfargument name="idSousTete" required="false" type="numeric" default=0>
		<cfset boolIsManaged = "">
		
		
		<cfif idSousTete eq 0> <!--- si première navigation sur cette page, affiche premier terminal de la liste --->
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
							
					<!--- initialisation pour combo mon terminal / page mon terminal --->
					<cfset boolIsManaged = SESSION.queryMesTerminaux['BOOL_ISMANAGED'][currentRow] >
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset IMEI = SESSION.queryMesTerminaux['T_IMEI'][currentRow] >
					<cfset mdmIMEI = SESSION.queryMesTerminaux['ZDM_IMEI'][currentRow] >
					<cfset serial = SESSION.queryMesTerminaux['ZDM_SERIAL_NUMBER'][currentRow] >
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
					<cfbreak>
				</cfif>
			</cfloop>
			
		<cfelse>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] eq idSousTete>
					<!--- initialisation pour combo ma ligne / page accueil --->
					<cfset boolIsManaged = SESSION.queryMesTerminaux['BOOL_ISMANAGED'][currentRow] >
					<cfset idTerminal = SESSION.queryMesTerminaux['IDTERMINAL'][currentRow] >
					<cfset IMEI = SESSION.queryMesTerminaux['T_IMEI'][currentRow] >
					<cfset mdmIMEI = SESSION.queryMesTerminaux['ZDM_IMEI'][currentRow] >
					<cfset serial = SESSION.queryMesTerminaux['ZDM_SERIAL_NUMBER'][currentRow] >
					<cfset sous_tete = SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] >
					<cfset idSousTete = SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] >
					<cfset idSIM = SESSION.queryMesTerminaux['IDSIM'][currentRow] >
				</cfif>
			</cfloop>
		</cfif>
		
		
		<cfset action = '<div class="divEnteteMesActionsPossibles" style="">Mes actions possibles</div>	'>
		
		<!--- INITALISATION DES VARIABLES --->
		<cfset erreur = "">
		<cfset lockTerminal = "">
		<cfset changeCodeLock = "">
		
		<cfset btnType2 = "button">
		<cfset btnType3 = "button">
		<cfset btnType4 = "button">
		<cfset btnDisabled4 = "">
		<cfset btnDisabled5 = "">
		
		<cfset opacityPopupVerifMDP = ""><!--- opacite div MDP --->
		<cfset opacityPopupChangeCodeLock = ""><!--- opacite div de changement code verrouillage --->
		
		<cfset displayPopupPriseEnCompte = "display:none;">
		<cfset displayPopupChangeCodeLockTerminalAndroid = "display:none;">
		<cfset displayPopupChangeCodeLockTerminalIOS = "display:none;">
		<cfset displayPopupWipePARTIEL = "display:none;">
		<cfset displayPopupWipeAll = "display:none;">
		
		
		<cfif #boolIsManaged# eq 1> <!--- TERMINAL EST ENROLLÉ --->
			<cfset class = "inputActionTerminal">
			<cfset classBtnWipePartiel = "inputActionTerminal">
			<cfset btnType1 = "submit">
			<cfset disabled = "">
			<cfset disabledWipePartiel = "">
			<cfset title = 'title="Enlever le terminal de la politique de sécurité de l''entreprise"'>
			
		<cfelse> <!--- TERMINAL N'EST PAS ENROLLÉ --->
		
			<cfset class = "inputActionGrisTerminal"> <!--- boutons des actions possibles grisés --->
			<cfset classBtnWipePartiel = "inputActionGrisTerminal"> <!--- bouton du wipe partiel grisé --->
			<cfset btnType1 = "button">
			<cfset disabled = "disabled='disabled'">
			<cfset disabledWipePartiel = "disabled='disabled'">
			<cfset title = "">
		</cfif>
		
		
		<!--- SESSION QUERY INFODEVICE --->
		<cfset queryInfoFicheEquipement = #SESSION.queryDeviceInfo.DEVICEPROPERTIES#>
		<cfset queryInfoFicheLogiciel = #SESSION.queryDeviceInfo.SOFTWAREINVENTORY#>
		
		
		<!--- INITIALISATION VARIABLE DE SESSION POUR GESTION PLATEFORME DU TELEPHONE --->
		<cfset plateforme = "">
		<cfloop query="queryInfoFicheEquipement">
			<cfset currentRow = queryInfoFicheEquipement.currentrow>
			
			<cfif queryInfoFicheEquipement['NAME'][currentRow] eq "Plateforme" >
				<cfset plateforme = queryInfoFicheEquipement['VALUE'][currentRow]>
				<cfbreak>
			</cfif>
		</cfloop>
		
		
		<!--- METHODE DE GESTION DES PLATEFORMES DU TERMINAL(iOS,ANDROID,ECT) / retourne composant --->
		<!--- <cfset gestionPlateforme = createObject("component","portail.class.classData.gestionPlateformeMDM").gestionPlateformeMDM(plateforme) > --->
		
		
		<!--- DETECTION PLATEFORM DU TERMINAL(iOS, ANDROID, ECT) --->
		<cfif isDefined("SESSION.queryDeviceInfo")>
			
			<!--- PLATERFORME ANDROID --->
			<cfif plateforme eq "ANDROID">
			
				<!--- initialisation --->
				<cfset btnType1 = "button" >
				<cfset btnType2 = "button" > <!--- btn action -> changer code verrou --->
				<cfset btnType3 = "submit" > <!--- btn popup IOS -> changer code verrou -> valider mot de passe --->
				<cfset btnType4 = "submit" > <!--- btn popup Android -> changer code verrou -> valider mot de passe --->
				<cfset disabledWipePartiel = "disabled='disabled'">
				<cfset btnDisabled5 = "disabled='disabled'"> <!--- btn popup changeCodeLockAndroid disable --->
				<cfset classBtnWipePartiel = "inputActionGrisTerminal">
				<cfset lockTerminal = "codeLockTerminal();" > <!--- affiche popup verouillage --->
				<cfset changeCodeLock = "changeCodeLockTerminalAndroid();" > <!--- affiche popup changement code verou --->
				
				<!--- si verrouillage du Terminal a été soumis --->
				<cfif isDefined("FORM.lockTerminal") && FORM.lockTerminal eq "Verrouiller" > 
	
					<!--- METHODE LOCK ANDROID --->
					<cfset retourLock = createObject("component","fr.consotel.consoview.M111.MDMService").lock(IMEI,serial,mdmimei,idTerminal,FORM.codeVerrouillage,2000,"") >
					<cfset displayPopupPriseEnCompte = "display:block;">
				
				<!--- si FORM saisie du mot de passe a été soumis --->
				<cfelseif isDefined("FORM.validerSaisieMDP") && FORM.validerSaisieMDP eq "Valider" >
					
					<!--- verifie le type de connexion email/numero de ligne --->
					<cfif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 1>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPEmail(SESSION.USER.EMAIL,FORM.saisieMDPAndroid) >
					<cfelseif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 2>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPLigne(SESSION.NUMEROLIGNE,FORM.saisieMDPAndroid) >
					</cfif>
					
					<!--- verification identifiants --->
					<cfif retourCnx eq 1>
						<cfset erreur = "">
						<cfset btnDisabled4 = "disabled='disabled'">
						<cfset btnDisabled5 = ""> <!--- btn popup changeCodeLockAndroid disable --->
						<cfset opacityPopupChangeCodeLock = ""><!--- opacité div nouveau code de verouillage --->
						<cfset opacityPopupVerifMDP = "opacity:0.3;filter:alpha(opacity=30);"> <!--- opacité div mot de passe verouillage --->
						<cfset displayPopupChangeCodeLockTerminalAndroid = "display:block;">
					<cfelse>
						<cfset erreur = "Mot de passe invalide">
						<cfset displayPopupChangeCodeLockTerminalAndroid = "display:block;">
						<cfset opacityPopupVerifMDP = ""> <!--- opacité div mot de passe verouillage --->
						<cfset opacityPopupChangeCodeLock = "opacity:0.3;filter:alpha(opacity=30);"> <!--- opacité div nouveau code de verouillage --->
					</cfif>
					
				<!--- si FORM saisie du nouveau code de verrouillage a été soumis --->
				<cfelseif isDefined("FORM.changerLockTerminal") && FORM.changerLockTerminal eq "Changer" >
				
					<cfset displayPopupPriseEnCompte = "display:block;">
					<cfset displayPopupChangeCodeLockTerminalAndroid = "display:none;">
					<!--- METHODE LOCK(CHANGER CODE VERROUILLAGE) ANDROID --->
					<cfset retourLock = createObject("component","fr.consotel.consoview.M111.MDMService").lock(IMEI,serial,mdmimei,idTerminal,FORM.changeCodeLock,2000,"") >
					
				<!--- si FORM saisie du mot de passe pour WIPE ALL a été soumis --->
				<cfelseif isDefined("FORM.validerSaisieMDPWipeAll") && FORM.validerSaisieMDPWipeAll eq "Valider" >
					
					<!--- verifie le type de connexion email/numero de ligne --->
					<cfif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 1>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPEmail(SESSION.USER.EMAIL,FORM.saisieMDPWipeAll) >
					<cfelseif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 2>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPLigne(SESSION.NUMEROLIGNE,FORM.saisieMDPWipeAll) >
					</cfif>
					
					<!--- verification identifiants --->
					<cfif retourCnx eq 1>
						<cfset erreur = "">
						<cfset displayPopupWipeALL = "display:none;">
						<cfset displayPopupPriseEnCompte = "display:block;">
						
						<!--- METHODE WIPE ALL ANDROID --->
						<cfset retourLock = createObject("component","fr.consotel.consoview.M111.MDMService").wipe(IMEI,serial,mdmimei,idTerminal,false,true,"",3000,"") >
					<cfelse>
						<cfset erreur = "Mot de passe invalide">
						<cfset displayPopupWipeALL = "display:block;">
					</cfif>
				
				<!--- si FORM saisie du mot de passe pour WIPE PARTIEL a été soumis --->
				<cfelseif isDefined("FORM.validerSaisieMDPWipePARTIEL") && FORM.validerSaisieMDPWipePARTIEL eq "Valider" >
					
					<!--- verifie le type de connexion email/numero de ligne --->
					<cfif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 1>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPEmail(SESSION.USER.EMAIL,FORM.saisieMDPWipePARTIEL) >
					<cfelseif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 2>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPLigne(SESSION.NUMEROLIGNE,FORM.saisieMDPWipePARTIEL) >
					</cfif>
					
					<!--- verification identifiants --->
					<cfif retourCnx eq 1>
						<cfset erreur = "">
						<cfset displayPopupWipePARTIEL = "display:none;">
						<cfset displayPopupPriseEnCompte = "display:block;">
						<!--- METHODE WIPE PARTIEL ANDROID --->
						<cfset retourLock = createObject("component","fr.consotel.consoview.M111.MDMService").wipe(IMEI,serial,mdmimei,idTerminal,false,false,"",3000,"") >
					<cfelse>
						<cfset erreur = "Mot de passe invalide">
						<cfset displayPopupWipePARTIEL = "display:block;">
					</cfif>
				
				<!--- si FORM pour REVOKE a été soumis --->
				<cfelseif isDefined("FORM.revokTerminal") && FORM.revokTerminal eq "Revoquer mon terminal" >
					
					<!--- METHODE REVOKE ANDROID --->
					<cfset retourRevok = createObject("component","fr.consotel.consoview.M111.MDMService").revoke(IMEI,serial,mdmimei,idTerminal) >
					<cfset displayPopupPriseEnCompte = "display:block;">
					
				</cfif>
			
			
			<!--- --------------- --->
			<!--- PLATERFORME IOS --->
			<!--- --------------- --->
			<cfelseif plateforme eq "iOS">
				
				
				<!--- initialisation --->
				<cfset changeCodeLock = "changeCodeLockTerminalIOS();" >
				<cfset lockTerminal = "if(confirmLockIOS()) return true; else return false;">
				
				<!--- si FORM verouuillage Terminal a été soumis --->
				<cfif isDefined("FORM.btnLockTerminal") && FORM.btnLockTerminal eq "Verrouiller mon terminal" >
					<!--- METHODE LOCK IOS --->
					<cfset retourLock = createObject("component","fr.consotel.consoview.M111.MDMService").lock(IMEI,serial,mdmimei,idTerminal,2000,"") >
					<cfset displayPopupPriseEnCompte = "display:block;">
				
				<!--- si FORM saisie du mot de passe a été soumis --->
				<cfelseif isDefined("FORM.validerSaisieMDPIOS") && FORM.validerSaisieMDPIOS eq "Valider" >
					<!--- verifie le type de connexion email/numero de ligne --->
					<cfif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 1>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPEmail(SESSION.USER.EMAIL,FORM.saisieMDPIOS) >
					<cfelseif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 2>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPLigne(SESSION.NUMEROLIGNE,FORM.saisieMDPIOS) >
					</cfif>
					
					<!--- verification identifiants --->
					<cfif retourCnx eq 1>
						<!--- METHODE LOCK (CHANGER CODE DE VERROUILLAGE) IOS --->
						<cfset retourLock = createObject("component","fr.consotel.consoview.M111.MDMService").lock(IMEI,serial,mdmimei,idTerminal,2000,"") >
						<cfset displayPopupPriseEnCompte = "display:block;">
						<cfset displayPopupChangeCodeLockTerminalIOS = "display:none;">
					<cfelse>
						<cfset erreur = "mot de passe invalide">
						<cfset displayPopupChangeCodeLockTerminalIOS = "display:block;">				
					</cfif>
				
				<!--- si FORM saisie du mot de passe pour WIPE ALL a été soumis --->
				<cfelseif isDefined("FORM.validerSaisieMDPWipeAll") && FORM.validerSaisieMDPWipeAll eq "Valider" >
					
					<!--- verifie le type de connexion email/numero de ligne --->
					<cfif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 1>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPEmail(SESSION.USER.EMAIL,FORM.saisieMDPWipeAll) >
					<cfelseif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 2>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPLigne(SESSION.NUMEROLIGNE,FORM.saisieMDPWipeAll) >
					</cfif>
					
					<!--- verification identifiants --->
					<cfif retourCnx eq 1>
						<cfset erreur = "">
						<cfset displayPopupWipeALL = "display:none;">
						<cfset displayPopupPriseEnCompte = "display:block;">
						<!--- METHODE WIPE ALL IOS --->
						<cfset retourLock = createObject("component","fr.consotel.consoview.M111.MDMService").wipe(IMEI,serial,mdmimei,idTerminal,false,true,"",3000,"") >
					<cfelse>
						<cfset erreur = "Mot de passe invalide">
						<cfset displayPopupWipeALL = "display:block;">
					</cfif>
				
				<!--- si FORM saisie du mot de passe pour WIPE PARTIEL a été soumis --->
				<cfelseif isDefined("FORM.validerSaisieMDPWipePARTIEL") && FORM.validerSaisieMDPWipePARTIEL eq "Valider" >
					
					<!--- verifie le type de connexion email/numero de ligne --->
					<cfif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 1>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPEmail(SESSION.USER.EMAIL,FORM.saisieMDPWipePARTIEL) >
					<cfelseif isDefined("SESSION.TYPECNX") && SESSION.TYPECNX eq 2>
						<cfset retourCnx = createObject("component","portail.class.classData.AccessManagerService").verifMDPLigne(SESSION.NUMEROLIGNE,FORM.saisieMDPWipePARTIEL) >
					</cfif>
					
					<!--- verification identifiants --->
					<cfif retourCnx eq 1>
						<cfset erreur = "">
						<cfset displayPopupWipePARTIEL = "display:none;">
						<cfset displayPopupPriseEnCompte = "display:block;">
						<!--- METHODE WIPE PARTIEL IOS --->
						<cfset retourWipePartiel = createObject("component","fr.consotel.consoview.M111.MDMService").wipe(IMEI,serial,mdmimei,idTerminal,false,false,"",3000,"") >
					<cfelse>
						<cfset erreur = "Mot de passe invalide">
						<cfset displayPopupWipePARTIEL = "display:block;">
					</cfif>
				
				<!--- si FORM pour REVOKE a été soumis --->
				<cfelseif isDefined("FORM.revokTerminal") && FORM.revokTerminal eq "Revoquer mon terminal" >
					
					<!--- METHODE REVOKE IOS --->
					<cfset retourRevok = createObject("component","fr.consotel.consoview.M111.MDMService").revoke(IMEI,serial,mdmimei,idTerminal) >
					<cfset displayPopupPriseEnCompte = "display:block;">
				
				</cfif>
				
			</cfif>
		</cfif>
		
		
		<!--- MES BOUTONS ACTIONS --->
		<cfset action &= '
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input  type="#btnType1#" #disabled# name="btnLockTerminal" value="Verrouiller mon terminal" class="#class#" onclick="#lockTerminal#">
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType2#" #disabled# name="btnChangeLockTerminal" value="Changer le code verrou" class="#class#" onclick="#changeCodeLock#">
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType2#" #disabled# name="" onclick="popupWipeAll()" value="Effacer TOUTES les données" class="#class#" >
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="#btnType2#" #disabledWipePartiel# name="" onclick="popupWipePartiel()" value="Effacer les données Entreprise" class="#classBtnWipePartiel#" >
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="submit" #title# #disabled# name="revokTerminal" id="revokTerminal" onclick="if(confirmRevok()) return true; else return false;" value="Revoquer mon terminal" class="#class#" >
						</form>
					</div>
					<div align="center" class="divBtnActionPoss" style="">
						<form method="post" action="">
							<input type="button" name="" onclick="popupEmail(''ombreee2'')" value="Contacter mon gestionnaire" class="btnContacterGestionnaireTerminal" >
						</form>
					</div>'
		>
		
		
		<!--- ------ --->
		<!--- POPUPS --->
		<!--- ------ --->
		
		<!--- POPUP CODE VERROUILLAGE TERMINAL ANDROID --->
		<cfset action &= '
			<div id="popupCodeLockTerminal" class="divCodeLockTerminal" style="">
				<form action="" method="post" name="codeLock" id="codeLock" >
					<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
						<tr>
							<td align="center" style="font-weight:bold;color:##009FE3;">Verrouiller mon terminal</td>
							<td align="right"><a style="cursor:pointer" onclick="closePopup(''popupCodeLockTerminal'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" align="center" style="font-weight:bold;font-size:10px;color:##009FE3;">Veuillez saisir votre code de verrouillage</td>
						</tr>
						<tr height="20px;">
								<td colspan="2" align="center"><span style="color:red;display:none;" id="erreurLockTerminal">Saisissez un code de verrouillage</span></td>
							</tr>
						<tr>
							<td colspan="2" align="center" style="font-size:12px;"><input type="text" value="" id="codeVerrouillage" name="codeVerrouillage"/></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr style="height:0px;">
			              <td colspan="2" align="center" style="">
				              <input type="submit" name="lockTerminal" value="Verrouiller" onclick="if(verifSaisieVerrouillage()) document.forms[''codeLock''].submit(); else return false;" style="" >
				              <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
						  </td>
			            </tr>
					</table>
				</form>
			</div>
		'>
		
		<!--- POPUP CHANGE CODE VERROUILLAGE TERMINAL ANDROID --->
		<cfset action &= '
			<div id="popupChangeCodeLockTerminalAndroid" class="divChangeCodeLockTerminal" style="#displayPopupChangeCodeLockTerminalAndroid#">
				<div id="divPopupClose" style="" >
					<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
						<tr>
							<td align="center" style="font-weight:bold;color:##009FE3;">Changer le code de verrouillage</td>
							<td align="right"><a style="cursor:pointer" onclick="closePopup(''popupChangeCodeLockTerminalAndroid'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
				</div>
				<div id="divOpacityPopupMDPChangeCodeLock" style="#opacityPopupVerifMDP#" > <!--- class="#opacityPopupVerifMDP#" --->
					<form action="" method="post" name="changeLockAndroid" id="changeLockAndroid">
						<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
							<tr>
								<td colspan="2" align="center" style="font-weight:bold;font-size:10px;color:##009FE3;">Mot de passe Portail Utilisateur</td>
							</tr>
							<tr height="20px;">
								<td align="center" style="color:red;" colspan="2">
									<span style="display:none;" id="erreurMDPChangeLockTerminalAndroid">Saisir un mot de passe</span>
									<span style="" id="erreurBISMDPChangeLockTerminalAndroid">#erreur#</span>
								</td>
							</tr>
							<tr>
								<td align="center" style="font-size:12px;"><input type="password" #btnDisabled4# value="" id="saisieMDPAndroid" name="saisieMDPAndroid"/></td>
								<td align="center" style="">
						            <input type="#btnType3#"  #btnDisabled4# value="Valider" id="validerSaisieMDP" name="validerSaisieMDP" onclick="if(verifSaisieMDPVerouillageAndroid()) document.forms[''changeVerouillageAndroid''].submit(); else return false;" style="">
						            <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
							    </td>
							</tr>
						</table>
					</form>
				</div>
				<div id="divOpacityPopupChangeCodeLock" style="#opacityPopupChangeCodeLock#"> <!--- class="#opacityPopupChangeCodeLock#" --->
					<form action="" method="post" id="changeCodeVerouillageAndroid" name="changeCodeVerouillageAndroid">
						<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
							<tr>
								<td colspan="2" align="center" style="font-weight:bold;font-size:10px;color:##009FE3;">Nouveau code de verouillage</td>
							</tr>
							<tr height="20px;">
								<td align="center" style="color:red;" colspan="2">
									<span style="display:none;" id="erreurMDPChangeCodeLockTerminalAndroid">Saisir un code de verrouillage</span>
								</td>
							</tr>
							<tr>
								<td align="center" style="font-size:12px;"><input type="text" #btnDisabled5# value="" id="changeCodeLock" name="changeCodeLock"/></td>
								<td align="center" style="">
						            <input type="#btnType4#" #btnDisabled5# value="Changer" id="changerLockTerminal" name="changerLockTerminal" onclick="if(verifSaisieChangeCodeVerouillageAndroid()) document.forms[''changeCodeLockAndroid''].submit(); else return false;" style="">
						            <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
							    </td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		'>
		
		
		
		<!--- POPUP CHANGE CODE VERROUILLAGE TERMINAL IOS --->
		<cfset action &= '
			<div id="popupChangeCodeLockTerminalIOS" class="divChangeCodeLockTerminal" style="#displayPopupChangeCodeLockTerminalIOS#">
				<div>
					<form action="" method="post" name="changeVerouillageIOS" id="changeVerouillageIOS">
						<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
							<tr>
								<td align="right" style="font-weight:bold;color:##009FE3;">Changer le code de verrouillage</td>
								<td align="right"><a style="cursor:pointer" onclick="closePopup(''popupChangeCodeLockTerminalIOS'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" align="center" style="font-weight:bold;font-size:10px;color:##009FE3;">Mot de passe Portail Utilisateur</td>
							</tr>
							<tr height="20px;">
								<td align="center" style="color:red;" colspan="2">
									<span style="display:none;" id="erreurMDPChangeLockTerminalIOS">Saisir un mot de passe</span>
									<span style="" id="erreurBISMDPChangeLockTerminalIOS">#erreur#</span>
								</td>
							</tr>
							<tr>
								<td align="center" style="font-size:12px;"><input type="password" value="" id="saisieMDPIOS" name="saisieMDPIOS"/></td>
								<td align="center" style="">
						            <input type="submit" value="Valider" name="validerSaisieMDPIOS" onclick="if(verifSaisieChangeVerouillageIOS()) document.forms[''changeVerouillageIOS''].submit(); else return false;" style="">
						            <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
							    </td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		'>
		
		
		
		<!--- POPUP VALIDATION VERROUILLAGE TERMINAL --->	
		<cfset action &= '
			<div id="popupPriseEnCompte" class="divPriseEnCompte" style="#displayPopupPriseEnCompte#">
				<form action="" method="post">
					<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
						<tr>
							<td align="right"><a style="cursor:pointer" onclick="closePopup(''popupPriseEnCompte'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center" style="font-weight:bold;font-size:12px;color:##009FE3;">Votre demande a bien été prise en compte.</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr style="height:0px;">
			              <td align="center" style="">
				              <input type="button" value="Fermer" name="popupPriseEnCompte" onclick="closePopup(''popupPriseEnCompte'')" style="" >
				              <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
						  </td>
			            </tr>
					</table>
				</form>
			</div>
		'>

		
		<!--- POPUP VALIDATION WIPE TOTAL --->	
		<cfset action &= '
			<div id="popupWipeAll" class="divPopupWipeAll" style="#displayPopupWipeAll#">
				<div>
					<form action="" method="post" name="wipeAll" id="wipeAll">
						<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
							<tr>
								<td align="right" style="font-weight:bold;color:##009FE3;">Effacer TOUTES les données</td>
								<td align="right"><a style="cursor:pointer" onclick="closePopup(''popupWipeAll'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" align="center" style="font-weight:bold;font-size:10px;color:##009FE3;">Mot de passe Portail Utilisateur</td>
							</tr>
							<tr height="20px;">
								<td align="center" style="color:red;" colspan="2">
									<span style="display:none;" id="erreurMDPWipeAll">Saisir un mot de passe</span>
									<span style="" id="erreurBISMDPWipeAll">#erreur#</span>
								</td>
							</tr>
							<tr>
								<td align="center" style="font-size:12px;"><input type="password" value="" id="saisieMDPWipeAll" name="saisieMDPWipeAll"/></td>
								<td align="center" style="">
						            <input type="submit" value="Valider" name="validerSaisieMDPWipeAll" onclick="if(verifSaisieWipeAll()) document.forms[''wipeAll''].submit(); else return false;" style="">
						            <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
							    </td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		'>
		
		
		<!--- POPUP VALIDATION WIPE partiel --->	
		<cfset action &= '
			<div id="popupWipePARTIEL" class="divPopupWipePARTIEL" style="#displayPopupWipePARTIEL#">
				<div>
					<form action="" method="post" name="wipePARTIEL" id="wipePARTIEL">
						<table class="tableEmail" cellspading="0" cellspacing="0" width="100%">
							<tr>
								<td align="right" style="font-weight:bold;color:##009FE3;">Effacer les données Entreprise</td>
								<td align="right"><a style="cursor:pointer" onclick="closePopup(''popupWipePARTIEL'')"><img src="img/close.png" title="Fermer" alt="Fermer"></a></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" align="center" style="font-weight:bold;font-size:10px;color:##009FE3;">Mot de passe Portail Utilisateur</td>
							</tr>
							<tr height="20px;">
								<td align="center" style="color:red;" colspan="2">
									<span style="display:none;" id="erreurMDPWipePARTIEL">Saisir un mot de passe</span>
									<span style="" id="erreurBISMDPWipePARTIEL">#erreur#</span>
								</td>
							</tr>
							<tr>
								<td align="center" style="font-size:12px;"><input type="password" value="" id="saisieMDPWipePARTIEL" name="saisieMDPWipePARTIEL"/></td>
								<td align="center" style="">
						            <input type="submit" value="Valider" name="validerSaisieMDPWipePARTIEL" onclick="if(verifSaisieWipePARTIEL()) document.forms[''wipePARTIEL''].submit(); else return false;" style="">
						            <!--- width:1px;height:1px;background-color:transparent;background:none;border:0; --->
							    </td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		'>
		
		 
		<cfreturn action >
	</cffunction>
	
</cfcomponent>