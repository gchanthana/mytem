package gestioncommande.events
{
	import flash.events.Event;
	
	import gestioncommande.entity.ViewStackObject;
	
	public class ViewStackEvent extends Event
	{
		
		public static const CREATION_COMPLETE					:String = "CREATION_COMPLETE";
		public static const CHANGEVIEWSTACK						:String = "CHANGEVIEWSTACK";
		public static const CLOSETHIS							:String = "CLOSETHIS";
		public static const ANNUL_COMMANDE						:String = "ANNUL_COMMANDE";
		
		public static const VS_VIEW_LISTE						:String = "VS_VIEW_LISTE";
		
		public static const VS_VIEW_HISTORIQUE					:String = "VIEW_HISTORIQUE";
		public static const VS_VIEW_LISTECMDES					:String = "VIEW_LISTECMDES";
		public static const VS_VIEW_MODIFCMDES					:String = "VS_VIEW_MODIFCMDES";
		
		public static const BUILD_COMMANDE						:String = "BUILD_COMMANDE";
		public static const COMMANDE_BUILT						:String = "COMMANDE_BUILT";
		public static const GIVEME_OPTIONS_FOR_ERASE			:String = "GIVEME_OPTIONS_FOR_ERASE";
		public static const SEND_OPTIONS_FOR_ERASE				:String = "SEND_OPTIONS_FOR_ERASE";

		public static const ETAPE_RESILIATION					:String = "ETAPE_RESILIATION";
		public static const RECUPERATION_OPTION_TO_ERASE		:String = "RECUPERATION_OPTION_TO_ERASE";
		public static const ETAPE_NEWCOMMANDE					:String = "ETAPE_NEWCOMMANDE";
		public static const ETAPE_NEWCOMMANDE_NOPOOL			:String = "ETAPE_NEWCOMMANDE_NOPOOL";

		public static const BUTTON_VALIDE_ENABLED				:String = "BUTTON_VALIDE_ENABLED";

		public static const BUTTON_WIZZARD_ENABLED				:String = "BUTTON_WIZZARD_ENABLED";
		public static const BUTTON_WIZZARD_DISABLED				:String = "BUTTON_WIZZARD_DISABLED";
		
		public var vsObject										:ViewStackObject = null;
		
		public function ViewStackEvent(type:String, bubbles:Boolean = false, vsObj:ViewStackObject = null, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.vsObject	= vsObj;
		}

		override public function clone():Event
		{
			return new ViewStackEvent(type, bubbles, vsObject, cancelable);
		}

	}
}