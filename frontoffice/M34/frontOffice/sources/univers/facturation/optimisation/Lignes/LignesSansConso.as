package univers.facturation.optimisation.Lignes {
	import composants.tb.periode.AMonth;
	import mx.events.FlexEvent;
	import composants.util.DateFunction;
	import flash.net.URLRequest;
	import composants.tb.periode.PeriodeEvent;
	import univers.facturation.optimisation.AbstractReport;
	import flash.net.URLVariables;
	import mx.controls.Menu;
	import mx.events.MenuEvent;
	import flash.events.MouseEvent;
	import mx.core.IFactory;
	import composants.access.perimetre.PerimetreTreeItemRenderer;
	import mx.core.ClassFactory;
	import composants.access.PerimetreTreeEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class LignesSansConso extends LignesSansConsoIHM {
		public static const RP_NO_CONSO_ALL_NDI:String = ""; // Toutes les lignes
		public static const RP_NO_CONSO_ALL_NDI_LABEL:String = "Toutes les lignes";
		public static const RP_NO_CONSO_FUNCT_NDI:String = "avec_fonction";
		public static const RP_NO_CONSO_FUNCT_NDI_LABEL:String = "Lignes avec fonction";
		public static const RP_NO_CONSO_NO_FUNCT_NDI:String = "sans_fonction";
		public static const RP_NO_CONSO_NO_FUNCT_NDI_LABEL:String = "Lignes sans fonction";
		
		private var ndiRestrictOptions:Array;
		
		public function LignesSansConso() {
			super();
			reportType = "NdiSansConso";
			selectorThumbCount = 2; // 2 Mois à choisir
			formatList = new Array(2);
			
			var xlsObj:Object = new Object();
			xlsObj.label = AbstractReport.XLS_FORMAT_LABEL;
			xlsObj.value = AbstractReport.XLS_FORMAT_VALUE;
			
			var csvObj:Object = new Object();
			csvObj.label = AbstractReport.CSV_FORMAT_LABEL;
			csvObj.value = AbstractReport.CSV_FORMAT_VALUE;
						
			xlsObj.enabled = csvObj.enabled = true; 
			formatList[0] = xlsObj;
			formatList[1] = csvObj;
			
			// Liste des options sur les lignes
			var allObj:Object = new Object();
			var functionObj:Object = new Object();
			var noFunctionObj:Object = new Object();
			
			allObj.label = LignesSansConso.RP_NO_CONSO_ALL_NDI_LABEL;
			allObj.value = LignesSansConso.RP_NO_CONSO_ALL_NDI;
			
			functionObj.label = LignesSansConso.RP_NO_CONSO_FUNCT_NDI_LABEL;
			functionObj.value = LignesSansConso.RP_NO_CONSO_FUNCT_NDI;
			
			noFunctionObj.label = LignesSansConso.RP_NO_CONSO_NO_FUNCT_NDI_LABEL;
			noFunctionObj.value = LignesSansConso.RP_NO_CONSO_NO_FUNCT_NDI;
			/* ndiRestrictOptions = new Array(3);
			ndiRestrictOptions[0] = allObj;
			ndiRestrictOptions[1] = functionObj;
			ndiRestrictOptions[2] = noFunctionObj; */
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}

		public override function getReportName():String {
			return "Lignes sans consommations";
		}
		
		protected override function initIHM(event:FlexEvent):void {
			super.initIHM(null);
			//	cmbLignesRestrict.dataProvider = ndiRestrictOptions;
			//	cmbLignesRestrict.selectedItem = cmbLignesRestrict.dataProvider[0];
		}
		
		protected override function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			super.getReportUrlRequest(secureBool);
			//urlRequestPath.data.FONCTION_BOOL = cmbLignesRestrict.selectedItem.value;
			//urlRequestPath.data.CONSO_MAX = parseInt(txtConsoMax.text,10);
			
			if(isNaN(urlRequestPath.data.CONSO_MAX)) {
				
				//urlRequestPath.data.CONSO_MAX = 0;
				//txtConsoMax.text = "0";
			}
			return urlRequestPath;
		}
	}
}
