package listedesfactures.entity
{	
	/**
	 * <b>Classe <code>CriteresRechercheVo</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class CriteresRechercheVo
	{	
		
		/**
		 * <b>Constructeur <code>CriteresRechercheVo()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe CriteresRechercheVo.
		 * </pre></p>
		 */
		public function CriteresRechercheVo()
		{	
		}
		
		public var typePerimetre:String;
		public var dateDebut:Date;
		public var dateFin:Date;
		public var operateurId:Number;
		public var cle:String;
		public var tri:String;
		public var offset:Number = 1;
		public var limit:Number = 21;
		
		/**
		 * <b>Fonction <code>getParameter()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Get de CriteresRechercheVo
		 * </pre></p>
		 *
		 */
		public function getParameter():Object
		{
			return this;
		}
	}
}