package gestionparc.ihm.fiches.listedispo
{
	import flash.events.Event;

	import gestionparc.entity.AbstractVue;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.liste.ListeService;

	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;


	public class DispoLigne extends DispoLigneIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/Ligne.png")]
		private var adrImg:Class;
		
		private var serv:ListeService=new ListeService();

		public function DispoLigne()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}

		private function initIHM(evt:FlexEvent):void
		{
			initData();
			initListeners();

			/*rbFixe.selected=false;
			rbMobile.selected=true;
			
			if (SpecificationVO.getInstance().elementDataGridSelected.IDTYPE_LIGNE == )
			{
				rbMobile.selected=true;
			}
			else
			{
				rbFixe.selected=true;
			}*/

			txtFiltre.addEventListener(Event.CHANGE, txtFiltreChangeHandler);
			txtFiltre.setFocus();
		}

		private function initListeners():void
		{
			rbMobile.addEventListener(Event.CHANGE, rbMobileHandler);
			rbFixe.addEventListener(Event.CHANGE, rbFixeHandler);
		}

		private function rbMobileHandler(ev:Event):void
		{
			serv.myDatas.dataListeligne.filterFunction=filtrerMobile;
			serv.myDatas.dataListeligne.refresh();
			txtFiltre.setFocus();
			resetSelection();
		}

		private function rbFixeHandler(ev:Event):void
		{
			serv.myDatas.dataListeligne.filterFunction=filtrerFixe;
			serv.myDatas.dataListeligne.refresh();
			txtFiltre.setFocus();
			resetSelection();
		}

		private function resetSelection():void
		{
			dgListeLigne.selectedIndex=-1;
			dispatchEvent(new gestionparcEvent(gestionparcEvent.REFRESH_LIBELLESELECTION));
		}

		private function filtrerFixe(item:Object):Boolean
		{
			if (item.ISLIGNEFIXE == 1)
				return true
			else
				return false;
		}

		private function filtrerMobile(item:Object):Boolean
		{
			if (item.ISLIGNEMOBILE == 1)
				return true
			else
				return false;
		}

		public function getSelection():Object
		{
			if (dgListeLigne.selectedIndex != -1)
			{
				return dgListeLigne.selectedItem;
			}
			else
				return null;
		}

		public function getIDSelection():int
		{
			if (dgListeLigne.selectedIndex != -1)
				return dgListeLigne.selectedItem.IDSOUS_TETE;
			else
				return -1;
		}

		public function getDataGrid():DataGrid
		{
			return dgListeLigne;
		}

		public function getLibelleType():String
		{
			return ResourceManager.getInstance().getString('M111', 'Liste_des_lignes_disponibles');
		}

		public function getSourceIcone():Class
		{
			return adrImg;
		}

		public function getLibelleSelection():String
		{
			if (dgListeLigne.selectedIndex != -1)
			{
				return dgListeLigne.selectedItem.LIGNE;
			}
			else
			{
				return ResourceManager.getInstance().getString('M111', 'Aucune_s_lection');
			}
		}

		private function initData():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.REFRESH_DATALISTE_LIGNE, updateDG);
			serv.ListeLigne();
		}

		private function updateDG(re:gestionparcEvent):void
		{
			dgListeLigne.dataProvider=serv.myDatas.dataListeligne;
			
			/*if (SpecificationVO.getInstance().ViewObject.TYPE == AbstractVue.MOBILE)
			{
				rbMobileHandler(null);
			}
			else
			{
				rbFixeHandler(null);
			}*/
		}

		private function txtFiltreChangeHandler(ev:Event):void
		{
			try
			{
				serv.myDatas.dataListeligne.filterFunction=filtrerLeGrid;
				serv.myDatas.dataListeligne.refresh();
			}
			catch (e:Error)
			{
			}
		}

		private function filtrerLeGrid(item:Object):Boolean
		{
			if ((String(item.LIGNE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
	}
}
