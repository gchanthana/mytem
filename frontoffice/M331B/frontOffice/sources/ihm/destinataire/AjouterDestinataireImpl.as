package ihm.destinataire
{
	import event.destinataire.DestinataireEvent;
	
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.validators.EmailValidator;
	import mx.validators.Validator;
	
	import service.destinataire.ajoutermodifierdestinataire.AjouterModifierDestinataireService;
	import service.destinataire.listedestinataires.ListeDestinatairesService;
	
	import vo.destinataire.Destinataire;
	
	public class AjouterDestinataireImpl extends TitleWindow
	{
		
		private var destinataireService:AjouterModifierDestinataireService;
		private var _destinataire:Destinataire;
		
		[Bindable]public var listeDestinatairesService:ListeDestinatairesService;
	    [Bindable]public var email:TextInput;
		[Bindable]public var nom:TextInput;
		[Bindable]public var prenom:TextInput;

		public var validateur_mail:EmailValidator;
		[Bindable]public var btn_ajouter:Button;
		public var btn_fermer:Button;
		
		public function AjouterDestinataireImpl()
		{
			super();		
			destinataire = new Destinataire();
		}
		
		[Bindable]
		public function get destinataire():Destinataire
		{
			return _destinataire;
		}

		public function set destinataire(value:Destinataire):void
		{
			if(value != null)
			{
				_destinataire = value;		
			}
		}

		/**
		 * lors de la creation complete on ajoute les Event Listeners sur les boutons 
		*/
		protected function ajouterdestinataireimpl1_creationCompleteHandler(event:FlexEvent):void
		{
			btn_ajouter.addEventListener(MouseEvent.CLICK,btn_ajouter_clickHandler);
			btn_fermer.addEventListener(MouseEvent.CLICK, btn_fermer_clickHandler);
		}
		
		/**
		 *  validateAll : valide si tous les validateurs sont validés  ,
		 *	cette methodes prends les id des validateurs comme paramètres sous form d'un tableau
		 */
		
		public function validateMail():void
		{
			var validationResult:Array=Validator.validateAll([validateur_mail]);
			
			if(validationResult.length ==0) // la methode validateAll renvoi un tableau vide si tout est ok
			{
				btn_ajouter.enabled=true;
				btn_ajouter.useHandCursor=true;
				btn_ajouter.buttonMode=true;
			}
			else
			{
				btn_ajouter.enabled=false;
				btn_ajouter.useHandCursor=false;
				btn_ajouter.buttonMode=false;
			}
		}
		
		/**
		 *  permet d'ajouter et modifier un destinataire en appelant le service
		 */		
		protected function btn_ajouter_clickHandler(event:MouseEvent):void
		{	
			destinataire.nom=nom.text;
			destinataire.prenom=prenom.text;
			destinataire.mail=email.text;
			
			destinataireService=new AjouterModifierDestinataireService();
			destinataireService.ajouterDestinataire(destinataire);
			destinataireService.model.addEventListener(DestinataireEvent.USER_LISTE_DESTINATRAIRE_EVENT,ajouterDistinataireHandler);	
		}
		
		/**
		 * fermer la popup 
		 * @param event
		 */		
		private function btn_fermer_clickHandler(event:MouseEvent):void
		{
			closePopup(null);
		}
		
		/**
		 * fermer le popup
		 */		
		protected function closePopup(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/**
		 * mettre à jour la liste des destinataires après l'ajout  
		*/
		private function ajouterDistinataireHandler(event:DestinataireEvent):void
		{
			closePopup(null);
			dispatchEvent(new DestinataireEvent(DestinataireEvent.USER_LISTE_DESTINATRAIRE_EVENT)); // mettre a jour la liste des destinataires 
			dispatchEvent(new DestinataireEvent(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT)); // mettre a jour la liste  des destinataires associés au noeud	
		}
		
	}
}