package composants.tb.ligne
{
	import composants.tb.controls.TextInputLabeled;
	import composants.tb.ligne.entity.FicheProduitVO;
	import composants.tb.ligne.entity.FiltreVO;
	import composants.tb.ligne.entity.TotalVO;
	import composants.tb.ligne.event.FicheEvent;
	import composants.tb.ligne.services.FicheProduitService;
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.utils.ObjectUtil;

	[Bindable]
	public class FicheProduitImpl extends VBox
	{
		private var _ligne : Number;
		private var _ficheProduit:ArrayCollection;
		
		private var _total:TotalVO;
		public var volumeTotalAffiche:String = "";
		public var montantTotalAffiche:String = "";
		public var qteTotalAffiche:String = "";
		public var nbAppelotalAffiche:String = "";
		
		private var _filtre:FiltreVO;
		
		public var myGrid:DataGrid;
		public var txtFiltreInventaire:TextInputLabeled;
		public var abo:CheckBox;
		public var conso:CheckBox;
		
		private var serviceFicheProduit : FicheProduitService = new FicheProduitService();
		
		public function FicheProduitImpl()
		{
		}

		public function get filtre():FiltreVO
		{
			return _filtre;
		}

		public function set filtre(value:FiltreVO):void
		{
			_filtre = value;
		}

		public function get total():TotalVO
		{
			return _total;
		}

		public function set total(value:TotalVO):void
		{
			_total = value;
		}

		public function get ficheProduit():ArrayCollection
		{
			return _ficheProduit;
		}

		public function set ficheProduit(value:ArrayCollection):void
		{
			_ficheProduit = value;
		}

		public function get ligne():Number
		{
			return _ligne;
		}

		public function set ligne(value:Number):void
		{
			_ligne = value;
		}
		
		public function init():void
		{
			serviceFicheProduit.getFicheProduit(ligne);
			serviceFicheProduit.model.addEventListener(FicheEvent.FICHEPRODUIT,ficheProduitHandler);
		}
		
		private function ficheProduitHandler(ev:FicheEvent):void
		{
			filtre = new FiltreVO();
			ficheProduit = serviceFicheProduit.model.ficheProduit;
			getTotal(true);
		}
		
		private function getTotal(value:Boolean):void
		{
			
			total = new TotalVO();
			
			var tab:ArrayCollection = new ArrayCollection();
			
			if(value){tab=ficheProduit}else{tab=(myGrid.dataProvider as ArrayCollection);}
			
			for(var i:int=0;i<tab.length;i++)
			{
				total.QUANTITE += (tab[i] as FicheProduitVO).QUANTITE;
				total.NBAPPEL += (tab[i] as FicheProduitVO).NBAPPEL;
				total.VOLUME += (tab[i] as FicheProduitVO).VOLUME;
				total.MONTANT += (tab[i] as FicheProduitVO).MONTANT;
			}
			
			montantTotalAffiche = moduleTableauDeBordE0IHM.formatNumberCurrency(total.MONTANT, 2);
			volumeTotalAffiche = ConsoviewFormatter.formatNumber(Number(total.VOLUME), 2);
			qteTotalAffiche = ConsoviewFormatter.formatNumber(total.QUANTITE, 2);
			nbAppelotalAffiche = ConsoviewFormatter.formatNumber(total.NBAPPEL, 2);
			
		}
		
		public function filtreProduit(event:Event):void
		{
			filtre.text = txtFiltreInventaire.text;
			filtre.abonnement = (abo.selected)? 1 : 0;
			filtre.consommation = (conso.selected)? 1 : 0;
			filtreProduitFactureChangeHandler(event);
		}
		
		private function filtreProduitFactureChangeHandler(event:Event):void
		{
			if(myGrid.dataProvider != null){
				(myGrid.dataProvider as ArrayCollection).filterFunction = getFiltre;
				(myGrid.dataProvider as ArrayCollection).refresh();
				getTotal(false);
			}
		}
		
		private function getFiltre(produit:FicheProduitVO):Boolean
		{
			var result:Boolean = ((txtFiltre(produit))&&(typeFiltre(produit)));
			
			return result;
		}
		
		private function txtFiltre(produit:FicheProduitVO):Boolean
		{
			
			if (
				((produit.THEME) && (produit.THEME.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1))
				||
				((produit.PRODUIT) && (produit.PRODUIT.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1))
				||
				((produit.FACTURE) && (produit.FACTURE.toLowerCase().search(txtFiltreInventaire.text.toLowerCase()) != -1))
			)
				return true;
			else
				return false;
			
		}
		
		private function typeFiltre(produit:FicheProduitVO):Boolean
		{
			
			if (
				((filtre.abonnement==1) && (produit.IDTYPE ==1))
				||
				((filtre.consommation==1) && (produit.IDTYPE == 2))
			)
				return true;
			else
				return false;
			
		}
		
		protected function formateQTE(item:Object, column:DataGridColumn):String
		{
			if((item as FicheProduitVO).TYPE == 'Consommations')
			{
				return '-';
			}
			else
			{
				if (item != null)
					return ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
				else
					return "-";
			}
		}
				
		protected function formateEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return moduleTableauDeBordE0IHM.formatNumberCurrency(item[column.dataField], 2);
			else
				return "-";
		}
		
		protected function formateNumber(item:Object, column:DataGridColumn):String
		{
			if (item != null)
				return ConsoviewFormatter.formatNumber(Number(item[column.dataField]), 2);
			else
				return "-";
		}
		
		protected function sortProduit(itemA:FicheProduitVO, itemB:FicheProduitVO):int
		{
			return ObjectUtil.stringCompare(itemA.PRODUIT, itemB.PRODUIT);
		}
		
		protected function sortProduitTheme(itemA:FicheProduitVO, itemB:FicheProduitVO):int
		{
			return ObjectUtil.stringCompare(itemA.THEME, itemB.THEME);
		}
		
		protected function sortProduitQTE(itemA:FicheProduitVO, itemB:FicheProduitVO):int
		{
			return ObjectUtil.numericCompare(itemA.QUANTITE, itemB.QUANTITE);
		}
		
		protected function sortProduitNbAppel(itemA:FicheProduitVO, itemB:FicheProduitVO):int
		{
			return ObjectUtil.numericCompare(itemA.NBAPPEL, itemB.NBAPPEL);
		}
		
		protected function sortProduitVolume(itemA:FicheProduitVO, itemB:FicheProduitVO):int
		{
			return ObjectUtil.numericCompare(itemA.VOLUME, itemB.VOLUME);
		}
		
		protected function sortProduitMontant(itemA:FicheProduitVO, itemB:FicheProduitVO):int
		{
			return ObjectUtil.numericCompare(itemA.MONTANT, itemB.MONTANT);
		}
		
		protected function sortProduitFacture(itemA:FicheProduitVO, itemB:FicheProduitVO):int
		{
			return ObjectUtil.stringCompare(itemA.FACTURE, itemB.FACTURE);
		}
		
	}
}