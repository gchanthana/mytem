package entity
{
	[Bindable]
	public class ProfilVO
	{
		private var _Id				:Number;
		private var _libelle		:String;
		private var _description	:String;
		private var _nbrUtilisateur	:Number;
		private var _dateCreation	:Date;
		private var _createur		:String;
		private var _listeFonctionVo:Array;
		
		public function ProfilVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDPROFIL"))
				{
					this._Id = obj.IDPROFIL;	
				}
				
				if(obj.hasOwnProperty("NOM"))
				{
					this._libelle = obj.NOM;
				}
				
				if(obj.hasOwnProperty("DESCRIPTION"))
				{
					this._description =  obj.DESCRIPTION;
				}
				
				if(obj.hasOwnProperty("NB_UTILISATEUR"))
				{
					this._nbrUtilisateur =  Number(obj.NB_UTILISATEUR);
				}
				
				if(obj.hasOwnProperty("CREATEUR"))
				{
					this._createur = obj.CREATEUR;
				}
				
				if(obj.hasOwnProperty("DATE_CREATION"))
				{
					this.dateCreation = obj.DATE_CREATION;
				}
				
				if(obj.hasOwnProperty("LISTE_FONCTION"))
				{
					for each(var fct:Object in obj.LISTE_FONCTION)
					{
						var item:FonctionVO = new FonctionVO();
						item.fill(fct);
						this._listeFonctionVo.push(item); 
					}
					this._listeFonctionVo = obj.LISTE_FONCTION;
				}
				
			}
			catch(e:Error)
			{
				trace("##initialisation du profil erronée");
			}
		}//End fill
		
		public function get nbrUtilisateur():Number
		{
			return _nbrUtilisateur;
		}
		
		public function set nbrUtilisateur(value:Number):void
		{
			_nbrUtilisateur = value;
		}
		
		public function get description():String
		{
			return _description;
		}
		
		public function set description(value:String):void
		{
			_description = value;
		}
		
		public function get libelle():String
		{
			return _libelle;
		}
		
		public function set libelle(value:String):void
		{
			_libelle = value;
		}
		
		public function get Id():Number
		{
			return _Id;
		}
		
		public function set Id(value:Number):void
		{
			_Id = value;
		}
		
		public function get createur():String
		{
			return _createur;
		}
		
		public function set createur(value:String):void
		{
			_createur = value;
		}
		
		public function get dateCreation():Date
		{
			return _dateCreation;
		}
		
		public function set dateCreation(value:Date):void
		{
			_dateCreation = value;
		}
		
		public function get listeFonctionVo():Array { return _listeFonctionVo; }
		public function set listeFonctionVo(value:Array):void
		{
			if (_listeFonctionVo == value)
				return;
			_listeFonctionVo = value;
		}		
		
	}
}