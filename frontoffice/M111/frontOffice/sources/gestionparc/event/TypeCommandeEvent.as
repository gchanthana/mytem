package gestionparc.event
{
	import flash.events.Event;

	public class TypeCommandeEvent extends Event
	{
		//CONSTANTES
		//---TYPE COMMANDE - MOBILE, FIXE, RESEAU
		public static const TYPE_MOBILE:String="TYPE_MOBILE";
		public static const TYPE_FIXE:String="TYPE_FIXE";
		public static const TYPE_RESEAU:String="TYPE_RESEAU";

		//---TYPE OPERATION - RENOUVELLEMENT, MODIFICATION, RESILIATION
		public static const OPE_NEWCMD:String="OPE_NEWCMD";
		public static const OPE_MODIFCMD:String="OPE_MODIFCMD";

		//---TYPE - TYPE EVENT
		public static const OPE_TYPE_CMD:String="OPE_TYPE_CMD";
		public static const VIEW_OPETYPECMD:String="VIEW_OPETYPECMD";

		//VARIABLES
		public var typeCommande:String="";
		public var typeOperation:String="";

		public function TypeCommandeEvent(type:String, typeOPE:String, typeCMD:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);

			this.typeCommande=typeCMD;
			this.typeOperation=typeOPE;
		}

		override public function clone():Event
		{
			return new TypeCommandeEvent(type, typeCommande, typeOperation, bubbles, cancelable);
		}

	}
}
