package univers.parametres.parametrelogin.event
{
	import flash.events.Event;
	
	public class UserAccountEvent extends Event
	{
		public static const USER_ACCOUNT_EVENT:String = "UserAccountEvent";
		public static const USER_ACCOUNT_CREATED_EVENT:String = "userAccountCreatedEvent";
		public static const USER_ACCOUNT_UPDATED_EVENT:String = "userAccountUpdatedEvent";
		public static const USER_ACCOUNT_LIST_COMPLETE_EEVENT:String = "userAccountListCompleteEvent";
		
		public static var LOCK_COMPLETE:String = "lockComplete"; 
		public static var UNLOCK_COMPLETE:String = "unlockComplete";
		public static var DELETE_COMPLETE:String = "deleteComplete";
		
		public function UserAccountEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new UserAccountEvent(type,bubbles,cancelable)
		}
	}
}