<cfcomponent name="calcul">

	<cffunction name="getGroupes" returntype="query" access="remote">
			<cfquery name="qGetGroupes" datasource="#session.offreDSN#">
				select *
				from groupe_client gc
				WHERE gc.Iddecoupage IS NULL
			</cfquery>
			<cfquery dbtype="query" name="results">
				SELECT libelle_groupe_client AS label, idgroupe_client AS data
				FROM qGetGroupes
				ORDER BY label
			</cfquery>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getTypeProduit" returntype="query" access="remote">
			 <cfif IsDefined('session.etude.idetudeencours')>
				<cfquery name="qGetTypeProduit" datasource="#session.offreDSN#">
				     select tp.libelle
					from compte_facturation cf, sous_tete st, sous_compte sco, etude_sous_tete etu, produit_client pcl,
					produit_catalogue pca, type_produit tp
					WHERE cf.Idcompte_Facturation=sco.Idcompte_Facturation
					AND sco.idsous_compte=st.Idsous_Compte
					AND st.Idsous_Tete=etu.Idsous_Tete
					and cf.idref_client=pcl.idref_client
					And pcl.idproduit_catalogue=pca.idproduit_catalogue
					and pca.idtype_produit=tp.idtype_produit
					AND etu.Idetude=#session.etude.idetudeencours#
					group by tp.libelle
					ORDER BY lower(tp.libelle)
				</cfquery>
				<cfset session.requete.qtypeProduit=qGetTypeProduit>
				<cfquery dbtype="query" name="results">
					SELECT libelle AS label, libelle AS data
					FROM session.requete.qtypeProduit
					ORDER BY label
				</cfquery>
			<cfelse>
				<cfset results=queryNew("libelle,libelle")>
			</cfif>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getGroupeProduit" returntype="query" access="remote">
			<cfquery name="qGetGroupeProduit" datasource="#session.offreDSN#">
				select *
				from groupe_produit
			</cfquery>
			<cfquery dbtype="query" name="results">
				SELECT libelle_groupe_produit AS label, idgroupe_produit AS data
				FROM qGetGroupeProduit
				ORDER BY label
			</cfquery>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getThemeProduit" returntype="query" access="remote">
			<cfargument name="arr" type="array">
			<cfif len(arr[1]) gt 2>
				<cfquery name="qGetGroupeProduit" datasource="#session.offreDSN#">
					select idtheme_produit, theme_libelle 
					from THEME_PRODUIT tp
					WHERE tp.theme_libelle NOT LIKE 'vide%'
					AND lower(tp.segment_theme) like lower('#arr[1]#')
					ORDER BY tp.segment_theme ASC,tp.type_theme, tp.ordre_affichage
				</cfquery>
			<cfelse>
				<cfquery name="qGetGroupeProduit" datasource="#session.offreDSN#">
					select idtheme_produit, tp.segment_theme || ' : ' || theme_libelle as theme_libelle
					from THEME_PRODUIT tp
					WHERE tp.theme_libelle NOT LIKE 'vide%'
					ORDER BY tp.segment_theme ASC,tp.type_theme, tp.ordre_affichage
				</cfquery>
			</cfif>
			<cfquery dbtype="query" name="results">
				SELECT theme_libelle AS label, idtheme_produit AS data
				FROM qGetGroupeProduit
			</cfquery>
			<cfreturn results>
		</cffunction>
	
	<cffunction name="r1" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.ratio_cout_groupe_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r2" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_theme_produit_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r3" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_sans_conso_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r4" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_sans_conso_mobile">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r5" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_groupe_fixe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="r6" returntype="query" access="remote">
		<cfargument name="arr" type="array">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_stats.nblignes_groupe_mobile">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[1]#" null="No">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#arr[2]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[3]#" null="No">
			<cfprocparam type="In" cfsqltype="cF_SQL_VARCHAR" value="#arr[4]#" null="No">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
</cfcomponent>