package univers.parametres.perimetres
{
    import flash.display.DisplayObject;
    import flash.events.ContextMenuEvent;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.URLVariables;
    import flash.net.navigateToURL;
    import flash.ui.ContextMenu;
    import flash.ui.ContextMenuItem;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.controls.Tree;
    import mx.core.ClassFactory;
    import mx.core.DragSource;
    import mx.core.IFactory;
    import mx.core.UIComponent;
    import mx.events.CloseEvent;
    import mx.events.DragEvent;
    import mx.events.FlexEvent;
    import mx.events.ListEvent;
    import mx.managers.DragManager;
    import mx.managers.PopUpManager;
    import mx.resources.ResourceManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    import mx.utils.ObjectUtil;
    
    import composants.access.perimetre.PerimetreEvent;
    import composants.access.perimetre.PerimetreTreeDataDescriptor;
    import composants.util.ConsoviewAlert;
    import composants.util.ConsoviewUtil;
    import composants.util.NodeRefresh;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import univers.inventaire.lignesetservices.vo.Ligne;
    import univers.parametres.perimetres.gestiontechnique.MajCollaborateurView;
    import univers.parametres.perimetres.gestiontechnique.MajWindowImpl;
    import univers.parametres.perimetres.gestiontechnique.MajWindowView;
    import univers.parametres.perimetres.mainComponent.FicheCollaborateur;
    import univers.parametres.perimetres.mainComponent.FicheCollaborateurEvent;
    import univers.parametres.perimetres.mainComponent.FicheSite;
    import univers.parametres.perimetres.mainComponent.NodeProperties;
    import univers.parametres.perimetres.mainComponent.WinAssignLines;
    import univers.parametres.perimetres.mainComponent.griditemRender;
    import univers.parametres.perimetres.mainComponent.refreshTreeEvent;
    import univers.parametres.perimetres.perimetresV2.controls.GestionModele;
    import univers.parametres.perimetres.perimetresV2.views.ChoixTypeNoeudView;
    import univers.parametres.perimetres.rightSide.assignmentComponent.newAssign;
    import univers.parametres.perimetres.rightSide.structureComponent.structureNewOrga;

    public class main extends mainIHM
    {
        //		public var myViewStack : ViewStack;
        private var _contextList:ArrayCollection;
        private var _contextGrid:ArrayCollection;
        private var _propertiesWin:NodeProperties;
        private var _propertiesData:ArrayCollection;
        [Bindable]
        private var _dataLines:ArrayCollection;
        private var _rightRef:right;
        public var _organisationData:ArrayCollection;
        private var _showLines:Boolean = false;
        private var perimetreTreeItemRenderer:IFactory;
        private var _movedNode:XML;
        private var _affectHisParent:Number = 0;
        private const TYPE_MOBILE:String = "MOB";

        public function get modeEcriture():Boolean
        {
            return true;
        }
        private var _currentAction:Object;
        private var _boolRenamedFromFiche:Boolean = false;
		
		private var _annuaire:Object;
		
        public function main()
        {
            /*			perimetreTreeItemRenderer =
             new ClassFactory(composants.access.perimetre.PerimetreTreeItemRenderer);*/
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
            /** Tableau qui permet de remplir le menu contextuel du grid */
            configContextMenu();
        /** Tableau qui permet de remplir le menu contextuel du tree */
        }

        private function configContextMenu():void
        {
            _contextGrid = new ArrayCollection([ /*{label:"Désaffecter", callback:onbtCloseDownLinesClicked},*/   /* {label:"Affecter en manuel", callback:onApplyManuelMode},
                 {label:"Affecter en auto", callback:onApplyAutoMode}, */ /* {label:"Propriétés", callback:onbtlineProprertiesClicked} */]);
            if (modeEcriture)
            {
                _contextList = new ArrayCollection([ { id: "mnAjouter", label: "Ajouter un noeud", callback: onbtAjouterNoeudClicked, type: 1 }, { id: "mnRenomer", label: "Renommer noeud", callback: onbtRenameNoeudClicked, type: 1 }, { id: "mnSupprimer", label: "Supprimer noeud", callback: onbtSupprimerNoeudClicked, type: 1 }, { id: "mnDesaffecter", label: "Désaffecter les lignes", callback: onbtCloseDownLinesNode, type: 1, separate: true }, { id: "mnAfficherLignes", label: "Afficher les lignes", callback: onbtAfficherLignesClicked, type: 1, separate: true }, { id: "mnProprietes", label: "Propriétés", callback: onbtShowNodePropertiesClicked, type: 1, separate: true }, ]);
            }
            else
            {
                _contextList = new ArrayCollection([ { id: "mnAfficherLignes", label: "Afficher les lignes", callback: onbtAfficherLignesClicked, type: 1, separate: true }, { id: "mnProprietes", label: "Propriétés", callback: onbtShowNodePropertiesClicked, type: 1, separate: true }, ]);
            }
        }

        public function setRightRef(ref:right):void
        {
            _rightRef = ref;
        }

        protected function initIHM(event:Event):void
        {
            imgRefresh.addEventListener(MouseEvent.CLICK, rafraichirArbre);
            _rightRef.getSelectedRef().addEventListener("duplicateNode", onDuplicateNode);
            initFillOrganisation();
            orgaStructure.setContextMenu(_contextList);
            initGridContextMenu();
            // this.btnExportOrga.addEventListener(MouseEvent.CLICK, exportOrgaCSV);
            this.btExportLignes.addEventListener(MouseEvent.CLICK, exportLignesCSV);
            // btnCouperNoeud.addEventListener(MouseEvent.CLICK,onbtCouperNoeudClicked)
            //btnCouperNoeud.visible = modeEcriture;	
            //btnCollerNoeud.addEventListener(MouseEvent.CLICK,onbtCollerNoeudClicked);		
            //btnCollerNoeud.visible = modeEcriture; 
            btnAddOrganisation.visible = modeEcriture && (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE == "Groupe");
            btnAddOrganisation.addEventListener(MouseEvent.CLICK, onbtnAddOrganisationClicked);
            btnAjouterNoeud.visible = modeEcriture;
            btnAjouterNoeud.addEventListener(MouseEvent.CLICK, onbtAjouterNoeudClicked);
            btnSupprimerNoeud.visible = modeEcriture;
            btnSupprimerNoeud.addEventListener(MouseEvent.CLICK, onbtSupprimerNoeudClicked);
            btnRenommerNoeud.visible = modeEcriture;
            btnRenommerNoeud.addEventListener(MouseEvent.CLICK, onbtRenameNoeudClicked);
            btShowCollaborateur.visible = modeEcriture;
            btShowCollaborateur.addEventListener(MouseEvent.CLICK, btShowCollaborateurClickHandler);
            btFicheSite.addEventListener(MouseEvent.CLICK, onbtFicheSite);
            btnAfficherLignes.addEventListener(MouseEvent.CLICK, onbtAfficherLignesClicked);
            btClosedown.visible = modeEcriture;
            btClosedown.addEventListener(MouseEvent.CLICK, onbtCloseDownLinesClicked);
            gdrLignes.addEventListener(ListEvent.CHANGE, gdrLignesChangeHandler);
            gdrLignes.addEventListener(FlexEvent.UPDATE_COMPLETE, gdrLignesUpdateCompleteHandler);
            //Drag en drop sur le treePerimetre			
            orgaStructure.getTreeRef().dropEnabled = true;
            orgaStructure.getTreeRef().dragEnabled = true;
            orgaStructure.getTreeRef().dragMoveEnabled = true;
            orgaStructure.getTreeRef().addEventListener(DragEvent.DRAG_START, treePerimetreDragStartHandler);
            orgaStructure.getTreeRef().addEventListener(DragEvent.DRAG_ENTER, treePerimetreDragEnterHandler);
            orgaStructure.getTreeRef().addEventListener(DragEvent.DRAG_OVER, treePerimetreDrageOverHandler);
            orgaStructure.getTreeRef().addEventListener(DragEvent.DRAG_DROP, treePerimetreDrageDropHandler);
            orgaStructure.getTreeRef().addEventListener(DragEvent.DRAG_COMPLETE, treePerimetreDrageCompepleteHandler);
            orgaStructure.getTreeRef().addEventListener(FlexEvent.UPDATE_COMPLETE, treePerimetreUpdateCompleteHandler);
            orgaStructure.cmbOrganisation.addEventListener(FlexEvent.UPDATE_COMPLETE, cmbOrganisationUpdateCompleteHandler);
            orgaStructure.getTreeRef().addEventListener(ListEvent.ITEM_EDIT_END, treeItemEditEnd);
            orgaStructure.getTreeRef().addEventListener(ListEvent.CHANGE, treePerimetreChangeHandler);
            orgaStructure.setParentRef(this);
            orgaStructure.setXMLFilter(menuFilterData);
            refreshButtons();
        }

        private function onDuplicateNode(e:Event):void
        {
            //trace("Duplicate");
            _currentAction = new Object();
            _currentAction.type = "acceptDuplicateNode";
            _currentAction.comment = "Les lignes existantes dans l’organisation cible seront desaffectées puis réaffectées sur le nœud dupliqué. Voulez vous continuer ?";
            checkLines();
        }

        protected function treePerimetreDragStartHandler(de:DragEvent):void
        {
            trace("************************************* drag start ************************************************");
            if (de.dragInitiator == de.currentTarget)
            {
                if ((Tree(de.dragInitiator).selectedItem as XML).parent().@NID < 0)
                {
                    de.preventDefault();
                    return;
                }
                else
                {
                    _movedNode = ObjectUtil.copy(Tree(de.dragInitiator).selectedItem) as XML;
                    _movedNode.@IDOLDPARENT = (Tree(de.dragInitiator).selectedItem as XML).parent().@NID;
                }
            }
        }

        /** ####################### EVENTS ###############################**/ /**
         * Called as soon as the dragProxy enters the target. You can add logic
         * to determine if the target will accept the drop based on the
         * dragInitiator, the data available in the dragSource.
         * Here the drop is blindly accepted.
         */
        protected function treePerimetreDragEnterHandler(de:DragEvent):void
        {
            trace("drag enter")
            if (de.dragInitiator == de.currentTarget)
            {
                if ((Tree(de.dragInitiator).selectedItem as XML).parent().@NID < 0)
                {
                    de.preventDefault();
                    return;
                }
            }
            DragManager.acceptDragDrop(UIComponent(de.currentTarget));
        }
        //Variables raison de l'echec du drag n drop (déplacer un noeud)
        private static const OPERATION_INTERDITE_NO_DETAIL:Number = 88;
        private static const OPERATION_INTERDITE_TYPE_MISMATCH:Number = 215;
        private var dragNdropFailedReason:Number = OPERATION_INTERDITE_NO_DETAIL;

        /**
         * Called while the dragProxy is over the drop target. You can
         * use this function to determine the type of feedback to show.
         * Since the List is set to allow MOVE (the item is deleted
         * once dropped), different feedback possibilities are given.
         *
         * Also, for this application, the Tree control node the dragProxy is
         * over is selected. As the dragProxy moves, the Tree control's
         * selection changes.
         *
         * The feedback is removed.
         */
        protected function treePerimetreDrageOverHandler(de:DragEvent):void
        {
            var dropTarget:Tree = Tree(de.currentTarget);
            var r:int = dropTarget.calculateDropIndex(de);
            dropTarget.selectedIndex = r;
            var node:XML = dropTarget.selectedItem as XML;
            if (de.dragInitiator == dropTarget)
            {
                Tree(de.dragInitiator).showDropFeedback(de);
                if (node.parent().@NID < 0)
                {
                    DragManager.showFeedback(DragManager.NONE);
                    dragNdropFailedReason = OPERATION_INTERDITE_NO_DETAIL;
                    return;
                }
                if (node.parent().@NID == _movedNode.@IDOLDPARENT)
                {
                    DragManager.showFeedback(DragManager.NONE);
                    dragNdropFailedReason = OPERATION_INTERDITE_NO_DETAIL;
                    return;
                }
                if (node.@NID < 0 || _movedNode.@NID < 0)
                {
                    DragManager.showFeedback(DragManager.NONE);
                    dragNdropFailedReason = OPERATION_INTERDITE_NO_DETAIL;
                    return;
                }
                if (isNaN(node.@NID))
                {
                    DragManager.showFeedback(DragManager.NONE);
                    dragNdropFailedReason = OPERATION_INTERDITE_NO_DETAIL;
                    return;
                }
                if (String(_movedNode.@NUMERO_NIVEAU).charCodeAt() <= String((node.parent() as XML).@NUMERO_NIVEAU).charCodeAt())
                {
                    DragManager.showFeedback(DragManager.NONE);
                    dragNdropFailedReason = OPERATION_INTERDITE_TYPE_MISMATCH;
                    return;
                }
                if (node.@IS_ACTIVE == "0")
                {
                    DragManager.showFeedback(DragManager.NONE);
                    dragNdropFailedReason = OPERATION_INTERDITE_NO_DETAIL;
                    return;
                }
                DragManager.showFeedback(DragManager.MOVE);
            }
            else
            {
                var node2:XML = dropTarget.selectedItem as XML;
                if (node2.parent().@IS_ACTIVE == "0")
                {
                    DragManager.showFeedback(DragManager.NONE);
                    dragNdropFailedReason = OPERATION_INTERDITE_NO_DETAIL;
                    return;
                }
                if (Number(node2.@NTY) == 0)
                {
                    DragManager.showFeedback(DragManager.LINK);
                }
                else
                {
                    DragManager.showFeedback(DragManager.NONE);
                }
            }
        }

        protected function treePerimetreDrageCompepleteHandler(de:DragEvent):void
        {
            Tree(de.dragInitiator).hideDropFeedback(de);
        }

        /**
         * Called when the dragProxy is released
         * over the drop target. The information in the dragSource
         * is extracted and processed.
         *
         * The target node is determined and
         * all of the data selected (the List has allowMultipleSection
         * set) is added.
         */
        protected function treePerimetreDrageDropHandler(de:DragEvent):void
        {
            var dgSource:DragSource = de.dragSource;
            var dropTarget:Tree = Tree(de.currentTarget);
            var r:int = dropTarget.calculateDropIndex(de);
            dropTarget.selectedIndex = r;
            var node:XML = dropTarget.selectedItem as XML;
            if (dropTarget == de.dragInitiator)
            {
                var items:Array = dgSource.dataForFormat("treeItems") as Array;
                var dropedNode:XML = items[0] as XML;
                if (de.action == DragManager.MOVE)
                {
                    moveNode(Number(_movedNode.@NID), Number(node.parent().@NID));
                    de.updateAfterEvent();
                }
                else if (de.action == DragManager.NONE)
                {
                    switch (dragNdropFailedReason)
                    {
                        case OPERATION_INTERDITE_NO_DETAIL:
                        {
                            Alert.okLabel = "Fermer";
                            Alert.show("Impossible de déplacer ce nœud", "Opération non autorisée");
                            break;
                        }
                        case OPERATION_INTERDITE_TYPE_MISMATCH:
                        {
                            Alert.okLabel = "Fermer";
                            Alert.show("Impossible de déplacer ce nœud dans un nœud de niveau inférieur", "Opération non autorisée");
                            break;
                        }
                    }
                    _movedNode = null;
                }
                else
                {
                    _movedNode = null;
                }
                return;
            }
            else
            {
                trace("************ Deplacer les lignes 2 *************");
            }
        }

        protected function treePerimetreUpdateCompleteHandler(fe:FlexEvent):void
        {
            if (orgaStructure.getTreeRef().initialized)
            {
                //	lblTypeNoeud.text = (orgaStructure.getTreeRef().selectedItem != null)?getSelectedItemNode().@TYPE_NIVEAU:"-";
                refreshButtons();
            }
        }

        private function rafraichirArbre(me:MouseEvent):void
        {
            var refreshTree:NodeRefresh = new NodeRefresh(orgaStructure._treeNodes[0] as XML)
            orgaStructure.getTreeRef().expandItem(orgaStructure._treeNodes[0], false);
        }

        protected function treePerimetreChangeHandler(ev:Event):void
        {
            // lblTypeNoeud.text = (orgaStructure.getTreeRef().selectedItem != null)?getSelectedItemNode().@TYPE_NIVEAU:"-";
        }

        private function updateTypeOrgaLbl():void
        {
            var typeOrga:String = (orgaStructure.cmbOrganisation.selectedItem != null) ? getSelectedTypeOrga() : "-";
            switch (typeOrga)
            {
                case "CUS":
                    lblTypeOrga.text =  ResourceManager.getInstance().getString('M22', 'Personnalis_e');
                    break;
                case "OPE":
                    lblTypeOrga.text = ResourceManager.getInstance().getString('M22', 'Op_rateur');
                    break;
                /*case "ANU":
                   lblTypeOrga.text = "Annuaire";
                 break;*/
                case "GEO":
                    lblTypeOrga.text = ResourceManager.getInstance().getString('M22', 'Geographique');
                    break;
                default:
                    lblTypeOrga.text = "-";
                    break;
            }
        }

        protected function cmbOrganisationUpdateCompleteHandler(fe:FlexEvent):void
        {
            if (this.initialized)
            {
                refreshButtons();
                updateTypeOrgaLbl();
            }
        }

        private function exportOrgaCSV(event:MouseEvent):void
        {
            var url:String = modulePerimetreIHM.urlBackoffice + "/fr/consotel/consoview/cfm/tools/exportOrgaCSV.cfm";
            var variables:URLVariables = new URLVariables();
            //si on est connecté en racine ou sur une organisation alors on exporte l'organisation sélectionnés
            // si on est en 'sous-périmètre' alors on exporte le 'sous-périmètre';
			if (orgaStructure.cmbOrganisation.selectedItem != -1)
			{
				
				// Cas : le perimetre est la racine
				if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT ) 
				{
					if (orgaStructure.cmbOrganisation.selectedItem.hasOwnProperty("IDGROUPE_CLIENT") && orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT != null)
					{
						
						variables.ID_ORGA = orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT
						variables.LIBELLE_ORGA = orgaStructure.cmbOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT
					}
				}
				else
				{
					variables.ID_ORGA = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
					variables.LIBELLE_ORGA = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
					
				}
			}
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = "POST";
            navigateToURL(request, "_blank");
        }

        private function exportLignesCSV(event:MouseEvent):void
        {
            //var url:String = modulePerimetreIHM.urlBackoffice + "/fr/consotel/consoview/cfm/tools/exportOrgaStCSV.cfm";
            var url:String = modulePerimetreIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M22/exportUtils.cfm";
            var variables:URLVariables = new URLVariables();
            variables.MODE = 1;
            if (orgaStructure.cmbOrganisation.selectedItem != -1)
            {
             
				// Cas : le perimetre est la racine
				if (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT ) 
                {
                    if (orgaStructure.cmbOrganisation.selectedItem.hasOwnProperty("IDGROUPE_CLIENT") && orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT != null)
                    {
                        
                        variables.ID_ORGA = orgaStructure.cmbOrganisation.selectedItem.IDGROUPE_CLIENT
                        variables.LIBELLE_ORGA = orgaStructure.cmbOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT
                    }
                }
                else
                {
                    variables.ID_ORGA = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
                    variables.LIBELLE_ORGA = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
                        
                }
            }
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method = "POST";
            navigateToURL(request, "_blank");
        }

        protected function gdrLignesChangeHandler(le:ListEvent):void
        {
            refreshButtons();
            if (gdrLignes.selectedItems.length == 1 && gdrLignes.selectedItem.TYPE_FICHELIGNE == TYPE_MOBILE)
            {
                btShowCollaborateur.visible = true && modeEcriture;
            }
            else
            {
                btShowCollaborateur.visible = false;
            }
        }

        protected function gdrLignesUpdateCompleteHandler(fe:FlexEvent):void
        {
            if (gdrLignes.initialized)
                if (gdrLignes.selectedItems.length == 1 && gdrLignes.selectedItem.TYPE_FICHELIGNE == TYPE_MOBILE)
                {
                    btShowCollaborateur.visible = true && modeEcriture;
                }
                else
                {
                    btShowCollaborateur.visible = false;
                }
        }

        protected function btShowCollaborateurClickHandler(me:MouseEvent):void
        {
            showTwMajCollaborateur();
        }

        protected function onbtnAddOrganisationClicked(event:MouseEvent):void
        {
            var structNewOrga:structureNewOrga = PopUpManager.createPopUp(this, structureNewOrga, true) as structureNewOrga;
            structNewOrga.setStructureRef(this);
        }

        private function checkLines():void
        {
            var IDnode:Number = orgaStructure.getSelectedTreeItemValue();
            if (orgaStructure.getSelectedTreeIndex() < 0)
                return;
            if (isLastSelectedChild() == false)
            {
                //acceptAddNodeWithLines();
                this.dispatchEvent(new Event(_currentAction.type));
                return;
            }
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "getLines", checkLinesResult);
            RemoteObjectUtil.callService(op, IDnode.toString(), 0, getSelectedDate());
        }

        protected function onbtAjouterNoeudClicked(event:Event):void
        {
            _currentAction = new Object();
            _currentAction.type = "acceptAddNodeWithLines";
            _currentAction.comment = "Les lignes vont être affectées au nouveau noeud.";
            this.addEventListener("acceptAddNodeWithLines", acceptAddNodeWithLines);
            checkLines();
        }

        protected function onbtCouperNoeudClicked(ev:Event):void
        {
        }

        protected function onbtCollerNoeudClicked(ev:Event):void
        {
        }

        protected function onbtRenameNoeudClicked(event:Event):void
        {
            if (orgaStructure.getSelectedTreeIndex() < 0)
            {
                return;
            }
            orgaStructure.setSelectedTreeItemEditable(true);
        }
        private var pchaine:String;
        private var oldChaine:String;
        private var idNodeToRename:Number;
        private var nbrDoublons:Number;
        private var message:String;

        protected function treeItemEditEnd(event:ListEvent):void
        {
            var idnodeErreur:Number = Number(orgaStructure.treePerimetre.selectedItem.@NID);
            var idnodeFromEvent:Number = XML(event.currentTarget.editedItemRenderer.data).@NID;
            var idNodeFromParams:Number = orgaStructure.treeEditedItemNodeId;
            oldChaine = XML(event.currentTarget.editedItemRenderer.data).@LBL;
            idNodeToRename = orgaStructure.treeEditedItemNodeId;
            if (oldChaine != event.currentTarget.itemEditorInstance.text)
            {
                checkDoublons(event.currentTarget.itemEditorInstance.text, oldChaine);
            }
            else
            {
                orgaStructure.setSelectedTreeItemEditable(false);
            }
        }

        /***/ /** Verifie si le libellé est déjà utilisé dans l'orga ***/
        private function checkDoublons(chaine:String, oldValue:String):void
        {
            pchaine = chaine;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "checkDoublons", checkDoublonsResultHandler);
            RemoteObjectUtil.callService(op, orgaStructure.getSelectedOrganisationValue(), chaine);
            orgaStructure.setSelectedTreeItemEditable(false);
        }

        /*--*/
        private function checkDoublonsResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                message = "Il existe " + re.result.toString() + " noeud(s) avec le même libellé.\nVoulez-vous poursuivre?";
                Alert.cancelLabel = "Annuler";
                Alert.yesLabel = "Oui";
                Alert.show(message, "Attention !", Alert.YES | Alert.CANCEL, orgaStructure, renomerLeNoeud);
            }
            else
            {
                renameNode(idNodeToRename, pchaine);
            }
        }

        private function renomerLeNoeud(cle:Object):void
        {
            if (cle != null && CloseEvent(cle).detail == Alert.YES)
            {
                renameNode(idNodeToRename, pchaine);
            }
            else
            {
                var nodeEdited:Object = orgaStructure._treeNodes.descendants().(@NID == idNodeToRename)[0];
                if (nodeEdited != null)
                    nodeEdited.@LBL = oldChaine;
            }
        }

        /*--*/ /** Close down lines from a node */
        protected function onbtCloseDownLinesNode(event:Event):void
        {
            var id:Number = orgaStructure.getSelectedTreeItemValue();
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "closeDownLinesNode", CloseDownLinesNodeResult);
            RemoteObjectUtil.callService(op, id);
        }

        protected function CloseDownLinesNodeResult(event:Event):void
        {
            onbtAfficherLignesClicked(new Event("refresh"));
        }

        /** lors de la suppression :
         * Si le noeud possède des enfants = message d'alert
         * Sinon = on supprime
         * */
        protected function onbtSupprimerNoeudClicked(event:Event):void
        {
            if (orgaStructure.getSelectedTreeIndex() < 0)
                return;
            /* if (XML(orgaStructure.getSelectedItemNode()).parent() == null){
               Alert.show("Suppression d'organisation","INFO");
               return;
             } */
            var messageDeConfirmation:String = "";
            if (isLastSelectedChild() == true)
            {
                messageDeConfirmation = "Etes vous sur de vouloir supprimer ce noeud?";
            }
            else
            {
                /* var _winAssign:Object = PopUpManager.createPopUp(this, WinConfirmDelete);
                 _winAssign.setParentRef(this); */
                messageDeConfirmation = "Etes vous sur de vouloir supprimer ce noeud et toute sa descendance?";
            }
            ConsoviewAlert.afficherAlertConfirmation(messageDeConfirmation, "confirmation", confirmerSupprimerNoeudHandler);
        }

        protected function confirmerSupprimerNoeudHandler(ce:CloseEvent):void
        {
            if (ce.detail == Alert.OK)
            {
                acceptDeleteNodesChilds();
            }
        }

        /** Affiche les propriétés d'un noeud */
        protected function onbtShowNodePropertiesClicked(event:Event):void
        {
            if (orgaStructure.getSelectedTreeIndex() < 0)
                return;
            displayNodeProperties(orgaStructure.getSelectedTreeItemValue());
        }

        public function onbtAfficherLignesClicked(event:Event):void
        {
            var id:Number = orgaStructure.getSelectedTreeItemValue();
            _showLines = true;
            displayLines(id);
        }

        /** Propriétés d'une ligne en orga analytique */
        protected function onbtlineProprertiesClicked(event:Event):void
        {
            var id:Number = orgaStructure.getSelectedTreeItemValue();
            var idLine:Number = parseInt(getSelectedLinesList());
            var lineType:String = gdrLignes.selectedItem.COEFFAUTO;
            var assignWin:newAssign = PopUpManager.createPopUp(this, newAssign, true) as newAssign;
            assignWin.updateWinTitle(getSelectedLinesLabel());
            assignWin.setDataID(idLine);
            assignWin.setOrgaID(getSelectedOrganisation());
            if (lineType == "A")
                assignWin.displayAuto();
        }

        protected function onbtCloseDownLinesClicked(event:Event):void
        {
            var id:Number = orgaStructure.getSelectedTreeItemValue()
            var items:String = getSelectedLinesList();
            removeSelectedItemsGrid();
            closeDownLines(id, items);
        }

        /** En analytique : evenement pour afficher la date */
        protected function onCheckcbShowDate(event:MouseEvent):void
        {
            /*cdcDate.visible = false;
               if (cbShowDate.selected == true)
             cdcDate.visible = true;*/
            refresh();
            try
            {
                _rightRef.getSelectedRef().mainDateChanged();
            }
            catch (error:Error)
            {
            }
        }

        /** En analytique : Si la date change = rafraichit l'arbre */
        protected function onDateChange(event:Event):void
        {
            refresh();
            try
            {
                _rightRef.getSelectedRef().mainDateChanged();
            }
            catch (error:Error)
            {
            }
            //_rightRef.getSelectedRef(1);
        }

        protected function onbtManageClicked(event:Event):void
        {
            parentDocument.displayRightSide();
        }

        protected function onApplyManuelMode(event:Event):void
        {
            updateLinesAssignType(0);
        }

        protected function onApplyAutoMode(event:Event):void
        {
            updateLinesAssignType(1);
        }

        /** Affiche le popup - les données d'un site */
        protected function onbtFicheSite(event:Event):void
        {
            if (getSelectedTypeOrga() == "GEO")
            {
                var ficheSiteWin:FicheSite = PopUpManager.createPopUp(this, FicheSite, true) as FicheSite;
                ficheSiteWin.setParentRef(this);
                return
            }
            if (getSelectedTypeOrga() == "ANU")
            {
                var getNodeInfosOp:AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "getNodeProperties", getNodePropertiesHandler);
                RemoteObjectUtil.callService(getNodeInfosOp, getSelectedNode());
                return;
            }
        }

        private function getNodePropertiesHandler(re:ResultEvent):void
        {
            if (String(re.result[0].TYPE_NIVEAU).toUpperCase() == "COLLABORATEUR")
            {
                var ficheCollWin:FicheCollaborateur = new FicheCollaborateur();
                ficheCollWin.setId(getSelectedNode());
                ficheCollWin.addEventListener("updateLabel", ficheCollaborateurUpdateLabelEventHandler);
                PopUpManager.addPopUp(ficheCollWin, this, true);
                PopUpManager.centerPopUp(this);
            }
            else
            {
            }
        }

        private function ficheCollaborateurUpdateLabelEventHandler(fce:FicheCollaborateurEvent):void
        {
            oldChaine = orgaStructure.getSelectedItem().@LBL;
            idNodeToRename = orgaStructure.getSelectedItem().@NID;
            _boolRenamedFromFiche = true;
            if (oldChaine != fce._label)
                checkDoublons(fce._label, oldChaine);
        }

        /** Perimetre change */
        public function onPerimetreChange():void
        {
            btnAddOrganisation.visible = modeEcriture && (CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE == "Groupe");
        }

        /** Methode appelée quand l'organisation change */
        public function organisationChanged():void
        {
            orgaStructure.clearSearch();
            clearGridLines();
            refreshButtons();
            _rightRef.newEvent("organisationChanged");
            refreshTreeInterface();
        }

        /** Methode appelée quand on change de noeud */
        public function treeSelectionChanged():void
        {
            clearGridLines();
            refreshButtons();
            _rightRef.newEvent("treeSelectionChanged");
        }

        /** Lorsque l'arbre a été modifié (noeud supprimé ou renommé)
         * on Rafraichit l'arbre de droite si on est dans la même organisation
         * */
        private function treeModified():void
        {
            refreshButtons();
            refreshMainTree();
            try
            {
                _rightRef.getSelectedRef().mainTreeModified();
            }
            catch (error:Error)
            {
            }
        /*try {_rightRef.getSelectedRef(0).mainTreeModified(); }
         catch (error:Error) { }*/
        }

        /** ####################### END EVENT ###############################*/ /** Envoie requête pour récupérer les organisation de la racine */
        private function fillOrganisation():void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "getOrganisationPlus", fillOrganisationResult);
            RemoteObjectUtil.callService(op, idGroupeMaitre);
        }

        private function initFillOrganisation():void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "getOrganisationPlus", initFillOrganisationResult);
            RemoteObjectUtil.callService(op, idGroupeMaitre);
        }

        private function fillOrganisationResult(result:ResultEvent):void
        {
            var collec:ArrayCollection = ArrayCollection(result.result);
            // suppression du choix annuaire 
            for (var i:int = 0; i < collec.length; i++)
            {
                if (collec.getItemAt(i).TYPE_ORGA == "ANU")
                {
                   annuaire = collec.removeItemAt(i);
					break
                }
            }
            // collec_organisationData = ArrayCollection(collec);
            orgaStructure.setCmbData(_organisationData, "LIBELLE_GROUPE_CLIENT");
            refreshTreeInterface();
            _rightRef.refreshMenu();
        }

        private function initFillOrganisationResult(result:ResultEvent):void
        {
            var collec:ArrayCollection = ArrayCollection(result.result);
            // suppression du choix annuaire 
            for (var i:int = 0; i < collec.length; i++)
            {
                if (collec.getItemAt(i).TYPE_ORGA == "ANU")
                {
                    annuaire = collec.removeItemAt(i);
                    break;
                }
            }
            _organisationData = ArrayCollection(collec);
            orgaStructure.initSetCmbData(_organisationData, "LIBELLE_GROUPE_CLIENT");
            refreshTreeInterface();
            _rightRef.refreshMenu();
        }
        //////////////// ===============================================	
        private var selectNiveau:ChoixTypeNoeudView;

        private function addNode(IDparent:Number, nodeName:String):void
        {
            var listeTypesPossiblesUpdated:Function = function handler(ev:Event):void
                {
                    if (gestionModele.listeTypesPossibles.length > 0 && gestionModele.listeTypesPossibles[0].hasOwnProperty("ERREUR"))
                    {
                        Alert.show("Organisation sans modèle", "Opération non autorisée");
                    }
                    else if (gestionModele.listeTypesPossibles.length > 0 && (!gestionModele.listeTypesPossibles[0].hasOwnProperty("ERREUR")))
                    {
                        selectNiveau = new ChoixTypeNoeudView();
                        selectNiveau.dataProvider = gestionModele.listeTypesPossibles;
                        selectNiveau.addEventListener("niveauSelected", niveauSelectedHandler);
                        PopUpManager.addPopUp(selectNiveau, DisplayObject(parent), true);
                        PopUpManager.centerPopUp(selectNiveau);
                    }
                    else
                    {
                        Alert.show("Niveau maximum atteind", "Opération non autorisée");
                    }
                }
            var gestionModele:GestionModele = new GestionModele();
            if (getSelectedItemNode().hasOwnProperty("@IDORGA_NIVEAU"))
            {
                gestionModele.getTypePossible(getSelectedItemNode().@IDORGA_NIVEAU);
            }
            else
            {
                gestionModele.getTypePossible(getSelectedOrganisationItem().IDORGA_NIVEAU);
            }
            gestionModele.addEventListener("listeTypesPossiblesUpdated", listeTypesPossiblesUpdated);
        }

        private function niveauSelectedHandler(ev:Event):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "addNode", addNodeResult);
            RemoteObjectUtil.callService(op, getSelectedNode(), selectNiveau.selectedNiveauId, "Sans Titre", _affectHisParent);
            btnAjouterNoeud.enabled = false;
        }
        /////////////======================================================
        private var localIdNewNode:Number = -1;

        private function addNodeResult(event:ResultEvent):void
        {
            if (event.result > 0)
            {
                localIdNewNode = Number(event.result);
                PopUpManager.removePopUp(selectNiveau);
                //si le noeud selectionné à déjà charger ses enfants alors on ajoute la novelle feuille
                //et on la sélectionne 
                //sinon on charge les enfants et on sélèctionne la nouvelle feuille
                var _localNode:XML = orgaStructure.getSelectedItem() as XML;
                var _childs:XMLList = _localNode.children();
                var len:Number = _childs.length();
                if (len > 0)
                {
                    var newNode:Object = new Object();
                    newNode.parentID = getSelectedNode();
                    newNode.LBL = "Sans titre";
                    newNode.IDORGA_NIVEAU = selectNiveau.selectedNiveauId;
                    newNode.NUMERO_NIVEAU = selectNiveau.selectedNiveauNumero;
                    newNode.NID = Number(event.result);
                    orgaStructure.addItemToTree(newNode);
                    selectNodeByAttribute("NID", newNode.NID);
                }
                else
                {
                    PerimetreTreeDataDescriptor(orgaStructure.treePerimetre.dataDescriptor).addEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreTreeDataDescriptorPerimetreChildResultHandler);
                    if (_localNode.@NTY == 0)
                        _localNode.@NTY = 1;
                    PerimetreTreeDataDescriptor(orgaStructure.treePerimetre.dataDescriptor).loadNodeChild(_localNode);
                }
                clearGridLines();
                btnAjouterNoeud.enabled = true;
                treeModified();
                selectNiveau = null;
                _affectHisParent = 0;
            }
            else
            {
                trace("addNode Result !ok" + event.result);
            }
        }

        protected function perimetreTreeDataDescriptorPerimetreChildResultHandler(pe:PerimetreEvent):void
        {
            if (localIdNewNode > 0)
            {
                selectNodeByAttribute("NID", localIdNewNode.toString());
                PerimetreTreeDataDescriptor(orgaStructure.treePerimetre.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreTreeDataDescriptorPerimetreChildResultHandler);
            }
        }

        public function selectNodeByAttribute(attribute:String, value:String):void
        {
            var dept:XMLList;
            dept = orgaStructure._treeNodes.descendants().(@[attribute] == value);
            //recherche dans les noeuds déjà en mémoire
            var pr:Object = dept[0];
            while ((pr = pr.parent()) != null)
                orgaStructure.treePerimetre.expandItem(pr, true);
            orgaStructure.treePerimetre.selectedItem = dept[0];
            orgaStructure.treePerimetre.scrollToIndex(orgaStructure.treePerimetre.selectedIndex);
            treeSelectionChanged();
        }
        /**
         * Supprime un groupe
         * Si deleteChild est true : supprime tous les noeuds fils
         * */
        private var localIdNode:Number = -1;

        private function deleteNode(IDnode:Number, deleteChild:Boolean):void
        {
            localIdNode = IDnode;
            var method:String = "deleteNode";
            if (getSelectedTypeOrga() == "ANA")
                method = "deleteNodeAna";
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", method, deleteNodeResult);
            var p_cascade:Number = 0;
            if (deleteChild)
                p_cascade = 1;
            RemoteObjectUtil.callService(op, IDnode, p_cascade);
        }

        private function deleteNodeResult(event:ResultEvent):void
        {
            if (event.result > 0)
            {
                orgaStructure.deleteTreeItem(localIdNode);
                clearGridLines();
                refreshButtons();
                refreshRightSide();
                treeModified();
            }
            else
            {
                var messsage:String = "Opérateion Impossible : ";
                switch (event.result)
                {
                    case -2:
                    {
                        messsage += "La suppression d'organisation n'est pas autorisée";
                        break
                    }
                    case -3:
                    {
                        messsage += "Vous essayez de supprimer un noeud Racine";
                        break
                    }
                    case -4:
                    {
                        messsage += "Un site est associé au noeud que vous tentez de supprimer";
                        break
                    }
                    case -5:
                    {
                        messsage += "Un fournisseur est associé au noeud que vous tentez de supprimer";
                        break
                    }
                    case -6:
                    {
                        messsage += "Un abonnement est associé au noeud que vous tentez de supprimer";
                        break
                    }
                    case -7:
                    {
                        messsage += "Une opération est associée au noeud que vous tentez de supprimer";
                        break
                    }
                    case -1:
                    {
                        messsage += "-1";
                        break
                    }
                }
                Alert.show(messsage, "Erreur");
            }
        }

        private function renameNode(IDnode:Number, nodeName:String):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "renameNode", renameNodeResult);
            RemoteObjectUtil.callService(op, IDnode, nodeName);
        }

        private function renameNodeResult(event:ResultEvent):void
        {
            var newName:String = event.token.message.body[1];
            if (_boolRenamedFromFiche)
            {
                orgaStructure.getSelectedItem().@LBL = newName;
                _boolRenamedFromFiche = false;
            }
            treeModified();
        }

        /** Open the window to show the properties of node */
        private function displayNodeProperties(IDnode:Number):void
        {
            if (_propertiesWin != null)
                PopUpManager.removePopUp(_propertiesWin);
            _propertiesWin = PopUpManager.createPopUp(this, NodeProperties, true) as NodeProperties;
            _propertiesWin.setParentRef(this);
            _propertiesWin.setID(getSelectedOrganisation(), getSelectedTypeOrga(), IDnode);
        }

        /** Get the lines which belong to a node and display them on the datagrid
         *  The second parameter of getLines is not used.
         *  */
        private function displayLines(IDnode:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "getLines", getLinesResult);
            RemoteObjectUtil.callService(op, IDnode.toString(), 0, getSelectedDate());
        }

        private function getLinesResult(event:ResultEvent):void
        {
            _dataLines = ArrayCollection(event.result);
            gdrLignes.dataProvider = _dataLines;
            gdrLignes.itemRenderer = new ClassFactory(griditemRender);
        }

        /** Close down an list of lines which belong to node */
        private function closeDownLines(IDnode:Number, listItems:String):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "closeDownLines", closeDownLinesResult);
            RemoteObjectUtil.callService(op, IDnode, listItems);
        }

        private function closeDownLinesResult(event:ResultEvent):void
        {
            refreshRightSide();
        }

        /**
         * Verifie si le noeud selectionné a des lignes
         * */
        private function checkLinesResult(event:ResultEvent):void
        {
            var list:ArrayCollection = ArrayCollection(event.result);
            var nb:Number = list.length;
            if (nb > 0)
            {
                var _winAssign:Object = PopUpManager.createPopUp(this, WinAssignLines, true);
                //_winAssign.setParentRef(this);
                _winAssign.setTexte(_currentAction.comment);
                _winAssign.addEventListener("acceptRequest", onAcceptRequest);
            }
            else
            {
                //acceptAddNodeWithLines();
                this.dispatchEvent(new Event(_currentAction.type));
            }
        }

        /** remove the selected lines in grid */
        private function removeSelectedItemsGrid():void
        {
            for (var i:Number = gdrLignes.selectedIndices.length - 1; i >= 0; i--)
            {
                var indice:Number = gdrLignes.selectedIndices[i];
                gdrLignes.dataProvider.removeItemAt(indice);
            }
        }

        private function onAcceptRequest(e:Event):void
        {
            this.dispatchEvent(new Event(_currentAction.type));
        }

        private function applyDispatchAction():void
        {
            this.dispatchEvent(new Event(_currentAction.type));
            _currentAction = null;
        }

        /**  Tree Context Menu   */
        private function initGridContextMenu():void
        {
            var cm:ContextMenu = new ContextMenu();
            cm.hideBuiltInItems();
            for (var i:int = 0; i < _contextGrid.length; i++)
            {
                var item:ContextMenuItem = new ContextMenuItem(_contextGrid.getItemAt(i).label);
                if (_contextGrid.getItemAt(i).separate == true)
                    item.separatorBefore = true;
                item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, _contextGrid.getItemAt(i).callback);
                cm.customItems.push(item);
            }
            gdrLignes.contextMenu = cm;
            cm.addEventListener(ContextMenuEvent.MENU_SELECT, displayContextOptions);
        }

        private function displayContextOptions(event:Event):void
        {
            for (var i:int = 0; i < _contextGrid.length; i++)
            {
                event.currentTarget.customItems[i].enabled = true;
                if (_contextGrid[i].forceEnabled == true)
                    event.currentTarget.customItems[i].enabled = false;
            }
        }

        private function refreshRightSide():void
        {
            try
            {
                clearSearch();
                _rightRef.getSelectedRef().refreshLineGrid();
            }
            catch (error:Error)
            {
            }
        }

        private function refreshTreeInterface():void
        {
            refreshButtons();
        }

        /** Met à jour le type d'affectation des lignes selectionnées
         * assignType : 0 - Manuel
         * 				1 - Auto
         *  */
        private function updateLinesAssignType(assignType:Number):void
        {
            var lines:String = getSelectedLinesList();
            var idOrga:String = getSelectedOrganisation().toString();
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetre", "updateAssignMode", updateAssignModeResult);
            RemoteObjectUtil.callService(op, lines, assignType, idOrga);
        }

        private function updateAssignModeResult(event:ResultEvent):void
        {
            onbtAfficherLignesClicked(new Event(""));
        }

        public function refreshMainTree():void
        {
            var e:refreshTreeEvent = new refreshTreeEvent();
            e.orgaID = getSelectedOrganisation();
            dispatchEvent(e);
        }

        /** ######################## PUBLIC METHODS ##################### **/ /** Return the selected Organisation */
        public function getSelectedOrganisationItem():Object
        {
            return orgaStructure.getSelectedOrganisation();
        }

        /** Return the ID (IDGROUPE_CLIENT) of the selected Organisation */
        public function getSelectedOrganisation():Number
        {
            return orgaStructure.getSelectedOrganisationValue();
        }

        /** Return the selected node from the tree */
        public function getSelectedNode():Number
        {
            return orgaStructure.getSelectedTreeItemValue();
        }
		
		/** Return the selected node from the tree */
		public function getNomSelectedNode():String
		{
			return orgaStructure.getNomSelectedNode();
		}
		

        /** Retourne un XML qui correspond au noeud selectionné */
        public function getSelectedItemNode():Object
        {
            return orgaStructure.getSelectedItem();
        }

        /** Return node's data */
        public function getNodeProperties():ArrayCollection
        {
            return _propertiesData;
        }

        /** Return an list id corresponding to the selected lines */
        public function getSelectedLinesList():String
        {
            var items:String = "";
            var bFlag:Boolean = true;
            for each (var obj:Object in gdrLignes.selectedItems)
            {
                if (!bFlag)
                    items += ",";
                items += obj.IDSOUS_TETE;
                bFlag = false;
            }
            return items;
        }

        public function getSelectedLinesLabel():String
        {
            if (gdrLignes.selectedIndex < 0)
                return null;
            return gdrLignes.selectedItem.SOUS_TETE;
        }

        /** Appellée si l'utilisateur accepte d'ajouter un noeud
         *  et de lui affecter les lignes du père
         * */
        protected function acceptAddNodeWithLines(e:Event):void
        {
            _affectHisParent = 1;
            addNode(orgaStructure.getSelectedTreeItemValue(), "Sans Titre");
        }

        /** Appellée si l'utilisateur accepte de supprimer un noeud
         *  et tous ses fils
         * */
        public function acceptDeleteNodesChilds():void
        {
            deleteNode(orgaStructure.getSelectedTreeItemValue(), true);
            orgaStructure.getTreeRef().selectedIndex = -1;
        }

        /** Rafraichit l'arbre */
        public function refresh(myData:Object = null, idPere:Number = -1):void
        {
            if (myData != null)
            {
                orgaStructure.addMovedItemToTree(myData, idPere);
            }
            else
            {
                orgaStructure.refreshTree();
            }
            orgaStructure.clearSearch();
        }

        /** Vide le grid des lignes */
        public function clearGridLines():void
        {
            gdrLignes.dataProvider = null;
            _dataLines = null
            gdrLignes.selectedItem = null;
            _showLines = false;
        }

        /**
         * Retourne True si l'element selectionné n'a pas de fils
         * sinon false
         * */
        public function isLastSelectedChild():Boolean
        {
            var treeRef:Tree = orgaStructure.getTreeRef();
            if (treeRef.selectedIndex < 0)
                return false;
            var tmp:XML = XML(treeRef.selectedItem);
            if (tmp.@NTY == 0)
                return true;
            return false;
        }

        /** Renvoie toutes les organisations */
        public function getOrganisationData():ArrayCollection
        {
            return _organisationData;
        }

        /** Renvoie le type de l'orga selectionné */
        public function getSelectedTypeOrga():String
        {
            return orgaStructure.getSelectedTypeOrga();
        }

        /** initialise la recherche */
        public function clearSearch():void
        {
            orgaStructure.clearSearch();
        }

        /** En analytique : renvoie la date selectionné */
        public function getSelectedDate():String
        {
            /* if (cdcDate.visible == true)
             return df.format(cdcDate.data); */
            return null;
        }

        /** Rafraichit le grid lignes */
        public function refreshLineGrid(showLines:Boolean = false):void
        {
            if (showLines)
            {
                var id:Number = orgaStructure.getSelectedTreeItemValue();
                displayLines(id);
            }
            else
            {
                if (_showLines == false)
                    return;
                var id2:Number = orgaStructure.getSelectedTreeItemValue();
                displayLines(id2);
            }
        }

        /** Ajoute un élement dans les organisations */
        public function refreshOrganisation(item:Object):void
        {
            orgaStructure.addOrganisationInArray(item);
            orgaStructure.addNewOrganisation(item);
            if (_rightRef.canvAssignment.initialized)
                _rightRef.canvAssignment.fillOrganisation();
        }

        /** Rafraichit l'état de tous les boutons de la partie gauche */
        public function refreshButtons():void
        {
            btnAjouterNoeud.enabled = false;
            btnSupprimerNoeud.enabled = false;
            /* btnCouperNoeud.enabled = false;
             btnCollerNoeud.enabled = false; */
            btnRenommerNoeud.enabled = false;
            btnAfficherLignes.enabled = false;
            btFicheSite.enabled = false;
            //  btnExportOrga.enabled = true;
            btExportLignes.enabled = true;
            btClosedown.enabled = false;
            btShowCollaborateur.visible = false;
			_contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer")).forceEnabled = true;
            /* [{label:"Ajouter un noeud", callback:onbtAjouterNoeudClicked, type:1},
               {label:"Renommer noeud", callback:onbtRenameNoeudClicked, type:1},
               {label:"Supprimer noeud", callback:onbtSupprimerNoeudClicked, type:1},
               {label:"Couper le noeud", callback:onbtCouperNoeudClicked, type:1},
               {label:"Coller le noeud", callback:onbtCollerNoeudClicked, type:1},
               {label:"Désaffecter les lignes", callback:onbtCloseDownLinesNode, type:1, separate:true},
               {label:"Afficher les lignes", callback:onbtAfficherLignesClicked, type:1, separate:true},
               {label:"Propriétés", callback:onbtShowNodePropertiesClicked, type:1, separate:true},
             ]); */
            if (orgaStructure.cmbOrganisation.selectedIndex != 1)
            {
                // activation du bouton d'export
                btExportLignes.enabled = true;
            }
            if (getSelectedTypeOrga() == "OPE")
            {
                /* btnCollerNoeud.enabled = false;
                 btnCouperNoeud.enabled = false; */
                btnRenommerNoeud.enabled = false;
                btnSupprimerNoeud.enabled = false;
                orgaStructure.getTreeRef().dropEnabled = false;
                orgaStructure.getTreeRef().dragEnabled = false;
                orgaStructure.getTreeRef().dragMoveEnabled = false;
                if (modeEcriture)
                {
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAjouter")).forceEnabled = true;
                    //_contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer")).forceEnabled = true;
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnSupprimer")).forceEnabled = true;
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnDesaffecter")).forceEnabled = true;
                }
            }
            else
            {
                btnAjouterNoeud.enabled = true;
                var pr:Object = XML(getSelectedItemNode()).parent();
                if (pr != null && pr.@NID > 0)
				{	
					btnSupprimerNoeud.enabled = true;
					btnRenommerNoeud.enabled = true;
					_contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer")).forceEnabled = false;
				}
				else
				{	
					btnSupprimerNoeud.enabled = false;
                	btnRenommerNoeud.enabled = false;
				}
                /* btnCouperNoeud.enabled = true;	 */
                orgaStructure.getTreeRef().dropEnabled = false;
                orgaStructure.getTreeRef().dragEnabled = true;
                orgaStructure.getTreeRef().dragMoveEnabled = true;
                if (modeEcriture && _contextList.length > 0)
                {
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAjouter")).forceEnabled = false;
                    //_contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer")).forceEnabled = false;
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnSupprimer")).forceEnabled = false;
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnDesaffecter")).forceEnabled = false;
                }
            }
            if (orgaStructure.getSelectedTreeItemValue() >= 0)
            {
                //btnCouperNoeud.enabled = false;
                if (orgaStructure.isLastSelectedChild())
                {
                    btnAfficherLignes.enabled = true;
                    if (getSelectedTypeOrga() == "GEO" || getSelectedTypeOrga() == "ANU")
                        btFicheSite.enabled = true;
                    if (gdrLignes.selectedItems.length > 0 && getSelectedTypeOrga() != "OPE")
                        btClosedown.enabled = true
                    else
                        btClosedown.enabled = false;
                    if (gdrLignes.selectedItems.length == 1 && gdrLignes.selectedItem.TYPE_FICHELIGNE == TYPE_MOBILE)
                    {
                        btShowCollaborateur.visible = true && modeEcriture;
                    }
                    else
                    {
                        btShowCollaborateur.visible = false;
                    }
                }
            }
            _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAfficherLignes")).forceEnabled = true;
            if (isLastSelectedChild())
            {
                _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAfficherLignes")).forceEnabled = false;
            }
            var pr2:Object = XML(getSelectedItemNode()).parent();
            if (pr2 != null && pr2.@IS_ACTIVE && pr2.@IS_ACTIVE == 0)
            {
                if (ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAjouter") != -1)
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAjouter")).forceEnabled = true;
                if (ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer") != -1)
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer")).forceEnabled = true;
                btnAddOrganisation.enabled = false;
                btnAjouterNoeud.enabled = false;
                btnRenommerNoeud.enabled = false;
                btShowCollaborateur.enabled = false;
                btClosedown.enabled = false;
                orgaStructure.getTreeRef().dropEnabled = false;
            }
            var pr2Bis:Object = XML(getSelectedItemNode());
            if (pr2Bis != null && pr2Bis.@IS_ACTIVE && pr2Bis.@IS_ACTIVE == 0)
            {
                if (ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAjouter") != -1)
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnAjouter")).forceEnabled = true;
                if (ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer") != -1)
                    _contextList.getItemAt(ConsoviewUtil.getIndexByLabel(_contextList, "id", "mnRenomer")).forceEnabled = true;
                btnAddOrganisation.enabled = false;
                btnAjouterNoeud.enabled = false;
                btnRenommerNoeud.enabled = false;
                btShowCollaborateur.enabled = false;
                btClosedown.enabled = false;
                orgaStructure.getTreeRef().dropEnabled = false;
            }
        }

        public function updateSelectedOrgaType(orgaType:String):void
        {
            this.getSelectedOrganisationItem().TYPE_ORGA = orgaType;
        }

        private function moveNode(idNodeToVove:Number, idDestination:Number):void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionPerimetreStructure", "moveNodes", moveNodesResult);
            RemoteObjectUtil.callService(op, idNodeToVove, idDestination);
        }

        private function moveNodesResult(event:ResultEvent):void
        {
            if (event.result > 0)
            {
                refresh(_movedNode, Number(event.token.message.body[1]));
                refreshMainTree();
                orgaStructure.refreshTree();
            }
            else if (event.result == -1)
            {
                Alert.okLabel = "Fermer";
                Alert.show("Le noeud cible a des lignes affectées", "Opération non autorisée");
            }
            else if (event.result == -5)
            {
                Alert.okLabel = "Fermer";
                Alert.show("Impossible de déplacer ce nœud dans un nœud de niveau inférieur", "Opération non autorisée");
                DragManager.showFeedback(DragManager.NONE);
            }
            else
            {
                Alert.show("Erreur interne", "Erreur lors du remoting [Deplacer]");
                DragManager.showFeedback(DragManager.NONE);
            }
        }
        /** ######################## END PUBLIC METHODS ###################### */ ///---- RENOMER LE COLLABORATEUR MOBILE --------
        //
        private var twMajCollaborateur:MajWindowView;

        private function showTwMajCollaborateur():void
        {
            if (twMajCollaborateur != null)
                twMajCollaborateur = null;
            if (gdrLignes.selectedItems.length == 1)
            {
                if (gdrLignes.selectedItem.TYPE_FICHELIGNE == TYPE_MOBILE)
                {
                    twMajCollaborateur = new MajWindowView();
                    twMajCollaborateur.addEventListener(MajWindowImpl.SAVE_CLICKED, twMajCollaborateurSaveClickedHandler);
                    var ligne:Ligne = new Ligne();
                    ligne.SOUS_TETE = gdrLignes.selectedItem.SOUS_TETE;
                    ligne.IDSOUS_TETE = gdrLignes.selectedItem.IDSOUS_TETE;
                    twMajCollaborateur.ligne = ligne;
                    PopUpManager.addPopUp(twMajCollaborateur, DisplayObject(parentApplication), true);
                    PopUpManager.centerPopUp(twMajCollaborateur);
                }
            }
        }

        private function twMajCollaborateurSaveClickedHandler(ev:Event):void
        {
            gdrLignes.selectedItem.COMMENTAIRES = MajCollaborateurView(twMajCollaborateur.cmp).NOM_COLLABORATEUR_SI_OPERATEUR.text + " " + MajCollaborateurView(twMajCollaborateur.cmp).PRENOM_COLLABORATEUR_SI_OPERATEUR.text;
            _dataLines.itemUpdated(gdrLignes.selectedItem);
            try
            {
                var ar:ArrayCollection = (_rightRef.canvAssignment.grdLinesBloc.gdrLignes.dataProvider as ArrayCollection);
                var index:Number = ConsoviewUtil.getIndexById(ar, "IDSOUS_TETE", gdrLignes.selectedItem.IDSOUS_TETE);
                var obj:Object = ar.getItemAt(index);
                obj.COMMENTAIRES = gdrLignes.selectedItem.COMMENTAIRES;
                ar.itemUpdated(obj)
            }
            catch (e:Error)
            {
            }
        }
        ///---- FIN RENOMER LE COLLABORATEUR MOBILE ----
   
		public function set annuaire(value:Object):void
		{
			_annuaire = value;
		}

		public function get annuaire():Object
		{
			return _annuaire;
		}
 }
}