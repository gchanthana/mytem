<cfcomponent output="false">

	<cffunction name="getCompletel" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		
		<cfhttp method="get" resolveurl="false" url="https://espaceclient.completel.fr" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
		</cfhttp>
		
		<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
		<cfset jCookie=GetToken(GetToken(chaine,1,";"),2,"=")>
		
		<cfhttp method="get" resolveurl="false" url="https://espaceclient.completel.fr/loginservlet" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="url" name="login" value="#login#">
			<cfhttpparam type="url" name="pwd" value="#pwd#">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie)#">
		</cfhttp>
		
		<cfhttp method="get" resolveurl="false" url="https://espaceclient.completel.fr/redirectservlet?page=DETAILSAPPELS" useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
			<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie)#">
		</cfhttp>
		
		<cfset deb=find("file=",cfhttp.FileContent,1)>
		<cfset fin=find("</a><br></div></td></tr>",cfhttp.FileContent,deb)>
		
		<cfset FichiersTotal=0>
		<cfset FichiersImportes=0>
		<cfset FichiersPresents=0>
		
		<cfif fin gt 0>
			<cfset chaine=mid(cfhttp.FileContent,deb,fin)>
			
			<cfset temp=ListToArray(chaine,"=")>
			<cfset FileArray=ArrayNew(1)>
			<cfset compteur=1>
			<cfloop from="1" to="#ArrayLen(temp)#" index="i">
				<cfif find(".csv",temp[i],1) neq 0>
					<cfset fin=find(".csv",temp[i],1)>
					<cfset chaine=mid(temp[i],1,fin+3)>
					<cfset FileArray[compteur]=chaine>
					<cfset compteur=compteur+1>
					<cfset FichiersTotal=FichiersTotal+1>
				</cfif>
			</cfloop>
					
			<cfloop from="1" to="#ArrayLen(FileArray)#" index="i">
				<!--- A remplacer par une requete dans la base lors du vrai import --->
				<cfdirectory action="list" filter="#FileArray[i]#" name="qListe" sort="name asc" 
								directory="\\dbcl2\web_directory\import\cdr\Completel\">
				<!--- Fin du remplacer --->
				<cfif qListe.recordcount eq 0>
					<cfhttp method="get" resolveurl="true" url="https://espaceclient.completel.fr/download.jsp" 
							useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"
							getasbinary="no">
							<cfhttpparam type="cookie" name="JSESSIONID" value="#jCookie#">
							<cfhttpparam type="url" name="file" value="#FileArray[i]#">
					</cfhttp>
					<cffile action="write" file="\\dbcl2\web_directory\import\cdr\Completel\#FileArray[i]#" output="#cfhttp.FileContent#">
					<cfset FichiersImportes=FichiersImportes+1>
				<cfelse>
					<cfset FichiersPresents=FichiersPresents+1>
				</cfif>
			</cfloop>
			<cfif FichiersImportes gt 0>
				<cfmail from="consoProd@consotel.fr" server="192.168.3.119" port="25" to="brice.miramont@consotel.fr" type="HTML" subject="[Recupération Extranet]Completel">
					Il y avait #FichiersTotal# fichiers sur l'extranet Completel.<br>
					#FichiersPresents# fichiers sont deja présents dans la base.<br>
					Téléchargement de #FichiersImportes# fichier<cfif FichiersImportes gt 1>s</cfif>.<br>
				</cfmail>
			</cfif>
		<cfelse>
			<cfmail from="consoProd@consotel.fr" server="192.168.3.119" port="25" to="brice.miramont@consotel.fr" type="HTML" subject="Pb Recup Completel">
				Problème sur ce compte (Probablement pas de factures) : <br>
					<a href="https://espaceclient.completel.fr/loginservlet?login=#login#&pwd=#pwd#">#login#</a><br>
			</cfmail>
		</cfif>
	</cffunction>
	
</cfcomponent>