package intranet.GestionProduit.renderers
{
	import flash.events.FocusEvent;
	
	import mx.containers.Canvas;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	import intranet.GestionProduit.DataSample;
	
	
	public class UIComboIR extends Canvas
	{
		protected var _combo:ComboBox;
		private var _measure:Boolean = false;
		
		
		public function UIComboIR()
		{
			super();
			
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			 
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			if( ! _combo ) _combo = new ComboBox();
			_combo.dropdownWidth=150;
			_combo.x = 5;			
			_combo.rowCount=4;
			_combo.prompt="";
			_combo.width = 110;
					
			addChild(_combo);
		}
		
		 
		 
		
		protected function selectValue():void
		{
			if(_combo == null) return;
			
			if(data.BOOL_ACCES==1)
			{//prod access.
				_combo.selectedIndex = 2;
			}else if(data.TOUT_ACCES == 1)
			{//remise
				_combo.selectedIndex = 1;
			}else if(data.BOOL_ACCES == 0)
			{//aucun
				_combo.selectedIndex = 3;
			}else //rien
			{
				_combo.selectedIndex = 0;		
			}
		}
		
		
		
		override public function set data(value:Object):void{
			
			if(value)
			{
				super.data=value;
				selectValue();
			}
		}
		
		
		protected function creationCompleteHandler(event:FlexEvent):void
		{	
			_combo.dataProvider = DataSample.TabAcces;
			_combo.addEventListener(ListEvent.CHANGE,changeHandler);
			horizontalScrollPolicy = "off";
		}
		
		
		override protected function focusInHandler(event:FocusEvent):void
		{
			super.focusInHandler(event);
			_combo.open();			 
			 selectValue();
		}
		
		override protected function measure():void
		{
			_measure = true;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			
			
			if(_measure)
			{
				var __delta:Number = 0;
				var __widthCbo:Number = unscaledWidth * 90 / 100; 
				__delta = (unscaledWidth - __widthCbo) / 2;
				
				
				_combo.x = __delta;
				_combo.width = __widthCbo;
				_measure = false;
			}
			
		}
		
	
		
		
		
		protected function changeHandler(event:ListEvent):void
		{	
			switch (_combo.selectedIndex)
			{
				case 0:
					data.BOOL_ACCES=-1;
					data.TOUT_ACCES=-1;
					break;
				case 1: 
					data.BOOL_ACCES=0;
					data.TOUT_ACCES=1;
					break;
				case 2:
					data.BOOL_ACCES=1;
					data.TOUT_ACCES=1;
					break;
				case 3:
					data.BOOL_ACCES=0;
					data.TOUT_ACCES=0;
					break;
			}
		
		
		}
		
		
	}
}