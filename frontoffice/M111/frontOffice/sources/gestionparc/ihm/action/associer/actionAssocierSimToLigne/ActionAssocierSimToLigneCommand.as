package gestionparc.ihm.action.associer.actionAssocierSimToLigne
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierSimToLigneCommand implements ICommand
	{
		private var receiver:ActionAssocierSimToLigneReceiver=null;

		public function ActionAssocierSimToLigneCommand(rec:ActionAssocierSimToLigneReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
