package classe
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	[Bindable] public class Login extends EventDispatcher implements IEventDispatcher
	{
	// PROPRIETES	
		private var _LOGIN:String = "";
		private var _NOMCOMPLET:String = "";
		private var _NOM:String = "";
		private var _PRENOM:String = "";
		private var _APP_LOGINID:int;
		private var _SELECTED:Boolean;
	// GETTER	
		public function get LOGIN():String
		{
			return _LOGIN;
		}
		public function get NOMCOMPLET():String
		{
			return _NOMCOMPLET;
		}
		public function get NOM():String
		{
			return _NOM;
		}
		public function get PRENOM():String
		{
			return _PRENOM;
		}
		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}
	// SETTER
		public function set LOGIN(s:String):void
		{
			_LOGIN = s;
		}
		public function set NOMCOMPLET(s:String):void
		{
			_NOMCOMPLET = s;
		}
		public function set NOM(s:String):void
		{
			_NOM = s;
		}
		public function set PRENOM(s:String):void
		{
			_PRENOM = s;
		}
		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;
		}
	// MAIN	
		public function Login()
		{
		}
	// FONCTION
	

		public function set APP_LOGINID(value:int):void
		{
			_APP_LOGINID = value;
		}

		public function get APP_LOGINID():int
		{
			return _APP_LOGINID;
		}
	}
}