﻿<cfcomponent extends="AbstractAction">
	
	<!---sauvegarder une fonction --->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="struct" required="true" >
		
		<cftry>
			
				<!--- procedure pour création d'une fonctionnalité
   			  --- ajouter un paramètre "droit_niveau" qui est une chaine de caractère contenant des niveaux qui ont accès à cette fonctionnalité. --->
				<cfstoredproc procedure="pkg_m28.IUFonction" datasource="ROCOFFRE" >
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.nomFonction#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.commentFct#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.codeFct#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#data.idCategorie#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.p_function#">
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.keyFct#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" value=0> <!--- dans la procédure IUFonction 0 pour Insert (nouveau)  --->
					<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.droit_niveau#">
					<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
				</cfstoredproc>
				
				<cfset qResult = structNew()>
				<cfset qResult.RESULT = p_retour>
				<cfreturn CreateReponse("AJ004", 1, qResult)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ004", 0, ThrowCfCatchError("AJ004",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
				
	</cffunction>

</cfcomponent>