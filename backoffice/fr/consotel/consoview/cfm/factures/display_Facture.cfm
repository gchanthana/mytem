<head>
	<title><cfoutput >#getAppliSaaswedoById()#</cfoutput></title>
</head>
<body>
	<style>
		html body {
		  margin:0;
		  padding:0;
		  background: url(/images/bg.gif) #CCCCCC;
		  /* background: #FFFFFF; */
		  font:x-small Verdana,Georgia, Sans-serif;
		  voice-family:inherit;
		  font-size:x-small;
		  } html>body {
			font-size: x-small;
		}
		
		span {
		  text-transform:uppercase;
		  font-weight:bold;
		  color:#9cf;
		}
		
		td {
			font-size : x-small;
			/*color: #629FB5;*/
		}
		
		a:link, a:visited {
			color: #000000;
			text-decoration: underline;
			font : normal 11px EurostileBold,verdana,arial,helvetica,serif;
		}
		
		a:hover {
		  color: #483D8B;
		  text-decoration: underline;
		  font : normal 11px EurostileBold,verdana,Georgia, arial,helvetica,serif;
		  }
		a.noline:link, a.noline:visited, a.noline:hover {border-style:none;}
		
		.tcontenu2 {
			border-bottom :1px solid #999999;
			border-top    :1px solid #C7C7C7;
			border-left   : 1px solid #C7C7C7;
			border-right  :1px solid #999999;
			border-collapse : collapse;
			padding:0px;
			background: transparent;
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.tcontenu {
			border-bottom :1px solid #999999;
			border-top    :1px solid rgb(241,241,241);
			border-left   :1px solid rgb(241,241,241);
			border-right  :1px solid #999999;
			border-collapse : collapse;
			padding:0px;
			background: rgb(241,241,241);
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		input{
		background:url(/images/inputbg.gif);
		color:rgb(0,0,0);
		font:normal 10px  Arial,Helvetica,sans-serif;
		text-decoration:none;
		background:rgb(255,255,255);
		}
		.input2{
			font 			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color			: black;
			text-align 	: left;
			background	: white;
			border 		: 1px solid #BED0FC;
		}
		button, .button{
		background:url(/images/inputbg.gif);
		color:rgb(0,0,0);
		font:normal 10px  Arial,Helvetica,sans-serif;
		text-decoration:none;
		background:rgb(255,255,255);
		cursor : hand;
		}
		
		select.option{
		background:rgb(255,255,255);
		color:rgb(0,0,0);
		font:normal 10px  EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		table .ghost
		{
			border-right :1px solid #999999;
			border-bottom :1px solid #999999;
			border-left :1px solid #DED6D3;
			border-top :1px solid #DED6D3;
			background: #DED6D3;
			width: 100%;
			padding: 0px;
			color : #353618;
			font : 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		
		table .cumul_rapport
		{
			border : none;
			background: transparent;
			font: normal 11px Tahoma,Arial, sans-serif;
			color:#005EBB;
		}
		
		table .menuconsoview
		{
			border : none;
			background: transparent;
			font: normal 11px Tahoma,Arial, sans-serif;
			color : #005EBB;
		}
		
		.menuconsoview caption{
			background:#036 ;
			color : white;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 4px;
			letter-spacing: 1px;
		}
		
		.menuconsoview th{
			background   :#99CCFF ;
			color        : #036;
			font         : normal 11px EurostileBold,verdana,arial,helvetica,serif;
			padding      : 3px;
			border-collapse: collapse;
		}
		.menuconsoview a{
			font  : normal 11px EurostileBold,verdana,arial,helvetica,serif;
			color : #005EBB;
			text-decoration: none;
			cursor : hand;
		}
		.menuconsoview .a_black{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color : #000000;
			text-decoration: underline;
			cursor : hand;
		}
		.menuconsoview .a_disable{
			font  : normal 11px EurostileBold,verdana,arial,helvetica,serif;
			color : #B9B9B9;
			text-decoration: none;
			cursor : hand;
		}
		
		.menuconsoview a:link, a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color : #005EBB;
			text-decoration:none;
			cursor : hand;
		}
		
		.menuconsoview a:hover, a:active{
			font  : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color : red;
			text-decoration: underline;
			cursor : hand;
		}
		
		.menuconsoview .grid_title
		{
			layout-grid: both loose 2px 2px;
			background: #D9E5FA;
			border-left: 15px solid rgb(86, 143, 250);
			border-right: 1px solid #D9E5FA;
			border-bottom: 1px solid rgb(86, 143, 250);
			border-top: 1px solid rgb(86, 143, 250);
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
		}
		
		.menuconsoview .grid_title_simple
		{
			layout-grid: both loose 2px 2px;
			background: #D9E5FA;
			border-bottom: 1px solid #225F36;
			border-top: 1px solid #225F36;
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
		}
		
		.menuconsoview .grid_normal
		{
			background: #FDFDFB;
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
		}
		
		.menuconsoview .grid_green
		{
			margin-top:0px;
			border:0px solid #CCCC99;
			background : #CDF7AE;
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
		}
		
		.menuconsoview .grid_blue
		{
			margin-top:0px;
			border:0px solid #CCCC99;
			background : #99CCFF;
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
		}
		
		.menuconsoview .grid_titlerouge
		{
			layout-grid: both loose 2px 2px;
			background: #FFE1E1;
			border-left: 15px solid  rgb(204, 0, 0);
			border-right: 1px solid #FFE1E1;
			border-bottom: 1px solid rgb(204, 0, 0);
			border-top: 1px solid rgb(204, 0, 0);
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
			cursor : hand;
		}
		
		.menuconsoview .grid_titlevert
		{
			layout-grid: both loose 2px 2px;
			background: #CDF7AE;
			border-left: 15px solid #004F02;
			border-right: 1px solid #CCCC99;
			border-bottom: 1px solid #004F02;
			border-top: 1px solid #004F02;
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
			cursor : hand;
		}
		
		/* Fin Class menuconsoview */
		
		
		/*  Header */
		.theader_consoview {
			float:left;
			background:#036 ;
			border-top :1px solid #225F36;
			border-right :1px solid #999999;
			border-bottom :1px solid #999999;
			border-left : 1px solid #225F36;
			color : #99CCFF;
			font : normal 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		.theader_mini1_consoview {
			float:left;
			background:#036 ;
			border-top :1px solid #225F36;
			border-right :1px solid #999999;
			border-bottom :1px solid #225F36;
			border-left :1px solid #225F36;
			color : #99CCFF;
			font : normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			vertical-align: bottom;
		}
		
		.theader_mini2_consoview {
			float:left;
			background: #036;
			border-top :1px solid #225F36;
			border-right :1px solid #225F36;
			border-bottom :1px solid #225F36;
			border-left :1px solid #225F36;
			color : #CDF7AE;
			font : normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			vertical-align: top;
		}
		.theader_bis_consoview {
			float:left;
			/* background: #036; */
			background: rgb(241,241,241); 
			border : none;
			colr : black;
			font : normal 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		.theader2 {
			background   :#99CCFF ;
			border-top   :1px solid #99CCFF;
			border-right :1px solid #999999;
			border-bottom:1px solid #999999;
			border-left  :1px solid #99CCFF;
			color        : #036;
			font         : bold 14px EurostileBold,verdana,arial,helvetica,serif;
			padding      : 2px;
		}
		
		/*  Header */
		.theader {
			float:left;
			background:#036 ;
			border-top :1px solid #036;
			border-right :1px solid #999999;
			border-bottom :1px solid #999999;
			border-left :1px solid #036;
			border-collapse : collapse;
			color : #99CCFF;
			font : normal 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		.theader_mini1 {
			float:left;
			background:#036 ;
			border-top :1px solid #036;
			border-right :1px solid #999999;
			border-bottom :1px solid #036;
			border-left :1px solid #036;
			border-collapse : collapse;
			color : #99CCFF;
			font : normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			vertical-align: bottom;
		}
		.theader_mini2 {
			float:left;
			background:#036 ;
			border-top :1px solid #036;
			border-right :1px solid #999999;
			border-bottom :1px solid #999999;
			border-left :1px solid #036;
			border-collapse : collapse;
			color : #99CCFF;
			font : normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			vertical-align: top;
		}
		
		.theader_bis {
			float:left;
			background:#036 ;
			border-top :1px solid #036;
			border-right :1px solid #036;
			border-bottom :1px solid #999999;
			border-left :1px solid #036;
			border-collapse : collapse;
			color : #99CCFF;
			font : normal 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		
		.theader a:link{
			color: #99CCFF;
			text-decoration: underline;
			font : normal 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		.theader a:visited{
			color: #99CCFF;
			text-decoration: underline;
			font : normal 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		
		.theader a:hover{
		  color: #ffffff;
		  text-decoration:underline;
		  font : normal 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		
		/* t_consoActif */
		table.t_consoActif 
		{
			font-size:100%;
			margin-top:0px;
			border:1px solid #CCCC99;
			background : #EAEAD9;
		}
		.t_consoActif a:link{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		 .t_consoActif a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
		}
		 .t_consoActif a:hover{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:red;
			text-decoration:none;
		}
		
		 .t_consoActif span{
			background : #292820;
			color : #EAEAD9;
			font : bold 15px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 0px;
			width: 100%;
			border-bottom : 1px dotted #CCCC99;
		}
		
		.t_consoActif th{
			background : #292820;
			color : #EAEAD9;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		.t_consoActif td {
			padding:0px 0px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		.t_consoActif input.button {
			background : rgb(255,255,255);
			padding:0 0px;
			border: 1px solid #292820;
			text-align:center;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			cursor : hand;
		}
		.t_consoActif input {
		    background : rgb(255,255,255);
			padding:0 0px;
			border: 1px solid #292820;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		
		.t_consoActif checkbox {
		    background : rgb(255,255,255);
			padding:0 0px;
			border: 1px solid #292820;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		
		.t_consoActif select {
		    background : rgb(255,255,255);
			padding:0 0px;
			border: 1px solid #292820;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		/* Fin Class t_consoActif */
		
		
		
		.titre_menu{
			background: #036;
			color : #99CCFF;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		
		.titre_menu2{
			background: #99CCFF;
			color : #036;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		
		.item_menu{
			color : #005EBB;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			border : 1px solid #ffffff;
			background : #ffffff;
		}
		
		.item_menu_hover{
		   background : #D7EBFF;
			color : #036;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			border-top : 1px solid #D7EBFF;
			border-bottom : 1px solid #D7EBFF;
			border-right : 1px solid #D7EBFF;
			border-left : 1px solid #D7EBFF;
			border-collapse : collapse;
			cursor : hand;
		}
		
		.item_menu_selected{
		   background : #99CCFF;
			color : #036;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			border-top : 1px solid #036;
			border-bottom : 1px solid #036;
			border-right : 1px solid #99CCFF;
			border-left : 1px solid #99CCFF;
			cursor : hand;
			border-collapse : collapse;
		}
		.titres_gros{
			background : transparent;
			color:#292820;
			font : bold 14px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			letter-spacing: 2px;
			border-collapse: collapse;
			text-align: left;
		}
		
		.sous_titres_gros{
			background : transparent;
			color:#292820;
			font : normal 13px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			font-weight : bold;
			border-collapse: collapse;
			text-align: left;
		}
		
		.Redzone {
			color: red;
			font:bold 14px Verdana,Georgia, Sans-serif;
			vertical-align: middle;
			text-align: center;
		}
		.Consultation {
			color: #036;
			font:bold 14px Verdana,Georgia, Sans-serif;
			vertical-align: middle;
			text-align: center;
		}
		
		.radio{
			border-bottom : 0px;
			background    : transparent;
			color:rgb(0,0,0);
			font:normal 9px EurostileBold,verdana,Georgia, arial,helvetica,sans-serif;
		}
		
		.text {
			padding:0px;
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.text2 {
			padding:0px;
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			background: #DFDFF3;
		}
		.texte_bas_page{
			color : #353618;
			font : 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			letter-spacing: 4px;
		}
		
		.texte_bas_page_little{
			color : #353618;
			font : 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			letter-spacing: 4px;
		}
		
		.texte_warning{
			padding:2px 2px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color: Green;
			border-collapse: collapse;
		}
		.texte_alerte{
			padding:2px 2px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color: #D40000;
			border-collapse: collapse;
		}
		.tbutton {
			border-bottom :1px ridge #999999;
			border-top    :1px ridge rgb(214,214,214); /*rgb(214,214,214);*/
			border-left   :1px ridge rgb(214,214,214); /*rgb(214,214,214);*/
			border-right  :1px ridge #999999;
			background: rgb(241,241,241);
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			cursor : hand;
		}
		
		.tmenu {
			float:left;
			background:rgb(255,255,255) ;
			border-top    :1px solid #ffffff;
			border-left   :1px solid #ffffff;
		    border-bottom :1px solid #999999;
			border-right  :1px solid #999999;
			border-collapse : collapse;
		}
		.cadre0{
			color : #005EBB;
			background:#eee;
			font : normal 12px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 4px;
		}
		
		.title_critere{	
			color : #036;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		
		.text_critere{
		font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		
		/* actions */
		table.formconsoeco
		{
			padding: 1px, 1px;
			/* border: 1px solid #292820;  */
			background : transparent;
			border-collapse: collapse;
		}
		.formconsoeco td {
			color: black;
			font: normal 11px verdana, Georgia, arial,helvetica,serif;
		}
		.formconsoeco select {
			color: black;
			font: normal 11px verdana, Georgia, arial,helvetica,serif;
		}
		.formconsoeco .td1 {
			background: rgb(105, 129, 219);
			border: rgb(0, 0, 0) 1px solid;
			border-collapse : collapse;
			color: rgb(255, 255, 255);
			font: bold 10px verdana, Georgia, arial,helvetica,serif;
		}
		.formconsoeco .td2 {
			background: transparent;
			border: rgb(0, 0, 0) 1px solid;
			border-collapse : collapse;
			color: black;
			font: bold 10px verdana, Georgia, arial,helvetica,serif;
		}
		.formconsoeco .td3 {
			background: white;
			border: rgb(0, 0, 0) 1px solid;
			border-collapse : collapse;
			color: black;
			font: bold 10px verdana, Georgia, arial,helvetica,serif;
		}
		
		.formconsoeco .tda {
			background: rgb(215, 221, 247);
			border: rgb(0, 0, 0) 1px solid;
			border-collapse : collapse;
			color: rgb(0, 0, 0);
			font: 10px normal verdana, Georgia, arial,helvetica,serif;
		}
		.formconsoeco .tdb {
			background: white;
			border: rgb(0, 0, 0) 1px solid;
			border-collapse : collapse;
			color: rgb(0, 0, 0);
			font: 10px normal verdana, Georgia, arial,helvetica,serif;
		}
		
		.formconsoeco .td_ko {
			background: rgb(166, 66, 66);
			border: rgb(0, 0, 0) 1px solid;
			border-collapse : collapse;
			color: rgb(166, 66, 66);
			font: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			height: 17px;
			letter-spacing: .05em;
		}
		.formconsoeco .td_neutre {
			background: rgb(255, 255, 255);
			border: rgb(0, 0, 0) 1px dashed;
			border-collapse : collapse;
			color: rgb(255, 255, 255);
			font: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			height: 17px;
			letter-spacing: .05em;
		}
		.formconsoeco .td_ok {
			background: rgb(155, 189, 189);
			border: rgb(0, 0, 0) 1px solid;
			border-collapse : collapse;
			color: rgb(155, 189, 189);
			font: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			height: 17px;
			letter-spacing: .05em;
		}
		
		/* Suggestions */
		table .Suggestions{
			border-bottom :1px solid rgb(241,241,241);
			border-top    :1px solid rgb(241,241,241);
			border-left   :1px solid rgb(241,241,241);
			border-right  :1px solid rgb(241,241,241);
			border-collapse : collapse;
			padding:0px;
			background: transparent;
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.Suggestions th{
			color: rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
		}
		.Suggestions td{
			color: rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
		}
		.Suggestions .input{
			color: rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			height : 15px;
			width: 500px;
		}
		.Suggestions .input{
			color: rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			height : 15px;
			width: 500px;
		}
		.Suggestions textarea{
			color: rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			width: 500px;
		}
		
		/* Param?tres */
		table .Parametres{
			border-bottom :1px solid rgb(241,241,241);
			border-top    :1px solid rgb(241,241,241);
			border-left   :1px solid rgb(241,241,241);
			border-right  :1px solid rgb(241,241,241);
			border-collapse : collapse;
			padding:0px;
			background: transparent;
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			
		}
		.Parametres input{
			background: transparent;
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.Parametres .input{
			background: white;
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		
		.Parametres th{
			background: transparent;
			color: rgb(0,0,0);
			font:bold 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			text-align: left;
		}
		
		
		.Parametres td{
			background: transparent;
			color: rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
		}
		
		
		.Parametres .item_hausse{
			background: transparent;
			color: #538181;
			font:italic 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			text-align: right;
		}
		.Parametres .item_baisse{
			background: transparent;
			color: #B50000;
			font:italic 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			text-align: right;
		}
		
		/* Param?tres2 */
		table .Parametres2{
			border 			: 1px solid rgb(241,241,241);
			border-collapse: collapse;
			padding		  	: 0px;
			background	  	: transparent;
			color				: rgb(0,0,0);
			font				: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			
		}
		.Parametres2 input{
			background: transparent;
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.Parametres2 .input{
			background: rgb(255,255,255);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		
		.Parametres2 th{
			background: transparent;
			color: rgb(0,0,0);
			font:bold 10px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			text-align: left;
			border-collapse: collapse;
		}
		
		
		.Parametres2 td{
			background: transparent;
			color: rgb(0,0,0);
			font:normal 10px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			border-collapse: collapse;
		}
		
		
		.Parametres2 .item_hausse{
			background: transparent;
			color: #538181;
			font:italic 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			text-align: right;
		}
		.Parametres2 .item_baisse{
			background: transparent;
			color: #B50000;
			font:italic 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			vertical-align: middle;
			text-align: right;
		}
		
		/* Graph_Accueil */
		table.Graph_Accueil 
		{
			font-size:100%;
		}
		
		.Graph_Accueil th {
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			color: #000000;
			text-align : center;
			background : Silver;
			border-bottom : 1px solid silver;
			border-left : 1px solid #000000;
			border-right : 1px solid #000000;
			border-top : 1px solid #000000;
			border-collapse : collapse;
		}
		
		.Graph_Accueil td {
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color: Black;
			border : 1px solid #000000;
			background: white;
		}
		.Graph_Accueil .alerte {
			font : bold 13px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color: Red;
			text-align : left;
			background: transparent;
			border : 0px;
		}
		/* Top 10 */
		table.Top10
		{
			font-size:100%;
			margin-top:0px;
			border:1px solid #CCCC99;
			background : #EAEAD9;
			border-collapse : collapse;
		}
		
		.Top10 a{
			font : bold 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
			cursor : hand;
		}
		.Top10 a:hover{
			font : bold 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:red;
			text-decoration:none;
			cursor : hand;
		}
		.Top10 button, .button{
			font : bold 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
			cursor : hand;
		}
		.Top10 a:link{
			font : bold 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
			cursor : hand;
		}
		.Top10 a:visited{
			font : bold 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
			cursor : hand;
		}
		.Top10 .main_critere{
			background : #292820;
			color : #EAEAD9;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		.Top10 th{
			background : #292820;
			color : #EAEAD9;
			font : normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		}
		.Top10 td {
			padding:2px 2px;
			font : normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		
		/* Recherche */
		table.recherche 
		{
			font-size:100%;
			margin-top:0px;
			border:1px solid #CCCC99;
			border-collapse : collapse;
			background : #EAEAD9;
		}
		.recherche a:link{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
		}
		 .recherche a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
		}
		 .recherche a:hover{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:red;
			text-decoration:none;
		}
		
		 .recherche span{
			background : #292820;
			color : #EAEAD9;
			font : bold 15px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 0px;
			width: 100%;
			border-bottom : 1px dotted #CCCC99;
			border-collapse : collapse;
		}
		
		.recherche th{
			background : #292820;
			color : #EAEAD9;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		.recherche td {
			padding:2px 2px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		.recherche input.button {
			background : rgb(255,255,255);
			padding:0px 0px;
			border: 1px solid #292820;
			text-align:center;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			width : 100px;
			cursor : hand;
			border-collapse : collapse;
		}
		.recherche input {
		    background : rgb(255,255,255);
			padding:0px 0px;
			border: 1px solid #292820;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		
		.recherche select {
		   background : rgb(255,255,255);
			padding:0px 0px;
			border: 1px solid #292820;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse : collapse;
		}
		/* Fin Class recherche */
		
		/* class_form */
		table.class_form 
		{
			font-size:100%;
			border:1px solid black;
			background : #F4F2EF;
			border-collapse: collapse;
			padding-right: 2;
			padding-left: 2;
		}
		.class_form  a:link{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:underline;
		}
		.class_form  a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:underline;
		}
		.class_form  a:hover{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:red;
			text-decoration: underline;
		}
		
		.class_form  caption{
			/*background : #EDCEA5;*/
			color : #7F2114;
			font : normal 13px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		
		.class_form  th{
			background : #EDCEA5;
			color : #7F2114;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.class_form  tfoot{
			background : #EDCEA5;
			color : black;
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.class_form  td {
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		.class_form .entete {
			background : #EDCEA5;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		.class_form input.button {
			background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:center;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:black;
			width : 100px;
			cursor : hand;
		}
		.class_form input {
		   background : transparent;
			padding:0 2px;
			border: none;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:black;
		}
		
		.class_form select {
		   background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:black;
		}
		
		/* resultat */
		table.resultat 
		{
			font-size:100%;
			/*border: 1px solid #CCCC99;*/
			background : #F4F2EF;
			border-collapse: collapse;
			padding-right: 2;
			padding-left: 2;
		}
		
		.resultat a:link{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:underline;
		}
		 .resultat a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:underline;
		}
		 .resultat a:hover{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:red;
			text-decoration: underline;
		}
		
		.resultat th{
			background : #EDCEA5;
			color : #7F2114;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.resultat tfoot{
			background : #EDCEA5;
			color : black;
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.resultat td {
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.resultat .td_autres {
			background: #DFDFF3;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.resultat .td_clair {
			background: rgb(241,241,241);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.resultat .td_fonce {
			background: #E9E9E9;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.resultat .td_rouge {
			background: rgb(242,169,41);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.resultat .sous_th_total {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #CCDD99;
			/* border 	  : 1px solid #CCCC99; */
			border-collapse: collapse;
		}
		.resultat .sous_th_total1 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #ECFFAF;
			/* border 	  : 1px solid #CCCC99; */
			border-collapse: collapse;
		}
		.resultat .sous_th_total2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #E1F3A6;
			border-collapse: collapse;
		}
		.resultat .sous_th_total3 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #D9F480;
			border-collapse: collapse;
		}
		.resultat .sous_th_total4 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #E1F3A6;
			/* border 	  : 1px solid #CCCC99; */
			border-collapse: collapse;
		}
		.resultat .sous_th_total5 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #ECFFAF;
			border-collapse: collapse;
		}
		.resultat .sous_th_constate {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #A0B3DE;
			border-collapse: collapse;
		}
		.resultat .sous_th_constate1 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #CBD1DA;
			border-collapse: collapse;
		}
		.resultat .sous_th_constate2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #E4E6F2;
			border-collapse: collapse;
		}
		.resultat .sous_th_constate3 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #F1F2F7;
			border-collapse: collapse;
		}
		
		.resultat .sous_th_extrapole{
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #FEB469;
			border-collapse: collapse;
		}
		.resultat .sous_th_extrapole1 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #FED3A7;
			border-collapse: collapse;
		}
		.resultat .sous_th_extrapole2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #FFE6A4;
			/* border 	  : 1px solid #CCCC99; */
			border-collapse: collapse;
		}
		.resultat .sous_th_extrapole3 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : #000000;
			text-align : center;
			background : #FEF2CC;
			border-collapse: collapse;
		}
		
		.resultat input.button {
			background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:center;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			width : 100px;
			cursor : hand;
			border-collapse : collapse;
		}
		.resultat input {
		    background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		.resultat select {
		   background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#CCCC99;
		}
		.resultat .td_out
		{
			layout-grid: both loose 2px 2px;
			background: #FFE1E1;
			color: rgb(0 , 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			padding: 2px;
			border-collapse: collapse;
		}
		/* Fin Class Resultat */
		
		/* titres */
		table.titres
		{
			padding: 2px, 2px;
			font-size:100%;
			border: 1px solid #292820;
			background : rgb(255,255,255);
		}
		.titres th{
			background : transparent;
			color:#292820;
			font : normal 12px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			letter-spacing: 5px;
			border-collapse: collapse;
			text-align: left;
		}
		.titres td {
			font : normal 12px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:black;
			border-collapse: collapse;
		}
		.titres td_spec {
			background : transparent;
			color:#292820;
			font : normal 12px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			letter-spacing: 5px;
			border-collapse: collapse;
		}
		
		/* tbord */
		table.tbord 
		{
			font-size:100%;
			/* border: 1px solid #292820; */
			background : rgb(255,255,255);
			border-collapse: collapse;
		}
		
		.tbord th{
			background : #292820;
			color : #EAEAD9;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			letter-spacing: 5px;
			border-collapse: collapse;
		}
		.tbord td {
			/* border : solid 1px rgb(0,0,0); */
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.tbord .thparam{
			background : #EAEAD9;
			color : rgb(0,0,0);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		
		.tbord .tdblanc {
			border : solid 1px transparent;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.tbord .thtableau{
			background : rgb(241,241,241);
			border : solid 1px rgb(0,0,0);
			color : rgb(0,0,0);
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			text-align: center;
			border-collapse: collapse;
		}
		.tbord .tdrow{
			background : rgb(255,255,255);
			border : solid 1px rgb(0,0,0);
			color: rgb(0,0,0);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.tbord .tdrownull {
			background: #DFDFF3;
			border : solid 1px rgb(0,0,0);
			color: rgb(0,0,0);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.tbord .tdneutre{
			background : rgb(255,255,255);
			border : solid 1px rgb(0,0,0);
			color : rgb(0,0,0);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			text-align: right;
			border-collapse: collapse;
		}
		.tbord .tdhausse{
			background : rgb(255,255,255);
			border : solid 1px rgb(0,0,0);
			color: #D40000;
			text-align: right;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.tbord .tdbaisse{
			background : rgb(255,255,255);
			border : solid 1px rgb(0,0,0);
			color: green;
			text-align: right;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		/* Fin Class Tbord */
		
		
		
		
		/* actions */
		table.actions 
		{
			padding: 2px, 2px;
			font-size:100%;
			border: 1px solid #292820;
			background : rgb(255,255,255);
			border-collapse : collapse;
		}
		 .actions a:link{
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
			cursor : hand;
			border-collapse: collapse;
		}
		 .actions  a:visited{
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
		}
		 .actions a:hover{
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:red;
			text-decoration: underline;
			cursor : hand;
		}
		
		.actions th{
			background : #292820;
			color : #EAEAD9;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			letter-spacing: 5px;
			border-collapse: collapse;
		}
		.actions .thparam{
			background : #EAEAD9;
			color : rgb(0,0,0);
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.actions td {
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.actions .tdblanc {
			padding: 1px, 1px;
			border-top : solid 1px rgb(255,255,255);
			border-right : solid 1px rgb(0,0,0);
			border-bottom : solid 1px  rgb(255,255,255);
			border-left : solid 1px rgb(255,255,255);
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.actions .thtableau{
			padding: 1px, 1px;
			background : rgb(241,241,241);
			border-left : solid 1px rgb(241,241,241);
			border-top : solid 1px rgb(0,0,0);
			border-right : solid 1px rgb(0,0,0);
			border-bottom : solid 1px rgb(241,241,241);
			color : rgb(0,0,0);
			font : bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			text-align: center;
			border-collapse: collapse;
		}
		.actions .tdrow{
			padding: 1px, 1px;
			background : rgb(255,255,255);
			border-left : solid 1px rgb(0,0,0);
			border-top : solid 1px rgb(0,0,0);
			border-right : solid 1px rgb(0,0,0);
			border-bottom : solid 1px rgb(255,255,255);
			color: rgb(0,0,0);
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.actions .tdrownull {
			padding: 1px, 1px;
			background: #DFDFF3;
			border-left : solid 1px rgb(0,0,0);
			border-top : solid 1px rgb(0,0,0);
			border-right : solid 1px rgb(0,0,0);
			border-bottom : solid 1px #DFDFF3;
			color: rgb(0,0,0);
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.actions .tdneutre{
			padding: 1px, 1px;
			background : rgb(255,255,255);
			border-left : solid 1px rgb(255,255,255);
			border-top : solid 1px rgb(0,0,0);
			border-right : solid 1px rgb(0,0,0);
			border-bottom : solid 1px rgb(255,255,255);
			color : rgb(0,0,0);
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			text-align: right;
			border-collapse: collapse;
		}
		.actions .tdhausse{
			padding: 1px, 1px;
			background : rgb(255,255,255);
			border-left : solid 1px rgb(255,255,255);
			border-top : solid 1px rgb(0,0,0);
			border-right : solid 1px rgb(0,0,0);
			border-bottom : solid 1px rgb(255,255,255);
			color: #D40000;
			text-align: right;
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		.actions .tdbaisse{
			padding: 1px, 1px;
			background : rgb(255,255,255);
			border-left : solid 1px rgb(255,255,255);
			border-top : solid 1px rgb(0,0,0);
			border-right : solid 1px rgb(0,0,0);
			border-bottom : solid 1px rgb(255,255,255); 
			color: green;
			text-align: right;
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			border-collapse: collapse;
		}
		
		/* Fin Class Alerte */
		
		/* Alerte */
		table.alerte 
		{
			font-size:100%;
			margin-top:0px;
			border: 1px solid #BB0000;
			background : #EAF4EC;
		}
		.alerte a:link{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:rgb(255,255,255);
			text-decoration:none;
		}
		 .alerte  a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:rgb(255,255,255);
			text-decoration:none;
		}
		 .alerte a:hover{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color: rgb(0,0,0);
			text-decoration:none;
		}
		
		 .alerte th{
			background : Red;
			color : rgb(255,255,255);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			border-collapse: collapse;
		}
		.alerte td {
			padding:2px 2px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		.alerte .tdrouge{
			padding:2px 2px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color: #D40000;
			border-collapse: collapse;
		}
		
		.alerte input.button {
			background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:center;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			width : 100px;
			cursor : hand;
		}
		.alerte input {
		    background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		.alerte select {
		    background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#CCCC99;
		}
		/* Fin Class Alerte */
		
		/* noAlerte */
		table.noalerte 
		{
			font-size:100%;
			margin-top:0px;
			border: 1px solid #669999;
			background : #EAF4EC;
		}
		 .noalerte  a:link{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
		}
		 .noalerte   a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			text-decoration:none;
		}
		 .noalerte  a:hover{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:red;
			text-decoration:none;
		}
		
		 .noalerte  th{
			background : #669999;
			color : Black;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			border-collapse: collapse;
		}
		.noalerte  td {
			padding:2px 2px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		.noalerte  input.button {
			background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:center;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			width : 100px;
			cursor : hand;
		}
		.noalerte  input {
		    background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		.noalerte  select {
		    background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#CCCC99;
		}
		/* Fin Class NOAlerte */
		
		.cadre{
		border-bottom-width:1px;
		border-color:rgb(0,0,0);
		border-left-width:1px;
		border-right-width:1px;
		border-style:solid;
		border-top-width:1px;
		border-collapse:collapse;
		}
		
		.cadre1{
		background: rgb(255,255,255);
		border-bottom :1px solid rgb(0,0,0);
		border-left:1px solid rgb(0,0,0);
		border-right:1px solid rgb(0,0,0);
		border-top:1px solid rgb(0,0,0);
		border-collapse : collapse;
		}
		
		.td0{
		padding:5px;
		background:rgb(255,255,255);
		color:rgb(0,0,0);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td1{
			padding:5px;
			background: #B4CCF5;
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td2{
		padding:5px;
		background:rgb(253,253,243);
		color:rgb(0,0,0);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td3{
		padding:5px;
		background:rgb(5,77,128);
		color:rgb(255,255,255);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td4{
			padding:5px;
			background: #FFF7F2;
			color:rgb(255,255,255);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td5{
		padding:5px;
		background:#036;
		color:rgb(255,255,255);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td6{
		padding:5px;
		background:rgb(0,0,102);
		color:rgb(255,255,255);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td7{
		padding:5px;
		background:rgb(204,204,204);
		color:rgb(0,0,0);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td8{
		padding:5px;
		background:rgb(241,241,241);
		color:rgb(0,0,0);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td9{
		padding:5px;
		background:rgb(66,73,115);
		color:rgb(255,255,255);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		.td10{
		padding:5px;
		background : #292820;
		color : #EAEAD9;
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		
		.td11{
			padding:5px;
			background : #CDF7AE;
			color : #000000;
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		
		.cadre8{
		padding:5px;
		background:rgb(241,241,241);
		color:rgb(0,0,0);
		font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
		}
		
		
		/* Alerte */
		table.consooptimum
		{
			font-size:100%;
			margin-top:0px;
			border: 1px solid rgb(0,0,0);
			background : transparent;
			border-collapse : collapse;
		}
		.consooptimum a:link{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:rgb(255,255,255);
			text-decoration:none;
		}
		.consooptimum a:visited{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:rgb(255,255,255);
			text-decoration:none;
		}
		.consooptimum a:hover{
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color: rgb(0,0,0);
			text-decoration:none;
		}
		
		.consooptimum th{
			color : rgb(255,255,255);
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
		}
		.consooptimum td {
			padding:2px 2px;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		
		.consooptimum input.button {
			background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:center;
			font : bold 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			width : 100px;
			cursor : hand;
			border-collapse : collapse;
		}
		.consooptimum input {
		    background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
		}
		.consooptimum select {
		   background : rgb(255,255,255);
			padding:0 2px;
			border: 1px solid #CCCC99;
			text-align:left;
			font : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#CCCC99;
		}
		.consooptimum .title {
			color: rgb(0, 128, 0);
			font: bold 16px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		}
		.consooptimum .ih3 {
		  	background: transparent;
			color: rgb(0, 128, 0);
			font: bold 15px  Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
			margin-top: 10px;
		}
		.consooptimum .td8 {
		 	background: rgb(105, 129, 219);
			color: rgb(255, 255, 255);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .td8_mini {
		 	background: rgb(5, 77, 128);
			color: rgb(255, 255, 255);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .td1_mini {
		 	background: rgb(235, 234, 216);
			color: rgb(0, 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .td6_mini {
		 	background: rgb(253, 253, 243);
			color: rgb(0, 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .td_titre_mini {
		 	background: rgb(204, 0, 0);
			color: rgb(255, 255, 255);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .td2 {
		 	background: rgb(193, 228, 242);
			color: rgb(0, 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .td2_mini {
		 	background: rgb(253, 253, 243);
			color: rgb(0, 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .textminirouge {
		 	background: transparent;
			color: rgb(153, 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
			padding-top: 0;
		}
		.consooptimum .textminivert {
		 	background: transparent;
			color: rgb(0, 128, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
			padding-top: 0;
		}
		.consooptimum .textmini {
		 	background: transparent;
			color: rgb(0, 0, 0);
			font: normal 11px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
			padding-top: 0;
		}
		.consooptimum .td8_gros {
		 	background: rgb(105, 129, 219);
			color: rgb(255, 255, 255);
			font: normal 15px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum .td8_gros2 {
		 	background: rgb(5, 77, 128);
			color: rgb(255, 255, 255);
			font: normal 15px Tahoma,Arial, sans-serif;
			letter-spacing: .05em;
		/*	padding: 5px; */
		}
		.consooptimum hr {
		 	background: transparent;
			border: 0 none;
			color: rgb(153, 0, 0);
			height: 1px;
			padding: 0;
			text-align: left;
			width: 100%;
		}
		/* Fin Class Consooptimum */
		
		/* Logique budg?taire */
		table.logique_budget
		{
			font-size:100%;
			padding : 1px;
			border : 1px solid black;
			border-collapse: collapse; 
		}
		.logique_budget .Titre {
			font       : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : white;
			background : black;
			border 		: 1px solid black;
			border-collapse : collapse;
		}
		.logique_budget th {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : left;
			background : Silver;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.logique_budget .td_total {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : black;
			background : #E9E9E9;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.logique_budget .td_black {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #E9E9E9;
			text-align : center;
			background : black;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.logique_budget .td_alerte {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : red;
			border     : 1px solid black;
			background : white;
			border-collapse: collapse;
		}
		.logique_budget .td_alerte2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : black;
			border     : 1px solid black;
			background : #FFB9B9;
			border-collapse: collapse;
			vertical-align: middle;
		}
		
		.logique_budget .td_status_no {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : black;
			border     : 1px solid black;
			background : rgb(241,241,241);
			border-collapse: collapse;
		}
		.logique_budget td {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : Black;
			border     : 1px solid black;
			background : white;
			border-collapse: collapse;
		}
		.logique_budget .td_transparent {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			background : transparent;
			color      : Black;
			border     : none;
			border-collapse: collapse;
		}
		
		.logique_budget .alerte {
			font 		   : bold 13px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color			: Red;
			text-align 	: left;
			background	: transparent;
			border 		: 0px;
		}
		
		.logique_budget input{
			font 			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color			: black;
			text-align 	: left;
			background	: white;
			border 		: 1px solid #BED0FC;
		}
		.logique_budget textarea {
			font 			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color			: black;
			text-align 	: left;
			background	: white;
			border 		: 1px solid #BED0FC;
		}
		
		.logique_budget select{
			font 			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color			: black;
			text-align 	: center;
			background	: white;
		}
		.logique_budget input.button{
			font 			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color			: black;
			text-align 	: center;
			background-color: #BED0FC;
			border 		: 1px solid black;
		
		}
		.logique_budget .tbutton {
			border-bottom :1px ridge #999999;
			border-top    :1px ridge rgb(214,214,214);
			border-left   :1px ridge rgb(214,214,214);
			border-right  :1px ridge #999999;
			padding:0px;
			background: rgb(241,241,241);
			color:rgb(0,0,0);
			font:normal 11px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
			cursor : hand;
			text-align: center;
		}
		.logique_budget .button_logique{
			font  		: normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color 		: #ECF6FF;
			background	: transparent;
			cursor 		: hand;
			border 		: none;
		}
		.logique_budget .button_logique2{
			font  		: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color 		: #005EBB;
			text-decoration: underline;
			background	: transparent;
			text-align: center;
			cursor 		: hand;
			border 		: none;
		}
		
		
		
		/* rapport budg?taire */
		table.rapport_budget
		{
			padding    : 1px;
			font-size:100%;
			border : 0px solid black;
			border-collapse: collapse; 
		}
		.rapport_budget .Titre {
			font       : normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #660000;
			background : #DDDDCC;
			border 		: 1px solid black;
			border-collapse : collapse;
		}
		.rapport_budget .td_total {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : black;
			background : #DDDDCC;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget th {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
		/*	text-align : center; */
			background : #EEEEDD;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		
		
		.rapport_budget .td_autres {
			background: #DFDFF3;
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.rapport_budget .td_clair {
			background: rgb(241,241,241);
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.rapport_budget .td_fonce {
			background: #E9E9E9;
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.rapport_budget .td_rouge {
			background: rgb(242,169,41);
			font : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color:#292820;
			border-collapse: collapse;
		}
		
		.rapport_budget .td_out
		{
			layout-grid: both loose 2px 2px;
			background: #FFE1E1;
			color: rgb(0 , 0, 0);
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding: 2px;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_total1 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #CCDD99;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_total2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #E1F3A6;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_total3 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #ECFFAF;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_total4 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #E1F3A6;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_total5 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #ECFFAF;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_constate1 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px; 
			color      : #000000;
			text-align : center;
			background : #A0B3DE;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_constate2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px; 
			color      : #000000;
			text-align : center;
			background : #E4E6F2;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_constate3 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px; 
			color      : #000000;
			text-align : center;
			background : #F1F2F7;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		
		.rapport_budget .sous_th_extrapole1 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #FEB469;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_extrapole2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #FFECB9;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		.rapport_budget .sous_th_extrapole3 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			padding    : 2px;
			color      : #000000;
			text-align : center;
			background : #FEF2CC;
			border 	  : 1px solid black;
			border-collapse: collapse;
		}
		
		.rapport_budget td {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : Black;
			border     : 1px solid black;
			background : white;
			border-collapse: collapse;
		}
		.rapport_budget .td_2 {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color      : Black;
			border     : 1px solid black;
			background : transparent;
			border-collapse: collapse;
		}
		.rapport_budget .td_transparent {
			font       : normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			background : transparent;
			color      : Black;
			border     : none;
			border-collapse: collapse;
		}
		
		
		table .ongletsconsoview
		{
			border : none;
			background: transparent;
			font: normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color : #005EBB;
			/* border-collapse : collapse; */
		}
		.ongletsconsoview .onglets
		{
		  list-style-type: none; 
		  border-bottom: 2px solid black; 
		  padding-bottom: 24px;
		  margin: 0;
		  }
		
		.ongletsconsoview a
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		
		.ongletsconsoview a:link
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		.ongletsconsoview a:visited
		{
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		} 
		.ongletsconsoview a:hover
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		
		.ongletsconsoview td 
		  {
		  float: right; 
		  margin: 2px 2px 0 2px;
		  border: 1px solid black;
		  background-color: #F5821F; 
		  font: normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		  padding: 3px;
		  text-align: center;
		  }
		.ongletsconsoview .actif
		  {
		  border-bottom: 1px solid rgb(241,241,241); 
		  background-color: transparent;
		  padding: 3px;
		  color : black;
		  text-align: center;
		  }
		  
		  
		table .ongletsconsoviewbleu
		{
			border : none;
			background: transparent;
			font: normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
			color : #005EBB;
			/* border-collapse : collapse; */
		}
		.ongletsconsoviewbleu .onglets
		{
		  list-style-type: none; 
		  border-bottom: 2px solid black; 
		  padding-bottom: 24px;
		  margin: 0;
		  }
		
		.ongletsconsoviewbleu a
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		
		.ongletsconsoviewbleu a:link
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		.ongletsconsoviewbleu a:visited
		{
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		} 
		.ongletsconsoviewbleu a:hover
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		
		.ongletsconsoviewbleu td 
		  {
		  float: right; 
		  margin: 2px 2px 0 2px;
		  border: 1px solid black;
		  background-color: #99CCFF; 
		  font: normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
		  padding: 3px;
		  }
		.ongletsconsoviewbleu .actif
		  {
		  border-bottom: 1px solid rgb(241,241,241); 
		  background-color: transparent;
		  padding: 3px;
		  color : black;
		  }
		  
		  
		table .ongletsoffre
		{
			border : none;
			background: transparent;
			font: normal 11px Tahoma,Arial, sans-serif;
			color : #005EBB;
			/* border-collapse : collapse; */
		}
		.ongletsoffre .onglets
		{
		  list-style-type: none; 
		  border-bottom: 2px solid black; 
		  padding-bottom: 24px;
		  margin: 0;
		  }
		
		.ongletsoffre a
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		.ongletsoffre a:link
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		.ongletsoffre a:visited
		{
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		} 
		.ongletsoffre a:hover
		  {
		  display: block;
		  color: black;
		  text-decoration: none;
		  padding: 3px;
		  }
		
		.ongletsoffre td 
		  {
		  float: right; 
		  margin: 2px 2px 0 2px;
		  border: 1px solid black;
		  background-color: #F5821F; 
		  padding: 3px;
		  }
		.ongletsoffre .actif
		  {
		  border-bottom: 1px solid rgb(241,241,241); 
		  background-color: transparent;
		  font: normal 11px Tahoma,Arial, sans-serif;
		  padding: 3px;
		  color : black;
		  }
		  
		table .FT
		{
			border : none;
			background: transparent;
			font: normal 11px Tahoma,Arial, sans-serif;
			color : #000000;
		}
		
		.FT .small_header
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font: italic 10px Arial, sans-serif;
			padding: 2px;
			font-style : italic;
			vertical-align : top;
			text-align : center;
		}
		
		.FT .big
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font:  13px Arial, sans-serif;
			padding: 2px;
		}
		
		.FT .very_big
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font:  16px Arial, sans-serif;
			padding: 2px;
		}
		
		.FT .huge
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font:  22px Arial, sans-serif;
			padding: 2px;
		}
		
		.FT .normal
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font:  12px Arial, sans-serif;
			padding: 2px;
		}
		
		.FT .normalColored
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font:  12px Arial, sans-serif;
			padding: 2px;
			background-color:rgb(255 , 0, 0);
		}
		
		.FT .normalColoredGreen
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font:  12px Arial, sans-serif;
			padding: 2px;
			background-color:rgb(0 , 255, 0);
		}
		.FT .normalColoredBlue
		{
			margin-top:0px;
			color: rgb(0 , 0, 0);
			font:  12px Arial, sans-serif;
			padding: 2px;
			background-color:rgb(0 , 255, 255);
		}
	</style>
<cfparam name="form.PRODUIT_RECHERCHE" default="">
<cfparam name="form.id" default="0">
<cfparam name="form.type_facture" 	default="Complete">
<cfparam name="form.prixCat" 	default="">
<cfset factureFactory=createObject("component","fr.consotel.consoview.Facture.ConcreteFactureCreator")>
<cfset facture = factureFactory.createFacture(form.ID_FACTURE)>
<cfset facture.setTypeFactureStrategy(form.type_facture,form.type_perimetre)>
<cfif isDefined("ID_GRP_CTRL")>
	<!--- Uniquement facture 9Cegetel (534) --->
	<cfset facture.afficherFacture(form.ID_FACTURE,form.idx_perimetre,ID_GRP_CTRL)>
<cfelse>
	<cfset facture.afficherFacture(form.ID_FACTURE,form.idx_perimetre)>
</cfif>
</body>
<cffunction name="getAppliSaaswedoById" returntype="string" access="private" >
	<cfset var myAppli = "MyTEM 360">
	<cfswitch expression="#SESSION.CODEAPPLICATION#" >
		<cfcase value="101" >
			<cfset myAppli = "MyTEM 360">
		</cfcase>
		
		<cfcase value="MyTrace" >
			<cfset myAppli = "MyTrace">
		</cfcase>
		
		<cfcase value="1" >
			<cfset myAppli = "ConsoView v4 - Affichage des factures">
		</cfcase>
		
		<cfcase value="51" >
			<cfset myAppli = "PILOTAGE">
		</cfcase>
		
	</cfswitch>
	<cfreturn myAppli>
</cffunction>