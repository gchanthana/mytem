<cfif isDefined('form.num')>
	<cfquery name="qGetShoot" dataSource="ROCOFFRE">
		SELECT commentaire, max(date_page) date_page, max(idcir_url) idcir_url, page_saved, texte_highlight, ticket_jira, titre_page, url_page, user_jira, 
			nvl(cr.descrip_resolution,'-') descrip_resolution
		FROM CIR_URL, CIR_RESOLUTION cr
		where cir_url.id_resolution = cr.id_resolution (+)
		<cfif isDefined('url.user')>
			and user_jira like '#url.user#' and ticket_jira='#form.num#'
		<cfelse>
			and ticket_jira='#form.num#'
		</cfif>
		group by commentaire, page_saved, texte_highlight, ticket_jira, titre_page, url_page, user_jira, nvl(cr.descrip_resolution,'-')
		order by date_page desc
	</cfquery>
<cfelse>
	<cfquery name="qGetShoot" dataSource="ROCOFFRE">
		SELECT commentaire, max(date_page) date_page, max(idcir_url) idcir_url, page_saved, texte_highlight, ticket_jira, titre_page, url_page, user_jira, 
			nvl(cr.descrip_resolution,'-') descrip_resolution
		FROM CIR_URL, CIR_RESOLUTION cr
		where cir_url.id_resolution = cr.id_resolution (+)
		<cfif isDefined('url.user')>
			and user_jira like '#url.user#'
		</cfif>
		group by commentaire, page_saved, texte_highlight, ticket_jira, titre_page, url_page, user_jira, nvl(cr.descrip_resolution,'-')
		order by date_page desc
	</cfquery>
</cfif>



<cfset nbcol = 7>

<html>
	<head>
		<title>CIR SHOOTER v1.2</title>
		 <style type="text/css">
		 	td {
			 	font-family: Verdana,Geneva,sans-serif;
			 	font-size: 10px;
			 }
			 .ligne {
				 background-color: gray;
			 }
			 .titre {
				 font-size: 24px;
				 text-align: right;
				 vertical-align: middle;
			 }
			 body {
				 background-color: rgba(191,191,191,0.36);
			 }
			 .detail {
				 border-style: solid double none dashed;
				 border-width: 1px 0px 0px 0px;
				 padding: 5px 2px 5px 2px;
				 border-color: rgba(94,94,94,0.86) rgba(191,191,191,0.36) rgba(191,191,191,0.36) rgba(191,191,191,0.36);
				 background-color:white;
				 color: #4b4b4b;
			 }
			 .detail_blue {
				 border-style: solid double none dashed;
				 border-width: 1px 0px 0px 0px;
				 padding: 5px 2px 5px 2px;
				 border-color: rgba(94,94,94,0.86) rgba(191,191,191,0.36) rgba(191,191,191,0.36) rgba(191,191,191,0.36);
				 background-color:white;
				 color: blue;
			 }
			 table {
				 border-spacing: 0px 0px;
			 }
			 a {
				 color: #4b4b4b;
				 font-weight: 600;
				 text-decoration: none;
			 }
		 </style>
  	</head>
	<body>
	<table width="98%" align="center" >
		<cfoutput>
		<tr>
			<td colspan="4"><img src="img/saaswedo_relief_sansfond_moyen.png" width="150"></td>
			<td class="titre" colspan="#Evaluate(nbcol-2)#">CIR SHOOTER v1.1</td>
		</tr>
		<tr height="3">
			<td class="ligne" width="100%" colspan="#nbcol#"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="4">
				<form action="get_url.cfm" method="post">
					<input type="text" size="10" name="num" value="" placeholder="Ticket Jira" /><input type="submit" name="btnSearch" value="Rechercher" />
				</form>
			</td>
			<td align="center">
				<input type="button" name="btn1" value="Liste complète" onclick="javascript:location.href='get_url.cfm';">
			</td>
			<td align="center">
				<input type="button" name="btn2" value="Classement" onclick="javascript:location.href='get_rank.cfm';">
			</td>
		</tr>
		<tr height="3">
			<td class="ligne" width="100%" colspan="#nbcol#"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="#nbcol#"></td>
		</tr>
		<tr align="center">
			<td></td>
			<td><b>Date<b></td>
			<td><nobr><b>Ticket JIRA<b></nobr></td>
			<td width="100px"><b>Page</b></td>
			<td>Contexte</td>
		</tr>
		<tr height="1">
			<td class="ligne" width="100%" colspan="#nbcol#"></td>
		</tr>
		<tr height="5">
			<td width="100%" colspan="#nbcol#"></td>
		</tr>
		</cfoutput>
		<cfoutput query="qGetShoot">
		<tr>
			<td class="detail"><a href="edit_url.cfm?id=#idcir_url#"><img src="img/document.png" width="16"></a></td>
			<td class="detail" align="center">#LsDateFormat(date_page,"dd/mm/yyyy")#</td>
			<td class="detail_blue" align="center"><nobr><a href="https://saaswedo.atlassian.net/browse/#ticket_jira#" target="_blank">#ticket_jira#</a></nobr></td>
			<td class="detail" width="400px">
				<cfif len(trim(commentaire)) gt 0>
					<img src="img/info.png" width="16" title="#commentaire#"/>
				</cfif>
					<a href="#url_page#" target="_blank">#titre_page#</a></td>
			<td class="detail" width="200px"><nobr>&nbsp;&nbsp;#descrip_resolution#&nbsp;&nbsp;</nobr></td>
			<td class="detail"><a href="get_url.cfm?user=#user_jira#">#user_jira#</a></td>
		</tr>
		</cfoutput>
	</table>
	</body>
</html>
