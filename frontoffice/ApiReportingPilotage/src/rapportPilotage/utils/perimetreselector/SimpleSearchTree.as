package rapportPilotage.utils.perimetreselector 
{
	import mx.collections.XMLListCollection;
	import mx.controls.Tree;

	public class SimpleSearchTree extends Tree {
		public function SimpleSearchTree() {
			super();
			//this.dataDescriptor = new PerimetreTreeDataDescriptor();
			trace("(SimpleSearchTree) Instance Creation");
			setStyle("rollOverColor","#DDEEDD");
			setStyle("selectionColor","#C1C1C1");
		}
		
		/**
		 * Séléctionne le noeud ayant le nodeId passé en paramètre puis déroule le chemin
		 * jusqu'à ce noeud
		 * */
		public function selectNodeById(nodeId:int):Boolean {
			var node:XML = (dataProvider as XMLListCollection).getItemAt(0) as XML;
			if(parseInt(node.@NID,10) == nodeId) {
				selectedIndex = 0;
				selectedItem = node;
				return true;
			} else {
				var resultList:XMLList = node.descendants().(@NID == nodeId);
				if(resultList.length() > 0) {
					var targetNode:XML = resultList[0] as XML;
					var tmpParentNode:Object = targetNode.parent();
					while(tmpParentNode != null) {
						if(isItemOpen(tmpParentNode) == false)
							expandItem(tmpParentNode,true);
						tmpParentNode = (tmpParentNode as XML).parent();
					}
					selectedItem = targetNode;
					scrollToIndex(selectedIndex);
					return true;
				} else
					return false;
			}
		}
	}
}