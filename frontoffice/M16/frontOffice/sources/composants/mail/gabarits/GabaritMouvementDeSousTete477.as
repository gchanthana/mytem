package composants.mail.gabarits
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewFormatter;
	
	public class GabaritMouvementDeSousTete477 extends AbstractGabarit
	{
		public function GabaritMouvementDeSousTete477()
		{
			super();
		}
		
		override public function getGabarit():String{
			var listeLignes : String = printListeSousTete(infos.LIGNES);
			return	ResourceManager.getInstance().getString('M16', '_p_Soci_t_____b_')+infos.raison_sociale+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_p_Bonjour___p_')	
					+"<br/>"				
					+ResourceManager.getInstance().getString('M16', '_p_Les_lignes_ci_dessous_font_l_objet_d_')
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', 'Origine_des_lignes__br__')
					+ infos.INFOS_DEST[2][0].TYPE_NIVEAU + " : <b>" +infos.INFOS_DEST[2][0].LIBELLE_GROUPE_CLIENT  + ResourceManager.getInstance().getString('M16', '__b___Gestionnaire____b_') + infos.gestionnaire + "</b>. <br/>"
					+ResourceManager.getInstance().getString('M16', 'Destination__br__') 
					+ infos.INFOS_DEST[1][0].TYPE_NIVEAU + " : <b><font color='#FF1200'>Ref BV :230?</font>" +infos.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT  + ResourceManager.getInstance().getString('M16', '__b___Gestionnaire____b_') + infos.gestionnaireCible + "</b>. <br/>"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+ResourceManager.getInstance().getString('M16', '_br___font_color___FF1200___b_Message___')+ infos.gestionnaireCible +".</b></font>"
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_p_Merci_de_prendre_bonne_note_de_ce_tra')+ infos.INFOS_DEST[1][0].TYPE_NIVEAU + " " +infos.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT + ResourceManager.getInstance().getString('M16', '___b__par__b_') + infos.gestionnaire + "</b></p>"
					+ResourceManager.getInstance().getString('M16', '_p_En_cas_d_erreur_merci_de_prendre_cont')+ infos.gestionnaire + "</b> "+ infos.usermail + ".</p>"
					+"<br/>_______________________________________________________________________________________________"
					+ResourceManager.getInstance().getString('M16', '_br___font_color___FF1200___b_Message___')
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_p_1_____Merci_de_proc_der___la_correcti')+infos.raison_sociale+ResourceManager.getInstance().getString('M16', '__b__de_ces_lignes__si_renseign_____p_')	
					+ResourceManager.getInstance().getString('M16', '_p_2_____Merci_de_proc_der_au_changement')	
					+"<br/>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_p_Cordialement___p_')
					+"<br/>"
					+"<b>"+infos.gestionnaire+ ".</b>"
					+"<br>"
					+"<b>"+infos.raison_sociale+"</b>";
		}
		
		protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var titulaire: String = "";
					if (liste[i].COMMENTAIRES != null && String(liste[i].COMMENTAIRES).length > 0) titulaire = " (" + liste[i].COMMENTAIRES + ")";
					output = output + liste[i].LIBELLE_TYPE_LIGNE + " : <b>"+ ConsoviewFormatter.formatPhoneNumber(liste[i].SOUS_TETE) + titulaire  + ResourceManager.getInstance().getString('M16', '__b__t_t_t_tcompte_actuel_______________') + ResourceManager.getInstance().getString('M16', 'compte_cible_________________br__');
				}
			}
			return output;
		}
		
	}
}
