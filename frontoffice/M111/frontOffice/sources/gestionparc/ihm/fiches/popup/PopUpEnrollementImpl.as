package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.services.mdm.MDMService;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ValidationResultEvent;
	import mx.managers.PopUpManager;
	import mx.validators.EmailValidator;

	public class PopUpEnrollementImpl extends TitleWindow
	{
		protected var _currentEquipementInfos:AbstractMatriceParcVO;
		protected var _mdmServ:MDMService;

		[Bindable]
		public var btnEnvoyer:Button=null;
		public var changeEmailItem:Button=null;
		public var ti_email:TextInput;
		public var ti_ligne:Label=null;
		public var emailValidator:EmailValidator;
		public var message:Label;
		public var os_Terminal:String;
		private var emailIsLocked:Boolean=true;

		public function PopUpEnrollementImpl()
		{
			super();
			_mdmServ=new MDMService();
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleted);
		}

		protected function creationCompleted(eventObject:FlexEvent):void
		{
			ti_email.text=CvAccessManager.getUserObject().EMAIL;
			ti_ligne.text=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE != null ? SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE : "N.D";
			ti_email.enabled=false;
			changeEmailItem.addEventListener(MouseEvent.CLICK, changeEmailAction);
			changeEmailItem.enabled=true;
		}

		protected function changeEmailAction(eventObject:Event):void
		{
			// L'email est verrouillé
			if (emailIsLocked == true)
			{
				emailIsLocked=false;
				ti_email.enabled=true;
				changeEmailItem.label=resourceManager.getString('M111', 'Annuler');
					// L'email est déverrouillé
			}
			else
			{
				emailIsLocked=true;
				ti_email.enabled=false;
				ti_email.text=CvAccessManager.getUserObject().EMAIL;
				changeEmailItem.label=resourceManager.getString('M111', 'Modifier');
			}
		}

		protected function envoyerEmailHandler(event:Event):void
		{
			emailValidator.validate(ti_email.text);
			if (emailValidator.validate(ti_email.text).type == "valid")
			{
				/*
				lors d'envoi d'un OBJECT en parametre[coté Front], nous utilisons un STRUCT en équivalent cote BACKOFFICE
				si cet OBJECT est la seule parametre à passer alors ca génère un bug coté BACK [le Struct n'est passer en parametre].
				Pour contourner à ce problème nous pouvons ajouter un parametre supplémentaire avec l'objet par exemple un number.
				*/
				// un parametre supplémentaire utilisé pour contourner un bug de coldfusion
				var sup_bug:Number=1;
				// Object à envoyer
				var params:Object={serial: SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, imeiMyTEM: SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, imei: SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, idTerminal: SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, ligne: SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE, idSousTete: int(SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE), idTypeEquipement: SpecificationVO.getInstance().elementDataGridSelected.T_IDTYPE_EQUIP, operatingSystem: SpecificationVO.getInstance().elementDataGridSelected.OS, userId: int(CvAccessManager.getUserObject().CLIENTACCESSID), codeApp: int(CvAccessManager.getUserObject().CODEAPPLICATION), userLocale: CvAccessManager.getUserObject().GLOBALIZATION, email: ti_email.text}
				// Handler de l'envoi
				_mdmServ.myDatas.addEventListener(MDMDataEvent.ENROLLMENT_INFOS_SENT, enrollmentInfoSent);
				// envoi de message
				_mdmServ.sendEnrollmentInfos(sup_bug, params);
			}
		}

		protected function enrollmentInfoSent(eventObject:MDMDataEvent):void
		{
			// Fermeture de popUp après l'envoie de message
			// ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_demande_a__t__prise_en_compte'), Application.application as DisplayObject);
			dispatchEvent(eventObject);
			//closeHandler(null);
			//PopUpManager.removePopUp(this);
		}

		protected function closeHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		protected function emailValidator_valid(evt:ValidationResultEvent):void
		{
			ti_email.errorString="";
			message.text="";
		}

		protected function emailValidator_invalid(evt:ValidationResultEvent):void
		{
			ti_email.errorString=evt.message;
			message.text=evt.message;
		}

		public function setInfos(currentEquipementInfos:AbstractMatriceParcVO, mdmServ:MDMService, osTerminal:String):void
		{
			this._currentEquipementInfos=currentEquipementInfos
			this._mdmServ=mdmServ;
			this.os_Terminal=osTerminal;
		}
	}
}
