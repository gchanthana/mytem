package univers.facturation.suivi.controles.event
{
	import flash.events.Event;

	public class SiteEvent extends Event
	{
		public static var SITE_COMPELTE:String = "SiteComplete"; 
		
		public function SiteEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ActeGestionEvent(type,bubbles,cancelable);
		}
	}
}