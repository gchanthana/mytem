package general.abstract
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.utils.getQualifiedClassName;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.ProgressBarMode;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import appli.connection.ConnectionLoginMDPConsoleGestionInternational;
	import appli.connection.ConnectionLoginMDPInternational;
	import appli.connection.ConnectionLoginMDPfrance;
	import appli.connection.ConnectionSPTokenMytem360;
	import appli.connection.ConnectionSPmytem360;
	import appli.connection.ConnectionSSOSeSouvenir;
	import appli.connection.ConnectionSSOsfr;
	import appli.control.AppControlEvent;
	import appli.events.ConnectionEvent;
	import appli.events.ConsoViewEvent;
	
	import composants.ConsoProgressBar.ConsoProgressBar;
	import composants.util.ConsoviewAlert;
	
	import entity.AppParams;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import general.entity.Constantes;
	
	import interfaceClass.IApplication;
	import interfaceClass.IConnection;
	
	import styles.ConsoViewStyleManager;
	
	
	public class AbstractApplication extends VBox implements IApplication
	{
		private static const DEFAULT_STYLE:String = "MYTEM_DEFAUT";
		
		private var _compLog:IConnection;
		private var _compIHM:AbstractCreateApplication;
		private var _progressBar:ConsoProgressBar;
		
		private var _params:AppParams;
		
		public function AbstractApplication()
		{
			//                      
			//                      if(!this.hasEventListener(FlexEvent.INITIALIZE))
			//                              this.addEventListener(FlexEvent.INITIALIZE,init);
		}
		// ---------------------- --------------- ---------------------------------             
		// ---------------------- FONCTION PUBLIC ---------------------------------
		// ---------------------- --------------- ---------------------------------     
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		public function init(param:AppParams):void
		{
			initComposant();
			initDisplay();
			
			defineConnection();
			
			initListener();
			
			if(this.getChildren().length > 0)
				this.removeAllChildren();
			
			executeLogin()
		}
		/** 
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		public function executeLogin():void
		{
			if(_compLog)
			{
				if(_compLog.MustBeDisplay())
					_compLog.goLogin(_params,this);
				else
					_compLog.goLogin(_params);
			}
		}       
		/** 
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		public function deconnect(evt:AppControlEvent=null):void
		{
			closeAllPopups();
			_compLog.deconnect(_params,this);
			if(ExternalInterface.available)
			{
				ExternalInterface.call("splogoff");
			}
		}
		/**
		 *      <code>displayIHM()</code> 
		 * @param
		 * @param
		 * 
		 * cette fonction affiche l'interface graphique de l'application sélectionnée ( appel de la fonction defineDisplay() ).
		 * 
		 * 
		 * @see
		 */
		public function displayIHM(evt:Event = null):void
		{
			if(_compIHM)
				_compIHM = null;
			processAppStyle();
			defineDisplay();                        
			this.removeAllChildren();
			this.addChild(_compIHM);
			unshowProgressBar();
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		protected function defineDisplay():void
		{
			this.compIHM.moduleLoader.progress = progressBar;
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		public function displayError(evt:ConnectionEvent):void
		{
			//Ajouter un écran de sortie pour le SSO
			switch(evt.objErreur.codeErreur)
			{
				case  -11:      setResultAuth(evt.objErreur.codeErreur, ResourceManager.getInstance().getString('M00', 'Acc_s_Refus____Login_ou_Mot_de_passe_invalide_s_'), "(Login) AUTH FAILED : Invalid Credentials");break;
				case  -10:  setResultAuth(evt.objErreur.codeErreur, ResourceManager.getInstance().getString('M00', 'Acc_s_Refus____Login_ou_Mot_de_passe_invalide_s_'), "(Login) AUTH FAILED : Invalid Credentials");break;
				case   -1:      setResultAuth(evt.objErreur.codeErreur, ResourceManager.getInstance().getString('M00', 'Acc_s_Refus____Erreur_Interne'), "(Login) AUTH FAILED : UNKNOWN ERROR"); break;
				case    0:      setResultAuth(evt.objErreur.codeErreur, ResourceManager.getInstance().getString('M00', 'Acc_s_Refus_'), "(Login) AUTH FAILED : Login expired");break;
				case    1:      setResultAuth(evt.objErreur.codeErreur, "", "(Login) AUTH SUCCESS : " + params.login, params); break;
				case    2:      setResultAuth(evt.objErreur.codeErreur, ResourceManager.getInstance().getString('M00', 'Acc_s_Refus_'), "(Login) AUTH FAILED : Login locked");break;
				case    3:      setResultAuth(evt.objErreur.codeErreur, ResourceManager.getInstance().getString('M00', 'Acc_s_Refus___Adresse_IP_non_autoris_e'), "(Login) AUTH FAILED : Invalid Ip Address");break;
				case  404:      afficherEcranLogin(); break;//token invalide
				case  444:      afficherEcranLoginAlternatif(); break;//token invalide sso login alternatif
				default  :      setResultAuth(evt.objErreur.codeErreur, ResourceManager.getInstance().getString('M00', 'Acc_s_Refus____Erreur_Interne'), "(Login) AUTH FAILED : UNKNOWN ERROR"); break;
			}
		}
		private function setResultAuth(authresult:int, msglogin:String, msgTrace:String, rslt:Object = null):void
		{
			
			if(compLog.MustBeDisplay())
				_compLog.setMessageErreur(msglogin);
			
			if(authresult == 0)
				printAlert(ResourceManager.getInstance().getString('M00', 'Votre_acc_s_est_desactiv__'));
			else if(authresult == 2)
				printAlert(ResourceManager.getInstance().getString('M00', 'Vous_avez_effectu__plusieurs_tentatives_de_connexion_'));
			else if(!_compLog.MustBeDisplay())
				afficherEcranLogin();
			else
				printAlert(msglogin);
		}
		
		private function printAlert(message:String):void
		{
			ConsoviewAlert.afficherAlertInfo(message, ResourceManager.getInstance().getString('M00', 'consoview'),null);
		}
		
		private function closeAllPopups():void
		{	
			var len:Number=systemManager.numChildren - 1;
			var i:Number=len;
			
			while (i >= 0)
			{
				if (systemManager.getChildAt(i).hasOwnProperty("isPopUp"))
				{
					PopUpManager.removePopUp(IFlexDisplayObject(systemManager.getChildAt(i)));
				}
				i--;
			}
		}
		
		public function afficherEcranLoginAlternatif(evt:Event = null):void
		{
			if(_progressBar && _progressBar.isPopUp)
				PopUpManager.removePopUp(_progressBar as IFlexDisplayObject);
			
			closeAllPopups();
			
			params.seSouvenir = false;
			params.token = "";
			deconnectionSPtoken();
		}
		
		protected function deconnectionSPtoken():void
		{
			closeAllPopups();
			_compLog.deconnect(_params,this);
			if(ExternalInterface.available)
			{
				ExternalInterface.call("splogoff");
			}
		}
		
		
		public function afficherEcranLogin(evt:Event = null):void
		{
			
			if(_progressBar && _progressBar.isPopUp)
				PopUpManager.removePopUp(_progressBar as IFlexDisplayObject);
			
			closeAllPopups();
			
			params.seSouvenir = false;
			params.token = "";
			
			if(!_compLog.MustBeDisplay())
			{
				params.codeConnection = Constantes.CODECONNECTION_CLASSIQUE
				defineConnection();
				initListener();
				executeLogin();
			}
			else
			{
				executeLogin();
			}
		}
		/**
		 *      <code>validateLogin</code> 
		 * @param login String valeur du paramètre login
		 * @param mdp String valeur du paramètre mot de passe
		 * @param codeLangue CodeLangue Objet contenant les informations de la langue
		 *  
		 * Cette fonction est appellée par un moduleLogin implémentant l'interface IModuleLogin. 
		 * Elle laisse au composant compLog de valider la demande de login. 
		 * 
		 * @see entity.CodeLangue
		 * @see general.abstract.AbstractConnection
		 * @see interfaceClass.IConnection
		 */
		public function validateLogin(objLogin:Object):void
		{
			compLog.validateLogin(objLogin);
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		public function changeModule(evt:AppControlEvent):void
		{
			var menuDataProvider:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.accessListData;
			var toggledNodes:XMLList = menuDataProvider.descendants("*").(@toggled == 'true');
			var selectedNode:XMLList = menuDataProvider.descendants("*").(@KEY == evt.key );
			// on mets tous les univers / function en toggled = false
			for(var i:int=0; i < toggledNodes.length(); i++)
			{
				toggledNodes[i].@toggled = false;
			}
			// dans le cas d'un univers =>  toggled = true
			// dans le cas d'une fonction => univers et function en toggled = true
			selectedNode.@toggled = 'true';
			if(selectedNode.parent().@KEY != "ROOT")
				selectedNode.parent().@toggled = 'true';
			// On demande au Display d'afficher le module ( comportement par défaut de la classe Abstract
			_compIHM.displayModule();
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		public function askNewPerimetre(evt:AppControlEvent = null,idperimetre:int = -1):void
		{
			if(evt != null)
			{
				if(evt.key is int)
					_compLog.changePerimetre(evt.key as int);
				else if(evt.key is String)
					_compLog.changePerimetre(parseInt(evt.key.toString()));
			}
			else if(idperimetre > -1 )
				_compLog.changePerimetre(idperimetre);
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		public function changeGroupe(evt:AppControlEvent = null,idperimetre:int = -1):void
		{
			if(evt != null)
			{
				if(evt.key is int)
					_compLog.changeGroupe(evt.key as int);
				else if(evt.key is String)
					_compLog.changeGroupe(parseInt(evt.key.toString()));
			}
			else if(idperimetre > -1 )
				_compLog.changeGroupe(idperimetre);
		}
		// ---------------------- --------------- ---------------------------------             
		// ---------------------- FONCTION PRIVEE ---------------------------------
		// ---------------------- --------------- ---------------------------------
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		private function initComposant():void
		{
			initComposantProgressBar();
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		private function initComposantProgressBar():void
		{
			if(_progressBar && _progressBar.isPopUp)
			{
				
				PopUpManager.removePopUp(_progressBar as IFlexDisplayObject);
				_progressBar = null;
			}
			
			_progressBar = new ConsoProgressBar(    
				ResourceManager.getInstance().getString("M00","Traitement_en_cours___"),
				"",
				null,
				true,
				0,
				0,
				true,
				ProgressBarMode.EVENT,
				true,
				_params.codeApp
			);
			initListenerRemoteObjectUtil()
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		private function initListener():void
		{
			if(compLog)
			{
				if(!(compLog as AbstractConnection).hasEventListener(ConnectionEvent.VALID_AUTH))
					(compLog as AbstractConnection).addEventListener(ConnectionEvent.VALID_AUTH,displayIHM)
				if(!(compLog as AbstractConnection).hasEventListener(ConnectionEvent.VALID_ERROR))
					(compLog as AbstractConnection).addEventListener(ConnectionEvent.VALID_ERROR,displayError)
			}
			if(CvAccessManager.singletonInstance)
			{
				if(!CvAccessManager.singletonInstance.hasEventListener(AppControlEvent.CHANGE_MODULE_EVENT))
					CvAccessManager.singletonInstance.addEventListener(AppControlEvent.CHANGE_MODULE_EVENT,changeModule)
				if(!CvAccessManager.singletonInstance.hasEventListener(AppControlEvent.CHANGE_PERIMETRE_EVENT))
					CvAccessManager.singletonInstance.addEventListener(AppControlEvent.CHANGE_PERIMETRE_EVENT,askNewPerimetre)
				if(!CvAccessManager.singletonInstance.hasEventListener(AppControlEvent.CHANGE_GROUPE_EVENT))
					CvAccessManager.singletonInstance.addEventListener(AppControlEvent.CHANGE_GROUPE_EVENT,changeGroupe)
				if(!CvAccessManager.singletonInstance.hasEventListener(AppControlEvent.LOGOFF_EVENT))
					CvAccessManager.singletonInstance.addEventListener(AppControlEvent.LOGOFF_EVENT,deconnect)
			}
			initListenerRemoteObjectUtil();
		}
		private function initListenerRemoteObjectUtil():void
		{
			if(RemoteObjectUtil.getSession() != null)
			{
				if(!RemoteObjectUtil.getSession().hasEventListener(ConsoViewEvent.INVOKE_REMOTE_OPERATION))
					RemoteObjectUtil.getSession().addEventListener(ConsoViewEvent.INVOKE_REMOTE_OPERATION,showProgressBar);
				if(!RemoteObjectUtil.getSession().hasEventListener(ConsoViewEvent.CALL_REMOTE_OPERATION))
					RemoteObjectUtil.getSession().addEventListener(ConsoViewEvent.CALL_REMOTE_OPERATION,showProgressBar);
				if(!RemoteObjectUtil.getSession().hasEventListener(ConsoViewEvent.CANCEL_REMOTE_OPERATIONS))
					RemoteObjectUtil.getSession().addEventListener(ConsoViewEvent.CANCEL_REMOTE_OPERATIONS,unshowProgressBar);
				if(!RemoteObjectUtil.getSession().hasEventListener(ConsoViewEvent.FAULT_REMOTE_OPERATION))
					RemoteObjectUtil.getSession().addEventListener(ConsoViewEvent.FAULT_REMOTE_OPERATION,unshowProgressBar);
				if(!RemoteObjectUtil.getSession().hasEventListener(ConsoViewEvent.RESULT_REMOTE_OPERATION))
					RemoteObjectUtil.getSession().addEventListener(ConsoViewEvent.RESULT_REMOTE_OPERATION,unshowProgressBar);
				if(!RemoteObjectUtil.getSession().hasEventListener(ConsoViewEvent.EXPIRED_SESSION))
					RemoteObjectUtil.getSession().addEventListener(ConsoViewEvent.EXPIRED_SESSION,fctSessionEnd);
			}
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		private function defineConnection():void
		{
			switch(params.codeConnection)
			{
				// Connection france ( cv4 ) 
				case Constantes.CODECONNECTION_CLASSIQUE:
				{
					this.compLog = new ConnectionLoginMDPfrance(progressBar);
					break;
				}
					// Connection SSO SFR
				case Constantes.CODECONNECTION_SSO_SFR:
				{
					this.compLog = new ConnectionSSOsfr(progressBar);
					break;
				}
					// Connection International ( mytem360 )
				case Constantes.CODECONNECTION_INTERNATIONAL:
				{
					this.compLog = new ConnectionLoginMDPInternational(progressBar);
					break;
				}
					
				case Constantes.CODECONNECTION_SP:
				{
					this.compLog = new ConnectionSPmytem360(progressBar);
					break;
				}

				case Constantes.CODECONNECTION_SPTOKEN:
				{
					this.compLog = new ConnectionSPTokenMytem360(progressBar);
					break;
				}

				
					
				case Constantes.CODECONNECTION_SSO_SESOUVENIR:
				{
					this.compLog = new ConnectionSSOSeSouvenir(progressBar);
					break;
				}       
				case Constantes.CODECONNECTION_CONSOLE_GESTION_INTERNATIONAL:
				{
					this.compLog = new ConnectionLoginMDPConsoleGestionInternational(progressBar);
					break;
				}       
				default:
				{
					this.params.codeConnection = Constantes.CODECONNECTION_CLASSIQUE;
					this.compLog = new ConnectionLoginMDPfrance(progressBar);
					break;
				}
			}
		}
		/**
		 *      <code>initDisplay </code> 
		 *      
		 *      initialise l'IHM à afficher.
		 * 
		 * @see
		 */
		protected function initDisplay():void
		{
			this.setStyle("horizontalAlign","center");
			this.setStyle("verticalAlign","middle");
			
			this.width = 1050;
			this.height = 1000;
			
			this.setStyle("backgroundAlpha","0");
			this.setStyle("backgroundColor","#FFFFFF");
			
			this.setStyle("verticalScrollPolicy","off");
			this.setStyle("horizontalScrollPolicy","off");
		}
		
		public function processAppStyle():void
		{       
			var styleString:String= DEFAULT_STYLE; 
			ConsoViewStyleManager.changeStyle(styleString);
		}
		
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		protected function showProgressBar(evt:ConsoViewEvent = null):void
		{
			var remotCount:int = RemoteObjectUtil.RemotingCount;
			
			if(!_progressBar.isPopUp) // On n'affiche la popup que si un remoting ( celui en cours ) ou aucun.
			{
				_progressBar.show(Application.application as DisplayObject);
			}
		}
		/**
		 *      <code> </code> 
		 * @param
		 * @param
		 * 
		 * @see
		 */
		protected function unshowProgressBar(evt:ConsoViewEvent = null):void
		{
			var remotCount:int = RemoteObjectUtil.RemotingCount;
			
			if(_progressBar
				&& _progressBar.isPopUp
				&& remotCount < 1) 
			{
				_progressBar.hide();
			}
		}
		protected function fctSessionEnd(evt:ConsoViewEvent = null):void
		{
			afficherEcranLogin();
		}
		// ---------------------- --------------- ---------------------------------             
		// ---------------------- GETTER / SETTER ---------------------------------
		// ---------------------- --------------- ---------------------------------
		public function get compIHM():AbstractCreateApplication
		{
			return _compIHM;
		}
		public function set compIHM(value:AbstractCreateApplication):void
		{
			_compIHM = value;
		}
		public function get compLog():IConnection
		{
			return _compLog;
		}
		public function set compLog(value:IConnection):void
		{
			_compLog = value;
		}
		public function get params():AppParams
		{
			return _params;
		}
		public function set params(value:AppParams):void
		{
			_params = value;
		}
		public function get progressBar():ConsoProgressBar
		{
			return _progressBar;
		}
		public function set progressBar(value:ConsoProgressBar):void
		{
			_progressBar = value;
		}
	}
}