package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;

	public class RulefournirLigne implements IRuleDisplayItem
	{
		public function RulefournirLigne()
		{
		}

		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem=new IsLigneFixe();
			var r3:IRuleDisplayItem=new IsLigneMobile();
			var r2:IRuleDisplayItem=new HasLigneAssociated();
			
			if (r1.isDisplay() && !r2.isDisplay())
				return true;
			else if (!r1.isDisplay() && !r3.isDisplay())
				return true;
			else
				return false;
		}
	}
}
