package gestionparc.services.sim
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.rpc.events.ResultEvent;
	
	public class SimHandlers
	{
		
		private var myDatas:SimDatas;
		
		public function SimHandlers(instanceOfDatas:SimDatas)
		{
			myDatas=instanceOfDatas;
		}
		
		
		/**
		 * Fonction appellée au retour de la fonction <code>getInfosFicheSim()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetInfosFicheSim()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */
		internal function getInfosFicheSimResultHandler(re:ResultEvent):void
		{
			if ((re.result is Array) && (re.result as Array)[0] == 1)
			{
				myDatas.treatDataGetInfosFicheSim(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.RECUP_FICHE_SIM_IMPOSSIBLE, gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosFicheSim()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosFicheSim()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */
		internal function setInfosFicheSimResultHandler(re:ResultEvent):void
		{
			if (re.result == 1)
			{
				myDatas.treatSetInfosFicheSim();
			}
			else if (re.result == -2)
			{
				ConsoviewAlert.afficherError(gestionparcMessages.NUMERO_SIM_EXISTANT, gestionparcMessages.ERREUR);
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.UPDATE_FICHE_SIM_IMPOSSIBLE, gestionparcMessages.ERREUR);
			}
		}
		
		internal function echangerSimResultHandler(re:ResultEvent):void
		{
			if (re.result && re.result != -1)
			{
				myDatas.treatechangerSim();
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.RECUP_SS_EQPT_DISPO_IMP, gestionparcMessages.ERREUR);
			}
		}
		
		internal function echangerExtResultHandler(re:ResultEvent):void
		{
			if (re.result && re.result != -1)
			{
				myDatas.treatechangerSim();
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.ECHANGE_EXT_DISPO_IMP, gestionparcMessages.ERREUR);
			}
		}
		
		internal function rebusSimResultHandler(re:ResultEvent):void
		{
			if (re.result && re.result != -1)
			{
				myDatas.treatrebusTerminal();
			}
			else
			{
				ConsoviewAlert.afficherError(gestionparcMessages.RECUP_SS_EQPT_DISPO_IMP, gestionparcMessages.ERREUR);
			}
		}
		
	}
}