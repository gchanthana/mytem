package vo.tab
{

	[Bindable]
	public class TabVo
	{

		private var _nom:String;
		private var _position:Number;
		private var _tabId:Number;
		private var _appLoginId:Number;

		public function TabVo()
		{
		}

		public function fill(tab:Object):void
		{
			this._nom=tab.NAME;
			this._position=tab.POSITION;
			this._tabId=tab.ONGLETID;
			this._appLoginId=tab.APP_LOGINID;
		}

		/** ------------------------------ getter et setter -------------------------------------------*/


		public function get nom():String
		{
			return _nom;
		}

		public function set nom(value:String):void
		{
			_nom=value;
		}

		public function get position():Number
		{
			return _position;
		}

		public function set position(value:Number):void
		{
			_position=value;
		}

		public function get tabId():Number
		{
			return _tabId;
		}

		public function set tabId(value:Number):void
		{
			_tabId=value;
		}

		public function get appLoginId():Number
		{
			return _appLoginId;
		}

		public function set appLoginId(value:Number):void
		{
			_appLoginId=value;
		}
	}
}
