<cfcomponent name="BIPublisher" alias="fr.consotel.consoview.api.bi.BIPublisher">
	<cffunction name="validateLogin" access="remote" returntype="numeric">
		<cfargument name="userID" required="true" type="string">
		<cfargument name="password" required="true" type="string">
		<cfset webService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
		<cfset result=webService.validateLogin(arguments.userID,arguments.password)>
		<cfif result EQ TRUE>
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	
	<cffunction name="scheduleReport" access="remote" returntype="numeric">
		<cfargument name="reportAbsolutePath" required="true" type="string">
		<cfargument name="template" required="true" type="string">
		<cfargument name="format" required="true" type="string" hint="values : pdf,rtf,html,excel,excel2000,mhtml,csv,data,flash,powerpoint">
		<cfargument name="remoteDir" required="true" type="string" hint="including end slash separator">
		<cfargument name="arrayOfParamNameValue" required="true" type="array" hint="ArrayOfParamNameValue (BI Publisher WebService Data Types)">
		<cfargument name="userID" required="true" type="string">
		<cfargument name="password" required="true" type="string">
		<cfargument name="deliveryMode" required="true" type="string" default="FTP">
		<cfargument name="filename" required="false" type="string" default="NULL">
		<cfargument name="notifiedEmail" required="false" type="string" default="NULL">
		<cfset scheduleRequest=structNew()>
		<cfset deliveryRequest=structNew()>
		<cfset remoteFileUUID=arguments.filename>
		<cfif arguments.filename EQ "NULL">
			<cfset remoteFileUUID=createUUID()>
		</cfif>
		<cfset deliveryOption=structNew()>
		<cfset localOption=structNew()>
		<cfset fileName=arguments.remoteDir & remoteFileUUID & getFormatFileExtension(arguments.format)>
		<cfif UCase(arguments.deliveryMode) EQ "FILE">
			<cfset deliveryOption["destination"]=fileName>
			<cfset deliveryRequest["localOption"]=deliveryOption>
		<cfelseif UCase(arguments.deliveryMode) EQ "FTP">
			<cfset deliveryOption["ftpServerName"]="container">
			<cfset deliveryOption["ftpUserName"]="services">
			<cfset deliveryOption["ftpUserPassword"]="services">
			<cfset deliveryOption["remoteFile"]=fileName>
			<cfset deliveryRequest["ftpOption"]=deliveryOption>
		</cfif>
		<cfset reportRequest=structNew()>
		<cfset reportRequest["reportAbsolutePath"]=arguments.reportAbsolutePath>
		<cfset reportRequest["parameterNameValues"]=arguments.arrayOfParamNameValue>
		<cfset reportRequest["attributeLocale"]="fr_FR">
		<cfset reportRequest["attributeTemplate"]=arguments.template>
		<cfset reportRequest["attributeFormat"]=arguments.format>
		<cfset reportRequest["flattenXML"]=TRUE>
		<cfset reportRequest["sizeOfDataChunkDownload"]=-1>
		
		<cfset scheduleRequest["deliveryRequest"]=deliveryRequest>
		<cfset scheduleRequest["reportRequest"]=reportRequest>
		<cfset scheduleRequest["jobLocale"]="fr-FR">
		<cfset scheduleRequest["userJobName"]=remoteFileUUID>
		<cfset scheduleRequest["schedulePublicOption"]=TRUE>

		<cfif arguments.notifiedEmail EQ "NULL">
			<cfset scheduleRequest["notificationTo"]="cedric.rapiera@consotel.fr">
		<cfelse>
			<cfset scheduleRequest["notificationTo"]=arguments.notifiedEmail>
		</cfif>
		<cfset scheduleRequest["notifyWhenSuccess"]=TRUE>
		<cfset scheduleRequest["notifyWhenWarning"]=TRUE>
		<cfset scheduleRequest["notifyWhenFailed"]=TRUE>
		
		<cfset webService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
		<cfreturn VAL(webService.scheduleReport(scheduleRequest,arguments.userID,arguments.password))>
	</cffunction>
	
	<cffunction name="getFormatFileExtension" access="remote" returntype="string">
		<cfargument name="format" required="true" type="string">
		<cfset formatUCase=UCase(arguments.format)>
		<cfif formatUCase EQ "EXCEL">
			<cfreturn ".xls">
		<cfelseif formatUCase EQ "PDF">
			<cfreturn ".pdf">
		<cfelseif formatUCase EQ "CSV">
			<cfreturn ".csv">
		<cfelseif formatUCase EQ "XML">
			<cfreturn ".xml">
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<cffunction name="deleteScheduledReport" access="remote" returntype="numeric" hint="just perform an update and does not delete row in database tables">
		<cfargument name="job_id" required="true" type="numeric">
		<cfset webService=createObject("webservice",APPLICATION.BI_SERVICE_URL)>
		<cfset deleteStatus=webService.deleteScheduledReport(arguments.job_id,arguments.userID,arguments.password)>
		<cfif deleteStatus EQ TRUE>
			<cfreturn 1>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	
	<cffunction name="getFtpGeneratedFileInfos" access="remote" returntype="query" hint="Only for FTP delivery">
		<cfargument name="job_id" required="true" type="numeric">
		<!--- 
		<cfargument name="remoteDir" required="true" type="string" hint="including end slash separator">
		<cfargument name="format" required="true" type="string" hint="including the dot caracter : .ext">
		<cfargument name="userID" required="true" type="string">
		<cfargument name="password" required="true" type="string">
		 --->
		<cfquery name="qGetGeneratedFileInfos" datasource="DW">
			select substr(message,ftp_start,(ftp_end-ftp_start)+1) as filepath,
			       CAST(substr(message,size_start,(size_end-size_start)+1) as INTEGER) as fileSizeBytes
			from   (
			        select message, ftp_start, ftp_end,
			               substr(message,ftp_start,(ftp_end-ftp_start)+1) as filepath, size_after,
			               instr(message,'(',size_after,1) + 1 as size_start,
			               instr(message,' bytes)',size_after,1) - 1 as size_end
			        from   (
			                select message,
			                       instr(MESSAGE,'d_ftpd_to0=',1,1)+length('d_ftpd_to0=') as ftp_start,
			                       instr(MESSAGE,CHR(10),instr(MESSAGE,'d_ftpd_to0=',1,1)+length('d_ftpd_to0='),1) as ftp_end,
			                       instr(MESSAGE,'dstmsg0=',1,1)+length('dstmsg0=') as size_after
			                from   (
			                        select UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(MESSAGE,4000,1)) as MESSAGE
			                        from   xmlp_sched_output j
			                        where  j.job_id=#job_id#
                               			   and upper(j.deleted)='N'
			                       ) a
			               ) b
			       ) c
		</cfquery>
		<cfreturn qGetGeneratedFileInfos>
	</cffunction>
	
	<cffunction name="getReportParameters" access="remote" returntype="query">
		<cfargument name="job_id" required="true" type="numeric">
		<cfquery name="qGetReportParameters" datasource="DW">
			select XSCHURL,
				UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(report_parameters,4000,1)) as report_parameters
			from   xmlp_sched_job j
			where  j.job_id=#arguments.job_id#
		</cfquery>
		<cfreturn qGetReportParameters>
	</cffunction>
</cfcomponent>
