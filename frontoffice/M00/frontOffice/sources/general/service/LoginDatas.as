package general.service
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	
	import appli.events.LoginEvent;
	import appli.impersonnification.refreshManager;
	import appli.impersonnification.event.RelogEvent;
	
	import composants.util.ConsoviewAlert;
	
	import general.event.PerimetreEvent;
	import general.event.UserEvent;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Traite les données reçues aux retours des procedures appelées dans <code>CollaborateurServices</code>.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * </pre></p>
	 * 
	 * @see gestionparc.services.collaborateur.CollaborateurServices
	 * 
	 */
	public class LoginDatas extends EventDispatcher
	{
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		public function treatProcessLogin(objResult:Object):void
		{
			if (objResult.hasOwnProperty("CODEERREUR") && refreshManager.isReloading == true)
			{
				trace("LoginDatas.dispatchEvent(OTHERCONNECTION_ERROR)");
				refreshManager.failedRelogging = true;
				dispatchEvent(new RelogEvent(RelogEvent.OTHERCONNECTION_ERROR));
			}
			if (refreshManager.failedRelogging != true)
			{
				trace("LoginDatas.dispatchEvent(LoginEvent.VALIDE)");
				dispatchEvent(new LoginEvent(LoginEvent.VALIDE,objResult));
			}
			refreshManager.failedRelogging = false;
		}
		
		public function treatUpdateLogin(objresult:Object):void
		{
			/*
			 * <!--- RETOUR  : VERIFICATION EXISTENCE COUPLE  LOGIN/ANCIEN PASSWORD
			-- p_retour:=0;      le login/pwd est bon mais il est expire
			-- p_retour:=1;      le login/pwd est bon
			-- p_retour:=2;      le login/pwd est bon mais il est bloque ou le login/pwd n'est pas bon et on l'a bloque
			-- p_retour:=-1;     Erreur inconnue
			-- p_retour:=-10;    on trouve pas le login , le mail n'est pas bon
			-- p_retour:=-11;    Le login est bon mais pas le pwd
			--->
			
			<!--- RETOUR : CHANGMENT MOT DE PASSE
			1 : Succes de maj
			0 : Echec de maj, login inexistant ou password vide
			-1 : Errreur
			--->
			
			*/
			if(objresult.VAL == 1)
			{//SUCCES de MAJ de mot passe
				dispatchEvent(new UserEvent(UserEvent.USER_INFOS_UPDATED_EVENT, true));		
			}
			else if(objresult.VAL == -11)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M00','Erreur_de_mise_jour_de_mot_de_passe_')+ ' ' +ResourceManager.getInstance().getString('M00','the_current_password_entered_is_invalid_'), ResourceManager.getInstance().getString('M00','Erreur'));
			}	
			else
			{//ERREUR MAJ de mot de passe
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M00','Erreur_de_mise_jour_de_mot_de_passe_'), ResourceManager.getInstance().getString('M00','Erreur'));
			}	
		}
		// PUBLICS PROPERTIES (GETTER/SETTER)-----------------------------------------------------
	}
	
}