package general.interfaceClass
{
	import flash.events.Event;
	
	import mx.events.FlexEvent;

	public interface ICreateBanniere
	{
		function logoff(event:Event):void;
		function init(evt:FlexEvent = null):void;
	}
}