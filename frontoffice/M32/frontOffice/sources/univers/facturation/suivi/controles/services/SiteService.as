package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	[Bindable]
	public class SiteService
	{
		
		public var model:SiteModel;
		private var handler:SiteHandler;
		
		public function SiteService()
		{
			model = new SiteModel();
			handler = new SiteHandler(model);
		}
		
		public function getSite(idSite:Number):void{
						
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetSite",
				"getSite",
				handler.getSiteHandler);
			RemoteObjectUtil.callService(opData,idSite);
			
		}
		
	}
	
}