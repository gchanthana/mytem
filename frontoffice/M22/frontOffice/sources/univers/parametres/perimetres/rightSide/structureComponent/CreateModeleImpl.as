package univers.parametres.perimetres.rightSide.structureComponent
{
	import composants.controls.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.Tree;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Event(name="NouveauModeleAnnuler")]
	
	[Event(name="NouveauModeleValider")]
	
	
	[Bindable]
	public class CreateModeleImpl extends VBox
	{
		public var treeModele : Tree;
		public var btValider : Button;
		public var btAnnuler : Button;
		
		/* public var btCouperNiveau : Button;
		public var btCollerNiveau : Button; */
		
		
		public var btInsertModele : Button;		
		public var btSupprimerNiveau : Button;
		public var btTyperNiveau : Button;
		public var btAjouterNiveau : Button;
		public var lblError : Label;
		
		public var txtLibelleModele : TextInputLabeled;
		public var txtCommentaireModele : TextInputLabeled;
		
		
		protected var _modeleStructure: XML = <node TYPE_NIVEAU="Organisation" isBranch="true" NIVEAU = "B">
											  </node>;
												
		protected var _modeleStructureArray : Array = new Array();
		
		protected var lastNode : XML = _modeleStructure;
        
		
		
		
		public function CreateModeleImpl()
		{	
			super();
		}
		
		
		private var _modele : Object;
		public function get nouveauModele():Object{
			return _modele;
		}
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
		
			/* treeModele.dragEnabled = true;
			treeModele.dragMoveEnabled = true;
			treeModele.dropEnabled = true; */
			treeModele.labelField = "@TYPE_NIVEAU";			
			
/* 			treeModele.addEventListener(DragEvent.DRAG_ENTER,treeModeleDragEnterHandler);			
			treeModele.addEventListener(DragEvent.DRAG_OVER,treeModeleDragOverHandler);
			treeModele.addEventListener(DragEvent.DRAG_COMPLETE,treeModeleDragCompleteHandler);
			treeModele.addEventListener(DragEvent.DRAG_DROP,treeModeleDragDropHandler);*/
			
			treeModele.addEventListener(ListEvent.ITEM_EDIT_END, treeModeleItemEditEndHandler);
			treeModele.addEventListener(ListEvent.ITEM_EDIT_BEGIN, treeModeleItemEditBeginHandler);
 			
 						
			treeModele.dataProvider = _modeleStructure;			
			//btInsertModele.addEventListener(MouseEvent.CLICK,btInsertModeleClickHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,btAnnulerClickHandler);
			btValider.addEventListener(MouseEvent.CLICK,btValiderClickHandler);
			//btCollerNiveau.addEventListener(MouseEvent.CLICK,btCollerNiveauClickHandler);
			btSupprimerNiveau.addEventListener(MouseEvent.CLICK,btSupprimerNiveauClickHandler);
			btTyperNiveau.addEventListener(MouseEvent.CLICK,btTyperNiveauClickHandler);
		//	btCouperNiveau.addEventListener(MouseEvent.CLICK,btCouperNiveauClickHandler);
			btAjouterNiveau.addEventListener(MouseEvent.CLICK,btAjouterNiveauClickHandler);
		}
		
		
		
		//========================== HANDLERS ============================================
		protected function treeModeleItemEditBeginHandler(le : ListEvent):void{
			if (String(le.currentTarget.selectedItem.@TYPE_NIVEAU).toUpperCase() == "ORGANISATION"){
				setSelectedItemEditable(false);
				le.preventDefault();				
			}
			
		}
		
		protected function treeModeleItemEditEndHandler(le : ListEvent):void{
			var oldVal:String = XML(le.currentTarget.editedItemRenderer.data).@TYPE_NIVEAU;
			lblError.text = "";
			
			if(checkDoublons(le.currentTarget.itemEditorInstance.text)){				
				
			}else{
				lblError.text = "Le libellé doit être unique";
				le.preventDefault();
			};
			setSelectedItemEditable(false);				
		}
		
		protected function btInsertModeleClickHandler(me : MouseEvent):void{
			
		}
		
		protected function btAnnulerClickHandler(me : MouseEvent, num : Number = 0):void{
			trace("========" + num);
			dispatchEvent(new Event("NouveauModeleAnnuler"));
		}
		protected function btValiderClickHandler(me : MouseEvent):void{
			enregistrerModele();
		}
		protected function btCollerNiveauClickHandler(me : MouseEvent):void{
			
		}
		protected function btCouperNiveauClickHandler(me : MouseEvent):void{
			
		}
		protected function btTyperNiveauClickHandler(me : MouseEvent):void{
			if (treeModele.selectedItem != null){
				setSelectedItemEditable(true);	
			}
		}
		protected function btSupprimerNiveauClickHandler(me : MouseEvent):void{
			//if (treeModele.selectedItem != null){
				//if (treeModele.selectedItem == lastNode){
					trace("Dernier noeud");
					//var parentNode : * = XML(treeModele.selectedItem).parent();
					var parentNode : * = lastNode.parent();									
					if (parentNode != null){
						var result : Boolean =
						treeModele.dataDescriptor.removeChildAt(parentNode,lastNode,0,_modeleStructure);
						/* var result : Boolean =
						treeModele.dataDescriptor.removeChildAt(parentNode,treeModele.selectedItem,0,_modeleStructure); */
						if (result)lastNode = parentNode;
					}	
				//}	
			//}
		}	
			
		protected function btAjouterNiveauClickHandler(me : MouseEvent):void{
			
		 	if (lastNode.@NIVEAU != "J"){
			 		var node : XML = <node/>
				node.@NIVEAU = String.fromCharCode(String(lastNode.@NIVEAU).charCodeAt()+1);
				//node.@TYPE_NIVEAU = "TYPE " + node.@NIVEAU;
				//Incident n° I-01135-AQAV
				node.@TYPE_NIVEAU = "sans titre";
				
				lastNode.appendChild(node);
				treeModele.expandItem(lastNode,true,true,true);
				lastNode = node;
				
				
				var lg : Number = _modeleStructure.children().length();
				trace(_modeleStructure.children().length());	
		 	}else{
		 		lblError.text = "Nombre maximum de niveaux atteind";
		 	}
			
		}
		
		/* protected function treeModeleDragEnterHandler(de : DragEvent):void{
			var dropTarget:Tree = Tree(de.currentTarget);
			if (de.dragInitiator == dropTarget){
				DragManager.acceptDragDrop(UIComponent(de.currentTarget));
			}
		} */
		
		
		/* protected function treeModeleDragOverHandler(de : DragEvent):void{
			var dropTarget:Tree = Tree(de.currentTarget);
			if (de.dragInitiator == dropTarget){
				DragManager.showFeedback(DragManager.MOVE);	
				
				 
		        
	       		trace("**********Self Drag over **************");
	    	}else{
	    		DragManager.showFeedback(DragManager.NONE);	

	    	}	
		} */
		
		
		/* protected function treeModeleDragCompleteHandler(de : DragEvent):void{
			if(de.action == DragManager.MOVE){
				de.preventDefault();
				var ds:DragSource = de.dragSource;
	            var dropTarget:Tree = Tree(de.currentTarget);
	            var items:Array = ds.dataForFormat("items") as Array;                    
	            
	            var nodeSoucre:XML = dropTarget.selectedItem as XML;
	            
	            var r:int = dropTarget.calculateDropIndex(de);
	            dropTarget.selectedIndex = r;
	            var nodeDest:XML = dropTarget.selectedItem as XML;            
	            
	          //  treeModele.getObjectsUnderPoint(mouseX,mouseY);
	            _modeleStructure.insertChildAfter(nodeDest,nodeSoucre);        
			}else{
				de.preventDefault();
			}    
		}
		protected function treeModeleDragDropHandler(de : DragEvent):void{
		} */
		
		//========================== HANDLERS ============================================
		private function enregistrerModele():void{
			if(txtLibelleModele.text != null && txtLibelleModele.text.length > 0){
				var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionmodele.GestionModeleGateWay",
																				"enregistrerModeleEtSctucture",
						      													 enregistrerModeleResultHandler);				
				RemoteObjectUtil.callService(op,txtLibelleModele.text,txtCommentaireModele.text,xmlModeleToArray(_modeleStructure));	
			}else{
				lblError.text = "Le libellé est obligatoire";
			}
		}
		
		private function enregistrerModeleResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				lblError.text = "";
				_modele = _modeleStructure;
				_modele.@IDORGA_NIVEAU = Number(re.result);
				dispatchEvent(new Event("NouveauModeleValider"));
				
			}else if (re.result == -2){
				lblError.text = "Ce libellé est existe déjà";
			}else{
				lblError.text = "Erreur";
			}
		}
		
		private function checkDoublons(libelle : String):Boolean{
			return true;
		}		
		
		
		private function setSelectedItemEditable(editable : Boolean):void{
			if (treeModele.selectedItem != null){
				treeModele.editable = editable;
				if(editable) treeModele.editedItemPosition = {columnIndex:0,rowIndex:treeModele.selectedIndex};	
			}else{
				treeModele.editable = false;
			}
		}
		
		private function xmlModeleToArray(modele:XML):Array{
			var array : Array = new Array();			
			var nodeObj : Object = new Object();			
			nodeObj.NIVEAU = String(modele.@NIVEAU);		
			nodeObj.TYPE_NIVEAU = String(modele.@TYPE_NIVEAU);
			array.push(nodeObj);
			
			
			var node : XMLList;
			node = modele.children();
			
			while(node.length() > 0){
				
				nodeObj = new Object();							
				nodeObj.NIVEAU = String(node[0].@NIVEAU);		
				nodeObj.TYPE_NIVEAU = String(node[0].@TYPE_NIVEAU);
				array.push(nodeObj);
				
				node = node[0].children();
			}
			
			return array;
		}
		
	}
}