<cfcomponent output="false" alias="fr.consotel.consoview.M16.DataToSend">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="DATACOMMMANDE" type="DataCommande" default="">
	<cfproperty name="COMMANDE" type="CommandeVO" default="">
	<cfproperty name="ALLSAMECOMPTE" type="array" default="">
	<cfproperty name="ARTICLE" type="XML" default="">
	<cfproperty name="ARTICLES" type="XML" default="">
	<cfproperty name="ARTICLEARRAY" type="array" default="">

	<cfscript>
		//Initialize the CFC with the default properties values.
		this.DATACOMMMANDE = "";
		this.COMMANDE = "";
		this.ALLSAMECOMPTE = "";
		this.ARTICLE = "";
		this.ARTICLES = "";
		this.ARTICLEARRAY = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="DataToSend">
		<cfreturn this>
	</cffunction>


</cfcomponent>