package univers.facturation.optimisation {
	import flash.net.URLRequest;
	
	public interface IRapport {
		function getReportName():String;
		
		function onPerimetreChange():void;
	}
}
