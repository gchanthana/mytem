<cfcomponent output="false" hint=" pour enregistrer les devices IOS et android" extends="fr.saaswedo.api.datalert.Core">
	
	<cffunction access="private" name="URl_PLAN" returntype="string">
		<cfset url_plan="#url_server#/api/da/plan.cfm">
		<cfreturn  url_plan>
	</cffunction>
	
	<cffunction name="init" access="private" output="true">
	
		<cfargument name="httpRequestData" type="String" required="true">
		
		<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
		<cfset jsonRequest=deserializejson(httpRequestData)>
		<cfset serverName = URl_PLAN()>	
			
		<cfif structKeyExists(#jsonRequest#,"devId") and jsonRequest.devId neq "">
			<cfset  devId = #jsonRequest.devId# >
			<cfset  cause =" cause :  devId = #devId# non trouvé dans la base SWD">
		</cfif>
		
	</cffunction>
	
	<cffunction name="get_planinfo" access="public" returntype="string">
		
		<cfargument name="httpRequestData" type="String" required="true">
		<cfargument name="user_agent" type="String" required="true">
		<cfargument name="ip_origin" type="String" required="false">
		
		<cfset uuid =createuuid() >
		
		<cftry>
			
			<cfset init(httpRequestData)>	
			<!--- logger les données reçues --->
			<cfset saveLog(uuid,"#Debut# get_planinfo","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin#",idMntMobileProxy) >

			<cfset devInfo=bdd.getStoredInfo(devId)>
			
			<cfif devInfo.devid neq "">
		
				<cfhttp method="post" url="#serverName#" useragent="#user_agent#">
					<cfhttpparam type="header" name="Authorization" value="#authorization#"><!--- Authentication basique obligatoire sinon erreur  --->
					<cfhttpparam type="header" name="Accept" value="application/json">
		 			<cfhttpparam type="formfield" name="phone_no" value="#devInfo.phoneNum#"> 	
				 </cfhttp>
			 
			 	<!---  logger les données envoyées à l'API SWD  --->	
				<cfset saveLog(uuid,"#invoke# get_planinfo","#lab_sentData# le phone_no = #devInfo.phoneNum# comme cfhttpparam #newLine# #link# #serverName# ",idMntEventProxyAPI) >
				
				<cfif cfhttp.Responseheader.Status_Code eq "200">
					
					<cflog type="information" text=" response http OK - methode : get_planinfo, on renvoie resp:ok ">
					
					<!--- logger la réponse de l'API SWD  --->
					<cfset saveLog(uuid,"#responseSWD# get_planinfo","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName#",idMntEventAPIProxy) >
					
					<cfset responeJson = deserializeJson(cfhttp.Filecontent) > 
					<cfset responeJson['resp']='OK'> 
					<cflog type="information" text="plan : #serializeJson(responeJson)# ">
					<cfset responeJson = serializeJson(responeJson) >
	
					<!---  logger la réponse envoyée au mobile --->
					<cfset saveLog(uuid,"#responseProxy# get_planinfo","#respHttp# #responeJson#",idMntEventProxyMobile) >
					<cfreturn responeJson>		
					
		 		 <cfelse> <!--- logger le message de l'API SWD en cas d'erreur, par exemple :  {"Message":"phone_no is not present"}  et le meme message est renvoyé au mobile ---> 
					<cflog type="information" text="reponse http not OK - methode : get_planinfo">
					<cfset saveLog(uuid,"#responseSWD# get_planinfo","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName# #newLine# #info#",idMntEventAPIProxy) >
					<cfset responeJson = cfhttp.Filecontent >
					<cfreturn responeJson>
				</cfif>
			<cfelse>
				<cflog type="information" text="device non trouve dans la base SWD - methode : get_planinfo ">
				<cfset responeJson = '{"resp":"Device is not registered"}' >
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# get_planinfo","#respHttp# #responeJson# #newLine# #cause#",idMntEventProxyMobile) >
				<cfreturn responeJson>	
			</cfif>
		<cfcatch type="any"> <!---  logger l'exception --->
			<cfthread action="sleep" duration="1000"/>
			<cfset saveLog(uuid,"#exce# get_planinfo",bodyException(CFCATCH),idMntEventProxyException) >
			<cfset responeJson = '{"resp":"#cfcatch.Message#" }' >
			<cfreturn responeJson>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>