package univers.inventaire.commande.ihm
{
	
	
	import composants.util.ConsoviewUtil;
	
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.equipements.revendeurs.ihm.ListeContactsRevendeurIHM;
	import univers.inventaire.equipements.revendeurs.system.AbstractGestionContacts;
	import univers.inventaire.equipements.revendeurs.system.GestionContactsMobile;
	
	[Bindable]
	public class SelectionContactImpl extends SelectionBaseViewImpl
	{
	    public var btFermer: Button;
	    public var btValider: Button;
	    
		private var _listeSelectedContact:Array = [];
	    public var cpListeContacts:ListeContactsRevendeurIHM;
	    private var _gestionContact:AbstractGestionContacts = new GestionContactsMobile();
	
		public function SelectionContactImpl()
		{	
		}
		
		
		protected function creationCompleteHandler(event:FlexEvent):void
		{	
		  	cpListeContacts.btCreer.visible = false;
			cpListeContacts.btCreer.height = 0;
			
			cpListeContacts.btEditer.visible = false;
			cpListeContacts.btEditer.height = 0;
			
			cpListeContacts.btSupprimer.visible = false
			cpListeContacts.btSupprimer.height = 0;
			
			cpListeContacts.dgListe.allowMultipleSelection = true;
			cpListeContacts.dgListe.doubleClickEnabled = true;
			cpListeContacts.dgListe.addEventListener(ListEvent.CHANGE, _dgListeChangeHandler);
			cpListeContacts.dgListe.addEventListener(MouseEvent.DOUBLE_CLICK,dgListeDoubleClickHandler);
		}
		
		 /**
	     * Ferme la fen�tre
	     * 
	     * @param me    MouseEvent.CLIK
	     */
	    protected function btFermerClickHandler(me:MouseEvent): void
	    {
	    	PopUpManager.removePopUp(this);
	    }

	    /**
	     * Selectionne le revendeur pour la commande
	     * 
	     * @param me    MouseEvent.CLIK
	     */
	    protected function btValiderClickHandler(me:MouseEvent): void
	    {
	    	if (cpListeContacts.dgListe.selectedItem != null)
	    	{
	    		
	    		updateCommande()	
		    	
		    	PopUpManager.removePopUp(this);
		    	
	    	}else{
	    		Alert.show("Vous devez sélectionner un contact !","Erreur");
	    	}
	    }
	    
	    protected function dgListeDoubleClickHandler(event:MouseEvent):void
	    {
	    	if (cpListeContacts.dgListe.selectedItem != null)
	    	{
	    		
	    		updateCommande()	
		    	
		    	PopUpManager.removePopUp(this);
		    	
	    	}else{
	    		Alert.show("Vous devez sélectionner un contact !","Erreur");
	    	}
	    }
	    
	    protected function updateCommande():void
	    {
	    	if (commande != null)
	    	{
	    		
	    		commande.IDCONTACT = cpListeContacts.dgListe.selectedItem.IDCDE_CONTACT;
	    		var prenom:String = (cpListeContacts.dgListe.selectedItem.PRENOM != null)?cpListeContacts.dgListe.selectedItem.PRENOM:"";
	    		commande.PATRONYME_CONTACT = cpListeContacts.dgListe.selectedItem.NOM +" "+ prenom;
	    		commande.EMAIL_CONTACT = cpListeContacts.dgListe.selectedItem.EMAIL_CONTACT;
	    	}
	    }

	    /**
	     * 
	     * @param value 
	     */
	    public function set gestionContact(value :AbstractGestionContacts): void
	    {
	    	_gestionContact = value;
	    }

	    public function get gestionContact(): AbstractGestionContacts
	    {
	    	return _gestionContact;
	    }



		public function set listeSelectedContact(value:Array):void
		{
			_listeSelectedContact = value;
		}

		public function get listeSelectedContact():Array
		{
			return _listeSelectedContact;
		}

		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			var selectedEmails:Array = event.currentTarget.selectedItems;
			
			for (var i:int = 0; i < selectedEmails.length; i++)
			{
				if(!ConsoviewUtil.isPresent(selectedEmails[i],listeSelectedContact))
				{
					listeSelectedContact.push(selectedEmails[i]);		
				}	
			}
		}
	}
}