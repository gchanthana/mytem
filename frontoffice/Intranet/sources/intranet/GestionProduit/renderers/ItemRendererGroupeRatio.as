package intranet.GestionProduit.renderers
{
	import intranet.GestionProduit.DataSample;
	import intranet.GestionProduit.event.ChangeData_EVENT;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class ItemRendererGroupeRatio extends Canvas
	{
		public var cbo:ComboBox= new ComboBox();
		
		public function ItemRendererGroupeRatio()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
// OVVERIDE SET DATA
		override protected function set data(value:Object):void{
			this.data= value;
			for(var i:int=0;i<cbo.dataProvider.length;i++){
				if(data.GROUPE == cbo.dataProvider[i].GROUPE){
					cbo.selectedIndex=i;
					break;
				}
			}
		}
		
// INIT DU TABLEAU		
		public function init(e:FlexEvent):void{
			if(!DataSample.TabGroProOk){
				var tmpOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestionproduits.GestionProduits",
						"getListeGroupe",
						Result,Fault);
				RemoteObjectUtil.callService(tmpOpe);
				DataSample.TabGroProOk=true;
			}
			
			cbo.addEventListener(ListEvent.CHANGE,changeData);
		}
		
		public function result(e:ResultEvent):void{
			var item:Object=new Object();
			item.IDGROUPE_PRODUIT=0;
			item.GROUPE="";
			
			DataSample.TabGroupeProduit=e.result as ArrayCollection;
			DataSample.TabGestionProduit.addItemAt(item,0);
			DataSample.TabGroupeProduit.refresh();
		}
		
		public function fault(e:FaultEvent):void{
			trace("ItemRendererGroupeRatio - innit() :"+e.message);
		}
	
// QUAND ON CHANGE DE DONNÉES DANS LA COMBOBOX	
		public function changeData(e:ListEvent):void{
			var item:Object= new Object();
			
			data.GROUPE=cbo.selectedItem.GROUPE;
			data.IDGROUPE_PRODUIT=cbo.selectedItem.IGGROUPE_PRODUIT;
			
			item.GROUPE=data.GROUPE;
			item.IDGROUPE_PRODUIT=data.IDGROUPE_PRODUIT;
			
			dispatchEvent(new ChangeData_EVENT(ChangeData_EVENT.ChangeData_EVENT,item,true,false);
		}
		
	}
}