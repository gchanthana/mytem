<!--- =========================================================================
Classe: SiteUsageStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="SiteUsageStrategy" hint="This class declares an interface common to all supported algorithms. Context uses this interface to call the algorithm defined by a ConcreteStrategy." >
    
	<cffunction name="getCDRbyRefClient" access="public" returntype="query" output="false"  >
		<cfargument name="IDgroupe" required="false" type="numeric" default="" displayname="numeric siteID" hint="Initial value for the siteIDproperty." />
		<cfargument name="Idref_client" required="false" type="numeric" default="" displayname="numeric siteID" hint="Initial value for the siteIDproperty." />
		<cfargument name="datedeb" required="false" type="date" default="" displayname="date datedeb" hint="Initial value for the datedebproperty." />
		<cfargument name="datefin" required="false" type="date" default="" displayname="date datefin" hint="Initial value for the datefinproperty." />
		<cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getSites" access="public" returntype="query" output="false"  >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
	        <cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getCDRbyCommune" access="public" returntype="query" output="false"  >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDref_client" required="false" type="numeric" default="" displayname="numeric IDref_client" hint="Initial value for the IDref_clientproperty." />
		<cfargument name="Commune" required="false" type="string" default="" displayname="string Commune" hint="Initial value for the Communeproperty." />
		<cfargument name="datedeb" required="false" type="date" default="" displayname="date datedeb" hint="Initial value for the datedebproperty." />
		<cfargument name="datefin" required="false" type="date" default="" displayname="date datefin" hint="Initial value for the datefinproperty." />
	    <cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getSyntheseParVille" access="public" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="Array qui contient les donnÃ©es" />
	        <cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getCDRbySite" access="public" returntype="query" output="false"  >
		<cfargument name="siteID" required="false" type="numeric" default="" displayname="numeric siteID" hint="Initial value for the siteIDproperty." />
		<cfargument name="datedeb" required="false" type="date" default="" displayname="date datedeb" hint="Initial value for the datedebproperty." />
		<cfargument name="datefin" required="false" type="date" default="" displayname="date datefin" hint="Initial value for the datefinproperty." />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
			SELECT 	decode(substr(c.appelant,1,2),'33','0' || substr(c.appelant,3,length(c.appelant)-2),c.appelant) as appelant, decode(substr(c.sda,1,2),'33','0' || substr(c.sda,3,length(c.sda)-2),c.sda) as sda, 
			decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as appele, cout2 as Cout, c.duree, c.paysconsotelID as destination, 
					datedeb, sda.sdaid, c.offreclientID,
					to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID,trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
			FROM 	cdrcurrent@orccdr c, sda, pays_consotel p, site_client scl, cli
			WHERE 	c.offreclientID in (
							SELECT ofc.offreclientid
						   	FROM offre_client ofc, offre_client_site ofcs, site_client scl
						   	WHERE ofc.offreclientid=ofcs.offreclientid
                   			AND ofcs.siteid=scl.siteid
						   	AND scl.siteid=#siteID#
						   	AND valide=1
						   	AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#)
				   			AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#) OR ofc.dateresiliation IS NULL)) 
			AND c.paysconsotelid>0 
			AND p.paysconsotelID=c.paysconsotelID
			AND lower(trim(c.sda))=lower(trim(sda.sda))
			AND lower(trim(c.appelant))=lower(trim(cli.cli))
			AND scl.siteID=cli.siteID
		   	AND cli.cliid=sda.cliid
		   	AND scl.siteid=#siteID#
			AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#,'MM')
			AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#,'MM')
			ORDER BY datedeb, heuredeb asc
		</cfquery>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getSynthese" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
				SELECT SUM(cout/100) as cout,SUM(round(c.duree/60)) as duree, SUM(c.nbappel) as nombre, c.sdaid, 
				 c.cliid, 
				 decode(substr(cli.cli,1,2),'33','0' || substr(cli.cli,3,length(cli.cli)-2),cli.cli) as cli, 
				 decode(substr(sda.sda,1,2),'33','0' || substr(sda.sda,3,length(sda.sda)-2),sda.sda) as sda
				FROM 	stat_mois@orccdr c, cli, sda
				WHERE c.siteid=#var[1]#
				AND c.cliid=cli.cliid
				AND c.sdaid=sda.sdaid
				AND offreclientID IN (
					SELECT ofc.offreclientid
				   FROM offre_client ofc, offre_client_site ofcs, site_client scl
				   WHERE ofc.offreclientid=ofcs.offreclientid
                   AND ofcs.siteid=scl.siteid
				   AND scl.siteid=#var[1]#
				   AND valide=1
				   AND trunc(ofc.datesouscription,'MM')<=trunc(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#,'MM')
				   AND (trunc(ofc.dateresiliation,'MM')>=trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#,'MM') OR ofc.dateresiliation IS NULL))
				AND trunc(c.moisdate,'MM') >= trunc(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#,'MM')
				AND trunc(c.moisdate,'MM') <= trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#,'MM')
				GROUP BY c.cliid, c.sdaid, decode(substr(sda.sda,1,2),'33','0' || substr(sda.sda,3,length(sda.sda)-2),sda.sda)
						, decode(substr(cli.cli,1,2),'33','0' || substr(cli.cli,3,length(cli.cli)-2),cli.cli)
				order by cout desc
			</cfquery>
			<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getSyntheseByRefClient" access="remote" returntype="query" output="false" >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
		<cfabort showerror="Ceci est une classe abstraite. Veuillez dÃ©finir une stratÃ©gie.">
	</cffunction>
	
	<cffunction name="getIndicateurs" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="numeric siteID" hint="Initial value for the siteIDproperty." />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
				SELECT SUM(cout/100) as cout,SUM(c.duree) as duree , SUM(c.nbappel) as nombre, c.sdaid, c.cliid, sda.sda, cli.cli
				FROM 	stat_mois@orccdr c, cli, sda, pays_consotel p
				WHERE c.sdaid=#var[1]#
				AND p.paysconsotelID=c.paysconsotelID
				AND c.cliid=cli.cliid
				AND c.sdaid=sda.sdaid
				AND offreclientID IN (
					SELECT ofc.offreclientid
				   FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sda s
				   WHERE ofc.offreclientid=ofcs.offreclientid
                   AND ofcs.siteid=scl.siteid
				   AND scl.siteID=c.siteID
				   AND c.cliid=s.cliid
				   AND s.sdaid=#var[1]#
				   AND valide=1
				   AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#)
				   AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#) OR ofc.dateresiliation IS NULL))
				AND trunc(c.moisdate,'MM') >= trunc(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#,'MM')
				AND trunc(c.moisdate,'MM') <= trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#,'MM')
				GROUP BY c.cliid, c.sdaid, sda.sda, cli.cli
				ORDER BY cout DESC
			</cfquery>
			<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getEvolution" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="numeric siteID" hint="Initial value for the siteIDproperty." />
		<cfset date1=CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))>
		<cfset date2=CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))>
		<cfset periode=ceiling(12/(365/DateDiff("d",date1,date2)))>
		<cfquery name="qGetNumero" datasource="#session.offreDSN#" timeout="800">
			SELECT to_char(moisdate, 'yyyy/mm/dd') as mois, SUM(cout/100) as cout,SUM(round(c.duree/60)) as duree, SUM(c.nbappel) as nbappel
				FROM 	stat_mois@orccdr c, cli, sda
				WHERE sda.sdaid=#var[1]#
				AND c.cliid=cli.cliid
				AND c.sdaid=sda.sdaid
				AND offreclientID IN (
					SELECT ofc.offreclientid
				   FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli, sda
				   WHERE ofc.offreclientid=ofcs.offreclientid
                   AND ofcs.siteid=scl.siteid
				   AND scl.siteid=cli.siteID
				   AND cli.cliid=sda.cliid
				   AND sda.sdaID=#var[1]#
				   AND valide=1
				   AND trunc(ofc.datesouscription,'MM')<=trunc(add_months(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#,-(#periode#)),'MM')
				   AND (trunc(ofc.dateresiliation,'MM')>=trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#,'MM') OR ofc.dateresiliation IS NULL))
				AND trunc(c.moisdate,'MM') >= trunc(add_months(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#,-(#periode#)),'MM')
				AND trunc(c.moisdate,'MM') <= trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#,'MM')
				GROUP BY to_char(moisdate, 'yyyy/mm/dd')
				order by mois
				
			<!--- SELECT 	to_char(datedeb, 'yyyy/mm')||'/01' as mois
					,sum(cout2)/100 as Cout, count(*) as nbappel, sum(duree) as duree
			FROM 	(select * from cdrcurrent 
					 where 	offreclientID IN (
							   SELECT ofc.offreclientid
							   FROM offre_client@orcoffre ofc, site_client@orcoffre scl, cli@orcoffre c, sda@orcoffre s
							   WHERE scl.clientid=ofc.clientid
							   AND scl.siteID=c.siteID
							   AND c.cliid=s.cliid
							   AND s.sdaid=#var[1]#
							   AND valide=1
							   AND trunc(ofc.datesouscription,'MM')<=trunc(add_months(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#,-(#periode#)),'MM')
							   AND (trunc(ofc.dateresiliation,'MM')>=trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#,'MM') OR ofc.dateresiliation IS NULL))
					 and lower(trim(appelant)) in 
							(select lower(trim(cli)) as cli 
							 from cli@orcoffre c , sda@orcoffre s
							 where c.cliid=s.cliid
				   			 AND s.sdaid=#var[1]#)
				   	 and lower(trim(sda)) in 
							(select lower(trim(s.sda)) as sda 
							 from sda@orcoffre s
							 where s.sdaid=#var[1]#)
					AND trunc(datedeb,'MM') >= trunc(add_months(to_date('#var[2]#','dd/mm/yyyy'),-(#periode#)),'MM')
					AND trunc(datefin,'MM') <= trunc(to_date('#var[3]#','dd/mm/yyyy'),'MM')
					and paysconsotelid>0 and cout2>0) c
			group by to_char(datedeb, 'yyyy/mm')
			order by mois --->
		</cfquery>
			<cfreturn qGetNumero /> 
	</cffunction>
	
	<cffunction name="getDetailLigne" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
				SELECT SUM(cout/100) as cout,SUM(round(c.duree/60)) as duree , SUM(c.nbappel) as nombre, c.sdaid, c.paysconsotelid, c.paysid, c.cliid, 
					trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
				FROM 	stat_mois@orccdr c, cli, sda, pays_consotel p
				WHERE c.sdaid=#var[1]#
				AND p.paysconsotelID=c.paysconsotelID
				AND c.cliid=cli.cliid
				AND c.sdaid=sda.sdaid
				AND offreclientID IN (
					SELECT ofc.offreclientid
				   FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sda s
				   WHERE ofc.offreclientid=ofcs.offreclientid
                            AND ofcs.siteid=scl.siteid
				   AND scl.siteID=c.siteID
				   AND c.cliid=s.cliid
				   AND s.sdaid=#var[1]#
				   AND valide=1
				   AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#)
				   AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#) OR ofc.dateresiliation IS NULL))
				AND trunc(c.moisdate,'MM') >= trunc(#CreateDate(getToken(var[2],3,"/"),getToken(var[2],2,"/"),getToken(var[2],1,"/"))#,'MM')
				AND trunc(c.moisdate,'MM') <= trunc(#CreateDate(getToken(var[3],3,"/"),getToken(var[3],2,"/"),getToken(var[3],1,"/"))#,'MM')
				GROUP BY c.cliid, c.sdaid, c.paysconsotelid, c.paysid,	
						trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays))
				ORDER BY pays
			</cfquery>
			<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getDetailLigneSITE" access="remote" returntype="query" output="false"  >
		<cfargument name="siteID" required="false" type="numeric" default="" displayname="numeric SDAID" hint="Initial value for the SDAIDproperty." />
		<cfargument name="datedeb" required="false" type="string" default="" displayname="date datedeb" hint="Initial value for the datedebproperty." />
		<cfargument name="datefin" required="false" type="string" default="" displayname="date datefin" hint="Initial value for the datefinproperty." />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
				SELECT SUM(cout/100) as cout,SUM(round(c.duree/60)) as duree , SUM(c.nbappel) as nombre, c.sdaid, c.paysconsotelid, c.paysid, c.cliid, 
					trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
				FROM 	stat_mois@orccdr c, sda, pays_consotel p
				WHERE p.paysconsotelID=c.paysconsotelID
				AND c.siteID=#siteID#
				AND c.sdaid=sda.sdaid
				AND offreclientID IN (
					SELECT ofc.offreclientid
				   FROM offre_client ofc, offre_client_site ofcs, site_client scl
				   WHERE ofc.offreclientid=ofcs.offreclientid
                            AND ofcs.siteid=scl.siteid
				   AND scl.siteID=#siteID#
				   AND valide=1
				   AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#)
				   AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#) OR ofc.dateresiliation IS NULL))
				AND trunc(c.moisdate,'MM') >= trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#,'MM')
				AND trunc(c.moisdate,'MM') <= trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#,'MM')
				GROUP BY c.cliid, c.sdaid, c.paysconsotelid, c.paysid,	
						trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays))
				ORDER BY cout DESC
			</cfquery>
			<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getCDRbyPays" access="remote" returntype="query" output="false"  >
		<cfargument name="SDAID" required="false" type="numeric" default="" displayname="numeric SDAID" hint="Initial value for the SDAIDproperty." />
		<cfargument name="PaysconsotelID" required="false" type="numeric" default="" displayname="numeric PaysconsotelID" hint="Initial value for the PaysconsotelIDproperty." />
		<cfargument name="paysID" required="false" type="numeric" default="" displayname="numeric paysID" hint="Initial value for the paysIDproperty." />
		<cfargument name="datedeb" required="false" type="date" default="" displayname="date datedeb" hint="Initial value for the datedebproperty." />
		<cfargument name="datefin" required="false" type="date" default="" displayname="date datefin" hint="Initial value for the datefinproperty." />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
			select  TMP.appelant, TMP.sda, TMP.appele, TMP.Cout, TMP.duree, TMP.destination,
					TMP.datedeb, TMP.sdaid, TMP.heuredeb, TMP.paysid, TMP.pays, TMP.offreclientID, cb.code_budgetaire, lo.libelle as logique,
					op.nom as nom_operateur, sc.sous_compte
			from (
			SELECT  decode(substr(c.appelant,1,2),'33','0' || substr(c.appelant,3,length(c.appelant)-2),c.appelant) as appelant, decode(substr(c.sda,1,2),'33','0' || substr(c.sda,3,length(c.sda)-2),c.sda) as sda, 
			decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as appele, cout2 as Cout, c.duree, c.paysconsotelID as destination, 
					datedeb, sda.sdaid, c.offreclientID,
					to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID,trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
			FROM 	cdrcurrent@orccdr c, sda, pays_consotel p
			WHERE 	c.offreclientID in (
							SELECT ofc.offreclientid
						   	FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sda s
						   	WHERE ofc.offreclientid=ofcs.offreclientid
                            AND ofcs.siteid=scl.siteid
						   	AND scl.siteID=c.siteID
						   	AND c.cliid=s.cliid
						   	AND s.sdaid=#SDAID#
						   	AND valide=1
						   	AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#)
				   			AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#) OR ofc.dateresiliation IS NULL)) 
			AND c.paysconsotelid>0 
			AND c.paysconsotelID=#PaysconsotelID#
			AND c.paysID=#PaysID#
			AND p.paysconsotelID=c.paysconsotelID
			AND lower(trim(c.sda))=lower(trim(sda.sda))
			AND sda.sdaid=#SDAID#
			AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#,'MM')
			AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#,'MM')) TMP,
				sda, cli, sous_tete st, code_budgetaire_sous_tete cbst, code_budgetaire cb,
				logique_budgetaire lo, offre_client ofc, offre o, operateur op,
				site_client scl, sous_compte sc
			where TMP.offreclientID=ofc.offreclientID
			and	ofc.offreid=o.offreid
			and	o.operateurid=op.operateurid
			and	TMP.sdaid=sda.sdaid
			and	sda.cliid=cli.cliid
			and	cli.siteid=scl.siteid
			and	scl.siteid=sc.siteid
			and	cli.cliid=st.cliid
			and	st.idsous_tete=cbst.idsous_tete (+)
			and	cbst.idcode_budgetaire=cb.idcode_budgetaire (+)
			and	cb.logiqueid=lo.logiqueid (+)
			and (cbst.datedeb IS NULL OR cbst.datedeb<=sysdate)
			and (cbst.datefin IS NULL OR cbst.datefin>=sysdate)
			ORDER BY datedeb, heuredeb ASC
		</cfquery>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfreturn qGetAppels />
	</cffunction>
	
	<cffunction name="getResumeAppelParPays" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
			SELECT 	decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele) as appele, sum(cout2)/100 as Cout, SUM(c.duree) as duree,count(*) as nombre,
					trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays,
					decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as display_appele
			FROM 	cdrcurrent@orccdr c, sda, pays_consotel p
			WHERE 	c.offreclientID in (
							SELECT ofc.offreclientid
						   	FROM offre_client ofc, offre_client_site ofcs,site_client scl, cli c, sda s
						   	WHERE ofc.offreclientid=ofcs.offreclientid
                            AND ofcs.siteid=scl.siteid
						   	AND scl.siteID=c.siteID
						   	AND c.cliid=s.cliid
						   	AND s.sdaid=#var[1]#
						   	AND valide=1
						   	AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(var[4],3,"/"),getToken(var[4],2,"/"),getToken(var[4],1,"/"))#)
				   			AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(var[5],3,"/"),getToken(var[5],2,"/"),getToken(var[5],1,"/"))#) OR ofc.dateresiliation IS NULL)) 
			AND c.paysconsotelid>0 
			AND p.paysconsotelID=c.paysconsotelID
			AND c.paysconsotelID=#var[2]#
			AND c.paysID=#var[3]#
			AND lower(trim(c.sda))=lower(trim(sda.sda))
			AND sda.sdaid=#var[1]#
			AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(var[4],3,"/"),getToken(var[4],2,"/"),getToken(var[4],1,"/"))#,'MM')
			AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(var[5],3,"/"),getToken(var[5],2,"/"),getToken(var[5],1,"/"))#,'MM')
			group by decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele), 
					trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)),
					decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))
			ORDER BY nombre desc
		</cfquery>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getDetailAppelParPays" access="remote" returntype="query" output="false"  >
		<cfargument name="var" required="false" type="array" default="" displayname="" hint="" />
		<cfif left(var[4],1) eq "0">
			<cfset temp="33" & right(var[4],len(var[4])-1)>
		<cfelse>
			<cfset temp=var[4]>
		</cfif>
		
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
			SELECT 	decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele) as appele,
					(cout2)/100 as Cout,c.duree, c.paysconsotelID as destination, datedeb,
					to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID,
					trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
			FROM 	cdrcurrent@orccdr c, sda, pays_consotel p
			WHERE 	c.offreclientID in (
							SELECT ofc.offreclientid
						   	FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sda s
						   	WHERE ofc.offreclientid=ofcs.offreclientid
                            AND ofcs.siteid=scl.siteid
						   	AND scl.siteID=c.siteID
						   	AND c.cliid=s.cliid
						   	AND s.sdaid=#var[1]#
						   	AND valide=1
						   	AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(var[5],3,"/"),getToken(var[5],2,"/"),getToken(var[5],1,"/"))#)
				   			AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(var[6],3,"/"),getToken(var[6],2,"/"),getToken(var[6],1,"/"))#) OR ofc.dateresiliation IS NULL)) 
			AND c.paysconsotelid>0 
			AND p.paysconsotelID=c.paysconsotelID
			AND c.paysconsotelID=#var[2]#
			AND c.paysID=#var[3]#
			AND lower(trim(c.sda))=lower(trim(sda.sda))
			AND lower(trim(c.appele))=lower(trim(#temp#))
			AND sda.sdaid=#var[1]#
			AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(var[5],3,"/"),getToken(var[5],2,"/"),getToken(var[5],1,"/"))#,'MM')
			AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(var[6],3,"/"),getToken(var[6],2,"/"),getToken(var[6],1,"/"))#,'MM')
			ORDER BY datedeb, heuredeb ASC
		</cfquery>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="getCDRbySDA" access="remote" returntype="query" output="false"  >
		<cfargument name="SDAID" required="false" type="numeric" default="" displayname="numeric SDAID" hint="Initial value for the SDAIDproperty." />
		<cfargument name="datedeb" required="false" type="date" default="" displayname="date datedeb" hint="Initial value for the datedebproperty." />
		<cfargument name="datefin" required="false" type="date" default="" displayname="date datefin" hint="Initial value for the datefinproperty." />
		<cfquery name="qGetAppels" datasource="#session.offreDSN#">
			select  TMP.appelant, TMP.sda, TMP.appele, TMP.Cout, TMP.duree, TMP.destination,
					TMP.datedeb, TMP.sdaid, TMP.heuredeb, TMP.paysid, TMP.pays, cb.code_budgetaire, lo.libelle as logique,
					op.nom as nom_operateur, sc.sous_compte
			from (
			SELECT  decode(substr(c.appelant,1,2),'33','0' || substr(c.appelant,3,length(c.appelant)-2),c.appelant) as appelant,
					decode(substr(c.sda,1,2),'33','0' || substr(c.sda,3,length(c.sda)-2),c.sda) as sda,
					decode(#session.perimetre.flagdisplaynumber#,0,substr(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele),1,length(decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele))-4) || 'XXXX',decode(substr(c.appele,1,2),'33','0' || substr(c.appele,3,length(c.appele)-2),c.appele)) as appele
					, cout2 as Cout, c.duree, c.paysconsotelID as destination, datedeb, sda.sdaid,
					to_char(c.datedeb,'hh24:mi:ss') as heuredeb, c.paysID, c.offreclientID,
					trim(decode(p.paysconsotelID,4,decode(paysID,1151,'GSM Bouygtel',1150,'GSM SFR',493,'GSM Orange','Autres GSM'),pays)) AS pays
			FROM 	cdrcurrent@orccdr c, sda, pays_consotel p
			WHERE 	c.offreclientID in (
							SELECT ofc.offreclientid
						   	FROM offre_client ofc, offre_client_site ofcs, site_client scl, cli c, sda s
						   	WHERE ofc.offreclientid=ofcs.offreclientid
                            AND ofcs.siteid=scl.siteid
						   	AND scl.siteID=c.siteID
						   	AND c.cliid=s.cliid
						   	AND s.sdaid=#SDAID#
						   	AND valide=1
						   	AND trunc(ofc.datesouscription)<=trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#)
				   			AND (trunc(ofc.dateresiliation)>=trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#)
				   				OR ofc.dateresiliation IS NULL))
			AND c.paysconsotelid>0 
			AND p.paysconsotelID=c.paysconsotelID
			AND lower(trim(c.sda))=lower(trim(sda.sda))
			AND sda.sdaid=#SDAID#
			AND trunc(c.datedeb,'MM') >= trunc(#CreateDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/"))#,'MM')
			AND trunc(c.datefin,'MM') <= trunc(#CreateDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/"))#,'MM')) TMP,
				sda, cli, sous_tete st, code_budgetaire_sous_tete cbst, code_budgetaire cb,
				logique_budgetaire lo, offre_client ofc, offre o, operateur op,
				site_client scl, sous_compte sc
			where TMP.offreclientID=ofc.offreclientID
			and	ofc.offreid=o.offreid
			and	o.operateurid=op.operateurid
			and	TMP.sdaid=sda.sdaid
			and	sda.cliid=cli.cliid
			and	cli.siteid=scl.siteid
			and	scl.siteid=sc.siteid
			and	cli.cliid=st.cliid
			and	st.idsous_tete=cbst.idsous_tete (+)
			and	cbst.idcode_budgetaire=cb.idcode_budgetaire (+)
			and	cb.logiqueid=lo.logiqueid (+)
			and (cbst.datedeb IS NULL OR cbst.datedeb<=sysdate)
			and (cbst.datefin IS NULL OR cbst.datefin>=sysdate)
			ORDER BY datedeb, heuredeb ASC
		</cfquery>
		<cfloop from="1" to="#qGetAppels.recordcount#" index="i">
			<cfset qGetAppels.datedeb[i]=LsDateFormat(qGetAppels.datedeb[i],"dd mmmm yyyy")>
		</cfloop>
		<cfreturn qGetAppels /> 
	</cffunction>
	
	<cffunction name="dirquery2treequery" access="public" returntype="query" output="false"  >
		<cfargument name="dquery" required="false" type="query" default="" displayname="query dquery" hint="Initial value for the dqueryproperty." />
		<cfset var tquery=QueryNew("display,value,parent,societe")>
		<cfset var tempPath=""> 
		<cfset var i=0> 

		<!--- Ajout de la racine --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.libelle_groupe_client[1])>
		<cfset QuerySetCell(tquery,"value",0)>
		<cfset QuerySetCell(tquery,"parent",dquery.libelle_groupe_client[1])>
		
		<!--- Ajout societe initiale --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.libelle[1])>
		<cfset QuerySetCell(tquery,"value",dquery.idref_client[1])>
		<cfset QuerySetCell(tquery,"parent",0)>
		
		<!--- Ajout du Departement --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.dep[1])>
		<cfset QuerySetCell(tquery,"value",dquery.zipkey[1])>
		<cfset QuerySetCell(tquery,"parent",dquery.idref_client[1])>
				
		<!--- Ajout de la commune --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.commune[1])>
		<cfset QuerySetCell(tquery,"value",dquery.communekey[1])>
		<cfset QuerySetCell(tquery,"parent",dquery.zipkey[1])>
		<!--- Ajout du Site --->
		<cfset QueryAddRow(tquery)>
		<cfset QuerySetCell(tquery,"display",dquery.adresse[1])>
		<cfset QuerySetCell(tquery,"value",dquery.siteID[1])>
		<cfset QuerySetCell(tquery,"parent",dquery.communekey[1])>
		<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
		
		<cfset cntj=2>
		<cfloop from="2" to="#dquery.RecordCount#" index="i">
			<cfif dquery.libelle[cntj] neq dquery.libelle[cntj-1]>
				<!--- Ajout des societes --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.libelle[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.idref_client[cntj])>
				<cfset QuerySetCell(tquery,"parent",0)>
				<!--- Ajout du Departement --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.dep[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.zipkey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.idref_client[cntj])>
				<!--- Ajout de la commune --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.commune[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.zipkey[cntj])>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			<cfelseif dquery.dep[cntj] neq dquery.dep[cntj-1]>
				<!--- Ajout du Departement --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.dep[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.zipkey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.idref_client[cntj])>
				<!--- Ajout de la commune --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.commune[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.zipkey[cntj])>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			<cfelseif dquery.commune[cntj] neq dquery.commune[cntj-1]>
				<!--- Ajout de la commune --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.commune[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.zipkey[cntj])>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			<cfelse>
				<!--- Ajout du Site --->
				<cfset QueryAddRow(tquery)>
				<cfset QuerySetCell(tquery,"display",dquery.adresse[cntj])>
				<cfset QuerySetCell(tquery,"value",dquery.siteID[cntj])>
				<cfset QuerySetCell(tquery,"parent",dquery.communekey[cntj])>
				<cfset QuerySetCell(tquery,"societe",dquery.idref_client[1])>
				<cfset cntj=cntj+1>
			</cfif>
		</cfloop>
		<!--- And return the new query --->
		<cfreturn tquery>
	</cffunction>
</cfcomponent>