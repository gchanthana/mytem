<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.MdmServer" extends="fr.saaswedo.api.patterns.cfc.Component"
implements="fr.saaswedo.api.mdm.core.services.IMDM" hint="Implémentation abstraite de base pour représenter un serveur MDM.
Il en est de meme pour getIDeviceInfo() et getIDeviceManagement() dont les instances doivent etre créées et définies dans le constructeur init().
La fonction init() compare la valeur de IMdmServerInfo.getProvider() avec celle de la fonction PROVIDER().
Cette fonction doit etre redéfinie par les implémentations de ce composant. Une exception est levée quand les 2 valeurs sont différentes">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.MdmServer">
		<cfreturn "fr.saaswedo.api.mdm.impl.MdmServer">
	</cffunction>
	
	<cffunction name="checkConnection" returntype="Struct" hint="Vérifie la connexion avec le serveur MDM et retourne une structure
	contenant :<br>- IS_TEST_OK: TRUE si la connexion en succès<br>- TEST_MSG: Message explicatif lié à la vérification de la connexion">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.checkConnection() should be implemented">
	</cffunction>
	
	<cffunction access="public" name="PROVIDER" returntype="String" hint="Doit etre implémentée pour retourner la valeur du provider de ce serveur">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.PROVIDER() should be implemented">
	</cffunction>
	
	<cffunction access="public" name="getIDeviceInfo" returntype="fr.saaswedo.api.mdm.core.services.IDeviceInfo" description="LOCK:ReadOnly">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfif structKeyExists(VARIABLES,"iDeviceInfo")>
				<cfreturn VARIABLES.iDeviceInfo>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="IDeviceInfo instance not defined">
			</cfif>
		</cflock>
	</cffunction>
	
	<cffunction access="public" name="getIDeviceManagement" returntype="fr.saaswedo.api.mdm.core.services.IDeviceManagement" description="LOCK:ReadOnly">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfif structKeyExists(VARIABLES,"iDeviceManagement")>
				<cfreturn VARIABLES.iDeviceManagement>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="IDeviceManagement instance not defined">
			</cfif>
		</cflock>
	</cffunction>
	
	<cffunction access="public" name="getMdmServerInfo" returntype="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" description="LOCK:ReadOnly">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="readOnly" throwontimeout="true">
			<cfif structKeyExists(VARIABLES,"MDM_SERVER_INFOS")>
				<cfreturn VARIABLES.MDM_SERVER_INFOS>
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="#componentName()#: MDM server infos are not defined">
			</cfif>
		</cflock>
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentURL" returntype="String" hint="Cette fonction n'est pas implémentée et lève une exception">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<cfargument name="requireAgent" type="Boolean" required="false" default="true" hint="TRUE pour un enrollment qui requiert un agent MDM">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getEnrollmentURL() should be implemented">
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentServer" returntype="String" hint="Retourne le DNS ou l'IP du serveur d'enrollment">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getEnrollmentServer() should be implemented">
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentServerPort" returntype="Numeric" hint="Retourne le numéro de port du serveur d'enrollment">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.getEnrollmentServerPort() should be implemented">
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.mdm.core.services.IMDM" description="LOCK:Exclusive"
	hint="Lève une exception si la valeur retournée par mdmServerInfos.getProvider() ne correspond pas au provider de ce serveur">
		<cfargument name="mdmServerInfo" type="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" required="true" hint="Infos du serveur MDM">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset var serverInfo=ARGUMENTS.mdmServerInfo>
			<!--- Vérification de la valeur du provider contenue dans les infos MDM fournies (CASE INSENSITIVE) --->
			<cfset var mdmProvider=serverInfo.getProvider()>
			<cfif compareNoCase(mdmProvider,PROVIDER()) EQ 0>
				<cfset SUPER.init()>
				<!--- Initialisation des infos du serveur MDM --->
				<cfset setMdmServerInfo(serverInfo)>
				<!--- Initialisation des services (e.g Instanciation) --->
				<cfset initMdmServices()>
				<cfreturn THIS>
			<!--- Exception levée quand la valeur IMdmServerInfo.getProvider() est invalide --->
			<cfelse>
				<cfset var code=GLOBALS().CONST("MDM.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="#componentName()#: Invalid provider value '#mdmProvider#' found in MDM server info">
			</cfif>
		</cflock>
	</cffunction>

	<!--- =========== Fonctions de gestion des services du serveur =========== --->
	
	<cffunction access="private" name="initMdmServices" returntype="void" description="LOCK:Exclusive"
	hint="Appelée par init() pour initialiser les services du serveur. Cette fonction n'est pas implémentée et lève une exception">
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.initMdmServices() should be implemented">
	</cffunction>
	
	<cffunction access="private" name="setIDeviceInfo" returntype="void" description="LOCK:Exclusive"
	hint="Défini l'implémentation qui sera retournée par setIDeviceInfo(). Toute valeur existante est remplacée">
		<cfargument name="iDeviceInfo" type="fr.saaswedo.api.mdm.core.services.IDeviceInfo" required="true" hint="API d'accès aux infos des devices">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset VARIABLES.iDeviceInfo=ARGUMENTS.iDeviceInfo>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="setIDeviceManagement" returntype="void" description="LOCK:Exclusive"
	hint="Défini l'implémentation qui sera retournée par setIDeviceManagement(). Toute valeur existante est remplacée">
		<cfargument name="iDeviceManagement" type="fr.saaswedo.api.mdm.core.services.IDeviceManagement" required="true" hint="API de gestion des devices">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset VARIABLES.iDeviceManagement=ARGUMENTS.iDeviceManagement>
		</cflock>
	</cffunction>
	
	<!--- =========== Informations concernant le serveur =========== --->
	
	<cffunction access="private" name="setMdmServerInfo" returntype="void" description="LOCK:Exclusive" hint="Défini et remplace les infos du serveur">
		<cfargument name="mdmServerInfo" type="fr.saaswedo.api.mdm.core.services.IMdmServerInfo" required="true" hint="Infos du serveur MDM">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset VARIABLES.MDM_SERVER_INFOS=ARGUMENTS.mdmServerInfo>
		</cflock>
	</cffunction>
	
	<!--- Stockage des logs --->
	<cffunction access="private" name="storeWarning" returntype="void"
	hint="Stocke le log d'erreur. La valeur de log.LOG_TYPE est remplacée par celle de logType">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
		<cfset logInfos.LOG_TYPE=logStorage.WARNING_LOG()>
		<cfset storeLog(logStorage,logInfos)>
	</cffunction>
	
	<cffunction access="private" name="storeError" returntype="void"
	hint="Stocke le log d'erreur. La valeur de log.LOG_TYPE est remplacée par la constante ERROR_LOG() de la librairie de stockage des logs">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
		<cfset logInfos.LOG_TYPE=logStorage.ERROR_LOG()>
		<cfset storeLog(logStorage,logInfos)>
	</cffunction>
	
	<cffunction access="private" name="storeLog" returntype="void" hint="Stocke le log avec la librairie de stockage de log de l'API MDM.
	Ajoute dans le log la clé IDGROUP associée à la valeur de l'identifiant du groupe dans la session utilisateur s'il est défini.
	Ajoute dans le log la clé IDMNT_ID associée à la valeur correspondante commune à toutes les fonctionnalités de l'API MDM">
		<cfargument name="logger" type="fr.saaswedo.utils.log.MDM" required="true" hint="Librairie de stockage du log">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<!--- Stockage et log de l'erreur: Cette implémentation remplace les valeurs de IMNT_CAT,SUBJECTID,BODY
		Toute exception durant le stockage du log est notifiée par mail par la librairie de stockage des logs
		--->
		<cfset var logStorage=ARGUMENTS.logger>
		<cfset var logInfos=ARGUMENTS.log>
		<cfset logInfos.IDMNT_ID=logStorage.DRT_002()>
		<cfset logInfos.BODY_FORMAT=logStorage.HTML_BODY()>
		<!--- TODO: Dans l'attente du refactoring de l'API MDM, la racine est fournie ici --->
		<cfif isDefined("SESSION") AND structKeyExists(SESSION,"PERIMETRE") AND structKeyExists(SESSION.PERIMETRE,"ID_GROUPE")>
			<cfset logInfos.IDGROUP=VAL(SESSION.PERIMETRE.ID_GROUPE)>
		<cfelse>
			<cfset logInfos.IDGROUP=-1>
		</cfif>
		<cfset logStorage.logIt(logInfos)>
	</cffunction>
</cfcomponent>