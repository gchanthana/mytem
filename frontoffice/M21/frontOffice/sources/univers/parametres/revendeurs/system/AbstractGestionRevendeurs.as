package univers.parametres.revendeurs.system
{
    ///////////////////////////////////////////////////////////
    //  AbstractGestionRevendeurs.as
    //  Macromedia ActionScript Implementation of the Class AbstractGestionRevendeurs
    //  Generated by Enterprise Architect
    //  Created on:      20-ao�t-2008 16:26:31
    ///////////////////////////////////////////////////////////
    import composants.util.ConsoviewAlert;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.sampler.DeleteObjectSample;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.events.IndexChangedEvent;
    import mx.resources.ResourceManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;

    /**
     * @version 1.0
     * @created 20-ao�t-2008 16:26:31
     */
    [Bindable]
    public class AbstractGestionRevendeurs extends EventDispatcher
    {
		private var _listeDevises:ArrayCollection = new ArrayCollection();
		//private var _idDevise:int = 0;
        private var _listeMesRevendeurs:ArrayCollection;
        private var _listeRevendeursNonAffectes:ArrayCollection;
        private var _listeTousLesRevendeurs:ArrayCollection;
        private var _listeOperateursDistrib:ArrayCollection = new ArrayCollection();
        private var _listeRevendeurGeneriques:ArrayCollection = new ArrayCollection();
        private var _revendeur:Revendeur;
        private var _currentSla:SlaVO = new SlaVO();
		private var _isDistribGenerique:Boolean = false;
        public var _agence:Agence;
        public var m_AbstractGestionAgences:AbstractGestionAgences = new AbstractGestionAgences();
        public static const REVENDEUR_ENREGISTRE:String = "revendeurEnregistre";
		public static const REVENDEUR_MODIFIE:String = "REVENDEUR_MODIFIE";
        public static const REVENDEUR_AJOUTE:String = "revendeur_ajoute";
        public static const REVENDEUR_SLA_ENREGISTRE:String = "revendeurSlaEnregistre";
        public static const REVENDEUR_SLA_LOADED:String = "revendeur_sla_loaded";
        public static const REVENDEUR_SLA_NOT_YET_EXISTS:String = "revendeur_sla_not_yet_exists";
        public static const LIST_OPERATEUR_LOADED:String = "list_operateur_loaded";
        public static const LIST_REVENDEUR_GENERIQUES_LOADED:String = "list_revendeur_generiques_loaded";
		public static const LISTE_DEVISES_LOADED:String = "liste_devises_loaded";

        public function AbstractGestionRevendeurs():void
        {
            addEventListener("allRevendeur", allRevendeurHandler);
        }

        private function allRevendeurHandler(e:Event):void
        {
            dispatchEvent(new Event("allRevendeurActifOrNot", true));
        }

        /**
         * Ajoute un revendeur � la liste de mes revendeurs
         *
         * @param chaine    si on a choisi un constructeur alors on doit cr�er le
         * revendeur avec le m�me libell� et l'ajouter � la liste de mes revendeurs
         */
        public function affecterRevendeur(revendeur:Revendeur):void
        {
            var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var idRevendeur:Number = revendeur.ID;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", "affecter_revendeur", affecterRevendeurResultHandler);
            RemoteObjectUtil.callService(opData, Idracine, idRevendeur);
        }

        /**
         * Retourne la liste des revendeurs génériques qui ne sont pas constructeurs
         *
         */
        public function getListeRevendeurGenerique():void
        {
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", "getListeRevendeurGenerique", getListeRevendeurGeneriqueResultHandler);
            RemoteObjectUtil.callService(opData);
        }

        /**
         * Réception de la liste des revendeurs génériques
         *
         */
        protected function getListeRevendeurGeneriqueResultHandler(re:ResultEvent):void
        {
            if (ArrayCollection(re.result).length > 0)
            {
                var collec:ArrayCollection = re.result as ArrayCollection;
                listeRevendeurGeneriques = collec;
                dispatchEvent(new Event(LIST_REVENDEUR_GENERIQUES_LOADED));
            }
            else if (re.result == -1)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', '_Probl_me_de_r_cup_ration_des_revendeurs'));
            }
        }

        /**
         * Si on a choisit un constructeur alors on doit cr�er le revendeur et l'ajouter �
         * la liste de mes revendeurs
         *
         * @param re    si on a choisi un constructeur alors on doit cr�er le revendeur
         * avec le m�me libell� et l'ajouter � la liste de mes revendeurs
         */
        protected function affecterRevendeurResultHandler(re:ResultEvent):void
        {
            if (re.result == -1)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', '_deja_affect____'));
            }
            else if (re.result == -2)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', '_Pas_g_n_rique___'));
            }
            else if (re.result == -3)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', '_autre_erreur___'));
            }
            dispatchEvent(new Event(REVENDEUR_AJOUTE));
        }

        /**
         * On retire le vendeur de la liste de mes revendeurs
         *
         * @param revendeur
         */
        public function desaffecterRevendeur(revendeur:Revendeur):void
        {
            var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var idRevendeur:Number = revendeur.ID;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", "desaffecter_revendeur", desaffecterRevendeurResultHandler);
            RemoteObjectUtil.callService(opData, Idracine, idRevendeur);
        }

        /**
         * Affiche une pop up d'information avec les message suivant :
         *
         * si re &gt; 0 'Revendeur d�saffect�'.
         * sir re = -1 'Le revendeur n'etait pas affecte'.
         * si re = -2 'Une erreur s'est produite lors de l'op�ration'
         *
         * @param re
         */
        protected function desaffecterRevendeurResultHandler(re:ResultEvent):void
        {
            if (re.result == -1)
            {
            }
            else if (re.result == -2)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'Une_erreur_se_produire_lors_de_l_op_rati'));
            }
            else
            {
            }
        }

        /**
         * -creer le revendeur
         * -creer la fiche revendeur
         *
         * @param revendeur
         */
        public function enregistrerRevendeur(revendeur:Revendeur):void
        {
            //voir les classes filles 
        }
        /**
         *
         * @param re
         */
        public var idNewRevendeur:int = 0;

        protected function enregistrerRevendeurResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                idNewRevendeur = int(re.result);
                // après creation du revendeur on renseigne ses param SLA
                dispatchEvent(new Event(REVENDEUR_ENREGISTRE));
            }
            else if (re.result == -1)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'Une_erreur_est_survenue'));//Avec l'ancienne procédure (v1), -1 signifiait que le libellé existait déjà, maintenant (v3) c'est pour les erreurs lors de la création
            }
            else if (re.result == -2)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', '_Il_y_avait_une_erreur___'));
            }
        }

        public function editRevendeurSLA(sla:SlaVO):void
        {
            var APP_LOGINID:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
            // Requête pour renseigner les delais du revendeur vis à vis de son opérateur
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", 
				"editRevendeurSLA", enregistrerRevendeurSLAResultHandler);
            if(sla.operateurId > 0)
				RemoteObjectUtil.callService(opData, revendeur.ID, sla.operateurId, APP_LOGINID, sla.delai_livraison, sla.delai_hour_livraison, sla.delai_option, sla.delai_hour_option, sla.delai_resiliation, sla.delai_hour_resiliation, sla.delai_suppression, sla.delai_hour_suppression, sla.delai_reactivation, sla.delai_hour_reactivation)
        }

        public function getRevendeurSLA(idRevendeur:int, idOperator:int):void
        {
            // Requête pour obtenir les caractéristiques sla du revendeur
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", 
				"getRevendeurSLA", getRevendeurSLAResultHandler);
            RemoteObjectUtil.callService(opData, idRevendeur, idOperator);
        }

        public function getlistOperateur(idRevendeur:int):void
        {
            var op:AbstractOperation;
            op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService", 
				"listOpDistrib", getlistOperateur_resultHandler);
            RemoteObjectUtil.callService(op, idRevendeur);
        }

        public function getlistOperateur_resultHandler(re:ResultEvent):void
        {
            if (ArrayCollection(re.result).length > 0)
            {
                var collec:ArrayCollection = re.result as ArrayCollection;
                listeOperateursDistrib = new ArrayCollection();
                // on ajoute à collec les elem dont le statut est à  1 ( ei rattaché au revendeur )
                for (var i:int = 0; i < collec.length; i++)
                {
                    var item:Object = collec.getItemAt(i);
                    if (item.STATUT == 1)
                    {
                        listeOperateursDistrib.addItem(item);
                    }
                }
                dispatchEvent(new Event(LIST_OPERATEUR_LOADED));
            }
            else if (re.result == -1)
            {
				Alert.show(ResourceManager.getInstance().getString('M21', 'Une_erreur_se_produire_lors_de_l_op_rati')); 
            }
        }

        protected function getRevendeurSLAResultHandler(re:ResultEvent):void
        {
            if (ArrayCollection(re.result).length > 0)
            {
                currentSla = SlaVO.serialize(re.result[0]);
                dispatchEvent(new Event(REVENDEUR_SLA_LOADED));
            }
            else /*if (re.result == -2)*/
            {
                dispatchEvent(new Event(REVENDEUR_SLA_NOT_YET_EXISTS));
            }
        }

        protected function enregistrerRevendeurSLAResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                dispatchEvent(new Event(REVENDEUR_SLA_ENREGISTRE));
            }
            else if (re.result == -1)
            {
				Alert.show(ResourceManager.getInstance().getString('M21', '_libell__existant___'),ResourceManager.getInstance().getString('M21', 'Erreur'));
            }
            else if (re.result == -2)
            {
				Alert.show(ResourceManager.getInstance().getString('M21', '_Il_y_avait_une_erreur___'));
				
            }
        }

        /**
         * Affiche le d�tail d'un revendeur et sa fiche
         *
         * @param revendeur
         */
        public function getDetailRevendeur(revendeur:Revendeur):void
        {
        }

        /**
         *
         * @param re
         */
        protected function getDetailRevendeurResultHandler(re:ResultEvent):void
        {
        }

        /**
         *
         * @param revendeur    Mes � jour les donn�es du revendeur et de sa fiche (agence :
         * CDE_CONTACT_SOCIETE)
         *
         * IDREVENEUR > 0
         */
        public function modifierRevendeur(revendeur:Revendeur):void
        {
            var libelle:String = revendeur.Raison_sociale;
            var idrevendeur:Number = revendeur.ID;
            var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var Idcde_contact_societe:int = revendeur.IDCDE_CONTACT_SOCIETE;
            var SIREN:String = revendeur.SIREN;
            var ADRESSE1:String = revendeur.ADRESSE1;
            var ADRESSE2:String = revendeur.ADRESSE2;
            var COMMUNE:String = revendeur.COMMUNE;
            var ZIPCODE:String = revendeur.ZIPCODE;
            var PAYSCONSOTELID:int = revendeur.PAYSCONSOTELID;
            var TELEPHONE:String = revendeur.TELEPHONE;
            var FAX:String = revendeur.FAX;
            var APP_LOGINID:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
            var CODE_INTERNE:String = revendeur.CODE_INTERNE;
            var ID_SOCIETE_MERE:Number = 0;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", "edit_revendeurEtSociete", modifierRevendeurEtSocieteResultHandler);
            RemoteObjectUtil.callService(opData, libelle, idrevendeur, Idracine, Idcde_contact_societe, SIREN, ADRESSE1, ADRESSE2, COMMUNE, ZIPCODE, PAYSCONSOTELID, TELEPHONE, FAX, APP_LOGINID, CODE_INTERNE, ID_SOCIETE_MERE);
        }

        /**
         *
         * @param re
         */
        protected function modifierRevendeurEtSocieteResultHandler(re:ResultEvent):void
        {
            if (re.result > 0)
            {
                dispatchEvent(new Event(REVENDEUR_MODIFIE));
            }
            else if (re.result == -1)
            {
				Alert.show(ResourceManager.getInstance().getString('M21', '_libell__existant___'),ResourceManager.getInstance().getString('M21', 'Erreur'));
            }
            else
            {
				Alert.show(ResourceManager.getInstance().getString('M21', 'Une_erreur_est_survenue'));
            }
        }

        /**
         *
         * @param revendeur
         */
        public function renommerRevendeur(revendeur:Revendeur):void
        {
            var libelle:String = revendeur.Raison_sociale;
            var idrevendeur:Number = revendeur.ID;
            var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", "rename_revendeur", renommerRevendeurResultHandler);
            RemoteObjectUtil.callService(opData, libelle, idrevendeur, Idracine);
        }

        /**
         *
         * @param re
         */
        protected function renommerRevendeurResultHandler(re:ResultEvent):void
        {
            if (re.result == -1)
            {
				Alert.show(ResourceManager.getInstance().getString('M21', '_libell__existant___'),ResourceManager.getInstance().getString('M21', 'Erreur'));
            }
            else if (re.result == -2)
            {
				Alert.show(ResourceManager.getInstance().getString('M21', '_Il_y_avait_une_erreur___'));
            }
            else
            {
				Alert.show(ResourceManager.getInstance().getString('M21', 'Votre_demande_a__t__prise_en_compte'), 'Succes');
            }
        }

        /**
         * Supprime le revendeur s�lectionn�
         * On ne peut supprimer que les revendeurs priv�s � une Racine
         *
         * @param revendeur
         */
        public function supprimerRevendeur(revendeur:Revendeur):void
        {
            var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var idRevendeur:Number = revendeur.ID;
            var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur", "delete_revendeur", supprimerRevendeurResultHandler);
            RemoteObjectUtil.callService(opData, idRevendeur, Idracine);
        }

        /**
         *
         * @param re
         */
        protected function supprimerRevendeurResultHandler(re:ResultEvent):void
        {
            if (re.result == 1)
            {
                ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'Revendeur_supprim___'));
            }
            else if (re.result == -1)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'Le_revendeur_a_des__quipements__'));
            }
            else if (re.result == -2)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'Les_contacts_du_revendeur_sont_rattach_s'));
            }
            else if (re.result == -3)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'l_idracine__donn__est_diff_rent_de_l_id_'));
            }
            else if (re.result == -4)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'Il_ya_des_contacts_qui_ont_un_idracine_d'));
            }
            else if (re.result == -5)
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'Les_contacts_du_revendeur_sont_rattach_s'));
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M21', 'Veuillez_d_saffecter_le_revendeur__'));
            }
        }

        /**
         * methode abstraite
         *
         * @param chaine    cl� de recherche
         */
        public function rechecherMesRevendeurs(chaine:String,onlyThis:Boolean=false):void
        {
        }

        /**
         *
         * @param re
         */
        protected function rechecherMesRevendeursResultHandler(re:ResultEvent):void
        {
        }

        /**
         * methode abstraite
         *
         * @param chaine    cl� de recherche
         */
        public function rechercherRevendeursNonAffectes(chaine:String):void
        {
        }

        /**
         *
         * @param re
         */
        protected function rechercherRevendeursNonAffectesResultHandler(re:ResultEvent):void
        {
        }

        /**
         * methode abstraite
         *
         * @param chaine    cl� de recherche
         */
        public function rechercherTousLesRevendeurs(chaine:String):void
        {
        }

        /**
         *
         * @param re
         */
        protected function rechercherTousLesRevendeursResultHandler(re:ResultEvent):void
        {
        }
		
		public function getDevises():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
				"getDevises", getDevisesResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		public function getDevisesResultHandler(re:ResultEvent):void
		{
			listeDevises = re.result as ArrayCollection;
			dispatchEvent(new Event(AbstractGestionRevendeurs.LISTE_DEVISES_LOADED));
		}

		/**
		 * set/get de la liste des devises
		 */
		public function get listeDevises():ArrayCollection
		{
			return _listeDevises;
		}
		public function set listeDevises(value:ArrayCollection):void
		{
			_listeDevises = value;
		}
        /**
         * getter pour la liste de mes revendeurs (_listeMesRevendeurs)
         */
        public function get listeMesRevendeurs():ArrayCollection
        {
            return _listeMesRevendeurs;
        }

        /**
         * setter pour la liste de mes revendeurs (_listeMesRevendeurs)
         *
         * @param liste
         */
        public function set listeMesRevendeurs(liste:ArrayCollection):void
        {
            _listeMesRevendeurs = liste;
        }

        /**
         * getter pour la liste des revendeurs non affect�s (_listeRevendeursNonAffectes).
         */
        public function get listeRevendeursNonAffectes():ArrayCollection
        {
            return _listeRevendeursNonAffectes;
        }

        /**
         * setter pour la liste des revendeurs non affect�s (_listeRevendeursNonAffectes).
         *
         * @param liste
         */
        public function set listeRevendeursNonAffectes(liste:ArrayCollection):void
        {
            _listeRevendeursNonAffectes = liste;
        }

        /**
         * getter pour la liste des revendeurs (_listeTousLesRevendeurs)
         */
        public function get listeTousLesRevendeurs():ArrayCollection
        {
            return _listeTousLesRevendeurs;
        }

        /**
         * setter pour la liste des revendeurs (_listeTousLesRevendeurs)
         *
         * @param liste
         */
        public function set listeTousLesRevendeurs(liste:ArrayCollection):void
        {
            _listeTousLesRevendeurs = liste;
        }

        /**
         * getter pour le revendeur
         */
        public function get revendeur():Revendeur
        {
            return _revendeur;
        }

        /**
         * setter pour le revendeur
         *
         * @param revendeur
         */
        public function set revendeur(revendeur:Revendeur):void
        {
            _revendeur = revendeur;
        }

        public function get agence():Agence
        {
            return _agence;
        }

        public function set agence(a:Agence):void
        {
            _agence = a;
        }

        public function get listeOperateursDistrib():ArrayCollection
        {
            return _listeOperateursDistrib;
        }

        public function set listeOperateursDistrib(value:ArrayCollection):void
        {
            _listeOperateursDistrib = value;
        }

        public function get currentSla():SlaVO
        {
            return _currentSla;
        }

        public function set currentSla(value:SlaVO):void
        {
            _currentSla = value;
        }
		
		public function get isDistribGenerique():Boolean
		{
			return _isDistribGenerique;
		}
		
		public function set isDistribGenerique(value:Boolean):void
		{
			_isDistribGenerique = value;
		}

        public function get listeRevendeurGeneriques():ArrayCollection
        {
            return _listeRevendeurGeneriques;
        }

        public function set listeRevendeurGeneriques(value:ArrayCollection):void
        {
            _listeRevendeurGeneriques = value;
        }
    } //end AbstractGestionRevendeurs
}