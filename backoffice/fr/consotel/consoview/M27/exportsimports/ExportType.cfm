﻿
	<!--- VARIABLES --->

	<cfset libelleheader 	= FORM.LIBELLE_HEADER>
	
	<cfset filename		 	= FORM.FILE_NAME>
	
	<cfset directory = "/container/importdemasse/">
	
	<cfset uuid = createUUID()>
	
	<cfset path = #directory# & #uuid#>
	
	<!--- CREATION DU REPERTOIRE ET COPIE DU FICHIER *.XLS POUR POUVOIR ECRIRE DEDANS --->

	<cfif not DirectoryExists('#path#')>

		<cfdirectory action="Create" directory="#path#" type="dir" mode="777">
	
	<cfelse>
		
		<cfset uuid = createUUID()>
		
		<cfset path = #directory# & #uuid#>
		
		<cfdirectory action="Create" directory="#path#" type="dir" mode="777">
	
	</cfif>
		
	<cfif DirectoryExists('#path#')>
			
		<cfset templatepath 	= GetCurrentTemplatePath()/>
		<cfset currentdirectory = GetDirectoryFromPath(templatepath)/>
	
		<cffile action="copy" source="#currentdirectory#exportType.xls" destination="#path#" mode="777">
				
		<cffile action="rename" source="#path#/exportType.xls" destination="#path#/#filename#" attributes="normal">
	
	</cfif>
		
	<cfspreadsheet action="read" src="#path#/#filename#" sheetname="Sheet1" name="sObj"/> 
		
	<!--- CREATION DU REPERTOIRE ET COPIE DU FICHIER *.XLS POUR POUVOIR ECRIRE DEDANS --->
	
	<!--- TETE DE COLONNES --->
	<cfset SpreadsheetAddRow(sObj, "#libelleheader#")>
	
	<!--- SAVE IT --->
	<cfspreadsheet action="write" name="sObj" filename="#path#/#filename#" overwrite="true" sheetname="Sheet1"/>

	<!--- TELECHARGEMENT --->
	<cffile action="readbinary" variable="fileContent" file="#path#/#filename#">
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#">
	<cfcontent type="application/vnd.ms-excel" variable="#fileContent#">
	