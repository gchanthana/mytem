package composants.mail.gabarits
{
	import composants.util.ConsoviewFormatter;
	
	public class GabaritBlank extends AbstractGabarit
	{
		public function GabaritBlank()
		{
			super();
		}
		
		
		
		
		
		
		override public function getGabarit():String{
			return	"<p>Société : <b>"+infos.SOCIETE_EXPEDITEUR+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+"<p>Bonjour,</p>"	
					+"<br/>"				
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<br/>"
					+"<p>Cordialement,</p>"
					+"<br/>"
					+"<br/><b>"+infos.NOM_EXPEDITEUR+" "+infos.PRENOM_EXPEDITEUR+".</b>"
					+"<br/><b>"+infos.MAIL_EXPEDITEUR+".</b>"
					+"<br>"
		}
	}
}