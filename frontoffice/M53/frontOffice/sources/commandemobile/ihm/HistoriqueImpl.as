package commandemobile.ihm
{
	import commandemobile.entity.Commande;
	import commandemobile.temp.temp;
	
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import exportData.DataGridDataExporter;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class HistoriqueImpl extends Box
	{
		
//VARIABLES------------------------------------------------------------------------------

		//COMPOSANTS
		public var cbxPools						:ComboBox;
		public var cbxDepuis					:ComboBox;
		public var cbxType						:ComboBox;
		public var cbxPar						:ComboBox;
		public var txtClef						:TextInput;
		public var txtClefSearch				:TextInput;
		public var dcDebut						:CvDateChooser;
		public var dcFin						:CvDateChooser;
		public var rdbtnTous					:RadioButton;
		public var rdbtnCent					:RadioButton;
		public var dgListHistorique				:DataGrid;
		
		
		//OBJECTS
		private var _temp						:temp 				= new temp();
		public var cmd							:Commande;
		
		//VARIABLES LOCALES PUBLICS
		public var dateDebut					:Date 				= new Date();
		public var dateFin						:Date 				= new Date();
		public var listPool						:ArrayCollection 	= new ArrayCollection();
		public var listHistorique				:ArrayCollection 	= new ArrayCollection();
		public var periodeObject				:Object 			= {rangeStart:new Date(new Date().getTime() - (30 * DateFunction.millisecondsPerDay) )};
		public var listDepuis					:ArrayCollection 	= new ArrayCollection();
		public var listType						:ArrayCollection 	= new ArrayCollection();
		public var listPar						:ArrayCollection 	= new ArrayCollection();
		
		//VARIABLES LOCALES PRIAVTE
		private var dateDebutString				:String 			= "";
		private var dateFinString				:String 			= "";
		private var privateHisto				:ArrayCollection 	= new ArrayCollection();
		public var _listHistoriqueReference		:ArrayCollection 	= new ArrayCollection();
		
		private var searchIn					:ArrayCollection 	= new ArrayCollection();
		
		

//VARIABLES------------------------------------------------------------------------------

//METHODES PUBLICS-----------------------------------------------------------------------				
		
		//CONSTRUCTEUR (>LISTENER)
		public function HistoriqueImpl()
		{
			addEventListener("fournirPoolsDuGestionnaire",	showHandler);
			
			_temp.addEventListener("listPool",					listPoolHandler);
			_temp.addEventListener("listeHistoriqueActionOk",	listHistoriqueHandler);
			
			dateLessTwo();
		}

//METHODES PUBLICS-----------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		protected function showHandler(e:Event):void
		{
			_temp.fournirPoolsDuGestionnaire();
		}
		
		protected function rdbtnTousChangeHandler():void
		{
			if(rdbtnTous.selected)
			{
				checkRdbtn(false);
			}
		}
		
		protected function rdbtnCentChangeHandler():void
		{
			if(rdbtnCent.selected)
			{
				checkRdbtn(true);
			}
		}
		
		protected function cbxDepuisCloseHandler(bool:Boolean):void
		{
			txtptSearchHandler();
		}
		
		protected function cbxTypeCloseHandler():void
		{
			txtptSearchHandler();
		}
		
		protected function cbxParCloseHandler():void
		{
			txtptSearchHandler();
		}

		protected function btnSearchClickhandler():void
		{
			btnReinitialisationClickHandler(true);
			cbxDepuis.dataProvider 	= cbxDepuis.dataProvider;
		}
		
		protected function btnReinitClickHandler():void
		{
			txtClefSearch.text = "";
			if(rdbtnTous.selected)
			{
				dgListHistorique.dataProvider = listHistorique;
			}
			else
			{
				dgListHistorique.dataProvider = privateHisto;
			}
			cbxDepuis.dataProvider 	= cbxDepuis.dataProvider;
			cbxPar.dataProvider 	= listPar;
			cbxType.dataProvider 	= listType;
		}
		
		protected function txtptSearchHandler():void
		{
			var dataForPrint:ArrayCollection = new ArrayCollection();
			var arrayC		:ArrayCollection;
			listHistorique = new ArrayCollection();
			if(rdbtnTous.selected)
			{
				arrayC = new ArrayCollection(_listHistoriqueReference.source);
			}
			else
			{
				arrayC = new ArrayCollection(privateHisto.source);
			}
			
			if(txtClefSearch.text == "")
			{
				listHistorique = _listHistoriqueReference;
			}
			else
			{
				for(var i:int = 0;i < arrayC.length;i++)
				{
					if(arrayC[i].DATE_EFFET.toString().toLowerCase().indexOf(txtClefSearch.text.toLowerCase()) != -1
					|| arrayC[i].DESC_ACTION.toString().toLowerCase().indexOf(txtClefSearch.text.toLowerCase()) != -1
					|| arrayC[i].QUI.toString().toLowerCase().indexOf(txtClefSearch.text.toLowerCase()) != -1
					|| arrayC[i].DATE_ACTION.toString().toLowerCase().indexOf(txtClefSearch.text.toLowerCase()) != -1
					|| arrayC[i].COMMANDE.toString().toLowerCase().indexOf(txtClefSearch.text.toLowerCase()) != -1
					|| arrayC[i].SAV.toString().toLowerCase().indexOf(txtClefSearch.text.toLowerCase()) != -1)
					{ 
						dataForPrint.addItem(arrayC[i]); 
					}
				}
				listHistorique = dataForPrint;
			}
			findDataForPrint(listHistorique);
		}
		
		private function searchInData():void
		{
		
		}
		
		protected function btnReinitialisationClickHandler(bool:Boolean):void
		{
			var clef:String = "";
			if(cbxPools != null)
			{
				if(cbxPools.selectedItem != null)
				{
					listHistorique = new ArrayCollection();
					privateHisto = new ArrayCollection();
					if(bool)
					{
						clef = txtClef.text;
						dateDebut = dcDebut.selectedDate;
						dateFin = dcFin.selectedDate;
					}
					else
					{
						dateLessTwo();
						dateFin			= new Date();
						txtClef.text 	= "";
					}
					convertDateToString();
					_temp.getHistoriqueAction(cbxPools.selectedItem.IDPOOL,dateDebutString,dateFinString,clef);
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('Historique', 'Veuillez_s_lectionner_un__pool__'),"Consoview");
				}
			}
		}
		
		protected function btnExporterClickHandler():void
		{
			exportCSVLignes();
		}
		
		private function exportCSVLignes():void 
		{
			if((dgListHistorique.dataProvider as ArrayCollection).length > 0) 
			{	
				var dataToExport	:String = DataGridDataExporter.getCSVString(dgListHistorique,";","\n",false);
				var url				:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/gridexports/exportCSVString.cfm";
				DataGridDataExporter.exportCSVString(url,dataToExport,"export");
			}
			else
			{
				Alert.show("Pas de données à exporter !","Consoview");
			}
		}
		
		protected function btnRetourClickHandler():void
		{
			dispatchEvent(new Event("etapeListCommande",true));
		}

//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PIRVATE-----------------------------------------------------------------------

		private function findDataForPrint(searhArray:ArrayCollection = null):void
		{
			var arrayC		:ArrayCollection = new ArrayCollection();
			var dataSorted	:ArrayCollection = new ArrayCollection();
			
			if(searhArray == null)
			{
				if(rdbtnTous.selected)
				{
					searhArray = new ArrayCollection(_listHistoriqueReference.source);
				}
				else
				{
					searhArray = new ArrayCollection(privateHisto.source);
				}
			}
			
			dataSorted = dateSortCompareFunc(searhArray);
			for(var i:int = 0;i < dataSorted.length;i++)
			{
				if(cbxType.selectedItem.ID  != -1 || cbxPar.selectedItem.ID != -1)
				{
					if(cbxType.selectedItem.ID  != -1 && cbxPar.selectedItem.ID != -1)
					{
						if(dataSorted[i].QUI == cbxPar.selectedItem.QUI && dataSorted[i].DESC_ACTION  ==  cbxType.selectedItem.DESC_ACTION )
						{
							arrayC.addItem(dataSorted[i]);
						}
					}
					else
					{
						if(cbxPar.selectedItem.ID  != -1)
						{
							if(dataSorted[i].QUI == cbxPar.selectedItem.QUI)
							{
								arrayC.addItem(dataSorted[i]);
							}
						}
						if(cbxType.selectedItem.ID  != -1)
						{
							if(dataSorted[i].DESC_ACTION  ==  cbxType.selectedItem.DESC_ACTION )
							{
								arrayC.addItem(dataSorted[i]);
							}
						}
					}
				}
				else
				{
					arrayC.addItem(dataSorted[i]);
				}
			}
			listHistorique = arrayC;
		}
		
		private function dateSortCompareFunc(dataToSearshIn:ArrayCollection):ArrayCollection 
        {
			var dateA		:Date 				= dateReference();											//DATE DE REFERENCE
			var dateB		:Date				= new Date();
			var arrayC		:ArrayCollection 	= new ArrayCollection();
			var dataPro		:ArrayCollection 	= new ArrayCollection(dataToSearshIn.source);
			
			for(var i:int = 0;i < dataPro.length;i++)
			{
				if(dataPro[i].DATE_EFFET != null)
				{
					var tempDateString:String = DateFunction.formatDateAsString(dataPro[i].DATE_EFFET);
            		dateB = new Date(dataPro[i].DATE_EFFET);												//DATE A COMPARER A ALA REFERENCE
            		var code:int = ObjectUtil.dateCompare(dateA, dateB);
            		if(code != 1)
            		{
            			arrayC.addItem(dataPro[i]);
            		}
    			}
			}
            return arrayC;
        }
        
        private function dateReference():Date
        {
        	var thisDate:Date = new Date();
        	switch(cbxDepuis.selectedItem.value)
			{
				case 0: thisDate = new Date(new Date().getTime() - (1 * DateFunction.millisecondsPerDay));//24H  
						break;
				case 1: thisDate = new Date(new Date().getTime() - (7 * DateFunction.millisecondsPerDay));//1 SEMAINE
						break;
				case 2: thisDate = new Date(new Date().getTime() - (31 * DateFunction.millisecondsPerDay));//1 MOIS
						break;
				case 3: thisDate = new Date(new Date().getTime() - (31 * DateFunction.millisecondsPerDay));//TOUS
						break;
			}
			return thisDate;
        }

		private function addCentFirst():void
		{
			for(var i:int = 0;i < listHistorique.length;i++)
			{
				if(i < 100)
				{
					privateHisto.addItem(listHistorique[i]);
				}
			}
		}
		
		private function recupDataForcbx():void
		{
			listPar		= norepeatCbx(_temp.listUser,"QUI");
			listType 	= norepeatCbx(_temp.listType,"DESC_ACTION ");
			checkIfown();
		}
		
		private function norepeatCbx(arrayCollecetion:ArrayCollection,field:String):ArrayCollection
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			if(field == "QUI")
			{
				for(var i:int = 0;i < arrayCollecetion.length;i++)
				{
					if(i == 0)
					{
						arrayC.addItem(arrayCollecetion[i]);
					}
					else
					{
						for(var j:int = 0;j < arrayC.length;j++)
						{
							if(arrayCollecetion[i].QUI != arrayC[j].QUI)
							{
								arrayC.addItem(arrayCollecetion[i]);
							}
						}
						var placeQUI:int = arrayC.length;
						var cptrQUI:int = 0;
						for(var m:int = 0;m < arrayC.length;m++)
						{
							if(arrayC[placeQUI-1].QUI == arrayC[m].QUI)
							{
								cptrQUI++;
							}
							if(cptrQUI != 0 && cptrQUI != 1)
							{
								arrayC.removeItemAt(m);
								m--;
								placeQUI--;
							}
						}					
					}
				}
			}
			else
			{
				for(var k:int = 0;k < arrayCollecetion.length;k++)
				{
					if(k == 0)
					{
						arrayC.addItem(arrayCollecetion[k]);
					}
					for(var l:int = 0;l < arrayC.length;l++)
					{
						if(arrayCollecetion[k].DESC_ACTION  != arrayC[l].DESC_ACTION )
						{
							arrayC.addItem(arrayCollecetion[k]);
						}
					}
					var placeTYPE:int = arrayC.length;
					var cptrTYPE:int = 0;
					for(var n:int = 0;n < arrayC.length;n++)
					{
						if(arrayC[placeTYPE-1].DESC_ACTION  == arrayC[n].DESC_ACTION )
						{
							cptrTYPE++;
						}
						if(cptrTYPE != 0 && cptrTYPE != 1)
						{
							arrayC.removeItemAt(n);
							n--;
							placeTYPE--;
						}
					}	
				}
			}
			return arrayC;
		}
		
		private function checkIfown():void
		{
			var object:Object = new Object();
			object.DESC_ACTION	= "Tous";
			object.QUI 			= "Tous";
			object.APP_LOGINID 	= -1;
			object.ID		 	= -1;
			
			listType.addItemAt(object,0);
			listPar.addItemAt(object,0);
			cbxType.dataProvider 	= listType;
			cbxPar.dataProvider 	= listPar;
		}

		private function convertDateToString():void
		{
			dateDebutString	= formatDate(dateDebut);
			dateFinString	= formatDate(dateFin);
		}
		
		private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return year + "/" + temp + "/" + day;
		}

		private function dateLessTwo():void
		{
			var date:Date = new Date();
			date.setDate(date.date -2);;
			dateDebut = date;
		}
		
		private function checkRdbtn(bool:Boolean):void
		{
			if(bool)
			{
				dgListHistorique.dataProvider = privateHisto;
			}
			else
			{
				dgListHistorique.dataProvider = listHistorique;
			}
		}

//EVENT----------------------------

		private function listPoolHandler(e:Event):void
		{
			if(cbxPools != null)
			{
				if(_temp.listeConcatPools.length > 1)
				{
					cbxPools.prompt = ResourceManager.getInstance().getString('Historique', 'Sélectionnez');
					listPool = _temp.listeConcatPools;
					cbxPools.dataProvider = listPool;
				}
				else
				{
					if(_temp.listeConcatPools.length != 0)
					{
						listPool.addItem(_temp.listeConcatPools[0]);
						cbxPools.dataProvider = listPool;
					}
				}
				if(cbxPools.selectedItem != null)
				{
					btnReinitialisationClickHandler(false);
				}
			}
		}
		
		protected function changeHandler():void
		{
			if(cbxPools.selectedItem != null)
			{
				btnReinitialisationClickHandler(false);
			}
		}
		
		private function listHistoriqueHandler(e:Event):void
		{
			if(_temp.listeHistoriqueAction != null)
			{
				listHistorique 				= _temp.listeHistoriqueAction;
				_listHistoriqueReference 	= _temp.listeHistoriqueAction;
			}
			recupDataForcbx();
			addCentFirst();
		}
		
//EVENT----------------------------

//METHODES PIRVATE-----------------------------------------------------------------------

	}
}
