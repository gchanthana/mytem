<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.Delivery"
extends="fr.consotel.api.ibis.publisher.reporting.impl.ReportingLogging" implements="fr.consotel.api.ibis.publisher.handler.IDelivery"
hint="Stratégie identifiée par le type Delivery qui sert de classe parent pour la plupart des stratégies de diffusion de type IDelivery 
Par défaut cette implémentation rend asynchrone l'exécution du reporting et le traitement correspondant par cette stratégie de diffusion
Les propriétés suivantes doivent etre fournies par la méthode getProperties() du reporting : 
- bipReport : Structure qui contient les propriétés identifiant un rapport BIP à exécuter ou planifier
- bipReport.xdoAbsolutePath : Chemin absolu du XDO
- bipReport.xdoTemplateId : Nom du template du XDO
- bipReporting.outputFormat : Format de sortie (e.g : excel, csv, html, etc...)
- delivery : Structure contenant les propriétés d'exécution de la stratégie de diffusion et qui déterminent le mode de génération du rapport
Les propriétés suivantes sont facultatives :
- bipReport.reportParameters : Structure contenant les paramètres spécifiques au rapport BIP correspondant
Chaque clé correspondant au nom du paramètre du rapport et est associé à une structure contenant une clé VALUES associée à un tableau de ses valeurs
- bipReport.localization : Localisation dont la valeur doit etre fournie au format xx_XX où xx indique le code de la langue et XX celui du pays
- bipReport.timeZone : Time Zone dont la valeur doit etre un TimeZone ID Java i.e une des valeurs retournées par java.util.TimeZone.getAvailableIDs()
- bipReport.burstOption : TRUE pour activer la ventilation et FALSE sinon (Par défaut : FALSE)
- delivery.type : Type de la stratégie de diffusion a utiliser pour l'exécution du reporting. La valeur dépend de l'implémentation IReportingService
- DESTINATION : Nom du serveur de notification HTTP prédéfini dans BIP qui sera utilisé dans le cas d'une planification (Par défaut : NOTIFICATION)
Le JOBNAME utilisé pour le JOB correspondant est le nom du reporting et le fichier correspondant n'est pas généré physiquement
La localisation et le timezone utilisés par défaut sont respectivement : fr_FR et Europe/Paris
La récupération des infos concernant la ventilation n'est pas implémentée par l'API car ils ne sont pas accessibles à partir de BIP">
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IDelivery">
		<!--- Propriétés minimales permettant : Log, Notifications mails de l'API (ReportingLogging) --->
		<cfset SUPER.getService("","")>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getReportingParameters" returntype="Struct">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var reportingProps=reportingImpl.getProperties()>
		<cfreturn reportingProps.bipReport.reportParameters>
	</cffunction>

	<cffunction access="public" name="getDeliveryProperties" returntype="Struct" hint="Retourne une structure contenant :
	- TYPE : Type de la stratégie de diffusion qui est retournée par reporting.getDeliveryType()
	- REPORTING : Nom du reporting qui est retourné par reporting.getReportingName()">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfreturn {TYPE=reportingImpl.getDeliveryType(), REPORTING=reportingImpl.getReportingName()}>
	</cffunction>
	
	<cffunction access="public" name="getDeliveryRequest" returntype="Struct"
	hint="La structure qui est retournée permet de planifier le rapport BIP spécifié par le reporting pour une exécution immédiate avec notification HTTP
	Les propriétés retournées par getDeliveryProperties() ne sont pas utilisées car le fichier correspondant au rapport n'est pas généré physiquement
	Les valeurs des propriétés sont directement récupérés à partir de celles fournies par le reporting et qui sont retournées par reporting.getProperties()
	Une exception est levée si une des propriétés suivantes n'est pas présente dans la structure bipReport : xdoAbsolutePath, xdoTemplateId, outputFormat
	La propriété d'exécution permettant d'utiliser le cache de document BIP est désactivée (i.e : byPassCache est positionné à TRUE)">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<!--- Propriétés du reporting --->
		<cfset var reportingProps=reportingImpl.getProperties()>
		<!--- Structure ScheduleRequest avec DeliveryRequest --->
		<cfset var scheduleRequest = {
			userJobName=reportingImpl.getReportingName(), jobLocale="fr_FR", schedulePublicOption=FALSE, jobTZ="Europe/Paris",
			useUTF8Option=TRUE, scheduleBurstringOption=FALSE, saveDataOption=FALSE, saveOutputOption=FALSE,
			notificationTo="", notifyWhenFailed=TRUE, notifyWhenWarning=TRUE, notifyWhenSuccess=TRUE,
			httpNotificationServer=notificationServer(), httpNotificationUserName="", httpNotificationPassword="",
			deliveryRequest={}
		}>
		<!--- Défini le nom du serveur de notification HTTP qui sera utilisé et qui est prédéfini dans BIP --->
		<cfif structKeyExists(reportingProps,"DESTINATION") AND (LEN(TRIM(reportingProps["DESTINATION"])) GT 0)>
			<cfset scheduleRequest.httpNotificationServer=reportingProps["DESTINATION"]>
		</cfif>
		<!--- Vérifie que les propriétés obligatoires sont fournies dans la structure reporting.getProperties().bipReport --->
		<cfset var requiredKeyList="xdoAbsolutePath,xdoTemplateId,outputFormat">
		<cfloop index="key" list="#requiredKeyList#" delimiters=",">
			<cfif NOT structKeyExists(reportingProps.bipReport,key)>
				<cfthrow attributecollection="#getErrorProperties()#" message="La propriété #key# n'est pas fournie par le reporting"
					detail="Cette propriété doit etre fournie par le reporting dans la clé bipReport">
			</cfif>
		</cfloop>
		<!--- Structure ReportRequest --->
		<cfset scheduleRequest.reportRequest = {
			byPassCache=TRUE, attributeLocale="fr_FR", attributeTimeZone="Europe/Paris",
			reportAbsolutePath=reportingProps.bipReport.xdoAbsolutePath, attributeTemplate=reportingProps.bipReport.xdoTemplateId,
			attributeFormat=reportingProps.bipReport.outputFormat, parameterNameValues=[]
		}>
		<!--- MAJ de la localisation utilisée (Par défaut : fr_FR) --->
		<cfif structKeyExists(reportingProps.bipReport,"localization")>
			<cfset scheduleRequest.reportRequest.attributeLocale=reportingProps.bipReport.localization>
			<cfset scheduleRequest.jobLocale=scheduleRequest.reportRequest.attributeLocale>
		</cfif>
		<!--- MAJ du TimeZone utilisé (Par défaut : Europe/Paris) --->
		<cfif structKeyExists(reportingProps.bipReport,"timeZone")>
			<cfset scheduleRequest.reportRequest.attributeTimeZone=reportingProps.bipReport.timeZone>
			<cfset scheduleRequest.jobTZ=scheduleRequest.reportRequest.attributeTimeZone>
		</cfif>
		<!--- MAJ de l'option de ventilation (Par défaut : FALSE) --->
		<cfif structKeyExists(reportingProps.bipReport,"burstOption")>
			<cfset scheduleRequest.scheduleBurstringOption=reportingProps.bipReport.burstOption>
		</cfif>
		<cfreturn scheduleRequest>
	</cffunction>
	
	<cffunction access="public" name="handleJobId" returntype="void" hint="N'est pas implémentée. Affiche le nom du reporting et le JOBID dans le log">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfargument name="jobId" type="Numeric"  required="true">
		<cfset var deliveryClassName=listLast(getMetadata(THIS).NAME,".")>
		<cfset logInfo(deliveryClassName & ".handleJobId(#ARGUMENTS.reporting.getReportingName()#,#ARGUMENTS.jobId#)")>
	</cffunction>
	
	<cffunction access="public" name="handleReportingEvent" returntype="void" hint="N'est pas implémentée. Affiche le event.getStatus() dans le log">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true">
		<cfset var deliveryClassName=listLast(getMetadata(THIS).NAME,".")>
		<cfset logInfo(deliveryClassName & " event.getStatus() : " & ARGUMENTS["event"].getStatus())>
	</cffunction>
	
	<cffunction access="private" name="notificationServer" returntype="String" hint="Retourne le serveur de notification HTTP utilisé par défaut">
		<cfreturn "NOTIFICATION">
	</cffunction>
</cfcomponent>