package gestioncommande.ihm.listecommande
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.ui.Mouse;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import commandemobile.composants.Livraisonpartiel.LivraisonpartielIHM;
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	import commandemobile.itemRenderer.PanelAffectationEvent;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.ConsoviewAlert;
	import composants.util.URLUtilities;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Pool;
	import gestioncommande.entity.Transporteur;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.entity.ViewStackObject;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.TypeCommandeEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.popup.PopUpAnnulerCommandeIHM;
	import gestioncommande.popup.PopUpCatalogueIHM;
	import gestioncommande.popup.PopUpConfirmationIHM;
	import gestioncommande.popup.PopUpDetailsCommandeIHM;
	import gestioncommande.popup.PopUpLivraisonIHM;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.DataGridDataExporter;
	import gestioncommande.services.Export;
	import gestioncommande.services.Formator;
	import gestioncommande.services.PoolService;
	import gestioncommande.services.RevendeurService;
	import gestioncommande.services.TransporteurSevice;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ListeCommandeResiliationImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListCommande		:DataGrid;
		
		public var cbxPools				:ComboBox;
		public var cbxEtat				:ComboBox;
		public var cbxGest				:ComboBox;
		public var cbxOpe				:ComboBox;
		public var cbxCpte				:ComboBox;
		public var cbxSsCpte			:ComboBox;
		public var cbxExport			:ComboBox;
		
		public var chxSelectAll			:CheckBox;
		public var ckxallCommandes		:CheckBox;
				
		public var txtFiltre			:TextInput;
		public var txtptSearch			:TextInput;
		
		public var rbtTous				:RadioButton;
		public var rbtMobile			:RadioButton;
		public var rbtFixe				:RadioButton;
		public var rbtData				:RadioButton;
		public var rdTous				:RadioButton;
		public var rdCours				:RadioButton;
		public var rdCloses				:RadioButton;
		public var rdAnnul				:RadioButton;
		public var rdbtnMesCommandes	:RadioButton;
		public var rdbtnToutesCommandes	:RadioButton;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpMailBox		:ActionMailBoxIHM;
		private var _popUpTransporteurs	:PopUpLivraisonIHM;
		private var _popUpLivPart		:LivraisonpartielIHM;
		private var _popUpDetails		:PopUpDetailsCommandeIHM; 
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _transSrv			:TransporteurSevice = new TransporteurSevice();
				
		private var _revSrv				:RevendeurService = new RevendeurService();
		
		private var _wflowSrv			:WorkflowService = new WorkflowService();
		
		private var _cmdSrv				:CommandeService = new CommandeService();
		
		private	var _poolsSrv			:PoolService = new PoolService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		private var _selectedAction		:Action = new Action();
		
		private var _cmd				:Commande = new Commande();
		
		private var _infosMail			:InfosObject = new InfosObject();

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//

		public var listeCommandes		:ArrayCollection = new ArrayCollection();
		public var listeExport			:ArrayCollection = new ArrayCollection([
																					{label:ResourceManager.getInstance().getString('M16','Export_d_taill__de_la_s_l_ction'), data:1},
																					{label:ResourceManager.getInstance().getString('M16','Export_non_d_taill__de_la_s_l_ction'), data:2} 
																				]);

		public var isAutorised			:Boolean = false;
		public var isLivPartielle		:Boolean = false;
		public var isLivraison			:Boolean = false;

		public var nbCmdSelected		:Number  = 0;

		private var _pools				:ArrayCollection = new ArrayCollection();
		private var _listeHistorique	:ArrayCollection = new ArrayCollection();
		
		private var _clobIdsCommande	:String  = '';

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//

		public function ListeCommandeResiliationImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
			
			addEventListener(PanelAffectationEvent.DISPLAY_PANEL, panelItemClickHandler);

			getUserProfil();
		}

		public function refreshListeCommande():void
		{
			listeCommandes.removeAll();
			
			getListeCommandesAndFiltres();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//
	
		protected function cbxPoolsChangeHandler(e:Event):void
		{
			razArraycollection();
			
			if(cbxPools.selectedItem != null)
			{
				if(cbxEtat.dataProvider as ArrayCollection && (cbxEtat.dataProvider as ArrayCollection).length == 0)
					getListeCommandesAndFiltres();
				else
					getListeCommandes();
			}
		}
		
		protected function cbxChangeHandler(e:Event):void
		{
			filtreData();
		}
		
		protected function rdbtnClickHandler(me:MouseEvent):void
		{
			filtreData();
		}
		
		protected function btnListeCommandeClickHandler(me:MouseEvent):void
		{
			dispatchEvent(new ViewStackEvent(ViewStackEvent.VS_VIEW_LISTE, true));
		}
		
		protected function btnNewCommandeClickHandler(me:MouseEvent):void
		{
			if(cbxPools.selectedItem != null)
			{
				if(cbxPools.selectedItem.IDPOOL != -1)
				{
					SessionUserObject.singletonSession.POOL 		= mappPool(cbxPools.selectedItem);
					SessionUserObject.singletonSession.POOLSELECTED = mappPool(cbxPools.selectedItem);
					
					var object:Object = SessionUserObject.singletonSession.POOL;
					
					dispatchEvent(new TypeCommandeEvent(TypeCommandeEvent.VIEW_OPETYPECMD, '', '', true));
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool__'), 'Consoview', null);
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool__'), 'Consoview', null);
		}
		
		private function mappPool(valueObject:Object):Pool
		{
			var poolObject:Pool 			= new Pool();
				poolObject.IDPOOL			= valueObject.IDPOOL;
				poolObject.IDPROFIL			= valueObject.IDPROFIL;
				poolObject.IDREVENDEUR		= valueObject.IDREVENDEUR;
				poolObject.LIBELLE_CONCAT	= StringUtil.trim(valueObject.LIBELLE_POOL) + " - " + StringUtil.trim(valueObject.LIBELLE_PROFIL);
				poolObject.LIBELLE_POOL		= valueObject.LIBELLE_POOL;
				poolObject.LIBELLE_PROFIL	= valueObject.LIBELLE_PROFIL; 
				
			return poolObject;
		}
		
		protected function btnHistoriqueClickhandler(me:MouseEvent):void
		{
			if(cbxPools.selectedItem != null)
				SessionUserObject.singletonSession.POOLSELECTED = cbxPools.selectedItem as Pool;
			else
				SessionUserObject.singletonSession.POOLSELECTED = null;
			
			dispatchEvent(new ViewStackEvent(ViewStackEvent.VS_VIEW_HISTORIQUE, true));
		}
		
		protected function btnCatalogueClickhandler(me:MouseEvent):void
		{
			if(cbxPools.selectedItem != null && cbxPools.selectedItem.IDPOOL != -1)
			{
				var catalogue:PopUpCatalogueIHM = new PopUpCatalogueIHM();
					catalogue.idPool 			= cbxPools.selectedItem.IDPOOL;
					catalogue.idrevendeurpool	= cbxPools.selectedItem.IDREVENDEUR;
	
				PopUpManager.addPopUp(catalogue, this, true);
				PopUpManager.centerPopUp(catalogue);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool__'), 'Consoview');
		}
		
		protected function btnExportClickHandler(me:MouseEvent):void
		{
			if(cbxPools.selectedIndex != -1)
			{
				if((dgListCommande.dataProvider as ArrayCollection).length != 0)
				{
					switch(cbxExport.selectedItem.data)
					{
						case 1: exportDetaille(); 	 break;
						case 2: exportNonDetaille(); break; 
					}
				}
				else
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Vous_devez_s_lectionner_au_moins_un__l_m'), 'Consoview');
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_pool__'), 'Consoview');
		}
		
		protected function lblActualiserClickHandler(me:MouseEvent):void
		{
			listeCommandes.removeAll();
			
			getListeCommandes();
		}
		
		protected function chxSelectAllClickHandler(me:MouseEvent):void
		{
			if(dgListCommande.dataProvider)
			{
				if(chxSelectAll.selected)
					nbCmdSelected = (dgListCommande.dataProvider as ArrayCollection).length;
				else
					nbCmdSelected = 0;
			}
			else
				nbCmdSelected = 0;
		}
		
		protected function txtFiltreChangeHandler(e:Event): void
		{	
			filtreData();
		}
		
		protected function ckxallCommandesClickHandler(me:MouseEvent):void
		{
			listeCommandes.removeAll();
			
			getListeCommandesAndFiltres();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getUserProfil():void
		{
			_revSrv.getUserProfil();
			_revSrv.addEventListener(CommandeEvent.IDTYPEPROFIL, userProfilHandler);
		}
		
		public function getPoolsGestionnaires():void
		{
			nbCmdSelected = 0;
			
			poolsGestionnaireHandler();
		}

		private function getListeCommandesAndFiltres():void
		{
			nbCmdSelected = 0;
			
			if(cbxPools.selectedItem == null) return;
			
			_cmdSrv.fournirListesCommandesAndFiltres(cbxPools.selectedItem.IDPOOL,cbxPools.selectedItem.IDPROFIL, ckxallCommandes.selected);
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_LISTED_FIL, listeCommandesAndFiltresHandler);
		}
		
		private function getListeCommandes():void
		{
			nbCmdSelected = 0;
			
			_cmdSrv.fournirListesCommandes(cbxPools.selectedItem.IDPOOL, ckxallCommandes.selected);
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_LISTED, listeCommandesHandler);
		}
		
		private function getTransporteurs():void
		{
			_transSrv.fournirListeTransporteurs();
			_transSrv.addEventListener(CommandeEvent.LISTED_TRANSPORTS, transporteursHandler);
		}
		
		private function getDetailsCommande():void
		{
			_cmdSrv.fournirDetailOperation((dgListCommande.selectedItem as Commande).copyCommande(), (dgListCommande.selectedItem as Commande).copyCommande().IDCOMMANDE);
			_cmdSrv.addEventListener(CommandeEvent.COMMANDE_DETAILS, detailsCommandeHandler);
		}
		
		private function getWorflowHistorique():void
		{
			_wflowSrv.fournirHistoriqueEtatsCommande(dgListCommande.selectedItem as Commande);
			_wflowSrv.addEventListener(CommandeEvent.LISTED_WFLOW_HIST, worflowHistoriqueHandler);
		}
		
		private function sendAction():void
		{
			_wflowSrv.faireActionWorkFlow(_selectedAction, dgListCommande.selectedItem as Commande);
			_wflowSrv.addEventListener(CommandeEvent.SEND_ACTION, sendActionHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function userProfilHandler(cmde:CommandeEvent):void
		{
			SessionUserObject.singletonSession.USERPROFIL = _revSrv.idtypeprofil;
		}

		private function poolsGestionnaireHandler():void
		{
			_pools = new ArrayCollection()
			_pools = ObjectUtil.copy(SessionUserObject.singletonSession.USERPOOLS) as ArrayCollection;
		
			var lenPools:int = _pools.length;
			
			if(lenPools > 1)
				getItemAllPools();
			
			attributDataToCombobox(cbxPools, _pools, ResourceManager.getInstance().getString('M16', 'Aucun_pool'), false);

			lenPools = _pools.length;
			
			if(lenPools == 1)
			{
				getListeCommandesAndFiltres();
			}
			else
			{
				if(SessionUserObject.singletonSession.POOLSELECTED != null)
				{
					var idPool:int = SessionUserObject.singletonSession.POOLSELECTED.IDPOOL;
					
					for(var i:int = 0;i < lenPools;i++)
					{
						if((_pools[i] as Pool).IDPOOL == idPool)
						{
							cbxPools.selectedIndex = i;
							
							getListeCommandesAndFiltres();
							
							return;
						}
					}
				}
			}
		}
		
		private function getItemAllPools():void
		{
			var tous:Pool 			= new Pool();
				tous.IDPOOL 		= -1;
				tous.LIBELLE_CONCAT = ResourceManager.getInstance().getString('M16', 'Tous_les_pools');
				
			_pools.addItemAt(tous, 0);
		}

		private function listeCommandesAndFiltresHandler(cmde:CommandeEvent):void
		{
			attributDataToCombobox(cbxEtat, _cmdSrv.listeEtats, ResourceManager.getInstance().getString('M16', 'Aucun__tat'));
			attributDataToCombobox(cbxGest, _cmdSrv.listeGestionnaires, ResourceManager.getInstance().getString('M16', 'Aucun_Gestionnaire'));
			attributDataToCombobox(cbxOpe, _cmdSrv.listeOperateurs, ResourceManager.getInstance().getString('M16', 'Aucun_op_rateur'));
			attributDataToCombobox(cbxCpte, _cmdSrv.listeComptes, ResourceManager.getInstance().getString('M16', 'Aucun_compte'));
			attributDataToCombobox(cbxSsCpte, _cmdSrv.listeSousComptes, ResourceManager.getInstance().getString('M16', 'Aucun_sous_compte'));

			listeCommandes = _cmdSrv.listeCommandes;
			
			SessionUserObject.singletonSession.LISTEACTIONSUSER 	= _cmdSrv.listeActions;
			SessionUserObject.singletonSession.LISTEACTIONSPROFIL 	= _cmdSrv.listeActionsProfil;
			
			isAutorisedToCommande(_cmdSrv.listeActionsProfil);
			
			callLater(refreshDataprovider);
			
			filtreData();
		}
		
		private function isAutorisedToCommande(values:ArrayCollection):void
		{
			var fixIsAutorised	:Boolean = false;
			var mobIsAutorised	:Boolean = false;
			var len				:int = values.length;
			
			for (var i:int = 0;i < len;i++)
			{
				if(values[i].hasOwnProperty("FIXE"))
					fixIsAutorised = values[i].ACTIF;
				
				if(values[i].hasOwnProperty("MOBILE"))
					mobIsAutorised = values[i].ACTIF;
			}
			
			if(fixIsAutorised == false && mobIsAutorised == false)
				isAutorised = false;
			else
				isAutorised = true;
		}
		
		private function refreshDataprovider():void
		{
			(cbxEtat.dataProvider as ArrayCollection).refresh();
			cbxEtat.validateProperties();
			
			(cbxGest.dataProvider as ArrayCollection).refresh();
			cbxGest.validateProperties();
			
			(cbxOpe.dataProvider as ArrayCollection).refresh();
			cbxOpe.validateProperties();
			
			(cbxCpte.dataProvider as ArrayCollection).refresh();
			cbxCpte.validateProperties();
			
			(cbxSsCpte.dataProvider as ArrayCollection).refresh();
			cbxSsCpte.validateProperties();
		}
		
		private function listeCommandesHandler(cmde:CommandeEvent):void
		{			
			listeCommandes = _cmdSrv.listeCommandes;
			
			filtreData();
		}
		
		private function transporteursHandler(cmde:CommandeEvent):void
		{
			var transporteur :Transporteur = new Transporteur();
			var lenTrans	 :int = _transSrv.listeTransporteurs.length;

			for(var i:int = 0;i < lenTrans;i++)
			{
				if(_transSrv.listeTransporteurs[i].IDTRANSPORTEUR == dgListCommande.selectedItem.IDTRANSPORTEUR)
				{
					transporteur = _transSrv.listeTransporteurs[i];
					break;
				}
			}
			
			if(transporteur.URL_TRACKING != '')
				navigateToURL(new URLRequest(transporteur.URL_TRACKING + dgListCommande.selectedItem.NUMERO_TRACKING),"_blank");
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_transporteur_'), 'Consoview', null);
		}
		
		private function detailsCommandeHandler(cmde:CommandeEvent):void
		{
			_cmd = _cmdSrv.currentCommande.copyCommande();
			
			popUpmanageLivraison();
		}
		
		private function worflowHistoriqueHandler(cmde:CommandeEvent):void
		{
			if(_selectedAction != null)
			{
				_listeHistorique = new ArrayCollection();
				_listeHistorique = _wflowSrv.historiqueWorkFlow;
				
				if(_selectedAction.CODE_ACTION == 'ANNUL')
				{
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16', 'Etes_vous_sur_de_vouloir_annuler_cette_c'), 'Consoview', resultAlertWorkflow);
				}
				else if(_selectedAction.CODE_ACTION != null)
				{
					if(_selectedAction.CODE_ACTION == 'EXPED')
					{
						popUpTransporteur();
					}
					else if(_selectedAction.CODE_ACTION == 'LIVRE')
						{
							popUpMail(true);
						}
						else
						{
							popUpMail();
						}
				}
				else
					sendAction();
			}
		}
		
		private function sendActionHandler(cmde:CommandeEvent):void
		{
			getListeCommandes();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			addEventListener('COMMANDE_SELECTED', compteurNbrCmdHandler);
			addEventListener('REMOVE_COMMANDE', compteurNbrCmdHandler);
		}
		
		private function showHandler(fe:FlexEvent):void
		{
		}
		
		private function panelItemClickHandler(pae:PanelAffectationEvent):void
		{
			if(pae.codeItem == 'VIEW_CMD' || pae.codeItem == 'VIEW_CMD_PDF' || pae.codeItem == 'VIEW_CMD_TRACKING' || pae.codeItem == 'VIEW_SAISETECHNIQUE')
			{
				switch(pae.codeItem)
				{
					case "VIEW_CMD"				: viewCommande();	break;
					case "VIEW_CMD_PDF"			: viewPDF();		break;
					case "VIEW_CMD_TRACKING"	: viewTracking();	break;
					case "VIEW_SAISETECHNIQUE"	: viewLivraison();	break;
				}
			}
			else
			{
				_selectedAction = pae.actionItem;
				
				if(_selectedAction.IDACTION == 2166 || _selectedAction.IDACTION == 2229)
				{
					viewModifCmde();
				}
				else if(_selectedAction.IDACTION == 2316 || _selectedAction.IDACTION == 2317)
					{
						isLivPartielle = true;
						viewLivraison();
					}
					else
						getWorflowHistorique();
			}
		}
		
		private function resultAlertWorkflow(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				sendAction();
		}
		
		private function resultAlertExport(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				exportData();
		}
		
		private function compteurNbrCmdHandler(e:Event):void
		{
			if(e.type == 'COMMANDE_SELECTED')
			{
				nbCmdSelected++;
			}
			else if(e.type == 'REMOVE_COMMANDE')
				{
					nbCmdSelected --;
				}
		}

		//--------------------------------------------------------------------------------------------//
		//					PANEL ACTION
		//--------------------------------------------------------------------------------------------//

		private function viewCommande():void
		{
			if(dgListCommande.selectedItem != null)
			{
				_popUpDetails 			 = new PopUpDetailsCommandeIHM();
				_popUpDetails.myCommande = (dgListCommande.selectedItem as Commande).copyCommande();
				_popUpDetails.addEventListener('DETAILS_CLOSED', popUpDetailsHandler);
				
				PopUpManager.addPopUp(_popUpDetails, this, true);
				PopUpManager.centerPopUp(_popUpDetails);
			}
		}
		
		private function viewPDF():void
		{
			if(dgListCommande.selectedItem.IDCOMMANDE > 0)
			{
				var export:Export = new Export();
					export.exporterCommandeEnPDF(dgListCommande.selectedItem as Commande);
			}	
		}
		
		private function viewTracking():void
		{
			if(dgListCommande.selectedItem == null) return;
			
			var boolNumTracking	:Boolean = false;
			var boolTransporteur:Boolean = false;
			var message			:String  = "";
			
			if(dgListCommande.selectedItem.NUMERO_TRACKING != "")
				boolNumTracking = true	    		
			else
				message += ResourceManager.getInstance().getString('M16', 'Pas_de_num_ro_de_suivi_sp_cifi__n')
			
			if(dgListCommande.selectedItem.IDTRANSPORTEUR)
				boolTransporteur = true	    		
			else
				message += ResourceManager.getInstance().getString('M16', 'Pas_de_transporteur_sp_cifi_')
			
			if(boolTransporteur && boolNumTracking)
				getTransporteurs();
			else
				ConsoviewAlert.afficherAlertInfo(message, 'Consoview', null);
		}
		
		private function viewLivraison():void
		{
			if(dgListCommande.selectedItem != null)
				getDetailsCommande();
		}
		
		private function viewModifCmde():void
		{
			if(dgListCommande.selectedItem != null)
			{
				SessionUserObject.singletonSession.COMMANDEARRAYCOL = new ArrayCollection();
				SessionUserObject.singletonSession.COMMANDEARTICLE	= new ArrayCollection();
				SessionUserObject.singletonSession.ARTICLES			= new ArrayCollection();
				
				SessionUserObject.singletonSession.COMMANDE	= new Commande();
				SessionUserObject.singletonSession.COMMANDE = (dgListCommande.selectedItem as Commande).copyCommande();
				
				dispatchEvent(new ViewStackEvent(ViewStackEvent.VS_VIEW_MODIFCMDES, true));
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_une_commande__'), 'Consoview', null);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private function popUpmanageLivraison():void
		{
			_popUpLivPart 			= new LivraisonpartielIHM();
			_popUpLivPart.commande 	= _cmd.copyCommande();
									
			if(_cmd.IDTYPE_COMMANDE == 1382 || _cmd.IDTYPE_COMMANDE == 1587)
				_popUpLivPart.isCommandeLivre = true;
			
			if(isLivPartielle && SessionUserObject.singletonSession.USERPROFIL > 0)
				_popUpLivPart.isAccesLivraison = true;
			
			_popUpLivPart.addEventListener("MAIL_LIV_PART", popUplivraisonHandler);
			
			isLivPartielle 	= false;
			isLivraison 	= false;
			
			PopUpManager.addPopUp(_popUpLivPart, this, true);
			PopUpManager.centerPopUp(_popUpLivPart);
		}
		
		private function popUpTransporteur():void
		{	
			if(dgListCommande.selectedItem != null)
			{
				_popUpTransporteurs 			 = new PopUpLivraisonIHM();
				_popUpTransporteurs.myCommande = dgListCommande.selectedItem as Commande;		
				_popUpTransporteurs.addEventListener('TRANSPORTEUR_IGNORED',transporteurIsSelectedCloseHandler);
				_popUpTransporteurs.addEventListener('TRANSPORTEUR_VALIDATE',transporteurIsSelectedCloseHandler);
				
				PopUpManager.addPopUp(_popUpTransporteurs,this,true);
				PopUpManager.centerPopUp(_popUpTransporteurs);
			}
		}
		
		private function popUpMail(isLiv:Boolean = false):void
		{
			_popUpMailBox = new ActionMailBoxIHM();
			
			_infosMail.commande 		 	= (dgListCommande.selectedItem as Commande);
			_infosMail.historiqueCommande 	= _listeHistorique;
			_infosMail.corpMessage 		  	= _selectedAction.MESSAGE;
			
			_popUpMailBox.idsCommande 		= [(dgListCommande.selectedItem as Commande).IDCOMMANDE];
			_popUpMailBox.numCommande 		= (dgListCommande.selectedItem as Commande).NUMERO_COMMANDE;
			_popUpMailBox.idtypecmd			= (dgListCommande.selectedItem as Commande).IDTYPE_COMMANDE;
			_popUpMailBox.isCommandePartielle = isLivPartielle;
			
			var moduleText	:String = giveMeTheSegementTheme(dgListCommande.selectedItem as Commande);
			var sujet		:String = giveMeSubject(dgListCommande.selectedItem as Commande);
			var libelle		:String	= (dgListCommande.selectedItem as Commande).LIBELLE_COMMANDE;
			var numero		:String	= (dgListCommande.selectedItem as Commande).NUMERO_COMMANDE;
			var objet		:String = sujet + " / " + libelle + " / " + numero;
			
			_popUpMailBox.initMail(moduleText, objet, "");
			_popUpMailBox.configRteMessage(_infosMail, GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_popUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE, popUpmailHandler);
			
			_popUpMailBox.selectedAction = _selectedAction;
			
			isLivraison = isLiv;
			
			PopUpManager.addPopUp(_popUpMailBox, this, true);
			PopUpManager.centerPopUp(_popUpMailBox);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private function popUpDetailsHandler(e:Event):void
		{
			if(_popUpDetails.isModified)
				getListeCommandes();
		}
		
		private function popUplivraisonHandler(e:Event):void
		{
			if(!_popUpLivPart.commandePartielleSend)
				PopUpManager.removePopUp(_popUpLivPart);
			else
			{
				_selectedAction = _popUpLivPart.myaction;
				PopUpManager.removePopUp(_popUpLivPart);
				popUpMail();
			}
		}
		
		private function transporteurIsSelectedCloseHandler(e:Event):void
		{
			popUpMail(true);
		}
		
		private function popUpmailHandler(e:Event):void
		{
			_popUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE, popUpmailHandler);
			
			if(!_popUpMailBox.cbMail.selected)
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Validation_enregistr__'), this);
			else
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_mail_a__t__envoy__'), this);
			
			PopUpManager.removePopUp(_popUpMailBox);

			if(isLivraison)
			{
				var cmdSelected:Commande = (dgListCommande.selectedItem as Commande).copyCommande();
				
				if(cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.MODIFICATION_OPTIONS &&
					cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.MODIFICATION_FIXE &&
					cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.RESILIATION &&
					cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.RESILIATION_FIXE &&
					cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.SUSPENSION &&
					cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.SUSPENSION_FIXE &&
					cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.REACTIVATION &&
					cmdSelected.IDTYPE_COMMANDE != TypesCommandesMobile.REACTIVATION_FIXE)
				{
					viewLivraison();
				}
			}
			
			getListeCommandes();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		//---ATTRIBUT LES DONNEES A LA COMBOBOX PASSEE EN PARAMETRE
		private function attributDataToCombobox(cbx:ComboBox, dataValues:ArrayCollection, msgError:String, isFirstItem:Boolean = true):void
		{
			var lenDataValues:int = dataValues.length;
			
			cbx.errorString = "";
			
			if(lenDataValues != 0)
			{
				if(isFirstItem)
				{
					cbx.dataProvider  = dataValues;
					cbx.selectedIndex = 0;
				}
				else
				{
					cbx.prompt 			= ResourceManager.getInstance().getString('M16', 'S_lectionnez');
					cbx.dataProvider 	= dataValues;
					
					if(lenDataValues > 1)
						cbx.selectedIndex = -1;
					else
						cbx.selectedIndex = 0;
				}
				
				if(lenDataValues > 10)
					cbx.rowCount = 10;
				else
					cbx.rowCount = lenDataValues;
				
				(cbx.dataProvider as ArrayCollection).refresh();
			}
			else
				cbx.prompt = msgError;
		}
		
		private function razArraycollection():void
		{
			try
			{
				listeCommandes.removeAll();
				(cbxEtat.dataProvider as ArrayCollection).removeAll();
				(cbxGest.dataProvider as ArrayCollection).removeAll();
				(cbxOpe.dataProvider as ArrayCollection).removeAll();
				(cbxCpte.dataProvider as ArrayCollection).removeAll();
				(cbxSsCpte.dataProvider as ArrayCollection).removeAll();
			}
			catch (error:Error) {}
		}
		
		private function exportNonDetaille():void
		{
			if((dgListCommande.dataProvider as ArrayCollection).length > 0) 
			{	
				var dataToExport	:String = DataGridDataExporter.getCSVString(dgListCommande, ";", "\n", true);
				var url				:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M16/csv/ExportCSV.cfm";
				
				DataGridDataExporter.exportCSVString(url, dataToExport, "export");
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es___exporter__'), 'Consoview', null);
		}

		private function exportDetaille():void
		{
			var cmds :ArrayCollection = dgListCommande.dataProvider as ArrayCollection;
			var len	 :int = cmds.length;
			
			_clobIdsCommande = '';
			
			for(var i:int = 0; i < len;i++)
			{
				if(cmds[i].SELECTED)
				{
					if(i == 0)
						_clobIdsCommande = cmds[i].IDCOMMANDE;
					else
						_clobIdsCommande = _clobIdsCommande + cmds[i].IDCOMMANDE;
					
					if(i != len-1)
						_clobIdsCommande = _clobIdsCommande + ',';
				}
			}
			
			if(_clobIdsCommande == '')
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_es___exporter__'), 'Consoview', null);
			else
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M16', 'Cela_peut_prendre_quelques_minutes__Voul'), 'Consoview', resultAlertExport);
		}
		
		private function exportData():void
		{
			var url		  :String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/M16/csv/ExportCSV2.cfm";
			var request	  :URLRequest = new URLRequest(url);         
			var variables :URLVariables = new URLVariables();
				variables.FILE_NAME 	= ResourceManager.getInstance().getString('M16', 'Commandes');
			
			if(chxSelectAll.selected)
				variables.METHODE 		= "fournirAllDataSetToExport";
			else
			{
				variables.ID_COMMANDES  = _clobIdsCommande;
				variables.METHODE 		= "fournirDataSetToExport";
			}
			
			request.data = variables;
			request.method = URLRequestMethod.POST;
			
			navigateToURL(request,"_blank");
		}
		
		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function giveMeSubject(objectCommande:Commande):String
		{
			var TC:String = "";
			
			switch(objectCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	TC = ResourceManager.getInstance().getString('M16', 'Commande_Mobile');				break;//--->  NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_Fixe_R_seau');		break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_');		break;//--->  NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_'); 		break;//--->  NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--->  MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--->  RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--->  RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--->  RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--->  RENVLT
				case TypesCommandesMobile.SUSPENSION: 			TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--->  SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--->  SUSP
				case TypesCommandesMobile.REACTIVATION: 		TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--->  REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--->  REACT
				case TypesCommandesMobile.SAV: 					TC = ResourceManager.getInstance().getString('M16', 'SAV_'); 						break;//--- SAV
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM: 	TC = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_');			break;//--- REACT
			}
			
			return TC;
		}
		
		private function giveMeTheSegementTheme(objectCommande:Commande):String
		{
			var moduleText:String = "";
			var idseg:int = 1;
			if(objectCommande.SEGMENT_MOBILE == 1 && objectCommande.SEGMENT_FIXE == 0 && objectCommande.SEGMENT_DATA == 0)
				idseg = 1;//---> MOBILE
			
			if(objectCommande.SEGMENT_MOBILE == 0 && objectCommande.SEGMENT_FIXE == 1 && objectCommande.SEGMENT_DATA == 0)
				idseg = 2;//---> FIXE
			
			if(objectCommande.SEGMENT_MOBILE == 0 && objectCommande.SEGMENT_FIXE == 0 && objectCommande.SEGMENT_DATA == 1)
				idseg = 3;//---> DATA
			
			
			switch(idseg)//--->  1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Mobile'); break;
				case 2: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Fixe'); 	break;
				case 3: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Data'); 	break;
			}
			
			return moduleText;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//

		private function filtreData():void
		{
			if(dgListCommande.dataProvider != null)
			{
				(dgListCommande.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListCommande.dataProvider as ArrayCollection).refresh();
			}
		}
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && 	( 
										(item.LIBELLE_COMMANDE != null && item.LIBELLE_COMMANDE.toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)
										||
										(item.REF_CLIENT1 != null && item.REF_CLIENT1.toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)
										||
										(item.NUMERO_COMMANDE != null && item.NUMERO_COMMANDE.toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)
										||
										(item.LIBELLE_LASTETAT != null && item.LIBELLE_LASTETAT.toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)
										||
										(item.MONTANT != null && item.MONTANT.toString().toLocaleLowerCase().search(txtFiltre.text.toLocaleLowerCase()) != -1)
									)
				
				&& searchMyCommande(item) && searchStatusCommande(item) && searchEtatCommande(item) && searchTypeCommande(item)
				&& searchGestionnaire(item) && searchOperateur(item) && searchCompte(item) && searchSousCompte(item);
			
			return rfilter;
		}
		
		private function searchPoolCommande(item:Object):Boolean//---> FILTRE SUR LE POOL DE LA COMMANDE
		{
			var OK:Boolean = false;
			
			if(cbxPools.selectedItem != null)
			{
				var pool:int = cbxPools.selectedItem.IDPOOL;
				
				if(pool != -1)
				{
					if(item.IDPOOL_GESTIONNAIRE == pool)
						OK = true;
				}
				else
					OK = true;
			}
			else
				OK = false;
			
			return OK;
		}		
		
		private function searchMyCommande(item:Object):Boolean//---> FILTRE SUR 'MES COMMANDES' OU 'TOUTES LES COMMANDES'
		{
			var OK	 :Boolean = false;
			var myId :int = CvAccessManager.getUserObject().CLIENTACCESSID;
			
			if(rdbtnMesCommandes.selected)
			{
				if(item.IS_CONCERN == 1 && item.USERID == myId)
					OK = true;
			}
			else
				OK = true;
			
			return OK;
		}
		
		private function searchStatusCommande(item:Object):Boolean//---> FILTRE SUR 'TOUTES' OU 'EN COURS' OU 'CLOSE'
		{
			var OK:Boolean = false;		
			
			if(rdTous.selected)
				OK = true;
			else
			{
				if(rdCloses.selected)
				{
					if(item.ENCOURS == 0)
						OK = true;
				}
				
				if(rdCours.selected)
				{
					if(item.ENCOURS == 1)
						OK = true;
				}
			}
			
			return OK;
		}	
		
		private function searchEtatCommande(item:Object):Boolean//---> FILTRE SUR L'ETAT DE LA COMMANDE
		{
			var OK:Boolean = false;
			
			if(cbxEtat.selectedItem != null)
			{
				var myId:int = cbxEtat.selectedItem.IDETAT;
				
				if(myId != -1)
				{
					if(item.IDLAST_ETAT == myId)
						OK = true;
				}
				else
					OK = true;
			}
			else
				OK = true;
			
			return OK;
		}
		
		private function searchGestionnaire(item:Object):Boolean//---> FILTRE SUR LE GESTIONNAIRE
		{
			var OK:Boolean = false;
			
			if(cbxGest.selectedItem != null)
			{
				var myId:int = cbxGest.selectedItem.USERID;
				
				if(myId != -1)
				{
					if(item.USERID == myId)
						OK = true;
				}
				else
					OK = true;
			}
			else
				OK = true;
			
			return OK;
		}
		
		private function searchOperateur(item:Object):Boolean//---> FILTRE SUR L'OPERATEUR
		{
			var OK:Boolean = false;
			
			if(cbxOpe.selectedItem != null)
			{
				var myId:int = cbxOpe.selectedItem.IDOPERATEUR;
				
				if(myId != -1)
				{
					if(item.IDOPERATEUR == myId)
						OK = true;
				}
				else
					OK = true;
			}
			else
				OK = true;
			
			return OK;
		}
		
		private function searchCompte(item:Object):Boolean//---> FILTRE SUR LE COMPTE
		{
			var OK:Boolean = false;
			
			if(cbxCpte.selectedItem != null)
			{
				var myId:int = cbxCpte.selectedItem.IDCOMPTE_FACTURATION;
				
				if(myId != -1)
				{
					if(item.IDCOMPTE_FACTURATION == myId)
						OK = true;
				}
				else
					OK = true;
			}
			else
				OK = true;
			
			return OK;
		}
		
		private function searchSousCompte(item:Object):Boolean//---> FILTRE SUR LE SOUSCOMPTE
		{
			var OK:Boolean = false;
			
			if(cbxSsCpte.selectedItem != null)
			{
				var myId:int = cbxSsCpte.selectedItem.IDSOUS_COMPTE;
				
				if(myId != -1)
				{
					if(item.IDSOUS_COMPTE == myId)
						OK = true;
				}
				else
					OK = true;
			}
			else
				OK = true;
			
			return OK;
		}
		
		private function searchTypeCommande(item:Object):Boolean//---> FILTRE SUR LE TYPE DE COMMANDE
		{
			var OK:Boolean = false;
			
			if(rbtTous.selected)
				OK = true; 
			
			if(rbtMobile.selected)
			{
				if(item.SEGMENT_MOBILE == 1 && item.SEGMENT_FIXE == 0 && item.SEGMENT_DATA == 0)
					OK = true; 
			}
			
			if(rbtFixe.selected)
			{
				if(item.SEGMENT_MOBILE == 0 && item.SEGMENT_FIXE == 1 && item.SEGMENT_DATA == 0)
					OK = true; 
			}
			
			if(rbtData.selected)
			{
				if(item.SEGMENT_MOBILE == 0 && item.SEGMENT_FIXE == 0 && item.SEGMENT_DATA == 1)
					OK = true; 
			}
			
			return OK;
		}

	}
}
