package gestionparc.ihm.action.dissocier.actionDissocierExtFromLigne
{
	import gestionparc.ihm.question.ligne.QuestionDissocierExtFromLigne;

	public class ActionDissocierExtFromLigneReceiver
	{
		private var qn:QuestionDissocierExtFromLigne;
		
		public function ActionDissocierExtFromLigneReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			qn = new QuestionDissocierExtFromLigne();
		}
	}
}