package univers.parametres.perimetres.gestiontechnique
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.lignesetservices.applicatif.GestionAffectationMobile;
	import univers.inventaire.lignesetservices.views.OngletAffectationMobileImpl;
	import univers.inventaire.lignesetservices.vo.Ligne;
	import univers.inventaire.lignesetservices.vo.OngletAffectationVo;
	
	[Bindable]
	public class MajCollaborateurImpl extends OngletAffectationMobileImpl
	{	
		public static const SAVE_CLICK : String = "EnregistrerClicked";
		
	 
		private var _gm : GestionAffectationMobile;
		public function get gm():GestionAffectationMobile{
			return _gm;
		}
		public function set gm(g :GestionAffectationMobile):void{
			_gm = g;
		}
		
		public function MajCollaborateurImpl(){
			
			super();
		}
		
		private var _ligne : Ligne;
		public function get ligne():Ligne{
			return _ligne;
		}
		public function set ligne(st : Ligne):void{
			_ligne = st;
		}
		
		override protected function commitProperties():void{
			gm  = new GestionAffectationMobile(ligne);			
			gm.getInfosLigneCollaborateur();
			
			frmCompte.addEventListener(FlexEvent.SHOW,frmCompteShowHandler);
			imgSelectOperateur.addEventListener(MouseEvent.CLICK,imgSelectOperateurClickHandler);
			imgSelectOperateur.visible = (modeEcriture);
			imgCancel.visible =(modeEcriture);
			imgCancel.addEventListener(MouseEvent.CLICK,imgCancelClickHandler);
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
			NOM_COLLABORATEUR_SI_OPERATEUR.addEventListener(Event.CHANGE,NOM_COLLABORATEUR_SI_OPERATEURChangeHandler);
			PRENOM_COLLABORATEUR_SI_OPERATEUR.addEventListener(Event.CHANGE,PRENOM_COLLABORATEUR_SI_OPERATEURChangeHandler);
			
			NOM_COLLABORATEUR_SI_OPERATEUR.addEventListener(FlexEvent.VALUE_COMMIT,NOM_COLLABORATEUR_SI_OPERATEURValueCommitHandler);
			PRENOM_COLLABORATEUR_SI_OPERATEUR.addEventListener(FlexEvent.VALUE_COMMIT,PRENOM_COLLABORATEUR_SI_OPERATEURValueCommitHandler);
//			NOM_COLLABORATEUR_SI_OPERATEUR.editable = (modeEcriture);
//			PRENOM_COLLABORATEUR_SI_OPERATEUR.editable = (modeEcriture);
			btUpdate.visible = (modeEcriture);
		}
		
		override protected function frmCompteShowHandler(fe : FlexEvent):void{
			
		}
		
		override protected function NOM_COLLABORATEUR_SI_OPERATEURChangeHandler(ev : Event):void{
			tmpcollaborateur.COMPTE = COMPTE.text;
			tmpcollaborateur.COLLABORATEURID = gm.ligne.COLLABORATEURID;
			tmpcollaborateur.NOM = NOM_COLLABORATEUR_SI_OPERATEUR.text;
			
		}
		
		override protected function PRENOM_COLLABORATEUR_SI_OPERATEURChangeHandler(ev : Event):void{
			tmpcollaborateur.COMPTE = COMPTE.text;
			tmpcollaborateur.COLLABORATEURID = gm.ligne.COLLABORATEURID;
			tmpcollaborateur.PRENOM = PRENOM_COLLABORATEUR_SI_OPERATEUR.text;
		}
		
		override protected function  NOM_COLLABORATEUR_SI_OPERATEURValueCommitHandler(fe : FlexEvent):void{
			if (NOM_COLLABORATEUR_SI_OPERATEUR.initialized){
				if (oldNOM == "" && gm.ligne.NOM != null){
					oldNOM = ObjectUtil.copy(gm.ligne.NOM).toString();
				}
				tmpcollaborateur.COMPTE = COMPTE.text;
				tmpcollaborateur.COLLABORATEURID = gm.ligne.COLLABORATEURID;
				tmpcollaborateur.NOM = NOM_COLLABORATEUR_SI_OPERATEUR.text;	
			}
		}		
		
		override protected function  PRENOM_COLLABORATEUR_SI_OPERATEURValueCommitHandler(fe : FlexEvent):void{
			if (PRENOM_COLLABORATEUR_SI_OPERATEUR.initialized){
				if (oldPRENOM == "" && gm.ligne.PRENOM){
					oldPRENOM = ObjectUtil.copy(gm.ligne.PRENOM).toString();
				}
				tmpcollaborateur.COMPTE = COMPTE.text;
				tmpcollaborateur.COLLABORATEURID = gm.ligne.COLLABORATEURID;
				tmpcollaborateur.PRENOM = PRENOM_COLLABORATEUR_SI_OPERATEUR.text;	
			}
		}
		
		override protected function enregistrer():void{
			tmpcollaborateur.COMPTE = COMPTE.text;
			tmpcollaborateur.NOM = NOM_COLLABORATEUR_SI_OPERATEUR.text;
			tmpcollaborateur.PRENOM = PRENOM_COLLABORATEUR_SI_OPERATEUR.text;
			tmpcollaborateur.COLLABORATEURID = gm.ligne.COLLABORATEURID;
			
			var boolOk : Boolean = true;
			if(NOM_COLLABORATEUR_SI_OPERATEUR.text.length == 0){
				boolOk = false
				NOM_COLLABORATEUR_SI_OPERATEUR.setStyle("borderColor","red");				
			}
			
			if(String(OPERATEUR.text).search("@") != -1){
				gm.oldValue = new Ligne();
				gm.oldValue.NOM = oldNOM;
				gm.oldValue.PRENOM = oldPRENOM;
				gm.envoyerMail = true;
				gm.serviceClientOperateur = new Object();
				gm.serviceClientOperateur.EMAIL = _contact.email;
				gm.serviceClientOperateur.NOM = _contact.nom;
				gm.serviceClientOperateur.PRENOM = _contact.prenom;				
				gm.serviceClientOperateur.OPERATEUR = (_contact.societeNom != "")?_contact.societeNom:_distrib.raisonSociale;
			}else{
				gm.envoyerMail = false;
			}
			
			if (boolOk){ 
				mapData(donnees = new OngletAffectationVo());
				gm.updateInfosAffectation(donnees);
				NOM_COLLABORATEUR_SI_OPERATEUR.clearStyle("borderColor");
				dispatchEvent(new Event(SAVE_CLICK));
			}else{
				Alert.show(ResourceManager.getInstance().getString('M22', 'Les_champs_en_rouge_sont_obligatoires'),ResourceManager.getInstance().getString('M22', 'Erreur'));
			}
		}
		
	
		
		override protected function constactSelectedHandler(ev : Event):void{
			if (_contactWindow.contact.email != null && _contactWindow.contact.email.length > 0) emailSelected  = true;
			else  emailSelected  = false;
			
			OPERATEUR.text = _contactWindow.contact.email;		
			_contact = _contactWindow.contact;
			_distrib = _contactWindow.distributeur;
			gm.getListeSousComptes();
		}
		///----
		
	}
}