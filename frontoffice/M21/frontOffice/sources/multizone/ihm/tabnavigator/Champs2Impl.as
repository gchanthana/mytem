package multizone.ihm.tabnavigator
{
	import flash.events.Event;
	
	import multizone.entity.ContenuZone;
	import multizone.entity.Zone;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	
	[Bindable]
	public class Champs2Impl extends Box
	{
		
//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					GLOBALE
		//--------------------------------------------------------------------------------------------//
		
		public var LIBELLE		:String  = "";
		public var EXEMPLE		:String  = "";
		
		public var ACTIVER		:Boolean = false;
		
		//--------------------------------------------------------------------------------------------//
		//					IHM 
		//--------------------------------------------------------------------------------------------//
		
		public var btnAdd			:Button;
		
		public var dgListeZones		:DataGrid;
		
		public var txtbxLibelle		:TextInput;
		public var txtbxExemple		:TextInput;
		
		public var chbxActive		:CheckBox;
		public var chbxAuto			:CheckBox;
		
		//--------------------------------------------------------------------------------------------//
		//					GLOBALE IHM 
		//--------------------------------------------------------------------------------------------//
		
		public var listeZone		:ArrayCollection = new ArrayCollection();
		
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTOR
//--------------------------------------------------------------------------------------------//

		public function Champs2Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, 	creationCompleteHandler);
			addEventListener("ERASE_THIS_LINE", 			eraseLineHandler);
			addEventListener("RSLT_ZONESAISIE",				zoneSaisieHandler);
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM 
		//--------------------------------------------------------------------------------------------//
		
		protected function txtbxChangeHandler(isLibelle:Boolean):void
		{
			if(isLibelle)
			{
				txtbxLibelle.errorString = "";
			}
			else
			{
				txtbxExemple.errorString = "";
			}
		}
		
		protected function btnAddClickHandler(e:Event):void
		{
			oneMore();
		}
		
		protected function btnRAZClickHandler(e:Event):void
		{
			listeZone = new ArrayCollection();
			oneMore();
		}
		
		protected function btnRefreshClickHandler(e:Event):void
		{
			dispatchEvent(new Event("REFRESH_DATAGRID2", true));
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE
//--------------------------------------------------------------------------------------------//
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IHM 
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fl:FlexEvent):void
		{
			//ENVOI DE LA PROCEDURE PERMETTANT DE REMPLIR LES CHAMPS
			oneMore();
		}
		
		private function eraseLineHandler(e:Event):void
		{
			if(dgListeZones.selectedItem != null && (dgListeZones.dataProvider as ArrayCollection).length > 1)
			{
				removeItemSelected(dgListeZones.selectedItem);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENER PROC 
		//--------------------------------------------------------------------------------------------//

		private function zoneSaisieHandler(e:Event):void
		{
			if(listeZone.length > 0)
			{
				//FAUDRA FAIRE LE TRAITEMENT XML ICI
			}
			else
			{
				oneMore();
			}
		}	
			
		//--------------------------------------------------------------------------------------------//
		//					METHODES 
		//--------------------------------------------------------------------------------------------//		
		
		private function oneMore():void
		{
			var thisFirstZone:Zone = new Zone();
				thisFirstZone.COMPTEUR_ZONE	= listeZone.length + 1;
				thisFirstZone.CONTENU_ZONE	= new ContenuZone();
				thisFirstZone.IDTYPE_ZONE	= 1;
				thisFirstZone.STEPPEUR_ZONE = 20;
				thisFirstZone.TYPE_ZONE		= " ";
				thisFirstZone.SAISIE_ZONE	= true;
			listeZone.addItem(thisFirstZone);
		}
		
		private function removeItemSelected(lineSelected:Object):void
		{
			for(var i:int = 0; i < listeZone.length;i++)
			{
				if(listeZone[i].ID_UNIQUE_ZONE == lineSelected.ID_UNIQUE_ZONE)
				{
					listeZone.removeItemAt(i);
					i = i-1;
				}
				else
				{
					listeZone[i].COMPTEUR_ZONE =  i + 1;
				}
			}
		}

	}
}