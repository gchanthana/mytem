package univers.inventaire.lignesetservices.applicatif
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.lignesetservices.vo.LibellePerso;
	
	 
	
	
	[Event(name="libelleChanged")]
	
	
	[Bindable]
	public class LibellesPersos extends EventDispatcher
	{
		public static const NB_LIBELLE : Number = 10;
		public var libelle1 : String = "Libellé 1";		
		public var libelle2 : String = "Libellé 2";
		public var libelle3 : String = "Libellé 3";
		public var libelle4 : String = "Libellé 4";
		public var libelle5 : String = "Libellé 5";
		public var libelle6 : String = "Libellé 6";
		public var libelle7 : String = "Libellé 7";
		public var libelle8 : String = "Libellé 8";
		public var libelle9 : String = "Libellé 9";
		public var libelle10 : String = "Libellé 10";
		public var libelle11 : String = "Libellé 11";
		public var libelle12 : String = "Libellé 12";
		public var libelle13 : String = "Libellé 13";
		public var libelle14 : String = "Libellé 14";
		public var libelle15 : String = "Libellé 15";
		public var libelle16 : String = "Libellé 16";
		public var libelle17 : String = "Libellé 17";
		public var libelle18 : String = "Libellé 18";
		public var libelle19 : String = "Libellé 19";
		public var libelle20 : String = "Libellé 20";
		public var libelle21 : String = "Libellé 21";
		public var libelle22 : String = "Libellé 22";
		public var libelle23 : String = "Libellé 23";
		public var libelle24 : String = "Libellé 24";
		public var libelle25 : String = "Libellé 25";
		public var libelle26 : String = "Libellé 26";		
		
		public function LibellesPersos(){
			
		}
		
		public function updateAllLibelles(c1:String,
										  c2:String,
										  c3:String,
										  c4:String,
										  c5:String,
										  c6:String,
										  c7:String,
										  c8:String,
										  c9:String,
										  c10:String):void{
										  	
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateCustomFieldsLabel",
																				updateAllLibellesResultHandler);
				RemoteObjectUtil.callService(op,c1,
												c2,
												c3,
												c4,
												c5,
												c6,
												c7,
												c8,
												c9,
												c10)
				
			 
		}
		
		private function updateAllLibellesResultHandler(re : ResultEvent):void{			
			if(re.result > 0){
				ConsoviewAlert.afficherOKImage("Champs mis à jour");				
				for (var i : Number = 1; i< NB_LIBELLE+1; i++){					
					if (re.token.message.body[i-1] != "Libellé "+i){
						this["libelle"+i] = String(re.token.message.body[i-1]);
					}
				}
				
				
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur");
			}
		}		
		
		
		public function updateLibelle(libelleVo : LibellePerso):void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"updateCustomFieldLabel",
																				updateLibelleResultHandler);
			RemoteObjectUtil.callService(op,Number(libelleVo.idLibelle),libelleVo.libelle)
		}
		private function updateLibelleResultHandler(re : ResultEvent):void{			
			if(re.result){
				this["libelle"+re.token.message.body[0]] = String(re.token.message.body[1]);
				dispatchEvent(new Event("libelleChanged"));
			}else{
				Alert.show("Une erreur s'est produite lors de la mise à jour","Erreur");
			}
		}		
		
		
		public function getLibellesPersos():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getCustomFieldsLabel",
																				getLibellesPersosResultHandler);
			RemoteObjectUtil.invokeService(op)
		}
		private function getLibellesPersosResultHandler(re : ResultEvent):void{			
			if(re.result && (re.result as ArrayCollection).length > 0){
				var tabResult : ArrayCollection = re.result as ArrayCollection;
				for (var i : Number = 1; i< NB_LIBELLE+1; i++){					
					if (tabResult[0]["C"+i] != null){
						this["libelle"+i] = tabResult[0]["C"+i]
					} 
				}
			}
		}
	}
}