package utils.composants.pagination.event
{
	
	import flash.events.Event;
	
	public class PaginateEvent extends Event
	{
		public static const PAGE_CHANGE:String = "pageChange";
		public static const TOTAL_PAGES_CHANGE:String = "totalPagesChange";
		
		public var index:int;
		public var itemsTotal:int;
		public var itemsPerPage:int;
		
		public function PaginateEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false){
			super(type, bubbles, cancelable);
		}
		
	}
}