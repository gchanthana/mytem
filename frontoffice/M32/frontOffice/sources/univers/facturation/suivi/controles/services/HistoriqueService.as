package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	[Bindable]
	public class HistoriqueService
	{
			
		public var model:HistoriqueModel;
		public var handler:HistoriqueHandler;
		
		public function HistoriqueService()
		{
			model = new HistoriqueModel();
			handler = new HistoriqueHandler(model);
		}
		
		public function getHistorique(idProduit:Number):void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetHistorique",
				"getHistorique",
				handler.getHistoriqueHandler);
			RemoteObjectUtil.callService(opData,idProduit);
			
		}
		
	}
}