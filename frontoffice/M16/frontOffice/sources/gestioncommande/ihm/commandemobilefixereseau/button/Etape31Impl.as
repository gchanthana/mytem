package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import composants.colorDatagrid.ColorDatagrid;
	
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpFicheProduitAccessoireIHM;
	import gestioncommande.services.EquipementService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Etape31Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListAccessoire			:ColorDatagrid;
		
		public var txtptSearch				:TextInput;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipements			:EquipementService 	= new EquipementService();		
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					AURES
		//--------------------------------------------------------------------------------------------//
		
		public var accessoire				:ArrayCollection = new ArrayCollection();
		
		public var phoneLibelle				:String = "";
		public var nivo						:String = "";
		public var engementLibelle			:String = "";
				
		public var selectedNumber			:Number = 0;
		public var totalNumber				:Number = 0;

		private var _cptrProc				:int = 0;
		private var _idTer					:int = -1;
		private var _idTerminal				:int = -1;

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES BUTTON CUSSTOM
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 2;
		
		public var ACCESS						:Boolean = false;
		
		private var _razl						:Boolean = false;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
					
		public function Etape31Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					LISTENER DE L'IHM
	//--------------------------------------------------------------------------------------------//

		public function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener("DETAILS",	descriptionHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}

		public function reset():void
		{
			accessoire.removeAll();
			
			phoneLibelle = "";
			nivo 		 = "";
			
			_idTer = _idTerminal = 0;
		}
		
		public function setConfiguration():void
		{
			_razl = true;
			
			var id:int = getIdTerminalSelected();
			
			_idTer = _idTerminal = id;
			
			if(_idTerminal == 0)
				getAccessoires();
			else
				getAccessoiresCompatibles();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getAccessoires():void
		{
			_equipements.fournirListeAccessoires(SessionUserObject.singletonSession.COMMANDE.IDREVENDEUR, SessionUserObject.singletonSession.COMMANDE.IDPROFIL_EQUIPEMENT);
			_equipements.addEventListener(CommandeEvent.LISTED_ACCESSOIRES, listeAccessoiresHandler);
		}
		
		private function getAccessoiresCompatibles():void
		{
			_equipements.fournirListeAccessoiresCompatibles(_idTerminal, SessionUserObject.singletonSession.COMMANDE.IDPROFIL_EQUIPEMENT);
			_equipements.addEventListener(CommandeEvent.LISTED_ACCESSOIRES, listeAccessoiresHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//	
		
		private function listeAccessoiresHandler(cmde:CommandeEvent):void
		{
			accessoire.removeAll();
			
			accessoire = _equipements.listeAccessoires;
			
			if(_razl == false && accessoire.length == 0)
			{
				if(SessionUserObject.singletonSession.LASTVSINDEX <= IDPAGE)
				{
					dispatchEvent(new Event('NO_ACCESSORIES_NEXT', true));
				}
				else if(SessionUserObject.singletonSession.LASTVSINDEX > IDPAGE)
				{
					dispatchEvent(new Event('NO_ACCESSORIES_PREV', true));
				}
			}
			else
			{
				if(_razl)
					getAccessoiresSelected(accessoire);
			}
		}
		
		public function viewStackChange():void
		{
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			findTerminal();
			
			if(_idTer != _idTerminal)
			{
				if(_idTerminal == 0)
					getAccessoires();
				else
					getAccessoiresCompatibles();
				
				_idTer = _idTerminal;
			}
			else
			{
				if(accessoire.length == 0)
				{
					if(SessionUserObject.singletonSession.LASTVSINDEX <= IDPAGE)
					{
						dispatchEvent(new Event('NO_ACCESSORIES_NEXT', true));
					}
					else if(SessionUserObject.singletonSession.LASTVSINDEX > IDPAGE)
					{
						dispatchEvent(new Event('NO_ACCESSORIES_PREV', true));
					}
				}
			}
		}

	//--------------------------------------------------------------------------------------------//
	//					VERIFICATION ET CONTRUCTION DES DONNÉES CHOISIES
	//--------------------------------------------------------------------------------------------//

		public function checkData():Boolean
		{
			buildParameters();
			return true;
		}
		
		public function buildParameters():void
		{
			var len:int = accessoire.length;
			
			_myElements.ACCESSOIRES = new ArrayCollection();
			
			for(var i:int = 0; i < len;i++)
			{
				if(accessoire[i].SELECTED)
					_myElements.ACCESSOIRES.addItem(accessoire[i]);
			}
		}

//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A L'ETAPE 
//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function txtptSearchHandler():void 
		{
			if(dgListAccessoire.dataProvider != null)
	    	{
	    		(dgListAccessoire.dataProvider as ArrayCollection).filterFunction = filterFunction;
	    		(dgListAccessoire.dataProvider as ArrayCollection).refresh();
	    	}
		}
		
		protected function dgListAccessoireItemClickHandler(e:Event):void 
		{
			if(dgListAccessoire.selectedItem != null)
			{				
				if(dgListAccessoire.selectedItem.SELECTED)
				{
					dgListAccessoire.selectedItem.SELECTED = false;
					selectedNumber--;
				}
				else if(dgListAccessoire.selectedItem.SUSPENDU_CLT != 1)
				{
					dgListAccessoire.selectedItem.SELECTED = true;
					selectedNumber++;
				}
				
				(dgListAccessoire.dataProvider as ArrayCollection).itemUpdated(dgListAccessoire.selectedItem);
				
				calculTotalSelected();
			}
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//	

		//--------------------------------------------------------------------------------------------//
		//					LISTENER
		//--------------------------------------------------------------------------------------------//
		
		private function descriptionHandler(e:Event):void
		{
			if(dgListAccessoire.selectedItem != null)
			{
				var popUpDetails:PopUpFicheProduitAccessoireIHM = new PopUpFicheProduitAccessoireIHM();
					popUpDetails.equipement 					= dgListAccessoire.selectedItem;
				
				PopUpManager.addPopUp(popUpDetails, this.parent.parent.parent, true);
				PopUpManager.centerPopUp(popUpDetails);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		public function selectColor(datagrid:DataGrid, rowIndex:int, color:uint):uint			
		{				
			var rColor:uint;				
			rColor = color;				
			var item:Object =  datagrid.dataProvider.getItemAt(rowIndex);
			
			if (item != null )				
			{				
				if(item.SUSPENDU_CLT == 1)//En repture
				{	
					rColor = 0xF7FF04;
				}			
				else					
					rColor = color;					
			}				
			return rColor;				
		}
		
		private function findTerminal():void
		{
			var len	:int = _myElements.TERMINAUX.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(_myElements.TERMINAUX[i].IDEQUIPEMENT != 71)
				{
					phoneLibelle = _myElements.TERMINAUX[i].LIBELLE;
					_idTerminal  = _myElements.TERMINAUX[i].IDFOURNISSEUR;
					nivo 		 = _myElements.TERMINAUX[i].NIVEAU;
					return;
				}
			}
		}
		
		private function getIdTerminalSelected():int
		{
			var lenTerSel	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var idTerminal	:int = 0;
			
			for(var i:int = 0;i < lenTerSel;i++)
			{
				if(SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDEQUIPEMENT != 71)
					idTerminal  = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].IDFOURNISSEUR;
			}	
			
			return idTerminal;
		}

		private function calculTotalSelected():void
		{
			var len:int = accessoire.length;
			
			totalNumber = 0;
			
			for(var i:int = 0; i < len;i++)
			{
				if(accessoire[i].SELECTED)
					totalNumber =  Number(totalNumber) + Number(accessoire[i].PRIX);
			}
		}
		
		private function getAccessoiresSelected(values:ArrayCollection):void
		{
			var access	:ArrayCollection = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES;
			var lenSel	:int = access.length;
			var lenAcc	:int = values.length;
			
			for(var i:int = 0;i < lenAcc;i++)
			{
				var idAcc:int = values[i].IDFOURNISSEUR;
				
				for(var j:int = 0;j < lenSel;j++)
				{
					if(access[j].IDFOURNISSEUR == idAcc)
						values[i].SELECTED = true;
				}	
			}
			
			calculTotalSelected();
			
			_razl = false;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//
				
		private function filterFunction(item:Object):Boolean
	    {
		    var rfilter:Boolean = true;		    
		    	rfilter 		= rfilter && (item.LIBELLE != null && item.LIBELLE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1)
		   
		    return rfilter;
		}

	}
}