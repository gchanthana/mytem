package gestionparc.ihm.action.associer.ActionAssocierEqpToExt
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.extension.FicheAssocierEqpToExt;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierEqpToExtReceiver
	{
		private var fiche:FicheAssocierEqpToExt;

		public function ActionAssocierEqpToExtReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierEqpToExt();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
