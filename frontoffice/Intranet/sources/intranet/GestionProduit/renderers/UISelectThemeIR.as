package intranet.GestionProduit.renderers
{
	import mx.containers.Canvas;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.events.ResizeEvent;
	
	import intranet.GestionProduit.DataSample;
	
	public class UISelectThemeIR extends Canvas
	{
		
		private var _boolCreationComplete:Boolean = false;
		
		protected var _cbAbo:ComboBox;
		protected var _cbConso:ComboBox;
		protected var _lbAffectation:Label;
		protected var _boolAffecte:Boolean = false; 
		protected var _boolcombo:Boolean = false;
		
		
		
		
		
		protected var _selectedIndex:Number = -1;
		protected var _cpVisible:Number = -1; 
		protected var _text:String = "";
		protected var _combo_affected:Number = -1;//abo/conso/lbl
		
		
		
		
		protected const COMBO_ABO:Number = 1;
		protected const COMBO_CONSO:Number = 2;
		protected const LIBELLE:Number = 3;
		private var _dataChanged:Boolean = false;
		private var _measure:Boolean = false;
		
		
		
		
		public function UISelectThemeIR()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
		}
		
		
		
		
		
		
		protected function resetComponent():void
		{	
			
			if (_boolCreationComplete)
			{	
				_selectedIndex = -1;
				_text = "";
				_cpVisible = LIBELLE;
				_combo_affected =-1
			}
			
		}
		
		
		
		
		
		
		
		
		override public function set data(value:Object):void
		{				
			
			resetComponent();
			
			if(value != null)
			{
				_dataChanged =true;
				
				super.data = value;
				
				if(data.IDTHEME_PRODUIT > 0)
				{
					_boolAffecte = true;
				}
				else
				{
					_boolAffecte = false;
				}
				
				if(data.hasOwnProperty("EDITED"))
				{
					if(data.EDITED)
					{
						_boolcombo = true;
					}
					else
					{
						_boolcombo = false;
					}					
				}
				
				invalidateDisplayList()
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if(!_boolCreationComplete) return;
		}
		
		
		
		override protected function measure():void
		{
			_measure = true;
		}
		 
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			
			
			if(_measure)
			{
				var __delta:Number = 0;
				var __widthCbo:Number = unscaledWidth * 48 / 100; 
				__delta = (unscaledWidth - (__widthCbo * 2 + 10))/2;
				
				
				_cbAbo.x = __delta;
				_cbConso.x = __widthCbo + 10;
				
				_cbAbo.width = __widthCbo;
				_cbConso.width = __widthCbo;
				_lbAffectation.width = unscaledWidth -10;
				_measure = false;
			}
				
				
				
				
				
				
				
				
				 
		
			
			
			if(_dataChanged == false)
			{
				return
			}
			if(_boolAffecte)
			{
				if(!_boolcombo)
				{
					_lbAffectation.text = data.THEME;
					_lbAffectation.visible = true;
					_cbAbo.visible = _cbConso.visible = false;
				}
				else if(data.hasOwnProperty("CB"))
				{	
					_lbAffectation.visible = false;
					_cbAbo.visible = _cbConso.visible =true;
					
					if(data.CB == "abo")
					{
						_cbAbo.selectedIndex=data.INDEX;
					}
					else if(data.CB == "conso")
					{				
						_cbConso.selectedIndex=data.INDEX;
					}
				}
			}
			else
			{
				_cbAbo.visible = _cbConso.visible =true;
				_lbAffectation.visible = false;
				
				if(_boolcombo && data.hasOwnProperty("CB"))
				{	
					if(data.CB == "abo")
					{
						_cbAbo.selectedIndex=data.INDEX;
						_cbConso.selectedIndex = -1
						
					}
					else if(data.CB == "conso")
					{				
						_cbConso.selectedIndex=data.INDEX;
						_cbAbo.selectedIndex = -1
					}
				}
				else
				{
					_cbAbo.selectedIndex =  _cbConso.selectedIndex = -1
				}
			}
			_dataChanged = false;
			
		}
		
		
		
		
		
		
		
		
		
		
		protected function creationCompleteHandler(event:FlexEvent):void
		{
			// TODO Auto-generated method stub
			_cbAbo.prompt="Abonnement";
			_cbAbo.dataProvider = DataSample.TabAbo;
			_cbAbo.addEventListener(ListEvent.CHANGE,cbAboChangeHandler);
			
			_cbConso.prompt="Consomation";
			_cbConso.dataProvider = DataSample.TabConso;
			_cbConso.addEventListener(ListEvent.CHANGE,cbConsoChangeHandler);
			_boolCreationComplete = true;
			
			horizontalScrollPolicy = "off";
		}
		
			
		
		
		
		
		public function cbAboChangeHandler(e:ListEvent):void
		{	
			if(_cbAbo.selectedIndex == -1) return;
			
			_cbConso.selectedIndex = -1;
			_cbConso.toolTip = "";
			
			
			data.IDTHEME_PRODUIT = _cbAbo.selectedItem.IDTHEME_PRODUIT;
			data.EDITED = true;
			data.INDEX = _cbAbo.selectedIndex;
			data.CB = "abo";
			data.THEME = _cbAbo.selectedItem.THEME;
			_dataChanged = true;
			_cbAbo.toolTip = _cbAbo.selectedItem.THEME;
			invalidateDisplayList();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		public function cbConsoChangeHandler(e:ListEvent):void
		{
			if(_cbConso.selectedIndex == -1) return;
			
			_cbAbo.selectedIndex = -1;
			_cbAbo.toolTip = "";
			
			data.IDTHEME_PRODUIT = _cbConso.selectedItem.IDTHEME_PRODUIT;
			data.EDITED = true;
			data.INDEX = _cbConso.selectedIndex;
			data.CB = "conso";
			data.THEME = _cbConso.selectedItem.THEME;
			data.TOUT_ACCES=0;
			data.BOOL_ACCES=0;
			_dataChanged = true;
			
			_cbConso.toolTip = _cbConso.selectedItem.THEME;
			
			invalidateDisplayList();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			if(_cbAbo == null) _cbAbo = new ComboBox();			
			 
			_cbAbo.labelField="THEME";
			_cbAbo.x = 5;
			_cbAbo.y = 0;
			_cbAbo.rowCount=4;
			_cbAbo.prompt="";
			_cbAbo.width = 190; 
			_cbAbo.height = 20;
			_cbAbo.minWidth = 0;
			_cbAbo.maxWidth = 200;
			_cbAbo.dropdownWidth=450;
			_cbAbo.rowCount=20;
			_cbAbo.visible = false;
			addChild(_cbAbo);
			
			
			if(_cbConso == null) _cbConso = new ComboBox();
			_cbConso.dropdownWidth=450;			 
			_cbConso.labelField="THEME";
			_cbConso.x = 200;
			_cbConso.y = 0;
			_cbConso.rowCount=4;
			_cbConso.prompt="";
			_cbConso.width = 190; 
			_cbConso.height = 20;
			_cbConso.minWidth = 0;
			_cbConso.maxWidth = 200;
			_cbConso.rowCount=20;
			_cbConso.visible = false;
			addChild(_cbConso);
			
			
			
			if(_lbAffectation == null) _lbAffectation = new Label();
			_lbAffectation.x = 5;
			_lbAffectation.y = 0;
			_lbAffectation.percentWidth = 100;
			_lbAffectation.visible = false;
			addChild(_lbAffectation);
		}
	}
}