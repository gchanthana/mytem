package general.appliComposants.ListePerimetreWindow.SimplePerimetreTree
{
	import flash.events.Event;
	
	import general.event.PerimetreEvent;
	import general.event.PerimetreTreeEvent;
	
	import mx.collections.IViewCursor;
	import mx.collections.XMLListCollection;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import mx.events.TreeEvent;

	public class PerimetreTree extends GenericPerimetreTree {
		private var currentOpenNode:Object;
		
		protected var rootId : Number;
		
		protected var currentIndex:Number=0;
		
		public function PerimetreTree() {
			this.itemRenderer = new ClassFactory(general.appliComposants.ListePerimetreWindow.SimplePerimetreTree.PerimetreTreeItemRenderer);
			this.addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		override protected function afterCreationComplete(event: Event):void {
			currentOpenNode = null;
			addEventListener(TreeEvent.ITEM_OPENING,onItemOpening);
			//addEventListener(TreeEvent.ITEM_OPEN,onItemOpen);
			setStyleProps();
		}
		
		
		override public function updateDataProvider(xmlDataProvider:XML,dataLabel:String):void {
			this.enabled = false;
			dataProvider = null;
			labelField = "@" + dataLabel;
			dataProvider = xmlDataProvider;			
			rootId = Number(xmlDataProvider.@NID);
			callLater(dataProviderUpdated);
		}
		
		override public function setDataProvider(nodesDataSet:Object,dataLabel:String,
												dataDesc:ITreeDataDescriptor):void {
			this.enabled = false;
			rootId = 0;
			
			dataProvider = null;
			dataProvider = nodesDataSet[0];
			labelField = "@" + dataLabel;
			
			dataDescriptor = null;
			dataDescriptor = dataDesc;
			(dataDescriptor as PerimetreTreeDataDescriptor).addEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, onChildDataAdded);			
			
			rootId = XML(dataProvider[0]).@NID;
			callLater(dataProviderUpdated);
		}
		
		
		/**
		 * Handler utilisé par le callLater de ce Tree
		 * */
		override protected function dataProviderUpdated():void {
			//this.visible = true;
			this.enabled = true;
			dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.DATA_PROVIDER_SET));
			
		}
		
		/**
		 * Ne pas mettre en protected car n'est utilisé que dans cette classe
		 * */
		override protected function onItemOpening(event: TreeEvent):void {
		 
			var node:XML = (event.item as XML);
			
			var childrenList:XMLList = node.children();
			
			currentIndex = scrollPositionToIndex(horizontalScrollPosition,verticalScrollPosition);
			
			
			if(parseInt(node.@NTY,10) > 0) {
				if(childrenList.length() == 0){
					currentOpenNode = node;
					(dataDescriptor as PerimetreTreeDataDescriptor).loadNodeChild(event.item);
					expandItem(node,false);
				}
			}
		}
		
		override protected function onItemOpen(event: TreeEvent):void 
		{
			var node:XML = (event.item as XML);
			var childrenList:XMLList = node.children();
			if(childrenList.length() == 0)
				expandItem(node,false);
			else
				expandItem(node,true);
		}
		override protected function onChildDataAdded(event:Event):void 
		{
			var nombre:int = 0;
			if(dataProvider != null){
                        var arr : Array = openItems as Array;
                        var localNodID :  Number = XML(dataProvider[0]).@NID;
                          
                  		
                  		var collection:XMLListCollection =  new XMLListCollection(XMLList(dataProvider));
						var cursor:IViewCursor = collection.createCursor();
                        
                        if(cursor.current && localNodID == (cursor.current).@NID){nombre = -1;}else{nombre = localNodID;};
                        
                        if (nombre != rootId || (currentOpenNode == dataProvider[0]))
                        {         
                                                                      validateNow();
                                   var tmpData : Object = dataProvider;
                                   var i : int = 0;
                                   var size : int= (tmpData as XMLListCollection).length;
                                   while(i<size && XML(tmpData[i]).@NID != rootId){
                                         i++;
                                   }
                                   var dataset : XML =  tmpData[i];//                                                                       
                                   dataProvider = dataset;
                                   openItems = arr;
                                   refreshData = true;
                        }
                  }
                  
                  expandItem(currentOpenNode,false);
                  expandItem(currentOpenNode,true);
                  
                  
                  invalidateList();
                  validateNow();
		}
		 
		private var refreshData:Boolean=false;
		
		override protected function commitProperties():void
		{
			super.commitProperties();
			
			if(refreshData)
			{
				refreshData = false;
				
				scrollToIndex(currentIndex);
				verticalScrollPosition = currentIndex; 
				getFocus();
			}
		}
		 
	}
}
