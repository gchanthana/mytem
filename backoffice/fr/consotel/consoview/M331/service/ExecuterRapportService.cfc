﻿<cfcomponent>
	<!--------------------------------------------------------------------------------------------------------------------------------------------------------------------->
	<!-------------------------------------------------------- Préparer l'execution de rapport ---------------------------------------------------------------------------->
	<cffunction access="remote" output="false" name="executeRapport" returntype="boolean" >			
		<cfargument name="tabParametre" type="Array" required="true">
		<cfargument name="selectedRapport" type="Struct" required="true">		
	  	<cftry>
			<!---  préparer du  rapport --->
			<cfset userID   = SESSION.USER.CLIENTACCESSID>
			<cfset racineId = SESSION.PERIMETRE.ID_GROUPE>
			<cfset codeApplication  = SESSION.CODEAPPLICATION>
			<cfset racineid_master  = SESSION.PERIMETRE.IDRACINE_MASTER>
			<cfset code_pays  = SESSION.USER.GLOBALIZATION>			
			<cfset id  = ARGUMENTS["selectedRapport"]["id"] >				
			<cfset localization  = ARGUMENTS["selectedRapport"]["localization"] />
			<cfset template  	 = ARGUMENTS["selectedRapport"]["template"] />
		
			 <!--- reportingTypeRapports : la structure qui doit être envoyé à l'API de cedric permettant d'execution du rapport'  --->
		     <cfset reportingTypeRapports = {
										DESTINATION="NOTIFICATION",
							            RAPPORT = {
							                        ID=#id#, 
							                        USER_ID=#userID#,
							                        IDRACINE=#racineId#, 
							                        CODE_APPLICATION=#codeApplication#,
							                        LOCALIZATION=#code_pays#
							            		  },
							             PARAMETRE={}				            		  					      
		            					}
		    />
			<cfset MyNewArray = ArrayNew(1)> 
			<cfset valeurNiveau="">
			<cfset valeurIdOrga=""> 
			<cfset valueLperimetre = "">
			<cfset GIdNiveau=0>
			
			<!--- déterminer les valeurs de valeurNiveau,valeurIdOrga,valueLperimetre nécessaires à l'execution de rapport --->
			<cfloop from="1" to="#ArrayLen(tabParametre)#" index="i" step="1">
				<cfloop from="1" to="#ArrayLen(tabParametre[i])#" index="j" step="1">
					<cfset cle=ARGUMENTS["tabParametre"][i][j]["cle"] />		
					<cfif (lcase (cle) eq lcase("G_IDPERIMETRE"))>	
						<!--- selecteur périmètre n'est pas affiché dans le L'IHM  et l'utilisateur ne selectionne rien --->
						<!--- execution de rapport sera sur la racine donc  valeurNiveau =A  --->			
						<cfif val(ARGUMENTS["tabParametre"][i][j]["value"][1]) eq -1>
							<cfset ARGUMENTS["tabParametre"][i][j]["value"][1]=racineId > 
							<cfset valeurNiveau="A">
						</cfif>
						<!--- ce code est executé parce que  ARGUMENTS["tabParametre"][i][j]["value"][1] est initialisé à racineId--->
						<cfif ARGUMENTS["tabParametre"][i][j]["value"][1] neq "">	
							<cfset MyNewArray=getAllDataPerimetre(ARGUMENTS["tabParametre"][i][j]["value"][1])/>
							<cfif  MyNewArray[1] eq "A">
								<cfset valeurIdOrga=racineId>
								<cfset valeurNiveau="A">
								<cfelse>
									<cfset valeurNiveau=MyNewArray[1]>
									<cfset valeurIdOrga=MyNewArray[2]>      		
							</cfif>
							<cfelse> 
						</cfif>  
					</cfif>
					<cfif (lcase (cle) eq lcase("L_POOL"))>
						<cfset valueLperimetre = ARGUMENTS["tabParametre"][i][j]["value"][1] >								
					</cfif>
				</cfloop>
			</cfloop>
		
			<!--- associer valeurNiveau à un chiffre exemple : A->0,B->1,C->2 --->
			<!--- A,B,C c'est le profondeur de l'arbre dans le selecteur du périmètre ( front office) --->	
			<cfswitch expression="#valeurNiveau#"> 
				<cfcase value="A"><cfset GIdNiveau=0></cfcase>
				<cfcase value="B"><cfset GIdNiveau=1></cfcase>
				<cfcase value="C"><cfset GIdNiveau=2></cfcase>
				<cfcase value="D"><cfset GIdNiveau=3></cfcase>
				<cfcase value="E"><cfset GIdNiveau=4></cfcase>
				<cfcase value="F"><cfset GIdNiveau=5></cfcase>
				<cfcase value="G"><cfset GIdNiveau=6></cfcase>
				<cfcase value="H"><cfset GIdNiveau=7></cfcase>
				<cfcase value="I"><cfset GIdNiveau=8></cfcase>
				<cfcase value="J"><cfset GIdNiveau=9></cfcase>
				<cfcase value="K"><cfset GIdNiveau=10></cfcase>
			</cfswitch>
	
			<!--- positionner des valeurs sur des varaibles --->
			<cfloop from="1" to="#ArrayLen(tabParametre)#" index="i" step="1">			
				<cfloop from="1" to="#ArrayLen(tabParametre[i])#" index="j" step="1">							
					<cfset cle=ARGUMENTS["tabParametre"][i][j]["cle"] /> <!--- définir une varaible --->			
					<cfif (lcase (cle) neq lcase("FORMAT")) and (lcase(cle) neq lcase("TEMPLATE")) ><!---lcase : convertir la chaine en minuscule --->
						<cfif (lcase (cle) eq lcase("G_NIVEAU"))>
					 		 <cfset reportingTypeRapports.PARAMETRE[cle]=
							 {
							 	VALEUR=[valeurNiveau],
							 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
							 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
							 }/>
							<cfelseif (lcase (cle) eq lcase("G_IDORGA"))>
							 		<cfset reportingTypeRapports.PARAMETRE[cle]=
									 {
									 	VALEUR=[valeurIdOrga],
									 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
									 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
									 }/>
									<cfelseif (lcase (cle) eq lcase("G_IDMASTER"))>
									 		 <cfset reportingTypeRapports.PARAMETRE[cle]=
											 {
											 	VALEUR=[racineid_master],
											 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
											 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
											 }/>
										 	<cfelseif (lcase (cle) eq lcase("G_IDRACINE"))>
											 		 <cfset reportingTypeRapports.PARAMETRE[cle]=
													 {
													 	VALEUR=[racineId],
													 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
													 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
													 }/>
													 <cfelseif (lcase (cle) eq lcase("G_LANGUE_PAYS"))>
													 		 <cfset reportingTypeRapports.PARAMETRE[cle]=
															 {
															 	VALEUR=[code_pays],
															 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
															 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
															 }/>
															  <cfelseif (lcase (cle) eq lcase("G_SEGMENT"))>
															 		 <cfset reportingTypeRapports.PARAMETRE[cle]=
																	 {
																	 	VALEUR=ARGUMENTS["tabParametre"][i][j]["value"],
																	 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
																	 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
																	 }/>
																	  <cfelseif (lcase (cle) eq lcase("L_PERIMETRE")) AND ( valueLperimetre neq "" )>
																	 		 <cfset reportingTypeRapports.PARAMETRE[cle]=
																			 {
																			 	VALEUR=[valueLperimetre],
																			 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
																			 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
																			 }/>
																			  <cfelseif (lcase (cle) eq lcase("G_IDNIVEAU"))>
																			 		 <cfset reportingTypeRapports.PARAMETRE[cle]=
																					 {
																					 	VALEUR=[GIdNiveau],
																					 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
																					 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
																					 }/>
																					 <cfelse>
																					 		<cfset reportingTypeRapports.PARAMETRE[cle]=
																							 {
																							 	VALEUR=ARGUMENTS["tabParametre"][i][j]["value"],
																							 	IS_SYSTEM=ARGUMENTS["tabParametre"][i][j]["IS_SYSTEM"],
																							 	LIBELLE=ARGUMENTS["tabParametre"][i][j]["LIBELLE"]
																							 }/>
					 	</cfif> <!--- fin  : <cfif (lcase (cle) eq lcase("G_NIVEAU"))>  --->	
				     <cfelse>
				     	<cfset value_cle=ARGUMENTS["tabParametre"][i][j]["value"] >
						<cfset reportingTypeRapports.RAPPORT[cle]=value_cle[1] />
						<cfif   (lcase (cle) eq lcase("FORMAT")) >
							<cfset file_ext=get_ext(value_cle)	/>
							<cfset reportingTypeRapports.RAPPORT.FILE_EXT=file_ext>
						</cfif>
					</cfif><!---  fin : <cfif (lcase (cle) neq lcase("FORMAT")) and (lcase(cle) neq lcase("TEMPLATE")) > --->								
				</cfloop> <!--- fin 2 loop  --->			
			</cfloop><!--- fin 1 loop  --->

			<!--- lors que on execute le rapport sur la racine sans choisir un périmètre (selecteur périmètre n'affiche pas dans le L'IHM') --->
			<cfif StructKeyExists(reportingTypeRapports.PARAMETRE,"L_PERIMETRE") >
				<cfif val(reportingTypeRapports.PARAMETRE.L_PERIMETRE.VALEUR[1]) eq -1>
					<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m331.get_l_perimetre">
			              <cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER"  value="#racineId#" variable="p_idperimetre">                
			              <cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR"  variable="p_libelle_perimetre">
			        </cfstoredproc>
					<cfset reportingTypeRapports.PARAMETRE["L_PERIMETRE"]={VALEUR=["#p_libelle_perimetre#"],IS_SYSTEM='NO',LIBELLE='Périmètre'} />
				 </cfif>
			</cfif> 
			<!--- executer le rapport --->
			<cfset  execute=executeRapportByApi(reportingTypeRapports)/>
			<cfreturn execute >
			<cfcatch type="any"><!---en cas d'erreur--->
				<cfset execute=false>
				
				<cfmail to="monitoring@saaswedo.com"
								from="no-reply@saaswedo.com" 
								subject="ApiReporting - Exception dans executeRapport()" type="html" server="mail-cv.consotel.fr:25">
								
					<cfdump var="#CFCATCH#" label="Contenu de l'erreur de l'execution de rapport">
				</cfmail>
				
				<cfreturn execute >
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--------------------------------------------------------------------------------------------------------------------------------------------------------------------->
	<!--------------------------------------------------- Execution du rapport par l'API  de cédric ----------------------------------------------------------------------->
	
	<cffunction  name="executeRapportByApi" access="public" output="false" returntype="boolean">	
		<cfargument name="reportingTypeRapports"  type="struct" required="true" >
		<cftry>
			<!--- API Reporting (Type Rapports) --->
			<cfset TYPE_RAPPORTS="fr.consotel.api.ibis.publisher.reporting.service.Rapports">
			<!--- Instanciation du service avec le login et mot de passe requis pour son utilisation --->
			<cfset service=createObject("component",TYPE_RAPPORTS)>
			<cfset service=service.getService("scheduler","public")>
			<!--- Création de la définition du reportin à partir des propriétés (reportingTypeRapports) --->
			<cfset reporting=service.createReporting(reportingTypeRapports)>
			<!--- Exécution du reporting --->	
			<cfset service.execute(reporting)>		
			
			<!--- permet afficher le message 'votre rapport à été bien executé au niveau du front officce' --->
			<cfset execute=true><!--- la fonction  service.execute ne retourne aucun resultat --->
		
			<cftry>
				
				<cfset properties = reporting.GETPROPERTIES()>
				<cfset subject = properties.DELIVERY.FICHIER>
				
				
				<cfsavecontent variable="body">
					<b>BackOffice : <cfoutput>#CGI.SERVER_NAME#</cfoutput></b><br/>
					<cfdump var="#properties#" >
				</cfsavecontent>

				
				
				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(13)>
				<cfset mLog.setIDMNT_ID(1)>
				<cfset mLog.setBODY(body)>
				<cfset mLog.setSUBJECTID(subject)>
				
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="monitoring@saaswedo.com" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			
			</cftry>

				
			<cfreturn execute >
			
			<cfcatch type="any"><!---en cas d'erreur'--->
				<cfset execute=false>
				<cfmail to="monitoring@saaswedo.com" from="no-reply@saaswedo.com" subject="ApiReporting - Exception dans executeRapportByApi()" type="html" server="mail-cv.consotel.fr:25">
					<!--- CFCATCH un  mot cle de type structure  --->
					<cfdump var="#CFCATCH#" label="Contenu de l'erreur de l'execution de rapport par l'API">
				</cfmail>
				<cfreturn execute >
				
			</cfcatch>
		</cftry>
	</cffunction>

	<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->
	<!------------------------ getAllDataPerimetre fonction qui permet de récupérer le niveau dans l'arbre des périmètres ------------------------------------------------>
	
	<cffunction name="getAllDataPerimetre" returntype="array" access="public" output="false">
		<cfargument name="idPerimetre"  type="numeric" required="true" >
		<cftry>
			<cfset valeurIdPerimetre=ARGUMENTS.idPerimetre>		
			     <cfquery datasource="#SESSION.OFFREDSN#" name="qOrga">
	                       SELECT PKG_CV_RAGO.GET_ORGA(#valeurIdPerimetre#) as IDORGA FROM DUAL
	             </cfquery>
				<cfstoredproc datasource="ROCOFFRE" procedure="pkg_consoview_v3.get_numero_niveau">
	                   <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER"     value="#valeurIdPerimetre#">                
	                   <cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR"    variable="p_retour">
	            </cfstoredproc>
			<cfset NewArray = ArrayNew(1)> 
			<cfset NewArray[1] = p_retour> 
			<cfset NewArray[2] = qOrga['IDORGA'][1]> 
			
			<cfreturn NewArray>
			
			<cfcatch type="any">
				<cfmail to="monitoring@saaswedo.com" from="no-reply@saaswedo.com" subject="ApiReporting - Exception dans executeRapportByApi()" type="html" server="mail-cv.consotel.fr:25">
						<cfdump var="#CFCATCH#" label="Erreur orga">
				</cfmail> 
				
				<cfset NewArray = ArrayNew(1)>
				<cfset NewArray[1] = -1> 
				<cfset NewArray[2] = 0> 
				
				<cfreturn NewArray>
			</cfcatch>
	   </cftry>
	</cffunction>

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--------------------------------------- permet de récupérer les extention selon le format de rapport ------------------------------------------------------------------->

	<cffunction name="get_ext" returntype="String" access="public" output="false" >
		<cfargument name="format"  type="Array" required="true" >
		<cftry>
			<cfset valeurFormat=ARGUMENTS.format>
			<cfset listeDesExt={EXCEL="xls", PDF="pdf", CSV="csv"}>
			<cfif structKeyExists(listeDesExt,valeurFormat[1])>
				
			<cfelse>
				<!--- on declenche une erreur  --->
				<cfthrow type="Custom" message="l'extention est introuvable pour le format : #valeurFormat[1]#">
			</cfif>
			
			<cfreturn listeDesExt[valeurFormat[1]]>
			
			<cfcatch type="any">
				
				<cfmail to="monitoring@saaswedo.com" from="no-reply@saaswedo.com" subject="ApiReporting - Exception dans executeRapportByApi()" type="html" server="mail-cv.consotel.fr:25">
					<cfdump var="#CFCATCH#" label="Erreur extention format">
				</cfmail> 
				
				<cfreturn "error : get_ext">
			</cfcatch>
	   </cftry>
	</cffunction>
</cfcomponent>