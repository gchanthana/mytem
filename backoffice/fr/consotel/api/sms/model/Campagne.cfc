<cfcomponent displayname="Class for campaign" output="true">

	<cffunction name="create" output="true" access="public" returntype="boolean" hint="I insert a campaign.">

		<cfargument name="data" type="struct" required="true" hint="This is a structure.">
		<cfset uuid =arguments.data.UUID>
		<cfset valid =arguments.data.VALID>
		<cfset idracine =arguments.data.IDRACINE>

		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.createCampagne_v2">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#uuid#" variable="p_uuid">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#valid#" variable="p_valide">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#idracine#" variable="p_valide">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">
		</cfstoredproc>

		<cfif p_retour eq 1 >
			<cfreturn true >
			<cfelse>
				<cfreturn false >
		</cfif>

	</cffunction>

	<!--- récupérer id de campagne --->
	<cffunction name="getIdCampaign" access="public" returntype="any" hint=" I get id campaign" output="true">

		<cfargument name="uuid"  required="true" type="string"  hint="UUID campaign">

		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.getIdFromUuuid">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#uuid#" variable="p_uuid" >
				<cfprocresult name="qUUID">
			</cfstoredproc>
		<cfcatch>
			<cfreturn false>
		</cfcatch>
		</cftry>
		<cfreturn "#qUUID.id#">

	</cffunction>

	<!--- enregistrer les sms dans la base  --->
	<cffunction name="insertSms" output="true" access="public" returntype="any" hint="I insert a SMS">

		<cfargument name="data" type="struct" required="true" hint="This is a structure">

		<cfset contenu_sms=arguments.data.contentSms>
		<cfset phone=arguments.data.phoneNumber>
		<cfset id_uuid=arguments.data.id_uuid>
		<cfset id_operator=arguments.data.id_operator>
		<cfset messageid=arguments.data.messageid>

		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_api_sms.addSmsContent_v2">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#contenu_sms#" variable="p_contenu">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#phone#" variable="p_ligne">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#id_uuid#" variable="p_idcampagne">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#id_operator#" variable="p_idoperateur">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#messageid#" variable="p_messageid">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">
		</cfstoredproc>

		<cfif p_retour eq 1 >
			<cfreturn true >
		<cfelse>
			<cfreturn false >
		</cfif>

	</cffunction>

</cfcomponent>