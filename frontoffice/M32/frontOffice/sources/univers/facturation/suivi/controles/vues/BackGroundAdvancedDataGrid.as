package univers.facturation.suivi.controles.vues
{
	import flash.display.Sprite;
	
	import mx.controls.AdvancedDataGrid;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;

	public class BackGroundAdvancedDataGrid extends AdvancedDataGrid
	{
		override protected function drawColumnBackground(s:Sprite, columnIndex:int,color:uint, column:AdvancedDataGridColumn):void
        {
            if ( rowInfo[listItems.length - 1] != null )
                  super.drawColumnBackground( s, columnIndex, color, column ) ;
        }
	}
}