package composants.tb.graph
{
	import mx.events.FlexEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	import composants.tb.graph.charts.PieGraph_IHM;
	import mx.rpc.events.ResultEvent;
	import mx.controls.Alert;
	import mx.charts.HitData;
	 
	import composants.util.DateFunction;
	
	public class GraphEvolution extends BarGraphBase
	{
		private var op : AbstractOperation;
		
		[Bindable]
		private var _dataProviders : ArrayCollection;
		
		[Bindable]
		private var _dataProviders2 : ArrayCollection;
	 		
		private var _totalAbo : Number;

		private var _totalConso : Number;

		private var _segment : String;
				
		public function GraphEvolution(segment : String = "Complet")
		{	
			super();
			setSegment(segment);
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		override public  function clean():void{		
			graph.myChartHistorique.dataProvider = null;
			myGrid.dataProvider = null;
			
		}
				
		/**
		* Permet de passer les valeurs au composant.(Inutilisée)		
		* @param d Une collection d'objets. 
		* @see setXMLdataProviders, getXMLdataProviders
		**/						
		override public function set dataProviders(d : ArrayCollection):void{
						
		}
		
		/**
		*  Retourne le dataproviders
		* Inutilisée
		**/	
		[Bindable]			
		override public   function get dataProviders():ArrayCollection{
			return null;
		}	
		
		/**
		* Permet de passer les valeurs au composant.		
		* @param d Une XMLList d'objets. 
		**/						
		public function setXMLdataProviders(l : Object):void{
			
			_dataProviders = _formatterGridDataProvider(l);
			_dataProviders2 = _formatterChartDataProvider(l);
			
			updateProviders();
			
		}
		
		/**
		* Retourne le dataproviders		
		**/				
		public function getGridataProviders():ArrayCollection{
			return _dataProviders;
		}	
		/**
		* Retourne le dataproviders		
		**/				
		public function getChartdataProviders():ArrayCollection{
			return _dataProviders2;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		override public function update():void{			
			chargerDonnees();			 
		}	
		
		/** 
		 * Met à jour le segment et rafraichit le graph
		 * */
		 override public function updateSegment(segment:String=null):void{
		 	_segment = segment;			 		 	
		 }
		/*--------------------- PROTECTED -----------------------------------------------*/
		protected function _formatterGridDataProvider(elmt : Object):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();			 
			var i : String;
			var s : String;
						
			_totalAbo = 0;
			_totalConso = 0;
			
		
		
			for	(i in elmt){	
				var obj : Object = new Object();
				for (s in elmt[i] ){
					
					if (!isNaN(parseFloat(elmt[i][s]))) obj[s] = parseFloat(elmt[i][s]);
						else  obj[s] = elmt[i][s];
				}										 
				tmpCollection.addItem(formateObject(obj));																		 								
			}	
			
		 	var totalObject : Object = new Object();
		 	
			totalObject["Pèriode"] = "TOTAL";
			totalObject["Consommations"] = cf.format(_totalConso);
			totalObject["Abonnements"] = cf.format(_totalAbo);
			
			tmpCollection.addItem(totalObject);
					
			return tmpCollection; 
		}	
		
		protected function _formatterChartDataProvider(elmt : Object):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();			 
			var i : String;
			var s : String;
						
			_totalAbo = 0;
			_totalConso = 0;
			
			for	(i in elmt){	
				var obj : Object = new Object();
				for (s in elmt[i] ){
					
					if (!isNaN(parseFloat(elmt[i][s]))) obj[s] = parseFloat(elmt[i][s]);
						else  obj[s] = elmt[i][s];
				}										 
				tmpCollection.addItem(formateObjectChart(obj));																		 								
			}									
			return tmpCollection;
		}	
				
		protected function formateObject(obj : Object):Object{
			var o : Object = new Object();
				
			_totalAbo = _totalAbo + parseFloat(obj.COUTABO);	
			_totalConso = _totalConso + parseFloat(obj.COUTCONSO);	
		
			o["Abonnements"] = cf.format(obj.COUTABO);	
			o["Consommations"] = cf.format(obj.COUTCONSO);
			o["Pèriode"] = obj.MOIS;	
			
		
														
			return o;
		}	
		
		protected function formateObjectChart(obj : Object):Object{
			var o : Object = new Object();
				
			_totalAbo = _totalAbo + parseFloat(obj.COUTABO);	
			_totalConso = _totalConso + parseFloat(obj.COUTCONSO);	
		
			o["Abonnements"] = parseFloat(obj.COUTABO);	
			o["Consommations"] = parseFloat(obj.COUTCONSO);
			o["Pèriode"] = obj.MOIS;	
													
			return o;
		}	
		
		/*---------- PRIVATE --------------------*/
		private function init(fe : FlexEvent):void{
			title = "Evolution des coûts sur les 6 derniers mois";			
		}
		
		
		private function updateProviders():void{
			myGrid.dataProvider = _dataProviders;			
			graph.myChartHistorique.dataProvider = _dataProviders2;			 
			 
		}
		
		private function setSegment(segment : String):void{
			
			if (segment == "null")
				_segment = "Complet";
			else
				_segment = segment;
		};
					
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected override function chargerDonnees():void{
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getEvolution",
																			chargerDonneesResultHandler,null);			
			RemoteObjectUtil.callService(op,
									perimetre,
									modeSelection,
									identifiant,
									2,
									DateFunction.getPeriode()[6].toString(),
									DateFunction.getPeriode().pop().toString(),
									_segment);		
								 
									
			
		}	
		
		protected override function chargerDonneesResultHandler(re :ResultEvent):void{	
				
		  		setXMLdataProviders(re.result);		  		
		} 	
		
	}
}