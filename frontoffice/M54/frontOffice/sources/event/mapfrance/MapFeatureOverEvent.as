package event.mapfrance
{
	import flash.events.Event;
	
	import ilog.maps.MapFeature;
	
	/**
	 * classe relative à l'événement MapFeatureOverEvent, ayant comme propriété _mapF de type MapFeature
	 */
	public class MapFeatureOverEvent extends Event
	{
		
		private var _mapF:MapFeature;
		
		//bubbles indiquent si l'evenement peut être propagé, cancelable s'il peut être arrêté en cours de propagation
		public function MapFeatureOverEvent(type:String, mapF:MapFeature, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_mapF=mapF;
		}
		
		/**
		 * Retourne un élément de carte de type MapFeature.
		 */
		public function get mapF():MapFeature
		{
			return _mapF;
		}

		public function set mapF(value:MapFeature):void
		{
			_mapF = value;
		}

		override public function clone():Event
		{
			
			return new MapFeatureOverEvent(type, mapF, bubbles,cancelable);	
		}
	}
}