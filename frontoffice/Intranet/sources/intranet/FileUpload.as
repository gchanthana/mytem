package intranet
{
	import flash.events.Event;
	import mx.controls.ProgressBar;
	import mx.controls.Button;
	import flash.net.FileReference;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	
	public class FileUpload
	{
		internal const UPLOAD_URL:String = urlBackoffic + "/fr/consotel/consoprod/intranet/processUpload.cfm";
		internal var fr:FileReference;
		internal var pb:ProgressBar;
		internal var btn:Button;
		
		public function init(pb:ProgressBar, btn:Button):void
			{
			    this.pb = pb;
			    this.btn = btn;
			
			    fr = new FileReference();
			    fr.addEventListener(Event.SELECT, selectHandler);
			    fr.addEventListener(Event.OPEN, openHandler);
			    fr.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			    fr.addEventListener(Event.COMPLETE, completeHandler);
			}

		public function startUpload():void
			{
			    fr.browse();
			}
			
		private function selectHandler(event:Event):void
			{
			    var request:URLRequest = new URLRequest();
			    request.url = UPLOAD_URL;
			    fr.upload(request);
			}
		
		 /**
     * When the OPEN event has dispatched, change the progress bar's label 
     * and enable the "Cancel" button, which allows the user to abort the 
     * download operation.
     */
	    private function openHandler(event:Event):void
	    {
	        pb.label = "DOWNLOADING %3%%";
	        btn.enabled = true;
	    }
	/**
     * While the file is downloading, update the progress bar's status.
     */
    private function progressHandler(event:ProgressEvent):void
    {
        pb.setProgress(event.bytesLoaded, event.bytesTotal);
    }
		
	/**
     * Once the download has completed, change the progress bar's label one 
     * last time and disable the "Cancel" button since the download is 
     * already completed.
     */
    private function completeHandler(event:Event):void
    {
        pb.label = "DOWNLOAD COMPLETE";
        btn.enabled = false;
    }
	
	/**
     * Cancel the current file download.
     */
    public function cancelUpload():void
    {
        fr.cancel();
        pb.label = "UPLOAD CANCELLED";
        btn.enabled = false;
    }

	}
}