<cfcomponent output="false"  
			 displayname="DataService" 
			 hint="Classe regroupant les methodes récupérant les données concernant l'utilisateur" >
	
	
	<!--- RÉCUPÈRE L'ID EMPLOYÉ --->
	<cffunction name="getIdEmploye" access="remote" output="true" returntype="query" 
				displayname="" 
				hint="Récupère l'ID et d'autres infos employé d'un client">
		<cfargument name="email" type="string" required="true">
		<cfargument name="clientId" type="numeric" required="true">
		<cfset SESSION.OffreDSN="ROCOFFRE">

		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_PORTAIL_END_USER.getInfoEmploye" >
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR2" variable="p_email" value="#email#" >
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#clientId#" >
			
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>
	
	
	<!--- FOURNIS LA LISTE DES EQUIPEMENTS ASSOCIÉS A UN COLLABORATEUR --->
	<cffunction name="getListEqptFromCollab" access="remote" returntype="query" 
				hint="Fournis la liste des Equipements associés a un collaborateur">
		<cfargument name="idEmploye" required="true" type="numeric"/>
		<cfargument name="idRacine" required="true" type="numeric"/>
		
		<cfset SESSION.OffreDSN="ROCOFFRE">
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M111.getTermCollab">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#" >
		    <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idemploye" value="#idemploye#" >
			
		<cfprocresult name="p_retour"/>
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	
	
	<!--- RECUPERE LES DATES DE FACTURATION(EMISSION) D'UNE LIGNE --->
	<cffunction name="getDateFactByLigne" output="true" access="remote" returnType="query" 
				displayname="" 
				hint="Récupère les dates de facturation (emission) d'une ligne">
		<cfargument name="idSous_tete" required="true" type="numeric" />
		<cfargument name="id_racine" required="true" type="numeric" />
		<cfargument name="nbMois" required="true" type="numeric" default="6" />
		<cfargument name="langue" required="true" type="string" />
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_PORTAIL_END_USER.getDateFactuLgn">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete" value="#idSous_tete#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#id_racine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_nb_mois" value="#nbMois#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_langue" value="#langue#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>
	</cffunction>
	
	
	
	<!--- RECUPERE LE COUT MOYEN MENSUEL SUR LES 6 DERNIERS MOIS DE FACTURATION POUR UNE LIGNE --->
	<cffunction name="getMontantFactureByLigne" output="false" access="public" returnType="query" 
				displayname="" 
				hint="Récupère les montants de facturation d'une ligne pour une date d'emission donnée">
		<cfargument name="idracine_master" required="true" type="numeric" defaut=0>
		<cfargument name="idSous_tete" required="true" type="numeric">
		<cfargument name="idInventaire_periode" required="true" type="numeric" defaut=0>
		<cfargument name="langue" required="true" type="string">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_PORTAIL_END_USER.getLgnFactuMontant_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="310812" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idsous_tete" value="#idSous_tete#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#idInventaire_periode#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" variable="p_langue" value="#langue#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result >
	</cffunction>
	
	
	
	<!--- CREATION DU CHART AERA / PAGE ACCUEIL --->
	<cffunction name="getChartAera" output="true" access="remote" returnType="Any">
		<cfargument name="idSous_Tete" required="false" type="numeric" default=0>
		<cfargument name="moyMois" required="true" type="any" default= " -- ">
		
		<!--- debut chart --->
		<cfsavecontent variable="aera" >
		
			<cfchart title="Évolution de votre facture sur les #moyMois# derniers mois"
			   		 showLegend="false" showMarkers="true" showXGridlines="true" showYGridlines="true"
					 format="png" scalefrom="0" scaleto="50" xOffset="0.1" labelFormat="currency"
					 show3d="true" chartHeight="200" chartWidth="480" markersize="11">
				   
			  <cfchartseries type="area"
			      			 serieslabel="Évolution de votre facture sur les #moyMois# derniers mois"
			      			 seriescolor="##009fe3" > <!--- ##318e24 --->
			    
				<!--- traitement données --->
				<cfif #SESSION.queryFact.recordcount# gt 0> <!--- si query date de facturation est non vide --->
					<cfset cpt = 0>
					<cfset idInventaire_periode = 0 >
					<cfset saveIdPeriode = 0 >
						
						<!--- loop sur query factu --->
						<cfloop index="i" step="-1" from="#SESSION.queryFact.recordcount#" to="1" ><!--- boucle sur le max de enregistrement --->
							<cfset currentRow = #i# >
							
							
							<cfif #saveIdPeriode# neq #SESSION.queryFact["IDPERIODE_MOIS"][#currentRow#]#>
								<cfset saveIdPeriode = #SESSION.queryFact["IDPERIODE_MOIS"][#currentRow#]# >
								<cfset cpt = #cpt# + 1>
								
								<cfset moisFacture = SESSION.queryFact["DATE_EMISSION"][#currentRow#] >
								<cfset date1 = #moisFacture# >
								<cfset dateFact = LSDateFormat(#date1#, "dd/mm/yyyy") >
								
								<cfset idInventaire_periode = SESSION.queryfact["idinventaire_periode"][#currentrow#] >
								<cfset mnt = SESSION.queryfact["montant"][#currentrow#] >
							</cfif>
							
							<cfif #cpt# eq 7> <!---  on sort de la boucle au bout de 6 (mois) si il y a plus que 6 mois dans le resultset --->
								<cfbreak>
							</cfif>
						
						<!--- initialisation data graphique --->
						<cfchartdata item="#dateFact#" value="#mnt#" >
						
						</cfloop><!--- fin loop --->
						
				</cfif>
				
			  </cfchartseries>
			  
			</cfchart><!--- fin chart --->
			
		</cfsavecontent>
		
		<cfreturn #aera#>
		
	</cffunction>
	
	
	
	<!--- CREATION DU CHART PIE / PAGE MES FACTURES --->
	<cffunction name="getChartPie" output="true" access="remote" returnType="Any">
		<cfargument name="abo" required="true" type="numeric" default=0>
		<cfargument name="conso" required="true" type="numeric" default=0>
		<cfargument name="dateGraph" required="false" type="string" default="" >
		
		<cfsavecontent variable="pie" >
			
			<cfchart title="#dateGraph#"
					 showLegend="true" showMarkers="true" showXGridlines="true" pieslicestyle="sliced"
				     format="png" scalefrom="0" scaleto="100" 
				     show3d="true" chartHeight="240" chartWidth="480" labelFormat="percent">
					     
			  <cfchartseries
			      type="pie"
			      serieslabel="#dateGraph#"
			      colorlist="##009FE3,##71CAEF" >
				      
					<cfchartdata item="Abonnements" value="#abo#">
	    			<cfchartdata item="Consommations" value="#conso#">
			    
			  </cfchartseries>
			  
			</cfchart>
			
		</cfsavecontent>
		
		<cfreturn #pie#>
	</cffunction>
	
	
	
	<!--- FORMATAGE DU NUMERO DE TELEPHONE / FORMATÉ POUR LA FRANCE POUR LE MOMENT --->
	<cffunction name="phoneFormat" returnformat="plain" access="public"> 
		<cfargument name="phone" required="true" type="string"> 
		
		<cfset formattedPhone = "">
		<cfset cleanPhone = ReReplace(#phone#, "[^0-9]", "", "ALL")> 
		
		<cfif len(cleanPhone) eq 10>
			
			<cfset areaCode = Left(cleanPhone, 2)> 
			<cfset midFirst = Mid(cleanPhone, 3,2)>
			<cfset midTwo = Mid(cleanPhone, 5,2)>
			<cfset midThree = Mid(cleanPhone, 7,2)>
			<cfset lastTwo = Right(cleanPhone, 2)> 
			<cfset formattedPhone="#areaCode# #midFirst# #midTwo# #midThree# #lastTwo#">

		<cfelseif len(cleanPhone) eq 7> 
		
			<cfset firstThree = Left(cleanPhone, 3)> 
			<cfset lastFour = Right(cleanPhone, 4)> 
			<cfset formattedPhone="#firstThree#-#lastFour#"> 

		</cfif>
		<cfreturn formattedPhone> 
	</cffunction>
		
	
</cfcomponent>