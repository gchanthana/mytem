package intranet.GestionProduit
{
	import intranet.GestionProduit.event.GTViewProduit_event;
	
	import mx.containers.Accordion;
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.events.FlexEvent;
	import mx.events.IndexChangedEvent;

	public class OngletTheme extends Canvas
	{
		public var GT:GestionThemeIHM;
		public var GT2:GestionTheme2IHM;		
		public var Acc:Accordion;
		public var h1:HBox;
		public var h2:HBox;

		public function OngletTheme()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function init(e:FlexEvent):void{
			this.addEventListener(GTViewProduit_event.GTViewProduit_event,ViewH2);
			Acc.addEventListener(IndexChangedEvent.CHANGE,ChangePanel);
		}
		
		public function ChangePanel(e:IndexChangedEvent):void{
			if(e.newIndex==0){
				ViewH1();
			}
		}

		public function ViewH1():void{
			Acc.selectedIndex=0;
			h2.label="Affichage des produits du thème";
			this.GT.getData();
		}
		
		public function ViewH2(e:GTViewProduit_event):void
		{
			Acc.selectedIndex=1;
			h2.label="Affichage des produits du thème '"+e.str+"'";
			this.GT2.idproduit=e.id;				
			this.GT2.getData();
		}
	}
}