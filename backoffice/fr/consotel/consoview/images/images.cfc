<cfcomponent name="images" access="public">
<!--- 
		EXEMPLE D'Utilisation de ce composant
		
		<cfset objimg=createObject("component","com.consoview.images.images")>
		<cfset objimg.displayimage(4,60)> 
--->

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
<cffunction name="displayimage" output="true" hint="Affiche l'image" 
					access="public" returntype="void">
	<cfargument name="p_idlogo" required="true" type="numeric" hint="id du logo de la table">
	<cfargument name="p_width" required="false" type="numeric" default="0" hint="width de l'image">
	
	<cfoutput>
		<cfif p_width eq "0">
			<img src="/fr/consotel/consoview/cfm/loadimage.cfm?idlogo=#p_idlogo#">
		<cfelse>
			<img src="/fr/consotel/consoview/cfm/loadimage.cfm?idlogo=#p_idlogo#" width="#p_width#">
		</cfif>
	</cfoutput>
</cffunction>
 

</cfcomponent>
