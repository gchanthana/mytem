package univers.inventaire.equipements.gestionflottemobile
{
	import flash.display.GradientType;
	import flash.geom.Matrix;
	
	import mx.skins.halo.HaloBorder;
	
	public class CanvasSkin extends HaloBorder
	{
	
		public function CanvasSkin() {
			 super(); 
		}
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
				graphics.clear();
				//Draw "border"
				graphics.beginFill(0xDDD9D7,1);
				var leftBorder : int = 0;
				graphics.drawRect(leftBorder, 0, unscaledWidth, unscaledHeight);
				
				graphics.beginFill(0xFFFFFF, 1);
				
				graphics.drawRect(leftBorder + 1, 1, unscaledWidth - 2, unscaledHeight - 2);
				var matrixRotate90 : Matrix = new Matrix();
				
				matrixRotate90.createGradientBox(unscaledWidth - 4, 12, -Math.PI / 2, 0, unscaledHeight - 13);
				graphics.beginGradientFill(GradientType.LINEAR, [0xdfdfdf, 0xFFFFFF], [0.5, 0.5], [0, 255], matrixRotate90);
				
				graphics.drawRect(leftBorder + 2, unscaledHeight - 13, unscaledWidth - 4, 12);
				graphics.endFill();
		}
		
	

	}
}