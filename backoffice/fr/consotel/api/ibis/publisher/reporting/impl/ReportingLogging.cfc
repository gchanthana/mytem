<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.reporting.impl.ReportingLogging"
implements="fr.consotel.api.ibis.publisher.reporting.IReportingService" hint="Implementation des méthodes de logging du service de reporting
- Le logging est implémenté avec CFLOG et CFMAIL (Affichage de message de log et notifications mails)
- Le fichier de log dédié à l'API est retourné par getLog()
- Le destinataire principal des notifications mails est retourné par 
- Le domaine des addresses mails des notifications mails est VARIABLES.SERVICE.DOMAIN
Aucune implémentation n'est fournie pour les méthodes de l'interface IReportingService à l'exception de getService()
Cette implémentation met à disposition des méthodes permettant d'écrire des messages de log (Pas d'interface correspondante actuellement)">
	<!--- fr.consotel.api.ibis.publisher.reporting.impl.ReportingLogging --->
	<cffunction access="public" name="getLog" returntype="String" hint="Retourne la valeur utilisée pour l'attribut file de CFLOG">
		<cfreturn getServiceProperties().LOG>
	</cffunction>
	
	<cffunction access="public" name="logInfo" returntype="void" hint="Affiche le message dans les logs avec le level INFORMATION. La valeur de l'attribut file est getLog()
	Aucune action n'est effectuée si getServiceProperties().DEBUG_LEVEL vaut TRUE">
		<cfargument name="message" type="String" required="true" hint="Message à logger">
		<cflog file="#getLog()#" type="information" text="#ARGUMENTS.message#">
	</cffunction>

	<cffunction access="public" name="logWarning" returntype="void" hint="Affiche le message dans les logs avec le level WARNING. La valeur de l'attribut file est getLog()
	Aucune action n'est effectuée si getServiceProperties().DEBUG_LEVEL vaut TRUE">
		<cfargument name="message" type="String" required="true" hint="Message à logger">
		<cflog file="#getLog()#" type="warning" text="#ARGUMENTS.message#">
	</cffunction>
	
	<cffunction access="public" name="logError" returntype="void" hint="Affiche le message dans les logs avec le level ERROR. La valeur de l'attribut file est getLog()
	Le message de log est affiché quelque soit la valeur retournée par getServiceProperties().DEBUG_LEVEL">
		<cfargument name="message" type="String" required="true" hint="Message à logger">
		<cflog file="#getLog()#" type="error" text="#ARGUMENTS.message#">
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.reporting.IReportingService --->
	<cffunction access="public" name="getService" returntype="fr.consotel.api.ibis.publisher.reporting.IReportingService"
	hint="Initialise le service en appelant les méthodes : setErrorProperties() et setServiceProperties()">
		<cfargument name="user" type="string"  required="true" hint="Login Utilisateur">
		<cfargument name="pwd" type="string"  required="true" hint="Mot de passe Utilisateur">
		<!--- Défini les propriétés utilisés pour le log des exceptions --->
		<cfset setErrorProperties()>
		<!--- Initialisation des propriétés du service --->
		<cfset setServiceProperties()>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="createReporting" returntype="fr.consotel.api.ibis.publisher.reporting.IReporting" hint="N'est pas implémentée. Lève une exception">
		<cfargument name="props" type="Struct"  required="true" hint="Contient les propriétés et paramètres">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION">
	</cffunction>
	
	<cffunction access="public" name="execute" returntype="void" hint="N'est pas implémentée. Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION">
	</cffunction>
	
	<cffunction access="public" name="handleNotification" returntype="void" hint="N'est pas implémentée. Lève une exception">
		<cfargument name="notification" type="Struct"  required="true" hint="Structure représentant la notification">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION">
	</cffunction>
	
	<cffunction access="public" name="dispatchReportingEvent" returntype="void" hint="N'est pas implémentée. Lève une exception">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION">
	</cffunction>
	
	<!--- Méthodes de traitement des erreurs et exceptions --->
	<cffunction access="private" name="persistantError" returntype="boolean" hint="Retourne TRUE si les exceptions sont stockées et FALSE sinon (Par défaut)">
		<cfreturn FALSE>
	</cffunction>
	
	<cffunction access="private" name="handleException" returntype="void" hint="Traitement par défaut des exceptions capturées avec CFCATCH
	Si la méthode persistantError() vaut TRUE alors l'exception est stockée avec la méthode storeException(). Ce stockage influant sur les performances
	Les exceptions durant les notifications et stockages des exceptions ne sont pas stockées (Car indirectement n'étant pas directement liées à l'API)">
		<cfargument name="catchObject" type="any" required="true" hint="Exception capturée avec CFCATCH. Doit contenir les clés (MESSAGE,DETAIL,ERRORCODE,TAGCONTEXT)">
		<cfargument name="customDesc" type="String"  required="false" hint="Eventuellement une description supplémentaire concernant l'exception (200 caractères max)">
		<cfargument name="reportingProps" type="Struct"  required="false" hint="Eventuellement les propriétés du reporting correspondant">
		<!--- Infos concernant l'exception --->
		<cfset var errorInfos=ARGUMENTS["catchObject"]>
		<!--- Sujet de la notification d'erreur --->
		<cfset var exceptionSubject="Une exception a été capturée par l'API (Sans message d'erreur)">
		<!--- Description supplémentaire à l'exception --->
		<cfset var addDesc="N.D">
		<!--- Diffusion de la notification mail de l'exception --->
		<cftry>
			<cfif LEN(TRIM(exceptionSubject)) GT 0>
				<cfset exceptionSubject=errorInfos.MESSAGE>
			</cfif>
			
			
			
			
			
			
			<cfsavecontent variable="body">
				<b><u>Détails de l'exception :</u></b><br>
				<cfoutput>
					- Stockage de l'exception : #persistantError()#<br>
					- Code d'erreur : #errorInfos.ERRORCODE#<br>
					- Message : #errorInfos.MESSAGE#<br>
					- Detail : #errorInfos.DETAIL#<br><br>
					<cfif isDefined("ARGUMENTS.customDesc")>
						<b><u>Description supplémentaire à l'exception :</u></b><br>
						#ARGUMENTS.customDesc#<br><br>
					</cfif>
					<b><u>Contexte Web :</u></b><br>
					BackOffice : #CGI.SERVER_NAME#<br>
					Remote Host : #CGI.REMOTE_HOST# (#CGI.REMOTE_ADDR#) [#CGI.HTTP_USER_AGENT#]<br>
					Page : #CGI.SCRIPT_NAME#<br>
					Méthode HTTP : #CGI.REQUEST_METHOD#<br><br>
					<cfdump var="#errorInfos.TAGCONTEXT#" label="Contexte d'exécution"><br>
					<cfif isDefined("ARGUMENTS.reportingProps")>
						<cfdump var="#ARGUMENTS.reportingProps#" label="Propriétés fournies pour le reporting">
					</cfif>
				</cfoutput>
			</cfsavecontent>
			
			<cftry>
				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(13)>
				<cfset mLog.setIDMNT_ID(29)>
				<cfset mLog.setBODY(body)>
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			
			</cftry>
			
			
			
			
			
			
			<!---
			<cfmail attributecollection="#getMailProperties().ERROR#" subject="#exceptionSubject#">
				<b><u>Détails de l'exception :</u></b><br>
				<cfoutput>
					- Stockage de l'exception : #persistantError()#<br>
					- Code d'erreur : #errorInfos.ERRORCODE#<br>
					- Message : #errorInfos.MESSAGE#<br>
					- Detail : #errorInfos.DETAIL#<br><br>
					<cfif isDefined("ARGUMENTS.customDesc")>
						<b><u>Description supplémentaire à l'exception :</u></b><br>
						#ARGUMENTS.customDesc#<br><br>
					</cfif>
					<b><u>Contexte Web :</u></b><br>
					BackOffice : #CGI.SERVER_NAME#<br>
					Remote Host : #CGI.REMOTE_HOST# (#CGI.REMOTE_ADDR#) [#CGI.HTTP_USER_AGENT#]<br>
					Page : #CGI.SCRIPT_NAME#<br>
					Méthode HTTP : #CGI.REQUEST_METHOD#<br><br>
					<cfdump var="#errorInfos.TAGCONTEXT#" label="Contexte d'exécution"><br>
					<cfif isDefined("ARGUMENTS.reportingProps")>
						<cfdump var="#ARGUMENTS.reportingProps#" label="Propriétés fournies pour le reporting">
					</cfif>
				</cfoutput>
			</cfmail>
			--->
			
			
			
			
			<!--- Stockage de l'exception : Activé --->
			<cfif persistantError()>
				<cfset var storedDesc="N.D">
				<cfset var errorCode=1>
				<cfif isDefined("errorInfos.MESSAGE") AND (LEN(TRIM(errorInfos.MESSAGE)) GT 0)>
					<cfset storedDesc=errorInfos.MESSAGE>
				</cfif>
				<cfif isDefined("errorInfos.DETAIL") AND (LEN(TRIM(errorInfos.DETAIL)) GT 0)>
					<cfset storedDesc=errorInfos.DETAIL>
				</cfif>
				<cfif isDefined("errorInfos.ERRORCODE")>
					<cfset errorCode=errorInfos.ERRORCODE>
				</cfif>
				<cfset storeException({CLASS="", METHOD="", EVENT=errorCode, DESC=storedDesc})>
			<!--- Stockage de l'exception : Désactivé --->
			<cfelse>
				<cfset logInfo("Storing Exception : DISABLED")>
			</cfif>
			<!--- Notification mail en cas d'exception (de la notification précédente) --->
			<cfcatch type="any">
				<cfset logError("Exception Handling FAILED : " & CFCATCH.MESSAGE)>
				<!--- Diffusion d'une autre notification d'exception notifiant la précédente : N'est pas stockée car n'est pas directement liée à l'API (Pas d'appel récursif non plus) --->
				<cfset exceptionSubject="Echec du traitement d'une exception">
				
				<cfsavecontent variable="body">
				   Le traitement d'une exception capturée par l'API a échouée avec l'erreur suivante :<br><br>  
					<cfoutput>
						- Stockage de l'exception : #persistantError()#<br>
						- Code d'erreur : #CFCATCH.ERRORCODE#<br>
						- Message : #CFCATCH.MESSAGE#<br>
						- Detail : #CFCATCH.DETAIL#<br><br>
						<b><u>Contexte Web :</u></b><br>
						BackOffice : #CGI.SERVER_NAME#<br>
						Remote Host : #CGI.REMOTE_HOST# (#CGI.REMOTE_ADDR#) [#CGI.HTTP_USER_AGENT#]<br>
						Page : #CGI.SCRIPT_NAME#<br>
						Méthode HTTP : #CGI.REQUEST_METHOD#<br><br>
						<cfdump var="#CFCATCH.TAGCONTEXT#" label="Contexte d'exécution"><br>
						<cfdump var="#ARGUMENTS.catchObject#" label="Exception dont le traitement a échouée"><br>
					</cfoutput>
				</cfsavecontent>
				
				<cftry>
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(13)>
					<cfset mLog.setIDMNT_ID(29)>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfdump var="#cfcatch#">						
					</cfmail>
				</cfcatch>
				</cftry>
				
				
				<!---
				<cfmail attributecollection="#getMailProperties().ERROR#" subject="#exceptionSubject#">
					Le traitement d'une exception capturée par l'API a échouée avec l'erreur suivante :<br><br>  
					<cfoutput>
						- Stockage de l'exception : #persistantError()#<br>
						- Code d'erreur : #CFCATCH.ERRORCODE#<br>
						- Message : #CFCATCH.MESSAGE#<br>
						- Detail : #CFCATCH.DETAIL#<br><br>
						<b><u>Contexte Web :</u></b><br>
						BackOffice : #CGI.SERVER_NAME#<br>
						Remote Host : #CGI.REMOTE_HOST# (#CGI.REMOTE_ADDR#) [#CGI.HTTP_USER_AGENT#]<br>
						Page : #CGI.SCRIPT_NAME#<br>
						Méthode HTTP : #CGI.REQUEST_METHOD#<br><br>
						<cfdump var="#CFCATCH.TAGCONTEXT#" label="Contexte d'exécution"><br>
						<cfdump var="#ARGUMENTS.catchObject#" label="Exception dont le traitement a échouée"><br>
					</cfoutput>
				</cfmail>
				--->
				
				
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- TODO : Remplacer la valeur de la variable locale APP une fois les tests terminés --->
	<cffunction access="private" name="storeException" returntype="void" hint="Sauvegarde l'exception dans la table de log utilisée par ConsoView (LOG_REPORT)
	Utilise une source de données appelée ROCOFFRE. Le LOGIN_LOG utilisé est toujours it@consotel.fr (Groupe : CONSOTEL).
	La structure catchInfos doit contenir les clés suivantes : Les valeurs sont tronquées en fonction du nombre max de caractères autorisé
		- CLASS (CLASSE_LOG : 200 caractères max) : Infos relative à la classe concernée (Car déjà renseignée dans CFCATCH.TAGCONTEXT)
		- METHOD (METHODE_LOG : 200 caractères max) : Infos relatives à l'opération effectuée ou la méthode appelée (Car déjà renseignée dans CFCATCH.TAGCONTEXT)
		- DESC (DESCRIPTION_LOG : 1500 caractères max) : Description ou détail du log
		- EVENT (EVENT_LOG : 50 caractères max) : Infos relative à l'évènement">
		<cfargument name="catchInfos" type="Struct" required="true" hint="Structure contenant les colonnes à stocker">
		<!--- Infos concernant l'exception  --->
		<cfset var INFOS=ARGUMENTS["catchInfos"]>
		<!--- Nommage de l'application (APP_LOG : 100 caractères max) --->
		<cfset var APP_NAME="Test API Reporting">
		<cfset var APP=LEFT(CGI.SERVER_NAME,(100 - LEN(APP_NAME)) - 3) & " : " & APP_NAME>
		<!--- Code d'erreur correspondant dans LOG_ERREUR (CODE_ERREUR : 0 pour une opération en succès) --->
		<cfset var ERR_CODE=1>
		<!--- Login utilisé pour le log (LOGIN_LOG : 100 caractères max, GROUPE : CONSOTEL) --->
		<cfset var emailLogin="it@consotel.fr">
		<!--- Source de données utilisée : ROCOFFRE --->
		<cfset var DSN="ROCOFFRE">
		<!--- Stockage de l'exception --->
		<cftry>
			<cfquery name="qInsertLogEvent" datasource="#DSN#" >
				INSERT INTO LOG_REPORT(DATE_LOG, APP_LOG, CODE_ERREUR, EVENT_LOG, IPADDR, LOGIN_LOG, CLASSE_LOG, METHODE_LOG, DESCRIPTION_LOG)
				VALUES (
					SYSDATE, '#APP#', #ERR_CODE#, '#LEFT(INFOS.EVENT,50)#', '0.0.0.0', '#emailLogin#',
					'#LEFT(INFOS.CLASS,200)#','#LEFT(INFOS.METHOD,200)#','#LEFT(INFOS.DESC,1500)#'
				)
			</cfquery>
			<cfset logInfo("Storing Exception : COMPLETED")>
			<cfcatch type="any">
				<!--- Diffusion d'une autre notification d'exception notifiant la précédente : N'est pas stockée car n'est pas directement liée à l'API (Pas d'appel récursif non plus) --->
				<cfset var errorMsg="Exception Storage FAILED : " & CFCATCH.MESSAGE>
				<cfset var exceptionSubject="Echec du stockage d'une exception : " & CFCATCH.Message>
				<cfset logError(errorMsg)>
				
				
				
				<cfsavecontent variable="body">
				  Le stockage d'une exception capturée par l'API a échouée avec l'erreur suivante :<br><br>  
					<cfoutput>
						- Stockage de l'exception : #persistantError()#<br>
						- Code d'erreur : #CFCATCH.ERRORCODE#<br>
						- Message : #CFCATCH.MESSAGE#<br>
						- Detail : #CFCATCH.DETAIL#<br><br>
						<cfdump var="#CFCATCH.TAGCONTEXT#" label="Contexte d'exécution"><br>
						<cfdump var="#ARGUMENTS.catchInfos#" label="Infos concernant l'exception à stocker"><br>
						<b><u>Contexte Web :</u></b><br>
						BackOffice : #CGI.SERVER_NAME#<br>
						Remote Host : #CGI.REMOTE_HOST# (#CGI.REMOTE_ADDR#) [#CGI.HTTP_USER_AGENT#]<br>
						Page : #CGI.SCRIPT_NAME#<br>
						Méthode HTTP : #CGI.REQUEST_METHOD#
					</cfoutput>
				</cfsavecontent>
				
				<cftry>
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(13)>
					<cfset mLog.setIDMNT_ID(29)>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfdump var="#cfcatch#">						
					</cfmail>
				</cfcatch>
				</cftry>
				
				
				<!---
				<cfmail attributecollection="#getMailProperties().ERROR#" subject="#exceptionSubject#">
					Le stockage d'une exception capturée par l'API a échouée avec l'erreur suivante :<br><br>  
					<cfoutput>
						- Stockage de l'exception : #persistantError()#<br>
						- Code d'erreur : #CFCATCH.ERRORCODE#<br>
						- Message : #CFCATCH.MESSAGE#<br>
						- Detail : #CFCATCH.DETAIL#<br><br>
						<cfdump var="#CFCATCH.TAGCONTEXT#" label="Contexte d'exécution"><br>
						<cfdump var="#ARGUMENTS.catchInfos#" label="Infos concernant l'exception à stocker"><br>
						<b><u>Contexte Web :</u></b><br>
						BackOffice : #CGI.SERVER_NAME#<br>
						Remote Host : #CGI.REMOTE_HOST# (#CGI.REMOTE_ADDR#) [#CGI.HTTP_USER_AGENT#]<br>
						Page : #CGI.SCRIPT_NAME#<br>
						Méthode HTTP : #CGI.REQUEST_METHOD#
					</cfoutput>
				</cfmail>
				--->				
				
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- Méthodes de logging et de notifications mails de l'API --->
	<cffunction access="private" name="getNotificationTo" returntype="String" hint="Destinataire principal des notifications mails de l'API">
		<cfreturn "monitoring@saaswedo.com">
	</cffunction>
	
	<cffunction access="private" name="getMailProperties" returntype="Struct"
	hint="Retourne une structure contenant les propriétés MAIL utilisées par défaut par l'API et où chaque clé est associée à une collection d'attributs CFMAIL
	- NOTIFICATION : Collection d'attributs CFMAIL utilisés pour notifier un message d'information
	- WARNING : Collection d'attributs CFMAIL utilisés pour notifier un message d'avertissement
	- ERROR : Collection d'attributs CFMAIL utilisés pour notifier d'une erreur">
		<cfreturn VARIABLES.MAIL>
	</cffunction>

	<cffunction access="private" name="setMailProperties" returntype="void" hint="Défini les propriétés relatives aux notifications mails de l'API">
		<cfset var DQOT="""">
		<!--- Propriétés Mail : Notifications de l'API et Cas d'erreurs --->
		<cfset VARIABLES.MAIL = {
			NOTIFICATION={FROM="#DQOT#Notification API Reporting#DQOT# <api@" & getServiceProperties().DOMAIN & ">", CHARSET="utf-8",
				TO=getNotificationTo(), REPLYTO="", TYPE="html"
			},
			WARNING={FROM="#DQOT#Avertissement API Reporting#DQOT# <warning@" & getServiceProperties().DOMAIN & ">", CHARSET="utf-8",
				TO=getNotificationTo(), REPLYTO="", TYPE="html"
			},
			ERROR={FROM="#DQOT#Erreur API Reporting#DQOT# <error@" & getServiceProperties().DOMAIN & ">", CHARSET="utf-8",
				TO=getNotificationTo(), REPLYTO="", TYPE="html"
			}
		}>
	</cffunction>
	
	<cffunction access="private" name="getServiceProperties" returntype="Struct" hint="Retourne les propriétés du service de reporting">
		<cfreturn VARIABLES.SERVICE>
	</cffunction>
	
	<cffunction access="private" name="setServiceProperties" returntype="void" hint="Défini les propriétés minimales qui permettent le logging et les notifications mails">
		<cfset VARIABLES.SERVICE={LOG="REPORTING", DOMAIN="consotel.fr", LOCALIZATION="fr_FR", DEBUG_LEVEL=isDebugMode()}>
		<!--- Défini les propriétés relatives aux notifications mails de l'API --->
		<cfset setMailProperties()>
	</cffunction>
	
	<cffunction access="private" name="setErrorProperties" returntype="void" hint="Défini les propriétés utilisés pour les erreurs et exceptions">
		<!--- Propriétés des erreurs et exceptions --->
		<cfset VARIABLES.ERROR={TYPE="Custom", ERRORCODE="ILLEGAL_OPERATION"}>
	</cffunction>
	
	<cffunction access="private" name="getErrorProperties" returntype="Struct" hint="Retourne les propriétés de gestion des erreurs et exceptions (Utilisées avec CFTHROW)">
		<cfreturn VARIABLES.ERROR>
	</cffunction>
</cfcomponent>