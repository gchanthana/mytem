<cfcomponent hint="Logging d evenement dans la table mnt_detail" output="false">
	
	<!---========================= Paramètres du mail ================================--->
	<cfset errorMessage = {}>
	<cfset errorMessage.from = "MLogger <monitoring@saaswedo.com>">
	<cfset errorMessage.to = "monitoring@saaswedo.com">
	<cfset errorMessage.bcc = "monitoring@saaswedo.com">
	<cfset errorMessage.subject = "[EVT-9999] Erreur dans l enregistrement d'un log de monitoring">
	<cfset errorMessage.server = "mail-cv.consotel.fr">
	<cfset errorMessage.port = "25">
	<cfset errorMessage.type = 	type="text/html">
	<!--- =========================================================================== --->
	
	
	<cffunction name="logIt" hint="log un evenement de type Mlog" access="remote" output="false" returntype="void">
		<cfargument name="mLog" type="MLog" hint="l event a logger" required="true" />
		<cfset result = 0>
		<cfset t_name = createUuid()>
		<cfthread action="run" name="#t_name#" pmlog="#mLog#" perrormessage="#errorMessage#">
		
			<cftry>
				
		    	<cfset idMnt_id = pmlog.getIDMNT_ID()>
		    	<cfset idMnt_cat = pmlog.getIDMNT_CAT()>
 		    	<cfset subjectid = left(trim(pmlog.getSUBJECTID()),199)>
		    	<cfset body = pmlog.getBODY()>
				
		    	<cfset bool_racine_null = true>
				<cfset idracine = pmLog.getIDRACINE()>	    	
				<cfif idracine gt 0>
					<cfset bool_racine_null = false>
				</cfif>
				
		    	<cfquery name="qInsert" dataSource="ROCOFFRE">
		       		INSERT into mnt_detail(IDMNT_ID,IDMNT_CAT,BODY,SUBJECTID,IDRACINE)
		       		values
		       		(<cfqueryparam cfsqltype="cf_sql_integer" value="#idMnt_id#" >,
		       		 <cfqueryparam cfsqltype="cf_sql_integer" value="#idMnt_cat#">,
		       		 <cfqueryparam cfsqltype="cf_sql_clob" value="#body#">,
		       		 <cfqueryparam cfsqltype="cf_sql_varchar" value="#subjectid#">,
					 <cfqueryparam cfsqltype="cf_sql_integer" value="#idracine#" null="#bool_racine_null#">)		       		
		       	</cfquery>
				
				
			<cfcatch type="any">
				
			  	<cfmail attributecollection="#errorMessage#">				  			
					<cfdump var="#cfcatch#">											
				</cfmail>
				
			</cfcatch>
			
			</cftry>
			
		</cfthread>
		

	</cffunction>
	
	
</cfcomponent>