package univers.inventaire.lignesetservices.views
{
	import mx.containers.VBox;
	import univers.inventaire.lignesetservices.applicatif.GestionTechniqueLignesApp;
	import mx.events.FlexEvent;
	import flash.display.DisplayObject;
	import flash.errors.IllegalOperationError;
	import mx.states.AddChild;
	
	
	[Bindable]
	public class OngletCablageImpl extends BaseView
	{
		
		
		public static const LA : String = "LA";
		public static const MOBILE : String = "MOB";
		public static const GSM : String = "GSM";
		
		public static const T0 : String = "T0";
		public static const T2 : String = "T2";
		public static const LS : String = "LS";
		public static const SER : String = "SER";
		public static const AUT : String = "AUT";
		
		
		public function OngletCablageImpl() 
		{	
			super();
		}
		
		private var onglet : BaseView;
		
		
		public function configurerOnglet():void{
			if (onglet != null){
				removeChild(onglet.displayObject);
				onglet = null;
				
			}
			onglet = createFicheCablage(gestionTechnique.ligne.TYPE_FICHELIGNE);
			onglet.modeEcriture = modeEcriture;
			onglet.gestionTechnique = gestionTechnique;
			addChild(onglet.displayObject);
		}
		
		override protected function commitProperties():void{
			super.commitProperties();
		}	
		
		//================ HANDLERS =====================================
		
		//===============================================================
		
		
			
		
		private function createFicheCablage(type : String):BaseView{
			switch(type){
				
				case LA : return new OngletCablageLAView();
				case MOBILE : case GSM :return new OngletCablageLAView();
				case T0 : return new OngletCablageT0View();
				case T2 : return new OngletCablageT2View(); 
				case LS : return new OngletCablageLSView();
				case AUT : return new OngletNonDisponibleView();
				case SER : return new OngletNonDisponibleView();
				default : return new OngletNonDisponibleView();
			}	
		}
		
		
	}
}