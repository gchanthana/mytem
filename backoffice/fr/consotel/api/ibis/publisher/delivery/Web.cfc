<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.Web" extends="fr.consotel.api.ibis.publisher.delivery.Delivery"
hint="Stratégie identifiée par le type Web permettant soit de diffuser en mode Web le contenu d'un reporting généré par une autre stratégie
Le contenu du reporting est alors soit affiché dans une page web courante soit retourné en tant qu'attachement dans la page web courante
Ces actions sont effectuées par la méthode handleReportingEvent() et l'évènement qui lui est fourni doit etre de type IDeliveryEvent
Le reporting qui est diffusé par cette stratégie doit etre exécuté avec une stratégie utilisant un mode synchrone pour accéder à la page web courante
Un exemple de stratégie utilisant un mode synchrone est : fr.consotel.api.ibis.publisher.delivery.Sync
Les propriétés suivantes doivent etre retournée par la méthode getDeliveryProperties() de la stratégie utilisée pour l'exécution du reporting : 
- ATTACHMENT : Nom à donner à l'attachement web ou une chaine vide s'il est à afficher dans la page web courante
Cette stratégie ne peut pas et ne doit pas etre utilisée pour l'exécution du reporting. Les méthodes correspondantes lèvent une exception">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="handleReportingEvent" returntype="void" hint="Effectue la diffusion web du contenu du reporting
	Récupère la valeur de la propriété ATTACHMENT à partir de l'évènement event et procède à sa diffusion en fonction de sa valeur
	Si ATTACHMENT est une chaine vide alors :
	- Si event.isTextContent() vaut TRUE le contenu est affiché dans la page web courante
	- Sinon un message est affiché dans la page web courante et notifie que le type du contenu ne permet pas de l'afficher
	Si ATTACHMENT n'est pas une chaine vide : Le contenu est retourné en tant qu'attachement web avec la valeur de ATTACHMENT comme nom
	Une exception est levée si event n'est pas de type fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true" hint="Evènement de type IDeliveryEvent">
		<cfset var deliveryEvent=ARGUMENTS["event"]>
		<cfif NOT isInstanceOf(ARGUMENTS["event"],"fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent")>
			<cfthrow attributecollection="#getErrorProperties()#"
			message="L'évènement doit etre de type : fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent pour Web"
			detail="Type de l'évènement : #getMetadata(deliveryEvent).NAME#">
		<cfelse>
			<!--- Récupération de la valeur de la propriété ATTACHMENT à partir de l'évènement source IReportingEvent --->
			<cfset var reportingEvent=deliveryEvent.getReportingEvent()>
			<cfset var deliveryProperties=reportingEvent.getDelivery()>
			<!--- Nom par défaut de l'attachement Web --->
			<cfset var webAttachment="">
			<cfif structKeyExists(deliveryProperties,"ATTACHMENT")>
				<cfset var attachmentArrayProperty=deliveryProperties["ATTACHMENT"]>
				<cfif isValid("Array",attachmentArrayProperty) AND (arraylen(attachmentArrayProperty) EQ 1)>
					<cfset webAttachment=attachmentArrayProperty[1]>
				</cfif> 
			</cfif>
			<!--- Si ATTACHMENT est une chaine vide  : Vérifie le type du contenu --->
			<cfif TRIM(webAttachment) EQ "">
				<!--- Si le contenu est de type texte : Il est affiché dans la page web courante --->
				<cfif deliveryEvent.isTextContent()>
					<cfset diffuseAsWebContent(deliveryEvent)>
				<!--- Sinon un message d'erreur est affiché notifiant que le type du contenu ne permet pas de l'afficher --->
				<cfelse>
					<cfset onInvalidContentType(deliveryEvent)>
				</cfif>
				</html>
			<!--- Si ATTACHMENT n'est pas une chaine vide : Le contenu est retourné en tant qu'attachement web --->
			<cfelse>
				<cfset diffuseAsWebAttachment(deliveryEvent)>
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getReportingParameters" returntype="Struct" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>

	<cffunction access="public" name="getDeliveryProperties" returntype="Struct" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>
	
	<cffunction access="public" name="getDeliveryRequest" returntype="Struct" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>
	
	<cffunction access="public" name="handleJobId" returntype="void" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfargument name="jobId" type="Numeric"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.delivery.Web --->
	<cffunction access="private" name="diffuseAsWebContent" returntype="void"
	hint="Méthode appelée lorsque le contenu du reporting est à affiché dans la page web courante
	Cette implémentation affiche le contenu du reporting dans la page web courante">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent" required="true" hint="Provient de handleReportingEvent()">
		<cfset var deliveryEvent=ARGUMENTS["event"]>
		<html>
			<head>
				<style type="text/css">
				body {background-color: ##FFFFFF;}
				</style>
			</head>
			<body>
			<cfoutput>
				#deliveryEvent.getContent("","")#<br>
			</cfoutput>
			</body>
		</html>
	</cffunction>
	
	<cffunction access="private" name="diffuseAsWebAttachment" returntype="void"
	hint="Méthode appelée lorsque le contenu du reporting est à retourné en tant qu'attachement web dans la page web courante
	Cette implémentation retourne le contenu du reporting en tant qu'attachement web dans la page web courante">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent" required="true" hint="Provient de handleReportingEvent()">
		<cfset logInfo("WEB ATTACHMENT")>
		<cfset var deliveryEvent=ARGUMENTS["event"]>
		<cfset var webContent=deliveryEvent.getContent("","")>
		<!--- Récupération de la valeur de la propriété ATTACHMENT à partir de l'évènement source IReportingEvent --->
		<cfset var reportingEvent=deliveryEvent.getReportingEvent()>
		<cfset var deliveryProperties=reportingEvent.getDelivery()>
		<cfset var webAttachment="">
		<cfif structKeyExists(deliveryProperties,"ATTACHMENT")>
			<cfset var attachmentArrayProperty=deliveryProperties["ATTACHMENT"]>
			<cfif isValid("Array",attachmentArrayProperty) AND (arraylen(attachmentArrayProperty) EQ 1)>
				<cfset webAttachment=attachmentArrayProperty[1]>
			</cfif> 
		</cfif>
		<cfheader name="Content-Disposition" value="attachment;filename=#webAttachment#" charset="utf-8">
		<cfcontent type="#deliveryEvent.getContentType()#; charset=utf-8" variable="#webContent#">
		<html>
			<head>
				<style type="text/css">
				body {background-color: ##FFFFFF;}
				</style>
			</head>
			<body></body>
		</html>
	</cffunction>
	
	<cffunction access="private" name="onInvalidContentType" returntype="void"
	hint="Méthode appelée lorsque la propriété ATTACHMENT est une chaine vide et que event.isTextContent() vaut FALSE
	Cette implémentation affiche un message notifiant que le type du contenu du reporting ne permet pas de l'afficher dans la page web courante">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent" required="true" hint="Provient de handleReportingEvent()">
		<html>
			<head>
				<style type="text/css">
				body {background-color: ##FFFFFF;}
				</style>
			</head>
			<body>
			<cfoutput>
				<br><center><b>Erreur : Le contenu n'est pas de type texte et ne peut pas etre affich&eacute; dans la page web courante</b></center>
			</cfoutput>
			</body>
		</html>
	</cffunction>
</cfcomponent>