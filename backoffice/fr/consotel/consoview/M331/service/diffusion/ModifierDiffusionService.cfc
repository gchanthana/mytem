<cfcomponent output="false">

	<cffunction access="remote" output="false" name="modifierDiffusion" returntype="numeric">
		
		<cfargument name="idRapport" type="numeric" required="true">		
		<cfargument name="idEvent" type="numeric" required="true">
		<cfargument name="idOrga" type="numeric" required="true">
		<cfargument name="idProfil" type="numeric" required="true">
		<cfargument name="nbTop" type="numeric" required="true">
		<cfargument name="finPeriode" type="numeric" required="true">
		<cfargument name="periodeEtude" type="numeric" required="true">
		<cfargument name="jourLancement" type="numeric" required="true">
		<cfargument name="destinatairesCaches" type="string" required="true">
		<cfargument name="objetMail" type="string" required="true">
		<cfargument name="nomAttachementFile" type="string" required="true">
		<cfargument name="corpsMail" type="String" required="true">
		<cfargument name="template" type="string" required="true">
		<cfargument name="format" type="numeric" required="true">
		<cfargument name="noContrat" type="numeric" required="true">
		
		<cfset periodicite=1>
		<cfset recurrent=1>
		
		<cfset isNullIdProfil=false>
		<cfif idProfil eq 0>			
			<cfset isNullIdProfil=true>
			<cfelse>
				<cfset isNullIdProfil=false>
		</cfif>
		
        <cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.modifierDiffusion_V2">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRapport#" variable="p_idRapport">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idEvent#" variable="p_idEvent">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idOrga#" variable="p_idOrga">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" variable="p_idProfil" null="#isNullIdProfil#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#nbTop#" variable="p_nbTop">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#finPeriode#" variable="p_finPeriode">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#periodeEtude#" variable="p_periodeEtude">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#jourLancement#" variable="p_jourLancement">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#destinatairesCaches#" variable="p_destinatairesCaches">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#objetMail#" variable="p_objetMail">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#nomAttachementFile#" variable="p_nomAttachementFile">
			<cfprocparam type="in" cfsqltype="CF_SQL_CLOB" value="#corpsMail#" variable="p_corpsMail">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#template#" variable="p_template">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#periodicite#" variable="p_periodicite">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#format#" variable="p_format">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#recurrent#" variable="p_recurrent">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#noContrat#" variable="p_no_contrat">
	
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
			
		<cfset resultat=#p_retour#>
		<cfreturn resultat>	
		
     </cffunction>
	
</cfcomponent>