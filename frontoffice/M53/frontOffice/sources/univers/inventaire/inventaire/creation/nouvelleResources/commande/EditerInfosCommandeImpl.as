package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import composants.controls.TextInputLabeled;
	import composants.mail.MailBoxIHM;
	import composants.mail.gabarits.InfosObject;
	import composants.util.CvDateChooser;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.TextArea;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	[Bindable]
	public class EditerInfosCommandeImpl extends Canvas
	{
		//---Composants graphiques--------------------------------------------------
		public var txtLibelle : TextInputLabeled;
		
		public var txtRefClient : TextInputLabeled;
		
		public var txtRefOperateur : TextInputLabeled;
		
		public var txtCommentaires : TextArea;
					
		public var txtOperateur : TextInputLabeled;
		
		public var txtLivraisonPrevue : TextInputLabeled;
		
		public var txtCible : TextInputLabeled;
		
		public var imgChangeCibleOrga : Image;
		
		public var imgChangeContact : Image;
		
		public var imgMail : Image;
		
		public var btUpdate : Button;
		
		public var popUp : TitleWindow;	
		
		public var dcDateLivraisonPrevue : CvDateChooser;
		
		private var _cpMail : MailBoxIHM		
		//----------------------------------------------------------------
		
		private var _infosMail : InfosObject;
			
		private var _modeEcriture : Boolean = true;
		public function get modeEcriture():Boolean{
			return _modeEcriture;
		}
		public function set modeEcriture(mode : Boolean):void{
			_modeEcriture = mode;
		}
		
		
		private var _commande : Commande;
		public function set commande(value : Commande):void{
			_commande = value;
			
			/* 
			
			if (_commande != null){
				if(_commande.societeID > 0){
					distributeur = new Societe(_commande.societeID);
					distributeur.addEventListener(Societe.CREATION_COMPLETE,distributeurCreationCompleteHandler);
				}
				
				if(_commande.contactID >0){
					contact = new Contact(_commande.contactID);
				}
			} */
		}
		public function get commande():Commande{
			return _commande
		}
		
		private var _panier : ElementCommande;
		public function set panier(value : ElementCommande):void{
			_panier = value
		}
		public function get panier():ElementCommande{
			return _panier
		}
		
		/* private var _contact : Contact;
		public function get contact():Contact{
			return _contact
		}
		public function set contact(value : Contact):void{
			_contact = value;
		}
		
		private var _distibuteur : Societe;
		public function get distributeur():Societe{
			return _distibuteur
		}
		public function set distributeur(value : Societe):void{
			_distibuteur = value
		}
		private var _agence : Societe;
		public function get agence():Societe{
			return _agence
		}
		public function set agence(value : Societe):void{
			_agence = value
		}		 */
		//
		public function EditerInfosCommandeImpl()
		{
			super();
			
			
		}
		
		//---HANDLERS --------------------------------------------------
		protected function contactCreationCompleteHandler(event : Event):void{
			
		}
		protected function distributeurCreationCompleteHandler(event : Event):void{
			/* if (distributeur.societeParenteID > 0){
				agence = new Societe(distributeur.societeParenteID)
			} */
		}
		protected function agenceCreationCompleteHandler(event : Event):void{
			
		}
		
		protected function imgChangeCibleOrgaClickHandler(me : MouseEvent):void{
			
		}
		
		protected function imgChangeContactClickHandler(me : MouseEvent):void{
		
		}
		
		protected function imgMailClickHandler(event : MouseEvent):void{
			/* if (_cpMail != null){
				if(_cpMail.hasEventListener(MailBoxImpl.MAIL_ENVOYE))_cpMail.removeEventListener(MailBoxImpl.MAIL_ENVOYE,cpMailBoxMailEnvoyeHandler);
				 _cpMail = null;
			}
			if (_infosMail != null) _infosMail = null;
			
			_infosMail = new InfosObject();
			_infosMail.MAIL_EXPEDITEUR = CvAccessManager.getSession().USER.EMAIL;
			_infosMail.NOM_EXPEDITEUR = CvAccessManager.getSession().USER.NOM;
			_infosMail.PRENOM_EXPEDITEUR = CvAccessManager.getSession().USER.PRENOM;
			_infosMail.commande = commande;
			_infosMail.panierCommande = panier;
			
			
			
						
			_cpMail = new MailBoxIHM();
			_cpMail.contact.contactID = commande.contactID;
			
			if(contact != null){
				_cpMail.initMail("WorkFlow","Commande",contact.email);
			}else{
				_cpMail.initMail("WorkFlow","Commande","----");
			}
			
			_cpMail.configRteMessage(_infosMail);
			
			_cpMail.addEventListener(MailBoxImpl.MAIL_ENVOYE,cpMailBoxMailEnvoyeHandler);
			
			PopUpManager.addPopUp(_cpMail,this,true);					
			PopUpManager.centerPopUp(_cpMail); */	
			
		}
		
		protected function cpMailBoxMailEnvoyeHandler(event : Event):void{
			/* commande.contactID = _cpMail.contact.contactID;
			commande.societeID = _cpMail.contact.societeID;
			
			contact.updateContact();
			distributeur.updateSociete();
			 */
		}
		
		protected function btUpdateClickHandler(me : MouseEvent):void{
			if(txtLibelle.text.length > 0){
				
				commande.libelle = txtLibelle.text;
				commande.commentaire = txtCommentaires.text;
				commande.refClient = txtRefClient.text;
				commande.refOperateur = txtRefOperateur.text;
				//commande.dateLivraisonPrevue = dcDateLivraisonPrevue.text;				
				commande.updateCommande();	
				
			}else{
				Alert.okLabel = "Fermer";
				Alert.buttonWidth = 100;
				Alert.show("Le libellé de la commande est obligatoire");
			}
		}
		
		protected function cpContactsChangeHandler(ev : Event):void{
			/* commande.contactID = ;
			commande.societeID = ;
			 */
		}
		
		protected function cpOrganisationChangeHandler(ev : Event):void{
			//commande.idGroupeCible = ;
			//commande.groupeCibleLibelle = ;
		}
	}
}