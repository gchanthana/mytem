package service
{	
	
	import mx.rpc.events.ResultEvent;

	public class PerimetreE0Handler
	{
		private var _model:PerimetreE0Model;
				
		
		public function PerimetreE0Handler(model:PerimetreE0Model)
		{
			_model = model;
		}
		
		internal function getPerimetreInfosResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.udpatePerimetreInfos(evt.result);
			}
		}
		
		
	}
}