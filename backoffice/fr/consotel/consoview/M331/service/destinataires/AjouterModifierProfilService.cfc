<cfcomponent output="false">
	
	
	<cffunction access="remote" name="ajouterProfil" hint="ajouter un profil" returntype="Numeric" output="true">
		
		<cfargument name="profil" type="struct" required="true">
		
		<cfset libelle=trim(ARGUMENTS["profil"]["nomProfil"])>
		<cfset codeInterne=trim(ARGUMENTS["profil"]["codeInterne"])>
		<cfset commentaire=trim(ARGUMENTS["profil"]["commentaire"])>
		<cfset idracine   = SESSION.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.addProfil">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idracine#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>		
	</cffunction>
	
	<cffunction name="modifierProfil" access="remote" returntype="Numeric" output="true">
		
		<cfargument name="profil" type="struct" required="true">
		
		<cfset idProfil=ARGUMENTS["profil"]["idProfil"]>
		<cfset libelle=trim(ARGUMENTS["profil"]["nomProfil"])>
		<cfset codeInterne=trim(ARGUMENTS["profil"]["codeInterne"])>
		<cfset commentaire=trim(ARGUMENTS["profil"]["commentaire"])>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.updateProfil">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterne#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaire#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

		
	<cffunction name="supprimerProfil" access="remote" returntype="numeric" hint="supprimer un profil">
		
		<cfargument name="idProfil" type="numeric" required="true">	 
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.delProfil">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idProfil#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>	
		<cfreturn p_retour>
		
	</cffunction>
	
		<!--- permet de supprimer un profil definitivement meme si sont associés à des destinataires --->
	<cffunction access="remote" name="supprimerProfilCascade"  returntype="numeric">
		
		<cfargument name="idProfil" type="numeric" required="true">
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.DELPROFILCASCADE">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idProfil#" variable="p_idprofil" >
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
		
	</cffunction>
	
		
</cfcomponent>