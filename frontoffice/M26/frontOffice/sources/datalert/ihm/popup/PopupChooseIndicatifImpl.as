package datalert.ihm.popup
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	public class PopupChooseIndicatifImpl extends TitleWindow
	{
		//--------------- VARIABLES ----------------//
		[Bindable] 
		public var rbgIndicatif			:RadioButtonGroup;
		[Bindable] 
		public var itemCurrentLabel		:Label;
		public var dgChooseIndicatif	:DataGrid;
		public var tiFiltre				:TextInput;
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		[Bindable]
		public var btnValidEnabled		:Boolean = false;
		
		private var _itemCurrent		:Object = null;
		private var _selectedIndex		:Number;
		private var _listeIndicatifs 	:ArrayCollection;
		private var _itemSelectedFirst 	: Object = null;
		
		//--------------- METHODES ----------------//
		
		public function PopupChooseIndicatifImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		private function initData():void
		{
			listeIndicatifs.filterFunction = filtrerIndicatifs;
			listeIndicatifs.refresh();
			
			
			// initialisation de l'affichage du label du pool
			afficherInitialIndicatifLabel();
			initCurseur();
		}
		
		private function initCurseur():void
		{
			callLater(tiFiltre.setFocus);
		}
		
		// initialisation de l'affichage du label du pool
		private function afficherInitialIndicatifLabel():void
		{
			if(listeIndicatifs != null)
			{
				var longIndicatifs:int = listeIndicatifs.length;
				for (var i:int = 0; i < longIndicatifs; i++) 
				{
					if(listeIndicatifs[i].SELECTED)
					{
						itemCurrent = listeIndicatifs[i];
						_itemSelectedFirst = listeIndicatifs[i]; 
						break;
					}
				}
			}
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		private function initListeners():void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseIndicatifHandler);
			addEventListener(KeyboardEvent.KEY_DOWN,validerChooseIndicatifHandler2);
			btnAnnuler.addEventListener(MouseEvent.CLICK,onCloseHandler);
			dgChooseIndicatif.addEventListener(ListEvent.CHANGE,currentItemHandler);
			
		}
		
		/* */
		protected function currentItemHandler(le:ListEvent):void
		{
			_itemCurrent = le.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listeIndicatifs.length)
			{
				if(_itemCurrent!=listeIndicatifs[i])
					listeIndicatifs[i].SELECTED = false;
				else
				{
					listeIndicatifs[i].SELECTED = true;
					_selectedIndex = i;
				}
				i++;
			}
			dgChooseIndicatif.invalidateList();//A presque le même effet que dgChooseIndicatif.refresh(); => force la sélection du nouvel item cliqué, mais sans effectuer de saut en mettant l'élément choisi tout en haut de la vue cependant.
			itemCurrentLabel.text = getItemCurrentLabel();
			btnValidEnabled = true;
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeIndicatifs.filterFunction = filtrerIndicatifs;
			listeIndicatifs.refresh();
		}
		
		/* systeme de filtre */
		private function filtrerIndicatifs(item:Object):Boolean
		{
			rbgIndicatif.selection = rbgIndicatif.getRadioButtonAt(dgChooseIndicatif.selectedIndex + 1);
			if (item.label != null && (item.label as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		/**
		 * Retourne le libelle racine approprié peuplant la combobox :
		 *   libelle_racine ( nbCollectes collectes)
		 */
		public function labelIndicatifFunction(item:Object, column:DataGridColumn):String
		{
			return item.label;
		}
		
		protected function onCloseHandler(event:Event):void
		{
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			this.initPopUp();
			PopUpManager.removePopUp(this);
		}
		
		// initialise la popup vers le choix du pool initial
		private function initPopUp():void
		{
			for (var i:int = 0; i < listeIndicatifs.length; i++) 
			{
				listeIndicatifs[i].SELECTED = false;
				if(listeIndicatifs[i].label == _itemSelectedFirst.label)
					listeIndicatifs[i].SELECTED = true;
			}
			listeIndicatifs.refresh();
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		/* au click sur valider */
		private function validerChooseIndicatifHandler(evt:MouseEvent):void
		{
			validerChoix();
		}
		
		private function validerChoix():void
		{
			dispatchEvent(new Event('VALID_CHOIX_INDICATIF',true));
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			onCloseHandler(null);
			
		}
		
		private function validerChooseIndicatifHandler2(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
			{
				validerChoix();
			}
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		
		[Bindable]
		public function get listeIndicatifs():ArrayCollection
		{
			return _listeIndicatifs;
		}
		public function set listeIndicatifs(value:ArrayCollection):void
		{
			_listeIndicatifs = value;
		}
		
		public function get itemCurrent():Object
		{
			return _itemCurrent;
		}
		
		public function set itemCurrent(value:Object):void
		{
			_itemCurrent = value;
		}
		
		public function getItemCurrentLabel():String
		{
			if (_itemCurrent != null)
				return _itemCurrent.label;
			else
				return '';
		}
	}
}