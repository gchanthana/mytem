package univers.parametres.perimetres.rightSide.destinataireComponent
{
	import flash.events.Event;

	public class modifyProfilEvent extends Event
	{
		public var id:Number;
		public var libelle:String;
		public var codeInterne:String;
		public var commentaire:String;
		
		public function modifyProfilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}