package rapportPilotage.service.chercherselector
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import rapportPilotage.service.chercherselector.*;

	public class RechercherSelectorService
	{
		
		private var _model:RechercherSelectorModel;
		public var handler:RechercherSelectorHandler;
	
		public function RechercherSelectorService()
		{
			this._model = new RechercherSelectorModel;
			this.handler = new RechercherSelectorHandler(model);
		}		
		
		public function get model():RechercherSelectorModel
		{
			return this._model;
		}
		
		public function getSelector(idRapport:int):void
		{		
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M332.service.RechercherSelectorService","getSelector",handler.getSelectorResultHandler); 
		
			RemoteObjectUtil.callService(op,idRapport);	// appel au service coldfusion 
		}
		
		
	}
}