<cfcomponent output="false" alias="fr.consotel.consoprod.intranet.cataloguepublic.GroupeProduitControle" extends="AccessObject">
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="id" type="numeric" default="0">
	<cfproperty name="libelle" type="string" default="">
	<cfproperty name="segment" type="string" default="">
	<cfproperty name="operateurId" type="numeric" default="0">
	<cfproperty name="operateurLibelle" type="string" default="">
	<cfproperty name="poids" type="numeric" default="0">
	<cfproperty name="pourControle" type="boolean" default="false">
	<cfproperty name="prixUnitaire" type="numeric" default="0">
	<cfproperty name="prixRemise" type="numeric" default="0">
	<cfproperty name="montant" type="numeric" default="0">
	<cfproperty name="remiseContrat" type="numeric" default="0">
	<cfproperty name="dateDebut" type="date" default="">
	<cfproperty name="dateFin" type="date" default="">
	<cfproperty name="nbVersions" type="numeric" default="0">
	<cfproperty name="type" type="string" default="">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.id = 0;
		variables.libelle = "";
		variables.segment = "";
		variables.operateurId = 0;
	 	variables.operateurLibelle = "";
		variables.pourControle = false;
		variables.poids = 0;
		variables.prixUnitaire = 0;
		variables.prixRemise = 0;
		variables.montant = 0;
		variables.remiseContrat = 0;
		variables.dateDebut = "";
		variables.dateFin = "";
		variables.nbVersions = 0;
		variables.type = "";
	</cfscript>

	<cffunction name="init" output="false" returntype="Produit">
		<cfreturn this>
	</cffunction>
	
	<cffunction name="getId" output="false" access="public" returntype="any">
		<cfreturn variables.Id>
	</cffunction>

	<cffunction name="setId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Id = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.Libelle>
	</cffunction>

	<cffunction name="setLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.Libelle = arguments.val>
	</cffunction>
	
	<cffunction name="getType" output="false" access="public" returntype="any">
		<cfreturn variables.type>
	</cffunction>

	<cffunction name="setType" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.type = arguments.val>
	</cffunction>
	
	<cffunction name="getSegment" output="false" access="public" returntype="any">
		<cfreturn variables.segment>
	</cffunction>

	<cffunction name="setSegment" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.segment = arguments.val>
	</cffunction>

	<cffunction name="getOperateurId" output="false" access="public" returntype="any">
		<cfreturn variables.OperateurId>
	</cffunction>

	<cffunction name="setOperateurId" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.OperateurId = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getOperateurLibelle" output="false" access="public" returntype="any">
		<cfreturn variables.OperateurLibelle>
	</cffunction>

	<cffunction name="setOperateurLibelle" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset variables.OperateurLibelle = arguments.val>
	</cffunction>
	
	<cffunction name="getPoids" output="false" access="public" returntype="any">
			<cfreturn variables.Poids>
	</cffunction>

	<cffunction name="setPoids" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.Poids = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
		
	<cffunction name="getPourControle" output="false" access="public" returntype="any">		
		<cfreturn variables.PourControle>
	</cffunction>

	<cffunction name="setPourControle" output="false" access="public" returntype="void">		
		<cfargument name="val" required="true">
		<cfif (IsBoolean(arguments.val)) AND (arguments.val EQ 0)>
			<cfset variables.PourControle = false>
		<cfelseif (IsBoolean(arguments.val)) AND (arguments.val EQ 1)>
			<cfset variables.PourControle = true>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid boolean"/>
		</cfif>
		
	</cffunction>
	
	<cffunction name="getPrixUnitaire" output="false" access="public" returntype="any">
		<cfreturn variables.PrixUnitaire>
	</cffunction>

	<cffunction name="setPrixUnitaire" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PrixUnitaire = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getPrixRemise" output="false" access="public" returntype="any">
		<cfreturn variables.PrixRemise>
	</cffunction>

	<cffunction name="setPrixRemise" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.PrixRemise = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	
	<cffunction name="getMontant" output="false" access="public" returntype="any">
		<cfreturn variables.montant>
	</cffunction>
	
	
	
	<cffunction name="setMontant" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.montant= arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	
	<cffunction name="getRemiseContrat" output="false" access="public" returntype="any">
		<cfreturn variables.RemiseContrat>
	</cffunction>

	<cffunction name="setRemiseContrat" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.RemiseContrat = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>
	
	<cffunction name="getNbVersion" output="false" access="public" returntype="any">
		<cfreturn variables.nbVersion>
	</cffunction>

	<cffunction name="setNbVersion" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.nbVersion = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateDebut" output="false" access="public" returntype="any">
		<cfreturn variables.DateDebut>
	</cffunction>

	<cffunction name="setDateDebut" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateDebut = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>

	<cffunction name="getDateFin" output="false" access="public" returntype="any">
		<cfreturn variables.DateFin>
	</cffunction>

	<cffunction name="setDateFin" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsDate(arguments.val)) OR (arguments.val EQ "")>
			<cfset variables.DateFin = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date"/>
		</cfif>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : tague le groupe de produit
		
		Param in 
		Param out
			Number 1 si ok sinon -1
	
	--->
	<cffunction name="taguer" access="public" output="false" returntype="numeric">
		<cfargument name="idRacine" required="true" type="numeric">
		 <!---
			 pkg_cv_grcl_facturation.taguer_grp_ctl( p_idgrp_ctl => :p_idgrp_ctl,
			                                         p_idracine => :p_idracine,
			                                         p_retour => :p_retour);
													
		 --->
		 <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation.taguer_grp_ctl">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGrpProduit" value="#this.getId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>		
		</cfstoredproc>		
		<cfreturn result>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : dï¿½tague le groupe de produit
		
		Param in 
		Param out
			Number 1 si ok sinon -1
	
	--->
	<cffunction name="detaguer" access="public" output="false" returntype="numeric">
		<cfargument name="idRacine" required="true" type="numeric">
		 <!---
			pkg_cv_grcl_facturation.detaguer_grp_ctl(		p_idgrp_ctl => :p_idgrp_ctl,
				                                            p_idracine => :p_idracine,
				                                            p_retour => :p_retour);
													
		 --->
		 <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation.detaguer_grp_ctl">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idGrpProduit" value="#this.getId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idRacine" value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="out" variable="result"/>		
		</cfstoredproc>		
		<cfreturn result>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : charge le produit
		
		Param in 
		Param out
			Produt
	
	--->
	<cffunction name="load" access="public" output="false" returntype="GroupeProduitControle">
		<cfset procedure = "">
		
		<cfif LCASE(getType()) eq "abo">
			<cfset procedure = "pkg_cv_grcl_facturation.liste_grp_ctl_abos">
		<cfelse>
			<cfset procedure = "pkg_cv_grcl_facturation.liste_grp_ctl_Consos">
		</cfif>
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#procedure#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_operateurId" 		value="#getOperateurId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="p_chaine" 		   	value=""/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#getIdGroupeMaitre()#" null="#iif((getIdGroupeMaitre() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGrpCtl" 			value="#getId()#"/>
			<cfprocresult name="qProduits"/>        
		</cfstoredproc>
		
		<cfscript>
			setPrixUnitaire(qProduits.TARIF_BRUT);
			setPrixRemise(qProduits.TARIF_REMISE);
			setNbVersion(1); 
			setRemiseContrat(qProduits.PCT_REMISE); 
			setDateDebut(qProduits.DATEDEB); 
			setDateFin(qProduits.DATEFIN);
		</cfscript>			
			
		<cfreturn THIS>
	</cffunction>
	
</cfcomponent>

