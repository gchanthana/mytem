<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.mobileIron.MdmService" extends="fr.saaswedo.api.mdm.impl.AbstractService"
hint="Implémentation de l'API MDM MobileIron utilisant fr.saaswedo.api.mdm.impl.mobileIron.RestClient pour accéder au serveur MDM<br>
L'implémentation met en cache la liste des devices de chaque endpoint en utilisant un des scopes SERVER ou APPLICATION<br>
Les valeurs username ayant le meme endpoint se partagent le meme cache. Un log de warning est écrit une seule fois quand celui utilisé est SERVER
<br>Le cache ne contient que les devices considérés comme enrollés car des doublons SERIAL,IMEI peuvent exister après chaque enrollment
<br>En effet dans MobileIron un meme device peut apparaitre plusieurs de fois avec un statut retiré (RETIRED) et une fois avec le statut enrollé
<br><b>Les doublons (SERIAL,IMEI) doivent d'abord etre retiré du MDM (MobileIron) avant d'utiliser cette implémentation</b>
<br>Une instance de ce composant ne référence que les services d'un seul serveur MDM et utilise les mêmes crédentiels pour y accéder">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.mobileIron.MdmService">
		<cfreturn "fr.saaswedo.api.mdm.impl.mobileIron.MdmService">
	</cffunction>

	<!--- =========== Fonctions de gestion des services =========== --->

	<cffunction access="private" name="getDeviceService" returntype="Struct" hint="Retourne une copie des propriétés du Device qui seront
	fournies à la librairie de Remoting (REST). La structure retournée est une copie car son contenu peut etre modifié par les fonctions appelantes">
		<cftry>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var service={
				endpoint=infos.getEndpoint() & "/api/v1/dm/devices",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfreturn DUPLICATE(service)>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH})>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- =========== Fonctions de gestion du cache de devices =========== --->

	<cffunction access="private" name="xmlDeviceKey" returntype="String"
	hint="Retourne la clé dans la structure retournée par findDeviceFromCache() et qui est associé au device provenant de l'API MDM du vendor">
		<cfreturn "xmlDevice">
	</cffunction>

	<cffunction access="private" name="getDevice" returntype="XML"
	hint="Utilise findDeviceFromCache() pour retourner un device présent le MDM. Mais lève une exception si le device n'existe pas">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>

	<!--- TODO : Les doublons (SERIAL,IMEI) doivent d'abord etre retiré du MDM (MobileIron) avant d'utiliser cette implémentation --->
	<cffunction access="private" name="findDeviceFromCache" returntype="Struct" description="LOCK:Exclusive/ReadOnly"
	hint="Recherche le device dans le Parc MDM local puis récupère son statut sur le serveur MDM. Retourne une structure contenant :<br>
	- Clé xmlDeviceKey(): Structure contenant les infos provenant du MDM concernant le device ou une structure vide sinon (e.g Non enrollé sur le MDM)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du Device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du Device">
		<cfset var serialValue=ARGUMENTS.serialNumber>
		<cfset var imeiValue=ARGUMENTS.imei>
		<cfset var device={}>
		<cfset device[xmlDeviceKey()]={}>
		<!--- Infos d'identification MDM du device --->
		<cfset var mdmPool=new fr.saaswedo.api.mdm.impl.MdmPool(SESSION.PERIMETRE.ID_GROUPE)>
		<cfset var deviceId=mdmPool.getDeviceId(serialValue,imeiValue)>
		<!--- TRUE si le device n'est pas présent le parc MDM local --->
		<cfset var deviceNotInPool=TRUE>
		<!--- TRUE si le device n'est pas présent ou non enrollé sur le MDM --->
		<cfset var deviceNotInMDM=TRUE>
		<cfif isDefined("deviceId")>
			<cfif NOT structIsEmpty(deviceId) AND structKeyExists(deviceId,"MDMID")>
				<cfset var deviceStatus=UNMANAGED()>
				<cfset var deviceUUID=deviceId.MDMID>
				<cfset deviceNotInPool=FALSE>
				<!--- Récupération du device dans le MDM (Librairie Remoting : REST) --->
				<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
				<cfset var service=getDeviceService()>
				<cfset var params=[{type="header",name="accept",value="application/json"}]>
				<cfset service.endpoint=service.endpoint & "/" & deviceUUID>
				<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
				<cfset var result=remoting.invoke(service,"",params)>
				<!--- Erreur durant le reporting --->
				<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
					<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
				<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
					<cfset var httpCode=remoting.getResponseHttpCode(result)>
					<cfset var httpResponse=result[remoting.RESULT()]>
					<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
					<cfset device.RESULT=result[remoting.RESULT()]>
					<!--- Code HTTP d'erreur --->
					<cfif httpCode NEQ 200>
						<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfelse>
						<!--- Désérialisation de la réponse HTTP --->
						<cfif structKeyExists(httpResponse,"FileContent")>
							<cfset var responseContent=httpResponse.FileContent>
							<cfset var dataUtils=new fr.saaswedo.utils.data.DataUtils()>
							<cfset var jsonResponse=deserializeJSON(dataUtils.byteArrayToString(responseContent))>
							<!--- Device enrollé sur le MDM --->
							<cfif structKeyExists(jsonResponse,"device")>
								<cfset var device=extractDevice(jsonResponse.device)>
								<cfif compareNoCase(MANAGED(),device.status) EQ 0>
									<cfset deviceNotInMDM=FALSE>
									<cfset device[xmlDeviceKey()]=jsonResponse.device>
								</cfif>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<!--- Stockage d'un log de warning:
		- Si le device n'est pas présent dans le parc MDM local
		- Si le device n'est pas enrollé ou n'est pas présent sur le MDM
		--->
		<cfif deviceNotInMDM EQ TRUE>
			<cfset var body="">
			<cfset var logUUID=createUUID()>
			<cfsavecontent variable="body">
				<cfoutput>
					Le device (SERIAL,IMEI) #serialValue#,#imeiValue#
					<cfif deviceNotInPool EQ TRUE>
						n'est pas présent dans le parc MDM local.
					<cfelse>
						est présent dans le parc MDM local.
						<cfif deviceNotInMDM EQ TRUE>
							Mais ce device n'est pas enrollé ou n'est pas présent sur le serveur MDM. 
						</cfif>
					</cfif>
				</cfoutput>
			</cfsavecontent>
			<cfset var logContent={UUID=logUUID,SERIAL=serialValue,IMEI=imeiValue,BODY=body}>
			<!--- Réponse HTTP du remoting --->
			<cfif structKeyExists(device,"RESULT")>
				<cfset logContent.SERVER_RESPONSE=device.RESULT>
			</cfif>
			<cfset storeWarning(logContent)>
		</cfif>
		<!--- Supression de la clé RESULT du résultat retourné --->
		<cfset structDelete(device,"RESULT",FALSE)>
		<cfreturn device>
	</cffunction>
	
	<cffunction access="private" name="extractDevice" returntype="Struct"
	hint="Retourne une structure contenant les infos utilisées le cache :<br>UUID (UUID MobileIron),SERIAL,IMEI,OS,STATUS (Statut du device).
	Cette implémentation est est utilisée par la fonction findDeviceFromCache() de ce composant">
		<cfargument name="jsonDevice" type="Struct" required="true" hint="Device de l'API MDM">
		<!--- TODO: Peut etre utiliser l'adresse MAC Wifi comme valeur du SERIAL et IMEI quand ces derniers ne sont pas renseignés --->
		<cfset var jsonStruct=ARGUMENTS.jsonDevice>
		<cfset var deviceUUID=structKeyExists(jsonStruct,"uuid") ? jsonStruct.uuid:"">
		<cfset var deviceStatus=structKeyExists(jsonStruct,"status") ? jsonStruct.status:"">
		<cfset var device={UUID=deviceUUID,SERIAL="",IMEI="",OS="",STATUS=getStatusValue(deviceStatus)}>
		<!--- Système d'exploitation du device --->
		<cfif structKeyExists(jsonStruct,"platform")>
			<cfset var mdmProperty=extractMdmProperty("platform",jsonStruct.platform)>
			<cfif NOT structIsEmpty(mdmProperty)>
				<!--- [feature/MYT-843] BUG dans l'implémentation MobileIron corrigé ici --->
				<cfif NOT structKeyExists(device,"GENERAL")>
					<cfset device.GENERAL={}>
				</cfif>
				<cfset device.GENERAL.OS=mdmProperty.VALUE>
			</cfif>
		</cfif>
		<!--- Détails du device --->
		<cfif structKeyExists(jsonStruct,"details") AND isArray(jsonStruct.details) AND (arrayLen(jsonStruct.details) EQ 1)>
			<cfset var details=[]>
			<cfif structKeyExists(jsonStruct["details"][1],"entry") AND isArray(jsonStruct["details"][1]["entry"])>
				<cfset details=jsonStruct["details"][1]["entry"]>
			</cfif>
			<cfset var detailsCount=arrayLen(details)>
			<!--- Parcours des propriétés du device --->
			<cfloop index="i" from="1" to="#detailsCount#">
				<cfset var property=details[i]>
				<cfset var key=property.key>
				<cfset var value=property.value>
				<cfif key EQ "SerialNumber">
					<cfset device.SERIAL=value>
				<cfelseif (key EQ "IMEI") OR (key EQ "ImeiOrMeid")>
					<cfset device.IMEI=value>
				<cfelseif (key EQ "WiFiMAC") OR (key EQ "wifi_mac_addr")>
					<cfset device.WIFI_MAC=value>
				</cfif>
			</cfloop>
			<!--- TODO: L'utilisation de l'adresse MAC Wifi est à confirmer car il y a un risque d'imcompatibilité avec le parc MDM local
			<cfset var wifiMAC="">
			<cfif (device.SERIAL EQ "") AND (device.IMEI EQ "")>
				<cfif wifiMAC NEQ "">
					<cfset device.SERIAL=wifiMAC>
					<cfset device.IMEI=wifiMAC>
				</cfif>
			</cfif>
			--->
		</cfif>
		<cfreturn device>
	</cffunction>
	
	<!--- Cette fonction n'est pas utilisée dans ce composant. Elle sert aux implémentations qui héritent de ce composant --->
	<cffunction access="private" name="extractMdmProperty" returntype="Struct" hint="Retourne une structure contenant NAME (Propriété),VALUE (Valeur)
	<br>Elle correspond à l'équivalent de property dans la spécification de l'API MDM. Une structure vide est retournée si elle n'est pas reconnue">
		<cfargument name="property" type="String" required="true" hint="Nom de la propriété du provider MDM">
		<cfargument name="propertyValue" type="String" required="true" hint="Valeur de la propriété">
		<cfset var key=ARGUMENTS.property>
		<cfset var value=ARGUMENTS.propertyValue>
		<cfset var mdmProperty={}>
		<!--- Mappage des propriétés du provider MDM avec celles de la spécification de l'API MDM --->
		<cfif (key EQ "WiFiMAC") OR (key EQ "wifi_mac_addr")>
			<cfset mdmProperty={NAME="WIFI_MAC",VALUE=value}>
		<cfelseif key EQ "platform">
			<cfif REFindNoCase("ANDROID",value) GT 0>
				<cfset value=ANDROID()>
			<cfelseif REFindNoCase("IOS",value) GT 0>
				<cfset value=IOS()>
			</cfif>
			<cfset mdmProperty={NAME="SYSTEM_PLATFORM",VALUE=value}>
		<cfelseif key EQ "manufacturer">
			<cfset mdmProperty={NAME="SYSTEM_OEM",VALUE=value}>
		<cfelseif key EQ "AvailableDeviceCapacity">
			<cfset mdmProperty={NAME="FREEDISK",VALUE=VAL(value)}>
		<cfelseif key EQ "DeviceCapacity">
			<cfset mdmProperty={NAME="TOTAL_DISK_SPACE",VALUE=VAL(value)}>
		<cfelseif key EQ "battery_life">
			<cfset mdmProperty={NAME="MAIN_BATTERY_PERCENT",VALUE=VAL(value)}>
		<cfelseif key EQ "BluetoothMAC">
			<cfset mdmProperty={NAME="BLUETOOTH_MAC",VALUE=value}>
		<cfelseif key EQ "locale">
			<cfset mdmProperty={NAME="SYSTEM_LANGUAGE",VALUE=value}>
		<cfelseif key EQ "DataRoamingEnabled">
			<cfset mdmProperty={NAME="DATA_ROAMING_ENABLED",VALUE=yesNoFormat(value)}>
		<cfelseif key EQ "SIMCarrierNetwork">
			<cfset mdmProperty={NAME="SIM_CARRIER_NETWORK",VALUE=value}>
		<cfelseif key EQ "SIM MNC">
			<cfset mdmProperty={NAME="SIM_MNC",VALUE=value}>
		<cfelseif key EQ "Current MCC">
			<cfset mdmProperty={NAME="CURRENT_MCC",VALUE=value}>
		<cfelseif key EQ "CarrierSettingsVersion">
			<cfset mdmProperty={NAME="CARRIER_SETTINGS_VERSION",VALUE=value}>
		<!--- Infos de localisation --->
		<cfelseif (key EQ "latitude_GPS") OR (key EQ "latitude_CELLULAR")>
			<cfset var suffix=listLast(key,"_")>
			<cfset mdmProperty={NAME="GPS_LATITUDE_FROM_" & suffix,VALUE=VAL(value)}>
		<cfelseif (key EQ "longitude_GPS") OR (key EQ "longitude_CELLULAR")>
			<cfset var suffix=listLast(key,"_")>
			<cfset mdmProperty={NAME="GPS_LONGITUDE_FROM_" & suffix,VALUE=VAL(value)}>
		<cfelseif (key EQ "capturedAt_GPS") OR (key EQ "capturedAt_CELLULAR")>
			<cfset var suffix=listLast(key,"_")>
			<cfset var locationDate=createObject("java","java.util.Date").init(VAL(value))>
			<cfset mdmProperty={NAME="GPS_TIMESTAMP_FROM_" & suffix,VALUE=locationDate}>
		</cfif>
		<cfreturn mdmProperty>
	</cffunction>
	
	<!--- =========== Fonctions d'extraction des infos des devices =========== --->
	
	<cffunction access="private" name="getStatusValue" returntype="String" hint="Retourne une des valeurs UNMANAGED(),MANAGED()">
		<cfargument name="mobileIronStatus" type="String" required="true" hint="Valeur d'un statut MobileIron">
		<cfset var deviceStatus=ARGUMENTS.mobileIronStatus>
		<cfset var managedStatusList=["ACTIVE","MANAGED"]>
		<cfloop array="#managedStatusList#" index="status">
			<cfif deviceStatus EQ status>
				<cfreturn MANAGED()>
			</cfif>
		</cfloop>
		<cfreturn UNMANAGED()>
	</cffunction>
	
	<cffunction access="private" name="validStatus" returntype="Array" hint="Retourne la liste des statuts considérés comme valides">
		<cfset var listKey="VALID_STATUS">
		<cfif NOT structKeyExists(VARIABLES,listKey)>
			<!--- Liste des statuts compatible avec la version de l'alimentation du Parc MDM local --->
			<cfset VARIABLES[listKey]=["ACTIVE"]>
		</cfif>
		<cfreturn VARIABLES[listKey]>
	</cffunction>
	

	<cffunction access="private" name="removeDeviceFromCache" returntype="void" description="LOCK:Exclusive" hint="Supprime un device du cache">
		<cfargument name="uuid" type="String" required="true" hint="UUID du device à supprimer">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>

	<cffunction access="private" name="updateDeviceCache" returntype="void" description="LOCK:Exclusive"
	hint="Met à jour le cache : Il est d'abord purgé puis alimenté avec les devices dont le statut est dans la liste retournée par validStatus()<br>
	Le scope utilisé est retourné par getDeviceCacheScope(). Lève une exception MobileIron.RESULT_ERROR en cas d'erreur liée au résultat">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>
	
	<cffunction access="private" name="purgeDeviceCache" returntype="void" description="LOCK:Exclusive"
	hint="Remplace le cache de getMdmServer().getEndpoint() par un nouveau (vide). Ce cache concerne tous les usernames ayant le meme endpoint">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>
	
	<cffunction access="private" name="getDeviceCache" returntype="Query" description="LOCK:ReadOnly" hint="Cache de getMdmServer().getEndpoint()">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>
	
	<cffunction access="private" name="createGlobalDeviceCache" returntype="void" description="LOCK:Exclusive" hint="Crée une seule fois le cache
	global des devices i.e sans les clés pour getMdmServer().getEndpoint(). Ecrit un log de warning si le scope utilisé est SERVER">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>
	
	<cffunction access="private" name="deviceCacheIsCreated" returntype="Boolean" description="LOCK:ReadOnly"
	hint="Retourne TRUE si le cache des devices pour la valeur de getMdmServer().getEndpoint() a été créé et FALSE sinon">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>
	
	<cffunction access="private" name="getDeviceCacheKey" returntype="String" hint="Retourne la clé du cache dans le scope">
		<cfreturn "MobileIron.Cache">
	</cffunction>
	
	<cffunction access="private" name="getDeviceCacheScope" returntype="Struct" description="LOCK:ReadOnly"
	hint="Scope pour le cache des devices : APPLICATION si getDeviceCacheLock() retourne 'APPLICATION' et SERVER sinon">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>
	
	<cffunction access="private" name="getDeviceCacheLock" returntype="String" description="LOCK:ReadOnly" hint="Nom du scope pour le cache des devices">
		<cfthrow type="Custom" message="This function should not be used (Refactoring API MDM)">
	</cffunction>
</cfcomponent>