<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSTunnelService" implements="fr.consotel.api.mdm.partner.private.ws.IWSTunnelService"
hint="Implémentation fournissant les méthodes du Service Tunnel Zenprise. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification">
	<!--- fr.consotel.api.mdm.partner.private.ws.management.WSTunnelService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSTunnelService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
	
	<!--- Tunnel Service --->
	<cffunction access="public" name="removeTunnel" returntype="Void" hint="Supprime un tunnel">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var tunnelService=zdmRepository.getTunnelService()>
		<cftry>
			<cfset tunnelService.removeTunnel(ARGUMENTS["osFamily"],ARGUMENTS["name"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getTunnelList" returntype="Array" hint="Retourne la liste des noms des tunnels dans un tableau">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var tunnelService=zdmRepository.getTunnelService()>
		<cftry>
			<cfreturn tunnelService.getTunnelList()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getOsTunnelList" returntype="Array" hint="Retourne la liste des noms des tunnels pour un OS dans un tableau">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var tunnelService=zdmRepository.getTunnelService()>
		<cftry>
			<cfreturn tunnelService.getTunnelList(ARGUMENTS["osFamily"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="createTunnel" returntype="String" hint="Crée un tunnel et retourne son nom">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à créer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var tunnelService=zdmRepository.getTunnelService()>
		<cftry>
			<cfreturn tunnelService.createTunnel(ARGUMENTS["name"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="createTunnelWithProperties" returntype="Void" hint="Crée un tunnel avec les propriétés fournies">
		<cfargument name="tunnel" type="Struct" required="true" hint="Propriétés du tunnel à créer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var tunnelService=zdmRepository.getTunnelService()>
		<cftry>
			<cfset tunnelService.createTunnel(ARGUMENTS["tunnel"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getTunnelProperty" returntype="Any" hint="Retourne les propriétés d'un tunnel">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var tunnelService=zdmRepository.getTunnelService()>
		<cftry>
			<cfreturn tunnelService.getTunnelProperty(ARGUMENTS["osFamily"],ARGUMENTS["name"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setTunnelProperty" returntype="Void" hint="Définit les propriétés d'un tunnel">
		<cfargument name="tunnel" type="Struct" required="true" hint="Propriétés pour le tunnel">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var tunnelService=zdmRepository.getTunnelService()>
		<cftry>
			<cfset tunnelService.setTunnelProperty(ARGUMENTS["tunnel"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>