package commandemobile.composants.importdemasse
{
	import composants.importmasse.utils.DataGridUtils;
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.external.ExternalInterface;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.ImportDeMasseService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class ImportDeMasseImpl extends Box
	{
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					COMPOSANTS
			//--------------------------------------------------------------------------------------------//
			
		public var dgElements					:DataGrid;
		
			//--------------------------------------------------------------------------------------------//
			//					IHMS
			//--------------------------------------------------------------------------------------------//
		
		public var dgcErase						:DataGridColumn;
		
			//--------------------------------------------------------------------------------------------//
			//					POPUPS
			//--------------------------------------------------------------------------------------------//
		
		private var _popUpUpload				:ImportDeMasseUpload;
		
			//--------------------------------------------------------------------------------------------//
			//					OBJECT
			//--------------------------------------------------------------------------------------------//
			
		private var _fileRef 					:FileReference;	
		
			//--------------------------------------------------------------------------------------------//
			//					SERVICES
			//--------------------------------------------------------------------------------------------//
		
		private var _importSrv					:ImportDeMasseService = new ImportDeMasseService();
		
			//--------------------------------------------------------------------------------------------//
			//					PUBLICS
			//--------------------------------------------------------------------------------------------//
		
		public var IDMdataProvider				:ArrayCollection = new ArrayCollection();
		public var tabColumnsName				:ArrayCollection = new ArrayCollection();
		public var tabColumnsRestricted			:ArrayCollection = new ArrayCollection();
		
		public var isVisibleDeleteColumn		:Boolean = true;
		public var isVisibleBtnAjout			:Boolean = true;
		public var isVisibleBtnEffacer			:Boolean = true;
		public var isVisibleBtnImporter			:Boolean = true;
		public var isReplaceData				:Boolean = false;
		public var isExport						:Boolean = false;
		
		public var textNumberElements			:String = '';
		public var textHelpImport				:String = '';
		public var urlMatriceDownloadOrExport	:String = '';
		public var urlMatriceUpload				:String = '';
		
			//--------------------------------------------------------------------------------------------//
			//					PRIVATE
			//--------------------------------------------------------------------------------------------//
		
		private var _infosImport				:ArrayCollection = new ArrayCollection();
		
		private var _uuid						:String = '';
		private var _fileName					:String = '';
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function ImportDeMasseImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLIC
		//--------------------------------------------------------------------------------------------//
				
		public function setDataGridColumns(myColumns:Array):void
		{
			var dgColumns:Array = dgElements.columns;
			
			for each(var obj : Object in myColumns)
			{
				dgColumns.unshift(obj);
			}
			
			dgElements.columns = dgColumns;
		}
		
		public function getResultXML():XML
		{
			var resultXML : XML = <elements></elements>;
			var myString : String = "";
			var longueur : int = tabColumnsName.length;
			var nameNode : String = "";
			var myPatternSpace:RegExp = / /gi;
			var myPatternA:RegExp = /[àâä]/gi;
			var myPatternC:RegExp = /[ç]/gi;
			var myPatternE:RegExp = /[éèêë]/gi;
			var myPatternI:RegExp = /[îï]/gi;
			var myPatternO:RegExp = /[ôö]/gi;
			var myPatternU:RegExp = /[ûüù]/gi;
			
			for each(var item:Object in IDMdataProvider)
			{
				myString = "<element>";
				
				for(var index:int = 0;index < longueur;index++)
				{
					nameNode = tabColumnsName[index];
					nameNode = nameNode.toLowerCase();
					nameNode = nameNode.replace(myPatternSpace,"_");
					nameNode = nameNode.replace(myPatternA,"a");
					nameNode = nameNode.replace(myPatternC,"c");
					nameNode = nameNode.replace(myPatternE,"e");
					nameNode = nameNode.replace(myPatternI,"i");
					nameNode = nameNode.replace(myPatternO,"o");
					nameNode = nameNode.replace(myPatternU,"u");
					
					myString += "<" + nameNode + ">";
					
					if(item[tabColumnsName[index]])
						myString += item[tabColumnsName[index]];
					
					myString += "</" + nameNode + ">";
				}
				
				myString += "</element>";
				
				var myChild : Object = myString;
				
				resultXML.appendChild(myChild);
			}
			
			return resultXML;
		}
		
		public function imgDeleteClickHandler(me:MouseEvent):void
		{
			if(dgElements.selectedItem != null)
				IDMdataProvider.removeItemAt((dgElements.dataProvider as ArrayCollection).getItemIndex(dgElements.selectedItem));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function btHelpClickHandler(me:MouseEvent):void
		{
			var popUpHelp:PopUpAideImport 	= new PopUpAideImport();
				popUpHelp.myHelpText		= textHelpImport;
			
			PopUpManager.addPopUp(popUpHelp, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(popUpHelp);
		}
		
		protected function btDownloadMatriceImportClickHandler(me:MouseEvent):void
		{
			if(!isExport)
			{
				_fileRef = new FileReference();
				_fileRef.download(new URLRequest(urlMatriceDownloadOrExport));
			}
			else
			{
				var request		:URLRequest = new URLRequest(urlMatriceDownloadOrExport);         
				var variables	:URLVariables 		= new URLVariables();
					variables.LIBELLE_HEADER		= getHeaderForExport();
					variables.DATA_TO_EXPORT		= this.parentDocument.getDataToExport();
					variables.RESTRICTED_HEADER		= this.parentDocument.getRestrictedHeader();
					variables.ADDITIONAL_HEADER		= this.parentDocument.getAdditionalHeader();
					variables.ADDITIONAL_VAR		= this.parentDocument.getAdditionalVar();
					variables.SINGLE_UUID			= this.parentDocument.getUuid();
					variables.FILE_NAME				= this.parentDocument.getFileName();
					
					request.data 	= variables;
					request.method 	= URLRequestMethod.POST;
					
					navigateToURL(request, "_blank");
			}
		}
		
		private function getHeaderForExport():Array
		{
			var header:Array = new Array();
			var lenLib:int = tabColumnsName.length;
			
			for(var i:int = 0;i < lenLib;i++)
			{
				header[i] = tabColumnsName[i].header;
			}
			
			return header;
		}

		protected function btAddRowClickHandler(me:MouseEvent):void
		{
			
		}
		
		protected function btClearClickHandler(me:MouseEvent):void
		{
			//popup de confirmation
		}
		
		protected function btUploadMatriceImportClickHandler(me:MouseEvent):void
		{
			_uuid = '';
			
			_popUpUpload 					= new ImportDeMasseUpload();
			_popUpUpload.urlMatriceUpload 	= urlMatriceUpload;
			
			_popUpUpload.addEventListener('POPUP_CLOSED', popUpClosedHandler);
			_popUpUpload.addEventListener('POPUP_CLOSED_UPLOADED', popUpClosedHandler);
			
			PopUpManager.addPopUp(_popUpUpload, this.parentApplication as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpUpload);
		}
		
		private function popUpClosedHandler(e:Event):void
		{
			if(e.type == 'POPUP_CLOSED_UPLOADED')
			{
				_uuid 		= _popUpUpload.uuid;
				_fileName 	= _popUpUpload.fileName;
				
				getDataFromXLSFile();
			}
		}

		private function getDataFromXLSFile():void
		{
			if(_uuid != '' && _fileName != '')
				getInfosFileImported();
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue_lors_du_traiteme'), 'Consoview');
		}
		
		private function getInfosFileImported():void
		{
			_importSrv.getInfosFileImported(_uuid, _fileName, tabColumnsName.length);
			_importSrv.addEventListener(CommandeEvent.INFOS_IMPORT, infosFileImportedHandler);
		}
		
		private function infosFileImportedHandler(cmde:CommandeEvent):void
		{
			_infosImport = formatInfosImported(_importSrv.infosImport, true);
			
			treatementData();
		}
		
		private function formatInfosImported(values:ArrayCollection, isImported:Boolean):ArrayCollection
		{
			var dataFormated:ArrayCollection = this.parentDocument.formatInfosImported(values, isImported);
			
			return dataFormated;
		}
		
		private function treatementData():void
		{
			var lenElements	:int = IDMdataProvider.length;
			var i			:int = 0;
			
			if(_infosImport != null && _infosImport.length > 1) // données de l'import
			{
				var isOk			:Boolean = false;
				var headerColumns	:Object = _infosImport.getItemAt(0);
				var rsltCheck		:Object = checkHeaderColumnXLS(headerColumns, true);
				var lenColumn		:int = _infosImport.length;
				
				if(rsltCheck.hasOwnProperty('isOk'))
					isOk = rsltCheck.isOk;
				
				if(isOk)
				{
					if(isReplaceData)
					{
						if((_infosImport.length -1) <= lenElements)
						{
							var columnsHeader	:ArrayCollection = rsltCheck.columns as ArrayCollection;
							var lenHeader		:int = columnsHeader.length;
							var bool1:Boolean = false;
							
							for(i = 0;i < lenElements;i++)
							{
								for(var k:int = 0;k < lenHeader;k++)
								{
									var datafieldHeader	:String = columnsHeader[k].datafield;
									
									if(!checkDatafieldRestricted(datafieldHeader))
									{
										if(i < _infosImport.length-1)
										{
											if(_infosImport[i+1]['col_' + (k+1)] != '' && _infosImport[i+1]['col_' + (k+1)] != ' ')
												IDMdataProvider[i][datafieldHeader] = _infosImport[i+1]['col_' + (k+1)];
										}
									}	
								}
							}
							
							dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
						}
						else
						{
							ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Le_nombre_d__l_ments_import_s_est_sup_ri'), 'Consoview');	
						}
					}
					else
					{
						for(i = 1;i < lenColumn;i++)
						{
							IDMdataProvider.addItem(formatObject(_infosImport[i]));
						}
						
						dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
					}
				}
				else
				{
					var headerFinded:Object = findHeaderColumnXLS(headerColumns, true);
					
					if(headerFinded.isOk)
					{
						if(isReplaceData)
						{
							var columnsName	:ArrayCollection = headerFinded.columns as ArrayCollection; // quelles colonnes du DataGrid doivent etre remplacées
							var lenColumns	:int = columnsName.length;
							
							if((_infosImport.length -1) <= lenElements)
							{
								for(i = 0;i < lenElements;i++)
								{
									for(var j:int = 0;j < lenColumns;j++)
									{
										var datafieldIDM	:String = columnsName[j].TABCOLUMNSNAME.datafield;
										var datafieldColumn	:String = columnsName[j].COLUMNS;
										
										if(!checkDatafieldRestricted(datafieldIDM))
										{
											if(i < _infosImport.length-1)
											{
												if(_infosImport[i+1][datafieldColumn] != '' && _infosImport[i+1][datafieldColumn] != ' ')
													IDMdataProvider[i][datafieldIDM] = _infosImport[i+1][datafieldColumn];
											}
										}
									}
								}
								
								dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
							}
							else
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Le_nombre_d__l_ments_import_s_est_sup_ri'), 'Consoview');	
							}
						}
						else
						{
							for(i = 1;i < lenColumn;i++)
							{
								IDMdataProvider.addItem(formatObject(_infosImport[i]));
							}
						}
					}
					else
					{
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');					
					}
				}
			}
			else if(_infosImport == null)
				{
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Aucune_donn_es_import_es__nV_rifier_la_c'), 'Consoview');
				}
				else if(_infosImport.length == 0 || _infosImport.length == 1)
					{
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Aucune_donn_es_a_importer_'), 'Consoview');
					}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					LISTENER IHM
			//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			initiatlisationListeners();
			
			createDataGridColumnErase();
		}
		
		private function refreshSelectedItemHandler(e:Event):void
		{
			if(dgElements.selectedItem != null)
				IDMdataProvider.itemUpdated(dgElements.selectedItem);
		}
		
		private function handleKeyPressed(ke:KeyboardEvent):void
		{
			if ((ke.ctrlKey || ke.altKey) && ke.charCode == 0 && (ke.target is DataGrid))
			{
				//---> AJOUT D'UN OBJET TEXTFIELD INVISIBLE POUR LE DATAGRID
				var textField:TextField	= new TextField();
					textField.name		= 'clipboardProxy';
					textField.visible	= false;
					textField.type		= TextFieldType.INPUT;
					textField.multiline	= true;
					textField.text 		= '';//---> METTRE DANS LE TEXTFIELD LES DONNÉES COPIÉES DEPUIS LE FORMAT TSV 
				
					dgElements.addChild(textField);
				
				textField.setSelection(0, textField.text.length - 1);
				
				textField.addEventListener(TextEvent.TEXT_INPUT, handleTextPasted, false, 0, true);
				
				//---> METTRE LE FOCUS AU TEXTFIELD
				this.systemManager.stage.focus = textField;
			}
		}
		
		protected function handleTextPasted(te:TextEvent):void
		{
			//---> EXTRACT VALUES FROM TSV FORMAT AND POPULATE THE DATAGRID
			var columnText	:Array = DataGridUtils.getItemsFromText(te.text);
			var lenColumn	:int = columnText.length;
			var lenElements	:int = IDMdataProvider.length;
			var i			:int = 0;
			
			columnText =  formatInfosImported(new ArrayCollection(columnText), false).source;
			if(lenColumn > 0)
			{
				var headerColumns:Object = checkHeaderColumnXLS(columnText[0], false);
				var isOk:Boolean = false;
				
				if(headerColumns.hasOwnProperty('isOk'))
					isOk = headerColumns.isOk;
				
				if(isOk)
				{
					if(isReplaceData)
					{
						if((columnText.length -1) <= lenElements)
						{
							var columnsHeader	:ArrayCollection = headerColumns.columns as ArrayCollection;
							var lenHeader		:int = columnsHeader.length;
							var bool1:Boolean = false;
							
							for(i = 0;i < lenElements;i++)
							{
								for(var k:int = 0;k < lenHeader;k++)
								{
									var datafieldHeader	:String = columnsHeader[k].datafield;
									
									if(!checkDatafieldRestricted(datafieldHeader))
									{
										if(i < columnText.length-1)
										{
											if(columnText[i+1]['col' + (k+1)] != '' && columnText[i+1]['col' + (k+1)] != ' ')
												IDMdataProvider[i][datafieldHeader] = columnText[i+1]['col' + (k+1)];
										}
									}
								}
							}
							
							dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
						}
						else
						{
							ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Le_nombre_d__l_ments_import_s_est_sup_ri'), 'Consoview');	
						}
					}
					else
					{
						for(i = 1;i < lenColumn;i++)
						{
							IDMdataProvider.addItem(formatObject(columnText[i]));
						}
						
						dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
					}
				}
				else
				{
					var headerFinded:Object = findHeaderColumnXLS(columnText[0], false);
					
					if(headerFinded.isOk)
					{
						if(isReplaceData)
						{
							var columnsName	:ArrayCollection = headerFinded.columns as ArrayCollection;
							var lenColumns	:int = columnsName.length;
							
							if((columnText.length -1) <= lenElements)
							{
								for(i = 0;i < lenElements;i++)
								{
									for(var j:int = 0;j < lenColumns;j++)
									{
										var datafieldIDM	:String = columnsName[j].TABCOLUMNSNAME.datafield;
										var datafieldColumn	:String = columnsName[j].COLUMNS;
										
										if(!checkDatafieldRestricted(datafieldIDM))
										{
											if(i < columnText.length-1)
											{
												if(columnText[i+1][datafieldColumn] != '' && columnText[i+1][datafieldColumn] != ' ')
													IDMdataProvider[i][datafieldIDM] = columnText[i+1][datafieldColumn];
											}
										}
											
									}
								}
								
								dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
							}
							else
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Le_nombre_d__l_ments_import_s_est_sup_ri'), 'Consoview');	
							}							
						}
						else
						{
							for(i = 1;i < lenColumn;i++)
							{
								IDMdataProvider.addItem(formatObject(columnText[i]));
							}
							
							dispatchEvent(new CommandeEvent(CommandeEvent.IMPORT_FINISHED));
						}
					}
					else
					{
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');					
					}
				}
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS - GESTIONS DU CTRL+C / CTRL-V
			//--------------------------------------------------------------------------------------------//

		private function findHeaderColumnXLS(firstColumn:Object, isImported:Boolean):Object
		{
			var isOk		:Boolean = false;
			var lenLibelle	:int = tabColumnsName.length;
			var i			:int = 0;
			var columns		:Array = new Array();
			var rsltObject	:Object = new Object();
			
			var colNumber	:ArrayCollection = findHeaderLigne(firstColumn);
			var lenCol		:int = colNumber.length;
			
			for(i = 0;i < lenCol;i++)
			{
				var label	:String = colNumber[i].toString();
				
				if(firstColumn[label] != null && firstColumn[label] != '')
					columns[columns.length] = label; // construction array de label
			}
			
			var columnsName	:ArrayCollection = new ArrayCollection();
			var lenColumns	:int = columns.length;
			
			for(i = 0;i < lenColumns;i++)
			{
				for(var j:int = 0;j < lenLibelle;j++)
				{	//attention aux correspondances de langues !!!!
					if(tabColumnsName[j].header == firstColumn[columns[i]]) // si noms de colonnes DataGrid / XSL correspondent
					{
						isOk = true;

						var columnsObject:Object 			= new Object();
							columnsObject.COLUMNS 			= columns[i];
							columnsObject.TABCOLUMNSNAME 	= tabColumnsName[j];
							
						columnsName.addItem(columnsObject);
					}
				}
			}
			
			rsltObject.isOk 	= isOk;
			rsltObject.columns 	= columnsName;
			
			return rsltObject;
		}
		
		private function checkHeaderColumnXLS(firstColumn:Object, isImported:Boolean):Object
		{
			var isOk		:Boolean = true;
			var lenLibelle	:int = tabColumnsName.length;
			var rsltObject	:Object = new Object();
			var columnsName	:ArrayCollection = new ArrayCollection();
			var colNumber	:ArrayCollection = findHeaderLigne(firstColumn);
			
			if(colNumber.length == 0) return false;
			
			if(colNumber.length <lenLibelle) return false;
			
			for(var i:int = 0;i < lenLibelle;i++)
			{
				var libelle	:String = tabColumnsName[i].header;
				var label	:String = colNumber[i].toString();
				
				if(firstColumn[label] != libelle)
					return false;
				else
				{
					columnsName.addItem(tabColumnsName[i]); // ajout des doonnées de la position[i]
				}
			}
			
			rsltObject.isOk 	= isOk;
			rsltObject.columns 	= columnsName;
			
			return rsltObject;
		}
		
		private function findHeaderLigne(value:Object):ArrayCollection
		{
			var proper	:Object = ObjectUtil.getClassInfo(value);			
			
			return new ArrayCollection(proper.properties);
		}
		
		private function formatObject(myObject:Object):Object
		{
			var dataFormated:Object = this.parentDocument.formatObject(myObject);
			
			return dataFormated;
		}
		//** vérifier si le champs à modifier est le champs d'une colonne restreinte **//
		private function checkDatafieldRestricted(datafieldIDM:String):Boolean
		{
			var isOk	:Boolean = false;
			var len		:int = tabColumnsRestricted.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(tabColumnsRestricted[i].datafield == datafieldIDM)
				{
					isOk = true;
					
					break;
				}
			}
			
			return isOk;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					FUNCTIONS
			//--------------------------------------------------------------------------------------------//
		
		private function initiatlisationListeners():void
		{
			systemManager.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyPressed);
		}
		
		private function createDataGridColumnErase():void
		{
			if(isVisibleDeleteColumn)
			{
				var tbTmp:Array = new Array();
					tbTmp.push(dgcErase);
				
				dgElements.columns = tbTmp;
				dgElements.validateNow();				
			}
		}
	}
}