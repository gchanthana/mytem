package univers.inventaire.inventaire.journaux
{
	import flash.events.Event;
	
	/**
	 * Classe evenment AfficherJournalEvent
	 * Transmet les informations nécessaires à l'affichage d'un journal 
	 * */
	public class AfficherJournalEvent extends Event
	{
		/**
		 * Constatnte static deffinissant le type 'AFFICHER_JOURAUX' pour cet évènement
		 * */
		public static const AFFICHER_JOURNAUX : String = "AFFICHER_JOURAUX";
		
		
		//Reference vers le type de journal à afficher
		private var _typeJournal : String;
		
		//Les informations sur le noeud pour lequel on affiche le journal
		private var _nodeInfos : Object;
		
		/**
		 * Constructeur 
		 * voir la classe flash.events.Event pour les parametres
		 * */
		public function AfficherJournalEvent(bubbles:Boolean=false, cancelable:Boolean=false)
		{
					
			super(AfficherJournalEvent.AFFICHER_JOURNAUX,bubbles, cancelable);
		}
				
		/**
		 * Getter pour le type de journal
		 * @return _typeJournal le type de journal
		 * */
		public function get typeJournal():String{
			return _typeJournal;
		}
		
		/**
		 * Setter pour le type de journal
		 * @param type le type de journal 
		 * */
		public function set typeJournal(type : String):void{
			_typeJournal = type;
		}		
		
		
		/**
		 * Getter pour l'objet contenant les infos sur le noeud
		 * attributs de l'objet NID -> identifiant du noeud
		 * 						LBL -> libelle du noeud
		 * @return _nodeInfos un objet dont les attributs sont les infos sur le noeud
		 * */
		public function get nodeInfos():Object{
			return _nodeInfos;
		}
		
		/**
		 * Setter pour l'objet contenant les infos sur le noeud
		 * @param Infos un objet dont les attributs sont les infos sur le noeud
		 * attributs de l'objet NID -> identifiant du noeud
		 * 						LBL -> libelle du noeud
		 * */
		public function set nodeInfos(infos : Object):void{
			_nodeInfos = infos;
		}
		
	}
}