package univers.parametres.perimetres.search
{
    import composants.access.GenericPerimetreTree;
    import composants.access.perimetre.PerimetreEvent;
    import composants.access.perimetre.PerimetreTreeDataDescriptor;
    import composants.parametres.perimetres.tree.FilteredTreeDataDescriptor;
    import composants.util.ConsoviewUtil;
    
    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.collections.ICollectionView;
    import mx.collections.IViewCursor;
    import mx.collections.XMLListCollection;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;

    public class SpecialSearch extends EventDispatcher
    {
        private var treeSrc:GenericPerimetreTree;
        private var idTargetNode:Number;
        private var listeNodeChemin:ArrayCollection;
        private var selectedNode:Object;

        public function SpecialSearch(target:IEventDispatcher = null)
        {
            //TODO: implement function
            super(target);
        }

        public function initSearch(treeSource:GenericPerimetreTree, idNodeCibe:Number):void
        {
            treeSrc = treeSource;
            treeSrc.selectedIndex = 0;
            selectedNode = treeSrc.selectedItem;
            treeSrc.validateNow();
            treeSrc.expandItem(selectedNode, false);
            PerimetreTreeDataDescriptor(treeSrc.dataDescriptor).addEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreChildResultEventHandler);
            idTargetNode = idNodeCibe;
            getListeNodesChemin();
        }

        /*private function getListeNodesChemin():void{
           var idSourceNode : Number = treeSrc.selectedItem.@NID;

           //si on est sur une branche on lance la recherche sinon on ne fait rien et on sort.
           if (PerimetreTreeDataDescriptor(treeSrc.dataDescriptor).isBranch(selectedNode)){
           var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
           "fr.consotel.consoview.parametres.perimetres.gestionorganisation.GestionOrganisationGateWay",
           "getCheminSourceCibleAsQuery",
           getCheminSourceCibleResultHandler);
           RemoteObjectUtil.callService(op,idSourceNode,idTargetNode);
           }else{
           PerimetreTreeDataDescriptor(treeSrc.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT,perimetreChildResultEventHandler);
           return;
           }
           }

           private function getCheminSourceCibleResultHandler (re : ResultEvent):void{
           if (re.result){
         listeNodeChemin = re.result as ArrayCollection;	*/
        private function getListeNodesChemin():void
        {
            var idSourceNode:Number = treeSrc.selectedItem.@NID;
            var idRacine:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
            //si on est sur une branche on lance la recherche sinon on ne fait rien et on sort.
            if (PerimetreTreeDataDescriptor(treeSrc.dataDescriptor).isBranch(selectedNode))
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, //"fr.consotel.consoview.parametres.perimetres.gestionorganisation.GestionOrganisationGateWay",
                                                                                    "fr.consotel.consoview.access.AccessManager", "getNodeXmlPath", getCheminSourceCibleResultHandler);
                RemoteObjectUtil.callService(op, idRacine, idSourceNode, idTargetNode);
            }
            else
            {
                PerimetreTreeDataDescriptor(treeSrc.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreChildResultEventHandler);
                return;
            }
        }

        private function xmlLlistToArray(xmllist:XMLList):Array
        {
            var tab:Array = new Array();
            var len:Number = xmllist.length();
            for (var i:Number = 0; i < len; i++)
            {
                var obj:Object = new Object();
                obj.TYPE_ORGA = "";
                obj.STC = Number(xmllist[i].@STC);
                obj.IDGROUPE_CLIENT = Number(xmllist[i].@NID);
                obj.LIBELLE_GROUPE_CLIENT = String(xmllist[i].@LBL);
                tab.push(obj);
            }
            return tab
        }

        private function getCheminSourceCibleResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                var tmp:XMLList = (re.result as XML).descendants("NODE");
                listeNodeChemin = new ArrayCollection();
                listeNodeChemin.source = xmlLlistToArray(tmp);
                listeNodeChemin.refresh();
                //si on est sur une branche et que les enfants de cette branche ne sont pas encore chargé												
                //On les charge et la recherche est traitée
                //Sinon  si on est sur une branche 
                //On lance la recherche sur les enfants de cette branche.
                //Sinon on est sur une feuille et on ne fait rien
                if ((!FilteredTreeDataDescriptor(treeSrc.dataDescriptor).hasChildren(selectedNode)) && FilteredTreeDataDescriptor(treeSrc.dataDescriptor).isBranch(selectedNode))
                {
                    FilteredTreeDataDescriptor(treeSrc.dataDescriptor).loadNodeChild(selectedNode);
                }
                else if (FilteredTreeDataDescriptor(treeSrc.dataDescriptor).isBranch(selectedNode))
                {
                    traiterLaRecherche();
                }
                else
                {
                    FilteredTreeDataDescriptor(treeSrc.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreChildResultEventHandler);
                    return;
                }
            }
        }

        private function perimetreChildResultEventHandler(pe:PerimetreEvent):void
        {
            treeSrc.validateNow();
            traiterLaRecherche();
        }

        //Traite la recherche
        private function traiterLaRecherche():void
        {
            var listechildren:ICollectionView = FilteredTreeDataDescriptor(treeSrc.dataDescriptor).getChildren(selectedNode);
            // s' il n y a pas d' enfants
            if (listechildren.length > 0)
            {
                var cursor:IViewCursor = listechildren.createCursor();
                //si on a trouvé le noeud on le selectionne et on sort
                if (cursor.current.@NID == idTargetNode)
                {
                    treeSrc.validateNow();
                    selectNodeById(idTargetNode);
                    trace("2");
                    FilteredTreeDataDescriptor(treeSrc.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreChildResultEventHandler);
                    return;
                }
                else
                {
                    do
                    {
                        if (ConsoviewUtil.isIdInArray(parseInt(cursor.current.@NID, 10), "IDGROUPE_CLIENT", listeNodeChemin.source))
                        {
                            selectedNode = cursor.current;
                            //si on a trouvé le noeud on le selectionne et on sort
                            if (cursor.current.@NID == idTargetNode)
                            {
                                treeSrc.validateNow();
                                selectNodeById(idTargetNode);
                                trace("1");
                                FilteredTreeDataDescriptor(treeSrc.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreChildResultEventHandler);
                                return;
                            }
                            //si on est sur une branche et que les enfants de cette branche ne sont pas encore chargé												
                            //On les charge et la recherche sur ces enfants est traitée
                            //Sinon  si on est sur une branche 
                            //On continue la recherche sur les enfants de cette branche.
                            //Sinon on est sur une feuille et on stop la recherche
                            if ((!FilteredTreeDataDescriptor(treeSrc.dataDescriptor).hasChildren(selectedNode)) && FilteredTreeDataDescriptor(treeSrc.dataDescriptor).isBranch(selectedNode))
                            {
                                FilteredTreeDataDescriptor(treeSrc.dataDescriptor).loadNodeChild(selectedNode);
                            }
                            else if (FilteredTreeDataDescriptor(treeSrc.dataDescriptor).isBranch(selectedNode))
                            {
                                traiterLaRecherche();
                                return;
                            }
                            else
                            {
                                FilteredTreeDataDescriptor(treeSrc.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreChildResultEventHandler);
                                //
                                return;
                            }
                        }
                    } while (cursor.moveNext())
                }
            }
            else
            {
                FilteredTreeDataDescriptor(treeSrc.dataDescriptor).removeEventListener(PerimetreEvent.PERIMETRE_CHILD_RESULT, perimetreChildResultEventHandler);
            }
            //		
        }

        private function selectNodeById(nodeId:int):Boolean
        {
            try
            {
                var node:XML = (treeSrc.dataProvider as XMLListCollection).getItemAt(0) as XML;
                if (parseInt(node.@NID, 10) == nodeId)
                {
                    treeSrc.selectedItem = node;
                    return true;
                }
                else
                {
                    var resultList:XMLList = node.descendants().(@NID == nodeId);
                    if (resultList.length() > 0)
                    {
                        var targetNode:XML = resultList[0] as XML;
                        var tmpParentNode:Object = targetNode.parent();
                        while (tmpParentNode != null)
                        {
                            if (treeSrc.isItemOpen(tmpParentNode) == false)
                            {
                                treeSrc.validateNow();
                                try
                                {
                                    treeSrc.expandItem(tmpParentNode, true)
                                }
                                catch (er:Error)
                                {
                                    trace(er.getStackTrace(), "*******", tmpParentNode, "*********")
                                }
                            }
                            tmpParentNode = (tmpParentNode as XML).parent();
                        }
                        treeSrc.selectedItem = targetNode;
                        treeSrc.validateNow();
                        treeSrc.scrollToIndex(treeSrc.selectedIndex);
                        treeSrc.invalidateDisplayList();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (te:TypeError)
            {
                return false
            }
            return false
        }
    }
}