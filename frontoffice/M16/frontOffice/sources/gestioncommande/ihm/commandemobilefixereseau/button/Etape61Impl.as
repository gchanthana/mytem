package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.TextEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import composants.importmasse.ImportMasseIHM;
	import composants.importmasse.utils.DataGridUtils;
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	import composants.util.lignes.LignesUtils;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.GestionParcMobileEvent;
	import gestioncommande.events.ImportDeMasseEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.services.EmployeService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.organisation.OrganisationServices;
	
	import session.SessionUserObject;
	
	public class Etape61Impl extends AbstractWorflow implements IWorflow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var myComposantImportMasse	:ImportMasseIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					COLUMN IMPORT DE MASSE
		//--------------------------------------------------------------------------------------------//
		
		public var colonne1					:DataGridColumn;
		public var colonne2					:DataGridColumn;
		public var colonne3					:DataGridColumn;
		public var colonne4					:DataGridColumn;

		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _numLine				:LignesUtils = new LignesUtils();
		private var _employeSrv				:EmployeService = new EmployeService();
		private var _orgaService			:OrganisationServices = new OrganisationServices();
				
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//

		private var _cmd					:Commande;
		private var _myElements				:ElementsCommande2	= new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES BUTTON CUSSTOM
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 4;
		
		public var ACCESS						:Boolean = false;
		
		private var _razl						:Boolean = false;

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
				
		public var elementsConfiguration	:ArrayCollection = new ArrayCollection();
		
		private var _listeOrgaCliente		:ArrayCollection = new ArrayCollection();
		private var _libelleHeader			:ArrayCollection = new ArrayCollection([
																						{header:ResourceManager.getInstance().getString('M16','Libell_')},
																						{header:ResourceManager.getInstance().getString('M16','Matricule')},
																						{header:ResourceManager.getInstance().getString('M16','Portabilit_')},
																						{header:ResourceManager.getInstance().getString('M16','Num_ro')},
																						{header:ResourceManager.getInstance().getString('M16','Code_RIO')},
																						{header:ResourceManager.getInstance().getString('M16','Date_de_portabilit_')}
																					]);
		
		private var _matriculeValidate		:Boolean = true;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//

		public function Etape61Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					LISTENER DE L'IHM
	//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			//---> LISTENER IMPORT DE MASSE
			addEventListener(ImportDeMasseEvent.IMPORT_ADD_ITEM, importAddElementHandler);
			addEventListener(ImportDeMasseEvent.IMPORT_REMOVE_ITEM, importRemoveItemHandler);
			addEventListener(ImportDeMasseEvent.IMPORT_REMOVE_ALL, importRemoveAllHandler);
			
			this.myComposantImportMasse.addEventListener('AUTO_FILL', autoFillHandler);
			this.myComposantImportMasse.addEventListener('ERASE_LIBELLE_FILL', eraseLibelleFillHandler);
			
			//--->LISTENER ITEMRENDERER IMPORT DE MASSE
			addEventListener("REFRESH_ITEMSELECTED", refreshItemSelecetdHandler);
			
			systemManager.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyPressed);
			
			initDataGridImportMasse();
			getOrganisationsClientes();
			
			elementsConfiguration.addEventListener(CollectionEvent.COLLECTION_CHANGE, updatePanier);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}

		public function viewStackChange():void
		{
			_cmd 		= SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION; 
			
			SessionUserObject.singletonSession.INFOS_DIVERS 		= new Object();
			SessionUserObject.singletonSession.INFOS_DIVERS.IDPOOL 	= _cmd.IDPOOL_GESTIONNAIRE;
			SessionUserObject.singletonSession.LASTVSINDEX 			= IDPAGE;
			
			var elements		:ArrayCollection = myComposantImportMasse.dgElement.dataProvider as ArrayCollection;
			var configNumber	:int 			 = _cmd.CONFIG_NUMBER;
			var elementsNbr		:int			 = configNumber - (elements.length);
			
			_myElements.CONFIGURATIONS = elements;
			
			for(var i:int = 0;i < elementsNbr;i++)
			{
				var newConfiguration:ConfigurationElements  = new ConfigurationElements();
					newConfiguration.ORGANISATIONS 			= _listeOrgaCliente;
					newConfiguration.ID_TYPELIGNE			= _myElements.IDTYPELIGNE;
					newConfiguration.TYPELIGNE				= _myElements.TYPELIGNE;
					
				/*if(SessionUserObject.singletonSession.IDSEGMENT > 1)
					newConfiguration.IS_MOBILE = false;*/
					
				elementsConfiguration.addItem(newConfiguration);
			}
			
			myComposantImportMasse.updateNbElementText();
		}
		
		public function reset():void
		{
			elementsConfiguration.removeAll();
		}
		
		public function setConfiguration():void
		{
			_razl = false;
			setElements();
		}

	//--------------------------------------------------------------------------------------------//
	//					VERIFICATION ET CONTRUCTION DES DONNÉES CHOISIES
	//--------------------------------------------------------------------------------------------//
		
		public function checkData():Boolean
		{
			var isOk:Boolean = true;
			var nbr	:int = elementsConfiguration.length;
			
			getNumeroLigne(nbr);
			
			isOk = checkInfosGrid();
			
			if(isOk)
				addElements();
			
			return isOk;
		}

		public function buildParameters():void
		{
		}
		
//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A L'ETAPE 
//--------------------------------------------------------------------------------------------//
	
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//

//----------------------------------------COPIER/COLLER----------------------------------------------------//		
		
		private function handleKeyPressed(ke:KeyboardEvent):void
		{
			if ((ke.ctrlKey || ke.altKey) && ke.charCode == 0 && (ke.target is DataGrid))
			{
				//---> AJOUT D'UN OBJET TEXTFIELD INVISIBLE POUR LE DATAGRID
				var textField:TextField	= new TextField();
				textField.name		= "clipboardProxy";
				textField.visible	= false;
				textField.type		= TextFieldType.INPUT;
				textField.multiline	= true;
				textField.text 		= '';//---> METTRE DANS LE TEXTFIELD LES DONNÉES COPIÉES DEPUIS LE FORMAT TSV 
				
				myComposantImportMasse.dgElement.addChild(textField);
				
				textField.setSelection(0, textField.text.length - 1);
				
				textField.addEventListener(TextEvent.TEXT_INPUT, handleTextPasted, false, 0, true);
				
				//---> METTRE LE FOCUS AU TEXTFIELD
				myComposantImportMasse.systemManager.stage.focus = textField;
			}
		}

		protected function handleTextPasted(te:TextEvent):void
		{
			//---> EXTRACT VALUES FROM TSV FORMAT AND POPULATE THE DATAGRID
			var columnText	:Array = DataGridUtils.getItemsFromText(te.text);
			var lenColumn	:int = columnText.length;
			
			if(lenColumn > 0)
			{
				if(lenColumn > 101)
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Il_y_a_trop_de_collaborateurs___plus_de_100_lignes__'), 'Consoview');
				else
				{
					if(checkHeaderColumnXLS(columnText[0]))
					{
						for(var i:int = 1;i < lenColumn;i++)
						{
							elementsConfiguration.addItem(formatObjectToConfigurationElements(columnText[i]));
						}
					}
					else
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');
				}
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Les_en_t_tes_des_colonnes_ne_sont_pas_en'), 'Consoview');
		}
		
		private function checkHeaderColumnXLS(firstColumn:Object):Boolean
		{
			var lenLibelle	:int = _libelleHeader.length;
			var isOk		:Boolean = true;
			
			if(!firstColumn.hasOwnProperty('col6'))
				return false;
			
			for(var i:int = 0;i < lenLibelle;i++)
			{
				var libelle	:String = _libelleHeader[i].header;
				var label	:String = 'col' + (i+1).toString();
				
				if(firstColumn[label] != libelle)
					return false;
			}
			
			return isOk;
		}
		
		private function formatObjectToConfigurationElements(column:Object):ConfigurationElements
		{
			var elts:ConfigurationElements  = new ConfigurationElements();
				elts.ORGANISATIONS 			= _listeOrgaCliente;
				elts.LIBELLE		 		= column['col1'];
				elts.MATRICULE		 		= column['col2'];
			
			if(elts.MATRICULE != '')
				elts.IS_COLLABORATEUR = true;
			
			if(column['col3'] != '')
			{
				elts.IS_PORTABILITE				 = true;
				elts.PORTABILITE.NUMERO			 = column['col4'];
				elts.PORTABILITE.CODERIO		 = column['col5'];
				elts.PORTABILITE.DATEPORTABILITE = Formator.formatDateStringInDate(column['col6']);
			}
			
			return elts;
		}
		
//----------------------------------------COPIER/COLLER----------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER
		//--------------------------------------------------------------------------------------------//
		
		private function updatePanier(ce:CollectionEvent):void
		{
			myComposantImportMasse.dispatchEvent(new ImportDeMasseEvent(ImportDeMasseEvent.IMPORT_REFRESH));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER IMPORT DE MASSE
		//--------------------------------------------------------------------------------------------//
		
		private function importAddElementHandler(ide:ImportDeMasseEvent):void
		{
			var newConfiguration:ConfigurationElements  = new ConfigurationElements();
				newConfiguration.ORGANISATIONS 			= _listeOrgaCliente;
				newConfiguration.ID_TYPELIGNE			= _myElements.IDTYPELIGNE;
				newConfiguration.TYPELIGNE				= _myElements.TYPELIGNE;
			
			/*if(SessionUserObject.singletonSession.IDSEGMENT > 1)
				newConfiguration.IS_MOBILE = false;*/
			
			elementsConfiguration.addItem(newConfiguration);
			
			myComposantImportMasse.updateNbElementText();
			
			_cmd.CONFIG_NUMBER++;
			
			dispatchEvent(new Event('PRICE_CHANGE', true));
		}
		
		private function importRemoveItemHandler(ide:ImportDeMasseEvent):void
		{
			if(myComposantImportMasse.dgElement.selectedItem != null)
			{
				var idx:int = myComposantImportMasse.dgElement.selectedIndex;
				
				if(elementsConfiguration.length > 1)
				{
					elementsConfiguration.removeItemAt(idx);
					_cmd.CONFIG_NUMBER--;
				}
				else
				{
					elementsConfiguration.removeAll();
					
					var newConfiguration:ConfigurationElements 	= new ConfigurationElements();
						newConfiguration.ORGANISATIONS 			= _listeOrgaCliente;
						newConfiguration.ID_TYPELIGNE			= _myElements.IDTYPELIGNE;
						newConfiguration.TYPELIGNE				= _myElements.TYPELIGNE;
						
					/*if(SessionUserObject.singletonSession.IDSEGMENT > 1)
						newConfiguration.IS_MOBILE = false;*/
					
					elementsConfiguration.addItem(newConfiguration);
					
					_cmd.CONFIG_NUMBER = 1;
				}
				
				myComposantImportMasse.updateNbElementText();
				
				dispatchEvent(new Event('PRICE_CHANGE', true));
			}
		}
		
		private function importRemoveAllHandler(ide:ImportDeMasseEvent):void
		{
			if(myComposantImportMasse.dgElement.dataProvider != null)
			{
				(myComposantImportMasse.dgElement.dataProvider as ArrayCollection).removeAll();
				
				var newConfiguration:ConfigurationElements 	= new ConfigurationElements();
					newConfiguration.ORGANISATIONS 			= _listeOrgaCliente;
					newConfiguration.ID_TYPELIGNE			= _myElements.IDTYPELIGNE;
					newConfiguration.TYPELIGNE				= _myElements.TYPELIGNE;
					
				/*if(SessionUserObject.singletonSession.IDSEGMENT > 1)
					newConfiguration.IS_MOBILE = false;*/
				
				elementsConfiguration.addItem(newConfiguration);
				
				myComposantImportMasse.updateNbElementText();
				
				_cmd.CONFIG_NUMBER = 1;
				
				dispatchEvent(new Event('PRICE_CHANGE', true));
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER ITEMRENDERER DE L'IMPORT DE MASSE
		//--------------------------------------------------------------------------------------------//
		
		private function refreshItemSelecetdHandler(e:Event):void
		{
			if(myComposantImportMasse.dgElement.selectedItem != null)
			{
				elementsConfiguration.itemUpdated(myComposantImportMasse.dgElement.selectedItem);
				myComposantImportMasse.updateNbElementText();
			}
		}
				
		private function autoFillHandler(e:Event):void
		{
			var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var cptr		:int = 0;
			
			for each(var commandeArticle:Object in elements)
			{
				cptr = cptr + commandeArticle.CONFIGURATIONS.length;
			}
			
			if(cptr == 0)
				cptr = 1;
			else
			{
				if(elements.length >= 1)
					cptr++;
			}
			
			for each(var currentElementsConfig:Object in elementsConfiguration)
			{
				if(currentElementsConfig.LIBELLE == '')
				{
					currentElementsConfig.LIBELLE = ResourceManager.getInstance().getString('M16','Utilisateur') + ' ' + cptr.toString() + ' - ' + _cmd.NUMERO_COMMANDE;
					
					elementsConfiguration.itemUpdated(currentElementsConfig);
				}
				
				cptr++;
			}
		}
		
		private function eraseLibelleFillHandler(e:Event):void
		{
			for each(var currentElementsConfig:Object in elementsConfiguration)
			{
				currentElementsConfig.LIBELLE = '';
				
				elementsConfiguration.itemUpdated(currentElementsConfig);
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENER PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		//---> AFFECTE A CHAQUE CONFIGURATION UN NUMERO DE COMMANDE
		private function initDataGridImportMasse():void
		{
			var largeurTotalDG:Number = myComposantImportMasse.dgElement.width;
			
			//---> POUR ÉVITER UN BUG DE DIMENSION DES COLONNES, IL FAUT SUPPRIMER LA COLONNE "SUPPRIMER" PUIS LA REMETTRE
			var colonne4:DataGridColumn	= new DataGridColumn();
			colonne4.dataField		= "DELETE";
			colonne4.editable		= false;
			colonne4.headerText		= ResourceManager.getInstance().getString('M16','Supprimer');
			colonne4.width			= 77;
			colonne4.minWidth		= 77;
			colonne4.itemRenderer	= myComposantImportMasse.dgElement.columns.pop().itemRenderer;
			colonne4.sortable		= false;
			
			myComposantImportMasse.dgElement.columns 	= new Array();
			myComposantImportMasse.myDataProvider 		= elementsConfiguration;
			
			myComposantImportMasse.addColumnInDG([colonne4, colonne3, colonne2, colonne1]);
			
			myComposantImportMasse.urlDownload = moduleCommandeMobileIHM.NonSecureUrlBackoffice + 
													"/fr/consotel/consoview/M16/matriceimportdemasse/Matrice_Import_" + 
													CvAccessManager.getSession().USER.GLOBALIZATION + 
													".xls";
		}
		
		private function orgaClientesHandler(gpme:GestionParcMobileEvent):void
		{
			_listeOrgaCliente = _orgaService.myDatas.listeOrga;
		}
		
		private function employeMatriculeHandler(cmde:CommandeEvent):void
		{
			var matricules	:ArrayCollection = _employeSrv.matrciuleAndInfos;
			var lenMatri	:int = matricules.length;
			var lenConfig	:int = elementsConfiguration.length;			
			
			for(var i:int = 0; i < lenConfig;i++)
			{
				var mat:String = elementsConfiguration[i].MATRICULE;
				
				for(var j:int = 0; j < lenMatri;j++)
				{
					if(matricules[j].ORIG == mat)
					{
						if(matricules[j].VALIDITEMATRICULE == "KO")
						{
							elementsConfiguration[i].ERROR 	  = ResourceManager.getInstance().getString('M16', 'Le_matricule_n_est_pas_valide');
							elementsConfiguration[i].IS_ERROR = true;
							_matriculeValidate = false;
						}
						else
						{
							elementsConfiguration[i].ERROR 	  			= "";
							elementsConfiguration[i].IS_ERROR 			= false;
							elementsConfiguration[i].ID_COLLABORATEUR	= matricules[j].IDEMPLOYE;
							elementsConfiguration[i].COLLABORATEUR		= matricules[j].PRENOM + ' ' + matricules[j].NOM;
						}
						
						break;
					}
				}
			}
			
			if(!_matriculeValidate)
				(this.parent.parent.parent as Object).selectedIndex = 5;
		}
		
		private function numeroLigneHandler(cmde:CommandeEvent):void
		{
			var lenConfig	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			var newNumber	:Array = _numLine.allLignes;
			
			if(lenConfig != newNumber.length) return;
			
			for(var i:int = 0;i < lenConfig;i++)
			{
				(SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i] as ConfigurationElements).PORTABILITE.NUMERO_HIDDEN = newNumber[i];
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getOrganisationsClientes():void
		{
			_orgaService.getListOrgaCliente();
			_orgaService.myDatas.addEventListener(GestionParcMobileEvent.LISTE_ORGA_LOADED, orgaClientesHandler);
		}
		
		private function getEmployFromMat(matricules:String):void
		{
			_employeSrv.getEmployFromMat(matricules);
			_employeSrv.addEventListener(CommandeEvent.MARICULES_LISTED, employeMatriculeHandler);
		}
		
		private function getNumeroLigne(nbr:int):void
		{
			_numLine.genererNumerosLignesUnique(nbr);
			_numLine.addEventListener(CommandeEvent.LISTED_NUMEROS, numeroLigneHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function checkInfosGrid():Boolean
		{
			var isOk	:Boolean = true;
			var len		:int = elementsConfiguration.length;
			
			if(len > 0)
			{
				if (len > 100)
				{
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Il_y_a_trop_de_lignes__plus_de_100_lignes__'), 'Consoview');
					isOk = false;
				}
				else
					isOk = checkDataImport();
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Au_moins_un_libell__doit__tre_d_fini'), 'Consoview');
				isOk = false;
			}
			
			return isOk;
		}

		private function addElements():void
		{
			var len:int = elementsConfiguration.length;
			
			_myElements.CONFIGURATIONS = new ArrayCollection();
			
			for(var i:int = 0; i < len;i++)
			{
				_myElements.CONFIGURATIONS.addItem(elementsConfiguration[i]);
			}
		}

		private function setElements():void
		{
			var len:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			
			elementsConfiguration = new ArrayCollection();
			
			for(var i:int = 0; i < len;i++)
			{
				elementsConfiguration.addItem(SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS[i]);
			}
			
			myComposantImportMasse.myDataProvider = elementsConfiguration;
			myComposantImportMasse.updateNbElementText();
		}
		
		private function checkDataImport():Boolean
		{
			var isOk				:Boolean = true;
			
			var reOneCharMin		:RegExp = /(\w+)/;
			
			var myclob				:String = "";
			var strLibelle			:String = "";
			var strCollabName		:String = "";
			var strMatricule		:String = "";
			var strNumero			:String = "";
			var strCodeRio			:String = "";
			
			var strDatePortabilite	:Date = null;
			
			var ALibelle			:ArrayCollection	= new ArrayCollection();
			var ANumero				:ArrayCollection	= new ArrayCollection();
			var ACodeRio			:ArrayCollection	= new ArrayCollection();
			
			var len					:int = elementsConfiguration.length;
			
			for(var i:int = 0; i < len;i++)
			{
				strLibelle			= (elementsConfiguration[i] as ConfigurationElements).LIBELLE;
				strCollabName		= (elementsConfiguration[i] as ConfigurationElements).COLLABORATEUR;
				strMatricule		= (elementsConfiguration[i] as ConfigurationElements).MATRICULE;
				strNumero			= (elementsConfiguration[i] as ConfigurationElements).PORTABILITE.NUMERO;
				strCodeRio			= (elementsConfiguration[i] as ConfigurationElements).PORTABILITE.CODERIO;
				strDatePortabilite	= (elementsConfiguration[i] as ConfigurationElements).PORTABILITE.DATEPORTABILITE;
				
				//---> LE "NOM" OU LE "MATRICULE" OU LE "LIBELLE" DOIT ÊTRE RENSEIGNÉ
				if (strCollabName == "" && strMatricule == "" && strLibelle == "")
				{
					ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Un_collaborateur_ou_un_libell__doit__tre_renseign___'), 'Consoview');
					return false;
				}
				else
				{
					//---> SI PAS DE "NOM" NI DE "MATRICULE" ALORS "LIBELLE"
					if (strCollabName == "" && strMatricule == "" && strLibelle != "")
					{
						//---> LE "LIBELLE" DOIT CONTENIR DES CARACTÈRES
						if (strLibelle.split(reOneCharMin).length <= 1)
						{
							ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Le_libell__doit_contenir_des_caract_res__'), 'Consoview');
							return false;
						}
						
						//---> TEST SUR L'UNICITÉ DU LIBELLÉ
						if (ALibelle.contains(strLibelle) == true)
						{
							ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Tous_les_libell_s_doivent__tre_unique__'), 'Consoview');
							return false;
						}
						else
							ALibelle.addItem(strLibelle);
					}
					
					if(strCollabName == "" && strMatricule != "")
					{
						myclob += "'" + strMatricule + "'" + ",";
					}
					
					//---> SI "PORTABILITE" ALORS LES CHAMPS "NUMERO", "CODE RIO" ET "DATE DE PORTABILITÉ" DOIVENT ÊTRE RENSEIGNÉS
					if ((elementsConfiguration[i] as ConfigurationElements).IS_PORTABILITE)
					{
						if (strNumero == "" || strCodeRio == "" || strDatePortabilite == null)
						{
							ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Si_la_portabilit__est_s_l_ctionn_e_alors_le__num__ro____le_code_RIO_'), 'Consoview');
							return false;
						}
						else
						{
							//---> NUMÉRO DE MOBILE
							if (isNaN(new Number(strNumero.replace(new RegExp(" ","g"),""))))
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Num_ro_erron_'), 'Consoview');
								return false;
							}
							
							//---> TEST SUR L'UNICITÉ DU NUMÉRO
							if (ANumero.contains(strNumero.replace(new RegExp(" ","g"),"")) == true)
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Tous_les_num_ros_doivent__tre_unique__'), 'Consoview');
								return false;
							}
							else
								ANumero.addItem(strNumero.replace(new RegExp(" ","g"),""));
							
							//---> CODE RIO
							if (strCodeRio.length != 12)
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Le_code_RIO_doit_contenir_12_caract_res__'), 'Consoview');
								return false;
							}
							
							//---> TEST SUR L'UNICITÉ DU CODE RIO
							if (ACodeRio.contains(strCodeRio) == true)
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16','Tous_les_code_RIO_doivent__tre_unique__'), 'Consoview');
								return false;
							}
							else
								ACodeRio.addItem(strCodeRio);
							
							//---> DATE DE PORTABILITÉ
							var dateRef	:Date = new Date(-1); //---> DATE 1970...
							var myDate	:Date = strDatePortabilite;
							
							if(dateRef.toString() == myDate.toString() || isNaN(myDate.date) || isNaN(myDate.month) || isNaN(myDate.fullYear))
							{
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'La_date_') +
									DateFunction.formatDateAsString(strDatePortabilite) +
									ResourceManager.getInstance().getString('M16', '_n_a_pas_un_format_valide__'), 'Consoview');
								return false;
							}
							
						}
					}
				}
			}
			
			if(myclob != "")
				getEmployFromMat(myclob.substr(0, myclob.length-1));
			
			return isOk;
		}

	}
}