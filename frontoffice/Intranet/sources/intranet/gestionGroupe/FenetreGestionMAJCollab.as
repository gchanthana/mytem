package intranet.gestionGroupe
{
	import mx.managers.PopUpManager;
	import mx.controls.Alert;
	import flash.events.MouseEvent;
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.ArrayCollection;
	import fr.consotel.consoview.util.ConsoViewErrorManager;
	import mx.rpc.events.FaultEvent;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	public class FenetreGestionMAJCollab extends FenetreGestionMajCollaborateurIHM
	{
	
  		private var listeSociete :ArrayCollection;
  		private var objGridSociete : GridSocieteAffectee;
        
        public function FenetreGestionMAJCollab(listeSociete : ArrayCollection,objGridSociete : GridSocieteAffectee)
        {
        	super();
         	this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
         	this.listeSociete = listeSociete;
         	this.objGridSociete= objGridSociete;
         	
        }
        public function initIHM(event:Event) : void
        {
        	MAJ_AUTO_COLLABORATEUR.labelFunction = setTypeAffichage;
        	btCancel.addEventListener(MouseEvent.CLICK,removeMe);    
            btValide.addEventListener(MouseEvent.CLICK, submitData);
            dgSociete.dataProvider=listeSociete;
       }
       private function setTypeAffichage(item : Object, co : DataGridColumn):String{
			
			return (item[co.dataField]==1)?"Oui":"Non";
				
		}
       private function submitData(event:MouseEvent):void {
       	if(dgSociete.selectedItems !=null && dgSociete.selectedItems.length>0)
				{
					var p_maj_auto : int;
					if(btRadioNon.selected==true){
						p_maj_auto = 0;
					}
					else
					{
						p_maj_auto = 1;	
					} 					
					
					var tab :Array = dgSociete.selectedItems;
						var op2:AbstractOperation =	RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoprod.intranet.procedureGestionGroupeSociete",
																		"upd_maj_auto_collaborateur",
																		processMAJCollab,remoteError);
						RemoteObjectUtil.callService(op2,tab,p_maj_auto);
						//Alert.show("-->"+tab[1].IDREF_CLIENT+"--val de p-->"+p_maj_auto);
				}
				else
				{
					Alert.show("Selectionnez un groupe maitre");
				}	 
       }
       private function processMAJCollab(e : Event):void{
       	Alert.show("Enregistrement : OK");
       	var id : int = objGridSociete.getIdRacine();
       	objGridSociete.getListeSocieteAffectee(id);
        PopUpManager.removePopUp(this);
       	
       }
	   private function removeMe(event:MouseEvent):void {
            PopUpManager.removePopUp(this);
       }
       	
		
	
		private function remoteError(faultEvent:FaultEvent):void {
			ConsoViewErrorManager.processRemotingFaultEvent(faultEvent);
		}
	}
}