package service.selecteur.chartselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ChartSelectorHandler
	{
		private var _model: ChartSelectorModel;
		
		public function ChartSelectorHandler(_model:ChartSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValueChartHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValues(evt.result as ArrayCollection);
			}
		}	
	}
}