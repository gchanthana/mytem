package gestionparc.event
{
	import flash.events.Event;

	public class FicheEvent extends Event
	{
		public static const FICHELIGNE:String = "FICHELIGNE";
		public static const FICHEINFO:String = "FICHEINFO";
		public static const FICHEPRODUIT:String = "FICHEPRODUIT";
		public static const FICHEEVOLUTION:String = "FICHEEVOLUTION";
		public static const FICHETICKET:String = "FICHETICKET";
		public static const LISTEFACTURES:String = "LISTEFACTURES";
		
		public static const FICHE_UPDATED:String="ficheUpdated";
		public static const FICHE_TERMINAL_SELECTED:String="ficheTerminalSelected";

		private var _idTerm:int;

		public function FicheEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, idTerm:int=0)
		{
			super(type, bubbles, cancelable);
			this._idTerm=idTerm;
		}

		public function get idTerm():int
		{
			return _idTerm;
		}

		override public function clone():Event
		{
			return new FicheEvent(type, bubbles, cancelable, idTerm);
		}
	}
}
