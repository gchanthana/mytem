component implements="fr.saaswedo.api.order.bridge.IOperation"   {

	property
		name = "context"
		type = "struct"
		hint = "Call object";

	property
		name = "result"
		type = "struct"
		hint = "result object";

	property
		name = "tmpSonarDir"
		type = "string"
		hint = "temp directory for the order";

	public CreateTicket function init(struct context)
	output="false"
	{
		var UUIDdirectory = createUUID();

		this.context = arguments.context;
		this.result = {};

		//create a directory in ram
		this.tmpSonarDir = "ram:///sonar/#UUIDdirectory#";
		DirectoryCreate(this.tmpSonarDir);

		updateContext();
		return( this );
	}

	public string function getDefinition()
	{
		return 'CreateTicket';
	}

	public boolean function manageResult(required struct result)
	{

		var status = (arguments.result.statuscode contains "201");

		if(status eq true)
		{
			var sonarCareTicketId = 0;

			this.result.data = parseResult(arguments.result);
			this.result.sonarCareTicketId = trim(Listlast(ReReplaceNoCase(this.result.data,'/','|','ALL'),'|','yes'));
			sonarCareTicketId = this.result.sonarCareTicketId;
			linkOrder(this.context.OrderId,sonarCareTicketId);
		}
		else
		{
			this.result.data = parseError(arguments.result);
		}


		this.result.status = arguments.result.Statuscode;

		return status;
	}

	public struct function getResult()
	{
		return this.result;
	}

	public void function purgeTemporyItems()
	{
		DirectoryDelete(this.tmpSonarDir,true);
	}



	//--- Private Methods -----------------------------------------------------------------------------

	private string function  parseResult(required struct result)
	{
		var token = tostring(arguments.result.Responseheader.Location);

		return token;

	}

	private string function  parseError(required struct result)
	{
		return tostring(arguments.result.fileContent);
	}


	private void function updateContext()
	output="false"
	{
		context = this.context;
		HttpCall = createHttpCall();
		context.HttpCall = HttpCall;
	}

	private struct function	createHttpCall()
	output="false"
	{
		HttpCall = {};
		HttpCall['url'] = this.context.endpoint & "CareTickets/";
		HttpCall['method'] = "POST";
		HttpCall['multipart'] = "yes";
		HttpCall['HttpParams'] = createHttpParams();

		return HttpCall;
	}

	private array function	createHttpParams()
	output="false"
	{

		var HttpParams = [];


		var param1 = {};
		param1['type'] = "header";
		param1['name'] = "api-key";
		param1['value'] = this.context.apiKey;

		HttpParams[1] = param1;



		var param2 = {};
		param2['type'] = "formfield";
		param2['name'] = "data";
		param2['value'] = createSonarCareTicket();
		HttpParams[2] = param2;

		HttpParams = addFileFormattedOrder(HttpParams,param2['value']);
		HttpParams = addMytemOrderFiles(HttpParams);

		return HttpParams;

	}

	private string function	createSonarCareTicket()
	output="true"
	{
		var sonarorder = new fr.saaswedo.api.order.sonar.database.GetSonarOrder();

		var cmd = sonarorder.execute(this.context.OrderId);

		var allRows = cmd['ARTICLE'].recordcount;

		var Ticket = {};
		var prefix = "::str::";
		var Issues = [];


		//Ticket
		var TicketCarrier = {};
		var TicketRequestor = {};
		var Notes = "";

		TicketCarrier['Name'] =  getCarrierName(cmd['COMMANDE']['NOMOP'][1]);
		TicketCarrier['AccountId'] =  cmd['COMMANDE'][getCarrierAccount(TicketCarrier['Name'])][1];

		WriteLog(type="INFO", file="sonar", text="[ACCOUNT INFO] Carrier : #TicketCarrier['Name']#, AccountId : #TicketCarrier['AccountId']#");


		TicketRequestor['FullName'] = prefix &  		cmd['COMMANDE']['PRENOM'][1] & ' ' & cmd['COMMANDE']['NOM'][1];
		TicketRequestor['Email'] = prefix & 			cmd['COMMANDE']['EMAIL'][1];
		Notes = 					prefix & 			cmd['COMMANDE']['COMMENTAIRES'][1];


		for (var intRow = 1;
			intRow <= allRows;
			intRow++)
		{
				var issue = new fr.saaswedo.api.order.sonar.entity.IssueMDN(cmd['ARTICLE']['IDARTICLE'][intRow],cmd);
				ArrayAppend(Issues,issue.getIssue());
		}


		Ticket['TicketCarrier'] = TicketCarrier;
		Ticket['TicketRequestor'] = TicketRequestor;
		Ticket['IssueMDNs'] = Issues;
		Ticket['Notes'] = Notes;

		jsTicketBase = SerializeJSON(Ticket);
		jsTicket = Replace(jsTicketBase,prefix,'','all');

		WriteLog(type="INFO", file="sonar", text="[CREATE_SONAR_TICKET]  SONAR JSON : #jsTicket#");


		return  jsTicket;
	}



	private array function addMytemOrderFiles(required array httpParamList)
	output="false"
	{
		var filesService = new fr.saaswedo.api.order.sonar.database.GetOrderFile();
		var FSresult = filesService.execute(this.context.OrderId);
		var qFileList = FSresult['FILELIST'];
		var allRows = qFileList.recordcount;
		var _timeout = 180; //timeout in sec

			for (	var intRow = 1;
				intRow <= allRows;
				intRow ++)
		{
			var param = {};
			var path = qFileList['PATH'][intRow];
			var filename = qFileList['FILE_NAME'][intRow];
			var file =  '/container/M16/#path#/#filename#';
			var timer = 10;

			WriteLog(type="INFO", file="sonar", text="[SONAR ADD FILE] FILE PATH : /container/M16/#path#/#filename#");
			while(!FileExists(file) && timer lte _timeout)
			{
				WriteLog(type="INFO", file="sonar", text="[SONAR ADD FILE] waiting for file creation  ...  duration : #timer# sec limit = #_timeout# sec");
				WriteLog(type="INFO", file="sonar", text="[SONAR ADD FILE] file exists : #FileExists(file)#");
				sleep(10000);//wait 10 sec
				timer = timer + 10;
			}

			if(FileExists(ExpandPath(file)))
			{
				var mimeType =  getPageContext().getServletContext().getMimeType(file);
				param['type'] = "file";
				param['name'] = filename;
				param['file'] = file;
				param['mimeType'] = mimeType;

				arrayAppend(arguments.httpParamList,param);
			}

		}

		return arguments.httpParamList;
	}

	private array function addFileFormattedOrder(required array httpParamList,required string jsOrder)
	{

		var filename = "OwnerList.txt";
		var file = "#this.tmpSonarDir#/RawRequest.txt";
		//Write the file
		FileWrite(file, arguments.jsOrder);
		var mimeType =  getPageContext().getServletContext().getMimeType(file);
		var param = {};
		param['type'] = "file";
		param['name'] = filename;
		param['file'] = file;
		param['mimeType'] = mimeType;

		arrayAppend(arguments.httpParamList,param);

		//debug
		//var owners = FileRead(file);
		//FileWrite("/Users/samuel.divioka/Documents/projects/owner_list2.txt", "#owners#",'utf-8');
		//fin debug

		return arguments.httpParamList;
	}

	private string function getCarrierName(required string MytemCarrierName)
	{
	  var SonarCarrierList = 'Sprint,AT&T,Verizon';
	  for(var carrier in listToArray(SonarCarrierList))
	  {

	    var result = FindNoCase(carrier,arguments.MytemCarrierName);

	    if(result gt 0)
	    {
	      return carrier;
	    }


	  }

	  return  "#arguments.MytemCarrierName# not found";

	}

	private string function getCarrierAccount(required string SonarCarrierName)
	{
		var SonarCarrierList = 'Sprint|COMPTE_FACTURATION,AT&T|SOUS_COMPTE,Verizon|COMPTE_FACTURATION';
		var index = listContainsNoCase(SonarCarrierList,arguments.SonarCarrierName,',');

		if(index > 0)
		{
			return listToArray(toSTring(ListGetAt(SonarCarrierList,index,',')),'|')[2];
		}
		else
		{
			return "COMPTE_FACTURATION";
		}

	}

	private void function linkOrder(required numeric MytemOrderId, required string SonarOrderId)
	{
		var OrderLinker = new fr.saaswedo.api.order.sonar.database.LinkOrder();
		var result = OrderLinker.execute(arguments.MytemOrderId, arguments.SonarOrderId);
		WriteLog(type="INFO", file="sonar", text="[LINKTICKET] MYTEMID : #arguments.MytemOrderId#, SONARID : #arguments.SonarOrderId#");

	}


}
