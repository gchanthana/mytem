package composants.tb.graph
{
	import composants.tb.graph.charts.PieGraph_IHM;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	 
	import mx.events.CloseEvent;
	import mx.controls.Alert;
	import flash.events.Event;
	import mx.charts.HitData;
	import flash.profiler.heapDump;
	import mx.rpc.events.HeaderEvent;
	import mx.containers.TitleWindow;
	import flash.display.InteractiveObject;
	import mx.controls.Image;
	import flash.events.MouseEvent;
	
	public class GraphDureeConso extends PieGraphBase
	{
		private var op : AbstractOperation;
		
		[Embed(source="/assets/images/suivant.gif")]
        private var icnSuivant:Class;

		
		[Bindable]
		private var _dataProviders : ArrayCollection;
		
		[Bindable]
		private var _dataProviders2 : ArrayCollection;
		
		
		
		private var _segment : String;
		
		public function GraphDureeConso(segment : String = "Complet")
		{
			//TODO: implement function
			super();
			setSegment(segment);
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
				
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		override public function clean():void{		
			graph.myPie.dataProvider = null;
			myGrid.dataProvider = null;
			
		}
				
		/**
		* Permet de passer les valeurs au composant. 
		* @param d Une collection de tableau. 
		**/						
		override public function set dataProviders(d : ArrayCollection):void{
			
			_dataProviders = _formatterDataProvider(d);	
			_dataProviders2 = _formatterDataProviderPie(d);		
			updateProviders();
			
		}
		
		/**
		*  Retourne le dataproviders
		**/	
		[Bindable]			
		override public function get dataProviders():ArrayCollection{
			return _dataProviders;
		}	
		
		/**
		 * Met à jour les donées du tableau
		 **/
		override public function update():void{			
			if (_segment.toUpperCase()=="COMPLET") {
				chargerDonnees(); 	
			} else {
				chargerDonneesBySegment(); 	
			}
			
		}	
		
		/*---------- protected --------------------*/
		protected override function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			
			_total = 0;
			for	(ligne in d){					
					tmpCollection.addItem(formateObject(d[ligne]));					   
			}
			
			var totalObject : Object = new Object();
			totalObject["Opérateur"] = "TOTAL";
			totalObject["Temps Consommé (min)"] = nf.format(_total);
			tmpCollection.addItem(totalObject);			
						
			return tmpCollection;
		}	
		
		protected function _formatterDataProviderPie(d: ArrayCollection):ArrayCollection{		
			
			var tmpCollection : ArrayCollection = new ArrayCollection();	
			var ligne : Object;				
			var len : int = d.length;
			var obj : Object = new Object();
			var chartData : ArrayCollection = new ArrayCollection();
			
						
			_total = 0;
			
			//////////////////formatage des données
			for	(ligne in d){		
				tmpCollection.addItem(formateObjectPie(d[ligne]));					   
			}
			/////////////////
			chartData = ObjectUtil.copy(tmpCollection) as ArrayCollection;
			
			/////////////////On coupe s'il y a trop de données
			if (len > 10){
				var sum : Number  = 0;
			
				for (var i : int = 9 ; i < len; i++){
					obj = chartData.getItemAt(i);
					sum = sum + parseFloat(obj["Montant"]);					
				}
				
				var len2 : int =  chartData.length ;
				
				for (var j : int = 10; j < len2 ; j++){
					chartData.removeItemAt(10);
				} 	
										
				chartData.getItemAt(9)["Libellé"] = "Autres...";
				chartData.getItemAt(9)["Montant"] = sum;					
				
				 return chartData;
			}
			
			
			
			////////////////// si la somme des montant est null on retourne rien
			if(_total == 0){
				return null;
			}
			
			////////
			return tmpCollection;
		}	
				
		protected function formateObject(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total + parseFloat(obj.duree_appel);		
	
			o["Opérateur"] = obj.nom;
			o["Temps Consommé (min)"]= nf.format(obj.duree_appel);											
			return o;
		}
		
		protected function formateObjectPie(obj : Object):Object{
			var o : Object = new Object();
				
			_total = _total + parseFloat(obj.duree_appel);		
	
			o["Libellé"] = obj.nom;
			o["Montant"]= obj.duree_appel;											
			return o;
		}
		
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected override function chargerDonnees():void{
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getRepartDuree",
																			chargerDonneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op);
			
		}	
		
		// Chargement des données par remoting
		protected function chargerDonneesBySegment():void{
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getRepartDureeBySegment",
																			chargerDonneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op,_segment);
			
		}	
		
		
		protected override function chargerDonneesResultHandler(re :ResultEvent):void{			 		 		 
		  		dataProviders =  re.result as ArrayCollection;			  		
		  		   
		}	
		
		//formate les tootips du graph des themes
		override protected function formatDataTip(obj:HitData):String{
		    var libelle :String = obj.item["Libellé"]; 
		    var montant:Number = obj.item["Montant"];
		  	var prop : Number = (montant *  100 / _total);  
		  	
   	  
	    	return "Opérateur : <b>"+libelle+
	    	"</b><br>Durée : <b><font color ='#ff0000'>"+nf.format(montant)+" min.</font></b><br>Proportion :<b>"+prop.toPrecision(4)+"%</b>";;
		    
		}		
		//--------------------------------------------------------------------------------------				
		private function init(fe : FlexEvent):void{
			title = "... / Répartition des durées des consommations";
			//showCloseButton = true;
		 	
		 	//Image(titleIcon = icnSuivant).addEventListener(MouseEvent.CLICK,ss);
		 	titleBar.enabled = true;	
		 	titleBar.addEventListener(MouseEvent.CLICK,closeIt);	
		 	titleBar.useHandCursor = true;
		 	titleBar.buttonMode = true;		 
		 	titleBar.mouseChildren = false;			
			//addEventListener(CloseEvent.CLOSE,closeIt);
						
		}
		
		private function closeIt(ev :Event):void{
			dispatchEvent(new Event("switchGraphEvent"));			
		}
		
		private function updateProviders():void{
			myGrid.dataProvider = dataProviders;			
			graph.callLater(updatePie);
		}
		
		private function updatePie():void{
			graph.myPie.dataProvider = _dataProviders2;
		}
		
		private function setSegment(segment : String):void{
			if (segment == "null")
				_segment = "Complet";
			else
				_segment = segment;
		};
			
	}
}