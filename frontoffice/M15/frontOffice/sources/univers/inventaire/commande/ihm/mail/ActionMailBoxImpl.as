package univers.inventaire.commande.ihm.mail
{
	import composants.mail.MailBoxImpl;
	import composants.util.CvDateChooser;
	
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.commande.ihm.SelectionContactIHM;
	import univers.inventaire.commande.system.Action;
	import univers.inventaire.commande.system.Commande;
	import univers.inventaire.equipements.revendeurs.ihm.PopUpImpl;
	import univers.inventaire.equipements.revendeurs.system.Agence;

	public class ActionMailBoxImpl extends MailBoxImpl
	{
		public function ActionMailBoxImpl()
		{
			super();
		}
		
		/**
	     * Référence vers une autre popUp
	     */
	    protected var _popUp: PopUpImpl;
	    
		public var selectedAction:Action;
	    /**
	     * La date de l'action
	     */
	    public var dcDateAction: CvDateChooser;
	    
	    /**
	     * Un commentaire pour l'action.
	     */
	    public var txtCommentaire: TextArea;
	    
	   	public var envoyerMail:Boolean;
	   	
	   	
	   	
	   	public var defaultSelectedDate:Date = new Date();		
		
	/* 	private var _commande:Commande;
		public function get commande():Commande
		{
			return _commande
		}
		public function set commande(value:Commande):void
		{
			_commande = value
		} */
		
		/**
	     * Si le mode écriture est à false alors on grise tous les boutons de
	     * modifications (Enregistrer, editer ...)
	     */
	    private var _modeEcriture: Boolean = true;
	    
	    /**
	     * setter pour _modeEcriture
	     * 
	     * @param mode
	     */
	    public function set modeEcriture(mode:Boolean): void
	    {
	    	_modeEcriture = mode;
	    }

	    /**
	     * getter pour _modeEcriture
	     */
	    public function get modeEcriture(): Boolean
	    {
	    	return _modeEcriture;		
	    }
	    
		
		private var _selectableRange:Object = {rangeStart:new Date()};
		public function get selectableRange():Object
		{			
			return _selectableRange
		}
		
		public function set selectableRange(value:Object):void
		{
			_selectableRange = value;
			
			var diff:int = ObjectUtil.dateCompare(_selectableRange.rangeStart as Date,new Date());
		 
			if(diff >= 0)
			{
				defaultSelectedDate = _selectableRange.rangeStart;
			}
			else
			{
				defaultSelectedDate = new Date();
			}
			
		}
	    
	    protected function removePopUp():void
		{
			if(_popUp != null)
			{
				
				if(_popUp.isPopUp)
				{
					PopUpManager.removePopUp(_popUp)
				}
				
				_popUp = null;
			}
		}
		
		
		/**
	     * Pour enregistrer l'action � la date s�lectionn�e.
	     * 
	     * @param event
	     */
	    override protected function btnEnvoyerClickHandler(event:MouseEvent): void
	    {
	    	if(_mail.destinataire!="")
	    	{
	    		super.btnEnvoyerClickHandler(event);	
	    	}
	    	else
	    	{
	    		Alert.buttonWidth = 100;
	    		Alert.show("Vous n'avez pas choisi de destinataire pour ce revendeur\n");
	    	}
	    }
	    
	    
	    
	    override protected function imgContactsClickHandler(event:MouseEvent):void
		{	
			removePopUp();
	    	
	    	if(_popUp != null) _popUp = null;			
			_popUp = new SelectionContactIHM();
			//_popUp.modeEcriture = modeEcriture;
			SelectionContactIHM(_popUp).commande = infosObject.commande;
			var agence:Agence = new Agence();
			agence.IDCDE_CONTACT_SOCIETE = infosObject.commande.IDSOCIETE;
			SelectionContactIHM(_popUp).gestionContact.agence = agence;			
			PopUpManager.addPopUp(_popUp,this,true);
			PopUpManager.centerPopUp(_popUp);
	    }
		
	}
}