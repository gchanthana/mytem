package gestionparc.ihm.action.equipement.actionEqpAuRebut
{
	import flash.display.DisplayObject;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.equipement.FicheEqpMettreAuRebusIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionEqpAuRebutReceiver
	{
		private var fiche:FicheEqpMettreAuRebusIHM;

		public function ActionEqpAuRebutReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			var fiche:FicheEqpMettreAuRebusIHM=new FicheEqpMettreAuRebusIHM();
			fiche.ligneSelected=SpecificationVO.getInstance().elementDataGridSelected;
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
