package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	[Bindable]
	public class FicheLigneService
	{
		
		public var model:FicheLigneModel;
		public var handler:FicheLigneHandler;
		
		public function FicheLigneService()
		{
			model = new FicheLigneModel();
			handler = new FicheLigneHandler(model);
		}
		
		public function getFicheLigne(idSousTete:Number):void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetFicheLigne",
				"getFicheLigne",
				handler.getFicheLigneHandler);
			RemoteObjectUtil.callService(opData,idSousTete);
			
		}
		
		public function getLibellePerso():void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetFicheLigne",
				"getLibellePerso",
				handler.getLibellePersoHandler);
			RemoteObjectUtil.callService(opData);
			
		}
		
	}
}