package gestionparc.utils
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;

	[Bindable]
	public class gestionparcConstantes
	{

		public static const ABO_VOIX:int=41; //	Abo Voix
		public static const ABO_DATA:int=54; //	Abo Data
		public static const OPT_VOIX:int=56; //	Options Voix
		public static const OPT_DATA:int=49; //	Options Data
		public static const OPT_PUSH:int=65; //	Abo Push Mail
		public static const OPT_DIV:int=52; //	Options Diverses

		public static const LA:String="LA";
		public static const MOBILE:String="MOB";
		public static const GSM:String="GSM";
		public static const T0:String="T0";
		public static const T2:String="T2";
		public static const LL:String="LL";
		public static const LS:String="LS";
		public static const XDSL:String="xDSL";
		public static const SER:String="SER";
		public static const AUT:String="AUT";

		public static const COLLABORATEUR:String="collab";
		public static const TERMINAL:String="term";
		public static const LIGNE:String="ligne";
		public static const SIM:String="sim";
		public static const DIVERS:String="divers";
		public static const SITE:String="site";
		public static const INCIDENT:String="incident";
		public static const ABOOPT:String="aboOpt";
		public static const OPERATEUR:String="operateur";

		public static const listAction:ArrayCollection=new ArrayCollection([{label: ResourceManager.getInstance().getString('M111', 'Exporter_les__l_ments_en_csv'), data: 3}, 
																			{label: ResourceManager.getInstance().getString('M111', 'Exporter_tout_les__l_ments_en_csv'), data: 6},
																			{label: ResourceManager.getInstance().getString('M111', 'Exclure_les__l_ments_du_pool'), data: 4}, 
																			{label: ResourceManager.getInstance().getString('M111', 'Demande_de_changement_de_compte_op_rateu'), data: 5}]);

		public static const DATAPROVIDER_CBODANS:ArrayCollection=new ArrayCollection([{LIBELLE: ResourceManager.getInstance().getString('M111', 'Collaborateur'), CODE: "COLLAB", SORT_FIELD: "COLLABORATEUR", SORT_ORDER:"ASC"}, 
																					  {LIBELLE: ResourceManager.getInstance().getString('M111', 'Matricule'), CODE: "MATRICULE", SORT_FIELD: "E_MATRICULE", SORT_ORDER:"ASC"}, 
																					  {LIBELLE: ResourceManager.getInstance().getString('M111', 'Ligne'), CODE: "LIGNE", SORT_FIELD: "SOUS_TETE", SORT_ORDER:"ASC"}, 
																					  {LIBELLE: ResourceManager.getInstance().getString('M111', 'IMEI'), CODE: "IMEI", SORT_FIELD: "T_IMEI", SORT_ORDER:"ASC"}, 
																					  {LIBELLE: ResourceManager.getInstance().getString('M111', 'N__S_rie'), CODE: "NUM_SERIE", SORT_FIELD: "T_NO_SERIE", SORT_ORDER:"ASC"}, 
																					  {LIBELLE: ResourceManager.getInstance().getString('M111', 'Mod_le'), CODE: "MODELE", SORT_FIELD: "T_MODELE", SORT_ORDER:"ASC"}, 
																					  {LIBELLE: ResourceManager.getInstance().getString('M111', 'Carte_SIM'), CODE: "SIM", SORT_FIELD: "S_IMEI", SORT_ORDER:"ASC"}, 
																					  {LIBELLE: ResourceManager.getInstance().getString('M111', 'Extension'), CODE: "EXT", SORT_FIELD: "S_IMEI", SORT_ORDER:"ASC"}]);

		[Embed(source="/assets/equipement/User_3.gif")]
		public static var adrImg:Class;
		[Embed(source="/assets/equipement/User.gif")]
		public static var adrImgEmp:Class;
		[Embed(source="/assets/equipement/SIM50x50.png")]
		public static var adrImgSIM:Class;
		[Embed(source="/assets/equipement/06-50x50BIG.png")]
		public static var adrImgLigne:Class;
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")]
		public static var adrImgTerm:Class;
		[Embed(source="/assets/equipement/ext.jpg")]
		public static var adrImgExt:Class;

		public static const LIST_GPE_MENU_COLLAB:Array=[ResourceManager.getInstance().getString('M111', 'FOURNIR'), ResourceManager.getInstance().getString('M111', 'REPRENDRE'), ResourceManager.getInstance().getString('M111', 'COLLABORATEUR')];

	}
}
