package univers.inventaire.inventaire.creation.nouvelleResources.commande.contacts
{
	import composants.mail.MailBoxIHM;
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import univers.inventaire.inventaire.etapes.OperationVO;
	import univers.inventaire.inventaire.mail.EnvoiDeMailIHM;

 	
 	
 	/**
 	 * Classe gerant l'affichage du contact
 	 * */
	public class ConsulterContact extends Canvas
	{	
		
		/**
		 * Refference vers le TextInput affichant le contact
		 * */
		public var txtContact : TextInput;		
		
		/**
		 * Refference vers le TextInput affichant la société (Agence)
		 * */
		public var txtSociete : TextInput;
		
		/**
		 * Refference vers le TextInput affichant l'opérateur pour la commande
		 * */
		public var txtOperateur : TextInput;
		
		/**
		 * Refference vers le TextInput affichant le distributeur (Agence)
		 * */
		public var txtDistrib : TextInput;
		
		/**
		 * Refference vers le Label affichant la date d'envoi du mail
		 * */
		public var txtMail : Label;
		
		/**
		 * Refference vers le Label affichant le titre 
		 * */		
		public var lblTitre : Label;
		
		
		/**		 
		 * Refference vers le bouton renvoyer mail 
		 * */
		 public var btRenvoyer : Button;
		 
		/**
		 *  
		 * */
		[Bindable]
		public var gap : int = 2;
		
		
		[Bindable]
		[Inspectable(defaultValue="true",enumeration="true,false")]
		public var OperateurVisible : Boolean = true;		
		
		[Bindable]
		public var spacerHeight : int = 5;
		
		//reference locale vers l'operation
		private var _operation : OperationVO;
		
		//reference locale vers la commande
		private var _commande : Commande;
		
		//reference locale vers le panier
		private var _panier : ElementCommande;
		
		
		//reference locale vers la liste des produits à résilier
		private var _listeProduitsOperation : ArrayCollection;
		
		
		//reference locale ves le contact 
		private var _contact : Contact;
		
		//reference locale vers l'Agence
		private var _societe : Societe;
		
		//reference locale vers le distributeur
		private var _distrib : Societe;
		
		//reference locale vers l'opérateur
		private var _operateur : OperateurVO;
		
		//reference locale vers la fentre d'envoi de mail
		private var _mailWindow : EnvoiDeMailIHM;
		
		//Booleen placé à true si on affiche un chiffre deva,t le titre
		private var _displayNumber : Boolean = true;		
				
		//Le titre par defaut au format HTML
		[Bindable]
		protected var htmltxt : String = "<font color='#008040' size='15'>2</font><font color='#000000' size='10'>. Contact chez le distributeur</font>";
		
		public function get modeEcriture():Boolean{
			return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
		}
		
		/**
		 * constructeur
		 * */
		public function ConsulterContact()
		{			
			super();
		}
		
		/**
		 * supprime la fenêtre au logoff
		 * (fait le menage)
		 * */
		public function logOff():void{
			if (_mailWindow != null) _mailWindow.logOff(); 
		}
		
		
		/**
		 * Setteru pour l'opérateur
		 * @param OperateurVO
		 * */
		public function set operateur(op : OperateurVO):void{
			_operateur = op;
		}
		
		/**
		 * Setter pour la propriété displayNumber (pour afficher le titre avec ou sans numero au début)
		 * @param boolValue si égal à true on affiche le numéro, si égal à false on affiche pas le numéro
		 * */
		[Inspectable(defaultValue="true",enumeration="true,false")]
		public function set displayNumber(boolValue : Boolean):void{
			_displayNumber = boolValue;
			if (_displayNumber)
				htmltxt = "<font color='#008040' size='15'>2</font><font color='#000000' size='10'>. Contact chez le distributeur</font>"
			else
				htmltxt = "<font color='#000000' size='10'>Contact chez le distributeur</font>";			
		}		
		
		/**
		 * Retourne la valeur de displayNumber
		 * @return bool
		 * */
		public function get displayNumber():Boolean{
			return _displayNumber;
		}
		
		
		/**
		 * affect l'id de l'operation pour les mail
		 * @param Number l'identifiant de l'opération
		 * */
		
		[Bindable]
		public function get operation():OperationVO{
			return _operation;
		}
		public function set operation(op : OperationVO):void{
			_operation = op;
		}
		
		[Bindable]
		public function get listeProduitsOperation():ArrayCollection{
			return _listeProduitsOperation;
		}
		public function set listeProduitsOperation(values : ArrayCollection):void{
			_listeProduitsOperation = values;
		}
		
		
		/**
		 * Affecte le contact
		 * @param idContact l'identifiant du contact que l'on veut afficher
		 * */
		public function setContacts(idContact : int = 0):void{
			if (idContact > 0){
				_contact = new Contact(idContact);
				_contact.addEventListener(Contact.CREATION_COMPLETE,afficherContact);
			}else{
				txtContact.text = "-";				
			}
		}	
		
		/**
		 * Affecte la societe
		 * @param idSociete l'identifiant de la societe que l'on veut afficher
		 * */
		public function setSociete(idSociete : int = 0):void{
			if (idSociete > 0){
				_societe = new Societe(idSociete);
				_societe.addEventListener(Societe.CREATION_COMPLETE,afficherSociete);
			}else{
				txtSociete.text = "-";
				txtDistrib.text = "-";	
			}
		}
		
		public function afficherContactOperation(ope : OperationVO, listeProduits : ArrayCollection):void{
			operation = ope;
			listeProduitsOperation = listeProduits;
			
			if (_operation.FLAG_MAIL > 0){
				
				txtMail.text = "Un mail à été envoyé le " + DateFunction.formatDateAsString(new Date(_commande.dateEnvoi));
				btRenvoyer.label = "Renvoyer";
				btRenvoyer.toolTip = "Renvoyer le mail.\nPour avoir un aperçu du mail, clickez sur le bouton PDF";
				//btRenvoyer.visible = true && modeEcriture;
								
			}else{
									
				btRenvoyer.label = "Envoyer";							
				btRenvoyer.toolTip = "Envoyer un mail à l'opérateur.";
				btRenvoyer.visible = true && modeEcriture;
				txtMail.text = "Envoyer un mail à l'opérateur";
				//txtMail.visible = true && modeEcriture;
			}		
				
			btRenvoyer.addEventListener(MouseEvent.CLICK,btMailResiliationClickHandler)
			
			setContacts(operation.IDCDE_CONTACT);
			setSociete(operation.IDCDE_CONTACT_SOCIETE);
		};
		
		/**
		 * Affiche le contact de la commande
		 * @param cmde la commande 
		 * */
		public function afficherContactCommande(cmde : Commande, panier : ElementCommande = null):void{
			_commande = cmde;	
			_panier = panier;
			
			if (_commande.flagMail > 0){
				
				txtMail.text = "Un mail à été envoyé le " + DateFunction.formatDateAsString(new Date(_commande.dateEnvoi));
				btRenvoyer.label = "Renvoyer";
				btRenvoyer.toolTip = "Renvoyer le mail.\nPour avoir un aperçu du mail, clickez sur le bouton PDF";
				btRenvoyer.visible = true && modeEcriture;
								
			}else{
				
				btRenvoyer.label = "Envoyer";							
				btRenvoyer.toolTip = "Envoyer un mail à l'opérateur.";
				btRenvoyer.visible = true && modeEcriture;
				txtMail.text = "Envoyer un mail à l'opérateur";
				txtMail.visible = true && modeEcriture;
			}
			
			btRenvoyer.addEventListener(MouseEvent.CLICK,btMailCommandeClickHandler)
						
			_contact = new Contact(_commande.contactID);
			_contact.addEventListener(Contact.CREATION_COMPLETE,afficherContact);			
			_societe = new Societe( _commande.societeID);
			_societe.addEventListener(Societe.CREATION_COMPLETE,afficherSociete);						
			
			txtOperateur.text = _commande.operateurNom;			
		}	
		
		/**
		 * Affiche le bouton mail
		 * */
		public function afficherMail():void{			
			btRenvoyer.visible = true && modeEcriture;
			btRenvoyer.label = "Mail";
		}
		
		
		/**
		 * Retourne la société de la commande
		 * */
		public function getSociete():Societe{
			if (_societe.societeID > 0 ) return _societe;
			else return _distrib;
		}	
		
		/**
		 * Retourne le contatc de la Commande
		 * */
		public function getConatct():Contact{
			return _contact;
		}
		
		//Affiche la société
		private function afficherSociete(ev: Event):void{
			if (_societe.societeParenteID > 0){
				
				_distrib = new Societe(_societe.societeParenteID);
				_distrib.addEventListener(Societe.CREATION_COMPLETE,afficherDistrib);					
			}else{
				txtDistrib.text = _societe.raisonSociale;
				txtSociete.text = "";				
			}
		}
		
		//Affiche l'agence et le distributeur
		private function afficherDistrib(ev : Event):void{
			txtDistrib.text = _distrib.raisonSociale;
			txtSociete.text = _societe.raisonSociale;	
		}
		
		
		//Affiche le contact civilite prenom nom		
		private function afficherContact(ev : Event):void{
			txtContact.text = _contact.civilite + " " + _contact.prenom + " "+ _contact.nom;
		}
		
		//gere le click sur le bouton Mail
		//Ouvre une fentre Gestioncontact
		private function _btMailClickHandler(me : MouseEvent):void{
			//-- TO DO --- ouvrir une boite de dialogue pour envoyer des mail
			_mailWindow = new EnvoiDeMailIHM();			
			_mailWindow.contact = _contact;
			_mailWindow.agence = _societe;
			_mailWindow.distributeur = _distrib;
			_mailWindow.operateur = _operateur;
			
			if (_commande){
				_mailWindow.commande = _commande;				
			}else{
				_mailWindow.operation = _operation;
			}
			
			ConsoviewUtil.scale(DisplayObject(_mailWindow),moduleCommandeMobileIHM.W_ratio,moduleCommandeMobileIHM.H_ratio);			
			PopUpManager.addPopUp(_mailWindow,UIComponent(parentApplication),true);
			PopUpManager.centerPopUp(_mailWindow);
		}
		
		
		//------MAIL 2 -----------------------------///
		private var _infosMail : InfosObject;
		private var _cpMail : MailBoxIHM;
		
		protected function btMailCommandeClickHandler(event : MouseEvent):void{
			if (_cpMail != null){
				if(_cpMail.hasEventListener(MailBoxImpl.MAIL_ENVOYE))_cpMail.removeEventListener(MailBoxImpl.MAIL_ENVOYE,cpMailBoxMailEnvoyeHandler);
				 _cpMail = null;
			}
			if (_infosMail != null) _infosMail = null;
			
			_infosMail = new InfosObject();
			_infosMail.MAIL_EXPEDITEUR = CvAccessManager.getSession().USER.EMAIL;
			_infosMail.NOM_EXPEDITEUR = CvAccessManager.getSession().USER.NOM;
			_infosMail.PRENOM_EXPEDITEUR = CvAccessManager.getSession().USER.PRENOM;
			_infosMail.SOCIETE_EXPEDITEUR = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
			
			_infosMail.commande = _commande;
			_infosMail.panier = _panier;
			
			_infosMail.contact = _contact;
			_infosMail.societe = _societe; 
			
			if (listeProduitsOperation.length > 0){
				_infosMail.compte = listeProduitsOperation[0].COMPTE;
				_infosMail.sousCompte = listeProduitsOperation[0].SOUS_COMPTE;
			}else{
				_infosMail.compte = " ";
				_infosMail.sousCompte = " ";
			}
						
			_cpMail = new MailBoxIHM();
			/* _cpMail.contact.contactID = _commande.contactID; */
			
			if(_contact != null){
				_cpMail.initMail("WorkFlow","Commande",_contact.email);
			}else{
				_cpMail.initMail("WorkFlow","Commande","----");
			}
			
			_cpMail.configRteMessage(_infosMail,GabaritFactory.TYPE_COMMANDE_FIXEDATE);
			
			_cpMail.addEventListener(MailBoxImpl.MAIL_ENVOYE,cpMailBoxMailEnvoyeHandler);
			
			PopUpManager.addPopUp(_cpMail,this,true);					
			PopUpManager.centerPopUp(_cpMail);
		}
		
		
		
		protected function btMailResiliationClickHandler(event : MouseEvent):void{
			if (_cpMail != null){
				if(_cpMail.hasEventListener(MailBoxImpl.MAIL_ENVOYE))_cpMail.removeEventListener(MailBoxImpl.MAIL_ENVOYE,cpMailBoxMailEnvoyeHandler);
				 _cpMail = null;
			}
			if (_infosMail != null) _infosMail = null;
			
			_infosMail = new InfosObject();
			_infosMail.MAIL_EXPEDITEUR = CvAccessManager.getSession().USER.EMAIL;
			_infosMail.NOM_EXPEDITEUR = CvAccessManager.getSession().USER.NOM;
			_infosMail.PRENOM_EXPEDITEUR = CvAccessManager.getSession().USER.PRENOM;
			_infosMail.SOCIETE_EXPEDITEUR = CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE;
			
			_infosMail.operation = operation;
			_infosMail.listeProduitsOperation = listeProduitsOperation;
			
			_infosMail.contact = _contact;
			_infosMail.societe = _societe; 
						
			_cpMail = new MailBoxIHM();
			_cpMail.contact.contactID = _operation.IDCDE_CONTACT;
			
			
			if (listeProduitsOperation.length > 0){
				_infosMail.compte = listeProduitsOperation[0].COMPTE;
				_infosMail.sousCompte = listeProduitsOperation[0].SOUS_COMPTE;
			}else{
				_infosMail.compte = " ";
				_infosMail.sousCompte = " ";
			}
			
			if(_contact != null){
				_cpMail.initMail("WorkFlow","Demande de résiliation",_contact.email);
			}else{
				_cpMail.initMail("WorkFlow","Demande de résiliation","----");
			}
			
			
			
			_cpMail.configRteMessage(_infosMail,GabaritFactory.TYPE_RESILIATION_FIXEDATE);
			
			_cpMail.addEventListener(MailBoxImpl.MAIL_ENVOYE,cpMailBoxMailEnvoyeHandler);
			
			PopUpManager.addPopUp(_cpMail,this,true);					
			PopUpManager.centerPopUp(_cpMail);
		}
				
		protected function cpMailBoxMailEnvoyeHandler(event : Event):void{
			var boolContactOK : Boolean = false;
			if (_commande != null){
				_commande.contactID = _cpMail.contact.contactID;
				_commande.societeID = _cpMail.contact.societeID;
				
				_contact = new Contact(_commande.contactID);
				_contact.addEventListener(Contact.CREATION_COMPLETE,afficherContact);			
				_societe = new Societe( _commande.societeID);
				_societe.addEventListener(Societe.CREATION_COMPLETE,afficherSociete);
				
				_commande.updateCommande();	
				
				
			}
			
			if (operation != null){
				operation.IDCDE_CONTACT = _cpMail.contact.contactID;
				operation.IDCDE_CONTACT_SOCIETE = _cpMail.contact.societeID;
				if (!boolContactOK){
					_contact = new Contact(_operation.IDCDE_CONTACT);
					_contact.addEventListener(Contact.CREATION_COMPLETE,afficherContact);			
					_societe = new Societe( _operation.IDCDE_CONTACT_SOCIETE);
					_societe.addEventListener(Societe.CREATION_COMPLETE,afficherSociete);
				}
				operation.updateContact();				
			}
						
		}
		
	}
}