package gestionparc.ihm.action.mdm.actionDeverouillerTerminal
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDeverrouillerTerminalCommand implements ICommand
	{
		private var receiver:ActionDeverrouillerTerminalReceiver=null;

		public function ActionDeverrouillerTerminalCommand(rec:ActionDeverrouillerTerminalReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
