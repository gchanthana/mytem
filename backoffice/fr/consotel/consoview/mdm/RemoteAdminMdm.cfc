<cfcomponent author="Cedric" displayName="fr.consotel.consoview.mdm.RemoteAdminMdm" extends="fr.saaswedo.api.myTEM.mdm.AdminMdm"
hint="Extension utilisée par le script updateMdmInstances.cfm pour la mise à jour de l'application des BackOffice MyTEM">
	<!--- Le constructeur init() est explicitement appelé à chaque instanciation --->
	<cfset init()>
	
	<cffunction access="remote" name="updateMdmApplication" returntype="void" description="LOCK:Exclusive" output="true"
	hint="Appelle SUPER.clearIMdmProviders() : Affiche une réponse texte au format JSON contenant. Codes HTTP: 200 (Succès),500 (Erreur)">
		<cflock scope="APPLICATION" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<!--- Code HTTP de la réponse --->
			<cfset var httpCode=200>
			<cfset var applicationName="N.D">
			<cfif structKeyExists(APPLICATION,"applicationName")>
				<cfset applicationName=APPLICATION.applicationName>
			</cfif>
			<cfset var resultObject={BACKOFFICE=CGI.SERVER_NAME,applicationName=applicationName,REMOTE_HOST=CGI.REMOTE_HOST,EXCEPTION={}}>
			<cftry>
				<cfset SUPER.clearIMdmProviders()>
				<cfcatch type="Any">
					<cfset var errorCode=structKeyExists(CFCATCH,"errorCode") ? CFCATCH.errorCode:"">
					<cfset var tagContext=[]>
					<cfset var cfCatchTagContext=CFCATCH.tagContext>
					<cfloop index="i" from="1" to="#arrayLen(cfCatchTagContext)#">
						<cfset arrayAppend(tagContext,{
							TEMPLATE=cfCatchTagContext[i]["TEMPLATE"],LINE=VAL(cfCatchTagContext[i]["LINE"]),COLUMN=VAL(cfCatchTagContext[i]["COLUMN"])
						})>
					</cfloop>
					<cfset resultObject.EXCEPTION={
						TYPE=CFCATCH.TYPE,ERRORCODE=errorCode,MESSAGE=CFCATCH.message,DETAIL=CFCATCH.detail,TAGCONTEXT=tagContext
					}>
					<cfset var httpCode=500>
					<cfset var log=GLOBALS().CONST('Remoting.LOG')>
					<cflog type="information" text="[#log#]#componentName()# Update MDM Application Error : #CFCATCH.message# #CFCATCH.detail#">
				</cfcatch>
			</cftry>
			<!--- Content-Type de la réponse HTTP : JSON --->
			<cfheader name="Content-Type" value="application/json">
			<!--- Réponse HTTP au format JSON --->
			<cfheader statuscode="#httpCode#">
			<cfoutput>#serializeJSON(resultObject)#</cfoutput>
		</cflock>
	</cffunction>
	
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.consotel.consoview.mdm.RemoteAdminMdm">
		<cfreturn "fr.consotel.consoview.mdm.RemoteAdminMdm">
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.myTEM.mdm.Infos" description="LOCK:Exclusive"
	hint="Appelle SUPER.init() puis configure la structure du scope SESSION pour qu'elle soit compatible avec les appels à l'API MDM">
		<cfset var defaultIdGroupe=2458788>
		<!--- Paramétrage de la structure de la session --->
		<cflock scope="SESSION" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfif NOT structKeyExists(SESSION,"USER")>
				<cfset SESSION.USER={}>
			</cfif>
			<cfif NOT structKeyExists(SESSION,"PERIMETRE")>
				<cfset SESSION.PERIMETRE={ID_GROUPE=defaultIdGroupe}>
			</cfif>
			<cfif NOT structKeyExists(SESSION.PERIMETRE,"ID_GROUPE")>
				<cfset SESSION.PERIMETRE.ID_GROUPE=defaultIdGroupe>
			</cfif>
		</cflock>
		<!--- Constructeur parent --->
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset SUPER.init()>
		</cflock>
		<cfreturn THIS>
	</cffunction>
</cfcomponent>