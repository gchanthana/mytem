package composants.tb.pages
{
	import mx.utils.ObjectUtil;
	import composants.tb.tableaux.TableauChangeEvent;
	import flash.profiler.showRedrawRegions;
	import mx.controls.Alert;
	import flash.events.EventDispatcher;

	
	/**
	 * A pour vocation de creer des objets de type Page  
	 **/
	public class PageFactory extends EventDispatcher
	{
		private var segmentChangeEvent : TableauChangeEvent = new TableauChangeEvent("segmentChange");
		/**
		 * Constructeur
		 **/
		public function PageFactory(){
			//init
		}
				
		/**
		 * Retourne un Objet Page suivant le type passé en paramètre
		 * @param type Le type de page à créer		  
		 * @throw PageNotFoundError la page n'existe pas ou n'a pu êtres créée.
		 **/
		public function createPage( type : TableauChangeEvent):IPage{				
			if (type != null){	
				
				switch (type.TYPE.toUpperCase()){
					case "ACCUEIL" : 
					{												
						return new TbAccueil(type);
						break;
					}
					
					case "SEGMENT" : {						 
						//segmentChangeEvent.SEGMENT = type.SEGMENT;
						//dispatchEvent(segmentChangeEvent);
						return new TbAccueilSegment(type);
						break;
					}
					//
					case "SURTHEME" : return new TbSurTheme(type);break;
					case "THEME" : return new TbTheme(type);break;
					case "PRODUIT" : return new TbProduit(type);break;
					//
					default : throw new PageNotFoundError();break;
				}	
				
			}else{
				
				throw new PageNotFoundError();
			}
			return null;						
		}		
	}
}