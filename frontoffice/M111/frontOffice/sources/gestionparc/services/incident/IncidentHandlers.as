package gestionparc.services.incident
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	public class IncidentHandlers
	{
		
		private var myDatas : IncidentDatas;
		
		public function IncidentHandlers(instanceOfDatas:IncidentDatas)
		{
			myDatas = instanceOfDatas;
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosIncident()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosIncident()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setInfosIncidentResultHandler(re:ResultEvent):void
		{
			
			if(re.result is Array && (re.result as Array).length > 0 && (re.result as Array)[0] > 0)
			{
				myDatas.treatSetInfosIncident(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.SAVE_INFOS_INCIDENT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>deleteIncident()</code> (appel de procédure).
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function deleteIncidentResultHandler(re:ResultEvent):void
		{
			if(re.result < 0)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DELETE_INCIDENT_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		internal function getTypeUserTypeProfileResultEvent(re:ResultEvent):void
		{
			if(re.result)
				myDatas.userProfil(re.result as int);
			else
				ConsoviewAlert.afficherError(gestionparcMessages.RECUP_TYPE_USER_PROFILE_ERREUR, gestionparcMessages.ERREUR);
		}
		
		internal function getEmailsRevendeurResultEvent(re:ResultEvent):void
		{
			if(re.result)
				myDatas.mailRevendeur(re.result as ArrayCollection);
			else
				ConsoviewAlert.afficherError(gestionparcMessages.RECUP_TYPE_USER_PROFILE_ERREUR, gestionparcMessages.ERREUR);
		}
		/**
		 * Fonction appellée au retour de la fonction <code>getListeIntervention()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetListeIntervention()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function getListeInterventionResultHandler(re:ResultEvent):void
		{
			if((re.result as ArrayCollection).length == 1 && (re.result as ArrayCollection)[0].error)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_LISTE_INTER_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
			else
			{
				myDatas.treatDataGetListeIntervention(re.result);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>setInfosIntervention()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosIntervention()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function setInfosInterventionResultHandler(re:ResultEvent):void
		{
			if((re.result as Array).length > 0 && (re.result as Array)[0] > 0)
			{
				myDatas.treatSetInfosIntervention(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.SAVE_INFOS_INTERVENTION_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * Fonction appellée au retour de la fonction <code>deleteIntervention()</code> (appel de procédure).
		 * Si le retour est KO, un message d'erreur est affiché.
		 */	
		internal function deleteInterventionResultHandler(re:ResultEvent):void
		{
			if(re.result < 0)
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.DELETE_INTERVENTION_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
	}
}