package rapport.utils
{
	import rapport.utils.consommationselector.consommationSelectorIHM;
	import rapport.utils.formattemplateselector.FormatTemplateSelectorIHM;
	import rapport.utils.groupeproduitselector.GroupeProduitSelectorIHM;
	import rapport.utils.monoperiodeselector.MonoPeriodeSelectorIHM;
	import rapport.utils.noSelector.NoSelector;
	import rapport.utils.perimetreselector.PerimetreOrgaSelectorIHM;
	import rapport.utils.perimetreselector.PerimetreSelectorIHM;
	import rapport.utils.periodecomboboxselector.PeriodeComboBoxSelectorIHM;
	import rapport.utils.periodeselector.PeriodeSelectorIHM;
	import rapport.utils.periodiciteselecteur.PeriodiciteSelectorIHM;
	import rapport.utils.poolsselector.PoolsSelectorIHM;
	import rapport.utils.produitdynamiqueselector.ProduitDynamiqueSelectorIHM;
	import rapport.utils.templateselector.TemplateSelectorIHM;
	import rapport.utils.topselector.TopSelectorIHM;
	import rapport.vo.SelectorParametre;
	
	public class SimpleFactory
	{
		public function SimpleFactory()	{}					
        private var returnSelector:SelectorAbstract;
	 
        public function createComposants(selector:Object):SelectorAbstract
		{
            switch(selector.typeSelecteur)
			{	          
	             case "PeriodeSelector": returnSelector =  new PeriodeSelectorIHM();(returnSelector as PeriodeSelectorIHM).setSelector(selector as SelectorParametre);
														   break;	
				
				 case "MonoPeriodeSelector": returnSelector = new rapport.utils.monoperiodeselector.MonoPeriodeSelectorIHM;(returnSelector as MonoPeriodeSelectorIHM).setSelector(selector as SelectorParametre);						
													          break;	
				 
				 case "TemplateSelector": returnSelector =  new  TemplateSelectorIHM();(returnSelector as TemplateSelectorIHM).setSelector(selector as SelectorParametre);
															break;	
				 
				 case "FormatTemplateSelector": returnSelector = new  FormatTemplateSelectorIHM();(returnSelector as FormatTemplateSelectorIHM).setSelector(selector as SelectorParametre);
										 			         	 break;
				 
				 case "PerimetreSelector": returnSelector = new PerimetreSelectorIHM();(returnSelector as PerimetreSelectorIHM).setSelector(selector as SelectorParametre);
															break;	
				 
				 case "PerimetreOragSelector": returnSelector = new PerimetreOrgaSelectorIHM();(returnSelector as PerimetreOrgaSelectorIHM).setSelector(selector as SelectorParametre);
															    break;
				
				 case "PeriodeComboBoxSelector": returnSelector = new PeriodeComboBoxSelectorIHM();(returnSelector as PeriodeComboBoxSelectorIHM).setSelector(selector as SelectorParametre);
																  break;	
				
				 case "PoolSelector": returnSelector = new PoolsSelectorIHM();(returnSelector as PoolsSelectorIHM).setSelector(selector as SelectorParametre);
													   break;
				 
				 case "TopSelector": returnSelector = new TopSelectorIHM();(returnSelector as TopSelectorIHM).setSelector(selector as SelectorParametre);
													   break;
				 
				 case "ConsoFtMinSelector": returnSelector = new consommationSelectorIHM();(returnSelector as consommationSelectorIHM).setSelector(selector as SelectorParametre);	break;
				 
				 case "GrpProduitSelector": returnSelector = new GroupeProduitSelectorIHM();(returnSelector as GroupeProduitSelectorIHM).setSelector(selector as SelectorParametre);
					 										 break;
				 
				 case "PeriodiciteSelector": returnSelector = new PeriodiciteSelectorIHM();(returnSelector as PeriodiciteSelectorIHM).setSelector(selector as SelectorParametre);
															 break;
				 
				 case "ProduitDynamiqueSelector": returnSelector = new ProduitDynamiqueSelectorIHM();(returnSelector as ProduitDynamiqueSelectorIHM).setSelector(selector as SelectorParametre);
					 break;
				  
				 default :  case "NoSelector": returnSelector = new NoSelector();(returnSelector as NoSelector).setSelector(selector as SelectorParametre);
															    break;	

		     }
			return returnSelector;
		 }		    				
	}
}