<cfcomponent displayname="fr.consotel.api.ibis.publisher.BipWebServiceFactory" hint="BIP WebService Factory">
	<!--- BIP WEBSERVICE FACTORY (Auteur : Cedric)
		- VARIABLES.WSDL_ENDPOINT : Named WSDL Url Address registered in Server Configuration
		- VARIABLES.WSDL_ENDPOINT_HELPER : CFC used to retrieve WSDL Address if fails with WSDL_ENDPOINT
		- VARIABLES.HELPER_WSDL_SUFFIX : WSDL Suffix used for Helper
	--->
	
	<!--- EXPLICIT INITIALIZATION --->
	<cfset initFactory()>
	
	<cffunction access="private" name="initFactory" returntype="void" hint="Initialize this factory instance">
		<cfset VARIABLES.WSDL_ENDPOINT="BIP">
		<cfset VARIABLES.WSDL_ENDPOINT_HELPER="fr.consotel.consoview.Application">
		<cfset VARIABLES.HELPER_WSDL_SUFFIX="/services/PublicReportService_v11?WSDL">
	</cffunction>
	
	<cffunction access="public" name="createWebService" returntype="any" hint="Creates and returns an instance of BIP WebService">
		<cfset startTick=0>
		<cfset webServiceInstance="NULL">
		<cftry>
			<cfset startTick=getTickCount()>
			<cfset webServiceInstance=createObject("webservice",VARIABLES.WSDL_ENDPOINT)>
			<cftrace category="DEBUG" type="information" text="Create WebService from WSDL_ENDPOINT DURATION : #(getTickCount() - startTick)# ms">
			<cfcatch type="any">
				<cftrace category="DEBUG" type="warning" text="Unable create webservice from SERVER endpoint : #VARIABLES.WSDL_ENDPOINT#">
				<cftrace category="DEBUG" type="warning" text="Trying to create webservice from HELPER : #VARIABLES.WSDL_ENDPOINT_HELPER#">
				<cftry>
					<cfset startTick=getTickCount()>
					<cfset wsdlHelper=createObject("component",VARIABLES.WSDL_ENDPOINT_HELPER)>
					<cfif structKeyExists(wsdlHelper,"BI_SERVER")>
						<cfset webServiceInstance=createObject("webservice",wsdlHelper.BI_SERVER & VARIABLES.HELPER_WSDL_SUFFIX)>
						<cftrace category="DEBUG" type="information" text="Create WebService from WSDL HELPER DURATION : #(getTickCount() - startTick)# ms">
					<cfelse>
						<cftrace category="DEBUG" type="error" text="Missing BI_SERVER key WSDL HELPER : #WSDL_ENDPOINT_HELPER#">
						<cfthrow type="Custom" errorcode="ILLEGAL_STATE" message="Missing BI_SERVER key WSDL HELPER : #WSDL_ENDPOINT_HELPER#">
					</cfif>
					<cfcatch type="any">
						<cftrace category="DEBUG" type="error" text="Unable create webservice from HELPER : #VARIABLES.WSDL_ENDPOINT_HELPER#">
						<cfrethrow/>
					</cfcatch>
				</cftry>
			</cfcatch>
		</cftry>
		<!--- LOGGING WEBSERVICE INFOS --->
		<cfset logWebServiceInfos(webServiceInstance)>
		<cfreturn webServiceInstance>
	</cffunction>
	
	<cffunction access="private" name="logWebServiceInfos" returntype="void" hint="Log infos about webservice">
		<cfargument name="webService" type="any" required="true" hint="WebService (As Java Object)">
		<cfset portName=ARGUMENTS.webService.getPortName()>
		<cfset serviceObject=ARGUMENTS.webService._getService()>
		<cfset wsdlAddress=EVALUATE("serviceObject.get#portName#Address()")>
		<cftrace category="DEBUG" type="information" text="BIP WEBSERVICE PORTNAME : #portName#">
		<cftrace category="DEBUG" type="information" text="BIP WEBSERVICE WSDL : #wsdlAddress#">
	</cffunction>
</cfcomponent>
