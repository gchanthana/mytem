package univers.inventaire.equipements.gestionflottemobile.listedispo.listeparcollab
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.DispoSimIHM;
	import univers.inventaire.equipements.gestionflottemobile.listedispo.IListeDispo;

	public class ListeSimOfCollab extends DispoSimIHM implements IListeDispo
	{
		[Embed(source="/assets/equipement/SIM50x50.png")] 
		private var adrImg : Class;
		private var dataListeSIM : ArrayCollection = new ArrayCollection();
		private var avecLigne : Boolean = true;
		private var idCollab : int;
		public function ListeSimOfCollab(idCollab : int,avecLigne : Boolean=true)//Par default on affiche les SIM avec et sans Ligne
		{
			this.idCollab = idCollab;
			this.avecLigne = avecLigne;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(evt : FlexEvent):void
		{
			initData();
		
		}
		public function getIDSelection():int
		{
			return dgListeSIM.selectedItem.IDSIM;
		}
		public function getDataGrid():DataGrid
		{
			return dgListeSIM;
		}
		public function getLibelleType():String
		{
			return "Liste des cartes SIM";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		public function getLibelleSelection():String
		{
			if(dgListeSIM.selectedIndex!=-1){
				return dgListeSIM.selectedItem.NUM_SIM;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		private function initData():void
		{
			var maProcdedure : String = "Liste_all_sim_emp";
			if(avecLigne==false){
				maProcdedure = "getAffectedSimToEmp";
			}
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
			
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							maProcdedure,
							initResultHandler);
			
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,idCollab,PanelGestionFoltteMobile.idPoolGestionnaire);							
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeSIM= re.result as ArrayCollection;
				dgListeSIM.dataProvider = dataListeSIM;
				dataListeSIM.refresh();
				trace("SIM disponible :"+ObjectUtil.toString(re.result));
			}		
		}
	}
}