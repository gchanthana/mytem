package gestioncommande.services.parameters
{
	import flash.events.EventDispatcher;
	
	import gestioncommande.entity.UserAccessPO;
	import gestioncommande.events.UserAccessModelEvent;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;

	internal class UserAccessModel extends EventDispatcher
	{
		
		private var _userAccess:UserAccessPO;
		public function UserAccessModel()
		{
			_userAccess = new UserAccessPO();	
		}

		private var _checkPassWordLastResult:Number = -1;
		public function get checkPassWordLastResult():Number
		{
			return _checkPassWordLastResult;
		}
		
		
		 
		public function get userAccess():UserAccessPO
		{
			return _userAccess;
		}

		internal function updateUserAccess(value:ArrayCollection):void
		{	
			_userAccess.removeAll();
			
			if(value)
			{
				var cursor:IViewCursor = value.createCursor();
				
				while(!cursor.afterLast)
				{	
					_userAccess[cursor.current.CODE_PARAMS]=cursor.current.IS_ACTIF;
					cursor.moveNext();
				}
			}	
			
			dispatchEvent(new UserAccessModelEvent(UserAccessModelEvent.UAME_USER_ACCESS_UPDATED));
		}
		
		
		internal function updateCheckPassWordResult(value:Number):void
		{
			_checkPassWordLastResult = value;
			dispatchEvent(new UserAccessModelEvent(UserAccessModelEvent.UAME_CHECK_PASSWORD_UPDATED));
		}

		
		
	}
}