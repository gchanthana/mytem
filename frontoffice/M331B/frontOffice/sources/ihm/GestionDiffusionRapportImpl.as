package ihm
{
	import event.diffusion.DiffusionEvent;
	
	import ihm.diffusion.ListeDiffusionIHM;
	import ihm.diffusion.NouvelleDiffusionIHM;
	
	import mx.containers.TabNavigator;
	import mx.containers.VBox;
	import mx.events.FlexEvent;
	
	import rapport.vo.ApiReportingSingleton;

	public class GestionDiffusionRapportImpl extends VBox
	{
		public var rapports:VBox;
		public var tabNavigatorDiffusion:TabNavigator;
		public var listeAllDiffusion:VBox;
		public var newDiffusion:NouvelleDiffusionIHM;
		public var listeDiffIHM :ListeDiffusionIHM;
		public var tabNavigator:TabNavigator;
		
		[Bindable]public var verif :Boolean;
		
		public function GestionDiffusionRapportImpl()
		{
			super();
		}

		protected function initIHMHandler(event:FlexEvent):void
		{				 
			rapports.addChild(ApiReportingSingleton.getInstance().moduleRapport);
			ApiReportingSingleton.getInstance().moduleRapport.percentHeight = 100;
			ApiReportingSingleton.getInstance().moduleRapport.percentWidth = 100;
			addEventListener(DiffusionEvent.SELECT_CHILD_EVENT,selectListeDiffusion);
			
			if(!verif)// pour afficher l'onglet diffusion ou pas 
			{
				tabNavigator.removeChildAt(1);
				tabNavigator.setStyle("tabHeight",0);// n'affiche pas le tab rapport
			}
		}
		
		private  function selectListeDiffusion(evt:DiffusionEvent):void
		{
			tabNavigatorDiffusion.selectedChild=listeAllDiffusion;
			newDiffusion.diffusionIHM.initialIHM();// initialiser le ihm apèrs la validation de la diffusion
			listeDiffIHM.getListeRapportDiffusion();// mettre a jour la liste des diffusion
		}
	}
}
