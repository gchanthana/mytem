package univers.parametres.perimetres.rightSide.destinataireComponent
{
	import composants.parametres.perimetres.OrgaStructure;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.collections.XMLListCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public dynamic class destinataireOrgaCmp extends OrgaStructure
	{
		public var _selectedOrga:Number;
		
		public function destinataireOrgaCmp()
		{
			//TODO: implement function
		}
		
		override public function initIHM(event:Event):void
		{
			super.initIHM(event);
			cmbOrganisation.visible = false;
			btnFilterCmb.visible = false;
			
		}
		
		/** Set the tree xml data in the dataprovider of the tree */
		override public function refreshTree():void
		{
			if (isNaN(_selectedOrga) == true)
				return ;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																					"getNode",
																					fillTreeResult);				
			RemoteObjectUtil.callService(op, _selectedOrga.toString(),  0, null);	
		}
		
		private function fillTreeResult(event: ResultEvent):void
		{
			
			_treeNodes = XML(event.result).children();
			var mylist:XMLListCollection = new XMLListCollection();
			mylist.source = XML(event.result).children();

			var tri:Sort = new Sort();
			tri.fields = [new SortField("@LABEL", true)];
			mylist.sort = tri;
			mylist.refresh();
			setTreeData(mylist.children(), "@LABEL");
			
		}
		
		override public function searchNode(nodeName:String):void
		{
			_searchTreeResult = _treeNodes.descendants().(@LABEL.toString().toLowerCase().search(nodeName.toLowerCase()) > -1);
			_searchItemIndex = 0;
			selectNextNode();
			_searchExecQuery = false;				
		}
		
		override public function selectNextNode():void
		{
			if (_searchTreeResult[_searchItemIndex] == null)
				_searchItemIndex = 0;
			if (_searchTreeResult[_searchItemIndex] == null)
				return ;
			selectNodeByAttribute("VALUE", _searchTreeResult[_searchItemIndex].@VALUE);
			_searchItemIndex++;
		}
		
	}
}