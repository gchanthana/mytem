package general.interfaceClass {
	import flash.events.IEventDispatcher;
	import mx.controls.treeClasses.ITreeDataDescriptor;

	public interface IPerimetreControl extends IEventDispatcher {
		function selectGroup(groupIndex:int):void;
		
		//function getPerimetreTreeDataDescriptor():ITreeDataDescriptor;
		
		//function getGroupList():Object;
		
		//function getNodeList():Object;
		
		//function getSelectedGroup():Object;
		
		//function getSelectedNodeInfos():Object;
		
		//function getSelectedGroupIndex():int;
	}
}
