﻿<cfcomponent output="false">
	<!---  --->


	<!--- LISTE DES TERMINAUX MOBILES SANS SIM DISPONIBLE D'UN POOL	 --->
	<cffunction name="listeTermDispo" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />

		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.listeTermDispo">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>

		<cfreturn result>
	</cffunction>

	<!--- LISTE DES TERMINAUX MOBILES AVEC SIM DISPONIBLE D'UN POOL	 --->
	<cffunction name="listeTermAvecSimDispo" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />

		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_M111B.listeTermAvecSimDispo">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>

		<cfreturn result>
	</cffunction>

	<!--- LISTE DES TERMINAUX FIXES SANS SIM FIXE DISPONIBLE D'UN POOL	 --->
	<cffunction name="listeTermFixeWithoutEqpFixe" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />

		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m111.listeTermFixeWithoutEqpFixe">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>

		<cfreturn result>
	</cffunction>

	<!--- LISTE DES TERMINAUX FIXES AVEC SIM FIXE DISPONIBLE D'UN POOL	 --->
	<cffunction name="listeTermFixeWithEqpFixe" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />

		<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m111.listeTermFixeWithEqpFixe">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>

		<cfreturn result>
	</cffunction>

	<!--- LISTE DES EQUIPEMENTS AFFECTES A UN COLLABORATEUR	 --->
	<cffunction name="listeEqpOfCollab" access="remote" returntype="query">
		<cfargument name="IDGROUPE_RACINE" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeEqpOfCollab">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDGROUPE_RACINE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<!--- LISTE DES LIGNES AFFECTEES A UN COLLABORATEUR	 --->
	<cffunction name="listeLigneOfCollab" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_m111.listeLigneOfCollab">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<!--- LISTE DES TERMINAUX FIXES AFFECTES A UN COLLABORATEUR	 --->
	<cffunction name="listeTermFixeOfCollab" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111B.listeTermFixeOfCollab">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES TERMINAUX MOBILES AFFECTES A UN COLLABORATEUR	 --->
	<cffunction name="listeTermMobileOfCollab" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111B.listeTermMobileOfCollab">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SIM MOBILES AVEC LIGNE. BOOLHASTERM DETERMINE SI ON AFFICHE EGALEMENT LES SIMS LIES A UN TERMINAL --->
	<cffunction name="ListeSimAvecLigne" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="boolhasterm" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.ListeSim">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhasterm#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SIM MOBILES SANS LIGNE. BOOLHASTERM DETERMINE SI ON AFFICHE EGALEMENT LES SIMS LIES A UN TERMINAL  --->
	<cffunction name="ListeSimSsLigne" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="boolhasterm" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.ListeSim">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhasterm#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<!--- LISTE DES SIM MOBILES AVEC LIGNE. BOOLHASTERM DETERMINE SI ON AFFICHE EGALEMENT LES SIMS LIES A UN TERMINAL --->
	<cffunction name="ListeSimAvecLigneV2" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="boolhasterm" required="true" type="Numeric" />
		<cfargument name="boolhascollab" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111B.ListeSim">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhasterm#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhascollab#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<cffunction name="ListeExtAvecLigne" access="remote" returntype="query">

		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="boolhasterm" required="true" type="Numeric" />
		<cfargument name="boolhascollab" required="true" type="Numeric" />

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111B.ListeExt">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhasterm#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhascollab#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>

	</cffunction>


	<!--- LISTE DES SIM MOBILES SANS LIGNE. BOOLHASTERM DETERMINE SI ON AFFICHE EGALEMENT LES SIMS LIES A UN TERMINAL  --->
	<cffunction name="ListeSimSsLigneV2" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="boolhasterm" required="true" type="Numeric" />
		<cfargument name="boolhascollab" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.ListeSim">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhasterm#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhascollab#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>


	<cffunction name="ListeExtSansLigne" access="remote" returntype="query">

		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="boolhasterm" required="true" type="Numeric" />
		<cfargument name="boolhascollab" required="true" type="Numeric" />

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111B.ListeExt">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhasterm#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolhascollab#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>


	<!--- LISTE DES SIM FIXES SANS TERMINAUX ET AVEC LIGNE --->
	<cffunction name="listeSimFixeAvecLigne" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeSimFixeAvecLigne">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SIM FIXES SANS TERMINAUX ET SANS LIGNE --->
	<cffunction name="listeSimFixeSansLigne" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeSimFixeSansLigne">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SIM MOBILES SANS TERMINAUX ET AVEC LIGNE --->
	<cffunction name="listeSimWithoutLigneOfCollab" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeSimWithoutLigneOfCollab">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SIM MOBILES SANS TERMINAUX ET SANS LIGNE --->
	<cffunction name="listeSimWithLigneOfCollab" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="ID_EMPLOYE" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeSimWithLigneOfCollab">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#ID_EMPLOYE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SIM MOBILES SANS TERMINAUX ET SANS LIGNE --->
	<cffunction name="ListeLigne" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111B.ListeLigneDispo">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<!--- LISTE DES COLLABORATEURS AVEC OU SANS EQUIPEMENT, SANS LIGNE--->
	<cffunction name="ListeCollab" access="remote" returntype="query">

		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="VALUE_LIGNE" required="true" type="any" />
		<cfargument name="VALUE_TERM" required="true" type="any" /><!--- collaborateur avec terminal ou non --->
		<cfargument name="isLigneMobile707" required="true" type="Numeric" />

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_M111B.ListeCollab">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
			<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="1" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<!--- LISTE DES COLLABORATEURS DISTINCTS AVEC OU SANS EQUIPEMENT, SANS LIGNE--->
	<cffunction name="listeAllCollaborateur" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.ListeAllCollab">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SOUS EQUIPEMENTS COMPATIBLES AVEC UN EQUIPEMENT  --->
	<cffunction name="ListeSsEqp" access="remote" returntype="query">
		<cfargument name="IDTERMINAL" required="true" type="Numeric" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfargument name="boolMobile" required="true" type="Numeric" />
		<cfargument name="boolFixe" required="true" type="Numeric" />
		<cfargument name="boolReseau" required="true" type="Numeric" />

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.ListeSsEqp">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDTERMINAL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolMobile#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolFixe#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#boolReseau#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>
	<!--- LISTE DES SOUS EQUIPEMENTS COMPATIBLES AVEC UN EQUIPEMENT  --->
	<cffunction name="ListeSsEqpOfEqp" access="remote" returntype="query">
		<cfargument name="IDTERMINAL" required="true" type="Numeric" />
		<cfargument name="ID_POOL" required="true" type="Numeric" />

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeSousEqpOfEqp">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#IDTERMINAL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>


	<cffunction name="listeEqptFixeDispo" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />

		<!--- LISTE DES EQUIPEMENTS DISPONIBLES SANS LIGNE LIGNE DIRECT OU INDIRECT --->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeTermFixeDispoSansLignes">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>

		<!--- LISTE DES EQUIPEMENTS AVEC AU MOIN UNE LIGNE DIRECT OU INDIRECT DISPONIBLE --->
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.listeTermFixeDispoAvecLignes">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result2">
		</cfstoredproc>


		<cfreturn result2>
	</cffunction>


	<!--- LISTE DES LIGNES DISPONIBLES AVEC OU SANS EQUIPEMENT --->
	<cffunction name="ListeLigneFixeDispo" access="remote" returntype="query">
		<cfargument name="ID_POOL" required="true" type="Numeric" />
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_m111.ListeLigneDispoFixe">
			<cfprocparam  value="#ID_POOL#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>




</cfcomponent>
