<!--- =========================================================================
Classe: TypeFactureCompleteGroupeLigneStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="TypeFactureCompleteGroupeLigneStrategy" hint=""  extends="TypeFactureStrategy" >
   <!---****
		*
		*
		*	On affiche la facture complete sans filtrer sur les lignes du groupe de ligne
		*
		******** ---> 
	
 
	<cffunction name="getData" access="public" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="Initial value for the IDINVENTAIRE_PERIODEproperty." />
		<cfargument name="fake" required="false" type="numeric" default="" displayname="numeric ID"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfargument name="code_langue" required="false" type="string">
		
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>		
		<cfset _code_langue = SESSION.USER.GLOBALIZATION><!--- Auparavant 'fr_FR' en dur --->		
		<!---<cfif isDefined("code_langue")>
			<cfset _code_langue = code_langue>	
		</cfif>--->
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_V3.FA_DETAILFACTURE_BYID_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#IDINVENTAIRE_PERIODE#"/>					
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="code_langue" value="#_code_langue#"/>
	        <cfprocresult name="qGetFactureAbo"/>        
		</cfstoredproc>
		
		<cfreturn qGetFactureAbo>
	</cffunction>

	<cffunction name="getDataCat" access="public" returntype="Query" output="false">
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfargument name="code_langue" required="false" type="string">
		
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>		
		<cfset _code_langue = SESSION.USER.GLOBALIZATION><!--- Auparavant 'fr_FR' en dur --->			
		<!---<cfif isDefined("code_langue")>
			<cfset _code_langue = code_langue>	
		</cfif>--->
		
        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL_V3.FA_SUMMARYFACTURE_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#IDINVENTAIRE_PERIODE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="code_langue" value="#_code_langue#"/>
	        <cfprocresult name="qGetFactureCat">
        </cfstoredproc>
		<cfreturn qGetFactureCat>
	</cffunction>
	
	<cffunction name="getFactureTva" access="public" returntype="query" output="true">
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfargument name="code_langue" required="false" type="string">
		
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>		
		<cfset _code_langue = SESSION.USER.GLOBALIZATION><!--- Auparavant 'fr_FR' en dur --->		
		<!---<cfif isDefined("code_langue")>
			<cfset _code_langue = code_langue>	
		</cfif>--->
		
        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GRCL_V3.FA_SUMMARYFACTURE_TVA_V2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#IDINVENTAIRE_PERIODE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="code_langue" value="#_code_langue#"/>
	        <cfprocresult name="qgetFactureTva">
        </cfstoredproc>
		<cfreturn qgetFactureTva>
	</cffunction>
	
	<cffunction name="displayType" access="public" returntype="string" output="false" >
		<cfreturn "Facture Complète">
	</cffunction>
</cfcomponent>