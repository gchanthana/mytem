<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.service.DemoService" displayname="fr.consotel.consoview.api.ibis.publisher.service.DemoService">
	<cffunction name="runReport" returntype="void" access="public">
		<cfset userId="developer">
		<cfset pwd="tred78">
		<cfset reportAbsPath="/~developer/DEV/LABS/INTEGRATION/Report/Report.xdo">
		<cfset taskUUID=createUUID()>
		<cfxml variable="xmlTask">
			<cfoutput>
			<TASK>
				<REPORT_ABSOLUTE_PATH>#reportAbsPath#</REPORT_ABSOLUTE_PATH>
				<TEMPLATE_ID></TEMPLATE_ID>
				<OUTPUT_FORMAT>xml</OUTPUT_FORMAT>
				<REMOTE_FILE>#(taskUUID & ".xml")#</REMOTE_FILE>
				<DELIVERY>FTP</DELIVERY>
				<JOB>API-JOB-#taskUUID#</JOB>
				<PARAMETERS>
					<P_DATEDEB>2009/01/01</P_DATEDEB>
				</PARAMETERS>
			</TASK>
			</cfoutput>
		</cfxml>
		<cfset reportingService=createObject("component","fr.consotel.consoview.api.ibis.publisher.service.reporting.ReportingService")>
		<cfset resultObject=reportingService.runReport(xmlTask,userId,pwd)>
		<cfcontent type="#resultObject.getReportContentType()#" variable="#resultObject.getReportBytes()#">
	</cffunction>
	
	<cffunction name="getSampleData" returntype="xml" access="remote">
		<cfxml variable="sampleXml">
			<ROWSET>
				<ROW/>
			</ROWSET>
		</cfxml>
		<cfif isDefined("ARGUMENTS")>
			<cfset keys=listToArray(structKeyList(ARGUMENTS))>
			<cfloop index="i" from="1" to="#arrayLen(keys)#">
				<cfset sampleXml["ROWSET"]["ROW"][1].xmlChildren[i]=xmlElemNew(sampleXml,keys[i])>
				<cfset sampleXml["ROWSET"]["ROW"][1][keys[i]].xmlText=ARGUMENTS[keys[i]]>
			</cfloop>
		</cfif>
		<cfreturn toString(sampleXml)>
	</cffunction>
</cfcomponent>