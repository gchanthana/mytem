package gestionparc.ihm.action.mdm.actionVerrouillerTerminal
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.ihm.fiches.popup.PopUpPinCodeDeviceIHM;
	import gestionparc.services.mdm.MDMService;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;

	public class ActionVerrouillerTerminalReceiver implements IReceiver
	{
		private var _popUpPinCode:PopUpPinCodeDeviceIHM;

		private var _mdmSrv:MDMService;

		//private var _isDroid			:Boolean = false;

		// Retourne TRUE si l'OS est ANDROID
		public function get isDroid():Boolean
		{
			return ObjectUtil.stringCompare(SpecificationVO.getInstance().elementDataGridSelected.OS, SpecificationVO.ANDROID, true) == 0;
		}

		public function set isDroid(value:Boolean):void
		{
			// N'effectue aucune action
		}

		public function ActionVerrouillerTerminalReceiver()
		{
			_mdmSrv=new MDMService();
		}

		public function action():void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etes_vous_surs_de_vouloir_verrouiller_ce'), "MDM", resultAlertLock);
		}

		private function resultAlertLock(ce:CloseEvent):void
		{
			if (ce.detail == Alert.OK)
			{
				if ((String(SpecificationVO.getInstance().elementDataGridSelected.MDM_PROVIDER).toLowerCase() == "mobileiron") || (!isDroid))
				{
					_mdmSrv.lock(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, '0000');
				}
				else
					popUpPinCode();
			}
		}

		private function popUpPinCode():void
		{
			_popUpPinCode=new PopUpPinCodeDeviceIHM();
			_popUpPinCode.setInfos(SpecificationVO.getInstance().elementDataGridSelected, "Code pin du terminal", "Veuillez saisir le code pin du terminal :", '', false, isDroid);
			_popUpPinCode.addEventListener('PIN_CODE_DEVICE', resultPopUpPinCodeHandler);

			PopUpManager.addPopUp(_popUpPinCode, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(_popUpPinCode);
		}

		private function resultPopUpPinCodeHandler(e:Event):void
		{
			if (e.type == 'PIN_CODE_DEVICE')
			{
				var statut:Object=_popUpPinCode.getInfos();

				PopUpManager.removePopUp(_popUpPinCode);

				_mdmSrv.lock(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, statut.PINCODE);

				//SpecificationVO.getInstance().dispatchEventRefresh();
			}
		}

	}
}
