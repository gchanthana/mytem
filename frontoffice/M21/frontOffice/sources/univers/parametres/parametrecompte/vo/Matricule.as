package univers.parametres.parametrecompte.vo
{
	[Bindable]
	public class Matricule
	{
		
		public var PREFIXE			:String = '';
		public var CURRENT_VALUE	:String = '';
		
		public var LAST_VALUE		:int = 0;
		public var IDRACINE			:int = 0;
		public var IS_ACTIF			:int = 0;
		public var NB_DIGIT			:int = 0;
		
		public function Matricule()
		{
		}
		
		public static function mapping(value:Object):Matricule
		{
			var matricule:Matricule 	= new Matricule();
				matricule.PREFIXE		= value.PREFIXE;
				matricule.CURRENT_VALUE	= value.CURRENT_VALUE;
				matricule.IDRACINE		= value.IDRACINE;
				matricule.IS_ACTIF		= value.IS_ACTIF;
				matricule.LAST_VALUE	= value.LAST_VALUE;
				matricule.NB_DIGIT		= value.NB_DIGIT;
			
			return matricule;
		}
	}
}