package univers.facturation.suivi.controles.vo
{
	/**
	 * <b>Class <code>OperateurVO</code></b>
	 * <p><pre>
	 * <u>Description :</u>
	 * Value Object représentant les propriétés d'un opérateur.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * </pre></p>
	 *
	 */
	[Bindable]
	public class OperateurVO
	{
		
		public var idOperateur			: int 				= 0; 
		
		public var nom					: String			= "";
		public var contact				: String			= "";
		
		public var adresse				: String			= ""; 
		public var codePostale			: String			= ""; 
		public var commune				: String			= ""; 
		public var pays					: String			= ""; 
		
	}
}