package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	public class AffecterSimCollab implements IComportementAffectation 
	{
		public function AffecterSimCollab()
		{
			
		}
		public function affecter(idSim : int, idCollab : int, libelle_sim : String, libelle_collab : String, date : String, pret : int):void
		{
				
			//Alert.show("AffecterSimCollab : "+idSim+"--"+idCollab);
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"affectSimCollab_V3",
																		affecter_handler);
			RemoteObjectUtil.callService(op,idSim,idCollab,libelle_sim,libelle_collab,date,pret);
		}
		private function affecter_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Affectation de la carte SIM effectuée");
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
		public function annuler():void
		{
			Alert.show("fermer");
		}


	}
}