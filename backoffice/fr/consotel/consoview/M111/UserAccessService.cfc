<cfcomponent>
	
	
	
	<cffunction access="remote" output="false" name="getModuleUserAccess" 
		displayname="getModuleUserAccess" returntype="Array"
		description="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application" 
		hint="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application">
		
		<cfset idUSer = SESSION.USER.CLIENTACCESSID>
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset idGroupeClient = SESSION.PERIMETRE.ID_PERIMETRE>
		<cfset idLang = SESSION.USER.IDGLOBALIZATION>
		<cfset codeModule = "M111">
		
		
		<cfset qUserAccess = getUserAccess(idUSer,idRacine,idGroupeClient,codeModule,idLang)>
		<cfset qRacineInfos = getRacineInfos(idRacine)>
		<cfset qListOS = createObject("component","fr.consotel.consoview.M111.enrollmentprocess.OSEnrollmentStrategy").getListOs()>
		<cfset qUserSession = getUserSession()>
		
		<cfset aInfos = arrayNew(1)> 
		<cfset arrayAppend(aInfos ,qUserAccess)>
		<cfset arrayAppend(aInfos ,qRacineInfos )>
		<cfset arrayAppend(aInfos ,qListOS)>
		<cfset arrayAppend(aInfos, qUserSession)>		

		<cfreturn aInfos>		
	</cffunction>
	
	
	
	<cffunction access="remote" output="false" name="checkPassWord" 
		displayname="checkPassWord" returntype="Numeric" 
		description="Vérifie le mot de passe de l'utilisateur connecté pour valider une action critique" 
		hint="Vérifie le mot de passe de l'utilisateur connecté pour valider une action critique">																	
		<cfargument name="password" type="string" required="true" hint="le mot de passe">
				
		<cfset login = SESSION.USER.EMAIL>
						
		<cfset pResult = checkCredentials(login,password)>
		
		<cfreturn pResult>
	</cffunction>
	
	
	
	
	<!---////////////////////////////////////// Private functions /////////////////////////////////////////////// --->
	<cffunction access="private" output="false" name="getUserAccess" 
		displayname="getUserAccess" returntype="query" 
		description="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application" 
		hint="Liste les droits utilisateur (au niveau de l'abonnement) pour un ou tous les modules de l'application">
		
		<cfargument name="idUSer" type="numeric" required="true" hint="l id user (clientaccessid)">
		<cfargument name="idRacine" type="numeric" required="true" hint="l id de la racine">
		<cfargument name="idGroupeClient" type="numeric" required="true" hint="l id du noeud sur laquelle on est connecté">
		<cfargument name="codeModule" type="string" required="true" hint="le code du module pour lequel on veut les droits ou '' pour avoir tous les modules de l application">
		<cfargument name="idLang" type="numeric" required="true" hint="l id de la langue">	
					
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M00.GET_APP_USERPARAMS_V2">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUSer#" variable="p_app_loginid">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#" variable="p_langid">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idGroupeClient#" variable="p_idgroupe_client">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#codeModule#">
			<cfprocresult name="qUserAccess">
		</cfstoredproc>		 
		
		<cfreturn qUserAccess>
	</cffunction>
	
	
	<cffunction access="private" output="false"  name="checkCredentials"  
				displayname="checkCredentials"	returntype="Numeric"
				description="verifie les credentials passés en paramètre"
				hint="verifie les credentials passés en paramètre">
		
		<cfargument name="login" type="string" required="true" hint="le login">
		<cfargument name="password" type="string" required="true" hint="le mot de passe">
		
		<cfset auhentification = -1>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GLOBAL.CONNECTCLIENT">
			<cfprocparam value="#login#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocparam value="#password#" 	 cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="qRetour">
		</cfstoredproc>

		<cfif qRetour.recordcount gt 0>
			<cfif qRetour['CLIENTACCESSID'][1] gt 0>
				<cfset auhentification = 1>
			</cfif>	
		</cfif>
		
		<cfreturn auhentification>		
	</cffunction>
	
	<cffunction access="private" output="false" name="getRacineInfos" 
		displayname="getRacineInfos" returntype="query" 
		description="Liste les infos de la racine passée en params" 
		hint="Liste les infos de la racine passée en params">
			
		<cfargument name="idRacine" type="numeric" required="true" hint="l id de la racine">			
		
		<cfquery datasource="#Session.OFFREDSN#" name="qRacineInfos">
			select * from racine_info ri where ri.idgroupe_client = #idRacine#
		</cfquery>			
				
		<!---<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M00.GETRACINEINFO">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
			<cfprocresult name="qRacineInfos">
		</cfstoredproc>--->		 
		
		<cfreturn qRacineInfos>
	</cffunction>

	<!--- 
		Récupère les infos de filtre dans la session utilisateur
	 --->
	<cffunction name="getUserSession" access="private" returntype="Query" output="false" hint="Les infos de filtre sauvegardé pour un client dans une racine">
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m111B.getUser_session">
				<cfprocparam type="In" variable="p_app_loginid" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
				<cfprocparam type="In" variable="p_idracine" 	cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="setUserSession" access="remote" returntype="Numeric" output="false" hint="Mettre à jour Les infos de filtre sauvegardé pour un client dans une racine">
			<cfargument name="idPool" type="numeric" required="true">
			<cfargument name="codeColonne" type="String">
			<cfargument name="valueColonne" type="String">
			
			<cfset idUSer = SESSION.USER.CLIENTACCESSID>
			<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m111B.saveUser_session">
				<cfprocparam type="In" variable="p_app_loginid" 	cfsqltype="CF_SQL_INTEGER" value="#idUser#">
				<cfprocparam type="In" variable="p_idracine" 		cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
				<cfprocparam type="In" variable="p_idpool" 			cfsqltype="CF_SQL_INTEGER" value="#idPool#">
				<cfprocparam type="In" variable="p_colonne_code" 	cfsqltype="CF_SQL_VARCHAR" value="#codeColonne#">
				<cfprocparam type="In" variable="p_colonne_value" 	cfsqltype="CF_SQL_VARCHAR" value="#valueColonne#">
				
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour"/>
			</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
</cfcomponent>