package gestioncommande.events
{
	import flash.events.Event;
	
	public class VSEvent extends Event
	{


		
		//VIEWSTACK A VOIR
		public static const VIEW_ETAPE_LISTCOMMANDE		:String = "VIEW_ETAPE_LISTCOMMANDE";
		public static const VIEW_ETAPE_NEWCOMMANDE		:String = "VIEW_ETAPE_NEWCOMMANDE";
		public static const VIEW_ETAPE_LINES			:String = "VIEW_ETAPE_LINES";
		public static const VIEW_ETAPE_LINESBYLINES		:String = "VIEW_ETAPE_LINESBYLINES";
		public static const VIEW_ETAPE_HISTORIQUE		:String = "VIEW_ETAPE_HISTORIQUE";
		
		

		public function VSEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new VSEvent(type, bubbles, cancelable);
		}

	}
}