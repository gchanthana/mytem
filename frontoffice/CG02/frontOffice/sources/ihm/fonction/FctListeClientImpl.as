package ihm.fonction
{
	import entity.ApplicationVO;
	import entity.ClientPartenaireVO;
	import entity.OffreVO;
	import entity.PartenaireVO;
	
	import event.FctListeClientEvent;
	
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	import service.FctListeClientServices;
	
	import utils.Constantes;
	import utils.abstract.AbstractFonction;
	
	public class FctListeClientImpl extends AbstractFonction
	{
		
/* ******************************************************************************************************
		1) PUBLIC
****************************************************************************************************** */		
		[Bindable] public var dgClientContractuel:DataGrid;
		[Bindable] public var cbApplication:ComboBox;
		[Bindable] public var cbOffre:ComboBox;
		[Bindable] public var cbPartenaire:ComboBox;
		[Bindable] public var btnRechercher:Button;
		[Bindable] public var tiRecherche:TextInput;
		
		private var _liste_Application:ArrayCollection;
		private var _liste_offre:ArrayCollection;
		private var _liste_partenaire:ArrayCollection;
		
		public function FctListeClientImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
/* ******************************************************************************************************
 		2) PRIVATE
****************************************************************************************************** */		
		private var serv:FctListeClientServices;
		
/* 2.1) PHASE D'INITIALISATION */
		private function init(evt:FlexEvent):void
		{
			initVar();
			initListener();
			initData();
		}
		private function initVar():void
		{
			this.serv = new FctListeClientServices();
		}
		private function initListener():void
		{
			cbApplication.addEventListener(ListEvent.CHANGE,cbApplication_changeHandler);
			btnRechercher.addEventListener(MouseEvent.CLICK,btnRechercher_clickHandler);
			tiRecherche.addEventListener(KeyboardEvent.KEY_UP,tiChangeEvent);
		}
		private function initData():void
		{
			getData();
			getListeclientcontractuel(0,0,0,"");
		}
/* 2.2) PHASE HANDLER */
		private function cbApplication_changeHandler(evt:ListEvent = null):void
		{
			if(cbApplication.selectedIndex > -1)
			{
				getListePartenaireOffreFromApplication((cbApplication.selectedItem as ApplicationVO).CODEAPP);
			}
		}
		private function btnRechercher_clickHandler(event:MouseEvent):void
		{
			getListeclientcontractuel( 	(cbApplication.selectedItem as ApplicationVO).CODEAPP,
										(cbPartenaire.selectedItem as PartenaireVO).ID,
										(cbOffre.selectedItem as OffreVO).ID,
										tiRecherche.text);
		}
		private function tiChangeEvent(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
				btnRechercher_clickHandler(null);
		}
/* 2.3) PHASE SERVICE */
	/* LISTE DES CLIENTS CONTRACTUELS : CLIENT OU PARTENAIRE */
		private function getListeclientcontractuel(codeApp:int,partenaireid:int,offreid:int,str:String):void
		{
			CvAccessManager.CURRENT_FUNCTION;
			this.serv.myDatas.addEventListener(FctListeClientEvent.proc_LISTE_CLIENT_CONTRACTUEL_EVENT,getListeClientContractuelHandler);
			this.serv.getListeClientContractuel(codeApp,partenaireid,offreid,str);
		}
		private function getListeClientContractuelHandler(evt:FctListeClientEvent):void
		{
			this.dgClientContractuel.dataProvider = this.serv.myDatas.listeClientContractuel;
			
			if(this.serv.myDatas.hasEventListener(FctListeClientEvent.proc_LISTE_CLIENT_CONTRACTUEL_EVENT))
				this.serv.myDatas.removeEventListener(FctListeClientEvent.proc_LISTE_CLIENT_CONTRACTUEL_EVENT,getListeClientContractuelHandler);
		}
	/* RECUPERATION DES DIFFERENTES LISTES DE LA FONCTIONNALITE : partenaire, application et offre */
		private function getData():void
		{
			this.serv.myDatas.addEventListener(FctListeClientEvent.proc_GETDATA_EVENT,getdataHandler);
			this.serv.getData();
		}
		private function getdataHandler(evt:FctListeClientEvent):void
		{
			this.cbApplication.dataProvider = Constantes.listeApplication;
			this.cbOffre.dataProvider = this.serv.myDatas.listeOffre;
			this.cbPartenaire.dataProvider = this.serv.myDatas.listePartenaire;
			
			if(this.serv.myDatas.hasEventListener(FctListeClientEvent.proc_GETDATA_EVENT))
				this.serv.myDatas.removeEventListener(FctListeClientEvent.proc_GETDATA_EVENT,getdataHandler);
		}
	/* RECUPERATION DES LISTES : partenaire et offre */
		private function getListePartenaireOffreFromApplication(codeApp:int=0):void
		{
			this.serv.myDatas.addEventListener(FctListeClientEvent.proc_GETDATA_FROMAPPLICATION_EVENT,getListePartenaireOffreFromApplicationHandler);
			this.serv.getListePartenaireOffreFromApplication(codeApp);
		}
		private function getListePartenaireOffreFromApplicationHandler(evt:FctListeClientEvent):void
		{
			this.cbOffre.dataProvider = this.serv.myDatas.listeOffre;
			this.cbOffre.selectedIndex = 0;
			this.cbPartenaire.dataProvider = this.serv.myDatas.listePartenaire;
			this.cbPartenaire.selectedIndex = 0;
			
			if(this.serv.myDatas.hasEventListener(FctListeClientEvent.proc_GETDATA_FROMAPPLICATION_EVENT))
				this.serv.myDatas.removeEventListener(FctListeClientEvent.proc_GETDATA_FROMAPPLICATION_EVENT,getListePartenaireOffreFromApplicationHandler);
		}
/* 2.4) PHASE UTILS */
		protected function sortCompareFunctionClient(obj1:Object, obj2:Object):int
		{
			if((obj1 as ClientPartenaireVO).CLIENT.NOM.toUpperCase() == (obj2 as ClientPartenaireVO).CLIENT.NOM.toUpperCase())
				return 0
			if((obj1 as ClientPartenaireVO).CLIENT.NOM.toUpperCase() > (obj2 as ClientPartenaireVO).CLIENT.NOM.toUpperCase())
				return 1
			else
				return -1
		}
		protected function sortCompareFunctionPartenaire(obj1:Object, obj2:Object):int
		{
			if((obj1 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase() == (obj2 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase())
				return 0
			else if((obj1 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase() > (obj2 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase())
				return 1
			else
				return -1
		}
		protected function sortCompareFunctionOffre(obj1:Object, obj2:Object):int
		{
			if((obj1 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase() == (obj2 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase())
				return 0;
			else if((obj1 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase() > (obj2 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase())
				return 1
			else
				return -1
		}
		protected function sortCompareFunctionApplication(obj1:Object, obj2:Object):int
		{
			if((obj1 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase() == (obj2 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase())
				return 0
			else if((obj1 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase() > (obj2 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase())
				return 1
			else
				return -1
		}
/* ******************************************************************************************************
		3) GETTER / SETTER
****************************************************************************************************** */
		public function get liste_partenaire():ArrayCollection { return _liste_partenaire; }
		public function set liste_partenaire(value:ArrayCollection):void
		{
			if (_liste_partenaire == value)
				return;
			_liste_partenaire = value;
		}
		public function get liste_offre():ArrayCollection { return _liste_offre; }
		public function set liste_offre(value:ArrayCollection):void
		{
			if (_liste_offre == value)
				return;
			_liste_offre = value;
		}
		public function get liste_Application():ArrayCollection { return _liste_Application; }
		public function set liste_Application(value:ArrayCollection):void
		{
			if (_liste_Application == value)
				return;
			_liste_Application = value;
		}
	}
}