package composants.tb.graph
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import flash.events.Event;
	
	public class GraphBase extends GraphBase_IHM implements IGraphModel
	{
		
		
		protected var _total : Number;
	 	
		protected var moisDeb : String;
		
		protected var moisFin : String;
		
		protected var perimetre : String;
		
		protected var modeSelection : String;
		
		protected var identifiant : String;
						
		public function GraphBase()
		{
			//TODO: implement function
			super();
			this.creationPolicy = "all";
		 
		}
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;
		}
	
				
		public function showChart():void
		{
			//TODO: implement functio			
		}
		
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null, identifiant : String= null):void
		{
			//TODO: implement function
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;
			this.identifiant = identifiant;
		}
		
		public function updateSegment(segment : String = null):void
		{
			//TODO: implement function
		}
		
		
		public function showGrid():void
		{
			//TODO: implement function
		}
		
		[Bindable]
		public function get dataProviders():ArrayCollection{
			var a : ArrayCollection ;
			return a;
		}
		
		public function set dataProviders(d : ArrayCollection):void{
		
		}
		
		public function update():void
		{
			//TODO: implement function
			chargerDonnees();
			
		}
		
		/**
		 * Efface les données, interrompt les 'remotings'
		 **/
		public function clean():void{
			//Abstract
		}		
		
		protected function _formatterDataProvider(d: ArrayCollection):ArrayCollection{
			return d;
		}	
		
		protected function calculTotal(arr : Array, colname : String):Number{
			var total : Number = 0;
			for(var i:int=0; i< arr.length;i++){
				
				total = total + parseFloat(arr[i][colname]);
			}
			return total;
		}
		
		protected function chargerDonnees():void{
		}
		
		protected function chargerDonneesFaultHandler(fe :FaultEvent):void{
			
		 	 	
		}     
		 
		protected function chargerDonneesResultHandler(re :ResultEvent):void{			 
		  
		}		
	}
}