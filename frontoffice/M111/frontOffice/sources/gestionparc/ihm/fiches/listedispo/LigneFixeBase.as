package gestionparc.ihm.fiches.listedispo
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.services.liste.ListeService;
	
	import mx.collections.ICollectionView;
	import mx.containers.Canvas;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;
	
	public class LigneFixeBase extends Canvas implements IListeDispo
	{
		public var dgList:DataGrid;
		public var txtFiltre:TextInput;
		public var lblCompteur:Label;
		public var cbBox:CheckBox;
		
		[Bindable]
		public var gridColumns:Array; 
		
		public var selectedItem:Object = {};
		public var selectedIndex:Number = -1;
		public var subjectField:String = "LIGNE";
		public var idSubjectField:String = "IDSOUS_TETE"; 
		public var excludeField:String = "IDEQ";
		public var datafiled:Object = 
			{LIGNE:"",
				MODELE:"",
				MARQUE:""}; 
		
		
		protected var _titre:String = ResourceManager.getInstance().getString('M111', 'Liste_des_lignes_disponibles');
		protected var _s_compteur:String = "";
		protected var _la_selection:String = "";
		protected var _dataProvider:ICollectionView;
		
		
		public var serv:ListeService = new ListeService();
		protected var _dataProviderChanged:Boolean = false;
		protected var _real_len:Number = 0;
			
		protected var _bool_include_eq:Boolean = true;
		
		
		private var _searchPattern:String="";
		
		public function LigneFixeBase()
		{
			super();
		}
		
		protected function getData():void
		{
			throw new Error("This methode must be implemented");
		}
		
		protected function addListener():void
		{	 
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			dgList.addEventListener(ListEvent.CHANGE,dgListeChangeHandler);
			cbBox.addEventListener(MouseEvent.CLICK,cbBoxClickHandler);
		}
		
		protected function removeListener():void
		{	 
			txtFiltre.removeEventListener(Event.CHANGE,txtFiltreChangeHandler);
			dgList.removeEventListener(ListEvent.CHANGE,dgListeChangeHandler);
			cbBox.removeEventListener(MouseEvent.CLICK,cbBoxClickHandler);
		}
		
		protected function hideEquipement(value:Boolean):void
		{
			gridColumns[1].visible = gridColumns[2].visible = value;
			_bool_include_eq = value;
			_dataProvider.filterFunction=filterList;
			_dataProvider.refresh();
		}
		
		
		public function getIDSelection():int
		{
			return selectedItem[idSubjectField]
		}
		
		public function getLibelleSelection():String
		{
			return _la_selection;
		}
		public function getLibelleType():String
		{
			return _titre;
		}
		public function getDataGrid():DataGrid
		{
			return dgList;
		}
		public function getSourceIcone():Class
		{
			return null
		}
		public function getSelection():Object
		{
			return selectedItem;
		}
		
		private function filterList(item:Object):Boolean
		{	
			
			for(var p:* in datafiled)
			{
				
				if ((String(item[p]).toLowerCase().search(_searchPattern) != -1) &&	((_bool_include_eq == true)? true : !(item[excludeField] > 0)))
				{
					return true
				}
				
			}
			return false;
		}
		
		protected function cbBoxClickHandler(evt:MouseEvent):void
		{
			if(evt.currentTarget.selected == true)
			{
				hideEquipement(true);
				
			}
			else
			{
				hideEquipement(false);
			}
			
		}
		
		protected function dgListeChangeHandler(evt:ListEvent):void
		{
			selectedItem = evt.currentTarget.selectedItem;
			selectedIndex = evt.currentTarget.selectedIndex;
			_la_selection = (selectedIndex > -1)?selectedItem[subjectField]:"-";
			
		}
		
		protected function listeChangeHandler(evt:CollectionEvent):void
		{
			 throw new Error("This methode must be implemented");
		}	
		
		protected function creationCompleteIHM(event:FlexEvent):void
		{	
			addListener();			
			cbBox.selected = true;
			txtFiltre.setFocus()
		}
		
		
		protected function txtFiltreChangeHandler(event:Event):void
		{
			if(_dataProvider)
			{
				_searchPattern = txtFiltre.text.toLowerCase();
				_dataProvider.disableAutoUpdate();
				_dataProvider.filterFunction=filterList;
				_dataProvider.refresh();	
			}
			
		}
		[Bindable(event="dataProviderChange")]
		public function get dataProvider():ICollectionView
		{
			return _dataProvider;
		}
		
		public function set dataProvider(value:ICollectionView):void
		{
			if(value != _dataProvider)
			{
				_dataProvider = value;
				_dataProviderChanged = true;
				_real_len = _dataProvider.length; 
				
				dispatchEvent(new Event("dataProviderChange"));
			}		
		}
		
		[Bindable]
		public function set s_compteur(value:String):void
		{
			if(value != _s_compteur)
			{
				_s_compteur = value;
			}
			
		}
		
		public function get s_compteur():String
		{
			return _s_compteur;
		}
	}
}