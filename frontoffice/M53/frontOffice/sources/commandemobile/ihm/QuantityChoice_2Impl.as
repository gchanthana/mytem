package commandemobile.ihm
{
	import commandemobile.composants.PanierIHM;
	import commandemobile.entity.Commande;
	import commandemobile.entity.ItemSelected;
	import commandemobile.entity.Panier;
	import commandemobile.system.Export;
	
	import composants.util.lignes.LignesUtils;
	
	import ficheemploye.InfosPersoCollaborateurIHM;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.DataGrid;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	[Bindable]
	public class QuantityChoice_2Impl extends Box
	{

//VARIABLES------------------------------------------------------------------------------
		
		//IHM
		public var composantPanier			:PanierIHM;
		public var newCollabo				:InfosPersoCollaborateurIHM;
		
		//COMPOSANTS
		public var dgRecapCommande			:DataGrid;
		 
		
		//OBJETS
		public var _cmd						:Commande;
		public var _itemsSelected			:ItemSelected;
		public var _panier					:Panier;
		private var _numLine				:LignesUtils = new LignesUtils();
		
		//VARIABLES GLOBALES
		public var parameterFill			:Boolean = false;
		public var newView					:Boolean = true;
		public var skipSubscription			:Boolean = false;
		public var skipAll					:Boolean = false;
		
		//VARIABLES LOCALES
		private var _listCommande			:ArrayCollection = new ArrayCollection();
		private var listCommandeRef			:ArrayCollection = new ArrayCollection();
		private var _collaborateurSelected	:Object = new Object();
		
//VARIABLES------------------------------------------------------------------------------

//PROPIETEES PUBLIC----------------------------------------------------------------------		
		
		//LISTE TOUTES LES COMMANDES
		public function get listCommande(): ArrayCollection
	    {
	    	return _listCommande;
	    }
	    
	    public function set listCommande(value:ArrayCollection): void
	    {
	    	_listCommande = value;
	    }

//PROPIETEES PUBLIC----------------------------------------------------------------------
	    
//METHODE PUBLIC-------------------------------------------------------------------------
		
		//CONSTRUCTEUR (>LISTENER)
		public function QuantityChoice_2Impl()
		{
			_numLine.addEventListener("genererNumeroLigneUnique",listnerNumeroLine);
			addEventListener("compteLastMontant",compteLastMontantHandler);
			addEventListener("nextPageClicked",checkIfAttributToCommandeFill);
			addEventListener("eraseThisCollaborateurConfig",eraseFromListCommande);
			addEventListener("refreshGrid",refreshGridHandler);
		}
		
//METHODE PUBLIC-------------------------------------------------------------------------

//METHODE PROTECTED----------------------------------------------------------------------
		
		protected function btnNewCollabClickHandler():void
		{
			newCollabo = new InfosPersoCollaborateurIHM();
			newCollabo.IDPOOL_GESTIONNAIRE = _cmd.IDPOOL_GESTIONNAIRE;
			newCollabo.ip_collaborateur = newCollabo;
			PopUpManager.addPopUp(newCollabo,this,true);
			PopUpManager.centerPopUp(newCollabo);
		}
		
		protected function composantPanierCreationCompleteHandler():void
		{
			composantPanier.panier = _panier;
		}
		
		protected function dgRecapCommandeClickHandler():void
		{
			var object:Object = dgRecapCommande.selectedItem;
		}
		
		//AFFECTE A CHAQUE CONFIGURATION UN NUMERO DE COMMANDE
		public function showHandler():void
		{
			composantPanierCreationCompleteHandler();
			listCommande = new ArrayCollection();
			var generateNbr:int = Math.random()*1000000;
			for(var j:int = 0; j < _cmd.COMMANDE_TEMPORAIRE.length;j++)
			{	
				var numeroCommande:int 													= _cmd.COMMANDE.length + j +1;
				_cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION.IDPOOL_GESTIONNAIRE 	= _cmd.IDPOOL_GESTIONNAIRE;
				_cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION.TYPE 					= numeroCommande;
				_cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION.CONFIG_ID 			= numeroCommande;
				_cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION.NUMEROCONFIG_UNIQUE	= generateNbr;
				_cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION.ENABLEPORTABILITE		= checkIfMobileOnly(_cmd.COMMANDE_TEMPORAIRE[j]);
				_cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION.ERROR					= false;
				listCommande.addItem(_cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION);
			}
			callLater(refreshDatagrid);
		}
		
		private function checkIfMobileOnly(object:Object):Boolean
		{
			var yes:Boolean = true;
			if(object.TERMINAUX.length == 1 && skipSubscription)
			{
				yes = false;
			}
			
			return yes;
		}
		
		//AJOUTE UNE CONFIGURATION IDENTIQUE A CELLE PREPAREE
		protected function ajouterConfigurationClickHandler():void
		{
			_cmd.COMMANDE_TEMPORAIRE.addItem(ObjectUtil.copy(_itemsSelected));
			addNumeroLine();
			showHandler();
			_cmd.CONFIG_NUMBER++;
		}
		
		//AFFICHE LA FATURE EN 'PDF'
		protected function btExporterClickHandler(event:MouseEvent):void
	    {
	    	if(_cmd.IDCOMMANDE > 0)
	    	{
	    		var export:Export = new Export();
	    		export.exporterCommandeEnPDF(_cmd);
	    	}
	    }

//METHODE PROTECTED----------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------
		
		//APPEL DE PROCEDURE PERMETTANT DE GENERER UN NUMERO DE LIGNE
		private function addNumeroLine():void
		{
			var len:int = _cmd.COMMANDE_TEMPORAIRE.length;
			if(_cmd.COMMANDE_TEMPORAIRE[len -1].EMPLOYE_CONFIGURATION.NUM_LINE == "")
			{
				_numLine.genererNumeroLigneUnique();
			}
		}
		
		//RAFRAICHI LE DATAGRID
		private function refreshDatagrid():void
		{
//			dgRecapCommande.dataProvider = listCommande;
//			listCommande.refresh();
			listCommande.itemUpdated(null);
			_panier.attributMontant(_itemsSelected);
		}
		
//EVENT----------------------------------------------------------------------------------	
		
		//DISPATCH UN EVENT DANS L'OBJET 'PANIER' POUR CALCULER LE MOTANT DE LA CONFIGURATION PRECEDANTE
		private function compteLastMontantHandler(e:Event):void
		{
			_panier.dispatchEvent(new Event("montantLastConfig"));
		}
		
		//REGARDE SI TOUS LES CHAMPS SONT BIEN REMPLIS
		private function checkIfAttributToCommandeFill(e:Event):void
		{
			dispatchEvent(new Event("pompom",true));///A RECHERHCE DANS LE RESTE DU PROJET
			var ok:Boolean = true;
			if(skipSubscription == true || skipAll == true)
			{
				if(skipSubscription)
				{
					doThis();
					ok = true;
				}
				else
				{
					if(_itemsSelected.ABO.length == 0 && _itemsSelected.OPTIONS.length == 0)
					{
						doThis();
						ok = true;
					}
					else
					{
						ok = doThis();
					}
				}
			}
			else
			{
				ok = doThis();
			}

			if(ok)
			{
				parameterFill = true;
			}
			else
			{
				parameterFill = false;
				
			}
			(dgRecapCommande.dataProvider as ArrayCollection).refresh();
		}
		
		private function doThis():Boolean
		{
			var ok:Boolean = true;
			for(var i:int = 0;i < listCommande.length;i++)
			{
				if(listCommande[i].PORTABILITE)
				{
					if(listCommande[i].NUM_LINE == "" ||
						listCommande[i].NUM_RIO == "" ||
						listCommande[i].DATE_PORTABILITE == null ||
						listCommande[i].DATE_PORTABILITE == "" ||
						listCommande[i].LIBELLE_CONFIGURATION == "Non affecté" ||
						listCommande[i].LIBELLE_CONFIGURATION == "")
					{
						ok = false;
						listCommande[i].ERROR = true;
					}
				}
				else
				{
					if(	listCommande[i].LIBELLE_CONFIGURATION == "Non affecté" ||
						listCommande[i].LIBELLE_CONFIGURATION == "")
					{
						ok = false;
						listCommande[i].ERROR = true;
					}
				}
			}
			return ok;
		}
		
		//EFFACE LA CONFIGURATION SELECTIONNEE
		private function eraseFromListCommande(e:Event):void
		{
			if(dgRecapCommande.selectedItem != null)
			{
				if(listCommande.length != 1)
				{
					for(var j:int = 0; j < _cmd.COMMANDE_TEMPORAIRE.length;j++)
					{
						if(dgRecapCommande.selectedItem.NUM_LINE == _cmd.COMMANDE_TEMPORAIRE[j].EMPLOYE_CONFIGURATION.NUM_LINE)
						{
							_cmd.COMMANDE_TEMPORAIRE.removeItemAt(j);
							_cmd.CONFIG_NUMBER--;
						}
					}
				}
			}
			
			showHandler();
			refreshDatagrid();
			_panier.attributMontant(_itemsSelected);
		}
		
		//ECOUTE LE RETOUR DE PROCEDURE POUR GENERER UNE LINE TEMPPORAIRE  ET ATTRIBUT CE NUMERO A LA CONFIGURATION AJOUTEE
		private function listnerNumeroLine(e:Event):void
		{
			if(_numLine.boolOK)
			{
				var len:int = _cmd.COMMANDE_TEMPORAIRE.length;
				_cmd.COMMANDE_TEMPORAIRE[len -1].EMPLOYE_CONFIGURATION.NUM_LINE = _numLine.numeroUnique;
			}
		}
		
		//RAFRAICHI LE DATAGRID
		private function refreshGridHandler(e:Event):void
		{
			dgRecapCommande.dataProvider = listCommande;
		}

//EVENT----------------------------------------------------------------------------------

//METHODE PRIVATE------------------------------------------------------------------------

	}
}