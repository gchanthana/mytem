package gestionparc.ihm.action.dissocier.actionDissocierCollabfromSim
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierCollabFromSimCommand implements ICommand
	{
		private var receiver:ActionDissocierCollabFromSimReceiver=null;

		public function ActionDissocierCollabFromSimCommand(rec:ActionDissocierCollabFromSimReceiver):void
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
