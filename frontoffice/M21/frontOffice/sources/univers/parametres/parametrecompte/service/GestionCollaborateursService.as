package univers.parametres.parametrecompte.service
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	[Bindable]
	public class GestionCollaborateursService
	{
		private var _model:GestionCollaborateursModel;
		public var handler:GestionCollaborateursHandler;
		
		public function GestionCollaborateursService()
		{
			_model = new GestionCollaborateursModel();
			handler = new GestionCollaborateursHandler(_model);
		}
		
		public function get model():GestionCollaborateursModel
		{
			return _model;
		}

		public function set model(value:GestionCollaborateursModel):void
		{
			_model = value;
		}

		public function getGestionCollaborateurs(choix:Number):void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.GestionCollaborateur",
				"getGestionCollaborateur",
				handler.gestionCollaborateursHandler);
			RemoteObjectUtil.callService(opData,choix);
			
		}
		
		public function getRacineGestCollab():void
		{
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M21.GestionCollaborateur",
				"getRacineGestCollab",
				handler.initgestionCollaborateursHandler);
			RemoteObjectUtil.callService(opData);
			
		}
		
	}
}