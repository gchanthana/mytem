package gestionparc.ihm.action.associer.actionAssocierLigneExtToEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionAssocierLigneExtToEqpCommand implements ICommand
	{
		private var receiver:ActionAssocierLigneExtToEqpReceiver=null;

		public function ActionAssocierLigneExtToEqpCommand(rec:ActionAssocierLigneExtToEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
