package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.LigneFacturation")]
	 
	[Bindable]
	public class LigneFacturation
	{

		public var id:Number = 0;
		public var idProduit:Number = 0;
		public var libelleProduit:String = "";
		public var themeId:Number = 0;
		public var themeLibelle:String = "";
		public var operateurId:Number = 0;
		public var operateurLibelle:String = "";
		public var typeTheme:String = "";
		public var typeTrafic:String = "";
		public var poids:Number = 0;
		public var pourControle:Boolean = false;
		public var prixUnitaire:Number = 0;
		public var prixUnitaireRemise:Number = 0;
		public var remiseContrat:Number = 0;
		public var dateDebutVersion:Date = null;
		public var dateFinVersion:Date = null;
		public var dateDebut:Date = null;
		public var dateFin:Date = null;
		public var qte:Number = 0;
		public var montantTotal:Number = 0;
		public var montantCalculeBrut:Number = 0;
		public var montantCalculeRemise:Number = 0;
		public var difference:Number = 0;
		public var poidsDifference:Number = 0;
		public var montantMoyen:Number = 0;
		public var duree:Number = 0;
		public var boolPublic:Number = 0;
		
		public var typeTarif:Number = 0;
		public var libelleCompte:String = "";
		public var libelleSousCompte:String = "";
		public var libelleTypeTarif:String = "";
		public var libelleSourceTarif:String = "";
		
		
		


		public function LigneFacturation()
		{
		}

	}
}
