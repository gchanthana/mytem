package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.ligne
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCarrier;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;
	
	public class RuleCommanderNvlSIM implements IRuleDisplayItem
	{
		public function RuleCommanderNvlSIM()
		{
		}
		
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new IsTypeLigneMobile();
			//var r2:IRuleDisplayItem = new HasSimAssociated();
			var r2:IRuleDisplayItem = new HasCarrier();
			if(r1.isDisplay() && r2.isDisplay())	
				return true;
			else
				return false;
		}
	}
}