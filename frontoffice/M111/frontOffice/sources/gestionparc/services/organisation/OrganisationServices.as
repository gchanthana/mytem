package gestionparc.services.organisation
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class OrganisationServices
	{
		
		public var myDatas 		: OrganisationDatas;
		public var myHandlers 	: OrganisationHandlers;
		
		public function OrganisationServices()
		{
			myDatas 	= new OrganisationDatas();
			myHandlers 	= new OrganisationHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - Tous les noeuds d'une organisation précédent une feuille.
		 * - La structure de l'organisation.
		 */	
		public function getNodesOrga(idOrga:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.Organisation",
				"getNodesOrga",
				myHandlers.getNodesOrgaResultHandler);
			
			RemoteObjectUtil.callService(op, idOrga);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - Toutes les informations liées aux organisations du collaborateurs.
		 */	
		public function getListOrgaCliente():void 
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.Organisation",
				"getListOrgaCliente", myHandlers.getListOrgaClienteResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
	}
}