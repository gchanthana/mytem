package intranet.cataloguepublic.vues
{
	import composants.util.CvDateChooser;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import intranet.cataloguepublic.controles.AbstractGestionTarifs;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.NumericStepper;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;

	public class AjoutVersionTarif extends TitleWindow
	{
		//Controles
		public var cbFrequence : ComboBox;
		public var nsQte : NumericStepper;	
		
		[Bindable]
		public var txtPrixU : TextInput;
		public var txtRemise : TextInput;
		public var txtPrixURemise : TextInput;
		public var cbDevise : ComboBox;
		public var dfDateDebut : CvDateChooser;
		
		public var btAjouter : Button;
		public var btAnnuler : Button;
		
		[Bindable]
		protected var _gestionTarif : AbstractGestionTarifs;
		
		//Event
		public static const AJOUTER : String = "AjouterVersionTarif";
		public static const ANNULER : String = "AnnulerVersion";
		
		
		
		
		
		
		
		public function AjoutVersionTarif(){
			
		}
		
		[Bindable]
		public function set gestionTarif(gestionDeTarif : AbstractGestionTarifs):void{
			_gestionTarif = gestionDeTarif;
		}
		
		public function get gestionTarif():AbstractGestionTarifs{
		return _gestionTarif;
		
		}
		
		
		///HANDLERS
		protected function ajoutVersionTarifCloseHandler(ce : CloseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		protected function btAnnulerClickHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
		
		protected function btAjouterClickHandler(me : MouseEvent):void{
			Alert.yesLabel = "Oui";
			Alert.cancelLabel = "Annuler";
			Alert.show("Etes vous sur de vouloir faire cette opération","Confirmer",Alert.YES | Alert.CANCEL,
			this,confirmerAjout);	
		}
		
		protected function txtRemiseChangeHandler(event : Event):void{						
			var prixRemise : Number = Number(txtPrixU.text) - (Number(txtPrixU.text) * Number(txtRemise.text) / 100);
			
			if (gestionTarif.selectedProduit.type.toLowerCase() == 'abo'){
				txtPrixURemise.text = prixRemise.toFixed(2);	
			}else{
				txtPrixURemise.text = prixRemise.toFixed(4);
			}
		}
		
		protected function txtPrixURemiseChangeHandler(event : Event):void{
			txtPrixU.text = "0";
			txtRemise.text = "0";
		}
		
		
		protected function init(fe : FlexEvent):void{
			dfDateDebut.selectedDate = ObjectUtil.copy(_gestionTarif.selectedVersion.dateDebut) as Date;
			
			if (_gestionTarif != null && _gestionTarif.selectedVersion != null && _gestionTarif.selectedVersion.idVersionTarif != 0){
				dfDateDebut.enabled = false;
			}else{
				dfDateDebut.enabled = true;
			}
		}
		
		protected function dfDateDebutChangeHandler(ev : Event):void{			
			/* trace(ObjectUtil.toString(dfDateDebut.selectedDate));
			trace(ObjectUtil.toString(dfDateDebut.selectedItem));
			trace(ObjectUtil.toString(dfDateDebut.text));
			trace(ObjectUtil.toString(new Date(dfDateDebut.text.substr(3,4),Number(dfDateDebut.text.substr(0,2))-1)));
			trace(DateFunction.formatDateAsString(new Date(dfDateDebut.text.substr(3,4),Number(dfDateDebut.text.substr(0,2))-1))); */
		}
		
		//PRIVATE 
		private function confirmerAjout(cle : CloseEvent):void{
			var message : String = "";
			var bool : Boolean = true;
			if (cle.detail == Alert.YES){
				if (txtPrixU.text.length == 0){
					message = message + "- Le prix unitaire\n";
					bool = false;
				}
				if (txtRemise.text.length == 0){
					message = message + "- La remise\n";
					bool = false
				}
				if (dfDateDebut.text.length == 0){
					message = message + "- La date de début de validité\n";
					bool = false
				}				
				if	(bool){
					_gestionTarif.selectedVersion.dateDebut = new Date(dfDateDebut.text.substr(3,4),Number(dfDateDebut.text.substr(0,2))-1);
					_gestionTarif.selectedVersion.remiseContrat = Number(txtRemise.text);
					_gestionTarif.selectedVersion.prixUnitaire = Number(txtPrixU.text);
					_gestionTarif.selectedVersion.prixRemise = Number(txtPrixURemise.text);
					callLater(_gestionTarif.enregistrerVersionTarif);	
					PopUpManager.removePopUp(this);
				}else{
					Alert.show(message,"Les champs suivants sont obligatoires");
				}		
				
			}else{
				//On fait rien pour le moment	
			}
		}
	}
}