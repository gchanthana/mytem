<cfcomponent author="Cedric" hint="POC d'activation/désactivation du roaming (Citrix)">
	<cffunction access="public" name="updateRoaming" returntype="void" hint="Ne nécessite pas d'identifier le device">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="phoneNumber" type="String" required="true" hint="Identifiant du device">
		<cfargument name="osFamilly" type="String" required="true" hint="OS du device (iOS,ANDROID)">
		<cfargument name="enableRoaming" type="Boolean" required="true" hint="TRUE pour activer le roaming et FALSE sinon">
		<cfset var everyWanPackage=getRemoteService(ARGUMENTS.idGroupe,"EveryWanPackage")>
		<!--- Création de la règle de déploiement --->
		<cfset var rule=createDeploymentRule(ARGUMENTS.phoneNumber,ARGUMENTS.osFamilly,ARGUMENTS.enableRoaming)>
		<!--- Vérification de la règle de déploiement (iOS only) --->
		<cfif LEN(TRIM(rule.rule)) GT 0>
			<cfset var status=everyWanPackage.checkPackageRule(rule.rule)>
			<cfif status>
				<cfset var currentRule=everyWanPackage.getPackageRule(rule.package)>
				<cfif isDefined("currentRule")>
					<!--- Suppression de la règle existante --->
					<cfset everyWanPackage.removePackageRule(rule.package)>
				</cfif>
				<!--- Affectation de la règle --->
				<cfset everyWanPackage.setPackageRule(rule.package,rule.rule)>
			</cfif>
		</cfif>
		<!--- Affectation du user dans le groupe --->
		<cfset var everyWanUserGroup=getRemoteService(ARGUMENTS.idGroupe,"EveryWanUserGroup")>
		<cfset everyWanUserGroup.setUserGroup(rule.user,rule.group)>
		<!--- Déploiement du package (Déploie tous les packages iOS et ANDROID)--->
		<cfset everyWanPackage.deploy()>
	</cffunction>
	
	<cffunction access="private" name="createDeploymentRule" returntype="Struct">
		<cfargument name="phoneNumber" type="String" required="true" hint="Identifiant du device">
		<cfargument name="osFamilly" type="String" required="true" hint="OS du device (iOS,ANDROID)">
		<cfargument name="enableRoaming" type="Boolean" required="true" hint="TRUE pour activer le roaming et FALSE sinon">
		<cfif findNoCase("iOS",ARGUMENTS.osFamilly) GT 0>
			<cfreturn createIOSUpdateRoamingRule(ARGUMENTS.phoneNumber,ARGUMENTS.enableRoaming)>
		<cfelseif findNoCase("ANDROID",ARGUMENTS.osFamilly) GT 0>
			<cfreturn createAndroidUpdateRoamingRule(ARGUMENTS.phoneNumber,ARGUMENTS.enableRoaming)>
		<cfelse>
			<cfthrow type="Custom" message="Invalid OS #ARGUMENTS.osFamilly#">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="createIOSUpdateRoamingRule" returntype="Struct">
		<cfargument name="phoneNumber" type="String" required="true" hint="Identifiant du device">
		<cfargument name="enableRoaming" type="Boolean" required="true" hint="TRUE pour activer le roaming et FALSE sinon">
		<cfset var package="swd_stop_roaming_ios">
		<cfset var iOSRule="">
		<cfxml variable="iOSRule">
			<function name="and"><function name="deviceProp"><string value="TEL_NUMBER"/><string value="eq"/><string value=""/></function></function>
		</cfxml>
		<cfset var phoneNumberNode=iOSRule.xmlRoot.function.string[3]>
		<cfset phoneNumberNode.xmlAttributes.value=ARGUMENTS.phoneNumber>
		<cfreturn {
			user="saaswedo1",group="Saaswedo.swd_stop_roaming_group",package=package,rule=toString(iOSRule)
		}>
	</cffunction>
	
	<cffunction access="private" name="createAndroidUpdateRoamingRule" returntype="Struct">
		<cfargument name="phoneNumber" type="String" required="true" hint="Identifiant du device">
		<cfargument name="enableRoaming" type="Boolean" required="true" hint="TRUE pour activer le roaming et FALSE sinon">
		<cfset var package="swd_stop_roaming_android">
		<cfreturn {
			user="saaswedo2",group="Saaswedo.swd_stop_roaming_group",package=package,rule=""
		}>
	</cffunction>
	
	<cffunction access="private" name="getMdmServerInfos" returntype="Struct" hint="Retourne les infos du serveur MDM de la racine">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfset var idRacine=ARGUMENTS.idGroupe>
		<cfset var dataSource="ROCOFFRE">
		<!--- Récupération des infos MDM de la racine --->
		<cfset var procedureName="PKG_MDM.get_server_racine_v2">
		<cfstoredproc datasource="#datasource#" procedure="#procedureName#">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="procedure_status">
			<cfprocresult name="qMdmServerInfos">
		</cfstoredproc>
		<cfif (qMdmServerInfos.recordcount GT 0) AND (procedure_status GT 0)>
			<!--- Code d'erreur --->
			<cfset var code="MDM.ERROR">
			<!--- Structure des infos MDM : La valeur ramenée de la propriété MDM doit etre de la forme "PROVIDER://URI" --->
			<cfset var mdmServerInfos={
				MDM_TYPE=qMdmServerInfos["MDM_TYPE"][1],MDM=qMdmServerInfos["MDM"][1],USE_SSL=yesNoFormat(VAL(qMdmServerInfos["USE_SSL"][1])),
				USERNAME=qMdmServerInfos["USERNAME"][1],PASSWORD=qMdmServerInfos["PASSWORD"][1],USERID=VAL(qMdmServerInfos["USERID"][1]),
				ENROLL_USERNAME=qMdmServerInfos["ENROLL_USERNAME"][1],ENROLL_PASSWORD=qMdmServerInfos["ENROLL_PASSWORD"][1],
				IS_DEFAULT=yesNoFormat(VAL(qMdmServerInfos["IS_DEFAULT"][1])),ADMIN_EMAIL=qMdmServerInfos["ADMIN_EMAIL"][1],
				TEST_MSG=qMdmServerInfos["TEST_MSG"][1],IS_TEST_OK=yesNoFormat(VAL(qMdmServerInfos["IS_TEST_OK"][1]))
			}>
			<!--- Vérification de la valeur de la propriété MDM --->
			<cfset var mdmValue=mdmServerInfos.MDM>
			<cfif (TRIM(mdmValue) EQ "") OR (TRIM(mdmValue) NEQ mdmValue)>
				<cfset var extendedInfos=isDefined("qMdmServerInfos") ? serializeJSON(qMdmServerInfos):"">
				<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns invalid MDM '#mdmValue#' for idGroupe #idRacine#">
			<cfelse>
				<!--- Propriétés MDM et MDM_TYPE --->
				<cfset mdmServerInfos.MDM_TYPE=extractMdmProvider(mdmValue)>
				<cfset mdmServerInfos.MDM=extractMdm(mdmValue)>
				<!--- Traitement et conversion des valeurs (Le type Booléen de Java est utilisé pour compatibilité avec ActionScript) --->
				<cfset mdmServerInfos.IS_DEFAULT=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_DEFAULT ? "true":"false")>
				<cfset mdmServerInfos.USE_SSL=createObject("java","java.lang.Boolean").init(mdmServerInfos.USE_SSL ? "true":"false")>
				<cfset mdmServerInfos.IS_TEST_OK=createObject("java","java.lang.Boolean").init(mdmServerInfos.IS_TEST_OK ? "true":"false")>
				<cfreturn mdmServerInfos>
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="#code#" message="#procedureName# returns status #procedure_status# for idGroupe #idRacine#">
		</cfif>
	</cffunction>
	
	<!--- Utilitaires : Infos serveur MDM --->
	
	<cffunction access="private" name="extractMdmProvider" returntype="String" hint="Retourne la valeur du Provider MDM correspondant">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmProvider=mdmTokens[1]>
		<cfif TRIM(mdmProvider) NEQ mdmProvider>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmProvider#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid Provider #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmProvider>
	</cffunction>
	
	<cffunction access="private" name="extractMdm" returntype="String" hint="Retourne l'URI d'accès au MDM sans le protocole (ou le Scheme)">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=extractMdmTokens(ARGUMENTS.mdm)>
		<cfset var mdmValue=mdmTokens[2]>
		<cfif TRIM(mdmValue) NEQ mdmValue>
			<cfset var code="MDM.ERROR">
			<cfset var errorMsg="'#mdmValue#' extracted from MDM value '#ARGUMENTS.mdm#'">
			<cfthrow type="Custom" errorCode="#code#" message="Invalid URI #errorMsg#. Cannot contain extra spaces">
		</cfif>
		<cfreturn mdmValue>
	</cffunction>
	
	<cffunction access="private" name="extractMdmTokens" returntype="Array" hint="Retourne les éléments du découpage d'une la valeur MDM d'origine">
		<cfargument name="mdm" type="String" required="true" hint="Valeur d'origine de la propriété MDM provenant de la base">
		<cfset var mdmTokens=listToArray(ARGUMENTS.mdm,"://",FALSE,TRUE)>
		<cfif arrayLen(mdmTokens) EQ 2>
			<cfreturn mdmTokens>
		<cfelse>
			<cfset var code="MDM.ERROR">
			<cfthrow type="Custom" errorCode="#code#" message="Cannot extract Provider and URI tokens from invalid MDM value '#ARGUMENTS.mdm#'">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getRemoteService" returntype="Any">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant de la racine">
		<cfargument name="serviceName" type="String" required="true" hint="Identifiant de la racine">
		<cfset var mdmInfos=getMdmServerInfos(ARGUMENTS.idGroupe)>
		<cfif structKeyExists(mdmInfos,"MDM_TYPE")>
			<cfif compareNoCase(mdmInfos.MDM_TYPE,"zenprise") EQ 0>
				<cfif structKeyExists(mdmInfos,"MDM") AND structKeyExists(mdmInfos,"USE_SSL")>
					<cfset var mdm=(mdmInfos.USE_SSL ? "https":"http") & "://" & mdmInfos.MDM>
					<cfif structKeyExists(mdmInfos,"USERNAME") AND structKeyExists(mdmInfos,"PASSWORD")>
						<cfset var stubEndpoint=mdm & "/services/" & ARGUMENTS.serviceName & "?wsdl">
						<cfset var stub=createObject("webservice",stubEndpoint)>
						<cfset stub.setUsername(mdmInfos.USERNAME)>
						<cfset stub.setPassword(mdmInfos.PASSWORD)>
						<cfreturn stub>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfthrow type="Custom" message="Failed to access Web Service">
	</cffunction>
</cfcomponent>