package rapport.service.chercherrapport 
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class RechercherRapportHandler
	{
		private var _model:RechercherRapportModel;
		
		public function RechercherRapportHandler(_model:RechercherRapportModel):void
		{
			this._model = _model;
		}
		
		internal function getRechercherRapportResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateRapports(event.result as ArrayCollection);
			}
		}	
	}
}