package univers.inventaire.equipements.lignes.v2.vo
{
	[Bindable]
	public class Ligne	
	{	

		public var DATE_COMMANDE:Date = null;
		public var DATE_RESILIATION:Date = null;
		public var DATE_SUSPENSION:Date = null;
		public var ETAT:String = "";
		public var IDSOUS_TETE:Number = 0;
		public var IDTETE_LIGNE:Number = 0;
		public var IDTYPE_LIGNE:Number = 0;
		public var IDTYPE_RACCORDEMENT:Number = 0;
		public var SOUS_TETE:String = "";
		public var TETE_LIGNE:String = "";
		public var TYPE_RACCORDEMENT:String = "";
		public var TYPE_FICHELIGNE:String = "";
		public var LIBELLE_TYPE_LIGNE:String = "";
		public var USAGE:String = "";
		public var NOM:String = "";
		public var PRENOM:String = "";
		public var COLLABORATEURID:Number=0;	
		public var MAJ_AUTO_COLLABORATEUR:Number;
		public var COMPTE:String = "";	
				
		public function Ligne()
		{	
			super();
		}
		
	}
}