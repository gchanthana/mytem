<!---
Auteur : Cï¿½dric RAPIERA
Facture SFR BUSINESS TEAM (ConsoView / ORACLE BI PUBLISHER)
--->
<cfcomponent name="Facture534" alias="fr.consotel.consoview.Facture.Facture534" extends="fr.consotel.consoview.Facture.AbstractFacture">
	<!--- 
	<cffunction name="afficherFacture" access="public" returntype="void" output="true">
		<cfargument name="ID" required="false" type="string">
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfobject name="prs" type="component" component="fr.consotel.consoview.util.publicreportservice.PublicReportServiceV2">
		<cfset prs.createRunReportRequest("consoview","public","/consoview/facturation/facture/Facture/Facture.xdo","COMPLET","pdf")>
		<cfset prs.AddRunReportParameter("P_IDRACINE","#SESSION.PERIMETRE.ID_GROUPE#")>
		<cfset prs.AddRunReportParameter("P_IDINVENTAIRE_PERIODE","#variables.instance.IDINVENTAIRE_PERIODE#")>
		<cfset prs.runReport()>
		<cfheader name="Content-Disposition" value="inline;filename=facture.#prs.getExtension("COMPLET","pdf")#">
		<cfcontent type="#prs.getRunReportContentType()#" variable="#prs.getRunReportData()#">
	</cffunction>
 --->
	<cffunction name="afficherFacture" access="public" returntype="void" output="true">
		<cfargument name="ID" required="false" type="string">
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfargument name="idGrpCtrl" required="false" type="numeric">
		<cfobject name="prs" type="component" component="fr.consotel.consoview.util.publicreportservice.PublicReportServiceV2">
		<cfset prs.createRunReportRequest("consoview","public","/consoview/facturation/facture/Facture/Facture.xdo","CTRL","pdf")>
		<cfset prs.AddRunReportParameter("P_IDRACINE","#SESSION.PERIMETRE.ID_GROUPE#")>
		<cfset prs.AddRunReportParameter("P_IDINVENTAIRE_PERIODE","#variables.instance.IDINVENTAIRE_PERIODE#")>
		<cfif isDefined("ID_GRP_CTRL")>
			<cfset prs.AddRunReportParameter("P_IDGRP_CTRL","#idGrpCtrl#")>
		</cfif>
		<cfset prs.runReport()>
		<cfheader name="Content-Disposition" value="inline;filename=facture.#prs.getExtension("COMPLET","pdf")#">
		<cfcontent type="#prs.getRunReportContentType()#" variable="#prs.getRunReportData()#">
	</cffunction>
	
	<cffunction name="afficherFactureCat" access="public" returntype="void" output="true" hint="Permet d'afficher la facture">
		<cfargument name="ID" required="false" type="string" hint="ID Site ou ID sous tete (Ligne) pas nï¿½cessaire pour Complï¿½te">
		<cfargument name="IDPerimetre" required="true" type="numeric">
		<cfargument name="factureParams" required="true" type="struct">
		<cfobject name="prs" type="component" component="fr.consotel.consoview.util.publicreportservice.PublicReportServiceV2">
		<cfset prs.createRunReportRequest("consoview","public","/consoview/facturation/facture/Facture/Facture.xdo","RESUME","pdf")>
		<cfset prs.AddRunReportParameter("P_IDRACINE","#SESSION.PERIMETRE.ID_GROUPE#")>
		<cfset prs.AddRunReportParameter("P_IDINVENTAIRE_PERIODE","#variables.instance.IDINVENTAIRE_PERIODE#")>
		<cfset prs.runReport()>
		<cfheader name="Content-Disposition" value="inline;filename=facture.#prs.getExtension("RESUME","pdf")#">
		<cfcontent type="#prs.getRunReportContentType()#" variable="#prs.getRunReportData()#">
	</cffunction>
</cfcomponent>
