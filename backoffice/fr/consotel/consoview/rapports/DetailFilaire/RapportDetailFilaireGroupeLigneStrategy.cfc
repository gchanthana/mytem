<!---
Package : fr.consotel.consoview.rapports.detailFilaire
--->
<cfcomponent name="RapportDetailFilaireGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP1_DETAIL_FILAIRE_LIG">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam value="#rapportParams.PERIODICITE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>

	<cffunction name="getInternational" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="NB_DEST_MAX" required="true" type="numeric">
        <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP2_DETAIL_FILAIRE_LIG">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam value="#rapportParams.PERIODICITE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qGetInternationalData">
        </cfstoredproc>
		<cfreturn qGetInternationalData>
	</cffunction>
	
	<cffunction name="GetTableau" access="private" returntype="query" output="true">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetperiod" dbtype="query">
			SELECT idref_client, libelle, ordre_affichage,
					type_theme, sur_theme, theme_libelle, idtheme_produit,
					SUM(qte) AS qte, SUM(montant_final) AS montant_final, SUM(nombre_appel) AS nombre_appel,
    				SUM(duree_appel) AS duree_appel
			from p_detail_lignes
			where 	mois_report >= #parseDateTime(RapportParams.date_m)#
					and mois_report <= #parseDateTime(RapportParams.date_m2)#
			group by idref_client, libelle, ordre_affichage,
						type_theme, sur_theme, theme_libelle, idtheme_produit
			order by idref_client, type_theme ASC, sur_theme ASC, idtheme_produit ASC
		</cfquery>
		<cfreturn qgetperiod>
	</cffunction>
	
	<cffunction name="GetTableauEvo" access="private" returntype="query" output="true">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfquery name="qgetevo" dbtype="query">
			SELECT idref_client, libelle, ordre_affichage,
					type_theme, sur_theme, theme_libelle, idtheme_produit,
					SUM(montant_final) AS montant_evo
			from p_detail_lignes
			where 	mois_report >= #parseDateTime(RapportParams.date_m_moins_deux)#
					and mois_report <= #parseDateTime(RapportParams.date_m_moins_un)#
			group by idref_client, libelle, ordre_affichage,
						type_theme, sur_theme, theme_libelle, idtheme_produit
			order by idref_client, type_theme ASC, sur_theme ASC, idtheme_produit ASC
		</cfquery>
		<cfreturn qgetevo>
	</cffunction>
	
	<cffunction name="qGetListeSocietes" access="private" returntype="query" output="true">
		<cfargument name="qQuery" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qListeSocietes" dbtype="query">
			select idref_client, libelle
			from qQuery
			group by idref_client, libelle
			order by idref_client
		</cfquery>
		<cfreturn qListeSocietes>
	</cffunction>
	
	<cffunction name="qGetListeThemesHierarchy" access="private" returntype="query" output="true">
		<cfargument name="qQuery" required="true" type="query"/>
		<cfquery name="qListeThemes" dbtype="query">
			select type_theme, sur_theme, theme_libelle, idtheme_produit
			from qQuery
			group by type_theme, sur_theme, theme_libelle, idtheme_produit
			order by type_theme ASC, sur_theme ASC, idtheme_produit ASC
		</cfquery>
		<cfreturn qListeThemes>
	</cffunction>
	
	<cffunction name="qGetListeTypesThemes" access="private" returntype="query" output="true">
		<cfargument name="qQuery" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qListeTypesThemes" dbtype="query">
			select type_theme
			from qQuery
			group by type_theme
			order by type_theme ASC
		</cfquery>
		<cfreturn qListeTypesThemes>
	</cffunction>
	
	<cffunction name="qGetListeSurThemes" access="private" returntype="query" output="true">
		<cfargument name="qQuery" required="true" type="query"/>
		<!--- METHODE REQUETTE query of queries --->
		<cfquery name="qListeSurThemes" dbtype="query">
			select type_theme, sur_theme
			from qQuery
			group by type_theme, sur_theme
			order by type_theme ASC, sur_theme ASC
		</cfquery>
		<cfreturn qListeSurThemes>
	</cffunction>
	
	<cffunction name="qGetListeThemes" access="private" returntype="query" output="true">
		<cfargument name="qQuery" required="true" type="query"/>
		<cfquery name="qListeThemes" dbtype="query">
			select idtheme_produit, theme_libelle
			from qQuery
			group by idtheme_produit, theme_libelle
			order by theme_libelle ASC
		</cfquery>
		<cfreturn qListeThemes>
	</cffunction>
	
	<cffunction name="fillTableauDetails" access="private" returntype="struct" output="true">
		<cfargument name="detailsTableauQuery" required="true" type="query"/>
		<cfargument name="detailsEvoTableauQuery" required="true" type="query"/>
		<cfargument name="listeSocietes" required="true" type="query"/>
		<cfargument name="listeThemesHierarchy" required="true" type="query"/>
		<cfset nbTypesThemes=0>
		<cfset totalMontantsTypesThemes=0>
		<cfset nbSurThemes=0>
		<cfset detailsStruct=structnew()>
		<cfoutput query="listeThemesHierarchy" group="type_theme">
			<cfset nbTypesThemes=nbTypesThemes + 1>
			<cfset typeThemeStruct=structnew()>
			<cfoutput group="sur_theme">
				<cfset nbSurThemes=nbSurThemes + 1>
				<cfset surThemeStruct=structnew()>
				<cfoutput group="idtheme_produit">
					<cfset themeStruct=structnew()>
					<cfloop query="listeSocietes">
						<cfset societeStruct=structnew()>
						<cfset societeStruct.qte=0>
						<cfset societeStruct.duree=0>
						<cfset societeStruct.montant=0>
						<cfset societeStruct.montant_evo=0>
						<cfset societeStruct.evo_average=0>
						<cfset societeStruct.pourc_evo_msg="NaN">
						<cfset var=structinsert(themeStruct,idref_client,societeStruct)>
					</cfloop>
					<cfset var=structinsert(surThemeStruct,idtheme_produit,themeStruct)>
				</cfoutput>
				<cfset var=structinsert(typeThemeStruct,sur_theme,surThemeStruct)>
			</cfoutput>
			<cfset var=structinsert(detailsStruct,type_theme,typeThemeStruct)>
		</cfoutput>
		<cfset detailsStruct.NB_COLS_TOTAL=1+3*(listeSocietes.recordcount)>
		<cfset detailsStruct.NB_ROWS_TOTAL=7+(5*nbTypesThemes)+(3*nbSurThemes)+listeThemesHierarchy.recordcount>

		<cfloop query="detailsTableauQuery">
			<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].qte=qte>
			<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].duree=(duree_appel)>
			<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].montant=montant_final>
		</cfloop>
		
		<cfloop query="detailsEvoTableauQuery">
				<cfset montant_actuel=detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].montant>
				<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].montant_evo=montant_evo>
				<cfset ancien_montant=detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].montant_evo>
			<cfif ancien_montant neq 0>
				<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].pourc_evo_msg="Number">
				<cfif montant_actuel neq 0>
					<cfset pourc_seuil=(1 - Abs((Abs(montant_actuel) - Abs(ancien_montant)) / montant_actuel))>
					<cfif Abs(pourc_seuil) lte 0.01>
						<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].pourc_evo_msg="NaN">
						<cfset pourc_evo_Temp=0>
					<cfelse>
						<cfset pourc_evo_Temp=(Sgn(montant_actuel - ancien_montant) * Abs((montant_actuel - ancien_montant) / ancien_montant))>
					</cfif>
					<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].evo_average=pourc_evo_Temp>
				<cfelse>
					<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].evo_average = -1>
				</cfif>
			<cfelse>
				<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].pourc_evo_msg="NaN">
				<cfset detailsStruct[type_theme][sur_theme][idtheme_produit][idref_client].evo_average = -999>
			</cfif>
		</cfloop>
		<cfreturn detailsStruct>
	</cffunction>
	
	<cffunction name="fillTableauSurThemesTotal" access="private" returntype="struct" output="true">
		<cfargument name="detailsTableauQuery" required="true" type="query"/>
		<cfargument name="detailsEvoTableauQuery" required="true" type="query"/>
		<cfargument name="listeSocietes" required="true" type="query"/>
		<cfargument name="listeThemesHierarchy" required="true" type="query"/>
		<cfset totalStruct=structnew()>
		<cfoutput query="listeThemesHierarchy" group="sur_theme">
			<cfset totalSurThemesStruct=structnew()>
			<cfloop query="listeSocietes">
				<cfset societeTotalSurThemeStruct=structnew()>
				<cfset societeTotalSurThemeStruct.totalMontantSurTheme=0>
				<cfset societeTotalSurThemeStruct.totalQteSurTheme=0>
				<cfset societeTotalSurThemeStruct.totalDureeSurTheme=0>
				<cfset var=structinsert(totalSurThemesStruct,idref_client,societeTotalSurThemeStruct)>
			</cfloop>
			<cfset var=structinsert(totalStruct,sur_theme,totalSurThemesStruct)>
		</cfoutput>
		<cfquery name="qgettot" dbtype="query">
			select sum(montant_final) as montant_final, sum(qte) as qte, sum(duree_appel) as duree_appel, idref_client, sur_theme
			from detailsTableauQuery
			group by idref_client, sur_theme
		</cfquery>
		<cfloop query="qgettot">
			<cfset totalStruct[sur_theme][idref_client].totalMontantSurTheme=montant_final>
			<cfset totalStruct[sur_theme][idref_client].totalQteSurTheme=qte>
			<cfset totalStruct[sur_theme][idref_client].totalDureeSurTheme=(duree_appel)>
		</cfloop>
		<cfreturn totalStruct>
	</cffunction>
	
	<cffunction name="fillTableauTypesThemesTotal" access="private" returntype="struct" output="true">
		<cfargument name="detailsTableauQuery" required="true" type="query"/>
		<cfargument name="detailsEvoTableauQuery" required="true" type="query"/>
		<cfargument name="listeSocietes" required="true" type="query"/>
		<cfargument name="listeThemesHierarchy" required="true" type="query"/>
		<cfset total1Struct=structnew()>
		<cfoutput query="listeThemesHierarchy" group="type_theme">
			<cfset totalTypeThemesStruct=structnew()>
			<cfloop query="listeSocietes">
				<cfset societeTotalTypeThemeStruct=structnew()>
				<cfset societeTotalTypeThemeStruct.totalMontantTypeTheme=0>
				<cfset societeTotalTypeThemeStruct.totalQteTypeTheme=0>
				<cfset societeTotalTypeThemeStruct.totalDureeTypeTheme=0>
				<cfset societeTotalTypeThemeStruct.totalMontantTypeTheme_Evo=0>
				<cfset var=structinsert(totalTypeThemesStruct,idref_client,societeTotalTypeThemeStruct)>
			</cfloop>
			<cfset var=structinsert(total1Struct,type_theme,totalTypeThemesStruct)>
		</cfoutput>
		<cfquery name="qgettot" dbtype="query">
			select sum(montant_final) as montant_final, sum(qte) as qte, sum(duree_appel) as duree_appel, idref_client, type_theme
			from detailsTableauQuery
			group by idref_client, type_theme
		</cfquery>
		<cfloop query="qgettot">
			<cfset total1Struct[type_theme][idref_client].totalMontantTypeTheme=montant_final>
			<cfset total1Struct[type_theme][idref_client].totalQteTypeTheme=qte>
			<cfset total1Struct[type_theme][idref_client].totalDureeTypeTheme=(duree_appel)>
		</cfloop>
		<cfreturn total1Struct>
	</cffunction>
	
	<cffunction name="GetTableauFt" access="private" returntype="query" output="true">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfargument name="p_operateurid" required="true" type="numeric" default="63"/>
		<cfquery name="qgetperiod" dbtype="query">
			SELECT idref_client, libelle, 
					type_theme, SUM(montant_final) AS montant_final
			from p_detail_lignes
			where 	mois_report >= #parseDateTime(RapportParams.date_m)#
					and mois_report <= #parseDateTime(RapportParams.date_m2)#
					and operateurid=#p_operateurid#
			group by idref_client, libelle, type_theme
			order by  idref_client, type_theme
		</cfquery>
		<cfreturn qgetperiod>
	</cffunction>
	
	<cffunction name="fillTableauTotalFinal" access="private" returntype="struct" output="true">
		<cfargument name="detailsTableauQuery" required="true" type="query"/>
		<cfargument name="listeSocietes" required="true" type="query"/>
		<cfset totalFinalStruct=structnew()>
		<cfloop query="listeSocietes">
			<cfset societeTotalFinalStruct=structnew()>
			<cfset societeTotalFinalStruct.totalMontantFinal=0>
			<cfset var=structinsert(totalFinalStruct,idref_client,societeTotalFinalStruct)>
		</cfloop>
		<cfquery name="qgettot" dbtype="query">
			select sum(montant_final) as montant_final, idref_client
			from detailsTableauQuery
			group by idref_client
		</cfquery>
		<cfloop query="qgettot">
			<cfset totalFinalStruct[idref_client].totalMontantFinal=montant_final>
		</cfloop>
		<cfreturn totalFinalStruct>
	</cffunction>
	
	<cffunction name="GetTableauFtEvo" access="private" returntype="query" output="true">
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfargument name="p_detail_lignes" required="true" type="query"/>
		<cfargument name="p_operateurid" required="true" type="numeric" default="63"/>
		<cfquery name="qgetevo" dbtype="query">
			SELECT idref_client, libelle, 
					type_theme, SUM(montant_final) AS montant_evo
			from p_detail_lignes
			where 	mois_report >= #parseDateTime(RapportParams.date_m_moins_deux)#
					and mois_report <= #parseDateTime(RapportParams.date_m_moins_un)#
					and operateurid=#p_operateurid#
			<!---
			where 	mois_report >= #createodbcdate(LsDateFormat(RapportParams.date_m_moins_deux,"yyyy/mm/dd"))#
					and mois_report <= #createodbcdate(LsDateFormat(RapportParams.date_m_moins_un,"yyyy/mm/dd"))#
					and operateurid=#p_operateurid#
			--->
			group by idref_client, libelle, type_theme
			order by  idref_client, type_theme
		</cfquery>
		<cfreturn qgetevo>
	</cffunction>
	
	<cffunction name="fillTableauFtDetails" access="private" returntype="struct" output="true">
		<cfargument name="detailsFtTableauQuery" required="true" type="query"/>
		<cfargument name="detailsFtEvoTableauQuery" required="true" type="query"/>
		<cfargument name="listeSocietes" required="true" type="query"/>
		<cfargument name="listeThemesHierarchy" required="true" type="query"/>
		<cfset detailsFtStruct=structnew()>
		<cfoutput query="listeThemesHierarchy" group="type_theme">
			<cfset typeThemeStruct=structnew()>
			<cfloop query="listeSocietes">
				<cfset societeStruct=structnew()>
				<cfset societeStruct.montant=0>
				<cfset societeStruct.montant_evo=0>
				<cfset societeStruct.evo_average=0>
				<cfset societeStruct.pourc_evo_msg="NaN">
				<cfset var=structinsert(typeThemeStruct,idref_client,societeStruct)>
			</cfloop>
			<cfset var=structinsert(detailsFtStruct,type_theme,typeThemeStruct)>
		</cfoutput>

		<cfloop query="detailsFtTableauQuery">
			<cfset detailsFtStruct[type_theme][idref_client].montant=montant_final>
		</cfloop>

		<cfloop query="detailsFtEvoTableauQuery">
			<cfset detailsFtStruct[type_theme][idref_client].montant_evo=montant_evo>
		</cfloop>

		<cfloop query="detailsFtTableauQuery">
			<cfset detailsFtStruct[type_theme][idref_client].montant=montant_final>
			<cfset montant_actuel=detailsFtStruct[type_theme][idref_client].montant>
			<cfset ancien_montant=detailsFtStruct[type_theme][idref_client].montant_evo>
			<cfif ancien_montant neq 0>
				<cfset detailsFtStruct[type_theme][idref_client].pourc_evo_msg="Number">
				<cfif montant_actuel neq 0>
					<cfset pourc_seuil=(1 - Abs((Abs(montant_actuel) - Abs(ancien_montant)) / montant_actuel))>
					<cfif pourc_seuil lte 0.01>
						<cfset detailsFtStruct[type_theme][idref_client].pourc_evo_msg="NaN">
						<cfset pourc_evo_Temp=0>
					<cfelse>
						<cfset pourc_evo_Temp=(Sgn(montant_actuel - ancien_montant) * Abs((montant_actuel - ancien_montant) / ancien_montant))>
					</cfif>
					<cfset detailsFtStruct[type_theme][idref_client].evo_average=pourc_evo_Temp>
				<cfelse>
					<cfset detailsFtStruct[type_theme][idref_client].evo_average = -1>
				</cfif>
			<cfelse>
				<cfset detailsFtStruct[type_theme][idref_client].pourc_evo_msg="NaN">
				<cfset detailsFtStruct[type_theme][idref_client].evo_average = -999>
			</cfif>
		</cfloop>
		<cfreturn detailsFtStruct>
	</cffunction>
	
	<cffunction name="createOtherReportFormatQuery" access="private" returntype="query" output="true">
		<cfargument name="detailsTableauQuery" required="true" type="query"/>
		<cfargument name="detailsEvoTableauQuery" required="true" type="query"/>
		<cfargument name="detailsFtTableauQuery" required="true" type="query"/>
		<cfargument name="detailsFtEvoTableauQuery" required="true" type="query"/>
		<cfset finalSocieteStruct=structnew()>
		<cfoutput query="detailsTableauQuery" group="idref_client">
			<cfset newSocieteID="_#idref_client#">
			<cfset structInsert(finalSocieteStruct,idref_client,newSocieteID)>
		</cfoutput>
		
		<cfset finalMontantEvoStruct=structnew()>
		<cfoutput query="detailsEvoTableauQuery" group="idref_client">
			<cfset societeStruct=structNew()>
			<cfoutput group="idtheme_produit">
				<cfset societeStruct['#idtheme_produit#']=montant_evo>
			</cfoutput>
			<cfset structInsert(finalMontantEvoStruct,idref_client,societeStruct)>
		</cfoutput>

		<cfset finalMontantFtStruct=structnew()>
		<cfoutput query="detailsFtTableauQuery" group="idref_client">
			<cfset societeStruct=structNew()>
			<cfoutput group="type_theme">
				<cfset societeStruct['#type_theme#']=montant_final>
			</cfoutput>
			<cfset structInsert(finalMontantFtStruct,idref_client,societeStruct)>
		</cfoutput>

		<cfset finalMontantFtEvoStruct=structnew()>
		<cfoutput query="detailsFtEvoTableauQuery" group="idref_client">
			<cfset societeStruct=structNew()>
			<cfoutput group="type_theme">
				<cfset societeStruct['#type_theme#']=montant_evo>
			</cfoutput>
			<cfset structInsert(finalMontantFtEvoStruct,idref_client,societeStruct)>
		</cfoutput>

		<cfset elem=1>
		<cfset finalSocieteArray=ArrayNew(1)>
		<cfset finalMontantEvoArray=ArrayNew(1)>
		<cfset finalMontantFtArray=ArrayNew(1)>
		<cfset finalMontantFtEvoArray=ArrayNew(1)>
		<cfset finalPourcEvoStringArray=ArrayNew(1)>
		<cfset finalPourcEvoFtStringArray=ArrayNew(1)>
		<cfoutput query="detailsTableauQuery" group="idref_client">
			<cfoutput group="idtheme_produit">
				<cfset finalMontantEvoArray[elem] = 0>
				<cfset finalMontantFtArray[elem] = 0>
				<cfset finalMontantFtEvoArray[elem] = 0>
				<cfset tmpID = finalSocieteStruct[#idref_client#]>
				<cfset finalSocieteArray[elem] = "#tmpID#">
				<cfif StructKeyExists(finalMontantEvoStruct,"#idref_client#")>
					<cfif StructKeyExists(finalMontantEvoStruct[#idref_client#],"#idtheme_produit#")>
						<cfset finalMontantEvoArray[elem] = finalMontantEvoStruct[#idref_client#][#idtheme_produit#]>
					</cfif>
				</cfif>
				<cfif StructKeyExists(finalMontantFtStruct,"#idref_client#")>
					<cfif StructKeyExists(finalMontantFtStruct[#idref_client#],"#type_theme#")>
						<cfset finalMontantFtArray[elem] = finalMontantFtStruct[#idref_client#][#type_theme#]>
					</cfif>
				</cfif>
				<cfif StructKeyExists(finalMontantFtEvoStruct,"#idref_client#")>
					<cfif StructKeyExists(finalMontantFtEvoStruct[#idref_client#],"#type_theme#")>
						<cfset finalMontantFtEvoArray[elem] = finalMontantFtEvoStruct[#idref_client#][#type_theme#]>
					</cfif>
				</cfif>
				<cfset finalPourcEvoStringArray[elem] = "N.D">
				<cfif finalMontantEvoArray[elem] neq 0>
					<cfif montant_final neq 0>
						<cfset pourc_seuil=(1 - Abs((Abs(montant_final) - Abs(finalMontantEvoArray[elem])) / montant_final))>
						<cfif Abs(pourc_seuil) gt 0.01>
							<cfset finalPourcEvoStringArray[elem] =
									"#(Sgn(montant_final - finalMontantEvoArray[elem]) * Abs((montant_final - finalMontantEvoArray[elem]) / finalMontantEvoArray[elem]))#">
						</cfif>
					</cfif>
				</cfif>
				<cfset finalPourcEvoFtStringArray[elem] = "N.D">
				<cfif finalMontantFtEvoArray[elem] neq 0>
					<cfif finalMontantFtArray[elem] neq 0>
						<cfset pourc_seuil=(1 - Abs((Abs(finalMontantFtArray[elem]) - Abs(finalMontantFtEvoArray[elem])) / finalMontantFtArray[elem]))>
						<cfif pourc_seuil gt 0.01>
							<cfset finalPourcEvoFtStringArray[elem] =
									"#(Sgn(finalMontantFtArray[elem] - finalMontantFtEvoArray[elem]) * Abs((finalMontantFtArray[elem] - finalMontantFtEvoArray[elem]) / finalMontantFtEvoArray[elem]))#">
						</cfif>
					</cfif>
				</cfif>
				<cfset elem = elem + 1>
			</cfoutput>
		</cfoutput>
		<cfset QueryAddColumn(detailsTableauQuery,"cIdref_client","VarChar",finalSocieteArray)>
		<cfset QueryAddColumn(detailsTableauQuery,"montant_evo","Integer",finalMontantEvoArray)>
		<cfset QueryAddColumn(detailsTableauQuery,"montant_ft","Integer",finalMontantFtArray)>
		<cfset QueryAddColumn(detailsTableauQuery,"montant_ft_evo","Integer",finalMontantFtEvoArray)>
		<cfset QueryAddColumn(detailsTableauQuery,"pourc_evo","VarChar",finalPourcEvoStringArray)>
		<cfset QueryAddColumn(detailsTableauQuery,"pourc_evo_ft","VarChar",finalPourcEvoFtStringArray)>
		<cfreturn detailsTableauQuery>
	</cffunction>
	
	<cffunction name="qGetTypeThemeCoutEvolution" access="private" returntype="query" output="true">
		<cfargument name="dataset" required="true" type="query" default=""/>
		<cfargument name="ID" required="true" type="numeric" default=""/>
		<cfargument name="periodiciteString" required="true" type="string" default=""/>
		<cfset MAX_ROWS = 6>
		<cfif UCase(periodiciteString) neq "MENSUEL">
			<cfset MAX_ROWS = (12)>
		</cfif>
		<cfquery name="result" dbtype="query" maxrows="#MAX_ROWS#">
			select idref_client, mois_report, type_theme, sum(montant_final) as montant_final
			from dataset
			where idref_client = #ID#
			group by idref_client, libelle, mois_report, type_theme
			order by type_theme, MOIS_REPORT DESC
		</cfquery>
		<cfquery name="resultFinal" dbtype="query">
			select idref_client, mois_report, type_theme, montant_final
			from result
			group by idref_client, mois_report, type_theme, montant_final
			order by type_theme, MOIS_REPORT ASC
		</cfquery>
		<cfreturn resultFinal>
	</cffunction>
	
	<cffunction name="qGetCoutsConsoStatsBySociete" access="private" returntype="query" output="false">
		<cfargument name="reportQuery" required="true" type="query" default=""/>
		<cfargument name="ID" required="true" type="numeric" default=""/>
		<cfquery name="qCoutConsoStats" dbtype="query">
			select IDREF_CLIENT, IDTHEME_PRODUIT, THEME_LIBELLE, MONTANT_FINAL
			from reportQuery
			where UPPER(TYPE_THEME) = UPPER('Consommations')
			and	idref_client = #ID#
			group by IDREF_CLIENT, IDTHEME_PRODUIT, THEME_LIBELLE, MONTANT_FINAL
			order by MONTANT_FINAL DESC
		</cfquery>
		<cfreturn qCoutConsoStats>
	</cffunction>
	
	<cffunction name="qGetVolumeConsoStatsBySociete" access="private" returntype="query" output="false">
		<cfargument name="reportQuery" required="true" type="query" default=""/>
		<cfargument name="ID" required="true" type="numeric" default=""/>
		<cfquery name="qVolumeStats" dbtype="query">
			select IDREF_CLIENT, IDTHEME_PRODUIT, THEME_LIBELLE, (DUREE_APPEL) as DUREE_APPEL
			from reportQuery
			where UPPER(TYPE_THEME) = UPPER('Consommations')
			and	idref_client = #ID#
			group by IDREF_CLIENT, IDTHEME_PRODUIT, THEME_LIBELLE, DUREE_APPEL
			order by DUREE_APPEL DESC
		</cfquery>
		<cfreturn qVolumeStats>
	</cffunction>
	
	<cffunction name="qGetTotalBySurTheme" access="private" returntype="struct" output="true">
		<cfargument name="reportQuery" required="true" type="query" default=""/>
		<cfquery name="qTotalBySurTheme" dbtype="query">
			select idref_client, sur_theme,
					sum(qte) as qte, sum(duree_appel) as duree_appel, sum(montant_final) as montant_final
			from reportQuery
			group by idref_client, sur_theme
			order by idref_client, sur_theme
		</cfquery>

		<cfset totalBySurThemeStruct = StructNew()>
		<cfoutput query="qTotalBySurTheme" group="idref_client">
			<cfset societeSurThemeTotal = structNew()>
			<cfoutput group="sur_theme">
				<cfset surThemeTotalStruct = StructNew()>
				<cfset surThemeTotalStruct.qte = qte>
				<cfset surThemeTotalStruct.duree_appel = (duree_appel)>
				<cfset surThemeTotalStruct.montant_final = montant_final>
				<cfset structInsert(societeSurThemeTotal,"#sur_theme#",surThemeTotalStruct)>
			</cfoutput>
			<cfset structInsert(totalBySurThemeStruct,"#idref_client#",societeSurThemeTotal)>
		</cfoutput>
		<cfreturn totalBySurThemeStruct>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<!---====== FORMATAGE DES DATES ======--->
		<cfset RapportParams.date_m = LSDateFormat(ParseDateTime(RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset RapportParams.date_m2=
				LSDateFormat(DateAdd("m",RapportParams.periodicite,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_un=
				LSDateFormat(DateAdd("m",-1,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_deux=
				LSDateFormat(DateAdd("m",(-1 - (RapportParams.periodicite)),ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_debut_annee = year(ParseDateTime(RapportParams.DATEDEB)) & "/01/01">
		<cfset rapportParams.chaine_date = rapportParams.DATE_M>
		<!---====================================================--->
		<cfset dataset=getReportData(ID_PERIMETRE,RapportParams)>
		<cfif dataset.recordcount neq 0>
			<cfif UCASE(arguments.RapportParams.FORMAT) EQ "CSV">
				<cfset qGetData=dataset>
				<cfset filename="Rapport_Telephonie_Fixe_Par_Societes_" & replace(session.perimetre.raison_sociale,' ','_',"all") & ".csv">
				<cfset exportService=createObject("component","fr.consotel.consoview.api.reporting.export.ExportService")>
				<cfset exportService.exportQueryToText(qGetData,filename)>
			<cfelse>
				<cfset listeSocietes=qGetListeSocietes(dataset)>
				<cfset listeThemesHierarchy=qGetListeThemesHierarchy(dataset)>
				<cfset detailsTableauQuery=GetTableau(RapportParams,dataset)>
				<cfset detailsEvoTableauQuery=GetTableauEvo(RapportParams,dataset)>
				<cfset detailsFtTableauQuery=GetTableauFt(RapportParams,dataset,63)>
				<cfset detailsFtEvoTableauQuery=GetTableauFtEvo(RapportParams,dataset,63)>
				<cfif UCase(arguments.RapportParams.FORMAT) eq "EXCEL">
					<cfset detailsTableauStruct=fillTableauDetails(detailsTableauQuery,detailsEvoTableauQuery,listeSocietes,listeThemesHierarchy)>
					<cfset detailsFtTableauStruct=fillTableauFtDetails(detailsFtTableauQuery,detailsFtEvoTableauQuery,listeSocietes,listeThemesHierarchy)>
					<cfset totalTypesThemes=fillTableauTypesThemesTotal(detailsTableauQuery,detailsEvoTableauQuery,listeSocietes,listeThemesHierarchy)>
					<cfset totalSurThemes=fillTableauSurThemesTotal(detailsTableauQuery,detailsEvoTableauQuery,listeSocietes,listeThemesHierarchy)>
					<cfset totalFinal=fillTableauTotalFinal(detailsTableauQuery,listeSocietes)>
					
					<cfset listeTypesThemes=qGetListeTypesThemes(dataset)>
					<cfset listeSurThemes=qGetListeSurThemes(dataset)>
					<cfset listeThemes=qGetListeThemes(dataset)>
					
					<cfset NB_ROWS_TOTAL=detailsTableauStruct.NB_ROWS_TOTAL>
					<cfset NB_COLS_TOTAL=detailsTableauStruct.NB_COLS_TOTAL>
			 		<cfinclude template="./detailfilaire_xls.cfm">
			 	<cfelse>
			 		<cfset NB_DEST_MAX = 8>
			 		<cfset reportQuery =
			 				createOtherReportFormatQuery(detailsTableauQuery,detailsEvoTableauQuery,detailsFtTableauQuery,detailsFtEvoTableauQuery)>
	 				<cfset datasetInterStruct=getInternational(ID_PERIMETRE,RapportParams,NB_DEST_MAX)>
					<cfset totalSurThemeStruct=qGetTotalBySurTheme(reportQuery)>
					<cfinclude template="./detailfilaire_document.cfm">
			 	</cfif>
			</cfif>
		<cfelse>
			<cfoutput><center><strong><h2>Aucune donn&eacute;es r&eacute;pondant &agrave; votre demande.</h2></strong></center></cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
