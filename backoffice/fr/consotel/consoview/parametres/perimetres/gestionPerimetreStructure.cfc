<cfcomponent name="gestionPerimetreStructure">

	<cffunction name="getOrganisation" returntype="query" access="remote">
		<cfreturn createObject("component","fr.consotel.consoview.parametres.perimetres.gestionorganisation.GestionOrganisationGateWay").getOrganisationByAcces()>
	</cffunction>
		
	<cffunction name="duplicateNodes" returntype="string" access="remote">
	 	<cfargument name="idsource" required="true" type="string" />
	 	<cfargument name="idDestination" required="true" type="string" />
	 	<cfargument name="withLines" required="true" type="boolean" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Duplicate_nodes">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsource#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDestination#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#withLines#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
		
	<cffunction name="moveNodes" returntype="numeric" access="remote">
		 	<cfargument name="idsource" required="true" type="numeric" />
		 	<cfargument name="idDestination" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.Move_node">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsource#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idDestination#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
		
	<cffunction name="addOrganisation" returntype="numeric" access="remote">
		 	<cfargument name="id_racine" required="true" type="numeric" />
		 	<cfargument name="idOrgaNiveau" required="true" type="numeric" />
		 	<cfargument name="libelle_orga" required="true" type="string" />
		 	<cfargument name="comment_orga" required="true" type="string" />
		 	<cfargument name="type_decoupage" required="true" type="numeric" />
		 	<cfargument name="type_orga" required="true" type="string" />
		 	<!---- A voir ---->
		 	<cfargument name="operateurid" required="true" type="numeric" />
		 	<cfargument name="copylines" required="true" type="numeric" />
		 	<cfargument name="idsouce_organisation" required="true" type="numeric" />
		 	<cfargument name="type_affect" required="true" type="string" />
		 	
		 
		 	
		 	
	 		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.add_organisation">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_racine#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrgaNiveau#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#comment_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#type_decoupage#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_CHAR" value="#type_orga#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#operateurid#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#copylines#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsouce_organisation#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#type_affect#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
		</cffunction>
	
</cfcomponent> 