package gestionparc.ihm.action.dissocier.actionDissocierLigneExtFromEqp
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionDissocierLigneExtFromEqpCommand implements ICommand
	{
		private var receiver : ActionDissocierLigneExtFromEqpReceiver = null;
		
		public function ActionDissocierLigneExtFromEqpCommand(rec:ActionDissocierLigneExtFromEqpReceiver)
		{
			this.receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action();
		}
	}
}