<cfcomponent name="Test" alias="fr.consotel.consoview.bi.Test">
	<cffunction name="getData" access="remote" returntype="any">
		<cfargument name="param" required="true" type="numeric">
		<cfset javaThread=createObject("java","java.lang.Thread")>
		<cfset javaThread.sleep(arguments.param)>
		<cfxml variable="testData">
			<ROWSET>
				<ROW>
					<CHAMPS1>Toto</CHAMPS1>
					<CHAMPS2>15</CHAMPS2>
					<CHAMPS3>IT</CHAMPS3>
				</ROW>
				<ROW>
					<CHAMPS1>Tata</CHAMPS1>
					<CHAMPS2>11</CHAMPS2>
					<CHAMPS3>IT</CHAMPS3>
				</ROW>
				<ROW>
					<CHAMPS1>Titi</CHAMPS1>
					<CHAMPS2>18</CHAMPS2>
					<CHAMPS3>IT</CHAMPS3>
				</ROW>
				<ROW>
					<CHAMPS1>Tutu</CHAMPS1>
					<CHAMPS2>20</CHAMPS2>
					<CHAMPS3>PROD</CHAMPS3>
				</ROW>
			</ROWSET>
		</cfxml>
		<cfreturn toString(testData)>
	</cffunction>
</cfcomponent>
