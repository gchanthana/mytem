<cfcomponent displayname="fr.consotel.api.Application" extends="fr.consotel.api.AbstractApplication" hint="API fr.consotel.api Application">
	<!--- APPLICATION REFERENCE (Auteur : Cedric) :
		NAME PARAMETER VALUE (onApplicationInit) : APPLICATION NAME.VERSION.SUBVERSION.MONTH-YEAR
	--->

	<!--- EXPLICIT INITIALIZATION --->
	<cfset onApplicationInit("fr_consotel_api_v1.0G5.12-2010",createtimespan(0,0,0,5))>
	
	<cffunction access="private" name="setApplicationLogger" returntype="void" hint="See AbstractApplication">
		<cfargument name="loggerClass" type="string" required="false" displayname="LOGGER_CLASS" hint="See AbstractApplication">
		<cfset SUPER.setApplicationLogger("fr.consotel.api.utils.logging.Logger")>
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="ON_ERROR handler" output="true">
		<cfargument name="exceptionObject" type="any" required="true"/>
		<cfargument name="eventName" type="string" required="false"/>
		<cfdump var="#arguments.exceptionObject#" label="ON_ERROR" expand="false">
	</cffunction>
</cfcomponent>