package commandemobile.itemRenderer.detailscommande
{	
	import flash.events.Event;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.WorkflowService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.events.FlexEvent;
	
	[Bindable]
	public class HistoriqueItemRendererImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd				:Commande;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _wflwSrv			:WorkflowService = new WorkflowService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listeHistorique		:ArrayCollection = new ArrayCollection();
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function HistoriqueItemRendererImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function setProcedure(selectedCommande:Commande):void
		{
			_cmd = selectedCommande;
			
			setHistorique(listeHistorique);
		}
		
		/*
		 * récuperer historique d'une commande
		*/
		public function getHistorique():void
		{
			listeHistorique.removeAll();
			
			_wflwSrv.fournirHistoriqueEtatsCommandeSilent(_cmd);
			_wflwSrv.addEventListener(CommandeEvent.LISTED_WFLOW_HIST,	historiqueHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
		}
		
		private function showHandler(fe:FlexEvent):void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function historiqueHandler(cmde:CommandeEvent):void
		{
			listeHistorique = _wflwSrv.historiqueWorkFlow;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		/*
		 * 
		*/
		private function setHistorique(values:ArrayCollection):void
		{
			listeHistorique = values;
		} 

	}
}