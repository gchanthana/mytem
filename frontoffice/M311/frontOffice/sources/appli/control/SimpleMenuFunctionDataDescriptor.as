package appli.control {
	import mx.collections.ICollectionView;
	import mx.controls.menuClasses.IMenuDataDescriptor;
	import mx.collections.XMLListCollection;

	public class SimpleMenuFunctionDataDescriptor implements IMenuDataDescriptor {
		public function SimpleMenuFunctionDataDescriptor() {
			
		}

		public function isBranch(node:Object, model:Object=null):Boolean {
			return ((node as XML).@groupName == "menuUniversGroup");
		}

		public function hasChildren(node:Object, model:Object=null):Boolean {
			return ((node as XML).@groupName == "menuUniversGroup");
		}
		
		public function getGroupName(node:Object):String {
			return node.@groupName;
		}
		
		public function getChildren(node:Object, model:Object=null):ICollectionView {
			return new XMLListCollection((node as XML).children());
		}
		
		public function isEnabled(node:Object):Boolean {
			if(node.@enabled == true)
				return true;
			else
				return false;
		}
		
		public function setEnabled(node:Object, value:Boolean):void {
			node.@enabled = value;
		}
		
		public function isToggled(node:Object):Boolean {
			if(node.@toggled == true)
				return true;
			else
				return false;
		}
		
		public function setToggled(node:Object, value:Boolean):void {
			node.@toggled = value;
		}
		
		public function getType(node:Object):String {
			return node.@type;
		}
		
		/*=============== Méthodes inutilisées mais obligatoire car on implémente une interface !!! ===============*/
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean {
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean {
			return false;
		}
		
		public function getData(node:Object, model:Object=null):Object {
			return null;
		}
	}
}