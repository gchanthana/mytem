package univers.parametres.perimetres.rightSide.destinataireComponent
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	 
	
	public class modifyProfil extends createProfilIHM
	{
		public var _id:Number;
		public var _form:Object;
		public function get modeEcriture():Boolean{ return true}		
		public function modifyProfil()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function setData(form:Object):void
		{
				
			_form = form;
		}
		
		protected function initIHM(e:Event):void
		{
			addEventListener(CloseEvent.CLOSE, closeWindow);
			btValid.visible = modeEcriture;
			btValid.addEventListener(MouseEvent.CLICK, btValidClicked);
			btAnnuler.addEventListener(MouseEvent.CLICK, closeWindow);
			initForm();
		}
		
		protected function initForm():void
		{
			//var obj:Object = DataGrid(parentDocument.dgProfils).selectedItem;
			txtLibelle.editable = modeEcriture;
			txtLibelle.text = _form.NOM_PROFIL;
			
			txtCodeInterne.editable = modeEcriture;
			txtCodeInterne.text = _form.CODE_INTERNE;
			
			txtCommentaire.editable = modeEcriture;
			txtCommentaire.text = _form.COMMENTAIRE_PROFIL;
			_id = _form.IDPROFIL_ANNUAIRE;
		}
		
		protected function btValidClicked(e:MouseEvent):void
		{
			var ev:modifyProfilEvent = new modifyProfilEvent("modifyItem");
			ev.id = _id;
			ev.libelle = txtLibelle.text;
			ev.codeInterne = txtCodeInterne.text;
			ev.commentaire = txtCommentaire.text;
			dispatchEvent(ev);
			
			closeWindow(new Event(""));
		}
		
		
		protected function closeWindow(e:Event):void
		{
			PopUpManager.removePopUp(this);	
		}
	}
}