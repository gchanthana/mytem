package composants.mail.gabarits
{
	import composants.util.ConsoviewFormatter;
	
	public class GabaritMouvementDeSousTete477 extends AbstractGabarit
	{
		public function GabaritMouvementDeSousTete477()
		{
			super();
		}
		
		override public function getGabarit():String{
			var listeLignes : String = printListeSousTete(infos.LIGNES);
			return	"<p>Société : <b>"+infos.raison_sociale+"</b></p>"					
					+"<br/>"
					+"<br/>"
					+"<p>Bonjour,</p>"	
					+"<br/>"				
					+"<p>Les lignes ci-dessous font l'objet d'un transfert de compte</p>"
					+"<br/>"
					+"Origine des lignes <br/>"
					+ infos.INFOS_DEST[2][0].TYPE_NIVEAU + " : <b>" +infos.INFOS_DEST[2][0].LIBELLE_GROUPE_CLIENT  + "</b>. Gestionnaire : <b>" + infos.gestionnaire + "</b>. <br/>"
					+"Destination <br/>" 
					+ infos.INFOS_DEST[1][0].TYPE_NIVEAU + " : <b><font color='#FF1200'>Ref BV :230?</font>" +infos.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT  + "</b>. Gestionnaire : <b>" + infos.gestionnaireCible + "</b>. <br/>"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+"<br/><font color='#FF1200'><b>Message à "+ infos.gestionnaireCible +".</b></font>"
					+"<br/>"
					+"<br/>"
					+"<p>Merci de prendre bonne note de ce transfert. Les lignes ont été affectées dans <b><i>CONSOVIEW</i></b> sur : <b> "+ infos.INFOS_DEST[1][0].TYPE_NIVEAU + " " +infos.INFOS_DEST[1][0].LIBELLE_GROUPE_CLIENT + " </b> par <b>" + infos.gestionnaire + "</b></p>"
					+"<p>En cas d’erreur merci de prendre contact avec <b> "+ infos.gestionnaire + "</b> "+ infos.usermail + ".</p>"
					+"<br/>_______________________________________________________________________________________________"
					+"<br/><font color='#FF1200'><b>Message à l'operateur</b></font>"
					+"<br/>"
					+"<br/>"
					+"<p>1.    Merci de procéder à la correction des la Référence <b>"+infos.raison_sociale+"</b> de ces lignes (si renseigné).</p>"	
					+"<p>2.    Merci de procéder au changement de compte (si renseigné).</p>"	
					+"<br/>"
					+"<br/>"
					+"<p>Cordialement,</p>"
					+"<br/>"
					+"<b>"+infos.gestionnaire+ ".</b>"
					+"<br>"
					+"<b>"+infos.raison_sociale+"</b>";
		}
		
		protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var titulaire: String = "";
					if (liste[i].COMMENTAIRES != null && String(liste[i].COMMENTAIRES).length > 0) titulaire = " (" + liste[i].COMMENTAIRES + ")";
					output = output + liste[i].LIBELLE_TYPE_LIGNE + " : <b>"+ ConsoviewFormatter.formatPhoneNumber(liste[i].SOUS_TETE) + titulaire  + "</b>\t\t\t\tcompte actuel : ____________,\t\t" + "compte cible : ____________ <br/>";
				}
			}
			return output;
		}
		
	}
}