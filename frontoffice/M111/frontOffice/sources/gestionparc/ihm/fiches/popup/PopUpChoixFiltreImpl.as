package gestionparc.ihm.fiches.popup
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.ui.TitleWindowBounds;
	
	import gestionparc.event.gestionparcEvent;
	
	public class PopUpChoixFiltreImpl extends TitleWindowBounds
	{
		//--------------- VARIABLES ----------------//
		[Bindable] 
		public var myRbg				:RadioButtonGroup;
		[Bindable] 
		public var itemCurrentLabel		:Label;
		public var dgChooseFiltre		:DataGrid;
		public var tiFiltre				:TextInput;
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		[Bindable]
		public var btnValidEnabled		:Boolean = false;
		
		private var _itemCurrentSelected		:Object = null;
		private var _selectedIndex		:Number;
		private var _listeFiltre		:ArrayCollection;
		private var _itemSelectedFirst 	: Object = null;
		
		public function PopUpChoixFiltreImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		private function initData():void
		{
			callLater(tiFiltre.setFocus);
		}
		
		private function initListeners():void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseRacineHandler);
			addEventListener(KeyboardEvent.KEY_DOWN,validerChooseRacineHandler2);
			btnAnnuler.addEventListener(MouseEvent.CLICK,onCloseHandler);
			dgChooseFiltre.addEventListener(ListEvent.CHANGE,currentItemHandler);
			
		}
		
		/* */
		protected function currentItemHandler(le:ListEvent):void
		{
			itemCurrentSelected = le.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listeFiltre.length)
			{
				if(itemCurrentSelected!=listeFiltre[i])
					listeFiltre[i].SELECTED = false;
				else
				{
					listeFiltre[i].SELECTED = true;
					//					_selectedIndex = i;
				}
				listeFiltre.itemUpdated(listeFiltre[i]);
				i++;
			}
			
			itemCurrentLabel.text = getItemCurrentLabel();
			itemCurrentLabel.toolTip = getItemCurrentLabel();
			btnValidEnabled = true;
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeFiltre.filterFunction = filtrerFunc;
			listeFiltre.refresh();
		}
		
		/* systeme de filtre */
		private function filtrerFunc(item:Object):Boolean
		{
			/*if(dgChooseFiltre.selectedIndex)
				myRbg.selection = myRbg.getRadioButtonAt(dgChooseFiltre.selectedIndex + 1);
			*/
			if (item.LIBELLE != null && (item.LIBELLE as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		protected function onCloseHandler(event:Event):void
		{
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			PopUpManager.removePopUp(this);
		}
		
		/* au click sur valider */
		private function validerChooseRacineHandler(evt:MouseEvent):void
		{
			validerChoix();
		}
		
		private function validerChoix():void
		{
			if(_itemCurrentSelected.LIBELLE && _itemCurrentSelected.LIBELLE != '')
			{
				dispatchEvent(new gestionparcEvent(gestionparcEvent.VALID_CHOIX_FILTRE, _itemCurrentSelected.LIBELLE,true));
				onCloseHandler(null);
			}
			else
			{
				ResourceManager.getInstance().getString('M111','Veuillez_s_lectionner_un__l_ment_dans_la')	
			}
			
		}
		
		private function validerChooseRacineHandler2(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
			{
				validerChoix();
			}
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		public function get itemCurrentSelected():Object
		{
			return _itemCurrentSelected;
		}
		
		public function set itemCurrentSelected(value:Object):void
		{
			_itemCurrentSelected = value;
		}
		
		public function getItemCurrentLabel():String
		{
			if (_itemCurrentSelected != null)
				return _itemCurrentSelected.LIBELLE;
			else
				return '';
		}
		
		[Bindable]
		public function get listeFiltre():ArrayCollection { return _listeFiltre; }
		
		public function set listeFiltre(value:ArrayCollection):void
		{
			if (_listeFiltre == value)
				return;
			_listeFiltre = value;
		}
	}
}