package classe
{
	public class Client
	{
		private var _LIBELLE:String;
		private var _IDRACINE:String;
		
		public function Client()
		{
		}


		public function set LIBELLE(value:String):void
		{
			_LIBELLE = value;
		}

		public function get LIBELLE():String
		{
			return _LIBELLE;
		}

		public function set IDRACINE(value:String):void
		{
			_IDRACINE = value;
		}

		public function get IDRACINE():String
		{
			return _IDRACINE;
		}
	}
}