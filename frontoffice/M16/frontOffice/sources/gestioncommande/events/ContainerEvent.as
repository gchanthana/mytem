package gestioncommande.events
{
	import flash.events.Event;

	public class ContainerEvent extends Event
	{
		static public const CLOSE_EVENT:String = "CLOSE_EVENT";
		static public const VALIDATE_EVENT:String = "VALIDATE_EVENT";
		static public const VALIDATEDEPOT_EVENT:String = "VALIDATEDEPOT_EVENT";
		static public const SUPPRESION_RESULTEVENT:String = "SUPPRESION_RESULTEVENT";
		static public const DOWNLOAD_RESULTEVENT:String = "DOWNLOAD_RESULTEVENT";
		static public const UPLOAD_COMPLETE:String = "UPLOAD_COMPLETE";
		static public const UPLOAD_ERROR:String = "UPLOAD_ERROR";
		static public const ZIP_READY:String = "ZIP_READY";
		static public const ZIP_SELECT:String = "ZIP_SELECT";
		static public const ZIP_ERROR:String = "ZIP_ERROR";
		static public const LISTEDOC_RESULTEVENT:String = "LISTEDOC_RESULTEVENT";
		static public const DOCUMENT_ADDED:String = "DOCUMENT_ADDED";
		static public const DOCUMENT_RENAMED:String = "DOCUMENT_RENAMED";
		static public const LISTEPARAMS_RESULTEVENT:String = "LISTEPARAMS_RESULTEVENT"
		
		private var _obj:Object;
		
		public function ContainerEvent(type:String, obj:Object= null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this._obj = obj
		}
		

		public function set obj(value:Object):void
		{
			_obj = value;
		}

		public function get obj():Object
		{
			return _obj;
		}
	}
}