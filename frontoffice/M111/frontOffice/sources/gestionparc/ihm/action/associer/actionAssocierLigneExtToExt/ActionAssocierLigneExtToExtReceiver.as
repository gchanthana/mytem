package gestionparc.ihm.action.associer.actionAssocierLigneExtToExt
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.extension.FicheAssocierLigneToExt;
	import gestionparc.ihm.fiches.sim.FicheAssocierLigneToSim;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierLigneExtToExtReceiver
	{
		private var fiche:FicheAssocierLigneToExt;

		public function ActionAssocierLigneExtToExtReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierLigneToExt();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
