﻿<cfcomponent displayname="fr.consotel.consoview.M312.HistoriqueService">

	<cffunction name="getEvolutionSurThemeConso" 
				output="true" 
				access="remote" 
				returntype="Query" hint="Evolution des coût de consommation par sur_theme sur les 12 derniers mois">
		<cfargument name="_typeperimetre" required="true" type="string">
		<cfset obj=createobject("component","fr.consotel.consoview.M312.PerimetreE0")>
		<cfset obj.init(_typeperimetre)>
		
			
	
		
		<cfset _segment = 2><!--- 2 pour mobile --->
		<cfset _nb_mois = 6>
		
		<cfset _idperiode_mois = SESSION.DATES.LASTPFGPIDPERIODE>				 
		<cfset _idracineMaster = SESSION.PERIMETRE.IDRACINE_MASTER>
		<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		
		<cfset _idorga = SESSION.PERIMETRE.SELECTED_ID_ORGA>
		<cfset _idPerimetre = SESSION.PERIMETRE.SELECTED_IDPERIMETRE>
		<cfset _niveau = SESSION.PERIMETRE.SELECTED_NIVEAU>
		<cfset _idniveau = SESSION.PERIMETRE.SELECTED_IDNIVEAU>
		

		<cfset dataTable = "E0_AGGREGS_DEFRAG">
		<cfswitch expression="#_niveau#">
			<cfcase value="A,B,C,D,E,F" delimiters=",">
				<cfset dataTable = "E0_AGGREGS_DEFRAG">
			</cfcase>
			<cfdefaultcase>
				<cfset dataTable = "E0_ENPROD_HIST">	
			</cfdefaultcase>
		</cfswitch>
			
		<cfquery name="qListeEvolution" datasource="BI_TEST">		
			SET VARIABLE 	p_idracine_master = #_idracineMaster#, 
							p_idracine = #_idracine#, 
							p_idorga = #_idorga#, 
							p_idperimetre = #_idPerimetre#, 
							p_niveau = '#_niveau#',
							p_idniveau = #_idniveau#, 
							p_langue_pays = 'fr_FR';
			
			SELECT saw_0 saw_0, saw_1 saw_1, saw_2 saw_2, saw_3 saw_3 
			FROM (
			SELECT saw_0 saw_0, saw_1 saw_1, saw_2 saw_2, SUM(saw_3) saw_3 
			FROM (
			      (SELECT 
			       PRODUIT1GROUPE.SUR_THEME saw_0,
			       PERIODE.DESC_MOIS saw_1,   
			       PERIODE.IDPERIODE_MOIS saw_2, 
			       FACTURATION.MONTANT saw_3
			       FROM #dataTable# 
			      WHERE PERIODEXPRODUIT.IDPERIODE_MOIS > (#_idperiode_mois# - #_nb_mois#)
				   AND  PERIODEXPRODUIT.IDPERIODE_MOIS <= #_idperiode_mois#
			       AND PRODUIT1GROUPE.IDSEGMENT_THEME = #_segment#
			       AND PRODUIT1GROUPE.IDTYPE_THEME = #_segment#
			      ) 
			       UNION 
			      (
					SELECT 
			       PERIODEXPRODUIT.SUR_THEME saw_0,
			       PERIODEXPRODUIT.DESC_MOIS saw_1,
			       PERIODEXPRODUIT.IDPERIODE_MOIS saw_2,  
			       0 saw_3
			       FROM #dataTable# 
			       WHERE PERIODEXPRODUIT.IDPERIODE_MOIS > (#_idperiode_mois# - #_nb_mois#)
				   AND  PERIODEXPRODUIT.IDPERIODE_MOIS <= #_idperiode_mois#
			       AND PERIODEXPRODUIT.IDSEGMENT_THEME = #_segment#
			       AND PERIODEXPRODUIT.IDTYPE_THEME = #_segment#
			      )
			           ) t1 GROUP BY saw_0, saw_1, saw_2
			           ) t2 ORDER BY saw_2
		   
				
		</cfquery>	
		<cfreturn qListeEvolution>
	</cffunction>
</cfcomponent>