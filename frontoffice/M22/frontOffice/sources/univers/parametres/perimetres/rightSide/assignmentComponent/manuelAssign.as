package univers.parametres.perimetres.rightSide.assignmentComponent
{
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.core.ClassFactory;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class manuelAssign extends manuelAssignIHM
	{
		private var _data:Array;
		private var _idLine:Number;
		private var _currentDate:Date;
		private var _newNode:Object;
		
		private var _beginField:String;
		private var _totalData:Array;
		
		
		public function manuelAssign()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);	
		}
		
		protected function initIHM(event:Event):void
		{
		//	 fillGridResult(); 
			_totalData = new Array();
			_totalData.push({NODE:"Total", visible: false});
			gridTotal.dataProvider = _totalData;
			fillGrid();
			gridNodes.addEventListener(DataGridEvent.ITEM_EDIT_BEGINNING, disableEditing);
			gridNodes.addEventListener(DataGridEvent.ITEM_EDIT_END, gridEditEnd);
			
		}
		
		public function addNodeAssign(obj:Object):void
		{
			_newNode = obj;
		}
		
		private function fillGrid():void
		{
			var lineID:String = parentDocument._lineID;
			
			
			_currentDate = new Date();
			var date_start:Date = new Date(_currentDate.getFullYear(), _currentDate.getMonth()-12);
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"getCoefByDate",
																				fillGridResult,
																				throwError);				
			_beginField = "col" + dfs.format(date_start);
			RemoteObjectUtil.callService(op, lineID, dfs.format(date_start), dfs.format(_currentDate));	
		}
		
		private function isNodeNotExist(newNode:Object, data:Array):Boolean
		{
			if (newNode == null)
				return false;
			var newNodeID:Number = parseInt(newNode.@NID);
			if (newNodeID < 0)
				return false;
			for (var j:Number = 0; j < _data.length; j++)
			{
				if (_data[j].NODE_ID == newNodeID)
					return false;
			}
			return true;
		}
		
		private function fillGridResult(event:ResultEvent):void
		{
			_data = event.result as Array;
			var tmp:Array = new Array();
			if (isNodeNotExist(parentDocument._newNode, _data))
				tmp.push({NODE:parentDocument._newNode.@LBL, NODE_ID:parentDocument._newNode.@NID});
	
			var nb:Number = tmp.length;
			
			//_data.push({NODE:"Total", visible: false});
			_data = tmp.concat(_data);
			/*if (_data.length >= 2)
				parentDocument.displayAuto();*/

			
			gridNodes.dataProvider = _data; 
			initGrid(_currentDate);
			gridNodes.itemRenderer = new ClassFactory(itemRender);
			gridTotal.itemRenderer = new ClassFactory(itemRender);
			fillEmptyField();
		}
		
		private function initGrid(d:Date):void
		{
			for (var i:Number = 1; i  < gridNodes.columns.length; i++)
			{
				var coef:Number = 12 - i;
				var newDate:Date = new Date(d.getFullYear(), d.getMonth()-coef, d.date);
				gridNodes.columns[i].dataField = "col" + df_fieldName.format(newDate);
				gridTotal.columns[i].dataField = "col" + df_fieldName.format(newDate);
				
				gridNodes.columns[i].headerText = df.format(newDate);
				gridTotal.columns[i].headerText = df.format(newDate);
				
				
			}
		}
		
		private function fillEmptyField():void
		{
			/*** To Do
			 * Si la première case n'a pas de valeur, aller chercher avant 
			 * */
			
			for (var j:Number = 0; j < _data.length; j++)
			{
				var oldValue:Number = 0;
				for (var i:Number = 1; i  < gridNodes.columns.length; i++)
				{
					var field:String = gridNodes.columns[i].dataField;
					if (_data[j][field] == null)
					{
						if (i == 1 && _data[j][_beginField])
						{
							_data[j][field] = _data[j][_beginField];
							_data[j]["flag"] = j;
						}
						else
							_data[j][field] = oldValue;
							
						if (_data[j]["fin" + field.substr(3)] != null)
							oldValue = 0;
					}
					else
						oldValue = _data[j][field];
				}
			}
			calculateTotal();
		}
		
		private function disableEditing(event:DataGridEvent):void
		{	
			if (event.columnIndex == 0)
				event.preventDefault(); 
		}
		
		private function gridEditEnd(event:DataGridEvent):void
		{	
			
			var newCoef:Number = parseInt(event.currentTarget.itemEditorInstance.text);
			if (isNaN(newCoef))
			{
				event.preventDefault();
				return ;
			}
			
			var index:Number = event.columnIndex;
			
			refreshNodeValue(event.rowIndex, index, _data[event.rowIndex][gridNodes.columns[index].dataField], newCoef);
			calculateTotal();

		}
		
		private function refreshNodeValue(row:Number, index:Number, oldValue:Number, newValue:Number):void
		{
			for (var i:Number = index; i  < gridNodes.columns.length; i++)
			{
				var field:String = gridNodes.columns[i].dataField;
				
				if (_data[row][field] == oldValue)
					_data[row][field] = newValue;
			}
		}
		
		private function calculateTotal():void
		{	
			for (var i:Number = 1; i  < gridNodes.columns.length; i++)
			{
				var field:String = gridNodes.columns[i].dataField;
				
				var total:Number = 0;
				for (var j:Number = 0; j < _data.length; j++)
					if (_data[j][gridNodes.columns[i].dataField] != null)
						total += parseInt(_data[j][field]);
						
				//_data[_data.length-1][field] = total;
				_totalData[0][field] = total;
				if (total > 100)
					gridNodes.columns[i].setStyle("backgroundColor", "red");
				else
					gridNodes.columns[i].setStyle("backgroundColor", "none");
			}
			refreshButtons();
			gridTotal.dataProvider = _totalData;
			gridTotal.dataProvider.refresh();
		}
		
		public function setDate(data:Date):void
		{
			initGrid(data);
			calculateTotal();
		}
		
		public function saveData():void
		{
			var nodes_id:String = "";
			var start_date_list:String = "";
			var end_date_list:String = "";
			var coef_list:String = "";
			var startFlag:Boolean = false;
			
			for (var j:Number = 0; j < _data.length; j++)
			{
				var currentValue:Number = -1;
				var lastDate:String = "";
				for (var i:Number = 1; i  < gridNodes.columns.length; i++)
				{
					var field:String = gridNodes.columns[i].dataField;
					if (i == 1)
					{
						currentValue = _data[j][field];
						lastDate = field;
					}
					
					if (currentValue != _data[j][field] || i+1 >= gridNodes.columns.length)
					{
						if (startFlag)
						{
							start_date_list += ",";
							end_date_list += ",";
							nodes_id += ",";
							coef_list += ",";
						}
						start_date_list += lastDate.substr(3);
						end_date_list += field.substr(3);
						nodes_id += _data[j].NODE_ID;
						coef_list += currentValue;
						
						currentValue = _data[j][field];
						lastDate = field;
						startFlag = true;
					}
				}
				if (startFlag)
					{
						start_date_list += ",";
						end_date_list += ",";
						nodes_id += ",";
						coef_list += ",";
					}
				start_date_list += lastDate.substr(3);
				end_date_list += lastDate.substr(3);
				nodes_id += _data[j].NODE_ID;
				coef_list += currentValue;
			}
			
			sendRequest(nodes_id, start_date_list, end_date_list , coef_list);
		}
		
		private function refreshButtons():void
		{
			var activate:Boolean = true;
			for (var i:Number = 1; i  < gridNodes.columns.length; i++)
			{
				if (_totalData[0][gridNodes.columns[i].dataField] > 100)
					activate = false;
			}
			parentDocument.activateBt(activate);
		}
		
		private function sendRequest(nodes_id:String, start_date_list:String, end_date_list:String, coef_list:String):void
		{
//			var nodeID:Number = parentDocument._nodeID;
			var lineID:String = parentDocument._lineID;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"multipleAssign",
																				multipleAssignResult,
																				throwError);				
			RemoteObjectUtil.callService(op, lineID, nodes_id, start_date_list, end_date_list, coef_list);	
		}
		
		private function multipleAssignResult(event:ResultEvent):void
		{
			
		}
		
		private function throwError(result:FaultEvent):void
		{
			
		}	
				
		
	}
}