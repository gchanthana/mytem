<cfcomponent output="true" extends="fr.saaswedo.api.datalert.Core">
	
	<cffunction access="private" name="URl_ROAMING" returntype="String">
		<cfset url_roaming="#url_server#/api/da/rd.cfm">
		<cfreturn  url_roaming>
	</cffunction>
	
	<cffunction name="init" access="private" output="true" returntype="void">
	
		<cfargument name="httpRequestData" type="String" required="true">
	
		<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
		
		<cfset serverName=URl_ROAMING()>
		<cfset jsonRequest=deserializejson(httpRequestData)>
		
		<cfset mccmnc ="">
		<cfset uts = "">
		
		<cfif structKeyExists(#jsonRequest#,"devId") and jsonRequest.devId neq "">
			<cfset  devId = #jsonRequest.devId# >
			<cfset cause =" cause :  devId = #devId# non trouvé dans la base SWD">
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"mccmnc") and jsonRequest.mccmnc neq "">
			<cfset mccmnc = Replace(#jsonRequest.mccmnc#,"-","","ALL")>
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"UTS") and jsonRequest.UTS neq "">
			<cfset uts = jsonRequest.UTS>
		</cfif>
		
		<cfset jsonPayload ='{"CODE":"#mccmnc#","UTS":"#uts#"}'>
		
		<cflog  text="initialisation variables pour roaming_detection = OK " type="information">
		
	</cffunction>
	
	<cffunction name="roaming_detection" access="public" returntype="any" output="true">
			
		<cfargument name="httpRequestData" 	type="String" required="true">
		<cfargument name="user_agent"		type="String" required="true">
		<cfargument name="ip_origin" 		type="String" required="true">

		<cfset uuid =createuuid() >
				
		<cftry>
			<cfset init(httpRequestData)>
			<!--- logger les données reçues --->
			<cfset saveLog(uuid,"#Debut# roaming_detection","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin#",idMntMobileProxy) >
			
			<cfset devInfo=bdd.getStoredInfo(devId)>
			<cfif devInfo.devid neq "">
			
				<cfhttp method="post" url="#serverName#" useragent="#user_agent#">
					<cfhttpparam type="header" name="Authorization" value="#authorization#"><!--- Authentication basique obligatoire sinon erreur  --->
					<cfhttpparam type="header" name="Accept" value="application/json">
					<cfhttpparam type="header" name="UUID" value="#devInfo.phoneNum#"> <!--- obligatoire sinon erreur ({"Message":"UUID not present in the HTTP header."} ou  {"Message":"UUID : bad format"} ) --->
		 			<cfhttpparam type="formfield" name="data" value="#jsonPayload#"> 	
				</cfhttp>
				
				<!---  logger les données envoyées à l'API SWD  --->	
			 	<cfset saveLog(uuid,"#invoke# roaming_detection","#lab_sentData# #jsonPayload# ,CODE= mccmnc #newLine# avec UUID = #devInfo.PhoneNum# comme Header http #newLine# #link# #serverName#",idMntEventProxyAPI) >
			 
				<cfif cfhttp.Responseheader.Status_Code eq "200">
					<cflog type="information" text="reponse http ok - methode : roaming_detection, on renvoie resp:ok ">
					
					<!--- logger la réponse de l'API SWD  --->
					<cfset saveLog(uuid,"#responseSWD# roaming_detection","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName#",idMntEventAPIProxy) >
					
					<cfset responeJson = '{"resp":"OK"}' >
					
					<!---  logger la réponse envoyée au mobile --->
					<cfset saveLog(uuid,"#responseProxy# roaming_detection","#respHttp# #responeJson#",idMntEventAPIProxy) >
				
					<cfreturn responeJson>		
		 		<cfelse> <!--- logger le message de l'API SWD en cas d'erreur, par exemple :  {"Message":"phone_no is not present"}  et le meme message est renvoyé au mobile ---> 
					<cflog type="information" text="reponse http not ok - methode : roaming_detection">
					<cfset saveLog(uuid,"#responseSWD# roaming_detection","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName# #newLine# #info#",idMntEventAPIProxy) >
					<cfset responeJson = cfhttp.Filecontent >
					<cfreturn responeJson>
				</cfif>
			<cfelse>
				<cflog type="information" text="device non trouve dans la base SWD - methode : roaming_detection ">
				<cfheader name="status" value="421">
				<cfset responeJson = '{"resp":"Device is not registered"}' >
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# roaming_detection","#respHttp# #responeJson# #newLine# #cause#",idMntEventProxyMobile) >
					
				<cfreturn responeJson>	
			</cfif>
		
		<cfcatch type="any">
			<cfthread action="sleep" duration="1000"/>
			<cfset saveLog(uuid,"#exce# roaming_detection",bodyException(CFCATCH),idMntEventProxyException) >
			<cfset responeJson = '{"resp":"#cfcatch.Message#" }' >
			<cfreturn responeJson>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>