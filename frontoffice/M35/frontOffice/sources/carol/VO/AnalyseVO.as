package carol.VO
{
	import carol.composants.perimetreAnalyseIHM;
	
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import ilog.charts.PivotAttribute;
	import ilog.charts.PivotChart;
	
	import mx.controls.DateField;
	import mx.olap.OLAPAttribute;
	import mx.olap.OLAPMeasure;
	import mx.olap.OLAPMember;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable] public class AnalyseVO
	{
		
//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - LOCALE GETTER/SETTER
		//--------------------------------------------------------------------------------------------//
		
		private var _arr_Attribute		:Array;
		private var _arr_cat			:Array;
		private var _arr_Series			:Array;
		private var _idanalyse			:int;
		private var _idcliche			:int;
		private var _description		:String;
		private var _libelle			:String;
		private var _nbrecord			:int = 0;
		private var _perimetre			:PerimetreVO;
		private var _isDefaultAnalyse   :Boolean;
		private var dspObj				:DisplayObject
		public var boolChargerDataset	:Boolean = false;
//--------------------------------------------------------------------------------------------//
//					SETTER
//--------------------------------------------------------------------------------------------//
		public function set ARR_ATTRIBUTE(value:Array):void
		{
	    	_arr_Attribute = value;
	    }
	    public function set ARR_CAT(value:Array):void
		{
	    	_arr_cat = value;
	    }
	 	public function set ARR_SERIES(value:Array):void
		{
	    	_arr_Series = value;
	    }
	    public function set DESCRIPTION(value:String):void
		{
	    	_description = value;
	    }
	    public function set LIBELLE(value:String):void
		{
	    	_libelle = value;
	    }
	    public function set IDANALYSE(value:int):void
		{
			_idanalyse = value;
		}
		public function set NBRECORD(value:int):void
		{
			_nbrecord = value;
		}
		public function set PERIMETRE(value:PerimetreVO):void
		{
			_perimetre = value;
		}
		public function set isDefaultAnalyse(value:Boolean):void
		{
			_isDefaultAnalyse = value;
		}
//--------------------------------------------------------------------------------------------//
//					GETTER
//--------------------------------------------------------------------------------------------//
		public function get ARR_ATTRIBUTE():Array
		{
	    	return _arr_Attribute;
	    }
	    public function get ARR_CAT():Array
		{
	    	return _arr_cat;
	    }
	 	public function get ARR_SERIES():Array
		{
	    	return _arr_Series;
	    }
	    public function get DESCRIPTION():String
		{
	    	return _description;
	    }
	    public function get LIBELLE():String
		{
	    	return _libelle;
	    }
		public function get IDANALYSE():int
		{
			return _idanalyse;
		}
		public function get NBRECORD():int
		{
			return _nbrecord;
		}
		public function get PERIMETRE():PerimetreVO
		{
			return _perimetre;
		}
		public function get isDefaultAnalyse():Boolean
		{
			return _isDefaultAnalyse;
		}
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//	
		
		public function AnalyseVO(dpsobj:DisplayObject=null)
		{
			ARR_ATTRIBUTE 	= new Array()
			ARR_CAT			= new Array()
			ARR_SERIES		= new Array()
			this.dspObj		= dpsobj 
		}
		
//--------------------------------------------------------------------------------------------//
//					METHODES PUBLIC
//--------------------------------------------------------------------------------------------//	
	/* ----------------------------------------------------------------------------	*/
	/* 	AFFICHE L'ANALYSE DANS UN OBJET DE TYPE PIVOTCHART D'ELIXIR					*/
	/* ----------------------------------------------------------------------------	*/	
		public function afficheAnalyse(myPivotChart:PivotChart, peri:perimetreAnalyseIHM):void
		{
			var arrSeriesFinal	:Array = [];
			var arrCatFinal		:Array = [];
			var arrAttribFinal	:Array = [];

		// reset du pivotChart			
			myPivotChart.chartsAttributes = [];
	        myPivotChart.seriesAttributes = [];
	        myPivotChart.categoryAttributes = [];
			
			for each(var ele:ElementVO in ARR_SERIES)
			{
				for each(var pivot:PivotAttribute in myPivotChart.attributes)
				{
					if(pivot.displayName == ele.LIBELLE)	
					{
						if(ele.SELECTEDITEMS != null && ele.SELECTEDITEMS[0] != null && ele.SELECTEDITEMS[0].toString().toUpperCase() != ele.LIBELLE.toUpperCase())
						{
							pivot.selectedItems = []
							for(var i:int;i<pivot.items.length;i++)
							{
								if(pivot.items.getItemAt(i) is OLAPMeasure || pivot.items.getItemAt(i) is OLAPAttribute)
								{
									
									for each(var str:String in ele.SELECTEDITEMS)
									{
									// penser pour plus tard : checker si l'élément est dispo dans le dataset. sinon l'analyse n'affiche rien.	
										var strtmp:String = pivot.items.getItemAt(i).name 
										if(strtmp.toUpperCase() == str.toUpperCase())
											pivot.selectedItems.push(pivot.items.getItemAt(i));
									}
								}
							}
						} 
						arrSeriesFinal.push(pivot);
					}
				}
			}
			
			for each(var ele1:ElementVO in ARR_ATTRIBUTE)
			{
				for each(var pivot1:PivotAttribute in myPivotChart.attributes)
				{
					if(pivot1.displayName == ele1.LIBELLE)	
					{
						if(ele1.SELECTEDITEMS != null && ele1.SELECTEDITEMS[0] != null && ele1.SELECTEDITEMS[0].toString().toUpperCase() != ele1.LIBELLE.toUpperCase())
						{
							pivot1.selectedItems = []
							//pivot1.selectedItems = []
							for(var k:int;k<pivot1.items.length;k++)
							{
								if(k > 0)
								{
									for each(var str1:String in ele1.SELECTEDITEMS)
									{
										var strtmp1:String = pivot1.items.getItemAt(k).name 
										if(strtmp1.toUpperCase() == str1.toUpperCase())
											pivot1.selectedItems.push(pivot1.items.getItemAt(k));
									}
								}
							}
						} 
						arrAttribFinal.push(pivot1);
					}
				}
			}
			
			for each(var ele2:ElementVO in ARR_CAT)
			{
				for each(var pivot2:PivotAttribute in myPivotChart.attributes)
				{
					if(pivot2.displayName == ele2.LIBELLE)	
					{
						if(ele2.SELECTEDITEMS != null && ele2.SELECTEDITEMS[0] != null && ele2.SELECTEDITEMS[0].toString().toUpperCase() != ele2.LIBELLE.toUpperCase())
						{
							pivot2.selectedItems = []
							//pivot2.selectedItems = []
							for(var j:int;j<pivot2.items.length;j++)
							{
								if(j > 0)
								{
									for each(var str2:String in ele2.SELECTEDITEMS)
									{
										var strtmp2:String = pivot2.items.getItemAt(j).name 
										if(strtmp2.toUpperCase() == str2.toUpperCase())
											pivot2.selectedItems.push(pivot2.items.getItemAt(j));
									}
								}
							}
						} 
						arrCatFinal.push(pivot2);
					}
				}
			}		
			
			myPivotChart.chartsAttributes 	= arrAttribFinal;
	        myPivotChart.seriesAttributes 	= arrSeriesFinal;
	        myPivotChart.categoryAttributes = arrCatFinal;
	        
	        peri.lblNomAnalyse.text = LIBELLE
	        peri.lblNomAnalyse.toolTip = LIBELLE
		}
	/* ---------------------------------------------------------------------------- */
	/* REMPLIT LES TABLEAUX ARR_SERIES, ARR_CAT ET ARR_ATTRIBUTE A PARTIR D'UN XML	*/
	/* ---------------------------------------------------------------------------- */	
/* 	 	<analyse>
	 		<series>
				<item>
					<nom></nom>
					<selecteditems>
						<selecteditem></selecteditem>
						<selecteditem></selecteditem>
					</selecteditems>
				</item>
				<item>
					<nom></nom>
					<selecteditems>
						<selecteditem></selecteditem>
					</selecteditems>
				</item>
			</series>
			<attribute>
				<item>
					<nom></nom>
					<selecteditems>
						<selecteditem></selecteditem>
						<selecteditem></selecteditem>
						<selecteditem></selecteditem>
					</selecteditems>
				</item>
			</attribute> 
			<categorie>
				<item>
					<nom></nom>
					<selecteditems>
						<selecteditem></selecteditem>
						<selecteditem></selecteditem>
					</selecteditems>
				</item>
			</categorie> 
	 	</analyse>
 */
		public function getArrFromXml(x:XML):void
		{
			if(x!=null)
			{
				ARR_ATTRIBUTE = []
				ARR_CAT = []
				ARR_SERIES = [] 
				for each(var itemXml:XML in x.series.children())
				{
					var ele:ElementVO = new ElementVO()
					ele.LIBELLE = itemXml.nom
					ele.SELECTEDITEMS = new Array()
					var xmlSelectedItems:XMLList = itemXml.selecteditems.selecteditem
					for each(var tmpxml:XML in xmlSelectedItems)
					{
						ele.SELECTEDITEMS.push(tmpxml);
					}
					ARR_SERIES.push(ele);
				}
				for each(var itemXml2:XML in x.attribute.children())
				{
					var ele2:ElementVO = new ElementVO()
					ele2.LIBELLE = itemXml2.nom
					ele2.SELECTEDITEMS = new Array()
					var xmlSelectedItems2:XMLList = itemXml2.selecteditems.selecteditem
					for each(var tmpxml2:XML in xmlSelectedItems2)
					{
						ele2.SELECTEDITEMS.push(tmpxml2);
					}
					ARR_ATTRIBUTE.push(ele2);
				}
				for each(var itemXml3:XML in x.categorie.children())
				{
					var ele3:ElementVO = new ElementVO()
					ele3.LIBELLE = itemXml3.nom
					ele3.SELECTEDITEMS = new Array()
					var xmlSelectedItems3:XMLList = itemXml3.selecteditems.selecteditem
					for each(var tmpxml3:XML in xmlSelectedItems3)
					{
						ele3.SELECTEDITEMS.push(tmpxml3);
					}
					ARR_CAT.push(ele3);
				}
			}
		}
	/* ---------------------------------------------------------------------------- */
	/* 	CREE UN XML A PARTIR DES TABLEAUX ARR_SERIES, ARR_CAT ET ARR_ATTRIBUTE		*/
	/* ---------------------------------------------------------------------------- */	
		public function getXmlFromArr(mypivotchart:PivotChart):String
		{
			var finalXml:XML = new XML("<analyse></analyse>");
			var seriesXml:XML = new XML("<series></series>");
			for each(var pivot:PivotAttribute in mypivotchart.seriesAttributes)
			{
				var ele:ElementVO = new ElementVO()
				ele.LIBELLE = pivot.displayName
				ele.SELECTEDITEMS = []
				if(pivot.selectedItems[0] is OLAPMember)
				{
					for each(var selectedObj:OLAPMember in pivot.selectedItems)
					{
						ele.SELECTEDITEMS.push(selectedObj.displayName)
					}
				}
				else
				{
					ele.SELECTEDITEMS = []
				}
				var x1:XML = 
					<item>
						<nom>{ele.LIBELLE}</nom> 
					</item>;
				var x11:XML = 	<selecteditems>
								</selecteditems>
				for each(var str1:String in ele.SELECTEDITEMS)
				{
					x11.appendChild(<selecteditem>{str1}</selecteditem>);
				}
				x1.appendChild(x11);
				seriesXml.appendChild(x1)
			}
			finalXml.appendChild(seriesXml);
			var attributeXml:XML = new XML("<attribute></attribute>");
			for each(var pivot2:PivotAttribute in mypivotchart.chartsAttributes)
			{
				var ele2:ElementVO = new ElementVO()
				ele2.LIBELLE = pivot2.displayName
				ele2.SELECTEDITEMS = []
				if(pivot2.selectedItems[0] is OLAPMember)
				{
					for each(var selectedObj2:OLAPMember in pivot2.selectedItems)
					{
						ele2.SELECTEDITEMS.push(selectedObj2.displayName)
					}
				}
				else
				{
					ele2.SELECTEDITEMS = []
				}
				var x2:XML = 
					<item>
						<nom>{ele2.LIBELLE}</nom>  
					</item>;
				var x21:XML = 	<selecteditems>
								</selecteditems>
				for each(var str2:String in ele2.SELECTEDITEMS)
				{
					x21.appendChild(<selecteditem>{str2}</selecteditem>);
				}
				x2.appendChild(x21);
				attributeXml.appendChild(x2)
			}
			finalXml.appendChild(attributeXml);
			var categorieXml:XML = new XML("<categorie></categorie>");
			for each(var pivot3:PivotAttribute in mypivotchart.categoryAttributes)
			{
				var ele3:ElementVO = new ElementVO()
				ele3.LIBELLE = pivot3.displayName
				ele3.SELECTEDITEMS = []
				if(pivot3.selectedItems[0] is OLAPMember)
				{
					for each(var selectedObj3:OLAPMember in pivot3.selectedItems)
					{
						ele3.SELECTEDITEMS.push(selectedObj3.displayName)
					}
				}
				else
				{
					ele3.SELECTEDITEMS = []
				}
				var x3:XML = 
					<item>
						<nom>{ele3.LIBELLE}</nom>  
					</item>;
				var x31:XML = 	<selecteditems>
								</selecteditems>
				for each(var str3:String in ele3.SELECTEDITEMS)
				{
					x31.appendChild(<selecteditem>{str3}</selecteditem>);
				}
				x3.appendChild(x31);
				categorieXml.appendChild(x3)
			}
			finalXml.appendChild(categorieXml)
			return finalXml
		}
	/* ----------------------------------------------------------------------------	*/
	/*  				SAUVEGARDER L'ANALYSE						   				*/
	/* ----------------------------------------------------------------------------	*/	
		public function sauvegarderAnalyse(mypivotchart:PivotChart):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"sauvegarderAnalyse", 
																		sauvegarderAnalyse_handler);
			RemoteObjectUtil.callService(op	,PERIMETRE.idcliche
											,LIBELLE
											,DESCRIPTION
											,PERIMETRE.niveau
											,PERIMETRE.niveau_aggreg
											,PERIMETRE.idperimetre
											,getXmlFromArr(mypivotchart)
											,PERIMETRE.idorga
											,PERIMETRE.idperiodedeb
											,PERIMETRE.idperiodefin
											,PERIMETRE.libelle_orga
											,DateField.dateToString(PERIMETRE.libelle_periodedeb ,"YYYY/MM/DD")
											,DateField.dateToString(PERIMETRE.libelle_periodefin ,"YYYY/MM/DD")
											);
		}
		private function sauvegarderAnalyse_handler(e:ResultEvent):void
		{
			ConsoviewAlert.afficherOKImage("Votre analyse "+LIBELLE+" a été sauvegardé avec succés",dspObj);
		}	
	/* ----------------------------------------------------------------------------	*/
	/*  				SUPPRIMER L'ANALYSE							   				*/
	/* ----------------------------------------------------------------------------	*/	
		public function supprimerAnalyse():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
																		"fr.consotel.consoview.M35.CarolService",
																		"SupprimerAnalyse", 
																		supprimerAnalyse_handler);
			RemoteObjectUtil.callService(op,_idanalyse);
		}
		private function supprimerAnalyse_handler(e:ResultEvent):void
		{
			ConsoviewAlert.afficherOKImage("Votre analyse "+LIBELLE+" a été supprimé avec succés",dspObj);
		}	
	/* ----------------------------------------------------------------------------	*/
	/*  			CHARGER LE PERIMETRE DE L'ANALYSE				   				*/
	/* ----------------------------------------------------------------------------	*/	
		public function chargerPerimetreAnalyse():void
		{
			PERIMETRE.getDataset()
		}
	/* ----------------------------------------------------------------------------	*/
	/*  			CHARGER LES NIVEAUX D'ORGA DE L'ANALYSE			   				*/
	/* ----------------------------------------------------------------------------	*/	
		public function chargerNiveauOrga():void
		{
			PERIMETRE.chargerNiveauOrga()
		}
	/* ----------------------------------------------------------------------------	*/
	/*  			COPIER LES VALEURS D'UNE INSTANCE VERS CELLE LA	   				*/
	/* ----------------------------------------------------------------------------	*/
		public function copyAnalyseVo(ana:AnalyseVO):void
		{
			this.LIBELLE 						= ana.LIBELLE 										
			this.DESCRIPTION					= ana.DESCRIPTION 					
			this.NBRECORD						= ana.NBRECORD						
			this.IDANALYSE						= ana.IDANALYSE						
			this.ARR_ATTRIBUTE					= ana.ARR_ATTRIBUTE;
			this.ARR_CAT						= ana.ARR_CAT;
			this.ARR_SERIES						= ana.ARR_SERIES;
			this.isDefaultAnalyse				= ana.isDefaultAnalyse
			if(this.PERIMETRE == null)
				this.PERIMETRE = new PerimetreVO()
			this.PERIMETRE.idorga				= ana.PERIMETRE.idorga			
			this.PERIMETRE.idcliche				= ana.PERIMETRE.idcliche	
			this.PERIMETRE.idperimetre			= ana.PERIMETRE.idperimetre			
			this.PERIMETRE.idperiodedeb			= ana.PERIMETRE.idperiodedeb			
			this.PERIMETRE.idperiodefin			= ana.PERIMETRE.idperiodefin			
			this.PERIMETRE.idracine				= ana.PERIMETRE.idracine				
			this.PERIMETRE.idracinemaster		= ana.PERIMETRE.idracinemaster		
			this.PERIMETRE.iduser				= ana.PERIMETRE.iduser				
			this.PERIMETRE.langue				= ana.PERIMETRE.langue	
			if(ana.PERIMETRE.libelle_racine != null && ana.PERIMETRE.libelle_racine != "")
				this.PERIMETRE.libelle_racine	= ana.PERIMETRE.libelle_racine
			else
				this.PERIMETRE.libelle_racine	= CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE			
			this.PERIMETRE.libelle_orga			= ana.PERIMETRE.libelle_orga			
			this.PERIMETRE.libelle_periodedeb	= ana.PERIMETRE.libelle_periodedeb	
			this.PERIMETRE.libelle_periodefin	= ana.PERIMETRE.libelle_periodefin	
			this.PERIMETRE.niveau				= ana.PERIMETRE.niveau				
			this.PERIMETRE.niveau_aggreg		= ana.PERIMETRE.niveau_aggreg 		
		}
	/* ------------------------------------------------------------------------------------	*/
	/* COPIER LES VALEURS D'UNE INSTANCE VERS CELLE LA	- SAUF CE QUI EST LIE A LA PERIODE 	*/
	/* ------------------------------------------------------------------------------------	*/
		public function copyAnalyseVoExceptDate(ana:AnalyseVO):void
		{
			this.LIBELLE 						= ana.LIBELLE 										
			this.DESCRIPTION					= ana.DESCRIPTION 					
			this.NBRECORD						= ana.NBRECORD						
			this.IDANALYSE						= ana.IDANALYSE						
			this.ARR_ATTRIBUTE					= ana.ARR_ATTRIBUTE;
			this.ARR_CAT						= ana.ARR_CAT;
			this.ARR_SERIES						= ana.ARR_SERIES;
			this.isDefaultAnalyse				= ana.isDefaultAnalyse
			if(this.PERIMETRE == null)
				this.PERIMETRE = new PerimetreVO()
			this.PERIMETRE.idorga				= ana.PERIMETRE.idorga			
			this.PERIMETRE.idcliche				= ana.PERIMETRE.idcliche	
			this.PERIMETRE.idperimetre			= ana.PERIMETRE.idperimetre			
			this.PERIMETRE.idracine				= ana.PERIMETRE.idracine				
			this.PERIMETRE.idracinemaster		= ana.PERIMETRE.idracinemaster		
			this.PERIMETRE.iduser				= ana.PERIMETRE.iduser				
			this.PERIMETRE.langue				= ana.PERIMETRE.langue	
			if(ana.PERIMETRE.libelle_racine != null && ana.PERIMETRE.libelle_racine != "")
				this.PERIMETRE.libelle_racine	= ana.PERIMETRE.libelle_racine
			else
				this.PERIMETRE.libelle_racine	= CvAccessManager.getSession().CURRENT_PERIMETRE.RACINE_LIBELLE			
			this.PERIMETRE.libelle_orga			= ana.PERIMETRE.libelle_orga			
			this.PERIMETRE.niveau				= ana.PERIMETRE.niveau				
			this.PERIMETRE.niveau_aggreg		= ana.PERIMETRE.niveau_aggreg 		
		}
	/* ----------------------------------------------------------------------------	*/
	/*  			COMPARER SI LES PERIMETRES SONT IDENTIQUES		   				*/
	/* ----------------------------------------------------------------------------	*/
		public function comparePerimetreVo(ana:AnalyseVO):Boolean
		{
			if(	this.PERIMETRE.idorga 			== ana.PERIMETRE.idorga &&			
				this.PERIMETRE.idcliche			== ana.PERIMETRE.idcliche &&	
				this.PERIMETRE.idperimetre		== ana.PERIMETRE.idperimetre &&
				this.PERIMETRE.idperiodedeb		== ana.PERIMETRE.idperiodedeb &&			
				this.PERIMETRE.idperiodefin		== ana.PERIMETRE.idperiodefin &&			
				this.PERIMETRE.idracine			== ana.PERIMETRE.idracine &&		 		
				this.PERIMETRE.idracinemaster	== ana.PERIMETRE.idracinemaster &&
				this.PERIMETRE.niveau			== ana.PERIMETRE.niveau &&				
				this.PERIMETRE.niveau_aggreg	== ana.PERIMETRE.niveau_aggreg)
				return true
			return false 		
		}	
	/* ----------------------------------------------------------------------------	*/
	/*  			COMPARER SI LES PERIMETRES (HORS DATE) SONT IDENTIQUES			*/
	/* ----------------------------------------------------------------------------	*/
		public function comparePerimetreVoExceptPeriode(ana:AnalyseVO):Boolean
		{
			if(	this.PERIMETRE.idorga 			== ana.PERIMETRE.idorga &&			
				this.PERIMETRE.idcliche			== ana.PERIMETRE.idcliche &&	
				this.PERIMETRE.idperimetre		== ana.PERIMETRE.idperimetre &&
				this.PERIMETRE.idracine			== ana.PERIMETRE.idracine &&		 		
				this.PERIMETRE.idracinemaster	== ana.PERIMETRE.idracinemaster &&
				this.PERIMETRE.niveau			== ana.PERIMETRE.niveau &&				
				this.PERIMETRE.niveau_aggreg	== ana.PERIMETRE.niveau_aggreg)
				return true
			return false 		
		}		
	/* ----------------------------------------------------------------------------	*/
	/*  				   				*/
	/* ----------------------------------------------------------------------------	*/
		public function setDefautAnalyse():void
		{
			LIBELLE = ResourceManager.getInstance().getString("MainCarol", "Evolution_des_couts_par_segment1234")
			DESCRIPTION = ResourceManager.getInstance().getString("MainCarol", "Ce_rapport_permet_a_partir_de_l_entite_et_la_periode_sélectionnee_de_visualiser_l_evolution_des_couts_d_abonnements_et_consommations_par_segment_telecom1234")
			var x:XML = 
				<analyse>
				  <series>
				    <item>
				      <nom>TypedeTheme</nom>
				      <selecteditems/>
				    </item>
				  </series>
				  <attribute>
				    <item>
				      <nom>Segment</nom>
				      <selecteditems/>
				    </item>
				  </attribute>
				  <categorie>
				    <item>
				      <nom>Mois</nom>
				      <selecteditems/>
				    </item>
				  </categorie>
				</analyse>;
			getArrFromXml(x) 	
		}			
	}
}