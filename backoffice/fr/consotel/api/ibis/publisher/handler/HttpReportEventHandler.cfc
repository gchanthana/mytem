<cfcomponent displayname="fr.consotel.api.ibis.publisher.handler.HttpReportEventHandler" hint="Handler for BIP HTTP NOTIFICATIONS">
	<!--- BIP HTTP NOTIFICATION HANDLER (Auteur : Cedric)
		HTTP NOTIFICATION HANDLER Mapping :
			- back-notif-dev.consotel.fr : bip-dev.consotel.fr
			- back-notif-int.consotel.fr : bip-int.consotel.fr
			- back-notification.consotel.fr : bip-prod.consotel.fr
			
		HTTP EVENT INFOS Keylist : According to fr.consotel.api.ibis.publisher.handler.event.ReportEvent
			- EVENT_DISPATCHER : Value of CGI.HTTP_HOST in the received HTTP REQUEST (POST)
			- EVENT_TARGET : Event Target (STRING). MAX LENGTH : 35
			- EVENT_TYPE : Event Type (STRING). MAX LENGTH : 3
			- BIP_SERVER : BIP Server DNS from DNS_MAPPING
			- JOB_ID : QUARTZ JOB_ID
			- REPORT_STATUS : QUARTZ REPORT_STATUS
			- REPORT_URL : QUARTZ REPORT_URL
		
		Process Handler : Created from fr.consotel.api.ibis.publisher.handler.process.ProcessXXX where XXX = EVENT_HANDLER
			- Case 1 - Unable to create Process : Delegate EVENT HANDLING to generic fr.consotel.api.ibis.publisher.handler.ServiceHandler.onError
			- Case 2 - Exception occurs during run() : Delegate EVENT HANDLING to Process onError() handler
			- Case 3 - Exception occurs during onError() OR to create GENERIC ServiceHandler : RETHROW EXCEPTION
	 --->
	
	<cffunction access="public" name="onHttpNotification" returntype="void" hint="Called when receiving an HTTP NOTIFICATION from BIP">
		<cfargument name="httpParameters" type="struct" required="true" hint="HTTP Parameters received from BIP">
		
		<cflog text="HttpReportEventHandler.onHttpNotification()">
		
		<cfset httpEvent=createHttpEvent()>
		<cfset jobName=httpParameters.JOBNAME>
		<cfset jobNameSeparator="|">
		<!--- JOB_NAME embeded infos (INCLUDING EMPTY VALUES) --->
		<cfset jobNameInfos=listToarray(jobName,"|",TRUE)>
		<cfif arrayLen(jobNameInfos) EQ 3>
			<!--- PARSING JOB_NAME : EVENT_HANDLER (INDEX : 3) --->
			<cfset eventHandler=jobNameInfos[3]>
			<!--- PARSING JOB_NAME : EVENT_HANDLER (INDEX : 1) --->
			<cfset httpEvent.EVENT_TARGET=jobNameInfos[1]>
			<!--- PARSING JOB_NAME : EVENT_HANDLER (INDEX : 2) --->
			<cfset httpEvent.EVENT_TYPE=jobNameInfos[2]>
			<!--- INFOS TO CREATE EVENT --->
			<cfset httpEvent.JOB_ID=httpParameters.JOBID>
			<cfset httpEvent.REPORT_STATUS=httpParameters.STATUS>
			<cfset httpEvent.REPORT_URL=httpParameters.REPORT_URL>
			<!--- DISPATCH DEEP COPY OF HTTP EVENT DATA --->
			<cfset dispatchEvent(DUPLICATE(httpEvent),eventHandler)>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT" message="onHttpNotification() - INVALID REPORT JOB_NAME : #jobName#">
		</cfif>
	</cffunction>

	<cffunction access="private" name="dispatchEvent" returntype="void"
			hint="Dispatch Event and calls listener handler. If exception">
		<cfargument name="eventInfos" type="struct" required="true" hint="Structure containing infos to create event">
		<cfargument name="eventHandler" type="string" required="true" hint="A string value to identify handler to call">
		<cfset PROCESS_CLASS="Process">
		<cfset PROCESS_PACKAGE="fr.consotel.api.ibis.publisher.handler.process.">
		<cfset DEFAULT_HANDLER="fr.consotel.api.ibis.publisher.handler.ServiceHandler">
		<cfset eventObject=createObject("component","fr.consotel.api.ibis.publisher.handler.event.ReportEvent").createInstance(ARGUMENTS.eventInfos)>
		<cftry>
			<cfset processHandler=createObject("component",PROCESS_PACKAGE & PROCESS_CLASS & eventHandler)>
			<cfset processHandler.run(eventObject)>
			<cfcatch type="any">
				<cftry>
					<!--- Case 1 : Unable to create Process --->
					<cfif NOT isDefined("processHandler")>
						<cftrace category="ERROR" type="error"
								text="Unable to create HANDLER : #PROCESS_PACKAGE##PROCESS_CLASS##eventHandler#">
						<cfset processHandler=createObject("component",DEFAULT_HANDLER)>
					</cfif>
					<!--- Case 2 : Exception occurs during run() --->
					<cfset processHandler.onError(eventObject,ARGUMENTS.eventHandler,CFCATCH)>
					<cfcatch type="any">
						<cftrace category="ERROR" type="error"
								text="An exception occurs durint onError. ORIGINAL HANDLER : #PROCESS_PACKAGE##PROCESS_CLASS##eventHandler#">
						<!--- Case 3 : Exception occurs during onError() OR to create GENERIC ServiceHandler --->
						<cfrethrow />
					</cfcatch>
				</cftry>
			</cfcatch>
		</cftry>
		<cftrace category="DEBUG" type="information" text="dispatchEvent() TO EVENT_HANDLER : #ARGUMENTS.eventHandler#">
	</cffunction>
	
	<cffunction access="private" name="createHttpEvent" returntype="struct"
			hint="Returns an initialized structure used to create Event Objects. Set EVENT_DISPATCHER to CGI.HTTP_HOST (HTTP NOTIFICATION TARGET)">
		<cfset httpEvent=structNew()>
		<cfset httpEvent.EVENT_TARGET="">
		<!--- PARSING JOB_NAME : EVENT_HANDLER (INDEX : 2) --->
		<cfset httpEvent.EVENT_TYPE="">
		<!--- INFOS TO CREATE EVENT --->
		<cfset httpEvent.EVENT_DISPATCHER=CGI.HTTP_HOST>
		<cfset httpEvent.BIP_SERVER="BIP">
		<cfset httpEvent.JOB_ID=0>
		<cfset httpEvent.REPORT_STATUS="">
		<cfset httpEvent.REPORT_URL="">
		<cfreturn httpEvent>
	</cffunction>
	
	<!--- 
	<cffunction access="private" name="getBipServerAddress" returntype="string" hint="Return BIP Server Address from CGI.HTTP_HOST using a map structure">
		<cfargument name="httpHost" type="string" required="true" hint="DNS of one BIP HTTP NOTIFICATION HANDLER (e.g back-notif-dev.consotel.fr)">
		<cfset bipServer="BIP">
		<cfset DNS_MAPPING=structNew()>
		<cfset DNS_MAPPING["back-notif-dev.consotel.fr"]="bip-dev.consotel.fr">
		<cfset DNS_MAPPING["back-notif-int.consotel.fr"]="bip-int.consotel.fr">
		<cfset DNS_MAPPING["back-notification.consotel.fr"]="bip-prod.consotel.fr">
		<cfif structKeyExists(DNS_MAPPING,ARGUMENTS.httpHost)>
			<cfset bipServer=DNS_MAPPING[ARGUMENTS.httpHost]>
		</cfif>
		<cfreturn bipServer>
	</cffunction>
	 --->

</cfcomponent>