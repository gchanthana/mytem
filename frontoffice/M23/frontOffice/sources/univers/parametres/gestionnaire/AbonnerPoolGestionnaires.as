package univers.parametres.gestionnaire
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestionnaire.classemetier.PoolGestionnaire;
	
	public class AbonnerPoolGestionnaires extends AbonnerPoolGestionnairesIHM
	{
		private var poolGestionnaire : PoolGestionnaire;
		//private var treeOrga : OrgaSearchTreeIHM;
		private var col_abonnement : ArrayCollection= new ArrayCollection();
		private var col_abonnement_ofPool : ArrayCollection = new ArrayCollection();
		public function AbonnerPoolGestionnaires(poolGestionnaire : PoolGestionnaire)
		{
			super();
			this.poolGestionnaire = poolGestionnaire;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer);
			
		}
		private function initIHM(evt : FlexEvent):void
		{
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			lab_libelle_pool.text = poolGestionnaire.libelle_pool;
			initLigneOfPool();
			bt_affecter.addEventListener(MouseEvent.CLICK,affecter);
			bt_desaffecter.addEventListener(MouseEvent.CLICK,desaffecter);
						
			//treeOrga.selctionnerLeNoeudCible(UniversManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			fillOrganisation();
			treeOrga.treePerimetre.addEventListener(ListEvent.CHANGE,treeOrga_handler); 
			//Filtre 
			text_input_filtre.addEventListener(Event.CHANGE,filtreAbonnement);
			text_input_filtre_pool.addEventListener(Event.CHANGE,filtreAbonnement_pool);
			check_sansPool.addEventListener(MouseEvent.CLICK,check_sansPool_handler);
			
			if(CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			{
				gestion_droit(true);
			}
			else
			{
				gestion_droit();
			}
			mode_lecture.addEventListener(Event.CHANGE,changeMode);
			
		}
		private function changeMode(evt : Event):void
		{
			if(mode_lecture.selected == true)
			{
				gestion_droit();
			}
			else
			{
				gestion_droit(true);
			}
		}
		private function gestion_droit(modeEcriture : Boolean = false):void
		{
			bt_affecter.enabled = modeEcriture;
			bt_desaffecter.enabled = modeEcriture;
		}
		private function check_sansPool_handler(evt :  Event):void{
			if(check_sansPool.selected == true)
			{
				getListeLignessansPool();
			}
			else
			{
				treeOrga_handler(null);
			}
		}
		//Filtre 
		private function filtreAbonnement(e:Event):void{
			col_abonnement.filterFunction=filtreGrid;
			col_abonnement.refresh();
		}
		//Filtre 
		private function filtreAbonnement_pool(e:Event):void{
			col_abonnement_ofPool.filterFunction=filtreGridPool;
			col_abonnement_ofPool.refresh();
		}
		//Filtre : chercher la chaine entrée
		public function filtreGrid(item:Object):Boolean {
	
			if (((item.SOUS_TETE as String).toLowerCase()).search(text_input_filtre.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		public function filtreGridPool(item:Object):Boolean {
	
			if (((item.SOUS_TETE as String).toLowerCase()).search(text_input_filtre_pool.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		private function affecter(evt : MouseEvent):void
		{
			if(dg_listeLigne.selectedIndex != -1){
			//TODO IMPLEMENTE MULTISELECT
			var ligne : String = dg_listeLigne.selectedItem.IDSOUS_TETE;
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"addLignesToPool",
																		process_addLigneToPool);
			RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,ligne);
			}
			else
			{
				Alert.show("Sélectionnez une ligne dans la liste des lignes de l'orga");
			}
		}
		private function process_addLigneToPool(re : ResultEvent):void
		{
			initLigneOfPool();
			treeOrga_handler(null);
			
		}
		private function desaffecter(evt : MouseEvent):void
		{
			if(dg_ligneOfPool.selectedIndex != -1){
			//TODO IMPLEMENTE MULTISELECT
			var ligne : String = dg_ligneOfPool.selectedItem.IDSOUS_TETE;
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"removeListeLignesFromePool",
																		process_addLigneToPool);
			RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,ligne);
			}
			else
			{
				Alert.show("Sélectionnez une ligne dans la liste des lignes du pool de gestionnaire");
			}
		}
		private function initLigneOfPool():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getListeLignePool",
																		process_init_dg_ligneOfPool);
			RemoteObjectUtil.callService(op,poolGestionnaire.IDPool);
		}
		private function process_init_dg_ligneOfPool(re : ResultEvent):void
		{
			col_abonnement_ofPool = re.result as ArrayCollection;
			dg_ligneOfPool.dataProvider = col_abonnement_ofPool;
		}
		private function treeOrga_handler(evt : ListEvent):void
		{
			var idGroupe : int = treeOrga.treePerimetre.selectedItem.@NID;
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                                                     "fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
                                                     "getListeLignesNotInPool",
            	                                      getLinesResult);                       
	       RemoteObjectUtil.callService(op,idGroupe,poolGestionnaire.IDPool,1);
		}
		private function getListeLignessansPool():void
		{
			var idGroupe : int = treeOrga.treePerimetre.selectedItem.@NID;
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                                                     "fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
                                                     "getListeLignessansPool",
            	                                      getLinesResult);                       
	       RemoteObjectUtil.callService(op,idGroupe,poolGestionnaire.IDPool,1);
		}
		
		private function getLinesResult(re : ResultEvent):void
		{
			col_abonnement = re.result as ArrayCollection;
			dg_listeLigne.dataProvider = col_abonnement;
		}
		protected function fillOrganisation():void
        {
             var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
             var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                                                     "fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
                                                     "getOrganisationCUSGEOANU",
                                                     fillOrganisationResult);                       
             RemoteObjectUtil.callService(op, idGroupeMaitre);
        }
            
       private function fillOrganisationResult(re:ResultEvent):void{
	      var listeOrganisations : ArrayCollection = re.result as ArrayCollection;
	      treeOrga.listeOrganisations = listeOrganisations;
	  }
		private function abonner_handler(evt : MouseEvent):void
		{
			Alert.show("abonner_handler");
		}
		private function desabonner_handler(evt : MouseEvent):void
		{
			Alert.show("desabonner_handler");
		}
		private function fermer(evt: Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
	}
}