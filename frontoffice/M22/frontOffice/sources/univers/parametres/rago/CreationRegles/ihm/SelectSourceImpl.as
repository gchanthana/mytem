package  univers.parametres.rago.CreationRegles.ihm
{
	import composants.tree.event.SearchTreeEvent;
	
	import flash.events.Event;
	
	import mx.controls.Button;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	
	[Bindable]
	public class SelectSourceImpl extends MainCreateRegleImpl
	{
		
//VARIABLES GLOBALES----

		public var rbgFindSource:RadioButtonGroup;
		public var btnFindSourceInDatagrid:Button;
		public var btnFindSourceInTree:Button;
		public var findItSource:TextInput;
		
		public var _sourceWizzard:Wizzard;
		
		public var _jumpOrNot:Boolean = false;

		private var _nbrOfStage:Boolean;
		private var _sourceResult:Object;

//FIN VARIABLES GLOBALES----

//PROPRIETEES PUBLICS----
		
		public function set sourceResult(value:Object):void
		{ _sourceResult = value; }

		public function get sourceResult():Object
		{ return _sourceResult; }

		

//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
			
		public function SelectSourceImpl()
		{
			addEventListener(FlexEvent.SHOW, _localeShowHandler);
			addEventListener(SearchTreeEvent.NODE_SELETCED, _localeNodeSeletcedHandler);
		}
		
//FIN METHODES PUBLICS----		

//METHODES PROTECTED----

		protected function initSelectSourceIHM(event:Event):void
		{  }

		//---------------------------- FORMATS DES COLONNES ---------------------------------------------------------		
		
		//Si c'est une feuille qui est sélectionnée en tant que source alors on met '_jumpOrNot'
		//à true sinon false et on affecte le noeuds (ou feuille sélectionné dans le viewstack
		protected function choixSource(event:Event):void
		{
			if(_sourceWizzard._endStageOrNot)
			{ _jumpOrNot = true; }
			else
			{ _jumpOrNot = false; }
		}
		
		//Ne fais rien lorsque l'on a appuyé sur le bouton 'annuler' du popUp grid ou arbre
		protected function pasChoixSource(event:Event):void
		{ }
		
		protected function _localeNodeSeletcedHandler(event:SearchTreeEvent):void
		{	
			_sourceWizzard.noeudSource = event.node;
			
			if(_sourceWizzard.noeudSource == null) return;
			
			if(_sourceWizzard.noeudSource.hasOwnProperty("@label"))
			{ libelleNodeSelected = event.node.@label; }
		}
		
		protected function _localeShowHandler(event:FlexEvent):void
		{ invalidateProperties(); }
		
		//Enabled ou non selon l'état du radiobtn (enabled = false la partie arbre)
		protected function rdgTreeChangeHandler(event:Event):void
		{ zAttributEnabledOrNot(false,false,true); }
		
		//Enabled ou non selon l'état du radiobtn (enabled = false la partie datagrid)
		protected function rdgNoTreeChangeHandler(event:Event):void
		{ zAttributEnabledOrNot(true,true,false); }
		
		//Ouvre la popUp pour la sélection de la source dans un grid	
		protected function findSourceInDatagridClickHandler(event:Event):void
		{
			var p:PopUpSourceDatagridIHM = new PopUpSourceDatagridIHM();
			
			p._wizzard = _sourceWizzard;
			p.resultOpeAnnu = sourceResult;
			p._strgToSearch = findItSource;
			p.addEventListener("getChoiceSource", choixSource);
			p.addEventListener("AnnulChoiceSource", pasChoixSource);
			PopUpManager.addPopUp(p,this,true);
			PopUpManager.centerPopUp(p);
		}
		
		//Ouvre la popUp pour la sélection de la source dans un arbre	
		protected function findSourceInTreeClickHandler(event:Event):void
		{
			var p:PopUpSourceTreeIHM = new PopUpSourceTreeIHM();

			p._wizzard = _sourceWizzard;
			p.resultOpeAnnu = sourceResult;
			p.addEventListener("getChoiceSource", choixSource);
			p.addEventListener("AnnulChoiceSource", pasChoixSource);
			PopUpManager.addPopUp(p,this,true);
			PopUpManager.centerPopUp(p);
		}

//FIN METHODES PROTECTED----

//METHODES RPIVATE----
		
		//Fonction : Enabled ou non selon l'état du radiobtn
		private function zAttributEnabledOrNot(bool1:Boolean,bool2:Boolean,bool3:Boolean):void
		{
			btnFindSourceInDatagrid.enabled = bool1;
			findItSource.enabled = bool2;
			btnFindSourceInTree.enabled = bool3;
		}

//FIN METHODES RPIVATE----		
		
	}
}