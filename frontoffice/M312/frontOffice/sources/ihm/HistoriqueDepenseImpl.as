//***************************************************	
//	author : samuel.divioka								*
//	date   : Apr 1, 2011								*
//	owner  : Consotel								*
//***************************************************
package ihm
{
	import composants.tb.utils.preloader.Spinner;
	import composants.util.ConsoviewFormatter;
	
	import event.HistoriqueServiceEvent;
	
	import mx.charts.AreaChart;
	import mx.charts.HitData;
	import mx.charts.series.AreaSeries;
	import mx.charts.series.items.AreaSeriesItem;
	import mx.collections.ArrayCollection;
	import mx.containers.Panel;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.graphics.SolidColor;
	import mx.graphics.Stroke;
	
	
	
	[Bindable]
	public class HistoriqueDepenseImpl extends Panel
	{
		
		//composant
		public var lineChart:AreaChart;		
		private var _spinner:Spinner;
		private var _spinnerVisible:Boolean = true;
		
		
		private var _surThemeConso:ArrayCollection;
		private var _evolutionSurThemeConso:ArrayCollection;
		private var _evolutionSurThemeConsoBrut:ArrayCollection;
		
		
		
		
		public var colorsArray:Array = new Array
			(
				0xFFF5F0, 
				0xFEE0D2, 
				0xFCBBA1,
				0xFB6A4A,
				0xEF3B2C,
				0xCB181D,
				0x99000D
			);
		
		
		public function HistoriqueDepenseImpl()
		{
			super();
		}
		
		
		
		//// FRAMEWORK
		
		private var _boolSurThemeConsoChanged : Boolean = false;
		private var _boolEvolutionSurThemeConsoChanged : Boolean = false;
		private var _boolEvolutionSurThemeConsoBrutChanged : Boolean = false;
		
		override protected function createChildren():void			
		{
			super.createChildren();
			_spinner = new Spinner();
			
			_spinner.includeInLayout = false;
			_spinner.x = width / 2 - _spinner.width /2;
			_spinner.y = height / 2 - _spinner.height /2;
			_spinner.visible = true;
			addChild(_spinner);
			
		}
		
		override protected function commitProperties():void
		{
			if(_spinner)
			{
				_spinner.visible = spinnerVisible;
				if(spinnerVisible)
				{
					_spinner.play();
				}
				else
				{
					_spinner.stop();
				}
			}
			if(_boolEvolutionSurThemeConsoChanged && _boolSurThemeConsoChanged)
			{
				createGraph();	
			}
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth,unscaledHeight);
			_spinner.x = width / 2 - _spinner.width /2;
			_spinner.y = height / 2 - _spinner.height /2;
		}
		////////////////////////
		
		
		
		private function createGraph():void
		{
			var currentSeries:Array;
			var randomNum:Number = 0;
			var localSeries:AreaSeries;
			lineChart.series = null;
			lineChart.series = new Array();
			
			for (var i:int=0 ; i<surThemeConso.length ; i++)
			{
				// Create the new series and set its properties.
				localSeries = new AreaSeries();
				localSeries.dataProvider = evolutionSurThemeConso;
				localSeries.yField = surThemeConso[i].SUR_THEME;
				localSeries.xField = "saw_1";					
				
				randomNum = i;
				localSeries.setStyle("areaStroke", new Stroke(colorsArray[randomNum],2,1));
				localSeries.setStyle("areaFill", new SolidColor(colorsArray[randomNum],.5));
				
				// Set values that show up in dataTips and Legend.
				localSeries.displayName = surThemeConso[i].SUR_THEME;
				
				
				// Back up the current series on the chart.
				currentSeries = lineChart.series;
				// Add the new series to the current Array of series.
				currentSeries.push(localSeries);
				// Add the new Array of series to the chart.
				lineChart.series = currentSeries;
			}
			
			if(i > 0)
			{
				_boolSurThemeConsoChanged = false;
				_boolEvolutionSurThemeConsoChanged = false;
				
			}
			
		}
		
		
		//Formattage des tips du graph de l'historique des dépenses
		protected function formatDataTip(obj:HitData):String
		{
			if(obj)
			{
				try
				{
					var periode:String = (obj.chartItem as AreaSeriesItem).xValue as String;						
					var montant:Number = (obj.chartItem as AreaSeriesItem).yValue as Number;
					var libelle :String = (obj.element as AreaSeries).yField as String; 
					
					return "<b>"+
						libelle+
						"</b><br>Période :<b><font color='#000000'>"+
						periode+
						"</font></b>"+
						"<br>Montant :<b><font color='#ff0000'>"+
						ConsoviewFormatter.formatEuroCurrency(montant,2)+
						"</font></b>";	
				}
				catch(e:Error)
				{
					trace(e.getStackTrace())
					return "";
				}
			}
			else
			{
				return "";
			}
			return "";
		}	
		
		
		//Formattage de la colonne montant du grid de l'historique des dépenses
		protected function dgMontantlabelFunction(item:Object,column:DataGridColumn):String
		{	
			var montant:String = ConsoviewFormatter.formatEuroCurrency(Number(item[column.dataField]),2);
			return montant;
		}

		public function set surThemeConso(value:ArrayCollection):void
		{
			if(value != _surThemeConso)
			{
				_surThemeConso = value;
				_boolSurThemeConsoChanged = true;
				invalidateProperties();
			}
			
		}

		public function get surThemeConso():ArrayCollection
		{
			return _surThemeConso;
		}
		public function set evolutionSurThemeConso(value:ArrayCollection):void
		{
			if(value != _evolutionSurThemeConso)
			{
				_evolutionSurThemeConso = value;
				_boolEvolutionSurThemeConsoChanged = true;		
				invalidateProperties();
				invalidateDisplayList();
			}
		}

		public function get evolutionSurThemeConso():ArrayCollection
		{
			return _evolutionSurThemeConso;
		}

		public function set evolutionSurThemeConsoBrut(value:ArrayCollection):void
		{
			if(value != _evolutionSurThemeConsoBrut)
			{
				_evolutionSurThemeConsoBrut = value;
				_boolEvolutionSurThemeConsoBrutChanged = true;
				invalidateProperties();
				invalidateDisplayList();
			}
			
		}

		public function get evolutionSurThemeConsoBrut():ArrayCollection
		{
			return _evolutionSurThemeConsoBrut;
		}
		
		public function set spinnerVisible(value:Boolean):void
		{
			if(value != _spinnerVisible)
			{
				_spinnerVisible = value;
				invalidateProperties();
				invalidateDisplayList();
			}
		}
		
		public function get spinnerVisible():Boolean
		{
			return _spinnerVisible;
		}
	}
	
}