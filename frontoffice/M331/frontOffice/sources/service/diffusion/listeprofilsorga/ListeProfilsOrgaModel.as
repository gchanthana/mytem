package service.diffusion.listeprofilsorga
{
	import event.diffusion.DiffusionEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import vo.diffusion.Profil;	internal class ListeProfilsOrgaModel extends EventDispatcher	{		private var _profilsOrga:ArrayCollection;		private var _idProfilOrgaSelected:int;		private var _defautSelectedIndex: int=0;				public function ListeProfilsOrgaModel()		{			_profilsOrga=new ArrayCollection();		}				public function get profilsOrga():ArrayCollection		{			return _profilsOrga;		}		public function get defautSelectedIndex():Number		{			return _defautSelectedIndex;		}		public function set defautSelectedIndex(value:Number):void		{			_defautSelectedIndex = value;		}		public function get idProfilOrgaSelected():int		{			return _idProfilOrgaSelected;		}				public function set idProfilOrgaSelected(value:int):void		{			_idProfilOrgaSelected = value;		}		internal function listeProfilsOrga(value:ArrayCollection):void		{			_profilsOrga.removeAll();			var unProfil:Profil;						/** ajouter un profil pour afficher tous  */			unProfil=new Profil();			unProfil.idProfil=0;			unProfil.nomProfil=ResourceManager.getInstance().getString('M331', 'Tous');						_profilsOrga.addItemAt(unProfil,0);						if (value.length > 0)			{				for (var i:int=0; i < value.length; i++)				{					unProfil=new Profil();					unProfil.fill(value[i]);					_profilsOrga.addItem(unProfil);										if(unProfil.idProfil==this._idProfilOrgaSelected)					{						defautSelectedIndex=i+1;						}				}			}			dispatchEvent(new DiffusionEvent(DiffusionEvent.LISTE_PROFILS_ORGA_EVENT));// capturé par diffusionImpl		}	}}