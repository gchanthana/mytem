package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	
	public class MultiSousCompte
	{
		
		public var SELECTED					:Boolean 			= false;					//sous compte selectionne ou non
		public var NUMBER_OF_LINES			:int 				= 0;						//nombre de ligne comprenant le meme sous compte
			
		public var IDSOUS_COMPTE			:int 				= 0;						//id du sou compte
		public var SOUS_COMPTE				:String 			= "";						//nom du sous compte	

		public var PRICE					:Number 			= 0;						//nom du sous compte	

		public var LIGNES					:ArrayCollection 	= new ArrayCollection();	//Contenant le meme idsouscompte		
		
			
		public function MultiSousCompte()
		{
		}

	}
}