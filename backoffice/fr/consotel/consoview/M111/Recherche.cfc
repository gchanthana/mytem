﻿<cfcomponent>
	
	<cfset init()>
		
	<cffunction name="init" hint="pour les variables de session" access="remote">
			<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
			<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
			<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
			<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
		</cffunction>
	
	<!--- VUE MOBILE --->
	
	<cffunction name="ViewMobileParc" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
			
			<!--- Pour les tests : [FACTICE Désactivé par -1] Liste des vues pour une racine factice ou de test e.g __DEMO-CONSOTEL (2458788) ou FACTICE (-1) --->
			<cfif session.perimetre.ID_GROUPE EQ -1>
				<!---<cfreturn createObject("component","fr.saaswedo.api.tests.MYT58").getMobileParc(ARGUMENTS.idpool)>--->
				<cfthrow type="Custom" message="ID_GROUPE is negative">
			<!--- Pour tous les autres groupes le fonctionnement reste inchangé --->
			<cfelse>
				<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getMobileParcView">
						<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
						<cfif search eq "">
							<cflog text="search = #search#, null = true">
							<cfprocparam  value="" cfsqltype="CF_SQL_VARCHAR" null="true" >
						<cfelse>
							<cflog text="search = #search#, null = true">	
							<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR">
						</cfif>
						<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
						<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
						<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
						<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
						<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
						<cfprocresult name="result"/>
						<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
						<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					</cfstoredproc>
				<cfreturn result /> 
			</cfif>
	</cffunction>
	
	<cffunction name="ViewMobileLigne" hint="retourne une liste d'equipements" access="remote" returntype="query">

			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getMobileLigneView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<cffunction name="ViewMobileEquipement" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getMobileEquipementView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	
	<!--- VUE MIXTE --->
	<cffunction name="ViewMixteContrat" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idorga" required="true" type="Numeric" />	
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getMixteContratView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idorga#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
		<!--- SUSPENDU POUR L'INSTANT : ViewMixteAlerte --->
	<cffunction name="ViewMixteAlerte" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111." blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	
	<cffunction name="ViewMixteConsommation" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idmois" required="true" type="Numeric" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getMixteConsoView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idmois#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<cffunction name="ViewMixteCollaborateur" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idorga" required="true" type="Numeric" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getMixteCollabView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idorga#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<cffunction name="ViewMixteEtat" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
				
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getMixteEtatView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="0" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<!--- VUE FIXE --->
	<cffunction name="ViewFixeReseauParc" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idorga" required="true" type="Numeric" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
			<cfargument name="idmois" required="true" type="Numeric" />		
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getFixeParcView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idorga#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idmois#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<cffunction name="ViewFixeReseauLigne" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getFixeLigneView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<cffunction name="ViewFixeReseauEquipement" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
					
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getFixeEquipementView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<cffunction name="ViewFixeReseauRaccordement" hint="retourne une liste d'equipements" access="remote" returntype="query">
			<cfargument name="search" required="true" type="String" />
			<cfargument name="idpool" required="true" type="Numeric" />
			<cfargument name="columnSearch" required="true" type="string" />
			<cfargument name="indexDebut" required="true" type="Numeric" />
			<cfargument name="nbItem" required="true" type="Numeric" />
			<cfargument name="labelSort" required="true" type="string" />
			<cfargument name="idrestriction" required="true" type="Numeric" />
			<cfargument name="valeurRestriction" required="true" type="string"/>
				
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getFixeRaccoView_v2" blockfactor="100">
					<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
					<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocresult name="result"/>
					<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
					<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
			<cfreturn result /> 
	</cffunction>
	<cffunction name="ViewFixeReseauCablages" hint="retourne une liste d'equipements" access="remote" returntype="query">
		<cfargument name="search" required="true" type="String" />
		<cfargument name="idpool" required="true" type="Numeric" />
		<cfargument name="columnSearch" required="true" type="string" />
		<cfargument name="indexDebut" required="true" type="Numeric" />
		<cfargument name="nbItem" required="true" type="Numeric" />
		<cfargument name="labelSort" required="true" type="string" />
		<cfargument name="idorga" required="true" type="Numeric" />
		<cfargument name="idrestriction" required="true" type="Numeric" />
		<cfargument name="valeurRestriction" required="true" type="string"/>
				
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_m111.getFixeCablageView_v2" blockfactor="100">
				<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#search#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam  value="#idpool#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#columnSearch#" cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam  value="#idorga#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="result"/>
				<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#valeurRestriction#" cfsqltype="CF_SQL_VARCHAR">
				<cfprocparam  value="#idLangue#" cfsqltype="CF_SQL_INTEGER">
			</cfstoredproc>
		<cfreturn result /> 
</cffunction>
</cfcomponent>