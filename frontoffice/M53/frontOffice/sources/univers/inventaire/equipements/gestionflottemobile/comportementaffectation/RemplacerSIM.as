package univers.inventaire.equipements.gestionflottemobile.comportementaffectation
{
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class RemplacerSIM implements IComportementAffectation
	{
		public function RemplacerSIM()
		{
			
		}
		
		public function affecter(idSIM : int, idSIM_New : int, libelle_SIM : String, libelle_New : String, date : String, pret :int):void
		{
			//Alert.show("AffecterSimTerm : "+idSIM+"--"+idTerm);
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"replaceSIM",
																		affecter_handler);
			RemoteObjectUtil.callService(op,PanelGestionFoltteMobile.objLigneSelected.IDEMPLOYE,PanelGestionFoltteMobile.idPoolGestionnaire,idSIM,idSIM_New,libelle_SIM,libelle_New,date);
		}
		private function affecter_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				ConsoviewAlert.afficherOKImage("Echange de la SIM effectué");
			}
			else
			{
				trace("Erreur replace_sim");
			}
		}
		public function annuler():void
		{
			Alert.show("fermer");
		}
		

	}
}