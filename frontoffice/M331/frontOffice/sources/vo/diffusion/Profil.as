package vo.diffusion
{
	public class Profil
	{
		private var _idProfil:int;
		private var _nomProfil:String;
		
		
		public function Profil()
		{
		}
		
		public function fill(unProfil:Object):void
		{
			this._idProfil=unProfil.IDPROFIL ;
			
			if(unProfil.IDPROFIL==null)
			{
				this._idProfil=0 ;
			}
			
			this._nomProfil=unProfil.NOM_PROFIL;
		}
		
		public function get idProfil():int
		{
			return _idProfil;
		}
		
		public function set idProfil(value:int):void
		{
			_idProfil = value;
		}
		
		public function get nomProfil():String
		{
			return _nomProfil;
		}
		
		public function set nomProfil(value:String):void
		{
			_nomProfil = value;
		}
	}
}