package composants.addremovecolumn
{
	import composants.addremovecolumn.listecolonne.ListeColonneIHM;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;

	public class AddRemoveColumnImpl extends VBox
	{
		public var datgridColonne : DataGrid;
		public var customPopup:ListeColonneIHM;
		
		public function AddRemoveColumnImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(e:FlexEvent):void
		{
			addEventListener(MouseEvent.CLICK, clickHandler);
		}

		private function clickHandler(event:MouseEvent):void
		{
			AfficherPopup(true); /** afficher un popup */
		}

		public function AfficherPopup(open:Boolean):void
		{
			if (open) 
			{
				createCustomMenu();
				PopUpManager.addPopUp(customPopup, this);

				/** calculer le positionnement de sous menu*/
				var tmpPoint:Point=this.localToGlobal(new Point(parent.x, parent.y));
				customPopup.x=tmpPoint.x-16;
				customPopup.y=tmpPoint.y+10;
			}
			else
			{
				PopUpManager.removePopUp(customPopup);
			}
		}
		
		private function createCustomMenu():void
		{
			if(customPopup==null) // creation 
			{
				customPopup=new ListeColonneIHM();
				customPopup.addEventListener(MouseEvent.ROLL_OUT, rollOutMenuHandler);
			}
			else
			{
				PopUpManager.addPopUp(customPopup, this);
			}
		}
				
		private function rollOutMenuHandler(evt:MouseEvent):void
		{
			if(customPopup !=null)
			{
				PopUpManager.removePopUp(customPopup);
			}
		}
	}
}
