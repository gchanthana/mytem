package gestioncommande.entity
{
	import mx.controls.RadioButton;
	import mx.utils.StringUtil;

	[Bindable]
	public class Pool
	{
		public var LIBELLE_POOL		:String = "";
		public var LIBELLE_PROFIL	:String = "";
		public var LIBELLE_CONCAT	:String = "";
		public var IDPOOL			:int = 0;
		public var IDPROFIL			:int = 0;
		public var IDREVENDEUR		:int = 0;
		public var SELECTED			:Boolean = false;//sert uniquement pour la sélection dans la popup de recherche des pools existants
		public var RADIOBUTTON		:RadioButton = null;
		public function Pool()
		{
		}
		
		public static function mappPool(valueObject:Object):Pool
		{
			var poolObject:Pool 			= new Pool();
			poolObject.IDPOOL			= valueObject.IDPOOL;
			poolObject.IDPROFIL			= valueObject.IDPROFIL;
			poolObject.IDREVENDEUR		= valueObject.IDREVENDEUR;
			poolObject.LIBELLE_CONCAT	= StringUtil.trim(valueObject.LIBELLE_POOL) + " - " + StringUtil.trim(valueObject.LIBELLE_PROFIL);
			poolObject.LIBELLE_POOL		= valueObject.LIBELLE_POOL;
			poolObject.LIBELLE_PROFIL	= valueObject.LIBELLE_PROFIL; 
			
			return poolObject;
		}
		
	}
}