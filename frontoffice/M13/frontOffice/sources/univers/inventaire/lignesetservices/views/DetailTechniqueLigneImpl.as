package univers.inventaire.lignesetservices.views
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Panel;
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.LinkBar;
	import mx.events.ItemClickEvent;
	
	
	
	
	
	
	
	[Event(name="DeatilRetourClicked")]
	[Event(name="ExportFicheClicked")]
	
	
	
	
	
	
	
	

	[Bindable]
	public class DetailTechniqueLigneImpl extends BaseView
	{
		
		public var lkOnglets : LinkBar;
		public var vsOnglets : ViewStack;
		public var cmpOngletDetail : OngletDetailView;
		public var cmpOngletEtatView : OngletEtatView;
		public var cmpOngletDetailNoGroupement : OngletDetailNoGroupementView		
		public var cmpOngletOperateurView : OngletOperateurView;		
		public var cmpOngletAffectationView : OngletAffectationView;
		public var cmpOngletCablageView : OngletCablageView;
		public var cmpOngletFluxApplicatifsView : OngletFluxApplicatifsView;
		public var cmpOngletChampsPersoView : OngletChampsPersoView;
		public var lblLigne : Label;		
		public var btRetour : Button;	
		public var btExport : Button;
		public var pnlDetail : Panel;	
		
		
		
		public function DetailTechniqueLigneImpl()
		{
			super();			
		}
		
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			
			btRetour.addEventListener(MouseEvent.CLICK,btRetourClickHandler);
			btExport.addEventListener(MouseEvent.CLICK,btExportFicheClickHandler);
		}
		
		
		
		
		
		/////====================== HANDLERS ========================//
		
		protected function btRetourClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event("DeatilRetourClicked"));
		}
		
		protected function btExportFicheClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event("ExportFicheClicked"));
		}
		
		protected function lkOngletsItemClickHandler(ice : ItemClickEvent):void{			
			//BaseView(vsOnglets.selectedChild).executeBindings(true);
			
		}
		////======================= FIN HANDLERS =====================//
		
	}
}