<cfcomponent output="false">
	<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
	<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
	<!--- Le chemin du rapport sous BIP --->
	<cfset VARIABLES["XDO"]="/consoview/M32/CTFG/CTFG.xdo">
	<!--- Le code rapport --->
	<cfset VARIABLES["CODE_RAPPORT"]="ctfg">
	<!--- TEMPLATE BI --->
	<cfset VARIABLES["TEMPLATE"]="Ventilation">
	<!--- OUTPUT --->
	<cfset VARIABLES["OUTPUT"]="Controle des factures - Produits en erreur">
	<!--- Module --->
	<cfset VARIABLES["MODULE"]="M32">
	<!--- Format --->
	<cfset VARIABLES["FORMAT"]="excel">
	<!--- Extension --->
	<cfset VARIABLES["EXT"]="xls">
<!--- ************************************************************ --->
	
	<cfset reportService = createObject("component",VARIABLES["reportServiceClass"])>
	<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
	<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>
	
<cffunction name="getRapportProduitErreur" access="remote" returnType="any">
	
	<cfargument name="PARAM" type="fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche" required="true" >
	
	<!-- Number(cbControlees.selectedItem.value), Number(cbExportees.selectedItem.value), Number(cbVisees.selectedItem.value), Number(cbViseesAna.selectedItem.value), Operateur(cmbOperateur.selectedItem).id, txtChaine.text, dateDebut, dateFin--->
	
	
	<cfset operateurId=PARAM.getoperateurId()>
	<cfset societeId=PARAM.getsocieteId()>
	<cfset mode=PARAM.getmode()>
	<cfset etatExporte=PARAM.getetatExporte()>
	<cfset operateurNom=PARAM.getoperateurNom()>
	<cfset etatVise=PARAM.getetatVise()>
	<cfset etatControle=PARAM.getetatControle()>
	<cfset themeId=PARAM.getthemeId()>
	<cfset chaine=PARAM.getchaine()>
	<cfset catalogueClient=PARAM.getcatalogueClient()>
	<cfset etatViseAna=PARAM.getetatViseAna()>
	<cfset dateDebut=PARAM.getdateDebut()>
	<cfset dateFin=PARAM.getdateFin()>
	
	
	
	<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
	<cfif initStatus EQ TRUE>
		<cfset setStatus=TRUE>
		
	<!--- PARAMETRES RECUPERES EN SESSION --->
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDRACINE_MASTER",tmpParamValues)>
		<!--- IDRACINE_MASTER --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=SESSION.PERIMETRE["ID_GROUPE"]>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDRACINE",tmpParamValues)>
	<!--- PARAMETRES RECUPERES VIA L'IHM--->		
		<!--- IDPERIMETRE --->			 
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDPERIMETRE = SESSION.PERIMETRE["ID_PERIMETRE"]>
			<cfset tmpParamValues[1]= G_IDPERIMETRE>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDPERIMETRE",tmpParamValues)>
		<!--- G_LANGUE_PAYS --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]= session.user.globalization>
			<cfset setStatus=setStatus AND reportService.setParameter("G_LANGUE_PAYS",tmpParamValues)>
	<!--- PARAMETRES RECUPERES VIA L'APIEO A PARTIR DE L'IDPERIMETRE --->
		<!--- ID DE L'ORGA --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDORGA=ApiE0.getOrga(G_IDPERIMETRE)>
			<cfset tmpParamValues[1]=G_IDORGA>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDORGA",tmpParamValues)>
		<!--- IDCLICHE : Celui du dernier mois du client --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset G_IDCLICHE=ApiE0.getCliche(G_IDORGA)>
			<cfset tmpParamValues[1]=G_IDCLICHE>
			<cfset setStatus=setStatus AND reportService.setParameter("G_IDCLICHE",tmpParamValues)>	
		<!--- U_IDPERIOD_DEB --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=lsdateFormat(dateDebut,'yyyy/mm/dd')>
			<cfset setStatus=setStatus AND reportService.setParameter("U_DATEDEB",tmpParamValues)>
		<!--- IDPERIODEMOIS FIN --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=lsdateFormat(dateFin,'yyyy/mm/dd')>
			<cfset setStatus=setStatus AND reportService.setParameter("U_DATEFIN",tmpParamValues)>
		<!--- U_ID_OPERATEUR --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=operateurId>
			<cfset setStatus=setStatus AND reportService.setParameter("U_OPERATEURID",tmpParamValues)>
		<!--- U_NUM_ETATEXPORTE --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=etatExporte>
			<cfset setStatus=setStatus AND reportService.setParameter("U_EXPORTEE",tmpParamValues)>
		<!--- U_CHAR_CHAINE --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=chaine>
			<cfset setStatus=setStatus AND reportService.setParameter("U_NUMERO_FACTURE",tmpParamValues)>
		<!--- U_NUM_ETATVISEANA --->
			<cfset tmpParamValues=arrayNew(1)>
			<cfset tmpParamValues[1]=etatViseAna>
			<cfset setStatus=setStatus AND reportService.setParameter("U_VISAANA",tmpParamValues)>
			
			
		<cfset pretour = -1>
		<cfif setStatus EQ TRUE>
			<cfset FILENAME=VARIABLES["OUTPUT"]>
			<cfset outputStatus = FALSE>
			<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],VARIABLES["FORMAT"],VARIABLES["EXT"],FILENAME,VARIABLES["CODE_RAPPORT"])>
			<cfif outputStatus EQ TRUE>
				<cfset reportStatus = FALSE>
				<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
				<cfset pretour = reportStatus['JOBID']>
				<!--- <cflog text="************ reportStatus.JOBID = #reportStatus['JOBID']#">
				<cflog text="************ reportStatus.UUID  = #reportStatus['UUID']#"> --->
				<cfif reportStatus['JOBID'] neq -1>
					<!---<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périodicité",valueParameter)>	
					<cfset containerService.ADDparamclient(reportStatus['UUID'],"String","Périmètre d'étude",LBLPERIMETRE)>	--->			
					<cfreturn reportStatus["JOBID"]>
				</cfif>
			</cfif>
		</cfif>	
	</cfif>
	<cfreturn pretour >
</cffunction>
	
</cfcomponent>