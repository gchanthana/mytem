package univers.inventaire.inventaire.recherche
{
	/**
	 * Interface permettent au classe qui l'implemante de changer de perimetre à la volée   
	 * */
	public interface IPanelRecherche
	{
		/**
		 * Change le périmetre à la volée
		 * */
		function onPerimetreChange():void;
		
	}
}