package univers.parametres.perimetres.mainComponent
{
	import mx.controls.dataGridClasses.DataGridItemRenderer;
	
	public class griditemRender  extends DataGridItemRenderer
	{
		public function griditemRender()
		{
			setStyle("borderStyle", "none"); 
		}
		
		override public function set data(value:Object):void {
            super.data = value;
         if (value != null && value.dataField == undefined)
         {
         	if (parseInt(value.NBNODE) > 1)
         		setStyle("color", 0xff9900); 
         	else
	         	setStyle("color", 0x000000); 
         }
  		}
  	}
	
}