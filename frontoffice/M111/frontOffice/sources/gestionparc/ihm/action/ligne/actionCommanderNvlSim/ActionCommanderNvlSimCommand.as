package gestionparc.ihm.action.ligne.actionCommanderNvlSim
{
	import gestionparc.ihm.action.ICommand;
	
	public class ActionCommanderNvlSimCommand implements ICommand
	{
		private var receiver:ActionCommanderNvlSimReceiver=null;
		
		public function ActionCommanderNvlSimCommand(rec:ActionCommanderNvlSimReceiver)
		{
			receiver = rec;
		}
		
		public function execute():void
		{
			receiver.action();
		}
	}
}