<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseTunnel" extends="fr.consotel.api.mdm.partner.public.ZenpriseRegistry"
hint="Implémentation Tunnel Service. Les propriétés pour un tunnel sont représentées en tant que structure contenant les clés suivantes :<br>
- deviceHost : Nom de machine de l'équipement (DEVICE)<br>
- devicePort : Numéro de port de l'équipement (DEVICE)<br>
- initiator : Initiateur (e.g : SERVER ou DEVICE)<br>
- maxConnect : Nombre max de connexion<br>
- name : Nom du tunnel<br>
- osFamily : OS (e.g : iOS, WINDOWS, ANDROID)<br>
- protocol : Protocole (e.g : GENERIC TCP ou FTP)<br>
- serverHost : Nom de machine du serveur<br>
- serverPort : Numéro de port du serveur<br>
- timeOut : Timeout en secondes<br>
- noRoaming : TRUE ou FALSE<br>
- SSL : TRUE pour le support SSL et FALSE sinon<br>
<b>La majorité de ces méthodes lèvent une exception provenant du web service lorsque les valeurs des paramètres fournis n'ont pas de correspondance logique dans le référentiel<br>
Exemple : Référencer un tunnel dans un OS qui n'en possède pas</b>">
	<!--- Tunnel Service --->
	<cffunction access="remote" name="removeTunnel" returntype="Void" hint="Supprime un tunnel">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à supprimer">
		<cfset var tunnelService=getManagementService().getTunnelService()>
		<cfset tunnelService.removeTunnel(ARGUMENTS["osFamily"],ARGUMENTS["name"])>
	</cffunction>
	
	<cffunction access="remote" name="getTunnelList" returntype="String" hint="Retourne la liste des noms des tunnels dans un tableau">
		<cfset var tunnelService=getManagementService().getTunnelService()>
		<cfset var resultList=tunnelService.getTunnelList()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="getOsTunnelList" returntype="String" hint="Retourne la liste des noms des tunnels pour un OS dans un tableau">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var tunnelService=getManagementService().getTunnelService()>
		<cfset var resultList=tunnelService.getOsTunnelList(ARGUMENTS["osFamily"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="createTunnel" returntype="String" hint="Crée un tunnel et retourne son nom">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à créer">
		<cfset var tunnelService=getManagementService().getTunnelService()>
		<cfreturn serializeJSON(tunnelService.createTunnel(ARGUMENTS["name"]))>
	</cffunction>
	
	<cffunction access="remote" name="createTunnelWithProperties" returntype="Void" hint="Crée un tunnel avec les propriétés fournies">
		<cfargument name="tunnel" type="Struct" required="true" hint="Propriétés du tunnel à créer (Voir la liste des clés pour représenter la structure correspondante)">
		<cfset var tunnelService=getManagementService().getTunnelService()>
		<cfset tunnelService.createTunnelWithProperties(ARGUMENTS["tunnel"])>
	</cffunction>
	
	<cffunction access="remote" name="getTunnelProperty" returntype="String" hint="Retourne les propriétés d'un tunnel ou une structure vide s'il n'est pas défini">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="name" type="String" required="true" hint="Nom du tunnel à supprimer">
		<cfset var tunnelService=getManagementService().getTunnelService()>
		<cfset var tunnel={}>
		<cfset var wsResult=tunnelService.getTunnelProperty(ARGUMENTS["osFamily"],ARGUMENTS["name"])>
		<cfif isDefined("wsResult")>
			<cfdump var="#wsResult#">
			<cfset tunnel={
				deviceHost=toString(wsResult.getDeviceHost()), devicePort=VAL(toString(wsResult.getDevicePort())), initiator=toString(wsResult.getInitiator()),
				maxConnect=VAL(toString(wsResult.getMaxConnect())), name=toString(wsResult.getName()), osFamily=toString(wsResult.getOsFamily()), protocol=toString(wsResult.getProtocol()),
				serverHost=toString(wsResult.getServerHost()), serverPort=VAL(toString(wsResult.getServerPort())), timeOut=VAL(toString(wsResult.getTimeOut())),
				noRoaming=yesNoFormat(toString(wsResult.isNoRoaming())), SSL=yesNoFormat(toString(wsResult.isSSL()))
			}>
		</cfif>
		<cfreturn serializeJSON(tunnel)>
	</cffunction>
	
	<cffunction access="remote" name="setTunnelProperty" returntype="Void" hint="Définit les propriétés d'un tunnel">
		<cfargument name="tunnel" type="Struct" required="true" hint="Propriétés pour le tunnel">
		<cfset var tunnelService=getManagementService().getTunnelService()>
		<cfset tunnelService.setTunnelProperty(ARGUMENTS["tunnel"])>
	</cffunction>
</cfcomponent>