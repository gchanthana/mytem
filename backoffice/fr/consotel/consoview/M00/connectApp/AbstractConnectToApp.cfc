﻿<cfcomponent displayname="fr.consotel.consoview.M00.connectApp.AbstractConnectToApp">
	<cfset THIS.AUTH_DSN = "ROCOFFRE">
	<cfset THIS.PKG_GLOBAL = "PKG_CV_GLOBAL">
	<cfset THIS.ROOT_TREE ="ROOT TREE">
	<cfset THIS.NODE_TREE ="NODE TREE">
	<cfset THIS.DEFAULT_UNIVERS ="HOME">
	<cfset THIS.CONNECT_CLIENT ="#THIS.PKG_GLOBAL#.connectClient_v2">


	<cffunction name="checkIpAddress" access="private" returntype="numeric" hint="Return valid address count otherwise 0 (no access)">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="ADRESSE_IP" required="true" type="string">
		<cfset checkResult = 0>
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="PKG_INTRANET.GetdecoupageAdresseIP">
			<cfprocparam value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetIpAddress">
		</cfstoredproc>
	   	<cfif qGetIpAddress.recordcount GT 0>
			<cfset adresseIp=ADRESSE_IP>
			<cfset adresseIpArray=arrayNew(1)>
			<cfloop index="i" from="1" to="4">
				<cfset adresseIpArray[i]=getToken(adresseIp,i,".")>
			</cfloop>
			<cfoutput query="qGetIpAddress">
				<cfset authIp=#P1# & "." & #P2# & "." & #P3# & "." & #P4#>
				<cfset C1=bitAnd(VAL(adresseIpArray[1]),VAL(#M1#))>
				<cfset C2=bitAnd(VAL(adresseIpArray[2]),VAL(#M2#))>
				<cfset C3=bitAnd(VAL(adresseIpArray[3]),VAL(#M3#))>
				<cfset C4=bitAnd(VAL(adresseIpArray[4]),VAL(#M4#))>
				<cfset adresseIpANDmasque = C1 & "." & C2 & "." & C3 & "." & C4>
				<cfif adresseIpANDmasque EQ authIp>
					<cfset checkResult = checkResult + 1>
				</cfif>
			</cfoutput>
			<cfreturn checkResult>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	<cffunction name="ThrowCfCatchError" access="private" returntype="Struct" hint="retourne un object contenant l'erreur du cfcatch">
		<cfargument name="idError" 		required="true" 	type="numeric" >
		<cfargument name="type" 		required="false" 	type="string" 	default="">
		<cfargument name="message" 		required="false" 	type="string"  	default="">
		<cfargument name="detail" 		required="false" 	type="string"  	default="">
		<cfargument name="stackTrace"	required="false" 	type="String" 	default=""/>
		
		<cflog text="AbstractConnectToApp.ThrowCfCatchError : #idError# - #type# - #message# - #detail#">
		
		<cfset cfCatchObject = structNew()>
		<cfset cfCatchObject.CODEERREUR = idError>
		<cfset cfCatchObject.CFCATCH = structNew()>
		<cfset cfCatchObject.CFCATCH.TYPE = type>
		<cfset cfCatchObject.CFCATCH.MESSAGE = message>
		<cfset cfCatchObject.CFCATCH.DETAIL = "">
		<cfset cfCatchObject.CFCATCH.STACKTRACE = "">
		
		<cfreturn cfCatchObject>
	</cffunction>

	<cffunction name="connectTo" 
				displayname="ConnectToApp" 
				description="Permet de se logger à une application" 
				access="public" 
				output="false" 
				returntype="struct">
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		<cflog text="AbstractConnectToApp.connectToApp()">
		<cfset clearOk = clearSession()>
		<cfif clearOk eq 1>
		<!--- VALIDATE LOGIN --->
			<cfset result = validateLogin(objConnection)>
			<!--- si validateLogin retourne un object et que celui ci contient un codeerreur
				  alors on retourne cet object pour afficher un message d'erreur
				 --->
			<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>	
			<cfelseif result eq 1>
					<!--- GET GROUPLIST : récupération de la liste des racines du login --->
					<cfset result = getGroupList(SESSION.USER.APP_LOGINID)>
				<cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
					<cfelse>
						<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
						<cfset result = connectOnGroup(SESSION.USER.CLIENTID)>
						 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
							<cfreturn result>
						<cfelse>
							<!--- SETPERIMETRE : définit le périmètre --->
							<cfset result = setPerimetre(SESSION.PERIMETRE.ID_GROUPE)>
							 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
								<cfreturn result>
							<cfelse>
								<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
								<cfset result = setXMLAccess()>
							</cfif>
						</cfif>
					</cfif>	
			<!--- --->
			<!--- MSG D'ERREUR DE VALIDATE LOGIN --->			
			<cfelse>
				<cfreturn ThrowCfCatchError(result)>	
				<cflog text="connectTo failed">
			</cfif>			
		</cfif>
		<cfreturn SESSION>
	</cffunction>	   

	<!--- CREATION DE L'OBJET USER DE LA SESSION --->		
	<cffunction name="validateLogin" access="private" output="false" returntype="Any" >
		<cfargument name="objConnection" required="true" type="struct" >
		<cflog text="AbstractConnectToApp.validateLogin()">
		
		<cftry>
			<cfif StructKeyExists(SESSION, "IMPERSONNIFICATION")>
				<cfif SESSION.IMPERSONNIFICATION eq FALSE>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">
				</cfif>
				<cfelse>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">					
			 </cfif>
			 
			<cfif StructKeyExists(arguments.objConnection,'supportMode') && arguments.objConnection.supportMode eq 1>
				<cfset objConnection.pwd = getUserPassword(objConnection.login).pass>
			</cfif>

			<cfif structkeyExists(arguments.objConnection,"method") and compareNoCase(arguments.objConnection.method,"token") eq 0>
				
					<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="PKG_CV_GLOBAL.connectClient_v6">
						<cfprocparam type="in" value="#objConnection.login#" cfsqltype="CF_SQL_VARCHAR" >
						<cfprocparam type="in" value="#SESSION.CODEAPPLICATION#" 	cfsqltype="CF_SQL_INTEGER" >
						<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
						<cfprocresult name="qValidateLogin">
					</cfstoredproc>

					<cfelse>	
					
					<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.CONNECT_CLIENT#">
						<cfprocparam type="in" value="#objConnection.login#" cfsqltype="CF_SQL_VARCHAR" >
						<cfprocparam type="in" value="#objConnection.pwd#" 	cfsqltype="CF_SQL_VARCHAR" >
						<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
						<cfprocresult name="qValidateLogin">
					</cfstoredproc>

			</cfif>

		
			<cfif p_retour eq 1>
				<cfif qValidateLogin.recordcount EQ 1>
					<cfset boolTokenHasToBeSaved = 0>
					<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
					<cfif boolCheckIP EQ 1>
						<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
						<cfif checkIpResult LT 1>
							<cfreturn ThrowCfCatchError(-2)>
						<cfelse>
							<cfif setupSessionUser(qValidateLogin)>
								<cfif objConnection.seSouvenir eq 1>
									<cfset boolTokenHasToBeSaved = 1>
								</cfif>
							<cfelse>
								<cfreturn ThrowCfCatchError(-5)>
							</cfif>
							<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
						</cfif>
					<cfelse>
						<cfif setupSessionUser(qValidateLogin)>
							<cfif objConnection.seSouvenir eq 1>
								<cfset boolTokenHasToBeSaved = 1>
							</cfif>
						<cfelse>
							<cfreturn ThrowCfCatchError(-5)>
						</cfif>
					</cfif>
			<!--- 11/07/2012 : AJOUT SSO SE SOUVENIR  --->	
					<cfif boolTokenHasToBeSaved eq 1>
						<cfset result = defineToken(objConnection)>
						 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
							<cfreturn result>
						<cfelse>
							<cfif result eq 0>
								<cfreturn ThrowCfCatchError(-27)>
							<cfelse>
								<cfreturn 1>
							</cfif>
						</cfif>
					<cfelse>	
						<cfreturn 1>
					</cfif>	
				<cfelse>
					<cfreturn ThrowCfCatchError(-3)>
				</cfif>
			<cfelse>
				<cfreturn ThrowCfCatchError(p_retour)>
				<cflog text="echec de la validation du login/mdp">
			</cfif>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-4,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>	
	</cffunction>
	<cffunction name="defineToken" returntype="Any" >
		<cfargument name="objConnection" type="struct" required="true" >
		<cftry>
			<cfset token = createUUID()>
			<cfset app_loginid = SESSION.USER.APP_LOGINID>
			<cfset dateAdd = dateAdd("h",1,now())>
			<cfset dateEnd = "#dateFormat(dateAdd,"yyyy/mm/dd")# #timeFormat(dateAdd,"HH:mm:ss")#">
			<cfset codeApp = SESSION.CODEAPPLICATION>
			
			<cflog text="AbstractConnectToApp.defineToken(app_loginid = #app_loginid# - CODEAPP = #codeApp# - token = #token# - dateEnd = #dateEnd#)">
			
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.setLoginToken">
				<cfprocparam type="in" value="#app_loginid#" 				cfsqltype="CF_SQL_INTEGER" >
				<cfprocparam type="in" value="#SESSION.CODEAPPLICATION#" 	cfsqltype="CF_SQL_INTEGER" >
				<cfprocparam type="in" value="#token#" 						cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="in" value="#dateEnd#" 					cfsqltype="CF_SQL_VARCHAR" >
				<cfprocparam type="out"	variable="p_retour" 				cfsqltype="CF_SQL_INTEGER">
			</cfstoredproc>
			<cfif p_retour eq 1>
				<cfset COOKIE["cleSSO#codeApp#"] = token>
			</cfif>
			<cfreturn p_retour>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-27,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="getGroupList" access="remote" returntype="Any">
		<cfargument name="APP_LOGINID" required="true" type="numeric">
		<cflog text="AbstractConnectToApp.getGroupList(APP_LOGINID = #APP_LOGINID#)">
		<cftry>
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.Login_Listeroot_v2">
				<cfprocparam  value="#APP_LOGINID#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#SESSION.CODEAPPLICATION#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qGetGroupList">
			</cfstoredproc>
			
			<cfif qGetGroupList.RecordCount gt 0>
				<cfif setupSessionListeRacine(qGetGroupList)>
					<cfreturn 1>
				<cfelse>
					<cfreturn ThrowCfCatchError(-5)>
				</cfif>	
			<cfelse>
				<cfreturn ThrowCfCatchError(-21)>	
			</cfif>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(-20,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<!--- CONNECTION A LA RACINE --->
	<cffunction name="connectOnGroup" output="false" access="private" returntype="Any" >
		<cfargument name="groupId" type="numeric" >
			<cflog text="AbstractConnectToApp.connecOnGroup (#groupId#))">
			
			<cfset clearPerimetre()>
			<!--- on cherche si le client a accés au groupId --->
			<cfset findGroupId = 0>
			<cftry>
				<cfif structKeyExists(SESSION,"LISTE_RACINE")>
					<cfloop query="SESSION.LISTE_RACINE">
						<cfif IDGROUPE_CLIENT eq groupId>
							<cfset findGroupId = 1>
							<cfbreak>		
						</cfif>
					</cfloop> 
				</cfif>	
			<!--- si il n'a pas accés, on récupère les informations du premier group de la liste : LISTE_RACINE --->	
				<cfif findGroupId eq 0>
					<cfset idGroup = SESSION.LISTE_RACINE["IDGROUPE_CLIENT"][1]>
				<cfelse>
					<cfset idGroup = groupId>
				</cfif>
				
				<cfset groupInfos=getGroupInfos(idGroup)>
				<cfif isStruct(groupInfos) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn groupInfos>
				<cfelse>
					<cfif setupSessionPerimetre(groupInfos)>
						<cfreturn 1>
					<cfelse>
						<cfreturn ThrowCfCatchError(-6)>
					</cfif>
				</cfif>
			<cfcatch type="any">
				<cfreturn ThrowCfCatchError(-22,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
			</cftry>
	</cffunction>
	<cffunction name="getGroupInfos" access="private" returntype="any">
		<cfargument name="groupId" type="numeric" >
		<cflog text="AbstractConnectToApp.getGroupInfos(#groupId#))">
		<cftry>	
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.GET_GROUPE_INFO">
				<cfprocparam  value="#groupId#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qGetGroupInfos">
			</cfstoredproc>
			<cfreturn qGetGroupInfos>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-23,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<!---  CREATION DU XML D'ACCESS DES MODULES DU USER / PERIMETRE--->
	<cffunction name="setXMLAccess"  access="private" output="false" returntype="Any" >
		<cftry>
			<!--- --->
			<!--- DEFINITION DU NIVEAU D'ORGA DONT ON VEUT CONNAITRE LES ACCES --->
			<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_GROUPE>
			<cfif structkeyExists(SESSION.PERIMETRE,"ID_PERIMETRE")>
				<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
			</cfif>
			
			<cflog text="AbstractConnectToApp.setXMLAccess - #SESSION.USER.CLIENTACCESSID# - #SESSION.PERIMETRE.ID_GROUPE# - #SESSION.PERIMETRE.ID_PERIMETRE#">
			
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="PKG_M00.getListeModuleClient_V4">
				<cfprocparam value="#SESSION.CODEAPPLICATION#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.USER.CLIENTACCESSID#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.USER.IDGLOBALIZATION#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#_IDGROUPE_CLIENT#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qResult">
			</cfstoredproc>
			
						 
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M00.getDefaultModule">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
				<cfprocresult name="qDefaultModule">
			</cfstoredproc>
			
			<cflog text="Module par défaut : #qDefaultModule.KEY#">
			
			<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset fileName = currentPath & "/menus/Menu_Univers_" & SESSION.USER.GLOBALIZATION & ".xml">
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset xmlAccess=XmlParse(XMLFileText)>
			
			<cfset idxHOME = 1>
			<cfset idxGEST_CONT = 1>
			<cfset idxFACT_REPORT_E0 = 1>
			<cfset idxFACT_REPORT_360 = 1>
			<cfset idxFACT = 1>
			<cfset idxGEST = 1>
			<cfset idxUSG = 1>
			<cfset idxSTRUCT = 1>
			<cfset loopindex= 1>
			<cfset idxUnivers = 1>
			<cfset idx = 0>
	
			<cfloop query="qResult">
				<cfset idx = Evaluate("idx#qResult.UNIVERS#")>			
			
				<!--- On trouve l'index de l'univers  --->	
				<cfloop from="1" to="#ArrayLen(xmlAccess.MENU.XmlChildren)#" index="i">
					<cfif xmlAccess.MENU.xmlChildren[i].xmlAttributes.KEY eq qResult.UNIVERS>
						<cfset idxUnivers = i>
						<cfbreak>
					</cfif>
				</cfloop> 
				<!--- On précise que cet univers est visible par l'utilisateur --->	
				<cfif qResult.ACCES_USER gt 0>
					<cfset xmlAccess.MENU.xmlChildren[idxUnivers].XmlAttributes.USR= 1>
				</cfif>
				<cfif qResult.KEY neq qResult.UNIVERS>
					<cfif qResult.ACCES_USER gt 0>
						<cfset tmpXml = xmlAccess.MENU.xmlChildren[idxUnivers]>
						<cfset tmpXml.xmlChildren[idx] = XmlElemNew(xmlAccess,"FONCTION")>
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM_MODULE#>
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.ENABLED = "true">
			 			
			 			<!--- Definition du module par défaut --->
			 			<cfif qDefaultModule.KEY eq qResult.KEY>
						 	<cflog text="FONCTION PAR DEFAUT : qResult.KEY = #qResult.KEY#">
							<cfset tmpXml.xmlChildren[idx].XmlAttributes.toggled = "true">	 	
						 </cfif>	
			 
			 			<cfset idx = idx +1>
						<cfset "idx#qResult.UNIVERS#" = idx>
						<cfset loopindex = loopindex +1>
					</cfif>
				</cfif>
			</cfloop>
		
			<cfset xmlArr = xmlSearch(xmlAccess,"/MENU/UNIVERS/FONCTION")>
			<cfset xmlArr2 = xmlSearch(xmlAccess,"/MENU/UNIVERS[@USR > 0]")>
			<cfset plusVar = arraylen(xmlArr) + arraylen(xmlArr2)>
			<cfif plusVar eq 0> 
				<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
				<cfset fileName = currentPath & "menus/Default_Menu_" & SESSION.USER.GLOBALIZATION & ".xml">
				<cffile action="read" file="#fileName#" variable="XMLFileText">
				<cfset xmlAccess=XmlParse(XMLFileText)>
			</cfif>
			
			<cfset SESSION.XML_ACCESS = xmlAccess>
			<cflog text="#xmlAccess#" >
			<cfreturn 1>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-24,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>	
	<!---  CREATION DE L'OBJET PERIMETRE DE LA SESSION  --->
	<cffunction name="setPerimetre" access="private" returntype="any" >
		<cfargument name="idGroupeClient" required="true" type="numeric">
		<cflog text="AbstractConnectToApp.setPerimetre(idGroupeClient = #idGroupeClient#)">
		<cftry>	
			<cfset setPeriod(idGroupeClient)>
			<cfset accessnodeId = buildGroupXmlTree(idGroupeClient)>
			
			<cfif isStruct(accessnodeId) AND structKeyExists(accessnodeId,"CODEERREUR")>
				<cfreturn accessnodeId>
			<cfelse>
				
				<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.detail_noeud_v2">
					<cfprocparam  value="#accessnodeId#" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam  value="#SESSION.USER.APP_LOGINID#" cfsqltype="CF_SQL_INTEGER">
					<cfprocresult name="qGetNodeInfos">
				</cfstoredproc>
				
				<cflog text="qGetNodeInfos.recordcount = #qGetNodeInfos.recordcount#">
				
				<cfif qGetNodeInfos.recordcount EQ 1>
					<cfset SESSION.PERIMETRE.ID_PERIMETRE=qGetNodeInfos['IDGROUPE_CLIENT'][1]>
					<cfset SESSION.PERIMETRE.RAISON_SOCIALE=qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
					<cfset SESSION.PERIMETRE.GROUP_CODE_STYLE=qGetNodeInfos['CODE_STYLE'][1]>
					<cfset SESSION.PERIMETRE.DROIT_GESTION_FOURNIS=qGetNodeInfos['DROIT_GESTION_FOURNIS'][1]>
					
					<cfif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPE">			
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="Groupe">
					<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPELIGNE">			
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="GroupeLigne">
					<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "SOUSTETE">			
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="SousTete">
					<cfelse>
						<cfset SESSION.PERIMETRE.TYPE_PERIMETRE=qGetNodeInfos['TYPE_PERIMETRE'][1]>
					</cfif>
					 <cfif compareNoCase(SESSION.PERIMETRE.TYPE_PERIMETRE,"GROUPE") eq 0>
                        <cfset SESSION.PERIMETRE.TYPE_LOGIQUE="ROOT">
                        <cfelse>
						<cfset SESSION.PERIMETRE.TYPE_LOGIQUE=qGetNodeInfos['TYPE_ORGA'][1]>
                    </cfif>
					<cfcookie name="CODE_STYLE" value="#qGetNodeInfos['CODE_STYLE'][1]#" expires="never">
				</cfif>
				<cfreturn 1>
			</cfif>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-25,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="setChangePerimetre" access="private" returntype="any" >
		<cfargument name="idGroupeClient" required="true" type="numeric">
		
		<cflog text="AbstractConnectToApp.setChangePerimetre(#idGroupeClient#)">
		
		<cftry>	
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.detail_noeud_v2">
				<cfprocparam  value="#idGroupeClient#" cfsqltype="CF_SQL_INTEGER">
				<cfprocparam  value="#SESSION.USER.APP_LOGINID#" cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qGetNodeInfos">
			</cfstoredproc>
			
			<cfif qGetNodeInfos.recordcount EQ 1>
				<cfset SESSION.PERIMETRE.ID_PERIMETRE=qGetNodeInfos['IDGROUPE_CLIENT'][1]>
				<cfset SESSION.PERIMETRE.RAISON_SOCIALE=qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
				<cfset SESSION.PERIMETRE.GROUP_CODE_STYLE=qGetNodeInfos['CODE_STYLE'][1]>
				<cfset SESSION.PERIMETRE.DROIT_GESTION_FOURNIS=qGetNodeInfos['DROIT_GESTION_FOURNIS'][1]>
				
				<cfif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPE">			
					<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="Groupe">
				<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "GROUPELIGNE">			
					<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="GroupeLigne">
				<cfelseif UCASE(qGetNodeInfos['TYPE_PERIMETRE'][1]) eq "SOUSTETE">			
					<cfset SESSION.PERIMETRE.TYPE_PERIMETRE="SousTete">
				<cfelse>
					<cfset SESSION.PERIMETRE.TYPE_PERIMETRE=qGetNodeInfos['TYPE_PERIMETRE'][1]>
				</cfif>
				<cfif compareNoCase(SESSION.PERIMETRE.TYPE_PERIMETRE,"GROUPE") eq 0>
                        <cfset SESSION.PERIMETRE.TYPE_LOGIQUE="ROOT">
                        <cfelse>
						<cfset SESSION.PERIMETRE.TYPE_LOGIQUE=qGetNodeInfos['TYPE_ORGA'][1]>
                </cfif>
				<cfcookie name="CODE_STYLE" value="#qGetNodeInfos['CODE_STYLE'][1]#" expires="never">
			</cfif>
			<cfreturn 1>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(-25,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="buildGroupXmlTree" access="private" output="false" returntype="Any" >
		<cfargument name="idperimetre" required="true" type="numeric">
		
		<cflog text="AbstractConnectToApp.buildGroupXmlTree(#idperimetre#)">
		
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.Login_nodes_acces_V3">
			<cfprocparam value="#idperimetre#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.APP_LOGINID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetPathToAccessNode">
		</cfstoredproc>
		
		<cfif qGetPathToAccessNode.recordcount GT 0>
			<cfset accessNodeId=qGetPathToAccessNode['IDGROUPE_CLIENT'][qGetPathToAccessNode.recordcount]>
			<cfset accessStatus=qGetPathToAccessNode['STC'][qGetPathToAccessNode.recordcount]>
			
			<cflog text="buildGroupXmlTree(#idperimetre#) - #SESSION.PERIMETRE.ID_GROUPE# - #accessNodeId#">
			
			<cfset _lang = left(SESSION.USER.GLOBALIZATION,2)>
			
			<cfswitch expression="#_lang#">
				<cfcase value="fr">
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfcase>
				<cfcase value="en"> 
					<cfset LEVELNo1 = "List of Carrier Organizations">
					<cfset LEVELNo2 = "List of Customer Organizations">
					<cfset LEVELNo3 = "List of Saved Searches">
				</cfcase>
				<cfcase value="es"> 
					<cfset LEVELNo1 = "Lista de las organizaciones de operadores">
					<cfset LEVELNo2 = "Lista de los Organismos de Clientes">
					<cfset LEVELNo3 = "Lista de las búsquedas guardadas">
				</cfcase>
				<cfcase value="nl">
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfcase>
				<cfdefaultcase>
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfdefaultcase>
			</cfswitch>
			
			<cfif accessStatus GT 0>
				<cfxml variable="perimetreXmlDoc">
					<cfoutput>
						<NODE LBL="#qGetPathToAccessNode['LIBELLE_GROUPE_CLIENT'][1]#" NID="#qGetPathToAccessNode['IDGROUPE_CLIENT'][1]#"
														NTY="#qGetPathToAccessNode['TYPE_NOEUD'][1]#" STC="#qGetPathToAccessNode['STC'][1]#">
														
							<NODE LBL="#LEVELNo1#" NID="-2" NTY="0" STC="0"/>
							<NODE LBL="#LEVELNo2#" NID="-3" NTY="0" STC="0"/>
							<NODE LBL="#LEVELNo3#" NID="-4" NTY="0" STC="0"/>
						</NODE>
					</cfoutput>
				</cfxml>
				<cfif accessNodeId EQ idperimetre>
					<cfset result = buildTreeFromGroup(idperimetre,perimetreXmlDoc)>
				<cfelse>
					<cfset result = buildTreeFromNode(idperimetre,perimetreXmlDoc,qGetPathToAccessNode)>
				</cfif>
				<cfset SESSION.XML_PERIMETRE = perimetreXmlDoc>
			</cfif>
			<cfreturn accessNodeId>
		<cfelse>
			<cfreturn ThrowCfCatchError(-26)>
		</cfif>
	</cffunction>
	<cffunction name="buildTreeFromGroup" access="private" returntype="numeric" output="false">
		<cfargument name="groupId" required="true" type="numeric">
		<cfargument name="xmlDocument" required="true" type="xml">
		<cflog text="AbstractConnectToApp.buildTreeFromGroup (groupId = #groupId#)">
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.enfant_noeud">
			<cfprocparam  value="#groupId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeChild">
		</cfstoredproc>
		<cfset NODE_CREATED_COUNT = 0>
		<cfoutput query="qGetNodeChild" group="TYPE_ORGA">
			<cfset parentNodeIndex = -1>
			<cfswitch expression="#TYPE_ORGA#">
				<cfcase value="OPE">
					<cfset parentNodeIndex = 1>
				</cfcase>
				<cfcase value="SAV">
					<cfset parentNodeIndex = 3> 
				</cfcase>
				<cfcase value="CUS">
					<cfset parentNodeIndex = 2> 
				</cfcase>
				<cfcase value="COP">
					<cfset parentNodeIndex = 2>
				</cfcase>
				<cfcase value="GEO">
					<cfset parentNodeIndex = 2> 
				</cfcase>
				<cfcase value="ANU">
					<cfset parentNodeIndex = 2> 
				</cfcase>
				<cfdefaultcase>
					<cfset parentNodeIndex = 0> 
				</cfdefaultcase>
			</cfswitch>
			<cfif parentNodeIndex GT 0>
				<cflog text="NODE_CREATED_COUNT = #NODE_CREATED_COUNT#">
				<cfoutput>
					<cfset rootNode = xmlDocument.xmlRoot>
					<cfset parentNode=rootNode.XmlChildren[parentNodeIndex]> 
					<cfset parentChildCount = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlAttributes.NTY = 1>
					<cfset parentNode.XmlChildren[parentChildCount + 1] = XmlElemNew(xmlDocument,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildCount + 1]>
					<cfset tmpNode.XmlAttributes.LBL = #LIBELLE_GROUPE_CLIENT#>
					<cfset tmpNode.XmlAttributes.NID = #IDGROUPE_CLIENT#>
					<cfset tmpNode.XmlAttributes.NTY = #TYPE_NOEUD#>
					<cfset tmpNode.XmlAttributes.STC = 1>
					<cfset NODE_CREATED_COUNT = NODE_CREATED_COUNT + 1>
				</cfoutput>
			</cfif>
		</cfoutput>
		<cfreturn NODE_CREATED_COUNT>
	</cffunction>
	<cffunction name="buildTreeFromNode" access="private" returntype="numeric" output="false">
		<cfargument name="groupId" required="true" type="numeric">
		<cfargument name="xmlDocument" required="true" type="xml">
		<cfargument name="pathQuery" required="true" type="query">
		<cflog text="AbstractConnectToApp.buildTreeFromNode()">
		<cfset NODE_CREATED_COUNT = 0>
		<cfset processStatus=-1>
		<cfset rootNode = xmlDocument.xmlRoot>
		<cfloop index="i" from="2" to="#pathQuery.recordcount#">
			<cfset currentNodeId = pathQuery['IDGROUPE_CLIENT'][i]>
			<cfset currentNodeType = pathQuery['TYPE_ORGA'][i]>
			<cfset currentParentId = pathQuery['ID_GROUPE_MAITRE'][i]>
			<cfset parentNodeArray = XMLSearch(xmlDocument,"//NODE[@NID=#currentParentId#]")>
			<cfif arrayLen(parentNodeArray) EQ 1>
				<cfset parentNode = parentNodeArray[1]>
				<cfif isXmlRoot(parentNode) EQ TRUE>
					<cfswitch expression="#currentNodeType#">
						<cfcase value="OPE">
							<cfset parentNodeIndex = 1>
						</cfcase>
						<cfcase value="SAV">
							<cfset parentNodeIndex = 3>
						</cfcase>
						<cfcase value="CUS">
							<cfset parentNodeIndex = 2>
						</cfcase>
						<cfcase value="GEO">
							<cfset parentNodeIndex = 2>
						</cfcase>
						<cfcase value="COP">
							<cfset parentNodeIndex = 2>
						</cfcase>
						<cfcase value="ANU">
							<cfset parentNodeIndex = 2>
						</cfcase>
						<cfdefaultcase>
							<cfset parentNodeIndex = 0> 
						</cfdefaultcase>
					</cfswitch>
					<cfif parentNodeIndex GT 0>
						<cfset parentNode = rootNode.XmlChildren[parentNodeIndex]>
						<cfset processStatus=1>
					<cfelse>
						<cfset processStatus=0>
					</cfif>
				<cfelse>
					<cfset processStatus=1>
				</cfif>
				<cfif processStatus EQ 1>
					<cfset parentNode.XmlAttributes.NTY = 1>
					<cfset parentChildCount = arrayLen(parentNode.XmlChildren)>
					<cfset parentNode.XmlChildren[parentChildCount + 1] = XmlElemNew(xmlDocument,"NODE")>
					<cfset tmpNode = parentNode.XmlChildren[parentChildCount + 1]>
					<cfset tmpNode.XmlAttributes.LBL = pathQuery['LIBELLE_GROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NID = pathQuery['IDGROUPE_CLIENT'][i]>
					<cfset tmpNode.XmlAttributes.NTY = pathQuery['TYPE_NOEUD'][i]>
					<cfset tmpNode.XmlAttributes.STC = pathQuery['STC'][i]>
					<cfset NODE_CREATED_COUNT = NODE_CREATED_COUNT + 1>
				</cfif>
			<cfelse>
				<cfset NODE_CREATED_COUNT = -1>
				<cfreturn NODE_CREATED_COUNT>
			</cfif>
		</cfloop>
		<cfreturn NODE_CREATED_COUNT>
	</cffunction>
	<cffunction name="setPeriod" access="private" output="false" returntype="void">
		
		<cfargument name="idGroupeClient" type="numeric" required="true">
		
		<cflog text="AbstractConnectToApp.setPeriod ( #idGroupeClient# - #SESSION.PERIMETRE.IDRACINE_MASTER# )">
		
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.GET_DATEFACTURE_V2">
	        <cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ARGUMENTS.idGroupeClient#" cfsqltype="CF_SQL_INTEGER">
	         <cfprocparam value="2004/01/01" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetPeriod">
        </cfstoredproc>
        
        <cfset SESSION.PERIMETRE.STRUCTDATE = {}>
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS = {}>
		
		<cfset LOCAL = {}>
		<cfset LOCAL.Now = CreateDateTime(	Year( Now() ),
											Month( Now() ),
											1,10,0,0	)
		/>
			
		<cfset LOCAL.dateUtil = createObject("component","fr.consotel.consoview.M00.connectApp.DateUtil")>
		<cfset LOCAL.periodeFactu_object = createObject("component","fr.consotel.consoview.M00.PeriodeFacturationByRacine")>
		
        <cfif LEN(qGetPeriod.datemin) EQ 0>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB = LOCAL.Now>
		<cfelse>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB = lsParseDateTime(qGetPeriod.datemin)>
		</cfif>
		
		<cfif LEN(qGetPeriod.datemax) EQ 0>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN_ORIGINE =  LOCAL.Now>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN = LOCAL.dateUtil.getLastDayOfMonth(LOCAL.Now)>
		<cfelse>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN_ORIGINE = lsParseDateTime(qGetPeriod.datemax)>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN = LOCAL.dateUtil.getLastDayOfMonth(qGetPeriod.datemax)>
		</cfif>
		
		<!--- mois max à afficher par defaut pour les selecteurs de période --->
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.MAX_MONTH_TO_DISPLAY = 24>
		
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_DEB = LOCAL.periodeFactu_object.setDebutPeriodFactuAccordingToRacine(idGroupeClient)>
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN = SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN>
		
		<!--- si la date de debut de factu à afficher est plus grande que la premiere date de debut de factu --->
		<cfif dateCompare(SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_DEB) GT 0>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_DEB = SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB>
		</cfif>
		<!--- si la date de fin de factu à afficher est plus petite que la derniere date de fin de factu --->
		<cfif dateCompare(SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN,SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN) GT 0>
			<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN = SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN>
		</cfif>
		
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_FACT = dateDiff("m",SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB,SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN)>
		<cfset SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY = dateDiff("m",SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_DEB,SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN)>
		
		<cfset SESSION.PERIMETRE.STRUCTDATE.DATEFIRSTFACTURE = SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_DEB>
		<cfset SESSION.PERIMETRE.STRUCTDATE.DATELASTFACTURE = SESSION.PERIMETRE.STRUCTDATE.PERIODS.FACTURE_FIN>
		<cfset SESSION.PERIMETRE.STRUCTDATE.DISPLAY_DATEDEB = SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_DEB>
		<cfset SESSION.PERIMETRE.STRUCTDATE.DISPLAY_DATEFIN = SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN>		
		
	</cffunction>
	
	
	<!--- SESSION : construction, destruction, modification etc... --->
	<cffunction name="clearSession" access="private" returntype="numeric">
		<cflog text="AbstractConnectToApp.clearSession())">
		<cfif structKeyExists(SESSION,"AUTH_STATE")>
			<cfif SESSION.AUTH_STATE EQ 1>
				<cfset structDelete(SESSION,"OFFREDSN")>
				<cfset structDelete(SESSION,"CDRDSN")>
				<cfset structDelete(SESSION,"PARCVIEWDSN")>
				<cfset structDelete(SESSION,"MAILSERVER")>
				<cfset structDelete(SESSION,"XML_ACCESS")>
				<cfset structDelete(SESSION,"XML_ACCESS_STATUS")>
				<cfset structDelete(SESSION,"UNIVERS_KEY")>
				<cfset structDelete(SESSION,"FUNCTION_KEY")>
				<cfif structKeyExists(SESSION,"USER")>
					<cfset structClear(SESSION.USER)>
					<cfset structDelete(SESSION,"USER")>
				</cfif>
				<cfif structKeyExists(SESSION,"DATES")>
					<cfset structClear(SESSION.DATES)>
					<cfset structDelete(SESSION,"DATES")>
				</cfif>
				<cfif structKeyExists(SESSION,"PERIMETRE")>
					<cfif structKeyExists(SESSION.PERIMETRE,"STRUCTDATE")>
						<cfset structClear(SESSION.PERIMETRE.STRUCTDATE)>
					</cfif>
					<cfset structClear(SESSION.PERIMETRE)>
					<cfset structDelete(SESSION,"PERIMETRE")>
				</cfif>
			   	<cflog type="Information" text="SESSION/#SESSION.IP_ADDR#/#CGI.REMOTE_ADDR# #SESSION.SESSIONID# - Cleared on #APPLICATION.ApplicationName#">
				<cfif SESSION.IP_ADDR EQ SESSION.IP_ADDR>
				   	<cfreturn 1>
				<cfelse>
				   	<cfreturn 0>
				</cfif>
			<cfelse>
				<cfreturn 1>
			</cfif>
		<cfelse>
			<cfreturn 1>	
		</cfif>	
	</cffunction>
	<cffunction name="clearPerimetre" access="private" returntype="boolean" >
		<cflog text="AbstractConnectToApp.clearPErimetre()">
		<cfif structKeyExists(SESSION,"PERIMETRE")>
			<cftry>
				<cfset structClear(SESSION.PERIMETRE)>
				<cfset structDelete(SESSION,"PERIMETRE")>
				<cfset structDelete(SESSION,XML_PERIMETRE)>
				<cfreturn TRUE>
			<cfcatch type="any" >
				<cfreturn FALSE>
			</cfcatch>
			</cftry>	
		<cfelse>
			<cfreturn TRUE>	
		</cfif>	
	</cffunction>	
	<cffunction name="setupSessionUser"  access="private" output="false" returntype="boolean" >
		<cfargument name="qValidateLogin" required="true" type="query" >
		
		<cflog text="AbstractConnectToApp.setupSessionUser()">
		
		<cftry>
			<cfset SESSION.AUTH_STATE = 1>
			<cfset SESSION.OFFREDSN = "ROCOFFRE">
			<cfset SESSION.CDRDSN = "ROCCDR">
			<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW">
			<cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
			<cfset SESSION.USER = structNew()>
			<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
			<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
			<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
			<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
			<cfset SESSION.USER.APP_LOGINID = qValidateLogin['CLIENTACCESSID'][1]>
			<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
			<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
			<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
			<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
			<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
			<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>
			<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
			<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>

			<cfset getDeviseInfos()>

			<cfreturn TRUE>
		<cfcatch type="any">
			<cfreturn FALSE>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="setupSessionListeRacine"  access="private" output="false" returntype="boolean" >
		<cfargument name="qGetGroupList" required="true" type="query" >
		<cftry>
			<cflog text="AbstractConnectToApp.setupSessionListeRacine()">
			<cfset SESSION.LISTE_RACINE = qGetGroupList>
			<cfreturn TRUE>
		<cfcatch type="any">
			<cfreturn FALSE>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="setupSessionPerimetre"  access="private" output="false" returntype="boolean" >
		<cfargument name="groupInfos" required="true" type="query" >
		<cftry>
			<cflog text="AbstractConnectToApp.setupSessionPerimetre(#groupInfos['IDRACINE_MASTER'][1]# - #groupInfos['LIBELLE_GROUPE_CLIENT'][1]# - #groupInfos['IDRACINE_MASTER'][1]#)">
			<cfset SESSION.PERIMETRE = structNew()>
			<cfset SESSION.PERIMETRE.ID_GROUPE		=groupInfos['IDGROUPE_CLIENT'][1]>
			<cfset SESSION.PERIMETRE.GROUPE			=groupInfos['LIBELLE_GROUPE_CLIENT'][1]>
			<cfset SESSION.PERIMETRE.IDRACINE_MASTER=groupInfos['IDRACINE_MASTER'][1]>
			
			<cfreturn TRUE>
		<cfcatch type="any">
			<cfreturn FALSE>
		</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="getDeviseInfos" access="public" output="false" returntype="boolean" >
		
		<cftry>
			<cfquery name="p_retour" datasource="ROCOFFRE">
				SELECT * 
				FROM langue_pays l,devise d 
				WHERE d.iddevise = l.iddevise 
				AND l.langid = #SESSION.USER.IDGLOBALIZATION#
				AND l.code = '#SESSION.USER.GLOBALIZATION#'
			</cfquery>

			<cfset SESSION.USER.DEVISEINFOS = {}>
			<cfset SESSION.USER.DEVISEINFOS.SYMBOL = p_retour.SYMBOL>
			<cfset SESSION.USER.DEVIDEINFOS.NAME = p_retour.NAME>
			<cfset SESSION.USER.DEVISEINFOS.IDDEVISE = p_retour.IDDEVISE>
			<cfset SESSION.USER.DEVISEINFOS.SHORT_DESC = p_retour.SHORT_DESC>
			<cfreturn TRUE>
		<cfcatch type="any">
			<cfreturn FALSE>
		</cfcatch>
		</cftry>

	</cffunction>
	

	<cffunction name="fromAdminToClient" access="remote" output="false" returntype="boolean" >
			<cfset SESSION.IMPERSONNIFICATION = TRUE>
			<cfreturn SESSION.IMPERSONNIFICATION>
	</cffunction>
	
	<cffunction name="fromClientToAdmin" access="remote" output="false" returntype="boolean" >
			<cfset SESSION.IMPERSONNIFICATION = FALSE>
			<cfreturn SESSION.IMPERSONNIFICATION>
	</cffunction>
	
	<cffunction name="getImpersonnification" access="remote" output="false" returntype="boolean">
		<cfif structKeyExists(SESSION,"IMPERSONNIFICATION")>
			<cfreturn SESSION.IMPERSONNIFICATION>
			<cfelse>
				<cfreturn FALSE>
		</cfif>
	</cffunction>
	
	<cffunction name="getCodeApplication" access="remote" output="false" returntype="numeric">
		<cfreturn SESSION.CODEAPPLICATION>
	</cffunction>
	
	<cffunction name="getCodeLangue" access="remote" output="false" returntype="string">

		<cfreturn SESSION.USER.GLOBALIZATION>
	</cffunction>
	
	<cffunction name="getSession" access="remote" output="false" returntype="struct">
		<cfreturn SESSION>
	</cffunction>

	<cffunction name="getUserPassword" access="remote" returntype="Any"  >		
		<cfargument name="email" 			required="true" type="string" />

			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m00.getuserpasword">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="#arguments.email#" >			
				<cfprocresult name="p_retour">
			</cfstoredproc>
			
			<cfset objUser = {email=p_retour.LOGIN_EMAIL,pass=p_retour.PSW}>
			<cfif val(objUser.pass)  eq -2>
				<cfset objUser.pass =  "*****">
			</cfif>
			<cfset t = serializeJson(objUser)>
			<cflog text="*******************************-#t#">
			<cfreturn objUser> 
	</cffunction>
	
</cfcomponent>