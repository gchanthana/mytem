package entity
{
	[Bindable]
	public class FormationVO
	{
		private var _id:Number;
		private var _objSujetFormation:SujetFormationVO = new SujetFormationVO();
		private var _dateFormation:Date;
		private var _nbrParticipants:int;
		private var _objLieuFormation:LieuFormationVO = new LieuFormationVO();
		private var _isActive:Boolean;

		public function FormationVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_FORMATION"))
					this.id = obj.IDFC_FORMATION;
				
				if(obj.hasOwnProperty("DATE_FORMATION"))
					this.dateFormation = obj.DATE_FORMATION as Date;
				
				if(obj.hasOwnProperty("NB_PARTICIPANT"))
					this.nbrParticipants = obj.NB_PARTICIPANT;
				
				this.objSujetFormation.fill(obj);
				
				this.objLieuFormation.fill(obj);
				
				if(obj.hasOwnProperty("IS_ACTIVE"))
					this.isActive = (obj.IS_ACTIVE == 1)?true:false;
					
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet FormationVO erroné ");
			}
			
		}
		
		public function get isActive():Boolean { return _isActive; }
		
		public function set isActive(value:Boolean):void
		{
			if (_isActive == value)
				return;
			_isActive = value;
		}
		
		public function get objLieuFormation():LieuFormationVO { return _objLieuFormation; }
		
		public function set objLieuFormation(value:LieuFormationVO):void
		{
			if (_objLieuFormation == value)
				return;
			_objLieuFormation = value;
		}
		
		public function get nbrParticipants():int { return _nbrParticipants; }
		
		public function set nbrParticipants(value:int):void
		{
			if (_nbrParticipants == value)
				return;
			_nbrParticipants = value;
		}

		public function get dateFormation():Date { return _dateFormation; }
		
		public function set dateFormation(value:Date):void
		{
			if (_dateFormation == value)
				return;
			_dateFormation = value;
		}
		
		public function get id():Number { return _id; }
		
		public function set id(value:Number):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
		public function get objSujetFormation():SujetFormationVO { return _objSujetFormation; }
		
		public function set objSujetFormation(value:SujetFormationVO):void
		{
			if (_objSujetFormation == value)
				return;
			_objSujetFormation = value;
		}
		
	}
}