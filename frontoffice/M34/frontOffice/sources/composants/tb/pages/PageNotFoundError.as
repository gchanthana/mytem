package composants.tb.pages
{
	public class PageNotFoundError extends Error
	{
		public function PageNotFoundError(message:String="", id:int=0)
		{
			
			name = "PageNotFoundError";
			message = "La page n'existe pas";
			id = 1;
			super(message, id);
		}
		
	}
}