package general.appliComposants.customClassButton
{
	import appli.CV4.CreateMenuCV4.CreateMenuCV4IHM;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import general.interfaceClass.IMenu;
	
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.controls.LinkButton;
	import mx.controls.Menu;
	import mx.core.Container;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	
	public class CreateMenuCv4CustomClassButton extends Box
	{
		private var __XMLUnivers:XML;
		private var __chemin:String = "";
		private var selectedItem:Object;
		
		private var _btn:LinkButton
		private var _customMenu:Menu;
		private var _boolUniversOnly:Boolean = true
		private var _boolIsPopup:Boolean = false;
		
		public function CreateMenuCv4CustomClassButton()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init)
		}
		/* ---------------------------------------------------------------------------------
		Initialisation des composants
		--------------------------------------------------------------------------------- */		
		private function init(e:FlexEvent):void
		{
			_btn = new LinkButton()
			_btn.label = __XMLUnivers.@LBL;
			_btn.enabled = true;
			if(__XMLUnivers.@toggled == 'true')
				setBtnStyle(true)	;
			else
				setBtnStyle(false)	;
			_btn.addEventListener(MouseEvent.CLICK,clickHandler);
			_btn.addEventListener(MouseEvent.ROLL_OVER,rolloverHandler);
			this.addChild(_btn)
		}
		/* ---------------------------------------------------------------------------------
		GESTION DU ROLL OVER / OUT du composant
		--------------------------------------------------------------------------------- */		
		private function rolloverHandler(e:MouseEvent):void
		{
			if(!_boolUniversOnly)
			{
				if(!_boolIsPopup)
					AfficherMenu(true);
			}
			else
			{
				refresh();
			}
		}
		private function rollouthandler(e:MouseEvent):void
		{
			AfficherMenu(false);
		}	
		/* ---------------------------------------------------------------------------------
		change le style du bouton en fonction du paramètre isHighlight.
		--------------------------------------------------------------------------------- */		
		private function setBtnStyle(isHighlight:Boolean):void
		{
			if(isHighlight)
			{
				_btn.setStyle('rollOverColor','#cccccc');
				_btn.setStyle('textDecoration','underline');
				_btn.setStyle('borderStyle','solid');
				_btn.setStyle('fontWeight','bold');
			}
			else
			{
				_btn.setStyle('rollOverColor','#cccccc');
				_btn.setStyle('textDecoration','none');
				_btn.setStyle('borderStyle','none');
				_btn.setStyle('fontWeight','normal');
			}
		}
		/* ---------------------------------------------------------------------------------
		Quand on clique sur le bouton
		--------------------------------------------------------------------------------- */		
		private function clickHandler(event:MouseEvent):void
		{
			if(_boolUniversOnly)
			{
				changeUnivers();
			}
			else
			{
				//AfficherMenu(true)	
			}
		}
		/* ---------------------------------------------------------------------------------
		On créé un menu personnalisé
		--------------------------------------------------------------------------------- */		
		private function createCustomMenu():void
		{
			_customMenu = new Menu();
			_customMenu = Menu.createMenu(_btn,__XMLUnivers.children());
			_customMenu.labelField = "@LBL";
			_customMenu.setVisible(true);
			_customMenu.addEventListener(MouseEvent.ROLL_OUT,rollouthandler);
			_customMenu.addEventListener(MenuEvent.CHANGE,changeUniversFunction);
		}
		/* ---------------------------------------------------------------------------------
		Affiche la popup contenant le menu
		--------------------------------------------------------------------------------- */		
		public function AfficherMenu(open:Boolean):void
		{
			if(!_boolUniversOnly)
			{	
				if(open)
				{
					createCustomMenu();
					var tmpPoint:Point = this.localToGlobal(new Point(_btn.x, _btn.y+_btn.height));
					_customMenu.x = tmpPoint.x;
					_customMenu.y = tmpPoint.y;
					_customMenu.selectedItem = this.selectedItem;
					PopUpManager.addPopUp(_customMenu,this);
					refresh();
					_boolIsPopup = true;
				}
				else
				{
					PopUpManager.removePopUp(_customMenu);
					_boolIsPopup = false;
				}
			}
			else
			{
				
			}
		}
		/* ---------------------------------------------------------------------------------
		Fonction qui mets à jour le style de ce bouton
		--------------------------------------------------------------------------------- */		
		public function refreshThisBtnStyle():void
		{
			if(__XMLUnivers.@toggled == 'true')
			{
				setBtnStyle(true);
				setSelectedMenuItem(true);
			}
			else
			{
				setBtnStyle(false);
				setSelectedMenuItem(false);
			}
		}
		/* ---------------------------------------------------------------------------------
		Fonction qui définit l'élément sélectionné du menu
		--------------------------------------------------------------------------------- */		
		private function setSelectedMenuItem(isToHighligght:Boolean):void
		{
			if(isToHighligght)
			{
				var x:XMLList = __XMLUnivers.descendants("FONCTION").(@toggled == true);
				if(x.length() == 1)
					selectedItem = x[0];
				else
					selectedItem = null;
			}
			else
			{
				selectedItem = null;
			}
		}
		/* ---------------------------------------------------------------------------------
		Fonction qui désaffiche les autres menus pour ne laisser 
		que le menu de ce bouton affiché
		--------------------------------------------------------------------------------- */		
		private function refresh():void
		{
			if(this.parent.parent.parent is CreateMenuCV4IHM)
				(this.parent.parent.parent as CreateMenuCV4IHM).refresh(this);  
		}	
		/* ---------------------------------------------------------------------------------
		Quand on sélectionne un élément du menu
		--------------------------------------------------------------------------------- */		
		public function changeUniversFunction(event:Event):void 
		{
			var node:XML = (event as MenuEvent).item as XML;

			CvAccessManager.changeModule(node.@KEY);
			
			_boolIsPopup = false;
		}
		public function changeUnivers():void 
		{
			var node:XML = __XMLUnivers as XML;

			CvAccessManager.changeModule(node.@KEY);
		}
		/* ---------------------------------------------------------------------------------
		GETTER / SETTER
		--------------------------------------------------------------------------------- */
		public function set _XMLUnivers(value:XML):void
		{
			__XMLUnivers = value;
			if(__XMLUnivers.children().length() > 0)
				_boolUniversOnly = false
			else
				_boolUniversOnly = true
		}
		public function get _XMLUnivers():XML
		{
			return __XMLUnivers;
		}
		public function set _chemin(value:String):void
		{
			__chemin = value;
		}
		public function get _chemin():String
		{
			return __chemin;
		}
	}
}