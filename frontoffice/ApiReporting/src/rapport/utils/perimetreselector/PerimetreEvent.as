package rapport.utils.perimetreselector{
	import flash.events.Event;

	public class PerimetreEvent extends Event {
		
		public static const PERIMETRE_GROUP_CHANGED:String = "PERIMETRE_GROUP CHANGED EVENT";
		public static const PERIMETRE_CHILD_RESULT:String = "PERIMETRE CHILD RESULT EVENT";
		public static const NODE_INFOS_RESULT:String = " NODE INFOS RESULT EVENT";
		public static const PERIMETRE_NODE_CHANGED:String = "PERIMETRE NODE CHANGED EVENT";
		
		public function PerimetreEvent(type:String) {
			super(type,true);
		}
	}
}