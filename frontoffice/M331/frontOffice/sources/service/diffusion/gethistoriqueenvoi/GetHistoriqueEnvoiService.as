package service.diffusion.gethistoriqueenvoi
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;

	public class GetHistoriqueEnvoiService
	{
		public var _model:GetHistoriqueEnvoiModel;
		public var handler:GetHistoriqueEnvoiHandler;

		public function GetHistoriqueEnvoiService()
		{
			this._model=new GetHistoriqueEnvoiModel();
			this.handler=new GetHistoriqueEnvoiHandler(model);
		}

		public function get model():GetHistoriqueEnvoiModel
		{
			return this._model;
		}

		public function getHistoriqueEnvoi(idEvent:int, noContrat:int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.GetHistoriqueEnvoiService", "getHistoriqueEnvoi", handler.getHistoriqueEnvoiHandler);

			RemoteObjectUtil.callService(callOp, idEvent, noContrat);
		}
	}
}
