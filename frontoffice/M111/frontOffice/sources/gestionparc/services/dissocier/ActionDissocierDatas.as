package gestionparc.services.dissocier
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	
	import mx.core.Application;
	import mx.resources.ResourceManager;
	
	public class ActionDissocierDatas extends EventDispatcher
	{
		public function ActionDissocierDatas(target:IEventDispatcher=null)
		{
			super(target);
		}
		public function treatDissociationRealized():void
		{
			SpecificationVO.getInstance().boolHasChanged = true;
			SpecificationVO.getInstance().refreshData();
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString("M111","ACTION_REALIZED"),Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.DISSOCIATION_REALIZED));
		}
	}
}