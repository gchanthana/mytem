package composants.util
{
	import flash.events.EventDispatcher;

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import gestionparc.event.EventData;

	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;


	public class LibellesPersos extends EventDispatcher
	{
		public static const LIBELLES_COLLAB_LOADED:String="libelles_collab_loaded";
		public static const LIBELLES_LIGNE_LOADED:String="libelles_ligne_loaded";

		public function LibellesPersos()
		{
		}

		public function getLibellesPersosCollab():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.employes.EmployesUtils", "Liste_Champs_Perso", getLibellesPersosCollabResultHandler);
			RemoteObjectUtil.invokeService(op)
		}

		private function getLibellesPersosCollabResultHandler(re:ResultEvent):void
		{
			if ((re.result is ArrayCollection) && (re.result as ArrayCollection).length > 0)
			{
				var tabResult:ArrayCollection=new ArrayCollection();
				for (var i:int=1; i < 5; i++)
				{
					tabResult.addItem(re.result[0]["C" + i]);
				}
				dispatchEvent(new EventData(LIBELLES_COLLAB_LOADED, false, false, tabResult));
			}
		}

		public function getLibellesPersosLigne():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes", "getCustomFieldsLabel", getLibellesPersosLigneResultHandler);
			RemoteObjectUtil.invokeService(op)
		}

		private function getLibellesPersosLigneResultHandler(re:ResultEvent):void
		{
			if ((re.result is ArrayCollection) && (re.result as ArrayCollection).length > 0)
			{
				var tabResult:ArrayCollection=new ArrayCollection();
				for (var i:int=1; i < 5; i++)
				{
					tabResult.addItem(re.result[0]["C" + i]);
				}
				dispatchEvent(new EventData(LIBELLES_LIGNE_LOADED, false, false, tabResult));
			}
		}

	}
}
