<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.delivery.ftp.FtpDeliveryDefinitionSupport" extends="fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryDefinition">
	<cfset init()>
	<cffunction name="init" returntype="fr.consotel.consoview.api.ibis.publisher.delivery.IDeliveryDefinition" access="public" description="Constructor">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["ftpServerName"]="CONTAINER">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["ftpUserName"]="container">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["ftpUserPassword"]="container">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["sftpOption"]=FALSE>
		<cfset VARIABLES["DELIVERY_OPTIONS"]["remoteFile"]="">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="getType" returntype="string" access="public"
				description="Returns delivery type (Example : ftpOption,emailOption,localOption).">
		<cfreturn "ftpOption">
	</cffunction>
	<!--- 
	<cffunction name="setParameter" returntype="boolean" access="public" description="Set value for a delivery parameter.">
		<cfargument name="parameterName" type="string" required="true" hint="Value is remoteFile for this component">
		<cfargument name="parameterValue" type="string" required="true" hint="Value is remote file path relative to FTP USER HOME DIRECTORY">
		<cfif NOT structIsEmpty(VARIABLES["DELIVERY_OPTIONS"])>
			<cfif UCASE(ARGUMENTS.parameterName) EQ UCASE("remoteFile")>
				<cfset VARIABLES["DELIVERY_OPTIONS"][getType()][ARGUMENTS.parameterName]="/app/pelican/services/OBIEE/" & ARGUMENTS.parameterValue>
				<cfreturn TRUE>
			<cfelse>
				<cfreturn FALSE>
			</cfif>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
	
	<cffunction name="getParameterInfos" returntype="struct" access="public"
				description="Returns Delivery Parameters Infos as a struture. This can be provided as delivery option when executing report.">
		<cfreturn VARIABLES["DELIVERY_OPTIONS"]>
	</cffunction>
	 --->
</cfcomponent>
