package service.selecteur.poolselector
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class PoolSelectorHandler
	{
		private var _model: PoolSelectorModel;
		
		public function PoolSelectorHandler(_model:PoolSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuePoolHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValues(evt.result as ArrayCollection);
			}
		}	
	}
}