package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{

	import gestionparc.services.mdm.MDMService;
	
	import mx.containers.HBox;

	public class ContentGeneralDeviceImpl extends HBox
	{
		/* ------------------------- */
		/* -------- VARIABLE ------- */
		/* ------------------------- */
		[Bindable]
		public var zenService:MDMService;

		/* ---------------------- */
		/* -------- PUBLIC------- */
		/* ---------------------- */
		public function ContentGeneralDeviceImpl()
		{
			super();
			initListener(true);
		}

		public function init():void
		{
		}

		/* -------------------------- */
		/* -------- PROTECTED ------- */
		/* -------------------------- */

		/* ------------------------ */
		/* -------- PRIVATE ------- */
		/* ------------------------ */
		private function initListener(toInit:Boolean):void
		{
			if (toInit)
			{

			}
			else
			{

			}
		}
	}
}
