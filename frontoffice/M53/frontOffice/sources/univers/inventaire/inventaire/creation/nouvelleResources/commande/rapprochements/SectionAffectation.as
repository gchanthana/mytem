package univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements
{
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	
	
	/**
	 * Classe gérant l'affectation des nouvelles lignes à un noeud
	 * */
	[Bindable]
	public class SectionAffectation extends Canvas
	{
		/**
		 * Constante definissant le type d'evenement dispatché lors de l'affectation d'une nouelle ligne
		 * */
		public static const LIGNES_AFFECTEES : String = "lignesAffectees";
		
		//Constante referençant le nom de la classe distante gerant les	rapprochements
		private const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.GestionRapprochement";
		
		//Constante referençant le nom de la méthode distante gerant les affectations
		private const COLDFUSION_AFFECTER : String = "assignLines";
		
		//Constante referençant le nom de la méthode distante qui charge les lignes affectées à un noeud
		private const COLDFUSION_LINES_AFFECTEES : String = "getLinesFromNode";
		
		/**
		 * Reference vers l'arbre des perimetres 
		 * */
		//public var boxTreeOrgaCible : InvSearchTree;
		
		/**
		 * Reference vers le DataGrid des lignes à rapprocher
		 * */		
		public var grdLignesRap : DataGrid;
		
		/**
		 * Reference vers le DataGrid des lignes à affecter
		 * */		
		public var grdLignesAffected : DataGrid;
		
		/**		  
		 * Reference vers la collection contenant la liste des lignes rapprochées
		 * */
		public var listeLignesRap : ArrayCollection = new ArrayCollection();
		
		
		/**		  
		 * Reference vers la collection contenant la liste des lignes à affecter
		 * */
		public var listeLignesAffectees : ArrayCollection = new ArrayCollection();
		
		/**
		 * Reference vers le composant affichant l'arbre des organisations
		 * */
		public var cpArbreAffectation : TreeOrgaIHM;
		
		/**
		 * Reference vers le libelle du noeud cible
		 * */
		 
		private var _libelleNoeudCible : String;
		public function get libelleNoeudCible():String{
			return _libelleNoeudCible;
		}
		public function set libelleNoeudCible(libelle : String):void{
			_libelleNoeudCible = libelle;
		}
				
		/**		  
		 * Reference vers la collection contenant la liste des organisations
		 * */
		/* private var _listeOrganisations : ArrayCollection;
		public function get listeOrganisations ():ArrayCollection{
			if (_listeOrganisations == null) _listeOrganisations = new ArrayCollection(); 
			return _listeOrganisations;
		}
		public function set listeOrganisations ( liste : ArrayCollection):void{
			_listeOrganisations = liste;
		} */
		
		
		
		//Reference locale vers le panier de la commande			
		private var _panier : ElementCommande;
		
		//Reference locale vers la commande
		private var _commande : Commande;
		
		//Reference vers la methode distante chargée d'affecter les nouvelles lignes
		private var opAffecter : AbstractOperation;
		
		//Reference vers la methode distante chargée de liste les lignes affectée à une feuille
		private var opLignesAffectees : AbstractOperation;
		
		//bool init
		private var boolInitArbre : Boolean;
		/***
		 * Constructeur
		 * */		
		public function SectionAffectation()
		{	
			super();		
		}
		
		/**
		 * Ne fait rien pour le moment
		 * */
		public function clean():void{
			 
		}
		
		/**
		 * Affecte la commande et le panier 
		 * @param commande, la commande
		 * @param panier le panier de la commande
		 * */
		public function setCommande(commande : Commande,panier : ElementCommande):void{
			_commande = commande;
			_panier = panier;
			initGrid();
			initOrgaCibel(_commande);
		}
		
		/**
		 * Ajoute une ligne au DataGrid des lignes rapprochées
		 * @param ligne, la ligne à ajouter
		 * */
		public function fillGrid(ligne : ElementCommande):void{
			if (!ConsoviewUtil.isIdInArray(ligne.ligneID,"ligneID",listeLignesRap.source)){
				listeLignesRap.addItem(ligne);
				listeLignesRap.refresh();
			}
		}
		
		
		/**
		 * Supprime une ligne au DataGrid des lignes rapprochées
		 * @param ligne, la ligne à ajouter
		 * */
		public function deleteLigneInGrid(ligne : Object):void{		
			if (ligne.hasOwnProperty("ligneID")){
				var index : int = ConsoviewUtil.getIndexById(listeLignesRap,"ligneID",ligne.ligneID);
				if (index > -1){
					listeLignesRap.removeItemAt(index);
					listeLignesRap.refresh();	
				}
				
				var index2 : int = ConsoviewUtil.getIndexById(listeLignesAffectees,"IDSOUS_TETE",ligne.ligneID);
				if (index2 > -1){
					listeLignesAffectees.removeItemAt(index2);
					listeLignesAffectees.refresh();	
				}
					
			}else{
				trace("Erreur Typage");
			}
			
		}
		
		/**
		 * Valide les affectaion ( les enregistres )
		 * */
		public function validerAffectations():void{
			if (listeLignesAffectees.length > 0){
				doAffecterLigne(listeLignesAffectees.source);	
			}else{
				dispatchEvent(new Event(SectionAffectation.LIGNES_AFFECTEES));
			}
		}
		
		/**
		 * Affecte une ligne à u noeud (Verifie que l'on a selectionner une ligne et une feuille de l'arbre)
		 * */
		protected function AffecterLigne(me : MouseEvent):void{
			if ((grdLignesRap.selectedIndices.length > 0) && 
					(cpArbreAffectation.selectedItem != null) && 
						(cpArbreAffectation.selectedItem.@NID > 0)){
				var nodeObject : Object;
				
				nodeObject = cpArbreAffectation.cpArbreAffectation.treePerimetre.selectedItem;
				
				if (nodeObject != null){
					affecterLigneTMP(grdLignesRap.selectedItems,nodeObject);
				}else{
					Alert.okLabel = "Fermer";   
					Alert.show("Vous devez sélectionner une ligne et une feuille","Erreur");						
				}
				
			}else{
				Alert.okLabel = "Fermer";   
				Alert.show("Vous devez sélectionner une ligne et une feuille","Erreur");
			}
		}
		
		
		/**
		 * Charge la liste des lignes affecté à un noeud
		 * */		
		/* protected function getligneAffctedToNode(me : MouseEvent):void{
			if (cpArbreAffectation.cpArbreAffectation.treePerimetre.selectedIndex != -1){
				listeLignesAffectees.filterFunction = filterFunc;
				listeLignesAffectees.refresh();
			}else{
				ScpArbreAffectation.cpArbreAffectation.treePerimetre.selectedIndex = -1;
			}   
		} */
		
		
		/**
		 * Formate le flag 'A'ffecté d'une colonne de DataGrid
		 * Voir la propriété LabelFunction d'un DataGridColumn pour les parametres
		 * */
		protected function formatColAffected(item : Object,column : DataGridColumn):String{		
			return (item[column.dataField] == 1)?"A":"NA";			
		}
		
		
		/**
		 * Formate les numeros de téléphone d'une colonne de DataGrid
		 * Voir la propriété LabelFunction d'un DataGridColumn pour les parametres
		 * */
		protected function formatPhoneNumber(item : Object,column : DataGridColumn):String{		
		 
			if (isNaN(item[column.dataField])){
				return item[column.dataField];
			}else{
				return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);		
			}	
			
		}
		
		/**
		 * Filtre les élements de la liste des lignes affectées sur la propriété NID;
		 * */
		/* protected function filterFunc(value : Object):Boolean{									
			if (value.NID == int(SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchTree.selectedItem.@NID)){
				return (true);
			} else {
				return (false);
			}			
		} */
		
		
		/**
		 * Initialisation de l'arbre 
		 * */
		protected function initTreeCible():void{
			/* boxTreeOrgaCible.mainView.selectedIndex = 1;
			boolInitArbre = true; */
				 
		/* 	SearchPerimetreWindow(boxTreeOrgaCible.searchTree).resultControl.enabled = false;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).resultControl.visible = false;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchInput.visible = false;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.visible = false;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).lblRecherche.visible = false;
			
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).lblRecherche.height = 0;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.height = 0;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchInput.height =0;			
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).resultControl.height = 0;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).resultControl.height = 0;
			
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).lblRecherche.width= 0;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.width = 0;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchInput.width =0;			
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).resultControl.width = 0;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).resultControl.width = 0; 
			
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.enabled = false;*/
			
			
			 //---- NEW ----------------------------------------------
			 
			 //--------------------------------------------------------
			/* SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchIndex.visible = false;
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.label = "Autre noeud";
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).lblRecherche.text = "";
			
			
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.addEventListener(MouseEvent.CLICK,btBackClickHandler);
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchTree.setStyle("borderStyle","solid");			
 */		}
		
		/* private function btBackClickHandler(me : MouseEvent):void{
			if (boolInitArbre){
				SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.removeEventListener(MouseEvent.CLICK,btBackClickHandler);
				boolInitArbre = false;
				SearchPerimetreWindow(boxTreeOrgaCible.searchTree).btnBack.label = "Retour";
				SearchPerimetreWindow(boxTreeOrgaCible.searchTree).lblRecherche.text = "Recherche de :";
			}
		} */
		
		
		//Initialisation du grid des lignes rapprochées
		private function initGrid():void{			
			for (var i: int = 0; i < _panier.liste.length; i++){
				if (ElementCommande(_panier.liste[i]).flagRapproche > 0)
					fillGrid(ElementCommande(_panier.liste[i]));
			}
		}
		
		//Initialisation du grid des lignes rapprochées
		private function initOrgaCibel(cmde : Commande):void{
			cpArbreAffectation.noeudSelectionneParDefaut = cmde.idGroupeCible; 
			//selectNode(cmde.idGroupeCible);
		}
		
		//selectionne un noeud de l'arbre suivant l'id passé en parametre
		//param in nodeId l'identifiant du noeud que l'on veut selectionner		
		/* private function selectNode(nodeId : int):void
		{	
			if (_commande.idGroupeCible > 0)
				boxTreeOrgaCible.getNodeXmlPathCible(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,_commande.idGroupeCible);
				boxTreeOrgaCible.addEventListener(InvSearchTree.NODE_READY_EXP,renseigneRechercheHandler);
		} */
		
		//Un fois le noeud selectionné
		/* private function renseigneRechercheHandler(ev : Event):void{
			SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchInput.text = SearchPerimetreWindow(boxTreeOrgaCible.searchTree).searchTree.selectedItem.@LBL;
		} */
		
		
		//Ajoute les lignes selectionnées à la liste des lignes à affecter
		//param in 	lignes un Array contenant les lignes sélectionées
		//			nodeID l'identifiant du noeud sur lequel on affecte les lignes
		private function affecterLigneTMP(lignes : Array, node : Object):void{
			if (node != null && node.@NTY != 1){
				for (var i: int = 0 ; i < lignes.length; i++){
					var index : int;
					var ligneAff : Object = new Object();				
					ligneAff.SOUS_TETE = ElementCommande(lignes[i]).libelleLigne;
					ligneAff.IDSOUS_TETE = ElementCommande(lignes[i]).ligneID;
					ligneAff.NODE_ID = Number(node.@NID);
					ligneAff.LIBELLE_GROUPE_CLIENT = String(node.@LBL);
					ligneAff.LIBELLE_ORGANISATION = String(cpArbreAffectation.cpArbreAffectation.cboOrganisation.selectedItem.LIBELLE_GROUPE_CLIENT);
					ligneAff.IDORGANISATION = Number(cpArbreAffectation.cpArbreAffectation.cboOrganisation.selectedItem.IDGROUPE_CLIENT);
					listeLignesAffectees.filterFunction = null;
					listeLignesAffectees.refresh();
					
					var boolSousTeteDejaAffectee : Boolean = ConsoviewUtil.isIdInArray(ligneAff.IDSOUS_TETE,"IDSOUS_TETE",listeLignesAffectees.source); 
					 
					if (!boolSousTeteDejaAffectee){
						listeLignesAffectees.addItem(ligneAff);
						index = listeLignesAffectees.getItemIndex(ligneAff);
						grdLignesAffected.selectedIndex = index;
						grdLignesAffected.scrollToIndex(index);						
					}else if(boolSousTeteDejaAffectee){						
						index = ConsoviewUtil.if2InArrayReturnIndex(ligneAff.IDSOUS_TETE,ligneAff.IDORGANISATION,"IDSOUS_TETE","IDORGANISATION",listeLignesAffectees.source);
						var ligneDejaAffectee : Object;																		
						if(index > -1){
							ligneDejaAffectee = listeLignesAffectees.source[index];
							var oldNodeLibelle : String = ligneDejaAffectee.LIBELLE_GROUPE_CLIENT; 
							var oldOrganisationLibelle : String =ligneDejaAffectee.LIBELLE_ORGANISATION;
							var newNodeLibelle : String = ligneAff.LIBELLE_GROUPE_CLIENT;							
							ligneDejaAffectee.NODE_ID = ligneAff.NODE_ID;
							ligneDejaAffectee.LIBELLE_GROUPE_CLIENT = ligneAff.LIBELLE_GROUPE_CLIENT;
						}else{
							listeLignesAffectees.addItem(ligneAff);
							index = listeLignesAffectees.getItemIndex(ligneAff);
							grdLignesAffected.selectedIndex = index;
							grdLignesAffected.scrollToIndex(index);
						}
									
						
						/* if (ligneDejaAffectee.IDORGANISATION == ligneAff.IDORGANISATION){							
							var oldNodeLibelle : String = ligneDejaAffectee.LIBELLE_GROUPE_CLIENT;
							var oldOrganisationLibelle : String =ligneDejaAffectee.LIBELLE_ORGANISATION;
							var newNodeLibelle : String = ligneAff.LIBELLE_GROUPE_CLIENT;							
							ligneDejaAffectee.NODE_ID = ligneAff.NODE_ID;
							ligneDejaAffectee.LIBELLE_GROUPE_CLIENT = ligneAff.LIBELLE_GROUPE_CLIENT;
						}else{
							listeLignesAffectees.addItem(ligneAff);
							index = listeLignesAffectees.getItemIndex(ligneAff);
							grdLignesAffected.selectedIndex = index;
							grdLignesAffected.scrollToIndex(index);
						} */					
					}
					listeLignesAffectees.refresh();
				}
			}else{
				Alert.okLabel = "Fermer";					
				Alert.show("Vous devez sélectionner une feuille");
			}
		}
		
		
		
		
		//------- REMOTINGS -------------------------------------------------
		
		//Affecte les lignes au noeud
		private function doAffecterLigne(lignes : Array):void{			
			opAffecter = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				COLDFUSION_COMPONENT,
																				COLDFUSION_AFFECTER,
																				affecterLigneResultHandler,
																				null);			
																				
			var tabLigneIds : Array = ConsoviewUtil.extractIDs("IDSOUS_TETE",lignes);			
			var tabNodeIds : Array = ConsoviewUtil.extractIDs("NODE_ID",lignes);
			RemoteObjectUtil.callService(opAffecter,tabNodeIds,tabLigneIds);																					
		}
		
		
		//Handler de la méthode doAffecterLigne, dispatch un évènement 'SectionAffectation.LIGNES_AFFECTEES' 
		//signifiant que les lignes ont été affectées avec succes
		private function affecterLigneResultHandler(re : ResultEvent):void{
				if (re.result > 0){					
					dispatchEvent(new Event(SectionAffectation.LIGNES_AFFECTEES));
				}else{
					var message : String = "Les produits ont bien été rapprochés. Cependant les lignes n'ont pas pu être affectées.";
					 /* code Erreur
						case -1 : "Erreur affectaion";break;
						case -2 : "Vous devez selectionner un feuille dans l'organisation";break;
						case -3 : "Vous devez selectionner un feuille dans l'organisation";break;
					*/
 					Alert.show(message,"Erreur " + re.result);
 					dispatchEvent(new Event(SectionAffectation.LIGNES_AFFECTEES));
				}		
		}
		
		//Charge la liste des lignes affectées a un noeud
		//param in nodeId 
		private function doGetligneAffctedToNode(nodeId : int):void{
			opLignesAffectees = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				COLDFUSION_COMPONENT,
																				COLDFUSION_LINES_AFFECTEES,
																				ligneAffctedToNodeResultHandler,
																				null);							
			RemoteObjectUtil.callService(opLignesAffectees,nodeId,0,DateFunction.formatDateAsInverseString(new Date()));																					
		}
		
		//Handler de la méthode doGetligneAffctedToNode,			
		private function ligneAffctedToNodeResultHandler(re : ResultEvent):void{
			if (re.result){
				listeLignesAffectees = re.result as ArrayCollection;				
			}else{
				Alert.show("Erreur listing","Erreur");
			}		
		}
		
	}
}