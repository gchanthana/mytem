package gestioncommande.entity
{
	public class ImportMasse
	{
		
		public var LIBELLE_SELECTED			:Boolean=false;
		public var COLLAB_SELECTED			:Boolean=false;
		public var PORTABILITÉ				:Boolean=false;
		public var ENABLEPORTABILITE		:Boolean=false;
		public var COLLAB_ENABLED			:Boolean=false;
		public var LIBELLE_ENABLED			:Boolean=false;
		
		public var IDEMPLOYE				:int	= 0;
		public var IDGROUPE_CLIENT			:int	= 0;
		public var IDCOLLABORATEUR			:int	= 0;
		public var IDSOUS_TETE				:int 	= 0;
		
		public var MATRICULE				:String = "";
		public var LIBELLE_CONFIGURATION	:String = "";
		public var LIBELLÉ					:String = "";
		public var NOM						:String = "";
		public var COLLABORATEUR			:String = "";
		public var NUM_LINE					:String = "";
		public var NUMÉRO					:String = "";
		public var NUM_RIO					:String = "";
		public var DATE_PORTABILITE			:String = "";
		public var SOUS_TETE				:String = "";
		
		public var CONFIG_ID				:int	= 0;
		public var CONFIGURATION_ID			:int	= 0;
		public var IDCONFIGNUMBER			:int	= 0;
		
		public var LIBELLETYPELIGNE			:String = "";
		public var LIBELLE_SOURCE			:String = "";
		public var LIBELLE_CIBLE			:String = "";
		public var IDSOURCE					:int = 0;
		public var IDCIBLE					:int = 0;

		public var IDTYPECOMMANDE			:int = 0;
		public var IDSEGMENT				:int = 0;
		public var IDTYPELIGNE				:int = 0;
		
		public var IDUNIQUE					:int = 0;
		
		public function ImportMasse()
		{
		}

	}
}