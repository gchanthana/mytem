package gestionparc.services.commande
{
	import composants.util.DateFunction;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.CommandeVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.TerminalVO;
	
	import mx.rpc.AbstractOperation;


	public class CommandeServices
	{

		public var myDatas:CommandeDatas;
		public var myHandlers:CommandeHandlers;

		public function CommandeServices()
		{
			myDatas=new CommandeDatas();
			myHandlers=new CommandeHandlers(myDatas);
		}

		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Toutes les informations concernant la commande.
		 */
		public function setInfosCommande(myCommande:CommandeVO, myEquipement:TerminalVO):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.Commande", "enregistrerCommande", myHandlers.setInfosCommandeResultHandler);

			var idoperateur:Number=-1;
			if (myEquipement.simRattache != null && myEquipement.simRattache.operateur != null)
				idoperateur=myEquipement.simRattache.operateur.idOperateur;


			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool, idoperateur, myEquipement.revendeur.idRevendeur, myCommande.idSiteLivraison, myCommande.libelle, myCommande.refClient, myCommande.refOperateur, DateFunction.formatDateAsInverseString(myCommande.dateAchat), DateFunction.formatDateAsInverseString(myCommande.dateAchat), myCommande.commentaires, myCommande.prix, myCommande.numCommande, myEquipement.idEquipement, DateFunction.formatDateAsInverseString(myCommande.dateDebut), myCommande.duree, myCommande.refLivraison, DateFunction.formatDateAsInverseString(myCommande.dateLivraison), (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEFIXE) ? true : false, // segment fixe 
				(SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE) ? true : false // segment mobile 
				);
		}

		/**
		 * Appelle la procédure permettant de générer un numéro de commande unique.
		 */
		public function genererNumeroDeCommande():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.Commande",
				"genererNumeroDeCommande", myHandlers.genererNumeroDeCommandeResultHandler);
			RemoteObjectUtil.callService(op);
		}
	}
}
