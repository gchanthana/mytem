package gestionparc.services.setcolonnes
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class SetColonneService
	{
		[Bindable]
		public var model:SetColonneData;
		public var myHandler:SetColonneHandler;
		private var myData:ArrayCollection;
		private var remote:RemoteObject;
		
		public function SetColonneService()
		{
			model=new SetColonneData();
			myHandler=new SetColonneHandler(model);
			myData=new ArrayCollection();
		}

		public function setColonneVueParc(id: String):void
		{
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			remote.source="fr.consotel.consoview.M111.colonnes.SetColonnesVue";
			remote.addEventListener(ResultEvent.RESULT,myHandler.ViewResultHandler,false,0,true);
			remote.setAllColonnes(id);
		}
	}
}
