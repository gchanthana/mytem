<cfcomponent hint="classe de gestion des services lies a la manipulation des templates de collectes" output="false">

   <!--- Fournit la liste des templates de collectes 
            en fonction de :
		    l'opérateur (id) : si null tous
            du type de données : si null tout type
            du mode de récupération : si null tout mode
	 --->
	<cffunction name="getCollecteTypeList" access="public" returntype="query" output="false">		
		<cfargument name="selectedOperateurId"  required="true" type="numeric" default=""/>
		<cfargument name="selectedDataType" required="true" type="numeric" default=""/>
		<cfargument name="selectedRecuperationMode" required="true" type="numeric" default=""/>
				
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.search_template">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#selectedOperateurId#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#selectedDataType#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#selectedRecuperationMode#" >
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
   <!--- Fournit la liste des templates publié 
            en fonction de :
		    l'opérateur (id) : si null tous
            du type de données : si null tout type
            du mode de récupération : si null tout mode
	 --->
	<cffunction name="getPublishedTemplate" access="public" returntype="query" output="false">		
		<cfargument name="selectedOperateurId"  required="true" type="numeric" default=""/>
		<cfargument name="selectedDataType" required="true" type="numeric" default=""/>
		<cfargument name="selectedRecuperationMode" required="true" type="numeric" default=""/>
				
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_published_template">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#selectedOperateurId#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#selectedDataType#" >
	 		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#selectedRecuperationMode#" > 
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	
   <!--- Fournit la liste des  collectes pour une racine donnée
          donné (idRacine)        
	 --->
	<cffunction name="getCollecteClientList" access="public" returntype="query" output="false">		
		<cfargument name="racineId"  required="true" type="numeric" default=""/>
						
		 <cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.search_collect_client">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#racineId#" >			
			<cfprocresult name="p_retour">
		</cfstoredproc> 
		<cfreturn p_retour>
	</cffunction>
	
   <!--- Fournit la liste des groupes maitres (libelle,racineId,nbre de collectes)
          donné (idRacine)        
	 --->
	<cffunction name="getRacineList" access="public" returntype="query" output="false">		
								
		 <cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_racine">				
			<cfprocresult name="p_retour">
		</cfstoredproc> 
		<cfreturn p_retour>
	</cffunction>
	
	
  
   <!--- 
        Crée ou édite une nouvelle collecte
	    si  p_idcollecteTemplate=0 : MODE CREATION else MODE EDITION
	    retour de l'id de la collecte crée/modifié
	    retour code erreur	           
	 --->
	<cffunction name="createEditCollecteClientVo" access="public" returntype="numeric" output="false">		
		   <cfargument name="p_idracine"  hint="l'id racine de l'utilisateur" type="numeric" />
		   <cfargument name="p_idcollectClient"  hint="l'id de la collecte" type="numeric" />
		   <cfargument name="p_idcollectTemplate"  hint="l'id du template associé" type="numeric" />
		   <cfargument name="p_libelleCollect"  hint="le libellé de la collecte" type="string" />
		   <cfargument name="p_roic"  hint="l'id roic de la collecte" type="string" />
		   <cfargument name="p_identifiant"  hint="le login de connexion si mode pull" type="string" />
		   <cfargument name="p_mdp"  hint="le password de connexion si mode pull" type="string" />
		   <cfargument name="p_plageImport"  hint="la plage d'ouverture des imports" type="date" />
		   <cfargument name="p_retroactivite"  hint="bool retroactivite ou non" type="numeric" />
		   <cfargument name="p_activiationAlerte"  hint="bool activation alerte ou non" type="numeric" />
		   <cfargument name="p_delaiAlerte"  hint="delai de l'alerte" type="numeric" />
		   <cfargument name="p_mailAlerte"  hint="mail du destinataire de l'alerte" type="string" />
		   <cfargument name="p_isActif"  hint="collecte active ou non" type="numeric" />
		   <cfargument name="p_userCreate"  hint="id de l'utilisateur ayant crée la collecte" type="numeric" />		  
		   <cfargument name="p_userModif"  hint="id de l'utilisateur ayant modifié la collecte" type="numeric" />
		

		 <cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.IU_collect_client">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idracine#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcollectClient#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcollectTemplate#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libelleCollect#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_roic#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_identifiant#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_mdp#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_DATE" value="#p_plageImport#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_retroactivite#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_activiationAlerte#" >	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_delaiAlerte#" >				
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_mailAlerte#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_isActif#" >					
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_userCreate#" >					
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_userModif#" >				
			<cfprocparam  variable="p_retour" type="Out" cfsqltype="CF_SQL_INTEGER">		
		</cfstoredproc> 
		<cfreturn p_retour>
										
	</cffunction>
	
   <!--- Crée ou met à jour un template de collecte	         
	          si vo.id = 0 : Mode CREATION
	          si vo.id !=0 : Mode UPDATE		
		 retourne -1 ou code erreur en cas d'echec'
	 --->
	<cffunction name="createEditCollecteTypeVo" access="public" returntype="numeric" output="false">		
    	<cfargument name="p_idcollecteTemplate"  hint="l'id de la collecte" type="numeric" />
		<cfargument name="p_uidcollecteTemplate" hint="l'identifiant unique du template ( pour stockage des fichiers)" type="string" />
		<cfargument name="p_idcollecteType" hint="l'id du type de donnees ( Facturation, Usages,Facturation et usages)" type="numeric" />
		<cfargument name="p_idcollecteMode"  hint="l'id du mode de recuperation du template (Push, Pull ...)" type="numeric" />
		<cfargument name="p_operateurId" hint="l'id de l'operateur" type="numeric" />
		<cfargument name="p_libelleTemplate" hint="le libelle du template" type="string" />
		<cfargument name="p_isPublished" hint="boolean indiquant si le template est publiee ou non" type="numeric" />
		<cfargument name="p_isCompleted" hint="boolean indiquant si le template est complet ou non" type="numeric" />
		<cfargument name="p_prestaPerifacturation" hint="prestation de perifacturation" type="string" />
		<cfargument name="p_instructionsPath" hint="url de la PJ d'instructions pour la perifacturation" type="string" />		
		<cfargument name="p_modeSouscription"  hint="mode de souscription ( extranet, contrat ...)" type="numeric" />
		<cfargument name="p_modeSouscriptionPath" hint="url de la PJ d'aide au mode de souscription" type="string" />
		<cfargument name="p_modeSouscriptionUrl" hint="url du mode de souscription extranet" type="string" />
		<cfargument name="p_delaiDispo" hint="delai de mise e disposition ( en j ouvres)" type="numeric" />
		<cfargument name="p_delaiDispoInfos" hint="info sur le delai de mise e disposition" type="string" />
		<cfargument name="p_contraintes" hint="boolean indiquant s'il y a contraintes ou non" type="numeric" />
		<cfargument name="p_contraintesInfos" hint="infos sur la contraintes ( ssi  contraintes=1)" type="string" />
		<cfargument name="p_anteriorite" hint="bool indiquant s'il y a anteriorite des donnees ou non" type="numeric" />
		<cfargument name="p_anterioriteInfos" hint="infos sur l'anteriorite ( ssi  anteriorite=1)" type="string" />
		<cfargument name="p_couts" hint="boolean indiquant s'il y a un cout ou non (payant ou gratuit)" type="numeric" />
		<cfargument name="p_coutsInfos" hint="Infos sur les couts ( ssi couts=1)" type="string" />
		<cfargument name="p_contact" hint="contact" type="string" />
		<cfargument name="p_contactInfos" hint="Infos sur le contact" type="string" />
		<cfargument name="p_libelleRoic" hint="libelle de la Ref Operateur Id Client" type="string" />
		<cfargument name="p_recuperationRoic" hint="Mode de recuperation du Roic" type="string" />
		<cfargument name="p_roicInfos" hint="Infos sur le  Ref Operateur Id Client" type="string" />
		<cfargument name="p_localisationRoic" hint="Loaclisation du Roic" type="string" />
		<cfargument name="p_localisationRoicPath" hint="Url de la PJ d'infos sur la localisation du Roic" type="string" />
		<cfargument name="p_libelleRocf" hint="libelle de la Ref Operateur Chaine de Factures" type="string" />
		<cfargument name="p_localisationRocf" hint=" localisation de la Ref Operateur Chaine de Factures" type="string" />
		<cfargument name="p_localisationRocfPath" hint="url de la Pj informant sur la localisation de la Ref Operateur Chaine de Factures" type="string" />
		<cfargument name="p_iduserCreate" hint="l'id de l'utilisateur ayant cree le template" type="numeric" />
		<cfargument name="p_idUserModif" hint="l'id de l'utilisateur ayant modifie  le template" type="numeric" /> 
            
	   <cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.IU_template">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcollecteTemplate#"  >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_uidcollecteTemplate#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcollecteType#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idcollecteMode#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_operateurId#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libelleTemplate#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_isPublished#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_isCompleted#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_prestaPerifacturation#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_instructionsPath#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_modeSouscription#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_modeSouscriptionPath#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_modeSouscriptionUrl#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_delaiDispo#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_delaiDispoInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_contraintes#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_contraintesInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_anteriorite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_anterioriteInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_couts#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_coutsInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_contact#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_contactInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libelleRoic#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_recuperationRoic#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_roicInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_localisationRoic#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_localisationRoicPath#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libelleRocf#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_localisationRocf#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_localisationRocfPath#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_iduserCreate#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idUserModif#">						 		
			<cfprocparam  variable="p_retour" type="Out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		<cfreturn p_retour >
	</cffunction>
	
	
	
	 <!--- Fournit la liste des opérateurs  [id, libelle, libellé rocf associé] --->
           		   
	<cffunction name="getOperateurList" access="public" returntype="query" output="false">		
						
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_operateur" >			
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	 <!--- Fournit la liste des opérateurs avec template  [id, libelle, libellé rocf associé] --->	
	<cffunction name="getOperateurWithTemplate" access="public" returntype="query" output="false">		
						
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_operateur_template" >			
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
   <!--- Fournit la liste des types de données [id,libelle] (ex : facturation, usages ...) --->
          		   
	<cffunction name="getDatatypeList" access="public" returntype="query" output="false">		
						
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_type_collecte" >			
			<cfprocresult name="p_retour">
		</cfstoredproc>
				
		<cfreturn p_retour>
		
	</cffunction>
	
   <!--- Fournit la liste des modes de récupération  [id,libelle] (ex : PULL-FTP, PUSH-MAIL ...) --->
           		   
	<cffunction name="getRecupModeList" access="public" returntype="query" output="false">		
						
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.get_mode_collecte" >			
			<cfprocresult name="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>
	
	<!--- Test les identifiants de connexion donnée par l'utilisateur pour recollecter
	      les données depuis son intranet --->
           		   
	<cffunction name="testIdentification" access="public" returntype="Numeric" output="false">		
		<cfargument name="intanet_url"  required="true" type="String" default=""/>
		<cfargument name="login"  required="true" type="String" default=""/>
		<cfargument name="password"  required="true" type="String" default=""/>
		
			<!--- TODO tester l'identification  OK : 1  , KO : -1 --->		
			
		<cfreturn 1 >
	</cffunction>
	
	<!--- Crée un identifiant unique associé à un template et 
	nécessaire pour stocker les documents sur le serveur
	   --->
	<cffunction name="createUIDStorage"  returntype="any" displayname="UID associé à un template de collecte" description="crée un identifiants unique pour le stockage des fichiers sur le serveur" hint="crée un identifiants unique pour le stockage des fichiers sur le serveur" >
	     <cfset uid=#createUUID()# />      		
		 <cfreturn #uid#>
	</cffunction>
	
	<!--- Supprime un dossier UID spécifique suite à l'annulation
	 de création d'un template	
	   --->
	<cffunction name="destroyLastUID" access="public" returntype="Numeric" output="false">		
		<cfargument name="p_uid"  required="true" type="String" />
				
			<!--- TODO suppimer le dossier et contenu de stockage
			  nommé comme l'uid passé en param  --->		
			
		<cfreturn 1 >
	</cffunction>
	

</cfcomponent>