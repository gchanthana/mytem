	package gestioncommande.entity
	{
		import flash.events.EventDispatcher;
		import flash.utils.describeType;
		
		import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
		
		import gestioncommande.events.CommandeEvent;
		
		import mx.collections.ArrayCollection;
		import mx.rpc.AbstractOperation;
		import mx.rpc.events.ResultEvent;
		
		import session.SessionUserObject;
	
		[RemoteClass(alias="fr.consotel.consoview.M16.CommandeVO")]
	
		[Bindable]
		public class Commande extends EventDispatcher
		{
		public var IS_CONCERN			:Number=0;
		public var IDCOMMANDE			:Number=0;
		public var IDCONTACT			:Number=0;
		public var IDREVENDEUR			:Number=0;
		public var IDROLE				:Number=101;
		public var IDOPERATEUR			:Number=0;
		public var IDTYPE_COMMANDE		:Number=0;
		public var IDSOCIETE			:Number=0;
		public var IDCOMPTE_FACTURATION	:Number=0;
		public var IDSOUS_COMPTE		:Number=0;
		public var IDRACINE				:Number=0;
		public var IDLAST_ETAT			:Number=0;
		public var IDLAST_ACTION		:Number=0;
		public var IDSITELIVRAISON		:Number=0;
		public var IDTRANSPORTEUR		:Number=0;
		public var IDPOOL_GESTIONNAIRE	:Number=0;
		public var IDGESTIONNAIRE		:Number=0;
		public var IDPROFIL				:Number=0;
		public var IDINV_ETAT			:Number=0;
		public var BOOL_MAIL			:Boolean=false;
		public var ID_MAIL				:int=-99;
		public var IDSITEFACTURATION	:Number=0;
		public var IDEQUIPEMENTPARENT	:Number=0;
		public var IDACTEPOUR			:Number=0;
		public var IDPROFIL_EQUIPEMENT	:Number=0;
		public var USERCREATE			:String="";
		public var USERID				:int=-1;
		public var NUMERO_COMMANDE		:String="";
		public var REF_OPERATEUR		:String="";
		public var REF_CLIENT1			:String="";
		public var REF_CLIENT2			:String="";
		public var REF_CLIENT11			:String="";
		public var REF_CLIENT21			:String="";
		public var LIBELLE_TO			:String="";
		public var LIBELLE_PROFIL		:String="";
		public var LIBELLE_COMMANDE		:String="";
		public var LIBELLE_POOL			:String="";
		public var LIBELLE_REVENDEUR	:String="";
		public var LIBELLE_OPERATEUR	:String="";
		public var LIBELLE_COMPTE		:String="";
		public var LIBELLE_SOUSCOMPTE	:String="";
		public var LIBELLE_TRANSPORTEUR	:String="";
		public var LIBELLE_SITELIVRAISON:String="";
		public var LIBELLE_LASTETAT		:String="";
		public var LIBELLE_LASTACTION	:String="";
		public var COMMENTAIRES			:String="";
		public var LIBELLE_SITEFACTURATION	:String="";
		public var LIBELLE_CONFIGURATION	:String="";
		public var SHAREMODELEALLPOOLS		:Boolean=false;
		public var PRICE_EQUIPEMENT			:Object=new Object();
		public var DATE_HEURE			:String="";
		public var TYPE_OPERATION		:String="";
		public var PATRONYME_CONTACT	:String="";
		public var CREEE_PAR			:String="";
		public var MODIFIEE_PAR			:String="";
		public var BOOL_ENVOYER_VIAMAIL	:Number=0;
		public var CREEE_LE				:Date=null;
		public var MODIFIEE_LE			:Date=null;
		public var ENVOYER_LE			:Date=null;
		public var LIVREE_LE			:Date=null;
		public var LIVRAISON_PREVUE_LE	:Date=null;
		public var DATE_COMMANDE		:Date=null;
		public var EXPEDIE_LE			:Date=null;
		public var DATE_CREATED			:String="";
		public var DATE_LIV				:String="";
		public var BOOL_DEVIS			:Number=0;
		public var NUMERO_TRACKING		:String="";
		public var MONTANT				:Number=0;
		public var MONTANT_CDE			:Number=0;
		public var MONTANT_TOTAL		:Number=0;
		public var SEGMENT_FIXE			:Number=0;
		public var SEGMENT_MOBILE			:Number=1;
		public var LIVRAISON_DISTRIBUTEUR	:int=0;
		public var IS_LIV_DISTRIB			:Boolean=false;	
		public var SEGMENT_DATA				:Number=0;
		public var IDGESTIONNAIRE_MODIF		:Number=0;
		public var IDGESTIONNAIRE_CREATE	:Number=0;
		public var IDGROUPE_REPERE			:Number=0;
		public var ENCOURS				:Number=1;
		public var EMAIL_CONTACT		:String="";
		public var DATE_CREATE			:Date=null;
		public var ENGAGEMENT			:String="";
		public var CONFIG_NUMBER		:int=1;
		public var ACTION				:String="";
		public var ARTICLES					:XML=<articles></articles>;
		public var COMMANDE					:ArrayCollection=new ArrayCollection();
		public var COMMANDE_TEMPORAIRE		:ArrayCollection=new ArrayCollection();
		public var CONFIGURATION_TEMPORAIRE	:ArrayCollection=new ArrayCollection();
		public var IMPORTMASSE_COLLAB		:ArrayCollection=new ArrayCollection();
		public var SELECTED					:Boolean=false;
		public var NEWCONFIG				:Boolean=true;
		public var V1						:String="";
		public var V2						:String="";
		public var V3						:String="";
		public var V4						:String="";
		public var V5						:String="";
		public var DEVISE					:String="";
		public var PAYS_OPERATEUR			:String=''; // Le pays de l'opérateur
		public var PAYS_CONSOTELID			:int=0; // L'identifiant du pays de l'opérateur
		public static const USA_PAYS_CONSOTELID:int = 20; // identifiant du pays="USA" des opérateur id_usa = 20
//		public static const USA_PAYS_CONSOTELID:int = 1577; // A effacerr ce test identifiant du pays="France" des opérateur id_usa = 1577
		public var ADRESSE_FACTURATION		:String = '';
		
		public var NBRECORD					:int=30;
		public var NBROWS					:int=0;
		public var NB_ROWS					:int=0;
		public static const ID_TOUS_LES_POOLS:Number 	= -1;
		
		
			public function Commande(auto:Boolean=false)
			{
				var idPool:Number = SessionUserObject.singletonSession.POOL.IDPOOL;
				if(auto)
				{
					genererNumeroDeCommande(idPool);
				}
			}
		
			public function copyCommande():Commande
			{
				var copyCmd		:Commande  	= new Commande();
				var classInfo 	:XML 		= describeType(this);

	            for each (var v:XML in classInfo..accessor)
	            {
	               if (this.hasOwnProperty(v.@name))
	               {
	               		copyCmd[v.@name] = this[v.@name];
	               }else{
	              // 	trace("-----> echec mapping ["+v.@name+"] ou valeur null" );
	               }
	            }   
	            
	            return copyCmd;         				 		
			}
		
			protected function genererNumeroDeCommande(idPool:Number = -1):void
		    {
		    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				    "fr.consotel.consoview.inventaire.commande.GestionCommande",
																				    "genererNumeroDeCommande",
																				    genererNumeroDeCommandeResultHandler);
		    	RemoteObjectUtil.callService(op, idPool);
		    }
		    
		    private function genererNumeroDeCommandeResultHandler(event:ResultEvent):void
		    {
		       	NUMERO_COMMANDE=String(event.result);
		       	REF_OPERATEUR=String(event.result);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.INFOS_COMMANDE, true));
		    }
		}
}