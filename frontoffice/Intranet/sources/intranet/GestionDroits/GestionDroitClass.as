package intranet.GestionDroits
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	public class GestionDroitClass
	{
		public function GestionDroitClass()
		{
		}

		public function getListeRacine(resulthandler:Function,faulthandler:Function):void{
			var tmpListeRacine:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getListeRacine",
						resulthandler,faulthandler);
			RemoteObjectUtil.callService(tmpListeRacine);			
		}

		public function getListeLogin(resulthandler:Function,faulthandler:Function,idracine:int):void{
			var tmpUser:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getListeLogin",
				resulthandler,faulthandler);
			if(idracine == -1){
				RemoteObjectUtil.callService(tmpUser);
			}else{
				RemoteObjectUtil.callService(tmpUser,idracine);
			}
		}
		
		public function getListeProfil(resulthandler:Function,faulthandler:Function,idracine:int):void{
			var tmpListeProfile:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getListeProfil",
				resulthandler,faulthandler);
			if(idracine==-1){
				RemoteObjectUtil.callService(tmpListeProfile);
			}else{
				RemoteObjectUtil.callService(tmpListeProfile,idracine);
			}
		}

		public function getListeProfils(resulthandler:Function,faulthandler:Function,idracine:int):void{
			var tmpListeProfile:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getListeProfils",
				resulthandler,faulthandler);
			if(idracine==-1){
				RemoteObjectUtil.callService(tmpListeProfile);
			}else{
				RemoteObjectUtil.callService(tmpListeProfile,idracine);
			}
		}

		public function addProfil(resulthandler:Function,faulthandler:Function,iduser:int,idprofil:int,idracine:int):void{
			var affecteOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","addprofil",
				resulthandler,faulthandler);
			if(idracine == -1){
				RemoteObjectUtil.callService(affecteOpe,iduser,idprofil);
			}else{
				RemoteObjectUtil.callService(affecteOpe,iduser,idprofil,idracine);
			}
		}

		public function creerProfil(resulthandler:Function,faulthandler:Function,profil:String,s:String,idracine:int):void{
			var creerOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","creerProfil",
				resulthandler,faulthandler);
			if(idracine == -1){
				RemoteObjectUtil.callService(creerOpe,profil,s);
			}else{
				RemoteObjectUtil.callService(creerOpe,profil,s,idracine);
			}
		}

		public function supprimerProfil(resulthandler:Function,faulthandler:Function,idprofil:int):void{
			var supprimerOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","supprimerProfil",
						resulthandler,faulthandler);
			RemoteObjectUtil.callService(supprimerOpe,idprofil);
		}
		
		public function majProfil(resulthandler:Function,faulthandler:Function,idprofil:int,profil:String,str:String,idracine:int):void{
			var majOpe:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","majProfil",
				resulthandler,faulthandler);
			if(idracine == -1){
				RemoteObjectUtil.callService(majOpe,idprofil,profil,str);
			}else{
				RemoteObjectUtil.callService(majOpe,idprofil,profil,str,idracine);
			}			
		}
		
		public function okProfil(resulthandler:Function,faulthandler:Function,iduser:int,idracine:int):void{
			var tmpProfil:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","okProfil",
				resulthandler,faulthandler);
			if(idracine == -1){
				RemoteObjectUtil.callService(tmpProfil,iduser);
			}else{
				RemoteObjectUtil.callService(tmpProfil,iduser,idracine);
			}
		}

		public function getProfil(resulthandler:Function,faulthandler:Function,idprofil:int):void{
			var tmpProfil:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","getProfil",
						resulthandler,faulthandler);
			RemoteObjectUtil.callService(tmpProfil,idprofil);
		}
	
		public function removeProfil(resulthandler:Function,faulthandler:Function,iduser:int,idprofil:int):void{
			var tmpRemoveProfil:AbstractOperation= RemoteObjectUtil.getOperationFrom(
					RemoteObjectUtil.DEFAULT_DESTINATION,
					"fr.consotel.consoprod.gestiondesdroits.gestiondesdroits","removeProfil",
					resulthandler,faulthandler);
			RemoteObjectUtil.callService(tmpRemoveProfil,iduser,idprofil);
		}
	
	}
}