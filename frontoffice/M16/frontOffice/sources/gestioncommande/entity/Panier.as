package gestioncommande.entity
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import gestioncommande.services.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class Panier extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		private var _NOMBRE_CONFIGURATION:		int = 0;
		private var _MONTANT_COMMANDE_ENCOURS:	String = "0";
		private var _MONTANT_TOTAL_COMMANDE:	String = "0";
	
	//--------------------------------------------------------------------------------------------//
	//					PROPRIETEES
	//--------------------------------------------------------------------------------------------//
		
		public function get NOMBRE_CONFIGURATION():int
		{
			return _NOMBRE_CONFIGURATION;
		}
			
		public function set NOMBRE_CONFIGURATION(values:int):void
		{
			_NOMBRE_CONFIGURATION = values;
		}
		
		public function get MONTANT_COMMANDE_ENCOURS():String
		{
			return _MONTANT_COMMANDE_ENCOURS;
		}
			
		public function set MONTANT_COMMANDE_ENCOURS(values:String):void
		{
			_MONTANT_COMMANDE_ENCOURS = values;
		}
		
		public function get MONTANT_TOTAL_COMMANDE():String
		{
			return _MONTANT_TOTAL_COMMANDE;
		}
			
		public function set MONTANT_TOTAL_COMMANDE(values:String):void
		{
			_MONTANT_TOTAL_COMMANDE = values;
		}

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function Panier()
		{
			NOMBRE_CONFIGURATION 		= 0;
			MONTANT_COMMANDE_ENCOURS 	= Formator.formatTotalWithSymbole(0);
			MONTANT_TOTAL_COMMANDE 		= Formator.formatTotalWithSymbole(0);
		}

		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		public function attributMontant():void
		{
			var qte			:Number = SessionUserObject.singletonSession.COMMANDEARTICLE.length;			
			var prixCurrent	:Number = calculCurrent();
			var prixTotal	:Number = calculTotal();
			
			NOMBRE_CONFIGURATION 		= qte;
			MONTANT_COMMANDE_ENCOURS 	= Formator.formatTotalWithSymbole(prixCurrent);
			MONTANT_TOTAL_COMMANDE 		= Formator.formatTotalWithSymbole(prixTotal);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATE
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION
		//--------------------------------------------------------------------------------------------//
		
		private function calculCurrent():Number
		{
			var lenTer	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX.length;
			var lenAcc	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES.length;
			var nbr		:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.CONFIGURATIONS.length;
			var qte		:int = 1;
			var i		:int = 0;
			var prix	:Number = 0;
			
			if(nbr != 0)
				qte = nbr;
			
			for(i = 0; i < lenTer;i++)
			{
				prix = prix + SessionUserObject.singletonSession.CURRENTCONFIGURATION.TERMINAUX[i].PRIX;
			}
			
			for(i = 0; i < lenAcc;i++)
			{
				prix = prix + SessionUserObject.singletonSession.CURRENTCONFIGURATION.ACCESSOIRES[i].PRIX;
			}
			
			prix = qte * prix;
			
			return prix;
		}
		
		private function calculTotal():Number
		{
			var articles	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var lenArt		:int = articles.length;
			var total		:Number = 0;
			
			for(var i:int = 0; i < lenArt;i++)
			{
				var lenTer	:int = articles[i].TERMINAUX.length;
				var lenAcc	:int = articles[i].ACCESSOIRES.length;
				var qte		:int = articles[i].CONFIGURATIONS.length;
				var j		:int = 0;
				var prix	:Number = 0;
				
				if(qte == 0)
					qte = 1;
				
				for(j = 0; j < lenTer;j++)
				{
					prix = prix + articles[i].TERMINAUX[j].PRIX;
				}
				
				for(j = 0; j < lenAcc;j++)
				{
					prix = prix + articles[i].ACCESSOIRES[j].PRIX;
				}
				
				prix = qte * prix;
				total = total + prix;
			}
			
			return total;
		}

	}
}
