package composants.tb.recherche
{
	import flash.events.Event;

	public class ResultatRechercheEvent extends Event
	{
		private var _id : String;
			
		private var _ids : Array;
		
		private var _perimetreRecherche : String;	
		
		private var _libelle : String;
		
		private var _windowHandler : ResultatRecherche_IHM;
				
		public function ResultatRechercheEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{			
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		public function set ID(s : String):void{
			_id = s;
		} 
		
		public function set perimetreRecherche(pr : String):void{
			_perimetreRecherche = pr;
		}
		
		public function set IDs(ids : Array):void{
			_ids = ids;
		}
		
		public function set libelle(l : String):void{
			_libelle = l;
		}
		
		public function set  windowHandler(w : ResultatRecherche_IHM):void{
			 _windowHandler = w;
		}
		
		
		public function get  windowHandler():ResultatRecherche_IHM{
			return _windowHandler;
		}
					
		public function get ID():String{
			return _id;
		}
		
		public function get IDs():Array{
			return _ids;
		}
		
		public function get perimetreRecherche():String{
			return _perimetreRecherche;
		}
		
		public function get libelle():String{
			return _libelle;
		}
		
	}
}