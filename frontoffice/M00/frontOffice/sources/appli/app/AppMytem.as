package appli.app
{
	import appli.control.AppControlEvent;
	import appli.impersonnification.refreshManager;
	import appli.mytem.CreateMytemIHM;
	
	import entity.AppParams;
	
	import general.abstract.AbstractApplication;
	import general.appliComposants.UtilM00;
	import general.entity.Constantes;
	import general.event.UtilM00Event;
	import general.helper.QueryString;

	
	import interfaceClass.IApplication;
	
	import rapport.vo.ApiReportingSingleton;

	public class AppMytem extends AbstractApplication implements IApplication
	{
		public function AppMytem()
		{
			var api:ApiReportingSingleton = new ApiReportingSingleton();

			super();
		}

		override protected function defineDisplay():void
		{
			this.compIHM=new CreateMytemIHM();

			super.defineDisplay();
			refreshManager.setChosenAppMytem(this);
		}
		
		/**
		 *	<code>deconnect</code>
		 * 
		 * 	Gère la déconnection de l'appli.
		 * 
		 * 	Dans le cas où la première connection à l'application est en SSO, on spécifie que dorénavant la connection sera de manière classique ( écran de login etc ...) 
		 * 
		 */
		override public function deconnect(evt:AppControlEvent=null):void
		{
			if(params.codeConnection == Constantes.CODECONNECTION_SSO_SESOUVENIR)
			{
				params.codeConnection = Constantes.CODECONNECTION_CLASSIQUE;
				super.deconnect(evt);
				init(params);
			}
			else
			{
				super.deconnect(evt);
				afficherEcranLogin();
			}
		}

		/**
		 *	définit les paramètres de l'application
		 *
		 * 	Si le paramètre "codeConnection" ou "cc" est présent dans l'URL, alors le codeConnection d'AppParam est ce paramètre.
		 * 	Sinon il est égal à Constantes.CODECONNECTION_CLASSIQUE
		 *
		 */
		override public function init(param:AppParams):void
		{
			this.params=param;

			// DEFINITION DU CODE CONNECTION
			if (param.codeConnection == 0)
			{
				var listeParamCC:Array=["codeConnection", "cc", "codeconnection"];
				var codeConnec:Object=UtilM00.getInstance().recupUrlParam(listeParamCC);
				var token:Object = UtilM00.getInstance().recupUrlParam(["token"]);
				var qs:QueryString = new QueryString();
				
				if (qs.parameters.cc != null && qs.parameters.token != null)
				{
					param.codeConnection = Number(qs.parameters.cc) as int ;
					trace(param.codeConnection);
					param.token = qs.parameters.token as String;
					trace(param.token);
					super.init(param);
						
				}else
				if (codeConnec != null && codeConnec != "")
				{
					param.codeConnection=codeConnec as int;
					trace(codeConnec);


					if( token != null && token != "")
					{
						param.token = token as String;
						trace(token);
					}

					super.init(param);
				}
				else
				{
					var listeParamSSO:Array=["CLESSO1", "CléSSO1"];
					UtilM00.getInstance().addEventListener(UtilM00Event.FINDCOOKIE_EVENT, init2);
					UtilM00.getInstance().findCookieParam(listeParamSSO);
				}
			}
			else
			{
				super.init(this.params);
				afficherEcranLogin();
			}
		}

		private function init2(evt:UtilM00Event):void
		{
			var codeSSO:Object=UtilM00.getInstance().valueCookie;
			if (codeSSO != null && codeSSO is String && codeSSO.toString().length > 0)
			{
				this.params.codeConnection=Constantes.CODECONNECTION_SSO_SESOUVENIR;
				this.params.token=codeSSO.toString();
			}
			else
				this.params.codeConnection=Constantes.CODECONNECTION_CLASSIQUE;
			super.init(this.params);
		}
	
	}
}
