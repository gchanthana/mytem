package univers.inventaire.lignesetservices.vo
{
	[Bindable]
	import mx.collections.ArrayCollection;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	
	public class OngletOperateurVo implements ItoXMLParams
	{
		protected var _xmlParams : XML;
		
		public var IDSOUS_TETE:Number = 0;
		public var OPERATEUR_CREATEUR_NUMERO:String;
		public var OPERATEUR_ACCES:String;
		public var COMPTE_FACTURATION:String;
		public var SOUS_COMPTE_FACTURATION:String;
		public var IDAGENCE_OPERATEUR:String;
		public var PRESTATAIRE:String;
		public var COMMENTAIRE_OPERATEUR:TextArea;
		
		
		public var LISTE_OPERATEURS:ArrayCollection;


		//champs perso
		public var V9:String;
		public var V10:String;
		
		public function OngletOperateurVo(){
			
		}
		 	
		
		public function toXMLParams(registerInLog : int=0):XML{
			_xmlParams = XmlParamUtil.createXmlParamObject(registerInLog);
			
			XmlParamUtil.addParam(_xmlParam,"IDSOUS_TETE","l'identifiant de la ligne",IDSOUS_TETE);
		  	XmlParamUtil.addParam(_xmlParam,"OPERATEUR_CREATEUR_NUMERO","L'opérateur qui a créé le numéro de ligne",OPERATEUR_CREATEUR_NUMERO);
		 	XmlParamUtil.addParam(_xmlParam,"OPERATEUR_ACCES","L'opérateur qui",OPERATEUR_ACCES);
		 	XmlParamUtil.addParam(_xmlParam,"COMPTE_FACTURATION","le numéro du compte de facturation",COMPTE_FACTURATION);
		 	XmlParamUtil.addParam(_xmlParam,"SOUS_COMPTE_FACTURATION","le numéro du sous compte de facturation",SOUS_COMPTE_FACTURATION);
		 	XmlParamUtil.addParam(_xmlParam,"IDAGENCE_OPERATEUR","l'identifiant de l'agence opérateur",IDAGENCE_OPERATEUR);
		 	XmlParamUtil.addParam(_xmlParam,"PRESTATAIRE","Le prestataire pour l'opérateur",PRESTATAIRE);
		 	XmlParamUtil.addParam(_xmlParam,"COMMENTAIRE_OPERATEUR","Commentaires sur l'opérateur",COMMENTAIRE_OPERATEUR);
		 	XmlParamUtil.addParam(_xmlParam,"V9","Valeurs du champ perso 9",V9);
		 	XmlParamUtil.addParam(_xmlParam,"V10","Valeurs du champ perso 10",V10);		 	
		 	return _xmlParams;
		}		
	}
}