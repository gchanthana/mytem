package gestionparc.event
{
	import flash.events.Event;
	
	import mx.events.CloseEvent;

	public class ConfirmWithPassWordIHMEvent extends CloseEvent
	{
		public static const CWPWIE_CONFIRM_EVENT:String='cwpwieConfirmEvent';
		public static const CWPWIE_CANCEL_EVENT:String='cwpwieCancelEvent';

		public static const PASS_OK:int=1;
		public static const PASS_KO:int=-1;


		public function ConfirmWithPassWordIHMEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, detail:Number=-1)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new ConfirmWithPassWordIHMEvent(type, bubbles, cancelable, detail)
		}
	}
}
