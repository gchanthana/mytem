<cfcomponent output="false" extends="fr.saaswedo.api.datalert.Core">
	
	<cffunction access="private" name="URL_CUMULATIVE_USAGE" returntype="String">
		<cfset url_cum="#url_server#/api/da/cu.cfm">
		<cfreturn  url_cum>
	</cffunction>
	
	<cffunction name="init" access="private" output="true" returntype="void">
	
		<cfargument name="httpRequestData" type="String" required="true">
		
		<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
		<cfset jsonRequest=deserializejson(httpRequestData)>
		<cfset serverName=URL_CUMULATIVE_USAGE()>

		<cfif structKeyExists(#jsonRequest#,"devId") and jsonRequest.devId neq "">
			<cfset  devId = #jsonRequest.devId# >
			<cfset cause =" cause :  devId = #devId# non trouvé dans la base SWD">
		</cfif>
		
		<cfset  cumRoamingUpUsage   = 0>
		<cfset  cumRoamingDownUsage = 0>
		<cfset  cumWifiUpUsage 		= 0>
		<cfset  cumWifiDownUsage 	= 0>
		<cfset  cumCellUpUsage 		= 0>
		<cfset  cumCellDownUsage 	= 0>
		
		<cfif structKeyExists(#jsonRequest#,"CRUU") and jsonRequest.CRUU neq "">
			<cfset cumRoamingUpUsage = jsonRequest.CRUU>
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"CRUD") and jsonRequest.CRUD neq "">
			<cfset cumRoamingDownUsage = jsonRequest.CRUD>
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"CWUU") and jsonRequest.CWUU neq "">
			<cfset cumWifiUpUsage = jsonRequest.CWUU> 
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"CWUD") and jsonRequest.CWUD neq "">
			<cfset cumWifiDownUsage = jsonRequest.CWUD> 
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"CDUU") and jsonRequest.CDUU neq "">
			<cfset cumCellUpUsage = jsonRequest.CDUU> 
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"CDUD") and jsonRequest.CDUD neq "">
			<cfset cumCellDownUsage = jsonRequest.CDUD>
		</cfif>
		
		<cfif structKeyExists(#jsonRequest#,"UTS") and jsonRequest.UTS neq "">
			<cfset uts = jsonRequest.UTS>
		</cfif>
		
		<cfset data={}>
		
		<cfif cumRoamingUpUsage neq 0>
			<cfset data.CRUU=cumRoamingUpUsage >
		</cfif>
		
		<cfif cumRoamingDownUsage neq 0>
			<cfset data.CRUD=cumRoamingDownUsage >
		</cfif>
		
		<cfif cumWifiUpUsage neq 0>
			<cfset data.CWUU=cumWifiUpUsage >
		</cfif>
		
		<cfif cumWifiDownUsage neq 0>
			<cfset data.CWUD=cumWifiDownUsage >
		</cfif>
		
		<cfif cumCellUpUsage neq 0>
			<cfset data.CDUU=cumCellUpUsage >
		</cfif>
		
		<cfif cumCellDownUsage neq 0>
			<cfset data.CDUD=cumCellDownUsage >
		</cfif>
		
		<cfset data.UTS=uts >
		
		<cflog  text="initialisation variables pour add_real_time_usage = OK " type="information">
		
	</cffunction>
	
	<cffunction name="add_real_time_usage" access="public" returntype="any" output="true">
			
		<cfargument name="httpRequestData" type="String" required="true">
		<cfargument name="user_agent" type="String" required="true">
		<cfargument name="ip_origin" type="String" required="false">

		<cfset uuid =createuuid() >
		
		<cftry>
			<cfset init(httpRequestData)>
			<!--- logger les données reçues --->
			<cfset saveLog(uuid,"#Debut# add_real_time_usage","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin#",idMntMobileProxy) >
			
			<cfset devInfo=bdd.getStoredInfo(devId)>	

			<cfif devInfo.devid neq "">
	
				<cfhttp method="post" url="#serverName#" useragent="#user_agent#">
					<cfhttpparam type="header" name="Authorization" value="#authorization#"><!--- Authentication basique obligatoire sinon erreur  --->
					<cfhttpparam type="header" name="Accept" value="application/json">
					<cfhttpparam type="header" name="UUID" value="#devInfo.PhoneNum#"> <!--- obligatoire sinon erreur ({"Message":"UUID not present in the HTTP header."} ou  {"Message":"UUID : bad format"} ) --->
		 			<cfhttpparam type="formfield" name="data" value="#serializejson(data)#"> 	
				 </cfhttp>
				 
				 <!---  logger les données envoyées à l'API SWD  --->	
				 <cfset saveLog(uuid,"#invoke# add_real_time_usage","#lab_sentData# #serializejson(data)# #newLine# avec UUID = #devInfo.PhoneNum# comme Header http #newLine# #link# #serverName#",idMntEventProxyAPI) >
				 
				 <cfif cfhttp.Responseheader.Status_Code eq "200">
					<cflog type="information" text=" reponse http ok - methode : add_real_time_usage, on renvoie resp:ok ">
				 	
				 	<!--- logger la réponse de l'API SWD  --->
					<cfset saveLog(uuid,"#responseSWD# add_real_time_usage","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName#",idMntEventAPIProxy) >
				 	
				 	<cfset responeJson = '{"resp":"Ok"}' >
					 <!---  logger la réponse envoyée au mobile --->
					<cfset saveLog(uuid,"#responseProxy# add_real_time_usage","#respHttp# #responeJson#",idMntEventAPIProxy) >
					<cfreturn responeJson>
					
				 <cfelse><!--- logger le message de l'API SWD en cas d'erreur, par exemple :  {"Message":"phone_no is not present"}  et le meme message est renvoyé au mobile ---> 
					 <cflog type="information" text="reponse http not ok - methode :  add_real_time_usage">
					 <cfset saveLog(uuid,"#responseSWD# add_real_time_usage","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName# #newLine# #info#",idMntEventAPIProxy) >
					 <cfset responeJson = cfhttp.Filecontent >
					 <cfreturn responeJson>
				 </cfif>
			<cfelse>
				<cflog type="information" text="device non trouve dans la base SWD - methode : add_real_time_usage ">
				<cfheader name="status" value="421">
				<cfset responeJson = '{"resp":"Device is not registered"}' >
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# add_real_time_usage","#respHttp# #responeJson# #newLine# #cause#",idMntEventProxyMobile) >
				<cfreturn responeJson>	
			</cfif>
		<cfcatch type="any">
			<cfthread action="sleep" duration="1000"/>
			<cfset saveLog(uuid,"#exce# add_real_time_usage",bodyException(CFCATCH),idMntEventProxyException) >
			<cfset responeJson = '{"resp":"#cfcatch.Message#" }' >
			<cfreturn responeJson>
		</cfcatch>
		</cftry>
					
	</cffunction>

</cfcomponent>