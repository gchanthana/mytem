<!--- Ce composant implémente IErrorHandler pour traiter tous les codes d'erreurs dispatchés par l'API Remoting qu'il utilise --->
<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.AbstractService"
extends="fr.saaswedo.api.patterns.error.ErrorTarget" implements="fr.saaswedo.api.patterns.error.IErrorHandler"
hint="Implémentation de base pour représenter les services d'un serveur MDM qui est représenté par l'implémentation IMDM qui est fournie à init()
Chaque service utilise les mêmes credentiels et leur destinations (e.g URL) seront toutes construites à partir du meme endpoint de base.
Une seule implémentation IRemoteClient est utilisée pour enregistrer chaque service avec la fonction register() et pour les accéder par la suite.
Une instance de ce composant ne référence que les services d'un seul serveur MDM et utilise les mêmes crédentiels pour y accéder<br>
Une clé sourceTarget est parfois présente dans l'erreur dispatchée par ce composant pour signifier que celle ci a été redispatchée à partir d'une erreur
fournie à l'implémentation handleError() et que la valeur associée à cette clé est celle du paramètre errorTarget au moment du traitement de l'erreur">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.AbstractService">
		<cfreturn "fr.saaswedo.api.mdm.impl.AbstractService">
	</cffunction>

	<!--- fr.saaswedo.api.patterns.error.IErrorHandler --->
	<cffunction name="getId" returntype="String" hint="Retourne la meme valeur quelque soit l'instance sur laquelle cette fonction est appelée">
		<cfreturn "API_MDM_IERROR_HANDLER_ID">
	</cffunction>

	<!--- fr.saaswedo.api.patterns.error.IErrorHandler --->
	<cffunction access="public" name="handleError" returntype="void" hint="Traitement exécuté pour les cas suivants :<br>
	- Tous les codes d'erreurs dispatchés par l'API Remoting utilisée par ce composant<br>Cette fonction dispatche à nouveau l'erreur :
	<br>THIS sera la valeur du paramètre errorTarget pour IErrorHandler.handleError()
	<br>Une clé sourceTarget sera ajoutée dans cfCatchObject et correspondra à la valeur du paramètre errorTarget fournie à cette fonction
	<b>Ce traitement NE DOIT PAS etre utilisé pour les erreurs dispatchées par ce composant sinon il y un risque de boucle sans fin</b>">
		<cfargument name="errorTarget" type="fr.saaswedo.api.patterns.error.IErrorTarget" required="true">
		<cfargument name="cfCatchObject" type="Any" required="true">
		<cfargument name="errorInfos" type="Struct" required="false">
		<!--- TODO : Dispatch de l'erreur vers les instances IErrorHandler qui sont enregistrées auprès de ce composant
		<cfset var error=ARGUMENTS.cfCatchObject>
		<cfset var target=ARGUMENTS.errorTarget>
		<cfset dispatchError({
			errorCode=error.errorCode,message=error.message,detail=error.detail,tagContext=error.tagContext,sourceTarget=target
		})>
		--->
		<cfset var code=GLOBALS().CONST("MDM.ERROR")>
		<cfthrow type="Custom" errorCode="#code#" message="#componentName()#.handleError() is not supported">
	</cffunction>

	<cffunction access="public" name="getMdmServer" returntype="fr.saaswedo.api.mdm.core.services.IMDM" description="LOCK:ReadOnly" hint="Serveur MDM">
		<cftry>
			<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="ReadOnly" throwontimeout="true">
				<cfif structKeyExists(VARIABLES,"MDM_SERVER")>
					<cfreturn VARIABLES.MDM_SERVER>
				<cfelse>
					<cfset var code=GLOBALS().CONST("MDM.ERROR")>
					<cfthrow type="Custom" errorCode="#code#" message="MDM Server is not defined">
				</cfif>
			</cflock>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH})>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.mdm.impl.AbstractService" description="LOCK:Exclusive"
	hint="Les crédentiels seront utilisés pour tous les services du serveur MDM et leur URLs seront basées à partir du meme endpoint">
		<cfargument name="mdmServer" type="fr.saaswedo.api.mdm.core.services.IMDM" required="true" hint="Serveur MDM. Il sera retourné par getMdmServer()">
		<cftry>
			<!--- La fonction init() héritée de Component n'est pas verrouillée par un CFLOCK --->
			<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
				<!--- Constructeur hérité de Component --->
				<cfset SUPER.init()>
				<!--- Implémentation du serveur MDM (IMDM) : VARIABLES.MDM_SERVER --->
				<cfif NOT structKeyExists(VARIABLES,"MDM_SERVER")>
					<cfset VARIABLES.MDM_SERVER=ARGUMENTS.mdmServer>
				</cfif>
				<cfreturn THIS>
			</cflock>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH})>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- =========== Liste des statuts de devices : Fonctions ajoutées ici pour compatibilité avec ce composant =========== --->
	
	<cffunction access="public" name="MANAGED" returntype="String" hint="Retourne getMdmServer().getMdmServerInfo().MANAGED()">
		<cfreturn getMdmServer().getMdmServerInfo().MANAGED()>
	</cffunction>

	<cffunction access="public" name="PENDING" returntype="String" hint="Retourne getMdmServer().getMdmServerInfo().PENDING()">
		<cfreturn getMdmServer().getMdmServerInfo().PENDING()>
	</cffunction>
	
	<cffunction access="public" name="UNMANAGED" returntype="String" hint="Retourne getMdmServer().getMdmServerInfo().UNMANAGED()">
		<cfreturn getMdmServer().getMdmServerInfo().UNMANAGED()>
	</cffunction>
	
	<!--- =========== Liste des systèmes d'exploitation : Fonctions ajoutées ici pour compatibilité avec ce composant =========== --->
	
	<cffunction access="public" name="ANDROID" returntype="String" hint="Retourne getMdmServer().getMdmServerInfo().ANDROID()">
		<cfreturn getMdmServer().getMdmServerInfo().ANDROID()>
	</cffunction>
	
	<cffunction access="public" name="IOS" returntype="String" hint="Retourne getMdmServer().getMdmServerInfo().IOS()">
		<cfreturn getMdmServer().getMdmServerInfo().IOS()>
	</cffunction>
	
	<cffunction access="public" name="UNKNOWN_OS" returntype="String" hint="Retourne getMdmServer().getMdmServerInfo().UNKNOWN_OS()">
		<cfreturn getMdmServer().getMdmServerInfo().UNKNOWN_OS()>
	</cffunction>
	
	<!--- Stockage des logs --->
	<cffunction access="private" name="storeWarning" returntype="void"
	hint="Stocke le log d'erreur. La valeur de log.LOG_TYPE est remplacée par celle de logType">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
		<cfset logInfos.LOG_TYPE=logStorage.WARNING_LOG()>
		<cfset storeLog(logStorage,logInfos)>
	</cffunction>
	
	<cffunction access="private" name="storeError" returntype="void"
	hint="Stocke le log d'erreur. La valeur de log.LOG_TYPE est remplacée par la constante ERROR_LOG() de la librairie de stockage des logs">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<cfset var logInfos=ARGUMENTS.log>
		<cfset var logStorage=new fr.saaswedo.utils.log.MDM()>
		<cfset logInfos.LOG_TYPE=logStorage.ERROR_LOG()>
		<cfset storeLog(logStorage,logInfos)>
	</cffunction>
	
	<cffunction access="private" name="storeLog" returntype="void" hint="Stocke le log avec la librairie de stockage de log de l'API MDM.
	Ajoute dans le log la clé IDGROUP associée à la valeur de l'identifiant du groupe dans la session utilisateur s'il est défini.
	Ajoute dans le log la clé IDMNT_ID associée à la valeur correspondante commune à toutes les fonctionnalités de l'API MDM">
		<cfargument name="logger" type="fr.saaswedo.utils.log.MDM" required="true" hint="Librairie de stockage du log">
		<cfargument name="log" type="Struct" required="true" hint="Log à stocker">
		<!--- Stockage et log de l'erreur: Cette implémentation remplace les valeurs de IMNT_CAT,SUBJECTID,BODY
		Toute exception durant le stockage du log est notifiée par mail par la librairie de stockage des logs
		--->
		<cfset var logStorage=ARGUMENTS.logger>
		<cfset var logInfos=ARGUMENTS.log>
		<cfset logInfos.IDMNT_ID=logStorage.DRT_002()>
		<cfset logInfos.BODY_FORMAT=logStorage.HTML_BODY()>
		<!--- TODO: Dans l'attente du refactoring de l'API MDM, la racine est fournie ici --->
		<cfif isDefined("SESSION") AND structKeyExists(SESSION,"PERIMETRE") AND structKeyExists(SESSION.PERIMETRE,"ID_GROUPE")>
			<cfset logInfos.IDGROUP=VAL(SESSION.PERIMETRE.ID_GROUPE)>
		<cfelse>
			<cfset logInfos.IDGROUP=-1>
		</cfif>
		<cfset logStorage.logIt(logInfos)>
	</cffunction>
</cfcomponent>