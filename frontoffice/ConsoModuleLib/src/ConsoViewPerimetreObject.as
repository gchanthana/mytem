package {
	import flash.sampler.DeleteObjectSample;
	
	public class ConsoViewPerimetreObject implements IConsoViewPerimetreObject {
		public static const TYPE_ROOT:String = "ROOT";
		public static const TYPE_OPE:String = "OPE";
		public static const TYPE_GEO:String = "GEO";
		public static const TYPE_CUS:String = "CUS";
		public static const TYPE_SAV:String = "SAV";
		
		public static const TYPE_GROUPE:String = "Groupe";
		public static const TYPE_GROUPELIGNE:String = "GroupeLigne";
		public static const TYPE_GROUPEANA:String = "GroupeAna";

//		private var groupIndex:int;
//		private var _TYPE_LOGIN:int; // 1 = CV3 / 2 = CV4
		private var perimetreInfosData:Object;
		private var accessInfosData:XML;
		private var DEFAULT_XML:XML =new XML("<MENU KEY='ROOT'><UNIVERS	LBL='Accueil'KEY='HOME'SYS='1'	USR='1' 	type='radio' 	groupName='menuUniversGroup' 	TOGGLED='true'	toggled='true' 		ENABLED='true'></UNIVERS></MENU>");
		private var XML_PERIMETRE:XML;
		
		public function ConsoViewPerimetreObject() {
//			groupIndex = 0;
			perimetreInfosData = null;
			accessInfosData = null;
		}
		
//		internal function setGroupIndex(groupIdx:int):Boolean {
//			groupIndex = groupIdx;
//			trace("(ConsoViewPerimetreObject) Group Index Updated");
//			return (groupIndex == groupIdx);
//		}
//		internal function setTypeLogin(typelogin:int):Boolean {
//			_TYPE_LOGIN = typelogin
//			trace("(ConsoViewPerimetreObject) Group Index Updated");
//			return (_TYPE_LOGIN == typelogin);
//		}
		public function get DROIT_FOURNIS():Boolean{
			if(perimetreInfosData != null && perimetreInfosData["PERIMETRE_DATA"] != null && perimetreInfosData["PERIMETRE_DATA"]["BUILD_STATUS"] > 0)
				return (perimetreInfosData["DROIT_GESTION_FOURNIS"]>0);
			else
				return false;
		}
		public function get BOOLINVENTAIRE():Boolean {
			return (perimetreInfosData["BOOLINVENTAIRE"] >= 1);
		}
		public function get CODE_STYLE():String {
			return perimetreInfosData["GROUP_CODE_STYLE"];
		}
		public function get RACINE_LIBELLE():String {
			return perimetreInfosData["GROUPE"];
		}
		public function get GROUPE_INDEX():int {
			return perimetreInfosData["ID_GROUPE"];
		}
		public function get PERIMETRE_INDEX():int {
			return perimetreInfosData["ID_PERIMETRE"];
		}
		public function get TYPE_LOGIQUE():String {
			return perimetreInfosData["TYPE_LOGIQUE"];
		}
		public function get PERIMETRE_LIBELLE():String {
			return perimetreInfosData["RAISON_SOCIALE"];
		}
		public function get TYPE_PERIMETRE():String {
			return perimetreInfosData["TYPE_PERIMETRE"];
		}
		public function get OPERATEURID():int {
			return perimetreInfosData["OPERATEURID"];
		}
		public function get nodeList():XML {
			return XML_PERIMETRE;
		}
		public function get accessListData():XML 
		{
			return accessInfosData;
		}
		public function get dateFirstFacture():Date {
			return perimetreInfosData["STRUCTDATE"]["PERIODS"]["FACTURE_DEB"];
		}
		public function get dateLastFacture():Date {
			return perimetreInfosData["STRUCTDATE"]["PERIODS"]["FACTURE_FIN"];
		}
		public function get displayDateDeb():Date {
			return perimetreInfosData["STRUCTDATE"]["PERIODS"]["DISPLAY_DEB"];
		}
		public function get displayDateFin():Date {
			return perimetreInfosData["STRUCTDATE"]["PERIODS"]["DISPLAY_FIN"];
		}
		public function get maxMonthToDisplay():Number {
			return perimetreInfosData["STRUCTDATE"]["PERIODS"]["MAX_MONTH_TO_DISPLAY"];
		}
//		[Deprecated] public function get GROUPE_IDX():int {
//			return 0;
//		}
//		[Deprecated] public function get IDCLIENTPV():int {
//			return CvAccessManager.getSession().GROUP_LIST.get 
//				[groupIndex]["IDCLIENTS_PV"];
//		}
		
	// FONCTION DE MISE A JOUR
		internal function updatePerimetreInfos(perimetreInfos:Object):Boolean {
			perimetreInfosData = null;
			
//			if(!perimetreInfos.hasOwnProperty("XML_PERIMETRE") 
//				&& perimetreInfosData.hasOwnProperty("XML_PERIMETRE"))
//				perimetreInfos.XML_PERIMETRE = perimetreInfosData.XML_PERIMETRE
			
			perimetreInfosData = perimetreInfos;
			trace("(ConsoViewPerimetreObject) Perimetre Infos Updated");
			return true;
		}
//		internal function updatePerimetreFromNode(perimetreNodeInfos:Object):Boolean {
//			perimetreInfosData = null;
//			perimetreInfosData = perimetreNodeInfos;
//			trace("(ConsoViewPerimetreObject) Perimetre Node Infos Updated");
//			return true;
//		}
		internal function updatePerimetreXML(perimetreInfos:XML):Boolean {
			try
			{
				XML_PERIMETRE = perimetreInfos;
				return true;
			}
			catch(err:Error)
			{
				return false;
			}
			return true;
		}
		private function buildDefault():Boolean
		{
			try
			{
				accessInfosData = new XML(DEFAULT_XML);
			}
			catch(err:Error)
			{
				return false;
			}
			return true;
		}
		internal function updateAccessInfos(accessInfos:XML):Boolean 
		{
			try
			{
				// si le menu n'est pas null, on traite 
				if(accessInfos != null)
				{
					accessInfosData = accessInfos;
				}
				// sinon on affiche le menu par défaut : page d'accueil
				else
				{
					buildDefault();
				}
			}
			catch(err:Error)
			{
				return false;
			}
			return true;
		}
		/**
		 * Eventuellement mettre aussi à jour les périodes (DISPLAY_DATE_DEB, DISPLAY_DATE_FIN) dans la session BackOffice
		 * */
		internal function setDisplayDateDeb(datedeb:Date):void 
		{
			perimetreInfosData["PERIODS"]["DISPLAY_DEB"] = datedeb;
			perimetreInfosData["PERIODS"]["DISPLAY_DEB"].toString();
		}
		internal function setDisplayDateFin(datefin:Date):void {
			perimetreInfosData["PERIODS"]["DISPLAY_FIN"] = datefin;
			perimetreInfosData["PERIODS"]["DISPLAY_FIN"].toString();
		}
		/**
		 * Eventuellement mettre aussi à jour le nb de mois maximum à afficher (MAX_MONTH_TO_DISPLAY) dans la session BackOffice
		 * */
		internal function setMaxMonthToDisplay(maxMonth:Number):void {
			perimetreInfosData["PERIODS"]["MAX_MONTH_TO_DISPLAY"] = maxMonth;
			perimetreInfosData["PERIODS"]["MAX_MONTH_TO_DISPLAY"].toString();
		}
		
		
	// FONCTION PUBLIC	
		internal function clearPerimetre():void {
//			groupIndex = -1;
			perimetreInfosData = null;
			accessInfosData = null;
		}
	}
}
