package univers.facturation.suivi.controles.vo
{
	//[RemoteClass(alias="fr.consotel.consoview.facturation.suiviFacturation.controles.Produit")]
	
	
	 
	[Bindable]
	public class Produit
	{

		public var id:Number = 0;
		public var idCli:Number = 0;
		public var idCat:Number = 0;
		public var libelle:String = "";
		public var segment:String = "";
		public var themeId:Number = 0;
		public var themeLibelle:String = "";
		public var operateurId:Number = 0;
		public var operateurLibelle:String = "";
		public var typeTheme:String = "";
		public var typeTrafic:String = "";
		public var typeTraficId:String = "";
		public var poids:Number = 0;
		public var pourControle:Boolean = false;
		public var idVersion:Number = 0;
		public var nbVersions:Number = 0;
		public var prixUnitaire:Number = 0;
		public var prixRemise:Number = 0;
		public var montant:Number = 0;
		public var remiseContrat:Number = 0;
		public var dateDebut:Date = null;
		public var dateFin:Date = null;


		public function Produit()
		{
		}

	}
}