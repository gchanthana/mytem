<cfcomponent displayname="WipeZP001" >

	<cffunction name="action">
	
		<cfargument name="param" type="struct" required="true">
			<!---
			<cfset var service = createObject("component","fr.consotel.api.mdm.partner.public.Zenprise").getPublicZenprise(#param.servermdmuser#,
																															#param.servermdmpwd#,
																															"https://" & #param.servermdm# & "/zdm/services/Version?WSDL")>	
			--->
			<cfset var service = createObject("component","fr.consotel.api.mdm.partner.public.Zenprise").getPublicZenprise()>
			<cfif param.eraseData EQ true>

				<cfset var variable = service.corporateDataWipeDevice(param.serialNumber, param.imei, param.waitTime)>
			
			<cfelse>

				<cfset var variable = service.wipeDevice(param.serialNumber, param.imei, param.erasedMemoryCard, param.waitTime)>
			
			</cfif>

	</cffunction>

</cfcomponent>
