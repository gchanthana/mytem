package univers.parametres.gestionnaire
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestionnaire.classemetier.Gestionnaire;
	import univers.parametres.gestionnaire.classemetier.PoolGestionnaire;
	
	public class ChoixProfile extends ChoixProfileIHM
	{
		private var gestionnaire : Gestionnaire;
		private var poolGestionnaire : PoolGestionnaire;
		//public static const ANNULER_EVENT:String = "POPUP_FERMER"; // Signifie Annulation d'un popUp
		public function ChoixProfile(poolGestionnaire : PoolGestionnaire,gestionnaire : Gestionnaire)
		{
			super();
			this.gestionnaire = gestionnaire;
			this.poolGestionnaire = poolGestionnaire;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer);
			
		}
		private function initIHM(evt : FlexEvent):void
		{
			//lab_gestionnaire.text = fiche_gestionnaire.dg_gestionnaire.selectedItem.LOGIN_PRENOM+" "+fiche_gestionnaire.dg_gestionnaire.selectedItem.LOGIN_NOM;
			//dg_gestionnaire
			
			init_profile();
			dg_profile.addEventListener(Event.CHANGE,dg_profile_handler);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			bt_creer_profile.addEventListener(MouseEvent.CLICK,creerProfile);
			bt_affecter.addEventListener(MouseEvent.CLICK,affecter);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			lab_titre.text = "Quel profil aura " +gestionnaire.nom_prenom+ " sur le pool : "+poolGestionnaire.libelle_pool;
			
		}
		private function bt_desaffecter_handeler(evt : MouseEvent):void
		{
			
		}
		private function affecter(evt : MouseEvent):void
		{
			//var accesCollab : int;
			//if (combo_collab.selectedIndex
			if(dg_profile.selectedIndex!=-1){
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"add_gestionnaire_in_pool",
																	process_affecter);
			RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,gestionnaire.id,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,combo_collab.selectedIndex,dg_profile.selectedItem.IDPROFIL_COMMANDE);
			}
			else
			{
				Alert.show("Selectionnez un profil");
			} 
			
		}
		private function process_affecter(evt : ResultEvent):void
		{
			//Alert.show("Ajouté");
			poolGestionnaire.init_dg_gestionnaire_of_pool();
			PopUpManager.removePopUp(this);
		}
		private function creerProfile(evt : MouseEvent):void
		{
			
			var panelCreerProfile : CreerProfileIHM = new CreerProfile();
			panelCreerProfile.addEventListener("CLOSE",init_profile); 
			PopUpManager.addPopUp(panelCreerProfile,this,true);
			PopUpManager.centerPopUp(panelCreerProfile);
			
			}
		
		private function dg_profile_handler(evt : Event):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"fournirListeActProfil",
																	process_fournirListeActProfil);
			RemoteObjectUtil.callService(op,dg_profile.selectedItem.IDPROFIL_COMMANDE);
		}
		private function process_fournirListeActProfil(re : ResultEvent):void
		{
			dg_action.dataProvider = re.result;
		}
		public function init_profile(evt : Event = null):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"fournirListeProfiles",
																	process_getProfile);
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);	
		}
		private function process_getProfile(re : ResultEvent):void
		{
			dg_profile.dataProvider = re.result;
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);	
		}
		
		
	}
	
}