package entity.GestionCollecte.vo
{
    import composants.util.DateFunction;
    import flash.utils.getDefinitionByName;
    import entity.GestionCollecte.enum.EBoolean;
    import entity.GestionCollecte.enum.Enumeratio;
    import utils.GestionCollecte.ConversionUtils;
    import mx.controls.Alert;
    import mx.messaging.errors.NoChannelAvailableError;

    [Bindable]
    public class CollecteTypeVO extends ValueObject
    {
        private var _anteriorite:EBoolean = EBoolean.FALSE;
        private var _anterioriteInfos:String = "";
        private var _contact:String = "";
        private var _contactInfos:String = "";
        private var _contraintes:EBoolean = EBoolean.FALSE;
        private var _contraintesInfos:String = "";
        private var _couts:Number;
        private var _coutsInfos:String = "";
        private var _idcollectType:Number;
        private var _idcollectMode:Number = 0;
        private var _libelleTemplate:String = "";
        private var _operateurId:Number = 0;
        private var _operateurNom:String;
        private var _prestaPerifacturation:String = "";
        private var _instructionsPath:String = "";
        // private var _prestaPerifactPjName:String = "";
        private var _souscriptionPjName:String = "";
        private var _recupMode:SimpleEntityVO;
        private var _modeSouscription:Number = 0;
        private var _modeSouscriptionPath:String = "";
        private var _modeSouscriptionUrl:String = "";
        private var _isPublished:EBoolean = EBoolean.FALSE;
        private var _delaiDispo:Number = 0;
        private var _idcollecteTemplate:Number = 0;
        private var _isCompleted:EBoolean = EBoolean.FALSE;
        private var _localisationRocf:String = "";
        private var _localisationRocfPath:String = "";
        private var _libelleRocf:String = "";
        private var _localisationRoic:String = "";
        private var _localisationRoicPath:String = "";
        // private var _roicLocalisationPjUrl:String = "";
        private var _libelleRoic:String = "";
        private var _recuperationRoic:String = "";
        private var _roicInfos:String = "";
        private var _uidcollecteTemplate:String = "";
        private var _iduserCreate:Number = 0;
        private var _dateCreate:Date = new Date();
        private var _iduserModif:Number = 0;
        private var _dateModif:Date = new Date();

        public function CollecteTypeVO()
        {
            super();
        }

        public override function deserialize(object:Object):IValueObject
        {
            var o:Object = object;
            o.hasOwnProperty("IDCOLLECT_TEMPLATE") ? this.idcollecteTemplate = new Number(o.IDCOLLECT_TEMPLATE) as Number : null;
            o.hasOwnProperty("UIDCOLLECT_TEMPLATE") ? this.uidcollecteTemplate = o.UIDCOLLECT_TEMPLATE : null;
            o.hasOwnProperty("OPERATEURID") ? this.operateurId = new Number(o.OPERATEURID) : null;
            o.hasOwnProperty("IDCOLLECT_TYPE") ? this.idcollectType = new Number(o.IDCOLLECT_TYPE) : null;
            o.hasOwnProperty("IDCOLLECT_MODE") ? this.idcollectMode = new Number(o.IDCOLLECT_MODE) : null;
            o.hasOwnProperty("OPNOM") ? this.operateurNom = o.OPNOM : null;
            o.hasOwnProperty("LIBELLE_TEMPLATE") ? this.libelleTemplate = o.LIBELLE_TEMPLATE : null;
            o.hasOwnProperty("IS_COMPLETED") ? this.isCompleted = EBoolean.getEntityFor(new Number(o.IS_COMPLETED)) : null;
            o.hasOwnProperty("IS_PUBLISHED") ? this.isPublished = EBoolean.getEntityFor(new Number(o.IS_PUBLISHED)) : null;
            o.hasOwnProperty("DELAI_DISPO") ? this.delaiDispo = new Number(o.DELAI_DISPO) : null;
            o.hasOwnProperty("ANTERIORITE") ? this.anteriorite = EBoolean.getEntityFor(new Number(o.ANTERIORITE)) : null;
            o.hasOwnProperty("ANTERIORITE_INFOS") ? this.anterioriteInfos = o.ANTERIORITE_INFOS as String : null;
            o.hasOwnProperty("CONTRAINTES") ? this.contraintes = EBoolean.getEntityFor(new Number(o.CONTRAINTES)) : null;
            o.hasOwnProperty("CONTRAINTES_INFOS") ? this.contraintesInfos = o.CONTRAINTES_INFOS as String : null;
            o.hasOwnProperty("COUTS") ? this.couts = new Number(o.COUTS) : null;
            o.hasOwnProperty("COUTS_INFOS") ? this.coutsInfos = o.COUTS_INFOS as String : null;
            o.hasOwnProperty("CONTACT") ? this.contact = o.CONTACT as String : null;
            o.hasOwnProperty("CONTACT_INFOS") ? this.contactInfos = o.CONTACT_INFOS as String : null;
            o.hasOwnProperty("PRESTA_PERIFACTURATION") ? this.prestaPerifacturation = o.PRESTA_PERIFACTURATION as String : null;
            o.hasOwnProperty("INSTRUCTIONS_PATH") ? this.instructionsPath = o.INSTRUCTIONS_PATH as String : null;
            o.hasOwnProperty("LIBELLE_ROIC") ? this.libelleRoic = o.LIBELLE_ROIC as String : null;
            o.hasOwnProperty("LOCALISATION_ROIC") ? this.localisationRoic = o.LOCALISATION_ROIC as String : null;
            o.hasOwnProperty("LOCALISATION_ROIC_PATH") ? this.localisationRoicPath = o.LOCALISATION_ROIC_PATH as String : null;
            o.hasOwnProperty("RECUPERATION_ROIC") ? this.recuperationRoic = o.RECUPERATION_ROIC as String : null;
            o.hasOwnProperty("ROIC_INFOS") ? this.roicInfos = o.ROIC_INFOS as String : null;
            o.hasOwnProperty("LIBELLE_ROCF") ? this.libelleRocf = o.LIBELLE_ROCF as String : null;
            o.hasOwnProperty("LOCALISATION_ROCF") ? this.localisationRocf = o.LOCALISATION_ROCF as String : null;
            o.hasOwnProperty("LOCALISATION_ROCF_PATH") ? this.localisationRocfPath = o.LOCALISATION_ROCF_PATH as String : null;
            o.hasOwnProperty("MODE_SOUSCRIPTION") ? this.modeSouscription = new Number(o.MODE_SOUSCRIPTION) : null;
            o.hasOwnProperty("MODE_SOUSCRIPTION_PATH") ? this.modeSouscriptionPath = o.MODE_SOUSCRIPTION_PATH as String : null;
            o.hasOwnProperty("MODE_SOUSCRIPTION_URL") ? this.modeSouscriptionUrl = o.MODE_SOUSCRIPTION_URL as String : null;
            o.hasOwnProperty("IDUSER_CREATE") ? this.iduserCreate = new Number(o.IDUSER_CREATE) : null;
            o.hasOwnProperty("DATE_CREATE") ? this.dateCreate = o.DATE_CREATE as Date : null;
            o.hasOwnProperty("IDUSER_MODIF") ? this.iduserModif = new Number(o.IDUSER_MODIF) : null;
            o.hasOwnProperty("DATE_MODIF") ? this.dateModif = o.DATE_MODIF as Date : null;
            return this;
        }

        public override function serialize(object:IValueObject):Object
        {
            return null;
        }

        public override function toString():String
        {
            return "CollecteTypeVO " /* + libelleTemplate + "\n" + "id :" + idcollecteTemplate.toString() + "\n" + "Publié :" + isPublished.label + "\n" +
                   "Complet :" + isCompleted.label + "\n" + "Opérateur:" + operateur.label + "\n" + "Données:" + dataType.label + "\n" + "Récupération :" +
                   recupMode.label + "\n" + "Délai :" + delaiDispo.toString() + " jours \n" + "Prestation :" + prestaPerifacturation + "\n" + "Prestation  path :" +
                   instructionsPath + "\n" + "Contraintes :" + contraintes.label + "\n" + "Contraintes info :" + contraintesInfos + "\n" + "Antériorité :" +
                   anteriorite.label + "\n" + "Antériorité info :" + anterioriteInfos + "\n" + "Cout :" + couts.label + "\n" + "Cout info :" + coutsInfos +
                   "\n" + "Contact :" + contact + "\n" + "Contact info :" + contactInfos + "\n" + "Cout :" + couts.label + "\n" + "Cout info :" + coutsInfos +
                   "\n" + "Roic :" + libelleRoic + "\n" + "Roic Recuperation :" + recuperationRoic + "\n" + "Roic Recuperation info :" + roicInfos + "\n" +
                   "Roic Localisation :" + localisationRoic + "\n" + "Roic Localisation PJ :" + localisationRoicPath + " \n" + "Rocf :" + libelleRocf + "\n" +
                   "Rocf Localisation :" + localisationRocf + "\n" + "Rocf Localisation PJ :" + localisationRocfPath + "\n" + "Créateur id :" + iduserCreate +
                   "\n" + "Date de création:" + dateCreate.toDateString() + "\n" + "LastUpate user ID :" + iduserModif + "\n" + "Date de la dernière update :" +
                 dateModif.toDateString() + "\n" + "UID :" + uidcollecteTemplate.toString() + "\n"*/;
        }

        public override function toXML():XML
        {
            var xml:XML = voToXml(this, "CollecteTypeVO");
            return xml;
        }

        public override function decode(object:Object):IValueObject
        {
            var vo:CollecteTypeVO = new CollecteTypeVO();
            /* vo.libelleTemplate = object.CollecteTypeVO.libelleTemplate;
               var op:OperateurVO = new OperateurVO();
               op.label = object.CollecteTypeVO.operateur.label;
               op.value = object.CollecteTypeVO.operateur.value;
               vo.operateur = op;
               //vo.dataType = Enumeratio.getItem(EDataType.getCollection(), object.CollecteTypeVO.dataType.value) as EDataType;
               vo.anteriorite = Enumeratio.getItem(EBoolean.getCollection(), object.CollecteTypeVO.anteriorite.value) as EBoolean;
               vo.anterioriteInfos = object.CollecteTypeVO.anterioriteInfos;
               vo.contraintes = Enumeratio.getItem(EBoolean.getCollection(), object.CollecteTypeVO.contraintes.value) as EBoolean;
               vo.contraintesInfos = object.CollecteTypeVO.contraintesInfos;
               //vo.couts = Enumeratio.getItem(ECout.getCollection(), object.CollecteTypeVO.couts.value) as ECout;
               vo.coutsInfos = object.CollecteTypeVO.coutsInfos;
               vo.idcollecteTemplate = object.CollecteTypeVO.idcollecteTemplate;
               vo.prestaPerifacturation = object.CollecteTypeVO.prestaPerifacturation;
               vo.instructionsPath = object.CollecteTypeVO.instructionsPath;
               vo.libelleRoic = object.CollecteTypeVO.libelleRoic;
               vo.recuperationRoic = object.CollecteTypeVO.recuperationRoic;
               vo.roicInfos = object.CollecteTypeVO.roicInfos;
               vo.idcollecteTemplate = object.CollecteTypeVO.idcollecteTemplate;
               vo.contact = object.CollecteTypeVO.contact;
               vo.contactInfos = object.CollecteTypeVO.contactInfos;
               vo.iduserCreate = object.CollecteTypeVO.iduserCreate;
               vo.iduserModif = object.CollecteTypeVO.iduserModif;
               vo.dateModif = getDateFromXmlString(object.CollecteTypeVO.dateModif);
               vo.dateCreate = getDateFromXmlString(object.CollecteTypeVO.dateCreate);
             vo.uidcollecteTemplate = object.CollecteTypeVO.uidcollecteTemplate;*/
            return vo;
        }

        public function get anteriorite():EBoolean
        {
            return _anteriorite;
        }

        public function set anteriorite(value:EBoolean):void
        {
            _anteriorite = value;
        }

        public function get anterioriteInfos():String
        {
            return _anterioriteInfos;
        }

        public function set anterioriteInfos(value:String):void
        {
            _anterioriteInfos = value;
        }

        public function get contact():String
        {
            return _contact;
        }

        public function set contact(value:String):void
        {
            _contact = value;
        }

        public function get contactInfos():String
        {
            return _contactInfos;
        }

        public function set contactInfos(value:String):void
        {
            _contactInfos = value;
        }

        public function get contraintes():EBoolean
        {
            return _contraintes;
        }

        public function set contraintes(value:EBoolean):void
        {
            _contraintes = value;
        }

        public function get contraintesInfos():String
        {
            return _contraintesInfos;
        }

        public function set contraintesInfos(value:String):void
        {
            _contraintesInfos = value;
        }

        public function get coutsInfos():String
        {
            return _coutsInfos;
        }

        public function set coutsInfos(value:String):void
        {
            _coutsInfos = value;
        }

        public function get libelleTemplate():String
        {
            return _libelleTemplate;
        }

        public function set libelleTemplate(value:String):void
        {
            _libelleTemplate = value;
        }

        public function get prestaPerifacturation():String
        {
            return _prestaPerifacturation;
        }

        public function set prestaPerifacturation(value:String):void
        {
            _prestaPerifacturation = value;
        }

        public function get instructionsPath():String
        {
            return _instructionsPath;
        }

        public function set instructionsPath(value:String):void
        {
            _instructionsPath = value;
        }

        public function get modeSouscriptionPath():String
        {
            return _modeSouscriptionPath;
        }

        public function set modeSouscriptionPath(value:String):void
        {
            _modeSouscriptionPath = value;
        }

        public function get isPublished():EBoolean
        {
            return _isPublished;
        }

        public function set isPublished(value:EBoolean):void
        {
            _isPublished = value;
        }

        public function get delaiDispo():Number
        {
            return _delaiDispo;
        }

        public function set delaiDispo(value:Number):void
        {
            _delaiDispo = value;
        }

        public function get idcollecteTemplate():Number
        {
            return _idcollecteTemplate;
        }

        public function set idcollecteTemplate(value:Number):void
        {
            _idcollecteTemplate = value;
        }

        public function get isCompleted():EBoolean
        {
            return _isCompleted;
        }

        public function set isCompleted(value:EBoolean):void
        {
            _isCompleted = value;
        }

        public function get localisationRocf():String
        {
            return _localisationRocf;
        }

        public function set localisationRocf(value:String):void
        {
            _localisationRocf = value;
        }

        public function get localisationRocfPath():String
        {
            return _localisationRocfPath;
        }

        public function set localisationRocfPath(value:String):void
        {
            _localisationRocfPath = value;
        }

        public function get libelleRocf():String
        {
            return _libelleRocf;
        }

        public function set libelleRocf(value:String):void
        {
            _libelleRocf = value;
        }

        public function get localisationRoic():String
        {
            return _localisationRoic;
        }

        public function set localisationRoic(value:String):void
        {
            _localisationRoic = value;
        }

        public function get localisationRoicPath():String
        {
            return _localisationRoicPath;
        }

        public function set localisationRoicPath(value:String):void
        {
            _localisationRoicPath = value;
        }

        public function get libelleRoic():String
        {
            return _libelleRoic;
        }

        public function set libelleRoic(value:String):void
        {
            _libelleRoic = value;
        }

        public function get recuperationRoic():String
        {
            return _recuperationRoic;
        }

        public function set recuperationRoic(value:String):void
        {
            _recuperationRoic = value;
        }

        public function get roicInfos():String
        {
            return _roicInfos;
        }

        public function set roicInfos(value:String):void
        {
            _roicInfos = value;
        }

        public function get iduserCreate():Number
        {
            return _iduserCreate;
        }

        public function set iduserCreate(value:Number):void
        {
            _iduserCreate = value;
        }

        public function get dateCreate():Date
        {
            return _dateCreate;
        }

        public function set dateCreate(value:Date):void
        {
            _dateCreate = value;
        }

        public function get iduserModif():Number
        {
            return _iduserModif;
        }

        public function set iduserModif(value:Number):void
        {
            _iduserModif = value;
        }

        public function get dateModif():Date
        {
            return _dateModif;
        }

        public function set dateModif(value:Date):void
        {
            _dateModif = value;
        }

        public function get uidcollecteTemplate():String
        {
            return _uidcollecteTemplate;
        }

        public function set uidcollecteTemplate(value:String):void
        {
            _uidcollecteTemplate = value;
        }

        public function get operateurId():Number
        {
            return _operateurId;
        }

        public function set operateurId(value:Number):void
        {
            _operateurId = value;
        }

        public function get operateurNom():String
        {
            return _operateurNom;
        }

        public function set operateurNom(value:String):void
        {
            _operateurNom = value;
        }

        public function get idcollectType():Number
        {
            return _idcollectType;
        }

        public function set idcollectType(value:Number):void
        {
            _idcollectType = value;
        }

        public function get idcollectMode():Number
        {
            return _idcollectMode;
        }

        public function set idcollectMode(value:Number):void
        {
            _idcollectMode = value;
        }

        public function get modeSouscription():Number
        {
            return _modeSouscription;
        }

        public function set modeSouscription(value:Number):void
        {
            _modeSouscription = value;
        }

        public function get couts():Number
        {
            return _couts;
        }

        public function set couts(value:Number):void
        {
            _couts = value;
        }

        public function get modeSouscriptionUrl():String
        {
            return _modeSouscriptionUrl;
        }

        public function set modeSouscriptionUrl(value:String):void
        {
            _modeSouscriptionUrl = value;
        }
    }
}