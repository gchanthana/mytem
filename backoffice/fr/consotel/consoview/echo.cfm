<cfdump var="#SESSION#">

<--- --------------------------------------------------------------------------------------- ----
	
	Blog Entry:
	Expired SESSION Available In ColdFusion CFThread Tag
	
	Author:
	Ben Nadel / Kinky Solutions
	
	Link:
	http://www.bennadel.com/index.cfm?event=blog.view&id=1081
	
	Date Posted:
	Dec 12, 2007 at 9:01 AM
	
---- --------------------------------------------------------------------------------------- --->


<!---
	Sleep the primary page request thread to make sure
	that the SESSION has expired by the time we launch our
	asynchronous thread.
--->
<cfthread
	action="sleep"
	duration="#(10 * 1000)#"
	/>
 
 
<!---
	Launch asynchronous thread. At this point, the user's
	SESSION will have already ended.
--->
<cfthread
	action="run"
	name="thread1">
 
	<!---
		Check to see if the session has timed out (this should
		always be the case, but just a precaution).
	--->
	<cfif StructKeyExists( SESSION, "DateEnded" )>
 
		<!--- Log session timeout time. --->
		<cfset THIS.LogValue(
			"Session Ended " &
			"(#TimeFormat( SESSION.DateEnded, 'hh:mm:ss' )#)"
			) />
 
	</cfif>
 <cfdump var="#SESSION#">
</cfthread>