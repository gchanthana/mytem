<cfcomponent  displayname="GetColonneVue">

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset idLang       = SESSION.USER.IDGLOBALIZATION>
	</cffunction>
	
	<cffunction access="remote" name="getAllColonnes" returntype="query">
			
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M111B.getUserColumns">
			
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idUser#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#">
			
			<cfprocresult name="result">
						
		</cfstoredproc>
		<cfreturn result >
	</cffunction>
	
</cfcomponent>