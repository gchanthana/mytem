<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.BaseClass" extends="fr.saaswedo.api.support.BaseClass" hint="Fonctionnalités de gestion des organisations">
	<cffunction access="public" name="show" returntype="Struct" hint="Retourne les infos concernant une organisation">
		<cfargument name="organizationId" type="Numeric" required="true" hint="Identifiant de l'organisation">
		<cfset var organizationApi=getOrganizations()>
		<cfset var requestResult=organizationApi.show(ARGUMENTS["organizationId"])>
		<cfif requestResult["STATUS"]["CODE"] EQ organizationApi.SUCCESS()>
			<cfreturn requestResult["RESULT"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="create" returntype="Struct" hint="Crée une organisation et retourne les infos le concernant">
		<cfargument name="customId" type="String" required="true" hint="Identifiant custom pour l'organisation à créer">
		<cfargument name="name" type="String" required="true" hint="Nom à donner à l'organisation">
		<cfargument name="shared_tickets" type="Boolean" required="true" hint="TRUE pour que tous les tickets soient visibles par tous les membres et FALSE sinon">
		<cfargument name="shared_comments" type="Boolean" required="true" hint="TRUE pour que tous les commentaires soient visibles par tous les membres et FALSE sinon">
		<cfargument name="tags" type="Array" required="true" hint="Tags pour l'organisation">
		<cfset var organizationApi=getOrganizations()>
		<cfset var orgaRequest={
			"organization"={
				"external_id"=ARGUMENTS["customId"],"name"=ARGUMENTS["name"],
				"shared_tickets"=ARGUMENTS["shared_tickets"],"shared_comments"=ARGUMENTS["shared_comments"],"tags"=ARGUMENTS["tags"]
			}
		}>
		<cfset var requestResult=organizationApi.create(orgaRequest)>
		<cfif requestResult["STATUS"]["CODE"] EQ organizationApi.SUCCESS()>
			<cfreturn requestResult["RESULT"]["organization"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
		</cfif>
	</cffunction>
</cfcomponent>