package catalogue.strategie.strategieconcrete
{
import catalogue.event.CatalogueEvent;
import catalogue.strategie.ICatalogue;
import catalogue.vo.ConstructeurVO;
import catalogue.vo.EquipementCatalogueVO;
import catalogue.vo.ParametreSearchCatalogueVO;
import catalogue.vo.RevendeurVO;

import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

import mx.collections.ArrayCollection;
import mx.resources.ResourceManager;
import mx.rpc.AbstractOperation;
import mx.rpc.events.ResultEvent;

import paginatedatagrid.pagination.vo.ItemIntervalleVO;
 
	[Bindable]
	public class AbonnementCS extends AbstractCatalogueCS implements ICatalogue
	{ 
		
		private var _listeAbonnement:ArrayCollection = new ArrayCollection;
		
		public function AbonnementCS()
		{
		}
		public function rechercheListeEquipement(parametresRechercheVO : ParametreSearchCatalogueVO, itemIntervalleVO : ItemIntervalleVO):void
		{
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"rechercheEquipementClient",
							rechercheEquipementConstructeur_handler,null);
			
			RemoteObjectUtil.callService(opData,
												parametresRechercheVO.SEARCH_TEXT,
												parametresRechercheVO.ORDER_BY,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,
												parametresRechercheVO.IDCONSTRUCTEUR,
												parametresRechercheVO.IDTYPEEQUIPEMENT,
												parametresRechercheVO.IDCLASSE,
												parametresRechercheVO.IDCLIENT
												);
		}
		
	 	public function addEquipement(equip : EquipementCatalogueVO):void
		{
			
			/* var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Constructeur",
							"addEquipementConstructeur",
							addEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData,equip.MODELE,equip.REFCONSTRUCTEUR,equip.IDCONSTRUCTEUR,equip.IDTYPE); */
		} 
		public function updateEquipement(equip : EquipementCatalogueVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Constructeur",
							"updateEquipementConstructeur",
							updateEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData,equip.IDEQUIPEMENT,equip.MODELE,equip.REFCONSTRUCTEUR,equip.IDCONSTRUCTEUR,equip.IDTYPE);
		}
		
		public function removeEquipement(equip : EquipementCatalogueVO):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"removeEquClient",
							removeEquipement_handler,null);
			
			RemoteObjectUtil.callService(opData,equip.IDEQUIPEMENT);
		}
		private function removeEquipement_handler(re : ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.REMOVE_EQUIPEMENT_COMPLETE,true));
		}
		private function addEquipement_handler(evt : ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.ADD_EQUIPEMENT_COMPLETE,true,false,evt.result));
		}
		private function updateEquipement_handler(evt : ResultEvent):void
		{
			dispatchEvent(new CatalogueEvent(CatalogueEvent.UPDATE_EQUIPEMENT_COMPLETE,true,false,evt.result));
		} 
		private function rechercheEquipementConstructeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeEquipementCatalogue = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : EquipementCatalogueVO = new EquipementCatalogueVO();
						
				item.IDEQUIPEMENT=tmpArr[i].IDEQUIPEMENT;
				item.REFERENCE=tmpArr[i].REFERENCE;
				item.REFCONSTRUCTEUR=tmpArr[i].REFERENCECONSTRUCTEUR;
				item.TYPE=tmpArr[i].TYPE;
				item.CLASSE=tmpArr[i].CLASSE;
				item.TYPE_CREATION = tmpArr[i].TYPE_CREATION;
				item.ORIGINE=(item.TYPE_CREATION == 1)?ResourceManager.getInstance().getString('M24','Manuel'):tmpArr[i].ORIGINE;				
				item.MODELE=tmpArr[i].MODELE;
				item.NBRECORD=tmpArr[i].NBRECORD;
				item.IDCONSTRUCTEUR = tmpArr[i].IDCONSTRUCTEUR;
				item.IDCLASSE = tmpArr[i].IDCATEGORIE;
				item.IDTYPE = tmpArr[i].IDTYPE;
		
				listeEquipementCatalogue.addItem(item);
			}
		}
		public function set listeEquipementCatalogue(value:ArrayCollection):void
		{
			_listeEquipementCatalogue = value;
		}
		override public function rechercheRevendeur(idClient : Number = -1):void
		{
			if(idClient !=-1){
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"getRevendeur",
							rechercheRevendeur_handler,null);
			
			RemoteObjectUtil.callService(opData,
											
											CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,
											"",
											idClient,
											0
											);
			}
			else
			{
				super.rechercheRevendeur();
			}
		}
		override protected function rechercheRevendeur_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeRevendeur = new ArrayCollection();
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : RevendeurVO = new RevendeurVO();
				item.ID_REVENDEUR=tmpArr[i].IDREVENDEUR;
				item.LIBELLE_REVENDEUR=tmpArr[i].LIBELLE;
				listeRevendeur.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_REVENDEUR_EQUIPEMENT_COMPLETE,true));
		}
		override public function rechercheClient(idDistributeur : Number = -1):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.M24.Client",
							"getClientOfDistributeur",
							rechercheClient_handler,null);
			
			RemoteObjectUtil.callService(opData,idDistributeur);
		}
		override public function listeConstructeurRacine_handler(re : ResultEvent):void
		{ 
			var tmpArr:Array = (re.result as ArrayCollection).source;
			listeConstructeur = new ArrayCollection();
			
			var  specialitem : ConstructeurVO = new ConstructeurVO();
			specialitem.ID_CONSTRUCTEUR=-1;
			specialitem.LIBELLE_CONSTRUCTEUR=ResourceManager.getInstance().getString('M24','Tous');;
			listeConstructeur.addItem(specialitem);
			
			for(var i : int=0; i < tmpArr.length;i++)
			{	
				var  item : ConstructeurVO = new ConstructeurVO();
				item.ID_CONSTRUCTEUR=tmpArr[i].IDCONSTRUCTEUR;
				item.LIBELLE_CONSTRUCTEUR=tmpArr[i].LIBELLECONSTRUCTEUR;
				listeConstructeur.addItem(item);
			}
			dispatchEvent(new CatalogueEvent(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,true));
		}
		
		public function get listeAbonnement():ArrayCollection
		{
			return _listeAbonnement;
		}
	
	}
}