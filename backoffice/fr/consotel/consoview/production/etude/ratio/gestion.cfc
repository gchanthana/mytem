<cfcomponent name="gestion">

	<cffunction name="getSociete" returntype="query">
		<cfif IsDefined('session.requete.qRefClient')>
			<cfquery name="qGetRefClient" datasource="#session.offreDSN#">
					select rc.idref_client, rc.libelle
					from compte_facturation cf, sous_tete st, ref_client rc, sous_compte sco, etude_sous_tete etu
					where rc.idref_client=cf.idref_client
					AND cf.Idcompte_Facturation=sco.Idcompte_Facturation
					AND sco.idsous_compte=st.Idsous_Compte
					AND st.Idsous_Tete=etu.Idsous_Tete
					AND etu.Idetude=#session.etude.idetudeencours#
					group by rc.idref_client, rc.libelle
					order by lower(rc.libelle)
				</cfquery>
				<cfset session.requete.qRefClient=qGetRefClient>
			<cfquery dbtype="query" name="results">
				SELECT libelle AS label, idref_client AS data
				FROM session.requete.qRefClient
				ORDER BY label
			</cfquery>
			<cfelse>
				<cfset results=queryNew("a")>
			</cfif>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getCF" returntype="query">
			<cfif IsDefined('session.requete.qCompteFacturation')>
				<cfquery name="qGetCompteFacturation" datasource="#session.offreDSN#">
					select cf.idcompte_facturation, cf.compte_facturation, cf.idref_client
					from compte_facturation cf, sous_tete st, sous_compte sco, etude_sous_tete etu
					where cf.Idcompte_Facturation=sco.Idcompte_Facturation
					AND sco.idsous_compte=st.Idsous_Compte
					AND st.Idsous_Tete=etu.Idsous_Tete
					AND etu.Idetude=#session.etude.idetudeencours#
					group by cf.idcompte_facturation, cf.compte_facturation, cf.idref_client
					ORDER BY compte_facturation
				</cfquery>
				<cfset session.requete.qCompteFacturation=qGetCompteFacturation>
			<cfquery dbtype="query" name="results">
				SELECT compte_facturation AS label, idcompte_facturation AS data, idref_client
				FROM session.requete.qCompteFacturation
				ORDER BY label
			</cfquery>
			<cfelse>
				<cfset results=queryNew("a")>
			</cfif>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getSite" returntype="query">
			<cfif IsDefined('session.requete.qSite')>
				<cfquery name="qGetSite" datasource="#session.offreDSN#">
					select nom_site , sc.siteID, cf.idcompte_facturation, cf.idref_client
					from compte_facturation cf, sous_tete st, sous_compte sco, etude_sous_tete etu, site_client sc
					where cf.Idcompte_Facturation=sco.Idcompte_Facturation
					AND sco.idsous_compte=st.Idsous_Compte
					and sco.siteid=sc.siteid
					AND st.Idsous_Tete=etu.Idsous_Tete
					AND etu.Idetude=#session.etude.idetudeencours#
					group by nom_site , sc.siteID, cf.idcompte_facturation, cf.idref_client
					ORDER BY lower(nom_site)
				</cfquery>
				<cfset session.requete.qSite=qGetSite>
			<cfquery dbtype="query" name="results">
				SELECT nom_site AS label, siteID AS data, idcompte_facturation
				FROM session.requete.qSite
				ORDER BY label
			</cfquery>
			<cfelse>
				<cfset results=queryNew("a")>
			</cfif>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="filtreCF" returntype="query">
			<cfargument name="data" type="array">
			<cfquery dbtype="query" name="results">
				SELECT compte_facturation AS label, idcompte_facturation AS data, idref_client
				FROM session.requete.qCompteFacturation
				<cfif data[1] neq 0>
					WHERE idref_client=#data[1]#
				</cfif>
				ORDER BY label
			</cfquery>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="filtreSite" returntype="query">
			<cfargument name="data" type="array">
			<cfquery dbtype="query" name="results">
				SELECT nom_site AS label, siteID AS data, idcompte_facturation
				FROM session.requete.qSite
				<cfif data[1] neq 0 AND data[2] neq 0>
					WHERE idref_client=#data[1]#
					AND idcompte_facturation=#data[2]#
				<cfelseif data[1] neq 0>
					where idref_client=#data[1]#
				<cfelseif data[2] neq 0>
					where idcompte_facturation=#data[2]#
				</cfif>
				ORDER BY label
			</cfquery>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getEtude" returntype="query">
			<cfif IsDefined('session.etude.idetudeencours')>
				<cfquery name="qGetEtude" datasource="#session.offreDSN#">
				     SELECT e.*, gc.libelle_groupe_client
				     FROM etude e, groupe_client gc
					where idetude=#session.etude.idetudeencours#
					AND e.idgroupe_client=gc.idgroupe_client
				</cfquery>
				<cfquery dbtype="query" name="results">
					SELECT libelle_groupe_client AS label, idetude AS data
					FROM qGetEtude
					ORDER BY label
				</cfquery>
			<cfelse>
				<cfset results=queryNew("a")>
			</cfif>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getTypeProduit" returntype="query">
			 <cfif IsDefined('session.etude.idetudeencours')>
				<cfquery name="qGetTypeProduit" datasource="#session.offreDSN#">
				     select tp.libelle
					from compte_facturation cf, sous_tete st, sous_compte sco, etude_sous_tete etu, produit_client pcl,
					produit_catalogue pca, type_produit tp
					WHERE cf.Idcompte_Facturation=sco.Idcompte_Facturation
					AND sco.idsous_compte=st.Idsous_Compte
					AND st.Idsous_Tete=etu.Idsous_Tete
					and cf.idref_client=pcl.idref_client
					And pcl.idproduit_catalogue=pca.idproduit_catalogue
					and pca.idtype_produit=tp.idtype_produit
					AND etu.Idetude=#session.etude.idetudeencours#
					group by tp.libelle
					ORDER BY lower(tp.libelle)
				</cfquery>
				<cfset session.requete.qtypeProduit=qGetTypeProduit>
				<cfquery dbtype="query" name="results">
					SELECT libelle AS label, libelle AS data
					FROM session.requete.qtypeProduit
					ORDER BY label
				</cfquery>
			<cfelse>
				<cfset results=queryNew("libelle,libelle")>
			</cfif>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getGroupeProduit" returntype="query">
			<cfquery name="qGetGroupeProduit" datasource="#session.offreDSN#">
				select *
				from groupe_produit
			</cfquery>
			<cfquery dbtype="query" name="results">
				SELECT libelle_groupe_produit AS label, idgroupe_produit AS data
				FROM qGetGroupeProduit
				ORDER BY label
			</cfquery>
			<cfreturn results>
		</cffunction>
		
		<cffunction name="getTypeLigne" returntype="query">
			<cfif IsDefined('session.etude.idetudeencours')>
				<cfquery name="qGettypeLigne" datasource="#session.offreDSN#">
				    select tl.idtype_ligne, tl.libelle_type_ligne
					from compte_facturation cf, sous_tete st, sous_compte sco, etude_sous_tete etu, type_ligne tl
					WHERE cf.Idcompte_Facturation=sco.Idcompte_Facturation
					AND sco.idsous_compte=st.Idsous_Compte
					AND st.Idsous_Tete=etu.Idsous_Tete
					AND st.idtype_ligne=tl.idtype_ligne
					AND etu.Idetude=#session.etude.idetudeencours#
					group by tl.idtype_ligne, tl.libelle_type_ligne
					ORDER BY lower(tl.libelle_type_ligne)
				</cfquery>
				<cfquery dbtype="query" name="results">
					SELECT libelle_type_ligne AS label, idtype_ligne AS data
					FROM qGettypeLigne
					ORDER BY label
				</cfquery>
			<cfelse>
				<cfset results=queryNew("a")>
			</cfif>
			<cfreturn results>
		</cffunction>
	</cfcomponent>