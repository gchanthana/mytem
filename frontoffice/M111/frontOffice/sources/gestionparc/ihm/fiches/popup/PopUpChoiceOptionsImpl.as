package gestionparc.ihm.fiches.popup
{	
	import flash.events.Event;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.ligne.LigneServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.TileList;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	[Bindable]
	public class PopUpChoiceOptionsImpl extends Box
	{

//VARIABLES------------------------------------------------------------------------------
		
		// Checkbox
		public var ckbxSelectAll			:CheckBox;
		
		// DataGrid
		public var dgListProduit			:DataGrid;
		
		// TILELIST
		public var tFiltre					:TileList;
		
		// TextInput
		public var txtptSearch				:TextInput;
		
		// Label
		public var lblFavoris				:Label;
		public var lblFavorisUnder			:Label;
		public var lblTout					:Label;
		public var lblToutUnder				:Label;
		
		// Variables exterieurs
		public var myServices				: LigneServices;
		public var idOperateur				: Number;
		public var typeCmdSelected			: Number;
		
		// Variables Locales
		private var _listProduit			:ArrayCollection = new ArrayCollection();
		private var _listFiltre				:ArrayCollection = new ArrayCollection();
		private var _arrayTemp				:ArrayCollection = new ArrayCollection();
		private var _listProduitVisible		:ArrayCollection = new ArrayCollection();
		private var _isFavorite				:Boolean = true;
		
//VARIABLES------------------------------------------------------------------------------

//PROPRIETEES PUBLIC---------------------------------------------------------------------

		//RECUPERE LA LISTE DES OPTIONS

		public function get listFiltre():ArrayCollection
		{
			return _listFiltre;
		}

		public function set listFiltre(value:ArrayCollection):void
		{
			_listFiltre = value;
		}

		public function get listProduit(): ArrayCollection
	    {
	    	return _listProduit;
	    }
	    
	    public function set listProduit(value:ArrayCollection): void
	    {
	    	_listProduit = value;
	    }

//PROPRIETEES PUBLIC---------------------------------------------------------------------

//METHODES PUBLIC------------------------------------------------------------------------

		//CONSTRUCTEUR
		public function PopUpChoiceOptionsImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
//METHODES PUBLIC------------------------------------------------------------------------

//METHODES PROTECTED---------------------------------------------------------------------
		
		//RECHERCHE ET AFFICHAGE DE LA LISTE DES OPTIONS AU FUR ET A MESURE QUE L'ON SAISIE DU TEXTE
		protected function txtptSearchHandler():void
		{
			var arrayC:ArrayCollection = new ArrayCollection();
			listProduit = new ArrayCollection();
			if(txtptSearch.text == "")
			{
				listProduit = _listProduitVisible;
			}
			else
			{
				for(var i:int = 0;i < _listProduitVisible.length;i++)
				{
					if(_listProduitVisible[i].LIBELLE_PRODUIT.toString().toLowerCase().indexOf(txtptSearch.text.toLowerCase()) != -1)
					{ 
						arrayC.addItem(_listProduitVisible[i]); 
					}
				}
				listProduit = arrayC;
			}
		}
		
		//REINITIALISE LE CHAMP DE RECHERCHE AINSI QUE LE DATAGRID (>REAFFICHE TOUS)
		protected function btReinitialisationClickHandler():void
		{
			txtptSearch.text = "";
			listProduit = new ArrayCollection();
			_listProduitVisible = new ArrayCollection();
			myServices.fournirListeAbonnementsOptions(idOperateur,typeCmdSelected);
			whatIsChbxSelected();
		}
		
		//REGADE QUELLES CHECKBOX SONT SELECTIONNEES ET AFFICHE LES OPTIONS SUIVANT LE FILTRE
		private function whatIsChbxSelected(evt:Event=null):void
		{
			listProduit.filterFunction = arrayFiltre;
			listProduit.refresh();
		}
		private function arrayFiltre(item:Object):Boolean
		{
			var isValid:Boolean = false;
			if(!ckbxSelectAll.selected)
			{
				for each(var obj:Object in listFiltre)
				{
					if(obj.SELECTED)
					{
						if(item.IDTHEME_PRODUIT == obj.ID)
						{
							isValid = true;
							break;
						}
					}
				}
			}
			else
				isValid = true;
			
			return isValid;
		}
		//VERIFIE SI TOUS LES FILTRES SONT COCHES OU PAS
		private function checkSelectAll():void
		{
			var allSelected:Boolean = true;			
			for each(var obj:Object in listFiltre)
			{
				if(!obj.SELECTED)
				{
					allSelected = false;
					break;
				}
			}
			ckbxSelectAll.selected = allSelected;
		}
		//AFFICHE LES FAVORIS
		protected function lblFavorisClickHandler():void
		{
			_isFavorite = true;
			
			lblFavoris.visible = false;
			lblFavoris.width = 0;
			lblFavorisUnder.visible = true;
			lblFavorisUnder.width = 60;
			
			lblTout.visible = true;
			lblTout.width = 50;
			lblToutUnder.visible = false;
			lblToutUnder.width = 0;
			
			listProduit = myServices.myDatas.listeOptFavoris;
			
			whatIsChbxSelected();
		}

		//AFFICHE LES FAVORIS
		protected function lblToutClickHandler():void
		{
			_isFavorite = false;

			lblTout.visible = false;
			lblTout.width = 0;
			lblToutUnder.visible = true;
			lblToutUnder.width = 50;
			
			lblFavoris.visible = true;
			lblFavoris.width = 60;
			lblFavorisUnder.visible = false;
			lblFavorisUnder.width = 0;

			listProduit = myServices.myDatas.listeOpt;

			whatIsChbxSelected();
		}

		protected function dgListProduitClickHandler(evt:ListEvent):void
		{
			if(dgListProduit.selectedItem != null && evt.columnIndex != 3) // columnIndex == 3 == favoris
			{
				var longueur : int = listProduit.length;
				for(var i:int = 0;i < longueur;i++)
				{
					if( listProduit[i].IDPRODUIT_CATALOGUE.toString().toLowerCase().indexOf(dgListProduit.selectedItem.IDPRODUIT_CATALOGUE.toString().toLowerCase()) != -1)
					{
						if(listProduit[i].SELECTED == true)
						{
							listProduit[i].SELECTED = false;
						}
						else
						{
							listProduit[i].SELECTED = true;
						}
					}
				}
			}
		}

//METHODES PROTECTED---------------------------------------------------------------------

//METHODES PRIVATE-----------------------------------------------------------------------
		
		//INITIALISE APRES LA CREATION DE TOUS LES COMPOSANTS
		private function init(evt:FlexEvent):void
		{
			listProduit = new ArrayCollection();
			listProduit = myServices.myDatas.listeOptFavoris;
			initListener();
		}
		
		//INITIALISE LES ECOUTEURS
		private function initListener():void
		{
			myServices.myDatas.addEventListener(gestionparcEvent.LISTED_ABO_OPT_LOADED,whatIsChbxSelected);
			addEventListener(gestionparcEvent.THIS_IS_FAVORITE, thisIsAFavoriteSelected);
			addEventListener(gestionparcEvent.THIS_IS_NOT_FAVORITE, thisIsNotFavoriteSelected);
		}
		//APPEL DE PROCEDURE PERMETTANT L'ENVOI D'UNE OPTIONS SELECTIONNE EN TANT QUE 'FAVORI'
		//APPEL DE PROCEDURE PERMETTANT DE LISTER TOUTES LES OPTIONS
		private function thisIsAFavoriteSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null && dgListProduit.selectedItem.FAVORI == 1)
			{
				myServices.ajouterProduitAMesFavoris(dgListProduit.selectedItem);
				// copie l'item dans les favoris
				myServices.myDatas.listeOptFavoris.addItem(dgListProduit.selectedItem);
			}
		}
		public function ckbxChangeHandler():void
		{
			checkSelectAll();
			whatIsChbxSelected();
		}
		//APPEL DE PROCEDURE PERMETTANT L'ENVOI D'UNE OPTIONS SELECTIONNE EN TANT QUE 'NON FAVORI'
		//APPEL DE PROCEDURE PERMETTANT DE LISTER TOUTES LES OPTIONS
		private function thisIsNotFavoriteSelected(e:Event):void
		{
			if(dgListProduit.selectedItem != null && dgListProduit.selectedItem.FAVORI == 0)
			{
				myServices.supprimerProduitDeMesFavoris(dgListProduit.selectedItem);
				// recherche et supprime l'item des favoris
				var longueur : int = myServices.myDatas.listeOptFavoris.length;
				for(var i:int = 0; i < longueur ; i++)
				{
					if(myServices.myDatas.listeOptFavoris[i] == dgListProduit.selectedItem)
					{
						myServices.myDatas.listeOptFavoris.removeItemAt(i);
						break;
					}
				}
				whatIsChbxSelected();
			}
		}

//METHODES PRIVATE-----------------------------------------------------------------------
		
	}
}