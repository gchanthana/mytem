package gestionparc.ihm.action.dissocier.actionDissocierCollabFromEqp
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierCollabFromEqpCommand implements ICommand
	{
		private var receiver:ActionDissocierCollabFromEqpReceiver;

		public function ActionDissocierCollabFromEqpCommand(rec:ActionDissocierCollabFromEqpReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
