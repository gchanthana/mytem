package gestionparc.ihm.fiches.ficheEntity
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Form;
	import mx.containers.FormItem;
	import mx.containers.HBox;
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.Spacer;
	import mx.controls.TabBar;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	import mx.validators.Validator;
	
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import gestionparc.entity.EmplacementVO;
	import gestionparc.entity.LigneVO;
	import gestionparc.entity.OrgaVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCablageSdaTypePointaPointIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCablageSdaTypeT2T0AnaIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCablageSdaTypeXdslIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCanauxGroupmentIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentCaracteristiqueFixeIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentContratOperateurLigneIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentDiversIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentTabNavSiteIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentTicketsAppelsIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.TabModelIHM;
	import gestionparc.ihm.fiches.historique.FicheHistoriqueIHM;
	import gestionparc.ihm.fiches.popup.PopUpAboOptIHM;
	import gestionparc.ihm.fiches.popup.PopUpSearchFonctionIHM;
	import gestionparc.ihm.fiches.popup.PopUpUsageIHM;
	import gestionparc.ihm.fiches.popup.popupResiliationLigne;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.ligne.LigneServices;
	import gestionparc.services.organisation.OrganisationServices;
	import gestionparc.services.site.SiteServices;
	import gestionparc.utils.gestionparcConstantes;
	import gestionparc.utils.gestionparcMessages;

	[Bindable]
	public class FicheLigneImpl extends TitleWindowBounds
	{
		public var tiSousTete:Label;
		public var tiNumContrat:TextInput;
		public var tiRio:TextInput;

		public var lbPosition:Label;
		public var lbAboPrincipal:Label;
		public var lbOperateur:Label;
		public var lbOperateurPreselection:Label;
		public var lbCompte:Label;
		public var lbSousCompte:Label;
		public var lbNomPrenomCollab:Label;
		public var lbMatriculeCollab:Label;
		public var lbNumSim:Label;

		public var taCommentaires:TextArea;

		public var valSousTete:Validator;
		public var valNumContrat:Validator;
		public var valOperateur:Validator;
		public var valDistributeur:Validator;

		public var cbEtat:ComboBox;
		public var cbFonction:ComboBox;
		public var cbTitulaire:ComboBox;
		public var cbPayeur:ComboBox;
		public var cbTypeLigne:ComboBox;
		public var cbOrganisation:ComboBox;
		public var cbDistributeur:ComboBox;

		public var tnLigne:ViewStack;
		public var tnLigneTabBar:TabBar;
		public var tncSite:ContentTabNavSiteIHM;
		public var tncDivers:ContentDiversIHM;
		public var tncTickets:ContentTicketsAppelsIHM;
		public var tncRaccordement:TabModelIHM;
		public var tncAboOption:TabModelIHM;
		public var tncContratOperateur:ContentContratOperateurLigneIHM;
		public var tncCablageSdaPointaPoint:ContentCablageSdaTypePointaPointIHM;
		public var tncCablageSdaXdsl:ContentCablageSdaTypeXdslIHM;
		public var tncCablageSdaT2T0Ana:ContentCablageSdaTypeT2T0AnaIHM;
		public var tncCanauxGroupment:ContentCanauxGroupmentIHM;
		public var tncCaracteristique:ContentCaracteristiqueFixeIHM;

		public var foCollab:Form;
		public var fiSim:FormItem;
		public var fiTypeLigne:FormItem;
		public var fiOperateur:FormItem;
		public var fiDistributeur:FormItem;
		public var fiOperateurPreselection:FormItem;
		public var fiRio:FormItem;
		public var fi_operateurTelecom	:FormItem;
		public var fi_compteFactu		:FormItem;
		
		public var btValid:Button;
		public var btCancel:Button;
		public var btModifierOrga:Button;
		public var cmbfiltre:ComboBox=new ComboBox();

		public var imgEditFonction:Image;
		public var imgSearchFonction:Image;

		public var hbLignePrincipaleBackUp:HBox;

		public var rbLignePrincipale:RadioButton;
		public var rbLigneBackUp:RadioButton;

		public var idsoustete:Number;
		public var idRow:Number=-1;
		
		public var lbl_operateurTelecom		:Label;
		public var lbl_compteFacturation	:Label;
		
		private var myServiceLigne:LigneServices=null;
		private var myServiceOrga:OrganisationServices=null;
		private var myServicesSite:SiteServices=null;

		private var ficheOrga:FicheOrgaIHM=null;
		private var myDataProviderAboOpt:ArrayCollection=null;

		private var indexTypeCmd:int=0;
		private var indexOperateur:int=0;

		private var strOldUsage:String="";

		private var pin1:String="";
		private var pin2:String="";
		private var puk1:String="";
		private var puk2:String="";

		private var boolNav:Boolean=false;

		private var oldNumero:String="";

		public var colonnesRaccordement:ArrayCollection;
		public var colonnesAboOptions:ArrayCollection;

		public var listeTypeLigne:ArrayCollection=new ArrayCollection;

		private var _popupSearchFonction:PopUpSearchFonctionIHM;

		[Embed(source='/assets/images/arrow_right_green.png')]
		private var myIconProduitAcces:Class;

		private var popupResi:popupResiliationLigne;
		private var dateResi:Date;

		private var boolSite:Boolean=false;
		private var boolContrat:Boolean=false;
		private var boolAbo:Boolean=false;
		private var boolDivers:Boolean=false;
		private var boolTicketsAppels:Boolean=false;
		private var boolT2:Boolean=false;
		private var boolXdsl:Boolean=false;
		private var boolPoint:Boolean=false;
		private var boolRacco:Boolean=false;
		private var boolCanaux:Boolean=false;
		private var boolCaracteristique:Boolean=false;

		// au Creation Complete
		private var TYPE_LIGNE_GSM:int=852;
		private var TYPE_LIGNE_MOBILE:int=707;

		public var visibleSNCF:Boolean = false;


		public function FicheLigneImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
				return DateFunction.formatDateAsString(item[column.dataField]);
			else
				return "";
		}

		private function init(fe:FlexEvent):void
		{
			myServiceLigne=new LigneServices();
			myServicesSite=new SiteServices();

			createOnglet();
			initListener();
			initData();
			initDisplay();
			var hgt:Number=this.titleBar.height;
			var wdt:Number=this.titleBar.width;
		}

		private function initData():void
		{
			myServiceLigne.getInfosFicheLigne(idsoustete, idRow);
		}

		private function tncCaracteristiqueComplete(evt:FlexEvent):void
		{
			boolCaracteristique=true;
			initIHM();
		}

		private function tncSiteComplete(evt:FlexEvent):void
		{
			boolSite=true;
			initIHM();
		}

		private function tncAboComplete(evt:FlexEvent):void
		{
			boolAbo=true;
			initIHM();
		}

		private function tncContratComplete(evt:FlexEvent):void
		{
			boolContrat=true;
			initIHM();
		}

		private function tncDiverComplete(evt:FlexEvent):void
		{
			boolDivers=true;
			initIHM();
		}
		
		private function tncTicketsComplete(evt:FlexEvent):void
		{
			boolTicketsAppels = true;
			initIHM();
		}

		private function tncRaccoComplete(evt:FlexEvent):void
		{
			boolRacco=true;
			initIHM();
		}

		private function tncT2Complete(evt:FlexEvent):void
		{
			boolT2=true;
			initIHM();
		}

		private function tncPointComplete(evt:FlexEvent):void
		{
			boolPoint=true;
			initIHM();
		}

		private function tncXdslComplete(evt:FlexEvent):void
		{
			boolXdsl=true;
			initIHM();
		}

		private function tncCanauxComplete(evt:FlexEvent):void
		{
			tncCanauxGroupment.setInfos();
			boolCanaux=true;
			initIHM();
		}

		private function initIHM():void
		{
			if (boolAbo && boolCanaux && boolContrat && boolDivers && (boolPoint || boolT2 || boolXdsl) && boolRacco && boolSite && boolCaracteristique)
			{
				boolAbo=false;
				boolCanaux=false;
				boolContrat=false;
				boolDivers=false;
				boolTicketsAppels=false;
				boolPoint=false;
				boolRacco=false;
				boolSite=false;
				boolT2=false;
				boolXdsl=false;
				boolCaracteristique=false;

				initListenerOnglet();
				initDisplayOnglet();
			}
		}

		private function majInfoOnglet():void
		{
			majInfosUsage();
			majInfosEtat();
			majInfosOrga();
			majInfosOngletSite();
			majInfosOngletAboOption();
			majInfosOngletContrat();
			majInfosOngletDivers();
			majCaracteristique();
			majInfosOngletRaccordement();
			majInfosOngletCanauxGroupement();
			majInfosOngletSDA();
		}

		private function createOnglet():void
		{
		
			tncSite=new ContentTabNavSiteIHM();
			tncSite.addEventListener(FlexEvent.CREATION_COMPLETE, tncSiteComplete);
			tncSite.label=ResourceManager.getInstance().getString('M111', 'Site');
			tncSite.height=370;
			tncSite.percentWidth=100;

			if (SpecificationVO.getInstance().elementDataGridSelected.S_IDTYPE_EQUIP==moduleGestionParcIHM.IDTYPESIM)
				tncSite.toolTip=ResourceManager.getInstance().getString('M111', 'Une_carte_sim_est_necessaire_pour_pouvoir_saisir_un_site');
			
			tncCaracteristique=new ContentCaracteristiqueFixeIHM();
			tncCaracteristique.addEventListener(FlexEvent.CREATION_COMPLETE, tncCaracteristiqueComplete);
			tncCaracteristique.label=resourceManager.getString('M111', 'onglet_Caract_ristique');
			tncCaracteristique.height=280;
			tncCaracteristique.percentWidth=100;
			tncCaracteristique.myLigneService=this.myServiceLigne;

			tncAboOption=new TabModelIHM();
			tncAboOption.addEventListener(FlexEvent.CREATION_COMPLETE, tncAboComplete);
			tncAboOption.label=resourceManager.getString('M111', 'Abos_et_options');

			tncAboOption.labelInventaire=resourceManager.getString('M111', 'Dans_l_inventaire');
			tncAboOption.labelHistorique=resourceManager.getString('M111', 'Historique');
			tncAboOption.typeAjoutColonnes="none";
			tncAboOption.colonnes=colonnesAboOptions;
			tncAboOption.datagridHeight=200;
			tncAboOption.height=280;
			tncAboOption.percentWidth=100;
			tncAboOption.buttonActionVisible=false

			tncContratOperateur=new ContentContratOperateurLigneIHM();
			tncContratOperateur.addEventListener(FlexEvent.CREATION_COMPLETE, tncContratComplete);
			tncContratOperateur.height=280;
			tncContratOperateur.label=resourceManager.getString('M111', 'Contrat_op_rateur');

			tncDivers=new ContentDiversIHM();
			tncDivers.addEventListener(FlexEvent.CREATION_COMPLETE, tncDiverComplete);
			tncDivers.height=280;
			tncDivers.label=resourceManager.getString('M111', 'Divers');
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("D_CDR") && moduleGestionParcIHM.globalParameter.userAccess.D_CDR == 1)
			{
				tncTickets = new ContentTicketsAppelsIHM();
				tncTickets.addEventListener(FlexEvent.CREATION_COMPLETE, tncTicketsComplete);
				tncTickets.label = resourceManager.getString('M111', 'Liste_des_appels');
			}
			
			tncRaccordement=new TabModelIHM();
			tncRaccordement.addEventListener(FlexEvent.CREATION_COMPLETE, tncRaccoComplete);
			tncRaccordement.label=resourceManager.getString('M111', 'Raccordement');
			tncRaccordement.typeAjoutColonnes="none";
			tncRaccordement.colonnes=colonnesRaccordement;
			tncRaccordement.datagridHeight=200;
			tncRaccordement.height=280;
			tncRaccordement.percentWidth=100;
			tncRaccordement.buttonActionVisible=true;
			tncRaccordement.buttonActionLabel=resourceManager.getString('M111', 'Changer_le_raccordement');

			tncCablageSdaT2T0Ana=new ContentCablageSdaTypeT2T0AnaIHM();
			tncCablageSdaT2T0Ana.addEventListener(FlexEvent.CREATION_COMPLETE, tncT2Complete);
			tncCablageSdaT2T0Ana.label=resourceManager.getString('M111', 'C_blage_SDA');
			tncCablageSdaT2T0Ana.height=280;
			tncCablageSdaT2T0Ana.percentWidth=100;
			tncCablageSdaT2T0Ana.myServiceLigne=this.myServiceLigne;

			tncCablageSdaPointaPoint=new ContentCablageSdaTypePointaPointIHM();
			tncCablageSdaPointaPoint.addEventListener(FlexEvent.CREATION_COMPLETE, tncPointComplete);
			tncCablageSdaPointaPoint.label=resourceManager.getString('M111', 'C_blage_SDA');
			tncCablageSdaPointaPoint.height=280;
			tncCablageSdaPointaPoint.percentWidth=100;
			tncCablageSdaPointaPoint.myServiceLigne=this.myServiceLigne;

			tncCablageSdaXdsl=new ContentCablageSdaTypeXdslIHM();
			tncCablageSdaXdsl.addEventListener(FlexEvent.CREATION_COMPLETE, tncXdslComplete);
			tncCablageSdaXdsl.label=resourceManager.getString('M111', 'C_blage_SDA');
			tncCablageSdaXdsl.height=280;
			tncCablageSdaXdsl.percentWidth=100;
			tncCablageSdaXdsl.myServiceLigne=this.myServiceLigne;
	
			tncCanauxGroupment=new ContentCanauxGroupmentIHM();
			tncCanauxGroupment.addEventListener(FlexEvent.CREATION_COMPLETE, tncCanauxComplete);
			tncCanauxGroupment.label=resourceManager.getString('M111', 'Canaux_Groupement');
			tncCanauxGroupment.height=280;
			tncCanauxGroupment.percentWidth=100;
			tncCanauxGroupment.myServiceLigne=this.myServiceLigne;
		}

		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			this.addEventListener(gestionparcEvent.AFFICHER_HISTORIQUE, displayHistoriqueHandler);
			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);
			imgEditFonction.addEventListener(MouseEvent.CLICK, imgEditFonctionClickHandler);
			imgSearchFonction.addEventListener(MouseEvent.CLICK, imgSearchFonctionClickHandler);
			cbTypeLigne.addEventListener(ListEvent.CHANGE, cbTypeLigneChangeHandler);
			cbEtat.addEventListener(ListEvent.CHANGE, cbEtatChangeHandler);
			cbOrganisation.addEventListener(ListEvent.CHANGE, changeOrgaHandler);
			btModifierOrga.addEventListener(MouseEvent.CLICK, clickModifierOrgaHandler);
			myServiceLigne.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_LOADED, infoFicheLigneLoadedHandler);
		}

		private function initListenerOnglet():void
		{
			if (tncSite != null && tncSite.lbEmplacement != null)
				tncSite.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, onInfosFicheSiteUploadedHandler);
			if (tncRaccordement != null && tncRaccordement.buttonAction != null)
				tncRaccordement.buttonAction.addEventListener(MouseEvent.CLICK, onTncRaccordementClickHandler);
		}

		private function initDisplay():void
		{
			btValid.enabled=true;
			lbNomPrenomCollab.text=(SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR) ? SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR : "-";
			lbMatriculeCollab.text=(SpecificationVO.getInstance().elementDataGridSelected.E_MATRICULE) ? SpecificationVO.getInstance().elementDataGridSelected.E_MATRICULE : "-";
			if (SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE != 0) // ligne associée à un collab
			{
				foCollab.visible=true;
				foCollab.includeInLayout=true;
				btModifierOrga.visible=false;
				btModifierOrga.includeInLayout=false;
			}
			else // ligne pas associée à un collab
			{
				foCollab.visible=false;
				foCollab.includeInLayout=false;
				btModifierOrga.visible=true;
				btModifierOrga.includeInLayout=true;
			}
			fiOperateur.required=false;
			fiTypeLigne.required=false;
			fiDistributeur.visible=false;
			fiDistributeur.includeInLayout=false;
			
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("MASTER_PARC_GFC")
				&& moduleGestionParcIHM.globalParameter.userAccess.MASTER_PARC_GFC == 1)
			{
				visibleSNCF = true;
			}
		}

		private function initDisplayOnglet():void
		{
			if (tncAboOption != null && tncAboOption.dataDatagrid != null)
				tncAboOption.addColonnes("ligne");

			if (tncSite != null && tncSite.lbEmplacement != null)
				tncSite.myServicesCache=CacheServices.getInstance();
		}

		// changer de type de ligne dans le combo
		private function cbTypeLigneChangeHandler(le:ListEvent):void
		{
			majDisplayOnglet(cbTypeLigne.selectedItem.TYPE_FICHELIGNE);
			selectedTypeLigneChanged();
		}

		// gérer affichage dans l'Input "protocole physique" lors d'un changement de valeurs dans combobox Type ligne
		private function selectedTypeLigneChanged():void
		{
			if (tncCaracteristique != null && tncCaracteristique.tiProtocolePhysique != null)
			{
				switch (cbTypeLigne.selectedItem.IDTYPE_LIGNE)
				{
					case "1":
						tncCaracteristique.tiProtocolePhysique.text="analogique";
						break; //ligne telephonique
					case "83":
						tncCaracteristique.tiProtocolePhysique.text="analogique";
						break; //ligne telephonique
					case "21":
						tncCaracteristique.tiProtocolePhysique.text="ISDN";
						break; // ligne numeris accès de base
					case "22":
						tncCaracteristique.tiProtocolePhysique.text="ISDN";
						break; // ligne numeris accès primaire
					default:
						tncCaracteristique.tiProtocolePhysique.text="";
						break;
				}
			}
		}

		// changer etat d'une ligne dans combo box
		private function cbEtatChangeHandler(evt:ListEvent):void
		{
			if (cbEtat.selectedIndex > -1)
			{
				if (cbEtat.selectedItem.data == 3)
				{
					popupResi=new popupResiliationLigne(tncAboOption.dataDatagrid);
					popupResi.addEventListener(gestionparcEvent.POPUP_RESILIATION_VALIDATED, popupResilitationHandler);
					PopUpManager.addPopUp(popupResi, Application.application as DisplayObject, true);
					PopUpManager.centerPopUp(popupResi);
				}
			}
		}

		private function popupResilitationHandler(evt:gestionparcEvent):void
		{
			dateResi=popupResi.selectedDate;
			for (var i:int=0; i < myServiceLigne.myDatas.ligne.listeAboOption.length; i++)
			{
				if (myServiceLigne.myDatas.ligne.listeAboOption[i].dansInventaire == 1)
				{
					myServiceLigne.myDatas.ligne.listeAboOption[i].aSortir=1;
					myServiceLigne.myDatas.ligne.listeAboOption[i].date=dateResi;
					myServiceLigne.myDatas.ligne.listeAboOption[i].aEntrer=0
				}
			}
			tncAboOption.dataDatagrid=myServiceLigne.myDatas.ligne.listeAboOption;
			myServiceLigne.myDatas.ligne.listeAboOption.refresh();
		}

		public function refreshInfoLigne(evt:FlexEvent):void
		{
			if (tncCanauxGroupment != null && tnLigne.selectedChild == tncCanauxGroupment)
				setInfoLigneVO()
		}

		private function majInfoFicheLigne():void
		{
			oldNumero=myServiceLigne.myDatas.ligne.sousTete;
			tiSousTete.text=myServiceLigne.myDatas.ligne.sousTete;
			tiRio.text=myServiceLigne.myDatas.ligne.codeRio;
			if (myServiceLigne.myDatas.ligne.numContrat != null && myServiceLigne.myDatas.ligne.numContrat != "")
				tiNumContrat.text=myServiceLigne.myDatas.ligne.numContrat;
			else
				tiNumContrat.text=ResourceManager.getInstance().getString('M111', 'n_c_');
			lbAboPrincipal.text=myServiceLigne.myDatas.ligne.aboPrincipale;
			lbOperateur.text = lbOperateur.toolTip = myServiceLigne.myDatas.ligne.operateur.nom;
			lbOperateurPreselection.text = lbOperateurPreselection.toolTip = myServiceLigne.myDatas.ligne.operateurPreselect.nom;
			lbCompte.text = lbCompte.toolTip= myServiceLigne.myDatas.ligne.compte;
			lbSousCompte.text = lbSousCompte.toolTip = myServiceLigne.myDatas.ligne.sousCompte;
			taCommentaires.text=myServiceLigne.myDatas.ligne.commentaires;
			cbTitulaire.selectedIndex=(myServiceLigne.myDatas.ligne.titulaire) ? 1 : 0;
			cbPayeur.selectedIndex=(myServiceLigne.myDatas.ligne.payeur) ? 1 : 0;

			cbFonction.dataProvider=CacheDatas.listeUsages;
			cbFonction.dropdown.dataProvider=CacheDatas.listeUsages;
			cbFonction.selectedItem=getItemListeUsage();

			listeTypeLigne=CacheDatas.listeTypesLigne;
			listeTypeLigne.filterFunction=filterDataProviderTypeLigne;
			listeTypeLigne.refresh();
			cbTypeLigne.selectedItem=getItemListeTypeLigne();

			rbLignePrincipale.selected=(myServiceLigne.myDatas.ligne.isLignePrincipale) ? true : false;
			rbLigneBackUp.selected=(myServiceLigne.myDatas.ligne.isLigneBackUp) ? true : false;

			fiSim.visible=true;
			fiOperateurPreselection.visible=false;
			hbLignePrincipaleBackUp.visible=false;
			
			// Equipement Mobile ?
			if (myServiceLigne.myDatas.ligne.idTypeLigne == TYPE_LIGNE_MOBILE || myServiceLigne.myDatas.ligne.idTypeLigne == TYPE_LIGNE_GSM)
			{

				fiSim.label=resourceManager.getString('M111', 'N__sim__');
				lbNumSim.text=myServiceLigne.myDatas.ligne.simRattache.numSerieEquipement;
			}
			else
			{
				fiSim.label=resourceManager.getString('M111', 'Equipement');
				lbNumSim.text=myServiceLigne.myDatas.ligne.eqpRattache.numSerieEquipement;

				fiOperateurPreselection.visible=true;
				hbLignePrincipaleBackUp.visible=true;
			}
			
			lbl_operateurTelecom.text = lbl_operateurTelecom.toolTip=myServiceLigne.myDatas.ligne.operateurTelecom;
			lbl_compteFacturation.text = lbl_compteFacturation.toolTip = myServiceLigne.myDatas.ligne.compteFacturationTelecom;
		}

		private function filterDataProviderTypeLigne(item:Object):Boolean
		{
			var rfilter:Boolean=true;

			if (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEFIXE)
				rfilter=rfilter && ((item.ISFIXE != null && item.ISMOBILE != null) && (item.ISFIXE == 1 && item.ISMOBILE == 0));
			else if (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE)
				rfilter=rfilter && ((item.ISFIXE != null && item.ISMOBILE != null) && (item.ISFIXE == 0 && item.ISMOBILE == 1));

			return rfilter;
		}

		// Récupèrer l'index de l'ArrayCollection du type ligne à afficher dans ComboBox
		private function getItemListeTypeLigne():Object
		{
			for each (var item:Object in listeTypeLigne)
			{
				if (myServiceLigne.myDatas.ligne.idTypeLigne == item.IDTYPE_LIGNE)
					return item;
			}
			return null;
		}

		private function getItemListeUsage():Object
		{
			for each (var item:Object in cbFonction.dataProvider)
			{
				if (myServiceLigne.myDatas.ligne.fonction !=null)
				{
					if (myServiceLigne.myDatas.ligne.fonction == item.USAGE)
						return item;
				}
			}
			return null;
		}

		//gere quels onglets seront affichés selon le type de fiche ligne selectionné
		private function majDisplayOnglet(type_ficheligne:String):void
		{
			tnLigne.removeAllChildren();

			switch (type_ficheligne)
			{
				case gestionparcConstantes.LS:
					changeOngletLS();
					break;
				case gestionparcConstantes.LL:
					changeOngletLS();
					break;
				case gestionparcConstantes.XDSL:
					changeOngletXDSL();
					break;
				case gestionparcConstantes.T2:
					changeOngletT2();
					break;
				case gestionparcConstantes.T0:
					changeOngletT2();
					break;
				case gestionparcConstantes.LA:
					changeOngletT2();
					break;
				default:
					changeOngletDefault();
					break;
			}
			tnLigneTabBar.selectedIndex=0;
		}

		private function majInfosUsage():void
		{
			var lenListeUsage:int=CacheDatas.listeUsages.length;
			for (var j:int=0; j < lenListeUsage; j++)
			{
				if (CacheDatas.listeUsages[j].USAGE != null && CacheDatas.listeUsages[j].USAGE == myServiceLigne.myDatas.ligne.fonction)
					cbFonction.selectedIndex=j;
				break;
			}
		}

		private function majInfosEtat():void
		{
			switch (myServiceLigne.myDatas.ligne.etat)
			{
				case 1:
					cbEtat.selectedIndex=0;
					break;
				case 2:
					cbEtat.selectedIndex=1;
					break;
				case 3:
					cbEtat.selectedIndex=2;
					break;
			}
		}

		private function majCaracteristique():void
		{
			if (tncCaracteristique && tncCaracteristique.tiDescendantContrat != null)
			{
				tncCaracteristique.myLigneService=this.myServiceLigne;
				tncCaracteristique.setInfos();
			}
		}

		private function majInfosOrga():void
		{
			// maj orga
			var lenListOrga:int=myServiceLigne.myDatas.ligne.listeOrganisation.length;
			var myPattern:RegExp=/\//g;

			// boucle pour remplacer les "/" des anciens chemins par des ";"
			for (var k:int=0; k < lenListOrga; k++)
			{
				if ((myServiceLigne.myDatas.ligne.listeOrganisation[k] as OrgaVO).chemin)
				{
					(myServiceLigne.myDatas.ligne.listeOrganisation[k] as OrgaVO).chemin=(myServiceLigne.myDatas.ligne.listeOrganisation[k] as OrgaVO).chemin.replace(myPattern, ";");
				}
			}
			cbOrganisation.dataProvider=myServiceLigne.myDatas.ligne.listeOrganisation;

			// selectionne le dernier chemin modifié
			var dateLastModifPosition:Date=new Date(0);
			var lenListeOrga:int=myServiceLigne.myDatas.ligne.listeOrganisation.length;

			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).position == 1 && (myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition != null)
				{
					if (ObjectUtil.dateCompare((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition, dateLastModifPosition) == 1)
					{
						lbPosition.text=(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).chemin;
						cbOrganisation.selectedIndex=i;
						dateLastModifPosition=(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition;
					}
				}
			}
		}

		private function majInfosOngletSite():void
		{
			// maj onglet SITE 
			if (tncSite != null && tncSite.lbSite != null)
			{
				tncSite.lbInstAdresse.text=myServiceLigne.myDatas.ligne.inst_adresse1;
				tncSite.lbInstSite.text=myServiceLigne.myDatas.ligne.inst_nomsite;
				tncSite.lbInstCodePostal.text=myServiceLigne.myDatas.ligne.inst_zipcode;
				tncSite.lbInstVille.text=myServiceLigne.myDatas.ligne.inst_commune;

				// pas de site possible si pas de sim ou d'équipemnt fixe associé
				if ((myServiceLigne.myDatas.ligne.simRattache.idEquipement > -1 && SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEMOBILE) || (SpecificationVO.getInstance().elementDataGridSelected.ISLIGNEFIXE && myServiceLigne.myDatas.ligne.eqpRattache.idEquipement > -1))
				{
					tncSite.enabled=true;
					tncSite.visible=true;

					if (myServiceLigne.myDatas.ligne.idSite > 0)
					{
						var objToGetEmplacement:Object=new Object();
						objToGetEmplacement.idSite=myServiceLigne.myDatas.ligne.idSite;
						myServicesSite.getSiteEmplacement(objToGetEmplacement);
						myServicesSite.getEmplacement(objToGetEmplacement);

						myServicesSite.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_LOADED, onInfosSiteLoadedHandler);
					}
					else
					{
						tncSite.txtSite.text="";
						tncSite.txtEmplacement.text="";
					}
				}
				else // pas de sim ou equipement associé
				{
					tncSite.HB_down.enabled=false; //disable la partie inférieure de l'onglet site
					tncSite.txtMsgSite.text=resourceManager.getString('M111', 'Msg_indication_selection_site_sans_equipement');
				}
			}
		}

		private function majInfosOngletAboOption():void
		{
			// maj onglet Abo et options
			if (tncAboOption != null && tncAboOption.dataDatagrid != null)
			{
				tncAboOption.dataDatagrid=myServiceLigne.myDatas.ligne.listeAboOption;

				var spacerAboOpt:Spacer=new Spacer();
				spacerAboOpt.percentWidth=100;

				var labelStatut:Label=new Label();
				labelStatut.text=ResourceManager.getInstance().getString('M111', 'Statut__');

				var labelFiltre:Label=new Label();
				labelFiltre.text=ResourceManager.getInstance().getString('M111', '__derniere_fact') + ":";

				var labelcmd:Label=new Label();
				labelcmd.text=ResourceManager.getInstance().getString('M111', 'cmd_en_cours__');

				var hbox:HBox=new HBox();
				hbox.setStyle("backgroundColor", "0xFEC46D");
				hbox.addChild(labelcmd);

				var labelLegende:Label=new Label();
				labelLegende.text=ResourceManager.getInstance().getString('M111', 'L_gende__');

				var imgLegende:Image=new Image();
				imgLegende.source=myIconProduitAcces;

				var labelLegende2:Label=new Label();
				labelLegende2.text=ResourceManager.getInstance().getString('M111', 'Produit_d_acc_s');

				cmbfiltre=new ComboBox();
				cmbfiltre.addEventListener(Event.CHANGE, filtreGridAboOpt);
				cmbfiltre.dataProvider=[{data: 0, label: "---"}, {data: 1, label: ResourceManager.getInstance().getString('M111', 'Oui')}, {data: 2, label: ResourceManager.getInstance().getString('M111', 'Non')}];

				tncAboOption.headerLabel=myServiceLigne.myDatas.ligne.listeAboOption.length.toString() + ResourceManager.getInstance().getString('M111', '_abonnements_et_options');
				tncAboOption.hb_top.addChild(spacerAboOpt);
				tncAboOption.hb_top.addChild(labelStatut);
				tncAboOption.hb_top.addChild(hbox);
				tncAboOption.hb_top.addChild(labelLegende);
				tncAboOption.hb_top.addChild(imgLegende);
				tncAboOption.hb_top.addChild(labelLegende2);
				tncAboOption.hb_top1.percentWidth=100;
				tncAboOption.hb_top1.addChild(labelFiltre);
				tncAboOption.hb_top1.addChild(cmbfiltre);
			}
		}

		private function majInfosOngletContrat():void
		{
			// maj onglet contrat opérateur
			if (tncContratOperateur != null && tncContratOperateur.lbOperateur != null)
			{
				tncContratOperateur.lbOperateur.text=myServiceLigne.myDatas.ligne.operateur.nom;

				switch (myServiceLigne.myDatas.ligne.dureeContrat)
				{
					case 12:
						tncContratOperateur.cbDureeContrat.selectedIndex=0;
						break;
					case 18:
						tncContratOperateur.cbDureeContrat.selectedIndex=1;
						break;
					case 24:
						tncContratOperateur.cbDureeContrat.selectedIndex=2;
						break;
					case 36:
						tncContratOperateur.cbDureeContrat.selectedIndex=3;
						break;
					case 48:
						tncContratOperateur.cbDureeContrat.selectedIndex=4;
						break;
				}

				tncContratOperateur.dcDateOuverture.selectedDate=myServiceLigne.myDatas.ligne.dateOuverture;
				tncContratOperateur.dcDateRenouvellement.selectedDate=myServiceLigne.myDatas.ligne.dateRenouvellement;
				tncContratOperateur.dcDateResiliation.selectedDate=myServiceLigne.myDatas.ligne.dateResiliation;
				tncContratOperateur.cbDureeEligibilite.selectedIndex=myServiceLigne.myDatas.ligne.dureeEligibilite - 1;
				tncContratOperateur.lbDateEligibilite.text=DateFunction.formatDateAsString(myServiceLigne.myDatas.ligne.dateEligibilite);
				tncContratOperateur.lbDateFpc.text=DateFunction.formatDateAsString(myServiceLigne.myDatas.ligne.dateFPC);
				tncContratOperateur.lbDateLastFacture.text=DateFunction.formatDateAsString(myServiceLigne.myDatas.ligne.dateDerniereFacture);
			}
		}

		private function majInfosOngletDivers():void
		{
			// maj onglet DIVERS
			if (tncDivers != null && tncDivers.fi1 != null)
			{
				tncDivers.fi1.label=myServiceLigne.myDatas.ligne.libelleChampPerso1;
				tncDivers.fi2.label=myServiceLigne.myDatas.ligne.libelleChampPerso2;
				tncDivers.fi3.label=myServiceLigne.myDatas.ligne.libelleChampPerso3;
				tncDivers.fi4.label=myServiceLigne.myDatas.ligne.libelleChampPerso4;
				tncDivers.champ1=myServiceLigne.myDatas.ligne.texteChampPerso1;
				tncDivers.champ2=myServiceLigne.myDatas.ligne.texteChampPerso2;
				tncDivers.champ3=myServiceLigne.myDatas.ligne.texteChampPerso3;
				tncDivers.champ4=myServiceLigne.myDatas.ligne.texteChampPerso4;
			}
		}

		private function majInfosOngletRaccordement():void
		{
			if (tncRaccordement != null && tncRaccordement.dataDatagrid != null)
			{
				tncRaccordement.dataDatagrid=myServiceLigne.myDatas.ligne.listeHistoRaccor;
			}
		}

		private function majInfosOngletCanauxGroupement():void
		{
			if (tncCanauxGroupment != null && tncCanauxGroupment.rbLigneGrpmentLigneKO != null)
			{
				tncCanauxGroupment.myServiceLigne=myServiceLigne;
				tncCanauxGroupment.setInfos();
			}
		}

		private function majInfosOngletSDA():void
		{
			if (tncCablageSdaT2T0Ana != null && tncCablageSdaT2T0Ana.tiAmorce != null)
			{
				tncCablageSdaT2T0Ana.myServiceLigne=myServiceLigne;
				tncCablageSdaT2T0Ana.setInfos();
			}
			else if (tncCablageSdaPointaPoint != null && tncCablageSdaPointaPoint.tiAdresseA != null)
			{
				tncCablageSdaPointaPoint.myServiceLigne=myServiceLigne;
				tncCablageSdaPointaPoint.setInfos();
			}
			else if (tncCablageSdaXdsl != null && tncCablageSdaXdsl.tiAdresseA != null)
			{
				tncCablageSdaXdsl.myServiceLigne=myServiceLigne;
				tncCablageSdaXdsl.setInfos();
			}

		}

		private function infoFicheLigneLoadedHandler(event:gestionparcEvent):void
		{
			myServiceLigne.myDatas.removeEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_LOADED, infoFicheLigneLoadedHandler);
			//trace("--->>>données fiche ligne chargées");
			myServiceLigne.myDatas.removeEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_LOADED, infoFicheLigneLoadedHandler);

			majInfoFicheLigne();
			majDisplayOnglet(myServiceLigne.myDatas.ligne.TYPE_FICHELIGNE); //gerer display des onglets avec les infos de la ligne
			majInfoOnglet(); //mettre les infos à jour dans les onglets
		}

		private function tncAboOptiongestionparcEventAjouterEltNavHandler(event:gestionparcEvent):void
		{
			var popupAboOpt:PopUpAboOptIHM=new PopUpAboOptIHM();
			popupAboOpt.addEventListener(gestionparcEvent.POPUP_ABO_OPT_VALIDATED, updateGridAboOpt);
			popupAboOpt.myServices=myServiceLigne;
			popupAboOpt.idOperateur=myServiceLigne.myDatas.ligne.operateur.idOperateur;
			popupAboOpt.idProfilEquipement=myServiceLigne.myDatas.ligne.idTypeLigne;

			if (tncAboOption.dataDatagrid.length > 0)
				popupAboOpt.isAbonnement=true;

			PopUpManager.addPopUp(popupAboOpt, this, true);
			PopUpManager.centerPopUp(popupAboOpt);
		}

		private function updateGridAboOpt(event:gestionparcEvent):void
		{
			myDataProviderAboOpt=new ArrayCollection();

			// pour l'abonnement
			for each (var obj:* in myServiceLigne.myDatas.listeAbo)
			{
				if (obj.SELECTED)
				{
					// creation d'un attribut "libelleProduit" pour l'affichage dans le tableau
					obj.libelleProduit=obj.LIBELLE_PRODUIT;
					// creation d'un attribut "operateur" pour l'affichage dans le tableau 
					// cet attribut est inutil pour les lignes fixe car toujours la meme valeur mais cela sera different pour les lignes fixes
					obj.operateur=myServiceLigne.myDatas.ligne.operateur.nom;
					myDataProviderAboOpt.addItem(obj);
					lbAboPrincipal.text=obj.LIBELLE_PRODUIT;
				}
			}
			// pour les options
			for each (var obj2:* in myServiceLigne.myDatas.listeOpt)
			{
				if (obj2.SELECTED)
				{
					// creation d'un attribut "libelleProduit" pour l'affichage dans le tableau
					obj2.libelleProduit=obj2.LIBELLE_PRODUIT;
					// creation d'un attribut "operateur" pour l'affichage dans le tableau 
					// cet attribut est inutil pour les lignes fixe car toujours la meme valeur mais cela sera different pour les lignes fixes
					obj2.operateur=myServiceLigne.myDatas.ligne.operateur.nom;
					myDataProviderAboOpt.addItem(obj2);
				}
			}
			tncAboOption.dataDatagrid=myDataProviderAboOpt;
			tncAboOption.dataDatagrid.refresh();

		}

		private function filtreGridAboOpt(event:Event):void
		{
			var listeAboOpt:ArrayCollection=new ArrayCollection();

			if (cmbfiltre.selectedItem.data == 0)
				listeAboOpt=myServiceLigne.myDatas.ligne.listeAboOption;
			else if (cmbfiltre.selectedItem.data == 1)
			{
				for (var i:int=0; i < myServiceLigne.myDatas.ligne.listeAboOption.length; i++)
				{
					if (myServiceLigne.myDatas.ligne.listeAboOption[i].boolFacture == 1)
						listeAboOpt.addItem(myServiceLigne.myDatas.ligne.listeAboOption.getItemAt(i));
				}
			}
			else
			{
				for (var j:int=0; j < myServiceLigne.myDatas.ligne.listeAboOption.length; j++)
				{
					if (myServiceLigne.myDatas.ligne.listeAboOption[j].boolFacture != 1)
						listeAboOpt.addItem(myServiceLigne.myDatas.ligne.listeAboOption.getItemAt(j));
				}
			}
			tncAboOption.dataDatagrid=listeAboOpt;
			tncAboOption.dataDatagrid.refresh();

		}

		private function listeOragLoadedHandler(event:gestionparcEvent):void
		{
			myServiceLigne.myDatas.ligne=new LigneVO();
			myServiceLigne.myDatas.ligne.listeOrganisation=CacheDatas.listeOrga;
			myServiceOrga.myDatas.listeOrga=CacheDatas.listeOrga;

			cbOrganisation.dataProvider=myServiceLigne.myDatas.ligne.listeOrganisation;
		}

		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validClickHandler(event:MouseEvent):void
		{
			// test les validators
			if (runValidators())
			{
				myServiceLigne.setInfosFicheLigne(createObjToServiceSet());
				myServiceLigne.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED, ficheLigneUploadHandler);
			}
		}

		private function createObjToServiceSet():Object
		{
			var obj:Object=new Object();

			setInfoLigneVO();
			if (tiSousTete.text != oldNumero)
				obj.BOOLNUMBERHASCHANGED=1;
			else
				obj.BOOLNUMBERHASCHANGED=0;

			obj.oldNumero=oldNumero;
			obj.myLigne=myServiceLigne.myDatas.ligne;
			obj.xmlListeAboOpt=createXmlListeAboOpt();
			obj.xmlListeOrga=createXmlListeOrga();
			obj.xmlListeCablageCanaux=createXmlCablageCanaux();

			return obj;
		}

		private function ficheLigneUploadHandler(event:gestionparcEvent):void
		{
			SpecificationVO.getInstance().refreshData();
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_LIGNE_UPLOADED));
		}

		private function setInfoLigneVO():void
		{
			if (cbFonction.selectedItem)
			{
				if (cbFonction.selectedItem.hasOwnProperty("USAGE")) // c'est une ancienne fonction
					myServiceLigne.myDatas.ligne.fonction=cbFonction.selectedItem.USAGE;
				else // c'est une nouvelle fonction
					myServiceLigne.myDatas.ligne.fonction=(cbFonction.selectedItem as String);
			}

			if (cbTypeLigne.selectedIndex > -1)
			{
				myServiceLigne.myDatas.ligne.typeLigne=cbTypeLigne.selectedItem.TYPE_FICHELIGNE;
				myServiceLigne.myDatas.ligne.idTypeLigne=cbTypeLigne.selectedItem.IDTYPE_LIGNE;
			}

			myServiceLigne.myDatas.ligne.sousTete=tiSousTete.text;
			myServiceLigne.myDatas.ligne.payeur=(cbPayeur.selectedItem) ? cbPayeur.selectedItem.data : 0;
			myServiceLigne.myDatas.ligne.titulaire=(cbTitulaire.selectedItem) ? cbTitulaire.selectedItem.data : 0;
			myServiceLigne.myDatas.ligne.codeRio=tiRio.text;
			myServiceLigne.myDatas.ligne.commentaires=taCommentaires.text;

			if (tncSite != null && tncSite.lbEmplacement != null)
			{
			}
			else
			{
				myServiceLigne.myDatas.ligne.idSite=-1;
				myServiceLigne.myDatas.ligne.idEmplacement=-1;
			}
			if (tncDivers != null && tncDivers.ti_chp1 != null)
			{
				myServiceLigne.myDatas.ligne.texteChampPerso1=tncDivers.ti_chp1.text;
				myServiceLigne.myDatas.ligne.texteChampPerso2=tncDivers.ti_chp2.text;
				myServiceLigne.myDatas.ligne.texteChampPerso3=tncDivers.ti_chp3.text;
				myServiceLigne.myDatas.ligne.texteChampPerso4=tncDivers.ti_chp4.text;
				myServiceLigne.myDatas.ligne.numContrat=tiNumContrat.text;
				myServiceLigne.myDatas.ligne.etat=(cbEtat.selectedItem) ? cbEtat.selectedItem.data : 0;
			}
			else
			{
				myServiceLigne.myDatas.ligne.texteChampPerso1="";
				myServiceLigne.myDatas.ligne.texteChampPerso2="";
				myServiceLigne.myDatas.ligne.texteChampPerso3="";
				myServiceLigne.myDatas.ligne.texteChampPerso4="";
				myServiceLigne.myDatas.ligne.numContrat="";
				myServiceLigne.myDatas.ligne.etat=0;
			}
			if (tncContratOperateur != null && tncContratOperateur.cbDureeContrat != null)
			{
				myServiceLigne.myDatas.ligne.dureeContrat=(tncContratOperateur.cbDureeContrat.selectedItem) ? tncContratOperateur.cbDureeContrat.selectedItem.value : 0;
				myServiceLigne.myDatas.ligne.dateOuverture=(tncContratOperateur.dcDateOuverture.selectedDate) ? tncContratOperateur.dcDateOuverture.selectedDate : null;
				myServiceLigne.myDatas.ligne.dateRenouvellement=(tncContratOperateur.dcDateRenouvellement.selectedDate) ? tncContratOperateur.dcDateRenouvellement.selectedDate : null;
				myServiceLigne.myDatas.ligne.dateResiliation=(tncContratOperateur.dcDateResiliation.selectedDate) ? tncContratOperateur.dcDateResiliation.selectedDate : null;
				myServiceLigne.myDatas.ligne.dureeEligibilite=(tncContratOperateur.cbDureeEligibilite.selectedItem) ? tncContratOperateur.cbDureeEligibilite.selectedItem.value : 0;
				myServiceLigne.myDatas.ligne.dateFPC=(tncContratOperateur.lbDateFpc.text) ? DateField.stringToDate(tncContratOperateur.lbDateFpc.text, 'DD/MM/YYYY') : null;
			}
			else
			{
				myServiceLigne.myDatas.ligne.dureeContrat=0;
				myServiceLigne.myDatas.ligne.dateOuverture=null;
				myServiceLigne.myDatas.ligne.dateRenouvellement=null;
				myServiceLigne.myDatas.ligne.dateResiliation=null;
				myServiceLigne.myDatas.ligne.dureeEligibilite=0;
				myServiceLigne.myDatas.ligne.dateFPC=null;
			}
		}

		private function createXmlListeOrga():String
		{
			var tmpListeOrga:ArrayCollection=new ArrayCollection();
			var lenListeOrga:int=myServiceLigne.myDatas.ligne.listeOrganisation.length;
			var myXML:String="";

			// trier la liste des orga par date de modification (pour pas changer procedure dba)
			for (var j:int=0; j < lenListeOrga; j++)
			{
				if (tmpListeOrga.length == 0)
				{
					tmpListeOrga.addItem((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO));
				}
				else
				{
					var index:int=0;
					var longueur:int=tmpListeOrga.length;

					if ((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO).dateModifPosition)
					{
						// tant la date de l'item de tmpListeOrga est plus recente que la date de l'item de listeOrganisation
						while ((index < longueur) && ((tmpListeOrga[index] as OrgaVO).dateModifPosition) && ((tmpListeOrga[index] as OrgaVO).dateModifPosition.getTime() > (myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO).dateModifPosition.getTime()))
						{
							index++;
						}
						tmpListeOrga.addItemAt((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO), index);
					}
					else
					{
						index++;
						tmpListeOrga.addItemAt((myServiceLigne.myDatas.ligne.listeOrganisation[j] as OrgaVO), index);
					}
				}
			}

			// création de l'XML
			myXML+="<Orgas>";
			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((tmpListeOrga[i] as OrgaVO).chemin != null && (tmpListeOrga[i] as OrgaVO).idCible != 0)
				{
					myXML+="<Orga>";
					myXML+="<idOrga><![CDATA[" + (tmpListeOrga[i] as OrgaVO).idOrga + "]]></idOrga>"
					myXML+="<chemin><![CDATA[" + (tmpListeOrga[i] as OrgaVO).chemin + "]]></chemin>";
					myXML+="<idCible><![CDATA[" + (tmpListeOrga[i] as OrgaVO).idCible + "]]></idCible>";
					myXML+="</Orga>";
				}
			}
			myXML+="</Orgas>";

			if (myXML != "<Orgas></Orgas>")
				return myXML;
			else
				return "";
		}

		private function createXmlListeAboOpt():String
		{
			var myXML:String="";

			var lenListeAboOpt:int=myServiceLigne.myDatas.ligne.listeAboOption.length;

			myXML+="<aboOpts>";
			for (var i:int=0; i < lenListeAboOpt; i++)
			{
				if ((myServiceLigne.myDatas.ligne.listeAboOption[i].dansInventaire == "1" && myServiceLigne.myDatas.ligne.listeAboOption[i].aSortir == 1) || (myServiceLigne.myDatas.ligne.listeAboOption[i].dansInventaire == "0" && myServiceLigne.myDatas.ligne.listeAboOption[i].aEntrer == 1))
				{
					myXML+="<aboOpt>";
					myXML+="<idInventaire>" + myServiceLigne.myDatas.ligne.listeAboOption[i].idInventaireProduit + "</idInventaire>"
					myXML+="<boolInInventaire>" + myServiceLigne.myDatas.ligne.listeAboOption[i].dansInventaire + "</boolInInventaire>";
					myXML+="<aEntrer>" + myServiceLigne.myDatas.ligne.listeAboOption[i].aEntrer + "</aEntrer>";
					myXML+="<aSortir>" + myServiceLigne.myDatas.ligne.listeAboOption[i].aSortir + "</aSortir>";
					myXML+="<date>" + DateField.dateToString(myServiceLigne.myDatas.ligne.listeAboOption[i].date, 'YYYY/MM/DD') + "</date>";
					myXML+="</aboOpt>";

				}
			}
			myXML+="</aboOpts>";

			if (myXML != "<aboOpts></aboOpts>")
				return myXML;
			else
				return "";
		}

		private function createXmlCablageCanaux():String
		{
			var myXML:String="<element>";
			if (tncCaracteristique && tncCaracteristique.tiDescendantContrat != null)
			{
				myXML+="<caracteristique>";
				myXML+="<nom_reseau>" + tncCaracteristique.tiNomReseau.text + "</nom_reseau>";
				myXML+="<usage>" + tncCaracteristique.getUsageValue() + "</usage>";
				myXML+="<debitMontant_Unite>" + tncCaracteristique.cbMontantUnite.selectedItem.VALUE + "</debitMontant_Unite>";
				myXML+="<debitMontant_Contrat>" + tncCaracteristique.tiMontantContrat.text + "</debitMontant_Contrat>";
				myXML+="<debitMontant_Max>" + tncCaracteristique.tiMontantDebitMax.text + "</debitMontant_Max>";
				myXML+="<debitDescendant_Unite>" + tncCaracteristique.cbDescendantUnite.selectedItem.VALUE + "</debitDescendant_Unite>";
				myXML+="<debitDescendant_Contrat>" + tncCaracteristique.tiDescendantContrat.text + "</debitDescendant_Contrat>";
				myXML+="<debitDescendant_Max>" + tncCaracteristique.tiDescendantDebitMax.text + "</debitDescendant_Max>";
				myXML+="<GTR>" + tncCaracteristique.tiGTRContractuel.text + "</GTR>";
				myXML+="<presentation>" + tncCaracteristique.tiPresentation.text + "</presentation>";
				myXML+="<protocole_physique>" + tncCaracteristique.tiProtocolePhysique.text + "</protocole_physique>";
				myXML+="<protocole_liaison>" + tncCaracteristique.tiProtocoleLiaison.text + "</protocole_liaison>";
				myXML+="<protocole_reseau>" + tncCaracteristique.tiProtocoleReseau.text + "</protocole_reseau>";
				myXML+="</caracteristique>";
			}
			if (tncCablageSdaPointaPoint && tncCablageSdaPointaPoint.tiElementActifA != null)
			{
				myXML+="<cablage_point_a_point>";
				myXML+="<element_actif_PA>" + tncCablageSdaPointaPoint.tiElementActifA.text + "</element_actif_PA>";
				myXML+="<element_actif_amorce_emission_PA>" + tncCablageSdaPointaPoint.tiElementActifEmissionA.text + "</element_actif_amorce_emission_PA>";
				myXML+="<element_actif_amorce_reception_PA>" + tncCablageSdaPointaPoint.tiElementActifReceptionA.text + "</element_actif_amorce_reception_PA>";
				myXML+="<amorce_tete_emission_PA>" + tncCablageSdaPointaPoint.tiAmorceTeteEmissionA.text + "</amorce_tete_emission_PA>";
				myXML+="<amorce_tete_reception_PA>" + tncCablageSdaPointaPoint.tiAmorceTeteReceptionA.text + "</amorce_tete_reception_PA>";
				myXML+="<adresse_PA>" + tncCablageSdaPointaPoint.tiAdresseA.text + "</adresse_PA>";
				myXML+="<code_postal_PA>" + tncCablageSdaPointaPoint.tiCodePostalA.text + "</code_postal_PA>";
				myXML+="<commune_PA>" + tncCablageSdaPointaPoint.tiCommuneA.text + "</commune_PA>";
				myXML+="<element_actif_PB>" + tncCablageSdaPointaPoint.tiElementActifB.text + "</element_actif_PB>";
				myXML+="<element_actif_amorce_emission_PB>" + tncCablageSdaPointaPoint.tiElementActifEmissionB.text + "</element_actif_amorce_emission_PB>";
				myXML+="<element_actif_amorce_reception_PB>" + tncCablageSdaPointaPoint.tiElementActifReceptionB.text + "</element_actif_amorce_reception_PB>";
				myXML+="<amorce_tete_emission_PB>" + tncCablageSdaPointaPoint.tiAmorceTeteEmissionB.text + "</amorce_tete_emission_PB>";
				myXML+="<amorce_tete_reception_PB>" + tncCablageSdaPointaPoint.tiAmorceTeteReceptionB.text + "</amorce_tete_reception_PB>";
				myXML+="<adresse_PB>" + tncCablageSdaPointaPoint.tiAdresseB.text + "</adresse_PB>";
				myXML+="<code_postal_PB>" + tncCablageSdaPointaPoint.tiCodePostalB.text + "</code_postal_PB>";
				myXML+="<commune_PB>" + tncCablageSdaPointaPoint.tiCommuneB.text + "</commune_PB>";
				myXML+="<identifiant_LL_PB>" + tncCablageSdaPointaPoint.tiIdentifiantLLB.text + "</identifiant_LL_PB>";
				myXML+="<identifiant_LL_PA>" + tncCablageSdaPointaPoint.tiIdentifiantLLA.text + "</identifiant_LL_PA>";
				myXML+="<nature>" + tncCablageSdaPointaPoint.tiNature.text + "</nature>";
				myXML+="<debit>" + tncCablageSdaPointaPoint.tiDebit.text + "</debit>";
				myXML+="<distance>" + tncCablageSdaPointaPoint.tiDistance.text + "</distance>";
				myXML+="</cablage_point_a_point>";
			}
			if (tncCablageSdaXdsl && tncCablageSdaXdsl.tiElementActifA != null)
			{
				myXML+="<cablage_point_a_point>";
				myXML+="<element_actif_PA>" + tncCablageSdaXdsl.tiElementActifA.text + "</element_actif_PA>";
				myXML+="<element_actif_amorce_emission_PA>" + tncCablageSdaXdsl.tiElementActifEmissionA.text + "</element_actif_amorce_emission_PA>";
				myXML+="<element_actif_amorce_reception_PA>" + tncCablageSdaXdsl.tiElementActifReceptionA.text + "</element_actif_amorce_reception_PA>";
				myXML+="<amorce_tete_emission_PA>" + tncCablageSdaXdsl.tiAmorceTeteEmissionA.text + "</amorce_tete_emission_PA>";
				myXML+="<amorce_tete_reception_PA>" + tncCablageSdaXdsl.tiAmorceTeteReceptionA.text + "</amorce_tete_reception_PA>";
				myXML+="<adresse_PA>" + tncCablageSdaXdsl.tiAdresseA.text + "</adresse_PA>";
				myXML+="<code_postal_PA>" + tncCablageSdaXdsl.tiCodePostalA.text + "</code_postal_PA>";
				myXML+="<commune_PA>" + tncCablageSdaXdsl.tiCommuneA.text + "</commune_PA>";
				myXML+="<identifiant_LL_PA>" + tncCablageSdaXdsl.tiIdentifiantLLA.text + "</identifiant_LL_PA>";
				myXML+="<nature>" + tncCablageSdaXdsl.tiNature.text + "</nature>";
				myXML+="<debit>" + tncCablageSdaXdsl.tiDebit.text + "</debit>";
				myXML+="<distance>" + tncCablageSdaXdsl.tiDistance.text + "</distance>";
				myXML+="</cablage_point_a_point>";
			}
			if (tncCablageSdaT2T0Ana && tncCablageSdaT2T0Ana.tiAmorce != null)
			{
				myXML+="<cablage_T2_T0_ANA>";
				myXML+="<cablage>";
				myXML+="<amorce>" + tncCablageSdaT2T0Ana.tiAmorce.text + "</amorce>";
				myXML+="<reglette1>" + tncCablageSdaT2T0Ana.tiReglette1.text + "</reglette1>";
				myXML+="<paire1>" + tncCablageSdaT2T0Ana.tiPaire1.text + "</paire1>";
				myXML+="<reglette2>" + tncCablageSdaT2T0Ana.tiReglette2.text + "</reglette2>";
				myXML+="<paire2>" + tncCablageSdaT2T0Ana.tiPaire2.text + "</paire2>";
				myXML+="</cablage>";
				myXML+="<tranches>";

				var lenDgSda:int=(tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection).length;

				for (var i:int=0; i < lenDgSda; i++)
				{
					myXML+="<tranche>";
					myXML+="<libelle>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].libelle + "</libelle>";
					myXML+="<type_tranche>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].typeTranche + "</type_tranche>";
					myXML+="<nombre_de_num>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].nbNum + "</nombre_de_num>";
					myXML+="<num_de_depart>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].numDepart + "</num_de_depart>";
					myXML+="<num_de_fin>" + (tncCablageSdaT2T0Ana.dgSDA.dataProvider as ArrayCollection)[i].numFin + "</num_de_fin>";
					myXML+="</tranche>";
				}
				myXML+="</tranches>";
				myXML+="</cablage_T2_T0_ANA>";
			}
			if (tncCanauxGroupment && tncCanauxGroupment.tiCanauxMixteDebut != null)
			{
				myXML+="<canaux_groupement>";
				myXML+="<specialisation_canaux>";
				myXML+="<canaux_mix_debut>" + tncCanauxGroupment.tiCanauxMixteDebut.text + "</canaux_mix_debut>";
				myXML+="<canaux_mix_fin>" + tncCanauxGroupment.tiCanauxMixteFin.text + "</canaux_mix_fin>";
				myXML+="<canaux_entrants_debut>" + tncCanauxGroupment.tiCanauxEntrantsDebut.text + "</canaux_entrants_debut>";
				myXML+="<canaux_entrants_fin>" + tncCanauxGroupment.tiCanauxEntrantsFin.text + "</canaux_entrants_fin>";
				myXML+="<canaux_sortants_debut>" + tncCanauxGroupment.tiCanauxSortantsDebut.text + "</canaux_sortants_debut>";
				myXML+="<canaux_sortants_fin>" + tncCanauxGroupment.tiCanauxSortantsFin.text + "</canaux_sortants_fin>";
				myXML+="<nbr_canaux>" + tncCanauxGroupment.lbNbCanaux.text + "</nbr_canaux>";
				myXML+="</specialisation_canaux>";
				myXML+="<groupement>";
				myXML+="<ligne_appartient_a_groupement>" + (tncCanauxGroupment.rbLigneGrpmentLigneOK.selected) ? 1 : 0 + "</ligne_appartient_a_groupement>";
				myXML+="<is_tete_de_ligne>" + (tncCanauxGroupment.cbxTeteLigne.selected) ? 1 : 0 + "</is_tete_de_ligne>";
				if ((tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection) != null && (tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection).length > 0)
					myXML+="<id_sous_tete_tete_de_ligne>" + (tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection)[0].idTeteLigne + "</id_sous_tete_tete_de_ligne>";
				else
					myXML+="<id_sous_tete_tete_de_ligne></id_sous_tete_tete_de_ligne>";
				myXML+="<lignes_du_groupement>";

				if ((tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection) != null && (tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection).length > 0)
				{
					var lenDgGrpment:int=(tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection).length;

					for (var j:int=1; j < lenDgGrpment; j++)
					{
						myXML+="<ligne>";
						myXML+="<id_sous_tete>" + (tncCanauxGroupment.dgLigneGrpement.dataProvider as ArrayCollection)[j].idTeteLigne + "</id_sous_tete>";
						myXML+="</ligne>";
					}
				}

				myXML+="</lignes_du_groupement>";
				myXML+="</groupement>";
				myXML+="</canaux_groupement>";
			}
			myXML+="</element>";

			return myXML;
		}

		public function clickModifierOrgaHandler(event:MouseEvent):void
		{
			ficheOrga=new FicheOrgaIHM();
			ficheOrga.libelleOrga=(cbOrganisation.selectedItem as OrgaVO).libelleOrga;
			ficheOrga.idOrga=(cbOrganisation.selectedItem as OrgaVO).idOrga;
			ficheOrga.addEventListener(gestionparcEvent.VALIDE_FICHE_ORGA, updateOrga);
			PopUpManager.addPopUp(ficheOrga, this, true);
			PopUpManager.centerPopUp(ficheOrga);
		}

		private function changeOrgaHandler(event:ListEvent):void
		{
			if (cbOrganisation.selectedItem && (cbOrganisation.selectedItem as OrgaVO).position == 1)
				lbPosition.text=(cbOrganisation.selectedItem as OrgaVO).chemin;
			else
				lbPosition.text="";
		}

		private function updateOrga(event:gestionparcEvent):void
		{
			var lenListeOrga:int=myServiceLigne.myDatas.ligne.listeOrganisation.length;

			for (var i:int=0; i < lenListeOrga; i++)
			{
				if ((myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).idOrga == ficheOrga.myService.myDatas.cheminSelected.idOrga)
				{
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).idCible=ficheOrga.myService.myDatas.cheminSelected.idFeuille;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).chemin=ficheOrga.myService.myDatas.cheminSelected.chemin;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).position=1;
					(myServiceLigne.myDatas.ligne.listeOrganisation[i] as OrgaVO).dateModifPosition=new Date();
				}
			}
			lbPosition.text=ficheOrga.myService.myDatas.cheminSelected.chemin;
		}

		//
		private function displayHistoriqueHandler(evt:gestionparcEvent):void
		{
			var ficheHistorique:FicheHistoriqueIHM=new FicheHistoriqueIHM();

			ficheHistorique.idRessource=evt.obj.idInventaireProduit;

			PopUpManager.addPopUp(ficheHistorique,Application.application as DisplayObject,true);
			PopUpManager.centerPopUp(ficheHistorique);
		}

		// handlers click image
		private function imgSearchFonctionClickHandler(me:MouseEvent):void
		{
			_popupSearchFonction=new PopUpSearchFonctionIHM();

			_popupSearchFonction.setFonctions(CacheDatas.listeUsages as ArrayCollection, this.cbFonction.selectedItem);

			PopUpManager.addPopUp(_popupSearchFonction, this, true);
			PopUpManager.centerPopUp(_popupSearchFonction);

			_popupSearchFonction.addEventListener('GET_SELECTED_FONCTION_VALIDEE', getFonctionSelectedHandler);
		}

		private function imgEditFonctionClickHandler(me:MouseEvent):void
		{
			var popupUsage:PopUpUsageIHM=new PopUpUsageIHM();
			if (cbFonction.selectedItem)
			{
				if (cbFonction.selectedItem.hasOwnProperty("USAGE")) // c'est une ancienne fonction
				{
					popupUsage.oldUsage=cbFonction.selectedItem.USAGE;
				}
				else // c'est une nouvelle fonction
				{
					popupUsage.oldUsage=(cbFonction.selectedItem as String);
					strOldUsage=(cbFonction.selectedItem as String);
				}
			}
			popupUsage.addEventListener(gestionparcEvent.POPUP_USAGE_VALIDED, addUsage);
			PopUpManager.addPopUp(popupUsage, this, true);
			PopUpManager.centerPopUp(popupUsage);
		}

		//
		private function getFonctionSelectedHandler(e:Event):void
		{
			removeEventListener('GET_SELECTED_FONCTION_VALIDEE', getFonctionSelectedHandler);

			var selectedFonction:Object=_popupSearchFonction.getFonctions();
			var listeFonctions:ArrayCollection=CacheDatas.listeUsages;

			for (var i:int=0; i < listeFonctions.length; i++)
			{
				if (listeFonctions[i].USAGE == selectedFonction.USAGE && listeFonctions[i].SELECTED == selectedFonction.SELECTED)
				{
					cbFonction.selectedIndex=i;
					break
				}
			}
			PopUpManager.removePopUp(_popupSearchFonction);
		}

		//
		private function addUsage(evt:gestionparcEvent):void
		{
			if (strOldUsage) // si ce n'est pas le premiere edition de fonction
			{
				var lenListFonction:int=(cbFonction.dataProvider as ArrayCollection).length;

				for (var i:int=0; i < lenListFonction; i++)
				{
					if (cbFonction.dataProvider[i] == strOldUsage)
					{
						(cbFonction.dataProvider as ArrayCollection).removeItemAt(i);
						break;
					}
				}
			}
			var objUsage :Object=new Object();
			objUsage.USAGE=evt.obj;
			(cbFonction.dataProvider as ArrayCollection).addItem(objUsage);
			cbFonction.selectedIndex=(cbFonction.dataProvider as ArrayCollection).length - 1;
		}

		private function cacheListeRevendeurProfilEqptLoadedHandler(event:gestionparcEvent):void
		{
			cbDistributeur.dataProvider=CacheDatas.listeRevendeurForProfEqpt;
			cbDistributeur.selectedIndex=0;
		}

		private function cacheInfoOpLoadedHandler(event:gestionparcEvent):void
		{
			if (CacheDatas.listeInfosOperateur)
			{
				var dureeEligibilite:int=CacheDatas.listeInfosOperateur[0].DUREE_ELLIGIBILITE;
				reinitiliseContratOperateur(dureeEligibilite - 1); // -1 car l'index commence de 0
			}
		}

		private function reinitiliseAboOpt():void
		{
			myServiceLigne.myDatas.listeAbo=new ArrayCollection();
			myServiceLigne.myDatas.listeAboFavoris=new ArrayCollection();
			myServiceLigne.myDatas.listeOpt=new ArrayCollection();
			myServiceLigne.myDatas.listeOptFavoris=new ArrayCollection();

			tncAboOption.dataDatagrid.removeAll();
		}

		private function reinitiliseContratOperateur(dureeEligibilite:int=3):void
		{
			tncContratOperateur.lbOperateur.text=myServiceLigne.myDatas.ligne.operateur.nom;
			tncContratOperateur.dcDateOuverture.selectedDate=new Date();
			tncContratOperateur.cbDureeContrat.selectedIndex=2; // 24 mois
			tncContratOperateur.cbDureeEligibilite.selectedIndex=dureeEligibilite;
			if (tncContratOperateur.cbDureeEligibilite.selectedIndex != -1)
			{
				tncContratOperateur.lbDateEligibilite.text=DateFunction.formatDateAsString(DateFunction.dateAdd("m", new Date(), tncContratOperateur.cbDureeEligibilite.selectedIndex));
			}
			if (tncContratOperateur.cbDureeContrat.selectedIndex != -1)
			{
				tncContratOperateur.lbDateFpc.text=DateFunction.formatDateAsString(DateFunction.dateAdd("m", new Date(), tncContratOperateur.cbDureeContrat.selectedIndex));
			}
		}

		private function onInfosFicheSiteUploadedHandler(event:gestionparcEvent):void
		{
			CacheServices.getInstance().getSiteLivraison(); // récuperer à nouveau la liste des sites
			CacheServices.getInstance().myDatas.addEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
		}

		private function onListeSiteLoadedhandler(evt:gestionparcEvent):void
		{
			CacheServices.getInstance().myDatas.removeEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
			if (myServiceLigne.myDatas.ligne.idSite > 0)
			{
				var longueurTabSite:int=CacheDatas.siteLivraison.length;
				for (var k:int=0; k < longueurTabSite; k++)
				{
					if ((CacheDatas.siteLivraison[k] as SiteVO).idSite == myServiceLigne.myDatas.ligne.idSite)
					{
						tncSite.lbSite.text=(CacheDatas.siteLivraison[k] as SiteVO).libelleSite;

						tncSite.txtAdresse.text=(CacheDatas.siteLivraison[k] as SiteVO).adresse1;
						tncSite.lbCodePostal.text=(CacheDatas.siteLivraison[k] as SiteVO).codePostale;
						tncSite.lbVille.text=(CacheDatas.siteLivraison[k] as SiteVO).ville;
						tncSite.lbPays.text=(CacheDatas.siteLivraison[k] as SiteVO).pays;
						break;
					}
				}
				if (myServiceLigne.myDatas.ligne.idEmplacement > 0)
				{
					//tncSite.majInfosEmplacement(myServiceLigne.myDatas.ligne.idSite,myServiceLigne.myDatas.ligne.idEmplacement)
				}
			}
			else
				tncSite.lbSite.text="";
		}

		private function onInfosSiteLoadedHandler(evt:Event):void
		{
			myServicesSite.myDatas.removeEventListener(gestionparcEvent.INFOS_FICHE_SITE_LOADED, onInfosSiteLoadedHandler);
			if (myServicesSite.myDatas.site.idSite > 0)
			{
				tncSite.lbSite.text=myServicesSite.myDatas.site.libelleSite;
				tncSite.txtAdresse.text=myServicesSite.myDatas.site.adresse1;
				tncSite.lbCodePostal.text=myServicesSite.myDatas.site.codePostale;
				tncSite.lbVille.text=myServicesSite.myDatas.site.ville;
				tncSite.lbPays.text=myServicesSite.myDatas.site.pays;

				myServicesSite.myDatas.addEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedHandler);
			}
		}

		private function onInfosEmplaSiteLoadedHandler(evt:gestionparcEvent):void
		{
			myServicesSite.myDatas.removeEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedHandler);
			if (myServiceLigne.myDatas.ligne.idEmplacement > 0)
			{
				var longueurTabEmplacement:int=myServicesSite.myDatas.listEmplacement.length;
				for (var k:int=0; k < longueurTabEmplacement; k++)
				{
					if ((myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).idEmplcament == myServiceLigne.myDatas.ligne.idEmplacement)
					{
						tncSite.lbEmplacement.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).libelleEmplacement;
						tncSite.txtBatiment.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).batiment;
						tncSite.txtEtage.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).etage;
						tncSite.txtSalle.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).salle;
						tncSite.txtCouloir.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).couloir;
						tncSite.txtArmoire.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).armoire;
						break;
					}
				}
			}
		}

		private function onTncRaccordementClickHandler(event:MouseEvent):void
		{
			var popUpChangementRaccordement:FicheChangementRaccordementIHM=new FicheChangementRaccordementIHM();
			popUpChangementRaccordement.myServiceLigne=myServiceLigne;
			popUpChangementRaccordement.addEventListener(gestionparcEvent.RACCOR_LIGNE_DID, onRaccorLigneDidHandler);
			PopUpManager.addPopUp(popUpChangementRaccordement, this, true);
			PopUpManager.centerPopUp(popUpChangementRaccordement);
		}

		private function onRaccorLigneDidHandler(evt:gestionparcEvent):void
		{
			tncRaccordement.dataDatagrid.addItemAt(evt.obj, 0);
			tncRaccordement.buttonAction.enabled=true;
		}

		private function runValidators():Boolean
		{
			var validators:Array=null;
			var results:Array=null;

			validators=[valSousTete, valNumContrat];
			results=Validator.validateAll(validators);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
				return true;
		}

		// creation onglet dans le tabNavigator
		private function changeOngletDefault():void
		{
			tnLigne.addChild(tncAboOption);
			tnLigne.addChild(tncContratOperateur);
			tnLigne.addChild(tncDivers);
			tnLigne.addChild(tncRaccordement);
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("D_CDR") && moduleGestionParcIHM.globalParameter.userAccess.D_CDR == 1)
				tnLigne.addChild(tncTickets);

			boolSite=true;
			boolPoint=true;
			boolT2=true;
			boolXdsl=true;
			boolCanaux=true;
			boolCaracteristique=true;
		}

		private function changeOngletLS():void
		{
			tnLigne.addChild(tncSite);
			tnLigne.addChild(tncAboOption);
			tnLigne.addChild(tncDivers);
			tnLigne.addChild(tncContratOperateur);
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("D_CDR") && moduleGestionParcIHM.globalParameter.userAccess.D_CDR == 1)
				tnLigne.addChild(tncTickets);
			
			tnLigne.addChild(tncRaccordement);
			tnLigne.addChild(tncCaracteristique);
			tnLigne.addChild(tncCablageSdaPointaPoint);

			boolT2=true;
			boolXdsl=true;
			boolCanaux=true;
		}

		private function changeOngletXDSL():void
		{
			tnLigne.addChild(tncSite);
			tnLigne.addChild(tncAboOption);
			tnLigne.addChild(tncDivers);
			tnLigne.addChild(tncContratOperateur);
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("D_CDR") && moduleGestionParcIHM.globalParameter.userAccess.D_CDR == 1)
				tnLigne.addChild(tncTickets);

			tnLigne.addChild(tncRaccordement);
			tnLigne.addChild(tncCaracteristique);
			tnLigne.addChild(tncCablageSdaXdsl);

			boolT2=true;
			boolPoint=true;
			boolCanaux=true;
		}

		private function changeOngletT2():void
		{
			tnLigne.addChild(tncSite);
			tnLigne.addChild(tncAboOption);
			tnLigne.addChild(tncDivers);
			tnLigne.addChild(tncContratOperateur);
			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("D_CDR") && moduleGestionParcIHM.globalParameter.userAccess.D_CDR == 1)
				tnLigne.addChild(tncTickets);

			tnLigne.addChild(tncRaccordement);
			tnLigne.addChild(tncCaracteristique);
			tnLigne.addChild(tncCablageSdaT2T0Ana);
			tnLigne.addChild(tncCanauxGroupment);

			boolPoint=true;
			boolXdsl=true;
		}
	}
}