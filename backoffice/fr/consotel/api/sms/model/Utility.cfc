<cfcomponent displayname="Utility" hint="I ckeck your data" output="false">
	
<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- tester si l'uuid est valide ' --->
	<cffunction name="isUUID" access="remote" output="false" returntype="boolean" hint="I check the string to make sure it's a UUID.">
		<cfargument name="uuid" type="string" required="false">
		<cfreturn isvalid("UUID",ARGUMENTS.uuid) >
	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------------------->

<!----------------------------------------------------------------------------------------------------------------------------------------->
	
	<!--- tester si operator est valide ' --->
	<cffunction name="isOperator" access="remote" output="false" returntype="boolean" hint="I check the string operator to make sure it's SFR or ORANGE.">
		<cfargument name="operator" type="string" required="false">
		<cfset res=(compareNoCase(operator,"SFR") eq 0) OR (compareNoCase(operator,"ORANGE") eq 0) OR (compareNoCase(operator,"TWILIO") eq 0)>	
		<cfreturn res>	
	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------------------->

<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- tester si le contenu du SMS est valide ' --->
	<cffunction name="isOkContentSms" access="remote" output="false" returntype="any" hint="I check the string content SMS to make sure it's ok '.">
		<cfargument name="contentSms" type="any" required="false">
		
		<cfset resultat=arrayNew(1)>
		
		<cfif isArray(contentSms)>
			<cfloop from="1" to="#ArrayLen(contentSms)#" index="i" step="1">
				<cfset position=find(";",contentSms[i])>
				<cfif position neq 0 ><!--- cela vaut dire que l ; existe dans le SMS --->
					<cfset resultat[1]=contentSms[i]>
					<cfreturn resultat> <!--- en cas d'erreur on return un tableau avec le num' --->
				</cfif>
			</cfloop>
			<cfreturn true>
		<cfelse>
			<cfset position=find(";",contentSms)>
			<cfif position neq 0 >
				<cfreturn false>
				<cfelse>
					<cfreturn true>	
			</cfif>	
		</cfif>
	
	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------------------->

<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- tester si le numéro tel est valide ' --->
	<cffunction name="isOkPhoneNumber" access="remote" output="false" returntype="any" hint="I check the phone number to make sure it's ok '.">
		
		<cfargument name="phoneNumber" type="any" required="false">
		<cfargument name="operator"    type="string"    required="false">
		
		<cfset num_ok=arrayNew(1)>
		<cfset resultat=arrayNew(1)>
		
		<cfif isArray(phoneNumber)> <!--- si j'ai un tableau des numéros tel --->
			<cfloop from="1" to="#ArrayLen(phoneNumber)#" index="i" step="1">
				<cfset num_ok = REMatch('^\+33(6|7)[0-9]{8}$',phoneNumber[i])>
				<cfif arraylen(num_ok) gt 0><!--- si ok , on fait rien  --->
				<cfelse >
					<cfset num_ok = REMatch('^\+[0-9]{8,15}$',phoneNumber[i])>
					<cfif (arraylen(num_ok) gt 0) ><!--- AND (compareNoCase(operator,"SFR") eq 0) --->
					<cfelse> 
						<cfset resultat[1]=phoneNumber[i]>
						<cfreturn resultat> <!--- en cas d'erreur on return un tableau avec le num --->
					</cfif>
				</cfif>
			</cfloop>
			<cfreturn true>
		<cfelse> <!--- si j'ai un seul numéros tel --->
			<cfset num_ok = REMatch('^\+33(6|7)[0-9]{8}$',phoneNumber)>
			<cfif arraylen(num_ok) gt 0 >
				<cfreturn true>
			<cfelse>
				<cfset num_ok = REMatch('^\+[0-9]{8,15}$',phoneNumber)>
				<cfif  (arraylen(num_ok) gt 0) > <!--- AND (compareNoCase(operator,"SFR") eq 0) --->
					<cfreturn true>
				<cfelse> 
					<cfreturn false>
				</cfif>
			</cfif>
		</cfif>
	</cffunction>
	
<!----------------------------------------------------------------------------------------------------------------------------------------->

<!----------------------------------------------------------------------------------------------------------------------------------------->

	<!--- tester si delay est valide --->
	<cffunction name="isOkDelay" access="remote" output="false" returntype="boolean" hint="I check the delay">
		
		<cfargument name="delay" type="any" required="false">		
		<cfset tabRetour=arrayNew(1) >
		<cfset tabRetour = REMatch('^[0-9]{0,6}$',delay)>
		<cfif arraylen(tabRetour) gt 0 >
			<cfreturn true>
		<cfelse> 
			<cfreturn false>
		</cfif>
			
	</cffunction>

<!----------------------------------------------------------------------------------------------------------------------------------------->

<!----------------------------------------------------------------------------------------------------------------------------------------->
	
	<cffunction name="isOkTabSMSNumber" access="remote" output="false" returntype="any" hint="">
		
		<cfargument name="tabSMSNumber" type="array" required="false">
		<cfargument name="operator"    type="string"    required="false">
		
		<cfset num_ok=arrayNew(1)>
		<cfset resultat=arrayNew(1)>
		
		 <cfloop from="1" to="#ArrayLen(tabSMSNumber)#" index="i" step="1"> 
			 
			<cfset num_ok = REMatch('^\+33(6|7)[0-9]{8}$',tabSMSNumber[i]["phoneNumber"])>
			<cfif arraylen(num_ok) gt 0><!--- si ok , on fait rien  --->
			<cfelse>
				<cfset num_ok = REMatch('^\+[0-9]{8,15}$',tabSMSNumber[i]["phoneNumber"])>
				<cfif (arraylen(num_ok) gt 0) AND (compareNoCase(operator,"SFR") eq 0)>
				<cfelse> 
					<cfset resultat[1]=tabSMSNumber[i]["phoneNumber"]>
					<cfset resultat[2]=1><!--- erreur num phone --->
					<cfreturn resultat> <!--- en cas d'erreur on return un tableau avec le num --->
				</cfif>
			</cfif>
			<cfset position=find(";",tabSMSNumber[i]["contentSms"])>
			<cfif position neq 0 ><!--- cela vaut dire que le ; existe dans le SMS --->
				<cfset resultat[1]=tabSMSNumber[i]["contentSms"]>
				<cfset resultat[2]=2><!--- erreur contenu SMS --->
				<cfreturn resultat> <!--- en cas d'erreur on return un tableau avec le contenu SMS' --->
			</cfif>
			
		</cfloop> 
		<cfreturn true>
		
	</cffunction>
	
<!----------------------------------------------------------------------------------------------------------------------------------------->


</cfcomponent>