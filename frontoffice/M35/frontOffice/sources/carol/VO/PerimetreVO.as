package carol.VO
{
	import carol.service.CarolService;
	
	import mx.controls.DateField;
	
	[Bindable]
	public class PerimetreVO
	{
		private var _niveau					:String;
		private var _libelle_niveau			:String;
		private var _idx_niveau				:int;
		private var _idx_niveau_aggreg		:int;
		private var _niveau_aggreg 			:String;
		private var _libelle_niveau_aggreg	:String;
		private var _idperimetre			:int;
		private var _iduser					:int;
		private var _idracine				:int;
		private var _libelle_racine			:String;
		private var _idracinemaster			:int;
		private var _idorga					:int;
		private var _idcliche				:int;
		private var _idperiodedeb			:int;
		private var _idperiodefin			:int;
		private var _langue					:String;
		private var _libelle_orga			:String;
		private var _libelle_periodedeb		:Date;
		private var _libelle_periodefin		:Date;
		
		private var _isSetUp:Boolean = false
		
		public var cs:CarolService;

		public function PerimetreVO()
		{
		}
/* -------------------------------------------------------------------------- */
/* 						FONCTIONS											  */
/* -------------------------------------------------------------------------- */		
	 	public function displayDateDeb():String
	 	{
	 		return DateField.dateToString(_libelle_periodedeb,'MM/YYYY')
	 	}	
		
		public function displayDateFin():String
	 	{
	 		return DateField.dateToString(_libelle_periodefin,'MM/YYYY')	
	 	}	
		
		public function getDataset():void
		{
			cs.getDataset(	_idorga,
							_idperimetre,
							_niveau,
							_idperiodedeb,
							_idperiodefin,
							_niveau_aggreg);
		}
		public function isSetUp():Boolean
		{
			return _isSetUp
		}
	/* ----------------------------------------------------------------------------	*/
	/*  			CHARGER LES NIVEAUX D'ORGA DE L'ANALYSE			   				*/
	/* ----------------------------------------------------------------------------	*/	
		public function chargerNiveauOrga():void
		{
			cs.getListeNiveauOrga(_idorga,_idcliche)
		}
/* -------------------------------------------------------------------------- */
/* 						GETTER SETTER										  */
/* -------------------------------------------------------------------------- */
	// SETTER
		public function set niveau(value:String):void
		{
			_niveau = value;
			_isSetUp = true;
		}
		public function set idperimetre(value:int):void
		{
			_idperimetre = value;
			_isSetUp = true;
		}
		public function set iduser(value:int):void
		{
			_iduser = value;
			_isSetUp = true;
		}
		public function set idracine(value:int):void
		{
			_idracine = value;
			_isSetUp = true;
		}
		public function set idracinemaster(value:int):void
		{
			_idracinemaster = value;
			_isSetUp = true;
		}
		public function set idorga(value:int):void
		{
			_idorga = value;
			_isSetUp = true;
		}
		public function set idperiodedeb(value:int):void
		{
			_idperiodedeb = value;
			_isSetUp = true;
		}
		public function set idperiodefin(value:int):void
		{
			_idperiodefin = value;
			_isSetUp = true;
		}
		public function set langue(value:String):void
		{
			_langue = value;
			_isSetUp = true;
		}
		public function set libelle_orga(value:String):void
		{
			_libelle_orga = value;
			_isSetUp = true;
		}
		public function set libelle_periodedeb(value:Date):void
		{
			_libelle_periodedeb = value;
			_isSetUp = true;
		}
		public function set libelle_periodefin(value:Date):void
		{
			_libelle_periodefin = value;
			_isSetUp = true;
		}
		public function set idcliche(value:int):void
		{
			_idcliche = value;
			_isSetUp = true;
		}
		public function set niveau_aggreg(value:String):void
		{
			_niveau_aggreg = value;
			_isSetUp = true;
		}
		public function set libelle_niveau_aggreg(value:String):void
		{
			_libelle_niveau_aggreg = value;
			_isSetUp = true;
		}
		public function set libelle_niveau(value:String):void
		{
			_libelle_niveau = value;
			_isSetUp = true;
		}
		public function set idx_niveau_aggreg(value:int):void
		{
			_idx_niveau_aggreg = value;
			_isSetUp = true;
		}
		public function set idx_niveau(value:int):void
		{
			_idx_niveau = value;
			_isSetUp = true;
		}
	// GETTER	
		public function get niveau():String
		{
			return _niveau;
		}
		public function get idperimetre():int
		{
			return _idperimetre;
		}
		public function get iduser():int
		{
			return _iduser;
		}
		public function get idracine():int
		{
			return _idracine;
		}
		public function get idracinemaster():int
		{
			return _idracinemaster;
		}
		public function get idorga():int
		{
			return _idorga;
		}
		public function get idperiodedeb():int
		{
			return _idperiodedeb;
		}
		public function get idperiodefin():int
		{
			return _idperiodefin;
		}
		public function get libelle_orga():String
		{
			return _libelle_orga;
		}
		public function get langue():String
		{
			if(_langue != null && _langue != "")
				return _langue;
			else
				return "fr_FR"
		}
		public function get libelle_periodedeb():Date
		{
			return _libelle_periodedeb;
		}
		public function get libelle_periodefin():Date
		{
			return _libelle_periodefin;
		}
		public function get idcliche():int
		{
			return _idcliche;
		}
		public function get niveau_aggreg():String
		{
			return _niveau_aggreg;
		}
		public function get libelle_niveau_aggreg():String
		{
			return _libelle_niveau_aggreg;
		}
		public function get libelle_niveau():String
		{
			return _libelle_niveau;
		}
		public function get idx_niveau_aggreg():int
		{
			return _idx_niveau_aggreg;
		}
		public function get idx_niveau():int
		{
			return _idx_niveau;
		}

		public function set libelle_racine(value:String):void
		{
			_libelle_racine = value;
		}

		public function get libelle_racine():String
		{
			return _libelle_racine;
		}
	}
}