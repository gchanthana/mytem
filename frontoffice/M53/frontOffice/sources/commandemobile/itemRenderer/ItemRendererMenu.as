package commandemobile.itemRenderer
{
	import commandemobile.entity.Commande;
	import commandemobile.system.GestionWorkFlow;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.Spacer;

	[Bindable]
	public class ItemRendererMenu extends HBox
	{
		private var lb_libelle				:Label;
		private var spacer					:Spacer;
		private var img						:Image;
		
		public var dataMenuCollabo 			:Array;

		private var customMenu				:CustomMenu2;
		private var pt						:Point;
		
		[Embed(source="/assets/images/icon_show_action.png")]
		private var iconClass				:Class;
		private var _gworkFlow				:GestionWorkFlow = new GestionWorkFlow();
		private var _addEvent				:Boolean;
		private var typeFiche				:String;
		
		private var _listActions			:ArrayCollection = new ArrayCollection();
		
		
		[Embed(source='/assets/images/arrow_down_blue.png',mimeType='image/png')]
		private var iconClassRecepCmd		:Class;
		[Embed(source='/assets/images/arrow_down_green.png',mimeType='image/png')]
		private var iconClassRecepLiv		:Class;
		[Embed(source='/assets/images/gear_stop.png',mimeType='image/png')]
		private var iconClassAnnul			:Class;
		[Embed(source='/assets/images/Download_2.png',mimeType='image/png')]
		private var iconClassExped			:Class;
		[Embed(source='/assets/images/RAGO - Desactiver.png',mimeType='image/png')]
		private var iconClassDesactive		:Class;
		[Embed(source='/assets/images/RAGO - Activer.png',mimeType='image/png')]
		private var iconClassActive			:Class;
		[Embed(source='/assets/images/RAGO - Modifier.png',mimeType='image/png')]
		private var iconClassDetails		:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassErase			:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassValide			:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassValTarif		:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassSend			:Class;
		[Embed(source='/assets/images/RAGO - Supprimer.png',mimeType='image/png')]
		private var iconClassValSend		:Class;
		
		
		public function ItemRendererMenu()
		{
			super();
			
			this.percentWidth = 100;
			
			lb_libelle = new Label();
			lb_libelle.mouseChildren = false;
			spacer = new Spacer();
			spacer.percentWidth = 100;
			img = new Image();
			img.source = iconClass;
			img.width = 20;
			img.height = 20;
			img.addEventListener(MouseEvent.CLICK, clickIconActionHandler);
			this.addChild(lb_libelle);
			this.addChild(spacer);
			this.addChild(img);

			_gworkFlow.addEventListener("actionsFinished",getActionsResultHandler);
		}
		
		private function getActionsResultHandler(e:Event):void
		{	
			if(_gworkFlow.listeActionsPossibles != null)
			{
				if(_gworkFlow.listeActionsPossibles.length > 0)
				{
					_listActions = _gworkFlow.listeActionsPossibles;
					dataMenuCollabo = attributObjectToArray(_listActions,true);
				}
				else
				{
					_listActions = _gworkFlow.listeActionsPossibles;
					dataMenuCollabo = attributObjectToArray(_listActions,false);
				}
			}
			else
			{
				_listActions = new ArrayCollection();
				dataMenuCollabo = attributObjectToArray(_listActions,false);
			}
			listDataMenu = dataMenuCollabo;
			customMenu.showMenu(pt.x, pt.y + 5);
			customMenu.addEventListener(CustomMenuEvent.MENU_ITEM_CLICK, menuItemEvent);
		}
		
		private function attributObjectToArray(arrayCollection:ArrayCollection,bool:Boolean):Array
		{
			var object:Object;
			var action:Array = new Array();
			action[0] = attributDetails();
			if(bool)
			{
				for(var i:int = 0;i < arrayCollection.length;i++)
				{
					object = new Object();
					object.label				= arrayCollection[i].LIBELLE_ACTION;
					object.icon					= attributIcon(arrayCollection[i].CODE_ACTION);
					object.code					= arrayCollection[i].CODE_ACTION;
					object.CODE_ACTION			= arrayCollection[i].CODE_ACTION;
					object.COMMENTAIRE_ACTION	= arrayCollection[i].COMMENTAIRE_ACTION;
					object.DATE_ACTION			= arrayCollection[i].DATE_ACTION;
					object.DEST					= arrayCollection[i].DEST;
					object.EXP					= arrayCollection[i].EXP;
					object.IDACTION				= arrayCollection[i].IDACTION;
					object.IDETAT_ENGENDRE		= arrayCollection[i].IDETAT_ENGENDRE;
					object.LIBELLE_ACTION		= arrayCollection[i].LIBELLE_ACTION;
					object.LIBELLEETAT_ENGENDRE	= arrayCollection[i].LIBELLEETAT_ENGENDRE;
					object.MESSAGE				= arrayCollection[i].MESSAGE;
					action[i+1] = object;
				}
			}
			var nbrElements:int = action.length;
			if(data.IDLAST_ETAT >= 3074)
			{
				action[nbrElements] = changeSaisieTechnique();
			}
			nbrElements = action.length;
			action[nbrElements] = viewPDF();
			nbrElements = action.length;
			action[nbrElements] = viewTracking();
			nbrElements = action.length;
			action[nbrElements] = attributErase();
			return action;
		}

		private function attributDetails():Object
		{
			var object:Object= new Object();
			object.label 	= "Détails de la commande";
			object.code		= "VIEW_CMD";
			object.icon 	= attributIcon(object.code);
			return object;
		}
		
		private function changeSaisieTechnique():Object
		{
			var object:Object= new Object();
			object.label 	= "Renseignement des références";
			object.code		= "VIEW_SAISETECHNIQUE";
			object.icon 	= attributIcon(object.code);
			return object;
		}
		
		private function viewPDF():Object
		{
			var object:Object= new Object();
			object.label 	= "Afficher la commande en PDF";
			object.code		= "VIEW_CMD_PDF";
			object.icon 	= attributIcon(object.code);
			return object;
		}
		
		private function viewTracking():Object
		{
			var object:Object= new Object();
			object.label 	= "Suivre l'expédition";
			object.code		= "VIEW_CMD_TRACKING";
			object.icon 	= attributIcon(object.code);
			return object;
		}
		
		private function attributIcon(actionCode:String):Class
		{
			var icone:Class;
			switch(actionCode)
			{
				case "EMA4"		:icone = iconClassRecepCmd;		break;
				case "LIVRE"	:icone = iconClassRecepLiv;		break;
				case "ANNUL"	:icone = iconClassAnnul;		break;
				case "EXPED"	:icone = iconClassExped;		break;
				case null		:icone = iconClassDesactive;	break;
				case "EMA7"		:icone = iconClassActive;		break;
				case "VIEW_CMD"	:icone = iconClassDetails;		break;
				case "ERASE_CMD":icone = iconClassErase;		break;
				case "EMA2B"	:icone = null/*iconClassValide*/;		break;
				case "EMA1"		:icone = null/*iconClassValTarif*/;		break;
				case "EMA3B"	:icone = null/*iconClassSend*/;			break;
				case "EMA3T"	:icone = null/*iconClassValSend*/;		break;
				case "VIEW_CMD_PDF"	:icone = null/*iconClassValSend*/;		break;
				case "VIEW_CMD_TRACKING"	:icone = null/*iconClassValSend*/;		break;
				default :icone = null/*iconClassValSend*/;		break;
			}
			return icone;
		}
		
		private function attributErase():Object
		{
			var object:Object= new Object();
			object.label 	= "Supprimer la commande";
			object.code		= "ERASE_CMD";
			object.icon 	= attributIcon(object.code);
			return object;
		}
		
		//////////////////////////////////////////////////////////
		//
		//					METHODES PUBLIQUES
		//
		//////////////////////////////////////////////////////////
		
		/* override public function set data(value:Object):void {
			if(value) {
				super.data = value;
			}
		} */
		
		
		
		public function set addEvent(value:Boolean):void {
			if(value == true) {
				this.addEventListener(MouseEvent.ROLL_OVER, rollHBoxEvent);
				this.addEventListener(MouseEvent.ROLL_OUT, rollHBoxEvent);
				img.visible = false;
				_addEvent = true;
			}
			else {
				img.visible = true;
				_addEvent = false;
			}
		}
		
		public function set textLabel(value:String):void {
			lb_libelle.text = value;
			
		}
		
		public function get textLabel():String {
			return lb_libelle.text;
		}
		
		[Inspectable(defaultValue="false",enumeration="true,false",type="Boolean")]
		public function set cursorLabel(value:Boolean):void {
			lb_libelle.buttonMode = value;
			lb_libelle.useHandCursor = value;
		}
		
		public function set textDecoration(value:String):void {
			lb_libelle.setStyle("textDecoration", value);
		}
		
		[Inspectable(defaultValue="collaborateur",enumeration="collaborateur,ligne,equipement,sim,emei",type="String")]
		public function set clickLabel(fiche:String):void {
			typeFiche = fiche;
			lb_libelle.addEventListener(MouseEvent.CLICK, clickLabelHandler);
		}
		
		[Inspectable(defaultValue="true",enumeration="true,false",type="Boolean")]
		public function set cursorImage(value:Boolean):void {
			img.buttonMode = value;
			img.useHandCursor = value;
		}

		public function set listDataMenu(list:Array):void {
			customMenu = CustomMenu2.createCustomMenu(this, list, false);
		}

		
		//////////////////////////////////////////////////////////
		//
		//					METHODES PRIVEES
		//
		//////////////////////////////////////////////////////////
		
		private function rollHBoxEvent(evt:MouseEvent):void {
			var type:String = evt.type;
			if(type == MouseEvent.ROLL_OVER) {
				if(textLabel == "") {
					this.visible = false;
					this.removeEventListener(MouseEvent.ROLL_OVER, rollHBoxEvent);
					this.removeEventListener(MouseEvent.ROLL_OUT, rollHBoxEvent);
				}
				else {
					img.visible = true;
				} 				
			}
			else if(type == MouseEvent.ROLL_OUT) {
				img.visible = false;
			}
		}

		private function clickLabelHandler(evt:MouseEvent):void {
			this.dispatchEvent(new PanelAffectationEvent(PanelAffectationEvent.DISPLAY_PANEL,
															 true,
															 "VIEW_CMD"));
//			this.dispatchEvent(new PopupFicheEvent(PopupFicheEvent.CLICK_BT_DISPLAY, true, typeFiche));
		}
		
		private function clickIconActionHandler(evt:MouseEvent):void {
	
			pt = new Point(mouseX, mouseY);
			pt = contentToGlobal(pt);
			
			if(_addEvent == true) 
			{
				customMenu.addEventListener(MenuRollEvent.MENU_ROLL_OVER, menuRollEvent);
				customMenu.addEventListener(MenuRollEvent.MENU_ROLL_OUT, menuRollEvent);
				_gworkFlow.fournirListeActionsPossibles(data.IDLAST_ETAT,data as Commande);
			}
			
		}
		
		private function menuRollEvent(evt:MenuRollEvent):void {
			var type:String = evt.type;
			if(type == MenuRollEvent.MENU_ROLL_OVER) {
				img.visible = true;
			}
			else if(type == MenuRollEvent.MENU_ROLL_OUT) {
				img.visible = false;
			}
		}
			
		private function menuItemEvent(evt:CustomMenuEvent):void {
			var type:String = evt.type;
			if(type == CustomMenuEvent.MENU_ITEM_CLICK) {
				
				this.dispatchEvent(new PanelAffectationEvent(PanelAffectationEvent.DISPLAY_PANEL,
															 true,
															 evt.codeItemMenu,evt.actionItemMenu));
				
			}
			if(img.visible == false) {
				img.visible = true;
			}
		}
	}
}