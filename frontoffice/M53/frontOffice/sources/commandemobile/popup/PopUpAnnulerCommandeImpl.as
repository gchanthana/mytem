package commandemobile.popup
{
	import commandemobile.system.Action;
	
	import flash.events.Event;
	
	import mx.containers.Box;
	import mx.controls.TextArea;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class PopUpAnnulerCommandeImpl extends Box
	{
		
		//COMPOSANTS
		public var txtCommentaire:TextArea;
		
		//OBJECTS
		public var selectedAction:Action;
		
		//VARIABLES LOCALES
		public var libelleAction:String = "";
		
		public function PopUpAnnulerCommandeImpl()
		{
			addEventListener("closeThis",closeThisHandler);
		}
		
		protected function btnValiderClickHandler():void
		{
			selectedAction.MESSAGE = txtCommentaire.text;
			dispatchEvent(new Event("messageSaisie"));
		}
		protected function btnAnnulerClickHandler():void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function closeThisHandler(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}



	}
}