package gestionparc.entity
{
	import flash.events.Event;
	import flash.events.EventDispatcher;

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	[Bindable]
	public class Commande extends EventDispatcher
	{


		public var IDCOMMANDE:Number=0;
		public var IDCONTACT:Number=0;
		public var IDREVENDEUR:Number=0;
		public var IDOPERATEUR:Number=0;
		public var IDTYPE_COMMANDE:Number=0;
		public var IDSOCIETE:Number=0;
		public var IDCOMPTE_FACTURATION:Number=0;
		public var IDSOUS_COMPTE:Number=0;
		public var IDRACINE:Number=0;
		public var IDLAST_ETAT:Number=0;
		public var IDLAST_ACTION:Number=0;
		public var IDSITELIVRAISON:Number=0;
		public var IDTRANSPORTEUR:Number=0;
		public var IDPOOL_GESTIONNAIRE:Number=0;
		public var IDGESTIONNAIRE:Number=0;
		public var IDPROFIL:Number=0;
		public var IDINV_ETAT:Number=0;

		public var IDACTEPOUR:Number=0;
		public var IDPROFIL_EQUIPEMENT:Number=0;

		public var USERCREATE:String="";
		public var USERID:int=-1;

		public var NUMERO_COMMANDE:String="";

		public var REF_OPERATEUR:String="";
		public var REF_CLIENT:String="";
		public var REF_LIVRAISON:String="";

		public var LIBELLE_COMMANDE:String="";
		public var LIBELLE_POOL:String="";
		public var LIBELLE_REVENDEUR:String="";
		public var LIBELLE_OPERATEUR:String="";
		public var LIBELLE_COMPTE:String="";
		public var LIBELLE_SOUSCOMPTE:String="";
		public var LIBELLE_TRANSPORTEUR:String="";
		public var LIBELLE_SITELIVRAISON:String="";
		public var LIBELLE_LASTETAT:String="";
		public var LIBELLE_LASTACTION:String="";
		public var COMMENTAIRES:String="";

		public var LIBELLE_CONFIGURATION:String="";
		public var SHAREMODELEALLPOOLS:Boolean=false;

		public var PRICE_EQUIPEMENT:Object=new Object();

		public var PRIX_ACHAT:String="";
		public var TYPE_OPERATION:String="";
		public var PATRONYME_CONTACT:String="";
		public var CREEE_PAR:String="";
		public var MODIFIEE_PAR:String="";
		public var BOOL_ENVOYER_VIAMAIL:Number=0;
		public var CREEE_LE:Date=null;
		public var MODIFIEE_LE:Date=null;
		public var ENVOYER_LE:Date=null;
		public var LIVREE_LE:Date=null;
		public var LIVRAISON_PREVUE_LE:Date=null;
		public var DATE_LIVRAISON:Date=null;
		public var DATE_COMMANDE:Date=null;
		public var EXPEDIE_LE:Date=null;
		public var BOOL_DEVIS:Number=0;
		public var NUMERO_TRACKING:String="";
		public var MONTANT:Number=0;
		public var SEGMENT_FIXE:Number=0;
		public var SEGMENT_MOBILE:Number=0;
		public var IDGESTIONNAIRE_MODIF:Number=0;
		public var IDGESTIONNAIRE_CREATE:Number=0;
		public var IDGROUPE_REPERE:Number=0;
		public var ENCOURS:Number=1;
		public var EMAIL_CONTACT:String="";
		public var REF_REVENDEUR:String="";

		public var DATE_CREATE:Date=null;
		public var ENGAGEMENT:String="";
		public var CONFIG_NUMBER:int=1;
		public var ACTION:String="";


		public var ARTICLES:XML=<articles></articles>;


		//pour créer un contrat de garantie pour un équipement
		public var IDEQUIPEMENT:Number;
		public var DATE_DEBUT:Date=null;
		public var DUREE:Number=0;
		public var TYPE_CONTRAT:Number=0;



		public var COMMANDE:ArrayCollection=new ArrayCollection();
		public var COMMANDE_TEMPORAIRE:ArrayCollection=new ArrayCollection();
		public var CONFIGURATION_TEMPORAIRE:ArrayCollection=new ArrayCollection();

		public static const NUM_CMDE_CREATED:String="numCmdeCreated";


		public function Commande(auto:Boolean=false)
		{
			if (auto)
			{
				genererNumeroDeCommande();
			}
		}

		protected function genererNumeroDeCommande():void
		{

			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.commande.GestionCommande", "genererNumeroDeCommande", genererNumeroDeCommandeResultHandler);
			RemoteObjectUtil.callService(op);
		}


		private function genererNumeroDeCommandeResultHandler(e:ResultEvent):void
		{
			NUMERO_COMMANDE=String(e.result);
			REF_OPERATEUR=String(e.result);
			this.dispatchEvent(new Event(NUM_CMDE_CREATED));
		}
	}
}
