package intranet.GestionDroits.event
{
	import flash.events.Event;

	public class profilEvent extends Event
	{
		public static const PROFIL_EVENT:String= "profilEvent";
		
		public function profilEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}