package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.ContrainteSuppressionEvent;
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.event.GestionTypeCommandeEvent;
	import univers.parametres.parametrecompte.service.TypeCommandeService;
	import univers.parametres.parametrecompte.vo.TypeCommande;
	
	[Bindable]
	public class ListeTypeCommande extends VBox
	{
		public var typeCommandeService : TypeCommandeService = new TypeCommandeService();
		
		
		//COMPONANT
		public var dg_type_cmd: DataGrid;
		private var popContrainteSuppressionIHM : ContrainteSuppressionIHM;
		private var popParametreTypeCommande : ParametreTypeCommandeIHM;
		
		public function ListeTypeCommande()
		{
			
		}
		public function init(evt : Event=null):void
		{
			typeCommandeService.getTypeCommande(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		public function modifier(typeCMD : TypeCommande):void
		{
			popParametreTypeCommande = new ParametreTypeCommandeIHM();
			popParametreTypeCommande.typeCommande = typeCMD;
			
			addPop();
		}
		public function addPool(event:MouseEvent):void
		{
			popParametreTypeCommande = new ParametreTypeCommandeIHM();
			addPop();
			
		}
		private function parametrePoolCOmpleteHandler(evt : GestionPoolEvent):void
		{
			typeCommandeService.getTypeCommande(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		private function addPop():void
		{
			popParametreTypeCommande.addEventListener(GestionTypeCommandeEvent.PARAMETRE_TYPE_CMD_COMPLETE,init);
			PopUpManager.addPopUp(popParametreTypeCommande,this,true);
			PopUpManager.centerPopUp(popParametreTypeCommande);
		}
		public function supprimer(data : TypeCommande):void
		{
			//ConsoviewAlert.afficherAlertConfirmation("Êtes vous sûr de vouloir supprimer le type de commande "+data.libelleTypeCmd+" ?","Confirmation",supprimerBtnValiderCloseEvent);
			typeCommandeService.addEventListener(GestionTypeCommandeEvent.LISTE_CONTRAINTE_COMPLETE,listeContrainteHandler);
			typeCommandeService.listeContrainteSuppression((dg_type_cmd.selectedItem as TypeCommande).idTypeCmd);
		}

		/*private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				//siteService.addEventListener(GestionSiteEvent.LISTE_CONTRAINTE_SITE_COMPLETE,listeContrainteOfSiteHandler);
				//siteService.listeContrainteOfSite((dg_site.selectedItem as Site).id_site);
				typeCommandeService.addEventListener(GestionTypeCommandeEvent.LISTE_CONTRAINTE_COMPLETE,listeContrainteHandler);
				typeCommandeService.listeContrainteSuppression((dg_type_cmd.selectedItem as TypeCommande).idTypeCmd);
			}
		}*/
		private function listeContrainteHandler(evt : GestionTypeCommandeEvent):void
		{
			if(typeCommandeService.col_contrainte.length>0) // il existe des contraintes qui empêche la suppression
			{
				popContrainteSuppressionIHM = new ContrainteSuppressionIHM();
				popContrainteSuppressionIHM.addEventListener(ContrainteSuppressionEvent.VALIDATE_SUPPRESSION_POOL,supprimerSelected);
				popContrainteSuppressionIHM.listeContraintes = typeCommandeService.col_contrainte;
				popContrainteSuppressionIHM.messageDecription = 'Un ou plusieurs éléments sont rattachés au type de commande '+(dg_type_cmd.selectedItem as TypeCommande).libelleTypeCmd+'. Vous devez supprimer ces éléments avant de continuer.';
				popContrainteSuppressionIHM.messageConfirmation = 'Êtes vous sûr de vouloir supprimer le type de commande ?';
				PopUpManager.addPopUp(popContrainteSuppressionIHM,this,true);
				PopUpManager.centerPopUp(popContrainteSuppressionIHM);
			}
			else // pas de contrainte on peut supprimer
			{
				supprimerSelected();
			}
		}
		private function supprimerSelected(evt : ContrainteSuppressionEvent=null):void
		{
			//TODO REQUETE DE SUPPRESSION
			//siteService.addEventListener(GestionSiteEvent.REMOVE_SITE_COMPLETE,remove_complete_handler);
			//siteService.removeSite((dg_type_cmd.selectedItem as TypeCommande).idTypeCmd);
			typeCommandeService.addEventListener(GestionTypeCommandeEvent.REMOVE_COMPLETE,remove_complete_handler);
			typeCommandeService.removeTypeCommande((dg_type_cmd.selectedItem as TypeCommande).idTypeCmd);
			
			
		}
		private function remove_complete_handler(evt : GestionTypeCommandeEvent=null):void
		{
			ConsoviewAlert.afficherOKImage("le type de commande "+(dg_type_cmd.selectedItem as TypeCommande).libelleTypeCmd+" a bien été supprimé.");
			init();
		}
		
	}
}