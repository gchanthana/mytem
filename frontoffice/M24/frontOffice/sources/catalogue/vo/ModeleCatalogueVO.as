package catalogue.vo
{
	public class ModeleCatalogueVO
	{
		private var _LIBELLE_MODELE:String;
	
		private var _ID_MODELE:Number;
		
		public function ModeleCatalogueVO()
		{ 
		}
		public function get ID_MODELE():Number
		{
			return _ID_MODELE;
		}
		public function get LIBELLE_MODELE():String
		{
			return _LIBELLE_MODELE;
		}
		public function set LIBELLE_MODELE(value:String):void
		{
			_LIBELLE_MODELE = value;
		}
		public function set ID_MODELE(value:Number):void
		{
			_ID_MODELE = value;
		}
	}
}