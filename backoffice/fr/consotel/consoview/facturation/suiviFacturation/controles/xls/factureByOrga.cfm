<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="UTF-8"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
 xmlns:s="uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Version>11.6360</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>10005</WindowHeight>
  <WindowWidth>10005</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>135</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
	    <ss:Styles>
	        <ss:Style ss:ID="1">
	            <ss:Font ss:Bold="1"/>
				<ss:Borders>
				    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
				    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
				     ss:Color="#000000"/>
			   </ss:Borders>
	        </ss:Style>
	  <ss:Style ss:ID="s21">
	   <ss:Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	  </ss:Style>
	  <ss:Style ss:ID="s20" ss:Name="Pourcentage">
   	  <ss:NumberFormat ss:Format="0%"/>
  </ss:Style>
	   <ss:Style ss:ID="s24" ss:Name="Total3">
   <ss:Borders>
    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
   </ss:Borders>
   <ss:Font ss:Bold="1"/>
  </ss:Style>
	  <ss:Style ss:ID="s30">
	   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Center" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Font ss:Bold="1"/>	  
	  </ss:Style>
	  
	  <ss:Style ss:ID="s31">
	   <ss:Alignment ss:Vertical="Center" ss:Horizontal="Right" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Font ss:Bold="1"/>	  
	  </ss:Style>
	  
		<ss:Style ss:ID="s16" ss:Name="Euro">
		   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s17" ss:Name="Total">
		   	<ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
	  	
		<ss:Style ss:ID="s18" ss:Name="Total1">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3" ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
		<ss:Style ss:ID="s19" ss:Name="Total2">
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
	  	
	  	<ss:Style ss:ID="s50" ss:Name="PourCent">
		   	<ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;%;&quot;_-;\-* #,##0.00\ &quot;%;&quot;_-;_-* &quot;-&quot;??\ &quot;%;&quot;_-;_-@_-"/>
			<ss:Font ss:Bold="1"/>
			<ss:Borders>
			    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
			    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
			     ss:Color="#000000"/>
		   </ss:Borders>
	  	</ss:Style>
	  	
	  	<ss:Style ss:ID="s35" ss:Parent="s24">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <Font ss:Bold="1"/>
	   <ss:Interior/>
	   <ss:NumberFormat ss:Format="#,##0"/>
	  </ss:Style>
	  <ss:Style ss:ID="s36" ss:Parent="s20">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <Font ss:Bold="1"/>
	   <ss:Interior/>
	  </ss:Style>
	  <ss:Style ss:ID="s41" ss:Parent="s20">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <Font ss:Bold="1"/>
	   <ss:Interior/>
	   <ss:NumberFormat ss:Format="Percent"/>
	  </ss:Style>
	  <ss:Style ss:ID="s37" ss:Parent="s21">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Interior/>
	   <ss:NumberFormat ss:Format="#,##0"/>
	  </ss:Style>
	  <ss:Style ss:ID="s38" ss:Parent="s20">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Interior/>
	  </ss:Style>
	<ss:Style ss:ID="s39">
	   <ss:Alignment ss:Vertical="Bottom" ss:WrapText="1"/>
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	  </ss:Style>
	<ss:Style ss:ID="s40">
	   <ss:Borders>
	    <ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"
	     ss:Color="#000000"/>
	    <ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
	     ss:Color="#000000"/>
	   </ss:Borders>
	   <ss:Interior/>
	  </ss:Style>
	</ss:Styles>
		<cfset totalAnalytique = 0>
	    <Worksheet ss:Name="Liste des Lignes par Site">
	      <Table>
	            <ss:Column ss:Width="550"/>
	            <ss:Column ss:Width="100"/>
	            <ss:Column ss:Width="100"/>
	            <ss:Column ss:Width="150"/>
	            <ss:Column ss:Width="150"/>
	            <ss:Column ss:Width="100"/>
	            <ss:Column ss:Width="130"/>
	            <ss:Column ss:Width="100"/>
	            <ss:Column ss:Width="200"/>
	            <ss:Column ss:Width="130"/>
	            <cfoutput>
		        
		        <ss:Row>
				<cfif SESSION.USER.DEVISEINFOS.SYMBOL EQ "€">
				 	<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Facture n&deg; #myFacture.getNumero()#   montant: #myFacture.getMontant()# #SESSION.USER.DEVISEINFOS.SYMBOL#  op&eacute;rateur: #myFacture.getOperateurLibelle()# CF.: #myFacture.getCompteFacturation()#</ss:Data>
	               	</ss:Cell>
					<cfelse>
						<ss:Cell ss:StyleID="s30">
							<ss:Data ss:Type="String">Facture n&deg; #myFacture.getNumero()#   montant: #SESSION.USER.DEVISEINFOS.SYMBOL##myFacture.getMontant()#  op&eacute;rateur: #myFacture.getOperateurLibelle()# CF.: #myFacture.getCompteFacturation()#</ss:Data>
						</ss:Cell>
				</cfif>
		         	<ss:Cell>
	                    <ss:Data ss:Type="String"></ss:Data>
	                </ss:Cell>
	               
	            </ss:Row>
	            
	            <ss:Row>
	            	               
	            </ss:Row>	            	                
				
				<ss:Row>
	               
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">localisation</ss:Data>
	                </ss:Cell>
	                
					<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Lignes</ss:Data>
	                </ss:Cell>
					
					<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Fonction</ss:Data>
	                </ss:Cell>
					
					<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Collaborateur Annuaire</ss:Data>
	                </ss:Cell>
					
					<ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">Collaborateur SI Opérateur</ss:Data>
	                </ss:Cell>
					
	                <ss:Cell ss:StyleID="s30">
	                    <ss:Data ss:Type="String">montant</ss:Data>
	                </ss:Cell>
	            </ss:Row>
				
				</cfoutput>
 	           	<cfoutput query="aDetail">
				<cfset totalAnalytique = totalAnalytique + val(#MONTANT#)> 
					<ss:Row>
			            
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#replace(TRIM(CHEMIN),"-->>","/","all")#</ss:Data>
		                </ss:Cell>		               
		                <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#SOUS_TETE#</ss:Data>
		                </ss:Cell>	
						 <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#FONCTION#</ss:Data>
		                </ss:Cell>	
						 <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#COLLABORATEUR_ANU#</ss:Data>
		                </ss:Cell>	
						 <ss:Cell ss:StyleID="s21">
		                    <ss:Data ss:Type="String">#COLLABORATEUR#</ss:Data>
		                </ss:Cell>								               
		                <cfif SESSION.USER.DEVISEINFOS.SYMBOL EQ "€">
							<ss:Cell ss:StyleID="s16">
								<ss:Data ss:Type="String">#MONTANT# #SESSION.USER.DEVISEINFOS.SYMBOL#</ss:Data>
							</ss:Cell>
							<cfelse>
								<ss:Cell ss:StyleID="s16">
									<ss:Data ss:Type="String">#SESSION.USER.DEVISEINFOS.SYMBOL##MONTANT#</ss:Data>
								</ss:Cell>
						</cfif>
		            </ss:Row>
				</cfoutput>
					<ss:Row>
		            	               
		            </ss:Row>
					<ss:Row>
		                <ss:Cell ss:StyleID="s31">
		                    <ss:Data ss:Type="String">Total analytique</ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>						
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>
						<ss:Cell ss:StyleID="s17">
		                    <ss:Data ss:Type="Number"><cfoutput>#totalAnalytique#</cfoutput></ss:Data>
		                </ss:Cell>
		                
		            </ss:Row>
		            <ss:Row>
		               
		                
		                <ss:Cell ss:StyleID="s31">
		                    <ss:Data ss:Type="String">Total non affect&eacute; </ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>						
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>		                
		                <ss:Cell ss:StyleID="s17">
		                    <ss:Data ss:Type="Number"><cfoutput>#myFacture.getMontant() - totalAnalytique#</cfoutput></ss:Data>
		                </ss:Cell>		                
		            </ss:Row>		
		            <ss:Row>
		               
		                
		                <ss:Cell ss:StyleID="s31">
		                    <ss:Data ss:Type="String">% du montant vis&eacute;</ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>						
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>
						<ss:Cell>
		                    <ss:Data ss:Type="String"></ss:Data>
		                </ss:Cell>         
		                
		                <cfif myFacture.getMontant() neq 0 >
			                <cfoutput>
				                <ss:Cell ss:StyleID="s50">
				                    <ss:Data ss:Type="Number">#(totalAnalytique * 100 / myFacture.getMontant())#</ss:Data>
				                </ss:Cell>
			                </cfoutput>
			            <cfelse>
			            	<cfoutput>
				                <ss:Cell ss:StyleID="s50">
				                    <ss:Data ss:Type="Number"></ss:Data>
				                </ss:Cell>
			                </cfoutput>
			            </cfif>
			            
		            </ss:Row>		
	        </Table>
	    </Worksheet>
 	</Workbook>