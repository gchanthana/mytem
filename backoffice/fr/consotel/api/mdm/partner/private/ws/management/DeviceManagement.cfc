<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.DeviceManagement" hint="Cette classe n'est pas utilisée (Peut être supprimée)">
	<cffunction access="package" name="getService" returntype="fr.consotel.api.mdm.partner.private.ws.management.DeviceManagement" hint="Lève une exception">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée par cette classe">
	</cffunction>
</cfcomponent>