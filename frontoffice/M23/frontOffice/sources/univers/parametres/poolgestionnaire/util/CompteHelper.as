package univers.parametres.poolgestionnaire.util
{
	import mx.collections.ArrayCollection;
	
	import univers.parametres.poolgestionnaire.vo.CompteOperateur;
	
	public class CompteHelper
	{
		public function CompteHelper()
		{
		}
		/**
		 * 
		 * Retourne un tableau d'id de sous compte 
		 * Qui ont pour compte le compte passé en paramêtre
		 * Qui ont la propriété boolSelected différente de celle passé en parametre
		 * Si le sous compte est déja bien formaté on ne l'ajoute pas au tableau --> sinon appel inutil de procédures  + bug dans le nombre de sous compte selectionnés
		 * Si la propriété boolSelected est différente de celle passé en parametre --> maj de la propriété boolSelected
		 * boolSelected = si l'élément est selectionné dans le grid
		 * 
		 * */ 
		public function getIDSousCompteOfCompteNotFormated(compteOperateur : CompteOperateur,colCompte : ArrayCollection,boolSelected :Boolean):Array
		{
			var arrIDSousCompte : Array= new Array();
			for(var a:int=0;a<colCompte.source.length;a++)
			{
				var curentObj : CompteOperateur = colCompte.source[a] as CompteOperateur;
				if(!curentObj.isCompteMaitre && compteOperateur.idCompteOP == curentObj.idCompteOP)
				{
					if(curentObj.boolSelected != boolSelected){
						curentObj.boolSelected = boolSelected;
						arrIDSousCompte.push(curentObj.idSousCompteOP);
						colCompte.itemUpdated(curentObj);
					}
				}
			}
			return arrIDSousCompte;
		}
		/**
		 * 
		 * Si tout les sous comptes d'un compte sont séléctionné : coche le compte
		 * Si un sous compte d'un compte n'est pas selectionné : Décoche le compte 
		 * 
		 * */
		public function formatListeCompte(colCompte : ArrayCollection):void
		{
			var idLastMaitre :int = 0;
			for(var i:int=0; i < colCompte.length;i++){	
						
				var  item : CompteOperateur = colCompte.getItemAt(i) as CompteOperateur;
				if(item.isCompteMaitre)
				{
					idLastMaitre = i;
					item.boolSelected = true;
				}
			
				if(!item.boolSelected)
				{
					colCompte.getItemAt(idLastMaitre).boolSelected = false;
				}
				colCompte.itemUpdated(item);
			}
		}

	}
}