package catalogue.ihm.parametre
{
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import catalogue.event.CatalogueEvent;
	import catalogue.event.PopUpCatalogueEvent;
	import catalogue.ihm.majcatalogueclient.MajCatalogueClientIHM;
	import catalogue.ihm.popup.PopupFilterIHM;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.strategie.ICatalogue;
	import catalogue.strategie.strategieconcrete.DistributeurCS;
	import catalogue.util.TarifDatagrid;
	import catalogue.util.custcb.ComboCheck;
	import catalogue.vo.ConstructeurVO;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.TypeEquipementVO;
	
	import composants.util.ConsoviewAlert;


	[Bindable]
	public class AddEquipementRevendeurImpl extends TitleWindow
	{
	
		public var items:ArrayCollection=new ArrayCollection();		
		
		private var _catalogueCS:ICatalogue;
		private var _equipement:EquipementCatalogueVO = new EquipementCatalogueVO();
		private var _popUpFilter:PopupFilterIHM = new PopupFilterIHM();
		private var boolContinueAdd:Boolean = false;
		public var globalCatalogueService : GlobalCatalogueService = new GlobalCatalogueService();
		public var listClts:ArrayCollection; //Liste de tous les clients d'un distributeur		
		public var listclient:ArrayCollection= new ArrayCollection();//liste des clients séléctionnés dans le combobox 
		
		//Public UI componant
		public var comboClt:ComboCheck;
		public var comboType : ComboBox;
		public var cbAddInCatConstr : CheckBox;
		public var cbPrevenirClient : CheckBox;
		public var cbSuspendu : CheckBox;
		public var cbAjouterEquipOtherCat : CheckBox;
		
		public var comboConstructeur : ComboBox;
		public var inputLib:TextInput;
		public var inputRef:TextInput;
		public var dgTarif : TarifDatagrid;
		
		
		public function AddEquipementRevendeurImpl()
		{
				
		}
		public function initData():void
		{
			catalogueCS = new DistributeurCS();
			
			if(equipement.IDEQUIPEMENT>0)
			{
				initMode_update();
			}
			else
			{
				initMode_create();
			}
			
			catalogueCS.rechercheTypeEquipement();
			catalogueCS.rechercheContructeur();
			setDataCltToCombo(listClts); //préparer les données pour le combobox Client
		}
		
		private function initMode_update():void
		{
			registerClassAlias("catalogue.vo.EquipementCatalogueVO",EquipementCatalogueVO);  
			equipement= EquipementCatalogueVO(ObjectUtil.copy(equipement));  // delete les ref sur le grid
			
			
			catalogueCS.addEventListener(CatalogueEvent.LISTE_TYPE_EQUIPEMENT_COMPLETE,listeTypeComplete);
			catalogueCS.addEventListener(CatalogueEvent.LISTE_CONSTRUCTEUR_EQUIPEMENT_COMPLETE,listeConstructeurComplete);
		}
		
		private function listeTypeComplete(evt : CatalogueEvent):void
		{
			utilFunctionInitCombo(comboType,'ID_TYPE',equipement.IDTYPE);
		}
		private function listeConstructeurComplete(evt : CatalogueEvent):void
		{
			utilFunctionInitCombo(comboConstructeur,'ID_CONSTRUCTEUR',equipement.IDCONSTRUCTEUR);
		}
		private function utilFunctionInitCombo(combo: ComboBox, labelFieldOfID : String, searchValue : Number):void
		{
			for(var i:int=0;i<(combo.dataProvider as ArrayCollection).length;i++)
			{
				if((combo.dataProvider as ArrayCollection).getItemAt(i)[labelFieldOfID]==searchValue)
				{
					combo.selectedIndex = i;
					break;
				}
			}
		}
		private function initMode_create():void
		{
						
		}
		private function initAttribut():void
		{
			
		}
		/*
		 * préparer les données pour le combobox Client
		 */
		private function setDataCltToCombo(list:ArrayCollection):void
		{			
			for (var i:int=-1;i<list.length;i++) {
				var item1:Object = new Object();
				if (i==-1) {
					item1.label = ResourceManager.getInstance().getString('M24','Tous');
					item1.value = i;
					item1.assigned=true;
				} else {
					item1.label = list.getItemAt(i).LIBELLE_CLIENT;
					item1.value = i;
					item1.assigned =list.getItemAt(i).assigned;
					item1.ID_CLIENT=list.getItemAt(i).ID_CLIENT;
				}
				items.addItem(item1);
				//trace(items.getItemAt(i+1).label+","+items.getItemAt(i+1).value+","+items.getItemAt(i+1).assigned+","+items.getItemAt(i+1).ID_CLIENT);
			}
		}
		
		public function fermer(event : Event=null):void
		{
			PopUpManager.removePopUp(this);			
		}
		public function valider():void
		{
			
		}
		public function combo_revendeur_commande_handler(evt : Event):void
		{
			
		}
		public function btValiderContinuerHandler():void
		{
			boolContinueAdd = true;
			
			validerPop();
			
		}
		public function btValiderFermerHandler():void
		{
			boolContinueAdd = false;
			validerPop();
		}
		public function cbAddInCatConstrHandler():void
		{
			if(cbAddInCatConstr.selected)
			{
				equipement.ADDINCATCONSTR = 1;
			}
			else
			{
				equipement.ADDINCATCONSTR = 0;
			}	
		}
		public function cbSuspenduHandler():void
		{
			if(cbSuspendu.selected)
			{
				equipement.SUSPENDU = 1;
			}
			else
			{
				equipement.SUSPENDU = 0;
			}	
		}
		private function validerPop():void
		{
			var boolAllTarifValide : Boolean = true;			
			var rowRenderers:Array = dgTarif.listRendererArray;
			listclient=new ArrayCollection();
			
			for (var j :int =0; j<comboClt.selectedItems.length;j++){
				if (comboClt.selectedItems.getItemAt(j).value != -1)
				{	
					listclient.addItem(comboClt.selectedItems.getItemAt(j).ID_CLIENT);
					//trace(listclient.getItemAt(j));
				}
			}
			
			for(var i:int = 0; i < rowRenderers[0].length; i++)
			{
				if(rowRenderers[0][i].tarifIsValide==false)
				{
					boolAllTarifValide = false;
				}
			}
			
			if(boolAllTarifValide)
				if(equipement.IDEQUIPEMENT>0)
				{
					//update
					catalogueCS.addEventListener(CatalogueEvent.UPDATE_EQUIPEMENT_COMPLETE,update_equipement_complete_handler);
					catalogueCS.updateEquipementRev(equipement,listclient);
				}
				else
				{
					//add
					catalogueCS.addEventListener(CatalogueEvent.ADD_EQUIPEMENT_COMPLETE,add_equipement_complete_handler);
					catalogueCS.addEquipementRev(equipement,listclient);
					
				}
				else
				{
					Alert.show(ResourceManager.getInstance().getString('M24','Tous_les_tarifs_doivent__tre_valides'), ResourceManager.getInstance().getString('M24','Erreur'));
				}
		}
		private function update_equipement_complete_handler(evt : CatalogueEvent):void
		{
			
			if(evt.objectReturn> 0)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24','Equipement_modifi_'));
				dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			}
			if(!boolContinueAdd)
			{
				fermer();
			}
		}
		private function add_equipement_complete_handler(evt : CatalogueEvent):void
		{
			//dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			if((evt.objectReturn.hasOwnProperty('IDEQUIP')) && (evt.objectReturn.IDEQUIP == -10))
			{
				Alert.show(ResourceManager.getInstance().getString('M24','Le_mod_le_existe_d_j__dans_le_catalogue'));
			}
			else
			{
				//ConsoviewAlert.afficherOKImage('Equipement ajouté. Référence consotel : '+evt.objectReturn.REF);
				
				
				if(cbAjouterEquipOtherCat.selected || cbPrevenirClient.selected)
				{
					var pop : MajCatalogueClientIHM = new MajCatalogueClientIHM();
					pop.boolAdd= cbAjouterEquipOtherCat.selected;
					var col : ArrayCollection = new ArrayCollection();
					col.addItem(equipement);
					equipement.IDEQUIPEMENT = evt.objectReturn.IDEQUIP;
					pop.listeEquipement =  col;
					pop.boolPrevenir = cbPrevenirClient.selected;
					pop.idDistributeur = equipement.IDREVENDEUR;
					PopUpManager.addPopUp(pop,this,true);
					PopUpManager.centerPopUp(pop);
					
				}
				if(boolContinueAdd)
				{
					var backupIdRevendeur : int= equipement.IDREVENDEUR;
					equipement = new EquipementCatalogueVO();
					equipement.IDREVENDEUR = backupIdRevendeur;
					
					comboConstructeur.selectedIndex = -1 ;
					comboType.selectedIndex = -1;
					inputLib.errorString = '';
					inputRef.errorString = '';
				}
				else
				{
					
					fermer();
				}
				dispatchEvent(new PopUpCatalogueEvent(PopUpCatalogueEvent.REFRESH_PARENT_DATA));
			}
		}

		public function set catalogueCS(value:ICatalogue):void
		{
			_catalogueCS = value;
		}

		public function get catalogueCS():ICatalogue
		{
			return _catalogueCS;
		}

		public function set equipement(value:EquipementCatalogueVO):void
		{
			
			_equipement = value;
		}

		public function get equipement():EquipementCatalogueVO
		{
			return _equipement;
		}
		public function comboTypeHandler():void
		{
			if(comboType.selectedItem)
			{
				equipement.IDTYPE = (comboType.selectedItem as TypeEquipementVO).ID_TYPE
			}
		}
		
		public function comboConstructeurHandler():void
		{
			if(comboConstructeur.selectedItem)
			{
				equipement.IDCONSTRUCTEUR = (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;
				resetSelection((comboConstructeur.dataProvider as ArrayCollection).source);
				comboConstructeur.selectedItem.SELECTED = true;
			}
		}
		
		
		public function btImgClickHandler():void
		{
			_popUpFilter = new PopupFilterIHM();
			_popUpFilter.LIBELLE_ITEM = 'LIBELLE_CONSTRUCTEUR';
			_popUpFilter.ID_ITEM = 'ID_CONSTRUCTEUR';
			_popUpFilter.listeFilter =  ObjectUtil.copy(catalogueCS.listeConstructeur) as ArrayCollection;
			_popUpFilter.titlePopup = ResourceManager.getInstance().getString('M24', 'Choisir_un_constructeur');
			_popUpFilter.addEventListener('VALID_CHOIX_Filter', _popUpFilterValidHandler);
			PopUpManager.addPopUp(_popUpFilter, this, true);
		}
		
		private function _popUpFilterValidHandler(e:Event):void
		{
			_popUpFilter.removeEventListener('VALID_CHOIX_Filter', _popUpFilterValidHandler);
			resetSelection((comboConstructeur.dataProvider as ArrayCollection).source);
			comboConstructeur.selectedIndex = _popUpFilter.getItemCurrentIndex();
			comboConstructeur.selectedItem.SELECTED = true;
			equipement.IDCONSTRUCTEUR = (comboConstructeur.selectedItem as ConstructeurVO).ID_CONSTRUCTEUR;

		}
		
		private function resetSelection(tab:Array):void
		{
			if(tab)
			{
				var len:Number =  tab.length;
				for(var index:Number = 0; index > len; index++)
				{
					tab[index].SELECTED = false;
				}
			}
		}
		
	}
}