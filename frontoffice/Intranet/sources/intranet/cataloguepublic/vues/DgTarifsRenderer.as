package intranet.cataloguepublic.vues
{
	import intranet.cataloguepublic.vo.GroupeProduitControle;
	
	import mx.controls.TextInput;
	
	
	public class DgTarifsRenderer extends TextInput
	{
		public function DgTarifsRenderer()
		{
			alpha = 0;
			setStyle("borderStyle", "none"); 
			editable = false;
		}
		
		override public function set data(value:Object):void {
            super.data = value;
	         if (value){
	         	if (GroupeProduitControle(value).dateDebut == null)
	         		setStyle("color", 0xff9900); 
	         	else
		         	setStyle("color", 0x000000);
	         }
  		}
  		
  		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
  			super.updateDisplayList(unscaledWidth, unscaledHeight);
  			
  		}
	}
}