package composants.tb.ligne.entity
{
	[Bindable]
	public class FicheEvolutionVO
	{
		public var IDPERIODE:Number = 0;
		public var PERIODE:String = "";
		public var PRODUIT:String = "";
		public var THEME:String = "";
		public var TYPE:String = "";
		public var IDTYPE:Number = 0;
		public var QTE:Number = 0;
		public var VOLUME:Number = 0;
		public var UNITE:String = "";
		public var MONTANT:Number = 0;
		public var ORDRE_AFFICHAGE:Number = 0;
		
		public function FicheEvolutionVO()
		{
		}
		
		public function fill(value:Object):void
		{
			this.IDPERIODE = value.IDPERIODE;
			this.PERIODE = value.PERIODE;
			this.PRODUIT = value.PRODUIT;
			this.THEME = value.THEME;
			this.TYPE = value.TYPE;
			this.IDTYPE = value.IDTYPE;
			this.QTE = value.QTE;
			this.VOLUME = value.VOLUME_MN;
			this.UNITE = value.UNITE_VOL_MN;
			this.MONTANT = value.MONTANT;
			this.ORDRE_AFFICHAGE = value.ORDRE_AFFICHAGE;
		}
	}
}