package univers.facturation.optimisation.rationalisation {
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import composants.tb.periode.AMonth;
	import composants.util.DateFunction;
	import composants.tb.periode.PeriodeEvent;
	import flash.net.URLRequest;
	import mx.events.DropdownEvent;
	import mx.controls.Menu;
	import mx.events.MenuEvent;
	import flash.events.MouseEvent;
	import univers.facturation.optimisation.AbstractReport;
	import flash.net.URLVariables;
	import flash.net.URLRequestMethod;
	
	public class Rationalisation extends RationalisationIHM {
		public function Rationalisation() {
			super();
			reportType = "Rationalisation";
			selectorThumbCount = 2; // 2 Mois à choisir
			formatList = new Array(2);
			var xlsObj:Object = new Object();
			var csvObj:Object = new Object();
			xlsObj.label = AbstractReport.XLS_FORMAT_LABEL;
			xlsObj.value = AbstractReport.XLS_FORMAT_VALUE;
			csvObj.label = AbstractReport.CSV_FORMAT_LABEL;
			csvObj.value = AbstractReport.CSV_FORMAT_VALUE;
			xlsObj.enabled = csvObj.enabled = true;
			formatList[0] = xlsObj;
			formatList[1] = csvObj;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public override function getReportName():String {
			return "Rapport de Rationalisation";
		}
		
		protected override function initIHM(event:FlexEvent):void {
			trace("Rationalisation initIHM");
			super.initIHM(event);
			cmbGpeProduits.addEventListener(DropdownEvent.CLOSE,onGroupeProduitChange);
			getListeGroupesProduits(null);
		}
		
		protected override function getReportUrlRequest(secureBool:Boolean = true):URLRequest {
			super.getReportUrlRequest(secureBool);
			urlRequestPath.data.PRODUITID = cmbGpeProduits.selectedItem.PRODUIT_ID;
			return urlRequestPath;
		}

		protected override function checkAndUpdateActionItemStatus():Boolean {
			if(this.initialized) {
				if(cmbGpeProduits.selectedItem != null) {
					var tmpProduitId:int = parseInt(cmbGpeProduits.selectedItem.PRODUIT_ID,10);
					if(tmpProduitId != 0) // Groupe de Produits
						actionItem.enabled = true;
					else // Choisir un groupe de produits
						actionItem.enabled = false;
				} else
					actionItem.enabled = false;
			} else
				actionItem.enabled = false;
			return actionItem.enabled;
		}
		
		private function getListeGroupesProduits(event:Event):void {
			if(event == null) {
				var cfcName:String = "RapportRationalisation" + 
						CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE + "Strategy";
				var getListeGroupProduitsOp:AbstractOperation =
							RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.rapports.Rationalisation." + cfcName,
																		"getListeGroupesProduits",
																		getListeGroupesProduits);
				RemoteObjectUtil.callService(getListeGroupProduitsOp,null);
			} else {
				if(event.type == ResultEvent.RESULT) {
					cmbGpeProduits.dataProvider = (event as ResultEvent).result;
					cmbGpeProduits.labelField = "PRODUIT";
					cmbGpeProduits.selectedItem = cmbGpeProduits.dataProvider[0];
				}
			}
		}

		private function onGroupeProduitChange(event:DropdownEvent):void {
			trace("Rationalisation onGroupeProduitChange()");
			checkAndUpdateActionItemStatus();
		}
	}
}
