﻿<cfcomponent output="false" displayname="fr.consotel.consoview.mobile.GetMatriceInfo">
	<cffunction name="getData" access="remote" returntype="any" output="false" 				
				description="Les infos présents dans la ligne de la matrice" 
				hint="liste des sites d'un pool">
					
		<cfargument name="idRacine" 	type="numeric" 	required="true" hint="id de la racine du demandeur">
		<cfargument name="idPool" 		type="numeric" 	required="true" hint="id du pool de gestion du demandeur">
		<cfargument name="idMatrice" 	type="numeric" 	required="true" hint="id de la ligne de la matrice saaswedo">		
				
		<cfset q_mtx_info = ''>
					
		<cfquery name="q_mtx_info" datasource="ROCOFFRE" >
			select * from pooL_EQUI_EMP_LGN where idracine = #idRacine# AND ID = #idMatrice# and idpool = #idPool#
		</cfquery>
<!--- 
			ID	1033585
IDPOOL	19118
IDRACINE	2458788
IDEMPLOYE	184239
IDTERMINAL	325416
IDSIM	
IDSOUS_TETE	2768868
IDTYPE_LIGNE	84
FLAG_SOUS_TETE	1
T_SEGMENT_MOBILE	0
S_SEGMENT_MOBILE	
T_SEGMENT_FIXE	1
S_SEGMENT_FIXE	
T_SEGMENT_DATA	1
S_SEGMENT_DATA	
TERMINAL	4400
T_IMEI	
T_MODELE	4400
T_NO_SERIE	PABX_DEM_00001
T_ID_STATUT	1
T_IDTYPE_EQUIP	15
T_NIVEAU	
T_PRODUCT_ID	
T_IDEMPLACEMENT	
T_IDSITE	
T_PIN1	
T_PIN2	
T_PUK1	
T_PUK2	
T_BOOL_MDM	0
T_ZDM_SERIAL_NUMBER	
T_ZDM_IMEI	
T_IDFABRIQUANT	9182
T_IDREVENDEUR	27284
T_REVENDEUR	Orange
T_FABRIQUANT	ALCATEL
T_REV_OP	
T_FAB_OP	
SIM	
S_IMEI	
S_MODELE	
S_NO_SERIE	
S_ID_STATUT	
S_IDTYPE_EQUIP	
S_NIVEAU	
S_PRODUCT_ID	
S_IDEMPLACEMENT	
S_IDSITE	
S_PIN1	
S_PIN2	
S_PUK1	
S_PUK2	
S_BOOL_MDM	
S_ZDM_SERIAL_NUMBER	
S_ZDM_IMEI	
S_IDFABRIQUANT	
S_IDREVENDEUR	
S_REVENDEUR	
S_FABRIQUANT	
S_REV_OP	
S_FAB_OP	
T_PRIX	
S_PRIX	
SOUS_TETE	0142218500
ST_COMMENTAIRES	
ST_SEGMENT_MOBILE	0
ST_SEGMENT_FIXE	1
ST_SEGMENT_DATA	0
IDTETE_LIGNE	2768868
OPERATEURID_ACCES	63
OPERATEURID_INDIRECT_1	938
IDTYPE_RACCORDEMENT	
ST_IDETAT	1
ACCESS_LAST_FACTURE	
IDPRODUIT_ACCES	
NOM	Guillaume
PRENOM	Chanthana
INOUT	1
E_NIVEAU	
E_MATRICULE	CON001
E_DATE_ENTREE	01/08/2000
E_EMAIL	g@consotel.fr
E_CLE_IDENTIFIANT	CON001
E_REFERENCE_EMPLOYE	
E_CODE_INTERNE	
E_DATE_SORTIE	
SEGMENT_MOBILE	0
SEGMENT_FIXE	1
SEGMENT_DATA	0
TYPE_LIGNE	N° azur
TYPE_FICHE	SER
TYPE_RACCORDEMENT	
TETE_LIGNE	0142218500
SID_TYPE_LIGNE	21466
SID_TYPE_RACCORDEMENT	
T_ZDM_DEVICE_ID	
S_ZDM_DEVICE_ID	

--->
		<cfreturn q_mtx_info>
	</cffunction>
</cfcomponent>