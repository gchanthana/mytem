﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	
	<!---recuperer la liste des Niveaux --->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true" hint="ne contient rien">
		
		<cftry>
			
			<cfstoredproc procedure="pkg_m28.getAllNiveaux" datasource="ROCOFFRE">
					<cfprocresult name="p_retour" >
			</cfstoredproc>
			
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL004", 1, data)>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL004", 0, err)/>
			</cfif>
			<cfcatch type="any" >
				<cfreturn CreateReponse("AL004", 0, ThrowCfCatchError("AL004",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>