package composants.tb.tableaux.renderer
{
	import mx.controls.TextInput;
	import mx.controls.Label;
	import univers.facturation.tb.TableauDeBord;
	import mx.states.SetStyle;
	import mx.rpc.events.ResultEvent;
	import mx.events.DataGridEvent;
	import mx.controls.Alert;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.debugger.enterDebugger;
	import mx.rpc.events.FaultEvent;
	import mx.controls.dataGridClasses.DataGridListData;
	
	import mx.events.FlexEvent;
	import flash.events.FocusEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;
	import mx.utils.ObjectUtil;
	
	public class FunctionCellField extends TextInput
	{		
        // Define the constructor and set properties.
       private var opEdition : AbstractOperation;
       
       public function FunctionCellField() {
       	 		 super();      
       	 		 setStyle("borderStyle", "none");   
       	 		 setStyle("backgroundAlpha",0);
       	 		 addEventListener(FlexEvent.ENTER,enregistrerDonnee);       	 		
        }	

        // Override the set method for the data property.
        override public function set data(value:Object):void {
            super.data = value;
       		
       		if (value) {				
                super.data = value;			
				if (String(value.SITE).length == 0){					
					setStyle("color","black");
					editable = TableauDeBord.editable; 
				}else{
					super.editable = false;
					editable = false;
					setStyle("color","black");
				}
			
			}else{
				editable = false;
			}
           
           	super.invalidateDisplayList();
        }
        
         protected function enregistrerDonneeResultHandler(re :ResultEvent):void{
			setStyle("color","red");
		 }  
		 
		 protected function defaultHandler(fe : FaultEvent):void{
		 	Alert.show("Erreur Remoting");
		 }
		 
		 protected function enregistrerDonnee(ev : FlexEvent):void{
		 	
		 	trace(data.IDSOUS_TETE);
		 	
		 
		 	
		 	opEdition  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.produits.facade",
																			"updateFunction",
																			enregistrerDonneeResultHandler,
																			defaultHandler);
	
			RemoteObjectUtil.callService(opEdition,
										data.IDSOUS_TETE,
										text
										); 
			
		 }					
    }	
}