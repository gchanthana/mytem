package rapport.utils.perimetreselector
{
	import flash.events.Event;

	public class ConsoViewModuleEvent extends Event
	{
		public static const INIT_OK:String = "INIT_OK";
		
		public function ConsoViewModuleEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}