package gestioncommande.events
{
	import flash.events.Event;

	public class PopUpTargetDatagridEvent extends Event
	{
		
		public static const ITEM_SELECTED:String = "itemSelected";
		private var _labelSelected:String;
		private var _idCibleSelected:Number;
		
		public function PopUpTargetDatagridEvent(type:String, bubbles:Boolean=false, labelSelected:String = "", idCibleSelected:Number = 0)
		{
			super(type, bubbles);
			this._labelSelected = labelSelected;
			this._idCibleSelected = idCibleSelected;
		}
		
		public function get labelSelected():String {
			return _labelSelected;
		}
		
		public function get idCibleSelected():Number {
			return _idCibleSelected;
		}
		
		override public function clone():Event {
			return new PopUpTargetDatagridEvent(type, bubbles, labelSelected, idCibleSelected);
		}
		
	}
}