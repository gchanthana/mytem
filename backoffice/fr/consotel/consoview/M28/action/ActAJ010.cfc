<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Insertion/mise à jour des sites" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient 11 variables au plus, 3 au moins">
		
		<cftry>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.IUSite">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idfc_site#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_Fc_Partenaireid#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_libelle_site#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_adresse1#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_adresse2#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_code_postal#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_ville#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.p_pays#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_bool_adresse_facturation#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_Date_Deb, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_Date_Fin, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
			
			<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ010", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ010", 0, err)/>
			<cfelseif p_retour EQ -10>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de chevauchement de date", "", "")>
				<cfreturn CreateReponse("AJ010", 0, err)/>
			<cfelseif p_retour EQ -20>
				<cfset err = ThrowCfCatchError(p_retour, "", "Libellé de site déjà existant", "", "")>
				<cfreturn CreateReponse("AJ010", 0, err)/>
			<cfelseif p_retour EQ -100>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de paramètres d'entrées obligatoires", "", "")>
				<cfreturn CreateReponse("AJ010", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ010", 0, ThrowCfCatchError("AJ010",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>