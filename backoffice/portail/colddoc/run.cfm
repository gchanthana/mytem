<!---
Let's generate our default HTML documentation on myself: 
 --->
<cfscript>
	colddoc = createObject("component", "portail.colddoc.ColdDoc").init();

	strategy = createObject("component", "portail.colddoc.strategy.api.HTMLAPIStrategy").init(expandPath("./docs"), "DOC PORTAIL END USER");
	colddoc.setStrategy(strategy);

	//colddoc.generate(expandPath("/portail/class"), "portail.class");
	paths = [
          { inputDir = expandPath("/portail/class"),inputMapping = "portail.class"}
          ,{inputDir = expandPath("/portail/D1/310812/composant") ,inputMapping = "portail.D1.310812.composant" }
          ,{inputDir = expandPath("/portail/D1/310812/page") ,inputMapping = "portail.D1.310812.page" }
	];
	colddoc.generate(paths);
	
</cfscript>

<h1>Done!</h1>

<a href="docs">Documentation</a>

<!---paths = [
         { inputDir = expandPath("/colddoc"),inputMapping = "colddoc"},{inputDir = expandPath("com") ,inputMapping = "com" }
]; 
colddoc.generate(paths);--->
