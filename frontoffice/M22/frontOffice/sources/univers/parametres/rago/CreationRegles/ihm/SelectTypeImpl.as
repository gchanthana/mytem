package univers.parametres.rago.CreationRegles.ihm
{
	import flash.events.Event;
	import flash.text.engine.JustificationStyle;
	
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextArea;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	import univers.parametres.rago.entity.TypeRegle;

	[Bindable]
	public class SelectTypeImpl extends MainCreateRegleImpl
	{
		
//VARIABLES GLOBALES----
	
		public var _myWizzard:Wizzard;
	
		public var rbgLineFollowRegle	:RadioButtonGroup;
		public var rbgTypeRegle			:RadioButtonGroup;
		
		public var rbTypaAnnuaire		:RadioButton;
		public var rbOperateur			:RadioButton;
		public var rbOrgaClient			:RadioButton;
		
		public var rbLineFollow			:RadioButton;
		public var rbLineNotFollow		:RadioButton;
		
		public var cboxOperateur		:ComboBox;
		public var cboxClienteCible			:ComboBox;
		public var cboxClienteSource		:ComboBox;
		public var txtAreaCommentaireNEwRegle:TextArea;
		
		//public var _jumpException:Boolean = false;
		
		private var _typeResult:Object;
		private var _clientResult:Object;
		private var _textSaisie:String = "";

//FIN VARIABLES GLOBALES----

//PROPRIETEES PUBLICS----
		
		//Texte saisie dans le champs commentaire
		public function set textSaisie(value:String):void
		{ _textSaisie = value; }

		public function get textSaisie():String
		{ return _textSaisie; }
		
		//Résultat du combobox opérateur/annuaire
		public function set typeResult(value:Object):void
		{ _typeResult = value; }

		public function get typeResult():Object
		{ return _typeResult; }
		
		//Résultat du combobox client
		public function set clientResult(value:Object):void
		{ _clientResult = value; }

		public function get clientResult():Object
		{ return _clientResult; }

//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur
		public function SelectTypeImpl()
		{}

//FIN METHODES PUBLICS----

//METHODES PROTECTED----

		//Appel à la procédure permettant de lister toutes les orga (annuaire, opérateur, cliente)
		protected function initSelectTypeIHM(event:Event):void
		{ 
			cboxOperateur.enabled = true;

			rules.fillOrganisation();
			rules.addEventListener("OrgaFind",executeResultListOrgaOpeIntoCbox);
		}
		
		//Appel a la fonction 'rdgAnnuaireChangeHandler' pour lister les orga annuaire
		protected function clickRdBtnCollaborateur(event:Event):void
		{
			rbTypaAnnuaire.selected = true;
			rdgAnnuaireChangeHandler(event);
			_myWizzard.jumpException = false;
		}
		
		//Appel a la fonction 'rdgOperateurChangeHandler' pour lister les orga opérateur
		protected function clickRdBtnCompte(event:Event):void
		{
			rbOperateur.selected = true;
			rdgOperateurChangeHandler(event);
			_myWizzard.jumpException = false;
		}
		
		//Appel a la fonction 'rdgOperateurChangeHandler' pour lister les orga opérateur
		protected function clickRdBtnOrgaClient(event:Event):void
		{
			rbOrgaClient.selected = true;
			rdgOrgaClientChangeHandler(event);
			_myWizzard.jumpException = true;
		}
		
		//Enregistre le texte saisie dans le champs commentaires
		protected function txtCmtrChangeHandler(event:Event):void
		{
			textSaisie = txtAreaCommentaireNEwRegle.text;
			_myWizzard.myRegle.REGLE_COMMENT = textSaisie;
		}
		
		//Affecte la liste des orga annuaire au combobox
		protected function rdgAnnuaireChangeHandler(event:Event):void
		{
			cboxOperateur.enabled 	= false;
			rbLineNotFollow.enabled = false;
			rbLineFollow.selected	= true;
			zAttributFollowInRegle(true);
			zAttributTypeRegleInRegle(TypeRegle.REGLE_ANNUAIRE);
			//typeResult = listOperateurAnnuaire = rules.ListOrgaAnu;
			listOperateurAnnuaire = rules.ListOrgaAnu;
			typeResult = cboxOperateur.selectedItem;
		}
		
		//Affecte la liste des orga opérateur au combobox
		protected function rdgOperateurChangeHandler(event:Event):void
		{
			cboxOperateur.enabled = true;
			rbLineNotFollow.enabled = true;
			zAttributTypeRegleInRegle(TypeRegle.REGLE_OPERATEUR);
			listOperateurAnnuaire = rules.ListOrgaOpe;
			typeResult = cboxOperateur.selectedItem;
		}
		
		//Affecte la liste des orga opérateur au combobox
		protected function rdgOrgaClientChangeHandler(event:Event):void
		{
			cboxOperateur.enabled 	= false;
			rbLineNotFollow.enabled = true;
			rbLineFollow.selected	= true;
			zAttributFollowInRegle(true);
			zAttributTypeRegleInRegle(TypeRegle.REGLE_CLIENT);
			//listOperateurAnnuaire = rules.ListOrgaCliente;
			listCliente = rules.ListOrgaCliente; 
			typeResult = cboxClienteSource.selectedItem;
			
			cboxClienteCible.selectedIndex = -1;
			cboxClienteCible.prompt = ResourceManager.getInstance().getString('M22','Select_');
		}

		//Evènement lors du click sur le bouton 'faire suivre toutes les lignes'
		protected function rdgFollowChangeHandler(event:Event):void
		{ zAttributFollowInRegle(true); }
		
		//Evènement lors du click sur le bouton 'ne pas faire suivre toutes les lignes'
		protected function rdgNoFollowChangeHandler(event:Event):void
		{ zAttributFollowInRegle(false); }

		//Affecte le résultat de la sélection du combobox opérateur/annuaire à la propriétée opérateur/annuaire
		protected function cboxOpeCloseHandler(event:Event):void
		{
			//typeResult = CboxOpeAnnuResult  = cboxOperateur.selectedItem; 
			typeResult  = cboxOperateur.selectedItem; 
		}
		
		/**
		 * Affecte l'orga client origin dans le cas d'une affectation orga ==> orga
		 */
		protected function cboxOrgaClienteSourceCloseHandler(event:Event):void
		{ 
			typeResult = cboxClienteSource.selectedItem;
			cboxClienteCible.selectedIndex = -1;
			cboxClienteCible.prompt = ResourceManager.getInstance().getString('M22','Select_');
		}
		
		//Affecte le résultat de la sélection du combobox client à la propriétée cliente
		protected function cboxClienteCibleCloseHandler(event:Event):void
		{ 
			//clientResult = CboxClienteResult  = cboxCliente.selectedItem;
			if(cboxClienteCible.selectedItem.IDGROUPE_CLIENT != cboxClienteSource.selectedItem.IDGROUPE_CLIENT)
			{
				clientResult  = cboxClienteCible.selectedItem;
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22','S_lectionner_un_autre_client_que_ci_dessus_'), 'Consoview');
				cboxClienteCible.selectedIndex = -1;
				cboxClienteCible.prompt = ResourceManager.getInstance().getString('M22','Select_');
			}
				
		}

//METHODES PROTECTED----

//METHODES PRIVATE----
		
		//Met dans le combobox 'opérateur/annuaire'la liste des opérateur ou annuaire
		//et liste dans le combobox 'client' la liste des orga cliente
		private function executeResultListOrgaOpeIntoCbox(event:Event):void
		{ 
			_myWizzard.myRegle.BOOL_SUIVRE_AUTO = true;
			_myWizzard.myRegle.TYPE_REGLE = TypeRegle.REGLE_OPERATEUR;
			listOperateurAnnuaire = rules.ListOrgaOpe;
			listCliente = rules.ListOrgaCliente; 
			
			//typeResult = CboxOpeAnnuResult = cboxOperateur.selectedItem;
			typeResult = cboxOperateur.selectedItem;
			//clientResult = CboxClienteResult = cboxCliente.selectedItem;
			clientResult = cboxClienteCible.selectedItem;
		}
		
		//Attribut la valeur opérateur ou annuaire à 'TYPE_REGLE'
		private function zAttributTypeRegleInRegle(typeRegle:String):void
		{	
			if(_myWizzard.myRegle != null)
			{ 
				_myWizzard.myRegle.TYPE_REGLE = typeRegle; 
			}
		}
		
		//Attribut la valeur boolean à true ou false à 'BOOL_SUIVRE_AUTO'
		private function zAttributFollowInRegle(followValue:Boolean):void
		{	
			if(_myWizzard.myRegle != null)
			{ _myWizzard.myRegle.BOOL_SUIVRE_AUTO = followValue; }
		}
		
		//Formateur string en bool
		private function zFormatStringInBoolean(strgValueToConvert:String):Boolean
		{
			if(strgValueToConvert == "true") { return true; }
			else { return false; }
		}
		
//METHODES PRIVATE----
		
	}
}