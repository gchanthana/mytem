package univers.inventaire.inventaire.creation.nouvelleResources.commande.cible
{
	import composants.access.perimetre.tree.PerimetreTreeWindow;
	import composants.access.perimetre.tree.SearchPerimetreWindow;
	
	import flash.events.Event;
	
	import mx.containers.Canvas;
	import mx.controls.CheckBox;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	
	
	/**
	 * Classe gerant la selection et l'affichage de la cible 
	 * dans le formulaire de commande
	 * */
	public class Cible extends Canvas
	{
		/**
		 * Référence vers l'arbre
		 * */
		public var treeCible : InvSearchTree;
		
		/**
		 * Référence vers le CheckBox 'Nouvelle cible'
		 * */
		public var cbNewCible : CheckBox;
		
		/**
		 * Constante référençant le type de l'évènement dispatché lors d'un click su la CheckBox
		 * */
		public static const CIBLE_CHANGED : String ="cibleChanged";
		
		//Reference locale vers la commande
		private var _commande : Commande; 
		
		//Reference locale vers le dernier noeud sélectionné
		private var lastSelectedNode : int = -1;
		
		
		public function get modeEcriture():Boolean{
			return (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE);
		}
		
		
		/**
		 * Ne fait rien
		 * */
		public function clean():void{
			
		}
		
		/**
		 * Constructeur
		 * */
		public function Cible()
		{
			super();
		}
		
		/**
		 * Affiche la cible de la commande dans l'arbre 'grisé'
		 * @param cmde la commande
		 * */
		public function affciherCibleCommande(cmde : Commande):void{
			
			_commande = cmde;	
			if (_commande.idGroupeCible != 0){			 	
					trace("on va selectionner la cible " + _commande.idGroupeCible);			
					callLater(afficherCible);
				 
			}
			cbNewCible.selected = Boolean(_commande.flagCible == 1);		
		}
		
		//parametre l'affichage de l'arbre et selectionne le noeud cible de la commande
		private function afficherCible():void{
			
			treeCible.mainView.selectedIndex = 1;
			SearchPerimetreWindow(treeCible.searchTree).resultControl.enabled = false;
			SearchPerimetreWindow(treeCible.searchTree).resultControl.visible = false;
			SearchPerimetreWindow(treeCible.searchTree).searchInput.visible = false;
			SearchPerimetreWindow(treeCible.searchTree).btnBack.visible = false;
			SearchPerimetreWindow(treeCible.searchTree).btnBack.enabled = false;			
			SearchPerimetreWindow(treeCible.searchTree).lblRecherche.visible = false;
						
			SearchPerimetreWindow(treeCible.searchTree).lblRecherche.height = 0;
			SearchPerimetreWindow(treeCible.searchTree).btnBack.height = 0;
			SearchPerimetreWindow(treeCible.searchTree).searchInput.height =0;	
			SearchPerimetreWindow(treeCible.searchTree).resultControl.height = 0;
			SearchPerimetreWindow(treeCible.searchTree).resultControl.height = 0;
			
			SearchPerimetreWindow(treeCible.searchTree).lblRecherche.width= 0;
			SearchPerimetreWindow(treeCible.searchTree).btnBack.width = 0;
			SearchPerimetreWindow(treeCible.searchTree).searchInput.width =0;			
			SearchPerimetreWindow(treeCible.searchTree).resultControl.width = 0;
			SearchPerimetreWindow(treeCible.searchTree).resultControl.width = 0;	

			selectNode(_commande.idGroupeCible);
		}
		
		/**
		 * Affecte la cible à la commande
		 * @param cmde la commande
		 * 
		 * */
		public function affecterCibleCommande(cmde : Commande):void{
			_commande = cmde;
			if ((SearchPerimetreWindow(treeCible.searchTree).searchTree.selectedIndex != -1)&&(treeCible.mainView.selectedIndex == 1)){								
				if (treeCible.nodeInfo != null ) _commande.idGroupeCible = treeCible.nodeInfo.NID;			
				_commande.flagCible = int(cbNewCible.selected);
			}else if ((PerimetreTreeWindow(treeCible.perimetreTree).perimetreTree.selectedIndex != -1)&&(treeCible.mainView.selectedIndex == 0)){
				if (treeCible.nodeInfo != null ) _commande.idGroupeCible = treeCible.nodeInfo.NID;			
				_commande.flagCible = int(cbNewCible.selected);
			}	
		}		
		
		
		/**
		 * Initialisation de l'IHM
		 * */
		protected function init():void{
			desactiverIHM();
			SearchPerimetreWindow(treeCible.searchTree).searchInput.width = 80;
			PerimetreTreeWindow(treeCible.perimetreTree).searchInput.width = 80;
			SearchPerimetreWindow(treeCible.searchTree).searchInput.styleName = "TextInputInventaire";
			PerimetreTreeWindow(treeCible.perimetreTree).searchInput.styleName = "TextInputInventaire";
			SearchPerimetreWindow(treeCible.searchTree).btnBack.styleName = "MainButton";
			SearchPerimetreWindow(treeCible.searchTree).searchTree.setStyle("borderStyle","solid");
			PerimetreTreeWindow(treeCible.perimetreTree).perimetreTree.setStyle("borderStyle","solid");
			treeCible.addEventListener(InvSearchTree.NODE_CHANGED,displayCbNewNode);
		}
		
		
		//Gere le click sur l'arbre
		//Affiche la CheckBox 'Nouvelle cible' si on click sur un noeud opérateur
		private function displayCbNewNode(e : Event):void{
			if (treeCible.nodeInfo != null){				
				if (treeCible.nodeInfo.TYPE_LOGIQUE =="OPE"){
					dispatchEvent(new Event(CIBLE_CHANGED));
					cbNewCible.visible = true && modeEcriture;
				}else{
					cbNewCible.visible = false;					
					dispatchEvent(new Event(CIBLE_CHANGED));
				}
			}else{
				cbNewCible.visible = false;
				SearchPerimetreWindow(treeCible.searchTree).searchTree.selectedIndex = -1;
				PerimetreTreeWindow(treeCible.perimetreTree).perimetreTree.selectedIndex = -1;
				treeCible.nodeInfo = null;
				afficherWarning();				
				e.preventDefault();
			}			
		}
		
		
		/**
		 * retourne les information sue le noeud sélectionné
		 * @return Object ou null si aucun noeud sélectionné
		 * */
		public function getSelectedNode():Object{		
			if (treeCible.nodeInfo != null )
				return treeCible.nodeInfo;				
			else
				return null;
		}
		      
		
		
		//affiche un warning sur la console 		
		private function afficherWarning():void{
			trace("non selectionnable");
		};
		
		//selectionne un noeud
		//param in nodeId
		private function selectNode(nodeId : int):void
		{
			if (nodeId > 0 && lastSelectedNode < 0){								
				treeCible.getNodeXmlPathCible(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX,nodeId);
				lastSelectedNode = nodeId;
			}		
		}
		
		//Grise le composant
		private function desactiverIHM():void{
			SearchPerimetreWindow(treeCible.searchTree).searchTree.enabled = false;
			cbNewCible.enabled = false;			
		}
		
	}
}