<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSUserService" hint="Interface décrivant les méthodes du Service UserGroup Zenprise">
	<cffunction access="public" name="getRoles" returntype="Array" hint="Retourne la liste des roles">
	</cffunction>
	
	<cffunction access="public" name="getGroups" returntype="Array" hint="Retourne la liste des groupes">
	</cffunction>
	
	<cffunction access="public" name="createGroup" returntype="String" hint="Crée un groupe et retourne son nom">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe à créer">
	</cffunction>

	<cffunction access="public" name="removeGroup" returntype="Void" hint="supprime un groupe">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe à supprimer">
	</cffunction>

	<cffunction access="public" name="getGroupUsers" returntype="Array" hint="Retourne la liste des utilisateur du groupe">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
	</cffunction>
	
	<cffunction access="public" name="getUserGroup" returntype="Array" hint="Retourne la liste des groupes d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
	</cffunction>
	
	<cffunction access="public" name="createUser" returntype="String" hint="Crée un utilisateur et retourne son nom">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfargument name="login" type="String" required="true" hint="Nom de l'utilisateur i.e Login utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe de l'utilisateur">
		<cfargument name="userRole" type="String" required="true" hint="Role de l'utilisateur (ADMIN, SUPPORT, USER, GUEST)">
	</cffunction>

	<cffunction access="public" name="setUserGroup" returntype="Void" hint="Ajoute l'utilisateur au groupe spécifié">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
	</cffunction>
	
	<cffunction access="public" name="removeUser" returntype="Void" hint="Supprime un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
	</cffunction>
	
	<cffunction access="public" name="removeUserFromGroup" returntype="Void" hint="Supprime un utilisateur d'un groupe">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
	</cffunction>

	<cffunction access="public" name="setUserPassword" returntype="Void" hint="Met à jour le mot de passe d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="newPassword" type="String" required="true" hint="Nouveau mot de passe de l'utilisateur">
	</cffunction>

	<cffunction access="public" name="checkUserPassword" returntype="Boolean" hint="Retourne TRUE si le mot de passe spécifié est correct pour l'utilisateur concerné">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe de l'utilisateur">
	</cffunction>

	<cffunction access="public" name="getUserRoles" returntype="Array" hint="Retourne les roles d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
	</cffunction>
	
	<cffunction access="public" name="getUserProperties" returntype="Array" hint="Retourne les propriétés d'un utilisateur (En tant que tableau de String)">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
	</cffunction>
	
	<cffunction access="public" name="putUserProperties" returntype="Void" hint="Spécifie une propriété pour un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="userProps" type="Array" required="true" hint="Tableau dans lequel chaque élément est une structure contenant les clés suivantes : NAME (Nom d'une propriété : String), VALUE (Valeur de la propriété : String)">
	</cffunction>
	
	<cffunction access="public" name="setUserRoles" returntype="Void" hint="Spécifie les roles d'un utilisateur. <b>Dans la version actuelle de Zenprise, un utilisateur ne doit avoir qu'un et un seul role</b>">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="userRoles" type="Array" required="true" hint="Tableau contenant la liste des roles">
	</cffunction>
	
	<cffunction access="public" name="searchUserByName" returntype="Array" hint="Retourne la liste des utilisateurs dont le nom contient la chaine spécifiée en paramètre">
		<cfargument name="searchInUserName" type="String" required="true" hint="Chaine à rechercher dans le nom des utilisateurs">
	</cffunction>
	
	<cffunction access="public" name="initializeUser" returntype="Void" hint="Crée une entrée LDAP pour l'utilisateur spécifié s'il n'est pas encore présent dans l'annuaire">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
	</cffunction>
</cfinterface>