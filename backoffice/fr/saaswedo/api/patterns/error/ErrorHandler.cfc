<cfcomponent author="Cedric" displayname="fr.saaswedo.api.patterns.error.ErrorHandler" extends="fr.saaswedo.api.patterns.cfc.Component"
implements="fr.saaswedo.api.patterns.error.IErrorHandler" hint="Implémentation de base de IErrorHandler">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.patterns.error.ErrorHandler">
		<cfreturn "fr.saaswedo.api.patterns.error.ErrorHandler">
	</cffunction>
	
	<cffunction name="getId" returntype="String" hint="Retourne la meme valeur quelque soit l'instance sur laquelle cette fonction est appelée">
		<cfreturn "DEFAULT_IERROR_HANDLER_ID">
	</cffunction>
	
	<cffunction access="public" name="handleError" returntype="void" hint="Ecrit dans les logs les infos contenues dans cfCatchObject">
		<cfargument name="errorTarget" type="fr.saaswedo.api.patterns.error.IErrorTarget" required="true">
		<cfargument name="cfCatchObject" type="Any" required="true">
		<cfargument name="errorInfos" type="Struct" required="false" hint="Ce paramètre est ignoré par cette implémentation">
		<cftry>
			<cfset var target=ARGUMENTS.errorTarget>
			<cfset var error=ARGUMENTS.cfCatchObject>
			<!--- Log de l'erreur contenant entres autres le nom complet de l'implémentation IErrorTarget --->
			<cfset var targetClass="fr.saaswedo.api.patterns.error.IErrorTarget">
			<cfset var log=GLOBALS().CONST("ErrorHandling.LOG")>
			<cfset var errorCode=structKeyExists(error,"errorCode") ? error.errorCode:"">
			<cfset var detail=TRIM(error.detail) NEQ "" ? " [" & error.detail & "]":"">
			<cfif isInstanceOf(target,"fr.saaswedo.api.patterns.cfc.Component")>
				<cfset targetClass=target.componentName()>
			</cfif>
			<cflog type="error" text="[#log#] #componentName()#.handleError(#targetClass#,#errorCode#) : #error.message##detail#">
			<!--- Toute exception capturée est écrite dans les logs puis à nouveau levée (Elle sera capturée par IErrorTarget) --->
			<cfcatch type="Any">
				<cfset var code=structKeyExists(CFCATCH,"errorCode") ? CFCATCH.errorCode:GLOBALS().CONST("ErrorHandling.ERROR")>
				<cfthrow type="Custom" errorCode="#code#" message="#CFCATCH.message#" detail="#CFCATCH.detail#">
			</cfcatch>
		</cftry>
	</cffunction>

	<!--- =========== Fin des fonction publiques =========== --->
</cfcomponent>