<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.ISecurityManagement"
hint="Interface décrivant les méthodes du fournisseur des services de gestion de la sécurité des équipements mobiles">
	<cffunction access="public" name="getCorporationService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSCorporationService" hint="Retourne le service de gestion des Corporations">
	</cffunction>
	
	<cffunction access="public" name="getAntitheftService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSAntitheftService" hint="Retourne le service : Anti-Theft">
	</cffunction>
	
	<cffunction access="public" name="getMultiLicenseService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSMultiLicenseService" hint="Retourne le service : Multi Licence Services">
	</cffunction>
	
	<cffunction access="public" name="getSingleLicenseService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSSingleLicenseService" hint="Retourne le service : Single Licence Services">
	</cffunction>
	
	<cffunction access="public" name="getOtherService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSOtherService" hint="Retourne le service : Other Services">
	</cffunction>
</cfinterface>