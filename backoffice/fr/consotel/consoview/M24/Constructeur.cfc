<cfcomponent name="Constructeur">
<cffunction name="rechercheEquipementConstructeur" access="remote" returntype="query">
		<cfargument name="SEARCHTEXTE" type="string" required="true">
		<cfargument name="ORDERBYTEXTE" type="string" required="true">
		<cfargument name="OFFSET" type="numeric" required="true">
		<cfargument name="LIMIT" type="numeric" required="true">
		<cfargument name="IDCONSTRUCTEUR" type="numeric" required="true">
		<cfargument name="IDTYPEEQUIPEMENT" type="numeric" required="true">
		<cfargument name="IDTYPECLASSE" type="numeric" required="true">
		<cfargument name="SEGMENT" type="numeric" required="false">
		
		<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
		<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
		<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
			
		<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
		<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
		<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
				
<!--- 
		Ajout du segment		
		PROCEDURE searchEquipConstr_v2(p_search_txt IN VARCHAR2, p_search_col IN VARCHAR2,
		p_order_by IN VARCHAR2, p_order_by_col IN VARCHAR2,
		p_start IN INTEGER, p_limit IN INTEGER,
		p_idconst IN INTEGER, p_type_eq IN INTEGER,
		p_idcategorie IN INTEGER, p_segment IN INTEGER DEFAULT 1,
		p_retour OUT sys_refcursor) ;
 --->
		<cfset _SEGMENT = 0>		
		<cfif isDefined("SEGMENT")>
			<cfset _SEGMENT = SEGMENT>
		</cfif>
		
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.searchEquipConstr_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCONSTRUCTEUR#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPEEQUIPEMENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECLASSE#" null="false">	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_SEGMENT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
	<cfreturn p_result>
</cffunction>
<cffunction name="searchEquipConstrImport" access="remote" returntype="query">
	<cfargument name="SEARCHTEXTE" 		type="string" required="true">
	<cfargument name="ORDERBYTEXTE" 	type="string" required="true">
	<cfargument name="OFFSET" 			type="numeric" required="true">
	<cfargument name="LIMIT" 			type="numeric" required="true">
	<cfargument name="IDCONSTRUCTEUR" 	type="numeric" required="true">
	<cfargument name="IDTYPEEQUIPEMENT" type="numeric" required="true">
	<cfargument name="IDTYPECLASSE" 	type="numeric" required="true">
	<cfargument name="IDREVENDEUR" 		type="numeric" required="true">
	<cfargument name="SEGMENT" 			type="numeric" required="false">
	
	<cfset TAB_SEARCH = ListToArray(SEARCHTEXTE, ',', true)>
	<cfset SEARCH_TEXT_COLONNE = TAB_SEARCH[1]>
	<cfset SEARCH_TEXT = TAB_SEARCH[2]> 
	
	<cfset TAB_ORDERBY = ListToArray(ORDERBYTEXTE, ',', true)>
	<cfset ORDER_BY_TEXTE_COLONNE = TAB_ORDERBY[1]>
	<cfset ORDER_BY_TEXTE = TAB_ORDERBY[2]> 
	
	
	<cfset _SEGMENT = 1>		
	<cfif isDefined("SEGMENT")>
		<cfset _SEGMENT = SEGMENT>
	</cfif>
	
	<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_24.searchEquipConstrImport_v3" cachedWithin="#createTimeSpan(0,0,4,0)#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SEARCH_TEXT_COLONNE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#OFFSET#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#LIMIT#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCONSTRUCTEUR#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPEEQUIPEMENT#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDTYPECLASSE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDREVENDEUR#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_SEGMENT#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc> 
	<cfreturn p_result>
</cffunction>


<cffunction name="addEquipementConstructeur" access="remote" returntype="struct" description="add equip in catalogue constructeur">
	
	<cfargument name="libelle" type="string" required="true">
	<cfargument name="reference" type="string" required="true">
	<cfargument name="idConstructeur" type="numeric" required="true">
	<cfargument name="idType" type="numeric" required="true">
	
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.addEquipementConstructeur">
		
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
		
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		<cfprocparam type="Out" cfsqltype="CF_SQL_vARCHAR" variable="ref">
	</cfstoredproc> 
	<cfset myStruct=structNew()>
	<cfset myStruct.IDEQUIP = p_retour>
 	<cfset myStruct.REF = ref>

	<cfreturn myStruct>
</cffunction>
<cffunction name="listeConstructeurRacine" access="remote" returntype="query">

 	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.getConstructeurs">
		<cfprocresult name="p_result">
	</cfstoredproc> 
	<cfreturn p_result>
</cffunction>

<cffunction name="getAllRevendeur" access="remote" returntype="any" description="Retourne les revendeurs du client">
	<cfargument name="IDGroupe_client" type="numeric" required="true">
	<cfargument name="search" type="string" required="true">
	<cfargument name="segment1" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.Get_all_revendeur">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDGroupe_client#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vaRCHAR" value="#search#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#segment1#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
<cffunction name="updateEquipementConstructeur" access="remote" returntype="any" description="update equip in catalogue constructeur">
	<cfargument name="id" type="string" required="true">
	<cfargument name="libelle" type="string" required="true">
	<cfargument name="reference" type="string" required="true">
	<cfargument name="idConstructeur" type="numeric" required="true">
	<cfargument name="idType" type="numeric" required="true">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_equipement.updateEquConstructeur">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#libelle#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#reference#" null="false">
		
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idType#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idConstructeur#" null="false">
		
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
</cfcomponent>
