package gestionparc.ihm.fiches.rago.entity
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class Regle
	{
		private var _IDREGELE_ORGA:int=0;
		private var _IDSOURCE:int=0;
		private var _IDCIBLE:int=0;
		private var _OPERATEURID:int=0;
		private var _CREATE_APPLOGINID:Number=0;
		private var _MODIF_APPLOGINID:Number=0;


		private var _TYPE_REGLE:String="";
		private var _REGLE_NOM:String="";
		private var _REGLE_COMMENT:String="";
		private var _LIBELLE_SOURCE:String="";
		private var _LIBELLE_CIBLE:String="";
		private var _OPERATEUR:String="";
		private var _NOM_USER_CREATE:String="";
		private var _NOM_USER_MODIF:String="";
		private var _CHAINE_BOOL_ACTIVE:String="";
		private var _ORGA_CIBLE:String="";
		private var _ORGA_SOURCE:String="";
		private var _CHEMIN_CIBLE:String="";
		private var _CHEMIN_SOURCE:String="";


		private var _BOOL_ACTIVE:Boolean=false;
		private var _BOOL_SUIVRE_AUTO:Boolean=false;

		private var _LAST_EXECUTED:String="";
		private var _CREATE_DATE:String="";
		private var _MODIF_DATE:String="";


		private var _LIST_EXCEPTION:ArrayCollection;

		public function Regle()
		{
		}

		public function set TYPE_REGLE(value:String):void
		{
			_TYPE_REGLE=value;
		}

		public function get TYPE_REGLE():String
		{
			return _TYPE_REGLE;
		}

		public function set REGLE_NOM(value:String):void
		{
			_REGLE_NOM=value;
		}

		public function get REGLE_NOM():String
		{
			return _REGLE_NOM;
		}

		public function set REGLE_COMMENT(value:String):void
		{
			_REGLE_COMMENT=value;
		}

		public function get REGLE_COMMENT():String
		{
			return _REGLE_COMMENT;
		}

		public function set IDSOURCE(value:Number):void
		{
			_IDSOURCE=value;
		}

		public function get IDSOURCE():Number
		{
			return _IDSOURCE;
		}

		public function set LIBELLE_SOURCE(value:String):void
		{
			_LIBELLE_SOURCE=value;
		}

		public function get LIBELLE_SOURCE():String
		{
			return _LIBELLE_SOURCE;
		}

		public function set IDCIBLE(value:Number):void
		{
			_IDCIBLE=value;
		}

		public function get IDCIBLE():Number
		{
			return _IDCIBLE;
		}

		public function set LIBELLE_CIBLE(value:String):void
		{
			_LIBELLE_CIBLE=value;
		}

		public function get LIBELLE_CIBLE():String
		{
			return _LIBELLE_CIBLE;
		}

		public function set BOOL_ACTIVE(value:Boolean):void
		{
			_BOOL_ACTIVE=value;
		}

		public function get BOOL_ACTIVE():Boolean
		{
			return _BOOL_ACTIVE;
		}

		//		public function set LAST_EXECUTED(value:Date):void
		//		{
		//			_LAST_EXECUTED = value;
		//		}
		//
		//		public function get LAST_EXECUTED():Date
		//		{
		//			return _LAST_EXECUTED;
		//		}

		public function set LAST_EXECUTED(value:String):void
		{
			_LAST_EXECUTED=value;
		}

		public function get LAST_EXECUTED():String
		{
			return _LAST_EXECUTED;
		}

		public function set BOOL_SUIVRE_AUTO(value:Boolean):void
		{
			_BOOL_SUIVRE_AUTO=value;
		}

		public function get BOOL_SUIVRE_AUTO():Boolean
		{
			return _BOOL_SUIVRE_AUTO;
		}

		public function set OPERATEUR(value:String):void
		{
			_OPERATEUR=value;
		}

		public function get OPERATEUR():String
		{
			return _OPERATEUR;
		}

		public function set OPERATEURID(value:Number):void
		{
			_OPERATEURID=value;
		}

		public function get OPERATEURID():Number
		{
			return _OPERATEURID;
		}

		public function set IDREGLE_ORGA(value:Number):void
		{
			_IDREGELE_ORGA=value;
		}

		public function get IDREGLE_ORGA():Number
		{
			return _IDREGELE_ORGA;
		}

		//		public function set CREATE_DATE(value:Date):void
		//		{
		//			_CREATE_DATE = value;
		//		}
		//
		//		public function get CREATE_DATE():Date
		//		{
		//			return _CREATE_DATE;
		//		}

		public function set CREATE_DATE(value:String):void
		{
			_CREATE_DATE=value;
		}

		public function get CREATE_DATE():String
		{
			return _CREATE_DATE;
		}

		public function set CREATE_APPLOGINID(value:Number):void
		{
			_CREATE_APPLOGINID=value;
		}

		public function get CREATE_APPLOGINID():Number
		{
			return _CREATE_APPLOGINID;
		}

		public function set MODIF_APPLOGINID(value:Number):void
		{
			_MODIF_APPLOGINID=value;
		}

		public function get MODIF_APPLOGINID():Number
		{
			return _MODIF_APPLOGINID;
		}

		//		public function set MODIF_DATE(value:Date):void
		//		{
		//			_MODIF_DATE = value;
		//		}
		//
		//		public function get MODIF_DATE():Date
		//		{
		//			return _MODIF_DATE;
		//		}

		public function set MODIF_DATE(value:String):void
		{
			_MODIF_DATE=value;
		}

		public function get MODIF_DATE():String
		{
			return _MODIF_DATE;
		}

		public function set NOM_USER_CREATE(value:String):void
		{
			_NOM_USER_CREATE=value;
		}

		public function get NOM_USER_CREATE():String
		{
			return _NOM_USER_CREATE;
		}

		public function set NOM_USER_MODIF(value:String):void
		{
			_NOM_USER_MODIF=value;
		}

		public function get NOM_USER_MODIF():String
		{
			return _NOM_USER_MODIF;
		}

		public function get LIST_EXCEPTION():ArrayCollection
		{
			return _LIST_EXCEPTION;
		}

		public function set LIST_EXCEPTION(value:ArrayCollection):void
		{
			_LIST_EXCEPTION=value;
		}

		public function get CHAINE_BOOL_ACTIVE():String
		{
			return _CHAINE_BOOL_ACTIVE;
		}

		public function set CHAINE_BOOL_ACTIVE(value:String):void
		{
			_CHAINE_BOOL_ACTIVE=value;
		}

		public function get ORGA_CIBLE():String
		{
			return _ORGA_CIBLE;
		}

		public function set ORGA_CIBLE(value:String):void
		{
			_ORGA_CIBLE=value;
		}

		public function get ORGA_SOURCE():String
		{
			return _ORGA_SOURCE;
		}

		public function set ORGA_SOURCE(value:String):void
		{
			_ORGA_SOURCE=value;
		}

		public function get CHEMIN_CIBLE():String
		{
			return _CHEMIN_CIBLE;
		}

		public function set CHEMIN_CIBLE(value:String):void
		{
			_CHEMIN_CIBLE=value;
		}

		public function get CHEMIN_SOURCE():String
		{
			return _CHEMIN_SOURCE;
		}

		public function set CHEMIN_SOURCE(value:String):void
		{
			_CHEMIN_SOURCE=value;
		}
	}

}
