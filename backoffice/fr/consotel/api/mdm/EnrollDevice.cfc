<cfcomponent displayname="EnrollDevice" implements="fr.consotel.api.mdm.IFunction">
	<cffunction access="public" name="execute" returntype="void" hint="Autorise le device (serialNumber,imei) pour une action d'enrollment<br>
	Envoi un SMS contenant un lien à cliquer par l'utilisateur pour procéder à l'enrollment. Le lien cliqué envoie entres autres en paramètres HTTP/GET :
	serialNumber, imei, uuid (Identifiant utilisé pour la récupération de la racine)">			
		<cfargument name="serialNumber" type="String"  required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" 		type="String"  required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="soustete" 	type="String"  required="true" hint="Numéro de téléphone du device">
		<cfargument name="idSousTete" 	type="Numeric" required="true" hint="idsoutete du numéro du téléphone du device">
		<cfargument name="idPool" 		type="Numeric" required="true" hint="idPool du device">
		<cfargument name="idlogcamp"	type="Numeric" required="true" hint="id log campagne">
		<cfargument name="idcampagne"	type="Numeric" required="false" hint="id de la campagne ou zéro">
			<cfset var _idcamapgne  = 0>
			<cfset var typeCamapgne = 'LIGNE'>
			<cfif structKeyExists(arguments, "idcampagne")>
				<cfset var _idcamapgne  = idcampagne>
				<cfset var typeCamapgne = 'CAMPAGNE'>
			</cfif>
			<cfset var isSecure=TRUE>
			<cfset serverDNS="backoffice4.consotel.fr">
			<cfset contextPath="">
			<cfif isDebugMode()>
				<cfset serverDNS=CGI.SERVER_NAME>
				<cfset contextPath="/backoffice">
				<cfset var isSecure=FALSE>
			</cfif>
			<cfset var enrollPage="/redirection.cfm">
			<cfif fileExists(expandPath(contextPath & "/redirection.cfm"))>
				<cfset enrollPage=contextPath & "/redirection.cfm">
			</cfif>
			<cfset var contentSms	= "http" & (isSecure ? "s":"") & "://" & serverDNS & enrollPage & "?&uuid=">
			<cfset var urlcle 		= RandRange(0, 99)>
			<!--- Par défaut : On utilise toujours SFR comme opérateur pour envoyer les SMS d'enrollment --->	
			<cfset var operator		= "SFR">
			<cfset var delay		= "1000">
			<cfset var statusSMS 	= -10>
			<cfset var urlwebserv	= createObject("component", "fr.consotel.api.sms.public.ApiSmsPublic")>
			<!--- Utilisation de l'api sms contenant le lien de detection de l'OS, avec la soustete passé en paramètre URL pour pouvoir logger --->
			<cfset rsltFindNoCase = FindNoCase("0", sousTete)>
			<cfset sousTeteWithIndicatif = sousTete>
			<cfif rsltFindNoCase EQ 1>
				<cfset sousTeteWithIndicatif = ligneToSmsFormat(sousTeteWithIndicatif)>
			</cfif>
			<!--- API SMS --->
			<cfset var uuid = urlwebserv.createCampagne()>
			<cfif isDefined("uuid") AND uuid NEQ "">
				<!--- Pour les tests : [FACTICE] Enregistrement des infos nécessaires à la page d'enrollment
				<cfset var myt58=createObject("component","fr.saaswedo.api.tests.MYT58")>
				<cfset var isStored=myt58.storeEnrollParameters("ROCOFFRE",uuid,ARGUMENTS["idPool"],ARGUMENTS["serialNumber"],ARGUMENTS["imei"],ARGUMENTS["soustete"])>
				<cfif NOT isStored>
					<cfthrow type="Custom" message="Echec de l'enregistrement des infos d'enrollment pour le device : [#ARGUMENTS.serialNumber#,#ARGUMENTS.imei#]">
				</cfif>
				--->
				<!--- Implémentation existente (Envoi du SMS d'enrollment) --->
				<cfset contentSms	= contentSms & #uuid#  & "&cle=" & #urlcle#>
				<cfset statusSMS = 10>
				<cfset var prepareSMSResult = createObject("component", "fr.consotel.consoview.M111.MDMService").prepareSMS(typeCamapgne, sousTete, uuid, urlcle, _idcamapgne, idSousTete, statusSMS, idlogcamp, 'ROCOFFRE')>
				<!--- Ancien Envoi de SMS H.S
				<cfset var returnSend = urlwebserv.sendMessageToSingleNumber(sousTeteWithIndicatif, contentSms, operator, uuid)>
				--->
				<!--- Envoi de SMS avec un tableau contenant les lignes mobiles destinataires --->
				<cfset var returnSend = urlwebserv.sendMessageToMultipleNumber([sousTeteWithIndicatif], contentSms, operator, uuid)>
				<cflog type="information"
					text="Send Enroll SMS to #sousTeteWithIndicatif#. Operator : #operator#. Length : #LEN(contentSms)#. Content : #contentSms#. UUID : #uuid#. Send ? #returnSend#">
				<cfif returnSend EQ true>
					<cfset statusSMS = 20>
				<cfelse>
					<cfset statusSMS = -20>
				</cfif>
				<cfset var envoiSMSResult = createObject("component", "fr.consotel.consoview.M111.MDMService").envoiSMS(typeCamapgne, uuid, urlcle, statusSMS, idlogcamp, 'ROCOFFRE', _idcamapgne)>
			<cfelse>
				<cfset statusSMS = -10>
				<cfset var prepareSMSResult = createObject("component", "fr.consotel.consoview.M111.MDMService").prepareSMS(typeCamapgne, sousTete, uuid, urlcle, _idcamapgne, idSousTete, statusSMS, idlogcamp, 'ROCOFFRE')>
			</cfif>
			<cfif statusSMS LT 0>
				<cfthrow type="Custom" message="Votre SMS n'a pas été envoyé. #statusSMS#">
			</cfif>
	</cffunction>

	<cffunction access="public" name="displayIOSEnrollPage" returntype="void" output="true" hint="Affiche la page d'enrollment pour IOS de Zenprise">
		<cfargument name="mdm" type="String" required="true" hint="Adresse ou IP du serveur Zenprise">
		<cfargument name="useSSL" type="Boolean" required="true" hint="TRUE si le protocole à utiliser pour accéder au MDM est HTTPS et FALSE sinon">
		<cfargument name="username" type="String" required="true" hint="Login d'enrollment Zenprise">
		<cfargument name="password" type="String" required="true" hint="Mot de passe d'enrollment Zenprise">
		<cfargument name="idGroupe" type="Numeric" required="true" hint="Identifiant du groupe">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number du device">
		<cfargument name="imei" type="String" required="true" hint="IMEI du device">
		<cfargument name="myTEM" type="fr.saaswedo.api.myTEM.mdm.MyTEM" required="true" hint="API MDM">
		<cfset var mdmUseSSL=ARGUMENTS["useSSL"]>
		<cfset var formURL=urldecode('http' & (mdmUseSSL ? 's':'') & '://' & ARGUMENTS.mdm & '/zdm/ios/otae/getrootca')>
		<cfset var hrefURL=urldecode('http' & (mdmUseSSL ? 's':'') & '://' & ARGUMENTS.mdm & '/zdm/ios/otae/dologin?ew_username=#ARGUMENTS.username#&ew_password=#ARGUMENTS.password#')>
		<cfset var iDeviceInfo=myTEM.getIDeviceInfo()>
		<cfoutput>
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
				<head profile="http://gmpg.org/xfn/11">
					<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
					<meta name="description" content="Dectection OS"/>
					<meta http-equiv="pragma" content="no-cache"/>

					<cfif iDeviceInfo.getManagedStatus(serialNumber,imei) NEQ "MANAGED">
						<form action="#formURL#" method="post" target="_self" id="rootCAFormID"></form>
						<title>Dectection OS</title>
					<cfelse>
						<title>Enrollment terminé</title>
					</cfif>
				</head>

				<cfif iDeviceInfo.getManagedStatus(serialNumber,imei) NEQ "MANAGED">
					<body onload="document.getElementById('rootCAFormID').submit(); return true;">
				<cfelse>
					<body>
				</cfif>
					<CENTER>
						<cfif iDeviceInfo.getManagedStatus(serialNumber,imei) NEQ "MANAGED">
							<a href="#hrefURL#"><h3>Cliquez ici pour finaliser l'enrollment de votre terminal (ou rafraichir cette page si cela a déjà été effectué)</h3></a>
						<cfelse>
							Enrollment effectué
						</cfif>
					</CENTER>					
				</body>
			</html>
		</cfoutput>
	</cffunction>
	
	<!--- La version de cette méthode ne support que les numéros de ligne en France --->
	<cffunction access="public" name="ligneToSmsFormat" returntype="String"
	hint="Retourne la ligne au format requis par l'API SMS : + suivi de INDICATIF_PAYS suivi de NUMERO_DE_LIGNE_SANS_LE_PREFIXE<br>
	Si la ligne commence par l'indicatif FRANCE alors elle est retournée telle quelle<br>
	Si la ligne commence par 0 alors ce chiffre est remplacé par l'indicatif FRANCE">
		<cfargument name="ligne" type="String" required="true" hint="Numéro de ligne">
		<cfset var numero=TRIM(ARGUMENTS["ligne"])>
		<cfif LEN(numero) GT 3>
			<cfset var indicatif="+33">
			<cfset var tmpIndicatif=MID(numero,1,3)>
			<!--- TRUE si la ligne commence par l'indicatif FRANCE --->
			<cfif tmpIndicatif EQ indicatif>
				<cfreturn numero>
			<cfelseif MID(numero,1,1) EQ "0">
				<cfreturn indicatif & MID(numero,2,LEN(numero) - 1)>
			<cfelse>
				<cfthrow type="Custom" message="[M111] Le format de la ligne #numero# n'est pas supportée ou est invalide">
			</cfif>
		</cfif>
	</cffunction>
</cfcomponent>
