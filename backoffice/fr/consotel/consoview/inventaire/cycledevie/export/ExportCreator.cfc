<cfcomponent output="false">
	<cffunction name="createExport" access="public" returntype="any" output="false">
		
		<cfargument name="format" required="true" type="string">
		<cfset demande=createObject("component","fr.consotel.consoview.inventaire.cycledevie.export." & #format# & ".Resiliation")>
		<cfreturn demande>
		
	</cffunction>
</cfcomponent>