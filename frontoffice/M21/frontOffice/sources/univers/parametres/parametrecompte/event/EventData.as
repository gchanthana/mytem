package univers.parametres.parametrecompte.event
{
	import flash.events.Event;

	public class EventData extends Event
	{
		private var _objData:Object;
		
		public function EventData(type:String, bubbles:Boolean=false, cancelable:Boolean=false, objData:Object =null)
		{
			super(type, bubbles, cancelable);
			this._objData = objData;
		}
		
		public function get objData():Object 
		{
			return _objData;
		}
		
		override public function clone():Event
		{
			return new EventData(type,bubbles,cancelable, objData);
		}
	}
}