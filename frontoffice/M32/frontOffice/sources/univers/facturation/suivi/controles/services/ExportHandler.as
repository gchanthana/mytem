package univers.facturation.suivi.controles.services
{
	import composants.util.ConsoviewAlert;
	
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	public class ExportHandler
	{		
		public function ExportHandler()
		{
		}
		
		public function getExportInventaireHandler(event:ResultEvent):void
		{
			if(event.result!=-1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M32', 'Votre_demande_de_rapport____t__prise_en_'));
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M32', 'Votre_demande_de_rapport_n___pu__tre_tra'));
			}
		}
	}
}