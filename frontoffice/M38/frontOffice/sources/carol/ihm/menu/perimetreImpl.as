package carol.ihm.menu
{
	import carol.VO.AnalyseVO;
	import carol.composants.perimetreAnalyseIHM;
	import carol.composants.periode.AMonth;
	import carol.composants.periode.PeriodeEvent;
	import carol.composants.periode.PeriodeSelector;
	import carol.composants.progressBarIHM;
	import carol.event.CarolEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.Menu;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class perimetreImpl extends VBox
	{

//--------------------------------------------------------------------------------------------//
//					VARIABLES
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - FLEX COMPONENTS
		//--------------------------------------------------------------------------------------------//
		//VSLIDER
		public var ps					:PeriodeSelector
		//COMBOBOX
		public var cbxTypeOrga			:ComboBox
		public var cbxPerimetre1		:ComboBox;
		public var cbxPerimetre2		:ComboBox;
		public var cbxLevel				:ComboBox;
		//RADIOBUTTONGROUP
		public var rbgCarlo				:RadioButtonGroup;
		public var rbCarlo				:RadioButton;
		// IMAGE
		public var imgFiltreTypeOrga	:Image;
		// LABEL
		public var lblDate				:Label
		//DATEFIELD
		public var dateFieldBegin		:DateField;
		public var dateFieldEnd			:DateField;
		//COMPOSANTS ANALYSE
		public var cpPerimetreAnalyse	:perimetreAnalyseIHM;
		public var _popUpProgressBar	:progressBarIHM
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES - LOCAL
		//--------------------------------------------------------------------------------------------//
		public var valuesObj			:ArrayCollection = null
		public var dateFomat			:String			 = ""
		public var analyseVoSelected	:AnalyseVO
		public var filterMenu			:Menu
		private var strTousOrga			:String = ""
		private var strOrgaClient		:String = ""
		private var strOrgaOpe			:String = ""
//--------------------------------------------------------------------------------------------//
//					CONSTRUCTEUR
//--------------------------------------------------------------------------------------------//

		public function perimetreImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, 		creationCompleteHandler);
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PROTECTED
//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED - EVENT COMPOSANTS - FLEX
		//--------------------------------------------------------------------------------------------//
		
		protected function creationCompleteHandler(fl:FlexEvent):void
		{
			cbxTypeOrga.selectedIndex = -1
			cbxTypeOrga.dataProvider = new ArrayCollection()
			cbxPerimetre1.selectedIndex = -1
			cbxPerimetre1.dataProvider = new ArrayCollection()
			cbxPerimetre2.selectedIndex = -1
			cbxPerimetre2.dataProvider = new ArrayCollection()
			cbxLevel.selectedIndex = -1
			cbxLevel.dataProvider = new ArrayCollection()			
			
			strTousOrga	= resourceManager.getString("MainCarol","tous_orgas__1234");
			strOrgaClient = resourceManager.getString("MainCarol","orga_client__1234");
			strOrgaOpe = resourceManager.getString("MainCarol","orga_ope__1234");
			var menuDataprovider:XML =
				<root>
			        <menuitem label={strTousOrga} 	value='ALL'  	type='radio' toggled='true' 	groupName='orgaType'/>
		            <menuitem label={strOrgaClient} 	value='CUS'  	type='radio' toggled='false' 	groupName='orgaType'/>
		            <menuitem label={strOrgaOpe} 		value='OPE'  	type='radio' toggled='false' 	groupName='orgaType'/>
		        </root> 
			filterMenu = Menu.createMenu(this, menuDataprovider, false);
			filterMenu.labelField="@label";
			cbxPerimetre1.addEventListener(CollectionEvent.COLLECTION_CHANGE,perimetreDataProviderChanged);
			analyseVoSelected.PERIMETRE.cs.addEventListener(CarolEvent.CHARGE_PERIODESELECTOR,initSlider)
			
			analyseVoSelected.PERIMETRE.cs.getListeIdMois()
			analyseVoSelected.PERIMETRE.cs.getListeOrga();
		}
		private function initSlider(e:CarolEvent):void
		{
			ps.getPeriode(analyseVoSelected.PERIMETRE.cs.arrListeIdmois);
		}
		
		protected function cbxTypeOrgaChangeHandler():void
		{
			if(cbxTypeOrga.selectedIndex > -1)
			{
				cbxPerimetre1.enabled = true
				cbxLevel.enabled = true
				cbxPerimetre2.enabled
				analyseVoSelected.PERIMETRE.cs.getListeNiveauOrga(cbxTypeOrga.selectedItem.IDORGA,cbxTypeOrga.selectedItem.IDCLICHE);
			}
		}
		
		protected function perimetreDataProviderChanged(event:CollectionEvent):void {
			if(cbxPerimetre1.dropdown)
				cbxPerimetre1.dropdown.dataProvider=cbxPerimetre1.dataProvider;
			if(cbxLevel.dropdown)
				cbxLevel.dropdown.dataProvider=cbxPerimetre1.dataProvider;
		}
		
		protected function cbxNiveauOrgaChangeHandler():void
		{
			if(cbxPerimetre1.selectedIndex > -1)
			{
				cbxPerimetre2.enabled = true;
				analyseVoSelected.PERIMETRE.cs.addEventListener(CarolEvent.LISTE_NOEUD_RESULTEVENT,listeNoeudResultHandler);
				analyseVoSelected.PERIMETRE.cs.getListeNoeudOrga(cbxTypeOrga.selectedItem.IDORGA,cbxTypeOrga.selectedItem.IDCLICHE,cbxPerimetre1.selectedItem.NIVEAU_CHOISI,cbxPerimetre1.selectedItem.IDNIVEAU_CHOISI);
			}
		}
		private function listeNoeudResultHandler(e:CarolEvent):void
		{
			if(cbxPerimetre2.dropdown)
				cbxPerimetre2.dropdown.dataProvider=cbxPerimetre2.dataProvider;
			if(cbxPerimetre2.dataProvider.length == 1)
				cbxPerimetre2.selectedIndex = 0
			else
				cbxPerimetre2.selectedIndex = -1
		}
		protected function cbxChangeHandler(e:Event):void
		{
		}
		protected function btnLunchClickHandler():void
		{
			var OK			:Boolean = true;
			var boolOrga	:Boolean = true
			var Boolp1		:Boolean = true;
			var Boolp2		:Boolean = true;
			var Boollevel	:Boolean = true;
			var BoolDb		:Boolean = true;
			var BoolDe		:Boolean = true;
			
		// ORGANISATION
			if(cbxTypeOrga.selectedIndex > -1)
			{
				analyseVoSelected.PERIMETRE.idcliche 		= cbxTypeOrga.selectedItem.IDCLICHE
				analyseVoSelected.PERIMETRE.idorga			= cbxTypeOrga.selectedItem.IDORGA
				analyseVoSelected.PERIMETRE.libelle_orga	= cbxTypeOrga.selectedItem.ORGA
			}
			else
			{
			 	boolOrga = false
			 	OK = false
			}
		//PERIMETRE	
			if(cbxPerimetre1.selectedIndex > -1)
			{
				analyseVoSelected.PERIMETRE.niveau			= cbxPerimetre1.selectedItem.NIVEAU_CHOISI
				analyseVoSelected.PERIMETRE.libelle_niveau = cbxPerimetre1.selectedItem.LIBELLE_COLONNE
				analyseVoSelected.PERIMETRE.idx_niveau		= cbxPerimetre1.selectedItem.IDNIVEAU_CHOISI
			}
			else
			{
				Boolp1 = false;
				OK = false;
			}
		//PERIMETRE VALUE	
			// Si on choisit le groupe, aucune valeur n'est à sélectionner
			if(cbxPerimetre1.selectedIndex > -1 && cbxPerimetre2.selectedIndex > -1)
			{
				analyseVoSelected.PERIMETRE.idperimetre 	= cbxPerimetre2.selectedItem.IDNOEUD
				analyseVoSelected.PERIMETRE.libelle_orga 	= cbxPerimetre2.selectedItem.LIBELLE_NOEUD
			}
			else
			{
				Boolp2 = false;
				OK = false;
			}
		//NIVEAU
			if(cbxLevel.selectedIndex > -1)
			{
				analyseVoSelected.PERIMETRE.niveau_aggreg 			= cbxLevel.selectedItem.NIVEAU_CHOISI
				analyseVoSelected.PERIMETRE.libelle_niveau_aggreg 	= cbxLevel.selectedItem.LIBELLE_COLONNE
				analyseVoSelected.PERIMETRE.idx_niveau_aggreg		= cbxLevel.selectedItem.IDNIVEAU_CHOISI
			}
			else
			{
				Boollevel = false;
				OK = false;
			}
		// DATE
//			peri.beginPeriode = (ps.getTabPeriode() as Array)[ps.slider.values[0]].dateDebut as Date;
//			peri.endPeriode   = (ps.getTabPeriode() as Array)[ps.slider.values[1]-1].dateFin as Date;
			
			var tabPeriode:Array 	= (ps.getTabPeriode() as Array) 
			analyseVoSelected.PERIMETRE.idperiodedeb 		= (tabPeriode[ps.slider.values[0]] as AMonth).idmois;
			analyseVoSelected.PERIMETRE.idperiodefin		= (tabPeriode[ps.slider.values[1]-1] as AMonth).idmois;
			analyseVoSelected.PERIMETRE.libelle_periodedeb	= (tabPeriode[ps.slider.values[0]] as AMonth).dateDebut
			analyseVoSelected.PERIMETRE.libelle_periodefin = (tabPeriode[ps.slider.values[1]-1] as AMonth).dateFin
			
		// VERIFIER LES DATES
			if(OK)
			{
				analyseVoSelected.isDefaultAnalyse = true;
				if(rbgCarlo.selectedValue == rbCarlo.value)
					analyseVoSelected.chargerPerimetreAnalyse();
				else
					analyseVoSelected.chargerAnalyseDansExcel();
			}
			else
			{
				if(!boolOrga)
				{
					Alert.show("Veuillez sélectionner une organisation",ResourceManager.getInstance().getString('MainCarol', 'erreur'))
				}
				else if(!Boolp1)
				{
					Alert.show(ResourceManager.getInstance().getString('MainCarol', 'Veuillez_s_lectionner_un_p_rim_tre'),ResourceManager.getInstance().getString('MainCarol', 'erreur'))
				}
				else if(!Boolp2)
				{
					Alert.show(ResourceManager.getInstance().getString('MainCarol', 'Veuillez_s_lectionner_une_valeur_pour_le'),ResourceManager.getInstance().getString('MainCarol', 'erreur'))
				}
				else if(!Boollevel)
				{
					Alert.show(ResourceManager.getInstance().getString('MainCarol', 'Veuillez_s_lectionner_un_niveau_d_analys'),ResourceManager.getInstance().getString('MainCarol', 'erreur'))	
				}
				else if(!BoolDb)
				{
					Alert.show(ResourceManager.getInstance().getString('MainCarol', 'Veuillez_s_lectionner_une_date_de_d_but_'),ResourceManager.getInstance().getString('MainCarol', 'erreur'))	
				}
				else if(!BoolDe)
				{
					Alert.show(ResourceManager.getInstance().getString('MainCarol', 'Veuillez_s_lectionner_une_date_de_fin_d_'),ResourceManager.getInstance().getString('MainCarol', 'erreur'))
				}
			} 
		}
	// FILTRE DU TYPE D'ORGA
		protected function onfilterCmbClicked(event:MouseEvent):void
		{
			var point1:Point = new Point();
			 point1.x=event.localX;
             point1.y=event.localY;                
             point1=imgFiltreTypeOrga.localToGlobal(point1);
             filterMenu.addEventListener(MenuEvent.ITEM_CLICK,onfilterCmbChange)
             filterMenu.show(point1.x, point1.y);
			
		}
	// QUAND ON CHOISIT UN TYPE D'ORGA
		protected function onfilterCmbChange(event:MenuEvent):void
		{
			(cbxTypeOrga.dataProvider as ArrayCollection).filterFunction = filterOrga
			cbxTypeOrga.dataProvider.refresh()
		}
	
	// QUAND ON CLIQUE SUR LE COMPOSANT PERIODESELECTOR
		protected function psPeriodeChangeHandler(e:Event=null):void
		{
			var dateDeb:Date 		= (e as PeriodeEvent).dateDeb
			var dateFin:Date		= (e as PeriodeEvent).dateFin
			var formatDate:String = resourceManager.getString("MainCarol","MM_YYYY");
			lblDate.text = DateField.dateToString(dateDeb,formatDate)
			if(dateDeb.fullYear != dateFin.fullYear || dateDeb.month != dateFin.month)
				lblDate.text += " "+resourceManager.getString("MainCarol","_au_")+" "+DateField.dateToString(dateFin,formatDate)
		}

//--------------------------------------------------------------------------------------------//
//					METHODES PRIVATE
//--------------------------------------------------------------------------------------------//
	// FONCTION DE FILTRE DES ORGAS
		private function filterOrga(item:Object):Boolean
		{
			if(filterMenu.selectedItem.@value == "ALL")
				return true
			else
			{
				if(item.TYPE_ORGA == filterMenu.selectedItem.@value)
					return true
			}
			return false
		}	
	}
}
