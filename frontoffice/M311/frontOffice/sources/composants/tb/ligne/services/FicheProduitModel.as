package composants.tb.ligne.services
{
	import composants.tb.ligne.entity.FicheProduitVO;
	import composants.tb.ligne.event.FicheEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	public class FicheProduitModel extends EventDispatcher
	{
		private var _ficheProduit:ArrayCollection;
		
		public function FicheProduitModel()
		{
			_ficheProduit = new ArrayCollection();
		}

		public function get ficheProduit():ArrayCollection
		{
			return _ficheProduit;
		}

		public function set ficheProduit(value:ArrayCollection):void
		{
			_ficheProduit = value;
		}
		
		internal function updateFicheProduit(value:ArrayCollection):void
		{		
			
			var temp:FicheProduitVO;
			for (var i:int=0; i < value.length; i++)
			{
				temp = new FicheProduitVO();
				temp.fill(value[i]);
				_ficheProduit.addItem(temp);
			}
			
			dispatchEvent(new FicheEvent(FicheEvent.FICHEPRODUIT));
		}

	}
}