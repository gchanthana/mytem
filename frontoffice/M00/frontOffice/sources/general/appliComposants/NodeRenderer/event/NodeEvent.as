package general.appliComposants.NodeRenderer.event
{
	import flash.events.Event;
	
	import general.appliComposants.CvPerimetreNodeVO;

	public class NodeEvent extends Event
	{
		public static var NODE_CHANGED_EVENT:String = "NODE_CHANGED_EVENT"
		
		public var node:CvPerimetreNodeVO
		
		public function NodeEvent(type:String, node:CvPerimetreNodeVO = null, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.node = node
		}
		
	}
}