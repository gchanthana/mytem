<cfcomponent name="CatalogueService">
<cffunction name="prevenirClient" output="false" description="" access="remote" returntype="void">
	<cfargument name="arrayEquip" required="true" type="array">
	<cfargument name="arrayClient" required="true" type="array">


		<cfloop index="a" from="1" to="#arrayLen(arrayClient)#">
			<cfset listeContactOfRacine = getContactOfRacine(arrayClient[a].ID_CLIENT)>
			<cfset nbContact = listeContactOfRacine.RecordCount >
			<cfset listeMail="">
			<cfloop index="b" from="1" to="#nbContact#">

				<cfset listeMail = listeContactOfRacine["LOGIN_EMAIL"][b] & "," & listeMail>

			</cfloop>

			<cfset mailProperties = getProperties(SESSION.CODEAPPLICATION,'NULL')>

			<cfmail to="#SESSION.USER.EMAIL#" bcc="#listeMail#"  server="#mailProperties.MAIL_SERVER#" port="#mailProperties.MAIL_PORT#" charset="utf-8"  from="#mailProperties.MAIL_EXP_N1#" subject="Nouveaux équipements disponibles pour votre catalogue" type="html">
				Bonjour,<br><br></br>
				Des nouveaux équipements sont disponibles pour votre catalogue.<br><br>
				<cfset dateNow=NOW()>
				Date et heure de traitement : #lsDateFormat(dateNow,"dddd dd mmmm yyyy")# &agrave; #lsTimeFormat(dateNow,"H:mm")#<br><br>

				Liste des équipements disponibles:<br><br>
				<cfloop index="i" from="1" to="#arrayLen(arrayEquip)#">
					Modèle : <b>#arrayEquip[i].MODELE#</b><br>
				</cfloop>

				</b><br>
				Cordialement,<br><br></b>

				<small>Merci de ne pas répondre à cet email, votre réponse ne sera pas traitée.</small>
			</cfmail>
	</cfloop>
</cffunction>


<cffunction name="prevenirClientAdds" output="false" description="" access="remote" returntype="void">
	<cfargument name="arrayEquip" required="true" type="array">
	<cfargument name="arrayClient" required="true" type="array">


		<cfloop index="a" from="1" to="#arrayLen(arrayClient)#">
			<cfset listeContactOfRacine = getContactOfRacine(arrayClient[a].ID_CLIENT)>
			<cfset nbContact = listeContactOfRacine.RecordCount >
			<cfset listeMail="">
			<cfloop index="b" from="1" to="#nbContact#">
				<cfset listeMail = listeContactOfRacine["LOGIN_EMAIL"][b] & "," & listeMail>
			</cfloop>

			<cfset mailProperties = getProperties(SESSION.CODEAPPLICATION,'NULL')>


			<cfmail to="#SESSION.USER.EMAIL#" bcc="#listeMail#"  server="#mailProperties.MAIL_SERVER#" port="#mailProperties.MAIL_PORT#" charset="utf-8"  from="#mailProperties.MAIL_EXP_N1#" subject="Nouveaux équipements ajoutés à  votre catalogue" type="html">

				Bonjour,<br><br></br>
				Des nouveaux équipements ont étés ajoutés dans votre catalogue.<br><br>
				<cfset dateNow=NOW()>
				Date et heure de traitement : #lsDateFormat(dateNow,"dddd dd mmmm yyyy")# &agrave; #lsTimeFormat(dateNow,"H:mm")#<br><br>

				Liste des équipements disponibles:<br><br>
				<cfloop index="i" from="1" to="#arrayLen(arrayEquip)#">
					Modèle : <b>#arrayEquip[i].MODELE#</b><br>
				</cfloop>

				</b><br>
				Cordialement,<br><br></b>

				<small>Merci de ne pas répondre à cet email, votre réponse ne sera pas traitée.</small>

			</cfmail>
	</cfloop>
</cffunction>

<cffunction name="getContactOfRacine" access="remote" returntype="any" description="la liste des gestionnaires des pools de cette racine sans doublon">
	<cfargument name="idRacine"  type="numeric" required="true">

	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.getContactOfRacine">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
		<cfprocresult name="p_result">
	</cfstoredproc>

	<cfreturn p_result>
</cffunction>

<cffunction name="createCatalogueModele" access="remote" returntype="any" description="add equip in catalogue revendeur">
	<cfargument name="IDREVENDEUR" type="numeric" required="true">
	<cfargument name="LIBELLE" type="string" required="true">
	<cfargument name="IDUSER" type="numeric" required="true">
	<cfargument name="IDRACINE" type="numeric" required="true">
	<cfargument name="IDOPERATEUR" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.createCatalogueModele_v2">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDREVENDEUR#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#LIBELLE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDUSER#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#" null="false">
		<cfif IDOPERATEUR eq -1>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDOPERATEUR#" null="true">
		<cfelse>
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#IDOPERATEUR#">
		</cfif>
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="editModele" access="remote" returntype="any" description="add equip in catalogue revendeur">
	<cfargument name="IDMODELE" type="numeric" required="true">
	<cfargument name="LIBELLE" type="string" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.editModele">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDMODELE#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_vARCHAR" value="#LIBELLE#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>
<cffunction name="removeCatalogueModele" access="remote" returntype="any" description="add equip in catalogue revendeur">
	<cfargument name="IDMODELE" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.removeModele">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDMODELE#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>

	<cffunction name="listeTypeEquipement" access="remote" returntype="query">
		<cfargument name="idCat"  type="numeric" required="true">
		<cfargument name="addItemAll"  type="boolean" required="true">
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.listeTypeEquipement_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idCat#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#_idlang#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>
	</cffunction>

	<cffunction name="listeClasseEquipement" access="remote" returntype="query">
		<cfset _idlang = SESSION.USER.IDGLOBALIZATION>
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.listeCategorieEquipement_v2">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" variable="p_idlang" value="#_idlang#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
<!---
add-update-remove
--->
<cffunction name="removeEquipement" access="remote" returntype="any" description="remove equip in catalogue ">
	<cfargument name="idEquip" type="numeric" required="true">
	 <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.removeEquCatalogue">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idEquip#" null="false">
	<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour>
</cffunction>

<cffunction name="getIdTypeProfil" access="remote" returntype="any" description="">

		<cfset Profil = {}>
		<cfset Profil["ID_TYPE_LOGIN"] = SESSION.USER.IDTYPEPROFIL>
		<cfset Profil["LIBELLE_TYPE_LOGIN"] = SESSION.USER.LIBELLETYPEPROFIL>
		<cfset Profil["ID_REVENDEUR"] = SESSION.USER.IDREVENDEUR>
		<cfset Profil["LIBELLE_REVENDEUR"] = SESSION.USER.NOM_REVENDEUR>
		<cfset Profil["LIBELLE_GROUPE_CLIENT"] = SESSION.USER.LIBELLE>


		<cfif val(Profil["ID_REVENDEUR"]) gt 0>

			<cfquery name = "qDeviseDistrib" datasource = "ROCOFFRE" >
					SELECT d.* FROM fournisseur f, devise d WHERE idfournisseur = #val(Profil["ID_REVENDEUR"])#  AND f.iddevise = d.iddevise
			</cfquery>

			<cfif qDeviseDistrib.recordcount gt 0>
				<cfloop query="qDeviseDistrib">
						<cfset Profil["CURRENCY_ID"] = IDDEVISE>
						<cfset Profil["CURRENCY_NAME"] = NAME>
						<cfset Profil["CURRENCY_SHORT_DESC"] = SHORT_DESC>
						<cfset Profil["CURRENCY_SYMBOL"] = SYMBOL>
				</cfloop>
				<cfelse>
					<cfloop query="qDeviseDistrib">
						<cfset Profil["CURRENCY_ID"] = 1>
						<cfset Profil["CURRENCY_NAME"] = "euro">
						<cfset Profil["CURRENCY_SHORT_DESC"] = "EUR">
						<cfset Profil["CURRENCY_SYMBOL"] = "€">
					</cfloop>
			</cfif>
		</cfif>



<cfreturn Profil>

</cffunction>



<cffunction access="private" name="getProperties" returntype="Struct">
		<cfargument name="codeapp" 	type="Numeric" 	required="true">
 		<cfargument name="key" 		type="String" 	required="true"/>

			<cfset properties 	 	= structNew()>
			<cfset mailProperties  	= createObject("component","fr.consotel.consoview.M01.Mailing")>
			<cfset rsltProperties  	= mailProperties.get_appli_properties(codeapp, key)>

			<cfloop query="rsltProperties">
				<cfset codeappPropert[rsltProperties.KEY] = rsltProperties.VALUE>
			</cfloop>

			<cfif NOT isDefined('properties.MAIL_SERVER')>
				<cfset properties.MAIL_SERVER = 'mail.consotel.fr'>
			</cfif>

			<cfif properties.MAIL_SERVER EQ ''>
				<cfset properties.MAIL_SERVER = 'mail.consotel.fr'>
			</cfif>

			<cfif NOT isDefined('properties.MAIL_PORT')>
				<cfset properties.MAIL_PORT = 26>
			</cfif>

			<cfif properties.MAIL_PORT EQ ''>
				<cfset properties.MAIL_PORT = 26>
			</cfif>

			<cfif NOT isDefined('properties.MAIL_FROM_COMMANDE')>
				<cfset properties.MAIL_FROM= 'no-reply@consotel.fr'>
			</cfif>

			<cfif properties.MAIL_FROM EQ ''>
				<cfset properties.MAIL_FROM = 'no-reply@consotel.fr'>
			</cfif>

		<cfreturn properties>
	</cffunction>

<!---
Procédures de démo
--->
<cffunction name="listeConstructeurRacineDemo" access="remote" returntype="query">
	<cfset var myQuery = QueryNew("IDCONSTRUCTEUR,LIBELLECONSTRUCTEUR","Integer,VarChar")>
		<cfscript>
			QueryAddRow(myQuery);
			QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
			QuerySetCell(myQuery,"LIBELLECONSTRUCTEUR","APPLE");
		</cfscript>
		<cfscript>
			QueryAddRow(myQuery);
			QuerySetCell(myQuery,"IDCONSTRUCTEUR",2);
			QuerySetCell(myQuery,"LIBELLECONSTRUCTEUR","ECONOCOM");
		</cfscript>
	<cfreturn myQuery>
</cffunction>

<cffunction name="listeClasseEquipementDemo" access="remote" returntype="query">
	<cfset var myQuery = QueryNew("IDCLASSEEQUIPEMENT,LIBELLECLASSEEQUIPEMENT","Integer,VarChar")>
		<cfscript>
			QueryAddRow(myQuery);
			QuerySetCell(myQuery,"IDCLASSEEQUIPEMENT",1);
			QuerySetCell(myQuery,"LIBELLECLASSEEQUIPEMENT","Stockage");
		</cfscript>
		<cfscript>
			QueryAddRow(myQuery);
			QuerySetCell(myQuery,"IDCLASSEEQUIPEMENT",2);
			QuerySetCell(myQuery,"LIBELLECLASSEEQUIPEMENT","Réseaux");
		</cfscript>
	<cfreturn myQuery>
</cffunction>
<cffunction name="rechercheEquipementConstructeurDemo" access="remote" returntype="query">
<cfset var myQuery = QueryNew("REFERENCE,TYPE,CLASSE,ORIGINE,MODELE,IDEQUIPEMENT,NBRECORD,IDCONSTRUCTEUR,IDCATEGORIE,IDTYPE","VarChar,VarChar,VarChar,VarChar,VarChar,Integer,Integer,Integer,Integer,Integer")>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Générique");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",1);
		QuerySetCell(myQuery,"NBRECORD",4500);
		QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",2);
	</cfscript>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Ajout manuel");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",2);
		QuerySetCell(myQuery,"NBRECORD",4500);
				QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",1);
	</cfscript>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Générique");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",3);
		QuerySetCell(myQuery,"NBRECORD",4500);
				QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",1);
	</cfscript>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Générique");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",3);
		QuerySetCell(myQuery,"NBRECORD",4500);
				QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",1);
	</cfscript>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Générique");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",3);
		QuerySetCell(myQuery,"NBRECORD",4500);
				QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",1);
	</cfscript>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Générique");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",3);
		QuerySetCell(myQuery,"NBRECORD",4500);
				QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",1);
	</cfscript>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Générique");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",3);
		QuerySetCell(myQuery,"NBRECORD",4500);
				QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",1);
	</cfscript>
	<cfscript>
		QueryAddRow(myQuery);
		QuerySetCell(myQuery,"REFERENCE","4561-GH");
		QuerySetCell(myQuery,"TYPE","Terminal");
		QuerySetCell(myQuery,"CLASSE","Mobile");
		QuerySetCell(myQuery,"ORIGINE","Générique");
		QuerySetCell(myQuery,"MODELE","HTC-3600");
		QuerySetCell(myQuery,"IDEQUIPEMENT",3);
		QuerySetCell(myQuery,"NBRECORD",4500);
				QuerySetCell(myQuery,"IDCONSTRUCTEUR",1);
		QuerySetCell(myQuery,"IDCATEGORIE",1);
		QuerySetCell(myQuery,"IDTYPE",1);
	</cfscript>
<cfreturn myQuery>
</cffunction>
</cfcomponent>
