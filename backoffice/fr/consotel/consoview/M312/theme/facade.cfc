<cfcomponent name="facade" displayname="facade">
	
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "/" & mid(oldDateString,4,2) & "/" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>

	<!--- RETOURNE LES PRODUITS DU THEME CHOISI--->	
	<cffunction name="getListeProduitsTheme" access="public" returntype="query">
		<cfargument name="perimetre" 	type="string"  required="false">
		<cfargument name="modeCalcul" 	type="string"  required="false">		
		<cfargument name="numero" 		type="numeric" required="false">
		<cfargument name="idproduit" 	type="numeric" required="true">
		<cfargument name="datedeb" 		type="string"  required="false">
		<cfargument name="datefin" 		type="string"  required="false">		
		
			<cfset obj = CreateObject("component","fr.consotel.consoview.M312.theme.Theme")>
			<cfset obj.init(perimetre,modeCalcul)>
			
			<cfset periE0 = CreateObject("component","fr.consotel.consoview.M312.PerimetreE0")>
					<cfset dataset = periE0.executQuery(1, 0, idproduit, "", 
												session.perimetre.IDRACINE_MASTER,
												session.perimetre.ID_GROUPE,
												numero,
												transformDate(datedeb),
												transformDate(datefin),
												perimetre)>	
		<cfreturn dataset>
	</cffunction>  

</cfcomponent>