package composants.usage
{
	import flash.events.MouseEvent;
	
	import appli.events.EventData;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	public class PopUpUsageImpl extends TitleWindow
	{
		public static const POPUP_USAGE_VALIDED : String = "popUpUsageValided";
		
		// button
		public var btValider 	:Button;
		public var btAnnuler 	:Button;
		
		// textInput
		public var tiNewUsage	:TextInput;
		
		public function PopUpUsageImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		private function init(evt:FlexEvent):void
		{
			initListener()
		}
		
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE,closePopup);
			btValider.addEventListener(MouseEvent.CLICK,btValiderHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,cancelPopup);
		}
		
		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		private function btValiderHandler(evt:MouseEvent):void
		{
			dispatchEvent(new EventData(POPUP_USAGE_VALIDED,false,false,tiNewUsage.text));
			PopUpManager.removePopUp(this);
		}

	}
}