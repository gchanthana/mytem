package commandemobile.ihm.mail
{
	import commandemobile.system.ElementHistorique;
	
	import composants.mail.MailBoxImpl;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.CvDateChooser;
	import composants.util.PopUpImpl;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.registerClassAlias;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.PiecesJointesEvent;
	import gestioncommande.services.Formator;
	import gestioncommande.services.PiecesJointesService;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	import mx.validators.EmailValidator;
	
	[Bindable]
	public class ActionMailBoxImpl extends MailBoxImpl
	{
		/**
	     * Référence vers une autre popUp
	     */
	    protected var _popUp			:PopUpImpl;	    
		
		private	var fileRef				:FileReference;
		
	    public var idsCommande			:Array  = null;
		public var numCommande			:String = '';
		public var isCommandePartielle	:Boolean = false;
		
	    public var idtypecmd			:int 	= 0;
		
		private var _popUpDestinataire	:SelectDestinataireIHM = new SelectDestinataireIHM();
		private var _selectedFunction	:Function;
		
		private var _selectedAction		:Action;
		private var _listeDestinataires	:String;
				
	    /**
	     * La date de l'action
	     */
	    public var dcDateAction: CvDateChooser;

	    /**
	     * Un commentaire pour l'action.
	     */
	    public var txtCommentaire: TextArea;
	    	    
	   	public var envoyerMail:Boolean;	   	
		
		public var cbMail:CheckBox;
		
		public function ActionMailBoxImpl()
		{
			super();
		}
		
		private var _pjSrv			:PiecesJointesService;
		private var _lastIdCommande	:int = 0;
		
		
		public function fournirAttachements(idThisCommande:int):void
		{
			_lastIdCommande = idThisCommande;
			
			_pjSrv = new PiecesJointesService();
			_pjSrv.createUUID();
			_pjSrv.addEventListener(PiecesJointesEvent.FILE_UUID_CREATED, createUUIDHandler);
		}
		
		private function createUUIDHandler(pje:PiecesJointesEvent):void
		{
			infosObject.UUID = _pjSrv.UUID;
			
			_pjSrv.fournirAttachements(_lastIdCommande);
			_pjSrv.addEventListener(PiecesJointesEvent.LAST_FILES_ATTACHED, fournirAttachementsHandler);
		}
		
		private function fournirAttachementsHandler(pje:PiecesJointesEvent):void
		{
			if(_pjSrv.lastFilesAttached != null)
			{
				if(_pjSrv.lastFilesAttached.length > 0)
					infosObject.UUID = _pjSrv.lastFilesAttached[0].UUID;
			}
		}

		/**
	     * Si le mode écriture est à false alors on grise tous les boutons de
	     * modifications (Enregistrer, editer ...)
	     */
	    private var _modeEcriture: Boolean = true;
	    
	    /**
	     * setter pour _modeEcriture
	     * 
	     * @param mode
	     */
	    public function set modeEcriture(mode:Boolean): void
	    {
	    	_modeEcriture = mode;
	    }

	    /**
	     * getter pour _modeEcriture
	     */
	    public function get modeEcriture(): Boolean
	    {
	    	return _modeEcriture;		
	    }
	    
	    public function set popUpDestinataire(value:SelectDestinataireIHM):void
		{
			_popUpDestinataire = value;
		}

		public function get popUpDestinataire():SelectDestinataireIHM
		{
			return _popUpDestinataire;
		}
	    
	    public function set selectedAction(value:Action):void
		{
			_selectedAction = value;
			configurerLesDestinatairesDuMail();
			
		}
		
		public function get selectedAction():Action
		{
			return _selectedAction;
		}
	    
	    protected function removePopUp():void
		{
			if(_popUp != null)
			{
				if(_popUp.isPopUp)
					PopUpManager.removePopUp(_popUp)
				
				_popUp = null;
			}
		}
		
		
		protected function cbMailClickHandler(mEvt:MouseEvent):void
		{
			if(cbMail.selected)
				currentState = "mailState";
			else
				currentState = "noMailState";
		}

		/**
	     * Pour enregistrer l'action à la date du jour
	     * 
	     * @param event
	     */
	    override protected function btnEnvoyerClickHandler(event:MouseEvent): void
	    {
	    	if(currentState ==  "mailState")
	    	{
		    	_mail.destinataire = popUpDestinataire.listeMails;
		    	
		    	if(_mail.destinataire != "" && _mail.destinataire != null)
		    	{	
		    		var bool:Boolean = false;
		    		
					if(txtcc.text != "")
		    		{
						bool = checkEmailAdress(txtcc.text);
						
						if(!bool)
							txtcc.errorString = ResourceManager.getInstance().getString('M16', 'Erreurs_dans_vos_adresses_email_saisies_');
						else
						{
							txtcc.errorString = "";
							
							if(txtcci.text != "")
			    			{
			    		 		bool = checkEmailAdress(txtcci.text);
				    		 	
								if(!bool)
									txtcci.errorString = ResourceManager.getInstance().getString('M16', 'Erreurs_dans_vos_adresses_email_saisies_');
				    		 	else
									txtcci.errorString = "";
				    		}
				  		}
		    		}
		    		else
		    		{
		    			if(txtcci.text != "")
		    			{
		    		 		bool = checkEmailAdress(txtcci.text);
			    		 	
							if(!bool)
								txtcci.errorString = ResourceManager.getInstance().getString('M16', 'Erreurs_dans_vos_adresses_email_saisies_');
			    		 	else
								txtcci.errorString = "";
			    		 }
		    		}
					
		    		if(bool)
		    			sendMail();
		    		else
		    		{
		    			if(txtcc.text == "" && txtcci.text == "")
		    				sendMail();
		    		}
		    	}
		    	else
		    	{
		    		Alert.buttonWidth = 100;
		    		Alert.show(ResourceManager.getInstance().getString('M16', 'Vous_n_avez_pas_choisi_de_destinataire__'));
		    	}
		    }
		    else
		    	goToSend();
	    }
	    
	    private function checkEmailAdress(emails:String):Boolean
	    {
	    	var emailSplited		:Array = emails.split(",");
	    	var resultValidator		:Array = new Array();
	    	var validator:EmailValidator = new EmailValidator();
	    		validator.property = "text";
				
	    	for(var i:int = 0; i < emailSplited.length;i++)
	    	{
	    		if(emailSplited[i] != "")
	    		{
	    			var adressmail	:String 	= emailSplited[i].toString();
	    			var firstIndex	:int		= adressmail.indexOf(" ");
	    			
					if(firstIndex > -1)
	    			{
	    				while(adressmail.indexOf(" ") > -1)
	    				{
	    					adressmail = adressmail.replace(" ","");
	    				}
	    			}
	    			
					resultValidator = EmailValidator.validateEmail(validator, adressmail, "email");
					
					if(resultValidator.length > 0)
					{
						if(resultValidator[0].isError)
							return false;
					}
	    		}
	    	}
			
			return true;
		}
		
		private function sendMail():void
		{
			_mail.cc = txtcc.text.replace(";",",");
			_mail.bcc = txtcci.text.replace(";",",");
			_mail.module = txtModule.text;			
			_mail.copiePourExpediteur = (cbCopie.selected)?"YES":"NO";
			_mail.copiePourOperateur = "NO";			
			_mail.sujet = txtSujet.text;
			_mail.message = rteMessage.text;
			_mail.repondreA = txtExpediteur.text;
			_mail.type = "text/plain";
					
			if(_mail.destinataire != null)
				goToSend();	
			else
			{
				Alert.buttonWidth = 100;
				Alert.okLabel = ResourceManager.getInstance().getString('M16', 'Fermer');
				Alert.show(ResourceManager.getInstance().getString('M16', 'Le_destinataire_est_obligatoire_'));		
			}
		}
			
		private function goToSend():void
		{
			registerClassAlias("fr.consotel.consoview.util.Mail", composants.mail.MailVO);
			
			selectedAction.DATE_ACTION 	= new Date();
    		selectedAction.DATE_HOURS	= formatDate(selectedAction.DATE_ACTION) + " " + formatHour(selectedAction.DATE_ACTION);
			
			var UUID:String = "";
			
			var myfct:String = "envoyer";		
			
			if(infosObject.hasOwnProperty("UUID"))
				UUID = infosObject.UUID;

			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.commande_sncf",
																				myfct,
																				sendTheMailResultHandler);
																			
			RemoteObjectUtil.callService(op,_mail,
											idsCommande,
											idtypecmd,
											selectedAction,
											Formator.formatBoolean(cbMail.selected),
											(infosObject.commande as Commande).IDOPERATEUR,
											UUID,
											numCommande);
		}
		
		private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return year + "/" + temp + "/" + day;
		}
		
		private function formatHour(dateobject:Date):String
		{
			var hours	:String = dateobject.hours.toString();
			var minutes	:String = dateobject.minutes.toString();
			var seconds	:String = dateobject.seconds.toString();

			return hours + ":" + minutes + ":" + seconds;
		}
		    
	    override protected function imgContactsClickHandler(event:MouseEvent):void
		{	
			afficherSelectionContact();
	    }
		
		/*
			"CC" : Liste des contacts client pouvant faire l'action 4
			"CR" : Liste des contact revendeurs ayant un role "Commande"
			"RC" (1): Personne ayant fait l'action 'Demande'
			"RC" (2): Personne ayant fait l'action 'Préparer une commande' et 'Valider et envoyer'
		*/
		
		protected function configurerLesDestinatairesDuMail():void
		{
			removePopUp();
			
			if(selectedAction != null && selectedAction.IDACTION > 0)
			{
				var localCode:String = String(_selectedAction.EXP + _selectedAction.DEST);
				
				switch(localCode)
				{
					case "CC": 	getListeDestinataire(); 			
								break;
					case "CR": 	getListeDestinatairesRevendeur();			
								break;
					case "RC": 	if(_selectedAction.IDACTION == 2068)
									getListeUserAyantFaitAction([2067]);						
								else
									getListeDestinataireUserAction([2066,2070,2116,2216]);
						
								break;
					
					default:
					{
						if(selectedAction.IDACTION == 2078)
						{
							getListeDestinatairesRevendeur();	
						}
						break;
					}
				}
			}
		}

		private var _listeActions:Array;
		
		protected function afficherSelectionContact():void
		{				
			if(_popUpDestinataire.dataProvider != null)
			{
				if(_popUpDestinataire.dataProvider.length == 1)
				{
					_popUpDestinataire.contactsSelectionnes = (_popUpDestinataire.dataProvider as ArrayCollection).source;
					_popUpDestinataire.listeMails 			= ConsoviewUtil.ICollectionViewToList(_popUpDestinataire.dataProvider, 'EMAIL', ',');
				}
				else if(_popUpDestinataire.dataProvider.length > 1)
				{
					PopUpManager.addPopUp(_popUpDestinataire, this, true);
					PopUpManager.centerPopUp(_popUpDestinataire);
				}
				else if(_popUpDestinataire.dataProvider.length == 0)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_mail_configur_'), 'Consoview', null);
				}
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Aucun_mail_configur_'), 'Consoview', null);
		}

		public function getListeDestinataireUserAction(values:Array):void
		{
			_listeActions = new Array();
			_listeActions = values;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.WorkFlowService",
																				"fournirHistoriqueEtatsCommande",
																				getListeDestinataireUserActionResultEvent);
			
			RemoteObjectUtil.callService(op,Commande(infosObject.commande).IDCOMMANDE);
		}
		
		private function getListeDestinataireUserActionResultEvent(re:ResultEvent):void
		{
			if(re.result)
			{   
				var myContatcs		:ArrayCollection = formatContactsHistorique(re.result as ArrayCollection);
				var lenContacts 	:int = myContatcs.length;
				var lenActions	 	:int = _listeActions.length;
				var contactsFinded	:ArrayCollection = new ArrayCollection();
				var i				:int = 0;
				
				for(i = 0;i < lenContacts;i++)
				{
					var idaction:int = myContatcs[i].IDACTION;
					for(var j:int = 0;j < lenActions;j++)
					{
						if(_listeActions[j] == idaction)
							contactsFinded.addItem(myContatcs[i]);
					}	
				}
				
				var contactSorted	:ArrayCollection = Formator.sortFunction(contactsFinded, "EMAIL");
				var lastEmail		:String = '';
				var newContacts		:ArrayCollection = new ArrayCollection();
				
				// Dans la liste trié par ordre alphabétique, éliminer les emails double (ou plus) s'il ya .
				for(i = 0;i < contactSorted.length;i++)
				{
					if(lastEmail != contactSorted[i].EMAIL)
					{
						lastEmail = contactSorted[i].EMAIL;
						
						newContacts.addItem(contactSorted[i]);		
					}
				}
				/* suppression de la personne connecté pour ne pas recevoir une copie de mail*/ 
				var emailGestionnaire:String = CvAccessManager.getSession().USER.EMAIL;
				var indexEmail:Number = ConsoviewUtil.getIndexByLabel(newContacts,"EMAIL",emailGestionnaire);
				if(indexEmail  >= 0 && newContacts.length >= 2)
				{
				 	newContacts.removeItemAt(indexEmail);	
				}

				_popUpDestinataire.dataProvider = newContacts;					
				_popUpDestinataire.listeMails = this.getLastEmailInActions(newContacts);
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M16', 'Pas_de_destinataire_configur__'));
		}
		
		private function getLastEmailInActions(values:ArrayCollection):String
		{
			var lastEmailUser:String = '';
			var taille:int = values.length;
			if(taille == 0)
				lastEmailUser = '';
			else if(taille ==1)
				lastEmailUser = values[0].EMAIL;
			else
			{
				var lastDate1Timestamp : Number = (values[0].DATE_ACTION as Date).getTime();
				lastEmailUser = values[0].EMAIL;
				
				for(var i:int = 1; i<taille ; i++)
				{ 
					var date2Timestamp : Number = (values[i].DATE_ACTION as Date).getTime();
					
					if (date2Timestamp > lastDate1Timestamp)
					{
						lastDate1Timestamp = date2Timestamp;
						lastEmailUser = values[i].EMAIL;
					}
				}
			}
			
			return lastEmailUser;
		}
		
		private function formatContactsHistorique(values:ArrayCollection):ArrayCollection
		{
			var retour 		:ArrayCollection = new ArrayCollection();
			var curretnList	:ArrayCollection = Formator.sortFunction(values, "EMAIL");
			
			if(curretnList != null)
			{					
				for(var i:int = 0; i < values.length;i++)
				{
					var ctcObj:Object = new Object();
					
					if(values[i].hasOwnProperty('NOM')){
						ctcObj.NOM 			= (curretnList[i].NOM != null)?curretnList[i].NOM:'';
					}
					else if(values[i].hasOwnProperty('PATRONYME_USERACTION'))
					{
						ctcObj.NOM 			= (curretnList[i].PATRONYME_USERACTION != null)?curretnList[i].PATRONYME_USERACTION:'';
					}
					
					if(values[i].hasOwnProperty('EMAIL')){
						ctcObj.EMAIL 		= (curretnList[i].EMAIL != null)?curretnList[i].EMAIL:'';
					}
					
					if(values[i].hasOwnProperty('IDINV_ACTIONS')){
						ctcObj.IDACTION		= (curretnList[i].IDINV_ACTIONS != null)?curretnList[i].IDINV_ACTIONS:-1;
					}
					else if(values[i].hasOwnProperty('IDACTION'))
					{
						ctcObj.IDACTION		= (curretnList[i].IDACTION != null)?curretnList[i].IDACTION:-1;
					}
					
					if(values[i].hasOwnProperty('DATE_ACTION')){
						ctcObj.DATE_ACTION	= (curretnList[i].DATE_ACTION != null)?curretnList[i].DATE_ACTION:'';
					}
					
					if(values[i].hasOwnProperty('LIBELLE_ACTION')){
						ctcObj.LIBELLE_ACTION	= (curretnList[i].LIBELLE_ACTION != null)?curretnList[i].LIBELLE_ACTION:'';
					}
					
					retour.addItem(ctcObj);		
				}
			}

			return retour;
		}
		
		public function getListeDestinataire():void
	    {
			var procedure:String = '';
			
			if(selectedAction.IDACTION == 2366 || selectedAction.IDACTION == 2367)
				procedure = 'getHistContact';
			else
				procedure = 'getContactsActSuivante';
			
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.commande.GestionWorkFlow",
																				procedure,
																				getListeDestinataireResultEvent);
																				
			RemoteObjectUtil.callService(op,Commande(infosObject.commande).IDPOOL_GESTIONNAIRE,
											selectedAction.IDACTION,
											Commande(infosObject.commande).IDCOMMANDE);
	    }
	    
	    private function getListeDestinataireResultEvent(re:ResultEvent):void
	    {
	    	if(re.result)
	    	{    
				var myContatcs:ArrayCollection = formatConatcts(re.result as ArrayCollection);
    			
				_popUpDestinataire.dataProvider = myContatcs;
				
				if(selectedAction.IDACTION == 2366 || selectedAction.IDACTION == 2367)
				{
					imgContacts.enabled			= false;
					imgContacts.visible 		= false;
					imgContacts.includeInLayout = false;
					
					popUpDestinataire.listeMails = ConsoviewUtil.arrayToList(myContatcs, "EMAIL", ",");
				}
				else
					afficherSelectionContact();    			
    		}
	    	else
	    		Alert.show(ResourceManager.getInstance().getString('M16', 'Pas_de_destinataire_configur__'));
	    }
	    
	    public function getListeDestinatairesRevendeur():void
	    {
			var idRev	:int = infosObject.commande.IDREVENDEUR;
	    	var idrole	:int = whatIsTypeCommande(infosObject.commande.IDTYPE_COMMANDE);
	    	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
																				"getContactsRevendeurWithRole",
																				getListeDestinatairesRevendeurResultEvent);
																	
			RemoteObjectUtil.callService(op,idRev,
											idrole);
	    }
	    
	    private function getListeDestinatairesRevendeurResultEvent(re:ResultEvent):void
	    {
	    	if(re.result)    	
			{
				var listeContactsRevendeur:ArrayCollection = re.result as ArrayCollection;
				_popUpDestinataire.dataProvider = listeContactsRevendeur;
				
				if(infosObject.hasOwnProperty('historiqueCommande'))
				{
					var contactsHistorique:ArrayCollection = formatContactsHistorique(infosObject.historiqueCommande as ArrayCollection);
					var lenContacts 	:int = contactsHistorique.length;
					var i				:int = 0;
					
					/* Comparer le dernier email dans l'historique avec les email des revendeurs
					si existe : on affiche le dernier email comme destination de email
					sinon     : affiche une liste pour choisir la destination
					*/
					var dernierEmail:String = this.getLastEmailInActions(contactsHistorique);
					
					for(var k:int=0; k< listeContactsRevendeur.length; k++)
					{
						if(dernierEmail == listeContactsRevendeur[k]['EMAIL'])
						{
							_popUpDestinataire.listeMails = dernierEmail;
							break;
						}
						else
						{
							afficherSelectionContact();
							break;
						}
					}
				}
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M16', 'Pas_de_destinataire_configur__pour_ce_re'));
	    }
	    
		private function whatIsTypeCommande(idtypecmde:int):int
		{
			var idrole:int = 101;
			
			switch(idtypecmde)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		idrole = 101;	break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		idrole = 101; 	break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: idrole = 201;	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	idrole = 201;	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			idrole = 6; 	break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	idrole = 6; 	break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		idrole = 202; 	break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	idrole = 202; 	break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			idrole = 203; 	break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		idrole = 203; 	break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		idrole = 203; 	break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	idrole = 203; 	break;//--- REACT
			}
			
			return idrole;
		}
	    
		public function getListeUserAyantFaitAction(selectedAction:Array):void
		{
			var dataProvider:ArrayCollection = new ArrayCollection();
			
			if(infosObject.hasOwnProperty("historiqueCommande"))
			{
				var cursor:IViewCursor = (infosObject.historiqueCommande as ArrayCollection).createCursor();
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.isPresent(cursor.current.IDACTION.toString(),selectedAction))
					{
						if(!ConsoviewUtil.isLabelInArray(cursor.current.EMAIL,"EMAIL",dataProvider.source))
						{
							dataProvider.addItem({NOM:cursor.current.PATRONYME_USERACTION,
								EMAIL:cursor.current.EMAIL})	
						}
					}
					cursor.moveNext();
				}
				
				_popUpDestinataire.dataProvider = dataProvider;
				_popUpDestinataire.contactsSelectionnes = dataProvider.source;
				_popUpDestinataire.listeMails = ConsoviewUtil.ICollectionViewToList(dataProvider,"EMAIL",",");
			}
		}	
		
		
		
		private function formatConatcts(values:ICollectionView):ArrayCollection
	    {
	    	var retour : ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{	
				var cursor : IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					var ctcObj:Object = new Object();					
					ctcObj.NOM = cursor.current.NOM;	
					ctcObj.EMAIL = cursor.current.LOGIN_EMAIL;
					retour.addItem(ctcObj);		
					cursor.moveNext();
				}
			}
			
			return retour;
	    }
		
	}
}
