package gestionparc.ihm.action.mdm.actionRevocation
{
	import gestionparc.ihm.action.ICommand;

	public class ActionRevocationCommand implements ICommand
	{

		private var receiver:ActionRevocationReceiver=null;

		public function ActionRevocationCommand(rec:ActionRevocationReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
