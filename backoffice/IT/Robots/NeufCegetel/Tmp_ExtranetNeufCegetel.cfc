<cfcomponent name="Tmp_ExtranetNeufCegetel" output="false">
	<cfset variables.instance.dateDansAdresse1=0>
	<cffunction name="Main" access="remote" returntype="xml" output="false" description="Main" hint="Main" displayname="Main">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfargument name="extranet" type="String">
		<cfargument name="nbreMois" type="numeric">
		<cfargument name="shiftMois" type="numeric">
		<cfargument name="compte" type="string">
		
	
		<!--- trim: supprime les espaces au debut et a la fin de la chaine --->
		<cfif Trim(compte) eq "">
			<CFSET compte="INCONNU">
		</cfif>
		
		
		<!--- Type d'accès --->		
		<cfif Trim(extranet) eq 1 or Trim(extranet) eq 2>
			<cfset Typeextranet='Simple'> <!--- Client Simple. Tout compte--->
			
			<!--- <cfelseif Trim(extranet) eq 2>  <!--- Client Simple. Compte par compte--->
				<cfset Typeextranet='SimpleByCompte'> --->
				<cfelseif Trim(extranet) eq 3 or Trim(extranet) eq 4>  <!--- Client Multi. Tout compte--->
					<cfset Typeextranet='Multi'>
					<!--- <cfelseif Trim(extranet) eq 4>  <!--- Client Multi. Compte par compte--->
						<cfset Typeextranet='MultiByCompte'> --->
		</cfif>
		
		<cfset tempChaine = "<html><body><table border='0'>">
		<cfset compteurFichier=0>

		<!--- On definie les objets --->
		<cfset setStrategy(Typeextranet)>


		<!--- On se log --->	
		<cfset jCookie = "">					
		<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>

		 <cfif jCookie eq "" >	
			 <!--- Probl�me de connexion � l'extranet --->	
		 	<cfoutput >Pas possible de se connecter � l'extranet neufCegetel <br/></cfoutput>
			<cfscript>
				 MyDoc = XmlNew();
				 MyDoc.xmlRoot = XmlElemNew(MyDoc,"MyRoot");
			</cfscript>		 
			<cfreturn MyDoc>
		</cfif>
					
		<cfoutput >preparer strategie OK <br/></cfoutput>
		
		<!--- Preparation de l'extranet avant d'aller a la page des factures --->
		<!--- Ramener la liste des sociètés concernes --->
		<cfset aSocietes=ArrayNew(2)>
		<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
		
		<!--- <cfdump var="#aSocietes#">
		<cfabort showerror="fin"> --->
		
		<!--- Aller chercher les factures selon le type de l'extranet --->
		<cfif Trim(extranet) eq 10>
		 <!--- =========================================== Client Simple. Tout compte============================================--->
				<cfset ListeFacture = ArrayNew(1)>
				
				<!---Aller chercher la liste des liens des factures--->
				<cfset LienFacture = ArrayNew(1)>
				<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,"TOUS",login,pwd,compte)>
																
				<!--- Télécharger les facture a partir de leurs liens --->								
				<cfloop from="1" to="#ArrayLen(LienFacture)#" index="j">
							
							<cfset FileName="">			
							<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,LienFacture[j],login,pwd,compte)>
							<cfif FileName neq "">
											<cfset ListeFacture[j]=#FileName#>	
									<cfelse >
											<cfset ListeFacture[j]="Facture non récupérée">
											<!--- On se log --->	
											<cfset jCookie = "">					
											<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
											<cfset aSocietes=ArrayNew(2)>
											<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
											
									</cfif>
								
				</cfloop>
															
				
				<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
							server="192.168.3.119" 
							port="26"  type="html" 
							subject="[Robot SFR Fixe]- Type 1 - Step 1">
					
							Bonjour. <br/>
							
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
							
							<cfdump var="#ListeFacture#">
							
				</cfmail> 
	
			
			
			
		<cfelseif Trim(extranet) eq 1> 
		<!--- =========================================== Client Simple. Compte par compte ===========================================--->				
				<!---Aller chercher la liste des comptes de facturation--->
				<cfset aCompte = ArrayNew(1)>
				<cfset aCompte=variables.instance.ProcessStrategy.ListeCompte(jCookie,login,pwd,compte)>
				
				<!---Téléchargement des factures--->
				<cfset ListeFacture = ArrayNew(1)>
				<cfset k = 0> <!---Indice facture--->
				<cfloop from="1" to="#ArrayLen(aCompte)#" index="i">	
						<!---Aller chercher la liste des liens des factures--->
						<cfset LienFacture = ArrayNew(1)>
						<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,aCompte[i],login,pwd,compte)>
																		
						<!--- si error = -1 alors on se reconnecte avant d'aller chercher la liste des factures qu'on a pu récupéré avant la deconnexion --->
						<cfif ArrayLen(LienFacture) neq 0>
							<cfset error = LienFacture[1]>	
							<cfif error eq -1>
								<cfset ArrayDeleteAt(LienFacture,1)>								
								<!--- On se log --->	
								<cfset jCookie = "">					
								<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
								
								<!--- Preparation de l'extranet avant d'aller a la page des factures --->
								<cfset aSocietes=ArrayNew(2)>
								<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>							
							</cfif>		
						</cfif>												
						
						<!--- Télécharger les facture a partir de leurs liens --->
						<cfset TmpListeFacture = ArrayNew(1)>							
						<cfloop from="1" to="#ArrayLen(LienFacture)#" index="j">
									
									<cfset FileName="">			
									<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,LienFacture[j],login,pwd,compte)>
									<cfif FileName neq "">
											<cfset k = k+1>	
											<cfset ListeFacture[k]=#FileName#>	
											<cfset TmpListeFacture[j]=#FileName#>	
									<cfelse >
											<cfset k = k+1>	
											<cfset ListeFacture[k]="Facture non récupérée">
											<cfset TmpListeFacture[j]="Facture non récupérée">
											<!--- Problème de connexion, on se reconencte --->	
											<cfset jCookie = "">					
											<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
											<cfset aSocietes=ArrayNew(2)>
											<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
											
									</cfif>
										
						</cfloop>
						
						<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
									server="192.168.3.119" 
									port="26"  type="html" 
									subject="[Robot SFR Fixe]- Type 2 - Step 1">
							
									Bonjour. <br/>
									
									Nom client = #compte#<br/>
									Login = #login#<br/>
									Mot de passe = #pwd#<br/>
									Compte#1# = #aCompte[i]#<br/>
									
									<cfdump var="#TmpListeFacture#">
							
						</cfmail> 
															
				 </cfloop>
				
				<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
							server="192.168.3.119" 
							port="26"  type="html" 
							subject="[Robot SFR Fixe]- Type 2 - Step 1">
					
							Bonjour. <br/>
							
							Compte = #compte#<br/>
							Login = #login#<br/>
							Mot de passe = #pwd#<br/>
							
							<cfdump var="#ListeFacture#">
							
				</cfmail> 
		
		
		<cfelseif Trim(extranet) eq 3>  
		<!--- =========================================== Client Multi. Tout compte=========================================== --->
				<cfif ArrayLen(aSocietes) eq 1>
						<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[1][1],aSocietes[1][2])>								
						<cfif jCookie neq "">			
								<!---Aller chercher la liste des liens des factures--->
								<cfset LienFacture =  ArrayNew(1) >
								<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,"TOUS",login,pwd,compte,aSocietes[1][2])>
								
								<!---Téléchargement des factures--->
								<cfset ListeFacture = ArrayNew(1)>	
								<cfloop from="1" to="#ArrayLen(LienFacture)#" index="j">		
									<cfset FileName="">									
										<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,LienFacture[j],login,pwd,compte,aSocietes[1][2])>
										<cfif FileName neq "">
												<cfset ListeFacture[j]=#FileName#>	
										<cfelse >
												<cfset ListeFacture[j]="Facture non récupérée">
												<!--- Problème de connexion, on se reconencte --->		
												<cfset jCookie = "">					
												<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
												
												<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
												
												<cfset jCookie=variables.instance.ProcessStrategy.connectCompte(jCookie,login,pwd,compte,aSocietes[1][1],aSocietes[1][2])>			
												
												<cfset LienFacture =  ArrayNew(1) >
												<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,"TOUS",login,pwd,compte,aSocietes[1][2])>					
		
												
										</cfif>																					
									</cfloop>
								
								<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
											server="192.168.3.119" 
											port="26"  type="html" 
											subject="[Robot SFR Fixe]- Type 3 - Step 1">
									
											Bonjour. <br/>
											
											Compte = #compte#<br/>
											Login = #login#<br/>
											Mot de passe = #pwd#<br/>
											Société: #aSocietes[1][2]#<br/>
											
											<cfdump var="#ListeFacture#">
											
								</cfmail> 
								
						<cfelse >	
									<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
										server="192.168.3.119" 
										port="26"  type="html" 
										subject="[Robot SFR Fixe]- Type 3 - Step 1- Erreur">
								
											Bonjour. <br/>
											
											Compte = #compte#<br/>
											Login = #login#<br/>
											Mot de passe = #pwd#<br/>
											
											Pas possible de se connecter à la société #aSocietes[1][2]#<br/>
											
											Cordialement.<br/>
										
							</cfmail> 
								
						</cfif>	
				
				
				
				<cfelse >
						<cfset ListeFacture = ArrayNew(1)>	
						<cfset k = 0>		
						<cfloop from="2" to="#ArrayLen(aSocietes)#" index="i">
						
								<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[i][1],aSocietes[i][2])>								
								<cfif jCookie neq "">			
										<!---Aller chercher la liste des liens des factures--->
										<cfset LienFacture = ArrayNew(1)>	
										<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,"TOUS",login,pwd,compte,aSocietes[i][2])>	
	
										<cfif jCookie neq "">												
												<cfset ListeFacture = ArrayNew(1)>								
												<!---Téléchargement des factures--->
												<cfloop from="1" to="#ArrayLen(LienFacture)#" index="j">		
														<cfset FileName="">									
														<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,LienFacture[j],login,pwd,compte,aSocietes[i][2])>
														<cfif FileName neq "">
																<cfset ListeFacture[j]=#FileName#>	
														<cfelse >
																<cfset ListeFacture[j]="Facture non récupérée">
																<!--- Problème de connexion, on se reconencte --->							
																<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
																
																<cfset aSocietes=ArrayNew(2)>														
																<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
																<cfif ArrayLen(aSocietes) gt 0>
																		<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[i][1],aSocietes[i][2])>								
													
																		<cfset LienFacture = ArrayNew(1)>	
																		<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,"TOUS",login,pwd,compte,aSocietes[i][2])>	
																</cfif>
														</cfif>																					
												</cfloop>
												
												<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
														server="192.168.3.119" 
														port="26"  type="html" 
														subject="[Robot SFR Fixe]- Type 3 - Step 2">
												
														Bonjour. <br/>
														
														Compte = #compte#<br/>
														Login = #login#<br/>
														Mot de passe = #pwd#<br/>
														Société: #aSocietes[i][2]#<br/>
														
														<cfdump var="#ListeFacture#">
										
												</cfmail> 
										
											
										<cfelse>
												<!--- Problème de connexion, on se reconencte --->	
												<cfset jCookie = "">					
												<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
												<cfset aSocietes=ArrayNew(2)>
												<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>								
										</cfif>
									
											
								<cfelse >	
										<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
											server="192.168.3.119" 
											port="26"  type="html" 
											subject="[Robot SFR Fixe]- Type 3 - Step 2- Erreur">
									
											Bonjour. <br/>
											
											Compte = #compte#<br/>
											Login = #login#<br/>
											Mot de passe = #pwd#<br/>
											
											Pas possible de se connecter à la société #aSocietes[i][2]#<br/>
											
											Cordialement.<br/>
											
										</cfmail> 
										
										<!--- Problème de connexion, on se reconencte --->		
										<cfset jCookie = "">					
										<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
										<cfset aSocietes=ArrayNew(2)>
										<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
										
								</cfif>	
							
						</cfloop>
				
				</cfif>
				
				
		
		<cfelseif Trim(extranet) eq 4>  
		<!--- =========================================== Client Multi. Compte par compte ===========================================--->
			 <cfif ArrayLen(aSocietes) eq 1>
						<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[1][1],aSocietes[1][2])>		
												
						<cfif jCookie neq "">	
						
								<!---Aller chercher la liste des comptes de facturation--->
								<cfset aCompte = ArrayNew(1)>
								<cfset aCompte=variables.instance.ProcessStrategy.ListeCompte(jCookie,login,pwd,compte,aSocietes[1][2])>
			
								<!---Téléchargement des factures--->
								<cfset ListeFacture = ArrayNew(1)>
								<cfset k = 0> <!---Indice facture--->
								<cfloop from="1" to="#ArrayLen(aCompte)#" index="l">	
										<!---Aller chercher la liste des liens des factures--->
										<cfset LienFacture = ArrayNew(1)>
										<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,aCompte[l],login,pwd,compte,aSocietes[1][2])>
																									
										<!--- si error = -1 alors on se reconnecte --->
										<cfif ArrayLen(LienFacture) neq 0>
											<cfset error = LienFacture[1]>	
											<cfif error eq -1>
												<cfset ArrayDeleteAt(LienFacture,1)>								
												<!--- On se log --->	
												<cfset jCookie = "">					
												<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
												<cfif jCookie neq "">
														<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
														 <cfif ArrayLen(aSocietes) gt 0>
																<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[1][1],aSocietes[1][2])>			
														</cfif>									
												</cfif>
												
																
											</cfif>		
										</cfif>												
										
										<!--- Télécharger les facture a partir de leurs liens --->																	
										<cfloop from="1" to="#ArrayLen(LienFacture)#" index="j">
													
													<cfset FileName="">			
													<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,LienFacture[j],login,pwd,compte,aSocietes[1][2])>
													<cfif FileName neq "">
															<cfset k = k+1>	
															<cfset ListeFacture[k]=#FileName#>															
													<cfelse >
															<cfset k = k+1>	
															<cfset ListeFacture[k]="Facture non récupérée">												
															<!--- Problème de connexion, on se reconencte --->	
															<cfset jCookie = "">	
															<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
															<cfif jCookie neq "">
																	<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
																	 <cfif ArrayLen(aSocietes) gt 0>
																			<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[1][1],aSocietes[1][2])>		
																			<cfif jCookie neq "">
																					<cfset LienFacture = ArrayNew(1)>											
																					<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,aCompte[l],login,pwd,compte,aSocietes[1][2])>	
																			</cfif>	
																	</cfif>									
															</cfif>				
															
															
													</cfif>
														
										</cfloop>
																
								 </cfloop>
								
								<!--- <cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
											server="192.168.3.119" 
											port="26"  type="html" 
											subject="[Robot SFR Fixe]- Type 4 - Step 1">
									
											Bonjour. <br/>
											
											Compte = #compte#<br/>
											Login = #login#<br/>
											Mot de passe = #pwd#<br/>
											Société= #aSocietes[1][2]#<br/>
											Liste de toutes les factures de la société #aSocietes[1][2]#<br/>
											<cfdump var="#ListeFacture#">
											
								</cfmail>  --->
		
			
						<cfelse >	
								<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
									server="192.168.3.119" 
									port="26"  type="html" 
									subject="[Robot SFR Fixe]- Type 4 - Step 1- Erreur">
							
										Bonjour. <br/>
											
										Compte = #compte#<br/>
										Login = #login#<br/>
										Mot de passe = #pwd#<br/>
										
										Pas possible de se connecter à la société #aSocietes[1][2]#<br/>
										
										Cordialement.<br/>														
								</cfmail> 
							
						</cfif>	
				
				
				
				<cfelse >
						<cfset ListeFacture = ArrayNew(1)>	
						<cfset k = 0>		
						<cfloop from="2" to="#ArrayLen(aSocietes)#" index="i">			
								<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[i][1],aSocietes[i][2])>								
								<cfif jCookie neq "">			
										<!---Aller chercher la liste des comptes de facturation--->
										<cfset aCompte = ArrayNew(1)>
										<cfset aCompte=variables.instance.ProcessStrategy.ListeCompte(jCookie,login,pwd,compte,aSocietes[i][2])>
																				
										<!---Téléchargement des factures--->
										<cfset ListeFacture = ArrayNew(1)>
										<cfset k = 0> <!---Indice facture--->
										<cfloop from="1" to="#ArrayLen(aCompte)#" index="l">	
												<!---Aller chercher la liste des liens des factures--->
												<cfset LienFacture = ArrayNew(1)>
												<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,aCompte[l],login,pwd,compte,aSocietes[i][2])>
												<!--- si error = -1 On se reconnecte --->
												<cfif ArrayLen(LienFacture) neq 0>
													<cfset error = LienFacture[1]>	
													<cfif error eq -1>
														<cfset ArrayDeleteAt(LienFacture,1)>								
														<!--- On se log --->	
														<cfset jCookie = "">		
														<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>			
														<cfif jCookie neq "">
																<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
																 <cfif ArrayLen(aSocietes) gt 0>
																		<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[i][1],aSocietes[i][2])>													
																 </cfif>									
														 </cfif>		
																	
													</cfif>		
												</cfif>												
												
												
												<!--- Télécharger les facture a partir de leurs liens --->		
												<cfset TmpListeFacture = ArrayNew(1)>							
												<cfloop from="1" to="#ArrayLen(LienFacture)#" index="j">
															
															<cfset FileName="">			
															<cfset FileName=variables.instance.ProcessStrategy.getFile(jCookie,LienFacture[j],login,pwd,compte,aSocietes[i][2])>
															<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[i][1],aSocietes[i][2])>		
															<cfif FileName neq "">
																	<cfset k = k+1>	
																	<cfset ListeFacture[k]=#FileName#>		
																	<cfset TmpListeFacture[j]=#FileName#>																	
															<cfelse >
																	<cfset k = k+1>	
																	<cfset ListeFacture[k]="Facture non récupérée">
																	<cfset TmpListeFacture[j]="Facture non récupérée">	
																	<!--- Problème de connexion, on se reconencte --->	
																	<cfset jCookie = "">	
																	<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>				
																	<cfif jCookie neq "">
																			<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
																			 <cfif ArrayLen(aSocietes) gt 0>
																					<cfset jCookie=variables.instance.ProcessStrategy.connectSociete(jCookie,login,pwd,compte,aSocietes[i][1],aSocietes[i][2])>		
																					<cfif jCookie neq "">
																							<cfset LienFacture = ArrayNew(1)>											
																							<cfset LienFacture=variables.instance.ProcessStrategy.getListeFactures(jCookie,nbreMois,shiftMois,aCompte[l],login,pwd,compte,aSocietes[i][2])>	
																					</cfif>	
																			</cfif>									
																	</cfif>	
																	
															</cfif>
																
												</cfloop>
												
												
												<!--- <cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
														server="192.168.3.119" 
														port="26"  type="html" 
														subject="[Robot SFR Fixe]- Type 4 - Step 2">
													
															Bonjour. <br/>
															
															Compte = #compte#<br/>
															Login = #login#<br/>
															Mot de passe = #pwd#<br/>
															Société= #aSocietes[i][2]#<br/>
															aCompte[#l#]=#aCompte[l]#
															<cfdump var="#TmpListeFacture#">
															
												</cfmail> --->
													
																												
										</cfloop>
										
										<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
													server="192.168.3.119" 
													port="26"  type="html" 
													subject="[Robot SFR Fixe]- Type 4 - Step 2">
											
													Bonjour. <br/>
													
													Compte = #compte#<br/>
													Login = #login#<br/>
													Mot de passe = #pwd#<br/>
													Société= #aSocietes[i][2]#<br/>
													Liste de toutes les factures de la société #aSocietes[i][2]#<br/>
													<cfdump var="#ListeFacture#">
													
										</cfmail> 
												
									
								<cfelse >	
					
										<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" 
											server="192.168.3.119" 
											port="26"  type="html" 
											subject="[Robot SFR Fixe]- Type 4 - Step 2- Erreur">
									
											Bonjour. <br/>
											
											Compte = #compte#<br/>
											Login = #login#<br/>
											Mot de passe = #pwd#<br/>
											
											Pas possible de se connecter à la société #aSocietes[i][2]#<br/>
											
											Cordialement.<br/>
											
										</cfmail> 
										
										<!--- Problème de connexion, on se reconencte --->		
										<cfset jCookie = "">					
										<cfset jCookie=variables.instance.ProcessStrategy.Login(login,pwd,compte)>
										<cfset aSocietes=ArrayNew(2)>
										<cfset aSocietes=variables.instance.ProcessStrategy.PrepareExtranet(jCookie,login,pwd,compte)>
										
								</cfif>

								
						</cfloop>
						
										
				</cfif>


		
		</cfif>
		


		<cfoutput > Fin OK </b></cfoutput>
			
		<cfscript>
			MyDoc = XmlNew();
			MyDoc.xmlRoot = XmlElemNew(MyDoc,"MyRoot");
		</cfscript>		 
		<cfreturn MyDoc>
	</cffunction>
		
	<!--- Pour mettre en place la strategie --->
	<cffunction name="setStrategy" access="public" returntype="void" output="false" >
		<cfargument name="TYPE_EXTRANET" required="false" type="string" default="" displayname="string TYPE_EXTRANET" hint="type de pï¿½rimetre" />		
		<cfset temp=createObject("component","Tmp_#TYPE_EXTRANET#ProcessStrategy")>		
		<cfset setProcessStrategy(temp)>
	</cffunction>
	
	<!--- Setter pour la strategie --->
	<cffunction name="setProcessStrategy" access="public" output="false" returntype="void">
		<cfargument name="newProcessStrategy" type="ProcessStrategy" required="true" />
		<cfset variables.instance.ProcessStrategy = arguments.newProcessStrategy />
	</cffunction>
	
	
</cfcomponent>


