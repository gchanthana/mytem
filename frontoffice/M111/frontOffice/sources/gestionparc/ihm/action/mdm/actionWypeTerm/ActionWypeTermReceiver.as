package gestionparc.ihm.action.mdm.actionWypeTerm
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.ConfirmWithPassWordIHMEvent;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.services.mdm.MDMService;
	import gestionparc.utils.ConfirmWithPassBoxManager;
	
	import mx.resources.ResourceManager;

	public class ActionWypeTermReceiver implements IReceiver
	{
		private var confirmBoxManager:ConfirmWithPassBoxManager=new ConfirmWithPassBoxManager();

		private var _mdmSrv:MDMService;

		public function ActionWypeTermReceiver()
		{
			_mdmSrv=new MDMService();
		}

		public function action():void
		{
			var msg:String=ResourceManager.getInstance().getString('M111', 'Effacement_des_donn_es_entreprise_du_ter') + SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			confirmBoxManager.displayConfirmBox(msg, WYPE_TERM_CloseHandler, SpecificationVO.getInstance().displayObjectRef);
		}

		private function WYPE_TERM_CloseHandler(e:ConfirmWithPassWordIHMEvent):void
		{
			if (e.detail == ConfirmWithPassWordIHMEvent.PASS_KO)
			{
				_mdmSrv.wipe(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, Number(SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL), false);

				confirmBoxManager.hideConfirmBox();

				//SpecificationVO.getInstance().dispatchEventRefresh();
			}
		}
	}
}
