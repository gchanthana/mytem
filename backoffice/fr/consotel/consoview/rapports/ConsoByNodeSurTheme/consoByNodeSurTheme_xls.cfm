<cfheader name="Content-Disposition" value="inline;filename=rapport_consoParNoeudSurTheme_#replace(rapportParams.RAISON_SOCIALE,' ','_')#.xls">
<cfcontent type="application/vnd.ms-excel">
<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY euro "&#8364;">]>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Brice Miramont</Author>
  <LastAuthor>cedric.rapiera</LastAuthor>
  <Created>2007-02-05T08:50:51Z</Created>
  <LastSaved>2007-02-06T14:35:37Z</LastSaved>
  <Version>11.6408</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>9795</WindowHeight>
  <WindowWidth>16320</WindowWidth>
  <WindowTopX>480</WindowTopX>
  <WindowTopY>105</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font x:Family="Swiss" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s18" ss:Name="Monétaire">
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="m169503352">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m169503362">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169517588">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169517598">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="m169517608">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="m169517618">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s21">
   <Font x:Family="Swiss" ss:Size="14" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s22">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s35">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s36">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Double" ss:Weight="3"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s37">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s39">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
  </Style>
  <Style ss:ID="s41" ss:Parent="s18">
   <Borders/>
   <Font x:Family="Swiss" ss:Color="#000000"/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s42" ss:Parent="s18">
   <Borders>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s43" ss:Parent="s18">
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s44">
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
   <ss:NumberFormat ss:Format="_-* #,##0.00\ &quot;&euro;&quot;_-;\-* #,##0.00\ &quot;&euro;&quot;_-;_-* &quot;-&quot;??\ &quot;&euro;&quot;_-;_-@_-"/>
  </Style>
  <Style ss:ID="s45">
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Color="#000000" ss:Bold="1"/>
   <NumberFormat ss:Format="0"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
 </Styles>


<cfset nbPg = 0>
<cfset nbLignesTotal = 0>
<cfoutput>
<cfloop index="z" from="1" to="#qGetData.recordcount#" step="#(NB_COL * MAX_ROWS)#">
	<cfset nbPg = nbPg + 1>
 <Worksheet ss:Name="Feuille #nbPg#">
  <Table x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65.25">
   <Column ss:AutoFitWidth="0" ss:Width="320"/>
 	<cfloop index="j" from="3" to="#(3 + (NB_COL * 3))#">
	   <Column ss:Index="#j#" ss:AutoFitWidth="0" ss:Width="130"/>
	</cfloop>
   <Column ss:Width="50"/>

   <Row ss:AutoFitHeight="0" ss:Height="18">
    <Cell ss:StyleID="s21"><Data ss:Type="String">Consos des lignes par Organisation</Data></Cell>
    <Cell ss:Index="5" ss:StyleID="s21"><Data ss:Type="String">#rapportParams.RAISON_SOCIALE#</Data></Cell>
   </Row>

   <Row>
    <Cell><Data ss:Type="String">Période</Data></Cell>
    <Cell><Data ss:Type="String">#LSDateFormat(parseDateTime(rapportParams.DATEDEB),'Mmmm YYYY')# à fin #LSDateFormat(parseDateTime(rapportParams.DATEFIN),'Mmmm YYYY')#</Data></Cell>
   </Row>

   <Row ss:Index="4">
    <Cell ss:MergeDown="1" ss:StyleID="m169517588"><Data ss:Type="String">Organisation</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="m169517598"><Data ss:Type="String">Nbre Lignes</Data></Cell>
 	<cfloop index="j" from="1" to="#NB_COL#">
	    <Cell ss:MergeAcross="2" ss:StyleID="m169517608"><Data ss:Type="String">#qGetData['SUR_THEME'][j]# - (#qGetData['SEGMENT_THEME'][j]#)</Data></Cell>
 	</cfloop>
    <Cell ss:MergeDown="1" ss:StyleID="m169503362"><Data ss:Type="String">Total Coûts</Data></Cell>
   </Row>

	<cfset startIdx1 = 3>
   <Row ss:AutoFitHeight="0" ss:Height="13.5">
 	<cfloop index="j" from="1" to="#NB_COL#">
	    <Cell ss:Index="#startIdx1#" ss:StyleID="s35"><Data ss:Type="String">Durée (min)</Data></Cell>
	    <Cell ss:StyleID="s36"><Data ss:Type="String">Nbre Appels</Data></Cell>
	    <Cell ss:StyleID="s22"><Data ss:Type="String">Coût TTC</Data></Cell>
	    <cfset startIdx1 = startIdx1 + 3>
 	</cfloop>
   </Row>

	<cfset startIdx1 = 3>
	<cfset nbLignes = 0>
	<cfloop index="k" from="1" to="#qGetData.recordcount#" step="#NB_COL#">
		<cfif (nbLignesTotal EQ qGetData.recordcount) OR (nbLignes GTE MAX_ROWS)>
			<cfbreak>
		</cfif>
		<cfset nbLignes = nbLignes + 1>
	   <Row ss:AutoFitHeight="0" ss:Height="13.5">
		    <Cell ss:StyleID="s37"><Data ss:Type="String">#qGetData['LIBELLE_GROUPE_CLIENT'][nbLignesTotal + 1]#</Data></Cell>
		    <Cell ss:StyleID="s41"><Data ss:Type="Number">#qGetData['NBSOUS_TETE'][nbLignesTotal + 1]#</Data></Cell>
		<cfset totalMontant = 0>
		<cfloop index="m" from="1" to="#NB_COL#">
		    <Cell ss:StyleID="s41"><Data ss:Type="Number">#qGetData['DUREE_APPEL'][nbLignesTotal + m]#</Data></Cell>
		    <Cell ss:StyleID="s41"><Data ss:Type="Number">#qGetData['NOMBRE_APPEL'][nbLignesTotal + m]#</Data></Cell>
		    <Cell ss:StyleID="s42"><Data ss:Type="Number">#qGetData['MONTANT_FINAL'][nbLignesTotal + m]#</Data></Cell>
			<cfset totalMontant = totalMontant + qGetData['MONTANT_FINAL'][nbLignesTotal + m]>
		    <cfset startIdx1 = startIdx1 + 3>
		</cfloop>
	    <Cell ss:StyleID="s43"><Data ss:Type="Number">#totalMontant#</Data></Cell>
	   </Row>
		<cfset nbLignesTotal = nbLignesTotal + NB_COL>
	</cfloop>
   <Row>
    <Cell ss:StyleID="s44"><Data ss:Type="String">TOTAL</Data></Cell>
    <Cell ss:StyleID="s45"/>
 	<cfloop index="j" from="1" to="#NB_COL#">
	    <Cell ss:StyleID="s45" ss:Formula="=SUM(R[-#(nbLignes)#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
	    <Cell ss:StyleID="s44"/>
	    <Cell ss:StyleID="s44" ss:Formula="=SUM(R[-#(nbLignes)#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
 	</cfloop>
    <Cell ss:StyleID="s44" ss:Formula="=SUM(R[-#(nbLignes)#]C:R[-1]C)"><Data ss:Type="Number"></Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>5</ActiveRow>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</cfloop>
</Workbook>
</cfoutput>