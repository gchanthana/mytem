package gestionparc.ihm.action.extension.actionextvoirhistorique 
{
	import gestionparc.ihm.action.ICommand;

	public class ActionExtVoirHistoriqueCommand implements ICommand
	{
		private var receiver:ActionExtVoirHistoriqueReceiver=null;

		public function ActionExtVoirHistoriqueCommand(rec:ActionExtVoirHistoriqueReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
