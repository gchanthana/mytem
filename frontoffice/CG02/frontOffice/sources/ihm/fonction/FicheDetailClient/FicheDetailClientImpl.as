package ihm.fonction.FicheDetailClient
{
	import entity.ClientPartenaireVO;
	import entity.ClientVO;
	
	import event.ficheDetailClient.FicheDetailClientEvent;
	
	import flash.events.MouseEvent;
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import service.FicheDetailClient.FicheDetailClientServices;
	
	public class FicheDetailClientImpl extends TitleWindow
	{
/* ******************************************************************************************************
		1) PUBLIC
****************************************************************************************************** */
		public var btnValider:Button;
		public var btnFermer:Button;
		
		private var _CLIENT_ID:int; // public, cf getter / setter
		
		public function FicheDetailClientImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		
/* ******************************************************************************************************
		2) PRIVATE
****************************************************************************************************** */			
		[Bindable] private var _client:ClientVO;
		
		private var serv:FicheDetailClientServices;
		
/* 2.1) PHASE D'INITIALISATION */
		private function init(evt:FlexEvent):void
		{
			initVar();
			initData();
			initListener();
		}
		private function initVar():void
		{
			this.serv = new FicheDetailClientServices();
		}
		private function initData():void
		{
			getInfosClient(this.CLIENT_ID);
		}
		private function initListener(boolAdd:Boolean = true):void
		{
			this.addEventListener(CloseEvent.CLOSE,close);
			btnValider.addEventListener(MouseEvent.CLICK,btnValider_clickHandler);
			btnFermer.addEventListener(MouseEvent.CLICK,btnClose_clickHandler);
		}
/* 2.2) PHASE HANDLER */
		protected function close(evt:CloseEvent=null):void
		{
			PopUpManager.removePopUp(this);
		}
		protected function btnValider_clickHandler(event:MouseEvent):void
		{
			close();
		}
		protected function btnClose_clickHandler(event:MouseEvent):void
		{
			close();
		}
/* 2.3) PHASE SERVICE */
		private function getInfosClient(clientId:int):void
		{
			this.serv.myDatas.addEventListener(FicheDetailClientEvent.proc_DETAIL_CLIENT_CONTRACTUEL_EVENT,getDetailClientContractuelHandler);
			this.serv.getDetailClientContractuel(clientId);
		}
		private function getDetailClientContractuelHandler(evt:FicheDetailClientEvent):void
		{
			this.CLIENT = this.serv.myDatas.client;
			if(this.serv.myDatas.hasEventListener(FicheDetailClientEvent.proc_DETAIL_CLIENT_CONTRACTUEL_EVENT))
				this.serv.myDatas.removeEventListener(FicheDetailClientEvent.proc_DETAIL_CLIENT_CONTRACTUEL_EVENT,getDetailClientContractuelHandler);
		}
/* 2.4) PHASE UTILS */
		protected function sortCompareFunctionPartenaire(obj1:Object, obj2:Object):int
		{
			if((obj1 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase() == (obj2 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase())
				return 0
			else if((obj1 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase() > (obj2 as ClientPartenaireVO).PARTENAIRE.NOM.toUpperCase())
				return 1
			else
				return -1
		}
		protected function sortCompareFunctionOffre(obj1:Object, obj2:Object):int
		{
			if((obj1 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase() == (obj2 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase())
				return 0;
			else if((obj1 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase() > (obj2 as ClientPartenaireVO).OFFRE.LABEL.toUpperCase())
				return 1
			else
				return -1
		}
		protected function sortCompareFunctionApplication(obj1:Object, obj2:Object):int
		{
			if((obj1 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase() == (obj2 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase())
				return 0
			else if((obj1 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase() > (obj2 as ClientPartenaireVO).OFFRE.APPLICATION_NAME.toUpperCase())
				return 1
			else
				return -1
		}
/* ******************************************************************************************************
		3) GETTER / SETTER
****************************************************************************************************** */		
		public function get CLIENT_ID():int { return _CLIENT_ID; }
		public function set CLIENT_ID(value:int):void
		{
			if (_CLIENT_ID == value)
				return;
			_CLIENT_ID = value;
		}
		[Bindable] public function get CLIENT():ClientVO { return _client; }
		public function set CLIENT(value:ClientVO):void
		{
			if (_client == value)
				return;
			_client = value;
		}
	}
}