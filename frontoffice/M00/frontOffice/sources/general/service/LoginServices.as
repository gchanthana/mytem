package general.service
{
	import mx.rpc.AbstractOperation;
	
	import entity.CodeLangue;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Appelle les procédures liées aux collaborateurs.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * </pre></p>
	 * 
	 */
	public class LoginServices
	{
		// VARIABLES------------------------------------------------------------------------------
		
		[Bindable] public var myDatas 		: LoginDatas;
		[Bindable] public var myHandlers 	: LoginHandlers;
		
		// VARIABLES------------------------------------------------------------------------------

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Instancie les datas.
		 * Instancie les handlers.
		 * </pre></p>
		 * 
		 */	
		public function LoginServices()
		{
			myDatas 	= new LoginDatas();
			myHandlers 	= new LoginHandlers(myDatas);
		}
		/**
		 *	<code>deleteTokenFromCookie</code>
		 * 
		 * 	@param codeApp : le code application correspondant à l'appli.
		 * 
		 * 	Appel une fonction cf qui va supprimer la clé SSO "se souvenir" du cookie. 
		 */
		public function deleteTokenFromCookie(codeApp:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.connectApp.AppUtil", 
				"deleteTokenFromCookie",
				myHandlers.recupererDeleteTokenFromCookie);
			RemoteObjectUtil.callSilentService(op,codeApp);
		}
		/**
		 *	<code>login</code> 
		 * 
		 * 	@param login
		 * 	@param mdp
		 * 	@param codeApp
		 * 	@param codeConnection
		 * 	
		 * 	Permet de se logger à l'univers consoview grâce à un login/mdp
		 */
		public function login(login:String,mdp:String,codeApp:int,codeConnection:int, codeLangue:CodeLangue,seSouvenir:Boolean,supportMode:Boolean =false):void
		{
			trace("LoginServices.login(login, mdp, codeApp, codeConnection, codeLangue, seSouvenir)");
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.M00.ConnectionManager", 
								"connectTo",
								myHandlers.recupererProcessLogin);
			var objConnection:Object = new Object();
			objConnection.login = login;
			objConnection.pwd = mdp;
			objConnection.codeLangue = codeLangue;
			objConnection.seSouvenir = seSouvenir?1:0;
			objConnection.supportMode = supportMode?1:0;
			RemoteObjectUtil.callService(op,codeApp,codeConnection, objConnection);
		}
		/**
		 *	<code>changePerimetre</code> 
		 * 
		 * 	@param idperimetre id du périmètre auquel on se connecte
		 * 	@param codeApp
		 * 	@param codeConnection
		 * 	
		 * 	Permet de se connecter en sous périmètre
		 */
		public function changePerimetre(idperimetre:int, codeApp:int,codeConnection:int):void
		{
			trace("LoginServices.changePerimetre(idperimetre, codeApp, codeConnection)");
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.ConnectionManager", 
				"ChangePerimetre",
				myHandlers.recupererChangePerimetre);
			
			var objConnection:Object = new Object();
			objConnection.idperimetre = idperimetre;
			objConnection.idgroupe = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			
			RemoteObjectUtil.callService(op,codeApp,codeConnection, objConnection);
		}
		/**
		 *	<code>changeGroupe</code> 
		 * 
		 * 	@param idperimetre id du groupe sur lequel on se connecte
		 * 	@param codeApp
		 * 	@param codeConnection
		 * 	
		 * 	Permet de se connecter à un groupe particulier
		 */
		public function changeGroupe(idperimetre:int, codeApp:int,codeConnection:int):void
		{
			trace("LoginServices.changeGroupe(idperimetre, codeApp, codeConnection)");
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.ConnectionManager", 
				"ChangeGroupe",
				myHandlers.recupererChangePerimetre);
			
			var objConnection:Object = new Object();
			objConnection.idperimetre = idperimetre;
			
			RemoteObjectUtil.callService(op,codeApp,codeConnection, objConnection);
		}
		/**
		 *	<code>connectViaSSOSFR</code> 
		 * 
		 * 	@param token token sécurisé et temporaire du SSO
		 * 	@param codeApp
		 * 	@param codeConnection
		 * 	
		 * 	Permet de se connecter à l'appli pilotage via le mode SSO SBOL
		 */
		public function connectViaSSOSFR(token:String, codeApp:int,codeConnection:int):void
		{
			trace("LoginServices.connectViaSSOSFR(token, codeApp, codeConnection)");
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.ConnectionManager", 
				"connectTo",
				myHandlers.recupererConnectToSSO);
			
			var objConnection:Object = new Object();
			objConnection.token = token;
			objConnection.etape = 1;
			
			RemoteObjectUtil.callService(op,codeApp,codeConnection, objConnection);
		}
		/**
		 *	<code>connectViaSSOCV</code> 
		 * 	@param token token sécurisé et temporaire du SSO
		 * 	@param codeApp
		 * 	@param codeConnection
		 * 	
		 * 	Permet de se connecter à consoview via le mode SSO
		 */
		public function connectViaSSOCV(token:String, codeApp:int,codeConnection:int):void
		{
			trace("LoginServices.connectViaSSOCV(token, codeApp, codeConnection)");
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M00.ConnectionManager", 
				"connectTo",
				myHandlers.recupererConnectToSSO);
			
			var objConnection:Object = new Object();
			objConnection.token = token;
			objConnection.codeapp = codeApp;
			
			RemoteObjectUtil.callService(op,codeApp,codeConnection, objConnection);
		}
		/**
		 * 	createLoginByTitu()
		 * 
		 * 	3ème étape du SSO SFR si la connection par clé SSO et par liste titu a échoué :
		 * 	- création du login
		 *  - connection via ce login fraichement créé
		 */
		 public function createLoginByTitu(codeApp:int,codeConnection:int):void
		 {
			 trace("LoginServices.createLoginByTitu(codeApp, codeConnection)");
			 
			 var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				 "fr.consotel.consoview.M00.ConnectionManager", 
				 "ConnectTo",
				 myHandlers.recupererCreateLoginByTitu);
			 
			 var objConnection:Object = new Object();
			 objConnection.etape = 3;
			 objConnection.codeApp = codeApp;
			 
			 RemoteObjectUtil.callService(op,codeApp,codeConnection, objConnection);
		 }
		 

		 /**
		 * <code>updateUserPassword()</code>
		 * @param oldPassword
		 * @param newPassword
		 * 
		 * method to modify the user password 
		  */
		 public function updateUserPassword(currentPassword:String, newPassword:String):void
		 {
			 var op2:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				 "fr.consotel.consoview.M00.UserAccessService", "updateUserCredentials", myHandlers.confirmUpdateUserInfos);
			 
			 var userInfos:Object = new Object();
			 userInfos.clientAccesId = CvAccessManager.getSession().USER.CLIENTACCESSID;
			 userInfos.nom 			= CvAccessManager.getSession().USER.NOM;
			 userInfos.prenom 		= CvAccessManager.getSession().USER.PRENOM;
			 userInfos.email 		= CvAccessManager.getSession().USER.EMAIL;
			 userInfos.newPassword 	= newPassword;
			 userInfos.restrictIp 	= 0;
			 
			 RemoteObjectUtil.callService(op2, currentPassword, userInfos);
		 }
	}
}