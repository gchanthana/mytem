package gestionparc.ihm.fiches.ficheEntity.contentTabNav
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.EmplacementVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.FicheSiteIHM;
	import gestionparc.ihm.fiches.popup.PopUpSearchSiteIHM;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.site.SiteServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.Text;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class ContentSiteFicheEquipementImpl extends VBox
	{
		public var cbSite				:ComboBox;
		public var cbEmplacement		:ComboBox;
		public var txtAdresse			:Text;
		public var txtBatiment			:Text;
		public var txtEtage				:Text;
		public var txtSalle				:Text;
		public var txtCouloir			:Text;
		public var txtArmoire			:Text;
		public var lbCodePostal			:Label;
		public var lbVille				:Label;
		public var lbPays				:Label;
		public var btAdd				:Button;
		public var btModify				:Button;
		public var imgSearchSite		:Image;
		public var vbEmplacement		:VBox;
		private var _popupSearchSite	:PopUpSearchSiteIHM;
		private var ficheSite			:FicheSiteIHM;

		public var myServicesCache		:CacheServices=null
		private var myServices			:SiteServices=null;
		
		private var _siteSelected		:Boolean = false;

		public function ContentSiteFicheEquipementImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			myServices=new SiteServices();
			initListener();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			cbSite.dataProvider=CacheDatas.siteLivraison;
			cbSite.addEventListener(ListEvent.CHANGE, onCbSiteChangeHandler);
			cbEmplacement.addEventListener(ListEvent.CHANGE, onCbEmplacementChangeHandler);

			imgSearchSite.addEventListener(MouseEvent.CLICK, onClickImgSearchSiteHandler);

			btAdd.addEventListener(MouseEvent.CLICK, onBtAddClickHandler);
			btModify.addEventListener(MouseEvent.CLICK, onBtModifyClickHandler);
		}

		/**
		 * Appellée après le click du bouton "Modifier" site.
		 */
		private function onBtModifyClickHandler(me:MouseEvent):void
		{
			if (cbSite.selectedItem)
			{
				displayFicheSite(false, (cbSite.selectedItem as SiteVO).idSite);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_site_'), ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
		}

		/**
		 * Appellée après le click du bouton "Ajouter" site.
		 */
		private function onBtAddClickHandler(me:MouseEvent):void
		{
			displayFicheSite(true, -1);
		}

		/**
		 * Affiche la fiche site.
		 */
		private function displayFicheSite(isNewFiche:Boolean, idSite:int):void
		{
			ficheSite=new FicheSiteIHM();
			ficheSite.isNewSite=isNewFiche;

			if (!isNewFiche)
			{
				ficheSite.idSite=idSite;
			}

			ficheSite.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, onInfosFicheSiteUploadedHandler);
			PopUpManager.addPopUp(ficheSite, this.parent.parent, true);
			PopUpManager.centerPopUp(ficheSite);
		}

		/**
		 * Appellée après la mise a jour des infos d'un site depuis sa fiche.
		 */
		private function onInfosFicheSiteUploadedHandler(event:gestionparcEvent):void
		{
			//this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED,true));
			myServicesCache.getSiteLivraison(); //récuperer à nouveau la liste des sites de livraisons
			myServicesCache.myDatas.addEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
		}

		private function onListeSiteLoadedhandler(event:gestionparcEvent):void
		{
			//myServicesCache.myDatas.removeEventListener(gestionparcEvent.LISTE_SITE_LOADED,onListeSiteLoadedhandler);
			cbSite.dataProvider=CacheDatas.siteLivraison;
			var myS:SiteServices=new SiteServices;
			myS=ficheSite.getObjSiteServices();

			if (myS.myDatas.site.idSite > 0)
			{
				for each (var item:Object in CacheDatas.siteLivraison)
				{
					if (item.idSite == myS.myDatas.site.idSite)
					{
						cbSite.selectedItem=item;
						txtAdresse.text=item.adresse1;
						lbCodePostal.text=item.codePostale;
						lbVille.text=item.ville;
						lbPays.text=item.pays;
						break;
					}
				}
			}
			else
			{
				cbSite.selectedIndex=-1;
				txtAdresse.text="";
				lbCodePostal.text="";
				lbVille.text="";
				lbPays.text="";
			}

			var objToServiceGet:Object=new Object();
			objToServiceGet.idSite=myS.myDatas.site.idSite;
			myServices.getEmplacement(objToServiceGet);
			myServices.myDatas.addEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedhandler);
		}

		// handlers click image
		private function onClickImgSearchSiteHandler(me:MouseEvent):void
		{
			var sites:ArrayCollection = cbSite.dataProvider as ArrayCollection;
			if(cbSite.selectedIndex > -1)
				this.majSelectPool(sites);
			_popupSearchSite=new PopUpSearchSiteIHM();
			_popupSearchSite.listeSites = sites;
			_popupSearchSite.addEventListener('GET_SELECTED_SITE_ISVALIDATE', getSiteSelectedHandler);
			PopUpManager.addPopUp(_popupSearchSite, parent.parent, true);
			PopUpManager.centerPopUp(_popupSearchSite);
		}
		
		private function majSelectPool(sites:ArrayCollection):void
		{
			var longSites:int = sites.length;
			var i:int = 0;
			while(i < longSites)
			{
				sites[i].SELECTED = false;
				if(sites[i].idSite == cbSite.selectedItem.idSite)
				{
					sites[i].SELECTED = true;
					_siteSelected = true;
				}
				i++;
			}
			
		}
		
		//
		private function getSiteSelectedHandler(evt:Event):void
		{
			removeEventListener('GET_SELECTED_SITE_ISVALIDATE', getSiteSelectedHandler);

			var selectedSite:Object=_popupSearchSite.getSite();
			if (selectedSite != null)
			{
				cbSite.selectedItem=selectedSite;
				txtAdresse.text=(selectedSite as SiteVO).adresse1;
				lbCodePostal.text=(selectedSite as SiteVO).codePostale;
				lbVille.text=(selectedSite as SiteVO).ville;
				lbPays.text=(selectedSite as SiteVO).pays;

				var objToServiceGet:Object=new Object();
				objToServiceGet.idSite=(selectedSite as SiteVO).idSite;
				myServices.getEmplacement(objToServiceGet);
				myServices.myDatas.addEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedhandler);
			}

			PopUpManager.removePopUp(_popupSearchSite);
		}


		/**
		 * Applique les changements par rapport au site sélectionné.
		 */
		private function onCbSiteChangeHandler(le:ListEvent):void
		{
			if (le.currentTarget.selectedItem)
			{
				txtAdresse.text=(le.currentTarget.selectedItem as SiteVO).adresse1;
				lbCodePostal.text=(le.currentTarget.selectedItem as SiteVO).codePostale;
				lbVille.text=(le.currentTarget.selectedItem as SiteVO).ville;
				lbPays.text=(le.currentTarget.selectedItem as SiteVO).pays;

				var objToServiceGet:Object=new Object();
				objToServiceGet.idSite=(le.currentTarget.selectedItem as SiteVO).idSite;
				myServices.getEmplacement(objToServiceGet);
				myServices.myDatas.addEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedhandler);
			}
		}

		/**
		 * Applique les changements par rapport à l'emplacement sélectionné.
		 */
		private function onCbEmplacementChangeHandler(le:ListEvent):void
		{
			if (le.currentTarget.selectedItem)
			{
				txtBatiment.text=(le.currentTarget.selectedItem as EmplacementVO).batiment;
				txtEtage.text=(le.currentTarget.selectedItem as EmplacementVO).etage;
				txtSalle.text=(le.currentTarget.selectedItem as EmplacementVO).salle;
				txtCouloir.text=(le.currentTarget.selectedItem as EmplacementVO).couloir;
				txtArmoire.text=(le.currentTarget.selectedItem as EmplacementVO).armoire;
			}
		}

		/**
		 * Charge les données recues de la base concernant les emplacements dans la combobox "cbEmplacement".
		 */
		private function onInfosEmplaSiteLoadedhandler(evt:gestionparcEvent):void
		{
			cbEmplacement.dataProvider=new ArrayCollection();
			cbEmplacement.dataProvider=myServices.myDatas.listEmplacement;
			cbEmplacement.dropdown.dataProvider=myServices.myDatas.listEmplacement;

			cbEmplacement.selectedIndex=-1;
			txtBatiment.text="";
			txtEtage.text="";
			txtSalle.text="";
			txtCouloir.text="";
			txtArmoire.text="";

			var longueurTabEmplacement:int=myServices.myDatas.listEmplacement.length;

			if (longueurTabEmplacement < 1)
			{
				cbEmplacement.prompt=resourceManager.getString('M111', 'Aucun_emplacement_disponible');
			}
			else if (longueurTabEmplacement > 1)
			{
				cbEmplacement.prompt=resourceManager.getString('M111', 'S_lectionner_un_emplacement');
			}
			else if (longueurTabEmplacement == 1)
			{
				for each (var item:Object in myServices.myDatas.listEmplacement)
				{
					cbEmplacement.selectedItem=item;
					txtBatiment.text=item.batiment;
					txtEtage.text=item.etage;
					txtSalle.text=item.salle;
					txtCouloir.text=item.couloir;
					txtArmoire.text=item.armoire;
				}
			}
			myServices.myDatas.removeEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedhandler);
		}
	}
}