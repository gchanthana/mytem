package univers.parametres.parametrelogin.vo
{
	import flash.utils.describeType;

	[Bindable]
	public class AccessVO
	{
				
		public var nomModule:String = "";
		public var codeModule:String = "";
		public var idModule:Number = 0;
		public var appLogModuleId:Number = 0;
		public var isActif:Number = -1;
		public var isActifRacineParam:Number = -1;
		public var libelle:String = "";
		public var idParam:Number = 0;
		public var codeParam:String = "";
		public var commentaire:String = "";
		public var selected:Boolean = false;
		public var isUpdated:Number = 0;
		
		public function AccessVO()
		{
		}
		
		public function addAccess(value:Object):void
		{
			this.nomModule = value.NOM_MODULE;
			this.codeModule = value.CODE_MODULE;
			this.idModule = value.MODULE_CONSOTELID;
			this.appLogModuleId = value.GROUPE_APP_LOG_MODULEID;
			if(value.IS_ACTIF!='null'){this.isActif = value.IS_ACTIF;}
			if(value.IS_ACTIF_RACINE_PARAM!='null'){this.isActifRacineParam = value.IS_ACTIF_RACINE_PARAM;}
			this.libelle = value.LIBELLE;
			this.idParam = value.ID_PARAM;
			this.codeParam = value.CODE_PARAMS;
			this.commentaire = value.COMMENTAIRE;
			if(value.IS_ACTIF!='null'){
				if(value.IS_ACTIF==1){this.selected = true}else{this.selected = false}
			}
			else{
				this.selected = false
			}
			this.isUpdated = value.isUpdated;
		}
		
		public function check(select:Boolean):void{
			this.selected = select;
			this.isActif = (select)? 1 : 0;
		}
		
		public function cast(value:Object):Boolean
		{
			var _cast_status : Boolean = true; //true si le caste à fonctionné pour ttes les propriété 
			
			
			var classInfo 	:XML = describeType(this);
			
			for each (var v:XML in classInfo..accessor)
			{
				if (this.hasOwnProperty(v.@name))
				{
					this[v.@name] = value[v.@name];
				}
				else
				{
					_cast_status = false
				} 
			}
		
			return 	_cast_status
		}
		
	}
}