package service
{
	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import utils.MessageAlerte;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Envoie les données reçues à la classe <code>FctGestionProfilDatas</code>,
	 * au retour des procédures appelées dans <code>FctGestionProfilServices</code>.
	 * 
	 * Version 	: 1.0
	 * Date 	: 05.06.2013
	 * 
	 * </pre></p>
	 */
	
	public class FctGestionProfilHandlers
	{
		// VARIABLES -----------------------------------------------------------------------------
		
		private var myDatas	:FctGestionProfilDatas;
		
		// PUBLIC FUNCTIONS ----------------------------------------------------------------------
		
		public function FctGestionProfilHandlers(instanceDatas:FctGestionProfilDatas)
		{
			this.myDatas = instanceDatas;
		}
		
		public function getClientsParUtilisateurHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				myDatas.treatGetClientsParUtilisateur(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR),MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		public function getProfilsParClientHandlers(re:ResultEvent):void
		{
			if(re.result != null)
			{
				myDatas.treatGetProfilsParClients(re.result.DATA);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
			
		}
		
		public function creerProfilHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				myDatas.treatCreerProfil(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		public function modifierProfilHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				myDatas.treatModifierProfil(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		public function supprimerProfilHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				myDatas.treatSupprimerProfil(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
		}
		
		public function getListeFonctionUserHandler(re:ResultEvent):void
		{				
			if(re.result != null)
			{
				myDatas.treatListeFonctionUser(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
			
		}
		
		public function getListeFonctionProfilHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				myDatas.treatListeFonctionProfil(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR), MessageAlerte.msgErrorPopupTitle);
			}
			
		}
	}
}