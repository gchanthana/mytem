package univers.inventaire.commande.ihm
{
	import flash.errors.IllegalOperationError;
	
	import univers.inventaire.commande.system.TypesCommandesMobile;
	import univers.inventaire.contrat.ihm.GererOptionsAbonnementIHM;
	import univers.inventaire.contrat.ihm.RenouvelerContratIHM;
	
	public class GestionArticlesFactory
	{
		public function GestionArticlesFactory()
		{
		}
		
		public static function create(typeCommande:Number):CreerArticleImpl
		{
			switch(typeCommande)
			{
				case TypesCommandesMobile.EQUIPEMENTS_NUS:
				{
					return new CreerEquipementNueIHM();
					break;
				}
				case TypesCommandesMobile.NOUVELLES_LIGNES:
				{
					return new CreerNouvelleLigneIHM();
					break;
				}
				case TypesCommandesMobile.MODIFICATION_OPTIONS:
				{
					return new GererOptionsAbonnementIHM();
					break;
				}
				case TypesCommandesMobile.MODIFICATION_OPTIONS:
				{
					return new GererOptionsAbonnementIHM();
					break;
				}
				case TypesCommandesMobile.RENOUVELLEMENT:
				{
					return new RenouvelerContratIHM()
					break;
				}
				default:
				{
					throw new IllegalOperationError("Type de commande inconnue")
				}
			}
		}
		

	}
}