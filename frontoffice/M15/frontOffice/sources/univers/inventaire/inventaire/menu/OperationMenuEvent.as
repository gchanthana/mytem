package univers.inventaire.inventaire.menu
{
	import flash.events.Event;
	
	
	/**
	 * Classe Evenment dispatcher par l'onglet 'Operations'
	 * Permet de passer les parametres concernant une opération
	 **/ 
	public class OperationMenuEvent extends Event
	{
		//Constante static definissant le type ShowSuivitOperation (montrer le WorkFlow d'un operation)
		private static const ShowSuivitOperation : String = "ShowSuivitOperation";
		
		//Constante static definissant le type ShowSuivitOperationRapp (montrer une operation de verification)
		private static const ShowSuivitOperationRapp : String = "ShowSuivitOperationRapp";
		
		//Constante static definissant le type ShowNouvelleLigne (montrer le formulaire vide nouvelle ligne, ou nouveau produit)
		private static const ShowNouvelleLigne : String = "ShowNouvelleLigne";
		
		//Constante static definissant le type ShowDemande (montrer un formulaire en particulier)
		private static const ShowDemande : String = "ShowDemande";
		
		//Constante static definissant le type DemandeDeleted (Commande effacée)
		private static const DemandeDeleted : String = "DemandeDeleted";
		
		//le numero de l'operation
		private var numOperation : String;
		
		//Booleen qui permet de savoir si l'evenement concerne une commande
		private var _isDemande : Boolean = false;
		
		//L'identifiant de l'operation ou de la commande
		private var idOperation : int;
		
		//Qualificatif pour l'opération (en cours = 1, cloturée = 0)
		private var opEnCours : int;
		
		//Le type d'operation
		private var typeOperation : int;
		
		/**
		 * Constructeur
		 * Par defaut l'evenement est de type ShowSuivitOperation (Montrer WorkFlow pour une opération)
		 * Voir la classe flash.events.Event
		 * */
		public function OperationMenuEvent(type : String = "ShowSuivitOperation",bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		
		/**
		 * setter pour le Booleen permettant de savoir si l'evenement concerne une Commande
		 * @param b  
		 * */
		public function set isDemande(b : Boolean):void{
			_isDemande = b;
		}
		
		
		/**
		 * setter pour le numero de l'operation
		 * @param num le numero de l'operation  
		 * */
		public function set numero_Operation(num : String):void{
			numOperation = num;
		}
		
		
		/**
		 * setter pour le type d'operation
		 * @param num le le type d'operation
		 * */
		public function set type_Operation(num : int):void{
			typeOperation = num;
		}
		
		/**
		 * setter pour l'identifiant de l'operation ou de la commande
		 * @param num l'identifiant de l'operation ou de la commande
		 * */
		public function set id_Operation(num : int):void{
			idOperation = num;
		}
		
		/**
		 * setter pour le qualificatif de l'operation
		 * @param num le qualificatif de l'operation   
		 * */
		public function set op_En_Cours(num : int):void{
			opEnCours = num;
		}
		
		/**
		 * getter pour le numero de l'operation
		 * @return num le numero de l'operation  
		 * */
		public function get numero_Operation():String{
			return numOperation;
		}
		
		
		/**
		 * getter pour l'identifiant de l'operation
		 * @return num l'identifiant de l'operation
		 * */
		public function get id_Operation():int{
			return idOperation;
		}
		
		
		/**
		 * getter pour le qualificatif 'En Cours' de l'operation
		 * @return le qualificatif de l'operation 1 pour operation en cours 0 pour operation cloturer 
		 * */
		public function get op_En_Cours():int{
			return opEnCours;
		}
		
		
		/**
		 * getter pour le booleen qualifiant l'element (commande ou operation)
		 * @return num le booleen qualifiant l'element true pour une commande ou false pour une operation
		 * */
		public function get isDemande():Boolean{
			return _isDemande;
		}
		
		/**
		 * getter pour le type d'operation 
		 * @return num le numero de l'operation  
		 * */
		public function get type_Operation():int{
			return typeOperation;
		}
		
		
		private var _infosDemende : Object;
		public function get infosDemende():Object{
			return _infosDemende;
		};
		public function set infosDemende(infos : Object):void{
			_infosDemende = infos;
		}
		
		
		/**
		 * getter static pour le type d'évenement ShowSuivitOperation (Montrer le WorkFlow pour une operation)
		 * @return ShowSuivitOperation
		 * */		
		public static function get SHOW_SUIVIT_OPERATION():String{
			return ShowSuivitOperation;
		}
		
		
		/**
		 * getter static pour le type d'évenement ShowNouvelleLigne (Montrer le formulaire de saisie de nouvelles lignes ou nouveaux produits)
		 * @return ShowSuivitOperation
		 * */
		public static function get SHOW_NOUVELLES_DEMANDE():String{
			return ShowNouvelleLigne;
		}
		
		/**
		 * getter static pour le type d'évenement ShowDemande (Montrer le detail d'une commande)
		 * @return ShowSuivitOperation
		 * */
		public static function get SHOW_DEMANDE():String{
			return ShowDemande;
		}
		
		/**
		 * getter static pour le type d'évenement DemandeDeleted (Demande effacée)
		 * @return DemandeDeleted
		 * */
		public static function get DEMANDE_DELETED():String{
			return DemandeDeleted;
		}
		
	}
}