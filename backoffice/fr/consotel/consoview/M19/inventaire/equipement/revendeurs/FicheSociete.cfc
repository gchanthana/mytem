<!--- =========================================================================
Classe: FicheSociete
Auteur: brice.miramont
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="FicheSociete" hint="Pour la gestion des fiches societes et des agences" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="FicheSociete" hint="Remplace le constructeur de FicheSociete.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->

	<cffunction name="create_cde_contact_societe" access="public" returntype="numeric" output="false" hint="Création de la fiche société ou agence." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="Raison_sociale" required="false" type="string" default="" displayname="string Raison_sociale" hint="Initial value for the Raison_socialeproperty." />
		<cfargument name="SIREN" required="false" type="string" default="" displayname="string SIREN" hint="Initial value for the SIRENproperty." />
		<cfargument name="ADRESSE1" required="false" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="false" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="false" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="false" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="false" type="numeric" default="1577" displayname="numeric PAYSCONSOTELID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="TELEPHONE" required="false" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="false" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="APP_LOGINID" required="false" type="numeric" default="" displayname="numeric APP_LOGINID" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="CODE_INTERNE" required="false" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="ID_SOCIETE_MERE" required="false" type="numeric" default="" displayname="numeric ID_SOCIETE_MERE" hint="Initial value for the ID_SOCIETE_MEREproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.create_cde_contact_societe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Raison_sociale#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SIREN#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE1#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE2#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#COMMUNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ZIPCODE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#PAYSCONSOTELID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TELEPHONE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FAX#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#APP_LOGINID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#CODE_INTERNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_SOCIETE_MERE#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>
	
	<cffunction name="delete_cde_contact_societe" access="public" returntype="numeric" output="false" hint="Supprimer une fiche revendeur et ses contacts" >
		<cfargument name="Idcde_contact_societe" required="false" type="numeric" default="" displayname="numeric Idcde_contact_societe" hint="Initial value for the Idcde_contact_societeproperty." />
		<cfargument name="IDRACINE" required="false" type="numeric" default="" displayname="numeric IDRACINE" hint="Initial value for the IDRACINEproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.delete_cde_contact_societe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idcde_contact_societe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDRACINE#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="edit_cde_contact_societe" access="public" returntype="numeric" output="false" hint="Mise é jour des informations de société ou d'agence." >
		<cfargument name="Idcde_contact_societe" required="false" type="numeric" default="" displayname="numeric Idcde_contact_societe" hint="Initial value for the Idcde_contact_societeproperty." />
		<cfargument name="Raison_sociale" required="false" type="string" default="" displayname="string Raison_sociale" hint="Initial value for the Raison_socialeproperty." />
		<cfargument name="SIREN" required="false" type="string" default="" displayname="string SIREN" hint="Initial value for the SIRENproperty." />
		<cfargument name="ADRESSE1" required="false" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="false" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="false" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="false" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="false" type="numeric" default="1577" displayname="numeric PAYSCONSOTELID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="TELEPHONE" required="false" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="false" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="USER_CREATE" required="false" type="numeric" default="" displayname="numeric USER_CREATE" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="CODE_INTERNE" required="false" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="ID_SOCIETE_MERE" required="false" type="numeric" default="" displayname="numeric ID_SOCIETE_MERE" hint="Initial value for the ID_SOCIETE_MEREproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.edit_cde_contact_societe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idcde_contact_societe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Raison_sociale#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SIREN#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE1#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ADRESSE2#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#COMMUNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ZIPCODE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#PAYSCONSOTELID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TELEPHONE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#FAX#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#USER_CREATE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#CODE_INTERNE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ID_SOCIETE_MERE#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="get_cde_contact_societe" access="public" returntype="query" output="false" hint="Raméne les informations de société ou d'agence." >
		<cfargument name="IDCDE_CONTACT_SOCIETE" required="false" type="numeric" default="" displayname="numeric IDCDE_CONTACT_SOCIETE" hint="Initial value for the IDCDE_CONTACT_SOCIETEproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_cde_contact_societe">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT_SOCIETE#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="get_liste_agence" access="public" returntype="query" output="false" hint="Lister les agences d'une société." >
		<cfargument name="IDCDE_CONTACT_SOCIETE" required="false" type="numeric" default="" displayname="numeric IDCDE_CONTACT_SOCIETE" hint="Initial value for the IDCDE_CONTACT_SOCIETEproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="Initial value for the chaineproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_liste_agence">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IDCDE_CONTACT_SOCIETE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>