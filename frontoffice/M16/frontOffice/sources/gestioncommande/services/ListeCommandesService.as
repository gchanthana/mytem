package gestioncommande.services
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.containers.HBox;
	import mx.controls.TextArea;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;
	
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.Pool;
	import gestioncommande.entity.Zone;
	import gestioncommande.events.CommandeEvent;
	
	import session.SessionUserObject;

	public class ListeCommandesService extends EventDispatcher
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					GETTERS
			//--------------------------------------------------------------------------------------------//
		
		public var listeCommandes				:ArrayCollection;
		public var listeEtats					:ArrayCollection;
		public var listeGestionnaires			:ArrayCollection;
		public var listeOperateurs				:ArrayCollection;
		public var listeComptes					:ArrayCollection;
		public var listeSousComptes				:ArrayCollection;
		public var listeActions					:ArrayCollection;
		public var listeActionsProfil			:ArrayCollection;
		public var listePools					:ArrayCollection;
				
		public var numberRevendeurs				:int = 0;
		public var isUserProfil					:int = 0;
		
			//--------------------------------------------------------------------------------------------//
			//					VARIABLES DES CHAMPS PERSO
			//--------------------------------------------------------------------------------------------//
		
		private var _champs1XML					:XMLList;
		private var _champs2XML					:XMLList;
		private var _champs1Pers				:ArrayCollection;
		private var _champs2Pers				:ArrayCollection;
		
		public var hbxChamps1					:HBox = new HBox;
		public var hbxChamps2					:HBox = new HBox;
		
		public var LIBELLE_CHAMPS1				:String  = ResourceManager.getInstance().getString('M16', 'R_f_rence_client_1_');
		public var EXEMPLE_CHAMPS1				:String  = "";
		[Bindable]
		public var LIBELLE_CHAMPS2				:String  = ResourceManager.getInstance().getString('M16', 'R_f_rence_client_2_');
		public var EXEMPLE_CHAMPS2				:String  = "";
		[Bindable]
		public var ISACTIVE_CHAMPS2				:Boolean = false;
		
		private var champs1						:ArrayCollection;
		private var champs2						:ArrayCollection;

		private var _myIdPool:int = -1;
			//--------------------------------------------------------------------------------------------//
			//					STATICS
			//--------------------------------------------------------------------------------------------//
		
		private static var _source				:String = 'fr.consotel.consoview.M16.v2.ListeCommandesService';
		
		public static var SHADOW_CHAR			:String = "/*|*/";
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//	
		
		public function ListeCommandesService()
		{
		}
		
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					APPELS PROCEDURES
			//--------------------------------------------------------------------------------------------//
		
		
		
		public function getLoadInfosThread(idpool:int, idprofil:int):void
		{	
			_myIdPool = idpool;
			listeEtats			= new ArrayCollection();
			listeGestionnaires	= new ArrayCollection();
			listeOperateurs		= new ArrayCollection();
			listeComptes		= new ArrayCollection();
			listeSousComptes	= new ArrayCollection();
			listeActions		= new ArrayCollection();
			listeActionsProfil	= new ArrayCollection();
			listePools			= new ArrayCollection();
			
			numberRevendeurs 	= 0;
			isUserProfil 		= 0;
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_source,
																				'getLoadInfosThread',
																				getLoadInfosThreadResultHandler);			
			
			RemoteObjectUtil.callService(op,idpool, idprofil);
		}
		
		public function getFiltres(idpool:int, idprofil:int, segment:int, clef:String):void
		{			
			listeEtats			= new ArrayCollection();
			listeGestionnaires	= new ArrayCollection();
			listeOperateurs		= new ArrayCollection();
			listeComptes		= new ArrayCollection();
			listeSousComptes	= new ArrayCollection();
			listeActions		= new ArrayCollection();
			listeActionsProfil	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_source,
																				'getFiltres',
																				getFiltresResultHandler);
			
			RemoteObjectUtil.callService(op,idpool,
											idprofil,
											segment,
											clef);
		}
		
		public function getCommandes(idpool:int, idprofil:int, segment:int, clef:String):void
		{
			listeCommandes		= new ArrayCollection();

			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_source,
																				'getCommandes',
																				getCommandesResultHandler);
						
			RemoteObjectUtil.callService(op,idpool,
											idprofil,
											segment,
											clef);
		}
		
		public function getFiltersCommandeAndActions(idpool:int, idprofil:int):void
		{
			_myIdPool = idpool;
			listeEtats			= new ArrayCollection();
			listeGestionnaires	= new ArrayCollection();
			listeOperateurs		= new ArrayCollection();
			listeComptes		= new ArrayCollection();
			listeSousComptes	= new ArrayCollection();
			listeActions		= new ArrayCollection();
			listeActionsProfil	= new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_source,
																				'getFiltersCommandeAndActions',
																				getFiltersCommandeAndActionsResultHandler);
			
			RemoteObjectUtil.callService(op,idpool,
											idprofil);
		}
		
		public function getCommandesPaginate(idpool:int, idsegment:int, idstatut:int, idetat:int, idoperateur:int, idcompte:int, idsouscompte:int, idusercreate:int, startindex:int, nbrRecord:int, clef:String, idcritere:int, txtFilter:String, nb_mois_histo:int):void
		{
			listeCommandes = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				_source,
																				'fournirInventaireCommandePaginate',
																				getCommandesPaginateResultHandler);
			
			RemoteObjectUtil.callService(op,idsegment,
											clef,
											idpool,
											idstatut,
											idetat,
											idoperateur,
											idcompte,
											idsouscompte,
											idusercreate,
											startindex,
											nbrRecord,
											idcritere,
											txtFilter,
											nb_mois_histo);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATES
		//--------------------------------------------------------------------------------------------//
		
			//--------------------------------------------------------------------------------------------//
			//					RETOURS PROCEDURES
			//--------------------------------------------------------------------------------------------//

		private function getLoadInfosThreadResultHandler(re:ResultEvent):void 
		{
			if(re.result)	    	
			{							
				listeEtats			= addFirstItem(new ArrayCollection(re.result.FILTERSCMD.LISTEETATS.source),'LIBELLE_ETAT', 'IDINV_ETAT', false);
				listeGestionnaires	= addFirstItemGestionnaires(new ArrayCollection(re.result.FILTERSCMD.LISTEGESTIONNAIRES.source),'NOM', 'APP_LOGINID', false);
				listeOperateurs		= addFirstItem(new ArrayCollection(re.result.FILTERSCMD.LISTEOPERATEURS.source),'NOM', 'OPERATEURID', false);
				listeComptes		= addFirstItem(new ArrayCollection(re.result.FILTERSCMD.LISTECOMPTES.source),'COMPTE_FACTURATION', 'IDCOMPTE_FACTURATION', true);
				listeSousComptes	= addFirstItem(new ArrayCollection(re.result.FILTERSCMD.LISTESOUSCOMPTE.source),'SOUS_COMPTE', 'IDSOUS_COMPTE', true);
				
				listeActions 		= new ArrayCollection(re.result.FILTERSCMD.LISTEACTIONS.source);
				listeActionsProfil 	= addActionsProfil(new ArrayCollection(re.result.FILTERSCMD.LISTEACTIONSPROFIL.source));
				
				var tempPools	:ArrayCollection = new ArrayCollection(re.result.POOLS.source);
				var lenPools	:int = tempPools.length;
				
				for(var i:int = 0;i < lenPools;i++)
				{
					listePools.addItem(mappPool(tempPools[i]));
				}

				if((re.result.CHAMPS as ArrayCollection).length > 0)
					readXML(new XML(re.result.CHAMPS[0].XML));
				
				numberRevendeurs 	= re.result.NBRREVENDEURS;
				isUserProfil 		= re.result.IDUSERPROFIL;

				dispatchEvent(new CommandeEvent(CommandeEvent.INFOS_THREAD_CMD));														    		
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 'Consoview');
		}
		
		
		private function getFiltresResultHandler(re:ResultEvent):void 
		{
			if(re.result)	    	
			{							
				listeEtats			 = addFirstItem(new ArrayCollection(re.result.LISTEETATS.source), 'LIBELLE', 'IDETAT');
				listeGestionnaires	 = addFirstItemGestionnaires(new ArrayCollection(re.result.LISTEGESTIONNAIRES.source),'LIBELLE', 'USERID');
				listeOperateurs		 = addFirstItem(new ArrayCollection(re.result.LISTEOPERATEURS.source),'LIBELLE', 'IDOPERATEUR');
				listeComptes		 = addFirstItem(new ArrayCollection(re.result.LISTECOMPTES.source),'LIBELLE', 'IDCOMPTE_FACTURATION', true);
				listeSousComptes	 = addFirstItem(new ArrayCollection(re.result.LISTESOUSCOMPTE.source),'LIBELLE', 'IDSOUS_COMPTE', true);
				
				listeActions 		 = new ArrayCollection(re.result.LISTEACTIONS.source);
				listeActionsProfil 	 = addActionsProfil(new ArrayCollection(re.result.LISTEACTIONSPROFIL.source));
				
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_FILTRES));														    		
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 'Consoview');
		}
		
		private function getCommandesResultHandler(re:ResultEvent):void 
		{
			if(re.result)	    	
			{																			    			
				listeCommandes = mappDataListeCommandes(re.result as ArrayCollection);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDES_LISTE));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 'Consoview');
		}
		
		private function getFiltersCommandeAndActionsResultHandler(re:ResultEvent):void 
		{
			if(re.result)	    	
			{							
				listeEtats			 = addFirstItem(new ArrayCollection(re.result.LISTEETATS.source),'LIBELLE_ETAT', 'IDINV_ETAT', false);
				listeGestionnaires	 = addFirstItemGestionnaires(new ArrayCollection(re.result.LISTEGESTIONNAIRES.source),'NOM', 'APP_LOGINID', false);
				listeOperateurs		 = addFirstItem(new ArrayCollection(re.result.LISTEOPERATEURS.source),'NOM', 'OPERATEURID', false);
				listeComptes		 = addFirstItem(new ArrayCollection(re.result.LISTECOMPTES.source),'COMPTE_FACTURATION', 'IDCOMPTE_FACTURATION', true);
				listeSousComptes	 = addFirstItem(new ArrayCollection(re.result.LISTESOUSCOMPTE.source),'SOUS_COMPTE', 'IDSOUS_COMPTE', true);
				
				listeActions 		 = new ArrayCollection(re.result.LISTEACTIONS.source);
				listeActionsProfil 	 = addActionsProfil(new ArrayCollection(re.result.LISTEACTIONSPROFIL.source));
				
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_FILTRES));														    		
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 'Consoview');
		}
		
		private function getCommandesPaginateResultHandler(re:ResultEvent):void 
		{
			if(re.result)	    	
			{																			    			
				listeCommandes = mappDataListeCommandes(re.result as ArrayCollection);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDES_LISTE));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Pas_de_donn_e'), 'Consoview');
		}
		
		//--------------------------------------------------------------------------------------------//
		//					TRAITEMENTS RETOUR PROCEDURES
		//--------------------------------------------------------------------------------------------//
		private function addFirstItemGestionnaires(values:ArrayCollection, libelle:String, idlibelle:String, isDescending:Boolean = false):ArrayCollection
		{
			var myFistObject:Object = new Object();
			
			if(values != null && values.length > 0)
			{
				values = Formator.sortFunction(values, idlibelle, isDescending);
				
				//var firstElement	:Object = values[0];
				//var libFirstItem	:String = ResourceManager.getInstance().getString('M16', 'Tous');
				
				//if(idlibelle == 'APP_LOGINID')
				var	libFirstItem:String = ResourceManager.getInstance().getString('M16', 'Toutes_les_commandes');
				myFistObject[libelle] 	= libFirstItem;
				myFistObject[idlibelle] = -1;
				
				values.addItemAt(myFistObject, 0);
				values.refresh();
				
				if(idlibelle == 'APP_LOGINID')
				{
					var len	 :int = values.length;
					var gest :Object = null;
					
					for(var i:int = 0;i < len;i++)
					{
						if(values[i].APP_LOGINID == CvAccessManager.getUserObject().CLIENTACCESSID)
						{
							gest = new Object();
							gest = values[i];
							values.removeItemAt(i);
							break;
						}
					}
					
					if(gest != null)
					{
						gest[libelle] = ResourceManager.getInstance().getString('M16', 'Mes_Commandes');
						
						if(_myIdPool != -1)
							values.addItemAt(gest, 0);
						else
							values.addItemAt(gest, 1);
					}
				}
			}
			else
			{
				values = new ArrayCollection();
				
				myFistObject[libelle] 	= ResourceManager.getInstance().getString('M16', 'Toutes_les_commandes');
				myFistObject[idlibelle] = -1;
				
				values.addItem(myFistObject);
			}
			
			return values;
		}
		
		
		private function addFirstItem(values:ArrayCollection, libelle:String, idlibelle:String, isDescending:Boolean = false):ArrayCollection
		{
			var myFistObject:Object = new Object();
			
			if(values != null && values.length > 0)
			{
				values = Formator.sortFunction(values, idlibelle, isDescending);
				
				var firstElement	:Object = values[0];
				var libFirstItem	:String = ResourceManager.getInstance().getString('M16', 'Tous');
				
				if(idlibelle == 'APP_LOGINID')
					libFirstItem = ResourceManager.getInstance().getString('M16', 'Toutes_les_commandes');
				
				if(firstElement[idlibelle] > 0)
				{
					myFistObject[libelle] 	= libFirstItem;
					myFistObject[idlibelle] = -1;
				}
				else
				{
					firstElement[libelle]	= ResourceManager.getInstance().getString('M16', 'Aucun');
					
					myFistObject[libelle] 	= libFirstItem;
					myFistObject[idlibelle] = -1;
				}
				
				values = Formator.sortFunction(values, libelle);

				if(values.length > 1)
					values.addItemAt(myFistObject, 0);
				
				values.refresh();
				
				if(idlibelle == 'APP_LOGINID')
				{
					var len	 :int = values.length;
					var gest :Object = null;
					
					for(var i:int = 0;i < len;i++)
					{
						if(values[i].APP_LOGINID == CvAccessManager.getUserObject().CLIENTACCESSID)
						{
							gest = new Object();
							gest = values[i];
							
							values.removeItemAt(i);

							break;
						}
					}
					
					if(gest != null)
					{
						gest[libelle] = ResourceManager.getInstance().getString('M16', 'Mes_Commandes');
						
						values.refresh();
						
						if(values.length > 0)
							values.addItemAt(gest, 1);
						else
							values.addItemAt(gest, 0);
					}
				}
			}
			else
			{
				values = new ArrayCollection();
				
				myFistObject[libelle] 	= ResourceManager.getInstance().getString('M16', 'Aucun');
				myFistObject[idlibelle] = -1;
				
				values.addItem(myFistObject);
			}
			
			return values;
		}
		
		
		private function addActionsProfil(values:ArrayCollection):ArrayCollection
		{
			var rsltActionsProfil:ArrayCollection = new ArrayCollection();
			
			if(values)
			{
				var lenActions:int = values.length;
				
				for(var i:int = 0; i < lenActions;i++)
				{
					if(values[i].IDINV_ACTIONS == 2066 || values[i].IDINV_ACTIONS == 2216)
					{
						var actionProfile:Object = new Object();
						
						if(values[i].IDINV_ACTIONS == 2066)
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.MOBILE		= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(values[i].SELECTED);
						}
						else
						{
							actionProfile.IDSEGEMENT 	= 1;
							actionProfile.FIXE			= "SEGMENT";
							actionProfile.ACTIF			= Formator.formatInteger(values[i].SELECTED);
						}
						
						rsltActionsProfil.addItem(actionProfile);
					}
				}
			}
			
			return rsltActionsProfil;
		}
		
		private function mappDataListeCommandes(values:ICollectionView):ArrayCollection
		{
			var retour:ArrayCollection = new ArrayCollection();
			
			if(values != null)
			{
				var cursor:IViewCursor = values.createCursor();
				
				while(!cursor.afterLast)
				{
					var commandeObj:Commande = new Commande();
					
					mappDataCommande(commandeObj, cursor.current);
					
					retour.addItem(commandeObj);
					cursor.moveNext();
				}
			}
			
			return retour;
		}
		
			//--------------------------------------------------------------------------------------------//
			//					MAPPING
			//--------------------------------------------------------------------------------------------//
		
		private function mappPool(valueObject:Object):Pool
		{
			var poolObject:Pool 			= new Pool();
				poolObject.IDPOOL			= valueObject.IDPOOL;
				poolObject.IDPROFIL			= valueObject.IDPROFIL;
				poolObject.IDREVENDEUR		= valueObject.IDREVENDEUR;
				poolObject.LIBELLE_CONCAT	= StringUtil.trim(valueObject.LIBELLE_POOL) + " - " + StringUtil.trim(valueObject.LIBELLE_PROFIL);
				poolObject.LIBELLE_POOL		= valueObject.LIBELLE_POOL;
				poolObject.LIBELLE_PROFIL	= valueObject.LIBELLE_PROFIL; 
			
			return poolObject;
		}
		
		private function mappDataCommande(commande:Commande, value:Object):void
		{
			if(value.hasOwnProperty('IDPOOL_GESTIONNAIRE'))
				commande.IDPOOL_GESTIONNAIRE = value.IDPOOL_GESTIONNAIRE;
			else
				commande.IDPOOL_GESTIONNAIRE = value.IDPOOL; 
			
			if(value.hasOwnProperty('LIBELLE_POOLGESTIONNAIRE'))
				commande.LIBELLE_POOL = value.LIBELLE_POOLGESTIONNAIRE;
			else
				commande.LIBELLE_POOL = value.POOL; 
			
			
			if(value.hasOwnProperty('IDCOMMANDE'))
				commande.IDCOMMANDE = value.IDCOMMANDE;
			else
				commande.IDCOMMANDE	= commande.IDCOMMANDE; 
			
			if(value.hasOwnProperty('DATE_EFFET_PREVUE'))
				commande.LIVRAISON_PREVUE_LE = value.DATE_EFFET_PREVUE;
			else
				commande.LIVRAISON_PREVUE_LE = value.LIVRAISON_PREVUE_LE;
			
			if(value.hasOwnProperty('LIBELLE_COMPTE'))
				commande.LIBELLE_COMPTE = value.LIBELLE_COMPTE;
			else
				commande.LIBELLE_COMPTE = value.LIBELLE_COMPTE;
			
			if(value.hasOwnProperty('LIBELLE_SOUSCOMPTE'))
				commande.LIBELLE_SOUSCOMPTE = value.LIBELLE_SOUSCOMPTE;
			else
				commande.LIBELLE_SOUSCOMPTE = value.LIBELLE_SSCOMPTE;
			
			if(value.hasOwnProperty('IDCOMPTE_FACTURATION'))
				commande.IDCOMPTE_FACTURATION = Number(value.IDCOMPTE_FACTURATION);
			else
				commande.IDCOMPTE_FACTURATION = Number(value.IDCOMPTE);
			
			if(value.hasOwnProperty('IDSOUS_COMPTE'))
				commande.IDSOUS_COMPTE = Number(value.IDSOUS_COMPTE);
			else
				commande.IDSOUS_COMPTE = Number(value.IDSSCOMPTE);
			
			if(value.hasOwnProperty('OPERATEURID'))
				commande.IDOPERATEUR = Number(value.OPERATEURID);
			else
				commande.IDOPERATEUR = Number(value.IDOPERATEUR);
			
			commande.NBROWS					= int(value.ROWN);
			commande.NBRECORD				= int(value.NB_ROWS);
			
			commande.SEGMENT_MOBILE 		= int(value.SEGMENT_MOBILE);
			commande.SEGMENT_FIXE 			= int(value.SEGMENT_FIXE);
			commande.SEGMENT_DATA 			= int(value.SEGMENT_DATA);
			commande.IDLAST_ETAT 			= int(value.IDINV_ETAT);
			commande.IDLAST_ACTION 			= value.IDINV_ACTIONS;
			commande.LIBELLE_LASTACTION 	= value.LIBELLE_ACTION;
			commande.MONTANT_CDE 			= Number(value.MONTANT_CDE);
			commande.DEVISE					= value.DEVISE;
			commande.USERCREATE 			= value.USERCREATE;
			commande.USERID 				= Number(value.USERID);			
			commande.IS_CONCERN				= Number(value.IS_CONCERN);
			commande.NUMERO_COMMANDE 		= value.NUMERO_OPERATION;
			commande.LIBELLE_COMMANDE 		= value.LIBELLE;
			commande.REF_CLIENT1 			= value.REF_CLIENT;
			commande.REF_CLIENT2			= value.REF_CLIENT_2;
			commande.REF_CLIENT11			= value.REF_CLIENT;
			commande.REF_CLIENT21			= value.REF_CLIENT_2;
			commande.REF_OPERATEUR 			= value.REF_REVENDEUR;
			commande.COMMENTAIRES 			= value.COMMENTAIRES;
			commande.LIBELLE_OPERATEUR 		= value.LIBELLE_OPERATEUR;
			commande.NUMERO_TRACKING 		= value.NUMERO_TRACKING;
			commande.IDTRANSPORTEUR 		= value.IDTRANSPORTEUR;
			commande.IDSITELIVRAISON 		= value.IDSITE_LIVRAISON;
			commande.LIBELLE_SITELIVRAISON 	= value.LIBELLE_SITELIVRAISON;	
			commande.IDGESTIONNAIRE_MODIF 	= value.IDGESTIONNAIRE_MODIF;
			commande.IDGESTIONNAIRE_CREATE 	= value.IDGESTIONNAIRE_CREATE;
			commande.CREEE_PAR 				= value.LIBELLEGESTIONNAIRE_CREATE;
			commande.MODIFIEE_PAR			= value.LIBELLEGESTIONNAIRE_MODIF;
			commande.ENCOURS 				= Number(value.EN_COURS);
			commande.EXPEDIE_LE 			= value.EXPEDIE_LE; 
			commande.PATRONYME_CONTACT 		= value.NOM_CONTACT;
			commande.IDCONTACT 				= value.IDCDE_CONTACT;
			commande.LIVREE_LE 				= value.LIVREE_LE;
			commande.IDTYPE_COMMANDE 		= Number(value.IDTYPE_OPERATION);
			commande.LIBELLE_TO 			= value.A_L_ATTENTION;
			commande.LIBELLE_SITEFACTURATION= value.LIBELLE_SITE_FACTURATION;
			commande.IDSITEFACTURATION		= value.IDSITE_FACTURATION;
			commande.IDSOCIETE 				= value.IDSOCIETE;
			commande.LIBELLE_REVENDEUR		= value.LIBELLE_REVENDEUR;
			commande.IDREVENDEUR			= value.IDREVENDEUR;
			commande.IDPROFIL_EQUIPEMENT	= value.IDTYPECOMMANDE;
			commande.LIBELLE_PROFIL			= value.TYPECOMMANDE;
			commande.DATE_COMMANDE			= value.LIVRE_LE;
			commande.IDGESTIONNAIRE			= CvAccessManager.getUserObject().CLIENTACCESSID;
			commande.TYPE_OPERATION			= value.TYPE_OPERATION;
			commande.CREEE_LE				= value.DATE_CREATE ;
			commande.LIBELLE_LASTETAT		= value.LIBELLE_EAT;
			commande.MONTANT				= value.MONTANT;
			commande.MONTANT_TOTAL			= value.MONTANT_TOTAL;
			commande.LIVRAISON_DISTRIBUTEUR = int(value.LIVRAISON_DISTRIBUTEUR);
			commande.IS_LIV_DISTRIB			= Formator.formatInteger(commande.LIVRAISON_DISTRIBUTEUR);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					TRAITEMENT XML
		//--------------------------------------------------------------------------------------------//
		
		private function readXML(myChamps:XML):void
		{
			SessionUserObject.singletonSession.CHAMPS 			= new Object();
			SessionUserObject.singletonSession.CHAMPS.LIBELLE1 	= myChamps.champ1_libelle;
			SessionUserObject.singletonSession.CHAMPS.EXEMPLE1 	= myChamps.champ1_exemple;
			SessionUserObject.singletonSession.CHAMPS.ACTIVE1 	= convertIntToBool(myChamps.champ1_actif);
			SessionUserObject.singletonSession.CHAMPS.OBLIG1	= false;
			
			var auto1:Boolean = convertIntToBool(myChamps.champ1_auto);
			SessionUserObject.singletonSession.CHAMPS.AUTO1		= convertIntToBool(myChamps.champ1_auto);
			
			SessionUserObject.singletonSession.CHAMPS.LIBELLE2 	= myChamps.champ2_libelle;
			this.LIBELLE_CHAMPS2 =  myChamps.champ2_libelle;
			SessionUserObject.singletonSession.CHAMPS.EXEMPLE2 	= myChamps.champ2_exemple;
			SessionUserObject.singletonSession.CHAMPS.ACTIVE2	= convertIntToBool(myChamps.champ2_actif);
			this.ISACTIVE_CHAMPS2 = convertIntToBool(myChamps.champ2_actif);
			SessionUserObject.singletonSession.CHAMPS.OBLIG2	= false;
			
			var auto2:Boolean = convertIntToBool(myChamps.champ2_auto);
			SessionUserObject.singletonSession.CHAMPS.AUTO2		= convertIntToBool(myChamps.champ2_auto);
			
			_champs1XML = (myChamps.champs1[0] as XML).children();
			_champs2XML = (myChamps.champs2[0] as XML).children();
			
			attributXMLToDatagrid();
		}
		
		private function attributXMLToDatagrid():void
		{
			var i	:int  = 0;
			var zone:Zone = new Zone();		
			_champs1Pers  = new ArrayCollection();
			_champs2Pers  = new ArrayCollection();
			
			for(i = 0;i < _champs1XML.length();i++)
			{
				zone = new Zone();		
				zone.COMPTEUR_ZONE					= _champs1XML[i].zone;
				zone.STEPPEUR_ZONE					= _champs1XML[i].number_caracteres;
				zone.EXACT_NBR						= convertIntToBool(_champs1XML[i].exact_number);
				zone.COMPTEUR_OBLIG					= _champs1XML[i].number_caracteres_oblig;
				zone.TYPE_ZONE						= _champs1XML[i].caracteres;
				if(zone.TYPE_ZONE == SHADOW_CHAR)
					zone.TYPE_ZONE = " ";
				zone.IDTYPE_ZONE					= 1;
				zone.CONTENU_ZONE.ID_CARACTERE		= _champs1XML[i].idtype_caracteres;
				zone.CONTENU_ZONE.LIBELLE_CARATERE	= _champs1XML[i].type_caracteres;
				zone.SAISIE_ZONE					= convertIntToBool(_champs1XML[i].obligatoire);
				if(zone.SAISIE_ZONE)
					SessionUserObject.singletonSession.CHAMPS.OBLIG1	= true;
				_champs1Pers.addItem(zone);
			}
			
			for(i = 0;i < _champs2XML.length();i++)
			{
				zone = new Zone();		
				zone.COMPTEUR_ZONE					= _champs2XML[i].zone;
				zone.STEPPEUR_ZONE					= _champs2XML[i].number_caracteres;
				zone.EXACT_NBR						= convertIntToBool(_champs2XML[i].exact_number);
				zone.COMPTEUR_OBLIG					= _champs2XML[i].number_caracteres_oblig;
				zone.TYPE_ZONE						= _champs2XML[i].caracteres;
				if(zone.TYPE_ZONE == SHADOW_CHAR)
					zone.TYPE_ZONE = " ";
				zone.IDTYPE_ZONE					= 1;
				zone.CONTENU_ZONE.ID_CARACTERE		= _champs2XML[i].idtype_caracteres;
				zone.CONTENU_ZONE.LIBELLE_CARATERE	= _champs2XML[i].type_caracteres;
				zone.SAISIE_ZONE					= convertIntToBool(_champs2XML[i].obligatoire);
				if(zone.SAISIE_ZONE)
					SessionUserObject.singletonSession.CHAMPS.OBLIG2	= true;
				_champs2Pers.addItem(zone);
			}
			SessionUserObject.singletonSession.CHAMPS.CHAMPS1 = _champs1Pers;
			SessionUserObject.singletonSession.CHAMPS.CHAMPS2 = _champs2Pers;
			createDinamicTextinput();
		}
		
		public function convertIntToBool(isTrue:int):Boolean
		{
			var bool:Boolean = false;;
			
			if(isTrue == 1)
				bool = true;
			
			return bool;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					TRAITEMENT DE LA CREATION DE TEXTAREA
		//--------------------------------------------------------------------------------------------//
		
		private function createDinamicTextinput():void
		{
			champs1 = SessionUserObject.singletonSession.CHAMPS.CHAMPS1;
			champs2 = SessionUserObject.singletonSession.CHAMPS.CHAMPS2;
			
			var i	:int = 0;
			var cl	:TextArea = new TextArea;
			var lb	:TextArea = new TextArea;
			
			for(i = 0;i < champs1.length;i++)
			{
				cl 				= new TextArea;
				lb	 			= new TextArea;
				cl.width		= widthAdaptator(champs1[i].STEPPEUR_ZONE);
				cl.height		= 20;
				cl.restrict		= restrictChamps(champs1[i].CONTENU_ZONE.ID_CARACTERE)
				cl.maxChars		= champs1[i].STEPPEUR_ZONE;
				cl.id			= "champs1_" + i;
				cl.addEventListener(Event.CHANGE,textinputChangeHandler);
				hbxChamps1.addChild(cl);				
				lb.text			= champs1[i].TYPE_ZONE;
				lb.width		= widthAdaptator(lb.text.length);
				lb.height		= 20;
				lb.editable		= false;
				lb.selectable	= false;
				lb.setStyle("styleName","noBorderTextArea");
				hbxChamps1.addChild(lb);
			}
			
			for(i = 0;i < champs2.length;i++)
			{
				cl 				= new TextArea;
				lb	 			= new TextArea;
				cl.width		= widthAdaptator(champs2[i].STEPPEUR_ZONE);
				cl.height		= 20;
				cl.restrict		= restrictChamps(champs2[i].CONTENU_ZONE.ID_CARACTERE)
				cl.maxChars		= champs2[i].STEPPEUR_ZONE;
				cl.id			= "champs2_" + i;
				cl.addEventListener(Event.CHANGE,textinputChangeHandler);
				hbxChamps2.addChild(cl);
				lb.text			= champs2[i].TYPE_ZONE;
				lb.width		= widthAdaptator(lb.text.length);
				lb.height		= 20;
				lb.editable		= false;
				lb.selectable	= false;
				lb.setStyle("styleName","noBorderTextArea");
				hbxChamps2.addChild(lb);			
			}
			SessionUserObject.singletonSession.CHAMPS.hbxChamps1 = hbxChamps1;
			SessionUserObject.singletonSession.CHAMPS.hbxChamps2 = hbxChamps2;			
		}
		
		private function textinputChangeHandler(e:Event):void
		{
			(e.currentTarget as TextArea).errorString = "";
		}
		
		private function widthAdaptator(value:int):int
		{
			var cteSize		:int = 7;
			var widthNbr	:int = 0;
			var rslt		:int = 0;
			
			if(value == 1)
				widthNbr = 14;
			else
				widthNbr = (value * cteSize)+14;
			
			rslt = widthNbr;
			
			return rslt;
		}
		
		private function restrictChamps(value:int):String
		{
			var restrictedCar:String = "";
			switch(value)
			{
				case 0 :	restrictedCar = null;
					break;
				case 1 :	restrictedCar = "0123456789";
					break;
				case 2 :	restrictedCar = "a-zA-Z";
					break;
			}
			return restrictedCar;
		}
		
	}
}