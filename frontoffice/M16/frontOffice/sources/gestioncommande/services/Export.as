package gestioncommande.services
{
	import gestioncommande.entity.Commande;
	
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	public class Export
	{
		
		public function Export()
		{
		}
		
		public function exporterCommandeEnPDF(commande:Commande):void
	    {	
 			var url:String = moduleCommandeMobileIHM.urlBackoffice + "/fr/consotel/consoview/M16/export/ExportBDC.cfm";
            var variables:URLVariables = new URLVariables();
            
            variables.IDCOMMANDE 		= commande.IDCOMMANDE;
            variables.NUMERO_COMMANDE 	= commande.NUMERO_COMMANDE;
            variables.IDTYPE_COMMANDE 	= commande.IDTYPE_COMMANDE;
			variables.IDOPERATEUR 		= commande.IDOPERATEUR;
            
            var request:URLRequest = new URLRequest(url);
            request.data = variables;    
            request.method = "POST";
            navigateToURL(request,"_blank");
	    }
	    
	}
}