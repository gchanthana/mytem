<cfcomponent output="false">
	
	<cfset ID_RACINE = SESSION.PERIMETRE.ID_GROUPE>
	<cfset ID_USER = SESSION.USER.CLIENTACCESSID>
	
	<!---
		création d'un numéro d'intervention pour un ticket'
	 --->
	<cffunction name="createInterventionNumber" access="remote" output="false" returntype="String">
		<cfargument name="idticket" required="true" type="numeric" />
		 
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.generate_num_intervention">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idticket#" null="false">
			<cfprocparam  type="out" variable="p_num_intervention" cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam  type="out" variable="p_retour" cfsqltype="CF_SQL_INTEGER" >
		</cfstoredproc>
		<cfif p_retour gt -1>
			<cfreturn p_num_intervention>
		<cfelse>
			<cfreturn "-1">
		</cfif>	
	</cffunction>
	
	
	
	<!---
		création d'un numéro de ticket chez un revendeur
	 --->
	<cffunction name="createTicketNumber" access="remote" output="false" returntype="String">
		<cfargument name="idfournisseur" required="true" type="numeric" />
		
		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTicketNumber">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idfournisseur#" null="false">
			<cfprocparam  variable="return_code" cfsqltype="CF_SQL_VARCHAR" type="out">
		</cfstoredproc>
		
		<cfif return_code gt -1>
			<cfreturn return_code>
		<cfelse>
			<cfreturn "-1">
		</cfif>
	</cffunction>
	
	<!---
		liste des tickets ouvert ou fermé pour un équipement.
		On ne ramène pas les tickets marqués deleted.
	 --->
	<cffunction name="getTicketsEquipement" access="remote" output="false" returntype="Query">
		<cfargument name="idequipement" type="Numeric" default="0" required="true"/>
		<!--- TODO: pkg_cv_flotte.getTicketOfEquip	manque des params en sortie--->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTicketOfEquip">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idequipement#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
	
		 
		<cfreturn p_result/>
	</cffunction>
	
	
	<!---
		création d'un ticket		
	 --->
	<cffunction name="createTicketEquipement" access="public" output="false" returntype="array">
		<cfargument name="idequipement" type="Numeric" required="true" />
		<cfargument name="idrevendeur" type="Numeric" required="true" />
		<cfargument name="reference" type="String" required="true" />
		<cfargument name="probleme" type="String" required="true" />
		<cfargument name="isSav" type="Numeric" required="true" />
				
		<cfset result = arraynew(1)>
		
		<cfset numero = createTicketNumber(idrevendeur)>
				
		<cfif numero eq "-1">
			<cfset result[1] = -2>
			<cfset result[2] = "Erreur lors de la génération du numero">
			<cfreturn result>	
		</cfif>
		    
		<!--- TODO: tester proc pkg_cv_flotte.createTicket --->				
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.createTicket_V2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_RACINE#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idequipement#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numero#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#probleme#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_USER#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isSav#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfset result[1] = p_retour>
		<cfset result[2] = numero>
		
		<cfreturn result>
	</cffunction>
	
	<!--- 
		mise à jour d'un ticket
		si bool_close = 1 alors mettre à jour le date de cloture, l'user_close en +
	 --->
	<cffunction name="updateTicket" access="remote" output="false" returntype="numeric">
		<cfargument name="idticket" type="Numeric" required="true" />
		<cfargument name="idrevendeur" type="Numeric" required="false" />
		<cfargument name="reference" type="String" required="false" />
		<cfargument name="probleme" type="String" required="false" />
		<cfargument name="solution" type="String" required="true" />
		<cfargument name="bool_close" type="boolean" required="true"/>
		<cfargument name="isSav" type="Numeric" required="true" />
		
		<!--- TODO: Tester proc pkg_cv_flotte.updateTicket --->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.updateTicket_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idticket#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#probleme#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#solution#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#bool_close#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_USER#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#isSav#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	
	<!--- 
		suppresion d'un ticket, on marque le ticket et ses interventions comme deleted (BOOL_DELETED = 1)
	 --->
	<cffunction name="deleteTicket" access="remote" output="false" returntype="Numeric">
		<cfargument name="idticket" type="Numeric" required="true" />		
		
		<!--- TODO: tester proc pkg_cv_flotte.del_Ticket --->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.del_Ticket">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idticket#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>

		
	<!--- 
		liste des interventions d'un ticket'
	 --->
	<cffunction name="getInterventionsTicket" access="remote" output="false" returntype="Query">
		<cfargument name="idticket" type="Numeric" required="true" />
		
		<!--- TODO: tester proc getTicketInfo--->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTicketInfo">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idticket#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result/>
	</cffunction>
	
	<!--- 
		creation d'une intervention pour un ticket
	 --->
	<cffunction name="createIntervention" access="remote" output="false" returntype="array">
		<cfargument name="idticket" required="true" type="numeric" />
		<cfargument name="idtype" required="true" type="date"/>
		<cfargument name="libelle" required="true" type="String" />
		<cfargument name="reference" required="true" type="String" />
		<cfargument name="description" required="true" type="String" />
		<cfargument name="dateIntervention" required="true" type="date"/>		
		<cfargument name="periode" required="true" type="Numeric"/>
		<cfargument name="bool_cloturer" required="true" type="Numeric"/>
		<cfargument name="cout" required="true" type="numeric"/>
		
		<cfset result = arraynew(1)>
		<cfset numero = createInterventionNumber(idticket)>
		
		<cfif numero eq "-1">
			<cfset result[1] = -2>
			<cfset result[2] = "Erreur lors de la génération du numero">
			<cfreturn result>	
		</cfif>
		
		<!--- TODO: tester proc pkg_cv_flotte.addIntervention_v2--->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.addIntervention_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idticket#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_USER#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsdateformat(dateIntervention,'yyyy/mm/dd')#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#periode#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idtype#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#cout#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numero#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#description#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#bool_cloturer#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		
		
		
		
		<cfset result[1] = p_retour>
		<cfset result[2] = numero>
		
		<cfreturn result>
	</cffunction>

	<!--- 
		mise à jour d'une intervention pour un ticket
	 --->
	<cffunction name="udpateIntervention" access="remote" output="false" returntype="numeric">
		<cfargument name="idticket" required="true" type="numeric" />
		<cfargument name="idItervention" required="true" type="numeric" />
		<cfargument name="idtype" required="true" type="date"/>
		<cfargument name="libelle" required="true" type="String"/>		
		<cfargument name="reference" required="true" type="String"/>		
		<cfargument name="description" required="true" type="String" />
		<cfargument name="dateIntervention" required="true" type="date"/>		
		<cfargument name="periode" required="true" type="Numeric"/>
		<cfargument name="bool_cloturer" required="true" type="Numeric"/>
		<cfargument name="cout" required="true" type="numeric"/>
		
		<!--- TODO: TESTER Method pkg_cv_flotte.updateIntervention_v2 --->
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.updateIntervention_v2">
			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idticket#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idItervention#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#reference#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_USER#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#lsdateformat(dateIntervention,'yyyy/mm/dd')#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#periode#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idtype#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   value="#cout#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#description#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#val(bool_cloturer)#" null="false">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			
		</cfstoredproc>
		
		<cfreturn p_retour/>
	</cffunction>
	
	
	<cffunction name="deleteIntervention" access="remote" output="false" returntype="Numeric">
		<cfargument name="idIntervention" type="Numeric" required="true"/>
		
		<!--- TODO: Demander procédure à guillaume pkg_cv_flotte.delIntervention 
			mettre à jour l'user_delete et bool_deleted en +--->
			
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.del_Intervention">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idIntervention#" null="false">
			<!--- <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_USER#" null="false"> --->
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
				
		<cfreturn p_retour/>
	</cffunction>

	<cffunction name="getTypesIntervention" access="remote" output="false" returntype="Query">		
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_flotte.getTypeIntervention">
			<cfprocresult name="p_result">
		</cfstoredproc>		
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getListeRevendeurs" access="remote" output="false" returntype="Query">
		<cfargument name="segment" required="true" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
				
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_catalogue_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_RACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_USER#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
				
	</cffunction>
		
</cfcomponent>