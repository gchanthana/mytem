package service.diffusion.listevenement
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;

	public class ListeEvenementService
	{
		public var _model:ListeEvenementModel;
		public var handler:ListeEvenementHandler;	
		
		public function ListeEvenementService()
		{
			this._model  = new ListeEvenementModel();
			this.handler = new ListeEvenementHandler(model);
		}
		public function get model():ListeEvenementModel
		{
			return this._model;
		}	
		public function getListeEvenement(): void
		{
			var callOp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ListeEvenementService","getListeEvenement",handler.getListeEventHandler); 
			
			RemoteObjectUtil.callService(callOp);	
		}
	}
}