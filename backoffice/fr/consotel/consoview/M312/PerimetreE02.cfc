<cfcomponent output="false" extends="PerimetreE0">
	
	<cffunction name="executQueryByID" output="true" access="public" returntype="Query">
		<cfargument name="idTabBord" 				required="true"	type="Numeric" default="0">
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idsurtheme" 				required="true" type="String"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="dateDebutProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="dateFinProvenantDuTDB" 	required="true" type="String"  default="2009/01/01">
		<cfargument name="perimetreParm" 			required="true" type="String"  default="0">		
			
			<cfset idtdb 	 = idTabBord>
			<cfset queryrslt = "NULL">
			<cfset query 	 = "NULL">
			<!--- 
			le mot clé var devant après cfset signifie que la variable est locale à la fonction.
			Donc elle disparait lorsque l'on sort de la fonction
			 --->
			<cfset idDebut 	= zgetIdPeriodeMois(dateDebutProvenantDuTDB)>	
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_DEB = idDebut>
					
			<cfset idFin 	= zgetIdPeriodeMois(dateFinProvenantDuTDB)>
			<cfset SESSION.PERIMETRE.SELECTED_IDPERIOD_FIN = idFin>
						
			<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga = Val(rsltIdOrga)>
		    </cfif>
		    <cfset SESSION.PERIMETRE.SELECTED_ID_ORGA = idorga>
		    
			<cfset idCliche = zGetCliche(idorga)>
			<cfset SESSION.PERIMETRE.SELECTED_ID_CLICHE = idCliche>
			
			<cfif idCliche EQ "">
				<cfabort showerror="Aucun cliché">
			</cfif>
			
			<cfif perimetreParm EQ "SousTete">
				<cfset niveau 	= 'IDSOUS_TETE'>
			<cfelse>
				<cfset niveau 	= zgetNiveauPerimetre(p_idperimetre)>
			</cfif>
			
			<cfset idniveau 	= getIdNiveau(niveau)>			
			<cfset SESSION.PERIMETRE.SELECTED_IDNIVEAU = idniveau>
			<cfset SESSION.PERIMETRE.SELECTED_NIVEAU = niveau>
			
			<cfset rsltHandlerquery = getQueryByID(p_idproduit_catalogue, p_idtheme, p_idsurtheme, p_idracine_master, p_idracine, p_idperimetre, idDebut, idFin, idCliche, idorga)>

		<cfreturn rsltHandlerquery>
	</cffunction>
	
	<cffunction name="getQueryByID" output="true" access="private" returntype="Query">
		
		<cfargument name="p_idproduit_catalogue" 	required="true" type="Numeric" default="0">
		<cfargument name="p_idtheme" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idsurtheme" 			required="true" type="Numeric"  default="0">
		<cfargument name="p_idracine_master" 		required="true" type="Numeric" default="0">
		<cfargument name="p_idracine" 				required="true" type="Numeric" default="0">
		<cfargument name="p_idperimetre" 			required="true" type="Numeric" default="0">
		<cfargument name="idDebut" 					required="true" type="Numeric" default="0">
		<cfargument name="idFin" 					required="true" type="Numeric" default="0">
		<cfargument name="idCliche" 				required="true" type="Numeric" default="0">
		<cfargument name="idorga" 					required="true" type="Numeric" default="0">
		
			<cfset dataTable = "E0_AGGREGS">
			<cfswitch expression="#niveau#">
				<cfcase value="A,B,C,D,E,F" delimiters=",">
					<cfset dataTable = "E0_AGGREGS">
				</cfcase>
				<cfdefaultcase>
					<cfset dataTable = "E0_ENPROD_HIST">	
				</cfdefaultcase>
			</cfswitch>
						
			<cfswitch expression="#idtdb#">
				<cfcase value="0">
					<!--- ACCEUIL --->
					<cfquery datasource="#dataSourceName#" name="queryrslt0">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_idsurtheme				= '#arguments.p_idsurtheme#',										
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_idproduit_catalogue	= #arguments.p_idproduit_catalogue#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';

										
					 	 SELECT 
							 	PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
							 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
					 	 		ROUND(FACTURATION.VOLUME_MN,0) as duree_appel, 
					 	 		UNITE_VOLUME.UNITE_SOMMEE as unite,
								PRODUIT1GROUPE.IDTHEME_PRODUIT as idtheme_produit,
								DEVISE.TAUX_CONV as TAUX_CONV,
								FACTURATION.MONTANT_LOC as montant_final,
								PRODUIT1GROUPE.OPERATEUR_PRODUIT as nom, 
								FACTURATION.NOMBRE_APPEL as nombre_appel, 
								PRODUIT1GROUPE.ORDRE_AFFICHAGE as ordre_affichage, 
								FACTURATION.QTE as qte, 
								PRODUIT1GROUPE.SEGMENT_THEME as segment_theme, 
								PRODUIT1GROUPE.SUR_THEME as sur_theme, 
								PRODUIT1GROUPE.THEME as theme_libelle, 
								PRODUIT1GROUPE.TYPE_THEME as type_theme,
								PRODUIT1GROUPE.IDSUR_THEME as idsur_theme
							FROM #dataTable#
							WHERE PERIODE.IDPERIODE_MOIS BETWEEN  VALUEOF(NQ_SESSION."p_idperiode_mois_debut") 
							AND  VALUEOF(NQ_SESSION."p_idperiode_mois_fin") ORDER BY ordre_affichage
					</cfquery>
					<cfset query = queryrslt0>
				</cfcase>
				
				<cfcase value="1">
					<!--- THEME --->
					<cfquery datasource="#dataSourceName#" name="queryrslt1">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_idsurtheme				= '#arguments.p_idsurtheme#',
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_idproduit_catalogue	= #arguments.p_idproduit_catalogue#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
					 	 SELECT PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT, 
									PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CLIENT, 
									PRODUIT1GROUPE.LIBELLE_PRODUIT as LIBELLE_PRODUIT, 
									PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
								 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
									PRODUIT1GROUPE.THEME as THEME_LIBELLE, 
									PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
									PRODUIT1GROUPE.OPERATEUR_PRODUIT as NOM, 
									FACTURATION.QTE as QTE, 
									DEVISE.TAUX_CONV as TAUX_CONV,
									FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
									FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
									ROUND(FACTURATION.VOLUME_MN,0) as DUREE_APPEL,
									FACTURATION.UNITE_VOL as unite
									
							FROM #dataTable#
							WHERE (PERIODE.IDPERIODE_MOIS BETWEEN VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND  VALUEOF(NQ_SESSION."p_idperiode_mois_fin"))
									 AND (PRODUIT1GROUPE.IDTHEME_PRODUIT =  VALUEOF(NQ_SESSION."p_idtheme")) 
							ORDER BY MONTANT_FINAL DESC	
					</cfquery>
					<cfset query = queryrslt1>
				</cfcase>
				
				<cfcase value="2">
					<!--- SURTHEME --->
					<cfquery datasource="#dataSourceName#" name="queryrslt2">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_idsurtheme				= '#arguments.p_idsurtheme#',
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_idproduit_catalogue	= #arguments.p_idproduit_catalogue#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
					 	 SELECT PRODUIT1GROUPE.THEME as THEME_LIBELLE, 
									PRODUIT1GROUPE.IDSEGMENT_THEME as idsegment_theme,
								 	PRODUIT1GROUPE.IDTYPE_THEME as idtype_theme,
									PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
									PRODUIT1GROUPE.IDTHEME_PRODUIT as IDTHEME_PRODUIT, 
									FACTURATION.QTE as QTE, 
									DEVISE.TAUX_CONV as TAUX_CONV,
									FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
									FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
									ROUND(FACTURATION.VOLUME_MN,0) as DUREE_APPEL,
									FACTURATION.UNITE_VOL as unite
						FROM #dataTable#
						WHERE (PERIODE.IDPERIODE_MOIS BETWEEN VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) 
								AND (PRODUIT1GROUPE.IDSUR_THEME =  VALUEOF(NQ_SESSION."p_idsurtheme")) 
						ORDER BY MONTANT_FINAL DESC
					</cfquery>					
					<cfset query = queryrslt2>
				</cfcase>
				
				<cfcase value="3">
					<!--- PRODUIT --->
					<cfquery datasource="#dataSourceName#" name="queryrslt3">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_idsurtheme				= '#arguments.p_idsurtheme#',
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_idproduit_catalogue	= #arguments.p_idproduit_catalogue#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
							SELECT PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
								PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CATALOGUE, 
								0 prorata, 
								ORGA1CLIENT.IDSOUS_TETE as IDSOUS_TETE, 
								ORGA1CLIENT.SOUS_TETE as SOUS_TETE, 
								ORGAOPERATEUR.USAGE as COMMENTAIRES, 
								ORGAOPERATEUR.SITEID as SITEID, 
								ORGAOPERATEUR.CR_NOMSITE as NOM_SITE, 
								CASE ORGAOPERATEUR.CR_ADRESSE1 WHEN 'Flotte Mobile' THEN '' ELSE 
									ORGAOPERATEUR.CR_ADRESSE1 END as ADRESSE1, 
								ORGAOPERATEUR.CR_ADRESSE2 as ADRESSE2, 
								ORGAOPERATEUR.CR_ZIPCODE as ZIPCODE, 
								CASE ORGAOPERATEUR.CR_COMMUNE WHEN 'Communes' THEN '' ELSE
								ORGAOPERATEUR.CR_COMMUNE END as COMMUNE, 
								FACTURE.IDINVENTAIRE_PERIODE as IDINVENTAIRE_PERIODE,
								FACTURE.NUMERO_FACTURE as NUMERO_FACTURE, 
								FACTURATION.QTE as QTE, 
								DEVISE.TAUX_CONV as TAUX_CONV,
								FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
								FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
								ROUND(FACTURATION.VOLUME_MN, 0) as DUREE_APPEL,
								FACTURATION.UNITE_VOL as unite
							FROM E0_ENPROD_HIST
							WHERE (PRODUIT1GROUPE.IDPRODUIT_CATALOGUE = VALUEOF(NQ_SESSION."p_idproduit_catalogue")) 
							AND (PERIODE.IDPERIODE_MOIS BETWEEN  VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) 
							ORDER BY NOM_SITE, SITEID, MONTANT_FINAL DESC, ADRESSE1, SOUS_TETE
					</cfquery>					
					<cfset query = queryrslt3>
				</cfcase>
				
				<cfcase value="4">
					<!--- PRODUIT CAT --->
					<cfquery datasource="#dataSourceName#" name="queryrslt4">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_idsurtheme				= '#arguments.p_idsurtheme#',
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_idproduit_catalogue	= #arguments.p_idproduit_catalogue#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
						SELECT 	FACTURATION.QTE as QTE, 
							DEVISE.TAUX_CONV as TAUX_CONV,
							FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
							DEVISE.TAUX_CONV as TAUX_CONV,
							FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
							ROUND(FACTURATION.VOLUME_MN, 0) as DUREE_APPEL,
							FACTURATION.UNITE_VOL as unite,
							PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CLIENT,  
							PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CATALOGUE,
							0 as PRORATA, 
							ORGA1CLIENT.SOUS_TETE as SOUS_TETE, 
							ORGAOPERATEUR.SITEID as SITEID, 
							ORGA1CLIENT.IDSOUS_TETE as IDSOUS_TETE, 
							ORGAOPERATEUR.CR_NOMSITE as NOM_SITE, 
							CASE ORGAOPERATEUR.CR_ADRESSE1 WHEN 'Flotte Mobile' THEN '' ELSE 
								ORGAOPERATEUR.CR_ADRESSE1 END as ADRESSE1, 
							ORGAOPERATEUR.CR_ADRESSE2 as ADRESSE2,
							ORGAOPERATEUR.CR_ZIPCODE as ZIPCODE,
						  CASE ORGAOPERATEUR.CR_COMMUNE WHEN 'Communes' THEN '' ELSE 
							  	ORGAOPERATEUR.CR_COMMUNE END as COMMUNE, 	
						  FACTURE.NUMERO_FACTURE as NUMERO_FACTURE,
						  PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
						  FACTURE.IDINVENTAIRE_PERIODE as IDINVENTAIRE_PERIODE,
						  ORGAOPERATEUR.USAGE as COMMENTAIRES
						FROM E0_ENPROD_HIST
						WHERE (PRODUIT1GROUPE.IDPRODUIT_CATALOGUE = 
							VALUEOF(NQ_SESSION."p_idproduit_catalogue")) 
						AND (PERIODE.IDPERIODE_MOIS BETWEEN  
							VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND 
							VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) 
						ORDER BY NOM_SITE, SITEID, MONTANT_FINAL DESC, ADRESSE1, SOUS_TETE
					</cfquery>					
					<cfset query = queryrslt4>
				</cfcase>
				
				<cfcase value="5">
					<!--- PRODUIT MOB --->
					<cfquery datasource="#dataSourceName#" name="queryrslt5">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_idsurtheme				= '#arguments.p_idsurtheme#',
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_idproduit_catalogue	= #arguments.p_idproduit_catalogue#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
								SELECT PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
								PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CATALOGUE, 
								0 as PRORATA, 
								ORGA1CLIENT.IDSOUS_TETE as IDSOUS_TETE, 
								ORGA1CLIENT.SOUS_TETE as SOUS_TETE, 
								ORGAOPERATEUR.NOM || ' ' || ORGAOPERATEUR.PRENOM as COMMENTAIRES, 
								ORGAOPERATEUR.SITEID as SITEID, 
								ORGAOPERATEUR.CR_NOMSITE as NOM_SITE, 
								CASE ORGAOPERATEUR.CR_ADRESSE1 WHEN 'Flotte Mobile' THEN '' ELSE 
									ORGAOPERATEUR.CR_ADRESSE1 END ADRESSE1, 
								ORGAOPERATEUR.CR_ADRESSE2 as ADRESSE2, 
								ORGAOPERATEUR.CR_ZIPCODE as ZIPCODE, 
								CASE ORGAOPERATEUR.CR_COMMUNE WHEN 'Communes' THEN '' ELSE
								ORGAOPERATEUR.CR_COMMUNE END as COMMUNE, 
								FACTURE.IDINVENTAIRE_PERIODE as IDINVENTAIRE_PERIODE,
								FACTURE.NUMERO_FACTURE as NUMERO_FACTURE, 
								FACTURATION.QTE as QTE, 
								DEVISE.TAUX_CONV as TAUX_CONV,
								FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
								FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
								ROUND(FACTURATION.VOLUME_MN, 0) as DUREE_APPEL,
								FACTURATION.UNITE_VOL as unite
							FROM E0_ENPROD_HIST 
							WHERE (PRODUIT1GROUPE.IDPRODUIT_CATALOGUE = VALUEOF(NQ_SESSION."p_idproduit_catalogue")) 
							AND (PERIODE.IDPERIODE_MOIS BETWEEN VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) 
							ORDER BY NOM_SITE, SITEID, MONTANT_FINAL DESC, ADRESSE1, SOUS_TETE
					</cfquery>					
					<cfset query = queryrslt5>
				</cfcase>
				
				<cfcase value="6">
					<!--- PRODUIT CAT MOB --->
					<cfquery datasource="#dataSourceName#" name="queryrslt6">
						set variable 	p_idracine_master		= #arguments.p_idracine_master#,
										p_idracine				= #arguments.p_idracine#,
										p_idorga				= #idorga#,
										p_idperimetre			= #arguments.p_idperimetre#,
										p_idperiode_mois_debut	= #idDebut#,
										p_idperiode_mois_fin	= #idFin#,
										p_idcliche				= #idCliche#,
										p_idtheme				= #arguments.p_idtheme#,
										p_idsurtheme				= '#arguments.p_idsurtheme#',
										p_niveau				= '#niveau#',
										p_idniveau				= #idniveau#,
										p_idproduit_catalogue	= #arguments.p_idproduit_catalogue#,
										p_langue_pays='#SESSION.USER.GLOBALIZATION#';
										
								SELECT PRODUIT1GROUPE.TYPE_THEME as TYPE_THEME, 
								PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CLIENT,  
								PRODUIT1GROUPE.IDPRODUIT_CATALOGUE as IDPRODUIT_CATALOGUE, 
								0 as prorata, 
								ORGA1CLIENT.IDSOUS_TETE as IDSOUS_TETE, 
								ORGA1CLIENT.SOUS_TETE as SOUS_TETE, 
								ORGAOPERATEUR.NOM || ' ' || ORGAOPERATEUR.PRENOM as COMMENTAIRES, 
								ORGAOPERATEUR.SITEID as SITEID, 
								ORGAOPERATEUR.CR_NOMSITE as NOM_SITE, 
								CASE ORGAOPERATEUR.CR_ADRESSE1 WHEN 'Flotte Mobile' THEN '' ELSE 
									ORGAOPERATEUR.CR_ADRESSE1 END as ADRESSE1, 
								ORGAOPERATEUR.CR_ADRESSE2 as ADRESSE2, 
								ORGAOPERATEUR.CR_ZIPCODE as ZIPCODE, 
								CASE ORGAOPERATEUR.CR_COMMUNE WHEN 'Communes' THEN '' ELSE
								ORGAOPERATEUR.CR_COMMUNE END as COMMUNE, 
								FACTURE.IDINVENTAIRE_PERIODE as IDINVENTAIRE_PERIODE,
								FACTURE.NUMERO_FACTURE as NUMERO_FACTURE, 
								FACTURATION.QTE as QTE,
								DEVISE.TAUX_CONV as TAUX_CONV, 
								FACTURATION.MONTANT_LOC as MONTANT_FINAL, 
								FACTURATION.NOMBRE_APPEL as NOMBRE_APPEL, 
								ROUND(FACTURATION.VOLUME_MN, 0) as DUREE_APPEL,
								FACTURATION.UNITE_VOL as unite
							FROM E0_ENPROD_HIST
							WHERE (PRODUIT1GROUPE.IDPRODUIT_CATALOGUE = VALUEOF(NQ_SESSION."p_idproduit_catalogue")) 
							AND (PERIODE.IDPERIODE_MOIS BETWEEN VALUEOF(NQ_SESSION."p_idperiode_mois_debut") AND VALUEOF(NQ_SESSION."p_idperiode_mois_fin")) 
							ORDER BY NOM_SITE, SITEID, MONTANT_FINAL DESC, ADRESSE1, SOUS_TETE
					</cfquery>					
					<cfset query = queryrslt6>
				</cfcase>
				
			</cfswitch>	
		<cfreturn query>
	</cffunction>
	
</cfcomponent>
