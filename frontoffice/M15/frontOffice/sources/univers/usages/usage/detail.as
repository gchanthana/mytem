package univers.usages.usage
{
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import flash.events.Event;
	import composants.util.ConsoviewFormatter;
	import mx.utils.ObjectUtil;
	
	public class detail extends detailIHM
	{
		public function detail()
		{
			super();this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		public function initIHM(e:Event):void {
			// Grid Liste des appels
			grdAppels.columns[0].labelFunction=phoneFormat;
			grdAppels.columns[1].labelFunction=numberFormat;
			grdAppels.columns[2].labelFunction=numberFormat;
			grdAppels.columns[3].labelFunction=monnaieFormat;
			// Grid detail des appels
			grdDetailAppels.columns[2].labelFunction=numberFormat;
			grdDetailAppels.columns[3].labelFunction=monnaieFormat;
			//
			filtreListe.addEventListener(Event.CHANGE,filtreGrdAppel);
			filtreDetail.addEventListener(Event.CHANGE,filtreGrdDetail);
		}
		
		// Mise à jour du datagrid de la liste des appels par destination
		public function updateGridAppels(r:Object):void {
			grdAppels.dataProvider=r;
		}
		
		// Mise à jour du datagrid de la liste des appels
		public function updateGridDetailAppels(r:Object):void {
			grdDetailAppels.dataProvider=r;
		}
		
		//efface le grid
		public function clearGrid():void {
			grdAppels.dataProvider=null;
			grdDetailAppels.dataProvider=null;
		}
		
		//efface le grid
		public function clearDetailGrid():void {
			grdDetailAppels.dataProvider=null;
		}
		
		//formate le numéro de téléphone dans les datagrid
		private function phoneFormat(item:Object,column:DataGridColumn):String {
		    if (item[column.dataField].length == 10) {
		    	return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
		    } else {
		    	return item[column.dataField];	
		    }
		}
		
		private function numberFormat(item:Object,column:DataGridColumn):String
		{
	    	return ConsoviewFormatter.formatNumber(item[column.dataField],0);
		}
		
		private function monnaieFormat(item:Object,column:DataGridColumn):String
		{
	    	return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);
		}
		
		//filtre les elemenents du tableau en fonction du filtre
		public function filtreGrdAppel(ev : Event): void {
			grdAppels.dataProvider.filterFunction = filterFunc;
			grdAppels.dataProvider.refresh();
		}	
		
		//filtre les elemenents du tableau en fonction du filtre
		 public function filterFunc(item:Object):Boolean {	
			if 	(item.DISPLAY_APPELE.toLowerCase().search(filtreListe.text.toLowerCase())!= -1)
			{
				return (true);
			} else {
				return (false);
			}
		 }
		 
		 //filtre les elemenents du tableau en fonction du filtre
		public function filtreGrdDetail(ev : Event): void {
			grdDetailAppels.dataProvider.filterFunction = filterFuncDetail;
			grdDetailAppels.dataProvider.refresh();
		}	
		
		//filtre les elemenents du tableau en fonction du filtre
		 public function filterFuncDetail(item:Object):Boolean {	
			if 	(item.DATEDEB.toLowerCase().search(filtreDetail.text.toLowerCase())!= -1 || 
				item.HEUREDEB.toLowerCase().search(filtreDetail.text.toLowerCase())!= -1)
			{
				return (true);
			} else {
				return (false);
			}
		 }
	}
}