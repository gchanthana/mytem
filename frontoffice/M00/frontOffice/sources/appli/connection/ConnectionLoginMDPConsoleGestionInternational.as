package appli.connection
{
	import composants.ConsoProgressBar.ConsoProgressBar;
	
	import entity.AppParams;
	
	import general.abstract.AbstractConnection;
	
	import interfaceClass.IConnection;
	
	import mx.core.Container;
	
	public class ConnectionLoginMDPConsoleGestionInternational extends AbstractConnection implements IConnection
	{
		public function ConnectionLoginMDPConsoleGestionInternational(progress:ConsoProgressBar)
		{
			super(progress);
		}
		override public function goLogin(param:AppParams, container:Container = null):void
		{
			trace("ConnectionCVclassique.login()"); 
			
			super.goLogin(param,container);
			
			loadModule("login");
		}
		override public function validateLogin(objLogin:Object):void
		{
			super.validateLogin(objLogin);
			loginService.login(params.login,params.mdp,params.codeApp,params.codeConnection,params.codeLangue,false);
		}
		/**
		 * @private ConnectionCVClassique.updateSession() 
		 */
		override protected function updateSession(obj:Object):Boolean
		{
			try
			{
				if(CvAccessManager.singletonInstance != null && obj != null)
				{
					if(obj.hasOwnProperty("user"))
						CvAccessManager.singletonInstance.updateUser(obj.user);
					
					if(obj.hasOwnProperty("cg"))
					{
						CvAccessManager.getSession().INFOS_DIVERS.CG = new Object();
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER = new Object();
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDPROFIL = obj.cg.CG_IDPROFIL;
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.LABEL_PROFIL = obj.cg.CG_LABEL_PROFIL;
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDNIVEAU = obj.cg.CG_IDNIVEAU;
						CvAccessManager.getSession().INFOS_DIVERS.CG.USER.LABEL_NIVEAU = obj.cg.CG_LABEL_NIVEAU;
					}
					if(obj.hasOwnProperty("xml_access"))
						buildMenuXml(obj.xml_access as XML);
					if(obj.hasOwnProperty("XML_ACCESS"))
						buildMenuXml(obj.XML_ACCESS as XML);
					if(obj.hasOwnProperty("liste_racine"))
						CvAccessManager.singletonInstance.updateGroupList(obj.liste_racine);
					//					_singletonInstance.setGroupIndex(obj as int);
					//					_singletonInstance.setTypeLogin(obj as int);
					if(obj.hasOwnProperty("perimetre"))
					{
						CvAccessManager.singletonInstance.updatePerimetre(obj.perimetre);
						//						CvAccessManager.singletonInstance.updatePerimetreFromNode(obj.perimetre);
					}
					if(obj.hasOwnProperty("PERIMETRE"))
					{
						CvAccessManager.singletonInstance.updatePerimetre(obj.PERIMETRE);
						//						CvAccessManager.singletonInstance.updatePerimetreFromNode(obj.perimetre);
					}
					
					if(obj.hasOwnProperty('codeapplication'))
					{
						CvAccessManager.singletonInstance.updateCodeApplication(obj.codeapplication);
					}
					
				}
				else
					trace("_singletonInstance == NULL");
			}
			catch(error:Error)
			{
				return false;
			}
			return true;
		}
	}
}