<cfcomponent name="addFacture">

	<cffunction name="getOrganisation" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfset realid=id_groupe/>
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_global.get_organisations_plus">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#realid#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfquery name="qGetOpe" dbtype="query">
				SELECT *
				from p_result
				where type_orga='OPE'
			</cfquery>
			<cfreturn p_result>
	</cffunction>
	
	<cffunction name="getNode" returntype="xml" access="remote">
		<cfargument name="id_groupe" required="true" type="numeric" />
		<cfargument name="filterType" required="true" type="numeric" />
		<cfset sourceQuery = getNodePerimetre(id_groupe, filterType, "null")>
		<cfreturn sourceQuery>
	</cffunction>
	
	<cffunction name="getCF" returntype="query" access="remote">
			<cfargument name="idGroupe" required="true" type="numeric" />
			<cfargument name="operateurID" required="true" type="numeric" />
			
 			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_MANUEL_V3.getCF">
		        <cfprocparam value="#idGroupe#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#operateurID#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocresult name="qGetCF">
		    </cfstoredproc>
		    
			<cfreturn qGetCF>
	</cffunction>
	
	<!--- Ajoute le produit passé en paramètre aux favoris de l'utilisateur de session --->
	<cffunction name="ajouterProduitAMesFavoris" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idProduitCatalogue" type="numeric" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.ADDPRODFAVORI">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idRacine" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idGestinnaire" value="#idGestionnaire#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idProduit" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">		  
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>
	
	<!--- Supprime le produit passé en paramètre des  favoris de l'utilisateur de session --->
	<cffunction name="supprimerProduitDeMesFavoris" access="public" returntype="numeric" output="false" hint="" >
		<cfargument name="idProduitCatalogue" type="numeric" required="true">
		
		<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
		
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTIONNAIRE.DELPRODFAVORI">	
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idRacine" value="#idRacine#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idGestinnaire" value="#idGestionnaire#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idProduit" value="#idProduitCatalogue#">
			<cfprocparam  cfsqltype="CF_SQL_INTEGER" type="out" variable="result">
		</cfstoredproc>		 		
		<cfreturn result/>
	</cffunction>	
	
	<cffunction name="getListePerimetresDataset" access="remote" returntype="any">
			<cfargument name="idGroupe" type="numeric" required="true">
			<cfargument name="filterType" type="numeric" required="true">
			<cfargument name="date" type="string" required="true">
	        <cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_GLOBAL.Arbre_orga">
		        <cfprocparam value="#idGroupe#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#filterType#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#date#" cfsqltype="CF_SQL_VARCHAR">
		        <cfprocresult name="qGetListePerimetresDataset">
		    </cfstoredproc>
			<cfreturn qGetListePerimetresDataset>
		</cffunction>
		
	<cffunction name="getNodePerimetre" returntype="xml" access="remote">
			<cfargument name="idGroupe" type="numeric" required="true">
			<cfargument name="filterType" type="numeric" required="true">
			<cfargument name="date" type="string" required="true">
			
			<cfset sourceQuery = getListePerimetresDataset(idGroupe, filterType, date)>
	 
			
			<cfset dataStruct = structNew()>
			<cfset perimetreXmlDoc = XmlNew()>
			<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode = perimetreXmlDoc.node>
			<cfset dataStruct.ROOT_NODE = rootNode>
			<cfset rootNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][1]>

			<cfset rootNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][1]>
		
			<cfset rootNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][1]>
			<cfset rootNode.XmlAttributes.NIV = sourceQuery['NIV'][1]>
			

			<cfloop index="i" from="1" to="#sourceQuery.recordcount#">
				<cfset currentKey = sourceQuery['IDGROUPE_CLIENT'][i]>
				<cfset currentParentKey = sourceQuery['ID_GROUPE_MAITRE'][i]>
				<cfif sourceQuery['IDGROUPE_CLIENT'][i] EQ idGroupe>
					<cfset currentParentKey = "ROOT_NODE">
				</cfif>
				<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>
				<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
				<cfset tmpChildNode.XmlAttributes.LABEL = sourceQuery['LIBELLE_GROUPE_CLIENT'][i]>
				<cfset tmpChildNode.XmlAttributes.NIV = sourceQuery['NIV'][i]>
				<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = sourceQuery['BOOL_ORGA'][i]>

				<cfset tmpChildNode.XmlAttributes.VALUE = sourceQuery['IDGROUPE_CLIENT'][i]>

				<cfset structInsert(dataStruct,currentKey,tmpChildNode)>
			</cfloop>
			<cfreturn perimetreXmlDoc>
	</cffunction>
	
	<cffunction name="getTypePaiement" returntype="query" access="remote">
			<cfstoredproc datasource="#session.OffreDSN#" procedure="PKG_CV_MANUEL_V3.getTypePaiement">		        
		        <cfprocresult name="qGetTypePaiement">
		    </cfstoredproc>
			<cfreturn qGetTypePaiement>
	</cffunction>
	
	<cffunction name="getAllLines" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="operateurID" required="true" type="numeric" />
			<cfargument name="idcompte_facturation" required="true" type="numeric" />
			 
			
			 <cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_MANUEL_V3.GetLines">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#operateurID#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcompte_facturation#">
				<cfprocresult name="qGetLines">
			</cfstoredproc> 
			
			<cfreturn qGetLines>
	</cffunction>
	
	<cffunction name="getAllLinesSousCompte" returntype="query" access="remote">
			<cfargument name="idsous_compte_facturation" required="true" type="numeric" />
			 <cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_MANUEL_V3.GetCrLines">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idsous_compte_facturation#">
				<cfprocresult name="qGetLines">
			</cfstoredproc>
			<cfreturn qGetLines>
	</cffunction>
	
	<!--- Retourne la liste des produits  par regroupé par compte  facturation --->
	<cffunction name="getProduit" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="idcompte_facturation" required="true" type="numeric" />
			<cfargument name="id_operateur" required="true" type="numeric" />
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_MANUEL_V3.GetProduit">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_operateur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idcompte_facturation#">
				<cfprocresult name="qGetProduit">
			</cfstoredproc> 
			<cfreturn qGetProduit>
	</cffunction>
	
	<!--- Retourne la liste  de tous les produits d'un groupe d'un opérateur
	 param : l'id du groupe,l'id de l'opérateur , l'id du connecté'--->
	<cffunction name="getAllProduit" returntype="query" access="remote">			
			<cfargument name="idOperateur" required="true" type="numeric" />	
			
			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
		    <cfset idGestionnaire = Session.USER.CLIENTACCESSID>	
		    	
			<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_M32.GETPRODUIT">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOperateur#">		
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGestionnaire#">		
				<cfprocresult name="qGetProduit">
			</cfstoredproc> 
			<cfreturn qGetProduit>
	</cffunction>
	
	<cffunction name="createNewInvoice" returntype="string" access="remote">
		 	<cfargument name="noeudFacturation" required="true" type="string" />
		 	<cfargument name="numero_facture" required="true" type="string" />
		 	<cfargument name="datedeb" required="true" type="string" />
		 	<cfargument name="datefin" required="true" type="string" />
		 	<cfargument name="commentaires" required="true" type="string" />
		 	<cfargument name="echeance_paiement" required="true" type="string" />
		 	<cfargument name="date_emission" required="true" type="string" />
		 	<cfargument name="operateurid" required="true" type="string" />
		 	<cfargument name="type_paiement" required="true" type="string" />
		 	<cfargument name="itemsInvoice" required="true" type="array" />
		 	<!--- Transforme la date --->
		 	<cfset ddebut=LsDateFormat(createDate(getToken(datedeb,3,"/"),getToken(datedeb,2,"/"),getToken(datedeb,1,"/")),"yyyy/mm/dd")>
			<cfset dfin=LsDateFormat(createDate(getToken(datefin,3,"/"),getToken(datefin,2,"/"),getToken(datefin,1,"/")),"yyyy/mm/dd")>
			<cfset demission=LsDateFormat(createDate(getToken(date_emission,3,"/"),getToken(date_emission,2,"/"),getToken(date_emission,1,"/")),"yyyy/mm/dd")>
			<cfset decheance=LsDateFormat(createDate(getToken(echeance_paiement,3,"/"),getToken(echeance_paiement,2,"/"),getToken(echeance_paiement,1,"/")),"yyyy/mm/dd")>
			<!--- Vérifie le compte de facturation du noeuds --->
			<cfset userId = session.user.CLIENTACCESSID>
			<cfset idinventaire_periode=0>
			<cfset p_retour_final = -1>
			
			<cfset ls="">			
			
			<cfloop index="i" from="1" to="#ArrayLen(itemsInvoice)#">
				<cfset ls=ListAppend(ls,itemsInvoice[i]['IDSOUS_TETE'])>
			</cfloop>
						
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_manuel_V3.GetIdCF">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#noeudFacturation#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ls#" null="false">
				<cfprocresult name="qGetIdCF">
			</cfstoredproc>
			
				<cfif qGetIdCF.recordcount eq 1>
					
					<!--- Insertion de la facture --->
					<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_manuel_v3.add_facture">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#qGetIdCF.Idcompte_Facturation#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numero_facture#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ddebut#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dfin#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaires#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#decheance#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#demission#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#operateurid#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#type_paiement#" null="false">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#userId#" null="false">
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="idinventaire_periode">
					</cfstoredproc>
					
					
					<!---- Procédure d'insert de la facture---->
				 	<cfloop index="j" from="1" to="#ArrayLen(itemsInvoice)#">
				 		<cfif itemsInvoice[j]['PCT_REMISE'] eq 0>
							<cfset assietteRemise=0>
						<cfelse>
							<cfset assietteRemise=itemsInvoice[j]['PCT_REMISE']*(1-(itemsInvoice[j]['PCT_REMISE']/100))>
						</cfif>
						<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_manuel_v3.add_dfa">
							<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idinventaire_periode#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#qGetIdCF.idref_client#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#itemsInvoice[j]['IDPRODUIT_CLIENT']#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#itemsInvoice[j]['IDSOUS_TETE']#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#itemsInvoice[j]['IDST_OP']#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#int(itemsInvoice[j]['QTE'])#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#itemsInvoice[j]['TOTAL_REMISE']#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#ddebut#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#dfin#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#itemsInvoice[j]['PRIX_UNIT']#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#itemsInvoice[j]['PCT_REMISE']#" >
							<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#assietteRemise#">							
							<cfprocparam type="Out" cfsqltype="CF_SQL_NUMERIC" variable="p_retour">
						</cfstoredproc>
				 	</cfloop>
				 	
				 	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_manuel_v3.updt_mt_facture">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idinventaire_periode#">
						<cfprocparam type="Out" cfsqltype="CF_SQL_InTEGER" variable="p_retour_final">
					</cfstoredproc>
				 	
				</cfif>
		<cfreturn p_retour_final>
	</cffunction>
	
	<cffunction name="setDataInSession" access="private" returntype="void" description="Pour mettre en session le resultat de la recherche de la facture">
		<cfargument name="q" type="query">
		<cfif IsDefined('session.listeCF')>
			<cfset structDelete(session,"listeCF")>
		</cfif>
		<cfset session.addFacture=q>
	</cffunction>
</cfcomponent>