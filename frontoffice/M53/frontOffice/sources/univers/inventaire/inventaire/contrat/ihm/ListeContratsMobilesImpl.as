package univers.inventaire.contrat.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ICollectionView;
	import mx.containers.VBox;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Button;
	import mx.events.ListEvent;
	
	import univers.inventaire.contrat.system.ContratAbonnementMobile;
	
	[Event(name="btCreerContratClicked",type="flash.events.Event")]
	
	[Bindable]
	public class ListeContratsMobilesImpl extends VBox
	{
		public var dgListe:AdvancedDataGrid;
		public var btCreerContrat:Button;
		
		private var _selectedContrat:ContratAbonnementMobile;
		private var _dataProvider:ICollectionView;
		
		
		
		public function ListeContratsMobilesImpl()
		{
			super();
			
			
		}

		public function set selectedContrat(value:ContratAbonnementMobile):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():ContratAbonnementMobile
		{
			return _selectedContrat;
		}
		
		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			if(dgListe.selectedIndex != -1)
			{
				selectedContrat = dgListe.selectedItem as ContratAbonnementMobile;	
				selectedContrat.getDetailContrat();
			}
			else
			{
				selectedContrat = null;
			}
		}
		
		public function set dataProvider(value:ICollectionView):void
		{
			_dataProvider = value;
		}

		public function get dataProvider():ICollectionView
		{
			return _dataProvider;
		}

		protected function _btCreerContratClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new Event("btCreerContratClicked"));
		}
/* 
		public function set resultatRecherche(value:IResultatRecherche):void
		{
			_resultatRecherche = value;
		}

		public function get resultatRecherche():IResultatRecherche
		{
			return _resultatRecherche;
		}
 */
	}
}