package gestionparc.ihm.question.extention
{
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.services.associer.ActionAssocierServices;
	
	import mx.controls.DateField;
	import mx.events.CloseEvent;

	public class QuestionAssocierEqpToExt
	{
		import flash.display.Sprite;

		import gestionparc.ihm.fiches.listedispo.IListeDispo;

		import mx.controls.Alert;
		import mx.core.Application;
		import mx.resources.ResourceManager;

		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionAssocierServices=new ActionAssocierServices();

		public function QuestionAssocierEqpToExt(obj_dispo:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=obj_dispo;
				
			if (liste.getSelection().IDSIM != null && liste.getSelection().IDSIM > -1)
			{
				ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString("M111", "Etre_vous_s_r_de_vouloir_associer_cette_extention___un__quipement_avec_sim"), ResourceManager.getInstance().getString("M111", "Associer_extention___un__quipement"), alertClickHandler);
			}
			else
			{
				associerEqpToExt();
			}
		}

		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				associerEqpToExt1();
			}
		}
		
		private function associerEqpToExt1():void
		{
			serv.associerEqpWithSimToExt(setObj());
		}
		
		private function associerEqpToExt():void
		{
			serv.associerEqpToExt(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID; 
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.EX_NUMERO;
			obj.ITEM2=liste.getSelection().IMEI;
			obj.ID_TERMINAL=liste.getSelection().IDTERM;
			obj.ID_EXT=SpecificationVO.getInstance().elementDataGridSelected.IDEXTENSION;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}