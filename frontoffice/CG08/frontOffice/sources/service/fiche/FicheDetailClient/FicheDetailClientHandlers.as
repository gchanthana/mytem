package service.fiche.FicheDetailClient{	import composants.util.ConsoviewAlert;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	import utils.MessageAlerte;
	public class FicheDetailClientHandlers	{		private var myDatas : FicheDetailClientDatas;		public function FicheDetailClientHandlers(instanceOfDatas:FicheDetailClientDatas)		{			myDatas = instanceOfDatas;		}		internal function getDetailClientHandler(re:ResultEvent):void		{			if(re.result != null			&& re.result is ArrayCollection			&& (re.result as ArrayCollection).length > 0)				myDatas.treatProcessGetDetailClient(re.result as ArrayCollection);			else			{				if(re.result is int)				{					if(re.result == -1)						ConsoviewAlert.afficherError(MessageAlerte.createMessage(re.result.DATA.CODEERREUR),MessageAlerte.msgErrorPopupTitle);				}			}		}	}}