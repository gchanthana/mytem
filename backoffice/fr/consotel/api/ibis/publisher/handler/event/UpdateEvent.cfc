<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent" extends="fr.consotel.api.ibis.publisher.handler.event.DelegateEvent"
hint="Evènement contenant les infos liées à un traitement de mise à jour spécifique à la stratégie : fr.consotel.api.ibis.publisher.delivery.Evenement">
	<!--- fr.consotel.api.ibis.publisher.handler.event.UpdateEvent --->
	<cffunction access="public" name="getId" returntype="Numeric"
	hint="Retourne la valeur fournie par le reporting pour la propriété ID ou 0 si elle n'est pas définie">
		<cfreturn VAL(getDeliveryKeyValue("ID"))>
	</cffunction>
	
	<cffunction access="public" name="getPostProcess" returntype="String"
	hint="Retourne la valeur fournie par le reporting pour la propriété POST_PROCESS ou une chaine vide si elle n'est pas définie">
		<cfreturn getDeliveryKeyValue("POST_PROCESS")>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getDeliveryInfos" returntype="any">
		<cfset var deliveryInfos="">
		<cfset var deliveryContent=getDeliveryEvent().getDeliveryInfos()>
		<cfsavecontent variable="deliveryInfos">
			<cfoutput>
				#deliveryContent#<br><br>
				<b>Les propriétés de traitement de mise à jour de l'évènement sont :</b><br>
				ID : #getId()#<br>
				Post Process : #getPostProcess()#
			</cfoutput>
		</cfsavecontent>
		<cfreturn deliveryInfos>
	</cffunction>
</cfcomponent>