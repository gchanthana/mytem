package gestionparc.entity
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;

	[Bindable]
	public class SIMVO extends EquipementVO
	{
		public var PIN1:String="";
		public var PIN2:String="";
		public var PUK1:String="";
		public var PUK2:String="";

		public var byod:Number=0;
		
		public var operateur:OperateurVO=null;
		public var nomOperateur:String=""; // pour le dataField

		public var termRattache:TerminalVO=null;
		public var imei:String=""; // pour le dataField

		public var ligneRattache:LigneVO=null;
		public var numLigne:String=""; // pour le dataField
		public var typeLigne:String=""; // pour le dataField

		public var dureeContrat:int=0;
		public var dateOuverture:Date=null;
		public var dateRenouvellement:Date=null;
		public var dateResiliation:Date=null;
		public var dureeEligibilite:int=0;
		public var dateEligibilite:Date=null;
		public var dateFPC:Date=null;
		public var dateDerniereFacture:Date=null;

		public var listeAboOption:ArrayCollection=null;
		public var listeOrganisation:ArrayCollection=null; // celle liée au collab ou a la ligne
		public var listeIncident:ArrayCollection=null;

		public function fillSimVO(obj:Object):SIMVO
		{
			var mySimVO:SIMVO=new SIMVO();

			// Ajout des infos concernant la SIM
			mySimVO.idEquipement=obj[1][0].IDEQUIPEMENT;
			mySimVO.numSerieEquipement=obj[1][0].NUM_SIM;
			mySimVO.PIN1=obj[1][0].PIN1;
			mySimVO.PIN2=obj[1][0].PIN2;
			mySimVO.PUK1=obj[1][0].PUK1;
			mySimVO.PUK2=obj[1][0].PUK2;
			mySimVO.idSite=(obj[1][0].IDSITE_PHYSIQUE) ? obj[1][0].IDSITE_PHYSIQUE : -1;
			mySimVO.idEmplacement=(obj[1][0].IDEMPLACEMENT) ? obj[1][0].IDEMPLACEMENT : -1;
			mySimVO.byod=obj[1][0].BYOD;

			// Ajout des infos concernant le contrat de la SIM
			mySimVO.dureeContrat=obj[1][0].DUREE_CONTRAT;
			mySimVO.dateOuverture=obj[1][0].DATE_OUVERTURE;
			mySimVO.dateRenouvellement=obj[1][0].DATE_RENOUVELLEMENT;
			mySimVO.dateResiliation=obj[1][0].DATE_RESILIATION;
			mySimVO.dureeEligibilite=obj[1][0].DUREE_ELLIGIBILITE;
			mySimVO.dateEligibilite=obj[1][0].DATE_ELLIGIBILITE;
			mySimVO.dateFPC=obj[1][0].DATE_FPC;
			mySimVO.dateDerniereFacture=obj[1][0].ACCES_LAST_FACTURE;
			mySimVO.revendeur=new RevendeurVO();
			mySimVO.revendeur.nom=obj[1][0].FABRIQUANT;
			mySimVO.revendeur.idRevendeur=obj[1][0].IDFABRIQUANT;
			mySimVO.nomRevendeur=obj[1][0].FABRIQUANT;

			// Ajout des infos concernant la ligne rattachée à la SIM
			mySimVO.ligneRattache=new LigneVO();
			mySimVO.ligneRattache.idSousTete=obj[1][0].IDSOUS_TETE;
			mySimVO.ligneRattache.sousTete=obj[1][0].SOUS_TETE;
			mySimVO.ligneRattache.numContrat=obj[1][0].NUM_CONTRAT_ABO;
			mySimVO.ligneRattache.aboPrincipale=obj[1][0].ABO_PRINCIPAL;
			mySimVO.ligneRattache.typeLigne=obj[1][0].LIBELLE_TYPE_LIGNE;
			mySimVO.ligneRattache.idTypeLigne=obj[1][0].IDTYPE_LIGNE;
			mySimVO.ligneRattache.titulaire=obj[1][0].BOOL_TITULAIRE;
			mySimVO.ligneRattache.payeur=obj[1][0].BOOL_PAYEUR;
			mySimVO.ligneRattache.fonction=obj[1][0].FONCTION;
			mySimVO.ligneRattache.compte=obj[1][0].COMPTE_FACTURATION;
			mySimVO.ligneRattache.sousCompte=obj[1][0].SOUS_COMPTE;
			mySimVO.ligneRattache.idCompte=obj[1][0].IDCOMPTE_FACTURATION;
			mySimVO.ligneRattache.idSousCompte=obj[1][0].IDSOUS_COMPTE;
			mySimVO.numLigne=mySimVO.ligneRattache.sousTete;
			mySimVO.typeLigne=mySimVO.ligneRattache.typeLigne;

			// Ajout des infos concernant la liste des abonnements et options de la SIM
			mySimVO.listeAboOption=new ArrayCollection();
			var lenListeAboOption:int=(obj[2] as ArrayCollection).length;
			var aboOpt:Object;

			for (var index:int=0; index < lenListeAboOption; index++)
			{
				aboOpt=new Object();
				aboOpt.operateur=obj[2][index].OPERATEUR_NOM;
				aboOpt.libelleProduit=obj[2][index].LIBELLE_PRODUIT;
				aboOpt.idInventaireProduit=obj[2][index].IDINVENTAIRE_PRODUIT;
				aboOpt.dansInventaire=obj[2][index].BOOL_IN_INVENTAIRE;

				mySimVO.listeAboOption.addItem(aboOpt);
			}

			// Ajout des Incidents
			mySimVO.listeIncident=new ArrayCollection();
			var lenTabIncident:int=(obj[3] as ArrayCollection).length;
			var newIncident:IncidentVO;

			for (var j:int=0; j < lenTabIncident; j++)
			{
				newIncident=new IncidentVO();
				newIncident.idIncident=obj[3][j].ID_SAV;
				newIncident.refIncident=obj[3][j].REFERENCE_SAV;
				newIncident.numIncident=obj[3][j].NUMERO_TICKET;

				newIncident.userCreate=obj[3][j].USER_CREATE;
				newIncident.userModify=obj[3][j].USER_MODIF;
				newIncident.userClose=obj[3][j].USER_CLOSE;

				newIncident.totalCoutIntervention=obj[3][j].TOTAL_COUT_INTERVENTION;
				newIncident.nbIntervention=obj[3][j].NB_INTERVENTIONS;

				newIncident.solution=obj[3][j].SOLUTION_SAV;
				newIncident.description=obj[3][j].DESCRIPTION;

				newIncident.dateDebut=obj[3][j].DATE_DEBUT;
				newIncident.dateFin=obj[3][j].DATE_FIN;

				newIncident.revendeur=obj[1][0].LIBELLE_REVENDEUR; // pour le dataField

				newIncident.isClose=(newIncident.dateFin) ? 1 : 0;
				if (newIncident.isClose)
				{
					newIncident.etat=ResourceManager.getInstance().getString('M111', 'Ferm_');
				}
				else
				{
					newIncident.etat=ResourceManager.getInstance().getString('M111', 'Ouvert');
				}

				// liste des interventions
				newIncident.listeIntervention=new ArrayCollection();

				mySimVO.listeIncident.addItem(newIncident);
			}

			// Ajout des infos concernant les organisations et position
			mySimVO.listeOrganisation=new ArrayCollection();

			var lenListeOrga:int=(obj[4] as ArrayCollection).length;
			var newOrga:OrgaVO;

			for (var i:int=0; i < lenListeOrga; i++)
			{
				newOrga=new OrgaVO();
				newOrga.idOrga=obj[4][i].IDORGA;
				newOrga.libelleOrga=obj[4][i].LIBELLE_ORGA;
				newOrga.idCible=obj[4][i].IDCIBLE;
				newOrga.position=obj[4][i].POSITION;
				newOrga.chemin=obj[4][i].CHEMIN_CIBLE;
				newOrga.idRegleOrga=obj[4][i].IDREGLE;
				newOrga.dateModifPosition=obj[4][i].DATE_MODIF_POSITION;

				mySimVO.listeOrganisation.addItem(newOrga);
			}

			return mySimVO;
		}
	}
}
