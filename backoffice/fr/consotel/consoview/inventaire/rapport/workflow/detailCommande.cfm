<cfset etat="NONE">
<cfif form.type_commande eq "Commande">
	<cfif form.detail_produit eq 1>
		<cfset etat="/consoview/gestion/rapport/workflow/DetailCommandeParProduit/DetailCommandeParProduit.xdo">
	<cfelse>
		<cfset etat="/consoview/gestion/rapport/workflow/DetailCommande/DetailCommande.xdo">
	</cfif>
<cfelse>
	<cfif form.detail_produit eq 1>
		<cfset etat="/consoview/gestion/rapport/workflow/DetailResiliationParProduit/DetailResiliationParProduit.xdo">
	<cfelse>
		<cfset etat="/consoview/gestion/rapport/workflow/DetailResiliation/DetailResiliation.xdo">
	</cfif>
</cfif>
<!--- 
<cfset biServer=APPLICATION.BI_SERVICE_URL>
 --->
<cfset biServer=SESSION.BI_SERVICE_URL>

<cfset ArrayOfParamNameValues=ArrayNew(1)>
<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="P_IDRACINE">
<cfset t=ArrayNew(1)>
<cfset t[1]=SESSION.PERIMETRE.ID_GROUPE>
<cfset ParamNameValue.values=t>

<cfset ArrayOfParamNameValues[1]=ParamNameValue>
<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="P_CODE_ETAT">
<cfset t=ArrayNew(1)>
<cfset t[1]=FORM.CODE_ETAT>
<cfset ParamNameValue.values=t>

<cfset ArrayOfParamNameValues[2]=ParamNameValue>
<cfset ParamNameValue=structNew()>
<cfset ParamNameValue.multiValuesAllowed=FALSE>
<cfset ParamNameValue.name="P_EN_COURS">
<cfset t=ArrayNew(1)>
<cfset t[1]=FORM.EN_COURS>
<cfset ParamNameValue.values=t>
<cfset ArrayOfParamNameValues[3]=ParamNameValue>

<cfset myParamReportRequest=structNew()>
<cfset myParamReportRequest.reportAbsolutePath=etat>
<cfset myParamReportRequest.attributeTemplate=FORM.TEMPLATE>
<cfset myParamReportRequest.attributeLocale="fr-FR">
<cfset myParamReportRequest.attributeFormat=FORM.FORMAT>
<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
<cfset myParamReportParameters=structNew()>
<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
<cfset myParamReportParameters.userID="consoview">
<cfset myParamReportParameters.password="public">

<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
<cfset filename="#FORM.NOM_RAPPORT#_" & replace(session.perimetre.raison_sociale,' ','_',"all")>
<cfif UCase(FORM.FORMAT) EQ "EXCEL">
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#.xls" charset="ISO-8859-1">
<cfelse>
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#.csv" charset="ISO-8859-1">
</cfif>
<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">


<!--- ANCIENNE VERSION : SEMBLE NE PAS FONCTIONNER 
<cfset locale="fr-FR">
<cfif StructKeyExists(session,"perimetre")>
	<cfset o=CreateObject("component","fr.consotel.consoview.util.PublicReportService")>
	<cfset d=o.createReportRequest(etat,form.template,FORM.FORMAT,locale)>
	<cfset d=o.AddParameter("P_IDRACINE",session["perimetre"]["id_groupe"])>
	<cfset d=o.AddParameter("P_CODE_ETAT",form.code_etat)>
	<cfset d=o.AddParameter("P_EN_COURS",form.en_cours)>
	<cfset contenu=o.runReport()>
	<cfif UCase(FORM.FORMAT) EQ "EXCEL">
		<cfheader name="Content-Disposition" value="inline;filename=#form.libelle#_#LsDateFormat(Now(),"yyyymmdd")#.xls">
	<cfelse>
		<cfheader name="Content-Disposition" value="inline;filename=#form.libelle#_#LsDateFormat(Now(),"yyyymmdd")#.csv">
	</cfif>
	<cfcontent type="#contenu.getReportContentType()#" variable="#contenu.getReportBytes()#">
<cfelse>
Session expirée.
</cfif>
 --->
