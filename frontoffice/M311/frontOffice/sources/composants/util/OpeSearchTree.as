package composants.util
{
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.controls.Tree;
	import mx.collections.ArrayCollection;
	import mx.managers.CursorManager;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import composants.tb.recherche.renderer.TIRFolderLines;
	import mx.core.ClassFactory;
	import univers.CvAccessManager;
	import mx.utils.ObjectUtil;
	import mx.collections.XMLListCollection;
	import mx.core.IFactory;
	import mx.rpc.events.ResultEvent;
	import composants.access.PerimetreTreeEvent;
	import mx.events.ListEvent;
	import composants.access.perimetre.PerimetreTreeItemRenderer;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	public class OpeSearchTree extends SearchTreeIHM
	{
		public static const NODE_CHANGED : String = "nodeChanged";
		
		public var nodeInfos:Object; // Infos sur le noeud séléctionné
		private var perimetreTreeItemRenderer:IFactory;
		private var currentSelectedIndex:int = 0; // Index du groupe
		private var currentXmlResultIndex:int = 0; // Index de la recherche
		private var searchResult:int = 0; // Nombre de résultat de la recherche de noeuds
		
		protected var xmlResult:XMLList = null;
		
		public function OpeSearchTree(){
			super();
			perimetreTreeItemRenderer = new ClassFactory(composants.access.perimetre.PerimetreTreeItemRenderer);
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		} 
				
		public function initIHM(e:FlexEvent):void {
			myTree.addEventListener(ListEvent.CHANGE,loadNodeInfos);
			myTree.addEventListener(PerimetreTreeEvent.DATA_PROVIDER_SET,onDataProviderReady);
			initDp(0);
		}
		
		private function loadNodeInfos(event:ListEvent):void {
			var clientID : Number = CvAccessManager.getSession().USER.CLIENTACCESSID;
			var loadNodeInfosOp:AbstractOperation =
				RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
										"fr.consotel.consoview.access.PerimetreManager",
										"getNodeInfos",processNodeInfos);
			if(myTree.searchTree.selectedItem == null)
				myTree.searchTree.selectNodeById(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
			var perimetreIndex:int = myTree.searchTree.selectedItem.@NID;
			
			if (perimetreIndex > 0){
				RemoteObjectUtil.callService(loadNodeInfosOp,clientID,perimetreIndex);
			} else{
				nodeInfos = null;
			}
		}
		
		private function processNodeInfos(event:ResultEvent):void {
			nodeInfos = event.result;
			dispatchEvent(new Event(NODE_CHANGED));
		}
				
		private function onDataProviderReady(event:PerimetreTreeEvent):void {
			if(myTree.searchTree.selectedItem == null)
				myTree.searchTree.selectNodeById(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
			loadNodeInfos(null);
		}
		public function initDp(idGroupe : int):void {
			if(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_LOGIQUE == ConsoViewPerimetreObject.TYPE_ROOT) {
				myTree.searchTree.dataProvider = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList
				myTree.searchTree.labelField = "LBL";
				//myTree.searchTree.dataDescriptor = CvAccessManager.getTreeDataDescriptor();
				
			} else {
				var xmlPerimetreData:XML = CvAccessManager.getSession().CURRENT_PERIMETRE.nodeList as XML;
				var resultList:XMLList =
					xmlPerimetreData.descendants().(@NID == CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
				if(resultList.length() > 0) {
					myTree.searchTree.dataProvider = resultList[0];
					myTree.searchTree.labelField = "LBL";
					//myTree.searchTree.dataDescriptor = CvAccessManager.getTreeDataDescriptor();
				}
			}
			myTree.searchTree.itemRenderer = perimetreTreeItemRenderer;
		}
		 
		
		public function setDp(xml:Object):void {
		}
		
		private function processKey(event:KeyboardEvent):void {
		}
		
		/**
		 * Filtre l'objet nodeItem (XML ou XMLList) en renvoyant tous les noeuds dont
		 * l'attribut attributeName contient le mot clé keyword dans sa valeur.
		 * */
		public function xmlSearch(nodeItem:Object,attributeName:String,keyword:String):XMLList {
			return null
		}
		
		/**
		 *  Recherche le ou les noeuds contenant le mot clé dans la zone de texte searchInput.
		 **/
		private function searchNode(event:Event):void {			
			
		}
		
		/**
		 * Séléctionne le résultat précédent dans la recherche.
		 * */
		private function previousSearchNode(event:Event):void {
		}

		/**
		 * Séléctionne le résultat suivant dans la recherche.
		 * */
		private function nextSearchNode(event:Event):void {
		}
		
		/**
		 * Déroule ou ferme suivant la valeur du paramètre open, tous les noeuds
		 * parents du noeud nodeItem (XML) passé en paramètre jusqu'à atteindre la racine.
		 * Puis renvoie le noeud passé en paramètre (nodeItem).
		 * */
		public function expandToNode(nodeItem:Object,open:Boolean):Object {
			return new Object();
		}
		
		public function closeTreeNodes(nodeItem:Object):Object {			
			return new Object()
		}
		
		/**
		 * Séléctionne le noeud (XML) passé en paramètre, puis positionne la
		 * scroll bar à l'index correspondant si le paramètre scrollTo vaut true.
		 * */
		public function selectGivenNode(nodeItem:Object,scrollTo:Boolean):void {
		}
		
		/**
		 * Efface la dernière recherche effectuée.
		 * */
		private function clearSearch(event:Event):void {
		}
		/**
		 * Active la recherche 
		 * */
		public function enabledSearch():void{		
			myTree.enabled = true;
			myTree.searchTree.doubleClickEnabled =true;
		}
		
		/**
		 * Desactive la recherche
		 * */
		public function disableSearch():void{
			myTree.enabled = false;			
			myTree.searchTree.doubleClickEnabled = false;
		}
	}
}