package catalogue.ihm.catalogueperso
{
	import mx.resources.ResourceManager;
	import catalogue.event.CatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.vo.ClientCatalogueVO;
	import catalogue.vo.OperateurVO;
	
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.managers.PopUpManager;
	import composants.util.ConsoviewAlert;
	
	[Bindable]
	public class CreateCatClientAbonnementImpl extends TitleWindow
	{
		public var globalCatalogueService : GlobalCatalogueService = new GlobalCatalogueService(); 
		public var pop : ListeModeleIHM;
		public var idDistributeur : int;
		public var boolShowMethodeCrea : Boolean;
		
		public var radioModele : RadioButton;
		public var radioStandard : RadioButton;
		public var radioEmulation: RadioButton;
		public var comboCli : ComboBox;
		public var comboOp : ComboBox;
		private var _idModele : int = -1;
		public var libelleModele : String = "";
		public var libelleOpSelected : String = "";
		public var libelleClient : String = "";
		
		public function CreateCatClientAbonnementImpl()
		{
			this.visible = false; // Attente de vérifier si le distributeur à au moins 1 client	
		}
		public function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		public function chargerHandler():void
		{
			pop = new ListeModeleIHM();
			pop.typeAbo = true;
			pop.idDistrib = idDistributeur;
			pop.addEventListener(CatalogueEvent.SELECTE_MODELE_COMPLETE,selectModeleCompleteHandler);
			PopUpManager.addPopUp(pop,this,true);	
			PopUpManager.centerPopUp(pop);	
		}
		public function radioStandardHandler():void
		{
			if(radioStandard.selected){
				_idModele = -1;
				libelleModele = "";
			}
		}
		public function radioEmulationHandler():void
		{
			if(radioEmulation.selected){
				_idModele = -1;
				libelleModele = "";
			}
		}
		public function comboHandler():void
		{
			if(comboCli.selectedItem && comboOp.selectedItem)
			{
				boolShowMethodeCrea = true;
				libelleClient = (comboCli.selectedItem as ClientCatalogueVO).LIBELLE_CLIENT;
				libelleOpSelected = (comboOp.selectedItem as OperateurVO).LIBELLE_OP;
			}
			else
			{
				boolShowMethodeCrea = false;
			}
		}
	
		
		private function selectModeleCompleteHandler(evt : CatalogueEvent):void
		{
			if(pop.idModeleSelected!=-1)
			{
				libelleModele = pop.libelleModeleSelected;
				_idModele = pop.idModeleSelected;
			}
			else
			{
				libelleModele = "";
				_idModele = -1;
				radioStandard.selected = true;
			}
			PopUpManager.removePopUp(pop);
		}
		public function creationCompleteHandler():void
		{
			globalCatalogueService.addEventListener(CatalogueEvent.LISTE_CLIENT_DISTRIBUTEUR,listeClientHandler);
			globalCatalogueService.getClientOfDistributeur(idDistributeur);
			globalCatalogueService.listOpDistrib(idDistributeur);
		}
		public function validerHandler():void
		{
			if(comboCli.selectedIndex != -1 &&  comboOp.selectedIndex != -1)
			{
				globalCatalogueService.addEventListener(CatalogueEvent.CREATE_CATALOGUE_CLIENT_COMPLETE,createCatalogueClientCompleteHandler);
			
				var idModel : int;
				var idMethode :int;
				
				if(radioStandard.selected)
				{
					idModel = -1;
					idMethode = 1;
				}
				if(radioEmulation.selected)
				{
					idModel = -1;
					idMethode =2;
				}
				if(radioModele.selected)
				{
					idModel = _idModele;
					idMethode =3;
				}
				globalCatalogueService.createCatalogueClientAbonnement((comboCli.selectedItem as ClientCatalogueVO).ID_CLIENT,(comboOp.selectedItem as OperateurVO).ID_OP,idMethode,idModel);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Un_client_et_un_op_rateur_doivent__tre_s'),ResourceManager.getInstance().getString('M24', 'Impossible_de_cr_er_le_catalogue'));
			}
		}
		private function createCatalogueClientCompleteHandler(evt : CatalogueEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M24', 'Catalogue_cr___avec_succ_s'));
			PopUpManager.removePopUp(this);
		}
		private function listeClientHandler(evt : CatalogueEvent):void
		{
			 if(!globalCatalogueService.listeClient.length>0)
			{
				Alert.show(ResourceManager.getInstance().getString('M24', 'Ce_distributeur_n_a_pas_de_client'),ResourceManager.getInstance().getString('M24', 'Impossible_de_cr_er_un_catalogue_personn'));
				PopUpManager.removePopUp(this);
			}
			else
			{
				this.visible = true;
			} 
		}
		
	}
}