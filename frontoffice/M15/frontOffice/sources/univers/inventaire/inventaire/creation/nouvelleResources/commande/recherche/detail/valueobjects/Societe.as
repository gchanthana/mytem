package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.valueobjects
{
	import mx.rpc.events.ResultEvent;
	import flash.events.EventDispatcher;
	import flash.events.Event;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.ArrayCollection;
	
	public class Societe extends EventDispatcher
	{
	
		public static const CREATION_COMPLETE : String = "societeOk";
		public static const UPDATE_COMPLETE : String = "societeUpdated";
		public static const SAVE_COMPLETE : String = "societeSaved";
		public static const LISTE_COMPLETE : String = "societeListOk";
		public static const DELETE_COMPLETE : String = "societeDeleted";
		
		public static const CREATION_ERROR : String = "societeCreationError";
		public static const UPDATE_ERROR : String ="societeUpdateError";  
		public static const SAVE_ERROR : String = "societeSavedError";
		public static const LISTE_ERROR : String = "societeListError";
		public static const DELETE_ERROR : String = "societeDeleteError";
		
		public static const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Societe";
  		public static const COLDFUSION_GET : String = "get";
  		public static const COLDFUSION_UPDATE : String = "update";
  		public static const COLDFUSION_CREATE : String = "create";
		public static const COLDFUSION_GETLISTE : String = "getList";
		public static const COLDFUSION_GETCONTACTLISTE : String = "getContactList";
		public static const COLDFUSION_DELETE : String = "delete";
		
		private var opGet : AbstractOperation;
		private var opCreate : AbstractOperation;
		private var opUpdate : AbstractOperation;		
		private var opList : AbstractOperation;
		private var opDelete : AbstractOperation;
		
		public function Societe(societeID : Number = 0){
			if (societeID != 0){
				_societeID = societeID;
				getSociete(_societeID);		
			}			
		}
		
		public function clean():void{						
			if (opGet != null) opGet.cancel();
			if (opCreate != null) opCreate.cancel();							
			if (opUpdate != null) opUpdate.cancel();
			if (opList != null) opList.cancel();
			if (opDelete != null) opDelete.cancel();
		}
		
		private var _societeID : Number;
		public function get societeID():Number{
			return _societeID;
		}
		public function set societeID(id : Number):void{
			_societeID = id;
		}
		
		private var _contactID : Number;
		public function get contactID():Number{
			return _contactID;
		}
		public function set contactID(id : Number):void{
			_contactID = id;
		}
			
		
		private var _raisonSociale : String;
		public function set raisonSociale( r : String):void{
			_raisonSociale = r;
		}
		public function get raisonSociale():String{
			return _raisonSociale; 
		}
		
		private var _societeParenteID : Number;
		public function set societeParenteID( sp : Number):void{
			_societeParenteID = sp;
		}
		public function get societeParenteID():Number{
			return _societeParenteID; 
		}
		
		private var _operateurID : Number;
		public function set operateurID( opid : Number):void{
			_operateurID = opid;
		}
		public function get operateurID():Number{
			return _operateurID; 
		}
		
		private var _operateurNom : String;
		public function set operateurNom(n : String):void{
			_operateurNom = n;
		}
		public function get operateurNom():String{
			return _operateurNom;
		}
		
		private var _siretSiren : String;
		public function set siretSiren( s : String):void{
			_siretSiren = s;
		}
		public function get siretSiren():String{
			return _siretSiren; 
		}
		
		private var _adresse1 : String;
		public function set adresse1( adr1 : String):void{
			_adresse1 = adr1;
		}
		public function get adresse1():String{
			return _adresse1; 
		}
		
		private var _adresse2: String;
		public function set adresse2( adr2 : String ):void{
			_adresse2 = adr2;
		}		
		public function get adresse2():String{
			return _adresse2;
		}
		
		private var _codePsotal : String;
		public function set codePsotal( cp : String):void{
			_codePsotal = cp;
		}
		public function get codePsotal():String{
			return _codePsotal; 
		}
		
		private var _commune : String;
		public function set commune( c : String):void{
			_commune = c;
		}
		public function get commune():String{
			return _commune; 
		}
		
		private var _pays : String;
		public function set pays( p : String):void{
			_pays = p;
		}
		public function get pays():String{
			return _pays; 
		}
		
		private var _telephone : String;
		public function set telephone(t : String):void{
			_telephone = t;
		}
		public function get telephone():String{
			return _telephone;
		}	
		
		private var _fax: String;
		public function set fax(f : String):void{
			_fax = f;
		}
		public function get fax():String{
			return _fax;
		}
		
		//---------------------------------------------------------/
		
		private function fill(value : Object):void{
			contactID = value.IDCDE_CONTACT;
			adresse1 = value.ADRESSE1;
			adresse2 = value.ADRESSE2;
			codePsotal = value.ZIPCODE;
			commune = value.COMMUNE;
			operateurID = value.OPERATEURID;
			pays = value.PAYS;
			raisonSociale = value.RAISON_SOCIALE;
			siretSiren = value.SIREN;
			societeParenteID = value.IDSOCIETE_MERE;
			telephone = value.TELEPHONE;
			operateurNom = value.OPENOM;
			fax = value.FAX;
		}
		
		///---------------- REMOTING ------------------------------/
		private function getSociete(id : Number):void{
			opGet = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_GET,
													getSocieteResultHandler,null);

			RemoteObjectUtil.callService(opGet,id);

		}
		
		private function getSocieteResultHandler(re : ResultEvent):void{
			if (re.result){							
				fill(re.result);				
				dispatchEvent(new Event(Societe.CREATION_COMPLETE));				
			}else{
				dispatchEvent(new Event(Societe.CREATION_ERROR));
			}			
		}
		//--------
		
		public function deleteSociete(id : Number):void{
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_DELETE,
													deleteSocieteResultHandler,null);

			RemoteObjectUtil.callService(opDelete,id);

		}
		
		private function deleteSocieteResultHandler(re : ResultEvent):void{
			if (re.result > 0){							
				dispatchEvent(new Event(Societe.DELETE_COMPLETE));				
			}else{
				dispatchEvent(new Event(Societe.DELETE_ERROR));
			}			
		}
		
		//--------			
		public function updateSociete():void{
			opUpdate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_UPDATE,
													updateSocieteResultHandler,null);

			RemoteObjectUtil.callService(opUpdate,this);						
		}	
		
		private function updateSocieteResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				dispatchEvent(new Event(Societe.UPDATE_COMPLETE));
			}else{
				dispatchEvent(new Event(Societe.UPDATE_ERROR));
			}
		}
		
		//----------
		public function sauvegarder(idGroupe : Number):void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_CREATE,
													sauvegarderResultHandler,null);

			RemoteObjectUtil.callService(opCreate,idGroupe,this);
					 											
		}		
		
		private function sauvegarderResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				societeID = Number(re.result);				
				dispatchEvent(new Event(Societe.SAVE_COMPLETE));
			}else{
				dispatchEvent(new Event(Societe.SAVE_ERROR));
			}			
		}		
//---------LISTE---------------------------------------------------------------------------
		private var _liste : ArrayCollection;
		private function setliste(values : ArrayCollection):void{
			var ligne : Object;
			_liste = new ArrayCollection();
			for (ligne in values){
				var st : Societe = new Societe();
				st.societeID = values[ligne].IDCDE_CONTACT_SOCIETE; 
				st.societeParenteID = values[ligne].IDSOCIETE_MERE;
				st.raisonSociale = values[ligne].RAISON_SOCIALE;			
				st.siretSiren = values[ligne].SIREN;				
				st.telephone = values[ligne].TELEPHONE;		
				st.fax = values[ligne].FAX;
				st.operateurID = values[ligne].OPERATEURID;				
				st.operateurNom = values[ligne].OPENOM;
				st.adresse1 = values[ligne].ADRESSE1;
				st.adresse2 = values[ligne].ADRESSE2;
				st.codePsotal = values[ligne].ZIPCODE;
				st.commune = values[ligne].COMMUNE;
				st.pays = values[ligne].PAYS;								
				_liste.addItem(st);			
			}			
		}
		
		public function get liste():ArrayCollection{
			return _liste;
		} 
		
		public function prepareList(idGpe : Number):void{
			if (opList != null) opList.cancel();
			
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_GETLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		public function  prepareContactList(contact_ID : Number):void{			
			if (opList != null) opList.cancel();			
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_GETCONTACTLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,contact_ID);
		}
		
		
		private function getListResultHandler(re : ResultEvent):void{
			try{
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(Societe.LISTE_COMPLETE));
			}catch(e : Error){
				dispatchEvent(new Event(Societe.LISTE_ERROR));
			}			
		}
	}
}