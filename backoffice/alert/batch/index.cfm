
	<style type="text/css">
		
		table {
		border: medium solid #6495ed;
		border-collapse: collapse;
		}
		
		th {
		font-family: monospace;
		border: thin solid #6495ed;
		padding: 5px;
		background-color: #D0E3FA;
		}
		
		td {
		font-family: verdana;
		font-size: 10px;
		border: thin solid #6495ed;
		padding: 5px;
		text-align: center;
		background-color: #ffffff;
		}
		
		caption {
		font-family: sans-serif;
		}
	
	</style>

	<cfset qCommandes = createobject("component", "ListQueryErrorMail").getData(5)>

	<table summary="liste des commandes en erreur.">
		
		<caption>
			
			<cfoutput>
				liste des commandes en erreur au #lsdateformat(now(),'DD/MM/YYYY')#
			</cfoutput>	
			
		</caption>

		<thead>
			<tr>     
				<th>Racine</th>
				<th>Type opération</th>
				<th>N°commande</th>
				<th>Date création</th>
				<th>Ref client</th>
				<th>Expéditeur</th>
				<th>Destinataires</th>
		    </tr>
		</thead>
	
		<tfoot>
		    <tr>      
				<th>Racine</th>
				<th>Type opération</th>
				<th>N°commande</th>
				<th>Date création</th>
				<th>Ref client</th>
				<th>Expéditeur</th>
				<th>Destinataires</th>
			</tr>
		</tfoot>

		<cfoutput query="qCommandes">	
  
			<tbody>
				<tr>         
					<td>#RACINE#</td>
					<td>#TYPE_OPERATION#</td> 
					<td>#NUMERO_OPERATION#</td>
					<td>#DATE_LOG#</td>
					<td>#REF_CLIENT#</td>
					<td>#MAIL_FROM#</td>
					<td>#MAIL_TO#</td>
				</tr>
			</tbody>
		  
		</cfoutput>	
		
	</table> 

	<cfform action="RunCmd.cfm" method="post">
		
		<cfinput type="Submit" name="SubmitForm" value="renvoyer">
		
	</cfform>