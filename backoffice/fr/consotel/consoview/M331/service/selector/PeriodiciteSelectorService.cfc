<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="getValuesPeriodicite" returntype="query"> 
		
		 <cfargument name="idRapportParametre" type="numeric" required="true">
		 <cfset idLang  = SESSION.USER.IDGLOBALIZATION>
		
		<!--- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
		<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.--->		 
        <cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_PeriodiciteSelector">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRapportParametre#" variable="p_idrapport_parametre">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#" variable="p_langid">
			<cfprocresult name="qPeriodicite">
		</cfstoredproc>		
          <cfreturn qPeriodicite><!--- le resultat de la procedure --->
     </cffunction>
	
	
</cfcomponent>