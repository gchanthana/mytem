package gestionparc.ihm.main.vue
{

	public interface IVue
	{
		function exportAll():void;
		function rechercher(indexDebut:int, nbItem:int):void;
	}
}
