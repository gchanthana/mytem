<cfcomponent output="false">

	<cffunction access="remote" output="false" name="getCorpsMailApp" returntype="query">
		<cfset codeApp  = SESSION.CODEAPPLICATION>
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.getCorpsMailApp">
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#codeApp#" variable="p_code_app">
			<cfprocresult name="qBodyMail">
		</cfstoredproc>				
        <cfreturn qBodyMail><!--- le resultat de la procedure --->
	</cffunction>

</cfcomponent>
