<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.ServiceHandlerFactory"
			implements="fr.consotel.api.ibis.publisher.handler.IServiceHandlerFactory" hint="Factory implementation for IServiceHandler">
	<cffunction access="public" name="getFactory" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandlerFactory" hint="Returns an initialized instance of IServiceHandlerFactory">
		<cfset VARIABLES["PROCESS_PACKAGE"]="fr.consotel.api.ibis.publisher.handler.process.">
		<cfset VARIABLES["IS_INITIALIZED"]=TRUE>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="createServiceHandler" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Returns an instance of IServiceHandler. May throw an exception">
		<cfargument name="eventHandler" type="string" required="true" hint="Event Handler used to instanciate coresponding IServiceHandler">
		<cfset var processClass="ServiceHandler" & ARGUMENTS["eventHandler"]>
		<cfif isInitialized() EQ TRUE>
			<cftry>
				<cfset processClass=VARIABLES["PROCESS_PACKAGE"] & processClass>
				<cfreturn createObject("component",processClass).createInstance()>
				<cfcatch type="any">
					<cftrace type="error" text="Unable to create process : #ARGUMENTS.eventHandler#">
					<cfthrow object="#CFCATCH#">
				</cfcatch>
			</cftry>

		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_STATE_EXCEPTION" message="This instance was not created using getFactory()">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="isInitialized" returntype="boolean" hint="Returns TRUE if this instance was initialized using createInstance and FALSE otherwise">
		<cfset var isInitKey="IS_INITIALIZED">
		<cfif structKeyExists(VARIABLES,isInitKey)>
			<cfreturn VARIABLES[isInitKey]>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
</cfcomponent>