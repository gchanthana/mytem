<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput></cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">															
								
								<th>&nbsp;Op&eacute;rateur&nbsp;</th>
									
								<th>&nbsp;Th&egrave;me&nbsp;</th>
									
								<th>&nbsp;Segment&nbsp;</th> 
									
								<th>&nbsp;Produit&nbsp;</th>
								
								<th>&nbsp;Entr&eacute;e inventaire&nbsp;</th>
								
								<th>&nbsp;Sortie inventaire&nbsp;</th>
								
								<th>&nbsp;Dans inventaire&nbsp;</th>
								
								<th>&nbsp;Sous r&eacute;f&eacute;rence&nbsp;</th>
								
								<th>&nbsp;Compte&nbsp;</th>
								
								<th>&nbsp;Sous compte&nbsp;</th>
								
								<th>&nbsp;Ligne&nbsp;</th>	
																							
								<th>&nbsp;Prix unitaire (&euro;H.T.)&nbsp;</th>														
																						
							</tr>
<cfset numRow = 1>
<cfset DANS_INV = "">
<cfoutput query="qGetGrid" group="IDINVENTAIRE_PRODUIT">
	<cfif DANS_INVENTAIRE eq 1><cfset DANS_INV = "OUI"><cfelse><cfset DANS_INV = "NON"></cfif>	
	<cfif (numRow mod 30) eq 0>
							<tr><td colspan="22" height="1" bgcolor="808080"></td></tr>	
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>

<cfdocumentitem type="pagebreak"/>
<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput></cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
	 					<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">															
								
								<th>&nbsp;Op&eacute;rateur&nbsp;</th>
									
								<th>&nbsp;Th&egrave;me&nbsp;</th>
									
								<th>&nbsp;Segment&nbsp;</th>
									
								<th>&nbsp;Produit&nbsp;</th>
								
								<th>&nbsp;Entr&eacute;e inventaire&nbsp;</th>
								
								<th>&nbsp;Sortie inventaire&nbsp;</th>
								
								<th>&nbsp;Dans inventaire&nbsp;</th>
								
								<th>&nbsp;Sous r&eacute;f&eacute;rence&nbsp;</th>
								
								<th>&nbsp;Compte&nbsp;</th>
								
								<th>&nbsp;Sous compte&nbsp;</th>
								
								<th>&nbsp;Ligne&nbsp;</th>	
																							
								<th>&nbsp;Prix unitaire (&euro;H.T.)&nbsp;</th>																
																						
							</tr>							
		<cfset numRow = numRow +1>
	<cfelse>
						<cfif (numRow mod 2) eq 0>
							<tr>
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#OPNOM#&nbsp;</td>								
																		
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#THEME_LIBELLE#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#SEGMENT_THEME#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#TRIM(LSDATEFORMAT(ENTREE_INVENTAIRE,"DD/MM/YYYY"))#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#TRIM(LSDATEFORMAT(SORTIE_INVENTAIRE,"DD/MM/YYYY"))#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#DANS_INV#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#CODE_INTERNE#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#COMPTE_FACTURATION#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#SOUS_COMPTE#&nbsp;</td>
								
								<td class="grid_normal_alt" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LsEuroCurrencyFormat(PRIX_UNIT, 'none')#&nbsp;</td>
							</tr>
						<cfelse>
							<tr>
								<td class="grid_normal" align="right" valign="top">&nbsp;#OPNOM#&nbsp;</td>								
																		
								<td class="grid_normal" align="right" valign="top">&nbsp;#THEME_LIBELLE#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#SEGMENT_THEME#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#LIBELLE_PRODUIT#&nbsp;</td>
																
								<td class="grid_normal" align="right" valign="top">&nbsp;#TRIM(LSDATEFORMAT(ENTREE_INVENTAIRE,"DD/MM/YYYY"))#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#TRIM(LSDATEFORMAT(SORTIE_INVENTAIRE,"DD/MM/YYYY"))#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#DANS_INV#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#CODE_INTERNE#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#COMPTE_FACTURATION#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#SOUS_COMPTE#&nbsp;</td>
								
								<td class="grid_normal" valign="top">&nbsp;#SOUS_TETE#&nbsp;</td>
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#LsEuroCurrencyFormat(PRIX_UNIT, 'none')#&nbsp;</td>								
																					
							</tr>
						</cfif>							 					
							
							
		<cfset numRow = numRow +1>		
	</cfif>
</cfoutput>
							 						
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>