package univers.facturation.suivi.controles.controls.formatsclients
{
	import mx.resources.ResourceManager;
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	
	import univers.facturation.suivi.controles.controls.AbstractControleFacture;
	import univers.facturation.suivi.controles.vues.ConfirmBox;
	
	public class LogControl
	{
		
//****VARIABLES GLOBALES****//

		public var dgControleSauver : DataGrid;
		[Bindable]
		private var ctrlFactures : AbstractControleFacture;
		private var confirmBox : ConfirmBox;
		public var btnwhatIsThisButton : Button;
//		private var UserProfileID : int;

//****END VARIABLES GLOBALES****//
		
//****FONCTIONS PUBLIC****//
		
		public function LogControl()
		{}

		//Permet de connaître sur quel bouton on a appuyer
		public function whatIsThisButton(btnNumber:Number,tailleOfButton:Number):String
		{
			var labelNameToSend:String = null;
			
			if(btnNumber == 0)
			{
				if(tailleOfButton == -1)
				{
					labelNameToSend = ResourceManager.getInstance().getString('M32', 'Viser_la_facture_papier');
				}
				else{
						labelNameToSend = ResourceManager.getInstance().getString('M32', 'Annuler_le_visa_papier');
					}
			}
			
			if(btnNumber == 1)		
			{
				if(tailleOfButton>0)
				{
					labelNameToSend = ResourceManager.getInstance().getString('M32', 'Viser_l_analytique');
				}
				else{
						labelNameToSend = ResourceManager.getInstance().getString('M32', 'Annuler_le_visa_analytique');
				}
			}
			
			if(btnNumber == 2)	
			{	
				if(tailleOfButton == -1)
				{
					labelNameToSend = ResourceManager.getInstance().getString('M32', 'Valider_le_contr_le');
				}
				else{
						labelNameToSend = ResourceManager.getInstance().getString('M32', 'Annuler_le_contr_le');
					}
			}
			
			if(btnNumber == 3)
			{		
				labelNameToSend = ResourceManager.getInstance().getString('M32', 'Editer_le_visa_analytique');
			}
			
			if(btnNumber == 4)
			{		
				if(tailleOfButton>0)
				{
					labelNameToSend = ResourceManager.getInstance().getString('M32', 'Exporter');
				}
				else{
						labelNameToSend = ResourceManager.getInstance().getString('M32', 'Ne_pas_exporter');
					}
			}
			
			if(btnNumber == 5)
			{		
				labelNameToSend = ResourceManager.getInstance().getString('M32', 'Editer_le_visa_analytique');
			}
			
			if(btnNumber == 6)
			{		
				labelNameToSend = ResourceManager.getInstance().getString('M32', 'D_cision_ERP');
			}
			
			return labelNameToSend;
		}	

//****END FONCTIONS PUBLIC****//
		
//****PROPRIETEES PUBLIC****//
		
		//Permet de récupérer 'ID_Profile' de la session en cours
		private var _LogControles_ProfileID : int;
		public function get LogControles_ProfileID(): int
		{
			return _LogControles_ProfileID;
		} 
		
		public function set LogControles_ProfileID(ProfileID : int):void
		{
			_LogControles_ProfileID = ProfileID;
		}
		
		//Permet de récupérer 'ID_Periode' de la facture en cours
		private var _LogControles_PeriodeID : int;
		public function get LogControles_PeriodeID(): int
		{
			return _LogControles_PeriodeID;
		} 
		
		public function set LogControles_PeriodeID(PeriodeID : int):void
		{
			_LogControles_PeriodeID = PeriodeID;
		}
		
		//Permet de récupérer les logs analytique vers la procédure(tableau contenant : le montant ana, non affeccté, pourcentage, ...)
		private var _logControlAna_ArrayListFacturesAnalytique:Array;
		public  function get logControlAna_ArrayListFacturesAnalytique():Array
		{
			return _logControlAna_ArrayListFacturesAnalytique;
		}
		
		public function set logControlAna_ArrayListFacturesAnalytique(ArrayListFacturesAnalytique:Array):void
		{
			_logControlAna_ArrayListFacturesAnalytique = ArrayListFacturesAnalytique;
		}
				
		//Permet de récupérer les logs analytique (récupéretion du tableau de la procédure)
		private var _ArrayQueryCtrlAna : ArrayCollection;
		public function get ArrayQueryCtrlAna(): ArrayCollection
		{
			return _ArrayQueryCtrlAna;
		} 
		
		public function set ArrayQueryCtrlAna(ArrayQuery : ArrayCollection):void
		{
			_ArrayQueryCtrlAna = ArrayQuery;
		}
		
		//Permet l'incrementation du tableau contenant les paramètres reçut par la procédure
		protected var _arrayNumber : Number = 0;
		public function set arrayNumber(an : Number):void
		{
			_arrayNumber = an;
		}
		
		public function get arrayNumber():Number{
			return _arrayNumber;
		}	
		
		//Permet de connaître l'état du bouton analytique 0->bouton 'Editer' invisible, 1->bouton 'Editer' visible 
		private var _EtatEditerVisaAnalytique : Number;
		public function get EtatEditerVisaAnalytique(): Number
		{
			return _EtatEditerVisaAnalytique;
		} 
		
		public function set EtatEditerVisaAnalytique(EditerOrNot : Number):void
		{
			_EtatEditerVisaAnalytique = EditerOrNot;
		}

//****END PROPRIETEES PUBLIC****//		
	
	}
}
