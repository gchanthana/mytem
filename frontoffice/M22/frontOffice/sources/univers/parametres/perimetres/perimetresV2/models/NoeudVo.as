package univers.parametres.perimetres.perimetresV2.models
{
	[RemoteClass(alias="fr.consotel.consoview.parametres.perimetres.gestionorganisation.NoeudVo")]

	[Bindable]
	public class NoeudVo
	{

		public var IDGROUPE_CLIENT:Number = 0;
		public var LIBELLE_GROUPE_CLIENT:String = "";
		public var COMMENTAIRES:String = "";
		public var ISPRIVATE:Number = 0;
		public var ID_GROUPE_MAITRE:Number = 0;
		public var IDDECOUPAGE:Number = 0;
		public var TYPE_ORGA:String = "";
		public var TYPE_DECOUPAGE:Number = 0;
		public var OPERATEURID:Number = 0;
		public var DATE_CREATION:Date = null;
		public var NB_ELEM:Number = 0;
		public var BOOL_ORGA:Number = 0;
		public var MARGE_REFACTURATION:Number = 0;
		public var MODE_AFFECTATION:Number = 0;
		public var IDCLIENTS_PV:Number = 0;
		public var DATE_MODIF:Date = null;
		public var HISTO:Number = 0;
		public var IDEMPLOYE:Number = 0;
		public var IDLEVEL_MODELE:Number = 0;
		public var IDORGA_NIVEAU:Number = 0;
		public var DATE_CREATION_STRING:String = "";


		public function NoeudVo()
		{
		}

	}
}