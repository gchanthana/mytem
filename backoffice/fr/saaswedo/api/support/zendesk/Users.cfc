<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.zendesk.Users" extends="fr.saaswedo.api.support.zendesk.Search" hint="Fonctionnalités de Gestion des utilisateurs Zendesk">
	<cffunction access="public" name="show" returntype="Struct" hint="Retourne les infos concernant l'utilisateur dont l'identifiant est spécifié en paramètre">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant utilisateur">
		<cfif ARGUMENTS["userId"] GTE 0>
			<cfset var jsonTarget="api/v2/users/#ARGUMENTS.userId#">
			<cfreturn sendJSONRequest(jsonTarget,GET_METHOD(),{},"")>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les valeurs du paramètre userId doit etre positif" detail="Méthode Users.show()">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="getUserId" returntype="Numeric" hint="Recherche un utilisateur et retourne son identifiant ou 0 s'il n'existe pas">
		<cfargument name="email" type="String" required="true" hint="Email utilisateur">
		<cfset var queryString="type:user email:" & ARGUMENTS["email"]>
		<cfset var requestResult=search(queryString,"","",1,1)>
		<cfset var queryResult=requestResult["RESULTS"]>
		<cfif arrayLen(queryResult) EQ 1>
			<cfreturn VAL(queryResult[1]["id"])>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="update" returntype="Struct" hint="Met à jour un utilisateur et retourne les infos correspondantes">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant utilisateur">
		<cfargument name="userInfos" type="Struct" required="true" hint="Infos utilisateur">
		<cfif ARGUMENTS["userId"] GTE 0>
			<cfset var jsonTarget="api/v2/users/#ARGUMENTS.userId#">
			<cfreturn sendJSONRequest(jsonTarget,PUT_METHOD(),ARGUMENTS["userInfos"],"")>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les valeurs du paramètre userId doit etre positif" detail="Méthode Users.delete()">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="delete" returntype="Struct" hint="Supprime l'utilisateur spécifié et retourne les infos le concernant">
		<cfargument name="userId" type="Numeric" required="true" hint="Identifiant utilisateur">
		<cfif ARGUMENTS["userId"] GTE 0>
			<!--- Bug - API v2 : HTTP 403 (Access Refused) 
			<cfset var jsonTarget="api/v2/users/#ARGUMENTS.userId#">
			--->
			<cfset var jsonTarget="api/v1/users/#ARGUMENTS.userId#">
			<cfreturn sendJSONRequest(jsonTarget,DELETE_METHOD(),{},"")>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les valeurs du paramètre userId doit etre positif" detail="Méthode Users.delete()">
		</cfif>
	</cffunction>
</cfcomponent>