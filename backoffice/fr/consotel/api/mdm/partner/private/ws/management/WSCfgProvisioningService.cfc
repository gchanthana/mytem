<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSCfgProvisioningService"
implements="fr.consotel.api.mdm.partner.private.ws.IWSCfgProvisioningService"
hint="Implémentation fournissant les méthodes du Service EveryWanCFG. Les méthodes de cette implémentation sont soumises à la vérification de l'authentification
<br><b>Pour des raisons de performance : Certaines méthodes lèvent une exception provenant du Web Service lorsqu'aucun équipement ne correspond aux infos fournies</b>">
	<cffunction access="public" name="addCfgFile" returntype="Void" hint="Ajoute un fichier de configuration avec son contenu">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var cfgProvisioninService=zdmRepository.getCfgProvisioningService()>
		<cftry>
			<cfset cfgProvisioninService.addCfgFile(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"],ARGUMENTS["fileContent"],ARGUMENTS["comment"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="updateCfgFile" returntype="Void" hint="Met à jour le contenu d'un fichier de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="oldFileName" type="String" required="true" hint="Nom actuel du fichier de configuration">
		<cfargument name="newFileName" type="String" required="true" hint="Nouveau nom à donner au fichier de configuration">
		<cfargument name="comment" type="String" required="true" hint="Commentaire à associer au fichier">
		<cfargument name="fileContent" type="Array" required="true" hint="Contenu du fichier en tant que tableau de bytes">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var cfgProvisioninService=zdmRepository.getCfgProvisioningService()>
		<cftry>
			<cfset cfgProvisioninService.updateCfgFile(
				ARGUMENTS["strOSFamily"],ARGUMENTS["oldFileName"],asJavaNullValue(ARGUMENTS["newFileName"]),ARGUMENTS["comment"],ARGUMENTS["fileContent"]
			)>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeCfgFile" returntype="Void" hint="Supprime un fichier de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var cfgProvisioninService=zdmRepository.getCfgProvisioningService()>
		<cftry>
			<cfset cfgProvisioninService.removeCfgFile(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getCfgFileInfo" returntype="Array" hint="Retourne les infos d'un fichier de configuration dans un tableau (vide si les infos ne sont pas définis)<br>
	Les éléments du tableau sont par ordre d'apparition : Nom du fichier, Taille, Commentaire associé, Date de création ou d'upload, Date de moficiation">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var cfgProvisioninService=zdmRepository.getCfgProvisioningService()>
		<cftry>
			<cfset var cfgInfos=cfgProvisioninService.getCfgFileInfo(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"])>
			<cfif isDefined("cfgInfos")>
				<cfreturn cfgInfos>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getCfgFileList" returntype="Array" hint="Retourne un tableau contenant la liste des fichiers de configuration">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var cfgProvisioninService=zdmRepository.getCfgProvisioningService()>
		<cftry>
			<cfset var cfgFileList=cfgProvisioninService.getCfgFileList(ARGUMENTS["strOSFamily"])>
			<cfif isDefined("cfgFileList")>
				<cfreturn cfgFileList>
			<cfelse>
				<cfreturn []>
			</cfif>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="validateCfgFile" returntype="Boolean" hint="Retourne TRUE si le contenu du fichier de configuration est valide et FALSE sinon<br>
	Une exception est levée par le service Zenprise et remontée par cette méthode lorsque la valeur retournée est false">
		<cfargument name="strOSFamily" type="String" required="true" hint="OS cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="fileName" type="String" required="true" hint="Nom du fichier">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var cfgProvisioninService=zdmRepository.getCfgProvisioningService()>
		<cftry>
			<cfreturn cfgProvisioninService.validateCfgFile(ARGUMENTS["strOSFamily"],ARGUMENTS["fileName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSCfgProvisioningService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
</cfcomponent>