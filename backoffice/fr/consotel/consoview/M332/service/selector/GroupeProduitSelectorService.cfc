<cfcomponent output="false">
	
	<cffunction access="public" output="false" name="getValuesGroupeProduit" returntype="query">
			
		     <cfset idLang  = SESSION.USER.IDGLOBALIZATION>
             
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_GrpProduitSelector">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idlang#" variable="p_langid">
				<cfprocresult name="qGroupeProduit">
			</cfstoredproc>				
            <cfreturn qGroupeProduit><!--- le resultat--->
      </cffunction> 
	
</cfcomponent>