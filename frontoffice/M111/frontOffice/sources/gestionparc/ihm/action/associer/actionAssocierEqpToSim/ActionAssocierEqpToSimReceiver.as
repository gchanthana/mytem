package gestionparc.ihm.action.associer.actionAssocierEqpToSim
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.sim.FicheAssocierEqpToSim;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierEqpToSimReceiver
	{
		private var fiche:FicheAssocierEqpToSim;

		public function ActionAssocierEqpToSimReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierEqpToSim();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
