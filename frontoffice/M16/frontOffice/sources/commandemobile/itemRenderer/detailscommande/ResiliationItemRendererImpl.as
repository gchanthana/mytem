package commandemobile.itemRenderer.detailscommande
{
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.services.Formator;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	
	[Bindable]
	public class ResiliationItemRendererImpl extends Box
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		public var listeResiliation	:ArrayCollection = new ArrayCollection();
		
		public var text_libelle		:String = '';
		
		public var headerColumn		:String = ResourceManager.getInstance().getString('M16','Ajout_R_siliation');
		
		public var isResiliation	:Boolean = true;
		
		public var nombreLignes		:int = 0;
		
		private var _myArticles		:XML = new XML();
		
		private var _cmd			:Commande;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//

		public function ResiliationItemRendererImpl()
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					EVENEMENT IHM
	//--------------------------------------------------------------------------------------------//		
	
		protected function creationCompleteHandler(fe:FlexEvent):void
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					PUBLIC
	//--------------------------------------------------------------------------------------------//
		
		public function setValueConfiguration(articles:XML, myCommande:Commande):void
		{
			_myArticles = articles;
			_cmd 		= myCommande;
			
			formatToConfigurationSelected();
			setLibelleByTypeOperation();
		}
		
		private function formatToConfigurationSelected():void
		{
			var listObject	:ArrayCollection = new ArrayCollection();
			var xmlarticles	:XMLList = _myArticles.children();
			var bool		:Boolean = false;
			var len			:int = xmlarticles.length();
			var id			:int = 0;
			var i			:int = 0;
			
			for(i = 0;i < len;i++)
			{
				var articles	:XML = xmlarticles[i];
				var myElements:ElementsCommande2 = new ElementsCommande2();
				
				getRessources(articles.ressources[0], myElements);
				getConfiguration(articles, myElements);					
				
				listObject.addItem(myElements);	
			}
			
			len = listObject.length;
			
			for(i = 0;i < len;i++)
			{
				getItems(listObject[i] as ElementsCommande2);
			}
			
			nombreLignes = listeResiliation.length;
		}
		
		private function setLibelleByTypeOperation():void
		{
			switch(_cmd.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.RESILIATION: 		isResiliation = true;  
															text_libelle = ResourceManager.getInstance().getString('M16','lignes_s_lectionn_es_pour_la_r_siliation');
															break;
				
				case TypesCommandesMobile.RESILIATION: 		isResiliation = true;  
															text_libelle = ResourceManager.getInstance().getString('M16','lignes_s_lectionn_es_pour_la_r_siliation');
															break;
								
				case TypesCommandesMobile.SUSPENSION: 		isResiliation = false;  
															text_libelle = ResourceManager.getInstance().getString('M16','lignes_s_lectionn_es_pour_la_suspension');
															break;
				
				
				case TypesCommandesMobile.SUSPENSION_FIXE: 	isResiliation = false;  
															text_libelle = ResourceManager.getInstance().getString('M16','lignes_s_lectionn_es_pour_la_suspension');
															break;
				
				
				case TypesCommandesMobile.REACTIVATION: 	isResiliation = false;  
															text_libelle = ResourceManager.getInstance().getString('M16','lignes_s_lectionn_es_pour_la_r_activatio');
															break;
				
				case TypesCommandesMobile.REACTIVATION_FIXE: 	isResiliation = false;  
																text_libelle = ResourceManager.getInstance().getString('M16','lignes_s_lectionn_es_pour_la_r_activatio');
																break;
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - FORMAT XML
		//--------------------------------------------------------------------------------------------//
		
		private function getRessources(articlesRe:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesRe[0].ressource.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML:XML = articlesRe[0].ressource[i];
				
				var reElts:RessourcesElements	= new RessourcesElements();	
					reElts.LIBELLETHEME 		= String(myXML[0].theme);
					reElts.LIBELLE 				= String(myXML[0].libelle);
					reElts.IDCATALOGUE 			= int(myXML[0].idproduit_catalogue);
					reElts.IDPRODUIT 			= int(myXML[0].idThemeProduit);
					reElts.BOOLACCES 			= int(myXML[0].bool_acces);
					reElts.ACTION				= String(myXML[0].action);
					reElts.PRIX					= Number(myXML[0].prix).toString();
				
				//---> 'BOOLACCES' = 0 -> OPTION, 'BOOLACCES' = 1 -> ABONNEMENT
				if(reElts.BOOLACCES > 0)
					valuesElements.ABONNEMENTS.addItem(reElts);
				else
					valuesElements.OPTIONS.addItem(reElts);
			}
		}
		
		private function getConfiguration(configuration:XML, valuesElements:ElementsCommande2):void
		{
			var conElts:ConfigurationElements 		= new ConfigurationElements();
				conElts.LIBELLE 					= String(configuration.code_interne);
				conElts.PORTABILITE.NUMERO 			= String(configuration.soustete[0]);
				conElts.COLLABORATEUR 				= String(configuration.nomemploye[0]);
			
			valuesElements.CONFIGURATIONS.addItem(conElts);
		}
		
		private function getItems(elts:ElementsCommande2):void
		{
			var obj		:Object = new Object();
			var price	:Number = 0;
			var len		:int = elts.OPTIONS.length;
			var ligne	:String = '';
			
			if(elts.CONFIGURATIONS.length > 0)
				ligne = (elts.CONFIGURATIONS[0] as ConfigurationElements).PORTABILITE.NUMERO;
			
			obj 		 = new Object();
			obj.LIGNE	 = ligne;
			
			for(var i:int = 0;i < len;i++)
			{
				price = price + Number((elts.OPTIONS[i] as RessourcesElements).PRIX);
				
				var type:String = '';
				
				switch((elts.OPTIONS[i] as RessourcesElements).ACTION)//---> R:RESILIER, A:AJOUTER, S:SUSPENDRE, V:REACTIVATION 
				{
					case 'R': 	type = ResourceManager.getInstance().getString('M16', 'R_siliation');
								isResiliation = true;  
								text_libelle = ResourceManager.getInstance().getString('M16','lignes_s_lectionn_es_pour_la_r_siliation');
								break;
					case 'S': 	type = ResourceManager.getInstance().getString('M16', 'Suspension');
								isResiliation = false; 
								text_libelle = ResourceManager.getInstance().getString('M16', 'lignes_s_lectionn_es_pour_la_suspension');
								break;
					case 'V': 	type = ResourceManager.getInstance().getString('M16', 'R_activation');
								isResiliation = false;
								text_libelle = ResourceManager.getInstance().getString('M16', 'lignes_s_lectionn_es_pour_la_r_activatio');
								break;
				}
				
				obj.TYPE = type;
			}
			
			obj.PRIX = Formator.formatTotalWithSymbole(price);
			
			listeResiliation.addItem(obj);
		}
		
	}
}