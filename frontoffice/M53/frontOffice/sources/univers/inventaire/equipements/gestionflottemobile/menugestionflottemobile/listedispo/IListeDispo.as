package univers.inventaire.equipements.gestionFlotteMobile.ListeDispo
{
	import mx.controls.DataGrid;
		
	public interface IListeDispo
	{
		function getIDSelection():int;
		function getLibelleType():String;
		function getLibelleSelection():String;
		function getSourceIcone():Class;
		function getDataGrid():DataGrid;
	}
}