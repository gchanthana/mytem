package univers.facturation.suivi.controles.services
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import univers.facturation.suivi.controles.event.HistoriqueEvent;
	import univers.facturation.suivi.controles.vo.HistoriqueVO;

	[Bindable]
	public class HistoriqueModel extends EventDispatcher
	{
		public var historique:ArrayCollection;
		
		public function HistoriqueModel()
		{
		}
		
		public function updateHistorique(values:ArrayCollection):void
		{
			
			historique = new ArrayCollection();
			
			var histo:HistoriqueVO;
			for(var i:int=0;i<values.length;i++){
				histo = new HistoriqueVO();
				histo.addHistorique(values[i]);
				historique.addItem(histo);
			}
			
			dispatchEvent(new HistoriqueEvent(HistoriqueEvent.HISTORIQUE_COMPELTE));
			
		}
	}
}