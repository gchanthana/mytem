<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.WebEvent"
extends="fr.consotel.api.ibis.publisher.handler.event.ReportingEvent"
hint="Extension de ReportingEvent permettant d'obtenir les mêmes informations pour un reporting exécuté directement (i.e : non planifié)
Certaines méthodes ne peuvent retourner la valeur exacte compte tenu du fait qu'il n'y a pas d'infos et d'historique de JOB pour ce type d'exécution
La localisation du rapport ne peut pas etre récupérée avec ce type d'exécution (Méthode manquante dans l'API BI Publisher)
Le TimeZone ne peut pas etre récupéré avec ce type d'exécution (Méthode manquante dans l'API BI Publisher)">
	<cffunction access="public" name="getUserName" returntype="String" hint="Retourne le login utilisateur BIP utilisé pour exécuter le rapport">
		<cfreturn getEventSource().getJob().USERNAME>
	</cffunction>

	<cffunction access="public" name="getMessage" returntype="String" hint="Retourne le message concernant l'exécution">
		<cfreturn getEventSource().getJob().MESSAGE>
	</cffunction>

	<cffunction access="public" name="getContentType" returntype="String" hint="Retourne le type MIME du contenu du rapport.
	La valeur retournée pour ce type d'exécution inclut l'encodage (e.g : text/html;charset=UTF-8)
	Une chaine vide est retournée si getReportContent() retourne une chaine vide">
		<cfset var reportingContent=getReportContent()>
		<cfif isValid("String",reportingContent) AND (LEN(TRIM(reportingContent)) EQ 0)>
			<cfreturn "">
		<cfelse>
			<cfreturn getReportContent().getReportContentType()>
		</cfif>
	</cffunction>
	
	<!--- Méthode spécifique à ce type d'évènement --->
	<cffunction access="public" name="getReportContent" returntype="any"
	hint="Retourne le contenu du rapport sans vérifier son type par rapport à la valeur de getStatus().
	Une chaine vide est retournée lorsque le contenu n'est pas défini (e.g : Echec d'exécution)
	Le nom de la classe correspondant au contenu du rapport provenant de BIP est retourné par getReportContentImpl()">
		<cfif structKeyExists(getEventSource().getJob(),"CONTENT")>
			<cfreturn getEventSource().getJob().CONTENT>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	
	<!---
	<cffunction access="public" name="getLocalization" returntype="String" hint="Retourne la localisation du rapport (e.g : fr_FR)
	Cette implémentation retourne toujours une chaine vide (Bug de l'implémentation BIP ReportResponse)">
		<cfif isValidBipContentImpl()>
			<cfreturn getReportContent().getReportLocale()>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>
	--->
	
	<!--- Méthodes spécifiques à ce type d'évènement --->
	<cffunction access="public" name="isValidBipContentImpl" returntype="boolean"
	hint="Compare (case sensitive) le nom la classe (Java) du contenu du rapport avec la valeur de getReportContentImpl() et retourne TRUE s'ils ont la meme valeur">
		<cfreturn NOT compareNoCase(getMetadata(getReportContent()).NAME,getReportContentImpl())>
	</cffunction>
	
	<cffunction access="private" name="getReportContentImpl" returntype="String"
	hint="Retourne la nom complet l'implémentation BIP correspondant au contenu du rapport lorsque getStatus() vaut SUCCESS()
	Bug : Le test avec la fonction isInstanceOf() retourne FALSE meme si le type correspond.
	Il est conseillé de comparer le nom de la classe avec la valeur retournée ou d'utiliser la valeur retournée par isValidBipContentImpl()">
		<cfreturn "com.oracle.xmlns.oxp.service.PublicReportService.ReportResponse">
	</cffunction>
</cfcomponent>