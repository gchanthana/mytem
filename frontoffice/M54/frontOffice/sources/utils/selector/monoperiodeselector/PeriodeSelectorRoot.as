package utils.selector.monoperiodeselector
{
	import event.selector.MonoPeriodeEvent;
	
	import mx.containers.Box;
	import mx.controls.HSlider;
	import mx.events.SliderEvent;

	public class PeriodeSelectorRoot extends Box
	{
		[Bindable]public var slider : HSlider;
		
		public function PeriodeSelectorRoot()
		{
			super();
		}
		
		protected function slider_changeHandler(event:SliderEvent):void
		{
			dispatchEvent(new MonoPeriodeEvent(MonoPeriodeEvent.CHANGER_VALUE_HSLIDER_EVENT));
		}
	}
}