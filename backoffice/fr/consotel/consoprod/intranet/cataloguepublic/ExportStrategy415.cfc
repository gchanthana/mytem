<cfcomponent output="true" extends="ExportStrategy" alias="fr.consotel.consoprod.intranet.cataloguepublic.ExportStrategy415">
	<cffunction name="ExporterCSV" access="public" returntype="string">
		<cfargument name="facture" type="Facture" required="true">
		<cftry>
					
			<cfset userMail = session.user.Email>	
			<cfmail from="#userMail#"
					to="samuel.divioka@consotel.fr"
					subject="Export des factures du PMU"
					type="html"
					charset="utf-8"
					wraptext="72"
					replyto="#userMail#"				
					server="#SESSION.MAILSERVER#">	
								 							
					Bonjour.<br>						
					Je vous prie de bien vouloir lancer la prcï¿½dure d'export des factures vers mon ERP.<br>
					Cordialement.<br>					
					<br>
					Client :<strong>Le PMU<strong>
			</cfmail>
			
	
			<cfcatch type="any">
				<cfreturn "#SESSION.MAILSERVER#">
			</cfcatch>			
		</cftry>		 
		<cfreturn "production">		 
	</cffunction>
</cfcomponent>