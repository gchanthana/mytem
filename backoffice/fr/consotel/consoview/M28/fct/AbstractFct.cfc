﻿<cfcomponent>
	<cffunction name="ThrowCfCatchError" access="public" returntype="Struct" hint="retourne un object contenant l'erreur du cfcatch">
		<cfargument name="idError" 		required="true" 	type="string" >
		<cfargument name="type" 		required="false" 	type="string" 	default="">
		<cfargument name="message" 		required="false" 	type="string"  	default="">
		<cfargument name="detail" 		required="false" 	type="string"  	default="">
		<cfargument name="stackTrace"	required="false" 	type="String" 	default=""/>
		
		<cfset cfCatchObject = createobject("component","fr.consotel.consoview.M28.Util").ThrowCfCatchError(idError,type,message,detail,stackTrace)/>
		
		<cfreturn cfCatchObject>
	</cffunction>
	
</cfcomponent>