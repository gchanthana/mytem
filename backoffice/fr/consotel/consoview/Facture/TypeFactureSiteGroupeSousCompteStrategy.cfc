<!--- =========================================================================
Classe: TypeFactureSiteGroupeSousCompteStrategy
Auteur: Cedric RAPIERA
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="TypeFactureSiteGroupeSousCompteStrategy" extends="TypeFactureStrategy" hint="">
<!--- METHODS --->
	<cffunction name="getData" access="public" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="false" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="Initial value for the IDINVENTAIRE_PERIODEproperty." />
		<cfargument name="siteID" required="false" type="string">
	</cffunction>
	<cffunction name="displayType" access="public" returntype="string" output="false" >
		<cfreturn "Facture du Site">
	</cffunction>
</cfcomponent>