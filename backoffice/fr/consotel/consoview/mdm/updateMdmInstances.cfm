<!---
Author: Cedric
Hint: Script de mise à jour des instances des implémentations fr.saaswedo.api.mdm.core.services.IMDM dans le scope APPLICATION pour tous les BackOffices
La liste des BackOffices provient de la base. Chaque BackOffice doit avoir l'application MyTEM et l'API MDM --->
<cftry>
	<cfset mailProperties=getMailProperties()>
	<cfoutput>
		<p>
			<cfoutput>
Script de mise à jour des instances des implémentations fr.saaswedo.api.mdm.core.services.IMDM dans le scope APPLICATION pour tous les BackOffices.
<br><br>La liste des BackOffices provient de la base (Procédure: #getBackOfficesProc()#). Chaque BackOffice concerné doit avoir :<br>
- L'application MyTEM et l'API MDM<br>
- Le composant fr.consotel.consoview.mdm.RemoteAdminMdm (API MDM)<br><br>
Un mail est envoyé par #mailProperties.from# à #mailProperties.to# après la mise à jour de de chaque BackOffice.<br>
Un mail est envoyé par #mailProperties.from# à #mailProperties.to# si une exception est rencontrée durant la mise d'un BackOffice.
Mais le traitement continuera pour les autres BackOffices.<br>
Un mail est envoyé par #mailProperties.from# à #mailProperties.to# si une exception qui ne concerne pas la mise à jour est rencontrée dans cette page.
Mais le traitement s'arretera pour tous les BackOffices.
			</cfoutput> 
		</p><hr>
	</cfoutput>
	<!--- URL d'accès au BackOffice pour la mise à jour de l'application --->
	<cfset currentBackOfficeUpdateURL="N.D">
	<cfset servicePath="/fr/consotel/consoview/mdm/RemoteAdminMdm.cfc">
	<!--- API Remoting --->
	<cfset httpClient=new fr.saaswedo.api.remoting.http.HttpClient()>
	<cfset httpParams=[
		{TYPE=httpClient.FORMFIELD_TYPE(),NAME="method",VALUE="updateMdmApplication"},
		{TYPE=httpClient.FORMFIELD_TYPE(),NAME="RANDOMID",VALUE=lsDateFormat(NOW(),"dd-mm-yyyy") & "-" & lsTimeFormat(NOW(),"HH-MM-SS")}
	]>
	<!--- Parcours de la liste des BackOffices --->
	<cfset backoffices=getBackOffices()>
	<cfset backofficeCount=backoffices.recordcount>
	<cfloop index="i" from="1" to="#backofficeCount#">
		<cfset backoffice=TRIM(backoffices["BACKOFFICE"][i])>
		<cfset protocol=VAL(backoffices["USE_SSL"][i]) EQ 1 ? "https":"http">
		<cfset currentBackOfficeUpdateURL=protocol & "://" & backoffice & servicePath>
		<cftry>
			<!--- Appel HTTP du service de mise à jour de l'application du BackOffice --->
			<cfset serviceId=httpClient.register(currentBackOfficeUpdateURL)>
			<cfset httpCall=httpClient.getServiceCall(serviceId)>
			<cfset httpResponse=httpClient.invoke(httpCall,httpClient.GET_METHOD(),httpParams)>
			<!--- Traitement de la réponse HTTP --->
			<cfset currentDate=NOW()>
			<cfset httpCode=httpClient.getResponseCode(httpResponse)>
			<cfset updateFailed=httpCode NEQ 200>
			<cfset updateStatus=updateFailed ? "Echouée":"Effectuée avec succès">
			<cfoutput>
				<h3>Mise à jour pour '#backoffice#' : #updateStatus# (HTTP #httpCode#)</h3>
				<cfflush/>
			</cfoutput>
			<!--- Notification mail du statut de la mise à jour ---> 
			<cfset mailProperties.subject="[MDM] Mise à jour de l'application de '#backoffice#' : #updateStatus#">
			<cfmail attributeCollection="#mailProperties#">
				<cfoutput>
					Mise à jour pour '#backoffice#' : #updateStatus# - HTTP #httpCode#
					(#lsDateFormat(currentDate,"dd/mm/yyyy")# - #lsTimeFormat(currentDate,"HH:MM:SS")#).<br><br>
					Script de mise à jour : #CGI.SCRIPT_NAME#<br>
					BackOffice d'excécution du script : #CGI.SERVER_NAME#<br>
					Exécuté par : #CGI.REMOTE_HOST#<br>
					URL du service HTTP de mise à jour de l'application du BackOffice (N.D si indéfini): #currentBackOfficeUpdateURL#<br><hr>
					Réponse retournée par le service de mise à jour (Dump si le contenu ne peut pas etre affiché):<br><br>
					<cfif isValid("String",httpResponse.fileContent)>
						<cfif isJSON(httpResponse.fileContent)>
							<cfdump var="#deserializeJSON(httpResponse.fileContent)#"><br>
						<cfelse>
							#httpResponse.fileContent#<br>
						</cfif>
					<cfelse>
						<cfdump var="#httpResponse.fileContent#"><br>
					</cfif>
				</cfoutput>
			</cfmail>
			<cfcatch type="Any">
				<cfset currentDate=NOW()>
				<cfoutput>
					<h3>Mise à jour pour '#backoffice#' : Echouée (Exception rencontrée : #CFCATCH.message#)</h3>
					<cfflush/>
				</cfoutput>
				<cfset mailProperties.subject="[MDM] Erreur durant la mise à jour de l'application d'un BackOffice : '#backoffice#'">
				<cfmail attributeCollection="#mailProperties#">
					<cfoutput>
						La mise à jour de l'application du BackOffice '#backoffice#' (N.D si indéfini) a échouée
						(#lsDateFormat(currentDate,"dd/mm/yyyy")# - #lsTimeFormat(currentDate,"HH:MM:SS")#).<br><br>
						Script de mise à jour : #CGI.SCRIPT_NAME#<br>
						BackOffice d'excécution du script : #CGI.SERVER_NAME#<br>
						Exécuté par : #CGI.REMOTE_HOST#<br>
						URL du service de mise à jour de l'application du BackOffice (N.D si indéfini): #currentBackOfficeUpdateURL#<br><hr>
						Dump des paramètres HTTP :<br><cfdump var="#httpParams#">
						<br><hr><br><h3>Dump de l'exception rencontrée l'appel au service de mise à jour :</h3>
						<cfdump var="#CFCATCH#">
					</cfoutput>
				</cfmail>
			</cfcatch>
		</cftry>
	</cfloop>
	<cfcatch type="Any">
		<cfset currentDate=NOW()>
		<cfoutput>
			<h3>Dump de l'exception rencontrée durant l'exécution de ce script<h3>
			<cfdump var="#CFCATCH#">
		</cfoutput>
		<cfset mailProperties.subject="[MDM] Erreur durant le script de mise à jour de l'application des BackOffices">
		<cfmail attributeCollection="#mailProperties#">
			<cfoutput>
				Le script de mise à jour de l'application des BackOffices a échoué
				(#lsDateFormat(currentDate,"dd/mm/yyyy")# - #lsTimeFormat(currentDate,"HH:MM:SS")#).<br><br>
				Script de mise à jour : #CGI.SCRIPT_NAME#<br>
				BackOffice d'excécution du script : #CGI.SERVER_NAME#<br>
				Exécuté par : #CGI.REMOTE_HOST#<br><hr>
				<h3>Dump de l'exception rencontrée durant l'exécution du script :</h3>
				<cfdump var="#CFCATCH#">
			</cfoutput>
		</cfmail>
	</cfcatch>
</cftry>

<!--- Liste des fonctions --->

<cffunction access="private" name="getBackOfficesProc" returntype="String" output="false" hint="Nom de la procédure retournant la liste des BackOffices">
	<cfreturn "PKG_MDM.getBackoffices">
</cffunction>

<cffunction access="private" name="getBackOffices" returntype="Query" output="false" hint="Retourne la liste des BackOffices">
	<cfset var backoffices="">
	<cfset var dataSource="ROCOFFRE">
	<!--- Exemple :
	<cfquery name="backoffices" datasource="#dataSource#">
		SELECT	'DNS_OU_IP_DU_BACKOFFICE:PORT' as BACKOFFICE, 0 as USE_SSL FROM DUAL
	</cfquery>
	--->
	<cfstoredproc datasource="#dataSource#" procedure="#getBackOfficesProc()#">
		<cfprocresult name="backoffices">
	</cfstoredproc>
	<cfreturn backoffices>
</cffunction>

<cffunction access="private" name="getMailProperties" returntype="Struct" output="false" hint="Retourne les propriétés mail">
	<cfreturn {
		server="mail-cv.consotel.fr",port=25,type="html",from="Notification MDM <monitoring@saaswedo.com>",to="monitoring@saaswedo.com",subject="[MDM]"
	}>
</cffunction>