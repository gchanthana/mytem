package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche
{
	import mx.events.FlexEvent;
	import mx.controls.Label;
	import flash.events.Event;
	import mx.controls.Alert;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import mx.rpc.events.ResultEvent;
	import univers.CvAccessManager;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.events.ListEvent;
	
	public class RechecrheDeLigne extends RechercheDeLigneIHM
	{		
		public static const SELECTION_CHANGED : String = "selectionChanged";
		
		
		private var _titre : String = "";
		private var _numTitre : String = "";
		
		private var _organisationData:ArrayCollection;
		private var _dataLines : ArrayCollection = new ArrayCollection();
		
		public function RechecrheDeLigne()
		{			
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		[Bindable(event="titreChanged")]
		public function get titre():String{
			return _titre;
		}
		
		public function set titre(t : String):void{
			_titre = t; 									
			var evtObj : Event = new Event("titreChanged");
			dispatchEvent(evtObj);
		}
				
		[Bindable(event="titreChanged")]
		public function get numTitre():String{
			return _numTitre;
		}
		
		public function set numTitre(n : String):void{			
			_numTitre = n; 				
			var evtObj : Event = new Event("titreChanged");
			dispatchEvent(evtObj);
		}
		
		private var _panier : ElementCommande;
		public function setPanier(panier : ElementCommande):void{
			_panier = panier;
		}
		
		public function updateLigneLibelle( tabLigne : Array):void{
			
		}
		
		public function getSelectedItems():Array{
			return myGrid.selectedItems;
		}
		
		private var _commande : Commande;
		public function affecterCommande(commande : Commande):void{
			_commande = commande;			
			/* if (treeCible.myTree.selectedIndex != -1){								
				_commande.idGroupeCible = treeCible.myTree.selectedItem.@NODE_ID;			
				_commande.flagCible = int(cbNewCible.selected);
			}	 */		
		}
		
		private function initIHM(fe : FlexEvent):void{		
			fillOrganisation();	
			afficherTitre();
			addEventListener("titreChanged",titreChangedHandler);
			pnlOrga.setXMLFilter(menuFilterData);
			pnlOrga.setParentRef(this);
			pnlOrga.treePerimetre.addEventListener(ListEvent.CHANGE,afficherLignes);
			txtFiltreLignes.addEventListener(Event.CHANGE,filtrerGrid);
			myGrid.addEventListener(ListEvent.CHANGE,selectionChanged);
		}
		
		private function filtrerGrid(ev : Event):void{			
			if (myGrid.dataProvider != null){
				(myGrid.dataProvider as ArrayCollection).filterFunction = filtrer;
				(myGrid.dataProvider as ArrayCollection).refresh();	
			}		
		}
		
		private function filtrer(ligne : ElementCommande):Boolean{
			if (String(ligne.libelleLigne).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1){
				return true;
			}else{
				return false;
			}
		}
		
		private function selectionChanged(le : ListEvent):void{
			if (myGrid.selectedIndex != -1){
				dispatchEvent(new Event(RechecrheDeLigne.SELECTION_CHANGED));	
			}			
		}
		
		private function titreChangedHandler(ev : Event):void{
			afficherTitre();
		}
		
		private function afficherTitre():void{			
			lblTitre.htmlText = "<font color='#008040' size='15'>" + numTitre + "</font>"+"<font color='#000000' size='10'> "+  titre + "</font>";
		}	
		//---------------------------------------------------------------------------------------------------------------------------------------//
		/** Rafraichit l'arbre */
		public function refresh():void
		{
			pnlOrga.refreshTree();
			pnlOrga.clearSearch();
		}
		
		/** Vide le grid des lignes */
		public  function clearGridLines():void
		{
			myGrid.dataProvider = null;
			_dataLines = null
			myGrid.selectedItem = null;	
			_dataLines = new ArrayCollection();		
		}
		
		/** Perimetre change */
		public function onPerimetreChange():void {
			fillOrganisation();
		}
		
		/** Methode appelée quand l'organisation change */
		public function organisationChanged():void
		{
			pnlOrga.clearSearch();
			clearGridLines();
		}
		
		/** Methode appelée quand on change de noeud */
		public function treeSelectionChanged():void
		{
			clearGridLines();			
		}
		
		
		/** ####################### END EVENT ###############################*/
		/** En analytique : renvoie la date selectionné */
		public function getSelectedDate():String 
		{
			return null;
		}
		
		/** Envoie requête pour récupérer les organisation de la racine */
		private function fillOrganisation():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getOrganisation",
																				fillOrganisationResult,
																				null);				
			RemoteObjectUtil.callService(op, idGroupeMaitre);		
		}
		
		private function fillOrganisationResult(result:ResultEvent):void
		{
			_organisationData = ArrayCollection(result.result);
			pnlOrga.setCmbData(_organisationData, "LIBELLE_GROUPE_CLIENT");
		
		
		}
		
		private function afficherLignes(ev : ListEvent):void{
			refreshLineGrid()
		}
		
		/** Rafraichit le grid lignes */
		public function refreshLineGrid():void
		{			
			var id:Number = pnlOrga.getSelectedTreeItemValue();
			displayLines(id);
		}
		
		private function fillData(values : ArrayCollection):void{
			var ligne : Object;			
			
			for (ligne in values){													
				var ec : ElementCommande = new ElementCommande();				
				
				ec.ligneID = values[ligne].IDSOUS_TETE;					
				ec.libelleLigne = values[ligne].SOUS_TETE;	
				_dataLines.addItem(ec);
			}			
			
			_dataLines.refresh();	
		}
		
		private function displayLines(IDnode:Number):void
		{
		 	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetre",
																				"getLines",
																				getLinesResult, null);
			RemoteObjectUtil.callService(op, IDnode, 0, getSelectedDate());		
		}
		
		private function getLinesResult(event: ResultEvent):void
		{
			fillData((event.result as ArrayCollection));
			myGrid.dataProvider = _dataLines;			
		}
		
	}
}