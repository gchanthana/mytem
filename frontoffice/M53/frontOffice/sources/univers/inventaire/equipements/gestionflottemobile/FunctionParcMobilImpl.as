package univers.inventaire.equipements.gestionflottemobile
{
	import mx.containers.VBox;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	

	public class FunctionParcMobilImpl extends VBox
	{
		public function FunctionParcMobilImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		protected  function afterCreationComplete(event:FlexEvent):void {
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
			afterPerimetreUpdated();
		}
		
		public function getUniversKey():String {
			return "USG";
		}
		
		public function getFunctionKey():String {
			return "USG_APPELS";
		}
		
		public function afterPerimetreUpdated():void {
			trace("(FunctionListeAppels) Perform After Perimetre Updated Tasks");
			trace("(FunctionListeAppels) SESSION FLEX :\n" +
						ObjectUtil.toString(CvAccessManager.getSession()));
		}
	}
}