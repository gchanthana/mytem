package composants.tb.ligne.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class FicheInfoService
	{
		
		private var _model:FicheInfoModel;
		public var handler:FicheInfoHandler;
		
		public function FicheInfoService()
		{
			_model = new FicheInfoModel();
			handler = new FicheInfoHandler(_model);
		}

		public function get model():FicheInfoModel
		{
			return _model;
		}

		public function set model(value:FicheInfoModel):void
		{
			_model = value;
		}
		
		public function getFicheInfo(idSous_tete:Number):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M311.ficheligne.FicheLigne",
				"getInfosPanelLigne",
				handler.getFicheInfoResultHandler);
			
			RemoteObjectUtil.callService(op,idSous_tete);		
		}

		public function setFonctionLigne(idSous_tete:Number, fonction:String):void//mettre les args
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M311.ficheligne.FicheLigne",
				"setFonctionLigne",
				handler.setFonctionLigne_resultHandler);
			
			RemoteObjectUtil.callService(op, idSous_tete, fonction);
		}
	}
}