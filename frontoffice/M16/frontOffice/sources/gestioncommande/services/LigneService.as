package gestioncommande.services
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.managers.SystemManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.LignesByOperateur;
	import gestioncommande.events.CommandeEvent;
	
	import session.SessionUserObject;
	
	public class LigneService extends EventDispatcher
	{
		
		private static const ERROR_MESSAGE	:String = ResourceManager.getInstance().getString('M16', 'Le_num_ro_de_ligne_n_est_pas_disponible');
		private static const OK_MESSAGE		:String = ResourceManager.getInstance().getString('M16', 'Le_num_ro_de_ligne_est_valide');		
		
		public var listeTypesDisponibles	:ArrayCollection;
		public var listeLignesByOperateur	:ArrayCollection = null;
		public var listeOperateurs			:ArrayCollection = null;

		
		public var listeLigneOrga			:Array;
		public var listeNumerosValides		:Array;
		
		public var numeroUnique				:String  = "";
		public var message					:String  = "";
		
		public var codeRIO					:String = "";
		
		public var boolOK					:Boolean = true;

		public function LigneService()
		{
		}
		
		public function genererNumeroLigneUnique():void
	    {
	    	listeNumerosValides = new Array();
	    	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.LigneService",
	    																		"genererNumeroLigneUnique",
	    																		genererNumeroLigneUniqueResultHandler);
	    	
	    	RemoteObjectUtil.callService(op,SessionUserObject.singletonSession.IDSEGMENT);
	    }
	    
	    public function genererNumerosLignesUnique(nbr:int): void
	    {
	    	listeNumerosValides = new Array();
	    	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.LigneService",
	    																		"genererNumerosLignesUnique",
	    																		genererNumerosLignesUniqueResultHandler);
	    	
	    	RemoteObjectUtil.callService(op,nbr,
	    									SessionUserObject.singletonSession.IDSEGMENT);
	    }
		
		public function getCodeRio(idSoustete:Number):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M16.v2.LigneService",
				"getCodeRio",
				getCodeRioResultHandler);
			
			RemoteObjectUtil.callService(op,idSoustete);
			
		}
	    
	    public function getTypeLigneAbo(idAbonnement:int): void
	    {
	    	listeTypesDisponibles = new ArrayCollection();
	    	
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.LigneService",
	    																		"getTypeLigneAbo",
	    																		getTypeLigneAboResultHandler);
	    	
	    	RemoteObjectUtil.callService(op,idAbonnement);
	    }

	    public function associerTypeLigneAbo(idTypeLigne:int, idProduit:int): void
	    {
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M16.v2.LigneService",
	    																		"associerTypeLigneAbo",
	    																		associerTypeLigneAboResultHandler);
	    	
	    	RemoteObjectUtil.callService(op,idTypeLigne,
	    									idProduit);
	    }
	    
	    public function verifierUniciteNumeroLigne(value:String):void
	    {	
	    	if(value != null)
	    	{
	    		boolOK = false;
	    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																		"fr.consotel.consoview.M16.v2.LigneService",
		    																		"verifierUniciteNumeroLigne",
		    																		verifierUniciteNumeroLigneResultHandler);
	    		
	    		RemoteObjectUtil.callService(op,value)	
	    	}
	    }
	    
	    public function searchLines(values:Array):void
	    {	
	    	if(values != null)
	    	{
	    		listeLigneOrga = new Array();
	    		
	    		var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																		"fr.consotel.consoview.M16.v2.LigneService",
		    																		"searchLines",
		    																		searchLinesResultHandler);
	    		
	    		RemoteObjectUtil.callService(op,values)	
	    	}
	    }
	    
		public function fournirListeOperateurs(): void
		{
			listeOperateurs = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.OperateurService",
																				"fournirListeOperateursSegment",
																				fournirListeOperateursResultHandler);
			
			RemoteObjectUtil.callService(op, SessionUserObject.singletonSession.IDSEGMENT);
		}
		
		public function fournirListeLignesByOperateurV3(idPool:int, idOperateur:int, idChampDeRecherche:int, etat:int, debut:int, fin:int, column:int, order:String,  clef:String = '', valWithCompte:Number=0, eligible:String=''):void
		{
			listeLignesByOperateur = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M16.v2.LigneService",
				"fournirListeLignesByOperateurV3",
				fournirListeLignesByOperateurResultHandler);
			
			RemoteObjectUtil.callService(op,idPool,
											idOperateur,
											clef,
											idChampDeRecherche,
											SessionUserObject.singletonSession.IDSEGMENT,
											etat,
											debut,
											fin,
											column,
											order,
											valWithCompte,
											eligible);
		}
		
		public function fournirListeLignesByOperateurV2(idPool:int, idOperateur:int, idChampDeRecherche:int, etat:int, debut:int, fin:int, column:int, order:String,  clef:String = '', isWithCompte:Boolean = false):void
		{
			listeLignesByOperateur = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.LigneService",
																				"fournirListeLignesByOperateurV2",
																				fournirListeLignesByOperateurResultHandler);
			
			RemoteObjectUtil.callService(op,idPool,
											idOperateur,
											clef,
											idChampDeRecherche,
											SessionUserObject.singletonSession.IDSEGMENT,
											etat,
											debut,
											fin,
											column,
											order,
											Formator.formatBoolean(isWithCompte));
			
		}
		
		private function getIdChampsRecherche(champs:String):int
		{
			return 1;
		}
		
		public function fournirListeLignesByOperateur(idPool:int, idOperateur:int, eligibilite:Boolean, champDeRecherche:int, clef:String = ""):void//--------A CHANGER!!!!!!!!!!!!!!!
		{
			listeLignesByOperateur = new ArrayCollection();
			
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M16.v2.LigneService",
																				"fournirListeLignesByOperateur",
																				fournirListeLignesByOperateurResultHandler);
			
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,
											idPool,
											idOperateur,
											clef,
											Formator.formatBoolean(eligibilite),
											champDeRecherche,
											SessionUserObject.singletonSession.IDSEGMENT);
			
		}
		
		
	    /**
	     * si ok alors numeroUnique = event.Result
	     */
	    private function genererNumeroLigneUniqueResultHandler(re:ResultEvent):void
	    {
	    	numeroUnique = re.result.toString();
	    	listeNumerosValides.push(numeroUnique);
	    	boolOK = true;
	    	dispatchEvent(new CommandeEvent(CommandeEvent.SEND_LIGNE_UNIQUE));
	    }
	    
	   	private function genererNumerosLignesUniqueResultHandler(re:ResultEvent):void
	    {
	    	listeNumerosValides = re.result as Array;
	    	dispatchEvent(new CommandeEvent(CommandeEvent.SEND_LIGNES_UNIQUE));
	    }
	    
	    private function getTypeLigneAboResultHandler(re:ResultEvent):void
	    {
	    	if(re.result)
	    	{
	    		if(re.result)
	    		{
	    			listeTypesDisponibles = re.result as ArrayCollection;
	    			dispatchEvent(new CommandeEvent(CommandeEvent.GET_TYPE_LIGNE));
	    		}
		    	else
		    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_des_types_de_lignes_impossible__'), 'Consoview');
	    	}
	    	else
		    	ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'R_cup_ration_des_types_de_lignes_impossible__'), 'Consoview');
	    }
	    
		private function associerTypeLigneAboResultHandler(re:ResultEvent):void
	    {
	    	if(re.result)
	    	{
	    		if(re.result > 0)
		    		dispatchEvent(new CommandeEvent(CommandeEvent.ASSOCIATED_LIGNE));
		    	else
		    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_d_associer_la_ligne__'), 'Consoview');
	    	}
	    	else
		    	ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_d_associer_la_ligne__'), 'Consoview');
	    }

	    private function verifierUniciteNumeroLigneResultHandler(re:ResultEvent):void
	    {
	    	if(re.result > 0)
	    	{
	    		message = ERROR_MESSAGE;	
	    		boolOK = false;
	    		ConsoviewAlert.afficherAlertInfo(message, ResourceManager.getInstance().getString('M16', 'Info'), null);
	    	}
	    	else if (re.result == 0)
	    	{
	    		message = OK_MESSAGE;
	    		ConsoviewAlert.afficherOKImage(message, SystemManager.getSWFRoot(this));
	    		listeNumerosValides.push(re.token.message.body[0]);
	    		boolOK = true;
	    	}
	    	else if (re.result < 0)
	    	{
	    		ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Une_erreur_s_est_produite_lors_de_la_v_r'), 'Consoview');
	    		boolOK = false;
	    	}
	    	
	    	dispatchEvent(new CommandeEvent(CommandeEvent.LIGNE_UNIQUE));
	    }
	    
	    private function searchLinesResultHandler(re:ResultEvent):void
	    {
	    	if(re.result)
			{
    			listeLigneOrga = re.result as Array;
	    		
				dispatchEvent(new CommandeEvent(CommandeEvent.LIGNE_FINDED));
	    	}
	    	else
		    	ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'Impossible_de_r_cuperer_la_liste_des_organisations__'), 'Consoview');
	    }
		
		private function fournirListeOperateursResultHandler(re:ResultEvent): void
		{	
			if(re.result)
			{
				listeOperateurs = re.result as ArrayCollection;
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_OPERATEURS));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'La_r_cup_ration_des_op_rateurs_est_impos'), 'Consoview');
			}
		}
		
		private function fournirListeLignesByOperateurResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				listeLignesByOperateur = mappingLignesByOperateur(re.result as ArrayCollection);
				
				dispatchEvent(new CommandeEvent(CommandeEvent.LISTED_LIGNESBY_OPE));
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M16', 'La_r_cup_ration_des_lignes_est_impossibl'), 'Consoview');
		}
		
		private function getCodeRioResultHandler(re:ResultEvent):void
		{
			if(re.result != "")
			{
				codeRIO=(re.result).toString();
			}
			dispatchEvent(new CommandeEvent(CommandeEvent.GET_CODE_RIO));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					MAPPING
		//--------------------------------------------------------------------------------------------//
		
		private function mappingLignesByOperateur(dataToMapp:ArrayCollection):ArrayCollection
		{
			var dataMapped		:ArrayCollection = new ArrayCollection();
			var lignesByOpe		:LignesByOperateur;
			
			for(var i:int = 0; i < dataToMapp.length;i++)
			{
				lignesByOpe								= new LignesByOperateur();
				lignesByOpe.COLLABORATEUR				= dataToMapp[i].COLLABORATEUR;
				lignesByOpe.IDCOLLABORATEUR				= dataToMapp[i].IDEMPLOYE;
				lignesByOpe.LIGNE						= dataToMapp[i].LIGNE;
				lignesByOpe.IDSOUS_TETE					= dataToMapp[i].IDSOUS_TETE;
				lignesByOpe.IDEMPL						= dataToMapp[i].IDEMP;
				lignesByOpe.IDPOOL						= dataToMapp[i].IDPOOL;
				lignesByOpe.IDETAT_LIGNE				= dataToMapp[i].IDETAT_LIGNE;
				lignesByOpe.NBRECORD					= dataToMapp[i].NB_ROWS;
				
				if(dataToMapp[i].CODE_INTERNE == null)
					lignesByOpe.CODE_INTERNE = '';
				else
					lignesByOpe.CODE_INTERNE = dataToMapp[i].CODE_INTERNE;
				
				if(dataToMapp[i].IDCOMPTE_FACTURATION == null)
				{
					lignesByOpe.COMPTE_FACTURATION		= '';
					lignesByOpe.IDCOMPTE_FACTURATION	= 0;
				}
				else
				{
					lignesByOpe.COMPTE_FACTURATION		= dataToMapp[i].COMPTE_FACTURATION;
					lignesByOpe.IDCOMPTE_FACTURATION	= dataToMapp[i].IDCOMPTE_FACTURATION;				
				}
				if(dataToMapp[i].IDSOUS_COMPTE == null)
				{
					lignesByOpe.SOUS_COMPTE				= '';
					lignesByOpe.IDSOUS_COMPTE			= 0;
				}
				else
				{
					lignesByOpe.SOUS_COMPTE				= dataToMapp[i].SOUS_COMPTE;
					lignesByOpe.IDSOUS_COMPTE			= dataToMapp[i].IDSOUS_COMPTE;				
				}
				
				lignesByOpe.DATE_ELLIGIBILITE			= dataToMapp[i].DATE_ELLIGIBILITE; 
				lignesByOpe.ELIGIBILITE					= dataToMapp[i].ELLIGIBILITE; 
				lignesByOpe.RESTE						= dataToMapp[i].NB_JR_FIN_LG;
				lignesByOpe.OPERATEUR					= dataToMapp[i].OPERATEUR;
				lignesByOpe.IDOPERATEUR					= dataToMapp[i].IDOPERATEUR;
				lignesByOpe.FIN_PERIODE_CONTRACTUELLE	= dataToMapp[i].FIN_PERIODE_CONTRACTUELLE;
				lignesByOpe.FPC						 	= dataToMapp[i].FPC;		
				lignesByOpe.MONTANT_MENSUEL_PEN			= dataToMapp[i].MONTANT_MENSUEL_PEN;
				lignesByOpe.FRAIS_FIXE_RESILIATION		= dataToMapp[i].FRAIS_FIXE_RESILIATION;
				lignesByOpe.FRAIS_TOTAL_RESIL			= dataToMapp[i].FRAIS_TOTAL_RESIL;
				
				dataMapped.addItem(lignesByOpe);
			}
			
			return dataMapped;
		}
		
	}
}