<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.service.AbstractTaskService" displayname="fr.consotel.consoview.api.ibis.publisher.service.AbstractTaskService"
			implements="fr.consotel.consoview.api.ibis.publisher.service.ITaskService">
	<cffunction name="getDefaultXmlReportTask" access="public" output="false" returntype="xml">
		<cfxml variable="defaultXmlReportTask">
			<TASK>
				<REPORT_ABSOLUTE_PATH/>
				<TEMPLATE_ID/>
				<OUTPUT_FORMAT/>
				<OUTPUT_NAME/>
				<OUTPUT_EXT/>
				<APP_NAME/>
				<CODE_RAPPORT/>
				<MODULE_NAME/>
				<DELIVERY>
					<JOB/>
					<CHANNEL>FTP</CHANNEL>
				</DELIVERY>
				<PARAMETERS>
					<!-- PARAMETER TAG DEFINITION :
					<PARAMETER>
						<NAME>PARAM_NAME</NAME>
						<VALUES>
							<VALUE>0</VALUE>
						</VALUES>
						<TYPE>CV</TYPE>
					</PARAMETER>
					-->
				</PARAMETERS>
			</TASK>
		</cfxml>
		<cfreturn defaultXmlReportTask>
	</cffunction>
	
	<cffunction name="run" access="public" output="false" returntype="struct">
		<cfargument name="userIdCv" type="numeric" required="true">
		<cfargument name="idgestionnaire" type="numeric">
		<cfargument name="xmlReportTaskParam" type="xml">
		<cfset var xmlReportTask="NULL">
		<cfset var resultStruct=structNew()>
		<cfset resultStruct["JOBID"]=-1>
		<cfset resultStruct["UUID"]="">
		<cflog text="************* CFMX - AbstractTaskService - run *************">
		<cftry>
			<cfset CV_USER="consoview">
			<cfset CV_PWD="public">
			<cfset MAIL_SERVER="mail-cv.consotel.fr">
			<cfset HANDLER_FROM_ADDR="monitoring@saaswedo.com">
			<cfset DEV_SUPPORT_ADDR="monitoring@saaswedo.com">
			<cfset EXCEPTION_SUBJECT_PREFIX="TASK-PROCESS BIP">
			<cflog text="Serveur BackOffice : #CGI.SERVER_NAME#">
			<cflog text="Serveur de mail (MAIL SERVER) : #MAIL_SERVER#">
			<cflog text="Expediteur (HANDLER_FROM_ADDR) : #HANDLER_FROM_ADDR#">
			<cflog text="Adresse Support de dev (DEV_SUPPORT_ADDR) : #DEV_SUPPORT_ADDR#">
			<cflog text="Prefixe du sujet (EXCEPTION_SUBJECT_PREFIX) : #EXCEPTION_SUBJECT_PREFIX#">
			<!--- ********************************************************************************** --->
			<cfif isDefined("ARGUMENTS.xmlReportTaskParam")>
				<cfset xmlReportTask=ARGUMENTS.xmlReportTaskParam>
			<cfelse>
				<cfset xmlReportTask=getDefaultXmlReportTask()>
			</cfif>
			<cfset userId="consoview">
			<cfset userPwd="public">
			<cfset PROCESS_ID=21>
			<cfset nomDuFichierOutput=TRIM(xmlReportTask["TASK"]["OUTPUT_NAME"].xmlText)>
			<cfset fileExtension=TRIM(xmlReportTask["TASK"]["OUTPUT_EXT"].xmlText)>
			<cfset CODE_RAPPORT=TRIM(xmlReportTask["TASK"]["CODE_RAPPORT"].xmlText)>
			<cfset cheminIdRacine="/TASK/PARAMETERS/PARAMETER[child::NAME = 'G_IDRACINE']">
			<cfset tIdracine=xmlSearch(xmlReportTask,cheminIdRacine)>
			<cfset idracine=VAL(tIdracine[1]["VALUES"]["VALUE"].xmlText)>
			<cfset MODULE_NAME=TRIM(xmlReportTask["TASK"]["MODULE_NAME"].xmlText)>
			<cfset BIP_SERVER=SESSION.bip_server_name>
			<cfset APP_NAME=TRIM(xmlReportTask["TASK"]["APP_NAME"].xmlText)>
			<cfset containerService=createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
			<cfset uuid=containerService.GETUuid()>
			<cfset resultStruct["UUID"]=uuid>
			<cfset addDocStatus=containerService.ADDDocumentTableContainer(nomDuFichierOutput,REPLACE(nomDuFichierOutput," ","_","all"),-1,CODE_RAPPORT,userIdCv,idracine,MODULE_NAME,fileExtension,lsDateFormat(NOW(),"yyyy/mm/dd"),BIP_SERVER,APP_NAME,0,1,-1,-1,session.CODEAPPLICATION)>
			<cfif (addDocStatus NEQ -1) AND (uuid EQ addDocStatus)>
				<cfset reportService=createObject("component","fr.consotel.consoview.api.ibis.publisher.service.ReportService")>
				<cfset requestObject=reportService.createScheduleRequest(xmlReportTask,userId,userPwd)>
				<cfset requestObject["userJobName"]=uuid>
				<cfset requestObject["deliveryRequest"]["ftpOption"]["remoteFile"]="/app/pelican/container/" & requestObject["userJobName"]>
				<!---
				<cfset requestObject["deliveryRequest"]["ftpOption"]["remoteFile"]="/app/pelican/container/" & nomDuFichierOutput & "." & fileExtension>
				--->
				<cfset addParamStatus=0>
				<cfset nbParams=arrayLen(xmlReportTask.TASK.PARAMETERS.xmlChildren)>
				<cfloop index="i" from="1" to="#nbParams#">
					<cfset paramValuesArray=arrayNew(1)>
					<cfset nbParamValues=arrayLen(xmlReportTask["TASK"]["PARAMETERS"]["PARAMETER"][i]["VALUES"].xmlChildren)>
					<cfloop index="j" from="1" to="#nbParamValues#">
						<cfset paramValuesArray[j]=xmlReportTask["TASK"]["PARAMETERS"]["PARAMETER"][i]["VALUES"]["VALUE"][j].xmlText>
					</cfloop>
					<cfset setParamValueStatus=reportService.changeParameterValues(requestObject["reportRequest"],
											TRIM(xmlReportTask["TASK"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText),paramValuesArray)>
					<cfif compareNoCase(TRIM(xmlReportTask["TASK"]["PARAMETERS"]["PARAMETER"][i]["TYPE"].xmlText),"client") EQ 0>
						<cfset addParamStatus=addParamStatus + containerService.ADDparamClient(uuid,"string",
																TRIM(xmlReportTask["TASK"]["PARAMETERS"]["PARAMETER"][i]["LABEL"].xmlText),
																TRIM(arrayToList(paramValuesArray,"/")))>
					</cfif>
					<cfif setParamValueStatus EQ FALSE>
						<cfset errorMsg="ERREUR AFFECTATION POUR LE PARAMETRE : " & xmlReportTask["TASK"]["PARAMETERS"]["PARAMETER"][i]["NAME"].xmlText>
						<cflog text="#errorMsg#">
						<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
							type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
							<cfoutput>
								#errorMsg#<br />
							</cfoutput>
							DUMP XML TASK :<br />
							<cfdump var="#xmlReportTask#">
						</cfmail>
					</cfif>
				</cfloop>
				<cfset paramClientXPath="/TASK/PARAMETERS/PARAMETER[child::TYPE = 'CLIENT']">
				<cfif addParamStatus EQ arrayLen(xmlSearch(xmlReportTask,paramClientXPath))>
					<!--- Etape h.	Ajout des paramètres du rapport dans la table container (format XML) : PROCEDURE STOCKEE A CORRIGER (PARAMETRES)--->
					<!--- ATTENDRE LA CORRECTION DE LA PROCEDURE STOCKEE --->
					<cfset addParamTableStatus=containerService.ADDParamsTableContainer(uuid,xmlReportTask)>
					<cfif addParamTableStatus EQ uuid>
						<cftry>
							<cfset jobId=reportService.scheduleReport(requestObject,userId,userPwd)>
							<cfset addJobIdStatus=containerService.ADDjobidToDocument(uuid,jobId)>
							<cfif addJobIdStatus EQ 1>
								<cflog text="-***************************** jobid = #jobId# - process_id = #PROCESS_ID# ">
								<cfset setCodeStatus=containerService.setCodeProcessForJOBID(jobId,PROCESS_ID)>
								<cfif setCodeStatus EQ jobId>
									<!--- 
									<cfreturn jobId>
									 --->
									<cfset fake=containerService.MAJStatus(uuid,1)>
									<cfset resultStruct["JOBID"]=jobId>
									<cfreturn resultStruct>
								<cfelse>
									<cfset errorMsg="ERREUR - setCodeProcessForJOBID : " & setCodeStatus>
									<cflog text="#errorMsg#">
									<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
										type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
										<cfoutput>#errorMsg#</cfoutput>
									</cfmail>
									<cfset fake=containerService.MAJStatus(uuid,3)>
								</cfif>
							<cfelse>
								<cfset errorMsg="ERREUR - ADDjobidToDocument : " & setCodeStatus>
								<cflog text="#errorMsg#">
								<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
									type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
									<cfoutput>#errorMsg#</cfoutput>
								</cfmail>
								<cfset fake=containerService.MAJStatus(uuid,3)>
							</cfif>
						<cfcatch type="any">
							<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
									type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
									<cfdump var="#CFCATCH#" label="ERREUR : scheduleReport (API BIP)">
							</cfmail>
							<cfset fake=containerService.MAJStatus(uuid,3)>
						</cfcatch>
						</cftry>
					<cfelse>
						<cfset errorMsg="ERREUR - ADDParamsTableContainer : " & addParamTableStatus>
						<cflog text="#errorMsg#">
						<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
							type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
							<cfoutput>#errorMsg#</cfoutput>
						</cfmail>
						<cfset fake=containerService.MAJStatus(uuid,3)>
					</cfif>
				<cfelse>
					<cfset errorMsg="ERREUR - Un des ADDparamClient : " & addParamStatus>
					<cflog text="#errorMsg#">
					<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
						type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
						<cfoutput>#errorMsg#</cfoutput>
					</cfmail>
					<cfset fake=containerService.MAJStatus(uuid,3)>
				</cfif>
			<cfelse>
				<cfset errorMsg="ERREUR - ADDDocumentTableContainer : " & addDocStatus>
				<cflog text="#errorMsg#">
				<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
					type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
					<cfoutput>#errorMsg#</cfoutput>
				</cfmail>
				<cfset fake=containerService.MAJStatus(uuid,3)>
			</cfif>
			<cfreturn resultStruct/>
			<cfcatch type="any">
				<cflog text="====== (BIP) API RAPPORT (AbstractTaskService) - EXCEPTION CATCHED - Server : #CGI.SERVER_NAME# - Remote : #CGI.REMOTE_HOST# ======">
				<cfmail from="#HANDLER_FROM_ADDR#" to="#DEV_SUPPORT_ADDR#" subject="#EXCEPTION_SUBJECT_PREFIX# (EXCEPTION) : Exception rencontrée" 
					type="html" charset="utf-8" wraptext="72"  server="#MAIL_SERVER#">
					<cfoutput>
						<b>Dump du CFCATCH :</b><br />
					</cfoutput>
					<cfdump var="#CFCATCH#" expand="true">
				</cfmail>
				<cfset fake=containerService.MAJStatus(uuid,3)>	
				<cfreturn resultStruct/>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="getDebug" access="public" output="false" returntype="xml">
		<cfargument name="debugKey" type="string" required="true">
		<cfreturn VARIABLES[arguments.debugKey]>
	</cffunction>
</cfcomponent>
