package univers.inventaire.equipements.gestionflottemobile.fiches.system.sav
{
	import composants.util.DateFunction;
	
	[Bindable]
	public class Intervention
	{
			
			public var IDINTERVENTION: Number = 0;
			public var IDSAV: Number = 0; //Un id SAV correspond à un idTicket (idSAV étant l appellation en bdd)
		    public var IDMOTIF: Number = 0;
		    public var IDTYPE: Number = 0;
		    public var LIBELLE: String ="";
		    public var REFERENCE: String ="";
		    public var CODE_INTERNE: String = "";
		    public var DATE_DEB: Date = new Date();
		    public var DATE_FIN: Date = new Date();
		    public var DUREE: String = "";
		    public var COUT_INTERVENTION: Number = 0;
		    public var COMMENTAIRE: String = "";
		    public var LIBELLE_TYPE: String = "";
		     public var LIBELLE_MOTIF: String = "";
		     
		     public function Intervention(idSAV:Number=0)
		    {
		    	if(idSAV)
		    	{
		    		this.IDSAV = idSAV;
		    	}
		    }
		    public function get DUREE_INTERVENTION():Number
			{
				return DateFunction.dateDiff("d",DATE_FIN,DATE_DEB); 
			}
			public function get DATE_DEB_STRING():String
			{
				return DateFunction.formatDateAsShortString(DATE_DEB);
			}
	
	}
}