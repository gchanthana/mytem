<cfcomponent output="false">

	<cffunction access="public" name="ajouterModifierDestinataire" hint="ajouter & modifier un destinataire" returntype="Numeric" output="true">
		
		<cfargument name="destinataire" type="struct" required="true">
		
		<cfset nom=trim(ARGUMENTS["destinataire"]["nom"])>
		<cfset prenom=trim(ARGUMENTS["destinataire"]["prenom"])>
		<cfset mail=trim(ARGUMENTS["destinataire"]["mail"])>
		<cfset id=ARGUMENTS["destinataire"]["id"]>
		<cfset racineIduser   = SESSION.PERIMETRE.ID_GROUPE>
		
		
		<cfset ajout=false>
		<cfif id eq 0>			
			<cfset action="A"><!--- lors de l'ajout' --->
			<cfset ajout=true>
			<cfelse>
				<cfset action="M">
				<cfset ajout=false>
		</cfif>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.IUDestinataireRapport">		
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#" variable="p_iddestinataire" null="#ajout#">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#racineIduser#" variable="p_idracine">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#nom#" variable="p_nom">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#prenom#" variable="p_prenom">
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#mail#" variable="p_email">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	

		<cfset resultat=p_retour>
		
		<cfif (resultat eq 1) AND (action eq "A") >
			<cfreturn resultat>
			<cfelseif (resultat eq 1) AND (action eq "M")><!--- en cas de modification --->
				<cfset resultat=2 >
				<cfreturn resultat>
		</cfif>
		<cfreturn resultat>
	
	</cffunction>
	
	<cffunction name="supprimerDestinataire" hint="supprimer un destinataire" returntype="numeric">
		
		<cfargument name="id" type="numeric" required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.DeleteDestinataire">		
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id#" variable="p_iddestinataire">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	
			
		<cfset resultat=#p_retour#>
		<cfreturn resultat>
	
	</cffunction>
	
	
	<cffunction name="supprimerPlusieursDestinataire" hint="supprimer plusieurs destinataire" returntype="numeric">
		
		<cfargument name="id" type="string" required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.DeleteDestinataire_v2">		
			<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#id#" variable="p_destinataires">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	
			
		<cfset resultat=#p_retour#>
		<cfreturn resultat>
	
	</cffunction>
	
</cfcomponent>