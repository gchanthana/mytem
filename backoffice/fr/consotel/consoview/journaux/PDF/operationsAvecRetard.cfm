<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong><cfoutput>Opérations avec retard au #Lsdateformat(now(), 'dd mmmm yyyy')#</cfoutput></strong></td>
				</tr>
				<tr>
					<td valign="top">
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
								
								<th>&nbsp;Type opération&nbsp;</th>
								 
								<th>&nbsp;Libellé opération&nbsp;</th>								
								
								<th>&nbsp;N°opération&nbsp;</th>
								 
								<th>&nbsp;Dernière Action&nbsp;</th>
								
								<th>&nbsp;Effectué par &nbsp;</th>	
								 
								<th>&nbsp;Dernier état&nbsp;</th>
								 
								<th>&nbsp;Durée réf.&nbsp;</th>								
								 
								<th>&nbsp;Retard(jours)&nbsp;</th>																
															
							</tr>
<cfset numRow = 1>
<cfoutput query="qGetGrid">	
	<cfif (numRow mod 30) eq 0>
<tr><td colspan="22" height="1" bgcolor="808080"></td></tr>	
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
		<cfdocumentitem type="pagebreak"/>		
<table align="center" width="100%" >
	<tr>	 	
		<td valign="top">
			<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
				<tr>
					<td class="td5"><strong>Opérations avec retard au #Lsdateformat(now(), 'dd mmmm yyyy')#</strong></td>
				</tr>
				<tr>
					<td valign="top">					
						<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
							<tr align="center">
																 
								<th>&nbsp;Type opération&nbsp;</th>
								 
								<th>&nbsp;Libellé opération&nbsp;</th>								
								
								<th>&nbsp;N°opération&nbsp;</th>
								 
								<th>&nbsp;Dernière Action&nbsp;</th>
								
								<th>&nbsp;Effectué par &nbsp;</th>	
								 
								<th>&nbsp;Dernier état&nbsp;</th>
								 
								<th>&nbsp;Durée réf.&nbsp;</th>								
								 
								<th>&nbsp;Retard(jours)&nbsp;</th>	
															
							</tr>
		<cfset numRow = numRow +1>
	<cfelse>				
					<cfif (numRow mod 2) eq 0>
							<tr>								
								 
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#TYPE_OPERATION#&nbsp;</td>								
								 										
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LIBELLE_OPERATIONS#&nbsp;</td>
								
								<td class="grid_normal_alt" valign="top">&nbsp;#NUMERO_OPERATION#&nbsp;</td>
								 
								<td class="grid_normal_alt" align="center" valign="top">&nbsp;#LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),"dd/mm/yyyy")#&nbsp;</td>
								 
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LOGIN_NOM# #LOGIN_PRENOM#&nbsp;</td>	
								 
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#LIBELLE_ETAT#&nbsp;</td>
								 
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#DUREE_ETAT#&nbsp;</td>
							 
								<td class="grid_normal_alt" align="right" valign="top">&nbsp;#RETARD#&nbsp;</td>
															
							</tr>
						<cfelse>
							<tr>								
								
								<td class="grid_normal" align="right" valign="top">&nbsp;#TYPE_OPERATION#&nbsp;</td>								
								 										
								<td class="grid_normal" align="right" valign="top">&nbsp;#LIBELLE_OPERATIONS#&nbsp;</td>
								
								<td class="grid_normal" valign="top">&nbsp;#NUMERO_OPERATION#&nbsp;</td>								 
								 
								<td class="grid_normal" align="center" valign="top">&nbsp;#LSDateFormat(createDate(left(DATE_ACTION,4),mid(DATE_ACTION,6,2),mid(DATE_ACTION,9,2)),"dd/mm/yyyy")#&nbsp;</td>
								 
								<td class="grid_normal" align="right" valign="top">&nbsp;#LOGIN_NOM# #LOGIN_PRENOM#&nbsp;</td>	
								 
								<td class="grid_normal" align="right" valign="top">&nbsp;#LIBELLE_ETAT#&nbsp;</td>
								 
								<td class="grid_normal" align="right" valign="top">&nbsp;#DUREE_ETAT#&nbsp;</td>
							 
								<td class="grid_normal" align="right" valign="top">&nbsp;#RETARD#&nbsp;</td>
															
							</tr>
						</cfif>							 					
							
		<cfset numRow = numRow +1>		
	</cfif>
</cfoutput>
						 					
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>