<cfcomponent displayname="GetInventaire" output="false">

<cffunction name="getHorsInventaireFacture" access="remote" returnType="query">
	
	<cfargument name="idInventaire" type="string" required="true">
	<cfargument name="effectif" type="numeric" required="true">
	
	<cfscript>
			idracine_master = session.perimetre.idracine_master;
			idgroupe_racine = session.perimetre.id_groupe;
			_idlang = iif(val(session.user.idglobalization) gt 0,val(session.user.idglobalization),3);
			qstring = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=db-1-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-2-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-3-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-4-vip) (PORT=1522)) (FAILOVER=ON) (CONNECT_DATA=(SERVICE_NAME=appli.consotel.fr)))";
            q = "begin :result := pkg_M32.fn_Det_Res_HorsInv_facturee_v2(:a,:b,:c,:d,:e); end;";
            Class = createObject("java","java.lang.Class");
            DriverManager = CreateObject("java", "java.sql.DriverManager");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = CreateObject("java", "java.sql.Connection");
            con = DriverManager.getConnection(qstring,
            "offre", "consnia");
            stmt = createObject("java", "java.sql.Statement");
            stmt = con.prepareCall(q);
            OracleTypes = CreateObject("java", "oracle.jdbc.driver.OracleTypes");
            stmt.registerOutParameter("result", OracleTypes.CURSOR);
            stmt.setInt ("a", javacast("int",idracine_master));
            stmt.setInt ("b", javacast("int",idgroupe_racine));
			stmt.setInt ("c", javacast("int",idInventaire));
			stmt.setInt ("d", javacast("int",effectif));
			stmt.setInt ("e", javacast("int",_idlang));
            stmt.execute();
            rs = stmt.getObject("result");
            ariesAdv = CreateObject("java", "coldfusion.sql.QueryTable").init(rs);
	</cfscript>
	
	
	
	<cfreturn ariesAdv>
	
</cffunction>


<cffunction name="getInventaireNonFacture" access="remote" returnType="query">	
	<cfargument name="idInventaire" type="string" required="true">
	<cfargument name="effectif" type="numeric" required="true">
	<cfargument name="solution" type="numeric" required="true">
	
	<cfscript>
			idracine_master = session.perimetre.idracine_master;
			idgroupe_racine = session.perimetre.id_groupe;
			_idlang = iif(val(session.user.idglobalization) gt 0,val(session.user.idglobalization),3);
			qstring = "jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=db-1-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-2-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-3-vip) (PORT=1522)) (ADDRESS=(PROTOCOL=TCP)(HOST=db-4-vip) (PORT=1522)) (FAILOVER=ON) (CONNECT_DATA=(SERVICE_NAME=appli.consotel.fr)))";
            q = "begin :result := pkg_M32.fn_Det_Res_Inv_Nonfacturee(:a,:b,:c,:d,:f); end;";
            Class = createObject("java","java.lang.Class");
            DriverManager = CreateObject("java", "java.sql.DriverManager");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = CreateObject("java", "java.sql.Connection");
            con = DriverManager.getConnection(qstring,
            "offre", "consnia");
            stmt = createObject("java", "java.sql.Statement");
            stmt = con.prepareCall(q);
            OracleTypes = CreateObject("java", "oracle.jdbc.driver.OracleTypes");
            stmt.registerOutParameter("result", OracleTypes.CURSOR);
            stmt.setInt ("a", javacast("int",idracine_master));
            stmt.setInt ("b", javacast("int",idgroupe_racine));
			stmt.setInt ("c", javacast("int",idInventaire));
			stmt.setInt ("d", javacast("int",effectif));
			stmt.setInt ("f", javacast("int",solution));			
            stmt.execute();  
            rs = stmt.getObject("result");
            ariesAdv = CreateObject("java", "coldfusion.sql.QueryTable").init(rs);
	</cfscript>
	
	<cfreturn ariesAdv>
	
</cffunction>


<cffunction name="getLibelleCompte" access="remote" returnType="array">
	
	<cfargument name="operateurid" type="numeric" required="true">
	
	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.getLibelleSCCF">
		<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#operateurid#"/>
		<cfprocresult name="p_operateur">
	</cfstoredproc>
	
	
	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M32.L_lastlevel_ope">
		<cfprocparam  type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#operateurid#"/>
		<cfprocresult name="p_dernierniveau">
	</cfstoredproc>
	
	<cfset p_retour = ArrayNew(1) >
	<cfset p_retour[1] = p_operateur >
	<cfset p_retour[2] = p_dernierniveau >
	
	
	<cfreturn p_retour>
	
</cffunction>

</cfcomponent>
