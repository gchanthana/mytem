package univers.parametres.parametrecompte.vo
{
	[Bindable]
	public class ProfilCommande
	{
		public var idProfil : int;
		public var libelle : String;
		public var codeInterne : String;
		public var nbUser : int;
		public var commentaire : String;
		
		public var autorise_edition : Boolean = true; // passe à false uniquement pour le profil par default
		
		public function Profil():void
		{
		} 

	}
}