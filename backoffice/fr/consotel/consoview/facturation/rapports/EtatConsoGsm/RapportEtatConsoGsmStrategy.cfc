<!---
Package : fr.consotel.consoview.facturation.rapports.EtatConsoGsm
--->
<cfcomponent name="RapportEtatConsoGsmStrategy">
	<cffunction name="getReportData" access="private" returntype="any">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset ID_ORGA = ID_PERIMETRE>
		<cfset biServer=APPLICATION.BI_SERVICE_URL>
        <!--- <cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_rep.etat_conso">
	        <cfprocparam value="#ID_ORGA#" cfsqltype="CF_SQL_INTEGER">
 	        <cfprocparam  value="#RapportParams.DATEDEB#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATEFIN#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc> --->
		<!--- getReportParameters --->
		<!--- Envoi des paramètres --->
		<cfset ArrayOfParamNameValues=ArrayNew(1)>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_IDGROUPE_CLIENT">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=ID_ORGA>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_DATEDEB">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=RapportParams.DATEDEB>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>
		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="P_DATEFIN">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=RapportParams.DATEFIN>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[3]=ParamNameValue>
		<!--- Rapport --->
		<cfset myParamReportRequest=structNew()>
		<cfset myParamReportRequest.reportAbsolutePath=url.folder>
		<cfset myParamReportRequest.attributeTemplate="cv">
		<cfset myParamReportRequest.attributeLocale="fr-FR">
		<cfset myParamReportRequest.attributeFormat="excel">
		<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
		<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<!---  --->
		<!--- <cfdump var="#myParamReportRequest#"> --->
		<cfset myParamReportParameters=structNew()>
		<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
		<cfset myParamReportParameters.userID="consoview">
		<cfset myParamReportParameters.password="public">
		<cfdump var="#myParamReportParameters#" label="myParamReportParameters">
		
		<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport"
				argumentCollection="#myParamReportParameters#">
		</cfinvoke>
		<cfreturn resultRunReport>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset rapportParams.chaine_date = rapportParams.DATEDEB>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEFIN)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEFIN)>
		<cfset rapportParams.DATEFIN =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>
		<cfcontent type="#qGetData.getReportContentType()#" variable="#qGetData.getReportBytes()#">
		<cfheader name="Content-Disposition" value="attachment;filename=a.xls">
		<!--- <cfelse>
			<cfoutput>
				<center><strong><h2>Aucunes données répondant à votre demande</h2></strong></center>
			</cfoutput>
		</cfif> --->
	</cffunction>
</cfcomponent>
