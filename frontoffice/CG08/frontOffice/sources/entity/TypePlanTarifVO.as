package entity
{
	public class TypePlanTarifVO
	{
		private var _id:int;
		private var _libelle:String;
		private var _description:String;
		
		public function TypePlanTarifVO()
		{
		}
		
		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("IDFC_TYPE_PLAN_TARIF"))
					this.id = obj.IDFC_TYPE_PLAN_TARIF;
					
				if(obj.hasOwnProperty("LIBELLE_TYPE_PLAN"))
					this.libelle = obj.LIBELLE_TYPE_PLAN;
				
				if(obj.hasOwnProperty("DESCRIPTION_TYPE_PLAN"))
					this.description = obj.DESCRIPTION_TYPE_PLAN;
				
			}
			catch(e:Error)
			{
				trace("##Initialisation d'un objet TypePlanVO erroné ");
			}
		}
		
		public function get description():String { return _description; }
		
		public function set description(value:String):void
		{
			if (_description == value)
				return;
			_description = value;
		}
		
		public function get libelle():String { return _libelle; }
		
		public function set libelle(value:String):void
		{
			if (_libelle == value)
				return;
			_libelle = value;
		}
		
		public function get id():int { return _id; }
		
		public function set id(value:int):void
		{
			if (_id == value)
				return;
			_id = value;
		}
		
	}
}