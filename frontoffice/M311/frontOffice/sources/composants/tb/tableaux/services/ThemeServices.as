package composants.tb.tableaux.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class ThemeServices extends EventDispatcher
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		private var _listeTheme			:ArrayCollection;
		private var _listeSurTheme		:ArrayCollection;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//		
		
		public function ThemeServices()
		{
		}
		
	//--------------------------------------------------------------------------------------------//
	//					PROPRIETEES
	//--------------------------------------------------------------------------------------------//		
		[Bindable]
		public function set listeTheme(value:ArrayCollection):void
		{
			if(value != null)
			{
				_listeTheme = value;
			} 
		}

		public function get listeTheme():ArrayCollection
		{
			return _listeTheme;
		}
		
		[Bindable]
		public function set listeSurTheme(value:ArrayCollection):void
		{
			if(value != null)
			{
				_listeSurTheme = value;
			} 
		}

		public function get listeSurTheme():ArrayCollection
		{
			return _listeSurTheme;
		}
	
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//	

		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
	
		public function getListTheme():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.M311.accueil.Tableaubord",
																				"fournirListeTheme_SurTheme",
																				 getListThemeResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS - RESULTEVENT
		//--------------------------------------------------------------------------------------------//
				
		private function getListThemeResultHandler(re:ResultEvent):void
		{
		 	try
		 	{
				if(re.result)
				{
					if(re.result.hasOwnProperty("SURTHEME") && re.result.hasOwnProperty("THEME"))
					{
						listeTheme 		= re.result.THEME as ArrayCollection;
						listeSurTheme 	= re.result.SURTHEME as ArrayCollection;
						
						dispatchEvent(new Event("THEME_LOADED"));
					}
					else
						ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M311', 'Erreur'), "Consoview");
				}
				else
		  			trace("erreur : fr.consotel.consoview.M311.accueil.Tableaubord  getListTheme ");
		  	}
		  	catch(er : Error)
		  	{		  	
		  		trace(er.message,"fr.consotel.consoview.M311.accueil.Tableaubord  getListTheme ");
		  	}
		}
		
	}
}