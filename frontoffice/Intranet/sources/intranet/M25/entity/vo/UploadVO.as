package intranet.M25.entity.vo
{
    import flash.net.FileReference;

    public class UploadVO
    {
        private var _uid:String;
        private var _docType:String;
        private var _uploadUrl:String;
        private var _uploadFileRef:FileReference;

        public function UploadVO()
        {
        }

        public function get docType():String
        {
            return _docType;
        }

        public function set docType(value:String):void
        {
            _docType = value;
        }

        public function get uploadUrl():String
        {
            return _uploadUrl;
        }

        public function set uploadUrl(value:String):void
        {
            _uploadUrl = value;
        }

        public function get uploadFileRef():FileReference
        {
            return _uploadFileRef;
        }

        public function set uploadFileRef(value:FileReference):void
        {
            _uploadFileRef = value;
        }

        public function get uid():String
        {
            return _uid;
        }

        public function set uid(value:String):void
        {
            _uid = value;
        }
    }
}