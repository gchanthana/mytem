package intranet.GestionDroits.renderer
{
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.CheckBox;

	public class Ckb12 extends VBox
	{
		public var ckb:CheckBox=new CheckBox();
		
		public function Ckb12()
		{
			super();
			setStyle("horizontalAlign","center");
			this.addChild(ckb);
			this.addEventListener(MouseEvent.CLICK,ckbChangeDataHandler);
		}
		
		public function ckbChangeDataHandler(e:MouseEvent):void{
			if(ckb.selected){
				data.obj12 = 1;
			}else{
				data.obj12 = 0;
			}
		}
					
		override public function set data(value:Object):void{
			if(value != null && value != ""){
				super.data=value;
				if(data.obj12 == 1){
					ckb.selected=true;
				}else{
					ckb.selected=false;
				}
				if(data.EDITABLE == false){
					ckb.enabled = false
				}else{
					ckb.enabled = true;
				}
			}
		}
	}
}