package gestionparc.ihm.fiches.popup
{
	import composants.ui.TitleWindowBounds;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.ModeleVO;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	public class PopupChooseModeleImpl extends TitleWindowBounds
	{
		//--------------- VARIABLES ----------------//
		
		public var dgChooseModele	:DataGrid;
		public var tiFiltre			:TextInput;
		public var btnValider		:Button;
		public var btnAnnuler		:Button;
		[Bindable]
		public var btnValidEnabled	:Boolean = false;
		
		private var _itemCurrent	:ModeleVO = null;
		private var _listeModele	:ArrayCollection = new ArrayCollection();
		
		
		//--------------- METHODES ----------------//
		
		/* constructeur */
		public function PopupChooseModeleImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseModeleHandler);
			btnAnnuler.addEventListener(MouseEvent.CLICK,annulerHandler);
			dgChooseModele.addEventListener(ListEvent.CHANGE,currentItemChangeHandler);
		}
		
		/* */
		private function currentItemChangeHandler(le:ListEvent):void
		{
			_itemCurrent = le.currentTarget.selectedItem;
			setSelectedItem(_itemCurrent);
		}
		
		/* */
		private function setSelectedItem(obj:ModeleVO):void
		{
			for each(var item:ModeleVO in listeModele)
			{
				item.SELECTED = false;
				if(obj.IDEQUIPEMENT_FOURNISSEUR == item.IDEQUIPEMENT_FOURNISSEUR)
				{
					item.SELECTED = true;
					_itemCurrent = item;
				}
				listeModele.itemUpdated(item);
			}
			btnValidEnabled = true;
		}
		
		/* */
		public function setModele(item:ModeleVO):void
		{
			setSelectedItem(item);
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeModele.filterFunction = filtreSaisieModele;
			listeModele.refresh();
		}
		
		/* systeme de filtre */
		private function filtreSaisieModele(item:Object):Boolean
		{
			if ((item.CATEGORIE_EQUIPEMENT != null && (item.CATEGORIE_EQUIPEMENT as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				|| (item.LIBELLE_EQ != null && (item.LIBELLE_EQ as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				|| (item.NOM_FABRIQUANT != null && (item.NOM_FABRIQUANT).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				|| (item.NOM_FOURNISSEUR != null && (item.NOM_FOURNISSEUR).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				|| (item.TYPE_EQUIPEMENT != null && (item.TYPE_EQUIPEMENT).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
			)
				return true;
			else
				return false;
		}
		
		/* au click sur annuler */
		private function annulerHandler(me:MouseEvent):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/* au click sur valider */
		private function validerChooseModeleHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event("VALID_CHOIX_MODELE",true));
			annulerHandler(null);
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		
		[Bindable]
		public function get listeModele():ArrayCollection
		{
			return _listeModele;
		}
		public function set listeModele(value:ArrayCollection):void
		{
			_listeModele = value;
		}

		public function get itemCurrent():ModeleVO
		{
			return _itemCurrent;
		}

		public function set itemCurrent(value:ModeleVO):void
		{
			_itemCurrent = value;
		}
	}
}