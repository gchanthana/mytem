<cfinterface displayName="fr.consotel.api.ibis.publisher.handler.event.IReportEvent" extends="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" hint="Reporting EVENT Interface">
	
	<!--- (BIP) REPORT EVENT INTERFACE (Auteur : Cedric)
		REPORT STATUS VALUES :
			- F : FAILED
			- C : RUNNING
			- S : SUCCESS
			- Other values : WARNING (W)
	 --->
	
	<cffunction access="public" name="getJobId" returntype="numeric" hint="Returns BI Publisher JOB_ID">
	</cffunction>
	
	<cffunction access="public" name="getReportStatus" returntype="String" hint="Returns BI Publisher REPORT_STATUS as a single character STRING. Possible value : S (SUCCESS), F (FAILED), C (RUNNING). Other values means WARNING">
	</cffunction>

	<cffunction access="public" name="getReportPath" returntype="string" hint="Returns BI Publisher REPORT_URL">
	</cffunction>
	
	<cffunction access="public" name="getBipServer" returntype="string" hint="Returns BI Publisher SERVER_NAME">
	</cffunction>
	
</cfinterface>