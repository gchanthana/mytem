package univers.inventaire.inventaire.etapes
{
	import mx.events.FlexEvent;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import flash.events.Event;
	import composants.util.DateFunction;
	import mx.controls.Alert;
	import univers.facturation.suivi.factures.Index;
	import composants.util.ConsoviewFormatter;
	import mx.utils.ObjectUtil;
	
	
	
	/**
	 * Classe gerant les informations pour une étape
	 * Rassemble les infos d'une action et sur l'enregistrement d'une action
	 * */	
	public class EtapeFlag extends EtapeFlagIHM
	{
		 
		//Reference vers l'identifiant une action 
		private var _IDACTION : int;

		//Reference vers l'identifiant d'un etat
		private var _IDETAT : int;

		//Reference vers l'identifaint d'une action enregistrée
		private var _IDINV_OP_ACTION : int;

		//Reference vers le qualificatif 'reference pour le calcul des régules' -> 1 = oui, 0 = non
		private var _REF_CALCUL : int;

		//Reference vers le qualificatif 'l'etat entraine un changement d'etat dans l'inventaire' -> 1 = oui, 0 = non
		private var _CHANGER_ETAT_INVENTAIRE : int;

		//Reference vers le qualificatif 'l'etat entraine une creation d'operation' -> 1 = oui, 0 = non
		private var _ENTRAINE_CREATION : int;

		//Reference vers le qualificatif 'l'etat entraine une cloture d'operation' -> 1 = oui, 0 = non
		private var _ENTRAINE_CLOTURE : int;
		 
		//ArrayCollection contenant les information pour l'etape
		[Bindable]
		private var infoEtape : ArrayCollection;
		
		//Reference vers la méthode distante qui ramene les infos sur l'état
		private var opInfoEtat : AbstractOperation;
		
		
		/**
		 * Constructeur
		 * @param idAction l'identifiant de l'action
		 * @param idinv_op_action l'identifiant de l'enregistrement de l'action
		 * */
		public function EtapeFlag( idAction : int, idinv_op_action : int)
		{	
			super();	
			_IDINV_OP_ACTION = idinv_op_action;
			_IDACTION = idAction;					
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			
		}
		
		
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{
			chargerInfoEtat();					
		}
		
		
		
		/**
		 * Getter sur l'identifiant de l'etat
		 * @return idEtat
		 * */
		public function get IDETAT():int{
			
			return _IDETAT;
		}
		
		/**
		 * Getter sur l'identifiant de l'etat
		 * @return idEtat
		 * */
		public function get IDACTION():int{
			return _IDACTION;
		}
		
		/**
		 * Getter sur l'identifiant de l'action enregistrer
		 * @return idInvOpAction
		 * */
		public function get IDINV_OP_ACTION():int{
			return _IDINV_OP_ACTION;
		}		
		
		/**
		 * Return 1 si la date de l'action est pris pour la reference du calcul des régul
		 * @return refCalcul
		 * */
		public function get REF_CALCUL():int{
			return _REF_CALCUL;
		}	
		
		/**
		 * Return 1 si l'etat entraine un changement dans l'inventaire
		 * @return changeEtatInventaire
		 * */
		public function get CHANGER_ETAT_INVENTAIRE():int{
			return _CHANGER_ETAT_INVENTAIRE;
		}
		
		/**
		 * Return 1 si l'etat entraine une cloture d'opération
		 * @return entraineCloture
		 * */	
		public function get ENTRAINE_CLOTURE():int{
			return _ENTRAINE_CLOTURE;
		}
		
		/**
		 * Return 1 si l'etat entraine une creation d'opération de vérification
		 * @return entraineCreation
		 * */	
		public function get ENTRAINE_CREATION():int{
			return _ENTRAINE_CREATION;
		}
		
		
		/**
		 * Retourne la durée indicative pour l'etape (etat)
		 * */
		public function get dureeIndicative():String{
			return infoEtape[0].DUREE_ETAT;
		}	
		
		
		/**
		 * Supprime la bordure de la vignette 
		 * */
		public function removeFocusHallow():void{			
			conteneur.setStyle("borderThickness",1);
			conteneur.setStyle("borderColor","#AAB3B3");				 
		}
		
		/**
		 * Ajoute une bordure à la vignettte
		 * */
		public function setFocusHallow():void{
			conteneur.setStyle("borderThickness",3);
			conteneur.setStyle("borderColor","#333366");			
		}
		
		/**
		 * Change la couleur de la bordure de la vignette selon l'etat des ressources
		 * de l'opération 
		 * @param  dansInventaire l'etat des ressources (dans ou hors inventaire)
		 * */
		public function setColorHallow(dansInventaire:Number):void{
			if (dansInventaire == 1){
				conteneur.setStyle("borderThickness",3);
				conteneur.setStyle("borderColor","#A9CC66");	
			}else{
				conteneur.setStyle("borderThickness",3);
				conteneur.setStyle("borderColor","red");				
			}		
		}
		
		
		/**
		 * Change la couleur de fond de la vignette suivant la date de l'action 
 		 * Si la date de l'action est anterieur à la date du jour, le fond vert, si non le fond est blanc
		 * @param dateEffetAction la date d'effet de l'action 
		 * */
		public function setBackGroundColor(dateEffetAction : Date):void{
			var now : Date = new Date();			
			var dateLimite : Date = dateEffetAction;
			var diff : Number = DateFunction.dateDiff("d",dateLimite,now);
			
			if (diff < -1){
				conteneur.setStyle("backgroundAlpha",0.3);
				conteneur.setStyle("backgroundColor","#A9CC66");
			}else{
				conteneur.setStyle("backgroundAlpha",1);
				conteneur.setStyle("backgroundColor","#ffffff");
			}			
			
		}
		
		
		/**
		 * Change la couleur de la date dans la vignette
		 * Si la date de l'action est anterieur à la date du jour, la date est affichée en rouge, si non elle est affichée en noir
		 * @param dateEffetAction la date d'effet de l'action 
		 * */
		public function manageDureeColor(dateEffetAction : Date):void{
			var now : Date = new Date();			
			var dateLimite : Date = dateEffetAction;
			var diff : Number = DateFunction.dateDiff("d",dateLimite,now);
		
			if (diff < -1){
				
				lblDuree.setStyle("color","red");
			}else{
				lblDuree.setStyle("color","black");
			}			
		} 
		
		/**
		 * Enleve la fleche apres la vignette
		 * */
		public function supprimerFlecheSuivant():void{			
			myImgSuivant.visible = false;
		}
		
		/**
		 * Met une fleche apres la vignette
		 * */
		public function montrerFlecheSuivant():void{
			myImgSuivant.visible = true;
		}
		
		
		/**
		 * Met une date dans la vignette au format jj/mm/aaaa
		 * @param d une date
		 * */
		public function setDate(d : Date):void{
			lblDuree.text = DateFunction.formatDateAsString(d);
		}
				
		/*-------------------------------------ReMOTING ------------------------------------------------------------------*/
		
		
		//Charge les informations pour la vignette
		private function chargerInfoEtat():void{
		
 			opInfoEtat = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.OperationAction",
																			"getInfoEtat",
																			chargerInfoEtatResultHandler);				
			RemoteObjectUtil.callService(opInfoEtat,
										 _IDACTION);					
			
			
		}
		
		//Handler de la methode 'chargerInfoEtat' 
		//Met à jour les infos de la vignette
		//dispatche un évenement de type 'EtapeFlagComplete' signifiant que les infos sont disponibles
		private function chargerInfoEtatResultHandler(re : ResultEvent):void{			
			infoEtape = re.result as ArrayCollection;
			txtTitre.text = String(infoEtape[0].LIBELLE_ETAT).toUpperCase();
			_IDETAT = infoEtape[0].IDINV_ETAT;	
			_REF_CALCUL = infoEtape[0].REF_CALCUL;
			if (_REF_CALCUL == 1){
				txtTitre.setStyle("color","red");
			}			
			_CHANGER_ETAT_INVENTAIRE = infoEtape[0].CHANGER_ETAT_INVENTAIRE;
			_ENTRAINE_CLOTURE = infoEtape[0].ENTRAINE_CLOTURE;
			_ENTRAINE_CREATION = infoEtape[0].ENTRAINE_CREATION;
			this.toolTip = infoEtape[0].COMMENTAIRE_ETAT;
			
			dispatchEvent(new Event("EtapeFlagComplete"));			
		}
		
	}
}