<cfcomponent output="false">
	
	<cffunction name="insertDestinataireImport" access="remote" returntype="any">
		
		<cfargument name="xml" 	required="true" type="String"/>
		<cfset racineIduser   = SESSION.PERIMETRE.ID_GROUPE>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.importDestinataire">		
			<cfprocparam type="in"  cfsqltype="CF_SQL_INTEGER" value="#racineIduser#" variable="p_idracine">
			<cfprocparam type="in"  cfsqltype="CF_SQL_VARCHAR" value="#xml#" variable="p_destinataire ">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER"    variable="p_retour">				
		</cfstoredproc>	
		
		<cfset resultat=#p_retour#>
		<cfreturn resultat>
	</cffunction>
	
</cfcomponent>