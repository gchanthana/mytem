<cfcontent type="application/pdf"> 
<cfheader name='Content-Disposition' VALUE='attachment;filename=SpecifiqueGsm-#rapportParams.RAISON_SOCIALE#.pdf'/>

<cfif rapportParams.periodicite EQ 0>
	<cfset periodiciteString = "Mensuel">
<cfelse>
	<cfset periodiciteString = "Bimestriel">
</cfif>

	<!--- Rapport Flash et pdf --->
<cfcontent type="application/pdf">
<cfheader name="Content-Disposition" value="inline;filename=detailGsm.pdf">

	<cfobject component="graph" name="mygraph">
	<cfdocument format="#rapportParams.format#" backgroundvisible="yes" fontembed = "yes" 
				unit="CM" margintop="1" pagetype="custom" pageheight="29" pagewidth="21" marginleft="1" orientation="landscape">
	<html>
		<head>
<!--- 			
			<link href="./css/pdf_css.css" rel="stylesheet" type="text/css">
			 --->
			<STYLE type="text/css">
			<!--
html body {
  margin		:0;
  padding		:0;
  background	: #FFFFFF;
  font			:x-small Verdana,Georgia, Sans-serif;
  voice-family	: "\"}\""; voice-family:inherit;
  font-size:x-small;
  } html>body {
	font-size	: x-small;
}

.cadre {
	border-color:#000000;
	border-style:solid;
	border-collapse:collapse;
	
	border-top-width:1px;
	border-left-width:1px;
	border-right-width:1px;
	border-bottom-width:1px;
}

.cadre1{
	background		: rgb(255,255,255);
	border-bottom	:1px solid rgb(0,0,0);
	border-left		:1px solid rgb(0,0,0);
	border-right	:1px solid rgb(0,0,0);
	border-top		:1px solid rgb(0,0,0);
	border-collapse : collapse;
}

table .menuconsoview
{
	border 		: none;
	background	: transparent;
	font		: normal 8px Tahoma,Arial, sans-serif;
	color		: #005EBB;
}

.menuconsoview caption{
	background		:#036 ;
	color			: white;
	font			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	padding			: 4px;
	letter-spacing	: 1px;

}

.menuconsoview th{
	background   	:#99CCFF ;
	color        	: #036;
	font        	: normal 9px EurostileBold,verdana,arial,helvetica,serif;
	padding      	: 3px;
	border-collapse	: collapse;
}

.menuconsoview .th_bottom{
	vertical-align	: middle;
	padding			: 3px;
	border-bottom	: 1px solid #808080;
	font			: normal 9px Tahoma,Arial, sans-serif;
	color        	: #036;
	background		: #99CCFF;
}

.menuconsoview .th_top{
	vertical-align	: middle;
	padding			:3px;
	border-top		: 1px solid #808080;
	font			: normal 9px Tahoma,Arial, sans-serif;
	color       	: #036;
	background		: #99CCFF;
}

.menuconsoview .grid_normal{
	background	: #FDFDFB;
	color		: rgb(0 , 0, 0);
	font		: normal 8px Tahoma,Arial, sans-serif;
	padding		: 2px;
}

.menuconsoview .grid_small{
	background	: #FDFDFB;
	color		: rgb(0 , 0, 0);
	font		: normal 8px Tahoma,Arial, sans-serif;
	padding		: 0px;
}

.menuconsoview .grid_medium {
	background	: #FDFDFB;
	color		: rgb(0 , 0, 0);
	font		: normal 8px Tahoma,Arial, sans-serif;
	padding-top		: 1px;
}

.menuconsoview .grid_total{
	background	: #008ACC;
	color		: rgb(0 , 0, 0);
	font		: normal 8px Tahoma,Arial, sans-serif;
	padding-top	: 2px;
}

.menuconsoview .grid_bold{
	background	: #FDFDFB;
	color		: rgb(0 , 0, 0);
	font		: bold 8px Tahoma,Arial, sans-serif;
	padding		: 0px;
}

/* caption impossible pour liste des produits */
.td5{
	padding		:5px;
	background	:#036;
	color		:rgb(255,255,255);
	font		:normal 9px EurostileBold,verdana, Georgia, arial,helvetica,sans-serif;
}

/*facture*/
table .FT{
	border 		: none;
	background	: transparent;
	font		: normal 7px Tahoma,Arial, sans-serif;
	color		: #000000;
}

.FT .small_header{
	margin-top	: 0px;
	color		: rgb(0 , 0, 0);
	font		: italic 6px Arial, sans-serif;
	padding		: 2px;
	font-style	: italic;
	vertical-align : top;
	text-align	: center;
}

.FT .big{
	border-top		: 2px solid #C0C0C0;
	border-bottom	: 1px solid #C0C0C0;
	margin-top		: 0px;
	color			: rgb(0 , 0, 0);
	font			: 9px Arial, sans-serif;
	padding			: 5px;
}

.FT .very_big{
	border-top	: 1px solid #C0C0C0;
	margin-top	: 0px;
	color		: rgb(0 , 0, 0);
	font		: 12px Arial, sans-serif;
	padding		: 6px;
}

.FT .huge{
	margin-top	: 0px;
	color		: rgb(0 , 0, 0);
	font		: 18px Arial, sans-serif;
	padding		: 2px;
}

.FT .normal{
	margin-top	: 0px;
	color		: rgb(0 , 0, 0);
	font		: 8px Arial, sans-serif;
	padding		: 2px;
}
/*fin facture*/


/* resultat (consofacture, redzone)*/
table .resultat 
{
	font-size		: 100%;
	border			: 1px solid #CCCC99;
	background 		: #F4F2EF;
	border-collapse	: collapse;
	padding-right	: 2;
	padding-left	: 2;
}

.resultat th{
	background		: #EDCEA5;
	color			: #7F2114;
	font			: normal 7px EurostileBold,Verdana,Georgia, Arial,Helvetica,Serif;
	font-weight		: normal; 
	border-collapse	: collapse;
	padding			: 2px;
}

.resultat td {
	font 			: normal 7px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 2px;
}

.resultat .td_clair {
	background		: rgb(241,241,241);
	font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .td_fonce {
	background		: #E9E9E9;
	font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .td_rouge {
	background		: rgb(242,169,41);
	font			: normal 6px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: #292820;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .td_out {
	layout-grid		: both loose 2px 2px;
	background		: #FFE1E1;
	border			: none;
	color			: rgb(0 , 0, 0);
	font			: normal 6px Tahoma,Arial, sans-serif;
	padding			: 2px;
	border-collapse	: collapse;
	padding			: 1px;
}

.resultat .grid_small{
	background	: #FDFDFB;
	color		: rgb(0 , 0, 0);
	font		: normal 8px Tahoma,Arial, sans-serif;
	padding		: 0px;
}
/*fin resultat*/

/*ConsoFacture*/
.titres_gros {
	background		: transparent;
	color			: #292820;
	font 			: bold 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	letter-spacing	: 2px;
	border-collapse	: collapse;
	text-align		: left;
}

.sous_titres_gros {
	background		: transparent;
	color			: #292820;
	font 			: normal 9px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	border-collapse	: collapse;
	text-align		: left;
}
/*fin ConsoFacture*/

/* Logique budgetaire */
table .logique_budget {
	font-size		:100%;
	padding			: 1px;
	border			: 1px solid black;
	border-collapse	: collapse; 
}

.logique_budget th {
	font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	padding			: 6px;
	color			: #000000;
	text-align		: left;
	background		: Silver;
	border			: 1px solid black;
	border-collapse	: collapse;
}

.logique_budget .Titre {
	font			: normal 11px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	padding			: 2px;
	color			: white;
	background		: black;
	border			: 1px solid black;
	border-collapse	: collapse;
}

.logique_budget td {
	font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: Black;
	border			: 1px solid black;
	background		: white;
	border-collapse	: collapse;
}

.logique_budget .td_total {
	font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	padding			: 2px;
	color			: black;
	background		: #E9E9E9;
	border			: 1px solid black;
	border-collapse	: collapse;
}

.logique_budget .td_alerte {
	font			: normal 10px EurostileBold,verdana, Georgia, arial,helvetica,serif;
	color			: red;
	border			: 1px solid black;
	background		: white;
	border-collapse	: collapse;
}
			-->
			</STYLE>
		</head>
	<cfoutput>
		<body>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
<!--- 				
				<tr>
					<td align="left" width="40%" height="45"><img src="./images/consoview_new.gif" height="40"></td>
					<td width="17%">&nbsp;</td>
					<td width="3%">&nbsp;</td>
					<td></td>
				</tr>
				 --->
				<tr>
					<td colspan="4">
					<font face="Tahoma" size="2"><strong>Rapport Sp�cifique GSM</strong></font>
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<font face="Tahoma" size="2"><strong>Flotte #rapportParams.raison_sociale# / #session.user.prenom# #session.user.nom#</strong></font>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<font face="Tahoma" size="1">
							<u>P&eacute;riode:</u>
							&nbsp;#LsDateFormat(parseDateTime(RapportParams.date_m),"Mmmm yyyy")#
<!--- 							&nbsp;#LsDateFormat(CreateDate(right(RapportParams.date_m,4),mid(RapportParams.date_m,4,2),"01"),"Mmmm yyyy")# --->
							<cfif periodiciteString eq "Bimestriel">
								- #LsDateFormat(parseDateTime(RapportParams.date_m2),"Mmmm yyyy")#
<!--- 								- #LsDateFormat(CreateDate(right(RapportParams.date_m2,4),mid(RapportParams.date_m2,4,2),"01"),"Mmmm yyyy")# --->
							</cfif>
						</font>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						&nbsp;
					</td>
				</tr>
			</table>
		</cfoutput>
		<!--- <cfinclude template="/#ELEM_CONTENU#"> ---> 
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr valign="top">
				<!--- Tableau --->
				<td width="50%">
					<table width="100%">
						<tr valign="top"><td>
					<table border="0" cellpadding="0" cellspacing="0" class="cadre"><tr><td>
					<table border="0" cellpadding="0" cellspacing="0" class="menuconsoview">
						<tr>
							<td colspan="4"></td>
							<td colspan="7" class="grid_normal" align="center">
								Cumul
								<cfoutput>
									#periodiciteString# #rapportParams.date_m# au #LsDateFormat(parseDateTime(RapportParams.date_m2),"dd/mm/yyyy")#
								</cfoutput>
							</td>
						</tr>
						<cfset grdTotal=0>
						<cfset grdTotalPrecedent=0>
						<cfoutput query="qFinale" group="type_theme">
							<cfset totalType=0>
							<cfset totalPrecedent=0>
							<tr>
								<td colspan="3" align="left" class="th_top">#type_theme#</td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="th_top" align="center">
								<cfif type_theme neq "Abonnements">Nombre d'appels</cfif>
								</td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="th_top" align="center">
								<cfif type_theme neq "Abonnements">Dur�e en minutes<cfelse>Quantit�</cfif>
								</td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="th_top" align="center">Montant (&euro;)</td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="th_top" align="center">Evo. M-1</td>
							</tr>
							<tr>
								<td class="grid_small" colspan="3" height="3"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="3"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="3"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="3"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="3"></td>
							</tr>
							<cfoutput group="sur_theme">
								<tr>
									<td class="grid_small"></td>
									<td class="grid_bold" colspan="2">#sur_theme#</td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small"></td>
								</tr>
								<cfset totalSurtheme=0>
								<cfoutput>
									<tr>
										<td class="grid_small"></td>
										<td class="grid_small"></td>
										<td class="grid_small">#theme_libelle#</td>
										<td width="1" bgcolor="AAAAAA"></td>
										<td class="grid_small" align="right"><cfif type_theme neq "Abonnements">#LsNumberFormat(nombre_appel)#</cfif></td>
										<td width="1" bgcolor="AAAAAA"></td>
										<td class="grid_small" align="right"><cfif type_theme neq "Abonnements">#LsNumberFormat(int(duree_appel))#<cfelse>#LsNumberFormat(qte)#</cfif></td>
										<td width="1" bgcolor="AAAAAA"></td>
										<td class="grid_small" align="right">#LsEuroCurrencyFormat(montant_final)#</td>
										<td width="1" bgcolor="AAAAAA"></td>
										<td class="grid_small" align="right"></td>
									</tr>
									<cfset totalSurtheme=totalSurtheme+montant_final>
									<cfset totalType=totalType+montant_final>
									<cfset totalPrecedent=totalPrecedent+montant_precedent>
									<cfset grdTotal=grdTotal+montant_final>
									<cfset grdTotalPrecedent=grdTotalPrecedent+montant_precedent>
								</cfoutput>
								<tr>
									<td class="grid_small"></td>
									<td class="grid_small"></td>
									<td class="grid_bold">Total #sur_theme#</td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_bold" align="right">#LsEuroCurrencyFormat(totalSurtheme)#</td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small"></td>
								</tr>
								<tr>
									<td class="grid_small" colspan="3" height="7"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small" height="7"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small" height="7"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small" height="7"></td>
									<td width="1" bgcolor="AAAAAA"></td>
									<td class="grid_small" height="7"></td>
								</tr>
							</cfoutput>
							<tr>
								<td class="grid_total" colspan="3">Total #type_theme#</td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_total"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_total"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_total" align="right">#LsEuroCurrencyFormat(totalType)#</td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" align="right">
								<cfif totalPrecedent neq 0>
									<cfset t=1-((Abs(totalPrecedent)-Abs(totalType))/totalType)>
									<cfif t lt 0.01>
										N.D
									<cfelse>
										#LsNumberFormat(-100*(Abs(totalPrecedent)-Abs(totalType))/totalPrecedent,"+.0")# %
									</cfif>
								<cfelse>
									N.D
								</cfif>
								</td>
							</tr>
							<tr height="1"><td colspan="11" bgcolor="000000"></td></tr>
						</cfoutput>
						<cfoutput>
							<tr>
								<td class="grid_small" colspan="3" height="7"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="7"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="7"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="7"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" height="7"></td>
							</tr>
							<tr>
								<td class="grid_total" colspan="3">Grand Total</td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_total"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_total"></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_total" align="right"><strong>#LsEuroCurrencyFormat(grdTotal)#</strong></td>
								<td width="1" bgcolor="AAAAAA"></td>
								<td class="grid_small" align="right">
								<cfif grdTotalPrecedent neq 0>
									<cfset t=1-((Abs(grdTotalPrecedent)-Abs(grdTotal))/grdTotal)>
									<cfif t lt 0.01>
										N.D
									<cfelse>
										#LsNumberFormat(-100*(Abs(grdTotalPrecedent)-Abs(grdTotal))/grdTotalPrecedent,"+.0")# %
									</cfif>
								<cfelse>
									N.D
								</cfif>
								</td>
							</tr>
						</cfoutput>
					</table>
					</td></tr></table>
					</td></tr></table>
				</td>
				<!--- Graphes --->
				<td>
					<table width="100%">
						<tr valign="top">
							<!--- Colonne 1 --->
							<td width="50%">
								<table width="100%">
									<!--- Graphe 1 --->
									<tr><td width="100%">
										<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%"> 
											<tr>
												<td valign="top">
													<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														<caption align="left">Top 10 des Consommations</caption>
														<tr align="center">
														<td valign="middle" width="300" height="100" bgcolor="ffffff">
															<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
																<tr><td colspan="3" class="grid_medium">Somme du total des consommations (Remises incluses)</td></tr>
																<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																<tr>
																	<td class="grid_medium">Ligne</td>
																	<td class="grid_medium">Nom</td>
																	<td class="grid_medium">Total</td>
																</tr>
																<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																<cfset tot=0>
																<cfoutput query="dTotal">
																	<tr valign="middle">
																		<td class="grid_medium">#sous_tete#</td>
																		<td class="grid_medium">#patronyme#</td>
																		<td class="grid_medium">#LsEuroCurrencyFormat(montant_final)#</td>
																	</tr>
																	<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																	<cfset tot=tot+montant_final>
																</cfoutput>
																<tr>
																	<td class="grid_medium" colspan="2">Total</td>
																	<td class="grid_medium"><cfoutput>#LsEuroCurrencyFormat(tot)#</cfoutput></td>
																</tr>
															</table>
														</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td></tr>
								<tr><td width="100%">
										<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%"> 
											<tr>
												<td valign="top">
													<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														<caption align="left">Top 5 des Num�ros sp�ciaux</caption>
														<tr align="center">
														<td valign="middle" width="300" height="100" bgcolor="ffffff">
															<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
																<tr><td colspan="3" class="grid_medium">Somme du total des num�ros sp�ciaux Mobile</td></tr>
																<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																<tr>
																	<td class="grid_medium">Ligne</td>
																	<td class="grid_medium">Nom</td>
																	<td class="grid_medium">Total</td>
																</tr>
																<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																<cfset tot=0>
																<cfoutput query="dspeciaux">
																	<tr valign="middle">
																		<td class="grid_medium">#sous_tete#</td>
																		<td class="grid_medium">#patronyme#</td>
																		<td class="grid_medium">#LsEuroCurrencyFormat(montant_final)#</td>
																	</tr>
																	<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																	<cfset tot=tot+montant_final>
																</cfoutput>
																<tr>
																	<td class="grid_medium" colspan="2">Total</td>
																	<td class="grid_medium"><cfoutput>#LsEuroCurrencyFormat(tot)#</cfoutput></td>
																</tr>
															</table>
														</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr><td width="100%">
										<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%"> 
											<tr>
												<td valign="top">
													<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														<caption align="left">Top 5 des Roaming et International</caption>
														<tr align="center">
														<td valign="middle" width="300" height="100" bgcolor="ffffff">
															<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
																<tr><td colspan="3" class="grid_medium">Somme du total des consommations internationales</td></tr>
																<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																<tr>
																	<td class="grid_medium">Ligne</td>
																	<td class="grid_medium">Nom</td>
																	<td class="grid_medium">Total</td>
																</tr>
																<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																<cfset tot=0>
																<cfoutput query="dInter">
																	<tr valign="middle">
																		<td class="grid_medium">#sous_tete#</td>
																		<td class="grid_medium">#patronyme#</td>
																		<td class="grid_medium">#LsEuroCurrencyFormat(montant_final)#</td>
																	</tr>
																	<tr><td colspan="3" height="1" bgcolor="CCCCCC"></td></tr>
																	<cfset tot=tot+montant_final>
																</cfoutput>
																<tr>
																	<td class="grid_medium" colspan="2">Total</td>
																	<td class="grid_medium"><cfoutput>#LsEuroCurrencyFormat(tot)#</cfoutput></td>
																</tr>
															</table>
														</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								</table>
							</td>
							<!--- Colonne 2 --->
							<td width="50%">
								<table width="100%">
									<!--- Graphe 1 --->
									<tr><td width="100%">
										<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%"> 
											<tr>
												<td valign="top">
													<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														<caption align="left">Statistiques par co�ts</caption>
														<tr align="center">
														<td valign="middle" width="300" height="100" bgcolor="ffffff">
															<cfsavecontent variable="chartData">
															<cfchart 
																style="./graph/graph_pie_no_insets_vsm_euro.xml"
																chartHeight = "100"
																chartWidth = "300"
																format="png"
																showLegend="yes"
																font="Verdana"
																fontsize="9"
																>
																<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#">
																	<cfloop query="qFinale">
																		<cfif type_theme eq "Consommations">
																			<cfset v=sur_theme>
																			<cfchartdata item="#v#" value="#trim(montant_final)#">
																		</cfif>
																	</cfloop>
																</cfchartseries>
															</cfchart>
															</cfsavecontent>
															<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
															<cfoutput>#chartData#</cfoutput>
														</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td></tr>
									<!--- Graphe 2 --->
									<tr><td>
										<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%"> 
											<tr>
												<td valign="top">
													<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														<caption align="left">R�partition des Co�ts internationaux</caption>
														<tr align="center">
														<td valign="middle" width="300" height="100" bgcolor="ffffff">
															<cfsavecontent variable="chartData">
															<cfchart 
																style="./graph/graph_pie_no_insets_vsm_euro.xml"
																chartHeight = "100"
																chartWidth = "300"
																format="png"
																showLegend="yes"
																font="Verdana"
																fontsize="9"
																>
																<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#">
																	<cfloop query="qFinale">
																		<cfif type_theme eq "Consommations" AND sur_theme eq "Conso international">
																			<cfset v=theme_libelle>
																			<cfchartdata item="#v#" value="#trim(montant_final)#">
																		</cfif>
																	</cfloop>
																</cfchartseries>
															</cfchart>
															</cfsavecontent>
															<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
															<cfoutput>#chartData#</cfoutput>
														</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td></tr>
									<!--- Graphe 3 --->
									<tr><td>
										<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%"> 
											<tr>
												<td valign="top">
													<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														<caption align="left">Statistiques par volume</caption>
														<tr align="center">
														<td valign="middle" width="300" height="100" bgcolor="ffffff">
															<cfsavecontent variable="chartData">
															<cfchart 
																style="./graph/graph_pie_no_insets_vsm_euro.xml"
																chartHeight = "100"
																chartWidth = "300"
																format="png"
																showLegend="yes"
																font="Verdana"
																fontsize="9"
																>
																<cfchartseries  type="pie" paintstyle="plain" colorlist="#mygraph.GetColor('pie',1)#">
																	<cfloop query="qFinale">
																		<cfif type_theme eq "Consommations">
																			<cfset v=sur_theme>
																			<cfchartdata item="#v#" value="#trim(int(duree_appel))#">
																		</cfif>
																	</cfloop>
																</cfchartseries>
															</cfchart>
															</cfsavecontent>
															<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
															<cfoutput>#chartData#</cfoutput>
														</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td></tr>
									<!--- Graphe 4 --->
									<tr><td>
										<table cellspacing="0" align="center" class="cadre" border="0" cellpadding="0" width="100%">
											<tr>
												<td valign="middle" bgcolor="ffffff">
													<table cellspacing="0" class="menuconsoview" border="0" align="center" cellpadding="0" width="100%">
														<caption align="left">Evolution des co�ts sur les <cfoutput>#Evaluate(int(histo.recordcount/2))#</cfoutput> derniers mois</caption>
														<tr align="center">
															<td valign="middle" width="300" height="100" bgcolor="ffffff">
																<cfsavecontent variable="chartData">
																<cfchart
																 	style="./graph/graph_bar_rapport.xml"
																	chartHeight = "150"
																	chartWidth = "300"
																	format="png"
																	showLegend="yes"
																	font="Verdana"
																	fontsize="9"
																	yaxistitle="Euros"
																	>
																	<cfchartseries  type="bar" seriesColor="#mygraph.GetColor('bar',1,2)#" serieslabel="Conso.">
																		<cfloop query="histo">
																			<cfif type_theme eq "Consommations">
																				<cfchartdata item='#LsDateFormat(mois_report,"mm/yyyy")#' value='#montant_final#'>
																			</cfif>
																		</cfloop>
																	</cfchartseries>
																	<cfchartseries type="bar" seriesColor="#mygraph.GetColor('bar',1,1)#" serieslabel="Abo.">
																		<cfloop query="histo">
																			<cfif type_theme eq "Abonnements">
																				<cfchartdata item='#LsDateFormat(mois_report,"mm/yyyy")#' value='#montant_final#'>
																			</cfif>
																		</cfloop>
																	</cfchartseries>
																</cfchart>
																</cfsavecontent>
																	<cfset chartData=Replace(chartData,"http://active.macromedia.com","https://active.macromedia.com")>
																	<cfoutput>#chartData#</cfoutput>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td></tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<cfoutput>
		<!--- problemes de tailles avec les graphes --->
		<cfdocumentitem type="footer">
			<style>
			.tfooter {
				color: rgb(0 , 0, 0);
				font: normal 9px Tahoma,Arial, sans-serif;
			}
			</style>
			<center>
			<table border="0" cellpadding="0" cellspacing="0" width="560" align="center">
				<tr>
					<td align="center" class="tfooter" width="560"><font size="1">copyright CONSOTEL � 2000 / 2006 - page #cfdocument.currentpagenumber# / #cfdocument.totalpagecount#</font></td>
				</tr>
			</table>
			</center>
		</cfdocumentitem>
		
		</body>
	</html> 
	</cfoutput>
	</cfdocument>