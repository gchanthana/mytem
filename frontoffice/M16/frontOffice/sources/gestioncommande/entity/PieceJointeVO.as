package gestioncommande.entity
{
	public class PieceJointeVO
	{
		
		public var LABEL_PIECE		:String		= "";
		public var OLD_LABEL_PIECE	:String		= "";
		public var AJOUT_PAR		:String		= "";
		public var MODIF_PAR		:String		= "";
		public var FORMAT			:String		= "";
		public var ACTION			:String		= "";
		public var DATE_AJOUT_STRG	:String		= "";
		public var UUID				:String		= "";
		
		public var DATE_AJOUT_DATE	:Date		= new Date();
		public var DATE_MODIF_DATE	:Date		= new Date();
	
		public var JOINDRE			:Boolean	= true;
		public var BTN_DL_VISIBLE	:Boolean	= false;
		
		public var JOINDRE_CFVALUE	:int		= 1;
		public var IDOCMMANDE		:int		= -1;
		public var SIZE				:int		= -1;
		public var IDCLIENT			:int		= -1;
		public var ID_PIECE			:int		= -1;
		
		public function PieceJointeVO()
		{
		}

	}
}