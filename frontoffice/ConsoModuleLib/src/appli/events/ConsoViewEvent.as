package appli.events {
	import flash.events.Event;

	public class ConsoViewEvent extends Event { 
		
		// Errors Events
		public static const UNABLE_TO_CONNECT:String = "UNABLE TO CONNECT EVENT";
		public static const NO_ACCESS_EXCEPTION:String = "NO ACCESS EXCEPTION EVENT";
		
		// Process Events
		public static const GROUP_SELECTED:String = "GROUP SELECTED EVENT";
		public static const PERIMETRE_INFOS_LOADED:String = "PERIMETRE INFOS LOADED EVENT";
		public static const ACCESS_INFOS_LOADED:String = "ACCESS INFOS LOADED EVENT";
		public static const DEFAULT_UNIVERS_CHANGED:String = "DEFAULT UNIVERS EVENT";
		public static const FUNCTION_CHANGED:String = "FUNCTION EVENT";
		public static const UNIVERS_FUNCTION_CHANGED:String = "UNIVERS AND FUNCTION EVENT";
		public static const GROUP_CHANGED:String = "GROUP CHANGED EVENT";
		public static const PERIMETRE_NODE_INFOS_LOADED:String = "PERIMETRE NODE INFOS LOADED EVENT";
		public static const PERIOD_DATE_DEB_CHANGED:String = "PERIOD DATE DEB CHANGED EVENT";
		public static const PERIOD_DATE_FIN_CHANGED:String = "PERIOD DATE FIN CHANGED EVENT";
		public static const TREE_MENU_FUNCTION_CHANGED:String = "TREE MENU FUNCTION CHANGED EVENT";
		
		// RPC Operation Events
		public static const INVOKE_REMOTE_OPERATION:String = "INVOKE REMOTE OPERATION EVENT"; //by invokeOperation method
		public static const CALL_REMOTE_OPERATION:String = "CALL REMOTE OPERATION EVENT";//by call serviceOperation method
		
		public static const RESULT_REMOTE_OPERATION:String = "RESULT REMOTE OPERATION EVENT";
		public static const FAULT_REMOTE_OPERATION:String = "FAULT REMOTE OPERATION EVENT";
		public static const CANCEL_REMOTE_OPERATIONS:String = "CANCEL REMOTE OPERATIONS EVENT";
		public static const EXPIRED_SESSION:String = "SESSION EXPRIRED OPERATION EVENT";
		
		private var _message:String;		
		private var _stack:String;
		private var _detail:String;		
		private var _code:String;
		private var _date:Date;
		
		public function ConsoViewEvent(type:String,bubbles:Boolean=true) {
			super(type,bubbles);
		}
		
		override public function clone():Event
		{
			var copie:ConsoViewEvent = new ConsoViewEvent(type,bubbles);
			copie.message = message;
			copie.stack = stack;
			copie.detail = detail;
			copie.code = code;
			
			return copie;
		}		

		public function set message(value:String):void
		{
			_message = value;
		}

		public function get message():String
		{
			return _message;
		}

		public function set stack(value:String):void
		{
			_stack = value;
		}

		public function get stack():String
		{
			return _stack;
		}

		public function set detail(value:String):void
		{
			_detail = value;
		}

		public function get detail():String
		{
			return _detail;
		}

		public function set code(value:String):void
		{
			_code = value;
		}

		public function get code():String
		{
			return _code;
		}

		public function set date(value:Date):void
		{
			_date = value;
		}

		public function get date():Date
		{
			return _date;
		}
	}
}
