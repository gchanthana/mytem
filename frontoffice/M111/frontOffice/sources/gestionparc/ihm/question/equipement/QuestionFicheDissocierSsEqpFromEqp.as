package gestionparc.ihm.question.equipement
{
	import flash.display.Sprite;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasEqpAssociated;
	import gestionparc.services.dissocier.ActionDissocierServices;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;

	public class QuestionFicheDissocierSsEqpFromEqp
	{
		private var liste:IListeDispo;
		private var check:Boolean;
		private var date:Date;
		public var serv:ActionDissocierServices=new ActionDissocierServices();

		public function QuestionFicheDissocierSsEqpFromEqp(dg_liste:IListeDispo, check:Boolean, date:Date)
		{
			this.liste=dg_liste;
			this.check=check;
			this.date=date;

			var ruleEQP:IRuleDisplayItem=new HasEqpAssociated();

			if (ruleEQP.isDisplay())
				Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHEDISSOCIERSSEQPFROMEQP"), ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.NO | Alert.CANCEL, Application.application as Sprite, qnClose)
			else
				dissocierSsEqpFromEqp3()
		}

		/* LIGNE MOBILE */
		private function qnClose(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				dissocierSsEqpFromEqp1()

			else if (evt.detail == Alert.CANCEL)
			{

			}
			else
				dissocierSsEqpFromEqp2()
		}

		private function dissocierSsEqpFromEqp1():void
		{
			serv.dissocierSsEqpFromEqp1(setObj());
		}

		private function dissocierSsEqpFromEqp2():void
		{
			serv.dissocierSsEqpFromEqp2(setObj());
		}

		private function dissocierSsEqpFromEqp3():void
		{
			serv.dissocierSsEqpFromEqp3(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.iddestination=liste.getSelection().ID; 
			obj.ITEM1=liste.getSelection().EQP;
			obj.ITEM2=SpecificationVO.getInstance().elementDataGridSelected.COLLABORATEUR;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(this.date, 'YYYY/MM/DD');
			obj.IS_PRETER=this.check ? 1 : 0;
			return obj;
		}
	}
}
