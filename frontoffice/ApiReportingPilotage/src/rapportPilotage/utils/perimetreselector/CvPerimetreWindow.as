package rapportPilotage.utils.perimetreselector  {
	import flash.events.Event;
	
	import mx.containers.ViewStack;
	import mx.events.FlexEvent;

	
	public class CvPerimetreWindow extends AbstractCvPerimetreWindow {
		public var mainView:ViewStack;
		/**
		 * Si true alors la racine est toujours le noeud sur lequel
		 * on est connecté dans l'application
		 * Si false alors la racine est toujours la racine de l'arbre des périmètres
		 * */
		public var connectedNodeIsRoot:Boolean = true;
		/**
		 * Si true alors à la séléction d'un noeud l'arbre récupère les infos supplémentaires
		 * concernant le noeud en question. Lorsque ces infos sont à jour alors l'instance
		 * lance un évènement de type PerimetreTreeEvent.NODE_INFOS_RESULT.
		 * Les infos du noeud sont alors obtenues par appel à get nodeInfos()
		 * Si false alors la séléction ne fait rien et la get nodeInfos() renvoie null
		 * */
		public var loadDataOnSelection:Boolean = false;
		
		/**
		 * Si on affiche la racine ou pas
		 * */
		public var showGroupeRacine:Boolean = true;
		
		
		
		private var _restrictToOrgaOp:Boolean = false;
		/**
		 *si true alors, ne sont affichées que les organisations de type opérateur  
		 *option ignorée si la racine est affichée.
		 */
		public function get restrictToOrgaOp():Boolean
		{
			return _restrictToOrgaOp;
		}
		
		/**
		 * @private
		 */
		public function set restrictToOrgaOp(value:Boolean):void
		{
			_restrictToOrgaOp = value;
		}
		
		public function CvPerimetreWindow() {
			super();
			trace("(CvPerimetreWindow) Instance Creation");
		}
		
		override protected function commitProperties():void{
			perimetreTree.connectedNodeIsRoot = connectedNodeIsRoot;
			perimetreTree.loadDataOnSelection = loadDataOnSelection;
			perimetreTree.showGroupeRacine = showGroupeRacine;
			perimetreTree.restrictToOrgaOp = restrictToOrgaOp;
		}
		
		override protected function initIHM(event:FlexEvent):void {
			super.initIHM(event);
			trace("(CvPerimetreWindow) Performing IHM Initialization");
			
			
			
			if(event.type == FlexEvent.INITIALIZE) {
				if(perimetreTree != null) {
					perimetreTree.connectedNodeIsRoot = connectedNodeIsRoot;
					perimetreTree.loadDataOnSelection = loadDataOnSelection;
					perimetreTree.showGroupeRacine = showGroupeRacine;
					perimetreTree.restrictToOrgaOp = restrictToOrgaOp;
				}
			} else if(event.type == FlexEvent.CREATION_COMPLETE) {
				mainView.selectedIndex = 0;
				
				
				/* cici */perimetreTree.addEventListener(ConsoViewDataEvent.PERFORM_NODE_SEARCH,performSearchFromPerimetreTree);
				
				perimetreTree.addEventListener(PerimetreTreeEvent.SELECTED_ITEM_CHANGE,onPerimetreTreeChange);
				perimetreTree.addEventListener(PerimetreTreeEvent.NODE_INFOS_RESULT,onNodeInfosResult);
				
				/* cici */ searchTree.addEventListener(ConsoViewDataEvent.NODE_SEARCH_NO_RESULT,nodeSearchResult);
				/* cici */ searchTree.addEventListener(ConsoViewDataEvent.NODE_SEARCH_RESULT,nodeSearchResult);
				/* cici */searchTree.addEventListener(ConsoViewDataEvent.BACK_NODE_SEARCH,backToPerimetreTree);
				searchTree.addEventListener(SearchPerimetreWindowImpl.NODE_DD_CLICKED,SearchPerimetreWindowNodeDClickHandler);
			}
			
		}
		
		/**
		 * Retourne true si c'est l'IHM d'affichage des résultats de la
		 * recherche est affiché. Sinon retourne false
		 * */
		/* cici */
		public function isOnSearchShown():Boolean {
			return (mainView.selectedIndex == 1);
		}
		
		
		public function onPerimetreChange():void {
			if(this.initialized == true) {
				mainView.selectedIndex = 0;
				perimetreTree.onPerimetreChange();
				searchTree.onPerimetreChange();
			}
		}/* cici */
		
		protected function onPerimetreTreeChange(event:PerimetreTreeEvent):void {
			dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.SELECTED_ITEM_CHANGE));
		}
		
		public function getPerimetreTreeSelectedItem():Object 
		{
			if(mainView.selectedChild is PerimetreTreeWindowImpl)
				return perimetreTree.getSelectedItem();
			else
				return (searchTree as SearchPerimetreWindowImpl).getCurrentItemSelected()
		}
		
		protected function onNodeInfosResult(event:PerimetreTreeEvent):void {
			dispatchEvent(new PerimetreTreeEvent(PerimetreTreeEvent.NODE_INFOS_RESULT));
		}
		
		public function get nodeInfos():INodeInfos {
			return perimetreTree.nodeInfos;
		}
		
		/* cici */		
		private function performSearchFromPerimetreTree(event:ConsoViewDataEvent):void {
			mainView.selectedIndex = 0;
			searchTree.performSearch(event.data.NID,event.data.KEY);
			dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.PERFORM_NODE_SEARCH,event.data));
		}
		
		private function nodeSearchResult(event:ConsoViewDataEvent):void {
			if(mainView.selectedIndex == 0)
				mainView.selectedIndex = 1;
			
			if(event.type == ConsoViewDataEvent.NODE_SEARCH_NO_RESULT) {
				trace("CvPerimetreWindow nodeSearchResult() : PAS DE RESULTATS");
				dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.NODE_SEARCH_NO_RESULT,event.data));
			} else if(event.type == ConsoViewDataEvent.NODE_SEARCH_RESULT) {
				trace("CvPerimetreWindow nodeSearchResult() : RESULTATS");
				dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.NODE_SEARCH_RESULT,event.data));
			}
		}
		
		private function backToPerimetreTree(event:ConsoViewDataEvent):void {
			mainView.selectedIndex = 0;
			dispatchEvent(new ConsoViewDataEvent(ConsoViewDataEvent.BACK_NODE_SEARCH,event.data));
		}
		/* cici */
		
		
		//--- rajout
		private function SearchPerimetreWindowNodeDClickHandler(ev : Event):void{
			
		}
	}
}
