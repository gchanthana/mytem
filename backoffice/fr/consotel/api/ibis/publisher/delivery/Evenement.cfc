<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.Evenement" extends="fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion"
hint="Stratégie identifiée par le type Evenement qui effectue un traitement de mis à jour à partir d'une propriété ID fournie par le reporting
Cette stratégie ne sert ni à l'exécution du reporting ni au traitement de délégation correspondant (e.g : Diffusion).
Elle doit uniquement etre utilisée en tant que traitement supplémentaire à la suite d'une exécution ou d'une délégation du traitement
Le traitement de mise à jour est effectué par un composant qui implémente : fr.consotel.api.ibis.publisher.handler.IPostProcess
Le composant est instancié par cette classe à partir de la valeur d'une propriété POST_PROCESS qui est fournie par le reporting
Le type composant est alors obtenu par la concaténation : fr.consotel.api.ibis.publisher.handler.process. avec la valeur pour POST_PROCESS
Les propriétés suivantes doivent etre retournée par la méthode getDeliveryProperties() de la stratégie utilisée pour l'exécution du reporting : 
- ID : Valeur numérique utilisée pour le traitement de mise à jour
- POST_PROCESS : Suffixe à ajouter au package fr.consotel.api.ibis.publisher.handler.process pour obtenir le type du composant effectuant la mise à jour
Cette stratégie ne peut pas et ne doit pas etre utilisée pour l'exécution du reporting. Les méthodes correspondantes lèvent une exception
Cette stratégie ne dispatche pas d'évènement après l'exécution du traitement correspondant à handleReportingEvent()">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="getReportingParameters" returntype="Struct" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>

	<cffunction access="public" name="getDeliveryProperties" returntype="Struct" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>
	
	<cffunction access="public" name="getDeliveryRequest" returntype="Struct" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>
	
	<cffunction access="public" name="handleJobId" returntype="void" hint="Lève une exception">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true">
		<cfargument name="jobId" type="Numeric"  required="true">
		<cfthrow attributecollection="#getErrorProperties()#" message="Méthode non supportée par cette stratégie">
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion --->
	<cffunction access="public" name="dispatchEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.UpdateEvent">
	</cffunction>
	
	<cffunction access="private" name="deliveryEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.UpdateEvent">
	</cffunction>
	
	<cffunction access="private" name="dispatchDeliveryEvent" returntype="void"
	hint="Instancie et exécute le traitement de mise à jour identifié par event.getPostProcess() avec l'ID retourné par event.getId()
	Cette méthode a été redéfinie pour ne pas dispatcher l'évènement event un fois le traitement de mise à jour effectué
	Le traitement de mise à jour n'est pas instancié si event.getPostProcess() est une chaine vide">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent" required="true">
		<cfset var updateEvent=ARGUMENTS["event"]>
		<!--- Le traitement de mise à jour n'est effectué que lorsque UpdateEvent.getId() et UpdateEvent.getPostProcess() sont valides --->
		<cfif TRIM(updateEvent.getPostProcess()) NEQ "">
			<!--- Instancie le composant de traitement de mise à jour --->
			<cfset var updateProcessPackage="fr.consotel.api.ibis.publisher.handler.process">
			<cfset var updateProcess=createObject("component",updateProcessPackage & "." & updateEvent.getPostProcess()).getInstance()>
			<cfset executePostProcess(updateProcess,updateEvent)>
			<cfset logInfo("Evenement POST_PROCESS " & updateEvent.getPostProcess() & " COMPLETED (Report status : " & updateEvent.getStatus() & ")")>
		<cfelse>
			<cfset logInfo("Evenement no POST_PROCESS specified")>
		</cfif>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.Evenement --->
	<cffunction access="private" name="executePostProcess" returntype="void"
	hint="Appelle la méthode handleUpdateEvent() du composant de traitement de mise à jour avec updateEvent en paramètre">
		<cfargument name="updateProcess" type="fr.consotel.api.ibis.publisher.handler.IPostProcess" required="true"
		hint="Composant de traitement de mise à jour provenant de handleReportingEvent()">
		<cfargument name="updateEvent" type="fr.consotel.api.ibis.publisher.handler.event.UpdateEvent" required="true"
		hint="Evènement provenant de handleReportingEvent() et contenant les infos permettant le traitement de mise à jour">
		<cfset var postProcess=ARGUMENTS["updateProcess"]>
		<cfset postProcess.handleUpdateEvent(ARGUMENTS["updateEvent"])>
	</cffunction>
</cfcomponent>