package appli.Pilotage.CreateBannierePFGP
{
	import appli.control.AppControlEvent;
	import appli.impersonnification.event.RelogEvent;
	import appli.impersonnification.popup.PopupChooseOtherAccountIHM;
	import appli.impersonnification.refreshManager;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import general.interfaceClass.ICreateBanniere;
	
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.controls.Label;
	import mx.controls.LinkButton;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	public class CreateBannierePFGPImpl extends Canvas implements ICreateBanniere
	{
		public var lblNom:Label;
		public var lkDeco:LinkButton;
		[Bindable] public var lblChangeAccount:LinkButton;
		public var popupChooseOtherAccount:PopupChooseOtherAccountIHM = new PopupChooseOtherAccountIHM();
		[Bindable] public var hbheader:HBox;
		
		public function CreateBannierePFGPImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		public function logoff(event:Event):void
		{
			refreshManager.impersonnification = false;
			lblNom.text = "";
			lkDeco.removeEventListener(MouseEvent.CLICK,logoff);
			CvAccessManager.deconnectSession();
		}
		
		public function init(evt:FlexEvent = null):void
		{
			if (refreshManager.impersonnification == false)
				lblNom.text = "Bienvenue "+CvAccessManager.getUserObject().PRENOM+" "+CvAccessManager.getUserObject().NOM;
			else
			{
				refreshManager.currentAccountName = CvAccessManager.getUserObject().PRENOM + " " + CvAccessManager.getUserObject().NOM;
				lblNom.text = ResourceManager.getInstance().getString('M00', 'Bonjour') + " " + refreshManager.getImpersString();
				hbheader.x = 430;
			}
			//lblNom.text = "Bienvenue "+CvAccessManager.getUserObject().NOM+" "+CvAccessManager.getUserObject().PRENOM;
			lkDeco.addEventListener(MouseEvent.CLICK,logoff);
			if (CvAccessManager.getSession().USER.EMAIL.indexOf("@saaswedo.com") == -1 && refreshManager.impersonnification == false)
			{
				trace ("Suppression du label permettant de changer de compte : " + refreshManager.impersonnification);
				hbheader.removeChild(lblChangeAccount);
				hbheader.x = 605;
				lblNom.x = 550;
			}
		}
		
		public function chooseOtherAccount():void
		{
			PopUpManager.addPopUp(popupChooseOtherAccount, this, true);
			PopUpManager.centerPopUp(popupChooseOtherAccount);
			
			popupChooseOtherAccount.addEventListener(RelogEvent.LOGIN_CHOSEN, accountChosenHandler);
		}
		public function chooseOtherAccount_mouseOverHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#000000');
		}
		public function chooseOtherAccount_mouseOutHandler(evt:MouseEvent):void
		{
			evt.currentTarget.setStyle('color', '#ffffff');
		}
		private function refreshName():void
		{
			trace("refresh en cours");
			refreshManager.refreshApp();
		}
		public function accountChosenHandler(evt:Event):void
		{
			refreshName();
		}
	}
}