<cfcomponent name="gestionPerimetreBudget">
	<cffunction name="getExerciceList" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_v3.get_liste_exercices">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocresult name="p_result">
			</cfstoredproc>
			<cfreturn p_result>
	</cffunction>
			
	<cffunction name="updateExercice" returntype="query" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_budget" required="true" type="numeric" />
			<cfargument name="name" required="true" type="string" />
			<cfargument name="comment" required="true" type="string" />
			<cfargument name="min" required="true" type="string" />
			<cfargument name="max" required="true" type="string" />
			<cfargument name="budget" required="true" type="string" />
			<cfargument name="date_start" required="true" type="string" />
			<cfargument name="date_end" required="true" type="string" />
			
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.maj_exercices">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_end#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#name#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#comment#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_start#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#min#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#max#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#budget#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_budget#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfif p_retour gt 0>
					<cfset result=getExerciceList(id_groupe)/>
					<cfreturn result>
				<cfelse>
					<cfreturn -1>
			</cfif>
			
	</cffunction>
	
	<cffunction name="addExercice" returntype="any" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric"/>
			<cfargument name="name" required="true" type="string"/>
			<cfargument name="comment" required="true" type="string"/>
			<cfargument name="min" required="true" type="string"/>
			<cfargument name="max" required="true" type="string"/>
			<cfargument name="budget" required="true" type="string"/>
			<cfargument name="date_start" required="true" type="string"/>
			<cfargument name="date_end" required="true" type="string"/>
			
			
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.create_exercice">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_end#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#name#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#comment#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#date_start#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#min#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#max#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#budget#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			
			<cfif p_retour gt 0>
					<cfset result=getExerciceList(id_groupe)/>
					<cfreturn result>
				<cfelse>
					<cfreturn -1>
			</cfif>
	</cffunction>
	
	
	<cffunction name="getNodesBudget" returntype="any" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_budget" required="true" type="numeric" />
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_ana_V3.get_parent_node_budjet">
		        <cfprocparam value="#id_groupe#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#id_budget#" cfsqltype="CF_SQL_INTEGER">
		       
		        <cfprocresult name="p_result_parent">
			</cfstoredproc>
	
			<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_ana_V3.get_brothers_Node_Budget">
		        <cfprocparam value="#id_groupe#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#id_budget#" cfsqltype="CF_SQL_INTEGER">
		       
		        <cfprocresult name="p_result_brothers">
			</cfstoredproc>
	
			<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_ana_V3.get_node_budjet">
		        <cfprocparam value="#id_groupe#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#id_budget#" cfsqltype="CF_SQL_INTEGER">
		       
		        <cfprocresult name="p_result_current">
			</cfstoredproc>
			
			<cfstoredproc datasource="#session.OffreDSN#" procedure="pkg_cv_ana_V3.get_total_fils_budjet">
		        <cfprocparam value="#id_groupe#" cfsqltype="CF_SQL_INTEGER">
		        <cfprocparam value="#id_budget#" cfsqltype="CF_SQL_INTEGER">
		       
		        <cfprocresult name="p_result_child">
			</cfstoredproc>
		<cfset dataStruct = structNew()>
		<cfset datastruct[0]=p_result_parent>
		<cfset datastruct[1]=p_result_brothers>
		<cfset datastruct[2]=p_result_current>
		<cfset datastruct[3]=p_result_child>
		<cfreturn dataStruct>
	
	</cffunction>
	

	<cffunction name="updateNodeBudget" returntype="Numeric" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_budget" required="true" type="numeric" />
			<cfargument name="min" required="true" type="string" />
			<cfargument name="max" required="true" type="string" />
			<cfargument name="budget" required="true" type="string" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.maj_node_budget">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_budget#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#min#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#max#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#budget#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
	
	<cffunction name="distributeNodeBudget" returntype="Numeric" access="remote">
			<cfargument name="id_groupe" required="true" type="numeric" />
			<cfargument name="id_budget" required="true" type="numeric" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_cv_ana_V3.Reparti_Node_Budget">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_groupe#" null="false">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#id_budget#" null="false">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>
	</cffunction>
		
</cfcomponent> 