package univers.inventaire.equipements.gestionflottemobile
{
	
	
	[Bindable]
	dynamic public class  DgRechercheItemVo
	{
		private var _COLLABORATEUR:String="";
		private var _DISTRIBUTEUR:String="";
		private var _IDEMPLOYE:String="";
		private var _IDSIM:Number=0;
		private var _IDSOUS_TETE:Number=0;
		private var _IDTERMINAL:String="";
		private var _IMEI:String="";
		private var _LIGNE:String="";
		private var _MARQUE:String="";
		private var _MODELE:String="";
		private var _NUM_SIM:String="";
		private var _OPERATEUR:String="";
		private var _SELECTION_COLLAB:Boolean = false;
		private var _SELECTION_TERM:Boolean = false;
		private var _SELECTION_LIGNE:Boolean = false;
		private var _SELECTION_SIM:Boolean = false;
		private var _TERMINAL:String="";
		private var _IDCONTRAT_ABO:Number = 0;
		private var _IDCOMPTE_FACTURATION:Number = 0;
		private var _IDSOUS_COMPTE:Number = 0;
		private var _COMPTE_FACTURATION:String = "";
		private var _SOUS_COMPTE:String = "";
		private var _IDDISTRIBUTEUR:Number = 0;
		private var _IDENTIFIANT_EMPLOYE:String= "";
		private var _NIVEAU_COLLABORATEUR:String="";		
		private var _EMAIL:String= "";
		private var _DATE_ENTREE:Date=null;
		private var _DATE_SORTIE:Date=null;
		private var _CODE_INTERNE:String= "";
		private var _MATRICULE:String = "";
		private var _REFERENCE_EMPLOYE:String = "";
		private var _DATE_COMMANDE:Date = null;
		private var _NUM_CONTRAT_ABO:String;
		private var _DATE_FIN_GARANTIE:Date;
		private var _RIX_TERMINAL:Number;
		private var _NBJR_FIN:Number;
		private var _LIBELLE_TYPE_LIGNE:String;
		private var _DATE_ELLIGIBILITE:Date;
		private var _NB_JRS_FIN_LIGNE:Number;
		private var _NIVEAU_TERMINAL:String = "";
		private var _ORGA_CLIENT:String = "";
		
		  /* IDEMPLOYE:Number
		  MARQUE (null): null
		  LIGNE (null): null
		  ACCES_LAST_FACTURE (null): null
		  PRENOM_EMPLOYE (String): Nicolas
		  DATE_OUVERTURE (null): null
		  FONCTION_EMPLOYE (null): null
		  
		  
		  DISTRIBUTEUR (String): ORANGE MOBILE
		 
		  COLLABORATEUR (String): ACT DANOY NICOLAS
		  
		  
		  
		  DUREE_GARANTIE (null): null
		  DATE_FPC (null): null
		  
		  FAX (null): null
		  
		 
		
		  DATEDEB (null): null
		  DATE_ELLIGIBILITE (null): null
		  MODELE (null): null
		  IDTYPE_RACCORDEMENT (null): null
		  NIVEAU_TERM (null): null
		  IDTERMINAL (null): null
		  IDTYPE_LIGNE (null): null
		   (null): null
		  
		  COLL_POOL (String): 1
		  LIBELLE_RACCORDEMENT (null): null
		  LIGNE_POOL (String): 0
		  C4 (null): null
		  NB_JRS_FIN_LIGNE (null): null
		  C3 (null): null
		   (null): null
		   (null): null
		  DATE_COMMANDE (Date): Wed Oct 29 00:00:00 CET 2008
		  TERMINAL (null): null
		  
		  TELEPHONE_FIXE (null): null
		  STATUS_EMPLOYE (null): null
		 
		 
		 
		  C1 (null): null
		  NOM_EMPLOYE (String): ACT DANOY
		  
		  C2 (null): null
		  REF_COMMANDE_TERMINAL (String): DAN000001
		  DATE_RESILIATION (null): null
		  TERM_POOL (String): 1
		  COMMENTAIRE (null): null
		  
		  DATE_DERNIER_ENGAG (null): null
		  NUM_CONTRAT_GARANTIE (null): null
		  
		  SIM_POOL (String): 1
		  IDSIM (String): 40088
		  IDCONTRAT_GARANTIE (null): null
		  DATE_ACHAT (Date): Tue Dec 23 00:00:00 CET 2008
		   

		/*
		*******************************************************************************************************************************
		*********************************************Accesseur*************************************************************************
		*******************************************************************************************************************************
		*/
		public function get COLLABORATEUR():String
		{	
			return _COLLABORATEUR;
		}
		public function get DISTRIBUTEUR():String
		{	
			return _DISTRIBUTEUR;
		}
		public function get IDEMPLOYE():String
		{	
			return _IDEMPLOYE;
		}
		public function get IDSIM():int
		{	
			return _IDSIM;
		}
		public function get IDSOUS_TETE():int
		{	
			return _IDSOUS_TETE;
		}
		public function get IDTERMINAL():String
		{	
			return _IDTERMINAL;
		}
		public function get IMEI():String
		{	
			return (_IMEI == null)?"":_IMEI;
		}
		public function get LIGNE():String
		{	
			return _LIGNE;
		}
		public function get MARQUE():String
		{	
			return _MARQUE;
		}
		public function get MODELE():String
		{
			return (_MODELE == null)?"":_MODELE;	
		}
		public function get NUM_SIM():String
		{	
			return _NUM_SIM;
		}
		public function get OPERATEUR():String
		{	
			return _OPERATEUR;
		}
		public function get TERMINAL():String
		{	
			return _TERMINAL;
		}
		public function get SELECTION_COLLAB():Boolean
		{	
			return _SELECTION_COLLAB;
		}
		public function get SELECTION_TERM():Boolean
		{	
			return _SELECTION_TERM;
		}
		public function get SELECTION_LIGNE():Boolean
		{	
			return _SELECTION_LIGNE;
		}
		public function get SELECTION_SIM():Boolean
		{	
			return _SELECTION_SIM;
		}
		/*
		*******************************************************************************************************************************
		*********************************************MUTATEURS*************************************************************************
		*******************************************************************************************************************************
		*/
		public function set COLLABORATEUR(value : String):void
		{	
			_COLLABORATEUR=value;
		}
		public function set DISTRIBUTEUR(value : String):void
		{	
			_DISTRIBUTEUR=value;
		}
		public function set IDEMPLOYE(value : String):void
		{	
			_IDEMPLOYE=value;
		}
		public function set IDSIM(value : int):void
		{	
			_IDSIM=value;
		}
		public function set IDSOUS_TETE(value : int):void
		{	
			_IDSOUS_TETE=value;
		}
		public function set IDTERMINAL(value : String):void
		{	
			_IDTERMINAL=value;
		}
		public function set IMEI(value : String):void
		{	
			_IMEI=value;
		}
		public function set LIGNE(value : String):void
		{	
			_LIGNE=value;
		}
		public function set MARQUE(value : String):void
		{	
			_MARQUE=value;
		}
		public function set MODELE(value : String):void
		{	
			_MODELE=value;
		}
		public function set NUM_SIM(value : String):void
		{	
			_NUM_SIM=value;
		}
		public function set OPERATEUR(value : String):void
		{	
			_OPERATEUR=value;
		}
		public function set TERMINAL(value : String):void
		{	
			_TERMINAL=value;
		}
		public function set SELECTION_COLLAB(value : Boolean):void
		{	
			_SELECTION_COLLAB = value;
		}
		public function set SELECTION_TERM(value : Boolean):void
		{	
			 _SELECTION_TERM=value;
		}
		public function set SELECTION_LIGNE(value : Boolean):void
		{	
			 _SELECTION_LIGNE=value;
		}
		public function set SELECTION_SIM(value : Boolean):void
		{	
			 _SELECTION_SIM=value;
		}
		

		public function set IDCONTRAT_ABO(value:Number):void
		{
			_IDCONTRAT_ABO = value;
		}

		public function get IDCONTRAT_ABO():Number
		{
			return _IDCONTRAT_ABO;
		}

		public function set IDCOMPTE_FACTURATION(value:Number):void
		{
			_IDCOMPTE_FACTURATION = value;
		}

		public function get IDCOMPTE_FACTURATION():Number
		{
			return _IDCOMPTE_FACTURATION;
		}
		public function set IDSOUS_COMPTE(value:Number):void
		{
			_IDSOUS_COMPTE = value;
		}

		public function get IDSOUS_COMPTE():Number
		{
			return _IDSOUS_COMPTE;
		}
		public function set COMPTE_FACTURATION(value:String):void
		{
			_COMPTE_FACTURATION = value;
		}

		public function get COMPTE_FACTURATION():String
		{
			return _COMPTE_FACTURATION;
		}
		public function set SOUS_COMPTE(value:String):void
		{
			_SOUS_COMPTE = value;
		}

		public function get SOUS_COMPTE():String
		{
			return _SOUS_COMPTE;
		}

		public function set IDDISTRIBUTEUR(value:Number):void
		{
			_IDDISTRIBUTEUR = value;
		}

		public function get IDDISTRIBUTEUR():Number
		{
			return _IDDISTRIBUTEUR;
		}

		public function set IDENTIFIANT_EMPLOYE(value:String):void
		{
			_IDENTIFIANT_EMPLOYE = value;
		}

		public function get IDENTIFIANT_EMPLOYE():String
		{
			return _IDENTIFIANT_EMPLOYE;
		}

		public function set NIVEAU_COLLABORATEUR(value:String):void
		{
			_NIVEAU_COLLABORATEUR = value;
		}

		public function get NIVEAU_COLLABORATEUR():String
		{
			return _NIVEAU_COLLABORATEUR;
		}

		public function set EMAIL(value:String):void
		{
			_EMAIL = value;
		}

		public function get EMAIL():String
		{
			return _EMAIL;
		}

		public function set DATE_ENTREE(value:Date):void
		{
			_DATE_ENTREE = value;
		}

		public function get DATE_ENTREE():Date
		{
			return _DATE_ENTREE;
		}

		public function set DATE_SORTIE(value:Date):void
		{
			_DATE_SORTIE = value;
		}

		public function get DATE_SORTIE():Date
		{
			return _DATE_SORTIE;
		}

		public function set CODE_INTERNE(value:String):void
		{
			_CODE_INTERNE = value;
		}

		public function get CODE_INTERNE():String
		{
			return _CODE_INTERNE;
		}

		public function set MATRICULE(value:String):void
		{
			_MATRICULE = value;
		}

		public function get MATRICULE():String
		{
			return _MATRICULE;
		}

		public function set REFERENCE_EMPLOYE(value:String):void
		{
			_REFERENCE_EMPLOYE = value;
		}

		public function get REFERENCE_EMPLOYE():String
		{
			return _REFERENCE_EMPLOYE;
		}

		public function set DATE_COMMANDE(value:Date):void
		{
			_DATE_COMMANDE = value;
		}

		public function get DATE_COMMANDE():Date
		{
			return _DATE_COMMANDE;
		}

		public function set NUM_CONTRAT_ABO(value:String):void
		{
			_NUM_CONTRAT_ABO = value;
		}

		public function get NUM_CONTRAT_ABO():String
		{
			return _NUM_CONTRAT_ABO;
		}

		public function set DATE_FIN_GARANTIE(value:Date):void
		{
			_DATE_FIN_GARANTIE = value;
		}

		public function get DATE_FIN_GARANTIE():Date
		{
			return _DATE_FIN_GARANTIE;
		}

		public function set RIX_TERMINAL(value:Number):void
		{
			_RIX_TERMINAL = value;
		}

		public function get RIX_TERMINAL():Number
		{
			return _RIX_TERMINAL;
		}

		public function set NBJR_FIN(value:Number):void
		{
			_NBJR_FIN = value;
		}

		public function get NBJR_FIN():Number
		{
			return _NBJR_FIN;
		}

		public function set LIBELLE_TYPE_LIGNE(value:String):void
		{
			_LIBELLE_TYPE_LIGNE = value;
		}

		public function get LIBELLE_TYPE_LIGNE():String
		{
			return _LIBELLE_TYPE_LIGNE;
		}

		public function set DATE_ELLIGIBILITE(value:Date):void
		{
			_DATE_ELLIGIBILITE = value;
		}

		public function get DATE_ELLIGIBILITE():Date
		{
			return _DATE_ELLIGIBILITE;
		}

		public function set NB_JRS_FIN_LIGNE(value:Number):void
		{
			_NB_JRS_FIN_LIGNE = value;
		}

		public function get NB_JRS_FIN_LIGNE():Number
		{
			return _NB_JRS_FIN_LIGNE;
		}

		public function set NIVEAU_TERMINAL(value:String):void
		{
			_NIVEAU_TERMINAL = value;
		}

		public function get NIVEAU_TERMINAL():String
		{
			return _NIVEAU_TERMINAL;
		}

		public function set ORGA_CLIENT(value:String):void
		{
			_ORGA_CLIENT = value;
		}

		public function get ORGA_CLIENT():String
		{
			return _ORGA_CLIENT;
		}
	}
	
	
}