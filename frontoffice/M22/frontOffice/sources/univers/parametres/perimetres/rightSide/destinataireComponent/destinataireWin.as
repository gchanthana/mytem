package univers.parametres.perimetres.rightSide.destinataireComponent
{
	import composants.util.ConsoviewAlert;
	
	import event.DestinataireEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import service.destinataires.associedestinatairenoeud.AssocierDestinataireNoeudService;
	
	import univers.parametres.perimetres.main;
	
	import vo.DestinataireVo;

	public class destinataireWin extends destinataireWinIHM
	{
		private var _mainRef:main;
		public var orgaCmp:destinataireOrgaCmp=new destinataireOrgaCmp();
		public var IDNodeSource:Number;
		public var nomNode:Number;
		public var assoicerDestNoeudService:AssocierDestinataireNoeudService;
		private var value:int=0;
		private var listeIdDest:String="";

		public function get modeEcriture():Boolean
		{
			return true
		}

		public function destinataireWin()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			addEventListener(CloseEvent.CLOSE, closeWindow);
		}

		public function setMainRef(mainRef:main):void
		{
			_mainRef=mainRef;
		}
		
		public function get mainRef():main
		{
			return _mainRef;
		}

		public function getAnnuaireOrga():Number
		{
			if (_mainRef.annuaire)
			{
				return _mainRef.annuaire.IDGROUPE_CLIENT;
			}
			else
			{
				return -1
			}
		}

		protected function initIHM(e:Event):void
		{
			IDNodeSource=_mainRef.getSelectedNode();
			var nomNode:String=_mainRef.getNomSelectedNode();
			
			if (IDNodeSource == -1)
			{
				listeDestinataireRacine.actionEnMasse.dataProvider=listeDestinataireRacine.actionSupp;
				listeDestinataireRacine.labelNode.text=ResourceManager.getInstance().getString('M22', 'Liste_des_destinataires');
			}
			else
			{
				listeDestinataireRacine.actionEnMasse.dataProvider=listeDestinataireRacine.actions;
				listeDestinataireRacine.labelNode.text=ResourceManager.getInstance().getString('M22', 'Association_des_destinataires_au_noeud') +" "+ nomNode; // afficher le nom du noeud
			}
			listeDestinataireRacine.idNode=IDNodeSource;
			listeDestinataireRacine.initListDestinataire();

			btAnnuler.addEventListener(MouseEvent.CLICK, closeWindow);
			addEventListener(DestinataireEvent.ACTION_MASSE_EVENT, associerSupprimerEnMasseHandler);
			addEventListener(DestinataireEvent.ASSOCIER_DESTINATAIRES_EVENT, associerDestinataireHandler);
			
		}
		/**
		 * associer un destinataire à un noeud (association unitaire) 
		 * @param evt
		 * 
		 */		
		private function associerDestinataireHandler(evt:DestinataireEvent):void
		{
			IDNodeSource=_mainRef.getSelectedNode();
			
			if (IDNodeSource==-1)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'Veuillez_choisir_votre_noeud'),ResourceManager.getInstance().getString('M22', 'Erreur_association_destinataire_noeud'));
			}
			else
			{
				var idDestinataire:int=(listeDestinataireRacine.destinataireDataGrid.selectedItem as DestinataireVo).id;
				assoicerDestNoeudService.associerDestinataireNoeud(idDestinataire, IDNodeSource);
				// supprimer la destinataires de la liste des destinataires
				listeDestinataireRacine.dp.removeItemAt(listeDestinataireRacine.dp.getItemIndex(listeDestinataireRacine.destinataireDataGrid.selectedItem as DestinataireVo));
			}

		}

		/**
		 * assoicer ou supprimer un (ou plus ) destinataire(s) à un noeud déjà séléctionné
		 * @param e
		 */
		private function associerSupprimerEnMasseHandler(evt:DestinataireEvent):void
		{
			value=evt.target.actionEnMasse.selectedItem.value; // target = ListeDestinataireIHM

			var listeId:Array=[];

			if (listeDestinataireRacine.listeItemSelected.length > 0)
			{
				for (var i:int=0; i < listeDestinataireRacine.listeItemSelected.length; i++)
				{
					var idDest:Number=(listeDestinataireRacine.listeItemSelected.getItemAt(i) as DestinataireVo).id;
					listeId[i]=idDest;
				}

				listeIdDest=listeId.toString(); // listeIdDest.toString() convertir le tableau en chaine séparé par un ,

				if (listeId.length == 1)
				{
					var idDestinataire:int=(listeDestinataireRacine.listeItemSelected.getItemAt(0) as DestinataireVo).id;
					assoicerDestNoeudService.associerDestinataireNoeud(idDestinataire, IDNodeSource);
				}
				else if (value == 1) // association en masse 
				{
					assoicerDestNoeudService.associerPlusieursDestinataireNoeud(listeIdDest, IDNodeSource);
					// supprimer la destinataires de la liste des destinataires
					for (var ii:int=0; ii < listeDestinataireRacine.listeItemSelected.length; ii++)
					{
						listeDestinataireRacine.dp.removeItemAt(listeDestinataireRacine.dp.getItemIndex(listeDestinataireRacine.listeItemSelected.getItemAt(ii) as DestinataireVo));
					}
					listeDestinataireRacine.listeItemSelected.removeAll(); // initialiser l'array collection listeItemSelected
				}
				else if (value == 2) // suppression en masse 
				{
					var nbTotalNode:Number=0;
					var nbNode :Number=0;
					var flage:Boolean=false;
					
					// verifier si le destinataire est associé à un noeud
					for (var k:int=0; k < listeDestinataireRacine.listeItemSelected.length; k++)
					{
						nbNode=(Number)((listeDestinataireRacine.listeItemSelected.getItemAt(k) as DestinataireVo).noeud);
						
						if(nbNode==0)
						{
							flage=true;
						}
						nbTotalNode=nbTotalNode+nbNode;
					}
					
					if(nbTotalNode==0) // Si les destinataires ne sont pas associés à aucun noeud
					{
						ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M22', 'Voulez_vous_supprimez_d_finitivement_ces'), ResourceManager.getInstance().getString('M22', 'Suppression_destinataires'), alertClickHandler);
					}
					else if (nbTotalNode >0 && flage==true)//Destinataires à statut mixte (associé , non associé) 
					{
						ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M22', 'Certains_destinataires_sont_associ_s___u'), ResourceManager.getInstance().getString('M22', 'Suppression_destinataires'), alertClickHandler);
					}
					else if (nbTotalNode >0 && flage==false)
					{
						ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M22', 'Ces_destinataires_sont_associ_s___un_noe'), ResourceManager.getInstance().getString('M22', 'Suppression_destinataires'), alertClickHandler);
					}
					
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M22', 'Veuillez_choisir_votre_destinataire'));
			}
		}

		/**
		 * permet de capter la réponse de l'utilisateur lors de la suppression
		 */
		private function alertClickHandler(e:CloseEvent):void
		{
			if (e.detail == Alert.OK)
			{
				listeDestinataireRacine.supprimerDestService.supprimerPlusieursDestinataire(listeIdDest);
				listeDestinataireRacine.listeItemSelected.removeAll(); // initialiser l'array collection listeItemSelected
			}
		}

		protected function onBtAddClicked(e:Event):void
		{
			var IDNodeSource:Number=_mainRef.getSelectedNode();
			var IDNodeDest:Number=orgaCmp.getSelectedTreeItemValue();
			if (orgaCmp.getSelectedTreeIndex() < 0 || isNaN(IDNodeSource) == true)
				return;
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.parametres.perimetres.gestionDestinataire", "addDestinataire", addDestinataireResult);
			RemoteObjectUtil.callService(op, IDNodeSource.toString(), IDNodeDest.toString(), _mainRef.getSelectedDate());
		}

		protected function addDestinataireResult(e:ResultEvent):void
		{
			var re:Event=new Event("refreshDestinataire");
			dispatchEvent(re);
			closeWindow(new Event(""));
		}

		protected function closeWindow(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}
	}
}
