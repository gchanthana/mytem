package rapport.vo
{
	/*
	** class répresente un rapport 
	*/
	[Bindable]
	public class Rapport
	{
		private var _id:int;// pour l'envoyer à la base
		private var _rapportIdIHM:String;
		private var _codeRapport:String;
		private var _dossier:String;// le chemin vers le dossier du rapport 
		private var _xdo:String;// le nom du rapport dans le serveur BIP
		private var _nomLong :String;
		private var _nomCourt :String;
		private var _description :String;
		private var _moduleOrigine:String;
		private var _typeRapport:String; // standard, spécifique
		private var _segemet: String;// mobile , fixe ,data 
		private var _typeAnalyse:String;
		private var _typeAgregation:String;
		private var _urlApercuPdf :String;
		private var _partenaireId:int;
		private var _template:String;
		private var _format :String;
		private var _localization: String;
		private var _userId :int;
		private var _idRapportRacine:int;
		private var _codeApplication:String;
		
		private var _NBROWS:Number;
		
		public function Rapport(){}
			
		/*
		** permet de creer construire un rapport 
		** param: rapport : un objet de type rapport 
		** retour : non 
		*/
		
		
		public function fill(newRapport:Object):void
		{
			this._id=newRapport.IDRAPPORT;
			this._rapportIdIHM=newRapport.IDRAPPORT_IHM;
			this._codeRapport=newRapport.CODE_RAPPORT ;
			this._dossier=newRapport.DOSSIER;
			this._xdo=newRapport.XDO;
			this._nomLong=newRapport.LONG_NAME ;
			this._nomCourt=newRapport.SHORT_NAME;
			this._NBROWS=newRapport.NB_ROWS;
			this._description=newRapport.DESCRIPTION_RAPPORT;
			this._typeRapport=newRapport.TYPE_RAPPORT;
			this._segemet=newRapport.LIBELLE_SEGMENT;
			this._urlApercuPdf=newRapport.URL_APERCU;
			this._partenaireId=newRapport.PARTENAIRE_ID ;
			this._moduleOrigine=newRapport.MODULE_CONSOTELID;
			this._template=newRapport.TEMPLATE;
			this._format=newRapport.FORMAT;
			this._localization=newRapport.LOCAIZATION;
			this._userId=newRapport.USER_ID;
			this._idRapportRacine=newRapport.IDRAPPORT_RACINE;
			this._partenaireId=newRapport.PARTENAIRES_ID;
			this._codeApplication=newRapport.CODE_APPLICATION;	
		}
		
		public function get rapportId():String
		{
			return _rapportIdIHM;
		}
		
		public function set rapportId(value:String):void
		{
			_rapportIdIHM = value;
		}
		
		public function get urlApercuPdf():String
		{
			return _urlApercuPdf;
		}
		
		public function set urlApercuPdf(value:String):void
		{
			_urlApercuPdf = value;
		}
		
		public function get NBRECORD():Number
		{
			return _NBROWS;
		}

		public function set NBRECORD(value:Number):void
		{
			_NBROWS = value;
		}

		public function get description():String
		{
			return _description;
		}

		public function set description(value:String):void
		{
			_description = value;
		}

		public function get nomLong():String
		{
			return _nomLong;
		}

		public function set nomLong(value:String):void
		{
			_nomLong = value;
		}

		public function get id():int
		{
			return _id;
		}

		public function set id(value:int):void
		{
			_id = value;
		}
		
		public function get codeRapport():String
		{
			return _codeRapport;
		}
		
		public function set codeRapport(value:String):void
		{
			_codeRapport = value;
		}
		
		public function get dossier():String
		{
			return _dossier;
		}
		
		public function set dossier(value:String):void
		{
			_dossier = value;
		}
		
		public function get xdo():String
		{
			return _xdo;
		}
		
		public function set xdo(value:String):void
		{
			_xdo = value;
		}
		
		public function get nomCourt():String
		{
			return _nomCourt;
		}
		
		public function set nomCourt(value:String):void
		{
			_nomCourt = value;
		}
		
		public function get moduleOrigine():String
		{
			return _moduleOrigine;
		}
		
		public function set moduleOrigine(value:String):void
		{
			_moduleOrigine = value;
		}
		
		public function get typeRapport():String
		{
			return _typeRapport;
		}
		
		public function set typeRapport(value:String):void
		{
			_typeRapport = value;
		}
		
		public function get typeAnalyse():String
		{
			return _typeAnalyse;
		}
		
		public function set typeAnalyse(value:String):void
		{
			_typeAnalyse = value;
		}
		
		public function get typeAgregation():String
		{
			return _typeAgregation;
		}
		
		public function set typeAgregation(value:String):void
		{
			_typeAgregation = value;
		}
		
		public function get partenaireId():int
		{
			return _partenaireId;
		}
		
		public function set partenaireId(value:int):void
		{
			_partenaireId = value;
		}
		
		public function get template():String
		{
			return _template;
		}
		
		public function set template(value:String):void
		{
			_template = value;
		}
		
		public function get format():String
		{
			return _format;
		}
		
		public function set format(value:String):void
		{
			_format = value;
		}
		
		public function get localization():String
		{
			return _localization;
		}
		
		public function set localization(value:String):void
		{
			_localization = value;
		}
		
		public function get userId():int
		{
			return _userId;
		}
		
		public function set userId(value:int):void
		{
			_userId = value;
		}
		
		public function get codeApplication():String
		{
			return _codeApplication;
		}
		
		public function set codeApplication(value:String):void
		{
			_codeApplication = value;
		}
		
		public function get segemet():String
		{
			return _segemet;
		}
		
		public function set segemet(value:String):void
		{
			_segemet = value;
		}
		
		public function get idRapportRacine():int
		{
			return _idRapportRacine;
		}
		
		public function set idRapportRacine(value:int):void
		{
			_idRapportRacine = value;
		}

	}
}