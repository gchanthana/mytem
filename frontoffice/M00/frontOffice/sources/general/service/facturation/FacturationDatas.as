package general.service.facturation
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	import general.event.FacturationEvent;

	public class FacturationDatas extends EventDispatcher
	{
		private var _datasBill:ArrayCollection;
		
		public function treadDatasFacturation(objResult:ArrayCollection):void
		{
			_datasBill = new ArrayCollection();
			datasBill = objResult;

			this.dispatchEvent(new FacturationEvent(FacturationEvent.FACTURATION_RESULT));
		}
		
		public function processError(objResult:ArrayCollection):void
		{			
			this.dispatchEvent(new FacturationEvent(FacturationEvent.FACTURATION_ERROR));
		}
		
		public function get datasBill():ArrayCollection { return _datasBill; }
		
		public function set datasBill(value:ArrayCollection):void
		{
			if (_datasBill == value)
				return;
			_datasBill = value;
		}
	}
}