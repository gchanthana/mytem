<cfcomponent output="false">
		
	<cffunction access="public" output="false" name="getListeProduits" returntype="any">
		
		<cfset cfthread.idRacine = SESSION.PERIMETRE.ID_GROUPE>
		<cfset cfthread.idTheme = ''>
		<cfset cfthread.typeTheme =''>
		<cfset cfthread.idLang  = 3>

		<cfthread action="run" name="TOP1438">
			
			<cfset TOP1438.idOperator = 1438>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_liste_produit_theme">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#cfthread.idRacine#" variable="p_idracine">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#TOP1438.idOperator#" variable="p_operateurid">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#cfthread.idTheme#" variable="p_idtheme_produit" null="true">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#cfthread.typeTheme#" variable="p_type_theme">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#cfthread.idLang#" variable="p_langid">
				<cfprocresult name="TOP1438.QPRODUIT">
			</cfstoredproc>
		
		</cfthread>

		<cfthread action="run" name="TOP2159">
			
			<cfset TOP2159.idOperator = 2159>
			
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_liste_produit_theme" blockfactor="100">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#cfthread.idRacine#" variable="p_idracine">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#TOP2159.idOperator#" variable="p_operateurid">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#cfthread.idTheme#" variable="p_idtheme_produit" null="true">
				<cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#cfthread.typeTheme#" variable="p_type_theme">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#cfthread.idLang#" variable="p_langid">
				<cfprocresult name="TOP2159.QPRODUIT">
			</cfstoredproc>
		
		</cfthread>

		<cfthread action="join" name="TOP1438,TOP2159"/>
		
		<cfquery name="QFINAL" dbtype="query">
			select IDPRODUIT_CATALOGUE,	IDTHEME_PRODUIT,	LIBELLE_PRODUIT,	THEME,	TYPE_THEME from TOP1438.QPRODUIT
			union
			select IDPRODUIT_CATALOGUE,	IDTHEME_PRODUIT,	LIBELLE_PRODUIT,	THEME,	TYPE_THEME from TOP2159.QPRODUIT
			order by LIBELLE_PRODUIT,THEME,TYPE_THEME
		</cfquery>

		<!--- le resultat--->
		<cfreturn QFINAL>
	</cffunction> 
 
</cfcomponent>