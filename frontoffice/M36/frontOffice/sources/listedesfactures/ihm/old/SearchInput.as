package listedesfactures.ihm.old
{
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import composants.util.DateFunction;
	
	import flash.events.ContextMenuEvent;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class SearchInput extends SearchInputIHM
	{
		
		//private var pnListe:SearchListe = new SearchListe();
		private var type_perimetre:String;
		private var id_perimetre:int;
		private var raison_sociale:String;
		private var datedeb:Date;
		private var datefin:Date;
		private var df:DateFormatter= new DateFormatter();
		private var myContextMenu:ContextMenu;
		[Bindable]
		internal var _moisDeb : String ;//le debut de la periode 
		[Bindable]
		internal var _moisFin : String ; //la fin de la periode
		
		private var currentPanel:String="";
		
		
		public function SearchInput()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,Init);
		}
		
		override protected function commitProperties():void{
			super.commitProperties();		
		}
		
		public function Init(e:Event):void {
			myContextMenu = new ContextMenu();     
			grdFacture.contextMenu = myContextMenu;
			searchField.styleName="nofiltre";
			imFacture.addEventListener(MouseEvent.CLICK, displayFacture);
			imFactureCat.addEventListener(MouseEvent.CLICK, displayFactureCat);
			//imPDF.addEventListener(MouseEvent.CLICK,exportFacturePDF);
			imXLS.addEventListener(MouseEvent.CLICK,exportXLS);
			imCSV.addEventListener(MouseEvent.CLICK,exportCSV);
			btnSearch.addEventListener(MouseEvent.CLICK,search);
			searchField.addEventListener(FlexEvent.ENTER,search);
			searchField.addEventListener(FocusEvent.FOCUS_IN,initSearch);
			grdFacture.addEventListener(Event.CHANGE,checkFacture);
			myContextMenu.addEventListener(ContextMenuEvent.MENU_SELECT,modifyCustomMenuItems);
			grdFacture.dataProvider=null;
			imXLS.visible=imCSV.visible=false;
			
			
			type_perimetre= CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			id_perimetre= CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			raison_sociale= CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			// Gestion de la date
			mySelector.onPerimetreChange();
			initDate();
			mySelector.addEventListener("periodeChange",updatePeriode);
			setSelectedPeriode(_moisDeb,_moisFin);
			
			
			btnValiderFacture.addEventListener(MouseEvent.CLICK,ValiderFacture);

			df.formatString="DD/MM/YYYY";
			grdFacture.columns[3].labelFunction=formatDate;
			grdFacture.columns[4].labelFunction=formatDate;
			grdFacture.columns[5].labelFunction=formatDate;
			grdFacture.columns[6].labelFunction=dataGridCurrencyFormat;
			grdFacture.columns[6].sortCompareFunction=sortCompareFunc;
			removeDefaultItems()
			addCustomMenuItems();
			clearPanel();
		}
	
		
		
		private function displayListeFactures(me:MouseEvent):void{
			try {
				this.removeChildAt(1);
			} catch (e:Error) {
				
			}			
			this.addChildAt(pnResult,1);
			currentPanel="Liste";
		}
		
		private function btAnnulerClickHandler(me : MouseEvent):void{
			clearPanel();
		}
		
		private function clearPanel():void{
			try {
				this.removeChildAt(1);
			} catch (e:Error) {
				
			}
			currentPanel="";
		}
		
		private function initSearch(fe:FocusEvent):void{
			if (searchField.text=="Toutes"){
				searchField.setStyle("fontStyle","normal");
				searchField.text="";	
			}
		}
		
		private function updatePeriode(pe : PeriodeEvent):void{
			_moisDeb = pe.moisDeb;
			_moisFin = pe.moisFin;
			datedeb = pe.dateDeb;
			datefin = pe.dateFin;
			trace(pe.moisDeb,pe.moisFin);
			setSelectedPeriode(_moisDeb,_moisFin);			
		}
		
		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
															
			var formMoisD : String = moisDebut.substr(0,2) 
				 					+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
									+ moisDebut.substr(6,4);
									
			var formMoisF : String = moisFin.substr(0,2)
								   	+ " " 
								   	+ DateFunction.moisEnLettre(parseInt(moisFin.substr(3,2),10)-1) 
								   	+ " " 
								   	+ moisFin.substr(6,4);
								   	
			///////////////// faire la suite 
			myPeriodeLbl.text = "Du  " + formMoisD + " au " + formMoisF;
			
			var opData:AbstractOperation;
			opData = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.Facture.ListeFacture"+type_perimetre+"Strategy",
					"getListeOperateur", processListeOpe);
			RemoteObjectUtil.callService(opData,[id_perimetre,getDateInvdeb()]);
		}
		
		public function ValiderFacture(e:Event):void {
			var opData:AbstractOperation =
					RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.Facture.ListeFacture"+type_perimetre+"Strategy",
																"ValiderFacture",
																processValiderFacture);
			RemoteObjectUtil.callService(opData,[grdFacture.selectedItem.IDINVENTAIRE_PERIODE]);
		}
		
		public function processValiderFacture(e:ResultEvent):void {
			grdFacture.selectedItem.IDETAT_FACTURE=2;
			grdFacture.selectedItem.LIBELLE_ETAT_FACTURE="Validée";
			grdFacture.dataProvider.refresh();
		}


		private function initDate():void{
			
			var periodeArray : Array = mySelector.getTabPeriode();
		    var len : int = periodeArray.length;
		    var month : AMonth;
		    					   
			month = periodeArray[len-2];								 
			_moisDeb = month.getDateDebut();	
			_moisFin = month.getDateFin();
			
			datedeb = month.dateDebut;
			datefin = month.dateFin;
		}
		
		public function checkFacture(e:Event):void{
			if (e.currentTarget.selectedIndex==-1) {
				imFacture.visible=false;
				imFacture.toolTip="Afficher la facture";
				imFactureCat.visible=false;
				imFacture.toolTip="Afficher la première page de la facture";
				imPDF.visible=false;
				imPDF.toolTip="Exporter la facture en format PDF";
				btnValiderFacture.enabled=false;
			} else {
				imFacture.visible=true;
				imFacture.toolTip="Afficher la facture "+grdFacture.selectedItem.NUMERO_FACTURE;
				imFactureCat.visible=true;
				imFactureCat.toolTip="Afficher la première page de la facture\n"+grdFacture.selectedItem.NUMERO_FACTURE;
				imPDF.visible=true;
				imPDF.toolTip="Exporter la facture en format PDF\n"+grdFacture.selectedItem.NUMERO_FACTURE;
				if (e.currentTarget.selectedItem.IDETAT_FACTURE==1) 
					btnValiderFacture.enabled=true;
			}
		}		
	    
		public function processSession(e:ResultEvent):void { 
			var opData:AbstractOperation;
			type_perimetre= CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			id_perimetre= CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			raison_sociale= CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
			datedeb=new Date();
			datefin=new Date();
			opData = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.Facture.ListeFacture"+type_perimetre+"Strategy",
					"getListeOperateur", processListeOpe);
			RemoteObjectUtil.callService(opData,[id_perimetre,getDateInvdeb()]);
		}
		
		public function processError(e:ErrorEvent):void {
			Alert.show("erreur dans le remoting");
		}
		
		public function search(e:Event):void {
			initSearch(new FocusEvent(""));
			var opData:AbstractOperation =
					RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.Facture.ListeFacture"+type_perimetre+"Strategy",
																"rechercheFactureByNumberEtOpe",
																processSearch);
			RemoteObjectUtil.callService(opData,[searchField.text,id_perimetre,getOperateurID(),getDateInvdeb(),getDateInvfin(),getDateInvdeb()]);
		}
		
		public function processListeOpe(e:ResultEvent):void {
			updateList(e);
			updateDate(datedeb,datefin);
		}
		
		public function processSearch(e:ResultEvent):void {
			grdFacture.dataProvider = e.result;
			if (e.result.length==0) {
				imXLS.visible=imCSV.visible=false;
			} else {
				imXLS.visible=imCSV.visible=true;
				displayListeFactures(new MouseEvent(""));
			}
			displayListeFactures(null);
			//lbl.text=myrep.currentItem.NUMERO_FACTURE;
		}
		
		public function setToolTip(s:String):void {
			var chaine:String;
			var lestyle:String;
			if (getOperateurID()==0){
				chaine="Pas de filtre";
				lestyle="nofiltre";
			} else {
				chaine="Filtre: "+s;
				lestyle="filtre";
			}
			searchField.toolTip=chaine;
			searchField.styleName=lestyle;
		}
		
		private function formatDate(item:Object,column:DataGridColumn):String {
			return df.format(item[column.dataField]);
		}
		
		//formate la maonaie dans les datagrid
		private function dataGridCurrencyFormat(item:Object,column:DataGridColumn):String
		{
		    return cf.format(item[column.dataField]);
		}
		
		//trier datagrid selon les montants
		private function sortCompareFunc(itemA:Object, itemB:Object):int {
			return ObjectUtil.numericCompare(itemA.MONTANT,itemB.MONTANT);
		}		
		
		private function removeDefaultItems():void {
            myContextMenu.hideBuiltInItems();
            myContextMenu.builtInItems.print = false;
        }

        private function addCustomMenuItems():void {
            // Affichage par Catégorie de produits
            var item:ContextMenuItem = new ContextMenuItem("Première page de la facture");
            myContextMenu.customItems.push(item);
            item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, displayFactureCat);
            // Affichage d'une facture
            item = new ContextMenuItem("Afficher la Facture");
            myContextMenu.customItems.push(item);
            item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, displayFacture);
            // Export d'une facture
            item = new ContextMenuItem("Exporter le tableau vers Excel");
            myContextMenu.customItems.push(item);
            item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, exportXLS);
            // Export de toutes les factures
            /*item = new ContextMenuItem("Exporter la Facture en PDF");
            myContextMenu.customItems.push(item);
            item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, exportFacturePDF);*/
        }

		private function modifyCustomMenuItems(e:Event):void {
            if (grdFacture.selectedIndex==-1) {
            	e.currentTarget.customItems[0].enabled=false;
            	e.currentTarget.customItems[1].enabled=false;
            } else {
				e.currentTarget.customItems[0].caption="Afficher la première page de la facture " + grdFacture.selectedItem.NUMERO_FACTURE;
				e.currentTarget.customItems[0].enabled=true;
            	
				e.currentTarget.customItems[1].caption="Afficher la Facture " + grdFacture.selectedItem.NUMERO_FACTURE;
				e.currentTarget.customItems[1].enabled=true;
            }
            if (grdFacture.dataProvider.length==0){
            	e.currentTarget.customItems[2].enabled=false;
            } else {
            	e.currentTarget.customItems[2].enabled=true;
            }
        }

        private function menuSelectHandler(event:ContextMenuEvent):void {
            trace("menuSelectHandler: " + event);
        }

        private function displayFacture(event:Event):void {
            var url:String = moduleListeDesFacturesIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/display_Facture.cfm";
            var variables:URLVariables = new URLVariables();
            variables.ID_FACTURE = grdFacture.selectedItem.IDINVENTAIRE_PERIODE;
            variables.IDX_PERIMETRE = id_perimetre;
            var request:URLRequest = new URLRequest(url);
            variables.type_perimetre=type_perimetre;
            request.data = variables;
            request.method="POST";
            navigateToURL(request,"_blank");
        }

        private function displayFactureCat(event:Event):void {
			var url:String = moduleListeDesFacturesIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/gabaritFacture.cfm";
            var variables:URLVariables = new URLVariables();
            variables.ID_FACTURE = grdFacture.selectedItem.IDINVENTAIRE_PERIODE;
            variables.IDX_PERIMETRE = id_perimetre;
            
            variables.DATE_EMISSION = (grdFacture.selectedItem.DATE_EMISSION as Date).toLocaleDateString();
            variables.DATEDEB = (grdFacture.selectedItem.DATEDEB as Date).toLocaleDateString();
            variables.DATEFIN = (grdFacture.selectedItem.DATEFIN as Date).toLocaleDateString();
            variables.OPNOM = grdFacture.selectedItem.NOM;
            variables.NUMERO_FACTURE = grdFacture.selectedItem.NUMERO_FACTURE;
            
            variables.IDREF_CLIENT = grdFacture.selectedItem.IDREF_CLIENT;
            variables.LIBELLE = grdFacture.selectedItem.LIBELLE;
            variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
            
            var request:URLRequest = new URLRequest(url);
            variables.type_perimetre=type_perimetre;
            request.data = variables;
            request.method="POST";
            navigateToURL(request,"_blank");
        }
		
		
		
		private function exportXLS(event:Event):void {
            var url:String = moduleListeDesFacturesIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/exportXLS.cfm";
            var variables:URLVariables = new URLVariables();
            variables.text = searchField.text;
            variables.id_perimetre=id_perimetre;
            variables.opID=getOperateurID();
            variables.type_perimetre=type_perimetre;
            variables.datedeb=getDateInvdeb();
            variables.datefin=getDateInvfin();
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method="POST";
            navigateToURL(request,"_blank");
        }
        
		private function exportCSV(event:Event):void {
            var url:String = moduleListeDesFacturesIHM.urlBackoffice + "/fr/consotel/consoview/cfm/factures/exportCSV.cfm";
            var variables:URLVariables = new URLVariables();
            variables.text = searchField.text;
            variables.id_perimetre=id_perimetre;
            variables.opID=getOperateurID();
            variables.type_perimetre=type_perimetre;
            variables.datedeb=getDateInvdeb();
            variables.datefin=getDateInvfin();
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method="POST";
            navigateToURL(request,"_blank");
        }
		
		public function getOperateurID():int {
			return cmbsearch.selectedItem.data;
		}
		
		private function formatDateAsString(d:Date):String{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString();
			var v:String=jour+"/"+mois+"/"+annee;
			return v;
		}
		
		private function formatDateAsInverseString(d:Date):String{
			var jour:String=d.getDate().toString();
			if (jour.length==1)jour="0"+jour;
			var mois:String=(d.getMonth()+1).toString();
			if (mois.length==1) mois="0"+mois;
			var annee:String=d.getFullYear().toString();
			var v:String=annee+"/"+mois+"/"+jour;
			return v;
		}
		
		public function getDatedeb():String{
			return _moisDeb;
		}
		
		public function getDatefin():String {
			return _moisFin;
		}
		
		public function getDateInvdeb():String{
			return _moisDeb;
		}
		
		public function getDateInvfin():String {
			return _moisFin;
		}
		
		public function updateList(e:ResultEvent):void{
			cmbsearch.dataProvider=e.result;
			cmbsearch.dataProvider.addItemAt({label:"Tout opérateur",data:0},0);
			cmbsearch.selectedIndex=0;
		}
		
		public function updateDate(db:Date,df:Date):void{
			//date_debut.data=db;
			//date_fin.data=df;
		}
	}
}