package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets
{
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.controls.Alert;
	
	import univers.inventaire.inventaire.export.IExportable;

	public class CommandeExportable implements IExportable
	{
		private static const URL_EXPORT : String = "/fr/consotel/consoview/cfm/cycledevie/demandes/Display_Demande.cfm";
		
		private var _commande : Commande;
		private var _panier : ElementCommande;
					
		/**
		 * Constructeur
		 * @param commandeId l'identifiant de la commande
		 * */
		public function CommandeExportable(commande : Commande,panier : ElementCommande)
		{			
			_commande = commande;
			_panier = panier;
		}
		
		
		/**
		 * Affiche la commande dans une nouvelle fenetre au format passé en paramètre
		 * @param format 
		 * */
		public function exporter(format:String="PDF"):void
		{
						
			if (_panier != null)
				if (_panier.liste != null){
					var url:String = moduleCommandeMobileIHM.NonSecureUrlBackoffice + "/fr/consotel/consoview/cfm/cycledevie/demandes/Display_Demande.cfm";
		            var variables:URLVariables = new URLVariables();
		            
		            variables.FORMAT = format;	             
		            variables.TYPE = _commande.typeCommande;
					variables.PERIMETRE_LIBELLE = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
					//infos commandes
					
					variables.LIBELLE = _commande.libelle;
					variables.REF_CLIENT = (_commande.refClient != null)?_commande.refClient:"---";				
					variables.REF_OPERATEUR =( _commande.refOperateur != null)?_commande.refOperateur:"---";					
					variables.CMDE_COMMENTAIRES = ( _commande.commentaire != null)?_commande.commentaire:"---";					
					variables.DATE_EFFECTIVE = (_commande.dateEffective.length > 10)?DateFunction.formatDateAsString(new Date(_commande.dateEffective)):_commande.dateEffective;
					variables.DATE_LIVRAISON_PRE = (_commande.dateLivraisonPrevue.length > 10)?DateFunction.formatDateAsString(new Date(_commande.dateLivraisonPrevue)):_commande.dateLivraisonPrevue;
		 			variables.DATE_LIVRAISON = (_commande.dateLivraison != null)?DateFunction.formatDateAsString(new Date(_commande.dateLivraison)):" ";
					
					//operateur
					variables.CONTACTID = _commande.contactID;
					variables.OPERATEURID = _commande.operateurID;				
					variables.SOCIETEID = _commande.societeID;
				
					variables.FLAG_CIBLE = _commande.flagCible;
					variables.CIBLE = _commande.idGroupeCible;

					variables.LI_PRODUITS = ConsoviewUtil.extraireColonne(_panier.liste,"libelleProduit");			
					variables.LI_LIGNES = ConsoviewUtil.extraireColonne(_panier.liste,"libelleLigne");
					variables.LI_THEMES = ConsoviewUtil.extraireColonne(_panier.liste,"libelletheme");
					variables.LI_COMS = ConsoviewUtil.extraireColonne(_panier.liste,"commentaire");	
					var request:URLRequest = new URLRequest(url);
		            request.data = variables;
		            request.method=URLRequestMethod.POST;
		            
		            navigateToURL(request,"_blank");
				}else{
					Alert.show("la demande n'a pas de produit");
				}
			else
				Alert.show("la demande n'a pas de produit");                 
		}		
	}
}