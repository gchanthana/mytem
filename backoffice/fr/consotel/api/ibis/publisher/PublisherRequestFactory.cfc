<cfcomponent displayname="fr.consotel.api.ibis.publisher.PublisherRequestFactory" hint="Factory for BipRequest">
	<cffunction access="public" name="getFactory" returntype="fr.consotel.api.ibis.publisher.PublisherRequestFactory" hint="Returns an instanciated factory">
		<cfset VARIABLES["REQUEST_CLASS"]="fr.consotel.api.ibis.publisher.BIPRequest">
		<cftrace type="information" text="getFactory() - Instance class is #VARIABLES.REQUEST_CLASS#">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="createRequest" returntype="fr.consotel.api.ibis.publisher.PublisherRequest" hint="Returns an instance of BIPRequest">
		<cftrace type="information" text="createRequest()">
		<cfif NOT structKeyExists(VARIABLES,"REQUEST_CLASS")>
			<cfthrow type="Custom" message="Unable to create request due to invalid factory. Check if this instance was create from getFactory()">
		</cfif>
		<cfreturn createObject("component",VARIABLES["REQUEST_CLASS"]).selfFactoryMethod(structNew())>
	</cffunction>
</cfcomponent>