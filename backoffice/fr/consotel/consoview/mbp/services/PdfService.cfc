<cfcomponent output="false">
	<cfset URLCGV = "https://#CGI.HTTP_HOST#/modular.pdf">
	<cfset URLGT  = "https://#CGI.HTTP_HOST#/modular.pdf">

	<cffunction name="getCGV" access="remote" returntype="String">
		<cfreturn URLCGV>
	</cffunction>
	
	<cffunction name="getGT" access="remote" returntype="String">
		<cfreturn URLGT>
	</cffunction>
</cfcomponent>