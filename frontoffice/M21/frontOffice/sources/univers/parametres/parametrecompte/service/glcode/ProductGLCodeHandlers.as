package univers.parametres.parametrecompte.service.glcode
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	public class ProductGLCodeHandlers
	{
		private var myDatas : ProductGLCodeDatas;
		public function ProductGLCodeHandlers(objData:ProductGLCodeDatas)
		{
			this.myDatas = objData;
		}
		
		public function getListProductCodeHandler(re:ResultEvent):void
		{
			if(re.result != null)
			{
				myDatas.treatListProductCode(re.result);				
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Erreur'),ResourceManager.getInstance().getString('M21', 'Erreur'));
			}
		} 
		
		public function affecterProductCodeHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.IDGLCODE > 0)
			{
				myDatas.treatAffecterProductCode(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Erreur'),ResourceManager.getInstance().getString('M21', 'Erreur'));
			}
		}
		
		public function affecterPlusieursElementsHandler(re:ResultEvent):void
		{
			if(re.result != null && re.result.IDGLCODE > 0)
			{
				myDatas.treatAffecterPlusieursElements(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M21', 'Erreur'),ResourceManager.getInstance().getString('M21', 'Erreur'));
			}
		}
	}
}