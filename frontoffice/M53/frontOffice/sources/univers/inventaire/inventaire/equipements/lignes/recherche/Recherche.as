package univers.inventaire.equipements.lignes.recherche {
	import flash.events.Event;
	import mx.events.FlexEvent;
	import fr.consotel.flex.utils.tree.ConsoViewCommunesTree;
	import fr.consotel.flex.utils.tree.ConsoViewCommunesTreeEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.flex.utils.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.flex.utils.ErrorHandlerUtil;
	import fr.consotel.flex.consoview.ConsoViewMXMLApplication;
	import mx.rpc.events.ResultEvent;
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import fr.consotel.flex.utils.ConsotelIdFormatter;
	
	public class Recherche extends RechercheIHM {
		private var session:Object;
		private var gridSitesDataProvider:ArrayCollection;
		private var gridLignesDataProvider:ArrayCollection;
		
		public function Recherche() {
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:Event):void {
			//session = ConsoViewMXMLApplication.getApplicationSession();
			// Arbre des communes
			/*sitesTree.addEventListener(ConsoViewCommunesTreeEvent.GROUPE_EVENT_TYPE,
					onSitesTreeGroupeClick);
			sitesTree.addEventListener(ConsoViewCommunesTreeEvent.SOCIETE_EVENT_TYPE,
					onSitesTreeSocieteClick);
			sitesTree.addEventListener(ConsoViewCommunesTreeEvent.ZIP_EVENT_TYPE,
					onSitesTreeZipClick);
			sitesTree.addEventListener(ConsoViewCommunesTreeEvent.COMMUNE_EVENT_TYPE,
					onSitesTreeCommuneClick);*/
			// Grid des sites
			gridSites.addEventListener(Event.CHANGE,onGridSitesChange);
			gridSitesAdressColumn.labelFunction = formatGridSitesAdress;
			// Bouton de recherche de sites
			searchSites.addEventListener(MouseEvent.CLICK,findSite);
			// Grid des lignes
			gridLignesSousTeteColumn.labelFunction = formatGridLignesSousTete;
			gridLignes.addEventListener(Event.CHANGE,dispatchGridLignesChangeEvent);
			// Filtre Texte sur les lignes
			lignesSearchText.addEventListener(Event.CHANGE,filterLignesBySearch);
		}

		/**
		 * Handler appelé lorsqu'on clique sur la racine (Groupe) de l'arbre des communes.
		 * Le handler requête les données nécessaires pour mettre à jour le contenu du grid
		 * des sites. Puis une fois ces données récupérées, la méthode populateGridSites est
		 * appelée pour mettre à jour le contenu du grid des sites.
		 * */
	   /*private function onSitesTreeGroupeClick(event:ConsoViewCommunesTreeEvent):void {
			lignesTitleGridLignes.visible = false;
			sitesSearchText.text = lignesSearchText.text = "";
			gridLignes.dataProvider = gridLignesDataProvider = null;
			var perimetreIndex:int = session[ConsoViewMXMLApplication.PERIMETRE_INDEX];
			var sitesTreeGroupeOp:AbstractOperation =
						RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.annuaire.site."+session.TYPE_PERIMETRE+"ListeSitesProviderStrategy",
																	"getSitesParGroupe",
																	populateGridSites,ErrorHandlerUtil.onFault);
			RemoteObjectUtil.callService(sitesTreeGroupeOp,perimetreIndex);
	   }*/
	
		/**
		 * Handler appelé lorsqu'on clique sur une société de l'arbre des communes.
		 * Le handler requête les données nécessaires pour mettre à jour le contenu du grid
		 * des sites. Puis une fois ces données récupérées, la méthode populateGridSites est
		 * appelée pour mettre à jour le contenu du grid des sites.
		 * */
	  /* private function onSitesTreeSocieteClick(event:ConsoViewCommunesTreeEvent):void {
			lignesTitleGridLignes.visible = false;
			sitesSearchText.text = lignesSearchText.text = "";
			gridLignes.dataProvider = gridLignesDataProvider = null;
			var perimetreIndex:int = session[ConsoViewMXMLApplication.PERIMETRE_INDEX];
			var sitesTreeSocieteOp:AbstractOperation =
						RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.annuaire.site."+session.TYPE_PERIMETRE+"ListeSitesProviderStrategy",
																	"getSitesParRefClient",
																	populateGridSites,ErrorHandlerUtil.onFault);
			RemoteObjectUtil.callService(sitesTreeSocieteOp,perimetreIndex,event.getNodeId());
	   }*/
		
		/**
		 * Handler appelé lorsqu'on clique sur un département de l'arbre des communes.
		 * Le handler requête les données nécessaires pour mettre à jour le contenu du grid
		 * des sites. Puis une fois ces données récupérées, la méthode populateGridSites est
		 * appelée pour mettre à jour le contenu du grid des sites.
		 * */
	 /*  private function onSitesTreeZipClick(event:ConsoViewCommunesTreeEvent):void {
			lignesTitleGridLignes.visible = false;
			sitesSearchText.text = lignesSearchText.text = "";
			gridLignes.dataProvider = gridLignesDataProvider = null;
			var perimetreIndex:int = session[ConsoViewMXMLApplication.PERIMETRE_INDEX];
			var zip:String = event.getZip();
			var sitesTreeZipOp:AbstractOperation =
						RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.annuaire.site."+session.TYPE_PERIMETRE+"ListeSitesProviderStrategy",
																	"getSitesParZip",
																	populateGridSites,ErrorHandlerUtil.onFault);
			if(zip == "006") // Pour fonctionner avec la version 2.2 de des requêtes de ConsoView
				RemoteObjectUtil.callService(sitesTreeZipOp,perimetreIndex,event.getNodeId(),"-1");
			else
				RemoteObjectUtil.callService(sitesTreeZipOp,perimetreIndex,event.getNodeId(),zip);
	   }
		*/
		/**
		 * Handler appelé lorsqu'on clique sur une commune de l'arbre des communes.
		 * Le handler requête les données nécessaires pour mettre à jour le contenu du grid
		 * des sites. Puis une fois ces données récupérées, la méthode populateGridSites est
		 * appelée pour mettre à jour le contenu du grid des sites.
		 * */
	/*   private function onSitesTreeCommuneClick(event:ConsoViewCommunesTreeEvent):void {
			lignesTitleGridLignes.visible = false;
			sitesSearchText.text = lignesSearchText.text = "";
			gridLignes.dataProvider = gridLignesDataProvider = null;
			var perimetreIndex:int = session[ConsoViewMXMLApplication.PERIMETRE_INDEX];
			var zip:String = event.getZip();
			var commune:String = event.getCommune();
			var sitesTreeCommuneOp:AbstractOperation =
						RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.annuaire.site."+session.TYPE_PERIMETRE+"ListeSitesProviderStrategy",
																	"getSitesParVille",
																	populateGridSites,ErrorHandlerUtil.onFault);
			if(zip == "006") // Pour fonctionner avec la version 2.2 de des requêtes de ConsoView
				RemoteObjectUtil.callService(sitesTreeCommuneOp,perimetreIndex,event.getNodeId(),"-1","Communes|COMMUNE");
			else
				RemoteObjectUtil.callService(sitesTreeCommuneOp,perimetreIndex,event.getNodeId(),zip,commune + "|COMMUNE");
	   }
	   */
		/**
		 * Recherche un site lorsqu'on clique sur le bouton Chercher de la zone de recherche
		 * de sites. Cette méthode effectue alors la requête puis une fois les données
		 * récupérées le contenu du grid des sites est mis à jour par la fonction populateGridSites.
		 * */
		private function findSite(event:Event):void {
			lignesTitleGridLignes.visible = false;
			sitesTree.selectedItem = gridLignes.dataProvider = gridLignesDataProvider = null;
			lignesSearchText.text = lignesTitleGridLignes.text = "";
			var perimetreIndex:int = session[ConsoViewMXMLApplication.PERIMETRE_INDEX];
			var findSitesOp:AbstractOperation =
						RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.annuaire.site."+session.TYPE_PERIMETRE+"ListeSitesProviderStrategy",
																	"findSite",
																	populateGridSites,ErrorHandlerUtil.onFault);
			RemoteObjectUtil.callService(findSitesOp,perimetreIndex,sitesSearchText.text);
		}
	   
		/**
		 * Rempli et met à jour le contenu du grid des sites.
		 * Il met aussi à jour le libéllé de la zone de recherche des sites.
		 * */
		private function populateGridSites(event:ResultEvent):void {
			gridSitesDataProvider = event.result as ArrayCollection;
			gridSites.dataProvider = gridSitesDataProvider;
			sitesTitle.text = "Total sites : " + gridSites.dataProvider.length;
		}
		
		/**
		 * Handler appelé lorsqu'on clique sur une ligne du grid des sites. Le handler requête
		 * alors la liste des lignes du site séléctionné. une fois les données récupérées, la
		 * méthode populateGridLignes est appélée pour remplir et mettre à jour le contenu du
		 * grid des lignes.
		 * */
		private function onGridSitesChange(event:Event):void {
			var siteId:int = gridSites.selectedItem.SITEID as int;
			var perimetreIndex:int = session[ConsoViewMXMLApplication.PERIMETRE_INDEX];
			lignesSearchText.text = "";
			gridLignes.dataProvider = gridLignesDataProvider = null;
			var gridSitesGetLignesOp:AbstractOperation =
								RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.annuaire.site."+session.TYPE_PERIMETRE+"ListeSitesProviderStrategy",
																			"getLignesFromSite",
																			populateGridLignes,ErrorHandlerUtil.onFault);
			RemoteObjectUtil.callService(gridSitesGetLignesOp,perimetreIndex,siteId);
		}
		
		/** Rempli et met à jour le contenu du grid des lignes **/
		private function populateGridLignes(event:ResultEvent):void {
			gridLignesDataProvider = event.result as ArrayCollection;
			gridLignes.dataProvider = gridLignesDataProvider;
			lignesTitleGridLignes.text = "Ligne(s) trouvée(s) : " + gridLignes.dataProvider.length;
			lignesTitleGridLignes.visible = true;
		}
		
		/**
		 * Handler qui filtre le contenu du grid des lignes lorsqu'on entre une chaîne dans
		 * l'entrée du filtre des lignes (TextInput).
		 * */
		private function filterLignesBySearch(event:Event):void {
	   	var arrDisplay:ArrayCollection = new ArrayCollection();
		   var fortext:String = lignesSearchText.text.toLowerCase();
	  		var i:int = 0;
	  		var soustete:String;
	  		var commentaires:String;
	  		var type:String;
	   	for(i = 0; i < gridLignesDataProvider.length; i++) {
	   		soustete = gridLignesDataProvider[i]['SOUS_TETE'] != null ?
	   								gridLignesDataProvider[i]['SOUS_TETE'] : "";
	   		commentaires = gridLignesDataProvider[i]['COMMENTAIRES'] != null ?
	   								gridLignesDataProvider[i]['COMMENTAIRES'] : "";
	   		type = gridLignesDataProvider[i]['LIBELLE_TYPE_LIGNE'] != null ?
	   								gridLignesDataProvider[i]['LIBELLE_TYPE_LIGNE'] : "";
				if(soustete.toLowerCase().indexOf(fortext) != -1 ||
						commentaires.toLowerCase().indexOf(fortext) != -1 ||
						type.toLowerCase().indexOf(fortext) != -1) {
					arrDisplay.addItem(gridLignesDataProvider[i]);
		  		}
	   	}
	   	gridLignes.dataProvider = arrDisplay;
			lignesTitleGridLignes.text = "Ligne(s) trouvée(s) : " + gridLignes.dataProvider.length;
		}
		
		/** Fonction de formatage de la colonne ADRESSE du grid des sites **/
		private function formatGridSitesAdress(item:Object,column:DataGridColumn):String {
			var str:String = "";
			if (item.NOM_SITE != null) {
				str = str + item.NOM_SITE + '\n';
			}
			if (item.ADRESSE1 != null && item.ADRESSE1 != 'Flotte Mobile') {
				str = str + item.ADRESSE1 + '\n';
			}
			if (item.ADRESSE2 != null) {
				str = str + item.ADRESSE2 + '\n';
			}
			if (item.ZIPCODE != null && item.ZIPCODE != '006') {
				str = str + item.ZIPCODE + ' ';
			}
			if (item.COMMUNE != null && item.ZIPCODE != '006') {
				str=str + item.COMMUNE;
			}
			return str;
		}
		
		/** Fonction de formatage de la colonne LIGNE du grid des lignes */
		private function formatGridLignesSousTete(item:Object,column:DataGridColumn):String {
			var str:String = "";
			if (item.SOUS_TETE.length == 10) {
				//str = ConsotelIdFormatter.formatSousTete(item.SOUS_TETE);
			} else { 
				str = item.SOUS_TETE;
			}
			return str;
		}
		
		private function dispatchGridLignesChangeEvent(event:Event):void {
			var selectedObject:Object = gridLignes.selectedItem;
			gridLignes.selectedItem = null;
			dispatchEvent(new RechercheEvent(selectedObject));
		}
	}
}
