package composants.util.sav
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class SavEquipementUtil extends EventDispatcher
	{
		private var _ticketSav : Object;
		private var _tmpValues:Ticket;
		
		public function SavEquipementUtil() 
		{
		}
		public static const MAJINFOSSAV:String = "majInfosSavSucced";
		
		public function createSAV(obj_ticket : Object):void
		{
			
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.utils.savEquipement",
							"createSAV", 
							function createSAVResultHandler(event:ResultEvent):void
							{
								if(event.result > 0)
								{
									ConsoviewAlert.afficherOKImage("Informations sauvegardées");
									dispatchEvent(new Event(MAJINFOSSAV));
									
								}
								else
								{
									Alert.show("Erreur lors de la sauvegarde","Erreur");									
								}
							}
							);
			RemoteObjectUtil.callService(opData,obj_ticket.idequipement,
												obj_ticket.idcontrat,
												obj_ticket.date_deb,
												obj_ticket.description,
												obj_ticket.garantie_applicable,
												obj_ticket.idfournisseur,
												obj_ticket.idsite,
												obj_ticket.iduser,
												obj_ticket.idemploye,
												obj_ticket.commentaires,
												obj_ticket.numero_sav
												
												);
		}
		public function update(obj_ticket : Object):void
		{
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.utils.savEquipement",
							"createSAV", 
							function updateSAVResultHandler(event:ResultEvent):void
							{
								if(event.result > 0)
								{
									ConsoviewAlert.afficherOKImage("Informations modifiées");
									dispatchEvent(new Event(MAJINFOSSAV));
									
								}
								else
								{
									Alert.show("Erreur lors de la sauvegarde","Erreur");									
								}
							}
							);
			RemoteObjectUtil.callService(opData,obj_ticket.idsav,
												obj_ticket.idequipement,
												obj_ticket.idcontrat,
												obj_ticket.date_deb,
												obj_ticket.description,
												obj_ticket.garantie_applicable,
												obj_ticket.idfournisseur,
												obj_ticket.idsite,
												obj_ticket.iduser,
												obj_ticket.commentaires,
												obj_ticket.numero_sav
											
												);
		}
		public function set selectedTicket(value:Ticket):void
		{
			_tmpValues = value;
		}

		public function get selectedTicket():Ticket
		{
			return _tmpValues;
		}
		public function set tmpValues(value:Ticket):void
		{
			_tmpValues = value;
		}

		public function get tmpValues():Ticket
		{
			return _tmpValues;
		}

	}
}