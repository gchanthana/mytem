<cfcomponent displayname="Gestion des compte de facturations dans le module Gestion des groupes" output="false">
<cffunction name="getListeCF" returntype="Query" output="false"access="remote">
	<cfargument name="idgroupe_client" type="numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getListeCF">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idgroupe_client#">
		<cfprocresult name="result"/>
	</cfstoredproc>
	<cfreturn result>
</cffunction>
<cffunction name="updateCF" returntype="Numeric" output="false"access="remote">
	<cfargument name="oldIdrefClient" type="String">
	<cfargument name="oldNumero_correspond" type="String">
	<cfargument name="oldIdOperateur" type="Numeric">
	<cfargument name="idrefClient" type="String">
	<cfargument name="numero_correspond" type="String">
	<cfargument name="idOperateur" type="Numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.updcorresp_numero_compte">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#oldIdrefClient#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#oldNumero_correspond#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#oldIdOperateur#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idrefClient#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numero_correspond#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idOperateur#">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result">
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="deleteCF" returntype="Numeric" output="false"access="remote">
	<cfargument name="p_idref_client" type="numeric">
	<cfargument name="p_operateurID" type="numeric">
	<cfargument name="p_numero_compte" type="String">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.delcorresp_numero_compte">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_idref_client#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_operateurID#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_numero_compte#">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result">
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="addCF" returntype="Numeric" output="false"access="remote">
	<cfargument name="p_idref_client" type="numeric">
	<cfargument name="p_operateurID" type="numeric">
	<cfargument name="p_numero_compte" type="String">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.addcorresp_numero_compte">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_idref_client#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_operateurID#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_numero_compte#">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result">
	</cfstoredproc>
<cfreturn result>
</cffunction>
</cfcomponent>