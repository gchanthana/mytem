package catalogue.ihm.parametre
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.TabNavigator;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.ProgressBar;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.graphics.codec.JPEGEncoder;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;
	
	import catalogue.service.upload.UploadFileService;
	import catalogue.util.Formator;
	import catalogue.vo.EquipementCatalogueVO;
	import catalogue.vo.FileUpload;
	
	import composants.util.ConsoviewAlert;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	
	public class ImageToolsImpl extends TabNavigator
	{
		public static const ADR_IMG_SERVER		:String = 'http://images.consotel.fr';
		public static const TYPE_LOW		:String = 'LOW';
		public static const TYPE_THUMB		:String = 'THUMB';
		[Bindable]
		[Embed(source="/assets/images/ok.png", mimeType='image/png')]
		public var imgok:Class;
		
		[Bindable]
		[Embed(source="/assets/images/notok.png", mimeType='image/png')]
		public var imgnotok:Class;
		
		//public static const NO_PHOTO			:String = '/NoImage.png'; 
		private var uploadServ				:UploadFileService;
		private var _fileRef				:FileReference;
		private var _imageTypes				:FileFilter;
		private var lowFile200				:FileUpload;
		private var thumbFile75				:FileUpload;
		private var _filesToUplaod			:ArrayCollection = new ArrayCollection();
		private var _equipement				:EquipementCatalogueVO = new EquipementCatalogueVO();
		
		public var btn_browse				:Button;
		public var btn_removeImg			:Button;

		public var lbl_msg					:Label;
		public var progressBarUpload		:ProgressBar;
		[Bindable]public var img_upload		:Image;
		[Bindable]public var img_law200		:Image;
		[Bindable]public var img_tumb75		:Image;
		[Bindable]public var img_law200use	:Image;
		[Bindable]public var img_tumb75use	:Image;
	    [Bindable]public var imgVisible		:Boolean = false;
		[Bindable]public var msgVisible		:Boolean = false;
		public var btn_searchLibrary		:Button;
		public var ti_search				:TextInput;
		public var pdg_library				:DataGrid;
		public var ti_filtre				:TextInput;
		public var btn_uploadImg			:Button;
		public var hb_imageLibrary			:HBox;
		public var hb_imageUpload			:HBox;
		public var myImgResult				:Image;
		
		public const NBITEMPARPAGE 			:int = 3000;
		private var _startIndex 			:int=1;
		private var _listeEquipementLibrary	:ArrayCollection = new ArrayCollection;
		
		private var _itemCurrent			:Object = null;
		
		private var aucunEquip :String = ResourceManager.getInstance().getString('M24', 'Aucun_image_associ_');
		[Bindable]
		public var labelSelectedEquip		:String = aucunEquip;
		
		private var _UUID			:String = "";
		
		public function ImageToolsImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, init)
		}
		
		protected function init(event:FlexEvent):void
		{
			uploadServ = new UploadFileService();
			_fileRef = new FileReference();
			// Instantiate a new FileFilter.
			_imageTypes = new FileFilter("Images (*.jpg, *.jpeg)", "*.jpg; *.jpeg");
			
			initListeners();
			initData();
		}
		
		private function initData():void
		{
			if(this.selectedIndex == 0)
			{
				if(equipement.PATH_LOW != '' && equipement.PATH_THUMB != '')
				{
					if(equipement.PATH_THUMB == '/NoImage.png')//
					{
						// to disable image scrolling
						img_tumb75use.width = img_tumb75use.height = 75;
						img_law200use.width = img_law200use.height = 200; 
					}
					img_law200use.source = ImageToolsImpl.ADR_IMG_SERVER + equipement.PATH_LOW;
					img_tumb75use.source = ImageToolsImpl.ADR_IMG_SERVER + equipement.PATH_THUMB;
					
					labelSelectedEquip = '';
				}
			}
		}
		
		private function initListeners():void
		{
			// Set the event listeners on the FileReference ojbect.
			_fileRef.addEventListener(Event.OPEN, openHandler);
			_fileRef.addEventListener(Event.SELECT, selectHandler);
			_fileRef.addEventListener(Event.COMPLETE, completeHandler);
			_fileRef.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			_fileRef.addEventListener(IOErrorEvent.IO_ERROR, uploadIoErrorHandler);
			
		}
		
		public function uploadImagesHandler(event:MouseEvent):void
		{
			if(filesToUplaod.length == 2)
			{
				uploadServ.uploadFiles(filesToUplaod.source);
				uploadServ.addEventListener('FILES_UPLOADED', uploadImageHandler);
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M24','Please_upload_an_image_or_select_one_from_library_') ,ResourceManager.getInstance().getString('M24','Erreur'), null);
			}
		}
		
		public function uploadImageHandler(evt:Event):void
		{
			btn_uploadImg.enabled = false;
			equipement.imageByUser = 1;
			myImgResult.source = imgok;
			lbl_msg.text = ResourceManager.getInstance().getString('M24','Image_charg__avec_succ_s_')
			msgVisible = true;
			
			uploadServ.removeEventListener('FILES_UPLOADED', uploadImageHandler);
		}
		
		protected function onRemoveHandler(event:MouseEvent):void
		{
			imgVisible = false;
			progressBarUpload.label = '';
			
			equipement.PATH_LOW = '';
			equipement.PATH_THUMB = '';
		}
		
		protected function imageIOErrorHandler(event:IOErrorEvent):void {
			progressBarUpload.label = ResourceManager.getInstance().getString('M24','Erreur');
			img_upload.source = null;
		}
		
		protected function uploadIoErrorHandler(evt:IOErrorEvent):void
		{
			progressBarUpload.label = ResourceManager.getInstance().getString('M24','Erreur');
			//Alert.show("IO Error: " + evt.toString());
			
		}
		
		protected function progressHandler(evt:ProgressEvent):void
		{
			progressBarUpload.setProgress(evt.bytesLoaded, evt.bytesTotal);		
		}
		
		protected function openHandler(event:Event):void
		{
			imgVisible = true;
			msgVisible = false;
			//progressBarUpload.visible = true;
			
		}
		
		/* get UUID */
		private function getUuid():void
		{
			uploadServ.createUUID();
			uploadServ.addEventListener("FILE_UUID_CREATED", uuidHandler);
		}
		
		/* get UUID crée - Handler */
		private function uuidHandler(pje:Event):void
		{
			_UUID = uploadServ.UUID; // UUID affilié à la PJ
			uploadServ.removeEventListener("FILE_UUID_CREATED", uuidHandler);
		}
		
		protected function completeHandler(event:Event):void
		{
			img_upload.source = _fileRef.data;
			progressBarUpload.label = _fileRef.name;
			img_upload.addEventListener(Event.COMPLETE, onImageComplete);
		}
		
		protected function selectHandler(event:Event):void
		{
			_fileRef.load();
			getUuid();
		}
		
		protected function onImageComplete(event:Event):void
		{
			var encoder:JPEGEncoder = new JPEGEncoder();
			var bmp:Bitmap = img_upload.content as Bitmap;
			
			// création d'image avec dimension max 200x200
			var resizedData200:BitmapData = Formator.resizeBitmap(bmp.bitmapData ,200, 200);
			var byteArray200:ByteArray = encoder.encode(resizedData200);
			img_law200.source = byteArray200;
			//création d'image avec dimension max 75x75
			var resizedData75:BitmapData = Formator.resizeBitmap(bmp.bitmapData ,75, 75);
			var byteArray75:ByteArray = encoder.encode(resizedData75)
			img_tumb75.source = byteArray75;
			
			// si extension null alors on récupère l'extesion à partir du nom du fichier
			var longName:int = _fileRef.name.split('.').length;
			
			lowFile200	 = new FileUpload();
			lowFile200.fileData = byteArray200;
			lowFile200.fileExt = (_fileRef.type != null)?_fileRef.type : StringUtil.trim(_fileRef.name.split('.')[longName -1]);
			lowFile200.fileName = StringUtil.trim(_fileRef.name.split('.')[0])+ _UUID + '.'+lowFile200.fileExt
			lowFile200.imageType = ImageToolsImpl.TYPE_LOW;
			
			thumbFile75	 = new FileUpload();
			thumbFile75.fileData = byteArray75;
			thumbFile75.fileExt = (_fileRef.type != null)?_fileRef.type : StringUtil.trim(_fileRef.name.split('.')[longName -1]);
			thumbFile75.fileName = StringUtil.trim(_fileRef.name.split('.')[0])+ _UUID + '.'+thumbFile75.fileExt;
			thumbFile75.imageType = ImageToolsImpl.TYPE_THUMB;
			
			filesToUplaod.source = [];
			filesToUplaod.addItem(lowFile200);
			filesToUplaod.addItem(thumbFile75);
			
			equipement.PATH_LOW = lowFile200.fileName;
			equipement.PATH_THUMB = thumbFile75.fileName;
			
			btn_uploadImg.enabled = true;
		}
				
		protected function onSearchLibraryHandler(event:Event):void
		{
			ti_filtre.text = '';
			getLibraryEquipement();
		}
		
		private function getLibraryEquipement():void
		{
			if(( !isNaN(equipement.IDTYPE)) && (!isNaN(equipement.IDCONSTRUCTEUR)))
			{
				rechercheImageFromLibrary(StringUtil.trim(ti_search.text), equipement.IDTYPE, equipement.IDCONSTRUCTEUR, startIndex, NBITEMPARPAGE );
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M24','Please_select_a_constuctor_and_a_type_') ,ResourceManager.getInstance().getString('M24','Erreur'));
		}
		
		
		public function rechercheImageFromLibrary(libelle:String, idType_equipement: Number, idConstructeur: Number, startIndex:Number, nombreParPage:Number):void
		{
			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.M24.v2.Revendeur", 
																					"rechercheImageFromLibrary", 
																					getImageFromLibrary_handler, null);
			
			RemoteObjectUtil.callService(opData, libelle, idType_equipement, idConstructeur, startIndex, nombreParPage);
		}
		
		/**
		 * Event handler for the "Browse" button's click.
		 * 
		 * @param event Event object contains details of the event triggering this handler.
		 */ 			
		protected function onBrowseHandler(event:Event):void {
			imgVisible = false;
			_fileRef.browse([_imageTypes]);
		}
		
		private function getImageFromLibrary_handler(re:ResultEvent):void
		{
			listeEquipementLibrary.source = [];
			if(re.result)
			{
				if(re.result.length > 0)
				{
					for (var i:int = 0; i < re.result.length; i++) 
					{
						var equipt:EquipementCatalogueVO = new EquipementCatalogueVO();
						equipt.REFCONSTRUCTEUR = re.result[i].PROD_ID;
						equipt.MARQUE 		= re.result[i].NOM_FOURNISSEUR;
						equipt.MODELE 		= re.result[i].NAME;
						equipt.PATH_LOW 	= re.result[i].PATH_LOW;
						equipt.PATH_THUMB 	= re.result[i].PATH_THUMB;
						
						listeEquipementLibrary.addItem(equipt);
					}
					
					var noEquip:EquipementCatalogueVO = new EquipementCatalogueVO();
					noEquip.REFCONSTRUCTEUR = '';
					noEquip.MARQUE 		= ResourceManager.getInstance().getString('M24','no_image_');
					noEquip.MODELE 		= ResourceManager.getInstance().getString('M24','no_image_');
					noEquip.PATH_LOW 	= '';
					noEquip.PATH_THUMB 	= '';
					listeEquipementLibrary.addItemAt(noEquip,0);
			
				}
				else
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M24','Aucun_r_sultat'), ResourceManager.getInstance().getString('M24','Information_'), null);
			}
		}
		
		protected function onChangeSelectLibraryHandler(evt:ListEvent):void
		{
			_itemCurrent = evt.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listeEquipementLibrary.source.length)
			{
				if(_itemCurrent!=listeEquipementLibrary.source[i])
					listeEquipementLibrary.source[i].SELECTED = false;
				else
					listeEquipementLibrary.source[i].SELECTED = true;
				i++;
			}
			pdg_library.invalidateList();
			
			// Set the selected Path for the the new equipement path
			setSelectedPath();
		}
		
		// prendre en compte le path de l'image sélectionnée
		private function setSelectedPath():void
		{
			equipement.imageByUser = 0;
			if((listeEquipementLibrary.length > 0) && (_itemCurrent != null))
			{
				equipement.PATH_LOW = _itemCurrent.PATH_LOW;
				equipement.PATH_THUMB = _itemCurrent.PATH_THUMB;
				
				img_law200use.source = (_itemCurrent.PATH_LOW != '') ? ADR_IMG_SERVER + _itemCurrent.PATH_LOW : '';
				img_tumb75use.source = (_itemCurrent.PATH_THUMB != '') ? ADR_IMG_SERVER + _itemCurrent.PATH_THUMB : '';
				
				labelSelectedEquip = ((_itemCurrent.PATH_LOW != '') && (_itemCurrent.PATH_THUMB != '')) ? _itemCurrent.MARQUE + ' ' + _itemCurrent.MODELE : aucunEquip;
			}
			else
			{
				equipement.PATH_LOW = '';
				equipement.PATH_THUMB = '';
				
				labelSelectedEquip = aucunEquip;
			}
			
		}
		
		protected function tiFiltre_changeHandler(event:Event):void
		{
			listeEquipementLibrary.filterFunction = filtrerFilterList;
			listeEquipementLibrary.refresh();
			
			// Set the selected Path for the the new equipement pat
			setSelectedPath();
		}
		
		/* systeme de filtre */
		private function filtrerFilterList(item:Object):Boolean
		{
			if((String(item.MARQUE).toLocaleLowerCase().search(ti_filtre.text.toLocaleLowerCase()) != -1) 
				|| (String(item.MODELE).toLocaleLowerCase().search(ti_filtre.text..toLocaleLowerCase()) != -1)
				|| (String(item.REFCONSTRUCTEUR).toLocaleLowerCase().search(ti_filtre.text.toLocaleLowerCase()) != -1))
				return true;
			else
				return false;
		}

		public function get filesToUplaod():ArrayCollection { return _filesToUplaod; }
		
		public function set filesToUplaod(value:ArrayCollection):void
		{
			if (_filesToUplaod == value)
				return;
			_filesToUplaod = value;
		}
		
		public function set equipement(value:EquipementCatalogueVO):void
		{
			_equipement = value;
		}
		[Bindable]
		public function get equipement():EquipementCatalogueVO
		{
			return _equipement;
		}
		
		[Bindable]
		public function get listeEquipementLibrary():ArrayCollection { return _listeEquipementLibrary; }
		
		public function set listeEquipementLibrary(value:ArrayCollection):void
		{
			if (_listeEquipementLibrary == value)
				return;
			_listeEquipementLibrary = value;
		}
		
		public function get startIndex():int
		{
			return _startIndex;
		}
		
		public function set startIndex(value:int):void
		{
			_startIndex = value;
		}
	
	}
}
