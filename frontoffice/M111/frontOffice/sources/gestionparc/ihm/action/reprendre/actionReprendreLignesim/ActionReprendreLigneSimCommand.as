package gestionparc.ihm.action.reprendre.actionReprendreLignesim
{
	import gestionparc.ihm.action.ICommand;

	public class ActionReprendreLigneSimCommand implements ICommand
	{
		private var receiver:ActionReprendreLigneSimReceiver=null;

		public function ActionReprendreLigneSimCommand(rec:ActionReprendreLigneSimReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
