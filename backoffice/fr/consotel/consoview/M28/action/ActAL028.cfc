﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	   <!--- Fournit la liste des collectes pour une racine donnée(idRacine)        
	 --->
	<cffunction name="executeAct" access="public" returntype="struct" output="false">		
		
		<cfargument name="data" required="true" type="struct"/>
						
						
		<cftry>
		 	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.search_collect_client">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.racineId#" >			
				<cfprocresult name="p_retour">
			</cfstoredproc> 
		
			<cfif isQuery(p_retour)>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AL028", 1, data)/>
			<cfelseif isnumeric(p_retour)>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur de procédure", "", "")>
				<cfreturn CreateReponse("AL028", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AL028", 0, ThrowCfCatchError("AL028",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
</cfcomponent>