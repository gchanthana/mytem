package commandemobile.temp
{
	import commandemobile.entity.Commande;
	import commandemobile.system.AbstractGestionPanier;
	import commandemobile.system.CommandeEvent;
	import commandemobile.system.Revendeur;
	
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.employes.Collaborateur;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;

   
	[Bindable]
	public class temp extends EventDispatcher
	{
		private var _listePools						:ArrayCollection;
		private var _listeConcatPools				:ArrayCollection;	
		private var _listeAgences					:ArrayCollection;
		private var _listeContacts					:ArrayCollection;
		private var _listeMesRevendeurs				:ArrayCollection;
		private var _listeOperateurs				:ArrayCollection;
		private var _listeComptes					:ArrayCollection;
		private var _listeSousComptes				:ArrayCollection;
		private var _listeSites						:ArrayCollection;
		private var _listeEquiementsClient			:ArrayCollection;
		private var _listeEquipementsRevendeur		:ArrayCollection;
		private var _listeProduitsCatalogue			:ArrayCollection;
		private var _listeAccessoiresClient			:ArrayCollection;
		private var _listeProduitsCatalogueFavoris	:ArrayCollection = new ArrayCollection();
		private var _invetaireCommandes				:ArrayCollection;
		private var _listConfigurationRecorded		:ArrayCollection;
		private var _listEngagement					:ArrayCollection = new ArrayCollection();
		private var _gestionArticles				:AbstractGestionPanier;
        private var _listeEmployes					:ArrayCollection;
        private var _selectedCollaborateur			:Collaborateur;
        private var _tmpEmploye						:Collaborateur = new Collaborateur();
		public var selectedCommande					:Commande;
		private var _revendeur						:Revendeur;
		private var _selectedPoolId					:Number = 0;
		private var _numeroUnique					:String = ""
		private var txtDefaultCbx					:String = "Sélectionnez";
		private var _listeArticles 					:XML = <articles></articles>;
		
		private var _tabListeCaracteristique		:ArrayCollection;
		private var _tabListeTarif					:ArrayCollection;
		private var _listActePour					:ArrayCollection;
		
		public function get listActePour():ArrayCollection
		{
			return _listActePour;
		}
			
		public function set listActePour(values:ArrayCollection):void
		{
			_listActePour = values;
		}
		
		public function get tabListeCaracteristique():ArrayCollection
		{
			return _tabListeCaracteristique
		}
			
		public function set tabListeCaracteristique(values:ArrayCollection):void
		{
			_tabListeCaracteristique = values;
		}
		
		public function get tabListeTarif():ArrayCollection
		{
			return _tabListeTarif
		}
			
		public function set tabListeTarif(values:ArrayCollection):void
		{
			_tabListeTarif = values;
		}

		public function get listePools():ArrayCollection
		{
			return _listePools
		}
			
		public function set listePools(values:ArrayCollection):void
		{
			_listePools = values;
		}
		
		public function get listeConcatPools():ArrayCollection
		{
			return _listeConcatPools;
		}
			
		public function set listeConcatPools(values:ArrayCollection):void
		{
			_listeConcatPools = values;
		}
		
		public function get listeAgences(): ArrayCollection
	    {
	    	return _listeAgences;
	    }

	    public function set listeAgences(agences:ArrayCollection): void
	    {
	    	_listeAgences = agences;
	    }
		
		public function get listeContacts(): ArrayCollection
	    {
	    	return _listeContacts;
	    }	

	    public function set listeContacts(ar:ArrayCollection): void
	    {
	    	_listeContacts = ar;
	    }

		public function get listeMesRevendeurs(): ArrayCollection
	    {
	    	return _listeMesRevendeurs;
	    }

	    public function set listeMesRevendeurs(liste:ArrayCollection): void
	    {
	    	_listeMesRevendeurs = liste;
	    }

	   	public  function get listeOperateurs():ArrayCollection
	    {
	    	return _listeOperateurs
	    }
	
	    public  function set listeOperateurs(values:ArrayCollection): void
	    {
	    	_listeOperateurs = values
	    }
	    
	    public function get listeComptes(): ArrayCollection
	    {
	    	return _listeComptes;
	    }
	
	    public function set listeComptes(values:ArrayCollection): void
	    {
	    	_listeComptes = values
	    }
	    
	    public function get listeSousComptes(): ArrayCollection
	    {
	    	return _listeSousComptes;
	    }
	
	    public function set listeSousComptes(values:ArrayCollection): void
	    {
	    	_listeSousComptes = values;
	    }
	    
	    public function get listeSites():ArrayCollection
	    {
	    	return _listeSites;
	    }

	    public function set listeSites(values:ArrayCollection):void
	    {
	    	_listeSites = values;    	
	    }

///////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\
	    
	    public function get listeEquiementsClient(): ArrayCollection
	    {
	    	return _listeEquiementsClient;
	    } 
	    
	    public function set listeEquiementsClient(value:ArrayCollection): void
	    {
	    	_listeEquiementsClient = value;
	    } 
	    
	    public function get listeEquiementsRevendeur(): ArrayCollection
	    {
	    	return _listeEquipementsRevendeur;
	    }
	    
	    public function set listeEquiementsRevendeur(value:ArrayCollection): void
	    {
	    	_listeEquipementsRevendeur = value;
	    }

		public function get listeProduitsCatalogue(): ArrayCollection
	    {
	    	return _listeProduitsCatalogue;
	    }
	    
	    public function set listeProduitsCatalogue(value:ArrayCollection): void
	    {
	    	_listeProduitsCatalogue = value;
	    }

	    public function get listeAccessoiresClient(): ArrayCollection
	    {
	    	return _listeAccessoiresClient;
	    }
	    
	    public function set listeAccessoiresClient(value:ArrayCollection): void
	    {
	    	_listeAccessoiresClient = value;
	    }

		public function get listeProduitsCatalogueFavoris(): ArrayCollection
	    {
	    	return _listeProduitsCatalogueFavoris;
	    }
	    
	    public function set listeProduitsCatalogueFavoris(value:ArrayCollection): void
	    {
	    	_listeProduitsCatalogueFavoris = value;
	    }
////\\\\\

		public function set inventaireCommandes(values:ArrayCollection): void
	    {
	    	_invetaireCommandes = values
	    }

	    public function get inventaireCommandes(): ArrayCollection
	    {
	    	return _invetaireCommandes;
	    }

		private var _selectedIndex:Number;
		public function get selectedIndex():Number
		{
			return _selectedIndex
		}
		public function set selectedIndex(value:Number):void
		{
			_selectedIndex = value
		}
		
		private var _listeRevendeursCommandes:ArrayCollection;
		public function set listeRevendeursCommandes(values:ArrayCollection): void
	    {
	    	_listeRevendeursCommandes = values
	    }
	    public function get listeRevendeursCommandes(): ArrayCollection
	    {
	    	return _listeRevendeursCommandes;
	    }
	    
	    private var _listeEtatsCommandes:ArrayCollection;
	    public function set listeEtatsCommandes(values:ArrayCollection): void
	    {
	    	_listeEtatsCommandes = values
	    }
	    public function get listeEtatsCommandes(): ArrayCollection
	    {
	    	return _listeEtatsCommandes;
	    }
//////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		public function set selectedCollaborateur(value:Collaborateur): void
        {
            _selectedCollaborateur = value;
        }
   
        public function get selectedCollaborateur(): Collaborateur
        {
            return _selectedCollaborateur;
        }

        public function set listeEmployes(values:ArrayCollection): void
        {
            _listeEmployes = values
        }
        
        public function get listeEmployes(): ArrayCollection
        {
            return _listeEmployes
        }
///////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		public function get gestionArticles():AbstractGestionPanier
		{
			if (_gestionArticles == null)
			{
				_gestionArticles = new AbstractGestionPanier();
			} 
			
			return _gestionArticles
		}
		public function set gestionArticles(value:AbstractGestionPanier):void
		{
			_gestionArticles = value;
		}
		
		public function set listeArticles(value:XML):void{			
			_listeArticles = value;
		}
		public function get listeArticles():XML{
			return _listeArticles;		
		}
////////\\\\\\\\\\\\\\\\
	    public function get numeroUnique():String
	    {
	    	return _numeroUnique;
	    }
	    public function set numeroUnique(value:String):void
	    {
	    	_numeroUnique = value;
	    }

//////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	    public function get listConfigurationRecorded():ArrayCollection
	    {
	    	return _listConfigurationRecorded;
	    }
	    public function set listConfigurationRecorded(value:ArrayCollection):void
	    {
	    	_listConfigurationRecorded = value;
	    }

//////////////////\\\\\\\\\\\\\\\\\\\\\\\\\
		private var _listeFeature			:ArrayCollection = new ArrayCollection();
	   	private var _listePhotos			:ArrayCollection = new ArrayCollection();;
	   	
	    private var _listeProductRelated	:ArrayCollection = new ArrayCollection();
	    private var _listeHistoriqueAction	:ArrayCollection = new ArrayCollection();
	    private var _listeModele			:XML = <articles></articles>;
		
		private var _listUser				:ArrayCollection = new ArrayCollection();
	    private var _listType				:ArrayCollection = new ArrayCollection();
	    
	    private var _listConfiguration		:ArrayCollection = new ArrayCollection();
	    private var _profil					:ArrayCollection = new ArrayCollection();

		public function get listConfiguration():ArrayCollection
		{
			return _listConfiguration;
		}
			
		public function set listConfiguration(values:ArrayCollection):void
		{
			_listConfiguration = values;
		}


		public function get listUser():ArrayCollection
		{
			return _listUser;
		}
			
		public function set listUser(values:ArrayCollection):void
		{
			_listUser = values;
		}
		
		public function get listType():ArrayCollection
		{
			return _listType;
		}
			
		public function set listType(values:ArrayCollection):void
		{
			_listType = values;
		}

		public function get listeFeature():ArrayCollection
		{
			return _listeFeature;
		}
			
		public function set listeFeature(values:ArrayCollection):void
		{
			_listeFeature = values;
		}
		
		public function set listePhotos(value:ArrayCollection):void
		{
			_listePhotos = value;
		}

		public function get listePhotos():ArrayCollection
		{
			return _listePhotos;
		}

		public function get listeProductRelated():ArrayCollection
		{
			return _listeProductRelated;
		}
			
		public function set listeProductRelated(values:ArrayCollection):void
		{
			_listeProductRelated = values;
		}
		
		public function get listeHistoriqueAction():ArrayCollection
		{
			return _listeHistoriqueAction;
		}
			
		public function set listeHistoriqueAction(values:ArrayCollection):void
		{
			_listeHistoriqueAction = values;
		}

		public function get listeModele():XML
		{
			return _listeModele;
		}
			
		public function set listeModele(values:XML):void
		{
			_listeModele = values;
		}

		public function get profil():ArrayCollection
		{
			return _profil;
		}
			
		public function set profil(values:ArrayCollection):void
		{
			_profil = values;
		}

/////////////////////// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		public function temp()
		{
			Alert.okLabel = "Fermer";
			Alert.buttonWidth = 100;
		}
	
		public function functionActePour(profil:int, idrevendeur:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.sncf.commande_sncf",
																	"functionActePour",
																	functionActePourHandler);
			RemoteObjectUtil.callService(op,profil,
											idrevendeur);
		}
		
		private function functionActePourHandler(re : ResultEvent):void
		{
			if(re.result)
			{
				listActePour = new ArrayCollection();
				listActePour = mappinglistActePour(re.result as ArrayCollection);
				dispatchEvent(new Event("listActePour",true));
			}
			else
			{
				Alert.show("Impossible de récupérer la liste des gestionnaire!","Consoview");
			}
		}
		
		private function mappinglistActePour(listGestionnaire:ArrayCollection):ArrayCollection
		{
			var object:Object 					= new Object();
			var gestionnaires:ArrayCollection 	= new ArrayCollection();
			for(var i:int = 0;i < listGestionnaire.length;i++)
			{
				object = new Object();
				object.LIBELLE 			= listGestionnaire[i].LOGIN_NOM + " " + listGestionnaire[i].LOGIN_PRENOM + " / " + listGestionnaire[i].LIBELLE_POOL + " / " + listGestionnaire[i].LIBELLE_PROFIL_COMMANDE;
				object.NOM 				= listGestionnaire[i].LOGIN_NOM;
				object.PRENOM 			= listGestionnaire[i].LOGIN_PRENOM;
				object.LOGIN_EMAIL 		= listGestionnaire[i].LOGIN_EMAIL;
				object.APPLOGINID 		= listGestionnaire[i].APP_LOGINID;
				object.IDPROFILE 		= listGestionnaire[i].IDPROFIL_COMMANDE;
				object.LIBELLE_PROFILE 	= listGestionnaire[i].LIBELLE_PROFIL_COMMANDE;
				object.IDPOOL 			= listGestionnaire[i].IDPOOL;
				object.LIBELLE_POOL 	= listGestionnaire[i].LIBELLE_POOL;
				gestionnaires.addItem(object);
			}
			return gestionnaires;
		}
	
		public function dg_profile_handler(profil:int):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"fournirListeActProfil",
																	process_fournirListeActProfil);
			RemoteObjectUtil.callService(op,profil);
		}
		
		private function process_fournirListeActProfil(re : ResultEvent):void
		{
			profil = re.result as ArrayCollection;
			dispatchEvent(new Event("actionsFinished",true));
		}
		
		
		
		
		public function fournirPoolsDuGestionnaire():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"fournirPoolsDuGestionnaire",
																		fournirPoolsDuGestionnaireResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		protected function fournirPoolsDuGestionnaireResultHandler(event:ResultEvent):void
		{
			listePools = event.result as ArrayCollection;
			listeConcatPools = new ArrayCollection();
			
			for(var i:int = 0;i < listePools.length;i++)
			{
				var strgTemp:String;
				
				strgTemp = listePools[i].LIBELLE_POOL;
				strgTemp = strgTemp + " - " + listePools[i].LIBELLE_PROFIL;
				
				var objectTemp:Object = new Object();
				objectTemp.LIBELLE_POOL 	= listePools[i].LIBELLE_POOL;
				objectTemp.LIBELLE_PROFIL 	= listePools[i].LIBELLE_PROFIL;
				objectTemp.LIBELLE_CONCAT 	= strgTemp;
				objectTemp.IDPOOL 			= listePools[i].IDPOOL;
				objectTemp.IDPROFIL 		= listePools[i].IDPROFIL;
				objectTemp.IDREVENDEUR 		= listePools[i].IDREVENDEUR;
				listeConcatPools.addItem(objectTemp);
			}
			dispatchEvent(new Event("listPool"));
	    }
	    
	    public function fournirListeSiteLivraisonsPoolGestionnaire(idPool:Number): void
	    {
	    	_selectedPoolId = idPool;
	    	
	    	if(_selectedPoolId>0)
	    	{
	    		var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  "fr.consotel.consoview.inventaire.sites.SitesUtils",
	    																		  "fournirListeSitesLivraisonsPoolGestionnaire",
	    																		  fournirListeSiteLivraisonsPoolGestionnaireResultHandler);
	    		RemoteObjectUtil.callService(op,idPool);	
	    	}
	    }
	
	    protected function fournirListeSiteLivraisonsPoolGestionnaireResultHandler(event:ResultEvent):void
	    {
	    	listeSites = event.result as ArrayCollection;
			dispatchEvent(new Event("listSites"));
	    }

		public function rechecherMesRevendeurs(chaine:String): void
	    {
	    	var Idracine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
	    	var chaine:String = chaine;
	    	var APP_LOGINID:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
	    	var p_segment:int = 2;
	    	
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.Revendeur",
																					"get_catalogue_revendeur", 
																					rechecherMesRevendeursResultHandler);
			RemoteObjectUtil.callService(opData, Idracine, chaine, APP_LOGINID, p_segment);
	    }
	    
	    private function rechecherMesRevendeursResultHandler(re:ResultEvent): void
	    {
	    	var source:ArrayCollection = re.result as ArrayCollection;  
           	var cursor:IViewCursor = source.createCursor();	 
           	
           	listeMesRevendeurs = new ArrayCollection();   	
            while (!cursor.afterLast)
            {
            	var currentObj:Object = cursor.current;
            	listeMesRevendeurs.addItem(currentObj);
            	cursor.moveNext();
          	}
			dispatchEvent(new Event("listRevendeurs"));
	    }
	    
	    public function rechercherAgencesRevendeur(chaine:String, revendeur:Revendeur): void
	    {
	    	var IDCDE_CONTACT_SOCIETE:int = revendeur.IDCDE_CONTACT_SOCIETE;
	    	var chaine:String = chaine;
	    	
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.FicheSociete",
																					"get_liste_agence", 
																					rechercherAgencesRevendeurResultHandler);
			RemoteObjectUtil.callService(opData, IDCDE_CONTACT_SOCIETE, chaine);
	    }
	    
	    private function rechercherAgencesRevendeurResultHandler(re:ResultEvent): void
	    {
	    	var source:ArrayCollection = re.result as ArrayCollection;
           	var cursor:IViewCursor = source.createCursor();
           	var i:Number = 0;
           	
           	listeAgences = new ArrayCollection();   
           		
            while (!cursor.afterLast)
            {
            	var currentObj:Object = cursor.current;
            	listeAgences.addItem(currentObj);
            	cursor.moveNext();
          	}
			dispatchEvent(new Event("listAgences"));
	    }
	    
	    public function rechercherContactsRevendeur(chaine:String, revendeur:Revendeur): void
	    {
	    	var IDCDE_CONTACT_SOCIETE:int = revendeur.IDCDE_CONTACT_SOCIETE;
	    	var APP_LOGINID:int = CvAccessManager.getSession().USER.CLIENTACCESSID;
	    	var chaine:String = chaine;
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.inventaire.equipement.revendeurs.Contact",
																					"lister_cde_contact", 
																					rechercherContactsRevendeurResultHandler);
			RemoteObjectUtil.callService(opData, IDCDE_CONTACT_SOCIETE, APP_LOGINID, chaine);
	    }

	    private function rechercherContactsRevendeurResultHandler(re:ResultEvent): void
	    {
	    	var source:ArrayCollection = re.result as ArrayCollection;
           	var cursor:IViewCursor = source.createCursor();
           	
           	listeContacts = new ArrayCollection();  
           	 	
            while (!cursor.afterLast)
            {
            	var currentObj:Object = cursor.current;
            	listeContacts.addItem(currentObj);
            	cursor.moveNext();
          	}
			dispatchEvent(new Event("listContacts"));
	    }

		public  function fournirListeOperateursSegment(segment:String): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		  "fr.consotel.consoview.inventaire.operateurs.OperateursUtils",
	    																		  "fournirListeOperateursSegment",
	    																		  fournirListeOperateursSegmentResultHandler);
	    	RemoteObjectUtil.callService(op,segment);
	    }
	    
	    protected  function fournirListeOperateursSegmentResultHandler(event:ResultEvent): void
	    {	
	    	listeOperateurs = event.result as ArrayCollection;
			dispatchEvent(new Event("listOperateurs"));
	    }

		public function fournirListeComptes(idOperateur:Number): void
	    {
	    	var idRacine:int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.comptes.ComptesUtils",
	    																			"fournirListeComptes",
	    																			fournirListeComptesResultHandler);
			RemoteObjectUtil.callService(op,idRacine,idOperateur);
	
	    }

 		protected function fournirListeComptesResultHandler(event:ResultEvent): void
	    {
	    	listeComptes = event.result as ArrayCollection;
			dispatchEvent(new Event("listComptes"));
	    }
	    
	    public function fournirListeSousComptes(idCompte:Number): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.comptes.ComptesUtils",
	    																			"fournirListeSousComptes",
	    																			fournirListeSousComptesResultHandler);
			RemoteObjectUtil.callService(op,idCompte);	
	    }
	    
	    protected function fournirListeSousComptesResultHandler(event:ResultEvent): void
	    {
	    	listeSousComptes = event.result as ArrayCollection;
			dispatchEvent(new Event("listSousComptes"));
	    }
	    
////////////////////////////////////////////////////////////////////////////////////************\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\	    

		public function fournirListeEquipementsCatalogueClient(idrevendeur:Number, clef:String, segment:String="MOBILE"): void///////////////////-----------------------***********
	    {
			listeEquiementsClient = null;
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils",
								"fournirListeEquipementsCatalogueClient", 
								fournirListeEquipementsCatalogueClientResultHandler);
			RemoteObjectUtil.callService(opData,idrevendeur,clef,segment)
	    		
		}
		
		protected function fournirListeEquipementsCatalogueClientResultHandler(event:ResultEvent): void
	    {
	    	var listTemp:ArrayCollection = new ArrayCollection();
	    	var obj:Object = new Object();
	    	
	    	listeEquiementsClient = event.result as ArrayCollection;
	    	
	    	for(var i:int = 0;i < listeEquiementsClient.length;i++)
			{
					listeEquiementsClient[i].SELECTED = false;
			}
					trace(ObjectUtil.toString(listeEquiementsClient[1]));

   	
	    	dispatchEvent(new Event("listCatalogueClient"));
	    }
		
		
		public function fournirListeAccessoiresCatalogueClient(idrevendeur:Number, clef:String, segment:String="MOBILE"): void
	    {
			listeEquiementsClient = null;
			var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
								"fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils",
								"fournirListeAccessoiresCatalogueClient", 
								fournirListeAccessoiresCatalogueClientResultHandler);
			RemoteObjectUtil.callService(opData,idrevendeur,clef,segment)
	    		
		}
		
		protected function fournirListeAccessoiresCatalogueClientResultHandler(event:ResultEvent): void
	    {
//	    	listeAccessoiresClient = event.result as ArrayCollection;
//	    	
//	    	if(listeAccessoiresClient.length > 0)
//	    	{
//		    	for(var i:int = 0;i < listeAccessoiresClient.length;i++)
//				{
//						listeAccessoiresClient[i].SELECTED = false;
//				}
//				trace(ObjectUtil.toString(listeAccessoiresClient[1]));
//
//	    	}
//	    	dispatchEvent(new Event("listCatalogueAccessoiresClient"));
			if(event.result)
	    	{
	    		var arrayC:ArrayCollection = event.result as ArrayCollection;
	    		if(arrayC.length > 0)
	    		{
		    		if(arrayC[0].hasOwnProperty("ERREUR"))
		    		{
		    			Alert.show("Pas de données!","Consoview");
		    		}
		    		else
		    		{
		    			listeProductRelated = new ArrayCollection();
		    			var reult:ArrayCollection = event.result as ArrayCollection;
		    			var cursor:IViewCursor = reult.createCursor();
		    			var item:Object;
		    			
		    			while(!cursor.afterLast)
		    			{
		    				item = cursor.current;
		    				item.PRIX_UNIT = item.PRIX_CATALOGUE;
		    				item.LIBELLE_PRODUIT = item.LIBELLE_EQ; 
		    				item.LIBELLE_MODELE = item.LIBELLE_EQ;
		    				listeProductRelated.addItem(item);
		    				cursor.moveNext();			
		    			}
		    			dispatchEvent(new Event("listeProductRelatedOk"));
		    		}
	    		}
	    		else
	    		{
	    			dispatchEvent(new Event("listeProductRelatedNoAccessory"));
	    		}
	    	}
	    	else
	    	{
	    		Alert.show("Pas de données!","Consoview");
	    	}
	    }

	    public function fournirListeEquipementsCatalogueRevendeur(idrevendeur:Number, clef:String, segment:String="MOBILE"): void
	    {	   
    		listeEquiementsRevendeur = null;
    		var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils",
							"fournirListeEquipementsCatalogueRevendeur", 
							fournirListeEquipementsCatalogueRevendeurResultHandler);
			RemoteObjectUtil.callService(opData,idrevendeur,clef,segment)
	    		
		}
		
		protected function fournirListeEquipementsCatalogueRevendeurResultHandler(event:ResultEvent): void
	    {
	    	listeEquiementsRevendeur = event.result as ArrayCollection;
	    }
	    
	    
/////ARAJOUTER DANS LES CLASS


	    public function fournirListeProduitCatalogueOperateur(idOperateur:Number):void
	    {
			var opTheme : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.produits.ProduitsUtils",
																				"fournirListeProduitsCatalogueOperateur",
																				fournirListeProduitCatalogueOperateurResultHandler);				
			RemoteObjectUtil.callService(opTheme,idOperateur);
		}
		
		protected function fournirListeProduitCatalogueOperateurResultHandler(event:ResultEvent):void
		{
			listeProduitsCatalogue = new ArrayCollection();
			listeProduitsCatalogueFavoris = new ArrayCollection();
			
			listeProduitsCatalogue = event.result as ArrayCollection;

			for(var i:int = 0;i < listeProduitsCatalogue.length;i++)
			{
				listeProduitsCatalogue[i].SELECTED = false;
				if(listeProduitsCatalogue[i].FAVORI == 1)
				{
					listeProduitsCatalogueFavoris.addItem(listeProduitsCatalogue[i]);
				}
			}
			dispatchEvent(new Event("listProduitCatalogue"));
		}

		public function ajouterProduitAMesFavoris(value:Object):void{
			if (!value.hasOwnProperty("IDPRODUIT_CATALOGUE")) return;
			if (value.IDPRODUIT_CATALOGUE < 1) return;
			
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.produits.ProduitsUtils",
																				"ajouterProduitAMesFavoris",
																				function ajouterProduitAMesFavorisResultHandler(event:ResultEvent):void
																				{
																					if(event.result > 0)
																					{
																						value.FAVORI = 1;
																						
																					}
																					else
																					{
																						value.FAVORI = 0;
																						Alert.show("Erreur : " + event.result.toString(),"Erreur");	
																					}
																				});				
			RemoteObjectUtil.callService(op,value.IDPRODUIT_CATALOGUE);
		}

		public function supprimerProduitDeMesFavoris(value:Object):void{
			if (!value.hasOwnProperty("IDPRODUIT_CATALOGUE")) return;
			if (value.IDPRODUIT_CATALOGUE < 1) return;
			
			var op : AbstractOperation  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.produits.ProduitsUtils",
																				"supprimerProduitDeMesFavoris",
																				function supprimerProduitDeMesFavorisResultHandler(event:ResultEvent):void
																				{
																					if(event.result > 0)
																					{
																						value.FAVORI = 0;
																						
																					}
																					else
																					{
																						value.FAVORI = 1;
																						Alert.show("Erreur : " + event.result.toString(),"Erreur");	
																					}
																				});				
			RemoteObjectUtil.callService(op,value.IDPRODUIT_CATALOGUE);
		}
		
///////\\\\\\\\\\\\\\\\\\\\\\\

		public function fournirInventaireCommandes(dateDebutPeriode:Date, dateFinPeriode:Date, clef:String, segment:Number = 2): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.commande.GestionCommande",
	    																			"fournirInventaireOperation",
	    																			fournirInventaireCommandesResultHandler);
			RemoteObjectUtil.callService(op,dateDebutPeriode,dateFinPeriode,clef,segment)
	    }
	    
	    protected function fournirInventaireCommandesResultHandler(event:ResultEvent): void
	    {
	    	inventaireCommandes = new ArrayCollection();
	    	inventaireCommandes = mappDataListeCommandes(event.result as ArrayCollection);
	    	if(selectedCommande != null)
	    	{
	    		selectedIndex = ConsoviewUtil.getIndexById(inventaireCommandes,"IDCOMMANDE",selectedCommande.IDCOMMANDE);
	    	}
	    	updateListeEtats(inventaireCommandes);
	    	updateListeRevendeurs(inventaireCommandes);
	    	dispatchEvent(new Event("listCommande"));
	    }
	    
	    private function mappDataListeCommandes(values : ICollectionView):ArrayCollection
	    {
			var retour : ArrayCollection = new ArrayCollection();
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
				while(!cursor.afterLast){
					var commandeObj : Commande = new Commande();
					commandeObj.IDCOMMANDE = cursor.current.IDCOMMANDE;
					commandeObj.REF_CLIENT =cursor.current.REF_CLIENT;
					commandeObj.LIBELLE_REVENDEUR =cursor.current.LIBELLE_REVENDEUR;
					commandeObj.IDREVENDEUR =cursor.current.IDREVENDEUR;
					commandeObj.LIBELLE_COMMANDE =cursor.current.LIBELLE;
					commandeObj.TYPE_OPERATION =cursor.current.TYPE_OPERATION;
					commandeObj.LIBELLE_SITELIVRAISON =cursor.current.NOM_SITE;	
					commandeObj.NUMERO_COMMANDE =cursor.current.NUMERO_OPERATION;
					commandeObj.DATE_COMMANDE = cursor.current.DATE_ENVOI;
					commandeObj.LIVRAISON_PREVUE_LE = cursor.current.DATE_EFFET_PRE;	
					commandeObj.IDLAST_ETAT =cursor.current.IDINV_ETAT;
					commandeObj.IDLAST_ACTION =cursor.current.IDINV_ACTIONS;
					commandeObj.LIBELLE_LASTETAT =cursor.current.LIBELLE_ETAT;
					commandeObj.LIBELLE_LASTACTION =cursor.current.LIBELLE_ACTION;
					commandeObj.NUMERO_TRACKING =cursor.current.NUMERO_TRACKING;
					commandeObj.IDTRANSPORTEUR = cursor.current.IDTRANSPORTEUR;	
					commandeObj.MONTANT = Number(cursor.current.MONTANT);
					commandeObj.IDPOOL_GESTIONNAIRE = cursor.current.IDPOOL;
					commandeObj.LIVREE_LE = cursor.current.LIVRE_LE;
					commandeObj.LIVRAISON_PREVUE_LE = cursor.current.LIVRAISON_PREVUE_LE;
					commandeObj.EXPEDIE_LE = cursor.current.EXPEDIE_LE;
					commandeObj.ENCOURS= Number(cursor.current.EN_COURS);
					commandeObj.IDTYPE_COMMANDE = Number(cursor.current.IDTYPE_OPERATION);
					commandeObj.USERCREATE = cursor.current.USERCREATE;
					commandeObj.USERID = Number(cursor.current.USERID);
					commandeObj.CREEE_LE = cursor.current.DATE_CREATE;
					commandeObj.IDINV_ETAT = Number(cursor.current.IDINV_ETAT);
					retour.addItem(commandeObj);
					cursor.moveNext();
				}
			}
			return retour 
		}
		
		private function modificationDate():void
		{
		
		}
		
		private function updateListeEtats(values:ICollectionView):void
		{	
			if(listeEtatsCommandes != null) listeEtatsCommandes = null
			listeEtatsCommandes = new ArrayCollection();
						
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.getIndexById(listeEtatsCommandes,"IDETAT",cursor.current.IDLAST_ETAT) < 0)
					{
						if(cursor.current.IDLAST_ETAT != 0)
						{
							listeEtatsCommandes.source.push({IDETAT:cursor.current.IDLAST_ETAT,LIBELLE_ETAT:cursor.current.LIBELLE_LASTETAT});
						}	    									
					}						
					cursor.moveNext();
				}
			}
			
			listeEtatsCommandes.addItemAt({IDETAT:-1,LIBELLE_ETAT:"Tous"},0);		
		}
	    
	    
	    private function updateListeRevendeurs(values:ICollectionView):void
		{	
			if(listeRevendeursCommandes != null) listeRevendeursCommandes = null
			listeRevendeursCommandes = new ArrayCollection();
						
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.getIndexById(listeRevendeursCommandes,"IDREVENDEUR",cursor.current.IDREVENDEUR) < 0)
					{
						listeRevendeursCommandes.source.push({IDREVENDEUR:cursor.current.IDREVENDEUR,LIBELLE_REVENDEUR:cursor.current.LIBELLE_REVENDEUR});
					}   				    			
					cursor.moveNext();
				}
			}
			listeRevendeursCommandes.source.unshift({IDREVENDEUR:-1,LIBELLE_REVENDEUR:"Tous"});
		}

		public function supprimerCommande(commande:Commande): void
	    {
	    	if(!(commande.IDCOMMANDE > 0))return;
	    	
	    	var op : AbstractOperation = RemoteObjectUtil
	    									.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.inventaire.commande.GestionCommande",
																	"eraseCommande",
																	function(event:ResultEvent):void
															    	{
															    		if(event.result > 0)	    	
															    		{					
																    		inventaireCommandes.removeItemAt(inventaireCommandes.getItemIndex(commande));																    		
																    		commande = null;
																    		updateListeEtats(inventaireCommandes);
																    		ConsoviewAlert.afficherOKImage("Commande effacée !");
																    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_DELETED));
																    	}
																    	else
																    	{
																    		var message:String = "";
																    		var titre:String = "Impossible de détruire la commande!";
																    																		    		 
																    		if(event.result == -1)
																    		{																    			
																    			message = "La commande a éte expédiée par le revendeur";
   																    		}
																    		else
																    		{
																    			message = "Erreur interne";
																    		}
																    		
																    		ConsoviewAlert.afficherAlertInfo(message,titre,null);
																    	}
															    	});
			RemoteObjectUtil.callService(op,commande.IDCOMMANDE)
	    }
	    
	    public function fournirDetailOperation(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.commande.GestionCommande",
	    																			"fournirDetailOperation",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result && (event.result as ArrayCollection).length > 0)	    	
																			    		{																			    			
																				    		mappDataCommande(commande,event.result[0]);
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Pas de donnée");
																				    	}	
																			    	});
			RemoteObjectUtil.callService(op,commande.IDCOMMANDE)
	    }
	    
	    public function majCommande(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.commande.GestionCommande",
	    																			"majCommande",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result > 0)	    	
																			    		{								
																			    			ConsoviewAlert.afficherOKImage("Informations modifiées");										    			
																				    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_UPDATED));
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Erreur");
																				    	}	
																			    	});
			RemoteObjectUtil.callService(op,commande,0)
	    }
	    
	    public function majInfosLivraisonCommande(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.commande.GestionCommande",
	    																			"majInfosLivraisonCommande",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result > 0)	    	
																			    		{							
																			    			ConsoviewAlert.afficherOKImage("Informations modifiées !");												    			
																				    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_UPDATED));
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Erreur");
																				    	}	
																			    	});
			RemoteObjectUtil.callService(op,commande)
	    }
	    
	    public var idCommande:int = 0;
	    public function enregistrerCommande(commande:Commande, articles:XML,actePour:Boolean): void
	    {	
	    	commande.SEGMENT_MOBILE = 1;
	    	commande.SEGMENT_FIXE = 0;
	    	if(!actePour)
	    	{
		    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
		    																			/*"fr.consotel.consoview.inventaire.commande.GestionCommande",*/
		    																			"fr.consotel.consoview.inventaire.sncf.commande_sncf",
		    																			"enregistrerCommande",
		    																			function(event:ResultEvent):void
																				    	{
																				    		if(event.result > 0)	    	{
																				    			commande.IDCOMMANDE = Number(event.result);
																				    			idCommande = Number(event.result);
																				    			ConsoviewAlert.afficherOKImage("Commande enregistrée !");
																					    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
																					    	}
																					    	else
																					    	{
																					    		Alert.show("Erreur");
																					    	}	
																				    	}
		    																		);
				RemoteObjectUtil.callService(op,	commande.IDGESTIONNAIRE,
													commande.IDPOOL_GESTIONNAIRE,	
													commande.IDCOMPTE_FACTURATION,
													commande.IDSOUS_COMPTE,
													commande.IDOPERATEUR,
													commande.IDREVENDEUR,
													commande.IDCONTACT,
													commande.IDTRANSPORTEUR,
													commande.IDSITELIVRAISON,
													commande.NUMERO_TRACKING,
													commande.LIBELLE_COMMANDE,
													commande.REF_CLIENT,
													commande.REF_OPERATEUR,
													formatDate(commande.DATE_COMMANDE),
													formatDate(commande.LIVRAISON_PREVUE_LE),
													commande.COMMENTAIRES,
													commande.BOOL_DEVIS,
													commande.MONTANT,
													commande.SEGMENT_FIXE,
													1,
													commande.IDTYPE_COMMANDE,
													commande.IDTYPE_COMMANDE,
													commande.IDGROUPE_REPERE,
													commande.NUMERO_COMMANDE,
													commande.IDPROFIL_EQUIPEMENT,
													articles);
    		}
			else
			{
				var op2 : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			/*"fr.consotel.consoview.inventaire.commande.GestionCommande",*/
		    																		"fr.consotel.consoview.inventaire.sncf.commande_sncf",
		    																		"enregistrerCommandeActePour",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result > 0)	    	{
																			    			commande.IDCOMMANDE = Number(event.result);
																			    			idCommande = Number(event.result);
																			    			ConsoviewAlert.afficherOKImage("Commande enregistrée !");
																				    		dispatchEvent(new CommandeEvent(CommandeEvent.COMMANDE_CREATED));
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Erreur");
																				    	}	
																			    	}
	    																		);
				RemoteObjectUtil.callService(op2,	commande.IDPOOL_GESTIONNAIRE,	
													commande.IDCOMPTE_FACTURATION,
													commande.IDSOUS_COMPTE,
													commande.IDOPERATEUR,
													commande.IDREVENDEUR,
													commande.IDCONTACT,
													commande.IDTRANSPORTEUR,
													commande.IDSITELIVRAISON,
													commande.NUMERO_TRACKING,
													commande.LIBELLE_COMMANDE,
													commande.REF_CLIENT,
													commande.REF_OPERATEUR,
													formatDate(commande.DATE_COMMANDE),
													formatDate(commande.LIVRAISON_PREVUE_LE),
													commande.COMMENTAIRES,
													commande.BOOL_DEVIS,
													commande.MONTANT,
													commande.SEGMENT_FIXE,
													1,
													commande.IDTYPE_COMMANDE,
													commande.IDTYPE_COMMANDE,
													commande.IDGROUPE_REPERE,
													commande.NUMERO_COMMANDE,
													commande.IDPROFIL_EQUIPEMENT,
													commande.IDACTEPOUR,
													articles);
			}
	    }
	    
	    private function formatDate(dateobject:Object):String
		{
			var day:String = dateobject.date;
			if(day.length < 2)
			{day = "0" + day;}
			var month:int = Number(dateobject.month) + 1;
			var temp:String = month.toString();
			if(temp.length < 2)
			{temp = "0" + temp;}
			var year:String = dateobject.fullYear;
			return year + "/" + temp + "/" + day;
		}

		private function mappDataCommande(commande : Commande, value : Object):void{
				commande.NUMERO_COMMANDE = value.NUMERO_OPERATION;
				commande.IDOPERATEUR = value.OPERATEURID;
				commande.LIBELLE_COMMANDE = value.LIBELLE;
				commande.REF_CLIENT = value.REF_CLIENT;
				commande.REF_OPERATEUR = value.REF_REVENDEUR;
				commande.LIVRAISON_PREVUE_LE = value.DATE_EFFET_PREVUE;
				commande.COMMENTAIRES = value.COMMENTAIRES;
				commande.LIBELLE_REVENDEUR = value.LIBELLE_REVENDEUR;
				commande.LIBELLE_OPERATEUR = value.LIBELLE_OPERATEUR;
				commande.LIBELLE_COMPTE = value.LIBELLE_COMPTE;
				commande.LIBELLE_SOUSCOMPTE = value.LIBELLE_SOUSCOMPTE;
				commande.NUMERO_TRACKING = value.NUMERO_TRACKING;
				commande.IDTRANSPORTEUR = value.IDTRANSPORTEUR;
				commande.IDSITELIVRAISON = value.IDSITE_LIVRAISON;
				commande.LIBELLE_SITELIVRAISON = value.LIBELLE_SITELIVRAISON;	
				commande.IDPOOL_GESTIONNAIRE = value.IDPOOL_GESTIONNAIRE;	
				commande.LIBELLE_POOL = value.LIBELLE_POOLGESTIONNAIRE;
				commande.IDGESTIONNAIRE_MODIF = value.IDGESTIONNAIRE_MODIF;
				commande.IDGESTIONNAIRE_CREATE = value.IDGESTIONNAIRE_CREATE;
				commande.CREEE_PAR = value.LIBELLEGESTIONNAIRE_CREATE;
				commande.MODIFIEE_PAR = value.LIBELLEGESTIONNAIRE_MODIF;
				commande.SEGMENT_FIXE = value.SEGMENT_FIXE;
				commande.SEGMENT_MOBILE = value.SEGMENT_MOBILE;
				commande.ENCOURS = Number(value.EN_COURS);
				commande.EXPEDIE_LE =  value.EXPEDIE_LE; 
				commande.PATRONYME_CONTACT = value.NOM_CONTACT;
				commande.IDCONTACT = value.IDCDE_CONTACT;
				commande.LIVREE_LE = value.LIVREE_LE;
				commande.IDTYPE_COMMANDE = Number(value.IDTYPE_OPERATION);
				commande.IDINV_ETAT = Number(value.IDINV_ETAT);
				///commande.MONTANT = Number(value.MONTANT);
				commande.IDSOCIETE = value.IDSOCIETE;
		}
		
/////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        public static const EMPLOYE_CREATED:String="employeCreated";
        public static const EMPLOYE_UPDATED:String="employeUpdated";
        public static const EMPLOYE_DELETED:String="employeDeleted";
        public static const EMPLOYE_LOADED:String="employeLoaded";

        public function set tmpEmploye(value:Collaborateur):void
        {
            _tmpEmploye = value;
        }

        public function get tmpEmploye():Collaborateur
        {
            return _tmpEmploye;
        }

		public function fournirListeEmployes(idPool:Number, clef:String): void
        {
            var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                                                                                 "fr.consotel.consoview.inventaire.employes.EmployesUtils",
                                                                                 "fournirListeEmployes",
                                                                                 fournirListeEmployesResultHandler);
             RemoteObjectUtil.callService(op,idPool,clef)
        }
        
         protected function fournirListeEmployesResultHandler(event:ResultEvent): void
        {
        	var arrayC:ArrayCollection = new ArrayCollection();
        	var arrayC1:ArrayCollection = new ArrayCollection();
        	listeEmployes = new ArrayCollection();
        	
           	arrayC = event.result as ArrayCollection;
           	arrayC1 = event.result as ArrayCollection;
           	for(var i:int = 0;i < arrayC.length;i++)
           	{
				arrayC[i].SELECTED = false;
				listeEmployes.addItem(arrayC[i]);
           	}
           	
           	arrayC = new ArrayCollection();
			arrayC = formatterData(arrayC1);
			dispatchEvent(new Event("employeLoaded",true));
//            if(listeEmployes.length > 0)
//            {
//                fournirDetailEmploye(arrayC[0]) ; 
//            }
        }
        
        private   function formatterData(values : ICollectionView):ArrayCollection
        {
            var retour : ArrayCollection = new ArrayCollection();
            if (values != null)
            {
                var cursor : IViewCursor = values.createCursor();
                while(!cursor.afterLast){
                   
                    var employeObj:Collaborateur = new Collaborateur();                   
                    employeObj.NOMPRENOM = cursor.current.NOM;
                    employeObj.IDEMPLOYE = cursor.current.IDEMPLOYE;
                    employeObj.IDGROUPE_CLIENT = cursor.current.IDGROUPE_CLIENT;
                    employeObj.MATRICULE = cursor.current.MATRICULE;
                    employeObj.CLE_IDENTIFIANT = cursor.current.CLE_IDENTIFIANT;
                    employeObj.INOUT =     cursor.current.INOUT;               
                    if(employeObj.INOUT > 0)
                    {
                        retour.addItem(employeObj);   
                    }
                    cursor.moveNext();
                }   
            }
            return retour;
        }
        
        public var selectedEmploye:Collaborateur = new Collaborateur();
        
        public function fournirDetailCollaborateur(idCollaborateur:int):void
        {
           
            var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
                                                                                 "fr.consotel.consoview.inventaire.employes.EmployesUtils",
                                                                                 "fournirDetailCollaborateur",
                                                                                 function fournirDetailCollaborateurResultHandler(event:ResultEvent):void
                                                                                 {                                                                                   
                                                                                    selectedCollaborateur = event.result as Collaborateur;
                                                                                    switch(selectedCollaborateur.CIVILITE)
                                                                                    {
                                                                                        case "1":
                                                                                        {
                                                                                            selectedCollaborateur.LCIVILITE = "Mr.";
                                                                                            break;   
                                                                                        }
                                                                                        case "2":
                                                                                        {
                                                                                            selectedCollaborateur.LCIVILITE = "Mme.";
                                                                                            break;   
                                                                                        }
                                                                                        case "3":
                                                                                        {
                                                                                            selectedCollaborateur.LCIVILITE = "Mlle";
                                                                                            break;   
                                                                                        }
                                                                                        default :
                                                                                        {
                                                                                            selectedCollaborateur.LCIVILITE = "";
                                                                                            break;
                                                                                        }
                                                                                    }
//                                                                                    tmpEmploye =  ObjectUtil.copy(selectedCollaborateur) as Collaborateur ;
                                                                                    dispatchEvent(new Event(EMPLOYE_LOADED));
                                                                                }
                                                                                );
             if(idCollaborateur)
             {
                 RemoteObjectUtil.callService(op,idCollaborateur)   
             }
             else
             {
                 RemoteObjectUtil.callService(op,idCollaborateur)
             }
        }
        
        public function fournirArticlesCommande(commande:Commande): void
	    {	
	    	if(commande != null)
	    	{
	    		var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.commande.GestionPanier",
	    																			"fournirArticlesCommande",
	    																			function(event:ResultEvent):void
																			    	{
																			    		if(event.result)	    	
																			    		{																			    			
																				    		
																				    		listeArticles = new XML(event.result[0].MODELEXML);
																				    		dispatchEvent(new Event("articlesArrived"));
//																				    		calculTotalByArticle();
//																				    			
//																				    		commande.MONTANT = calculTotal();																		    		
																				    	}
																				    	else
																				    	{
																				    		Alert.show("Pas de donnée");
																				    	}	
																			    	});
				RemoteObjectUtil.callService(op,commande.IDCOMMANDE)
	    	}
	    }
/////////////////////////\`\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	
		public function addListConfigurationRecorded(): void
	    {	   
    		listConfigurationRecorded = new ArrayCollection();
    		
//    		var opData : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
//							"fr.consotel.consoview.inventaire.equipement.utils.EquipementsUtils",
//							"fournirListeEquipementsCatalogueRevendeur", 
//							addListConfigurationRecordedResultHandler);
//			RemoteObjectUtil.callService(opData)
	    		dispatchEvent(new Event("listedConfigurationRecorded"));
		}
		
		protected function addListConfigurationRecordedResultHandler(event:ResultEvent): void
	    {
	    	listConfigurationRecorded = event.result as ArrayCollection;
	    	dispatchEvent(new Event("listedConfigurationRecorded"));
	    }
/////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//----------------- NOUVELLES PROCEDURES ---------------------

		//CHEMIN D'ACCES AUX PROCEDURES
		private var pathCommande:String = "fr.consotel.consoview.inventaire.commande.GestionCommande";
	
		//enregiste un modèle de commande
		public function saveModeleProduct(commande:Commande): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			pathCommande,
	    																			"saveModeleCommande",
	    																			saveModeleProductResultHandler);
			RemoteObjectUtil.callService(op,commande.IDPOOL_GESTIONNAIRE,
											commande.IDOPERATEUR,
											commande.IDREVENDEUR,
											commande.IDCONTACT,
											commande.IDSITELIVRAISON,
											commande.LIBELLE_CONFIGURATION,
											commande.REF_CLIENT,
											commande.REF_OPERATEUR,
											commande.COMMENTAIRES,
											commande.MONTANT,
											commande.SEGMENT_FIXE,
											1,
											commande.IDTYPE_COMMANDE,
											commande.IDGROUPE_REPERE,
											commande.ARTICLES);	
	    }
	    
	    private function saveModeleProductResultHandler(re:ResultEvent): void
	    {
	    	if(re.result > 0)
	    	{
	    		dispatchEvent(new Event("saveModeleOk"));
	    	}
	    	else
	    	{
	    		Alert.show("Erreur d'enregistrement!","Consoview");
	    	}
	    }

		//obtient la liste des modèles enregistrés
		public function getListModeleCommande(idPool:int): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			pathCommande,
	    																			"getListModeleCommande",
	    																			getListModeleCommandeResultHandler);
	  		RemoteObjectUtil.callService(op,idPool);	
	    }
	    
	    private function getListModeleCommandeResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		listConfiguration = re.result as ArrayCollection;
	    		dispatchEvent(new Event("listeConfigurationOk"));
	    	}
	    	else
	    	{
	    		Alert.show("Pas de données!","Consoview");
	    	}
	    }	

		//obtient les modèles enregistrés
		public function getModeleCommande(idModele:Number): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			pathCommande,
	    																			"getModeleCommande",
	    																			getModeleCommandeResultHandler);
	  		RemoteObjectUtil.callService(op,idModele);	
	    }
	    public var configObject:Object = new Object();
	    
	    private function getModeleCommandeResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		configObject = re.result[0] as Object;
	    		listeModele = XML(re.result[0].XML);
	    		dispatchEvent(new Event("listeModeleOk"));
	    	}
	    	else
	    	{
	    		Alert.show("Pas de données!","Consoview");
	    	}
	    }	

		//supprime un modèle enregisstré
		public function deleteModeleCommande(idModele:Number): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			pathCommande,
	    																			"deleteModeleCommande",
	    																			deleteModeleCommandeResultHandler);		
	    	RemoteObjectUtil.callService(op,idModele);	
	    }
	    
	    private function deleteModeleCommandeResultHandler(re:ResultEvent): void
	    {
	    	if(re.result > 0 )
	    	{
	    		dispatchEvent(new Event("eraseModeleOk"));
	    	}
	    	else
	    	{
	    		Alert.show("Effecement impossible!","Consoview");
	    	}
	    }	

		//obtient l'historique de gestion SAV/COMMANDE/GESTION
		public function getHistoriqueAction(idPool:int,dateDebut:String,dateFin:String,clef:String): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			"fr.consotel.consoview.inventaire.commande.GestionCommande",
	    																			"getHistoriqueAction",
	    																			getHistoriqueActionResultHandler);		
	    	RemoteObjectUtil.callService(op,idPool,dateDebut,dateFin,clef);	
	    }
	    
	    private function getHistoriqueActionResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		listeHistoriqueAction = addObjectSelected(re.result as ArrayCollection);
	    		getObjectForFiltre();
	    		dispatchEvent(new Event("listeHistoriqueActionOk"));
	    	}
	    	else
	    	{
	    		Alert.show("Pas de données!","Consoview");
	    	}
	    }
	    
private function getObjectForFiltre():void
	    {
	    	var listUserTemp	:ArrayCollection = new ArrayCollection();
	    	var listTypeTemp	:ArrayCollection = new ArrayCollection();
	    	for(var i:int = 0;i < listeHistoriqueAction.length;i++)
	    	{
	    		listUserTemp.addItem(listeHistoriqueAction[i]);
	    		listTypeTemp.addItem(listeHistoriqueAction[i]);
	    	}
	    	listUser = containThis(listUserTemp, true);
	    	listType = containThis(listTypeTemp, false);
	    }
	    
	    private function containThis(values:ArrayCollection, bool:Boolean):ArrayCollection
	    {
			var arrayAdded	:ArrayCollection = new ArrayCollection();
			if(bool)
			{
				arrayAdded = containQUI(values);
			}
			else
			{
				arrayAdded = containType(values);
			}
			return arrayAdded;
	    }
	    
	    private function containQUI(values:ArrayCollection):ArrayCollection
	    {
	    	var arrayAdded:ArrayCollection = new ArrayCollection();
			
			if (values != null)
			{
				var cursor : IViewCursor = values.createCursor();
								
				while(!cursor.afterLast)
				{
					if(ConsoviewUtil.getIndexById(arrayAdded,"APP_LOGINID",cursor.current.APP_LOGINID) < 0)
					{
						arrayAdded.source.push({APP_LOGINID:cursor.current.APP_LOGINID,QUI:cursor.current.QUI});
					}   				    			
					cursor.moveNext();
				}
			}
	    	return arrayAdded;
	    }
	    
	    private function containType(values:ArrayCollection):ArrayCollection
	    {
	    	var arrayAdded:ArrayCollection = new ArrayCollection();
			var libelle:String = "";
			if (values != null)
			{
				var tri:Sort = new Sort();
		    	tri.fields = [new SortField("DESC_ACTION", true)];
		    	values.sort = tri;
		    	values.refresh();
		    	values.sort = null;
				
				for(var i:int = 0;i < values.length;i++)
	    		{
	    			if(libelle != values[i].DESC_ACTION)
	    			{
	    				if(!numberInString(values[i].DESC_ACTION))
	    				{
							arrayAdded.addItem(values[i]);
	    				}
	    			}
	    		}
			}
	    	return arrayAdded;
	    }
	    
	    private function numberInString(valueLibelleType:String):Boolean
	    {
	    	var OK:Boolean = false;
	    	for(var i:int = 0;i < 10;i++)
    		{
    			if(valueLibelleType.indexOf(i.toString()) > 0)
    			{
    				return true;
    			}
    		}
	    	return OK;
	    }
	    
	    private function addObjectSelected(arrayToAddObject:ArrayCollection):ArrayCollection
	    {
	    	var object		:Object;
	    	var arrayAdded	:ArrayCollection = new ArrayCollection();
	    	
	    	for(var i:int = 0;i < arrayToAddObject.length;i++)
	    	{
	    		object = new Object();
	    		object = arrayToAddObject[i] as Object;
	    		if(arrayToAddObject[i].COMMANDE == null)
	    		{
	    			object.COMMANDE = "";
	    		}
	    		if(arrayToAddObject[i].SAV == null)
	    		{
	    			object.SAV = "";
	    		}
	    		object.SELECTED = false;
	    		arrayAdded.addItem(object);
	    	}
	    	
	    	return arrayAdded;
	    }

		//info sur l'equipements
		public function getProductFeature(idEquipement:int): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			pathCommande,
	    																			"getProductFeature",
	    																			getProductFeatureResultHandler);		
	  		RemoteObjectUtil.callService(op,idEquipement,3);//CHANGER '3' POUR L'INTERNATIONNALISATION (LE RECUPERER EN SESSION)	
	    }
    
	    private function getProductFeatureResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		
	    		listeFeature = re.result[0] as ArrayCollection;
	    		
	    		
	    		listePhotos = re.result[1] as ArrayCollection;
//	    		attribuFeature();
	    		dispatchEvent(new Event("listeFeatureOk"));
	    	}
	    	else
	    	{
	    		Alert.show("Pas de données!","Consoview");
	    	}
	    }
	    
	    private function attribuFeature():void
	    {
	    	var arra:ArrayCollection = new ArrayCollection();
	    	var arra2:ArrayCollection = new ArrayCollection();
	    	var obj:Object = new Object();
	    	for(var i:int = 0;i < listeFeature.length;i++)
	    	{
	    		obj = new Object();
	    		if(i != 0)
	    		{
	    			if(listeFeature[i].GROUPE == listeFeature[i-1].GROUPE)
	    			{
	    				obj.ITEMS = listeFeature[i];
	    				arra.addItem(obj);
	    			}
	    			else
	    			{
	    				arra2.addItem(arra);
	    				arra = new ArrayCollection();
	    				var objName:Object = new Object();
	    				objName.GROUPENAME 	= listeFeature[i].GROUPE;
	    				arra.addItem(objName);
	    				obj.ITEMS		= listeFeature[i];
	    				arra.addItem(obj);
	    			}
	    		}
	    		else
	    		{
	    			var objName2:Object = new Object();
	    			objName2.GROUPENAME 	= listeFeature[i].GROUPE;
	    			arra.addItem(objName2);
	    			obj.ITEMS		= listeFeature[i];
	    			arra.addItem(obj);
	    		}
	    	}
	    	
	    	listeFeature = new ArrayCollection();
	    	listeFeature = arra2;
	    }

		//obtient les accessoires compatibles avec l'appareil sélectionné
		public function getProductRelated(idEquipement:int,chaine:String = ""): void
	    {
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																			pathCommande,
	    																			"getProductRelated",
	    																			getProductRelatedResultHandler);		
	    	RemoteObjectUtil.callService(op,idEquipement,0,chaine,3);//CHANGER '3' POUR L'INTERNATIONNALISATION (LE RECUPERER EN SESSION)
	    }
	    
	    private function getProductRelatedResultHandler(re:ResultEvent): void
	    {
	    	if(re.result)
	    	{
	    		var arrayC:ArrayCollection = re.result as ArrayCollection;
	    		if(arrayC.length > 0)
	    		{
		    		if(arrayC[0].hasOwnProperty("ERREUR"))
		    		{
		    			Alert.show("Pas de données!","Consoview");
		    		}
		    		else
		    		{
		    			listeProductRelated = new ArrayCollection();
		    			var reult:ArrayCollection = re.result as ArrayCollection;
		    			var cursor:IViewCursor = reult.createCursor();
		    			var item:Object;
		    			
		    			while(!cursor.afterLast)
		    			{
		    				item = cursor.current;
		    				item.PRIX_UNIT = item.PRIX_CATALOGUE;
		    				item.LIBELLE_PRODUIT = item.LIBELLE_MODELE; 
		    				item.LIBELLE_EQ = item.LIBELLE_MODELE;
		    				listeProductRelated.addItem(item);
		    				cursor.moveNext();			
		    			}
		    			dispatchEvent(new Event("listeProductRelatedOk"));
		    		}
	    		}
	    		else
	    		{
	    			dispatchEvent(new Event("listeProductRelatedNoAccessory"));
	    		}
	    	}
	    	else
	    	{
	    		Alert.show("Pas de données!","Consoview");
	    	}
	    }

//----------------- FIN NOUVELLES PROCEDURES ---------------------
		
	}
}