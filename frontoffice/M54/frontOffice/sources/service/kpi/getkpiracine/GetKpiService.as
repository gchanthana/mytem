package service.kpi.getkpiracine
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	public class GetKpiService
	{
		private var _model:GetKpiModel;
		public var handler:GetKpiHandler;
		private var remote:RemoteObject;
	
		public function GetKpiService()
		{
			this._model = new GetKpiModel();
			this.handler = new GetKpiHandler(_model);
		}		
		
		public function get model():GetKpiModel
		{
			return this._model;
		}
		
		public function getListKpiByRacine():void
		{	
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			
			remote.source="fr.consotel.consoview.M54.commun.ListeComposantService";
			remote.addEventListener(ResultEvent.RESULT,handler.getListKpiResultHandler,false,0,true);
			remote.getAllComposant(1);/** 1 : pour récupérer la liste des kpi */
		}		
	}
}