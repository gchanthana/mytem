package gestionparc.services.incident
{
	import flash.events.EventDispatcher;
	
	import gestionparc.entity.IncidentVO;
	import gestionparc.entity.InterventionVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	
	import mx.collections.ArrayCollection;

	public class IncidentDatas extends EventDispatcher
	{

		private var _incident:IncidentVO=new IncidentVO();


		/**
		 * Dispatch un événements pour signaler que l'enregistrements des informations
		 * concernant la fiche Incident du incident s'est bien déroulée.
		 * Cette événement transporte le numéro de l'incident (pour les créations d'incidents)
		 * et l'idIncident (pour les suppressions d'incidents).
		 */
		public function treatSetInfosIncident(obj:Object):void
		{
			var idIncident:int=(obj[1] as int);
			var numIncident:String=(obj[2] as String);
			var myObj:Object=new Object();
			myObj.idIncident=idIncident;
			myObj.numIncident=numIncident;
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_INCIDENT_SAVED, myObj));
			refreshData()
		}

		/**
		 * Dispatch un événements pour signaler que la liste des interventions concernant l'incident
		 * sélectionné du incident a bien été chargée.
		 * Cette événement transporte la liste des interventions
		 */
		public function treatDataGetListeIntervention(obj:Object):void
		{
			var listeIntervention:ArrayCollection=new ArrayCollection();
			var lenListeIntervention:int=(obj as ArrayCollection).length;

			for (var i:int=0; i < lenListeIntervention; i++)
			{
				var myIntervention:InterventionVO=new InterventionVO();
				myIntervention.idIntervention=obj[i].IDINTERVENTIONS;
				myIntervention.numIntervention=obj[i].NUM_INTERVENTION;
				myIntervention.dateIntervention=obj[i].DATE_EVENEMENT;
				myIntervention.idTypeIntervention=obj[i].IDTYPE_INTERVENTION;
				myIntervention.typeIntervention=obj[i].TYPE_INTERVENTION;
				myIntervention.libelle=obj[i].LIBELLE_INTERVENTION;
				myIntervention.savedByUser=obj[i].LOGIN_NOM + " " + obj[i].LOGIN_PRENOM;
				myIntervention.horaire=obj[i].PERIODE;
				myIntervention.prix=obj[i].COUT_INTERVENTION;
				myIntervention.description=obj[i].COMMENTAIRE_INTERVENTION;
				myIntervention.isClose=(obj[i].IS_SAV == 1) ? 0 : 1;

				listeIntervention.addItem(myIntervention);
			}

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_INTER_LOADED, listeIntervention));
		}

		public function userProfil(idprofile:int):void
		{
			var myObj:Object=new Object();
			myObj.idprofile=idprofile;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_TYPE_PROFILE, myObj));
		}

		public function mailRevendeur(listeEmail:ArrayCollection):void
		{
			var myObj:Object=new Object();
			myObj.listeEmail=listeEmail;

			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LISTE_MAIL_REVENDEUR, myObj));
		}

		/**
		 * Dispatch un événements pour signaler que l'enregistrements des informations
		 * concernant la fiche l'intervention de l'incident de l'équipement s'est bien déroulée.
		 * Cette événement transporte le numéro de l'intervention (pour les créations d'interventions)
		 * et l'idIntervention (pour les suppressions d'interventions).
		 */
		public function treatSetInfosIntervention(obj:Object):void
		{
			var idIntervention:int=(obj[1] as int);
			var numIntervention:String=(obj[2] as String);
			var myObj:Object=new Object();
			myObj.idIntervention=idIntervention;
			myObj.numIntervention=numIntervention;
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_INTERVENTION_SAVED, myObj));
		}

		private function refreshData():void
		{
			SpecificationVO.getInstance().boolHasChanged=true;
			SpecificationVO.getInstance().refreshData();
		}

		public function set incident(value:IncidentVO):void
		{
			_incident=value;
		}

		public function get incident():IncidentVO
		{
			return _incident;
		}
	}
}
