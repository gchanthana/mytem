package classe
{
	import mx.messaging.AbstractConsumer;
	
	public class Comment
	{
		private var _IDCOMMENTAIRE	: 	int
		private var _LIBELLE		: 	String
		private var _NOM			: 	String
		private var _PRENOM			: 	String
		private var _DATE			: 	Date
		private var _WRITABLE		: 	Boolean	= false
		
	// GETTER	
		public function get IDCOMMENTAIRE():int
		{
			return _IDCOMMENTAIRE
		}
		public function get LIBELLE():String
		{
			return _LIBELLE
		}
		public function get NOM():String
		{
			return _NOM	
		}
		public function get PRENOM():String
		{
			return _PRENOM
		}
		public function get DATE():Date
		{
			return _DATE	
		}
		public function get WRITABLE():Boolean
		{
			return _WRITABLE
		}
	// SETTER	
		public function set IDCOMMENTAIRE(i:int):void
		{
			_IDCOMMENTAIRE = i
		}
		public function set LIBELLE(str:String):void
		{
			_LIBELLE = str
		}
		public function set NOM(str:String):void
		{
			_NOM = str;
		}
		public function set PRENOM(str:String):void
		{
			_PRENOM = str
		}
		public function set DATE(dt:Date):void
		{
			_DATE = dt;	
		}
		public function set WRITABLE(bool:Boolean):void
		{
			_WRITABLE = bool
		}
		
		public function Comment()
		{
		}

	}
}