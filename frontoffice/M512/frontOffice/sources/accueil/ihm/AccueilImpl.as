package accueil.ihm
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.containers.HBox;
	import mx.events.FlexEvent;
	
	import accueil.ihm.composant.AccueilGraph.AccueilGraphIHM;
	import accueil.ihm.composant.AccueilMenu.AccueilMenuIHM;
	
	[Bindable]
	public class AccueilImpl extends HBox
	{
		public var menu:accueil.ihm.composant.AccueilMenu.AccueilMenuIHM;
		public var graph:AccueilGraphIHM;
		
		private var minuteTimer:Timer = new Timer(300, 1);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 	
		public function AccueilImpl() 
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
			minuteTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
        }
		private function onTimerComplete(event:TimerEvent):void
		{
			getKPI();
		}
		private function afterCreationComplete(event:FlexEvent):void 
		{
			afterPerimetreUpdated();
		}
		private function getMenu():void
		{
			menu.init();
		}
		private function getKPI():void
		{
			graph.init();
		}
		public function afterPerimetreUpdated():void 
		{
			menu.reset();
			graph.reset();
			getMenu();
			minuteTimer.start();
		}
	}
}