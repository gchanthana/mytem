﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	   <!--- Crée ou met à jour un template de collecte	         
	          si vo.id = 0 : Mode CREATION
	          si vo.id !=0 : Mode UPDATE		
		 retourne -1 ou code erreur en cas d'echec'
	 --->
	<cffunction name="executeAct" access="public" returntype="struct" output="false">		
		<cfargument name="data" type="struct" required="true" />
    <!---	<cfargument name="p_idcollecteTemplate"  hint="l'id de la collecte" type="numeric" />
		<cfargument name="p_uidcollecteTemplate" hint="l'identifiant unique du template ( pour stockage des fichiers)" type="string" />
		<cfargument name="p_idcollecteType" hint="l'id du type de donnees ( Facturation, Usages,Facturation et usages)" type="numeric" />
		<cfargument name="p_idcollecteMode"  hint="l'id du mode de recuperation du template (Push, Pull ...)" type="numeric" />
		<cfargument name="p_operateurId" hint="l'id de l'operateur" type="numeric" />
		<cfargument name="p_libelleTemplate" hint="le libelle du template" type="string" />
		<cfargument name="p_isPublished" hint="boolean indiquant si le template est publiee ou non" type="numeric" />
		<cfargument name="p_isCompleted" hint="boolean indiquant si le template est complet ou non" type="numeric" />
		<cfargument name="p_prestaPerifacturation" hint="prestation de perifacturation" type="string" />
		<cfargument name="p_instructionsPath" hint="url de la PJ d'instructions pour la perifacturation" type="string" />		
		<cfargument name="p_modeSouscription"  hint="mode de souscription ( extranet, contrat ...)" type="numeric" />
		<cfargument name="p_modeSouscriptionPath" hint="url de la PJ d'aide au mode de souscription" type="string" />
		<cfargument name="p_modeSouscriptionUrl" hint="url du mode de souscription extranet" type="string" />
		<cfargument name="p_delaiDispo" hint="delai de mise e disposition ( en j ouvres)" type="numeric" />
		<cfargument name="p_delaiDispoInfos" hint="info sur le delai de mise e disposition" type="string" />
		<cfargument name="p_contraintes" hint="boolean indiquant s'il y a contraintes ou non" type="numeric" />
		<cfargument name="p_contraintesInfos" hint="infos sur la contraintes ( ssi  contraintes=1)" type="string" />
		<cfargument name="p_anteriorite" hint="bool indiquant s'il y a anteriorite des donnees ou non" type="numeric" />
		<cfargument name="p_anterioriteInfos" hint="infos sur l'anteriorite ( ssi  anteriorite=1)" type="string" />
		<cfargument name="p_couts" hint="boolean indiquant s'il y a un cout ou non (payant ou gratuit)" type="numeric" />
		<cfargument name="p_coutsInfos" hint="Infos sur les couts ( ssi couts=1)" type="string" />
		<cfargument name="p_contact" hint="contact" type="string" />
		<cfargument name="p_contactInfos" hint="Infos sur le contact" type="string" />
		<cfargument name="p_libelleRoic" hint="libelle de la Ref Operateur Id Client" type="string" />
		<cfargument name="p_recuperationRoic" hint="Mode de recuperation du Roic" type="string" />
		<cfargument name="p_roicInfos" hint="Infos sur le  Ref Operateur Id Client" type="string" />
		<cfargument name="p_localisationRoic" hint="Loaclisation du Roic" type="string" />
		<cfargument name="p_localisationRoicPath" hint="Url de la PJ d'infos sur la localisation du Roic" type="string" />
		<cfargument name="p_libelleRocf" hint="libelle de la Ref Operateur Chaine de Factures" type="string" />
		<cfargument name="p_localisationRocf" hint=" localisation de la Ref Operateur Chaine de Factures" type="string" />
		<cfargument name="p_localisationRocfPath" hint="url de la Pj informant sur la localisation de la Ref Operateur Chaine de Factures" type="string" />
		<cfargument name="p_iduserCreate" hint="l'id de l'utilisateur ayant cree le template" type="numeric" />
		<cfargument name="p_idUserModif" hint="l'id de l'utilisateur ayant modifie  le template" type="numeric" /> --->
        <cftry>
	   <cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_M25.IU_template">
	    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_idcollecteTemplate#"  >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_uidcollecteTemplate#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_idcollecteType#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_idcollecteMode#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_operateurId#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_libelleTemplate#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_isPublished#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_isCompleted#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_prestaPerifacturation#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_instructionsPath#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_modeSouscription#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_modeSouscriptionPath#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_modeSouscriptionUrl#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_delaiDispo#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_delaiDispoInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_contraintes#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_contraintesInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_anteriorite#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_anterioriteInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_couts#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_coutsInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_contact#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_contactInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_libelleRoic#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_recuperationRoic#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_roicInfos#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_localisationRoic#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_localisationRoicPath#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_libelleRocf#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_localisationRocf#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#data.p_localisationRocfPath#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_iduserCreate#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#data.p_idUserModif#">						 		
			<cfprocparam  variable="p_retour" type="Out" cfsqltype="CF_SQL_INTEGER">
		</cfstoredproc>
		
			<cfif p_retour EQ 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ019", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ019", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ019", 0, ThrowCfCatchError("AJ019",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		<cfreturn p_retour >
	</cffunction>
</cfcomponent>