<cfinterface displayName="fr.consotel.consoview.api.ibis.publisher.service.IPublisherService">
	<cffunction name="getReportDefaultXmlConfig" returntype="XML" description="Return report default XML config which is created using default report parameters.">
		<cfargument name="reportAbsolutePath" type="string" required="true" hint="Report Definition (Parameters,Templates,etc...)">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
	</cffunction>

	<cffunction name="getDeliveryClass" returntype="string"
				description="Return corresponding Delivery Definition Component Name OR NULL (as String) if delivery type not found. This is used when instanciating a delivery definition.">
		<cfargument name="deliveryType" type="string" required="true" hint="A delivery type. Available values are : LOCAL,FTP,EMAIL">
	</cffunction>

	<cffunction name="setTemplateId" returntype="boolean" description="Set templateId for XML configuration.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="templateId" type="string" required="true" hint="Template Id">
	</cffunction>

	<cffunction name="setOutputFormat" returntype="boolean" description="Set output format for XML configuration.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="outputFormat" type="string" required="true" hint="Output Format. Available values are : html,pdf,rtf,excel,excel2000,ppt,mhtml,csv,xml">
	</cffunction>

	<cffunction name="setParameterValues" returntype="boolean" description="Set values for parameter specified in xmlConfig.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="parameterName" type="string" required="true" hint="Parameter Name">
		<cfargument name="parameterValues" type="array" required="true" hint="Parameter Values as a List of String values">
	</cffunction>
	
	<cffunction name="setDeliveryConfig" returntype="boolean" description="Set delivery options for specified XML configuration.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="deliveryDefinition" type="fr.consotel.consoview.api.ibis.publisher.delivery.IDeliveryDefinition" required="true" hint="Delivery Definition">
	</cffunction>
	
	<cffunction name="runReport" returntype="any" description="Run report and returns report result as a base64 binary object.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
	</cffunction>
	
	<cffunction name="scheduleReport" returntype="numeric" description="Schedule report and returns schedule job_id.">
		<cfargument name="xmlConfig" type="XML" required="true" hint="XML Configuration Infos used to execute report (Contains report path,parameters,template id,output format,etc...)">
		<cfargument name="jobName" type="string" required="true" hint="Publisher User Job Name">
		<cfargument name="userId" type="string" required="true" hint="Publisher Username">
		<cfargument name="pwd" type="string" required="true" hint="Publisher User Password">
	</cffunction>
</cfinterface>