package univers.inventaire.equipements.gestionflottemobile
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	
	
	public class UtilGetListeDispo
	{
		private var sansEmp : int;
		private var sansTerm : int;
		private var sansSIM : int;
		private var sansLigne : int;
		private var idEmploye : String;
		private var resultHandler : Function;
		private var dataProvider : ArrayCollection = new ArrayCollection;
		private var allSim : Boolean = false;
		
		private var nbrAppel:int = 0;
		
		public function UtilGetListeDispo(sansEmp :int,sansTerm :int, sansSIM :int,sansLigne :int,idEmploye :String,resultHandler : Function,allSIM:Boolean=true)
		{
			this.sansEmp = sansEmp;
			this.sansLigne = sansLigne;
			this.sansSIM = sansSIM;
			this.sansLigne = sansLigne;
			this.idEmploye=idEmploye;
			this.resultHandler = resultHandler;
			this.allSim=allSIM;
			
			var cas : int = 1*sansEmp + 2*sansTerm + 4*sansSIM + 8*sansLigne;
		
			switch (cas)
			{
			
				case 0:	
					//Liste des collaborateurs
					getListeCollaborateur();
				break;
				case 1:
				
				break;
				case 2:
				
				break;
				case 3:
					//Liste des SIM affectées a l'employé qui possède le terminal
					getSIMOfEmp();
					if(allSIM==false){
						//Liste des SIM sans terminal, sans employé,
						getSIM_sansEmp_sansTerm();
					}
					else
					{
						get_all_SIM_sansEmp_sansTerm();
					}
					
				break;
				case 4:
				
				break;
				
				case 5:
					//Liste des Terminaux sans Employé et sans SIM
					getTerminal_sansEmp_sansSim();
					//Liste des terminaux affectés à l'employé
					getTermOfEmp();
				break;
				case 6:
				
				break;
				case 7:
					//Liste LIGNE sans SIM
					getLigne_sansSIM();
									
				break;
				case 8:
				
				break;
				case 9:
					//Liste des SIM sans Ligne
					getSIM_sansLigne();
				break;
				case 10:
				
				break;
				case 11:
				
				break;
				case 12:
				
				break;
				case 13:
					
				break;
				case 14:
				
				break;
				case 15:
				
				break;
				case 16:
				
				break;
			
			}
		}
		/*
		--------------------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------CAS 5--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		*/
		private function getTermOfEmp():void
		{
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
		 	var xmlParam :XML = XmlParamUtil.createXmlParamObject();
		 	XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","Groupe racine",GROUPE_INDEX);
		 	XmlParamUtil.addParam(xmlParam,"ID_EMPLOYE ","ID du collaborateur",idEmploye);
		   		 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getAffectedTermToEmp",
							initResultHandler);
			
			RemoteObjectUtil.callService(opData,xmlParam);						
		}
		private function getTerminal_sansEmp_sansSim():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
			 var xmlParam :XML = XmlParamUtil.createXmlParamObject();
			 XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","id Groupe racine",GROUPE_INDEX);
			   var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_terminaux_sans_sim",
							initResultHandler_getTerminal_sansEmp_sansSim);
			RemoteObjectUtil.callService(opData,xmlParam);						
		}
		private function initResultHandler_getTerminal_sansEmp_sansSim(re : ResultEvent):void
		{
			if (re.result)
			{
				for(var i : int = 0;i<(re.result as ArrayCollection).length;i++){
					dataProvider.addItem((re.result as ArrayCollection).getItemAt(i));
				}
				returnResult();
			}		
		}
		
		/*
		--------------------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------CAS 0---------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		*/
		private function getListeCollaborateur():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
			 var xmlParam :XML = XmlParamUtil.createXmlParamObject();
			 XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","id Groupe racine",GROUPE_INDEX);
			   var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getListeEmploye",
							initResultHandler_getListeCollaborateur);
			RemoteObjectUtil.callService(opData,xmlParam);	
		}
		private function initResultHandler_getListeCollaborateur(re : ResultEvent):void
		{
			if (re.result)
			{
				for(var i : int = 0;i<(re.result as ArrayCollection).length;i++){     
					dataProvider.addItem((re.result as ArrayCollection).getItemAt(i));
					dataProvider.getItemAt(i).COLLABORATEUR = dataProvider.getItemAt(i).NOM + " " +dataProvider.getItemAt(i).PRENOM;
				}
				//Contrairement au autres cas, il n'y pas besoin d'attendre l'execution d'une seconde requête pour retourner le résultat :
				nbrAppel = 1;
				returnResult();
			}		
		}
		/*
		--------------------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------CAS 7--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		*/
		private function getLigne_sansSIM():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
			 var xmlParam :XML = XmlParamUtil.createXmlParamObject();
			 XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","id Groupe racine",GROUPE_INDEX);
			   var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_Ligne_sans_sim",
							initResultHandler_getListe_L);
			RemoteObjectUtil.callService(opData,xmlParam);	
		}
		private function initResultHandler_getListe_L(re : ResultEvent):void
		{
			if (re.result)
			{
				//Alert.show(ObjectUtil.toString(re.result));
				for(var i : int = 0;i<(re.result as ArrayCollection).length;i++){
					dataProvider.addItem((re.result as ArrayCollection).getItemAt(i));
				}
				//Contrairement au autres cas, il n'y pas besoin d'attendre l'execution d'une seconde requête pour retourner le résultat :
				nbrAppel = 1;
				returnResult();
			}		
		}
		/*
		--------------------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------CAS 9--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		*/
		private function getSIM_sansLigne():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
			 var xmlParam :XML = XmlParamUtil.createXmlParamObject();
			 XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","id Groupe racine",GROUPE_INDEX);
			   var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_SIM_sans_ligne",
							initResultHandler_getSIM_sansLigne);
			RemoteObjectUtil.callService(opData,xmlParam);	
		}
		private function initResultHandler_getSIM_sansLigne(re : ResultEvent):void
		{
			if (re.result)
			{
				for(var i : int = 0;i<(re.result as ArrayCollection).length;i++){
					dataProvider.addItem((re.result as ArrayCollection).getItemAt(i));
				}
				//Contrairement au autres cas, il n'y pas besoin d'attendre l'execution d'une seconde requête pour retourner le résultat :
				nbrAppel = 1;
				returnResult();
			}		
		}
		/*
		--------------------------------------------------------------------------------------------------------------------------------------------
		------------------------------------------------------CAS 3--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		--------------------------------------------------------------------------------------------------------------------------------------------
		*/
		private function getSIMOfEmp():void
		{
			var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
		 	var xmlParam :XML = XmlParamUtil.createXmlParamObject();
		 	XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","Groupe racine",GROUPE_INDEX);
		 	XmlParamUtil.addParam(xmlParam,"ID_EMPLOYE ","ID du collaborateur",idEmploye);
		   		 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getAllAffectedSimToEmp",
							initResultHandler);
			
			RemoteObjectUtil.callService(opData,xmlParam);						
		}
		private function getSIM_sansEmp_sansTerm():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
			 var xmlParam :XML = XmlParamUtil.createXmlParamObject();
			 XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","id Groupe racine",GROUPE_INDEX);
			   var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getSimSansTerm",
							initResultHandler_getSIM_sansEmp_sansTerm);
			RemoteObjectUtil.callService(opData,xmlParam);						
		}
		private function get_all_SIM_sansEmp_sansTerm():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
							 
			 var xmlParam :XML = XmlParamUtil.createXmlParamObject();
			 XmlParamUtil.addParam(xmlParam,"IDGROUPE_RACINE","id Groupe racine",GROUPE_INDEX);
			 var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"get_ALL_SIM_sans_term",
							initResultHandler_getSIM_sansEmp_sansTerm,fault);
							//Alert.show("getAllSimSansTerm-->"+xmlParam.toString());
			RemoteObjectUtil.callService(opData,xmlParam);						
		}
		private function initResultHandler_getSIM_sansEmp_sansTerm(re : ResultEvent):void
		{
			//Alert.show("result getAllSimSansTerm ->"+ ObjectUtil.toString(re.result));
			if (re.result)
			{
				for(var i : int = 0;i<(re.result as ArrayCollection).length;i++){
					dataProvider.addItem((re.result as ArrayCollection).getItemAt(i));
				}
				returnResult();
			}
			else
			{
				
			}		
		}
		private function fault (evt:FaultEvent):void{
			Alert.show("ERREUR -> getAllSimSansTerm"+evt.toString());
		}
		/*-------------------------------------Result des procédures--------------------------------------------------------------
		*****************************************************************************************************/
		private function initResultHandler(re : ResultEvent):void
		{
			if (re.result){
				for(var i : int = 0;i<(re.result as ArrayCollection).length;i++){
					dataProvider.addItem((re.result as ArrayCollection).getItemAt(i));
				}
				returnResult();
			}		
		}
		private function returnResult():void
		{
			//Attendre que les deux requêtes soit executées :
			nbrAppel = nbrAppel + 1;
			if(nbrAppel > 1)
			{
				resultHandler(dataProvider);
			}
		}
		/*-***************************************************************************************************
		---------------------------------------------------------------------------------------------------*/
	}
}