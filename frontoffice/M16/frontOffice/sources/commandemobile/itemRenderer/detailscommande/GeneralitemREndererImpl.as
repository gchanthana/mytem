package commandemobile.itemRenderer.detailscommande
{	
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.CompteService;
	import gestioncommande.services.RevendeurService;
	import gestioncommande.services.TransporteurSevice;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextArea;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class GeneralitemREndererImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
	
		public var cbxTransporteur				:ComboBox;
		
		public var chxAdresseRevendeur			:CheckBox;
		
		public var hbxChamps1					:HBox;
		public var hbxChamps2					:HBox;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		public var cmd							:Commande;	
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listeTransporteurs			:ArrayCollection;
		public var listeLibellesComptes			:ArrayCollection;
		public var listeAdresseRevendeur		:ArrayCollection;
		
		public var periodeObject				:Object 				= {rangeStart:new Date(new Date().getTime() - (1 * DateFunction.millisecondsPerDay) )};
				
		//--------------------------------------------------------------------------------------------//
		//					LIEBELE COMPTE SOUSCOMPTE
		//--------------------------------------------------------------------------------------------//
		
		public var compteName 					:String = ResourceManager.getInstance().getString('M16', 'Compte');;
		public var souscompteName 				:String = ResourceManager.getInstance().getString('M16', 'Sous_compte');;

		public var adresseSiteLivraison			:String = '';
		public var adresseRevendeur				:String = '';
		
		//--------------------------------------------------------------------------------------------//
		//					MULTIZONE COMPOSANT
		//--------------------------------------------------------------------------------------------//
		
		public var champs1						:ArrayCollection;
		public var champs2						:ArrayCollection;
		
		public var LIBELLE_CHAMPS1				:String = ResourceManager.getInstance().getString('M16', 'R_f_rence_client_1_');
		public var EXEMPLE_CHAMPS1				:String = "";
		public var LIBELLE_CHAMPS2				:String = ResourceManager.getInstance().getString('M16', 'R_f_rence_client_2_');
		public var EXEMPLE_CHAMPS2				:String = "";
		public var NAME_BUTTON					:String = ResourceManager.getInstance().getString('M16', 'R_capitulatif');
		public var NAME_ETAPE					:String = "";

		public var CHAMPS1_EXEMPLE_VISIBLE		:Boolean = false;
		public var CHAMPS1_ACTIF				:Boolean = false;
		public var CHAMPS2_EXEMPLE_VISIBLE		:Boolean = false;
		public var CHAMPS2_ACTIF				:Boolean = false;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function GeneralitemREndererImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
			
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function setCommande(selectedCommande:Commande):void
		{
			cmd = selectedCommande;
		}
		
		public function setProcedure(selectedCommande:Commande):void
		{
			cmd = selectedCommande;
			
			setInfosCommande();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function commandeChangeHandler(e:Event):void
		{
			dispatchEvent(new Event('COMMANDE_CHANGE', true));
		}
		
		protected function chxAdresseRevendeurClickHandler(me:MouseEvent):void
		{
			setCkxAdresseRevendeur();
			commandeChangeHandler(me);
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
		}
		
		private function showHandler(fe:FlexEvent):void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function setDetailsCommande():void
		{
			adresseSiteLivraison = cmd.LIBELLE_SITELIVRAISON;
			
			chxAdresseRevendeur.selected = cmd.IS_LIV_DISTRIB;
			
			if(chxAdresseRevendeur.selected)
				cmd.LIBELLE_SITELIVRAISON = '';

			champsPersoHandler();
		}
		
		private function setTransporteurs(values:ArrayCollection):void
		{
			var lenTrans:int = values.length;
						
			if(cmd.IDTRANSPORTEUR != 0)
			{
				for(var i:int = 0;i < lenTrans;i++)
				{
					if(values[i].IDTRANSPORTEUR == cmd.IDTRANSPORTEUR)
						cbxTransporteur.selectedIndex = i;
				}
			}
			else
			{
				if(lenTrans > 1)
				{
					cbxTransporteur.prompt = ResourceManager.getInstance().getString('M16', 'S_lectionnez');
					cbxTransporteur.selectedIndex = -1;
				}
			}
		}
		
		private function setLibelleCompteSousCompte(values:ArrayCollection):void
		{
			var len:int = values.length;
			
			for(var i:int = 0; i < len;i++)
			{
				if(values[i].OPERATEURID == cmd.IDOPERATEUR)
				{
					if(values[i].COMPTE != null && values[i].COMPTE != '')
						compteName 		= values[i].COMPTE;
					else
						compteName 		= ResourceManager.getInstance().getString('M16','Compte');;
					
					if(values[i].SOUS_COMPTE != null && values[i].SOUS_COMPTE != '')
						souscompteName 	= values[i].SOUS_COMPTE;
					else
						souscompteName 	= ResourceManager.getInstance().getString('M16','Sous_compte');
				}
			}	
		}

		private function setAdresseRevendeur(values:ArrayCollection):void
		{
			if(values != null && values.length > 0)
				adresseRevendeur = values[0].ADRESSE1;

			if(adresseRevendeur == '')
			{
				chxAdresseRevendeur.selected	= false;
				chxAdresseRevendeur.enabled 	= false;
				chxAdresseRevendeur.errorString = ResourceManager.getInstance().getString('M16', 'Aucune_adresse_du_distributeur');
			}
			else
			{
				chxAdresseRevendeur.enabled 	= true;
				chxAdresseRevendeur.errorString = '';
			}
			
			setCkxAdresseRevendeur();
		}
		
		private function setCkxAdresseRevendeur():void
		{
			if(chxAdresseRevendeur.enabled)
			{
				if(chxAdresseRevendeur.selected)
					cmd.LIBELLE_SITELIVRAISON = adresseRevendeur;
				else
					cmd.LIBELLE_SITELIVRAISON = adresseSiteLivraison;
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//

		private function setInfosCommande():void
		{
			setDetailsCommande();
			setTransporteurs(listeTransporteurs);
			setLibelleCompteSousCompte(listeLibellesComptes);
			setAdresseRevendeur(listeAdresseRevendeur);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function champsPersoHandler():void
		{
			var i:int = 0;
			
			champs1 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS1;
			champs2 		= SessionUserObject.singletonSession.CHAMPS.CHAMPS2;
			
			if(champs1 != null && champs2 != null)
			{ 
				LIBELLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.LIBELLE1;
				EXEMPLE_CHAMPS1 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE1;
				CHAMPS1_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE1 as Boolean;
				LIBELLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.LIBELLE2;
				EXEMPLE_CHAMPS2 = SessionUserObject.singletonSession.CHAMPS.EXEMPLE2;
				CHAMPS2_ACTIF 	= SessionUserObject.singletonSession.CHAMPS.ACTIVE2 as Boolean;
			}
			
			if(EXEMPLE_CHAMPS1 == "")
				CHAMPS1_EXEMPLE_VISIBLE = false;
			else
				CHAMPS1_EXEMPLE_VISIBLE = true;
			
			if(EXEMPLE_CHAMPS2 == "")
				CHAMPS2_EXEMPLE_VISIBLE = false;
			else
				CHAMPS2_EXEMPLE_VISIBLE = true;
			
			if(SessionUserObject.singletonSession.CHAMPS.hbxChamps1 != null)
			{
				var nbrChild1	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox).numChildren;
				var hbx1		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps1 as HBox;
				var arrayTemp1	:Array 	= copyChampsPerso((hbx1 as HBox).getChildren());
				var nbrTemp1	:int 	= arrayTemp1.length;
				var cptr1		:int	= 1;
				
				for(i = 0;i < nbrTemp1;i++)
				{
					var libelle1:String = arrayTemp1[i].text;
					
					if(i == cptr1)
					{
						var texte1:String = splitString(arrayTemp1[i].text, true);
						(hbxChamps1.getChildAt(i-1) as TextArea).text = texte1;
						cptr1= cptr1 + 2;
					}
					
					hbxChamps1.addChild(arrayTemp1[i]);
					(hbxChamps1.getChildAt(i) as TextArea).text = libelle1;
					(hbxChamps1.getChildAt(i) as TextArea).addEventListener(Event.CHANGE, commandeChangeHandler);
				}
			}

			if(CHAMPS2_ACTIF)
			{
				if(SessionUserObject.singletonSession.CHAMPS.hbxChamps2 != null)
				{
					var nbrChild2	:int  	= (SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox).numChildren;
					var hbx2		:HBox	= SessionUserObject.singletonSession.CHAMPS.hbxChamps2 as HBox;
					var arrayTemp2	:Array 	= copyChampsPerso((hbx2 as HBox).getChildren());
					var nbrTemp2	:int 	= arrayTemp2.length;
					var cptr2		:int	= 1;
					
					for(i = 0;i < nbrTemp2;i++)
					{
						var libelle2:String = arrayTemp2[i].text;
						
						if(i == cptr2)
						{
							var texte2:String = splitString(arrayTemp2[i].text, false);
							(hbxChamps2.getChildAt(i-1) as TextArea).text = texte2;
							cptr2= cptr2 + 2;
						}
						
						hbxChamps2.addChild(arrayTemp2[i]);
						(hbxChamps2.getChildAt(i) as TextArea).text = libelle2;
						(hbxChamps2.getChildAt(i) as TextArea).addEventListener(Event.CHANGE, commandeChangeHandler);
					}
				}
			}
		}
		
		private function copyChampsPerso(champs:Array):Array
		{
			var newChildren	:ArrayCollection = new ArrayCollection();
			var lenChildren	:int = champs.length;
			
			for(var i:int = 0;i < lenChildren;i++)
			{
				var txtarea:TextArea 	= new TextArea();				
					txtarea.width		= (champs[i] as TextArea).width;
					txtarea.height		= (champs[i] as TextArea).height;
					txtarea.restrict	= (champs[i] as TextArea).restrict;
					txtarea.maxChars	= (champs[i] as TextArea).maxChars;
					txtarea.id			= (champs[i] as TextArea).id;
					txtarea.text		= (champs[i] as TextArea).text;
					txtarea.editable	= (champs[i] as TextArea).editable;
					txtarea.selectable	= (champs[i] as TextArea).selectable;	
					txtarea.addEventListener(Event.CHANGE, textinputChangeHandler);									
				
				if(txtarea != null)
					newChildren.addItem(txtarea);
				else
					newChildren.addItem(champs[i]);
			}
			
			return newChildren.source;
		}
		
		private function textinputChangeHandler(e:Event):void
		{
			(e.currentTarget as TextArea).errorString = "";
		}
		
		private function splitString(strgToFind:String, isRef1:Boolean):String
		{
			var part		:String 	= "";
			var partremove	:String 	= "";
			var refClient	:String		= "";
			var firstIndex	:int		= -1;
			
			if(isRef1)
				refClient = (cmd.REF_CLIENT11!=null)?cmd.REF_CLIENT11:"";
			else
				refClient = (cmd.REF_CLIENT21!=null)?cmd.REF_CLIENT21:"";
			
			firstIndex = refClient.indexOf(strgToFind);
			
			if(firstIndex > 0)
			{
				part 		= refClient.slice(0, firstIndex);
				partremove	= part + strgToFind;
				refClient 	= refClient.replace(partremove, '');
			}
			else
			{
				part 		= refClient.slice(0);
				refClient 	= refClient.slice();
			}
			
			if(isRef1)
				cmd.REF_CLIENT11 = refClient;
			else
				cmd.REF_CLIENT21 = refClient;
			
			return part;
		}

	}
}
