<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.process.evenement.EvenementDemo2"
extends="fr.consotel.api.ibis.publisher.handler.process.evenement.EvenementLibrary" hint="Exemple d'implémentation d'un évènement basé sur EvenementLibrary">
	<!--- fr.consotel.api.ibis.publisher.handler.process.evenement.Evenement --->
	<cffunction access="private" name="requiredProperties" returntype="Struct" hint="Retourne la liste des propriétés requises par l'évènement">
		<cfreturn SUPER.requiredProperties()>
	</cffunction>

	<cffunction access="private" name="currentParamsDefinition" returntype="Query"
	hint="Execute et retourne une requete contenant la définition des paramètres du rapport pour l'exécution dont l'index est currentIndex(). Les colonnes sont les suivantes :
	NOM (Nom du paramètre), LIBELLE (Libellé du paramètre), IS_SYSTEM (0 si le paramètre est visible dans le container et 1 sinon)">
		<cfset var RLIST=getExecutionList()>
		<cfset var reportId=VAL(RLIST["ID"][currentIndex()])>
		<cfquery name="qParamsDef" datasource="ROCOFFRE">
			select	'P_STATUS' as NOM, 'Statut' as LIBELLE, 0 as IS_SYSTEM
			from		DUAL
			UNION
			select	'P_NB_JOURS' as NOM, 'Nombre de jours' as LIBELLE, 0 as IS_SYSTEM
			from		DUAL
			UNION
			select	'P_DELETED' as NOM, null as LIBELLE, 1 as IS_SYSTEM
			from		DUAL
		</cfquery>
		<cfreturn qParamsDef>
	</cffunction>

	<cffunction access="private" name="getReportList" returntype="query" hint="Exécute et retourne la requete ramenant la liste des rapports à exécuter">
		<cfquery name="qEvenement" datasource="ROCOFFRE">
			select	/* Champs métiers */
						1 as ID, 'Container' as DELIVERY, 'E00' as POST_PROCESS, 88096 as USERID, 1 as CODE_APPLI, 2458788 as IDRACINE,
						null as EMAIL_FROM, null as EMAIL_TO, null as EMAIL_CC, null as EMAIL_SUBJECT, 'pdf' as FILE_EXT, 0 as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						-1 as IDRAPPORT, 'VIEW' as TEMPLATE, 'html' as FORMAT, 'fr_FR' as LOCALISATION,
						/* Colonnes requises par les bibliothèque des rapports */
						'PERIMETRE' as L_PERIMETRE, trim(to_char(SYSDATE,'MONTH') || '-' || to_char(SYSDATE,'YYYY')) as L_PERIODE,
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
						'F' as P_STATUS, 2 as P_NB_JOURS, 'N' as P_DELETED
			from		DUAL
			UNION
			select	/* Champs métiers */
						2 as ID, 'Mail' as DELIVERY, 'E00' as POST_PROCESS, 77084 as USERID, 1 as CODE_APPLI, 2458788 as IDRACINE,
						'api@consotel.fr' as EMAIL_FROM, 'monitoring@saaswedo.com' as EMAIL_TO, null as EMAIL_CC,
						'Evènement Test (Inline)' as EMAIL_SUBJECT, 'html' as FILE_EXT, 0 as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						-1 as IDRAPPORT, 'VIEW' as TEMPLATE, 'html' as FORMAT, 'fr_FR' as LOCALISATION,
						/* Colonnes requises par les bibliothèque des rapports */
						'PERIMETRE' as L_PERIMETRE, trim(to_char(SYSDATE,'MONTH') || '-' || to_char(SYSDATE,'YYYY')) as L_PERIODE,
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
						'F' as P_STATUS, 2 as P_NB_JOURS, 'N' as P_DELETED
			from		DUAL
			UNION
			select	/* Champs métiers */
						3 as ID, 'Mail' as DELIVERY, 'E00' as POST_PROCESS, 77084 as USERID, 1 as CODE_APPLI, 2458788 as IDRACINE,
						'api@consotel.fr' as EMAIL_FROM, 'monitoring@saaswedo.com' as EMAIL_TO, null as EMAIL_CC,
						'Evènement Test (Attachment)' as EMAIL_SUBJECT, 'pdf' as FILE_EXT, 1 as HAVE_ATTACHEMENT,
						/* Champs pour exécution du rapport BIP */
						-1 as IDRAPPORT, 'VIEW' as TEMPLATE, 'pdf' as FORMAT, 'fr_FR' as LOCALISATION,
						/* Colonnes requises par les bibliothèque des rapports */
						'PERIMETRE' as L_PERIMETRE, trim(to_char(SYSDATE,'MONTH') || '-' || to_char(SYSDATE,'YYYY')) as L_PERIODE,
						/* (Tout le reste est considéré comme) Champs Paramètres du rapport BIP */
						'F' as P_STATUS, 2 as P_NB_JOURS, 'N' as P_DELETED
			from		DUAL
		</cfquery>
		<cfreturn qEvenement>
	</cffunction>
	
	<cffunction access="public" name="getName" returntype="String" hint="Retourne le nom de l'évènement">
		<cfreturn "Evènement Démo 2">
	</cffunction>
	
	<cffunction access="private" name="currentReportDefinition" returntype="Query" hint="Pour le test car la valeur de IDRAPPORT est toujours -1 dans la liste d'exécution">
		<cfquery name="qReportingDefinition" datasource="ROCOFFRE">
			select	'/CONSOTEL/IT' as DOSSIER, 'Historique' as XDO, 'Historique' as FILE_NAME, 'Historique des rapports' as LIBELLE_IHM
			from		DUAL
		</cfquery>
		<cfreturn qReportingDefinition>
	</cffunction>
</cfcomponent>