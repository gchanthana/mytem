<cfcomponent implements="portail.class.specialClass.InterfacePage"
			 output="false"
			 displayname="Page" 
			 hint="Classe qui contient la structure de la page Mon terminal dans le template D1" >
	
	
	<!---  --->
  	<cffunction name="afficher" access="public" output="true" 
				displayname="" 
				hint="Affiche les composants de la page Mon terminal / template D1" >
		<cfargument name="head" required="true" type="string" displayname="" hint="Contenu de la balise header de la page à afficher">
		<cfargument name="context" required="true" type="struct"displayname="" hint="Structure contenant les données passés dans l'URL" >
		
		
		<!--- CREATION DUNE STRUC ET RECUPERATION DES DONNEES PASSEES EN URL DANS CETTE STRUCTURE --->
		<cfset refParam = createObject("component","portail.class.specialClass.AbstractComposant").getInfoSructURL(context) >
		
		
		<cfset objHeader = createObject("component","portail.D1.defaut.composant.Header") >
		<cfif NOT isDefined("refParam.idSous_tete")>
			<cfset header = objHeader.getHeader() >
		<cfelse>
			<cfset header = objHeader.getHeader(refParam.idPage,refParam.idSous_tete,refParam.sous_tete) >
		</cfif>
		
		
		<cfset objMonTerminalCombo = createObject("component","portail.D1.defaut.composant.MonTerminalDivSelectTerminal") >
		<cfif NOT isDefined("refParam.idSous_tete") >
			<cfset terminalCombo = objMonTerminalCombo.getDivSelectTerminal() >
		<cfelse>
			<cfset terminalCombo = objMonTerminalCombo.getDivSelectTerminal(refParam.idSous_tete) >
		</cfif>
		
		
		<cfset objActionsPossible = createObject("component","portail.D1.defaut.composant.MonTerminalDivMesActionsPossible") >
		<cfif NOT isDefined("refParam.idSous_tete") > <!--- boolIsManaged --->
			<cfset actionsPossible = objActionsPossible.getDivMesActionsPossible() >
		<cfelse>
			<cfset actionsPossible = objActionsPossible.getDivMesActionsPossible(refParam.idSous_tete) >
		</cfif>
		
		
		<cfset objDivEmail = createObject("component","portail.D1.defaut.composant.DivEmail") >
		<cfif NOT isDefined("refParam.idSous_tete") >
			<cfset divEmail = objDivEmail.getDivEmail() >
		<cfelse>
			<cfset divEmail = objDivEmail.getDivEmail(refParam.idSous_tete,5) >
		</cfif>
		
		<!--- <cfif isDefined("refParam.confirmEmail") >
			<cfset popupConfirmEmail = objDivEmail.confirmEnvoiMail(refParam.confirmEmail)>
		<cfelse>
			<cfset popupConfirmEmail = "">
		</cfif> --->
		
		<cfset objFicheTerminal = createObject("component","portail.D1.defaut.composant.MonTerminalDivFicheTerminal") >
		<cfif NOT isDefined("refParam.idSous_tete")>
			<cfset ficheTerminal = objFicheTerminal.getDivFicheTerminal() >
		<cfelse>
			<cfset ficheTerminal = objFicheTerminal.getDivFicheTerminal(refParam.idSous_tete) >
		</cfif>
		
		
		
		<cfoutput>
			<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
				#head#
				<body onunload="disableLoadsablier();">
					<div class="ombre" id="ombre">
					    <div class="background_ecran">
					        <img class="loading_image" id="imgLoading1" src="img/loading-icon.gif" />
					    </div>
					</div>
					<div class="ombre2" id="ombre2">
					    <div class="background_ecran2">
					        <img class="loading_image" id="imgLoading2" src="img/mytem/loading_bleu_mail.gif" />
					    </div>
					</div>
					
					<div class="divModalTransparenteEmail2" id="ombreee"></div>
					#divEmail#
					<!--- #popupConfirmEmail# --->
					
					<div id="global">
						#header#
						<div style="width:auto;">
							<div class="monTerminalDivGauche" style="">
								#terminalCombo#
								#actionsPossible#
							</div>
							<div class="monTerminalDivDroite" style="">
								#ficheTerminal#
							</div>
							
							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</body>
			</html>
		</cfoutput>
		
	</cffunction>
	
</cfcomponent>