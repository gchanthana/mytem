﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true">
		<cftry>
			<cfstoredproc procedure="pkg_m28.IUProfil" datasource="ROCOFFRE" >
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" 	value="#data.nom#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	value="#data.description#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" 	value="#data.idRacine#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" 	value=0><!--- dans la procédure IUFonction 0 pour Insert (nouveau)  --->
				<cfprocparam cfsqltype="CF_SQL_INTEGER" 	value="#data.createur#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" 	variable="p_retour" type="out" >
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			<cfreturn CreateReponse("AJ001", 1, qResult)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ001", 0, ThrowCfCatchError("AJ001",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>