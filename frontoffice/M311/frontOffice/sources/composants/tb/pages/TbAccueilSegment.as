package composants.tb.pages
{
	import composants.tb.tableaux.TableauChangeEvent;
	
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	public class TbAccueilSegment extends TbAccueil
	{
		public function TbAccueilSegment(type : TableauChangeEvent)
		{
			super();	
			_type =type;			 
			_pageLibelle = ResourceManager.getInstance().getString('M311', 'Segment___') + type.SEGMENT;
			addEventListener(FlexEvent.CREATION_COMPLETE,super.init);			
		}
	}
}