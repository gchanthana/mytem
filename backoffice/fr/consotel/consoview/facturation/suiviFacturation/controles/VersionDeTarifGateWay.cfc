<cfcomponent output="false">
	<cfproperty name="idGroupeMaitre" type="numeric" default="0">
	<cfproperty name="idGroupeClient" type="numeric" default="0">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupeMaitre = session.perimetre.ID_GROUPE;
		variables.idGroupeClient = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getIdGroupeMaitre" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeMaitre>
	</cffunction>
	
	<cffunction name="getIdGroupeClient" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeClient>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Enregistre la version de tarif
		
		Param out idVersion si ok sinon -1		
	
	--->
	<cffunction name="ajouterVersion" access="remote" output="false" returntype="Numeric">
		<cfargument name="pVersionDeTarif" type="VersionDeTarif" required="true" />
		<cfreturn  pVersionDeTarif.save()/>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/11/2007
		
		Description : Supprime une version de tarif
		
		Param out 1 si ok sinon -1		
	
	--->
	<cffunction name="supprimerVersion" access="remote" output="false" returntype="Numeric">
		<cfargument name="pVersionDeTarif" type="VersionDeTarif" required="true" />
		<cfreturn  pVersionDeTarif.delete()/>
	</cffunction>
	
	<cffunction name="getVersionsTarifProduitPerimetre" access="remote" output="false" returntype="VersionDeTarif[]">
		<cfargument name="pParamsRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="pProduit" type="GroupeProduitControle" required="true" />
		<cfreturn  Evaluate("getVersionsTarifProduit#getTypePerimetre()#(pParamsRecherche,pProduit)")>
	</cffunction>
	

	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des versions de tarif d'un produit client, sur la période passée en paramétre. 
		
		Params in
			ParamsRecherche
			Produit
		Params out
			VersionDeTarif[]
	
	--->
	<cffunction name="getVersionsTarifProduitGroupeLigne" access="remote" output="false" returntype="VersionDeTarif[]">
		<cfargument name="pParamsRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="pProduit" type="GroupeProduitControle" required="true" />		
		<cfset qlisteVersions = getVersionsTarifProduitGroupeLigneAsQuery(pParamsRecherche,pProduit)>	
		<cfset tabVersionsDeTarif = arrayNew(1)>			
		<cfloop query="qlisteVersions">
			<cfset version = createObject("component","VersionDeTarif")>
			<cfscript>
			//Initialize the CFC with the properties values.
				version.setIdProduit(pProduit.getId());
				version.setLibelleProduit(pProduit.getlibelle());
				version.setOperateurId(pProduit.getOperateurId());
				version.setIdVersionTarif(qlisteVersions.IDVERSION_GRP_CTL);
				version.setPrixUnitaire(qlisteVersions.TARIF_BRUT);
				version.setPrixRemise(qlisteVersions.TARIF_REMISE);
				version.setRemiseContrat(qlisteVersions.PCT_REMISE);
				version.setDateDebut(qlisteVersions.DATEDEB);
				version.setDateFin(qlisteVersions.DATEFIN);
			</cfscript>
			<cfset arrayAppend(tabVersionsDeTarif,version)>
		</cfloop>
		<cfreturn tabVersionsDeTarif>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des versions de tarif d'un produit pour un groupeLigne.
		
		Params in
			ParamsRecherche
			Produit
		Params out
			query
	--->
	<cffunction name="getVersionsTarifProduitGroupeLigneAsQuery" access="remote" output="false" returntype="Query">
		<cfargument name="pParamsRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="pProduit" type="GroupeProduitControle" required="true" />
		<!--- 
			 pkg_cv_grlig_facturation.liste_version_grp_ctl(p_idgrp_ctl => :p_idgrp_ctl,
                                                p_idracine => :p_idracine,
                                                p_retour => :p_retour);	
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_glig_facturation_V3.liste_version_grp_ctl">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="produitCatId" 	   	value="#pProduit.getId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#this.getIdGroupeClient()#" null="#iif((pParamsRecherche.getCatalogueClient() eq 0), de("yes"), de("no"))#">
			<cfprocresult name="qListeVersion"/>        
		</cfstoredproc>
		<cfreturn qListeVersion>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des versions de tarif d'un produit pour un groupe.
		
		Params in
			ParamsRecherche
			Produit
		Params out
			VersionDeTarif[]
			
	--->
	<cffunction name="getVersionsTarifProduitGroupe" access="remote" output="false" returntype="Any">
		<cfargument name="pParamsRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="pProduit" type="GroupeProduitControle" required="true" />
		<cfset qlisteVersions = getVersionsTarifProduitGroupeAsQuery(pParamsRecherche,pProduit)>	
		<cfset tabVersionsDeTarif = arrayNew(1)>			
		<cfloop query="qlisteVersions">
			<cfset version = createObject("component","VersionDeTarif")>

			<cfscript>
			//Initialize the CFC with the properties values.
				version.setIdProduit(pProduit.getId());
				version.setLibelleProduit(pProduit.getlibelle());
				version.setOperateurId(pProduit.getOperateurId());
				version.setIdVersionTarif(qlisteVersions.IDVERSION_GRP_CTL);
				version.setPrixUnitaire(qlisteVersions.TARIF_BRUT);
				version.setPrixRemise(qlisteVersions.TARIF_REMISE);
				version.setRemiseContrat(qlisteVersions.PCT_REMISE);
				version.setDateDebut(qlisteVersions.DATEDEB);
				version.setDateFin(qlisteVersions.DATEFIN);
			</cfscript>
			<cfset arrayAppend(tabVersionsDeTarif,version)>
		</cfloop>
		<cfreturn tabVersionsDeTarif>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Fournit la liste des versions de tarif d'un produit du Groupe, sur la période passée en paramétre. 
		
		Params in
			ParamsRecherche
			Produit
		Params out
			Query
	--->
	<cffunction name="getVersionsTarifProduitGroupeAsQuery" access="remote" output="false" returntype="Any">
		<cfargument name="pParamsRecherche" type="ParamsRecherche" required="true" />
		<cfargument name="pProduit" type="GroupeProduitControle" required="true" />
		<!--- 
			 pkg_cv_grcl_facturation.liste_version_grp_ctl(p_idgrp_ctl => :p_idgrp_ctl,
                                                p_idracine => :p_idracine,
                                                p_retour => :p_retour);	
		 --->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="pkg_cv_grcl_facturation_v3.liste_version_grp_ctl">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  	type="in" 	variable="produitCatId" 	   	value="#pProduit.getId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupeMaitre" 	value="#this.getIdGroupeMaitre()#" null="#iif((pParamsRecherche.getCatalogueClient() eq 0), de("yes"), de("no"))#">
			<cfprocresult name="qListeVersion"/>        
		</cfstoredproc>
		<cfreturn qListeVersion>		
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Met é jour la version de tarif
		
		Param out idVersion si ok sinon -1		
	
	--->
	<cffunction name="updateVersionTarif" access="remote" output="false" returntype="Numeric">
		<cfargument name="pVersionDeTarif" type="VersionDeTarif" required="true" />
		<cfreturn pVersionDeTarif.save() />
	</cffunction>
	
</cfcomponent>