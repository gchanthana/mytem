package univers.usages.usage
{
	import composants.util.ConsoviewFormatter;
	
	import flash.events.Event;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.formatters.DateFormatter;
	import mx.utils.ObjectUtil;
	//import composants.util.ConsoviewFormatter;
	
	public class ListeAppels extends ListeAppelsIHM
	{
		private var df:DateFormatter= new DateFormatter();
		
		public function ListeAppels()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
        		
		public function initIHM(e:Event):void {
			grdLigne.columns[0].labelFunction = formatPhoneNumber;
			grdLigne.columns[1].labelFunction = formatPhoneNumber;
			grdLigne.columns[2].sortCompareFunction = sortCompareFuncNbAppels;
			grdLigne.columns[3].labelFunction = formatVolume;
			grdLigne.columns[3].sortCompareFunction = sortCompareFunConsoViewModuleObjectolumes;
			grdLigne.columns[4].labelFunction = formateEuros;
			grdLigne.columns[4].sortCompareFunction=sortCompareFunc;
			
			grdDetails.columns[1].sortCompareFunction = sortCompareFuncNbAppels;
			grdDetails.columns[2].labelFunction = formatVolume;
			grdDetails.columns[2].sortCompareFunction = sortCompareFunConsoViewModuleObjectolumes;
			grdDetails.columns[3].labelFunction = formateEuros;
			grdDetails.columns[3].sortCompareFunction=sortCompareFunc;
	
			grdTicket.columns[0].labelFunction = formatDate;
			grdTicket.columns[0].sortCompareFunction = sortCompareFuncDates;								
			grdTicket.columns[1].labelFunction = formatHeure;
			grdTicket.columns[1].sortCompareFunction = sortCompareFuncHeures;
			grdTicket.columns[2].sortCompareFunction = formatPhoneNumber;
			grdTicket.columns[3].labelFunction = formatVolume;
			grdTicket.columns[3].sortCompareFunction = sortCompareFunConsoViewModuleObjectolumes;
			grdTicket.columns[4].labelFunction = formateEuros;
			grdTicket.columns[4].sortCompareFunction=sortCompareFunc;
		}
		
		//Concatination en tre Volume + Unité (si unité = second alors deviser le volume par 60)
        private function formatVolume(item:Object,column:DataGridColumn):String {
        	if(item.UNITE_APPEL == "S" || item.UNITE_APPEL == "s"){
        		var convVolume:int = item.VOLUME / 60;
        		return convVolume + " mn";
        	}else{
        		return item.VOLUME + " " + item.UNITE_APPEL;
        	}        	
        }	        
        
        private function formatPhoneNumber(item:Object,column:DataGridColumn):String {
        	if (item != null){
        		return ConsoviewFormatter.formatPhoneNumber(item[column.dataField])
        	}else{
        		return "-";
        	}
        }
        
        private function formatDate(item:Object,column:DataGridColumn):String {
        	df.formatString = "DD/MM/YYYY";
        	return df.format(item.DATEDEB);
        }	    
        
        private function formatHeure(item:Object,column:DataGridColumn):String {
        	df.formatString = "J:NN:SS";
        	return df.format(item.DATEDEB);
        }    
        
		//formate la monnaie dans les datagrid
		protected function formateEuros(item : Object, column : DataGridColumn):String{			
			if (item != null) return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],3);	
			else return "-";
		}
		
		protected function formateNumber(item : Object, column : DataGridColumn):String{
			if (item != null) return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
			else return "-";
		}
		
		private function sortCompareFunc(itemA:Object, itemB:Object):int {
			return ObjectUtil.numericCompare(itemA.COUT_OP,itemB.COUT_OP);
		}
		
		private function sortCompareFuncNbAppels(itemA:Object, itemB:Object):int {
			return ObjectUtil.numericCompare(itemA.NB_APPEL,itemB.NB_APPEL);
		}
		
		private function sortCompareFunConsoViewModuleObjectolumes(itemA:Object, itemB:Object):int {
			
			var volumeA:Number = 0;
			var volumeB:Number = 0;
			
			if(itemA.UNITE_APPEL == "S" || itemA.UNITE_APPEL == "s")
			{
        		volumeA = itemA.VOLUME / 60;        	
        	}
        	else
        	{
        		volumeA = itemA.VOLUME;
        	}
        	
        	if(itemB.UNITE_APPEL == "S" || itemB.UNITE_APPEL == "s")
			{
        		volumeB = itemB.VOLUME / 60;        	
        	}
        	else
        	{
        		volumeB = itemB.VOLUME;
        	}   
        	 
			return ObjectUtil.numericCompare(volumeA,volumeB);
		}
		
		private function sortCompareFuncDates(itemA:Object, itemB:Object):int {
			return ObjectUtil.dateCompare(itemA.DATEDEB,itemB.DATEDEB);
		}
		
		private function sortCompareFuncHeures(itemA:Object, itemB:Object):int {
			df.formatString = "HH:NN:SS";
			return ObjectUtil.stringCompare(df.format(itemA.DATEDEB),df.format(itemB.DATEDEB));
		}
		
		
		
		public function onPerimetreChange():void {
			
		}
				
	}
}
