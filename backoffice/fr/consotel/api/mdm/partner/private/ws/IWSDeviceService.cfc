<cfinterface author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.IWSDeviceService"
hint="Interface décrivant les méthodes du Service de gestion des entités de type Device">
	<cffunction access="public" name="getMasterKeyList" returntype="Array" hint="Retourne la liste des clés master (Master Keys)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
	</cffunction>

	<cffunction access="public" name="clearDeploymentHisto" returntype="Numeric" hint="Purge l'historique de déploiement. La valeur retournée n'est pas documentée par Zenprise">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
	</cffunction>

	<cffunction access="public" name="getDeploymentHisto" returntype="Array" hint="Retourne l'historique de déploiement par ordre chronologique">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
	</cffunction>

	<cffunction access="public" name="getLastUser" returntype="String" hint="Retourne le login du dernier utilisateur Zenprise de l'équipement ou une chaine vide s'il n'est pas défini">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
	</cffunction>

	<cffunction access="public" name="getManagedStatus" returntype="String" hint="Retourne la valeur ZDM pour un équipement géré par le MDM et UNMANAGED sinon">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement">
	</cffunction>

	<cffunction access="public" name="deviceExists" returntype="boolean" hint="Retourne TRUE si un équipement correspondant aux paramètres fournis existe">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
	</cffunction>

	<cffunction access="public" name="getAllDevices" returntype="Array" hint="Retourne un tableau contenant la liste de tous les équipements
	Chaque élément du tableau est une implémentation du Web Service correspondant à un équipement (com.sparus.ws.admin.WSDevice)">
	</cffunction>
	
	<cffunction access="public" name="getDeviceInfo" returntype="Any"
	hint="Retourne l'implémentation des infos concernant un équipement (com.sparus.ws.admin.DeviceInformation)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
	</cffunction>
	
	<cffunction access="public" name="lockDevice" returntype="Numeric" hint="Vérrouille l'équipement spécifié avec le nouveau code PIN newPinCode
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="newPinCode" type="String" required="true" hint="Nouveau code PIN du vérrouillage (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
	</cffunction>
	
	<cffunction access="public" name="unlockDevice" returntype="Numeric" hint="Retourne TRUE si un équipement correspondant aux paramètres fournis existe
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
	</cffunction>

	<cffunction access="public" name="wipeDevice" returntype="Numeric" hint="Effectue l'opération WIPE pour l'équipement existant correspondant aux paramètres fournis
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="erasedMemoryCard" type="Boolean" required="true" hint="TRUE pour supprimer le contenu de la carte mémoire">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
	</cffunction>

	<cffunction access="public" name="revokeDevice" returntype="Boolean" hint="Effectue l'opération REVOKE pour l'équipement existant correspondant aux paramètres fournis">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
	</cffunction>
	
	<cffunction access="public" name="removeDevice" returntype="void" hint="Effectue l'opération REMOVE pour l'équipement existant correspondant aux paramètres fournis">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
	</cffunction>
	
	<cffunction access="public" name="authorize" returntype="Boolean" hint="Effectue l'opération AUTHORIZE pour l'équipement existant correspondant aux paramètres fournis<br>
	La valeur retournée n'est pas spécifiée par la documentation Zenprise">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
	</cffunction>
	
	<cffunction access="public" name="deploy" returntype="Boolean"
	hint="Effectue une demande de déploiement d'Android/Windows pour un équipement connecté. Envoie un notification PUSH pour iOS<br>
	Retourne FALSE si l'équipement n'est pas connecté ou non managé et TRUE sinon">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
	</cffunction>
	
	<cffunction access="public" name="corporateDataWipeDevice" returntype="Numeric" hint="Effectue l'opération WIPE pour l'équipement<br>
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
	</cffunction>
	
	<cffunction access="public" name="createUser" returntype="String" hint="Crée un utilisateur et retourne son login">
		<cfargument name="group" type="String" required="true" hint="Nom du groupe pour l'utilisateur">
		<cfargument name="login" type="String" required="true" hint="Login pour l'utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe pour l'utilisateur">
		<cfargument name="role" type="String" required="true" hint="Rôle pour l'utilisateur (i.e : ADMIN, SUPPORT, USER, GUEST)">
	</cffunction>
	
	<cffunction access="public" name="locateDevice" returntype="Numeric" hint="Localise l'équipement.
	Retourne une des valeurs suivantes : 0 (Succès), 1 (Si Connecté), 2 (Si Déconnecté), 3 (Echec)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="waitTime" type="Numeric" required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
	</cffunction>
	
	<cffunction access="public" name="putDeviceProperties" returntype="Void" hint="Effectue l'opération putDeviceProperties() du service DeviceService Zenprise">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="properties" type="Array" required="true"
		hint="Tableau où chaque élément est une structure contenant les clés suivantes : NAME (Nom de la propriété), VALUE (Valeur de la propriété)">
	</cffunction>
	
	<cffunction access="public" name="registerDeviceForUser" returntype="String"
	hint="Spécifie que l'équipement est utilisé par l'utilisateur. Crée l'équipement s'il n'existe pas et retourne la valeur STRONGID associé à l'équipement">
		<cfargument name="osFamily" type="String" required="true" hint="OS cible - Case Sensitive (i.e : iOS, WINDOWS, ANDROID)">
		<cfargument name="serialNumber" type="String" required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="userIdentifier" type="String" required="true" hint="Identifiant utilisateur">
	</cffunction>

	<cffunction access="public" name="resetDeploymentState" returntype="Void" hint="Effectue l'opération resetDeploymentState() pour l'équipement">
		<cfargument name="serial" type="String" required="true" hint="Serial de l'équipement">
		<cfargument name="imei" type="String" required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
	</cffunction>
</cfinterface>