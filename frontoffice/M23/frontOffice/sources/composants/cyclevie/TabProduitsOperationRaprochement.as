package composants.cyclevie
{
	import composants.cyclevie.ctooltip.InfoToolTip;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CollectionEvent;
	import mx.events.FlexEvent;
	import mx.events.ToolTipEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.inventaire.creation.operationresiliation.SelectionProduitEvent;

	
	public class TabProduitsOperationRaprochement extends TabProduitsOperationRaprochementIHM
	{  
		private const RECLAMCREEE : int = 24;
		private const RECLAMENCOURS : int = 25;	
		private const RECLAMCREEEL : int = 26;
		private const RECLAMENCOURSL : int = 27;
		
		private const LETTREE : int = 22;
		private const LETTREEREC : int = 37;
		
		
		private var opProduits : AbstractOperation;
		private var infoOpMaitre : AbstractOperation;
		
		private var total : Number;	
		
		private var idOPeration : int;
		private var dateRef : String;
		private var type : int;//0 = resi 1 = crea 
		
		private var _titre : String = "";
		
		private var infoOperationMaitre : ArrayCollection;//iformation sur l'operation maitre
		
		[Bindable]
		private var tabProduitsARapprocher : ArrayCollection; // eproduits venants de l'op maitre;
		
		[Bindable]
		private var tabProduitNonAfficher : ArrayCollection = new ArrayCollection();//grid servant de filtre
		
		private var tabProduitsSelectionnes : Array; //les produits selectionnés
		
		public function TabProduitsOperationRaprochement()
		{
			//TODO: implement function
			super();
			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		
		
		/**
		 * Suprime la ligne visuel selectionnée du grid des produits de l'op
		 * */
		 public function supprimerElementsSelectionnes():void{
		 	myGridProduitsOp.selectedIndices.forEach(supprimerLigne);	
		 	
		 }
		 
		 /**
		 * Ajoute l'elements dans le Grid
		 * @param idInvProduit L'Identifiant de l'inventaire produit a rajouter
		 * */
		 public function ajouterElements(id : int):void{		 	
		 	tabProduitsARapprocher.addItem(retrouveObjetNonAfficher(id,tabProduitNonAfficher,"IDINVENTAIRE_PRODUIT"));
		 	tabProduitNonAfficher.refresh();
		 }
		
		
		/**
		 * charge les elements dans le grid pour la premiere fois
		 * @param idop  L'identifiant d'une operation
		 * @param dRef  La date de ref pour les calculs
		 * @param le type de calcul (rapprochement de creation = 2 ,rapprochement de resiliation = 1)
		 * */
		public function initialiserGrid(idOp : int = 0,dRef : String = null ,tpe : int = 0):void{
			
			//test
			idOPeration = idOp;
			dateRef = dRef;
			type = tpe;	
			chargerElementFacturationOpeMaitre();
		}
		/**
		 * Recharge les elements dans le grid
		 * */
		public function mettreAJourGrid():void{
			chargerElementFacturationOpeMaitre();
		}	
		
		/**
		 * Retourne la ligne du Grid des Preoduits pour l'idinventaire_produit passer en parametre
		 * @param ID L'idInventaire_produit 
		 * @return Object La ligne selectionnée
		 * */
		 public function rercherProduit(id : int):Object{
		 	var gridLine : Object;
		 	
		 	gridLine = retrouveObjetNonAfficher(id,tabProduitsARapprocher,"IDINVENTAIRE_PRODUIT");
		 	
		 	return gridLine 
		 }
		 
		 /**
		 * Retourne les donnée du grid
		 * @return les données sous forme de tableau
		 * */
		 public function get Donnees():Array{
		 	return tabProduitsARapprocher.source;
		 }
		 
		 /**
		 * Met le titre
		 * */
		 [Bindable]
		 public function set titre(t : String):void{
		 	_titre = t;
		 	if (leTitre != null) leTitre.text = _titre;
		 }
		 
		 public function get titre():String{
		 	return _titre;
		 }
		
		//-----------------------------PRIVATE------------------------------------------//
		//initialisation de l 'IHM
		private function initIHM(fe : FlexEvent):void{		
			//myGridProduitsOp.dataTipFunction = showgridTips;
			txtFiltre.addEventListener(Event.CHANGE,filtrerGird);
			if (type == 1) hdJfacture.headerText = "Der. jour facturé";
			else hdJfacture.headerText = "1er. jour facturé"
			myGridProduitsOp.addEventListener(ToolTipEvent.TOOL_TIP_CREATE,showToolTip);
			myGridProduitsOp.addEventListener(Event.CHANGE,selectionnerProduits);
			DataGridColumn(myGridProduitsOp.columns[0]).dataTipFunction = showgridTips;
			DataGridColumn(myGridProduitsOp.columns[1]).labelFunction = formateDates;
			DataGridColumn(myGridProduitsOp.columns[3]).labelFunction = formateLigne;
			DataGridColumn(myGridProduitsOp.columns[5])
			DataGridColumn(myGridProduitsOp.columns[6]).labelFunction = formateEuros;
			colMontant.sortCompareFunction = numericSortFunction;
			checkListEtatLettrage(null);
			checkListEtatReclam(null);						
			//tabProduitNonAfiicher.addEventListener(CollectionEvent.COLLECTION_CHANGE,filtrerGridProduitsOp);
		}
		
		private function showToolTip(te : ToolTipEvent):void{
			var myTLT : InfoToolTip = new InfoToolTip();						
			te.toolTip = myTLT;			
		}
		
		private function showgridTips(item : Object):String{
			var etat : String = item.LIBELLE_ETAT;
			var derJourFacturee : Date = new Date(item.DATE_DERNIERE_FACT);
			var derJourFactString : String = (item.DATE_DERNIERE_FACT != null)?DateFunction.formatDateAsString(derJourFacturee):"-------";
			var nbJourRegule : String = (item.NBJOUR != null)?item.NBJOUR:"--"; 
			var ligne : String = item.SOUS_TETE;
			var ope : String = item.OPNOM;
			var produits : String = item.LIBELLE_PRODUIT;
			var estimRegul : String = item.ESTIMATION;
			
			var detail : String = "Etat : "+ etat + "\n"	+
								"Dernier jour facturé "+derJourFactString+ "\n" +								
								"Nombre de jours à régulé "+nbJourRegule + "\n" +
								"Ligne concernée "+ConsoviewFormatter.formatPhoneNumber(ligne) + "\n" +
								"Operateur "+ ope + "\n" +
								"Produit "+produits + "\n" +
								"Estimation de la régule "+estimRegul + "\n" ;
			return detail;
		}
				
		private function checkListEtatReclam(ce : CollectionEvent):void{
			var ok : Boolean = false;
			
			ok = ( isPresent(RECLAMCREEE,tabProduitsARapprocher,"IDINV_ETAT")|| 
				   isPresent(RECLAMCREEEL,tabProduitsARapprocher,"IDINV_ETAT")||
				   isPresent(RECLAMENCOURS,tabProduitsARapprocher,"IDINV_ETAT")||
				   isPresent(RECLAMENCOURSL,tabProduitsARapprocher,"IDINV_ETAT"));
			trace("checkListEtatReclam  ++++++++++++++  ok= "+ok);
			
			if ((ok)||(tabProduitsARapprocher == null ) ||(tabProduitsARapprocher.length == 0) ){
				dispatchEvent(new Event("ReclamEnCours"));
			} else {
				dispatchEvent(new Event("AucuneReclamEnCours"));
			}
		}
		
		private function checkListEtatLettrage(ce : CollectionEvent):void{
			var ok : Boolean = false;
			
			 
			
			ok = (isAll([LETTREE,LETTREEREC],tabProduitsARapprocher,"IDINV_ETAT"));
				   
			if (ok){
				dispatchEvent(new Event("CloturesAvecLettrageOk"));	
				trace("CloturesAvecLettrageOk dipatché");
			} 	
			trace("checkListEtatLettrage= "+ok);	
			
		}
		
		
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item[column.dataField]);
			if (item[column.dataField] != null){
				return DateFunction.formatDateAsString(ladate);				
			}else{
				return "-----------";
			}
			
		}
		
		private function formateEuros(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item.ESTIMATION,2);	
			return "";		
		}
		
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);			
		}
		
		private function numericSortFunction(itemA:Object, itemB:Object):int{
			return ObjectUtil.numericCompare(itemA.ESTIMATION,itemB.ESTIMATION);
		}
		
		private function supprimerLigne(element:*, index:Number, arr:Array):void{	
			var obj : Object;			
			obj = tabProduitsARapprocher.getItemAt(element);
			tabProduitNonAfficher.addItem(obj);			
			tabProduitsARapprocher.removeItemAt(element);
			tabProduitsARapprocher.refresh();			
		}	
						
		//fait la somme de la colonne du tab passer en param
		private function calculTotal(d : ArrayCollection,colname : String):Number{
			var ligne : Object;
			var letotal : Number = 0;
			
			for (ligne in d){
				letotal = letotal + Number(d[ligne][colname]); 
			}	
									
			return letotal;	
		}
		
		//retourne l'Objet de tab passer en param pour un Id Donnee
		private function retrouveObjetNonAfficher(id : int, tab : ArrayCollection, colname : String):Object{
			var ligne : Object;
			for ( ligne in tab){
				if (id == tab[ligne][colname]){
					return tab[ligne];
				}
			}
			return null			
		}
		
		//regarde si l'id est présent dan la collection
		private function isPresent(id : int, tab : ArrayCollection, colname : String):Boolean{
			var ligne : Object;
			for ( ligne in tab){
				if (id == tab[ligne][colname]){
					return true;
				}
			}
			return false;			
		}
		
		//regarde si tous les elements du tableau sont dans le meme etat 
		private function isAll(liste : Array,tab : ArrayCollection, colname : String):Boolean{
			var ligne : Object;
			 
			if (tab == null ) return false;
			if (tab.length == 0) return false; 
			
			for (ligne in tab){			
				trace("liste[0]= "+liste[0]+"   "+"tab[ligne][colname])= "+tab[ligne][colname]+"ok = "+(liste[0]!= tab[ligne][colname]));
				trace("liste[1]= "+liste[1]+"   "+"tab[ligne][colname])= "+tab[ligne][colname]+"ok = "+(liste[1]!= tab[ligne][colname]));	
			
				if ((liste[0] != tab[ligne][colname]) && (liste[1] != tab[ligne][colname])  ) {
					trace("return false");
					return false;	 
				}
			}
			
			return true;					
		}
		
		//sélèctionne les produits selectionnés
		private function selectionnerProduits(ev : Event):void{
				//tabProduitsSelectionnes = formateData(ev.currentTarget.selectedItems,"IDINVENTAIRE_PRODUIT");
				tabProduitsSelectionnes = ev.currentTarget.selectedItems;
				
			//pour les tests
				var eventObjbis : SelectionProduitEvent = new SelectionProduitEvent("ProduitsOPSelectionnes");
				eventObjbis.tabProduits = tabProduitsSelectionnes;	
				dispatchEvent(eventObjbis);					
			 
		}
		
		//formate les donnees du selectedItems du grid
		private function formateData( data : Array , colname : String):Array{
			var s : String;
			var a : Array = new Array();
			for (var i : uint = 0 ; i < data.length ; i++ ){
				a.push(data[i][colname]);
			}
			return a;
		}	
		
		
		//----------------------- FILTRES -----------------------------------------------------------------
		
		private function filtrerGird(ev : Event):void{
			if (myGridProduitsOp.dataProvider != null){
				(myGridProduitsOp.dataProvider as ArrayCollection).filterFunction = filtrer;
				(myGridProduitsOp.dataProvider as ArrayCollection).refresh();
				total = calculTotal((myGridProduitsOp.dataProvider as ArrayCollection),"ESTIMATION");						
				afficgerTotal();
			}
		}
		
		private function filtrer(item : Object):Boolean{
			if ((String(item.LIBELLE_ETAT).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.DATE_DERNIERE_FACT).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.NBJOUR).toLowerCase().search(txtFiltre.text) != -1)		
				||
				(String(item.SOUS_TETE).toLowerCase().search(txtFiltre.text) != -1)		
				||
				(String(item.OPNOM).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.LIBELLE_PRODUIT).toLowerCase().search(txtFiltre.text) != -1)
				||
				(String(item.ESTIMATION).toLowerCase().search(txtFiltre.text) != -1))
			{
				return true;
			}else{
				return false;
			}
		}
		//----------------------- REMOTINGS ---------------------------------------------------------------
		private function chargerElementFacturationOpeMaitre():void{
			
			opProduits = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
																				"getListeElementFacturationTheoriqueOp",
																				chargerElementFacturationOpeMaitreResultHandler,null);				
			RemoteObjectUtil.callService(opProduits,idOPeration,dateRef,type-1);		
		}
		
		
		private function chargerElementFacturationOpeMaitreResultHandler(re : ResultEvent):void{
			tabProduitsARapprocher = re.result as ArrayCollection;		
			
			myGridProduitsOp.dataProvider = tabProduitsARapprocher;		
							
			total = calculTotal(tabProduitsARapprocher,"ESTIMATION");						
			afficgerTotal();
			checkListEtatReclam(null);
			checkListEtatLettrage(null);
		}
		
		private function afficgerTotal():void{
			DataGridColumn(myFooterGrid.columns[1]).headerText = ConsoviewFormatter.formatEuroCurrency(total,2);
		}
		
		
		
	
		
		//--------------
		private function chargerInfoOperationMaitre():void{
			
			infoOpMaitre = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getDetailOperation",
																				chargerInfoOperationMaitreResultHandler,null);				
			RemoteObjectUtil.callService(infoOpMaitre,idOPeration);		
		}
		
		
		private function chargerInfoOperationMaitreResultHandler(re : ResultEvent):void{
			infoOperationMaitre = re.result[0] as ArrayCollection;					
		}
			
		private function defaulFaultHandler(fe : FaultEvent):void{
			Alert.show("Erreur Remoting");
			Alert.show(fe.fault.faultDetail,"Erreur Remoting");
		}

	}
}