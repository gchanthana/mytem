<cfcomponent hint="La gestion des contrats de services">

<!--- **********************************************************
	Ce sont les contrats qui concernent les équipements <b>(IDTYPE_CONTRAT != 6)</b>

	<li>lister les contrats de service d'un équipement</li>
	<li>lister les contrats de service des équipements d'un site</li>
	<li>lister les type de contrats de service</li>
	<li>ajouter un contrat de service à un équipement</li>
	<li>supprimer un contrat de service d'un équipement</li>
	<li>voir le détail d'un contrat de service</li>
	<li>mettre à jour un contrat de service</li>

 ***************************************************************** --->

	<cfset iduser = session.user.clientaccessid>
	<cfset idracine = session.perimetre.id_groupe>

	<cffunction name="init" access="public" output="false" returntype="any" hint="Initialize the GestionContratsService object">
		<cfscript>
			return this;
		</cfscript>	
	</cffunction>


<!--- ****************************
	Getters 
********************************** --->




<!--- ****************************
	Setters 
********************************** --->




<!--- ****************************
	Methods 
********************************** --->

	<cffunction name="getListeTypesContrat" access="public" returntype="Query" output="false" hint="la liste de tous les types de contrats de services (IDTYPE_CONTRAT != 6)">
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_CONTRAT.L_TYPES_CTRT">
			<cfprocresult name="qGetListeTypesContrat">
		</cfstoredproc>
		<cfreturn qGetListeTypesContrat>
	</cffunction>
	
	<cffunction name="getListeContratsEquipement" access="public" returntype="Query" output="false" hint="La liste des contrat de services d'un équipement du parc client (IDTYPE_CONTRAT != 6)">
		<cfargument name="idEquipement" required="true" type="numeric" default="" hint="Initial value for the idEquipementproperty." />
		<cfargument name="nbMois" required="false" type="numeric" default="18" hint="Initial value for the nbMoisproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_CONTRAT.L_CTRTS_EQ">
			<cfprocparam type="in" cfsqltype="cf_SQL_INTEGER" value="#idEquipement#">
			<cfprocparam type="in" cfsqltype="cf_SQL_INTEGER" value="#nbMois#">
			<cfprocresult name="qGetListeContratsEquipement">
		</cfstoredproc>
		<cfreturn qGetListeContratsEquipement>
	</cffunction>
	
	<cffunction name="getListeContratsEquipementSite" access="public" returntype="Query" output="false" hint="La liste des contrat de services des équipements du parc client qui sont sur un site donné.
		(IDTYPE_CONTRAT != 6)">
		<cfargument name="idSite" required="false" type="numeric" default="" hint="Initial value for the idSitePhysiquetproperty." />
		<cfargument name="nbMois" required="true" type="numeric" />
		<cfargument name="sizeList" required="true" type="numeric" />
				<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_CONTRAT.L_CTRTS_EQ_SITE">
					<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
					<cfprocparam  value="#nbMois#" cfsqltype="cf_sql_inTEGER" type="in">
					<cfprocresult name="qGetListeContratSite" maxrows="#sizeList#"/>
				</cfstoredproc>
		<cfreturn qGetListeContratSite/>
	</cffunction>
	
	<cffunction name="addContrat" access="public" returntype="Numeric" output="false" hint="ajouter un contrat de service à un équipement">
		<cfargument name="idTypeContrat" 		required="true"  type="Numeric" default="" hint="l'id du type de contrat" />
		<cfargument name="idRevendeur" 			required="true"  type="Numeric" default="" hint="le revendeur chez qui est souscrit le contrat" />
		<cfargument name="reference" 			required="false" type="String" 	default="" hint="La référence du contrat sur 50 caractères maximum" />
		<cfargument name="date_signature" 		required="false" type="Date" 	default="" hint="la date à laquelle est signé le contrat" />
		<cfargument name="date_echeance"		required="false" type="Date" 	default="" hint="la date d'echeance du contrat" />
		<cfargument name="commentaire" 			required="false" type="String" 	default="" hint="les commentaires du contrat sur 300 caractères maximum" />
		<cfargument name="tarif" 				required="false" type="Numeric" default="" hint="le montant ponctuel du contrat" />
		<cfargument name="duree_contrat" 		required="false" type="Numeric" default="" hint="la durée en mois du contrat" />
		<cfargument name="tacite_reconduction"	required="false" type="Numeric" default="" hint="1 = le contrat est reconduit automatiquement 0 = le contrat n'est pas reconduit automatiquement" />
		<cfargument name="date_debut" 			required="true"  type="Date" 	default="" hint="la date de debut du contrat" />
		<cfargument name="duree_preavis" 		required="false" type="Numeric" default="" hint="la durée, en mois, du preavis de dénonciation du contrat" />
		<cfargument name="tarif_mensuel" 		required="false" type="Numeric" default="" hint="le tarif mensuel du contrat" />
		<cfargument name="date_resiliation" 	required="false" type="Date" 	default="" hint="la date de résiliation du contrat" />
		<cfargument name="date_renouvellement" 	required="false" type="Date" 	default="null" hint="la date de renouvellement du contrat" />
		<cfargument name="date_elligibilite" 	required="false" type="Date" 	default="" hint="la date d'elligibilite du contrat" />
		<cfargument name="duree_eligibilite" 	required="false" type="Numeric" default="" hint="la durée d'éligibilité  en mois du contrat" />
		<cfargument name="idEquipement" 		required="false" type="Numeric" default="" hint="l'id de l'équipement concerné" />
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_CONTRAT.C_CTRT_EQ">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idTypeContrat#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idRevendeur#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#reference#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_signature).dateInverse#" null="#inverseDate(date_signature).boolNull#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_echeance).dateInverse#" null="#inverseDate(date_echeance).boolNull#">
			<cfprocparam type="in" cfsqltype="cf_sql_vARCHAR" value="#commentaire#">
			<cfprocparam type="in" cfsqltype="CF_SQL_FlOAT"   value="#tarif#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#duree_contrat#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#tacite_reconduction#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#lsdateformat(date_debut,'yyyy/mm/dd')#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#duree_preavis#">
			<cfprocparam type="in" cfsqltype="CF_SQL_FlOAT"   value="#tarif_mensuel#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_resiliation).dateInverse#" null="#inverseDate(date_resiliation).boolNull#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_renouvellement).dateInverse#" null="#inverseDate(date_renouvellement).boolNull#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_elligibilite).dateInverse#" null="#inverseDate(date_elligibilite).boolNull#">
			<cfprocparam type="in" cfsqltype="CF_SQL_NUMERIC" value="#duree_eligibilite#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#iduser#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idEquipement#">
			<cfprocparam type="out" cfsqltype="CF_SQL_inTEGER" variable="pAddContrat">
		</cfstoredproc>
		<cfreturn pAddContrat>
	</cffunction>
	
	<cffunction name="getDetailContrat" access="public" returntype="Query" output="false" hint="le détail du contrat spécifié">
		<cfargument name="idContrat" required="true" type="numeric" default="" hint="Initial value for the idContratproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_CONTRAT.G_CTRTS_EQ">
			<cfprocparam type="in" cfsqltype="CF_SQL_NUMERIC" value="#idContrat#">
			<cfprocresult name="qGetDetailContrat">
		</cfstoredproc>
		<cfreturn qGetDetailContrat>
	</cffunction>
		
	<cffunction name="updateContrat" access="public" returntype="Numeric" output="false" hint="mise à jour du contrat">
		<cfargument name="idContrat" required="true" type="Numeric" default="" hint="l'id du contrat " />
		<cfargument name="idTypeContrat" required="true" type="Numeric" default="" hint="l'id du type de contrat" />
		<cfargument name="idRevendeur" required="false" type="Numeric" default="" hint="le revendeur chez qui est souscrit le contrat" />
		<cfargument name="reference" required="false" type="String" default="" hint="La référence du contrat sur 50 caractères maximum" />
		<cfargument name="date_signature" required="false" type="Date" default="" hint="la date à laquelle est signé le contrat" />
		<cfargument name="date_echeance" required="false" type="Date" default="" hint="la date de fin du contrat" />
		<cfargument name="commentaire" required="false" type="String" default="" hint="le commentaire sur 300 caractères maximum" />
		<cfargument name="tarif" required="false" type="Numeric" default="" hint="le montant ponctuel du contrat" />
		<cfargument name="duree_contrat" required="false" type="Numeric" default="" hint="la durée en mois du contrat" />
		<cfargument name="tacite_reconduction" required="false" type="Numeric" default="" hint="1 = le contrat est reconduit automatiquement 0 = le contrat n'est pas reconduit automatiquement" />
		<cfargument name="date_debut" required="true" type="Date" default="" hint="la date de debut du contrat" />
		<cfargument name="duree_preavis" required="false" type="Numeric" default="" hint="la durée, en mois, du preavis de dénonciation du contrat" />
		<cfargument name="tarif_mensuel" required="false" type="Numeric" default="" hint="le tarif mensuel du contrat" />
		<cfargument name="date_resiliation" required="false" type="Date" default="" hint="la date de résiliation du contrat" />
		<cfargument name="date_renouvellement" required="false" type="Date" default="" hint="la date de renouvellement du contrat" />
		<cfargument name="date_elligibilite" required="false" type="Date" default="" hint="la date d'elligibilite du contrat" />
		<cfargument name="duree_eligibilite" required="false" type="Numeric" default="" hint="la durée d'éligibilité  en mois du contrat" />
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_CONTRAT.M_CTRT_EQ">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idContrat#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idTypeContrat#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idRevendeur#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#reference#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_signature).dateInverse#" null="#inverseDate(date_signature).boolNull#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_echeance).dateInverse#" null="#inverseDate(date_echeance).boolNull#">
			<cfprocparam type="in" cfsqltype="cf_sql_vARCHAR" value="#commentaire#">
			<cfprocparam type="in" cfsqltype="CF_SQL_FlOAT" value="#tarif#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#duree_contrat#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#tacite_reconduction#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#lsdateformat(date_debut,'yyyy/mm/dd')#" >
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#duree_preavis#">
			<cfprocparam type="in" cfsqltype="CF_SQL_FlOAT" value="#tarif_mensuel#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_resiliation).dateInverse#" null="#inverseDate(date_resiliation).boolNull#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_renouvellement).dateInverse#" null="#inverseDate(date_renouvellement).boolNull#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idracine#">
			<cfprocparam type="in" cfsqltype="cf_SQL_VARCHAR" value="#inverseDate(date_elligibilite).dateInverse#" null="#inverseDate(date_elligibilite).boolNull#">
			<cfprocparam type="in" cfsqltype="CF_SQL_NUMERIC" value="#duree_eligibilite#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#iduser#">
			<cfprocparam type="out" cfsqltype="CF_SQL_inTEGER" variable="pUpdateContrat">
			<cfprocresult name="pUpdateContrat">
		</cfstoredproc>
		<cfreturn pUpdateContrat>
	</cffunction>
	
	<cffunction name="deleteContrat" access="public" returntype="any" output="false" hint="suppression d'un contrat de service">
		<cfargument name="idContrat" required="true" type="Numeric" default="" hint="Initial value for the idContratproperty." />
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="PKG_CV_CONTRAT.D_CTR_EQ">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#idContrat#">
			<cfprocparam type="in" cfsqltype="CF_SQL_inTEGER" value="#iduser#">
			<cfprocparam type="out" cfsqltype="CF_SQL_inTEGER" variable="pDeleteContrat">
		</cfstoredproc>
		<cfreturn pDeleteContrat>
			
	</cffunction>
	
	<!--- fonction testant la valeur nulle d'un date --->
	<cffunction  name="inverseDate" access="private" returntype="struct">
		<cfargument name="laDate" required="true" type="date"/>
		<cfset retour = structnew()>		
		<cfset dateInverse = lsdateformat(laDate,'yyyy/mm/dd')>
		
		<cfif dateInverse eq '1970/01/01'>
			<cfset retour.dateInverse = "">
			<cfset retour.boolNull = true>
		<cfelse>
			<cfset retour.dateInverse = dateInverse>
			<cfset retour.boolNull = false>
		</cfif>
		
		<cfreturn retour>	
	</cffunction>
	
</cfcomponent>