<!---
Dans : fr.consotel.consoview.util
---> 
<cfcomponent name="sendMail">
	<cffunction name="sendSingleMail" access="remote" returntype="numeric" output="true">
		<cfargument name="mail" type="Mail" required="true">
		
		<cfset cc = mail.getCc()>
		<cfif mail.getCopiePourExpediteur() eq "YES">
			<cfset cc = mail.getCc() & "," & mail.getExpediteur()>
		</cfif>
			<cftry>
				<cfset str1 = 'SIZE="10"'>
				<cfset str2 = 'SIZE="1"'>
				<cfset message = #Replace(mail.getMessage(),str1,str2,"all")#>
			
				<cfmail from="#mail.getExpediteur()#"
						to="#mail.getDestinataire()#"
						subject="#mail.getModule()# : #mail.getSujet()#"
						type="#mail.getType()#"
						charset="#mail.getCharset()#"
						bcc="#mail.getBcc()#"
						cc="#cc#"
						replyto="#mail.getRepondreA()#"
						server="192.168.3.119"
						port="26">
						
						<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01//EN' 'http://www.w3.org/TR/html4/strict.dtd'>
						<html lang='fr'>
						<head>
						<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
						<body>									
							#message#
						</body>
						</html>
				</cfmail>				
				<cfcatch>
					<cfreturn -1>
				</cfcatch>	
			</cftry>
			<cfreturn 1>
	 </cffunction>

</cfcomponent>
