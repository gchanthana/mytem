package univers.inventaire.inventaire.creation.operationresiliation
{ 
	import composants.access.ListePerimetresWindow;
	import composants.access.perimetre.tree.NodeInfos;
	import composants.access.perimetre.tree.PerimetreTreeWindow;
	import composants.access.perimetre.tree.SearchPerimetreWindow;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.OpeSearchTree;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.controls.Tree;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.DragEvent;
	import mx.events.FlexEvent;
	import mx.events.ItemClickEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.cible.InvSearchTree;
	import univers.parametres.perimetres.rightSide.assignmentComponent.itemRender;
	import univers.inventaire.inventaire.journaux.SelectionOperationEvent;
	import composants.util.ConsoviewUtil;
	import mx.collections.IList;

	/**
	 * Classe gerant le formulaire de saisie pour la creation d'une operation de résiliation
	 * */
	public class IdentificationRessourcesResiliation extends IdentificationRessourcesResiliationIHM
	{   
		/**
		 * Constante definissant le type d'evenement dispatché lorsque l'on veut exporter la Demande
		 * */
		public static const AFFICHER_PDF : String = "afficherPDF";
		
		//Reference vers les methodes distantes qui gere le module 
		private var op : AbstractOperation;					
		
		//liste des produits client selectionnés
		private var tabElementSelectionnee : ArrayCollection;
		
		//l'identifiant du noeud sur lequel on clique
		private var nodeId : int; 
		
		//le libellé du noeud sur lequel on clique 
		private var nodeLibelle : String; 
		
		//le type de perimetre pour le noeud sur lequel on clique 
		private var nodePerimetre : String;	
		
		//La reference vers l'arbre de recherche
		private var myTree : InvSearchTree;
		
		//Tableau contenant les indices des produits selectionne dans le DataGrid  des produits;
		private var selectedIndeces : Array;
		
		//Tableau contenant la liste des opérateurs distribués par un distributeur ou une agence (pour filtrer avec la liste des operateur facturant des produits sur les lignes affectées au noeud)
		private var listeOperateur : Array; 
		
		//Selection des lignes --------------------------------------------------------------------------------------------
		
		//ArrayCollection contenant la listes des sous-tetes du noeud selectionné
		[Bindable]
		private var listeSousTete : ArrayCollection;
		
		//ArrayCollection contenant la listes des sous-tetes selectionnées
		[Bindable]
		private var selectedSousTete : Array;
		
		
		//Selection des produits --------------------------------------------------------------------------------------------
		
		//tableau tempon contenant la liste des ressources selectionnées pour la résiliation
		[Bindable]
		private var tmpSelectedProduitsClient : Array ;
			
		//ArrayCollection contenant la liste des ressources pour une sous-tetes
		[Bindable]
		private var listeProduitsClient : ArrayCollection = new ArrayCollection();
		
		//ArrayCollection contenant la liste des ressources selectionnées pour le résiliation
		[Bindable]
		private var listeProduitsClientSelectionnes : ArrayCollection = new ArrayCollection();
		
		
		/**
		 * Interrompt les remotings en cours
		 * Ne fait rien
		 * */ 
		public function clean():void{
			try{
				reInitPanel();
			}catch(e : Error){
				trace("ERROR IDETIFICATION RESSOURCES CLEAN");
			}
		} 
		 
		/**
		 * Constructeur
		 **/ 
		public function IdentificationRessourcesResiliation()
		{	
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);						
			myTree = new InvSearchTree();	
		}
		
		
		/**
		 * Filtre La liste des opérateurs du client 
		 * @param listeOp
		 * Ne fait rien pour le moment 
		 * */
		public function setListOperateur(liste : ArrayCollection):void{
			if (liste != null)
				if (liste.length > 0)
					listeOperateur = liste.source;
		}
	 
		 
		//Initialisation de l'IHM
		private function initIHM(fe : FlexEvent):void{						
			//Ressources ------------------------------------				
							
			compte.addChildAt(myTree,1);			
			configTreeIHM();
			
			
 			
			txtFiltreLigne.addEventListener(Event.CHANGE,filtrerLeDataGrid);	
			txtFiltreLigne.addEventListener(FocusEvent.FOCUS_IN,selectAllText);		
			
			
			myGridLigne.addEventListener(Event.CHANGE,gridLigneChangeHandler);				
			myGridLigne.doubleClickEnabled = true;
			myGridLigne.addEventListener(MouseEvent.DOUBLE_CLICK,gridLineDClickHandler);
			DataGridColumn(myGridLigne.columns[0]).labelFunction = formatDataTip;
			
			
			DataGridColumn(myGridListeProduitsClient.columns[0]).labelFunction = formatDataTip;	
			DataGridColumn(myGridListeProduitsClientSelectionnes.columns[0]).labelFunction = formatDataTip;
					
			//Produits Client ------------------------------------------
			
			liOperateur.addEventListener(ListEvent.CHANGE,cbxChangeHandler);
			
			txtFiltre.addEventListener(Event.CHANGE,filtreChangeHandler);
			
			txtFiltre.addEventListener(FocusEvent.FOCUS_IN,selectAllText);
			
			cboInventaire.addEventListener(Event.CHANGE,filtreChangeHandler);
			
			
			//cbxSelectAll.selected = false;
			//cbxSelectAll.addEventListener(Event.CHANGE,selectAllProducts);
			 
			//Action 
			btEnregistrer.enabled = true;
			btEnregistrer.addEventListener(MouseEvent.CLICK,enregistrerHandler);		 			
			btAnnuler.addEventListener(MouseEvent.CLICK,annulerHandler);				
			//btPDF.addEventListener(MouseEvent.CLICK,afficherPDF);
			//myGridListeProduitsClient.addEventListener(Event.CHANGE,selectionnerProduitClient);
			
			//myGridListeProduitsClientSelectionnes.doubleClickEnabled = true;
			//myGridListeProduitsClientSelectionnes.addEventListener(MouseEvent.DOUBLE_CLICK,deselectProduitClient);
			
			myGridListeProduitsClient.dataProvider = listeProduitsClient;
			
			myGridListeProduitsClientSelectionnes.allowMultipleSelection = true;
			myGridListeProduitsClientSelectionnes.dataProvider = listeProduitsClientSelectionnes;
			//bouttons
			btUp.addEventListener(MouseEvent.CLICK,monterLesProduitsSelectionnes);
			btAllUp.addEventListener(MouseEvent.CLICK,monterTousLesProduitsSelectionnes);
			btDown.addEventListener(MouseEvent.CLICK,descendreLesProduisSelectionnes);
			btAllDown.addEventListener(MouseEvent.CLICK,descendreTousLesProduitsSelectionnes);
			
			getListeOperateur();  
		}
		
		
//navigation ---------------------------------------------------------------------------------------------------------
		
		
		
		
//arbre ---------------------------------------------------------------------------------------
		
		//Formate l'arbre de a recherche
		private function configTreeIHM():void{
			myTree.percentWidth = 100;
			SearchPerimetreWindow(myTree.searchTree).searchInput.width = 80;
			PerimetreTreeWindow(myTree.perimetreTree).searchInput.width = 80;
			SearchPerimetreWindow(myTree.searchTree).searchInput.styleName = "TextInputInventaire";
			PerimetreTreeWindow(myTree.perimetreTree).searchInput.styleName = "TextInputInventaire";
			SearchPerimetreWindow(myTree.searchTree).btnBack.styleName = "MainButton";
			SearchPerimetreWindow(myTree.searchTree).searchTree.setStyle("borderStyle","solid");
			PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.setStyle("borderStyle","solid");
			
			SearchPerimetreWindow(myTree.searchTree).lblRecherche.visible = false;
			SearchPerimetreWindow(myTree.searchTree).lblRecherche.width = 0;
			
			
			
			SearchPerimetreWindow(myTree.searchTree).searchTree.liveScrolling = false;
			PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.liveScrolling = false;
			
			SearchPerimetreWindow(myTree.searchTree).btnBack.addEventListener(MouseEvent.CLICK,treeGetBack);	
			myTree.addEventListener(InvSearchTree.NODE_CHANGED,chargerListeSousTete);	
				
 			myTree.height = 152;
		}
		
		//Charge la liste de sous-tete lors d'un click sur un element de l'arbre
		private function chargerListeSousTete(e : Event):void{
		 
			if (myTree.nodeInfo != null && myTree.nodeInfo.hasOwnProperty("TYPE_LOGIQUE")){
				if ((String(myTree.nodeInfo.TYPE_LOGIQUE) == "ANA")){					 
					SearchPerimetreWindow(myTree.searchTree).searchTree.selectedIndex = -1;
					PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.selectedIndex = -1;
					myTree.nodeInfo = null;
					myGridLigne.dataProvider = null; 
					e.preventDefault();
				}else /* if (myTree.nodeInfo.TYPE_LOGIQUE=="OPE" ) */{
					//param pour le retour															
					
					setLocalNodeParams(myTree.nodeInfo);										
					txtFiltre.enabled = true;
					getLinesFromNode();
					
					listeProduitsClient.source = null;
					listeProduitsClient.refresh();
						
					listeProduitsClientSelectionnes.source = null;
					listeProduitsClientSelectionnes.refresh();
					
					myTree.nodeInfo = null;
				}
			}else{
				myGridLigne.dataProvider = null; 
				SearchPerimetreWindow(myTree.searchTree).searchTree.selectedIndex = -1;
				PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.selectedIndex = -1;
				myTree.nodeInfo = null;
				unSetLocalNodeParams();								
				e.preventDefault();
			}
		}
		
		//Ne fait rien (Handler du click sur le bouton retour de la recherche)
		//Recharge la liste des sous-tete pour l'ancien noeud
		private function treeGetBack(me : MouseEvent):void{
			if (myTree.backedUpNodeInfo != null){
				myTree.nodeInfo = myTree.backedUpNodeInfo;
				//lastNodeInfo = null;
				chargerListeSousTete(null);	
			}
		}
		
		//change la racine de l'arbre par le noeud correspondant à l'opérateur  selectionné dans la liste
		public function cbxChangeHandler(evt :ListEvent):void{
			if (liOperateur.selectedIndex != -1){
				configTreeForOP(liOperateur.selectedItem.OPERATEURID,liOperateur.selectedItem.NODID);	
			}
						
			
			listeProduitsClient.source = null;
			listeProduitsClient.refresh();
			
			listeProduitsClientSelectionnes.source = null;
			listeProduitsClientSelectionnes.refresh();
			
			try{
				listeSousTete.source = null;
				listeSousTete.refresh();	
			}catch(e : Error){
				trace("pas de sous tete");
			}
			 
		}
		
		//Met à jour les parametres concernant l'element de l'arbre sélectionné
		//param in node un objet avec les attributs suivants TYPE_PERIMETRE,NID,LBL
		private function setLocalNodeParams(node : Object):void{			
			nodePerimetre = node.TYPE_PERIMETRE;
			nodeId = node.NID;
			nodeLibelle = node.LBL;
						
		}
		
		//Remet à zero les parametres concernant l'element de l'arbre sélectionné	
		private function unSetLocalNodeParams():void{
			
			nodeId = -1;
			nodeLibelle = "";
			nodePerimetre = "";		
		}
		
		
//fin arbre---------------------------------------------------------------------------------------------------------------

//Grid des lignes -------------------------------------------------		


		//gere le double click sur une ligne du tabelau des sous-tetes
		private function gridLineDClickHandler(me : MouseEvent):void{
			gridLigneChangeHandler(null);			
		}		
		
		//gere le click sur une ligne du tabelau des sous-tetes
		private function gridLigneChangeHandler(ev : Event):void{
			selectedSousTete = new Array();			
			selectedSousTete = myGridLigne.selectedItems;
			
									
			if (selectedSousTete.length > 0){				
				
				chargerListeProduitsHandler();
			}
		}
		
		//Extrait une colone d'un tableau et retourne le résultat sous forme de tableau
		//param in data le tableau source
		//		   colname le nom de la colonne à extraire
		//param out array la colonne extraite sous forme de tableau
		private function formateData( data : Array , colname : String):Array{
			var s : String;
			var a : Array = new Array();
			for (var i : uint = 0 ; i < data.length ; i++ ){
				a.push(data[i][colname]);
			}
			return a;
		}	
		
		//selectionne le texte taper dans le filtre au focus in
		private function selectAllText(fe : FocusEvent):void{
			var len : int = TextInput(fe.currentTarget).text.length;
			if (len != 0){									
				TextInput(fe.currentTarget).setSelection(0,len);					
			}
		}
		
		//-----------------------------------------------------------------------------------------------		
		
		//formate le numéro de telephone dans le tableau des sous-tetes (format ## ## ## ## ##)
		private function formatDataTip(item : Object, column : DataGridColumn):String{
			
			if ((String(item.SOUS_TETE).length == 10)&& parseInt(item.SOUS_TETE).toString().length == 9)
					return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);
				else
					return item.SOUS_TETE;
			
		}
		
		
		//---------filtres------------------------------------------------------------
		
		//Gere le filtre du tableau des sous-tetes
		private function filtrerLeDataGrid(e : Event):void{
			if(listeSousTete != null){
				listeSousTete.filterFunction = processfitrerLeDataGrid;
				listeSousTete.refresh();	
			}
		}
	
		///filtre sur les attributs SOUS_TETE des elements du tableau des sous-tetes
		private function processfitrerLeDataGrid(value : Object):Boolean{
		 	if (String(value.SOUS_TETE.toLowerCase()).search(txtFiltreLigne.text.toLowerCase()) != -1) {
				return (true);
			} else {
				return (false);
			}

		}
		
		//Gere le filtre du tableau des ressources des lignes selectionnées
		private function filtreChangeHandler(ev : Event):void{
			try{
				listeProduitsClient.filterFunction = filterFunc;
				listeProduitsClient.refresh();
			}catch(e : Error){
				trace("Erreur filtre liste Elements de facturation");
			}
			
		}
		
		//filtre sur les attributs SOUS_TETE, LIBELLE_PRODUIT du tableau des ressources des lignes selectionnées
		//voir la methode filterFunction d'un ArrayCollection
		private function filterFunc(item:Object):Boolean {	   
			var lettreMatch : Boolean = ((String(item.SOUS_TETE).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1)  
					   				||
					 (String(item.LIBELLE_PRODUIT).toLowerCase().search(String(txtFiltre.text).toLowerCase()) != -1));
	
	      switch (cboInventaire.selectedIndex) {
	         case 0: 
				if (lettreMatch && (Number(item.DANS_INVENTAIRE) == 1))return true
				else return false
	         case 1:
				if (lettreMatch && (Number(item.DANS_INVENTAIRE) == 0))return true
				else return false	
	         case 2: 
				if (lettreMatch && ((Number(item.DANS_INVENTAIRE) == 1)	|| (Number(item.DANS_INVENTAIRE) == 0)))return true					
				else return false
	         default: 
				return false;
	      }
			
		} 

		//Initialise l'arbre de recherche avec le noeud dont l'id est passé en parametre
		//param in opid l'identifiant de l'opérateur correspondant au noeud (non utilisé)
		//param int nodeId l'identifiant du noeud que l'on veut pour racine
		//Ne marche que si le noeud correspondant à l'id a été chargé au préalable
		private function configTreeForOP(opid : int, nodeId : int):void{
			myTree.initDP(nodeId);
			callLater(selectFirstTreeItem,[nodeId]);
			
		}
		
		private function selectFirstTreeItem(nodeId : Number):void{
			if (PerimetreTreeWindow(myTree.perimetreTree).perimetreTree != null)
				PerimetreTreeWindow(myTree.perimetreTree).perimetreTree.selectNodeById(int(nodeId));
		}
		
		//Regarde si la valeur passée en parametre est presente dans le tableau passer en param
		//param in s une chaine de caractere
		//param in a le tableau dans lequel on cherche
		//param out un booleen, true si la chaine correspond exactement a un élément du tableau, si non false
		private function isPresent(s:String, a : Array):Boolean{			
			var OK:Boolean = false;			
			for (var i:int = 0; i < a.length; i++){				 
					 if ( s == a[i].toString() ) OK = true;				 
			}
			return OK;
		}
		
		
		//Regarde si la valeur passée en paramètre est egale à un des élément dans la colonne choisie du tableau passé en paramètre
		//param in param la chaine de caractere que l'on cherche
		//param in colname le nom de la colonne dans laquelle on cherche
		//param in acol le tableau dans lequel on fait la recherche
		//param out un booléen true si l'élément à été trouvé, si non false
		private function isInArray(param:String, colname:String, acol : Array):Boolean{			
			var OK:Boolean = false;
			var ligne : Object;				
			var len : int = acol.length;
			for (ligne in acol){
				
				trace("param = "+param,"acol["+ligne+"][colname]= " + acol[ligne][colname]);		 
 				if (param == acol[ligne][colname]) OK = true; 
			}
			trace ("OK = "+ OK)
			return OK;
		}
		
		//bt------------------------------------------------------------------------------------------------
		
		//Annule le formulaire de creation
		//Dispatche un évenement de type 'AnnulerCreation' signifiant que l'on a annuler le formulaire
		private function annulerHandler(me : MouseEvent):void{
			ConsoviewAlert.afficherAlertConfirmation("Etes vous sur de vouloir Annuler l'Opération","Confirmation",executeannulerHandler);			
		}
		
		private function executeannulerHandler(eventObj:CloseEvent):void{	
			if (eventObj.detail == Alert.OK){	
				reInitPanel();			
				dispatchEvent(new Event("AnnulerCreation"));			
			}
		}
		
		
		//charge la liste des ressources des lignes sélectionnées
		private function chargerListeProduitsHandler():void{
			chargerListeProduitsClient();
		}
		
		//Affcihe une dialog box de confirmation, avant d'enregistrer la saisie
		private function enregistrerHandler(me : MouseEvent):void{	
			
			//afficher alert de confirmation	
			ConsoviewAlert.afficherAlertConfirmation("Etes vous sur de vouloir Enregistrer l'Opération","Confirmation",executerEnregistrer);
		}
		
		
		//Vérifie la validitée du formulaire
		//Si le formulaire est valide, dispatche un évenement SelectionProduitEvent de type 'EnregistrerSelectionProduit'
		// avec la sélection signifiant que l'on veut enregistrer cette sélection
		private function executerEnregistrer(eventObj:CloseEvent):void{
			
			if (eventObj.detail == Alert.OK){		
				listeProduitsClientSelectionnes.refresh();		
				var tmpArray : Array = formateData(myGridListeProduitsClientSelectionnes.dataProvider.source,"IDINVENTAIRE_PRODUIT");				
				var eventObjbis : SelectionProduitEvent = new SelectionProduitEvent("EnregistrerSelectionProduit");
				eventObjbis.tabProduits = tmpArray;	
				dispatchEvent(eventObjbis);			
			}
		}
		
//----------- SELECTION DES PRODUITS --------------------------------------------------------------------------------

		//Prend les élements séléctionnés du tableau des ressources, et les met dans le tableau des ressources à résilier
		private function descendreLesProduisSelectionnes(me:MouseEvent):void{
				try{	
					var len : int = myGridListeProduitsClient.selectedIndices.length;
					
					if (len > 1){
							
							myGridListeProduitsClient.selectedItems.forEach(descendreLeProduit);
							myGridListeProduitsClientSelectionnes.dataProvider.refresh();
							myGridListeProduitsClient.dataProvider.refresh();
					}else if (len == 1){
						var index : int = myGridListeProduitsClient.selectedIndex
						
						
						myGridListeProduitsClientSelectionnes.dataProvider.
							addItem(myGridListeProduitsClient.
							dataProvider.removeItemAt(index));	
							
						myGridListeProduitsClientSelectionnes.dataProvider.refresh();
						myGridListeProduitsClient.dataProvider.refresh();				
					}
					
				}catch( re : RangeError ){
					trace("l'indice sort des limites")
				}catch( e : Error){
					trace(e.message,e.getStackTrace());
				}	
		}
		
		//Prend tous élements du tableau des ressources, et les met dans le tableau des ressources à résilier
		private function descendreTousLesProduitsSelectionnes(me : MouseEvent):void{
			var len : int = (myGridListeProduitsClient.dataProvider as ArrayCollection).length;
			var arr : Array = (myGridListeProduitsClient.dataProvider as ArrayCollection).toArray();
			
			if (len > 0)
			{
				arr.forEach(descendreLeProduit);
				myGridListeProduitsClient.dataProvider.refresh();		
				myGridListeProduitsClientSelectionnes.dataProvider.refresh();												
			}
		}
		
		//Prend un élément du tableau appelant et le met dans le tableau des ressources à résilier
		//Voir la méthode forEach de la class Array pour les parametres
		private function descendreLeProduit(element : * , index : int , arr : Array):void{
			var liste : ArrayCollection = new ArrayCollection()
			liste.list = (myGridListeProduitsClient.dataProvider as ArrayCollection).list;
			var index : int = ConsoviewUtil.getIndexById
								(liste,"IDINVENTAIRE_PRODUIT",element.IDINVENTAIRE_PRODUIT);
			
			(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).addItem(element);			
			liste.removeItemAt(index);
			
			
		}
		
		//Prend les élements  sélectionnés du tableau des ressources à résilier, et les met dans le tableau des ressources
		private function monterLesProduitsSelectionnes(me : MouseEvent):void{		
			try{	
				var len : int = myGridListeProduitsClientSelectionnes.selectedIndices.length;
				
				if (len > 1){
						myGridListeProduitsClientSelectionnes.selectedItems.forEach(monterLeProduit);
						myGridListeProduitsClientSelectionnes.dataProvider.refresh();
						myGridListeProduitsClient.dataProvider.refresh();
				}else if (len == 1){
					myGridListeProduitsClient.dataProvider.
						addItem(myGridListeProduitsClientSelectionnes.
						dataProvider.removeItemAt(myGridListeProduitsClientSelectionnes.selectedIndex));	
					myGridListeProduitsClientSelectionnes.dataProvider.refresh();
					myGridListeProduitsClient.dataProvider.refresh();				
				}
				
			}catch( re : RangeError ){
				trace("l'indeice sort des limites")
			}catch( e : Error){
				trace(e.message,e.getStackTrace());
			}			
		}
		
		//Prend tous élements du tableau des ressources à résilier, et les met dans le tableau des ressources 
		private function monterTousLesProduitsSelectionnes(me:MouseEvent):void{
			if ((myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).length > 0){
				(myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection).source.forEach(monterLeProduit);
				myGridListeProduitsClientSelectionnes.dataProvider.refresh();
				myGridListeProduitsClient.dataProvider.refresh();			
			}	
		}
		
		//Prend un élement du tableau des ressources à résilier, et le met dans le tableau des ressources 
		private function monterLeProduit(element : * , index : int , arr : Array):void{
			(myGridListeProduitsClient.dataProvider as ArrayCollection).
				addItem((myGridListeProduitsClientSelectionnes.dataProvider as ArrayCollection)
					.removeItemAt(0));								
		}
	
//----------- REMOTINGS ------------------------------------------------------------------------------------//
		//------------------------------------
		
		
		//Charge les lignes du noeud selectioné							
		private function getLinesFromNode():void{
			
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.recherche.facade",
												  "rechercherListeLigne",
												  getLinesFromNodeResultHandler);
												  
			RemoteObjectUtil.callService(op,
										nodePerimetre,										
										nodeId); 
		}
		
		//Handler de la fonction 'getLinesFromNode'
		//Affecte le résultat à l'ArrayCollection listeSousTete
		//Affiche ce tableau via le DataGrid 'myGridLigne'
		//Efface le texte eventuellement présent dans la filtre
		private function getLinesFromNodeResultHandler(re : ResultEvent):void{			
			listeSousTete = re.result as ArrayCollection;			
			myGridLigne.dataProvider = listeSousTete;
			filtrerLeDataGrid(null);
		}
		
		//Récupere les ressources pour une ligne							
		private function chargerListeProduitsClient():void{				
				op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
												  "fr.consotel.consoview.inventaire.cycledevie.ElementDInventaire",
												  "getListeElementIventaire",
												  chargerListeProduitsClientResultHandler);
												  
				RemoteObjectUtil.callService(op,formateData(selectedSousTete,"IDSOUS_TETES")); 						
		}			
		
		
		//Handler de la fonction 'chargerListeProduitsClient'	
		//Affiche les données dans le DataGrid 'myGridListeProduitsClient'	 
		private function chargerListeProduitsClientResultHandler(re : ResultEvent):void{
			/// charger liste elements de facturation 
				
				var tmpListeProduitsClient : ArrayCollection = re.result as ArrayCollection;		 
				tmpListeProduitsClient.source.forEach(ajouterProduitClient);				
				myGridListeProduitsClient.dataProvider = listeProduitsClient;
				filtreChangeHandler(null);				
				txtFiltre.enabled = true;			 
		}
		
		//ajoute l'element au tableau des reeources sur ligne s'il n'est dans aucun des deux tableaux (ressources sur lignes et ressources à résilier)
		private function ajouterProduitClient(element:*, index:Number, arr:Array):void{				
			if (!isInArray(element.IDINVENTAIRE_PRODUIT,"IDINVENTAIRE_PRODUIT",listeProduitsClient.source) &&
				!isInArray(element.IDINVENTAIRE_PRODUIT,"IDINVENTAIRE_PRODUIT",listeProduitsClientSelectionnes.source)){
				listeProduitsClient.addItem(element);
			}			
		}
		
		//Recupère la liste des opérateurs qui facturent des produits sur les lignes du périmètre su lequel on est.
		private function getListeOperateur():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			var typeGroupe : String = CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Inventaire",
																				"getListeOperateur",
																				getListeOperateurResultHandler);				
			RemoteObjectUtil.callService(op,idGroupeMaitre
										   ,typeGroupe);		
		}
		
		//Handler de la méthode 'getListeOperateur'
		//Affiche la liste des opérateurs dans une 'list liOperateur'
		private function getListeOperateurResultHandler(re :ResultEvent):void{
			if (re.result){
				liOperateur.dataProvider = re.result as ArrayCollection;
				liOperateur.labelField = "OPNOM"; 
				liOperateur.selectedIndex = 0;
				liOperateur.dispatchEvent(new ListEvent(ListEvent.CHANGE));	
			} 
		}
				
///- FIN --- REMOTINGS ------------------------------------------------------------------------------------------------------------------		


//------ PDF ----------------------------------------------------------------------------------------------------------------------------
	private function afficherPDF(me : MouseEvent):void{
		var evtObj : SelectionProduitEvent = new SelectionProduitEvent(AFFICHER_PDF);
		evtObj.tabProduits = listeProduitsClientSelectionnes.source;
		dispatchEvent(evtObj);
	}


//---- FIN PDF --------------------------------------------------------------------------------------------------------------------------				
		//reinitialiser le composants
		//ne fait rien pour le moment
		private function reInitPanel():void{
			/* myGridLigne.dataProvider = null;			
			myTree.dataProvider = null;
			lblNbNoeud.text = "";
			cbFiltre.selected = false;	 */
						
		}	
		
	}
}