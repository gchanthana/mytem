package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.UniversManager;
	import univers.facturation.suivi.factures.Index;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheContact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.PanelRechercheSociete;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Contact;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Societe;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.IDemande;
	
	public class NouvelleLigne extends NouvelleLigneIHM implements IDemande
	{
		public static const  EN_COURS : int = 1;
		public static const  ANNULER : int = 2;
		public static const  LIVREE : int = 3;
		public static const  RAPPROCHEE : int = 4;
						
		public static const UPDATE_COMPLETE : String = "demandeUpdated";
		public static const SAVE_COMPLETE : String = "demandeSaved";
		public static const LISTE_COMPLETE : String = "demandeListOk";
		public static const DELETE_COMPLETE : String = "demandeDeleted";
		public static const SORTIR : String = "sortirDemande";
		
		private const NOUVELLE_LIGNE : int = 1;
		private const TYPE_COMMANDE : String = "NouvellesLignes";  
				
		private var opOperateur : AbstractOperation;
		private var currentSocieteId : int = 0;
		private var currentOperateurId : int = 0; 
		private var currentContactID : int = 0;
		//rapprochement
		private var rapprochement : Rapprochement;
		
		
		/// recherche contact / société
		private var rechercheContact : PanelRechercheContact;
		private var rechercheSociete : PanelRechercheSociete;		
		private var listeOperateur : ArrayCollection;		
		private var idGroupe : int =  UniversManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;	
		
		private var commande : Commande;
		private var panier : ElementCommande;
		private var themes : ArrayCollection;
		private var contacts : Contact;
		private var societes : Societe;
		
		private var opTheme : AbstractOperation;		
		
		private var indexLigne : int = 0;
						
		public function NouvelleLigne()
		{
			//TODO: implement function
			super();
						
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}
		
		public function clean():void{			
			if (opTheme != null) opTheme.cancel();
			if (commande != null)commande.clean();
			if (panier != null) panier.clean();			
		}
		
		public function cacherRapprochement():void{
			sortirDuRAprochementPassif(new FlexEvent(FlexEvent.HIDE));
		}
		
		private var _idDemande : int = 0;
		public function set idDemande(idd : int):void{
			_idDemande = idd;
		}
		
		public function get idDemande():int{
			return _idDemande;
		}
		
		public function getDemande(idd : int):void{
			if (idd > 0){
				commande = new Commande(idd);														
				
			}else{
				commande = new Commande();	
				commande.typeCommande = 1;									
			}
			
			contacts = new Contact();
			societes = new Societe();
			panier = new ElementCommande();									
			initCommande();
		}
		
		private function initIHM(fe : FlexEvent):void{
			addEventListener(FlexEvent.HIDE,sortirDuRAprochementPassif);
			addEventListener(FlexEvent.REMOVE,sortirDuRAprochementPassif);
			addEventListener(FlexEvent.EXIT_STATE,sortirDuRAprochementPassif);
			
			//rechercher Contact
			imgSearchContact.addEventListener(MouseEvent.CLICK,afficherRechercheContact);
			imgSearchSociete.addEventListener(MouseEvent.CLICK,afficherRechercheSociete);						
			
			cmbContact.addEventListener(Event.CHANGE,selectSocieteFromContact);													
			
			
			cmbSociete.addEventListener(Event.CHANGE,selectOperateursBySociete);
			cmbOperateur.addEventListener(Event.CHANGE,selectThemesByOperateur);
			
			btSortir.addEventListener(MouseEvent.CLICK,sortirDeLaCommande);
			btEnregistrer.addEventListener(MouseEvent.CLICK,sauvegarderCommande);				
			btPDF.addEventListener(MouseEvent.CLICK,exporterPDF);
			
			btAjouterLignes.addEventListener(MouseEvent.CLICK,ajouterLignes);			
			btSuppLignes.addEventListener(MouseEvent.CLICK,supprimerLignes);
			btRapprocher.addEventListener(MouseEvent.CLICK,rapprocherLigne);
			
			txtFiltreLignes.addEventListener(Event.CHANGE,filtrerGrid);
			
			colRapp.labelFunction = formateFlagRapproche;
			
			
			contacts.addEventListener(Contact.LISTE_COMPLETE,remplirComboContact);							
			contacts.prepareList(idGroupe);				
			
			societes.addEventListener(Societe.LISTE_COMPLETE,remplirComboSociete);
			
			myGridLignes.addEventListener(DataGridEvent.ITEM_EDIT_END,modifierCetteLigne);
			
			//getListeOperateur();
			getListeTheme();				
		}
		
		//-----FILTRE
		private function filtrerGrid(ev : Event):void{
			if (myGridLignes.dataProvider != null){
				(myGridLignes.dataProvider as ArrayCollection).filterFunction = filterFunc;
				(myGridLignes.dataProvider as ArrayCollection).refresh();	
			}			
		}
		
		private function filterFunc(value : Object):Boolean{
			if ((String(value.libelletheme).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1) 
				||
				(String(value.libelleLigne).toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1) 
				/* ||
				((String(value.commentaire).toLowerCase().search(txtFiltreLignes.text.toLowerCase() != -1)  */)
			{
				return true;
			}else{
				return false;
			}
				
		}
		
		//----- FORMAT			
		private function formateFlagRapproche(item : Object, column : DataGridColumn):String{
			return (int(item[column.dataField]) == 1)? "R" : "NR";
		}
		
		//----- INFORMATIONS
		
		
		//------ Recherche
		private function afficherRechercheContact(me :MouseEvent):void{
			rechercheContact = new PanelRechercheContact();	
			rechercheContact.addEventListener(PanelRechercheContact.CREER,rafraichirContact);
			rechercheContact.addEventListener(PanelRechercheContact.EFFACER,rafraichirContact);
			rechercheContact.addEventListener(PanelRechercheContact.VALIDER,selectionnerContact);		
			PopUpManager.addPopUp(rechercheContact,this,true);	
			PopUpManager.centerPopUp(rechercheContact);
		}
		
		private function rafraichirContact(ev : Event):void{
			contacts.prepareList(idGroupe);
		}
		
		private function afficherRechercheSociete(me : MouseEvent):void{
			rechercheSociete = new PanelRechercheSociete();
			rechercheSociete.addEventListener(PanelRechercheSociete.CREER,rafraichirSociete);
			rechercheSociete.addEventListener(PanelRechercheSociete.EFFACER,rafraichirSociete);
			rechercheSociete.addEventListener(PanelRechercheSociete.VALIDER,selectionnerSociete);		
			PopUpManager.addPopUp(rechercheSociete,this,true);	
			PopUpManager.centerPopUp(rechercheSociete);		
		}
		
		private function rafraichirSociete(ezv : Event):void{
			societes.prepareList(idGroupe);
		}
		
		//---- COMMANDE
		private function initCommande():void{				
			commande.addEventListener(Commande.CREATION_COMPLETE,afficherCommande);
			commande.addEventListener(Commande.SAVE_COMPLETE,commandeCree);
			commande.addEventListener(Commande.UPDATE_COMPLETE,commandeUpdated);
			commande.addEventListener(Commande.UPDATESTATUT_COMPLETE,commandeStautUpdated);
			initPanier();	
			
		}
		
		private function sortirDeLaCommande(me : MouseEvent):void{
			clean();
			rapprochement = null;
			dispatchEvent(new Event(NouvelleLigne.SORTIR));
			
		}
		
		private function sauvegarderCommande(me : MouseEvent):void{			
			//1.Informations
			
			commande.libelle = txtLibelle.text;
			commande.refClient = txtReferenceCli.text;
			commande.refOperateur = txtReferenceOpe.text;
			commande.dateEffective = dfDateEffet.text;
			commande.dateLivraison = dfDateLivraison.text;
			commande.statutID = EN_COURS;
						
			//2.Opérateur		
			if (cmbContact.selectedIndex != -1){
				commande.contactID = cmbContact.selectedItem.contactID;					
			}else{
				commande.contactID = 0;					
			}
			if (cmbOperateur.selectedIndex != -1){
				commande.operateurID = cmbOperateur.selectedItem.OPERATEURID; 			
			}else{
				commande.operateurID = 0;
			}
			
			//commande.societeID = contacts.societeID;
			
			if (cmbSociete.selectedIndex != -1){
				commande.societeID = cmbSociete.selectedItem.societeID;				
			}else{
				commande.societeID = 0;
			}		
				
			//3.Cible
			try{
				
				commande.idGroupeCible = treeCible.myTree.selectedItem.@NODE_ID;
			
			}catch(e :Error){
				trace("pas de cible selectionne");
			}
			
			commande.flagCible = int(cbNewCible.selected);
			commande.idGroupeClient = UniversManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
				
			if (!(commande.commandeID)){
				if (checkCommande(commande)){
					commande.sauvegarder(idGroupe);	
				}	
			}else {
				if (checkCommande(commande)){
					commande.updateCommande();	
				}				
			}
		}
		
		private function checkCommande(c : Commande):Boolean{
			var libelleOk : Boolean = false;
			var dateEffetOk : Boolean = false;
			var contactOk : Boolean = false;
			var produitsOk : Boolean = false;
			
			var message : String = "\n";
			var pronpt : String;
			var count : int = 0;
			
			if (c.libelle.length > 0) libelleOk = true;
			else {
				message = message + "- Libellé.\n";
				count++;
			}
			
			if (dfDateEffet.text.length > 0) dateEffetOk = true;
			else {
				message = message + "- Date effective.\n";
				count++;
			}
			
			if (cmbContact.selectedIndex != -1) contactOk = true;
			else {
				message = message + "- Contact\n";
				count++;
			}
			
			if (myGridLignes.dataProvider != null){
				if ((myGridLignes.dataProvider as ArrayCollection).length >0){
					return true;
				}else{
					message = message + "- La grille des produits\n";
					count++;
				}
			}else{
				message = message + "- La grille des produits\n";
				count++;
			} 
			
			if (libelleOk && dateEffetOk && contactOk && produitsOk){
				return true;
			}else{
				if (count > 1){
					pronpt = "Les champs suivant sont obligatoires.";
				}else{
					pronpt = "Le champ suivant est obligatoire."
				}
				Alert.show(message,pronpt);	
				return false;
			}
		}
		
		private function annulerCommande(me : MouseEvent):void{
			if (commande.commandeID) {
				commande.statutID = ANNULER;
				commande.updateStatut();				
			}
		}
		
		private function reprendreCommande(me : MouseEvent):void{
			if (commande.commandeID) {
				commande.statutID = EN_COURS;
				commande.updateStatut();				
			}
		}
		
		
		//--- EXPORT PDF --------------------
		private function exporterPDF(me : MouseEvent):void{
			if (commande.commandeID != 0){
				exporterLePdf();
			}
		}
		
		private function exporterLePdf():void{
			displayExport("pdf");
		}
		
		private function displayExport(format : String):void {			 
			var url:String = cv.NonSecureUrlBackoffice+"/fr/consotel/consoview/cfm/cycledevie/demandes/Display_Demande.cfm";
            var variables:URLVariables = new URLVariables();
            variables.FORMAT = format;	              
            variables.TYPE = TYPE_COMMANDE;
            
            var demande : Object = new Object();
            
			variables.TITRE = "Demande de nouvelles lignes";	 
			variables.PERIMETRE_LIBELLE = CvAccessManager.getCurrentPerimetre().PERIMETRE_LIBELLE;           			
			//infos commandes
			variables.LIBELLE = commande.libelle;
			variables.REF_CLIENT = commande.refClient;				
			variables.REF_OPERATEUR = commande.refOperateur;				
			variables.DATE_EFFECTIVE = commande.dateEffective;
			variables.DATE_LIVRAISON = commande.dateCreation;
			variables.SATUT = commande.statut;
			
			//operateur
			variables.CONTACT = Contact(cmbContact.selectedItem).nom + " " + Contact(cmbContact.selectedItem).prenom;
			variables.OPERATEUR = commande.operateurNom;				
			variables.SOCIETE = Societe(cmbSociete.selectedItem).raisonSociale;
			
			//cible
			variables.CIBLE = "TO DO";
			variables.FLAG_CIBLE = commande.flagCible;
			
			variables.LI_LIGNES = ConsoviewUtil.extraireColonne(panier.liste,"libelleLigne");
			variables.LI_THEME = ConsoviewUtil.extraireColonne(panier.liste,"libelletheme");
			variables.LI_COMMENTAIRE = ConsoviewUtil.extraireColonne(panier.liste,"commentaire");
			
            var request:URLRequest = new URLRequest(url);
            request.data = variables;
            request.method=URLRequestMethod.POST;
            
            navigateToURL(request,"_blank");
        } 			
        //------ FIN EXPORT PDF -----------------
		
		private function afficherCommande(ev : Event):void{
			
			//1.Informations
			txtLibelle.text = commande.libelle;
			txtReferenceCli.text = commande.refClient;
			txtReferenceOpe.text = commande.refOperateur;
			
			lblStatut.text= commande.statut;
			configBtAnuller(commande.statutID);
			
			dfDateEffet.selectedDate = (commande.dateEffective != null)? new Date(commande.dateEffective) : null;
			dfDateLivraison.selectedDate = (commande.dateLivraison != null)? new Date(commande.dateLivraison) : null;
			
			
			//2.Contact
			//contacts.prepareList(idGroupe);
						 
			currentContactID = commande.contactID;
			currentSocieteId = commande.societeID;			
			currentOperateurId = commande.operateurID;
			
			
			//3.Cible			
			if (commande.idGroupeCible != 0){
				try{					
					selectNode(commande.idGroupeCible);
				}catch(e : Error){
					
				}
			}
			
			cbNewCible.selected = Boolean(commande.flagCible == 1);			
			//4.lignes						
			panier.prepareList(commande.commandeID,NOUVELLE_LIGNE);			
			desactiverFormulaireOperateur();
		}
		
		private function selectContact():void{			
			//cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,"societeID",commande.societeID);
			//cmbOperateur.selectedIndex = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"OPERATEURID",commande.operateurID);
		}
		
		private function selectNode(nodeId : int):void
		{
			trace(nodeId);
			if (treeCible.myTree.selectedIndex < 0 && nodeId >= 0) {
				var tmp:XMLList = (treeCible.myTree.dataProvider[0] as XML).descendants().(@NODE_ID==nodeId);
				if (tmp[0] == null)
					return ;
				treeCible.expandToNode(tmp,true);
				treeCible.myTree.selectedItem = tmp;
				treeCible.myTree.scrollToIndex(treeCible.myTree.selectedIndex);
			}
		}
		
		private function configBtAnuller(statutID : int):void{
			if (statutID == ANNULER) {
				btAnnuler.label = "Reprendre";
				if (btAnnuler.hasEventListener(MouseEvent.CLICK)){
					try{
						btAnnuler.removeEventListener(MouseEvent.CLICK,annulerCommande);	
					}catch(e : Error){						
					}					
				}
				btAnnuler.addEventListener(MouseEvent.CLICK,reprendreCommande);
				desactiverFormulaire();
			}else if (statutID == EN_COURS){
				btAnnuler.label = "Annuler";
				if (btAnnuler.hasEventListener(MouseEvent.CLICK)){
					try{
						btAnnuler.removeEventListener(MouseEvent.CLICK,reprendreCommande);	
					}catch(e : Error){						
					}finally{
						btAnnuler.addEventListener(MouseEvent.CLICK,annulerCommande);	
						activerFormulaire();	
					}
								
				}
			}else if((statutID == RAPPROCHEE)||(statutID == LIVREE)){
				desactiverFormulaire();
			}
		}
		
		
		private function desactiverFormulaireOperateur():void{
			cmbContact.enabled = false;
			
			cmbOperateur.enabled = false;			
			
			cmbSociete.enabled = false;
			
			imgSearchContact.visible = false;
			
			imgSearchSociete.visible = false;			
		}
		
		private function desactiverFormulaire():void{
			
			btAjouterLignes.visible = false;
			
			btEnregistrer.visible = false;
			
			btRapprocher.visible = false;
			
			btSuppLignes.visible = false;
			
			imgSearchContact.visible = false;
			
			imgSearchSociete.visible = false;
			
			txtLibelle.enabled = false;
			
			txtLibelle.enabled = false;
			
			txtReferenceCli.enabled = false;
			
			txtReferenceOpe.enabled = false;
			
			dfDateEffet.enabled = false;
			
			dfDateLivraison.enabled = false;
			
			treeCible.enabled = false;
			
			cbNewCible.enabled = false;
			
			cmbContact.enabled = false;
			
			cmbOperateur.enabled = false;
			
			cmbSociete.enabled = false;
			
			cmbTheme.enabled = false;
			
			colComm.editable = false;
			
			nsNbLignes.enabled = false;
		}
		
		private function activerFormulaire():void{
			
			btAjouterLignes.visible = true;
			btEnregistrer.visible = true;
			btRapprocher.visible = true;
			btSuppLignes.visible = true;
			
			/* imgSearchContact.visible = true;
			
			imgSearchSociete.visible = true; */
			
			txtLibelle.enabled = true;
			
			txtLibelle.enabled = true;
			
			txtReferenceCli.enabled = true;
			
			txtReferenceOpe.enabled = true;
			
			dfDateEffet.enabled = true;
			
			dfDateLivraison.enabled = true;
			
			treeCible.enabled = true;
			
			cbNewCible.enabled = true;
			
			cmbTheme.enabled = true;
			
			colComm.editable = true;
			
			nsNbLignes.enabled = true;
		}
		
		private function commandeCree(ev : Event):void{
			//mettre à jour la liste des commandes			
			panier.commandeID = commande.commandeID;			
			panier.sauvegarderAll(NOUVELLE_LIGNE);				
			dispatchEvent(new Event(NouvelleLigne.SAVE_COMPLETE));
		}
		
		private function commandeUpdated(ev : Event):void{
			dispatchEvent(new Event(NouvelleLigne.UPDATE_COMPLETE));
		}
		
		private function commandeStatutUpdated(ev : Event):void{
			lblStatut.text = commande.statut;			
		}
		
		private function commandeStautUpdated(ev : Event):void{
			lblStatut.text = commande.statut;
			configBtAnuller(commande.statutID);
		}
		
		//----- PANIER
		private function initPanier():void{			
			panier.addEventListener(ElementCommande.LISTE_COMPLETE,afficherPanier);	
			panier.addEventListener(ElementCommande.DELETE_COMPLETE,panierDeleted);
			panier.addEventListener(ElementCommande.UPDATE_COMPLETE,panierUpdated);
			panier.addEventListener(ElementCommande.SAVE_COMPLETE,panierSauver);
			panier.addEventListener(ElementCommande.SAVEALL_COMPLETE,panierEntierSauver);  						
		}
		
		
		private function setIndexLigne():int{
			var index : int = 0;					
			
			try{	
				var tab : Array = ConsoviewUtil.extraireColonne((myGridLignes.dataProvider as ArrayCollection),"libelleLigne");			
				extraireNumeroLigne(tab);
				tab.sort();			
				trace('** index = ' + index,'tab[tab.length-1] = ' + tab[tab.length-1]);
				index = tab[tab.length-1] + 1;	
				trace('** index = ' + index,'tab[tab.length-1] = ' + tab[tab.length-1]);
			}catch(e : Error){
				trace(e.getStackTrace());
			}			
			return index;			
		}
		
		private function extraireNumeroLigne(arr : Array):void{
			for (var i : int = 0; i < arr.length; i++){
				var tmp : String = String(arr[i]);
				var endIndex : int = tmp.length;
				var tmp2 : String = tmp.substring(9,endIndex);				
				arr[i] = parseInt(tmp2);	
			}
		}
		
		private function rafraichirPanier():void{
			panier.prepareList(commande.commandeID,NOUVELLE_LIGNE);			
		}
		
		private function afficherPanier(ev : Event):void{			
			myGridLignes.dataProvider = panier.liste;	
			indexLigne = setIndexLigne();			
		}
		
		private function panierSauver(ev : Event):void{		
			
			rafraichirPanier();	
			nsNbLignes.value = 1;
		
		}
		
		private function panierEntierSauver(ev : Event):void{				

		}
		
		private function panierUpdated(ev : Event):void{
			rafraichirPanier();
		}
		
		private function panierDeleted(ev : Event):void{

			rafraichirPanier();
		}
							
		private function ajouterLignes(me : MouseEvent):void{			
			if (cmbTheme.selectedIndex != -1){				
				for (var i : int = 0; i < nsNbLignes.value; i++){
					var ligne : ElementCommande = new ElementCommande();											
					ligne.libelleLigne = "ligne n° " + indexLigne.toString();
					indexLigne = indexLigne +1;
					ligne.themeID = cmbTheme.selectedItem.IDTHEME_PRODUIT;
					ligne.flagRapproche = 0;
					ligne.libelletheme = cmbTheme.selectedItem.THEME_LIBELLE;
					
					if (commande.commandeID > 0)	{
						ligne.commandeID = commande.commandeID;
						ligne.addEventListener(ElementCommande.SAVE_COMPLETE,panierSauver);	
						ligne.sauvegarder(commande.typeCommande);
					}else{
						panier.liste.addItem(ligne);
						panier.liste.refresh();	
						myGridLignes.dataProvider = panier.liste;						
					}
				}														 						
			}	
		}	
		
		private function supprimerLignes(me : MouseEvent):void{		
			if (myGridLignes.selectedIndices.length > 0){
				if (commande.commandeID > 0){
					for ( var i : int = 0 ; i < myGridLignes.selectedItems.length ; i++){
						panier.deleteElementCommande(ElementCommande(myGridLignes.selectedItems[i]).elementID);	
					}
				}else{					
					 myGridLignes.selectedItems.forEach(supprimerCetteLigne);
					(myGridLignes.dataProvider as ArrayCollection).refresh();					
				}
									
			}
		}
		
		private function checkEditionGrid(de : DataGridEvent):void{
		 	if (myGridLignes.dataProvider == null){		 		
		 		de.preventDefault();	
		 	} 	 	
		 }
		 
		private function modifierCetteLigne(de : DataGridEvent):void{	
			if (myGridLignes.dataProvider != null){										
				 var myEditor:TextInput = TextInput(de.currentTarget.itemEditorInstance);                
                // Get the new value from the editor.
                var newVal:String = myEditor.text;	                
                // Get the old value.
                var oldVal:String = de.currentTarget.editedItemRenderer.data[de.dataField];                          					
				ElementCommande(myGridLignes.selectedItem).commentaire = newVal;					
				if (commande.commandeID > 0){
					ElementCommande(myGridLignes.selectedItem).update(commande.typeCommande);	
				}														 					
			}else{		
				de.preventDefault();					
			}   
		 }
		 		
		private function supprimerCetteLigne(element : * , index : int , arr : Array):void{
			(myGridLignes.dataProvider as ArrayCollection).removeItemAt((myGridLignes.dataProvider as ArrayCollection).getItemIndex(element));
		}
		
		private function rapprocherLigne(me : MouseEvent):void{
			if(commande.commandeID > 0){
				if ((myGridLignes.selectedIndex != -1)&&(ElementCommande(myGridLignes.selectedItem).flagRapproche == 0)){
					rapprochement = new Rapprochement(commande,ElementCommande(myGridLignes.selectedItem));
					rapprochement.addEventListener(Rapprochement.RAPPROCHER,changerEtatLigne);
					rapprochement.addEventListener(Rapprochement.SORTIR,sortirDuRapprochement);
					desactiverFormulaire();				
					PopUpManager.addPopUp(rapprochement,this,false);					
					PopUpManager.centerPopUp(rapprochement);		
				}	
			}			
		}
		
		public function sortirDuRAprochementPassif(fe : FlexEvent):void{			
			if (rapprochement != null){
				if (rapprochement.isPopUp){
					rapprochement.clean();
					rapprochement.dispatchEvent(new Event(Rapprochement.SORTIR));
					PopUpManager.removePopUp(rapprochement);
					rapprochement = null;
				}
			}
		}
		
		 
		private function sortirDuRapprochement(ev : Event):void{						
			activerFormulaire();
		}
		
		private function changerEtatLigne(ev : Event):void{
			panier.prepareList(commande.commandeID,NOUVELLE_LIGNE);	
			activerFormulaire();
		}
		
		//----------------------
		private function selectionnerContact(ev : Event):void{			
			cmbContact.selectedIndex = ConsoviewUtil.getIndexById(cmbContact.dataProvider as ArrayCollection,"contactID",rechercheContact.contactSelectionneID);
			cmbContact.dispatchEvent(new Event(Event.CHANGE));				
		}
		
		private function selectionnerSociete(ev : Event):void{
			cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,"societeID",rechercheSociete.societeSelectionneeID);
			
			if (currentOperateurId > 0) cmbOperateur.selectedIndex = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"OPERATEURID",currentOperateurId)
			else cmbOperateur.selectedIndex = 0;
				
			cmbSociete.dispatchEvent(new Event(Event.CHANGE));
		}
		
		private function selectSocieteFromContact(ev : Event):void{
			if (cmbContact.selectedIndex != -1){
								
				cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,
											"societeID",Contact(cmbContact.selectedItem).societeID);
																
				cmbSociete.dispatchEvent(new Event(Event.CHANGE));
			}
		}		
			
		private function selectOperateursBySociete(ev : Event):void{
			if (cmbSociete.selectedIndex != -1){
				cmbOperateur.labelField = "NOM";
				cmbOperateur.dataProvider = Societe(cmbSociete.selectedItem).listeOperateurs;				
				cmbOperateur.selectedIndex = 0;//ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"OPERATEURID",Societe(cmbSociete.selectedItem).o)						
				cmbOperateur.dispatchEvent(new Event(Event.CHANGE));
			}			
		}
		
		private function selectThemesByOperateur(ev : Event):void{
			if(cmbOperateur.selectedIndex != -1){
				try{
					getThemeByOperateur(cmbOperateur.selectedItem.OPERATEURID);	
				}catch(e : Error){
					("selectThemesByOperateur warning",e.getStackTrace());
				}
			}
		}
		
		private function filtrerContact(value : Object):Boolean{								
			if ((value.hasOwnProperty("societeID"))
				&&(cmbSociete.selectedItem.hasOwnProperty("societeID"))){
				if (parseInt(value.societeID) == cmbSociete.selectedItem.societeID){					
					return (true);
				} else {
					return (false);
				}	
			}
			return (false);				
		}
				
		
		private function remplirComboContact(ev : Event):void{		 
			cmbContact.labelField = "nom";			
			cmbContact.dataProvider = contacts.liste;		
			cmbContact.selectedIndex = ConsoviewUtil.getIndexById(cmbContact.dataProvider as ArrayCollection,"contactID",commande.contactID);
			societes.prepareList(idGroupe);
		}
		
		
		private function remplirComboSociete(ev : Event):void{
			cmbSociete.labelField = "raisonSociale";
			cmbSociete.dataProvider = societes.liste; 				
					
			//selection de la societe
			cmbSociete.selectedIndex = ConsoviewUtil.getIndexById(cmbSociete.dataProvider as ArrayCollection,"societeID",commande.societeID);			
			//selection de l'opérateur
			callLater(remplirComboOperateur);
		}
		
		private function remplirComboOperateur():void{
			cmbOperateur.labelField = "NOM";			
			cmbOperateur.dataProvider = Societe(cmbSociete.selectedItem).listeOperateurs;						
			(cmbOperateur.dataProvider as ArrayCollection).refresh();
			
			if (commande.operateurID > 0){
				cmbOperateur.selectedIndex = ConsoviewUtil.getIndexById(cmbOperateur.dataProvider as ArrayCollection,"OPERATEURID",currentOperateurId)
				cmbOperateur.dispatchEvent(new Event(Event.CHANGE));
			}
		}
		
		//-------------- REMOTING ------------------------------				
		private function getListeTheme():void
		{	
			if (opTheme != null) opTheme.cancel();
			opTheme  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
																				"getTheme",
																				getListeThemeResultHandler,
																				null);				
			RemoteObjectUtil.callService(opTheme);		
		}
		
		private function fillComboTheme( source : ArrayCollection):ArrayCollection{
			var newCollection : ArrayCollection = new ArrayCollection();
			var len : int = source.length;			
			for (var i:int = 0;i < len; i++){						
				if (!ConsoviewUtil.isIdInArray(int(source[i].IDTHEME_PRODUIT),"IDTHEME_PRODUIT",newCollection.source)){
					var obj : Object = new Object();				
					obj.THEME_LIBELLE = source[i].THEME_LIBELLE;
					obj.IDTHEME_PRODUIT = source[i].IDTHEME_PRODUIT;	
					newCollection.addItem(obj);
				}
			}			
			return newCollection;
		}
		
		private function getListeThemeResultHandler(re :ResultEvent):void{
			cmbTheme.dataProvider = re.result as ArrayCollection;
			cmbTheme.labelField = "THEME_LIBELLE"; 						
		}
		
		private function getThemeByOperateur(idope : int):void{
			if (opTheme != null) opTheme.cancel();
			opTheme  = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
																				"getThemebyOperateur",
																				getListeThemeResultHandler,
																				null);				
			RemoteObjectUtil.callService(opTheme,idope);		
		}		
	}
}