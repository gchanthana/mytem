package univers.inventaire.equipements.lignes.v2.views
{
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import mx.containers.Box;
	import composants.util.ConsoviewUtil;
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.DateFunction;
	import composants.util.ConsoviewFormatter;
	import flash.events.Event;
	import mx.events.FlexEvent;
	import flash.errors.IllegalOperationError;
	import mx.events.CloseEvent;
	import univers.usages.usage.detail;
	import mx.controls.Alert;
	import flash.display.DisplayObject;
	
	[Bindable]
	public class BaseView extends Box implements IGestionTechniqueLignesApp
	{
		public function BaseView()
		{	
			super();
		}
		
		protected  var _hasChanged : Boolean = false;
		
		
		private var _gestionTechnique : GestionTechniqueLignesApp;
		public function get gestionTechnique():GestionTechniqueLignesApp
		{	
			return _gestionTechnique;
		}
		public function set gestionTechnique(gestion:GestionTechniqueLignesApp):void
		{
			_gestionTechnique = gestion;
		}
		
		public function get displayObject():DisplayObject{
			return this
		}
		
		private var _modeEcriture : Boolean
		public function get modeEcriture():Boolean{
			return _modeEcriture;
		}		
		public function set modeEcriture(mode:Boolean):void{
			_modeEcriture = mode;
		}
		
		public function onPerimetreChange():void{};
		public function getData():void{
			gestionTechnique.getEtatLigne();
			gestionTechnique.getInfosLigne();
			gestionTechnique.gestionAffectationStrategy.getInfosAffectation();
		}
		
		protected function getIndexById(dataProvider : Object,colName : String,id : Number):Number{
			 return ConsoviewUtil.getIndexById(dataProvider as ArrayCollection,colName,id);
		}	
		
		override protected function commitProperties():void{
			super.commitProperties();
			addEventListener(FlexEvent.HIDE,hideHandler);
		}
		
		//Cette methode doit etre implementée
		protected function enregistrer():void{
			_hasChanged = false;
			throw new IllegalOperationError("abstract method");
		}
		//======================== FORMATTEURS =================================================
		//LABEL FUNCTION POUR DATAGRIDS		
		protected function formateDates(item : Object, column : DataGridColumn):String{
			if (item != null && item[column.dataField] != null){
				return DateFunction.formatDateAsString(item[column.dataField]);				
			}else return "-";
			
		}
		
		protected function formateEuros(item : Object, column : DataGridColumn):String{			
			if (item != null) return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);	
			else return "-";
		}
		
		
		protected function formateNumber(item : Object, column : DataGridColumn):String{
			if (item != null) return ConsoviewFormatter.formatNumber(Number(item[column.dataField]),2);
			else return "-";
		}
		
		protected function formateLigne(item : Object, column : DataGridColumn = null):String{			
			if (item != null && column != null) return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
			else if (item != null && item.hasOwnProperty("TETE")){
				return ConsoviewFormatter.formatPhoneNumber(item.TETE);
			}else if (item != null && item.hasOwnProperty("SOUS_TETE")){
				return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);
			}else return "-";
		}
		
		protected function formateBoolean(item : Object, column : DataGridColumn):String{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON";
			else return "-";
		}
		//======================== FIN FORMATTEURS =============================================
		
		
		//======================== HANDLERS ====================================================
		
		private function hideHandler(fe : FlexEvent):void{
			/* var closeHandler : Function = function  f(ce : CloseEvent):void{
				if (ce.detail == Alert.YES){
					enregistrer();
				}else{
					_hasChanged = false;
				}
			}
			Alert.yesLabel = "Oui";
			Alert.noLabel = "Non";
			if (_hasChanged){
				fe.stopPropagation();
				Alert.show("Voulez vous enregistrer les modifications","Info",Alert.YES|Alert.NO,this,closeHandler);	
				
			} */
			
		}
		
		//======================== FIN HANDLERS=================================================
		
	}
}