<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction"
			displayname="ActAF001" hint="copier une actualite vers une autre application" output="false">
	
	<!--- 
		copier une actualite vers une autre application
	 --->
	<cffunction name="executeAct" access="remote" returntype="Struct"
				hint="copier une actualite vers une autre application" output="false">
		
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28.cloneActualiteToApplication">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR2" variable="p_liste_actualite" type="in" value="#data.LISTEACTU#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_code_app" type="in" value="#data.CODEAPP#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out">
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			<cfreturn CreateReponse("AF001", 1, qResult)>
			
		<cfcatch type="Any">
			<cfreturn CreateReponse("AF001", 0, ThrowCfCatchError("AF001",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>