package univers.facturation.suivi.factures.services
{
    import composants.util.ConsoviewAlert;
    
    import flash.events.EventDispatcher;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.events.ResultEvent;
    
    import univers.facturation.suivi.factures.event.FactureEvent;

    public class FactureHandlers extends EventDispatcher
    {
        private var myDatas:FactureDatas;

        public function FactureHandlers(instanceOfDatas:FactureDatas)
        {
            myDatas = instanceOfDatas;
        }

        //METHODE INTERNAL------------------------------------------------------------------------
        internal function ajouterProduitAMesFavorisResultHandler(re:ResultEvent):void
        {
            if (re.result <= 0)
            {
                ConsoviewAlert.afficherError(FactureEvent.MESSAGE_ERROR_ADD_FAVORIS, FactureEvent.MESSAGE_ERROR_FAVORIS);
            }
        }

        internal function supprimerProduitDeMesFavorisResultHandler(re:ResultEvent):void
        {
            if (re.result <= 0)
            {
                ConsoviewAlert.afficherError(FactureEvent.MESSAGE_ERROR_DELETE_FAVORIS, FactureEvent.MESSAGE_ERROR_FAVORIS);
            }
        }

        internal function fournirListeProduitResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                myDatas.listeProduct.removeAll();
                myDatas.listeProductFavoris.removeAll();
                myDatas.listeProduct = (re.result as ArrayCollection);
                if (myDatas.listeProduct.length > 0)
                {
                    myDatas.listeProductFavoris = getFavorisList(myDatas.listeProduct);
                }
                dispatchEvent(new FactureEvent(FactureEvent.LISTED_PRODUIT_LOADED));
            }
            else
            {
                dispatchEvent(new FactureEvent(FactureEvent.LISTED_PRODUIT_LOADED_ERROR));
            }
        }

        internal function fournirListeProduitFavoriResultHandler(re:ResultEvent):void
        {
            if (re.result)
            {
                myDatas.listeProductFavoris.removeAll();
                myDatas.listeProductFavoris = (re.result as ArrayCollection);
                dispatchEvent(new FactureEvent(FactureEvent.LISTED_PRODUIT_FAVORI_LOADED));
            }
            else
            {
                dispatchEvent(new FactureEvent(FactureEvent.LISTED_PRODUIT_FAVORI_LOADED_ERROR));
            }
        }

        private function getFavorisList(list:ArrayCollection):ArrayCollection
        {
            var favList:ArrayCollection = new ArrayCollection();
            for (var i:int = 0; i < list.length; i++)
            {
                var item:Object = list.getItemAt(i);
                if (item.hasOwnProperty("FAVORI") && item.FAVORI == 1)
                {
                    favList.addItem(item);
                }
            }
            return favList;
        }
        //METHODE INTERNAL------------------------------------------------------------------------
    }
}