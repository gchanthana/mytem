package entity.vo
{
    import entity.ModelLocator;
    import entity.enum.EBoolean;
    import mx.messaging.errors.NoChannelAvailableError;

    [Bindable]
    public class CollecteClientVO extends ValueObject
    {
        private var _operateur:OperateurVO;
        private var _idRacine:Number = 0;
        private var _idcollectClient:Number = 0;
        private var _libelleCollect:String = "";
        private var _idcollectTemplate:Number = 0;
        private var _template:CollecteTypeVO = new CollecteTypeVO();
        private var _isActif:EBoolean = EBoolean.FALSE;
        private var _libelleRoic:String = "";
        private var _roic:Number = 0;
        private var _identifiant:String = "";
        private var _mdp:String = "";
        private var _plageImport:Date;
        private var _firstImportDate:Date;
        private var _lastImportDate:Date;
        private var _libelleClient:String = "";
        private var _nbTrackedCf:Number = 0;
        private var _retroactivite:EBoolean = EBoolean.FALSE;
        private var _activationAlerte:EBoolean;
        private var _delaiAlerte:Number = 0;
        private var _mailAlerte:String = "";
        private var _nbImport:Number = 0;
        private var _dateEmissionFirstImport:Date;
        private var _datedebFirstImport:Date;
        private var _datefinFirstImport:Date;;
        private var _dateEmissionLastImport:Date;
        private var _datedebLastImport:Date;
        private var _datefinLastImport:Date;
        private var _userCreate:Number = 0;
        private var _dateCreate:Date;
        private var _userModif:Number = 0;
        private var _dateModif:Date;

        public function CollecteClientVO()
        {
            super();
        }

        public override function deserialize(object:Object):IValueObject
        {
            var o:Object = object;
            o.hasOwnProperty("IDCOLLECT_CLIENT") ? this.idcollectClient = o.IDCOLLECT_CLIENT : null;
            o.hasOwnProperty("LIBELLE_COLLECT") ? this.libelleCollect = o.LIBELLE_COLLECT : null;
            o.hasOwnProperty("IDCOLLECT_TEMPLATE") ? this.idcollectTemplate = new Number(o.IDCOLLECT_TEMPLATE) : null;
            o.hasOwnProperty("IS_ACTIF") ? this.isActif = EBoolean.getEntityFor(new Number(o.IS_ACTIF)) : null;
            o.hasOwnProperty("ROIC") ? this.libelleRoic = o.ROIC as String : null;
            //o.hasOwnProperty("ROIC") ? this.roic = o.ROIC : null;
            o.hasOwnProperty("IDENTIFIANT") ? this.identifiant = o.IDENTIFIANT as String : null;
            o.hasOwnProperty("MDP") ? this.mdp = o.MDP as String : null;
            o.hasOwnProperty("PLAGE_IMPORT") ? this.plageImport = o.PLAGE_IMPORT as Date : null;
            o.hasOwnProperty("FIRST_IMPORT_DATE") ? this.firstImportDate = o.FIRST_IMPORT_DATE as Date : null;
            o.hasOwnProperty("LAST_IMPORT_DATE") ? this.lastImportDate = o.LAST_IMPORT_DATE as Date : null;
            o.hasOwnProperty("ID_RACINE") ? this.idRacine = new Number(o.ID_RACINE) : null;
            o.hasOwnProperty("LIBELLE_CLIENT") ? this.libelleClient = o.LIBELLE_CLIENT as String : null;
            o.hasOwnProperty("NB_TRACKED_CF") ? this.nbTrackedCf = new Number(o.NB_TRACKED_CF) : null;
            o.hasOwnProperty("RETROACTIVITE") ? this.retroactivite = EBoolean.getEntityFor(new Number(o.RETROACTIVITE)) : null;
            o.hasOwnProperty("ACTIVATION_ALERTE") ? this.activationAlerte = EBoolean.getEntityFor(new Number(o.ACTIVATION_ALERTE)) : null;
            o.hasOwnProperty("DELAI_ALERTE") ? this.delaiAlerte = new Number(o.DELAI_ALERTE) : null;
            o.hasOwnProperty("MAIL_ALERTE") ? this.mailAlerte = o.MAIL_ALERTE as String : null;
            o.hasOwnProperty("NB_IMPORT") ? this.nbImport = new Number(o.NB_IMPORT) : null;
            o.hasOwnProperty("DATE_EMISSION_FIRST_IMPORT") ? this.dateEmissionFirstImport = o.DATE_EMISSION_FIRST_IMPORT as Date : null;
            o.hasOwnProperty("DATE_EMISSION_LAST_IMPORT") ? this.dateEmissionLastImport = o.DATE_EMISSION_LAST_IMPORT as Date : null;
            o.hasOwnProperty("DATEDEB_LAST_IMPORT") ? this.datedebLastImport = o.DATEDEB_LAST_IMPORT as Date : null;
            o.hasOwnProperty("DATEFIN_FIRST_IMPORT") ? this.datefinFirstImport = o.DATEFIN_FIRST_IMPORT as Date : null;
            o.hasOwnProperty("DATE_EMISSION_LAST_IMPORT") ? this.dateEmissionLastImport = o.DATE_EMISSION_LAST_IMPORT as Date : null;
            o.hasOwnProperty("DATEDEB_LAST_IMPORT") ? this.datefinLastImport = o.DATEDEB_LAST_IMPORT as Date : null;
            o.hasOwnProperty("DATEFIN_LAST_IMPORT") ? this.datefinLastImport = o.DATEFIN_LAST_IMPORT as Date : null;
            o.hasOwnProperty("USER_CREATE") ? this.userCreate = new Number(o.USER_CREATE) : null;
            o.hasOwnProperty("DATE_CREATE") ? this.dateCreate = o.DATE_CREATE as Date : null;
            o.hasOwnProperty("DATE_MODIF") ? this.dateModif = o.DATE_MODIF as Date : null;
            // recuperation en locale du template correspondant à l'id ; TODO pr optimiser requête récuprant le template ?
            this.template = ModelLocator.getInstance().getTemplateFor(this.idcollectTemplate);
            return this;
        }

        public override function serialize(object:IValueObject):Object
        {
            return null;
        }

        public override function toString():String
        {
            return " CollecteClientVO " + libelleCollect + "\n" + "id :" + idcollectClient.toString();
        }

        public override function toXML():XML
        {
            var xml:XML = voToXml(this, "CollecteClientVO");
            return xml;
        }

        public override function decode(object:Object):IValueObject
        {
            var vo:CollecteClientVO = new CollecteClientVO();
            // TODO
            return vo;
        }

        public function get operateur():OperateurVO
        {
            return _operateur;
        }

        public function set operateur(value:OperateurVO):void
        {
            _operateur = value;
        }

        public function get idcollectTemplate():Number
        {
            return _idcollectTemplate;
        }

        /**
         * id du template de référence
         */
        public function set idcollectTemplate(value:Number):void
        {
            _idcollectTemplate = value;
        }

        public function get isActif():EBoolean
        {
            return _isActif;
        }

        /**
         * indique si la collecte est <b>active</b> ou non
         */
        public function set isActif(value:EBoolean):void
        {
            _isActif = value;
        }

        public function get libelleRoic():String
        {
            return _libelleRoic;
        }

        /**
         * libéllé de la Ref Operateur Id Client <b>ROIC</b>
         */
        public function set libelleRoic(value:String):void
        {
            _libelleRoic = value;
        }

        public function get roic():Number
        {
            return _roic;
        }

        /**
         * Identifiant de la Ref Operateur Id Client <b>ROIC</b>
         */
        public function set roic(value:Number):void
        {
            _roic = value;
        }

        public function get identifiant():String
        {
            return _identifiant;
        }

        /**
         * login de connexion à l'extranet
         * si mode de récup == PULL-%
         *
         */
        public function set identifiant(value:String):void
        {
            _identifiant = value;
        }

        public function get mdp():String
        {
            return _mdp;
        }

        /**
         * mot de passe de connexion à l'extranet
         * si mode de récup == PULL-%
         */
        public function set mdp(value:String):void
        {
            _mdp = value;
        }

        public function get plageImport():Date
        {
            return _plageImport;
        }

        /**
         * Date d'ouverture de la plage d'import
         */
        public function set plageImport(value:Date):void
        {
            _plageImport = value;
        }

        public function get firstImportDate():Date
        {
            return _firstImportDate;
        }

        /**
         *  Date de la 1ere facture
         * (la plus ancienne dans l’import
         *  en termes de date d’émission)
         */
        public function set firstImportDate(value:Date):void
        {
            _firstImportDate = value;
        }

        public function get lastImportDate():Date
        {
            return _lastImportDate;
        }

        /**
         *  Date de la dernière facture
         */
        public function set lastImportDate(value:Date):void
        {
            _lastImportDate = value;
        }

        public function get idRacine():Number
        {
            return _idRacine;
        }

        /**
         *  idRacine du périmètre à qui appartient la collecte
         */
        public function set idRacine(value:Number):void
        {
            _idRacine = value;
        }

        public function get libelleClient():String
        {
            return _libelleClient;
        }

        public function set libelleClient(value:String):void
        {
            _libelleClient = value;
        }

        public function get nbTrackedCf():Number
        {
            return _nbTrackedCf;
        }

        /**
         *  Nombre de comptes de facturation
         *  suivis
         */
        public function set nbTrackedCf(value:Number):void
        {
            _nbTrackedCf = value;
        }

        public function get retroactivite():EBoolean
        {
            return _retroactivite;
        }

        /**
         *  Demande de rétroactivité
         */
        public function set retroactivite(value:EBoolean):void
        {
            _retroactivite = value;
        }

        public function get activationAlerte():EBoolean
        {
            return _activationAlerte;
        }

        /**
         *  Activation alerte ou non
         */
        public function set activationAlerte(value:EBoolean):void
        {
            _activationAlerte = value;
        }

        public function get delaiAlerte():Number
        {
            return _delaiAlerte;
        }

        /**
         *  Delai de l'alerte en jours ouvrés
         */
        public function set delaiAlerte(value:Number):void
        {
            _delaiAlerte = value;
        }

        public function get mailAlerte():String
        {
            return _mailAlerte;
        }

        /**
         * Destinataire de l'alerte
         */
        public function set mailAlerte(value:String):void
        {
            _mailAlerte = value;
        }

        public function get nbImport():Number
        {
            return _nbImport;
        }

        /**
         * Nombre d'imports effectués
         */
        public function set nbImport(value:Number):void
        {
            _nbImport = value;
        }

        public function get dateEmissionFirstImport():Date
        {
            return _dateEmissionFirstImport;
        }

        /**
         * Date d'émission du premier import
         */
        public function set dateEmissionFirstImport(value:Date):void
        {
            _dateEmissionFirstImport = value;
        }

        public function get datedebFirstImport():Date
        {
            return _datedebFirstImport;
        }

        /**
         * Date de début du premier import
         */
        public function set datedebFirstImport(value:Date):void
        {
            _datedebFirstImport = value;
        }

        public function get datefinFirstImport():Date
        {
            return _datefinFirstImport;
        }

        /**
         * Date de fin du premier import
         */
        public function set datefinFirstImport(value:Date):void
        {
            _datefinFirstImport = value;
        }

        public function get dateEmissionLastImport():Date
        {
            return _dateEmissionLastImport;
        }

        /**
         * Date d'émission  du dernier import
         */
        public function set dateEmissionLastImport(value:Date):void
        {
            _dateEmissionLastImport = value;
        }

        public function get datedebLastImport():Date
        {
            return _datedebLastImport;
        }

        /**
         * Date de début du dernier import
         */
        public function set datedebLastImport(value:Date):void
        {
            _datedebLastImport = value;
        }

        public function get datefinLastImport():Date
        {
            return _datefinLastImport;
        }

        /**
         * Date de fin du dernier import
         */
        public function set datefinLastImport(value:Date):void
        {
            _datefinLastImport = value;
        }

        public function get userCreate():Number
        {
            return _userCreate;
        }

        /**
         * l'id de l'utilisateur ayant créé la collecte
         */
        public function set userCreate(value:Number):void
        {
            _userCreate = value;
        }

        public function get dateCreate():Date
        {
            return _dateCreate;
        }

        /**
         * Date de la création de la collecte
         */
        public function set dateCreate(value:Date):void
        {
            _dateCreate = value;
        }

        public function get userModif():Number
        {
            return _userModif;
        }

        /**
         * Id de l'utilisateur ayant modifié la collecte en dernier
         */
        public function set userModif(value:Number):void
        {
            _userModif = value;
        }

        public function get dateModif():Date
        {
            return _dateModif;
        }

        /**
         * Date de la dernière modification de la collecte
         */
        public function set dateModif(value:Date):void
        {
            _dateModif = value;
        }

        public function get idcollectClient():Number
        {
            return _idcollectClient;
        }

        /**
         * Identifiant de la collecte (N° incrémental)
         */
        public function set idcollectClient(value:Number):void
        {
            _idcollectClient = value;
        }

        public function get libelleCollect():String
        {
            return _libelleCollect;
        }

        /**
         * Libéllé de la collecte (N° incrémental)
         */
        public function set libelleCollect(value:String):void
        {
            _libelleCollect = value;
        }

        public function get template():CollecteTypeVO
        {
            return _template;
        }

        /**
         * Template de la collecte (feeding locale depuis la
         * liste chargée à l'init)
         */
        public function set template(value:CollecteTypeVO):void
        {
            _template = value;
        }
    }
}