package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import composants.util.DateFunction;
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.ConsoviewFormatter;
	import mx.controls.Alert;
	
	
	/**
	 * Journal des ressources 
	 * Affiche une synthese des mouvements sur un noeud à une date donnée
	 * */
	public class ConsoHorsInventaire extends ConsoHorsInventaireIHM implements IJournal
	{
		//Reference vers la date de fin sur laquelle on travaille
		private var dateDebut : String;
		
		//Reference vers la date de debut sur laquelle on travaille 
		private var dateFin : String;
				
		//ArrayCollection contenant les lignes du journal
		[Bindable]
		private var elementsDuJournal : ArrayCollection;
		
		//Reference vers la méthode distante qui ramène les lignes du journal
		private var opElementDuJournal : AbstractOperation;
		
		//les information du noeud (inutilisé ici)
		private var _nodeInfos : Object;
		
		
		/**
		 * Constructeur
		 * */		
		public function ConsoHorsInventaire()
		{
			//TODO: implement function
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
			
		/**
		 * Recharge les données du journal
		 * */
		public function refresh():void{
			if((dateDebut != null) && (dateFin != null))
				chargerDonnees();
		}
		
		/**
		 * Affecte les infos sur le noeud au journal
		 * */
		 public function set nodeInfos(infos : Object):void{
		 	_nodeInfos = infos;
		 }
		
				
		/**
		* Fonction qui permet d'afficher la période sur laquelle on travaille, au format jour mois annee 
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		* 
		* */
		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
															
			var formMoisD : String = moisDebut.substr(0,2) 
									+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
									+ moisDebut.substr(6,4);
									
			var formMoisF : String = moisFin.substr(0,2)
								   	+ " " 
								   	+ DateFunction.moisEnLettre(parseInt(moisFin.substr(3,2),10)-1) 
								   	+ " " 
								   	+ moisFin.substr(6,4);

			labelPeriode.text = "Factures émises du " + formMoisD + " au " + formMoisF;					
		}
		
		
		/**
		 * Gere le changement de périmetre
		 * */
		public function onPerimetreChange():void{
			//tente de couper le remoting
			
			//init filtre
			txtFiltre.text = "";
			
			//init periode
			initPeriode();
			setSelectedPeriode(dateDebut,dateFin);	
			
			//init le data provider
			elementsDuJournal.source = null
			elementsDuJournal.refresh();
			
			//données
			chargerDonnees();
			
			
		}
		
		/**
		 * Interrompt les remotings en cours
		 * Méthode non utilisée
		 * */
		public function cancelRemotings():void{
			try{
			 
			}catch(e : Error){
				trace("ok");
			}
		}
		
//--- INITIALISATION ---------------------------------------------------------------------------------------------------------------------------
		
		
		//Initialisation de l'ihm
		//Affecte les écouteurs d'évènements
		//Initialise la date pou laquel on affiche l'inventaire
		//Affiche la date du journal
		//Charge les données 
		private function initIHM(fe : FlexEvent):void{
			//Periode
			initPeriode();				
			myPeriodeSelector.addEventListener("periodeChange",updatePeriode); 				
			setSelectedPeriode(dateDebut,dateFin);							
						
			//Filtre
			txtFiltre.addEventListener(Event.CHANGE,filtrerGrid);
			
			//grid			
			colmontant.labelFunction = formateEuros;
			numligne.labelFunction = formateLigne;
								
			chargerDonnees();		
		}
		
		//init Periode
		private function initPeriode():void{
			var periodeArray : Array = myPeriodeSelector.getTabPeriode();
		    var len : int = periodeArray.length;
		    var month : AMonth;
		    					   
			month = periodeArray[len-2];	
							
			dateDebut = month.getDateDebut();	
			dateFin = month.getDateFin();
		}		
		
		//formate une colonne avec le symbol Euros	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres
		private function formateEuros(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatEuroCurrency(item.MONTANT,2);						
		}
		
		//formate une colonne de numéro de téléphone	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);			
		}
		
		//formate une colonne de date dans un DataGrid	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item.DATE_ACTION);
			return DateFunction.formatDateAsString(ladate);			
		}
		
//--- FIN INITIALISATION -----------------------------------------------------------------------------------------------------------------------

	

//--- PERIODE SELECTOR -------------------------------------------------------------------------------------------------------------------------
		
		//Handler du chagement de periode
		//Met à jour la période pour le journal, affiche la periode et charge les données pour cette date
		private function updatePeriode(pe : PeriodeEvent):void{
			
			dateDebut = pe.moisDeb;
			dateFin = pe.moisFin;
			setSelectedPeriode(dateDebut,dateFin);
			chargerDonnees();	
			
		}
		
//--- FIN PERIODE SELECTOR ---------------------------------------------------------------------------------------------------------------------
		
				
//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------
		
		//Gere le filtre du tableau
		private function filtrerGrid(ev :Event):void{
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();		
		}
		
		//filtre sur les attributs OPNOM, LIBELLE_PRODUIT, SOUS_TETE, MONTANT_FACTURE des elements du Tabelau
		private function filterFunc(value : Object):Boolean{	
			if(value.MONTANT_FACTURE == null) value.MONTANT_FACTURE = "";			
			if ((String(value.OPNOM.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.LIBELLE_PRODUIT.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.SOUS_TETE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				|| (String(value.MONTANT_FACTURE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1))
			{
				return (true);
			} else {
				return (false);
			}	
		}
		
//--- FIN FILTRE -------------------------------------------------------------------------------------------------------------------------------

//--- GRID -------------------------------------------------------------------------------------------------------------------------------------
		
//--- FIN GRID ---------------------------------------------------------------------------------------------------------------------------------

//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		
		//Charge les donnees du Journal
		private function chargerDonnees():void
		{
			 
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
			
			lblError.visible = false;			
			opElementDuJournal = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getConsommationsHorsInventaire",
																				chargerDonneesResultHandler);						
			RemoteObjectUtil.callService(opElementDuJournal,
										idGroupeMaitre,
										dateDebut,
										dateFin);		
			 
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerDonnees' à l'ArrayCollection 'elementsDuJournal' et les affiches
		private function chargerDonneesResultHandler(re :ResultEvent):void{
			elementsDuJournal = re.result as ArrayCollection;	
			if (elementsDuJournal.length > 0){
				txtFiltre.editable = true;
			 
				
			} 
			else {
				txtFiltre.editable = false;
				lblError.visible = true;
			}
			myGrid.dataProvider = elementsDuJournal;
			elementsDuJournal.refresh();
			 
		}
		
//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------


	}
}