package univers.inventaire.lignesetservices.views.popups
{
	import univers.inventaire.lignesetservices.vo.ModeRaccordementVo;
	
	public class SuspendreImpl extends DoActionRaccoImpl
	{
		public function SuspendreImpl()
		{	
			super();
			modeRacco = new ModeRaccordementVo();
			modeRacco.BOOL_DIRECT = 1;
			modeRacco.IDTYPE_RACCORDEMENT = 10;
			modeRacco.LIBELLE_RACCORDEMENT = "Suspendu";
		}
		
	}
}