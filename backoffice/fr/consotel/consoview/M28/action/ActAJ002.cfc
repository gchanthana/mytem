﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true">
	
		<cftry>
			<cfstoredproc procedure="pkg_m28.IUProfil" datasource="ROCOFFRE" >
				<cfprocparam cfsqltype="CF_SQL_VARCHAR"	type="in" value="#data.nom#">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" value="#data.description#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#data.idRacine#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#data.idProfil#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" value="#data.createur#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out" >
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.result = p_retour>
			<cfreturn CreateReponse("AJ002", 1, qResult)>
			
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ002", 0, ThrowCfCatchError("AJ002",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
				
	</cffunction>

</cfcomponent>