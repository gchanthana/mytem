package composants.parametres.perimetres.assignment
{
	import mx.events.FlexEvent;
	import mx.events.CloseEvent;
	import flash.events.Event
	import flash.events.MouseEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.FaultEvent;
	import mx.collections.XMLListCollection;
	import mx.collections.ArrayCollection;
	import mx.events.SliderEvent;
	
	public class PeriodeSelector extends PeriodeSelectorIHM
	{
		private var _lastDate:Date;
		private var _callback:Function;
		
		public function PeriodeSelector()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);	
		}
		
		public function setCallBack(callback:Function):void
		{
			_callback = callback;
			slider.addEventListener(SliderEvent.CHANGE, _callback);
		}
		
		public function getSelectedDate():Date
		{
			var coef:Number = 11 - slider.value;
			var currentDate:Date = new Date();
			_lastDate = new Date(currentDate.getFullYear(), currentDate.getMonth()-coef);
			return _lastDate;
		}
		
		
		private function getSliderLabel(value:String):String{
			var coef:Number = 11 - parseInt(value);
			var currentDate:Date = new Date();
			_lastDate = new Date(currentDate.getFullYear(), currentDate.getMonth()-coef);
			return df.format(_lastDate);
		}
		
		protected function initIHM(event:Event):void
		{
			slider.dataTipFormatFunction = getSliderLabel;
			slider.value = 11;
		}
		
	}
}