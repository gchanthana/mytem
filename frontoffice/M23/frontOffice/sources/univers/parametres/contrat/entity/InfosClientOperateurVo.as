package univers.parametres.contrat.entity
{
	import mx.collections.ArrayCollection;
	
	

	[Bindable]
	public class InfosClientOperateurVo
	{

		public var OPERATEUR_CLIENT_ID:Number = 0;
		public var NOM_OP:String = "";
		public var OPERATEURID:Number = 0;
		public var IDRACINE:Number = 0;
		public var FPC_UNIQUE:Date = null;
		public var MONTANT_MENSUEL_PENALITE:Number = 0;
		public var FRAIS_FIXE_RESILIATION:Number = 0;
		public var BOOL_ACTUEL:Number = 0;
		public var DATE_CREATE:Date = null;
		public var DATE_MODIF:Date = null;
		public var USER_CREATE:Number = 0;
		public var NOM_USER_CREATE:String = "";
		public var NOM_USER_MODIFIED:String = "";
		public var USER_MODIF:Number = 0;
		public var ENGAG_12:Number = 0;
		public var ENGAG_24:Number = 0;
		public var ENGAG_36:Number = 0;
		public var ENGAG_48:Number = 0;
		public var SEUIL_TOLERANCE:Number = 0;
	
   


		public function InfosClientOperateurVo()
		{
		}
		
		public function fill(values:Object):void
		{
			OPERATEUR_CLIENT_ID = values.OPERATEUR_CLIENT_ID;
			NOM_OP = values.NOM_OP;
			OPERATEURID = values.OPERATEURID;
			IDRACINE = values.IDRACINE;
			FPC_UNIQUE = values.FPC_UNIQUE;
			MONTANT_MENSUEL_PENALITE = values.MONTANT_MENSUEL_PENALITE;
			FRAIS_FIXE_RESILIATION = values.FRAIS_FIXE_RESILIATION;
			BOOL_ACTUEL = values.BOOL_ACTUEL;
			DATE_CREATE = values.DATE_CREATE;
			DATE_MODIF = values.DATE_MODIF;
			USER_CREATE = values.USER_CREATE;
			USER_MODIF = values.USER_MODIF;
			NOM_USER_CREATE = values.NOM_USER_CREATE;
			NOM_USER_MODIFIED = values.NOM_USER_MODIFIED
			ENGAG_12 = values.ENGAG_12;
			ENGAG_24 = values.ENGAG_24;
			ENGAG_36 = values.ENGAG_36;
			ENGAG_48 = values.ENGAG_48;
			SEUIL_TOLERANCE = values.SEUIL_TOLERANCE;
		}

	}
}