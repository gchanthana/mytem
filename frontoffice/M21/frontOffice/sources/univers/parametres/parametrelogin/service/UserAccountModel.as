package univers.parametres.parametrelogin.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	
	import univers.parametres.parametrelogin.event.UserAccountEvent;
	import univers.parametres.parametrelogin.vo.UserAccountVo;
	
	
	[Bindable]
	public class UserAccountModel extends EventDispatcher
	{
		private var _userAccountList:ArrayCollection;
		
		public function UserAccountModel()
		{
			userAccountList = new ArrayCollection();
		}
		
		public function updateUserAccountList(values:ArrayCollection):void
		{
			userAccountList.removeAll();
			
			if(values != null)
			{	
				var __cursor:IViewCursor = values.createCursor();
				var __userAccount:UserAccountVo;
				
				while(!__cursor.afterLast)
				{
					__userAccount = new UserAccountVo(
							__cursor.current.APP_LOGINID,
							__cursor.current.IDGROUPE_CLIENT,
							__cursor.current.APPLICATIONID,
							__cursor.current.LOGIN_NOM,
							__cursor.current.LOGIN_PRENOM,
							__cursor.current.LOGIN_EMAIL,
							__cursor.current.LOGIN_PWD);
					
					__userAccount.ADRESSE_POSTAL = 			__cursor.current.ADRESSE_POSTAL;
					__userAccount.BOOL_ACCES_PERIMETRE = 	__cursor.current.BOOL_ACCES_PERIMETRE;
					__userAccount.CODE_LANGUE = 			__cursor.current.CODE_LANGUE;
					__userAccount.CODE_POSTAL = 			__cursor.current.CODE_POSTAL;
					__userAccount.DIRECTION = 				__cursor.current.DIRECTION; 
				 
					__userAccount.GLOBALIZATION = 			__cursor.current.GLOBALIZATION; 
					__userAccount.IDPROFIL_THEME = 			__cursor.current.IDPROFIL_THEME; 
					__userAccount.IDREVENDEUR = 			__cursor.current.IDREVENDEUR; 
					__userAccount.LIBELLE_TYPE_PROFIL = 	__cursor.current.LIBELLE_TYPE_PROFIL; 
					 
					__userAccount.RACINE_APPARTENANCE = 	__cursor.current.RACINE_APPARTENANCE; 
					__userAccount.RESTRICTION_IP = 			__cursor.current.RESTRICTION_IP;
					__userAccount.TELEPHONE = 				__cursor.current.TELEPHONE; 
					__userAccount.TYPE_PROFIL = 			__cursor.current.TYPE_PROFIL; 
					__userAccount.VILLE_POSTAL = 			__cursor.current.VILLE_POSTAL;
					
					__userAccount.LOGIN_STATUS = 			__cursor.current.LOGIN_STATUS;
					__userAccount.FAILED_CONNECT = 			__cursor.current.FAILED_CONNECT;
					
					userAccountList.addItem(__userAccount);
					
					__cursor.moveNext();
				}
			}
		}
		
		public function updateUserAccountState(value:Object):void
		{
			if(value != null )
			{	
				if(value is Array 
					&& (value as Array).length > 1)
				{
					switch((value as Array)[1])
					{
						case  1 :dispatchEvent(new UserAccountEvent(UserAccountEvent.UNLOCK_COMPLETE));break;
						case  2 :dispatchEvent(new UserAccountEvent(UserAccountEvent.LOCK_COMPLETE));break;
						case  0 :dispatchEvent(new UserAccountEvent(UserAccountEvent.DELETE_COMPLETE));break;
						default : break;
					}
				}				
			}
		}
		
		
		/* ------- ACCESSORS -------- */
		
		public function get userAccountList():ArrayCollection
		{
			return _userAccountList;
		}
		
		private function set userAccountList(value:ArrayCollection):void
		{
			_userAccountList = value;
		}
		
	}
}