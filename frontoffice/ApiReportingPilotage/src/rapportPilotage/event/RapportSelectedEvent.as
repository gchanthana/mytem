package rapportPilotage.event
{
	import flash.events.Event;
	
	import rapportPilotage.vo.Rapport;

	public class RapportSelectedEvent extends Event
	{

		public var selectedRapport:Rapport; // un attributs de l'evenement
		
		public static const DATA_TRANSFER_RAPPORT:String = "data_transfer_rapport";
		
		public function RapportSelectedEvent(type:String,selectedRapport:Rapport,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable)
			this.selectedRapport=selectedRapport;
		}
		
		override public function clone():Event
		{
			return new RapportSelectedEvent(type,selectedRapport,bubbles,cancelable);
		}
	}
}