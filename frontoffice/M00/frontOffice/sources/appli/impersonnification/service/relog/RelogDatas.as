package appli.impersonnification.service.relog
{
	import entity.CodeLangue;
	
	import mx.collections.ArrayCollection;
	
	import paginatedatagrid.PaginateDatagrid;

	public class RelogDatas
	{
		public var impersonnification:Boolean; //informe entre autres s'il faut marquer X "as" Y ou pas
		public var codeApp:Number;// le code de l'application
		public var codeLangue:CodeLangue;
		public var firstLoading:Boolean;//true s'il s'agit du premier chargement de l'appli, sinon false. Sert lors de la récupération de la liste de logins
		public var infosSession:Object;//la session retournée par le getSession du back. Sert à mettre à jour CvAccessManager.
		
		public function RelogDatas()
		{
			firstLoading = true;
		}
		

	}
}