<!--- =========================================================================
Name: Theme
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="Theme" hint="This class is configured with a ConcreteStrategy object, maintains a reference to a Strategy object and may define an interface that lets Strategy access its data.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<!--- <cffunction name="init" access="public" output="false" returntype="Theme" displayname="Theme init()" hint="Initialize the Theme object">
		<cfargument name="Libelle" required="false" type="string" default="" displayname="string Libelle" hint="Initial value for the Libelle property." />
		<cfargument name="dataset" required="false" type="query" default="" displayname="query dataset" hint="Initial value for the dataset property." />
		<cfargument name="Compte" required="false" type="string" default="" displayname="string Compte" hint="Initial value for the Compte property." />
		<cfscript>
			variables.instance = structNew();
			setLibelle(arguments.Libelle);
			setDataset(arguments.dataset);
			setCompte(arguments.Compte);
			return this;
		</cfscript>	
	</cffunction> --->
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="init" access="public" returntype="void" output="false" displayname="void init(string, string)" >
		<cfargument name="perimetre" required="false" type="string" default="" displayname="string perimetre" hint="Initial value for the perimetreproperty." />
		<cfargument name="ModeCalcul" required="false" type="string" default="" displayname="string ModeCalcul" hint="Initial value for the ModeCalculproperty." />
		<cfset objStrategy=createObject("component","#perimetre##ModeCalcul#Strategy")>
		<cfset Variables.Strategy=objStrategy>
	</cffunction>
	
	<cffunction name="setStrategy" access="public" returntype="void" output="false" displayname="void setStrategy(string, string)" >
		<cfargument name="perimetre" required="false" type="string" default="" displayname="string perimetre" hint="Initial value for the perimetreproperty." />
		<cfargument name="ModeCalcul" required="false" type="string" default="" displayname="string ModeCalcul" hint="Initial value for the ModeCalculproperty." />
	</cffunction>
	
	<cffunction name="getLibelle" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfargument name="ID" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfreturn Strategy.getLibelle(ID)>
	</cffunction>
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfreturn Strategy.getStrategy()>
	</cffunction>
	
	<cffunction name="getNumero" access="public" returntype="string" output="false" displayname="string getNumero(numeric)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfreturn Strategy.getNumero(ID)>
	</cffunction>
	
	<cffunction name="queryData" access="public" returntype="query" output="false" displayname="query queryData(numeric, numeric, numeric)" >
		<cfargument name="ID" required="false" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="IDCompte" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="numeric" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="numeric" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		<cfreturn Strategy.getData(ID,IDCompte,DateDebut,DateFin)>
	</cffunction>
<!--- =========================================================================
GETTERS
========================================================================== --->
	<cffunction name="getDataset" access="public" output="false" returntype="query" displayName="query getDataset()" hint="Gets the dataset property">
		<cfreturn variables.instance.dataset />
	</cffunction>
	<cffunction name="getCompte" access="public" output="false" returntype="string" displayName="string getCompte()" hint="Gets the Compte property">
		<cfreturn variables.instance.Compte />
	</cffunction>
<!--- =========================================================================
SETTERS
========================================================================== --->
	<cffunction name="setLibelle" access="public" output="false" returntype="void" displayName="void setLibelle(string)" hint="Sets a new value for the Libelle property">
		<cfargument name="newLibelle" type="string" required="yes" />
		<cfset variables.instance.Libelle = arguments.newLibelle />
	</cffunction>
	<cffunction name="setDataset" access="public" output="false" returntype="void" displayName="void setDataset(query)" hint="Sets a new value for the dataset property">
		<cfargument name="newDataset" type="query" required="yes" />
		<cfset variables.instance.dataset = arguments.newDataset />
	</cffunction>
	<cffunction name="setCompte" access="public" output="false" returntype="void" displayName="void setCompte(string)" hint="Sets a new value for the Compte property">
		<cfargument name="newCompte" type="string" required="yes" />
		<cfset variables.instance.Compte = arguments.newCompte />
	</cffunction>
<!--- =========================================================================
INSTANCE METHODS
========================================================================== --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>