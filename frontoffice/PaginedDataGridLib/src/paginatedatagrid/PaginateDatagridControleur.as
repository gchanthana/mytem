package paginatedatagrid
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.containers.HBox;
    import mx.containers.Tile;
    import mx.containers.VBox;
    import mx.controls.CheckBox;
    import mx.controls.ComboBox;
    import mx.controls.DataGrid;
    import mx.controls.Spacer;
    import mx.controls.TextInput;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.events.CollectionEvent;
    import mx.events.FlexEvent;
    import mx.events.ListEvent;
    
    import paginatedatagrid.columns.event.StandardColonneEvent;
    import paginatedatagrid.columns.stantardcolumns.SelectPaginateDatagridColumn;
    import paginatedatagrid.event.PaginateDatagridEvent;
    import paginatedatagrid.pagination.Pagination;
    import paginatedatagrid.pagination.event.PaginationEvent;
    import paginatedatagrid.pagination.vo.ItemIntervalleVO;
    import paginatedatagrid.util.UtilPaginateDatagrid;
    import paginatedatagrid.util.datagrid.RowColorDataGrid;

    /**
     * Lire la documentation de ce composant : testtrack
     * Pour Personnaliser Flex builder 3 pour ce composant :
     * http://www.iteratif.fr/blog/index.php?post/2008/12/12/Personnaliser-les-composants-dans-Flex-builder-3
     * @author Vincent Le Gallic
     * @version : 1.2
     * @dateLasteVersion : 21/05/20010
     * @datecreation : 4/12/2009
     *
     */
    [Event(name="onIntervalleChange", type="paginatedatagrid.event.PaginateDatagridEvent")]
    [Event(name="onItemEdit", type="paginatedatagrid.columns.event.StandardColonneEvent")]
    [Event(name="onItemRemove", type="paginatedatagrid.columns.event.StandardColonneEvent")]
    [Event(name="onItemCheckBoxChange", type="paginatedatagrid.columns.event.StandardColonneEvent")]
    [Event(name="onSelectAllChange", type="paginatedatagrid.event.PaginateDatagridEvent")]
    [Event(name="onItemSelected", type="paginatedatagrid.event.PaginateDatagridEvent")]
    [Event(name="onBTExportPressed", type="paginatedatagrid.event.PaginateDatagridEvent")]
    [Bindable]
    public class PaginateDatagridControleur extends VBox
    {
        //Propriété Custom :
        private var _buttons:Array;
        private var _nbTotalItem:int;
        private var _nbItemParPage:int;
        private var _itemIntervalleSelected:ItemIntervalleVO;
        private var _allowSelection:Boolean = false;
        private var _allowExport:Boolean = false;
        private var _currentIntervalle:ItemIntervalleVO;
        private var _widthBoxBT:int;
        //Propriété du datagrid :
        private var _columns:Array;
        private var _comboBoxActions:ComboBox;
        private var _selectedItem:Object;
        private var _selectedIndex:int;
        private var _selectedItems:ArrayCollection = new ArrayCollection;
        private var _dataprovider:ArrayCollection = new ArrayCollection();
        //Public UI COMPONANT 
        public var dgPaginate:RowColorDataGrid;
        public var paginationBox:Pagination;
        public var txtFiltre:TextInput;
        public var cbAll:CheckBox;
        public var boxButtons:Tile;
        public var boxComboBox:HBox;
        public var utilSpacer:Spacer;

        public function PaginateDatagridControleur()
        {
            addEventListener(StandardColonneEvent.SELECT_CHANGE_ITEM, selectedItemHandler);
            addEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE, intervalleChangeHandler);
        }

        private function selectedItemHandler(evt:StandardColonneEvent):void
        {
        /*var utilPaginateDatagrid : UtilPaginateDatagrid = new UtilPaginateDatagrid();
         utilPaginateDatagrid.onItemChanged(evt.item,selectedItems);*/
        }

        public function creation_complete_handler(evt:FlexEvent):void
        {
            //dgPaginate.selectedItems
            dgPaginate.columns = _columns;
            dgPaginate.addEventListener(Event.CHANGE, dgPaginateHandler);
            dgPaginate.addEventListener(ListEvent.ITEM_CLICK, dgClickHandler);
            updateBoxButtons();
            updateBoxCombo();
        }

        /**
         *
         * dataChangeHandler
         * Si la colonne selection est affichée alors //Vérification que la data est correctement formattée avec SELECTED en propriété
         * Verification que NBRECORD existe
         * appel de utilPaginateDatagrid.formatdata qui met à jour la collection de selectedItems si certains items on leur propriété SELECTED à true
         * Maj du dataprovider du datagrid
         * Initialisation de la pagination avec la nouvelle data affichée
         *
         */
        private function dataChangeHandler(evt:CollectionEvent):void
        {
            if (allowSelection)
            {
                //todo vérifier le type boolean
                if (dataprovider.length > 0)
                {
                    if ((!dataprovider.getItemAt(0).hasOwnProperty('SELECTED')) && allowSelection)
                    {
                        throw new Error('COMPOSANT PAGINATE DATAGRID ERROR : \nSelectPaginateDatagridColumn est utilisé sans la proprité SELECTED \nRetirez la colonne SelectPaginateDatagridColumn ou ajoutez la propriété SELECTED à la data');
                    }
                }
                var utilPaginateDatagrid:UtilPaginateDatagrid = new UtilPaginateDatagrid();
                utilPaginateDatagrid.formatdata(dataprovider, selectedItems);
            }
            if (dataprovider.source[0])
            {
                if (!dataprovider.source[0].hasOwnProperty('NBRECORD'))
                {
                    throw new Error('COMPOSANT PAGINATE DATAGRID ERROR : \nLa propriété NBRECORD est obligatoire sur l objet dataprovider[0] \nNBRECORD est le nombre total d enregistrements correspondant aux critères de recherche');
                }
                nbTotalItem = dataprovider.source[0].NBRECORD;
            }
            paginationBox.initPagination(nbTotalItem, nbItemParPage);
        }

        /**
         *
         * TODO POUR LA V2: VERIFIER SI L INTERVALLE EXISTE DANS LE CACHE : V2 paginate-Dg-Composant
         * La gestion du cache est à implémenter dans intervalleChangeHandler
         * Si dans le cache -> ne pas propager d'event mais afficher le contenue du cache
         *
         */
        private function intervalleChangeHandler(evt:PaginationEvent):void
        {
            refreshPaginateDatagrid();
            currentIntervalle = evt.itemIntervalleVO;
            dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.PAGE_CHANGE, evt.itemIntervalleVO, true));
        }

        /**
         *
         * Event : Avant que la donnée change : On remet le visuelle a l'état initial
         * On décoche la case selectAll
         * On vide la collection d'item selectionné
         * On remet le selected Item a null
         * On selctionne l'intervalle 1
         */
        public function refreshPaginateDatagrid(initCombo:Boolean = false):void
        {
            selectedItems = new ArrayCollection();
            cbAll.selected = false;
            selectedItem = null;
            selectedIndex = -1;
            if (initCombo)
            {
                nbTotalItem = 0;
                paginationBox.myIndex = 0;
                paginationBox.initPagination(nbTotalItem, nbItemParPage);
            }
        }

        /**
         *
         * @param : arrColonne liste des boutons qui serront affichées au dessus du DG
         * */
        public function set buttons(arrColonne:Array):void
        {
            this._buttons = arrColonne;
            updateBoxButtons();
        }

        private function updateBoxButtons():void
        {
            if (boxButtons && buttons)
            {
                boxButtons.visible = false;
                for (var i:int = 0; i < buttons.length; i++)
                {
                    boxButtons.addChild(buttons[i]);
                }
                callLater(placeBoxButton);
            }
            else
            {
                if (utilSpacer)
                {
                    utilSpacer.percentWidth = 100;
                }
            }
        }

        private function placeBoxButton():void
        {
            boxButtons.visible = true;
            utilSpacer.percentWidth = 100;
        }

        private function updateBoxCombo():void
        {
            if (boxComboBox && comboBoxActions)
            {
                boxComboBox.addChildAt(comboBoxActions, 1);
                allowExport = true;
            }
            else
            {
                allowExport = false;
            }
        }

        /**
         * Si dans la liste des colonnes passées en parametres il existe une colonne de type SelectPaginateDatagridColumn
         * alors on autorise la selection ALL avec la CheckBox
         * @param : arrColonne liste des colonnes qui serront affichées dans le dg
         * */
        public function set columns(arrColonne:Array):void
        {
            this._columns = arrColonne;
            for (var i:int = 0; i < _columns.length; i++)
            {
                if (this._columns[i] is SelectPaginateDatagridColumn)
                {
                    _allowSelection = true;
                    break;
                }
            }
        }

        private function dgPaginateHandler(evt:Event = null):void
        {
            if (dgPaginate.selectedItem)
            {
                //Force la mise à jour des données du composant -> on peut binder un autre composant la propriété selectedIndex du paginateDatagrid
                selectedItem = dgPaginate.selectedItem;
                selectedIndex = dgPaginate.selectedIndex;
                dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.ON_ITEM_SELECTED, itemIntervalleSelected));
            }
        }

        private function dgClickHandler(evt:Event):void
        {
            if (allowSelection)
            {
                selectedItem.SELECTED = !selectedItem.SELECTED;
                dataprovider.itemUpdated(selectedItem);
                dispatchEvent(new StandardColonneEvent(StandardColonneEvent.SELECT_CHANGE_ITEM, selectedItem, true));
            }
        }

        protected function filtreHandler(e:Event):void
        {
            dataprovider.filterFunction = filtreGrid;
            dataprovider.refresh();
        }

        /**
         * appelée par filterFunction lorsque txtFiltre change
         * Cette fonction recherche dans toutes les colonnes d'un dg qui ont un dataField != null le texte entré dans txtFiltre
         * @param : item est un objet du dataprovider.
         *
         * */
        private function filtreGrid(item:Object):Boolean
        {
            for (var i:int = 0; i < columns.length; i++)
            {
                if (columns[i].dataField != null)
                {
                    var propriete:String = (columns[i] as DataGridColumn).dataField;
                    if (item[propriete])
                    {
                        if (((item[propriete].toString()).toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public function btExportHandler(event:MouseEvent):void
        {
            dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.ON_BT_EXPORT_PRESSED, itemIntervalleSelected));
        }

        public function cbAllHandler(evt:Event):void
        {
            var utilPaginateDatagrid:UtilPaginateDatagrid = new UtilPaginateDatagrid();
            utilPaginateDatagrid.onCBAllChanged(dataprovider, selectedItems, cbAll.selected);
            dispatchEvent(new PaginateDatagridEvent(PaginateDatagridEvent.ON_SELECT_ALL_CHANGE, itemIntervalleSelected));
        }

        public function get dataprovider():ArrayCollection
        {
            return this._dataprovider;
        }

        public function set dataprovider(valueData:ArrayCollection):void
        {
            this._dataprovider = valueData;
            _dataprovider.addEventListener(CollectionEvent.COLLECTION_CHANGE, dataChangeHandler);
        }

        public function get nbTotalItem():int
        {
            return this._nbTotalItem;
        }

        private function set nbTotalItem(nbTotalItem:int):void
        {
            this._nbTotalItem = nbTotalItem;
        }

        public function get nbItemParPage():int
        {
            return this._nbItemParPage;
        }

        public function set nbItemParPage(nbItemParPage:int):void
        {
            this._nbItemParPage = nbItemParPage;
        }

        public function get itemIntervalleSelected():ItemIntervalleVO
        {
            return this._itemIntervalleSelected;
        }

        public function set itemIntervalleSelected(itemIntervalleSelected:ItemIntervalleVO):void
        {
            this._itemIntervalleSelected = itemIntervalleSelected;
        }

        public function set selectedItems(value:ArrayCollection):void
        {
            _selectedItems = value;
        }

        public function get selectedItems():ArrayCollection
        {
            return _selectedItems;
        }

        public function set currentIntervalle(value:ItemIntervalleVO):void
        {
            this._currentIntervalle = value;
        }

        public function get currentIntervalle():ItemIntervalleVO
        {
            return this._currentIntervalle;
        }

        public function get allowSelection():Boolean
        {
            return _allowSelection;
        }

        internal function set allowSelection(allowSelection:Boolean):void
        {
            this._allowSelection = allowSelection;
        }

        public function get allowExport():Boolean
        {
            return _allowExport;
        }

        public function set allowExport(allowExport:Boolean):void
        {
            this._allowExport = allowExport;
        }

        public function get columns():Array
        {
            return _columns;
        }

        public function get buttons():Array
        {
            return _buttons;
        }

        public function set selectedItem(value:Object):void
        {
            this._selectedItem = value;
            //dgPaginate.selectedItem = _selectedItem;
        }

        public function get selectedItem():Object
        {
            return _selectedItem;
        }

        public function set selectedIndex(value:int):void
        {
            this._selectedIndex = value;
            dgPaginate.selectedIndex = _selectedIndex;
        }

        public function get selectedIndex():int
        {
            return _selectedIndex;
        }

        public function set comboBoxActions(value:ComboBox):void
        {
            this._comboBoxActions = value;
            updateBoxCombo();
        }

        public function get comboBoxActions():ComboBox
        {
            return _comboBoxActions
        }
    }
}