package utils.selector.monoperiodeselector
{
	import event.selector.MonoPeriodeEvent;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.events.FlexEvent;
	
	import utils.SelectorAccueilAbstract;
	
	import vo.selector.Parametre;


	public class MonoPeriodeSelectorImpl extends SelectorAccueilAbstract
	{
		[Bindable]
		public var monoperiode:MonoPeriodeSelector;

		[Bindable]
		public var selectedLibelleMois:String;

		public var btnValider:Button;

		private var _dataChange:Boolean=false;
		private var _canSelect:Boolean=false;

		[Bindable]
		private var _periodes:ArrayCollection;

		public function get periodes():ArrayCollection
		{
			return _periodes;
		}

		[Bindable]
		public function set periodes(value:ArrayCollection):void
		{
			if (value != _periodes)
			{
				_periodes=value;
				_dataChange=true;
				invalidateDisplayList();
				invalidateProperties();
			}
		}

		/**
		 * pour afficher le button valider lors que les données seront disponible
		 */
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);

			if (_dataChange)
			{
				btnValider.visible=true;
				_dataChange=false;
				_canSelect=true;
				

				if (periodes != null && periodes.length > 0)
				{
					monoperiode.setValuePeriode(periodes); /** permert d'initialiser les valeurs du selecteur*/
														
					monoperiode.slider.value = evaluateDefaultValueIndex(periodes,Number(defaultValue));
				}
			}
		}
		
		private function evaluateDefaultValueIndex(values:ArrayCollection,index:Number):Number
		{
			if(!values) return -1;
			
			var indexDefaut:Number =  (values.length -1) + Number(index);
			return (indexDefaut >= 0)?indexDefaut:-1
			
		}
			

		public function MonoPeriodeSelectorImpl(selector:Object=null)
		{
			super(selector);
		}

		protected function monoperiode_creationCompleteHandler(event:FlexEvent):void
		{
			monoperiode.addEventListener(MonoPeriodeEvent.CHANGER_VALUE_HSLIDER_EVENT, getLibelleSelectedPeriode);

			if (isKpi)
			{
				btnValider.styleName="M54buttonDoneKpi";
			}
			else
			{
				btnValider.styleName="M54buttonDoneWidget";
			}
		}

		/**
		 * afficher la periode dans l'IHM
		 * @param e
		 */
		public function getLibelleSelectedPeriode(e:MonoPeriodeEvent):void
		{
			if (_canSelect)
			{
				selectedLibelleMois=monoperiode.getSelectedLibellePeriode().libellePeriode;
			}
		}

		/**
		 * dispatcher un event lors quel'on clique sur le button valider
		 * @param e
		 */
		public function onClick_valider(e:MouseEvent):void
		{
			dispatchEvent(new MonoPeriodeEvent(MonoPeriodeEvent.VALIDER_VALUE_HSLIDER_EVENT)); /** capturé dans le kpiAbstract */
		}

		/**
		 * permet de récupérer les valeurs selectionnées par l'utilisateur
		 * @return
		 */
		override public function getSelectedValue():Array
		{
			var tabValeur:Array=[];
			var is_system:Boolean;
			var valueParam:Array=[];

			if (monoperiode != null)
			{
				var selectedDate:String=monoperiode.getSelectedIdDate().Idmois;
				valueParam.push(selectedDate);
				valueParam.push(selectedDate);
				valueParam.push(selectedLibelleMois);

				for (var i:int=0; i < arrayCleLibelle.length; i++)
				{
					var parametre:Parametre=new Parametre();
					parametre.value=[valueParam[i]];
					parametre.cle=arrayCleLibelle[i].NOM_CLE;
					parametre.libelle=arrayCleLibelle[i].LIBELLE_CLE;
					tabValeur.push(parametre);
				}
			}
			else
			{
				var param:Parametre=new Parametre();
				param.value=[arrayCleLibelle[0].VALUE];
				param.cle=arrayCleLibelle[0].NOM_CLE;
				param.libelle=arrayCleLibelle[0].LIBELLE_CLE;

				tabValeur.push(param);
			}
			return tabValeur;
		}
	}
}
