package univers.parametres.gestionnaire
{
	import mx.resources.ResourceManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.gestionnaire.classemetier.PoolGestionnaire;

	public class EquipementPoolGestionnaire extends EquipementsPoolGestionnaireIHM
	{
		private var poolGestionnaire : PoolGestionnaire;
		private var col_equipement : ArrayCollection = new ArrayCollection();
		private var col_equipement_ofPool : ArrayCollection = new ArrayCollection();
		public function EquipementPoolGestionnaire(poolGestionnaire : PoolGestionnaire)
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
			addEventListener(CloseEvent.CLOSE,fermer);
			this.poolGestionnaire=poolGestionnaire;
		
		}
		private function initIHM(evt : FlexEvent):void
		{
			bt_ajouter_equip.addEventListener(MouseEvent.CLICK,ajouter);
			bt_retirer_equip.addEventListener(MouseEvent.CLICK,desaffecter);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			initData();
			initData_ofpool();
			dg_equip.addEventListener(Event.CHANGE,dg_equip_handler);
			check_sansPool.addEventListener(Event.CHANGE,check_sansPool_handler);
			
			//Filtre sur le grid pool de gestionnaire
			input_filtre_equip_pool.addEventListener(Event.CHANGE,filtregridEquipPool);
			input_filtre_equip.addEventListener(Event.CHANGE,filtregridEquip);
			if(CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			{
				gestion_droit(true);
			}
			else
			{
				gestion_droit();
			}
			mode_lecture.addEventListener(Event.CHANGE,changeMode);
		}
		private function changeMode(evt : Event):void
		{
			if(mode_lecture.selected == true)
			{
				gestion_droit();
			}
			else
			{
				gestion_droit(true);
			}
		}
		private function gestion_droit(modeEcriture : Boolean = false):void
		{
			bt_ajouter_equip.enabled = modeEcriture;
			bt_retirer_equip.enabled = modeEcriture;
		}
		private function check_sansPool_handler(evt :  Event):void{
			if(check_sansPool.selected == true)
			{
				getEquipmentSansPool();	
			}
			else
			{
				initData();
			}
		}
		
		private function filtregridEquipPool(e:Event):void{
			col_equipement_ofPool.filterFunction=filtreGridPool;
			col_equipement_ofPool.refresh();
		}
		private function filtregridEquip(e:Event):void{
			col_equipement.filterFunction=filtreGridPool_all;
			col_equipement.refresh();
		}
		//Filtre : chercher la chaine entrée
		public function filtreGridPool(item:Object):Boolean {
	
			if (((item.LIBELLE_EQ as String).toLowerCase()).search(input_filtre_equip_pool.text.toLowerCase())!=-1 ||((item.TYPE_EQUIPEMENT as String).toLowerCase()).search(input_filtre_equip_pool.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		public function filtreGridPool_all(item:Object):Boolean {
	
			if (((item.LIBELLE_EQ as String).toLowerCase()).search(input_filtre_equip.text.toLowerCase())!=-1 ||((item.TYPE_EQUIPEMENT as String).toLowerCase()).search(input_filtre_equip.text.toLowerCase())!=-1 ) {
				return (true);
			}
			else
			{
				return (false);
			}
		}
		private function desaffecter(evt : MouseEvent):void
		{
			if(dg_equip_of_pool.selectedIndex != -1)
			{
				var idEquip : String = dg_equip_of_pool.selectedItem.IDEQUIPEMENT;
				var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																			"removeEquipementsFromPool",
																			process_removeequipementsfrompool);
				RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,idEquip);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionez_un__quipement'));
			}
		}	
		private function process_removeequipementsfrompool(re : ResultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Equipement_d_saffect_'));
			initData();
			initData_ofpool();
		}
		private function ajouter(evt : MouseEvent):void
		{
			
			if(dg_equip.selectedIndex != -1)
			{
				var idEquip : String = dg_equip.selectedItem.IDEQUIPEMENT;
				var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																			"addequipementtopool",
																			process_addequipementtopool);
				RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,idEquip);
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M21', 'S_lectionez_un__quipement'));
			}
		}
		private function process_addequipementtopool(re : ResultEvent):void
		{
			Alert.show(ResourceManager.getInstance().getString('M21', 'Equipement_ajout__au_pool'));
			initData();
			initData_ofpool();
		}
		
		private function dg_equip_handler(evt : Event):void
		{
			lab_lib.text = dg_equip.selectedItem.LIBELLE_EQ;
			lab_mod.text = dg_equip.selectedItem.LIBELLE_MODELE;
			lab_type.text = dg_equip.selectedItem.TYPE_EQUIPEMENT;
		}
		private function initData(evt : Event = null):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getEquipementNotInPool",
																		process_getEquipement);
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,1,poolGestionnaire.IDPool);
		}
		private function getEquipmentSansPool(evt : Event = null):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getEquipementsansPool",
																		process_getEquipement);
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,1,poolGestionnaire.IDPool);
		}
		private function initData_ofpool():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"getlisteequipementspool",
																		process_getEquipement_ofpool);
			RemoteObjectUtil.callService(op,poolGestionnaire.IDPool,0);
		}
		private function process_getEquipement_ofpool(re : ResultEvent):void
		{
			col_equipement_ofPool = re.result as ArrayCollection;
			dg_equip_of_pool.dataProvider = col_equipement_ofPool;
		}
		private function process_getEquipement(re : ResultEvent):void
		{
			col_equipement = re.result as ArrayCollection;
			dg_equip.dataProvider = col_equipement;
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
	}
}