package gestionparc.ihm.fiches.ficheEntity
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.TabNavigator;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBase;
	import mx.controls.ComboBox;
	import mx.controls.List;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.listClasses.ListBase;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ArrayUtil;
	
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import gestionparc.entity.CommandeVO;
	import gestionparc.entity.ContratVO;
	import gestionparc.entity.EmplacementVO;
	import gestionparc.entity.EquipementVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentGeneralDeviceIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentShowDeviceIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentShowSoftwareIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentSiteFicheEquipementIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.TabModelIHM;
	import gestionparc.ihm.fiches.popup.PopUpNewModeleOfTypeIHM;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.equipement.EquipementServices;
	import gestionparc.services.site.SiteServices;

	public class FicheAjoutEquipementImpl extends TitleWindowBounds
	{

		[Bindable]
		public var tiImei:TextInput;
		[Bindable]
		public var tiNewImei:TextInput;

		[Bindable]
		public var tiNewNumSerie:TextInput;

		[Bindable]
		public var cbMarque:ComboBox;
		[Bindable]
		public var cbModele:ComboBox;
		public var cbCategorie:ComboBox;
		[Bindable]
		public var cbType:ComboBox;
		public var cbNiveau:ComboBox;
		public var ckxPoolDistributeur:CheckBox;
		public var ckxPoolSociete:CheckBox;
		public var taCommentaires:TextArea;
		public var tncContrat:TabModelIHM;
		public var tncSite:ContentSiteFicheEquipementIHM;
		public var tnTerminal:TabNavigator;
		public var tncGeneral:ContentGeneralDeviceIHM;
		public var tncSoft:ContentShowSoftwareIHM;
		public var tncDevice:ContentShowDeviceIHM;
		public var btnValid:Button;
		public var btnCancel:Button;
		public var btNewModele:Button;
		public var formItemIMEI:FormItem;
		public var formItemNumSerie:FormItem;
		
		public const idTypeEqpMobile:Number=70;
		public const idTypeEqpCarteSim:Number=71;
		
		private var _init:Boolean = true;

		[Bindable]
		public var myServiceTerminal:EquipementServices=new EquipementServices();
		public var myServicesCache:CacheServices=CacheServices.getInstance();//=new CacheServices();
		private var modelAdded:Object=null;
		private var myServicesSite:SiteServices=new SiteServices();

		public function FicheAjoutEquipementImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null && item[column.dataField] != "")
			{
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
			{
				return "";
			}
		}

		protected function formateColonnneEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				return ConsoviewFormatter.formatEuroCurrency(item[column.dataField], 2);
			}
			else
			{
				return "";
			}
		}

		protected function formaterColonneBoolean(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				var value:Number=Number(item[column.dataField]);

				if (value == 1 || value == true)
				{
					return ResourceManager.getInstance().getString('M111', 'Oui')
				}
				else
				{
					return ResourceManager.getInstance().getString('M111', 'Non')
				}
			}
			else
			{
				return "";
			}
		}

		private function init(event:FlexEvent):void
		{
			_init = true;
			initListener();
			initDisplay();
		}

		private function initListener():void
		{
			// listeners lié aux boutons de FicheDetailleeNouvelleLigneIHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btnCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btnValid.addEventListener(MouseEvent.CLICK, validClickHandler);

			// listeners liés au tabNavigator
			tncContrat.addEventListener(gestionparcEvent.AJOUTER_ELT_NAV, onAjouterContratHandler);
			tncContrat.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onEditerContratHandler);
			tncContrat.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV, contratDeleteHandler);

			tncSite.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, onInfosFicheSiteUploadedHandler);

			// listeners liés au modèle
			btNewModele.addEventListener(MouseEvent.CLICK, clickNewModeleHandler);
			cbModele.addEventListener(ListEvent.CHANGE, onChangeModeleHandler);

			// listeners liés aux services
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_TYPE_LOADED, listeTypeLoadedHandler);
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_MODELES_LOADED, modeleLoaded);
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.LISTE_MARQUES_LOADED, listeMarquesLoadedHandler);
			
		}
		
		private function initCombos():void
		{
			if(_init)
			{
				myServiceTerminal.getListeType(EquipementVO.CATEGORIE_TERMINAUX);
				reset();
			}
			
		}

		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btnValid.enabled=false;

			cbMarque.visible=true;
			cbModele.visible=true;
			btNewModele.visible=true;
			this.height=this.height - 50;

			tncSite.cbSite.dataProvider=CacheDatas.siteLivraison;
			tncSite.myServicesCache=myServicesCache;
			
			cbCategorie.selectedIndex=0;
			//init terminaux mobile
			initCombos();
		}

		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validClickHandler(event:MouseEvent):void
		{
			var isOk:Boolean=validateHandler();

			if (isOk && cbModele.selectedIndex > -1)
			{
				if(ckxPoolSociete.selected)
				{
					ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'L_operation_s_effectuera_sur_tous_les_pools__'),
						ResourceManager.getInstance().getString('M111', 'Confirmation'),
						confirmationAddNewEqptHandler);
				}
				else
				{
					myServiceTerminal.setInfosFicheTerminal(createObjToServiceSet());
					myServiceTerminal.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED, endUpdateEqpt);
				}
			}
			else
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Veuillez_renseigner_les_champs_obligatoires'), 'Alerte', null);
			}
		}
		
		protected function confirmationAddNewEqptHandler(ce : CloseEvent):void{
			if(ce.detail == Alert.OK){
				myServiceTerminal.setInfosFicheTerminal(createObjToServiceSet());
				myServiceTerminal.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED, endUpdateEqpt);
			}
		}

		private function createObjToServiceSet():Object
		{
			var obj:Object=new Object();
			obj.isNotSameImei=0;
			obj.isNotSameNumSerie=0;
			obj.ID=0

			setInfoNewEquipementVO()
			obj.myTerminal=myServiceTerminal.myDatas.terminal;
			obj.insertPoolDistrib=ckxPoolDistributeur.selected ? 1 : 0;
			obj.insertPoolSociete=ckxPoolSociete.selected ? 1 : 0;
			return obj;
		}

		private function ajouterContratToEqp(idEqp:int):void
		{
			myServiceTerminal.myDatas.addEventListener(gestionparcEvent.INFOS_CONTRAT_SAVED, ajouterContratToEqpHandler);
			myServiceTerminal.setContratsToEqp(idEqp);
		}

		private function ajouterContratToEqpHandler(event:gestionparcEvent):void
		{
			tncContrat.dataDatagrid=myServiceTerminal.myDatas.terminal.listeContrat;
			showMsgEffectue()
		}

		private function endUpdateEqpt(evt:gestionparcEvent):void
		{
			if (myServiceTerminal.myDatas.terminal.listeContrat != null && myServiceTerminal.myDatas.terminal.listeContrat.length > 0)
				ajouterContratToEqp(parseInt(evt.obj.toString()));
			else
				showMsgEffectue()
		}

		private function showMsgEffectue():void
		{
			SpecificationVO.getInstance().refreshData();
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_TERM_UPLOADED));
		}

		private function setInfoNewEquipementVO():void
		{
			myServiceTerminal.myDatas.terminal.numIemiEquipement=tiNewImei.text;
			myServiceTerminal.myDatas.terminal.numSerieEquipement=tiNewNumSerie.text;

			myServiceTerminal.myDatas.terminal.commentaires=taCommentaires.text;

			if (cbNiveau.selectedIndex > -1)
				myServiceTerminal.myDatas.terminal.niveau=cbNiveau.selectedItem.ID;
			else
				myServiceTerminal.myDatas.terminal.niveau=1;

			myServiceTerminal.myDatas.terminal.commande=new CommandeVO();

			if (tncSite.cbSite.selectedItem)
			{
				myServiceTerminal.myDatas.terminal.idSite=(tncSite.cbSite.selectedItem as SiteVO).idSite;
			}
			else
			{
				myServiceTerminal.myDatas.terminal.idSite=-1; // pas de site
			}

			if (tncSite.cbEmplacement.selectedItem)
			{
				myServiceTerminal.myDatas.terminal.idEmplacement=(tncSite.cbEmplacement.selectedItem as EmplacementVO).idEmplcament;
			}
			else
			{
				myServiceTerminal.myDatas.terminal.idEmplacement=-1; // pas d'emplacement
			}

			myServiceTerminal.myDatas.terminal.typeEquipement=cbType.selectedItem.TYPE_EQUIPEMENT;
			myServiceTerminal.myDatas.terminal.idTypeEquipement=cbType.selectedItem.IDTYPE_EQUIPEMENT;
			myServiceTerminal.myDatas.terminal.libEqptFournis=cbModele.selectedItem ? cbModele.selectedItem.LIBELLE_EQ : "";
			myServiceTerminal.myDatas.terminal.idEqptFournis=cbModele.selectedItem ? cbModele.selectedItem.IDEQUIPEMENT_FOURNISSEUR : 0;
			myServiceTerminal.myDatas.terminal.idRevendeur=cbModele.selectedItem ? cbModele.selectedItem.IDREVENDEUR : 0;
			myServiceTerminal.myDatas.terminal.idFabricant=cbMarque.selectedItem.IDFOURNISSEUR;
		}

		private function clickNewModeleHandler(event:MouseEvent):void
		{
			if (cbCategorie.selectedIndex > -1 && cbType.selectedIndex > -1)
			{
				var popUpAddModele:PopUpNewModeleOfTypeIHM=new PopUpNewModeleOfTypeIHM();
				popUpAddModele.objCategorie=cbCategorie.selectedItem;
				popUpAddModele.objType=cbType.selectedItem;
				popUpAddModele.addEventListener(gestionparcEvent.NEW_MODELE_ADDED, newModeleAddedHandler);
				PopUpManager.addPopUp(popUpAddModele, this, true);
				PopUpManager.centerPopUp(popUpAddModele);
			}
		}

		private function newModeleAddedHandler(evt:gestionparcEvent):void
		{
			modelAdded=evt.obj;
			getListeMarque();
		}

		private function choiceModeleDidHandler(evt:gestionparcEvent):void
		{
			myServiceTerminal.myDatas.terminal.nomFabricant=evt.obj.marque;
			myServiceTerminal.myDatas.terminal.idFabricant=evt.obj.idMarque;
		
			myServiceTerminal.myDatas.terminal.idEqptFournis=evt.obj.idModele;
			myServiceTerminal.myDatas.terminal.libEqptFournis=evt.obj.modele;
			
			myServiceTerminal.myDatas.terminal.idTypeEquipement=evt.obj.idType;
		}

		private function onAjouterContratHandler(evt:gestionparcEvent):void
		{
			var ficheAddContratService:FicheContratServiceIHM=new FicheContratServiceIHM();

			ficheAddContratService.isAddContrat=true;
			ficheAddContratService.myServiceTerminal=myServiceTerminal;
			ficheAddContratService.myServiceCache=myServicesCache;
			ficheAddContratService.myContrat=new ContratVO();

			ficheAddContratService.addEventListener(gestionparcEvent.UPDATE_LISTE_CONTRAT, updateAddListeContratHandler);

			PopUpManager.addPopUp(ficheAddContratService, this, true);
			PopUpManager.centerPopUp(ficheAddContratService);
		}

		private function onEditerContratHandler(evt:gestionparcEvent):void
		{
			var ficheEditContratService:FicheContratServiceIHM=new FicheContratServiceIHM();

			ficheEditContratService.isAddContrat=false;
			ficheEditContratService.myServiceTerminal=myServiceTerminal;
			ficheEditContratService.myServiceCache=myServicesCache;

			// seléction du bon contrat parmis la liste des contrats de cet equipement
			var lenListeContrat:int=myServiceTerminal.myDatas.terminal.listeContrat.length;
			for (var i:int=0; i < lenListeContrat; i++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeContrat[i] as ContratVO) == (tncContrat.ligneDgSelected as ContratVO))
				{
					ficheEditContratService.myContrat=(myServiceTerminal.myDatas.terminal.listeContrat[i] as ContratVO);
					break;
				}
			}

			ficheEditContratService.addEventListener(gestionparcEvent.UPDATE_LISTE_CONTRAT, updateModifListeContratHandler);

			PopUpManager.addPopUp(ficheEditContratService, this, true);
			PopUpManager.centerPopUp(ficheEditContratService);
		}

		private function updateAddListeContratHandler(evt:gestionparcEvent):void
		{
			if (myServiceTerminal.myDatas.terminal.listeContrat == null)
				myServiceTerminal.myDatas.terminal.listeContrat=new ArrayCollection();

			myServiceTerminal.myDatas.terminal.listeContrat.addItem((evt.obj as ContratVO));
			tncContrat.dataDatagrid=myServiceTerminal.myDatas.terminal.listeContrat;
			//activeValidButton(null);
		}

		private function updateModifListeContratHandler(evt:gestionparcEvent):void
		{
			/** seléction du bon contrat parmis la liste des contrats de cet equipement et suppression */
			var lenListeContrat:int=myServiceTerminal.myDatas.terminal.listeContrat.length;
			for (var j:int=0; j < lenListeContrat; j++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeContrat[j] as ContratVO).idContrat == (evt.obj as ContratVO).idContrat)
				{
					myServiceTerminal.myDatas.terminal.listeContrat.removeItemAt(j);
					break;
				}
			}
			/** ajout du nouveau contrat (avec les mise a jour)*/
			myServiceTerminal.myDatas.terminal.listeContrat.addItem((evt.obj as ContratVO));
			//activeValidButton(null);
		}

		private function contratDeleteHandler(evt:gestionparcEvent):void
		{
			/** seléction du bon incident parmis la liste des incidents de cet equipement et suppression */
			
			var lenListeContrat:int=myServiceTerminal.myDatas.terminal.listeContrat.length;
			for (var j:int=0; j < lenListeContrat; j++)
			{
				if ((myServiceTerminal.myDatas.terminal.listeContrat[j] as ContratVO) == (evt.obj as ContratVO))
				{
					//myServiceTerminal.deleteContrat((myServiceTerminal.myDatas.terminal.listeContrat[j] as ContratVO));
					myServiceTerminal.myDatas.terminal.listeContrat.removeItemAt(j);
					break;
				}
			}
			//activeValidButton(null);
		}

		private function onInfosFicheSiteUploadedHandler(evt:gestionparcEvent):void
		{
			myServicesCache.getSiteLivraison();
			myServicesCache.myDatas.addEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
		}

		private function onListeSiteLoadedhandler(evt:gestionparcEvent):void
		{
			myServicesCache.myDatas.removeEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
			tncSite.cbSite.dataProvider=CacheDatas.siteLivraison;
			tncSite.txtAdresse.text="";
			tncSite.lbCodePostal.text="";
			tncSite.lbVille.text="";
			tncSite.lbPays.text="";
			tncSite.txtBatiment.text="";
			tncSite.txtSalle.text="";
			tncSite.txtEtage.text="";
			tncSite.txtCouloir.text="";
			tncSite.txtArmoire.text="";
			tncSite.cbEmplacement.selectedIndex=-1;
		}

		private function onInfosEmplaSiteLoadedHandler(evt:gestionparcEvent):void
		{
			myServicesSite.myDatas.removeEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedHandler);
			tncSite.cbEmplacement.dataProvider=new ArrayCollection();
			tncSite.cbEmplacement.dataProvider=myServicesSite.myDatas.listEmplacement;

			tncSite.txtAdresse.text="";
			tncSite.lbCodePostal.text="";
			tncSite.lbVille.text="";
			tncSite.lbPays.text="";
			tncSite.txtBatiment.text="";
			tncSite.txtSalle.text="";
			tncSite.txtEtage.text="";
			tncSite.txtCouloir.text="";
			tncSite.txtArmoire.text="";
			tncSite.cbEmplacement.selectedIndex=-1;
		}

		protected function cbCategorie_changeHandler(le:ListEvent):void
		{
			if (cbCategorie.selectedIndex > -1)
			{
				myServiceTerminal.getListeType(cbCategorie.selectedItem.IDCATEGORIE_EQUIPEMENT);
				reset();
			}
		}

		private function reset():void
		{
			tiNewImei.text="";
			tiNewNumSerie.text="";
			btnValid.enabled=false;
			tiNewImei.setStyle('borderColor', '#cccccc');
			tiNewNumSerie.setStyle('borderColor', '#cccccc');
		}

		private function onChangeModeleHandler(le:ListEvent):void
		{
			if (cbModele.selectedItem)
			{
			}
		}

		private function listeMarquesLoadedHandler(evt:gestionparcEvent):void
		{
			cbMarque.dataProvider=myServiceTerminal.myDatas.listeMarque;
			cbMarque.dropdown.dataProvider=myServiceTerminal.myDatas.listeMarque;

			if (cbMarque.dataProvider.length == 1)
			{
				cbMarque.selectedIndex=0;
				cbMarque_changeHandler(null);
			}
			else if (modelAdded)
			{
				for each (var o:Object in cbMarque.dataProvider)
				{
					if (o.IDFOURNISSEUR == modelAdded.idMarque)
					{
						cbMarque.selectedItem=o;
						break;
					}
				}
				if (cbMarque.selectedIndex > -1)
					refreshListe();
			}
		}

		protected function cbMarque_changeHandler(le:ListEvent):void
		{
			refreshListe();
		}

		private function listeTypeLoadedHandler(evt:gestionparcEvent):void
		{
			cbType.dataProvider=myServiceTerminal.myDatas.listeType;
			cbType.dropdown.dataProvider=myServiceTerminal.myDatas.listeType;
			
			if (cbType.dataProvider.length == 1)
			{
				cbType.selectedIndex=0;
				cbType_changeHandler(null);
			}
			else if(_init)
			{
				cbType.selectedIndex = ConsoviewUtil.getIndexById(myServiceTerminal.myDatas.listeType,"IDTYPE_EQUIPEMENT",EquipementVO.TYPE_MOBILE);
				cbType_changeHandler(null);
				_init = false;
			}
		}

		protected function cbType_changeHandler(evt:ListEvent):void
		{
			if (cbType.selectedIndex > -1)
			{
				refreshListe();
				tiNewImei.enabled=true;
				tiNewImei.maxChars=(cbType.selectedItem.IDTYPE_EQUIPEMENT == idTypeEqpMobile) ? 15 : 50;
				btNewModele.enabled=true;
				reset();
				validateHandler();
			}
			else
			{
				tiNewImei.enabled=false;
				btNewModele.enabled=false;
			}
		}


		public function validateHandler():Boolean
		{
			btnValid.enabled=true;

			if (cbType.selectedItem.IDTYPE_EQUIPEMENT == idTypeEqpMobile ||  cbType.selectedItem.IDTYPE_EQUIPEMENT == idTypeEqpCarteSim)
			{
				tiNewNumSerie.setStyle('borderColor', '#cccccc');
				
				if( cbType.selectedItem.IDTYPE_EQUIPEMENT == idTypeEqpCarteSim)
				{
					formItemIMEI.label=ResourceManager.getInstance().getString('M111','Carte_SIM') + " :";
				}
				
				if (tiNewImei.text == "")
				{
					formItemIMEI.required=true;
					formItemNumSerie.required=false;
					tiNewImei.setStyle('borderColor', 'red');
					return false;
				}
				else
				{
					tiNewImei.setStyle('borderColor', '#cccccc');
				}
			}
			else
			{
				tiNewImei.setStyle('borderColor', '#cccccc');
				formItemIMEI.label=ResourceManager.getInstance().getString('M111','IMEI') + " :";
				
				if (tiNewNumSerie.text == "")
				{
					formItemNumSerie.required=true;
					formItemIMEI.required=false;
					tiNewNumSerie.setStyle('borderColor', 'red');
					return false;
				}
				else
				{
					tiNewNumSerie.setStyle('borderColor', '#cccccc');
				}
			}
			return true;
		}

		// RAFRAICHIR LES LISTES
		private function refreshListe():void
		{
			if (cbType.selectedIndex > -1)
			{
				if (cbMarque.selectedIndex > -1)
				{
					getListeMOdele();
				}
				else
				{
					getListeMarque();
				}
			}
		}

		private function getListeMOdele():void
		{
			var idtype:int=cbType.selectedItem.IDTYPE_EQUIPEMENT;
			var idmarque:int=cbMarque.selectedItem.IDFOURNISSEUR;
			myServiceTerminal.getListeModele(idtype, idmarque);
		}

		private function getListeMarque():void
		{
			var idtype:int=cbType.selectedItem.IDTYPE_EQUIPEMENT;
			myServiceTerminal.getListeMarqueWithType(idtype);
		}

		private function modeleLoaded(evt:gestionparcEvent):void
		{
			cbModele.dataProvider=myServiceTerminal.myDatas.listeModele;
			cbModele.dropdown.dataProvider=myServiceTerminal.myDatas.listeModele;

			if (cbModele.dataProvider.length == 1)
			{
				cbModele.selectedIndex=0;
				onChangeModeleHandler(null);
			}
			else if (modelAdded)
			{
				for each (var o:Object in cbModele.dataProvider)
				{
					if (o.IDEQUIPEMENT_FOURNISSEUR == modelAdded.IDEQUIPEMENT)
					{
						cbModele.selectedItem=o;
						modelAdded=null;
						break;
					}
				}
			}
		}
		
		 
	}
}
