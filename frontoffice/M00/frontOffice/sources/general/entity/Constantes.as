package general.entity
{
	public class Constantes
	{
		public static const CODEAPP_CV4:int = 1;
		public static const CODEAPP_PILOTAGE:int = 51;
		public static const CODEAPP_MYTEM360:int = 101;
		public static const CODEAPP_CONSOLEGESTION:int = 351;
		
		public static const CODECONNECTION_CLASSIQUE:int = 1;
		public static const CODECONNECTION_SSO_SFR:int = 2;
		public static const CODECONNECTION_SSO_SESOUVENIR:int = 3;
		public static const CODECONNECTION_INTERNATIONAL:int = 4;
		public static const CODECONNECTION_CONSOLE_GESTION_INTERNATIONAL:int = 5;	
		public static const CODECONNECTION_SP:int = 6;
		public static const CODECONNECTION_SPTOKEN:int = 61;
		
		public function Constantes()
		{
		}
	}
}