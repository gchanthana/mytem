<cfcomponent extends="portail.class.specialClass.AbstractTemplate" >
	
	
	<!--- CONSTRUCTEUR --->
	<cffunction name="init" access="public" output="false" returntype="TemplateD1">
		
		<cfreturn THIS>
	</cffunction>
	
	
	<!--- CREATION DE LA PAGE --->
	<cffunction name="getPage" access="public" output="false" returntype="any">
		<cfargument name="context" required="true" type="struct"> <!--- le contexte = ref --->
		
		<cftry>			
			
			<!--- CREATION DUNE STRUC ET RECUPERATION DES DONNEES PASSEES EN URL DANS CETTE STRUCTURE --->
			<cfset refParam = createObject("component","portail.class.specialClass.AbstractComposant").getInfoSructURL(context) >
			
			
			<cfset clientId = "defaut" >
			<!--- INSTANCIATION DE LA PAGE EN FONCTION DU CLIENT CONNECTÉ ET SI IL EXISTE UN TEMPLATE CUSTOMISÉ POUR CE CLIENT OU NON --->
			<cfif isDefined("SESSION.USER.CLIENTID")><!---  --->
				<cfset clientId = SESSION.USER.CLIENTID > <!--- apres connexion on recupere le clientID du client connecté --->

				<cfif NOT DirectoryExists("portail/D1/#clientId#")><!--- si le client n'a pas un path customisé dans le template, on le dirige sur le chemin par defaut --->
					<cfset clientId = "defaut" ><!--- on lui donne le path par defaut --->
				</cfif>
			</cfif>
			
			<!--- DETECTER LA PAGE DE LOG OUBLIÉ ET ATTRIBUE UN IDPAGE POUR BASCULER SUR LA PAGE1--->
			<!--- <cfif isDefined("FORM.idPage") && #FORM.idPage# eq "1" >
				<cfset refParam.idpage = "1">
			</cfif> --->
			
			<cfset page = createObject("component","portail.D1.#clientId#.page.Page#refParam.idPage#") >
			<cfreturn page>
			
			<cfcatch><!--- SI LA VALEUR DE CONTEXT.IDPAGE N'EXISTE PAS, RETOUR SUR PAGE DE CONNEXION / PAGE0 --->
			
				<cfset refParam.idPage = "0"> <!--- 0test --->
				<!--- SUPPRESSION DE TOUT LES PARAMETRES DANS L'URL SAUF DU PARAM IDPAGE --->
				<cfloop item="x" collection="#refParam#">
					<cfif #x# neq "idPage">
						<cfset structDelete(refParam,"#x#")>
					</cfif>
				</cfloop>
				
				
				<cfset clientId = "defaut" >
				<!--- INSTANCIATION DE LA PAGE EN FONCTION DU CLIENT CONNECTÉ ET SI IL EXISTE UN TEMPLATE CUSTOMISÉ POUR CE CLIENT OU NON --->
				<cfif isDefined("SESSION.USER.CLIENTID")><!--- --->
					<cfset clientId = SESSION.USER.CLIENTID >
					
					<cfif NOT DirectoryExists("portail/D1/#clientId#")><!--- si le client n'a pas un path customisé dans le template, on le dirige sur le chemin par defaut --->
						<cfset clientId = "defaut" ><!--- on lui donne le path par defaut --->
					</cfif>
				</cfif>
				
				<!--- INSTANCIATION OBJET PAGE0 QUI PERMETTRA D'AFFICHER LA PAGE D'INDEX --->
				<cfset page = createObject("component","portail.D1.#clientId#.page.Page#refParam.idPage#") >
				<cfreturn page>
				
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>