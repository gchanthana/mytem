package gestionparc.ihm.action.mdm.actionGererTerminal
{
	import gestionparc.ihm.action.ICommand;

	public class ActionGererTerminalCommand implements ICommand
	{
		private var receiver:ActionGererTerminalReceiver=null;

		public function ActionGererTerminalCommand(rec:ActionGererTerminalReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
