<cfcomponent output="false" alias="fr.consotel.consoview.inventaire.equipement.affectationlignesfact.PortEquipement">
								   
	<!---
		 These are properties that are exposed by this CFC object.
		 These property definitions are used when calling this CFC as a web services, 
		 passed back to a flash movie, or when generating documentation

		 NOTE: these cfproperty tags do not set any default property values.
	--->
	<cfproperty name="IDPORT_EQUIPEMENT" type="numeric" default="0">
	<cfproperty name="IDEQUIPEMENT" type="numeric" default="0">
	
	<cfproperty name="IDTYPE_CONNECTEUR" type="numeric" default="0">
	<cfproperty name="NUMERO_PORT" type="numeric" default="0">
	<cfproperty name="LIBELLE_PORT" type="string" default="">
	<cfproperty name="USAGE_PORT" type="string" default="">
	<cfproperty name="CODE_INTERNE_PORT" type="string" default="">
	<cfproperty name="GENRE_CONNECTEUR" type="numeric" default="0">
	<cfproperty name="REFERENCE_PORT" type="string" default="">
	
	<cfproperty name="IDTYPE_EQUIPEMENT" type="numeric" default="0">	
	<cfproperty name="TYPE_EQUIPEMENT" type="string" default="">	
	<cfproperty name="LIBELLE_EQ" type="string" default="">	
	<cfproperty name="TYPE_CONNECTEUR" type="string" default="">
	
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		this.IDPORT_EQUIPEMENT = 0;
		this.IDEQUIPEMENT = 0;
		this.IDTYPE_CONNECTEUR = 0;
		this.NUMERO_PORT = 0;
		this.LIBELLE_PORT = "";
		this.USAGE_PORT = "";
		this.CODE_INTERNE_PORT = "";
		this.GENRE_CONNECTEUR = 0;
		this.REFERENCE_PORT = "";
		this.IDTYPE_EQUIPEMENT = 0;
		this.LIBELLE_EQ = "";
		
		this.TYPE_EQUIPEMENT = "";
		this.TYPE_CONNECTEUR = "";
	</cfscript>
	
	<cffunction name="getIDPORT_EQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.IDPORT_EQUIPEMENT>
	</cffunction>

	<cffunction name="setIDPORT_EQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDPORT_EQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDEQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.IDEQUIPEMENT>
	</cffunction>

	<cffunction name="setIDEQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDEQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDTYPE_CONNECTEUR" output="false" access="public" returntype="any">
		<cfreturn this.IDTYPE_CONNECTEUR>
	</cffunction>

	<cffunction name="setIDTYPE_CONNECTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDTYPE_CONNECTEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getNUMERO_PORT" output="false" access="public" returntype="any">
		<cfreturn this.NUMERO_PORT>
	</cffunction>

	<cffunction name="setNUMERO_PORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.NUMERO_PORT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getLIBELLE_PORT" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_PORT>
	</cffunction>

	<cffunction name="setLIBELLE_PORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_PORT = arguments.val>
	</cffunction>

	<cffunction name="getUSAGE_PORT" output="false" access="public" returntype="any">
		<cfreturn this.USAGE_PORT>
	</cffunction>

	<cffunction name="setUSAGE_PORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.USAGE_PORT = arguments.val>
	</cffunction>

	<cffunction name="getCODE_INTERNE_PORT" output="false" access="public" returntype="any">
		<cfreturn this.CODE_INTERNE_PORT>
	</cffunction>

	<cffunction name="setCODE_INTERNE_PORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.CODE_INTERNE_PORT = arguments.val>
	</cffunction>

	<cffunction name="getGENRE_CONNECTEUR" output="false" access="public" returntype="any">
		<cfreturn this.GENRE_CONNECTEUR>
	</cffunction>

	<cffunction name="setGENRE_CONNECTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.GENRE_CONNECTEUR = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getREFERENCE_PORT" output="false" access="public" returntype="any">
		<cfreturn this.REFERENCE_PORT>
	</cffunction>

	<cffunction name="setREFERENCE_PORT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.REFERENCE_PORT = arguments.val>
	</cffunction>
	
	<cffunction name="setIDTYPE_EQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfif (IsNumeric(arguments.val)) OR (arguments.val EQ "")>
			<cfset this.IDTYPE_EQUIPEMENT = arguments.val>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric"/>
		</cfif>
	</cffunction>

	<cffunction name="getIDTYPE_EQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.IDTYPE_EQUIPEMENT>
	</cffunction>
	
	<cffunction name="getLIBELLE_EQ" output="false" access="public" returntype="any">
		<cfreturn this.LIBELLE_EQ>
	</cffunction>

	<cffunction name="setLIBELLE_EQ" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.LIBELLE_EQ = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_EQUIPEMENT" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_EQUIPEMENT>
	</cffunction>

	<cffunction name="setTYPE_EQUIPEMENT" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_EQUIPEMENT = arguments.val>
	</cffunction>

	<cffunction name="getTYPE_CONNECTEUR" output="false" access="public" returntype="any">
		<cfreturn this.TYPE_CONNECTEUR>
	</cffunction>

	<cffunction name="setTYPE_CONNECTEUR" output="false" access="public" returntype="void">
		<cfargument name="val" required="true">
		<cfset this.TYPE_CONNECTEUR = arguments.val>
	</cffunction>
	

</cfcomponent>