package service.widget.updatedataselectorwidget
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class UpdateDataSelectorWidgetHandler
	{
		private var _model:UpdateDataSelectorWidgetModel;
		
		public function UpdateDataSelectorWidgetHandler(_model:  UpdateDataSelectorWidgetModel):void
		{
			this._model = _model;
		}	
		internal function updateDataWidgetResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateDataWidget(evt.result as ArrayCollection);
			}
		}
	}
}