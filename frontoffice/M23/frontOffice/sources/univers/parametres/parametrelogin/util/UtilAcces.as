package univers.parametres.parametrelogin.util
{
	import mx.collections.ArrayCollection;
	
	public class UtilAcces
	{
		private var id : int;
		private var sesPere : ArrayCollection = new ArrayCollection();
		
		public function UtilAcces(id : int){
			this.id = id;
		}
		public function getId():int{
			return id;
		}
		public function getSesPere(): ArrayCollection{
			return sesPere;
		}
		public function ajouteUnPere(acces : UtilAcces):void {
			sesPere.addItem(acces);
		}
	}
}