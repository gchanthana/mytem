package gestioncommande.popup
{
	import composants.ui.TitleWindowBounds;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import gestioncommande.events.CompteEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.utils.ObjectUtil;
	
	public class PopupChooseCompteImpl extends TitleWindowBounds
	{
		//--------------- VARIABLES ----------------//
		[Bindable] 
		public var rbgPool				:RadioButtonGroup;
		[Bindable] 
		public var itemCurrentLabel		:Label;
		public var dgChoosePool			:DataGrid;
		public var tiFiltre				:TextInput;
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		[Bindable]
		public var btnValidEnabled		:Boolean = false;
		
		private var _itemCurrent		:Object = new Object();
		private var _selectedIndex		:Number;
		private var _listeComptes 		:ArrayCollection;
		private var _itemSelectedFirst 	: Object = null;
		
		//--------------- METHODES ----------------//
		
		public function PopupChooseCompteImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		private function initData():void
		{
			listeComptes.filterFunction = filtrerCompte;
			listeComptes.refresh();
			
			// affichage de l'element sécelctionné déjà
			afficherInitialCompteLabel();
			initCurseur();
		}
		
		private function initCurseur():void
		{
			callLater(tiFiltre.setFocus);
		}
		
		// initialisation de l'affichage du label du pool
		private function afficherInitialCompteLabel():void
		{
			if(listeComptes.length > 0)
			{
				var longCompte:int = listeComptes.length;
				for (var i:int = 0; i < longCompte; i++) 
				{
					if(listeComptes[i].SELECTED)
					{
						itemCurrent = listeComptes[i];
						_itemSelectedFirst = listeComptes[i]; 
						break;
					}
				}
			}
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		private function initListeners():void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseRacineHandler);
			addEventListener(KeyboardEvent.KEY_DOWN,validerChooseRacineHandler2);
			btnAnnuler.addEventListener(MouseEvent.CLICK,onCloseHandler);
			dgChoosePool.addEventListener(ListEvent.CHANGE,currentItemHandler);
			
		}
		
		/* */
		protected function currentItemHandler(le:ListEvent):void
		{
			_itemCurrent = le.currentTarget.selectedItem;
			var i:int = 0;
			while (i < listeComptes.length)
			{
				if(_itemCurrent!=listeComptes[i])
					listeComptes[i].SELECTED = false;
				else
				{
					listeComptes[i].SELECTED = true;
					_selectedIndex = i;
				}
				i++;
			}
			
			listeComptes.refresh();
			itemCurrentLabel.text = getItemCurrentLabel();
			btnValidEnabled = true;
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeComptes.filterFunction = filtrerCompte;
			listeComptes.refresh();
		}
		
		/* systeme de filtre */
		private function filtrerCompte(item:Object):Boolean
		{
//			rbgPool.selection = rbgPool.getRadioButtonAt(dgChoosePool.selectedIndex + 1);
			if (item.COMPTE_FACTURATION != null && (item.COMPTE_FACTURATION as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		/**
		 * Retourne le libelle racine approprié peuplant la combobox :
		 *   libelle_racine ( nbCollectes collectes)
		 */
		public function labelCompteFunction(item:Object, column:DataGridColumn):String
		{
			return item.COMPTE_FACTURATION;
		}
		
		protected function onCloseHandler(event:Event):void
		{
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			this.initPopUp();
			PopUpManager.removePopUp(this);
		}
		
		// initialise la popup vers le choix du pool initial
		private function initPopUp():void
		{
			for (var i:int = 0; i < listeComptes.length; i++) 
			{
				listeComptes[i].SELECTED = false;
				if((_itemSelectedFirst) && (listeComptes[i].IDCOMPTE_FACTURATION == _itemSelectedFirst.IDCOMPTE_FACTURATION))
					listeComptes[i].SELECTED = true;
			}
			listeComptes.refresh();
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		/* au click sur valider */
		private function validerChooseRacineHandler(evt:MouseEvent):void
		{
			validerChoix();
		}
		
		private function validerChoix():void
		{
			if(dgChoosePool.selectedItem != null)
			{
				var idSelectedCompte:String =  dgChoosePool.selectedItem.IDCOMPTE_FACTURATION;
				dispatchEvent(new CompteEvent(CompteEvent.VALID_CHOIX_COMPTE,idSelectedCompte,true));
				tiFiltre.text = '';
				tiFiltre.dispatchEvent(new Event(Event.CHANGE));
				onCloseHandler(null);
			}
			
		}
		
		private function validerChooseRacineHandler2(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
			{
				validerChoix();
			}
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		
		[Bindable]
		public function get listeComptes():ArrayCollection
		{
			return _listeComptes;
		}
		public function set listeComptes(value:ArrayCollection):void
		{
			_listeComptes = value;
		}
		
		public function get itemCurrent():Object
		{
			return _itemCurrent;
		}
		
		public function set itemCurrent(value:Object):void
		{
			_itemCurrent = value;
		}
		
		public function getItemCurrentLabel():String
		{
			if (_itemCurrent != null)
				return _itemCurrent.COMPTE_FACTURATION;
			else
				return '';
		}
	}
}