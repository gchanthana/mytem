<cfcomponent author="Cedric" displayName="fr.saaswedo.api.da.impl.db.Usage" extends="fr.saaswedo.api.da.impl.Usage"
hint="Extension qui fournit les données à des procédures stockées">
	<cffunction access="private" name="saveCumulativeUsage" returntype="void"
	hint="Stocke le flux Cumulative Usage au format JSON dans la base. En cas d'exception il sera stocké avec l'implémentation parent">
		<cfargument name="datalertPayload" type="Struct" required="true" hint="Données du flux : Structure dont les clés sont les champs métiers">
		<cfset var targetURI=CGI.SCRIPT_NAME>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<cfset var dbJsonData=serializeJSON(ARGUMENTS.datalertPayload)>
		<cftry>
			<cfset storeUsageInDatabase("pkg_m26.saveCumulativeFlux",dbJsonData)>
			<!--- En cas d'exception : Le stockage de l'implémentation parent sera effectué --->
			<cfcatch type="Any">
				<cflog type="error" text="[#log#][#targetURI#] #clientInfo# - #CFCATCH.message# [#CFCATCH.detail#]">
				<cfset var requestData=DUPLICATE(getHttpRequestData())>
				<cfset requestData[DB_JSON_DATA()]=dbJsonData>
				<!--- Traitement de l'exception : Sans envoi de réponse HTTP au client car les données seront stockées avec l'implémentation parent --->
				<cfset processServerError(targetURI,requestData,CFCATCH,"Cumulative Usage : Storage FAILED")>
				<!--- Stockage par l'implémentation parent --->
				<cfset SUPER.saveCumulativeUsage(argumentCollection=ARGUMENTS)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="saveCompleteApplicationUsage" returntype="void"
	hint="Stocke le flux Complete Application Usage au format JSON dans la base. En cas d'exception il sera stocké avec l'implémentation parent">
		<cfargument name="datalertPayload" type="Array" required="true" hint="Données CSV : Tableau de structure dont les clés sont les champs métiers">
		<cfset var targetURI=CGI.SCRIPT_NAME>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<cfset var dbJsonData=serializeJSON(ARGUMENTS.datalertPayload)>
		<cftry>
			<cfset storeUsageInDatabase("pkg_m26.saveApplicationFlux",dbJsonData)>
			<!--- En cas d'exception : Le stockage de l'implémentation parent sera effectué --->
			<cfcatch type="Any">
				<cflog type="error" text="[#log#][#targetURI#] #clientInfo# - #CFCATCH.message# [#CFCATCH.detail#]">
				<cfset var requestData=DUPLICATE(getHttpRequestData())>
				<cfset requestData[DB_JSON_DATA()]=dbJsonData>
				<!--- Traitement de l'exception : Sans envoi de réponse HTTP au client car les données seront stockées avec l'implémentation parent --->
				<cfset processServerError(targetURI,requestData,CFCATCH,"Complete Application Usage : Storage FAILED")>
				<!--- Stockage par l'implémentation parent --->
				<cfset SUPER.saveCompleteApplicationUsage(argumentCollection=ARGUMENTS)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="saveCompleteUsage" returntype="void"
	hint="Stocke le flux Complete Usage au format JSON dans la base. En cas d'exception il sera stocké avec l'implémentation parent">
		<cfargument name="datalertPayload" type="Array" required="true" hint="Données CSV : Tableau de structure dont les clés sont les champs métiers">
		<cfset var targetURI=CGI.SCRIPT_NAME>
		<cfset var log=GLOBALS().CONST('Datalert.LOG')>
		<cfset var clientInfo=getClientStringInfo()>
		<cfset var dbJsonData=serializeJSON(ARGUMENTS.datalertPayload)>
		<cftry>
			<cfset storeUsageInDatabase("pkg_m26.saveCompleteFlux",dbJsonData)>
			<!--- En cas d'exception : Le stockage de l'implémentation parent sera effectué --->
			<cfcatch type="Any">
				<cflog type="error" text="[#log#][#targetURI#] #clientInfo# - #CFCATCH.message# [#CFCATCH.detail#]">
				<cfset var requestData=DUPLICATE(getHttpRequestData())>
				<cfset requestData[DB_JSON_DATA()]=dbJsonData>
				<!--- Traitement de l'exception : Sans envoi de réponse HTTP au client car les données seront stockées avec l'implémentation parent --->
				<cfset processServerError(targetURI,requestData,CFCATCH,"Complete Usage : Storage FAILED")>
				<!--- Stockage par l'implémentation parent --->
				<cfset SUPER.saveCompleteUsage(argumentCollection=ARGUMENTS)>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>