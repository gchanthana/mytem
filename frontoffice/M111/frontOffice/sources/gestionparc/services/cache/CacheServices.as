package gestionparc.services.cache
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	
	import mx.rpc.AbstractOperation;

	public class CacheServices
	{
		
		public var myDatas 		: CacheDatas;
		public var myHandlers 	: CacheHandlers;
		
		private static var _instance		:CacheServices;
		
		public function CacheServices()
		{
			
			myDatas 	= new CacheDatas();
			myHandlers 	= new CacheHandlers(myDatas);
			if(_instance == null)
			{
				_instance = this;
			}
		}

		public static function getInstance():CacheServices
		{
			return CacheServices._instance;
		}
		public static function resetInstance():void
		{
			if(_instance)
				_instance = null;
		}
		public function getCacheData():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getCacheData",
				myHandlers.getCacheDataResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		public function initDataInCache(idpool:int,idprofil:int):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.UtilisateurService",
				"threadInitData",
				myHandlers.recupererInitData);
			
			RemoteObjectUtil.callService(op,idpool,idprofil);
		}
		public function getListePool():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.UtilisateurService",
				"getListePool",
				myHandlers.getListePoolResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - Les sites de livraisons du pool sélectionné.
		 */	
		public function getSiteLivraison():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"fournirListeSitesLivraisonsPoolGestionnaire",
				myHandlers.getSiteLivraisonResultHandler);
			
			RemoteObjectUtil.callService(op, SpecificationVO.getInstance().idPool);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - La liste des civilités.
		 */	
		public function getCivilite():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getCivilite",
				myHandlers.getCiviliteResultHandler);
			
			RemoteObjectUtil.callService(op);
		}		
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - La liste des libellé perso.
		 */	
		public function getLibellePerso():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getLibellePerso",
				myHandlers.getLibellePersoResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer :
		 * - La liste des revendeurs.
		 */	
		public function getListeRevendeur():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListeRevendeur",
				myHandlers.getListeRevendeurResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer :
		 * - La liste des types de contrats de services.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		public function getListeTypeContrat():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListeTypeContrat",
				myHandlers.getListeTypeContratResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer :
		 * - La liste des types d'intervention de services.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		public function getListeTypeIntervention():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListeTypeIntervention",
				myHandlers.getListeTypeInterventionResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer :
		 * - La liste des usages.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		public function getListeUsages():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getUsages",
				myHandlers.getListeUsagesResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des profils équipement.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		public function getListProfilEquipements():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListProfilEquipement",
				myHandlers.getListProfilEquipementsResultHandler);
			
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des revendeurs pour un profil d'equipement.
		 * </pre></p>
		 * 
		 * @param Object
		 * @return void
		 * 
		 */	
		public function getListRevendeurForProfEqpt(objToServiceGet:Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListRevendeurForProfEqpt",
				myHandlers.getListRevendeurForProfEqptResultHandler);
			
			RemoteObjectUtil.callService(op, objToServiceGet.idPool, objToServiceGet.idProfilEquipement, objToServiceGet.clefRecherche);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des types de ligne.
		 * </pre></p>
		 * 
		 * @return void
		 * 
		 */	
		public function getTypeLigne():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getTypeLigne",
				myHandlers.getTypeLigneResultHandler);		
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des comptes et sous comptes.
		 * </pre></p>
		 * 
		 * @param Object
		 * @return void
		 * 
		 */	
		public function getListeComptesOperateurByLoginAndPool(objToServiceGet:Object):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListeComptesOperateurByLoginAndPool",
				myHandlers.getListeComptesOperateurByLoginAndPoolResultHandler);		
			RemoteObjectUtil.callService(op,objToServiceGet.idOperateur,objToServiceGet.idPool);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des opérateurs par segment.
		 * </pre></p>
		 * 
		 * @return void
		 * 
		 */
		public function getListeOperateurs():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"fournirListeOperateursSegment", 
				myHandlers.getListeOperateursResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer les informations liées à un operateur.
		 * </pre></p>
		 * 
		 * @param Object
		 * @return void
		 * 
		 */
		public function getInfoClientOp(objToServiceGet:Object):void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getInfoClientOp", 
				myHandlers.getInfoClientOpResultHandler);
			RemoteObjectUtil.callService(op, objToServiceGet.idOperateur);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des pays.
		 * </pre></p>
		 * 
		 * @return void
		 */
		public function getListPays():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheCommun",
				"getListePays", 
				myHandlers.getListePaysResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des constructeurs.
		 * </pre></p>
		 * 
		 * @return void
		 */	
		public function getConstructeur():void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview..M111.fiches.FicheCommun",
				"getConstructeur", 
				myHandlers.getConstructeurResultHandler);
			RemoteObjectUtil.callService(opData);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des categories des equipements
		 * </pre></p>
		 * 
		 * @return void
		 */	
		public function getCategoriesEquipement():void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview..M111.fiches.FicheCommun",
				"getCategoriesEquipement", 
				myHandlers.getCategoriesEquipementResultHandler);
			RemoteObjectUtil.callService(opData);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Appelle la procédure permettant de récupérer la liste des types des equipements
		 * </pre></p>
		 * 
		 * @return void
		 */	
		public function getTypesEquipement():void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview..M111.fiches.FicheCommun",
				"getTypesEquipement", 
				myHandlers.getTypesEquipementResultHandler);
			RemoteObjectUtil.callService(opData);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste des types des equipements avec leurs categories
		 */	
		public function getTypesEquipementWithCategorie():void
		{
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview..M111.fiches.FicheCommun",
				"getTypesEquipementWithCategorie", 
				myHandlers.getTypesEquipementWithCategorieResultHandler);
			RemoteObjectUtil.callService(opData);
		}
	
		/**
		 * Appelle la procédure permettant de récupérer la liste des modes de raccordements
		 */	
		public function getListeModeRaccordement():void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
				"getListeModeRaccordement",
				myHandlers.getListeModeRaccordementResultHandler);
			RemoteObjectUtil.callService(op);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste de tous les opérateurs
		 */
		public function getListeTousOperateurs():void
		{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
				"getListeOperateurs",
				myHandlers.getListeTousOperateursResultHandler);
			RemoteObjectUtil.callService(op);
		}
	}
}