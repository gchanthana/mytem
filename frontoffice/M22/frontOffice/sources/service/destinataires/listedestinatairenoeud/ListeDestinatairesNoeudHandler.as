package service.destinataires.listedestinatairenoeud
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class ListeDestinatairesNoeudHandler
	{
		private var _model:ListeDestinatairesNoeudModel;
		
		public function ListeDestinatairesNoeudHandler(_model:ListeDestinatairesNoeudModel):void
		{
			this._model = _model;
		}
		
		internal function getListeDestinatairesNoeudResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.listeDestinatairesNoeud(event.result as ArrayCollection);
			}
		}	
	}
}