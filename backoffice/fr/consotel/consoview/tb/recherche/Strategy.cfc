<!--- =========================================================================
Name: Strategy
Original Author: 
$History: $
$NoKeywords: $
========================================================================== --->
<cfcomponent displayname="Strategy" hint="This class declares an interface common to all supported algorithms. Context uses this interface to call the algorithm defined by a ConcreteStrategy.">
<!--- =========================================================================
CONSTRUCTOR
========================================================================== --->
	<cffunction name="init" access="public" output="false" returntype="Strategy" displayname="Strategy init()" hint="Initialize the Strategy object">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- =========================================================================
METHODS
========================================================================== --->
	<cffunction name="getData" access="public" returntype="query" output="false" displayname="query getData(numeric, string, string, string)" >
		<cfargument name="IDCompte" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="chaine" required="true" type="string" default="" displayname="la chaine de recherche" hint="Initial value for the chaine property." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		<cfabort showerror="This Method is Abstract and needs to be overridden">
	</cffunction>	
	
	<cffunction name="getLignes" access="public" returntype="query" output="false" displayname="query getData(numeric, string, string)" >
		<cfargument name="IDGroupe" required="true" type="numeric" default="" displayname="numeric ID" hint="Initial value for the IDproperty." />
		<cfargument name="DateDebut" required="false" type="string" default="" displayname="numeric DateDebut" hint="Initial value for the DateDebutproperty." />
		<cfargument name="DateFin" required="false" type="string" default="" displayname="numeric DateFin" hint="Initial value for the DateFinproperty." />
		<cfabort showerror="This Method is Abstract and needs to be overridden">
	</cffunction>	
	
	<cffunction name="getStrategy" access="public" returntype="string" output="false" displayname="string getLibelle(numeric)" >
		<cfabort showerror="This Method is Abstract and needs to be overridden">
	</cffunction>	
</cfcomponent>