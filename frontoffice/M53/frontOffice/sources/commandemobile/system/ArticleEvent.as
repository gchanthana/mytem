package commandemobile.system
{
	import composants.util.article.Article;
	
	import flash.events.Event;

	public class ArticleEvent extends Event
	{
		public static const ARTICLE_CREATED:String = "articleCreated";
		public static const ARTICLE_UPDATED:String = "articleUpdated";
		public static const ARTICLE_REMOVED:String = "articleRemoved";		
		public static const SHOW_ARTICLE:String = "showArticle";
		public static const EMPLOYE_ADDED:String = "employeAssocie";
		public static const EMPLOYE_REMOVED:String = "employeDissocie";
		
		private var _article:Article;		
		public function get article():Article{
			return _article
		};
		
		public function ArticleEvent(value:Article,type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_article = value;
		}
		
		override public function clone():Event{
            return new ArticleEvent(article,type,bubbles,cancelable);
        }

	}
}