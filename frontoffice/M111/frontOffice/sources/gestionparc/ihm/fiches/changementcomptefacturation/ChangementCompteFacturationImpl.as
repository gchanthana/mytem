package gestionparc.ihm.fiches.changementcomptefacturation
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.events.ValidationResultEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.EmailValidator;
	
	import composants.util.DateFunction;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.popup.PopUpChooseCompteIHM;
	import gestionparc.ihm.fiches.popup.PopUpChooseSousCompteIHM;

	/**
	 * Cette classe étend la classe <code>TitleWindow</code> et permet d'afficher une popup
	 * pour la demande de changement de compte facturation opérateur.
	 */
	[Bindanle]
	public class ChangementCompteFacturationImpl extends TitleWindow
	{
		public var btValider:Button;
		public var btAnnuler:Button;

		public var lbOperateur:Label;

		public var cbCompteCible:ComboBox;
		public var cbSousCompteCible:ComboBox;
		public var cbDestinataire:ComboBox;
		public var cbFormat:ComboBox;

		public var taTextMail:TextArea;

		public var tiDestinataireCopie:TextInput;
		public var cbCopie:CheckBox;

		public var operateurName:String;
		public var idPool:Number;
		public var idRevendeur:Number;
		public var idOperateur:Number;
		public var listeElementsSelectiones:ArrayCollection;

		public var mailValidator:EmailValidator;

		public var fiDestinataireCopie:FormItem;
		public var fiDestinataire:FormItem;
		public var fiCompte:FormItem;
		public var fiSousCompte:FormItem;
		public var fiComptetxt:FormItem;
		public var fiSousComptetxt:FormItem;
		
		public var cbAutreCpt:CheckBox;
		public var tiCompteCible:TextInput;
		public var tiSousCompteCible:TextInput;

		public var format:String="XLS";
		
		private var _itemSelected			:Boolean = false;
		private var _popUpChooseCompte		:PopUpChooseCompteIHM;
		private var _popUpChooseSousCompte	:PopUpChooseSousCompteIHM;

		private var myChangementCompteFacturationServices:ChangementCompteFacturationServices;


		public function ChangementCompteFacturationImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Initialisation générale de cette classe.
		 */
		private function init(evt:FlexEvent):void
		{
			myChangementCompteFacturationServices=new ChangementCompteFacturationServices(this);
			myChangementCompteFacturationServices.getListeDestinataires();
			myChangementCompteFacturationServices.getCompteOpeCompte();

			initDisplay();
			initListener();
		}

		/**
		 * Initialise l'affichage de cette interface.
		 */
		private function initDisplay():void
		{
			var userName:String=CvAccessManager.getSession().USER.PRENOM + " " + CvAccessManager.getSession().USER.NOM;
			taTextMail.htmlText=ResourceManager.getInstance().getString('M111', 'Bonjour__br____br___Veuillez_prendre_en_') + userName + ".";
			cbCopie.selected=true;
			tiCompteCible.enabled = false;
			tiSousCompteCible.enabled = false;
			lbOperateur.text=operateurName;
			cbFormat.selectedIndex=0;
		}


		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			myChangementCompteFacturationServices.addEventListener(ChangementCompteFacturationServices.LISTE_COMPTES_SOUSCOMPTES_LOADED, initCbCompte);
			myChangementCompteFacturationServices.addEventListener(ChangementCompteFacturationServices.LISTE_DESTINATAIRE_LOADED, initCbDestinataire);
			myChangementCompteFacturationServices.addEventListener(ChangementCompteFacturationServices.END_OF_OPERATION, closePopup);

			cbCompteCible.addEventListener(ListEvent.CHANGE, cbCompteCibleItemSelectedChange);
			cbFormat.addEventListener(ListEvent.CHANGE, cbFormateChangeHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validerHandler);
			cbAutreCpt.addEventListener(MouseEvent.CLICK, enableDisableCombo);
			
		}

		/**
		 * Initialise les deux ComboBox "Compte cible" et "Sous compte cilbe".
		 */
		private function initCbCompte(evt:Event):void
		{
			cbCompteCible.dataProvider=myChangementCompteFacturationServices.listeComptesAndSousCompte;
			cbCompteCible.dropdown.dataProvider=myChangementCompteFacturationServices.listeComptesAndSousCompte;

			cbCompteCible.prompt=ResourceManager.getInstance().getString('M111', 'S_lectionner_une_valeur');
			cbSousCompteCible.prompt=ResourceManager.getInstance().getString('M111', 'S_lectionner_une_valeur');

			if ((cbCompteCible.dataProvider as ArrayCollection).length == 1)
			{
				cbCompteCible.selectedIndex=0;
				cbCompteCible.dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}
			else
			{
				cbCompteCible.selectedIndex=-1;
			}
		}

		/**
		 * Initialise la ComboBox "Sous compte cilbe".
		 */
		private function cbCompteCibleItemSelectedChange(evt:ListEvent):void
		{
			initSousCompte();
		}

		private function initSousCompte():void
		{
			if (!cbCompteCible.selectedItem)
			{
				cbSousCompteCible.selectedIndex = -1;
				return;
			}
			
			cbSousCompteCible.dataProvider=cbCompteCible.selectedItem.TAB_SOUS_COMPTE;
			cbSousCompteCible.dropdown.dataProvider=cbCompteCible.selectedItem.TAB_SOUS_COMPTE;
			if ((cbSousCompteCible.dataProvider as ArrayCollection).length == 1)
			{
				cbSousCompteCible.selectedIndex=0;
			}
			else
			{
				cbSousCompteCible.selectedIndex=-1;
			}
		}
		
		/**
		 * Listener pour la ComboBox "Format".
		 */
		private function cbFormateChangeHandler(evt:ListEvent):void
		{
			if (evt.currentTarget.selectedItem)
			{
				format=evt.currentTarget.selectedItem.value;
			}
			else
			{
				evt.currentTarget.selectedIndex=0
				format=evt.currentTarget.selectedItem.value;
			}
		}
		
		/**
		 * Listener pour la CheckBox "cbAutreCpt".
		 */
		private function enableDisableCombo(evt:MouseEvent):void
		{
			if (cbAutreCpt.selected) // envoie de mail
			{
				tiCompteCible.enabled = true;
				tiSousCompteCible.enabled = true;
				cbCompteCible.enabled = false;
				cbSousCompteCible.enabled = false;
			}
			else
			{
				tiCompteCible.enabled = false;
				tiSousCompteCible.enabled = false;
				cbCompteCible.enabled = true;
				cbSousCompteCible.enabled = true;
			}
		}


		/**
		 * Initialise la ComboBox "Destinataire".
		 */
		private function initCbDestinataire(evt:Event):void
		{
			cbDestinataire.dataProvider=myChangementCompteFacturationServices.listeDestinataire;
			cbDestinataire.dropdown.dataProvider=myChangementCompteFacturationServices.listeDestinataire;
			cbDestinataire.prompt=ResourceManager.getInstance().getString('M111', 'S_lectionner_une_valeur');
			if ((cbDestinataire.dataProvider as ArrayCollection).length == 1)
			{
				cbDestinataire.selectedIndex=0;
			}
			else
			{
				cbDestinataire.selectedIndex=-1;
			}
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette méthode appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Lors du clique sur le bouton "Valider", effectue les vérifications et demande les comptes et sous comptes
		 * associés aux lignes sélectionnées, puis passe à l'étape 2.
		 */
		private function validerHandler(e:MouseEvent):void
		{
			var evt:ValidationResultEvent;

			if (cbFormat.selectedIndex == -1)
				cbFormat.selectedIndex=0;

			if (tiDestinataireCopie.text != "")
			{
				evt=mailValidator.validate(tiDestinataireCopie.text);
			}


			if (( !cbAutreCpt.selected &&  cbCompteCible.selectedIndex != -1 && cbSousCompteCible.selectedIndex != -1 ||
				   cbAutreCpt.selected &&  tiCompteCible.text != "" && tiSousCompteCible.text != "")  
					&& cbDestinataire.selectedIndex != -1 
					&& (evt == null || ((evt.results != null && evt.results.length == 0) || evt.results == null)))
			{
				var strLignesCompte:String="";
				for each (var tmp:* in listeElementsSelectiones)
				{
					strLignesCompte+=tmp.IDSOUS_TETE;
					strLignesCompte+=",";
				}

				fiCompte.required=false;
				fiSousCompte.required=false;
				fiDestinataire.required=false;
				fiDestinataireCopie.required=false;

				strLignesCompte=strLignesCompte.substr(0, strLignesCompte.length - 1);
				myChangementCompteFacturationServices.getListeCompte(strLignesCompte);
				myChangementCompteFacturationServices.addEventListener(ChangementCompteFacturationServices.LISTE_COMPTES_SOUSCOMPTES_LISTES_SELECTED_LOADED, validerHandlerStep2);
			}
			else
			{
				var boolOk:Boolean=true;
				if (!cbAutreCpt.selected && cbCompteCible.selectedIndex < 0)
				{
					boolOk=false;
					fiCompte.required=true;
				}
				else
					fiCompte.required=false;

				if (!cbAutreCpt.selected && cbSousCompteCible.selectedIndex < 0)
				{
					boolOk=false;
					fiSousCompte.required=true;
				}
				else
					fiSousCompte.required=false;
				if (cbAutreCpt.selected && tiCompteCible.text == "")
				{
					boolOk=false;
					fiComptetxt.required=true;
				}
				else
					fiComptetxt.required=false;
				
				if (cbAutreCpt.selected && tiSousCompteCible.text == "")
				{
					boolOk=false;
					fiSousComptetxt.required=true;
				}
				else
					fiSousComptetxt.required=false;

				if (cbDestinataire.selectedIndex < 0)
				{
					boolOk=false;
					fiDestinataire.required=true;
				}
				else
					fiDestinataire.required=false;

				if (evt != null && evt.results != null && evt.results.length > 0)
				{
					tiDestinataireCopie.errorString=ResourceManager.getInstance().getString("M111", "Veuillez_renseigner_correctement_le_cham");
					if (boolOk)
						Alert.show(ResourceManager.getInstance().getString('M111', 'Veuillez_renseigner_correctement_le_cham'), ResourceManager.getInstance().getString('M111', 'Erreur'));
				}
				else
					tiDestinataireCopie.errorString="";

				if (!boolOk)
					Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionnez_un_compte_cible_'), ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
		}

		/**
		 * Etape 2 lors du clique sur le bouton "Valider", effectue les vérifications et l'envoie de mail.
		 */
		private function validerHandlerStep2(evt:Event):void
		{
			var userEmail:String=CvAccessManager.getSession().USER.EMAIL;
			var copieEmail:String=tiDestinataireCopie.text;

			if (cbDestinataire.selectedItem.EMAIL)
			{
				if (cbCopie.selected) // envoie de mail
				{
					myChangementCompteFacturationServices.sendChangementCompteFacture(cbDestinataire.selectedItem.EMAIL, createCSV(), true, createHistoricString(), copieEmail, userEmail);
				}
				else
				{
					myChangementCompteFacturationServices.sendChangementCompteFacture(cbDestinataire.selectedItem.EMAIL, createCSV(), false, createHistoricString(), copieEmail);
				}
			}
			else
			{
				Alert.show(ResourceManager.getInstance().getString('M111', 'L_adresse_email_de_ce_destinataire_n_est'), ResourceManager.getInstance().getString('M111', 'Erreur'));
			}
		}

		/**
		 * Retourne une string au format csv.
		 */
		private function createCSV():String
		{
			var strResult:String=ResourceManager.getInstance().getString('M111', 'Ligne_Compte_actuel_Sous_compte_actuel_C');
			var compteCible:String;
			var sousCompteCible:String;
			
			if (cbAutreCpt.selected)
				{
					compteCible = tiCompteCible.text;
					sousCompteCible = tiSousCompteCible.text;
				}
			else	
				{
					compteCible = cbCompteCible.selectedItem.COMPTE;
					sousCompteCible = cbSousCompteCible.selectedItem.SOUS_COMPTE;	
				}
			
			for each (var obj:* in listeElementsSelectiones)
			{
				strResult+=obj.LIGNE + ";" + obj.COMPTE_FACTURATION + ";" + obj.SOUS_COMPTE + ";" + compteCible + ";" + sousCompteCible + ";\n";
			}
			return strResult;
		}

		/**
		 * Retourne un XML permettant l'enregistrement de l'historique de chaque ligne.
		 */
		private function createHistoricString():String
		{
			var strNode:String="<lignes>";
			var compteCible:String;
			var sousCompteCible:String;
			
			if (cbAutreCpt.selected)
			{
				compteCible = tiCompteCible.text;
				sousCompteCible = tiSousCompteCible.text;
			}
			else	
			{
				compteCible = cbCompteCible.selectedItem.COMPTE;
				sousCompteCible = cbSousCompteCible.selectedItem.SOUS_COMPTE;	
			}

			for each (var obj:* in listeElementsSelectiones)
			{
				strNode+="<ligneChangement>";
				strNode+="<idligne>" + obj.IDSOUS_TETE + "</idligne>";
				strNode+="<ligne>" + obj.LIGNE + "</ligne>";
				strNode+="<item1>" + obj.COMPTE_FACTURATION + "/" + obj.SOUS_COMPTE + "</item1>";
				strNode+="<item2>" + compteCible + "/" + sousCompteCible + ResourceManager.getInstance().getString('M111', '_faite___') + cbDestinataire.selectedItem.NOM + "</item2>";
				strNode+="<date>" + DateFunction.formatDateAsInverseString(new Date()) + "</date>";
				strNode+="</ligneChangement>";
			}
			strNode+="</lignes>";
			return strNode;
		}
		
		public function btImgChooseCompte_clickHandler(e:Event):void
		{
			var items:ArrayCollection = cbCompteCible.dataProvider as ArrayCollection;
			
			_popUpChooseCompte = new PopUpChooseCompteIHM;
			var point:Point = new Point(stage.mouseX,stage.mouseY);
			_popUpChooseCompte.x = point.x - cbCompteCible.width; 
			_popUpChooseCompte.y = point.y;
			_popUpChooseCompte.listeItem = items;
			_popUpChooseCompte.addEventListener(gestionparcEvent.VALID_CHOIX_COMPTE, getSelectedItemHandler);
			PopUpManager.addPopUp(_popUpChooseCompte, this, true);
		}
		
		public function btImgChooseSousCompte_clickHandler(e:Event):void
		{
			var items:ArrayCollection = cbSousCompteCible.dataProvider as ArrayCollection;
			
			_popUpChooseSousCompte = new PopUpChooseSousCompteIHM;
			var point:Point = new Point(stage.mouseX,stage.mouseY);
			_popUpChooseSousCompte.x = point.x - cbSousCompteCible.width; 
			_popUpChooseSousCompte.y = point.y;
			_popUpChooseSousCompte.listeItem = items;
			_popUpChooseSousCompte.addEventListener(gestionparcEvent.VALID_CHOIX_SOUSCOMPTE, getSelectedSousCompteHandler);
			PopUpManager.addPopUp(_popUpChooseSousCompte, this, true);
		}
		
		protected function getSelectedItemHandler(e:gestionparcEvent):void
		{
			_popUpChooseCompte.removeEventListener(gestionparcEvent.VALID_CHOIX_COMPTE, getSelectedItemHandler);
			cbCompteCible.selectedItem = _popUpChooseCompte.itemCurrent;
			initSousCompte();
		}
		
		protected function getSelectedSousCompteHandler(e:gestionparcEvent):void
		{
			_popUpChooseCompte.removeEventListener(gestionparcEvent.VALID_CHOIX_SOUSCOMPTE, getSelectedSousCompteHandler);
			cbSousCompteCible.selectedItem = e.obj;
		}
	}
}