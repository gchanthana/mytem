<cfcomponent extends="portail.class.specialClass.AbstractComposant" 
			 implements="portail.class.specialClass.InterfaceComposant" 
			 output="false" 
			 displayname="Composant" 
			 hint="Classe de composant dans le template D1" >
	
	<!---  --->
	<cffunction name="init" access="public" output="false" returnType="void">
		<!--- CONSTRUCTEUR --->
	</cffunction>
	
	
	<!---  --->
	<cffunction name="getDivSelectTerminal" access="public" output="true" returnType="string" 
				displayname="" 
				hint="Génère la div contenant la combo et la photo concernant les terminaux / template D1" >
		
		<cfargument name="idSousTete" required="false" type="string" default=0 >
		
		<cfset typeMod = "">
		<cfset t_imei = "">
		<cfset IMEI = "">
		<cfset image = '<img src="img/NoVisuelSmall.jpg" title="Aucune image disponible" alt="Aucune image disponible" style="" >'>
		
		
		<cfif SESSION.IDEMPLOYE eq "17010"><cfset SESSION.IDEMPLOYE = 75325></cfif>
		<cfset SESSION.queryMesTerminaux = createObject("component","portail.class.classData.DataService").getListEqptFromCollab(SESSION.IDEMPLOYE,SESSION.USER.CLIENTID) >
				
		
		<cfif idSousTete eq 0><!---  si première navigation sur cette page, affiche premier terminal de la liste --->
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
					SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
					SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
					
					<cfset type_modele = SESSION.queryMesTerminaux['T_MODELE'][currentRow]>
					<cfset path_ = SESSION.queryMesTerminaux['PATH_LOW'][currentRow] >
					<cfif path_ neq "/">
						<cfset image = '<img src="http://images.consotel.fr/#path_#" title="#type_modele#" alt="#type_modele#">'>
					<cfelse>
						<cfset image = '<img src="img/NoVisuel.jpeg" title="Aucune image disponible" alt="Aucune image disponible" style="" >'>
					</cfif>
					<cfset IMEI = SESSION.queryMesTerminaux['T_IMEI'][currentRow] >
					<cfbreak>
					
				</cfif>
			</cfloop>
			
		<cfelse>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] eq idSousTete>
					
					<cfset type_modele = SESSION.queryMesTerminaux['T_MODELE'][currentRow]>
					<cfset path_ = SESSION.queryMesTerminaux['PATH_LOW'][currentRow] >
					<cfif path_ neq "/">
						<cfset image = '<img src="http://images.consotel.fr/#path_#" title="#type_modele#" alt="#type_modele#">'>
					<cfelse>
						<cfset image = '<img src="img/NoVisuel.jpeg" title="Aucune image disponible" alt="Aucune image disponible" style="" >'>
					</cfif>
					<cfset IMEI = SESSION.queryMesTerminaux['T_IMEI'][currentRow] >
					<cfbreak>
					
				</cfif>
			</cfloop>
			
		</cfif>
		
		<cfset SESSION.queryDeviceInfo = createObject("component","fr.consotel.consoview.M111.MDMService").getDeviceInfo(IMEI) >
		
		
		<cfset monTerminal = "" >
		
		<!--- SI LE COLLABORATEUR A PLUSIEURS LIGNES --->
		<cfif SESSION.terminauxAvailable gt 1>
			
			<cfset monTerminal &= '
					<div align="left" class="divMonTerminalCombo" style="">
						<select id="selectTerminal" name="selectTerminal" onchange="comboTerminal();loadsablier();" class="selectMonTerminalCombo" style="">
			'>
			
			<cfset i = 0>
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				<cfset i = i+1>
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
					
					<cfset sousTete = "" >
					<cfset idSousTete_ = "" >
					<cfset typeModele = "" >
					<cfset numIMEI = "" >
					
					
					<cfif SESSION.queryMesTerminaux["IDSOUS_TETE"][currentRow] neq "" >
						<cfset idSousTete_ = SESSION.queryMesTerminaux["IDSOUS_TETE"][currentRow] >
					</cfif>
					<cfif SESSION.queryMesTerminaux["SOUS_TETE"][#currentRow#] neq "" >
						<cfset sousTete = SESSION.queryMesTerminaux["SOUS_TETE"][currentRow] >
					</cfif>
					<cfif SESSION.queryMesTerminaux["T_MODELE"][currentRow] neq "" >
						<cfset typeModele = SESSION.queryMesTerminaux["T_MODELE"][currentRow] >
					</cfif>
					<cfif SESSION.queryMesTerminaux["T_IMEI"][currentRow] neq "" >
						<cfset numIMEI = SESSION.queryMesTerminaux["T_IMEI"][currentRow] >
					</cfif>
					<cfif SESSION.queryMesTerminaux["ID"][currentRow] neq "" >
						<cfset id = SESSION.queryMesTerminaux["ID"][currentRow] >
					</cfif>
					<cfif SESSION.queryMesTerminaux["IDTERMINAL"][currentRow] neq "" >
						<cfset id_terminal = SESSION.queryMesTerminaux["IDTERMINAL"][currentRow] >
					</cfif>
					
					<!--- CRÉATION D'UNE STRUCTURE POUR LA CONSTRUCTION DE L'URL --->
					<cfif id_terminal neq "" && id neq "">
						<cfset struct = {idPage = "5",
										idSous_tete = "#idSousTete_#",
										sous_tete = "#sousTete#"
						} >
						<cfset urlString = createRefURL(struct)>
					</cfif>
					
					
					<cfif idSousTete eq idSousTete_>
						<cfset monTerminal &= '<option selected="selected" id="#i#" value="#urlString#" >#typeModele# - ref. du terminal : #numIMEI#</option>'>
					<cfelse>
						<cfset monTerminal &= '<option id="#i#" value="#urlString#" >#typeModele# - ref. du terminal : #numIMEI#</option>'>
					</cfif>
					
				</cfif>
			</cfloop>
		
			<cfset monTerminal &= '
							</select>
						</div>
			
			'>
		
		<!--- OU SI LE  COLLABORATEUR A UNE SEULE LIGNE --->
		<cfelse>
		
			<cfloop query="SESSION.queryMesTerminaux">
				<cfset currentRow = SESSION.queryMesTerminaux.currentrow>
				
				<cfif SESSION.queryMesTerminaux['IDSOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['SOUS_TETE'][currentRow] neq "" && 
						SESSION.queryMesTerminaux['IDTYPE_EQUIPEMENT'][currentRow] eq 70 >
						
					<cfset typeMod = SESSION.queryMesTerminaux["T_MODELE"][currentRow] >
					<cfset t_imei = "ref. du terminal : "&SESSION.queryMesTerminaux["T_IMEI"][currentRow] >
					
				</cfif>
			</cfloop>
			
			<cfif typeMod eq ""> <cfset typeMod = "Aucun terminal associé"> </cfif>
			<cfset monTerminal = '<div align="center" class="divMonTerminalCombo" style="">
									<span style="font-size:14px;font-weight:bold;color:##009fe3;">#typeMod#</span><br/>
									<span style="font-size:10px;font-weight:bold;color:##009fe3;">#t_imei#</span>
								  </div>
			'>
			
		</cfif>
		
		<cfset monTerminal &= '
						<div class="" style="margin-bottom:50px;border:1px solid ##eee;margin-top:44px;"> <!--- 14px --->
							<table class="tableIMGMonTerminal" width="100%" >
								<tr>
									<td align="center">#image#</td>	
								</tr>
							</table>
						</div>
			'>
		
		<cfreturn monTerminal >
	</cffunction>
	
</cfcomponent>