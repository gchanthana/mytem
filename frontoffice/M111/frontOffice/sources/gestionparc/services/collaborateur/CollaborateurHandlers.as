package gestionparc.services.collaborateur
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.utils.gestionparcMessages;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Envoie les données reçues à la classe <code>CollaborateurDatas</code>,
	 * au retour des procédures appelées dans <code>CollaborateurServices</code>.
	 * 
	 * Auteur	: Guiou Nicolas
	 * Version 	: 1.0
	 * Date 	: 15.07.2010
	 * 
	 * </pre></p>
	 * 
	 * @see gestionparc.services.collaborateur.CollaborateurDatas
	 * @see gestionparc.services.collaborateur.CollaborateurServices
	 * 
	 */
	public class CollaborateurHandlers
	{
		// VARIABLES------------------------------------------------------------------------------
		
		private var myDatas : CollaborateurDatas;

		// VARIABLES------------------------------------------------------------------------------

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Instancie les datas.
		 * </pre></p>
		 * 
		 * @param instanceOfDatas:<code>CollaborateurDatas</code>.
		 */	
		public function CollaborateurHandlers(instanceOfDatas:CollaborateurDatas)
		{
			myDatas = instanceOfDatas;
		}

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		// INTERNALS FUNCTIONS------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getInfosFicheCollaborateur()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatDataGetInfosFicheCollaborateur()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getInfosFicheCollaborateur()
		 * @see gestionparc.services.collaborateur.CollaborateurDatas#treatDataGetInfosFicheCollaborateur()
		 * 
		 */	
		internal function getInfosFicheCollaborateurResultHandler(re:ResultEvent):void
		{
			if((re.result is Array) && (re.result as Array)[0] == 1)
			{
				myDatas.treatDataGetInfosFicheCollaborateur(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_FICHE_COLLAB_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>setInfosFicheCollaborateur()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatSetInfosFicheCollaborateur()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.collaborateur.CollaborateurServices#setInfosFicheCollaborateur()
		 * @see gestionparc.services.collaborateur.CollaborateurDatas#treatSetInfosFicheCollaborateur()
		 * 
		 */	
		internal function setInfosFicheCollaborateurResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if((re.result as Array)[0]==-5)
				{
					ConsoviewAlert.afficherError(
						gestionparcMessages.MATRICULE_EXISTE+' ('+(re.result as Array)[1]+' '+(re.result as Array)[2]+')',
						gestionparcMessages.ERREUR);
				}
				else
				{
					myDatas.treatSetInfosFicheCollaborateur();
				}
				
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.UPDATE_FICHE_COLLAB_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function setInfosManagerFicheCollaborateurResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				if((re.result as Array)[0]==-5)
				{
					ConsoviewAlert.afficherError(
						gestionparcMessages.MATRICULE_EXISTE+' ('+(re.result as Array)[1]+' '+(re.result as Array)[2]+')',
						gestionparcMessages.ERREUR);
				}
				else
				{
					myDatas.treatSetInfosManagerFicheCollaborateur();
				}
				
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.UPDATE_FICHE_COLLAB_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>addNewCollaborateur()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatAddNewCollaborateur()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.collaborateur.CollaborateurServices#addNewCollaborateur()
		 * @see gestionparc.services.collaborateur.CollaborateurDatas#treatAddNewCollaborateur()
		 * 
		 */	
		internal function addNewCollaborateurResultHandler(re:ResultEvent):void
		{
			if(re.result
				&& re.result is Array
				&& (re.result as Array).length > 0)
			{
				if(re.result[0] > -1)
					myDatas.treatAddNewCollaborateur();
				else
				{
					switch(re.result[0])
					{
						case -5 :
							ConsoviewAlert.afficherError(
								gestionparcMessages.ADD_NEW_COLLAB_IMPOSSIBLE_CAUSE_5,
								gestionparcMessages.ERREUR);
							break;
						default :
							ConsoviewAlert.afficherError(
								gestionparcMessages.ADD_NEW_COLLAB_IMPOSSIBLE,
								gestionparcMessages.ERREUR);
							break;
					}
				}
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ADD_NEW_COLLAB_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		internal function initDepartEntrepriseResultHandler(re:ResultEvent):void
		{
			if(re.result && re.result != -1)
			{
				myDatas.treatinitDepartEntreprise(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ADD_NEW_COLLAB_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		internal function validerDepartEntrepriseResultHandler(re:ResultEvent):void
		{
			if(re.result > -1)
			{
				myDatas.treatvaliderDepartEntreprise(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.ADD_NEW_COLLAB_IMPOSSIBLE,
					gestionparcMessages.ERREUR);
			}
		}
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getListLigneSimEqptFromCollab()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetListLigneSimEqptFromCollab()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getListLigneSimEqptFromCollab()
		 * @see gestionparc.services.collaborateur.CollaborateurDatas#treatGetListLigneSimEqptFromCollab()
		 * 
		 */	
		internal function getListLigneSimEqptFromCollabResultHandler(re:ResultEvent):void
		{
			if(re.result[0] > 0)
			{
				myDatas.treatGetListLigneSimEqptFromCollab(re.result[1]);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_INFOS_LIGNE_SIM_TERM_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getListEqptFromCollab()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetListEqptFromCollab()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getListEqptFromCollab()
		 * @see gestionparc.services.collaborateur.CollaborateurDatas#treatGetListEqptFromCollab()
		 * 
		 */	
		internal function getListEqptFromCollabResultHandler(re:ResultEvent):void
		{
			if(re.result[0] > 0)
			{
				myDatas.treatGetListEqptFromCollab(re.result[1]);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_INFOS_EQPT_IMP,
					gestionparcMessages.ERREUR);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Fonction appellée au retour de la fonction <code>getMatriculeAuto()</code> (appel de procédure).
		 * Si le retour est OK, les données retournées sont traitées avec la fonction <code>treatGetMatriculeAuto()</code>.
		 * Si le retour est KO, un message d'erreur est affiché.
		 * </pre></p>
		 * 
		 * @param re:<code>ResultEvent</code>.
		 * @return void.
		 * 
		 * @see gestionparc.services.collaborateur.CollaborateurServices#getMatriculeAuto()
		 * @see gestionparc.services.collaborateur.CollaborateurDatas#treatGetMatriculeAuto()
		 * 
		 */	
		internal function getMatriculeAutoResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatGetMatriculeAuto(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(
					gestionparcMessages.RECUP_MATRICULE_AUTO,
					gestionparcMessages.ERREUR);
			}
		}
		
		internal function getManagersResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				myDatas.treatGetManagers(re.result);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111','aucun_contenu'),	gestionparcMessages.ERREUR);
			}
		}
		
		internal function getValeursResultHandlers(revt:ResultEvent):void
		{
			if(revt.result)
			{
				myDatas.treatGetValeursChampPerso(revt.result);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111','aucun_contenu'),	gestionparcMessages.ERREUR);
			}	
		}
	}
}
