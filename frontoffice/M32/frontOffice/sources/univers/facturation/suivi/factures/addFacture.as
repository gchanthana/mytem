package univers.facturation.suivi.factures
{
    import composants.Format;
    import composants.tb.periode.AMonth;
    import composants.util.ConsoviewFormatter;
    import composants.util.ConsoviewUtil;
    import composants.util.DateFunction;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.controls.TextInput;
    import mx.controls.dataGridClasses.DataGridColumn;
    import mx.events.CalendarLayoutChangeEvent;
    import mx.events.CloseEvent;
    import mx.events.DataGridEvent;
    import mx.events.FlexEvent;
    import mx.resources.ResourceManager;
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    
    import univers.facturation.suivi.factures.event.FactureEvent;
    import univers.facturation.suivi.factures.services.FactureServices;

    public class addFacture extends addFactureIHM
    {
        private var _fctLines:ArrayCollection;
        private var _isFavorite:Boolean;
        private var _factureServices:FactureServices;
        private var _listProduit:ArrayCollection;
        private var _listProduitFavori:ArrayCollection;

        public function addFacture()
        {
            this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
            //super();
        }

        protected function initIHM(event:Event):void
        {
            _fctLines = new ArrayCollection();
            listProduit = new ArrayCollection();
            listProduitFavori = new ArrayCollection();
            dgItemFacture.dataProvider = _fctLines;
            // initialisation du service et pose d'écouteur
            factureServices = new FactureServices();
            fillProduct();
            fillOrganisation();
            fillTypePaiement();
            cmbOperateur.addEventListener(Event.CHANGE, organisationChange);
            dgCF.addEventListener(Event.CHANGE, onCFSelectionChange);
            dgItemFacture.addEventListener(DataGridEvent.ITEM_EDIT_BEGINNING, initTotal);
            dgItemFacture.addEventListener(DataGridEvent.ITEM_EDIT_END, calculTotal);
            dgItemFacture.allowMultipleSelection = true;
            dgItemFacture.doubleClickEnabled = true;
            dgItemFacture.addEventListener(MouseEvent.DOUBLE_CLICK, dgItemFactureDoubleClickHandler);
            dgProducts.addEventListener(MouseEvent.DOUBLE_CLICK, onGridDbClick);
            // formattage des grids
            dgItemFacture.columns[0].labelFunction = phoneFormat;
            dgItemFacture.columns[2].labelFunction = numberFormat;
            dgItemFacture.columns[3].labelFunction = monnaieFormat;
            dgItemFacture.columns[4].labelFunction = monnaieFormat;
            dgItemFacture.columns[5].labelFunction = numberFormat;
            dgItemFacture.columns[6].labelFunction = monnaieFormat;
            dgItemFacture.columns[8].labelFunction = monnaieFormat;
            // ecouteur sur la liste des produits
            dgProducts.columns[3].labelFunction = monnaieFormat;
            dgProducts.addEventListener(FactureEvent.THIS_IS_FAVORITE, onFavoriteHandler);
            dgProducts.addEventListener(FactureEvent.THIS_IS_NOT_FAVORITE, onFavoriteHandler);
            gdrLignes.columns[1].labelFunction = phoneFormat;
            //
            btValid.addEventListener(MouseEvent.CLICK, onbtValidClick);
            btEmpty.addEventListener(MouseEvent.CLICK, emptyItemGrid);
            btnMail.addEventListener(MouseEvent.CLICK, openMailPanel);
            btSupp.addEventListener(MouseEvent.CLICK, btSuppClickHandler);
            // Filtrage des grids
            txtFiltreCF.addEventListener(Event.CHANGE, filtreGrid);
            txtFiltreLignes.addEventListener(Event.CHANGE, filtreGrid);
            txtFiltreProduits.addEventListener(Event.CHANGE, filtreGrid);
            txtNumeroFct.addEventListener(Event.CHANGE, enableCreation);
            dcDate.addEventListener(CalendarLayoutChangeEvent.CHANGE, dcDateChange);
            parentDocument.addListener();
            // ecouteur sur les labels de favoris
            lblFavoris.addEventListener(MouseEvent.CLICK, lblFavorisClickHandler);
            imgFav.addEventListener(MouseEvent.CLICK, lblFavorisClickHandler);
            lblTout.addEventListener(MouseEvent.CLICK, lblToutClickHandler);
            lblFavorisUnderline.visible = false;
        }

        // Initialise les dates
        internal function setDate(moisdeb:String, moisfin:String):void
        {
            var dateDeb:Date = new Date(moisdeb.substr(6, 4), parseInt(moisdeb.substr(3, 2), 10) - 1, moisdeb.substr(0, 2));
            var dateFin:Date = new Date(moisfin.substr(6, 4), parseInt(moisfin.substr(3, 2), 10) - 1, moisfin.substr(0, 2));
            dcDate.selectedDate = new Date(dateFin.getFullYear(), dateFin.getMonth() + 1, dateFin.getDay() + 3);
            dcEchance.selectedDate = new Date(dateFin.getFullYear(), dateFin.getMonth() + 1, dateFin.getDay() + 10);
            dcDateDeb.selectedDate = dateDeb;
            dcDateEnd.selectedDate = dateFin;
        }

        private function dcDateChange(clc:CalendarLayoutChangeEvent):void
        {
            if (dcDate.selectedDate != null)
            {
                var aMonth:AMonth = new AMonth(new Date(dcDate.selectedDate.getFullYear(), dcDate.selectedDate.getMonth() - 1));
                dcEchance.selectedDate = new Date(dcDate.selectedDate.getFullYear(), dcDate.selectedDate.getMonth(), dcDate.selectedDate.getDate() + 5);
                dcDateDeb.selectedDate = aMonth.dateDebut;
                dcDateEnd.selectedDate = aMonth.dateFin;
            }
        }

        private function onProduitLoadedHandler(evt:FactureEvent):void
        {
            factureServices.myHandlers.removeEventListener(FactureEvent.LISTED_PRODUIT_LOADED, onProduitLoadedHandler);
            if (_isFavorite)
            {
                dgProducts.dataProvider = factureServices.myDatas.listeProductFavoris;
            }
            else
            {
                dgProducts.dataProvider = factureServices.myDatas.listeProduct;
            }
            dgProducts.invalidateDisplayList();
            dgProducts.dataProvider.refresh();
        }

        //AFFICHE LES PRODUITS FAVORIS
        protected function lblFavorisClickHandler(evt:MouseEvent):void
        {
            _isFavorite = true;
            lblFavoris.useHandCursor = false;
            lblFavoris.buttonMode = false;
            lblFavoris.mouseChildren = true;
            lblTout.useHandCursor = true;
            lblTout.buttonMode = true;
            lblTout.mouseChildren = false;
            lblToutUnderline.visible = true;
            lblFavorisUnderline.visible = false;
            fillProduct();
        }

        //AFFICHE TOUS LES PRODUITS
        protected function lblToutClickHandler(evt:MouseEvent):void
        {
            _isFavorite = false;
            lblTout.useHandCursor = false;
            lblTout.buttonMode = false;
            lblTout.mouseChildren = true;
            lblFavoris.useHandCursor = true;
            lblFavoris.buttonMode = true;
            lblFavoris.mouseChildren = false;
            lblToutUnderline.visible = false;
            lblFavorisUnderline.visible = true;
            fillProduct();
        }

        public function setSelectedPeriode(moisDebut:String, moisFin:String):void
        {
            var formMoisD:String = moisDebut.substr(0, 2) + " " + DateFunction.moisEnLettre(parseInt(moisDebut.substr(3, 2), 10) - 1) + " " + moisDebut.substr(6, 4);
            var formMoisF:String = moisFin.substr(0, 2) + " " + DateFunction.moisEnLettre(parseInt(moisFin.substr(3, 2), 10) - 1) + " " + moisFin.substr(6, 4);
            ///////////////// faire la suite 
            //myPeriodeLbl.text = "Du  " + formMoisD + " au " + formMoisF;					
        }

        private function onFavoriteHandler(evt:FactureEvent):void
        {
            switch (evt.type)
            {
                // cas : le produit cliqué est ajouté au favori
                case FactureEvent.THIS_IS_FAVORITE:
                    factureServices.ajouterProduitAMesFavoris(dgProducts.selectedItem);
                    break;
                // cas : le produit est supprimé des favoris
                case FactureEvent.THIS_IS_NOT_FAVORITE:
                    factureServices.supprimerProduitDeMesFavoris(dgProducts.selectedItem);
                    break;
                default:
                    break;
            }
            fillProduct();
        }

        //filtre les elemenents du tableau en fonction du filtre
        private function enableCreation(ev:Event):void
        {
            if (txtNumeroFct.text.length > 0 && _fctLines.length > 0)
            {
                btValid.enabled = true;
            }
            else
            {
                btValid.enabled = false;
            }
        }

        //filtre les elemenents du tableau en fonction du filtre
        private function filtreGrid(ev:Event):void
        {
            if (ev.currentTarget.id == "txtFiltreCF")
            {
                if (dgCF.dataProvider != null)
                {
                    dgCF.dataProvider.filterFunction = filtreCF;
                    dgCF.dataProvider.refresh();
                }
            }
            else if (ev.currentTarget.id == "txtFiltreLignes")
            {
                if (gdrLignes.dataProvider != null)
                {
                    gdrLignes.dataProvider.filterFunction = filtreLignes;
                    gdrLignes.dataProvider.refresh();
                }
            }
            else if (ev.currentTarget.id == "txtFiltreProduits")
            {
                if (dgProducts.dataProvider != null)
                {
                    dgProducts.dataProvider.filterFunction = filtreProduits;
                    dgProducts.dataProvider.refresh();
                }
            }
        }

        //filtre les elemenents du tableau en fonction du filtre
        private function filtreCF(item:Object):Boolean
        {
            if (item.COMPTE_FACTURATION.toLowerCase().search(txtFiltreCF.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        //filtre les elemenents du tableau en fonction du filtre
        private function filtreLignes(item:Object):Boolean
        {
            if (item.SOUS_TETE.toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1 || item.LIBELLE_TYPE_LIGNE.toLowerCase().search(txtFiltreLignes.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        //filtre les elemenents du tableau en fonction du filtre
        private function filtreProduits(item:Object):Boolean
        {
            if (item.LIBELLE_PRODUIT.toLowerCase().search(txtFiltreProduits.text.toLowerCase()) != -1 || item.PRIX_UNIT.toString().toLowerCase().search(txtFiltreProduits.text.toLowerCase()) != -1 || item.LIBELLE.toLowerCase().search(txtFiltreProduits.text.toLowerCase()) != -1)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        protected function initTotal(event:DataGridEvent):void
        {
            if (event.columnIndex == 2)
            {
                dgItemFacture.columns[2].labelFunction = null;
            }
            else if (event.columnIndex == 3)
            {
                dgItemFacture.columns[3].labelFunction = null;
            }
            else if (event.columnIndex == 4)
            {
                dgItemFacture.columns[4].labelFunction = null;
            }
            else if (event.columnIndex == 5)
            {
                dgItemFacture.columns[5].labelFunction = null;
            }
            else if (event.columnIndex == 6)
            {
                dgItemFacture.columns[6].labelFunction = null;
            }
            else if (event.columnIndex == 8)
            {
                dgItemFacture.columns[8].labelFunction = null;
            }
        }

        protected function calculTotal(event:DataGridEvent):void
        {
            var newData:Number = Number(parseFloat(TextInput(dgItemFacture.itemEditorInstance).text).toFixed(2));
            if (event.dataField == "QTE")
            {
                dgItemFacture.dataProvider[event.rowIndex].TOTAL = newData * dgItemFacture.dataProvider[event.rowIndex].PRIX_UNIT;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE = dgItemFacture.dataProvider[event.rowIndex].TOTAL - dgItemFacture.dataProvider[event.rowIndex].TOTAL * dgItemFacture.dataProvider[event.rowIndex].PCT_REMISE / 100;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_TTC = Number(dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE * (1 + dgItemFacture.dataProvider[event.rowIndex].TVA / 100)).toFixed(2);
                dgItemFacture.columns[2].labelFunction = numberFormat;
            }
            else if (event.dataField == "PRIX_UNIT")
            {
                dgItemFacture.dataProvider[event.rowIndex].TOTAL = newData * dgItemFacture.dataProvider[event.rowIndex].QTE;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE = dgItemFacture.dataProvider[event.rowIndex].TOTAL - dgItemFacture.dataProvider[event.rowIndex].TOTAL * dgItemFacture.dataProvider[event.rowIndex].PCT_REMISE / 100;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_TTC = Number(dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE * (1 + dgItemFacture.dataProvider[event.rowIndex].TVA / 100)).toFixed(2);
                dgItemFacture.columns[3].labelFunction = numberFormatPrec4;
            }
            else if (event.dataField == "PCT_REMISE")
            {
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE = dgItemFacture.dataProvider[event.rowIndex].TOTAL - (newData / 100) * dgItemFacture.dataProvider[event.rowIndex].TOTAL;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_TTC = Number(dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE * (1 + dgItemFacture.dataProvider[event.rowIndex].TVA / 100)).toFixed(2);
                dgItemFacture.columns[5].labelFunction = monnaieFormat;
            }
            else if (event.dataField == "TOTAL")
            {
                dgItemFacture.dataProvider[event.rowIndex].TOTAL = newData;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE = dgItemFacture.dataProvider[event.rowIndex].TOTAL - dgItemFacture.dataProvider[event.rowIndex].TOTAL * dgItemFacture.dataProvider[event.rowIndex].PCT_REMISE / 100;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_TTC = Number(dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE * (1 + dgItemFacture.dataProvider[event.rowIndex].TVA / 100)).toFixed(2);
                dgItemFacture.dataProvider[event.rowIndex].PRIX_UNIT = dgItemFacture.dataProvider[event.rowIndex].TOTAL / Number(dgItemFacture.dataProvider[event.rowIndex].QTE);
                dgItemFacture.columns[4].labelFunction = numberFormat;
            }
            else if (event.dataField == "TOTAL_REMISE")
            {
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE = newData;
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_TTC = Number(dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE * (1 + dgItemFacture.dataProvider[event.rowIndex].TVA / 100)).toFixed(2);
                dgItemFacture.columns[6].labelFunction = monnaieFormat;
            }
            else if (event.dataField == "TOTAL_TTC")
            {
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_TTC = Number(newData).toFixed(2);
                dgItemFacture.dataProvider[event.rowIndex].TOTAL_REMISE = Number(dgItemFacture.dataProvider[event.rowIndex].TOTAL_TTC / (1 + dgItemFacture.dataProvider[event.rowIndex].TVA / 100)).toFixed(2);
                dgItemFacture.columns[8].labelFunction = monnaieFormat;
            }
        }

        protected function emptyItemGrid(event:Event):void
        {
            dgItemFacture.dataProvider = null;
            _fctLines.removeAll();
            btEmpty.enabled = false;
            btValid.enabled = false;
            cmbOperateur.enabled = true;
            dgCF.selectable = true;
        }

        protected function organisationChange(event:Event):void
        {
            fillCF();
            gdrLignes.dataProvider = null;
            //dgProducts.dataProvider = null;
            fillProduct();
        }

        protected function onCFSelectionChange(event:Event):void
        {
            fillLines();
            fillProduct();
        }

        protected function onGridDbClick(event:MouseEvent):void
        {
            var currentLine:Array = gdrLignes.selectedItems;
            if (currentLine.length > 0)
            {
                ajouterLaSelectionALaFacture(currentLine);
            }
            else
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Veuillez_choisir_une_ligne_pour_ins_rer_'));
            }
            enableCreation(new Event(""));
        }

        //ajoute la selection a la facture
        private function ajouterLaSelectionALaFacture(source:Array):void
        {
            var doFunction:Function = function(cle:CloseEvent):void
                {
                    switch (cle.detail)
                    {
                        //Si on choisit de poursuivre
                        case Alert.YES:
                        {
                            ajouterAvecLesDoublons(source);
                            trace("ok");
                            break;
                        }
                        //Si on choisit de ne pas ajouter des doublons 
                        case Alert.NO:
                        {
                            ajouterSansDoublons(source);
                            trace("no");
                            break;
                        }
                        //Si on choisit d'annuler
                        case Alert.CANCEL:
                        {
                            trace("cancel");
                            break;
                        }
                        //par defaut on annule
                        default:
                        {
                            trace("cancel");
                            break;
                        }
                    }
                }
            Alert.cancelLabel = ResourceManager.getInstance().getString('M32', 'Annuler');
            Alert.noLabel = ResourceManager.getInstance().getString('M32', 'Non');
            Alert.yesLabel = ResourceManager.getInstance().getString('M32', 'Oui');
            if (yatiledesDoublons(source))
            {
                Alert.show(ResourceManager.getInstance().getString('M32', 'Certains_des_produits_s_l_ctionn_s_sont_'), "Attention", Alert.YES | Alert.NO | Alert.CANCEL, this, doFunction);
            }
            else
            {
                ajouterAvecLesDoublons(source);
            }
            if (dgItemFacture.dataProvider)
            {
                dgItemFacture.dataProvider = _fctLines;
                dgItemFacture.dataProvider.refresh();
                dgCF.selectable = false;
                btEmpty.enabled = true;
                cmbOperateur.enabled = false;
            }
        }

        //Verification des doublons
        private function yatiledesDoublons(source:Array):Boolean
        {
            for (var i:int = 0; i < source.length; i++)
            {
                if (ConsoviewUtil.is2InArray(source[i].IDSOUS_TETE.toString(), dgProducts.selectedItem.IDPRODUIT_CLIENT.toString(), "IDSOUS_TETE", "IDPRODUIT_CLIENT", dgItemFacture.dataProvider.source))
                {
                    return true;
                }
            }
            return false;
        }

        //Ajoute la selection à la facture sans tenir colmpte des doublons
        private function ajouterAvecLesDoublons(source:Array):void
        {
            for (var i:int = 0; i < source.length; i++)
            {
                ajouterItemToFacture(source[i]);
            }
        }

        //Ajoute la selection à la facture en éliminant les doublons
        private function ajouterSansDoublons(source:Array):void
        {
            for (var i:int = 0; i < source.length; i++)
            {
                if (!ConsoviewUtil.is2InArray(source[i].IDSOUS_TETE.toString(), dgProducts.selectedItem.IDPRODUIT_CLIENT.toString(), "IDSOUS_TETE", "IDPRODUIT_CLIENT", dgItemFacture.dataProvider.source))
                {
                    ajouterItemToFacture(source[i])
                }
            }
        }

        //Ajoute un Item à la facture
        private function ajouterItemToFacture(sourceObject:Object):void
        {
            var newline:Object = new Object();
            newline.IDSOUS_TETE = sourceObject.IDSOUS_TETE.toString();
            newline.IDST_OP = sourceObject.IDST_OP;
            newline.SOUS_TETE = sourceObject.SOUS_TETE;
            newline.TVA = dgProducts.selectedItem.TVA;
            newline.QTE = 1;
            newline.TOTAL = dgProducts.selectedItem.PRIX_UNIT;
            newline.TOTAL_REMISE = dgProducts.selectedItem.PRIX_UNIT;
            newline.TOTAL_TTC = (newline.TOTAL_REMISE * (1 + (newline.TVA / 100))).toFixed(2);
            newline.PRIX_UNIT = dgProducts.selectedItem.PRIX_UNIT;
            newline.LIBELLE_PRODUIT = dgProducts.selectedItem.LIBELLE_PRODUIT;
            newline.IDPRODUIT_CLIENT = dgProducts.selectedItem.IDPRODUIT_CLIENT.toString();
            newline.PCT_REMISE = 0;
            _fctLines.addItem(newline);
            _fctLines.refresh();
        }

        //supprime la selection de la facture
        private function supprimerLaSeleection():void
        {
            if (dgItemFacture.selectedIndices.length > 0)
            {
                var forEcheCallBackFunction:Function = function(element:*, index:Number, arr:Array):void
                    {
                        (dgItemFacture.dataProvider as ArrayCollection).removeItemAt((dgItemFacture.dataProvider as ArrayCollection).getItemIndex(element));
                    }
                dgItemFacture.selectedItems.forEach(forEcheCallBackFunction);
                (dgItemFacture.dataProvider as ArrayCollection).refresh();
            }
        }

        protected function dgItemFactureDoubleClickHandler(me:MouseEvent):void
        {
            supprimerLaSeleection()
        }

        private function btSuppClickHandler(me:MouseEvent):void
        {
            supprimerLaSeleection();
        }

        protected function onbtValidClick(event:MouseEvent):void
        {
            //traite le resultat de l'appel distant
            var resultHandler:Function = function createNewInvoiceResult(event:ResultEvent):void
                {
                    if (event.result == -1)
                    {
                        Alert.show(ResourceManager.getInstance().getString('M32', 'Probl_me_lors_de_la_cr_ation_'));
                    }
                    else
                    {
                        Alert.show(ResourceManager.getInstance().getString('M32', 'La_facture_a__t__cr_e_avec_succ_s_'));
                    }
                }
            //Effectué si confirmation
            var traitement:Function = function confirmerAjoutFacture(cle:CloseEvent):void
                {
                    if (cle.detail == Alert.YES)
                    {
                        var idCompte:String = getSelectedCFItemValue().toString();
                        var numero_fact:String = txtNumeroFct.text;
                        var dateDeb:String = dcDateDeb.text;
                        var dateEnd:String = dcDateEnd.text;
                        var commentaire:String = txtComment.text;
                        var echeance:String = dcEchance.text;
                        var date_emission:String = dcDate.text;
                        var operateurId:String = getSelectedOperateur().toString();
                        var type_paiement:String;
                        if (cmbTypePaiement.selectedItem != null)
                            type_paiement = cmbTypePaiement.selectedItem.DATA;
                        if (checkFacture(numero_fact, dateDeb, dateEnd, echeance, date_emission, operateurId, type_paiement))
                        {
                            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.addFacture", "createNewInvoice", resultHandler);
                            RemoteObjectUtil.callService(op, idCompte, numero_fact, dateDeb, dateEnd, commentaire, echeance, date_emission, operateurId, type_paiement, _fctLines.source);
                        }
                    }
                }
            Alert.yesLabel = ResourceManager.getInstance().getString('M32', 'Oui');
            Alert.cancelLabel = ResourceManager.getInstance().getString('M32', 'Annuler');
            Alert.show(ResourceManager.getInstance().getString('M32', 'Etes_vous_sur_de_vouloir_faire_cette_op_'), ResourceManager.getInstance().getString('M32', 'Confirmer_la_cr_ation_de_la_facture'), Alert.YES | Alert.CANCEL, this, traitement);
        }

        private function checkFacture(numero_fact:String, dateDeb:String, dateEnd:String, echeance:String, date_emission:String, operateurId:String, type_paiement:String):Boolean
        {
            var message:String = "\n";
            trace(message.length);
            if (!numero_fact)
            {
                message = message + ResourceManager.getInstance().getString('M32', '__Num_ro_de_facture_n');
            }
            if (!date_emission)
            {
                message = message + ResourceManager.getInstance().getString('M32', '__Date_d__mission_de_la_facture_n');
            }
            trace(Number(operateurId));
            if (Number(operateurId) < 1)
            {
                message = message + ResourceManager.getInstance().getString('M32', '__L_op_rateur_n');
            }
            if (!type_paiement)
            {
                message = message + ResourceManager.getInstance().getString('M32', '__Le_type_de_paiement_n');
            }
            if (!echeance)
            {
                message = message + ResourceManager.getInstance().getString('M32', '__Date_d__ch_ance_de_la_facture_n');
            }
            if (!dateDeb)
            {
                message = message + ResourceManager.getInstance().getString('M32', '__Date_de_d_but_de_p_riode_de_facturatio');
            }
            if (!dateEnd)
            {
                message = message + ResourceManager.getInstance().getString('M32', '__Date_de_fin_de_p_riode_de_facturation_');
            }
            if (message.length > 1)
            {
                Alert.show(message, ResourceManager.getInstance().getString('M32', 'Erreur___les_champs_suivants_sont_obliga'));
                return false;
            }
            else
            {
                return true;
            }
        }

        private function fillOrganisation():void
        {
            var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.Facture.ListeFactureGroupeStrategy", "getListeOperateur", fillOrganisationResult, null);
            RemoteObjectUtil.callService(op, [ idGroupeMaitre ]);
        }

        private function fillOrganisationResult(r:ResultEvent):void
        {
            //var organisationData:ArrayCollection = ArrayCollection(r.result);
            //orgaStructure.setCmbData(_organisationData, "LIBELLE_GROUPE_CLIENT");
            cmbOperateur.dataProvider = r.result;
            //cmbOperateur.dataProvider.filterFunction = filterDataprovider;
            //cmbOperateur.dataProvider.refresh();
            fillCF();
        }

        /*private function filterDataprovider(item:Object):Boolean
        {
            try
            {
                if (item.TYPE_ORGA == "OPE")
                    return true;
                return false;
            }
            catch (error:Error)
            {
            }
            return true;
        }*/

        private function fillCF():void
        {
            if (cmbOperateur.selectedIndex != -1)
            {
                var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.addFacture", "getCF", fillCFResult, null);
                RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX, cmbOperateur.selectedItem.data);
            }
            else
            {
                dgItemFacture.dataProvider = null;
                dgCF.dataProvider = null;
                _fctLines.removeAll();
                btEmpty.enabled = false;
                btValid.enabled = false;
                cmbOperateur.enabled = true;
                dgCF.selectable = true;
            }
        }

        private function fillCFResult(event:ResultEvent):void
        {
            //var treeNodes:XMLList = XML(event.result).children().children();
            //treePerimetre.dataProvider = treeNodes;
            //treePerimetre.labelField = "@LABEL";
            dgCF.dataProvider = event.result;
        }

        private function fillTypePaiement():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.addFacture", "getTypePaiement", fillTypePaiementResult, null);
            RemoteObjectUtil.callService(op);
        }

        private function fillTypePaiementResult(event:ResultEvent):void
        {
            cmbTypePaiement.dataProvider = event.result;
        }

        private function fillLines():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.addFacture", "getAllLines", fillLinesResult, null);
            RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX, getSelectedOperateur(), getSelectedCFItemValue());
        }

        private function fillLinesResult(event:ResultEvent):void
        {
            gdrLignes.dataProvider = event.result;
        }

        // récupère la liste de tous les produits d'un groupe d'opérateur en fonction du compte facturation
        private function fillProduct():void
        {
            factureServices.myHandlers.addEventListener(FactureEvent.LISTED_PRODUIT_LOADED, onProduitLoadedHandler);
            factureServices.fournirListeProduit(getSelectedOperateur());
        }

        // OLD récupère la liste de tous les produits d'un groupe d'opérateur en fonction du compte facturation
        /*private function _fillProduct():void
        {
            var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.facturation.suiviFacturation.addFacture", "getProduit", fillProductResult, null);
            RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX, getSelectedCFItemValue(), getSelectedOperateur());
        }*/

        /*private function fillProductResult(event:ResultEvent):void
        {
            dgProducts.dataProvider = setFavori(event.result as ArrayCollection);
        }*/

        /*private function fillAllProductResult(event:ResultEvent):void
        {
            if (event.result)
            {
                dgProducts.dataProvider = setFavori(event.result as ArrayCollection);
            }
        }*/

        /*private function setFavori(collec:ArrayCollection):ArrayCollection
        {
            // Ajout d'une propriété favori initialisé à 0 = non favori)
            for (var i:int = 0; i < collec.length; i++)
            {
                collec.getItemAt(i).FAVORI = 0;
            }
            return collec;
        }*/

        public function getSelectedOrganisationValue():Number
        {
            if (cmbOperateur.selectedIndex != -1)
            {
                return cmbOperateur.selectedItem.IDGROUPE_CLIENT;
            }
            else
            {
                return -1
            }
        }

        public function getSelectedOperateur():Number
        {
            if (cmbOperateur.selectedIndex != -1)
            {
                return cmbOperateur.selectedItem.data;
            }
            else
            {
                return -1;
            }
        }

        public function getSelectedCFItemValue():Number
        {
            if (dgCF.selectedIndex != -1)
            {
                return dgCF.selectedItem.IDCOMPTE_FACTURATION;
            }
            else
            {
                return -1;
            }
        }

        public function getSelectedLinesLabel():String
        {
            if (gdrLignes.selectedIndex != -1)
            {
                return gdrLignes.selectedItem.SOUS_TETE;
            }
            else
            {
                return "";
            }
        }

        public function getSelectedLine():int
        {
            if (gdrLignes.selectedIndex != -1)
            {
                return gdrLignes.selectedItem.IDSOUS_TETE;
            }
            else
            {
                return 0;
            }
        }

        //formate le numéro de téléphone dans les datagrids
        private function phoneFormat(item:Object, column:DataGridColumn):String
        {
            if (item[column.dataField].length == 10)
            {
                return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);
            }
            else
            {
                return item[column.dataField];
            }
        }

        private function numberFormat(item:Object, column:DataGridColumn):String
        {
            return ConsoviewFormatter.formatNumber(item[column.dataField], 1);
        }

        private function monnaieFormat(item:Object, column:DataGridColumn):String
        {
            return Format.toCurrencyString(item[column.dataField], 4);
        }

		private function numberFormatPrec4(item:Object, column:DataGridColumn):String
		{
			return Format.toNumber(item[column.dataField], 4);
		}
		
        private function openMailPanel(me:MouseEvent):void
        {
        }

        public function get listProduit():ArrayCollection
        {
            return _listProduit;
        }

        public function set listProduit(value:ArrayCollection):void
        {
            _listProduit = value;
        }

        public function get factureServices():FactureServices
        {
            return _factureServices;
        }

        public function set factureServices(value:FactureServices):void
        {
            _factureServices = value;
        }

        public function get listProduitFavori():ArrayCollection
        {
            return _listProduitFavori;
        }

        public function set listProduitFavori(value:ArrayCollection):void
        {
            _listProduitFavori = value;
        }
    }
}