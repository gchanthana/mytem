package composants.tb.pages
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import composants.tb.tableaux.ITableauModel;
	import composants.tb.tableaux.TableauAboFactoryStratey;
	import composants.tb.tableaux.TableauChangeEvent;
	import composants.tb.tableaux.TableauConsoFactoryStrategy;
	import composants.tb.tableaux.TableauFactory;
	 
	
	public class TbAccueil extends TbAccueil_IHM implements IPage 
	{
		//le segment courrant;
		protected var _segment : String;		
		
		private var lastMoisFin : String = "x";
		
		private var lastMoisDebut : String = "x";				
		
		private var moisDeb : String; 
		 
		private var moisFin : String;	
		
		private var perimetre : String;
		
		private var modeSelection : String;
		
		private var lastIdentifiant : String = "z";
		
		private var identifiant : String;		
		
		private const _pageType : String = "ACCUEIL";
		
		/*----------------------- TABLEAUX ----------------------------*/

		//createur de tableau
		protected var _tf : TableauFactory = new TableauFactory();

		public var _tbAbo : ITableauModel;
		
		public var _tbConso : ITableauModel;
		
		private var _total : Number;
		 
		/*------------------------ GRAPHS -------------------------------*/
		//createur de graph
				
				 
		protected var _nextPageType : String; 
		
		protected var _pageLibelle : String ;
		
		protected var _type : TableauChangeEvent;
		
		protected var _goToNextPage : Function ;
				
		/**
		* Constructeur 
		**/
		public function TbAccueil(type :  TableauChangeEvent = null)
		{
			super();	
			_pageLibelle = ResourceManager.getInstance().getString('M312', 'Accueil');
			_type = type;
			
			addEventListener(FlexEvent.CREATION_COMPLETE,init);					
		}
		
		public function get pageType():String{
			return _pageType;
		}
		
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function get nextPageType():String{
			//TODO: implement function
			return _nextPageType;
		}
		
		/**
		 * Retourne le type de la page la suit
		 * @return type le type de la page suivante
		 * */		
		public function set nextPageType(type : String):void{
			_nextPageType = type;
		}
						
		/**
		* Reset le compossant, remet toutes les variables a 0, interromp les remotings en cours
		**/
		public function clean():void
		{
			//TODO: implement function
		}
		
		public function getLibelle():String
		{
			//TODO: implement function
			return _pageLibelle;
		}
		
		public function getTotal():Number
		{
			//TODO: implement function
			return _total;
		}		
		
		/**
		 * Met à jour le perimtre et le modeSelection
		 * @param perimetre Le perimetre (groupe, groupecompte, groupeSousCompte ...) 			
		 * @param modeSelection Le mode de selection(complet ou partiel)
		 * */
		public function updatePerimetre(perimetre : String = null, modeSelection : String = null, idt : String= null):void
		{			
			this.perimetre = perimetre;
			this.modeSelection = modeSelection;			
			this.identifiant = idt;
		}
		
		
		/**
		 * Permet d'exporter un rapport flash,pdf ou excel 
		 * @param format Le format (flashpaper, excel ou pdf)
		 * */
		public function exporterRapport( format: String, rSociale : String = ""):void
		{
		 	var urlback : String = moduleDashbordPilotageIHM.NonSecureUrlBackoffice+ "/fr/consotel/consoview/M312/pdf/print_document.cfm";	
            
            var _idSegment		:int 	= _type.IDSEGMENT_THEME;
        	var sgmtThem		:String = _type.SEGMENT.toUpperCase();
			var SGMT_FIXE		:String = ResourceManager.getInstance().getString('M312', 'SGMT_FIXE').toUpperCase();
			var SGMT_MOBILE		:String = ResourceManager.getInstance().getString('M312', 'SGMT_MOBILE').toUpperCase();
			var SGMT_DATA		:String = ResourceManager.getInstance().getString('M312', 'SGMT_DATA').toUpperCase();
			
			switch(sgmtThem)
			{
				case SGMT_FIXE 	:  _idSegment = 1;break;
				case SGMT_MOBILE:  _idSegment = 2;break;
				case SGMT_DATA 	:  _idSegment = 3;break;									
			}
            
            var variables :URLVariables = new URLVariables();
	            variables.format 		= format;
	            variables.tb 			= _type.SEGMENT;
	            variables.idsegment 	= _idSegment;
	            variables.datedeb 		= moisDeb; 
	            variables.datefin 		= moisFin;
	            variables.raisonsociale = rSociale;
	            variables.perimetre		= this.perimetre;
	            variables.identifiant	= this.identifiant;
	            variables.modeSelection	= this.modeSelection;
            
            var request:URLRequest = new URLRequest(urlback);
	            request.data = variables;
	            request.method=URLRequestMethod.POST;
            
            navigateToURL(request,"_blank");  
		}
		
		/**
		 * Met à jour la periode
		 * */		 
		public function updatePeriode(moisDeb : String = null, moisFin : String = null):void{
			//TOTO: implement function
			this.moisDeb = moisDeb;
			this.moisFin = moisFin;
		}			
		
		public function updateSegment(segment : String = ""):void
		{
			
			
			
			_type.SEGMENT = segment;
			_segment = segment;
			if (segment.toUpperCase() == "COMPLET"){
				_type.TYPE = "ACCUEIL";
			} else {
				_type.TYPE =  "SEGMENT";
			}			 
		}
		
		
		public function update():void{
			if (lastMoisDebut.concat(lastMoisFin).concat(lastIdentifiant) != moisDeb.concat(moisFin).concat(identifiant)){
				callLater(updateComposants);							
				lastMoisDebut = moisDeb;
				lastMoisFin = moisFin;
				lastIdentifiant = identifiant;
			} 		 	
		}
		 
		//initialisation de l'IHM
		protected function init(fe:FlexEvent):void
		{		
			update(); 
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			_tf.setStrategy(new TableauAboFactoryStratey());
			_tbAbo = _tf.createTableau(_type);			
			UIComponent(_tbAbo).x = 0;
			UIComponent(_tbAbo).y = 0;
			
			_tf.setStrategy(new TableauConsoFactoryStrategy());
			_tbConso = _tf.createTableau(_type);			
			UIComponent(_tbConso).x = 0;
			UIComponent(_tbConso).y = 0;
			
			this.addChildAt(DisplayObject(_tbConso),0);
			this.addChildAt(DisplayObject(_tbAbo),0);				
								
			
			//affectation des Handlers et des listeners			
			UIComponent(_tbAbo).addEventListener("tableauChange",_goToNextPage);			
			UIComponent(_tbConso).addEventListener("tableauChange",_goToNextPage);
			
			UIComponent(_tbConso).addEventListener("totalSet",setTotalHandler);
		}
		
		
		//creation des éléments graphiques
		private function createTableauAccueil():void{
			try{
				this.removeChild(DisplayObject(_tbConso));
				this.removeChild(DisplayObject(_tbAbo));	
			}catch(e : Error){
				trace("les conteneurs ne sont pas encore créés");
			}  
						
			_tf.setStrategy(new TableauAboFactoryStratey());
			_tbAbo = _tf.createTableau(_type);			
			UIComponent(_tbAbo).x = 0;
			UIComponent(_tbAbo).y = 0;
			
			_tf.setStrategy(new TableauConsoFactoryStrategy());
			_tbConso = _tf.createTableau(_type);			
			UIComponent(_tbConso).x = 0;
			UIComponent(_tbConso).y = 0;
			
			this.addChildAt(DisplayObject(_tbAbo),0);				
			this.addChildAt(DisplayObject(_tbConso),0);					
			
			//affectation des Handlers et des listeners			
			UIComponent(_tbAbo).addEventListener("tableauChange",_goToNextPage);			
			UIComponent(_tbConso).addEventListener("tableauChange",_goToNextPage);
			
			UIComponent(_tbConso).addEventListener("totalSet",setTotalHandler);
		}
				
		public function goToNextPageHandler(f : Function):void{			
			_goToNextPage = f;				
		}	
		
		private function updateComposants():void{			
			preUpdateComposants();
			_tbAbo.update();			
		 	_tbConso.update();	
		 	
		}
		
		private function setTotalHandler(e:Event):void
		{
			defineTotal();
		}
		
		private function defineTotal():void
		{
			_total = parseFloat(_tbAbo.total.toString()) + parseFloat(_tbConso.total.toString());
		
			var MyEv :  Event = new Event("tbTotal");
			dispatchEvent(MyEv);
		}
		
		private function preUpdateComposants():void{
			_tbAbo.updatePerimetre(perimetre,modeSelection);		  
		 	_tbConso.updatePerimetre(perimetre,modeSelection);	
		 	
		 	_tbAbo.updatePeriode(moisDeb,moisFin);	  
		 	_tbConso.updatePeriode(moisDeb,moisFin);	 
		 	
		}	
	}
}