package datalert.ihm.fiches.detailsFicheLigne
{
	import datalert.services.DatalertCS;
	
	import mx.containers.Box;
	import mx.events.FlexEvent;

	
	[Bindable]
	public class MessageRendererImpl extends Box
	{
		public var idSousTete:int;
		
		public var datalertCS:DatalertCS;
		
		public function MessageRendererImpl()
		{
			addEventListener(FlexEvent.SHOW, creationCompleteHandler);
		}
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{			
			
			datalertCS= new DatalertCS();
			datalertCS.searchHistoriqueMessage(idSousTete);
			
		}
		
		public function setCurrentData(currentInfos:int):void
		{
			idSousTete = currentInfos;
		}
		
	}
}