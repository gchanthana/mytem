package service.destinataires.associedestinatairenoeud
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	import event.DestinataireEvent;
	import flash.events.EventDispatcher;
	import mx.rpc.events.ResultEvent;
	

	internal class AssocierDestinataireNoeudModel  extends EventDispatcher
	{
		
		public function AssocierDestinataireNoeudModel()
		{
		}
				
		internal function getRemoveResultHandler(re:ResultEvent):void
		{
			if(re.result==1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Votre_destinataire_a_bien__t__dissoci__d'));
				
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'Une_erreur_s_est_produit_lors_de_la_diss'),ResourceManager.getInstance().getString('M22', 'Erreur_dissociation_destinataire_noeud'));
			}
		}
		
		internal function getResultHandler(re:ResultEvent):void
		{
			if(re.result==1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Votre_destinataire_a_bien__t__associ__au'));
				this.dispatchEvent(new DestinataireEvent(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'Une_erreur_s_est_produit_lors_de_l_assoc'),ResourceManager.getInstance().getString('M22', 'Erreur_association_destinataire_noeud'));
			}
		}
		
		internal function getAssociationResultHandler(re:ResultEvent):void
		{
			if(re.result==1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Vos_destinataires_ont_bien__t__associ_s_'));
				this.dispatchEvent(new DestinataireEvent(DestinataireEvent.LISTE_DESTINATAIRE_NOEUD_EVENT));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'Une_erreur_s_est_produit_lors_de_l_assoc'),ResourceManager.getInstance().getString('M22', 'Erreur_association_destinataire_noeud'));
			}
		}
	}
}