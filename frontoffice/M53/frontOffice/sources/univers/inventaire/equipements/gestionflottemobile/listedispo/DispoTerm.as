package univers.inventaire.equipements.gestionflottemobile.listedispo
{
	import flash.events.Event;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class DispoTerm extends DispoTermIHM implements IListeDispo 
	{
		
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
		private var adrImg : Class;
		private var dataListeTerm : ArrayCollection = new ArrayCollection();
		private var dataListeTermAvecSIM : ArrayCollection = new ArrayCollection();
		private var showDgAvecSIM : Boolean;
		[Bindable]
		private var datagridVisible : DataGrid = new DataGrid;
		private var libelleSelection : String;
	
		public function DispoTerm(swowDgAvecSIM : Boolean = false)
		{ 
			
			this.showDgAvecSIM  = swowDgAvecSIM;
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(evt : FlexEvent):void
		{
			datagridVisible = dgListeTerm;
			initData();
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			
			if(showDgAvecSIM == true)
			{
				boxRadioBt.visible = true;
				initDataAvecSIM();
				txtFiltreAvecSim.addEventListener(Event.CHANGE,txtFiltreAvecSIMChangeHandler);
				
				radioSansSIM.addEventListener(Event.CHANGE,radioSansSIM_handler);
				radioAvecSIM.addEventListener(Event.CHANGE,radioAvecSIM_handler);
				dgListeTermAvecSIM.addEventListener(Event.CHANGE,dgAvSIMhandler);
			}
		}
		private function dgAvSIMhandler(evt : Event):void
		{
			dgListeTerm.dataProvider = dataListeTerm; // Declenche un evt donc actualise le champs libelleSelection Dan PanelAffectation.as
		}
		public function getLibelleSelection():String
		{
			if(datagridVisible.selectedIndex!=-1){
				return datagridVisible.selectedItem.IMEI;
			}
			else
			{
				return "Aucune sélection";
			}
		}
		public function getDataGrid():DataGrid
		{
			if(radioSansSIM.selected==true)
			{
				return dgListeTerm;
			}
			else
			{
				return dgListeTermAvecSIM;
			}	
		}
		private function radioSansSIM_handler(evt : Event):void
		{
			if(radioSansSIM.selected==true)
			{
				viewstack_dispo_term.selectedIndex = 0;
				datagridVisible = dgListeTerm;
			}
		}
		private function radioAvecSIM_handler(evt : Event):void
		{
			if(radioAvecSIM.selected==true)
			{
				viewstack_dispo_term.selectedIndex = 1;
				datagridVisible = dgListeTermAvecSIM;
			}
		}
		public function getIDSelection():int
		{
			return datagridVisible.selectedItem.IDTERMINAL; 
		}
		public function getLibelleType():String
		{
			return "Liste des terminaux disponibles";
		}
		public function getSourceIcone():Class
		{
			return adrImg;
		}
		private function initData():void
		{
			
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
	 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"listeTermDispo",
							initResultHandler);
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,"",PanelGestionFoltteMobile.idPoolGestionnaire);					
		}
		private function initResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeTerm = re.result as ArrayCollection;
				dgListeTerm.dataProvider = dataListeTerm;
				dataListeTerm.refresh();
				trace("Terminal disponibles :"+ObjectUtil.toString(re.result));
			}		
		}
		private function initDataAvecSIM():void
		{
			 var sessionObj:IConsoViewSessionObject = CvAccessManager.getSession();
			 var perimetreObj:IConsoViewPerimetreObject = sessionObj.CURRENT_PERIMETRE;
			 var GROUPE_INDEX :int= perimetreObj.GROUPE_INDEX; // id groupe Racine
	 
			var opData : AbstractOperation= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
							"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
							"getNonAffectedTerm",//Devrait retourner uniquement les term DISPO AVEC SIM
							initAvecSIMResultHandler);
			RemoteObjectUtil.callService(opData,GROUPE_INDEX,"",PanelGestionFoltteMobile.idPoolGestionnaire);					
		}
		private function initAvecSIMResultHandler(re : ResultEvent):void{
			if (re.result){
				dataListeTermAvecSIM = re.result as ArrayCollection;
				var newCol : ArrayCollection = new ArrayCollection();
				var i:int;
				for(i=0; i < dataListeTermAvecSIM.length;i++){	
					if(dataListeTermAvecSIM.getItemAt(i).IDSIM  != null)
					{
						newCol.addItem(dataListeTermAvecSIM.getItemAt(i));
					}
				}
				dataListeTermAvecSIM = newCol;
				dgListeTermAvecSIM.dataProvider = dataListeTermAvecSIM;
				dataListeTermAvecSIM.refresh();
				trace("Terminal disponibles :"+ObjectUtil.toString(re.result));
			}		
		}
		private function txtFiltreChangeHandler(ev : Event):void
		{
			try{
				dataListeTerm.filterFunction = filtrerLeGrid;
				dataListeTerm.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
		private function filtrerLeGrid(item : Object):Boolean{
			if ((String(item.IMEI).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||
				(String(item.MARQUE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1)
				||				
				(String(item.MODELE).toLowerCase().search(txtFiltre.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
		
		private function txtFiltreAvecSIMChangeHandler(ev : Event):void
		{
			try{
				dataListeTermAvecSIM.filterFunction = filtrerLeGridAvecSIM;
				dataListeTermAvecSIM.refresh();
			}catch(e :Error){
				trace("Erreur Gestion Flotte GSM");
			}
		}
		private function filtrerLeGridAvecSIM(item : Object):Boolean{
			if ((String(item.IMEI).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1)
				||
				(String(item.MARQUE).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1)
				||				
				(String(item.MODELE).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1)
				||				
				(String(item.NUM_SIM).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1)
				||				
				(String(item.DISTRIBUTEUR).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1)
				||				
				(String(item.LIGNE).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1)
				||				
				(String(item.REVENDEUR).toLowerCase().search(txtFiltreAvecSim.text.toLowerCase()) != -1))
				return true
			else
				return false
		}
		
	}
}