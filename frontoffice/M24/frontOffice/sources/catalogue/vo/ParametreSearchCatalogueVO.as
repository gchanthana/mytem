package catalogue.vo
{
	

	public class ParametreSearchCatalogueVO
	{
		private var _IDCONSTRUCTEUR:Number;
		private var _IDCLIENT:Number;
		private var _IDETAT:Number;//0=tous,1=Disponibles, 2= En fin de vie, 3= Bientot fin de vie, 4= en repture
		private var _IDTYPEEQUIPEMENT:Number;
		private var _IDCLASSE:Number;
		private var _ORDER_BY:String;
		private var _SEARCH_TEXT:String;
		private var _GROUPE_INDEX:Number;
		private var _IDREVENDEUR:Number;
		private var _IDTYPECMD:int;
		private var _IDOP:Number;
		private var _IDSEGMENT:Number;//0=tous,1=mobile,2=fixe,3=réseaux
		private var _DEFINIDEFAUT:Number;//0=tous,1=mobile,2=fixe,3=réseaux

		
		
		
		
		public function ParametreSearchCatalogueVO()
		{
			
		}
		public function set IDOP(value:Number):void
		{
			_IDOP = value;
		}

		public function get IDOP():Number
		{
			return _IDOP;
		}
		public function set IDTYPECMD(value:int):void
		{
			_IDTYPECMD = value;
		}

		public function get IDTYPECMD():int
		{
			if(_IDTYPECMD>=0)
			{
				return _IDTYPECMD;
			}
			else
			{
				return _IDTYPECMD;
			}
		}
		public function set IDREVENDEUR(value:Number):void
		{
			_IDREVENDEUR = value;
		}

		public function get IDREVENDEUR():Number
		{
			return _IDREVENDEUR;
		}
		public function set IDCONSTRUCTEUR(value:Number):void
		{
			_IDCONSTRUCTEUR = value;
		}

		public function get IDCONSTRUCTEUR():Number
		{
			return _IDCONSTRUCTEUR;
		}
		
		public function get IDETAT():Number
		{
			return _IDETAT;
		}
		public function set IDETAT(value:Number):void
		{
			_IDETAT = value;
		}
		public function get IDCLIENT():Number
		{
			return _IDCLIENT;
		}
			public function set IDCLIENT(value:Number):void
		{
			_IDCLIENT = value;
		}

		public function set IDTYPEEQUIPEMENT(value:Number):void
		{
			_IDTYPEEQUIPEMENT = value;
		}

		public function get IDTYPEEQUIPEMENT():Number
		{
			return _IDTYPEEQUIPEMENT;
		}
		public function set IDCLASSE(value:Number):void
		{
			_IDCLASSE = value;
		}

		public function get IDCLASSE():Number
		{
			return _IDCLASSE;
		}

		public function set ORDER_BY(value:String):void
		{
			_ORDER_BY = value;
		}

		public function get ORDER_BY():String
		{
			return _ORDER_BY;
		}
		public function set SEARCH_TEXT(value:String):void
		{
			_SEARCH_TEXT = value;
		}

		public function get SEARCH_TEXT():String
		{
			return _SEARCH_TEXT;
		}

		public function set GROUPE_INDEX(value:Number):void
		{
			_GROUPE_INDEX = value;
		}

		public function get GROUPE_INDEX():Number
		{
			return _GROUPE_INDEX;
		}

		

		public function set IDSEGMENT(value:Number):void
		{
			_IDSEGMENT = value;
		}

		public function get IDSEGMENT():Number
		{
			return _IDSEGMENT;
		}
		
		public function set DEFINIDEFAUT(value:Number):void
		{
			_DEFINIDEFAUT = value;
		}
		
		public function get DEFINIDEFAUT():Number
		{
			return _DEFINIDEFAUT;
		}
	}
}