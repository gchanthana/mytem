<cfcomponent output="false">
	
	<cffunction access="remote" output="false" name="getListeOrgaClient" returntype="query">
		
		<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_pfgp_suivi_chargement.getListeOrgaClient">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
			<cfprocresult name="qOrga">
		</cfstoredproc>				
		<cfreturn qOrga><!--- le resultat de la procedure --->
     </cffunction>
	
</cfcomponent>