package ihm.popup
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import entity.FileUpload;
	import entity.MasterVO;
	
	import event.RacineMasterEvent;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	import mx.validators.NumberValidator;
	import mx.validators.StringValidator;
	import mx.validators.Validator;
	
	import service.FctGestionClientService;
	
	public class PopUpCreateClientImpl extends TitleWindowBounds
	{
		private var _imgFileRef				:FileReference;
		private var _fileToUpload			:FileUpload;
		private var _iServiceGestClient		:FctGestionClientService;
		private var numVa_grpMaster			:NumberValidator = new NumberValidator();		
		private var _dpListeMasters			:ArrayCollection;
		private var _dpSelectedMaster		:ArrayCollection = new ArrayCollection();
		
		public var lbl_uploadedImg 			:Label;
		public var img_uploaded 			:Image;
		public var hb_btnUpload				:HBox;
		public var hb_imgUpload				:HBox;
		public var combo_groupeMaster		:ComboBox;
		public var sv_nom					:StringValidator;
		public var img_recherche			:Image;
		[Bindable]public var text_lblErase	:String = ResourceManager.getInstance().getString('CG08', 'Supprimer');
		[Bindable]public var toHide			:Boolean = false; 
		[Bindable]public var ti_nomClient	:TextInput;
		[Bindable]public var ta_commentaire	:TextArea;
		
		public function PopUpCreateClientImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		protected function init(event:FlexEvent):void
		{
			initListeners();
			
		}
		
		private function initListeners():void
		{
			this.addEventListener(RacineMasterEvent.SELECT_MASTER, selectMasterHandler);
			this.img_recherche.addEventListener(MouseEvent.CLICK, clickRechercheBtnHandler);
		}
		
		protected function selectMasterHandler(evt:RacineMasterEvent):void
		{
			dpSelectedMaster.source = new Array();
			var mast:MasterVO = new MasterVO();
			mast = evt.objMaster;
			dpSelectedMaster.addItem(mast);
			combo_groupeMaster.selectedIndex = 0;
			
		}
		
		protected function clickRechercheBtnHandler(event:MouseEvent):void
		{
			var popup:PopUpListesRacinesMaster = new PopUpListesRacinesMaster();
			popup.addEventListener(RacineMasterEvent.SELECT_MASTER, selectMasterHandler);
			popup.listeMasters = this.dpListeMasters;
			PopUpManager.addPopUp(popup, this, true);
			PopUpManager.centerPopUp(popup);
			PopUpManager.bringToFront(popup);
		}
		
		protected function btnValider_clickHandler(event:MouseEvent):void
		{
			if(validateEnteredInfosClient() == true)
			{
				if(fileToUpload != null)
				{
					ConsoviewAlert.afficherAlertConfirmation('Voulez-vous confirmer la création du client?', 'Nouveau client', alertCreateContrat);
				}
				else
				{
					ConsoviewAlert.afficherAlertConfirmation('Voulez-vous confirmer la création du client sans logo?', 'Nouveau client', alertCreateContrat);
				}
			}
		}
		
		private function validateEnteredInfosClient():Boolean
		{
			var result:Boolean = false;
			numVa_grpMaster.source = combo_groupeMaster;
			numVa_grpMaster.property = 'selectedIndex';
			numVa_grpMaster.minValue = 0;
			numVa_grpMaster.lowerThanMinError = ResourceManager.getInstance().getString('CG08','Ce_champ_est_obligatoire');
			
			var validationResult:Array = Validator.validateAll([sv_nom, numVa_grpMaster]);
			if (validationResult.length == 0)
			{
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}
		
		private function alertCreateContrat(evt:CloseEvent):void
		{
			if (evt.detail == Alert.OK)
			{
				if(combo_groupeMaster.selectedIndex > -1){
					this.iServiceGestClient.createClient((combo_groupeMaster.selectedItem as MasterVO).idMaster, 0, ti_nomClient.text, ta_commentaire.text, fileToUpload);
					this.close_clickHandler(evt);
				}
			}
		}
		
		protected function close_clickHandler(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		protected function onUploadLogoClick(event:MouseEvent):void
		{
			var arrayfilters:Array = [];
			arrayfilters.push(new FileFilter('Types de fichiers supportés: *.png', '*.png'));
			_imgFileRef = new FileReference();
			_imgFileRef.browse(arrayfilters);
			_imgFileRef.addEventListener(Event.SELECT, onFileSelected);
		}
		
		protected function onFileSelected(event:Event):void
		{
			_imgFileRef.removeEventListener(Event.SELECT, onFileSelected);
			_imgFileRef.addEventListener(Event.COMPLETE, onFileLoaded);
			
			_imgFileRef.load();
			
			fileToUpload = new FileUpload();
			fileToUpload.fileData 		= _imgFileRef.data;
			fileToUpload.fileName 		= _imgFileRef.name;
			fileToUpload.fileExt 		= _imgFileRef.type;
			fileToUpload.fileSize 		= _imgFileRef.size;
		}
		
		protected function onFileLoaded(evt:Event):void
		{
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loader_complete);
			loader.loadBytes(_imgFileRef.data);
		}
		
		protected function loader_complete(evt:Event):void 
		{
			var sourceBMP:Bitmap = evt.currentTarget.loader.content as Bitmap;
			if((sourceBMP.width <= 300)||(sourceBMP.height <= 52))
			{
				toHide = true;
				lbl_uploadedImg.text 	=  'Logo téléchargé ' + _imgFileRef.name + ' (' + _imgFileRef.size +' octets).'
				img_uploaded.source 	= _imgFileRef.data;
				fileToUpload.fileData 	= _imgFileRef.data;
					
			}
			else
			{
				Alert.show( 'Logo trop grand '+'('+ sourceBMP.width+ ' px , ' +sourceBMP.height + ' px)' +'\.\n Merci de respecter les dimensions maximales: 300px en longeur et 52px en hauteur.)');
			}
			
			
		}
		
		protected function onDeleteLogoClick(event:MouseEvent):void
		{
			img_uploaded.source = null;

			toHide = false;
		}
		
		public function get fileToUpload():FileUpload { return _fileToUpload; }
		
		public function set fileToUpload(value:FileUpload):void
		{
			if (_fileToUpload == value)
				return;
			_fileToUpload = value;
		}
		
		public function get iServiceGestClient():FctGestionClientService { return _iServiceGestClient; }
		
		public function set iServiceGestClient(value:FctGestionClientService):void
		{
			if (_iServiceGestClient == value)
				return;
			_iServiceGestClient = value;
		}
		
		[Bindable]
		public function get dpListeMasters():ArrayCollection { return _dpListeMasters; }
		
		public function set dpListeMasters(value:ArrayCollection):void
		{
			if (_dpListeMasters == value)
				return;
			_dpListeMasters = value;
		}
		
		[Bindable]
		public function get dpSelectedMaster():ArrayCollection { return _dpSelectedMaster; }
		
		public function set dpSelectedMaster(value:ArrayCollection):void
		{
			if (_dpSelectedMaster == value)
				return;
			_dpSelectedMaster = value;
		}
		
	}
}