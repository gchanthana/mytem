package composants.parametres.perimetres
{
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.controls.Tree;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	public class specialTree extends Tree
	{
		private var _currentRollOverItem:Object;
		public var _lastSelectedItem:Number;
		private var _dataArray:ArrayCollection;
		
		
		public function get currentSelectedItem():Object
		{
			return this.selectedItem;
		}
				
		public function setCurrentSelectedIndex(curentXML:XML):void
		{
			if(curentXML == null)
			{
				this.selectedItem = -1;
				
				return;
			}
			
			var currentList:XMLList = (this.dataProvider as XMLListCollection).descendants().(@['VALUE'] == curentXML.@VALUE);
			
			if (currentList[0] == null)	
				return ;
			
			var currentNode:Object = currentList[0];
			
			while ((currentNode = currentNode.parent()) != null)
			{
				this.expandItem(currentNode, true);
			}
			
			this.selectedItem = currentList[0];
			this.scrollToIndex(this.selectedIndex);
		}
		
		public function specialTree()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(event:Event):void
		{
			this.addEventListener(ListEvent.ITEM_ROLL_OVER, treeItemRollover);
		    this.addEventListener(ListEvent.ITEM_ROLL_OUT, treeItemRollout);
		 	this.addEventListener(ListEvent.ITEM_CLICK, treeItemClick);
		}
		
		public function setContextMenu(data:ArrayCollection):void
		{
			_dataArray = data;
			var cm:ContextMenu = new ContextMenu();
			cm.hideBuiltInItems();
			
			for (var i:int = 0; i < data.length; i++)
			{
				var item:ContextMenuItem = new ContextMenuItem(data.getItemAt(i).label);
				if (data.getItemAt(i).separate == true)
					item.separatorBefore = true;
				item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, data.getItemAt(i).callback);
				cm.customItems.push(item);
			}
			this.contextMenu = cm;

			cm.addEventListener(ContextMenuEvent.MENU_SELECT, displayContextOptions);
		}
		
		private function treeItemClick(event:ListEvent):void
		{
			_currentRollOverItem = this.selectedItem;
			this.selectedIndex
			this.parent.parent.dispatchEvent(new Event('ITEM_TREE_CLICK', true));
			
		}
		private function treeItemRollover(event:ListEvent):void
		{
			_currentRollOverItem = event.itemRenderer.data;
		}
		
		private function treeItemRollout(event:ListEvent):void
		{
			_currentRollOverItem = null;
		}
		
		private function displayContextOptions(event:Event):void
		{
			var activate:Boolean = true;
			if (_currentRollOverItem == null)
			{
				this.selectedIndex = -1;
				activate= false;
			}
			else
			{
				this.selectedItem = _currentRollOverItem;
				this.dispatchEvent(new Event("change"));
				activate= true;
			}

			for (var i:int = 0; i < _dataArray.length; i++)
			{
				trace(value);
				event.currentTarget.customItems[i].enabled=activate;
				if (_dataArray[i].forceEnabled == true)
					event.currentTarget.customItems[i].enabled= false;
				
			}
			
		}
		
	}
}