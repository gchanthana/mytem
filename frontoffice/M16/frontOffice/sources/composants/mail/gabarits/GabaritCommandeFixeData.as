package composants.mail.gabarits
{
	import commandemobile.system.Contact;
	import commandemobile.system.ElementCommande;
	
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import gestioncommande.entity.Commande;
	
	import mx.resources.ResourceManager;
	
	public class GabaritCommandeFixeData extends AbstractGabarit
	{
		private var _commande : Commande;
		private var _panier : ElementCommande;
		private var _contact : Contact;
		private var _societe : Object;
		
		public function GabaritCommandeFixeData()
		{
			super();
		}
		
		override public function getGabarit():String{
			
			_commande = infos.commande;
			_panier = infos.panier;
			_contact = infos.contact;
			_societe = infos.societe;
			
			var listeLignes : String = "";
			if (_panier != null){
				listeLignes = printListeSousTete(_panier.liste.source);
			}
			
			
			
			var cnom : String = (_contact!= null  &&_contact.nom != null )? _contact.nom : " ";
			var cprenom : String = (_contact!= null && _contact.prenom != null )? _contact.prenom : " ";
			var cemail: String = (_contact!= null &&  _contact.email!=null)?_contact.email:" ";
			var csociete : String = (_societe != null && _societe.raisonSociale!=null)?_societe.raisonSociale:" ";
			var cadresse1 : String = (_societe != null  && _societe.adresse1!=null)?_societe.adresse1:" ";
			var czipcode : String = (_societe != null &&_societe.codePsotal!=null)?_societe.codePsotal:" ";
			var ccommune : String = (_societe != null && _societe.commune!=null)?_societe.commune:" ";
			
			var refclient : String = (_commande.REF_CLIENT1!=null)?_commande.REF_CLIENT1:" ";
			var refclient2 : String = (_commande.REF_CLIENT2!=null)?_commande.REF_CLIENT2:" ";
			var refOperateur : String = (_commande.REF_OPERATEUR!=null)?_commande.REF_OPERATEUR:" ";
			var dateLivPrevue : String = (_commande.LIVRAISON_PREVUE_LE != null) ? DateFunction.formatDateAsString(new Date(_commande.LIVRAISON_PREVUE_LE)) : " " ;
			var commentaires : String = (_commande.COMMENTAIRES!=null)?_commande.COMMENTAIRES:" ";
			var compte : String = infos.compte;
			var sousCompte : String = infos.sousCompte;
			
			return	"<br/>"
					+ResourceManager.getInstance().getString('M16', '_p_Objet___Commande__p_')
					+"<br/>"
					+"<b>"+infos.PRENOM_EXPEDITEUR+" "+infos.NOM_EXPEDITEUR +"</b>"
					+"<br/>"+infos.MAIL_EXPEDITEUR
					+ResourceManager.getInstance().getString('M16', '_br___u_Date__u____')+DateFunction.formatDateAsString(new Date())
					+ResourceManager.getInstance().getString('M16', '_br__Soci_t_____b_')+infos.SOCIETE_EXPEDITEUR+"</b>"
					+"<br/>"	
					+"<br/>"											
					+ResourceManager.getInstance().getString('M16', '_br___t_t_t_t_t_t_t_t_t_t_t_t_t_t_t_t_t_')+"</b>"	
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>"+cprenom +" "+cnom +"</b>"
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+cemail
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>"+csociete+"</b>"
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+cadresse1
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+czipcode
					+"<br/>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+ccommune
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_br__Libell__de_la_commande__b_') + _commande.LIBELLE_COMMANDE+"</b>"
					+ResourceManager.getInstance().getString('M16', '_br__R_f_rence_client____b_') + refclient +"</b>"
					+ResourceManager.getInstance().getString('M16', '_br__R_f_rence_op_rateur____b_') + refOperateur +"</b>"
					+ResourceManager.getInstance().getString('M16', '_br__Remarque____b_') + commentaires +"</b>"
					+ResourceManager.getInstance().getString('M16', '_br__Date_de_la_commande____b_') + DateFunction.formatDateAsString(new Date(_commande.DATE_COMMANDE))+"</b>"
					+ResourceManager.getInstance().getString('M16', '_br__Date_livraison_annonc_e____b_') + dateLivPrevue+"</b>"
					+"<br/>"
					+ResourceManager.getInstance().getString('M16', '_br___Compte_de_facturation____b_')+ compte +ResourceManager.getInstance().getString('M16', '__b____________Sous_compte____b_') + sousCompte+"</b>"
					+"<br/>_______________________________________________________________________________________________"
					+"<br/>"
					+ listeLignes
					+"<br/>_______________________________________________________________________________________________"
					+"<br/>"	
					+"<br/>"	
					+"<br/>"	
					+"<br/>"					
					+ResourceManager.getInstance().getString('M16', 'Cordialement_')
					+"<br/>"	
					+"<b>"+infos.PRENOM_EXPEDITEUR+" "+infos.NOM_EXPEDITEUR +".</b>"
		}
					
		
		protected function printListeSousTete(liste : Array):String{
			var output : String = "<br/>";
			if (liste != null){
				for (var i:Number=0; i<liste.length;i++){
					var e : ElementCommande = liste[i] as ElementCommande;					
					var compte : String = "";
					var sousCompte : String = "";
					var produit : String = e.libelleProduit;
					var ligne : String = e.libelleLigne;
					var theme : String = e.libelletheme;
					var comms : String = e.commentaire != null ? e.commentaire: " ";					
					output = output + ResourceManager.getInstance().getString('M16', '_li_Ligne___b_')+ ConsoviewFormatter.formatPhoneNumber(ligne) 
									+ ResourceManager.getInstance().getString('M16', '__b__t_t_t___Th_me____b_') 
									+ theme 
									+ ResourceManager.getInstance().getString('M16', '__b__t_t_t___Produits___b_') 
									+  produit
									+ ResourceManager.getInstance().getString('M16', '__b__t_t_t___Commentaire___b_')
									+  comms
									+ "</b></li>";
				}
			}
			return output;
		}
	}
}
