<cfcomponent name="PerimetreManager" alias="fr.consotel.consoview.access.PerimetreManager">
	<cfset THIS.PKG_GLOBAL="PKG_CV_GLOBAL">
	   
	<cffunction name="testRPC" access="remote" returntype="numeric">
		<cfargument name="milliSeconds" required="true" type="numeric">
		<cfset thread = CreateObject("java","java.lang.Thread")>
		<cfset thread.sleep(milliSeconds)>
		<cfreturn milliSeconds>
	</cffunction>
	
	<cffunction name="getNodeChild" access="remote" returntype="xml">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#THIS.PKG_GLOBAL#.enfant_noeud">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetChild">
		</cfstoredproc>
		<cfset childXmlTree = XmlNew()>
		<cfset childXmlTree.xmlRoot = XmlElemNew(childXmlTree,"NODE")>
		<cfset rootNode = childXmlTree.NODE>
		<cfset rootNode.XmlAttributes.LBL = "Enfants de " & nodeId>
		<cfset rootNode.XmlAttributes.NID = nodeId>
		<cfset rootNode.XmlAttributes.NTY = 0>
		<cfset rootNode.XmlAttributes.IS_ACTIVE = 0>
		<cfset rootNode.XmlAttributes.STC = 0>
		<cfloop index="i" from="1" to="#qGetChild.recordcount#">
			<cfset rootNode.XmlChildren[i] = XmlElemNew(childXmlTree,"NODE")>
			<cfset rootNode.XmlChildren[i].XmlAttributes.LBL = qGetChild['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NID = qGetChild['IDGROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NTY = qGetChild['TYPE_NOEUD'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.IS_ACTIVE = qGetChild['IS_ACTIVE'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NUMERO_NIVEAU = qGetChild['NUMERO_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.IDORGA_NIVEAU = qGetChild['IDORGA_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.STC = 1>
		</cfloop>
		<cfset qGetChild = ''>
		<cfreturn childXmlTree>
	</cffunction>
	
	<!--- renvoie la liste des noeuds organisation avec des attributs supplémentaires 
	  indiquant si les noeuds dossier contient des feuilles actives et/ou inactives  --->
	<cffunction name="getNodeChild_v2" access="remote" returntype="xml">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="#THIS.PKG_GLOBAL#.Enfant_Noeud_v2 ">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetChild">
		</cfstoredproc>
		<cfset childXmlTree = XmlNew()>
		<cfset childXmlTree.xmlRoot = XmlElemNew(childXmlTree,"NODE")>
		<cfset rootNode = childXmlTree.NODE>
		<cfset rootNode.XmlAttributes.LBL = "Enfants de " & nodeId>
		<cfset rootNode.XmlAttributes.NID = nodeId>
		<cfset rootNode.XmlAttributes.NTY = 0>
		<cfset rootNode.XmlAttributes.IS_ACTIVE = 0>
		<cfset rootNode.XmlAttributes.STC = 0>
		<cfloop index="i" from="1" to="#qGetChild.recordcount#">
			<cfset rootNode.XmlChildren[i] = XmlElemNew(childXmlTree,"NODE")>
			<cfset rootNode.XmlChildren[i].XmlAttributes.LBL = qGetChild['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NID = qGetChild['IDGROUPE_CLIENT'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NTY = qGetChild['TYPE_NOEUD'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.IS_ACTIVE = qGetChild['IS_ACTIVE'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.HAS_ACTIVE = qGetChild['HAS_ACTIVE'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.HAS_INACTIVE = qGetChild['HAS_INACTIVE'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.NUMERO_NIVEAU = qGetChild['NUMERO_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.IDORGA_NIVEAU = qGetChild['IDORGA_NIVEAU'][i]>
			<cfset rootNode.XmlChildren[i].XmlAttributes.STC = 1>
		</cfloop>
		<cfset qGetChild = ''>
		<cfreturn childXmlTree>
	</cffunction>
	
	<cffunction name="getNodeInfos" access="remote" returntype="struct">
		<cfargument name="accessId" required="true" type="numeric">
		<cfargument name="nodeId" required="true" type="numeric">
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="#THIS.PKG_GLOBAL#.detail_noeud_v2">
			<cfprocparam  value="#nodeId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetNodeInfos">
		</cfstoredproc>
		<cfset nodeInfos=structNew()>
		<cfset nodeInfos.INFOS_STATUS = qGetNodeInfos.recordcount>
		<cfif nodeInfos.INFOS_STATUS GT 0>
			<cfset nodeInfos.NID = qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset nodeInfos.LBL = qGetNodeInfos['LIBELLE_GROUPE_CLIENT'][1]>
			<cfif nodeInfos.NID EQ SESSION.PERIMETRE.ID_GROUPE>
				<cfset nodeInfos.TYPE_LOGIQUE = "ROOT">
			<cfelse>
				<cfset nodeInfos.TYPE_LOGIQUE = qGetNodeInfos['TYPE_ORGA'][1]>
			</cfif>
			<cfset nodeInfos.TYPE_PERIMETRE = qGetNodeInfos['TYPE_PERIMETRE'][1]>
			<cfset nodeInfos.STC = qGetNodeInfos['IDGROUPE_CLIENT'][1]>
			<cfset nodeInfos.CODE_STYLE = qGetNodeInfos['CODE_STYLE'][1]>
			<cfset nodeInfos.OPERATEURID = VAL(qGetNodeInfos['OPERATEURID'][1])>
			
			<cfset nodeInfos.MODULE_FACTURATION = qGetNodeInfos['MODULE_FACTURATION'][1]>
			
			<cfset nodeInfos.MODULE_USAGE = qGetNodeInfos['MODULE_USAGE'][1]>
			
			<cfset nodeInfos.MODULE_WORKFLOW = qGetNodeInfos['MODULE_WORKFLOW'][1]>
			<cfset nodeInfos.MODULE_FIXE_DATA = qGetNodeInfos['MODULE_FIXE_DATA'][1]>
			<cfset nodeInfos.MODULE_MOBILE = qGetNodeInfos['MODULE_MOBILE'][1]>
			<cfset nodeInfos.MODULE_GESTION_ORG = qGetNodeInfos['MODULE_GESTION_ORG'][1]>
			<cfset nodeInfos.MODULE_GESTION_LOGIN = qGetNodeInfos['MODULE_GESTION_LOGIN'][1]>
			<cfset nodeInfos.DROIT_GESTION_FOURNIS = qGetNodeInfos['DROIT_GESTION_FOURNIS'][1]>
			<!--- For compatibility with Flex NodeInfos Class --->
			<cfset nodeInfos.FACT = nodeInfos.MODULE_FACTURATION>
			<cfset nodeInfos.GEST = nodeInfos.MODULE_FIXE_DATA + nodeInfos.MODULE_MOBILE +
																	nodeInfos.MODULE_WORKFLOW>
			<cfset nodeInfos.STRUCT = nodeInfos.MODULE_GESTION_ORG + nodeInfos.MODULE_GESTION_LOGIN>
			<cfset nodeInfos.USG = nodeInfos.MODULE_USAGE>
		</cfif>
		<cfreturn nodeInfos>
	</cffunction>
</cfcomponent>