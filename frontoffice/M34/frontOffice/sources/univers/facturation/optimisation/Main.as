package univers.facturation.optimisation {
	import composants.tb.rapports.RapportEvent;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	
	public class Main extends MainIHM {
		private var filterArray:ArrayCollection; // Pour le filtre sur les rapports
		private var reportListSourceArray:ArrayCollection; // Pour le filtre sur les rapports
		
		public function Main() {
			filterArray = new ArrayCollection();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		private function initIHM(event:Event):void {
			reportList.visible = false;
			reportList.addEventListener(Event.CHANGE,changeReport);
			reportFilterText.addEventListener(Event.CHANGE,filterReportList);
		}
		
		public function getCurrentReportIndex():int {
			return reportList.selectedItem.id;
		}
		
		public function setReportList(dataProviderObject:Object):void {
			reportList.visible = true;
			reportList.dataProvider = dataProviderObject;
			if((reportList.dataProvider as ArrayCollection).length > 0) {
				reportList.selectedItem = reportList.dataProvider[0];
				reportListSourceArray = reportList.dataProvider as ArrayCollection;
				reportListTitle.headerText = "Liste des rapports (" + reportListSourceArray.length + ")";
				dispatchEvent(new RapportEvent(RapportEvent.REPORT_SELECTION_CHANGED));
			}
		}
		
		private function changeReport(event:Event):void {
			if(reportList.selectedItem != null)
				trace("Main changeReport() Séléction du rapport : " + reportList.selectedItem.label);
			else
				reportList.selectedItem = reportList.dataProvider[0];
			dispatchEvent(new RapportEvent(RapportEvent.REPORT_SELECTION_CHANGED));
		}
		
		private function filterReportList(event:Event):void {
			if(reportListSourceArray.length > 0) {
				var fortext:String = reportFilterText.text.toLowerCase();
				var libelle:String;
				var i:int = 0;
				filterArray.removeAll();
				reportList.dataProvider = null;
				for(i = 0; i < reportListSourceArray.length; i++) {
					libelle = reportListSourceArray[i]['label'] != null ? reportListSourceArray[i]['label'] : "";
					if(libelle.toLowerCase().indexOf(fortext) != -1)
						filterArray.addItem(reportListSourceArray[i]);
			   	}
		   		reportList.dataProvider = filterArray;
				reportListTitle.headerText = "Liste des rapports (" + filterArray.length + ")";
			}
		}
	}
}
