<cfcomponent output="false">
<!--- ****************************************************************************************************** --->		
	<cffunction access="remote" name="getListeChampsPersos" returntype="Query">
		<cfargument name="mobile" type="Numeric">
		<cfargument name="fixe" type="Numeric">
		<cfargument name="reseau" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.listeChampsPersos">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#mobile#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#fixe#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#reseau#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.USER.GLOBALIZATION#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="SauvegarderChampPerso" returntype="numeric">
		<cfargument name="listeAction" type="string">
		<cfargument name="listeLibelle" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.SauvegarderChampPerso">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#listeAction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#listeLibelle#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<!--- ****************************************************************************************************** --->	
	<cffunction access="remote" name="ListeActionProfilCommande" returntype="Query">
		<cfargument name="idprofil" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListeActionProfilCommande">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.USER.GLOBALIZATION#" cfsqltype="CF_SQL_VARCHAR">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="EnregistrerActionsProfilCommande" returntype="any">
		<cfargument name="idprofil" type="Numeric">
		<cfargument name="listeAction" type="string">
		<cfargument name="libelle" type="string">
		<cfargument name="code" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.EnregistrerActProfilCommande">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#listeAction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#libelle#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#code#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="CreateProfilCommande" returntype="any">
		<cfargument name="code" type="string">
		<cfargument name="libelle" type="string">
		<cfargument name="commentaire" type="string">
		<cfargument name="listeAction" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.CreateProfilCommande">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#code#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#libelle#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#commentaire#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#listeAction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<!--- ****************************************************************************************************** --->		
	<cffunction access="remote" name="getListProfilGestionParc" returntype="query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListeProfilGestionParc">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="SupprimerProfilGestionParc" returntype="any">
		<cfargument name="idprofil" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.SupprimerProfilGestionParc">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="RecupererActionProfilGestionParc" returntype="Query">
		<cfargument name="idprofil" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.RecupActionProfilGestionParc">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.USER.GLOBALIZATION#" cfsqltype="CF_SQL_varCHAR">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="MAJProfilGestionParc" returntype="any">
		<cfargument name="idprofil" type="Numeric">
		<cfargument name="listeAction" type="string">
		<cfargument name="libelle" type="string">
		<cfargument name="code" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.MAJProfilGestionParc">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#listeAction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#libelle#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#code#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="CreateProfilGestionParc" returntype="any">
		<cfargument name="libelle" type="string">
		<cfargument name="code" type="string">
		<cfargument name="listeAction" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.CreateProfilGestionParc">
			<cfprocparam value="#libelle#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#code#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#listeAction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="ListUserProfilGestionParc" returntype="query">
		<cfargument name="idprofil" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListUserProfilGestionParc">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="AffecterProfilGestionParc" returntype="Any">
		<cfargument name="idpool" type="Numeric">
		<cfargument name="idprofil" type="string">
		<cfargument name="applogin" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.MAJProfilGestionParcOfPool">
			<cfprocparam value="#idpool#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#applogin#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<!--- ****************************************************************************************************** --->		
	<cffunction access="remote" name="ListeProfilValidationFacturation" returntype="Query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListeProfilValidFact">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="SupProfilValidFact" returntype="any">
		<cfargument name="idprofil" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.SupProfilValidFact">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="ListeActValidFact" returntype="Query">
		<cfargument name="idprofil" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListeActValidFact">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="MAJProfilValidationFacturation" returntype="any">
		<cfargument name="idprofil" type="Numeric">
		<cfargument name="listeAction" type="string">
		<cfargument name="libelle" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.MAJProfilValidationFacturation">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#listeAction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#libelle#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="CreerProfValidFact" returntype="any">
		<cfargument name="listeAction" type="string">
		<cfargument name="libelle" type="string">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.CreerProfValidFact">
			<cfprocparam value="#libelle#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_inTEGER" type="in">
			<cfprocparam value="#listeAction#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="ListUserProfValidFact" returntype="query">
		<cfargument name="idprofil" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListUserProfValidFact">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="AffProfValidFactToClient" returntype="any">
		<cfargument name="applogin" type="Numeric">
		<cfargument name="idprofil" type="Numeric">
		<cfargument name="idracine" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.AffProfValidFactToClient">
			<cfprocparam value="#applogin#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#idprofil#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam variable="p_retour" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction access="remote" name="getProfilValFact" returntype="any">
		<cfargument name="applogin" type="Numeric">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.getProfilValFact">
			<cfprocparam value="#applogin#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>
 		<cfreturn p_retour>
	</cffunction>
<!--- ****************************************************************************************************** --->		
	<cffunction access="remote" name="ListerTypeCommande" returntype="Query">
		<cfstoredproc datasource="#SESSION.OFFREDSN#"  procedure="pkg_m21.ListerTypeCommande">
			<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

</cfcomponent>
