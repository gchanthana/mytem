package univers.inventaire.equipements.lignes.v2.views
{
	import flash.display.DisplayObject;
	
	public interface IOngletCablage
	{
		function get displayObject():DisplayObject;
		
	}
}