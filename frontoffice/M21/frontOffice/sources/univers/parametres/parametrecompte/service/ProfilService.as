package univers.parametres.parametrecompte.service
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.parametres.parametrecompte.event.GestionProfilEvent;
	import univers.parametres.parametrecompte.vo.Gestionnaire;
	import univers.parametres.parametrecompte.vo.ProfilCommande;
	import univers.parametres.parametrelogin.vo.ChampsPersosVO;
	[Bindable]
	public class ProfilService extends EventDispatcher
	{
		public var col_profil : ArrayCollection = new ArrayCollection();
		
		public var col_actionOfProfil_mobil : ArrayCollection = new ArrayCollection();
		public var tab_actionOfProfil_mob_selected : ArrayCollection = new ArrayCollection();
		public var col_actionOfProfil_fixe : ArrayCollection = new ArrayCollection();
		public var col_actionOfProfil_Reseau : ArrayCollection = new ArrayCollection();
		public var tab_actionOfProfil_fixe_selected : ArrayCollection = new ArrayCollection();
		
		public var col_userofProfil : ArrayCollection = new ArrayCollection();
		public var col_contrainte_profil : ArrayCollection = new ArrayCollection();
		public var idProfilSelected : int;
		
		public function ProfilService()
		{
		}
		public function ListeActionProfilCommande(idProfil:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.M21.m21service",
																	"ListeActionProfilCommande",
																	ListeActionProfilCommande_create_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
		public function listeContrainteOfProfil(idProfil:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"listeContrainteOfProfil",
																	listeContrainteOfProfil_handler, listeContrainteOfProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
		public function remove_profil(idProfil:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"removeProfil",
																	remove_profil_handler, remove_profil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
		private function remove_profil_handler(evt : ResultEvent):void
		{
			
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.REMOVE_PROFIL_COMPLETE));	
		}
		private function listeContrainteOfProfil_handler(evt : ResultEvent):void
		{
			col_contrainte_profil.removeAll();
			
			var cursor:IViewCursor = (evt.result as ArrayCollection).createCursor();
						
			while(!cursor.afterLast)
			{
				col_contrainte_profil.addItem(cursor.current);	
				cursor.moveNext();
			}
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_CONTRAINTE_PROFIL_COMPLETE));	
		}
		public function listeProfil(groupeIndex:int):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"fournirListeProfiles",
																	listeProfil_handler, listeProfil_fault_handler);
			RemoteObjectUtil.callService(op,groupeIndex);	
		}
		public function updateActionOfProfil(idProfil:int,tab_idAction : Array,value : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																	"updateXActionProfil",
																	updateActionOfProfil_handler,updateActionOfProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil,tab_idAction, value);
		}
		public function updateInfoProfil(idProfil:int,libelle : String,code:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"updateProfile",
																		updateActionProfil_handler,updateActionProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil,libelle, code);
		}
		public function createProfil(idGroupe:int,libelle : String,code:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"createProfile",
																		createProfile_handler,createProfileProfil_fault_handler);
			RemoteObjectUtil.callService(op,idGroupe,libelle, code);
		}
		public function listeUserOfProfil(idGroupe:int,idProfil : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.PoolGestionnaireService",
																		"listeUserOfProfil",
																		listeUserOfProfil_handler,listeUserOfProfil_fault_handler);
			RemoteObjectUtil.callService(op,idProfil);
			//RemoteObjectUtil.callService(op,idGroupe,idProfil);
		}
		public function listeUserOfProfilGestionParc(idProfil : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M21.m21service",
																		"ListUserProfilGestionParc",
																		listeUserOfProfilGestionParc_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
		public function listeUserOfProfilValidationFacturation(idProfil : int):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M21.m21service",
																		"ListUserProfValidFact",
																		listeUserOfProfilValidationFacturation_handler);
			RemoteObjectUtil.callService(op,idProfil);
		}
	/// ENREGISTRER LES MODIFICATIONS D'UN PROFIL COMMANDE
		public function EnregistrerActionsProfilCommande(idProfil : int, listeAction:String, libelle:String, code:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M21.m21service",
																		"EnregistrerActionsProfilCommande",
																		EnregistrerActionsProfilCommande_handler);
			RemoteObjectUtil.callService(op,idProfil,listeAction,libelle,code);
		}
		private function EnregistrerActionsProfilCommande_handler(e:ResultEvent):void
		{
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.ENREGISTREMENT_PROFILCOMMANDE,true,true));
		}
	// CREER UN PROFIL COMMANDE
		public function CreateProfilCommande(code:String,libelle:String,commentaire:String, listeAction:String):void
		{
			var op:AbstractOperation=RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.M21.m21service",
																		"CreateProfilCommande",
																		CreateProfilCommande_handler);
			RemoteObjectUtil.callService(op,code,libelle,commentaire,listeAction);
		}
		private function CreateProfilCommande_handler(e:ResultEvent):void
		{
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.ENREGISTREMENT_PROFILCOMMANDE,true,true));
		}
		
		private function listeUserOfProfil_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_userofProfil.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Gestionnaire = new Gestionnaire();
				item.id=tmpArr[i].APP_LOGINID;
				item.nom=tmpArr[i].LOGIN_NOM ;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
				item.idPool = tmpArr[i].IDPOOL;
				item.libellePool=tmpArr[i].LIBELLE_POOL;
				col_userofProfil.addItem(item);
			}
		}
		private function listeUserOfProfilGestionParc_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_userofProfil.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Gestionnaire = new Gestionnaire();
				item.id=tmpArr[i].APP_LOGINID;
				item.nom=tmpArr[i].LOGIN_NOM ;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
				item.idPool = tmpArr[i].IDPOOL;
				item.libellePool=tmpArr[i].LIBELLE_POOL;
				col_userofProfil.addItem(item);
			}
		}
		private function listeUserOfProfilValidationFacturation_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_userofProfil.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : Gestionnaire = new Gestionnaire();
				item.id=tmpArr[i].APP_LOGINID;
				item.nom=tmpArr[i].LOGIN_NOM ;
				item.prenom=tmpArr[i].LOGIN_PRENOM;
				col_userofProfil.addItem(item);
			}
		}
		private function updateActionProfil_handler(evt : ResultEvent):void
		{
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.INFO_PROFIL_COMPLETE,true,true,evt.result as int));
		}
		private function createProfile_handler(evt : ResultEvent):void
		{
			if(evt.result>1){
				dispatchEvent(new GestionProfilEvent(GestionProfilEvent.INFO_PROFIL_COMPLETE,true,true,evt.result as int));
			}
		}
	
		
		private function updateActionOfProfil_handler (evt : ResultEvent):void
		{
			
		}
		private function ListeActionProfilCommande_create_handler(e:ResultEvent):void
		{
			if(e.result != null)
			{
				var tmpArr:Array = (e.result as ArrayCollection).source;
				
				col_actionOfProfil_fixe = new ArrayCollection();
				col_actionOfProfil_mobil = new ArrayCollection();
				col_actionOfProfil_Reseau = new ArrayCollection();
				
				for each(var obj:Object in tmpArr)
				{	
					var  item : ChampsPersosVO = new ChampsPersosVO();
					item.CP_Code_Champ = obj.CODE_ACTION
					item.ordreAffichage = obj.ORDRE_AFFICHAGE
					item.CP_ID = obj.IDINV_ACTIONS
					item.CP_Libelle_Client = obj.LIBELLE_ACTION_CLIENT
					item.CP_Libelle_Client_Reference = obj.LIBELLE_ACTION_CLIENT
					item.CP_Libelle_Defaut = obj.LIBELLE_ACTION
					item.CP_Type_Commande = obj.SEGMENT
					if(obj.SELECTED == 1)
						item.Selected = true
					else
						item.Selected = false
					item.boolEdited = false
					if(item.CP_Type_Commande.toUpperCase() == "M")
						col_actionOfProfil_mobil.addItem(item)
					if(item.CP_Type_Commande.toUpperCase() == "F")
						col_actionOfProfil_fixe.addItem(item)
					if(item.CP_Type_Commande.toUpperCase() == "R")
						col_actionOfProfil_Reseau.addItem(item)
				}
				dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_ACTION_COMPLETE));
			}
		}
		private function listeProfil_handler(re : ResultEvent):void
		{
			var tmpArr:Array = (re.result as ArrayCollection).source;
			var i:int;
			col_profil.removeAll();//vider la collection
			for(i=0; i < tmpArr.length;i++)
			{	
				var  item : ProfilCommande = new ProfilCommande();
				item.idProfil=tmpArr[i].IDPROFIL_COMMANDE;
				item.commentaire=tmpArr[i].COMMENTAIRES;
				item.libelle=tmpArr[i].LIBELLE;
				item.codeInterne = tmpArr[i].CODE_INTERNE;
				if(item.codeInterne == 'DEFAUT'){
					item.autorise_edition = false;
				}
				item.nbUser=tmpArr[i].NB ;
				col_profil.addItem(item);
			}
			dispatchEvent(new GestionProfilEvent(GestionProfilEvent.LISTE_PROFIL_COMPLETE));
		}
		private function listeProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeProfil_fault_handler'+evt.toString());
		}
		private function listeAction_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeProfil_fault_handler'+evt.toString());
		}
		private function updateActionOfProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateActionOfProfil_fault_handler'+evt.toString());
		}
		private function updateActionProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.updateActionProfil_fault_handler'+evt.toString());
		}
		private function createProfileProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.createProfileProfil_fault_handler'+evt.toString());
		}
		private function listeUserOfProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeUserOfProfil_fault_handler'+evt.toString());
		}
		private function listeContrainteOfProfil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeContrainteOfProfil_fault_handler'+evt.toString());
		}
		private function remove_profil_fault_handler(evt : FaultEvent):void
		{
			trace('erreur univers.parametres.parametrecompte.service.listeContrainteOfProfil_fault_handler'+evt.toString());
		}
	}
}