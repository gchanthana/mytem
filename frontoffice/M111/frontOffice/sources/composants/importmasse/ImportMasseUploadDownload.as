package composants.importmasse
{
	import composants.util.ConsoviewAlert;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLRequest;

	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	/**
	 * <code>ImportMasseUploadDownload</code> est la classe qui gère l'upload et le download des fichiers pour l'import de masse.
	 * Elle instancie une référence unique de la classe <code>ImportMasseImpl</code>.
	 * Grâce à cette instance elle peut appeller toutes les méthodes qui lui seront utiles dans les classes <code>ImportMasseUtils</code> et <code>ImportMasseService</code>.
	 *
	 * @author GUIOU Nicolas
	 * @version 1.0 (02-03-2010)
	 */
	[Bindable]
	public class ImportMasseUploadDownload extends EventDispatcher
	{

		///////////////////////////////////////////////////////////
		/////////////		VARIABLES PUBLIQUES   /////////////////
		///////////////////////////////////////////////////////////

		public var fileRef:FileReference;
		public var pathFile:String;

		///////////////////////////////////////////////////////////
		/////////////		VARIABLES PRIVEES     /////////////////
		///////////////////////////////////////////////////////////

		private var _myImportMasse:ImportMasseImpl;

		/**
		 * <b>Constructeur <code>ImportMasseUploadDownload(instanceMyImportMasse:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Instancie la variable privée myImportMasse pour avoir une référence unique de l'objet ImportMasseImpl.
		 * </pre>
		 * </p>
		 *
		 * @param instanceMyImportMasse:<code>ImportMasseImpl</code>.
		 *
		 */
		public function ImportMasseUploadDownload(instanceMyImportMasse:ImportMasseImpl)
		{
			myImportMasse=instanceMyImportMasse;
		}

		///////////////////////////////////////////////////////////
		/////////////	    GETTER / SETTER       /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Setter <code>myImportMasse(value:ImportMasseImpl)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Attribut une valeur à la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 *
		 * @param value:<code>ImportMasseImpl</code>.
		 *
		 * @return void.
		 *
		 */
		public function set myImportMasse(value:ImportMasseImpl):void
		{
			_myImportMasse=value;
		}

		/**
		 * <b>Getter <code>myImportMasse()</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Retourne la valeur de la variable privée _myImportMasse.
		 * </pre>
		 * </p>
		 *
		 * @return ImportMasseImpl qui représente la référence unique de l'objet ImportMasseImpl.
		 *
		 */
		public function get myImportMasse():ImportMasseImpl
		{
			return _myImportMasse;
		}

		///////////////////////////////////////////////////////////
		/////////////	   FONCTIONS PUBLIQUES    /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Fonction <code>btUploadClickHandler(evt:MouseEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Deux possibilités par rapport au navigateur utilisé :
		 * - Crée un fileReference sur le fichier à importer.
		 * - Ouvre une popup indiquant que l'import est impossible par rapport à la configuration de sécurité de ce navigateur. Tous les navigateurs sauf IE.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>MouseEvent</code>.
		 *
		 * @return void.
		 *
		 */
		public function btUploadClickHandler(evt:MouseEvent):void
		{
			var userAgent:String="";
			var patternFirefox:RegExp=/(firefox)/i;
			var patternIE:RegExp=/(IE)/;

			if (ExternalInterface.available)
			{
				userAgent=ExternalInterface.call("navigator.userAgent.toString");
			}

			if (userAgent.search(patternIE) != -1) // IE
			{
				var xlsCvsFilter:FileFilter=new FileFilter("Excel Files (*.csv, *.xls)", "*.csv;*.xls");

				fileRef=new FileReference();
				fileRef.browse([xlsCvsFilter]);
				fileRef.addEventListener(Event.SELECT, fileSelectedHandler); //EVENT LORS DU CLICK SUR LE BOUTON ‘OUVRIR’ DE LA BOITE DE DIALOGUE
				fileRef.addEventListener(Event.COMPLETE, fileUploadedHandler); //EVENT LORS DE LA FIN DU TELECHARGEMENT
				fileRef.addEventListener(IOErrorEvent.IO_ERROR, fileNotUploadedHandler); //EVENT LORS D’UNE ERREUR QUELCONQUE 
			}
			else // Firefox ou autre
			{
				var popupImport:ImportMassePopUpImporter=new ImportMassePopUpImporter();
				PopUpManager.addPopUp(popupImport, myImportMasse, true);
				PopUpManager.centerPopUp(popupImport);
			}
		}

		/**
		 * <b>Fonction <code>btDownloadMatriceImportClickHandler(evt:MouseEvent)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Crée un fileReference sur le fichier à télécharger, la matrice d'import type.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>MouseEvent</code>.
		 *
		 * @return void.
		 *
		 */
		public function btDownloadMatriceImportClickHandler(evt:MouseEvent):void
		{
			fileRef=new FileReference();
			fileRef.download(new URLRequest(myImportMasse.urlDownload));
		}

		///////////////////////////////////////////////////////////
		/////////////		FONCTIONS PRIVEES     /////////////////
		///////////////////////////////////////////////////////////

		/**
		 * <b>Fonction <code>fileSelectedHandler(evt:Event)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Indique l'URL de l'import au fileReference.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>Event</code>.
		 *
		 * @return void.
		 *
		 */
		private function fileSelectedHandler(evt:Event):void
		{
			var url:URLRequest=new URLRequest(myImportMasse.urlUpload); // le chemin du fichier .cfm
			fileRef.upload(url);
		}

		/**
		 * <b>Fonction <code>fileUploadedHandler(evt:Event)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Appelle la méthode <code>renameFile()</code> de la classe <code>ImportMasseService</code> pour renommer de manière unique le fichier importé.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>Event</code>.
		 *
		 * @return void.
		 *
		 */
		private function fileUploadedHandler(evt:Event):void
		{
			myImportMasse.myImportMasseService.renameFile();
		}

		/**
		 * <b>Fonction <code>fileNotUploadedHandler(evt:Event)</code></b>
		 * <p>
		 * <pre>
		 * <u>Description :</u>
		 * Affiche une alerte indiquant qu'il y a eu une erreur pendant l'import du fichier.
		 * </pre>
		 * </p>
		 *
		 * @param evt:<code>Event</code>.
		 *
		 * @return void.
		 *
		 */
		private function fileNotUploadedHandler(evt:Event):void
		{
			ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M111', 'Impossible_de_charger_le_fichier__'), ResourceManager.getInstance().getString('M111', 'Consoview'), null);
		}

	}
}
