package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.fournir
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasExtAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasLigneAssociated;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneFixe;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsLigneMobile;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.IsTypeLigneMobile;

	public class RuleFournirLigneExt implements IRuleDisplayItem
	{
		public function RuleFournirLigneExt()
		{
		}

		public function isDisplay():Boolean
		{
			var r1 : IRuleDisplayItem= new HasExtAssociated();
			var r2 : IRuleDisplayItem = new HasLigneAssociated();
			var r3 : IRuleDisplayItem = new IsTypeLigneMobile();
			
			if (!r1.isDisplay() && !r2.isDisplay()) /** pas de ligne ni extension*/
				return true;
			else if (!r1.isDisplay() && r2.isDisplay() && !r3.isDisplay()) /** pas d'extension avec une ligne fixe*/
				return true;
			else
				return false;
		}
	}
}