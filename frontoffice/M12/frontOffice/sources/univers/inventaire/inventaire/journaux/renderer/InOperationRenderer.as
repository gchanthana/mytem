package univers.inventaire.inventaire.journaux.renderer
{
	import mx.controls.TextInput;
	import mx.states.SetStyle;

	public class InOperationRenderer extends TextInput
	{
		public function InOperationRenderer()
		{
			//TODO: implement function
			super();
			editable = false;
			setStyle("borderStyle","none");
			setStyle("backgroundAlpha",0);
		}
		
		override public function set data(value:Object):void {
			if (value){
				
				super.data = value;
				
				text = value.LIBELLE_PRODUIT;
				
				if (value.IDINV_OPERATIONS > 0){
					setStyle("backgroundAlpha",0.3);
					setStyle("backgroundColor","#6DC426");					
				}
			}	
		}		
	}
}