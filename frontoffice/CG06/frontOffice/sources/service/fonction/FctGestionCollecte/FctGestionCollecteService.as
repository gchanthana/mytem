package service.fonction.FctGestionCollecte
{
    import entity.GestionCollecte.ModelLocator;
    import entity.GestionCollecte.vo.CollecteClientVO;
    import entity.GestionCollecte.vo.CollecteTypeVO;
    import entity.GestionCollecte.vo.FilterCollecteTypeVO;
    
    import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
    
    import mx.rpc.AbstractOperation;
    import mx.rpc.events.ResultEvent;
    
    import utils.abstract.AbstractRemoteService;
	
    [Bindable]
    public class FctGestionCollecteService extends AbstractRemoteService
    {
        public static const COLDFUSION_FOLDER_PATH:String = "fr.consotel.consoview.M28.fct.FctGCOL";
        public var myDatas:FctGestionCollecteDatas;
        public var myHandlers:FctGestionCollecteHandlers;

        public function FctGestionCollecteService()
        {
            myDatas = new FctGestionCollecteDatas();
            myHandlers = new FctGestionCollecteHandlers(myDatas);
        }

        /******************************************************************************
         *                         Admin et Client : Procédures Communes
         ****************************************************************************/ /**
         *  Récupère la liste des <b>opérateurs</b> depuis la base
         * @see fr.consotel.consoprod.M25.CollecteService.getOperateurList()
         */
        public function getOperateurList(key:String, code:String):void
        {
			trace("La compilation a bien été actualisée...");
			
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getOperateurList",
                                                                                 myHandlers.getOperateurListResultHandler, null);
            RemoteObjectUtil.callService(op1);*/
			
			doRemoting(myHandlers.getOperateurListResultHandler, key, code, null);
        }

        /**
         *  Récupère la liste des <b>opérateurs</b> depuis la base qui possède un template
         * @see fr.consotel.consoprod.M25.CollecteService.getOperateurList()
         */
        public function getOperateurWithTemplateList(key:String, code:String):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getOperateurWithTemplate",
                                                                                 myHandlers.getOperateurWithTemplateListResultHandler, null);
            RemoteObjectUtil.callService(op1);*/
			doRemoting(myHandlers.getOperateurWithTemplateListResultHandler, key, code, null);
        }

        /**
         *  Récupère la liste des <b>types de données</b> depuis la base
         * @see fr.consotel.consoprod.M25.CollecteService.getDatatypeList()
         */
        public function getDataTypeList(key:String, code:String):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getDatatypeList",
                                                                                 myHandlers.getDataTypeListResultHandler, null);
            RemoteObjectUtil.callService(op1);*/
			doRemoting(myHandlers.getDataTypeListResultHandler, key, code, null);
        }

        /**
         *  Récupère la liste des <b>modes de récupération </b>  depuis la base
         * @see fr.consotel.consoprod.M25.CollecteService.getRecupModeList()
         */
        public function getRecupModeList(key:String, code:String):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getRecupModeList",
                                                                                 myHandlers.getRecupModeListResultHandler, null);
            RemoteObjectUtil.callService(op1);*/
			doRemoting(myHandlers.getRecupModeListResultHandler, key, code, null);
        }

        /******************************************************************************
         *                         Admin : Gestion des Templates
         ****************************************************************************/ /**
         *  Retourne la <b>liste des Templates </b> de collecte disponibles
         * @param p_filter : VO  filtre la liste attendue en fonction de :
         *  l'opérateur (id) : si null tous
         *  le type de données : si null tout type
         *  le mode de récupération : si null tout mode
         * @see fr.consotel.consoprod.M25.CollecteService.getCollecteTypeList()
         */
        public function getCollecteTypeList(key:String, code:String, p_filter:FilterCollecteTypeVO):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getCollecteTypeList",
                                                                                 myHandlers.getCollecteTypeListResultHandler, null);
            RemoteObjectUtil.callService(op1, p_filter.selectedOperateurId, p_filter.selectedDataTypeId, p_filter.selectedRecuperationModeId);
			*/
			var data:Object = new Object();
			data.selectedOperateurId = p_filter.selectedOperateurId;
			data.selectedDataType = p_filter.selectedDataTypeId;
			data.selectedRecuperationMode = p_filter.selectedRecuperationModeId;
			doRemoting(myHandlers.getCollecteTypeListResultHandler, key, code, data);
        }

        /**
         *  Crée ou met à jour un template de collecte
         *  @param vo : le Value Object du template
         *  si vo.id = 0 : Mode CREATION
         *  si vo.id !=0 : Mode UPDATE
         * @see fr.consotel.consoprod.M25.CollecteService.createEditCollecteTypeVo()
         */
        public function createEditCollecteTypeVo(key:String, code:String, vo:CollecteTypeVO):void
        {
           /* var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "createEditCollecteTypeVo",
                                                                                 myHandlers.createEditCollecteTypeVoResultHandler, null);
            RemoteObjectUtil.callService(op1, vo.idcollecteTemplate, vo.uidcollecteTemplate, vo.idcollectType, vo.idcollectMode, vo.operateurId, vo.libelleTemplate,
                                         vo.isPublished.value as Number, vo.isCompleted.value as Number, vo.prestaPerifacturation, vo.instructionsPath,
                                         vo.modeSouscription, vo.modeSouscriptionPath, vo.modeSouscriptionUrl, vo.delaiDispo, "info delai", vo.contraintes.
                                         value as Number, vo.contraintesInfos, vo.anteriorite.value as Number, vo.anterioriteInfos, vo.couts, vo.coutsInfos,
                                         vo.contact, vo.contactInfos, vo.libelleRoic, vo.recuperationRoic, vo.roicInfos, vo.localisationRoic, vo.localisationRoicPath,
                                         vo.libelleRocf, vo.localisationRocf, vo.localisationRocfPath, vo.iduserCreate, vo.iduserModif);
			*/
			var data:Object = new Object();
			data.p_idcollecteTemplate = vo.idcollecteTemplate;
			data.p_uidcollecteTemplate = vo.uidcollecteTemplate;
			data.p_idcollecteType = vo.idcollectType;
			data.p_idcollecteMode = vo.idcollectMode;
			data.p_operateurId = vo.operateurId;
			data.p_libelleTemplate = vo.libelleTemplate;
			data.p_isPublished = vo.isPublished.value as Number;
			data.p_isCompleted = vo.isCompleted.value as Number;
			data.p_prestaPerifacturation = vo.prestaPerifacturation;
			data.p_instructionsPath = vo.instructionsPath;
			data.p_modeSouscription = vo.modeSouscription;
			data.p_modeSouscriptionPath = vo.modeSouscriptionPath;
			data.p_modeSouscriptionUrl = vo.modeSouscriptionUrl;
			data.p_delaiDispo = vo.delaiDispo;
			data.p_delaiDispoInfos = "info delai";
			data.p_contraintes = vo.contraintes.value as Number;
			data.p_contraintesInfos = vo.contraintesInfos;
			data.p_anteriorite = vo.anteriorite.value as Number;
			data.p_anterioriteInfos = vo.anterioriteInfos;
			data.p_couts = vo.couts;
			data.p_coutsInfos = vo.coutsInfos;
			data.p_contact = vo.contact;
			data.p_contactInfos = vo.contactInfos;
			data.p_libelleRoic = vo.libelleRoic;
			data.p_recuperationRoic = vo.recuperationRoic;
			data.p_roicInfos = vo.roicInfos;
			data.p_localisationRoic = vo.localisationRoic;
			data.p_localisationRoicPath = vo.localisationRoicPath;
			data.p_libelleRocf = vo.libelleRocf;
			data.p_localisationRocf = vo.localisationRocf;
			data.p_localisationRocfPath = vo.localisationRocfPath;
			data.p_iduserCreate = vo.iduserCreate;
			data.p_idUserModif = vo.iduserModif;
			doRemoting(myHandlers.createEditCollecteTypeVoResultHandler, key, code, data);
        }

        /**
         *  Crée un identifiant unique nécéssaire au stockage des
         *  documents attachés à un template et génère l'arborescence
         *  de stockage
         *
         */
        public function createUIDStorage(key:String, code:String):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "createUIDStorage",
                                                                                 myHandlers.createUIDStorageResultHandler, null);
            RemoteObjectUtil.callService(op1);*/
			doRemoting(myHandlers.createUIDStorageResultHandler, key, code, null);
        }

        /**
         *  Retourne la liste des groupes racines (libelle, idRacine , nbre de collectes)
         */
        public function getRacineList(key:String, code:String):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getRacineList",
                                                                                 myHandlers.getRacineListResultHandler, null);
            RemoteObjectUtil.callService(op1);*/
			doRemoting(myHandlers.getRacineListResultHandler, key, code, null);
        }

        /**
         *  Detruit le répertoire UID et ses docs enfants
         *  associé après l'annulation de la création d'un template
         *  @see fr.consotel.consoprod.M25.CollecteService.createEditCollecteTypeVo()
         *
         */
        public function destroyLastUID(key:String, code:String, uid:String):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "destroyLastUID",
                                                                                 myHandlers.destroyLastUIDResultHandler, null);
            RemoteObjectUtil.callService(op1, uid);*/
			doRemoting(myHandlers.destroyLastUIDResultHandler, key, code, null);//la fonction pour détruire l'UID dans le back est un fake (fct vide avec return 1 dans tous les cas)
        }

        /******************************************************************************
         *                         Client : Gestion des Collectes
         ****************************************************************************/ /**
         *
         * Retourne la liste des Templates de collecte disponible
         * pour l'édition/création d'une collecte par le client
         * (dépend de l'opérateur, du type de données et du mode
         * de récupération sélectionnés)
         * @see fr.consotel.consoprod.M25.CollecteService.getCollecteTypeList()
         *
         */
        public function getPublishedTemplate(key:String, code:String, p_filter:FilterCollecteTypeVO):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getPublishedTemplate",
                                                                                 myHandlers.getPublishedTemplateResultHandler, null);
            RemoteObjectUtil.callService(op1, p_filter.selectedOperateurId, p_filter.selectedDataTypeId, p_filter.selectedRecuperationModeId);*/
			
			var data:Object = new Object();
			data.selectedOperateurId = p_filter.selectedOperateurId;
			data.selectedDataType = p_filter.selectedDataTypeId;
			data.selectedRecuperationMode = p_filter.selectedRecuperationModeId;
			doRemoting(myHandlers.getPublishedTemplateResultHandler, key, code, data);
        }

        /**
         * Retourne la <b>liste des collectes</b> pour un client donné
         * @see fr.consotel.consoprod.M25.CollecteService.getCollecteClientList
         *
         */
        public function getCollecteClientList(key:String, code:String, idRacine:Number):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "getCollecteClientList",
                                                                                 myHandlers.getCollecteClientListResultHandler, null);
            RemoteObjectUtil.callService(op1, idRacine);*/
			var data:Object = new Object();
			data.racineId = idRacine;
			doRemoting(myHandlers.getCollecteClientListResultHandler, key, code, data);
        }

        /**
         *  Crée ou met à jour une collecte cliente
         *  @param vo : le Value Object du template
         *  si vo.id = 0 : Mode CREATION
         *  si vo.id !=0 : Mode UPDATE
         * @see fr.consotel.consoprod.M25.CollecteService.createEditCollecteClientVo()
         */
        public function createEditCollecteClientVo(key:String, code:String, vo:CollecteClientVO):void
        {
            /*var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "createEditCollecteClientVo",
                                                                                 myHandlers.createEditCollecteClientVoResultHandler, null);
            RemoteObjectUtil.callService(op1, vo.idRacine, vo.idcollectClient, vo.template.idcollecteTemplate, vo.libelleCollect, vo.libelleRoic, vo.identifiant,
                                         vo.mdp, vo.plageImport, vo.retroactivite.value, vo.activationAlerte.value, vo.delaiAlerte, vo.mailAlerte, vo.
                                         isActif.value, vo.userCreate, vo.userModif);*/
			var data:Object = new Object();
			data.p_idracine = vo.idRacine;
			data.p_idcollectClient = vo.idcollectClient;
			data.p_idcollectTemplate = vo.template.idcollecteTemplate;
			data.p_libelleCollect = vo.libelleCollect;
			data.p_roic = vo.libelleRoic;
			data.p_identifiant = vo.identifiant;
			data.p_mdp = vo.mdp;
			data.p_plageImport = vo.plageImport;
			data.p_retroactivite = vo.retroactivite.value;
			data.p_activiationAlerte = vo.activationAlerte.value;
			data.p_delaiAlerte = vo.delaiAlerte;
			data.p_mailAlerte = vo.mailAlerte;
			data.p_isActif = vo.isActif.value;
			data.p_userCreate = vo.userCreate;
			data.p_userModif = vo.userModif;

			doRemoting(myHandlers.getCollecteClientListResultHandler, key, code, data);
			
			
        }

        /**
         *  Test les login/mdp  de connexion à l'intranet pour la récuperation
         *  des données
         *
         */
        public function testIdentification(intranet_url:String, login:String, password:String):void
        {
            var op1:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, COLDFUSION_FOLDER_PATH, "testIdentification",
                                                                                 myHandlers.testIdentificationResultHandler, null);
            RemoteObjectUtil.callService(op1, intranet_url, login, password);
        }
    }
}