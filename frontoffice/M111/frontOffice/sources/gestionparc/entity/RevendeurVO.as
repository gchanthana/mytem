package gestionparc.entity
{

	[Bindable]
	public class RevendeurVO
	{

		public var idRevendeur:int=0;
		public var idGpeClient:int=0; // racine du fournisseur 

		public var nom:String="";

		public var adresse:String="";
		public var codePostale:String="";
		public var commune:String="";
		public var pays:String="";

		public var typeRevendeur:int=0; // fabricant ou fournisseur

	}
}