	<cfset biServer=APPLICATION.BI_SERVICE_URL>
	
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDMASTER">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.IDRACINE_MASTER>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDGROUPE_CLIENT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE.ID_GROUPE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEDEB">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=LsdateFormat(FORM.DATEDEB,"YYYY/MM/DD")>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEFIN">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=LsdateFormat(FORM.DATEFIN,"YYYY/MM/DD")>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>	
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_OPERATEURID">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.OPERATEURID>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_ORDER_BY_TEXTE_COLONNE">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.ORDER_BY_TEXTE_COLONNE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[6]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_ORDER_BY_TEXTE">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.ORDER_BY_TEXTE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[7]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_SEARCH_TEXT_COLONNE">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.SEARCH_TEXT_COLONNE>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[8]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_SEARCH_TEXT">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.SEARCH_TEXT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[9]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_OFFSET">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.OFFSET>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[10]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_LIMIT">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=FORM.LIMIT>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[11]=ParamNameValue>
	
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_LANG">
	<cfset t=ArrayNew(1)>   
	<cfset t[1]=SESSION.USER.GLOBALIZATION>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[12]=ParamNameValue>
	
	<cfset myParamReportRequest=structNew()>
	
	<cfset myParamReportRequest.reportAbsolutePath="/consoview/M36/Liste des Factures fn_ListeFacuresRacine/Liste des Factures fn_ListeFacuresRacine.xdo">	
	
	<cfset myParamReportRequest.attributeLocale="#SESSION.USER.GLOBALIZATION#">
	<cfset myParamReportRequest.attributeFormat=LCase(FORM.FORMAT)>
	<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
	<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
	<cfset myParamReportParameters=structNew()>
	<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
	
	<cfset myParamReportParameters.userID="consoview">
	<cfset myParamReportParameters.password="public">
	<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport" argumentCollection="#myParamReportParameters#"/>
	<cfset reporting=createObject("component","fr.consotel.consoview.api.Reporting")>
	<cfset filename = "Liste_Factures_" & 
			replace(session.perimetre.raison_sociale,' ','_',"all") & reporting.getFormatFileExtension(FORM.FORMAT)>
			
	<cfheader name="Content-Disposition" value="attachment;filename=#filename#" charset="utf-8">	
	<cfif compareNoCase(trim(FORM.FORMAT),"csv") eq 0>
		<cfcontent type="text/csv; charset=utf-8" variable="#resultRunReport.getReportBytes()#">
		<cfelse>
		<cfcontent type="#resultRunReport.getReportContentType()#" variable="#resultRunReport.getReportBytes()#">		
	</cfif>

