package univers.facturation.suivi.controles.controls
{
    import flash.events.EventDispatcher;
    
    import mx.collections.ArrayCollection;
    import mx.resources.ResourceManager;
    
    import univers.facturation.suivi.controles.vo.Facture;
    import univers.facturation.suivi.controles.vo.GroupeProduitControle;
    import univers.facturation.suivi.controles.vo.LigneFacturation;
    import univers.facturation.suivi.controles.vo.ParamsRecherche;
    import univers.facturation.suivi.controles.vo.Ressource;

    [Bindable]
    public class AbstractControleFacture extends EventDispatcher
    {
        public static const PDF_FACTURECAT:String = "PDF_FACTURECAT";
        public static const PDF_FACTURE:String = "PDF_FACTURE";
        public static const XLS_LISTEFACTURE:String = "XLS_LISTEFACTURE";
        public static const XLS_LISTEFACTURESUPP:String = "XLS_LISTEFACTURESUPP";
        public static const CSV_LISTEFACTURE:String = "CSV_LISTEFACTURE";
        public static const CSV_LISTELIGNENONAFFECTEESAC:String = "XLS_LISTELIGNENONAFFECTEESAC";
        public static const TYPE_THEME_ABO:String = ResourceManager.getInstance().getString('M32', 'Abonnements');
        public static const TYPE_THEME_CONSO:String = ResourceManager.getInstance().getString('M32', 'Consommations');
        public static const PDF:String = "PDF";
        public static const CSV:String = "CSV";
        public static const XLS:String = "XLS";
        public static const BLOQUER_ACTION_FACTURE:uint = 1;
        public var prixReference:Number = 0;
        //Afficher le graph des évolutions des coûts
        public var _boolEvolution:Boolean = false;

        public function get boolEvolution():Boolean
        {
            return _boolEvolution;
        }
        ;

        public function set boolEvolution(bool:Boolean):void
        {
            _boolEvolution = bool;
            if (bool && ((_selectedFacture != null) && (_selectedFacture.periodeId > 0)))
                getEvolutionCout();
        }
        //liste des produits d'un groupe de produit
        //LIBELLE_PRODUIT
        //IDPRODUIT_CATALOGUE
        protected var _listeProduitsGroupeProduits:ArrayCollection = new ArrayCollection();

        public function set listeProduitsGroupeProduits(values:ArrayCollection):void
        {
            _listeProduitsGroupeProduits = values;
        }

        public function get listeProduitsGroupeProduits():ArrayCollection
        {
            return _listeProduitsGroupeProduits;
        }
        //les données d'evolution du graph des cout pour un cf
        protected var _evolutionCouts:ArrayCollection = new ArrayCollection();

        public function get evolutionCouts():ArrayCollection
        {
            return _evolutionCouts;
        }
        ;

        public function set evolutionCouts(evo:ArrayCollection):void
        {
            _evolutionCouts = evo;
        }
        ;
        //la liste des opérateurs du groupe
        protected var _listeOperateurs:ArrayCollection = new ArrayCollection();

        public function get listeOperateurs():ArrayCollection
        {
            return _listeOperateurs;
        }
        ;

        public function set listeOperateurs(lo:ArrayCollection):void
        {
            _listeOperateurs = lo;
        }
        ;
        //la liste des lignes de facturation ABO des produits controlables d'une facture 
        protected var _listeLignesFacturationsAbos:ArrayCollection = new ArrayCollection();

        public function get listeLignesFacturationsAbos():ArrayCollection
        {
            return _listeLignesFacturationsAbos;
        }

        public function set listeLignesFacturationsAbos(l:ArrayCollection):void
        {
            _listeLignesFacturationsAbos = l;
        }
        //les totaux
        protected var _totaux:ArrayCollection = new ArrayCollection();

        public function set totaux(t:ArrayCollection):void
        {
            _totaux = t;
        }

        public function get totaux():ArrayCollection
        {
            return _totaux;
        }
        //la liste des lignes de facturation CONSO des produits controlables d'une facture 
        protected var _listeLignesFacturationsConsos:ArrayCollection = new ArrayCollection();

        public function get listeLignesFacturationsConsos():ArrayCollection
        {
            return _listeLignesFacturationsConsos;
        }

        public function set listeLignesFacturationsConsos(l:ArrayCollection):void
        {
            _listeLignesFacturationsConsos = l;
        }
        //la liste des factures source pour le produit
        protected var _listeFactures:ArrayCollection = new ArrayCollection();

        public function get listeFactures():ArrayCollection
        {
            return _listeFactures;
        }
		       
        protected var _selectedLigneFacturation:LigneFacturation;

        public function get selectedLigneFacturation():LigneFacturation
        {
            return _selectedLigneFacturation;
        }

        public function set selectedLigneFacturation(lf:LigneFacturation):void
        {
            _selectedLigneFacturation = lf;
        }
        //le produit selectionne
        protected var _selectedFacture:Facture;

        public function get selectedFacture():Facture
        {
            return _selectedFacture;
        }

        public function set selectedFacture(f:Facture):void
        {
            _selectedFacture = f;
            if (boolEvolution)
                getEvolutionCout();
        }
        //le ratio
        protected var _cumulRatio:Number = 80;

        public function set cumulRatio(r:Number):void
        {
            _cumulRatio = r;
        }

        public function get cumulRatio():Number
        {
            return _cumulRatio;
        }
        //le sueil
        protected var _seuil:Number = 5;

        public function set seuil(r:Number):void
        {
            _seuil = r;
        }

        public function get seuil():Number
        {
            return _seuil;
        }
        //les abos hors inventaire
        protected var _abosHInv:Ressource;

        public function set abosHInv(abos:Ressource):void
        {
            _abosHInv = abos;
        }

        public function get abosHInv():Ressource
        {
            return _abosHInv;
        }
        //les consos hors inventaire
        protected var _consosHInv:Ressource;

        public function set consosHInv(consos:Ressource):void
        {
            _consosHInv = consos;
        }

        public function get consosHInv():Ressource
        {
            return _consosHInv;
        }
        //Etat de l'inventaire
        protected var _boolInventaire:Boolean = false;

        public function set boolInventaire(b:Boolean):void
        {
            _boolInventaire = b;
        }

        public function get boolInventaire():Boolean
        {
            return _boolInventaire
        }
        //liste des ressources hors inventaire pour une facture
        protected var _listeRessourcesHInv:ArrayCollection = new ArrayCollection();

        public function set listeRessourcesHInv(l:ArrayCollection):void
        {
            _listeRessourcesHInv = l;
        }

        public function get listeRessourcesHInv():ArrayCollection
        {
            return _listeRessourcesHInv;
        }
        //les abos dans inventaire non factures
        protected var _abosInv:Ressource;

        public function set abosInv(abos:Ressource):void
        {
            _abosInv = abos;
        }

        public function get abosInv():Ressource
        {
            return _abosInv;
        }
        //liste des ressources hors inventaire pour une facture
        protected var _listeRessourcesInv:ArrayCollection = new ArrayCollection();

        public function set listeRessourcesInv(l:ArrayCollection):void
        {
            _listeRessourcesInv = l;
        }

        public function get listeRessourcesInv():ArrayCollection
        {
            return _listeRessourcesInv;
        }
        //montant de la facture par feuille de l'organisation
        protected var _montantfactureEclateeByOrga:ArrayCollection;

        public function set montantfactureEclateeByOrga(l:ArrayCollection):void
        {
            if (_montantfactureEclateeByOrga != null)
                _montantfactureEclateeByOrga = null;
            _montantfactureEclateeByOrga = l;
        }

        public function get montantfactureEclateeByOrga():ArrayCollection
        {
            return _montantfactureEclateeByOrga;
        }
        //total de la facture pour une organisation
        protected var _totalfactureByOrga:Number = 0;

        public function set totalfactureByOrga(t:Number):void
        {
            _totalfactureByOrga = t;
        }

        public function get totalfactureByOrga():Number
        {
            return _totalfactureByOrga;
        }
        //total de la facture pour une organisation
        protected var _montantNonAffecte:Number = 0;

        public function set montantNonAffecte(mt:Number):void
        {
            _montantNonAffecte = mt;
        }

        public function get montantNonAffecte():Number
        {
            return _montantNonAffecte;
        }
        //permet l'incrementation du tableau contenant les paramètres à envoyer à la procédure**************************************
        protected var _arrayNumber:Number = 0;

        public function set arrayNumber(an:Number):void
        {
            _arrayNumber = an;
        }

        public function get arrayNumber():Number
        {
            return _arrayNumber;
        }
        //Part du montant total analytique par rapport au montant facture papier
        protected var _percentMontantViseAnalytique:Number = 0;

        public function set percentMontantViseAnalytique(p:Number):void
        {
            _percentMontantViseAnalytique = p;
        }

        public function get percentMontantViseAnalytique():Number
        {
            return _percentMontantViseAnalytique;
        }
        //La liste des organisations
        protected var _listeOrganistaion:ArrayCollection;

        public function set listeOrganistaion(l:ArrayCollection):void
        {
            if (_listeOrganistaion != null)
                _listeOrganistaion = null;
            _listeOrganistaion = l;
        }

        public function get listeOrganistaion():ArrayCollection
        {
            return _listeOrganistaion;
        }
        //Liste de sauvegarde pour construire le commentaire du visa analytique 
        private var _visaAnalytiqueleSaveList:ArrayCollection;

        public function set visaAnalytiqueleSaveList(l:ArrayCollection):void
        {
            if (_visaAnalytiqueleSaveList != null)
                _visaAnalytiqueleSaveList = null;
            _visaAnalytiqueleSaveList = l;
        }

        public function get visaAnalytiqueleSaveList():ArrayCollection
        {
            return _visaAnalytiqueleSaveList;
        }
        //L'organisation selectionnée pour le contrôle analytique
        protected var _seletcedOrganisation:Object;

        public function set seletcedOrganisation(orga:Object):void
        {
            _seletcedOrganisation = orga;
        }

        public function get seletcedOrganisation():Object
        {
            return _seletcedOrganisation;
        }
        //Les params de recherche
        protected var _paramsRecherche:ParamsRecherche = new ParamsRecherche();

        //Initialise les parametres de recherche
        public function setParamsRecherches(etatControle:Number, etatInventaire:Number, etatExporte:Number, etatVise:Number, etatViseAna:Number, operateurId:Number, chaine:String, dateDebut:Date, dateFin:Date):void
        {
        }
		
		public function calculMontantFacture(tab:Array):void
		{
		}

        public function getListeOperateurs():void
        {
        }

        public function getListeOrganisations():void
        {
        }

        public function exporterLaFacture(type:String,bool:int):void
        {
        }

        public function updateVisaFacture():void
        {
        }

        public function updateVisaAnaFacture():void
        {
        }

        public function updateVisaAnaFactureEditer():void
        {
        }

        public function updateVisaControleFacture():void
        {
        }
		
		public function updateVisaControleFactureInventaire():void
		{
		}

        public function updateVisaExporteFacture():void
        {
        }

        public function validerControle():void
        {
        }

        public function initSectionControle():void
        {
        }

        public function controlerFacture():void
        {
        }

        public function exportERPFacture():void
        {
        }

        public function getJustificatif():void
        {
        }

        public function initSectionInventaire():void
        {
        }

        public function getRessourcesHorsInventaire():void
        {
        }

        public function getDetailRessourcesHorsInventaire(typeTheme:String = "tout"):void
        {
        }

        public function getRessourcesInventaire():void
        {
        }

        public function getDetailRessourcesInventaire(typeTheme:String = "tout"):void
        {
        }

        public function exporterDetailRessourcesHorsInventaire(typeTheme:String = "tout"):void
        {
        }

        public function exporterDetailRessourcesInventaire(typeTheme:String = "tout"):void
        {
        }

        public function exporterEvolutionCoutByCf(format:String = "CSV"):void
        {
        }

        public function exporterEvolutionRatioCoutByCf(format:String = "CSV"):void
        {
        }

        public function exporterDetailFactureByOrga(format:String = "CSV"):void
        {
        }

        public function exporterFactureByOrga(format:String = "CSV"):void
        {
        }

        public function exporterLignesNonAffecteesCA(format:String = "PDF"):void
        {
        }

        public function getDetailGroupeProduit(groupeproduit:GroupeProduitControle):void
        {
        }

        protected function getListeFactures():void
        {
        }

        protected function getEvolutionCout():void
        {
        }

        protected function exporterListeFacturesCSV(bool:int):void
        {
        }

        public function detailFactureByOrga(orga:Object):void
        {
        }

        public function factureByOrga(orga:Object):void
        {
        }

        public function totalFactureByOrga(orga:Object):void
        {
        }

        public function chargerResultatAnalityque(orga:Object):void
        {
        }

        public function construireMemoControleAnalytique():String
        {
            return ""
        }

        public function construireSignature():String
        {
            return ""
        }

        public function exportContent(queryType:String, typeTheme:String = "tout"):void
        {
        }
		
		public function rapportProduitErreur():void
		{
		}
		
    }
}