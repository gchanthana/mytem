<!---
Package : fr.consotel.consoview.rapports.detailsConsoGlobal
--->
<cfcomponent name="RapportDetailsConsoGlobalGroupeLigneStrategy">
	<cffunction name="getReportData" access="private" returntype="query" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset RapportParams.date_m = LSDateFormat(ParseDateTime(RapportParams.DATEDEB),"yyyy/mm/dd")>
		<cfset RapportParams.date_m2=
				LSDateFormat(DateAdd("m",RapportParams.periodicite,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_un=
				LSDateFormat(DateAdd("m",-1,ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_m_moins_deux=
				LSDateFormat(DateAdd("m",(-1 - (RapportParams.periodicite)),ParseDateTime(RapportParams.DATEDEB)),"yyyy/mm/dd")>
		<cfset RapportParams.date_debut_annee = year(ParseDateTime(RapportParams.DATEDEB)) & "/01/01">
		<cfif periodicite eq 0>
			<cfset RapportParams.chaine_date=
					LsDateFormat(ParseDateTime(RapportParams.DATE_M),"mmmm yyyy")>
		<cfelse>
			<cfset RapportParams.chaine_date=
					LsDateFormat(ParseDateTime(RapportParams.DATE_M),"mmmm") & "/" & LsDateFormat(ParseDateTime(RapportParams.DATE_M2),"mmmm yyyy") >
		</cfif>
       <cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_REP_V3.REP_DETAIL_CONSO_LIG">
	        <cfprocparam  cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam value="#ID_PERIMETRE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#RapportParams.DATE_M#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M2#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M_MOINS_UN#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocparam  value="#RapportParams.DATE_M_MOINS_DEUX#" cfsqltype="CF_SQL_VARCHAR">
	        <cfprocresult name="qGetReportData">
        </cfstoredproc>
		<cfreturn qGetReportData>
	</cffunction>
	
	<cffunction name="displayRapport" access="public" returntype="void" output="true">
		<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
		<cfargument name="RapportParams" required="true" type="struct"/>
		<cfset NEW_DATEDEB = parseDateTime(RapportParams.DATEDEB)>
		<!---====== FORMATAGE DES DATES DE DEBUT ET DE FIN ======--->
		<cfset DATE_YEAR_YYYY = datePart("YYYY",rapportParams.DATEDEB)>
		<cfset DATE_MONTH_M = datePart("M",rapportParams.DATEDEB)>
		<cfset rapportParams.DATEDEB =
				LsDateFormat(createDate(DATE_YEAR_YYYY,DATE_MONTH_M,1),"YYYY/MM/DD")>
		<!---====================================================--->
		<cfset qGetData = getReportData(ID_PERIMETRE,RapportParams)>
		<cfif #qGetData.recordcount# gt 0>
			<cfif UCASE(arguments.RapportParams.FORMAT) EQ "CSV">
				<cfset filename="Detail_des_Appels_" & replace(session.perimetre.raison_sociale,' ','_',"all") & ".csv">
				<cfset exportService=createObject("component","fr.consotel.consoview.api.reporting.export.ExportService")>
				<cfset exportService.exportQueryToText(qGetData,filename)>
			<cfelse>
				<cfset total_minutes=0>
				<cfset total_nombre=0>
				<cfset total_montant=0>
				<cfoutput query="qGetData">
					<cfset total_minutes=total_minutes+duree_m>
					<cfset total_montant=total_montant+montant_m>
					<cfset total_nombre=total_nombre+nombre_appel>
				</cfoutput>
				<cfset cfrPath = "./rapport_detail_conso_par_compte_de_facturation.cfr">
				<cfreport format="#RapportParams.format#" template="#cfrPath#" query="qGetData">
					<cfreportparam name="total_minutes" value="#total_minutes#">
					<cfreportparam name="total_montant" value="#total_montant#">
					<cfreportparam name="total_nombre" value="#total_nombre#">
					<cfreportparam name="client_raison_sociale" value="#raison_sociale#">
					<cfreportparam name="date_rapport" value="#RapportParams.chaine_date#">
					<cfreportparam name="idref_client" value="#id_perimetre#">
				</cfreport>
			</cfif>
		<cfelse>
			<cfoutput><center><strong><h2>Aucune donn&eacute;es r&eacute;pondant &agrave; votre demande.</h2></strong></center></cfoutput>
		</cfif>
	</cffunction>
</cfcomponent>
