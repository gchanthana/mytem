 package composants.tb.tableaux
{
	import mx.events.FlexEvent;
	import mx.core.UIComponent;
	import mx.utils.ArrayUtil;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;	
	import mx.collections.ArrayCollection;	
	import composants.tb.tableaux.renderer.*;
	import flash.events.Event;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.AbstractOperation;	
	
	public class TableauAboSegment extends TableauAbo
	{
		private var _segment : String;
		
		private var _title : String;
		
		private var t : tableauAboAccueil = new tableauAboAccueil();
		
		/**
		* Constructeur 
		**/
		public function TableauAboSegment(ev : TableauChangeEvent = null){
				super();
				
				_segment = ev.SEGMENT;
				_title = "Abonnements Segment "+ev.SEGMENT.toLowerCase();				
				addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/*---------- Protected -----------------------*/
						
		//initialisation du composant
		protected override function init(fe :  FlexEvent):void{
			  myTabContener.addChildAt(t,1);
			  //update();
			  title = _title;
		}
		
		protected function drawTabs():void{
			t.rep.dataProvider = dataProviders;
			if (t.rep.dataProvider != null)	
				for (var i:int = 0; i < t.rep.dataProvider.length; i++){
						//myGrid[i].addEventListener(Event.CHANGE,_changeHandlerFunction);
						t.myGrid[i].addEventListener(Event.CHANGE,foo);	
						t.myGrid[i].columns[0].itemRenderer = null;	
						t.myGrid[i].columns[2].itemRenderer = null;			
				}  
			
			myFooterGrid.columns[1].headerText = nf.format(_total);
		}
		
		//formatte les donnees
		protected override function _formatterDataProvider(d: ArrayCollection):ArrayCollection{		
									
			var tmpCollection : ArrayCollection = new ArrayCollection();
			var o : Object;
		
			var segmentArray : Array = new Array();
			
			_total = 0;
			
			for	(o in d){		
				segmentArray.push(formateObject(d[o]));							   
			}
				
			var themes : Array = segmentArray.filter(filterFunc);
			
			_total = _total +  calculTotal(themes,"MONTANT_TOTAL");			
			
			tmpCollection.addItem(themes);
			 
			 	  					
			return tmpCollection;
		}  	
		
		private function filterFunc(element:*, index:int, arr:Array):Object{
			return element.SEGMENT.toUpperCase() == _segment.toUpperCase();
		}
		
		protected override function formateObject(obj : Object):Object{
			var o : Object = new Object();		
			o["LIBELLE"] = obj.theme_libelle;
			o["SEGMENT"] = obj.segment_theme;
			o["QUANTITE"] = nf.format(obj.qte);
			o["TYPE_THEME"] = obj.type_theme;
			o["SUR_THEME"] = obj.sur_theme;
			o["MONTANT_TOTAL"]= obj.montant_final;
			o["TYPE"] = "THEME";	
			o["ID"] = obj.idtheme_produit;				
			return o;
		}	
		//-------Chargement des donnees----------- remoting ---------------------------------------------------//
		// Chargement des données par remoting
		protected override function chargerDonnees():void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.tb.accueil.facade",
																			"getAboBySegment",
																			chargerDonnneesResultHandler,null);
			
			
			RemoteObjectUtil.callService(op,_segment);			
		}		
						
	}
	
}