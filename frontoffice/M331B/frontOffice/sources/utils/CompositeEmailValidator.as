package utils
{
	import mx.utils.StringUtil;

	public class CompositeEmailValidator
	{
		public function CompositeEmailValidator()
		{
		}
		
		/**
		 * permet de savoir si le mail est valide
		 */		
		public function doValidationMail(emailsString:String):Boolean
		{
			var emails:Array=emailsString.split(","); // convertir la chaine en tableau
			var retour :Boolean=true;		
			for each (var email:String in emails)
			{
				var emailExpression:RegExp=/^([a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4})$/;
				email=StringUtil.trim(email);
				if (!emailExpression.test(email))
				{
					retour=false;
				}
			}
			return retour;
		}
	}
}
