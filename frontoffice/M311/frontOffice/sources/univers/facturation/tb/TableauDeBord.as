package univers.facturation.tb
{
	import composants.tb.pages.IPage;
	import composants.tb.pages.PageFactory;
	import composants.tb.pages.TbAccueil;
	import composants.tb.pages.TbAccueilSegment;
	import composants.tb.periode.AMonth;
	import composants.tb.periode.PeriodeEvent;
	import composants.tb.rapports.RapportEvent;
	import composants.tb.recherche.RechercheEvent;
	import composants.tb.tableaux.TableauChangeEvent;
	import composants.tb.tableaux.services.ThemeServices;
	import composants.util.ConsoviewFormatter;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Label;
	import mx.controls.Spacer;
	import mx.events.FlexEvent;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import univers.facturation.tb.event.CheminOrgaEvent;
	import univers.facturation.tb.service.CheminOrgaService;

	public class TableauDeBord extends TableauDeBordIHM
	{
		private var op:AbstractOperation;

		private static const millisecondsPerDay:int=1000 * 60 * 60 * 24;

		private static var _editable:Boolean=false; //le mode consultaion ou edition

		[ArrayElementType("univers.facturation.tb.com.pages.IPage")]
		private var pageArray:Array=new Array(); // un tableau de pages

		private var currentPage:IPage; //une reference à la page courrante
		private var nextPage:IPage; //une reference à la page courrante

		private var titleMaxLen:int=70; // la longueur maximum du titre pour la page 	
		private var currentTitleLen:int=0; // la longueur maximum du titre pour la page 	

		//private var pSelector : PeriodeSelector = new PeriodeSelector();// la fenetre pour selectioner la periode

		private var pf:PageFactory=new PageFactory(); //createur de page

		private var isInit:Boolean;
		private var currentSegment:String; //le segment
		private var currentIdSegment:Number;  // id segment

		[Bindable] private var _previousDate:Date; //date de la mois precedent

		[Bindable] public static var libellePeriode:String;

		[Bindable] private var _moisDeb:String; //le debut de la periode 

		[Bindable] private var _moisFin:String; //la fin de la periode

		private var _perimetre:String; //le perimetre 
		private var _famille:String="Complet"; //le segment
		private var _modeSelection:String; //le mode de selection complet ou partiel
		private var _identifiant:String; //l'identifiant du groupe, de la sous-tete ... pour lequel on génère le tableau de bord.
		private var myDateFormatter:DateFormatter;
		private var _thmServices:ThemeServices=new ThemeServices();
		private var serviceCheminOrga:CheminOrgaService=new CheminOrgaService();

		public function getUniversKey():String
		{
			return "FACT";
		}

		public function getFunctionKey():String
		{
			return "FACT_DASHBOARD";
		}

		protected function afterCreationComplete(event:FlexEvent):void
		{
			init(event);
			trace("(TableauDeBord) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
		}

		public function afterPerimetreUpdated():void
		{
			onPerimetreChange();
			trace("(TableauDeBord) Perform After Perimetre Updated Tasks");
		}

		public function TableauDeBord()
		{
			super();
			trace("(TableauDeBord) Instance Creation");
			isInit=true;
			this.creationPolicy="all";

			_perimetre=CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			_identifiant=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX.toString();
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function themeLoadedHandler(e:Event):void
		{
			if (CvAccessManager.getSession().INFOS_DIVERS == null)
				CvAccessManager.getSession().INFOS_DIVERS=new Object();

			CvAccessManager.getSession().INFOS_DIVERS.THEME=new ArrayCollection(_thmServices.listeTheme.source);
			CvAccessManager.getSession().INFOS_DIVERS.SURTHEME=new ArrayCollection(_thmServices.listeSurTheme.source);

			var obj:Object=CvAccessManager.getSession().INFOS_DIVERS;
		}

		/**
		*Mise à jour du segment
		**/
		public function updateSegment(e:TableauChangeEvent):void
		{
			currentSegment=e.SEGMENT;
			chargerDonnees();
		}

		[Inspectable(enumeration="complet,partiel")]
		public function set modeSelection(modeSelection:String):void
		{
			_modeSelection=modeSelection;
		}

		/**
		 * Mode Edition ou Consultation
		 * @param m Le mode false = Consultaion, true = Edition
		 * */
		[Inspectable(enumeration="true,false")]
		public static function set editable(m:Boolean):void
		{

			_editable=m;
		/* switch (parseInt(b)){
			case 0 : _editable = false;break;
			case 1 : _editable = true;break;
			default : _editable = false;break;
		} */
		}


		public static function get editable():Boolean
		{
			return _editable;
		}

		/**
		 * Famille Telecom Tous, fixe,mobile, data
		 * @param f la famille telecom
		 * */
		[Inspectable(enumeration="tous,fixe,mobile,data")]
		public function set famille(f:String):void
		{
			_famille=f;
		}

		public function update(perimetre:String, identifiant:String=null, segment:String="Complet"):void
		{
			_perimetre=perimetre;
			_identifiant=identifiant;
			currentSegment=segment;
			chargerDonnees();
		}

		/**
		* Reset le compossant, remet toutes les variables a 0, interromp les remotings en cours
		**/
		public function clean():void
		{
			isInit=true;
			pageArray.forEach(cleanPagesFunction);

			myRechercheCps.clean();
		}

		public function onPerimetreChange():void
		{
			trace("TableauDeBord onPerimetreChange()");
			clean();

			mySelector.onPerimetreChange();

			initDate();
			setSelectedPeriode(_moisDeb, _moisFin);
			removeAllPages();
			isInit=true;
			doPreInit();
			_perimetre=CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			_identifiant=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX.toString();
			update(_perimetre, _identifiant, _famille);
		}

		public function onModeChange():void
		{

		}

		public function onFamilleChange():void
		{
			clean();
			initDate();
			removeAllPages();
			isInit=true;
			doPreInit();
			_perimetre=CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE;
			_identifiant=CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX.toString();
			update(_perimetre, _identifiant, _famille);
		}

		private function setGoodDateLocale(myDate:String):String
		{
			var jourD:String=myDate.substr(0, 2);
			var moisD:String=myDate.substr(3, 2);
			var anneeD:String=myDate.substr(6, 4);

			var tmp:Number=new Number(moisD);
			tmp--;

			var myDateTmp:Date=new Date(anneeD, tmp, jourD);

			myDateFormatter=new DateFormatter();
			myDateFormatter.formatString=ResourceManager.getInstance().getString('M311', 'DD_MMMM_YYYY');

			var myDateRes:String=myDateFormatter.format(myDateTmp);

			switch (moisD)
			{
				case "01":
					myDateRes=myDateRes.replace("January", ResourceManager.getInstance().getString('M311', 'MMMM_January'));
					break;
				case "02":
					myDateRes=myDateRes.replace("February", ResourceManager.getInstance().getString('M311', 'MMMM_February'));
					break;
				case "03":
					myDateRes=myDateRes.replace("March", ResourceManager.getInstance().getString('M311', 'MMMM_March'));
					break;
				case "04":
					myDateRes=myDateRes.replace("April", ResourceManager.getInstance().getString('M311', 'MMMM_April'));
					break;
				case "05":
					myDateRes=myDateRes.replace("May", ResourceManager.getInstance().getString('M311', 'MMMM_May'));
					break;
				case "06":
					myDateRes=myDateRes.replace("June", ResourceManager.getInstance().getString('M311', 'MMMM_June'));
					break;
				case "07":
					myDateRes=myDateRes.replace("July", ResourceManager.getInstance().getString('M311', 'MMMM_July'));
					break;
				case "08":
					myDateRes=myDateRes.replace("August", ResourceManager.getInstance().getString('M311', 'MMMM_August'));
					break;
				case "09":
					myDateRes=myDateRes.replace("September", ResourceManager.getInstance().getString('M311', 'MMMM_September'));
					break;
				case "10":
					myDateRes=myDateRes.replace("October", ResourceManager.getInstance().getString('M311', 'MMMM_October'));
					break;
				case "11":
					myDateRes=myDateRes.replace("November", ResourceManager.getInstance().getString('M311', 'MMMM_November'));
					break;
				case "12":
					myDateRes=myDateRes.replace("December", ResourceManager.getInstance().getString('M311', 'MMMM_December'));
					break;
			}

			return myDateRes;
		}

		/**
		* Fonction qui permet d'affecter la période sur laquelle on souhaite travailler,
		* de mettre à jour le titre du Tableau de bord et tous les composant de la page.
		*
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		*
		* */
		public function setSelectedPeriode(moisDebut:String, moisFin:String):void
		{
			myPeriodeLbl.text=ResourceManager.getInstance().getString('M311', 'P_riode_de_consommation__') + setGoodDateLocale(moisDebut) + ResourceManager.getInstance().getString('M311', '_au_') + setGoodDateLocale(moisFin);
		}

		//fait un clean sur tous les elements du tableau pageArray
		private function cleanPagesFunction(element:*, index:Number, arr:Array):void
		{
			element.clean();
			element=null;
		}

		/**
		 * Ajoute une Page au à l'univers Facturation, l'initilalise avec les données de périmetre et access
		 * @param page : Page un objet qui implemente les fonctions rafraichir,setperimetre, setperiode,setLigneID,affectChangeHandler,affectChartClickHandler,affectColletionChangeHandler			 *
		 * */
		private function addPage(page:IPage):void
		{
			if (pageArray.length > 0)
			{
				DisplayObject(pageArray[pageArray.length - 1]).visible=false;
			}

			page.goToNextPageHandler(goToNextPage);
			//on ajoute les listeners a la page

			//On recharge les donnees de la page au ShowEvent;			
			page.addEventListener(FlexEvent.SHOW, initPAge);

			//On charge les donnees de la page au creation complete
			page.addEventListener(FlexEvent.CREATION_COMPLETE, initPAge);

			//On change de page, au click sur un tableau		
			page.addEventListener("tableauChange", goToNextPage);

			serviceCheminOrga.model.addEventListener(CheminOrgaEvent.ORGA, cheminOrgaHandler);
			//page.updatePeriode(moisDeb,moisFin);

			pageArray.push(page);
			setTitre(page.getLibelle());
			//EffectProvider.FadeThat(DisplayObject(page));									
			myPageContener.addChild(DisplayObject(page));
		}

		//supprimer toutes les pages qui suivent la page passée en paramètre
		private function removePage(page:IPage):void
		{
			myPageContener.removeChild(DisplayObject(page));
			myTitleContener.removeChildAt(pageArray.length);
		}

		//Affiche la page suivante
		
		// modifié par samer
		
		protected function goToNextPage(tce:TableauChangeEvent):void
		{
			if (Number(tce.MONTANT_TOTAL) + parseInt(tce.QUANTITE) != 0)
			{
				tce.IDENTIFIANT=_identifiant;
				currentSegment=tce.SEGMENT;// c'est le segment
				
				(tce.IDSEGMENT_THEME !=null) ? currentIdSegment=int(tce.IDSEGMENT_THEME) : 	currentIdSegment=-1;
				
				currentPage=pf.createPage(tce);
				tce.SOURCE.selectedIndex=-1;
				addPage(currentPage);

				if (tce.TYPE == "SEGMENT")
					currentPage.addEventListener("tbTotal", setTotalHandler);
				else
					unSetTotal();

				callLater(isPossibleExport);
			}
		}


		/********
		* Affiche la page selectionée au click sur le menu
		* @param me
		*********/
		public function goToSelectedPage(me:MouseEvent):void
		{

			currentTitleLen=currentTitleLen - (pageArray[pageArray.length - 1].getLibelle().length + 2);

			var index:int=parseInt(me.currentTarget.id.split("-")[1]);
			var lastIndex:int=pageArray.length - 1;
			var i:int;

			for (i=lastIndex; i > index; i--)
			{
				try
				{
					var p:IPage=pageArray.pop();

					removePage(p); //supprimer la page

					currentTitleLen=currentTitleLen - (p.getLibelle().length + 2);

				}
				catch (error:Error)
				{
					trace(error.name, error.message);
				}
			}
			DisplayObject(pageArray[pageArray.length - 1]).visible=true;
			currentPage=pageArray[pageArray.length - 1];


			isPossibleExport();

			// voir la restriction au niveau segment//
			if ((currentPage.getLibelle().toUpperCase() == ResourceManager.getInstance().getString('M311', 'ACCUEIL')) && (currentSegment.toUpperCase() != "COMPLET"))
				currentSegment="Complet";
			else
				currentSegment=currentSegment;

			chargerDonnees();
		}

		//formate le titre 
		private function setTitre(t:String):void
		{
			var max:int=titleMaxLen;
			var cur:int=t.length + 2;
			var lb:Label=new Label();

			lb.id="myLabeltitre-" + (pageArray.length - 1).toString();

			if (lb.id.split("-")[1] == "0")
				lb.text=t;
			else
				lb.text="> " + t;

			currentTitleLen=currentTitleLen + lb.text.length;

			lb.setStyle("fontWeight", "bold");
			lb.buttonMode=true;
			lb.useHandCursor=true;
			lb.mouseChildren=false;
			lb.addEventListener(MouseEvent.CLICK, goToSelectedPage);
			lb.toolTip=t;
			myTitleContener.addChild(lb);

			//coupe le titre et rajoute trois ... si c'est trop long
			if (currentTitleLen > max)
				lb.text="> " + t.substr(0, max - cur - 6) + " ...";

		}

		private function setPeriode():void
		{
			if (myInfoContener.numChildren > 0)
				myInfoContener.removeChildAt(0);

			var libelle:Label=new Label();
			libelle.text=ResourceManager.getInstance().getString('M311', 'P_riode_de_consommation__') + setGoodDateLocale(_moisDeb) + ResourceManager.getInstance().getString('M311', '_au_') + setGoodDateLocale(_moisFin);
			libelle.toolTip=ResourceManager.getInstance().getString('M311', 'Du__') + _moisDeb + ResourceManager.getInstance().getString('M311', '_au_') + _moisFin;
			libelle.setStyle("fontWeight", "bold");
			libelle.setStyle("fontSize", "12");
			myInfoContener.addChildAt(libelle, 0);
			TableauDeBord.libellePeriode=libelle.text;
		}

		private function setTotal(nbr:Number):void
		{
			var myTotal:String=ConsoviewFormatter.formatNumber(nbr, 2);

			unSetTotal();

			var libelle:Label=new Label();
			var space:Spacer=new Spacer();
			space.width=50;

			var priceFormat:CurrencyFormatter=new CurrencyFormatter();
			priceFormat.precision=2;
			priceFormat.thousandsSeparatorTo=ResourceManager.getInstance().getString('M311', 'formateur_thousandsSeparatorTo');
			priceFormat.decimalSeparatorTo=ResourceManager.getInstance().getString('M311', 'formateur_decimalSeparatorTo');
			priceFormat.alignSymbol=ResourceManager.getInstance().getString('M311', 'formateur_align_symbol');
			priceFormat.currencySymbol=moduleTableauDeBordE0IHM.currencySymbol;

			libelle.text=ResourceManager.getInstance().getString('M311', 'Total__Abonnements_et_Consommations____') + priceFormat.format(nbr);
			libelle.toolTip=ResourceManager.getInstance().getString('M311', 'Total__Abonnements_et_Consommations____') + priceFormat.format(nbr);
			libelle.setStyle("fontWeight", "bold");
			libelle.setStyle("fontSize", "12");

			myInfoContener.addChildAt(space, 1);
			myInfoContener.addChildAt(libelle, 2);
			//myTotalContener.addChildAt(libelle, 0);
		}

		private function unSetTotal():void
		{
			//if (myTotalContener.numChildren > 0) myTotalContener.removeChildAt(0);
			if (myInfoContener.numChildren > 1)
				myInfoContener.removeChildAt(1);
			if (myInfoContener.numChildren > 1)
				myInfoContener.removeChildAt(1);
		}

		//initialisation de l'univers facturation
		private function init(fe:FlexEvent):void
		{
			_thmServices.getListTheme();
			_thmServices.addEventListener("THEME_LOADED", themeLoadedHandler);

			preInit(null);
			initDate();
			mySelector.addEventListener("periodeChange", updatePeriode);
			btnRefresh.addEventListener(MouseEvent.CLICK, refreshDonnees);
			setSelectedPeriode(_moisDeb, _moisFin);
			myRapprotCps.addEventListener("export", exporterRapport);
			myRechercheCps.addEventListener("InitTableauDeBord", traiterRecherche);
			myRechercheCps.addEventListener("RetourRechercheEvent", resetTableauHandler);
			myRechercheCps.setParameters(_moisDeb, _moisFin);
			addEventListener(FlexEvent.ADD, panelRechercheSetVisibility);
			addEventListener(FlexEvent.REMOVE, panelRechercheSetVisibility);
			update(_perimetre, _identifiant, _famille);// famille = segment
		}

		private function refreshDonnees(e:Event):void
		{
			chargerDonnees();
		}

		private function chargerDonnees():void
		{
			try
			{
				op=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M311.accueil.facade", "init", initResultHandler, initFaultHandler);
				RemoteObjectUtil.callService(op, _perimetre, _modeSelection, _identifiant, _moisDeb, _moisFin, currentSegment);

			}
			catch (er:Error)
			{
				trace("vous devez choisir un périmetre");
			}
		}

		private function initFaultHandler(fe:FaultEvent):void
		{
			trace(fe.fault.message, fe.fault.name);
		}

		private function initResultHandler(re:ResultEvent):void
		{

			setPeriode();
			setSelectedPeriode(_moisDeb, _moisFin);
			if (isInit)
			{
				var ev:TableauChangeEvent=new TableauChangeEvent("depart");
				var fp:PageFactory=new PageFactory();
				switch (_famille.toUpperCase())
				{
					case "COMPLET":	ev.TYPE="ACCUEIL"; break;
					case "FIXE"	  : ev.TYPE="SEGMENT"; break;
					case "MOBILE" : ev.TYPE="SEGMENT"; break;
					case "DATA"   :	ev.TYPE="SEGMENT"; break;
					default: ev.TYPE="ACCUEIL"; break;
				}

				ev.SEGMENT=currentSegment;
				ev.IDENTIFIANT=_identifiant;

				//myPeriodeChangeBt.addEventListener(MouseEvent.CLICK,showPeriodeSelector);			

				currentPage=fp.createPage(ev);
				currentPage.updatePeriode(_moisDeb, _moisFin);
				currentPage.updatePerimetre(_perimetre, _modeSelection, _identifiant);
				currentPage.addEventListener("tbTotal", setTotalHandler);
				addPage(currentPage);
				isInit=false;
				setLibelleTableauDeBord(_perimetre, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE);
			}
			else
			{
				currentPage.updatePerimetre(_perimetre, _modeSelection, _identifiant);
				currentPage.updatePeriode(_moisDeb, _moisFin);
				currentPage.updateSegment(currentSegment);
				currentPage.update();

				//voir pageFactory ...
				if (currentPage is TbAccueil || currentPage is TbAccueilSegment)
				{
					//UIComponent(currentPage).addEventListener("tbTotal",setTotalHandler);
					var MyEv:Event=new Event("tbTotal");
					currentPage.dispatchEvent(MyEv);
				}
				else
					unSetTotal();
			}
		}

		private function setTotalHandler(e:Event):void
		{
			setTotal(currentPage.getTotal());
		}

		//met a jour le libelle du tableau de bord
		private function setLibelleTableauDeBord(type:String, libelle:String):void
		{
			var cod:String;

			switch (type.toUpperCase())
			{
				case "SOUSTETE":
					cod=ResourceManager.getInstance().getString('M311', 'Chemin___') + formatPhoneNumber(libelle);
					break;
				case "GROUPELIGNE":
					cod=ResourceManager.getInstance().getString('M311', 'Groupe_de_lignes___') + libelle;
					break;
				default:
					cod=ResourceManager.getInstance().getString('M311', 'Groupe___') + CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE;
					break;
			}

			libelleTB.title=cod;
			if (cod.length > 140)
			{
				libelleTB.toolTip=cod;
			}
		}

		//formatage du telepone 
		private function formatPhoneNumber(s:String):String
		{
			var stringValue:String="";
			if (s != null)
			{
				if ((s.length == 10) && parseInt(s).toString().length == 9)
					stringValue=phft.format(s);
				else
					stringValue=s;
			}
			return stringValue;
		}

		private function initPAge(fe:FlexEvent):void
		{
			currentPage.updatePeriode(_moisDeb, _moisFin);
			currentPage.updatePerimetre(_perimetre, _modeSelection, _identifiant);
			currentPage.update();
		}


		private function updatePeriode(pe:PeriodeEvent):void
		{
			// a ne pas toucher avec l'internationlisation !!!
			var myFormatter:DateFormatter=new DateFormatter();
			myFormatter.formatString="DD/MM/YYYY";

			_moisDeb=myFormatter.format(pe.dateDeb);
			_moisFin=myFormatter.format(pe.dateFin);
			setSelectedPeriode(_moisDeb, _moisFin);

			myRechercheCps.setParameters(_moisDeb, _moisFin);
		}


		// Export des rapports
		private function exporterRapport(re:RapportEvent):void
		{
			currentPage.exporterRapport(re.format, libelleTB.title);
		}

		private function initDate():void
		{
			var periodeArray:Array=mySelector.getTabPeriode();
			var len:int=periodeArray.length;
			var month:AMonth;
			mySelector.setMini(2);

			month=periodeArray[len - 2];

			// a ne pas toucher avec l'internationlisation !!!
			var myFormatter:DateFormatter=new DateFormatter();
			myFormatter.formatString="DD/MM/YYYY";

			_previousDate=new Date(month.dateDebut.getFullYear(), month.dateDebut.getMonth() - 1, 1);
			_moisDeb=myFormatter.format(_previousDate);
			var tmpDate:Date=new Date(month.getDateFin().substr(6, 4), month.getDateFin().substr(0, 2), month.getDateFin().substr(3, 2))
			_moisFin=myFormatter.format(month.dateFin);

		}

		//verifier si l'export de donnée est possible
		private function isPossibleExport():void
		{
			var strg:String=currentPage.pageType.toUpperCase();
			if ((currentPage.pageType.toUpperCase() == "ACCUEIL"))
			{
				myRapprotCps.enableExport();
			}
			else if (currentPage.pageType.toUpperCase() == "PRODUIT")
			{
				myRapprotCps.enableExport();
			}
			else if (currentPage.pageType.toUpperCase() == "THEME")
			{
				myRapprotCps.enableExport(true, true, true);
			}
			else
			{
				//myRapprotCps.disableExport();
				myRapprotCps.enableExport(false, false, true);
			}
		}

		private function traiterRecherche(re:RechercheEvent):void
		{
			goToAccueil();
			//setLibelleTableauDeBord(re.perimetreRecherche,re.libelle);				
			update(re.perimetreRecherche, re.ID);
			getCheminOrga(re.perimetreRecherche, re.ID, re.libelle);
		}

		private function getCheminOrga(perimetreRecherche:String, idSous_tete:String, sous_tete:String):void
		{
			serviceCheminOrga.model.perimetreRecherche=perimetreRecherche;
			serviceCheminOrga.model.sous_tete=sous_tete;
			serviceCheminOrga.getCheminOrga(idSous_tete);
		}

		private function cheminOrgaHandler(ev:CheminOrgaEvent):void
		{
			setLibelleTableauDeBord(serviceCheminOrga.model.perimetreRecherche, serviceCheminOrga.model.chemin + '\\' + serviceCheminOrga.model.sous_tete);
		}

		private function panelRechercheSetVisibility(fe:FlexEvent):void
		{
			myRechercheCps.setVisibility();

		}

		private function removePanelsRecherche(fe:FlexEvent):void
		{
			myRechercheCps.removePanelsResultatRecherche();
		}

		private function preInit(fe:FlexEvent):void
		{
			doPreInit();
		}

		private function doPreInit():void
		{
			modeSelection="Complet";
			famille="Complet";

			if (CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
				TableauDeBord.editable=true;
			else
				TableauDeBord.editable=false;
		}

		//pour revenir au tableau de bord du perimetre Initiale apres une recherche
		private function resetTableauHandler(ev:Event):void
		{
			goToAccueil();
			setLibelleTableauDeBord(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_LIBELLE);
			update(CvAccessManager.getSession().CURRENT_PERIMETRE.TYPE_PERIMETRE, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX.toString(), currentSegment);
			myRechercheCps.deselctRecherche();
		}

		private function goToAccueil():void
		{
			var i:int;
			var len:int=pageArray.length;

			for (i=len - 1; i > 0; i--)
			{
				var p:IPage=pageArray.pop();
				removePage(p);
				currentTitleLen=currentTitleLen - (p.getLibelle().length + 2);

			}
			DisplayObject(pageArray[pageArray.length - 1]).visible=true;
			currentPage=pageArray[pageArray.length - 1];

			isPossibleExport();
			
			// voir la restriction au niveau segment
			if ((currentPage.getLibelle().toUpperCase() == ResourceManager.getInstance().getString('M311', 'ACCUEIL')) && (currentSegment.toUpperCase() != "COMPLET"))
				currentSegment="Complet";
			else
				currentSegment=currentSegment;
		}

		private function removeAllPages():void
		{
			var i:int;
			var len:int=pageArray.length;

			for (i=len - 1; i >= 0; i--)
			{
				var p:IPage=pageArray.pop();
				removePage(p);
				currentTitleLen=currentTitleLen - (p.getLibelle().length + 2);
			}

		}
	}
}