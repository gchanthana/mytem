﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	</cffunction>

	<!--- 
		Recupere les infos du site
	--->
	<cffunction name="getSiteEmplacement" access="remote" returntype="Array" output="false" hint="Recupere les infos du site">
		<cfargument name="idSite" required="true" type="numeric" />
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.get_site_emplacement">
				<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
				
				<cfprocresult name="site" 			resultset="1">
				<cfprocresult name="emplacements" 	resultset="2">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = site >
		<cfset p_retour[2] = emplacements >
			
		<cfreturn p_retour>
	</cffunction>
	
	<!--- 
		Recupere les infos des emplacements d'un site
	--->
	<cffunction name="getEmplacement" access="remote" returntype="Array" output="false" hint="Recupere les infos des emplacements d'un site">
		<cfargument name="idSite" required="true" type="numeric" />
			<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_M111B.get_emplacement">
				<cfprocparam  value="#idSite#" cfsqltype="CF_SQL_inTEGER" type="in">
				
				<cfprocresult name="emplacements" 	resultset="1">
		</cfstoredproc>
		
		<cfset p_retour = ArrayNew(1)>
		<cfset p_retour[1] = emplacements >
			
		<cfreturn p_retour>
	</cffunction>

	<!--- 
		Mis a jour ou ajout d'un nouveau site
	--->
	<cffunction name="setSiteEmplacement" access="remote" returntype="numeric" description="Permet de modifier les infos d un site">
	  
	    <cfargument name="IdSite" 			type="Numeric" required="true">
	    <cfargument name="libelleSite" 		type="string" required="true">
	    <cfargument name="xmlEmplacement" 	type="string" required="true">
	    <cfargument name="refSite" 			type="string" required="true">
	    <cfargument name="codeInterneSite" 	type="string" required="true">
	    <cfargument name="adrSite" 			type="string" required="true">
	    <cfargument name="adr2Site" 		type="string" required="true">
	    <cfargument name="cpSite" 			type="string" required="true">
	    <cfargument name="communeSite" 		type="string" required="true">
	    <cfargument name="commentaireSite" 	type="string" required="true">
		<cfargument name="idPaysSite" 		type="numeric" required="true">
		<cfargument name="idPool" 			type="numeric" required="true">
		
	    <cfstoredproc datasource="#session.offreDSN#" procedure="pkg_M111B.set_site_physique">
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#IdSite#" null="#iif((IdSite eq -1), de("yes"), de("no"))#"/>
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelleSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_CLOB" 	  value="#xmlEmplacement#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#refSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#codeInterneSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adrSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#adr2Site#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#cpSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#communeSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#commentaireSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPaysSite#" >
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idPool#" >
		    <cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
	    </cfstoredproc>
	    
		<cfreturn p_retour/>
	</cffunction>

</cfcomponent>