package univers.parametres.rago.CreationRegles.ihm
{
	import flash.events.Event;
	
	import mx.controls.ComboBox;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextArea;
	
	import univers.parametres.rago.CreationRegles.system.Wizzard;
	import univers.parametres.rago.entity.TypeRegle;

	public class SelectTypeImpl extends MainCreateRegleImpl
	{
		
//VARIABLES GLOBALES----
	
		public var _typeWizzard:Wizzard;
	
		public var rbgLineFollowRegle:RadioButtonGroup;
		public var rbgTypeRegle:RadioButtonGroup;
		
		public var rbTypaAnnuaire:RadioButton;
		public var rbOperateur:RadioButton;
		
		public var cboxOperateur:ComboBox;
		public var cboxCliente:ComboBox;
		public var txtAreaCommentaireNEwRegle:TextArea;
		
		private var _operateurResult:Object;
		private var _clientResult:Object;
		private var _textSaisie:String = "";

//FIN VARIABLES GLOBALES----

//PROPRIETEES PUBLICS----
		
		//Texte saisie dans le champs commentaire
		public function set textSaisie(value:String):void
		{ _textSaisie = value; }

		public function get textSaisie():String
		{ return _textSaisie; }
		
		//Résultat du combobox opérateur/annuaire
		public function set operateurResult(value:Object):void
		{ _operateurResult = value; }

		public function get operateurResult():Object
		{ return _operateurResult; }
		
		
		//Résultat du combobox client
		public function set clientResult(value:Object):void
		{ _clientResult = value; }

		public function get clientResult():Object
		{ return _clientResult; }

//FIN PROPRIETEES PUBLICS----

//METHODES PUBLICS----
		
		//Constructeur
		public function SelectTypeImpl()
		{}

//FIN METHODES PUBLICS----

//METHODES PROTECTED----

		//Appel à la procédure permettant de lister toutes les orga (annuaire, opérateur, cliente)
		protected function initSelectTypeIHM(event:Event):void
		{ 
			cboxOperateur.enabled = true;

			rules.fillOrganisation();
			rules.addEventListener("OrgaFind",executeResultListOrgaOpeIntoCbox);
		}
		
		//Appel a la fonction 'rdgAnnuaireChangeHandler' pour lister les orga annuaire
		protected function clickrdbtnHaut(event:Event):void
		{
			rbTypaAnnuaire.selected = true;
			rdgAnnuaireChangeHandler(event);
		}
		
		//Appel a la fonction 'rdgOperateurChangeHandler' pour lister les orga opérateur
		protected function clickrdbtnBas(event:Event):void
		{
			rbOperateur.selected = true;
			rdgOperateurChangeHandler(event);
		}
		
		//Enregistre le texte saisie dans le champs commentaires
		protected function txtCmtrChangeHandler(event:Event):void
		{
			textSaisie = txtAreaCommentaireNEwRegle.text;
			_typeWizzard.myRegle.REGLE_COMMENT = textSaisie;
		}
		
		//Affecte la liste des orga annuaire au combobox
		protected function rdgAnnuaireChangeHandler(event:Event):void
		{
			cboxOperateur.enabled = false;
			zAttributTypeRegleInRegle(TypeRegle.REGLE_ANNUAIRE);
			operateurResult = CboxOperateurAnnuaire = rules.ListOrgaAnu;
			operateurResult = cboxOperateur.selectedItem;
		}
		
		//Affecte la liste des orga opérateur au combobox
		protected function rdgOperateurChangeHandler(event:Event):void
		{
			cboxOperateur.enabled = true;
			zAttributTypeRegleInRegle(TypeRegle.REGLE_OPERATEUR);
			CboxOperateurAnnuaire = rules.ListOrgaOpe;
			operateurResult = cboxOperateur.selectedItem;
		}

		//Evènement lors du click sur le bouton 'faire suivre toutes les lignes'
		protected function rdgFollowChangeHandler(event:Event):void
		{ zAttributFollowInRegle(true); }
		
		//Evènement lors du click sur le bouton 'ne pas faire suivre toutes les lignes'
		protected function rdgNoFollowChangeHandler(event:Event):void
		{ zAttributFollowInRegle(false); }

		//Affecte le résultat de la sélection du combobox opérateur/annuaire à la propriétée opérateur/annuaire
		protected function cboxOpeCloseHandler(event:Event):void
		{ operateurResult = CboxOpeAnnuResult  = cboxOperateur.selectedItem; }
		
		//Affecte le résultat de la sélection du combobox client à la propriétée cliente
		protected function cboxClienteCloseHandler(event:Event):void
		{ clientResult = CboxClienteResult  = cboxCliente.selectedItem; }

//METHODES PROTECTED----

//METHODES PRIVATE----
		
		//Met dans le combobox 'opérateur/annuaire'la liste des opérateur ou annuaire
		//et liste dans le combobox 'client' la liste des orga cliente
		private function executeResultListOrgaOpeIntoCbox(event:Event):void
		{ 
			_typeWizzard.myRegle.BOOL_SUIVRE_AUTO = true;
			_typeWizzard.myRegle.TYPE_REGLE = TypeRegle.REGLE_OPERATEUR;
			CboxOperateurAnnuaire = rules.ListOrgaOpe;
			CboxCliente = rules.ListOrgaCliente; 
			
			operateurResult = CboxOpeAnnuResult = cboxOperateur.selectedItem;
			clientResult = CboxClienteResult = cboxCliente.selectedItem;
		}
		
		//Attribut la valeur opérateur ou annuaire à 'TYPE_REGLE'
		private function zAttributTypeRegleInRegle(typeRegle:String):void
		{	
			if(_typeWizzard.myRegle != null)
			{ _typeWizzard.myRegle.TYPE_REGLE = typeRegle; }
		}
		
		//Attribut la valeur boolean à true ou false à 'BOOL_SUIVRE_AUTO'
		private function zAttributFollowInRegle(followValue:Boolean):void
		{	
			if(_typeWizzard.myRegle != null)
			{ _typeWizzard.myRegle.BOOL_SUIVRE_AUTO = followValue; }
		}
		
		//Formateur string en bool
		private function zFormatStringInBoolean(strgValueToConvert:String):Boolean
		{
			if(strgValueToConvert == "true") { return true; }
			else { return false; }
		}
		
//METHODES PRIVATE----
		
	}
}