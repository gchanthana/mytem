package composants.util.poolsgestion
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import commandemobile.system.PoolGestionnaire;
	
	[Bindable]
	/**
	 * Aide pour les pools de gestions
	 * */
	public class PoolsGestionUtils
	{
		private var _listePools:ArrayCollection;		
		public function get listePools():ArrayCollection
		{
			return _listePools
		}		
		public function set listePools(values:ArrayCollection):void
		{
			_listePools = values;
		}
		 
		
		
		public function PoolsGestionUtils()
		{
		}
		
		
		public function fournirPoolsDuGestionnaire():void
		{
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																		"fournirPoolsDuGestionnaire", 
																		fournirPoolsDuGestionnaireResultHandler);
			RemoteObjectUtil.callService(op);
		} 
		
		protected function fournirPoolsDuGestionnaireResultHandler(event:ResultEvent):void
		{
			listePools = formatterData(event.result as ArrayCollection);
	    }
	    
	    private   function formatterData(values : ICollectionView):ArrayCollection{
	    	
	    	var retour : ArrayCollection = new ArrayCollection();
	    	
	    	
	    	if (values != null)
	    	{
	    		var cursor : IViewCursor = values.createCursor();
	    		while(!cursor.afterLast)
	    		{
	    			var poolObj : PoolGestionnaire = new PoolGestionnaire();
	    			poolObj.codeInterne_pool = cursor.current.CODE_INTERNE_POOL;
	    			poolObj.IDPool = cursor.current.IDPOOL;
	    			poolObj.libelle_pool = cursor.current.LIBELLE_POOL;
	    			poolObj.id_profil = cursor.current.IDPROFIL;
	    			poolObj.libelle_profil = cursor.current.LIBELLE_PROFIL;
	    			poolObj.commentaire_pool = poolObj.libelle_pool +" | "+ poolObj.libelle_profil;
	    			retour.addItem(poolObj);	    			
	    			cursor.moveNext();
	    		}	
	    	}
	    	
	    	return retour 
	    }
		
		

	}
}