package gestionparc.services.export
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.entity.SpecificationVO;
	
	import mx.rpc.AbstractOperation;

	public class ExportAllService
	{
		public var handler:ExportAllHandler;

		public function ExportAllService()
		{
			handler=new ExportAllHandler();
		}

		public function exporterViewParc():void
		{
			var op:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.export.ExportAll", "ViewParcGlobalExport", handler.exportHandler);
			
			RemoteObjectUtil.callService(op, 
											SpecificationVO.getInstance().idPool,							
											SpecificationVO.getInstance().keyWord,
											SpecificationVO.getInstance().idKeyWordScope,
											SpecificationVO.getInstance().keyWord2,
											SpecificationVO.getInstance().idKeyWordScope2,
											1, 
											SpecificationVO.getInstance().nbRowDataGrid, 
											SpecificationVO.getInstance().labelSort, 
											SpecificationVO.getInstance().idRestriction, 
											SpecificationVO.getInstance().orderByReq);
		}
	}
}