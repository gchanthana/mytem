package general.appliComposants.parametresuser
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.controls.Button;
	import mx.controls.Text;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import mx.validators.StringValidator;
	import mx.validators.Validator;
	
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	
	import general.event.UserEvent;
	import general.service.LoginServices;
	
	public class PopUpInfosUserImpl extends TitleWindowBounds
	{
		
		private var _servLogin						:LoginServices;
		private var _minLength						:int = 6;
		private var _maxLength						:int = 10;
		
		/*
		public static const  _patternPass			:String = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[_\W]).+$';
			^                  // the start of the string
			(?=.*[a-z])        // use positive look ahead to see if at least one lower case letter exists
			(?=.*[A-Z])        // use positive look ahead to see if at least one upper case letter exists
			(?=.*\d)           // use positive look ahead to see if at least one digit exists
			(?=.*[_\W])        // use positive look ahead to see if at least one underscore or non-word character exists// can replace with character set [-+_!@#$%^&*.,?]
			.+                 // gobble up the entire string
			$                  // the end of the string
		*/
		
		private var sv_passValidator 				:StringValidator = new StringValidator();
		private var sv_newPassValidator 			:StringValidator = new StringValidator();
		private var sv_confirmNewPassValid 			:StringValidator = new StringValidator();
		[Bindable]public var TXT_PASSFEATURES 		:String = ResourceManager.getInstance().getString('M00','New_password_must_be_8_characters_or_longer__');
		
		public var ti_currentPass					:TextInput;
		public var ti_newPass						:TextInput;
		public var ti_confirm_newpass				:TextInput;
		public var txt_error						:Text;
		public var btn_validate						:Button;
		public var btn_cancel						:Button;
		
		public function PopUpInfosUserImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initcc);
		}
		
		protected function initcc(event:FlexEvent):void
		{
			servLogin = new LoginServices();
			initListeners();
			initDisplay();
		}
		
		private function initDisplay():void
		{
			callLater(ti_currentPass.setFocus);
		}
		
		private function initListeners():void
		{
			btn_validate.addEventListener(MouseEvent.CLICK, onMouseValidateHandler);
			addEventListener(KeyboardEvent.KEY_DOWN, onKeyBoardValidateHandler);
			btn_cancel.addEventListener(MouseEvent.CLICK, onCloseHandler);
		}
		
		protected function onKeyBoardValidateHandler(evt:KeyboardEvent):void
		{
			if (evt.keyCode == Keyboard.ENTER)
				validerPasswords();				
		}
		
		protected function onMouseValidateHandler(evt:Event):void
		{
			validerPasswords();
		}
		
		private function validerPasswords():void
		{
			if(validateEnteredPassword())
			{
				if(ti_newPass.text == ti_confirm_newpass.text)
				{
					txt_error.text = '';
					if(validateSpecialPasswordCharacters())
					{
						// Appel de service de changement de passwrod
						servLogin.myDatas.addEventListener(UserEvent.USER_INFOS_UPDATED_EVENT, traiterResultatUpdate);
						servLogin.updateUserPassword(StringUtil.trim(ti_currentPass.text), StringUtil.trim(ti_newPass.text));
						this.onCloseHandler(null);
					}
				}
				else
				{
					txt_error.text = ResourceManager.getInstance().getString('M00','New_passwords_did_not_matches_');				
				}
			}
		}
		
		protected function traiterResultatUpdate(event:Event):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M00','Password_updated_') + ' ' + ResourceManager.getInstance().getString('M00','Un_email_vous_a__t__adress__'));		
		}
		
		private function validateEnteredPassword():Boolean
		{
			var result:Boolean = false;
			this.initValidators();
			txt_error.text = '';
			var validationResult:Array = Validator.validateAll([sv_passValidator, sv_newPassValidator, sv_confirmNewPassValid]);
			if (validationResult.length == 0)
				result = true;
			else
				result = false;
			
			return result;
		}
		
		private function initValidators():void
		{
			sv_passValidator.source = ti_currentPass;
			sv_passValidator.property = 'text';
			sv_passValidator.minLength = 1;
			sv_passValidator.maxLength = 50;
			sv_passValidator.requiredFieldError = ResourceManager.getInstance().getString('M00','Ce_champ_est_obligatoire');
			
			sv_newPassValidator.source = ti_newPass;
			sv_newPassValidator.property = 'text';
			sv_newPassValidator.minLength = 8;
			sv_newPassValidator.maxLength = 20;
			sv_newPassValidator.requiredFieldError = ResourceManager.getInstance().getString('M00','Ce_champ_est_obligatoire');
			sv_newPassValidator.tooShortError=ResourceManager.getInstance().getString('M00','This_password_is_shorter_than_');
			sv_newPassValidator.tooLongError=ResourceManager.getInstance().getString('M00','This_password_is_longer_than_');
			
			sv_confirmNewPassValid.source = ti_confirm_newpass;
			sv_confirmNewPassValid.property = 'text';
			sv_confirmNewPassValid.minLength = 8;
			sv_confirmNewPassValid.maxLength = 20;
			sv_confirmNewPassValid.requiredFieldError = ResourceManager.getInstance().getString('M00','Ce_champ_est_obligatoire');
			sv_confirmNewPassValid.tooShortError=ResourceManager.getInstance().getString('M00','This_password_is_shorter_than_');
			sv_confirmNewPassValid.tooLongError=ResourceManager.getInstance().getString('M00','This_password_is_longer_than_');
		}
		
		private function validateSpecialPasswordCharacters():Boolean
		{
			var result:Boolean = false;
			
			var patternPass:RegExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/;
			
			if (patternPass.exec(ti_newPass.text)==null){
				
				txt_error.text = ResourceManager.getInstance().getString('M00','Weak_password__') + TXT_PASSFEATURES;
				result = false;
			} 
			else
			{
				txt_error.text = '';
				result = true;
			}
			
			return result;
		}
		
		protected function onCloseHandler(event:Event):void
		{
			PopUpManager.removePopUp(this)
		}
		
		public function get servLogin():LoginServices { return _servLogin; }
		
		public function set servLogin(value:LoginServices):void
		{
			if (_servLogin == value)
				return;
			_servLogin = value;
		}
	}
}