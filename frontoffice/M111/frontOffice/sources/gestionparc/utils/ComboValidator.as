package gestionparc.utils
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	
	public class ComboValidator extends Validator
	{
		//----------- VARIABLES -----------//
		
		private var _rejectedValues		:Array = [];
//		private var _errorMessage		:String = "Option choisie invalide";
		
		
		//----------- METHODES -----------//
		
		/* */
		public function ComboValidator()
		{
			super();
		}
		
		/* override de la methode doValidation() */
		override protected function doValidation(value:Object):Array
		{
			var results		:Array 		= super.doValidation(value);
			var boolInvalid	:Boolean 	= false;
			
			if(rejectedValues)
			{
				if(rejectedValues.indexOf(value) != -1)
					boolInvalid = true;
			}
			if(boolInvalid)
				results.push(new ValidationResult(true,"","",super.requiredFieldError));
			
			return results;
		}
		
		
		//----------- GETTERS - SETTERS -----------//	
		
		[Inspectable]
		public function get rejectedValues():Array
		{
			return _rejectedValues;
		}
		public function set rejectedValues(value:Array):void
		{
			_rejectedValues = value;
		}
		
//		[Inspectable]
//		public function get errorMessage():String
//		{
//			return _errorMessage;
//		}
//		public function set errorMessage(value:String):void
//		{
//			_errorMessage = value;
//		}
	}
}