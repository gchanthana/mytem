<cfcomponent output="false" displayname="fr.consotel.consoview.M16.v2.CompteService">

	<!--- FOURNIT LA LISTE DES COMPTES ET SOUS COMPTES POUR UN GESTIONNAIRE, UN POOL ET UN OPÉRATEUR DONNÉ. --->

	<cffunction name="fournirCompteOperateurByLoginAndPoolV2" access="remote" returntype="any" output="false" hint="FOURNIT LA LISTE DES COMPTES ET SOUS COMPTES POUR UN GESTIONNAIRE, UN POOL ET UN OPÉRATEUR DONNÉ.">
		<cfargument name="idOperateur" 	required="true" type="numeric"/>
		<cfargument name="idPool" 		required="true" type="numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getcompteopeloginpool">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idlogin" 	value="#session.user.clientaccessid#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateur" value="#idOperateur#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 	 value="#idPool#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

			<cfquery name = "qSortAll" dbtype="query">
			  select *  from p_retour order by COMPTE_FACTURATION,SOUS_COMPTE
			</cfquery>

			<cfquery name = "qSortedCompte" dbtype="query">
			  select DISTINCT COMPTE_FACTURATION,IDCOMPTE_FACTURATION  from p_retour order by COMPTE_FACTURATION
			</cfquery>
			<cfset retour = {}>
			<cfset retour.SOUS_COMPTES = qSortAll>
			<cfset retour.COMPTES = qSortedCompte>
		<cfreturn retour>
	</cffunction>



	<!--- FOURNIT LA LISTE DES LIBELLES DES COMPTES ET SOUS COMPTES POUR UN OPERATEUR OU TOUS LES OPÉRATEURS --->

	<cffunction name="fournirLibelleCompteOperateur" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE DES LIBELLES DES COMPTES ET SOUS COMPTES POUR UN OPERATEUR OU TOUS LES OPÉRATEURS">
		<cfargument name="idOperateur" 	required="true" type="numeric"/>

			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m16.getLibelleSCCF">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_operateurid" value="#idOperateur#" null="#iif((idOperateur eq -1), de("yes"), de("no"))#">
				<cfprocresult name="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>

</cfcomponent>
