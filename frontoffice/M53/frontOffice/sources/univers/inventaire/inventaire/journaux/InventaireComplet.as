package univers.inventaire.inventaire.journaux
{
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.facturation.suivi.controles.vo.Operateur;
	
	
	/**
	 * Journal de l'inventaire complet 
	 * Affiche l'inventaire à une date donnée
	 * */
	public class InventaireComplet extends InventaireCompletIHM implements IJournal
	{
		//reference vers la date pour laquelle on souhaite afficher l'inventaire
		private var dateInventaire : String;
				
		//ArrayCollection contenant les lignes du journal
		[Bindable]
		private var elementsDuJournal : ArrayCollection;
		
		//ArrayCollection contenant la liste des opérateurs
		[Bindable]
		private var listeOperateurs : ArrayCollection;
		
		//Reference vers la méthode distante qui ramène les lignes du journal
		private var opElementDuJournal : AbstractOperation;
		
		//les information du noeud (inutilisé ici)
		private var _nodeInfos : Object;
		
		//a-t-on déjà lancer la requête?
		private var _boolInit : Boolean; 
		
		/**
		 * Constructeur
		 * */		
		public function InventaireComplet()
		{	
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
			
		/**
		 * Recharge les données du journal
		 * */
		public function refresh():void{			
			//tente de couper le remoting	
				 
			if (!_boolInit && dateInventaire != null) chargerDonnees();
		 
		}
		
		/**
		* Fonction qui permet d'afficher la période sur laquelle on travaille, au format jour mois annee 
		*  
		* @param moisDebut : String La date de debut de la période
		* @param moisFin : String La date de fin de la période
		* 
		* */
		public  function setSelectedPeriode(moisDebut : String , moisFin : String ):void{			 
															
			var formMois: String = moisDebut.substr(0,2) 
									+ " "
									+DateFunction.moisEnLettre(parseInt(moisDebut.substr(3,2),10)-1)
									+ " " 
								   	+ moisDebut.substr(6,4);
			
			labelPeriode.text = formMois;					
		}
		
		/**
		 * Gere le changement de périmetre
		 * */
		public function onPerimetreChange():void{
			//tente de couper le remoting
			 
			//init filtre
			txtFiltre.text = "";
			
		
			labelPeriode.text = "";
			lblError.visible = false;
			
			//init periode
			initPeriode();
			
			
			//init le data provider
			elementsDuJournal.source = null
			elementsDuJournal.refresh();
			
			_boolInit = true;
			
			getListeOperateurs();
		}
		
		/**
		 * Affecte les infos sur le noeud au journal
		 * */
		 public function set nodeInfos(infos : Object):void{
		 	_nodeInfos = infos;
		 }
		 
		 
		
		/**
		 * Interrompt les remotings en cours
		 * Méthode non utilisée
		 * */
		public function cancelRemotings():void{
			try{
				 
			}catch(e : Error){
				trace("ok - Inventaire - " + e.getStackTrace());
			}
		}
		


	


//---- HANDLERS --------------------------------------------------------------------------------------------------------------------------------
		//Initialisation de l'ihm
		//Affecte les écouteurs d'évènements
		//Initialise la date pou laquel on affiche l'inventaire
		protected function initIHM(fe : FlexEvent):void{
			//Periode
			initPeriode();
			
			//Filtre
			txtFiltre.addEventListener(Event.CHANGE,txtFiltreChangeHandler);
			cboInventaire.addEventListener(ListEvent.CHANGE,cboInventaireChangeHandler);
			cboOperateur.addEventListener(ListEvent.CHANGE,cboOperateurChangeHandler);
			imgRechercher.addEventListener(MouseEvent.CLICK,imgRechercherClickHandler);
			myCvDateChooser.addEventListener(CalendarLayoutChangeEvent.CHANGE,myCvDateChooserChangeHandler);
			
			//grid			
			numligne.labelFunction = formateLigne;
			dansInv.labelFunction = formateBoolean;
			entreeInv.labelFunction = formateShortDate;
			sortieInv.labelFunction = formateShortDate;
			_boolInit = true;
			
			getListeOperateurs();
		}
		
		//Handler du chagement de date
		//Met à jour la date pour le journal, affiche la date et charge les données pour cette date
		protected function myCvDateChooserChangeHandler(clce : CalendarLayoutChangeEvent):void{
			dateInventaire = myCvDateChooser.text;			
		}
		
		protected function cboOperateurChangeHandler(le : ListEvent):void{
			if(cboOperateur.selectedIndex != -1){
				filtrerGrid();	
			}
		}
		
		
		protected function cboInventaireChangeHandler(le : ListEvent):void{
			if(cboInventaire.selectedIndex != -1){
				filtrerGrid();	
			}
		}
		
		protected function txtFiltreChangeHandler(ev :Event):void{
			filtrerGrid();
		}
		
		protected function imgRechercherClickHandler(me : MouseEvent):void{
			chargerDonnees();
		}
		
		//Affecte les donnnées resultants de la méthode 'chargerDonnees' à l'ArrayCollection 'elementsDuJournal' et les affiches
		protected function chargerDonneesResultHandler(re :ResultEvent):void{
			elementsDuJournal = re.result as ArrayCollection;	
			if (elementsDuJournal.length > 0){
				txtFiltre.editable = true;
				 
			} else {
				txtFiltre.editable = false;
				lblError.visible = true;
			}
			myGrid.dataProvider = elementsDuJournal;
			elementsDuJournal.filterFunction = filterFunc;
			elementsDuJournal.refresh();
			
			_boolInit = false;
		}
		
		protected function getListeOperateursResultHandler(re : ResultEvent):void{
	    	if (re.result){
	    		var ops : Operateur = new Operateur();
	    		ops.nom = "Tous les opérateurs";
	    		ops.id = 0;
	    		var arr : Array = re.result as Array;
	    		arr.unshift(ops);
	    		
	    		cboOperateur.dataProvider = listeOperateurs;
	    		listeOperateurs.source = arr;
	    		listeOperateurs.refresh();
	    	}
	    }
	    
	    protected function updateFiltresResultHandler(re :ResultEvent):void{
	    	
	    }
//----------------------------------------------------------------------------------------------------------------------------------------------
				

//---  PRIVATE  ---------------------------------------------------------------------------------------------------------------------------
		//formate une colonne avec le symbol Euros	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres
		private function formateEuros(item : Object, column : DataGridColumn):String{			
			return ConsoviewFormatter.formatEuroCurrency(item[column.dataField],2);						
		}
		
		//formate une colonne de numéro de téléphone	
		//voir l'attribut LabelFunction pour un DataGRid pour les parametres
		private function formateLigne(item : Object, column : DataGridColumn):String{			
			return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);			
		}
		
		private function formateShortDate(item : Object, column : DataGridColumn):String{			
			return ( item[column.dataField] != null ) ? DateFunction.formatDateAsShortString(item[column.dataField]) : "";			
		}
		
		protected function formateBoolean(item : Object, column : DataGridColumn):String{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON";
			else return "-";
		}
		
		
		
		//Initialisation de la date pour le journal		
		private function initPeriode():void{
			var selectableRange : Object = new Object();
			selectableRange.rangeStart = CvAccessManager.getSession().CURRENT_PERIMETRE.dateFirstFacture;
			
				
			myCvDateChooser.selectableRange = selectableRange;
			
							
			dateInventaire = DateFunction.formatDateAsString(new Date());
		}		
		
		//--- FILTRE -----------------------------------------------------------------------------------------------------------------------------------
		
		//Gere le filtre du tableau
		private function filtrerGrid():void{
			if (elementsDuJournal != null){
				elementsDuJournal.filterFunction = filterFunc;
				elementsDuJournal.refresh();	
			}
		}
		
		//filtre sur les attributs OPNOM, THEME_LIBELLE, LIBELLE_PRODUIT, SOUS_TETE des elements du Tabelau
		private function filterFunc(value : Object):Boolean{	
			
			if (value == null) return false;
			
			var indexOperateur : Number =  ((cboOperateur.selectedItem == null)?0:cboOperateur.selectedIndex);
			var indexInventaire : Number = ((cboInventaire.selectedItem == null)?2:cboInventaire.selectedIndex);			 
			var boolMatch : Boolean = true;
			
			//filtre sur l'opérateur
			if (indexOperateur != 0){
				var valueOperateur : Number = cboOperateur.selectedItem.id
				boolMatch = boolMatch && (value.OPERATEURID == valueOperateur); 
			}
			
			//filtre sur l'état dans inventaire
			if(indexInventaire != 2){
				var valueInventaire : Number = Number(cboInventaire.selectedItem.value);
				boolMatch = boolMatch && (value.DANS_INVENTAIRE == valueInventaire);
			}
			
			//filtre sur les libellés
			if(txtFiltre.text != ""){
				var valueLibelle : String = txtFiltre.text;
				boolMatch = boolMatch && (
												(String(value.OPNOM.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
											||  (String(value.THEME_LIBELLE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
											|| 	(String(value.LIBELLE_PRODUIT.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
											||	(String(value.SOUS_TETE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
											||	(String(value.SOUS_COMPTE.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
											||	(String(value.COMPTE_FACTURATION.toLowerCase()).search(txtFiltre.text.toLowerCase()) != -1)
				)
			}
				
			
			return boolMatch;	
			
			
				
		}
		
		
	//--- FIN FILTRE -------------------------------------------------------------------------------------------------------------------------------

	//--- REMOTINGS --------------------------------------------------------------------------------------------------------------------------------
		
		//Charge les donnees du Journal
		private function chargerDonnees():void
		{				
			setSelectedPeriode(dateInventaire,null);
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;	
			lblError.visible = false;		
			opElementDuJournal = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getListeInventaire",
																				chargerDonneesResultHandler);						
			RemoteObjectUtil.callService(opElementDuJournal,
										idGroupeMaitre,
										dateInventaire);		
			 
		}
		
		/*------------------------------------------------------------------------------------------------------------------------------------------------*/
		/**
	     * 
	     * Fournit la liste des opérateurs du Groupe ou  groupe de ligne
	     */
	    private function getListeOperateurs(): void
	    {
	    	listeOperateurs = null;
	    	listeOperateurs = new ArrayCollection();
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.facturation.suiviFacturation.controles.OperateurGateWay",
																			"getListeOperateursPerimetre",
																			getListeOperateursResultHandler);
																			
			RemoteObjectUtil.callService(op);			
	    }
	    /*------------------------------------------------------------------------------------------------------------------------------------------------*/
		
		/*------------------------------------------------------------------------------------------------------------------------------------------------*/
		/**
	     * 
	     * Fournit la liste des opérateurs du Groupe ou  groupe de ligne
	     */
	    private function updateFiltres(): void
	    {	
	    	var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																			"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																			"updateFiltresJournalInventaire",
																			updateFiltresResultHandler);
																			
			RemoteObjectUtil.callService(op,[cboOperateur.selectedItem.id,cboInventaire.selectedItem.value,txtFiltre.text]);			
	    }
	    /*------------------------------------------------------------------------------------------------------------------------------------------------*/
		
	//--- FIN REMOTINGS ----------------------------------------------------------------------------------------------------------------------------
//--- FIN PRIVATE -----------------------------------------------------------------------------------------------------------------------
	}
}