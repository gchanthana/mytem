package intranet.GestionProduit.entities
{
	import flash.events.EventDispatcher;
	import flash.utils.describeType;
	
	import mx.collections.ArrayCollection;

	[Bindable]
	public dynamic class ProduitsVO extends EventDispatcher
	{
		
		//--------------------------------------------------------------------------------------------//
		//					PROPRIETEES
		//--------------------------------------------------------------------------------------------//
		
		public var PRODUIT 				:String = '';
		public var CATTYPE 				:String = '';
		public var OPERATEUR 			:String = '';
		public var THEME 				:String = '';
		
		private var _SELECTED 			:Boolean = false;
		
		public var IDPRODUIT_CATALOGUE 	:int = 0;
		private var _IDTHEME_PRODUIT 		:int = 0;
		public var LIBELLE_PRODUIT		:String = '';
		private var _BOOL_ACCES 			:int = -1;
		private var _TOUT_ACCES 			:int = -1;
		public var EDITED				:Boolean  = false;
		public var INDEX				:int  = -1;
		public var CB					:String = "";
		
		private var _BOOL_DRT				:int = 0;
		private var _PLAN_TYPE			:int;
		private var _PLAN_MCAP			:int;
		private var _DATA_CHANGED     :Boolean =false;
		
		
		public var DEBUG				:String = "";
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function ProduitsVO()
		{
		}
		
		public function get TOUT_ACCES():int
		{
			return _TOUT_ACCES;
		}

		public function set TOUT_ACCES(value:int):void
		{
			_TOUT_ACCES = value;
			_DATA_CHANGED = true;
		}

		public function get BOOL_ACCES():int
		{
			return _BOOL_ACCES;
		}

		public function set BOOL_ACCES(value:int):void
		{
			_BOOL_ACCES = value;
			_DATA_CHANGED = true;
		}

		public function get IDTHEME_PRODUIT():int
		{
			return _IDTHEME_PRODUIT;
		}

		public function set IDTHEME_PRODUIT(value:int):void
		{
			_IDTHEME_PRODUIT = value;
			_DATA_CHANGED = true;
		}

		public function get PLAN_MCAP():int
		{
			return _PLAN_MCAP;
		}

		public function set PLAN_MCAP(value:int):void
		{
			_PLAN_MCAP = value;
			_DATA_CHANGED = true;
		}

		public function get PLAN_TYPE():int
		{
			return _PLAN_TYPE;
		}

		public function set PLAN_TYPE(value:int):void
		{
			_PLAN_TYPE = value;
			_DATA_CHANGED = true;
		}

		public function get BOOL_DRT():int
		{
			return _BOOL_DRT;
		}

		public function set BOOL_DRT(value:int):void
		{
			_BOOL_DRT = value;
			_DATA_CHANGED = true;
		}
		
		public function get DATA_CHANGED():Boolean
		{
			return _DATA_CHANGED;
		}
		
		public function get SELECTED():Boolean
		{
			return _SELECTED;
		}

		public function set SELECTED(value:Boolean):void
		{
			_SELECTED = value;			
		}

		public static function formatProduitsVO(values:ArrayCollection):ArrayCollection
		{
			var produits	:ArrayCollection = new ArrayCollection();
			var lenProduits	:int = values.length;
			
			for(var i:int = 0;i < lenProduits;i++)
			{
				produits.addItem(mappingProduitsVO(values[i]));
			}
			
			return produits;
		}
		
		public static function mappingProduitsVO(value:Object):ProduitsVO
		{
			var produit:ProduitsVO 				= new ProduitsVO();
				
				produit.CATTYPE 				= value.CATTYPE;
				produit.OPERATEUR 				= value.OPERATEUR;
				produit.THEME 					= value.THEME;
				produit.IDPRODUIT_CATALOGUE 	= value.IDPRODUIT_CATALOGUE;
				produit.IDTHEME_PRODUIT 		= value.IDTHEME_PRODUIT;
				
				produit.BOOL_ACCES 				= value.BOOL_ACCES;
				produit.TOUT_ACCES 				= value.TOUT_ACCES;
				produit.EDITED					= (value.EDITED == true)?true:false;
				produit.INDEX					= (value.INDEX > -100)? value.INDEX : -1;	
			var	libelle_produit_inconnu:String	= produit.IDPRODUIT_CATALOGUE.toString() + " > non communiqué !";
				produit.LIBELLE_PRODUIT			= (value.LIBELLE_PRODUIT)?value.LIBELLE_PRODUIT : 	libelle_produit_inconnu;
				produit.PRODUIT 				= (value.PRODUIT)?value.PRODUIT : 	libelle_produit_inconnu;
				produit.CB						= value.CB;	
				produit.BOOL_DRT				= value.BOOL_DRT;
				produit.PLAN_TYPE				= value.PLAN_TYPE;
				produit.PLAN_MCAP				= value.PLAN_MCAP;
				produit._DATA_CHANGED		= false;
				
				produit.DEBUG = 
				 'PRODUIT 				= ' + produit.PRODUIT + '\n'
				+'CATTYPE 				= ' + produit.CATTYPE + '\n'
				+'OPERATEUR 				= ' + produit.OPERATEUR + '\n'
				+'THEME 					= ' + produit.THEME + '\n'
				+'IDPRODUIT_CATALOGUE 	= ' + produit.IDPRODUIT_CATALOGUE + '\n'
				+'IDTHEME_PRODUIT 		= ' + produit.IDTHEME_PRODUIT + '\n'
				+'LIBELLE_PRODUIT			= ' + produit.LIBELLE_PRODUIT + '\n'	
				+'BOOL_ACCES 				= ' + produit.BOOL_ACCES + '\n'
				+'TOUT_ACCES 				= ' + produit.TOUT_ACCES + '\n'
				+'EDITED					= ' + produit.EDITED + '\n'
				+'INDEX					= ' + produit.INDEX + '\n'				
				+'CB						= ' + produit.CB + '\n'	
				+'BOOL_DRT				= ' + produit.BOOL_DRT + '\n'
				+'PLAN_TYPE				= ' + produit.PLAN_TYPE + '\n'
				+'PLAN_MCAP				= ' + produit.PLAN_MCAP + '\n'
		
			return produit;
		}
		
		public function copyProduitsVO():ProduitsVO
		{
			var produit:ProduitsVO 				= new ProduitsVO();
				produit.PRODUIT 				= this.PRODUIT;
				produit.CATTYPE 				= this.CATTYPE;
				produit.OPERATEUR 				= this.OPERATEUR;
				produit.THEME 					= this.THEME;
				produit.IDPRODUIT_CATALOGUE 	= this.IDPRODUIT_CATALOGUE;
				produit.IDTHEME_PRODUIT 		= this.IDTHEME_PRODUIT;
				produit.BOOL_ACCES 				= this.BOOL_ACCES;
				produit.TOUT_ACCES 				= this.TOUT_ACCES;
				produit.EDITED					= (this.EDITED == true)?true:false;
				produit.INDEX					= (this.INDEX > -100)? this.INDEX : -1;				
				produit.CB						= this.CB;	
				produit.BOOL_DRT				= this.BOOL_DRT;
				produit.PLAN_TYPE				= this.PLAN_TYPE;
				produit.PLAN_MCAP				= this.PLAN_MCAP;
				
				
				
			return produit;
		}
	}
}