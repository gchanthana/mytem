 <cfcomponent name="FacadeSurTheme" displayname="FacadeSurTheme" extends="fr.consotel.consoview.M311.surtheme.facade">
	
	<!--- TRANSFORMATION DE LA DATE --->
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "/" & mid(oldDateString,4,2) & "/" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>
	
	<cffunction name="getListeThemesSurThemeByID" access="remote" returntype="query">
		<cfargument name="perimetre" 	type="string" 	required="true">
		<cfargument name="modeCalcul" 	type="string" 	required="true">		
		<cfargument name="numero" 		type="numeric" 	required="true">
		<cfargument name="idsurtheme" 	type="numeric" 	required="true">
		<cfargument name="datedeb" 		type="string" 	required="true">
		<cfargument name="datefin" 		type="string" 	required="true">
			
		<cfset periE0  = CreateObject("component","fr.consotel.consoview.M311.PerimetreE02")>
		
		<cfset dataset = periE0.executQueryByID(2, 0, 0, idsurtheme,
													session.perimetre.IDRACINE_MASTER,
													session.perimetre.ID_GROUPE,
													numero,
													transformDate(datedeb),
													transformDate(datefin),
													perimetre)>
		
		<cfreturn dataset>
	</cffunction>

</cfcomponent>