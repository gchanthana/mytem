package univers.facturation.optimisation
{
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	
	public class FunctioinOptimisationImpl
	{
		public var cpOptimisation : Optimisation;
		
		public function FunctioinOptimisationImpl()
		{
			super();
		}
		
		protected  function afterCreationComplete(event:FlexEvent):void {
			super.afterCreationComplete(event);
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
		}
		
		public  function getUniversKey():String {
			return "FACT";
		}
		
		public  function getFunctionKey():String {
			return "FACT_OPTIMISATION";
		}
		
		public  function afterPerimetreUpdated():void {
			cpOptimisation.onPerimetreChange();
			trace("(FunctionListeAppels) Perform After Perimetre Updated Tasks");
			trace("(FunctionListeAppels) SESSION FLEX :\n" +
						ObjectUtil.toString(CvAccessManager.getSession()));
		}
		
	}
}