<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.mobileIron.RestClient"
extends="fr.saaswedo.api.remoting.rest.RestClient" hint="Extension de RestClient spécifique à l'API REST de MobileIron">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.mobileIron.RestClient">
		<cfreturn "fr.saaswedo.api.mdm.impl.mobileIron.RestClient">
	</cffunction>
	
	<!--- Le format XML est utilisé car les chaines contenant des chiffres sont automatiquement transformés par CFMX en Nombres --->
	<cffunction access="public" name="getServiceCall" returntype="Any" description="LOCK:ReadOnly" hint="Appele la fonction parent et
	rajoute les modifications suivantes : <br/>- Valeur du header ACCEPT_HEADER() : Valeur retournée par XML_CONTENT_TYPE()">
		<cfargument name="serviceId" type="String" required="true" hint="Identifiant du service">
		<!--- Instance d'un appel de service créé avec la fonction parent --->
		<cfset var httpCall=SUPER.getServiceCall(ARGUMENTS.serviceId)>
		<!--- Ajout des modifications spécifiques à MobileIron --->
		<cfset var acceptHeader={TYPE=HEADER_TYPE(),NAME=ACCEPT_HEADER(),VALUE=XML_CONTENT_TYPE()}>
		<cfset setCallParameter(httpCall,acceptHeader)>
		<cfreturn httpCall>
	</cffunction>
	
	<cffunction access="public" name="invoke" returntype="Any"
	hint="Utilise la fonction parent pour envoyer une requete HTTP et retourne la réponse XML. Lève une erreur MobileIron.SERVICE_ERROR si :
	<br>- La clé errorDetail est renseignée dans la réponse CFHTTP<br>- Le code HTTP de la réponse est différent de 200 et 404">
		<cfargument name="serviceCall" type="Any" required="true" hint="fr.saaswedo.api.remoting.ext.ExtendedCFHTTP">
		<cfargument name="method" type="String" required="true" hint="Méthode HTTP e.g GET_METHOD()">
		<cfargument name="parameters" type="Array" required="true" hint="Paramètres de la requete qui seront ajoutés par ordre d'apparition.
		<br>Chaque élément doit etre une structure contenant les clés : TYPE e.g HEADER_TYPE(),NAME,VALUE">
		<!--- Les exceptions capturée par cet appel sont déjà traitées et dispatchées --->
		<cfset var httpResponse=SUPER.invoke(argumentCollection=ARGUMENTS)>
		<!--- Vérification de la validité de la réponse --->
		<cftry>
			<cfset var httpCode=getResponseCode(httpResponse)>
			<cfset var httpContent=getResponseContent(httpResponse)>
			<cfset var stringContent=getStringContent(httpContent)>
			<!--- MobileIron : Les codes HTTP différent de 200 et 404 correspondent à une erreur --->
			<cfif (httpCode NEQ 200) AND (httpCode NEQ 404)>
				<cfset var code=GLOBALS().CONST("MobileIron.SERVICE_ERROR")>
				<cfset var uri=ARGUMENTS.serviceCall.getUrl()>
				<cfset var errorDetail=structKeyExists(httpResponse,"errorDetail") ? httpResponse.errorDetail:"">
				<cfset var errorMsg="[URL: #uri#] - #stringContent#">
				<cfif httpCode GT 0>
					<cfset errorMsg="HTTP #httpCode# " & errorMsg>
				</cfif>
				<cfthrow type="Custom" errorCode="#code#" message="#errorMsg#" detail="#errorDetail#">
			<!--- Vérification du type de la réponse : XML --->
			<cfelse>
				<cfreturn getXmlContent(stringContent)>
			</cfif>
			<cfcatch type="Any">
				<cfset dispatchErrorAs(ARGUMENTS.serviceCall,CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!--- =========== Fin des fonction publiques =========== --->

	<cffunction access="private" name="isHeaderSupported" returntype="Boolean" hint="Rajoute le support de CONTENT_TYPE_HEADER() et ACCEPT_HEADER()">
		<cfargument name="headerName" type="String" required="true" hint="Header HTTP">
		<cfset var header=ARGUMENTS.headerName>
		<cfreturn (header EQ CONTENT_TYPE_HEADER()) OR (header EQ ACCEPT_HEADER()) OR SUPER.isHeaderSupported(header)>
	</cffunction>
	
	<cffunction access="private" name="checkHttpMethod" returntype="void" hint="Supprime le support de la méthode HTTP DELETE_METHOD()">
		<cfargument name="method" type="String" required="true" hint="Méthode HTTP">
		<cfset var httpMethod=ARGUMENTS.method>
		<cfset var isValidMethod=(httpMethod EQ GET_METHOD()) OR (httpMethod EQ POST_METHOD()) OR (httpMethod EQ PUT_METHOD())>
		<!--- Vérification de la validité de la méthode HTTP fournie --->
		<cfif NOT isValidMethod>
			<cfset var code=GLOBALS().CONST("HTTP.ERROR")>
			<cfthrow type="Custom" errorCode="#code#" message="Invalid http method '#httpMethod#'" detail="Not supported">
		</cfif>
	</cffunction>
</cfcomponent>