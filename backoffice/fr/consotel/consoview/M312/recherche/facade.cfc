 <cfcomponent name="facade" displayname="facade">
	

<!---
	GLOBAL
--->

	<cfset cliche = ""><!--- INTEGER --->
	<cfset idorga = ""><!--- INTEGER --->
	<cfset niveau = ""><!--- STRING --->
	<cfset dataSourceName = "BI_TEST"> <!--- STRING --->

<!---
	FUNCTION PRIVATE
--->

	<cffunction name="zRecupParametres" output="true" access="private">
		<cfargument name="numero" 	 type="numeric" required="true">
			<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ SESSION.PERIMETRE.ID_GROUPE>
		   		<cfset idorga = #SESSION.PERIMETRE.ID_GROUPE#>
		    <cfelse>
		    	<cfset rsltIdOrga = zgetOrga()>
		   		<cfset idorga 	= Val(rsltIdOrga)>
		    </cfif>
			<cfset rsltIdCliche = zGetCliche(idorga)>
			<cfset cliche 		= Val(rsltIdCliche)>
			<cfset niveau 		= zgetNiveauPerimetre(numero)>
			
	</cffunction>

	<cffunction name="zgetOrga" output="false" access="private" returntype="Any">
			<cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
				SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#) as IDORGA FROM DUAL
			</cfquery>
		<cfreturn rsltOrga['IDORGA'][1]>
	</cffunction>
		
	<cffunction name="zGetCliche" output="true" access="private" returntype="Any">
		<cfargument name="idorga" type="numeric" required="true">
			<cfquery datasource="E0" name="clicheRslt">
				SELECT idcliche FROM 
				( SELECT c.idcliche,c.idperiode_mois,MAX(c.idperiode_mois) over () MAX_p FROM cliche c WHERE c.idorganisation=#idorga# ) 
				WHERE max_p=idperiode_mois
			</cfquery>
		<cfreturn clicheRslt['IDCLICHE'][1]>
	</cffunction>
	
	<cffunction name="zgetIdPeriodeMois" output="true" access="private" returntype="Numeric">
		<cfargument name="thisDate" type="String" required="true" hint="yyyy/mm">
		<cfquery datasource="#dataSourceName#" name="qGetIdperiodeMois">	
		 	 SELECT PERIODE.IDPERIODE_MOIS IDPERIODE_MOIS
		 	 FROM	E0_AGGREGS
		 	 WHERE	PERIODE.SHORT_MOIS='#thisDate#'
		 	 ORDER BY IDPERIODE_MOIS
		</cfquery>
		<cfif qGetIdperiodeMois.recordcount EQ 1>
			<cfreturn qGetIdperiodeMois["IDPERIODE_MOIS"][1]>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	
	 <cffunction name="zgetNiveauPerimetre" output="false" access="public" returntype="String">
		<cfargument name="idNumber" type="numeric"  required="true">
		
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_consoview_v3.get_numero_niveau">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" 	variable="p_idgroupe_client" value="#idNumber#">			
				<cfprocparam type="out" cfsqltype="CF_SQL_VARCHAR" 	variable="p_retour">
			</cfstoredproc>
			
		<cfreturn p_retour>
	</cffunction>
	
<!---
	FUNCTION PUBLIC
--->
	
	<!--- 
	ETAT		//--MARCHE 										--OK
	FONCTION	//--SI L'ON EST SUR LA RACINE 
	--->
	<cffunction name="rechercherListeLigneGroupeRacine" access="public" returntype="query">
		<cfargument name="perimetre" type="string"  required="false">	
		<cfargument name="numero" 	 type="numeric" required="false">
		<cfargument name="chaine" 	 type="string" 	required="true">
				
		<!--- intervalle de date --->
		<cfset nb_mois = SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY-1/>
		<cfset idd2	= zgetIdPeriodeMois(LSDATEFORMAT(SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN,'MM/YYYY'))>							
		<cfif	nb_mois lte 0>						
			<cfset idd1	= val(idd2)>
			<cfelse>
			<cfset idd1	= val(idd2) - (SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY-1)>
		</cfif>
		
		
		
		<cfset zRecupParametres(numero)>
		<cfset obj = CreateObject("component","fr.consotel.consoview.M312.recherche.Recherche")>
		<cfset obj.init(perimetre)>
		<cfset result = obj.queryData(numero, cliche, chaine, niveau,idd1,idd2)>		 
	 	<cfreturn result>
	</cffunction> 
	
	<!--- 
	ETAT		//--L'ARBRE C'EST OK							--OK
	FONCTION	//--RECHERCHE LES NOEUDS CONTENANT LA CHAINE 
	--->	
	<cffunction name="rechercher" access="public" returntype="xml">
		<cfargument name="perimetre" type="string"  required="true">	
		<cfargument name="numero" 	 type="numeric" required="true">
		<cfargument name="chaine" 	 type="string" 	required="true">
				
		<!--- intervalle de date --->
		<cfset nb_mois = SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY-1/>
		<cfset idd2	= zgetIdPeriodeMois(LSDATEFORMAT(SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN,'MM/YYYY'))>
		<cfif	nb_mois lte 0>						
			<cfset idd1	= val(idd2)>
			<cfelse>
			<cfset idd1	= val(idd2) - (SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY-1)>
		</cfif>
		
		
	 	<cfset obj = CreateObject("component","fr.consotel.consoview.M312.recherche.Recherche")>
		<cfset obj.init(perimetre)>
				
		<cfset zRecupParametres(numero)>
		
		<cfset result = obj.queryData(numero, cliche, chaine, niveau,idd1,idd2)>
		
		<cfif result.recordcount eq 0>
			<cfset xml = XmlNew()>
			<cfset xml.xmlRoot =  XmlElemNew(xml,"node")>
			<cfreturn xml>
		<cfelse>
			<cfif not structKeyExists(session,"rechercheTb")>
				<cfset session.rechercheTb = structNew()>								
			</cfif>				
				<cfset structInsert(session.rechercheTb,chaine,result,1)>				
			<cfreturn transformXml(result,numero)>
		</cfif> 
	</cffunction>
	 
	<!--- 
	ETAT		//--											--KO
	FONCTION	//--RECHERCHE LES LIGNES DU NOEUD SELECTIONNE
	--->	
	<cffunction name="rechercherListeLigne" access="public" returntype="query">
		<cfargument name="perimetre" type="string"  required="false">	
		<cfargument name="idnode" 	 type="numeric" required="true">
		<cfargument name="level" 	 type="string"  required="true">
		<cfargument name="chaine" 	 type="string"  required="true">
 		
		<!--- intervalle de date --->
		<cfset nb_mois = SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY-1/>
		<cfset idd2	= zgetIdPeriodeMois(LSDATEFORMAT(SESSION.PERIMETRE.STRUCTDATE.PERIODS.DISPLAY_FIN,'MM/YYYY'))>							
		<cfif	nb_mois lte 0>						
			<cfset idd1	= val(idd2)>
			<cfelse>
			<cfset idd1	= val(idd2) - (SESSION.PERIMETRE.STRUCTDATE.PERIODS.NB_MONTH_DISPLAY-1)>
		</cfif>
		
		<cfset zRecupParametres(idnode)>
		<cfset obj = CreateObject("component","fr.consotel.consoview.M312.recherche.Recherche")>
		<cfset obj.init(perimetre)>		
		<cfset result = obj.listeLigne(idnode, cliche, level,idd1,idd2)>
 										
		<cfreturn result>
	</cffunction>
	
	<cffunction name="deleteSessionResultatRecherche" access="public">
		<cfif structKeyExists(session,"rechercheTb")>
			<cfset StructClear(session.rechercheTb)>								
		</cfif>			
	</cffunction>
	
	<cffunction name="deleteUnUsedSessionResultatRecherche" access="public">
		<cfargument name="cle" type="string">						
		<cfif structKeyExists(session,"rechercheTb")>
			<cfif structKeyExists(session.rechercheTb,cle)>
				<cfset structDelete(session.rechercheTb,cle)>
			</cfif>
		</cfif>		
	</cffunction>
	
	<cffunction name="getSessionListePerimetreNodId" access="public" returntype="numeric">
		<cfargument name="cle" required="true" type="string">
		<cfargument name="index" required="true" type="numeric">
	
<!--- 		<cfset idgroupe = searchIdGroupe(index,session.rechercheTb[cle])> --->
		<cfset result = searchNodeId(Evaluate("session.recherchetb."&cle&"['IDGROUPE_CLIENT'][index]")
													,SESSION.PERIMETRE.LISTE_PERIMETRES_QUERY)>
		<cfreturn result>
	</cffunction>	
	
	<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE:::::::::::::::::::::::::::::::::::::::::::::::::::::::::---> 
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>
	
	<cffunction name="transformXml" access="private" returntype="xml">
		<cfargument name="dataset" 		type="query" 	required="true">		
		<cfargument name="groupIndex" 	type="numeric" 	required="true">
		
		<cfset clientAccessId = SESSION.user.CLIENTACCESSID>
		
		<cfset idGroupe = dataset['IDGROUPE_CLIENT'][1] >
		<cfset libelleGroupeClient =  dataset['LIBELLE_GROUPE_CLIENT'][1]>

		<cfset dataStruct = structNew()>
		<cfset perimetreXmlDoc = XmlNew()>
		<cfset perimetreXmlDoc.xmlRoot = XmlElemNew(perimetreXmlDoc,"node")>
		<cfset rootNode = perimetreXmlDoc.node>
		<cfset dataStruct.ROOT_NODE = rootNode>
		<cfset rootNode.XmlAttributes.LABEL = dataset['LIBELLE_GROUPE_CLIENT'][1]>
		<cfset rootNode.XmlAttributes.NODE_ID = dataset['NODE_ID'][1]>
		<cfset rootNode.XmlAttributes.IDGROUPE_CLIENT = dataset['IDGROUPE_CLIENT'][1]>
		<cfset rootNode.XmlAttributes.LEVEL = dataset['NIVEAU'][1]>
		<cfset rootNode.XmlAttributes.TYPE_PERIMETRE = dataset['TYPE_PERIMETRE'][1]>
		<cfset rootNode.XmlAttributes.BOOL_ORGA = 1>
		<cfset rootNode.XmlAttributes.SELECTABLE  = 0>
		<cfset rootNode.XmlAttributes.TYPE_ORGA = "">
		<cfset rootNode.XmlAttributes.FLAG = "">
	
		<cfif ispresent("OPE","TYPE_ORGA",dataset) gte 1> 			
			<!---******** GROUPEMENTS ORGANISATIONS OPERATEUR***********--->			
			<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations Opérateurs">
			<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>
			<cfset rootNode.XmlChildren[1].XmlAttributes.SELECTABLE = 0>
			<cfset dataStruct.OP_ORGANISATIONS = rootNode.XmlChildren[1]>
		<cfelseif ispresent("CUS","TYPE_ORGA",dataset) gte 1
					or ispresent("GEO","TYPE_ORGA",dataset) gte 1
						or ispresent("ANA","TYPE_ORGA",dataset) gte 1>			
				<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations Clientes">
				<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>	
				<cfset rootNode.XmlChildren[1].XmlAttributes.SELECTABLE = 0>			
				<cfset dataStruct.CLIENT_ORGANISATIONS = rootNode.XmlChildren[1]>
			<cFif ispresent("SAV","TYPE_ORGA",dataset) gte 1>
				<cfset rootNode.XmlChildren[1] = XmlElemNew(perimetreXmlDoc,"node")>
				<cfset rootNode.XmlChildren[1].XmlAttributes.LABEL = "Organisations Sauvegardées">
				<cfset rootNode.XmlChildren[1].XmlAttributes.BOOL_ORGA = 1>	
				<cfset rootNode.XmlChildren[1].XmlAttributes.SELECTABLE = 0>			
				<cfset dataStruct.SAUVEGARDE_ORGANISATIONS = rootNode.XmlChildren[1]>					
			</cfif>
		</cfif>
		<!---**************************************************************************************--->		
		<cfloop index="i" from="2" to="#dataset.recordcount#">			
			<cfset currentKey = dataset['IDGROUPE_CLIENT'][i]>
			<cfset currentParentKey = dataset['IDGROUPE_MAITRE'][i]>
			<cfif dataset['IDGROUPE_MAITRE'][i] EQ idGroupe>								
				<cfswitch expression="#dataset['TYPE_ORGA'][i]#">
					<cfcase value="OPE">
						<cfset currentParentKey = 'OP_ORGANISATIONS'>
					</cfcase>
					<cfcase value="CUS">
						<cfset currentParentKey = 'CLIENT_ORGANISATIONS'>
					</cfcase>
					<cfcase value="GEO">
						<cfset currentParentKey = 'CLIENT_ORGANISATIONS'>
					</cfcase>
					<cfcase value="SAV">
						<cfset currentParentKey = 'SAUVEGARDE_ORGANISATIONS'>
					</cfcase>
					<cfdefaultcase>
						<cfset currentParentKey = 'ROOT_NODE'>
					</cfdefaultcase>					
				</cfswitch>
			</cfif>								
			<cfset parentChildCount = arrayLen(dataStruct['#currentParentKey#'].XmlChildren)>			
			<cfset dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1] = XmlElemNew(perimetreXmlDoc,"node")>
			<cfset tmpChildNode = dataStruct['#currentParentKey#'].XmlChildren[parentChildCount + 1]>
			<cfset tmpChildNode.XmlAttributes.LABEL = dataset['LIBELLE_GROUPE_CLIENT'][i]>
			<cfset tmpChildNode.XmlAttributes.NODE_ID = dataset['NODE_ID'][i]>
			<cfset tmpChildNode.XmlAttributes.IDGROUPE_CLIENT = dataset['IDGROUPE_CLIENT'][i]>
			<cfset tmpChildNode.XmlAttributes.LEVEL = dataset['NIVEAU'][i]> 
			<cfset tmpChildNode.XmlAttributes.TYPE_PERIMETRE = dataset['TYPE_PERIMETRE'][i]>
			<cfset tmpChildNode.XmlAttributes.FLAG = dataset['FLAG'][i]>						
			<cfset tmpChildNode.XmlAttributes.BOOL_ORGA = dataset['BOOL_ORGA'][i]>
			<cfset tmpChildNode.XmlAttributes.SELECTABLE = 1>	
			<cfset tmpChildNode.XmlAttributes.TYPE_ORGA = dataset['TYPE_ORGA'][i]>		
			<cfset tmpChildNode.XmlAttributes.DATE_CLICHE = dataset['DT_CLICHE'][i]>
			<cfset tmpChildNode.XmlAttributes.ANNEE_CLICHE = dataset['DT_ANNEE'][i]>
			<cfset tmpChildNode.XmlAttributes.MOIS_CLICHE = left(dataset['DT_MOIS'][i],2)>
			<cfset tmpChildNode.XmlAttributes.RAISON_SOCIALE = libelleGroupeClient>
			<cfset structInsert(dataStruct,currentKey,tmpChildNode,1)>			
		</cfloop>
			
		<cfreturn perimetreXmlDoc>
	</cffunction>	
	
	<cffunction name="searchNodeId" access="private" returntype="numeric">
		<cfargument name="idgroupeclient" type="numeric" required="true">
		<cfargument name="recordset" type="query" required="true">
		
		<cfloop index="i" from="1" to="#recordset.recordcount#">
			<cfif recordset['IDGROUPE_CLIENT'][i] eq idgroupeclient>
				<cfreturn recordset['IDGROUPE_CLIENT'][i]>				
			</cfif>
		</cfloop>		
		<cfreturn  -1>
	</cffunction>
	
	<cffunction name="searchIdGroupe" access="private" returntype="numeric">
		<cfargument name="nodeId" 		type="numeric" 	required="true">
		<cfargument name="recordset" 	type="query" 	required="true">
		
		<cfloop index="i" from="1" to="#recordset.recordcount#">
			<cfif recordset['IDGROUPE_CLIENT'][i] eq nodeId>
				<cfreturn recordset['IDGROUPE_CLIENT'][i]>				
			</cfif>
		</cfloop>		
		<cfreturn  -1>
	</cffunction>
	
	<cffunction name="ispresent" access="private" returntype="numeric" hint="contains(value : string the value to search\n, colname : string  the column name where to search into\n, data : query the query matrix)">
		<cfargument name="value" required="true" type="string">
		<cfargument name="colname" required="true" type="string">
		<cfargument name="data" required="true" type="query">		
		<cfset count = 0>
		<cfloop query="data">
			<cfif Evaluate("data."&colname) eq  value>				
				<cfset count = count + 1>
			</cfif>
		</cfloop>		
		<cfdump var="#count#">		
		<cfreturn count>
	</cffunction>

	<!---
	<cffunction name="setSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction> --->
	<cffunction name="setSessionValue" output="true" access="private">
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction>

</cfcomponent>