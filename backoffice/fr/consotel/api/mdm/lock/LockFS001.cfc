<cfcomponent displayname="Lock001" >

	<cffunction name="action">
	
		<cfargument name="param" type="struct" required="true" hint="arguments">
		
		<cfset service = createObject("component","fr.consotel.api.mdm.partner.public.FSecureAntiTheft") >
		<cfset variable = service.lockDevice(param.singleLicenseKey) >
				
	</cffunction>

</cfcomponent>
