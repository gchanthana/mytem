<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.management.WSUserService" implements="fr.consotel.api.mdm.partner.private.ws.IWSUserService"
hint="Implémentation de l'interface fr.consotel.api.mdm.partner.private.ws.IWSUserService">
	<!--- fr.consotel.api.mdm.partner.private.ws.management.WSUserService --->
	<cffunction access="package" name="getWSService" returntype="fr.consotel.api.mdm.partner.private.ws.IWSUserService" hint="Retourne une instance de cette classe">
		<cfargument name="serviceRepository" type="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" required="true" hint="Implémentation d'accès aux Web Services">
		<cfif NOT structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfset VARIABLES["WS_REPOSITORY"]=ARGUMENTS["serviceRepository"]>
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="private" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.management.Zenprise" hint="Implémentation d'accès aux Web Services">
		<cfif structKeyExists(VARIABLES,"WS_REPOSITORY")>
			<cfreturn VARIABLES["WS_REPOSITORY"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="L'entité MDM Service Repository n'est pas définie">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="asJavaNullValue" returntype="Any" hint="Retourne la valeur nulle Java si stringValue est une chaine vide et stringValue sinon">
		<cfargument name="stringValue" type="String" required="true">
		<cfif TRIM(ARGUMENTS["stringValue"]) EQ "">
			<cfreturn javaCast("NULL",0)>
		<cfelse>
			<cfreturn ARGUMENTS["stringValue"]>
		</cfif>
	</cffunction>
	
	<!--- UserGroup Service --->
	<cffunction access="public" name="getRoles" returntype="Array" hint="Retourne la liste des roles">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.getRoles()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getGroups" returntype="Array" hint="Retourne la liste des groupes">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.getGroups()>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="createGroup" returntype="String" hint="Crée un groupe et retourne son nom">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe à créer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.createGroup(ARGUMENTS["groupName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="removeGroup" returntype="Void" hint="supprime un groupe">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe à supprimer">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.removeGroup(ARGUMENTS["groupName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getGroupUsers" returntype="Array" hint="Retourne la liste des utilisateur du groupe">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.getGroupUsers(ARGUMENTS["groupName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getUserGroup" returntype="Array" hint="Retourne la liste des groupes d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.getUserGroup(ARGUMENTS["userName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="createUser" returntype="String" hint="Crée un utilisateur et retourne son nom">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfargument name="login" type="String" required="true" hint="Nom de l'utilisateur i.e Login utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe de l'utilisateur">
		<cfargument name="userRole" type="String" required="true" hint="Role de l'utilisateur (ADMIN, SUPPORT, USER, GUEST)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.createUser(ARGUMENTS["groupName"],ARGUMENTS["login"],ARGUMENTS["password"],ARGUMENTS["userRole"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="setUserGroup" returntype="Void" hint="Ajoute l'utilisateur au groupe spécifié">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.setUserGroup(ARGUMENTS["userName"],ARGUMENTS["groupName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeUser" returntype="Void" hint="Supprime un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.removeUser(ARGUMENTS["userName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="removeUserFromGroup" returntype="Void" hint="Supprime un utilisateur d'un groupe">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="groupName" type="String" required="true" hint="Nom du groupe">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.removeUserFromGroup(ARGUMENTS["userName"],ARGUMENTS["groupName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="setUserPassword" returntype="Void" hint="Met à jour le mot de passe d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="newPassword" type="String" required="true" hint="Nouveau mot de passe de l'utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.setUserPassword(ARGUMENTS["userName"],ARGUMENTS["newPassword"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="checkUserPassword" returntype="Boolean" hint="Retourne TRUE si le mot de passe spécifié est correct pour l'utilisateur concerné">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="password" type="String" required="true" hint="Mot de passe de l'utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.checkUserPassword(ARGUMENTS["userName"],ARGUMENTS["password"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="public" name="getUserRoles" returntype="Array" hint="Retourne les roles d'un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.getUserRoles(ARGUMENTS["userName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="getUserProperties" returntype="Array" hint="Retourne les propriétés d'un utilisateur (En tant que tableau de String)">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.getUserProperties(ARGUMENTS["userName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="putUserProperties" returntype="Void" hint="Spécifie une propriété pour un utilisateur">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="userProps" type="Array" required="true" hint="Tableau dans lequel chaque élément est une structure contenant les clés suivantes : NAME (Nom d'une propriété : String), VALUE (Valeur de la propriété : String)">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.putUserProperties(ARGUMENTS["userName"],ARGUMENTS["userProps"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="setUserRoles" returntype="Void" hint="Spécifie les roles d'un utilisateur. <b>Dans la version actuelle de Zenprise, un utilisateur ne doit avoir qu'un et un seul role</b>">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfargument name="userRoles" type="Array" required="true" hint="Tableau contenant la liste des roles">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.setUserRoles(ARGUMENTS["userName"],ARGUMENTS["userRoles"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="searchUserByName" returntype="Array" hint="Retourne la liste des utilisateurs dont le nom contient la chaine spécifiée en paramètre">
		<cfargument name="searchInUserName" type="String" required="true" hint="Chaine à rechercher dans le nom des utilisateurs">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfreturn userService.searchUserByName(ARGUMENTS["userName"],ARGUMENTS["searchInUserName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="public" name="initializeUser" returntype="Void" hint="Crée une entrée LDAP pour l'utilisateur spécifié s'il n'est pas encore présent dans l'annuaire">
		<cfargument name="userName" type="String" required="true" hint="Nom de l'utilisateur">
		<cfset var zdmRepository=getWSRepository()>
		<cfset zdmRepository.validateAuth()>
		<cfset var userService=zdmRepository.getUserService()>
		<cftry>
			<cfset userService.initializeUser(ARGUMENTS["userName"])>
			<cfcatch type="any">
				<cfset zdmRepository.notifyError({TYPE=CFCATCH["TYPE"],MESSAGE=CFCATCH["MESSAGE"],DETAIL=CFCATCH["DETAIL"]},CFCATCH["tagContext"])>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>