package service.diffusion.validerdiffusion
{
	import mx.rpc.events.ResultEvent;

	internal class ValiderDiffusionHandler
	{
		private var _model:ValiderDiffusionModel;

		public function ValiderDiffusionHandler(_model:ValiderDiffusionModel)
		{
			this._model=_model;
		}

		internal function validerDiffusionHandler(event:ResultEvent):void
		{
			if (event.result)
			{
				_model.validerDiffusion(event);
			}
		}
	}
}
