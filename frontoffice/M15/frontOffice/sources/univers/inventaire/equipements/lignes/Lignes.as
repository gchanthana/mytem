package univers.inventaire.equipements.lignes
{
	import flash.events.Event;
	import mx.events.FlexEvent;
	import univers.inventaire.equipements.lignes.fiche.ana.FicheLigneANA;
	import univers.inventaire.equipements.lignes.fiche.AbstractFicheLigne;
	import univers.inventaire.equipements.lignes.fiche.num.FicheLigneNUM;
	import univers.inventaire.equipements.lignes.fiche.ll.FicheLigneLL;
	import univers.inventaire.equipements.lignes.fiche.mob.FicheLigneMOB;
	import univers.inventaire.equipements.lignes.fiche.aut.FicheLigneAUT;
	import univers.inventaire.equipements.lignes.fiche.ser.FicheLigneSER;
	
	public class Lignes extends LignesIHM
	{
		public function Lignes()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
		}
		
		protected function initIHM(event:Event):void
		{
			wipeIn.play();
		}
		
		public function showFiche(soustete:Object):void
		{
			PanelFiche.removeAllChildren();
			var newFiche:AbstractFicheLigne;
			
			switch (soustete.TYPE_FICHELIGNE)
			{
				case "ANA":
					newFiche = new FicheLigneANA(soustete);
					break;
				case "NUM":
					newFiche = new FicheLigneNUM(soustete);
					break;
				case "LL":
					newFiche = new FicheLigneLL(soustete);
					break;
				case "MOB":
					newFiche = new FicheLigneMOB(soustete);
					break;
				case "AUT":
					newFiche = new FicheLigneAUT(soustete);
					break;
				case "SER":
					newFiche = new FicheLigneSER(soustete);
					break;
				default:
					return ;
			}
			
			
			PanelFiche.addChild(newFiche);
			PanelFiche.visible = true;
		}
		
	}
}