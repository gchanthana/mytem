package gestioncommande.ihm.modificationcommande
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.containers.HBox;
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.IndexChangedEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import commandemobile.ihm.mail.ActionMailBoxIHM;
	
	import composants.mail.MailBoxImpl;
	import composants.mail.gabarits.GabaritFactory;
	import composants.mail.gabarits.InfosObject;
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.composants.PanierIHM;
	import gestioncommande.composants.PiecesJointesMinIHM;
	import gestioncommande.custom.CustomButtonWizzard;
	import gestioncommande.entity.Action;
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ConfigurationElements;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.EquipementsElements;
	import gestioncommande.entity.RessourcesElements;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.events.ViewStackEvent;
	import gestioncommande.events.WorkflowEvent;
	import gestioncommande.ihm.modificationcommande.button.Etape1IHM;
	import gestioncommande.ihm.modificationcommande.button.Etape2IHM;
	import gestioncommande.ihm.modificationcommande.button.Etape3IHM;
	import gestioncommande.ihm.modificationcommande.button.Etape4IHM;
	import gestioncommande.ihm.modificationcommande.button.Etape5IHM;
	import gestioncommande.ihm.modificationcommande.button.Etape6IHM;
	import gestioncommande.ihm.modificationcommande.button.Etape7IHM;
	import gestioncommande.iinterface.IWizzard;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpConfirmationCommandeIHM;
	import gestioncommande.services.CommandeArticles;
	import gestioncommande.services.CommandeService;
	import gestioncommande.services.Formator;
	import gestioncommande.services.WorkflowService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class MainModificationImpl extends Box implements IWizzard
	{
		
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var vs						:ViewStack;
		
		public var txtAreaCommentaires		:TextArea;
		
		public var hbxButtonWizzard			:HBox;
		
		//---> IHM - BOUTON VIEWSTACK
		public var btPre					:Button;
		public var btNext					:Button;
		public var btValid					:Button;
		public var btCancel					:Button;
		
		//---> IHM - BOUTON WIZZARD
		public var btn1						:CustomButtonWizzard;
		public var btn2						:CustomButtonWizzard;
		public var btn3						:CustomButtonWizzard;
		public var btn4						:CustomButtonWizzard;
		public var btn5						:CustomButtonWizzard;
		
		public var  etape1					:Etape1IHM;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _pUpMailBox				:ActionMailBoxIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					COMPONENTS
		//--------------------------------------------------------------------------------------------//
		
		public var composantPanier			:PanierIHM;
		public var piecesJointes			:PiecesJointesMinIHM;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd					:Commande;
		
		private var infosMail				:InfosObject = new InfosObject();
		
		private var _selectedAction			:Action = new Action();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _commande				:CommandeService = new CommandeService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		//---> CHILDREN VIEWSTACK
		public var  etape2					:Etape2IHM = new Etape2IHM();
		public var  etape3					:Etape3IHM = new Etape3IHM();
		public var  etape4					:Etape4IHM = new Etape4IHM();
		public var  etape5					:Etape5IHM = new Etape5IHM();
		public var  etape6					:Etape6IHM = new Etape6IHM();
		public var  etape7					:Etape7IHM = new Etape7IHM();
		
		private var	_eltsArticles		 	:Array = new Array();				
		
		private var _myOldArticles			:XML; 
		
		private var _isRecorded				:Boolean = false;
		private var _modParameters			:Boolean = false;
		
		private var _prixEli				:Number = 0;
		private var _prixNonEli				:Number = 0;
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function MainModificationImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PUBLICS
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fe:FlexEvent):void
		{
			var bool:Boolean = CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE;
			
			if(bool)
			{	
				var myArticles	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARRAYCOL;
				var lenArt		:int = myArticles.length;
				
				CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = false;
				
				_cmd = SessionUserObject.singletonSession.COMMANDE;
				
				SessionUserObject.singletonSession.COMMANDE.IDROLE = whatIsRole(SessionUserObject.singletonSession.COMMANDE.IDTYPE_COMMANDE);
				
				SessionUserObject.singletonSession.IDTYPEDECOMMANDE = -1;
				
				SessionUserObject.singletonSession.CURRENTCONFIGURATION = new ElementsCommande2();
				
				addChildrenElement();
				
				enabledDisableButtonWizzard();
				buttonNavigationEnableDisable();
				
				if(lenArt == 0)
					getDetailsCommande();
			}
			else
				CvAccessManager.getSession().INFOS_DIVERS.SENDPROCEDURE = true;
			
			//---> PAS D'ACCESSOIRES ALORS ON SAUTE CETTE ETAPE
			addEventListener('NO_ACCESSORIES_NEXT', noAccessoriesHandler);
			addEventListener('NO_ACCESSORIES_PREV', noAccessoriesHandler);
			
			addEventListener('ONE_ENGAGEMENT_EQUIPEMENT', onlyHandler);
			addEventListener('ONE_ENGAGEMENT_RESSOURCES', onlyHandler);
			
			//---> LORSQUE L'ON CHANGE  UN DE CES 5 ELEMENTS ALORS ON BLOQUE LE WIZZARD
			addEventListener('CHANGE_CONFIGUR', configurationChangeHandler);
			addEventListener('MOBILE_CHANGE', mobileChangeHandler);
			
			addEventListener('VALIDER_ENABLED', btnValiderEnableHandler);
			
			addEventListener('PRICE_CHANGE', refreshPanier);
			
			addEventListener(CommandeEvent.MAIL_SEND, remove);
		}
		
		public function clickButtonPreviousHandler(me:MouseEvent):void
		{
			decompteVs();
			refreshPanier();
		}
		
		public function clickButtonNextHandler(me:MouseEvent):void
		{	
			var isOk:Boolean = (vs.getChildAt(vs.selectedIndex) as IWorflow).checkData();
			
			if(vs.selectedIndex == 0)
				whatIsTypeCommande();
			
			if(isOk)
				compteVs();
		}
		
		public function clickButtonCancelHandler(me:MouseEvent):void
		{
			dispatchEvent(new ViewStackEvent(ViewStackEvent.ANNUL_COMMANDE, true));
		}
		
		public function clickButtonValidHandler(me:MouseEvent):void
		{
			if(!_isRecorded)
			{
				if(txtAreaCommentaires.text.length > 500)
				{
					ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Le_champs_commentaire_poss_de_plus_de_____car'),'Consoview',null);
					txtAreaCommentaires.errorString = ResourceManager.getInstance().getString('M16', 'Le_champs_commentaire_poss_de_plus_de_____car');
					return;
				}
				else
					txtAreaCommentaires.errorString = '';
				
				
				if(etape7.mailCommande.myAction != null &&  etape7.mailCommande.myAction.IDACTION > 0)
				{
					if(etape7.mailCommande.checkMail())
					{
						if(etape7.mailCommande.myAction.CODE_ACTION == 'EMA3T')
						{ 
							var poUpConfir:PopUpConfirmationCommandeIHM = new PopUpConfirmationCommandeIHM();
								poUpConfir.addEventListener('sendCommande', registerCommande);
							
							PopUpManager.addPopUp(poUpConfir,this,true);
							PopUpManager.centerPopUp(poUpConfir);
							
						}
						else
							registerCommande();
					}
				}
				else
					registerCommande();
			}
			else
				ConsoviewAlert.afficherAlertConfirmation(resourceManager.getString('M16','Commande_d_j__modifi_e_et_enregistr_e___Voulez_vous_'), 'Consoview', commandeIsRecorded);
		}
		
		public function registerCommande(e:Event = null):void
		{
			var idType:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			_cmd 				= SessionUserObject.singletonSession.COMMANDE;
			_cmd.COMMENTAIRES 	= txtAreaCommentaires.text;
			_eltsArticles		= new Array();
			
			if(idType > 0)
			{
				_modParameters = false;
				_eltsArticles  = addArticles();
			}
			else
				_modParameters = true;
			
			updateCommande();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PROTECTED
		//--------------------------------------------------------------------------------------------//
		
		protected function creationCompletePiecesJointesIhmHandler():void
		{
			piecesJointes.idcommande 		= SessionUserObject.singletonSession.COMMANDE.IDCOMMANDE;
			piecesJointes.numerocommande 	= SessionUserObject.singletonSession.COMMANDE.NUMERO_COMMANDE;
		}
		
		protected function btn1ClickHandler(me:MouseEvent):void
		{
			vs.selectedIndex = 0;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		protected function btn2ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(1);
		}
		
		protected function btn3ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(3);
		}
		
		protected function btn4ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(4);
		}
		
		protected function btn5ClickHandler(me:MouseEvent):void
		{
			validatePreviousPage(6);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					METHODES PRIVATES
		//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getDetailsCommande():void
		{
			_commande.fournirDetailOperation(_cmd, _cmd.IDCOMMANDE);
			_commande.addEventListener(CommandeEvent.COMMANDE_DETAILS, detailsCommandeHandler);
		}
		
		private function getArticlesCommande():void
		{
			_commande.fournirArticlesCommande(_cmd.IDCOMMANDE);
			_commande.addEventListener(CommandeEvent.COMMANDE_ARRIVED, articlesCommandeHandler);
		}
		
		private function updateCommande():void
		{
			var actionModification:Action = createObjectActionMofiication();
			
			if(etape7.mailCommande.myAction != null && etape7.mailCommande.myAction.IDACTION > 0)
			{
				etape7.mailCommande.myAction.COMMENTAIRE_ACTION = etape7.txaCommentaires.text;
				etape7.mailCommande.myAction.DATE_ACTION 		 = new Date();
				etape7.mailCommande.myAction.DATE_HOURS		 = 	Formator.formatReverseDate(etape7.mailCommande.myAction.DATE_ACTION) 
					+ ' ' 
					+ Formator.formatHourConcat(etape7.mailCommande.myAction.DATE_ACTION);
				
				etape7.mailCommande.createMailInfos();
			}
			
			_commande.majCommandeV2(_cmd,
									_eltsArticles,
									piecesJointes.piecesjointes.source, 
									etape7.mailCommande.mailCommande, 
									Formator.formatBoolean(etape7.mailCommande.chxMailCommande.selected),
									etape7.mailCommande.myAction,
									actionModification,
									piecesJointes.UUID);
			
			_commande.addEventListener(CommandeEvent.COMMANDE_UPDATED, remove);
		}

		private function updateArticles():void
		{
			_commande.enregistrerArticles(_cmd, _eltsArticles);
			_commande.addEventListener(CommandeEvent.ARTICLES_UPDATED, updateArticlesHandler);
		}
		
		private function updateWorflowHandler():void
		{
			var action:Action 		 	= new Action();
			action.DATE_ACTION_STRG		= Formator.formatDate(new Date());
			action.COMMENTAIRE_ACTION 	= ResourceManager.getInstance().getString('M16', 'Modifier_la_commande');
			
			if(SessionUserObject.singletonSession.IDSEGMENT == 1)
				action.IDACTION	= 2166;
			else
				action.IDACTION	= 2229;
			
			var wflow:WorkflowService = new WorkflowService();
			wflow.addEventListener(CommandeEvent.SEND_ACTION, worflowSendHandler);
			wflow.faireActionWorkFlow(action, SessionUserObject.singletonSession.COMMANDE);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function detailsCommandeHandler(cmde:CommandeEvent):void
		{
			SessionUserObject.singletonSession.COMMANDE = _commande.currentCommande;
			_cmd = SessionUserObject.singletonSession.COMMANDE;
			
			etape1.champsPersoHandler();
			etape1.addElements();
			getArticlesCommande();
		}
		
		private function articlesCommandeHandler(cmde:CommandeEvent):void
		{
			_myOldArticles = _commande.listeArticles;

			if(_cmd.IDTYPE_COMMANDE != TypesCommandesMobile.RENOUVELLEMENT && _cmd.IDTYPE_COMMANDE != TypesCommandesMobile.RENOUVELLEMENT_FIXE)
				formatToConfigurationSelected();
			else
				formatToConfigurationSelectedRenew();
		}
		
		private function updateCommandeHandler(cmde:CommandeEvent):void
		{
			if(!_modParameters)
				updateArticles();
			else
				updateWorflowHandler();
		}
		
		private function  updateArticlesHandler(cmde:CommandeEvent):void
		{
			updateWorflowHandler();
		}
		
		private function worflowSendHandler(cmde:CommandeEvent):void
		{
			if(_selectedAction != null && _selectedAction.IDACTION > 0)
				afficherMailBox();
			else
				remove();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHMs
		//--------------------------------------------------------------------------------------------//
		
		private function btnValiderEnableHandler(e:Event):void
		{
			btValid.enabled = true;
		}
		
		private function configurationChangeHandler(e:Event):void
		{
			var myArticles	:ArrayCollection = SessionUserObject.singletonSession.ARTICLES;
			var lenArticles	:int = myArticles.length;
			
			SessionUserObject.singletonSession.COMMANDEARTICLE = new ArrayCollection();
			
			for(var i:int =0;i < lenArticles;i++)
			{
				SessionUserObject.singletonSession.COMMANDEARTICLE.addItem((myArticles[i] as ElementsCommande2).copyElementsCommande());
				SessionUserObject.singletonSession.COMMANDEARRAYCOL[i].ELEMENTS = (myArticles[i] as ElementsCommande2).copyElementsCommande();
			}
			
			enabledDisableButtonWizzard();
			refreshPanier();
		}
		
		private function mobileChangeHandler(e:Event):void
		{			
			enabledDisableButtonWizzard();
		}
		
		private function _pUpMailBoxMailEnvoyeHandler(e:Event):void
		{
			_pUpMailBox.removeEventListener(MailBoxImpl.MAIL_ENVOYE, _pUpMailBoxMailEnvoyeHandler);
			
			if(!_pUpMailBox.cbMail.selected)
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Validation_enregistr__'),this);
			else
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M16', 'Votre_mail_a__t__envoy__'),this);
			
			PopUpManager.removePopUp(_pUpMailBox);
			_pUpMailBox = null;
			
			_cmd.IDLAST_ETAT = _selectedAction.IDETAT;
			
			dispatchEvent(new CommandeEvent(CommandeEvent.MAIL_SEND));
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//
		
		private function whatIsRole(idtypecmde:int):int
		{
			var idRole:int = 101;
			
			if(idtypecmde == 1382 || idtypecmde == 1587)
				idRole = 202;
			
			return idRole;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION - WIZZARD
		//--------------------------------------------------------------------------------------------//
		
		private function validatePreviousPage(pageMax:int):void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			var childError	:int = -1;
			
			for(var i:int = 0;i < pageMax;i++)
			{
				if((children[i] as Object).ACCESS)
				{
					if(!(children[i] as Object).checkData())
					{
						childError = i;
						
						break;
					}
				}
			}
			
			if(childError > 0)
				vs.selectedIndex = childError;
			else
				vs.selectedIndex = pageMax;
			
			buttonNavigationEnableDisable();
			buttonWizzardColor();
		}
		
		private function buttonWizzardColor():void
		{
			var children	:Array = hbxButtonWizzard.getChildren();
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(vs.selectedIndex) as Object).IDPAGE;
			
			for(var i:int = 0;i < lenChild;i++)
			{
				if(children[i] as Button)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					var childrenVs	:Array = vs.getChildren();
					var lenVs		:int = childrenVs.length;
					
					for(var j:int = 0;j < lenVs;j++)
					{
						if((childrenVs[j] as Object).IDPAGE == idButton)
						{
							if(idButton == idPage)
								myButton.styleName = "btnWizzardActif";
							else
							{
								if(!(childrenVs[j] as Object).ACCESS)
								{
									myButton.styleName = "btnWizzardDesactivate";
								}
								else
								{
									if(myButton.enabled)
										myButton.styleName = "btnWizzardInactif";
									else
										myButton.styleName = "btnWizzardDesactivate";
								}
							}
						}
					}
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - FORMAT XML
		//--------------------------------------------------------------------------------------------//
		
		private function getEquipements(articlesEq:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesEq[0].equipement.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML	:XML = articlesEq[0].equipement[i];
				var price	:String = myXML[0].prix.toString().replace(',','.');
				
				var eqElts:EquipementsElements 	= new EquipementsElements();
					eqElts.LIBELLE	 			= String(myXML[0].libelle);
					eqElts.EQUIPEMENT 			= String(myXML[0].type_equipement);
					eqElts.IDEQUIPEMENT 		= int(myXML[0].idtype_equipement);
					eqElts.IDFOURNISSEUR		= int(myXML[0].idequipementfournis);
					eqElts.IDCLIENT				= int(myXML[0].idequipementclient);
					eqElts.PRIX					= Number(price);
					eqElts.REF_REVENDEUR		=  String(myXML[0].reference);
					eqElts.REFERENCE_PRODUIT	=  String(myXML[0].reference);
				if(eqElts.IDEQUIPEMENT == 2191)
				{
					valuesElements.ACCESSOIRES.addItem(eqElts);//---> ACCESSOIRES
				}
				else if(eqElts.IDEQUIPEMENT == 71)
					{
						valuesElements.TERMINAUX.addItem(eqElts);//---> CARTE SIM
					}
					else
					{
						valuesElements.TERMINAUX.addItem(eqElts);//---> TERMINAL
					}
			}
		}
		
		private function getRessources(articlesRe:XML, valuesElements:ElementsCommande2):void
		{
			var lenXML:int = articlesRe[0].ressource.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML	:XML = articlesRe[0].ressource[i];
				var price	:String = myXML[0].prix.toString().replace(',','.');
				
				var reElts:RessourcesElements	= new RessourcesElements();	
					reElts.LIBELLETHEME 		= String(myXML[0].theme);
					reElts.LIBELLE 				= String(myXML[0].libelle);
					reElts.IDCATALOGUE 			= int(myXML[0].idproduit_catalogue);
					reElts.IDPRODUIT 			= int(myXML[0].idThemeProduit);
					reElts.BOOLACCES 			= int(myXML[0].bool_acces);
					reElts.OBLIGATORY			= Formator.formatInteger(int(myXML[0].bonus));
					reElts.REFERENCE_PRODUIT    = String(myXML[0].reference);
					
				
				if(price != '')
				{
					reElts.PRIXVISIBLE		= true;
					reElts.PRIX_UNIT		= Number(price);
					reElts.PRIX_STRG		= Formator.formatTotalWithSymbole(reElts.PRIX_UNIT);
				}
				else
				{
					reElts.PRIXVISIBLE		= false;
					reElts.PRIX_UNIT		= 0;
					reElts.PRIX_STRG		= ResourceManager.getInstance().getString('M16', 'n_c');
				}				
					
				//---> 'BOOLACCES' = 0 -> OPTION, 'BOOLACCES' = 1 -> ABONNEMENT
				if(reElts.BOOLACCES > 0)
					valuesElements.ABONNEMENTS.addItem(reElts);
				else
					valuesElements.OPTIONS.addItem(reElts);
			}
		}
		
		private function getEngagement(engagement:XML, valuesElements:ElementsCommande2):void
		{
			var commitment	:Engagement = new Engagement();
			var duree		:int = int(engagement.engagement_res[0]);
			var idType		:int = int(engagement.idtype_ligne[0]);
			var eligibilite	:int = int(engagement.duree_elligibilite[0]);
			var fpc			:String = String(engagement.fpc[0]);
			var numCmd		:int = int(engagement.numero_config[0]);
			
			valuesElements.ID 	= numCmd;
			
			if(eligibilite > 0)
				commitment.ELEGIBILITE = eligibilite;
			
			if(fpc != '')
			{
				commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois');
				commitment.VALUE = '24';
				commitment.CODE  = 'EVQMFCP';
				commitment.FPC   = String(engagement.fpc[0]);
			}
			else
			{
				switch(duree)
				{
					case 12: commitment.CODE = 'EDM';  commitment.DUREE = ResourceManager.getInstance().getString('M16', '12_mois'); commitment.VALUE = '12'; break;
					case 24: commitment.CODE = 'EVQM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '24_mois'); commitment.VALUE = '24'; break;
					case 36: commitment.CODE = 'ETSM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '36_mois'); commitment.VALUE = '36'; break;
					case 48: commitment.CODE = 'EQHM'; commitment.DUREE = ResourceManager.getInstance().getString('M16', '48_mois'); commitment.VALUE = '48'; break;
				}
			}
			
			if(idType > 0)
			{
				valuesElements.IDTYPELIGNE  = idType;
				valuesElements.TYPELIGNE 	= String(engagement.type_ligne[0]);
			}
			else
			{
				valuesElements.IDTYPELIGNE  = -1;
				valuesElements.TYPELIGNE 	= '';
			}
			
			valuesElements.ENGAGEMENT 	= commitment;	
		}
		
		private function getConfiguration(configuration:XML, valuesElements:ElementsCommande2):void
		{
			var conElts:ConfigurationElements 		= new ConfigurationElements();
				conElts.LIBELLE 					= String(configuration.code_interne);
				conElts.ID_TYPELIGNE 				= int(configuration.idtype_ligne[0]);
				conElts.TYPELIGNE 					= String(configuration.type_ligne[0]);
				conElts.PORTABILITE.CODERIO 		= String(configuration.code_rio[0]);
				conElts.PORTABILITE.NUMERO 			= String(configuration.soustete[0]);
				conElts.PORTABILITE.IDSOUSTETE 		= int(configuration.idsoustete[0]);
				conElts.ID_ORGA 					= int(configuration.idSource[0]);
				conElts.ID_FEUILLE 					= int(configuration.idCible[0]);
				conElts.COLLABORATEUR 				= String(configuration.nomemploye[0]);
				conElts.ID_COLLABORATEUR 			= int(configuration.idemploye[0]);
				conElts.MATRICULE 					= String(configuration.matricule[0]);
			
			if(String(configuration.date_portage[0]) != '')
				conElts.PORTABILITE.DATEPORTABILITE = Formator.formatDateStringInDate(String(configuration.date_portage[0]));
			else
				conElts.PORTABILITE.DATEPORTABILITE = null;
			
			
			if(_cmd.IDTYPE_COMMANDE == 1382 || _cmd.IDTYPE_COMMANDE == 1587)
				conElts.LIGNE_ELIGIBLE = findEligibilite(String(configuration.soustete[0]), configuration.equipements[0]);
			
			var idsegment:int = int(configuration.idSegment[0]);
			
			if(idsegment != 0)
				SessionUserObject.singletonSession.IDSEGMENT = idsegment;
			else
				SessionUserObject.singletonSession.IDSEGMENT = getSegment();
			
			if(conElts.PORTABILITE.CODERIO != '')
				conElts.IS_PORTABILITE = true;
			
			if(conElts.ID_COLLABORATEUR > 0)
				conElts.IS_COLLABORATEUR = true;
			
			if(SessionUserObject.singletonSession.IDSEGMENT > 1)
				conElts.IS_MOBILE = false;
			else
			{
				var mysimSared:Object = findSimSpared(configuration.equipements[0]);
				
				if(mysimSared != null)
				{
					conElts.IDEQUCLIENT = mysimSared.IDEQUCLIENT;
					conElts.IDEQUFOURNI = mysimSared.IDEQUFOURNI;
					conElts.IDSIM 		= mysimSared.IDSIM;
					conElts.IMEISIM 	= mysimSared.IMEISIM;
				}
			}
			
			valuesElements.CONFIGURATIONS.addItem(conElts);
		}
		
		private function findSimSpared(equipements:XML):Object
		{
			var objSim:Object = null;
			var lenXML:int = equipements[0].equipement.length();
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML	:XML = equipements[0].equipement[i];
				
				if(int(myXML[0].idtype_equipement) == 71)
				{
					if(String(myXML[0].numeroserie) != '')
					{
						objSim 				= new Object();
						objSim.IDEQUCLIENT 	= int(myXML[0].idequipementclient);
						objSim.IDEQUFOURNI 	= int(myXML[0].idequipementfournis);
						objSim.IDSIM 		= int(myXML[0].idequipement);
						objSim.IMEISIM 		= String(myXML[0].numeroserie);
					}
				}
			}
			
			return objSim;
		}
		
		private function findEligibilite(soustete:String, equipements:XML):int
		{
			var lenXML		:int = equipements[0].equipement.length();
			var eligibilite	:int = 0;
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML	:XML = equipements[0].equipement[i];
				var price	:String = myXML[0].prix.toString().replace(',','.');
				
				if(int(myXML[0].idtype_equipement) != 71 && int(myXML[0].idtype_equipement) != 2191)
				{
					eligibilite = int(myXML[0].bonus);
					
					if(eligibilite == 0)
					{
						SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE++;
						_prixNonEli = Number(price);
					}
					else
					{					
						SessionUserObject.singletonSession.NBRLIGNEELIGIBLE++;
						_prixEli = Number(price);
					}
					
					break;
				}
			}				
			
			return eligibilite;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - TRANSFORMATION ARTICLES TO ARRAYCOLLECTION POUR ETAPE 1 
		//--------------------------------------------------------------------------------------------//
		
		private function formatToConfigurationSelected():void
		{
			var listObject	:ArrayCollection = new ArrayCollection();
			var xmlarticles	:XMLList = _myOldArticles.children();
			var bool		:Boolean = false;
			var len			:int = xmlarticles.length();
			var id			:int = 0;
			var i			:int = 0;
			
			SessionUserObject.singletonSession.NBRLIGNEELIGIBLE 	= 0;
			SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE 	= 0;
			
			for(i = 0;i < len;i++)
			{
				var articles	:XML = xmlarticles[i];
				var idCurrent	:int = int(articles.numero_config[0]);
				
				if(id != idCurrent)
				{
					var myElements:ElementsCommande2 = new ElementsCommande2();
					
					getEquipements(articles.equipements[0], myElements);
					getRessources(articles.ressources[0], myElements);
					getEngagement(articles, myElements);
					
					id = idCurrent;
					
					listObject.addItem(myElements);
				}
				
				getConfiguration(articles, myElements);
			}
			
			len = listObject.length;
			
			for(i = 0;i < len;i++)
			{
				getItems(listObject[i] as ElementsCommande2);
			}
		}

		private function formatToConfigurationSelectedRenew():void
		{
			var listObject	:ArrayCollection = new ArrayCollection();
			var xmlarticles	:XMLList = _myOldArticles.children();
			var bool		:Boolean = false;
			var len			:int = xmlarticles.length();
			var i			:int = 0;
			
			SessionUserObject.singletonSession.NBRLIGNEELIGIBLE 	= 0;
			SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE 	= 0;
			
			for(i = 0;i < len;i++)
			{
				var articles	:XML = xmlarticles[i];
				var idCurrent	:int = int(articles.numero_config[0]);
				
				if(i == 0)
				{
					var myElements:ElementsCommande2 = new ElementsCommande2();
					
					getEquipements(articles.equipements[0], myElements);
					getRessources(articles.ressources[0], myElements);
					getEngagement(articles, myElements);
										
					listObject.addItem(myElements);
				}
				
				getConfiguration(articles, myElements);
			}
			
			len = listObject.length;
			
			for(i = 0;i < len;i++)
			{
				getItems(listObject[i] as ElementsCommande2);
			}
		}
		
		private function getItems(elts:ElementsCommande2):void
		{
			var newElts:Object 		= getMyArticles(elts);
				newElts.SELECTED 	= false;
				newElts.IDTCMDE		= elts.TYPE	;
				newElts.ELEMENTS 	= elts;
				newElts.ENGAGEMENT	= elts.ENGAGEMENT.DUREE;
				newElts.IDRANDOM	= elts.RANDOM;
			
			SessionUserObject.singletonSession.ARTICLES.addItem(elts.copyElementsCommande());
			SessionUserObject.singletonSession.COMMANDEARTICLE.addItem(elts);
			SessionUserObject.singletonSession.COMMANDEARRAYCOL.addItem(newElts);
			
			refreshPanier();
		}
		
		private function getMyArticles(elts:ElementsCommande2):Object
		{
			var myItems	:ArrayCollection = new ArrayCollection();
			var eltsObj	:Object = new Object();
			var obj		:Object = new Object();
			var price	:Number = 0;
			var qte		:int= elts.CONFIGURATIONS.length
			var i		:int = 0;
			var len		:int = 0;
			
			len = elts.TERMINAUX.length;
			
			for(i = 0;i < len;i++)
			{
				obj = new Object();
				obj.TYPE	 = (elts.TERMINAUX[i] as EquipementsElements).EQUIPEMENT;
				obj.LIBELLE	 = (elts.TERMINAUX[i] as EquipementsElements).LIBELLE;
				obj.REFERENCE_PRODUIT = (elts.TERMINAUX[i] as EquipementsElements).REFERENCE_PRODUIT;
				obj.QUANTITY = qte;
				obj.PRIX_RE	 = '';
				
				if(_cmd.IDTYPE_COMMANDE != TypesCommandesMobile.RENOUVELLEMENT && _cmd.IDTYPE_COMMANDE != TypesCommandesMobile.RENOUVELLEMENT_FIXE)
				{
					obj.PRIX_EQ	 = Formator.formatTotalWithSymbole((elts.TERMINAUX[i] as EquipementsElements).PRIX);
					price = price + (elts.TERMINAUX[i] as EquipementsElements).PRIX;
				}
				else
				{
					if(SessionUserObject.singletonSession.NBRLIGNEELIGIBLE > 0 && SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE > 0)
					{
						obj.PRIX_EQ	 = Formator.formatTotalWithSymbole(_prixEli) + '|' +  Formator.formatTotalWithSymbole(_prixNonEli);
					}
					else if(SessionUserObject.singletonSession.NBRLIGNEELIGIBLE > 0)
						{
							obj.PRIX_EQ	 = Formator.formatTotalWithSymbole(_prixEli);
						}
						else if(SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE > 0)
							{
								obj.PRIX_EQ	 = Formator.formatTotalWithSymbole(_prixNonEli);
							}
				}
				
				myItems.addItem(obj);
			}
			
			len = elts.ACCESSOIRES.length;
			
			for(i = 0;i < len;i++)
			{
				obj = new Object();
				obj.TYPE	 = (elts.ACCESSOIRES[i] as EquipementsElements).EQUIPEMENT;
				obj.LIBELLE	 = (elts.ACCESSOIRES[i] as EquipementsElements).LIBELLE;
				obj.REFERENCE_PRODUIT = (elts.ACCESSOIRES[i] as EquipementsElements).REFERENCE_PRODUIT;
				obj.QUANTITY = qte;
				obj.PRIX_RE	 = ''
				obj.PRIX_EQ	 = Formator.formatTotalWithSymbole((elts.ACCESSOIRES[i] as EquipementsElements).PRIX);
				
				price = price + (elts.ACCESSOIRES[i] as EquipementsElements).PRIX;
				
				myItems.addItem(obj);
			}
			
			len = elts.ABONNEMENTS.length;
			
			for(i = 0;i < len;i++)
			{
				obj = new Object();
				obj.TYPE	 = (elts.ABONNEMENTS[i] as RessourcesElements).LIBELLETHEME;
				obj.LIBELLE	 = (elts.ABONNEMENTS[i] as RessourcesElements).LIBELLE;
				obj.REFERENCE_PRODUIT = (elts.ABONNEMENTS[i] as RessourcesElements).REFERENCE_PRODUIT;
				obj.QUANTITY = qte;
				obj.PRIX_RE	 = (elts.ABONNEMENTS[i] as RessourcesElements).PRIX_STRG;
				obj.PRIX_EQ	 = '';
				
				price = price + (elts.ABONNEMENTS[i] as RessourcesElements).PRIX_UNIT;
				
				myItems.addItem(obj);
			}
			
			len = elts.OPTIONS.length;
			
			for(i = 0;i < len;i++)
			{
				obj = new Object();
				obj.TYPE	 = (elts.OPTIONS[i] as RessourcesElements).LIBELLETHEME;
				obj.LIBELLE	 = (elts.OPTIONS[i] as RessourcesElements).LIBELLE;
				obj.REFERENCE_PRODUIT = (elts.OPTIONS[i] as RessourcesElements).REFERENCE_PRODUIT;
				obj.QUANTITY = qte;
				obj.PRIX_RE	 = (elts.OPTIONS[i] as RessourcesElements).PRIX_STRG;
				obj.PRIX_EQ	 = '';
				
				price = price + (elts.OPTIONS[i] as RessourcesElements).PRIX_UNIT;
				
				myItems.addItem(obj);
			}
			
			elts.TYPE = whatIsTypeDecommande(elts);
			
			if(SessionUserObject.singletonSession.NBRLIGNEELIGIBLE > 0)
				SessionUserObject.singletonSession.PRIX_REEL_ELIGIBLE = Number(_prixEli);
			else
				SessionUserObject.singletonSession.PRIX_REEL_ELIGIBLE = -1;
				
			SessionUserObject.singletonSession.PRIXELIGIBLE 	= Number(_prixEli);
			SessionUserObject.singletonSession.PRIXNONELIGIBLE 	= Number(_prixNonEli);
			
			if(_cmd.IDTYPE_COMMANDE != 1382 && _cmd.IDTYPE_COMMANDE != 1587)
			{
				eltsObj.PRIX 	 = Formator.formatTotalWithSymbole(price);
				eltsObj.ITEMS 	 = myItems;
				eltsObj.TOTAL 	 = Formator.formatTotalWithSymbole(qte*price);
				eltsObj.NBRELMTS = qte;
				eltsObj.RENVLT	 = false;
			}
			else
			{				
				eltsObj.NBRELMTS_E 	= SessionUserObject.singletonSession.NBRLIGNEELIGIBLE;
				eltsObj.NBRELMTS_NE = SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE;
				
				eltsObj.PRIX_E	= Formator.formatTotalWithSymbole(price + _prixEli);
				eltsObj.PRIX_NE	= Formator.formatTotalWithSymbole(price + _prixNonEli);
				
				var prixEl	:Number = price + _prixEli;
				var prixNEl	:Number = price + _prixNonEli;
				var total	:Number = (SessionUserObject.singletonSession.NBRLIGNEELIGIBLE * prixEl) + (SessionUserObject.singletonSession.NBRLIGNENONELIGIBLE * prixNEl)
				
				eltsObj.ITEMS 	 = myItems;
				eltsObj.TOTAL 	 = Formator.formatTotalWithSymbole(total);
				eltsObj.NBRELMTS = qte;
				eltsObj.RENVLT	 = true;
			}
			
			return eltsObj;
		}
		
		private function whatIsTypeDecommande(elts:ElementsCommande2):int
		{
			var idTypeCmde:int = 1;
			
			if(_cmd.IDTYPE_COMMANDE == TypesCommandesMobile.EQUIPEMENTS_NUS || _cmd.IDTYPE_COMMANDE == TypesCommandesMobile.EQU_NUS_FIXE)
			{
				if(_cmd.IDTYPE_COMMANDE == TypesCommandesMobile.EQUIPEMENTS_NUS)
					idTypeCmde = 3;
				else
					idTypeCmde = 10;
			}
			else
			{
				if(SessionUserObject.singletonSession.IDSEGMENT > 1)//---> FIXE/RESEAU
				{
					idTypeCmde = 2;//---> MODIFICATIONS DES ABO/OPT -> FIXE/RESEAU 
				}
				else//---> MOBILE
				{
					if(elts.TERMINAUX.length == 1 && elts.TERMINAUX[0].IDEQUIPEMENT == 71)//--> MODIFICATIONS DES ABO/OPT + CARTES SIM -> MOBILE
					{
						idTypeCmde = 2;
					}
					else if(elts.TERMINAUX.length > 1)//--> MODIFICATIONS DES EQUIPEMENTS + ABO/OPT -> MOBILE
					{
						idTypeCmde = 1;
					}
				}
			}
			
			return idTypeCmde;
		}
		
		private function addArticles():Array
		{
			var cart		:CommandeArticles = new CommandeArticles();
			var elements	:ArrayCollection =  SessionUserObject.singletonSession.COMMANDEARTICLE;
			var xmlArray	:ArrayCollection = new ArrayCollection();
			var len			:int = elements.length;
			
			for(var i:int = 0;i < len;i++)
			{
				var xmlEq		:XML = cart.addEquipements(elements[i] as ElementsCommande2);
				var xmlRs		:XML = cart.addRessources(elements[i] as ElementsCommande2);
				var config		:ArrayCollection = elements[i].CONFIGURATIONS;
				var engagement	:Engagement = elements[i].ENGAGEMENT;
				var idConfig	:int = elements[i].ID;
				var lenConfig	:int = config.length;
				var isMulti		:Boolean = isMultiLignesEligible(config);
				
				for(var j:int = 0;j < lenConfig;j++)
				{
					var articles	:XML = <articles></articles>;
					var article		:XML = <article></article>;
					
					if(_cmd.IDTYPE_COMMANDE == 1382 || _cmd.IDTYPE_COMMANDE == 1587)
						findRenouvellementPrice(config[j].LIGNE_ELIGIBLE, xmlEq[0]);
					
					var equXML:XML = new XML();
					
					if((config[j] as ConfigurationElements).IDSIM > 0)
						equXML = attributNumeroSim(xmlEq.copy(), config[j] as ConfigurationElements);
					else
						equXML = xmlEq.copy();
					
					article.appendChild(equXML);
					article.appendChild(xmlRs);
					
					if(_cmd.IDTYPE_COMMANDE == 1382 || _cmd.IDTYPE_COMMANDE == 1587)
					{
						var etatEligible:int = 1;
						
						if(isMulti)
						{
							if(config[j].LIGNE_ELIGIBLE > 0)
								etatEligible = 1;
							else
								etatEligible = 2;
						}
						else
						{
							etatEligible = 1;
						}

						article = cart.addConfiguration(article, config[j] as ConfigurationElements, engagement, etatEligible);
					}
					else
						article = cart.addConfiguration(article, config[j] as ConfigurationElements, engagement, idConfig);					
					
					articles.appendChild(article);
					
					xmlArray.addItem(articles)
				}
			}
			
			return xmlArray.source;
		}
		
		private function isMultiLignesEligible(values:ArrayCollection):Boolean
		{
			var isMulti		:Boolean = false;
			var nbrEli		:int = 0;
			var nbrNonEli	:int = 0;
			var len			:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].LIGNE_ELIGIBLE == 0)
					nbrNonEli++;
				else
					nbrEli++;
			}
			
			if(nbrEli > 0 && nbrNonEli > 0)
			{
				isMulti = true;
			}
			
			return isMulti;
		}
		
		private function attributNumeroSim(article:XML, conElts:ConfigurationElements):XML
		{
			var eqListe	:XMLList = article.children();
			var eqLen	:int = eqListe.length();
			var parent	:int = 0;
			
			for(var i:int = 0;i < eqLen;i++)
			{
				var equipement:XML = eqListe[i] as XML;
				
				if(int(equipement[0].idtype_equipement) == 71)
				{
					equipement[0].idequipementfournis = conElts.IDEQUFOURNI;
					equipement[0].idequipementclient  = conElts.IDEQUCLIENT;
					equipement[0].idequipement 		  = conElts.IDSIM;
					equipement[0].numeroserie  		  = conElts.IMEISIM;
				}
			}
			
			return article;
		}
		
		private function findRenouvellementPrice(isEligible:int, myEquipements:XML):void
		{
			var lenXML	:int = myEquipements[0].equipement.length();
			var prix	:Number = 0;
			
			if(isEligible == 0)
				prix = SessionUserObject.singletonSession.PRIXNONELIGIBLE;
			else
				prix = SessionUserObject.singletonSession.PRIXELIGIBLE;
			
			for(var i:int = 0; i < lenXML;i++)
			{
				var myXML	:XML = myEquipements[0].equipement[i];
				
				if(int(myXML[0].idtype_equipement) != 71 && int(myXML[0].idtype_equipement) != 2191)
				{
					myXML[0].prix = prix.toString().replace(',','.');
					myXML[0].bonus = isEligible;
				}
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION - GESTION WIZZARD
		//--------------------------------------------------------------------------------------------//
		
		private function addChildrenElement():void
		{
			vs.addChild(etape2);
			vs.addChild(etape3);
			vs.addChild(etape4);
			vs.addChild(etape5);
			vs.addChild(etape6);
			vs.addChild(etape7);
		}
		
		private function getSegment():int
		{
			var idseg:int = 1;
			
			if(_cmd.SEGMENT_MOBILE == 1 && _cmd.SEGMENT_FIXE == 0 && _cmd.SEGMENT_DATA == 0)
				idseg = 1;//---MOBILE
			
			if(_cmd.SEGMENT_MOBILE == 0 && _cmd.SEGMENT_FIXE == 1 && _cmd.SEGMENT_DATA == 0)
				idseg = 2;//---FIXE
			
			if(_cmd.SEGMENT_MOBILE == 0 && _cmd.SEGMENT_FIXE == 0 && _cmd.SEGMENT_DATA == 1)
				idseg = 3;//---DATA
			
			return idseg;
		}
		
		private function whatIsTypeCommande():void
		{
			var accessed	:Array = new Array(false,false,false,false,false,false,false,false);
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			var idty:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 1)
			{
				accessed = new Array(true,true,true,true,true,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				accessed = new Array(true,false,false,true,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 3 || SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 10)
			{
				accessed = new Array(true,true,true,false,true,true,true);
			}
			else if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 0)
			{
				accessed = new Array(true,false,false,false,false,false,true);
			}
			
			for(var i:int = 0;i < lenChild;i++)
			{
				(children[i] as Object).ACCESS = accessed[i];
			}
		}
		
		private function compteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = vs.selectedIndex+1;i < lenChild;i++)
			{
				if((children[i] as Object).ACCESS)
				{
					vs.selectedIndex = i;
					break;
				}
			}
			
			(vs.getChildAt(vs.selectedIndex) as IWorflow).viewStackChange();
			
			enabledDisableButtonWizzard();
		}
		
		private function decompteVs():void
		{
			var children	:Array = vs.getChildren();
			var lenChild	:int = children.length;
			
			for(var i:int = vs.selectedIndex-1;i > -1;i--)
			{
				if((children[i] as Object).ACCESS)
				{
					vs.selectedIndex = i;
					break;
				}
			}
			
			(vs.getChildAt(vs.selectedIndex) as IWorflow).viewStackChange();
			
			enabledDisableButtonWizzard();
		}
		
		private function buttonNavigationEnableDisable():void
		{
			refreshPanier();
			
			if(vs.selectedIndex == 0)
			{
				btPre.visible 	= btPre.includeInLayout = false;
				btNext.visible 	= btNext.includeInLayout = true;
				btValid.visible = btValid.includeInLayout = false;
			}
			else if(vs.selectedIndex > 0)
			{
				if(vs.selectedIndex == 6)
				{
					btPre.visible 	= btPre.includeInLayout = true;
					btNext.visible 	= btNext.includeInLayout = false;
					btValid.visible = btValid.includeInLayout = true;
				}
				else
				{
					btPre.visible 	= btPre.includeInLayout = true;
					btNext.visible 	= btNext.includeInLayout = true;
					btValid.visible = btValid.includeInLayout = false;
				}
			}
		}
		
		private function enabledDisableButtonWizzard():void
		{
			var children	:Array = hbxButtonWizzard.getChildren();
			var lenChild	:int = children.length;
			var idPage		:int = (vs.getChildAt(vs.selectedIndex) as Object).IDPAGE;
			var isCurrent	:Boolean = false;
			
			for(var i:int = lenChild;i > -1;i--)
			{
				if(children[i] as CustomButtonWizzard)
				{
					var myButton:Button = children[i] as Button;
					var idButton:Number = Number(myButton.id.replace('btn',''));
					
					if(idButton <= idPage)
					{
						var childrenVs	:Array = vs.getChildren();
						
						for(var j:int = vs.selectedIndex;j > -1;j--)
						{
							if((childrenVs[j] as Object).IDPAGE == idButton)
								(children[i] as Button).enabled = (childrenVs[j] as Object).ACCESS;	
						}
					}
					else
						myButton.enabled = false;
				}
			}
			
			buttonWizzardColor();
			buttonNavigationEnableDisable();
		}
		
		private function refreshPanier(e:Event = null):void
		{
			findConfigurationSelected();
			setMontantsPanier()
			
		}
		
		private function setMontantsPanier():void
		{
			
			if(vs.selectedIndex == 6)
			{
				composantPanier.attributMontantConfirme();
				composantPanier.attributMontantEnPreparation(true);
				composantPanier.attributMontantTotal(true);
			}
			else
			{
				composantPanier.attributMontantEnPreparation();	
				composantPanier.attributMontantTotal();
			}
			
		}
		
		private function findConfigurationSelected():void
		{
			var myArticles	:ArrayCollection = SessionUserObject.singletonSession.COMMANDEARTICLE;
			var lenArticles	:int = myArticles.length;
			var idArticle	:int = SessionUserObject.singletonSession.CURRENTCONFIGURATION.RANDOM;
			
			for(var i:int =0;i < lenArticles;i++)
			{
				if(myArticles[i].RANDOM == idArticle)
					myArticles[i] = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FORMATEURS
		//--------------------------------------------------------------------------------------------//
		
		private function formatDate(dateobject:String):String
		{
			var day		:String = dateobject.substr(0,2);
			var month	:int 	= Number(dateobject.substr(3,2)) + 1;
			var temp	:String = month.toString();
			var year	:String = dateobject.substr(6,4);
			return year + '/' + temp + '/' + day;  
		}
		
		//--------------------------------------------------------------------------------------------//
		//					ANCIENNE FUCNTION - GESTION WIZZARD
		//--------------------------------------------------------------------------------------------//
		
		private function afficherErreurWizzard(we:WorkflowEvent):void
		{
			var ID:int = we.idButton;
			
			if(ID > -1)
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Une_erreur_est_survenue_dans_l_etape_') + ID.toString(), 'Consoview', null);
		}
		
		private function viewstackChange(ie:IndexChangedEvent):void
		{
			ie.currentTarget.selectedChild.viewStackChange();
			setMontantsPanier();
		}
		
		private function returnListeCommandes(e:Event):void
		{
			dispatchEvent(new Event('etapeListCommande', true));
		}
		
		private function noAccessoriesHandler(e:Event):void
		{
			if(e.type == 'NO_ACCESSORIES_NEXT')
			{
				compteVs();
			}
			else if(e.type == 'NO_ACCESSORIES_PREV')
				{
					decompteVs();
				}
		}
		
		private function onlyHandler(e:Event):void
		{
			clickButtonNextHandler(new MouseEvent(MouseEvent.CLICK));
		}
		
		private function remove(cmde:CommandeEvent = null):void
		{
			dispatchEvent(new ViewStackEvent(ViewStackEvent.COMMANDE_BUILT, true));	
		}
		
		private function afficherMailBox():void
		{	
			_pUpMailBox = new ActionMailBoxIHM();
			
			infosMail.commande 		= _cmd;
			infosMail.corpMessage 	= _selectedAction.MESSAGE;
			infosMail.UUID			= piecesJointes.UUID;			//---->>>>>>>>>>>>>>>> UUID
			
			_pUpMailBox.idsCommande 	= [_cmd.IDCOMMANDE];
			_pUpMailBox.numCommande 	= _cmd.NUMERO_COMMANDE;
			_pUpMailBox.idtypecmd 		= _cmd.IDTYPE_COMMANDE;
			
			var moduleText	:String = giveMeTheSegementTheme();
			var sujet		:String = giveMeSubject(_cmd);
			var libelle		:String	= _cmd.LIBELLE_COMMANDE;
			var numero		:String	= _cmd.NUMERO_COMMANDE;
			var objet		:String = sujet + ' / ' + libelle + ' / ' + numero;
			
			_pUpMailBox.initMail(moduleText, objet, '');
			_pUpMailBox.configRteMessage(infosMail,GabaritFactory.TYPE_COMMANDE_MOBILE);			
			_pUpMailBox.addEventListener(MailBoxImpl.MAIL_ENVOYE, _pUpMailBoxMailEnvoyeHandler);
			_pUpMailBox.selectedAction = _selectedAction;
			
			PopUpManager.addPopUp(_pUpMailBox, this, true);
			PopUpManager.centerPopUp(_pUpMailBox);
		}
		
		//--- 1-> NVL COMMANDE, 2-> MODIF OPTIONS, 3-> RENVLT, 4-> RESI,SUSP,REACT, -1-> DEFAUT
		private function giveMeSubject(objectCommande:Commande):String
		{
			var TC:String = '';
			
			switch(objectCommande.IDTYPE_COMMANDE)
			{
				case TypesCommandesMobile.NOUVELLES_LIGNES: 	TC = ResourceManager.getInstance().getString('M16', 'Commande_Mobile');				break;//--- NVL COMMANDE
				case TypesCommandesMobile.NVL_LIGNE_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_Fixe_R_seau');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQUIPEMENTS_NUS: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_');		break;//--- NVL COMMANDE
				case TypesCommandesMobile.EQU_NUS_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Commande_accessoire_s_'); 		break;//--- NVL COMMANDE
				case TypesCommandesMobile.MODIFICATION_OPTIONS: TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.MODIFICATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'Modification_s__option_s_');	break;//--- MODIF OPTIONS
				case TypesCommandesMobile.RESILIATION: 			TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RESILIATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_siliation'); 				break;//--- RESI
				case TypesCommandesMobile.RENOUVELLEMENT: 		TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.RENOUVELLEMENT_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_engagement'); 				break;//--- RENVLT
				case TypesCommandesMobile.SUSPENSION: 			TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.SUSPENSION_FIXE: 		TC = ResourceManager.getInstance().getString('M16', 'Suspension'); 					break;//--- SUSP
				case TypesCommandesMobile.REACTIVATION: 		TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.REACTIVATION_FIXE: 	TC = ResourceManager.getInstance().getString('M16', 'R_activation'); 				break;//--- REACT
				case TypesCommandesMobile.SAV: 					TC = ResourceManager.getInstance().getString('M16', 'SAV_'); 						break;//--- SAV
				case TypesCommandesMobile.NOUVELLE_CARTE_SIM: 	TC = ResourceManager.getInstance().getString('M16', 'Nouvelle_carte_SIM_');			break;//--- REACT
			}
			
			return TC;
		}
		
		private function giveMeTheSegementTheme():String
		{
			var moduleText		:String = '';
			var idseg			:int = 1;
			var thisCommande	:Commande = SessionUserObject.singletonSession.COMMANDE;
			
			if(thisCommande.SEGMENT_MOBILE == 1 && thisCommande.SEGMENT_FIXE == 0 && thisCommande.SEGMENT_DATA == 0)
				idseg = 1;//---MOBILE
			
			if(thisCommande.SEGMENT_MOBILE == 0 && thisCommande.SEGMENT_FIXE == 1 && thisCommande.SEGMENT_DATA == 0)
				idseg = 2;//---FIXE
			
			if(thisCommande.SEGMENT_MOBILE == 0 && thisCommande.SEGMENT_FIXE == 0 && thisCommande.SEGMENT_DATA == 1)
				idseg = 3;//---DATA
			
			switch(idseg)//--- 1-> MOBILE, 2-> FIXE, 3-> DATA
			{
				case 1: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Mobile'); break;
				case 2: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Fixe'); 	break;
				case 3: moduleText = ResourceManager.getInstance().getString('M16', 'Commande_Data'); 	break;
			}
			
			return moduleText;
		}
		
		private function createObjectActionMofiication():Action
		{
			var modification:Action = new Action();
			
			modification.DATE_ACTION_STRG		= Formator.formatDate(new Date());
			modification.COMMENTAIRE_ACTION 	= ResourceManager.getInstance().getString('M16', 'Modifier_la_commande');
			
			modification.DATE_ACTION 			= new Date();
			modification.DATE_HOURS				= 	Formator.formatReverseDate(modification.DATE_ACTION) 
				+ ' ' 
				+ Formator.formatHourConcat(modification.DATE_ACTION);
			
			if(SessionUserObject.singletonSession.IDSEGMENT == 1)
				modification.IDACTION	= 2166;
			else
				modification.IDACTION	= 2229;
			
			return modification;
		}
		
		private function commandeIsRecorded(ce:CloseEvent):void
		{
			if(ce.detail == 4)
				remove();
		}
		
		private function registerArticles(cmde:CommandeEvent):void
		{
			var cas:int = SessionUserObject.singletonSession.IDTYPEDECOMMANDE;
			
			if(cas != 0)
				_commande.enregistrerArticles(_cmd, _eltsArticles);
		}
		
		private function sendWorkFlow(cmde:CommandeEvent):void
		{
			remove();
		}
		
	}
}