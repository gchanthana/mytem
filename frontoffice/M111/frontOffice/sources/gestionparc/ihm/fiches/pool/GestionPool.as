package gestionparc.ihm.fiches.pool
{
	import composants.util.ConsoviewAlert;

	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;

	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	import mx.collections.ArrayCollection;
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	public class GestionPool extends EventDispatcher
	{

		private var _listeSearch:ArrayCollection=new ArrayCollection();

		//liste des éléments pour l'exclusion du pool
		private var _listeCollab:ArrayCollection=new ArrayCollection();
		private var _listeTerm:ArrayCollection=new ArrayCollection();
		private var _listeLigne:ArrayCollection=new ArrayCollection();
		private var _listeSim:ArrayCollection=new ArrayCollection();
		private var _liste:ArrayCollection=new ArrayCollection();

		public static const LISTE_COLLAB_LOADED:String="listeCollabLoaded";
		public static const LISTE_TERM_LOADED:String="listeTermLoaded";
		public static const LISTE_LIGNE_LOADED:String="listeLigneLoaded";
		public static const LISTE_SIM_LOADED:String="listeSimLoaded";
		public static const LISTE_LOADED:String="listeLoaded";

		public static const LISTE_COLLAB_EXCLUDED:String="listeCollabExcluded";
		public static const LISTE_EQPT_EXCLUDED:String="listeEqptExcluded";
		public static const LISTE_LIGNE_EXCLUDED:String="listeLigneExcluded";
		public static const LISTE_EXCLUDED:String="listeExcluded";

		public static const LISTE_COLLAB_INCLUDED:String="listeCollabIncluded";
		public static const LISTE_EQPT_INCLUDED:String="listeEqptIncluded";
		public static const LISTE_LIGNE_INCLUDED:String="listeLigneIncluded";
		public static const LISTE_INCLUDED:String="listeIncluded";


		public function GestionPool()
		{
		}

		/**
		 * @params:
		 * 			idCollab (Number),
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de tous les éléments qui sont rattachés
		 * 			à un collaborateur
		 **/
		public function getListCollabWithElements(idCollab:Number, idPool:Number, etat:int):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElementEmploye", getListCollabWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, idCollab, idPool, etat);
		}

		private function getListCollabWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeCollab=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_COLLAB_LOADED));
			}
		}


		/**
		 * @params:
		 * 			idTerm (Number),
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de tous les éléments qui sont rattachés
		 * 			à un terminal dans un pool de gestion (collaborateur, ligne, sim)
		 **/
		public function getListTermWithElements(idTerm:Number, idPool:Number, etat:int):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElementTerminal", getListTermWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, idTerm, idPool, etat);
		}

		private function getListTermWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeTerm=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_TERM_LOADED));
			}
		}

		/**
		 * @params:
		 * 			idSousTete (Number),
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de tous les éléments qui sont rattachés
		 * 			à une ligne
		 **/
		public function getListLigneWithElements(idSousTete:Number, idPool:Number, etat:int):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElementLine", getListLigneWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, idSousTete, idPool, etat);
		}

		private function getListLigneWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeLigne=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_LIGNE_LOADED));
			}
		}


		/**
		 * @params:
		 * 			idSim (Number),
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de tous les éléments qui sont rattachés
		 * 			à une sim
		 **/
		public function getListSimWithElements(idSim:Number, idPool:Number, etat:int):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElementSim", getListSimWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, idSim, idPool, etat);
		}

		private function getListSimWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeSim=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_SIM_LOADED));
			}
		}

		/**
		 * Fonction générique !
		 * Dans l'exclusion, il n'y a plus que cela d'utilisé pour éviter des bugs liés un grand nombre d'appel de procédure
		 * @params:
		 * 			myXML (String),
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de tous les éléments qui sont rattachés
		 **/
		public function getListWithElements(myXML:String, idPool:Number, etat:int):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElement", getListWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, myXML, idPool, etat);
		}

		private function getListWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_liste=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_LOADED));
			}
		}

		/**
		 * Fonction générique qui est tout le temps appelée
		 * @params:
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de tous les collaborateurs hors du pool (dans un autre ou dans aucun)
		 * 			avec les éléments rattachés (terminal, ligne, sim)
		 **/
		public function getListCollabOutWithElements(idPool:Number, etat:int, displayAll:int, searchColumn:int, search:String="", segment:int=0):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.GestionPool", "getAttachedElementOutGenerique", getAttachedElementOutGeneriqueResultHandler);

			RemoteObjectUtil.callService(opData, idPool, etat, displayAll, searchColumn, search, segment);
		}

		private function getAttachedElementOutGeneriqueResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeCollab=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_COLLAB_LOADED));
			}
		}

		/**
		 * @params:
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de tous les terminaux hors du pool (dans un autre ou dans aucun)
		 * 			avec les éléments rattachés (sim, ligne, collaborateur)
		 **/
		public function getListTermOutWithElements(idPool:Number, etat:int, displayAll:int, searchColumn:int, search:String=""):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElementOutTerminal", getListTermOutWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, idPool, etat, displayAll, searchColumn, search);
		}

		private function getListTermOutWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeTerm=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_TERM_LOADED));
			}
		}


		/**
		 * @params:
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de toutes les lignes hors du pool (dans un autre ou dans aucun)
		 * 			avec les éléments rattachés (terminal, sim, collaborateur)
		 **/
		public function getListLigneOutWithElements(idPool:Number, etat:int, displayAll:int, searchColumn:int, search:String=""):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElementOutLine", getListLigneOutWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, idPool, etat, displayAll, search, searchColumn);
		}

		private function getListLigneOutWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeLigne=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_LIGNE_LOADED));
			}
		}

		/**
		 * @params:
		 * 			idPool (Number),
		 * 			etat (int) = 0 --> dans aucun pool
		 * 					   = 1 --> dans un autre pool
		 * 					   = 2 --> dans un autre pool + dans aucun pool
		 * 					   = 3 --> dans le pool (idPool)
		 *
		 * @return:
		 * 			Liste de toutes les sim hors du pool (dans un autre ou dans aucun)
		 * 			avec les éléments rattachés (terminal, ligne, collaborateur)
		 **/
		public function getListSimOutWithElements(idPool:Number, etat:int, displayAll:int, searchColumn:int, search:String=""):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "getAttachedElementOutSim", getListSimOutWithElementsResultHandler);

			RemoteObjectUtil.callService(opData, idPool, etat, displayAll, searchColumn, search);
		}

		private function getListSimOutWithElementsResultHandler(evt:ResultEvent):void
		{
			if (evt.result != null)
			{
				_listeSim=evt.result as ArrayCollection;
				dispatchEvent(new Event(LISTE_SIM_LOADED));
			}
		}

		/**
		* @params:
		* 			idPool (Number),
		* 			tabIdEmploye (Array)
		* @return:
		* 			Exclusion du pool de gestion de tous les collaborateurs
		* 			avec les éléments qui leur sont rattachés (terminal, ligne, sim)
		**/

		public function removeListeCollabFromPool(idPool:Number, tabIdEmploye:Array):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "removeListeCollabFromPool", removeListeCollabFromPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, tabIdEmploye);
		}

		private function removeListeCollabFromPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_COLLAB_EXCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		/**
		 * @params:
		 * 			idPool (Number),
		 * 			tabIdEqpt (Array)
		 * @return:
		 * 			Exclusion du pool de gestion de tous les terminaux / de toutes les sim
		 * 			avec les éléments qui leur sont rattachés (collaborateur, ligne, sim / terminal)
		 **/

		public function removeListeEqptFromPool(idPool:Number, tabIdEqpt:Array):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "removeListeEqptFromPool", removeListeEqptFromPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, tabIdEqpt);
		}

		private function removeListeEqptFromPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_EQPT_EXCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		/**
		 * @params:
		 * 			idPool (Number),
		 * 			tabIdSoustete (Array)
		 * @return:
		 * 			Exclusion du pool de gestion de toutes les lignes
		 * 			avec les éléments qui leur sont rattachés (collaborateur, terminal, sim)
		 **/

		public function removeListeLigneFromPool(idPool:Number, tabIdSoustete:Array):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "removeListeLigneFromPool", removeListeLigneFromPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, tabIdSoustete);
		}

		private function removeListeLigneFromPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_LIGNE_EXCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		/**
		 * Fonction générique !
		 * Dans l'exclusion, il n'y a plus que cela d'utilisé pour éviter des bugs liés un grand nombre d'appel de procédure
		 * @params:
		 * 			idPool (Number),
		 * 			myXML (String)
		 * @return:
		 * 			Exclusion du pool de gestion de tous les éléments
		 * 			avec les éléments qui leur sont rattachés (collaborateur, terminal, sim)
		 **/

		public function removeListeFromPool(idPool:Number, myXML:String):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.GestionPool", 
				"removeListeFromPool", removeListeFromPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, myXML);
		}

		private function removeListeFromPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_EXCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Exclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		public function moveListeFromPool(idPool:Number, idPoolDest:Number, myXML:String):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, 
				"fr.consotel.consoview.M111.GestionPool", "moveListeFromPool", moveListeFromPoolResultHandler);

			RemoteObjectUtil.callService(opData, myXML, idPool, idPoolDest);
		}

		private function moveListeFromPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_EXCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Transfert_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Transfert_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		/**
		 * @params:
		 * 			idPool (Number),
		 * 			tabIdCollab (Array)
		 * @return:
		 * 			Inclusion dans le pool de gestion de tous les collaborateurs
		 * 			avec les éléments qui leur sont rattachés (terminal, sim, ligne)
		 **/

		public function addListeCollabToPool(idPool:Number, tabIdCollab:Array):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.GestionPool", "addListeCollabToPool", addListeCollabToPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, tabIdCollab);
		}

		private function addListeCollabToPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_COLLAB_INCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111',
					'Inclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 
					'Inclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		/**
		 * @params:
		 * 			idPool (Number),
		 * 			tabIdEqpt (Array)
		 * @return:
		 * 			Inclusion dans le pool de gestion de tous les terminaux / toutes les sim
		 * 			avec les éléments qui leur sont rattachés (collaborateur, terminal / sim, ligne)
		 **/

		public function addListeEqptToPool(idPool:Number, tabIdEqpt:Array):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "addListeEqptToPool", addListeEqptToPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, tabIdEqpt);
		}

		private function addListeEqptToPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_EQPT_INCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Inclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Inclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		/**
		 * @params:
		 * 			idPool (Number),
		 * 			tabIdSoustete (Array)
		 * @return:
		 * 			Inclusion dans le pool de gestion de toutes les lignes
		 * 			avec les éléments qui leur sont rattachés (collaborateur, terminal, sim)
		 **/

		public function addListeLigneToPool(idPool:Number, tabIdSoustete:Array):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.GestionPool", "addListeLigneToPool", addListeLigneToPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, tabIdSoustete);
		}

		private function addListeLigneToPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_LIGNE_INCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Inclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Inclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}

		/**
		 * Fonction générique !
		 * Dans l'exclusion, il n'y a plus que cela d'utilisé pour éviter des bugs liés un grand nombre d'appel de procédure
		 * @params:
		 * 			idPool (Number),
		 * 			myXML (String)
		 * @return:
		 * 			Inclusion dans le pool de gestion de tous les éléments
		 * 			avec les éléments qui leur sont rattachés (collaborateur, terminal, sim, ligne)
		 **/

		public function addListeToPool(idPool:Number, myXML:String):void
		{

			var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.GestionPool", "addListeToPool", addListeToPoolResultHandler);

			RemoteObjectUtil.callService(opData, idPool, myXML);
		}

		private function addListeToPoolResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{
				dispatchEvent(new Event(LISTE_INCLUDED, true));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Inclusion_des__l_ments_effectu_e__'), Application.application as DisplayObject);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Inclusion_des__l_ments_non_effectu_e__'), ResourceManager.getInstance().getString('M111', 'Erreur___'));
			}
		}


		/**
		 *	Liste de recherche des elements pour l'inclusion dans le pool
		 **/
		public function get listeSearch():ArrayCollection
		{
			return _listeSearch;
		}

		/**
		 *	Liste des employes avec les elements rattachés a exclure / inclure du / dans le pool
		 **/
		public function get listeCollab():ArrayCollection
		{
			return _listeCollab;
		}

		/**
		 *	Liste des terminaux avec les elements rattachés a exclure du pool
		 **/
		public function get listeTerm():ArrayCollection
		{
			return _listeTerm;
		}

		/**
		 *	Liste des lignes avec les elements rattachés a exclure du poolc
		 **/
		public function get listeLigne():ArrayCollection
		{
			return _listeLigne;
		}

		/**
		 *	Liste des sim avec les elements rattachés a exclure du pool
		 **/
		public function get listeSim():ArrayCollection
		{
			return _listeSim;
		}

		/**
		 *	Liste générique des elements rattachés a exclure du pool
		 **/
		public function get liste():ArrayCollection
		{
			return _liste;
		}
	}
}
