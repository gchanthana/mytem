<cfcomponent author="Cedric" displayname="fr.consotel.api.ibis.publisher.delivery.Email" extends="fr.consotel.api.ibis.publisher.delivery.FTP"
hint="Stratégie identifiée par le type Email permettant de diffuser par mail le contenu d'un reporting généré par une autre stratégie
Le contenu est soit envoyé dans le corps du mail si son format le permet soit en tant que pièce jointe (Une et une seule uniquement)
L'évènement qui est passé à handleReportingEvent() doit etre de type : fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent
A partir de ce paramètre la méthode crée un évènement permettant d'accéder aux propriétés de diffusion mail
L'évènement créé est de type fr.consotel.api.ibis.publisher.handler.event.EmailEvent et n'est pas créé si le paramètre est déjà de ce type
Les propriétés suivantes doivent etre retournée par la méthode getDeliveryProperties() de la stratégie utilisée pour l'exécution du reporting : 
- FROM : Adresse mail expéditeur
- TO : Adresse mail destinataire
Les propriétés suivantes sont facultatives :
- SUBJECT : Sujet du mail (Par défaut : Chaine vide)
- REPLYTO : Adresse mail de réponse (Par défaut : Non renseigné)
- CC : Adresse mail en copie (Par défaut : Non renseigné)
- BCC : Adresse mail en copie cachée (Par défaut : Non renseigné)
- ATTACHMENT : Nom à donner à la pièce jointe (Par défaut : En attachement avec un nom générique)
- MAIL_SERVER : DNS du serveur mail à utiliser (Par défaut : Utilise la configuration du BackOffice)
- MAIL_USER : Login utilisateur pour le serveur Mail (Par défaut : Utilise la configuration du BackOffice)
- MAIL_PWD : Mot de passe pour le serveur Mail (Par défaut : Utilise la configuration du BackOffice)
Lorsque le contenu n'est pas de type texte et que ATTACHMENT n'est pas renseigné : Il est diffusé en tant que pièce jointe
Le nom utilisé pour la pièce jointe est alors celui du fichier retourné par getContentAsLocalFile() avec des chaines vides comme paramètres
Cette implémentation ne vérifie pas la cohérence entre le nom de domaine du serveur mail et ceux utilisés pour les adresses mail
L'implémentation de la diffusion par mail ne renseigne pas la propriété FAILTO (Return-Path) : Non supportés par le serveur ConsoTel
Cette stratégie ne peut pas et ne doit pas etre utilisée pour l'exécution du reporting. Les méthodes correspondantes lèvent une exception
L'implémentation IDeliveryEvent dispatché par cette stratégie est : fr.consotel.api.ibis.publisher.handler.event.EmailDiffusionEvent">
	<!--- fr.consotel.api.ibis.publisher.handler.IDelivery --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IDelivery">
		<!--- Constructeur parent --->
		<cfset SUPER.createInstance()>
		<!--- Propriétes Mail spécifiques à cette implémentation (Configuration du BackOffice) --->
		<cfset VARIABLES.EMAIL={SERVER="", USER="", PWD=""}>
		<cfreturn THIS>
	</cffunction>

	<cffunction access="public" name="getDeliveryProperties" returntype="Struct"
	hint="Retourne les propriétés retournées par la méthode de la classe parent en spécifiant des valeurs prédéfinies pour le FTP et la diffusion Mail
	Les propriétés FTP dont les valeurs sont prédéfinies pour cette implémentation sont : FTP_SERVER, FTP_USER, FTP_PWD, SFTP, FTP_DIR
	Les propriétés Mail dont les valeurs sont prédéfinies pour cette implémentation sont : MAIL_SERVER, MAIL_USER, MAIL_PWD">
		<cfargument name="reporting" type="fr.consotel.api.ibis.publisher.reporting.IReporting"  required="true" hint="Reporting à exécuter">
		<cfset var reportingImpl=ARGUMENTS["reporting"]>
		<cfset var deliveryProperties=SUPER.getDeliveryProperties(reportingImpl)>
		<!--- Ajout des propriétés FTP spéficiques à cette implémentation : Les valeurs existantes sont écrasées --->
		<cfset structAppend(deliveryProperties,{
			FTP_SERVER=VARIABLES.FTP.SERVER, FTP_USER=VARIABLES.FTP.USER, FTP_PWD=VARIABLES.FTP.PWD,
			SFTP=VARIABLES.FTP.SFTP, FTP_DIR=VARIABLES.FTP.DIR
		},TRUE)>
		<cfreturn deliveryProperties>
	</cffunction>
	
	<cffunction access="public" name="handleReportingEvent" returntype="void"
	hint="Crée un évènement de type fr.consotel.api.ibis.publisher.handler.event.EmailEvent à partir de event pour en extraire les propriétés de diffusion mail
	Si event est déjà de ce type, il est utilisé comme tel et il n'y a pas d'instanciation supplémentaire d'évènement de type EmailEvent
	Une exception est levée si event n'est pas de type fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent
	Cette implémentation diffuse par mail le contenu du reporting à partir de l'évènement EmailEvent ainsi créé
	Une notification mail est envoyée à onFailureTo() si une des conditions suivantes est vérifiées pour l'évènement EmailEvent créé :
	- L'adresse expéditeur ou destinataire n'est pas une adresse mail valide. La validation est effectuée avec isValid()
	- Le statut de l'évènement indique une erreur : event.getStatus() vaut event.FAILURE() ou event.EXCEPTION()
	Dans ce cas le contenu retourné par event.getDeliveryInfos() est diffusé dans le corps du mail
	Sauf mention contraire cette notification est envoyée en utilisant les propriétés mail de l'API (fr.consotel.api.ibis.publisher.reporting.impl.ReportingLogging)
	La diffusion de la notification lèvera une exception si onFailureTo() n'est pas une adresse mail valide
	La méthode de la classe parent est appelée en 1er pour afficher un message de log contenant la valeur de event.getStatus()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent"  required="true" hint="Evènement de type IDeliveryEvent">
		<cfset var reportingEvent=ARGUMENTS["event"]>
		<!--- Instanciation d'un évènement de type EmailEvent et traitement correspondant --->
		<cfset var emailEvent=getDeliveryEvent(reportingEvent)>
		<cfset handleEmailEvent(emailEvent)>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.DelegateDiffusion --->
	<cffunction access="public" name="dispatchEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.EmailDiffusionEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.EmailDiffusionEvent">
	</cffunction>
	
	<cffunction access="private" name="deliveryEventType" returntype="String" hint="fr.consotel.api.ibis.publisher.handler.event.EmailEvent">
		<cfreturn "fr.consotel.api.ibis.publisher.handler.event.EmailEvent">
	</cffunction>
	
	<cffunction access="private" name="getDeliveryEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent">
		<cfargument name="sourceEvent" type="fr.consotel.api.ibis.publisher.handler.event.IReportingEvent">
		<cfset var deliveryEvent=ARGUMENTS["sourceEvent"]>
		<!--- Si event n'est pas de type IDeliveryEvent : Le reporting a été exécuté avec cette stratégie --->
		<cfif NOT isInstanceOf(deliveryEvent,"fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent")>
			<cfset var deliveryEventImpl="fr.consotel.api.ibis.publisher.handler.event.FtpEvent">
			<cfset deliveryEvent=createObject("component",deliveryEventImpl).createDeliveryEvent(ARGUMENTS["sourceEvent"])>
		</cfif>
		<cfreturn SUPER.getDeliveryEvent(deliveryEvent)>
	</cffunction>

	<!--- fr.consotel.api.ibis.publisher.delivery.Email --->
	<cffunction access="public" name="onFailureTo" returntype="String"
	hint="Retourne l'adresse destinataire utilisée pour notifier que le statut de l'évènement indique un erreur ou que les adresses sont invalides
	Cette implémentation retourne le destinataire par défaut de l'API Reporting">
		<cfreturn SUPER.getNotificationTo()>
	</cffunction>

	<cffunction access="private" name="handleEmailEvent" returntype="void" hint="Effectue le traitement de l'évènement EmailEvent
	Le handler handleDiffusionStatus() est appelé avec le statut EmailEvent.INVALID_CONTENT() si l'une des adresses expéditeur ou destinataire est invalide">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"  required="true" hint="Provient de handleReportingEvent()">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<!--- Si event.getStatus() indique une erreur ou une exception --->
		<cfif (emailEvent.getStatus() EQ emailEvent.FAILURE()) OR (emailEvent.getStatus() EQ emailEvent.EXCEPTION())>
			<cfset handleDiffusionStatus(emailEvent,emailEvent.getStatus())>
		<cfelse>
			<!--- Si les adresses expéditeur et destinataire sont valides --->
			<cfset emailList = emailEvent.getTo()>
			<cflog text="@@@@@ ---------- email list check start ---------- @@@@@">
			<cflog text="@@@@@ ---------- email list #emailEvent.getTo()# ---------- @@@@@">				
			<cfset isMailOk = TRUE>			
			<cfloop list=#emailList# index=_email delimiters="," >
				<cfset isMailOk = isMailOk and IsValid("email",TRIM(_email))>				
				<cflog text="@@@@@ ---------- #TRIM(_email)# isValid : #IsValid('email',TRIM(_email))# ----- isListOk : #isMailOk# ---------- @@@@@">
			</cfloop>
			<cflog text="@@@@@ ---------- email list check end ---------- @@@@@">
			<cfif isValid("email",emailEvent.getFrom()) AND (isMailOk eq TRUE)>
				<cfset diffuseEmail(emailEvent)>
			<!--- Si les adresses expéditeur et destinataire sont invalides --->
			<cfelse>
				<cfset handleDiffusionStatus(emailEvent,emailEvent.INVALID_ADDRESS())>
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="handleDiffusionStatus" returntype="void"
	hint="Handler appelé après le diffusion mail du contenu avec le statut de diffusion correspondant. Dispatche l'évènement de type dispatchEventType()
	Crée un évènement dont le type est retourné par dispatchEventType() dont le statut sera égal à diffusionStatus puis le passe à dispatchDeliveryEvent()
	Le handler est appelé avec l'évènement créé en paramètre si le statut de diffusion est égal à une des valeurs suivantes :
	- EmailEvent.FAILURE() : Le status du rapport correspondant est en Echec
	- EmailEvent.EXCEPTION() : Le traitement de la notification du rapport par l'API Reporting a rencontré une erreur
	- EmailEvent.INVALID_CONTENT() : Le type du contenu ne permet pas de le diffuser dans le corps du mail
	- EmailEvent.INVALID_ADDRESS() : Une des adresses expéditeur ou destinataire n'est pas une adresse mail valide">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent" required="true" hint="EmailEvent correspondant">
		<cfargument name="diffusionStatus" type="String" required="true" hint="Statut de diffusion mail">
		<cfset var diffusionEvent=createObject("component",dispatchEventType()).createDeliveryEvent(ARGUMENTS["event"],ARGUMENTS["diffusionStatus"])>
		<!--- Diffuse une notification mail si le statut de diffusion indique la diffusion mail du contenu du reporting n'a pas pu etre effectuée --->
		<cfset logInfo("Email diffusion status : " & diffusionEvent.getStatus())>
		<cfif (diffusionEvent.getStatus() EQ diffusionEvent.FAILURE()) OR (diffusionEvent.getStatus() EQ diffusionEvent.EXCEPTION()) OR
			(diffusionEvent.getStatus() EQ diffusionEvent.INVALID_CONTENT()) OR (diffusionEvent.getStatus() EQ diffusionEvent.INVALID_ADDRESS())>
			<cfset diffusionNotPerformedStatus(diffusionEvent)>
		</cfif>
		<!--- Dispatch de l'évènement de type dispatchEventType() : EmailDiffusionEvent --->
		<cfset SUPER.dispatchDeliveryEvent(diffusionEvent)>
	</cffunction>
	
	<cffunction access="private" name="diffuseEmail" returntype="void" hint="Diffuse le contenu du reporting en fonction des cas suivants :
	- Si event.getAttachment() est une chaine vide : Le contenu retourné par event.getContent() est diffusé dans le corps du mail
	Le handler handleDiffusionStatus() est appelé avec le statut EmailEvent.INVALID_CONTENT() si event.isTextContent() vaut FALSE
	- Sinon si le contenu est diffusé en tant que pièce jointe avec le nom retourné par getAttachment() en utilisant event.getContentAsLocalFile()
	Si event.getContentAsLocalFile() retourne une chaine vide alors la pièce jointe correspond au contenu retourné par event.getContent()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"  required="true" hint="Provient de handleEmailEvent()">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<cfset var localAttachment=emailEvent.getAttachment()>
		<!--- Diffusion du contenu dans le corps du mail --->
		<cfif TRIM(localAttachment) EQ "">
			<!--- Si le contenu à diffuser dans le corps du mail est de type texte : Le mail est diffusé avec le contenu --->
			<cfif emailEvent.isTextContent()>
				<cfset diffuseEmailContent(emailEvent)>
			<cfelse>
				<cfset handleDiffusionStatus(emailEvent,emailEvent.INVALID_CONTENT())>
			</cfif>
		<!--- Diffusion du contenu dans en tant qu'attachement mail --->
		<cfelse>
			<cfset diffuseEmailAttachment(emailEvent)>
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="diffuseEmailContent" returntype="void"
	hint="Diffuse le contenu du reporting dans le corps du mail. Puis passe event à handleDiffusionStatus() avec le statut event.getStatus()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"  required="true" hint="Provient de handleEmailEvent()">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<cfset var mailProperties=getEmailProperties(emailEvent)>
		<cfset var reportingContent=emailEvent.getContent("","")>
		<cfmail attributecollection="#mailProperties#">
			<html>
				<head>
					<style type="text/css">
					body {background-color: ##FFFFFF;}
					</style>
				</head>
				<cfoutput>
					#reportingContent#<br>
				</cfoutput>
			</html>
		</cfmail>
		<!--- Traitement du statut de diffusion correspondant --->
		<cfset handleDiffusionStatus(emailEvent,event.getStatus())>
	</cffunction>
	
	<cffunction access="private" name="diffuseEmailAttachment" returntype="void"
	hint="Diffuse le contenu du reporting dans le corps du mail. Puis passe event à handleDiffusionStatus() avec le statut event.getStatus()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"  required="true" hint="Provient de handleEmailEvent()">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<cfset var mailProperties=getEmailProperties(emailEvent)>
		<cfset var attachmentName=emailEvent.getAttachment()>
		<!--- Nom de l'attachement (Chaine vide lorsque le contenu est à diffuser dans le corps du mail) --->
		<cfset var localFileAttachment=emailEvent.getContentAsLocalFile(emailEvent.getAttachment(),"")>
		<cfmail attributecollection="#mailProperties#">
			<!--- Si event..getContentAsLocalFile() est une chaine vide alors la pièce jointe correspond au contenu retourné par event.getContent() --->
			<cfif TRIM(localFileAttachment) EQ "">
				<cfset var reportingContent=emailEvent.getContent("","")>
				<cfmailparam file="#attachmentName#" type="#emailEvent.getContentType()#" content="#reportingContent#">
			<cfelse>
				<cfmailparam file="#localFileAttachment#" type="#emailEvent.getContentType()#" remove="true">
			</cfif>
			<!--- Contenu du corps du mail --->
			<cfoutput>#getEmailBody(emailEvent)#</cfoutput>
		</cfmail>
		<!--- Si le chemin du fichier local correspond à l'attachement n'est pas une chaine vide : Le répertoire du fichier correspondant est supprimé --->
		<cfif TRIM(localFileAttachment) NEQ "">
			<cfset deleteAttachmentDir(emailEvent,localFileAttachment)>
		</cfif>
		<!--- Traitement du statut de diffusion correspondant --->
		<cfset handleDiffusionStatus(emailEvent,event.getStatus())>
	</cffunction>

	<cffunction access="private" name="getEmailBody" returntype="Any" hint="Retourne le contenu utilisé comme corps du mail. Retourne une chaine vide">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"  required="true" hint="Provient de handleEmailEvent()">
		<cfreturn "">
	</cffunction>
	
	<cffunction access="private" name="diffusionNotPerformedStatus" returntype="void"
	hint="Appelé par handleDiffusionStatus() pour diffuser une notification générique du statut de diffusion au destinataire par défaut de l'API Reporting">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailDiffusionEvent" required="true" hint="Provient de handleDiffusionStatus()">
		<cfset var diffusionEvent=ARGUMENTS["event"]>
		<cfset var MAIL_API=getMailProperties()>
		<cfset var mailSubject="Diffusion mail non effectuée (Statut) : " & diffusionEvent.getSubject()>
		
		
		
		
		
		<cfsavecontent variable="body">
		  	<cfoutput>
				<b>La stratégie de diffusion Email n'a pas effectué la diffusion mail d'un reporting en raison du statut suivant : #diffusionEvent.getStatus()#</b><br><br>
				#diffusionEvent.getDeliveryInfos()#<br><br>
				<b>Implémentation de cette stratégie :</b> #getMetadata(THIS).NAME#
			</cfoutput>
		</cfsavecontent>
		
		<cftry>
			<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
			<cfset mLog.setIDMNT_CAT(13)>
			<cfset mLog.setIDMNT_ID(30)>
			<cfset mLog.setBODY(body)>
			
			<cfinvoke 
				component="fr.consotel.api.monitoring.MLogger"
				method="logIt"  returnVariable = "result">
				
				<cfinvokeargument name="mLog" value="#mLog#">  
				
			</cfinvoke>	
			
		<cfcatch type="any">
			<cfmail from="MLog <monitoring@saaswedo.com>" 
					to="monitoring@saaswedo.com" 
					server="mail-cv.consotel.fr" 
					port="25" type="text/html"
					subject="test log monitor">
						<cfdump var="#cfcatch#">						
			</cfmail>
		</cfcatch>
		</cftry>
		
		
		
		<!---
		<cfmail attributecollection="#MAIL_API.ERROR#" to="#onFailureTo()#" subject="#mailSubject#" priority="highest">
			<cfoutput>
				<b>La stratégie de diffusion Email n'a pas effectué la diffusion mail d'un reporting en raison du statut suivant : #diffusionEvent.getStatus()#</b><br><br>
				#diffusionEvent.getDeliveryInfos()#<br><br>
				<b>Implémentation de cette stratégie :</b> #getMetadata(THIS).NAME#
			</cfoutput>
		</cfmail>
		--->
		
		
		
	</cffunction>
	
	<cffunction access="private" name="deleteAttachmentDir" returntype="void" hint="Supprime le répertoire de l'attachement lorsque ce dernier est supprimé
	L'existence du fichier local (attachement) est vérifiée toutes les 5 secs. Son répertoire parent est supprimé lorsque l'attachement n'existe plus
	Le processus d'attente est limité à une durée maximale de 3 minutes auquel cas une notification mail est diffusé à onFailureTo()">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"  required="true" hint="Provient de handleReportingEvent()">
		<cfargument name="asboluteLocalPath" type="String"  required="true" hint="Fichier à supprimer retourné par EmailEvent.getContentAsLocalFile()">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<cfset var localFileAbsPath=ARGUMENTS["asboluteLocalPath"]>
		<!--- Durée max d'attente : 3 minutes --->
		<cfset var maxDuration=180 * 1000>
		<!--- Intervale d'attente : 5 secondes --->
		<cfset var waitInterval=5 * 1000>
		<!--- Durée d'attente (En cours) --->
		<cfset var waitSince=0>
		<!--- Condition d'attente : Le fichier existe ET la durée max d'attente n'est pas dépassée --->
		<cfset var waitCondition=(fileExists(localFileAbsPath) AND (waitSince LT maxDuration))>
		<cfset logInfo(listLast(getMetadata(THIS).NAME,".") & " : WAITING FOR ATTACHMENT DELETED...")>
		<cfloop condition="waitCondition">
			<!--- Le processus courant est mis en pause pendant waitInterval --->
			<cfset sleep(waitInterval)>
			<cfset waitSince=waitSince+waitInterval>
			<cfset waitCondition=(fileExists(localFileAbsPath) AND (waitSince LT maxDuration))>
		</cfloop>
		<cfset logInfo(listLast(getMetadata(THIS).NAME,".") & " : WAITING STOPPED AFTER " & (waitSince/1000) & " secs")>
		<!--- Si le fichier local n'existe pas : Son répertoire parent est supprimé --->
		<cfif NOT fileExists(localFileAbsPath)>
			<!--- Nom du fichier local --->
			<cfset var localFileName=listLast(localFileAbsPath,emailEvent.getLocalPathSep())>
			<cfset var localDirPathLen=LEN(localFileAbsPath) - LEN(localFileName)>
			<!--- Chemin absolu du répertoire à supprimer --->
			<cfset var localDirAbsPath=LEFT(localFileAbsPath,localDirPathLen)>
			<cfdirectory action="delete" recurse="true" directory="#localDirAbsPath#">
			<cfset logInfo("ATTACHMENT TEMP DIR DELETED : " & localDirAbsPath)>
		<!--- Sinon le délai d'attente max est atteint et le fichier existe encore : Une notification mail est envoyée à onFailureTo() --->
		</cfif>
		<cfif waitSince GTE maxDuration>
			<cfset logWarning(listLast(getMetadata(THIS).NAME,".") & " : MAX WAITING DURATION REACHED [" & (maxDuration/1000) & " secs]")>
			<cfset var MAIL_API=getMailProperties()>
			<cfset var mailSubject="Avertissement(s) concernant la diffusion mail : " & event.getReportingName()>
			
			<cfsavecontent variable="body">
			   <cfoutput>
					<b>La stratégie de diffusion Email a rencontré un fonctionnement anormal correspondant au message d'avertissement suivant :</b><br><br>
					Le fichier suivant a été stocké dans un répertoire temporaire pour etre envoyé en tant que pièce jointe mail : #localFileAbsPath#<br>
					Une fois sa diffusion effectuée, son répertoire parent n'a pas pu etre supprimé pour la raison suivante :<br>
					<b>Le fichier n'a pas été automatiquement supprimé par le BackOffice sous le délai d'attente maximum prévu</b><br>
					Le délai d'attente maximum était de : #(maxDuration/1000)# secondes<br>
					Le fichier peut avoir été automatiquement supprimé à la réception de cette notification mais pas son répertoire parent<br>
					<b>Cet avertissement n'affecte pas la diffusion mail du contenu du reporting correspondant</b><br><br>
					#emailEvent.getDeliveryInfos()#<br>
					<b>Implémentation de cette stratégie :</b> #getMetadata(THIS).NAME#
				</cfoutput>
			</cfsavecontent>
			
			<cftry>
				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(13)>
				<cfset mLog.setIDMNT_ID(30)>
				<cfset mLog.setBODY(body)>
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			</cftry>
			
			
			<!---
			<cfmail attributecollection="#MAIL_API.WARNING#" to="#onFailureTo()#" subject="#mailSubject#" priority="highest">
				<cfoutput>
					<b>La stratégie de diffusion Email a rencontré un fonctionnement anormal correspondant au message d'avertissement suivant :</b><br><br>
					Le fichier suivant a été stocké dans un répertoire temporaire pour etre envoyé en tant que pièce jointe mail : #localFileAbsPath#<br>
					Une fois sa diffusion effectuée, son répertoire parent n'a pas pu etre supprimé pour la raison suivante :<br>
					<b>Le fichier n'a pas été automatiquement supprimé par le BackOffice sous le délai d'attente maximum prévu</b><br>
					Le délai d'attente maximum était de : #(maxDuration/1000)# secondes<br>
					Le fichier peut avoir été automatiquement supprimé à la réception de cette notification mais pas son répertoire parent<br>
					<b>Cet avertissement n'affecte pas la diffusion mail du contenu du reporting correspondant</b><br><br>
					#emailEvent.getDeliveryInfos()#<br>
					<b>Implémentation de cette stratégie :</b> #getMetadata(THIS).NAME#
				</cfoutput>
			</cfmail>
			--->
			
			
			
			
		</cfif>
	</cffunction>

	<cffunction access="private" name="getEmailProperties" returntype="Struct"
	hint="Retourne les propriétés de diffusion mail contenues dans event en tant que collection d'attributs CFMAIL">
		<cfargument name="event" type="fr.consotel.api.ibis.publisher.handler.event.EmailEvent"  required="true">
		<cfset var emailEvent=ARGUMENTS["event"]>
		<!--- Propriétés mail utilisés et provenant de l'évènement EmailEvent --->
		<cfset var mailProperties = {
			CHARSET="utf-8", TYPE="html", FROM=event.getFrom(), TO=event.getTo(), SUBJECT=event.getSubject(),
			REPLYTO=event.getReplyTo(), CC=event.getCC(), BCC=event.getBCC()
		}>
		<!--- DNS du serveur Mail --->
		<cfif LEN(TRIM(event.getMailServer())) GT 0>
			<cfset mailProperties.SERVER=event.getMailServer()>
		</cfif>
		<!--- Credentials du serveur Mail --->
		<cfif LEN(TRIM(event.getMailUserName())) GT 0>
			<cfset mailProperties.USERNAME=event.getMailUserName()>
			<cfset mailProperties.PASSWORD=event.getMailUserPassword()>
		</cfif>
		<cfreturn mailProperties>
	</cffunction>
</cfcomponent>