package composants.tb.graph
{
	import composants.tb.graph.charts.ColunmGraph_IHM;
	import composants.util.ConsoviewFormatter;
	
	import mx.charts.HitData;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	public class BarGraphBase extends GraphBase
	{
		protected var graph : ColunmGraph_IHM = new ColunmGraph_IHM();
		
		public function BarGraphBase()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE , initGraph);
		}
		
		private function initGraph(fe : FlexEvent):void{
			graph.x = 0;
			graph.y = 0;
			myGraphContener.addChild(graph);	
			graph.myChartHistorique.dataTipFunction = formatDataTip;			 
		}
		
		//formate les tootips du graph des themes
		private function formatDataTip(obj:HitData):String
		{
			if(obj)
			{
				var periode:String = obj.item[ResourceManager.getInstance().getString('M311', 'P_riode')]; 
			    var montantConso:Number = obj.item[ResourceManager.getInstance().getString('M311', 'Consommations')];
			    var montantAbo:Number = obj.item[ResourceManager.getInstance().getString('M311', 'Abonnements')];
			    
			    if (obj.element.name == ResourceManager.getInstance().getString('M311', 'abo')){
			    	return ResourceManager.getInstance().getString('M311', 'P_riode____b_')+periode+
			    	ResourceManager.getInstance().getString('M311', '__b__br_Montant_Abo____b__font_color____')+moduleTableauDeBordE0IHM.formatNumberCurrency(montantAbo,2)+"</font></b>";
			    }else if(obj.element.name == ResourceManager.getInstance().getString('M311', 'conso')){
			    	return ResourceManager.getInstance().getString('M311', 'P_riode____b_')+periode+
			    	ResourceManager.getInstance().getString('M311', '__b__br_Montant_Conso____b__font_color__')+moduleTableauDeBordE0IHM.formatNumberCurrency(montantConso,2)+"</font></b>"
			    }
			    
			    return ResourceManager.getInstance().getString('M311', 'P_riode____b_')+periode+
			    ResourceManager.getInstance().getString('M311', '__b__br_Montant_Conso____b__font_color__')+moduleTableauDeBordE0IHM.formatNumberCurrency(montantConso,2)+"</font></b>"+
			    ResourceManager.getInstance().getString('M311', '_br_Montant_Abo____b__font_color____ff00')+moduleTableauDeBordE0IHM.formatNumberCurrency(montantAbo,2)+"</font></b>";
			}
			else
			{
				return "";
			}
		    
		}
	}
}