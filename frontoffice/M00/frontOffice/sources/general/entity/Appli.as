package general.entity
{
	public class Appli
	{
		private var _codeApp:int;
		private var _appName:String;
		
		public function Appli(intA:int, strA:String)
		{
			this._codeApp = intA;
			this._appName = strA;
		}

		public function get codeApp():int
		{
			return _codeApp;
		}
		public function get appName():String
		{
			return _appName;
		}
	}
}