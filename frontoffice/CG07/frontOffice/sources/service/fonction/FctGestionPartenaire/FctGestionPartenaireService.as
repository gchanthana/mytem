package service.fonction.FctGestionPartenaire
{
	import entity.ContratVO;
	import entity.FormationVO;
	import entity.InterlocuteurVO;
	import entity.PartenaireVO;
	import entity.SiteVO;
	
	import utils.abstract.AbstractRemoteService;

	public class FctGestionPartenaireService extends AbstractRemoteService
	{
		[Bindable] 
		public var myDatas		:FctGestionPartenaireDatas;
		public var myHandlers	:FctGestionPartenaireHandlers;
		
		public function FctGestionPartenaireService()
		{
			this.myDatas 		= new FctGestionPartenaireDatas();
			this.myHandlers 	= new FctGestionPartenaireHandlers(myDatas);
		}
		
		public function getListePartenaires(key:String, code:String, idUser:int):void
		{
			var data:Object = new Object();
			data.idUser = idUser;
			doRemoting(myHandlers.getListePartenairesHandler,key, code, data);
		}

		// Appel du service pour récupérer les sites du partenaire
		public function getListeSitesPartenaire(key:String, code:String, idPartenaire:int):void
		{
			var data:Object = new Object();
			data.IDPARTENAIRE = idPartenaire;
			doRemoting(myHandlers.getListeSitesHandler,key, code, data);
		}
		
		// Appel du service pour récupérer le contrat ACTIF
		public function getListeContratsPartenaire(key:String, code:String, idPartenaire:int):void
		{
			var data:Object = new Object();
			data.IDPARTENAIRE = idPartenaire;
			doRemoting(myHandlers.getListeContratsHandler,key, code, data);
		}
		
		// Appel du service pour récupérer l'historique des formations d'un partenaire
		public function getListeFormationsPartenaire(key:String, code:String, idPartenaire:int):void
		{
			var data:Object = new Object();
			data.IDPARTENAIRE = idPartenaire;
			
			doRemoting(myHandlers.getListeFormationsHandler,key, code, data);
		}
		
		// Appel du service pour récupérer les interlocuteurs du partenaire
		public function getListeInterlocuteursPartenaire(key:String, code:String, idPartenaire:int):void
		{
			var data:Object = new Object();
			data.IDPARTENAIRE = idPartenaire;
			
			doRemoting(myHandlers.getListeInterlocuteursHandler,key, code, data);
		}
		
		// Appel du service pour récupérer les niveaux de certification
		public function getNiveauxCertification(key:String, code:String):void
		{
			var data:Object = new Object();
			
			doRemoting(myHandlers.getNiveauxCertificationsHandler,key, code, data);
		}
		
		// Appel du service pour récupérer les types de plan tarifaire
		public function getTypesPlanTarifaire(key:String, code:String):void
		{
			var data:Object = new Object();
			
			doRemoting(myHandlers.getTypesPlanTarifaireHandler,key, code, data);
		}
		
		public function ajouterEditerSite(key:String, code:String, idPartenaire:int, site:SiteVO):void
		{
			var data:Object = new Object();
			
			data.p_Fc_Partenaireid 	= idPartenaire;
			data.p_idfc_site 		= site.id;
			data.p_libelle_site 	= site.libelle;
			data.p_adresse1 		= site.adresse1;
			data.p_adresse2			= site.adresse2;
			data.p_code_postal		= site.codePostale; 
			data.p_ville			= site.ville;
			data.p_pays				= site.pays;	
			data.p_bool_adresse_facturation = site.adresseFacturation;
			data.p_Date_Deb 		= site.dateDebut; 
			data.p_Date_Fin			= site.dateFin;
			
			doRemoting(myHandlers.ajoutEditSiteHandler,key, code, data);
		}

		public function modifierContrat(key:String, code:String, idPartenaire:int, contrat:ContratVO):void
		{
			var data:Object = new Object();
			data.p_Fc_Partenaireid = idPartenaire;
			data.p_idFc_contrat 	= contrat.id;
			data.p_date_deb_contrat = contrat.dateDebut;
			data.p_date_fin_contrat = contrat.dateFin;
			
			doRemoting(myHandlers.modifContratHandler,key, code, data);
		}
		
		public function desactiverContrat(key:String, code:String, idPartenaire:int, contrat:ContratVO):void
		{
			var data:Object = new Object();
			data.p_idFc_contrat 	= contrat.id;
			data.p_Fc_Partenaireid 	= idPartenaire;
			data.p_date_deb_contrat = contrat.dateDebut;
			data.p_date_fin_contrat = contrat.dateFin;
			
			doRemoting(myHandlers.desactiveContratHandler,key, code, data);
		}
		
		public function ajouterEditerFormation(key:String, code:String, idPartenaire:int, formation:FormationVO):void
		{
			var data:Object = new Object();
			
			data.IDFORMATIONPART 	= formation.id;
			data.IDPARTENAIRE 		= idPartenaire;
			data.IDSUJETFORMATION 	= formation.objSujetFormation.id;
			data.DATE_FORMATION 	= formation.dateFormation;
			data.IDLIEU_FORMATION 	= formation.objLieuFormation.id;
			
			doRemoting(myHandlers.ajoutEditerFormationHandler,key, code, data);
		}
		
		public function ajouterEditerParticipantsFormation(key:String, code:String, formation:FormationVO, listeLoginID:String):void
		{
			var data:Object = new Object();
			data.IDFORMATIONPART = formation.id;
			data.L_APPLOGINID = listeLoginID;
			
			doRemoting(myHandlers.ajoutEditParticipantsHandler,key, code, data);
		}
		
		public function ajouterEditerInterloc(key:String, code:String, idPartenaire:int, interloc:InterlocuteurVO):void
		{
			var data:Object = new Object();
			data.p_idfc_interlocuteur	= interloc.id;
			data.p_Fc_Partenaireid		= idPartenaire;
			data.p_Civilite 			= interloc.civilite;
			data.p_Nom 					= interloc.nom;
			data.p_Prenom 				= interloc.prenom;
			data.p_Qualite 				= interloc.qualite;
			data.p_Email_Inter 			= interloc.email;
			data.p_Bool_Principal_Inter = interloc.principal;
			data.p_Default_Langue 		= interloc.langue;
			data.p_Date_Deb 			= interloc.dateDebut;
			data.p_Date_Fin 			= interloc.dateFin;
			
			doRemoting(myHandlers.ajoutEditerInterlocHandler,key, code, data);
		}
		
		public function ajouterContrat(key:String, code:String, idPartn:int, nvContrat:ContratVO):void
		{
			var data:Object = new Object();
			data.p_Fc_Partenaireid 		= idPartn;
			data.p_idfc_certification 	= nvContrat.objCertification.id;
			data.p_idfc_type_plan_tarif	= nvContrat.objPlanTarifaire.objTypePlanTarif.id;
			data.p_date_deb_contrat		= nvContrat.dateDebut;
			data.p_date_fin_contrat		= nvContrat.dateFin;
			data.p_date_deb_plantarif 	= nvContrat.objPlanTarifaire.dateDebut;
			data.p_date_fin_plantarif	= nvContrat.objPlanTarifaire.dateFin;
			
			doRemoting(myHandlers.ajoutContratHandler,key, code, data);
		}
		
		public function getListeSujetsFormation(key:String, code:String):void
		{
			var data:Object = new Object();
			doRemoting(myHandlers.getListeSujetsFormationHandler,key, code, data);
		}
		
		public function getListeLieuxFormation(key:String, code:String):void
		{
			var data:Object = new Object();
			doRemoting(myHandlers.getListeLieuxFormationHandler,key, code, data);
		}
		
		public function getListeLangues(key:String, code:String):void
		{
			var data:Object = new Object();
			doRemoting(myHandlers.getListeLanguesHandler,key, code, data);
		}
		
		public function getListeParticipantsFormation(key:String, code:String, idPartn:int, idFormation:int):void
		{
			var data:Object = new Object();
			data.p_fc_partenaireid 	= idPartn;
			data.p_idfc_formation 	= idFormation;
			doRemoting(myHandlers.getListeParticipantsFormationHandler,key, code, data);
		}
		
		public function getListeCivilites(key:String, code:String):void
		{
			var data:Object = new Object();
			
			doRemoting(myHandlers.getListeCivilitesHandler,key, code, data);
		}
		
		public function ajouterPartenaire(key:String, code:String, nvPartenaire:PartenaireVO, nvContrat:ContratVO, nvSite:SiteVO, nvInterloc:InterlocuteurVO):void
		{
			var dataPartenaire:Object = new Object();
			dataPartenaire.p_partenaireid	= nvPartenaire.ID;
			dataPartenaire.p_nom			= nvPartenaire.NOM;
			dataPartenaire.p_adresse		= '';
			dataPartenaire.p_origine		= 0;
			dataPartenaire.p_idorigine		= 0;
			dataPartenaire.p_isProspect = nvPartenaire.isProspect;
			
			// Données du contrat : un contrat de partenaire est l'ensemble (id = 0, Contrat, PlanTarifaire)
			var dataContrat:Object = new Object();
			dataContrat.p_Fc_Partenaireid 		= 0; // un nouveau partenaire
			dataContrat.p_idfc_certification 	= nvContrat.objCertification.id;
			dataContrat.p_idfc_type_plan_tarif	= nvContrat.objPlanTarifaire.objTypePlanTarif.id;
			dataContrat.p_date_deb_contrat		= nvContrat.dateDebut;
			dataContrat.p_date_fin_contrat		= nvContrat.dateFin;
			dataContrat.p_date_deb_plantarif 	= nvContrat.objPlanTarifaire.dateDebut;
			dataContrat.p_date_fin_plantarif	= nvContrat.objPlanTarifaire.dateFin;
			
			// Données du Site
			var dataSite:Object = new Object();
			if(nvSite != null){
				dataSite.p_idfc_site 		= nvSite.id; // 0 pour un nouveau site
				dataSite.p_Fc_Partenaireid 	= 0;// sera récupérer au back à la suite de la création du contrat
				dataSite.p_libelle_site 	= nvSite.libelle;
				dataSite.p_adresse1 		= nvSite.adresse1;
				dataSite.p_adresse2			= nvSite.adresse2;
				dataSite.p_code_postal		= nvSite.codePostale; 
				dataSite.p_ville			= nvSite.ville;
				dataSite.p_pays				= nvSite.pays;	
				dataSite.p_bool_adresse_facturation = nvSite.adresseFacturation;
				dataSite.p_Date_Deb 		= nvSite.dateDebut; 
				dataSite.p_Date_Fin			= nvSite.dateFin;
			}
			
			// Données de l'interlocuteur
			var dataInterloc:Object = new Object();
			if(nvInterloc != null){
				dataInterloc.p_idfc_interlocuteur	= nvInterloc.id; // 0 pour un nouveau interloc
				dataInterloc.p_Fc_Partenaireid		= 0; // Sera changer au Back
				dataInterloc.p_Civilite 			= nvInterloc.civilite;
				dataInterloc.p_Nom 					= nvInterloc.nom;
				dataInterloc.p_Prenom 				= nvInterloc.prenom;
				dataInterloc.p_Qualite 				= nvInterloc.qualite;
				dataInterloc.p_Email_Inter 			= nvInterloc.email;
				dataInterloc.p_Bool_Principal_Inter = nvInterloc.principal;
				dataInterloc.p_Default_Langue 		= nvInterloc.langue;
				dataInterloc.p_Date_Deb 			= nvInterloc.dateDebut;
				dataInterloc.p_Date_Fin 			= nvInterloc.dateFin;
			}
			
			var data:Object = new Object();
			data.dataPartenaire = dataPartenaire;
			data.dataContrat	= dataContrat;
			data.dataSite		= dataSite;
			data.dataInterloc	= dataInterloc;
			
			doRemoting(myHandlers.ajouterPartenaireHandler, key, code, data);
		}
	}
}