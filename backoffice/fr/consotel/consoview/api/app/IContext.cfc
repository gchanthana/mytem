<cfinterface displayName="fr.consotel.consoview.api.app.IContext">
	<cffunction access="public" name="initInstance" returntype="fr.consotel.consoview.api.app.IContext" hint="Initialize instance : Explicit Call Only">
	</cffunction>
	
	<cffunction access="public" name="getContextInfos" returntype="any" hint="Returns Context Infos (Global Properties)">
		<cfargument name="contextKey" type="string" required="true">
	</cffunction>
</cfinterface>