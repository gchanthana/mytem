<cfcomponent displayname="Gestion des sociï¿½tï¿½s dans le module Gestion des groupes" output="false">
<cffunction name="getListeRefClient" returntype="Query" output="false"access="remote">
	<cfargument name="idSociete" type="numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getListeRefClient">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idSociete#">
		<cfprocresult name="result"/>
	</cfstoredproc>
	<cfreturn result>
</cffunction>
<cffunction name="getListeOperateur" output="false"access="remote">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getListeOperateur">
		<cfprocresult name="result"/>
	</cfstoredproc>
	<cfreturn result>
</cffunction>
<cffunction name="getAffectSoc" returntype="query" output="false"access="remote">
	<cfargument name="operateurID" type="numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getAffectSoc">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#operateurID#">
		<cfprocresult name="result"/>
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="getNonAffectSoc" returntype="query" output="false"access="remote">
	<cfargument name="operateurID" type="numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getNonAffectSoc">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#operateurID#">
		<cfprocresult name="result"/>
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="getAllSoc" returntype="query" output="false"access="remote">
	<cfargument name="operateurID" type="numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getAllSoc">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#operateurID#">
		<cfprocresult name="result"/>
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="addSociete" returntype="Numeric" output="false"access="remote">
	<cfargument name="p_libellesociete" type="String">
	<cfargument name="p_idOperateur" type="Numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.addSociete">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_libellesociete#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_idOperateur#">
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="updateSociete" returntype="Numeric" output="false"access="remote">
	<cfargument name="idref_client" type="Numeric">
	<cfargument name="libelle" type="String">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.update_societe">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idref_client#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result">
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="deleteSociete" returntype="Numeric" output="false"access="remote">
	<cfargument name="idref_client" type="Numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.deleteSociete">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idref_client#">
		<cfprocresult name="result"/>
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="affecteSociete" returntype="Numeric" output="false"access="remote">
	<cfargument name="tabIdRefCLient" type="array">
	<cfargument name="p_idracine" type="Numeric">
		<cfloop from="1" to="#arraylen(tabIdRefCLient)#" index="i">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.affecteSociete">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tabIdRefCLient[i].IDREF_CLIENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
	</cfloop>
<cfreturn result>
</cffunction>
<cffunction name="DesaffecteSociete" returntype="Numeric" output="false"access="remote">
	<cfargument name="tabIdRefCLient" type="Array">
	<cfargument name="p_idracine" type="Numeric">
	<cfloop from="1" to="#arraylen(tabIdRefCLient)#" index="i">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.DesaffecteSociete">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tabIdRefCLient[i].IDREF_CLIENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_idracine#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
	</cfloop>
<cfreturn result>
</cffunction>
<cffunction name="upd_maj_auto_collaborateur" returntype="Numeric" output="false"access="remote">
	<cfargument name="tabIdRefCLient" type="Array">
	<cfargument name="p_valeur" type="Numeric">
	<cfloop from="1" to="#arraylen(tabIdRefCLient)#" index="i">
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.upd_maj_auto_collaborateur">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#tabIdRefCLient[i].IDREF_CLIENT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_valeur#">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
		</cfstoredproc>
	</cfloop>
<cfreturn result>
</cffunction>
</cfcomponent>