package gestionparc.ihm.main
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestionparc.entity.Colonne;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.action.Invoker;
	import gestionparc.ihm.action.nouvellecommande.ActionNouvelleCommandeCommand;
	import gestionparc.ihm.action.nouvellecommande.ActionNouvelleCommandeReceiver;
	import gestionparc.ihm.fiches.changementcomptefacturation.ChangementCompteFacturationIHM;
	import gestionparc.ihm.fiches.ficheEntity.FicheImportMasseIHM;
	import gestionparc.ihm.fiches.pool.PanelExclusionPoolIHM;
	import gestionparc.ihm.fiches.pool.PanelInclusionPoolIHM;
	import gestionparc.ihm.fiches.popup.PopupAjoutUnitaireIHM;
	import gestionparc.ihm.main.export.DataGridDataExporter;
	import gestionparc.ihm.renderer.commonrenderer.SelectRenderer;
	import gestionparc.ihm.renderer.headerrenderer.ToggleSortHeaderRendererIHM;
	import gestionparc.services.comptefacturation.CompteFacturationService;
	import gestionparc.services.mdm.MDMService;
	import gestionparc.services.servicelocator.ServiceLocatorView;
	import gestionparc.utils.PanelExclusionInclusionUtils;
	import gestionparc.utils.gestionparcConstantes;
	
	import paginatedatagrid.pagination.Pagination;
	import paginatedatagrid.pagination.event.PaginationEvent;

	public class MainGridImpl extends VBox
	{

		[Bindable]
		public var nbEltSelected:Number=0;

		public var boxActions:HBox;
		public var cb_choix_action:ComboBox;
		public var cb_choix:ComboBox;
		public var chbx_all:CheckBox;
		public var btn_action_ok:Button;
		public var btn_action:Button;
		public var lblAction:Label;

		[Bindable]
		public var dgMain:DataGrid;
		public var cb_gestion_pool:ComboBox;
		public var paginationComp:Pagination;

		private static const nbItemParPage:Number=24;
		private var _mdmSrv:MDMService=new MDMService();
		private var _serviceLocator:ServiceLocatorView;
		private var _serviceCompteFacturation:CompteFacturationService;
		private var _isPassedThere:Boolean=false;
		private var _drawColumn:Boolean=true;
		private var _positionHorizontalScroll:int=0;

		/** variable pour le changement de compte facture operateur */
		private var typeOperateur:ArrayCollection=new ArrayCollection();
		private var typeDistributeur:ArrayCollection=new ArrayCollection();
		private var operateurName:String;
		private var _tempListeSelected:ArrayCollection=new ArrayCollection();

		public function MainGridImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(event:FlexEvent):void
		{
			initListener();
		}

		public static function removeActionEnrollActionOrNot():void
		{
			var isMDM:Boolean=false;
			var obj:Object=moduleGestionParcIHM.globalParameter;

			if (moduleGestionParcIHM.globalParameter.userAccess.hasOwnProperty("MDM_N1") && moduleGestionParcIHM.globalParameter.userAccess.MDM_N1 != null && moduleGestionParcIHM.globalParameter.racineInfos && moduleGestionParcIHM.globalParameter.racineInfos.length > 0 && moduleGestionParcIHM.globalParameter.racineInfos[0].BOOL_MDM == 1)
			{
				isMDM=true;
			}

			var len:int=gestionparcConstantes.listAction.length;

			for (var i:int=0; i < len; i++)
			{
				if (gestionparcConstantes.listAction[i].data == 10)
				{
					if (!isMDM)
					{
						gestionparcConstantes.listAction.removeItemAt(i);
						i--;
						len--;
					}
				}
			}
		}

		/**
		 * init des listeners de l'appli
		 **/
		public function initListener(bool:Boolean=true):void
		{
			if (bool)
			{
				SpecificationVO.getInstance().addEventListener(gestionparcEvent.REFRESH_MAIN_GRID, refreshData);
				paginationComp.addEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE, paginateSearch);
			}
			else
			{
				if (SpecificationVO.getInstance() && SpecificationVO.getInstance().hasEventListener(gestionparcEvent.REFRESH_MAIN_GRID))
					SpecificationVO.getInstance().removeEventListener(gestionparcEvent.REFRESH_MAIN_GRID, refreshData);
				if (paginationComp && paginationComp.hasEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE))
					paginationComp.removeEventListener(PaginationEvent.INTERVALLE_SELECTED_CHANGE, paginateSearch);
			}
			SpecificationVO.getInstance().dataGridView=dgMain; // dgMain :pour pouvoir acceder à cette référence dans ListeColonneImps
			if(dgMain)
				dgMain.addEventListener(ListEvent.ITEM_CLICK,dgClickHandler);
		}
		
		protected function dgClickHandler(event:ListEvent):void
		{
			dgMain.selectedItem.SELECTED  = !dgMain.selectedItem.SELECTED;
			dgMain.dataProvider.itemUpdated(dgMain.selectedItem);
			
			if(dgMain.selectedItem.SELECTED)
			{
				nbEltSelected++;
				if(!elementExists(dgMain.selectedItem, tempListeSelected))
					tempListeSelected.addItem(dgMain.selectedItem)
			}
			else
			{
				nbEltSelected--;
				tempListeSelected.source = this.removeElement(tempListeSelected.source, 'ID', dgMain.selectedItem.ID);
			}
			tempListeSelected.refresh();

			if (nbEltSelected == dgMain.dataProvider.length)
				chbx_all.selected=true;
			else
				chbx_all.selected=false;
		}
		
		/**
		 * Vérifie l'existance d'un element unique
		 */
		private function elementExists(element:Object, elements:ArrayCollection):Boolean
		{
			var trouv:Boolean = false;
			for each (var elet:Object in elements) 
			{
				if(elet.ID == element.ID){
					trouv = true;
					break;
				}
			}
			return trouv;
		}
		
		/**
		 * Supprime un élement d'un tableau et renvoi le nouveau tableau
		 */
		private function removeElement(array: Array, propertyName: String, value: String): Array
		{
			var newArray: Array = [];
			
			for (var i: int = 0; i < array.length; i++) {
				var item: Object = array[i];
				
				if (item && item.hasOwnProperty(propertyName)) {
					if (item[propertyName] != value)
						newArray.push(item);
				}
			}
			return newArray;
		}
		
		/**
		 * permet de creer la colonne des case à cocher tout a gauche
		 * @return
		 */
		public function createSelectedColumn():DataGridColumn
		{
			var dgc:DataGridColumn=new DataGridColumn("");
			dgc.itemRenderer=new ClassFactory(gestionparc.ihm.renderer.commonrenderer.SelectRenderer);
			dgc.resizable=false;
			dgc.sortable=false;
			dgc.width=20;
			return dgc;
		}

		public function refreshColumn():void
		{
			nbEltSelected=0;
			var columnTmp:Array=[];
			var k:int=0;
			columnTmp.push(createSelectedColumn());

			for each (var item:Colonne in SpecificationVO.getInstance().ViewObject.LISTE_COLONNE)
			{
				var dgc:DataGridColumn=new DataGridColumn();
				dgc.dataField=item.DATAFIELD;
				dgc.visible=item.VISIBLE; // afficher la colonne si true

				dgc.headerRenderer=new ClassFactory(ToggleSortHeaderRendererIHM);

				dgc.showDataTips=true;
				dgc.dataTipField=item.DATAFIELD;

				if (item.ITEMRENDERER != null)
					dgc.itemRenderer=new ClassFactory(item.ITEMRENDERER);
				dgc.headerText=item.LIBELLE;

				if (item.WIDTH > -1)
					dgc.width=item.WIDTH;

				if (item.LABEL_FUNCTION != null)
					dgc.labelFunction=item.LABEL_FUNCTION;

				columnTmp.push(dgc);
			}
			dgMain.columns=columnTmp;
			dgMain.rowHeight=24;

		}

		public function resetDG():void
		{
			dgMain.columns=[];
			tempListeSelected.source = [];
			paginationComp.initPagination(0, 0);
		}

		/**
		 * utiliser dans les fiches pour mettre à jour la data grid
		 */
		public function refreshData(ev:gestionparcEvent=null):void
		{
			initService();

			if (paginationComp.getCurrentIntervall())
			{
				_serviceLocator.service.getData(paginationComp.getCurrentIntervall().indexDepart + 1, nbItemParPage);
			}
		}

		/**
		 * cette methode est utilisé dans GestionparcMainImpl et lorsquel'on clique sur le bouton rechercher
		 */
		public function getData(ev:Event=null):void
		{
			_drawColumn=true;
			paginationComp.selectFirstIntervalle()
		}

		private function rechercherFinishedResultHandler(evt:gestionparcEvent):void
		{
			_serviceLocator.service.getModel().removeEventListener(gestionparcEvent.RECHERCHER_FINISHED, rechercherFinishedResultHandler)

			if (_drawColumn)
				refreshColumn();

			dgMain.dataProvider=_serviceLocator.service.restoreData();
			this.majTempListeSelected(dgMain.dataProvider as ArrayCollection);
			dgMain.horizontalScrollPosition=_positionHorizontalScroll; /** recupérer la position de scroll bar horizontal*/

			SpecificationVO.getInstance().nbRowDataGrid=_serviceLocator.service.getAllnbRow(); /** nb total des lignes de la data grid*/

			if (dgMain.dataProvider != null)
			{
				cb_choix_action.enabled=true;
				chbx_all.enabled=true;
				btn_action_ok.enabled=true;
			}

			paginationComp.initPagination(parseInt(evt.obj.toString()), MainGridImpl.nbItemParPage);
		}
		
		public function majTempListeSelected(listeInterval:ArrayCollection):void
		{
			for each(var tempv:Object in tempListeSelected)
			{
				for each (var elet:Object in listeInterval) 
				{
					if(tempv.ID == elet.ID )
						elet.SELECTED = true;
				}
			}
		}

		private function initService():void
		{
			_serviceLocator=null;
			var vueSelected:Number=1;
			if ((SpecificationVO.getInstance().idPool == SpecificationVO.POOL_PARC_GLOBAL_ID) ) // pool parc global
			{
				vueSelected=0;
				lblAction.visible=false;
				cb_choix.visible=false;
				btn_action.visible=false;
			}
			else if((SpecificationVO.getInstance().idPool == SpecificationVO.POOL_TOUT_PARC_ID)  || (SpecificationVO.pool_is_tout_parc == true)) // Tout le parc, vue pour tout le parc
			{
				vueSelected=2;
				lblAction.visible=false;
				cb_choix.visible=false;
				btn_action.visible=false;
			}
			else
			{
				vueSelected=1;
				lblAction.visible=true;
				cb_choix.visible=true;
				btn_action.visible=true;
			}
			_serviceLocator=new ServiceLocatorView(vueSelected); /**  idview :id la vue selectionnée , ici il y a d'une seule vue */
			_serviceLocator.service.getModel().addEventListener(gestionparcEvent.RECHERCHER_FINISHED, rechercherFinishedResultHandler, false, 0, true);
			_serviceCompteFacturation=new CompteFacturationService();
		}

		private function paginateSearch(evt:PaginationEvent):void
		{
			chbx_all.selected = false;
			initService();

			if (evt.itemIntervalleVO)
				_serviceLocator.service.getData(evt.itemIntervalleVO.indexDepart + 1, evt.itemIntervalleVO.tailleIntervalle);
			else
				_serviceLocator.service.getData(1, MainGridImpl.nbItemParPage);
		}

		/**
		 *	Inclure dans le pool des éléments selon leur type (collaborateur, terminal, ligne, sim)
		 **/
		public function clickAddElementPoolHandler(evt:MouseEvent):void
		{
			if (SpecificationVO.getInstance().idPoolNotValid > 0)
			{
				var fichePool:PanelInclusionPoolIHM=new PanelInclusionPoolIHM();
				addPopup(fichePool);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_de_gesti'), ResourceManager.getInstance().getString('M111', 'Attention'));
		}

		/**
		 *	(MouseEvent) Fonction d'ajout de terminal
		 **/
		public function clickAddImportMasseHandler(evt:MouseEvent):void
		{
			var spec:SpecificationVO=SpecificationVO.getInstance();

			if (spec.idPool < 0 && spec.idPoolNotValid < 0)
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_de_gesti'), ResourceManager.getInstance().getString('M111', 'Attention__'));
			}
			else
				afficherImportMasseMobile();
		}


		private function afficherImportMasseMobile(evt:gestionparcEvent=null):void
		{
			var ficheImportMobile:FicheImportMasseIHM=new FicheImportMasseIHM();
			addPopup(ficheImportMobile);
		}

		public function selectAllChbx(evt:Event):void
		{
			var longueur:int=(dgMain.dataProvider as ArrayCollection).length; /** sélection de tous les chbx du datagrid */
			var bool:Boolean = chbx_all.selected;

			for (var i:int=0; i < longueur; i++)
			{
				dgMain.dataProvider[i].SELECTED = bool;
				if(bool)
				{
					if(! elementExists(dgMain.dataProvider[i], tempListeSelected))
						tempListeSelected.addItem(dgMain.dataProvider[i])
				}
				else
				{
					if(elementExists(dgMain.dataProvider[i], tempListeSelected))
						tempListeSelected.source = this.removeElement(tempListeSelected.source, 'ID', dgMain.dataProvider[i].ID);
				}
				tempListeSelected.refresh();
			}

			dgMain.dataProvider.refresh();

			if (chbx_all.selected == true) /** affichage du nombre d'elements selectionnes */
			{
				nbEltSelected=dgMain.dataProvider.length;
			}
			else
			{
				nbEltSelected=0;
			}
		}

		/**
		 *	(MouseEvent) Fonction d'ajout d'une nouvelle commande
		 **/
		public function clickNewCommandeHandler(evt:MouseEvent):void
		{
			var invoker:Invoker=new Invoker();
			var receiver:ActionNouvelleCommandeReceiver=new ActionNouvelleCommandeReceiver();
			var command:ActionNouvelleCommandeCommand=new ActionNouvelleCommandeCommand(receiver);
			invoker.setCommand(command);
			invoker.executeCommand();
		}

		/**
		 *	(MouseEvent) Affichage de la popup d'ajout unitaire
		 **/
		public function clickAddAjoutUnitaireHandler(evt:MouseEvent):void
		{
			var popup:PopupAjoutUnitaireIHM=new PopupAjoutUnitaireIHM();
			addPopup(popup);
		}

		/**
		 *	(MouseEvent) Fonction d'exécution de l'action spécifiée pour les lignes du tableau
		 **/
		public function clickActionHandler(evt:MouseEvent):void
		{
			var tab:Array=[];
			var selectedCodeAction:int=cb_choix_action.selectedItem.data;
			var doublon:Number=0;
			var typeElement:Array=[];
			var k:int=0;
			var listeIdSousTete:Array=[];
			
			typeDistributeur.removeAll();
			typeOperateur.removeAll();
			
			for each (var obj:AbstractMatriceParcVO in dgMain.dataProvider)
			{
				if (obj.SELECTED == true)
				{
					/** stock dans typeOperateur l'operateur et dans typeDistributeur le distributeur (ils doivent etre unique !) */

					if (obj.OPERATEURID > 0 && !typeOperateur.contains(obj.OPERATEURID))
					{
						typeOperateur.addItem(obj.OPERATEURID);
						operateurName=obj.OPERATEUR;
					}

					if (obj.S_IDREVENDEUR > 0 && !typeDistributeur.contains(obj.S_IDREVENDEUR))
						typeDistributeur.addItem(obj.S_IDREVENDEUR);

					if (selectedCodeAction == 5) /** pour le changement de compte facturation */
					{
						if (obj.IDSOUS_TETE > 0 && obj.IDSIM > 0)
						{
							listeIdSousTete[k]=obj.IDSOUS_TETE;
							k++;
						}
					}

					/** stocker dans le tableau une liste d'idemploye, d'idequipement (terminal/sim) ou d'idsoustete */

					if (obj.IDEMPLOYE > 0)
					{
						if (doublon != obj.IDEMPLOYE)
						{
							doublon=obj.IDEMPLOYE;
							tab.push(doublon);
						}
						typeElement.push('collab');
					}
					else if (obj.IDTERMINAL > 0)
					{
						if (doublon != obj.IDTERMINAL)
						{
							doublon=obj.IDTERMINAL;
							tab.push(doublon);
						}
						typeElement.push('term');
					}
					else if (obj.IDSOUS_TETE > 0)
					{
						if (doublon != obj.IDSOUS_TETE)
						{
							doublon=obj.IDSOUS_TETE;
							tab.push(doublon);
						}
						typeElement.push('ligne');
					}
					else if (obj.IDSIM > 0)
					{
						if (doublon != obj.IDSIM)
						{
							doublon=obj.IDSIM;
							tab.push(doublon);
						}
						typeElement.push('sim');
					}
				}
			}

			var currentData:ArrayCollection=(dgMain.dataProvider as ArrayCollection);
			var longueur:int=currentData.length;
			var compteur:int=0;

			for (var i:int=0; i < longueur; i++)
			{
				if (currentData[i].SELECTED)
					compteur++;
			}

			/** affichage du nombre d'elements selectionnes */

			if (longueur == compteur)
				chbx_all.selected == true;
			else
				chbx_all.selected == false;

			chbx_all.validateNow();
			nbEltSelected=compteur;

			/** action sur les lignes sélectionnées du tableau de la vue courante */

			if (tab.length > 0)
			{
				switch (selectedCodeAction)
				{
					case 1: //renouvellerLignes();
						break;

					case 2: // resilierLignes();
						break;

					case 3:
						exportCSVLignes();
						break;

					case 4:
						if (tab.length > nbItemParPage)
							ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_pouvez_exclure_que_100__l_ments___l'), ResourceManager.getInstance().getString('M111', 'Attention____'));
						else
							excludeItemFromPool(tab, typeElement);
						break;

					case 5:

						if (typeDistributeur.length == 1 && typeOperateur.length == 1 && listeIdSousTete.length > 0)
						{
							_serviceCompteFacturation.model.addEventListener(gestionparcEvent.GET_COMPTE_FACTURATION, changeCompteFacturation);
							_serviceCompteFacturation.getCompteFacturation(listeIdSousTete.toString());
						}
						else
						{
							if (typeDistributeur.length == 1 && typeOperateur.length == 1)
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_au_moins_une_lig'), ResourceManager.getInstance().getString('M111', 'Attention____'));
							else
								ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_des__l_ments_pos'), ResourceManager.getInstance().getString('M111', 'Attention____'));
						}
						break;
				}
			}
			else if (selectedCodeAction == 6) /** auncun élement selectionné dans data grid */
				exportAllCSVLignes();
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_au_moins_un__l_m'), ResourceManager.getInstance().getString('M111', 'Attention____'));
		}

		private function changeCompteFacturation(event :gestionparcEvent):void
		{
			var listeElementsSelectiones:ArrayCollection=new ArrayCollection();
			var resultat:ArrayCollection=_serviceCompteFacturation.model.listeResultat;
			var objElementsSelectiones:Object;

			if(resultat.length >0)
			{
				for each (var obj:Object in resultat)
				{
					objElementsSelectiones=new Object();
					objElementsSelectiones.LIGNE=obj.SOUS_TETE;
					objElementsSelectiones.IDSOUS_TETE=obj.IDSOUS_TETE;
					objElementsSelectiones.COMPTE_FACTURATION=(obj.COMPTE_FACTURATION) ? obj.COMPTE_FACTURATION : ResourceManager.getInstance().getString('M111', 'n_c_');
					objElementsSelectiones.SOUS_COMPTE=(obj.SOUS_COMPTE) ? obj.SOUS_COMPTE : ResourceManager.getInstance().getString('M111', 'n_c_');
					listeElementsSelectiones.addItem(objElementsSelectiones);
				}
				this.changementCompteFacturation(operateurName, new Number(typeOperateur.getItemAt(0)), new Number(typeDistributeur.getItemAt(0)), SpecificationVO.getInstance().idPool, listeElementsSelectiones);
			}
			else
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Aucun_compte_de_facturation_n_existe_pour_cette_ligne'), ResourceManager.getInstance().getString('M111', 'Attention____'));
			
		}

		public function clickAction2Handler(evt:MouseEvent):void
		{
			if (SpecificationVO.getInstance().idPoolNotValid is Number && SpecificationVO.getInstance().idPoolNotValid > 0)
			{
				if (cb_choix.selectedIndex > -1)
				{
					switch (cb_choix.selectedItem.CODE_ACTION)
					{
						case "AJOUT":
							clickAddAjoutUnitaireHandler(evt);
							break;
						case "COMMANDE":
							clickNewCommandeHandler(evt);
							break;
						case "POOL":
							clickAddElementPoolHandler(evt);
							break;
						case "MASSE":
							clickAddImportMasseHandler(evt);
							break;
					}
				}
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_pool_de_gestion_avant_'));
		}

		private function exportCSVLignes():void
		{
			if (tempListeSelected.length > 0)
			{
				/** ; pour le csv
				 false : pour exporter le datagrid si true : on exporte que l'element selectionné
				 */
				var dataToExport:String=DataGridDataExporter.getCSVStringFromList(dgMain, tempListeSelected,';', '\n', true);
				var url:String=moduleGestionParcIHM.NonSecureUrlBackoffice + '/fr/consotel/consoview/M111/export/csv/exportCSVString.cfm';
				DataGridDataExporter.exportCSVString(url, dataToExport, 'export');
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'pas_de_donnees_dans_le_datagrid___export'));
		}

		private function exportAllCSVLignes():void
		{
			if (dgMain.dataProvider.length > 0)
				SpecificationVO.getInstance().ViewObject.exportAll()
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'pas_de_donnees_dans_le_datagrid___export'));
		}

		/**
		 *	@param in: Array, Array --> Exclure du pool les éléments selon leur type (collaborateur, terminal, ligne, sim)
		 **/
		private function excludeItemFromPool(tab:Array, type:Array):void
		{
			var panelExclusionPool:PanelExclusionPoolIHM=new PanelExclusionPoolIHM();
			panelExclusionPool.liste_element=buildListeSelected();
			addPopup(panelExclusionPool);
			panelExclusionPool.addEventListener(PanelExclusionInclusionUtils.OPERATION_POOL_DONE, getData);
		}

		private function buildListeSelected():ArrayCollection
		{
			var arr:ArrayCollection=new ArrayCollection();
			for each (var obj:AbstractMatriceParcVO in dgMain.dataProvider)
			{
				if (obj.SELECTED)
					arr.addItem(obj);
			}
			return arr
		}

		private function changementCompteFacturation(operateurName:String, idOperateur:Number, idRevendeur:Number, idPool:Number, listeElementsSelectiones:ArrayCollection):void
		{
			var panelChangementCompteFacturation:ChangementCompteFacturationIHM=new ChangementCompteFacturationIHM();
			panelChangementCompteFacturation.operateurName=operateurName;
			panelChangementCompteFacturation.idOperateur=idOperateur;
			panelChangementCompteFacturation.idRevendeur=idRevendeur;
			panelChangementCompteFacturation.idPool=idPool;
			panelChangementCompteFacturation.listeElementsSelectiones=listeElementsSelectiones;
			addPopup(panelChangementCompteFacturation);
		}

		/**
		 * Afficher une popup
		 * **/
		private function addPopup(popupToAdd:TitleWindow):void
		{
			PopUpManager.addPopUp(popupToAdd, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(popupToAdd);
		}

		public function headerClickHandler(data:String):void
		{
			_positionHorizontalScroll=dgMain.horizontalScrollPosition; /** garde la position de scroll bar horizontal*/
			

			if(SpecificationVO.getInstance().labelSort != data){
				SpecificationVO.getInstance().labelSort=data;
				SpecificationVO.getInstance().orderByReq='ASC';
			}else{
				var current_sort:String = SpecificationVO.getInstance().orderByReq;
				SpecificationVO.getInstance().orderByReq=(current_sort=='ASC')?'DESC':'ASC';
			}
	
			_drawColumn=false;
			initService();

			if (paginationComp.getCurrentIntervall())
				_serviceLocator.service.getData(paginationComp.getCurrentIntervall().indexDepart + 1, nbItemParPage);
		}
		
		[Bindable]
		public function get tempListeSelected():ArrayCollection { return _tempListeSelected; }
		
		public function set tempListeSelected(value:ArrayCollection):void
		{
			if (_tempListeSelected == value)
				return;
			_tempListeSelected = value;
		}
	}
}
