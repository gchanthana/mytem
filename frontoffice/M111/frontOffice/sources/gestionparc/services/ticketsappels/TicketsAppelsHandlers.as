package gestionparc.services.ticketsappels
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	public class TicketsAppelsHandlers
	{
		public var datas:TicketsAppelsDatas = new TicketsAppelsDatas();
		
		public function TicketsAppelsHandlers(d:TicketsAppelsDatas)
		{
			datas = d;
		}
		
		public function setCurrencySymbol(e:ResultEvent):void
		{
			datas.currencySymbol = e.result.toString();
		}
		
		internal function getFicheTicketResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				datas.updateFicheTicket(event.result as ArrayCollection);
			}
		}
		
		internal function getListeFacturesHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				datas.updateListeFactures(event.result as ArrayCollection);
			}
		}
	}
}