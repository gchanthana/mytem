<cfcomponent author="Cedric" displayName="fr.saaswedo.api.mdm.impl.zenprise.MdmService" extends="fr.saaswedo.api.mdm.impl.AbstractService"
hint="Implémentation de l'API MDM Zenprise utilisant fr.saaswedo.api.remoting.soap.WSClient pour accéder au serveur MDM.
<br>Une instance de ce composant ne référence que les services d'un seul serveur MDM et utilise les mêmes crédentiels pour y accéder">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.zenprise.MdmService">
		<cfreturn "fr.saaswedo.api.mdm.impl.zenprise.MdmService">
	</cffunction>
	
	<!--- =========== Fonctions de gestion des services =========== --->
	
	<!---
		
	<cffunction access="public" name="init" returntype="fr.saaswedo.api.mdm.impl.zenprise.MdmService" description="LOCK:Exclusive">
		<cfargument name="mdmServer" type="fr.saaswedo.api.mdm.impl.zenprise.Zenprise" required="true" hint="Serveur Zenprise">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset SUPER.init(argumentCollection=ARGUMENTS)>
			<!--- API Remoting : SOAP
			<cfset var soapClient=new fr.saaswedo.api.remoting.soap.WSClient()>
			<cfset setRemoteClient(soapClient)>
			--->
			<!--- Instanciation des services qui seront utilisés
			<cfset registerServices(getUsername(),getMdmServer().getPassword())>
			--->
			<cfreturn THIS>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="registerServices" returntype="void" description="LOCK:Exclusive" hint="Instancie les services utilisés
	par ce composant. Cette implémentation crée les services suivants :<br>- deviceService : Service d'accès aux infos des devices">
		<cfargument name="username" type="String" required="false" default="" hint="Login de l'API">
		<cfargument name="password" type="String" required="false" default="" hint="Password de l'API">
		<cflock name="#LOCKID()#" timeout="#TIMEOUT()#" type="exclusive" throwontimeout="true">
			<cfset var services=getServices()>
			<!--- API Remoting : SOAP --->
			<cfset var soapClient=getRemoteClient()>
			<!--- Enregistrement du service d'accès aux infos des devices --->
			<cfset var serviceId=soapClient.register(getEndpoint() & "/services/EveryWanDevice?WSDL",ARGUMENTS.username,ARGUMENTS.password)>
			<cfset services.deviceService={id=serviceId}>
		</cflock>
	</cffunction>
	--->
	
	<cffunction access="private" name="getDeviceService" returntype="Struct"
	hint="Retourne les propriétés du service qui seront fournies à la librairie de Remoting (SOAP)">
		<cftry>
			<cfset var mdmServer=getMdmServer()>
			<cfset var infos=mdmServer.getMdmServerInfo()>
			<cfset var service={
				endpoint=infos.getEndpoint() & "/services/EveryWanDevice?WSDL",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfreturn service>
			<cfcatch type="Any">
				<cfset storeError({UUID=createUUID(),EXCEPTION=CFCATCH})>
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction access="private" name="defaultWaitTime" returntype="Numeric" hint="Utilisée comme paramètre de délai d'attente par défaut Zenprise">
		<cfreturn 10000>
	</cffunction>
</cfcomponent>