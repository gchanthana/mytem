<cfcomponent  displayname="Enregistrement" output="false">

	<!--- ENREGISTRE UNE COMMANDE --->
	
	<cffunction name="enregistrerCommande" access="public" returntype="remote" output="false" hint="ENREGISTRE UNE COMMANDE">
		<cfargument name="idGestionnaire" 		required="true" type="numeric"/>
		<cfargument name="idPoolGestionnaire" 	required="true" type="numeric" 	default="" 		displayname="date dateDebut"/>
		<cfargument name="idCompte" 			required="true" type="numeric" 	default="" 		displayname="date dateDebut"/>
		<cfargument name="idSousCompte" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut"/>
		<cfargument name="idOperateur" 			required="true" type="numeric" 	default="NULL" 	displayname="date dateFin"/>
		<cfargument name="idRevendeur" 			required="true" type="numeric" 	default="" 		displayname="string clef"/>
		<cfargument name="idContactRevendeur" 	required="true" type="numeric" 	default="0" 	displayname="numeric segment"/>
		<cfargument name="idTransporteur" 		required="true" type="numeric" 	default="" 		displayname="numeric segment"/>
		<cfargument name="idSiteDeLivraison" 	required="true" type="numeric" 	default="NULL" 	displayname="date dateDebut"/>
		<cfargument name="numeroTracking" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut"/>
		<cfargument name="libelleCommande"		required="true" type="string" 	default="" 		displayname="date dateFin"/>
		<cfargument name="refClient" 			required="true" type="string" 	default="" 		displayname="string clef"/>
		<cfargument name="refClient2" 			required="true" type="string" 	default="" 		displayname="string clef"/>
		<cfargument name="refRevendeur" 		required="true" type="string" 	default="" 		displayname="numeric segment"/>
		<cfargument name="dateOperation"		required="true" type="string" 	default="" 		displayname="date dateFin"/>
		<cfargument name="dateLivraison"		required="true" type="string" 	default="" 		displayname="date dateFin"/>
		<cfargument name="commentaires" 		required="true" type="string" 	default="NULL" 	displayname="date dateDebut"/>
		<cfargument name="boolDevis" 			required="true" type="string" 	default="NULL" 	displayname="date dateDebut"/>
		<cfargument name="montant" 				required="true" type="numeric" 	default="" 		displayname="date dateFin"/>
		<cfargument name="segmentFixe" 			required="true" type="numeric" 	default="" 		displayname="string clef"/>
		<cfargument name="segmentMobile" 		required="true" type="numeric" 	default="" 		displayname="numeric segment"/>
		<cfargument name="idTypeCommande" 		required="true" type="numeric" 	default="" 		displayname="numeric segment"/>
		<cfargument name="idInventaireOpe" 		required="true" type="numeric" 	default="" 		displayname="date dateDebut"/>
		<cfargument name="idGroupeRepere" 		required="true" type="numeric" 	default="NULL" 	displayname="date dateFin"/>
		<cfargument name="numeroCommande" 		required="true" type="string" 	default="NULL" 	displayname="date dateFin"/>
		<cfargument name="idProfileEquipement"	required="true" type="numeric" 	default="" 		displayname="date dateFin"/>
		<cfargument name="idSiteFacturation"	required="true" type="numeric" 	default="NULL"	displayname="date dateFin"/>
		<cfargument name="libelleAttention"	    required="true" type="string" 	default="NULL"	displayname="date dateFin"/>
		<cfargument name="articlesInArray" 		required="true" type="Any" 		default="" 		displayname="string clef"/>
		<cfargument name="V1" 					required="true" type="string" 	default="" 		displayname="string clef"/>
		<cfargument name="V2" 					required="true" type="string" 	default="" 		displayname="string clef"/>
		<cfargument name="V3" 					required="true" type="string" 	default="" 		displayname="string clef"/>
		<cfargument name="V4" 					required="true" type="string" 	default="" 		displayname="string clef"/>
		<cfargument name="V5" 					required="true" type="string" 	default="" 		displayname="string clef"/>
				
		<cfset IDRACINE = Session.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.enregistrerOperation_v3">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#IDRACINE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">	
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#" null="#iif((idGroupeRepere eq 0), de("yes"), de("no"))#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="P_idsite_facturation"  value="#idSiteFacturation#">
 			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false"  variable="p_v1"					value="#V1#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false"  variable="p_v2"				 	value="#V2#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false"  variable="p_v3"				 	value="#V3#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false"  variable="p_v4"				 	value="#V4#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false"  variable="p_v5"				 	value="#V5#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="#libelleAttention#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client_2"   		value="#refClient2#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
		</cfstoredproc>

			<cfif p_idoperation GT 0>
				<cfset rslt = enregistrerArticles(p_idoperation, idPoolGestionnaire, articlesInArray)>
				<cfif ArrayLen(rslt) GT 0>
					<cfset attachements = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>
					<cfset attach = attachements.queryAppendAttachements(p_idoperation, filesInArray)>
				<cfelse>
					<cfset p_idoperation = 0>
				</cfif>
			</cfif>
				
		<cfreturn p_idoperation>
	</cffunction>

	<!--- ENREGISTRE DES COMMANDES - PRINCIPALEMENT POUR LA RESILIATION... --->
	
	<cffunction name="enregistrerCommandes" access="remote" returntype="Array" output="false" hint="ENREGISTRE DES COMMANDES">
		<cfargument name="commandes" 		required="true" type="Array"/>

			<cfset var idsCommande = ArrayNew(1)>
 			<cfset var idRacine = Session.PERIMETRE.ID_GROUPE>
			<cfset var index = 1>
			<cfset var checkOK = 1>

 			<cfloop index="idx" from="1" to="#ArrayLen(commandes)#">
				<cfif checkOK GTE 1>
					<cfset idGestionnaire = commandes[idx].cmd.IDGESTIONNAIRE>
					<cfset idPoolGestionnaire = commandes[idx].cmd.IDPOOL_GESTIONNAIRE>
					<cfset idCompte = commandes[idx].cmd.IDCOMPTE_FACTURATION>
					<cfset idSousCompte = commandes[idx].cmd.IDSOUS_COMPTE>
					<cfset idOperateur = commandes[idx].cmd.IDOPERATEUR>
					<cfset idRevendeur = commandes[idx].cmd.IDREVENDEUR>
					<cfset idContactRevendeur = commandes[idx].cmd.IDCONTACT>
					<cfset idTransporteur = commandes[idx].cmd.IDTRANSPORTEUR>
					<cfset idSiteDeLivraison = commandes[idx].cmd.IDSITELIVRAISON>
					<cfset numeroTracking = commandes[idx].cmd.NUMERO_TRACKING>
					<cfset libelleCommande = commandes[idx].cmd.LIBELLE_COMMANDE>
					<cfset refClient = commandes[idx].cmd.REF_CLIENT1>
					<cfset refClient2 = commandes[idx].cmd.REF_CLIENT2>
					<cfset refRevendeur = commandes[idx].cmd.REF_OPERATEUR>
					<cfset dateOperation = commandes[idx].cmd.DATE_HEURE>
					<cfset dateLivraison = DateFormat(commandes[idx].cmd.LIVRAISON_PREVUE_LE,'YYYY-MM-DD')>
					<cfset commentaires = commandes[idx].cmd.COMMENTAIRES>
					<cfset boolDevis = commandes[idx].cmd.BOOL_DEVIS>
					<cfset montant = commandes[idx].cmd.MONTANT>
					<cfset segmentFixe = commandes[idx].cmd.SEGMENT_FIXE>
					<cfset segmentMobile = commandes[idx].cmd.SEGMENT_MOBILE>
					<cfset idTypeCommande = commandes[idx].cmd.IDTYPE_COMMANDE>
					<cfset idGroupeRepere = commandes[idx].cmd.IDGROUPE_REPERE>
					<cfset numeroCommande = commandes[idx].cmd.NUMERO_COMMANDE>
					<cfset idProfileEquipement = commandes[idx].cmd.IDPROFIL_EQUIPEMENT>
					
					<cfset libelleAttention = commandes[idx].cmd.LIBELLE_TO>
					<cfset idSiteFacturation = commandes[idx].cmd.IDSITEFACTURATION>
					<cfset V1 = commandes[idx].cmd.V1>
					<cfset V2 = commandes[idx].cmd.V2>
					<cfset V3 = commandes[idx].cmd.V3>
					<cfset V4 = commandes[idx].cmd.V4>
					<cfset V5 = commandes[idx].cmd.V5>

					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.enregistrerOperation_v3">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idracine" 			value="#idRacine#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idgestionnaire" 		value="#idGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idpoolgestionnaire" 	value="#idPoolGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcompte" 			value="#idCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsous_compte" 		value="#idSousCompte#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperateur" 		value="#idOperateur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idrevendeur" 		value="#idRevendeur#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idcontact_revendeur" value="#idContactRevendeur#" 	null="#iif((idContactRevendeur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idtransporteur" 		value="#idTransporteur#" 		null="#iif((idTransporteur eq 0), de("yes"), de("no"))#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_livraison" 	value="#idSiteDeLivraison#">	
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_tracking" 	value="#numeroTracking#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_operation" 	value="#libelleCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client" 			value="#refClient#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_revendeur" 		value="#refRevendeur#">			
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_date_operation" 		value="#dateOperation#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 			   variable="p_date_effet_prevue" 	value="#dateLivraison#" >			
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_commentaires" 		value="#commentaires#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_bool_devis" 			value="#boolDevis#">
						<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT"   null="false" variable="p_montant" 			value="#montant#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_fixe" 		value="#segmentFixe#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_segment_mobile" 		value="#segmentMobile#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_idinv_type_ope" 		value="#idTypeCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idgroupe_repere" 	value="#idGroupeRepere#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_numero_commande" 	value="#numeroCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" variable="p_profil_commande" 	value="#idProfileEquipement#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idsite_facturation"	value="#idSiteFacturation#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V1" 					value="#V1#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V2" 					value="#V2#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V3" 					value="#V3#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V4" 					value="#V4#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_V5" 					value="#V5#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_libelle_attention"   value="#libelleAttention#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" variable="p_ref_client_2"   		value="#refClient2#">
						<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 			   variable="p_idoperation">
					</cfstoredproc>
					
					<cfif p_idoperation GT 0>
						<cfset rslt = enregistrerArticles(p_idoperation, idPoolGestionnaire, commandes[idx].articles)>
						<cfif ArrayLen(rslt) GT 0>
							<cfset copyAttch = copyAttachement(p_idoperation, uuidRef, files)>
							<cfset checkOK =  p_idoperation>
							<cfset ArrayAppend(idsCommande, checkOK)>
						<cfelse>
							<cfset checkOK =  -1>
							<cfset ArrayAppend(idsCommande, checkOK)>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
			
		<cfreturn idsCommande>  
	</cffunction>

	<!--- COPIE LA LISTE DES PIECES JOINTES DANS DE NOUVEAUX REPERTOIRE SUIVANT LE NOMBRE DE COMMADNE (RESILIATION, REENGAGEMENT, ETC) --->
	
	<cffunction name="copyAttachement" access="remote" output="false" returntype="any" hint="COPIE LA LISTE DES PIECES JOINTES DANS DE NOUVEAUX REPERTOIRE SUIVANT LE NOMBRE DE COMMADNE (RESILIATION, REENGAGEMENT, ETC)">
		<cfargument name="idcommande" 	required="true" type="Numeric"/>
		<cfargument name="dirSource" 	required="true" type="String"/>
		<cfargument name="files"	 	required="true" type="Array"/>

			<cfset rsltCopy = -10>

			<cfset co = createObject('component',"fr.consotel.consoview.M16.v2.AttachementService")>
			<cfset dirCible = co.initUUID()>	
			<cfset rsltCopy = co.copyAttachement(dirSource, dirCible)>
			
			<cfif rsltCopy EQ 1>
				<cfset rsltCopy = co.queryAppendAttachements(idcommande, files, dirCible)>
			</cfif>			

		<cfreturn rsltcopy> 
	</cffunction>

	<!--- MAJ DES COMMANDES --->
	
	<cffunction name="majCommande" access="remote" returntype="numeric" output="false" hint="MAJ DES COMMANDES">
		<cfargument name="idoperation" 			required="true" type="numeric"/>
		<cfargument name="idgestionnaire" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idtransporteur" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedelivraison" 	required="true" type="numeric" 	default="" 		/>
		<cfargument name="idcontactrevendeur" 	required="true" type="numeric" 	default="null" 	/>
		<cfargument name="numerooperation" 		required="true" type="string" 	default="" 		/>
		<cfargument name="numerotracking" 		required="true" type="string" 	default="null" 	/>
		<cfargument name="libelleoperation" 	required="true" type="string" 	default="null" 	/>
		<cfargument name="refclient"			required="true" type="string" 	default="" 		/>
		<cfargument name="refclient2" 			required="true" type="string" 	default="" 		/>
		<cfargument name="refrevendeur" 		required="true" type="string" 	default="" 		/>
		<cfargument name="datelivraison" 		required="true" type="string" 	default="" 		/>
		<cfargument name="commentaires"			required="true" type="string" 	default="" 		/>
		<cfargument name="montant"				required="true" type="string" 	default="" 		/>
		<cfargument name="libelleto" 			required="true" type="string" 	default="null" 	/>
		<cfargument name="idcompte" 			required="true" type="numeric" 	default="null" 	/>
		<cfargument name="idsouscompte" 		required="true" type="numeric" 	default="" 		/>
		<cfargument name="idsitedefacturation"	required="true" type="numeric" 	default="" 		/>
		<cfargument name="articlesinarray" 		required="true" type="any"  	default=""		/>
		
 			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.majCommande_v3">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idoperation" 			value="#idoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idgestionnaire" 		value="#idgestionnaire#">			
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idtransporteur" 		value="#idtransporteur#" 		null="#iif((idtransporteur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idsite_livraison" 		value="#idsitedelivraison#" 	null="#iif((idsitedelivraison eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 				variable="p_idcontact_revendeur" 	value="#idcontactrevendeur#" 	null="#iif((idcontactrevendeur eq 0), de("yes"), de("no"))#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_operation" 		value="#numerooperation#">		
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_numero_tracking" 		value="#numerotracking#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_operation" 		value="#libelleoperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_client" 			value="#refclient#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_ref_revendeur" 			value="#refrevendeur#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" 				variable="p_date_effet_prevue" 		value="#datelivraison#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_commentaires" 			value="#commentaires#">
				<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" null="false" 	variable="p_montant" 				value="#montant#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="false" 	variable="p_libelle_attention"		value="#libelleto#">
				<cfprocparam type="In" cfsqltype="cf_SQL_VARCHAR" null="false" 	variable="p_ref_client_2"			value="#refclient2#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idcompte"				value="#idcompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" null="false" 	variable="p_idsous_compte"			value="#idsouscompte#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 		 		variable="p_idsite_facturation"		value="#idsitedefacturation#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" 				variable="p_retour">
			</cfstoredproc>
		
<!--- 			<cfset rslt = ArrayNew(1)>
			<cfif p_retour GT 0>
				<cfset rsltErase = earaseAticles(idoperation)>
				<cfif rsltErase GT 1>
					<cfset rslt = enregitrerAticles(idoperation, idpoolgestionnaire, articlesInArray)>
					<cfif ArrayLen(rslt) GT 0>
					<cfelse>
						<cfset ArrayAppend(rslt, 0)>
					</cfif>
				<cfelse>
					<cfset ArrayAppend(rslt, 0)>
				</cfif>
			</cfif> --->
		
		<cfreturn p_retour> 
	</cffunction>

	<!--- ENREGISTRE DES ARTILES --->
	
	<cffunction name="enregistrerArticles" access="remote" returntype="Array" output="false" hint="ENREGISTRE DES ARTICLES">
		<cfargument name="idcommande" 			required="true" type="Numeric"/>
		<cfargument name="idpoolgestionnaire" 	required="true" type="Numeric"/>
		<cfargument name="articles" 			required="true" type="Array"/>
				
			<cfset var rsltErase = earaseAticles(idcommande)>
			
			<cfset var rslt = 1>
			<cfset var idsCommande = ArrayNew(1)>
 			<cfloop index="idx" from="1" to="#ArrayLen(articles)#">
				<cfif rslt GTE 1>
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.enregistrerArticle" blockfactor="1">
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idcommande" 		value="#idcommande#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_CLOB" 	variable="p_articles" 			value="#tostring(articles[idx])#"/>
						<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" 	variable="p_idpoolgestionnaire" value="#idpoolgestionnaire#"/>
						<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
					</cfstoredproc>

					<cfset rslt = p_retour>
					<cfset ArrayAppend(idsCommande, rslt)>
				</cfif>
			</cfloop>
			
		<cfreturn idsCommande>  
	</cffunction>

	<!--- SUPPRIMME LES ARTICLES D'UNE COMMANDE --->
	
	<cffunction name="earaseAticles" access="public" returntype="Numeric" output="false" hint="SUPPRIMME LES ARTICLES D'UNE COMMANDE">
		<cfargument name="idcommande" 			required="true" type="Numeric"/>
		
			<cfset user = Session.USER.CLIENTACCESSID>
			
			<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.delete_articles_op">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idoperation" 	value="#idcommande#"/>
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_user" 			value="#user#"/>
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>  
	</cffunction>

</cfcomponent>