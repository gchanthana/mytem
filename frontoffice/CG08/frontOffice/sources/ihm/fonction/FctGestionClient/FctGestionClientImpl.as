package ihm.fonction.FctGestionClient
{
	import composants.util.ConsoviewAlert;
	
	import entity.ClientVO;
	
	import event.fonction.FctGestionClient.FctGestionClientEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import ihm.fonction.FctGestionClient.popup.PopUpCreateClientIHM;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import paginatedatagrid.PaginateDatagrid;
	import paginatedatagrid.event.PaginateDatagridEvent;
	
	import service.fonction.FctGestionClient.FctGestionClientService;
	
	import utils.abstract.AbstractFonction;
	import utils.composants.export.FlexToExcel;

	public class FctGestionClientImpl extends AbstractFonction
	{
		private var _niveauUtilisateur		:Number;
		private var _iServiceGestionClient	:FctGestionClientService;
		private var _dpListeClients			:ArrayCollection;
		private var _isSWD					:Boolean;
		private var _offset 				:int=1;
		private var _nbreSelectedClient		:int;
		
		public var pdg_clients				:PaginateDatagrid;
		public var dg_clients				:DataGrid;
		
		[Bindable]public var btn_nouveau		:Button;
		[Bindable]public var btn_exporter		:Button;
		[Bindable]public var cbx_allClient		:CheckBox;
		[Bindable]public var ti_filtre			:TextInput;
		
		public function FctGestionClientImpl()
		{
			iServiceGestionClient = new FctGestionClientService();
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, intit);
		}
		
		protected function intit(event:FlexEvent):void
		{
			this.initDispaly();
			this.initAcces();
			this.initListeners();
			this.initData();
		}
		
		private function initDispaly():void
		{
			(pdg_clients.getChildAt(0) as UIComponent).includeInLayout = false;
			pdg_clients.getChildAt(0).visible = false;
			(pdg_clients.getChildAt(1) as UIComponent).includeInLayout = false;
			pdg_clients.getChildAt(1).visible = false;
			
		}
		
		private function initData():void
		{
			this.getListeClients();
		}
		
		private function getListeClients():void
		{
			this.iServiceGestionClient.getListeClients();
		}
		
		private function initListeners():void
		{
			this.iServiceGestionClient.myDatas.addEventListener(FctGestionClientEvent.LISTE_CLIENT, getListeClientHandler);
			
			this.btn_nouveau.addEventListener(MouseEvent.CLICK, clickNouveauClientHandler);
			this.btn_exporter.addEventListener(MouseEvent.CLICK, clickExportClientsHandler);
			this.cbx_allClient.addEventListener(MouseEvent.CLICK, clickAllclientClientHandler);
			
		}
		
		protected function clickExportClientsHandler(event:MouseEvent):void
		{
			try
			{
				if(pdg_clients != null && dpListeClients.length > 0 )
				{
//						FlexToExcel.exportDataGrid(pdg_clients,'Listclients.xls');
				}
				else
				{
					ConsoviewAlert.afficherError("La liste est vide", "Erreur");
				}
				
			} 
			catch(error:Error) 
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('CG08', 'Probl_me_d_export_des_donn_es__'), ResourceManager.getInstance().getString('CG08', 'Erreur'));
			}
			
		}
		
		protected function clickAllclientClientHandler(event:MouseEvent):void
		{
			var ns:int = 0;
			var taille:int = dpListeClients.length
			for(var i:int =0; i < taille; i++ )
			{
				(dpListeClients[i] as ClientVO).SELECTED = cbx_allClient.selected;
				if((dpListeClients[i] as ClientVO).SELECTED)
					ns ++;
			}
			
			this.nbreSelectedClient = ns;
			
			this.dpListeClients.refresh();
		}
		
		protected function clickNouveauClientHandler(event:MouseEvent):void
		{
			var popup:PopUpCreateClientIHM 	= new PopUpCreateClientIHM();
			PopUpManager.addPopUp(popup, this, true);
			PopUpManager.centerPopUp(popup);
			PopUpManager.bringToFront(popup);
			
		}
		
		protected function getListeClientHandler(event:Event):void
		{
			this.dpListeClients = iServiceGestionClient.myDatas.listeClients;
			pdg_clients.nbItemParPage = 10;
			pdg_clients.nbTotalItem = dpListeClients.length;
		}
		
		private function initAcces():void
		{
			_niveauUtilisateur = Number(CvAccessManager.getSession().INFOS_DIVERS.CG.USER.IDNIVEAU);
//			_niveauUtilisateur = 2;
			switch(_niveauUtilisateur)
			{
				case 1:
				{
					// Vue Saaswedo (SWD)
					isSWD = true;
					break;
				}
					
				case 2:
				{
					// Vue Application Master ou Partenaire (AMP)
					isSWD = false;
					break;
				}
					
				default :
				{
					// Comportement à préciser : Quel message ?
					// un email ? deconnexion ?
					break;
				}
			}
		}
		
		protected function onIntervalleChangeHandler(evt:PaginateDatagridEvent):void
		{
			if(pdg_clients.currentIntervalle!=null)
			{
				if (pdg_clients.currentIntervalle.indexDepart == 0)
				{
					offset=1;
				}
				else
				{		
					offset=pdg_clients.currentIntervalle.indexDepart+1;
				}
			}
		}
		
		protected function onChangeFiltreHandler(event:Event):void
		{
			dpListeClients.filterFunction = onFilterInformation;
			dpListeClients.refresh();
		}
		
		private function onFilterInformation(item:Object):Boolean
		{
			if((String((item as ClientVO).NOM).toLowerCase().search(ti_filtre.text.toLowerCase()) != -1) || 
			   (String((item as ClientVO).master).toLowerCase().search(ti_filtre.text.toLowerCase()) != -1))
				return true;
			else
				return false;
		}
		
		
		public function get iServiceGestionClient():FctGestionClientService { return _iServiceGestionClient; }
		
		public function set iServiceGestionClient(value:FctGestionClientService):void
		{
			if (_iServiceGestionClient == value)
				return;
			_iServiceGestionClient = value;
		}
		
		[Bindable]
		public function get dpListeClients():ArrayCollection { return _dpListeClients; }
		
		public function set dpListeClients(value:ArrayCollection):void
		{
			if (_dpListeClients == value)
				return;
			_dpListeClients = value;
		}
		
		[Bindable]
		public function get isSWD():Boolean { return _isSWD; }
		
		public function set isSWD(value:Boolean):void
		{
			if (_isSWD == value)
				return;
			_isSWD = value;
		}

		public function get offset():int
		{
			return _offset;
		}

		public function set offset(value:int):void
		{
			_offset = value;
		}
		
		[Bindable]
		public function get nbreSelectedClient():int { return _nbreSelectedClient; }
		
		public function set nbreSelectedClient(value:int):void
		{
			if (_nbreSelectedClient == value)
				return;
			_nbreSelectedClient = value;
		}

		
	}
}