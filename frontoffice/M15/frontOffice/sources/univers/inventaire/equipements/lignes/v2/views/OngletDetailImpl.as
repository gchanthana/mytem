package univers.inventaire.equipements.lignes.v2.views
{
 	
	import composants.util.TextInputLabeled;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Form;
	import mx.containers.FormItem;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.List;
	import mx.controls.RadioButton;
	import mx.controls.TextArea;
	import mx.events.ListEvent;
	
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import univers.inventaire.equipements.lignes.v2.util.ShowEditWindow;
	import univers.inventaire.equipements.lignes.v2.vo.Ligne;
	import univers.inventaire.equipements.lignes.v2.vo.OngletGeneralVo;
	

	
	 
 	
	
	
	
	[Bindable]
	public class OngletDetailImpl extends BaseView
	{
		public var LIBELLE_TYPE_LIGNE : ComboBox;
		public var SOUS_TETE : TextInputLabeled;
		
		
		//Groupement de lignes
		public var TETE_GROUPEMENT : DataGrid;
		public var TETES_GROUPEMENT_LIST : List;
		public var TETE_GROUPEMENT_TXT : TextInputLabeled;		
		public var imgSetTete : Image;		
		public var imgCleanTete : Image;		
		public var imgAddLigneGpt : Image;		
		public var imgRemoveLigneGpt : Image;
		public var bxAutreLignes : HBox;
		
		
		public var NBR_LIGNE : TextInputLabeled;
		public var IDENTIFIANT_COMPLEMENTAIRE : TextInputLabeled;
		public var CODE_INTERNE : TextInputLabeled;
		public var LIBELLE_RACCORDEMENT : ComboBox;
		
		
		/* protected var tmpTeteLigne : Ligne; */
		
		 
		/* private var _tmpListeGroupementLignes : ArrayCollection = new ArrayCollection();
		public function get tmpListeGroupementLignes():ArrayCollection{
			return _tmpListeGroupementLignes;
		}
		public function set tmpListeGroupementLignes(col : ArrayCollection):void{
			_tmpListeGroupementLignes = col;
		} */
		
		public var BOOL_PRINCIPAL : RadioButton;
		public var BOOL_BACKUP : RadioButton;
		
		public var COMMENTAIRES_GENERAL : TextArea;
		public var USAGE_LIGNE : TextInputLabeled;
		public var USAGES_LIGNES : ComboBox;
		
		public var POPULATION : TextInputLabeled;	
		public var lblUsage : FormItem;
		  
		public var leftForm : Form;
		
		
		public var txtFiltreLigne : TextInputLabeled;
		
		public var libelle1 : FormItem;
		public var libelle2 : FormItem;	
		
		public var V1 : TextInputLabeled;
		public var V2 : TextInputLabeled;
		
		public var btChampPerso1 : Button;
		public var btChampPerso2 : Button;
		public var btUpdate : Button;
		//Test
		public var btReset : Button;
		
		protected var donnees : OngletGeneralVo;
				
		
		public function OngletDetailImpl()
		{
			//TODO: implement function
			super();
		}
		
		override public function getData():void{						
			super.getData();
			txtFiltreLigne.text = "";
		}
		
		
		
		
				
		//======================== HANDLERS ==========================================		
		protected function btUpdateClickHandler(me : MouseEvent):void{
			enregistrer();
		}
		
		
		protected function LIBELLE_TYPE_LIGNEUpdateHandler(fe : Event):void{
			if (LIBELLE_TYPE_LIGNE.initialized)
				if (fe.currentTarget.selectedIndex != -1){
					lblUsage.label = "Usage";					
				}
		}
		
		protected function LIBELLE_TYPE_LIGNEChangeHandler(le : ListEvent):void{
			_hasChanged = true
			if (le.currentTarget.selectedIndex != -1){				
				lblUsage.label = "Usage";
			}else{
				le.preventDefault();
			}
		}
		
		protected function LIBELLE_RACCORDEMENTChangeHandler(le : ListEvent):void{
			if (le.currentTarget.selectedIndex != -1){				
			}else{
				le.preventDefault();
			}			
		}
		
		protected function btChampPerso1ClickHandler(me : MouseEvent):void{						
			var showWindow : ShowEditWindow = new ShowEditWindow(gestionTechnique,libelle1);
			showWindow.showEditWindow();
		}
		
		
		protected function btChampPerso2ClickHandler(me : MouseEvent):void{
			var showWindow : ShowEditWindow = new ShowEditWindow(gestionTechnique,libelle2);
			showWindow.showEditWindow();
		}
		
		protected function btResetClickHandler(me : MouseEvent):void{			
			executeBindings(true);
		}
		
		
		protected function txtFiltreLigneChangeHandler(ev : Event):void{
			if(gestionTechnique.listeTetesLigne != null){
				gestionTechnique.listeTetesLigne.filterFunction = filtrerListeTetes;
				gestionTechnique.listeTetesLigne.refresh();
			}
		}
		
		protected function imgSetTeteClickHandler (me : MouseEvent):void{
			if (TETE_GROUPEMENT.selectedItems.length > 1){
				Alert.show("Vous ne devez sélectionner qu'une seule ligne","Info");
			}else if (TETE_GROUPEMENT.selectedItems.length == 1){
				var teteligne : Ligne = new Ligne();
				teteligne.IDSOUS_TETE =  TETE_GROUPEMENT.selectedItem.IDTETE_LIGNE;
				teteligne.SOUS_TETE = TETE_GROUPEMENT.selectedItem.TETE;								
				gestionTechnique.setTeteLigne(gestionTechnique.ligne,teteligne);
			}else{
				Alert.show("Vous devez sélectionner des lignes","Info");
			}
		}
		
		
		protected function imgCleanTeteClickHandler (me : MouseEvent):void{
			if (gestionTechnique.ligne.IDTETE_LIGNE>0){
				gestionTechnique.setTeteLigne(gestionTechnique.ligne,new Ligne());
			}else{
				trace("pas de tete");
			}
			
		}
		
		
		protected function imgAddLigneGptClickHandler (me : MouseEvent):void{			
			if (gestionTechnique.ligne.IDTETE_LIGNE > 0){
				if(TETE_GROUPEMENT.selectedIndex != -1){					
					
					var ligne : Ligne = new Ligne();
					ligne.IDSOUS_TETE =  TETE_GROUPEMENT.selectedItem.IDTETE_LIGNE;
					ligne.SOUS_TETE = TETE_GROUPEMENT.selectedItem.TETE;
					
					var tete : Ligne = new Ligne();
					tete.IDSOUS_TETE =  gestionTechnique.ligne.IDTETE_LIGNE;
					tete.SOUS_TETE = gestionTechnique.ligne.TETE_LIGNE;
					
					gestionTechnique.setTeteLigne(ligne,tete);
				}else{
					Alert.show("Vous devez sélectionner une ligne","Info");
				}
			}else{
				Alert.show("Vous devez indiquer la tête de groupement avant d'ajouter des lignes au groupe");
			} 
		}		
		
		protected function imgRemoveLigneGptClickHandler (me : MouseEvent):void{
			if(TETES_GROUPEMENT_LIST.selectedItem !=null ){				
				var ligne : Ligne = new Ligne();
				ligne.IDSOUS_TETE =  TETES_GROUPEMENT_LIST.selectedItem.IDSOUS_TETE;
				ligne.SOUS_TETE = TETES_GROUPEMENT_LIST.selectedItem.SOUS_TETE;
				gestionTechnique.setTeteLigne(ligne,new Ligne());
			}else{
				Alert.show("Vous devez sélectionner des lignes","Info");
			} 
		}
		
		protected function listeTeteLigneCompleteHandler(ev : Event):void{
			txtFiltreLigne.text = "";	
			TETE_GROUPEMENT.setFocus();
		}
		//======================== FIN HANDLERS ======================================		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			btUpdate.addEventListener(MouseEvent.CLICK,btUpdateClickHandler);
			imgSetTete.addEventListener(MouseEvent.CLICK,imgSetTeteClickHandler);
			imgCleanTete.addEventListener(MouseEvent.CLICK,imgCleanTeteClickHandler);
			imgAddLigneGpt.addEventListener(MouseEvent.CLICK,imgAddLigneGptClickHandler);
			imgRemoveLigneGpt.addEventListener(MouseEvent.CLICK,imgRemoveLigneGptClickHandler);
			gestionTechnique.addEventListener(GestionTechniqueLignesApp.LISTETETESLIGNE_COMPLETE,listeTeteLigneCompleteHandler);
		}
		
		
		//Enregistre le formulaire
		override protected function enregistrer():void{
			var boolOk : Boolean = true;
			if(LIBELLE_TYPE_LIGNE.selectedItem == null){
				boolOk = false
				LIBELLE_TYPE_LIGNE.setStyle("borderColor","red");				
			}
			if(LIBELLE_RACCORDEMENT.selectedItem == null){
				boolOk = false
				LIBELLE_RACCORDEMENT.setStyle("borderColor","red");				
			}
			if (boolOk){ 
				mapData(donnees = new OngletGeneralVo());
				gestionTechnique.updateInfosLigne(donnees);
				LIBELLE_RACCORDEMENT.clearStyle("borderColor");
				LIBELLE_TYPE_LIGNE.clearStyle("borderColor");
				_hasChanged = false;
			}else{
				Alert.show("Les champs en rouge sont obligatoires","Erreur");
			}
		}
		
		private function mapData(item : OngletGeneralVo):void{
			if (gestionTechnique != null){		
				item.IDSOUS_TETE = gestionTechnique.ligne.IDSOUS_TETE;						
				item.LIBELLE_TYPE_LIGNE = LIBELLE_TYPE_LIGNE.selectedItem.LIBELLE_TYPE_LIGNE;
				item.IDTYPE_LIGNE = LIBELLE_TYPE_LIGNE.selectedItem.IDTYPE_LIGNE;
								
				item.LIBELLE_RACCORDEMENT = (LIBELLE_RACCORDEMENT.selectedItem != null)?LIBELLE_RACCORDEMENT.selectedItem.LIBELLE_RACCORDEMENT:"";
				item.IDTYPE_RACCORDEMENT = (LIBELLE_RACCORDEMENT.selectedItem != null)?LIBELLE_RACCORDEMENT.selectedItem.IDTYPE_RACCORDEMENT:0;	
						
				item.IDENTIFIANT_COMPLEMENTAIRE = IDENTIFIANT_COMPLEMENTAIRE.text;
				item.CODE_INTERNE = CODE_INTERNE.text;
				item.BOOL_PRINCIPAL = (BOOL_PRINCIPAL.selected)?1:0;
			 
				item.COMMENTAIRES_GENERAL = COMMENTAIRES_GENERAL.text;
				item.USAGE = USAGE_LIGNE.text;
				item.POPULATION = POPULATION.text;
				item.V1 = V1.text;
				item.V2 = V2.text;	
			}else{
				throw (new IllegalOperationError("Pas de controlleur pour l'application"))	
			}			
		}
		
		private function filtrerListeTetes(item :Object):Boolean{
			if (String(item.TETE).toLowerCase().search(txtFiltreLigne.text) != -1) return true
			else return false;
		}
	}
}