<cfcomponent author="Cedric" displayName="fr.consotel.api.ibis.publisher.handler.event.DelegateEvent" extends="fr.consotel.api.ibis.publisher.handler.event.DeliveryEvent"
hint="Extension de DeliveryEvent permettant de séparer les infos liées au contenu du reporting de celles liées à son traitement (e.g : Diffusion)
Les infos liées au contenu du reporting sont encapsulées dans un évènement de type IDeliveryEvent qui est passé au constructeur
Les infos liées à son traitement sont récupérées à partir de la méthode getReportingEvent().getDelivery() et des infos précédentes
Cette implémentation redirige les méthodes IDeliveryEvent vers l'instance IDeliveryEvent qui est passée à son constructeur
Les méthodes suivantes retournent la valeur correspondante pour l'évènement de type IReportingEvent :
getStatus(), getReportingName(), getReportingService(), getDeliveryType()">
	<!--- fr.consotel.api.ibis.publisher.handler.event.DelegateEvent --->
	<cffunction access="public" name="createDeliveryEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent" hint="Appelle le constructeur de la classe parent">
		<cfargument name="eventSource" type="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"  required="true"
		hint="Implémentation de l'évènement à partir duquel les infos et le contenu du reporting sont extraites">
		<cfset var deliveryEvent=ARGUMENTS["eventSource"]>
		<cfset SUPER.createDeliveryEvent(deliveryEvent)>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="getDeliveryEvent" returntype="fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent"
	hint="Retourne l'évènement source IDeliveryEvent qui à partir duquel les infos retournées sont récupérées et qui a été fourni à l'instanciation">
		<cfreturn SUPER.getEventSource()>
	</cffunction>
	
	<!--- fr.consotel.api.ibis.publisher.handler.event.IDeliveryEvent --->
	<cffunction access="public" name="getContent" returntype="any"
	hint="Retourne la valeur retournée par la méthode getContent() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfargument name="localFileName" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfargument name="localDirAbsPath" type="String"  required="true" hint="La valeur de ce paramètre n'est pas utilisée par cette implémentation">
		<cfreturn getDeliveryEvent().getContent(ARGUMENTS["localFileName"],ARGUMENTS["localDirAbsPath"])>
	</cffunction>

	<cffunction access="public" name="getContentAsLocalFile" returntype="String"
	hint="Retourne la valeur retournée par la méthode getContentAsLocalFile() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfargument name="localFileName" type="String"  required="true">
		<cfargument name="localDirAbsPath" type="String"  required="true">
		<cfreturn getDeliveryEvent().getContentAsLocalFile(ARGUMENTS["localFileName"],ARGUMENTS["localDirAbsPath"])>
	</cffunction>
	
	<cffunction access="public" name="getDeliveryInfos" returntype="any"
	hint="Retourne la valeur retournée par la méthode getDeliveryInfos() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfreturn getDeliveryEvent().getDeliveryInfos()>
	</cffunction>
	
	<cffunction access="public" name="getContentType" returntype="String"
	hint="Retourne la valeur retournée par la méthode getContentType() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfreturn getDeliveryEvent().getContentType()>
	</cffunction>
	
	<cffunction access="public" name="isTextContent" returntype="boolean"
	hint="Retourne la valeur retournée par la méthode isTextContent() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfreturn getDeliveryEvent().isTextContent()>
	</cffunction>
	
	<cffunction access="public" name="isBinaryContent" returntype="boolean"
	hint="Retourne la valeur retournée par la méthode isBinaryContent() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfreturn getDeliveryEvent().isBinaryContent()>
	</cffunction>
	
	<cffunction access="public" name="isUnknownContent" returntype="boolean"
	hint="Retourne la valeur retournée par la méthode isUnknownContent() de l'évènement IDeliveryEvent utilisé pour créer cette instance">
		<cfreturn getDeliveryEvent().isUnknownContent()>
	</cffunction>
</cfcomponent>