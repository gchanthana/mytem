package intranet.gestionLoginClasse.entity
{

    public class StyleVO
    {
        private var _libelle_style:String;
        private var _commentaire:String;
        private var _idconso_style:String;
        private var _code_style:String;

        public function StyleVO()
        {
        }

        public function deserialize(styleObj:Object):StyleVO
        {
            libelle_style = styleObj.LIBELLE_STYLE;
            commentaire = styleObj.COMMENTAIRE;
            idconso_style = styleObj.IDCONSO_STYLE;
            code_style = styleObj.CODE_STYLE;
            return this;
        }

        public function serialize():Object
        {
            var styleObj:Object = new Object();
            styleObj.LIBELLE_STYLE = libelle_style;
            styleObj.COMMENTAIRE = commentaire;
            styleObj.IDCONSO_STYLE = idconso_style;
            styleObj.CODE_STYLE = code_style;
            return styleObj;
        }

        public function toString():String
        {
            return ("Stlye : " + libelle_style + "  id : " + idconso_style + "  code : " + code_style);
        }

        public function get libelle_style():String
        {
            return _libelle_style;
        }

        public function set libelle_style(value:String):void
        {
            _libelle_style = value;
        }

        public function get commentaire():String
        {
            return _commentaire;
        }

        public function set commentaire(value:String):void
        {
            _commentaire = value;
        }

        public function get idconso_style():String
        {
            return _idconso_style;
        }

        public function set idconso_style(value:String):void
        {
            _idconso_style = value;
        }

        public function get code_style():String
        {
            return _code_style;
        }

        public function set code_style(value:String):void
        {
            _code_style = value;
        }
    }
}