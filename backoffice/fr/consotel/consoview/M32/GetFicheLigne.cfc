<cfcomponent displayname="GetFicheLigne" output="false">


<cffunction name="getFicheLigne" access="remote" returntype="array" >
	
	<cfargument name="idSousTete" required="true" type="numeric"/>
	
	<cfset idracine_master = session.perimetre.idracine_master>
	<cfset idgroupe_client = session.perimetre.id_groupe>
	<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION> 
	
	<cfstoredproc datasource="#Session.OFFREDSN#" procedure="pkg_m11.getfiche_ligne">
		<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#idracine_master#" null="false">
		<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idSousTete" value="#idSousTete#" null="false">
		<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" value="#idgroupe_client#" null="false">
		<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idLangue" value="#idLangue#" null="false">
		
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="codeRetour"/>
					
		<cfprocresult name="ligne" 			resultset="1">
		<cfprocresult name="aboOpt"		 	resultset="2">
		<cfprocresult name="orga" 			resultset="3">
	</cfstoredproc>
	
	<cfset p_retour = ArrayNew(1)>
	<cfset p_retour[1] = codeRetour >
	<cfset p_retour[2] = ligne >
	<cfset p_retour[3] = aboOpt >
	<cfset p_retour[4] = orga >
		
	<cfreturn p_retour>
	
</cffunction>

<cffunction name="getLibellePerso" access="public" returntype="query" output="false" >
		
		<cfset idgroupe_client = session.perimetre.id_groupe>
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="pkg_cv_gtligne.Liste_Champs_Perso">
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" variable="p_idgroupe_client" value="#idgroupe_client#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		
		<cfreturn p_result>
	</cffunction>

</cfcomponent>