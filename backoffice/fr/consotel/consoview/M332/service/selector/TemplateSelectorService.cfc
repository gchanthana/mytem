﻿<cfcomponent>
	<cffunction access="public" output="false" name="getValuesTemplate" returntype="query"> 
			<cfargument name="idRapportRacine" type="numeric" required="true">
			<cfset idLang  = SESSION.USER.IDGLOBALIZATION>
			<!--- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
			<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.--->		 
             <cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_TemplateSelector">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRapportRacine#" variable="p_idrapport_racine">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idLang#" variable="p_langid">
				<cfprocresult name="qTemplate">
			</cfstoredproc>		
            <cfreturn qTemplate><!--- le resultat de la procedure --->
      </cffunction>
</cfcomponent>