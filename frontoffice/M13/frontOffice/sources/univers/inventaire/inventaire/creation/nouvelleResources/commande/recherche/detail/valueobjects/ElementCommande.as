package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.valueobjects
{
	import flash.events.EventDispatcher;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.collections.ArrayCollection;
	import flash.events.Event;
	
	public class ElementCommande extends EventDispatcher
	{
		public static const CREATION_COMPLETE : String = "elementOk";
		public static const UPDATE_COMPLETE : String = "elementUpdated";
		public static const SAVE_COMPLETE : String = "elementSaved";
		public static const SAVEALL_COMPLETE : String = "allElementSaved";
		public static const LISTE_COMPLETE : String = "elementListOk";		
		public static const DELETE_COMPLETE : String = "elementDeleteOk";
				
		public static const CREATION_ERROR : String = "elementCreationError";
		public static const UPDATE_ERROR : String ="elementUpdateError";  
		public static const SAVE_ERROR : String = "elementSavedError";
		public static const LISTE_ERROR : String = "elementListError";
		public static const DELETE_ERROR : String = "elementDeleteOk";
		
		public static const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.ElementCommande";
  		public static const COLDFUSION_GET : String = "get";
  		public static const COLDFUSION_UPDATE : String = "update";
  		public static const COLDFUSION_CREATE : String = "create";
		public static const COLDFUSION_GETLISTE : String = "getList";
		public static const COLDFUSION_GETGROUPELISTE : String = "getGroupetList";
		public static const COLDFUSION_DELETE : String = "delete";
		
		private var opGet : AbstractOperation;
		private var opCreate : AbstractOperation;
		private var opUpdate : AbstractOperation;		
		private var opList : AbstractOperation;
		private var opDelete : AbstractOperation;
		
		public function ElementCommande(id : Number = 0){
			if (id != 0){
				_elementID = id;
				getElementCommande(elementID)
			}else{
				_liste = new ArrayCollection();
			}
		}
		
		public function clean():void{			
			if (opGet != null) opGet.cancel();
			if (opCreate != null) opCreate.cancel();							
			if (opUpdate != null) opUpdate.cancel();
			if (opList != null) opList.cancel();
		}
		
		private var _elementID : Number;
		public function set elementID(id : Number):void{
			_elementID = id;
		}
		public function get elementID():Number{
			return _elementID;
		}
				
		private var _commandeID : Number;
		public function set commandeID(id : Number):void{
			_commandeID = id;
		}
		public function get commandeID():Number{
			return _commandeID;
		}
		
		private var _flagRapproche : Number;
		public function set flagRapproche(flag : Number):void{
			_flagRapproche = flag;
		}
		public function get flagRapproche():Number{
			return _flagRapproche;
		}
		
		private var _libelleLigne : String;
		public function set libelleLigne(l : String):void{
			_libelleLigne = l;
		}
		public function get libelleLigne():String{
			return _libelleLigne;
		}
		
		private var _themeID : Number;
		public function set themeID(id : Number):void{
			_themeID = id;
		}
		public function get themeID():Number{
			return _themeID;
		}
		
		private var _libelletheme : String;
		public function set libelletheme(l : String):void{
			_libelletheme = l;
		}
		public function get libelletheme():String{
			return _libelletheme;
		}
		
		private var _ligneID : Number;
		public function set ligneID(id : Number):void{
			_ligneID = id;
		}
		public function get ligneID():Number{
			return _ligneID;
		}
		
		private var _numLigne : String;
		public function set numLigne(l : String):void{
			_numLigne = l;
		}
		public function get numLigne():String{
			return _numLigne;
		}
		
		private var _commenatire : String;
		public function set commenatire(l : String):void{
			_commenatire = l;
		}
		public function get commenatire():String{
			return _commenatire;
		}
		
		
		//---------------------------------------------------------/
		
		private function fill(value : Object):void{
			commandeID = value.IDCDE_CDE_DETAIL; 
			elementID = value.IDCDE_COMMANDE;			
			libelleLigne = value.LIBELLE_LIGNE;
			libelletheme = value.THEME_LIBELLE;
			ligneID = value.IDSOUS_TETE;
			numLigne = value.SOUS_TETE;				
			flagRapproche = value.FLAG;
		}
		
		///---------------- REMOTING ------------------------------/
		private function getElementCommande(id : Number):void{
			opGet = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_GET,
													getElementCommandeResultHandler,null);
			RemoteObjectUtil.callService(opGet,id);
		}
		
		private function getElementCommandeResultHandler(re : ResultEvent):void{
			if (re.result){							
				fill(re.result);				
				dispatchEvent(new Event(ElementCommande.CREATION_COMPLETE));				
			}else{
				dispatchEvent(new Event(ElementCommande.CREATION_ERROR));
			}			
		}
		
		//--------	
		public function deleteElementCommande(id : Number):void{
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_DELETE,
													deleteElementCommandeResultHandler,null);

			RemoteObjectUtil.callService(opDelete,id);

		}
		
		private function deleteElementCommandeResultHandler(re : ResultEvent):void{
			if (re.result > 0){							
				dispatchEvent(new Event(ElementCommande.DELETE_COMPLETE));				
			}else{
				dispatchEvent(new Event(ElementCommande.DELETE_ERROR));
			}			
		}
				
		//--------					
		public function update():void{
			opUpdate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_UPDATE,
													updateResultHandler,null);

			RemoteObjectUtil.callService(opUpdate,this);						
		}	
		
		private function updateResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				dispatchEvent(new Event(ElementCommande.UPDATE_COMPLETE));
			}else{
				dispatchEvent(new Event(ElementCommande.UPDATE_ERROR));
			}
		}
		
		//----------
		public function sauvegarder():void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_CREATE,
													sauvegarderResultHandler,null);

			RemoteObjectUtil.callService(opCreate,this);					 											
		}		
		
		private function sauvegarderResultHandler(re : ResultEvent):void{
			if (re.result > 0){								
				dispatchEvent(new Event(ElementCommande.SAVE_COMPLETE));
			}else{
				dispatchEvent(new Event(ElementCommande.SAVE_ERROR));
			}			
		}
		
		public function sauvegarderAll():void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_CREATE,
													sauvegarderAllResultHandler,null);

			RemoteObjectUtil.callService(opCreate,commandeID,extraireColonne(liste,"libelleLigne")
												,extraireColonne(liste,"themeID")
												,extraireColonne(liste,null));					 											
		}		
		
		private function extraireColonne(liste : ArrayCollection, nomColonne : String):Array{
			var len : Number = liste.length;
			var tab: Array = new Array();
			if (nomColonne != null){
				for (var i : int = 0; i < len ; i++){
					tab.push(liste[i][nomColonne]);
				}	
			}else{
				for (var j : int = 0; j < len ; j++){
					tab.push(0);
				}
			}
			return tab;
		}
		
		private function sauvegarderAllResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				dispatchEvent(new Event(ElementCommande.SAVEALL_COMPLETE));
			}else{
				dispatchEvent(new Event(ElementCommande.SAVE_ERROR));
			}			
		}
		//---------LISTE---------------------------------------------------------------------------
		private var _liste : ArrayCollection;
		private function setliste(values : ArrayCollection):void{
			var ligne : Object;
			_liste = new ArrayCollection();
			for (ligne in values){
				
				var ec : ElementCommande = new ElementCommande();
				ec.commandeID = values[ligne].IDCDE_CDE_DETAIL; 
				ec.elementID = values[ligne].IDCDE_COMMANDE;			
				ec.libelleLigne = values[ligne].LIBELLE_LIGNE;
				ec.libelletheme = values[ligne].THEME_LIBELLE;
				ec.ligneID = values[ligne].IDSOUS_TETE;
				ec.numLigne = values[ligne].SOUS_TETE;				
				ec.flagRapproche = values[ligne].FLAG;				
				_liste.addItem(ec);
				
			}			
		}
		
		public function get liste():ArrayCollection{
			return _liste;
		} 
		public function set liste(a : ArrayCollection):void{
			_liste = a;
		}
		
		
		public function prepareList(idcommande : Number):void{
			if (opList != null) opList.cancel();
			
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_GETLISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idcommande);
		}
		
		public function  prepareGroupeList(idGpe : Number):void{			
			if (opList != null) opList.cancel();			
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													ElementCommande.COLDFUSION_COMPONENT,
													ElementCommande.COLDFUSION_GETGROUPELISTE,
													getListResultHandler,null);
													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		
		private function getListResultHandler(re : ResultEvent):void{
			try{
				setliste(re.result as ArrayCollection);
				dispatchEvent(new Event(ElementCommande.LISTE_COMPLETE));
			}catch(e : Error){
				dispatchEvent(new Event(ElementCommande.LISTE_ERROR));
			}			
		}
	}
}