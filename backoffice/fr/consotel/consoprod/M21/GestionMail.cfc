<cfcomponent displayname="fr.consotel.consoprod.M21.GestionMail" hint="Gestion de l'envoi de login, mot de passe" output="false">

	<!--- 
		[1]->nom
		[2]->prenom
		[3]->email
		[4]->idclientje vois jeok
		
		[5]->racine
		[6]->langue
		[7]->mot de passe (UNIQUEMENT DANS LE CAS D'ENVOI DE MOT DE PASSE)
	 --->

	<cffunction name="send_Login" output="false" description="" access="remote" returntype="Numeric">
		<cfargument name="tableau" 			required="true"  type="Array"/>
		<cfargument name="codeapplication" 	required="false" type="Numeric"/>
			
			<cfset _codeapplication = 1>
			<cfif isDefined("codeapplication")>
				<cfset _codeapplication = codeapplication>
			</cfif>
			
			<cfset SESSION.CODEAPPLICATION  = _codeapplication>
			<cfset SESSION.USER = {GLOBALIZATION = 'fr_FR'}>	
			
			<cfset var _sender = createObject("component","fr.consotel.consoview.M21.GestionMail")/>
			<cfset var rsltBIPReport = _sender.send_Login(tableau)>
		
			
		<cfreturn 1>
	</cffunction>

	<cffunction name="send_Pass" output="false" description="" access="remote" returntype="Numeric">
		<cfargument name="tableau" 			required="true"  type="Array" />
		<cfargument name="codeapplication" 	required="false" type="Numeric"/>
			
			<cfset var _codeapplication = 1>
		
			<cfset _codeapplication = 1>
			<cfif isDefined("codeapplication")>
				<cfset _codeapplication = codeapplication>
			</cfif>
			
			<cfset SESSION.CODEAPPLICATION  = _codeapplication>
			<cfset SESSION.USER = {GLOBALIZATION = 'fr_FR'}>	
			
			<cfset var _sender = createObject("component","fr.consotel.consoview.M21.GestionMail")/>
			<cfset var rsltBIPReport = _sender.send_Pass(tableau)>

		<cfreturn 1>
	</cffunction>
	
	<cffunction name="createBIPReport" output="false" description="" access="remote" returntype="Numeric">
		<cfargument name="tableau" 		required="true" type="Array"/>
		<cfargument name="templateID"  	required="true" type="String"/>
		<cfargument name="prefixeFile" 	required="true" type="String"/>
		<cfargument name="subject" 		required="true" type="String"/>
		<cfargument name="type" 		required="true" type="String"/>
			
			<cfset var mailing  = createObject("component","fr.consotel.consoview.M01.Mailing")>				
			
			<cfset var M21_UUID = ''>
			
			<cfset var myVar = false>
			
			<cfset var ImportTmp="/container/M21/">
			
			<cfset var rsltApi  = -4>
			<cfset var resltRecordMail = 0>
			<cfset var iduser = tableau[4]>
			<cfset var langue = tableau[6]>
			
			<cfset var fromMailing 	= 'no-reply@consotel.fr'>
			<cfset var bccMailing 	= 'support@consotel.fr'>

			<cfloop condition="myVar eq false">
			  <cfset M21_UUID = createUUID()>
			    
			  <cfif NOT DirectoryExists('#ImportTmp##M21_UUID#')>
				<cftry>
					<cfdirectory  action="Create" directory="#ImportTmp##M21_UUID#" type="dir" mode="777">
					<cfset myVar="true">
						<cfmail from="samuel.divioka@consotel.fr" subject="error mail login" to="samuel.divioka@consotel.fr" type="text/html" >							
							<cfdump var="#ImportTmp##M21_UUID#">
						</cfmail>
					<cfcatch type="any">
						<cfmail from="samuel.divioka@saaswedo.com" subject="error mail login" to="samuel.divioka@saaswedo.com" type="text/html" >							
						<cfdump var="#cfcatch#">
						</cfmail>
						
						
					</cfcatch>
					
				</cftry>
					
			  </cfif>
			  
			</cfloop>

			<!--- BIP REPORT --->
			<cfset parameters["bipReport"] = structNew()>
			<cfset parameters["bipReport"]["xdoAbsolutePath"]	= "/consoview/LOGIN/LOGIN.xdo">
			<cfset parameters["bipReport"]["outputFormat"] 		= "html">
			<cfset parameters["bipReport"]["localization"]		= '#langue#'>
			
			<!--- DONNEES ENVOYEES A BIP --->
			<cfset parameters["bipReport"]["reportParameters"]	= structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"] = structNew()>
			<cfset parameters["bipReport"]["reportParameters"]["P_LOGIN"]["parameterValues"] = [tableau[3]]>
			
			<!--- PARAMÈTRES DE GÉNÉRATION FTP DU RAPPORT BIP --->
			<cfset parameters.bipReport.delivery.parameters.ftpUser="container">
			<cfset parameters.bipReport.delivery.parameters.ftpPassword="container">
			
			<!--- CONFIGURATION EVENT --->					
			<cfset parameters.EVENT_TARGET  = #M21_UUID#>
			<cfset parameters.EVENT_HANDLER = "M21">
			<cfset parameters.EVENT_TYPE 	= #type#>

			<!--- PARAMETRES POUR BIP --->
			<cfset parameters["bipReport"]["xdoTemplateId"]		= "#templateID#">
			<cfset parameters.bipReport.delivery.parameters.fileRelativePath="M21/#M21_UUID#/#prefixeFile#_#M21_UUID#.html">
						
			<!--- ENREGISTREMENT DU REPERTOIRE DE DESTINATION --->
			<cfset resltRecordMail = mailing.setMailToDatabase(#iduser#, #tableau[3]#, #M21_UUID#, "M21", #type#, #fromMailing#, #tableau[3]#, #subject#, #bccMailing#, '')>
			
			<!--- VERIFICATION DE L'ENVOI A BIP OU NON --->
			<cfif resltRecordMail GT 0>
				<cfset api= createObject("component","fr.consotel.consoview.api.CV")>
				<cfset rsltApi = api.invokeService("IbIs","scheduleReport",parameters)>
			<cfelse>
				<cfset rsltApi = -10>
			</cfif>
		
		<cfreturn rsltApi>
	</cffunction>


</cfcomponent>