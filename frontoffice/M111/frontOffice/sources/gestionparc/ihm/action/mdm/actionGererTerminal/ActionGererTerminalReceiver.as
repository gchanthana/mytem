package gestionparc.ihm.action.mdm.actionGererTerminal
{
	import composants.util.ConsoviewAlert;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.MDMDataEvent;
	import gestionparc.ihm.action.IReceiver;
	import gestionparc.ihm.fiches.popup.PopUpEnrollementIHM;
	import gestionparc.services.mdm.MDMService;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;

	public class ActionGererTerminalReceiver implements IReceiver
	{
		private var popUpEnrollement:PopUpEnrollementIHM;
		private var _mdmSrv:MDMService;
		private var osTab:String=" OS NOT DEFINED";

		public function ActionGererTerminalReceiver()
		{
			_mdmSrv=new MDMService();
		}

		public function action():void
		{
			var ligne:String=SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE != null ? SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE : "";
			var idSouTete:int=StringUtil.trim(ligne) != "" ? int(SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE) : 0;
			popUpEnrollement=new PopUpEnrollementIHM();
			popUpEnrollement.addEventListener(MDMDataEvent.ENROLLMENT_INFOS_SENT, afterEnrollmentInfoSent);
			PopUpManager.addPopUp(popUpEnrollement, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(popUpEnrollement);
		}

		private function showPopUpEnrollement():void
		{
			popUpEnrollement=new PopUpEnrollementIHM();
			popUpEnrollement.setInfos(SpecificationVO.getInstance().elementDataGridSelected, getMDMServ(), getOsTab());
			PopUpManager.addPopUp(popUpEnrollement, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(popUpEnrollement);
		}

		private function afterEnrollmentInfoSent(eventObject:Event):void
		{
			popUpEnrollement.removeEventListener(MDMDataEvent.ENROLLMENT_INFOS_SENT, afterEnrollmentInfoSent);
			PopUpManager.removePopUp(popUpEnrollement);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_demande_a__t__prise_en_compte'), Application.application as DisplayObject);
		}

		public function getMDMServ():MDMService
		{
			return _mdmSrv;
		}

		public function getOsTab():String
		{
			return osTab;
		}

		public function setOsTab(_os:String):void
		{
			this.osTab=_os;
		}

		private function enrollDevice():void
		{
			_mdmSrv.enrollDevice(SpecificationVO.getInstance().elementDataGridSelected.T_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_IMEI, SpecificationVO.getInstance().elementDataGridSelected.ZDM_SERIAL_NUMBER, SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL, SpecificationVO.getInstance().elementDataGridSelected.SOUS_TETE, int(SpecificationVO.getInstance().elementDataGridSelected.IDSOUS_TETE), int(SpecificationVO.getInstance().idPoolNotValid));
			_mdmSrv.myDatas.addEventListener(MDMDataEvent.MDM_ENROLL_COMPLETE, enrollDeviceHandler);
		}

		private function enrollDeviceHandler(mdmde:MDMDataEvent):void
		{
			SpecificationVO.getInstance().dispatchEventRefresh();
		}

		private function getOSHandler(mde:MDMDataEvent):void
		{
			if (_mdmSrv.myDatas.arrListeDevice != null && _mdmSrv.myDatas.arrListeDevice.length > 0)
			{
				for each (var item:Object in _mdmSrv.myDatas.arrListeDevice)
				{
					if (item.NAME.toString().toLowerCase() == "plateforme".toLowerCase())
					{
						setOsTab(item.VALUE.toString());
						break;
					}
				}
			}
			// Affichage de PopUpEnrollementIHM
			showPopUpEnrollement();
		}
	}
}
