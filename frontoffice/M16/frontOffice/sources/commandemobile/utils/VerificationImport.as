package commandemobile.utils
{
	import commandemobile.itemRenderer.IRImei;
	
	import composants.util.lignes.LignesUtils;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import gestioncommande.entity.References;
	import gestioncommande.entity.TypesCommandesMobile;
	import gestioncommande.services.CommandeService;
	
	import mx.collections.ArrayCollection;
	import mx.core.Container;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	
	public class VerificationImport extends EventDispatcher
	{	
		public var LIGNESUTILS				:LignesUtils = new LignesUtils();
		private var _references:References;
		private var _cmdServ:CommandeService = new CommandeService(); 

		/***************************************
		 ********** Vérification IMEI **********
		 ***************************************/
		
		public function verifierIMEIDoublon(value:References):Boolean
		{
			_references = value;

			var _arrayRef			:Array = (_references.arrayReferences).source;
			var lgt					:int = _arrayRef.length;
			var currentChanged		: int = _arrayRef.indexOf(_references);
			var isDoublon			:Boolean = false;
			
			for(var i:int=0; i<lgt; i++)
			{
				// comparer les differentes lignes de grille
				if(currentChanged !=  _arrayRef.indexOf(_arrayRef[i] as References))
				{
					// si input a la meme valeur qu'un autre input
					if(value.NUM_IMEI == (_arrayRef[i] as References).NUM_IMEI)
					{
						_references.IMEI_IS_DOUBLON = true;
						_references.ETAT_IMEI = 2;
						_references.IMEI_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16','Num_ro_IMEI_d_j__pr_sent_dans_la_grille');
						isDoublon = true;
						break;
					}
					else
					{
						_references.IMEI_IS_DOUBLON = false;
						if(_references.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.EQU_NUS_FIXE)
						{
							_references.ETAT_IMEI = 51;
						}
						else
						{
							_references.ETAT_IMEI = 1;
						}
						_references.IMEI_LIBELLE_ERROR = '';
						isDoublon = false;
					}
				}
			}
			return isDoublon;
			
		}// End verifierIMEIDoublon
		
		public function verifierImeiEnBase(value:References):void
		{
			_references = value;
			_references.ETAT_IMEI = 3;
			LIGNESUTILS.uniciteIMEI(value.NUM_IMEI);
			LIGNESUTILS.addEventListener("verifierUniciteIMEI", verifierUniciteIMEIResultHandler);
		}
		
		protected function verifierUniciteIMEIResultHandler(evt:Event):void
		{
			if(LIGNESUTILS.IMEIIsEnBase)
			{
				_references.ETAT_IMEI = 2;
				_references.IMEI_IS_ENBASE = true;
				_references.IMEI_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16','Num_ro_IMEI_d_j__pr_sent_dans_la_base');
			}
			else 
			{
				if(_references.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.EQU_NUS_FIXE)
				{
					_references.ETAT_IMEI = 51;
				}
				else
				{
					_references.ETAT_IMEI = 1;
				}
				_references.IMEI_IS_ENBASE = false;
				_references.IMEI_LIBELLE_ERROR = '';
			}
		}
		
		/************************************************************************
		 ******* Verification du Numero de ligne (sous tete)  dans le DataGrid **
		 ************************************************************************/
		public function verifierLigneDoublon(value:References):Boolean
		{
			_references = value;
			
			var _listeRef:ArrayCollection = _references.arrayReferences;
			var lgt:int = _listeRef.length;
			var isDoublon:Boolean = false;
			for(var i:int=0; i<lgt; i++)
			{
				// comparer des lignes de references differentes
				if(_references.IDSOUSTETE != (_listeRef[i] as References).IDSOUSTETE)
				{
					// si input a la meme valeur qu'un autre input
					if(value.SOUSTETE == (_listeRef[i] as References).SOUSTETE)
					{
						_references.SOUSTETE_IS_DOUBLON = true;
						_references.ETAT_SOUSTETE = 2;
						_references.SOUSTETE_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16','Num_ro_d_j__pr_sent_dans_la_grille');
						isDoublon = true;
						break;
					}
					else
					{
						_references.SOUSTETE_IS_DOUBLON = false;
						if(_references.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.NVL_LIGNE_FIXE)
						{
							_references.ETAT_SOUSTETE = 51;
						}
						else
						{
							_references.ETAT_SOUSTETE = 1;
						}
						_references.SOUSTETE_LIBELLE_ERROR = '';
						isDoublon = false;
					}
				}
			}
			return isDoublon;
		}
		
		public function verifierLigneEnBase(value:References):void
		{
			_references = value;
			_references.ETAT_SOUSTETE = 3;
			LIGNESUTILS.uniciteLigne(value.SOUSTETE);
			LIGNESUTILS.addEventListener("verifierUniciteNumeroLigne", verifierUniciteLigneResultHandler);
		}
		
		protected function verifierUniciteLigneResultHandler(evt:Event):void
		{
			if(LIGNESUTILS.ligneIsEnBase)
			{
				_references.ETAT_SOUSTETE = 2;
				_references.SOUSTETE_IS_ENBASE = true;
				_references.SOUSTETE_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16','Num_ro_d_j__pr_sent_dans_la_base');
			}
			else 
			{
				if(_references.MYCOMMANDE.IDTYPE_COMMANDE == TypesCommandesMobile.NVL_LIGNE_FIXE)
				{
					_references.ETAT_SOUSTETE = 51;
				}
				else
				{
					_references.ETAT_SOUSTETE = 1;
				}
				_references.SOUSTETE_IS_ENBASE = false;
				_references.OLD_IDSOUSTETE = LIGNESUTILS.idsoustete;
				_references.SOUSTETE_LIBELLE_ERROR = '';
			}
		}
		
		/******************************************************
		 ******* Verification du Numero de carte SIM ***********
		 ******************************************************/
		public function verifierSimDoublon(value:References):Boolean
		{
			_references = value;
			
			var _listeRef:ArrayCollection = _references.arrayReferences;
			var lgt:int = _listeRef.length;
			var isDoublon:Boolean = false;
			
			for(var i:int=0; i<lgt; i++)
			{
				// comparer des lignes de references differentes
				if(_references.IDSOUSTETE != (_listeRef[i] as References).IDSOUSTETE)
				{
					// si input a la meme valeur qu'un autre input
					if(value.NUM_SIM == (_listeRef[i] as References).NUM_SIM)
					{
						_references.SIM_IS_DOUBLON = true;
						_references.ETAT_SIM = 2;
						_references.SIM_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16','Num_ro_SIM_d_j__pr_sent_dans_la_grille');
						isDoublon = true;
						break;
					}
					else
					{
						_references.SIM_IS_DOUBLON = false;
						_references.ETAT_SIM = 1;
						_references.SIM_LIBELLE_ERROR = '';
						isDoublon = false;
					}
				}
			}
			return isDoublon;
		}
		
		public function verifierSimEnBase(value:References):void
		{
			_references = value;
			_references.ETAT_SIM = 3;
			LIGNESUTILS.uniciteSIM(value.NUM_SIM);
			
			LIGNESUTILS.addEventListener("verifierUniciteSIM", verifierUniciteSimResultHandler);
			
		}
		
		protected function verifierUniciteSimResultHandler(evt:Event):void
		{
			if(LIGNESUTILS.SIMIsEnBase)
			{
				_references.ETAT_SIM = 2;
				_references.SIM_IS_ENBASE = true;
				_references.SIM_LIBELLE_ERROR = ResourceManager.getInstance().getString('M16','Num_ro_SIM_d_j__pr_sent_dans_la_base');
			}
			else 
			{
				_references.ETAT_SIM = 1;
				_references.SIM_IS_ENBASE = false;
				_references.SIM_LIBELLE_ERROR = '';
			}
		}
	
		/******************************************************************* 
		 *****  verification globale dans la grille et dans la base
		 *******************************************************************/	
		public function checkDoublon(listeRef:ArrayCollection):Boolean
		{
			for(var i:int = 0; i<listeRef.length; i++)
			{
				if(  ((listeRef[i] as References).IMEI_IS_DOUBLON ) || ((listeRef[i] as References).IMEI_IS_ENBASE)
					|| ((listeRef[i] as References).SOUSTETE_IS_DOUBLON ) || ((listeRef[i] as References).SOUSTETE_IS_ENBASE)
					||((listeRef[i] as References).SIM_IS_DOUBLON ) || ((listeRef[i] as References).SIM_IS_ENBASE) 
					|| ((listeRef[i] as References).ETAT_IMEI == 3) ||((listeRef[i] as References).ETAT_SIM == 3) || ((listeRef[i] as References).ETAT_SOUSTETE == 3)   )					  
				{	
					return true;	
				}
			}
			return false;
		}
		
	}
}