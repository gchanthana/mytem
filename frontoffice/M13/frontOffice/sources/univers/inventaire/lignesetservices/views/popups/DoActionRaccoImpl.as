package univers.inventaire.lignesetservices.views.popups
{
	import composants.controls.TextInputLabeled;
	import composants.util.CvDateChooser;
	
	import mx.controls.TextArea;
	
	import univers.inventaire.lignesetservices.applicatif.GestionEtatRaccordement;
	import univers.inventaire.lignesetservices.vo.Action;
	import univers.inventaire.lignesetservices.vo.ModeRaccordementVo;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	
	[Bindable]
	public class DoActionRaccoImpl extends PopUpImpl
	{
		public static const OPERATEUR_HISTORIQUE:Number = 63;
		
		public var dcDateRacco : CvDateChooser;
		public var txtReference :TextInputLabeled;
		public var txtCommentaires : TextArea;
		
		protected var _app:GestionEtatRaccordement;
		public function get app():GestionEtatRaccordement{
			return _app;
		} 
		public function set app(appli :GestionEtatRaccordement):void{
			_app = appli;
		} 
		
		private var _modeRacco : ModeRaccordementVo;
		public function get modeRacco():ModeRaccordementVo{
			return _modeRacco;
		}
		public function set modeRacco(mode :ModeRaccordementVo):void{
			_modeRacco = mode;
		}
		
		private var _operateurIndirect : OperateurVO;
		public function get operateurIndirect():OperateurVO{
			return _operateurIndirect;
		}
		public function set operateurIndirect(ope:OperateurVO):void{
			_operateurIndirect = ope;
		}
		
		private var _action : Action;
		public function get action():Action{
			if (_action == null) _action = new Action();
			
			_action.commentaire = (txtCommentaires != null)?txtCommentaires.text : "-";
			_action.reference = (txtReference != null)?txtReference.text : "-";
			_action.dateAction = (dcDateRacco != null)?dcDateRacco.selectedDate:null;
			return _action;
		} 
		public function set action(act : Action):void{
			_action = act;
		}
		
		
		private var _ressourcesSelectionees : Array;
		public function get ressourcesSelectionees():Array{
			if (_ressourcesSelectionees == null) _ressourcesSelectionees = new Array();
			return _ressourcesSelectionees;
		} 
		public function set ressourcesSelectionees(liste : Array):void{			
			_ressourcesSelectionees = liste;
		}
		
		
		
		public function DoActionRaccoImpl()
		{
			super();
		}
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			configurerDateSelector();
		}
		
		
		protected function configurerDateSelector():void{
			if (app != null){
				var selectableRange : Object = new Object();
				if(app.historiqueActions != null && app.historiqueActions.length > 0){
					selectableRange.rangeStart = app.historiqueActions[0].DATE_EFFET;
					dcDateRacco.selectableRange = selectableRange;
					dcDateRacco.selectedDate = new Date();
				} else{
					dcDateRacco.selectedDate = new Date();
				}
			}
		}
		
	}
}