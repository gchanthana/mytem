<cfcomponent name="fr.consotel.consoview.api.ibis.publisher.delivery.email.EmailDeliveryDefinitionSupport" extends="fr.consotel.consoview.api.ibis.publisher.delivery.AbstractDeliveryDefinition">
	<cfset init()>
	<cffunction name="init" returntype="fr.consotel.consoview.api.ibis.publisher.delivery.IDeliveryDefinition" access="public" description="Constructor">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["emailTo"]="">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["emailFrom"]="">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["emailCC"]="">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["emailBCC"]="">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["emailReplyTo"]="">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["emailSubject"]="">
		<cfset VARIABLES["DELIVERY_OPTIONS"]["emailBody"]="">
		<cfreturn THIS>
	</cffunction>
	
	<cffunction name="getType" returntype="string" access="public"
				description="Returns delivery type (Example : ftpOption,emailOption,localOption).">
		<cfreturn "emailOption">
	</cffunction>
	<!--- 
	<cffunction name="setParameter" returntype="boolean" access="public" description="Set value for a delivery parameter.">
		<cfargument name="parameterName" type="string" required="true" hint="Available values are : emailTo,emailFrom,emailCC,emailBCC,emailReplyTo,emailSubject,emailBody">
		<cfargument name="parameterValue" type="string" required="true" hint="Value is remote file absolute path">
		<cfset var tmpStruct="NULL">
		<cfif NOT structIsEmpty(VARIABLES["DELIVERY_OPTIONS"])>
			<cfset tmpStruct=VARIABLES["DELIVERY_OPTIONS"][getType()]>
			<cfloop item="it" collection="#tmpStruct#">
				<cfif UCASE(it) EQ UCASE(ARGUMENTS.parameterName)>
					<cfset VARIABLES["DELIVERY_OPTIONS"][getType()][ARGUMENTS.parameterName]=ARGUMENTS.parameterValue>
					<cfreturn TRUE>
				</cfif>
			</cfloop>
			<cfreturn FALSE>
		<cfelse>
			<cfreturn FALSE>
		</cfif>
	</cffunction>
	
	<cffunction name="getParameterInfos" returntype="struct" access="public"
				description="Returns Delivery Parameters Infos as a struture. This can be provided as delivery option when executing report.">
		<cfreturn VARIABLES["DELIVERY_OPTIONS"]>
	</cffunction>
	 --->
</cfcomponent>
