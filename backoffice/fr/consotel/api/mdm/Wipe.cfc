<cfcomponent displayname="Wipe" implements="fr.consotel.api.mdm.IFunction" >	
	<cffunction name="execute">
		<cfargument name="serialNumber" 	type="String" 	required="true" hint="Serial Number de l'équipement">
		<cfargument name="imei"				type="String"	required="true" hint="IMEI de l'équipement (Ou une chaine vide pour envoyer la valeur nulle)">
		<cfargument name="idTerminal"		type="Numeric" 	required="true" hint="ID de l'equipement dans CV">
		<cfargument name="erasedMemoryCard" type="boolean" 	required="true" hint="TRUE pour supprimer le contenu de la carte mémoire">
		<cfargument name="eraseData" 		type="Boolean" 	required="true" hint="True si on efface les données entreprise">
		<cfargument name="pinCode" 			type="String"  	required="true" hint="Code pin du mobile">
		<cfargument name="waitTime"			type="Numeric" 	required="true" hint="Délai d'attente pour de la réponse de l'opération en millisecondes">
		<cfargument name="singleLicenseKey" type="String" 	required="true" hint="Single Licence Key (Ou une chaine vide)">
		<cfargument name="idracine" 		type="Numeric" 	required="true" hint="Idracine courant">
			
			<cfset var infos			= structNew()><!--- cette structure est obligatoire pour récupérer les infos sur le serveur MDM ainsi que le code function --->
			<cfset var infos.idracine 	= idracine><!--- il faudra rajouter des informations dans la structure et les gérer dans get_ListeCodeFunction_ServeurMDM() --->
			
			<cfset var infosMDM = createObject("component", "fr.consotel.api.mdm.CodeFunction").get_ListeCodeFunction_ServeurMDM(infos)>
		
			<cfset param = structNew() >
			<cfset StructInsert(param, "serialNumber", "#serialNumber#", 1)>
			<cfset StructInsert(param, "imei", "#imei#", 1)>
			<cfset StructInsert(param, "idTerminal", "#idTerminal#", 1)>
			<cfset StructInsert(param, "erasedMemoryCard", "#erasedMemoryCard#", 1)>
			<cfset StructInsert(param, "eraseData", "#eraseData#", 1)>
			<cfset StructInsert(param, "pinCode", "#pincode#", 1)>
			<cfset StructInsert(param, "waitTime", "#waitTime#", 1)>
			<cfset StructInsert(param, "singleLicenseKey", "#singleLicenseKey#", 1)>
			<cfset StructInsert(param, "servermdmpwd", "#infosMDM.servermdmpwd#", 1)>
			<cfset StructInsert(param, "servermdmuser", "#infosMDM.servermdmuser#", 1)>
			<cfset StructInsert(param, "servermdm", "#infosMDM.servermdm#", 1)>
		
			<cfset wipeDevice = createObject("component","fr.consotel.api.mdm.wipe.Wipe#infosMDM.codefunction#")>
			<!---<cfset wipeDevice.action(param)>--->
			<cfset var myTEM=createObject("component","fr.saaswedo.api.myTEM.mdm.MyTEM").getInstance()>
			<cfset var iDeviceMngt=myTEM.getIDeviceManagement()>
			<!--- Effacement complet : Réinitialisation du device --->
			<cfif param.eraseData EQ true>
				<cfset var lockDevice = iDeviceMngt.wipe(serialNumber, imei)>
				<cflog type="information" text="Wipe device [#param.serialNumber#,#param.imei#] : #lockDevice#"> 
				<cfif lockDevice EQ 3>
					<cfthrow type="Custom" message="Echec de l'effacement complet des données. Code de retour : #lockDevice#">
				</cfif>
			<!--- Effacement des données : Supprime les données entreprise liées à Zenprise --->
			<cfelse>
				<cfset var lockDevice = iDeviceMngt.corporateWipe(param.serialNumber,param.imei,param.waitTime)>
				<cflog type="information" text="Corporate Wipe device [#param.serialNumber#,#param.imei#] : #lockDevice#">
				<cfif lockDevice EQ 3>
					<cfthrow type="Custom" message="Echec de l'effacement des données. Code de retour : #lockDevice#">
				</cfif>
			</cfif>
	</cffunction>
</cfcomponent>
