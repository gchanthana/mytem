package vo.widgets
{
	import mx.utils.StringUtil;

	public class Widget10DataVo
	{
		public function Widget10DataVo()
		{
		}

		private var _nbCommande:Number;
		private var _etatCommande:String;
		
		public function fill(obj:Object):void
		{
			this._nbCommande=obj.saw_1;
			this._etatCommande=StringUtil.trim(obj.saw_0);
		}
		
		public function get nbCommande():Number
		{
			return _nbCommande;
		}
		
		public function set nbCommande(value:Number):void
		{
			_nbCommande = value;
		}
		
		public function get etatCommande():String
		{
			return _etatCommande;
		}
		
		public function set etatCommande(value:String):void
		{
			_etatCommande = value;
		}
	}
}