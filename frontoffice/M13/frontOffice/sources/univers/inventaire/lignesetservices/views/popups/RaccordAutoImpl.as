package univers.inventaire.lignesetservices.views.popups
{
	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	import univers.inventaire.lignesetservices.applicatif.GestionEtatRaccordement;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.OperateurVO;
	
	[Bindable]
	public class RaccordAutoImpl extends DoActionRaccoImpl
	{
		public var dgInventaire : DataGrid;
		
		
		
		
		public function RaccordAutoImpl()
		{
			super();
			operateurIndirect = new OperateurVO();
			operateurIndirect.id = OPERATEUR_HISTORIQUE;
			
		}
		
		override public function set app(appli:GestionEtatRaccordement):void{
			_app = appli;			
			_app.getListeModeRaccordement();
		}
		
		//======== FORMATTERS =========================================================================================
		protected function formateBoolean(item : Object, column : DataGridColumn):String{
			if (item != null) return (Number(item[column.dataField]) == 1)?"OUI":"NON";
			else return "-";
		}
		
		//======== FIN FORMATTERS =========================================================================================
		
		override protected function verifierDonnees():void{
			var boolOK : Boolean = true;
			var message : String = "\n";
			
			if(dcDateRacco.selectedDate == null){
				boolOK = false;
				message = message + "- Vous devez sélectionner une date de raccordement.\n";
			}
			
			if(modeRacco == null){
				throw new IllegalOperationError("Le mode de raccordement n'est pas spécifié");
			}
			
			if(boolOK){
				//Si on était en raccord indirect via prefixe, on vide les opérateur indirect.
				/* if (app.etatVo.IDTYPE_RACCORDEMENT == GestionEtatRaccordement.PRESELECTION){
					app.supprimerOperateursIndirect(new Array(5));
				} */
				
				//Sauvegarde des ressources à entrer et à sortir de l'inventaire.
				ressourcesSelectionees = null;
				for(var i : Number = 0; i < (dgInventaire.dataProvider as ArrayCollection).length; i ++){
					if ((dgInventaire.dataProvider[i].ENTRER == true) || (dgInventaire.dataProvider[i].SORTIR == true) ){
						ressourcesSelectionees.push(dgInventaire.dataProvider[i]);
					}
				}
				
				super.verifierDonnees();
			}else{
				Alert.okLabel = "Fermer";
				Alert.show(message,"Erreur",Alert.OK);			
			}
		}
		
	}
}