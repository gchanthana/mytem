package composants.tb.tableaux.services
{
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class FonctionServices extends EventDispatcher
	{
		private var _myArrayUsage:ArrayCollection;
		
		public var theGoodIndexUsageForCB:int=0;
		
		public static const LISTE_USAGE_LOADED : String = "listeUsageLoaded";
		
		public function FonctionServices()
		{
		}
		
		public function getListeUsages():void{
			var op :AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.equipement.lignes.GestionLignes",
																				"getUsages",
																				getListeUsagesResultHandler); 
			RemoteObjectUtil.invokeService(op);
		}

		private function getListeUsagesResultHandler(re:ResultEvent):void
		{
			if (re.result)
			{
				myArrayUsage = re.result as ArrayCollection;
				this.dispatchEvent(new Event(LISTE_USAGE_LOADED));
			}
		}			


		public function set myArrayUsage(value:ArrayCollection):void
		{
			_myArrayUsage = value;
		}

		public function get myArrayUsage():ArrayCollection
		{
			return _myArrayUsage;
		}
	}
}