package univers.facturation.tb.service
{
	import mx.rpc.events.ResultEvent;

	public class CheminOrgaHandler
	{
		private var _model:CheminOrgaModel;
		
		public function CheminOrgaHandler(model:CheminOrgaModel)
		{
			_model = model;
		}
		
		public function get model():CheminOrgaModel
		{
			return _model;
		}
		
		public function set model(value:CheminOrgaModel):void
		{
			_model = value;
		}
		
		internal function getCheminOrga(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateCheminOrga(event.result as String);
			}
		}
	}
}