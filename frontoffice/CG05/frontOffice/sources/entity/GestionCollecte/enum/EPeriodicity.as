package entity.GestionCollecte.enum
{
    import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
    public class EPeriodicity extends Enumeratio
    {
        public static const PONCTUELLE:EPeriodicity = new EPeriodicity("PONCTUELLE", ResourceManager.getInstance().getString('CG05','Ponctuelle'));
        public static const MENSUELLE:EPeriodicity = new EPeriodicity("MENSUELLE", ResourceManager.getInstance().getString('CG05','Mensuelle'));
        public static const BIMESTRIELLE:EPeriodicity = new EPeriodicity("BIMESTRIELLE", ResourceManager.getInstance().getString('CG05','Bimestrielle'));
        public static const TRIMESTRIELLE:EPeriodicity = new EPeriodicity("TRIMESTRIELLE", ResourceManager.getInstance().getString('CG05','Trimestrielle'));
        public static const SEMESTRIELLE:EPeriodicity = new EPeriodicity("SEMESTRIELLE", ResourceManager.getInstance().getString('CG05','Semestrielle'));
        public static const ANNUELLE:EPeriodicity = new EPeriodicity("ANNUELLE", ResourceManager.getInstance().getString('CG05','Anuelle'));

        public function EPeriodicity(value:String, label:String)
        {
            super(value, label);
        }

        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(PONCTUELLE, MENSUELLE, BIMESTRIELLE, TRIMESTRIELLE, SEMESTRIELLE, ANNUELLE));
        }
    }
}