package event.news
{
	import flash.events.Event;

	public class NewsEvent extends Event
	{
		public static const GET_LIST_NEWS_EVENT :String ="getListNewsEvent";
		public static const CHANGER_STATUS_NEWS_EVENT :String ="ChangerStatusNewsEvent";
		
		public function NewsEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new NewsEvent(type, bubbles, cancelable);
		}
	}
}