package gestioncommande.services
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestioncommande.entity.Zone;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.TextArea;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class GenrateField extends EventDispatcher
	{

		//--------------------------------------------------------------------------------------------//
		//					VARIBLES PUBLIC 
		//--------------------------------------------------------------------------------------------//
		
		public var libelleChampsPers	:Object;
		
		//--------------------------------------------------------------------------------------------//
		//					VARIBLES PRIVATE 
		//--------------------------------------------------------------------------------------------//
		
		private var _listeChamps		:XML;
		private var _champs1XML			:XMLList;
		private var _champs2XML			:XMLList;
		private var _champs1Pers		:ArrayCollection;
		private var _champs2Pers		:ArrayCollection;
		
		public var hbxChamps1			:HBox = new HBox;
		public var hbxChamps2			:HBox = new HBox;
		
		public var LIBELLE_CHAMPS1		:String  = ResourceManager.getInstance().getString('M16', 'R_f_rence_client_1_');
		public var EXEMPLE_CHAMPS1		:String  = "";
		public var LIBELLE_CHAMPS2		:String  = ResourceManager.getInstance().getString('M16', 'R_f_rence_client_2_');
		public var EXEMPLE_CHAMPS2		:String  = "";
		
		private var champs1				:ArrayCollection;
		private var champs2				:ArrayCollection;
			
		public static var SHADOW_CHAR:String = "/*|*/";
		
		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR 
		//--------------------------------------------------------------------------------------------//
		
		public function GenrateField()
		{
			addEventListener("CHAMPS_PERSO", setSessionUserObject)
		}

		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURE 
		//--------------------------------------------------------------------------------------------//		
		
		public function fournirChampsPersonnalises():void
	    {
	    	var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
	    																		"fr.consotel.consoview.M21.Champs",
	    																		"getChamps",
	    																		fournirChampsPersonnalisesResultHandler);
			RemoteObjectUtil.callService(op, CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX);
	    }
	    
	    //--------------------------------------------------------------------------------------------//
		//					RETOUR PROCEDURE 
		//--------------------------------------------------------------------------------------------//	
	    
	    private function fournirChampsPersonnalisesResultHandler(re:ResultEvent):void
	    {
	    	if(re.result)
	    	{
	    		if((re.result as ArrayCollection).length > 0)
	    		{
		    		_listeChamps = new XML(re.result[0].XML);
					readXML();
					dispatchEvent(new Event("CHAMPS_PERSO"));
				}
				else
	    			ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Param_trage_du_champ_de_r_f_rence_client'), 'Consoview', null);
	    	}
	    	else
	    		ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Param_trage_du_champ_de_r_f_rence_client'), 'Consoview', null);
	    }
	    
	    private function setSessionUserObject(e:Event):void
	    {
	    	SessionUserObject.singletonSession.CHAMPS;
	    }
	    
	    //--------------------------------------------------------------------------------------------//
		//					TRAITEMENT XML
		//--------------------------------------------------------------------------------------------//

	    private function readXML():void
		{
			SessionUserObject.singletonSession.CHAMPS 			= new Object();
			SessionUserObject.singletonSession.CHAMPS.LIBELLE1 	= _listeChamps.champ1_libelle;
			SessionUserObject.singletonSession.CHAMPS.EXEMPLE1 	= _listeChamps.champ1_exemple;
			SessionUserObject.singletonSession.CHAMPS.ACTIVE1 	= convertIntToBool(_listeChamps.champ1_actif);
			SessionUserObject.singletonSession.CHAMPS.OBLIG1	= false;
			
			var auto1:Boolean = convertIntToBool(_listeChamps.champ1_auto);
			SessionUserObject.singletonSession.CHAMPS.AUTO1		= convertIntToBool(_listeChamps.champ1_auto);
						
			SessionUserObject.singletonSession.CHAMPS.LIBELLE2 	= _listeChamps.champ2_libelle;
			SessionUserObject.singletonSession.CHAMPS.EXEMPLE2 	= _listeChamps.champ2_exemple;
			SessionUserObject.singletonSession.CHAMPS.ACTIVE2	= convertIntToBool(_listeChamps.champ2_actif);
			SessionUserObject.singletonSession.CHAMPS.OBLIG2	= false;
			
			var auto2:Boolean = convertIntToBool(_listeChamps.champ2_auto);
			SessionUserObject.singletonSession.CHAMPS.AUTO2		= convertIntToBool(_listeChamps.champ2_auto);
			
			_champs1XML = (_listeChamps.champs1[0] as XML).children();
			_champs2XML = (_listeChamps.champs2[0] as XML).children();
			
			attributXMLToDatagrid();
		}
		
		private function attributXMLToDatagrid():void
		{
			var i	:int  = 0;
			var zone:Zone = new Zone();		
			_champs1Pers  = new ArrayCollection();
			_champs2Pers  = new ArrayCollection();

			for(i = 0;i < _champs1XML.length();i++)
			{
				zone = new Zone();		
				zone.COMPTEUR_ZONE					= _champs1XML[i].zone;
				zone.STEPPEUR_ZONE					= _champs1XML[i].number_caracteres;
				zone.EXACT_NBR						= convertIntToBool(_champs1XML[i].exact_number);
				zone.COMPTEUR_OBLIG					= _champs1XML[i].number_caracteres_oblig;
				zone.TYPE_ZONE						= _champs1XML[i].caracteres;
				if(zone.TYPE_ZONE == SHADOW_CHAR)
					zone.TYPE_ZONE = " ";
				zone.IDTYPE_ZONE					= 1;
				zone.CONTENU_ZONE.ID_CARACTERE		= _champs1XML[i].idtype_caracteres;
				zone.CONTENU_ZONE.LIBELLE_CARATERE	= _champs1XML[i].type_caracteres;
				zone.SAISIE_ZONE					= convertIntToBool(_champs1XML[i].obligatoire);
				if(zone.SAISIE_ZONE)
					SessionUserObject.singletonSession.CHAMPS.OBLIG1	= true;
				_champs1Pers.addItem(zone);
			}
			
			for(i = 0;i < _champs2XML.length();i++)
			{
				zone = new Zone();		
				zone.COMPTEUR_ZONE					= _champs2XML[i].zone;
				zone.STEPPEUR_ZONE					= _champs2XML[i].number_caracteres;
				zone.EXACT_NBR						= convertIntToBool(_champs2XML[i].exact_number);
				zone.COMPTEUR_OBLIG					= _champs2XML[i].number_caracteres_oblig;
				zone.TYPE_ZONE						= _champs2XML[i].caracteres;
				if(zone.TYPE_ZONE == SHADOW_CHAR)
					zone.TYPE_ZONE = " ";
				zone.IDTYPE_ZONE					= 1;
				zone.CONTENU_ZONE.ID_CARACTERE		= _champs2XML[i].idtype_caracteres;
				zone.CONTENU_ZONE.LIBELLE_CARATERE	= _champs2XML[i].type_caracteres;
				zone.SAISIE_ZONE					= convertIntToBool(_champs2XML[i].obligatoire);
				if(zone.SAISIE_ZONE)
					SessionUserObject.singletonSession.CHAMPS.OBLIG2	= true;
				_champs2Pers.addItem(zone);
			}
			SessionUserObject.singletonSession.CHAMPS.CHAMPS1 = _champs1Pers;
			SessionUserObject.singletonSession.CHAMPS.CHAMPS2 = _champs2Pers;
			createDinamicTextinput();
		}
		
		public function convertIntToBool(isTrue:int):Boolean
		{
			var bool:Boolean = false;;
			
			if(isTrue == 1)
				bool = true;

			return bool;
		}

	    //--------------------------------------------------------------------------------------------//
		//					TRAITEMENT DE LA CREATION DE TEXTAREA
		//--------------------------------------------------------------------------------------------//
		
		private function createDinamicTextinput():void
		{
			champs1 = SessionUserObject.singletonSession.CHAMPS.CHAMPS1;
			champs2 = SessionUserObject.singletonSession.CHAMPS.CHAMPS2;
			var i	:int = 0;
			var cl	:TextArea = new TextArea;
			var lb	:TextArea = new TextArea;
			
			for(i = 0;i < champs1.length;i++)
			{
				cl 				= new TextArea;
				lb	 			= new TextArea;
				cl.width		= widthAdaptator(champs1[i].STEPPEUR_ZONE);
				cl.height		= 20;
				cl.restrict		= restrictChamps(champs1[i].CONTENU_ZONE.ID_CARACTERE)
				cl.maxChars		= champs1[i].STEPPEUR_ZONE;
				cl.id			= "champs1_" + i;
				cl.addEventListener(Event.CHANGE,textinputChangeHandler);
				hbxChamps1.addChild(cl);				
				lb.text			= champs1[i].TYPE_ZONE;
				lb.width		= widthAdaptator(lb.text.length);
				lb.height		= 20;
				lb.editable		= false;
				lb.selectable	= false;
				lb.setStyle("styleName","noBorderTextArea");
				hbxChamps1.addChild(lb);
			}
			
			for(i = 0;i < champs2.length;i++)
			{
				cl 				= new TextArea;
				lb	 			= new TextArea;
				cl.width		= widthAdaptator(champs2[i].STEPPEUR_ZONE);
				cl.height		= 20;
				cl.restrict		= restrictChamps(champs2[i].CONTENU_ZONE.ID_CARACTERE)
				cl.maxChars		= champs2[i].STEPPEUR_ZONE;
				cl.id			= "champs2_" + i;
				cl.addEventListener(Event.CHANGE,textinputChangeHandler);
				hbxChamps2.addChild(cl);
				lb.text			= champs2[i].TYPE_ZONE;
				lb.width		= widthAdaptator(lb.text.length);
				lb.height		= 20;
				lb.editable		= false;
				lb.selectable	= false;
				lb.setStyle("styleName","noBorderTextArea");
				hbxChamps2.addChild(lb);			
			}
			SessionUserObject.singletonSession.CHAMPS.hbxChamps1 = hbxChamps1;
			SessionUserObject.singletonSession.CHAMPS.hbxChamps2 = hbxChamps2;			
		}
		
		private function textinputChangeHandler(e:Event):void
		{
			(e.currentTarget as TextArea).errorString = "";
		}
		
		private function widthAdaptator(value:int):int
		{
			var cteSize		:int = 7;
			var widthNbr	:int = 0;
			var rslt		:int = 0;
			
			if(value == 1)
				widthNbr = 14;
			else
				widthNbr = (value * cteSize)+14;

			rslt = widthNbr;
			
			return rslt;
		}
		
		private function restrictChamps(value:int):String
		{
			var restrictedCar:String = "";
			switch(value)
			{
				case 0 :	restrictedCar = null;
							break;
				case 1 :	restrictedCar = "0123456789";
							break;
				case 2 :	restrictedCar = "a-zA-Z";
							break;
			}
			return restrictedCar;
		}

	}
}