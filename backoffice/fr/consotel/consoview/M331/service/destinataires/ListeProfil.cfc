<cfcomponent output="false">
	
	<cffunction name="getAllProfils" access="remote" returntype="query" hint="recupérer tous les profil d'une racine">
		
		<cfset idRacine   = SESSION.PERIMETRE.ID_GROUPE>
		<cfstoredproc datasource="#session.offreDSN#" procedure="PKG_CV_ANNUAIRE.getAllProfils">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>
	
</cfcomponent>