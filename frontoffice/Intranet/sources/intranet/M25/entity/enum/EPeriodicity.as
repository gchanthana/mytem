package intranet.M25.entity.enum
{
    import mx.collections.ArrayCollection;

    public class EPeriodicity extends Enumeratio
    {
        public static const PONCTUELLE:EPeriodicity = new EPeriodicity("PONCTUELLE", "Ponctuelle");
        public static const MENSUELLE:EPeriodicity = new EPeriodicity("MENSUELLE", "Mensuelle");
        public static const BIMESTRIELLE:EPeriodicity = new EPeriodicity("BIMESTRIELLE", "Bimestrielle");
        public static const TRIMESTRIELLE:EPeriodicity = new EPeriodicity("TRIMESTRIELLE", "Trimestrielle");
        public static const SEMESTRIELLE:EPeriodicity = new EPeriodicity("SEMESTRIELLE", "Semestrielle");
        public static const ANNUELLE:EPeriodicity = new EPeriodicity("ANNUELLE", "Annuelle");

        public function EPeriodicity(value:String, label:String)
        {
            super(value, label);
        }

        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(PONCTUELLE, MENSUELLE, BIMESTRIELLE, TRIMESTRIELLE, SEMESTRIELLE, ANNUELLE));
        }
    }
}