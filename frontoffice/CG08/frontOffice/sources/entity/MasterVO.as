package entity
{
	[Bindable]
	public class MasterVO
	{
		private var _idMaster:Number;
		private var _nomMaster:String = '';
		private var _nomApplication:String = '';
		private var _SELECTED:Boolean = false;
		
		public function MasterVO()
		{
		}
		
		public function fill(obj:Object):Boolean
		{
			try
			{
				if(obj.hasOwnProperty('IDRACINE_MASTER'))
					this.idMaster = obj.IDRACINE_MASTER;
				
				if(obj.hasOwnProperty('RACINE_MASTER'))
					this.nomMaster = obj.RACINE_MASTER;
				
				if(obj.hasOwnProperty('NOM_APPLICATION'))
					this.nomApplication = obj.NOM_APPLICATION;
				
				return true;
			}
			catch(e:Error)
			{
				trace("Erreur pendant le chargement du MasterVO");
				return false
			}
			return false
		}
		
		public function get nomApplication():String { return _nomApplication; }
		
		public function set nomApplication(value:String):void
		{
			if (_nomApplication == value)
				return;
			_nomApplication = value;
		}
		
		
		public function get nomMaster():String { return _nomMaster; }
		
		public function set nomMaster(value:String):void
		{
			if (_nomMaster == value)
				return;
			_nomMaster = value;
		}
		
		public function get idMaster():Number { return _idMaster; }
		
		public function set idMaster(value:Number):void
		{
			if (_idMaster == value)
				return;
			_idMaster = value;
		}
		
		public function get SELECTED():Boolean { return _SELECTED; }
		
		public function set SELECTED(value:Boolean):void
		{
			if (_SELECTED == value)
				return;
			_SELECTED = value;
		}
	}
}