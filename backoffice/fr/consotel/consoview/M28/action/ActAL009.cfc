﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction">
	
	<!--- Recuperer les droit d'une fonction avec le niveau de visibilité de --->
	<cffunction name="executeAct" access="public" returntype="Struct" >
		<cfargument name="data" type="Struct" required="true">
		
		<cftry>
			<cfstoredproc procedure="pkg_M28.getParametreFonction2" datasource="ROCOFFRE">
				<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#data.idFonction#">
				<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_langid" type="in" value="#data.langueID#">
				<cfprocresult name="p_retour" >
			</cfstoredproc>
			
			<cfset qResult = structNew()>
			<cfset qResult.LIST_DROITS = p_retour>
			<cfreturn CreateReponse("AL009", 1, qResult)>
		
			<cfcatch type="any" >
				<cfreturn CreateReponse("AL009", 0, ThrowCfCatchError("AL009",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>

</cfcomponent>