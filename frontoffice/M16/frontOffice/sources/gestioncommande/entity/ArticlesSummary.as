package gestioncommande.entity
{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import gestioncommande.services.Formator;

	public class ArticlesSummary
	{
		
		//--------------------------------------------------------------------------------------------//
		//					PROPRIETEES
		//--------------------------------------------------------------------------------------------//
		
		public var LIBELLE			:String = '';
		public var TYPE				:String = '';
		public var TYPE_DATA		:String = '';
		public var ACTION			:String = '';
		public var LIBELLE_ACTION	:String = '';
		public var REF_DISTRIBUTEUR	:String = '';
		
		public var PRIX_TOTAL_S		:String = '0.00';
		public var PRIX_UNIT_S		:String = '0.00';
		public var PRIX_MENSUEL_S	:String = '0.00';
		
		public var NUM_CONFIG		:int = 0;
		public var QUANTITY			:int = 0;
		
		public var PRIX_TOTAL		:Number = 0;
		public var PRIX_UNIT		:Number = 0;
		public var PRIX_MENSUEL		:Number = 0;

		//--------------------------------------------------------------------------------------------//
		//					CONSTRUCTEUR
		//--------------------------------------------------------------------------------------------//
		
		public function ArticlesSummary()
		{
		}
		
		public static function mappingArticlesSummary(value:Object, myCommande:Commande):ArticlesSummary
		{
			var newArtSum:ArticlesSummary 	= new ArticlesSummary();
				newArtSum.LIBELLE 			= value.LIBELLE;
				newArtSum.TYPE 				= value.TYPE;
				newArtSum.REF_DISTRIBUTEUR	= value.REF_DISRIBUTEUR;
				newArtSum.TYPE_DATA		 	= value.TYPE_DATA;
				newArtSum.ACTION 			= value.ACTION;
				newArtSum.QUANTITY 			= int(value.NB);
				newArtSum.NUM_CONFIG		= int(value.NUM_CONFIG);
				
			if(newArtSum.TYPE_DATA == 'EQUIPEMENT')
			{
				newArtSum.PRIX_TOTAL 		= Number(value.MONTANT);
				newArtSum.PRIX_TOTAL_S 		= Formator.formatTotalWithSymbole(newArtSum.PRIX_TOTAL);
				newArtSum.PRIX_UNIT 		= newArtSum.PRIX_TOTAL / newArtSum.QUANTITY;
				newArtSum.PRIX_UNIT_S 		= Formator.formatTotalWithSymbole(newArtSum.PRIX_UNIT);
				newArtSum.PRIX_MENSUEL 		= 0;
				newArtSum.PRIX_MENSUEL_S 	= '';
			}
			else if(newArtSum.TYPE_DATA == 'RESSOURCE')
				{
					newArtSum.PRIX_TOTAL 		= 0;
					newArtSum.PRIX_TOTAL_S 		= '';
				
					if(newArtSum.ACTION == 'A')
					{
						newArtSum.LIBELLE_ACTION = ResourceManager.getInstance().getString('M16','Ajout');
					}
					else if(newArtSum.ACTION == 'R')
						{
							newArtSum.LIBELLE_ACTION = ResourceManager.getInstance().getString('M16','R_siliation');
						}
					
					if(value.PRIX_MENSUEL == null)
					{
						newArtSum.PRIX_UNIT 		= 0;
						newArtSum.PRIX_UNIT_S 		= ResourceManager.getInstance().getString('M16', 'n_c');
						newArtSum.PRIX_MENSUEL 		= 0;
						newArtSum.PRIX_MENSUEL_S 	= ResourceManager.getInstance().getString('M16', 'n_c');
					}
					else
					{
						newArtSum.PRIX_MENSUEL 		= Number(value.PRIX_MENSUEL);
						newArtSum.PRIX_MENSUEL_S 	= Formator.formatTotalWithSymbole(newArtSum.PRIX_MENSUEL);
						newArtSum.PRIX_UNIT 		= value.PRIX_MENSUEL/newArtSum.QUANTITY;
						newArtSum.PRIX_UNIT_S 		= Formator.formatTotalWithSymbole(newArtSum.PRIX_UNIT);
					}
				}
			
			if(myCommande.IDTYPE_COMMANDE == TypesCommandesMobile.MODIFICATION_OPTIONS || myCommande.IDTYPE_COMMANDE == TypesCommandesMobile.MODIFICATION_FIXE)
			{
				if(newArtSum.ACTION == 'R')
				{
					newArtSum.PRIX_UNIT 		= 0;
					newArtSum.PRIX_UNIT_S 		= ' - ';
					newArtSum.PRIX_MENSUEL 		= 0;
					newArtSum.PRIX_MENSUEL_S 	= ' - ';
				}
			}
			
			return newArtSum;
		}
		
		public static function formatArticlesSummary(values:ArrayCollection, myCommande:Commande):ArrayCollection
		{
			var artSum	:ArrayCollection = new ArrayCollection();
			var lenOri	:int = values.length;
			
			for(var i:int = 0;i < lenOri;i++)
			{
				artSum.addItem(mappingArticlesSummary(values[i], myCommande));
			}
			
			return artSum;
		}
	}
}