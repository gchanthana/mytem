package service.kpi.updatevalueselectorkpi
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class UpdateValueSelectorKpiHandler
	{
		private var _model:UpdateValueSelectorKpiModel;
		
		public function UpdateValueSelectorKpiHandler(_model:  UpdateValueSelectorKpiModel):void
		{
			this._model = _model;
		}	
		internal function updateValueKpiResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateValueKpi(evt.result as ArrayCollection);
			}
		}
	}
}