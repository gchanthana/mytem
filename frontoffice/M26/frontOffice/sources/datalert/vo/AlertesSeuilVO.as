package datalert.vo
{
	public class AlertesSeuilVO
	{
		public var IDSOUS_TETE:Number = 0;
		public var TYPE_PERIODE:Number = 0;
		public var IS_ROAMING:Number = 0;
		public var LIMIT:Number = 0;
		public var TYPE_ALERTE:String = "";
		public var SEND_MESSAGER_USER:Number = 0;
		public var IDMESSAGE_USER:Number = 0;
		public var SEND_MESSAGE_GEST:Number = 0;
		public var DISABLE_DATA:Number = 0;

		public function AlertesSeuilVO()
		{
		}
	}
}