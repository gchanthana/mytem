package univers.parametres.parametrecompte.ihm
{
	import mx.containers.VBox;
	import mx.controls.DataGrid;
	
	import univers.parametres.parametrecompte.service.ProfilService;
	[Bindable]
	public class ListeProfilCompte extends VBox
	{
		
		public var profilService : ProfilService = new ProfilService();
		//COMPONANT
		public var dg_profil: DataGrid;
		public var champs1:FicheProfilsCommandeIHM
		public var champs2:FicheProfilGestionParcIHM
		public var champs3:FicheProfilValidationFacturationIHM
		
		public function ListeProfilCompte() 
		{
		}
		public function initData():void
		{
			champs1.initData()
			champs2.initData()
			champs3.initData()
		}
	}
}
