package gestionparc.ihm.fiches.popup
{
	import composants.util.ConsoviewAlert;

	import flash.display.DisplayObject;
	import flash.events.MouseEvent;

	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.ligne.LigneServices;

	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;


	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations des lignes à ajouter au raccordement
	 * pour les lignes fixe de type T2, T0, ANA
	 *
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 25.11.2010
	 *
	 * </pre></p>
	 */
	[Bindable]
	public class PopUpAddLineToGrpmentImpl extends TitleWindow
	{
		// VARIABLES------------------------------------------------------------------------------

		// IHM components
		public var dgLigne:DataGrid;

		public var btValid:Button;
		public var btCancel:Button;

		// publics variables
		public var myServiceLigne:LigneServices=null;
		public var lignesDuGrid:ArrayCollection=null;
		public var isTete:Boolean=false;

		// VARIABLES------------------------------------------------------------------------------

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe PopUpAddLineToGrpmentImpl.
		 * </pre></p>
		 */
		public function PopUpAddLineToGrpmentImpl()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		// PUBLIC FUNCTIONS-------------------------------------------------------------------------

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Appel des fonctions <code>initListener()</code>
		 * - Initialisation du service ligne
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #initListener()
		 */
		private function init(event:FlexEvent):void
		{
			var objToGetService:Object=new Object();
			objToGetService.idSousCompte=myServiceLigne.myDatas.ligne.idSousCompte;

			myServiceLigne.getTeteLigneViaSouscompte(objToGetService);
			initListener();
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 *
		 * @return void
		 */
		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);

			// listener lié aux services
			myServiceLigne.myDatas.addEventListener(gestionparcEvent.LISTE_LINE_DISPO_GRPMENT, onListeLineDispoGrpmentHandler);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 *
		 * @see #closePopup()
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Ferme la popup courante.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Valide l'ajout de la ligne au groupement.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			if (dgLigne.selectedItem)
			{
				var obj:Object=new Object();
				obj.idTeteLigne=dgLigne.selectedItem.IDTETE_LIGNE;
				obj.numLigne=dgLigne.selectedItem.TETE;
				obj.isTete=isTete;
				obj.typeLigne=(dgLigne.selectedItem.ISTETE == 1) ? ResourceManager.getInstance().getString('M111', 'T_te_de_ligne') : ResourceManager.getInstance().getString('M111', 'Ligne_du_groupement');

				this.dispatchEvent(new gestionparcEvent(gestionparcEvent.LINE_GRPMENT_ADDED, obj));
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Votre_ligne_a_bien__t__ajout__'), Application.application as DisplayObject);
				closePopup(null);
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M111', 'Erreur'), ResourceManager.getInstance().getString('M111', 'Vous_devez_chosir_une_ligne_avant_de_valider'));
			}
		}

		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Met a jour le datagrid contenant les lignes disponibles pour un groupement d'apres les données recues de la base.
		 * </pre></p>
		 *
		 * @param event
		 * @return void
		 */
		private function onListeLineDispoGrpmentHandler(event:gestionparcEvent):void
		{
			dgLigne.dataProvider=new ArrayCollection();

			var lenLignesDispoPourGrpment:int=myServiceLigne.myDatas.listeLignesDispoPourGrpment.length;
			var lenLignesDuGrid:int=lignesDuGrid.length;
			var ispresent:Boolean=false;

			for (var i:int=0; i < lenLignesDispoPourGrpment; i++)
			{
				ispresent=false;
				for (var j:int=0; j < lenLignesDuGrid; j++)
				{
					if (myServiceLigne.myDatas.listeLignesDispoPourGrpment[i].IDTETE_LIGNE == lignesDuGrid[j].idTeteLigne)
					{
						ispresent=true;
						break;
					}
				}
				if (!ispresent)
				{
					(dgLigne.dataProvider as ArrayCollection).addItem(myServiceLigne.myDatas.listeLignesDispoPourGrpment[i])
				}
			}
		}

		// PRIVATE FUNCTIONS-------------------------------------------------------------------------

	}
}
