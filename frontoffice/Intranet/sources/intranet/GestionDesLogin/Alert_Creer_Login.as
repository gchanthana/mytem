package intranet.GestionDesLogin
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;

	public class Alert_Creer_Login extends TitleWindow
	{
		public var cbOpe:ComboBox;
		public var tiType:TextInput;
		public var tiLogin:TextInput;
		public var tiMDP:TextInput;
		public var tiCDR:TextInput;
		public var tiCompte:TextInput;
		public var ckbTelecharge:CheckBox;		

		[Bindable]
		public static var obj:Object=new Object();

		public var isLogin:Boolean=false;	// true = le login existe déjà - false = le login n'existe pas 
		public var isMdp:Boolean=false;
		public var isCDR:Boolean=false;
		public var isCompte:Boolean=false;
		public var isType:Boolean=false;
		public var isOperateur:Boolean=false;
		public var loginExiste:Boolean=false;
		
		public function Alert_Creer_Login()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,thisCreationCompleteHandler);			
		}
		
		public function selectOp(ev:Event):void
		{
			if(obj != null)
				for(var i:int=0;i<(cbOpe.dataProvider as ArrayCollection).length;i++){
					trace("cbOpe.dataProvider[i].OPERATEURID="+cbOpe.dataProvider[i].OPERATEURID);
					trace("obj.OPERATEURID="+obj.OPERATEURID);
					if(cbOpe.dataProvider[i].OPERATEURID == obj.OPERATEURID){
					trace("ok");
						cbOpe.selectedIndex=i;
						break;
					}
				}
		}
		
		public function thisCreationCompleteHandler(e:FlexEvent):void{
			cbOpe.addEventListener("LISTE_OP_LOADED_EVENT",selectOp)
			if(obj != null){				
				if(obj.TELECHARGER == 0)
					ckbTelecharge.selected=false;
				else
					ckbTelecharge.selected=true;
			}
		}
		
		public function valider(e:MouseEvent):void{
		// Login
			if(tiLogin.text.length>0){
				isLogin=true;
				tiLogin.errorString="";
			}else{
				isLogin=false;
				tiLogin.errorString="Veuillez saisir un login";
			}
		
		// MDP
			if(tiMDP.text != ""){
				isMdp=true;
				tiMDP.errorString="";
			}else{
				isMdp=false;
				tiMDP.errorString="Veuillez saisir un mot de passe";
			}
			
		// CDR
			if(tiCDR.length > 0){
			 	isCDR=true;
				tiCDR.errorString="";
			}else{
				isCDR=false;
			 	tiCDR.errorString="Veuillez entrer un CDR";
			}

		// Compte
			if(tiCompte.length > 0){
				isCompte=true;
				tiCompte.errorString="";
			}else{
				isCompte=false;
				tiCompte.errorString="Veuillez entrer un Compte";
			}
		 
		// Type
			if(tiType.text.length < 1 && !parseInt(tiType.text)){
				isType=false;
				tiType.errorString="Veuillez choisir un Type";
			}else{
			 	isType=true;
			 	tiType.errorString="";
			}
		 	
		// Operateur
		 	if(cbOpe.selectedIndex > -1){
		 		isOperateur=true;
		 		cbOpe.errorString="";
		 	}else{
		 		isOperateur=false;
		 		cbOpe.errorString="Veuillez choisir un Operateur";
		 	}
		 	
		 	if(isCDR && isCompte && isLogin && isMdp && isOperateur && isType){
		 		rechercher();
		 	}
		}
		public function rechercher():void{
		// login
 			var tmpope:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondeslogins.GestionDesLogins",
						"DisplayLogin", 
						ok,no);
			RemoteObjectUtil.callService(tmpope,0); 
		}		
		
		public function ok(e:ResultEvent):void{
			var s:String=tiLogin.text;
			var t:ArrayCollection = e.result as ArrayCollection;
			
			if(s.length >0){
				for(var i:int=0;i<t.length;i++){
					if(s==t[i].LOGIN_EXTRANET){
						loginExiste= true;
						break;						
					}
				}
			}else
				loginExiste=false;
		 		
		 	if(!loginExiste){
				var login:String=StringUtil.trim(tiLogin.text);
		 		var mdp:String=StringUtil.trim(tiMDP.text);
		 		var type:int=parseInt(tiType.text);
		 		var CDR:int=parseInt(tiCDR.text);
		 		var telecharger:int;
		 		if(ckbTelecharge.selected==true)
		 			telecharger=1;
		 		else
		 			telecharger=0;
		 		var compte:String=StringUtil.trim(tiCompte.text);
		 		var operateurid:int=cbOpe.selectedItem.OPERATEURID;
 				
 				var tmp:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondeslogins.GestionDesLogins",
						"CreateUpdateLogin",
						oui,non);
				RemoteObjectUtil.callService(tmp,login,mdp,type,CDR,telecharger,compte,operateurid); 		 			
		 	}else{
		 		Alert.show("Etes vous sur(s) de vouloir modifier ce login ?","Login existant",Alert.YES | Alert.NO,this,modifCloseHandler);
		 	}
		}
		
		public function modifCloseHandler(e:CloseEvent):void{
			if(e.detail==Alert.YES){
				var login:String=StringUtil.trim(tiLogin.text);
		 		var mdp:String=StringUtil.trim(tiMDP.text);
		 		var type:int=parseInt(tiType.text);
		 		var CDR:int=parseInt(tiCDR.text);
		 		var telecharger:int;
		 		if(ckbTelecharge.selected==true)
		 			telecharger=1;
		 		else
		 			telecharger=0;
		 		var compte:String=StringUtil.trim(tiCompte.text);
		 		var operateurid:int=cbOpe.selectedItem.OPERATEURID;

				var tmp:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondeslogins.GestionDesLogins",
						"CreateUpdateLogin",
						oui,non);
				RemoteObjectUtil.callService(tmp,login,mdp,type,CDR,telecharger,compte,operateurid); 
			}
		}
		
		public function no(e:FaultEvent):void{
			trace('Alert_creer_login - Lister Login :'+e.message); 
			
		}

		public function oui(e:ResultEvent):void{
			Alert.show("Login créé ou modifié !","Réussite",Alert.OK,this,closeHandler);
		}
		
		public function non(e:FaultEvent):void{
			trace('Alert_creer_login - Valider :'+e.message); 
		}
		
		public function closeHandler(e:Event):void{
			if(this){
				PopUpManager.removePopUp(this);
				var tmpope:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondeslogins.GestionDesLogins",
						"DisplayLogin",
						getDataOk, getDataNo);
				RemoteObjectUtil.callService(tmpope,0);
			}
		}
		public function getDataOk(e:ResultEvent):void{
			GestionDesLogin.TabLogin= e.result as ArrayCollection;
		}
		
		public function getDataNo(e:FaultEvent):void{
			trace('Alert_Creer_Login - closeHandler :'+e.message);
		}
	
	
	}
}