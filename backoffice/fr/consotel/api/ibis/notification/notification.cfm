<!--- 
Page : /fr/consotel/api/ibis/notification/notification.cfm
Auteur : Cédric
Interface du Handler : 
Implémentation du Handler : fr.consotel.api.ibis.publisher.handler.NotificationServlet (fr.consotel.api.ibis.publisher.handler.HttpReportEventHandler)
--->
<!--- ************************************************************************************************************************* --->
<cfsetting enablecfoutputonly="true" showdebugoutput="#isDebugMode()#" requesttimeout="120">
<cfset NOTIFICATION="REPORTING"><!--- Doit correspondre au nom du serveur de notification HTTP prédéfini dans BIP --->
<cflog text="NOTIFICATION (#NOTIFICATION#:#CGI.HTTP_USER_AGENT#) : #CGI.SCRIPT_NAME# (#CGI.REMOTE_ADDR#)" type="information">
<cfset startTick=getTickCount()>
<cfif isDefined("FORM") AND (NOT structIsEmpty(FORM))>
	<cftry>
		<cfset notificationServlet=createObject("component","fr.consotel.api.ibis.publisher.handler.NotificationServlet").createInstance(NOTIFICATION)>
		<cfset notificationServlet.onHttpNotification(FORM)>		
		<cfcatch type="any">
			<!---
			<cfset DOMAIN="consotel.fr">
			<cfset MAIL = {type="html", from="api@" & DOMAIN, to="monitoring@saaswedo.com"}>
			
			
			
			<cfmail subject="[*Error*] NOTIFICATION : #CGI.SCRIPT_NAME#" attributecollection="#MAIL#">
				<cfoutput>
					Notifiant : #CGI.REMOTE_ADDR# (#CGI.HTTP_USER_AGENT#)<br>
					BackOffice (Notifié) : #CGI.SERVER_NAME#
				</cfoutput>
				<br><br>
				<cfdump var="#FORM#" label="Formulaire HTTP/POST (BIP)"><br />
				<cfdump var="#CFCATCH#" label="Exception rencontrée"><br />
			</cfmail>
			--->
			
			
			
			<cfsavecontent variable="body">
				<cfoutput>
					[*Error*] NOTIFICATION : #CGI.SCRIPT_NAME#<br>
					Notifiant : #CGI.REMOTE_ADDR# (#CGI.HTTP_USER_AGENT#)<br>
					BackOffice (Notifié) : #CGI.SERVER_NAME#
				</cfoutput>
				<br><br>
				<cfdump var="#FORM#" label="Formulaire HTTP/POST (BIP)"><br />
				<cfdump var="#CFCATCH#" label="Exception rencontrée"><br />
			</cfsavecontent>
			<cftry>
				
				

				<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
				<cfset mLog.setIDMNT_CAT(13)>
				<cfset mLog.setIDMNT_ID(27)>
				<cfset mLog.setBODY(body)>
				
				<cfinvoke 
					component="fr.consotel.api.monitoring.MLogger"
					method="logIt"  returnVariable = "result">
					
					<cfinvokeargument name="mLog" value="#mLog#">  
					
				</cfinvoke>	
				
			<cfcatch type="any">
				<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">						
				</cfmail>
			</cfcatch>
			
			</cftry>
			
			
			
			
			<cflog type="error" text="Notification CFCATCH Mail Sent : #CFCATCH.message#">
		</cfcatch>
	</cftry>
</cfif>
<cfset duration=getTickCount() - startTick>
<cflog type="information" text="NOTIFICATION (#CGI.SCRIPT_NAME#) : COMPLETED in #duration# ms">