<cfcomponent output="false" extends="fr.consotel.consoview.access.AccessObject">
	
	
	<!--- <cffunction name="getFacture" access="remote" output="false" returntype="Facture">
		<cfargument name="facture" type="Facture">				
		<cfreturn facture.get()/>
	</cffunction>
	
	<cffunction name="saveFacture" access="remote" output="false" returntype="numeric">
		<cfargument name="facture" required="true" type="Facture">				
		<cfreturn facture.save()/>
	</cffunction> --->
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Fournit la liste des lignes de facturation de la facture qui concerne les produits marquÃ©s pour le contrï¿½le
					  et qui sont facturÃ©s sur toute la pÃ©riode de la facture
		Param in 
			Facture
		Param out	
			LigneFacturation[]
	--->
	<cffunction name="getDetailFacture" access="remote" output="false" returntype="LigneFacturation[]">
		<cfargument name="myfacture" type="Facture" required="true">		
		<cfreturn myfacture.getDetail()/>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Fournit la liste des lignes de facturation de la facture qui concerne les produits marquÃ©s pour le contrï¿½le
					  et qui sont facturÃ©s sur toute la pÃ©riode de la facture
		Param in 
			Facture
		Param out	
			query
	--->
	<cffunction name="getDetailFactureAsQuery" access="remote" output="false" returntype="Query">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfreturn myfacture.getDetailAsQuery()/>
	</cffunction>

<!---
		
		Auteur : samuel.divioka
		
		Date : 12/06/2007
		
		Description : Retourne la liste des factures d'un client contenant le produit sur une pï¿½riode donnï¿½e.
					  La facture est retournï¿½ si et seulement si le produit est facturï¿½ ï¿½ 100% sur la pï¿½riode de la facture	
		Param in 
			LigneFacturation une agregation de lignes de facturation
		Param out
			Facture[]
	
	--->
	<cffunction name="getListeFacturesWithProduit" access="remote" output="false" returntype="Facture[]">
		<cfargument name="myLigneFacturation" type="LigneFacturation" required="true">
		<cfset qListeFacture = getListeFacturesAsQuery(myLigneFacturation)>
		
		<cfset tabFactures = arrayNew(1)>
		<cfset myFacture = "">
		
		<cfset i = 1>
		
		<cfLoop query="qListeFacture">
			<cfset myFacture = createObject("component","Facture").setProprietes(qListeFacture[i])>
			<cfset arrayAppend(tabFactures,myFacture)>
			<cfset i = i + 1>
		</cfLoop>
		
		<cfreturn tabFactures />
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/06/2007
		
		Description : la liste des organisation si on est en racine sinon l'orga sur laquel on se trouve
		
		Param in
		 
			
			
		Param out
			query
	
	--->
	<cffunction name="getListeOrganistaionsAsQuery" access="remote" output="false" returntype="query">
		<cfset type = getTypePerimetre()>
		
		
		<cfif LCase(type) eq "groupe">
			
			<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLOBAL.GET_ORGANISATIONS">
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe" value="#getidGroupe()#"/>
				<cfprocresult name="qListeOrgas"/>        
			</cfstoredproc>
			
			<cfquery name="qResult" dbtype="query">
				select * from qListeOrgas 
				where TYPE_ORGA <> 'OPE' 
				and TYPE_ORGA <> 'SAV'
			</cfquery>
		
			<cfreturn qResult/>
		<cfelse>
					
			<cfquery name="qResult" datasource="#SESSION.OFFREDSN#">
				SELECT * FROM groupe_client gc
				WHERE gc.bool_orga = 1
				START WITH gc.idgroupe_client = #getidGroupeLigne()#
				CONNECT BY PRIOR gc.id_groupe_maitre = gc.idgroupe_client 
			</cfquery>
			
			<cfreturn qResult/>
		</cfif>
		
	</cffunction>

	<cffunction name="calculMontantFacture" access="remote" output="false" returntype="Facture[]">
		<cfargument name="periodid" type="array" required="true"/>
	
		<cfset tabFactures = arrayNew(1)>
	
		<cfloop index="i" from="1" to="#arraylen(periodid)#">
		
			<cfset qListeFacture = Evaluate("calculMontantFacture#getTypePerimetre()#AsQuery(periodid[i])")>
		
			<cfset myFacture = createObject("component", "Facture")>
			
			<cfscript>
			
				myFacture.setOperateurId(qListeFacture.OPERATEURID);
			
				myFacture.setOperateurLibelle(qListeFacture.OPNOM);
				myFacture.setNumero(qListeFacture.NUMERO_FACTURE);
				myFacture.setCompteFacturation(qListeFacture.COMPTE_FACTURATION);
				myFacture.setCompteFacturationId(qListeFacture.IDCOMPTE_FACTURATION);
				myFacture.setPeriodeId(qListeFacture.IDINVENTAIRE_PERIODE);
				myFacture.setDateDebut(qListeFacture.DATEDEB);
				myFacture.setDateFin(qListeFacture.DATEFIN);
				myFacture.setDateEmission(qListeFacture.DATE_EMISSION);
				myFacture.setMontant(qListeFacture.MONTANT_FACTURE);
				myFacture.setRefClientId(qListeFacture.IDREF_CLIENT);
			
				myFacture.setMontantVerifiable(qListeFacture.MONTANT_VERIFIABLE);
				myFacture.setMontantCalcule(qListeFacture.MONTANT_CALCULE);
				myFacture.setMontantCalculeFacture(qListeFacture.MONTANT_VERIFIABLE);
			
				myFacture.setVisee(qListeFacture.BOOL_VISA);
				myFacture.setViseeAna(qListeFacture.BOOL_VISAANA);
				myFacture.setControlee(qListeFacture.BOOL_VALIDE);
			
				//CTRL INVENTAIRE
				myFacture.setControleeInv(qListeFacture.BOOL_INVENTAIRE);
				//FIN CTRL INVENTAIRE
				myFacture.setExportee(qListeFacture.BOOL_EXPORTEE);
			
				myFacture.setCommentaireViser(qListeFacture.COMMENTAIRE_VISA);
				myFacture.setCommentaireViserAna(qListeFacture.COMMENTAIRE_VISAANA);
				myFacture.setCommentaireControler(qListeFacture.COMMENTAIRE_VALIDE);
			
				//CTRL INVENTAIRE
				myFacture.setCommentaireControlerInv(qListeFacture.COMMENTAIRE_INVENTAIRE);
				//FIN CTRL INVENTAIRE
				myFacture.setCommentaireExporter(qListeFacture.COMMENTAIRE_EXPORTEE);
			
			</cfscript>
			
			<cfset arrayAppend(tabFactures, myFacture)>
		</cfloop>
	
		<cfreturn tabFactures/>
	</cffunction>
	

	<cffunction name="calculMontantFacturegroupeAsQuery" access="remote" output="false" returntype="query" hint="liste des factures et leurs états suivant le perimetre / sous perimetre">
		<cfargument name="periodid" type="numeric" required="true"/>
	
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.get_montant_facture_v2" blockfactor="100">
		
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idgroupe_client" value="#SESSION.PERIMETRE.ID_GROUPE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" type="in" variable="p_idinventaire_periode" value="#periodid#"/>
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="qListeFacture"/>
		
		</cfstoredproc>
		<cfreturn qListeFacture/>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Retourne la liste des factures d'un client rï¿½pondant au critï¿½res de recherche
		
		Param in 
			ParamsRecherche les paramï¿½tre de rechercehe
		Param out
			Facture[]
	
	--->
	<cffunction name="getListeFacturesPerimetre" access="remote" output="false" returntype="Facture[]">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<cfset qListeFacture = Evaluate("getListeFactures#getTypePerimetre()#AsQuery(myParamsRecherche)")>	
		
		<cfdump var="#myParamsRecherche#">
		
		<cfset tabFactures = arrayNew(1)>		
		<cfLoop query="qListeFacture">
		 
			<cfset myFacture = createObject("component","Facture")>
			<cfscript>
			
				myFacture.setOperateurId(qListeFacture.OPERATEURID);	
							
				myFacture.setOperateurLibelle(qListeFacture.OPNOM);
				myFacture.setNumero(qListeFacture.NUMERO_FACTURE);
				myFacture.setCompteFacturation(qListeFacture.COMPTE_FACTURATION);
				myFacture.setCompteFacturationId(qListeFacture.IDCOMPTE_FACTURATION);
				myFacture.setPeriodeId(qListeFacture.IDINVENTAIRE_PERIODE);
				myFacture.setDateDebut(qListeFacture.DATEDEB);
				myFacture.setDateFin(qListeFacture.DATEFIN);
				myFacture.setDateEmission(qListeFacture.DATE_EMISSION);
				myFacture.setMontant(qListeFacture.MONTANT_FACTURE);
				myFacture.setRefClientId(qListeFacture.IDREF_CLIENT);
				
								
				
			
				myFacture.setVisee(qListeFacture.BOOL_VISA);
				myFacture.setViseeAna(qListeFacture.BOOL_VISAANA);
				myFacture.setControlee(qListeFacture.BOOL_VALIDE);
				
				//CTRL INVENTAIRE
				myFacture.setControleeInv(qListeFacture.BOOL_INVENTAIRE);
				//FIN CTRL INVENTAIRE
				
				myFacture.setExportee(qListeFacture.BOOL_EXPORTEE);
					
				myFacture.setCommentaireViser(qListeFacture.COMMENTAIRE_VISA);
				myFacture.setCommentaireViserAna(qListeFacture.COMMENTAIRE_VISAANA);
				myFacture.setCommentaireControler(qListeFacture.COMMENTAIRE_VALIDE);
				
				//CTRL INVENTAIRE
				myFacture.setCommentaireControlerInv(qListeFacture.COMMENTAIRE_INVENTAIRE);
				//FIN CTRL INVENTAIRE
				
				myFacture.setCommentaireExporter(qListeFacture.COMMENTAIRE_EXPORTEE);
				
			</cfscript>
			<cfset arrayAppend(tabFactures,myFacture)>
		</cfLoop>
		
		<cfreturn tabFactures />
	</cffunction>
		

	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Retourne la liste des factures d'un client rï¿½pondant au critï¿½res de recherche
		
		Param in 
			ParamsRecherche les paramï¿½tre de rechercehe
		Param out
			query
	
	--->
	<cffunction name="getListeFacturesGroupeAsQuery" access="remote" output="false" returntype="query" hint="liste des factures et leurs états suivant le perimetre / sous perimetre">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.ListFacture_V4" blockfactor="100">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#SESSION.PERIMETRE.ID_GROUPE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#myParamsRecherche.getOperateurId()#" null="#iif((myParamsRecherche.getOperateurId() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#myParamsRecherche.getChaine()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_viseesAna" value="#myParamsRecherche.getEtatViseAna()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_visees" value="#myParamsRecherche.getEtatVise()#"/>		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_controlees" value="#myParamsRecherche.getEtatControle()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_exportees" value="#myParamsRecherche.getEtatExporte()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_inventaire" value="#myParamsRecherche.getEtatInventaire()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#LSDateFormat(myParamsRecherche.getDateDebut(),'yyyy/mm/dd')#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(myParamsRecherche.getDateFin(),'yyyy/mm/dd')#"/>    
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="qListeFacture"/>   
		</cfstoredproc>
		
		<cfreturn qListeFacture/>
	</cffunction>
	
	<cffunction name="getListeFacturesGroupeLigneAsQuery" access="remote" output="false" returntype="Query" hint="liste des factures et leurs états suivant le perimetre / sous perimetre">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.ListFacture_orga_v4">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#SESSION.PERIMETRE.ID_GROUPE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#SESSION.PERIMETRE.ID_PERIMETRE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#myParamsRecherche.getOperateurId()#" null="#iif((myParamsRecherche.getOperateurId() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#myParamsRecherche.getChaine()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_viseesAna" value="#myParamsRecherche.getEtatViseAna()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_visees" value="#myParamsRecherche.getEtatVise()#"/>		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_controlees" value="#myParamsRecherche.getEtatControle()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_exportees" value="#myParamsRecherche.getEtatExporte()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_inventaire" value="-1"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#LSDateFormat(myParamsRecherche.getDateDebut(),'yyyy/mm/dd')#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(myParamsRecherche.getDateFin(),'yyyy/mm/dd')#"/>      
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="qListeFacture"/>   
		</cfstoredproc>	
		
		<cfreturn qListeFacture/>
	</cffunction>
	
	
	
	<!---
		
		
		
		
		
		
		 --->
	<cffunction name="getListeFacturesPerimetrewithcalc" access="remote" output="false" returntype="Facture[]">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<cfset qListeFacture = Evaluate("getListeFactures#getTypePerimetre()#AsQuerywithcalc(myParamsRecherche)")>	
		
		<cfdump var="#myParamsRecherche#">
		
		<cfset tabFactures = arrayNew(1)>		
		<cfLoop query="qListeFacture">
		 
			<cfset myFacture = createObject("component","Facture")>
			<cfscript>
			
				myFacture.setOperateurId(qListeFacture.OPERATEURID);	
							
				myFacture.setOperateurLibelle(qListeFacture.OPNOM);
				myFacture.setNumero(qListeFacture.NUMERO_FACTURE);
				myFacture.setCompteFacturation(qListeFacture.COMPTE_FACTURATION);
				myFacture.setCompteFacturationId(qListeFacture.IDCOMPTE_FACTURATION);
				myFacture.setPeriodeId(qListeFacture.IDINVENTAIRE_PERIODE);
				myFacture.setDateDebut(qListeFacture.DATEDEB);
				myFacture.setDateFin(qListeFacture.DATEFIN);
				myFacture.setDateEmission(qListeFacture.DATE_EMISSION);
				myFacture.setMontant(qListeFacture.MONTANT_FACTURE);
				myFacture.setRefClientId(qListeFacture.IDREF_CLIENT);
				
				myFacture.setMontantVerifiable(qListeFacture.MONTANT_VERIFIABLE);
				myFacture.setMontantCalcule(qListeFacture.MONTANT_CALCULE);
				myFacture.setMontantCalculeFacture(qListeFacture.MONTANT_VERIFIABLE);				
			
				myFacture.setVisee(qListeFacture.BOOL_VISA);
				myFacture.setViseeAna(qListeFacture.BOOL_VISAANA);
				myFacture.setControlee(qListeFacture.BOOL_VALIDE);
				
				//CTRL INVENTAIRE
				myFacture.setControleeInv(qListeFacture.BOOL_INVENTAIRE);
				//FIN CTRL INVENTAIRE
				
				myFacture.setExportee(qListeFacture.BOOL_EXPORTEE);
					
				myFacture.setCommentaireViser(qListeFacture.COMMENTAIRE_VISA);
				myFacture.setCommentaireViserAna(qListeFacture.COMMENTAIRE_VISAANA);
				myFacture.setCommentaireControler(qListeFacture.COMMENTAIRE_VALIDE);
				
				//CTRL INVENTAIRE
				myFacture.setCommentaireControlerInv(qListeFacture.COMMENTAIRE_INVENTAIRE);
				//FIN CTRL INVENTAIRE
				
				myFacture.setCommentaireExporter(qListeFacture.COMMENTAIRE_EXPORTEE);
				
			</cfscript>
			<cfset arrayAppend(tabFactures,myFacture)>
		</cfLoop>
		
		<cfreturn tabFactures />
	</cffunction>
		

	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Retourne la liste des factures d'un client rï¿½pondant au critï¿½res de recherche
		
		Param in 
			ParamsRecherche les paramï¿½tre de rechercehe
		Param out
			query
	
	--->
	<cffunction name="getListeFacturesGroupeAsQuerywithcalc" access="remote" output="false" returntype="query" hint="liste des factures et leurs états suivant le perimetre / sous perimetre">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
					
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.ListFacture_V4" blockfactor="100">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#SESSION.PERIMETRE.ID_GROUPE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#myParamsRecherche.getOperateurId()#" null="#iif((myParamsRecherche.getOperateurId() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#myParamsRecherche.getChaine()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_viseesAna" value="#myParamsRecherche.getEtatViseAna()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_visees" value="#myParamsRecherche.getEtatVise()#"/>		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_controlees" value="#myParamsRecherche.getEtatControle()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_exportees" value="#myParamsRecherche.getEtatExporte()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_inventaire" value="#myParamsRecherche.getEtatInventaire()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#LSDateFormat(myParamsRecherche.getDateDebut(),'yyyy/mm/dd')#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(myParamsRecherche.getDateFin(),'yyyy/mm/dd')#"/>    
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="qListeFacture"/>   
		</cfstoredproc>
		
		<cfreturn qListeFacture/>
	</cffunction>
	
	<cffunction name="getListeFacturesGroupeLigneAsQuerywithcalc" access="remote" output="false" returntype="Query" hint="liste des factures et leurs états suivant le perimetre / sous perimetre">
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_M32.ListFacture_orga_v4">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idracine" value="#SESSION.PERIMETRE.ID_GROUPE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#SESSION.PERIMETRE.ID_PERIMETRE#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#myParamsRecherche.getOperateurId()#" null="#iif((myParamsRecherche.getOperateurId() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numero_cle" value="#myParamsRecherche.getChaine()#"/>			
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_viseesAna" value="#myParamsRecherche.getEtatViseAna()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_visees" value="#myParamsRecherche.getEtatVise()#"/>		
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_controlees" value="#myParamsRecherche.getEtatControle()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_exportees" value="#myParamsRecherche.getEtatExporte()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_inventaire" value="-1"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedebut" value="#LSDateFormat(myParamsRecherche.getDateDebut(),'yyyy/mm/dd')#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(myParamsRecherche.getDateFin(),'yyyy/mm/dd')#"/>   
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">			
			<cfprocresult name="qListeFacture"/>   
		</cfstoredproc>	
		
		<cfreturn qListeFacture/>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Marque la facture comme contrï¿½lï¿½e ou comme non contrï¿½lï¿½e.					 
					  Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
			forceControle = 0 (Boolean);
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatControle" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.updateEtatControle()>			
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 07/06/2011
		
		Description : Marque la facture comme inventaire contôlee ou comme inventaire non contôleecontrutire.					 
					  Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
			forceControle = 0 (Boolean);
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatControleInventaire" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.updateEtatControleInventaire()>			
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : Marque la facture comme contrï¿½lï¿½e analytriquement parlant ou comme non contrï¿½lï¿½e.					 
					  Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatViseAna" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.updateEtatViseAna()>			
	</cffunction>
	
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 04/29/2008
		
		Description : 
					Marque la facture comme bon ï¿½  Ãªtre exportï¿½ ou non exportï¿½					
					Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatExporte" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">		
		<cfreturn myFacture.updateEtatExporte()>
	</cffunction>	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 10/30/2007
		
		Description : 
					Marque la facture comme visee ou non visee					
					Retourne 1 en cas de succees sinon retourne -1.
		
		Param in 
			Facture
		Param out 
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="updateEtatVise" access="remote" output="false" returntype="numeric">
		<cfargument name="myFacture" type="Facture" required="true">		
		<cfreturn myFacture.updateEtatVise()>
	</cffunction>	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/19/2007
		
		Description : 	Marque la facture comme exportee ou non exportee
						Exporte une facture au format CSV
		Param in 
			Facture
		Param out	
			Numeric (1 en cas de succes sinon -1)
	--->
	<cffunction name="exporterCSV" access="remote" output="true" returntype="string">
		<cfargument name="myfacture" type="Facture" required="true">				
		<cfreturn myfacture.exporterCSV(getidGroupe())/>		
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : evolution des montants des facture sur 12 mois
	
	--->
	<cffunction name="getEvolutionCoutByCF" access="remote" output="true" returntype="array">		
		<cfargument name="pFacture" type="Facture" required="true">
		<cfreturn getEvolutionAsTab(Evaluate("getEvolutionCoutByCF#getTypePerimetre()#AsQuery(pFacture)"))>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/5/2007
		
		Description : evolution des coï¿½ts de facturation d'un cf pour un groupe
		
		In Facure
		
		out query	
	--->
	<cffunction name="getEvolutionCoutByCFGroupeAsQuery" access="remote" output="true" returntype="query">		
		<cfargument name="pFacture" type="Facture" required="true">
		 <!--- PROCEDURE PKG_CV_GRCL_FACTURATION.EVOCF(      p_idgroupe_client                              IN INTEGER,
                                                                   p_idcompte_facturation                         IN INTEGER,
                                                                   p_datedeb                                      IN VARCHAR2,
                                                                   p_datefin                                      IN VARCHAR2,
                                                                   p_retour                                       OUT SYS_REFCURSOR) 
		 --->		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GRCL_FACTURATION_V3.EVOCF">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#getidGroupe()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcompte_facturation" value="#pFacture.getCompteFacturationId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedeb" value="#LSDateFormat(DateAdd('m',-11,now()),'YYYY/MM/DD')#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(now(),'YYYY/MM/DD')#"/>		
			<cfprocresult name="qEvolution"/>   
		</cfstoredproc>	
		<cfreturn qEvolution>
	</cffunction>	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/5/2007
		
		Description : evolution des coï¿½ts de facturation d'un cf pour un groupe
		
		In Facure
		
		out query	
	--->
	<cffunction name="getEvolutionCoutByCFGroupeLigneAsQuery" access="remote" output="true" returntype="query">		
		<cfargument name="pFacture" type="Facture" required="true">
		 <!--- PROCEDURE PKG_CV_GRCL_FACTURATION.EVOCF(      p_idgroupe_client                              IN INTEGER,
                                                                   p_idcompte_facturation                         IN INTEGER,
                                                                   p_datedeb                                      IN VARCHAR2,
                                                                   p_datefin                                      IN VARCHAR2,
                                                                   p_retour                                       OUT SYS_REFCURSOR) 
		 --->		
		
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_GLIG_FACTURATION_V3.EVOCF">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idgroupe_client" value="#getidGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_idcompte_facturation" value="#pFacture.getCompteFacturationId()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datedeb" value="#LSDateFormat(DateAdd('m',-11,now()),'YYYY/MM/DD')#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_datefin" value="#LSDateFormat(now(),'YYYY/MM/DD')#"/>		
			<cfprocresult name="qEvolution"/>   
		</cfstoredproc>			
		<cfreturn qEvolution>
		
	</cffunction>	
				
		
		
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/7/2007
		
		Description : transpose les donnï¿½es par mois
	
	--->	
	<cffunction name="getEvolutionAsTab" returntype="array">
		<cfargument name="qEvolution" type="query" required="true">		
		
		<cfquery name="listeMois" dbtype="query">
			select MOIS from qEvolution group by MOIS order by MOIS ASC
		</cfquery>		
		
		
		<cfset coutMoyenConsos = 0>
		<cfset coutMoyenAbos = 0>
		
		<cfquery name="qCoutMoyenAbos" dbtype="query">
			select AVG(MONTANT_FINAL) as COUTMOYENABO from qEvolution where TYPE_THEME = 'Abonnements' and MONTANT_FINAL <> 0
		</cfquery>
		
		<cfquery name="qCoutMoyenConsos" dbtype="query">
			select AVG(MONTANT_FINAL) as COUTMOYENCONSO from qEvolution where TYPE_THEME = 'Consommations' and MONTANT_FINAL <> 0
		</cfquery>
		
		<cfif qCoutMoyenConsos.recordCount eq 0>
			<cfset coutMoyenConsos = 0>
		<cfelse>
			<cfset coutMoyenConsos = qCoutMoyenConsos.COUTMOYENCONSO[1]>
		</cfif>		
		
		<cfif qCoutMoyenAbos.recordCount eq 0>
			<cfset coutMoyenAbos = 0>
		<cfelse>
			<cfset coutMoyenAbos = qCoutMoyenAbos.COUTMOYENABO[1]>
		</cfif>		
		
		<cfset tab = arrayNew(1)>
		<cfloop query="listeMois">
			<cfset donnee = createobject("component","DataEvolution")>
			<cfset donnee.setMOIS(listeMois.MOIS)>
			<cfset arrayAppend(tab,donnee)>
		
		</cfloop>
				
		<cfloop query="qEvolution">
			<cfloop index="i" from="1" to="#arrayLen(tab)#">
				<cfif qEvolution.MOIS eq tab[i].getMOIS()>
					<cfif LCASE(qEvolution.TYPE_THEME) eq  "Abonnements">
						<cfset tab[i].setCOUTABO(qEvolution.MONTANT_FINAL)>
						<cfset tab[i].setRATIOCOUTABO(qEvolution.RATIO_MOYENNE)>
						<cfset tab[i].setCOUTMOYENABO(coutMoyenAbos)>
					<cfelse>
						<cfset tab[i].setCOUTCONSO(qEvolution.MONTANT_FINAL)>
						<cfset tab[i].setRATIOCOUTCONSO(qEvolution.RATIO_MOYENNE)>
						<cfset tab[i].setCOUTMOYENCONSO(coutMoyenConsos)>
					</cfif> 
						<cfset tab[i].setRATIOMOYENNE(100)>
				</cfif>					
			</cfloop>			
		</cfloop>			 		
		<cfreturn tab>
	</cffunction>
	
	<!---		
		Auteur : samuel.divioka		
		Date : 12/14/2007		
		Description : Exporte les donnes du graph des ï¿½volutions des ratio cout au format CSV
		Param in 	
			facture		
		Param out
			string le nom du fichier si ok sinon 'erreur'	
	--->
	<cffunction name="exporterEvolutionRatioCoutByCfCSV" access="public" output="true" returntype="any">
		<cfargument name="pFacture" type="Facture" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset cf_name = rereplace(pFacture.getcompteFacturation(),"[^A-Za-z0-9]","","all")>
			<cfset fileName = UnicId&"_"&"EvolutionRatioCoutsCompte_"&cf_name&".csv">	
			
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail =getEvolutionCoutByCF(pFacture)>				
				<cfset NewLine = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">		
				<cfoutput>mois;montant abos;motant moyen abos;ratio abos;montant consos;montant moyen consos;ratio consos#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#"><cfoutput>#TRIM(LSDateFormat(aDetail[i].getMOIS(),'DD/MM/YYYY'))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTABO()/100,"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTCONSO()/100,"________.__"))#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>		

			<!--- Crï¿½ation du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">
			 
			<cfreturn "#fileName#">			
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/13/2007
		
		Description : Export des donnï¿½es du graph des evolutions des coï¿½ts par compte de facturation
		
		Param in 	
			facture		
		Param out
			string le nom du fichier si ok sinon 'erreur'
	--->
	<cffunction name="exporterEvolutionCoutByCfCSV" access="public" output="true" returntype="any">
		<cfargument name="pFacture" type="Facture" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset cf_name = rereplace(pFacture.getcompteFacturation(),"[^A-Za-z0-9]","","all")>
			<cfset fileName = UnicId&"_"&"EvolutionCoutsCompte_"& cf_name &".csv">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail =getEvolutionCoutByCF(pFacture)>				
				<cfset NewLine = Chr(13) & Chr(10)>		
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="utf-8">
				<cfcontent type="text/plain">		
				<cfoutput>mois;montant abos;motant moyen abos;ratio abos;montant consos;montant moyen consos;ratio consos#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#"><cfoutput>#TRIM(LSDateFormat(aDetail[i].getMOIS(),'DD/MM/YYYY'))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENABO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTABO()/100,"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getCOUTMOYENCONSO(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getRATIOCOUTCONSO()/100,"________.__"))#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>		
			<!--- Crï¿½ation du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="utf-8"
					addnewline="true" fixnewline="true" output="#contentObj#">
					 
			<cfreturn "#fileName#"> 		
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	
	<!---
		
		Auteur : daisy.bachelin
		
		Date : 11/06/2010
		
		Description : Export des donnï¿½es du grid des facture au format xls
		
		Param in 	
			ParamsRecherche		
		Param out
			string le nom du fichier si ok sinon 'erreur'
	--->
	<!--- deprecated --->
	<cffunction name="_exporterListeFacturesXLS" access="public" output="true" returntype="any">	
		
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<cfset UnicId = createUUID()>
		<cfset rootPath=expandPath("/")>
		<cftry>
					<cfset fileName = UnicId&"_"&"EtatFactures.xls">	
					<cfsavecontent variable="contentObj">
						<cfset aDetail = getListeFacturesPerimetre(myParamsRecherche)>
						<cfinclude template="/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/exportXLS.cfm">
					</cfsavecontent>
					
					
					<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/#fileName#" charset="utf-8" 	addnewline="true" fixnewline="true" output="#contentObj#" >
				
					<cfreturn "#fileName#">				
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="exporterListeFacturesXLS" access="public" output="true" returntype="any">		
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<cfargument name="boolcalc" type="numeric" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"EtatFactures.xls">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfif boolcalc eq 0>
					<cfset aDetail = getListeFacturesPerimetre(myParamsRecherche)>
   			 	<cfelse> 
   			 	 	<cfset aDetail = getListeFacturesPerimetrewithcalc(myParamsRecherche)>
				</cfif>		 
   			 	 <cfoutput>
	   			 <table border=1 cellSpacing=2 cellPadding=0>
		        	<tr bgcolor="BADEF7" ><!-- Header Row-->
		            <td colspan="1" style="font-size:15px;">Op&eacute;rateur</td>
		            <td colspan="1" style="font-size:15px;">compte facturation</td>
		            <td colspan="1" style="font-size:15px;">num&eacute;ro facture</td>
		            <td colspan="1" style="font-size:15px;">date &eacute;mission</td>
		            <td colspan="1" style="font-size:15px;">montant factur&eacute;</td>
		            <td colspan="1" style="font-size:15px;">prct ctrl</td>
		            <td colspan="1" style="font-size:15px;">montant controlable</td>
		            <td colspan="1" style="font-size:15px;">produits control&eacute;s (facture)</td>
		            <td colspan="1" style="font-size:15px;">produits control&eacute;s (controle)</td>
		            <td colspan="1" style="font-size:15px;">vis&eacute;e</td>
		            <td colspan="1" style="font-size:15px;">commentaire visa</td>
		            <td colspan="1" style="font-size:15px;">vis&eacute;e ana</td>		     
		            <td colspan="1" style="font-size:15px;">commentaire visa ana</td>
		            <td colspan="1" style="font-size:15px;">control&eacute;e</td>
		            <td colspan="1" style="font-size:15px;">commentaire controle</td>
					<td colspan="1" style="font-size:15px;">control&eacute;e inventaire</td>
		            <td colspan="1" style="font-size:15px;">commentaire controle inventaire</td>
		            <td colspan="1" style="font-size:15px;">ERP</td>
		            <td colspan="1" style="font-size:15px;">commentaire ERP</td>
		        </tr>
		
		
			<cfloop index="i" from="1" to="#arrayLen(aDetail)#"> 
			
			    <cfif aDetail[i].getMontant() eq 0>
				<cfset pct = LSNumberFormat(100,"________.__")><cfelse>
				<cfset pct = LSNumberFormat(aDetail[i].getMontantVerifiable()/aDetail[i].getMontant()*100,"________.__")></cfif>
								
				<cfset commentaireViserAna = formateString(aDetail[i].getCommentaireViserAna())/>
				<cfset commentaireViser = formateString(aDetail[i].getCommentaireViser())/>
				<cfset commentaireControler = formateString(aDetail[i].getCommentaireControler())/>
				<cfset commentaireControlerInv = formateString(aDetail[i].getCommentaireControlerInv())/>
				<cfset commentaireExporter = formateString(aDetail[i].getCommentaireExporter())/>	  
	        <tr>
	         
	           <td colspan="1"  align="center" >#TRIM(aDetail[i].getOperateurLibelle())#</td>
	            <td colspan="1" align="center" >#aDetail[i].getCompteFacturation()#</td>
	           	<td colspan="1"  align="center"  >#TRIM(toString(aDetail[i].getNumero()))#</td>
	            <td colspan="1" align="center"  >#TRIM(LSDateFormat(aDetail[i].getDateEmission(),'DD/MM/YYYY'))#</td>
	            <td colspan="1"  align="center" >#TRIM(LSNumberFormat(aDetail[i].getMontant(),"________.__"))#</td>
	            <td colspan="1" align="center">#TRIM(pct)#</td>
	            <td colspan="1" align="left"  >#TRIM(LSNumberFormat(aDetail[i].getMontantVerifiable(),"________.__"))#</td>
	            <td colspan="1"  align="left" >#TRIM(LSNumberFormat(aDetail[i].getMontantCalculeFacture(),"________.__"))#</td>
	            <td colspan="1"  align="left" >#TRIM(LSNumberFormat(aDetail[i].getMontantCalcule(),"________.__"))#</td>
	            <td colspan="1" align="center" >#TRIM(aDetail[i].getVisee())#</td>	           
	            <td colspan="1" align="left" >#TRIM(commentaireViser)#</td>
	            <td colspan="1" align="center" >#TRIM(aDetail[i].getViseeAna())#</td>
				<td colspan="1" align="left" >#TRIM(commentaireViserAna)#</td>	            
				<td colspan="1" align="center" >#TRIM(aDetail[i].getControlee())#</td>
	            <td colspan="1" align="left" >#commentaireControler#</td>
				<td colspan="1" align="center" >#TRIM(aDetail[i].getControleeInv())#</td>
	            <td colspan="1" align="left" >#commentaireControlerInv#</td>				
	            <td colspan="1" align="center" >#TRIM(aDetail[i].getExportee())#</td>
	            <td colspan="1" align="left" >#commentaireExporter#</td>	           	               
	        </tr> 
			     
		</cfloop>
		 
				
</cfoutput>
    </table>		 	  			 	  			 												
	</cfsavecontent>
			
			<!--- Crï¿½ation du fichier xls --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/#fileName#" charset="iso-8859-1" 
			addnewline="true" fixnewline="true" output="#contentObj#">	
								 
			<cfreturn "#fileName#"> 		
		<cfcatch type="any">					
				<cfreturn "error">	
		</cfcatch>
		</cftry>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/13/2007
		
		Description : Export des donnï¿½es du grid des facture
		
		Param in 	
			ParamsRecherche		
		Param out
			string le nom du fichier si ok sinon 'erreur'
	--->
	<cffunction name="exporterListeFacturesCSV" access="public" output="true" returntype="any">		
		<cfargument name="myParamsRecherche" type="ParamsRecherche" required="true">
		<cfargument name="boolcalc" type="numeric" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"EtatFactures.csv">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>
				<cfif boolcalc eq 0>
					<cfset aDetail = getListeFacturesPerimetre(myParamsRecherche)>
   			 	<cfelse> 
   			 	 	<cfset aDetail = getListeFacturesPerimetrewithcalc(myParamsRecherche)>
				</cfif>									
				<cfset NewLine = Chr(13) & Chr(10)>		   
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="iso-8859-1">
				<cfcontent type="text/plain">
				<cfoutput>opérateur;compte facturation;numéro facture;date émission;montant facturé;prct ctrl;montant controlable;produits controlés (facture);produits controlés (controle);visée;commentaire visa;visée ana;commentaire visa ana;controlée;commentaire controle;controlée inventaire;commentaire controle inventaire;ERP;commentaire ERP;#NewLine#</cfoutput>
				<cfloop index="i" from="1" to="#arrayLen(aDetail)#">
				<cfif aDetail[i].getMontant() eq 0>
				<cfset pct = LSNumberFormat(100,"________.__")><cfelse>
				<cfset pct = LSNumberFormat(aDetail[i].getMontantVerifiable()/aDetail[i].getMontant()*100,"________.__")></cfif>
								
				<cfset commentaireViserAna = formateString(aDetail[i].getCommentaireViserAna())/>
				<cfset commentaireViser = formateString(aDetail[i].getCommentaireViser())/>
				<cfset commentaireControler = formateString(aDetail[i].getCommentaireControler())/>
				<cfset commentaireControlerInv = formateString(aDetail[i].getCommentaireControlerInv())/>
				<cfset commentaireExporter = formateString(aDetail[i].getCommentaireExporter())/>
				
				<cfoutput>#TRIM(aDetail[i].getOperateurLibelle())#;#TRIM(toString(aDetail[i].getCompteFacturation()))#;#TRIM(toString(aDetail[i].getNumero()))#;#TRIM(LSDateFormat(aDetail[i].getDateEmission(),'DD/MM/YYYY'))#;#TRIM(LSNumberFormat(aDetail[i].getMontant(),"________.__"))#;#TRIM(pct)#;#TRIM(LSNumberFormat(aDetail[i].getMontantVerifiable(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getMontantCalculeFacture(),"________.__"))#;#TRIM(LSNumberFormat(aDetail[i].getMontantCalcule(),"________.__"))#;#TRIM(aDetail[i].getVisee())#;#commentaireViser#;#TRIM(aDetail[i].getViseeAna())#;#commentaireViserAna#;#TRIM(aDetail[i].getControlee())#;#commentaireControler#;#TRIM(aDetail[i].getControleeInv())#;#commentaireControlerInv#;#TRIM(aDetail[i].getExportee())#;#commentaireExporter#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>		
			<!--- Création du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="iso-8859-1" 
					addnewline="true" fixnewline="true" output="#contentObj#" >
					 
			<cfreturn "#fileName#"> 		
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/13/2007
		
		Description : Export des donnees de facturation sur une organisation
		
		Param in
			ParamsRecherche
		Param out
			string le non du fichier genï¿½rï¿½
	--->
	<cffunction name="exporterDetailFactureByOrga" access="public" output="true" returntype="any">		
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="idOrganisation" type="numeric" required="true">
		<cfargument name="format" type="string" required="true">
		
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			
			
			<cfswitch expression="#format#">
				<cfcase value="XLS">
					
					<cfset fileName = UnicId
						&"_"
							&"facture"
								&rereplace(myFacture.getNumero(),"[^A-Za-z0-9]","","all")
									& ".xls">
					<cfsavecontent variable="contentObj">
						<cfset aDetail = detailFactureByOrga(myFacture,idOrganisation)>
						<cfinclude template="/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/factureByOrga.cfm">
					</cfsavecontent>
					
					<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/#fileName#" charset="utf-8" 	addnewline="true" fixnewline="true" output="#contentObj#" >
					
					
					<cfreturn "#fileName#">
				</cfcase>
				
				<cfcase value="PDF">
				</cfcase>
				
				<cfcase value="CSV">
					<cfset fileName = UnicId
						&"_"
							&"facture"
								&rereplace(myFacture.getNumero(),"[^A-Za-z0-9]","","all")
									& ".csv">
					<cfsavecontent variable="contentObj">
						<cfsetting enablecfoutputonly="true"/>										 				
						<cfset aDetail = detailFactureByOrga(myFacture,idOrganisation)>
												
						<cfset NewLine = Chr(13) & Chr(10)>																		
						<cfheader name="Content-Disposition" value="inline;filename=#fileName#.csv" charset="iso-8859-1">
						<cfcontent type="text/plain">
						<cfset setlocale('French (Standard)')>									
						<cfoutput>libelle du noeud;localisation;lignes;fonction;collaborateur annuaire;collaborateur si opéareteur;montant (euros);#NewLine#</cfoutput>
														
						<cfloop query="aDetail">				
							<cfset _MONTANT = LSNumberFormat(val(TRIM(MONTANT)),"-_________.___")>				
							<cfset _CHEMIN = formateString(CHEMIN) />
							<cfset _LIBELLE = formateString(LIBELLE_GROUPE_CLIENT)/>
							<cfset _LIGNE = formateString(SOUS_TETE)/>
							<cfoutput>#TRIM(_LIBELLE)#;#TRIM(_CHEMIN)#;#TRIM(_LIGNE)#;#TRIM(FONCTION)#;#TRIM(COLLABORATEUR_ANU)#;#TRIM(COLLABORATEUR)#;#TRIM(_MONTANT)#;#NewLine#</cfoutput>
						</cfloop>
					</cfsavecontent>
					
					<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="iso-8859-1" 
					addnewline="true" fixnewline="true" output="#contentObj#">
										
					<cfreturn "#fileName#">
				</cfcase>
					
				<cfdefaultcase>
					<cfreturn "error">
				</cfdefaultcase>				
			</cfswitch>			
			 		
			<cfcatch type="any">			
					<cfreturn "error">	
			</cfcatch>
			
		</cftry>
	</cffunction>
	
	
	
	<cffunction name="exporterFactureByOrga" access="public" output="true" returntype="any">		
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="idOrganisation" type="numeric" required="true">
		<cfargument name="format" type="string" required="true">
		
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			
			
			<cfswitch expression="#format#">
				<cfcase value="XLS">
					<cfset fileName = UnicId
						&"_"
							&"facture"
								&rereplace(myFacture.getNumero(),"[^A-Za-z0-9]","","all")
									& ".xls">
					
					<cfsavecontent variable="contentObj">
						<cfset aDetail = factureByOrga(myFacture,idOrganisation)>
						<cfinclude template="/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/factureByOrgaAggreg.cfm">
					</cfsavecontent>
					
					<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/#fileName#" charset="utf-8" 	addnewline="true" fixnewline="true" output="#contentObj#" >
					
					
					<cfreturn "#fileName#">
				</cfcase>
				
				<cfcase value="PDF">
				</cfcase>
				
				<cfcase value="CSV">
					<cfset fileName = UnicId
						&"_"
							&"facture"
								&rereplace(myFacture.getNumero(),"[^A-Za-z0-9]","","all")
									& ".csv">					
					<cfsavecontent variable="contentObj">
						<cfsetting enablecfoutputonly="true"/>										 				
						
						<cfset aDetail = factureByOrga(myFacture,idOrganisation)>														
						<cfset NewLine = Chr(13) & Chr(10)>																		
						<cfheader name="Content-Disposition" value="inline;filename=#fileName#.csv" charset="iso-8859-1">
						<cfcontent type="text/plain">
						<cfset setlocale('French (Standard)')>								
						<cfoutput>libelle du noeud;localisation;lignes;montant (euros);#NewLine#</cfoutput>
														
						<cfloop query="aDetail">				
							<cfset _MONTANT = LSNumberFormat(val(TRIM(MONTANT)),"-_________.___")>				
							<cfset _CHEMIN = formateString(CHEMIN) />
							<cfset _LIBELLE = formateString(LIBELLE_GROUPE_CLIENT)/>
							<cfoutput>#TRIM(_LIBELLE)#;#TRIM(_CHEMIN)#;#TRIM(_MONTANT)#;#NewLine#</cfoutput>
						</cfloop>
					</cfsavecontent>
					
					<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="iso-8859-1" 
					addnewline="true" fixnewline="true" output="#contentObj#">
					
					<cfreturn "#fileName#">
				</cfcase>
					
				<cfdefaultcase>
					<cfreturn "error"> 
				</cfdefaultcase>
				
			</cfswitch>
					 
			 		
		<cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 30/6/2008
		
		Description : Ressources inventaire facturï¿½es
	--->
	<cffunction access="remote" name="getMontantRessourcesInventaireNonFacturee" returntype="Ressource">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.getMontantRessourcesInventaireNonFacturee()>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 02/07/2008
		
		Description : forurnit la liste des ressources dans inventaire non facturee du cf dune facture, dans la periode de cette facture 
	
	--->
	<cffunction access="remote" name="getDetailRessourcesInventaireNonFacturee" returntype="Ressource[]">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		<cfreturn myFacture.getDetailRessourcesInventaireNonFacturee(typeTheme)>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 02/07/2008
		
		Description : exporte la la liste des ressources dans inventaire non facturee du cf dune facture, dans la periode de cette facture
	
	--->
	<cffunction name="exporterRessourcesICSV" access="remote" output="true" returntype="string">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		<cfreturn myfacture.exporterRessourcesICSV(typeTheme)/>
	</cffunction>
	  
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/6/2007
		
		Description : Ressources hors inventaire facturï¿½es
		
		Ressoures (couple produit-ligne) facturï¿½es hors inventaire = 
		-	ressources qui n?ont jamais fait partie de l?inventaire et qui sont facturï¿½es ou 
		-	qui sont sortis de l?inventaire avant le debut de la pï¿½riode de la facture mais qui sont toujours facturï¿½es ou
		-	qui sont entrï¿½s dans l?inventaire aprï¿½s la fin de la facture et qui sont dï¿½jï¿½ facturï¿½es
		
		1 - On est d?accord que pour la partie abos, je prends tout ce qui est facturï¿½ sur ces ressources hors inventaire, et qui est de type de theme Abos.
		
		2 - Pour ce qui est des consos, tu me demandes de prendre, parmi les lignes des ressources hors inventaire, ceux qui ont un abo de sur theme ï¿½ Lignes ï¿½, et d?afficher les consos sur ces lignes.
		C?est bien ï¿½a ?
		
		Dans la feuille explicative, partie consos, tu me dis ï¿½ Faire remonter tous les produits, sauf ceux dï¿½jï¿½ identifiï¿½ par le processus ci-dessus afin d?eviter une remontï¿½ en double ï¿½. OK sauf que dans le 1er cas je ramene des abos et dans l?autre des consos.
		Donc il ne peut y avoir de remontï¿½ de doublon, je me trompe ?

	--->
	<cffunction access="remote" name="getMontantRessourcesHorsInventaire" returntype="array">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfreturn myFacture.getMontantRessourcesHorsInventaire()>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : forurnit la liste des ressources hors inventaire d'une facture
	
	--->
	<cffunction access="remote" name="getDetailRessourcesHorsInventaire" returntype="any">
		<cfargument name="myFacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		<cfreturn myFacture.getDetailRessourcesHorsInventaire(typeTheme)>
	</cffunction>
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 12/11/2007
		
		Description : exporte la liste des ressources hors inventaire d'une facture au format CVS
	
	--->
	<cffunction name="exporterRessourcesHICSV" access="remote" output="true" returntype="any">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="typeTheme" type="string" required="true">
		<cfreturn myfacture.exporterRessourcesHICSV(typeTheme)/>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 23/04/2008
		
		Description : eclate la facture dans une organisation détailé par ligne
	--->
	<cffunction name="detailFactureByOrga" access="remote" output="true" returntype="query">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfreturn myfacture.detailFactureByOrga_ligne(idOrga)/>
	</cffunction>
	
	<!---
		Auteur : samuel.divioka
		
		Date : 23/04/2008
		
		Description : eclate la facture dans une organisation agregé par feuille
	--->
	<cffunction name="factureByOrga" access="remote" output="true" returntype="query">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfreturn myfacture.detailFactureByOrga(idOrga)/>
	</cffunction>
	
	
	<!---
		Auteur : samuel.divioka
		
		Date : 23/04/2008
		
		Description : total de la facture dans une organisation
	--->
	<cffunction name="totalFactureByOrga" access="remote" output="true" returntype="query">
		<cfargument name="myfacture" type="Facture" required="true">
		<cfargument name="idOrga" type="numeric" required="true">		
		<cfreturn myfacture.totalFactureByOrga(idOrga)/>
	</cffunction>
	
	
	<cffunction name="formateString" access="private" returntype="string">
		<cfargument name="chaine">
		<cfset chaineTmp1 = replace(TRIM(chaine),"-"," ","all")>
		<cfset chaineTmp2 = replace(chaineTmp1,";"," ","all")>
		<cfset chaineTmp3 = replace(chaineTmp2,#chr(13)#," ","all")>
		<cfset returnedChaine = replace(chaineTmp3,#chr(10)#," ","all")>
		<cfreturn returnedChaine>
	</cffunction>
</cfcomponent>
