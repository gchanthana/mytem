package gestionparc.ihm.action.mdm.actionWypeTerm
{
	import gestionparc.ihm.action.ICommand;

	public class ActionWypeTermCommand implements ICommand
	{
		private var receiver:ActionWypeTermReceiver=null;

		public function ActionWypeTermCommand(rec:ActionWypeTermReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action()
		}
	}
}
