package univers.parametres.rago.entity
{
	[Bindable]
	public class User
	{
		public function User()
		{
		}
		
		private var _NOM:String;
		private var _PRENOM:String;
		private var _NOMPRENOM:String;
		private var _EMAIL:String;

		public function set NOM(value:String):void
		{
			_NOM = value;
		}

		public function get NOM():String
		{
			return _NOM;
		}
		public function set PRENOM(value:String):void
		{
			_PRENOM = value;
		}

		public function get PRENOM():String
		{
			return _PRENOM;
		}
		public function set NOMPRENOM(value:String):void
		{
			_NOMPRENOM = value;
		}

		public function get NOMPRENOM():String
		{
			return _NOMPRENOM;
		}
		public function set EMAIL(value:String):void
		{
			_EMAIL = value;
		}

		public function get EMAIL():String
		{
			return _EMAIL;
		}
	}
}