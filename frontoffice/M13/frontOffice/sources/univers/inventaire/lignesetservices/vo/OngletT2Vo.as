package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
	
	[Bindable]
	public class OngletT2Vo extends OngletT0Vo implements ItoXMLParams
	{
		public var CANAUX_MIXTE_CAN_DEB:String="-";
		public var CANAUX_MIXTE_CAN_FIN:String="-";
		public var CANAUX_ENTREE_CAN_DEB:String="-";
		public var CANAUX_ENTREE_CAN_FIN:String="-";
		public var CANAUX_SORTIE_CAN_DEB:String="-";
		public var CANAUX_SORTIE_CAN_FIN:String="-";
		
		
		public function OngletT2Vo()
		{
			super();
		}
		
		override public function toXMLParams(registerInLog:int=0):XML
		{
			_xmlParams = super.toXMLParams();
			XmlParamUtil.addParam(_xmlParams,"CANAUX_MIXTE_CAN_DEB","",CANAUX_MIXTE_CAN_DEB);
			XmlParamUtil.addParam(_xmlParams,"CANAUX_MIXTE_CAN_FIN","",CANAUX_MIXTE_CAN_FIN);
			XmlParamUtil.addParam(_xmlParams,"CANAUX_ENTREE_CAN_DEB","Le nombre de canaux",CANAUX_ENTREE_CAN_DEB);
			XmlParamUtil.addParam(_xmlParams,"CANAUX_ENTREE_CAN_FIN","Le nombre de canaux",CANAUX_ENTREE_CAN_FIN);
			XmlParamUtil.addParam(_xmlParams,"CANAUX_SORTIE_CAN_DEB","Le nombre de canaux",CANAUX_SORTIE_CAN_DEB);
			XmlParamUtil.addParam(_xmlParams,"CANAUX_SORTIE_CAN_FIN","Le nombre de canaux",CANAUX_SORTIE_CAN_FIN);
			
			return _xmlParams;
		}
		
	}
}