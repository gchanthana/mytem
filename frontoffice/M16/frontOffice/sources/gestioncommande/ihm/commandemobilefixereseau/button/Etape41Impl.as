package gestioncommande.ihm.commandemobilefixereseau.button
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.entity.Engagement;
	import gestioncommande.entity.RessourcesNumber;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.iinterface.AbstractWorflow;
	import gestioncommande.iinterface.IWorflow;
	import gestioncommande.popup.PopUpChoiceOptionsIHM;
	import gestioncommande.popup.PopUpChoiceSubscriptionIHM;
	import gestioncommande.popup.SelectionTypeLigneIHM;
	import gestioncommande.services.EquipementService;
	import gestioncommande.services.RessourceService;
	
	import session.SessionUserObject;
	
	
	[Bindable]
	public class Etape41Impl extends AbstractWorflow implements IWorflow
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//

		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var cboxEngagement				:ComboBox;
		
		public var dgListOptions				:DataGrid;
		
		public var boxVisible					:Box;
		
		public var btnChoisirAbo				:Button;
		public var btnChoisirOpt				:Button;
		
		//--------------------------------------------------------------------------------------------//
		//					POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private var _popUpAbo					:PopUpChoiceSubscriptionIHM;	
		
		private var _popUpOpt					:PopUpChoiceOptionsIHM;		
		
		private var _popUpType					:SelectionTypeLigneIHM;		

		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _equipements				:EquipementService = new EquipementService();
		
		private var _ressources					:RessourceService = new RessourceService();
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var  _cmd 						:Commande;
		
		private var _myElements					:ElementsCommande2	= new ElementsCommande2();
		
		private var _ressNumber					:RessourcesNumber;

		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listEngagement				:ArrayCollection = new ArrayCollection();
		
		public var strgAbo						:String = "";
		public var strgPrixAbo					:String = "";
		public var strgTypeLigne				:String = "";

		public var isFicheLigneVisible			:Boolean = false;
		public var isEnable						:Boolean = false;
		
		private var _isAboReturn				:Boolean = false;
		private var _isOptReturn				:Boolean = false;
		private var _isEngReturn				:Boolean = false;
		
		private var _idOpe						:int	= -1;
	
		//--------------------------------------------------------------------------------------------//
		//					VARIABLES BUTTON CUSSTOM
		//--------------------------------------------------------------------------------------------//
		
		public const IDPAGE						:int = 3;
		
		public var ACCESS						:Boolean = false;
		
		private var _razl						:Boolean = false;
		
	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function Etape41Impl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);
		}
		
	//--------------------------------------------------------------------------------------------//
	//					LISTENER DE L'IHM
	//--------------------------------------------------------------------------------------------//
		
		public function creationCompleteHandler(fl:FlexEvent):void
		{
			addEventListener("ERASEOPTIONS", eraseOptionsHandler);
		}
		
		public function showHandler(fl:FlexEvent):void
		{
		}

		public function viewStackChange():void
		{
			_cmd 			= SessionUserObject.singletonSession.COMMANDE;
			_myElements 	= SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			if(SessionUserObject.singletonSession.IDSEGMENT > 1)
				isFicheLigneVisible = true;

			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				if(_idOpe != _cmd.IDOPERATEUR)
				{
					_idOpe 	= _cmd.IDOPERATEUR;
					
					_ressNumber = new RessourcesNumber(_cmd.IDOPERATEUR, _cmd.IDPROFIL_EQUIPEMENT);
					
					getEngagement();
				}
			}
			else
			{
				_idOpe 	= -1;
			}
			
			if(_myElements.ABONNEMENTS.length > 0)
				boxVisible.visible = true;
			else
			{
				getAbonnementAssociated();
				getOptionsAssociated();
				
				_myElements.IDTYPELIGNE = 0;
				_myElements.TYPELIGNE 	= "";
				
				boxVisible.visible = isEnable =  false;
			}
		}
		
		public function reset():void
		{
			strgAbo = "";
			strgPrixAbo = "";
			_idOpe = -1;
			
			listEngagement.removeAll();
		}
		
		public function setConfiguration():void
		{
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				_razl = true;
				
				getEngagement();
			}
			else
				_razl = false;
		}

	//--------------------------------------------------------------------------------------------//
	//					VERIFICATION ET CONTRUCTION DES DONNÉES CHOISIES
	//--------------------------------------------------------------------------------------------//

		public function checkData():Boolean
		{
			var bool:Boolean = false;
			
			if(_myElements.ABONNEMENTS.length > 0 && _myElements.IDTYPELIGNE > 0)
				bool = true;
			
			if(_myElements.IDTYPELIGNE == 0)
			{
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_type_de_ligne__'), 'Consoview', null);
				return false;
			}
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 2)
			{
				if(bool && cboxEngagement.selectedItem != null)
					buildParameters();
				// la durée d'engagement n'est pas obligatoire
				/*else 
				{
					if(cboxEngagement.selectedItem == null)
						ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M16', 'Veuillez_s_lectionner_un_engagement__'), 'Consoview', null);
					
					return false;
				}*/
			}
						
			return bool;
		}
		
		public function buildParameters():void
		{
			_myElements.ENGAGEMENT = cboxEngagement.selectedItem as Engagement;
		}


//--------------------------------------------------------------------------------------------//
//					FUNCTION PROPRE A L'ETAPE 
//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PUBLIC
	//--------------------------------------------------------------------------------------------//
		
	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES ABONNEMENTS
		protected function btnChoisirAboClickHandler(me:MouseEvent):void
		{
			_popUpAbo 			 = new PopUpChoiceSubscriptionIHM();
			_popUpAbo.cmd 		 = _cmd;
			_popUpAbo.myElements = _myElements;
			
			_popUpAbo.addEventListener("POPUP_ABONNEMENTS_CLOSED_AND_VALIDATE", closedPopUpSubscribHandler);
			
			PopUpManager.addPopUp(_popUpAbo, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpAbo);
		}
		
		//OUVRE LA POPUP PERMETTANT DE SELECTIONNER LES OPTIONS
		protected function btnChoisirOptClickHandler(me:MouseEvent):void
		{
			_popUpOpt 			= new PopUpChoiceOptionsIHM();
			_popUpOpt.cmd 		= _cmd;
			_popUpOpt.myElements = _myElements;
			
			_popUpOpt.addEventListener("POPUP_OPTIONS_CLOSED_AND_VALIDATE", closedPopUpOptionsHandler);
			
			PopUpManager.addPopUp(_popUpOpt, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpOpt);
		}

		protected function btnFicheLigneClickHandler(me:MouseEvent):void
		{
			_popUpType 						= new SelectionTypeLigneIHM();
			_popUpType.myElements 			= _myElements;
			_popUpType.idAbonnement 		= _myElements.ABONNEMENTS[0].IDCATALOGUE;
			_popUpType.libelleAbonnement	= _myElements.ABONNEMENTS[0].LIBELLE;
			_popUpType.addEventListener("GET_TYPELIGNE", getTypeLigneHandler); 
			
			PopUpManager.addPopUp(_popUpType, this.parent.parent.parent, true);
			PopUpManager.centerPopUp(_popUpType);
		}

	//--------------------------------------------------------------------------------------------//
	//					FUNCTION - PRIVATE
	//--------------------------------------------------------------------------------------------//	

		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function getEngagement():void
		{
			_isEngReturn = false;
			
			_equipements.fournirEngagement(SessionUserObject.singletonSession.COMMANDE.IDOPERATEUR);
			_equipements.addEventListener(CommandeEvent.LISTED_ENGAGEMENTS, listEngagementHandler);
		}
		
		private function getAbonnementAssociated():void
		{
			_isAboReturn = false;
			
			_ressources.getassociatedabo(SessionUserObject.singletonSession.COMMANDE.IDPROFIL_EQUIPEMENT,
				SessionUserObject.singletonSession.COMMANDE.IDOPERATEUR);
			_ressources.addEventListener(CommandeEvent.LISTED_ABO_ASS, abonnementAssoHandler);
		}
		
		private function getOptionsAssociated():void
		{
			_isOptReturn = false;
			
			_ressources.getassociatedoptions(SessionUserObject.singletonSession.COMMANDE.IDPROFIL_EQUIPEMENT,
				SessionUserObject.singletonSession.COMMANDE.IDOPERATEUR);
			_ressources.addEventListener(CommandeEvent.LISTED_OPT_ASS, optionsAssoHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//
		
		private function listEngagementHandler(cmde:CommandeEvent):void
		{
			_isEngReturn = false;
			
			if(_equipements.listEngagement.length > 1)
				cboxEngagement.prompt = ResourceManager.getInstance().getString('M16','Choisir_la_dur_e_d_engagement');
			else
			{
				if(_equipements.listEngagement.length == 0)
					cboxEngagement.prompt = ResourceManager.getInstance().getString('M16', 'Aucun_engagement');
				else
					cboxEngagement.prompt = "";
			}
			
			cboxEngagement.dataProvider = _equipements.listEngagement;
			
			if(_razl)
				getEngegementSelected(cboxEngagement.dataProvider as ArrayCollection);
			
			checkIfOnly();
		}
		
		private function abonnementAssoHandler(cmde:CommandeEvent):void
		{
			_isAboReturn = false;
			
			var value:ArrayCollection = _ressources.ABO_ASSOCIATED;
			
			if(value.length > 0)
			{
				addToItemSelected(value, true);
				
				btnChoisirAbo.enabled = !_myElements.ABONNEMENTS[0].OBLIGATORY;
			}
			else
				btnChoisirAboClickHandler(new MouseEvent(MouseEvent.CLICK));
			
			checkIfOnly();
		}
		
		private function optionsAssoHandler(cmde:CommandeEvent):void
		{
			_isOptReturn = false;
			
			var value:ArrayCollection = _ressources.OPT_ASSOCIATED;
			
			if(value.length > 0)
				addToItemSelected(value, false);
			
			checkIfOnly();
		}
		
		private function addToItemSelected(value:ArrayCollection, isAbo:Boolean):void
		{
			if(isAbo)
			{
				_myElements.ABONNEMENTS = new ArrayCollection();
				_myElements.ABONNEMENTS.addItem(value[0]);
				
				var ref:String = (_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT)?_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT + ' | ' :'';
				strgAbo 	=  ref  + _myElements.ABONNEMENTS[0].LIBELLE;
				strgPrixAbo = _myElements.ABONNEMENTS[0].PRIX_STRG;
				
				boxVisible.visible = isEnable = true;
			}
			else
			{	
				_myElements.OPTIONS = new ArrayCollection();
				
				var len:int = value.length;
				
				for(var i:int = 0;i < len;i++)
				{
					_myElements.OPTIONS.addItem(value[i]);
				}
				
				_myElements.OPTIONS.refresh();
			}
		}
		
		private function getEngegementSelected(values:ArrayCollection):void
		{
			var duree	:int = int(SessionUserObject.singletonSession.CURRENTCONFIGURATION.ENGAGEMENT.VALUE);
			var len		:int = values.length;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].VALUE == duree)
				{
					cboxEngagement.selectedIndex = i;
					break;
				}
			}
			
			addLastInfosRessources();
			
			_razl = false;
		}
		
		private function addLastInfosRessources():void
		{
			var ref:String = (_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT)?_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT + ' | ' :'';
			strgAbo 	=  ref  + _myElements.ABONNEMENTS[0].LIBELLE;
			strgPrixAbo 	= _myElements.ABONNEMENTS[0].PRIX_STRG;
			strgTypeLigne 	= _myElements.TYPELIGNE;
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENER
		//--------------------------------------------------------------------------------------------//
	
		private function eraseOptionsHandler(e:Event):void
		{
			if(dgListOptions.selectedItem != null)
			{
				var id	:int = dgListOptions.selectedItem.IDPRODUIT;
				var len	:int = _myElements.OPTIONS.length;
				var idx	:int = 0;
				
				for(var i:int = 0;i < len;i++)
				{
					if(_myElements.OPTIONS[i].IDPRODUIT == id)
						idx = i;
				}
				
				_myElements.OPTIONS.removeItemAt(idx);
			}
		}

		//--------------------------------------------------------------------------------------------//
		//					LISTENER - POPUPs
		//--------------------------------------------------------------------------------------------//
		
		private function getTypeLigneHandler(e:Event):void
		{
			strgTypeLigne = _myElements.TYPELIGNE;
			
			PopUpManager.removePopUp(_popUpType);
		}
		
		//---> AFFECTE LES OPTIONS SELECTIONNEES A '_myElements.ABO' ET 
		//---> AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos abonnements'
		private function closedPopUpSubscribHandler(e:Event = null):void
		{
			if(_myElements.ABONNEMENTS.length > 0)
			{
				var ref:String = (_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT)?_myElements.ABONNEMENTS[0].REFERENCE_PRODUIT + ' | ' :'';
				strgAbo 	=  ref  + _myElements.ABONNEMENTS[0].LIBELLE;
				strgPrixAbo = _myElements.ABONNEMENTS[0].PRIX_STRG;
				
				boxVisible.visible = isEnable = true;
			}
			else
			{
				boxVisible.visible = isEnable = false;
				_myElements.IDTYPELIGNE = 0;
				_myElements.TYPELIGNE 	= '';
			}
		}		
		
		//---> AFFECTE LES OPTIONS SELECTIONNEES A '_myElements.OPTIONS' ET 
		//---> AFFICHE LE DATAGRID LORS DE LA FERMETURE DE LA POPUP 'Sélectionnez vos options'
		private function closedPopUpOptionsHandler(e:Event = null):void
		{
			dgListOptions.dataProvider = _myElements.OPTIONS;
			(dgListOptions.dataProvider as ArrayCollection).refresh();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTIONS
		//--------------------------------------------------------------------------------------------//
		
		private function checkIfOnly():void
		{
			var isPass:Boolean = false;
			
			if(_isAboReturn && _isOptReturn && _isEngReturn)
			{
				if(_ressNumber.NB_ABO == 1)
				{
					isPass = true;
				}
				else if(_ressNumber.NB_ABO > 1 && _ressNumber.NB_ABO_OBL > 0)
					{
						isPass = true;
					}
				
				btnChoisirAbo.enabled = !isPass;
				
				if(isPass)
				{
					if(_ressNumber.NB_OPT == 0 || _ressNumber.NB_OPT == _ressNumber.NB_OPT_OBL)
						isPass = true;
					else
						isPass = false;
				}
				
				btnChoisirOpt.enabled = !isPass;
			}
		}

	}
}