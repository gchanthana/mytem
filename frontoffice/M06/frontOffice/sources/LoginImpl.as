package
{
	import composants.HintTextInput;
	
	import entity.CodeLangue;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import interfaceClass.IModuleLogin;
	
	import modulelogin.ihm.retrievepassword.RetrievePasswordIHM;
	
	import mx.containers.Canvas;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;

	public class LoginImpl extends Canvas
	{
		private static const default_code_langue :String = "fr_FR";
		
		private var currentlogin		:String;		
		private var infosMessages		:String;
		private var code_langue			:String = default_code_langue;
		private var idLang				:int = 3;
		private var messColor			:int;
		public var _appliVersion		:String;
		public var _infosMessage		:String;
		private var passwordPopup		:RetrievePasswordIHM;
		private var sendMailOp			:AbstractOperation;
		
		public var loginText		:HintTextInput;
		public var pwdText			:HintTextInput;
		public var btnAcrobat		:Image;
		public var btnPwd			:Label;
		public var stdConnectButton	:Button;
		public var btnFR 			:Image;
		public var btnES 			:Image;
		public var btnGB 			:Image;
		public var btnUS 			:Image;
		public var btnEU 			:Image;
		public var btnNL 			:Image;
		public var selectedflag		:Image;
		[Bindable]
		public var ckxSeSouvenir	:CheckBox;
		
		public function LoginImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, afterCreationComplete);
		}
		public function setInformations(appliVersion:String, infosMessage:String):void{
			this._appliVersion = appliVersion;
			this._infosMessage = infosMessage;
		}
		
		public function setcurrentlogin(curLogin:String = ""):void
		{
			this.currentlogin = curLogin;
		}
		public function setInfosMessages(str:String = ""):void
		{
			this.infosMessages = str;
		}
		
		private function afterCreationComplete(event:FlexEvent):void
		{
			trace("(Login) Performing IHM Initialization ****");
			loginText.text = currentlogin;
			pwdText.text = "";
			
			// cursor focus
			if((loginText.text).length == 0){
				loginText.setFocus();
			}else{
				pwdText.setFocus();
			}
			
			//listeners for actions
			btnAcrobat.addEventListener(MouseEvent.CLICK, getAcrobat);
			btnPwd.addEventListener(MouseEvent.CLICK, askPwd);
			stdConnectButton.addEventListener(MouseEvent.CLICK, connectItemHandler);
			this.addEventListener(KeyboardEvent.KEY_DOWN, connectItemHandler);
			activeControls(true);
		}
		
		/*
		*	--------------------	CONNECTION		-----------------------------------
		*/
		private function connectItemHandler(event:Event):void
		{
			if(event is KeyboardEvent)
			{
				if((event as KeyboardEvent).keyCode == 13)
				{
					if ((loginText.text.length > 0) || pwdText.text.length > 0)
						letsCOnnect()
				}
			}
			else if(event is MouseEvent) 
			{
				if ((loginText.text.length == 0) || pwdText.text.length == 0)
				{
					var myAlert:Alert = Alert.show(ResourceManager.getInstance().getString('M06', 'Acc_s_Refus____Login_ou_Mot_de_passe_invalide_s_'), ResourceManager.getInstance().getString('M04', 'Connexion'));
					PopUpManager.centerPopUp(myAlert);
				}
				else
					letsCOnnect()
			}
		}
		private function letsCOnnect():void
		{
			
			var cdeLg:CodeLangue = new CodeLangue("fr_FR","€","french",3,"FR");
			var objLogin:Object = new Object();
			objLogin.login = loginText.text;
			objLogin.mdp = pwdText.text;
			objLogin.codeLangue = cdeLg;
			if(ckxSeSouvenir != null)
				objLogin.boolSeSouvenir = ckxSeSouvenir.selected;
			else
				objLogin.boolSeSouvenir = false;
			
			(parentDocument as IModuleLogin).validateLogin(objLogin);
		}
		
		private function getAcrobat(event:Event):void
		{
			var url:String = "http://www.adobe.com/fr/products/acrobat/readstep2.html";
			var variables:URLVariables = new URLVariables();
			var request:URLRequest = new URLRequest(url);
			request.method = "GET";
			navigateToURL(request, "_blank");
		}
		
		private function askPwd(event:Event):void
		{
			passwordPopup = new RetrievePasswordIHM();
			PopUpManager.addPopUp(passwordPopup, this, true);
			PopUpManager.centerPopUp(passwordPopup);
			trace("#### askPwd ");
			if (loginText.text != "")
			{
				passwordPopup.tiEmail.text = loginText.text;
			}
		}
		/*
		*	--------------------	FONCTIONS UTILES 	-----------------------------------
		*/
		private function activeControls(state:Boolean):Boolean
		{
			loginText.enabled = pwdText.enabled = stdConnectButton.enabled = btnPwd.enabled = state;
			return state;
		}
		
	}
}