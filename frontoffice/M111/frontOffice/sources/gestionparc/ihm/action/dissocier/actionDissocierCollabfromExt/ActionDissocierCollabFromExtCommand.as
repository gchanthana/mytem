package gestionparc.ihm.action.dissocier.actionDissocierCollabfromExt
{
	import gestionparc.ihm.action.ICommand;

	public class ActionDissocierCollabFromExtCommand implements ICommand
	{
		private var receiver:ActionDissocierCollabFromExtReceiver=null;

		public function ActionDissocierCollabFromExtCommand(rec:ActionDissocierCollabFromExtReceiver):void
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
