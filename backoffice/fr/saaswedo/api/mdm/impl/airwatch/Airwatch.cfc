<cfcomponent author="Cedric" extends="fr.saaswedo.api.mdm.impl.MdmServer">
	<cffunction access="public" name="LOCKID" returntype="String" hint="Retourne fr.saaswedo.api.mdm.impl.airwatch.AirWatch">
		<cfreturn "fr.saaswedo.api.mdm.impl.airwatch.Airwatch">
	</cffunction>
	
	<cffunction access="public" name="checkConnection" returntype="Struct" hint="Vérifie la connexion avec le serveur MDM et retourne une structure
	contenant :<br>- IS_TEST_OK: TRUE si la connexion en succès<br>- TEST_MSG: Message explicatif lié à la vérification de la connexion">
		<cfset var checkResult={IS_TEST_OK=createObject("java","java.lang.Boolean").init("false"),TEST_MSG="NONE"}>
		<cfset var httpResponse={}>
		<cftry>
			<cfset var infos=getMdmServerInfo()>
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset var service={
				endpoint=infos.getEndpoint() & "/api/v1/help",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfset service.HTTP_METHOD=remoting.HTTP_GET()>
			<!--- Spécifique au endpoint de test de connexion : Attention la MimeType de la réponse est quand meme "text/html" --->
			<cfset var params=[
				{type="header",name="accept",value="application/xml"},
				{type="header",name="User-Agent",value="SaaSwedo"},
				{type="header",name="aw-tenant-code",value=infos.getToken()}
			]>
			<cfset var result=remoting.invoke(service,"",params)>
			<!--- Erreur durant le reporting --->
			<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
				<cfset var errorUUID=createUUID()>
				<cfset checkResult.TEST_MSG="Connection test failed (MNT_DETAIL.UUID: #errorUUID#)">
				<cfset storeError({UUID=errorUUID,EXCEPTION=result[remoting.RESULT()],CLIENT_REQUEST={service=service,params=params}})>
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset var httpCode=remoting.getResponseHttpCode(result)>
				<cfset httpResponse=result[remoting.RESULT()]>
				<!--- Code HTTP d'erreur --->
				<cfif httpCode NEQ 200>
					<cfset checkResult.TEST_MSG=httpResponse.statusCode>
					<cfset storeError({
						UUID=createUUID(),EXCEPTION=result[remoting.RESULT()],SERVER_RESPONSE=httpResponse,CLIENT_REQUEST={service=service,params=params}
					})>
				<cfelse>
					<cfset checkResult.IS_TEST_OK=createObject("java","java.lang.Boolean").init("true")>
					<cfset checkResult.TEST_MSG=httpResponse.statusCode>
				</cfif>
			</cfif>
			<cfreturn checkResult>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfif isDefined("service") ANd (NOT structIsEmpty(service))>
					<cfif NOT structKeyExists(errorLog,"CLIENT_REQUEST")>
						<cfset errorLog.CLIENT_REQUEST={}>
					</cfif>
					<cfset errorLog.CLIENT_REQUEST.service=service>
				</cfif>
				<cfif isDefined("params") ANd (NOT arrayIsEmpty(params))>
					<cfif NOT structKeyExists(errorLog,"CLIENT_REQUEST")>
						<cfset errorLog.CLIENT_REQUEST={}>
					</cfif>
					<cfset errorLog.CLIENT_REQUEST.params=params>
				</cfif>
				<cfset storeError(errorLog)>
				<cfreturn checkResult>
			</cfcatch>
		</cftry>
		<!---
		<cfset var checkResult={IS_TEST_OK=createObject("java","java.lang.Boolean").init("false"),TEST_MSG="NONE"}>
		<cftry>
			<cfset var httpResponse={}>
			<cfset var remoting=new fr.saaswedo.utils.remoting.HttpClient()>
			<cfset var infos=getMdmServerInfo()>
			<cfset var service={
				endpoint=infos.getEndpoint() & "/api/v1/sm/authentication",username=infos.getUsername(),password=infos.getPassword()
			}>
			<cfset var params=[
				{type="header",name="accept",value="application/json"},
				{TYPE="formfield",NAME="username",VALUE=service.username},
				{TYPE="formfield",NAME="Password",VALUE=service.password}
			]>
			<cfset service.HTTP_METHOD=remoting.HTTP_POST()>
			<cfset var result=remoting.invoke(service,"",params)>
			<!--- Erreur durant le reporting --->
			<cfif result[remoting.STATUS()] EQ remoting.ERROR()>
				<cfset storeError({UUID=createUUID(),EXCEPTION=result[remoting.RESULT()]})>
			<cfelseif result[remoting.STATUS()] EQ remoting.DEFINED()>
				<cfset var httpCode=remoting.getResponseHttpCode(result)>
				<cfset httpResponse=result[remoting.RESULT()]>
				<!--- Réponse HTTP du remoting : Utilisé pour le log de warning plus bas --->
				<cfset device.RESULT=result[remoting.RESULT()]>
				<!--- La connexion est en succès si le code HTTP est 200 ou 401 (username/password invalide)--->
				<cfif (httpCode EQ 200) OR (httpCode EQ 401)>
					<cfset checkResult.IS_TEST_OK=createObject("java","java.lang.Boolean").init("true")>
				<!--- La connexion est en echec sinon (e.g 404,500,etc...) --->
				<cfelse>
					<cfset storeError({UUID=createUUID(),SERVER_RESPONSE=httpResponse})>
					<cfif structKeyExists(httpResponse,"errorDetail")>
						<cfset checkResult.TEST_MSG=httpResponse.errorDetail>
					<cfelseif structKeyExists(httpResponse,"statusCode")>
						<cfset checkResult.TEST_MSG=httpResponse.statusCode>
					<cfelse>
						<cfset checkResult.TEST_MSG="MDM server returned HTTP Code : #httpCode#">
					</cfif>
				</cfif>
			</cfif>
			<cfreturn checkResult>
			<cfcatch type="Any">
				<cfset var errorLog={UUID=createUUID(),EXCEPTION=CFCATCH}>
				<cfset checkResult.TEST_MSG=CFCATCH.message>
				<cfif NOT structIsEmpty(httpResponse)>
					<cfset errorLog.SERVER_RESPONSE=httpResponse>
				</cfif>
				<cfset storeError(errorLog)>
				<cfreturn checkResult>
			</cfcatch>
		</cftry>
		--->
	</cffunction>
	
	<cffunction access="public" name="PROVIDER" returntype="String" hint="Retourne Airwatch">
		<cfreturn "Airwatch">
	</cffunction>

	<cffunction access="public" name="getEnrollmentURL" returntype="String" hint="Retourne le endpoint MDM si aucune URL ne correspond aux paramètres">
		<cfargument name="osFamily" type="String" required="true" hint="OS du device">
		<cfargument name="requireAgent" type="Boolean" required="false" default="true" hint="TRUE pour un enrollment qui requiert un agent MDM">
		<!--- TODO: A implémenter --->
		<cfthrow type="Custom" message="#componentName()#.getEnrollmentURL() is not supported for this version for this provider">
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentServer" returntype="String" hint="Retourne le DNS ou l'IP du serveur d'enrollment">
		<cfset var mdmInfos=getMdmServerInfo()>
		<cfset var endpoint=mdmInfos.getEndpoint()>
		<cfset var mdmServerURL=createObject("java","java.net.URL").init(endpoint)>
		<cfreturn mdmServerURL.getHost()>
	</cffunction>
	
	<cffunction access="public" name="getEnrollmentServerPort" returntype="Numeric" hint="Retourne le numéro de port du serveur d'enrollment">
		<!--- TODO: A implémenter --->
		<cfthrow type="Custom" message="#componentName()#.getEnrollmentServerPort() is not supported for this version for this provider">
	</cffunction>

	<!--- =========== Fonctions de gestion des services du serveur =========== --->
	
	<!--- La fonction init() qui appelle cette fonction avec un verrou CFLOCK exclusif --->
	<cffunction access="private" name="initMdmServices" returntype="void" hint="Instancie et initialise les implémentations des services">
		<!--- API d'accès aux infos des devices --->
		<cfset setIDeviceInfo(new fr.saaswedo.api.mdm.impl.airwatch.DeviceInfo(THIS))>
		<!--- API de gestion des devices --->
		<cfset setIDeviceManagement(new fr.saaswedo.api.mdm.impl.airwatch.DeviceManagement(THIS))>
	</cffunction>
</cfcomponent>