package univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile
{
	import flash.events.MouseEvent;
	
	import mx.controls.Label;
	import mx.events.FlexEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	
	
	public class ButtomMenu extends ButtomMenuIHM
	{
		private var msgTooltip : String ;
		private var isEnabled : Boolean;
		private var lacible : Label;
		private var texteItem : String;
		public function ButtomMenu(){
			
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		private function initIHM(fe : FlexEvent):void
		{
			this.addEventListener(MouseEvent.ROLL_OVER,rollOverHandler);
			this.addEventListener(MouseEvent.ROLL_OUT,rollOutHandler);
		}
		
		
		public function initParametre(actif : Boolean,msgTooltip : String=""):void{
			
			setEnabled(actif);
			MsgTooltip = msgTooltip;
		}
		
		
		public function set cibleMsgTooltip(cibleMsgTooltip : Label):void
		{
			lacible = cibleMsgTooltip;
		}
		
		public function set MsgTooltip(msg : String):void
		{
			msgTooltip = msg;
		}
		
		public function get MsgTooltip():String
		{
			if(isEnabled==true)
			{
				return msgTooltip +""+ PanelGestionFoltteMobile.texteItem ;
			}
			else
			{
				return "Cette action est désactivée";
			}
		}
		public function setEnabled(val : Boolean):void{
			this.isEnabled=val;	
			if (isEnabled==true)
			{
				enabled=true;
			}
			else
			{
				enabled=false;
			}
			
		}
		override protected   function rollOverHandler(ev : MouseEvent):void{
			super.rollOverHandler(ev);
			lacible.text = MsgTooltip;
			
		}
		override protected  function rollOutHandler(ev : MouseEvent):void{
			super.rollOutHandler(ev);
			lacible.text = "";
		}
		public function setPersonnaliseBt(texteItem : String):void{
			
			this.texteItem=(texteItem != null)?texteItem:"";
		}
		
		
	}
}