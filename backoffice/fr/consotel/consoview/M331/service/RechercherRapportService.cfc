﻿<!-- displayname : le nom de composant -->

<cfcomponent displayname="Modules" output="false">

	<!-- déclarer une fonction  name : le nom de la fonction ;returntype : le type du resultat retourné -->
	<!-- access : visibilité -->

	<cffunction access="remote" output="false" name="getRapport" returntype="query">

			<!-- les arguements de la fonction ; name: le nom de l'arguement; required="true": obligatoire -->
			<!-- plusieurs arguements = plusieurs balise -->
			<!-- on peut donner des noms differents des noms utilisé dans la fonction flex,  mais il faut garder l'order' -->
			<cfargument name="idModule" type="numeric" required="true">
			<cfargument name="text_filtre" type="String" required="true">
			<cfargument name="segment" type="String" required="true">
			<cfargument name="typeRapport" type="numeric" required="true">
			<cfargument name="listeTypeAnalyse" type="Array" required="true">
			<cfargument name="listeTypeAgregation" type="Array" required="true">
			<cfargument name="indexDepart" type="numeric" required="true">
			<cfargument name="showAll" type="numeric" required="true">

			<cfset stringAnalyse=''>

			<cfset nbItemParPage=10>

			<cfset stringAgreag=''>
			<cfloop from="1" to="#ArrayLen(listeTypeAnalyse)#" index="i" step="1">
				<cfset cleAnalyse=ARGUMENTS["listeTypeAnalyse"][i]["selected"] />

				 <cfif cleAnalyse eq "YES">
					<cfset value="#i#">
					<cfelse>
					   <cfset value="0">
				 </cfif>
				<cfset stringAnalyse=stringAnalyse & value >
		    </cfloop>
			<cfloop from="1" to="#ArrayLen(listeTypeAgregation)#" index="i" step="1">

				<cfset cleAgregation=ARGUMENTS["listeTypeAgregation"][i]["selected"] />

				<cfif cleAgregation eq "YES">
					<cfset value="#i#">
					<cfelse>
					   <cfset value="0">
				 </cfif>
				<cfset stringAgreag=stringAgreag & value >
		    </cfloop>
			<!-- déclarer des varaibles pour passer au procédure -->
			<!-- cette décalration n'est pas obligatoire et dépend de procédure' -->

			<cfset iduser   = SESSION.USER.CLIENTACCESSID>
			<cfset idRacine = SESSION.PERIMETRE.ID_GROUPE> <!--- l'id de l'user conncté  --->
			<cfset codeApp  = SESSION.CODEAPPLICATION>
			<cfset idLang  	= SESSION.USER.IDGLOBALIZATION>
			<cfset currentModule  =  SESSION.CURRENT_MODULE>

			<!-- dataSource: obligatoires - nom de la source de données qui pointe vers la base de données qui contient une procédure stockée. -->
			<!-- procédure : Obligatoires nom de la procédure stockée sur le serveur de base de données.-->

            <cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M331.get_rapport_racine_v2">

				 <!-- paramtères : Si la procédure stockée utilise des paramètres d'entrée ou de sortie -->
				 <!-- ces paramètres  dépendent de la procedure -->
				  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRacine#" variable="p_idracine">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#iduser#" variable="p_app_loginid">
				  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#codeApp#" variable="p_application_consotelid">
				  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idModule#"  variable="p_module_consotelid">
                  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#typeRapport#" variable="p_type_rapport ">
                  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#segment#" variable="p_segment_rapport ">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#stringAnalyse#" variable="p_idrapport_periodicite">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#stringAgreag#" variable="p_type_aggreg">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#text_filtre#" variable="p_texte_filtre">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#indexDepart#" variable="p_index_debut ">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#nbItemParPage#" variable="p_number_of_records">
				  <cfprocparam type="in" cfsqltype="CF_SQL_VARCHAR" value="#idLang#" variable="p_langid">
				  <cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#showAll#" variable="P_show_all">


                  <cfprocresult name="qRapports"><!-- Si la procédure stockée retourne un résultat -->
            </cfstoredproc>
            <cfreturn qRapports><!-- le resultat de la procedure-->
      </cffunction>

</cfcomponent>