package service.destinataires.destinataireprofil
{
	import mx.resources.ResourceManager;
	import composants.util.ConsoviewAlert;
	import event.DestinataireEvent;
	import flash.events.EventDispatcher;
	import mx.rpc.events.ResultEvent;
	

	internal class DestinataireProfilModel  extends EventDispatcher
	{
		
		public function DestinataireProfilModel()
		{
		}
				
		internal function getResultHandler(re:ResultEvent):void
		{
			if(re.result==1)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Votre_profil_a_bien__t__associ__au_desti'));
				this.dispatchEvent(new DestinataireEvent(DestinataireEvent.LISTE_DESTINATAIRE_PROFIL_EVENT));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'Une_erreur_s_est_produit_lors_de_l_assoc'),ResourceManager.getInstance().getString('M22', 'Erreur_association_destinataire_profil'));
			}
		}
		
		internal function getRemoveResultHandler(re:ResultEvent):void
		{
			if(re.result)
			{
				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M22', 'Votre_profil_a_bien__t__dissoci__du_dest'));
				this.dispatchEvent(new DestinataireEvent(DestinataireEvent.DELETE_DESTINATAIRE_PROFIL_EVENT));
			}
			else
			{
				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M22', 'Une_erreur_s_est_produit_lors_de_la_d_sa'),ResourceManager.getInstance().getString('M22', 'Erreur_destinataire_profil'));
			}
		}
	}
}