package service.news
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;

	public class GetNewsService
	{	
		private var _model:GetNewsModel;
		public var handler:GetNewsHandler;
		private var remote:RemoteObject;
	
		public function GetNewsService()
		{
			this._model = new GetNewsModel();
			this.handler = new GetNewsHandler(_model);
		}		
		
		public function get model():GetNewsModel
		{
			return this._model;
		}
		
		public function getListNews():void
		{		
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);	
			remote.source="fr.consotel.consoview.M54.news.GetListNews";
			remote.addEventListener(ResultEvent.RESULT,handler.getListNewsResultHandler,false,0,true);
			remote.getListNews();
		}
	}
}