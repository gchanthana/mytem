package gestionparc.entity
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class GpDroitUserVO
	{
		private var _DROIT_MOBILE:Boolean=false;
		private var _DROIT_FIXE:Boolean=false;
		private var _DROIT_COMPTE:Boolean=false;
		private var _DROIT_VUES_CONSO:Boolean=false;
		private var _DROIT_VUES_ALERTE:Boolean=false;
		private var _DROIT_VUES_MOBILE:Boolean=false;
		private var _DROIT_VUES_FIXE:Boolean=false;

		public function GpDroitUserVO()
		{
		}

		public function set DROIT_MOBILE(value:Boolean):void
		{
			_DROIT_MOBILE=value;
		}

		public function get DROIT_MOBILE():Boolean
		{
			return _DROIT_MOBILE;
		}


		public function set DROIT_FIXE(value:Boolean):void
		{
			_DROIT_FIXE=value;
		}

		public function get DROIT_FIXE():Boolean
		{
			return _DROIT_FIXE;
		}


		public function set DROIT_COMPTE(value:Boolean):void
		{
			_DROIT_COMPTE=value;
		}

		public function get DROIT_COMPTE():Boolean
		{
			return _DROIT_COMPTE;
		}


		public function set DROIT_VUES_CONSO(value:Boolean):void
		{
			_DROIT_VUES_CONSO=value;
		}

		public function get DROIT_VUES_CONSO():Boolean
		{
			return _DROIT_VUES_CONSO;
		}


		public function set DROIT_VUES_ALERTE(value:Boolean):void
		{
			_DROIT_VUES_ALERTE=value;
		}

		public function get DROIT_VUES_ALERTE():Boolean
		{
			return _DROIT_VUES_ALERTE;
		}


		public function set DROIT_VUES_MOBILE(value:Boolean):void
		{
			_DROIT_VUES_MOBILE=value;
		}

		public function get DROIT_VUES_MOBILE():Boolean
		{
			return _DROIT_VUES_MOBILE;
		}


		public function set DROIT_VUES_FIXE(value:Boolean):void
		{
			_DROIT_VUES_FIXE=value;
		}

		public function get DROIT_VUES_FIXE():Boolean
		{
			return _DROIT_VUES_FIXE;
		}

		/* FONCTION PUBLICS */
		public function cloneParProcedure(obj:Object):void
		{
			if ((obj as ArrayCollection).getItemAt(0).hasOwnProperty("DROIT_COMPTE") && (obj as ArrayCollection).getItemAt(0).DROIT_COMPTE == "1")
				this.DROIT_COMPTE=true;
			else
				this.DROIT_COMPTE=false;

			if ((obj as ArrayCollection).getItemAt(0).hasOwnProperty("DROIT_FIXE") && (obj as ArrayCollection).getItemAt(0).DROIT_FIXE == "1")
				this.DROIT_FIXE=true;
			else
				this.DROIT_FIXE=false;
			if ((obj as ArrayCollection).getItemAt(0).hasOwnProperty("DROIT_MOBILE") && (obj as ArrayCollection).getItemAt(0).DROIT_MOBILE == "1")
				this.DROIT_MOBILE=true;
			else
				this.DROIT_MOBILE=false;

			if ((obj as ArrayCollection).getItemAt(0).hasOwnProperty("DROIT_VUE_ALERTE") && (obj as ArrayCollection).getItemAt(0).DROIT_VUE_ALERTE == "1")
				this._DROIT_VUES_ALERTE=true;
			else
				this.DROIT_VUES_ALERTE=false;

			if ((obj as ArrayCollection).getItemAt(0).hasOwnProperty("DROIT_VUE_CONSO") && (obj as ArrayCollection).getItemAt(0).DROIT_VUE_CONSO == "1")
				this.DROIT_VUES_CONSO=true;
			else
				this.DROIT_VUES_CONSO=false;

			if ((obj as ArrayCollection).getItemAt(0).hasOwnProperty("DROIT_VUE_FIXE") && (obj as ArrayCollection).getItemAt(0).DROIT_VUE_FIXE == "1")
				this.DROIT_VUES_FIXE=true;
			else
				this.DROIT_VUES_FIXE=false;

			if ((obj as ArrayCollection).getItemAt(0).hasOwnProperty("DROIT_VUE_MOBILE") && (obj as ArrayCollection).getItemAt(0).DROIT_VUE_MOBILE == "1")
				this.DROIT_VUES_MOBILE=true;
			else
				this.DROIT_VUES_MOBILE=false;
		}
	}
}
