<cfcomponent displayname="Interface de communication avec consoprod" hint="API de communication" output="false">
	<cfset OFFREDSN = "ROCOFFRE">
	
	<cffunction name="containerListClientCV" returntype="query" access="remote">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.containerListClientCV">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerlistuserbyracine" returntype="query" access="remote">
		<cfargument name="idracine" type="Numeric" required="true">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.containerlistuserbyracine">
			<cfprocparam value="#idracine#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListRapport" returntype="query" access="remote">
		<cfargument name="iduser" 		type="Numeric">
		<cfargument name="type" 		type="Numeric">
		<cfargument name="search" 		type="string">
		<cfargument name="module" 		type="string">
		<cfargument name="nbstart" 		type="Numeric">
		<cfargument name="nbaffiche" 	type="Numeric">
		<cfargument name="idracine" 	type="Numeric">
		
		<cftrace text="pkg_m61.containerListRapport_V2">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.containerListRapport_V2">
			<cfprocparam value="#iduser#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#type#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#search#" 		cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#module#" 		cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#nbstart#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#nbaffiche#" 	cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#" 	cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="fr_FR" 	cfsqltype="CF_SQL_varCHAR" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListRapport_V2" returntype="query" access="remote">
		<cfargument name="iduser" 		type="Numeric">
		<cfargument name="type" 		type="Numeric">
		<cfargument name="search" 		type="string">
		<cfargument name="module" 		type="string">
		<cfargument name="nbstart" 		type="Numeric">
		<cfargument name="nbaffiche" 	type="Numeric">
		<cfargument name="idracine" 	type="Numeric">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m62.containerListRapport_V4">
			<cfprocparam value="#iduser#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#type#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#search#" 		cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#module#" 		cfsqltype="cf_SQL_VARCHAR" type="in">
			<cfprocparam value="#nbstart#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#nbaffiche#" 	cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#" 	cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="fr_FR" 	cfsqltype="CF_SQL_varCHAR" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListModuleByUser" access="remote" returntype="query">
		<cfargument name="iduser" type="numeric">
		<cfargument name="idracine" type="numeric">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.containerListModuleByUser">
			<cfprocparam value="#iduser#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#idracine#"		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerListComments" returntype="query" access="remote">
		<cfargument name="iddoc" type="Numeric">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.containerListComments">
			<cfprocparam value="#iddoc#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="containerAddComments" returntype="numeric" access="remote">
		<cfargument name="iddoc" 		type="Numeric">
		<cfargument name="iduser" 		type="Numeric">
		<cfargument name="commentaire"	type="string">
		<cfargument name="date" 		type="string">
		<cfargument name="heure" 		type="string">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.containerAddComments">
			<cfprocparam value="#iddoc#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#iduser#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam value="#commentaire#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#date#" 		cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam value="#heure#" 		cfsqltype="CF_SQL_VARCHAR" type="in">
			<cfprocparam variable="p_retour" 	cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
<!---  ENVOI UN MAIL A UN CLIENT LE NOTIFIANT QUE DES DOCUMENTS ONT ETE AJOUTE A SON CONTAINER --->
	<cffunction name="SENDMailPublication" returntype="Numeric" access="remote">
		<cfargument name="IDUSER" 	type="numeric"/>
		<cfargument name="IDRACINE" type="numeric"/>
		<cfargument name="infosDoc" type="Struct"/>
		
	 	<cfset api = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
		<cfset strRight = api.KNOWMailRight(IDUSER,IDRACINE,"GENERE")> 
		
 		<cfif strRight eq 1>
			<cfset mailFactory	= 	createObject("component","fr.consotel.consoview.api.container.mail.SENDMailPublicationParProductionClient")>
			<cfset mailSend 	=	mailFactory.sendMail(infosDoc)>
			<cfreturn mailSend>
		<cfelse>
			<cfreturn -1>
		</cfif> 
	</cffunction>
	<cffunction name="containerPublishRapportConsotel" returntype="String" access="remote" hint="publie un document dans la partie autre document">
		<cfargument name="NOM_DOC" 				type="string" 	required="true" 					hint="NOM LONG DU DOCUMENT (avec espace)"> 
		<cfargument name="SHORT_NAME" 			type="string" 	required="true" 					hint="NOM COURT DU DOCUMENT (sans espace)"> 
		<cfargument name="JOB_ID" 				type="numeric" 	required="false" 	default="-1" 	hint="ID DU JOB DANS BI"> 
		<cfargument name="CODE_RAPPORT"			type="string" 	required="false" 	default=""		hint="CODE DU DOCUMENT"> 
		<cfargument name="APP_LOGINID" 			type="numeric" 	required="false" 	default="-1"	hint="ID DE L'UTILISATEUR CONNECTE"> 
		<cfargument name="IDRACINE" 			type="numeric" 	required="false" 	default="-1"	hint="ID RACINE"> 
		<cfargument name="NOM_MODULE" 			type="string" 	required="false" 	default=""		hint="MODULE D'OU A ETE DEMANDE LE DOCUMENT"> 
		<cfargument name="FORMAT" 				type="string"	required="false" 	default=""		hint="FORMAT DU DOCUMENT"> 
		<cfargument name="DATE" 				type="string" 	required="false" 	default=""		hint="DATE DE PLANIFICATION DU JOB SOUS BI"> 
		<cfargument name="SERVER_BI" 			type="string" 	required="false" 	default=""		hint="NOM DU SERVER BI"> 
		<cfargument name="APPLI" 				type="string" 	required="false" 	default=""		hint="ID DE L'APPLI"> 
		<cfargument name="IS_PUBLIC" 			type="boolean" 	required="false" 	default="0"		hint="JE SAIS PAS"> 
		<cfargument name="STATUS" 				type="numeric" 	required="false" 					hint="LE STATUS DU DOCUMENT => VALEUR POSSIBLE : 2 DISPONIBLE, 3 ERREUR, 1 ENCOURS">
		<cfargument name="TAILLE" 				type="numeric" 	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT NON ARCHIVE">
		<cfargument name="TAILLEZIP" 			type="numeric"	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT ARCHIVE (.ZIP)">
		
		<cflog text="************** CONTAINER ************ nom_doc = #NOM_DOC#">
		<cflog text="************** CONTAINER ************ format = #FORMAT#">
		<cflog text="************** CONTAINER ************ apploginid = #APP_LOGINID#">
		<cflog text="************** CONTAINER ************ IDRACINE = #IDRACINE#">
		
		
		<cfset uuid = CreateUuid()>
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.PublishRappConsotel_v2">
				<cfprocparam value="#uuid#"					cfsqltype="CF_SQL_VARCHAR"  type="in">			
				<cfprocparam value="#JOB_ID#" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#NOM_DOC#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#SHORT_NAME#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#CODE_RAPPORT#" 		cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="641" 					cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="2458788" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#NOM_MODULE#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#FORMAT#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#DATE#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#SERVER_BI#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#APPLI#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#IS_PUBLIC#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#TAILLE#" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#TAILLEZIP#" 			cfsqltype="CF_SQL_INTEGER" 	type="in">
				<cfprocparam value="#STATUS#" 				cfsqltype="cf_sql_INTEGER" 	type="in">
				<cfprocparam value="#APP_LOGINID#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam variable="p_retour" 			cfsqltype="CF_SQL_VARCHAR"  type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

	<cffunction name="ADDDocumentTableContainer" returntype="String" access="remote">
		<cfargument name="NOM_DOC" 				type="string" 	required="true" 					hint="NOM LONG DU DOCUMENT (avec espace)"> 
		<cfargument name="SHORT_NAME" 			type="string" 	required="true" 					hint="NOM COURT DU DOCUMENT (sans espace)"> 
		<cfargument name="JOB_ID" 				type="numeric" 	required="false" 	default="-1" 	hint="ID DU JOB DANS BI"> 
		<cfargument name="CODE_RAPPORT"			type="string" 	required="false" 	default=""		hint="CODE DU DOCUMENT"> 
		<cfargument name="APP_LOGINID" 			type="numeric" 	required="false" 	default="-1"	hint="ID DE L'UTILISATEUR CONNECTE"> 
		<cfargument name="IDRACINE" 			type="numeric" 	required="false" 	default="-1"	hint="ID RACINE"> 
		<cfargument name="NOM_MODULE" 			type="string" 	required="false" 	default=""		hint="MODULE D'OU A ETE DEMANDE LE DOCUMENT"> 
		<cfargument name="FORMAT" 				type="string"	required="false" 	default=""		hint="FORMAT DU DOCUMENT"> 
		<cfargument name="DATE" 				type="string" 	required="false" 	default=""		hint="DATE DE PLANIFICATION DU JOB SOUS BI"> 
		<cfargument name="SERVER_BI" 			type="string" 	required="false" 	default=""		hint="NOM DU SERVER BI"> 
		<cfargument name="APPLI" 				type="string" 	required="false" 	default=""		hint="ID DE L'APPLI"> 
		<cfargument name="IS_PUBLIC" 			type="boolean" 	required="false" 	default="0"		hint="JE SAIS PAS"> 
		<cfargument name="STATUS" 				type="numeric" 	required="false" 					hint="LE STATUS DU DOCUMENT => VALEUR POSSIBLE : 2 DISPONIBLE, 3 ERREUR, 1 ENCOURS">
		<cfargument name="TAILLE" 				type="numeric" 	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT NON ARCHIVE">
		<cfargument name="TAILLEZIP" 			type="numeric"	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT ARCHIVE (.ZIP)">
		
		<cfset api = createObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
		<cfset p_retour = api.ADDDocumentTableContainer(NOM_DOC,SHORT_NAME,JOB_ID,CODE_RAPPORT,APP_LOGINID,IDRACINE,NOM_MODULE,FORMAT,DATE,SERVER_BI,APPLI,IS_PUBLIC,STATUS,TAILLE,TAILLEZIP)>
		<cfif p_retour neq -1>
			<cfset boolRenameStatus = RENAMEDocument(NOM_DOC,FORMAT,p_retour)>
			<cfif boolRenameStatus eq true>
				<cfreturn p_retour>
			<cfelse>
				<cfreturn -1>
			</cfif>
		<cfelse>
			<cfreturn -1>
		</cfif>
	</cffunction>
	<cffunction name="DELETEDocumentTableContainer" returntype="String" access="remote">
		<cfargument name="arr" 			type="Array" 	required="true" hint="la liste des documents à supprimer"> 
		<cfargument name="app_loginid" 	type="numeric"	required="true"	hint="app_loginid de l'utilisateur qui demande la suppression">
	
		<cfset retour = 0>
		
		<cfloop index="i" from="1" to=#ArrayLen(arr)#>
		<!--- Suppression du fichier dans la table container --->	
			<cfset FILEID = val(arr[i]['FILEID'])>
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.CONTAINERDELETERAPPORT">
				<cfprocparam value="#FILEID#" 		cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#app_loginid#"	cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam variable="p_retour"	cfsqltype="CF_SQL_INTEGER"  type="out">
			</cfstoredproc>
			<cflog text="*************************ContainerService.DELETEDocumentTableContainer() : retour = #p_retour#">
			<cfif p_retour eq -1>
					<cfset retour = -1>
			<cfelseif p_retour eq 1>
				<!--- Suppression du fichier du FTP --->
				<cfset UUID   		= arr[i]['UUID']>
				<cflog text="*********************** Suppression du fichier du FTP : #UUID#">
				<cfset del = DELETEDocumentToFTP(UUID)>
				<cfset retour = del>
			<cfelseif p_retour eq 2>
				<cfset retour = 2>
			</cfif>
		</cfloop>
		<cfreturn retour>
	</cffunction>
	<cffunction name="containerShareRapport" returntype="numeric" access="remote">
		<cfargument name="arrLogin" type="Array">
		<cfargument name="arrFile" 	type="Array">
		<cfargument name="iduser"	type="Numeric">
		
		<cfset retour = 1>
		
		<cfloop index="i" from="1" to="#ArrayLen(arrLogin)#">
			<cfloop index="j" from="1" to="#ArrayLen(arrFile)#">
				<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.CONTAINERSHARERAPPORT">
					<cfprocparam value="#val(arrFile[j]['FILEID'])#" 		cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#iduser#" 							cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#val(arrLogin[i]['APP_LOGINID'])#" 	cfsqltype="cf_SQL_INTEGER" type="in">
					<cfprocparam variable="p_retour" 						cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				
				<cfif p_retour eq -1>
					<cfset retour = -1>
				</cfif>					
			</cfloop>	
		</cfloop>
		<cfreturn retour>
	</cffunction>
	<!--- EDITER UN DOCUMENT --->	
	<cffunction name="EDITNameDocumentTableContainer" 	access="public" returntype="numeric" 	hint="modifie les données d'un document. En V1 : uniquement le nom du rapport, visible dans l'IHM">
		<cfargument name="NOM_DOC" 	type="string" 	required="true" hint="NOM LONG DU DOCUMENT (avec espace)"> 
		<cfargument name="FILEID" 	type="numeric" 	required="true" hint="ID DU FICHIER A EDITER">
		
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.EDITDocumentTableContainer">
			<cfprocparam value="#FILEID#" 		cfsqltype="CF_SQL_INTEGER"  type="in">
			<cfprocparam value="#NOM_DOC#"		cfsqltype="CF_SQL_VARCHAR"  type="in">
			<cfprocparam variable="p_retour"	cfsqltype="CF_SQL_INTEGER"  type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="DOWNLOADListDocument" returntype="Any" access="remote">
		<cfargument name="list" type="array" required="true">
		<cfargument name="archivename" type="String" required="true">
		
 		<cfset arr = ArrayNew(1)>
		
		<cfloop index="i" from="1" to=#ArrayLen(list)#>
			<cfset arr[i] = list[i]['UUID']>
		</cfloop> --->
		
		<cfset arr = "">
		
		<cfloop index="i" from="1" to=#ArrayLen(list)#>
			<cfset arr = arr & "," & list[i]['UUID']>
		</cfloop>
		
		<cfset zip = CREATEZipArchive(arr,archivename)>

		<cfreturn zip>
	</cffunction>
	<cffunction name="containerDepotRapport" returntype="numeric" access="remote">
		<cfargument name="arrLogin" type="Array">
		<cfargument name="arrFile" 	type="Array">
		<cfargument name="iduser"	type="Numeric">
		
		<cfset retour = 1>
		
		<cfloop index="i" from="1" to="#ArrayLen(arrLogin)#">
			<cfloop index="j" from="1" to="#ArrayLen(arrFile)#">
				<cflog text="ContainerShareFile : i = #i# ; j = #j#">
				<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.CONTAINERSHAREFILE">
					<cfprocparam value="#arrFile[j]#" 						cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam value="#iduser#" 							cfsqltype="CF_SQL_INTEGER" type="in">
					<cfprocparam value="#val(arrLogin[i]['APP_LOGINID'])#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
					<cfprocparam variable="p_retour" 						cfsqltype="CF_SQL_INTEGER" type="out">
				</cfstoredproc>
				
				<cfif p_retour eq -1>
					<cfset retour = -1>
				</cfif>					
			</cfloop>	
		</cfloop>
		<cfreturn retour>
	</cffunction>
	<cffunction name="DOWNLOADListDocumentClient" returntype="void" access="remote" output="true">
		<cfargument name="list" type="array" required="true">
		<cfargument name="archivename" type="String" required="true">
		
		<cflog text="*************************** ContainerService.DOWNLOADListDocumentClient()">
		
		<cfset arr = ArrayNew(1)>
		<cflog text="*************************** ContainerService - nombre de fichier à zipper : #ArrayLen(list)#">
		<cflog text="*************************** ContainerService - liste des fichiers :">
		<cfloop index="i" from="1" to=#ArrayLen(list)#>
			<cflog text="*************************** #list[i]['UUID']#">
			<cfset arr[i] = list[i]['UUID']>
		</cfloop>
		
		<cfset zip = CREATEZipArchiveClient(arr,archivename)>
	</cffunction>
	<cffunction name="LISTEparams" returntype="query" access="remote">
		<cfargument name="iddoc" type="Numeric">
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.getParams">
			<cfprocparam value="#iddoc#" 		cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<cffunction name="RENAMEDocument" access="public" returntype="boolean">
		<cfargument name="fileName"			type="string"	required="true">
		<cfargument name="fileExtension"	type="String" 	required="true">
		<cfargument name="UUID"				type="string" 	required="true">
		
		<cflog text="********************** containerService.RENAMEDocument : ">
		<cflog text="********************** fichier : #fileName##fileExtension#">
		<cflog text="********************** uuid : #UUID#">
		
		<cfset ApiContainer = CreateObject("component","fr.consotel.consoview.api.container.ApiContainerV1")>
		
		<cfset renameStatus = ApiContainer.RENAMEDocumentToFTP(arguments.fileName,arguments.fileExtension,arguments.UUID)>
				
		<cfif renameStatus eq 1>
				<cfreturn true>
			<cfelse>
				<cfmail 		from="container@consotel.fr"
								to="olivier.boulard@consotel.fr"
								subject="(Container) ContainerService.RENAMEDocument : Erreur lors du renommage du fichier (BackOffice : #CGI.SERVER_NAME#)"
								type="html"
								charset="utf-8"
								wraptext="72"
								server="192.168.3.119:26">
					LE fichier #arguments.fileName#.#arguments.fileExtension# n'a pas pu être renommé en #arguments.UUID#.<br><br>
					La cause peut être : <br>
						- Impossible de se connecter au FTP<br>
						- Le fichier n'existe pas sur le FTP<br>
						- Le fichier est en cours d'utilisation<br>
				</cfmail>
				<cfreturn false>
		</cfif>
		
	</cffunction>
	
	<cffunction name="getGroupList" access="remote" returntype="Any">
		<cfargument name="accessId" required="true" type="numeric">
 		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_GLOBAL.Login_Listeroot_v2">
			<cfprocparam  value="#accessId#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetGroupList">
		</cfstoredproc>
		<cfreturn qGetGroupList>
	</cffunction>
	
<!--- 
		METHODE PRIVATE
 --->
	
	<!--- CREATION DE ZIP --->	
		<cffunction name="CREATEZipArchive" access="private" returntype="Any"	hint="méthode de création d'une archive(.zip) d'un répertoire contenant des documents">	
			<cfargument name="arUuid" 		type="string" 	required="true" 	hint="Array de UUID qui correspondent aux fichiers demandés" >
		    <cfargument name="archiveName" 	type="string" 	required="true" 	hint="nom de l'archive">
		    
		    <cflog text="*************************** ContainerService.CREATEZipArchive()">
		    <cfset tempUuid=createUUID()>
		    <cflog text="*************************** ContainerService - dossier temporaire : #tempUuid#">
		    <cfset arrDoc = GETInfosFromUUID(arUuid)>
		    <cflog text="*************************** ContainerService - nombre de fichier dont on a les informations : #ArrDoc.recordcount#">
			<cfif arrDoc.recordcount gt 0>
	            <cfset filepath="/container/#tempUuid#">
	            <cfset racineDir="/container">
		            <!--- Supprime ancien répertoire si il existe --->
		            <cftry>
		           	<cfdirectory action="delete" directory="#filepath#" mode="777" recurse="true">
		            <cfcatch></cfcatch>
		            </cftry>
		            <!--- création répertoire temporaire --->
		            <cfdirectory action="create" directory="#filepath#">	
		            <!--- boucle sur les fichiers --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset uuid=arrDoc.uuid>
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="copy" source="#racineDir#/#uuid#"
												destination="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		            <!--- Zip les fichiers --->
		            <cfzip action="zip" source="#filepath#/" file="#racineDir#/#archiveName#.zip"/>
		            <cffile action="readbinary" variable="fileContent" file="#racineDir#/#archiveName#.zip">
	 		        <cffile action="delete" file="#racineDir#/#archiveName#.zip">
		            <!--- Supression des fichiers individuels --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="delete" file="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		    		<cfdirectory action="delete" directory="#filepath#" recurse="true">
		    		<cfdirectory action="list" directory="#filepath#" name="rslt">
		    		<cfif rslt.recordcount eq 1>
						<cflog text="********************** le répertoire #filepath# n'a pas été supprimé (ECHEC)">
					<cfelse>
						<cflog text="********************** le répertoire #filepath# a été supprimé (REUSSITE)">
					</cfif>
		    		<cfreturn fileContent>
		    <cfelse>
		    		<cfreturn -1>
			</cfif>
		    
		</cffunction>
	<!--- CREATION DE ZIP CLIENT --->	
		<cffunction name="CREATEZipArchiveClient" access="private" returntype="void"	hint="méthode de création d'une archive(.zip) d'un répertoire contenant des documents" output="true">	
			<cfargument name="arUuid" 		type="string" 	required="true" 	hint="Array de UUID qui correspondent aux fichiers demandés" >
		    <cfargument name="archiveName" 	type="string" 	required="true" 	hint="nom de l'archive">
		    
		    <cfset tempUuid=createUUID()>
		    <cfset arrDoc = GETInfosFromUUID(arUuid)>
			<cfif arrDoc.recordcount gt 0>
	            <cfset filepath="/container/#tempUuid#">
	            <cfset racineDir="/container">
		            <!--- Supprime ancien répertoire si il existe --->
		            <cftry>
		           	<cfdirectory action="delete" directory="#filepath#" mode="777" recurse="true">
		            <cfcatch></cfcatch>
		            </cftry>
		            <!--- création répertoire temporaire --->
		            <cfdirectory action="create" directory="#filepath#">	
		            <!--- boucle sur les fichiers --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset uuid=arrDoc.uuid>
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="copy" source="#racineDir#/#uuid#"
												destination="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		            <!--- Zip les fichiers --->
		            <cfzip action="zip" source="#filepath#/" file="#racineDir#/#archiveName#.zip"/>
		            <cffile action="readbinary" variable="fileContent" file="#racineDir#/#archiveName#.zip">
	 		        <cffile action="delete" file="#racineDir#/#archiveName#.zip">
		            <!--- Supression des fichiers individuels --->
		            <cfset index=1>
		            <cfloop query="arrDoc">
		                  <cfset filename="#arrDoc.short_name_rapport#_#index#">
		                  <cfset filesuffix=arrDoc.format_fichier>
		                  <cffile action="delete" file="#filepath#/#filename#.#filesuffix#">
		                  <cfset index=index+1>
		            </cfloop>
		    		<cfdirectory action="delete" directory="#filepath#" recurse="true">
		    		<cfdirectory action="list" directory="#filepath#" name="rslt">
		    		<cfif rslt.recordcount eq 1>
						<cflog text="********************** le répertoire #filepath# n'a pas été supprimé (ECHEC)">
					<cfelse>
						<cflog text="********************** le répertoire #filepath# a été supprimé (REUSSITE)">
					</cfif>
		    	</cfif>
		    	<cfheader name="Content-Disposition" value="inline;filename=#archiveName#.zip">
				<cfcontent type="multipart/x-zip" variable="#fileContent#">
		</cffunction>
	<!--- RECUPERER LES INFOS D'UN DOCUMENT --->	
		<cffunction name="GETInfosFromUUID" 			access="public" returntype="query"		hint="">
      		<cfargument name="arUuid" type="string" required="true">
		
		<!--- 	<cfset arr = ArrayToList(arUuid)> --->
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.UUIDARRAYTOQUERY">
				<cfprocparam value="#arUuid#" cfsqltype="CF_SQL_CLOB" type="in">
				<cfprocresult name="retour">
			</cfstoredproc>
			
      		<cfreturn retour>
		</cffunction>
	<!--- SUPPRIMER UN FICHIER SUR LE FTP --->
		<cffunction name="DELETEDocumentToFTP" 			access="public" returntype="string" 	hint="méthode de suppression de document sur le FTP">	
			<cfargument name="uuid"	type="string"	required="true">
			
			<cfset retour = 0>
			
			<cftry>
				<cffile action="delete" file="/container/#uuid#">
				<cfdirectory action="list" directory="/container/#arguments.uuid#" name="listDir" listInfo="all">
				<cfif listDir.recordCount eq 1>
					<cflog text="************** containerService.DELETEDocumentToFTP : ERREUR">
				<cfelse>
					<cflog text="************** containerService.DELETEDocumentToFTP : SUCCES">
				</cfif>
				
				<cfcatch>
					<cfset retour = -1>
				</cfcatch>
			</cftry>
			<cfset retour = uuid>
			<cfreturn retour>	
		</cffunction>
</cfcomponent>