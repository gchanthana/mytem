package catalogue.util
{
	import flash.system.Capabilities;
	
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.NumberBaseRoundType;

	public class Currency
	{
		private var _precision:String = "2";
		private var _symbol:String = "€";
		private var _decimalSeparator:String =",";
		private var _thousandsSeparator:String =" ";
		private var _alignSymbol:String ="right";
		
		private var cf:CurrencyFormatter = new CurrencyFormatter(); 
		
		public function Currency(symbole:String)
		{
			_symbol = symbole;
			setValuesByCurrency(_symbol);
			
			cf.precision = _precision;//ResourceManager.getInstance().getString('M24', 'CURRENCY_PRECISION');
			cf.rounding = NumberBaseRoundType.NONE;
			cf.decimalSeparatorTo = _decimalSeparator;//ResourceManager.getInstance().getString('M24', 'DECIMAL_SEPARATOR');
			cf.thousandsSeparatorTo = _thousandsSeparator;//ResourceManager.getInstance().getString('M24', 'THOUSANDS_SEPARATOR');
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.alignSymbol = _alignSymbol;//ResourceManager.getInstance().getString('M24', 'ALIGN_SYMBOL');
			cf.currencySymbol = _symbol;
			
		}
		
		public function format(num:Number):String
		{
			return cf.format(num);
		}
		
		
		private function setValuesByCurrency(insymbole:String):Boolean
		{
			Capabilities.language
			switch (insymbole)
			{
				case "€":
					this._decimalSeparator = ",";
					this._thousandsSeparator = " ";
					this._alignSymbol = "right";
					return true;
				case "$": case "£":
					this._decimalSeparator = ".";
					this._thousandsSeparator = ",";
					this._alignSymbol = "left";
					return true;
				case "¥":
					this._decimalSeparator = ",";
					this._thousandsSeparator = " ";
					this._alignSymbol = "right";
					return true;
				case "FR":
					this._decimalSeparator = ",";
					this._thousandsSeparator = " ";
					this._alignSymbol = "right";
					return true;
				case "FCFA":
					this._decimalSeparator = ",";
					this._thousandsSeparator = " ";
					this._alignSymbol = "right";
					return true;
				case "LEU":
					this._decimalSeparator = ",";
					this._thousandsSeparator = " ";
					this._alignSymbol = "right";
					return true;
				default:
					this._decimalSeparator = ",";
					this._thousandsSeparator = " ";
					this._alignSymbol = "right";
					return false;
			}
		}

		public function get alignSymbol():String
		{
			return _alignSymbol;
		}

		public function get thousandsSeparator():String
		{
			return _thousandsSeparator;
		}

		public function get decimalSeparator():String
		{
			return _decimalSeparator;
		}

		public function get symbol():String
		{
			return _symbol;
		}

		public function get precision():String
		{
			return _precision;
		}


	}
}