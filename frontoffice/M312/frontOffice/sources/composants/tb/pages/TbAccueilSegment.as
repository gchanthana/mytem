package composants.tb.pages
{
	import mx.resources.ResourceManager;
	import composants.tb.tableaux.ITableauModel;
	import composants.tb.tableaux.ITableau_IHM;
	import composants.tb.tableaux.TableauAbo;
	import composants.tb.tableaux.TableauAboFactoryStratey;
	import composants.tb.tableaux.TableauAbo_IHM;
	import composants.tb.tableaux.TableauConsoFactoryStrategy;
	import composants.tb.tableaux.TableauConso_IHM;
	import composants.tb.tableaux.TableauFactory;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	 
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	import composants.tb.effect.EffectProvider;
	import composants.tb.tableaux.TableauChangeEvent;
	
	public class TbAccueilSegment extends TbAccueil
	{
		/**
		* Constructeur 
		**/
		public function TbAccueilSegment(type : TableauChangeEvent)
		{
			super();	
			_type =type;			 
			_pageLibelle = ResourceManager.getInstance().getString('M312', 'Segment___') + type.SEGMENT;
			addEventListener(FlexEvent.CREATION_COMPLETE,super.init);			
		}
		
	  
		 
					
	}
}