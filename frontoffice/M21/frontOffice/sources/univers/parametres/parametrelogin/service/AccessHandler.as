package univers.parametres.parametrelogin.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	
	[Bindable]
	public class AccessHandler extends EventDispatcher
	{
		private var _model:AccessModel;
		
		public function AccessHandler(model:AccessModel)
		{
			_model = model;
		}
		
		public function getAccessHandler(evt:ResultEvent):void
		{
			_model.updateAccess(evt.result as ArrayCollection);
		}
		
		public function activeParamHandler(evt:ResultEvent):void
		{
			var values:ArrayCollection = new ArrayCollection(evt.result as Array);
			_model.activeParamHandler(values);
			
		}
		
	}
}