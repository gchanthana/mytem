package univers.inventaire.equipements.gestionflottemobile.menugestionflottemobile
{
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	
	[Bindable]
	public class FreeButtonMenu extends Button
	{
		private var _msgTooltip:String;
		private var _messageAffihcee:String;
		private var _lacible:Label;		
		
		public function FreeButtonMenu()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		protected override function commitProperties():void
		{
			super.commitProperties();
			
			width = 40;
		}
		
		private function initIHM(fe : FlexEvent):void
		{
			this.addEventListener(MouseEvent.ROLL_OVER,rollOverHandler);
			this.addEventListener(MouseEvent.ROLL_OUT,rollOutHandler);
		}
		
		
		/////////// ACCESSORS /////////////////////////////////////
		public function set cibleMsgTooltip(cible : Label):void
		{
			lacible = cible;
		}
		
		public function set msgTooltip(value:String):void
		{
			_msgTooltip = (value != null)?value:"";
		}
		
		public function get msgTooltip():String
		{
			if(enabled)
			{
				return _msgTooltip;
			}
			else
			{
				return "Cette action est désactivée";
			}
		}
		
		public function set messageAffihcee(value:String):void
		{
			_messageAffihcee = value;
		}

		public function get messageAffihcee():String
		{
			return _messageAffihcee;
		}
		///////////////////////////
		
		
		
		///////////HANDLERS/////////////////////////////////////
		override protected   function rollOverHandler(ev : MouseEvent):void{
			super.rollOverHandler(ev);
			
			if(lacible != null)
			{
				lacible.text = msgTooltip;
			}
			
		}
		
		override protected  function rollOutHandler(ev : MouseEvent):void{
			super.rollOutHandler(ev);
			
			if(lacible != null)
			{
				lacible.text = "";	
			}
			
			
			
		}
		///////////////////////////
		

		public function set lacible(value:Label):void
		{
			_lacible = value;
		}

		public function get lacible():Label
		{
			return _lacible;
		}
	}
}