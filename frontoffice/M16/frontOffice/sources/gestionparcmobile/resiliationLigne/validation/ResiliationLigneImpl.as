package gestionparcmobile.resiliationLigne.validation
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Box;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	
	import gestioncommande.entity.Commande;
	import gestioncommande.entity.ElementsCommande2;
	import gestioncommande.events.CommandeEvent;
	import gestioncommande.services.RessourceService;
	
	import session.SessionUserObject;
	
	[Bindable]
	public class ResiliationLigneImpl extends Box
	{
		
	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dgListResiliation		:DataGrid;
		
		public var txtptSearch				:TextInput;
		
		public var chkbxAll					:CheckBox;
		
		//--------------------------------------------------------------------------------------------//
		//					OBJETS TYPES
		//--------------------------------------------------------------------------------------------//
		
		private var _cmd					:Commande;
		
		private var _myElements				:ElementsCommande2 = new ElementsCommande2();
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		private var _resSrv					:RessourceService = new RessourceService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//

		public var listeLigne				:ArrayCollection = new ArrayCollection();
		
		public var libelleAction			:String = '';
		
		public var isResiliation			:Boolean = false;
		
		public var numberLigneSelected		:int = 0;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function ResiliationLigneImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
			addEventListener(FlexEvent.SHOW, showHandler);			
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PUBLIC
	//--------------------------------------------------------------------------------------------//

		public function setMyDataToGrid():void
		{
			_cmd = SessionUserObject.singletonSession.COMMANDE;
			_myElements = SessionUserObject.singletonSession.CURRENTCONFIGURATION;
			
			listeLigne = _myElements.CONFIGURATIONS;
			
			if(SessionUserObject.singletonSession.IDTYPEDECOMMANDE == 9)
				isResiliation = true;
			
			if(listeLigne.length > 0)
			{
				callLater(getOptionsByLignes);
				numberLigneSelected = listeLigne.length;
			}
			
			this.validateNow();
		}
		
		public function validateCurrentPage():Boolean
		{
			var isOk :Boolean = false;
			var len	 :int = listeLigne.length;
			
			for(var i:int;i < len;i++)
			{
				if(listeLigne[i].SELECTED)
					isOk = true;
			}
			
			return isOk;
		}
		
		public function reset():void
		{
		}

	//--------------------------------------------------------------------------------------------//
	//					METHODES PROTECTED
	//--------------------------------------------------------------------------------------------//

		protected function txtptSearchHandler(e:Event):void
		{
			if(dgListResiliation.dataProvider != null)
			{
				(dgListResiliation.dataProvider as ArrayCollection).filterFunction = filterFunction;
				(dgListResiliation.dataProvider as ArrayCollection).refresh();
			}
		}

		protected function dgListResiliationClickHandler(e:Event):void
		{
			if(dgListResiliation.selectedItem != null)
			{
				if(dgListResiliation.selectedItem.SELECTED)
					dgListResiliation.selectedItem.SELECTED = false;
				else
					dgListResiliation.selectedItem.SELECTED = true;
				
				(dgListResiliation.dataProvider as ArrayCollection).itemUpdated(dgListResiliation.selectedItem);
				
				calculLignesSelected();
			}
		}
		
		protected function chkbxAllClickHandler(me:MouseEvent):void
		{
			var len:int = listeLigne.length;
			
			for(var i:int;i < len;i++)
			{
				listeLigne[i].SELECTED = chkbxAll.selected;
				listeLigne.itemUpdated(listeLigne[i]);
			}
			
			calculLignesSelected();
		}
		
	//--------------------------------------------------------------------------------------------//
	//					METHODES PRIVATE
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS IHM
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
		}

		private function showHandler(fe:FlexEvent):void
		{
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES
		//--------------------------------------------------------------------------------------------//	
		
		private function getOptionsByLignes():void
		{
			_resSrv.fournirLignesProduitAbonnement(addAllSousTete());
			_resSrv.addEventListener(CommandeEvent.LISTED_OPTIONSERASE, listeOptionsHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES
		//--------------------------------------------------------------------------------------------//	
		
		private function listeOptionsHandler(cmde:CommandeEvent):void
		{
			_myElements.LINEOPTIONS = _resSrv.listeProduitsAbonnementByLines;
			
			setOptionsSelected();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUNCTION
		//--------------------------------------------------------------------------------------------//	
		
		private function addAllSousTete():Array
		{
			var ids	:ArrayCollection = new ArrayCollection();
			var len	:int = _myElements.CONFIGURATIONS.length;
			
			for(var i:int = 0;i < len;i++)
			{
				ids.addItem( _myElements.CONFIGURATIONS[i].IDSOUS_TETE);
			}
			
			return ids.source;
		}

		private function setOptionsSelected():void
		{
			var lenConfg	:int = _myElements.CONFIGURATIONS.length;
			var lenLines	:int = _myElements.LINEOPTIONS.length;
			
			for(var i:int = 0;i < lenConfg;i++)
			{
				_myElements.CONFIGURATIONS[i].ADDOPTIONS = _myElements.OPTIONS;
				_myElements.CONFIGURATIONS[i].REMOPTIONS = new ArrayCollection();
				
				var num:String = _myElements.CONFIGURATIONS[i].LIGNE;
				
				for(var j:int = 0;j < lenLines;j++)
				{
					if(_myElements.LINEOPTIONS[j].SOUSTETE == num)
						_myElements.CONFIGURATIONS[i].REMOPTIONS.addItem(_myElements.LINEOPTIONS[j]);
				}
			}
		}
		
		private function calculLignesSelected():void
		{
			var len:int = listeLigne.length;
			
			numberLigneSelected = 0;
			
			for(var i:int;i < len;i++)
			{
				if(listeLigne[i].SELECTED)
					numberLigneSelected++;
			}
			
			if(numberLigneSelected == len)
				chkbxAll.selected = true;
			else
				chkbxAll.selected = false;
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FILTRE
		//--------------------------------------------------------------------------------------------//	
		
		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean = true;		    
			
			rfilter = rfilter && ( (item.LIGNE != null && item.LIGNE.toLocaleLowerCase().search(txtptSearch.text.toLocaleLowerCase()) != -1) );
			
			return rfilter;
		}

	}
}
