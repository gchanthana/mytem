package gestionparc.ihm.action.associer.actionAssocierExtToLigne
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.ligne.FicheAssocierExtToLigne;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierExtToLigneReceiver
	{
		private var fiche:FicheAssocierExtToLigne;

		public function ActionAssocierExtToLigneReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierExtToLigne();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject);
			PopUpManager.centerPopUp(fiche);
		}
	}
}