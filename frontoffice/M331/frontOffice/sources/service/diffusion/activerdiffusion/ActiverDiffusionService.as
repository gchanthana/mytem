package service.diffusion.activerdiffusion
{
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	public class ActiverDiffusionService
	{
		public var _model:ActiverDiffusionModel;
		public var handler:ActiverDiffusionHandler;
		
		public function ActiverDiffusionService()
		{
			this._model=new ActiverDiffusionModel();
			this.handler=new ActiverDiffusionHandler(model);
		}

		public function activerDiffusion(idEvent:int,noContrat:int):void
		{
			var callOp:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.ActionsDiffusionService", "activerDiffusion", handler.activerDiffusionHandler);
			
			RemoteObjectUtil.callService(callOp,idEvent,noContrat);
		}

		public function get model():ActiverDiffusionModel
		{
			return this._model;
		}
	}
}
