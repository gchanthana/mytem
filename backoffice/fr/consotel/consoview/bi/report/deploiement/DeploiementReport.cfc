<cfcomponent name="DeploiementReport" alias="fr.consotel.consoview.bi.report.deploiement.DeploiementReport">
	<cffunction name="createReportInfos" access="public" returntype="struct">
		<cfargument name="qwInfos" required="true" type="struct">
		<cfset deploiementInfos=arguments.qwInfos["DEPLOIEMENT"]>
		<cfset newDeploiementReportInfos=structNew()>
		<cfset newDeploiementReportInfos["ABSOLUTE_PATH"]="/LABS/QUICKWIN/DeploiementRacine/DeploiementRacine.xdo">
		<cfset newDeploiementReportInfos["TEMPLATE"]="default">
		<cfset newDeploiementReportInfos["OUTPUT_DIRECTORY"]="/opt/oracle/home/OBIEE/QUICKWIN/LABS/" & arguments.qwInfos["IDRACINE"] & "/">
		<!--- NE SONT PAS UTILISES 
		<cfset deploiementDateDebString=parseDateTime(deploiementInfos["DATEDEB"],"yyyy/mm/dd")>
		<cfset periodiciteString="Mensuel">
		<cfif deploiementInfos["PERIODICITE"] EQ 1>
			<cfset periodiciteString="Bimestriel">
		</cfif>
		 --->
		<!--- 3- est la position de la méthodo du déploiement --->
		<cfset newDeploiementReportInfos["OUTPUT_FILENAME"]="03-detail_deploiement">
		<cfset newDeploiementReportInfos["OUTPUT_FORMAT"]="EXCEL">
		<cfset newDeploiementReportInfos["BURST_OPTION"]="FALSE">
		<cfset newDeploiementReportInfos["JOB_NAME"]=arguments.qwInfos["IDRACINE"] & "-DEPLOIEMENT-QWREPORT">
		<!--- REPORT PARAMETERS --->
		<cfset i=1>
		<cfset arrayOfParamNameValues=arrayNew(1)>
		<cfset P_IDRACINE_MASTER=structNew()>
		<cfset P_IDRACINE_MASTER["name"]="P_IDRACINE_MASTER">
		<cfset P_IDRACINE_MASTER["values"]=[arguments.qwInfos["IDRACINE_MASTER"]]>
		<cfset P_IDRACINE_MASTER["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE_MASTER>
		<cfset i=i+1>
		<cfset P_IDRACINE=structNew()>
		<cfset P_IDRACINE["name"]="P_IDRACINE">
		<cfset P_IDRACINE["values"]=[arguments.qwInfos["IDRACINE"]]>
		<cfset P_IDRACINE["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_IDRACINE>
		<cfset i=i+1>
		<cfset P_DATEDEB=structNew()>
		<cfset P_DATEDEB["name"]="P_DATEDEB">
		<cfset P_DATEDEB["values"]=[deploiementInfos["DATEDEB"]]>
		<cfset P_DATEDEB["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_DATEDEB>
		<cfset i=i+1>
		<cfset P_PERIODICITE=structNew()>
		<cfset P_PERIODICITE["name"]="P_PERIODICITE">
		<cfset P_PERIODICITE["values"]=[deploiementInfos["PERIODICITE"]]>
		<cfset P_PERIODICITE["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_PERIODICITE>
		<cfset i=i+1>
		<cfset P_MONTANT_FT_MIN=structNew()>
		<cfset P_MONTANT_FT_MIN["name"]="P_MONTANT_FT_MIN">
		<cfset P_MONTANT_FT_MIN["values"]=[deploiementInfos["MONTANT_FT_MIN"]]>
		<cfset P_MONTANT_FT_MIN["multiValuesAllowed"]=FALSE>
		<cfset arrayOfParamNameValues[i]=P_MONTANT_FT_MIN>
		
		<cfset newDeploiementReportInfos["ARRAY_OF_PARAMETERS"]=arrayOfParamNameValues>
		<cfreturn newDeploiementReportInfos>
	</cffunction>
</cfcomponent>
