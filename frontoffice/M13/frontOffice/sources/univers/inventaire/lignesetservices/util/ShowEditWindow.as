package univers.inventaire.lignesetservices.util
{
	import flash.events.Event;
	
 
	import mx.core.Container;
	import mx.core.IFlexDisplayObject;
	import mx.managers.PopUpManager;
	
	import univers.inventaire.lignesetservices.applicatif.GestionTechniqueLignesApp;
	import univers.inventaire.lignesetservices.vo.LibellePerso;
	import univers.inventaire.lignesetservices.views.EditLibelleView;
	
	 
	
	public class ShowEditWindow
	{
		private var _app:GestionTechniqueLignesApp;
		private var _parent : Container;
		
		public function ShowEditWindow(gestionTechnique : GestionTechniqueLignesApp,parentLibelle : Container){
			_app = gestionTechnique;
			_parent = parentLibelle;
		}
		
		public function showEditWindow():void{
			var libellePerso : LibellePerso = new LibellePerso();
			libellePerso.libelle = _parent.label;
			libellePerso.libelleName = _parent.id;
			libellePerso.idLibelle = Number((_parent.id).substring(7));
			
			var edit : EditLibelleView = new EditLibelleView();
			edit.gestionTechnique = _app;
			edit.libellePerso = libellePerso;
			edit.addEventListener("libelleChanged",editHandler);
			
					
			PopUpManager.addPopUp(edit,_parent,true);
			PopUpManager.centerPopUp(edit);
		}
		
		
		private function editHandler(ev : Event):void{
			PopUpManager.removePopUp(IFlexDisplayObject(ev.currentTarget));
		}		
	}
}