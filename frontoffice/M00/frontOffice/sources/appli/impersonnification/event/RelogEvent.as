package appli.impersonnification.event
{
	import flash.events.Event;

	public class RelogEvent extends Event
	{
		public static const OTHERCONNECTION_ERROR:String = "otherconnection_error";
		public static const SESSION_CHANGED:String = "session_changed";
		public static const INIT_LOGINS:String = "init_logins";
		public static const LISTLOGINS_ERROR:String = "listlogins_error";
		public static const LOGIN_CHOSEN:String = "login_chosen";
		public static const CODEAPP_RECIEVED:String = "codeapp_recieved";
		
		public function RelogEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
	}
}