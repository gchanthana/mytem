package composants.searchpagindatagrid
{
	import composants.util.DateFunction;
	
	public class ParametresRechercheVO
	{
		private var _SEARCH_TEXT:String;
		private var _WithAlert:Number=1;
		private var _WithoutAlert:Number=1;
		private var _OnAlert:Number=0;
		private var _OnRoaming:Number=0;
		private var _OnRoamingDate:Date=null;
		private var _WithoutConnexion:Number;
		
		
		public function ParametresRechercheVO()
		{
		}
		
		/**
		 * The setSearchTexte function is for internal use only
		 * Cette fonction permet de définir le parametre de recherche _SEARCH_TEXT
		 * Ce parametre permet de trier le résultat de la procédure
		 * @param colonne La colonne sur la quelle la procédure doit rechercher
		 * @param valeur La valeur est une chaine.
		 * Une fonction en Coldfusion permet de séparer la clé et la valeur grâce à la virgule entre les deux paramêtres
		 * 
		 **/		
		
		public function set SEARCH_TEXT(value:String):void
		{
			_SEARCH_TEXT=value;
		}
		
		public function get SEARCH_TEXT():String
		{
			if(_SEARCH_TEXT)
			{
				return _SEARCH_TEXT;
			}
			else
			{
				return "";
			}
			
		}
		
		public function set WITHALERT(value:Number):void
		{
			_WithAlert = value;
		}
		
		public function get WITHALERT():Number
		{
			return _WithAlert;
		}
		
		public function set WITHOUTALERT(value:Number):void
		{
			_WithoutAlert = value;
		}
		
		public function get WITHOUTALERT():Number
		{
			return _WithoutAlert;
		}
		
		public function set ONALERT(value:Number):void
		{
			_OnAlert = value;
		}
		
		public function get ONALERT():Number
		{
			return _OnAlert;
		}
		
		public function set ONROAMING(value:Number):void
		{
			_OnRoaming = value;
		}
		
		public function get ONROAMING():Number
		{
			return _OnRoaming;
		}
		
		public function set ONROAMINGDATE(value:Date):void
		{
			_OnRoamingDate = value;
		}
		
		public function get ONROAMINGDATE():Date
		{
			return _OnRoamingDate;
		}
		
		public function set WITHOUTCONNEXION(value:Number):void
		{
			_WithoutConnexion = value;
		}
		
		public function get WITHOUTCONNEXION():Number
		{
			return _WithoutConnexion;
		}
		
			
	}
}