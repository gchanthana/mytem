package ihm.fonction.FctGestionCollecte.popup
{
	import composants.ui.TitleWindowBounds;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class PopupChooseRacineImpl extends TitleWindow
	{
		//--------------- VARIABLES ----------------//
		
		public var dgChooseRacine:DataGrid;
		public var tiFiltre			:TextInput;
		public var btnValider		:Button;
		public var btnAnnuler		:Button;
		[Bindable]
		public var btnValidEnabled	:Boolean = false;
		
		private var _itemCurrent	:Object = null;//A modifier avec un simple arrayCollection, ça devrait suffire
		private var _listeRacine 	:ArrayCollection;
		
		//--------------- METHODES ----------------//
		
		public function PopupChooseRacineImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseRacineHandler);
			btnAnnuler.addEventListener(MouseEvent.CLICK,annulerHandler);
			dgChooseRacine.addEventListener(ListEvent.CHANGE,currentItemHandler);
		}
		
		/* */
		protected function currentItemHandler(le:ListEvent):void
		{
			_itemCurrent = le.currentTarget.selectedItem;
			var i:Number = 0;
			for each(var item:Object in le.currentTarget.dataProvider as ArrayCollection)
			{
				if(_itemCurrent!=item)
					item.SELECTED = false;
				else
				{
					item.SELECTED = true;
					_itemCurrent.ID = i;
				}
				i++;
			}
			btnValidEnabled = true;
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeRacine.filterFunction = filtreSaisieTypeLIgne;
			listeRacine.refresh();
		}
		
		/* systeme de filtre */
		private function filtreSaisieTypeLIgne(item:Object):Boolean
		{
			if (item.RACINE != null && (item.RACINE as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		/**
		 * Retourne le libelle racine approprié peuplant la combobox :
		 *   libelle_racine ( nbCollectes collectes)
		 */
		public function labelRacineFunction(item:Object, column:DataGridColumn):String
		{
			return item.RACINE + "   -  " + item.NB_COLLECTES.toString() + ResourceManager.getInstance().getString('CG08','_collecte_s_');
		}
		
		/* au click sur annuler */
		private function annulerHandler(me:MouseEvent):void
		{
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			PopUpManager.removePopUp(this);
		}
		
		/* au click sur valider */
		private function validerChooseRacineHandler(me:MouseEvent):void
		{
			dispatchEvent(new Event("VALID_CHOIX_RACINE",true));
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			annulerHandler(null);
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		
		[Bindable]
		public function get listeRacine():ArrayCollection
		{
			return _listeRacine;
		}
		public function set listeRacine(value:ArrayCollection):void
		{
			_listeRacine = value;
		}

		public function get itemCurrent():Object
		{
			return _itemCurrent;
		}
		
		public function set itemCurrent(value:Object):void
		{
			_itemCurrent = value;
		}
		
	}
}