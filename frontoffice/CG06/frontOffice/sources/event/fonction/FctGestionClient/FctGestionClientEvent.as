package event.fonction.FctGestionClient
{
	import flash.events.Event;
	
	public class FctGestionClientEvent extends Event
	{
		// les types Event liés au service remote
		public static const LISTE_CLIENT				:String = 'LISTE_CLIENT';
		
		
		public function FctGestionClientEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}