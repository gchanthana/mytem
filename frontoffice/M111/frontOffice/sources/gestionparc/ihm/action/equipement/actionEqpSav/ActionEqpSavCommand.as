package gestionparc.ihm.action.equipement.actionEqpSav
{
	import gestionparc.ihm.action.ICommand;

	public class ActionEqpSavCommand implements ICommand
	{
		private var receiver:ActionEqpSavReceiver=null;

		public function ActionEqpSavCommand(rec:ActionEqpSavReceiver)
		{
			this.receiver=rec;
		}

		public function execute():void
		{
			receiver.action();
		}
	}
}
