package univers.inventaire.equipements.gestionflottemobile.rebus
{
	import composants.util.ConsoviewAlert;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.equipements.gestionflottemobile.DgRechercheItemVo;
	import univers.inventaire.equipements.gestionflottemobile.PanelGestionFoltteMobile;
	
	public class RebusTerminal extends RebusTerminalIHM
	{
		[Embed(source="/assets/equipement/PHONE 50x50BIG.png")] 
		private var adrImg : Class;
		private var ligneSelected : DgRechercheItemVo;
		
		public function RebusTerminal(ligneSelected : DgRechercheItemVo)
		{
			super();
			this.ligneSelected = ligneSelected;

			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);			
		}

		private function initIHM(evt : FlexEvent):void
		{
			imgTitre.source = adrImg; 
			libelle_terminal_rebus.text = ligneSelected.IMEI;
			addEventListener(CloseEvent.CLOSE,fermer);
			calendar.selectedDate = new Date();	
			bt_annuler.addEventListener(MouseEvent.CLICK,fermer);
			bt_valider.addEventListener(MouseEvent.CLICK,rebusTerminal);	
		}
		
		private function fermer(evt : Event):void 
		{
			dispatchEvent(new Event(PanelGestionFoltteMobile.ANNULER_EVENT));
		}
		
		private function rebusTerminal(evt : MouseEvent):void
		{
			var idCause : int;
			if (radio_perte.selected == true){
				idCause = 1;
			}
			else
			{
				if (radio_casse.selected == true){
					idCause = 2 ;
				}
				else
				{
					if (radio_vol.selected == true){
						idCause = 3;
					}
					else
					{
						if (radio_autre.selected == true){
						idCause = 0;
						}
						
					}
				}
			}
		
			var idTerm : String = ligneSelected.IDTERMINAL;
			var commentaire : String = inputComm.text;
			var date : String = DateFunction.formatDateAsInverseString(calendar.selectedDate);
			//Alert.show("Rebus : Terminal :"+ligneSelected.IMEI+" mis au rebus. Cause : "+idCause+" Commentaire : "+commentaire+" date : "+date );
		
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.inventaire.equipement.flotte.FlotteMobileManager",
																		"putEquipRecycle_term_v3",
																		rebusTerminal_handler);
			RemoteObjectUtil.callService(op,idTerm,ligneSelected.IMEI,date,commentaire,idCause);
			//Alert.show(""+idTerm+" "+ligneSelected.MODELE+" ("+ligneSelected.IMEI+") "+date+" "+commentaire+" "+idCause);
		}
		
		private function rebusTerminal_handler (evt : ResultEvent):void
		{
			if(evt.result!=-1)
			{
				dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));
				
				ConsoviewAlert.afficherOKImage("Mise au rebus du terminal effectuée");
				dispatchEvent(new Event(PanelGestionFoltteMobile.VALIDER_EVENT));
			}
			else
			{
				trace("Erreur getPoolGestionnaire");
			}
		}
	}
}