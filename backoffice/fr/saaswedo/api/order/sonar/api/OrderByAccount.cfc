component implements="fr.saaswedo.api.order.bridge.IOperation" { 

	property
		name = "context"
		type = "struct"
		hint = "Call object";
		
	property
		name = "result"
		type = "struct"
		hint = "result object";
		
	public OrderByAccount function init(struct context)
	output="false"
	{
		this.context = arguments.context;
		this.result = {};
		updateContext();
		return( this );
	}
	
	public string function getDefinition()		
	{
		return 'OrderByAccount';
	}
	
	public boolean function manageResult(required struct result)		
	{	
		var status = (arguments.result.statuscode contains "200");
		
		if(status eq true)
		{
			this.result.data = parseResult(arguments.result);
		}
		else
		{
			this.result.data = parseError(arguments.result);	
		}
	 
		this.result.status = arguments.result.Statuscode;
		
		return status;
	}
	
	public struct function getResult()
	{
		return this.result;
	}
	
	public void function purgeTemporyItems()
	{
		
	}
	
	
	
	//--- Private Methods ----------------------------------------------------------------------------- 
	
	private string function  parseResult(required struct result)
	{
		var token = tostring(arguments.result.fileContent);
		
		return token;
		 
	}
	
	private string function  parseError(required struct result)
	{	
		return tostring(arguments.result.fileContent);
	}
	
	
	
	private void function updateContext()
	output="false"
	{
		context = this.context;
		HttpCall = createHttpCall();		
		context.HttpCall = HttpCall;
	}
	
	private struct function	createHttpCall()
	output="false"	
	{
		HttpCall = {};
		HttpCall['url'] = this.context.endpoint & "orders/";
		HttpCall['method'] = "get";		
		HttpCall['multipart'] = "false";
		HttpCall['HttpParams'] = createHttpParams();
		
		return HttpCall;
	}
	
	private array function	createHttpParams()
	output="false"	
	{
		
		HttpParams = [];	
			
		
		param = {};
		param['type'] = "header";
		param['name'] = "api-key";
		param['value'] = this.context.apiKey;
		 
		HttpParams[1] = param;
		
		
		
		param = {};
		param['type'] = "url";
		param['name'] = "AccountIds";
		param['value'] = getSonarAccount();
		
		HttpParams[2] = param;
		
		
		
		return HttpParams;
				
		 
	}
	
	private string function	getSonarAccount()
	output="false"	
	{
		return mock_getSonarAccount();
	}
	
	
	
	private string function	mock_getSonarAccount()
	output="false"	
	{
		 return "000000002";
	}
	
	
	
}