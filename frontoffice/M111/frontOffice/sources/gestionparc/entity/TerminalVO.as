package gestionparc.entity
{
	import composants.util.DateFunction;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;

	[Bindable]
	public class TerminalVO extends EquipementVO
	{
		public var niveau:int=-1;
		public var commentaires:String="";
		public var simRattache:SIMVO=null;
		public var numSim:String=""; // pour le dataField
		public var byod:Number = 0;
		public var listeLigneSim:ArrayCollection=null;
		public var listeIncident:ArrayCollection=null;
		public var listeContrat:ArrayCollection=null;
		public var listeSousEquipement:ArrayCollection=null;

		public var commande:CommandeVO=null;
		public var dateFinGarantie:Date=null;

		// pour le modele
		public var idEqptFournis:int=-1;
		public var idRevendeur:int=-1;
		public var libEqptFournis:String="";
		public var refConst:String="";
		public var refRevendeur:String="";
		
		/**
		 * La fonction remplit un objet TerminalVO avec les donnees passees en parametre et retourne cette TerminalVO
		 */
		public function fillTerminalVO(obj:Object):TerminalVO
		{
			var myTerminalVO:TerminalVO=new TerminalVO();

			(obj[1][0].SERIAL_NUMBER != null ) ? myTerminalVO.numSerieEquipement=obj[1][0].SERIAL_NUMBER : myTerminalVO.numSerieEquipement="";
			(obj[1][0].IMEI!= null ) ? myTerminalVO.numIemiEquipement=obj[1][0].IMEI : myTerminalVO.numIemiEquipement="";

			myTerminalVO.libelleEquipement=obj[1][0].MODELE;
			myTerminalVO.typeEquipement=obj[1][0].TYPE_EQUIPEMENT;
			myTerminalVO.categorie=obj[1][0].LIBELLE_CATEGORIE;
			myTerminalVO.idCategorie=obj[1][0].IDCATEGORIE_EQUIPEMENT;
			myTerminalVO.niveau=(obj[1][0].NIVEAU_TERM != null) ? obj[1][0].NIVEAU_TERM : -1;
			myTerminalVO.commentaires=obj[1][0].COMMENTAIRE_EQ;
			myTerminalVO.nomRevendeur=obj[1][0].LIBELLE_REVENDEUR;
			myTerminalVO.idRevendeur=obj[1][0].IDFOURNISSEUR;
			myTerminalVO.revendeur=new RevendeurVO();
			myTerminalVO.revendeur.nom=obj[1][0].LIBELLE_REVENDEUR;
			myTerminalVO.revendeur.idRevendeur=obj[1][0].IDFOURNISSEUR;
			myTerminalVO.idFabricant=obj[1][0].IDFABRIQUANT;
			myTerminalVO.nomFabricant=obj[1][0].MARQUE;
			myTerminalVO.idTypeEquipement=obj[1][0].IDTYPE_EQUIPEMENT;
			myTerminalVO.byod=obj[1][0].BYOD;

			// s'il existe une commande associée
			if (obj[1][0].IDCOMMANDE)
			{
				myTerminalVO.commande=new CommandeVO();
				myTerminalVO.commande.idCommande=obj[1][0].IDCOMMANDE;
				myTerminalVO.commande.dateAchat=obj[1][0].DATE_COMMANDE;
				myTerminalVO.commande.dateLivraison=obj[1][0].DATE_LIVRAISON;
				myTerminalVO.commande.prix=obj[1][0].MONTANT;
				myTerminalVO.commande.numCommande=obj[1][0].REF_COMMANDE_TERMINAL;
				myTerminalVO.commande.refLivraison=(obj[1][0].REF_LIVRAISON) ? obj[1][0].REF_LIVRAISON : "";
			}

			// Ajout des sites
			myTerminalVO.idSite=obj[1][0].IDSITE_PHYSIQUE;
			myTerminalVO.idEmplacement=obj[1][0].IDEMPLACEMENT;

			// Ajout des LigneSim
			myTerminalVO.listeLigneSim=new ArrayCollection();
			var longueurTabLigneSim:int=(obj[2] as ArrayCollection).length;
			var newSIM:SIMVO;

			for (var i:int=0; i < longueurTabLigneSim; i++)
			{
				newSIM=new SIMVO();
				newSIM.idEquipement=obj[2][i].IDSIM;
				newSIM.numSerieEquipement=obj[2][i].NUM_SIM;
				newSIM.ligneRattache=new LigneVO();
				newSIM.ligneRattache.idSousTete=obj[2][i].IDSOUS_TETE;
				newSIM.ligneRattache.sousTete=obj[2][i].SOUS_TETE;
				newSIM.ligneRattache.typeLigne=obj[2][i].IBELLE_TYPE_LIGNE;
				newSIM.numLigne=obj[2][i].SOUS_TETE;
				newSIM.typeLigne=obj[2][i].LIBELLE_TYPE_LIGNE;
				newSIM.revendeur=new RevendeurVO();
				newSIM.revendeur.nom=obj[2][i].REVENDEUR_SIM;
				newSIM.revendeur.idRevendeur=obj[2][i].IDREVENDEUR_SIM;
				newSIM.nomRevendeur=obj[2][i].REVENDEUR_SIM;
				newSIM.operateur=new OperateurVO();
				newSIM.operateur.idOperateur=obj[2][i].OPERATEURID;
				newSIM.operateur.nom=obj[2][i].OPERATEUR;
				
				if(obj[2][i].OPERATEUR == null)
				{
					newSIM.nomOperateur="";
				}
				else
				{
					newSIM.nomOperateur=obj[2][i].OPERATEUR;
				}
				
				newSIM.idRow=obj[2][i].ID;
				myTerminalVO.listeLigneSim.addItem(newSIM);
			}

			// Ajout des Incidents
			myTerminalVO.listeIncident=new ArrayCollection();
			var longueurTabIncident:int=(obj[3] as ArrayCollection).length;
			var newIncident:IncidentVO;

			for (var j:int=0; j < longueurTabIncident; j++)
			{
				newIncident=new IncidentVO();
				newIncident.idIncident=obj[3][j].ID_SAV;
				newIncident.refIncident=obj[3][j].REFERENCE_SAV;
				newIncident.numIncident=obj[3][j].NUMERO_TICKET;

				newIncident.userCreate=obj[3][j].USER_CREATE;
				newIncident.userModify=obj[3][j].USER_MODIF;
				newIncident.userClose=obj[3][j].USER_CLOSE;

				newIncident.totalCoutIntervention=obj[3][j].TOTAL_COUT_INTERVENTION;
				newIncident.nbIntervention=obj[3][j].NB_INTERVENTIONS;

				newIncident.solution=obj[3][j].SOLUTION_SAV;
				newIncident.description=obj[3][j].DESCRIPTION;

				newIncident.dateDebut=obj[3][j].DATE_DEBUT;
				newIncident.dateFin=obj[3][j].DATE_FIN;

				newIncident.revendeur=obj[1][0].LIBELLE_REVENDEUR; // pour le dataField

				newIncident.isClose=(newIncident.dateFin) ? 1 : 0;
				if (newIncident.isClose)
				{
					newIncident.etat=ResourceManager.getInstance().getString('M111', 'Ferm_');
				}
				else
				{
					newIncident.etat=ResourceManager.getInstance().getString('M111', 'Ouvert');
				}

				// liste des interventions
				newIncident.listeIntervention=new ArrayCollection();

				myTerminalVO.listeIncident.addItem(newIncident);
			}

			// Ajout des Contrats
			myTerminalVO.listeContrat=new ArrayCollection();
			var longueurTabContrat:int=(obj[4] as ArrayCollection).length;
			var newContrat:ContratVO;

			for (var k:int=0; k < longueurTabContrat; k++)
			{
				newContrat=new ContratVO();
				newContrat.idContrat=obj[4][k].IDCONTRAT;
				newContrat.refContrat=obj[4][k].REFERENCE_CONTRAT;
				newContrat.idTypeContrat=obj[4][k].IDTYPE_CONTRAT;
				newContrat.typeContrat=obj[4][k].TYPE_CONTRAT;
				newContrat.idUserCreate=obj[4][k].IDUSER_CREATE;
				newContrat.idUserModif=obj[4][k].IDUSER_MODIF;
				newContrat.idUserResi=obj[4][k].IDUSER_RESI;
				newContrat.nomUserCreate=obj[4][k].NOM_USER_CREATE;
				newContrat.nomUserModif=obj[4][k].NOM_USER_MODIF;
				newContrat.nomUserResi=obj[4][k].NOM_USER_RESI;

				newContrat.dateCreate=obj[4][k].DATE_CREATE;
				newContrat.dateDebut=obj[4][k].DATE_DEBUT;
				newContrat.dateEcheance=obj[4][k].DATE_ECHEANCE;
				newContrat.dateEligibilite=obj[4][k].DATE_ELLIGIBILITE;
				newContrat.dateModif=obj[4][k].DATE_MODIF;
				newContrat.dateRenouvellement=obj[4][k].DATE_RENOUVELLEMENT;
				newContrat.dateResi=obj[4][k].DATE_RESI;
				newContrat.dateResiliation=obj[4][k].DATE_RESILIATION;
				newContrat.dateSignature=obj[4][k].DATE_SIGNATURE

				newContrat.commentaires=obj[4][k].COMMENTAIRE_CONTRAT;
				newContrat.dureeContrat=obj[4][k].DUREE_CONTRAT;
				newContrat.dureeEligibilite=obj[4][k].DUREE_ELLIGIBILITE;
				newContrat.tarifMensuel=obj[4][k].LOYER;
				newContrat.tarif=obj[4][k].MONTANT_CONTRAT;
				newContrat.preavis=obj[4][k].PREAVIS;
				newContrat.taciteReconduction=obj[4][k].TACITE_RECONDUCTION;
				newContrat.idRevendeur=obj[4][k].IDFOURNISSEUR;
				newContrat.revendeur=obj[4][k].NOM_FOURNISSEUR;

				if (newContrat.idTypeContrat == 3 && newContrat.dateDebut && newContrat.dureeContrat) // contrat garantie
				{
					var dureePourCeContrat:Date=DateFunction.dateAdd("m", newContrat.dateDebut, newContrat.dureeContrat);
					if (myTerminalVO.dateFinGarantie == null)
					{
						myTerminalVO.dateFinGarantie=dureePourCeContrat;
					}
					else if (dureePourCeContrat.getTime() - myTerminalVO.dateFinGarantie.getTime() > 0)
						myTerminalVO.dateFinGarantie=dureePourCeContrat;
				}

				myTerminalVO.listeContrat.addItem(newContrat);
			}

			myTerminalVO.listeSousEquipement=new ArrayCollection();
			var longueurTabSousEquipement:int=(obj[5] as ArrayCollection).length;
			var newSousEquipement:SousEquipementVO;
			for (i=0; i < longueurTabSousEquipement; i++)
			{
				newSousEquipement=new SousEquipementVO();
				newSousEquipement.fill(obj[5][i]);
				myTerminalVO.listeSousEquipement.addItem(newSousEquipement);
			}

			return myTerminalVO;
		}

		/**
		 * La fonction met à jour la liste listeLigneSim avec les donnees passees en parametre.
		 */
		public function fillListLigneSim(objResult:Object):void
		{
			var objResultLen:int=(objResult as ArrayCollection).length;
			var sim:SIMVO=null;

			listeLigneSim=new ArrayCollection();

			for (var i:int; i < objResultLen; i++)
			{
				sim=new SIMVO();
				sim.ligneRattache=new LigneVO();
				sim.ligneRattache.idSousTete=(objResult as ArrayCollection)[i].IDSOUS_TETE;
				sim.typeLigne=(objResult as ArrayCollection)[i].LIBELLE_TYPE_LIGNE;
				sim.numLigne=(objResult as ArrayCollection)[i].SOUS_TETE;
				sim.numSerieEquipement=(objResult as ArrayCollection)[i].NUM_SIM;
				sim.nomOperateur=(objResult as ArrayCollection)[i].OPERATEUR;
				sim.nomRevendeur=(objResult as ArrayCollection)[i].NOM_FOURNISSEUR;
				sim.idEquipement=(objResult as ArrayCollection)[i].IDSIM;

				listeLigneSim.addItem(sim);
			}
		}
	}
}
