package composants.util
{
	import mx.charts.chartClasses.StackedSeries;
	import mx.formatters.PhoneFormatter;
	import mx.formatters.CurrencyFormatter;
	import mx.formatters.NumberFormatter;
	import mx.formatters.NumberBaseRoundType;
	import mx.controls.Alert;
	import mx.formatters.DateFormatter;
		
	public class ConsoviewFormatter
	{
		//formate le numero de télé98phone
		public static function formatPhoneNumber( num : String):String{
			var pf : PhoneFormatter = new PhoneFormatter();
			pf.formatString = "## ## ## ## ##";
			pf.areaCode = -1;
			if (pf.format(num)=="") {
				return num.substr(0,2) + " " + num.substr(2,2) + " " + num.substr(4,2) + " " + num.substr(6,2) + " " + num.substr(8,2);
			} else {
				return pf.format(num);	
			}			
		} 
				
		//formate la monaie avec la précision passée en parametre	
		public static function formatEuroCurrency( num : Number , precision : uint):String{
			
			var cf : CurrencyFormatter = new CurrencyFormatter();			
			cf.precision = precision;   
			cf.rounding = NumberBaseRoundType.NONE;		
			cf.decimalSeparatorTo = ",";
			cf.thousandsSeparatorTo = " ";
			cf.useThousandsSeparator = true;
			cf.useNegativeSign = true;
			cf.currencySymbol = " €";
			cf.alignSymbol = "right";
			
			return cf.format(num);
		}
		
		//formate le pourcentage avec zero ou un chiffre apres la virgule
		public static function formatePourcent(num : Number):String{
			var ret : String;
			var nf : NumberFormatter = new NumberFormatter();			
			
			nf.rounding = NumberBaseRoundType.NONE;		
			nf.decimalSeparatorTo = ",";
			nf.thousandsSeparatorTo = " ";    
			nf.useThousandsSeparator = true;
			nf.useNegativeSign = true;
			
			if ( Math.abs(num) == num ){
				nf.precision = 0;
			}else{
				nf.precision = 1;
			}
			
			return nf.format(ret);			
		}
		
	
		//formtae un chiffre avec la precision passée en parametre
		public static function formatNumber(num : Number , precision : int):String{
			
			var nf : NumberFormatter = new NumberFormatter();			
			
			nf.rounding = NumberBaseRoundType.NONE;		
			nf.decimalSeparatorTo = ",";
			nf.thousandsSeparatorTo = " ";    
			nf.useThousandsSeparator = true;
			nf.useNegativeSign = true;
			
			if ( Math.abs(num) == num ){
				nf.precision = 0;
			}else{
				nf.precision = precision;
			}

			return nf.format(num);	
		}
		
		
	}
}