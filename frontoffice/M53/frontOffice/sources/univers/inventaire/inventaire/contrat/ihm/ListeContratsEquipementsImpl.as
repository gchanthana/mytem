package univers.inventaire.contrat.ihm
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ICollectionView;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.events.ListEvent;
	
	[Event(name="btCreerContratClicked")]
	
	[Bindable]
	public class ListeContratsEquipementsImpl extends VBox
	{
		public var dgListe:DataGrid;	
		public var btCreerContrat:Button;
		
		private var _dataProvider:ICollectionView;			
		private var _selectedContrat:Object;
		
		private var _modeEcriture:Boolean;
		
		public function ListeContratsEquipementsImpl()
		{
			super();
		}		
		
		
		public function set selectedContrat(value:Object):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():Object
		{
			return _selectedContrat;
		}
		
		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			if(dgListe.selectedIndex != -1)
			{
				selectedContrat = dgListe.selectedItem;	
			}
			else
			{
				selectedContrat = null;
			}
		}


		public function set dataProvider(value:ICollectionView):void
		{
			_dataProvider = value;
		}

		public function get dataProvider():ICollectionView
		{
			return _dataProvider;
		}

		protected function _btCreerContratClickHandler(event:MouseEvent):void
		{
			dispatchEvent(new Event("btCreerContratClicked"));
		}

		public function set modeEcriture(value:Boolean):void
		{
			_modeEcriture = value;
		}

		public function get modeEcriture():Boolean
		{
			return _modeEcriture;
		}
	}
}