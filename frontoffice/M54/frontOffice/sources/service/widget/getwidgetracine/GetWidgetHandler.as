package service.widget.getwidgetracine
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GetWidgetHandler
	{
		private var _model:GetWidgetModel;
		
		public function GetWidgetHandler(_model: GetWidgetModel):void
		{
			this._model = _model;
		}	
		internal function getListWidgetResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateListWidget(evt.result as Array);
			}
		}
	}
}