<cfcomponent name="fr.consotel.consoview.rapports.EtatConsoGsm.RapportEtatConsoGsmStrategy" extends="fr.consotel.consoview.api.ibis.publisher.service.AbstractTaskService" output="false">
	<cffunction name="getDefaultXmlReportTask" access="public" output="false" returntype="xml">
		<cfxml variable="defaultXmlReportTask">
			<TASK>
				<!-- Chemin absolu du rapport BI Publisher -->
				<REPORT_ABSOLUTE_PATH>/consoview/facturation/rapport/EtatConsoGsm/EtatConsoGsm.xdo</REPORT_ABSOLUTE_PATH>
				<!-- Nom du template du rapport -->
				<TEMPLATE_ID>cv</TEMPLATE_ID>
				<!--- 
				<!-- Format du fichier de sortie (NO CASE) -->
				<OUTPUT_FORMAT>excel</OUTPUT_FORMAT>
				<!-- Nom du fichier output -->
				<OUTPUT_NAME>output</OUTPUT_NAME>
				<!-- Extension du fichier output -->
				<OUTPUT_EXT>xls</OUTPUT_EXT>
				<!-- Nom de l'application -->
				<APP_NAME>ConsoView DEV</APP_NAME>
				<!-- Nom de code du rapport (SANS ESPACES) -->
				<CODE_RAPPORT>CONSOTEL</CODE_RAPPORT>
				<!-- Nom de code du rapport -->
				<MODULE_NAME>MODULE RAPPORT-CONTAINER</MODULE_NAME>
				 --->
 
				<!-- Format du fichier de sortie (NO CASE) -->
				<OUTPUT_FORMAT>excel</OUTPUT_FORMAT>
				<!-- Nom du fichier output -->
				<OUTPUT_NAME>fichier output</OUTPUT_NAME>
				<!-- Extension du fichier output -->
				<OUTPUT_EXT>xls</OUTPUT_EXT>
				<!-- Nom de l'application -->
				<APP_NAME>ConsoView DEV</APP_NAME>
				<!-- Nom de code du rapport (SANS ESPACES) -->
				<CODE_RAPPORT>CONSOTEL</CODE_RAPPORT>
				<!-- Nom de code du rapport -->
				<MODULE_NAME>MODULE RAPPORT-CONTAINER</MODULE_NAME>

				<DELIVERY>
					<!-- Nom de la tache lorsque le rapport est en planification -->
					<JOB>Etat des consommations par familles</JOB>
					<!-- Mode de stockage du fichier de sortie (NO CASE) : FTP, LOCAL, EMAIL -->
					<CHANNEL>FTP</CHANNEL>
				</DELIVERY>
				<PARAMETERS>
					<PARAMETER>
						<!-- P_IDRACINE est OBLIGATOIRE pour le process CONTAINER -->
						<NAME>P_IDRACINE</NAME>
						<VALUE>3245401</VALUE>
						<TYPE>CV</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>S_IDPERIMETRE</NAME>
						<VALUE>3245401</VALUE>
						<TYPE>CLIENT</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>Q_SHORT_DEB</NAME>
						<VALUE>2009/01/01</VALUE>
						<TYPE>CLIENT</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>Q_SHORT_FIN</NAME>
						<VALUE>2009/01/31</VALUE>
						<TYPE>CLIENT</TYPE>
					</PARAMETER>
					<PARAMETER>
						<NAME>Q_USERID</NAME>
						<VALUE>88096</VALUE>
						<TYPE>CV</TYPE>
					</PARAMETER>
				</PARAMETERS>
			</TASK>
		</cfxml>
		<cfreturn defaultXmlReportTask>
	</cffunction>
</cfcomponent>
