	
	<!--- URL VARIABLE --->
		
	<cfset uuid 		= ''>
	<cfset cle			= ''>
	
	<cfset _idcampagne  = 0>
	<cfset idlogCamp	= 0>
	
	<!--- PROCEDURES VARIABLE --->
	
	<cfset infosEnrollmentResult = structNew()>
	<cfset currentInfos 		 = structNew()>
	
	<!--- satus --->
	
	<cfset statusUrl 	= 0>
	<cfset isExist		= 0>
	<cfset statusSMS 	= 0>

	<!--- TYPE D'OS --->

	<cfif structKeyExists(URL, "uuid") AND structKeyExists(URL, "cle")>
	
		<cfif URL.uuid NEQ '' AND URL.cle NEQ ''>
			
			<cfset statusUrl = 1>
			<cfset uuid 	 = URL.uuid>
			<cfset cle		 = URL.cle>
			
			<cfset infosEnrollmentResult = createObject("component", "fr.consotel.consoview.M111.MDMService").getEnrollDevice(uuid, cle, 'ROCOFFRE')>
			
			<cfif structKeyExists(infosEnrollmentResult, "ETAT") AND structKeyExists(infosEnrollmentResult, "INFOS")>
			 
				<cfif infosEnrollmentResult.ETAT GT 0 AND infosEnrollmentResult.INFOS.RecordCount GT 0>
					
					<cfset isExist = 1>
					
					<cfset currentInfos = infosEnrollmentResult.INFOS>
		
					<cfset _idcampagne 	= currentInfos['IDMDM_CAMPAGNE'][currentInfos.RecordCount]>
					
					<cfset idlogCamp	= currentInfos['IDMDM_LOG_CAMPAGNE'][currentInfos.RecordCount]>
					
					<!--- ici renvoi du texto log/mdp --->
					<cfset myUrlWebServiceFct = createObject("component", "fr.consotel.api.sms.public.ApiSmsPublic")>
					
					<cfset uuidSMS = myUrlWebServiceFct.createCampagne()>
					
					<cfset sousTete 		= currentInfos['SOUS_TETE'][currentInfos.RecordCount]>
					<cfset rsltFindNoCase 	= FindNoCase("0", sousTete)>
					
					<cfif rsltFindNoCase EQ 1>
					
						<cfset sousTeteWithIndicatif = "+33" & Mid(sousTete, rsltFindNoCase+rsltFindNoCase, Len(sousTete))>
					
					</cfif>
					
					<cfset sautDeLigne	= CHR(13) & CHR(10)>
					<cfset contentSms	= "Connexion au MDM :" & #sautDeLigne# & "Login : " & #currentInfos['USERNAME'][currentInfos.RecordCount]# & #sautDeLigne# & "Mot de passe :" & #currentInfos['USERPWD'][currentInfos.RecordCount]#>
					<cfset operator		= "sfr">
					<cfset delay		= "1000">
	
					<cfif isDefined("uuidSMS") AND uuidSMS NEQ "">  
									
						<cfset returnSend = myUrlWebServiceFct.sendMessageToSingleNumber(#sousTeteWithIndicatif#, #contentSms#, #operator#, #uuidSMS#)>
										
						<cfif returnSend EQ true>
											
							<cfset statusSMS = 40>
						
						<cfelse>
						
							<cfset statusSMS = -30>
						
						</cfif>
						
					<cfelse>
						
						<cfset statusSMS = -30>
						
					</cfif>
					
				<cfelse>
				
					<!--- ETAT = 0 ou INFOS.RecordCount = 0 => ERREUR --->
					<cfset isExist = -2>
				
				</cfif>
			
			<cfelse>
			
				<!--- ETAT ou INFOS non définis  => ERREUR --->
				<cfset isExist = -1>
			
			</cfif>
		
		<cfelse>
		
			<!--- uuid ou cle vides  => ERREUR --->
			<cfset statusUrl = -2>
		
		</cfif>
	
	<cfelse>
	
		<!--- uuid ou cle non definis => ERREUR --->
		<cfset statusUrl = -1>
	
	</cfif>

	<cfif statusSMS EQ 40>
		
		<!--- texto non envoyé --->
		<cfoutput>
						
			Votre SMS a été envoyé.
		
		</cfoutput>
		
		<cfset redirURL = urldecode('http://sun-dev-nicolas.consotel.fr/detection.cfm?' & cgi.query_string)>
		<cfset redirURL = urldecode('https://backoffice4.consotel.fr/detection.cfm?' & cgi.query_string)>
		
		<cflocation url="#redirURL#">
		
	<cfelseif statusSMS EQ -30>
	
		<!--- texto non envoyé --->
		<cfoutput>
						
			Votre SMS n'a pas été envoyé.
		
		</cfoutput>
	
	<cfelseif statusUrl EQ -1 OR statusUrl EQ -2>
		
		<!--- url non conforme --->
		<cfoutput>
		
			Page interdite
		
		</cfoutput>
	
	<cfelseif isExist EQ -1 OR isExist EQ -2>
		
		<!--- n'existe pas dans la base de données --->
		<cfoutput>
		
			Mobile inconnu
		
		</cfoutput>

	</cfif>
