<cfcomponent output="false" name="FactureGateWay">
	<cfproperty name="idGroupe" type="numeric" default="0">
	<cfproperty name="idGroupeLigne" type="numeric" default="0">
	<cfproperty name="typePerimetre" type="string" default="Groupe">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
		
	<!---
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : GETTER
	
	--->
	<cffunction name="getidGroupe" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupe>
	</cffunction>
	
	<cffunction name="getidGroupeLigne" output="false" access="public" returntype="numeric">
		<cfreturn variables.idGroupeLigne>
	</cffunction>
	
	<cffunction name="getTypePerimetre" output="false" access="public" returntype="string">
		<cfreturn variables.typePerimetre>
	</cffunction>
	
	

	
	<cffunction name="exporterListeMesFacturesXLS" access="public" output="true" returntype="any">		
		<cfargument name="myParamsRecherche" type="fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche" required="true">
		<cftry>
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"MesFactures.xls">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail = getListeFactureSupprimables(myParamsRecherche)>	
				<!---<cfabort showError = "error_message"> 	   	--->						
   			 	 <cfoutput>
	   			 <table  border="1" cellSpacing=0 cellPadding=0  >
		        	<tr  ><!-- Header Row-->
                    <td  align="center" style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:170px">Op&eacute;rateur</td>		           
		            <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:170px">Num&eacute;ro facture</td>
					 <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:200px">Soci&eacute;t&eacute;</td>
					 <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:170px">CF</td>
					 <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:360px">Adresse 1</td>
					 <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:360px">Adresse 2</td>
					 <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:50px">CP</td>
					 <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:200px">Commune</td>
					 <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:280px">SIREN/SIRET</td>
		            <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:200px">Date &eacute;mission</td>
		            <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:200px">D&eacute;but Fact.</td>
		            <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:200px">Fin Fact.</td>
		            <td colspan_"1" align="center"  style="font-family:Helvetica,Times,serif;font-size:100%;font-weight:bold;width:200px">Montant</td>
		            
		        </tr>
			
			<cfloop index="i" from="1" to="#arrayLen(aDetail)#"> 
						  
	        <tr  >
	         
	           <td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:170px" align="left" >#TRIM(aDetail[i].getOperateurLibelle())#</td>
	           <td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:170px" align="left" >#TRIM(aDetail[i].getnumero())#</td>
	           <td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:200px" align="left" >#TRIM(aDetail[i].getNOM())#</td>
	            <td colspan="1"style="font-family:Helvetica,Times,serif;font-size:13px;width:170px"  align="left" >#aDetail[i].getCompteFacturation()#</td>
	           	<td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:360px" align="left"  >#TRIM(toString(aDetail[i].getBY_ADRESSE1()))#</td>
	           	<td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:360px" align="left"  >#TRIM(toString(aDetail[i].getBY_ADRESSE2()))#</td>
	           	<td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:50px" align="left"  >#TRIM(toString(aDetail[i].getBY_ZIPCODE()))#</td>
	           	<td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:200px" align="left"  >#TRIM(toString(aDetail[i].getBY_COMMUNE()))#</td>
	           	<td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:280px" align="left"  >#TRIM(toString(aDetail[i].getBY_CODE_SITE()))#</td>
	            <td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:200px" align="left"  >#TRIM(LSDateFormat(aDetail[i].getDateEmission(),'DD/MM/YYYY'))#</td>
	            <td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:200px" align="left"  >#TRIM(LSDateFormat(aDetail[i].getdateDebut(),'DD/MM/YYYY'))#</td>
	            <td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:200px" align="left"  >#TRIM(LSDateFormat(aDetail[i].getdateFin(),'DD/MM/YYYY'))#</td>	                               	               
	            <td colspan="1" style="font-family:Helvetica,Times,serif;font-size:13px;width:200px" align="right" >#LsEuroCurrencyFormat(aDetail[i].getMontant(), 'none')# &euro;</td>	                     	               
	        </tr> 
			     
		</cfloop>		 				
</cfoutput>
    </table>		 	  			 	  			 												
	</cfsavecontent>
			
			<!--- Crï¿½ation du fichier xls --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/#fileName#" charset="iso-8859-1" 
			addnewline="true" fixnewline="true" output="#contentObj#">	
								 
			<cfreturn "#fileName#"> 		
		<cfcatch type="any">					
				<cfreturn "error">	
		</cfcatch>
		</cftry>
	</cffunction>
	
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 1/18/2008
		
		Description : Liste des factures ajoutï¿½es manuellement pour un groupe
		
		param in fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche
		param out fr.consotel.consoview.facturation.suiviFacturation.controles.Facture[]
		
	--->
	

	<cffunction name="getListeFactureSupprimables" access="remote" output="false" returntype="fr.consotel.consoview.facturation.suiviFacturation.controles.Facture[]">
		<cfargument name="myParamsRecherche" type="fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche" required="true">
  		<cfset qListeFactures = getListeFactureSupprimablesAsQuery(myParamsRecherche)>			
		<cfset tabFactures = arrayNew(1)>
		
		<cftry>
			
			
			<cfLoop query="qListeFactures">
				<cfset myFacture = createObject("component","fr.consotel.consoview.facturation.suiviFacturation.controles.Facture")>
				<cfscript>
					
					//myFacture.setOperateurId(qListeFacture.OPERATEURID);				
					myFacture.setOperateurLibelle(qListeFactures.NOM);
					myFacture.setNumero(qListeFactures.NUMERO_FACTURE);
					myFacture.setCompteFacturation(qListeFactures.COMPTE_FACTURATION);
					//myFacture.setCompteFacturationId(qListeFactures.IDCOMPTE_FACTURATION);
					myFacture.setPeriodeId(qListeFactures.IDINVENTAIRE_PERIODE);
					myFacture.setDateDebut(qListeFactures.DATEDEB);
					myFacture.setDateFin(qListeFactures.DATEFIN);
					myFacture.setDateEmission(qListeFactures.DATE_EMISSION);
					myFacture.setMontant(qListeFactures.MONTANT);
					myFacture.setCreeePar(qListeFactures.LOGIN_NOM & ' ' & qListeFactures.LOGIN_PRENOM);
					myFacture.setVisee(qListeFactures.BOOL_VISA);
					myFacture.setControlee(qListeFactures.BOOL_VALIDE);				
					myFacture.setExportee(qListeFactures.BOOL_EXPORTEE);
					myFacture.setBY_ZIPCODE(qListeFactures.BY_ZIPCODE);
					myFacture.setBY_ADRESSE2(qListeFactures.BY_ADRESSE2);
					myFacture.setBY_ADRESSE1(qListeFactures.BY_ADRESSE1);
					myFacture.setBY_COMMUNE(qListeFactures.BY_COMMUNE);
					myFacture.setBY_CODE_SITE(qListeFactures.BY_CODE_SITE);							
					
				</cfscript>
				<cfset arrayAppend(tabFactures,myFacture)>
			</cfLoop>
			<cfreturn tabFactures/>
		<cfcatch type="any">
			<cfset myFacture = createObject("component","fr.consotel.consoview.facturation.suiviFacturation.controles.Facture")>
			<cfset myFacture.setPeriodeId(-1)>
			<cfset myFacture.setDateEmission(now())>
			<cfset arrayAppend(tabFactures,myFacture)>			
			<cfreturn tabFactures>
		</cfcatch>
		
		</cftry>
	</cffunction>
	
	
	
	<!---
		
		Auteur : samuel.divioka
		
		Date : 1/18/2008
		
		Description : Liste des factures ajoutï¿½es manuellement pour un groupe
		
		param in ParamsRecherche
		param out query
		
	--->
	<cffunction name="getListeFactureSupprimablesAsQuery" access="remote" output="false" returntype="Query">
		<cfargument name="myParamsRecherche" type="fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche" required="true">
		<!--- PROCEDURE Liste_Facture_manuel (            p_Idgroupe_Client                                     IN INTEGER,
	                                                      p_operateurid                                         IN INTEGER,
	                                                      p_numeroFacture                                       IN VARCHAR2,
	                                                      p_date_emission                                       IN VARCHAR2,
	                                                      p_retour                                              OUT SYS_REFCURSOR);
	 	--->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_MANUEL_V3.Liste_Facture_manuel_v2">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_Idgroupe_Client" value="#getIdGroupeLigne()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_operateurid" value="#myParamsRecherche.getOperateurId()#" null="#iif((myParamsRecherche.getOperateurId() eq 0), de("yes"), de("no"))#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_numeroFacture" value="#myParamsRecherche.getChaine()#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_debut" value="#LSDateFormat(myParamsRecherche.getDateDebut(),'yyyy/mm/dd')#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR"  type="in" variable="p_date_fin" value="#LSDateFormat(myParamsRecherche.getDateFin(),'yyyy/mm/dd')#"/>	
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" type="in" value"#SESSION.USER.GLOBALIZATION#"/>
			<cfprocresult name="qListeFacture"/>   
		</cfstoredproc>		
		<cfreturn qListeFacture/>
	</cffunction>
	
	
	
	
	
	
	
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 1/18/2008
		
		Description : Supprime une facture qui n'est ni visï¿½e,ni controlï¿½e, ni exportï¿½ 
				et qui a ï¿½tï¿½ ajoutï¿½e manuellement
				
		Param in fr.consotel.consoview.facturation.suiviFacturation.controles.Facture
		Param out 1 si ok, -1 si erreur, -2 si opï¿½ration non autorisï¿½e		
	--->
	<cffunction name="supprimerFacture" access="remote" output="false" returntype="numeric">
		<cfargument name="facture" type="fr.consotel.consoview.facturation.suiviFacturation.controles.Facture" required="true">
		<!--- PROCEDURE PKG_CV_MANUEL.DEL_FACTURE_CASCADE(p_IdiventairePeriode       IN INTEGER,                 IN VARCHAR2,
	                                                      p_retour                   OUT INTEGER)                OUT SYS_REFCURSOR);
	 	--->
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_MANUEL_v3.del_facture_cascade">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_IdiventairePeriode" value="#facture.getPeriodeId()#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="p_retour"/>				
		</cfstoredproc>		
		<cfreturn p_retour/>
	</cffunction>
	
	
	
	
	
	
	
	
	<!---		
		Auteur : samuel.divioka
		
		Date : 1/18/2008
		
		Description : Supprime une liste de factures qui ne sont ni visï¿½e,ni controlï¿½e, ni exportï¿½ 
				et qui ont ï¿½tï¿½ ajoutï¿½e manuellement
				
		Param in fr.consotel.consoview.facturation.suiviFacturation.controles.Facture
		Param out 1 si ok, -1 si erreur,numero des factures non supprimable.		
	--->
	<cffunction name="supprimerListeFactures" access="remote" output="false" returntype="array">
		<cfargument name="factures" type="fr.consotel.consoview.facturation.suiviFacturation.controles.Facture[]" required="true">
		<cfset fTab = arrayNew(1)>
		<cfset errCode = "">
		<cfloop index="i" from="1" to="#arrayLen(factures)#">
			<cfset errCode = supprimerFacture(factures[i])>			
			<cfset factures[i].setLibelle(errCode)/>
			<cfset arrayAppend(ftab,factures[i])>				
		</cfloop>
		<cfreturn ftab>
	</cffunction>



<cffunction name="exporterListeFacturesXLS" access="public" output="true" returntype="any">		
		<cfargument name="myParamsRecherche" type="fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche" required="true">
		<!--- <cftry>--->
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"EtatFactures.xls">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail = getListeFactureSupprimables(myParamsRecherche)>	
				<!---<cfabort showError = "error_message"> 	   	--->						
   			 	 <cfoutput>
	   			 <table border=1 cellSpacing=2 cellPadding=0>
		        	<tr bgcolor="BADEF7" ><!-- Header Row-->
		            <td colspan="1" style="font-size:15px;">Op&eacute;rateur</td>
		            <td colspan="1" style="font-size:15px;">compte facturation</td>
		            <td colspan="1" style="font-size:15px;">num&eacute;ro facture</td>
		            <td colspan="1" style="font-size:15px;">date &eacute;mission</td>
		            <td colspan="1" style="font-size:15px;">montant factur&eacute;</td>
		            <td colspan="1" style="font-size:15px;">prct ctrl</td>
		            <td colspan="1" style="font-size:15px;">montant controlable</td>
		            <td colspan="1" style="font-size:15px;">produits control&eacute;s (facture)</td>
		            <td colspan="1" style="font-size:15px;">produits control&eacute;s (controle)</td>
		            <td colspan="1" style="font-size:15px;">vis&eacute;e</td>
		            <td colspan="1" style="font-size:15px;">commentaire visa</td>
		            <td colspan="1" style="font-size:15px;">vis&eacute;e ana</td>		     
		            <td colspan="1" style="font-size:15px;">commentaire visa ana</td>
		            <td colspan="1" style="font-size:15px;">control&eacute;e</td>
		            <td colspan="1" style="font-size:15px;">commentaire controle</td>
		            <td colspan="1" style="font-size:15px;">ERP</td>
		            <td colspan="1" style="font-size:15px;">commentaire ERP</td>
		        </tr>
		
		
			<cfloop index="i" from="1" to="#arrayLen(aDetail)#"> 
			
			    <cfif aDetail[i].getMontant() eq 0>
				<cfset pct = LSNumberFormat(100,"________.__")><cfelse>
				<cfset pct = LSNumberFormat(aDetail[i].getMontantVerifiable()/aDetail[i].getMontant()*100,"________.__")></cfif>
								
				<cfset commentaireViserAna = formateString(aDetail[i].getCommentaireViserAna())/>
				<cfset commentaireViser = formateString(aDetail[i].getCommentaireViser())/>
				<cfset commentaireControler = formateString(aDetail[i].getCommentaireControler())/>
				<cfset commentaireExporter = formateString(aDetail[i].getCommentaireExporter())/>	  
	        <tr>
	         
	           <td colspan="1"  align="center" >#TRIM(aDetail[i].getOperateurLibelle())#</td>
	            <td colspan="1" align="center" >#aDetail[i].getCompteFacturation()#</td>
	           	<td colspan="1"  align="center"  >#TRIM(toString(aDetail[i].getNumero()))#</td>
	            <td colspan="1" align="center"  >#TRIM(LSDateFormat(aDetail[i].getDateEmission(),'DD/MM/YYYY'))#</td>
	            <td colspan="1"  align="center" >#TRIM(LSNumberFormat(aDetail[i].getMontant(),"________.__"))#</td>
	            <td colspan="1" align="center">#TRIM(pct)#</td>
	            <td colspan="1" align="left"  >#TRIM(LSNumberFormat(aDetail[i].getMontantVerifiable(),"________.__"))#</td>
	            <td colspan="1"  align="left" >#TRIM(LSNumberFormat(aDetail[i].getMontantCalculeFacture(),"________.__"))#</td>
	            <td colspan="1"  align="left" >#TRIM(LSNumberFormat(aDetail[i].getMontantCalcule(),"________.__"))#</td>
	            <td colspan="1" align="center" >#TRIM(aDetail[i].getVisee())#</td>	           
	            <td colspan="1" align="left" >#TRIM(commentaireViser)#</td>
	            <td colspan="1" align="center" >#TRIM(aDetail[i].getViseeAna())#</td>
				<td colspan="1" align="left" >#TRIM(commentaireViserAna)#</td>
	            <td colspan="1" align="center" >#TRIM(aDetail[i].getControlee())#</td>
	            <td colspan="1" align="left" >#commentaireControler#</td>
	            <td colspan="1" align="center" >#TRIM(aDetail[i].getExportee())#</td>
	            <td colspan="1" align="left" >#commentaireExporter#</td>	           	               
	        </tr> 
			     
		</cfloop>
		 
				
</cfoutput>
    </table>		 	  			 	  			 												
	</cfsavecontent>
			
			<!--- Crï¿½ation du fichier xls --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/xls/#fileName#" charset="iso-8859-1" 
			addnewline="true" fixnewline="true" output="#contentObj#">	
								 
			<cfreturn "#fileName#"> 		
	<!--- 	<cfcatch type="any">					
				<cfreturn "error">	
		</cfcatch>
		</cftry> --->
	</cffunction>


<cffunction name="formateString" access="private" returntype="string">
		<cfargument name="chaine">
		<cfset chaineTmp1 = replace(TRIM(chaine),"-"," ","all")>
		<cfset chaineTmp2 = replace(chaineTmp1,";"," ","all")>
		<cfset chaineTmp3 = replace(chaineTmp2,#chr(13)#," ","all")>
		<cfset returnedChaine = replace(chaineTmp3,#chr(10)#," ","all")>
		<cfreturn returnedChaine>
	</cffunction>


	<!---		
		Auteur : samuel.divioka
		
		Date : 2/06/2008
		
		Description : Exporte la liste de factures supprimable en CSV
				
		Param in fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche
		Param out 1 si ok, -1 si erreur,numero des factures non supprimable.		
	--->
	<cffunction name="exporterListeFacturesSupprimablesCSV" access="public" output="true" returntype="any">		
		<cfargument name="myParamsRecherche" type="fr.consotel.consoview.facturation.suiviFacturation.controles.ParamsRecherche" required="true">
		<!--- <cftry> --->
			<cfset rootPath=expandPath("/")>
			<cfset UnicId = createUUID()>
			<cfset fileName = UnicId&"_"&"EtatFactures.csv">	
			
			<cfsavecontent variable="contentObj">
				<cfsetting enablecfoutputonly="true"/>										 				
				<cfset aDetail = getListeFactureSupprimables(myParamsRecherche)>								
				<cfset NewLine = Chr(13) & Chr(10)>		   
				<cfheader name="Content-Disposition" value="inline;filename=#fileName#" charset="iso-8859-1" >
				<cfcontent type="text/plain">		
				<cfoutput>opérateur;numero facture;compte facturation;date émission;date début;date fin;montant facture;créée par;#NewLine#</cfoutput><cfloop index="i" from="1" to="#arrayLen(aDetail)#"><cfoutput>#TRIM(aDetail[i].getoperateurLibelle())#;#TRIM('n° ' & '#aDetail[i].getNumero()#')#;#TRIM(toString('n° ' & aDetail[i].getcompteFacturation()))#;#TRIM(LSDateFormat(aDetail[i].getDateEmission(),'DD/MM/YYYY'))#;#TRIM(LSDateFormat(aDetail[i].getDateDebut(),'DD/MM/YYYY'))#;#TRIM(LSDateFormat(aDetail[i].getDateFin(),'DD/MM/YYYY'))#;#TRIM(LSNumberFormat(aDetail[i].getMontant(),"________.__"))#;#TRIM(aDetail[i].getCreeePar())#;#NewLine#</cfoutput></cfloop>
			</cfsavecontent>		 
			<!--- Création du fichier CSV --->			
			<cffile action="write" file="#rootPath#/fr/consotel/consoview/facturation/suiviFacturation/controles/csv/#fileName#" charset="iso-8859-1" 
					addnewline="true" fixnewline="true" output="#contentObj#">
					 
			<cfreturn "#fileName#"> 		
		<!--- <cfcatch type="any">					
				<cfreturn "error">	
			</cfcatch>
		</cftry> --->
	</cffunction>
	
	



</cfcomponent>
