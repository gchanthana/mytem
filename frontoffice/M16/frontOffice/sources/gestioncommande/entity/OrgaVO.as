package gestioncommande.entity
{
	[Bindable]
	public class OrgaVO
	{
		public var idOrga				: int 				= 0;
		public var libelleOrga			: String			= ""; 
		
		public var idCible				: int 				= 0; 
		public var idSource				: int 				= 0; // corrrespond à l'idGpeClient
		
		public var position				: int				= 0;
		public var chemin				: String			= "";
		
		public var idRegleOrga			: int				= 0;
		
		public var dateModifPosition	: Date				= null;
	}
}