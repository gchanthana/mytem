package vo.selector
{
	

	public class ValeurSelectorVo
	{
		
		private var _id:Number;
		private var _valeur: String;
		private var _libelle:String;
		private var _typePool:Number;

		private var _idPeriodeMois: int; // l'id dans la base par exemple 133 date debut , 
		private var _dispo_mois:String;
		private var _libelle_periode : String
		
		private var _indexInList :Number;
		private var _idSegment :Number;
		private var _selected :Boolean=true;
		
		public function ValeurSelectorVo(){}

		public function fill(valeurSelector:Object,index :Number):void
		{
			this._id=valeurSelector.ID;
			this._valeur=valeurSelector.VALUE;
			
			this._libelle=valeurSelector.LIBELLE;
			this._idSegment=valeurSelector.IDSEGMENT; 
			this._indexInList=index;
			
			this._idPeriodeMois=valeurSelector.IDPERIODE_MOIS;
			this._dispo_mois=valeurSelector.DISP_MOIS;
			this._libelle_periode=valeurSelector.LIBELLE_PERIODE ;
			this._typePool = valeurSelector.POOL_REVENDEUR;
		}
		
		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle = value;
		}
		
		public function get id():Number
		{
			return _id;
		}
		
		public function set id(value:Number):void
		{
			_id = value;
		}

		public function get valeur():String
		{
			return _valeur;
		}

		public function set valeur(value:String):void
		{
			_valeur = value;
		}
		
		public function get idPeriodeMois():int
		{
			return _idPeriodeMois;
		}
		
		public function set idPeriodeMois(value:int):void
		{
			_idPeriodeMois = value;
		}
			
		public function get dispo_mois():String
		{
			return _dispo_mois;
		}
		
		public function set dispo_mois(value:String):void
		{
			_dispo_mois = value;
		}
		
		public function get libelle_periode():String
		{
			return _libelle_periode;
		}
		
		public function set libelle_periode(value:String):void
		{
			_libelle_periode = value;
		}
		
		public function get indexInList():Number
		{
			return _indexInList;
		}
		
		public function set indexInList(value:Number):void
		{
			_indexInList = value;
		}
		public function get selected():Boolean
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void
		{
			_selected = value;
		}
		
		public function get idSegment():Number
		{
			return _idSegment;
		}
		
		public function set idSegment(value:Number):void
		{
			_idSegment = value;
		}
		
		public function get typePool():Number { return _typePool; }
		
		public function set typePool(value:Number):void
		{
			if (_typePool == value)
				return;
			_typePool = value;
		}
	}
}