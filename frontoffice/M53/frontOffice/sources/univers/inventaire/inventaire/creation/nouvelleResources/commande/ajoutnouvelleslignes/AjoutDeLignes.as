package univers.inventaire.inventaire.creation.nouvelleResources.commande.ajoutnouvelleslignes
{
	import composants.controls.TextInputLabeled;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.NumericStepper;
	import mx.controls.TextInput;
	import mx.events.DataGridEvent;
	import mx.events.DataGridEventReason;
	import mx.events.ListEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	
	/**
	 * Classe gérant la section 'Ajout de Lignes' dans le formulaire commande de nouvelle lignes
	 * */
	 [Bindable]public class AjoutDeLignes extends Canvas
	{	
		private const MAX_LIGNE:int = 10;
		/**
		 * Constante definissant le type de l'évenement dispatché lors de la selection de lignes
		 * */
		public static const SELECTION_CHANGED : String = "selectionChanged";
		
		/**
		 * Référence vers le NumericStepper de l'ecran
		 * */
		public var nsNbLignes : NumericStepper;
		
		/**
		 * Référence vers le bouton 'Ajouter'
		 * */
		public var btAjouterLignes : Button;
		
		/**
		 * Référence vers le TextInputLabeled 'Filtre' 
		 * */
		public var txtFiltreNllesLignes : TextInputLabeled;
		
		/**
		 * Référence vers le DataGrid 'Lignes' 
		 * */	
		public var myGridLignes : DataGrid;
		
		//Référence locale vers le panier de la commande
		private var _panier : ElementCommande;
		
		//Référence vers l'objet gérant la liste des lignes ajoutées
		private var listeLignes : ElementCommande = new ElementCommande();		
		
		//Variable Tampon referençant une ligne 
		private var tmpElement : ElementCommande;
		
		//Numéro de la lignes dans sons libellé
		private var indexLigne : Number = 0;
		
		/**
		 * Constructeur
		 * */
		public function AjoutDeLignes()
		{
			super();
		}
		
		/**
		 * Retourne le tableau contenant la liste des lignes selectionnées 
		 * @return Array
		 * */
		public function getSelectedItems():Array{
			return myGridLignes.selectedItems;
		}
		
		/**
		 * Affectation du panier de la commande
		 * @param panier 
		 * */
		public function setPanier(panier : ElementCommande):void{
			_panier = panier;
		}
		
		
		/**
		 * Met à jour le libelle des lignes du DataGrid avec le libelle de la ligne passé en paramètre
		 * Stocke une référence  vers cette ligne dans la variable tmpElement
		 * @param ligne 
		 * */
		public function updateLigneLibelle(ligne : ElementCommande):void{
			tmpElement = ligne;			
			if (listeLignes.liste != null){
				listeLignes.liste.source.forEach(updatelibelle);
				listeLignes.liste.refresh();
			}
		}
		
		
		
		/**
		 * Ajoute une ligne dans le DataGrid 
		 * */
		protected function ajouterLigne(me : MouseEvent):void{	
			if(listeLignes.liste.length < MAX_LIGNE){
				for (var i : int = 0; i < nsNbLignes.value; i++){
					
					var ligne : ElementCommande = new ElementCommande();
					ligne.flagRapproche = 0;
					ligne.ligneID = indexLigne++;										
					listeLignes.liste.addItem(ligne);
					
					getNewNumeroLigne(ligne);
					
						
					
					
					 
				}
				
			}else{
				Alert.show("Le nombre maximum de lignes pour une commande est " + MAX_LIGNE.toString(),"Info");
			}
			
		}
		
		/**
		 * Supprime la ligne selectionnée dans le DataGrid
		 * */
		protected function supprimerLignes(me : MouseEvent):void{
			if (myGridLignes.selectedIndices.length > 0){
				myGridLignes.selectedItems.forEach(supprimerCetteLigne);
				(myGridLignes.dataProvider as ArrayCollection).refresh();
			}
		}
		
		
		/**
		 * Met à jour les libelle des lignes du panier
		 * */
		protected function myGridLignesItemEditEndHandler(de : DataGridEvent):void{			
			if (myGridLignes.dataProvider != null){
				
				if (de.reason == DataGridEventReason.NEW_ROW){
					var myEditor:TextInput = TextInput(de.currentTarget.itemEditorInstance);                
	                // Get the new value from the editor.
	    	        var newVal:String = myEditor.text;
	                // Get the old value.
	                var oldVal:String = de.currentTarget.editedItemRenderer.data[de.dataField];               					
					//
					var elementCommande : ElementCommande = ElementCommande(myGridLignes.selectedItem);
					verifierUniciteSousTete(oldVal,newVal,elementCommande);	
				}										
				
			}else{		
				de.preventDefault();					
			}   
		}
		
		private function verifierUniciteSousTete(oldValue : String, newValue : String, elementCommande : ElementCommande):void{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
											  "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
											  "verifierUniciteSousTete",
											 verifierUniciteSousTeteResultHandler,null);
			RemoteObjectUtil.callService(op,newValue,oldValue,elementCommande);
		}
		
		private  function verifierUniciteSousTeteResultHandler(re : ResultEvent):void{
			var elementCommande : ElementCommande = re.token.message.body[2];
			var oldValue : String = re.token.message.body[1];
			var newValue : String = re.token.message.body[0];
			
			if (re.result > 0){
				Alert.show("Ce numéro de téléphone n'est pas disponible","Erreur ! ");
				elementCommande.libelleLigne = oldValue;
				listeLignes.liste.itemUpdated(elementCommande);
			}else{
				elementCommande.libelleLigne = newValue;
				listeLignes.liste.itemUpdated(elementCommande);
				tmpElement = elementCommande;
				_panier.liste.source.forEach(updatePanier);
			}
	  	}
		
		
		/**
		 * Dispatche un évènement signifiant que l'on selectionné une ligne du DataGrid
		 * 
		 * */
		protected function selectionChanged(le : ListEvent):void{
			if (myGridLignes.selectedIndex != -1){
				dispatchEvent(new Event(AjoutDeLignes.SELECTION_CHANGED));	
			}			
		}
		
		/**
		 * Gere le filtre du DataGrid
		 * */
	 	protected function filtrerGrid(ev : Event):void{			
			if (myGridLignes.dataProvider != null){
				(myGridLignes.dataProvider as ArrayCollection).filterFunction = filtrer;
				(myGridLignes.dataProvider as ArrayCollection).refresh();	
			}		
		}
		
		
		// Met à jour le libelle d'un élément du panier avec celui de tmpElement 
		// si leur identifiant est le même
		// Voir la méthode forEach de la Class Array pour les parametres
		private function updatePanier(element : * , index : int , arr : Array):void{			
			
		 	if (ElementCommande(element).ligneID == tmpElement.ligneID){
		 		ElementCommande(element).libelleLigne = tmpElement.libelleLigne;
		 		_panier.liste.itemUpdated(tmpElement);
		 	}	
		}
		
		
			
		//Filtre les éléments du tableau sur la propriété libelleLigne
		private function filtrer(ligne : ElementCommande):Boolean{
			if (String(ligne.libelleLigne).toLowerCase().search(txtFiltreNllesLignes.text.toLowerCase()) != -1){
				return true;
			}else{
				return false;
			}
		}
		
		//supprime un élément du DataGrid
		// Voir la méthode forEach de la Class Array pour les parametres
		private function supprimerCetteLigne(element : * , index : int , arr : Array):void{			
			(myGridLignes.dataProvider as ArrayCollection).removeItemAt((myGridLignes.dataProvider as ArrayCollection).getItemIndex(element));
		}
		
		//met à jour le libelle d'une ligne avec celui de stocké dans tmpElement  
		private function updatelibelle(element : * , index : int , arr : Array):void{						
		 	if (ElementCommande(element).ligneID == tmpElement.ligneID){
		 		ElementCommande(element).libelleLigne = tmpElement.libelleLigne;
		 	}		 		
		}
		
		/*========= REMOTINGS ============================*/
		private function getNewNumeroLigne(ligne : ElementCommande):void{			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
											  "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Commande",
											  "getNewNumeroLigne",
											  function getNewNumeroLigneResultHandler(re : ResultEvent):void{
												if(re.result){
													ligne.libelleLigne = re.result.toString();	
													listeLignes.liste.itemUpdated(ligne);													
													myGridLignes.dataProvider = listeLignes.liste;
													myGridLignes.selectedItem = ligne;
													myGridLignes.validateNow();
													myGridLignes.scrollToIndex(myGridLignes.selectedIndex);	
													
												}	
											  });
											  
			RemoteObjectUtil.callService(op); 	
			
			
		}	
	}
}