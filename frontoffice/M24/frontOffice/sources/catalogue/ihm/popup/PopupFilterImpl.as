package catalogue.ihm.popup
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButtonGroup;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	public class PopupFilterImpl extends TitleWindow
	{
		//--------------- VARIABLES ----------------//
		[Bindable] 
		public var rbgFilter				:RadioButtonGroup;
		[Bindable] 
		public var itemCurrentLabel		:Label;
		public var titlePopup			:String;
		public var dgChooseFilter			:DataGrid;
		public var tiFiltre				:TextInput;
		public var btnValider			:Button;
		public var btnAnnuler			:Button;
		public var LIBELLE_ITEM			:String = "LIBELLE_ITEM";
		public var ID_ITEM				:String = "IDITEM";
		[Bindable]
		public var btnValidEnabled		:Boolean = false;
		
		private var _itemCurrent		:Object = null;
		private var _selectedIndex		:Number;
		private var _scrollpos		:Number;
		private var _listeFilter 			:ArrayCollection;
		private var _itemSelectedFirst 	: Object = null;
		
		//--------------- METHODES ----------------//
		
		public function PopupFilterImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		/* initialisation */
		private function init(fe:FlexEvent):void
		{
			initData();
			initListeners();
		}
		
		private function initData():void
		{
			listeFilter.filterFunction = filtrerFilterList;
			listeFilter.refresh();
			// initialisation de l'affichage du label du Filter
			afficherInitialFilterLabel();
			initCurseur();
		}
		
		private function initCurseur():void
		{
			callLater(tiFiltre.setFocus);
		}
		
		// initialisation de l'affichage du label du Filter
		private function afficherInitialFilterLabel():void
		{
			if(listeFilter != null)
			{
				var longFilter:int = listeFilter.length;
				for (var i:int = 0; i < longFilter; i++) 
				{
					if(listeFilter[i].SELECTED)
					{
						itemCurrent = listeFilter[i];
						_itemSelectedFirst = listeFilter[i]; 						
						dgChooseFilter.selectedIndex=i;	
						if (i>=10 && i<14)					
							_scrollpos=10;
						else if(i>=14)
							_scrollpos=i-3;
							
					}
				}
			}
			itemCurrentLabel.text = getItemCurrentLabel();
			dgChooseFilter.scrollToIndex(_scrollpos);
		}
		
		private function initListeners():void
		{
			tiFiltre.addEventListener(Event.CHANGE,saisieFiltreHandler);
			btnValider.addEventListener(MouseEvent.CLICK,validerChooseRacineHandler);
			addEventListener(KeyboardEvent.KEY_DOWN,validerChooseRacineHandler2);
			btnAnnuler.addEventListener(MouseEvent.CLICK,onCloseHandler);
			dgChooseFilter.addEventListener(ListEvent.CHANGE,currentItemHandler);			
		}
		
		/* */
		protected function currentItemHandler(le:ListEvent):void
		{
			_itemCurrent = le.currentTarget.selectedItem;
			//_scrollpos = le.currentTarget.verticalScrollPosition;
			var i:int = 0;
			while (i < listeFilter.source.length)
			{
				if(_itemCurrent!=listeFilter.source[i])
					listeFilter.source[i].SELECTED = false;
				else
				{
					listeFilter.source[i].SELECTED = true;
					_selectedIndex = i;
				}
				i++;
			}
			
			//listeFilter.refresh();
			//dgChooseFilter.verticalScrollPosition =_scrollpos;
			dgChooseFilter.invalidateList();// a tester
			itemCurrentLabel.text = getItemCurrentLabel();
			btnValidEnabled = true;
		}
		
		/* lors de la saisie du filtre */
		private function saisieFiltreHandler(e:Event):void
		{
			listeFilter.filterFunction = filtrerFilterList;
			listeFilter.refresh();
		}
		
		/* systeme de filtre */
		private function filtrerFilterList(item:Object):Boolean
		{
			rbgFilter.selection = rbgFilter.getRadioButtonAt(dgChooseFilter.selectedIndex + 1);
			if (item[LIBELLE_ITEM] != null && (item[LIBELLE_ITEM] as String).toLowerCase().search((tiFiltre.text).toLowerCase()) != -1)
				return true;
			else
				return false;
		}
		
		/**
		 * Retourne le libelle racine approprié peuplant la combobox :
		 *   libelle_racine ( nbCollectes collectes)
		 */
		public function labelFilterFunction(item:Object, column:DataGridColumn):String
		{
			return item[LIBELLE_ITEM];			
		}
		
		protected function onCloseHandler(event:Event):void
		{
			tiFiltre.text = '';
			resetFilter();
			PopUpManager.removePopUp(this);
		}
		
		// initialise la popup vers le choix du Filter initial
		private function initPopUp():void
		{
			for (var i:int = 0; i < listeFilter.length; i++) 
			{
				listeFilter[i].SELECTED = false;
				if(listeFilter[i].IDFilter == _itemSelectedFirst.IDFilter)
					listeFilter[i].SELECTED = true;
			}
			listeFilter.refresh();
			itemCurrentLabel.text = getItemCurrentLabel();
		}
		
		/* au click sur valider */
		private function validerChooseRacineHandler(evt:MouseEvent):void
		{
			validerChoix();
		}
		
		private function validerChoix():void
		{
			dispatchEvent(new Event('VALID_CHOIX_Filter',true));
			tiFiltre.text = '';
			tiFiltre.dispatchEvent(new Event(Event.CHANGE));
			onCloseHandler(null);			
		}
		
		private function validerChooseRacineHandler2(evt:KeyboardEvent):void
		{
			if(evt.keyCode == Keyboard.ENTER)
			{
				validerChoix();
			}
		}
		
		//--------------- GETETRS - SETTERS ----------------//
		
		[Bindable]
		public function get listeFilter():ArrayCollection
		{
			return _listeFilter;
		}
		public function set listeFilter(value:ArrayCollection):void
		{
			_listeFilter = value;
		}
		
		public function get itemCurrent():Object
		{
			return _itemCurrent;
		}
		
		public function set itemCurrent(value:Object):void
		{
			_itemCurrent = value;
		}
		
		public function getItemCurrentLabel():String
		{
			if (_itemCurrent != null)
				return _itemCurrent[LIBELLE_ITEM];
			else
				return '';
		}
		public function getItemCurrentID():Number
		{
			if (_itemCurrent != null)
				return _itemCurrent[ID_ITEM];
			else
				return -1;
		}
		
		public function getItemCurrentIndex():Number
		{
			if (_itemCurrent != null)
				return _selectedIndex;
			else
				return -1;
		}
		
		private function resetFilter():void
		{
			listeFilter.filterFunction = null;
			listeFilter.refresh();
		}
			
	}
}