package entity.vo
{
    import entity.GestionCollecte.enum.EPeriodicity;

    public class CompteFacturationVO extends ValueObject
    {
        private var _idcompteFacturation:Number;
        private var _dataType:Number;
        private var _ecart:Number;
        private var _emissionDate:Date;
        private var _expectedNbImport:Number;
        private var _firstImportDate:Date;
        private var _firstImportEmissionFactureDate:Date;
        private var _firstImportEmissionFacturePeriod:String;
        private var _lastImportDate:Date;
        private var _lastImportEmissionFactureDate:Date;
        private var _lastImportEmissionFacturePeriod:String;
        private var _nbImport:Number;
        private var _periodicity:EPeriodicity;
        private var _rocfId:Number;

        public function CompteFacturationVO()
        {
            super();
        }

        public override function deserialize(object:Object):IValueObject
        {
            return null;
        }

        public override function serialize(object:IValueObject):Object
        {
            return null;
        }

        public override function toString():String
        {
            return "Compte Facturation VO " + id.toString() + "\n";
        }

        public override function toXML():XML
        {
            var xml:XML = voToXml(this, "CompteFacturationVO");
            return xml;
        }

        public override function decode(object:Object):IValueObject
        {
            var vo:CompteFacturationVO = new CompteFacturationVO();
            // TODO parsage de l'objet reçu
            return vo;
        }

        public function get dataType():Number
        {
            return _dataType;
        }

        public function set dataType(value:Number):void
        {
            _dataType = value;
        }

        public function get ecart():Number
        {
            return _ecart;
        }

        public function set ecart(value:Number):void
        {
            _ecart = value;
        }

        public function get emissionDate():Date
        {
            return _emissionDate;
        }

        public function set emissionDate(value:Date):void
        {
            _emissionDate = value;
        }

        public function get expectedNbImport():Number
        {
            return _expectedNbImport;
        }

        public function set expectedNbImport(value:Number):void
        {
            _expectedNbImport = value;
        }

        public function get firstImportDate():Date
        {
            return _firstImportDate;
        }

        public function set firstImportDate(value:Date):void
        {
            _firstImportDate = value;
        }

        public function get firstImportEmissionFactureDate():Date
        {
            return _firstImportEmissionFactureDate;
        }

        public function set firstImportEmissionFactureDate(value:Date):void
        {
            _firstImportEmissionFactureDate = value;
        }

        public function get firstImportEmissionFacturePeriod():String
        {
            return _firstImportEmissionFacturePeriod;
        }

        public function set firstImportEmissionFacturePeriod(value:String):void
        {
            _firstImportEmissionFacturePeriod = value;
        }

        public function get lastImportDate():Date
        {
            return _lastImportDate;
        }

        public function set lastImportDate(value:Date):void
        {
            _lastImportDate = value;
        }

        public function get lastImportEmissionFactureDate():Date
        {
            return _lastImportEmissionFactureDate;
        }

        public function set lastImportEmissionFactureDate(value:Date):void
        {
            _lastImportEmissionFactureDate = value;
        }

        public function get lastImportEmissionFacturePeriod():String
        {
            return _lastImportEmissionFacturePeriod;
        }

        public function set lastImportEmissionFacturePeriod(value:String):void
        {
            _lastImportEmissionFacturePeriod = value;
        }

        public function get nbImport():Number
        {
            return _nbImport;
        }

        public function set nbImport(value:Number):void
        {
            _nbImport = value;
        }

        public function get periodicity():EPeriodicity
        {
            return _periodicity;
        }

        public function set periodicity(value:EPeriodicity):void
        {
            _periodicity = value;
        }

        public function get rocfId():Number
        {
            return _rocfId;
        }

        public function set rocfId(value:Number):void
        {
            _rocfId = value;
        }

        public function get id():Number
        {
            return _idcompteFacturation;
        }

        public function set id(value:Number):void
        {
            _idcompteFacturation = value;
        }
    }
}