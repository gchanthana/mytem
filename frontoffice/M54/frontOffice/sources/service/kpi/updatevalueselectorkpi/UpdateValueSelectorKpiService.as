package service.kpi.updatevalueselectorkpi
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;

	public class UpdateValueSelectorKpiService
	{
		private var _model:UpdateValueSelectorKpiModel;
		public var handler:UpdateValueSelectorKpiHandler;
		private var remote:RemoteObject;
		
		public function UpdateValueSelectorKpiService()
		{
			this._model = new UpdateValueSelectorKpiModel();
			this.handler = new UpdateValueSelectorKpiHandler(_model);
		}		
		
		public function get model():UpdateValueSelectorKpiModel
		{
			return this._model;
		}

		public function saveValueSelectorKpi(kpi_ongletid :Number, kpi_paramid :Number ,param:Array ):void
		{		
			remote = new RemoteObject(RemoteObjectUtil.DEFAULT_DESTINATION);
			
			remote.source="fr.consotel.consoview.M54.commun.DataManager";
			remote.addEventListener(ResultEvent.RESULT,handler.updateValueKpiResultHandler,false,0,true);
			remote.updateParametrePermanent(kpi_ongletid,kpi_paramid,param);
		}	
	}
}