package intranet.M25.entity.vo
{
    import intranet.M25.entity.vo.SimpleEntityVO;

    /**
     *
     * Value Object permettant de <b>filtrer une liste </b>de collectes ou templates
     * en fonction d'un operateur,d'un type de données,d'une mode de récup
     * et de l'état de completion
     *
     */
    public class FilterCollecteTypeVO
    {
        private var _selectedDataTypeId:Number = 0;
        private var _selectedOperateurId:Number = 0;
        private var _selectedRecuperationModeId:Number = 0;
        private var _templateComplete:Number = 2;

        public function FilterCollecteTypeVO()
        {
        }

        public function toString():String
        {
            return "FilterCollectypeVO Recherche de Templates pour : \n" + "Types de données :" + selectedDataTypeId.toString() + "\n" + "Opérateur :" +
                selectedOperateurId.toString() + "\n" + "Mode de récup :" + selectedRecuperationModeId.toString() + "\n";
        }

        public function get templateComplete():Number
        {
            return _templateComplete;
        }

        /**
         * Indique l'état de complétion des templates que l'on souhaite
         * retourner :
         *  0 -> template incomplets
         *  1 -> template complets
         *  2 -> tous les templates
         * @param value
         *
         */
        public function set templateComplete(value:Number):void
        {
            _templateComplete = value;
        }

        public function get selectedDataTypeId():Number
        {
            return _selectedDataTypeId;
        }

        /**
         * Identifiant du type de données ( Facturation , Usages ...)
         *
         */
        public function set selectedDataTypeId(value:Number):void
        {
            _selectedDataTypeId = value;
        }

        public function get selectedOperateurId():Number
        {
            return _selectedOperateurId;
        }

        /**
         * Identifiant de l'opérateur
         *
         */
        public function set selectedOperateurId(value:Number):void
        {
            _selectedOperateurId = value;
        }

        public function get selectedRecuperationModeId():Number
        {
            return _selectedRecuperationModeId;
        }

        /**
         * Identifiant du mode de récupération ( Pull-MAIL , PUSH-FTP etc ...)
         *
         */
        public function set selectedRecuperationModeId(value:Number):void
        {
            _selectedRecuperationModeId = value;
        }
    }
}