<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.private.ws.WSRepository" extends="fr.consotel.api.remoting.RemoteService"
hint="Implémentation abstraite d'une Factory contenant les Web Services MDM et permettant à la fois de les instancier et d'y accéder
Cette classe est instanciée que par une implémentation fr.consotel.api.mdm.partner.private.MdmServiceProvider qui lui fourni les credentials d'authentification utilisateur">
	<cffunction access="public" name="getFactory" returntype="fr.consotel.api.remoting.RemoteService"
	hint="Cette méthode n'est pas supportée et lève une exception et ne doit pas être redéfinie par les classes dérivées">
		<cfargument name="userLogin" type="String" required="true">
		<cfargument name="userPassword" type="String" required="true">
		<cfargument name="serviceEndpoint" type="String" required="false" default="">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas supportée par cette classe">
	</cffunction>
	
	<cffunction access="public" name="getWSRepository" returntype="fr.consotel.api.mdm.partner.private.ws.WSRepository"
	hint="Retourne une instance de cette classe qui est spécifique aux credentials d'authentification fournis
	Dans l'ordre : Définit et valide les credentials, Appelle le constructeur de la classe parent sans renseigner les credentials, Définit la liste des instances des Web Services">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Peut contenir les clés supplémentaires suivantes :<br>
		LOGIN (Login), PWD (Mot de passe. Obligatoire si LOGIN est spécifié), WS_ENDPOINT (Endpoint). Le serveur ConsoTel est utilisé par défaut">
		<!--- Liste des instances des Web Services --->
		<cfif NOT structKeyExists(VARIABLES,"WS_SERVICES")>
			<cfset VARIABLES["WS_SERVICES"]={}>
		</cfif>
		<!--- Credentials d'authentification --->
		<cfset var userLogin="">
		<cfset var userPwd="">
		<cfset var wsEndpoint="">
		<cfset var AUTH=ARGUMENTS["authCredentials"]>
		<cfif structKeyExists(AUTH,"LOGIN") AND structKeyExists(AUTH,"PWD")>
			<cfset userLogin=AUTH["LOGIN"]>
			<cfset userPwd=AUTH["PWD"]>
		</cfif>
		<cfif structKeyExists(AUTH,"WS_ENDPOINT")>
			<cfset wsEndpoint=AUTH["WS_ENDPOINT"]>
		</cfif>
		<cfset setAuthCredentials(ARGUMENTS["authCredentials"])>
		<!--- Constructeur de la classe parent --->
		<cfset SUPER.getFactory(userLogin,userPwd,wsEndpoint)>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="public" name="validateAuth" returntype="void" hint="Appele authLogout() puis lève une exception si les credentials d'authentification ne sont pas validés">
		<cfif NOT getAuthManager().isAuthValid(getAuthCredentials())>
			<cfset authLogout()>
			<cfthrow type="Custom" errorcode="ACCESS_REFUSED" message="Accès Refusé (MDM Service Repository)">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="authLogout" returntype="void" hint="Cette méthode n'est pas implémentée et lève une exception
	Elle doit être redéfinie par les classes dérivées pour déconnecter l'accès aux Web Service pour les credentials fournis à getWSRepository()">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée par cette classe">
	</cffunction>
	
	<cffunction access="private" name="getAuthManager" returntype="fr.consotel.api.mdm.partner.private.auth.MdmAuthManager" hint="Gestionnaire d'authenficiation de l'API">
		<cfif NOT structKeyExists(VARIABLES,"AUTH_MANAGER")>
			<cfset VARIABLES["AUTH_MANAGER"]=createObject("component","fr.consotel.api.mdm.partner.private.auth.MdmAuthManager").getAuthManager()>
		</cfif>
		<cfreturn VARIABLES["AUTH_MANAGER"]>
	</cffunction>
	
	<cffunction access="private" name="getAuthCredentials" returntype="Struct" hint="Retourne les credentials d'authentification fournis à l'instanciation">
		<cflock scope="REQUEST" type="readonly" timeout="300" throwontimeout="TRUE">
			<cfif structKeyExists(VARIABLES,"AUTH_CREDENTIALS")>
				<cfreturn VARIABLES["AUTH_CREDENTIALS"]>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les credentials d'authentification ne sont pas définis">
			</cfif>
		</cflock>
	</cffunction>

	<cffunction access="private" name="setAuthCredentials" returntype="void" hint="Définit les credentials d'authentification">
		<cfargument name="authCredentials" type="Struct" required="true" hint="Credentials d'authentification contenant au moins les clés suivantes :
		- DOMAIN : Valeur utilisée par l'API pour déterminer l'implémentation fr.consotel.api.mdm.auth.ClientAuthManager à utiliser pour vérifier l'authentification de l'utilisateur">
		<!--- Credentials d'authentification utilisateur --->
		<cflock scope="REQUEST" type="exclusive" timeout="300" throwontimeout="TRUE">
			<cfset var userCredentials=ARGUMENTS["authCredentials"]>
			<cfset VARIABLES["AUTH_CREDENTIALS"]=userCredentials>
		</cflock>
	</cffunction>
	
	<cffunction access="private" name="getWSInstances" returntype="Struct" hint="Retourne une structure dans laquelle chaque clé identifie un Web Service associé à l'instance correspondante">
		<cfif structKeyExists(VARIABLES,"WS_SERVICES")>
			<cfreturn VARIABLES["WS_SERVICES"]>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La liste des services n'est pas définie (WS Repository)">
		</cfif>
	</cffunction>
	
	<cffunction access="private" name="getWSStub" returntype="Any" hint="Retourne l'instance de l'implémentation du service correspondant à la clé serviceName dans getWSInstances()">
		<cfargument name="serviceName" type="String" required="true" hint="Nom permettant d'identifier le service à instancier">
		<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Cette méthode n'est pas implémentée par cette classe">
	</cffunction>
</cfcomponent>