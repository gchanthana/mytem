package gestionparc.ihm.fiches
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.utils.gestionparcConstantes;
	
	public class FicheAffectationImpl extends FicheAffectationIHM
	{
		public var img_liste_big:Image;
		public var lab_titre:Label;
		public var box_listeDispo:VBox;
		public var lab_cellule_name:Label;
		public var img_liste:Image;
		public var img_cellule:Image;
		public var labDgSelection:Label;
		
		public var btValider:Button;
		public var btAnnuler:Button;
		
		private var obj_listeDispo:IListeDispo;
		
		public function FicheAffectationImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		private function init(ev:FlexEvent):void
		{
			initListeners();
			initIHM();
		}
		private function initListeners():void
		{
			(obj_listeDispo.getDataGrid() as DataGrid).addEventListener(Event.CHANGE,dgListeDispo_handler);
			btAnnuler.addEventListener(MouseEvent.CLICK,fermer);
			btValider.addEventListener(MouseEvent.CLICK,valider);
			this.addEventListener(CloseEvent.CLOSE,fermer);
		}
		private function initIHM():void
		{
			this.title = "";
			
			this.img_cellule.source 	= gestionparcConstantes.adrImg;
			this.img_liste.source		= gestionparcConstantes.adrImg;
			this.img_liste_big.source	= gestionparcConstantes.adrImg;
			this.lab_cellule_name.text 	= ""
			this.lab_titre.text			= ""
			
			this.box_listeDispo.addChild(obj_listeDispo as VBox);
		}
		
		private function dgListeDispo_handler(evt : Event):void
		{
			labDgSelection.text = obj_listeDispo.getLibelleSelection();
		}
		private function valider(e:Event):void
		{
			
		}
		private function fermer(evt : Event):void 
		{
			PopUpManager.removePopUp(this);
		}
	}
}