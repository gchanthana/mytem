<cfcomponent output="false">

	<cfset dictionnairePublication=querynew("fr,es,en,nl","varchar,varchar,varchar,varchar")>
	<cfset dictionnaireErreur=querynew("fr,es,en,nl","varchar,varchar,varchar,varchar")>
	<cfset dictionnairePartage=querynew("fr,es,en,nl","varchar,varchar,varchar,varchar")>
	<cfset dictionnaireXPublication=querynew("fr,es,en,nl","varchar,varchar,varchar,varchar")>

	<cfset init()>
	
	<cffunction access="public" name="init" returntype="Any">
		<cfset newrow=QueryAddRow(dictionnairePublication,1)>
		<cfset temp = QuerySetCell(dictionnairePublication,"fr","Publication de rapport")>
		<cfset temp = QuerySetCell(dictionnairePublication,"es","Publicación del informe")>
		<cfset temp = QuerySetCell(dictionnairePublication,"en","Report publication")>		
		<cfset temp = QuerySetCell(dictionnairePublication,"nl","Report publication")>		

		<cfset newrow=QueryAddRow(dictionnaireErreur,1)>		
		<cfset temp = QuerySetCell(dictionnaireErreur,"fr","Rapport en erreur")>
		<cfset temp = QuerySetCell(dictionnaireErreur,"es","Informe en error")>
		<cfset temp = QuerySetCell(dictionnaireErreur,"en","Report in Error")>
		<cfset temp = QuerySetCell(dictionnaireErreur,"nl","Report in Error")>
		
		<cfset newrow=QueryAddRow(dictionnairePartage,1)>		
		<cfset temp = QuerySetCell(dictionnairePartage,"fr","Partage de rapport")>
		<cfset temp = QuerySetCell(dictionnairePartage,"es","Documento compartido")>
		<cfset temp = QuerySetCell(dictionnairePartage,"en","Report sharing")>
		<cfset temp = QuerySetCell(dictionnairePartage,"nl","Report sharing")>	

		<cfset newrow=QueryAddRow(dictionnaireXPublication,1)>		
		<cfset temp = QuerySetCell(dictionnaireXPublication,"fr","Publication de plusieurs documents")>
		<cfset temp = QuerySetCell(dictionnaireXPublication,"es","Publicación de varios documentos")>
		<cfset temp = QuerySetCell(dictionnaireXPublication,"en","Publication of several documents")>
		<cfset temp = QuerySetCell(dictionnaireXPublication,"nl","Publication of several documents")>		
	</cffunction>
	
	<cffunction access="public" name="formatLocaleTime" returntype="string">
		<cfargument name="dateTime" required="true" type="date">
		<cfargument name="locale" required="true" type="string">
				
		<cftry>
			<cfset SetLocale(arguments.locale)>
		<cfcatch type="any">
			<cfset SetLocale("en")>
		</cfcatch>
		</cftry>

		<cfset _ret = LSTimeFormat(arguments.dateTime, 'LONG')>

		<cfreturn _ret>
	</cffunction>
	
	<cffunction access="public" name="formatLocaleDate" returntype="string">
		<cfargument name="dateTime" required="true" type="date">
		<cfargument name="locale" required="true" type="string">
				
		<cftry>
			<cfset SetLocale(arguments.locale)>
		<cfcatch type="any">
			<cfset SetLocale("en")>
		</cfcatch>
		</cftry>

		<cfset _ret = LsDateFormat(arguments.dateTime, 'SHORT')>

		<cfreturn _ret>
	</cffunction>
</cfcomponent>