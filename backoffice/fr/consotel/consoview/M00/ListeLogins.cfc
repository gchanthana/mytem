<cfcomponent name="GestionClient" alias="fr.consotel.consoprod.GestionClient">
	<cfset pkg = "PKG_INTRANET">
	
<cffunction name="getListeLogin" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="query">
		<cfargument name="IDGROUPE_MAITRE" required="true" type="numeric">
		<cfstoredproc datasource="#session.offreDSN#" procedure="#pkg#.getListeLogin_V2">
			<cfif IDGROUPE_MAITRE EQ -1>
				<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_MAITRE#" null="true"/>
			<cfelse>
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" value="#IDGROUPE_MAITRE#" null="false"/>
			</cfif>
			<cfprocresult name="result"/>
		</cfstoredproc>
		<cfreturn result /> 
</cffunction>
	
</cfcomponent>
