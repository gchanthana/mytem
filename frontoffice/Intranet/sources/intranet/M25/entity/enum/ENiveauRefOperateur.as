package intranet.M25.entity.enum
{
    import mx.collections.ArrayCollection;

    public class ENiveauRefOperateur extends Enumeratio
    {
        public static const CHAINE_FACTURATION:ENiveauRefOperateur = new ENiveauRefOperateur("CHAINE_FACTURATION", "Chaîne Facturation");
        public static const GROUPEMENT_CHAINES_FACTURATION:ENiveauRefOperateur = new ENiveauRefOperateur("GROUPEMENT_CHAINES_FACTURATION", "Groupement Chaînes Facturation");

        public function ENiveauRefOperateur(value:String, label:String)
        {
            super(value, label);
        }

        public static function getCollection():ArrayCollection
        {
            return new ArrayCollection(new Array(CHAINE_FACTURATION, GROUPEMENT_CHAINES_FACTURATION));
        }
    }
}