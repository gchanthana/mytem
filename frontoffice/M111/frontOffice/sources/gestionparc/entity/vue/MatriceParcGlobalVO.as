package gestionparc.entity.vue
{

	[Bindable]
	public dynamic class MatriceParcGlobalVO extends AbstractMatriceParcVO
	{

		private var _POOL:String;
		private var _IDPOOL:String;


		public function MatriceParcGlobalVO()
		{
		}

		override public function createObject(obj:Object):void
		{
			super.createObject(obj);
			this._POOL=obj.POOL;
			this._IDPOOL=obj.IDPOOL;
		}

		public function get IDPOOL():String
		{
			return _IDPOOL;
		}

		public function set IDPOOL(value:String):void
		{
			_IDPOOL=value;
		}

		public function get POOL():String
		{
			return _POOL;
		}

		public function set POOL(value:String):void
		{
			_POOL=value;
		}
	}
}
