package composants.tb.tableaux
{
	public class TableauError extends Error
	{
		public function TableauError(message:String="", id:int=0):void{
			name = "TableauError";
			message = "le tableau n'existe pas";
			id = 2;			
			super(message,id);
		}
	}
}