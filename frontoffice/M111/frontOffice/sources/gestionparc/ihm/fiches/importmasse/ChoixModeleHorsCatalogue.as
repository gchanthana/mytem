package gestionparc.ihm.fiches.importmasse
{
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import gestionparc.event.gestionparcEvent;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.equipement.EquipementServices;
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;

	[Bindable]
	public class ChoixModeleHorsCatalogue extends TitleWindow
	{
		public var dgConstructeur		:DataGrid;
		public var dgModele				:DataGrid;

		public var comboModeleFiltre	:ComboBox;
		public var cbRevendeur			:ComboBox;
		
		public var tiAutreModele		:TextInput;
		public var txtSearchModel		:TextInput;
		public var txtSearch			:TextInput;
		
		public var lblRevendeur			:Label;

		public var cbAutreModele		:CheckBox;

		public var btAnnuler			:Button;
		public var btValider			:Button;

		public var formItemModele		:FormItem;
		public var formItemModeleDansCatalogue:FormItem;
		public var formItemAutreModele	:FormItem;
		public var formItemConstructeur	:FormItem;

		public var idRevendeur			:Number;
		public var parentPanel			:String;

		public var oldModele			:String;
		public var oldMarque			:String;
		public var nouveauModele		:String;
		public var idType				:Number=70;
		public var boolInOutCatalogue	:Boolean=true;

		private var modeleChoisi:Object=new Object();
		private var serv:EquipementServices=new EquipementServices();

		public var myArrayCollection:ArrayCollection;
		public var myArrayModel:ArrayCollection;

		public function ChoixModeleHorsCatalogue()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(evt:FlexEvent):void
		{
			initListener();
			initComboBox();
			initDefaultDisplay();
		}

		private function initListener():void
		{
			this.addEventListener(CloseEvent.CLOSE, closePopup);
			dgConstructeur.addEventListener(ListEvent.CHANGE, dgConstructeurItemClickHandler);
			dgConstructeur.addEventListener(ListEvent.ITEM_CLICK, setSelectedConstructeur); // ecouteur pour la maj des modeles lorsque le revendeur change			
			txtSearch.addEventListener(Event.CHANGE, txtSearchHandler);
			comboModeleFiltre.addEventListener(ListEvent.CHANGE, dgConstructeurItemClickHandler);
			cbRevendeur.addEventListener(ListEvent.CHANGE,dgConstructeurItemClickHandler);
			dgModele.addEventListener(ListEvent.ITEM_CLICK, setSelectedModel);
			txtSearchModel.addEventListener(Event.CHANGE, txtSearchModelHandler);
			cbAutreModele.addEventListener(MouseEvent.CLICK, clickRadioButton);
			btAnnuler.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValider.addEventListener(MouseEvent.CLICK, validerHandler);
		}

		private function initComboBox():void
		{
			initComboConstructeur();
		}

		private function initComboConstructeur():void
		{
			var myArray:Array=new Array();
			var longueur:int=CacheDatas.listeConstructeur.length;

			for (var index:int=0; index < longueur; index++)
			{
				var item:Object=new Object();

				item.IDFOURNISSEUR=CacheDatas.listeConstructeur[index].IDFOURNISSEUR;
				item.NOM_FOURNISSEUR=CacheDatas.listeConstructeur[index].NOM_FOURNISSEUR;
				item.SELECTED=false;
				myArray.push(item);
			}
			myArrayCollection=new ArrayCollection(myArray.sortOn("NOM_FOURNISSEUR"));

			dgConstructeur.dataProvider=myArrayCollection;
			cbRevendeur.dataProvider=CacheDatas.listeRevendeur;
			cbRevendeur.selectedIndex=ConsoviewUtil.getIndexById(CacheDatas.listeRevendeur, "IDREVENDEUR", idRevendeur);
			lblRevendeur.text=(cbRevendeur.selectedItem as Object).LIBELLE;
			
			var tmp:int=ConsoviewUtil.getIndexByLabel(myArrayCollection, "NOM_FOURNISSEUR", oldMarque);
			if (tmp != -1)
			{
				dgConstructeur.selectedIndex=tmp;
				//scroller jusqu'à l'élement sélectionné
				if (tmp > 5)
				{
					dgConstructeur.validateNow();
					dgConstructeur.scrollToIndex(tmp-1);
				}
				myArrayCollection.getItemAt(tmp).SELECTED=true;
			}
			dgConstructeurItemClickHandler(null);
		}
		
		private function dgConstructeurItemClickHandler(evt:Event):void
		{
			var opData:AbstractOperation;

			if (comboModeleFiltre.selectedIndex == 0)
			{
				opData=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview..M111.fiches.FicheTerminal", "getModeleHorsCatalogue", dgConstructeurItemClickHandlerResultHandler);

				if (idRevendeur > 0)
				{
					RemoteObjectUtil.callService(opData, idType, dgConstructeur.selectedItem.IDFOURNISSEUR, idRevendeur)
				}
				else
				{
					ConsoviewAlert.afficherSimpleAlert('', ResourceManager.getInstance().getString('M111', 'Vous_devez_selectionner_un_revendeur'));
				}
			}
			else
			{
				opData=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview..M111.fiches.FicheTerminal", "getListeModele", dgConstructeurItemClickHandlerResultHandler);
				RemoteObjectUtil.callService(opData, idType, dgConstructeur.selectedItem.IDFOURNISSEUR);
			}
		}

		private function dgConstructeurItemClickHandlerResultHandler(evt:ResultEvent):void
		{
			if (evt.result != -1)
			{	
				if(comboModeleFiltre.selectedIndex == 0)
				{
					setListModelHorsCatalogue(evt);
					cbRevendeur.enabled=false;
					boolInOutCatalogue=true;
				}
				else
				{
					setListModelDansCatalogue(evt);
					cbRevendeur.enabled=true;
					boolInOutCatalogue=false;
				}
				var tmp:int=ConsoviewUtil.getIndexByLabel(myArrayModel, "LIBELLE_EQ", oldModele);
				if (tmp != -1)
				{
					dgModele.selectedIndex=tmp;
					myArrayModel.getItemAt(tmp).SELECTED=true;
					//scroller jusqu'à l'élement sélectionné
					if (tmp > 5)
					{
						dgModele.validateNow();
						dgModele.scrollToIndex(tmp - 1);
					}
					myArrayModel.getItemAt(tmp).SELECTED=true;
				}
			}
		}
		
		private function setListModelHorsCatalogue(evt:ResultEvent):void
		{
			var myArray:Array=new Array();
			var longueur:int=(evt.result as ArrayCollection).length;
			for (var index:int=0; index < longueur; index++)
			{
				var item:Object=new Object();
				
				item.LIBELLE_EQ=(evt.result as ArrayCollection)[index].LIBELLE_EQ;
				item.IDEQUIPEMENT_FOURNISSEUR=(evt.result as ArrayCollection)[index].IDEQUIPEMENT_FOURNISSEUR;
				item.REF_CONSTRUCTEUR=(evt.result as ArrayCollection)[index].REF_CONSTRUCTEUR;
				item.REFERENCE_EQ=(evt.result as ArrayCollection)[index].REFERENCE_EQ;
				item.REF_REVENDEUR=(evt.result as ArrayCollection)[index].REF_REVENDEUR;
				item.IDREVENDEUR=(evt.result as ArrayCollection)[index].IDREVENDEUR;
				item.SELECTED=false;
				
				myArray.push(evt.result[index]);							
			}
			
			myArrayModel=new ArrayCollection(myArray.sortOn("LIBELLE_EQ"));
			
			dgModele.dataProvider=myArrayModel;
		}
		
		private function setListModelDansCatalogue(evt:ResultEvent):void
		{
			var myArray:Array=new Array();
			var longueur:int=(evt.result as ArrayCollection).length;
			for (var index:int=0; index < longueur; index++)
			{
				var item:Object=new Object();
				if ((evt.result as ArrayCollection)[index].IDREVENDEUR==(cbRevendeur.selectedItem as Object).IDREVENDEUR)
				{
					item.LIBELLE_EQ=(evt.result as ArrayCollection)[index].LIBELLE_EQ;
					item.IDEQUIPEMENT_FOURNISSEUR=(evt.result as ArrayCollection)[index].IDEQUIPEMENT_FOURNISSEUR;
					item.REF_CONSTRUCTEUR=(evt.result as ArrayCollection)[index].REF_CONSTRUCTEUR;
					item.REFERENCE_EQ=(evt.result as ArrayCollection)[index].REFERENCE_EQ;
					item.REF_REVENDEUR=(evt.result as ArrayCollection)[index].REF_REVENDEUR;
					item.IDREVENDEUR=(evt.result as ArrayCollection)[index].IDREVENDEUR;
					item.SELECTED=false;
					
					myArray.push(evt.result[index]);
				}					
			}
			
			myArrayModel=new ArrayCollection(myArray.sortOn("LIBELLE_EQ"));
			
			dgModele.dataProvider=myArrayModel;
		}

		protected function txtSearchHandler(e:Event):void
		{
			if (myArrayCollection != null)
			{
				myArrayCollection.filterFunction=filterFunction;
				myArrayCollection.refresh();
			}
		}

		private function filterFunction(item:Object):Boolean
		{
			var rfilter:Boolean=true;

			rfilter=rfilter && (item.NOM_FOURNISSEUR != null && item.NOM_FOURNISSEUR.toLocaleLowerCase().search(txtSearch.text.toLocaleLowerCase()) != -1);

			return rfilter;
		}

		protected function txtSearchModelHandler(e:Event):void
		{
			if (myArrayModel != null)
			{
				myArrayModel.filterFunction=filterFunctionModel;
				myArrayModel.refresh();
			}
		}

		private function filterFunctionModel(item:Object):Boolean
		{
			var rfilter:Boolean=true;

			rfilter=rfilter && (item.LIBELLE_EQ != null && item.LIBELLE_EQ.toLocaleLowerCase().search(txtSearchModel.text.toLocaleLowerCase()) != -1);

			return rfilter;
		}

		private function setSelectedConstructeur(evt:ListEvent):void
		{
			var currentConstructeur:Object=evt.itemRenderer.data;

			for each (var item:Object in myArrayCollection)
			{
				if (currentConstructeur != null)
				{
					if (item.IDFOURNISSEUR == currentConstructeur.IDFOURNISSEUR)
						item.SELECTED=true;
					else
						item.SELECTED=false;
				}

				myArrayCollection.itemUpdated(item);
			}
		}

		private function setSelectedModel(evt:ListEvent):void
		{
			var currentModel:Object=evt.itemRenderer.data;

			if (!cbAutreModele.selected)
			{
				for each (var item:Object in myArrayModel)
				{
					if (currentModel != null)
					{
						if (item.IDEQUIPEMENT_FOURNISSEUR == currentModel.IDEQUIPEMENT_FOURNISSEUR)
							item.SELECTED=true;
						else
							item.SELECTED=false;
					}

					myArrayModel.itemUpdated(item);
				}
			}

		}

		private function initDefaultDisplay():void
		{
			tiAutreModele.enabled=false;
			cbAutreModele.selected=false;
		}

		private function clickRadioButton(evt:MouseEvent):void
		{
			if (cbAutreModele.selected)
			{
				dgModele.enabled=dgModele.editable=
				dgModele.selectable=
				comboModeleFiltre.enabled=
				cbRevendeur.enabled=false;
				tiAutreModele.enabled=true;
			}
			else
			{
				dgModele.enabled=
				dgModele.selectable=
				comboModeleFiltre.enabled=
				cbRevendeur.enabled=true;
				tiAutreModele.enabled=false;
				tiAutreModele.text="";
			}
		}

		private function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}

		private function closePopup(evt:CloseEvent):void
		{
			PopUpManager.removePopUp(this);
		}

		private function validerHandler(evt:MouseEvent):void
		{
			modeleChoisi.idType=idType;
			modeleChoisi.idMarque=dgConstructeur.selectedItem.IDFOURNISSEUR;
			if (comboModeleFiltre.selectedIndex !=0)
			{
				idRevendeur=(cbRevendeur.selectedItem as Object).IDREVENDEUR;
			}
			modeleChoisi.idRevendeur=idRevendeur;
			//Si ça n'est pas un nouveau modèle :
			if (!(cbAutreModele.selected))
			{
				if (dgConstructeur.selectedIndex == -1 || dgModele.selectedIndex == -1)
				{
					Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un_revendeur_et_un_mod_le'));
				}
				else
				{
					var opData:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.fiches.FicheTerminal", "insertModeleRevendeur", validerResultHandler);
					RemoteObjectUtil.callService(opData, dgModele.selectedItem.IDEQUIPEMENT_FOURNISSEUR, idRevendeur);
					modeleChoisi.modele=dgModele.selectedItem.LIBELLE_EQ;
					modeleChoisi.marque=dgConstructeur.selectedItem.NOM_FOURNISSEUR;
				}
			}
			else // Si c'est un nouveau modèle : 
			{
				if (dgConstructeur.selectedIndex == -1 || tiAutreModele.text.length == 0)
				{
					Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un_revendeur_et_une_marque_'));
				}
				else
				{
					var opDataNewModele:AbstractOperation=RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M111.fiches.FicheTerminal", "addEquipementRevendeur", validerResultHandler);
					RemoteObjectUtil.callService(opDataNewModele, tiAutreModele.text, "", "ND", dgConstructeur.selectedItem.IDFOURNISSEUR, idType, idRevendeur, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					modeleChoisi.modele=tiAutreModele.text;
					modeleChoisi.marque=dgConstructeur.selectedItem.NOM_FOURNISSEUR;
				}
			}
		}

		private function validerResultHandler(evt:ResultEvent):void
		{
			if (evt.result && ((evt.result.hasOwnProperty("IDEQUIP") && evt.result.IDEQUIP < 0) || evt.result < 0))
			{
				ConsoviewAlert.afficherError(gestionparcMessages.MODELES_EXISTANT, gestionparcMessages.ERREUR);
				return;
			}


			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Nouveau_mod_le_ajout_'), Application.application as DisplayObject);
			closePopup(null);

			if (!(cbAutreModele.selected)) // pour selectionner automatiquement celui qu'on vient de chosir
			{
				if (parentPanel == "importDeMasse")
				{
					nouveauModele=dgModele.selectedItem.LIBELLE_EQ;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, nouveauModele));
				}
				else
				{
					modeleChoisi.idModele=evt.result as int;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, modeleChoisi));
				}
			}
			else
			{
				if (parentPanel == "importDeMasse")
				{
					nouveauModele=tiAutreModele.text;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, nouveauModele));
				}
				else
				{
					modeleChoisi.idModele=evt.result.IDEQUIP;
					dispatchEvent(new gestionparcEvent(gestionparcEvent.NEW_MODELE_ADDED, modeleChoisi));
				}
			}
		}

	}
}