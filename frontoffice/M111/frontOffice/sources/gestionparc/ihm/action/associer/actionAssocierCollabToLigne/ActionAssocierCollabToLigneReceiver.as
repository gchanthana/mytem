package gestionparc.ihm.action.associer.actionAssocierCollabToLigne
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.ligne.FicheAssocierCollabToLigne;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierCollabToLigneReceiver
	{
		private var fiche:FicheAssocierCollabToLigne;

		public function ActionAssocierCollabToLigneReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheAssocierCollabToLigne();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
