package univers.parametres.rago.inventaire.ihm
{
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.controls.CheckBox;
	import mx.controls.Image;
	
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.inventaire.event.InventaireReglesEvent;
	
	[Bindable] 
	public class ActionsItemRendererImpl extends HBox
	{
		public var imgView:Image;
		public var ckbActiver:CheckBox;
		public var imgRefresh:Image;
		public var imgDelete:Image; 
	 
		
		private var boolData:Boolean = false;
		
		public function ActionsItemRendererImpl()
		{
			super();
			addEventListener(MouseEvent.CLICK, _localeClickHandler);
		}
		
		override protected function commitProperties():void
		{	
			if(boolData)
			{ currentState = '' }
			else
			{ currentState = '' }
		}
		
		override public function set data(value:Object):void
		{
			boolData = false;
			
			if(value)
			{
				super.data = value;
				
				if(!data.hasOwnProperty("IDREGELE_ORGA"))
				{ boolData = false; }
				else
				{ boolData = true; }		
			}
			
			
			invalidateProperties();
		}
		
		

		protected function _localeClickHandler(event:MouseEvent):void
		{
			if(event == null) return;			
		
			switch(event.target.id)
			{
				case "ckbActiver":
				{
					dispatchEvent(new InventaireReglesEvent(data as Regle,InventaireReglesEvent.ACTIVER_REGLE_CLICKED,true));
					break;
				}
				case "imgView":
				{
					dispatchEvent(new InventaireReglesEvent(data as Regle,InventaireReglesEvent.VOIR_REGLE_CLICKED,true));
					break;
				}
				case "imgRefresh":
				{
					dispatchEvent(new InventaireReglesEvent(data as Regle,InventaireReglesEvent.EXECUTER_REGLE_CLICKED,true));
					break;
				}
				case "imgDelete":
				{
					dispatchEvent(new InventaireReglesEvent(data as Regle,InventaireReglesEvent.ANNULER_REGLE_CLICKED,true));
					break;
				}
				case "imgAdd":
				{
					dispatchEvent(new InventaireReglesEvent(new Regle(),InventaireReglesEvent.CREER_REGLE_CLICKED,true));
					break;
				}
				default:
				{
					//do nothing	
				}
			}
		
		}
		
		
	}
}