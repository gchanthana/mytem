package gestionparc.ihm.main.vue
{
	import gestionparc.entity.AbstractVue;	
	import mx.collections.ArrayCollection;

	public class VueParcGlobal extends AbstractVue implements IVue
	{
		private var _colonneList:ArrayCollection = new ArrayCollection();

		public function VueParcGlobal(colonneList:ArrayCollection)
		{
			if(colonneList){
				_colonneList = colonneList;
			}
			super(_colonneList);
			createVue(GestionnaireVue.nomVueParcGlobal, AbstractVue.MIXTE, 0, null, true, false, false, true, true, true, true, false);
		}

		/**
		 * permet d'exportre la vue
		 */
		override public function exportAll():void
		{
			exportService.exporterViewParc();
		}

		override public function setColonne(value:ArrayCollection):void{
			createVue(GestionnaireVue.nomVueParcGlobal, AbstractVue.MIXTE, 0, createListeColonne(value), true, false, false, true, true, true, true, false);
		}

		/**
		 * creer la vue de parc avec ses colonnes
		 * @return
		 */
		private function createListeColonne(value:ArrayCollection):ArrayCollection
		{
			var arrayColonnes :ArrayCollection=new ArrayCollection();
			var listeColonnes:ArrayCollection=value;
			return arrayColonnes=createAllColonnes(listeColonnes);
		}
	}
}
