<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseRegistry" extends="fr.consotel.api.mdm.partner.public.ZenpriseProvisioning"
hint="Implémentation Registry Service. Le séparateur de chemin pour les clés est \ (WINDOWS)">
	<!--- Registry Service --->
	<cffunction access="remote" name="addRegistryKey" returntype="Void" hint="Ajoute une nouvelle clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset registryService.addRegistryKey(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
	</cffunction>
	
	<cffunction access="remote" name="addRegistryValue" returntype="Void" hint="Ajoute une nouvelle propriété dans une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfargument name="value" type="String" required="true" hint="Valeur de la propriété">
		<cfargument name="valType" type="Numeric" required="true" hint="Type de la valeur">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset registryService.addRegistryValue(
			ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"],ARGUMENTS["valName"],ARGUMENTS["value"],ARGUMENTS["valType"]
		)>
	</cffunction>
	
	<cffunction access="remote" name="updateRegistryValue" returntype="Void" hint="Met à jour la valeur d'une propriété dans une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfargument name="value" type="String" required="true" hint="Valeur de la propriété">
		<cfargument name="valType" type="Numeric" required="true" hint="Type de la valeur">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset registryService.updateRegistryValue(
			ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"],ARGUMENTS["valName"],ARGUMENTS["value"],ARGUMENTS["valType"]
		)>
	</cffunction>
	
	<cffunction access="remote" name="removeRegistryKey" returntype="Void" hint="Supprime une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset registryService.removeRegistryKey(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
	</cffunction>
	
	<cffunction access="remote" name="getRegistryValue" returntype="String" hint="Retourne la valeur d'une propriété d'une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="String" required="true" hint="Nom de la propriété">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfreturn serializeJSON(registryService.getRegistryValue(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"],ARGUMENTS["valName"]))>
	</cffunction>
	
	<cffunction access="remote" name="removeRegistryValue" returntype="Void" hint="Supprime des propriétés d'une clé de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfargument name="valName" type="Array" required="true" hint="Liste des propriétés à supprimer">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset registryService.removeRegistryValue(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"],ARGUMENTS["valName"])>
	</cffunction>
	
	<cffunction access="remote" name="createTypedConfiguration" returntype="Void" hint="Crée une configuration registre typé">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration registre typé">
		<cfargument name="typeName" type="String" required="true" hint="Type de la configuration. Valeurs possibles : Chaine vide, ZDM, SCHEDULING, UNINSTALL">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset registryService.createTypedConfiguration(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["typeName"])>
	</cffunction>
	
	<cffunction access="remote" name="removeConfiguration" returntype="Void" hint="Supprime une configuration de registre">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration registre typé">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset registryService.removeConfiguration(ARGUMENTS["osFamily"],ARGUMENTS["config"])>
	</cffunction>
	
	<cffunction access="remote" name="getConfigurationList" returntype="String" hint="Retourne un tableau contenant la liste de toutes les configurations">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset var resultList=registryService.getConfigurationList()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getOsRegistryKeysByFullPath" returntype="String" hint="Retourne un tableau contenant la liste des chemins des clés de registre pour un OS">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre ou une chaine vide pour la racine">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset var resultList=registryService.getOsRegistryKeysByFullPath(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getRegistryKeysByFullPath" returntype="String" hint="Retourne un tableau contenant la liste des chemins des clés de registre">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre ou une chaine vide pour la racine">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset var resultList=registryService.getRegistryKeysByFullPath(ARGUMENTS["config"],ARGUMENTS["fullPath"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getRegistryValueNamesByFullPath" returntype="String" hint="Retourne un tableau contenant la liste des noms de valeurs incluant par chemins">
		<cfargument name="osFamily" type="String" required="true" hint="OS Cible (e.g : WINDOWS, ANDROID, iOS)">
		<cfargument name="config" type="String" required="true" hint="Nom de la configuration">
		<cfargument name="fullPath" type="String" required="true" hint="Chemin absolu de la clé de registre">
		<cfset var registryService=getManagementService().getRegistryService()>
		<cfset var resultList=registryService.getRegistryValueNamesByFullPath(ARGUMENTS["osFamily"],ARGUMENTS["config"],ARGUMENTS["fullPath"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
</cfcomponent>