<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseServerGroup" extends="fr.consotel.api.mdm.partner.public.ZenpriseXml" hint="Implémentation ServerGroup Service">
	<!--- ServerGroup Service --->
	<cffunction access="remote" name="addServerToServerGroup" returntype="String" hint="Ajoute une serveur à un groupe de serveur et retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.addServerToServerGroup(ARGUMENTS["serverGroupId"],ARGUMENTS["serverId"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="changeServerPriority" returntype="String" hint="Change la priorité d'un serveur et retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfargument name="isUpPriority" type="boolean" required="true" hint="TRUE pour une priorité élévée et FALSE sinon">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.changeServerPriority(ARGUMENTS["serverGroupId"],ARGUMENTS["serverId"],ARGUMENTS["isUpPriority"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="createServer" returntype="String" hint="Crée un nouveau serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverName" type="String" required="true" hint="Nom du serveur">
		<cfargument name="serverHost" type="String" required="true" hint="Nom de machine du serveur">
		<cfargument name="serverPort" type="numeric" required="true" hint="Numéro de port du serveur">
		<cfargument name="serverIsSSL" type="boolean" required="true" hint="TRUE si le serveur utilise SSL et FALSE sinon">
		<cfargument name="serverUseProxy" type="boolean" required="true" hint="TRUE si le serveur utilise un proxy et FALSE sinon">
		<cfargument name="serverIsDefault" type="boolean" required="true" hint="TRUE si le serveur est à utiliser par défaut et FALSE sinon">
		<cfargument name="ipBegin" type="Array" required="true" hint="Tableau contenant la plage de début d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="ipEnd" type="Array" required="true" hint="Tableau contenant la plage de fin d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.createServer(
			ARGUMENTS["serverGroupId"],ARGUMENTS["serverName"],ARGUMENTS["serverHost"],ARGUMENTS["serverPort"],ARGUMENTS["serverIsSSL"],
			ARGUMENTS["serverUseProxy"],ARGUMENTS["serverIsDefault"],ARGUMENTS["ipBegin"],ARGUMENTS["ipEnd"]
		)>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="createServerGroup" returntype="String" hint="Crée un groupe de serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupName" type="String" required="true" hint="Nom du groupe de serveur">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.createServerGroup(ARGUMENTS["serverGroupName"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="deleteServer" returntype="String" hint="Supprime un ou plusieurs serveurs. Retourne 0 en cas de succès et -1 en cas d'erreur<br>
	<b>Bug (Zenprise) :</b> Cette méthode lève une exception car elle n'est pas correctement supportée">
		<cfargument name="serverIds" type="Array" required="true" hint="Tableau contenant les ID des serveurs à supprimer">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.deleteServer(ARGUMENTS["serverIds"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="deleteServerGroup" returntype="String" hint="Supprime un groupe de serveur. Retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.deleteServerGroup(ARGUMENTS["serverGroupId"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="getDefaultServerInfos" returntype="String" hint="Retourne un tableau contenant les infos du serveur par défaut<br>
	Le 1er élément du tableau contient le nom d'une propriété et le 2ème élément contient la valeur correspondante et ainsi de suite">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.getDefaultServerInfos()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="getServerFromServerGroup" returntype="String" hint="Retourne un tableau contenant les infos d'un serveur d'un groupe de serveur<br>
	Le 1er élément du tableau contient le nom d'une propriété et le 2ème élément contient la valeur correspondante et ainsi de suite">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.getServerFromServerGroup(ARGUMENTS["serverGroupId"],ARGUMENTS["serverId"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="getServerGroupManager" returntype="String" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.getServerGroupManager()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>

	<cffunction access="remote" name="getServerListfromServerGroup" returntype="String" hint="Retourne la liste des serveur d'un groupe de serveur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur ou -1 pour retourner tous les serveurs">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.getServerListfromServerGroup(ARGUMENTS["serverGroupId"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="getServerManager" returntype="String" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.getServerManager()>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="removeServerFromServerGroup" returntype="String"
	hint="Supprime un ou plusieurs serveurs d'un groupe de serveur. Retourne 0 en cas de succès et -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverIds" type="Array" required="true" hint="Tableau contenant les ID des serveurs à supprimer">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.removeServerFromServerGroup(ARGUMENTS["serverGroupId"],ARGUMENTS["serverIds"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="setServerGroupManager" returntype="void" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfargument name="serverGroupManager" type="Struct" required="true" hint="Structure représentant un groupe de serveur">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset serverGroupService.setServerGroupManager(ARGUMENTS["serverGroupManager"])>
	</cffunction>
	
	<cffunction access="remote" name="setServerManager" returntype="void" hint="N.D (Cette méthode lève une exception car elle n'est pas supportée par Zenprise actuellement)">
		<cfargument name="serverManager" type="Struct" required="true" hint="Structure représentant un serveur">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset serverGroupService.setServerManager(ARGUMENTS["serverManager"])>
	</cffunction>
	
	<cffunction access="remote" name="updateServer" returntype="String" hint="Met à jour les infos d'un serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupId" type="numeric" required="true" hint="ID du groupe de serveur">
		<cfargument name="serverName" type="String" required="true" hint="Nom du serveur">
		<cfargument name="serverHost" type="String" required="true" hint="Nom de machine du serveur">
		<cfargument name="serverPort" type="numeric" required="true" hint="Numéro de port du serveur">
		<cfargument name="serverIsSSL" type="boolean" required="true" hint="TRUE si le serveur utilise SSL et FALSE sinon">
		<cfargument name="serverUseProxy" type="boolean" required="true" hint="TRUE si le serveur utilise un proxy et FALSE sinon">
		<cfargument name="serverIsDefault" type="boolean" required="true" hint="TRUE si le serveur est à utiliser par défaut et FALSE sinon">
		<cfargument name="ipBegin" type="Array" required="true" hint="Tableau contenant la plage de début d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="ipEnd" type="Array" required="true" hint="Tableau contenant la plage de fin d'adresses IP (e.g : 192.168.1.1, 192.168.1.2, etc...)">
		<cfargument name="serverId" type="numeric" required="true" hint="ID du serveur">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.updateServer(
			ARGUMENTS["serverGroupId"],ARGUMENTS["serverName"],ARGUMENTS["serverHost"],ARGUMENTS["serverPort"],ARGUMENTS["serverIsSSL"],
			ARGUMENTS["serverUseProxy"],ARGUMENTS["serverIsDefault"],ARGUMENTS["ipBegin"],ARGUMENTS["ipEnd"],ARGUMENTS["serverId"]
		)>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
	
	<cffunction access="remote" name="updateServerGroup" returntype="String" hint="Met à jour les infos d'un groupe de serveur et retourne son ID ou -1 en cas d'erreur">
		<cfargument name="serverGroupName" type="String" required="true" hint="Nom du groupe de serveur">
		<cfargument name="id" type="numeric" required="true" hint="N.D">
		<cfset var serverGroupService=getManagementService().getServerGroupService()>
		<cfset var resultList=serverGroupService.updateServerGroup(ARGUMENTS["serverGroupName"],ARGUMENTS["id"])>
		<cfreturn serializeJSON(resultList)>
	</cffunction>
</cfcomponent>