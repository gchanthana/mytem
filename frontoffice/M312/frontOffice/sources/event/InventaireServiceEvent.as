package event
{
	import flash.events.Event;
	
	public class InventaireServiceEvent extends Event
	{	
		public static const INVENTAIRE_SERVICE_MODEL_UPDATED:String = "inventaireServiceModelUpdated";		
		public static const INVENTAIRE_SERVICE_MODEL_LMARQUE_UPDATED:String = "inventaireServiceModelLmarqueUpdated";
		public static const INVENTAIRE_SERVICE_MODEL_LMODELE_UPDATED:String = "inventaireServiceModelLmodeleUpdated";
		public static const INVENTAIRE_SERVICE_MODEL_LEQPT_UPDATED:String = "inventaireServiceModelLeqptUpdated";
		public static const INVENTAIRE_SERVICE_MODEL_DATE_COMPLETE_INVENTAIRE_UPDATED:String = "inventaireServiceModelDateCompleteInventaireUpdated";		
				
		
		public function InventaireServiceEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new InventaireServiceEvent(type, bubbles, cancelable)
		}
	}
}