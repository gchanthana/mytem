package intranet.GestionDesLogin.services
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import intranet.GestionDesLogin.vo.LoginExtranetVo;
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import paginatedatagrid.pagination.vo.ItemIntervalleVO;
	
	import searchpaginatedatagrid.ParametresRechercheVO;
	
	[Event(name="deleteComplete")]
	
	[Bindable]
	public class Service extends EventDispatcher
	{
		public static const DELETE_COMPELTE:String = "deleteComplete";
		public static const NBITEMPARPAGE:Number = 100;
		
		public var listeLogin:ArrayCollection;
		public var nbRecord:int ;
				
		public function Service()
		{
		}
		
		public function getListeLogins(parametresRechercheVO : ParametresRechercheVO  , itemIntervalleVO : ItemIntervalleVO, operateurid:Number=0):void
		{
			listeLogin = null;
			nbRecord = 0 ; 
			// paramètres par défaut si aucun param de recherches n'a été défini
			var string_search:String = "LOGIN_EXTRANET,";
			var string_order:String = "LOGIN_EXTRANET, ASC";
			if ( parametresRechercheVO ) {
				string_search = parametresRechercheVO.SEARCH_TEXT ;
				string_order = parametresRechercheVO.ORDER_BY
			}
			
			var tmpope:AbstractOperation = RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondeslogins.GestionDesLogins",
						"DisplayLogin_v2",
						getListeLoginsResultHandler, faultHandler);
						
			RemoteObjectUtil.callService(tmpope,
				string_search,
												string_order,
												itemIntervalleVO.indexDepart,
												itemIntervalleVO.tailleIntervalle,operateurid);
		}
		
		public function getListeLoginsResultHandler(e:ResultEvent):void
		{
			var tmp:ArrayCollection = e.result as ArrayCollection;
			if (tmp.length > 0)
			{
				nbRecord = tmp.getItemAt(0).NBRECORD ;
			} else {
				nbRecord = 0 ;
			}
		
			var cursor:IViewCursor = tmp.createCursor();
			var item:LoginExtranetVo;
			if(!listeLogin) listeLogin = new ArrayCollection()
			while(!cursor.afterLast)
			{
				item = new LoginExtranetVo();
				item.fill(cursor.current);
				listeLogin.addItem(item);				
				cursor.moveNext()
			}	
		}
		
		
		
		public function deleteLogin(login:LoginExtranetVo):void
		{	
			if(!login) return
			
			
			
			
			var handler:Function = function deleteLoginResultHandler(e:ResultEvent):void
			{
				if(!listeLogin)return
				listeLogin.removeItemAt(listeLogin.getItemIndex(login));
				dispatchEvent(new Event(DELETE_COMPELTE));
			} 			
			
			var tmpope:AbstractOperation=RemoteObjectUtil.getOperationFrom(
						RemoteObjectUtil.DEFAULT_DESTINATION,
						"fr.consotel.consoprod.gestiondeslogins.GestionDesLogins",
						"DeleteLogin",
						handler, faultHandler);
						
			RemoteObjectUtil.callService(tmpope,login.LOGIN_EXTRANET);
		}
		
		
		
		public function faultHandler(e:FaultEvent):void{
			trace('GestionDesLogin  :'+e.message);
		}

	}
}