package datalert.services{	import composants.util.ConsoviewAlert;
	
	import datalert.event.AnnuaireServiceEvent;
	import datalert.vo.AnnauireVO;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
		public class AnnuaireService extends EventDispatcher	{				public var areasCode:ArrayCollection = new ArrayCollection();		public function AnnuaireService(target:IEventDispatcher=null)		{			super(target);		}				public function addItem(value:AnnauireVO):void		{				if(value)			{				var op:AbstractOperation = RemoteObjectUtil.getOperation("fr.consotel.consoview.M26.services.annuaire.AnnuaireService"					,"addItem",addItemHandler);				RemoteObjectUtil.callService(op,value,0);				}		}				private function addItemHandler(re:ResultEvent):void		{			if(re.result > 0)			{				ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M26', 'ok'));				dispatchEvent(new AnnuaireServiceEvent(AnnuaireServiceEvent.ADD_ITEM));			}			else			{				ConsoviewAlert.afficherError(ResourceManager.getInstance().getString('M26', 'Erreur'),ResourceManager.getInstance().getString('M26', 'mytem'));			}		}				public function getAreasCode():void		{			var op:AbstractOperation = RemoteObjectUtil.getOperation("fr.consotel.consoview.M26.services.annuaire.AnnuaireService", 				"getAreasCode", getAreasCodeHandler);			RemoteObjectUtil.callService(op);		}				private function getAreasCodeHandler(re:ResultEvent):void		{			if (re.result)			{				areasCode = re.result as ArrayCollection;				dispatchEvent(new AnnuaireServiceEvent(AnnuaireServiceEvent.AREAS_CODE));			}		}	}}