function remoteCall(service:String, adresse:String,fonction:String,data:Array):Void {
	var connection:mx.remoting.Connection = mx.remoting.NetServices.createGatewayConnection(adresse);
	var myService:mx.remoting.NetServiceProxy;
	var responseHandler = {};
	responseHandler.onResult = function( results: Object ):Void {
		var v="result_"+fonction;
		eval(v)(results);
		mx.managers.CursorManager.removeAllCursors();
	}
	responseHandler.onStatus  = function( stat: Object ):Void {
		mx.managers.CursorManager.removeAllCursors();
		alert("Error while calling cfc:" + stat.description);
		var v="error_"+fonction;
		eval(v)(stat);
	}
	myService = connection.getService(service, responseHandler );
	myService[fonction](data);
}

/*
Fonction pour filtrer une colonne (colonne) d'un Grid control (grdDest) avec 
une chaine saisie dans un champs texte (chpsFiltre)
ex: onchange="FiltreGrid(data,forInput,'libelle');"
*/
function FiltreGrid(grdDest:Object,chpsFiltre:Object,colonne:String):Void {
	if(_global.arrMembers == undefined) _global.arrMembers = grdDest.dataProvider.slice(1);
	var arrMembers = _global.arrMembers;
	var arrDisplay:Array = [];
	var fortext = chpsFiltre.text.toLowerCase();
	var selected = colonne.toUpperCase();
	
	for(var i = 0; i < arrMembers.length; i++)
	 {
		if (arrMembers[i][selected].toLowerCase().indexOf(fortext) != -1)
		{
			arrDisplay.push(arrMembers[i]);
		}
	}
	grdDest.dataProvider = arrDisplay;
}