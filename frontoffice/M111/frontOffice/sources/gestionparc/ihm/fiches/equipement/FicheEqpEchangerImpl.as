package gestionparc.ihm.fiches.equipement
{
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;

	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.listedispo.DispoEqp;
	import gestionparc.services.equipement.EquipementServices;
	import gestionparc.utils.gestionparcConstantes;

	import mx.containers.TitleWindow;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.DateField;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheEqpEchangerImpl extends TitleWindow
	{
		public var img_cellule:Image;
		public var img_liste:Image;
		public var img_liste_big:Image;
		public var lab_cellule_name:Label;
		public var lab_titre:Label;
		public var labDgSelection:Label;
		public var box_listeDispo:VBox;
		public var calendar:CvDateChooser;
		public var btAnnuler:Button;
		public var btValider:Button;

		private var obj_liste:DispoEqp;
		private var serv:EquipementServices;

		public function FicheEqpEchangerImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, initIHM)
		}

		private function initIHM(evt:FlexEvent):void
		{
			serv=new EquipementServices();
			obj_liste=new DispoEqp();

			img_cellule.source=gestionparcConstantes.adrImgTerm; //Afficher l'icone de la cellule séléctionné
			img_liste.source=obj_liste.getSourceIcone(); //Afficher l'icone de la liste
			img_liste_big.source=obj_liste.getSourceIcone(); //Afficher l'icone de la liste
			lab_cellule_name.text=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI; //Affichage du nom de la cellule séléctionné
			lab_titre.text="" + obj_liste.getLibelleType() + ""; //Affichage du type d'objet de la liste
			box_listeDispo.addChild(obj_liste as VBox); //Ajout du composant contenant la dg + le filtre
			calendar.selectedDate=new Date(); // Selection de la date actuel dans la date du jour
			labDgSelection.text=obj_liste.getLibelleSelection();

			serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, associationHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);
			addEventListener(CloseEvent.CLOSE, fermer);

			obj_liste.addEventListener(MouseEvent.CLICK, dgListeDispo_handler);
			addEventListener(MouseEvent.CLICK, dgListeDispo_handler);
		}

		private function dgListeDispo_handler(evt:Event):void
		{
			labDgSelection.text=obj_liste.getLibelleSelection();
		}

		private function valider(evt:MouseEvent):void
		{
			if (obj_liste.getIDSelection() != -1)
			{
				echangerEqp()
			}
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
		}

		private function echangerEqp():void
		{
			serv.echangerEqp(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idOldeqp=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.idNeweqp=obj_liste.getSelection().ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ITEM2=obj_liste.getSelection().IMEI;
			obj.ID_TERMINAL1=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_TERMINAL2=obj_liste.getSelection().IDTERM;
			obj.ID_SIM=0;
			
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(calendar.selectedDate, 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}

		private function associationHandler(evt:gestionparcEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Echange_du_terminal_effectu_'), Application.application as DisplayObject);
			fermer(null);
		}

		private function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}
	}
}
