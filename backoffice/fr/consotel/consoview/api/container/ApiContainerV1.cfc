 <cfcomponent displayname="Interface de communication avec flex" hint="API de communication" output="false">
	<cfset FTPserver = "pelican">
	<cfset FTPport = 21>	
	<cfset FTPusername = "container">
	<cfset FTPpassword = "container">
	<cfset OFFREDSN = "ROCOFFRE">
	<cfset DEV_SUPPORT_ADDR="monitoring@saaswedo.com">
	<cfset uuid = CreateUuid()>
	
<!--- METHODE PUBLIC --->
	<!--- LE FTP --->	
		<!--- COPIE APRES GENERATION DU RAPPORT VIA .CFM --->
		<cffunction name="UPLOADDocumentCfmToFTP" 		access="remote" returntype="Numeric"
					hint="Méthode de création de document sur le ftp Container à partir d'un rapport .cfm. fileContent = contenu binaire (array of bytes) ">
			<cfargument name="fileContent" 		type="Any"		required="true">
			<cfargument name="fileName"			type="string"	required="true">
			<cfargument name="fileExtension"	type="String" 	required="true">
			
			<cfoutput><br>Appel : api.UPLOADDocumentCfmToFTP</cfoutput>
			
			<cfftp action="open" username="#FTPusername#" password="#FTPpassword#" port="#FTPport#" server="#FTPserver#" connection="ftp">
 			<cfif #cfftp.Succeeded#>
				<cfoutput><br>Connecté au FTP<br>dir : #getTempDirectory()#</cfoutput>
				<cffile action="write" output="#fileContent#" file="#getTempDirectory()#/#fileName#.#fileExtension#" addNewLine="false">
				<cfftp action="putfile" localfile="#getTempDirectory()#/#fileName#.#fileExtension#" remotefile="#fileName#.#fileExtension#" connection="ftp" failifexists="false" transfermode="binary">
				<cffile action="delete" file="#getTempDirectory()#/#fileName#.#fileExtension#">
				
				<cfoutput><br>#cfftp.ReturnValue#</cfoutput>
				
				<cfif Find("226",#cfftp.ReturnValue#) gt 0>
					<cfoutput><br>Api.UploadDocumentCfmToFTP : Copie réalisée avec succés</cfoutput>
					<cfreturn 1>
				<cfelse>
					<cfoutput><br>Api.UploadDocumentCfmToFTP : Echec de la copie</cfoutput>
					<cfreturn -1>
				</cfif>
			<cfelse>
				<cfoutput><br>Api.UploadDocumentCfmToFTP : Impossible de se connecter au ftp</cfoutput>
				<cfreturn -2>
			</cfif>
			<cfftp action="close" connection="ftp">
			<cfoutput><br>Fin : api.UPLOADDocumentCfmToFTP<br><br></cfoutput>
		</cffunction>
		<!--- RENOMMER UN FICHIER SUR LE FTP --->
		<cffunction name="RENAMEDocumentToFTP" 			access="remote" returntype="string" 	hint="renomme un fichier présent sur le FTP. Utilisé quand on ajoute un document à la table container. Permet de vérifier que le fichier sur le FTP a le nom correct">	
			<cfargument name="fileName"			type="string"	required="true">
			<cfargument name="fileExtension"	type="String" 	required="true">
			<cfargument name="UUID"				type="string" 	required="true">
			
			<cflog text="<br>Appel : api.RenameDocumentToFtp">
			
			<cfftp action="open" username="#FTPusername#" password="#FTPpassword#" port="#FTPport#" server="#FTPserver#" connection="ftp">
 			<cfif #cfftp.Succeeded#>
				<cflog text="<br>Connecté au FTP<br>fichier : #fileName#.#fileExtension#<br>">
				<cfftp action="existsfile" remotefile="#fileName#.#fileExtension#" connection="ftp">
				<cflog  text="********************************************** ApiContainerV1 - RenameDocumentToFTP : Fichier existe ? : #cfftp.ReturnValue#">
				<cfif Find("True",cfftp.ReturnValue) gt 0>
					<cfftp action="rename" existing="#fileName#.#fileExtension#" new="#UUID#" connection="ftp">
					<cflog  text="********************************************** ApiContainerV1 - RenameDocumentToFTP : #cfftp.ReturnValue#">
					<cfif Find("250",cfftp.ReturnValue) gt 0>
						<cflog text="(REUSSITE)Renommage du fichier #fileName#.#fileExtension# en #UUID#">
						<cfreturn 1>
					<cfelse>
						<cflog text="(ECHEC)Renommage du fichier #fileName#.#fileExtension# en #UUID#">
						<cfreturn -3>
					</cfif>
					
				<cfelse>
					<cflog text="(ECHEC)Le fichier #fileName#.#fileExtension# est inexistant sur le FTP Container">
					<cfreturn -2>
				</cfif>
			<cfelse>
				<cflog text="<br>Erreur de connection au FTP">
				<cfreturn -1>
			</cfif>
			<cflog text="<br>fin : api.RenameDocumentToFtp<br>">
		</cffunction>
	<!--- GESTION TABLE CONTAINER --->	
		<!--- AJOUTER UN DOCUMENT  ---> 
		<cffunction name="ADDDocumentTableContainer" 	access="remote" returntype="any" 		hint="méthode d'ajout d'un document dans la table CONTAINER. Le document en question devra être copié également sur le FTP">		
			<cfargument name="NOM_DOC" 				type="string" 	required="true" 	hint="NOM LONG DU DOCUMENT (avec espace)"> 
			<cfargument name="SHORT_NAME" 			type="string" 	required="true" 	hint="NOM COURT DU DOCUMENT (sans espace)"> 
			<cfargument name="JOB_ID" 				type="numeric" 	required="false" 	hint="ID DU JOB DANS BI"> 
			<cfargument name="CODE_RAPPORT"			type="string" 	required="false" 	hint="CODE DU DOCUMENT"> 
			<cfargument name="APP_LOGINID" 			type="numeric" 	required="false" 	hint="ID DE L'UTILISATEUR CONNECTE"> 
			<cfargument name="IDRACINE" 			type="numeric" 	required="false" 	hint="IDRACINE"> 
			<cfargument name="NOM_MODULE" 			type="string" 	required="false" 	hint="MODULE D'OU A ETE DEMANDE LE DOCUMENT"> 
			<cfargument name="FORMAT" 				type="string"	required="false" 	hint="FORMAT DU DOCUMENT"> 
			<cfargument name="DATE" 				type="string" 	required="false" 	hint="DATE DE PLANIFICATION DU JOB SOUS BI"> 
			<cfargument name="SERVER_BI" 			type="string" 	required="false" 	hint="NOM DU SERVER BI"> 
			<cfargument name="APPLI" 				type="string" 	required="false" 	hint="ID DE L'APPLI"> 
			<cfargument name="IS_PUBLIC" 			type="numeric" 	required="false" 	hint="un truc inconnu"> 
			<cfargument name="STATUS" 				type="numeric" 	required="false" 	hint="LE STATUS DU DOCUMENT => VALEUR POSSIBLE : 2 DISPONIBLE, 3 ERREUR, 1 ENCOURS">
			<cfargument name="TAILLE" 				type="numeric" 	required="false" 	hint="LA TAILLE DU DOCUMENT NON ARCHIVE">
			<cfargument name="TAILLEZIP" 			type="numeric"	required="false" 	hint="LA TAILLE DU DOCUMENT ARCHIVE (.ZIP)">
			<cfargument name="CODEAPPLICATION"		type="numeric"	required="false" 	hint="Le code application qui fait la demande" default="1">
			<cfset var ret=-1>
			
			<cfoutput><br>Appel : api.ADDDocumentTableContainer<br></cfoutput>
			<cflog text="Appel : api.ADDDocumentTableContainer">
			<cflog text="fichier : #NOM_DOC#">
			
			
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.CONTAINERPUBLISHRAPPORT_V4">
				<cfprocparam value="#uuid#"					cfsqltype="CF_SQL_VARCHAR"  type="in">			
				<cfprocparam value="#JOB_ID#" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#NOM_DOC#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#SHORT_NAME#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#CODE_RAPPORT#" 		cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#APP_LOGINID#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#IDRACINE#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#NOM_MODULE#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#FORMAT#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#DATE#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#SERVER_BI#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#APPLI#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#IS_PUBLIC#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#TAILLE#" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#TAILLEZIP#" 			cfsqltype="CF_SQL_INTEGER" 	type="in">
				<cfprocparam value="#STATUS#" 				cfsqltype="cf_sql_INTEGER" 	type="in">
				<cfprocparam value="#CODEAPPLICATION#"		cfsqltype="cf_sql_INTEGER" 	type="in">
				<cfprocparam variable="p_retour" 			cfsqltype="CF_SQL_VARCHAR"  type="out">
			</cfstoredproc>
			<cflog text="retour publish : #p_retour#">
			<cfoutput>return : #p_retour# - UUID : #uuid#<br></cfoutput>
			<cfif p_retour eq -1>
				<cfset ret = -1>				
			<cfelse>
				<!--- 
				<cfset ret = RENAMEDocumentToFTP(NOM_DOC,FORMAT,uuid)>
				<cflog text="retour Rename : #ret#">
				 --->
				<cfset ret=uuid>
			</cfif>
			<cfreturn ret>
			<cfoutput><br>fin : api.ADDDocumentTableContainer<br><br></cfoutput>
		</cffunction>
		<!--- AJOUTE UN DOCUMENT DANS L'ONGLET AUTRE DOCUMENT --->
	<cffunction name="containerPublishRapportConsotel" returntype="Any"  access="remote" hint="publie un document dans la partie autre document">
		<cfargument name="NOM_DOC" 				type="string" 	required="true" 					hint="NOM LONG DU DOCUMENT (avec espace)"> 
		<cfargument name="SHORT_NAME" 			type="string" 	required="true" 					hint="NOM COURT DU DOCUMENT (sans espace)"> 
		<cfargument name="JOB_ID" 				type="numeric" 	required="false" 	default="-1" 	hint="ID DU JOB DANS BI"> 
		<cfargument name="CODE_RAPPORT"			type="string" 	required="false" 	default=""		hint="CODE DU DOCUMENT"> 
		<cfargument name="APP_LOGINID" 			type="numeric" 	required="false" 	default="-1"	hint="ID DE L'UTILISATEUR CONNECTE"> 
		<cfargument name="IDRACINE" 			type="numeric" 	required="false" 	default="-1"	hint="ID RACINE"> 
		<cfargument name="NOM_MODULE" 			type="string" 	required="false" 	default=""		hint="MODULE D'OU A ETE DEMANDE LE DOCUMENT"> 
		<cfargument name="FORMAT" 				type="string"	required="false" 	default=""		hint="FORMAT DU DOCUMENT"> 
		<cfargument name="DATE" 				type="string" 	required="false" 	default=""		hint="DATE DE PLANIFICATION DU JOB SOUS BI"> 
		<cfargument name="SERVER_BI" 			type="string" 	required="false" 	default=""		hint="NOM DU SERVER BI"> 
		<cfargument name="APPLI" 				type="string" 	required="false" 	default=""		hint="ID DE L'APPLI"> 
		<cfargument name="STATUS" 				type="numeric" 	required="false" 					hint="LE STATUS DU DOCUMENT => VALEUR POSSIBLE : 2 DISPONIBLE, 3 ERREUR, 1 ENCOURS">
		<cfargument name="TAILLE" 				type="numeric" 	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT NON ARCHIVE">
		<cfargument name="TAILLEZIP" 			type="numeric"	required="false" 	default="-1"	hint="LA TAILLE DU DOCUMENT ARCHIVE (.ZIP)">
		<cfargument name="IS_PUBLIC" 			type="boolean" 	required="false" 	default="0"		hint="JE SAIS PAS">
		
		<cflog text="************** CONTAINER ************ nom_doc = #NOM_DOC#">
		<cflog text="************** CONTAINER ************ format = #FORMAT#">
		<cflog text="************** CONTAINER ************ apploginid = #APP_LOGINID#">
		<cflog text="************** CONTAINER ************ IDRACINE = #IDRACINE#">
		
		
		<cfset uuid = CreateUuid()>
		<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.PublishRappConsotel_v2">
				<cfprocparam value="#uuid#"					cfsqltype="CF_SQL_VARCHAR"  type="in">			
				<cfprocparam value="#JOB_ID#" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#NOM_DOC#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#SHORT_NAME#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#CODE_RAPPORT#" 		cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="641" 					cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="2458788" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#NOM_MODULE#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#FORMAT#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#DATE#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#SERVER_BI#" 			cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#APPLI#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#IS_PUBLIC#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#TAILLE#" 				cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam value="#TAILLEZIP#" 			cfsqltype="CF_SQL_INTEGER" 	type="in">
				<cfprocparam value="#STATUS#" 				cfsqltype="cf_sql_INTEGER" 	type="in">
				<cfprocparam value="#APP_LOGINID#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam variable="p_retour" 			cfsqltype="CF_SQL_VARCHAR"  type="out">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>
	<!--- FACTORY MAIL --->		
		<cffunction name="SENDMailFactory" 				access="remote" returntype="numeric" 	hint="">	
			<cfargument name="TypeMail" 	type="String" required="true">
			<cfargument name="destinataire" type="String" required="true">
			<cfargument name="infosDoc"		type="struct" required="true">
			
			<cflog text="********************************************** Appel : api.SENDMailFactory">
			
			<cflog text="********************************************** #TypeMail# - #destinataire#">
			<cftry>
				<cfset mf	= createObject("component","fr.consotel.consoview.api.container.mail.SENDMail#TypeMail##destinataire#")>
				<cfset mailSend = mf.sendMail(arguments.infosDoc)>
				<cflog text="********************************************** aprés création de l'objet : #mailSend#">
				<cflog text="<br>fin : api.SendMailFactory<br>typemail : #TypeMail#<br>destinataire : #destinataire#">	
				<cfoutput><br>fin : api.SENDMailFactory<br><br></cfoutput>
				<cfreturn mailSend>
			<cfcatch>
				<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
				<cflog text="********************************************** #str#">
				<cfreturn -2>
			</cfcatch>
			</cftry>
		</cffunction>
	
	<!--- EST CE QU'UN APPLOGINID DESIRE ETRE NOTIFIE --->		
		<cffunction name="KNOWMailRight" 				access="remote" returntype="numeric" 	hint="méthode d'interrogation des droits de notifications d'un utilisateur">	
			<cfargument name="IDUSER" 	type="numeric"	hint="l'id de l'utilisateur dont on souhaite connaitre le droit">
			<cfargument name="IDRACINE"	type="numeric"	hint="sous quelle racine">
			<cfargument name="fonction" type="String" 	hint="Précise quel droit de notification on souhaite connaitre - choix : genere, erreur et partage">

			<cfoutput><br><br>Appel : api.KNOWMailRight</cfoutput>
			
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.GETNOTIFICATION" >
				<cfprocparam value="#IDUSER#"	cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam value="#IDRACINE#" cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocresult name="objRetour">
			</cfstoredproc>
			
			<cfif isDefined("objRetour") neq FALSE>
				<cfif StructKeyExists(objRetour, "RAPP_#fonction#")>
					<cfset rf = evaluate("objRetour.RAPP_#fonction#")>
					<cfreturn rf>
				<cfelse>
					<cfreturn -2>
				</cfif>
			<cfelse>
				<cfreturn -1>	
			</cfif>
			<cfoutput><br>fin : api.KNOWMailRight<br><br></cfoutput>
		</cffunction>
	<!--- AJOUTER UN JOB ID A UN DOCUMENT DANS LA TABLE CONTAINER --->
		<cffunction name="ADDJobIdToDocument"			access="remote"	returntype="Numeric"	hint="ajoute le jobID BI à un document dans la table container">
			<cfargument name="uuid" 	type="String">
			<cfargument name="JOB_ID" 	type="numeric" 	required="false" 	hint="ID DU JOB DANS BI">
			 
			 <cfoutput><br><br>Appel : api.ADDJobIdToDocument</cfoutput>
			 
			 <cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.ADDJobID">
				<cfprocparam value="#uuid#"				cfsqltype="CF_SQL_VARCHAR"  type="in">			
				<cfprocparam value="#JOB_ID#" 			cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam variable="p_retour" 		cfsqltype="CF_SQL_VARCHAR"  type="out">
			</cfstoredproc>
			<cfreturn p_retour>
			<cfoutput><br>fin : api.ADDJobIdToDocument<br><br></cfoutput>
		</cffunction>
	<!--- RECUPERATION DE L'UUID --->
		<cffunction name="GETUuid" 						access="remote"	returntype="String"		hint="retourne l'UUID">
			<cfreturn uuid>
		</cffunction>
	<!--- MODIFIE LE STATUS D'UN DOCUMENT --->
		<cffunction name="MAJStatus"					access="remote"  returntype="numeric"	hint="Mets à jour le status d'un document">
			<cfargument name="uuid" 		type="String" 	required="true">
			<cfargument name="STATUS_ID" 	type="numeric" 	required="true">					
			
			<cfoutput><br><br>Appel : api.MAJStatus</cfoutput>
			
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.MAJStatus">
				<cfprocparam value="#uuid#"				cfsqltype="CF_SQL_VARCHAR"  type="in">			
				<cfprocparam value="#STATUS_ID#" 		cfsqltype="CF_SQL_INTEGER"  type="in">
				<cfprocparam variable="p_retour" 		cfsqltype="CF_SQL_VARCHAR"  type="out">
			</cfstoredproc>
			<cfreturn p_retour>
			<cfoutput><br>fin : api.MAJStatus<br><br></cfoutput>
		</cffunction>
	<!--- AJOUTER UN PARAMETRE CLIENT --->
		<cffunction name="ADDparamClient"				access="remote"	returntype="Numeric"	hint="Ajoute un parametre visible par le client">
			<cfargument name="uuid"			type="String">					
			<cfargument name="type"			type="String">					
			<cfargument name="name"			type="String">					
			<cfargument name="value"		type="String">		
			
			<cflog text="Appel : api.ADDParamClient">
			<cflog text="		uuid : #uuid# - #name#:#type# = #value#">
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.ADDPARAM">
				<cfprocparam value="#uuid#"					cfsqltype="CF_SQL_VARCHAR"  type="in">			
				<cfprocparam value="#type#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#name#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam value="#value#" 				cfsqltype="CF_SQL_VARCHAR"  type="in">
				<cfprocparam variable="p_retour" 			cfsqltype="CF_SQL_INTEGER"  type="out">
			</cfstoredproc>	
			
			<cfreturn p_retour>
			<cfoutput><br>fin : api.ADDParamClient<br><br></cfoutput>
		</cffunction>
	<!--- AJOUTER LE XML DES PARAMETRES D'UN DOCUMENT  --->
		<cffunction name="ADDParamsTableContainer"		access="remote"	returntype="String"		hint="Ajoute le xml des paramétres à un document">
			<cfargument name="uuid" 	type="string">				
			<cfargument name="OBJ" 		type="xml">
			
			<cfoutput><br><br>Appel : api.ADDParamsTableContainer</cfoutput>
			
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_M61.addParamsTableContainer_V2">
				<cfprocparam value="#uuid#"			cfsqltype="CF_SQL_VARCHAR"  type="in">			
				<cfprocparam value="#toString(OBJ)#" cfsqltype="CF_SQL_CLOB"		type="in">
				<cfprocparam variable="p_retour" 	cfsqltype="CF_SQL_VARCHAR"  type="out">
			</cfstoredproc>
			<cfreturn p_retour>		
			<cfoutput><br>fin : api.ADDParamsTableContainer<br><br></cfoutput>	
		</cffunction>
	<!--- RECUPERER LES INFOS D'UN DOCUMENT VIA LE JOBID --->
		<cffunction name="getInfosFromJOBID"			access="remote"	returntype="Query"		hint="récupére les informations du document à partir du job_id - utilisé par le processContainer">
			<cfargument name="JOB_ID" type="Numeric" required="true">
			<cfargument name="BI_SERVER"	type="String" 	required="true">
			
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.getInfosFromJOBID_V3">
				<cfprocparam value="#JOB_ID#" 		cfsqltype="CF_SQL_INTEGER" type="in">			
				<cfprocparam value="#BI_SERVER#" 	cfsqltype="CF_SQL_VARCHAR" type="in">			
				<cfprocresult name="p_retour">
			</cfstoredproc>
			<cfreturn p_retour>				
		</cffunction>
	<!--- PRECISE UN CODE PROCESS POUR UN JOBID - insére un enregistrement dans la table jobDetail --->
		<cffunction name="setCodeProcessForJOBID"		access="remote" returntype="numeric"	hint="précise le code process d'un document via son jobid">
			<cfargument name="JOBID" 		type="Numeric">				
			<cfargument name="PROCESSID" 	type="Numeric">				
			
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.addJobDetail_V2">
				<cfprocparam value="#JOBID#" 							cfsqltype="CF_SQL_INTEGER" type="in">			
				<cfprocparam value="#PROCESSID#" 						cfsqltype="CF_SQL_INTEGER" type="in">
				<!---<cfprocparam value="BIP" 	cfsqltype="CF_SQL_VARCHAR" type="in">--->
				<cfprocparam value="#SESSION.BIP_SERVER_NAME#" 	cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam variable="p_retour" 						cfsqltype="CF_SQL_INTEGER" type="out">			
			</cfstoredproc>
			<cfreturn p_retour>						
		</cffunction>
	<!--- RECUPERE LE LIBELLE DU CODEPROCESS D'UN JOBID --->
		<cffunction name="getCodeProcess"				access="remote" returntype="String"		hint="précise le code process d'un document via son jobid">
			<cfargument name="JOBID" 								type="Numeric">	
			<cfargument name="BI_SERVER"							type="string">	
			<cflog text="********************************************** ApiContainerV1.getCodeProcess : jobid = #arguments.JOBID#">
			<cflog text="********************************************** ApiContainerV1.getCodeProcess : BI_SERVER = #arguments.BI_SERVER#">
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.getCodeProcess_V2">
				<cfprocparam value="#JOBID#" 		cfsqltype="CF_SQL_INTEGER" type="in">			
				<cfprocparam value="#BI_SERVER#"	cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocresult name="p_retour">
				<!--- <cfprocparam variable="p_retour" 	cfsqltype="CF_SQL_VARCHAR" type="out">			 --->
			</cfstoredproc>
			<cflog text="********************************************** 		recordcount : #p_retour.RecordCount#">
			<cfif p_retour.RecordCount eq 1>
				<cflog text="********************************************** 		retour : #p_retour.LIBELLE#">
				<cfreturn p_retour.LIBELLE>
			<cfelse>
				<cflog text="********************************************** 		retour : -1">
				<cfreturn "-1">
			</cfif>
		</cffunction>
	<!--- RECUPERE L'APPLOGINID A PARTIR D'UN EMAIL--->
		<cffunction name="GetApploginIdFromMail" access="remote" returntype="Numeric"  hint="retourne l'app login id d'un email-cv Le premier si un mail a plusieurs app_loginid">
			<cfargument name="EMAIL" type="string">	
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.GetApploginIdFromMail">
				<cfprocparam value="#EMAIL#"	cfsqltype="CF_SQL_VARCHAR" type="in">
				<cfprocparam variable="p_retour"	cfsqltype="CF_SQL_INTEGER" type="out"> 	
			</cfstoredproc>
			<cflog text ="ApiContainerV1.GetApploginIdFromMail (#EMAIL#) = #p_retour#">
			<cfreturn p_retour>
		</cffunction>
	<!--- RECUPERE L'IDRACINE A PARTIR D'UN IDCLIENT EXTERNE--->
		<cffunction name="getIdracineFromtIdClient" access="remote" returntype="Numeric" hint="retourne l'idracine d'un client externe">
			<cfargument name="IDCLIENT"			type="numeric" >
			<cfargument name="CODEAPPLICATION"	type="numeric" >		
			
			<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.getIdracineFromtIdClient">
				<cfprocparam value="#IDCLIENT#"	cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam value="#CODEAPPLICATION#"	cfsqltype="CF_SQL_INTEGER" type="in">
				<cfprocparam variable="p_retour"	cfsqltype="CF_SQL_INTEGER" type="out"> 	
			</cfstoredproc>
			
			<cflog text ="ApiContainerV1.getIdracineFromtIdClient (#IDCLIENT#, #CODEAPPLICATION#) = #p_retour#">
s			<cfreturn p_retour>
		</cffunction>			
	<!--- RENSEIGNE LA TAILLE DU FICHIER --->		
		<cffunction name="MAJTaille" 					access="remote" returntype="Numeric"	hint="renseigne la taille d'un rapport">
			<cfargument name="uuid" type="string">					
			
			<cflog text="********************************************** Apicontainer.MAJTaille(#uuid#)">
			
			<cftry>
				<cfdirectory action="list" directory="/container" name="qListFile" filter="#uuid#">
				<cfif qListFile.recordcount GT 0>
				<cflog text="********************************************** Apicontainer - taille de qListFile : #qListFile.recordcount#">
				<cflog text="********************************************** Apicontainer - taille de qListFile : #qListFile['SIZE'][1]#">
					<cfset taille = #qListFile['SIZE'][1]#>
					<cflog text="taille du fichier #uuid# : #taille#">
			
					<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_m61.MAJTaille">
						<cfprocparam value="#uuid#"					cfsqltype="CF_SQL_VARCHAR"  type="in">			
						<cfprocparam value="#taille#" 				cfsqltype="CF_SQL_NUMERIC"  type="in">
						<cfprocparam variable="p_retour" 			cfsqltype="CF_SQL_NUMERIC"  type="out">
					</cfstoredproc>	
					<cfreturn p_retour>	
				<cfelse>
				<cflog type="error" text="Container : Fichier FTP #uuid# introuvable sur le FTP pour le serveur #CGI.SERVER_NAME#">
			
				<cftry>					
					<cfsavecontent variable="body">
						
						<cfoutput>			
							[EVT-041]
							Notification d'erreur pour le ticket http://nathalie.consotel.fr/issues/5761<br>
							Le fichier FTP #uuid# correspondant au rapport est introuvable dans le répertoire /container du backoffice #CGI.SERVER_NAME#<br>
							Le code de retour retourné sera -3
						</cfoutput>
						
					</cfsavecontent>
					

					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(13)>
					<cfset mLog.setIDMNT_ID(21)>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
						to="monitoring@saaswedo.com" 
						server="mail-cv.consotel.fr" 
						port="25" type="text/html"
						subject="test log monitor">
							<cfdump var="#cfcatch#">
					</cfmail>
				</cfcatch>				
				</cftry>
				
			<cfreturn -3>
			</cfif>
					
			<cfcatch type="any">
				<cflog text="************************************************** ApiContainer : catch de MajTaille : #cfcatch#">
			</cfcatch>
			</cftry>
		</cffunction>
	<!--- NETTOYAGE DU FTP CONTAINER --->
		<cffunction name="PURGEFTP" 					access="remote"	returntype="boolean" 	hint="supprime tous les fichiers sur le ftp qui ne sont pas dans la table container (sauf le répertoires liens\ utile pour rapportE0)">
			<cflog text="RECUPERATION DES FICHIERS DANS LA TABLE CONTAINER">
			<!---<cfquery datasource="ROCOFFRE" result="rslt" name="result">
				SELECT UUID FROM detail_container
			</cfquery>
			<cfdump var="#rslt#"/>
			<cfdump var="#result#"/>
			
			<cfif rslt.recordcount gt 0>
				<cflog text="RECUPERATION DE LA LISTE DES FICHIERS DU FTP CONTAINER">
				<cfdirectory action="list" directory="/container/" name="listDir" listInfo="all">
				<cflog text="Liste des fichiers : ">
				<cfdump var="#listDir#">
				 
				<cfif listDir.recordcount gt 0>
					<cfloop query="listDir">
						<cfset boolExist = false>
						<cfif ucase(#listDir.type#) eq 'DIR'>
							<cfif ucase(#listDir.name#) neq 'LIENS'>
								<cflog text="Le répertoire [ #listDir.name# - taille  : #listDir.size# - type  : #listDir.type# ] a été supprimé">
								<cfdirectory action="delete" directory="#listDir.DIRECTORY#/#listDir.NAME#" recurse="true">
							</cfif>
						<cfelseif ucase(#listDir.type#) eq 'FILE'>
							<cfloop query="result">
								<cfif ucase(#result.UUID#) eq ucase(#listDir.name#)>
									<cfset boolExist = true>
			 						<cfbreak> 
								</cfif>
						    </cfloop> 
							<cfif boolExist eq false>
								<cffile action="delete" file="/container/#listDir.name#">
								<cflog text="Le fichier #listDir.name# a été supprimé">
							</cfif>
						</cfif>
					</cfloop>
				</cfif>			
			</cfif>	--->
			<cfreturn FALSE>
		</cffunction>
<!--- PARTIE SBPM  --->		
		
	<!--- FACTORY MAIL PFGP - permet à SBPM d'envoyer un mail Container.--->		
		<cffunction name="SENDMailFactoryViaSBPM" access="remote" returntype="numeric" 	hint="">	
			<cfargument name="TYPEMAIL" 	type="String" required="true">
			<cfargument name="DESTINATAIRE" type="String" required="true">
			<cfargument name="APP_LOGINID" type="String" required="true">
			<cfargument name="MAIL_DESTINATAIRE" type="String" required="true">
			<cfargument name="NOM_DETAILLE_RAPPORT" type="String" required="true">
			<cfargument name="PERIMETRE" type="String" required="true">
			<cfargument name="CODEAPPLICATION" type="String" required="false" default="51">
			<cfargument name="GLOBALIZATION" type="String" required="false" default="fr_FR">
			
			<cflog text="********************************************** Appel : api.SENDMailFactory">
			<cflog text="********************************************** #TypeMail# - #destinataire#">
			<cftry>
				<cfquery name="qRetour" datasource="ROCOFFRE">
					SELECT al.login_nom, al.login_prenom FROM app_login al WHERE al.app_loginid = #arguments.APP_LOGINID#
				</cfquery>
				
				<cfset infosDoc = structNew()>
				<cfset infosDoc.APP_LOGINID = arguments.APP_LOGINID>
				<cfset infosDoc.CODEAPPLICATION = arguments.CODEAPPLICATION>
				<cfset infosDoc.DATE_DEMANDE = now()>
				<cfset infosDoc.GLOBALIZATION = arguments.GLOBALIZATION>
				<cfset infosDoc.MAIL_DESTINATAIRE = arguments.MAIL_DESTINATAIRE>
				<cfset infosDoc.NOM_DETAILLE_RAPPORT = arguments.NOM_DETAILLE_RAPPORT>
				<cfset infosDoc.PERIMETRE = arguments.PERIMETRE>
				<cfset infosDoc.LASTNAME = qRetour.LOGIN_NOM>
				<cfset infosDoc.SURNAME = qRetour.LOGIN_NOM>
				<cfset infosDoc.FIRSTNAME = qRetour.LOGIN_PRENOM>
				
				<cfset mf = createObject("component","fr.consotel.consoview.api.container.mail.SENDMail#TypeMail##destinataire#")>
				<cfset mailSend = mf.sendMail(infosDoc)>
				<cflog text="********************************************** aprés création de l'objet : #mailSend#">
				<cflog text="<br>fin : api.SendMailFactory<br>typemail : #TypeMail#<br>destinataire : #destinataire#">	
				<cfoutput><br>fin : api.SENDMailFactory<br><br></cfoutput>
				<cfreturn mailSend>
			<cfcatch>
				<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
				<cflog text="********************************************** #str#">
				<cfreturn -2>
			</cfcatch>
			</cftry>
		</cffunction>
	<!--- CONSOREPORTING SODEXO : work order #4989# genererEnvoi()--->		
		<cffunction name="genererEnvoi" access="remote" returntype="any">	
			<cfargument name="idperiodemois" 	type="numeric" required="true">
			<cfargument name="idracine" 		type="numeric" required="true">
			<cfargument name="isRattrapage" 	type="numeric" required="true">
			
			<cflog text="********************************************** Appel : api.genererEnvoi">
			<cflog text="********************************************** idperiodemois = #idperiodemois# - idracine = #idracine# - isRattrapage = #isRattrapage#">
			
			<cftry>
				<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_pfgp_suivi_chargement.type_20_liste">
					<cfprocparam value="#idperiodemois#"		cfsqltype="CF_SQL_NUMERIC"  type="in">
					<cfprocparam value="#idracine#"				cfsqltype="CF_SQL_NUMERIC"  type="in">
					<cfprocparam value="#isRattrapage#" 		cfsqltype="CF_SQL_NUMERIC"  type="in">
					<cfprocresult name="p_retour"/>
				</cfstoredproc>
				
				<cfset NewLine = Chr(13) & Chr(10)>
				<cfset loginList = "">
				<cfloop Query="p_retour">
					<cfset loginList &="#TRIM(APP_LOGINID)#|#TRIM(LOGIN_EMAIL)#|#TRIM(IDRACINE)#|#TRIM(RACINE)#|#TRIM(DIRECTION)#|#TRIM(FILIALE)#|#TRIM(ID)##NewLine#">
				</cfloop>
				    
				<cftry>
					<!--- MLOG --->
					<cfsavecontent variable="body">					
						<cfoutput>GenererEnvoi : Dump de la liste<br></cfoutput>			
						<cfdump var="#loginList#">
					</cfsavecontent>

					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(13)>
					<cfset mLog.setIDMNT_ID(22)>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					<!--- --->
					<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfdump var="#cfcatch#">						
					</cfmail>
				</cfcatch>
				
				</cftry>
					
							
				<cfreturn loginList>	
			<cfcatch>
				<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
				<cflog text="***** CONSOREPORTING ***** genererEnvoi : #str#">
				<cfset strError=structNew()>
				<cfset strError["ID"] = -1>
				<cfset strError["ERROR"] = str>
				
				<cftry>
					<!--- MLOG --->
					<cfsavecontent variable="body">					
						<cfoutput>GenererEnvoi : Dump de la liste<br></cfoutput>			
						<cfdump var="#cfcatch#">
					</cfsavecontent>
	
					<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
					<cfset mLog.setIDMNT_CAT(13)>
					<cfset mLog.setIDMNT_ID(23)>
					<cfset mLog.setBODY(body)>
					
					<cfinvoke 
						component="fr.consotel.api.monitoring.MLogger"
						method="logIt"  returnVariable = "result">
						
						<cfinvokeargument name="mLog" value="#mLog#">  
						
					</cfinvoke>	
					<!--- --->
				<cfcatch type="any">
					<cfmail from="MLog <monitoring@saaswedo.com>" 
							to="monitoring@saaswedo.com" 
							server="mail-cv.consotel.fr" 
							port="25" type="text/html"
							subject="test log monitor">
								<cfdump var="#cfcatch#">						
					</cfmail>
				</cfcatch>
				
				</cftry>	
				
				<cfreturn strError>
			</cfcatch>
			</cftry>
		</cffunction>	
	<!--- CONSOREPORTING SODEXO : work order #4989# updateEnvoi()--->		
		<cffunction name="updateEnvoi" access="remote" returntype="any">	
			<cfargument name="id" 		type="numeric" required="true">
			<cfargument name="statut" 	type="numeric" required="false">
			
			<cflog text="********************************************** Appel : api.UpdateEnvoi">
			<cflog text="********************************************** id = #id#">
			
			<cftry>
				<cfstoredproc datasource="#OFFREDSN#" procedure="pkg_pfgp_suivi_chargement.valide_suivi_chargement">
					<cfprocparam value="#id#"		cfsqltype="CF_SQL_NUMERIC"  type="in">
					<cfprocparam variable="p_retour" cfsqltype="CF_SQL_NUMERIC"  type="out">			
				</cfstoredproc>	
				<cfreturn p_retour>	
				
			<cfcatch>
				<cfset str= "#cfcatch.Type# : #cfcatch.Message# - #cfcatch.Detail#">
				<cflog text="***** CONSOREPORTING ***** updateEnvoi : #str#">
				<!--- <cfset strError=structNew()>
				<cfset strError["ID"] = -1>
				<cfset strError["ERROR"] = str> --->
				
				<cfreturn -1>
			</cfcatch>
			</cftry>
		</cffunction>	
</cfcomponent>