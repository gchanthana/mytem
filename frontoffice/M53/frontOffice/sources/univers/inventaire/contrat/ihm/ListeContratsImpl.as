package univers.inventaire.contrat.ihm
{
	import flash.events.MouseEvent;
	
	import mx.collections.ICollectionView;
	import mx.containers.TitleWindow;
	import mx.controls.DataGrid;
	import mx.events.CloseEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;

	[Bindable]
	public class ListeContratsImpl extends TitleWindow
	{	
		public var dgListe:DataGrid;
		
		private var _selectedContrat:Object;		
		private var _fenetreContrat:IFenetreContrat;
		private var _titre:String;
		private var _dataProvider:ICollectionView;
		
		public function ListeContratsImpl()
		{
			super();
		}

		public function set selectedContrat(value:Object):void
		{
			_selectedContrat = value;
		}

		public function get selectedContrat():Object
		{
			return _selectedContrat;
		}
		
		protected function _localeCloseHandler(event:CloseEvent):void
		{
			PopUpManager.removePopUp(this)
		}
		
		protected function _btValiderClickHander(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this)
		}
		
		protected function _btAnnulerClickHandler(event:MouseEvent):void
		{
			PopUpManager.removePopUp(this)	
		}

		protected function _dgListeChangeHandler(event:ListEvent):void
		{
			if(dgListe.selectedIndex != -1)
			{
				selectedContrat = dgListe.selectedItem;	
			}
			else
			{
				selectedContrat = null;
			}
		}

		public function set titre(value:String):void
		{
			_titre = value;
		}

		public function get titre():String
		{
			return _titre;
		}

		public function set dataProvider(value:ICollectionView):void
		{
			_dataProvider = value;
		}

		public function get dataProvider():ICollectionView
		{
			return _dataProvider;
		}
	}
}