<cfcomponent name="facade" displayname="facade">
	
	<cffunction name="transformDate" access="private" description="translate string_date dd-mm-yyyy to string_date yyyy-mm-dd" returntype="string">
		<cfargument name="oldDateString" type="string" required="true" default="01-01-1970">		
		<cfset newDateString = right(oldDateString,4) & "-" & mid(oldDateString,4,2) & "-" & left(oldDateString,2)>			
		<cfreturn newDateString> 
	</cffunction>
	
	<!--- RETOURNE LES PRODUITS DU THEME CHOISI--->	
	<cffunction name="getListeProduitsTheme" access="public" returntype="query">
		
		<cfargument name="perimetre" type="string" required="false">
		<cfargument name="modeCalcul" type="string" required="false">		
		<cfargument name="numero" type="numeric" required="false">
		<cfargument name="idproduit" type="numeric" required="true">
		<cfargument name="datedeb" type="string" required="false">
		<cfargument name="datefin" type="string" required="false">
		
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.theme.Theme")>
		<cfset obj.init(perimetre,modeCalcul)>				
		
		<cfif perimetre eq "soustete">
			<cfset result=obj.queryData(idproduit,numero,
					transformDate(datedeb), 
					transformDate(datefin))>	
		<cfelse>	
			<cfset result=obj.queryData(idproduit,numero,
					transformDate(datedeb), 
					transformDate(datefin))>		
		
		</cfif>	
		
		<cfreturn result>
		
	</cffunction>  
 	
	<!--- RETOURNE LES SUR-THEMES CONSOS 
	<cffunction name="getSurThemeConsos" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset recordset = session.RECORDSET >
		<cfset result = obj.getSurthemeConsos()>
		<cfreturn result>
	</cffunction>  --->	
	
	<!--- RETOURNE LES  THEMES CONSOS 
	<cffunction name="getConso" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset recordset = session.RECORDSET >
		<cfset result = obj.getConso()>
		<cfreturn result>
	</cffunction>  --->	
	
	<!--- RETOURNE LES  THEMES ABOS 
	<cffunction name="getAbo" access="public" returntype="query">
		<cfset obj=CreateObject("component","fr.consotel.consoview.tb.produits.produit")>
		<cfset recordset = session.RECORDSET >
		<cfset result = obj.getAbo()>
		<cfreturn result>
	</cffunction>  --->	

<!---::::::::::::::::::::::::::::::::::::::::::::::::::PRIVATE::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 

	<cffunction name="setSessionValue" output="true" access="private" >
		<cfargument name="cle" type="any" default=""/>		
		<cfargument name="valeur" type="any" default=""/>	
		<cfif StructKeyExists(session,cle)>
			<cfset "session.#cle#"=valeur>
		<cfelse>
			<cfset structInsert(session,cle,valeur)>
		</cfif>
	</cffunction> --->


</cfcomponent>