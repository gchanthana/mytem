package composants.ConsoProgressBar
{
	import flash.events.Event;
	
	public class ConsoProgressBarEvent extends Event
	{
		public static const PROGRESSBAR_COMPLETE:String="progressbar_complete";
		
		public function ConsoProgressBarEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}