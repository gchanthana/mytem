package univers.facturation.grilletarif.entity
{
	public class ProduitVO
	{
		private var _idProduit : int;
		private var _libelleProduit : String;
		
		public function ProduitVO()
		{
			
		}
		public function get libelleProduit():String
		{
			return _libelleProduit;
		}
		public function set libelleProduit(value:String):void
		{
			_libelleProduit = value;
		}
		public function get idProduit():int
		{
			return _idProduit;
		}
		public function set idProduit(value:int):void
		{
			_idProduit = value;
		}
		

	}
}