package rapportPilotage.service.formattemplatetelector 
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class FormatTemplateSelectorHandler
	{
		private var _model: FormatTemplateSelectorModel;
		
		public function FormatTemplateSelectorHandler(_model:FormatTemplateSelectorModel):void
		{
			this._model = _model;
		}
		
		internal function getValuesFormatHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateValues(event.result as ArrayCollection);
			}
		}
	}
}