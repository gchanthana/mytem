package univers.parametres.rago.inventaire.ihm
{
	
	import flash.events.Event;
	
	import mx.containers.HBox;
	import mx.controls.CheckBox;
	import mx.controls.Label;
	
	import univers.parametres.rago.entity.Regle;
	import univers.parametres.rago.inventaire.event.InventaireReglesEvent;
	
	[Bindable]
	public class StatutImpl extends HBox
	{

//VARIABLES GLOBALES

		public var ckbxActiver:CheckBox;
		public var actifOrNot:Label;

//FIN VARIABLES GLOBALES

//METHODES PUBLICS
		
		public function StatutImpl() 
		{ super(); }

//FIN METHODES PUBLICS

//METHODES PROTECTED
		
		//Dispatch un event lors de l'activation/désactivation de la règle
		protected function ChangeStringActifOrNot(event:Event):void
		{ dispatchEvent(new InventaireReglesEvent(data as Regle,InventaireReglesEvent.ACTIVER_REGLE_CLICKED,true)); }
		
		//Affecte la valeur de l'objet règle (data) dans l'itemrenderer 
		//(coche ou non le checkbox et affiche le texte 'active', 'inactive')
		protected function initStatut(event:Event):void
		{ 
			if(data != null)
			{ 
				ckbxActiver.selected = data.BOOL_ACTIVE;
				actifOrNot.text = data.CHAINE_BOOL_ACTIVE;
			}
		}

//FIN METHODES PROTECTED
		
	}
}