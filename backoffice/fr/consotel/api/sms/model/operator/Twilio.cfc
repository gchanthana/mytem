<cfcomponent displayname="Twilio Operator" output="false" implements="fr.consotel.api.sms.model.operator.IOperator">
	
	<cfset VARIABLES.twilioUsername="datalert@saaswedo.com">
	<cfset VARIABLES.twilioPassword ="Datalert!">
	
	<!--- ces information est données par Twilio lorsquel'on se cpnnecte à leur extranet' --->
	<cfset VARIABLES.accountSid="AC5e2a48a8c4c76a0a6c8c7590adbfb89b">
	<cfset VARIABLES.authToken="c1746286055896f7ff04d5693dd8df02">
	<cfset VARIABLES.fromSid="+19782613232">
	<cfset VARIABLES.twilioSMSResource ="https://api.twilio.com/2010-04-01/Accounts/#accountSid#/Messages/" />
	
    <cfset VARIABLES.errorException=createobject("component","fr.consotel.api.sms.model.ApiException") >

	<cffunction name="perpareSMS" access="public" output="false" returnType="any">
		
		<cfargument name="phoneNumber"   required="true"  type="any">
		<cfargument name="contentSms"    required="true"  type="any">
		<cfargument name="tabSMSNumber"  required="true"  type="any">
		<cfargument name="uuid"          required="true"  type="any">
		<cfargument name="delay"         required="false" type="any">
		<cfargument name="withWebService"  required="false" type="numeric"><!--- ne sert à rien pour Twilio --->
		
		<cfset VARIABLES.phoneNumber  = ARGUMENTS.phoneNumber >
	    <cfset VARIABLES.contentSms   = ARGUMENTS.contentSms >
	    <cfset VARIABLES.uuid = ARGUMENTS.uuid >
		    	
		<cfif isArray(phoneNumber) or isArray(contentSms) or isArray(tabSMSNumber) >
			 	    
			<cfif isArray(tabSMSNumber)><!--- tableau d'objet des n°phone+SMS --->
			
				<cfloop from="1" to="#ArrayLen(tabSMSNumber)#" index="z" step="1"> 
					
					<cfset resultatSendSms=callTwilioService(tabSMSNumber[z]["phoneNumber"],tabSMSNumber[z]["contentSms"])>	<!--- appele le web service --->	
					 <cfif delay lt 10000>
						<cfthread action="sleep" duration="10000"></cfthread>
					<cfelse>
						<cfthread action="sleep" duration="#delay#"></cfthread>
					</cfif>
				</cfloop>
				<cfreturn true>
				
			<cfelseif isArray(phoneNumber)><!--- un tableau des N° phone et un seule SMS --->
				<cfloop from="1" to="#ArrayLen(phoneNumber)#" index="z" step="1"> 	
					<cfset resultatSendSms=callTwilioService(phoneNumber[z],contentSms)>	<!--- appele le web service --->	
					 <cfif delay lt 10000>
						<cfthread action="sleep" duration="10000"></cfthread><!--- faire une pause --->
					 <cfelse>
						<cfthread action="sleep" duration="#delay#"></cfthread>
					 </cfif>				
				</cfloop>
				<cfreturn true>
				
			<cfelse><!--- un tableau des SMS  et un seul n° phone --->
				 <cfloop from="1" to="#ArrayLen(contentSms)#" index="z" step="1"> 
					 <cfset resultatSendSms=callTwilioService(phoneNumber,contentSms[z])>	<!--- appele le web service --->	
					 <cfif delay lt 10000>
						<cfthread action="sleep" duration="10000"></cfthread>
					<cfelse>
						<cfthread action="sleep" duration="#delay#"></cfthread>
					</cfif>
				 </cfloop>
				 <cfreturn true> 
			</cfif>
			
		<cfelse>
			 <cfset resultatSendSms=callTwilioService(phoneNumber,contentSms)>
			 <cfreturn resultatSendSms> 
		</cfif>
	</cffunction>


	<cffunction  name="callTwilioService" access="private" returntype="any" output="true">
		
		<cfargument name="phoneNumber"   required="true"  type="any"><!--- le n° tel à qui on envoi le sms --->
		<cfargument name="contentSms"    required="true"  type="any"><!--- le sms --->

		<cfset exception="">
		
		 <!--- creer une requete http   avec plusieurs paramètres --->
		<cfhttp
             url="#VARIABLES.twilioSMSResource#"
             method="post"
             result="httpResponse"
			 username="#VARIABLES.accountSid#"
		 	 password="#VARIABLES.authToken#" >
			
			<cfhttpparam
				type="formfield"
				name="To"
				value="#phoneNumber#"/>
				
			<cfhttpparam
				type="formfield"
				name="From"
				value="#VARIABLES.fromSid#"/><!--- +19782613232 --->
			
            <cfhttpparam
				type="formfield"
				name="Body"
				value="#contentSms#"/>
          </cfhttp>
			
<!--- 	 <cfoutput>
             <cfdump var="#VARIABLES#">
			   <cfdump var="#httpResponse#">
             <br>
         </cfoutput> 
			<cfabort> --->		
		
		<cfif httpResponse.StatusCode eq "201 CREATED"><!--- tester si ok  --->
			<cfset xmlResult=xmlparse(httpResponse.Filecontent)>
			<cfset messageid=xmlResult.xmlRoot.XmlChildren[1].XmlChildren[1].xmlText>
		  	<cfset retrunDB=insertDataBase(phoneNumber,contentSms,messageid)>
			<cfreturn retrunDB>
		  	<cfelse>
		  		<cfset exception=VARIABLES.errorException.throwException("apiSMS_099", "Le REST Twilio est momentanément indisponible,Veuillez essayer ultérieurement")>
		 </cfif>
		  
	</cffunction>
	
	<cffunction name="insertDataBase" access="private" returntype="any" hint="I insert your data" output="true">
	    
	    <cfargument name="phoneNumber" type="any" 	required="true">
	    <cfargument name="contentSms"  type="any" 	required="true">
	    <cfargument name="messageid"   type="any"   required="true">
	    
	    <cfif not (isDefined("messageid"))>
			<cfset messageid="">
		</cfif>
		
		<cfset id_operator=3><!--- twilio --->		
		<cfset newCompgane=createobject("component","fr.consotel.api.sms.model.Campagne")>		
		<cfset id_uuid=newCompgane.getIdCampaign(VARIABLES.uuid)>
		
		<!--- messageid est utilisé pour récuperer le status de sms envoyé par le WebService --->
		<cfset dataStruct={id_uuid=#id_uuid#,contentSms=#contentSms#,phoneNumber=#phoneNumber#,id_operator=#id_operator#,messageid=#messageid#}>	
		<cfset insertData=newCompgane.insertSms(dataStruct)>
		
		 <cfif insertData eq false>
			<cfset exception=VARIABLES.errorException.throwException("apiSMS_099", "Une erreur s'est produit lors de l'insertion dans la base Oracle après l'envoi des SMS")>
			<cfelse>
				<cfreturn true>		
		</cfif>
		
	</cffunction>
	
</cfcomponent>