<cfcomponent output="false">

	<cffunction name="RenameFile"
				access="remote" 
				output="false"
				hint=" Rename the file to put it an unique name" >
	
		<cfargument name="oldName"
					required="true"
					type="string" >
					
		<cfargument name="newName"
					required="true"
					type="string" >

		<cfargument name="pathName"
					required="true"
					type="string" >
					
		<cfset destination = #pathName# & #newName#>
		<cfset source = #pathName# & #oldName#>
		
		<cffile 
		    action = "rename"
		    destination = "#destination#" 
		    source = "#source#">
						
	</cffunction>

</cfcomponent>