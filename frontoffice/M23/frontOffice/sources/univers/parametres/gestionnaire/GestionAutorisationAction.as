package univers.parametres.gestionnaire
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	
	public class GestionAutorisationAction extends GestionAutorisationsActionsIHM  {
	
		private var fiche_gestionnaire : FicheGestionnaire;
		private var id_selectPool : int;
		
		public function GestionAutorisationAction(fiche_gestionnaire : FicheGestionnaire)
		{
			super();
			this.fiche_gestionnaire = fiche_gestionnaire;
			addEventListener(FlexEvent.CREATION_COMPLETE,acces_mode);
			addEventListener(CloseEvent.CLOSE,fermer);
			
			
		}
		private function acces_mode(evt : FlexEvent):void {
			
			//this.objDonnee = new DonneeSource(this);
			//Alert.show("--Sur ce groupe Vous êtes en mode : "+CvAccessManager.getSession().CURRENT_ACCESS+" et WRITE_MODE = "+ConsoViewSessionObject.WRITE_MODE);
			if(CvAccessManager.getSession().CURRENT_ACCESS == ConsoViewSessionObject.WRITE_MODE)
			{
				initIHM(true);
			}
			else
			{
				initIHM();
			}
			
		}
		public function initIHM(modeEcriture : Boolean = false):void
		{
			lab_gestionnaire.text = fiche_gestionnaire.dg_gestionnaire.selectedItem.LOGIN_PRENOM+" "+fiche_gestionnaire.dg_gestionnaire.selectedItem.LOGIN_NOM;
			//dg_gestionnaire
			init_dg_pool_gestionnaire();
			init_profile();
			dg_profile.addEventListener(Event.CHANGE,dg_profile_handler);
			dg_pool_gestionnaire.addEventListener(Event.CHANGE,dg_pool_gestionnaire_handler);
			bt_creer_profile.addEventListener(MouseEvent.CLICK,creerProfile);
			bt_affecter.addEventListener(MouseEvent.CLICK,affecter);
			bt_desaffecter.addEventListener(MouseEvent.CLICK,bt_desaffecter_handeler);
			bt_fermer.addEventListener(MouseEvent.CLICK,fermer);
			bt_fermer.enabled = modeEcriture;
			bt_creer_profile.enabled = modeEcriture;
			bt_affecter.enabled = modeEcriture;
			bt_desaffecter.enabled = modeEcriture;
		}
		
		private function bt_desaffecter_handeler(evt : MouseEvent):void
		{
			
		}
		private function affecter(evt : MouseEvent):void
		{
			if(dg_pool_gestionnaire.selectedIndex == -1 || dg_profile.selectedIndex == -1)
			{
				Alert.show("Sélectionnez un pool + un profile");
			}
			else
			{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
															"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
															"setProfileGestionnaire",
															process_affecter);
			RemoteObjectUtil.callService(op,
											fiche_gestionnaire.dg_gestionnaire.selectedItem.APP_LOGINID,
											CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX,
											dg_pool_gestionnaire.selectedItem.IDPOOL,
											dg_profile.selectedItem.IDPROFIL_COMMANDE,
											combo_collab.selectedItem.data);	
			}
		}
		private function process_affecter(evt : ResultEvent):void
		{
				Alert.show("Profile affecté");
				init_dg_pool_gestionnaire();
				init_profile();
		}
		private function creerProfile(evt : MouseEvent):void
		{
			var panelCreerProfile : CreerProfileIHM = new CreerProfile();
			panelCreerProfile.addEventListener("CLOSE",init_profile); 
			PopUpManager.addPopUp(panelCreerProfile,this,true);
			PopUpManager.centerPopUp(panelCreerProfile);
		}
		private function dg_pool_gestionnaire_handler(evt : Event):void
		{
		    var idProfileSelect : int = dg_pool_gestionnaire.selectedItem.IDPROFIL;
			var i : int = 0;
		//	trace ("test : "+i);
			for (i; i < (dg_profile.dataProvider as ArrayCollection).length;i++)
			{
				if((dg_profile.dataProvider as ArrayCollection).getItemAt(i).IDPROFIL_COMMANDE == idProfileSelect)
				{
					dg_profile.selectedIndex = i;
					dg_profile.scrollToIndex(i);
					dg_profile_handler(null);
				} 
			}
			
			combo_collab.selectedIndex = dg_pool_gestionnaire.selectedItem.ACCES_COLLABORATEUR;
		}
		private function dg_profile_handler(evt : Event):void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"fournirListeActProfil",
																	process_fournirListeActProfil);
			RemoteObjectUtil.callService(op,dg_profile.selectedItem.IDPROFIL_COMMANDE);
		}
		private function process_fournirListeActProfil(re : ResultEvent):void
		{
			dg_action.dataProvider = re.result;
		}
		private function init_profile():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"fournirListeProfiles",
																	process_getProfile);
			RemoteObjectUtil.callService(op,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);	
		}
		private function process_getProfile(re : ResultEvent):void
		{
			dg_profile.dataProvider = re.result;
		}
		private function fermer(evt : Event):void
		{
			PopUpManager.removePopUp(this);	
		}
		private function init_dg_pool_gestionnaire():void
		{
			var op:AbstractOperation =RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																	"fr.consotel.consoview.parametres.gestionnaire.gestionnaire",
																	"getPool_of_Gestionnaire",
																	process_getPool);
			RemoteObjectUtil.callService(op,fiche_gestionnaire.dg_gestionnaire.selectedItem.APP_LOGINID,CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		private function process_getPool(re : ResultEvent):void
		{
			dg_pool_gestionnaire.dataProvider = re.result;
			/*if(dg_pool_gestionnaire.selectedIndex != -1)
			{
			combo_collab.selectedIndex = dg_pool_gestionnaire.selectedItem.ACCES_COLLABORATEUR;
			trace("-----------------SELECT COMBO---------------"+dg_pool_gestionnaire.selectedItem.ACCES_COLLABORATEUR);
			
			}*/
			
		}
	}
	
}