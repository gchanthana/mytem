<cfcomponent displayname="fr.consotel.consoview.M27.CampagneService" hint="Gestion des campagne d'enrollment" output="false">
	
	<!--- CREATION UUID (ID UNIQUE SUR 35 CAR) --->
	
	<cffunction name="getUUID" access="remote" output="false" returntype="String" hint="CREATION UUID (ID UNIQUE SUR 35 CAR)">
		
			<cfset uuid = createUUID()>
					
		<cfreturn uuid> 
	</cffunction>

	<!--- RÉCUPÈRE LES DONNÉES D'UN FICHIER XLS OU XLSX --->

	<cffunction name="getImportData" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
		<cfargument name="uuidFile" 	required="true" type="String"/>
		<cfargument name="filename" 	required="true" type="String"/>
		
			<cfset directory = "/container/M27/">	
			
			<cfset path = #directory# & #uuidFile# & "/" & #filename#>

			<cfspreadsheet action="read" query="queryImportData" src="#path#"/> 
					
			<!--- FUNCTION POUR CHECKER LES DATAS --->
					
			<cfloop query="queryImportData">
				
				<cfset var currrentLigne = queryImportData['col_1'][queryImportData.currentRow]>
				
				<cfif VAL(currrentLigne) GT 0>
				
					<cfif LEN(currrentLigne) EQ 9 AND (FindNoCase('6', currrentLigne) EQ 1 OR FindNoCase('7', currrentLigne) EQ 1)>
										
						<cfset queryImportData['col_1'][queryImportData.currentRow] = '0' & queryImportData['col_1'][queryImportData.currentRow]> 
					
					</cfif>
				
				</cfif>					
			
			</cfloop>
					
		<cfreturn queryImportData> 
	</cffunction>

	<!--- CRÉATION D'UN FICHIER CSV POUR QUE LA BDD RÉCUPÈRE LES LIGNES DU FICHIER IMPORTÉ --->

	<cffunction name="setImportDataToCSV" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
		<cfargument name="uuidFile" 	required="true" type="String"/>
		<cfargument name="lignes" 		required="true" type="Array"/>
		
			<cfset var directory 	= "/container/M27/">	
			<cfset var path		 	= #directory# & #uuidFile#>
			<cfset var filepath	 	= #path# & '/' & #uuidFile# & '.csv'>
			<cfset var statut 	 	= 0>
			<cfset var sautDeLigne	= CHR(13) & CHR(10)>
			
			<cftry>
			
				<cfif DirectoryExists('#path#')>
					
					<cfset var templatepath 	= GetCurrentTemplatePath()/>
					<cfset var currentdirectory = GetDirectoryFromPath(templatepath)/>
					
					<cfif FileExists('#filepath#')>
					
						<cffile action="delete" file="#filepath#">
					
					</cfif>
					
					<cffile action="copy" source="#currentdirectory#exportsimports/csv_exemple.csv" destination="#path#" mode="777">
									
					<cffile action="rename" source="#path#/csv_exemple.csv" destination="#filepath#" attributes="normal">
					
					<cfset var lenLignes = ArrayLen(lignes)>
					<cfset var strLignes = ''>
					
					<cfloop index="idx" from="1" to="#lenLignes#">
					
						<cfset var currentLigne = lignes[idx]>
					
						<cfset var strLignes = strLignes & currentLigne & ';' & sautDeLigne>
					
					</cfloop>
					
					<cffile action="write" output="#strLignes#" file="#filepath#" mode="777">
					
					<cfset var statut = 1>
					
				</cfif>
			
			<cfcatch>
				
				<cfset var statut = -10>
				
				
				<cfmail server="mail.consotel.fr" port="26" from="upload@consotel.fr" to="dev@consotel.fr" 
						failto="dev@consotel.fr" bcc="dev@consotel.fr" subject="[WARN-UPLOAD]Import de ligne M27" type="text/html">
						
					<cfoutput>
						
						Import de ligne gestion du MDM en erreur -> <br/><br/>
						#CFCATCH.Message#<br/>
					
					</cfoutput>
						
				</cfmail>
				
			</cfcatch>					
			</cftry>
					
		<cfreturn statut> 
	</cffunction>

	<!--- RÉCUPÈRE LA LISTE DES CAMPAGNES (SAUVEGARDEES ET ENVOYEES) --->

	<cffunction name="getCampagnes" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
				
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idcodelangue 	= session.user.idglobalization>
			<cfset var codelangue 		= session.user.globalization>
				
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_mdm.liste_campagne">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#idracine#">
				<cfprocresult name="p_retour">			
			</cfstoredproc>
	
			 <!--- ORDRE D'AFFICHAGE PAR DATE DE CREATION DECROISSANTE  --->
			<cfquery name="p_retour_order" dbtype="query">
				SELECT 	* 
				FROM p_retour
				ORDER BY DATE_CREATION
			</cfquery>
			 						
		<cfreturn p_retour_order> 
	</cffunction>

	<!--- RÉCUPÈRE LE NOMBRE DE LIGNES D'UN PERIMETRE --->
	
	<cffunction name="getNombreLignes" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
		<cfargument name="idperimetre" 	required="true" type="Numeric"/>
		
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idcodelangue 	= session.user.idglobalization>
			<cfset var codelangue 		= session.user.globalization>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_mdm.get_nblignes_perimetre">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idperimetre" value="#idperimetre#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour> 
	</cffunction>
	
	<!--- SAUVEGARDE UNE CAMPAGNE --->
	
	<cffunction name="saveCampagne" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
		<cfargument name="libelle" 		required="true" type="String"/>
		<cfargument name="description" 	required="true" type="String"/>
		<cfargument name="original" 	required="true" type="String"/>
		<cfargument name="currentuuid" 	required="true" type="String"/>
		<cfargument name="idperimetre" 	required="true" type="Numeric"/>
		
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idcodelangue 	= session.user.idglobalization>
			<cfset var codelangue 		= session.user.globalization>
				
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_mdm.new_campagne">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_libelle_campagne" 	value="#libelle#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_desc_campagne" 		value="#description#">			
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_perimetre_campagne" 	value="#idperimetre#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_reference_campagne"  value="#original#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_VARCHAR" variable="p_uuid_fichier" 		value="#currentuuid#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid_create" 	value="#idgestionnaire#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour"><!--- p_idmdm_campagne --->
			</cfstoredproc>
					
		<cfreturn p_retour> 
	</cffunction>
	
	<!--- ENVOI UNE CAMPAGNE --->

	<cffunction name="sendCampagne" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
		<cfargument name="libelle" 		required="true" type="String"/>
		<cfargument name="description" 	required="true" type="String"/>
		<cfargument name="original" 	required="true" type="String"/>
		<cfargument name="currentuuid" 	required="true" type="String"/>
		<cfargument name="idperimetre" 	required="true" type="Numeric"/>
		<cfargument name="idcampagn" 	required="true" type="Numeric"/>
		
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idcodelangue 	= session.user.idglobalization>
			<cfset var codelangue 		= session.user.globalization>
			
			<cfset var p_retour = 0>
				
			<cfif idcampagn EQ 0>
			
				<cfset var rsltSave = saveCampagne(libelle, description, original, currentuuid, idperimetre)>
			
			<cfelse>
			
				<cfset var rsltSave = idcampagn>
			
			</cfif>
				
			<cfif rsltSave GT 0>
			
				<cfset p_retour = rsltSave>
				<!--- 
				<cfset var idcampagne 			= rsltSave>
				<cfset var currentracine 		= idracine>
				<cfset var currentgestionnaire 	= idgestionnaire>
				 --->
				<cfthread action="run" name="sendSMSToLignes" idcampagne="#rsltSave#" currentracine="#idracine#" currentgestionnaire="#idgestionnaire#">

					<cfset var rsltLignesForSMS = getLignesForSMS(idcampagne, currentracine, currentgestionnaire)>

					<cftry>

						<cfif rsltLignesForSMS.STATUT GT 0>
							
							<cfset var currentLignes 	= rsltLignesForSMS.LIGNES>
							<cfset var rsltLigne 		= arrayNew(1)>
							
							<cfloop query="currentLignes">
								
								<cfset var serial 		= ''>
								<cfset var mdmimei 		= ''>
								<cfset var soustete 	= currentLignes['USERNAME'][currentLignes.currentRow]>
								<cfset var idsoustete 	= currentLignes['IDSOUS_TETE'][currentLignes.currentRow]>
								<cfset var idlogCampCur	= currentLignes['IDMDM_LOG_CAMPAGNE'][currentLignes.currentRow]>
								<cfset var idpool 		= ''>
								<cfset var infosLignes 	= structNew()>
								
								<cfset var username 	= currentLignes['USERNAME'][currentLignes.currentRow]>
								<cfset var password 	= currentLignes['USERPWD'][currentLignes.currentRow]>
								
								<cfset rsltFindNoCase = FindNoCase("+33", sousTete)>
			
								<cfif rsltFindNoCase EQ 1>
								
									<cfset soustete = "0" & Mid(sousTete, rsltFindNoCase+3, Len(sousTete))>
								
								</cfif>
								
								<cfif idpool EQ ''>
								
									<cfset var idpool = 0>
								
								</cfif>
								
								<cfif idsoustete EQ ''>
								
									<cfset var idsoustete = 0>
								
								</cfif>
																
								<cfset var mdmResult = createObject("component", "fr.consotel.api.mdm.EnrollDevice").execute(serial, mdmimei, soustete, idsoustete, idpool, idlogCampCur, idcampagne)>
															
								<cfset var usrRslt = createObject("component", "fr.consotel.consoview.M111.MDMService").createUserMDM('DEMO', username, password, 'USER')>
						
								<cfset arrayAppend(rsltLigne, soustete)>
								
							</cfloop>	
														 
						<cfelse>
					
							<!---  --->
						
						</cfif>

					<cfcatch>
						
						<cfmail from="mdmCampagne@consotel.fr" to="dev@consotel.fr" server="mail.consotel.fr" port="25"
									failto="dev@consotel.fr" bcc="dev@consotel.fr" 
								subject="[WARN-CATCH LAUNCH_CAMPAGNE]Dump launch_campagne catch M27" type="text/html">
							
								<cfoutput>
									
									Lancement de campagne en erreur-> <br/><br/>
									
									idcamapgne 		: #idcampagne#<br/>
									racine 			: #currentracine#<br/>
									gestionnaires 	: #currentgestionnaire#<br/><br/>
									result 			: <br/><cfdump var="#rsltLignesForSMS#"><br/>
									
									message 		: <br/>#CFCATCH.Message#<br/>
									
									catch 			: <br/><cfdump var="#CFCATCH#"><br/>
								
								</cfoutput>
									
						</cfmail>
						
					</cfcatch>
					</cftry> 
				
				</cfthread>

			</cfif>
					
		<cfreturn p_retour> 
	</cffunction>
	
	<!--- ENVOI UNE CAMPAGNE --->

	<cffunction name="getLignesForSMS" access="remote" output="false" returntype="any" hint="Lecture d'un fichier excel et récupération des informations">
		<cfargument name="idcampagne" 		required="true" type="Numeric"/>
		<cfargument name="idracine" 		required="true" type="Numeric"/>
		<cfargument name="idgestionnaire" 	required="true" type="Numeric"/>
				
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_mdm.launch_campagne">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_campagne" 		value="#idcampagne#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 			value="#idracine#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" 		value="#idgestionnaire#">
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
				<cfprocresult name="p_ret_ligne" resultset="1">
			</cfstoredproc>
			
			<cfset var infos = structNew()>
 			<cfset var infos.STATUT = p_retour>
			<cfset var infos.LIGNES = p_ret_ligne>
					
		<cfreturn infos> 
	</cffunction>

	<!--- LISTE DES LIGNES D'UNE CAMPAGNE --->

	<cffunction name="getLignesCampagne" access="remote" output="false" returntype="any" hint="Liste des lignes d'une campagne">
		<cfargument name="idcampagne" 	required="true" type="Numeric"/>
			
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idcodelangue 	= session.user.idglobalization>
			<cfset var codelangue 		= session.user.globalization>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_mdm.liste_ligne_campagne">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_campagne" 	value="#idcampagne#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_langid" 			value="#idcodelangue#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour> 
	</cffunction>

	<!--- HISTORIQUE D'UNE LIGNE D'UNE CAMPAGNE  --->
	
	<cffunction name="getHistoriqueLigneCampagne" access="remote" output="false" returntype="any" hint="Liste des lignes d'une campagne">
		<cfargument name="idlogcampagne" 	required="true" type="Numeric"/>
			
			<cfset var idracine 		= session.perimetre.id_groupe>
			<cfset var idgestionnaire 	= session.user.clientaccessid>
			<cfset var idcodelangue 	= session.user.idglobalization>
			<cfset var codelangue 		= session.user.globalization>
			
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_mdm.histo_ligne_campagne">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_idmdm_log_campagne" 	value="#idlogcampagne#">
				<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" variable="p_langid" 				value="#idcodelangue#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
					
		<cfreturn p_retour> 
	</cffunction>


</cfcomponent>