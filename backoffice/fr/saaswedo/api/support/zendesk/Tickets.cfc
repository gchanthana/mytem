<cfcomponent author="Cedric" displayname="fr.saaswedo.api.support.zendesk.Tickets" extends="fr.saaswedo.api.support.zendesk.RestService"
hint="Fonctionnalités des tickets Zendesk. Les Tickets sont créés exclusivement par les agents du support et les Requests par les utilisateurs finaux (et agents)">
	<cffunction access="public" name="view" returntype="Struct" hint="Retourne la liste des tickets pour la page <b>resultPage</b> (Chaque page contient au plus 15 résultats)">
		<cfargument name="viewId" type="Numeric" required="true" hint="Identifiant du type de vue à retourner (Valeur positive). Ignoré lorsque ticketType vaut REQUEST_TYPE()">
		<cfargument name="resultPage" type="Numeric" required="true" hint="Numéro de la page de résultat à retourner (Valeur positive). Ignoré lorsque ticketType vaut REQUEST_TYPE(). La 1ère page correspond à la valeur 1">
		<cfif (ARGUMENTS["viewId"] GTE 0) AND (ARGUMENTS["resultPage"] GTE 0)>
			<cfset var requestContent={}>
			<cfset var xmlTarget="api/v1/rules/views/#ARGUMENTS.viewId#">
			<cfif ARGUMENTS["resultPage"] GT 0>
				<cfset requestContent={page=ARGUMENTS["resultPage"]}>
			</cfif>
			<cfreturn sendJSONRequest(xmlTarget,GET_METHOD(),requestContent,"")>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="Les valeurs des paramètres viewId et resultPage doivent etre positives">
		</cfif>
	</cffunction>

	<cffunction access="public" name="show" returntype="Struct" hint="Retourne les infos contenues dans un ticket">
		<cfargument name="ticketId" type="Numeric" required="true" hint="Identifiant du ticket (Valeur positive)">
		<cfif ARGUMENTS["ticketId"] GT 0>
			<cfset var xmlTarget="api/v2/tickets/#ARGUMENTS.ticketId#">
			<cfreturn sendJSONRequest(xmlTarget,GET_METHOD(),{},"")>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur du paramètre ticketId doit etre positive">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="create" returntype="Struct" hint="Crée un ticket et retourne les infos qu'il contient">
		<cfargument name="ticketInfos" type="Struct" required="true" hint="Structure contenant les infos du ticket et correspondant aux clés suivantes :<br>
		<b>Les clés doivent etre spécifiées entre doubles quotes et etre en minuscules (Cause : Sérialization JSON et compatibilité avec le service Zendesk)</b><br>
		subject : Sujet du ticket<br>
		description : Description du ticket<br>
		requester_name : Prénom Nom du demandeur (Requester)<br>
		requester_email : Email du demandeur<br>
		tags : Tableau contenant les tags à affecter au ticket<br>
		fields : Tableau où chaque élément est une structure contenant les clés ID (Identifiant d'un champ de ticket), VALUE (Valeur à attribuer au champ de ticket correspondant)">
		<cfargument name="onBehalfOf" type="String" required="true" hint="Chaine vide ou email d'un utilisateur pour lequel effectuer l'opération (Impersonate Login)">
		<cfset var apiVersion="v2">
		<cfset var xmlTarget="api/#apiVersion#/tickets">
		<!--- Bug : Ticket API v2 does not support the X-On-Behalf-Of header --->
		<cfset var impersonateLogin=(apiVersion EQ "v1") ? ARGUMENTS["onBehalfOf"]:"">
		<cfreturn sendJSONRequest(xmlTarget,POST_METHOD(),ARGUMENTS["ticketInfos"],impersonateLogin)>
	</cffunction>

	<cffunction access="public" name="update" returntype="void" hint="Met à jour les infos d'un ticket">
		<cfargument name="ticketId" type="Numeric" required="true" hint="Identifiant du ticket (Valeur positive)">
		<cfargument name="ticketUpdateInfos" type="Struct" required="true" hint="Structure contenant les infos à mettre à jour pour le ticket">
		<cfargument name="onBehalfOf" type="String" required="true" hint="Chaine vide ou email d'un utilisateur pour lequel effectuer l'opération (Impersonate Login)">
		<cfif ARGUMENTS["ticketId"] GT 0>
			<cfset var apiVersion="v1">
			<cfset var xmlTarget="api/#apiVersion#/tickets/#ARGUMENTS.ticketId#">
			<!--- Bug : Ticket API v2 does not support the X-On-Behalf-Of header --->
			<cfset var impersonateLogin=(apiVersion EQ "v1") ? ARGUMENTS["onBehalfOf"]:"">
			<cfset requestResult=sendJSONRequest(xmlTarget,PUT_METHOD(),ARGUMENTS["ticketUpdateInfos"],impersonateLogin)>
			<cfif requestResult["STATUS"]["CODE"] NEQ SUCCESS()>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur du paramètre ticketId doit etre positive">
		</cfif>
	</cffunction>
	
	<cffunction access="public" name="updateRequest" returntype="void" hint="Met à jour les infos d'un ticket (En passant par l'URI api/[API_VERSION]/requests/)">
		<cfargument name="ticketId" type="Numeric" required="true" hint="Identifiant du ticket (Valeur positive)">
		<cfargument name="ticketUpdateInfos" type="Struct" required="true" hint="Structure contenant les infos à mettre à jour pour le ticket">
		<cfargument name="onBehalfOf" type="String" required="true" hint="Chaine vide ou email d'un utilisateur pour lequel effectuer l'opération (Impersonate Login)">
		<cfif ARGUMENTS["ticketId"] GT 0>
			<cfset var apiVersion="v1">
			<cfset var xmlTarget="api/#apiVersion#/requests/#ARGUMENTS.ticketId#">
			<cfset var impersonateLogin=(apiVersion EQ "v1") ? ARGUMENTS["onBehalfOf"]:"">
			<cfset requestResult=sendJSONRequest(xmlTarget,PUT_METHOD(),ARGUMENTS["ticketUpdateInfos"],impersonateLogin)>
			<cfif requestResult["STATUS"]["CODE"] NEQ SUCCESS()>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur du paramètre ticketId doit etre positive">
		</cfif>
	</cffunction>

	<cffunction access="public" name="delete" returntype="void" hint="Supprime un ticket et retourne les infos concernant l'opération effectuée">
		<cfargument name="ticketType" type="Numeric" required="true" hint="Valeur indiquant le type de ticket concerné e.g REQUEST_TYPE()">
		<cfargument name="ticketId" type="Numeric" required="true" hint="Identifiant du ticket (Valeur positive)">
		<cfif ARGUMENTS["ticketId"] GT 0>
			<cfset var xmlTarget="api/v2/tickets/#ARGUMENTS.ticketId#">
			<cfset var requestResult=sendJSONRequest(xmlTarget,DELETE_METHOD(),{},"")>
			<cfif requestResult["STATUS"]["CODE"] NEQ SUCCESS()>
				<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="#requestResult.FAULT.MESSAGE#" detail="HTTP CODE : #requestResult.STATUS.HTTP_STATUS#">
			</cfif>
		<cfelse>
			<cfthrow type="Custom" errorcode="ILLEGAL_OPERATION" message="La valeur du paramètre ticketId doit etre positive">
		</cfif>
	</cffunction>
</cfcomponent>