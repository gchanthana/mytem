<cfcomponent implements="portail.class.specialClass.InterfacePage"
			 output="false"
			 displayname="Page" 
			 hint="Classe qui contient la structure de la page Mon terminal dans le template D1" >
	
	
	<!---  --->
  	<cffunction name="afficher" access="public" output="true" 
				displayname="" 
				hint="Affiche les composants de la page Mon terminal / template D1" >
		<cfargument name="head" required="true" type="string" displayname="" hint="Contenu de la balise header de la page à afficher">
		<cfargument name="context" required="true" type="struct"displayname="" hint="Structure contenant les données passés dans l'URL" >
		
		
		<!--- CREATION DUNE STRUC ET RECUPERATION DES DONNEES PASSEES EN URL DANS CETTE STRUCTURE --->
		<cfset refParam = createObject("component","portail.class.specialClass.AbstractComposant").getInfoSructURL(context) >
		
		<cfset objHeader = createObject("component","portail.D1.defaut.composant.Header") >
		<cfif NOT isDefined("refParam.idSous_tete")>
			<cfset header = objHeader.getHeader() >
		<cfelse>
			<cfset header = objHeader.getHeader(refParam.idPage,refParam.idSous_tete,refParam.sous_tete) >
		</cfif>
		
		
		<cfset objDivDetailAppel = createObject("component","portail.D1.defaut.composant.MesAppelsDivDetails") >
		<cfif NOT isDefined("refParam.idSous_tete") && NOT isDefined("refParam.idPeriode") >
			<cfset divDetailAppel = objDivDetailAppel.getDivDetailAppel() >
		<cfelseif isDefined("refParam.idInventaire_periode") >
			<cfset divDetailAppel = objDivDetailAppel.getDivDetailAppel(refParam.sous_tete,refParam.idSous_Tete,refParam.idInventaire_periode) >
		<cfelse>
			<cfset divDetailAppel = objDivDetailAppel.getDivDetailAppel(refParam.sous_tete,refParam.idSous_Tete) >
		</cfif>
		
		
		<cfoutput>
			<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
				#head#
				<body onunload="disableLoadsablier();">
					<div class="ombre" id="ombre">
					    <div class="background_ecran">
					        <img class="loading_image" id="imgLoading1" src="img/loading-icon.gif" />
					    </div>
					</div>
					<div id="global" >
						#header#
						<div style="width:auto;">
							<div class="divDetailAppel" style="">
								#divDetailAppel#
							</div>
						</div>
					</div>
				</body>
			</html>
		</cfoutput>
		
	</cffunction>
	
</cfcomponent>