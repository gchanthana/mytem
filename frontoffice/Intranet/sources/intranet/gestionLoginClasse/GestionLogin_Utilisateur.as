package intranet.gestionLoginClasse
{
	import mx.collections.ArrayCollection;
	
	public class GestionLogin_Utilisateur
	{
		private var idUtilisateur : int;
		private var nomUtilisateur : String;
		private var prenomUtilisateur : String;
		private var mailUtilisateur : String;
		private var MDPUtilisateur : String;
		private var  listeIDMaitreSesAcces : Array = new Array;
		
		public function GestionLogin_Utilisateur(IDutilisateur : int,nomUtilisateur : String,prenomUtilisateur : String,mailUtilisateur : String,MDPUtilisateur : String){
			this.idUtilisateur = IDutilisateur;
			this.nomUtilisateur = nomUtilisateur;
			this.prenomUtilisateur=prenomUtilisateur;
			this.mailUtilisateur=mailUtilisateur;
			this.MDPUtilisateur= MDPUtilisateur;
		}
		
		public function getIdUtilisateur() : int{
			return idUtilisateur;
		}
		public function getPrenomUtilisateur():String{
			return prenomUtilisateur;
		}
		public function getNomUtilisateur():String{
			return nomUtilisateur;
		}
		public function getMailUtilisateur():String{
			return mailUtilisateur;
		}
		public function getMDPUtilisateur():String{
			return MDPUtilisateur;
		}
		public function getListeIDMaitreDeSesAcces():Array{
			return listeIDMaitreSesAcces;
		}
		public function setListeIDMaitreDeSesAcces(listeIDMaitreSesAcces : Array) : void{
			this.listeIDMaitreSesAcces = listeIDMaitreSesAcces;
		}
		
	}
}