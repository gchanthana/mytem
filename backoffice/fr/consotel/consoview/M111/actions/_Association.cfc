﻿<cfcomponent>

	<cfset init()>
	
	<cffunction name="init" hint="pour les variables de session" access="remote">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLangue 	= SESSION.USER.IDGLOBALIZATION>
	</cffunction>

	<!--- ----------------- DEBUT ASSOCIATION ----------------- --->

	<!--- 
		associerCollabToLigneSim : Association d'un collab avec une ligne-sim
	 --->
	<cffunction name="associerCollabToLigneSim" access="remote" returntype="numeric" hint="associerCollabToLigneSim : Association d'un collab avec une ligne-sim">
		<cfargument name="idGpeClient" 		required="true" type="Numeric" />
		<cfargument name="libelleCollab" 	required="true" type="String" />
		<cfargument name="idSim" 			required="true" type="Numeric" />
		<cfargument name="idSousTete" 		required="true" type="Numeric" />
		<cfargument name="libelleSim" 		required="true" type="String" />
		<cfargument name="libelleSousTete" 	required="true" type="String" />
		<cfargument name="dateEffet" 		required="true" type="String" />
		<cfargument name="pret" 			required="true" type="Numeric" />
		
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_emp_v2">
			<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idGpeClient#"  >
			<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#idSim#" >
			<cfprocparam type="in" 	cfsqltype="CF_SQL_INTEGER" value="#pret#" >
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="affectStatus" >
		</cfstoredproc>
		
		<cfif affectStatus gt 0>
			<cfset myFicheCommun = CreateObject("component","fr.consotel.consoview.M111.fiches.FicheCommun")>
 			<cfif pret eq 0>
         		<cfset ID_WORD = 3>
			<cfelse>
				<cfset ID_WORD = 14>
			</cfif>
			<!--- carte sim fournie au collab --->
           	<cfset myFicheCommun.logAction(ID_WORD,1,libelleSim,libelleCollab,-1,idSim,idGpeClient,-1,-2,dateEffet)/>
           	
			<!--- ligne fournie au collab --->
           	<cfset myFicheCommun.logAction(2,1,libelleSousTete,libelleCollab,-1,-1,idGpeClient,idSousTete,-2,dateEffet)/>
      
        <cfelse>
            <cfreturn affectStatus>
		</cfif>
		
		<cfreturn affectStatus>
	</cffunction>


	<!--- 
		associerCollabWithTermToLigneSim : Association d'un collab possedant un terminal avec une ligne-sim
	 --->
	 	<cffunction name="associerCollabWithTermToLigneSim" access="remote" returntype="numeric" hint="associerCollabWithTermToLigneSim : Association d'un collab possedant un terminal avec une ligne-sim">
		<cfargument name="idGpeClient" 		required="true" type="Numeric">
		<cfargument name="libelleCollab"	required="true" type="String">
		<cfargument name="idTerm" 			required="true" type="Numeric">
		<cfargument name="libelleTerm" 		required="true" type="String">
		<cfargument name="idSim" 			required="true" type="Numeric">
		<cfargument name="libelleSim" 		required="true" type="String">
		<cfargument name="idSousTete" 		required="true" type="Numeric">
		<cfargument name="libelleSousTete" 	required="true" type="String">
		<cfargument name="dateEffet" 		required="true" type="String">

		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="PKG_CV_FLOTTE.Affect_equip_parent_v2">
			<cfprocparam type="in"	cfsqltype="CF_SQL_INTEGER" value="#idTerm#" >
			<cfprocparam type="in"	cfsqltype="CF_SQL_INTEGER" value="#idSim#" >
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="affectStatus" >
		</cfstoredproc>
		
		<cfif affectStatus gt 0>
				<cfset myFicheCommun = CreateObject("component","fr.consotel.consoview.M111.fiches.FicheCommun")>
				
				<!--- carte sim associée au term --->
				<cfset myFicheCommun.logAction(11,1,libelleSim,libelleTerm,idTerm,idSIM,-1,-1,-2,dateEffet)/>
				
				<!--- ligne associée au term --->
				<cfset myFicheCommun.logAction(7,1,libelleSousTete,libelleTerm,idTerm,-1,-1,idSousTete,-2,dateEffet)/>
				
				<!--- carte sim fournie au collab --->
	           	<cfset myFicheCommun.logAction(3,1,libelleSim,libelleCollab,-1,idSim,idGpeClient,-1,-2,dateEffet)/>
	           	
				<!--- ligne fournie au collab --->
	           	<cfset myFicheCommun.logAction(2,1,libelleSousTete,libelleCollab,-1,-1,idGpeClient,idSousTete,-2,dateEffet)/>
      
            <cfelse>
                  <cfreturn affectStatus>
            </cfif>
		<cfreturn affectStatus>
	</cffunction>

	<!--- ----------------- FIN ASSOCIATION ----------------- --->

</cfcomponent>