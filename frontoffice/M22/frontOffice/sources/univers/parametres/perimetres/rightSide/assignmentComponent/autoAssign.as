package univers.parametres.perimetres.rightSide.assignmentComponent
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	import mx.core.ClassFactory;
	import mx.events.FlexEvent;
	import mx.events.SliderEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	
	public class autoAssign extends autoAssignIHM
	{
		private var _sdaList:ArrayCollection;
		private var _sdaListCopy:ArrayCollection;
		public var _currentDate:Date;
		private var _data:Array;
		
		private var _sdaAssign:ArrayCollection;
		private var _sdaNotAssign:ArrayCollection;
		
		private var _beginField:String;
		private var _newNode:Object;
	
		
		
		
		public function autoAssign()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);	
		}
		
		protected function initIHM(event:Event):void
		{
			btnAssign.addEventListener(MouseEvent.CLICK, btnAssignClicked);
			btnCloseDown.addEventListener(MouseEvent.CLICK, btnCloseDownClicked);
			gridNodes.addEventListener(Event.CHANGE, onselectedGroupChange);
			txtFilterNAssign.addEventListener(Event.CHANGE, refreshDatagridsNAssign);
			txtFilterAssign.addEventListener(Event.CHANGE, refreshDatagridsAssign);
			psDate.setCallBack(periodeChange);
			fillGrid();
		} 
		
		/** ############################### EVENTS ################################# */
		
		protected function onselectedGroupChange(event:Event):void
		{	
			refreshDatagrids();
		}
		
		protected function  btnAssignClicked(event: MouseEvent):void
		{
			/*if (dgridNotAffect.selectedIndex == -1)
				return ;*/
			var idgroupe:String = gridNodes.selectedItem.NODE_ID;
			var date:String = dfs.format(psDate.getSelectedDate());
			//var idSda:String = dgridNotAffect.selectedItem.SDAID;
			var idSda:String = getSelectedLinesList(dgridNotAffect);
			var flag:Number = 0;
			if (assignNext.selected)
				flag = 1;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"assignSda",
																				assignSdaResult,
																				throwError);	
			RemoteObjectUtil.callService(op, idgroupe, date, idSda, flag);	
			
			for each (var obj:Object in dgridNotAffect.selectedItems)
			{
				obj.IDGROUPE_CLIENT = parseInt(gridNodes.selectedItem.NODE_ID);
			}					
//			dgridNotAffect.selectedItem.IDGROUPE_CLIENT = gridNodes.selectedItem.NODE_ID;
			_sdaAssign = ObjectUtil.copy(_sdaNotAssign) as ArrayCollection;
			refreshDatagrids();
		}
		
		private function assignSdaResult(event:ResultEvent):void
		{
			
		}
		
		protected function btnCloseDownClicked(event: MouseEvent):void
		{
			/*if (dgridAffect.selectedIndex == -1)
				return ;			*/
				
			var idgroupe:String = gridNodes.selectedItem.NODE_ID;
			var date:String = dfs.format(psDate.getSelectedDate());
//			var idSda:String = dgridAffect.selectedItem.SDAID;
			var idSda:String = getSelectedLinesList(dgridAffect);
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"closedownSda",
																				closedownSdaResult,
																				throwError);	
			RemoteObjectUtil.callService(op, idgroupe, date, idSda);	
			
//			dgridAffect.selectedItem.IDGROUPE_CLIENT = 0;
			for each (var obj:Object in dgridAffect.selectedItems)
			{
				obj.IDGROUPE_CLIENT = 0;
			}			
			_sdaNotAssign = ObjectUtil.copy(_sdaAssign) as ArrayCollection;
			refreshDatagrids();
		}
		
		private function closedownSdaResult(event:ResultEvent):void
		{
			
		}
		
		public function periodeChange(event: SliderEvent):void
		{
			refreshMainGridView();
		}
		
		/** ############################# END EVENTS ############################### */
		
		private function getNodes():void
		{
			var lineID:String = parentDocument._lineID;
			var date_start:Date = new Date(_currentDate.getFullYear(), _currentDate.getMonth()-12);
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"getCoefByDate",
																				getNodesResult,
																				throwError);				
			RemoteObjectUtil.callService(op, lineID, dfs.format(date_start), dfs.format(_currentDate));	
		}
		
		private function getNodesResult(event:ResultEvent):void
		{
			var tmp:Array = event.result as Array;
			for each (var o:Object in tmp)
			{
				var x:XML = new XML();
				x.@NID = o.NODE_ID;
				if (isNodeNotExist(x, _data))
					_data.push({NODE_ID:o.NODE_ID, NODE:o.NODE});
			}
			fillTotal();
		}
		
		private function fillTotal():void
		{
			_data.push({NODE:"Total", visible: false});
			//_data = _data.concat(_data);
			gridNodes.dataProvider = _data; 
			initGrid(_currentDate);
			gridNodes.itemRenderer = new ClassFactory(itemRender);
			fillEmptyField();
			refreshMainGridView();
		}
		
		
		private function fillGrid():void
		{
			var lineID:String = parentDocument._lineID;
			var orgaID:String = parentDocument._orgaID;
				
			_currentDate = new Date();
			var date_start:Date = new Date(_currentDate.getFullYear(), _currentDate.getMonth()-12);
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"getCoefAutoByDate",
																				fillGridResult,
																				throwError);				
			_beginField = "col" + dfs.format(date_start);
			RemoteObjectUtil.callService(op, lineID, dfs.format(date_start), dfs.format(_currentDate), orgaID);	
		}
		
		private function fillGridResult(event:ResultEvent):void
		{
			_data = event.result as Array;
			var tmp:Array = new Array();
			if (isNodeNotExist(parentDocument._newNode, _data))
				_data.push({NODE:parentDocument._newNode.@LBL, NODE_ID:parentDocument._newNode.@NID});
			/*_data.push({NODE:"Total", visible: false});
			_data = tmp.concat(_data);
			gridNodes.dataProvider = _data; 
			initGrid(_currentDate);
			gridNodes.itemRenderer = new ClassFactory(itemRender);
			fillEmptyField();
			refreshMainGridView();*/
			getNodes();
		}
		
		/** Verifie si la ligne a été déjà affectée au noeud, sinon ca rajoute une ligne
		 * dans le tableau */
		private function isNodeNotExist(newNode:Object, data:Array):Boolean
		{
			if (newNode == null)
				return false;
			var newNodeID:Number = parseInt(newNode.@NID);
			if (newNodeID < 0)
				return false;
			for (var j:Number = 0; j < _data.length; j++)
			{
				if (_data[j].NODE_ID == newNodeID)
					return false;
			}
			return true;
		}
		
		private function initGrid(d:Date):void
		{
			for (var i:Number = 1; i  < gridNodes.columns.length; i++)
			{
				var coef:Number = 12 - i;
				var newDate:Date = new Date(d.getFullYear(), d.getMonth()-coef, d.date);
				gridNodes.columns[i].dataField = "col" + df_fieldName.format(newDate);
				
				gridNodes.columns[i].headerText = df.format(newDate);
			}
		}
		
		private function fillEmptyField():void
		{
			for (var j:Number = 0; j < _data.length-1; j++)
			{
				var oldValue:Number = 0;
				for (var i:Number = 1; i  < gridNodes.columns.length; i++)
				{
					var field:String = gridNodes.columns[i].dataField;
					if (_data[j][field] == null)
					{
						if (i == 1 && _data[j][_beginField])
						{
							_data[j][field] = _data[j][_beginField];
							_data[j]["flag"] = j;
						}
						else
							_data[j][field] = oldValue;
							
						if (_data[j]["fin" + field.substr(3)] != null)
							oldValue = 0;
					}
					else
						oldValue = _data[j][field];
				}
			}
			calculateTotal();
		}
		
		private function calculateTotal():void
		{	
			for (var i:Number = 1; i  < gridNodes.columns.length; i++)
			{
				var field:String = gridNodes.columns[i].dataField;
				
				var total:Number = 0;
				for (var j:Number = 0; j < _data.length-1; j++)
					if (_data[j][gridNodes.columns[i].dataField] != null)
						total += parseInt(_data[j][field]);
						
				_data[_data.length-1][field] = total;
				
				if (total > 100)
					gridNodes.columns[i].setStyle("backgroundColor", "red");
				else
					gridNodes.columns[i].setStyle("backgroundColor", "none");
			}
		}
		
		private function refreshMainGridView():void
		{
			for (var i:Number = 1; i  < gridNodes.columns.length; i++)
			{
				var field:String = gridNodes.columns[i].dataField;
				var sDateStr:String = "col" + df_fieldName.format(psDate.getSelectedDate());
				if (sDateStr == field)
					gridNodes.columns[i].setStyle("backgroundColor", 0xAADEFF);
				else
					gridNodes.columns[i].setStyle("backgroundColor", "none");
			}
			txtPeriod.text = "Date : " + titleDate.format(psDate.getSelectedDate());
			getSdabyDate(dfs.format(psDate.getSelectedDate()));
		}
		
		private function getSdabyDate(date:String):void
		{
			var lineID:String = parentDocument._lineID;
			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.parametres.perimetres.gestionPerimetreAssignment",
																				"getSdaByDate",
																				getSdabyDateResult,
																				throwError);				
			RemoteObjectUtil.callService(op, lineID, date, parentDocument._orgaID);	
		}
		
		private function getSdabyDateResult(event: ResultEvent):void
		{
			_sdaAssign = event.result as ArrayCollection;
			_sdaNotAssign = ObjectUtil.copy(_sdaAssign) as ArrayCollection;
			 
			refreshDatagrids();
			
			btnAssign.enabled =  true;
			btnCloseDown.enabled =  true;
		}
		
		private function refreshDatagrids():void
		{	
			refreshDatagridsAssign(new Event("init"));
			refreshDatagridsNAssign(new Event("init"));
		}
		
		protected function refreshDatagridsAssign(event:Event):void
		{
			dgridAffect.dataProvider = _sdaAssign;
			dgridAffect.dataProvider.filterFunction = filterAssign;
			dgridAffect.dataProvider.refresh();
		}
		
		protected function refreshDatagridsNAssign(event:Event):void
		{
			dgridNotAffect.dataProvider = _sdaNotAssign;
			dgridNotAffect.dataProvider.filterFunction = filterNotAssign;
			dgridNotAffect.dataProvider.refresh();
		}
		
		private function filterNotAssign(item:Object):Boolean
		{
			if (gridNodes.selectedIndex < 0)
				return false;
			
			var nb:Number = parseInt(item.IDGROUPE_CLIENT);
			var found:Boolean =(item.SDA.toLowerCase().search(txtFilterNAssign.text.toLowerCase()) != -1);
			if (!found && txtFilterNAssign.text != "")
				return false;
			if (!nb)
				return true;
			return false;
		}
		
		private function filterAssign(item:Object):Boolean
		{
			if (gridNodes.selectedIndex < 0)
				return false;
				
			var nb:Number = parseInt(item.IDGROUPE_CLIENT);
			var node_id:Number = gridNodes.selectedItem.NODE_ID;
			if (isNaN(node_id))
				node_id = parseInt(gridNodes.selectedItem.NODE_ID);
			var found:Boolean =(item.SDA.toLowerCase().search(txtFilterAssign.text.toLowerCase()) != -1);
			if (!found && txtFilterAssign.text != "")
				return false;
			if (nb == node_id)
				return true;
			return false;
		}
		
		/** Return an list id corresponding to the selected items */
		public function getSelectedLinesList(grid:DataGrid):String
		{
			var items:String = "";
			var bFlag:Boolean = true;
			for each (var obj:Object in grid.selectedItems)
			{
				if (!bFlag)
					items += ",";
				items += obj.SDAID;
				bFlag = false;
			}
			return items;
		}
		
		/** ############################ PUBLIC METHODS ############################# */
		
		public function setDate(data:Date):void
		{
			txtPeriod.text = df.format(data);
			getSdabyDate(dfs.format(data));
		}
		
		public function addNodeAssign(obj:Object):void
		{
			_newNode = obj;
		}
		
		/** ########################### END PUBLIC METHODS ########################### */
		
		
		
		private function throwError(result:FaultEvent):void
		{
			
		}	
		
	}
}