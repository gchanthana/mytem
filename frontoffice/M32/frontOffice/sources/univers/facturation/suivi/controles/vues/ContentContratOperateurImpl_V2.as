package univers.facturation.suivi.controles.vues
{
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import mx.containers.VBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Classe concernant toutes les informations d'un contrat opérateur.
	 * 
	 * Auteur : Guiou Nicolas
	 * Version : 1.0
	 * Date : 1.09.2010
	 * 
	 * </pre></p>
	 * 
	 */
	[Bindable]
	public class ContentContratOperateurImpl_V2 extends VBox
	{
		// VARIABLES------------------------------------------------------------------------------
		
		// IHM components
		public var dcDateOuverture			:CvDateChooser;
		public var dcDateRenouvellement		:CvDateChooser;
		public var dcDateResiliation		:CvDateChooser;
		
		public var lbDateEligibilite		:Label;
		public var lbDateLastFacture		:Label;
		public var lbDateFpc				:Label;
		public var lbOperateur				:Label;
		
		public var lbDureeContrat			:Label;
		public var lbDateOuverture			:Label;
		public var lbDateRenouvellement		:Label;
		public var lbDateResiliation		:Label;
		public var lbDureeEligibilite		:Label;
		
		public var cbDureeContrat			:ComboBox;
		public var cbDureeEligibilite		:ComboBox;
		
		// publics variables 
		
		// privates variables
		
		// VARIABLES------------------------------------------------------------------------------
		
		// PUBLIC FUNCTIONS-------------------------------------------------------------------------
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe ContentContratOperateurImpl_V2.
		 * </pre></p>
		 */	
		public function ContentContratOperateurImpl_V2()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE,init);
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialisation générale de cette classe  :
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 * </pre></p>
		 * 
		 * @param event:<code>FlexEvent</code>.
		 * @return void.
		 * 
		 * @see #initListener()
		 */
		private function init(event:FlexEvent):void
		{
			initListener();
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Initialise les écouteurs.
		 * </pre></p>
		 * 
		 * @return void.
		 */	
		private function initListener():void
		{
			if(dcDateOuverture) // fiche ligne
			{
				cbDureeContrat.addEventListener(ListEvent.CHANGE,dcDateChangeHandler);
				cbDureeEligibilite.addEventListener(ListEvent.CHANGE,dcDateChangeHandler);
				dcDateOuverture.addEventListener(CalendarLayoutChangeEvent.CHANGE,dcDateChangeHandler);
				dcDateRenouvellement.addEventListener(CalendarLayoutChangeEvent.CHANGE,dcDateChangeHandler);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Cette fonction est appelée après le changement de la date d'ouverture, de la date de renouvellement, 
		 * de la duree d'eligibilite ou de la duree du contrat.
		 * </pre></p>
		 * 
		 * @param event:<code>Event</code>.
		 * @return void.
		 * 
		 * @see #calculDateEligibilite()
		 * @see #calculDateFPC()
		 */	
		private function dcDateChangeHandler(event:Event):void
		{
			lbDateEligibilite.text 	= DateFunction.formatDateAsString(calculDateEligibilite());
			lbDateFpc.text 			= DateFunction.formatDateAsString(calculDateFPC());
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Calcul la date d'eligibilite
		 * </pre></p>
		 * 
		 * @return Date.
		 */	
		private function calculDateEligibilite():Date
		{
			if(dcDateRenouvellement.selectedDate)
			{
				return DateFunction.dateAdd("m",dcDateRenouvellement.selectedDate,cbDureeEligibilite.selectedItem.value);
			}
			else
			{
				return DateFunction.dateAdd("m",dcDateOuverture.selectedDate,cbDureeEligibilite.selectedItem.value);
			}
		}
		
		/**
		 * <p><pre>
		 * <u>Description :</u>
		 * Calcul la date FPC
		 * </pre></p>
		 * 
		 * @return Date.
		 */	
		private function calculDateFPC():Date
		{
			if(dcDateRenouvellement.selectedDate)
			{
				return DateFunction.dateAdd("m",dcDateRenouvellement.selectedDate,cbDureeContrat.selectedItem.value);
			}
			else
			{
				return DateFunction.dateAdd("m",dcDateOuverture.selectedDate,cbDureeContrat.selectedItem.value);
			}
		}
		
	}
}