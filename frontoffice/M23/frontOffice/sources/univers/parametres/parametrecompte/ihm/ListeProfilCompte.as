package univers.parametres.parametrecompte.ihm
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import univers.parametres.parametrecompte.event.ContrainteSuppressionEvent;
	import univers.parametres.parametrecompte.event.GestionProfilEvent;
	import univers.parametres.parametrecompte.service.ProfilService;
	import univers.parametres.parametrecompte.vo.Profil;
	[Bindable]
	public class ListeProfilCompte extends VBox
	{
		
		public var profilService : ProfilService = new ProfilService();
		//COMPONANT
		public var dg_profil: DataGrid;
		private var popContrainteSuppressionIHM : ContrainteSuppressionIHM;
		private var popUpParametreProfilIHM : ParametreProfillIHM;
		private var popUpUserOfProfilIHM : ListeUtilisateurProfilIHM;
		
		public function ListeProfilCompte() 
		{
			initData()
		}
		//Cette fonction peut être appelée dans la gestion des logins (via un event) --> Quand on change le profil d'un login
		public function initData():void
		{
			profilService.listeProfil(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		public function addProfil(evt : MouseEvent):void
		{		
			popUpParametreProfilIHM = new ParametreProfillIHM();
			popUpParametreProfilIHM.addEventListener(GestionProfilEvent.PARAMETRE_PROFIL_COMPLETE,popupComplete);
			addPop(popUpParametreProfilIHM);
		}
		public function supprimer(profil : Profil):void
		{
			var msg : String;
			if(profil.nbUser>0)
			{
				msg = "Êtes vous sûr de vouloir supprimer le profil "+profil.libelle+" ?\nLes gestionnaires ("+profil.nbUser+") utilisant le profil  "+profil.libelle+" auront le profil par défault. ";	
			}
			else
			{
				msg = "Êtes vous sûr de vouloir supprimer le profil "+profil.libelle+" ?";	
			}
			ConsoviewAlert.afficherAlertConfirmation(msg,"Confirmation",supprimerBtnValiderCloseEvent);
			
		}
		private function supprimerBtnValiderCloseEvent(e:CloseEvent):void{
			if(e.detail == Alert.OK){
				profilService.addEventListener(GestionProfilEvent.LISTE_CONTRAINTE_PROFIL_COMPLETE,listeContrainteOfProfilHandler);
				profilService.listeContrainteOfProfil((dg_profil.selectedItem as Profil).idProfil);
			}
		}
		private function listeContrainteOfProfilHandler(evt : GestionProfilEvent):void
		{
			if(profilService.col_contrainte_profil.length>0) // il existe des contraintes qui empêche la suppression
			{
				popContrainteSuppressionIHM = new ContrainteSuppressionIHM();
				popContrainteSuppressionIHM.addEventListener(ContrainteSuppressionEvent.VALIDATE_SUPPRESSION_POOL,supprimerPoolSelected);
				popContrainteSuppressionIHM.listeContraintes = profilService.col_contrainte_profil;
				popContrainteSuppressionIHM.messageDecription = 'Un ou plusieurs éléments sont rattachés au profil '+(dg_profil.selectedItem as Profil).libelle+'. Vous devez supprimer ces éléments avant de continuer.';
				popContrainteSuppressionIHM.messageConfirmation = 'Êtes vous sûr de vouloir supprimer le profil ?';
				PopUpManager.addPopUp(popContrainteSuppressionIHM,this,true);
				PopUpManager.centerPopUp(popContrainteSuppressionIHM);
			}
			else // pas de contrainte on peut supprimer
			{
				supprimerPoolSelected();
			}
		}
		private function supprimerPoolSelected(evt : ContrainteSuppressionEvent=null):void
		{
			//TODO REQUETE DE SUPPRESSION
			profilService.addEventListener(GestionProfilEvent.REMOVE_PROFIL_COMPLETE,remove_complete_handler);
			profilService.remove_profil((dg_profil.selectedItem as Profil).idProfil);
		}
		private function remove_complete_handler(evt : GestionProfilEvent):void
		{
			ConsoviewAlert.afficherOKImage("Le profil "+(dg_profil.selectedItem as Profil).libelle+" a bien été supprimé.");
			profilService.listeProfil(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		public function btNbUserHandler(profil : Profil):void
		{
			//if(profil.nbUser>0){
			popUpUserOfProfilIHM = new ListeUtilisateurProfilIHM();
			popUpUserOfProfilIHM.profil = dg_profil.selectedItem as Profil;
			popUpUserOfProfilIHM.addEventListener(GestionProfilEvent.LISTE_PROFIL_OFUSER_COMPLETE,popupComplete);
			addPop(popUpUserOfProfilIHM)
			/*}
			else
			{
				Alert.show("Aucun utilisateur n'est relié à ce profil");
			}*/
		}
		public function modifier(profil : Profil):void
		{
			popUpParametreProfilIHM = new ParametreProfillIHM();
			popUpParametreProfilIHM.profil = profil;
			popUpParametreProfilIHM.addEventListener(GestionProfilEvent.PARAMETRE_PROFIL_COMPLETE,popupComplete);
			addPop(popUpParametreProfilIHM);
		}
		private function addPop(pop : UIComponent):void
		{
			PopUpManager.addPopUp(pop,this,true);
			PopUpManager.centerPopUp(pop);
		}
		private function popupComplete(evt : GestionProfilEvent):void
		{
			//dispatchEvent(new GestionProfilEvent(GestionProfilEvent.PROFIL_USER_CHANGE,true));
			profilService.listeProfil(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		
	}
}