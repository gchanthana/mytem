package ihm.evenements
{
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	import utils.evenements.AbstractEvenement;
	import utils.evenements.IEvenements;
	
	public class Evenement25Impl extends AbstractEvenement implements IEvenements
	{
		public var periodEtude:Label;
		
		private var idPeriodeEtude:int;
		
		[Bindable]
		public var arrayPeriodeFin:Array=[{label: "M-1", value: 1}];
		
		[Bindable]
		public var plagePeriodEtude:ComboBox;
		
		public function Evenement25Impl()
		{
		}
		
		protected function evenement25CreationCompleteHandler(event:FlexEvent):void
		{
			plagePeriodEtude.dataProvider=arrayPeriodeEtude;
			plagePeriodEtude.addEventListener(ListEvent.CHANGE, plagePeriodEtude_changeHandler);
		}
		
		/**
		 * mettre à jour idPeriodeEtude
		 * @param evt
		 */
		private function plagePeriodEtude_changeHandler(evt:ListEvent):void
		{
			idPeriodeEtude=plagePeriodEtude.selectedIndex + 1; /** +1 selected index commence par 0 */
		}
		
		/**
		 * set les valeurs de chaque composant de l'IHM
		 */
		override public function setValues(value:Object):void
		{
			if (value.isPeriodTwoCursor)
			{
				plagePeriodEtude.visible=true;
				periodEtude.visible=false;
			}
			else
			{
				plagePeriodEtude.visible=false;
				periodEtude.visible=true;
			}
			
			periodEtude.text=value.text;
			idPeriodeEtude=value.id;
		}
		
		/**
		 *  récupérer les valeurs de chaque composant de l'IHM
		 */
		override public function getValues():Object
		{
			var allValues:Object=new Object();
			allValues.jourLancement=""; // pas jour de lancement
			allValues.idPeriodFin=1; //periodeFin.selectedItem.value
			allValues.idPeriodeEtude=idPeriodeEtude; // initialisé par setValues
			return allValues;
		}
	}
}