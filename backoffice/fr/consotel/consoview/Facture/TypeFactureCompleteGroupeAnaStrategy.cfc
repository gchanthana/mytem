<!--- =========================================================================
Classe: TypeFactureCompleteGroupeAnaStrategy
Auteur: 
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="TypeFactureCompleteGroupeAnaStrategy" hint=""  extends="TypeFactureStrategy" >
   <cffunction name="getData" access="public" returntype="Query" output="false" >
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric" default="" displayname="numeric IDINVENTAIRE_PERIODE" hint="Initial value for the IDINVENTAIRE_PERIODEproperty." />
		<cfargument name="fake" required="false" type="numeric" default="" displayname="numeric ID"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">
		
		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_ANA_V3.fa_detailfacture_byid">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam  value="#IDPerimetre#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#IDINVENTAIRE_PERIODE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qGetFactureAbo"/>        
		</cfstoredproc>
		
		<cfreturn qGetFactureAbo>
	</cffunction>

	<cffunction name="getDataCat" access="public" returntype="Query" output="false">
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">
        <cfstoredproc datasource="#session.OffreDSN#" procedure="Pkg_cv_ana_V3.fa_summaryfacture">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam  value="#IDPerimetre#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#IDINVENTAIRE_PERIODE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qGetFactureCat">
        </cfstoredproc>
		<cfreturn qGetFactureCat>
	</cffunction>
	
	<cffunction name="getFactureTva" access="public" returntype="query" output="true">
		<cfargument name="IDINVENTAIRE_PERIODE" required="true" type="numeric"/>
		<cfargument name="IDPerimetre" required="true" type="numeric">
        <cfstoredproc datasource="#session.OffreDSN#" procedure="Pkg_cv_ana_V3.fa_summaryfacture_tva">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="in" variable="p_racine_master" value="#SESSION.PERIMETRE.IDRACINE_MASTER#"/>
	        <cfprocparam  value="#IDPerimetre#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocparam  value="#IDINVENTAIRE_PERIODE#" cfsqltype="CF_SQL_INTEGER">
	        <cfprocresult name="qgetFactureTva">
        </cfstoredproc>
		<cfreturn qgetFactureTva>
	</cffunction>
	
	<cffunction name="displayType" access="public" returntype="string" output="false" >
		<cfreturn "Facture Complète">
	</cffunction>
</cfcomponent>