package univers.parametres.parametrelogin.ihm
{
	import mx.containers.VBox;
	import mx.controls.ComboBox;
	
	import univers.parametres.parametrecompte.event.ValidationFacturationEvent;
	import univers.parametres.parametrecompte.service.GestionValidationFacturationService;
	import univers.parametres.parametrecompte.vo.ProfilValidationFacturation;

	public class ListeAffectationProfilValidationFacturationImpl extends VBox
	{
		[Bindable] public var vfservice:GestionValidationFacturationService
		[Bindable] public var idLogin:int
		
		public var cbo:ComboBox
		
		public function ListeAffectationProfilValidationFacturationImpl()
		{
			super();
		}
		public function init():void
		{
			initData()
		} 
		public function initData():void
		{
			if(vfservice == null)
				vfservice= new GestionValidationFacturationService()
			vfservice.getListeProfilValidationFacturation()
			vfservice.addEventListener(ValidationFacturationEvent.PROFIL_SELECTED_EVENT,AfficheProfilSelected)
			vfservice.getProfilValFact(idLogin)
		}
	// on affiche le bon profil dans la combobox
		private function AfficheProfilSelected(e:ValidationFacturationEvent):void
		{
			var idprofil:int = vfservice.idProfilSelected
			cbo.selectedIndex = -1
			if(idprofil > 0)
			{
				for each(var vf:ProfilValidationFacturation in cbo.dataProvider)
				{
					if(vf.idProfil == idprofil)
					{
						cbo.selectedItem = vf
						vfservice.getListeActionProfilValidationFacturation(idprofil);
						break;
					}
				}
			}
		}
	// Quand on change d'élément dans la combobox
		public function cboChangeHandler():void
		{
			var idx:int = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX
			vfservice.AffecterProfilValidationFacturationToClient(idLogin,cbo.selectedItem.idProfil,idx)
			vfservice.getListeActionProfilValidationFacturation(cbo.selectedItem.idProfil);
		}	
	}
}