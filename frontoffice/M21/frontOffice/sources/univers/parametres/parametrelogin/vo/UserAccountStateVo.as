package univers.parametres.parametrelogin.vo
{
	[Bindable]
	public class UserAccountStateVo
	{
		public static const UNLOKED:Number = 1;
		public static const LOKED:Number = 2;
		public static const EXPIRED:Number = 0;
		
		private var	_APP_LOGINID			: Number = 0;
		private var	_FAILED_CONNECT			: Number = 0;
		private var	_LOGIN_STATUS			: Number = 0;
		
		public function UserAccountStateVo(idaccount:Number, failedConnectionCount:Number,idstatus:Number)
		{
			_APP_LOGINID = idaccount;
			_FAILED_CONNECT =  failedConnectionCount;
			_LOGIN_STATUS = idstatus;
		}
		
		public function get FAILED_CONNECT():Number
		{
			return _FAILED_CONNECT;
		}
		
		public function get LOGIN_STATUS():Number
		{
			return _LOGIN_STATUS;
		}
		
		public function get APP_LOGINID():Number
		{
			return _APP_LOGINID;
		}

		public function set APP_LOGINID(value:Number):void
		{
			_APP_LOGINID = value;
		}

		public function set FAILED_CONNECT(value:Number):void
		{
			_FAILED_CONNECT = value;
		}

		public function set LOGIN_STATUS(value:Number):void
		{
			_LOGIN_STATUS = value;
		}


	}
}