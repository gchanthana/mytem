<cfcomponent output="false">
	<cffunction name="getInfosOrganisation" access="remote" output="false" returntype="Query">
		 <cfargument name="idOrganisation" required="true" type="numeric"/>
		 <cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_global.infos_organisation">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idOrganisation#" null="false">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result/>
	</cffunction>
</cfcomponent>