package intranet.cataloguepublic.vo
{
	[RemoteClass(alias="fr.consotel.consoprod.intranet.cataloguepublic.Societe")]
	[Bindable]
	public class Societe
	{
		public var groupeId:Number = 0;
		public var id:Number = 0;
		public var raisonSociale:String = "";
		
		public function Societe(){
			
		}
	}
}