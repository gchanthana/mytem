<cfcomponent name="FaxDeliveryOption" displayname="FaxDeliveryOption">
	<cfset variables.FaxDeliveryOption=structNew()>
	
	<cffunction name="setFaxDeliveryOption" access="package" returntype="void">
		<cfargument name="faxNumber" type="String" required="false">
		<cfargument name="faxServer" type="String" required="false">

		<cfset variables.FaxDeliveryOption.faxNumber=faxNumber>
		<cfset variables.FaxDeliveryOption.faxServer=faxServer>
	</cffunction>
	
	<cffunction name="getFaxDeliveryOption" access="package" returntype="Struct">
		<cfreturn variables.FaxDeliveryOption>
	</cffunction>
</cfcomponent>