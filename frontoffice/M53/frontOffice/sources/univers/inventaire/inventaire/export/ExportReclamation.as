package univers.inventaire.inventaire.export
{
	import mx.rpc.events.ResultEvent;
	import flash.net.URLVariables;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	public class ExportReclamation implements IExportable
	{
		private static const URL_EXPORT : String = "/fr/consotel/consoview/cfm/cycledevie/reclamation/Display_Reclamation.cfm";
		
		public static const PDF : String = "PDF";
		public static const MAIL : String = "MAIL";
		
		private const TYPE : String = "BordereauDeReclamation";
				
		private var _reclamation : ReclamationVO;
		
		public function ExportReclamation(r : ReclamationVO):void{
			_reclamation = r;
		}
		
		public function exporter(format:String = "PDF"):void
		{
			switch(format){
				case MAIL : {
						envoyerMail();
					break;
				}
				case PDF : {
						afficher(PDF);
					break;
				}
				default : {
					throw new Error("Format Inconnu");
				}
			}	
		}
		
		
		//Affiche la commande sous forme de PDF
		private function afficher(format : String):void{
			if (_reclamation){
					var url:String = cv.NonSecureUrlBackoffice + URL_EXPORT;
		            var variables:URLVariables = new URLVariables();
		          	
		          	variables.TYPE = TYPE;  
		            variables.FORMAT = format;	             
		            variables.ID_CONTACT = _reclamation.ID_CONTACT;
					variables.ID_SOCIETE = _reclamation.ID_SOCIETE;
					variables.IDINV_OPERATIONS = _reclamation.IDINV_OPERATIONS;
					variables.IDINV_TYPEOP = _reclamation.IDINV_TYPEOP;
					variables.LIBELLE_OPERATIONS = _reclamation.LIBELLE_OPERATIONS;		
					variables.DATE_REF = _reclamation.DATE_REF;
					var request:URLRequest = new URLRequest(url);
		            request.data = variables;
		            request.method=URLRequestMethod.POST;
		            
		            navigateToURL(request,"_blank");
				}else{
					Alert.show("la demande n'a pas de produit");
				}
		}
		
		
		//Envoi un mail au contact avec la commande en pièce jointe
		private function  envoyerMail():void{			
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																		"fr.consotel.consoview.parcview.inventaire.commande.CommandeGateWay",
																		"envoyer",envoyerResultHandler);
			RemoteObjectUtil.callService(op,_reclamation);
		}
		
		private function envoyerResultHandler(re : ResultEvent):void{
			if (re.result){
				Alert.show("Mail envoyé");
			}else{
				Alert.show("Erreur lors de l'envoi","Mail Error");
			}
		}
	
		
	}
}