package composants.tb.ligne.services
{
	import composants.tb.ligne.entity.CriteresRechercheVO;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	public class FicheTicketService
	{
		
		private var _model:FicheTicketModel;
		public var handler:FicheTicketHandler;
		
		public function FicheTicketService()
		{
			_model = new FicheTicketModel();
			handler = new FicheTicketHandler(_model);
		}

		public function get model():FicheTicketModel
		{
			return _model;
		}

		public function set model(value:FicheTicketModel):void
		{
			_model = value;
		}
		
		public function getFicheTicket(idSous_tete:Number,idinventaire_periode:Number,params:CriteresRechercheVO):void
		{	
			var op:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M311.ficheligne.FicheLigne",
				"getTicket",
				handler.getFicheTicketResultHandler);
			
			RemoteObjectUtil.callService(op,idSous_tete,idinventaire_periode,params.orderColonne,params.orderBy,params.searchColonne,params.searchText,params.offset,params.limit);		
		}

	}
}