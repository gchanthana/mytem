//***************************************************	
//	author : samuel.divioka							*
//	date   : Apr 20, 2011							*
//	owner  : Consotel								*
//***************************************************
package univers.facturation.suivi.controles.services
{
	import mx.rpc.events.ResultEvent;

	public class RessourceHandler
	{
		private var _model:RessourceModel;
		
		public function RessourceHandler(model:RessourceModel)
		{
			_model = model;
		}
		
		
		internal function updateStatusResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateStatus(Number(event.result));
			}
		}
		
		internal function updateInventoryStateResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.updateInventoryState(Number(event.result));
			}
		}
		
		internal function mergeProductResultHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				_model.mergeProduct(Number(event.result));
			}
		}
		
	}
}