package gestionparcmobile.resiliationLigne.messages
{
	import mx.resources.ResourceManager;

	public class GestionParcMobileMessages
	{

		//--------------------------------------------------------------------------------------------//
		//					GENERAL
		//--------------------------------------------------------------------------------------------//

		public static const CONSOVIEW							: String = ResourceManager.getInstance().getString('M11', 'Consoview') ;
		public static const ERREUR								: String = ResourceManager.getInstance().getString('M11', 'Erreur') ;
		public static const RENSEIGNER_CHAMPS_OBLI				: String = ResourceManager.getInstance().getString('M11', 'Veuillez_renseigner_les_champs_obligatoi') ;

		//--------------------------------------------------------------------------------------------//
		//					LIGNE
		//--------------------------------------------------------------------------------------------//

		public static const NB_CHAR_POUR_LIGNE					: String = ResourceManager.getInstance().getString('M11', 'Le_n__de_ligne_doit_contenir_10_chiffres') ;

		//--------------------------------------------------------------------------------------------//
		//					EQUIPEMENT
		//--------------------------------------------------------------------------------------------//

		public static const RECUP_PROFILS_EQPT_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_des_profils_des__quipeme') ;
		public static const RECUP_ABO_OPT_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_des_abonnements_et_des_o') ;

		//--------------------------------------------------------------------------------------------//
		//					FICHES
		//--------------------------------------------------------------------------------------------//
		
		// fiche historique
		public static const RECUP_FICHE_HISTO_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_fiche_histo_a__chou___') ;
		
		// fiche collab
		public static const RECUP_FICHE_COLLAB_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_fiche_collab_impossible') ;
		public static const UPDATE_FICHE_COLLAB_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_mise___jour_de_la_fiche_collab_impossible') ;
		public static const ADD_NEW_COLLAB_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'L_ajout_du_nouveau_collaborateur_a__chou___') ;
		
		// fiche terminal
		public static const RECUP_FICHE_TERM_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_fiche_terminal_a__chou__') ;
		public static const UPDATE_FICHE_TERM_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_mise___jour_de_la_fiche_terminal_a__chou___') ;
		public static const NUMERO_IMEI_EXISTANT				: String = ResourceManager.getInstance().getString('M11', 'Le_num_ro_IMEI_que_vous_avez_choisi_est_d_j__utilis__') ; 
		// commande
		public static const SET_INFOS_CMD_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'L_enregistrement_des_informations_concernant_la_commande_a__chou___') ;
		public static const GENERATION_NUM_CMD_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_g_n_ration_du_num_ro_de_commande_a__chou___') ;
		// modèle
		public static const RECUP_CONSTRU_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_constructeurs_a__chou__') ;
		public static const RECUP_MODELES_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_mod_les_a__chou__') ;
		// onglet contrat
		public static const SAVE_INFOS_CONTRAT_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'L_enregistrement_des_informations_concernant_le_contrat_a__chou___') ;
		public static const DELETE_CONTRAT_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_suppression_du_contrat_a__chou___') ;
		// onglet incident
		public static const SAVE_INFOS_INCIDENT_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'L_enregistrement_des_informations_concernant_l_incident_a__chou__') ;
		public static const DELETE_INCIDENT_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_suppression_de_l_incident_a__chou__') ;
		public static const SAVE_INFOS_INTERVENTION_IMPOSSIBLE	: String = ResourceManager.getInstance().getString('M11', 'L_enregistrement_des_informations_concernant_l_intervention_a__chou__') ;
		public static const DELETE_INTERVENTION_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_suppression_de_l_intervention_a__chou__') ;
		public static const RECUP_LISTE_INTER_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_types_d_interventions_a__chou__') ;
		// fiche sim
		public static const RECUP_FICHE_SIM_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_fiche_sim_a__chou__') ; 
		public static const UPDATE_FICHE_SIM_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_mise___jour_de_la_fiche_sim_a__chou___') ; 
		public static const NUMERO_SIM_EXISTANT					: String = ResourceManager.getInstance().getString('M11', 'Le_num_ro_de_sim_que_vous_avez_choisi_est_d_j__utilis__') ; 
		
		public static const GET_ORGA_IMPOSSIBLE					: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_organisations_a__chou__') ;

		
		//--------------------------------------------------------------------------------------------//
		//					CACHE
		//--------------------------------------------------------------------------------------------//
		
		public static const RECUP_LISTE_TYPE_CONTRAT_IMPOSSIBLE	: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_contrats_a__chou__') ;
		public static const RECUP_LISTE_TYPE_INTER_IMPOSSIBLE	: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_types_d_interventions_a__chou__') ;
		public static const RECUP_LISTE_REVENDEUR_IMPOSSIBLE	: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_revendeurs_a__chou__') ;
		public static const RECUP_SITES_IMPOSSIBLE				: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_sites_a__chou__') ;
		public static const RECUP_LIBELLE_PERSO_IMPOSSIBLE		: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_libell_s_personnalis_s_a__chou__') ;
		public static const RECUP_CIVILITES_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_de_la_liste_des_civilit_s_a__chou__') ;
		
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//

		public static const PB_GESTION_FAVORIS					: String = ResourceManager.getInstance().getString('M11', 'Probl_me_avec_la_gestion_des_favoris__') ;
		public static const RECUP_CPT_SSCPT_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'La_r_cup_ration_des_comptes_et_des_sous_') ;
		public static const RECHERCHE_IMPOSSIBLE				: String = ResourceManager.getInstance().getString('M11', 'Recherche_impossible__') ;
		public static const LOAD_NODE_ORGA_IMPOSSIBLE			: String = ResourceManager.getInstance().getString('M11', 'Le_chargement_des_noeuds_de_l_organisation_a__chou__') ;

	}
}