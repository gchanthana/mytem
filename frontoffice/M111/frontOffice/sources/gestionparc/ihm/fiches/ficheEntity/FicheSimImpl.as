package gestionparc.ihm.fiches.ficheEntity
{
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.EmplacementVO;
	import gestionparc.entity.IncidentVO;
	import gestionparc.entity.OrgaVO;
	import gestionparc.entity.SiteVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentContratOperateurSimIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.ContentSiteFicheEquipementIHM;
	import gestionparc.ihm.fiches.ficheEntity.contentTabNav.TabModelIHM;
	import gestionparc.ihm.fiches.historique.FicheHistoriqueIHM;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.incident.IncidentServices;
	import gestionparc.services.sim.SimServices;
	import gestionparc.services.site.SiteServices;
	import gestionparc.utils.gestionparcMessages;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.containers.TabNavigator;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.validators.Validator;

	/**
	 * Classe concernant toutes les informations de la fiche sim.
	 */
	[Bindable]
	public class FicheSimImpl extends TitleWindowBounds
	{

		public var tiNumSim:TextInput;
		public var tiPin1:TextInput;
		public var tiPin2:TextInput;
		public var tiPuk1:TextInput;
		public var tiPuk2:TextInput;

		public var lbNumLigne:Label;
		public var lbNumContrat:Label;
		public var lbAboPrincipal:Label;
		public var lbTypeLigne:Label;
		public var lbOperateur:Label;
		public var lbTitulaire:Label;
		public var lbfonction:Label;
		public var lbCompte:Label;
		public var lbSousCompte:Label;
		public var lbPayeur:Label;
		public var lbPosition:Label;
		public var lbNomPrenomCollab:Label;
		public var lbMatriculeCollab:Label;

		public var cbOrganisation:ComboBox;
		
		public var cbxByod:CheckBox;

		public var valNumSim:Validator;

		public var tnSim:TabNavigator;

		public var tncAboOption:TabModelIHM;
		public var tncIncident:TabModelIHM;

		public var tncContratOperateur:ContentContratOperateurSimIHM;

		public var tncSite:ContentSiteFicheEquipementIHM;

		public var btValid:Button;
		public var btCancel:Button;

		public var idSim:int=-1;
		public var idSousTete:int=-1;
		public var idGpeClient:int=-1;
		public var ID:int=-1;
		public var matriculeCollab:String="-";
		public var nomPrenomCollab:String="-";
		public var myServicesCache:CacheServices =CacheServices.getInstance();// new CacheServices();

		private var myServiceSim:SimServices=null;
		private var myServicesSite:SiteServices=null;

		public var lblFormItemSimExt : String;
		public var formItemSimExt : FormItem;
		
		/**
		 * Constructeur de la classe FicheSimImpl.
		 */
		public function FicheSimImpl()
		{
			this.addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		/**
		 * Formate les données de la colonne avec le bon affichage des dates au format String.
		 */
		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
			{
				return "";
			}
		}

		/**
		 * Formate les données de la colonne avec le bon affichage du prix selon le pays.
		 */
		protected function formateColonnneEuros(item:Object, column:DataGridColumn):String
		{
			if (item != null)
			{
				return ConsoviewFormatter.formatEuroCurrency(item[column.dataField], 2);
			}
			else
			{
				return "";
			}
		}

		/**
		 * Initialisation générale de cette classe  :
		 * - Instanciation du service lié aux sims.
		 * - Appel des fonctions <code>initListener()</code> et <code>initDisplay()</code>
		 */
		private function init(event:FlexEvent):void
		{
			myServiceSim=new SimServices();
			myServicesSite=new SiteServices();

			// pour passer -1 à la base et non 0
			if (idSousTete == 0)
			{
				idSousTete=-1;
			}
			if (idGpeClient == 0)
			{
				idGpeClient=-1;
			}

			var objToServiceGet:Object=new Object();
			objToServiceGet.idSim=idSim;
			objToServiceGet.idSousTete=idSousTete;
			objToServiceGet.idGpeClient=idGpeClient;
			objToServiceGet.ID=ID;
			myServiceSim.getInfosFicheSim(objToServiceGet);

			formItemSimExt.label=lblFormItemSimExt;
			
			initListener();
			initDisplay();
		}

		/**
		 * Initialise les écouteurs.
		 */
		private function initListener():void
		{
			// listeners lié aux boutons de l'IHM
			this.addEventListener(CloseEvent.CLOSE, closePopup);

			btCancel.addEventListener(MouseEvent.CLICK, cancelPopup);
			btValid.addEventListener(MouseEvent.CLICK, validClickHandler);

			// listeners permettant l'activation du bouton "valider"
			tiNumSim.addEventListener(Event.CHANGE, activeValidButton);
			tiPin1.addEventListener(Event.CHANGE, activeValidButton);
			tiPin2.addEventListener(Event.CHANGE, activeValidButton);
			tiPuk1.addEventListener(Event.CHANGE, activeValidButton);
			tiPuk2.addEventListener(Event.CHANGE, activeValidButton);
			cbxByod.addEventListener(Event.CHANGE, activeValidButton);
			tncSite.cbSite.addEventListener(ListEvent.CHANGE, activeValidButton);
			tncSite.cbEmplacement.addEventListener(ListEvent.CHANGE, activeValidButton);

			// listeners liés au tabNavigator
			tncIncident.addEventListener(gestionparcEvent.AJOUTER_ELT_NAV, onAjouterIncidentHandler);
			tncIncident.addEventListener(gestionparcEvent.EDITER_ELT_NAV, onEditerIncidentHandler);
			tncIncident.addEventListener(gestionparcEvent.SUPPRIMER_ELT_NAV, incidentDeleteHandler);
			this.addEventListener(gestionparcEvent.AFFICHER_HISTORIQUE, onAfficherHistoriqueHandler);
			tncSite.addEventListener(gestionparcEvent.INFOS_FICHE_SITE_UPLOADED, onInfosFicheSiteUploadedHandler);

			// listeners liés à l'organisation
			cbOrganisation.addEventListener(ListEvent.CHANGE, changeOrgaHandler);

			// listeners liés aux services
			myServiceSim.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_SIM_LOADED, infoFicheSimLoadedHandler);
		}

		/**
		 * Initialise l'affichage de cette IHM.
		 */
		private function initDisplay():void
		{
			// tant que tous les champs requit ne sont pas renseignés ou qu'aucune modif n'a été réalisée
			btValid.enabled=false;

			lbNomPrenomCollab.text=nomPrenomCollab;
			lbMatriculeCollab.text=matriculeCollab;

			tncSite.cbSite.dataProvider=CacheDatas.siteLivraison;
			tncSite.myServicesCache=myServicesCache;
		}

		/**
		 * Charge les données qui viennent d'être collectées de la base dans l'IHM.
		 */
		private function infoFicheSimLoadedHandler(event:gestionparcEvent):void
		{
			//trace("données fiche sim chargées");

			// maj des infos concernant la sim
			tiNumSim.text=myServiceSim.myDatas.sim.numSerieEquipement;
			tiPin1.text=myServiceSim.myDatas.sim.PIN1;
			tiPin2.text=myServiceSim.myDatas.sim.PIN2;
			tiPuk1.text=myServiceSim.myDatas.sim.PUK1;
			tiPuk2.text=myServiceSim.myDatas.sim.PUK2;
			cbxByod.selected = (myServiceSim.myDatas.sim.byod == 1);

			// maj des infos concernant la ligne
			lbNumLigne.text=(myServiceSim.myDatas.sim.ligneRattache.sousTete) ? myServiceSim.myDatas.sim.ligneRattache.sousTete : "-";
			lbNumContrat.text=(myServiceSim.myDatas.sim.ligneRattache.numContrat) ? myServiceSim.myDatas.sim.ligneRattache.numContrat : "-";
			lbAboPrincipal.text=(myServiceSim.myDatas.sim.ligneRattache.aboPrincipale) ? myServiceSim.myDatas.sim.ligneRattache.aboPrincipale : "-";
			lbTypeLigne.text=(myServiceSim.myDatas.sim.ligneRattache.typeLigne) ? myServiceSim.myDatas.sim.ligneRattache.typeLigne : "-";
			lbfonction.text=(myServiceSim.myDatas.sim.ligneRattache.fonction) ? myServiceSim.myDatas.sim.ligneRattache.fonction : "-";
			lbCompte.text=(myServiceSim.myDatas.sim.ligneRattache.compte) ? myServiceSim.myDatas.sim.ligneRattache.compte : "-";
			lbSousCompte.text=(myServiceSim.myDatas.sim.ligneRattache.sousCompte) ? myServiceSim.myDatas.sim.ligneRattache.sousCompte : "-";
			if (lbNumLigne.text == "-")
			{
				lbTitulaire.text="-";
				lbOperateur.text="-";
				lbPayeur.text="-";
			}
			else
			{
				lbOperateur.text=(myServiceSim.myDatas.sim.nomRevendeur) ? myServiceSim.myDatas.sim.nomRevendeur : "-";
				if (myServiceSim.myDatas.sim.ligneRattache.titulaire == true)
				{
					lbTitulaire.text=ResourceManager.getInstance().getString('M111', 'oui');
				}
				else
				{
					lbTitulaire.text=ResourceManager.getInstance().getString('M111', 'non');
				}
				if (myServiceSim.myDatas.sim.ligneRattache.payeur == true)
				{
					lbPayeur.text=ResourceManager.getInstance().getString('M111', 'oui');
				}
				else
				{
					lbPayeur.text=ResourceManager.getInstance().getString('M111', 'non');
				}
			}

			// maj orga
			cbOrganisation.dataProvider=myServiceSim.myDatas.sim.listeOrganisation;
			if (myServiceSim.myDatas.sim.listeOrganisation.length > 0)
			{
				cbOrganisation.selectedIndex=0;
				lbPosition.text=(cbOrganisation.selectedItem as OrgaVO).chemin;
			}

			// maj onglet SITE 
			tncSite.cbSite.dataProvider=CacheDatas.siteLivraison;
			if (myServiceSim.myDatas.sim.idSite > 0)
			{
				var objToGetEmplacement:Object=new Object();
				objToGetEmplacement.idSite=myServiceSim.myDatas.sim.idSite;
				myServicesSite.getEmplacement(objToGetEmplacement);
				myServicesSite.myDatas.addEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedHandler);
				var longueurTabSite:int=CacheDatas.siteLivraison.length;
				for (var k:int=0; k < longueurTabSite; k++)
				{
					if ((CacheDatas.siteLivraison[k] as SiteVO).idSite == myServiceSim.myDatas.sim.idSite)
					{
						//tncSite.cbSite.selectedItemId = myServiceSim.myDatas.sim.idSite;
						tncSite.cbSite.selectedIndex=k;

						tncSite.txtAdresse.text=(CacheDatas.siteLivraison[k] as SiteVO).adresse1;
						tncSite.lbCodePostal.text=(CacheDatas.siteLivraison[k] as SiteVO).codePostale;
						tncSite.lbVille.text=(CacheDatas.siteLivraison[k] as SiteVO).ville;
						tncSite.lbPays.text=(CacheDatas.siteLivraison[k] as SiteVO).pays;
						break;
					}
				}
			}
			else
			{
				tncSite.cbSite.selectedIndex=-1;
					//tncSite.cbSite.selectedItemId = -1;
			}

			// maj onglet Abo et options
			tncAboOption.dataDatagrid=myServiceSim.myDatas.sim.listeAboOption;

			// maj onglet contrat opérateur
			tncContratOperateur.lbOperateur.text=myServiceSim.myDatas.sim.nomRevendeur;
			tncContratOperateur.lbDureeContrat.text=myServiceSim.myDatas.sim.dureeContrat.toString() + ResourceManager.getInstance().getString('M111', '_mois');
			tncContratOperateur.lbDateOuverture.text=DateFunction.formatDateAsString(myServiceSim.myDatas.sim.dateOuverture);
			tncContratOperateur.lbDateRenouvellement.text=DateFunction.formatDateAsString(myServiceSim.myDatas.sim.dateRenouvellement);
			tncContratOperateur.lbDateResiliation.text=DateFunction.formatDateAsString(myServiceSim.myDatas.sim.dateResiliation);
			tncContratOperateur.lbDureeEligibilite.text=myServiceSim.myDatas.sim.dureeEligibilite.toString() + ResourceManager.getInstance().getString('M111', '_mois');
			tncContratOperateur.lbDateEligibilite.text=DateFunction.formatDateAsString(myServiceSim.myDatas.sim.dateEligibilite);
			tncContratOperateur.lbDateFpc.text=DateFunction.formatDateAsString(myServiceSim.myDatas.sim.dateFPC);
			tncContratOperateur.lbDateLastFacture.text=DateFunction.formatDateAsString(myServiceSim.myDatas.sim.dateDerniereFacture);

			// maj onglet INCIDENT
			tncIncident.dataDatagrid=myServiceSim.myDatas.sim.listeIncident;
			activeButtonNewIncident();
		}

		/**
		 * Active le bouton "Valider".
		 */
		private function activeValidButton(event:Event):void
		{
			btValid.enabled=true;
		}

		/**
		 * Active le bouton "nouvel incident" ou non
		 */
		private function activeButtonNewIncident():void
		{
			var lenListeIncident:int=myServiceSim.myDatas.sim.listeIncident.length;
			var isActive:Boolean=true;

			for (var i:int=0; i < lenListeIncident; i++)
			{
				if ((myServiceSim.myDatas.sim.listeIncident[i] as IncidentVO).isClose == 0)
				{
					isActive=false;
					break;
				}
			}

			if (isActive)
			{
				tncIncident.buttonAction.enabled=true;
			}
			else
			{
				tncIncident.buttonAction.enabled=false;
			}
		}

		/**
		 * Sélectionne le "chemin cible" s'il existe.
		 */
		private function changeOrgaHandler(event:ListEvent):void
		{
			if ((cbOrganisation.selectedItem as OrgaVO).chemin)
			{
				lbPosition.text=(cbOrganisation.selectedItem as OrgaVO).chemin;
			}
			else
			{
				lbPosition.text="";
			}
			activeValidButton(null);
		}

		/**
		 * Met à jour les données de la sim.
		 */
		private function setInfoSimVO():void
		{
			if (myServiceSim.myDatas.sim.numSerieEquipement == tiNumSim.text) // numero sim inchangé
			{
				myServiceSim.myDatas.sim.numSerieEquipement = "-1";
			}
			else
			{
				myServiceSim.myDatas.sim.numSerieEquipement=tiNumSim.text;
			}
			myServiceSim.myDatas.sim.PIN1 = tiPin1.text;
			myServiceSim.myDatas.sim.PIN2 = tiPin2.text;
			myServiceSim.myDatas.sim.PUK1 = tiPuk1.text;
			myServiceSim.myDatas.sim.PUK2 = tiPuk2.text;
			myServiceSim.myDatas.sim.byod = (cbxByod.selected) ? 1 : 0;

			if ((tncSite.cbSite.selectedItem != null))
			{
				myServiceSim.myDatas.sim.idSite = (tncSite.cbSite.selectedItem as SiteVO).idSite;
			}
			else
			{
				myServiceSim.myDatas.sim.idSite = -1; // pas de site
			}

			if (tncSite.cbEmplacement.selectedItem != null)
			{
				myServiceSim.myDatas.sim.idEmplacement = (tncSite.cbEmplacement.selectedItem as EmplacementVO).idEmplcament;
			}
			else
			{
				myServiceSim.myDatas.sim.idEmplacement = -1; // pas d'emplacement
			}
		}

		/**
		 * Lors du clique sur le bouton "Annuler", cette fonction appelle <code>closePopup</code> pour femrer la popup courante.
		 */
		private function cancelPopup(event:MouseEvent):void
		{
			closePopup(null);
		}

		/**
		 * Ferme la popup courante.
		 */
		private function closePopup(event:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/**
		 * Valide la fiche sim.
		 */
		private function validClickHandler(event:MouseEvent):void
		{
			// test les validators
			if (runValidators())
			{
				myServiceSim.setInfosFicheSim(createObjToServiceSet());
				myServiceSim.myDatas.addEventListener(gestionparcEvent.INFOS_FICHE_SIM_UPLOADED, ficheSimUploadHandler);
			}
		}

		/**
		 * Créé l'objet non typé que l'on va passer au service
		 */
		private function createObjToServiceSet():Object
		{
			var obj:Object=new Object();
			var hasSite:int=0;

			hasSite=(myServiceSim.myDatas.sim.idSite != -1) ? 1 : 0;
			obj.hasSite=hasSite;
			setInfoSimVO();
			obj.mySim=myServiceSim.myDatas.sim;

			return obj;
		}

		/**
		 * Dispatch un évenement pour dire que les informations concernant la sim ont été mise à jour.
		 */
		private function ficheSimUploadHandler(event:gestionparcEvent):void
		{
			SpecificationVO.getInstance().refreshData();
			closePopup(null);
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'Modifications_enregistr_es'), Application.application as DisplayObject);
			this.dispatchEvent(new gestionparcEvent(gestionparcEvent.INFOS_FICHE_SIM_UPLOADED));
		}

		/**
		 * Affiche la pop up de l'historique du produit selectionné
		 */
		private function onAfficherHistoriqueHandler(evt:gestionparcEvent):void
		{
			var ficheHistorique:FicheHistoriqueIHM=new FicheHistoriqueIHM();

			ficheHistorique.idRessource=evt.obj.idInventaireProduit;

			PopUpManager.addPopUp(ficheHistorique, parent, true);
			PopUpManager.centerPopUp(ficheHistorique);
		}

		/**
		 * Affiche la popup permettant d'ajouter un incident.
		 */
		private function onAjouterIncidentHandler(evt:gestionparcEvent):void
		{
			if (tncIncident.buttonAction.enabled)
			{
				var ficheAddIncident:FicheIncidentIHM=new FicheIncidentIHM();

				ficheAddIncident.isAddIncident=true;
				ficheAddIncident.isSIM=true;
				ficheAddIncident.idEqpt=myServiceSim.myDatas.sim.idEquipement;
				ficheAddIncident.libelleEqpt=myServiceSim.myDatas.sim.numSerieEquipement;
				ficheAddIncident.listIncidentEqpt=myServiceSim.myDatas.sim.listeIncident;
				ficheAddIncident.revendeurEqpt=myServiceSim.myDatas.sim.revendeur.nom;
				ficheAddIncident.idRevendeurEqpt=myServiceSim.myDatas.sim.revendeur.idRevendeur;
				ficheAddIncident.myServiceCache=myServicesCache;
				ficheAddIncident.myIncident=new IncidentVO();

				ficheAddIncident.addEventListener(gestionparcEvent.UPDATE_LISTE_INCIDENT, updateAddListeIncidentHandler);

				PopUpManager.addPopUp(ficheAddIncident, this, true);
				PopUpManager.centerPopUp(ficheAddIncident);
			}
		}

		/**
		 * Affiche la popup permettant d'éditer un incident.
		 */
		private function onEditerIncidentHandler(evt:gestionparcEvent):void
		{
			var ficheEditIncident:FicheIncidentIHM=new FicheIncidentIHM();

			ficheEditIncident.isAddIncident=false;
			ficheEditIncident.isSIM=true;
			ficheEditIncident.idEqpt=myServiceSim.myDatas.sim.idEquipement;
			ficheEditIncident.libelleEqpt=myServiceSim.myDatas.sim.numSerieEquipement;
			ficheEditIncident.listIncidentEqpt=myServiceSim.myDatas.sim.listeIncident;
			ficheEditIncident.revendeurEqpt=myServiceSim.myDatas.sim.revendeur.nom;
			ficheEditIncident.idRevendeurEqpt=myServiceSim.myDatas.sim.revendeur.idRevendeur;
			ficheEditIncident.myServiceCache=myServicesCache;
			ficheEditIncident.myIncident=new IncidentVO();

			// seléction du bon incident parmis la liste des incidents de cet equipement
			var lenListeIncident:int=myServiceSim.myDatas.sim.listeIncident.length;
			for (var j:int=0; j < lenListeIncident; j++)
			{
				if ((myServiceSim.myDatas.sim.listeIncident[j] as IncidentVO).idIncident == (tncIncident.ligneDgSelected as IncidentVO).idIncident)
				{
					ficheEditIncident.myIncident=(myServiceSim.myDatas.sim.listeIncident[j] as IncidentVO);
					break;
				}
			}

			ficheEditIncident.addEventListener(gestionparcEvent.UPDATE_LISTE_INCIDENT, updateModifListeIncidentHandler);

			PopUpManager.addPopUp(ficheEditIncident, parent, true);
			PopUpManager.centerPopUp(ficheEditIncident);
		}

		/**
		 * Applique l'ajout de l'incident.
		 * @return void
		 */
		private function updateAddListeIncidentHandler(evt:gestionparcEvent):void
		{
			myServiceSim.myDatas.sim.listeIncident.addItem((evt.obj as IncidentVO));
			activeValidButton(null);
			activeButtonNewIncident();
		}

		/**
		 * Applique les modifications de l'incident sélectionné.
		 */
		private function updateModifListeIncidentHandler(evt:gestionparcEvent):void
		{
			// seléction du bon incident parmis la liste des incidents de cet equipement et suppression
			var lenListeIncident:int=myServiceSim.myDatas.sim.listeIncident.length;
			for (var j:int=0; j < lenListeIncident; j++)
			{
				if ((myServiceSim.myDatas.sim.listeIncident[j] as IncidentVO).idIncident == (evt.obj as IncidentVO).idIncident)
				{
					myServiceSim.myDatas.sim.listeIncident.removeItemAt(j);
					break;
				}
			}
			// ajout du nouvel incident (avec les mise a jour)
			myServiceSim.myDatas.sim.listeIncident.addItem((evt.obj as IncidentVO));
			activeValidButton(null);
			activeButtonNewIncident();
		}

		/**
		 * Applique la suppression de l'incident sélectionné en base et en front.
		 */
		private function incidentDeleteHandler(evt:gestionparcEvent):void
		{
			var lenListeIncident:int=myServiceSim.myDatas.sim.listeIncident.length;
			var myServiceIncident:IncidentServices=new IncidentServices();

			for (var i:int=0; i < lenListeIncident; i++)
			{
				if ((myServiceSim.myDatas.sim.listeIncident[i] as IncidentVO).idIncident == (evt.obj as IncidentVO).idIncident)
				{
					myServiceIncident.deleteIncident(idSim, 0, (myServiceSim.myDatas.sim.listeIncident[i] as IncidentVO).idIncident);
					myServiceSim.myDatas.sim.listeIncident.removeItemAt(i);
					break;
				}
			}
			activeValidButton(null);
			activeButtonNewIncident();
		}

		/**
		 * Appellée après la mise a jour des infos d'un site depuis sa fiche,
		 * pour mettre appeler le service cache permettant de recupere la nouvelle liste des sites.
		 */
		private function onInfosFicheSiteUploadedHandler(evt:gestionparcEvent):void
		{
			myServicesCache.getSiteLivraison();
			myServicesCache.myDatas.addEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
		}

		/**
		 * Appellée après la mise a jour des sites de livraison dans le cache,
		 * pour mettre a jour le data provider et les infos affichées.
		 */
		private function onListeSiteLoadedhandler(evt:gestionparcEvent):void
		{
			myServicesCache.myDatas.removeEventListener(gestionparcEvent.LISTE_SITE_LOADED, onListeSiteLoadedhandler);
			tncSite.cbSite.dataProvider=CacheDatas.siteLivraison;
			if (myServiceSim.myDatas.sim.idSite > 0)
			{
				var longueurTabSite:int=CacheDatas.siteLivraison.length;
				for (var k:int=0; k < longueurTabSite; k++)
				{
					if ((CacheDatas.siteLivraison[k] as SiteVO).idSite == myServiceSim.myDatas.sim.idSite)
					{
						//tncSite.cbSite.selectedItemId = myServiceSim.myDatas.sim.idSite;
						tncSite.cbSite.selectedItem=myServiceSim.myDatas.sim.idSite;
						tncSite.txtAdresse.text=(CacheDatas.siteLivraison[k] as SiteVO).adresse1;
						tncSite.lbCodePostal.text=(CacheDatas.siteLivraison[k] as SiteVO).codePostale;
						tncSite.lbVille.text=(CacheDatas.siteLivraison[k] as SiteVO).ville;
						tncSite.lbPays.text=(CacheDatas.siteLivraison[k] as SiteVO).pays;
						break;
					}
				}
			}
			else
			{
				//tncSite.cbSite.selectedItemId = -1;
				tncSite.cbSite.selectedItem=-1;
			}
		}

		/**
		 * Appellée après la mise a jour des sites de livraison dans le cache,
		 * pour mettre a jour le data provider et les infos affichées.
		 */
		private function onInfosEmplaSiteLoadedHandler(evt:gestionparcEvent):void
		{
			myServicesSite.myDatas.removeEventListener(gestionparcEvent.INFOS_EMPLA_SITE_LOADED, onInfosEmplaSiteLoadedHandler);
			tncSite.cbEmplacement.dataProvider=new ArrayCollection();
			tncSite.cbEmplacement.dataProvider=myServicesSite.myDatas.listEmplacement;

			if (myServiceSim.myDatas.sim.idEmplacement > 0)
			{
				var longueurTabEmplacement:int=myServicesSite.myDatas.listEmplacement.length;
				for (var k:int=0; k < longueurTabEmplacement; k++)
				{
					if ((myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).idEmplcament == myServiceSim.myDatas.sim.idEmplacement)
					{
						tncSite.cbEmplacement.selectedIndex=k;
						tncSite.txtBatiment.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).batiment;
						tncSite.txtEtage.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).etage;
						tncSite.txtSalle.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).salle;
						tncSite.txtCouloir.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).couloir;
						tncSite.txtArmoire.text=(myServicesSite.myDatas.listEmplacement[k] as EmplacementVO).armoire;
						break;
					}
				}
			}
			else
			{
				tncSite.cbEmplacement.selectedIndex=-1;
			}
		}

		/**
		 * Teste les validateurs et permet ainsi la cohérence des valeurs attendues.
		 */
		private function runValidators():Boolean
		{
			var validators:Array=[valNumSim];
			var results:Array=Validator.validateAll(validators);

			if (results.length > 0)
			{
				ConsoviewAlert.afficherAlertInfo(gestionparcMessages.RENSEIGNER_CHAMPS_OBLI, ResourceManager.getInstance().getString('M111', 'Attention__'),null);
				return false;
			}
			else
			{
				return true;
			}
		}
	}
}
