package appli.Pilotage.service
{
	import appli.Pilotage.VO.DatePFGPVo;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import composants.RemoteObjectUtil.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	public class AppPFGPService extends EventDispatcher
	{
		
		private var _dateInfos:Object;
		
		public function AppPFGPService()
		{
		}
		
		public function getInfosDates():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																					"fr.consotel.consoview.M00.E0DateService",
																					"getPFGPInfosDates",
																					getInfosDatesHandler);				
			op.makeObjectsBindable = true;
			RemoteObjectUtil.callService(op);
		}
		private function getInfosDatesHandler(event:ResultEvent):void
		{
			if(event.result)
			{
				dateInfos = event.result;
				dispatchEvent(new Event("DATE_LOADED"));
			}
		}
		[Bindable]
		public function set dateInfos(value:Object):void
		{
			_dateInfos = value;
		}

		public function get dateInfos():Object
		{
			return _dateInfos;
		}
	}
}