package intranet.gestionLoginClasse.event
{
	import flash.events.Event;
	
	public class UserAccountStateEvent extends Event
	{
		public static const CHANGE_STATE_SUCCED:String = "changeStateSucced";
		public static const CHANGE_STATE_FAILED:String = "changeStateFailed";
		
		public function UserAccountStateEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		override public function clone():Event
		{
			return new UserAccountStateEvent(type, bubbles, cancelable);
		}
		override public function toString():String
		{
			return formatToString("UserAccountStateEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}