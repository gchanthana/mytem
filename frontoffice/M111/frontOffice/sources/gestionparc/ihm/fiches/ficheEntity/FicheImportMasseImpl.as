package gestionparc.ihm.fiches.ficheEntity
{
	import composants.importmasse.ImportMasseIHM;
	import composants.importmasse.ImportMassePopUpHelp;
	import composants.ui.TitleWindowBounds;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewUtil;
	import composants.util.CvDateChooser;
	import composants.util.DateFunction;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.ImportDeMasseVO;
	import gestionparc.entity.ModeleVO;
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.TypeLigneVO;
	import gestionparc.event.ImportDeMasseEvent;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.importmasse.ImportConfirmationIHM;
	import gestionparc.ihm.fiches.importmasse.alimentaionpool.AlimentationPoolsIHM;
	import gestionparc.ihm.fiches.popup.PopupChooseModeleIHM;
	import gestionparc.ihm.fiches.popup.PopupChooseTypeLigneIHM;
	import gestionparc.services.cache.CacheDatas;
	import gestionparc.services.cache.CacheServices;
	import gestionparc.services.importmasse.ImportMasseServices;
	
	import mx.collections.ArrayCollection;
	import mx.containers.FormItem;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;

	[Bindable]
	public class FicheImportMasseImpl extends TitleWindowBounds
	{
		
		//--------------- VARIABLES ----------------//

		public static var arrayDataEquipement:ArrayCollection = new ArrayCollection();

		public static var _datagrid_import_mobile_:DataGrid = new DataGrid();
		public static var bool_check_term:Boolean = true;
		public static var bool_check_sim:Boolean = true;
		public static var bool_check_ligne:Boolean = true;
		public static var bool_check_collab:Boolean = true;
		public static var bool_check_extension:Boolean = true;
		
		public var imgHelp:Image;
				
		private var bool_check_garantie:Boolean = true;
		private var _bool_new_libelle_modele:Boolean = false;

		// ComboBox
		public var comboRevendeur:ComboBox;
		public var comboOperateur:ComboBox;
		public var comboModele:ComboBox;
		public var comboDuree:ComboBox;
		public var comboEngagement:ComboBox;
		public var comboRevendeurSIM:ComboBox;
		public var comboQte:ComboBox;
		public var comboTypeLigne:ComboBox;
		
		// Buttons
		public var btAnnuler:Button;
		public var btValider:Button;
		public var btnModifierPools:Button;
		public var btImgChooseModele:Image;
		public var btImgChooseTypeLigne:Image;
		
		//Images
		[Embed(source="/assets/images/view.png")]
		public var imgChooseModele:Class;
		[Embed(source="/assets/images/view_gris.png")]
		public var imgChooseModeleGris:Class;
		
		// CheckBox
		public var checkTerminaux:CheckBox;
		public var checkGarantie:CheckBox;
		public var checkCollaborateurs:CheckBox;
		public var checkSimExtension:CheckBox;
		public var checkExtension:RadioButton;
		public var checkSim:RadioButton;
		public var checkLignes:CheckBox;
		public var checkLigneMobile:RadioButton;
		public var checkLigneAutre:RadioButton;
		public var checkFPC:CheckBox;
		
		//Label
		public var lblChangePools:Label;

		// FormItem
		public var formItemRevendeur:FormItem;
		public var formItemDebutGarantie:FormItem;
		public var formItemOperateur:FormItem;
		public var formItemDebutLigne:FormItem;
		public var formItemModele:FormItem;
		public var formItemDuree:FormItem;
		public var formItemEngagement:FormItem;
		public var formItemTypeLigne:FormItem; 

		// DataGridColumn
		public var colonne1:DataGridColumn;
		public var colonne2:DataGridColumn;
		public var colonne3:DataGridColumn;
		public var colonne4:DataGridColumn;
		public var colonne5:DataGridColumn;
		public var colonne6:DataGridColumn;

		// CvDateChooser
		public var dcDateDebutGarantie:CvDateChooser;
		public var dcDateDebutLignes:CvDateChooser;

		public var myComposantImportMasse:ImportMasseIHM;
		public var myServices:ImportMasseServices;
		public var myCachesServices:CacheServices;
		
		public var poolDistributeur:Object;
		
		public var isPoolRevendeur:Boolean = true;
		
		public var mydoc:XML;
		
		public var popupChooseModele:PopupChooseModeleIHM;
		public var popupChooseTypeLigne:PopupChooseTypeLigneIHM;
		
		private var _popupModificationPools:AlimentationPoolsIHM;
		private var myPoolSelected:ArrayCollection;
		
		private var nbColumn:int = 0;
		private var nouveauModele:String = "";
		
		private var listeDuree:ArrayCollection = new ArrayCollection(
			[
				{DUREE: ResourceManager.getInstance().getString('M111', '12_mois'), value: "12"}, 
				{DUREE: ResourceManager.getInstance().getString('M111', '24_mois'), value: "24"}, 
				{DUREE: ResourceManager.getInstance().getString('M111', '36_mois'), value: "36"}, 
				{DUREE: ResourceManager.getInstance().getString('M111', '48_mois'), value: "48"},
				{DUREE: ResourceManager.getInstance().getString('M111', '60_mois'), value: "60"}
			]
		);
		
		
		//--------------- METHODES ----------------//
		
		/* */
		public function FicheImportMasseImpl()
		{
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}
		
		/* */
		private function init(evt:FlexEvent):void
		{
			this.title += ResourceManager.getInstance().getString('M111', 'Import_de_masse');
			this.title += ' (';
			this.title += (SpecificationVO.getInstance().idPool == 0) ? SpecificationVO.getInstance().labelPoolNotValid : SpecificationVO.getInstance().labelPool;
			this.title += ')';
			
			checkIsPoolRevendeur();
			
			// initialisation des services
			myServices = new ImportMasseServices();
			myServices.myHandlers.nouveauModele = nouveauModele;
			myServices.myHandlers.comboModele = comboModele;
			myServices.myHandlers.comboEngagement = comboEngagement;
			myServices.myHandlers.checkFPC = checkFPC;
			myServices.myHandlers.checkLignes = checkLignes;
			
			myServices.initComboOperateur();
			var chaine:String; // a voir ce que c !!!!!!!!!!!!!!!!!!!!!!!!!!!!
			myServices.initComboRevendeur(CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX, CvAccessManager.getSession().USER.CLIENTACCESSID, chaine);
			
			comboTypeLigne.dataProvider = CacheDatas.listeTypesLigne;
			
			// initialisation des durées garantis et engagement
			comboDuree.dataProvider = listeDuree;
			comboEngagement.dataProvider = listeDuree;
			
			// NE PAS SUPPRIMER ***************
//			comboQte.dataProvider = fillComboQuantite(); NE PAS SUPPRIMER ***************
//			comboQte.selectedIndex = -1; NE PAS SUPPRIMER ***************
//			comboQte.prompt = "choisir la quantité"; NE PAS SUPPRIMER ***************
			
			// initialisation des ecouteurs
			initListener();
			
			// initialisation de l'afficge par defaut
			initDefaultDisplay(false);
			
			// initialisation du datagrid
			initDgEquipement();
			
			// initialisation du dataGrid
			initDataGridImportMasse();
		}
		
		/* */
		private function fillComboQuantite():Array
		{
			var arr:Array = new Array();
			for (var i:int = 1; i <= 100; i++)
			{
				arr.push(i);
			}
			return arr;
		}
		
		/* */
		private function initDataGridImportMasse():void
		{
			var largeurTotalDG:Number = myComposantImportMasse.dgElement.width;
			
			// pour éviter un bug de dimension des colonnes, il faut supprimer la colonne "supprimer" puis la remettre
			var colonne7:DataGridColumn = new DataGridColumn();
			colonne7.dataField = "DELETE";
			colonne7.headerText = "";
			colonne7.width = 30;
			colonne7.itemRenderer = myComposantImportMasse.dgElement.columns.pop().itemRenderer;
			colonne7.sortable = false;
			colonne7.editable = false;
			
			myComposantImportMasse.dgElement.columns = new Array();
			
			myComposantImportMasse.addColumnInDG([colonne7, colonne6, colonne5, colonne4, colonne3, colonne2, colonne1]);
			
			// selectionne la "matrice type d'import" selon la langue.
			switch (CvAccessManager.getSession().USER.GLOBALIZATION)
			{
				case "fr_FR": // francais
					myComposantImportMasse.urlDownload = moduleGestionParcIHM.NonSecureUrlBackoffice + "/fichierModeleExcel/M11/Matrice_Import_FR.xls";
					break;
				case "en_US": // anglais
					myComposantImportMasse.urlDownload = moduleGestionParcIHM.NonSecureUrlBackoffice + "/fichierModeleExcel/M11/Matrice_Import_US.xls";
					break;
				case "es_ES": // espagnol
					myComposantImportMasse.urlDownload = moduleGestionParcIHM.NonSecureUrlBackoffice + "/fichierModeleExcel/M11/Matrice_Import_ES.xls";
					break;
				case "nl_NL":
					myComposantImportMasse.urlDownload = moduleGestionParcIHM.NonSecureUrlBackoffice + "/fichierModeleExcel/M11/Matrice_Import_NL.xls";
					break;
				default:
				{
					try
					{
						var _lg:String = CvAccessManager.getSession().USER.GLOBALIZATION.split("_")[0].toString().toUpperCase();
						myComposantImportMasse.urlDownload = moduleGestionParcIHM.NonSecureUrlBackoffice + "/fichierModeleExcel/M111/Matrice_Import_" + _lg + ".xls";
					}
					catch (err:Error)
					{
						myComposantImportMasse.urlDownload = moduleGestionParcIHM.NonSecureUrlBackoffice + "/fichierModeleExcel/M11/Matrice_Import_FR.xls";
					}
				}
			}
		}
		
		/* */
		private function initListener():void
		{	
			// NE PAS SUPPRIMER ***************
//			comboQte.addEventListener(ListEvent.CHANGE,choixQteHandler); NE PAS SUPPRIMER ***************
			
			// Action User sur composant ImportMasse
			myComposantImportMasse.btClear.addEventListener("DISABLE_BTN_VALID",disableBtnValidHandler);
			addEventListener("CHECK_BTN_VALID",isBtnValidEnableHandler);
			addEventListener("IS_ROW_DG_LEFT",isRowDgLeftHandler);
			// NE PAS SUPPRIMER ***************
//			myComposantImportMasse.btAddRow.addEventListener("INIT_INDEX_COMBO",initIndexComboQteHandler); NE PAS SUPPRIMER ***************
			
			// SERVICES
			myServices.myHandlers.addEventListener(gestionparcEvent.LISTED_MODELE_LOADED, initComboBox);
			myServices.myHandlers.addEventListener(gestionparcEvent.LISTED_OPERATEUR_LOADED, initComboBox);
			myServices.myHandlers.addEventListener(gestionparcEvent.LISTED_REVENDEUR_LOADED, initComboBox);
			myServices.myHandlers.addEventListener(gestionparcEvent.EQPT_IMPORTE, closePopup);
			myServices.myHandlers.addEventListener(gestionparcEvent.OPEN_POPUP_CONFIRMATION, openPopUpConfirmation);
			myServices.myHandlers.addEventListener(gestionparcEvent.POPUP_CONFIRMATION_IMPORT, callServiceSaveMultipleEquip);
		}
		
		/* quand le bouton valider doit etre enabled ou non */
		protected function isBtnValidEnableHandler(e:Event):void
		{
			btValider.enabled = false;
			var boolValid:Boolean = true;
			var myAC:ArrayCollection = myComposantImportMasse.dgElement.dataProvider as ArrayCollection;
			
			for each(var item:ImportDeMasseVO in myAC)
			{
				if ((! item.COLLAB_OK && item.COLLAB_TO_IMPORT)
					|| !item.IMEI_OK && item.IMEI_TO_IMPORT && (comboModele.selectedItem.IDTYPE_EQUIPEMENT == 70 || comboModele.selectedItem.ISMOBILE == 1)
					|| !item.NUMSERIE_OK && item.NUMSERIE_TO_IMPORT && comboModele.selectedItem.ISFIXE == 1
					|| !item.NUMEXT_OK && item.NUMEXT_TO_IMPORT
					|| !item.NUMSIM_OK && item.NUMSIM_TO_IMPORT
					|| !item.NUMLIGNE_OK && item.NUMLIGNE_TO_IMPORT)
				{
					boolValid = false;
					break;
				}
			}
			if(boolValid)
				btValider.enabled = true;
		}
		
		/* */
		private function isRowDgLeftHandler(e:Event):void
		{
			btValider.enabled = false;
		}
		
		/* */
		public function checkFPCUClickHandler(me:MouseEvent):void
		{
			if (checkFPC.selected)
			{
				checkFPC.setStyle("color", 0x000000);
				formItemEngagement.enabled = false;
				comboEngagement.enabled = false;
			}
			else
			{
				checkFPC.setStyle("color", 0xCCCCCC);
				formItemEngagement.enabled = true;
				comboEngagement.enabled = true;
			}
		}
		
		/**
		 * Créer une popUp et l'affiche. Cette popUp contient les aides pour l'import de masse.
		 */
		public function btHelpClickHandler(e:Event):void
		{
			var poppHelp:ImportMassePopUpHelp = new ImportMassePopUpHelp();
			PopUpManager.addPopUp(poppHelp, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(poppHelp);
		}
		
		/* */
		protected function formaterColonneDates(item:Object, column:DataGridColumn):String
		{
			if (item != null && item[column.dataField] != null)
			{
				return DateFunction.formatDateAsString(item[column.dataField]);
			}
			else
			{
				return "";
			}
		}
		
		/* */
		private function checkIsPoolRevendeur():void
		{
			if (poolDistributeur != null)
			{
				if (poolDistributeur.IDREVENDEUR != null)
					isPoolRevendeur = true;
				else
					isPoolRevendeur = false; //---> A METTRE A FALSE, TRUE C'EST POUR LES TESTS
			}
			else
				isPoolRevendeur = false;
		}
		
		/* */
		public function btnModifierPoolsClickHandler(me:MouseEvent):void
		{
			_popupModificationPools = new AlimentationPoolsIHM();
			_popupModificationPools.objPoolRev = poolDistributeur;
			_popupModificationPools.isPoolRev = isPoolRevendeur;
			_popupModificationPools.addEventListener('POOL_CHANGED', popupModificationPoolsValidHandler);

			PopUpManager.addPopUp(_popupModificationPools, this, true);
			PopUpManager.centerPopUp(_popupModificationPools);
		}
		
		/* */
		private function popupModificationPoolsValidHandler(e:Event):void
		{
			myPoolSelected = null;

			if (_popupModificationPools.isDefault)
				lblChangePools.text = "« Par défaut »";
			else
			{
				lblChangePools.text = "« Modifié »";
				myPoolSelected = _popupModificationPools.pools;
			}

			PopUpManager.removePopUp(_popupModificationPools);
		}
		
		/* */
		private function formatMyXML(xmlequipements:XML):XML
		{
			var items:XML = <item></item>;
			var pools:XML = <pools></pools>;

			if (myPoolSelected != null)
			{
				var poolsSelected:Array = myPoolSelected.source;
				var lenPools:int = poolsSelected.length;
				var pool:XML = <pool></pool>;

				for (var i:int = 0; i < lenPools; i++)
				{
					pools.appendChild(<pool>{myPoolSelected[i]}</pool>);
				}
			}

			items.appendChild(xmlequipements);
			items.appendChild(pools);

			return items;
		}
		
		/* lors du clic sur l'image loupe (choix modele) */
		public function chooseModeleHandler(me:MouseEvent):void
		{
			popupChooseModele = new PopupChooseModeleIHM();
			popupChooseModele.addEventListener("VALID_CHOIX_MODELE",getItemModeleHandler);
			
			var cpt:int = 0;
			var itemSelected:ModeleVO;
			
			for each(var obj:Object in ObjectUtil.copy(myServices.myDatas.listeModele.source) as Array)
			{
				var itemModele:ModeleVO = new ModeleVO();
				itemModele.fillVO(obj);
				
				if(comboModele.selectedIndex == cpt)
					itemSelected = itemModele;
				popupChooseModele.listeModele.addItem(itemModele);
				cpt++;
			}
			popupChooseModele.setModele(itemSelected);
			
			PopUpManager.addPopUp(popupChooseModele,this,true);
			PopUpManager.centerPopUp(popupChooseModele);
		}
		
		/* recuperer l'item modele choisi */
		private function getItemModeleHandler(e:Event):void
		{
			popupChooseModele.removeEventListener("VALID_CHOIX_MODELE",getItemModeleHandler);
			comboModele.selectedIndex = ConsoviewUtil.getIndexById(comboModele.dataProvider as ArrayCollection,"IDEQUIPEMENT_FOURNISSEUR",popupChooseModele.itemCurrent.IDEQUIPEMENT_FOURNISSEUR)
		}
		
		/* */
		public function chooseTypeLigneHandler(me:MouseEvent):void
		{
			popupChooseTypeLigne = new PopupChooseTypeLigneIHM();
			popupChooseTypeLigne.addEventListener("VALID_CHOIX_TYPELIGNE",getItemTypeLigneHandler);
			
			var typeLigneSelected:int = (checkLigneMobile.selected) ? 1 : 0;
			var cpt:int = 0;
			var itemSelected:TypeLigneVO;
			
			for each(var obj:Object in ObjectUtil.copy(CacheDatas.listeTypesLigne.source) as Array)
			{
				var boolFill:Boolean = false;
				
				if(typeLigneSelected == 0 && obj.ISFIXE != null && obj.ISFIXE == 1 && obj.ISMOBILE != 1)
					boolFill = true;
				if (typeLigneSelected == 1 && obj.ISMOBILE != null && obj.ISMOBILE == 1 && obj.IDTYPE_LIGNE == 707)
					boolFill = true;
				
				if(boolFill)
				{
					var itemTypeLigne:TypeLigneVO = new TypeLigneVO();
					itemTypeLigne.fillVO(obj);
					
					if(comboTypeLigne.selectedIndex == cpt)
						itemSelected = itemTypeLigne;
					
					popupChooseTypeLigne.listeTypeLigne.addItem(itemTypeLigne);
					cpt++;
				}
			}
			popupChooseTypeLigne.setTypeLigne(itemSelected);
						
			PopUpManager.addPopUp(popupChooseTypeLigne,this,true);
			PopUpManager.centerPopUp(popupChooseTypeLigne);
		}
		
		/* recuperer l'item type ligne choisi */
		private function getItemTypeLigneHandler(e:Event):void
		{
			popupChooseTypeLigne.removeEventListener("VALID_CHOIX_TYPELIGNE",getItemTypeLigneHandler);
			comboTypeLigne.selectedIndex = ConsoviewUtil.getIndexById(comboTypeLigne.dataProvider as ArrayCollection,"IDTYPE_LIGNE",popupChooseTypeLigne.itemCurrent.IDTYPE_LIGNE)
		}
		
		/* */
		private function initComboBox(evt:gestionparcEvent):void
		{
			// initialisation des revendeurs, opérateurs et modèles
			switch (evt.type)
			{
				case gestionparcEvent.LISTED_MODELE_LOADED:
					comboModele.dataProvider = myServices.myDatas.listeModele;
					comboModele.dropdown.dataProvider = myServices.myDatas.listeModele;
					if (nouveauModele)
					{
						var longueur:int = myServices.myDatas.listeModele.length;
						for (var i:int = 0; i < longueur; i++)
						{
							if (myServices.myDatas.listeModele[i].LIBELLE_EQ == nouveauModele)
							{
								comboModele.selectedIndex = i;
								break;
							}
						}
					}
					break;
				case gestionparcEvent.LISTED_REVENDEUR_LOADED:
					comboRevendeurSIM.dataProvider = myServices.myDatas.listeRevendeur;
					comboRevendeurSIM.dropdown.dataProvider = myServices.myDatas.listeRevendeur;
					comboRevendeur.dataProvider = myServices.myDatas.listeRevendeur;
					comboRevendeur.dropdown.dataProvider = myServices.myDatas.listeRevendeur;
					if ((comboRevendeur.dataProvider as ArrayCollection).length > 0)
					{
						if (comboRevendeur.selectedItem)
						{
							myServices.initComboModele(comboRevendeur.selectedItem);
						}
						else
						{
							comboModele.selectedIndex = -1;
						}
					}
					myServices.myHandlers.removeEventListener(gestionparcEvent.LISTED_REVENDEUR_LOADED, initComboBox);
					break;
				case gestionparcEvent.LISTED_OPERATEUR_LOADED:
					comboOperateur.dataProvider = myServices.myDatas.listeOperateur;
					comboOperateur.dropdown.dataProvider = myServices.myDatas.listeOperateur;
					callServiceGetOperatorFPCU(null);
					myServices.myHandlers.removeEventListener(gestionparcEvent.LISTED_OPERATEUR_LOADED, initComboBox);
					break;
			}
		}
		
		
//		/* */ NE PAS SUPPRIMER ***************
//		private function choixQteHandler(le:ListEvent):void
//		{
//			myComposantImportMasse.qteToImport = comboQte.selectedIndex + 1;
//		}
//		
//		/* */ NE PAS SUPPRIMER ***************
//		private function initIndexComboQteHandler(e:Event):void
//		{
//			comboQte.selectedIndex = -1;
//			comboQte.prompt = ResourceManager.getInstance().getString('M111', 'Choisir_la_quantit_');
//			myComposantImportMasse.qteToImport = comboQte.selectedIndex;
//		}
		
		/* */
		private function disableBtnValidHandler(e:Event):void
		{
			btValider.enabled = false;
		}
		
		/* */
		private function initDefaultDisplay(value:Boolean):void
		{
			// (dé)sactivation de toute les lignes car les checkBox n'ont pas été sélectionnées
			formItemRevendeur.enabled = value;
			formItemDebutGarantie.enabled = value;
			formItemOperateur.enabled = value;
			formItemDebutLigne.enabled = value;
			formItemModele.enabled = value;
			formItemDuree.enabled = value;
			formItemEngagement.enabled = !checkFPC.visible;
			formItemTypeLigne.enabled = value;
			btImgChooseModele.source = imgChooseModeleGris;
			btImgChooseModele.buttonMode = value;
			btImgChooseModele.mouseEnabled = value;
			btImgChooseTypeLigne.source = imgChooseModeleGris; // même image que pour modele
			btImgChooseTypeLigne.buttonMode = value;
			btImgChooseTypeLigne.mouseEnabled = value;
			checkFPC.enabled = value;
			checkFPC.visible = value;
			
			// bouton valider disable
			btValider.enabled = value;
			
			// on (dé)coche les checkBox
			checkCollaborateurs.selected = value;
			checkGarantie.selected = value;
			checkLignes.selected = value;
			checkSim.selected = value;
			checkTerminaux.selected = value;
			
			// renseignement des variables statiques
			bool_check_term = value;
			bool_check_sim = value;
			bool_check_ligne = value;
			bool_check_collab = value;
			bool_check_extension = value;

			var myDate:Date = new Date();
			dcDateDebutGarantie.text = DateFunction.formatDateAsString(myDate);
			dcDateDebutLignes.text = DateFunction.formatDateAsString(myDate);
		}
		
		/* */
		private function initDgEquipement():void
		{
			_datagrid_import_mobile_ = myComposantImportMasse.dgElement;
		}
		
		/* */
		private function comboRevendeurChange(evt:Event):void
		{
			if (FicheImportMasseImpl.bool_check_term)
			{
				if (FicheImportMasseImpl.bool_check_sim)
					comboRevendeurSIM.selectedItem = comboRevendeur.selectedItem;
				else
					comboRevendeurSIM.selectedItem = -1;
				callServiceInitComboModele(evt);
			}
		}
		
		/* Pour la mise à jour des modèles lorsque le revendeur change*/
		public function callServiceInitComboModele(evt:Event):void
		{
			if (evt.type == gestionparcEvent.NEW_MODELE_ADDED)
			{
				// sélection du modèle qu'on vient d'ajouter
				nouveauModele = (evt as gestionparcEvent).obj.toString();
			}
			if ((comboRevendeur.dataProvider as ArrayCollection).length > 0)
			{
				if (comboRevendeur.selectedItem)
				{
					myServices.initComboModele(comboRevendeur.selectedItem);
				}
				else
				{
					myServices.initComboModele(comboRevendeur.dataProvider[0]);
				}
			}
		}
		
		/* */
		public function callServiceGetOperatorFPCU(evt:Event):void
		{
			if (comboOperateur.selectedItem)
			{
				myServices.getOperatorFPCU(comboOperateur.selectedItem);
			}
		}
		
		/* */
		private function callServiceSaveMultipleEquip(evt:Event):void
		{
			mydoc=formatMyXML(mydoc);

			if (comboRevendeur.selectedItem && FicheImportMasseImpl.bool_check_term && comboModele.selectedItem && comboOperateur.selectedItem && FicheImportMasseImpl.bool_check_sim && comboRevendeurSIM.selectedItem)
			{
				myServices.saveMultipleEquip(comboRevendeur.selectedItem, comboModele.selectedItem, comboOperateur.selectedItem, mydoc.toString(), dcDateDebutGarantie.selectedDate ? DateFunction.formatDateAsInverseString(dcDateDebutGarantie.selectedDate) : DateFunction.formatDateAsInverseString(new Date()), comboDuree.selectedItem, dcDateDebutLignes.selectedDate ? DateFunction.formatDateAsInverseString(dcDateDebutLignes.selectedDate) : DateFunction.formatDateAsInverseString(new Date()), comboEngagement.selectedItem, convertBoolToInt());
			}
			else
			{
				var objRev:Object = null;
				var objModele:Object = null;
				var objOpe:Object = null;

				if (comboOperateur.selectedItem && FicheImportMasseImpl.bool_check_sim && comboRevendeurSIM.selectedItem)
				{
					objRev = comboRevendeurSIM.selectedItem;
					objOpe = comboOperateur.selectedItem;
				}
				if (comboRevendeur.selectedItem && FicheImportMasseImpl.bool_check_term && comboModele.selectedItem)
				{
					objRev = comboRevendeur.selectedItem;
					objModele = comboModele.selectedItem;
				}

				myServices.saveMultipleEquip(objRev, objModele, objOpe, mydoc.toString(), dcDateDebutGarantie.selectedDate ? DateFunction.formatDateAsInverseString(dcDateDebutGarantie.selectedDate) : DateFunction.formatDateAsInverseString(new Date()), comboDuree.selectedItem, dcDateDebutLignes.selectedDate ? DateFunction.formatDateAsInverseString(dcDateDebutLignes.selectedDate) : DateFunction.formatDateAsInverseString(new Date()), comboEngagement.selectedItem, convertBoolToInt());
			}
		}
		
		/* */
		private function convertBoolToInt():int
		{
			var boolInt:int = 0;

			if (isPoolRevendeur)
				boolInt = 1;

			return boolInt;
		}
		
		/* */
		public function checkBoxChange(e:Event):void
		{
			var arrayC:ArrayCollection = myComposantImportMasse.dgElement.dataProvider as ArrayCollection;
			var item:ImportDeMasseVO;
			
			switch (e.target.id)
			{
				case "checkTerminaux":
				{
					if (checkTerminaux.selected)
					{
						formItemRevendeur.enabled = true;
						formItemModele.enabled = true;
						btImgChooseModele.buttonMode = true;
						btImgChooseModele.mouseEnabled = true;
						btImgChooseModele.source = imgChooseModele;
						btImgChooseModele.toolTip = ResourceManager.getInstance().getString('M111', 'voir_les_mod_les');
						bool_check_term = true;
						
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.imei_isChecked = true;
					}
					else
					{
						formItemRevendeur.enabled = false;
						formItemModele.enabled = false;
						btImgChooseModele.buttonMode = false;
						btImgChooseModele.mouseEnabled = false;
						btImgChooseModele.source = imgChooseModeleGris;
						btImgChooseModele.toolTip = "";
						
						bool_check_term = false;
						
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.imei_isChecked = false;
						
						if (checkGarantie.selected)
						{
							formItemDebutGarantie.enabled = false;
							formItemDuree.enabled = false;
							checkGarantie.selected = false;
						}
					}
					break;
				}
				case "checkGarantie":
				{
					if (checkGarantie.selected)
					{
						if (checkTerminaux.selected)
						{
							formItemDebutGarantie.enabled = true;
							formItemDuree.enabled = true;
							
							// specifier les colonnes à importer pour chaque row à ajouter
							myComposantImportMasse.garantie_isChecked = true;
						}
						else
						{
							Alert.show(ResourceManager.getInstance().getString('M111', 'Vous_devez_s_lectionner_un_terminal_pour'), ResourceManager.getInstance().getString('M111', 'Erreur'));
							checkGarantie.selected = false;
						}
					}
					else
					{
						formItemDebutGarantie.enabled = false;
						formItemDuree.enabled = false;
						
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.garantie_isChecked = false;
					}
					break;
				}
				case "checkSim":
				{
					if (checkSim.selected)
					{
						formItemOperateur.enabled = true;
						bool_check_sim = true;
						// annuler les initialisations du radiobutton sim
						bool_check_extension = false;
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.sim_isChecked = true;
						myComposantImportMasse.extension_isChecked = false;
					}
					break;
				}
				case "checkLignes":
				{
					if (checkLignes.selected)
					{
						btImgChooseTypeLigne.buttonMode = true;
						btImgChooseTypeLigne.mouseEnabled = true;
						btImgChooseTypeLigne.source = imgChooseModele;
						btImgChooseTypeLigne.toolTip = ResourceManager.getInstance().getString('M111', 'voir_les_types_de_ligne');
						
						formItemDebutLigne.enabled = true;
						formItemEngagement.enabled = true;
						formItemTypeLigne.enabled = true;
						comboEngagement.enabled = !checkFPC.visible;
						checkFPC.enabled = true;
						checkFPC.selected = true;
						checkFPCUClickHandler(null);
						bool_check_ligne = true;
						
						checkLigneMobileHandler(null);
						
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.ligne_isChecked = true;
						
						// selectionner en meme temps le radioboutton SIM
						checkSimExtension.selected = true;
						formItemOperateur.enabled = true;
						bool_check_sim = true;
						bool_check_extension = false;
						myComposantImportMasse.sim_isChecked = true;
						myComposantImportMasse.extension_isChecked = false;
					}
					else
					{
						btImgChooseTypeLigne.buttonMode = false;
						btImgChooseTypeLigne.mouseEnabled = false;
						btImgChooseTypeLigne.source = imgChooseModeleGris;  // même image que pour modele
						btImgChooseTypeLigne.toolTip = "";
						
						formItemDebutLigne.enabled = false;
						formItemEngagement.enabled = false;
						formItemDebutLigne.enabled = false;
						formItemTypeLigne.enabled = false;
						checkFPC.enabled = false;
						checkFPC.selected = false;
//						checkFPC.visible=false;
//						checkFPCUClickHandler(null);
						bool_check_ligne = false;
						
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.ligne_isChecked = false;
					}
					break;
				}
				case "checkCollaborateurs":
				{
					if (checkCollaborateurs.selected)
					{
						bool_check_collab = true;
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.collab_isChecked = true;
					}
					else
					{
						bool_check_collab = false;
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.collab_isChecked = false;
					}
					break;
				}
				case "checkExtension":
				{
					if (checkExtension.selected)
					{
						bool_check_extension = true;
						//annuler les initialisations du radiobutton sim
						formItemOperateur.enabled = false;
						bool_check_sim = false;
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.extension_isChecked = true;
						myComposantImportMasse.sim_isChecked = false;
					}
					break;
				}
				case "checkSimExtension":
				{
					if(checkSimExtension.selected)
					{
						checkSim.enabled = true;
						checkSim.selected = true;
						checkExtension.enabled = true;
						formItemOperateur.enabled = true;
						
						bool_check_sim = true;
						bool_check_extension = false;
						
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.sim_isChecked = true;
						myComposantImportMasse.extension_isChecked = false;
					}
					else
					{
						checkSim.enabled = false;
						checkSim.selected = false;
						checkExtension.enabled = false;
						checkExtension.selected = false;
						formItemOperateur.enabled = false;
						
						bool_check_sim = false;
						bool_check_extension = false;
						
						// specifier les colonnes à importer pour chaque row à ajouter
						myComposantImportMasse.extension_isChecked = false;
						myComposantImportMasse.sim_isChecked = false;
					}
				}
			}
			isBtnImportEnabled();
		}
		
		/* affiche un warning si aucune checkbox est cochée */
		private function isBtnImportEnabled():void
		{
			myComposantImportMasse.isEnabledBtnImport = (bool_check_term || bool_check_sim || bool_check_ligne || bool_check_collab || bool_check_extension) ? true : false;
		}
		
		/* lors du click sur radiobutton typeLigne */
		public function checkLigneMobileHandler(me:MouseEvent):void
		{
			(comboTypeLigne.dataProvider as ArrayCollection).filterFunction = filtreComboTypeLigneMobile;
			(comboTypeLigne.dataProvider as ArrayCollection).refresh();
			comboTypeLigne.selectedIndex = 0;
		}
		
		/* lors du click sur radiobutton typeLigne */
		public function checkLigneAutreHandler(me:MouseEvent):void
		{
			(comboTypeLigne.dataProvider as ArrayCollection).filterFunction = filtreComboTypeLigneAutre;
			(comboTypeLigne.dataProvider as ArrayCollection).refresh();
			comboTypeLigne.selectedIndex = 0;
		}
		
		/* systeme de filtre */
		private function filtreComboTypeLigneMobile(item:Object):Boolean
		{
			if (item.ISMOBILE != null && item.ISMOBILE == 1 && item.ISFIXE != 1 && item.IDTYPE_LIGNE == 707)
				return true;
			else
				return false;
		}
		
		/* systeme de filtre */
		private function filtreComboTypeLigneAutre(item:Object):Boolean
		{
			if (item.ISFIXE != null && item.ISFIXE == 1 && item.ISMOBILE != 1)
				return true;
			else
				return false;
		}
		
		/* appel de la methode de fermeture popup */
		public function cancelPopup(evt:MouseEvent):void
		{
			closePopup(null);
		}
		
		/* fermeture popup */
		public function closePopup(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}
		
		/* popup pour demander confirmation de l'import */
		public function confirmValidationHandler(me:MouseEvent):void
		{
			ConsoviewAlert.afficherAlertConfirmation(ResourceManager.getInstance().getString('M111', 'Etes_vous_s_r_de_vouloir_importer_ces__l_ments_'),ResourceManager.getInstance().getString('M111', 'Consoview'),AlertConfirmValidation);
		}
		
		/*  confirmer la validation de l'import */
		private function AlertConfirmValidation(ce:CloseEvent):void
		{
			if(ce.detail == Alert.OK)
				validateImport();
		}
		
		/* appui sur bouton valider pour l'import */
		private function validateImport():void
		{
			var data:ArrayCollection = new ArrayCollection();
			var arrayC:ArrayCollection = myComposantImportMasse.dgElement.dataProvider as ArrayCollection;
			var idpool:int = (SpecificationVO.getInstance().idPool == 0) ? SpecificationVO.getInstance().idPoolNotValid : SpecificationVO.getInstance().idPool;
			
			for(var i:int = 0; i < arrayC.length; i++) //row
			{
				var row:Object = new Object();
				
				//collab
				var vmatricule	:String = arrayC[i].MATRICULE;
				var vnom		:String = arrayC[i].NOM;
				var vprenom		:String = arrayC[i].PRENOM;
				
				if((arrayC[i] as ImportDeMasseVO).COLLAB_TO_IMPORT)
				{
					row["collab"] = {
						matricule:vmatricule, 
						nom:vnom, 
						prenom:vprenom
					};
				}
				
				//contrat equipement
				var vidtype_contrat_eq		:int = 3;
				var vdate_deb_contrat_eq	:String = (dcDateDebutGarantie.selectedDate == null) ? DateFunction.formatDateAsInverseString(new Date()) : DateFunction.formatDateAsInverseString(dcDateDebutGarantie.selectedDate);
				var vduree_contrat_eq		:int = comboDuree.selectedItem.value;
				var vfpc_contrat_eq			:String = "";
				//equipement
				var viddistrib	:int = comboRevendeur.selectedItem.IDREVENDEUR;
				var vidtype		:int = (comboModele.selectedItem)?comboModele.selectedItem.IDTYPE_EQUIPEMENT : -1;
				var videq_fourn	:int = (comboModele.selectedItem)?comboModele.selectedItem.IDEQUIPEMENT_FOURNISSEUR : -1;
				var vimei		:String = arrayC[i].IMEI;
				var vnserie		:String = arrayC[i].NUMSERIE;
				
				if((arrayC[i] as ImportDeMasseVO).IMEI_TO_IMPORT)
				{
					row["equipement"] = {
						iddistrib:viddistrib,
						idtype:vidtype,
						ideq_fourn:videq_fourn,
						imei:vimei,
						nserie:vnserie
					};
					if((arrayC[i] as ImportDeMasseVO).GARANTIE_AVAILABLE)
					{
						row["equipement"]["contrat"] = {
							idtype:vidtype_contrat_eq,
							date_deb:vdate_deb_contrat_eq,
							duree:vduree_contrat_eq,
							fpc:vfpc_contrat_eq
						};
					}
				}
				
				//sim
				var vidoperateur:int = comboOperateur.selectedItem.OPERATEURID;
				var vnumero		:String = arrayC[i].NUMSIM;
				var vidDistributeurSim :int = (comboRevendeurSIM.selectedItem)?comboRevendeurSIM.selectedItem.IDREVENDEUR : -1;
				if((arrayC[i] as ImportDeMasseVO).NUMSIM_TO_IMPORT)
				{
					row["sim"] = {
						iddistrib:vidDistributeurSim, 
						idoperateur:vidoperateur,
						numero:vnumero
					};
				}
				
				//contrat ligne
				var vidtype_contrat_ligne	:int = 6;
				var vdate_deb_contrat_ligne	:String = (dcDateDebutLignes.selectedDate == null) ? DateFunction.formatDateAsInverseString(new Date()) : DateFunction.formatDateAsInverseString(dcDateDebutLignes.selectedDate);
				var vduree_contrat_ligne	:int = (formItemEngagement.enabled) ? comboEngagement.selectedItem.value : 0;
				var vfpc_contrat_ligne		:String = (checkFPC != null && checkFPC.selected) ? DateFunction.formatDateAsInverseString(myServices.myHandlers.dateFPC) : "";
				//ligne
				var vidtype_ligne		:int = (comboTypeLigne.selectedItem)?comboTypeLigne.selectedItem.IDTYPE_LIGNE : -1; //707 = mobile
				var vidoperateur_ligne	:int = (comboOperateur.selectedItem.OPERATEURID)?comboOperateur.selectedItem.OPERATEURID : -1;
				var vnumero_ligne		:String = arrayC[i].NUMLIGNE;
				
				if((arrayC[i] as ImportDeMasseVO).NUMLIGNE_TO_IMPORT)
				{
					row["ligne"] = {
						idtype:vidtype_ligne,
						idoperateur:vidoperateur_ligne,
						numero:vnumero_ligne
					};
					row["ligne"]["contrat"] = {
						idtype:vidtype_contrat_ligne,
						date_deb:vdate_deb_contrat_ligne,
						duree:vduree_contrat_ligne,
						fpc:vfpc_contrat_ligne
					};
				}
				
				//extension
				var vnumero_ext		:String = arrayC[i].NUMEXT;
				var vidoperateur_ext:int = 190; //comboOperateur.selectedItem.OPERATEURID;
				if((arrayC[i] as ImportDeMasseVO).NUMEXT_TO_IMPORT)
				{
					row["extension"] = {
						idoperateur:vidoperateur_ext,
						numero:vnumero_ext
					};
				}
				
				data.addItem(row);
			}
			
			myServices.importFichier(data,idpool);
			myServices.myDatas.addEventListener(ImportDeMasseEvent.IMPORT_DONE,importDoneHandler);
		}
		
		/* lorsque l'import est effectué - handler*/
		protected function importDoneHandler(e:Event):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M111', 'L_import_a_bien_ete_effectu_'));
			closePopup(null);
		}
				
		/* */
		private function openPopUpConfirmation(evt:gestionparcEvent):void
		{
			var popupConfirmationImport:ImportConfirmationIHM = new ImportConfirmationIHM();
			popupConfirmationImport.nbIMEI = evt.obj.NBIMEI;
			popupConfirmationImport.nbSIM = evt.obj.NBSIM;
			popupConfirmationImport.nbLignes = evt.obj.NBLIGNE;
			popupConfirmationImport.nbCollabo = evt.obj.NBCOLLAB;
			popupConfirmationImport.xmlErreur = evt.obj.ERREUR;
			popupConfirmationImport.addEventListener(gestionparcEvent.POPUP_CONFIRMATION_IMPORT, callServiceSaveMultipleEquip);
			PopUpManager.addPopUp(popupConfirmationImport, this, true);
			PopUpManager.centerPopUp(popupConfirmationImport);
		}
		
		protected function checkLigneMobile_ChangeHandler():void
		{
			myComposantImportMasse.myImportMasseUtils.LIGNE_IS_MOBILE = checkLigneMobile.selected;
		}
	}
}