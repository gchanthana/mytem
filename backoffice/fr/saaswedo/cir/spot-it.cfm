var t = document.getElementsByTagName('head')[0];
var p = t.querySelectorAll("script");
var swdUser = p[p.length-1].id;

var jq = null;

var jamespot_UA = {
    urlBase : 'https://backoffice4.consotel.fr/fr/saaswedo/cir',
	include : 	function(url)
				{
					var m = document.createElement('script');
					m.setAttribute('type', 'text/javascript');
					m.setAttribute('src', url);
					m.setAttribute('language', "javascript");
					//alert(url)
					document.getElementsByTagName('head')[0].appendChild(m);
				},
				
	ready :	function()
			{				
				jq = jQuery.noConflict();
			
				var params = new Array();
				/*
					a = action
					v = version
					f = from
					tl = tools
					t = title
					u = url
					x = extract
				*/
				params['a']='se';
				params['v']= 2;
				params['f']='clip';
				params['tl']='bk';
				params['u']=encodeURIComponent(document.location);
				params['t']=encodeURIComponent(document.title);
				params['x']=encodeURIComponent(jamespot_UA.selection());
				params['user']=swdUser;
								
				if(/Firefox/.test(navigator.userAgent))
				{
					setTimeout(jamespot_UA.go(params), 0);
				}else{
					jamespot_UA.go(params);
				}
			},
			
	getParams : function(params)
				{
					var p="";
					var f=true;
  					for(key in params)
					{  
						if(typeof params[key] != 'function')
						{
							if(f)
							{
								p = '?'+key+'='+params[key];
								f = false;
							}else{
								p += '&'+key+'='+params[key];
							}
						}
					}
					return p;
				},
				
	go : 	function(params)
			{
										var u = jamespot_UA.urlBase+"/index.cfm"+jamespot_UA.getParams(params);

						if(!window.open(u, '_blank',"location=no,menubar=no,height=600,width=700,resizable=no,toolbar=no")){
							document.location.href=u+'&sw=1';
						}
							},
			
	selection :	function()
				{
					return document.selection?document.selection.createRange().text:(d=document.getSelection?d=document.getSelection():(w.getSelection?window.getSelection():''))
				},
				
	showModal : function(content)
				{					
					jq('body').append('<div id="jamespot_spot_it_modal"></div>');
					jq('#jamespot_spot_it_modal').css({'margin':'0','padding':'0','display':'none', 'position':'absolute','z-index':'110000'});
					
					jq('#jamespot_spot_it_modal').html(content);
					jq('body').append('<div id="jamespot_spot_it_background_modal"></div>');
					jq('#jamespot_spot_it_background_modal').css({'display':'none','position':'absolute','top':'0','right':'0','bottom':'0','left':'0','z-index':'100000','background':'#fff','opacity':'0.6'});
					
					w = jq('#jamespot_spot_it_modal').width();
					h = jq('#jamespot_spot_it_modal').height();
					
					screenSize = jamespot_UA.screeSize();
					
					// position left
					posLeft = ((screenSize["w"]-w)/2);
					jq('#jamespot_spot_it_modal').css('left', posLeft.toFixed(0)+'px');
					
					// position top
					posTop = ((screenSize["h"]-h)/2);
					jq('#jamespot_spot_it_modal').css('top', posTop.toFixed(0)+'px');
										
					jq('#jamespot_spot_it_background_modal').show();
					jq('#jamespot_spot_it_modal').show();
				},
				
	closeModal : 	function()
					{
						jq('#jamespot_spot_it_modal').hide();
						jq('#jamespot_spot_it_background_modal').hide();
					},
						
	screeSize :	function()
				{
					if(window.innerWidth)
				    {
				      var screenW=window.innerWidth;
				      var screenH=window.innerHeight;
				    }else if(document.documentElement && document.documentElement.clientHeight){ 
						var screenW = document.documentElement.clientWidth;
						var screenH = document.documentElement.clientHeight;
					}
					
				    var ret=new Array();
				    ret['w'] = screenW;
				    ret['h'] = screenH;
				    return ret;
				}							
};

jamespot_UA.include(jamespot_UA.urlBase+"/jquery-1.3.2.min.js");