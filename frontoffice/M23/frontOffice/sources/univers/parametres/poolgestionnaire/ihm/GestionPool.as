package univers.parametres.poolgestionnaire.ihm
{
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	
	
	
	public class GestionPool extends VBox
	{
		public static const COLOR_MESSAGE : uint = 0x006CFF;
		
		public var ongletListeProfilCompte : ListeProfilCompteIHM;
		
		
		
		public function GestionPool()
		{
		}
		public static function afficherMessage(msg:String,titre : String,labelBT:String="OK",color:uint = 0x000000):void
		{
			Alert.okLabel = "OK";
			var a : Alert = Alert.show(msg,titre,4,null,handleAlertClose);
			a.setStyle('color',color);
		}
 		public static function handleAlertClose(e:CloseEvent):void
		{
			Alert.okLabel = "OK";
		}

		
	}
}