package gestionparc.services.site
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class SiteServices
	{
		
		public var myDatas 		: SiteDatas;
		public var myHandlers 	: SiteHandlers;
		
		public function SiteServices()
		{
			myDatas 	= new SiteDatas();
			myHandlers 	= new SiteHandlers(myDatas);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer les infos d'un Site.
		 */	
		public function getSiteEmplacement(createObjToServiceGet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSite",
				"getSiteEmplacement",
				myHandlers.getSiteEmplacementResultHandler);
			
			RemoteObjectUtil.callService(op, createObjToServiceGet.idSite);
		}
		
		/**
		 * Appelle la procédure permettant de récupérer la liste des emplacements pour un Site.
		 */	
		public function getEmplacement(createObjToServiceGet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSite",
				"getEmplacement",
				myHandlers.getEmplacementResultHandler);
			
			RemoteObjectUtil.callService(op, createObjToServiceGet.idSite);
		}
		
		/**
		 * Appelle la procédure permettant de mettre à jour :
		 * - Tous les informations concernant le Site.
		 * - Tous ses emplacements
		 */	
		public function setSiteEmplacement(createObjToServiceSet : Object):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M111.fiches.FicheSite",
				"setSiteEmplacement",
				myHandlers.setSiteEmplacementResultHandler);
			
			RemoteObjectUtil.callService(op, 
				createObjToServiceSet.mySite.idSite,
				createObjToServiceSet.mySite.libelleSite,
				createObjToServiceSet.xmlEmplacement,
				createObjToServiceSet.mySite.refSite,
				createObjToServiceSet.mySite.codeInterneSite,
				createObjToServiceSet.mySite.adresse1,
				createObjToServiceSet.mySite.adresse2,
				createObjToServiceSet.mySite.codePostale,
				createObjToServiceSet.mySite.ville,
				createObjToServiceSet.mySite.commentaires,
				createObjToServiceSet.mySite.idPays,
				createObjToServiceSet.idPool);
		}
	}
}