package gestionparc.utils
{
	import flash.events.MouseEvent;
	
	import mx.controls.DataGrid;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.IFocusManagerComponent;
	
	public class CustomDatagrid extends DataGrid
	{
		/* */
		public var rowActuel : int;
		public var foc:IFocusManagerComponent;
		
		/* */
		public function CustomDatagrid()
		{
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE,initListeners);
		}
		
		/* */
		private function initListeners(fle:FlexEvent):void
		{
			this.addEventListener(DataGridEvent.ITEM_EDIT_BEGIN,itemEditBeginHandler);
			this.addEventListener(DataGridEvent.ITEM_EDIT_END,itemEditEndHandler);
			this.addEventListener(ListEvent.CHANGE,itemChangeHandler);
			this.addEventListener(ListEvent.ITEM_CLICK,itemClickHandler);
			this.addEventListener(MouseEvent.CLICK,mouseHandler);
		}
		
		/* */
		private function mouseHandler(me:MouseEvent):void
		{
//			this.selectedIndex = le.rowIndex;
//			trace("**MOUSECLICK= " + me.buttonDown + " **");
		}
		
		/* */
		override protected function mouseClickHandler(event:MouseEvent):void
		{
//			trace("**PROTECMOUSE**");
		}
		
		/* */
		private function itemClickHandler(le:ListEvent):void
		{
//			this.selectedIndex = le.rowIndex;
//			var ind:int = indexToRow(le.rowIndex);
//			trace("**CLICKID= " + le.rowIndex + " **"); // + " / "+ ind +" **");
		}
		
		/* */
		private function itemChangeHandler(le:ListEvent):void
		{
//			this.selectedIndex = le.rowIndex;
//			trace("**CHANGE= " + le.rowIndex + " ***");
		}
		
		/* */
		private function itemEditEndHandler(dge:DataGridEvent):void
		{
			// supprimer le comportement par defaut suite à cet evenement
//			dge.preventDefault();
//			trace("**ITEMEDITENDHANDLER**");
		}
		
		/* */
		private function itemEditBeginHandler(dge:DataGridEvent):void
		{
//			foc = focusManager.getFocus();
//			trace("******************"+foc+"*******************");
			// supprimer le comportement par defaut suite à cet evenement
			dge.preventDefault();
		}
		
//		/* */
//		override protected function indexToRow(index:int):int
//		{
//			var ind:int = super(index);
//			return ind
//		}
		
//		protected function customItemClickHandler(cle:CustomListEvent):void
//		{
//			this.selectedIndex = rowActuel;
//			cle.rowIndex;
//			trace("******CUSTOM_ITEM_CLICK****SELECTEDINDEX= "+rowActuel+" ************");
//			trace("*************ITEM_CLICK****************");
//		}

		/* */
		public function get listItemsArray():Array
		{
			return listItems;
		}
	}
}