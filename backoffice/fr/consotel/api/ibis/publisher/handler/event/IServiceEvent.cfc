<cfinterface displayName="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" hint="EVENT Interface (Pattern OBSERVER)">
	
	<!--- EVENT INTERFACE (Auteur : Cedric) --->
	
	<cffunction access="public" name="getEventTarget" returntype="String" hint="Returns EVENT_TARGET as STRING value. Check returned TYPE as this returntype can change">
	</cffunction>
	
	<cffunction access="public" name="getEventType" returntype="string" hint="Returns EVENT_TYPE as STRING value">
	</cffunction>
	
	<cffunction access="public" name="getEventDispatcher" returntype="string" hint="(Subject to modifications) Returns HTTP_HOST that sends (HTTP NOTIFICATION TARGET)">
	</cffunction>
	
</cfinterface>