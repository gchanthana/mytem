package vo.selector
{
	[Bindable]
	public class SelectorParametreVo
	{
		private var _libelle : String;
		private var _typeSelecteur:String;
		private var _typeParametre:Number;
		private var _cleJson:String;
		private var _defaultValue: String;
		private var _parametreId:Number;
		private var _description : String;
		private var _widgetParam_id :Number;
		
		private var _isMonoPeriode : Boolean;
		
		public function SelectorParametreVo(){}

		public function fill(selector:Object):void
		{
			this._libelle=selector.LIBELLE;
			this._typeSelecteur=selector.TYPE_SELECTEUR ;
			this._defaultValue=selector.DEFAULT_VALUE;
			this._cleJson=selector.CLE_JSON;			
			this._typeParametre=selector.TYPE_PARAMETRE;
			this._parametreId=selector.PARAMETREID;
			this._widgetParam_id=selector.WIDGET_PARAMID 
		}
		
		public function get libelle():String
		{
			return _libelle;
		}

		public function set libelle(value:String):void
		{
			_libelle = value;
		}

		public function get typeSelecteur():String
		{
			return _typeSelecteur;
		}

		public function set typeSelecteur(value:String):void
		{
			_typeSelecteur = value;
		}
		
		public function get typeParametre():Number
		{
			return _typeParametre;
		}
		
		public function set typeParametre(value:Number):void
		{
			_typeParametre = value;
		}	
		
		public function get defaultValue():String
		{
			return _defaultValue;
		}
		
		public function set defaultValue(value:String):void
		{
			_defaultValue = value;
		}
		public function get cleJson():String
		{
			return _cleJson;
		}
		
		public function set cleJson(value:String):void
		{
			_cleJson = value;
		}
		
		public function get parametreId():Number
		{
			return _parametreId;
		}
		
		public function set parametreId(value:Number):void
		{
			_parametreId = value;
		}
		public function get description():String
		{
			return _description;
		}
		
		public function set description(value:String):void
		{
			_description = value;
		}
		
		public function get widgetParam_id():Number
		{
			return _widgetParam_id;
		}
		
		public function set parametre_widget_id(value:Number):void
		{
			_widgetParam_id = value;
		}
	}
}