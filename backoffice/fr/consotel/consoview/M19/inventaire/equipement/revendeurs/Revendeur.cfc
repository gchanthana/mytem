<!--- =========================================================================
Classe: te
Auteur: brice.miramont
$Historique: $
$Version: 1.0 $
========================================================================== --->
<cfcomponent displayname="Revendeur" hint="Pour la Gestion des revendeurs" >
    <cfset Init()>
<!--- CONSTRUCTOR --->
	<cffunction name="Init" access="public" output="false" returntype="Revendeur" hint="Remplace le constructeur de Revendeur.">
		<cfscript>
			variables.instance = structNew();
			return this;
		</cfscript>	
	</cffunction>
<!--- METHODS --->

	<cffunction name="create_revendeur" access="public" returntype="numeric" output="false" hint="Pour l'ajout d'un revendeur pour la gestion de parc Fixe / Data ou a partir du module Gestion Parc Mobile " >
		
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="libelle de la societe du revendeur" />
		<cfargument name="idracine" required="false" type="numeric" default="" displayname="numeric idracine" hint="idgroupe_client de la racine du perimetre sur laquelle on se trouve" />
		<cfargument name="segment_mobile" required="false" type="numeric" default="" displayname="numeric segment_mobile" hint="1 si couvre le segment mobile, 0 sinon" />
		<cfargument name="segment_fixe_data" required="false" type="numeric" default="" displayname="numeric segment_fixe_data" hint="1 séil couvre le segment fixe/data, 0 sinon" />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.create_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment_mobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#segment_fixe_data#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
				
		<cfreturn p_result>
	</cffunction>
	
	<cffunction name="rename_revendeur" access="public" returntype="numeric" output="false" hint="Renommer un revendeur." >
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="Libelle du revendeur" />
		<cfargument name="idrevendeur" required="false" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer" />
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.rename_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		<cfreturn p_result>		
	</cffunction>	
	
	<cffunction name="edit_revendeur" access="public" returntype="numeric" output="false" hint="Renommer un revendeur." >
		<cfargument name="idrevendeur" required="true" type="numeric" default="" displayname="numeric idrevendeur" hint="ID du revendeur a renommer" />
		<cfargument name="libelle" required="true" type="string" default="" displayname="string libelle" hint="Libelle du revendeur" />
		<cfargument name="Idracine" required="true" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="segment_fixe" required="true" type="numeric" default="0"/> 
		<cfargument name="segment_mobile" required="true" type="numeric" default="0"/>		
		<cfargument name="idcde_contact_societe" required="true" type="numeric" default="0"/>
				
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.edit_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idrevendeur" value="#idrevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_libelle" value="#libelle#">			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_fixe" value="#segment_fixe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment_mobile" value="#segment_mobile#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcde_contact_societe" value="#idcde_contact_societe#">			
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		
		<cfreturn p_result>		
	</cffunction>
	
	
	<cffunction name="delete_revendeur" access="public" returntype="numeric" output="false" hint="Supprimer un revendeur, ses contacts et sa fiche.Attention aux codes retour de la procedure stockée." >
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.delete_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>	
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="get_all_revendeurs" access="public" returntype="query" output="false" hint="Lister lesconstructeurs, revendeurs, generiques et privés pour une racine donnée." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
		<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_all_revendeurs">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>
	
	<cffunction name="get_revendeurs_nonaffectes" access="public" returntype="query" output="false" hint="Lister les constructeurs, revendeurs, génériques et privés non affectés au catalogue fournisseur d'une racine donnée." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
		<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		
		<cfset p_segment= 0>
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_revendeurs_nonaffectes">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>
	</cffunction>	
	
	<cffunction name="get_catalogue_revendeur" access="public" returntype="query" output="false" hint="Lister revendeurs, génériques et privés pour une racine donnée. (Catalogue fournisseur)." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="chaine" required="false" type="string" default="" displayname="string chaine" hint="paramNotes" />
		<cfargument name="app_loginID" required="false" type="numeric" default="" displayname="numeric app_loginID" hint="paramNotes" />
		<cfargument name="p_segment" required="false" type="numeric" default="0" displayname="numeric p_segment" hint="Initial value for the p_segmentproperty." />
		
		<cfset p_segment= 0>
		
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.get_catalogue_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#chaine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#app_loginID#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_segment#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result>		
	</cffunction>

	<cffunction name="affecter_revendeur" access="public" returntype="numeric" output="false" hint="Ajouter un enregistrement au catalogue des revendeurs d'une racine (table CATALOGUE_REVENDEUR)." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.affecter_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>
		<cfreturn p_result>			
	</cffunction>
	
	<cffunction name="create_revendeurEtSociete" access="public" returntype="numeric" output="false" hint="Ajouter un enregistrement au catalogue des revendeurs d'une racine (table CATALOGUE_REVENDEUR)." >
	
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="libelle de la societe du revendeur" />
		<cfargument name="idracine" required="false" type="numeric" default="" displayname="numeric idracine" hint="idgroupe_client de la racine du perimetre sur laquelle on se trouve" />
		<cfargument name="segment_mobile" required="false" type="numeric" default="" displayname="numeric segment_mobile" hint="1 si couvre le segment mobile, 0 sinon" />
		<cfargument name="segment_fixe_data" required="false" type="numeric" default="" displayname="numeric segment_fixe_data" hint="1 séil couvre le segment fixe/data, 0 sinon" />
		
		<cfargument name="Raison_sociale" required="false" type="string" default="" displayname="string Raison_sociale" hint="Initial value for the Raison_socialeproperty." />
		<cfargument name="SIREN" required="false" type="string" default="" displayname="string SIREN" hint="Initial value for the SIRENproperty." />
		<cfargument name="ADRESSE1" required="false" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="false" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="false" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="false" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="false" type="numeric" default="1577" displayname="numeric PAYSCONSOTELID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="TELEPHONE" required="false" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="false" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="APP_LOGINID" required="false" type="numeric" default="" displayname="numeric APP_LOGINID" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="CODE_INTERNE" required="false" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="ID_SOCIETE_MERE" required="false" type="numeric" null="true" default="" displayname="numeric ID_SOCIETE_MERE" hint="Initial value for the ID_SOCIETE_MEREproperty." />
		
		<cfset revendeurId = -1>
		<cfset societeId = -1>
		<cfset retourEditRevendeur = -1>
		
		<!--- creation du revendeur --->		
		<cfset revendeurId = create_revendeur(libelle, idracine, segment_mobile, segment_fixe_data)>		
		<cfif revendeurId gt 0>
			<!--- creation de l'agence --->	
			<cfset societeId = createObject("component","fr.consotel.consoview.inventaire.equipement.revendeurs.FicheSociete").create_cde_contact_societe(idracine, Raison_sociale, SIREN, ADRESSE1, ADRESSE2, COMMUNE, ZIPCODE, PAYSCONSOTELID, TELEPHONE, FAX, APP_LOGINID, CODE_INTERNE, ID_SOCIETE_MERE)>
			<cfif societeId gt 0>
					<!--- maj de revendeur--->	
					<cfset retourEditRevendeur = edit_revendeur(revendeurId, libelle, idracine, segment_mobile, segment_fixe_data, societeId)>
					<cfif retourEditRevendeur lt 1>
						<cfreturn -3>	
					</cfif>	
			<cfelse>
				<cfreturn -2> 
			</cfif>				
		<cfelse>
			<cfreturn -1>
		</cfif>
		
		
		<cfreturn revendeurId>			
	</cffunction>
	
	<cffunction name="edit_revendeurEtSociete" access="public" returntype="numeric" output="false" hint="Ajouter un enregistrement au catalogue des revendeurs d'une racine (table CATALOGUE_REVENDEUR)." >
		<cfargument name="libelle" required="false" type="string" default="" displayname="string libelle" hint="libelle de la societe du revendeur" />
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfargument name="idracine" required="false" type="numeric" default="" displayname="numeric idracine" hint="idgroupe_client de la racine du perimetre sur laquelle on se trouve" />		
		
		<cfargument name="Idcde_contact_societe" required="false" type="numeric" default="" displayname="numeric Idcde_contact_societe" hint="Initial value for the Idcde_contact_societeproperty." />
		<cfargument name="SIREN" required="false" type="string" default="" displayname="string SIREN" hint="Initial value for the SIRENproperty." />
		<cfargument name="ADRESSE1" required="false" type="string" default="" displayname="string ADRESSE1" hint="Initial value for the ADRESSE1property." />
		<cfargument name="ADRESSE2" required="false" type="string" default="" displayname="string ADRESSE2" hint="Initial value for the ADRESSE2property." />
		<cfargument name="COMMUNE" required="false" type="string" default="" displayname="string COMMUNE" hint="Initial value for the COMMUNEproperty." />
		<cfargument name="ZIPCODE" required="false" type="string" default="" displayname="string ZIPCODE" hint="Initial value for the ZIPCODEproperty." />
		<cfargument name="PAYSCONSOTELID" required="false" type="numeric" default="1577" displayname="numeric PAYSCONSOTELID" hint=%qt%%paramNotes%%qt% />
		<cfargument name="TELEPHONE" required="false" type="string" default="" displayname="string TELEPHONE" hint="Initial value for the TELEPHONEproperty." />
		<cfargument name="FAX" required="false" type="string" default="" displayname="string FAX" hint="Initial value for the FAXproperty." />
		<cfargument name="USER_CREATE" required="false" type="numeric" default="" displayname="numeric USER_CREATE" hint="Initial value for the APP_LOGINIDproperty." />
		<cfargument name="CODE_INTERNE" required="false" type="string" default="" displayname="string CODE_INTERNE" hint="Initial value for the CODE_INTERNEproperty." />
		<cfargument name="ID_SOCIETE_MERE" required="false" type="numeric" default="" displayname="numeric ID_SOCIETE_MERE" hint="Initial value for the ID_SOCIETE_MEREproperty." />
		
		<cfset revendeurId = -1>
		<cfset revendeurId = rename_revendeur(libelle, Idrevendeur, idracine)>
		
		<cfif revendeurId gt 0>
			<cfset societeId = createObject("component","fr.consotel.consoview.inventaire.equipement.revendeurs.FicheSociete").edit_cde_contact_societe(Idcde_contact_societe, libelle, SIREN, ADRESSE1, ADRESSE2, COMMUNE, ZIPCODE, PAYSCONSOTELID, TELEPHONE, FAX, USER_CREATE, CODE_INTERNE, ID_SOCIETE_MERE)>
			
			<cfif societeId lt 0>
				<cfreturn -2> 
			</cfif>				
			
		<cfelse>
			<cfreturn -1>
		</cfif>	

		<cfreturn revendeurId>
		
	</cffunction>
	
	<cffunction name="desaffecter_revendeur" access="public" returntype="numeric" output="false" hint="Supprime un enregistrement de la table CATALOGUE_REVENDEUR." >
		<cfargument name="Idracine" required="false" type="numeric" default="" displayname="numeric Idracine" hint="Initial value for the Idracineproperty." />
		<cfargument name="Idrevendeur" required="false" type="numeric" default="" displayname="numeric Idrevendeur" hint="Initial value for the Idrevendeurproperty." />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_cv_gestion_fournisseur_v1.desaffecter_revendeur">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#Idracine#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idrevendeur#">
			<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_result">
		</cfstoredproc>		
		<cfreturn p_result>			
	</cffunction>	
	
	
	<cffunction name="getContactsRevendeur" access="public" returntype="query" output="false" hint="" >		
		<cfargument name="idrevendeur" required="true" type="numeric"/>
		<cfset user = Session.USER.CLIENTACCESSID>
		<cfset role = 101>
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_CV_GESTION_FOURNISSEUR_V1.GETCONTACTSROLESREVENDEUR">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#idrevendeur#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#user#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idoperation" value="#role#" >
			<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour>
	</cffunction>

<!--- GETTERS --->
<!--- SETTERS --->
<!--- INSTANCE METHODS --->
	<cffunction name="getInstance" access="public" returntype="struct" output="false">
		<cfreturn variables.instance />
	</cffunction>
	
	<cffunction name="setInstance" access="public" returntype="void" output="false">
		<cfargument name="newInstance" type="struct" required="yes" />
		<cfset variables.instance = arguments.newInstance />
	</cffunction>
</cfcomponent>