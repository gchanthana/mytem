package univers.parametres.parametrelogin.service
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import univers.parametres.parametrecompte.event.ChampPersoEvent;
	import univers.parametres.parametrelogin.vo.ChampsPersosVO;
	
	public class ChampsPersosService extends EventDispatcher
	{
		[Bindable] public var listeChampsPersosFixe:ArrayCollection = new ArrayCollection();
		[Bindable] public var listeChampsPersosReseau:ArrayCollection = new ArrayCollection();
		[Bindable] public var listeChampsPersosMobile:ArrayCollection = new ArrayCollection();
				
		public function ChampsPersosService()
		{
		}
		
	/* ************************************************************************************/
	/* **************************** LISTE DES CHAMPS PERSOS *******************************/
	/* ************************************************************************************/	
	// FIXE
		public function getListeChampsPersosFixe():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M21.m21service","getListeChampsPersos",
																		listeChampsPersosFixeResultHandler);
			RemoteObjectUtil.callService(op,0,1,0);
		}
		private function listeChampsPersosFixeResultHandler(e:ResultEvent):void
		{
			listeChampsPersosFixe = new ArrayCollection()
			if(e.result != null && e.result.length > 0)
			{
				var cp:ChampsPersosVO
				for each(var obj:Object in e.result)
				{
					cp= new ChampsPersosVO()
					cp.CP_Code_Champ = obj.CODE_ACTION 
					cp.CP_ID = obj.IDINV_ACTIONS
					if(obj.LIBELLE_ACTION_CLIENT == null || obj.LIBELLE_ACTION_CLIENT == " ")
						obj.LIBELLE_ACTION_CLIENT = ""
					cp.CP_Libelle_Client = obj.LIBELLE_ACTION_CLIENT
					cp.CP_Libelle_Client_Reference = cp.CP_Libelle_Client
					if(obj.LIBELLE_ACTION_CLIENT == null)
						obj.LIBELLE_ACTION_CLIENT = ""
					cp.CP_Libelle_Defaut = obj.LIBELLE_ACTION
					cp.CP_Type_Commande = obj.SEGMENT
					cp.ordreAffichage = obj.ORDRE_AFFICHAGE
					listeChampsPersosFixe.addItem(cp)
				}
			}
		}
	// RESEAU
		public function getListeChampsPersosReseau():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M21.m21service","getListeChampsPersos",
																		listeChampsPersosReseauResultHandler);
			RemoteObjectUtil.callService(op,0,0,1);
		}
		private function listeChampsPersosReseauResultHandler(e:ResultEvent):void
		{
			listeChampsPersosReseau = new ArrayCollection()
			if(e.result != null && e.result.length > 0)
			{
				var cp:ChampsPersosVO
				for each(var obj:Object in e.result)
				{
					cp= new ChampsPersosVO()
					cp.CP_Code_Champ = obj.CODE_ACTION 
					cp.CP_ID = obj.IDINV_ACTIONS
					if(obj.LIBELLE_ACTION_CLIENT == null)
						obj.LIBELLE_ACTION_CLIENT = ""
					cp.CP_Libelle_Client = obj.LIBELLE_ACTION_CLIENT
					cp.CP_Libelle_Client_Reference = cp.CP_Libelle_Client
					if(obj.LIBELLE_ACTION_CLIENT == null)
						obj.LIBELLE_ACTION_CLIENT = ""
					cp.CP_Libelle_Defaut = obj.LIBELLE_ACTION
					cp.CP_Type_Commande = obj.SEGMENT
					cp.ordreAffichage = obj.ORDRE_AFFICHAGE
					listeChampsPersosReseau.addItem(cp)
				}
			}
		}
	// MOBILE
		public function getListeChampsPersosMobile():void
		{
			var op : AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,"fr.consotel.consoview.M21.m21service","getListeChampsPersos",
																		listeChampsPersosMobileResultHandler);
			RemoteObjectUtil.callService(op,1,0,0);
		}
		private function listeChampsPersosMobileResultHandler(e:ResultEvent):void
		{
			listeChampsPersosMobile = new ArrayCollection()
			if(e.result != null && e.result.length > 0)
			{
				var cp:ChampsPersosVO
				for each(var obj:Object in e.result)
				{
					cp= new ChampsPersosVO()
					cp.CP_Code_Champ = obj.CODE_ACTION 
					cp.CP_ID = obj.IDINV_ACTIONS
					if(obj.LIBELLE_ACTION_CLIENT == null || obj.LIBELLE_ACTION_CLIENT == " ")
						obj.LIBELLE_ACTION_CLIENT = ""
					cp.CP_Libelle_Client = obj.LIBELLE_ACTION_CLIENT
					cp.CP_Libelle_Client_Reference = cp.CP_Libelle_Client
					if(obj.LIBELLE_ACTION_CLIENT == null)
						obj.LIBELLE_ACTION_CLIENT = ""
					cp.CP_Libelle_Defaut = obj.LIBELLE_ACTION	
					cp.CP_Type_Commande = obj.SEGMENT
					cp.ordreAffichage = obj.ORDRE_AFFICHAGE
					listeChampsPersosMobile.addItem(cp)
				}
			}
		}
	/* ************************************************************************************/
	/* ************************** SAUVEGARDE DES CHAMPS PERSOS ****************************/
	/* ************************************************************************************/	
		public function saveChampsPersos(listeIdaction:String,listeLibelle:String):void
		{
			var op : AbstractOperation = RemoteObjectUtil.getOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION, "fr.consotel.consoview.M21.m21service","SauvegarderChampPerso",
																		SauvegarderChampPersoResultHandler);
			RemoteObjectUtil.callService(op,listeIdaction,listeLibelle);
		}
		private function SauvegarderChampPersoResultHandler(e:ResultEvent):void
		{
			if(e.result != null && e.result > 0)
				dispatchEvent(new ChampPersoEvent(ChampPersoEvent.SAVE_PROFIL_EVENT));
		}
	}
}