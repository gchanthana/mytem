<cfcomponent author="Cedric" displayname="fr.consotel.api.mdm.partner.public.ZenpriseBase" extends="fr.consotel.api.mdm.partner.public.PublicAPI" hint="Implémentation de base">
	<!--- fr.consotel.api.mdm.partner.public.ZenpriseBase --->
	<cffunction access="public" name="getPublicZenprise" returntype="fr.consotel.api.mdm.partner.public.PublicAPI"
	hint="Les autres méthodes doivent etre appelées sur une instance retournée par cette méthode.
	Lorsque l'un des paramètres userLogin ou userPassword n'est pas renseigné alors les propriétés du WebService sont fonction de l'idRacine dans la session.
	Dans tous les cas le serveur Saaswedo est utilisé lorsque le serveur Zenprise à utiliser ne peut pas etre déterminé (e.g : idRacine vaut 0)">
		<cfargument name="userLogin" type="String" required="false" hint="Login Zenprise">
		<cfargument name="userPassword" type="String" required="false" hint="Mot de passe Zenprise. Obligatoire si userLogin est spécifié">
		<cfargument name="serviceEndpoint" type="String" required="false" hint="Endpoint du Web Service Zenprise e.g ZENPRISE">
		<cfargument name="idRacine" type="Numeric" required="false" default="-1" hint="Identifiant de la racine. Si ce paramètre est renseigné alors il sera utilisé pour identifier le serveur Zenprise">
		<cfset VARIABLES["PUBLIC_CREDENTIALS"]={DOMAIN="public.consotel.fr", PUBLIC_CLIENT=THIS}>
		<!--- Le serveur Zenprise à utiliser dépend de la valeur idRacine dans la session --->
		<cfset var idGroupe=isDefined("SESSION") AND structKeyExists(SESSION,"PERIMETRE") AND structKeyExists(SESSION.PERIMETRE,"ID_GROUPE") ? SESSION.PERIMETRE.ID_GROUPE:getDefaultIdGroupe()>
		<!--- Le serveur Zenprise à utiliser dépend de la valeur idRacine fournie en paramètre --->
		<cfset idGroupe=(ARGUMENTS["idRacine"] GT -1 ? ARGUMENTS["idRacine"]:idGroupe)>
		<cfset VARIABLES["PUBLIC_CREDENTIALS"]["IDGROUPE"]=idGroupe>
		<!--- Récupération à partir de la base des infos concernant le serveur Zenprise --->
		<cfset var serviceInfos=getMdmServiceInfos(idGroupe)>
		<cfif serviceInfos.recordcount GT 0>
			<cfset VARIABLES["PUBLIC_CREDENTIALS"]["LOGIN"]=ARGUMENTS["idRacine"] GT -1 ? serviceInfos["WS_LOGIN"][1] :
				structKeyExists(ARGUMENTS,"userLogin") AND (TRIM(ARGUMENTS["userLogin"]) NEQ "") ? ARGUMENTS["userLogin"]:serviceInfos["WS_LOGIN"][1]>
			<cfset VARIABLES["PUBLIC_CREDENTIALS"]["PWD"]=ARGUMENTS["idRacine"] GT -1 ? serviceInfos["WS_PWD"][1] :
				structKeyExists(ARGUMENTS,"userPassword") AND (TRIM(ARGUMENTS["userPassword"]) NEQ "") ? ARGUMENTS["userPassword"]:serviceInfos["WS_PWD"][1]>
			<cfset VARIABLES["PUBLIC_CREDENTIALS"]["WS_ENDPOINT"]=ARGUMENTS["idRacine"] GT -1 ? serviceInfos["ENDPOINT"][1] :
				structKeyExists(ARGUMENTS,"serviceEndpoint") AND (TRIM(ARGUMENTS["serviceEndpoint"]) NEQ "") ? ARGUMENTS["serviceEndpoint"]:serviceInfos["ENDPOINT"][1]>
			<cfset VARIABLES["PUBLIC_CREDENTIALS"]["ADMIN_EMAIL"]=ARGUMENTS["idRacine"] GT -1 ? serviceInfos["ADMIN_EMAIL"][1] :
				structKeyExists(ARGUMENTS,"serviceEndpoint") AND (TRIM(ARGUMENTS["serviceEndpoint"]) NEQ "") ? getDefaultAdminEmail():serviceInfos["ADMIN_EMAIL"][1]>
		<cfelse>
			<cfthrow type="Custom" message="Aucun paramétrage de WebService Zenprise pour l'idRacine #idGroupe#">
		</cfif>
		<cfreturn THIS>
	</cffunction>
	
	<cffunction access="package" name="getManagementService" returntype="fr.consotel.api.mdm.partner.private.ws.IManagementService" hint="Accès aux services Zenprise">
		<cfif NOT structKeyExists(VARIABLES,"MANAGEMENT_SERVICE")>
			<cfset VARIABLES["MANAGEMENT_SERVICE"]=getServiceProvider().getManagementService()>
		</cfif>
		<cfreturn VARIABLES["MANAGEMENT_SERVICE"]>
	</cffunction>
	
	<cffunction access="package" name="getMdmServiceInfos" returntype="Query" hint="Retourne les paramètres utilisés pour instancier et accéder au WebService Zenprise">
		<cfargument name="idRacine" type="numeric" default="0" hint="Valeur idRacine provenant de la session ou 0 pour retourner les paramètres par défaut">
		<cfset var idGroupe=ARGUMENTS["idRacine"]>
		<cfset var qMdmServiceInfos=queryNew("IDRACINE,ENDPOINT,WS_LOGIN,WS_PWD,ADMIN_EMAIL","Integer,VarChar,VarChar,VarChar,VarChar")>
		<cfset queryAddRow(qMdmServiceInfos,2)>
		<!--- MDM Server 1 --->
		<cfset querySetCell(qMdmServiceInfos, "IDRACINE",0,1)>
		<cfset querySetCell(qMdmServiceInfos, "ENDPOINT","http://mdm.consotel.fr/zdm/services/Version?WSDL",1)>
		<cfset querySetCell(qMdmServiceInfos, "WS_LOGIN","axel",1)>
		<cfset querySetCell(qMdmServiceInfos, "WS_PWD","consotel",1)>
		<cfset querySetCell(qMdmServiceInfos, "ADMIN_EMAIL","cedric.rapiera@consotel.fr",1)>
		<!--- MDM Server 2 --->
		<cfset querySetCell(qMdmServiceInfos, "IDRACINE",310812,2)>
		<cfset querySetCell(qMdmServiceInfos, "ENDPOINT","http://mdm.saaswedo.com/zdm/services/Version?WSDL",2)>
		<cfset querySetCell(qMdmServiceInfos, "WS_LOGIN","admin",2)>
		<cfset querySetCell(qMdmServiceInfos, "WS_PWD","zenprise",2)>
		<cfset querySetCell(qMdmServiceInfos, "ADMIN_EMAIL","cedric.rapiera@saaswedo.com",2)>
		<!--- Requete de test --->
		<cfquery name="qMdmServerInfos" dbtype="query">
			select	ENDPOINT, WS_LOGIN, WS_PWD, ADMIN_EMAIL
			from		qMdmServiceInfos
			where	IDRACINE =<cfqueryparam value="#(idGroupe EQ 310812 ? 310812:0)#" cfsqltype="cf_sql_integer">
		</cfquery>
		<cfset var mdmEndpoint=qMdmServerInfos["ENDPOINT"][1]>
		<cflog type="information" text="Mdm Service - idRacine #idGroupe# (Recordcount : #qMdmServerInfos.recordcount#): #mdmEndpoint#">
		<cfreturn qMdmServerInfos>
	</cffunction>

	<cffunction access="package" name="getDefaultAdminEmail" returntype="String" hint="Email par défaut de l'administrateur Zenprise">
		<cfreturn "cedric.rapiera@consotel.com">
	</cffunction>
	
	<cffunction access="package" name="getDefaultIdGroupe" returntype="Numeric" hint="idGroupe par défaut">
		<cfreturn 0>
	</cffunction>
</cfcomponent>