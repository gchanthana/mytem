package gestionparc.entity
{
	[Bindable]
	public class CommandeVO
	{

		public var idCommande:int=0;
		public var terminal:TerminalVO=null;

		public var pool:String="";
		public var numCommande:String="";
		public var refOperateur:String="";
		public var libelle:String="";
		public var refClient:String="";
		public var distributeur:String="";
		public var prix:Number=0;
		public var dateAchat:Date=null;
		public var dateLivraison:Date=null;
		public var idSiteLivraison:int=0;
		public var refLivraison:String="";
		public var commentaires:String="";
		public var dateDebut:Date=null;
		public var duree:String="";

	}
}
