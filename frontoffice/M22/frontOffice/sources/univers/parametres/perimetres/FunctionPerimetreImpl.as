package univers.parametres.perimetres
{
	import mx.containers.Box;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;

	public class FunctionPerimetreImpl extends Box
	{
		public var cpPerimetre : Perimetres;
		
		public function FunctionPerimetreImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		protected  function afterCreationComplete(event:FlexEvent):void {
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
		}
		
		public  function getUniversKey():String {
			return "STRUCT";
		}
		
		public  function getFunctionKey():String {
			return "STRUCT_ORG";
		}
		
		public  function afterPerimetreUpdated():void {
			cpPerimetre.onPerimetreChange();
			trace("(FunctionListeAppels) Perform After Perimetre Updated Tasks");
			trace("(FunctionListeAppels) SESSION FLEX :\n" +
						ObjectUtil.toString(CvAccessManager.getSession()));
		}
		
	}
}