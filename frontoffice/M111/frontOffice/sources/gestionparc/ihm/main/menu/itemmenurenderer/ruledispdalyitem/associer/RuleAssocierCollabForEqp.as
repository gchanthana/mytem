package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.associer
{
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic.HasCollaborateurAssociated;

	public class RuleAssocierCollabForEqp implements IRuleDisplayItem
	{
		public function RuleAssocierCollabForEqp()
		{
		}
		public function isDisplay():Boolean
		{
			var r1:IRuleDisplayItem = new HasCollaborateurAssociated();
			if(!r1.isDisplay())
				return true;
			else
				return false;
		}
	}
}