<cfcomponent name="procedureGestionGroupeGroupeRacine" displayname="Gestion des groupes racine dans le module Gestion des groupes" output="false">
<cffunction name="getGroupe" returntype="query" output="false"access="remote">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getGroupe_V2">
		<cfprocresult name="result"/>
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="getGroupeMaster" returntype="query" output="false"access="remote">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.getGroupeMaster">
		<cfprocresult name="result"/>
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="addGroupeMaitre" returntype="struct" output="false"access="remote">
	<cfargument name="libelleGroupe" type="string">
	<cfargument name="Commentaire" type="String">
	<cfargument name="idMaster" type="Numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.addGroupeMaitre">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idMaster#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelleGroupe#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#Commentaire#">
		
		<cfprocparam cfsqltype="CF_SQL_INTEGER"  type="out" variable="result"/>
	</cfstoredproc>
	
	<cfif result gt -1>
		<cfstoredproc datasource="#session.offredsn#" procedure="Pkg_cv_grcl_facturation_v3.addDefaultRules">
			<cfprocparam value="#result#" cfsqltype="CF_SQL_INTEGER" type="in">
			<cfprocparam variable="p_result" cfsqltype="CF_SQL_INTEGER" type="out">
		</cfstoredproc>
	</cfif>
		
	<cfset obj= StructNew()>
	<cfset obj.resultAddMaitre = result>
	<cfset obj.resultDefautProfil = p_result>
	<cfreturn obj>
</cffunction>
<cffunction name="update_racine" returntype="Numeric" output="false"access="remote">
	<cfargument name="idgroupe_client" type="Numeric">
	<cfargument name="libelle_groupe_client" type="String">
	<cfargument name="commentaires" type="String">
	<cfargument name="idMaster" type="Numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.update_racine">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idMaster#">
		<cfprocparam cfsqltype="CF_SQL_INTEGER" value="#idgroupe_client#">
		<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#libelle_groupe_client#">
		<cfprocparam cfsqltype="CF_SQL_VARCHAR" value="#commentaires#">
		
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result">
	</cfstoredproc>
<cfreturn result>
</cffunction>
<cffunction name="deleteGroupeClient" returntype="Numeric" output="false"access="remote">
	<cfargument name="idgroupe_client" type="Numeric">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_intranet.delete_racine">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#idgroupe_client#">
		<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="result">
	</cfstoredproc>
<cfreturn result>
</cffunction>
</cfcomponent>
