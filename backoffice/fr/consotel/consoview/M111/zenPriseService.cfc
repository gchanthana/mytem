﻿<cfcomponent name="zenPriseService" output="false" >
	<!---<cfset VARIABLES["ApiZenObj"]=createObject("component", "fr.consotel.consoview.api.Zenprise.ApiZenPrise")>--->
	
	<cffunction access="remote" name="getAllDevices" returntype="Array">
		<cfset arr=arrayNew(1)>
		
		<cfset res=ApiZenObj.callMethod("getAllDevices",arr)>
		
		<cfif res[1] neq 'FALSE'>
			<cfset listDevice=arrayNew(1)>
			<cfloop from="1" to="#arraylen(res)#" index="idx">
				<cfset device = getDeviceInfo(res[IDX].getImei(),res[IDX].getSerialNumber())>
				<cfset obj= structNew()>
				<cfset obj.DEVICE = device>
				<cfset obj.IMEI = res[IDX].getImei()>
				<cfset obj.SERIALNUMBER = res[IDX].getSerialNumber()>
				<cfset obj.STRONGID = res[IDX].getStrongID()>
				<cfset obj.TYPEDESC = res[IDX].getTypeDesc()>
				<cfset obj.HASHCODE = res[IDX].hashCode()>
				<cfset arrayAppend(listDevice,#obj#)>
			</cfloop>
		</cfif>
		<cfdump var="#res#" label="RES">
		<cfdump var="#listDevice#" label="LISTE DEVICE">
		<cfreturn listDevice>
	</cffunction>
	
	<cffunction access="remote" name="lockTerm" returntype="Any">
		<cfargument name="IMEI" type="string">
		<cfargument name="SERIAL" type="string" >
		<cfargument name="PIN" type="string" default="0000">
		<cfargument name="WAIT" type="numeric" default="30000" >
		<cfargument name="Api" type="string" default="ZenPrise">
		
		<cfset api = createObject("component", "fr.consotel.consoview.api.mdm.Api#arguments.Api#") >
		<cfset result = api.lockTerm(arguments.IMEI,arguments.SERIAL,arguments.PIN,arguments.WAIT)>
		<cfreturn result>
	</cffunction>
	
	<cffunction access="remote" name="corporateDataWipeDevice" returntype="Any">
		<cfargument name="IMEI" type="string">
		<cfargument name="SERIAL" type="string" >
		<cfargument name="WAIT" type="numeric" default="30000" >
		<cfargument name="Api" type="string" default="ZenPrise">
		<cfargument name="PIN" type="string" default="0000">
		
		<cfset api = createObject("component", "fr.consotel.consoview.api.mdm.Api#arguments.Api#") >
		<cfset result = api.corporateDataWipeDevice(arguments.IMEI,arguments.SERIAL,arguments.PIN,arguments.WAIT)>
		<cfreturn result>
	</cffunction>
	
	<cffunction access="remote" name="wipeDevice" returntype="Any">
		<cfargument name="IMEI" type="string">
		<cfargument name="SERIAL" type="string">
		<cfargument name="ERASEDMEMOMYCARD" type="boolean" default="false">
		<cfargument name="WAIT" type="numeric" default="30000" >
		<cfargument name="Api" type="string" default="ZenPrise">
		
		<cfset api = createObject("component", "fr.consotel.consoview.api.mdm.Api#arguments.Api#") >
		<cfset result = api.wipeDevice("01 181100 292510 2","8291913YY7H",arguments.ERASEDMEMOMYCARD,arguments.WAIT)>		
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction access="remote" name="getDeviceInfoFromIMEI" returntype="Struct">
		<cfargument name="IMEI" type="string">
		<!--- TODO : Si cette méthode n'est plus d'utilité alors décommenter ceci pour provoquer une erreur si elle est appelée par erreur --->
		<cfthrow type="Custom" message="Cette méthode est obsolète et ne doit plus etre appelée">
	</cffunction>
	
	
<!--- On recupère les informations d'un device à partir d'un IMEI et d'un numéro de série --->	
	<cffunction access="private" name="getDeviceInfo" returntype="Any" >
		<cfargument name="IMEI" type="string">
		<cfargument name="SERIALNUMBER" type="string">
		<cfset deviceError= -1>
		<cftry>
			<cfset arrArgs= arrayNew(1)>
			<cfset strSerialNumber = structNew()>
			<cfif structKeyExists(arguments,"SERIALNUMBER")>
				<cfset strSerialNumber.VALUE = arguments.SERIALNUMBER>
			<cfelse>
				
			</cfif>
			<cfset strIMEI = structNew()>
			<cfif  structKeyExists(arguments,"IMEI")>
				<cfset strIMEI.VALUE = arguments.IMEI>
			<cfelse>
				<cfset strIMEI.VALUE = 0>
			</cfif>
			<cfset arrArgs[1]= strSerialNumber>
			<cfset arrArgs[2]= strIMEI>
			<cfif strIMEI.VALUE neq 0>
				<cfset device = ApiZenObj.callMethod("getDeviceInfo",arrArgs)>
			<cfelse>
				<cfreturn deviceError>
			</cfif>
		<cfcatch type="any">
			<cflog text="#cfcatch.Message#">
			<cfreturn deviceError>
		</cfcatch> 	
		</cftry>
		<cfreturn device>
	</cffunction>
	
	
	<cffunction access="remote" name="enrollDeviceAndLogIt" returntype="Any" hint="Procédure d'inscription du Device au MDM">
		<cfargument name="IDSOUS_TETE" type="numeric" hint="l idendtifiant de la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="SOUS_TETE" type="string" hint="le numéro la sous - tete (ligne)) vers laquelle sera envoyé le SMS d'inscription" required="true" >
		<cfargument name="cvIDTERMINAL" type="numeric" hint="le numéro IMEI du Device" required="true" >
		<cfargument name="cvIMEI" type="string" hint="le numéro IMEI du Device" required="true" >
		<cfargument name="OSCode" type="string" hint="l'os du terminal" required="true" >
		
		<cfset deviceEnrollmentProcess = createObject("component","fr.consotel.consoview.M111.enrollmentprocess.OSEnrollmentStrategy")>
		<cfset deviceEnrollmentProcess.setStrategy(OSCode)>
		<cfset deviceEnrollmentProcess.initProcess(IDSOUS_TETE,SOUS_TETE,cvIDTERMINAL,cvIMEI)>
		
		<!--- LOGS ---> 	
		<cfset uuid = createUUID()>
		<cfset today = now()>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
		
		<cftry>
			<cfset logResult = logger.logAction(27,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today)>
			<cfset logResult = 1>
		<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result = logger.sendMailAction(27,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today,ERROR)>
			<cfset logResult = -1>
		</cfcatch>
		</cftry>
						
		
		
		<cfreturn  logResult>
	</cffunction>
	
	
	<cffunction access="remote" name="wypeTermAndLogIt" returntype="Any">		
		<cfargument name="cvIDTERMINAL" type="numeric" >
		<cfargument name="cvIMEI" type="numeric">
		<cfargument name="zdmIMEI" type="string">
		<cfargument name="zdmSERIAL" type="string" >
		<cfargument name="ERASEDMEMOMYCARD" type="boolean" default="false">
		<cfargument name="WAIT" type="numeric" default="30000" >
		<cfargument name="Api" type="string" default="ZenPrise">
		
		<cfset api = createObject("component", "fr.consotel.consoview.api.mdm.Api#arguments.Api#")>
		<cfset result = api.wipeDevice(arguments.zdmIMEI,arguments.zdmSERIAL,arguments.ERASEDMEMOMYCARD,arguments.WAIT)>
		
		<!--- LOGS --->
		<cfset uuid = createUUID()>
		<cfset today = now()>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
		
		<cftry>
			<cfset logResult = logger.logAction(25,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today)>
			<cfset logResult = 1>
		<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result = logger.sendMailAction(25,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today,ERROR)>
			<cfset logResult = -1>
		</cfcatch>
		</cftry>
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction access="remote" name="wypeDataTermAndLogIt" returntype="Any">
		<cfargument name="cvIDTERMINAL" type="numeric" >
		<cfargument name="cvIMEI" type="numeric">
		<cfargument name="zdmIMEI" type="string">
		<cfargument name="zdmSERIAL" type="string" >
		
		<cfargument name="PIN" type="string" default="0000">
		<cfargument name="WAIT" type="numeric" default="30000" >
		<cfargument name="Api" type="string" default="ZenPrise">
		
		<cfset api = createObject("component", "fr.consotel.consoview.api.mdm.Api#arguments.Api#") >
		<cfset result = api.corporateDataWipeDevice(arguments.zdmIMEI,arguments.zdmSERIAL,arguments.PIN,arguments.WAIT)>
				
		<!--- LOGS --->
		<cfset uuid = createUUID()>
		<cfset today = now()>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
		<cftry>
			<cfset logResult = logger.logAction(26,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today)>
			<cfset logResult = 1>
		<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result = logger.sendMailAction(26,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today,ERROR)>
			<cfset logResult = -1>
		</cfcatch>
		</cftry>
		<cfreturn result>
	</cffunction>
	
	
	
	<cffunction access="remote" name="lockTermAndLogIt" returntype="Any">
		<cfargument name="cvIDTERMINAL" type="numeric" >
		<cfargument name="cvIMEI" type="string" >
		<cfargument name="zdmIMEI" type="string">
		<cfargument name="zdmSERIAL" type="string" >
		
		<cfargument name="PIN" type="string" default="0000">
		<cfargument name="WAIT" type="numeric" default="30000" >
		<cfargument name="Api" type="string" default="ZenPrise">
			
		<cflog text="lockTermAndLogIt">
						
		<cfset api = createObject("component", "fr.consotel.consoview.api.mdm.Api#arguments.Api#") >
		<cfset result = api.lockTerm(arguments.zdmIMEI,arguments.zdmSERIAL,arguments.PIN,arguments.WAIT)>
		
		<!--- LOGS --->
		<cfset uuid = createUUID()>
		<cfset today = now()>
		<cfset logger = createObject('component','fr.consotel.consoview.M111.Logger')>
		<cftry>
			<cfset logResult = logger.logAction(24,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today)>
		<cfcatch type="any" >
				<cfset ERROR = logger.setError(cfcatch.detail,cfcatch.errorcode,cfcatch.message,cfcatch.type)>
				<cfset result = logger.sendMailAction(24,1,cvIMEI,"",cvIDTERMINAL,-1,-1,-1,uuid,today,ERROR)>
			<cfset logResult = -1>
		</cfcatch>
		</cftry>
		<cflog text="lockTermAndLogIt fin">
		
		<cfreturn result>
	</cffunction>
	
</cfcomponent>