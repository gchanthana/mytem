package general.event
{
	import flash.events.Event;
	
	public class UtilM00Event extends Event
	{
		public static const FINDCOOKIE_EVENT:String = "FINDCOOKIE_EVENT";
		
		public function UtilM00Event(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}