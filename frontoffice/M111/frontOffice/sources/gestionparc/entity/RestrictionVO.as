package gestionparc.entity
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class RestrictionVO
	{
		private var _LIBELLE:String;
		private var _DATA:int;
		private var _isFacturation:Boolean;
		private var _ISTITLE:Boolean;
		private var _SAISIE:Boolean;
		private var _VALUE:Boolean;
		private var _LISTE_VALUE:ArrayCollection;
		private var _ENABLED:Boolean=false;
		private var _ISRESTRICTIONLIGNE:Boolean;
		private var _ISRESTRICTIONSIM:Boolean;
		private var _ISRESTRICTIONEQUIPEMENT:Boolean;
		private var _ISRESTRICTIONCOLLABORATEUR:Boolean;
		private var _ISRESTRICTIONCONTRAT:Boolean;
		private var _ISCONSOMMATION:Boolean;

		public function RestrictionVO()
		{
		}

		public function get LIBELLE():String
		{
			return _LIBELLE;
		}

		public function get DATA():int
		{
			return _DATA;
		}
		public function get ISFACTURATION():Boolean
		{
			return _isFacturation;
		}

		public function get ISTITLE():Boolean
		{
			return _ISTITLE;
		}

		public function get SAISIE():Boolean
		{
			return _SAISIE;
		}

		public function get VALUE():Boolean
		{
			return _VALUE;
		}

		public function get LISTE_VALUE():ArrayCollection
		{
			return _LISTE_VALUE;
		}

		public function set LIBELLE(value:String):void
		{
			_LIBELLE=value;
		}

		public function set DATA(value:int):void
		{
			_DATA=value;
		}

		public function set ISFACTURATION(value:Boolean):void
		{
			_isFacturation=value;
		}

		public function set ISTITLE(value:Boolean):void
		{
			_ISTITLE=value;
		}

		public function set SAISIE(value:Boolean):void
		{
			_SAISIE=value;
		}

		public function set VALUE(value:Boolean):void
		{
			_VALUE=value;
		}

		public function set LISTE_VALUE(value:ArrayCollection):void
		{
			_LISTE_VALUE=value;
		}

		public function get ENABLED():Boolean
		{
			return _ENABLED;
		}

		public function set ENABLED(value:Boolean):void
		{
			_ENABLED=value;
		}

		public function get ISRESTRICTIONCONTRAT():Boolean
		{
			return _ISRESTRICTIONCONTRAT;
		}

		public function set ISRESTRICTIONCONTRAT(value:Boolean):void
		{
			_ISRESTRICTIONCONTRAT=value;
		}

		public function get ISRESTRICTIONCOLLABORATEUR():Boolean
		{
			return _ISRESTRICTIONCOLLABORATEUR;
		}

		public function set ISRESTRICTIONCOLLABORATEUR(value:Boolean):void
		{
			_ISRESTRICTIONCOLLABORATEUR=value;
		}

		public function get ISRESTRICTIONEQUIPEMENT():Boolean
		{
			return _ISRESTRICTIONEQUIPEMENT;
		}

		public function set ISRESTRICTIONEQUIPEMENT(value:Boolean):void
		{
			_ISRESTRICTIONEQUIPEMENT=value;
		}

		public function get ISRESTRICTIONSIM():Boolean
		{
			return _ISRESTRICTIONSIM;
		}

		public function set ISRESTRICTIONSIM(value:Boolean):void
		{
			_ISRESTRICTIONSIM=value;
		}

		public function get ISRESTRICTIONLIGNE():Boolean
		{
			return _ISRESTRICTIONLIGNE;
		}

		public function set ISRESTRICTIONLIGNE(value:Boolean):void
		{
			_ISRESTRICTIONLIGNE=value;
		}

		public function get ISCONSOMMATION():Boolean
		{
			return _ISCONSOMMATION;
		}

		public function set ISCONSOMMATION(value:Boolean):void
		{
			_ISCONSOMMATION=value;
		}
	}
}
