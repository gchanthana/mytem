package gestionparc.ihm.question.ligne
{
	import flash.display.Sprite;
	
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.resources.ResourceManager;
	
	import composants.util.ConsoviewAlert;
	import composants.util.CvDateChooser;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.ihm.fiches.listedispo.DispoSimMobile;
	import gestionparc.ihm.fiches.listedispo.IListeDispo;
	import gestionparc.services.sim.SimServices;

	public class QuestionFournirSimStock
	{
		private var calendar		:CvDateChooser;
		private var liste			:IListeDispo;
		private var objetCreateSIM	:Object;
		//private var objectEmail		:Object;
		public var serv:SimServices=new SimServices();
		
		public function QuestionFournirSimStock(dg_liste:IListeDispo, cal:CvDateChooser, /*objectEmail:Object = null,*/ objetCreateSIM:Object = null)
		{
			this.liste=dg_liste;
			this.calendar=cal;
			this.objetCreateSIM = objetCreateSIM;
			//this.objectEmail = objectEmail;
			
			Alert.show(ResourceManager.getInstance().getString("M111", "QUESTION_FICHESIMECHANGER"), 
						ResourceManager.getInstance().getString('M111', 'confirmation'), Alert.YES | Alert.CANCEL, Application.application as Sprite, 
						qnClose);
		}
		
		/* LIGNE MOBILE */
		private function qnClose(evt:CloseEvent):void
		{
			if (evt.detail == Alert.YES)
				echangerSim2();
			else if (evt.detail == Alert.NO)
				echangerSim1();
			else
				;
		}
		
		private function echangerSim1():void
		{
			serv.echangerSim1(setObj())
		}
		
		private function echangerSim2():void
		{
			if((liste as DispoSimMobile).ajoutSimHorsParc == true)
			{
				var idOldeqp:Number = SpecificationVO.getInstance().elementDataGridSelected.ID;
				serv.echangerSim2Manuellement(setObj(), objetCreateSIM);
			}
			
			else
				serv.echangerSim2(setObj());
		}
		
		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idOldeqp=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.idNeweqp=liste.getSelection().ID;
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.S_IMEI;
			obj.ITEM2=liste.getSelection().NUM_SIM;
			obj.ID_TERMINAL=0;
			obj.ID_SIM1=SpecificationVO.getInstance().elementDataGridSelected.IDSIM;
			obj.ID_SIM2=parseInt(liste.getSelection().IDEQUIPEMENT);
			obj.ID_EMPLOYE=SpecificationVO.getInstance().elementDataGridSelected.IDEMPLOYE;
			obj.IDSOUS_TETE=0;
			obj.DATE_EFFET=DateField.dateToString(calendar.selectedDate, 'YYYY/MM/DD');
			obj.IS_PRETER=0;
			return obj;
		}
	}
}