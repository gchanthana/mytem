<cfcomponent extends="AbstractConnectToApp">
<!--- CONNECTION A MYTEM PAR LOGIN / MDP --->

<!---- CHAQUE FONCTION GERE CES ERREURS ET RETOURNE UN OBJECT ERROR AVEC UN CODEERREUR
	   IL EST POSSIBLE DE PRECISER LE TYPE, LE MESSAGE ET LE DETAIL
	   <cfreturn ThrowCfCatchError(CODEERREUR,TYPE,MESSAGE,DETAIL)>
	   codeerreur : int, type : string, message : string, detail  : string --->
	<cfset THIS.codeLangue = createObject("component","fr.consotel.consoview.access.CodeLangue")>
	<cfset THIS.codeLangue.CODE = "default">
	<cfset THIS.codeLangue.ID_LANGUE = "default">
	<cfset THIS.codeLangue.DEVISE = "-">
	<cfset THIS.codeLangue.LANGUE = "n.c.">
	<cfset THIS.jwtService = "http://backoffice-10.swd-internal.com/LAB/helper/Saml.cfc?wsdl">

	<cffunction name="connectTo"
				displayname="ConnectToApp"
				description="Permet de se logger à une application"
				access="public"
				output="false"
				returntype="struct">
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />


		<cfif len(arguments.objConnection.pwd) gt 0 >
			<cfset arg = {}>
			<cfset arg.token = arguments.objConnection.pwd>

			<cfinvoke webservice = "#THIS.jwtService#" method = "verifyToken" returnVariable = "result" argumentCollection = "#arg#">
			
			<cflog text="CONNECTION SP jwt ---> 61 #SerializeJSON(result)# code app = #SESSION.CODEAPPLICATION#">
			<cflog text="CONNECTION SP jwt ---> 61 #SerializeJSON(arguments.objConnection)# code app = #SESSION.CODEAPPLICATION#">
			<cfset objConnection.pwd = result.token>
			<cfset objConnection.method = "token">
			<cflog text="CONNECTION SP jwt ---> 61 #SerializeJSON(objConnection.pwd)# code app = #SESSION.CODEAPPLICATION#">
			<cfset res = super.connectTo(objConnection)>
			<cfreturn res>
		</cfif>
		
		<cfabort showerror="connection failed">
	</cffunction>

	<!--- CREATION DE L'OBJET USER DE LA SESSION --->		
	<cffunction name="validateLogin" access="private" output="false" returntype="Any" >
		<cfargument name="objConnection" required="true" type="struct" >
		<cflog text="connection61.validateLogin(#SerializeJSON(objConnection)#)">
		<cftry>
			<cfif StructKeyExists(SESSION, "IMPERSONNIFICATION")>
				<cfif SESSION.IMPERSONNIFICATION eq FALSE>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">
				</cfif>
				<cfelse>
					<cfcookie name="LAST_LOGIN" value="#objConnection.login#" expires="never">
			 </cfif>

			<cfif structkeyExists(arguments.objConnection,"method") and compareNoCase(arguments.objConnection.method,"token") eq 0>
				
					<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="PKG_CV_GLOBAL.connectClient_v6">
						<cfprocparam type="in" value="#objConnection.pwd#" cfsqltype="CF_SQL_VARCHAR" >
						<cfprocparam type="in" value="#SESSION.CODEAPPLICATION#" 	cfsqltype="CF_SQL_INTEGER" >
						<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
						<cfprocresult name="qValidateLogin">
					</cfstoredproc>
					<cflog text="CONNECTION SP jwt PKG_CV_GLOBAL.connectClient_v6---> 61 retour #p_retour# result : #SerializeJSON(qValidateLogin)# code app = #SESSION.CODEAPPLICATION#">

					<cfelse>	
					
					<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.CONNECT_CLIENT#">
						<cfprocparam type="in" value="#objConnection.login#" cfsqltype="CF_SQL_VARCHAR" >
						<cfprocparam type="in" value="#objConnection.pwd#" 	cfsqltype="CF_SQL_VARCHAR" >
						<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
						<cfprocresult name="qValidateLogin">
					</cfstoredproc>
					<cflog text="CONNECTION #THIS.CONNECT_CLIENT# ---> 61 retour #p_retour# result : #SerializeJSON(qValidateLogin)# code app = #SESSION.CODEAPPLICATION#">

			</cfif>

		
			<cfif p_retour eq 1>
				<cfif qValidateLogin.recordcount EQ 1>
					<cfset boolTokenHasToBeSaved = 0>
					<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
					<cfif boolCheckIP EQ 1>
						<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
						<cfif checkIpResult LT 1>
							<cfreturn ThrowCfCatchError(444)>
						<cfelse>
							<cfif setupSessionUser(qValidateLogin)>
								<cfif objConnection.seSouvenir eq 1>
									<cfset boolTokenHasToBeSaved = 1>
								</cfif>
							<cfelse>
								<cfreturn ThrowCfCatchError(444)>
							</cfif>
							<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
						</cfif>
					<cfelse>
						<cfif setupSessionUser(qValidateLogin)>
							<cfif objConnection.seSouvenir eq 1>
								<cfset boolTokenHasToBeSaved = 1>
							</cfif>
						<cfelse>
							<cfreturn ThrowCfCatchError(444)>
						</cfif>
					</cfif>
			<!--- 11/07/2012 : AJOUT SSO SE SOUVENIR  --->	
					<cfif boolTokenHasToBeSaved eq 1>
						<cfset result = defineToken(objConnection)>
						 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
							<cfreturn result>
						<cfelse>
							<cfif result eq 0>
								<cfreturn ThrowCfCatchError(444)>
							<cfelse>
								<cfreturn 1>
							</cfif>
						</cfif>
					<cfelse>	
						<cfreturn 1>
					</cfif>	
				<cfelse>
					<cfreturn ThrowCfCatchError(444)>
				</cfif>
			<cfelse>
				<cflog text="echec de la validation du token #p_retour#">
				<cfreturn ThrowCfCatchError(444)>				
			</cfif>
		<cfcatch type="any" >
			<cfreturn ThrowCfCatchError(444,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>	
	</cffunction>


	<cffunction name="ChangeGroupe"
				displayname="ChangeGroupe"
				description="Gère le changement de Groupe de la personne connectée"
				access="public"
				output="false"
				returntype="struct">

		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />

		<cflog text="connection6.ChangeGroupe(objConnection.idperimetre = #objConnection.idperimetre#)">

		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfset result = connectOnGroup(objConnection.idperimetre)>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETPERIMETRE : définit le périmètre --->
				<cfset result = setPerimetre(SESSION.PERIMETRE.ID_GROUPE)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
					<cfset result = setXMLAccess()>
					<cfif isDefined("result") AND isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					 </cfif>
				</cfif>
			</cfif>
			<cfset str= structNew()>
			<cfset str.xml_perimetre = SESSION.XML_PERIMETRE>
			<cfset str.perimetre = SESSION.PERIMETRE>
			 <cfset str.xml_access = SESSION.XML_ACCESS>
			<cfreturn str>
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(444,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="ChangePerimetre"
				displayname="ChangePerimetre"
				description="Gère le changement de périmètre de la personne connectée"
				access="public"
				output="false"
				returntype="struct">

		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />

		<cflog text="connection6.ChangePerimetre(objConnection.idgroupe = #objConnection.idgroupe#, objConnection.idperimetre=#objConnection.idperimetre#)">

		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfif objConnection.idgroupe neq SESSION.PERIMETRE.ID_GROUPE>
				<cfset result = connectOnGroup(objConnection.idgroupe)>
			</cfif>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETPERIMETRE : définit le périmètre --->
				<cfset result = setChangePerimetre(objConnection.idperimetre)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
					<cfset result = setXMLAccess()>
					<cfif isDefined("result") AND isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					 </cfif>
				</cfif>
			</cfif>
			<cfset str= structNew()>
			<cfset str.perimetre = SESSION.PERIMETRE>
			 <cfset str.xml_access = SESSION.XML_ACCESS>
			<cfreturn str>
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(444,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction name="setXMLAccess"  access="private" output="false" returntype="Any" >
		<cftry>
			<!--- --->
			<!--- DEFINITION DU NIVEAU D'ORGA DONT ON VEUT CONNAITRE LES ACCES --->
			<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_GROUPE>
			<cfif structkeyExists(SESSION.PERIMETRE,"ID_PERIMETRE")>
				<cfset _IDGROUPE_CLIENT = SESSION.PERIMETRE.ID_PERIMETRE>
			</cfif>

			<cflog text="connection6.setXMLAccess - #SESSION.USER.CLIENTACCESSID# - #SESSION.PERIMETRE.ID_GROUPE# - #SESSION.PERIMETRE.ID_PERIMETRE#">

			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="PKG_M00.getListeModuleClient_V4">
				<cfprocparam value="#SESSION.CODEAPPLICATION#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.USER.CLIENTACCESSID#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.PERIMETRE.ID_GROUPE#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#SESSION.USER.IDGLOBALIZATION#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocparam value="#_IDGROUPE_CLIENT#"	type="in"	cfsqltype="CF_SQL_INTEGER">
				<cfprocresult name="qResult">
			</cfstoredproc>
			<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="PKG_M00.getDefaultModule">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.CLIENTACCESSID#">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#SESSION.PERIMETRE.ID_PERIMETRE#">
				<cfprocresult name="qDefaultModule">
			</cfstoredproc>

			<cflog text="Module par défaut : #qDefaultModule.KEY#">

			<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
			<cfset fileName = currentPath & "/menus/Menu_Univers_" & SESSION.USER.GLOBALIZATION & ".xml">
			<cffile action="read" file="#fileName#" variable="XMLFileText">
			<cfset xmlAccess=XmlParse(XMLFileText)>

			<cfset idxHOME = 1>
			<cfset idxGEST_CONT = 1>
			<cfset idxFACT_REPORT_E0 = 1>
			<cfset idxFACT_REPORT_360 = 1>
			<cfset idxFACT = 1>
			<cfset idxGEST = 1>
			<cfset idxUSG = 1>
			<cfset idxSTRUCT = 1>
			<cfset loopindex= 1>
			<cfset idxUnivers = 1>
			<cfset idx = 0>

			<cfloop query="qResult">
				<cfset idx = Evaluate("idx#qResult.UNIVERS#")>

				<!--- On trouve l'index de l'univers  --->
				<cfloop from="1" to="#ArrayLen(xmlAccess.MENU.XmlChildren)#" index="i">
					<cfif xmlAccess.MENU.xmlChildren[i].xmlAttributes.KEY eq qResult.UNIVERS>
						<cfset idxUnivers = i>
						<cfbreak>
					</cfif>
				</cfloop>
				<!--- On précise que cet univers est visible par l'utilisateur --->
				<cfif qResult.ACCES_USER gt 0>
					<cfset xmlAccess.MENU.xmlChildren[idxUnivers].XmlAttributes.USR= 1>
				</cfif>
				<cfif qResult.KEY neq qResult.UNIVERS>
					<cfif qResult.ACCES_USER gt 0>
						<cfset tmpXml = xmlAccess.MENU.xmlChildren[idxUnivers]>
						<cfset tmpXml.xmlChildren[idx] = XmlElemNew(xmlAccess,"FONCTION")>
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.LBL = #qResult.NOM_MODULE#>
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.KEY = #qResult.KEY#>
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.SYS = "1">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.USR = "2">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.type = "radio">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.groupName = "menuUniversGroup">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.toggled = "false">
						<cfset tmpXml.xmlChildren[idx].XmlAttributes.ENABLED = "true">

			 			<!--- Definition du module par défaut --->
			 			<cfif qDefaultModule.KEY eq qResult.KEY>
						 	<cflog text="FONCTION PAR DEFAUT : qResult.KEY = #qResult.KEY#">
							<cfset tmpXml.xmlChildren[idx].XmlAttributes.toggled = "true">
						 </cfif>

			 			<cfset idx = idx +1>
						<cfset "idx#qResult.UNIVERS#" = idx>
						<cfset loopindex = loopindex +1>
					</cfif>
				</cfif>
			</cfloop>

			<cfset xmlArr = xmlSearch(xmlAccess,"/MENU/UNIVERS/FONCTION")>
			<cfset xmlArr2 = xmlSearch(xmlAccess,"/MENU/UNIVERS[@USR > 0]")>
			<cfset plusVar = arraylen(xmlArr) + arraylen(xmlArr2)>
			<cfif plusVar eq 0>
				<cfset currentPath = GetDirectoryFromPath(GetCurrentTemplatePath())/>
				<cfset fileName = currentPath & "menus/Default_Menu_" & SESSION.USER.GLOBALIZATION & ".xml">
				<cffile action="read" file="#fileName#" variable="XMLFileText">
				<cfset xmlAccess=XmlParse(XMLFileText)>
			</cfif>

			<cfset SESSION.XML_ACCESS = xmlAccess>
			<cflog text="#xmlAccess#" >

			<cfreturn 1>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(444,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="setupSessionUser"  access="private" output="false" returntype="Any" >
		<cfargument name="qValidateLogin" required="true" type="query" >

		<cflog text="connection6.setupSessionUser">

		<cftry>
			<cfset SESSION.AUTH_STATE = 1>
			<cfset SESSION.OFFREDSN = "ROCOFFRE">
			<cfset SESSION.CDRDSN = "ROCCDR">
			<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW">
			<cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
			<cfset SESSION.USER = structNew()>
			<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
			<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
			<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
			<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
			<cfset SESSION.USER.APP_LOGINID = qValidateLogin['CLIENTACCESSID'][1]>
			<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
			<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
			<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>
			<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
			<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
			<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>
			<cfif compareNoCase(THIS.codeLangue.CODE,"default") eq 0>
				<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
				<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
			<cfelse>
				<cfset SESSION.USER.GLOBALIZATION =  THIS.codeLangue.CODE>
				<cfset SESSION.USER.IDGLOBALIZATION = THIS.codeLangue.ID_LANGUE>
			</cfif>
			<cfset getDeviseInfos()>
			<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#" expires="never">
			<cfreturn 1>
		<cfcatch type="any">
			<cfreturn ThrowCfCatchError(444,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
		</cfcatch>
		</cftry>
	</cffunction>
	<cffunction name="buildGroupXmlTree" access="private" output="false" returntype="Any" >
		<cfargument name="idperimetre" required="true" type="numeric">

		<cflog text="connection6.buildGroupXmlTree(#idperimetre#)">

		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.PKG_GLOBAL#.Login_nodes_acces_V3">
			<cfprocparam value="#idperimetre#" cfsqltype="CF_SQL_INTEGER">
			<cfprocparam  value="#SESSION.USER.APP_LOGINID#" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetPathToAccessNode">
		</cfstoredproc>

		<cfif qGetPathToAccessNode.recordcount GT 0>
			<cfset accessNodeId=qGetPathToAccessNode['IDGROUPE_CLIENT'][qGetPathToAccessNode.recordcount]>
			<cfset accessStatus=qGetPathToAccessNode['STC'][qGetPathToAccessNode.recordcount]>

			<cflog text="connection6.buildGroupXmlTree(#idperimetre#) - #SESSION.PERIMETRE.ID_GROUPE# - #accessNodeId#">

			<cfset _lang = left(SESSION.USER.GLOBALIZATION,2)>

			<cfswitch expression="#_lang#">
				<cfcase value="fr">
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfcase>
				<cfcase value="en">
					<cfset LEVELNo1 = "List of Carrier Organizations">
					<cfset LEVELNo2 = "List of Customer Organizations">
					<cfset LEVELNo3 = "List of Saved Searches">
				</cfcase>
				<cfcase value="es">
					<cfset LEVELNo1 = "Lista de las organizaciones de operadores">
					<cfset LEVELNo2 = "Lista de los Organismos de Clientes">
					<cfset LEVELNo3 = "Lista de las búsquedas guardadas">
				</cfcase>
				<cfcase value="nl">
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfcase>
				<cfdefaultcase>
					<cfset LEVELNo1 = "Liste des Organisations Opérateurs">
					<cfset LEVELNo2 = "Liste des Organisations Clientes">
					<cfset LEVELNo3 = "Liste des Recherches Sauvegardées">
				</cfdefaultcase>
			</cfswitch>

			<cfif accessStatus GT 0>
				<cfxml variable="perimetreXmlDoc">
					<cfoutput>
						<NODE LBL="#qGetPathToAccessNode['LIBELLE_GROUPE_CLIENT'][1]#" NID="#qGetPathToAccessNode['IDGROUPE_CLIENT'][1]#"
														NTY="#qGetPathToAccessNode['TYPE_NOEUD'][1]#" STC="#qGetPathToAccessNode['STC'][1]#">

							<NODE LBL="#LEVELNo1#" NID="-2" NTY="0" STC="0"/>
							<NODE LBL="#LEVELNo2#" NID="-3" NTY="0" STC="0"/>
							<NODE LBL="#LEVELNo3#" NID="-4" NTY="0" STC="0"/>
						</NODE>
					</cfoutput>
				</cfxml>
				<cfif accessNodeId EQ idperimetre>
					<cfset result = buildTreeFromGroup(idperimetre,perimetreXmlDoc)>
				<cfelse>
					<cfset result = buildTreeFromNode(idperimetre,perimetreXmlDoc,qGetPathToAccessNode)>
				</cfif>
				<cfset SESSION.XML_PERIMETRE = perimetreXmlDoc>
			</cfif>
			<cfreturn accessNodeId>
		<cfelse>
			<cfreturn ThrowCfCatchError(444)>
		</cfif>
	</cffunction>
	<cffunction name="getAlgo" access="private" hint="algo used" returntype="String" >
		<cfreturn "HS256">
	</cffunction>

	<cffunction name="getSecret" access="private" hint="secret" returntype="String" >
		<cfreturn "nXxtRqG|KrnBmWssFrMGPrPWXwtQrRQ2gGuwbEwRiliSSkaVVwUnmiWDoatPSxOr">
	</cffunction>
</cfcomponent>