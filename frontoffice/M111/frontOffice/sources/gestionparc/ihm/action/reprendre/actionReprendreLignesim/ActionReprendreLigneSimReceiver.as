package gestionparc.ihm.action.reprendre.actionReprendreLignesim
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.collaborateur.FicheReprendreLigneSIMIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionReprendreLigneSimReceiver
	{
		private var popup:FicheReprendreLigneSIMIHM=null;

		public function ActionReprendreLigneSimReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}


		private function displayIHM():void
		{
			popup=new FicheReprendreLigneSIMIHM();
			PopUpManager.addPopUp(popup, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(popup);
		}
	}
}
