package univers.facturation.suivi.controles.services
{
	import flash.events.EventDispatcher;
	
	import univers.facturation.suivi.controles.event.SiteEvent;
	import univers.facturation.suivi.controles.vo.SiteVO;
	
	[Bindable]
	public class SiteModel extends EventDispatcher
	{
		
		public var produit:SiteVO;
		
		public function SiteModel()
		{
		}
		
		public function updateSite(value:Object):void
		{
			produit = new SiteVO();
			
			produit.addSite(value);
			
			dispatchEvent(new SiteEvent(SiteEvent.SITE_COMPELTE));
		}
		
	}
}