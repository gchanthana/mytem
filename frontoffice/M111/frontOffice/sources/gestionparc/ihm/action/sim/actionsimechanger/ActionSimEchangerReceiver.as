package gestionparc.ihm.action.sim.actionsimechanger
{
	import flash.display.DisplayObject;

	import gestionparc.ihm.fiches.sim.FicheSimEchangerIHM;

	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionSimEchangerReceiver
	{
		private var fiche:FicheSimEchangerIHM;

		public function ActionSimEchangerReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			fiche=new FicheSimEchangerIHM();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}
