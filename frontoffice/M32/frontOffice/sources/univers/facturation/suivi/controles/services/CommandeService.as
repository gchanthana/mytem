package univers.facturation.suivi.controles.services
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;
	
	[Bindable]
	public class CommandeService
	{
		public var model:CommandeModel;
		public var handler:CommandeHandler;
		
		public function CommandeService()
		{
			model = new CommandeModel();
			handler = new CommandeHandler(model);
		}
		
		public function getCommande(idCommande:Number):void{
			
			//idCommande = 48723;
			
			var opData:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M32.GetCommande",
				"getCommande",
				handler.getCommandeInfoGeneraleHandler);
			
			RemoteObjectUtil.callService(opData,idCommande);
			
		}
		
	}
}