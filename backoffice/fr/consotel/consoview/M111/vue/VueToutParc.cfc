<cfcomponent output="false">
	
	<cfset init()>
		
	<cffunction name="init" hint="pour les variables de session" access="private">
		<cfset idRacine 	= SESSION.PERIMETRE.ID_GROUPE>
		<cfset idUser 		= SESSION.USER.CLIENTACCESSID>
		<cfset codeLangue 	= SESSION.USER.GLOBALIZATION>
		<cfset idLang 		= SESSION.USER.IDGLOBALIZATION>
		<cfset p_app_loginid = session.user.clientaccessid>
		<cfif SESSION.PERIMETRE.ID_PERIMETRE EQ idRacine>
			   <cfset idorga=idracine>
			   <cfelse>
				   <cfset idorga  = getOrga()>
	    </cfif>
	</cffunction>
	     
	<cffunction name="getOrga" access="private" output="false" returntype="Any"   hint="recupere id_organisation">												   
	   <cfquery datasource="#SESSION.OFFREDSN#" name="rsltOrga">
					   SELECT PKG_CV_RAGO.GET_ORGA(#SESSION.PERIMETRE.ID_PERIMETRE#) as IDORGA FROM DUAL
	   </cfquery>
	   <cfreturn rsltOrga['IDORGA'][1]>
	</cffunction>
	
	<cffunction name="getVueToutParc" access="remote" returntype="query">
		<cfargument name="idpool" required="true" type="Numeric">
		<cfargument name="search1" required="true" type="String">
		<cfargument name="columnSearch1" required="true" type="string">
		<cfargument name="search2" required="true" type="String">
		<cfargument name="columnSearch2" required="true" type="string"><!---doit etre E_C1,E_C2,E_C3 ou E_C4 --->
		<cfargument name="labelSort" required="true" type="string"><!---doit etre E_C1,E_C2,E_C3 ou E_C4 --->
		<cfargument name="indexDebut" required="true" type="Numeric">
		<cfargument name="nbItem" required="true" type="Numeric">
		<cfargument name="idrestriction" required="true" type="Numeric">
		<cfargument name="orderByReq" required="true" type="string">
 		
		<cfset GFC_MASTER = 7348802>
		<cfset version= "v4">
		<cfif SESSION.PERIMETRE.IDRACINE_MASTER eq GFC_MASTER>
			<cfset version= "v5">
		</cfif>

		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m111b.GetAllPoolView_#version#" >
				<cfprocparam  value="#idRacine#" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" type="in">
				<cfprocparam  value="#p_app_loginid#" cfsqltype="CF_SQL_INTEGER" variable="p_app_loginid" type="in">
				<cfprocparam  value="#search1#" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine1" type="in">
				<cfprocparam  value="#search2#" cfsqltype="CF_SQL_VARCHAR" variable="p_chaine2" type="in">
				<cfprocparam  value="#columnSearch1#" cfsqltype="CF_SQL_VARCHAR" variable="p_column_search1"  type="in">	
				<cfprocparam  value="#columnSearch2#" cfsqltype="CF_SQL_VARCHAR" variable="p_column_search2"  type="in"><!---doit etre E_C1,E_C2,E_C3 ou E_C4 --->
				<cfprocparam  value="#labelSort#" cfsqltype="CF_SQL_VARCHAR" variable="p_order_column" type="in"><!---doit etre E_C1,E_C2,E_C3 ou E_C4 --->
				<cfprocparam  value="#indexDebut#" cfsqltype="CF_SQL_INTEGER"  variable="p_index_debut" type="in">
				<cfprocparam  value="#nbItem#" cfsqltype="CF_SQL_INTEGER" variable="p_number_of_records" type="in">
				<cfprocparam  value="#idorga#" cfsqltype="CF_SQL_INTEGER" variable="p_idorga" type="in" null='true'>
				<cfprocparam  value="#idrestriction#" cfsqltype="CF_SQL_INTEGER" variable="p_idrestriction" type="in">
				<cfprocparam  value="#idLang#" cfsqltype="CF_SQL_INTEGER" variable="p_langid" type="in">
				<cfprocparam  value="#orderByReq#" cfsqltype="CF_SQL_VARCHAR" variable="p_order_by">
				<cfprocresult name="p_retour">
		</cfstoredproc>
		<cfreturn p_retour /> 
	</cffunction>
</cfcomponent>