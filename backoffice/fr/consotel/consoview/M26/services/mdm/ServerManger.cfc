<cfcomponent output="false">
	
	<cfprocessingdirective pageEncoding="utf-8"/>
	
	<cffunction name="syncServer" access="remote" returntype="any"> 
		
		<cfset mdmPool= new fr.saaswedo.api.tests.mdm.POC_MdmPool()>
		<cfset mdm= mdmPool.getMdmServerUrl(session.perimetre.id_groupe)>
		
		<!--- log début synchro mdm 471--->
		<cfsavecontent variable="bodyContext">
			Une connexion pour la synchro MDM a débuté :
			Racine : <cfoutput>#session.perimetre.groupe#</cfoutput> (<cfoutput>#session.perimetre.id_groupe#</cfoutput>)
			User : <cfoutput>#session.user.email#</cfoutput>
			Url : <cfoutput>#mdm#</cfoutput>
		</cfsavecontent>
		
		<cfset body=removeLineBreaksAndtabs(#bodyContext#)>
			
		<cfset logInfo(59,471, body, "Début Synchro MDM", 0)>
		<!--- fin log début synchro mdm 471--->
	
		<cfset idGroupe=session.perimetre.id_groupe>
		<cfset mdmPool= new fr.saaswedo.api.tests.mdm.POC_MdmPool()>

		<cftry>
			<cfset mdmPool.import(idGroupe)>
			
			<!--- début log synchro ok 473--->
			<!--- Ne pas remettre les cfsavecontent en indentation, car les tabs qui précèdent sont enregistrés dans la variable body aussi --->
			<cfsavecontent variable="bodyContext">
				Une connexion pour la synchro MDM a réussi :
				Racine : <cfoutput>#session.perimetre.groupe#</cfoutput> (<cfoutput>#session.perimetre.id_groupe#</cfoutput>)
				User : <cfoutput>#session.user.email#</cfoutput>
				Url : <cfoutput>#mdm#</cfoutput>
			</cfsavecontent>
			
			<cfset body=removeLineBreaksAndtabs(#bodyContext#)>
			
			<cfset logInfo(59,473, body, "Synchro MDM OK", 0)>
			<!--- fin log synchro ok 473--->
		<cfcatch type="any">
			<cfset throwHipChatPost(CFCATCH.MESSAGE)>
			<cfthrow object=#CFCATCH#>
		</cfcatch>
		</cftry>
		<cfreturn 1>
	</cffunction>
	
	<cffunction name="loadMdmInfo" access="remote" returntype="any"> 
		<cfargument name="ligneInfos" type="any" required="true">
		<cfargument name="fake" type="any" required="false">
		
		<cfset idGroupe=session.perimetre.id_groupe>
		<cfset _numligne = trim(ar_ligne[i]['UUID'])>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_mdm.getLineInfo">
			<cfprocparam type="In" cfsqltype="cf_sql_varchar" value="#_numligne#">
			<cfprocparam type="In"  cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocresult  name="qResult">
		</cfstoredproc>
		
		<cfreturn qResult>
	</cffunction>
	
	
	<cffunction name="throwHipChatPost" access="remote" returntype="any">
		
		<cfargument name="erreur" required="true" type="any">
		<cfset mdmPool= new fr.saaswedo.api.tests.mdm.POC_MdmPool()>
		<cfset mdm= mdmPool.getMdmServerUrl(session.perimetre.id_groupe)>
		
		<cfsavecontent variable="bodyContext">
			Une connexion pour la synchro MDM a échoué :
			<cfoutput>#erreur#</cfoutput>
			Racine : <cfoutput>#session.perimetre.groupe#</cfoutput> (<cfoutput>#session.perimetre.id_groupe#</cfoutput>)
			User : <cfoutput>#session.user.email#</cfoutput>
			Url : <cfoutput>#mdm#</cfoutput>
		</cfsavecontent>
		
		<cfset body=removeLineBreaksAndtabs(#bodyContext#)>
		
		<cfset errorType=findErrorType(erreur)>
		
		<cfif errorType NEQ "">
			<cfset logInfo(59,472, body, "Synchro MDM (" & errorType & ")", 1)>
			<cfelse>
			<cfset logInfo(59,472, body, "Synchro MDM", 1)>
		</cfif>

		<cfreturn 1>
	</cffunction>

	
	<cffunction name="findErrorType" access="private" returntype="string"><!--- Fonction retournant un court descriptif de l'erreur obtenue --->
		<cfargument name="erreur" required="true" type="string">
		
		<cfset posCode=REFind("4[0-9][0-9]", erreur)><!--- position d'un éventuel code d'erreur 400+ dans la chaine --->
		<cfif posCode GT 0><!--- Si code 400+ --->
			<cfset codeErreur=Mid(erreur, posCode, 3)>
			<cfreturn "code "& codeErreur>
		</cfif>
		<cfset posCode=REFind("peer\snot\sauthenticated", erreur)>
		<cfif posCode GT 0><!--- Si c'est une "peer not authenticated" Exception --->
			<cfreturn "Peer not authenticated">
		</cfif>
		<cfset posCode=REFind("Connection\sreset", erreur)>
		<cfif posCode GT 0><!--- Si c'est une "connection reset" Exception --->
			<cfreturn "Connection reset">
		</cfif>
		<cfset posCode=REFind("5[0-9][0-9]", erreur)><!--- position d'un éventuel code d'erreur 500+ dans la chaine --->
		<cfif posCode GT 0><!--- Si code 500+ --->
			<cfset codeErreur=Mid(erreur, posCode, 3)>
			<cfreturn "code "& codeErreur>
		</cfif>
		
		<cfreturn "">
	</cffunction>
	
	<cffunction name="removeLineBreaksAndtabs" returntype="string">
		
		<cfargument name="mystring" type="String" required="true">
		
		<!--- chr(9) = tab  --->
		<cfset killList="#chr(9)#"> <!---  "#chr(10)#,#chr(13)#,#chr(9)#" pour supprimer le retour chariot et tab --->
		<cfset mywhitespacelessstring = replacelist(mystring,killList,"")>
		<cfreturn mywhitespacelessstring>
		
	</cffunction>
	
	<cffunction name="logInfo" access="private" returntype="any">
		
		<cfargument name="p_idmnt_process" type="numeric" required="true">
		<cfargument name="p_idmnt_event" type="numeric" required="true">
		<cfargument name="body" type="string" required="true">
		<cfargument name="p_subject" type="string" required="true">
		<cfargument name="notify" type="numeric" required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="mnt.pkg_mnt_log.logit">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idmnt_process#" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#p_idmnt_event#">
			<cfprocparam type="In" cfsqltype="CF_SQL_CLOB" value="#body#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#p_subject#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#session.perimetre.id_groupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" null="true">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#notify#">
			<cfprocparam type="In" cfsqltype="CF_SQL_TIMESTAMP" null="true">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="-">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  value="-">
			<cfprocparam type="Out" cfsqltype="cf_sql_integer" variable="p_retour">
		</cfstoredproc>
		
		<!---		<!--- Debut log --->
		<cfset mLog = createObject('component','fr.consotel.api.monitoring.MLog')>
		<cfset mLog.setIDMNT_CAT(62)>
		<cfset mLog.setIDMNT_ID(202)>
		<cfset mLog.setBODY(body)>
		
		<cfset posCode=REFind("4[0-9][0-9]", erreur)><!--- position du code d'erreur dans la chaine --->
		<cfif posCode GT 0><!--- code 400+ --->
			<cfset mLog.setSUBJECTID("<strong>[WARNING]-[MDM-002] : Synchro MDM</strong>")>
			<cfelse><!--- 500+ + Peer not authenticated + le reste--->
			<cfset mLog.setSUBJECTID("<strong>[EXCEPTION]-[MDM-002] : Synchro MDM</strong>")>
		</cfif>
		<cfset mLog.setIDRACINE(SESSION.PERIMETRE.ID_GROUPE)>
		 
		<cfinvoke component="fr.consotel.api.monitoring.MLogger"
			method="logIt"  returnVariable = "result">
			<cfinvokeargument name="mLog" value="#mLog#"> 
		</cfinvoke>
		<!--- Fin log--->--->
		
		<cfreturn 1>
	</cffunction>

</cfcomponent>