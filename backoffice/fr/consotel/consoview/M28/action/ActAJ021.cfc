﻿<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Insertion ou modification d'une racine (client)">
	
	<cffunction name="executeAct" access="public" returntype="struct" output="false">		
		<cfargument name="data" type="struct" required="true"/>
		<cftry>
			<cfif #data.p_idracine# EQ 0>
				<!--- création d'une nouvelle racine --->
				<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_client.IURACINE">
					<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idracine_master#">
					<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idracine#">
					<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.P_LIBELLE_racine#">
					<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.P_COMMENTAIRE#">
					
					<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
				</cfstoredproc>
				
				<cfif (p_retour GTE 1) AND (isObject(data.fileToUpload))>
					<cfset idRacine = p_retour>
					<cfset statusUpload = uploadFile(idRacine,data.fileToUpload)>
				</cfif>
			<cfelseif (#data.p_idracine# GTE 0)>
				<!--- si #data.p_idracine# > 1 alors Modification; on peut modifier que le logo ou le commentaire --->
					<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_client.IURACINE">
						<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idracine_master#">
						<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idracine#">
						<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.P_LIBELLE_racine#">
						<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#data.P_COMMENTAIRE#">
						
						<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
					</cfstoredproc>
					
				<cfif (p_retour GTE 1) AND (isObject(data.fileToUpload))>
					<cfset idRacine = p_retour>
					<cfset statusUpload = uploadFile(idRacine,data.fileToUpload)>
				</cfif>
			</cfif>
			
		 	<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ012", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur Inconnue", "", "")>
				<cfreturn CreateReponse("AJ012", 0, err)/>
			<cfelseif p_retour EQ -10>
				<cfset err = ThrowCfCatchError(p_retour, "", "On ne peut pas changer le maitre de cette racine il faut d'abord enlever les comptes de cette racine du groupe_maitre", "", "")>
				<cfreturn CreateReponse("AJ012", 0, err)/>
			</cfif>
		<cfcatch type="Any">
			<cfreturn CreateReponse("AJ021", 0, ThrowCfCatchError("AJ021",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
		</cfcatch>
		</cftry>		
	</cffunction>
	
	<!---
		fonction pour uploader un fichier
	 --->
	<cffunction name="uploadFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="idRacine"	type="Numeric" required="true"/>
		<cfargument name="currentFile" 	type="fr.consotel.consoview.M28.vo.FileUploadVo" required="true"/>
		
		<cfset var webPath 		 = GetDirectoryFromPath( GetCurrentTemplatePath() )>
		<cfset var dirTmp 		 = "tmpDirLogos/">
		<!---<cfset var dirLogoClient = "cv/cache/assets/logo/">--->
		
		<cfset var arrDirLogoClient = ArrayNew(1)>
		<cfset arrayAppend(arrdirLogoClient,"cv/cache/assets/logo/")>
		<cfset arrayAppend(arrdirLogoClient,"cv/image_mobile/assets/logo/")>
		
		<!--- creation d'UUID pour le fichier--->
		<cfset currentFile.fileUUID = createUUID()>
		
		<cfset var pathDirTMP = webPath & dirTmp & currentFile.fileUUID>
		<cfset var pathFileTMP = pathDirTMP & "/" & currentFile.fileName>
		<cfset var pathFileTMPNewName = pathDirTMP & "/" & ARGUMENTS.idRacine & ".png">
		
		<!--- gestion TMP --->
		<cfset var statusReturn	 = 0>
		<cfset var statusReturn = createDirTMP(pathDirTMP,pathFileTMP,currentFile)>
		
		<cfif statusReturn eq 1 >
			
			<cfset var renameReturn = renameFile(pathFileTMP,pathFileTMPNewName)>
			<cfset currentFile.fileName = idRacine & ".png">
			
			<cfloop from="1" to="#ArrayLen(arrdirLogoClient)#" index="indexDir">
        		<!--- gestion FTP (creation DIR/File) --->
				<cfset handleFTP(pathFileTMPNewName,arrDirLogoClient[indexDir],currentFile)>
    		</CFLOOP>
    		
			<!--- delete DIR TMP --->
			<cfset deleteFileTMP(webPath & dirTmp)>

		</cfif>
		
		<cfreturn statusReturn>
		
	</cffunction>
	
	<!---
		Création du dossier et du fichier temporaires dans le path du serveur
	 --->
	<cffunction name="createDirTMP" access="private" output="false" returntype="numeric">
		
		<cfargument name="pathDirTMP" type="string" required="true">
		<cfargument name="pathFileTMP" type="string" required="true">
		<cfargument name="currentFile" type="struct" required="true">
		
		<cftry>
			
			<cfif NOT DirectoryExists("#pathDirTMP#")>
				<cfdirectory action="Create" directory="#pathDirTMP#" type="dir" mode="777">
			</cfif>
			
			<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M28.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
			
			<cffile action="write" output="#currentFile.fileData#" file="#pathFileTMP#" mode="777">
			
			<cfset statusReturn = 0>
			<cfif FileExists("#pathFileTMP#") is "Yes">  
			    <cfset statusReturn=1>  
			</cfif>
			
			<cfreturn statusReturn>
			
		<cfcatch>
			<!--- log --->
			<cflog type="error" text="#CFCATCH.Message#">
			<!--- mail d'erreur --->
			<cfmail server="mail-cv.consotel.fr" port="25" from="soufiane.ghenimi@saaswedo.com" to="soufiane.ghenimi@saaswedo.com" 
					failto="soufiane.ghenimi@saaswedo.com" subject="[WARN-CG08-createDirTMP]Erreur lors de l'upload de fichiers" type="html">
					
					#CFCATCH.Message#<br/><br/><br/><br/>
					
			</cfmail>
			
		</cfcatch>					
		</cftry>
		
	</cffunction>
	
	<!--- 
	 	gerer le ftp (cnx + creation du dir et creation de file + close)
	--->
	<cffunction name="handleFTP" access="private" output="false" returntype="void">
		<cfargument name="tmpPathFile" 			type="string" required="true">
		<cfargument name="ftpPathDirectory" 	type="string" required="true">
		<cfargument name="currentFile" 			type="struct" required="true">
		
		<cftry>	
			<cfset currentFile.fileName = createObject("component","fr.consotel.consoview.M28.RegexRenameFile").changeSpecialCaracters(currentFile.fileName)>
			
			<cfset var fileDestination = ARGUMENTS.ftpPathDirectory & currentFile.fileName>
			
			<!--- OUVRIR LA CONNEXION --->
			<cfftp connection="cnxFTP" action="open" username="services" password="services" server="Pelican.consotel.fr" stopOnError="Yes" >
			<cfif cfftp.succeeded EQ "YES">
				<!--- CREATION DOSSIER --->
				<cfftp connection="cnxFTP" action="existsdir" directory="#ARGUMENTS.ftpPathDirectory#">	
				
				<cfif cfftp.returnValue eq "YES">
					<!--- UPLOAD DU FICHIER --->
					<cfftp connection="cnxFTP" action="putfile"  localFile="#ARGUMENTS.tmpPathFile#" remoteFile="#fileDestination#">
				</cfif>
				
				<!--- FERMER LA CONNEXION --->
				<cfftp connection="cnxFTP" action="close" stopOnError="Yes">	
			</cfif>
		<cfcatch>
				<cflog type="error" text="#CFCATCH.Message#">													
				<cfmail server="mail.consotel.fr" port="26" from="soufiane.ghenimi@saaswedo.com" to="soufiane.ghenimi@saaswedo.com" 
						failto="soufiane.ghenimi@saaswedo.com" subject="[CG08:gestion client]Erreur lors de l'upload d'un fichier" type="html">
						#CFCATCH.Message#<br/><br/><br/><br/>
				</cfmail>
			</cfcatch>					
			</cftry>
		
	</cffunction>
	
	<!--- 
	 	supprimer le fichier dans le dossier TMP
	--->
	<cffunction name="deleteFileTMP" access="private" output="false" returntype="void">
		<cfargument name="pathDirTMP" type="string" required="true">
		
		<cfif DirectoryExists('#pathDirTMP#')>
			<cfdirectory action="delete" directory="#pathDirTMP#" type="dir" mode="777" recurse="true">
		</cfif>
	</cffunction>
	
	<!---
		Renommmer un fichier
	 --->
	<cffunction name="renameFile" access="remote" output="false" returntype="Numeric" hint="Upload un fichier">
		<cfargument name="pathFileOldName"		type="String" required="true"/>
		<cfargument name="pathFileNewName" 			type="String" required="true"/>
			
			<cfset var renameStatus	= 0>
			<cftry>	
				<cfif NOT FileExists(pathFileOldName)>
					<cfset renameStatus = -1>
				<cfelse>
					<cffile action="rename" source="#pathFileOldName#" destination="#pathFileNewName#" attributes="normal">
					<cfset renameStatus = 1>
				</cfif>
				
			<cfreturn renameStatus>
			
			<cfcatch>
				<cflog type="error" text="#CFCATCH.Message#">													
				<cfmail server="mail.consotel.fr" port="26" from="soufiane.ghenimi@saaswedo.com" to="soufiane.ghenimi@saaswedo.com" 
						failto="soufiane.ghenimi@saaswedo.com" subject="[CG08:Gestion client]Erreur lors de renommage de l'image" type="html">
						#CFCATCH.Message#<br/><br/><br/><br/>
				</cfmail>
			</cfcatch>					
			</cftry>
		
	</cffunction>

</cfcomponent>