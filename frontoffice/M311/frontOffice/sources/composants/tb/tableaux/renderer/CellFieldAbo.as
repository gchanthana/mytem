package composants.tb.tableaux.renderer
{
	import mx.containers.Canvas;
	import mx.controls.Label;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	
	import composants.util.preloader.Spinner;
	
	import ihm.infobulle.ImgHelp;
	
	public class CellFieldAbo extends Canvas
    {
		private var _spin:Spinner;
		private var _info_text:String = ResourceManager.getInstance().getString('M311', 'Le_kpi__b_Nb_de_lignes__b__affiche_les_l');
			
		private var _info:ImgHelp = new ImgHelp();
		
		
        // Define the constructor and set properties.
       private var _libelle : Label;
       public function CellFieldAbo() {
		    
            setStyle("borderStyle", "none");
			addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
        }
	   
	   protected function creationCompleteHandler(event:FlexEvent):void
	   {
		   var delta:int = width - 20;
		   trace("width " + width,"delta :" + delta);
		   _spin.x = delta;
		   _info.x = delta;
		   _spin.play();
	   }
	   
	   override protected function createChildren():void
	   {
		   super.createChildren();
		   
		   
		   if(_spin == null)
		   {
			   _spin = new Spinner();
			   _spin.size = 15;
			   _spin.numTicks = 12;
			   
			   _spin.y = 1;
			   addChild(_spin);
			   _spin.visible = false;
		   }
		   
		   if(_libelle == null)
		   {
			   _libelle = new Label(); 
			   _libelle.percentWidth = 100;      
			   addChild(_libelle);   
		   }
		   
		   if(_info)
		   {
			   _info = new ImgHelp();
			  
			   _info.y = 1;
			   _info.text = _info_text;
			   _info.visible = false;
			   addChild(_info);   
		   }
		   
	   }
	   
        // Override the set method for the data property.
        override public function set data(value:Object):void {
            super.data = value;
       		if (value) 
			{				
                super.data = value;			
				if (value.TYPE.toUpperCase() == "SEGMENT" ){
					
					setStyle("color", "#336699");
					setStyle("fontFamily", "verdana");
					setStyle("fontWeight", "bold");
					var sNbLigne :String = (Number(value.NBLIGNE) > 1)?'Lignes':'Ligne' ;
					if(value.NBLIGNE != "-")
					{
						_libelle.text  =  ResourceManager.getInstance().getString('M311', 'Segment_') + value.SEGMENT + " : " + value.NBLIGNE + " " 
							+ ResourceManager.getInstance().getString('M311', sNbLigne) ;
						
						_info.visible = true;
						_spin.visible = false;
							
					}else
					{
						_libelle.text  =  ResourceManager.getInstance().getString('M311', 'Segment_') + value.SEGMENT + " : " + value.NBLIGNE + " " 
							+ ResourceManager.getInstance().getString('M311', sNbLigne) ;
						
						_info.visible = false;
						_spin.visible = true;
					}
					
				}else{
					_libelle.text  = value.LIBELLE;
					
				}
			}
           	super.invalidateDisplayList();
        }
		
    }
}
