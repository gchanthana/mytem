package catalogue.ihm.typecommande
{
	import catalogue.event.CatalogueEvent;
	import catalogue.service.AboOptService;
	import catalogue.service.GlobalCatalogueService;
	import catalogue.vo.TypeAboOpt;
	
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class AboOptionsDefautImpl extends TitleWindow
	{

	//--------------------------------------------------------------------------------------------//
	//					VARIABLES
	//--------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------//
		//					IHMs
		//--------------------------------------------------------------------------------------------//
		
		public var dg					:DataGrid;
		
		public var cbAll 				:CheckBox;
		
		//--------------------------------------------------------------------------------------------//
		//					SERVICES
		//--------------------------------------------------------------------------------------------//
		
		public var globalService 		:GlobalCatalogueService = new GlobalCatalogueService();
		
		public var aboOptSrv 			:AboOptService = new AboOptService();
		
		//--------------------------------------------------------------------------------------------//
		//					AUTRES
		//--------------------------------------------------------------------------------------------//
		
		public var listeAbo 			:ArrayCollection;
		public var listeTypeCommande	:ArrayCollection = new ArrayCollection();
		
		public var listeAboEquipement 	:String ;
		
		public var groupeIndex 			:int;
		public var idClient 			:int;
		public var idthemetype 			:int;//1->ABO, 2->OPT
		public var idsegment 			:int = 1;
		public var idoperateur 			:int = 0;
		public var compteurSelected		:int;

	//--------------------------------------------------------------------------------------------//
	//					CONSTRUCTEUR
	//--------------------------------------------------------------------------------------------//
		
		public function AboOptionsDefautImpl()
		{			
			addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
		}

	//--------------------------------------------------------------------------------------------//
	//					PROTECTED
	//--------------------------------------------------------------------------------------------//
		
		protected function fermer(e:Event):void
		{
			PopUpManager.removePopUp(this);
		}

	//--------------------------------------------------------------------------------------------//
	//					PRIVATE
	//--------------------------------------------------------------------------------------------//
	
		//--------------------------------------------------------------------------------------------//
		//					LISTENER 
		//--------------------------------------------------------------------------------------------//
		
		private function creationCompleteHandler(fe:FlexEvent):void
		{
			dg.addEventListener('REFRESH', refreshHandler);
			
			getTypeCommande();
		}
		
		private function refreshHandler(e:Event):void
		{
			if(dg.selectedItem != null)
				onEtatChanged();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					LISTENERS PROCEDURES 
		//--------------------------------------------------------------------------------------------//
		
		private var _idsTypeCommande:ArrayCollection = new ArrayCollection();
		
		private function getTypeCommandeHandler(e:Event):void
		{
			listeTypeCommande = new ArrayCollection();
			listeTypeCommande = aboOptSrv.TYPECOMMANDE;

			var lenTC:int = listeTypeCommande.length;
			
			if(lenTC > 0)
			{
				if(idthemetype == 2)
					getLastAssociated();
				else
				{
					addTypeCommande();
					getassociatedabo();
				}
			}
			else
				ConsoviewAlert.afficherAlertInfo(ResourceManager.getInstance().getString('M24', 'Vous_n_avez_acc_s___aucun_type_de_comman'), 'Consoview', null);
		}
		
		private function getassociatedaboHandler(e:Event):void
		{
			var abonn	:ArrayCollection = aboOptSrv.ABONNEMENT;
			var lenAB	:int = abonn.length;
			var lenAO	:int = listeAbo.length;
			var lenTC	:int = listeTypeCommande.length;
			var id		:int = 0;
			var i		:int = 0;
			var j		:int = 0;
			
			for(i = 0;i < lenAO;i++)
			{
				id = listeAbo[i].IDABO;
				
				for(j = 0;j < lenAB;j++)
				{
					if(id == int(abonn[j].IDCATALOGUE))
						abonn[j].ISPRESENT = true;
				}
			}
			
			for(i = 0;i < lenAB;i++)
			{
				id = abonn[i].IDPROFIL;
				
				for(j = 0;j < lenTC;j++)
				{
					if(id == listeTypeCommande[j].IDPROFIL)
					{
						listeTypeCommande[j].IDRESSOURCE = int(abonn[i].IDCATALOGUE);
						listeTypeCommande[j].ISPRESENT 	 = formatBoolean(abonn[i].ISPRESENT.toString());
					}
				}
			}
			
			getLastAssociated();
		}

		private function lastAssociatedHandler(e:Event):void
		{
			var liste	:ArrayCollection = aboOptSrv.ABO_ASSOCIATED;
			var lenAO	:int = liste.length;
			var lenTC	:int = listeTypeCommande.length;
			
			var i:int = 0;
			
			if(lenAO == 0)
			{
				for(i = 0;i < lenTC;i++)
				{
					(listeTypeCommande[i] as TypeAboOpt).ETAT_PROFIL = 0;
					(listeTypeCommande[i] as TypeAboOpt).ETAT_OBLIGA = 0;
					(listeTypeCommande[i] as TypeAboOpt).PROFIL_SELECTED = false;
					(listeTypeCommande[i] as TypeAboOpt).OBLIGA_SELECTED = false;
				}
			}
			else
			{
				for(i = 0;i < lenTC;i++)
				{
					var id:int = listeTypeCommande[i].IDPROFIL;
					
					for(var j:int = 0;j < lenAO;j++)
					{
						if(liste[j].IDPROFIL == id)
						{
							(listeTypeCommande[i] as TypeAboOpt).ETAT_PROFIL++;
							(listeTypeCommande[i] as TypeAboOpt).PROFIL_SELECTED = true;
							
							if(liste[j].OBLIGATOIRE > 0)
							{
								(listeTypeCommande[i] as TypeAboOpt).ETAT_OBLIGA++;
								(listeTypeCommande[i] as TypeAboOpt).OBLIGA_SELECTED = true;
							}
						}
					}
				}
			}
			
			(dg.dataProvider as ArrayCollection).refresh();
		}
		
		//--------------------------------------------------------------------------------------------//
		//					APPELS PROCEDURES 
		//--------------------------------------------------------------------------------------------//
		
		private function getTypeCommande():void
		{
			aboOptSrv.getTypeCommande(listeAboEquipement, idsegment, groupeIndex, idoperateur, idthemetype);
			aboOptSrv.addEventListener(AboOptService.ABOOPT_TYPECOMMANDE, getTypeCommandeHandler);
		}
		
		private function getassociatedabo():void
		{
			aboOptSrv.getassociatedabo(_idsTypeCommande.source, idoperateur);
			aboOptSrv.addEventListener(AboOptService.ABOOPT_ABONNEMENT, getassociatedaboHandler);
		}
		
		private function getLastAssociated():void
		{
			aboOptSrv.getLastAssociated(listeAboEquipement, groupeIndex);
			aboOptSrv.addEventListener(AboOptService.ABOOPT_LASTABOASSOCIATED, lastAssociatedHandler);
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FUCNTION 
		//--------------------------------------------------------------------------------------------//
		
		private function addTypeCommande():void
		{
			var lenTC:int = listeTypeCommande.length;
			
			_idsTypeCommande = new ArrayCollection();
			
			for(var i:int = 0;i < lenTC;i++)
			{
				_idsTypeCommande.addItem(listeTypeCommande[i].IDPROFIL);
			}
		}
		
		private function onEtatChanged():void
		{
			(dg.dataProvider as ArrayCollection).itemUpdated(dg.selectedItem);
			
			var values	:ArrayCollection = (dg.dataProvider as ArrayCollection);
			var len		:int = values.length;
			
			compteurSelected = 0;
			
			for(var i:int = 0;i < len;i++)
			{
				if(values[i].ETAT_PROFIL > 0)
					compteurSelected++;
			}
		}
		
		//--------------------------------------------------------------------------------------------//
		//					FORMATEURS 
		//--------------------------------------------------------------------------------------------//
		
		private function formatBoolean(bool:String):Boolean
		{
			var boool:Boolean = true;
			
			if(bool == 'false')
				boool = false;
			
			return boool;
		}

	}
}