 <cfcomponent displayname="NeufMultiCDRProcessStrategy" extends="ProcessStrategy" output="true">
	<cfproperty  name="ID_CLIENT" type="string">
	<cfproperty  name="LIBELLE_CLIENT" type="string">
	<cfset variables.repertoire="/app/fichiers/1-recup/sfr_fixe/cdr">
	
	<!--- Mï¿½thode pour se logger, on ramï¿½ne le cookie --->
	<cffunction name="Login" access="remote" returntype="String" output="true">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		
		<cfhttp method="get" resolveurl="true" url="https://extranet.sfrbusinessteam.fr/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/DiveLoginPage" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
		</cfhttp>
		
			<!--- Parse pour identifier le cookie --->
			<cfset chaine=cfhttp.ResponseHeader["Set-Cookie"]>
			<cfset jCookie=GetToken(GetToken(chaine,1,";"),2,"=")>
			<cfoutput>
				Strategie : NeufMulti<br>
			Cookie 1: #jCookie#<br>
			</cfoutput>
		
		<cfhttp method="POST" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/EntrepriseApplicationServlet/ConnexionURL/../ConnexionURL/DiveLoginPage/wicket:interface/:0:loginForm::IFormSubmitListener::;" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jCookie)#">
				<cfhttpparam type="formfield" name="login" value="#login#">
				<cfhttpparam type="formfield" name="password" value="#pwd#">
				<cfhttpparam type="formfield" name="Submit" value="Ok">
		</cfhttp>
		
		<cfif Len(cfhttp.FileContent) gte  5600 and Len(cfhttp.FileContent) lte  5800>
			<cfmail from="Agent Oracle <production@consotel.fr>" to="djalel.meftouh@consotel.fr" bcc="brice.miramont@consotel.fr,production@consotel.fr" server="192.168.3.119" 
						port="26"  type="HTML" subject="[Extranet NeufCegetel]- #login#">
				<style>
				html body {
					margin:0;
					padding:0;
					background: ##D4D0C8;
					font:x-small Verdana,Sans-serif;
				}
				p {
					font:bold x-small EurostileBold,verdana,arial,helvetica,serif;
				}
				td {
					font-size : x-small;
					/*color: ##629FB5;*/
				}
				</style>
						
				<p>Login : #login#</p>
				Probl�me de login:<br>
				login: #login#<br>
				password: #pwd#<br>
			</cfmail> 
			
		</cfif>
		
		<cfreturn jCookie>
	</cffunction>
	
	<!--- Mï¿½thode pour prï¿½parer l'extranet avant d'accï¿½der aux factures --->
	<cffunction name="PrepareExtranet" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="login" type="String">
		<cfargument name="pwd" type="String">
		<cfhttp method="GET" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/AccessLogger" 
				useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
				<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
				<cfhttpparam type="url" name="access_url" value="/extranet/servlet/Facture---tache">
				<cfhttpparam type="url" name="access_srv" value="1369999">
				<cfhttpparam type="url" name="tache" value="image---">
				<cfhttpparam type="url" name="rapport" value="1">
		</cfhttp>
		
		<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="tache" value="image">
					<cfhttpparam type="url" name="rapport" value="1">
			</cfhttp>
		
		<cfset v=ArrayNew(1)>
		<cfset v=ListToArray(cfhttp.FileContent,"<")>
		<cfset u=ArrayNew(1)>
		
		<cfset kl=1>
		<cfloop from="1" to="#ArrayLen(v)#" index="i">
			<cfif Left(v[i],5) eq "input">
				<cfif getToken(v[i],4,"#chr(34)#") eq "ID_CLIENT">
					<cfset u[kl]=v[i]>
					<cfset kl=kl+1>
				</cfif>
			</cfif>
		</cfloop>
			
		<!--- Fake array pour coller a la strategy --->
		<!--- <cfset aTemp=ArrayNew(1)>
		<cfset aTemp[1]=u[2]> --->
		<cfreturn u>
	</cffunction>
	
	<!--- Mï¿½thode se connecter au compte --->
	<cffunction name="connectCompte" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="IdCompte" type="String">
		<cfset this.ID_CLIENT=getToken(IdCompte,6,"#chr(34)#")>
		<cfset this.LIBELLE_CLIENT=getToken(IdCompte,8,"#chr(34)#")>
			<cfhttp method="POST" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="formfield" name="tache" value="image">
					<cfhttpparam type="formfield" name="rapport" value="1">
					<cfhttpparam type="formfield" name="LIBELLE_CLIENT" value="#this.LIBELLE_CLIENT#">
					<cfhttpparam type="formfield" name="ID_CLIENT" value="#this.ID_CLIENT#">
					<cfhttpparam type="formfield" name="action" value="framesetHierarchique">
			</cfhttp>
		<cfreturn jsession>
	</cffunction>
	
	<!--- Mï¿½thode pour aller chercher la liste des factures --->
	<cffunction name="getListeFactures" access="remote" returntype="array" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="nbreMois" type="numeric">
		<cfargument name="shiftMois" type="numeric" default="0">
		
			<cfhttp method="GET" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="url" name="tache" value="image">
					<cfhttpparam type="url" name="rapport" value="1">
					<cfhttpparam type="url" name="LIBELLE_CLIENT" value="#this.LIBELLE_CLIENT#">
					<cfhttpparam type="url" name="ID_CLIENT" value="#this.ID_CLIENT#">
					<cfhttpparam type="url" name="action" value="formDIVGE">
			</cfhttp>
			
			<cfset thread = CreateObject("java", "java.lang.Thread")>
			<cfset thread.sleep(2000)>
			
			<cfset v=ArrayNew(1)>
			<cfset v=ListToArray(cfhttp.FileContent,"<")>
			<cfset u=ArrayNew(1)>
			
			<cfloop from="1" to="#ArrayLen(v)#" index="i">
				<cfif Left(v[i],5) eq "input">
					<cfset u=ListToArray(v[i],"#chr(34)#")>
					<cfbreak>
				</cfif>
			</cfloop>

			<cfset afichiers=ArrayNew(1)>
			<cfset aTotalFactures=ArrayNew(1)>
			
			<cfif ArrayLen(u) eq 0>
				<cfmail from="consoProd@consotel.fr" to="brice.miramont@consotel.fr" server="192.168.3.119" 
					port="26"  type="HTML" subject="[Extranet NeufCegetel] Pas de facture">
					<style>
					html body {
						margin:0;
						padding:0;
						background: ##D4D0C8;
						font:x-small Verdana,Sans-serif;
					}
					p {
						font:bold x-small EurostileBold,verdana,arial,helvetica,serif;
					}
					td {
						font-size : x-small;
						/*color: ##629FB5;*/
					}
					</style>
							
					<p>Pas de page de facture<br>
					</p>
					
				</cfmail>
			<cfelse>
				
				<cfset idW=u[6]>
				<cfset flag=0>
				<cfoutput>wait</cfoutput>
				<cfloop condition="flag eq 0">
					<cfif left(idW,8) eq "idWaiter"><cfoutput>wait</cfoutput>
						<cfset thread.sleep(5000)>
						<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Wait" 
								useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
								<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
								<cfhttpparam type="formfield" name="idWaiter" value="#idW#">
						</cfhttp>
						
						<cfset v=ArrayNew(1)>
						<cfset v=ListToArray(cfhttp.FileContent,"<")>
						
						<cfloop from="1" to="#ArrayLen(v)#" index="i">
							<cfif Left(v[i],5) eq "input">
								<cfset u=ArrayNew(1)>
								<cfset u=ListToArray(v[i],"#chr(34)#")>
								<cfbreak>
							</cfif>
						</cfloop>
						<cfset idW=u[6]>
						<cfset err=find("pas de facture disponible",ArrayToList(v),1)>
						<cfif err gt 0>
							<cfbreak>
						</cfif>
					<cfelse>
						<cfset flag=1>
					</cfif>
				</cfloop>
				<cfdump var="#cfhttp.FileContent#">
				<cfset datefin=LsDateFormat(DateAdd("m",-#shiftMois#,now()),"yyyymm")>
				<cfset datedeb=LsDateFormat(DateAdd("m",-#Evaluate(nbreMois+shiftMois)#,now()),"yyyymm")>
				
				<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
						useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
						<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
						<cfhttpparam type="formfield" name="tache" value="image">
						<cfhttpparam type="formfield" name="action" value="search">
						<cfhttpparam type="formfield" name="CONTRAT_T" value="TOUS">
						<cfhttpparam type="formfield" name="FACTURE_T" value="Saisir un NÂ° de facture">
						<cfhttpparam type="formfield" name="rapport" value="1">
						<cfhttpparam type="formfield" name="DATE_DEB" value="#datedeb#">
						<cfhttpparam type="formfield" name="DATE_FIN" value="#datefin#">
						<cfhttpparam type="formfield" name="COMPTE_T" value="TOUS">
						<cfhttpparam type="formfield" name="LIBELLE_CLIENT" value="#this.LIBELLE_CLIENT#">
						<cfhttpparam type="formfield" name="ID_CLIENT" value="#this.ID_CLIENT#">
				</cfhttp>
				
				
				<cfset v=ArrayNew(1)>
				<cfset v=ListToArray(cfhttp.FileContent,"<")>
				
				<cfloop from="1" to="#ArrayLen(v)#" index="i">
					<cfif Left(v[i],5) eq "input">
						<cfset u=ArrayNew(1)>
						<cfset u=ListToArray(v[i],"#chr(34)#")>
						<cfbreak>
					</cfif>
				</cfloop>
				<cfset idW=u[6]>
				<cfset flag=0>

				<cfloop condition="flag eq 0">
					<cfif left(idW,8) eq "idWaiter">
						<cfset thread.sleep(5000)>
						<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Wait" 
								useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
								<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
								<cfhttpparam type="formfield" name="idWaiter" value="#idW#">
						</cfhttp>
						
						<cfset v=ArrayNew(1)>
						<cfset v=ListToArray(cfhttp.FileContent,"<")>
						
						<cfloop from="1" to="#ArrayLen(v)#" index="i">
							<cfif Left(v[i],5) eq "input">
								<cfset u=ArrayNew(1)>
								<cfset u=ListToArray(v[i],"#chr(34)#")>
								<cfbreak>
							</cfif>
						</cfloop>
						<cfset idW=u[6]>
						<cfset err=find("texteerreur",ArrayToList(v),1)>
						<cfif err gt 0>
							<cfbreak>
						</cfif>
					<cfelse>
						<cfset flag=1>
					</cfif>
				</cfloop>
				
				<!--- <cfset thread.sleep(6000)> --->
				
				
				
				
				<cfset v=ArrayNew(1)>
				<cfset v=ListToArray(cfhttp.FileContent,"<")>
				<cfset aFacture=ArrayNew(1)>
				<cfset compteur=1>
				<cfset compteur2=1>
				
				<cfset page=1>
				<cfset flagPage=0>
				
				<cfoutput>
				<cfloop from="1" to="#ArrayLen(v)#" index="i">
					<cfif left(v[i],1) eq "a">
						<cfset aFacture[compteur]=GetToken(v[i],6,"#chr(34)#")>
						<cfif Find("CSV",aFacture[compteur],1) gt 0>
							<cfset afichiers[compteur2]=aFacture[compteur]>
							<cfset compteur2=compteur2+1>
						</cfif>
						<cfif Find("goToPage",GetToken(v[i],4,"#chr(34)#"),1) gt 0>
							<cfset flagPage=1>
							<cfset pageMax=GetToken(GetToken(v[i],4,"#chr(34)#"),2,"'")>
						</cfif>
						<cfset compteur=compteur+1>
					</cfif>
				</cfloop>
				</cfoutput>
				
				<cfset aTotalFactures=afichiers>
				
				<cfif flagPage eq 1>
					<cfloop from="2" to="#pageMax#" index="i">
						
						<cfhttp method="POST" redirect="no" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/Facture" 
								useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
								<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
								<cfhttpparam type="formfield" name="tache" value="image">
								<cfhttpparam type="formfield" name="action" value="search">
								<cfhttpparam type="formfield" name="rapport" value="1">
								<cfhttpparam type="formfield" name="getAndsendmail" value="0">
								<cfhttpparam type="formfield" name="filename" value="get">
								<cfhttpparam type="formfield" name="ID_CLIENT" value="">
								<cfhttpparam type="formfield" name="LIBELLE_CLIENT" value="">
								<cfhttpparam type="formfield" name="systFact" value="ARBOR">
								<cfhttpparam type="formfield" name="listeFacture" value="ARBOR">
								<cfhttpparam type="formfield" name="melangeFacture" value="1">
								<cfhttpparam type="formfield" name="pageNum" value="#i#">
						</cfhttp>
						<cfset v=ArrayNew(1)>
						<cfset v=ListToArray(cfhttp.FileContent,"<")>
						<cfset aFacture=ArrayNew(1)>
						<cfset compteur=1>
						<cfoutput>
							<cfloop from="1" to="#ArrayLen(v)#" index="i">
								<cfif left(v[i],1) eq "a">
									<cfset aFacture[compteur]=GetToken(v[i],6,"#chr(34)#")>
									<cfif Find("CSV",aFacture[compteur],1) gt 0>
										<cfset aTotalFactures[compteur2]=aFacture[compteur]>
										<cfset compteur2=compteur2+1>
									</cfif>
									<cfset compteur=compteur+1>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfloop>
				</cfif>
				
			</cfif>
		<cfreturn aTotalFactures>
	</cffunction>
	
	<!--- Mï¿½thode pour aller tï¿½lï¿½charger le fichier --->
	<cffunction name="getFile" access="remote" returntype="string" output="true">
		<cfargument name="jsession" type="String">
		<cfargument name="ElementFacture" type="string">
		<cfargument name="compte" type="string">
		<cfset crm=GetToken(ElementFacture,2,"'")>
		<cfset numFact=GetToken(ElementFacture,4,"'")>
		<cfset date=GetToken(ElementFacture,6,"'")>
		<cfset fast=GetToken(ElementFacture,9,"'")>
		<cfset ordDoc=GetToken(ElementFacture,11,"'")>
		<cfset ordSDoc=GetToken(ElementFacture,13,"'")>
		<cfset filename=numFact & "_" & ordDoc & "_" & ordSDoc>
		<cfif ordSDoc eq "1">
			<cfquery datasource="ROCOFFRE" name="qListe">
				SELECT *
				FROM file_source t
				WHERE lower(t.extranet_filename)=lower('#filename#.zip')
				AND t.operateurid=534
			</cfquery>
			<cfif qListe.recordcount eq 0>
				<cfset retour=#filename# & ".zip">
				<cfhttp method="POST" redirect="no" getasbinary="yes" resolveurl="true" url="https://extranet.sfrbusinessteam.fr:443/extranet/servlet/CRMAccesFacture" 
					useragent=" Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)">
					<cfhttpparam type="cookie" name="JSESSIONID" value="#Ucase(jsession)#">
					<cfhttpparam type="formfield" name="tache" value="getFile">
					<cfhttpparam type="formfield" name="action" value="directSousDocCE">
					<cfhttpparam type="formfield" name="CRM" value="#crm#">
					<cfhttpparam type="formfield" name="numFact" value="#numFact#">
					<cfhttpparam type="formfield" name="dateEmis" value="#date#">
					<cfhttpparam type="formfield" name="refCli" value="#date#">
					<cfhttpparam type="formfield" name="fact" value="#fast#">
					<cfhttpparam type="formfield" name="idDoc" value="#fast#">
					<cfhttpparam type="formfield" name="ordDoc" value="#ordDoc#">
					<cfhttpparam type="formfield" name="ordSDoc" value="#ordSDoc#">
					<cfhttpparam type="formfield" name="format" value="CSV">
				</cfhttp>
				<cfdirectory action="list" directory="#variables.repertoire#" 
			recurse="false" name="qget" filter="#compte#">
				<cfif qget.recordcount eq 0>
					<cfdirectory action="create" directory="#variables.repertoire#/#compte#" mode="777">
				</cfif>
				<cffile mode="777" action="write" file="#variables.repertoire#/#compte#/#filename#.zip" output="#cfhttp.FileContent#">
				<cfzip action="list" file="#variables.repertoire#/#compte#/#filename#.zip" name="qFile"/>
				<cfquery datasource="ROCOFFRE" name="qListe">
					insert into
					file_source(extranet_filename,operateurid,date_recuperation,extranet_path,etat,date_mise_a_dispo,date_etat,source_filename)
					VALUES('#filename#.zip',534,sysdate,'#variables.repertoire#/#compte#',1,
							to_date('#lsDateFormat(qFile.DATELASTMODIFIED,"dd/mm/yyyy")#','dd/mm/yyyy'),sysdate,'#filename#.zip')
				</cfquery>
			<cfelse>
				<cfset retour="">
			</cfif>
		<cfelse>
			<cfset retour="">
		</cfif>
		<cfreturn retour>
	</cffunction>
</cfcomponent>