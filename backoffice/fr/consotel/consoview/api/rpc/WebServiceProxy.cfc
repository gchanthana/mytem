<cfcomponent name="WebServiceProxy" alias="fr.consotel.consoview.api.rpc.WebServiceProxy"
			hint="CFC Component and Service should be both accessible (Security,Session,etc...). Only String and Numeric parameter types (NO TYPE CHECKING)">
	<cffunction name="invokeComponentService" access="remote" returntype="any">
		<cfargument name="cfcComponent" required="true" type="string">
		<cfargument name="cfcService" required="true" type="string">
		<cfargument name="paramValueList" required="true" type="string" hint="Parameters values list (comma delimited)/Retrieve Infos By Introspection">
		<cftry>
			<cfset paramNamesArray=getCfcServiceInfos(arguments.cfcComponent,arguments.cfcService)>
			<cfset paramValuesArray=listToArray(arguments.paramValueList)>
			<cfif arrayLen(paramNamesArray) EQ arrayLen(paramValuesArray)>
				<cfinvoke component="#arguments.cfcComponent#" method="#arguments.cfcService#" returnvariable="resultObject">
					<cfloop index="paramIterator" from="1" to="#arrayLen(paramValuesArray)#">
						<cfset currentParamName=paramNamesArray[paramIterator]["NAME"]>
						<cfset currentParamValue=paramValuesArray[paramIterator]>
						<cfif UCase(paramNamesArray[paramIterator]["TYPE"]) EQ "NUMERIC">
							<cfinvokeargument name="#currentParamName#" value="#VAL(currentParamValue)#">
						<cfelse>
							<cfinvokeargument name="#currentParamName#" value="#currentParamValue#">
						</cfif>
					</cfloop>
				</cfinvoke>
			<cfelse>
				<cfset errorMessage="Function #arguments.cfcService# in #arguments.cfcComponent# requires #arrayLen(paramNamesArray)# argument(s)">
				<cfthrow type="CUSTOM" errorcode="#ILLEGAL_OPERATION()#" message="#errorMessage#">
			</cfif>
			<cfreturn resultObject>
			<cfcatch type="any">
				<cfreturn errorHandler(CFCATCH)>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<cffunction name="getCfcServiceInfos" access="private" returntype="array" hint="Throw a Custom Exception if fails">
		<cfargument name="cfcComponent" required="true" type="string">
		<cfargument name="cfcService" required="true" type="string">
		<cfset cfcInfos=getComponentMetaData(arguments.cfcComponent)>
		<cfset functionArray=cfcInfos["FUNCTIONS"]>
		<cfloop index="i" from="1" to="#arrayLen(functionArray)#">
			<cfif functionArray[i]["NAME"] EQ arguments.cfcService>
				<cfreturn functionArray[i]["PARAMETERS"]>
			</cfif>
		</cfloop>
		<cfthrow type="CUSTOM" errorcode="#CFC_SERVICE_NOT_FOUND()#" message="Unable to find Function #arguments.cfcService# in #arguments.cfcComponent#">
	</cffunction>
	
	<cffunction name="errorHandler" access="public" returntype="any" hint="Returns Custom XML Error Message">
		<cfargument name="exceptionObject" required="true" type="any">
		<cfxml variable="errorXmlMsg">
			<ERROR>
				<cfoutput>
				<ERROR_CODE>
					<cfif structKeyExists(arguments.exceptionObject,"CODE")>#CFCATCH.CODE#</cfif>
				</ERROR_CODE>
				<SERVERID>SUN-DEV-CEDRIC</SERVERID>
				<SERVER_NAME>#CGI.SERVER_NAME#</SERVER_NAME>
				<REMOTE_HOST>#CGI.REMOTE_HOST#</REMOTE_HOST>
				<REMOTE_ADDR>#CGI.REMOTE_ADDR#</REMOTE_ADDR>
				<REMOTE_USER>#CGI.REMOTE_USER#</REMOTE_USER>
				<TYPE>#arguments.exceptionObject.TYPE#</TYPE>
				<MSG>#arguments.exceptionObject.MESSAGE#</MSG>
				<MISSINGFILENAME>
					<cfif structKeyExists(arguments.exceptionObject,"MISSINGFILENAME")>#CFCATCH.MISSINGFILENAME#</cfif>
				</MISSINGFILENAME>
				</cfoutput>
			</ERROR>
		</cfxml>
		<cfreturn errorXmlMsg>
	</cffunction>

	<cffunction name="CFC_SERVICE_NOT_FOUND" access="public" returntype="string">
		<cfreturn "CFC_SERVICE_NOT_FOUND">
	</cffunction>
	
	<cffunction name="ILLEGAL_OPERATION" access="public" returntype="string">
		<cfreturn "ILLEGAL_OPERATION">
	</cffunction>
</cfcomponent>