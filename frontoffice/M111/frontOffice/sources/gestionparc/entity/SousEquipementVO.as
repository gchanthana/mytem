package gestionparc.entity
{

	[Bindable]
	public class SousEquipementVO
	{

		public var idDistributeur:int=-1;
		public var noSerieEnfant:String="";
		public var idEqp:int=0;
		public var distributeur:String="";
		public var idSousTete:int=0;
		public var idSim:int=0;
		public var ID:int=0;
		public var marque:String="";
		public var ligne:String="";
		public var noSerie:String="";
		public var modele:String="";
		public var typeEquipement:String="";
		public var categorie:String="";
		public var numSim:String = '';

		public function SousEquipementVO()
		{
		}

		public function fill(value:Object):void
		{
			this.idDistributeur=value.IDDISTRIBUTEUR;
			this.noSerieEnfant=value.NO_SERIE_ENFANT;
			this.idEqp=value.IDEQP;
			this.distributeur=value.DISTRIBUTEUR;
			this.idSousTete=value.IDSOUS_TETE;
			this.idSim=value.IDSIM;
			this.ID=value.ID;
			this.marque=value.MARQUE;
			this.ligne=value.LIGNE;
			this.noSerie=value.S_NO_SERIE;
			this.numSim = value.S_IMEI;
			this.modele=value.MODELE;
			this.typeEquipement=value.TYPE_EQUIPEMENT;
			this.categorie=value.CATEGORIE_EQUIPEMENT;
		}

	}
}
