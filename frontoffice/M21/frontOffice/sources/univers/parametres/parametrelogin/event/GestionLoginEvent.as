package univers.parametres.parametrelogin.event
{
	import flash.events.Event;
	
	
	/* INITIALISATION DU TYPE DEVENEMENT DES DISPATCHERS */
	public class GestionLoginEvent extends Event
	{

		public static const LOGIN_INFO_CHANGE_COMPLETE:String= "loginInfoChangeComplete"; //Changement des infos(libellé,code interne...)
		public static const LOGIN_ATTRIBUT_CHANGE_COMPLETE:String= "loginAttributChangeComplete";//Changement des sites afectés, de type de commandes affectés...)
		public static const PARAMETRE_LOGIN_COMPLETE:String= "parametreLoginComplete";
		public static const PARAMETRE_IP_COMPLETE:String= "parametreIPComplete";
		public static const DELETE_IP_COMPLETE:String= "deleteIPComplete";
		public static const DELETE_LOGIN:String= "deleteLogin";
		public static const SEND_MAIL_COMPLETE:String= "sendMailComplete";
		public static const SEND_PASS_COMPLETE:String= "sendPassComplete";
		public static const PARAMETRE_ABONNEMENT_COMPLETE:String= "parametreAbonnementComplete";
		
		public static const TRAITEMENT_ARBRE_COMPLETE:String= "traitementArbreComplete";
		public static const TRAITEMENT_COHERENCE_ARBRE_COMPLETE:String= "traitementCoherenceArbreComplete";
		public static const CREATE_ABONNEMENT_COMPLETE:String= "createAbonnementComplete";
		
		public static const LISTE_ACCES_COMPLETE:String= "listeAccesComplete";
		public static const INIT_ARBRE_COMPLETE:String= "initArbreComplete";
		public static const DELETE_ACCES_COMPLETE:String= "deleteAcces";
		public static const INFO_ACCES_COMPLETE : String="infoAccesComplete";
		public static const LISTE_LOGIN_COMPLETE : String="listeLoginComplete";
		public static const ATTCHEMENTRACINEINFOS : String="attchementRacineInfos";
		public static const LISTE_MODULE_COMPLETE : String="listeModuleComplete";
		
		public static const SUPPRIMER_PROFIL_COMPLETE : String = "SUPPRIMER_PROFIL_COMPLETE"
		public static const ENREGISTRER_PROFIL_COMPLETE : String = "ENREGISTRER_PROFIL_COMPLETE"
		public static const RECUP_PROFIL_COMPLETE : String = "RECUP_PROFIL_COMPLETE"
		public static const SAUVEGARDER_DROIT_COMPLETE : String = "SAUVEGARDER_DROIT_COMPLETE"
		public static const SAUVEGARDER_DROIT_ERROR : String = "SAUVEGARDER_DROIT_ERROR" 
			
		public static const ID_PROFIL_USER : String = "idProfilUser" 
			
		public static const LISTE_RACINE_RATTACHEMENT : String = "ListeRacineRattachement" 
			
		public var idLogin:int;
		
		public function GestionLoginEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,idLogin:int = -1)
		{
			super(type, bubbles, cancelable);
			this.idLogin = idLogin;
		}
		
	}
}