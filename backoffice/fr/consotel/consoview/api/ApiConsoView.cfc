<cfcomponent displayname="fr.consotel.consoview.api.ApiConsoView" hint="Provides access to a set of modules and services">
	<!--- (Auteur : Cedric)
		PARAMETERS KEY LIST REFERENCE : (http://nathalie.consotel.fr:3000/issues/418)
		================= Parameters structure =================
		{
			"EVENT_TARGET" = "(STRING) EVENT_TARGET element. DEFAULT : EMPTY STRING. MAX LENGTH : 35",
			"EVENT_HANDLER" = "(STRING) EVENT_HANDLER element. DEFAULT : EMPTY STRING. MAX_LENGTH : 3",
			"EVENT_TYPE" = "(STRING) EVENT_TYPE element. DEFAULT : EMPTY STRING. MAX_LENGTH : 3",
			"bipReport" = { BIP Report properties :
				"xdoAbsolutePath" = "XDO Absolute Path. DEFAULT : EMPTY STRING",
				"xdoTemplateId" = "XDO Template ID. DEFAULT : EMPTY STRING",
				"outputFormat" = "Report output format. DEFAULT : xml",
				"localization" = "Localization. DEFAULT : fr_FR"
				"reportParameters" = { List of XDO report parameters :
					"PARAMETER_NAME" = { XDO parameter name
						[Array of PARAMETER_NAME values]
					}
				},
				"delivery" = { Delivery properties
					"channel" = "Channel. Available values : FTP",
					"parameters" = { Channel parameters
						Depends on channel value
					}
				}
			}
		}
		================= FTP channel parameters =================
		{
			"ftpUser" = "FTP USERNAME (Case Insensitive). Available values : SERVICES, CONTAINER",
			"fileRelativePath" = "Report output file FTP USER relative path"
		}
	 --->
	
	<cffunction access="remote" name="invokeService" returntype="any" hint="Invokes a service returns its result if successful. Throws an exception if failed">
		<cfargument name="moduleId" type="string" required="true" hint="Module ID (e.g IBIS)">
		<cfargument name="serviceId" type="string" required="true" hint="Service ID (e.g scheduleReport)">
		<cfargument name="parameters" type="struct" required="true" hint="Structure of parameter (KEYS,VALUES)">
		<cfargument name="httpTarget" type="string" required="false" default="IBIS" hint="String value used to identify HTTP NOTIFICATION TARGET">
		<cftry>
			<cfset localParameters=ARGUMENTS.parameters>
			<cfset reportParamObject="">
			<cfset serviceName=VARIABLES.MODULES[ARGUMENTS.moduleId].SERVICES[ARGUMENTS.serviceId]>
			<cfset schedulePrototype=createObject("component",VARIABLES.MODULES[ARGUMENTS.moduleId].CLASS)>
			<cfset scheduleRequest=schedulePrototype.cloneScheduleRequest()>
			<!--- BIP REPORT --->
			<cfset scheduleRequest.reportRequest.reportAbsolutePath=localParameters.bipReport.xdoAbsolutePath>
			<cfset scheduleRequest.reportRequest.attributeTemplate=localParameters.bipReport.xdoTemplateId>
			<cfset scheduleRequest.reportRequest.attributeFormat=localParameters.bipReport.outputFormat>
			<cfset scheduleRequest.reportRequest.attributeLocale=localParameters.bipReport.localization>
			<cfset scheduleRequest.jobLocale=scheduleRequest.reportRequest.attributeLocale>
			<!--- REGISTER OBSERVER : BIP HTTP NOTIFICATION HANDLING --->
			<cftrace category="DEBUG" type="information" text="HTTP TARGET KEY : #ARGUMENTS.httpTarget#">
			<cfset scheduleRequest.httpNotificationServer=ARGUMENTS.httpTarget>
			<cfset scheduleRequest.userJobName=registerEventListener(localParameters)>
			<!--- DELIVERY --->
			<cfif NOT (UCASE(ARGUMENTS.serviceId) EQ UCASE("envoyer1RapportParMail"))>
				<cfset scheduleRequest.deliveryRequest.ftpOption.ftpUserName=localParameters.bipReport.delivery.parameters.ftpUser>
				<cfset scheduleRequest.deliveryRequest.ftpOption.ftpUserPassword=localParameters.bipReport.delivery.parameters.ftpPassword>
				<cfset scheduleRequest.deliveryRequest.ftpOption.remoteFile=localParameters.bipReport.delivery.parameters.fileRelativePath>
			</cfif>
			<!--- REPORT PARAMETERS --->
			<cfif structKeyExists(localParameters.bipReport,"reportParameters")>
				<cfloop item="paramName" collection="#localParameters.bipReport.reportParameters#">
					<cfset reportParamObject=structNew()>
					<cfset reportParamObject.name=paramName>
					<cfif NOT isValid("Array",localParameters.bipReport.reportParameters[paramName].parameterValues)>
						<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT" message="invokeService() : Values for parameter #paramName# is not a valid ARRAY">
					<cfelse>
						<cfset reportParamObject.values=localParameters.bipReport.reportParameters[paramName].parameterValues>
					</cfif>
					<cfset reportParamObject.multiValuesAllowed=(arrayLen(reportParamObject.values) GT 0)>
					<cfif arrayAppend(scheduleRequest.reportRequest.parameterNameValues,reportParamObject) EQ FALSE>
						<cfthrow type="Custom" errorcode="ILLEGAL_STATE" message="invokeService() : Unable to append parameter #paramName#">
					</cfif>
				</cfloop>
			<cfelse>
				<cftrace category="DEBUG" type="warning" text="invokeService(#ARGUMENTS.moduleId#,#ARGUMENTS.serviceId#) : Using default report parameters values">
			</cfif>
			<!--- Outrepasser le cache de documents pour le rapport --->
			<cfset scheduleRequest.reportRequest.byPassCache=TRUE>
			<cfif UCASE(ARGUMENTS.serviceId) EQ UCASE("envoyer1RapportParMail")>
				<cfset scheduleRequest=envoyer1RapportParMailAdapter(scheduleRequest,localParameters)>
			</cfif>
			<cfreturn EVALUATE(serviceName & "(scheduleRequest)")>
			<cfcatch type="any">
				<cftrace category="DEBUG" type="error" text="FAILED : invokeService(#ARGUMENTS.moduleId#,#ARGUMENTS.serviceId#)">
				<cfrethrow />
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction access="remote" name="cloneParameters" returntype="struct" hint="Returns a DEEP COPY of parameters with default values">
		<cfreturn DUPLICATE(VARIABLES.PARAMETERS_PROTOTYPE)>
	</cffunction>
	
	<cffunction access="private" name="initInstance" returntype="void" hint="Initialize this instance. Called explicitly by this instance at creation">
		<!--- API PARAMETER PROTOTYPE --->
		<cfset VARIABLES.PARAMETERS_PROTOTYPE=structNew()>
		<!--- NOTIFICATION HANDLING --->
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.EVENT_TARGET="">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.EVENT_HANDLER="">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.EVENT_TYPE="API">
		<!--- BIP REPORT --->
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport=structNew()>
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.xdoAbsolutePath="">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.xdoTemplateId="">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.outputFormat="xml">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.localization="fr_FR">
		<!--- REPORT PARAMETERS --->
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.reportParameters=structNew()>
		<!--- DELIVERY --->
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.delivery=structNew()>
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.delivery.channel="FTP">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.delivery.parameters=structNew()>
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.delivery.parameters.ftpUser="">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.delivery.parameters.ftpPassword="">
		<cfset VARIABLES.PARAMETERS_PROTOTYPE.bipReport.delivery.parameters.fileRelativePath="">
	</cffunction>
	
	<cffunction access="private" name="registerEventListener" returntype="string" hint="Return a string value used for JOB_NAME">
		<cfargument name="parameters" type="struct" required="true" hint="Structure of parameter (KEYS,VALUES)">
		<cfset var eventTarget="">
		<cfset var eventType="API">
		<cfset var eventHandler="">
		<cfif structKeyExists(ARGUMENTS.parameters,"EVENT_TARGET")>
			<cfif LEN(ARGUMENTS.parameters.EVENT_TARGET) LTE 35>
				<cfset eventTarget=ARGUMENTS.parameters.EVENT_TARGET>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT" message="registerEventListener() : EVENT_TARGET has an invalid LENGTH">
			</cfif>
		</cfif>
		<cfif structKeyExists(ARGUMENTS.parameters,"EVENT_TYPE")>
			<cfif LEN(ARGUMENTS.parameters.EVENT_TYPE) LTE 3>
				<cfset eventType=ARGUMENTS.parameters.EVENT_TYPE>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT" message="registerEventListener() : EVENT_TYPE has an invalid LENGTH">
			</cfif>
		</cfif>
		<cfif structKeyExists(ARGUMENTS.parameters,"EVENT_HANDLER")>
			<cfif LEN(ARGUMENTS.parameters.EVENT_HANDLER) LTE 3>
				<cfset eventHandler=ARGUMENTS.parameters.EVENT_HANDLER>
			<cfelse>
				<cfthrow type="Custom" errorcode="ILLEGAL_ARGUMENT" message="registerEventListener() : EVENT_HANDLER has an invalid LENGTH">
			</cfif>
		</cfif>
		<cfreturn eventTarget & "|" & eventType & "|" & eventHandler>
	</cffunction>
	
	<cffunction access="private" name="envoyer1RapportParMailAdapter" returntype="struct" hint="ADAPTER for envoyer1RapportParMail parameters">
		<cfargument name="scheduleRequest" type="struct" required="true" hint="Schedule Request to adapt (Mail Delivery)">
		<cfargument name="parameters" type="struct" required="true" hint="Structure of parameter (KEYS,VALUES)">
		<cfset var adaptedScheduleRequest=ARGUMENTS.scheduleRequest>
		<cfset var adaptedDeliveryRequest=structNew()>
		<cfset var localParameters=ARGUMENTS.parameters>
		<cfset adaptedDeliveryRequest.emailOption=structNew()>
		<cfif structKeyExists(localParameters,"emailFrom")>
			<cfset adaptedDeliveryRequest.emailOption.emailFrom=localParameters.emailFrom>
		<cfelse>
			<cfset adaptedDeliveryRequest.emailOption.emailFrom=#CGI.SERVER_NAME# & "@consotel.fr">
		</cfif>
		<cfif structKeyExists(localParameters,"emailTo")>
			<cfset adaptedDeliveryRequest.emailOption.emailTo=localParameters.emailTo>
		<cfelse>
			<cfset adaptedDeliveryRequest.emailOption.emailTo="monitoring@saaswedo.com">
		</cfif>
		<cfif structKeyExists(localParameters,"emailCC")>
			<cfset adaptedDeliveryRequest.emailOption.emailCC=localParameters.emailCC>
		</cfif>
		<cfif structKeyExists(localParameters,"emailSubject")>
			<cfset adaptedDeliveryRequest.emailOption.emailSubject=localParameters.emailSubject>
		<cfelse>
			<cfset adaptedDeliveryRequest.emailOption.emailSubject="envoyer1RapportParMail">
		</cfif>
		<!--- DELETE UNUSED PARAMETERS KEYS --->
		<cfif structKeyExists(adaptedScheduleRequest,"httpNotificationServer")>
			<cfset structDelete(adaptedScheduleRequest,"httpNotificationServer")>
		</cfif>
		<cfif structKeyExists(adaptedScheduleRequest,"deliveryRequest")>
			<cfset structDelete(adaptedScheduleRequest,"deliveryRequest")>
		</cfif>
		<!--- ADAPT PARAMETERS --->
		<cfset adaptedScheduleRequest.userJobName="envoyer1RapportParMail">
		<cfset adaptedScheduleRequest.deliveryRequest=adaptedDeliveryRequest>
		<cftrace type="warning" category="WARNING" text="+++++++++++++++ USE OF DEPRECATED envoyer1RapportParMail +++++++++++++++">
		<cfreturn adaptedScheduleRequest>
	</cffunction>
</cfcomponent>