package entity.vo
{
	import mx.resources.ResourceManager;
    [Bindable]
    public class SimpleEntityVO extends ValueObject
    {
        private var _type:String;
        private var _label:String;
        private var _value:Number;

        public function SimpleEntityVO()
        {
            super();
        }

        public override function deserialize(object:Object):IValueObject
        {
            // this.type = object.TYPE;
            this.label = object.LABEL;
            this.value = object.VALUE;
            return this;
        }

        public override function serialize(object:IValueObject):Object
        {
            return null;
        }

        public override function toString():String
        {
            return "EntityVO :  " + type + " valeur " + value + "\n" + "label :" + label + " \n";
        }

        public override function toXML():XML
        {
            var xml:XML = voToXml(this, "SimpleEntityVO");
            return xml;
        }

        public override function decode(object:Object):IValueObject
        {
            var vo:SimpleEntityVO = new SimpleEntityVO();
            vo.label = object.SimpleEntityVO.label;
            vo.value = object.SimpleEntityVO.value;
            vo.type = object.SimpleEntityVO.type;
            return vo;
        }

        public function get type():String
        {
            return _type;
        }

        public function set type(value:String):void
        {
            _type = value;
        }

        public function get label():String
        {
            return _label;
        }

        public function set label(value:String):void
        {
            _label = value;
        }

        public function get value():Number
        {
            return _value;
        }

        public function set value(value:Number):void
        {
            _value = value;
        }
    }
}