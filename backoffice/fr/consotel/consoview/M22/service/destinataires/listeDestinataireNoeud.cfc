<cfcomponent output="false">
	
	<cffunction access="public" name="ListeDestNoeud"  returntype="query" output="true">
	
		<cfargument name="id_noeud" type="numeric" required="true">
	
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m22.getDestNoeud">		
			<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#id_noeud#" variable="p_idnoeud">
			<cfprocresult name="qListeDestNoeud">		
		</cfstoredproc>	
	
		<cfreturn qListeDestNoeud><!--- le resultat de la procedure --->
		
	</cffunction>
	
</cfcomponent>