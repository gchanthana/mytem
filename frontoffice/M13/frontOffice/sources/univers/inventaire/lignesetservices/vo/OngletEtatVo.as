package univers.inventaire.lignesetservices.vo
{
	import fr.consotel.consoview.util.xml.ItoXMLParams;
	import fr.consotel.consoview.util.xml.XmlParamUtil;
		
	[Bindable]
	public class OngletEtatVo implements ItoXMLParams
	{
		public var IDSOUS_TETE:Number = 0;
		
		protected var _xmlParams : XML;
		
		public var COMMENTAIRE_ETAT:String="";
		//
		public var TYPE_RACCORDEMENT:String = "Raccordé présélection";
		public var IDTYPE_RACCORDEMENT:Number = 0;
		
		public var OPERATEUR_CREATEUR_NUMERO:String="";
		public var IDOPERATEUR_CREATEUR_NUMERO:Number = -1;
				
		//
		public var OPERATEUR_ACCES:String="";
		public var IDOPERATEUR_ACCES:Number=0;
		
		
		//Facturation
		public var OPERATEUR_ACCES_FACTURANT:String="";
		public var IDOPERATEUR_ACCES_FACTURANT:Number=0;
		
		
				
		public var IDCOMPTE_FACTURATION_ACTUEL:Number=0;		
		public var COMPTE_FACTURATION_ACTUEL:String="";
		
		public var IDSOUS_COMPTE_ACTUEL:Number=0;
		public var SOUS_COMPTE_ACTUEL:String="";
		public var IDENTIFIANT_AGENCE:Number=-1;		
		
		//Divers
		public var BOOL_TITULAIRE:Number = 0;
		public var BOOL_PAYEUR:Number = 0;
		
		public var V3:String = "";
		public var V4:String = "";
				
		public function toXMLParams(registerInLog:int=0):XML
		{
			XmlParamUtil.addParam(_xmlParams,"IDSOUS_TETE","l'identifiant de la ligne",IDSOUS_TETE);	
					
			XmlParamUtil.addParam(_xmlParams,"TYPE_RACCORDEMENT","le type de raccordement ou mode d'acces",TYPE_RACCORDEMENT);			
			XmlParamUtil.addParam(_xmlParams,"IDTYPE_RACCORDEMENT","l'id du type de raccordent",IDTYPE_RACCORDEMENT);
			
			
			XmlParamUtil.addParam(_xmlParams,"OPERATEUR_CREATEUR_NUMERO","le nom de l'opérateur qui a créér le numéro",OPERATEUR_CREATEUR_NUMERO);
			XmlParamUtil.addParam(_xmlParams,"IDOPERATEUR_CREATEUR_NUMERO","l'id de l'opérateur qui a créér le numéro",IDOPERATEUR_CREATEUR_NUMERO);
			
			XmlParamUtil.addParam(_xmlParams,"OPERATEUR_ACCES","le nom de l'opérateur d'acces désigné",OPERATEUR_ACCES);
			XmlParamUtil.addParam(_xmlParams,"IDOPERATEUR_ACCES","l'id de l'opérateur d'acces désigné",IDOPERATEUR_ACCES);
			
			
			XmlParamUtil.addParam(_xmlParams,"OPERATEUR_ACCES_FACTURANT","le nom de l'opérateur d'acces rapporté de la facturation",OPERATEUR_ACCES_FACTURANT);
			XmlParamUtil.addParam(_xmlParams,"IDOPERATEUR_ACCES_FACTURANT","l'id de l'opérateur d'acces rapporté de la facturation",IDOPERATEUR_ACCES_FACTURANT);
			
			
			XmlParamUtil.addParam(_xmlParams,"COMPTE_FACTURATION_ACTUEL","le compte de facturation actuel sur lequel l'opérateur d'accés rapporté facture",COMPTE_FACTURATION_ACTUEL);
			XmlParamUtil.addParam(_xmlParams,"IDCOMPTE_FACTURATION_ACTUEL","l'id du compte de facturation actuel sur lequel l'opérateur d'accés rapporté facture",IDCOMPTE_FACTURATION_ACTUEL);
			
			
			XmlParamUtil.addParam(_xmlParams,"SOUS_COMPTE_ACTUEL","le sous compte de facturation actuel sur lequel l'opérateur d'accés rapporté facture",SOUS_COMPTE_ACTUEL);
			XmlParamUtil.addParam(_xmlParams,"IDSOUS_COMPTE_ACTUEL","l'id du sous compte de facturation actuel sur lequel l'opérateur d'accés rapporté facture",IDSOUS_COMPTE_ACTUEL);
			
			XmlParamUtil.addParam(_xmlParams,"COMMENTAIRE_ETAT","des commentaires sur l'état",COMMENTAIRE_ETAT);
			
			
			XmlParamUtil.addParam(_xmlParams,"BOOL_TITULAIRE","Un booléen",BOOL_TITULAIRE);
			XmlParamUtil.addParam(_xmlParams,"BOOL_PAYEUR","Un booléen",BOOL_PAYEUR);
			
			XmlParamUtil.addParam(_xmlParams,"V3","Champ perso 3",V3);
			XmlParamUtil.addParam(_xmlParams,"V4","Champ perso 4",V4);
						
			return _xmlParams;
		}
		
	}
}