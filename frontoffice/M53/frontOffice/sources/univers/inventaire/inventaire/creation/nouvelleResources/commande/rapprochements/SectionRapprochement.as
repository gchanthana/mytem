package univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	import composants.util.ConsoviewFormatter;
	import composants.util.ConsoviewUtil;
	import composants.util.DateFunction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.Label;
	import mx.controls.TextInput;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.events.CloseEvent;
	import mx.events.DataGridEvent;
	import mx.rpc.AbstractOperation;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.Commande;
	import univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets.ElementCommande;
	
	/**
	 * Classe gerant le rapprochement de produits
	 * */
	 [Bindable]
	public class SectionRapprochement extends Canvas
	{
		/**
		 * Reference vers le DataGrid contenant les produits de la commande
		 * */
		public var grdPdtsDemande : DataGrid;
		
		/**
		 * Reference vers le DataGrid contenant les ressources de la facturation
		 * */
		public var grdPdtsFactures : DataGrid;
		
		/**
		 * Reference vers le boutton 'R'approcher
		 * */
		public var btRapp : Button;
		
		/**
		 * Reference vers le boutton 'Annuler le Rapprochement'
		 * */
		public var btAnnRapp : Button;
		
		
		/**
		 * Reference vers la ComboBox 'technique de rapprochement'
		 * */
		public var cbRecherche : ComboBox;
		
		/**
		 * Reference vers le Label 'Resultat de la Recherche'
		 * */
		public var lblRechercheResult : Label;
		
		
		/**
		 * Reference vers le filtre du DataGrid des produits de la commande
		 * */
		public var txtFiltreCom : TextInputLabeled;
		
		/**
		 * Reference vers le filtre du DataGrid des ressources de la facturation
		 * */
		public var txtFiltreFact : TextInputLabeled;
		
		
		/**
		 * Retourne une reference vers le produit en cours de rapprochement
		 * @return produit 
		 * */
		public function get produitRapproche():ElementCommande{
			return produit;
		}
		
		/**
		 * Retourne une reference vers le produit sur lequel on annule le rapprochement
		 * @return produitAnnule 
		 * */
		public function get produitRapprocheAnnule():Object{
			return produitAnnule;
		}
		
		
		/**
		 * Constante definissant le type d'evenement dispatché lors d'un rapprochement de produits
		 * */
		public static const RAPPROCHER : String = "rapprochementComplete";
		
		
		/**
		 * Constante definissant le type d'evenement dispatché lors d'une annulation de rapprochement
		 * */
		public static const ANNULER_RAPPROCHEMENT : String = "annulerRapprochementComplete";		
		
		
		/**
		 * Reference vers l'ArrayCollection contenant la liste des produits de la facturation
		 * */
		public var listeProduitFact : ArrayCollection = new ArrayCollection();
		
		
		// Reference vers le produit de la demande en cours de rapprochement
		private var produit : ElementCommande;
		
		
		//Reference vers le produit sur qui on annule le rapprochement;		
		private var produitAnnule : Object;
		
		
		//Refernece vers le type d'action Rapprocher
		private const RAPP : int = 1;
		
		
		//Refernece vers le type d'action Annuler le Rapprochement
		private const AN_RAPP : int = 0;
		
		
		//Refernece vers le type d'action Annuler tous les Rapprochements
		private const AN_TT_RAPP : int = 3;
		
		
		//Refernece vers le type d'action courrant
		private var currentAction : int;
		
		
		//Reference locale vers la commande
		private var _commande : Commande;
		public function get commande():Commande{
			return _commande;
		}
		public function set commande(cmde:Commande):void{
			_commande = cmde;
		}
		
		//Reference locale vers le panier
		protected var _panier : ElementCommande;
		public function get panier():ElementCommande{
			return _panier;
		}
		public function set panier(ec:ElementCommande):void{
			_panier = ec;
		}
		
		
		//Reference vers la methode distante chargée de rapprocher les produits
		private var opRap : AbstractOperation;
		
		/**
		 * Constructeur
		 * */
		public function SectionRapprochement()
		{
			super();
			Alert.okLabel = "Fermer";
			Alert.buttonWidth = 100;
			
		}
		
		/**
		 * Affecte la commande et le panier 
		 * @param commande, la commande
		 * @param panier le panier de la commande
		 * */
		public function setCommande(cmde : Commande,p : ElementCommande):void{
			commande = cmde;
			panier = p;		
			//panier.liste.refresh()			
			panier.addEventListener(ElementCommande.LISTE_COMPLETE,rafraichirGrids);
		}
		
		/**
		 * Ne fait rien
		 * */
		public function clean():void{
			 
		}
		
		/**
		 * Annule tous les rapprochements
		 * */
		public function annuler():void{
			processAnnulerTousLesRapprochement();
		}
		
		/**
		 * Retourne un tableau avec les produits rapprochés
		 * @return newArray 
		 * */
		public function getProduitsRapp():Array{
			var source : Array = panier.liste.source;
			var len : int = source.length;			
			var newArray : Array = new Array();
			
			for (var i : int = 0; i < len; i++){								
				if (ElementCommande(source[i]).flagRapproche == RAPP)
						newArray.push(ElementCommande(source[i]).ressourceID);	
			}
			return newArray;			
		}
				
		//--- FORMAT---------------------
		
		/**
		 * Formate les dates d'une colonne d'un DataGrid
		 * Voir la propriété LabelFunction d'un DataGrid pour les parametre
		 * */
		protected function formatDate(item : Object,column : DataGridColumn):String{
			return DateFunction.formatDateAsString(new Date(item[column.dataField]));
		}
		
		/**
		 * Formate les numéros de téléophone d'une colonne d'un DataGrid
		 * Voir la propriété LabelFunction d'un DataGrid pour les parametre
		 * */
		protected function formatPhoneNumber(item : Object,column : DataGridColumn):String{		
		 
			if (isNaN(item[column.dataField])){
				return item[column.dataField];
			}else{
				return ConsoviewFormatter.formatPhoneNumber(item[column.dataField]);		
			}	
			
		}
		
		/**
		 * Formate les flag rapproché ou non rapproché d'une colonne de DataGrid
		 * Voir la propriété LabelFunction d'un DataGrid pour les parametre
		 * */
		protected function formatRap(item : Object,column : DataGridColumn):String{		
			switch(Number(item[column.dataField])){
				case 0 : return "NR"; break;
				case 1 : return "R";break;
				case 2 : return "RC";break;
				default : return "NR";break ;
			}	 			
		}
		
		//-----FILTRE
		
		/**
		 * Gere le filtre du DataGrid des produits de la commande
		 * */
		protected function filtrerGridCom(ev : Event):void{
			if (grdPdtsDemande.dataProvider != null){
				panier.liste.filterFunction = filterFuncCom;
				panier.liste.refresh();	
			}			
		}
		
		//filtre les elements d'un DataGrid sur les propriétés libelleLigne,libelleProduit,commentaire et flagRapproche
		private function filterFuncCom(value : ElementCommande):Boolean{
			if ((String(value.libelleLigne).toLowerCase().search(txtFiltreCom.text.toLowerCase()) != -1)
				||
				(String(value.libelleProduit).toLowerCase().search(txtFiltreCom.text.toLowerCase()) != -1)
				||
				(String(value.commentaire).toLowerCase().search(txtFiltreCom.text.toLowerCase()) != -1)
				||
				((Number(value.flagRapproche) == 0)&& ("nr".search(txtFiltreCom.text.toLowerCase()) != -1))
				||
				((Number(value.flagRapproche) == 1)&& ("r".search(txtFiltreCom.text.toLowerCase()) != -1)))
			{
				return true;
			}else{
				return false;
			}
				
		}
		
		/**
		 * Gere le filtre du DataGrid des ressources 
		 * */
		protected function filtrerGridFact(ev : Event):void{
			if (grdPdtsFactures.dataProvider != null){
				(grdPdtsFactures.dataProvider as ArrayCollection).filterFunction = filterFuncFact;
				(grdPdtsFactures.dataProvider as ArrayCollection).refresh();	
			}			
		}
		
		//filtre les elements d'un DataGrid sur les propriétés libelleLigne,libelleProduit,commentaire et flagRapproche
		private function filterFuncFact(value : ElementCommande):Boolean{
			if ((String(value.libelleLigne).toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1)
				||
				(String(value.libelleProduit).toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1)
				||
				(String(value.commentaire).toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1)
				||
				(String(value.flagRapproche).toLowerCase().search(txtFiltreFact.text.toLowerCase()) != -1))
			{
				return true;
			}else{
				return false;
			}
				
		}
		
		/**
		 * Modifie le libelle de ligne dans le DataGrid des produits d'une commande
		 * */
		protected function modifierCetteLigne(de : DataGridEvent):void{	
			
			if (grdPdtsDemande.dataProvider != null){										
				
				
				var myEditor:TextInput = TextInput(de.currentTarget.itemEditorInstance);                
                // Get the new value from the editor.
                var newVal:String = myEditor.text;	                
                // Get the old value.
                var oldVal:String = de.currentTarget.editedItemRenderer.data[de.dataField];                          					
				
				ElementCommande(grdPdtsDemande.selectedItem).addEventListener(ElementCommande.UPDATE_COMPLETE,panierUpDated);
				ElementCommande(grdPdtsDemande.selectedItem).commentaire = newVal;					
				if (commande.commandeID > 0){
					ElementCommande(grdPdtsDemande.selectedItem).update();	
				}
																		 					
			}else{		
				de.preventDefault();					
			}   
		}
		
		
		/**
		 * Recharge les données du panier
		 * */
		private function panierUpDated(ev : Event):void{
			panier.prepareList(commande.commandeID);
		}
		
		
		//rapprocher---------------------------------------------------------
		private var listeResoourcesRapprochees : ArrayCollection = new ArrayCollection();
		
		
		
		/**
		 * Verifie que l'on a sélectionné un produit de la commande et une ressource avant de faire un rapprochement
		 * */		
		protected function btRappClickHAndler(me : MouseEvent):void{
			if ((grdPdtsDemande.selectedIndex != -1)&&(grdPdtsFactures.selectedIndex != -1)){
				
				if(ElementCommande(grdPdtsFactures.selectedItem).flagRapproche != 1){
					
					switch(ElementCommande(grdPdtsDemande.selectedItem).flagRapproche){
						case 0 :ConsoviewAlert.afficherAlertConfirmation("Etes-vous sur de vouloir rapprocher ce produit","Confirmation !",rapprocherLigneConfirmationHandler);break;
						case 1 :Alert.show("Impossible de rapprocher ce produit.","Infos");break;
						case 2 :Alert.show("Impossible de rapprocher ce produit.","Infos !");;break;break;
						default:Alert.show("Impossible de rapprocher ce produit.","Infos");break;
					}
					
					
					
				}else{
					Alert.show("Impossible de rapprocher ce produit.");
				}
			}else{
				Alert.show("Vous devez sélectionner un produit dans chaque tableau");
			}
		}
		
		private function rapprocherLigneConfirmationHandler(ce : CloseEvent):void{			
			if (ce.detail == Alert.OK){
				processRapprochement();		
			}	
		}
		
		
				
		//Rapproche le produit selectioné avec la ressource choisie
		private function processRapprochement():void{			
			produit = null;			
			produit = ElementCommande(grdPdtsDemande.selectedItem);
			produit.addEventListener(ElementCommande.RAPPROCHER_COMPLETE,rapprocherHandler);
			produit.addEventListener(ElementCommande.RAPPROCHER_ERROR,rapprocherErrorHandler);
			produit.rapprocher(ElementCommande(grdPdtsFactures.selectedItem).ressourceID);
		};
		
		//Recharge les données du panier
		private function rapprocherHandler(ev : Event):void{			
			currentAction = RAPP;
			
			//Mettre à jour les donées du grid des factures
			var elementFact : ElementCommande = ElementCommande(grdPdtsFactures.selectedItem);			
			elementFact.flagRapproche = 1;			
			(grdPdtsFactures.dataProvider as ArrayCollection).itemUpdated(elementFact);
			
			
			
			//Mettre à jour les données du grid des nouvelle_lignes
			/* var elementCmde : ElementCommande = ElementCommande(grdPdtsDemande.selectedItem);						 
			
			elementCmde.libelleLigne = elementFact.libelleLigne;
			elementCmde.libelleProduit = elementFact.libelleProduit;
			elementCmde.numLigne = elementFact.numLigne;
			elementCmde.libelletheme = elementFact.libelletheme;
						
			elementCmde.boolAccess = elementFact.boolAccess;			
			elementCmde.flagRapproche = 1;
									
			elementCmde.produitCatID = elementFact.produitCatID;
			elementCmde.ressourceID = elementFact.ressourceID;
			elementCmde.ligneID = elementFact.ligneID;
			elementCmde.themeID = elementFact.themeID;
			
			elementCmde.quantite = elementFact.quantite;
			
			panier.liste.itemUpdated(elementCmde); */
			panier.prepareList(commande.commandeID);
		}
		
		//Affiche un fenêtre d'alerte quand un rapprochement échoue
		private function rapprocherErrorHandler(ev : Event):void{
			Alert.okLabel = "Fermer";			
			Alert.show("Erreur lors du rapprochement");			
		}
		
		
		//annulerRapprochement -----------------------------------------------
		
		/**
		 * Verifie que l'on a sélectionné un produit avant d'annuler un rapprochement
		 * */
		protected function btAnnRappClickHandler(me : MouseEvent):void{
			if (grdPdtsDemande.selectedIndex != -1){
				var elementCommande : ElementCommande = ElementCommande(grdPdtsDemande.selectedItem);
				if (elementCommande.ligneID > 0 ){
					switch(elementCommande.flagRapproche){
						case 0 :Alert.show("Le produit n'a pas été rapproché","Infos !");break;
						case 1 :processAnnulerRapprochement();break;
						case 2 :Alert.show("Impossible d'annuler le rapprochement pour ce produit.","Infos !");break;break;
						default:break;
					}
				}else{
					Alert.show("Impossible d'annuler le rapprochement","Infos !");
				}
			}
		}
		
		//Annule le rapprochement sur  le produit selectioné
		private function processAnnulerRapprochement():void{
			
			
			produit = null; 			 
			produit = ElementCommande(grdPdtsDemande.selectedItem);
			produitAnnule = ObjectUtil.copy(produit);			
			produit.addEventListener(ElementCommande.ANNULER_RAPPROCHER_COMPLETE,annulerRapprochementHandler);			
			produit.addEventListener(ElementCommande.ANNULER_RAPPROCHER_ERROR,annulerRapprochementErrorHandler);						
			produit.annulerRapprochement();
		};
		
		//Annule le rapprochement sur  tous les produits du panier 
		private function processAnnulerTousLesRapprochement():void{
		 	currentAction = AN_TT_RAPP;
		 	var arrayRapp : Array = [];
		 	
		 	for (var i : int; i <  panier.liste.source.length ; i++){
				if (panier.liste.source[i].flagRapproche == 1){
					arrayRapp.push(panier.liste.source[i].elementID);
				}		 	
		 	}
		 	 							
		 							
			panier.annulerTousLesRapprochements(arrayRapp,commande.commandeID);
		};
		
		
		//Affiche un fenêtre d'alerte quand une annulation de rapprochement échoue
		private function annulerRapprochementErrorHandler(ev : Event):void{
			
			produit.removeEventListener(ElementCommande.ANNULER_RAPPROCHER_COMPLETE,annulerRapprochementHandler);			
			produit.removeEventListener(ElementCommande.ANNULER_RAPPROCHER_ERROR,annulerRapprochementErrorHandler);
			
			Alert.show("Erreur lors de l'annulation");			
		}
		
		
		//Recharge les données du panier
		private function annulerRapprochementHandler(ev : Event):void{
			
			produit.removeEventListener(ElementCommande.ANNULER_RAPPROCHER_COMPLETE,annulerRapprochementHandler);			
			produit.removeEventListener(ElementCommande.ANNULER_RAPPROCHER_ERROR,annulerRapprochementErrorHandler);
			
			currentAction = AN_RAPP;
			
			//Mettre à jour les donées du grid des factures			
			
			
			
			var index : Number = ConsoviewUtil.getIndexByIdInArray(listeProduitFact.source,"ressourceID",ElementCommande(grdPdtsDemande.selectedItem).ressourceID)
			if (index >= 0){
				listeProduitFact.source[index].flagRapproche = 0;
				listeProduitFact.itemUpdated(ElementCommande(grdPdtsFactures.selectedItem));	
			}
			
			
			//Mettre à jour les données du grid des nouvelle_lignes
			/* var elementCmde : ElementCommande = ElementCommande(grdPdtsDemande.selectedItem);						 
			
			elementCmde.libelleLigne = elementFact.libelleLigne;
			elementCmde.libelleProduit = elementFact.libelleProduit;
			elementCmde.numLigne = elementFact.numLigne;
			elementCmde.libelletheme = elementFact.libelletheme;
						
			elementCmde.boolAccess = elementFact.boolAccess;			
			elementCmde.flagRapproche = 1;
									
			elementCmde.produitCatID = elementFact.produitCatID;
			elementCmde.ressourceID = elementFact.ressourceID;
			elementCmde.ligneID = elementFact.ligneID;
			elementCmde.themeID = elementFact.themeID;
			
			elementCmde.quantite = elementFact.quantite;
			
			panier.liste.itemUpdated(elementCmde);*/
			
			panier.prepareList(commande.commandeID);
		}
		
		
		//Rafraichit le tableau des produits de la commande
		private function rafraichirGrids(ev : Event):void{
			
			if (produit != null){
				var index : int = ConsoviewUtil.getIndexById(panier.liste,"elementID",produit.elementID);
				if (index > -1){
					grdPdtsDemande.selectedItem = panier.liste.getItemAt(index);
					grdPdtsDemande.selectedIndex = index;
					grdPdtsDemande.scrollToIndex(index);
					produit = ElementCommande(panier.liste.getItemAt(index));	
					grdPdtsDemande.executeBindings(true);	
				}
				
				 
				
				switch (currentAction){
					case RAPP : dispatchEvent(new Event(SectionRapprochement.RAPPROCHER));break;
					case AN_RAPP : {
						if (produitAnnule	!= null){						
									
							if ((!ConsoviewUtil.isIdInArray(produitAnnule.ligneID,"ligneID",panier.liste.source))){
								dispatchEvent(new Event(SectionRapprochement.ANNULER_RAPPROCHEMENT));
							}
						}
						
						break;
					}
				}
			}
		}
		
		//-Recherche des nouveaux produits--------------------------------------		
		
		//
		protected function imgRechercherClickHandler(me : MouseEvent):void{
			if (cbRecherche.selectedItem != null){
				rechercherLigneFacturation();
			}
		}
		
		//Charge les produits de la facturation suivant la technique de recherche choisie
		protected function rechercherLigneFacturation():void{
			lblRechercheResult.visible = false;
			
			var methode : String = "";			
			switch(cbRecherche.selectedItem.value){
				case GestionRappObject.BYLIBELLE : {
					methode = GestionRappObject.getNouveauxProduitsByLibelle;
					break;
				}
				case GestionRappObject.BYCIBLE : {
					methode = GestionRappObject.getNouveauxProduitsByCible;
					break;
				}
				case GestionRappObject.BYOP: {
					methode = GestionRappObject.getNouveauxProduitsByOP
					break;
				}
				default :{
					methode = GestionRappObject.getNouveauxProduitsByLibelle;
					break;
				}				
			}
			
			opRap = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													GestionRappObject.RemoteName,
													methode,
													rechercherLigneFacturationResultHandler,null);

			RemoteObjectUtil.callService(opRap,commande.commandeID);	
		}
		

		//Transforme les elements d'un ArrayCollection resultat de la recherche en ArrayCollection de 'ElementCommade'
		//param in values l'ArrayCollection
		private function formatProduit(values : ArrayCollection):void{
			var ligne : Object;			
			listeProduitFact = null;
			listeProduitFact = new ArrayCollection();
			for (ligne in values){													
				var ec : ElementCommande = new ElementCommande();				
				
				ec.ligneID = values[ligne].IDSOUS_TETE;												
				ec.ressourceID = values[ligne].IDINVENTAIRE_PRODUIT;												
				ec.libelleProduit = values[ligne].LIBELLE_PRODUIT;							
				ec.libelletheme = values[ligne].THEME_LIBELLE;	
				ec.themeID = values[ligne].IDTHEME_PRODUIT;
				ec.libelleLigne = values[ligne].SOUS_TETE;
				
				var index : Number = ConsoviewUtil.getIndexByIdInArray(panier.liste.source,"ressourceID",ec.ressourceID);				 	
				if (index > -1 && panier.liste[index].flagRapproche > 0){
					ec.flagRapproche = 1;
				}else{
					ec.flagRapproche = 0;	
				}
				listeProduitFact.addItem(ec);
			}
			listeProduitFact.refresh();		
		}
		
		//Handler de la méthode rechercherLigneFacturation
		//Affiche le résultat de la recherche dans le DataGrid des ressources
		//Si pas de résultat, affiche un message
		private function rechercherLigneFacturationResultHandler(re : ResultEvent):void{
			if ((re.result as ArrayCollection).length > 0){				
				formatProduit(re.result as ArrayCollection);
				lblRechercheResult.visible = false;
			}else{
				listeProduitFact = (re.result as ArrayCollection);
				lblRechercheResult.visible = true;
			}	
		}
		
	}
}