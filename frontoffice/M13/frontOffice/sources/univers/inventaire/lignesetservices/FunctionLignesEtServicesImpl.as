package univers.inventaire.lignesetservices
{
	import mx.containers.Box;
	import mx.events.FlexEvent;
	import mx.utils.ObjectUtil;
	

	public class FunctionLignesEtServicesImpl extends Box
	{
		public var cpLignesEtServices : MainGestionTechniquesLignesView = new MainGestionTechniquesLignesView();
		
		public function FunctionLignesEtServicesImpl()
		{
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,afterCreationComplete);
		}
		
		protected  function afterCreationComplete(event:FlexEvent):void {
			trace("(FunctionListeAppels) Perform IHM Initialization (" + getUniversKey() + "," + getFunctionKey() + ")");
			addChild(cpLignesEtServices);
			cpLignesEtServices.init();
		}
		
		public  function getUniversKey():String {
			return "GEST";
		}
		
		public  function getFunctionKey():String {
			return "GEST_NDI_SERVICES";
		}
		
		public  function afterPerimetreUpdated():void {
			cpLignesEtServices.onPerimetreChange();
			trace("(FunctionListeAppels) Perform After Perimetre Updated Tasks");
			trace("(FunctionListeAppels) SESSION FLEX :\n" +
						ObjectUtil.toString(CvAccessManager.getSession()));
		}
		
	}
}