package univers.inventaire.equipements.lignes.v2.views
{
	import mx.containers.VBox;
	import flash.events.Event;
	import univers.inventaire.equipements.lignes.v2.applicatif.GestionTechniqueLignesApp;
	import mx.controls.LinkBar;
	import mx.controls.Button;
	import flash.events.MouseEvent;
	import mx.containers.ViewStack;
	import mx.controls.Label;
	import mx.events.ItemClickEvent;
	import mx.events.MenuEvent;
	import mx.containers.Panel;
	
	
	
	
	
	
	
	[Event(name="DeatilRetourClicked")]
	
	
	
	
	
	
	
	
	

	[Bindable]
	public class DetailTechniqueLigneImpl extends BaseView
	{
		
		public var lkOnglets : LinkBar;
		public var vsOnglets : ViewStack;
		public var cmpOngletDetail : OngletDetailView;
		public var cmpOngletEtatView : OngletEtatView;
		public var cmpOngletOperateurView : OngletOperateurView;
		public var cmpOngletAffectationView : OngletAffectationView;
		public var cmpOngletCablageView : OngletCablageView;
		public var cmpOngletFluxApplicatifsView : OngletFluxApplicatifsView;
		public var cmpOngletChampsPersoView : OngletChampsPersoView;
		public var lblLigne : Label;		
		public var btRetour : Button;	
		public var pnlDetail : Panel;	
		
		
		
		public function DetailTechniqueLigneImpl()
		{
			super();			
		}
		
		
		
		
		override protected function commitProperties():void{
			super.commitProperties();
			
			btRetour.addEventListener(MouseEvent.CLICK,btRetourClickHandler);
			
		}
		
		
		
		
		
		/////====================== HANDLERS ========================//
		
		protected function btRetourClickHandler(me : MouseEvent):void{
			dispatchEvent(new Event("DeatilRetourClicked"));
		}
		
		
		protected function lkOngletsItemClickHandler(ice : ItemClickEvent):void{			
			BaseView(vsOnglets.selectedChild).executeBindings(true);
			
		}
		////======================= FIN HANDLERS =====================//
		
	}
}