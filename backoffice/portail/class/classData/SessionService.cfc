<cfcomponent output="false" 
			displayname="SessionService" 
			hint="Classe utilisé pour gérer la SESSION.UUID(sa navigation) d'un utilisateur">
			
	
	<!--- PSEUDO CONSTRUCTEUR --->
	<cffunction name="init" access="private" output="false" returnType="any" 
				displayname="Constructeur de la classe" 
				hint="Constructeur privé qui est appellé uniquement par la méthode getInstance()">
				
		<cfreturn THIS >
	</cffunction>
	
	
	<!--- METHODE CHARGÉ DE RETOURNER LA MEME INSTANCE OU DE LA CREER SI ELLE N'EXISTE PAS --->
	<cffunction name="getInstance" access="public" output="false" returntype="any"
				displayname="Retourne la référence d'un unique objet" 
				hint="Crée un Singleton et l'initialise dans une variable du scope APPLICATION">
		<cfif NOT structKeyExists(APPLICATION,"objS")>
			<!--- ici, initialisé dans le scope application --->
			<cfset APPLICATION.objS = init() >
		</cfif>
		<cfreturn APPLICATION.objS >
	</cffunction>
	
	
	<!--- SUPPRIME STRUCTURE UUID DE LA SESSION --->
	<cffunction name="suppStructUUID" access="public" output="false" returntype="void"
				displayname="" 
				hint="Détecte si la structure SESSION.UUID existe et le cas échéant la supprime de la session">
		<cfargument name="structUUID" required="true" type="any">
		<cfif isDefined("structUUID")>
			<cfset structClear(structUUID)>
		</cfif>
	</cffunction>
	
	
	<!--- SUPPRIME STRUCTURE DE SESSION AU CLIC BTN DECONNEXION --->
	<cffunction name="logOutSession" access="public" output="false" returntype="boolean"
				displayname="" 
				hint="Supprime structure de SESSION si clic sur bouton de deconnexion">
		<cfargument name="URL" required="false" type="struct">
		
		<cfif NOT structIsEmpty(URL)>
			<cfset refParam = structNew()>
			
			<!--- recuperer la key de la structure de l'URL --->
			<cfloop item="x" collection="#URL#" >
				<cfset ref = #x#>
			</cfloop>
			
			<cfif structKeyExists(SESSION.UUID,"#ref#")> <!--- si struct UUID spécifié existe en  SESSION --->
				<cfset refParam = #SESSION.UUID['#ref#']# >
			
				<cfif isDefined("refParam.logOut") && #refParam.logOut# eq "0" >
		
					<!--- Penser à la mettre dans la fonction clearSession() --->
					<cfset objSess = createObject("component","portail.class.classData.SessionService").getInstance()>
					<cfset objSess.suppStructUUID(#SESSION.UUID#) >
					<!--- suppression de variables crées spécifiquement --->
					<cfset StructDelete(session,"idemploye")>
					<cfset StructDelete(session,"idpool")>				
					<cfset StructDelete(session,"bool_mdm_codeparams")>
					<cfset StructDelete(session,"bool_mdm_rac")>
					<cfset StructDelete(session,"bool_mdm_user")>
					<cfset StructDelete(session,"terminauxAvailable")>
					<cfset StructDelete(session,"queryMesInfos")>
					<cfset StructDelete(session,"queryInfoFicheTerminal")>
					<cfset StructDelete(session,"queryInfoSim")>
					<cfset StructDelete(session,"gstdroits")>
					<cfset StructDelete(session,"queryFact")>
					<cfset StructDelete(session,"queryMesTerminaux")>
					<cfset StructDelete(session,"querydeviceinfo")>
					<cfset StructDelete(session,"querymontant")>
					<cfset StructDelete(session,"clone_IdSous_tete")>
					<cfset StructDelete(session,"clone_IdInventaire_periode")>
					<cfset StructDelete(session,"firstco")>
					<cfset StructDelete(session,"varurlstring")>
					<cfset StructDelete(session,"typecnx")>
					<cfset StructDelete(session,"numeroligne")>
					<cfset StructDelete(session,"stremail")>
					
					<cfif isDefined("SESSION.USER.EMAIL")>
						<cfset loggg = createObject("component","fr.consotel.consoview.access.AccessManager").logoff(#SESSION.USER.EMAIL#)>
					</cfif>
					<cflocation url="index.cfm" addtoken="no">
				</cfif>
				
				<cfreturn false>			
			</cfif>
			
			<cfreturn false>
		</cfif>
		
		<cfreturn false>
	</cffunction>


	<!--- REDIRECTION EVENTUELLE SI SESSION A EXPIRÉ --->
	<cffunction access="remote" name="redirectIfSessionInvalid" returntype="void" 
				hint="redirection eventuelle si session a expiré" >
		<cfargument name="SESSION" type="struct" required="false">
		<cfargument name="URL" type="struct" required="false">
		
		<cfif isSessionInvalidPrivate(SESSION,URL) >
			<cflocation url="index.cfm" addtoken="no">
		</cfif>
	</cffunction>
	
	
	<!--- VERIFIE SI LA SESSION A EXPIRÉ / RETOURNE UN BOOLEEN --->
 	<cffunction access="private" name="isSessionInvalidPrivate" returnType="boolean"
				hint="vérifie si la session a expiré">
		<cfargument name="SESSION" type="struct" required="false">
		<cfargument name="URL" type="struct" required="false">
		
		<cfif NOT isDefined("SESSION.AUTH_RESULT") >
			<cfif structKeyExists(URL,"idPage") && (URL.idPage neq "0" && URL.idPage neq "1") >
				<cfreturn true >
			</cfif>
		</cfif>
		<cfreturn false >
	</cffunction>

	
</cfcomponent>