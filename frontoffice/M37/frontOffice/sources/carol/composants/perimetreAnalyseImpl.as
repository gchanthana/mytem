package carol.composants
{
	import carol.event.CarolEvent;
	
	import mx.containers.Box;
	import mx.resources.ResourceManager;
	
	[Bindable]
	public class perimetreAnalyseImpl extends Box
	{
		
		public var jonction		:String = ResourceManager.getInstance().getString('M37', '_au_');
		
		public var perimetre	:String = "";
		public var niveau		:String = "";
		public var beginPeriode	:String = "";
		public var endPeriode	:String = "";
		
		public function perimetreAnalyseImpl()
		{
		}
	}
}
