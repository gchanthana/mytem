package rapport.utils.groupeproduitselector
{
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	
	import rapport.event.RapportEvent;
	import rapport.service.groupeproduitselector.GroupeProduitSelectorService;
	import rapport.utils.SelectorAbstract;
	import rapport.utils.serialization.json.*;
	import rapport.vo.Parametre;
	import rapport.vo.SelectorParametre;
	
	public class GroupeProduitSelectorImpl extends SelectorAbstract
	{
		[Bindable]public var GroupeProduitComboBoxSelectorService:GroupeProduitSelectorService;
		[Bindable]public var comboBoxGroupeProduit:ComboBox;
		public var arrayCleLibelle :Array;
		
		public function GroupeProduitSelectorImpl(selector:Object=null)
		{
			super(selector);
		}
		
		override public function setSelector(selector:SelectorParametre):void
		{
			super.setSelector(selector);
			if(selector!=null)
			{
				this.descriptionComposant=selector.description;
				this.messageAideComposant=selector.messageAide;
				this.is_systemComposant=selector.is_system;
				this.is_hiddenComposant=selector.is_hidden;
				this.typeParametreComposant=selector.typeParametre;
				this.idRapportParamtre=selector.idRapportParam;
				this.CleAndLibelleParametreComposant=selector.cleJson;
				
				arrayCleLibelle= (JSON.decode(CleAndLibelleParametreComposant)); // au format json
			}
		}
		
		protected function creationCompleteGroupeProduitHandler(event:FlexEvent):void
		{
			GroupeProduitComboBoxSelectorService=new GroupeProduitSelectorService();
			GroupeProduitComboBoxSelectorService.model.addEventListener(RapportEvent.USER_GROUPE_PRODUIT_SELECTOR_EVENT,fillData);
			GroupeProduitComboBoxSelectorService.getValuesGroupeProduit();
		}
		
		private function fillData(event:RapportEvent):void
		{
			comboBoxGroupeProduit.dataProvider=GroupeProduitComboBoxSelectorService.model.produits;			
			showHelp();
		}
		
		override public function getSelectedValue():Array
		{
			var tabValeur:Array = [];
			var parametre :Parametre=  new Parametre();
			
			parametre.value=[comboBoxGroupeProduit.selectedItem.id];					
			parametre.cle =arrayCleLibelle[0].NOM_CLE;
			parametre.libelle=arrayCleLibelle[0].LIBELLE_CLE;
			parametre.is_system=arrayCleLibelle[0].IS_SYSTEM;
			parametre.typeValue=this.typeParametreComposant.toUpperCase();
			
			tabValeur[0]=parametre;
			return tabValeur;
		}
	}
}