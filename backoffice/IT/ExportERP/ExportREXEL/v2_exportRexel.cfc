<cfcomponent name="exportRexel" displayname="exportRexel">
	<!--- <cfset biServer="http://xmlpserver-3.consotel.fr/xmlpserver/services/PublicReportService?wsdl"> --->
	<cfset biServer="http://bip-int.consotel.fr/xmlpserver/services/PublicReportService?wsdl">

	<!--- Ram�ne la liste des factures qui sont marqu�es bonnes pour export ERP --->
	<cffunction name="main" access="remote" returntype="void" output="true">
		<cfset thread = CreateObject("java", "java.lang.Thread")>
		<!--- Array pour le traitement des fichiers burst --->
		<cfset stJustif=StructNew()>
		<cfset compteur=1>
		
		<!--- <cffile action = "move" source = "/opt/webroot/backoffice-prod/ExportERP/ExportREXEL/test.txt" destination = "/app/archives/service">
		
		<cfabort showerror="fin"> --->
		
		
		<!--- On récupère la liste des facture --->
		<cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.Liste_facture_a_exporter">
			<cfprocparam value="186521" cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qGetFactures">
		</cfstoredproc>

		<!---  <cfdump var="#qGetFactures#">
		 <cfabort showerror="fin"> --->
		<!--- <cfdump var="#stJustif#" label="Avant la boucle"> --->
		
		<!--- <Si la liste des factures n'est pas vide> --->
		<cfif qGetFactures.recordcount gt 0>
			
			<cfset countFact=0>
			<!--- <Nombre de factures à traiter> --->
			<cfoutput>
			Nombre de factures : #qGetFactures.recordcount#<br />
			</cfoutput>
			
		
			<cfloop query="qGetFactures">
				
				<cfoutput>Boucle numéro : #compteur#</cfoutput>
				<!--- <Affichier la facture à traiter> --->
				<cfoutput> Facture en cours de traitement:  #qGetFactures.numero_facture# <br /></cfoutput> 
				<cfflush interval="20">
		  					
				<!--- <Appel de la fonction qui génère le rapport> --->
				<!--- <cfset w=generateExport(186521,3280501,0,qGetFactures.idinventaire_periode,qGetFactures.numero_facture & ".csv")> ---> <!--- l'orga a chang�--->
				<cfset w=generateExport(186521,5037006,0,qGetFactures.idinventaire_periode,qGetFactures.numero_facture & ".csv")>
				<cfoutput >#qGetFactures.numero_facture#</cfoutput>
				<cfoutput >#qGetFactures.idinventaire_periode#</cfoutput>

				

				<cfset compteur=compteur+1>
				<cfset thread = CreateObject("java", "java.lang.Thread")>
				<cfset thread.sleep(1100)>
				<cfset countFact=countFact+1>
				
			</cfloop>
			
			
				
			<cfset mois=month(now())>
				<cfif mois lt 10>
					<cfset mois="0" & mois>
				</cfif>		
			<cfset archive="rexel_#year(now())##mois##day(now())#.zip">	
			
			<cfloop query="qGetFactures">
				
				<!--- <Fichier � traiter> --->
				<cfset filename="output_" & qGetFactures.numero_facture & ".csv">	
				<cfoutput> Fichier � rajouter au fichier zip est:  #filename# <br /></cfoutput> 
				
					
				<!--- <Ziper tous les rapports> --->					
				<cfzip action="zip" source="/opt/webroot/backoffice-prod/IT/ExportERP/ExportREXEL/csv/" file="/opt/webroot/backoffice-prod/IT/ExportERP/ExportREXEL/csv/#archive#" filter="#filename#"/>	
				<!--- <cfzip action="zip" source="/opt/webroot/backoffice-int/IT/ExportERP/ExportREXEL/csv/" file="/opt/webroot/backoffice-int/IT/ExportERP/ExportREXEL/csv/#archive#" filter="#filename#"/>	 --->
				
				
				<!--- <cfoutput>MAJ Base pour la facture #qGetFactures.numero_facture# <br /></cfoutput>
				
				 <cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.switch_to_exportee">
					<cfprocparam value="#qGetFactures.idinventaire_periode#" type="in" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam type="out" variable="t" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc> --->
				
			</cfloop>
			
		
		<!--- <cffile action = "move" source = "/opt/webroot/backoffice-int/IT/ExportERP/ExportREXEL/csv/#archive#"  --->
		<cffile action = "move" source = "/opt/webroot/backoffice-prod/IT/ExportERP/ExportREXEL/csv/#archive#" 
								destination = "/app/archives/service/Export_ERP/REXEL">

	
		<!--- 	Envoyer les rapports par mail au client --->

			<cfmail from="monitoring@saaswedo.com" to="comptacentrale@rexel.fr,emendoza@rexel.fr" cc="production@consotel.com,monitoring@saaswedo.com,djalel.meftouh@saaswedo.com" subject="[Export ERP] - REXEL"
						server="mail-cv.consotel.fr" port="25" type="html">
				<cfmailparam file="/app/archives/service/Export_ERP/REXEL/#archive#">
				Liste des factures REXEL exportées :<br>
				<cfloop query="qGetFactures">
				#qGetFactures.numero_facture#<br>
				</cfloop>
			</cfmail> 
			
			<cfloop query="qGetFactures">
				
				<!--- <Fichier  trait�> --->
			
				<!--- <cfset filename="output_" & qGetFactures.numero_facture & ".csv">	
				<cfoutput> Fichier � rajouter au fichier zip est:  #filename# <br /></cfoutput> ---> 
				
					
				<cfoutput>MAJ Base pour la facture #qGetFactures.numero_facture# <br /></cfoutput>		
				
				 <cfstoredproc datasource="ROCOFFRE" procedure="PKG_CV_REP_V3.switch_to_exportee">
					<cfprocparam value="#qGetFactures.idinventaire_periode#" type="in" cfsqltype="CF_SQL_INTEGER">
					<cfprocparam type="out" variable="t" cfsqltype="CF_SQL_INTEGER">
				</cfstoredproc>
				
		      </cfloop>
		      

			<cfmail from="monitoring@saaswedo.com" to="djalel.meftouh@saaswedo.com" cc="production@consotel.com,monitoring@saaswedo.com" subject="[Export ERP] - REXEL"
						server="mail-cv.consotel.fr" port="25" type="html">

				Traitemenent OK :<br>
				
			</cfmail> 

		<cfelse> <cfoutput >Pas de facture � exporter</cfoutput>	
			
		</cfif>
		 <cfoutput >Fin OK</cfoutput>	
	</cffunction>
	
	<!--- G�n�re en burst le justificatif par client/magasin --->
	<cffunction name="generateExport" access="remote" returntype="string" output="true">
		<cfargument name="p_idmaster" type="Numeric" default="186521">
		<!--- <cfargument name="p_idorga" type="Numeric" default="3280501"> ---> <!--- l'orga a chang�--->
		<cfargument name="p_idorga" type="Numeric" default="5037006">
		<cfargument name="p_recette" type="Numeric" default="0">
		<cfargument name="p_idinventaire_periode" type="Numeric">
		<cfargument name="p_chaine" type="string">
		<!--- getReportParameters --->
		<!--- Envoi des param�tres --->
		<cfset ArrayOfParamNameValues=ArrayNew(1)>

		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="p_idmaster">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=#p_idmaster#>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[1]=ParamNameValue>

		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="p_idorga">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=#p_idorga#>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[2]=ParamNameValue>

		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="p_recette">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=#p_recette#>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[3]=ParamNameValue>

		<cfset ParamNameValue=structNew()>
		<cfset ParamNameValue.multiValuesAllowed=FALSE>
		<cfset ParamNameValue.name="p_idinventaire_periode">
		<cfset t=ArrayNew(1)>
		<cfset t[1]=#p_idinventaire_periode#>
		<cfset ParamNameValue.values=t>
		<cfset ArrayOfParamNameValues[4]=ParamNameValue>

		<!--- Rapport --->
		<cfset myParamReportRequest=structNew()>
		<cfset myParamReportRequest.reportAbsolutePath="/CONSOTEL/Services/EXPORT_ERP/REXEL/exportERP/exportERP.xdo">
		<cfset myParamReportRequest.attributeTemplate="csv">
		<cfset myParamReportRequest.attributeLocale="fr-FR">
		<cfset myParamReportRequest.attributeFormat="csv">
		<cfset myParamReportRequest.sizeOfDataChunkDownload=-1>
		<cfset myParamReportRequest.parameterNameValues=ArrayOfParamNameValues>
		<!---  --->
		<!--- <cfdump var="#myParamReportRequest#"> --->
		<cfset myParamReportParameters=structNew()>
		<cfset structInsert(myParamReportParameters,"reportRequest",myParamReportRequest)>
		<cfset myParamReportParameters.userID="consoview">
		<cfset myParamReportParameters.password="public">
		<cfoutput>Lancement du rapport<br /></cfoutput>
		<cfinvoke webservice="#biServer#" returnvariable="resultRunReport" method="runReport"
				argumentCollection="#myParamReportParameters#">
		</cfinvoke>
		
		<cfoutput>traitement du résultat du rapport<br /></cfoutput>		
		
		<cfset v=resultRunReport.getReportBytes()>
		
		<cfoutput>Début de la conversion CSV vers Tableau<br /></cfoutput>
		
		<!--- <cfset csvdata = >	 --->
		<cfset csvdata = replace(ToString(v),'"',"",'all')>
		
		<!--- <cfdump var ="#csvdata#"> --->
		
		<cfset arrData = CSVToArray(CSV = csvdata)>
		
		
		<cfoutput>Fin de la conversion CSV vers Tableau<br /></cfoutput>
		
		<cfoutput>Nbre de lignes de la facture : #arrayLen(arrData)#<br /></cfoutput>
		
		<cfdump var="#arrData#" expand="false"><br />
		
		
		<cfset thread = CreateObject("java", "java.lang.Thread")>
		<cfset thread.sleep(3100)>
		<cfif ArrayLen(arrData) gt 1>
			<cfset nomFichier=p_chaine>
		<cfelse>
			<cfset nomFichier="">
		</cfif>
		
		<!--- <cfoutput> #ArrayLen(arrData)# </cfoutput> --->

		<cfloop index="i" from="1" to="#ArrayLen(arrData)#" >

			<cfset arrData[i][6]=replace(arrData[i][6],".",",")>
			<cfset chaine=arrData[i][1]>
			
			<cfloop from="2" to="20" index="j">
				<cfset chaine=chaine & ";" & arrData[i][j]>
			</cfloop>
							
			<cfif i gte 2>
				<!--- Rajouter dans le fichier --->
				<cffile action="append" addnewline="true" file="/opt/webroot/backoffice-prod/IT/ExportERP/ExportREXEL/csv/output_#nomFichier#" output="#chaine#">
				<!--- <cffile action="append" addnewline="true" file="/opt/webroot/backoffice-int/IT/ExportERP/ExportREXEL/csv/output_#nomFichier#" output="#chaine#"> --->
			<cfelse>
				<!--- Créer le fichier et insérer l'en tête' --->
				<cffile action="write" file="/opt/webroot/backoffice-prod/IT/ExportERP/ExportREXEL/csv/output_#nomFichier#" output="#chaine#">
				<!--- <cffile action="write" file="/opt/webroot/backoffice-int/IT/ExportERP/ExportREXEL/csv/output_#nomFichier#" output="#chaine#"> --->
			</cfif>

																	
		</cfloop>

		<cfreturn "output_" & #nomFichier#>
	
	</cffunction>
	
	<!--- Pour convertir un csv en array --->
	<cffunction name="CSVToArray"
		
		access="public"
		returntype="array"
		output="false"
		hint="Takes a CSV file or CSV data value and converts it to an array of arrays based on the given field delimiter. Line delimiter is assumed to be new line / carriage return related.">
		 
		
		<!--- Define arguments. --->
		<cfargument
		name="File"
		type="string"
		required="false"
		default=""
		hint="The optional file containing the CSV data."
		/>
		 
		
		<cfargument
		name="CSV"
		type="string"
		required="false"
		default=""
		hint="The CSV text data (if the file was not used)."
		/>
		 
		
		<cfargument
		name="Delimiter"
		type="string"
		required="false"
		default=";"
		hint="The data field delimiter."
		/>
		 
		
		<cfargument
		name="Trim"
		type="boolean"
		required="false"
		default="true"
		hint="Flags whether or not to trim the END of the file for line breaks and carriage returns."
		/>
		 
		
		 
		
		<!--- Define the local scope. --->
		<cfset var LOCAL = StructNew() />
		 
		
		 
		
		<!---
		Check to see if we are using a CSV File. If so,
		then all we want to do is move the file data into
		the CSV variable. That way, the rest of the algorithm
		can be uniform.
		--->
		<cfif Len( ARGUMENTS.File )>
		 
		
		<!--- Read the file into Data. --->
		<cffile
		action="read"
		file="#ARGUMENTS.File#"
		variable="ARGUMENTS.CSV"
		/>
		 
		
		</cfif>
		 
		
		 
		
		<!---
		ASSERT: At this point, no matter how the data was
		passed in, we now have it in the CSV variable.
		--->
		 
		
		 
		
		<!---
		Check to see if we need to trim the data. Be default,
		we are going to pull off any new line and carraige
		returns that are at the end of the file (we do NOT want
		to strip spaces or tabs).
		--->
		<cfif ARGUMENTS.Trim>
		 
		
		<!--- Remove trailing returns. --->
		<cfset ARGUMENTS.CSV = REReplace(
		ARGUMENTS.CSV,
		"[\r\n]+$",
		"",
		"ALL"
		) />
		 
		
		</cfif>
		 
		
		 
		
		<!--- Make sure the delimiter is just one character. --->
		<cfif (Len( ARGUMENTS.Delimiter ) NEQ 1)>
		 
		
		<!--- Set the default delimiter value. --->
		<cfset ARGUMENTS.Delimiter = "," />
		 
		
		</cfif>
		 
		
		 
		
		<!---
		Create a compiled Java regular expression pattern object
		for the experssion that will be needed to parse the
		CSV tokens including the field values as well as any
		delimiters along the way.
		--->
		<cfset LOCAL.Pattern = CreateObject(
		"java",
		"java.util.regex.Pattern"
		).Compile(
		JavaCast(
		"string",
		 
		
		<!--- Delimiter. --->
		"\G(\#ARGUMENTS.Delimiter#|\r?\n|\r|^)" &
		 
		
		<!--- Quoted field value. --->
		"(?:""([^""]*+(?>""""[^""]*+)*)""|" &
		 
		
		<!--- Standard field value --->
		"([^""\#ARGUMENTS.Delimiter#\r\n]*+))"
		)
		)
		/>
		 
		
		<!---
		Get the pattern matcher for our target text (the
		CSV data). This will allows us to iterate over all the
		tokens in the CSV data for individual evaluation.
		--->
		<cfset LOCAL.Matcher = LOCAL.Pattern.Matcher(
		JavaCast( "string", ARGUMENTS.CSV )
		) />
		 
		
		 
		
		<!---
		Create an array to hold the CSV data. We are going
		to create an array of arrays in which each nested
		array represents a row in the CSV data file.
		--->
		<cfset LOCAL.Data = ArrayNew( 1 ) />
		 
		
		<!--- Start off with a new array for the new data. --->
		<cfset ArrayAppend( LOCAL.Data, ArrayNew( 1 ) ) />
		 
		
		 
		
		<!---
		Here's where the magic is taking place; we are going
		to use the Java pattern matcher to iterate over each
		of the CSV data fields using the regular expression
		we defined above.
		 
		
		Each match will have at least the field value and
		possibly an optional trailing delimiter.
		--->
		<cfloop condition="LOCAL.Matcher.Find()">
		 
		
		<!---
		Get the delimiter. We know that the delimiter will
		always be matched, but in the case that it matched
		the START expression, it will not have a length.
		--->
		<cfset LOCAL.Delimiter = LOCAL.Matcher.Group(
		JavaCast( "int", 1 )
		) />
		 
		
		 
		
		<!---
		Check for delimiter length and is not the field
		delimiter. This is the only time we ever need to
		perform an action (adding a new line array). We
		need to check the length because it might be the
		START STRING match which is empty.
		--->
		<cfif (
		Len( LOCAL.Delimiter ) AND
		(LOCAL.Delimiter NEQ ARGUMENTS.Delimiter)
		)>
		 
		
		<!--- Start new row data array. --->
		<cfset ArrayAppend(
		LOCAL.Data,
		ArrayNew( 1 )
		) />
		 
		
		</cfif>
		 
		
		 
		
		<!---
		Get the field token value in group 2 (which may
		not exist if the field value was not qualified.
		--->
		<cfset LOCAL.Value = LOCAL.Matcher.Group(
		JavaCast( "int", 2 )
		) />
		 
		
		<!---
		Check to see if the value exists. If it doesn't
		exist, then we want the non-qualified field. If
		it does exist, then we want to replace any escaped
		embedded quotes.
		--->
		<cfif StructKeyExists( LOCAL, "Value" )>
		 
		
		<!---
		Replace escpaed quotes with an unescaped double
		quote. No need to perform regex for this.
		--->
		<cfset LOCAL.Value = Replace(
		LOCAL.Value,
		"""""",
		"""",
		"all"
		) />
		 
		
		<cfelse>
		 
		
		<!---
		No qualified field value was found, so use group
		3 - the non-qualified alternative.
		--->
		<cfset LOCAL.Value = LOCAL.Matcher.Group(
		JavaCast( "int", 3 )
		) />
		 
		
		</cfif>
		 
		
		 
		
		<!--- Add the field value to the row array. --->
		<cfset ArrayAppend(
		LOCAL.Data[ ArrayLen( LOCAL.Data ) ],
		LOCAL.Value
		) />
		 
		
		</cfloop>
		 
		
		 
		
		<!---
		At this point, our array should contain the parsed
		contents of the CSV value. Return the array.
		--->
		<cfreturn LOCAL.Data />
	</cffunction>
</cfcomponent>