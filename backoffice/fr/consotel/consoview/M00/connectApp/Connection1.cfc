﻿<cfcomponent extends="AbstractConnectToApp">
<!--- CONNECTION A CV PAR LOGIN / MDP --->	
	
<!---- CHAQUE FONCTION GERE CES ERREURS ET RETOURNE UN OBJECT ERROR AVEC UN CODEERREUR
	   IL EST POSSIBLE DE PRECISER LE TYPE, LE MESSAGE ET LE DETAIL
	   <cfreturn ThrowCfCatchError(CODEERREUR,TYPE,MESSAGE,DETAIL)>
	   codeerreur : int, type : string, message : string, detail  : string --->
	<cffunction name="connectTo" 
				displayname="ConnectToApp" 
				description="Permet de se logger à une application" 
				access="public" 
				output="false" 
				returntype="struct">
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		<cflog text="connection1.connectToApp(login = #objConnection.login#, pwd = #objConnection.pwd#)">
		
		 
		<cfset res = createObject("component","fr.consotel.consoview.M00.connectApp.Connectioncc1ca#SESSION.CODEAPPLICATION#").connectTo(#objConnection#)>
		
		<cfreturn res>
	</cffunction>	   
	<cffunction name="ChangeGroupe" 
				displayname="ChangeGroupe" 
				description="Gère le changement de Groupe de la personne connectée" 
				access="public" 
				output="false" 
				returntype="struct">
					
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		
		<cflog text="Connection1.ChangeGroupe(objConnection.idperimetre = #objConnection.idperimetre#)">
		
		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfset result = connectOnGroup(objConnection.idperimetre)>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETPERIMETRE : définit le périmètre --->
				<cfset result = setPerimetre(SESSION.PERIMETRE.ID_GROUPE)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur --->
					<cfset res = createObject("component","fr.consotel.consoview.M00.connectApp.Connectioncc1ca#SESSION.CODEAPPLICATION#")>
					<cfset result = res.setXMLAccess()>
					<cfif isDefined("result") AND isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					 </cfif>
				</cfif>
			</cfif>
			<cfset str= structNew()>
			<cfset str.xml_perimetre = SESSION.XML_PERIMETRE>
			<cfset str.perimetre = SESSION.PERIMETRE>
			 <cfset str.xml_access = SESSION.XML_ACCESS>
			<cfreturn str>
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-16,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
	</cffunction>		
	<cffunction name="ChangePerimetre" 
				displayname="ChangePerimetre" 
				description="Gère le changement de périmètre de la personne connectée" 
				access="public" 
				output="false" 
				returntype="struct">
					
		<cfargument name="objConnection" displayName="objConnection" type="struct" required="true" />
		
		
		
		<cftry>
			<!--- CONNECTONGROUP : se connecte au group correspondant à la racine rattaché à l'utilisateur à la création --->
			<cfset result = 0>
			<cfif objConnection.idgroupe neq SESSION.PERIMETRE.ID_GROUPE>
				<cfset result = connectOnGroup(objConnection.idgroupe)>
			</cfif>
			 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
				<cfreturn result>
			<cfelse>
				<!--- SETPERIMETRE : définit le périmètre --->
				<cfset result = setChangePerimetre(objConnection.idperimetre)>
				 <cfif isStruct(result) AND structKeyExists(result,"CODEERREUR")>
					<cfreturn result>
				<cfelse>
					<!--- SETXMLACCESS : créé la liste des modules accessibles selon le périmètre et l'utilisateur 
					<cfset res = createObject("component","fr.consotel.consoview.M00.connectApp.Connectioncc1ca#SESSION.CODEAPPLICATION#")>
					<cfset result = res.setXMLAccess()>			--->		
					<cfif isDefined("result") AND isStruct(result) AND structKeyExists(result,"CODEERREUR")>
						<cfreturn result>
					 </cfif>
				</cfif>
			</cfif>
			<cfset str= structNew()>
			<cfset str.perimetre = SESSION.PERIMETRE>
			 <cfset str.xml_access = SESSION.XML_ACCESS>
			<cfreturn str>
			<cfcatch type="any" >
				<cfreturn ThrowCfCatchError(-15,CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE)>
			</cfcatch>
		</cftry>
	</cffunction>	
</cfcomponent>