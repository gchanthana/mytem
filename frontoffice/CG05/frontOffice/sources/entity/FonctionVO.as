package entity
{
	[Bindable]
	public class FonctionVO
	{
		private var _nomFonction		:String;
		private var _idFonction			:Number;
		private var _accesSWD			:Boolean;
		private var _accesAppMaster		:Boolean;
		private var _accesDistributeur	:Boolean;
		private var _accesClient		:Boolean;
		// _access : définit si la fonction a un accès ou aucun. Lié au profil
		private var _access				:Boolean; 
		private var _isSelected			:Boolean;
		private var _nbreUser			:Number;
		private var _nbreProfil			:Number;
		private var _commentFct			:String;
		private var _idCategorie		:Number;
		private var _categorie			:String;
		private var _codeFonction		:String;
		private var _keyFonction 		:String;

		// _accessChanged : montre si les niveaux d'acces ont changé, utile pour maj de la fonction actuelle ou pas.
		private var _accessChanged		:Boolean = false; 
		private var _listeParams		:Array;

		public function FonctionVO()
		{
		}

		public function fill(obj:Object):void
		{
			try
			{
				if(obj.hasOwnProperty("NOM_FONCTION")){
					this.nomFonction = obj.NOM_FONCTION;
				}
				
				if(obj.hasOwnProperty("IDFONCTION")){
					this.idFonction = obj.IDFONCTION;
				}
				
				if(obj.hasOwnProperty("NIV1")){
					this.accesSWD = (obj.NIV1 == "1");
				}
				
				if(obj.hasOwnProperty("NIV2")){
					this.accesAppMaster = (obj.NIV2 == "1");
				}
				
				if(obj.hasOwnProperty("NIV3")){
					this.accesDistributeur = (obj.NIV3 == "1");
				}
				
				if(obj.hasOwnProperty("NIV4")){
					this.accesClient = (obj.NIV4 == "1");
				}
				
				if(obj.hasOwnProperty("COMMENTAIRES")){
					this.commentFct = obj.COMMENTAIRES;
				}
				
				if(obj.hasOwnProperty("FUNCTION_KEY")){
					this.keyFonction = obj.FUNCTION_KEY;
				}
				
				if(obj.hasOwnProperty("IDUNIVERS")){
					this.idCategorie = obj.IDUNIVERS;
				}
				
				if(obj.hasOwnProperty("UNIVERS")){
					this.categorie = obj.UNIVERS;
				}
				
			}catch(err:Error)
			{
				trace("##Initialisation d'un objet Fonction erronée ");
			}
		}
		
		public function get SELECTED():Boolean
		{
			return _isSelected;
		}
		
		public function set SELECTED(value:Boolean):void
		{
			_isSelected = value;
		}
		
		public function get access():Boolean
		{
			return _access;
		}
		
		public function set access(value:Boolean):void
		{
			_access = value;
		}
		
		public function get accesClient():Boolean
		{
			return _accesClient;
		}
		
		public function set accesClient(value:Boolean):void
		{
			_accesClient = value;
		}
		
		public function get accesDistributeur():Boolean
		{
			return _accesDistributeur;
		}
		
		public function set accesDistributeur(value:Boolean):void
		{
			_accesDistributeur = value;
		}
		
		public function get accesAppMaster():Boolean
		{
			return _accesAppMaster;
		}
		
		public function set accesAppMaster(value:Boolean):void
		{
			_accesAppMaster = value;
		}
		
		public function get accesSWD():Boolean
		{
			return _accesSWD;
		}
		
		public function set accesSWD(value:Boolean):void
		{
			_accesSWD = value;
		}
		
		public function get idFonction():Number
		{
			return _idFonction;
		}
		
		public function set idFonction(value:Number):void
		{
			_idFonction = value;
		}
		
		public function get nomFonction():String
		{
			return _nomFonction;
		}
		
		public function set nomFonction(value:String):void
		{
			_nomFonction = value;
		}
		
		public function get nbreUser():Number { return _nbreUser; }		
		public function set nbreUser(value:Number):void
		{
			if (_nbreUser == value)
				return;
			_nbreUser = value;
		}
		public function get nbreProfil():Number { return _nbreProfil; }
		
		public function set nbreProfil(value:Number):void
		{
			if (_nbreProfil == value)
				return;
			_nbreProfil = value;
		}
		
		public function get commentFct():String { return _commentFct; }
		
		public function set commentFct(value:String):void
		{
			if (_commentFct == value)
				return;
			_commentFct = value;
		}
		
		public function get categorie():String { return _categorie; }
		
		public function set categorie(value:String):void
		{
			if (_categorie == value)
				return;
			_categorie = value;
		}
		
		public function get codeFonction():String { return _codeFonction; }
		
		public function set codeFonction(value:String):void
		{
			if (_codeFonction == value)
				return;
			_codeFonction = value;
		}
		
		public function get listeParams():Array { return _listeParams; }
		
		public function set listeParams(value:Array):void
		{
			if (_listeParams == value)
				return;
			_listeParams = value;
		}
		public function get accessChanged():Boolean { return _accessChanged; }
		
		public function set accessChanged(value:Boolean):void
		{
			if (_accessChanged == value)
				return;
			_accessChanged = value;
		}
		
		public function get keyFonction():String { return _keyFonction; }
		
		public function set keyFonction(value:String):void
		{
			if (_keyFonction == value)
				return;
			_keyFonction = value;
		}
		
		public function get idCategorie():Number { return _idCategorie; }
		
		public function set idCategorie(value:Number):void
		{
			if (_idCategorie == value)
				return;
			_idCategorie = value;
		}
		
		
		/**
		 * Une chaine de caractère contenant les idNiveau des niveaux qui ont accès à cette fonctionnalité (this).
		 * */
		public function getNiveauxHasAcces():String
		{
			var niveauxHasAcces:String = "";
			
			if(this.accesSWD == true)
			{
				if(niveauxHasAcces != "")
					niveauxHasAcces = niveauxHasAcces +",1";
				else
					niveauxHasAcces = "1";
			}
			if(this.accesAppMaster == true)
			{
				if(niveauxHasAcces != "")
					niveauxHasAcces = niveauxHasAcces +",2";
				else
					niveauxHasAcces = "2";
			}
			if(this.accesDistributeur == true)
			{
				if(niveauxHasAcces != "")
					niveauxHasAcces = niveauxHasAcces +",3";
				else
					niveauxHasAcces = "3";
			}
			if(this.accesClient == true)
			{
				if(niveauxHasAcces != "")
					niveauxHasAcces = niveauxHasAcces +",4";
				else
					niveauxHasAcces = "4";
			}
			
			return niveauxHasAcces;
		}

	}
}