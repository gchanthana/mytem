package univers.inventaire.inventaire.creation.nouvelleResources.commande.rapprochements
{
	import flash.events.Event;
	
	
	/**
	 * Classe évenement permettant de passer les parametres de choix 
	 * pour la technique de rapprochement
	 * */
	public class AlgoSelectedEvent extends Event
	{
		/**
		 * Indentifiant de la methode de rapprochement choisie
		 * */
		public var selectedAlgo : int;
		
		/**
		 * Constante definissant le type d'evenement dispatché lors de la sélection d'une méthode 
		 * */
		public static const ALGO_SELECTED : String = "algoSelected";
		
		/**
		 * Constructeur 
		 * Voir la classe flash.events.Event pour les parametres
		 * */
		public function AlgoSelectedEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
	}
}