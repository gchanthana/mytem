package composants.util
{
	import mx.events.FlexEvent;
	import flash.events.Event;
	import mx.controls.Tree;
	import mx.collections.ArrayCollection;
	import mx.managers.CursorManager;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;	
	import mx.core.ClassFactory;
	import univers.UniversManager;
	import flash.xml.XMLNode;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import composants.access.ListePerimetresTree;
	
	public class GenericSearchTree extends GenericSearchTreeIHM
	{
		public var _myDp:XMLNode;
		public var _myDpRef:XMLNode;
		public var lbl:String;
		
		private var currentSelectedIndex:int = 0; // Index du groupe
		private var currentXmlResultIndex:int = 0; // Index de la recherche
		private var searchResult:int = 0; // Nombre de résultat de la recherche de noeuds
		
		[Bindable]
		private var datagridData : ArrayCollection = new ArrayCollection();// les donnees pour le DataGrid
		
		[Bindable]
		public var treeData : XMLList = new XMLList();//les donnees pour le Tree
		
		protected var xmlResult:XMLList = null;
		
		public function GenericSearchTree(dp:XMLNode,labl:String){
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, initIHM);
			lbl=labl;
			_myDp=dp;
			_myDpRef=dp;
		}
				
		public function initIHM(e:FlexEvent):void {
			myTree.labelField="@"+lbl;
			//myTree.itemRenderer=new ClassFactory(composants.tb.recherche.renderer.TIRFolderLines);
			initDp();
			//myMainBox.addChild(myTree);
			nodeSearchItem.addEventListener(MouseEvent.CLICK,searchNode);
			previousItem.toolTip = "Précédent\n(Flèche du Haut)";
			previousItem.addEventListener(MouseEvent.CLICK,previousSearchNode);
			nextItem.toolTip = "Suivant\n(Flèche du Bas)";
			nextItem.addEventListener(MouseEvent.CLICK,nextSearchNode);
			this.addEventListener(KeyboardEvent.KEY_DOWN,processKey);
			myTree.percentHeight=100;
			myTree.percentWidth=100;
			imgCancel.addEventListener(MouseEvent.CLICK,clearTree);
		}
		
		public function clearTree(ev:MouseEvent):void {
			clearSearch(null);
			imgCancel.visible = false;
			if (myTree.dataProvider != null){
				myTree.selectedIndex = 0;
				myTree.expandItem(myTree.selectedItem,false);
			} else{
				
			}
		}
		
		public function initDp():void {
			clearSearch(null);
			treeData = _myDp as XMLList;
			setDp(_myDp);
		}
		
		public function setDp(xml:Object):void {
			//myTree.dataProvider = null;
			myTree.dataProvider = xml;
		}
		
		private function processKey(event:KeyboardEvent):void {
	      	if((event.keyCode == 13)) { // 38 et 40
		      	searchNode(null);
	      	} else if((nextItem.enabled) && (event.keyCode == 40)) {
					nextSearchNode(null);
	      	} else if((previousItem.enabled) && (event.keyCode == 38)) {
					previousSearchNode(null);
	      	}
		}
		
		/**
		 * Filtre l'objet nodeItem (XML ou XMLList) en renvoyant tous les noeuds dont
		 * l'attribut attributeName contient le mot clé keyword dans sa valeur.
		 * */
		public function xmlSearch(nodeItem:Object,attributeName:String,keyword:String):XMLList {
			return nodeItem.(@[attributeName].toString().toLowerCase().search(keyword.toLowerCase()) > -1);
		}
		
		/**
		 *  Recherche le ou les noeuds contenant le mot clé dans la zone de texte searchInput.
		 **/
		private function searchNode(event:Event):void {
			if (myTree.selectedIndex >= 0) {
				closeTreeNodes(myTree.selectedItem);	
			}
			currentXmlResultIndex = 0;
			CursorManager.setBusyCursor();
			imgCancel.visible=true;
			var tmpIndex:int=0;
			var keyword:String=searchInput.text;
			var selectedObj:XML = null;
			var nbChild:int = 0;
			var childNodes:XMLList = null;
			var tmpParent:XML = null;
			myTree.selectedIndex = 0;
			selectedObj = myTree.selectedItem as XML;
			/*if(myTree.selectedItem == null) { // Aucun noeud séléctionné
				myTree.selectedIndex = 0;
				selectedObj = myTree.selectedItem as XML;
			}
			*/
			
			childNodes = myTree.selectedItem.descendants();
			xmlResult = xmlSearch(childNodes,lbl,keyword);
			if(xmlResult.length() > 0) {
				searchResult = xmlResult.length();
			} else {
				xmlResult = null;
				searchResult = 0;
			}
			
			if(searchResult == 1) {
				currentXmlResultIndex =currentXmlResultIndex +1
				tmpIndex = currentXmlResultIndex;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			} else if(searchResult > 1) {
				bxArrow.visible=true;
				currentXmlResultIndex =currentXmlResultIndex+1;
				tmpIndex = currentXmlResultIndex;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			} else { // Aucun résultat
				bxArrow.visible=false;
				tmpIndex = 0;
			}
			searchIndex.text = tmpIndex + "/" + searchResult;
			CursorManager.removeBusyCursor();
		}
		
		/**
		 * Séléctionne le résultat précédent dans la recherche.
		 * */
		private function previousSearchNode(event:Event):void {
			if(currentXmlResultIndex > 1 && searchResult!=0) {
				currentXmlResultIndex = currentXmlResultIndex - 1;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
				
			}else if(currentXmlResultIndex == 1 && searchResult!=0) {
				currentXmlResultIndex = searchResult;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			}
		}

		/**
		 * Séléctionne le résultat suivant dans la recherche.
		 * */
		private function nextSearchNode(event:Event):void {
			//Alert.show(currentXmlResultIndex+":"+searchResult);
			if(currentXmlResultIndex < searchResult && searchResult!=0) {
				currentXmlResultIndex = currentXmlResultIndex + 1;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			} else if(currentXmlResultIndex == searchResult && searchResult!=0) {
				currentXmlResultIndex = 1;
				searchIndex.text = (currentXmlResultIndex) + "/" + searchResult;
				expandToNode(xmlResult[currentXmlResultIndex-1],true);
				selectGivenNode(xmlResult[currentXmlResultIndex-1],true);
			}
		}
		
		/**
		 * Déroule ou ferme suivant la valeur du paramètre open, tous les noeuds
		 * parents du noeud nodeItem (XML) passé en paramètre jusqu'à atteindre la racine.
		 * Puis renvoie le noeud passé en paramètre (nodeItem).
		 * */
		public function expandToNode(nodeItem:Object,open:Boolean):Object {
			var tmpParent:XML = nodeItem.parent() as XML;
			while(tmpParent != null) {
				myTree.expandItem(tmpParent,open);
				tmpParent = tmpParent.parent() as XML;
			}
			return nodeItem;
		}
		
		public function closeTreeNodes(nodeItem:Object):Object {
			var tmpParent:XML = nodeItem.parent() as XML;
			while(tmpParent != null) {
				myTree.expandItem(tmpParent,false);
				tmpParent = tmpParent.parent() as XML;
			}
			return nodeItem;
		}
		
		/**
		 * Séléctionne le noeud (XML) passé en paramètre, puis positionne la
		 * scroll bar à l'index correspondant si le paramètre scrollTo vaut true.
		 * */
		public function selectGivenNode(nodeItem:Object,scrollTo:Boolean):void {
			myTree.selectedItem = nodeItem;
			myTree.dispatchEvent(new Event(Event.CHANGE));
			if(scrollTo) {
				myTree.scrollToIndex(myTree.selectedIndex);
			}
		}
		
		/**
		 * Efface la dernière recherche effectuée.
		 * */
		private function clearSearch(event:Event):void {
			bxArrow.visible=false;
			searchInput.text="";
			nextItem.enabled = previousItem.enabled = false;
			currentXmlResultIndex = searchResult = 0;
			searchIndex.text = currentXmlResultIndex + "/" + searchResult;
			xmlResult = null;
		}

		// getter
		public function getSelectedItem():Object {
			return myTree.selectedItem
		}
	}
}