package gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.generic
{
	import gestionparc.entity.SpecificationVO;
	import gestionparc.entity.vue.AbstractMatriceParcVO;
	import gestionparc.ihm.main.menu.itemmenurenderer.ruledispdalyitem.IRuleDisplayItem;

	public class IsLigneTerminated implements IRuleDisplayItem
	{
		public function IsLigneTerminated() 
		{
		}
		
		public function isDisplay():Boolean
		{
			var ligne:AbstractMatriceParcVO=SpecificationVO.getInstance().elementDataGridSelected;
			return (ligne.ST_IDETAT == 3)?true:false;				 		
		}
		 
		
	}
}