package gestionparc.ihm.action.associer.actionAssocierLigneExtToEqp
{
	import flash.display.DisplayObject;
	
	import gestionparc.ihm.fiches.equipement.FicheAssocierLigneExtToEqp;
	
	import mx.core.Application;
	import mx.managers.PopUpManager;

	public class ActionAssocierLigneExtToEqpReceiver
	{
		private var fiche:FicheAssocierLigneExtToEqp;
		
		public function ActionAssocierLigneExtToEqpReceiver()
		{
		}
		public function action():void
		{
			displayIHM();
		}
		private function displayIHM():void
		{
			fiche = new FicheAssocierLigneExtToEqp();
			PopUpManager.addPopUp(fiche, Application.application as DisplayObject, true);
			PopUpManager.centerPopUp(fiche);
		}
	}
}