package vo.destinataire
{

	// pour faire le mapping entre cette class et le composant coldfuison (entre deux objets)
	[RemoteClass(alias="fr.consotel.consoview.M331.service.destinataires.IUDDestinataireService")]

	[Bindable]
	public class Destinataire
	{
		private var _id:int=0;
		private var _nom:String;
		private var _prenom:String;
		private var _mail:String;
		private var _patronyme:String="";
		private var _profil:String;
		private var _noeud:Number;// nb noeud associé au destinataire
		private var _selected:Boolean=false; //pour compter les case à cocher  

		public function Destinataire()
		{
		}

		/**
		 * permert de faire le mapping entre le front et le back
		 */
		public function fill(unDestinataire:Object):void
		{
			this._id=unDestinataire.IDDESTINATAIRE;
			this._mail=unDestinataire.EMAIL;
			this._nom=unDestinataire.NOM;
			this._prenom=unDestinataire.PRENOM;
			this._profil=unDestinataire.PROFIL;
			this._noeud=unDestinataire.NOEUDS;
		}

		public function get id():int
		{
			return _id;
		}

		public function set id(value:int):void
		{
			_id=value;
		}


		public function get nom():String
		{
			return _nom;
		}

		public function set nom(value:String):void
		{
			_nom=value;
		}

		public function get prenom():String
		{
			return _prenom;
		}

		public function set prenom(value:String):void
		{
			_prenom=value;
		}

		public function get mail():String
		{
			return _mail;
		}

		public function set mail(value:String):void
		{
			_mail=value;
		}

		public function get patronyme():String
		{
			return _patronyme;
		}

		public function set patronyme(value:String):void
		{
			_patronyme=value;
		}

		public function get profil():String
		{
			return _profil;
		}

		public function set profil(value:String):void
		{
			_profil=value;
		}

		public function get noeud():Number
		{
			return _noeud;
		}

		public function set noeud(value:Number):void
		{
			_noeud=value;
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		public function set selected(value:Boolean):void
		{
			_selected=value;
		}

	}
}
