<cfcomponent name="FicheTicket">	
	<cffunction name="getTicket" access="remote" returntype="query">
		
		<cfargument name="idsous_tete" type="numeric" required="true">
		<cfargument name="idinventaire_periode" type="numeric" required="true">
		
		<cfargument name="ORDER_BY_TEXTE_COLONNE" type="string" required="true">
		<cfargument name="ORDER_BY_TEXTE" type="string" required="true">
		
		<cfargument name="SEARCH_TEXT_COLONNE" type="string" required="true">
	    <cfargument name="SEARCH_TEXT" type="string" required="true">
		
	    <cfargument name="OFFSET" type="numeric" required="true">
	    <cfargument name="LIMIT" type="numeric" required="true">
		
		<cfset idracine_master = session.perimetre.idracine_master>
		<cfset _lang = session.user.globalization >		
	
		<cfstoredproc datasource="#SESSION.OffreDSN#" procedure="CDR.PKG_M311.GET_ALL_TICKET_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine_master" value="#idracine_master#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idinventaire_periode" value="#idinventaire_periode#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_idsous_tete" value="#idsous_tete#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE_COLONNE" value="#ORDER_BY_TEXTE_COLONNE#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_ORDER_BY_TEXTE" value="#ORDER_BY_TEXTE#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT_COLONNE" value="#SEARCH_TEXT_COLONNE#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_SEARCH_TEXT" value="#SEARCH_TEXT#" null="false">        
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_OFFSET" value="#OFFSET#" null="false">
		    <cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="P_LIMIT" value="#LIMIT#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="P_LANGUE" value="#_lang#" null="false">			
		    <cfprocresult name="p_result">
	    </cfstoredproc>
		
		<cfreturn p_result>
		
	</cffunction>
</cfcomponent>