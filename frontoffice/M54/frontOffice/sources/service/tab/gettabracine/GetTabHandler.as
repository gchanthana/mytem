package service.tab.gettabracine
{
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;

	internal class GetTabHandler
	{
		private var _model:GetTabModel;
		
		public function GetTabHandler(_model:  GetTabModel):void
		{
			this._model = _model;
		}	
		internal function getListKpiResultHandler(evt:ResultEvent):void
		{
			if(evt.result)
			{
				_model.updateListTab(evt.result as ArrayCollection);
			}
		}
	}
}