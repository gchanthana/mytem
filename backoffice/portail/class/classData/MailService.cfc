<cfcomponent output="false" 
			 displayname="MailService" 
			 hint="Classe regroupant methodes d'envoi d'email ">
	

	<!---  --->
	<cffunction name="sendEmailGest" access="public" output="false" returnType="string"
				displayname="" 
				hint="Envoi d'email / template D1" >
		<cfargument name="from" required="true" type="string">
		<cfargument name="obj" required="true" type="string">
		<cfargument name="dest" required="true" type="string">
		<cfargument name="exped" required="true" type="string" default="">
		<cfargument name="email" required="true" type="string" default="">
		
		<cfif obj eq "">
			<cfset obj = "[Portail Utilisateur - Message envoyé à mon gestionnaire]">
		</cfif>
		
		<cfmail from="#from#" to="#dest#" cc="#exped#" bcc="#from#" priority="urgent" subject="#obj#" failto="joan.cirencien@saaswedo.com" type="text/html" 
				server="mail.consotel.fr" port="26" charset="utf-8" >
							
			<pre>#email#</pre>
			
		</cfmail>
				
	</cffunction>
	

	<!---  --->
	<cffunction name="emailFirstCnxValidation" access="public" output="false" returnType="string"
				displayname="" 
				hint="Envoi d'email de confirmation envoyé à l’utilisateur lors de sa premiere connexion  / template D1" >
					
		<cfargument name="idCivilite" required="true" type="string" default="">
		<cfargument name="nom" required="true" type="string" default=" - ">
		<cfargument name="prenom" required="true" type="string" default=" - ">
		<cfargument name="numMobile" required="true" type="string" default=" - ">
		<cfargument name="dest" required="true" type="string">
		<cfargument name="btnCliked" required="true" type="numeric">
		
		<cfset indRetour = "">
		<cfset from = "joan.cirencien@saaswedo.com">
		<cfset obj = "[Portail Utilisateur - Email de confirmation]">
		<cfset civilite = "">
		
		
		<cfif #idCivilite# eq "1">
			<cfset civilite = "Monsieur">
		<cfelseif #idCivilite# eq "2">
			<cfset civilite = "Madame">
		<cfelseif #idCivilite# eq "3">
			<cfset civilite = "Mademoiselle">
		<cfelse>
			<cfset civilite = " - ">
		</cfif>
		
		<cftry>
		
			<cfif #btnCliked# eq 1>
				<!--- CREATE OBJECT --->
				<cfset objURL = createObject("component","portail.class.specialClass.AbstractComposant") >
				
				<!--- CREATION URL POUR REDIRECTION SUR PAGE DINDEX AVEC NUMERO DE TELEPHONE DEJA RENSEIGNÉ	--->
				<cfset struct = {	idPage = "0",
									sendSMSAfterEmail = true,
									sous_tete = "#numMobile#"
								} >
				<cfset urlString = "" >
				<cfset urlString = objURL.createRefURL(#struct#) >
				
				<cfset varUUID = RemoveChars(#urlString#, 1, 10) > 
				<cfset SESSION.varURLString = #urlString# > <!--- initialisation SESSION.UUID pour utiliser cette variable de session lors du clic sur le bouton de renvoi de mail("réenvoyer le mail") --->
				<cfset strURL = #urlString#> <!--- variable contenant le lien URL dans le mail --->
				
			<cfelseif #btnCliked# eq 4>
				<cfset strURL = #SESSION.varURLString#> <!--- variable contenant le lien URL dans le mail --->
				<cfset indRetour = "true" >
			</cfif>
			
			
			<cfif #btnCliked# eq 1> <!--- lors du clic sur le btn enregistrer de la popup "1ere Connexion", on sauvegarde la fiche dans la BDD(annuaire) --->
				<!--- methode associant une ligne à un user --->
				<cfset indRetour = createObject("component","portail.class.classData.AccessManagerService").createUserAccesByLigne(#telFirstCnx#,#nom#,#prenom#,#email#,#varUUID#) >
			</cfif>
			
		<cfcatch>
			<!--- ENVOI DE MAIL D'ERREUR --->
			<cfdump var="#cfcatch#">
			<cfmail from="ReportError@portail.fr" to="joan.cirencien@saaswedo.com" subject="ERREUR first cnx"  type="text/html" server="mail.consotel.fr" port="26" charset="utf-8" > 
				<cfdump var="#SESSION#" label="SESSION ERROR (CFCATCH)">
				<cfdump var="#cfcatch#">
				<cfdump var="#indRetour#">
			</cfmail>
		</cfcatch>
		</cftry>
		
		
		<cfif indRetour eq "1" || indRetour eq "true"> <!--- SI LA FICHE A BIEN ÉTÉ CRÉEE ET/OU SI ON REENVOIE LE MAIL, ON PERMET L'ENVOI DU MAIL --->
			
		<!--- generation du mail à envoyer --->
		<cfmail from="#from#" to="#dest#" subject="#obj#" priority="urgent" type="text/html" 
				server="mail.consotel.fr" cc="joan.cirencien@saaswedo.com" failto="joan.cirencien@saaswedo.com" port="26" charset="utf-8" > <!--- failto="#from#" --->
			
<pre style="font-size:14px;font-family:Arial, Helvetica, sans-serif">
Bonjour, 

Vous venez de renseigner les informations suivantes pour vous connecter au <span style="font-style:italic">Portail Télécom Utilisateur</span> de votre entreprise

Civilité : <span style="font-weight:bold">#civilite#</span>
Nom : <span style="font-weight:bold">#nom#</span>
Prénom : <span style="font-weight:bold">#prenom#</span>
Numéro de mobile : <span style="font-weight:bold">#numMobile#</span>

Confirmez vous la justesse de ces informations ? Si oui, cliquez sur le lien suivant <a style="color:blue" href="http://sun-dev-reda/portail/#strURL#">portail télécom utilisateur</a>.
Après confirmation, vous allez recevoir par SMS un code de connexion qu’il faudra garder pour vous connecter tous les mois.

Si les informations ne sont pas justes, retournez saisir les bonnes informations sur le <a style="color:blue" href="http://sun-dev-reda/portail/index.cfm">portail télécom utilisateur</a>.
</pre>
			
		</cfmail>
		
		</cfif>
		
		<cfreturn #indRetour#> <!--- valeur de retour / spécifie erreur ou aucune erreur --->
	</cffunction>
	
</cfcomponent>