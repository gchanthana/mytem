﻿<cfcomponent displayName="fr.consotel.api.ibis.publisher.handler.process.ProcessM61" 
			 implements="fr.consotel.api.ibis.publisher.handler.IServiceHandler"
			 extends="fr.consotel.api.ibis.publisher.handler.process.PostProcess" 
			 hint="handler du service Container">
	<cffunction access="public" name="run" returntype="void" hint="Called with the event that triggered this handler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered this handler">
			<cflog text="PROCESSM61.run()">
			<cfset VARIABLES.MY_CODE.run(ARGUMENTS.eventObject)><!--- Spécifique Container --->
			<cfset SUPER.run(ARGUMENTS.eventObject)><!--- Inclus MAJ ID --->
	</cffunction>
	
	<cffunction access="public" name="onError" returntype="void" hint="Called when an error has occurred either during creation or execution of IServiceHandler">
		<cfargument name="eventObject" type="fr.consotel.api.ibis.publisher.handler.event.IServiceEvent" required="true" hint="Event object that triggered run() handler">
		<cfargument name="eventHandler" type="string" required="true" hint="The corresponding EVENT_HANDLER supposed to handle eventObject">
		<cfargument name="exceptionObject" type="any" required="false" hint="Optionally the corresponding catched exception (Same structure as CFCATCH scope)">
		
			<cfset VARIABLES.MY_CODE.onError(ARGUMENTS.eventObject,ARGUMENTS.eventHandler,ARGUMENTS.exceptionObject)>
	
	</cffunction>
	
	<cffunction access="private" name="explicitInit" returntype="void" hint="Initialization">
		<cflog text="PROCESSM61.EXPLICITINIT()">
			<cfset VARIABLES.MY_CODE=createObject("component","fr.consotel.consoview.M61.ServiceHandlerM61")>
	
	</cffunction>

	<!--- Obligatoire pour Evènement : Appelé comme constructeur pour tous les POSTPROCESS --->
	<cffunction access="public" name="createInstance" returntype="fr.consotel.api.ibis.publisher.handler.IServiceHandler" hint="Constructeur de cette classe">
		<cfset SUPER.createInstance()><!--- Constructeur de la classe parent --->
		<cfset explicitInit()><!--- constructeur Spécifique Container --->
		<cfreturn THIS>
	</cffunction>

</cfcomponent>
