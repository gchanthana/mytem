package gestionparc.ihm.fiches.listedispo
{
	import mx.controls.DataGrid;

	public interface IListeDispo
	{
		function getIDSelection():int;
		function getLibelleSelection():String;
		function getLibelleType():String;
		function getDataGrid():DataGrid;
		function getSourceIcone():Class;
		function getSelection():Object;
	}
}
