<cfcomponent output="true" extends="fr.saaswedo.api.datalert.Core">
	
	<cffunction access="private" name="URL_COMPLETE_APP_USAGE" returntype="String">
		<cfset url_app="#url_server#/api/da/app.cfm">
		<cfreturn  url_app>
	</cffunction>
	
	<cffunction name="init" access="private" output="true" returntype="void">
	
		<cfargument name="httpRequestData" type="String" required="true">
	
		<cfset serverName=URL_COMPLETE_APP_USAGE()>
		
		<cfset bdd=createobject("component","fr.saaswedo.api.datalert.Bdd")>
		<cfset jsonRequest=deserializejson(httpRequestData)>
		
		<cfif structKeyExists(#jsonRequest#,"devId") and jsonRequest.devId neq "">
			<cfset  devId = #jsonRequest.devId# >
			<cfset  cause =" cause :  devId = #devId# non trouvé dans la base SWD">
		</cfif>
				
		<cfif structKeyExists(#jsonRequest#,"data") and jsonRequest.data neq "">
			<cfset donnees = jsonRequest.data>
		</cfif>
		
		<cflog  text="initialisation variables pour add_app_usage = OK " type="information">
	
	</cffunction>
	
	<cffunction name="add_app_usage" access="public" returntype="any" output="true">
			
		<cfargument name="httpRequestData" 	type="String" required="true">
		<cfargument name="user_agent"		type="String" required="true">
		<cfargument name="ip_origin" 		type="String" required="true">

		<cfset uuid =createuuid() >				

		<cftry>
			<cfset init(httpRequestData)>
			<!--- logger les données reçues --->
			<cfset saveLog(uuid,"#Debut# add_app_usage","#lab_recevedData# #httpRequestData# #newLine# user_agent: #user_agent# #newLine# ip_origin : #ip_origin#",idMntMobileProxy) >

			<cfset devInfo=bdd.getStoredInfo(devId)> <!---  get device by id --->
			
			<cfif devInfo.devid neq "">
			
				<cfhttp method="post" url="#serverName#" useragent="#user_agent#">
					<cfhttpparam type="header" name="Authorization" value="#authorization#"><!--- Authentication basique obligatoire sinon erreur  --->
					<cfhttpparam type="header" name="Accept" value="application/json">
					<cfhttpparam type="header" name="UUID" value="#devInfo.PhoneNum#"> <!--- obligatoire sinon erreur ({"Message":"UUID not present in the HTTP header."} ou  {"Message":"UUID : bad format"} ) --->
		 			<cfhttpparam type="formfield" name="data" value="#donnees#"> 	
				</cfhttp>
				
				<!---  logger les données envoyées à l'API SWD  --->	
				<cfset saveLog(uuid,"#invoke# add_app_usage","#lab_sentData# #donnees# #newLine# avec UUID = #devInfo.PhoneNum# comme Header http#newLine# #link# #serverName#",idMntEventProxyAPI) >
				
				<cfif cfhttp.Responseheader.Status_Code eq "200">
					
					<cflog type="information" text=" reponse http OK - methode : add_app_usage, on renvoie resp:ok ">
					
					<!--- logger la réponse de l'API SWD  --->
					<cfset saveLog(uuid,"#responseSWD# add_app_usage","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName#",idMntEventAPIProxy) >
					
					<cfset responeJson = '{"resp":"OK"}' >
					
					<!---  logger la réponse envoyée au mobile --->
					<cfset saveLog(uuid,"#responseProxy# add_app_usage","#respHttp# #responeJson#",idMntEventProxyMobile) >
					<cfreturn responeJson>		
					
		 		<cfelse> <!--- logger le message de l'API SWD en cas d'erreur, par exemple :  {"Message":"phone_no is not present"}  et le meme message est renvoyé au mobile ---> 
		 			<cflog type="information" text="reponse http not OK - methode : add_app_usage">
		 			<cfset saveLog(uuid,"#responseSWD# add_app_usage","#respHttp# #cfhttp.Filecontent# #newLine# #link# #serverName# #newLine# #info#",idMntEventAPIProxy) >
					<cfset responeJson = cfhttp.Filecontent >
					<cfreturn responeJson>
				</cfif>
			<cfelse>
				<cflog type="information" text="device non trouve dans la base SWD - methode : add_app_usage ">
				<cfheader name="status" value="421">
				<cfset responeJson = '{"resp":"Device is not registered"}' >
				<!---  logger la réponse envoyée au mobile --->
				<cfset saveLog(uuid,"#responseProxy# add_app_usage","#respHttp# #responeJson# #newLine# #cause#",idMntEventProxyMobile) >
				<cfreturn responeJson>	
			</cfif>
		<cfcatch type="any"> <!---  logger l'exception --->
			<cfthread action="sleep" duration="1000"/>
			<cfset saveLog(uuid,"#exce# add_app_usage",bodyException(CFCATCH),idMntEventProxyException) >
			<cfset responeJson = '{"resp":"#cfcatch.Message#" }' >
			<cfreturn responeJson>
		</cfcatch>
		</cftry>
						
	</cffunction>
	
</cfcomponent>