<cfcomponent name="grilletarif">
	
	<cfscript>
		//Initialize the CFC with the default properties values.
		variables.idGroupe = session.perimetre.ID_GROUPE;
		variables.idGroupeLigne = session.perimetre.ID_PERIMETRE;
		variables.typePerimetre = session.perimetre.TYPE_PERIMETRE;
	</cfscript>
	
	<cffunction name="L_OpeRacine" access="remote" returntype="query" description="retourne la liste des operateurs d une racine">
		<cfargument name="idracine" required="false" type="numeric" default="" displayname="numeric" />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.L_OpeRacine" >
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocresult name="tab_op">
		</cfstoredproc>
		
		<cfreturn tab_op />
	</cffunction>

	<cffunction name="L_OpeLibelles" access="remote" returntype="query" description="retourne la liste des libelle de type compte sous compte d une racine">
		<cfargument name="operateurid" required="false" type="numeric" default=""  />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.L_OpeLibelles">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#operateurid#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result />
	</cffunction>

	<cffunction name="L_Versions_generales" access="remote" returntype="query" description="retourne les versions du tarif general">
		<cfargument name="idracine" required="false" type="numeric" default=""  />
		<cfargument name="idgrp_ctl" required="false" type="numeric" default=""  />
	
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.L_Versions_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idgrp_ctl#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result />
	</cffunction>

	<cffunction name="taguer_grp_ctl" access="remote" returntype="any" description="Met a jour l etat de la case a cocher controlee dans l ihm	">
		<cfargument name="ID_GROUPEPRODUIT" required="false" type="numeric" default=""  />
		<cfargument name="p_idracine" required="false" type="numeric" default=""  />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.taguer_grp_ctl">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_GROUPEPRODUIT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour />
	</cffunction>

	<cffunction name="addVersionTarif" access="remote" returntype="any" description="Ajout d une version de tarif,  si les parametres ID_COMPTE_FACTURATION et ID_SOUS_COMPTE_FACTURATION sont null, cette procedure creer une version de tarif generale, sinon elle creer une version de tarif de type exeption Info: si   ID_COMPTE_FACTURATION est renseigne, ID_SOUS_COMPTE_FACTURATION est a null et inversement, si ID_SOUS_COMPTE_FACTURATION est renseigne ID_COMPTE_FACTURATION est a null	">
		<cfargument name="ID_GROUPEPRODUIT" required="false" type="numeric" default=""  />
		<cfargument name="ID_RACINE" required="false" type="numeric" default=""  />
		<cfargument name="TYPE_TARIF" required="false" type="string" default=""  />
		<cfargument name="ID_COMPTE_FACTURATION" required="false" type="numeric" default=""  />
		<cfargument name="ID_SOUS_COMPTE_FACTURATION" required="false" type="numeric" default=""  />
		<cfargument name="DATE_DEB_VALIDITE" required="false" type="numeric" default=""  />
		<cfargument name="PRIX_UNITAIRE" required="false" type="numeric" default=""  />
		<cfargument name="PRIX_REMISE" required="false" type="numeric" default=""  />
		<cfargument name="POURCENT_REMISE" required="false" type="numeric" default=""  />
			
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.I_version_Ctl">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_GROUPEPRODUIT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TYPE_TARIF#">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#ID_COMPTE_FACTURATION#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_SOUS_COMPTE_FACTURATION#">
			<cfprocparam type="In" cfsqltype="CF_SQL_DATE" value="#DATE_DEB_VALIDITE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#PRIX_UNITAIRE#">
				<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#PRIX_REMISE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#POURCENT_REMISE#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour />
	</cffunction>



	<cffunction name="RemoveAllVersionTarif" access="remote" returntype="any" description="Supprime toutes les versions d une Exeption au tarif General + l exeption passee en parametre	">
		<cfargument name="ID_GROUPEPRODUIT" required="false" type="numeric" default=""  />
		<cfargument name="ID_RACINE" required="false" type="numeric" default=""  />
		<cfargument name="ID_COMPTE_FACTURATION" required="false" type="numeric" default=""  />
		<cfargument name="ID_SOUS_COMPTE_FACTURATION" required="false" type="numeric" default=""  />
		<cfargument name="TYPE_TARIF" required="false" type="string" default=""  />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m32.Del_all_version_exception">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_GROUPEPRODUIT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_iNTEGER" value="#ID_COMPTE_FACTURATION#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_SOUS_COMPTE_FACTURATION#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TYPE_TARIF#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour />
	</cffunction>

	<cffunction name="updateDateVersionTarif" access="remote" returntype="any" description="renseigner la Date Fin Validite de la version d exception de tarif la plus recente">
		<cfargument name="ID_VERSION_TARIF" type="numeric" required="true" />
		<cfargument name="DATE_FIN" type="numeric" required="true" />
		<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m32.U_date_exception">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_VERSION_TARIF#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_DATE" value="#DATE_FIN#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour />
	</cffunction>

	<cffunction name="getTarifException" access="remote" returntype="any" description="retourne toute les exceptions valide au tarif general passe en parametre">
		<cfargument name="RACINE" required="false" type="numeric" default=""  />
		<cfargument name="GROUPEPRODUIT" required="false" type="numeric" default=""  />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.SearchException_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#GROUPEPRODUIT#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#SESSION.USER.GLOBALIZATION#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result />
	</cffunction>

	<cffunction name="getVersionTarifException" access="remote" returntype="query" description="retourne toutes les versions du tarif exception valide">
		<cfargument name="RACINE" required="false" type="numeric" default=""  />
		<cfargument name="ID_GROUPE_PROD" required="false" type="numeric" default=""  />
		<cfargument name="TYPE_TARIF" required="false" type="string" default=""  />
		<cfargument name="ID_COMPTE" required="false" type="numeric" default=""  />
		<cfargument name="ID_SOUS_COMPTE" required="false" type="numeric" default=""  />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.L_Versions_exceptions">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_GROUPE_PROD#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#TYPE_TARIF#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_COMPTE#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_SOUS_COMPTE#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result />
	</cffunction>



	<cffunction name="removeVersionTarif" access="remote" returntype="any" description="Suppression d'une Version de tarif d'un Tarif Général">
		<cfargument name="ID_VERSION_TARIF" type="numeric" required="true" />
		<cfargument name="METHODE_SUPPRESSION" type="string" required="true" />
			<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m32.Del_version_Ctl">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_VERSION_TARIF#" null="false">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#METHODE_SUPPRESSION#">
			<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" variable="p_retour">
		</cfstoredproc>
		<cfreturn p_retour />
	</cffunction>

	<cffunction name="Searchcompte_sous_compte" access="remote" returntype="query" description="recherche de comptes sous compte (si il existe des exceptions sur les comptes/sous_comptes, ajout d un flag)	">
		<cfargument name="p_idracine" required="false" type="numeric" default=""  />
		<cfargument name="ID_OPERATEUR" required="false" type="numeric" default=""  />
		<cfargument name="numero" required="false" type="string" default=""  />		
		<cfargument name="type_tarif" required="false" type="string" default=""  />
		<cfargument name="groupeprod" required="false" type="numeric" default=""  />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.Searchcompte_sous_compte">
		<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" value="#idGroupe#">
		<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" value="#ID_OPERATEUR#">
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#numero#">			
		<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#type_tarif#">
		<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" value="#groupeprod#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result />
	</cffunction>

	<cffunction name="L_Produit_groupe" access="remote" returntype="query" description="Liste des produits d'un groupe">
		<cfargument name="p_idgrp_ctl" required="false" type="numeric" default=""  />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.L_Produit_groupe">
			<cfprocparam type="In" cfsqltype="CF_SQL_NUMERIC" value="#p_idgrp_ctl#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result />
	</cffunction>

	<cffunction name="SearchGroupe" access="remote" returntype="query" description="retourne toute les exceptions valide au tarif général passé en parametre">
		<cfargument name="idracine" required="false" type="numeric" default=""  />
		<cfargument name="operateurid" required="false" type="numeric" default=""  />
		<cfargument name="code_type_tarif" required="false" type="numeric" default=""  />
		<cfargument name="type_grp_ctl" required="false" type="numeric" default=""  />
		<cfargument name="controler" required="false" type="numeric" default=""  />
		<cfargument name="grp_segment" required="false" type="numeric" default=""  />
		<cfargument name="avec_exception" required="false" type="numeric" default=""  />
		<cfargument name="etat_exception" required="false" type="numeric" default=""  />
		<cfargument name="libelle_grp" required="false" type="string" default=""  />
		<cfargument name="avec_calcul" required="false" type="numeric" default=""  />
		<cfargument name="index_debut" required="false" type="numeric" default=""  />
		<cfargument name="number_of_records" required="false" type="numeric" default=""  />
		<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.SearchGroupe_v2">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idGroupe#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#operateurid#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#code_type_tarif#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#type_grp_ctl#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#controler#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#grp_segment#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#avec_exception#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#etat_exception#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" value="#libelle_grp#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#avec_calcul#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#index_debut#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#number_of_records#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#SESSION.USER.IDGLOBALIZATION#">
			<cfprocresult name="p_result">
		</cfstoredproc>
		<cfreturn p_result />
	</cffunction>
<cffunction name="save_X_version_tarif_general" access="remote" returntype="numeric">
		<cfargument name="tab_abo" type="any" required="true">
		<cfset len = arrayLen(tab_abo)>
		<cfset r = -1>
		<cfloop index="i" from="1" to="#len#">
			<cfset r = UpdateVersionTarif(tab_abo[i].idVersionTarif,tab_abo[i].prixUnitaire,tab_abo[i].prixUnitaireRemise,tab_abo[i].remisePercent)>
		</cfloop>
		<cfreturn r>
</cffunction>

<cffunction name="UpdateVersionTarif" access="remote" returntype="any" description="">
	<cfargument name="ID_VERSION_TARIF" required="false" type="numeric" default=""  />
	<cfargument name="PRIX_UNITAIRE" required="false" type="numeric" default=""  />
	<cfargument name="PRIX_REMISE" required="false" type="numeric" default=""  />
	<cfargument name="POURCENT_REMISE" required="false" type="numeric" default=""  />
	<cfstoredproc datasource="#Session.offreDSN#" procedure="pkg_m32.U_version_Ctl">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#ID_VERSION_TARIF#">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#PRIX_UNITAIRE#">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#PRIX_REMISE#">
		<cfprocparam type="In" cfsqltype="CF_SQL_FLOAT" value="#POURCENT_REMISE#">
		<cfprocparam type="Out" cfsqltype="CF_SQL_VARCHAR" variable="p_retour">
	</cfstoredproc>
	<cfreturn p_retour />
</cffunction>

<cffunction name="addAboClient" access="remote" returntype="any">
	<cfargument name="idClient"  type="numeric" required="true">
	<cfargument name="idabo" type="numeric" required="true">
	<cfstoredproc datasource="#session.offreDSN#" procedure="pkg_m24.addAbonnementClient">
		<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" value="#idabo#" null="false">
		<cfprocparam type="In" cfsqltype="CF_SQL_inTEGER" value="#idClient#" null="false">
		<cfprocparam type="Out" cfsqltype="CF_SQL_inTEGER" variable="p_result">
	</cfstoredproc>
	<cfreturn p_result>
</cffunction>
</cfcomponent>