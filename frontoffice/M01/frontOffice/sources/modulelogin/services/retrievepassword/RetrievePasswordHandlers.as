package modulelogin.services.retrievepassword
{
    import flash.events.EventDispatcher;
    import modulelogin.event.RetrievePasswordEvent;
    import mx.collections.ArrayCollection;
    import mx.rpc.events.ResultEvent;

    public class RetrievePasswordHandlers extends EventDispatcher
    {
        private var myDatas:RetrievePasswordDatas;

        public function RetrievePasswordHandlers(instanceOfDatas:RetrievePasswordDatas)
        {
            myDatas = instanceOfDatas;
        }

        //METHODE INTERNAL------------------------------------------------------------------------
        internal function getUserPasswordResultHandler(re:ResultEvent):void
        {
            try
            {
                if (re.result != -1)
                {
                    // un email a été envoyé
                    dispatchEvent(new RetrievePasswordEvent(RetrievePasswordEvent.PASSWORD_EMAILED));
                }
                else
                {
                    // l'utilisateur n'est pas référencé dans la base
                    dispatchEvent(new RetrievePasswordEvent(RetrievePasswordEvent.USER_NOT_FOUND));
                }
            }
            catch (e:Error)
            {
                throw new Error(" # RetrievePasswordHandlers-getUserPasswordResultHandler() Erreur :" + e);
            }
        }
    }
}