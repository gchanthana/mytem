package univers.inventaire.inventaire.journaux
{
	import mx.events.FlexEvent;
	import mx.rpc.AbstractOperation;
	import mx.collections.ArrayCollection;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.events.FaultEvent;
	
	import mx.controls.dataGridClasses.DataGridColumn;
	import composants.util.DateFunction;
	import composants.util.ConsoviewFormatter;
	
	/**
	 * Classe non utilisée
	 * 
	 * */
	public class FacturationHorsInventaire extends FacturationHorsInventaireIHM
	{
		private var op : AbstractOperation;
		[Bindable]
		private var dataGridData : ArrayCollection;
		
		public function FacturationHorsInventaire()
		{
			//TODO: implement function
			super();			
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		public function refresh():void{
			chargerData();
		}
		
		private function initIHM(fe : FlexEvent):void{
			myGrid.dataProvider = dataGridData;			
			dateFacture.labelFunction = formateDates;
			montantfacture.labelFunction = formateEuros;
			numligne.labelFunction = formateLigne;
			
		}
		
		private function formateDates(item : Object, column : DataGridColumn):String{
			var ladate:Date = new Date(item.DATE_ACTION);
			return DateFunction.formatDateAsString(ladate);			
		}
		
		private function formateEuros(item : Object, column : DataGridColumn):String{
			
			//return ConsoviewFormatter.formatEuroCurrency(item.MONTANT_FACTURE,2);	
			return "";		
		}
		
		private function formateLigne(item : Object, column : DataGridColumn):String{
			
			return ConsoviewFormatter.formatPhoneNumber(item.SOUS_TETE);			
		}
		
		
		
		private function chargerData():void
		{
			var idGroupeMaitre:Number = CvAccessManager.getSession().CURRENT_PERIMETRE.PERIMETRE_INDEX;
						
			op = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
																				"fr.consotel.consoview.inventaire.cycledevie.Journaux",
																				"getListeRessourcesFactureesHorsInvt",
																				chargerDataResultHandler);				
			RemoteObjectUtil.callService(op,idGroupeMaitre,"");		
		}
		
		private function chargerDataResultHandler(re :ResultEvent):void{
			
			dataGridData = re.result as ArrayCollection;
			myGrid.dataProvider = dataGridData;
			dataGridData.refresh();			 
		}
		
		
	}
}