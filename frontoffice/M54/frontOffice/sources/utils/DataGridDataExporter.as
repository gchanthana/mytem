/**
	_________________________________________________________________________________________________________________

	DataGridDataExporter is a util-class to export DataGrid's data into different format.	
	@class DataGridDataExporter (public)
	@author Abdul Qabiz (mail at abdulqabiz dot com) 
	@version 0.01 (2/8/2007)
	@availability 9.0+
	@usage<code>DataGridDataExporter.<staticMethod> (dataGridReference)</code>
	@example
		<code>
			var csvData:String = DataGridDataExporter.exportCSV (dg);
		</code>
	__________________________________________________________________________________________________________________

**/
package utils
{
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.collections.IViewCursor;
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridColumn;

	public class DataGridDataExporter
	{

		public static function getCSVString(dg:DataGrid, csvSeparator:String="\t", lineSeparator:String="\n", onlySelectedElement:Boolean = false):String
		{
			var data:String = "";
			var columns:Array = dg.columns;
			var columnCount:int = columns.length;
			var column:DataGridColumn;
			var header:String = "";
			var headerGenerated:Boolean = false;
			var dataProvider:Object = dg.dataProvider;

			var rowCount:int = dataProvider.length;
			var dp:Object = null;

		
			var cursor:IViewCursor = dataProvider.createCursor ();
			var j:int = 0;
			
			var condition:Boolean = true;
			
			
			//loop through rows
			while (!cursor.afterLast)
			{
				if(onlySelectedElement && !cursor.current.SELECTED)
				{		
				 	cursor.moveNext();	
				}
				else
				{
					var obj:Object = null;
					obj = cursor.current;
					
					//loop through all columns for the row
					for(var k:int = 0; k < columnCount; k++)
					{
						
						column = columns[k];
						
						
						//Exclude column data which is invisible (hidden) and no header
						if(!column.visible || !(column).headerText)
						{
							continue;
						}
						
						var myPattern:RegExp = /€/g;
						data += "\""+ column.itemToLabel(obj).replace(myPattern,"")+ "\"";
	
						if(k < (columnCount))
						{
							data += csvSeparator;
						}
	
						//generate header of CSV, only if it's not genereted yet
						if (!headerGenerated)
						{
							header += "\"" + column.headerText + "\"";
							if (k < columnCount)
							{
								header += csvSeparator;
							}
						}
						
					
					}
					
					headerGenerated = true;
	
					if (j < (rowCount))
					{
						data += lineSeparator;
					}
	
					j++;
					cursor.moveNext ();
			
				}
				
			}
			
			//set references to null:
			dataProvider = null;
			columns = null;
			column = null;

			var allDataConcat:String = (header + "\r\n" + data);
			return allDataConcat;
		}
		
		public static function getCSVStringDataSet(dg:DataGrid, csvSeparator:String="\t", lineSeparator:String="\n", onlySelectedElement:Boolean = false):String
		{
			var data:String = "";
			var columns:Array = dg.columns;
			var columnCount:int = columns.length;
			var column:DataGridColumn;
			var header:String = "";
			var headerGenerated:Boolean = false;
			var dataProvider:Object = dg.dataProvider;

			var rowCount:int = dataProvider.length;
			var dp:Object = null;

		
			var cursor:IViewCursor = dataProvider.createCursor ();
			var j:int = 0;
			
			var condition:Boolean = true;
			
			
			//loop through rows
			while (!cursor.afterLast)
			{
				if(onlySelectedElement)
				{		
				 	cursor.moveNext();	
				}
				else
				{
					var obj:Object = null;
					obj = cursor.current;
					
					//loop through all columns for the row
					for(var k:int = 0; k < columnCount; k++)
					{
						
						column = columns[k];
						
						
						//Exclude column data which is invisible (hidden) and no header
						if(!column.visible || !(column).headerText)
						{
							continue;
						}
						
						var myPattern:RegExp = /€/g;
						data += "\""+ column.itemToLabel(obj).replace(myPattern,"")+ "\"";
	
						if(k < (columnCount))
						{
							data += csvSeparator;
						}
	
						//generate header of CSV, only if it's not genereted yet
						if (!headerGenerated)
						{
							header += "\"" + column.headerText + "\"";
							if (k < columnCount)
							{
								header += csvSeparator;
							}
						}
						
					
					}
					
					headerGenerated = true;
	
					if (j < (rowCount))
					{
						data += lineSeparator;
					}
	
					j++;
					cursor.moveNext ();
			
				}
				
			}
			
			//set references to null:
			dataProvider = null;
			columns = null;
			column = null;

			var allDataConcat:String = (header + "\r\n" + data);
			return allDataConcat;
		}
		
		public static function exportCSVString(urlString:String,csvString:String,fileName:String):void
		 {	
			var url:String = urlString;
	        var request:URLRequest = new URLRequest(url);         
	        var variables:URLVariables = new URLVariables();
	        variables.FILE_NAME = fileName;
	        variables.DATA = csvString;
	        request.data = variables;
	        request.method = URLRequestMethod.POST;
            navigateToURL(request,"_blank");
		 }	
	}

}
