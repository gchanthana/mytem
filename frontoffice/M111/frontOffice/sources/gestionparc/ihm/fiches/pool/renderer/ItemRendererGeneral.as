package gestionparc.ihm.fiches.pool.renderer
{
	import mx.containers.HBox;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.DataGridColumn;

	public class ItemRendererGeneral extends HBox
	{

		private var lb_libelle:Label;

		public function ItemRendererGeneral()
		{
			super();

			this.percentWidth=100;
			lb_libelle=new Label();
			this.addChild(lb_libelle);
		}


		override public function set data(value:Object):void
		{
			if (value != null)
			{
				super.data=value;

				setStyle("backgroundColor", 0xFFFFFF);
				setStyle("backgroundAlpha", 0);
			}
		}

		public function set textLabel(value:String):void
		{
			lb_libelle.text=value;
			if (value != "")
			{
				lb_libelle.toolTip=value;
			}
		}

		public function get textLabel():String
		{
			return lb_libelle.text;
		}
	}
}
