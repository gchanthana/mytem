package listedesfactures.entity
{
	
	import mx.collections.ArrayCollection;
	
	/**
	 * <b>Classe <code>OperateurVo</code></b>
	 * 
	 * <p><pre>
	 * Auteur : Sahraoui Mohamed
	 * Version : 1.0
	 * Date : 31.08.2010
	 * </pre></p> 
	 * 
	 */
	
	[Bindable]
	public class OperateurVo
	{
		
		public var NOM:String;
		public var OPERATEURID:String;
		public var IDORGA:String;
		
		/**
		 * <b>Constructeur <code>OperateurVo()</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Constructeur de la classe OperateurVo.
		 * </pre></p>
		 */
		public function OperateurVo()
		{
		}
		
		/**
		 * <b>Fonction <code>getParameters(value:Object)</code></b>
		 * <p><pre>
		 * <u>Description :</u>
		 * Set de OperateurVo
		 * </pre></p>
		 *
		 * @value Object 
		 *
		 */
		public function getParameters(value:Object):void{
			this.NOM = value.NOM;
			this.OPERATEURID = value.OPERATEURID;
			this.IDORGA = value.IDORGA;
		}
		
	}
}