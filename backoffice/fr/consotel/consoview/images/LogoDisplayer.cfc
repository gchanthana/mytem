<!--- =========================================================================
Classe: Facture63
Auteurs: Cedric RAPIERA, Brice MIRAMONT
$Historique: $
$Version: 1.0 $
========================================================================== --->

<cfcomponent name="LogoDisplayer" displayname="LogoDisplayer" hint="Permet d'afficher un logo">

	<cffunction name="DisplayLogoByOperateurID" access="public" returntype="void" output="true" >
		<cfargument name="OPERATEURID" required="true" type="numeric">
		<cfargument name="width" required="true" type="numeric">
		<cfquery name="getLogoID" datasource="#session.OffreDSN#">
			SELECT IDLOGO
			FROM OPERATEUR
			WHERE OPERATEURID=#OPERATEURID#
		</cfquery>
		<cfset objimg=createObject("component","fr.consotel.consoview.images.images")>
		<cfset objimg.displayimage(#getLogoID.IDLOGO#,#width#)>
	</cffunction>

</cfcomponent>
