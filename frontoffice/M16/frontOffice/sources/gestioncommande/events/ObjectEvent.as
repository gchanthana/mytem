package gestioncommande.events
{
	import gestioncommande.entity.ItemSelected;
	
	import flash.events.Event;
	
	public class ObjectEvent extends Event
	{
		
		public static const PRICE_CHANGE	:String = "PRICE_CHANGE";
		public var object					:ItemSelected = null;
		
		public function ObjectEvent(type:String, bubbles:Boolean = false, obj:ItemSelected = null, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.object	= obj;
		}

		override public function clone():Event
		{
			return new ObjectEvent(type, bubbles, object, cancelable);
		}

	}
}