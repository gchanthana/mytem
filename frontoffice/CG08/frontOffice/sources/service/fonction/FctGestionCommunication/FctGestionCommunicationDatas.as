package service.fonction.FctGestionCommunication
{
	import entity.ActualiteVO;
	import entity.ApplicationVO;
	import entity.LangueVO;
	import entity.PieceJointeActualiteVO;
	
	import event.fonction.fctGestionCommunication.FctGestionCommunicationEvent;
	
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;

	/**
	 * <p><pre>
	 * <u>Description :</u>
	 * Traite les données reçues aux retours des procedures appelées dans <code>FctGestionCommunicationServices</code>.
	 * 
	 * Version 	: 1.0
	 * Date 	: 13.08.2013
	 * </pre></p>
	 */
	
	public class FctGestionCommunicationDatas extends EventDispatcher
	{
		//------------ VARIABLES ------------//
		
		private var _listeApplication	:ArrayCollection = new ArrayCollection();
		private var _listeLangue		:ArrayCollection = new ArrayCollection();
		private var _listePJActualite	:ArrayCollection = new ArrayCollection();
		private var _listeActualite		:ArrayCollection = new ArrayCollection();
		private var _infosActualite		:ActualiteVO = new ActualiteVO();
		private var _idActualite		:int;
		
		
		//------------ METHODES ------------//
		
		/* */
		public function FctGestionCommunicationDatas()
		{
		}
		
		/* */
		public function treatGetListeApplication(ret:Object):void
		{
			var longResult:int = (ret.RESULT as ArrayCollection).length;
			
			_listeApplication.removeAll();
			for(var i:uint=0; i<longResult; i++)
			{
				if(ret.RESULT[i].CODEAPP != 51) // on ne recupere pas l'application Pilotage
				{
					var application:ApplicationVO = new ApplicationVO();
					application.fill(ret.RESULT[i]);
					_listeApplication.addItem(application);
				}
			}
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.LISTE_APPLICATION_EVENT));
		}
		
		/* */
		public function treatGetListeActualite(ret:Object):void
		{
			var longResult:int = (ret.RESULT as ArrayCollection).length;
			
			_listeActualite.removeAll();
			for(var i:uint=0; i<longResult; i++)
			{
				var actualite:ActualiteVO = new ActualiteVO();
				actualite.fill(ret.RESULT[i]);
				_listeActualite.addItem(actualite);
			}
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.LISTE_ACTUALITE_EVENT));
		}
		
		/* */
		public function treatGetLangueByApp(ret:Object):void
		{
			var longResult:int = (ret.RESULT as ArrayCollection).length;
			
			_listeLangue.removeAll();
			for(var i:uint=0; i<longResult; i++)
			{
				var langue:LangueVO = new LangueVO();
				langue.fill(ret.RESULT[i]);
				_listeLangue.addItem(langue);
			}
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.LISTE_LANGUE_EVENT));
		}
		
		/* */
		public function treatGetInfosActualite(ret:Object):void
		{
			var longResult:int = (ret.RESULT as ArrayCollection).length;
			
			for(var i:uint=0; i<longResult; i++)
			{
				_infosActualite.fill(ret.RESULT[i]);
			}
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.INFOS_ACTUALITE_EVENT));
		}
		
		/* */
		public function treatGetPJActualite(ret:Object):void
		{
			var longResult:int = (ret.RESULT as ArrayCollection).length;
			
			for(var i:uint=0; i<longResult; i++)
			{
				var pjActu:PieceJointeActualiteVO = new PieceJointeActualiteVO();
				pjActu.fill(ret.RESULT[i]);
				_listePJActualite.addItem(pjActu);
			}
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.PJ_ACTUALITE_EVENT));
		}
		
		/* */
		public function treatCreateActualite(ret:Object):void
		{
			idActualite = ret.RESULT as int;
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.CREATE_ACTUALITE_EVENT));
		}
		
		/* */
//		public function treatAddFileToActualite(ret:Object):void
//		{
//			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.ADD_FILE_TO_ACTUALITE_EVENT));
//		}
		
		/* */
		public function treatUpdateSliderActualite(ret:Object):void
		{
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.UPDATE_SLIDER_ACTUALITE_EVENT));
		}
		
		/* */
		public function treatCopyActualite(ret:Object):void
		{
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.COPY_ACTUALITE_EVENT));
		}
		
		/* */
		public function treatDeleteActualite(ret:Object):void
		{
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.DELETE_ACTUALITE_EVENT));
		}
		
		/* */
		public function treatDeletePJActualite(ret:Object):void
		{
			this.dispatchEvent(new FctGestionCommunicationEvent(FctGestionCommunicationEvent.DELETE_PJ_ACTUALITE_EVENT));
		}
		
		
		//------------ GETTERS - SETTERS ------------//
		
		public function get listeApplication():ArrayCollection
		{
			return _listeApplication;
		}
		
		public function get listeActualite():ArrayCollection
		{
			return _listeActualite;
		}
		
		public function set listeActualite(value:ArrayCollection):void
		{
			_listeActualite = value;
		}
		
		public function get listeLangue():ArrayCollection
		{
			return _listeLangue;
		}
		
		public function set listeLangue(value:ArrayCollection):void
		{
			_listeLangue = value;
		}
		
		public function get infosActualite():ActualiteVO
		{
			return _infosActualite;
		}
		
		public function set infosActualite(value:ActualiteVO):void
		{
			_infosActualite = value;
		}

		public function get idActualite():int
		{
			return _idActualite;
		}

		public function set idActualite(value:int):void
		{
			_idActualite = value;
		}

		public function get listePJActualite():ArrayCollection
		{
			return _listePJActualite;
		}

		public function set listePJActualite(value:ArrayCollection):void
		{
			_listePJActualite = value;
		}
	}
}