package {
	import entity.AppParams;
	
	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;
	
	public class ConsoViewSessionObject implements IConsoViewSessionObject {
		private var userField:ConsoViewUserObject;
		private var groupListField:Object;
		private var perimetreField:ConsoViewPerimetreObject;
		private var _INFOS_DIVERS:Object = new Object();
		private var _appParams:AppParams;
		private var _codeApplication :Number=101;
		
		public static const NONE_MODE:int = 0;
		public static const READ_MODE:int = 1;
		public static const WRITE_MODE:int = 2;
		
		
		
		public function ConsoViewSessionObject()
		{
			userField = new ConsoViewUserObject();
			perimetreField = new ConsoViewPerimetreObject();
		}
		
		internal function updateUserInfos(userInfos:Object):Boolean {
			return userField.updateUserInfos(userInfos);
		}

		internal function updateGroupList(groupList:Object):Boolean {
			groupListField = groupList;
			return true;
		}
		internal function updateCodeApplication(codeApplication:Number):void
		{
			this._codeApplication=codeApplication;
		}
		
		
//		internal function setGroupIndex(groupIdx:int):Boolean {
//			return perimetreField.setGroupIndex(groupIdx);
//		}
//		internal function setTypeLogin(typelogin:int):Boolean {
//			return perimetreField.setTypeLogin(typelogin);
//		}
		internal function updatePerimetre(perimetreInfos:Object):Boolean {
			return perimetreField.updatePerimetreInfos(perimetreInfos);
		}
		
		internal function updateAccess(accessInfos:XML):Boolean {
			return perimetreField.updateAccessInfos(accessInfos);
		}
		
		public function updatePerimetreXML(perimetreInfos:XML):Boolean {
			return perimetreField.updatePerimetreXML(perimetreInfos);
		}
		public function updateAppParams(params:AppParams):Boolean 
		{
			try
			{
				_appParams = params;
			}
			catch(err:Error)
			{
				return false;
			}
			return true;
		}
//		internal function updatePerimetreFromNode(perimetreNodeInfos:Object):Boolean {
//			return perimetreField.updatePerimetreFromNode(perimetreNodeInfos);
//		}
		
		internal function clearSession():void {
			groupListField = null;
			userField.clearUser();
			perimetreField.clearPerimetre();
		}
		
		public function get USER():IConsoViewUserObject {
			return userField;
		}
		
		public function get CURRENT_PERIMETRE():IConsoViewPerimetreObject {
			return perimetreField;
		}

		public function get GROUP_LIST():ArrayCollection {
			return groupListField as ArrayCollection;
		}
		
		internal function setDisplayDateDeb(dt:Date):void {
			perimetreField.setDisplayDateDeb(dt);
		}
		
		internal function setDisplayDateFin(dt:Date):void {
			perimetreField.setDisplayDateFin(dt);
		}
		
		internal function setMaxMonthToDisplay(maxMonth:Number):void {
			perimetreField.setMaxMonthToDisplay(maxMonth);
		}
		
		public function get CURRENT_ACCESS():int {
			if(CvAccessManager.CURRENT_FUNCTION == null) {
				if(CvAccessManager.CURRENT_UNIVERS == CvAccessManager.DEFAULT_UNIVERS_KEY) {
					var defaultUniversList:XMLList = perimetreField.accessListData.UNIVERS.(@KEY == CvAccessManager.DEFAULT_UNIVERS_KEY);
					if(defaultUniversList.length() == 1)
						return defaultUniversList[0].@USR; // DEFAULT Univers Access
					else {
						throw new IllegalOperationError("[CV-ERROR](ConsoViewSessionObject) Unable to find DEFAULT Univers");
						return ConsoViewSessionObject.NONE_MODE;
					}
				}
				else if(CvAccessManager.CURRENT_UNIVERS == CvAccessManager.DEFAULT_UNIVERS_KEY_PFGP)
				{
					var defaultUniversList2:XMLList = perimetreField.accessListData.UNIVERS.(@KEY == CvAccessManager.DEFAULT_UNIVERS_KEY_PFGP);
					if(defaultUniversList2.length() == 1)
						return defaultUniversList2[0].@USR; // DEFAULT Univers Access
					else {
						throw new IllegalOperationError("[CV-ERROR](ConsoViewSessionObject) Unable to find DEFAULT Univers");
						return ConsoViewSessionObject.NONE_MODE;
					}
				}
				else
				{
					throw new IllegalOperationError("[CV-ERROR](ConsoViewSessionObject) Function is null - Current Univers is not DEFAULT Univers");
					return ConsoViewSessionObject.NONE_MODE; // Function is null and Univers is not DEFAULT
				}
			} else {
				var currentAccessList:XMLList =
						perimetreField.accessListData.descendants("FONCTION").(@KEY == CvAccessManager.CURRENT_FUNCTION);
				if(currentAccessList.length() == 1) {
					return currentAccessList[0].@USR;
				} else {
					return ConsoViewSessionObject.NONE_MODE;
				}
			}
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////

		internal function updateNodeList(nodeListData:Object):Boolean {
			//return perimetreField.updateNodeList(nodeListData);
			return true;
		}

		internal function updatePerimetreInfos(perimetreInfos:Object):Boolean {
			return perimetreField.updatePerimetreInfos(perimetreInfos);
		}

		[Bindable]
		public function set INFOS_DIVERS(value:Object):void
		{
			if(value)
			{
				_INFOS_DIVERS = value;		
			}
			else
			{
				_INFOS_DIVERS = new Object();
			}
			
		}

		public function get INFOS_DIVERS():Object
		{
			if(_INFOS_DIVERS)
			{
				return _INFOS_DIVERS
			}
			else
			{
				return new Object();
			}
		}

		public function get AppPARAMS():AppParams
		{
			return _appParams;
		}
		
		public function get CODE_APPLICATION():Number
		{
			return  _codeApplication;
		}
	}
}
