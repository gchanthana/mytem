component  displayname="fr.saaswedo.api.order.sonar.entity.IssueMDN"
{
	property name="ArticleId" type="numeric" displayname="ArtilceId" hint="Identifiant de l'article";

	property name="Order" type="struct" displayname="Order" hint="All Order products";

	//operationMatrice = [{1282,1,1},{1283,5,1},{1383,3,1},{1483,4,1}];

	public IssueMDN function init(required numeric ArticleId, required struct Order)
	{
		this.ArticleId = arguments.ArticleId;
		this.Order = arguments.Order;
		return this;
	}

	public struct function getIssue()
		description="struct sonar ticket"
	{
		var issue = {};

		var MTIssueType = this.Order['COMMANDE']['IDTYPE_OPERATION'][1];

		switch(val(MTIssueType)){
			case 1282:
				issue = getModifyOptionsTicket(this.ArticleId,this.Order["ARTICLE"],this.Order["PLAN"]);
			break;

			case 1283:
				issue = getTerminationTicket(this.ArticleId,this.Order["ARTICLE"]);
			break;

			case 1383:
				issue = getSuspensionTicket(this.ArticleId,this.Order["ARTICLE"]);
			break;

			case 1483:
				issue = getReactivationTicket(this.ArticleId,this.Order["ARTICLE"]);
			break;

			default:
				throw "Error Unknow type";
			break;
		}
		return issue;
	}




	private struct function  getModifyOptionsTicket(required numeric ArticleId,required query qArticleSource,qPlanSource)
		description="struct modifications options"
	{
		var prefix = "::str::";
		var issue = {};
		var articleData = getArticleData(arguments.ArticleId,arguments.qArticleSource);
		var planData = getPlanData(arguments.ArticleId,arguments.qPlanSource);
		issue["CareTransactionPKey"] = 1; //modify options
		issue["IssueTypePKey"] = 1; //always 1
		issue["NewValue"] = planData;//"7807777";
    //issue["TargetESN"] = formatPhoneNumber(articleData["NUMERO"]);
    issue["TargetMDN"] = prefix & trim(formatPhoneNumber(articleData["NUMERO"]));
		return issue;
	}


	private struct function getTerminationTicket(required numeric ArticleId,required query qSource)
		description="struct Termination"
	{
		var prefix = "::str::";
		var issue = {};
		var articleData = getArticleData(arguments.ArticleId,arguments.qSource);
		issue["CareTransactionPKey"] = 5; //to terminate the liine
		issue["IssueTypePKey"] = 1; //always 1
		issue["TargetMDN"]= prefix & trim(formatPhoneNumber(articleData["NUMERO"])); //SOUS_TETE
		//issue["TargetESN"] = '';

		return issue;
	}

	private struct function getSuspensionTicket(required numeric ArticleId,required query qSource)
		description="struct suspension"
	{
		var prefix = "::str::";
		var issue = {};
		var articleData = getArticleData(arguments.ArticleId,arguments.qSource);
		issue["CareTransactionPKey"] = 3; //to Suspend the line
		issue["IssueTypePKey"] = 1; //always 1
		issue["TargetMDN"] = prefix & trim(formatPhoneNumber(articleData["NUMERO"])); //SOUS_TETE
		//issue["TargetESN"] = '';

		return issue;
	}


	private struct function getReactivationTicket(required numeric ArticleId,required query qSource)
		description="struct Reactivate "
	{
		var prefix = "::str::";
		var issue = {};
		var articleData = getArticleData(arguments.ArticleId,arguments.qSource);
		issue["CareTransactionPKey"] = 4; //to Reactivate the suspended line
		issue["IssueTypePKey"] = 1; //always 1
		issue["TargetMDN"] = prefix & trim(formatPhoneNumber(articleData["NUMERO"])); //SOUS_TETE
		//issue["TargetESN"] = '';

		return issue;
	}

	private struct function getArticleData(required numeric ArticleId,required query qSource)
	{
		var queryService = new query();
		queryService.setName("qGetArticle");
		queryService.setDBType("query");
		queryService.setAttributes(sourceQuery=arguments.qSource);
		var qArticle =
		queryService.execute(sql="SELECT * FROM sourceQuery  WHERE	IDARTICLE = #arguments.ArticleId#").getResult();



	    var allRows = qArticle.recordcount;
	    if(allRows gte 1)
	    {
	    	return GetQueryRow(qArticle,1);
	    }
	    else
	    {
	    	var ret = {};
	    	ret["NUMERO"] = "";
	    	ret[""];
			return ret;
	    }

	}

	private string function getPlanData(required numeric ArticleId,required query qPlanSource)
	{
		var prefix =  "::str::";
		var queryService = new query();
		queryService.setName("qGetPlan");
		queryService.setDBType("query");
		queryService.setAttributes(sourceQuery=arguments.qPlanSource);
		var qPlan =
		queryService.execute(sql="SELECT * FROM sourceQuery  WHERE	IDARTICLE = #arguments.ArticleId#").getResult();



	    var allRows = qPlan.recordcount;
	    var plans = [];
	    for (	var intRow = 1;
				intRow <= allRows;
				intRow ++)
		{
			if(qPlan['ACTION'][intRow] eq "A")
			{
			 ArrayAppend(plans,prefix & qPlan['REFERENCE'][intRow]);
			}
		}

		return ArrayToList(plans,",");

	}



	private struct function GetQueryRow(query, rowNumber)
	{
		var i = 0;
		var rowData = {};
		var cols = ListToArray(query.columnList);

		for (i = 1; i <= ArrayLen(cols); i++)
		{
			rowData[cols[i]] = query[cols[i]][rowNumber];
		}
		return rowData;
	}

	private string function formatPhoneNumber(required string input){
		var _input = arguments.input;
		var arrSearch = rematch("[\d]+",_input);
		var output = ArrayToList(arrSearch,"");
		return output;
	}




}
