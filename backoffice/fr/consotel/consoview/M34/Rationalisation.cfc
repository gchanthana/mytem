<cfcomponent output="false">

<cfset VARIABLES["reportServiceClass"]="fr.consotel.consoview.api.ibis.publisher.service.developer.DeveloperReportService">
<cfset VARIABLES["containerServiceClass"]="fr.consotel.consoview.api.container.ApiContainerV1">
<!--- ***************************** CODE A MODIFIER ******************************* --->		
<!--- Le chemin du rapport sous BIP Groupe--->
<cfset VARIABLES["XDO"]="/consoview/facturation/rapport/RationalisationGroupe/RationalisationGroupe.xdo">
<!--- Le chemin du rapport sous BIP GroupeLigne--->
<cfset VARIABLES["XDOLigne"]="/consoview/facturation/rapport/RationalisationGroupeLigne/RationalisationGroupeLigne.xdo">
<!--- Le code rapport --->
<cfset VARIABLES["CODE_RAPPORT"]="RationalisationGroupe">
<!--- TEMPLATE BI --->
<cfset VARIABLES["TEMPLATE"]="cv">
<!--- OUTPUT --->
<cfset VARIABLES["OUTPUT"]="Rapport de rationalisation">
<!--- Module --->
<cfset VARIABLES["MODULE"]="M34">



<cfset reportService = createObject("component",VARIABLES["reportServiceClass"])>
<cfset ApiE0 = CreateObject("component","fr.consotel.consoview.api.E0.ApiE0")>
<cfset containerService	= createObject("component",VARIABLES["containerServiceClass"])>


<cffunction name="executer" access="remote" returntype="any">
	<cfargument name="RapportParams" type="array" required="true">
	<cfset TYPE_PERIMETRE = RapportParams[1].TYPE_PERIMETRE>
	
	<!--- Format --->
	<cfset VARIABLES["FORMAT"]=RapportParams[1].FORMAT>
	
	<cfif lcase(#RapportParams[1].format#) eq "excel">
		<cfset typeformat = "excel">
	<cfelseif lcase(#RapportParams[1].format#) eq "csv">
		<cfset typeformat = "csv">
	<cfelseif lcase(#RapportParams[1].format#) eq "PDF">
		<cfset typeformat = "pdf">
	</cfif> 
	
	<!--- Extension --->
	<cfset VARIABLES["EXT"]=typeformat>
	
	<cfset ID_PERIMETRE = SESSION.PERIMETRE["ID_PERIMETRE"]>
	<cfset p_result = Evaluate("rapportRationalisation#TYPE_PERIMETRE#Strategy(ID_PERIMETRE,RapportParams[1])")>
	
	<cfreturn p_result>
	
</cffunction>



<cffunction name="rapportRationalisationGroupeLigneStrategy" access="private" returntype="any">		
	<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
	<cfargument name="RapportParams" required="true" type="struct"/>
	<cfset ID_ORGA = ID_PERIMETRE>
	<cfset biServer=APPLICATION.BI_SERVICE_URL>
	<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDOLigne"],"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
	<cfif initStatus EQ TRUE>
		<cfset setStatus=TRUE>
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDGROUPE_CLIENT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=ID_ORGA>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEDEB">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=RapportParams.DATEDEB>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEFIN">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=RapportParams.DATEFIN>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDRACINE_MASTER">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>	<!---  --->	<!---  --->	<!---  --->	<!---  --->	<!---  --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDGROUPE_PRODUIT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=RapportParams.PRODUITID>	<!---  --->	<!---  --->	<!---  --->	<!---  --->	<!---  --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>
	
	<cfif lcase(#RapportParams.format#) eq "excel">
		<cfset typeformat = "xls">
	<cfelseif lcase(#RapportParams.format#) eq "csv">
		<cfset typeformat = "csv">
	</cfif> 
	
	<!--- Rapport --->
	<cfset pretour = -1>
<cfif setStatus EQ TRUE>
	<cfset FILENAME=VARIABLES["OUTPUT"]>
	<cfset outputStatus = FALSE>
	<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],RapportParams.FORMAT,typeformat,FILENAME,VARIABLES["CODE_RAPPORT"])>
	<cfif outputStatus EQ TRUE>
		<cfset reportStatus = FALSE>
		<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
		<cfset pretour = reportStatus['JOBID']>
	</cfif>
</cfif>	
</cfif>	
<cfreturn pretour >

</cffunction>
	

<cffunction name="rapportRationalisationGroupeStrategy" access="private" returntype="any">
	<cfargument name="ID_PERIMETRE" required="true" type="numeric"/>
	<cfargument name="RapportParams" required="true" type="struct"/>
	<cfset ID_ORGA = ID_PERIMETRE>
	<cfset biServer=APPLICATION.BI_SERVICE_URL>
	<cfset initStatus=reportService.init(SESSION.PERIMETRE["ID_GROUPE"],VARIABLES["XDO"],"ConsoView",VARIABLES["MODULE"],VARIABLES["CODE_RAPPORT"])>
	<cfif initStatus EQ TRUE>
		<cfset setStatus=TRUE>
	<cfset ArrayOfParamNameValues=ArrayNew(1)>
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDGROUPE_CLIENT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=ID_ORGA>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[1]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEDEB">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=RapportParams.DATEDEB>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[2]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_DATEFIN">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=RapportParams.DATEFIN>
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[3]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDRACINE_MASTER">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=SESSION.PERIMETRE["IDRACINE_MASTER"]>	<!---  --->	<!---  --->	<!---  --->	<!---  --->	<!---  --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[4]=ParamNameValue>
	<!---  --->
	<cfset ParamNameValue=structNew()>
	<cfset ParamNameValue.multiValuesAllowed=FALSE>
	<cfset ParamNameValue.name="P_IDGROUPE_PRODUIT">
	<cfset t=ArrayNew(1)>
	<cfset t[1]=RapportParams.PRODUITID>	<!---  --->	<!---  --->	<!---  --->	<!---  --->	<!---  --->
	<cfset ParamNameValue.values=t>
	<cfset ArrayOfParamNameValues[5]=ParamNameValue>
	
	<cfif lcase(#RapportParams.format#) eq "excel">
		<cfset typeformat = "xls">
	<cfelseif lcase(#RapportParams.format#) eq "csv">
		<cfset typeformat = "csv">
	</cfif>
	
	
	<cfset pretour = -1>
	<cfif setStatus EQ TRUE>
		<cfset FILENAME=VARIABLES["OUTPUT"]>
		<cfset outputStatus = FALSE>
		<cfset outputStatus=reportService.setOutput(VARIABLES["TEMPLATE"],RapportParams.FORMAT,typeformat,FILENAME,VARIABLES["CODE_RAPPORT"])>
		<cfif outputStatus EQ TRUE>
			<cfset reportStatus = FALSE>
			<cfset reportStatus = reportService.runTask(SESSION.USER["CLIENTACCESSID"])>
			<cfset pretour = reportStatus['JOBID']>
		</cfif>
	</cfif>	
</cfif>	
	<cfreturn pretour >
	
	
	
</cffunction>
	

</cfcomponent>