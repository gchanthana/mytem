package event.tab
{
	import flash.events.Event;

	public class TabEvent extends Event
	{
		public static const CHANGER_DATA_TAB_EVENT :String ="changerDataTabEvent";
		
		public function TabEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new TabEvent(type, bubbles, cancelable);
		}
	}
}