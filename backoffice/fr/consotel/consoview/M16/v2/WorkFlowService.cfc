<cfcomponent displayname="fr.consotel.consoview.M16.v2.WorkFlowService" output="false">
	
	<!--- 
		FOURNIT LA LISTE LES ACTIONS POSSIBLES POUR UN GESTIONNAIRE, A PARTIR D'UN  ÉTAT DE COMMANDE. (DETAILLER CHAQUE ACTION) 
		SI IDETAT = 0 ET IDGESTIONNAIRE = 0 ON FOURNIT LA LISTE DE TOUTES LES ACTIONS.
		SI IDETAT &GT; ET IDGESTIONNAIRE = 0 ON FOURNIT LA LISTE DE TOUTES LES ACTIONS POSSIBLES É PARTIR DE L'ÉTAT.
		SI IDETAT = 0 ET IDGESTIONNAIRE = &GT; ON FOURNIT LA LISTE DE TOUTES LES ACTIONS POSSIBLES POUR LE GESTIONNAIRE.
		SI IDETAT = &GT; ET IDGESTIONNAIRE = &GT; ON FOURNIT LA LISTE DE TOUTES LES ACTIONS POSSIBLES É PARTIR DE L'ÉTAT ET POUR UN GESTIONNAIRE.
		
	 --->
	
	<cffunction name="fournirListeActionsPossibles" access="remote" returntype="query" output="false" hint="FOURNIT LA LISTE LES ACTIONS POSSIBLES POUR UN GESTIONNAIRE, A PARTIR D'UN  ÉTAT DE COMMANDE">		
		<cfargument name="idEtat" 	required="true" type="numeric" default="" />		
		<cfargument name="idPool" 	required="true" type="numeric" default="" />
		<cfargument name="segment" 	required="true" type="numeric" default="" />
		
		<cfargument name="idgestionnaire" 		required="false" type="numeric"/>
		<cfargument name="code_langue" 			required="false" type="numeric"/>
		
		<cfif isdefined("idgestionnaire")>
			<cfset _idgestionnaire= idgestionnaire>
		<cfelse>
			<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
		</cfif>
		
		<cfif isdefined("code_langue")>		
			<cfset _code_langue = code_langue>
			<cfelse>
			<cfset _code_langue  = SESSION.USER.GLOBALIZATION>
		</cfif>
	 
		<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.LISTEACTIONSPOSSIBLES" blockfactor="10" >			
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idetat" value="#idEtat#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" value="#_idgestionnaire#">
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idPool" value="#idPool#">
			<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" value="#_code_langue#" >	
			<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_segment" value="#segment#">	
			
			<cfprocresult name="result">
		</cfstoredproc>
		<cfreturn result>
	</cffunction>

	<!--- FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE --->
	
	<cffunction name="faireActionWorkFlow" access="remote" returntype="numeric" output="false" hint="FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE">
		<cfargument name="idAction" 		 	required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="dateAction" 		 	required="true" type="string" 	default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="commentaireAction" 	required="true" type="string"  default="" displayname="struct commande" 	hint="Initial value for the commandeproperty." />
		<cfargument name="idOperation" 		 	required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="boolmail" 		 	required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="logmail" 		 	 	required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="idgestionnaire" 		required="false" type="numeric"/>
		<cfargument name="idracine" 			required="false" type="numeric"/>
		
	
		 
		<cfif isdefined("idgestionnaire")>
			<cfset _idgestionnaire= idgestionnaire>
		<cfelse>
			<cfset _idgestionnaire= SESSION.USER.CLIENTACCESSID>
		</cfif>
		
		<cfif isdefined("idracine")>		
			<cfset _idracine = idracine>
			<cfelse>
			<cfset _idracine = SESSION.PERIMETRE.ID_GROUPE>
		</cfif>

		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.faireactiondeworkflowmobile_v5">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idracine" 		value="#_idracine#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_date_action" 	value="#dateAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_commentaire" 	value="#commentaireAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idgestionnaire" 	value="#_idgestionnaire#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idoperation" 	value="#idOperation#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idaction" 		value="#idAction#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_bool_mail" 		value="#boolmail#">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idcv_log_mail" 	value="#logmail#">
				<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
			</cfstoredproc>
			
			<!--- TEST POUR JAMESPOT  
					
			<cfset _log = _jslogCmd(idOperation)>
			
			 FIN TEST POUR JAMESPOT --->
					
		<cfreturn p_retour>
	</cffunction>
	
	<!--- FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE --->
	
	<cffunction name="faireXActionWorkFlow" access="remote" returntype="numeric" output="false" hint="FAIT UNE ACTION DE WORKFLOW POUR CHAQUE ARCTICLE DE LA COMMANDE">
		<cfargument name="idAction" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="dateAction" 		 required="true" type="date" 	default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="commentaireAction" required="true" type="string"  default="" displayname="struct commande" 	hint="Initial value for the commandeproperty." />
		<cfargument name="idOperation" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="boolmail" 		 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="logmail" 		 	 required="true" type="numeric" default="" displayname="Struct action" 		hint="Initial value for the actionproperty." />
		<cfargument name="idsCommande" 		 required="true" type="Array">

			<cfset idRacine = Session.PERIMETRE.ID_GROUPE>
			<cfset idGestionnaire = Session.USER.CLIENTACCESSID>
			<cfset checkOK = 1>
			
			<cfloop index="idx" from="1" to="#ArrayLen(idsCommande)#">
				<cfset idCommande = idsCommande[idx]>
				<cfif checkOK GTE 1>
					<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.faireActionDeWorkFlowMobile_v4">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idracine" 		value="#idRacine#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_date_action" 	value="#LSDATEFORMAT(dateAction,'YYYY/MM/DD')#">
						<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_commentaire" 	value="#commentaireAction#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idgestionnaire" 	value="#idGestionnaire#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idoperation" 	value="#idCommande#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idaction" 		value="#idAction#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_bool_mail" 		value="#boolmail#">
						<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER"  variable="p_idcv_log_mail" 	value="#logmail#">
						<cfprocparam type="out" cfsqltype="CF_SQL_INTEGER" variable="p_retour" >
					</cfstoredproc>					
					<cfset checkOK =  p_retour>
					
					<!--- TEST POUR JAMESPOT  
					
					<cfset _log = _jslogCmd(idCommande)>
					
					 FIN TEST POUR JAMESPOT --->
					
				</cfif>
			</cfloop>
			
			
		<cfreturn checkOK>
	</cffunction>

	<!--- COMPTEUR TIMER --->

	<cffunction name="timerDuration" access="private" returntype="numeric" output="false" hint="COMPTEUR TIMER">
		<cfargument name="delayTimer" 	required="false" type="numeric" default="5" hint="Par defaut 5 = 10s " />
			
			<cfset sessionTimeout = delayTimer*1000000>
			<cfloop index="idx" from="1" to="#sessionTimeout#"></cfloop>
			
		<cfreturn 1>
	</cffunction>	
	
	<!--- PROCEDURE HISTORIQUEETATSOPERATION(  P_IDCOMMANDE IN INTEGER,P_RETOUR OUT SYS_REFCURSOR) --->
	
	<cffunction name="fournirHistoriqueEtatsCommande" access="remote" returntype="Query" output="false" hint="FOURNIT L'HISTORIQUE DES ETATS D'UNE COMMANDE">
		<cfargument name="idCommande" required="true" type="numeric" displayname="ID DE LA COMMANDE"/>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.historiqueetatsoperation" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcommande" value="#idCommande#">	
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_codelangue" value="#session.user.globalization#">
				<cfprocresult name="p_retour">
			</cfstoredproc>
		
		<cfreturn p_retour>
	</cffunction>

	<!---  procédure pour savoir si une commande a déjà fait le renseignement de ref
     0:non , 1:oui--->
	
	<cffunction name="iscmdRef" access="remote" returntype="Numeric" output="false">
		<cfargument name="idCommande" required="true" type="numeric" displayname="ID DE LA COMMANDE"/>
		
			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.cdeRempliRef" >
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idcommande" value="#idCommande#">	
				<cfprocparam type="Out" cfsqltype="CF_SQL_INTEGER" 	variable="p_retour">
			</cfstoredproc>

		<cfreturn p_retour>
	</cffunction>
	
	<!--- PROCEDURE HISTORIQUE ACTION --->
	
	<cffunction name="getHistoriqueAction" access="remote" returntype="query" output="false">
		<cfargument name="idPool" 	required="true" type="numeric" 	 displayname="id pool" 			hint=""/>
		<cfargument name="dateDeb" 	required="true" type="Date" 	 displayname="date dateDebut"	hint=""/>
		<cfargument name="dateFin" 	required="true" type="Date" 	 displayname="date dateFin" 	hint=""/>
		<cfargument name="clef" 	required="true" type="string" 	 displayname="string clef" 		hint=""/>

			<cfset dateD = DateFormat(dateDeb,'YYYY-MM-DD')>
			<cfset dateF = DateFormat(DateAdd('d',1,dateFin),'YYYY-MM-DD')>

			<cfstoredproc datasource="#session.offredsn#" procedure="pkg_m16.gethistoaction_v2" blockfactor="100">
				<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" 	variable="p_idpool" 	value="#idPool#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_date_debut" value="#dateD#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_date_fin" 	value="#dateF#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR2" 	variable="p_cle" 		value="#clef#">
				<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR"  variable="p_langue" 		value="#session.user.globalization#">		
				<cfprocresult name="p_retour">			
			</cfstoredproc>
				
		<cfreturn p_retour>
	</cffunction>
	
	<!--- FOURNI TOUTES LES ACTIONS D'UN POOL --->
	
	<cffunction name="fournirToutesLesActionsPossible" access="remote" returntype="Query" output="false" hint="FOURNIT LA LISTE LES ACTIONS POSSIBLES POUR UN GESTIONNAIRE, A PARTIR D'UN  ÉTAT DE COMMANDE">		
		<cfargument name="idpool" required="true" type="Numeric" default="" />
		 
		 <cfset idRacine = SESSION.PERIMETRE.ID_GROUPE>

			<cfif idpool LTE 0>
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.ListeAllActionsPossibles_V2">			
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idracine" 	value="#idRacine#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#session.user.clientaccessid#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idPool#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#session.user.globalization#">	
					<cfprocresult name="p_retour">
				</cfstoredproc>
				
				<cfreturn p_retour>
			<cfelse>
				<cfstoredproc datasource="#Session.OFFREDSN#" procedure="PKG_M16.ListeAllActionsPossibles">			
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idgestionnaire" 	value="#session.user.clientaccessid#">
					<cfprocparam type="In" cfsqltype="CF_SQL_INTEGER" variable="p_idpool" 			value="#idPool#">
					<cfprocparam type="In" cfsqltype="CF_SQL_VARCHAR" variable="p_code_langue" 		value="#session.user.globalization#">	
					<cfprocresult name="p_retour">
				</cfstoredproc>
				<cfreturn p_retour>
			</cfif>
		
	</cffunction>
		
	<!--- TEST POUR JAMESPOT --->
	
	<cffunction name="_jslogCmd" access="private" hint="Log l'action de workflow dans le référentiel Jamespot via un thread">
		<cfargument name="idCommande" required="true" type="numeric" />
		
		<cfthread name="logCmd" action="run" idcmde="#idCommande#">
			<cfobject component="fr.consotel.consoview.mobile.proxyConsoView" type="component" name="obj">
			<cfset t = obj.updateJS(idcmde)> 
		</cfthread> 		
		
	</cffunction>
	
	<!--- FIN TEST POUR JAMESPOT --->
	
</cfcomponent>