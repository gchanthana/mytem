package univers.inventaire.inventaire.etapes
{
	import mx.events.FlexEvent;
	import flash.events.MouseEvent;
	import mx.managers.PopUpManager;
	
	/**
	 * Gere la 'Boite de Dialogue' de confirmation
	 **/
	public class ConfirmBox extends ConfirmBoxIHM
	{
		
		//ref vers le message de la boite de dialogue
		private var _message : String;
		
		//ref vers l'action a confirmater
		private var _action : String;
		
		
		/**
		 * Constructeur
		 * */
		public function ConfirmBox()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE,initIHM);
		}
		
		/**
		 * Setter sur le message de la boite de dialogue
		 * @param m le message à afficher
		 * */
		public function set message(m : String):void{
			_message = m;
		}
		
		/**
		 * Setter sur l'action à confirmer dans la boite de dialogue
		 * @param a l'action à confirmer ou infirmer
		 * */
		public function set action(a : String):void{
			_action = a;
		}
		
		//Initilaistion de l'IHIM		
		private function initIHM(fe : FlexEvent):void{
			btValider.addEventListener(MouseEvent.CLICK,validerConfirmationHandler);
			btAnnuler.addEventListener(MouseEvent.CLICK,annulerConfirmationHandler);			 
			txtMessage.htmlText = _message + "<b>"+_action+"</b>";
			PopUpManager.centerPopUp(this);
		}
		
		//Valide l'action et ferme la dialog box
		private function validerConfirmationHandler(me : MouseEvent):void{
			var ce : ConfirmationEvent = new ConfirmationEvent();
			ce.CommentaireConfirmation = txtCommentaire.text;
			dispatchEvent(ce);
			PopUpManager.removePopUp(this);
		}
		
		//Infirme l'action et ferme la dialog box
		private function annulerConfirmationHandler(me : MouseEvent):void{
			PopUpManager.removePopUp(this);
		}
	}
}