package service.diffusion.corpsmail
{
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	
	import mx.rpc.AbstractOperation;

	public class CorpsMailService
	{
		public var _model:CorpsMailModel;
		public var handler:CorpsMailHandler;	
		
		public function CorpsMailService()
		{
			this._model  = new CorpsMailModel();
			this.handler = new CorpsMailHandler(model);
		}
		public function get model():CorpsMailModel
		{
			return this._model;
		}	
		public function getCorpsMailApp(): void
		{
			var callOp:AbstractOperation = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
				"fr.consotel.consoview.M331.service.diffusion.CorpsMailAppService","getCorpsMailApp",handler.getCorpsMailAppHandler); 
			
			RemoteObjectUtil.callService(callOp);	
		}
	}
}