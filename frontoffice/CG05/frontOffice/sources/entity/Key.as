package entity
{
	public class Key
	{
		private var _KEY:String; 
		private var _CLASSE:String;
		private var _isFunction:Boolean;
		private var _isMenu:Boolean;
		
		public function Key(key:String, classe:String, isFunction:Boolean = true,isMenu:Boolean = false)
		{
			this._KEY = key;
			this._CLASSE = classe;
			this._isFunction = isFunction;
			this._isMenu = isMenu;
		}
		public function get CLASSE():String { return _CLASSE; }
		public function get KEY():String { return _KEY; }
		public function get isFunction():Boolean { return _isFunction; }
		public function get isMenu():Boolean { return _isMenu; }
	}
}