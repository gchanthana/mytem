package univers.inventaire.inventaire.creation.nouvelleResources.commande
{
	import mx.collections.ArrayCollection;
	
	/**
	 * Classe Factory chargé de construire les différents écrans relatifs à une commande
	 * Les ecran doivent obligatoirement implementer l'interface IDemande
	 * */
	public class DemandeFactory
	{
		/**
		 * Constante définissant le libellé  des nouvelles lignes dans la liste des type d'ecran disponible
		 * */
		public static const NOUVELLES_LIGNES : String = "Nouvelles Lignes";
		
		/**
		 * Constante définissant le libellé  des nouveaux produits dans la liste des type d'ecran disponible
		 * */
		public static const NOUVEAUX_PRODUITS : String = "Nouveaux Produits";
		
		
		/**
		 * Constante définissant le CODE IHM  des nouvelles lignes dans la liste des type d'ecran disponible
		 * */
		public static const CODE_NOUVELLES_LIGNES : int = 1;
		
		
		/**
		 * Constante définissant le CODE IHM  des nouveaux produits dans la liste des type d'ecran disponible 
		 * */
		public static const CODE_NOUVEAUX_PRODUITS : int = 2;							
		
		
		
		/**
		 * Retourne une instance de d'ecran (nouvelle ligne ou nouveaux produit) suivant les parametres
		 * @param code le CODE IHM de l'ecran à afficher (utiliser les constantes de cette classe)
		 * @param ido l'identifiant de la commande à afficher
		 * @return IDemande une instance d'ecran
		 * @throw DemandeError une erreur signifiant que le type d'écran demandé n'existe pas
		 * */
		public static function createDemande(code : int,ido : int = 0):IDemande{
			switch (code){
				case DemandeFactory.CODE_NOUVELLES_LIGNES :{
					if(ido==0){
						return new  PanelNouvellesLignes_maquette();
					}else{
						return new  ConsulterNouvelleLigne();
					}break;
				}  
				
				case DemandeFactory.CODE_NOUVEAUX_PRODUITS  :{
					if(ido == 0){
						return new PanelNouveauxProduits_maquette();
					}else{
						return new  ConsulterNouveauxProduits();
					}break ;
				} 
				default : throw new DemandeError("Ce type de demande est inconnu");break;
			}
		}	
		
		/**
		 * Tableau contenant la liste des ecrans disponibles et leur CODE_IHM
		 * */
		public static function getTypesDemandes():Array{
			var typesDemandes : Array = [{label:DemandeFactory.NOUVELLES_LIGNES,code:DemandeFactory.CODE_NOUVELLES_LIGNES},
											{label:DemandeFactory.NOUVEAUX_PRODUITS,code:DemandeFactory.CODE_NOUVEAUX_PRODUITS}];
			return typesDemandes;
		}
	}
}