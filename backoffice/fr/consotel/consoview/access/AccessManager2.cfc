<cfcomponent extends="fr.consotel.consoview.access.AccessManager" output="false">
	<cffunction name="validateLogin" access="remote" output="false" returntype="struct" hint="validation des identifiants du gestionnaire">
		<cfargument name="loginValue" required="true" type="string">
		<cfargument name="pwdValue" required="true" type="string">
		<cfargument name="codeApp" required="false" type="numeric" default="1">
		<cfargument name="locale" required="false" type="fr.consotel.consoview.access.CodeLangue">
		
		<cfset _locale = "">
		<cfif isdefined("locale")>
			<cfset _locale = locale>
			<cfelse>
			<cfset _locale = createObject("component","fr.consotel.consoview.access.CodeLangue")>		
			<cfset _locale.CODE = "default">	
			<cfset _locale.ID_LANGUE = "default">	
			<cfset _locale.DEVISE = "-">
			<cfset _locale.LANGUE = "n.c.">		
		</cfif>
		<cfcookie name="LAST_LOGIN" value="#loginValue#"  expires="never">
		<cflog text="CODE APP = #codeApp#"/>
		 
		<cfif structKeyExists(SESSION,"AUTH_STATE")>
			<cfif SESSION.AUTH_STATE EQ 1>
			   	<cflog type="Information" text="AUTH REQUEST WITH CONNECTED SESSION">
				<cfset clearSession()>
			</cfif>
		</cfif>
		
		<cfswitch expression="#codeApp#">
			<cfcase value="2">
				<cfset THIS.AUTH_DSN = "MBP_MERCURE">
				<cfset SESSION.OffreDSN = "MBP_MERCURE">
				<cfset SESSION.CODEAPPLICATION = 2 >
				<cfset COOKIE.CODEAPPLICATION = 2 >
				<cfset THIS.CONNECT_CLIENT = "offre.pkg_connection_appli.connectClient">
				
			</cfcase>
			
			<cfcase value="3"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 3>
				<cfset COOKIE.CODEAPPLICATION = 3>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfcase value="51"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 51>
				<cfset COOKIE.CODEAPPLICATION = 51>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfcase value="101"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 101>
				<cfset COOKIE.CODEAPPLICATION = 101>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfdefaultcase>
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 1>
				<cfset COOKIE.CODEAPPLICATION = 1>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfdefaultcase>
			
		</cfswitch>
		
		<!--- cfcase sur le code_appli pour redefinir le AUTH_DSN, et le package global --->
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.CONNECT_CLIENT#">
			<cfprocparam type="in" value="#loginValue#" cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="in" value="#pwdValue#" 	cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qValidateLogin">
		</cfstoredproc>
		
		<cfset userInfos = structNew()>		
		<cfset userInfos.AUTH_RESULT = 0>
		<cfset userInfos.OFFREDSN = SESSION.OffreDSN>
		
		<cfswitch expression="#p_retour#">
			<cfcase value="0">
				<cfset userInfos.AUTH_RESULT = 0>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="1"> 
				<cfset userInfos.AUTH_RESULT = 1>
			</cfcase>
			<cfcase value="2"> 
				<cfset userInfos.AUTH_RESULT = 2>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-1"> 
				<cfset userInfos.AUTH_RESULT = -1>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-10"> 
				<cfset userInfos.AUTH_RESULT = -10>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-11"> 
				<cfset userInfos.AUTH_RESULT = -11>
				<cfreturn userInfos>
			</cfcase>
			<cfdefaultcase>
				<cfset userInfos.AUTH_RESULT = -100>
				<cfreturn userInfos>
			</cfdefaultcase>
		</cfswitch>
		
		<cfif qValidateLogin.recordcount EQ 1>
			<cfset boolProcessLogin=0>
			<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
			<cfif boolCheckIP EQ 1>
				<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
				<cfif checkIpResult GT 0>
					<cfset boolProcessLogin=1>
				<cfelse>
					<cfset boolProcessLogin=0>
					<cfset userInfos.AUTH_RESULT = 2>
					<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
				</cfif>
			<cfelse>
				<cfset boolProcessLogin=2>
			</cfif>
			<cfif boolProcessLogin GT 0>		
				
				<cfset SESSION.AUTH_STATE = 1>
				<cfset SESSION.CDRDSN = "ROCCDR">
				<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW"><cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
				<cfset SESSION.USER = structNew()>
				<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
				<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
				<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
				<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
				<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
				<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
				<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
				<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
				<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
				<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>

				<cfset userInfos.AUTH_RESULT = 1>
				<cfset userInfos.NOM = SESSION.USER.NOM>
				<cfset userInfos.PRENOM = SESSION.USER.PRENOM>
				<cfset userInfos.EMAIL = SESSION.USER.EMAIL>
				
				<!---<cfif SESSION.CODEAPPLICATION eq 1>
					<cfif compareNoCase(locale.CODE,"default") eq 0>
						<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
						<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
					<cfelse>
						<cfset SESSION.USER.GLOBALIZATION =  _locale.CODE>
						<cfset SESSION.USER.IDGLOBALIZATION =  _locale.ID_LANGUE>
					</cfif>
					
				<cfelse>
					<cfset SESSION.USER.GLOBALIZATION =  "fr_FR"/>
					<cfset SESSION.USER.IDGLOBALIZATION =  "0"/>
				</cfif>--->
				
				<cfset setupGlobalization(qValidateLogin,locale)>				
				<cfset userInfos.TYPE_LOGIN = SESSION.USER.IDTYPEPROFIL>
				<cfset userInfos.CLIENTID = SESSION.USER.CLIENTID>
				<cfset userInfos.CODEAPPLICATION = SESSION.CODEAPPLICATION>
				<cfset userInfos.CLIENTACCESSID = SESSION.USER.CLIENTACCESSID>
				<cfset userInfos.GLOBALIZATION = SESSION.USER.GLOBALIZATION>
				
				<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#"  expires="never">
				
				<cfset initSessionXmlAccess()>
			<cfelse>
				<cfset SESSION.FAILED_AUTH = 1>
				<cfset SESSION.AUTH_STATE = 4>
			</cfif>
		<cfelse>
			<cfset SESSION.FAILED_AUTH = 1>
			<cfset SESSION.AUTH_STATE = 3>
		</cfif>	
	
		<cfreturn userInfos>
	</cffunction>
	<cffunction name="validateLoginAndLocale" access="remote" output="false" returntype="struct" hint="validation des identifiants du gestionnaire">
		<cfargument name="loginValue" required="true" type="string">
		<cfargument name="pwdValue" required="true" type="string">
		<cfargument name="codeApp" required="false" type="numeric" default="1">
		<cfargument name="locale" required="false" type="fr.consotel.consoview.access.CodeLangue">
		
		<cfset _locale = "">
		<cfif isdefined("locale")>
			<cfset _locale = locale>
			<cfelse>
			<cfset _locale = createObject("component","fr.consotel.consoview.access.CodeLangue")>		
			<cfset _locale.CODE = "default">	
			<cfset _locale.ID_LANGUE = "default">	
			<cfset _locale.DEVISE = "-">
			<cfset _locale.LANGUE = "n.c.">		
		</cfif>
		<cfcookie name="LAST_LOGIN" value="#loginValue#" domain="mytem360.com" expires="never">
		<cflog text="CODE APP = #codeApp#"/>
		 
		<cfif structKeyExists(SESSION,"AUTH_STATE")>
			<cfif SESSION.AUTH_STATE EQ 1>
			   	<cflog type="Information" text="AUTH REQUEST WITH CONNECTED SESSION">
				<cfset clearSession()>
			</cfif>
		</cfif>
		
		<cfswitch expression="#codeApp#">
			<cfcase value="2">
				<cfset THIS.AUTH_DSN = "MBP_MERCURE">
				<cfset SESSION.OffreDSN = "MBP_MERCURE">
				<cfset SESSION.CODEAPPLICATION = 2 >
				<cfset COOKIE.CODEAPPLICATION = 2 >
				<cfset THIS.CONNECT_CLIENT = "offre.pkg_connection_appli.connectClient">
				
			</cfcase>
			
			<cfcase value="3"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 3>
				<cfset COOKIE.CODEAPPLICATION = 3>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfcase value="51"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 51>
				<cfset COOKIE.CODEAPPLICATION = 51>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfcase value="101"> 
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 101>
				<cfset COOKIE.CODEAPPLICATION = 101>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfcase>
			
			<cfdefaultcase>
				<cfset THIS.AUTH_DSN = "ROCOFFRE">
				<cfset SESSION.OffreDSN = "ROCOFFRE">
				<cfset SESSION.CODEAPPLICATION = 1>
				<cfset COOKIE.CODEAPPLICATION = 1>
				<cfset THIS.CONNECT_CLIENT  = "#THIS.PKG_GLOBAL#.connectClient_v2">
			</cfdefaultcase>
			
		</cfswitch>
		
		<!--- cfcase sur le code_appli pour redefinir le AUTH_DSN, et le package global --->
		<cfstoredproc datasource="#THIS.AUTH_DSN#" procedure="#THIS.CONNECT_CLIENT#">
			<cfprocparam type="in" value="#loginValue#" cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="in" value="#pwdValue#" 	cfsqltype="CF_SQL_VARCHAR" >
			<cfprocparam type="out"	variable="p_retour" 	cfsqltype="CF_SQL_INTEGER">
			<cfprocresult name="qValidateLogin">
		</cfstoredproc>
		
		<cfset userInfos = structNew()>		
		<cfset userInfos.AUTH_RESULT = 0>
		<cfset userInfos.OFFREDSN = SESSION.OffreDSN>
		
		<cfswitch expression="#p_retour#">
			<cfcase value="0">
				<cfset userInfos.AUTH_RESULT = 0>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="1"> 
				<cfset userInfos.AUTH_RESULT = 1>
			</cfcase>
			<cfcase value="2"> 
				<cfset userInfos.AUTH_RESULT = 2>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-1"> 
				<cfset userInfos.AUTH_RESULT = -1>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-10"> 
				<cfset userInfos.AUTH_RESULT = -10>
				<cfreturn userInfos>
			</cfcase>
			<cfcase value="-11"> 
				<cfset userInfos.AUTH_RESULT = -11>
				<cfreturn userInfos>
			</cfcase>
			<cfdefaultcase>
				<cfset userInfos.AUTH_RESULT = -100>
				<cfreturn userInfos>
			</cfdefaultcase>
		</cfswitch>
		
		
		<cfif qValidateLogin.recordcount EQ 1>
			<cfset boolProcessLogin=0>
			<cfset boolCheckIP=qValidateLogin['RESTRICTION_IP'][1]>
			<cfif boolCheckIP EQ 1>
				<cfset checkIpResult=checkIpAddress(qValidateLogin['CLIENTACCESSID'][1],#CGI.REMOTE_ADDR#)>
				<cfif checkIpResult GT 0>
					<cfset boolProcessLogin=1>
				<cfelse>
					<cfset boolProcessLogin=0>
					<cfset userInfos.AUTH_RESULT = 2>
					<cflog type="Information" text="AUTH REQUEST WITH INVALID IP ADDR">
				</cfif>
			<cfelse>
				<cfset boolProcessLogin=2>
			</cfif>
			<cfif boolProcessLogin GT 0>		
				
				<cfset SESSION.AUTH_STATE = 1>
				<cfset SESSION.CDRDSN = "ROCCDR">
				<cfset SESSION.PARCVIEWDSN = "CLPARCVIEW"><cfset SESSION.MAILSERVER = "mail.consotel.fr:26">
				<cfset SESSION.USER = structNew()>
				<cfset SESSION.USER.NOM = qValidateLogin['NOM'][1]>
				<cfset SESSION.USER.PRENOM = qValidateLogin['PRENOM'][1]>
				<cfset SESSION.USER.EMAIL = qValidateLogin['EMAIL'][1]>
				<cfset SESSION.USER.CLIENTACCESSID = qValidateLogin['CLIENTACCESSID'][1]>
				<cfset SESSION.USER.CLIENTID =  qValidateLogin['ID'][1]>
				<cfset SESSION.USER.IDTYPEPROFIL =  qValidateLogin['TYPE_PROFIL'][1]>
				<cfset SESSION.USER.LIBELLETYPEPROFIL =  qValidateLogin['LIBELLE_TYPE_PROFIL'][1]>								
				<cfset SESSION.USER.IDREVENDEUR =  qValidateLogin['IDREVENDEUR'][1]>
				<cfset SESSION.USER.NOM_REVENDEUR =  qValidateLogin['NOM_REVENDEUR'][1]>
				<cfset SESSION.USER.LIBELLE =  qValidateLogin['LIBELLE'][1]>

				<cfset userInfos.AUTH_RESULT = 1>
				<cfset userInfos.NOM = SESSION.USER.NOM>
				<cfset userInfos.PRENOM = SESSION.USER.PRENOM>
				<cfset userInfos.EMAIL = SESSION.USER.EMAIL>
				
				<!---<cfif SESSION.CODEAPPLICATION eq 1>
					<cfif compareNoCase(locale.CODE,"default") eq 0>
						<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
						<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
					<cfelse>
						<cfset SESSION.USER.GLOBALIZATION =  _locale.CODE>
						<cfset SESSION.USER.IDGLOBALIZATION =  _locale.ID_LANGUE>
					</cfif>
					
				<cfelse>
					<cfset SESSION.USER.GLOBALIZATION =  "fr_FR"/>
					<cfset SESSION.USER.IDGLOBALIZATION =  "0"/>
				</cfif>--->
				
				<cfset setupGlobalization(qValidateLogin,locale)>				
				<cfset userInfos.TYPE_LOGIN = SESSION.USER.IDTYPEPROFIL>
				<cfset userInfos.CLIENTID = SESSION.USER.CLIENTID>
				<cfset userInfos.CODEAPPLICATION = SESSION.CODEAPPLICATION>
				<cfset userInfos.CLIENTACCESSID = SESSION.USER.CLIENTACCESSID>
				<cfset userInfos.GLOBALIZATION = SESSION.USER.GLOBALIZATION>
				
				<cfcookie name="CODE_PAYS" value="#SESSION.USER.GLOBALIZATION#" domain="mytem360.com" expires="never">
				
				<cfset initSessionXmlAccess()>
			<cfelse>
				<cfset SESSION.FAILED_AUTH = 1>
				<cfset SESSION.AUTH_STATE = 4>
			</cfif>
		<cfelse>
			<cfset SESSION.FAILED_AUTH = 1>
			<cfset SESSION.AUTH_STATE = 3>
		</cfif>	
	
		<cfreturn userInfos>
	</cffunction>
	<cffunction name="setupGlobalization" access="private" hint="Definit le globalization">
		<cfargument name="qValidateLogin" required="true" type="query">
		<cfargument name="locale" required="false" type="fr.consotel.consoview.access.CodeLangue">
		
		<cfif SESSION.CODEAPPLICATION eq 51>
			<cfset SESSION.USER.GLOBALIZATION =  "fr_FR"/>
			<cfset SESSION.USER.IDGLOBALIZATION = "3" />
		<cfelseif SESSION.CODEAPPLICATION eq 101>
			<cfif compareNoCase(locale.CODE,"default") eq 0>
				<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
				<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
			<cfelse>
				<cfset SESSION.USER.GLOBALIZATION =  _locale.CODE>
				<cfset SESSION.USER.IDGLOBALIZATION =  _locale.ID_LANGUE>
			</cfif>
		<cfelse>
			<cfset SESSION.USER.GLOBALIZATION =  qValidateLogin['GLOBALIZATION'][1]>
			<cfset SESSION.USER.IDGLOBALIZATION =  qValidateLogin['IDCODE_LANGUE'][1]>
		</cfif>
		<cflog text="SESSION.USER.GLOBALIZATION = #SESSION.USER.GLOBALIZATION# - SESSION.USER.IDGLOBALIZATION = #SESSION.USER.IDGLOBALIZATION#">
	</cffunction>
</cfcomponent>