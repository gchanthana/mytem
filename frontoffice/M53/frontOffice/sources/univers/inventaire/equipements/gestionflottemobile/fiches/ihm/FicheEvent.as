package univers.inventaire.equipements.gestionflottemobile.fiches.ihm
{
	import flash.events.Event;

	public class FicheEvent extends Event
	{
		public static const FICHE_UPDATED:String = "ficheUpdated";
		
		public function FicheEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new FicheEvent(type,bubbles,cancelable);
		}
	}
}