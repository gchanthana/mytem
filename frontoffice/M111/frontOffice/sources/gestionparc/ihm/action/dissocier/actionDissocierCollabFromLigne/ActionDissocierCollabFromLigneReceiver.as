package gestionparc.ihm.action.dissocier.actionDissocierCollabFromLigne
{
	import gestionparc.ihm.question.ligne.QuestionDissocierCollabFromLigne;

	public class ActionDissocierCollabFromLigneReceiver
	{
		private var qn:QuestionDissocierCollabFromLigne;

		public function ActionDissocierCollabFromLigneReceiver()
		{
		}

		public function action():void
		{
			displayIHM();
		}

		private function displayIHM():void
		{
			qn=new QuestionDissocierCollabFromLigne();
		}
	}
}
