package classe
{
	import eventPerso.ContainerEvent;
	
	import mx.containers.TitleWindow;
	import mx.events.CloseEvent;
	
	public class PopupWindows extends TitleWindow
	{
		private var _selectedDoc:Doc;
		
		public function PopupWindows()
		{
			super();			
			this.addEventListener(CloseEvent.CLOSE, _thisCloseHandler);			
			this.layout="vertical" 
			this.showCloseButton=true
		}
		public function validateWindow():void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.VALIDATE_EVENT));
		}
		public function removeWindow():void
		{
			dispatchEvent(new ContainerEvent(ContainerEvent.CLOSE_EVENT));
		}

		protected function _thisCloseHandler(event:CloseEvent):void
		{
			removeWindow();
		}

		public function set selectedDoc(value:Doc):void
		{
			_selectedDoc = value;
		}

		[Bindable] public function get selectedDoc():Doc
		{
			return _selectedDoc;
		}
	}
}