<cfcomponent output="false" extends="AccessObject">
	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des sociï¿½tï¿½s du groupe
		
		Params in idGroupeMaitre
		Params out Societe[]
	--->
	<cffunction name="getListeSocietes" access="remote" output="false" returntype="Societe[]">
		<cfset qListeSocietes = getListeSocietesAsQuery()>	
		<cfset tabSociete = arrayNew(1)>			
		<cfloop query="qListeSocietes">
			<cfset pSociete = createObject("component","Societe")>
			<cfscript>
			//Initialize the CFC with the properties values.
				pSociete.setGroupeId(qListeSocietes.IDGROUPE_CLIENT);
				pSociete.setId(qListeSocietes.IDREF_CLIENT);
				pSociete.setRaisonSociale(qListeSocietes.LIBELLE & " " & qListeSocietes.REF_CLIENT);			
			</cfscript>
			<cfset arrayAppend(tabSociete,pSociete)>
		</cfloop>
		<cfreturn tabSociete>
	</cffunction>

	<!---
		
		Auteur : samuel.divioka
		
		Date : 11/6/2007
		
		Description : Liste des sociï¿½tï¿½s du groupe
	--->
	<cffunction name="getListeSocietesAsQuery" access="remote" output="false" returntype="query">
		<!--- TODO: PKG_CV_FACTURATION.GET_GROUPESOCIETES ï¿½ transformer en PROC--->
		<!--- 
			PROCEDURE			
				PKG_CV_FACTURATION.GET_GROUPESOCIETES
			PARAM	
			in			
				p_idGroupe_maitre		INTEGER
			out				  	
				p_retour				QUERY				
		 --->
		 
		<cfquery name="qListeSocietes" datasource="#SESSION.OFFREDSN#">
			select * 
			from 
			       groupe_client_ref_client gcrc,
			       ref_client rc 
			       
			where 
			       gcrc.idgroupe_client = #this.getIdGroupeMaitre()#
			       and rc.idref_client = gcrc.idref_client

		</cfquery>
		
<!--- 		<cfstoredproc datasource="#SESSION.OFFREDSN#" procedure="PKG_CV_FACTURATION.GET_GROUPESOCIETES">
			<cfprocparam cfsqltype="CF_SQL_INTEGER"  	type="in" 	variable="p_idGroupe_maitre" 	value="#this.getIdGroupeMaitre()#"/>
			<cfprocresult name="qListeSocietes"/>        
		</cfstoredproc>
 --->
		<cfreturn qListeSocietes>
		
	</cffunction>
</cfcomponent>