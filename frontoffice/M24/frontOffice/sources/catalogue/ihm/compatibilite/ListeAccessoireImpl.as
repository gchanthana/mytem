package catalogue.ihm.compatibilite
{
	import catalogue.event.CatalogueEvent;
	import catalogue.service.GlobalCatalogueService;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.CheckBox;
	import mx.controls.DataGrid;
	import mx.controls.TextInput;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.managers.PopUpManager;
	
	[Bindable]
	public class ListeAccessoireImpl extends TitleWindow
	{
		public var globalService : GlobalCatalogueService = new GlobalCatalogueService();
		
		public var listeIDEquipement : String ;
		public var groupeIndex : int;
		public var idRevendeur : int;
		public var listeEquipement : ArrayCollection;
		public var txtFiltre : TextInput;
		
		//COMPONANT
		public var dg: DataGrid;
		public var cbAll : CheckBox;	
		//public var cmb : CheckBox;	
		
		public function ListeAccessoireImpl()
		{
			groupeIndex = CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX;
		}
		public function init(evt : FlexEvent):void
		{
			initData();
			dg.addEventListener(ListEvent.ITEM_CLICK,onItemChanged);
		}
		public function initData():void 
		{
			globalService.getAccessoire(listeIDEquipement,idRevendeur);
		}
		public function fermer():void
		{
			PopUpManager.removePopUp(this);
		}
		public function filtreHandler():void
		{
			globalService.listeAccessoire.filterFunction= filtreGrid;
			globalService.listeAccessoire.refresh();
		}
			
		private function filtreGrid(item:Object):Boolean
		{
			if (((item.libelleAccessoire.toString()).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		public function onItemChanged(evt : Event):void
		{
			var item : Object = (evt.target as DataGrid).selectedItem;
					
			if(item.ETAT != 1)
			{
				item.ETAT = 1;
				//(evt.target as CheckBox).selected = true;
			}
			else
			{
				item.ETAT = 0;
			}
			((evt.target as DataGrid).dataProvider as ArrayCollection).itemUpdated(item);
			var boolExiste : Boolean = false;
			for(var i:int=0;i<globalService.listeAccessoireSelected.length;i++)
			{
				if(globalService.listeAccessoireSelected[i]==item.idAccessoire)
				{
					globalService.listeAccessoireSelected.removeItemAt(i);
					globalService.updateCompatibiliteAcc(listeIDEquipement,Number([item.idAccessoire]),0);
					boolExiste = true;
				}
			}
			if(!boolExiste) //Si l'item n'est pas déja dans la liste
			{
				globalService.listeAccessoireSelected.addItem(item.idAccessoire);
				globalService.updateCompatibiliteAcc(listeIDEquipement,Number([item.idAccessoire]),1);
			}
			globalService.listeAccessoireSelected.refresh();
		}
		private function update_type_commande_handler(evt : CatalogueEvent):void
		{
			//dispatchEvent(new GestionTypeCommandeEvent(GestionTypeCommandeEvent.TYPE_COMMANDE_CHANGE,true));	
		}
		public function add():void
		{
			
		}
		
	}
}