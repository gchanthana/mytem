package gestioncommande.services.article
{
	import composants.util.ConsoviewAlert;
	
	import flash.events.EventDispatcher;
	
	import gestioncommande.entity.ArticleItemVO;
	import gestioncommande.events.ArticleEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.events.ResultEvent;

	public class ArticleDatas extends EventDispatcher
	{
		private var _listeArticles:ArrayCollection = new ArrayCollection();
		
		public function ArticleDatas()
		{
			
		}
		
		public function treatListeEquipements(retour:ArrayCollection):void
		{
			var len			:int = retour.length;
			listeArticles.source = new Array();
			
			for(var i:int = 0; i<len; i++)
			{
				var eqpSpie:ArticleItemVO = new ArticleItemVO();
				eqpSpie.fill(retour[i]);
				listeArticles.addItem(eqpSpie);
			}
			dispatchEvent(new ArticleEvent(ArticleEvent.LISTED_ARTICLES));
		}
		
		public function treatEnregistrementArticles(retour:ArrayCollection):void
		{
			Alert.show("Retour apres enregistrement de la commande SPIE");
		}
		
		[Bindable]
		public function get listeArticles():ArrayCollection { return _listeArticles; }
		
		public function set listeArticles(value:ArrayCollection):void
		{
			if (_listeArticles == value)
				return;
			_listeArticles = value;
		}
	}
}