package univers.parametres.parametrecompte.ihm
{
	import composants.controls.TextInputLabeled;
	import composants.util.ConsoviewAlert;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DataGrid;
	import mx.core.IFlexDisplayObject;
	
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	
	import univers.parametres.parametrecompte.event.ContrainteSuppressionEvent;
	import univers.parametres.parametrecompte.event.GestionPoolEvent;
	import univers.parametres.parametrecompte.service.GestionPoolService;
	import univers.parametres.parametrecompte.vo.PoolGestionnaire;
	import univers.parametres.poolgestionnaire.ihm.PopGestionnairePoolIHM;
	import univers.parametres.poolgestionnaire.ihm.PopTypeCommandeIHM;
	
	[Bindable]
	public class ListePool extends VBox
	{
		public var gestionPoolService : GestionPoolService = new GestionPoolService();
		
		
		//COMPONANT
		public var dg_pool_gestionnaire: DataGrid;
		public var txtFiltre:TextInputLabeled;
		private var popContrainteSuppressionIHM : ContrainteSuppressionIHM;
		private var popUpdatePoolIHM : ParametrePoolIHM;
		
		public function ListePool()
		{
			
		}
		public function init(evt : FlexEvent):void
		{
			gestionPoolService.listePool(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		public function modifier(pool : PoolGestionnaire):void
		{
			popUpdatePoolIHM = null;
			popUpdatePoolIHM = new ParametrePoolIHM();
			popUpdatePoolIHM.poolGestionnaire = pool;
			addPop();
		}
		public function addPool(event:MouseEvent):void
		{
			popUpdatePoolIHM = new ParametrePoolIHM();
			addPop();
			
		}
		private function parametrePoolCOmpleteHandler(evt : GestionPoolEvent):void
		{
			//PopUpManager.removePopUp(popUpdatePoolIHM);
			//gestionPoolService.listePool(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
			initData();
		}
		public function supprimer(data : PoolGestionnaire):void
		{
			gestionPoolService.addEventListener(GestionPoolEvent.LISTE_CONTRAINTE_COMPLETE,listeContrainteOfPoolHandler);
			gestionPoolService.listeContrainteOfPool((dg_pool_gestionnaire.selectedItem as PoolGestionnaire).IDPool);
		}

	
		private function listeContrainteOfPoolHandler(evt : GestionPoolEvent):void
		{
			if(gestionPoolService.col_contrainte_of_pool.length>0) // il existe des contraintes qui empêche la suppression
			{
				popContrainteSuppressionIHM = new ContrainteSuppressionIHM();
				popContrainteSuppressionIHM.addEventListener(ContrainteSuppressionEvent.VALIDATE_SUPPRESSION_POOL,supprimerPoolSelected);
				popContrainteSuppressionIHM.listeContraintes = gestionPoolService.col_contrainte_of_pool;
				
				//Test singulier ou pluriel
				if(gestionPoolService.col_contrainte_of_pool.length>1)
				{
					popContrainteSuppressionIHM.messageDecription = ResourceManager.getInstance().getString('M21', 'Des__l_ments_sont_rattach_s_au_pool_')+(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).libelle_pool+ResourceManager.getInstance().getString('M21', '__La_suppression_de_ce_pool_entrainera_l');
				}
				else
				{
					popContrainteSuppressionIHM.messageDecription = ResourceManager.getInstance().getString('M21', 'Un__l_ment_est_rattach__au_pool_')+(dg_pool_gestionnaire.selectedItem as PoolGestionnaire).libelle_pool+ResourceManager.getInstance().getString('M21', '__La_suppression_de_ce_pool_entrainera_l');
				}
				popContrainteSuppressionIHM.messageConfirmation = ResourceManager.getInstance().getString('M21', '_tes_vous_s_r_de_vouloir_supprimer_le_po');
				PopUpManager.addPopUp(popContrainteSuppressionIHM,parentDocument as DisplayObject,true);
				PopUpManager.centerPopUp(popContrainteSuppressionIHM);
			}
			else // pas de contrainte on peut supprimer
			{
				supprimerPoolSelected();
			}
		}
		private function supprimerPoolSelected(evt : ContrainteSuppressionEvent=null):void
		{
			//TODO REQUETE DE SUPPRESSION
			gestionPoolService.addEventListener(GestionPoolEvent.REMOVE_POOL_COMPLETE,remove_complete_handler);
			gestionPoolService.remove_pool_gestionnaire((dg_pool_gestionnaire.selectedItem as PoolGestionnaire).IDPool);
		}
		private function remove_complete_handler(evt : GestionPoolEvent):void
		{
			ConsoviewAlert.afficherOKImage(ResourceManager.getInstance().getString('M21', 'le_pool_')+dg_pool_gestionnaire.selectedItem.libelle_pool+ResourceManager.getInstance().getString('M21', '_a_bien__t__supprim__'));
			initData();
		}
		public function initData():void
		{
			gestionPoolService.listePool(CvAccessManager.getSession().CURRENT_PERIMETRE.GROUPE_INDEX);
		}
		
		private function addPop():void
		{
			popUpdatePoolIHM.addEventListener(GestionPoolEvent.PARAMETRE_POOL_COMPLETE,parametrePoolCOmpleteHandler);
			PopUpManager.addPopUp(popUpdatePoolIHM,parentDocument as DisplayObject,true);
			PopUpManager.centerPopUp(popUpdatePoolIHM);
		}
		
		
		//protected
		public function linkNbTypeCommandeHandler(pool:PoolGestionnaire):void
		{	
			var popListeTypecommande:PopTypeCommandeIHM = new PopTypeCommandeIHM();
			popListeTypecommande.pool = pool;			
			PopUpManager.addPopUp(popListeTypecommande,parentDocument as DisplayObject,true);
			PopUpManager.centerPopUp(popListeTypecommande);
		}
		
		//protected
		public function linkNbnbGestionnaireHandler(pool:PoolGestionnaire):void
		{	
			var popListeGestionnaire:PopGestionnairePoolIHM = new PopGestionnairePoolIHM();
			popListeGestionnaire.pool = pool;			
			PopUpManager.addPopUp(popListeGestionnaire,parentDocument as DisplayObject,true);
			PopUpManager.centerPopUp(popListeGestionnaire);
		}
		
		
		protected function imgActualiserHandler(event:MouseEvent):void
		{
			init(null);
		}
		
		protected function filtreHandler(e:Event = null):void
		{
			if(gestionPoolService.col_pool_gestionnaire != null)
			{
				gestionPoolService.col_pool_gestionnaire.filterFunction= filtreGrid;
				gestionPoolService.col_pool_gestionnaire.refresh();
			}
		}
		
		private function filtreGrid(item:Object):Boolean{
			if(item != null)
			{
				if ((item.libelle_pool && ((item.libelle_pool as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 )||
					(item.codeInterne_pool && ((item.codeInterne_pool as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1 )||
						(item.libelle_revendeur &&((item.libelle_revendeur as String).toLowerCase()).search(txtFiltre.text.toLowerCase())!=-1))					
						
						{
							return true;
						}
						else
						{
							return false;	
						}
			}
			else
			{
				return false;
			}
		}
		
	}
}