package univers.inventaire.inventaire.creation.nouvelleResources.commande.recherche.detail.objets
{
	import mx.rpc.events.ResultEvent;
	import flash.events.EventDispatcher;
	import flash.events.Event;
	import mx.controls.Alert;
	import mx.rpc.AbstractOperation;
	import fr.consotel.consoview.util.remoting.coldfusion.RemoteObjectUtil;
	import mx.collections.ArrayCollection;
	import mx.utils.ArrayUtil;
	import composants.util.ConsoviewUtil;
	 
	
	[Bindable]
	public class Societe extends EventDispatcher
	{	
		public static const CREATION_COMPLETE : String = "societeOk";
		public static const UPDATE_COMPLETE : String = "societeUpdated";
		public static const SAVE_COMPLETE : String = "societeSaved";
		public static const LISTE_COMPLETE : String = "societeListOk";
		public static const DELETE_COMPLETE : String = "societeDeleted";
		public static const LISTE_OPERATEUR_COMPLETE : String = "societeListOperateurOk";
		public static const LISTE_OPERATEUR_SAVED : String = "societeListOperateurSaved";
		public static const LISTE_AGENCE_COMPLETE : String = "societeListeAgenceOK";
					
		public static const CREATION_ERROR : String = "societeCreationError";
		public static const UPDATE_ERROR : String ="societeUpdateError";  
		public static const SAVE_ERROR : String = "societeSavedError";
		public static const LISTE_ERROR : String = "societeListError";
		public static const DELETE_ERROR : String = "societeDeleteError";
	  	public static const LISTE_OPERATEUR_ERROR : String = "societeListOperateurError";
	  	public static const LISTE_OPERATEUR_SAVEDERROR : String = "societeListOperateurSavedError";
	  	public static const LISTE_AGENCE_ERROR : String = "societeListeAgenceError";
	  	
	  	
		private static const COLDFUSION_COMPONENT : String = "fr.consotel.consoview.inventaire.cycledevie.nouveauxproduits.Societe";
  		private static const COLDFUSION_GET : String = "get";
  		private static const COLDFUSION_UPDATE : String = "update";
  		private static const COLDFUSION_CREATE : String = "create";
		private static const COLDFUSION_GETLISTE : String = "getList";
		private static const COLDFUSION_GETCONTACTLISTE : String = "getContactList";
		private static const COLDFUSION_DELETE : String = "delete";
		private static const COLDFUSION_GETOPELISTE : String = "getOperateurlistSociete";
		private static const COLDFUSION_CREATEOPELISTE : String = "saveListOperateurSociete";
		private static const COLDFUSION_GETLISTEAGENCE : String = "getAgences";
		
		private var opGet : AbstractOperation;
		private var opCreate : AbstractOperation;
		private var opUpdate : AbstractOperation;		
		private var opList : AbstractOperation;
		private var opDelete : AbstractOperation;
		
		public function Societe(societeID : Number = 0){
			if (societeID != 0){
				_societeID = societeID;
				getSociete(_societeID);	
				_contacts = new Contact();					
			}			
		}
		
		public function clean():void{						
			 
		}
		
		private var _societeID : Number;
		public function get societeID():Number{
			return _societeID;
		}
		public function set societeID(id : Number):void{
			_societeID = id;
		}
		
		private var _contacts : Contact;		
		public function get contacts():Contact{			
			return _contacts;
		}
						
		private var _raisonSociale : String;
		public function set raisonSociale( r : String):void{
			_raisonSociale = r;
		}
		public function get raisonSociale():String{
			return _raisonSociale; 
		}
		
		private var _societeParenteID : Number;
		public function set societeParenteID( sp : Number):void{
			_societeParenteID = sp;
		}
		public function get societeParenteID():Number{
			return _societeParenteID; 
		}
		
		private var _operateurIDs : Array;
		public function set operateurIDs( opids : Array):void{
			_operateurIDs = opids;
		}
		public function get operateurIDs():Array{
			return _operateurIDs; 
		}
		
		private var _operateurNom : String;
		public function set operateurNom(n : String):void{
			_operateurNom = n;
		}
		public function get operateurNom():String{
			return _operateurNom;
		}
		
		private var _siretSiren : String;
		public function set siretSiren( s : String):void{
			_siretSiren = s;
		}
		public function get siretSiren():String{
			return _siretSiren; 
		}
		
		private var _adresse1 : String;
		public function set adresse1( adr1 : String):void{
			_adresse1 = adr1;
		}
		public function get adresse1():String{
			return _adresse1; 
		}
		
		private var _adresse2: String;
		public function set adresse2( adr2 : String ):void{
			_adresse2 = adr2;
		}		
		public function get adresse2():String{
			return _adresse2;
		}
		
		private var _codePsotal : String;
		public function set codePsotal( cp : String):void{
			_codePsotal = cp;
		}
		public function get codePsotal():String{
			return _codePsotal; 
		}
		
		private var _commune : String;
		public function set commune( c : String):void{
			_commune = c;
		}
		public function get commune():String{
			return _commune; 
		}
		
		private var _pays : String;
		public function set pays( p : String):void{
			_pays = p;
		}
		public function get pays():String{
			return _pays; 
		}
		
		private var _paysId : int;
		public function set paysId( idp : int ):void{
			_paysId = idp;
		}
		public function get paysId():int{
			return _paysId; 
		}
		
		private var _telephone : String;
		public function set telephone(t : String):void{
			_telephone = t;
		}
		public function get telephone():String{
			return _telephone;
		}	
		
		private var _fax: String;
		public function set fax(f : String):void{
			_fax = f;
		}
		public function get fax():String{
			return _fax;
		}
		
		private var _groupeClientID : int;
		public function set groupeClientID (id : int):void{
			_groupeClientID = id;
		}
		public function get groupeClientID ():int{		
			return _groupeClientID;
		}
		
		private var _listeOperateurStr : String = " ";
		public function get listeOperateurStr():String{
			return _listeOperateurStr;
		}
		public function set listeOperateurStr(s : String):void{
			_listeOperateurStr = s;
		}
				 
		//---------------------------------------------------------/		
		private function fill(value : Object):void{			
			adresse1 = value.ADRESSE1;
			adresse2 = value.ADRESSE2;
			codePsotal = value.ZIPCODE;
			commune = value.COMMUNE;			
			pays = value.PAYS;
			raisonSociale = value.RAISON_SOCIALE;
			siretSiren = value.SIREN;
			societeParenteID = value.IDSOCIETE_MERE;
			telephone = value.TELEPHONE;
			operateurNom = value.OPENOM;
			groupeClientID = value.IDGROUPE_CLIENT;
			listeOperateurStr = value.LISTE_OPE;
			fax = value.FAX;
		}
		
		///---------------- REMOTING ------------------------------/
		private function getSociete(id : int):void{
			opGet = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_GET,
													getSocieteResultHandler,null);

			RemoteObjectUtil.callService(opGet,id);

		}
		
		private function getSocieteResultHandler(re : ResultEvent):void{
			if (re.result){							
				fill(re.result[0]);				
				dispatchEvent(new Event(Societe.CREATION_COMPLETE));				
			}else{
				dispatchEvent(new Event(Societe.CREATION_ERROR));
			}			
		}		
		//--------
		
		public function deleteSociete(id : Number):void{
			opDelete = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_DELETE,
													deleteSocieteResultHandler,null);

			RemoteObjectUtil.callService(opDelete,id);

		}
		
		private function deleteSocieteResultHandler(re : ResultEvent):void{
			if (re.result > 0){							
				dispatchEvent(new Event(Societe.DELETE_COMPLETE));				
			}else{
				dispatchEvent(new Event(Societe.DELETE_ERROR));
			}			
		}
		
		//--------			
		public function updateSociete():void{
			opUpdate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_UPDATE,
													updateSocieteResultHandler,null);
			var fake : int = 0;
			RemoteObjectUtil.callService(opUpdate,fake,this);						
		}	
		
		private function updateSocieteResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				dispatchEvent(new Event(Societe.UPDATE_COMPLETE));
			}else{
				dispatchEvent(new Event(Societe.UPDATE_ERROR));
			}
		}
		
		//----------
		public function sauvegarder(idGroupe : Number):void{			
			opCreate= RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_CREATE,
													sauvegarderResultHandler,null);

			RemoteObjectUtil.callService(opCreate,idGroupe,this);
					 											
		}		
		
		private function sauvegarderResultHandler(re : ResultEvent):void{
			if (re.result > 0){
				societeID = Number(re.result);				
				dispatchEvent(new Event(Societe.SAVE_COMPLETE));
			}else{
				dispatchEvent(new Event(Societe.SAVE_ERROR));
			}			
		}		
//---------LISTE---------------------------------------------------------------------------
		
		private function setliste(values : ArrayCollection):ArrayCollection{
			var ligne : Object;		 
			var newliste : ArrayCollection = new ArrayCollection();
			
			for (ligne in values){
				
				var st : Societe = new Societe();
				//st.addEventListener(Societe.LISTE_OPERATEUR_COMPLETE,fillOperateursComplete);
				st.societeID = values[ligne].IDCDE_CONTACT_SOCIETE; 
				st.societeParenteID = values[ligne].IDSOCIETE_MERE;
				st.raisonSociale = values[ligne].RAISON_SOCIALE;			
				st.siretSiren = values[ligne].SIREN;				
				st.telephone = values[ligne].TELEPHONE;		
				st.fax = values[ligne].FAX;						
				st.operateurNom = values[ligne].OPENOM;
				st.adresse1 = values[ligne].ADRESSE1;
				st.adresse2 = values[ligne].ADRESSE2;
				st.codePsotal = values[ligne].ZIPCODE;
				st.commune = values[ligne].COMMUNE;
				st.pays = values[ligne].PAYS;	
				st.groupeClientID = values[ligne].IDGROUPE_CLIENT;
				st.listeOperateurStr = values[ligne].LISTE_OPE;
				//st.preparelistOperateurSociete(st.societeID);															
				newliste.addItem(st);										
			}	
			
			var blancObject : Societe = new Societe();
			blancObject.adresse1 = "";			
			blancObject.societeID = 0;
			blancObject.societeParenteID = 0;
			blancObject.raisonSociale = "-";
			newliste.addItemAt(blancObject,0);			
			return newliste;
		}	
			
		private function fillOperateursComplete(ev : Event):void{
			//dispatchEvent(new Event(Societe.LISTE_COMPLETE));									
		}
		
		private var _liste : ArrayCollection;	
		public function get liste():ArrayCollection{
			return _liste;
		} 
		
		public function prepareList(idGpe : Number):void{
			 		
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_GETLISTE,
													getListResultHandler,null);													
			RemoteObjectUtil.callService(opList,idGpe);
		}
		
		private function getListResultHandler(re : ResultEvent):void{
			try{
				_liste = setliste(re.result as ArrayCollection);	
				dispatchEvent(new Event(Societe.LISTE_COMPLETE));			
			}catch(e : Error){
				dispatchEvent(new Event(Societe.LISTE_ERROR));
			}			
		}
		//------------------		
		private var _listeAgences : ArrayCollection
		public function get listeAgences():ArrayCollection{
			return _listeAgences;
		}		
		public function getAgences():void{
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_GETLISTEAGENCE,
													getAgencesResultHandler,null);

			RemoteObjectUtil.callService(opList,societeID);

		}
		
		private function getAgencesResultHandler(re : ResultEvent):void{
			if (re.result){							
				_listeAgences = setliste(re.result as ArrayCollection);				
				dispatchEvent(new Event(Societe.LISTE_AGENCE_COMPLETE));				
			}else{
				dispatchEvent(new Event(Societe.LISTE_AGENCE_ERROR));
			}			
		}
				
		//-----------------------
		private var _listeOperateurs : ArrayCollection;	
				
		public function get listeOperateurs():ArrayCollection{
			return _listeOperateurs;
		}		
				
		public function  preparelistOperateurSociete(socId : Number):void{			
			 	
			opList = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_GETOPELISTE,
													getOperateurListResultHandler,null);													
			RemoteObjectUtil.callService(opList,socId);
		}
	
		private function getOperateurListResultHandler(re : ResultEvent):void{
			try{
				_listeOperateurs = re.result as ArrayCollection;
				
				_listeOperateurStr = ConsoviewUtil.arrayToList(_listeOperateurs,"NOM",",");
				
				dispatchEvent(new Event(Societe.LISTE_OPERATEUR_COMPLETE));
			}catch(e : Error){
				dispatchEvent(new Event(Societe.LISTE_OPERATEUR_ERROR));
			}			
		}
		
		//-----------------------		
		public function saveOperateur(idsociete : int, list : Array):void{
			opCreate = RemoteObjectUtil.getHandledOperationFrom(RemoteObjectUtil.DEFAULT_DESTINATION,
													Societe.COLDFUSION_COMPONENT,
													Societe.COLDFUSION_CREATEOPELISTE,
													saveOperateurResultHandler,null);													
			RemoteObjectUtil.callService(opCreate,idsociete,list);
		}
		
		private function saveOperateurResultHandler(re : ResultEvent):void{
			try{
				if (re.result > 0){
					dispatchEvent(new Event(Societe.LISTE_OPERATEUR_SAVED));	
				}else{
					dispatchEvent(new Event(Societe.LISTE_OPERATEUR_SAVEDERROR));
				}								
			}catch(e : Error){
				dispatchEvent(new Event(Societe.LISTE_OPERATEUR_SAVEDERROR));
			}			
		}
		
		
	}
}