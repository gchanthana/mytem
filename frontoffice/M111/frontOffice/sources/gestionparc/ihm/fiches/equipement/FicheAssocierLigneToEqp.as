package gestionparc.ihm.fiches.equipement
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import gestionparc.entity.SpecificationVO;
	import gestionparc.event.gestionparcEvent;
	import gestionparc.ihm.fiches.FicheAffectationIHM;
	import gestionparc.ihm.fiches.listedispo.DispoLigne;
	import gestionparc.services.associer.ActionAssocierServices;
	import gestionparc.utils.gestionparcConstantes;
	
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;

	public class FicheAssocierLigneToEqp extends FicheAffectationIHM
	{
		[Bindable]
		public var liste:DispoLigne;

		private var serv:ActionAssocierServices=new ActionAssocierServices();

		public function FicheAssocierLigneToEqp()
		{
			super();
			liste=new DispoLigne()
			addEventListener(FlexEvent.CREATION_COMPLETE, init);
		}

		private function init(ev:FlexEvent):void
		{
			initListeners();
			initIHM();
		}

		private function initListeners():void
		{
			serv.myDatas.addEventListener(gestionparcEvent.ASSOCIATION_REALIZED, fermer);
			liste.addEventListener(MouseEvent.CLICK, dgListeDispo_handler);
			liste.addEventListener(gestionparcEvent.REFRESH_LIBELLESELECTION, dgListeDispo_handler);
			btAnnuler.addEventListener(MouseEvent.CLICK, fermer);
			btValider.addEventListener(MouseEvent.CLICK, valider);
			this.addEventListener(CloseEvent.CLOSE, fermer);
		}

		private function initIHM():void
		{
			this.title=ResourceManager.getInstance().getString("M111", "ASSOCIER_LIGNETOEQP");

			this.img_cellule.source=gestionparcConstantes.adrImgTerm;
			this.lab_cellule_name.text=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI

			this.img_liste.source=gestionparcConstantes.adrImgSIM;
			this.img_liste_big.source=gestionparcConstantes.adrImgSIM;
			this.lab_titre.text=liste.getLibelleType()

			this.box_listeDispo.addChild(liste as VBox);
		}

		private function dgListeDispo_handler(evt:Event):void
		{
			labDgSelection.text=liste.getLibelleSelection();
		}

		private function valider(e:Event):void
		{
			if (liste.getIDSelection() > -1)
				associerLigneToEqp();
			else
				Alert.show(ResourceManager.getInstance().getString('M111', 'S_lectionnez_un__l_ment_dans_la_liste'));
		}

		protected function listeClickHandler(e:MouseEvent):void
		{
			if (liste.getIDSelection() > -1)
			{
				labDgSelection.text=liste.getLibelleSelection();
			}
		}

		protected function fermer(evt:Event):void
		{
			PopUpManager.removePopUp(this);
		}

		/* APPEL BACKOFFICE */
		private function associerLigneToEqp():void
		{
			serv.associerLigneToEqp1(setObj());
		}

		private function setObj():Object
		{
			var obj:Object=new Object();
			obj.idorigine=SpecificationVO.getInstance().elementDataGridSelected.ID;
			obj.iddestination=liste.getSelection().ID
			obj.ITEM1=SpecificationVO.getInstance().elementDataGridSelected.T_IMEI;
			obj.ITEM2=liste.getSelection().COLLABORATEUR;
			obj.ID_TERMINAL=SpecificationVO.getInstance().elementDataGridSelected.IDTERMINAL;
			obj.ID_SIM=0;
			obj.ID_EMPLOYE=0;
			obj.IDSOUS_TETE=liste.getSelection().IDSOUS_TETE;
			obj.DATE_EFFET=DateField.dateToString((calendar.selectedDate != null) ? calendar.selectedDate : new Date(), 'YYYY/MM/DD');
			obj.IS_PRETER=check_pret.selected;
			return obj;
		}
	}
}
