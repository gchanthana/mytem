<cfcomponent displayname="Gestion des Imports" output="false">

	<cffunction name="getListeCF" access="remote" output="false" returntype="Query">
		<cfargument name="idgroupe" type="numeric" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			select o.nom, o.operateurid, rco.idref_client, rco.numero_correspond as CF
			from ref_client_operateur rco, operateur o, groupe_client_ref_client gc
			where gc.idgroupe_client=#idgroupe#
			and rco.operateurid=o.operateurid
			and gc.idref_client=rco.idref_client
			order by lower(o.nom), lower(rco.numero_correspond)
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getListeRefClient" access="remote" output="false" returntype="Query">
		<cfargument name="idgroupe" type="numeric" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			select rc.libelle, rc.idref_client, rc.ref_client
			from ref_client rc, groupe_client_ref_client gcrc
			where gcrc.idgroupe_client=#idgroupe#
			and gcrc.idref_client=rc.idref_client
		</cfquery>
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getListeOperateur" access="remote" returntype="Query" output="false" >
		<cfquery name="getListe" datasource="#SESSION.OffreDSN#">
			select nom , operateurID
			from operateur
			order by lower(nom)
		</cfquery>
		<cfquery name="qGetListe" dbtype="query">
			select nom as label, operateurID as data
			from getListe
		</cfquery>
		<cfreturn qGetListe>
	</cffunction>
	
	<cffunction name="addCF" access="remote" output="false" returntype="void">
		<cfargument name="idrefclient" type="string" default="0">
		<cfargument name="operateurID" type="string" default="0">
		<cfargument name="CF" type="string" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			insert into ref_client_operateur(operateurid,idref_client,numero_correspond)
			VALUES(#operateurID#,#idrefclient#,'#CF#')
		</cfquery>
	</cffunction>
	
	<cffunction name="deleteCF" access="remote" output="false" returntype="void">
		<cfargument name="operateurID" type="string" default="">
		<cfargument name="idrefclient" type="string" default="">
		<cfargument name="CF" type="string" default="0">
		<cfquery name="result" datasource="#SESSION.OffreDSN#">
			delete from ref_client_operateur
			where operateurid=#operateurID#
			and idref_client=#idrefclient#
			and numero_correspond='#CF#'
		</cfquery>
	</cffunction>
</cfcomponent>