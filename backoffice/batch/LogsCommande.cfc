<cfcomponent output="false">
	<!---
		Insertion du log d'une commande
	 --->
	<cffunction name="insertlogCommande" returntype="numeric">
		
		<cfargument name="idRacine" 		type="numeric"	 						required="true">
		<cfargument name="libelleRacine" 	type="string"	 						required="true">
		<cfargument name="idGestionnaire" 	type="numeric"	 						required="true">
		<cfargument name="gestionnaire" 	type="string"	 						required="true">
		<cfargument name="mailGestionnaire"	type="string"	 						required="true">
		<cfargument name="mail" 			type="batchcommande.Mail" 	required="true">
		<cfargument name="idCommande" 		type="numeric" 							required="true">
		<cfargument name="message"	 		type="string" 							required="true">
		<cfargument name="jobId" 			type="numeric"							required="true">
		<cfargument name="status" 			type="string" 							required="true" default="n.c.">
		<cfargument name="theUuid" 			type="string" 							required="true" default="n.c.">
		
		<cfset cc = mail.getCc()>
				
		<cfif mail.getCopiePourExpediteur() eq "YES">
			<cfif len(mail.getCc()) eq 0>
				<cfset cc = mail.getExpediteur()>
			</cfif>
		</cfif>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.insert_log_commande">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDCOMMANDE" type="in"   value="#idCommande#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDGESTIONNAIRE" type="in"   value="#idGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_RACINE" type="in"   value="#libelleRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_GESTIONNAIRE" type="in"   value="#gestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MAIL" type="in"   value="#mailGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_FROM" type="in"   value="#mail.getExpediteur()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_TO" type="in"   value="#mail.getDestinataire()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_CC" type="in"   value="#cc#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_BCC" type="in"   value="#mail.getBcc()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MODULE" type="in"   value="#mail.getModule()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_SUBJECT" type="in"   value="#mail.getModule()#-#mail.getSujet()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MESSAGE" type="in"   value="#message#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_JOB_ID" type="in"   value="#jobId#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_STATUS" type="in"   value="#status#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" type="in"   value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_UUID" type="in"   value="#theUuid#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_RESULT" type="out"  />
		</cfstoredproc>		
		<cfreturn P_RESULT>
	</cffunction>
	
	<cffunction name="insertlogBDCMobile" returntype="numeric">
		
		<cfargument name="idRacine" 		type="numeric"	 						required="true">
		<cfargument name="libelleRacine" 	type="string"	 						required="true">
		<cfargument name="idGestionnaire" 	type="numeric"	 						required="true">
		<cfargument name="gestionnaire" 	type="string"	 						required="true">
		<cfargument name="mailGestionnaire"	type="string"	 						required="true">
		<cfargument name="mail" 			type="batchcommande.Mail" 	required="true">
		<cfargument name="idCommande" 		type="numeric" 							required="true">
		<cfargument name="message"	 		type="string" 							required="true">
		<cfargument name="bdcUuid"	 		type="string" 							required="true">
		<cfargument name="jobId" 			type="numeric"							required="true">
		<cfargument name="status" 			type="string" 							required="true" default="n.c.">
		
		<cfset cc = mail.getCc()>
				
		<cfif mail.getCopiePourExpediteur() eq "YES">
			<cfif len(mail.getCc()) eq 0>
				<cfset cc = mail.getExpediteur()>
			</cfif>
		</cfif>
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.insert_log_commande">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDCOMMANDE" type="in"   value="#idCommande#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDGESTIONNAIRE" type="in"   value="#idGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_RACINE" type="in"   value="#libelleRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_GESTIONNAIRE" type="in"   value="#gestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MAIL" type="in"   value="#mailGestionnaire#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_FROM" type="in"   value="#mail.getExpediteur()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_TO" type="in"   value="#mail.getDestinataire()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_CC" type="in"   value="#cc#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_BCC" type="in"   value="#mail.getBcc()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MODULE" type="in"   value="#mail.getModule()#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_SUBJECT" type="in"   value="#mail.getModule()#-#mail.getSujet()#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_MESSAGE" type="in"   value="#message#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_JOB_ID" type="in"   value="#jobId#"/>			
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_STATUS" type="in"   value="#status#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_IDRACINE" type="in"   value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="P_UUID" type="in"   value="#bdcUuid#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="P_RESULT" type="out"  />
		</cfstoredproc>		
		<cfreturn P_RESULT>
	</cffunction>
	
	<!--- 
		Affichage des logs des commandes pour une racine ou pour toutes les racines si idracine = 0
		entre deux dates
	 --->
	<cffunction name="updateJobIDBDCMobile" returntype="numeric">
		<cfargument name="theUuid" type="string" 	required="true">
		<cfargument name="jobId" type="numeric" 		required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.updateJobId">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_UUID" type="in"   value="#theUuid#"/>
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_jobId" type="in"   value="#jobId#"/>
			 <cfprocparam cfsqltype="CF_SQL_INTEGER" variable="p_retour" type="out"/>
		</cfstoredproc>
		
		<cfreturn p_retour>	
	</cffunction>
	
	<!--- 
		Affichage des logs des commandes pour une racine ou pour toutes les racines si idracine = 0
		entre deux dates
	 --->
	<cffunction name="getlogsCommande" returntype="query">	
		
		<cfargument name="idRacine" type="Numeric" 	required="true">
		<cfargument name="dateDebut" type="Date" 	required="true">
		<cfargument name="dateFin" type="Date" 		required="true">
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.get_logs_commande">
			<cfprocparam cfsqltype="CF_SQL_INTEGER" variable="IDRACINE" type="in" value="#idRacine#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_date_debut" type="in"   value="#lsDateFormat(dateDebut,'YYYY/MM/DD')#"/>
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_date_fin" type="in"   value="#lsDateFormat(dateDebut,'YYYY/MM/DD')#"/>
			<cfprocresult name="P_RESULT">
		</cfstoredproc>
		<cfreturn P_RESULT>	
	</cffunction>

	<!--- AFFICHAGE DES LOGS DES COMMANDES SUIVANT L'UUID --->

	<cffunction name="getlogsCommandeuuid" returntype="Any" access="remote" hint="AFFICHAGE DES LOGS DES COMMANDES SUIVANT L'UUID">
		<cfargument name="uuidlog" 		type="String" 	required="true">
<!--- 		<cfargument name="idcommande" 	type="Numeric" 	required="false">
		<cfargument name="dirsource" 	type="String" 	required="false"> --->
		
		<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m16.get_logs_commande_uuid">
			<cfprocparam cfsqltype="CF_SQL_VARCHAR" variable="p_uuidlog" type="IN"   value="#uuidlog#"/>
			<cfprocresult name="p_result">
		</cfstoredproc>

		<cfreturn p_result>	
	</cffunction>

</cfcomponent>