package gestionparc.event
{
	import flash.events.Event;

	public class ZenPriseDataEvent extends Event
	{
		public static const ZPDE_DATA_COMPLETE:String="zpdeDataComplete";

		public function ZenPriseDataEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}

		override public function clone():Event
		{
			return new ZenPriseDataEvent(type, bubbles, cancelable);
		}

	}
}
