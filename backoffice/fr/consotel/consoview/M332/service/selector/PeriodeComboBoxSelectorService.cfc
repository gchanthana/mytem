﻿<cfcomponent>
	<cffunction access="public" output="false" name="getValuesPeriode" returntype="query">
			
			 <cfargument name="idRapportParametre" type="numeric" required="true">
		     <cfset idLang  = SESSION.USER.IDGLOBALIZATION>
             
			<cfstoredproc datasource="ROCOFFRE" procedure="PKG_M331.get_PeriodeComboBoxSelector">
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idRapportParametre#" variable="p_idrapport_parametre ">*
				<cfprocparam type="in" cfsqltype="CF_SQL_INTEGER" value="#idlang#" variable="p_langid">
				<cfprocresult name="qPeriode">
			</cfstoredproc>				
            <cfreturn qPeriode><!--- le resultat--->
      </cffunction> 
</cfcomponent>