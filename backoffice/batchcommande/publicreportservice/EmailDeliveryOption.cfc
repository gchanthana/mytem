<cfcomponent name="EmailDeliveryOption" displayname="EmailDeliveryOption">
	<cfset variables.EmailDeliveryOption=structNew()>
	
	<cffunction name="setEmailDeliveryOption" access="package" returntype="void">
		<cfargument name="emailBCC" type="String" required="false">
		<cfargument name="emailBody" type="String" required="false">
		<cfargument name="emailCC" type="String" required="false">
		<cfargument name="emailFrom" type="String" required="true">
		<cfargument name="emailReplyTo" type="String" required="false">
		<cfargument name="emailSubject" type="String" required="true">
		<cfargument name="emailTo" type="String" required="true">
		<cfif emailBody eq "">
			<cfset structDelete(variables.EmailDeliveryOption,"emailBody")>
		<cfelse>
			<cfset structInsert(variables.EmailDeliveryOption,"emailBody",emailBody,true)>
		</cfif>
		<cfif emailCC eq "">
			<cfset structDelete(variables.EmailDeliveryOption,"emailCC")>
		<cfelse>
			<cfset structInsert(variables.EmailDeliveryOption,"emailCC",emailCC,true)>
		</cfif>
		<cfif emailBCC eq "">
			<cfset structDelete(variables.EmailDeliveryOption,"emailBCC")>
		<cfelse>
			<cfset structInsert(variables.EmailDeliveryOption,"emailBCC",emailBCC,true)>
		</cfif>
		<cfif emailReplyTo eq "">
			<cfset structDelete(variables.EmailDeliveryOption,"emailReplyTo")>
		<cfelse>
			<cfset structInsert(variables.EmailDeliveryOption,"emailReplyTo",emailReplyTo,true)>
		</cfif>
		<cfset structInsert(variables.EmailDeliveryOption,"emailFrom",emailFrom,true)>
		<cfset structInsert(variables.EmailDeliveryOption,"emailSubject",emailSubject,true)>
		<cfset structInsert(variables.EmailDeliveryOption,"emailTo",emailTo,true)>
	</cffunction>
	
	<cffunction name="getEmailDeliveryOption" access="package" returntype="Struct">
		<cfreturn variables.EmailDeliveryOption>
	</cffunction>
</cfcomponent>