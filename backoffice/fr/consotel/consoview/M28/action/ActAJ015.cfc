<cfcomponent extends="fr.consotel.consoview.M28.action.AbstractAction" hint="Insertion d'un contrat" author="Florian">
	
	<cffunction name="executeAct" access="remote" returntype="Struct" hint="">
		<cfargument name="data" type="Struct" required="true" hint="contient 7 variables">
		
		<cftry>
			<cfstoredproc datasource="ROCOFFRE" procedure="pkg_m28_Partenaire.IContrat">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_Fc_Partenaireid#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idfc_certification#">
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="in" value="#data.p_idfc_type_plan_tarif#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_deb_contrat, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_fin_contrat, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_deb_plantarif, 'YYYY/MM/DD')#">
				<cfprocparam CFSQLTYPE="CF_SQL_VARCHAR" type="in" value="#lsdateFormat(data.p_date_fin_plantarif, 'YYYY/MM/DD')#">
				
				<cfprocparam CFSQLTYPE="CF_SQL_INTEGER" type="out" variable="p_retour">
			</cfstoredproc>
				
			<cfif p_retour GTE 1>
				<cfset data = structNew()>
				<cfset data.RESULT = p_retour>
				<cfreturn CreateReponse("AJ015", 1, data)/>
			<cfelseif p_retour EQ -1>
				<cfset err = ThrowCfCatchError(p_retour, "", "Erreur inconnue", "", "")>
				<cfreturn CreateReponse("AJ015", 0, err)/>
			<cfelseif p_retour EQ -10>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de chevauchement de date", "", "")>
				<cfreturn CreateReponse("AJ015", 0, err)/>
			<cfelseif p_retour EQ -20>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème insertion de plan tarifaire", "", "")>
				<cfreturn CreateReponse("AJ015", 0, err)/>
			<cfelseif p_retour EQ -100>
				<cfset err = ThrowCfCatchError(p_retour, "", "Problème de paramètres d'entrées obligatoires", "", "")>
				<cfreturn CreateReponse("AJ015", 0, err)/>
			</cfif>
			<cfcatch type="Any">
				<cfreturn CreateReponse("AJ015", 0, ThrowCfCatchError("AJ015",CFCATCH.TYPE,CFCATCH.MESSAGE,CFCATCH.DETAIL,CFCATCH.STACKTRACE))>
			</cfcatch>
		</cftry>
		
	</cffunction>
	
</cfcomponent>